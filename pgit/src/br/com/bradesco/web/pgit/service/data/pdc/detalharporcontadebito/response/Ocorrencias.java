/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdDigitoAgencia
     */
    private int _cdDigitoAgencia = 0;

    /**
     * keeps track of state for field: _cdDigitoAgencia
     */
    private boolean _has_cdDigitoAgencia;

    /**
     * Field _cdContaBancaria
     */
    private long _cdContaBancaria = 0;

    /**
     * keeps track of state for field: _cdContaBancaria
     */
    private boolean _has_cdContaBancaria;

    /**
     * Field _cdDigitoContaBancaria
     */
    private java.lang.String _cdDigitoContaBancaria;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _cdPessoaContratoDebito
     */
    private long _cdPessoaContratoDebito = 0;

    /**
     * keeps track of state for field: _cdPessoaContratoDebito
     */
    private boolean _has_cdPessoaContratoDebito;

    /**
     * Field _cdTipoContratoDebito
     */
    private int _cdTipoContratoDebito = 0;

    /**
     * keeps track of state for field: _cdTipoContratoDebito
     */
    private boolean _has_cdTipoContratoDebito;

    /**
     * Field _nrSequenciaContratoDebito
     */
    private long _nrSequenciaContratoDebito = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoDebito
     */
    private boolean _has_nrSequenciaContratoDebito;

    /**
     * Field _vlrPagamentoPendente
     */
    private java.math.BigDecimal _vlrPagamentoPendente = new java.math.BigDecimal("0");

    /**
     * Field _vlrPagamentoSemSaldo
     */
    private java.math.BigDecimal _vlrPagamentoSemSaldo = new java.math.BigDecimal("0");

    /**
     * Field _vlrPagamentoAgendado
     */
    private java.math.BigDecimal _vlrPagamentoAgendado = new java.math.BigDecimal("0");

    /**
     * Field _vlrPagamentoEfetivado
     */
    private java.math.BigDecimal _vlrPagamentoEfetivado = new java.math.BigDecimal("0");

    /**
     * Field _vlTotalPagamentos
     */
    private java.math.BigDecimal _vlTotalPagamentos = new java.math.BigDecimal("0");


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setVlrPagamentoPendente(new java.math.BigDecimal("0"));
        setVlrPagamentoSemSaldo(new java.math.BigDecimal("0"));
        setVlrPagamentoAgendado(new java.math.BigDecimal("0"));
        setVlrPagamentoEfetivado(new java.math.BigDecimal("0"));
        setVlTotalPagamentos(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdContaBancaria
     * 
     */
    public void deleteCdContaBancaria()
    {
        this._has_cdContaBancaria= false;
    } //-- void deleteCdContaBancaria() 

    /**
     * Method deleteCdDigitoAgencia
     * 
     */
    public void deleteCdDigitoAgencia()
    {
        this._has_cdDigitoAgencia= false;
    } //-- void deleteCdDigitoAgencia() 

    /**
     * Method deleteCdPessoaContratoDebito
     * 
     */
    public void deleteCdPessoaContratoDebito()
    {
        this._has_cdPessoaContratoDebito= false;
    } //-- void deleteCdPessoaContratoDebito() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteCdTipoContratoDebito
     * 
     */
    public void deleteCdTipoContratoDebito()
    {
        this._has_cdTipoContratoDebito= false;
    } //-- void deleteCdTipoContratoDebito() 

    /**
     * Method deleteNrSequenciaContratoDebito
     * 
     */
    public void deleteNrSequenciaContratoDebito()
    {
        this._has_nrSequenciaContratoDebito= false;
    } //-- void deleteNrSequenciaContratoDebito() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdContaBancaria'.
     * 
     * @return long
     * @return the value of field 'cdContaBancaria'.
     */
    public long getCdContaBancaria()
    {
        return this._cdContaBancaria;
    } //-- long getCdContaBancaria() 

    /**
     * Returns the value of field 'cdDigitoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdDigitoAgencia'.
     */
    public int getCdDigitoAgencia()
    {
        return this._cdDigitoAgencia;
    } //-- int getCdDigitoAgencia() 

    /**
     * Returns the value of field 'cdDigitoContaBancaria'.
     * 
     * @return String
     * @return the value of field 'cdDigitoContaBancaria'.
     */
    public java.lang.String getCdDigitoContaBancaria()
    {
        return this._cdDigitoContaBancaria;
    } //-- java.lang.String getCdDigitoContaBancaria() 

    /**
     * Returns the value of field 'cdPessoaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'cdPessoaContratoDebito'.
     */
    public long getCdPessoaContratoDebito()
    {
        return this._cdPessoaContratoDebito;
    } //-- long getCdPessoaContratoDebito() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'cdTipoContratoDebito'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoDebito'.
     */
    public int getCdTipoContratoDebito()
    {
        return this._cdTipoContratoDebito;
    } //-- int getCdTipoContratoDebito() 

    /**
     * Returns the value of field 'nrSequenciaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoDebito'.
     */
    public long getNrSequenciaContratoDebito()
    {
        return this._nrSequenciaContratoDebito;
    } //-- long getNrSequenciaContratoDebito() 

    /**
     * Returns the value of field 'vlTotalPagamentos'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlTotalPagamentos'.
     */
    public java.math.BigDecimal getVlTotalPagamentos()
    {
        return this._vlTotalPagamentos;
    } //-- java.math.BigDecimal getVlTotalPagamentos() 

    /**
     * Returns the value of field 'vlrPagamentoAgendado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrPagamentoAgendado'.
     */
    public java.math.BigDecimal getVlrPagamentoAgendado()
    {
        return this._vlrPagamentoAgendado;
    } //-- java.math.BigDecimal getVlrPagamentoAgendado() 

    /**
     * Returns the value of field 'vlrPagamentoEfetivado'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrPagamentoEfetivado'.
     */
    public java.math.BigDecimal getVlrPagamentoEfetivado()
    {
        return this._vlrPagamentoEfetivado;
    } //-- java.math.BigDecimal getVlrPagamentoEfetivado() 

    /**
     * Returns the value of field 'vlrPagamentoPendente'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrPagamentoPendente'.
     */
    public java.math.BigDecimal getVlrPagamentoPendente()
    {
        return this._vlrPagamentoPendente;
    } //-- java.math.BigDecimal getVlrPagamentoPendente() 

    /**
     * Returns the value of field 'vlrPagamentoSemSaldo'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlrPagamentoSemSaldo'.
     */
    public java.math.BigDecimal getVlrPagamentoSemSaldo()
    {
        return this._vlrPagamentoSemSaldo;
    } //-- java.math.BigDecimal getVlrPagamentoSemSaldo() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdContaBancaria
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaBancaria()
    {
        return this._has_cdContaBancaria;
    } //-- boolean hasCdContaBancaria() 

    /**
     * Method hasCdDigitoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDigitoAgencia()
    {
        return this._has_cdDigitoAgencia;
    } //-- boolean hasCdDigitoAgencia() 

    /**
     * Method hasCdPessoaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaContratoDebito()
    {
        return this._has_cdPessoaContratoDebito;
    } //-- boolean hasCdPessoaContratoDebito() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasCdTipoContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoDebito()
    {
        return this._has_cdTipoContratoDebito;
    } //-- boolean hasCdTipoContratoDebito() 

    /**
     * Method hasNrSequenciaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoDebito()
    {
        return this._has_nrSequenciaContratoDebito;
    } //-- boolean hasNrSequenciaContratoDebito() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdContaBancaria'.
     * 
     * @param cdContaBancaria the value of field 'cdContaBancaria'.
     */
    public void setCdContaBancaria(long cdContaBancaria)
    {
        this._cdContaBancaria = cdContaBancaria;
        this._has_cdContaBancaria = true;
    } //-- void setCdContaBancaria(long) 

    /**
     * Sets the value of field 'cdDigitoAgencia'.
     * 
     * @param cdDigitoAgencia the value of field 'cdDigitoAgencia'.
     */
    public void setCdDigitoAgencia(int cdDigitoAgencia)
    {
        this._cdDigitoAgencia = cdDigitoAgencia;
        this._has_cdDigitoAgencia = true;
    } //-- void setCdDigitoAgencia(int) 

    /**
     * Sets the value of field 'cdDigitoContaBancaria'.
     * 
     * @param cdDigitoContaBancaria the value of field
     * 'cdDigitoContaBancaria'.
     */
    public void setCdDigitoContaBancaria(java.lang.String cdDigitoContaBancaria)
    {
        this._cdDigitoContaBancaria = cdDigitoContaBancaria;
    } //-- void setCdDigitoContaBancaria(java.lang.String) 

    /**
     * Sets the value of field 'cdPessoaContratoDebito'.
     * 
     * @param cdPessoaContratoDebito the value of field
     * 'cdPessoaContratoDebito'.
     */
    public void setCdPessoaContratoDebito(long cdPessoaContratoDebito)
    {
        this._cdPessoaContratoDebito = cdPessoaContratoDebito;
        this._has_cdPessoaContratoDebito = true;
    } //-- void setCdPessoaContratoDebito(long) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoDebito'.
     * 
     * @param cdTipoContratoDebito the value of field
     * 'cdTipoContratoDebito'.
     */
    public void setCdTipoContratoDebito(int cdTipoContratoDebito)
    {
        this._cdTipoContratoDebito = cdTipoContratoDebito;
        this._has_cdTipoContratoDebito = true;
    } //-- void setCdTipoContratoDebito(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoDebito'.
     * 
     * @param nrSequenciaContratoDebito the value of field
     * 'nrSequenciaContratoDebito'.
     */
    public void setNrSequenciaContratoDebito(long nrSequenciaContratoDebito)
    {
        this._nrSequenciaContratoDebito = nrSequenciaContratoDebito;
        this._has_nrSequenciaContratoDebito = true;
    } //-- void setNrSequenciaContratoDebito(long) 

    /**
     * Sets the value of field 'vlTotalPagamentos'.
     * 
     * @param vlTotalPagamentos the value of field
     * 'vlTotalPagamentos'.
     */
    public void setVlTotalPagamentos(java.math.BigDecimal vlTotalPagamentos)
    {
        this._vlTotalPagamentos = vlTotalPagamentos;
    } //-- void setVlTotalPagamentos(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrPagamentoAgendado'.
     * 
     * @param vlrPagamentoAgendado the value of field
     * 'vlrPagamentoAgendado'.
     */
    public void setVlrPagamentoAgendado(java.math.BigDecimal vlrPagamentoAgendado)
    {
        this._vlrPagamentoAgendado = vlrPagamentoAgendado;
    } //-- void setVlrPagamentoAgendado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrPagamentoEfetivado'.
     * 
     * @param vlrPagamentoEfetivado the value of field
     * 'vlrPagamentoEfetivado'.
     */
    public void setVlrPagamentoEfetivado(java.math.BigDecimal vlrPagamentoEfetivado)
    {
        this._vlrPagamentoEfetivado = vlrPagamentoEfetivado;
    } //-- void setVlrPagamentoEfetivado(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrPagamentoPendente'.
     * 
     * @param vlrPagamentoPendente the value of field
     * 'vlrPagamentoPendente'.
     */
    public void setVlrPagamentoPendente(java.math.BigDecimal vlrPagamentoPendente)
    {
        this._vlrPagamentoPendente = vlrPagamentoPendente;
    } //-- void setVlrPagamentoPendente(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlrPagamentoSemSaldo'.
     * 
     * @param vlrPagamentoSemSaldo the value of field
     * 'vlrPagamentoSemSaldo'.
     */
    public void setVlrPagamentoSemSaldo(java.math.BigDecimal vlrPagamentoSemSaldo)
    {
        this._vlrPagamentoSemSaldo = vlrPagamentoSemSaldo;
    } //-- void setVlrPagamentoSemSaldo(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharporcontadebito.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
