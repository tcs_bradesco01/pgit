/*
 * Nome: br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean;

// TODO: Auto-generated Javadoc
/**
 * Nome: ConsultarManutencaoServicoContratoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarManutencaoServicoContratoSaidaDTO {
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;	
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;	
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo cdCanalManutencao. */
	private int cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;	
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo cdCanalInclusao. */
	private int cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	//private String dsAcao;
	/** Atributo dsMotivo. */
	private String dsMotivo;
	
	/** Atributo dsServico. */
	private String dsServico;
	
	/** Atributo dsModalidade. */
	private String dsModalidade;
	
	/** Atributo dsSituacao. */
	private String dsSituacao;
	
	/** Atributo dsTipo. */
	private String dsTipo;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdTipoManutencaoContrato. */
	private Integer cdTipoManutencaoContrato;
	
	/** Atributo nrManutencaoContratoNegocio. */
	private Long nrManutencaoContratoNegocio;
	
	/** Atributo cdSituacaoManutencaoContrato. */
	private Integer cdSituacaoManutencaoContrato;
	
	/** Atributo cdMotivoManutencaoContrato. */
	private Integer cdMotivoManutencaoContrato;
	
	/** Atributo nrAditivoContratoNegocio. */
	private Long nrAditivoContratoNegocio;
	
	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;
	
	/** Atributo cdModalidadeServico. */
	private Integer cdModalidadeServico;
	
	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;
	
	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;
	
    /** Atributo dsTipoDataFloat. */
    private String dsTipoDataFloat;
    
    /** Atributo dsFloarServicoContratado. */
    private String dsFloarServicoContratado;
    
    /** Atributo dsMomentoProcessamentoPagamento. */
    private String dsMomentoProcessamentoPagamento;
    
    /** Atributo dsPagamentoNaoUtil. */
    private String dsPagamentoNaoUtil;
    
    /** Atributo dsIndicadorFeriadoLocal. */
    private String dsIndicadorFeriadoLocal;
    
    /** Atributo cdFloatServicoContratado. */
    private Integer cdFloatServicoContratado;
    
    /** Atributo cdPagamentoNaoUtil. */
    private Integer cdPagamentoNaoUtil;
    
    /** Atributo cdMomentoProcessamentoPagamento. */
    private Integer cdMomentoProcessamentoPagamento;
    
    /** Atributo cdIndicadorFeriadoLocal. */
    private Integer cdIndicadorFeriadoLocal;
    
    /** Atributo cdTipoDataFloat. */
    private Integer cdTipoDataFloat;
    
    /** Atributo cdIndicadorDiaFloat. */
    private Integer cdIndicadorDiaFloat;
    
    /** Atributo cdIndicadorDiaRepique. */
    private Integer cdIndicadorDiaRepique;
    
    /** Atributo qtdeDiaFloatPagamento. */
    private Integer qtdeDiaFloatPagamento;
    
    /** Atributo qtdeDiaRepiqueCons. */
    private Integer qtdeDiaRepiqueCons;
	
	
			
	/*public String getDsAcao() {
		return dsAcao;
	}
	public void setDsAcao(String dsAcao) {
		this.dsAcao = dsAcao;
	}*/
	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}
	
	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}
	
	/**
	 * Get: dsMotivo.
	 *
	 * @return dsMotivo
	 */
	public String getDsMotivo() {
		return dsMotivo;
	}
	
	/**
	 * Set: dsMotivo.
	 *
	 * @param dsMotivo the ds motivo
	 */
	public void setDsMotivo(String dsMotivo) {
		this.dsMotivo = dsMotivo;
	}
	
	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}
	
	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}
	
	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}
	
	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}
	
	/**
	 * Get: dsTipo.
	 *
	 * @return dsTipo
	 */
	public String getDsTipo() {
		return dsTipo;
	}
	
	/**
	 * Set: dsTipo.
	 *
	 * @param dsTipo the ds tipo
	 */
	public void setDsTipo(String dsTipo) {
		this.dsTipo = dsTipo;
	}
	
	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public int getCdCanalInclusao() {
		return cdCanalInclusao;
	}
	
	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(int cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}
	
	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public int getCdCanalManutencao() {
		return cdCanalManutencao;
	}
	
	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(int cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}
	
	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}
	
	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}
	
	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: cdModalidadeServico.
	 *
	 * @return cdModalidadeServico
	 */
	public Integer getCdModalidadeServico() {
		return cdModalidadeServico;
	}
	
	/**
	 * Set: cdModalidadeServico.
	 *
	 * @param cdModalidadeServico the cd modalidade servico
	 */
	public void setCdModalidadeServico(Integer cdModalidadeServico) {
		this.cdModalidadeServico = cdModalidadeServico;
	}
	
	/**
	 * Get: cdMotivoManutencaoContrato.
	 *
	 * @return cdMotivoManutencaoContrato
	 */
	public Integer getCdMotivoManutencaoContrato() {
		return cdMotivoManutencaoContrato;
	}
	
	/**
	 * Set: cdMotivoManutencaoContrato.
	 *
	 * @param cdMotivoManutencaoContrato the cd motivo manutencao contrato
	 */
	public void setCdMotivoManutencaoContrato(Integer cdMotivoManutencaoContrato) {
		this.cdMotivoManutencaoContrato = cdMotivoManutencaoContrato;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdSituacaoManutencaoContrato.
	 *
	 * @return cdSituacaoManutencaoContrato
	 */
	public Integer getCdSituacaoManutencaoContrato() {
		return cdSituacaoManutencaoContrato;
	}
	
	/**
	 * Set: cdSituacaoManutencaoContrato.
	 *
	 * @param cdSituacaoManutencaoContrato the cd situacao manutencao contrato
	 */
	public void setCdSituacaoManutencaoContrato(Integer cdSituacaoManutencaoContrato) {
		this.cdSituacaoManutencaoContrato = cdSituacaoManutencaoContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoManutencaoContrato.
	 *
	 * @return cdTipoManutencaoContrato
	 */
	public Integer getCdTipoManutencaoContrato() {
		return cdTipoManutencaoContrato;
	}
	
	/**
	 * Set: cdTipoManutencaoContrato.
	 *
	 * @param cdTipoManutencaoContrato the cd tipo manutencao contrato
	 */
	public void setCdTipoManutencaoContrato(Integer cdTipoManutencaoContrato) {
		this.cdTipoManutencaoContrato = cdTipoManutencaoContrato;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: cdUsuarioInclusaoExterno.
	 *
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Set: cdUsuarioInclusaoExterno.
	 *
	 * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}
	
	/**
	 * Get: cdUsuarioManutencaoExterno.
	 *
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Set: cdUsuarioManutencaoExterno.
	 *
	 * @param cdUsuarioManutencaoExterno the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}
	
	/**
	 * Get: nrAditivoContratoNegocio.
	 *
	 * @return nrAditivoContratoNegocio
	 */
	public Long getNrAditivoContratoNegocio() {
		return nrAditivoContratoNegocio;
	}
	
	/**
	 * Set: nrAditivoContratoNegocio.
	 *
	 * @param nrAditivoContratoNegocio the nr aditivo contrato negocio
	 */
	public void setNrAditivoContratoNegocio(Long nrAditivoContratoNegocio) {
		this.nrAditivoContratoNegocio = nrAditivoContratoNegocio;
	}
	
	/**
	 * Get: nrManutencaoContratoNegocio.
	 *
	 * @return nrManutencaoContratoNegocio
	 */
	public Long getNrManutencaoContratoNegocio() {
		return nrManutencaoContratoNegocio;
	}
	
	/**
	 * Set: nrManutencaoContratoNegocio.
	 *
	 * @param nrManutencaoContratoNegocio the nr manutencao contrato negocio
	 */
	public void setNrManutencaoContratoNegocio(Long nrManutencaoContratoNegocio) {
		this.nrManutencaoContratoNegocio = nrManutencaoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

    /**
     * Get: dsTipoDataFloat.
     *
     * @return dsTipoDataFloat
     */
    public String getDsTipoDataFloat() {
        return dsTipoDataFloat;
    }

    /**
     * Set: dsTipoDataFloat.
     *
     * @param dsTipoDataFloat the ds tipo data float
     */
    public void setDsTipoDataFloat(String dsTipoDataFloat) {
        this.dsTipoDataFloat = dsTipoDataFloat;
    }

    /**
     * Get: dsFloarServicoContratado.
     *
     * @return dsFloarServicoContratado
     */
    public String getDsFloarServicoContratado() {
        return dsFloarServicoContratado;
    }

    /**
     * Set: dsFloarServicoContratado.
     *
     * @param dsFloarServicoContratado the ds floar servico contratado
     */
    public void setDsFloarServicoContratado(String dsFloarServicoContratado) {
        this.dsFloarServicoContratado = dsFloarServicoContratado;
    }

    /**
     * Get: dsMomentoProcessamentoPagamento.
     *
     * @return dsMomentoProcessamentoPagamento
     */
    public String getDsMomentoProcessamentoPagamento() {
        return dsMomentoProcessamentoPagamento;
    }

    /**
     * Set: dsMomentoProcessamentoPagamento.
     *
     * @param dsMomentoProcessamentoPagamento the ds momento processamento pagamento
     */
    public void setDsMomentoProcessamentoPagamento(String dsMomentoProcessamentoPagamento) {
        this.dsMomentoProcessamentoPagamento = dsMomentoProcessamentoPagamento;
    }

    /**
     * Get: dsPagamentoNaoUtil.
     *
     * @return dsPagamentoNaoUtil
     */
    public String getDsPagamentoNaoUtil() {
        return dsPagamentoNaoUtil;
    }

    /**
     * Set: dsPagamentoNaoUtil.
     *
     * @param dsPagamentoNaoUtil the ds pagamento nao util
     */
    public void setDsPagamentoNaoUtil(String dsPagamentoNaoUtil) {
        this.dsPagamentoNaoUtil = dsPagamentoNaoUtil;
    }

    /**
     * Get: dsIndicadorFeriadoLocal.
     *
     * @return dsIndicadorFeriadoLocal
     */
    public String getDsIndicadorFeriadoLocal() {
        return dsIndicadorFeriadoLocal;
    }

    /**
     * Set: dsIndicadorFeriadoLocal.
     *
     * @param dsIndicadorFeriadoLocal the ds indicador feriado local
     */
    public void setDsIndicadorFeriadoLocal(String dsIndicadorFeriadoLocal) {
        this.dsIndicadorFeriadoLocal = dsIndicadorFeriadoLocal;
    }

    /**
     * Get: cdFloatServicoContratado.
     *
     * @return cdFloatServicoContratado
     */
    public Integer getCdFloatServicoContratado() {
        return cdFloatServicoContratado;
    }

    /**
     * Set: cdFloatServicoContratado.
     *
     * @param cdFloatServicoContratado the cd float servico contratado
     */
    public void setCdFloatServicoContratado(Integer cdFloatServicoContratado) {
        this.cdFloatServicoContratado = cdFloatServicoContratado;
    }

    /**
     * Get: cdPagamentoNaoUtil.
     *
     * @return cdPagamentoNaoUtil
     */
    public Integer getCdPagamentoNaoUtil() {
        return cdPagamentoNaoUtil;
    }

    /**
     * Set: cdPagamentoNaoUtil.
     *
     * @param cdPagamentoNaoUtil the cd pagamento nao util
     */
    public void setCdPagamentoNaoUtil(Integer cdPagamentoNaoUtil) {
        this.cdPagamentoNaoUtil = cdPagamentoNaoUtil;
    }

    /**
     * Get: cdMomentoProcessamentoPagamento.
     *
     * @return cdMomentoProcessamentoPagamento
     */
    public Integer getCdMomentoProcessamentoPagamento() {
        return cdMomentoProcessamentoPagamento;
    }

    /**
     * Set: cdMomentoProcessamentoPagamento.
     *
     * @param cdMomentoProcessamentoPagamento the cd momento processamento pagamento
     */
    public void setCdMomentoProcessamentoPagamento(Integer cdMomentoProcessamentoPagamento) {
        this.cdMomentoProcessamentoPagamento = cdMomentoProcessamentoPagamento;
    }

    /**
     * Get: cdIndicadorFeriadoLocal.
     *
     * @return cdIndicadorFeriadoLocal
     */
    public Integer getCdIndicadorFeriadoLocal() {
        return cdIndicadorFeriadoLocal;
    }

    /**
     * Set: cdIndicadorFeriadoLocal.
     *
     * @param cdIndicadorFeriadoLocal the cd indicador feriado local
     */
    public void setCdIndicadorFeriadoLocal(Integer cdIndicadorFeriadoLocal) {
        this.cdIndicadorFeriadoLocal = cdIndicadorFeriadoLocal;
    }

    /**
     * Get: cdTipoDataFloat.
     *
     * @return cdTipoDataFloat
     */
    public Integer getCdTipoDataFloat() {
        return cdTipoDataFloat;
    }

    /**
     * Set: cdTipoDataFloat.
     *
     * @param cdTipoDataFloat the cd tipo data float
     */
    public void setCdTipoDataFloat(Integer cdTipoDataFloat) {
        this.cdTipoDataFloat = cdTipoDataFloat;
    }

    /**
     * Get: cdIndicadorDiaFloat.
     *
     * @return cdIndicadorDiaFloat
     */
    public Integer getCdIndicadorDiaFloat() {
        return cdIndicadorDiaFloat;
    }

    /**
     * Set: cdIndicadorDiaFloat.
     *
     * @param cdIndicadorDiaFloat the cd indicador dia float
     */
    public void setCdIndicadorDiaFloat(Integer cdIndicadorDiaFloat) {
        this.cdIndicadorDiaFloat = cdIndicadorDiaFloat;
    }

    /**
     * Get: cdIndicadorDiaRepique.
     *
     * @return cdIndicadorDiaRepique
     */
    public Integer getCdIndicadorDiaRepique() {
        return cdIndicadorDiaRepique;
    }

    /**
     * Set: cdIndicadorDiaRepique.
     *
     * @param cdIndicadorDiaRepique the cd indicador dia repique
     */
    public void setCdIndicadorDiaRepique(Integer cdIndicadorDiaRepique) {
        this.cdIndicadorDiaRepique = cdIndicadorDiaRepique;
    }

    /**
     * Get: qtdeDiaFloatPagamento.
     *
     * @return qtdeDiaFloatPagamento
     */
    public Integer getQtdeDiaFloatPagamento() {
        return qtdeDiaFloatPagamento;
    }

    /**
     * Set: qtdeDiaFloatPagamento.
     *
     * @param qtdeDiaFloatPagamento the qtde dia float pagamento
     */
    public void setQtdeDiaFloatPagamento(Integer qtdeDiaFloatPagamento) {
        this.qtdeDiaFloatPagamento = qtdeDiaFloatPagamento;
    }

    /**
     * Get: qtdeDiaRepiqueCons.
     *
     * @return qtdeDiaRepiqueCons
     */
    public Integer getQtdeDiaRepiqueCons() {
        return qtdeDiaRepiqueCons;
    }

    /**
     * Set: qtdeDiaRepiqueCons.
     *
     * @param qtdeDiaRepiqueCons the qtde dia repique cons
     */
    public void setQtdeDiaRepiqueCons(Integer qtdeDiaRepiqueCons) {
        this.qtdeDiaRepiqueCons = qtdeDiaRepiqueCons;
    }
	
	


}
