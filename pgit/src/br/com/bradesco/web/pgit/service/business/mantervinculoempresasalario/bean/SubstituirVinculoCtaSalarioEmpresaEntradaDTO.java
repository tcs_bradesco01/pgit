/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean;

/**
 * Nome: SubstituirVinculoCtaSalarioEmpresaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SubstituirVinculoCtaSalarioEmpresaEntradaDTO {
	
	 /** Atributo cdPessoa. */
 	private Long cdPessoa;
     
     /** Atributo cdTipoContrato. */
     private Integer cdTipoContrato;
     
     /** Atributo nrSeqContrato. */
     private Long nrSeqContrato;
     
     /** Atributo cdPessoaJuridicaVinculo. */
     private Long cdPessoaJuridicaVinculo;
     
     /** Atributo cdTipoContratoVinculo. */
     private Integer cdTipoContratoVinculo;
     
     /** Atributo nrSeqContratoVinculo. */
     private Long nrSeqContratoVinculo;
     
     /** Atributo cdTipoVinculoContrato. */
     private Integer cdTipoVinculoContrato;
     
     /** Atributo cdBancoAtualizado. */
     private Integer cdBancoAtualizado;
     
     /** Atributo dsBancoAtualizado. */
     private String dsBancoAtualizado;
     
     /** Atributo cdAgenciaAtualizada. */
     private Integer cdAgenciaAtualizada;
     
     /** Atributo cdDigitoAgenciaAtualizada. */
     private Integer cdDigitoAgenciaAtualizada;
     
     /** Atributo dsAgenciaAtualizada. */
     private String dsAgenciaAtualizada;
     
     /** Atributo cdContaAtualizada. */
     private Long cdContaAtualizada;
     
     /** Atributo cdDigitoContaAtualizada. */
     private String cdDigitoContaAtualizada;

	/**
	 * Substituir vinculo cta salario empresa entrada dto.
	 */
	public SubstituirVinculoCtaSalarioEmpresaEntradaDTO() {
		super();
	}
	
	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco() {
		if (!dsBancoAtualizado.trim().equals("")  && dsBancoAtualizado != null) {
			return  cdBancoAtualizado + " " + dsBancoAtualizado;
		}else {
			return  String.valueOf(cdBancoAtualizado);
		}
	}
	
	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		if (!dsAgenciaAtualizada.trim().equals("") && dsAgenciaAtualizada != null) {
			return  cdAgenciaAtualizada + " " + dsAgenciaAtualizada;
		}else {
			return  String.valueOf(cdAgenciaAtualizada);
		}
	}
	
	/**
	 * Get: contaDigitoAtualizado.
	 *
	 * @return contaDigitoAtualizado
	 */
	public String getContaDigitoAtualizado(){
		return cdDigitoContaAtualizada.trim().equals("") ? String.valueOf(cdContaAtualizada) : cdContaAtualizada + "-" + cdDigitoContaAtualizada;
	}
	
	/**
	 * Get: cdDigitoAgenciaAtualizada.
	 *
	 * @return cdDigitoAgenciaAtualizada
	 */
	public Integer getCdDigitoAgenciaAtualizada() {
		return cdDigitoAgenciaAtualizada;
	}

	/**
	 * Set: cdDigitoAgenciaAtualizada.
	 *
	 * @param cdDigitoAgenciaAtualizada the cd digito agencia atualizada
	 */
	public void setCdDigitoAgenciaAtualizada(Integer cdDigitoAgenciaAtualizada) {
		this.cdDigitoAgenciaAtualizada = cdDigitoAgenciaAtualizada;
	}

	/**
	 * Get: cdAgenciaAtualizada.
	 *
	 * @return cdAgenciaAtualizada
	 */
	public Integer getCdAgenciaAtualizada() {
		return cdAgenciaAtualizada;
	}
	
	/**
	 * Set: cdAgenciaAtualizada.
	 *
	 * @param cdAgenciaAtualizada the cd agencia atualizada
	 */
	public void setCdAgenciaAtualizada(Integer cdAgenciaAtualizada) {
		this.cdAgenciaAtualizada = cdAgenciaAtualizada;
	}
	
	/**
	 * Get: cdBancoAtualizado.
	 *
	 * @return cdBancoAtualizado
	 */
	public Integer getCdBancoAtualizado() {
		return cdBancoAtualizado;
	}
	
	/**
	 * Set: cdBancoAtualizado.
	 *
	 * @param cdBancoAtualizado the cd banco atualizado
	 */
	public void setCdBancoAtualizado(Integer cdBancoAtualizado) {
		this.cdBancoAtualizado = cdBancoAtualizado;
	}
	
	/**
	 * Get: cdContaAtualizada.
	 *
	 * @return cdContaAtualizada
	 */
	public Long getCdContaAtualizada() {
		return cdContaAtualizada;
	}
	
	/**
	 * Set: cdContaAtualizada.
	 *
	 * @param cdContaAtualizada the cd conta atualizada
	 */
	public void setCdContaAtualizada(Long cdContaAtualizada) {
		this.cdContaAtualizada = cdContaAtualizada;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: nrSeqContrato.
	 *
	 * @return nrSeqContrato
	 */
	public Long getNrSeqContrato() {
		return nrSeqContrato;
	}
	
	/**
	 * Set: nrSeqContrato.
	 *
	 * @param nrSeqContrato the nr seq contrato
	 */
	public void setNrSeqContrato(Long nrSeqContrato) {
		this.nrSeqContrato = nrSeqContrato;
	}

	/**
	 * Get: cdDigitoContaAtualizada.
	 *
	 * @return cdDigitoContaAtualizada
	 */
	public String getCdDigitoContaAtualizada() {
		return cdDigitoContaAtualizada;
	}

	/**
	 * Set: cdDigitoContaAtualizada.
	 *
	 * @param cdDigitoContaAtualizada the cd digito conta atualizada
	 */
	public void setCdDigitoContaAtualizada(String cdDigitoContaAtualizada) {
		this.cdDigitoContaAtualizada = cdDigitoContaAtualizada;
	}

	/**
	 * Get: cdPessoaJuridicaVinculo.
	 *
	 * @return cdPessoaJuridicaVinculo
	 */
	public Long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}

	/**
	 * Set: cdPessoaJuridicaVinculo.
	 *
	 * @param cdPessoaJuridicaVinculo the cd pessoa juridica vinculo
	 */
	public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}

	/**
	 * Get: cdTipoContratoVinculo.
	 *
	 * @return cdTipoContratoVinculo
	 */
	public Integer getCdTipoContratoVinculo() {
		return cdTipoContratoVinculo;
	}

	/**
	 * Set: cdTipoContratoVinculo.
	 *
	 * @param cdTipoContratoVinculo the cd tipo contrato vinculo
	 */
	public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}

	/**
	 * Get: cdTipoVinculoContrato.
	 *
	 * @return cdTipoVinculoContrato
	 */
	public Integer getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}

	/**
	 * Set: cdTipoVinculoContrato.
	 *
	 * @param cdTipoVinculoContrato the cd tipo vinculo contrato
	 */
	public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}

	/**
	 * Get: nrSeqContratoVinculo.
	 *
	 * @return nrSeqContratoVinculo
	 */
	public Long getNrSeqContratoVinculo() {
		return nrSeqContratoVinculo;
	}

	/**
	 * Set: nrSeqContratoVinculo.
	 *
	 * @param nrSeqContratoVinculo the nr seq contrato vinculo
	 */
	public void setNrSeqContratoVinculo(Long nrSeqContratoVinculo) {
		this.nrSeqContratoVinculo = nrSeqContratoVinculo;
	}

	/**
	 * Get: dsAgenciaAtualizada.
	 *
	 * @return dsAgenciaAtualizada
	 */
	public String getDsAgenciaAtualizada() {
		return dsAgenciaAtualizada;
	}

	/**
	 * Set: dsAgenciaAtualizada.
	 *
	 * @param dsAgenciaAtualizada the ds agencia atualizada
	 */
	public void setDsAgenciaAtualizada(String dsAgenciaAtualizada) {
		this.dsAgenciaAtualizada = dsAgenciaAtualizada;
	}

	/**
	 * Get: dsBancoAtualizado.
	 *
	 * @return dsBancoAtualizado
	 */
	public String getDsBancoAtualizado() {
		return dsBancoAtualizado;
	}

	/**
	 * Set: dsBancoAtualizado.
	 *
	 * @param dsBancoAtualizado the ds banco atualizado
	 */
	public void setDsBancoAtualizado(String dsBancoAtualizado) {
		this.dsBancoAtualizado = dsBancoAtualizado;
	}

}
