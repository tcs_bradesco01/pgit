/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;

/**
 * Nome: ConsultarManterLimitesContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarManterLimitesContratoEntradaDTO {
//	ATRIBUTOS	
	/** Atributo cboServicoContratado. */
private Integer cboServicoContratado;
	
	/** Atributo cboOperacaoContratada. */
	private Integer cboOperacaoContratada;
	
	/** Atributo servicoGrid. */
	private String servicoGrid;
	
	/** Atributo operacaoGrid. */
	private String operacaoGrid;
	
	/** Atributo individualGrid. */
	private int individualGrid;
	
	/** Atributo diarioGrid. */
	private int diarioGrid;
//GET/SET
	/**
 * Get: cboOperacaoContratada.
 *
 * @return cboOperacaoContratada
 */
public Integer getCboOperacaoContratada() {
		return cboOperacaoContratada;
	}
	
	/**
	 * Set: cboOperacaoContratada.
	 *
	 * @param cboOperacaoContratada the cbo operacao contratada
	 */
	public void setCboOperacaoContratada(Integer cboOperacaoContratada) {
		this.cboOperacaoContratada = cboOperacaoContratada;
	}
	
	/**
	 * Get: cboServicoContratado.
	 *
	 * @return cboServicoContratado
	 */
	public Integer getCboServicoContratado() {
		return cboServicoContratado;
	}
	
	/**
	 * Set: cboServicoContratado.
	 *
	 * @param cboServicoContratado the cbo servico contratado
	 */
	public void setCboServicoContratado(Integer cboServicoContratado) {
		this.cboServicoContratado = cboServicoContratado;
	}
	
	/**
	 * Get: diarioGrid.
	 *
	 * @return diarioGrid
	 */
	public int getDiarioGrid() {
		return diarioGrid;
	}
	
	/**
	 * Set: diarioGrid.
	 *
	 * @param diarioGrid the diario grid
	 */
	public void setDiarioGrid(int diarioGrid) {
		this.diarioGrid = diarioGrid;
	}
	
	/**
	 * Get: individualGrid.
	 *
	 * @return individualGrid
	 */
	public int getIndividualGrid() {
		return individualGrid;
	}
	
	/**
	 * Set: individualGrid.
	 *
	 * @param individualGrid the individual grid
	 */
	public void setIndividualGrid(int individualGrid) {
		this.individualGrid = individualGrid;
	}
	
	/**
	 * Get: operacaoGrid.
	 *
	 * @return operacaoGrid
	 */
	public String getOperacaoGrid() {
		return operacaoGrid;
	}
	
	/**
	 * Set: operacaoGrid.
	 *
	 * @param operacaoGrid the operacao grid
	 */
	public void setOperacaoGrid(String operacaoGrid) {
		this.operacaoGrid = operacaoGrid;
	}
	
	/**
	 * Get: servicoGrid.
	 *
	 * @return servicoGrid
	 */
	public String getServicoGrid() {
		return servicoGrid;
	}
	
	/**
	 * Set: servicoGrid.
	 *
	 * @param servicoGrid the servico grid
	 */
	public void setServicoGrid(String servicoGrid) {
		this.servicoGrid = servicoGrid;
	}
	
}
