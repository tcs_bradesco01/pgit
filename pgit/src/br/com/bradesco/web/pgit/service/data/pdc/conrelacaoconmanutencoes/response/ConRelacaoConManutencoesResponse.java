/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConRelacaoConManutencoesResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConRelacaoConManutencoesResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _numeroOcorrenciasParticipante
     */
    private int _numeroOcorrenciasParticipante = 0;

    /**
     * keeps track of state for field: _numeroOcorrenciasParticipant
     */
    private boolean _has_numeroOcorrenciasParticipante;

    /**
     * Field _numeroOcorrenciasVinculacao
     */
    private int _numeroOcorrenciasVinculacao = 0;

    /**
     * keeps track of state for field: _numeroOcorrenciasVinculacao
     */
    private boolean _has_numeroOcorrenciasVinculacao;

    /**
     * Field _numeroOcorrenciasServico
     */
    private int _numeroOcorrenciasServico = 0;

    /**
     * keeps track of state for field: _numeroOcorrenciasServico
     */
    private boolean _has_numeroOcorrenciasServico;

    /**
     * Field _cdFormaAutorizacaoPagamento
     */
    private int _cdFormaAutorizacaoPagamento = 0;

    /**
     * keeps track of state for field: _cdFormaAutorizacaoPagamento
     */
    private boolean _has_cdFormaAutorizacaoPagamento;

    /**
     * Field _dsFormaAutorizacaoPagamento
     */
    private java.lang.String _dsFormaAutorizacaoPagamento;

    /**
     * Field _cdUnidadeOrganizacional
     */
    private int _cdUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _cdUnidadeOrganizacional
     */
    private boolean _has_cdUnidadeOrganizacional;

    /**
     * Field _dsUnidadeOrganizacional
     */
    private java.lang.String _dsUnidadeOrganizacional;

    /**
     * Field _cdFuncionarioGerenteContrato
     */
    private long _cdFuncionarioGerenteContrato = 0;

    /**
     * keeps track of state for field: _cdFuncionarioGerenteContrato
     */
    private boolean _has_cdFuncionarioGerenteContrato;

    /**
     * Field _dsFuncionarioGerenteContrato
     */
    private java.lang.String _dsFuncionarioGerenteContrato;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;

    /**
     * Field _ocorrencias1List
     */
    private java.util.Vector _ocorrencias1List;

    /**
     * Field _ocorrencias2List
     */
    private java.util.Vector _ocorrencias2List;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConRelacaoConManutencoesResponse() 
     {
        super();
        _ocorrenciasList = new Vector();
        _ocorrencias1List = new Vector();
        _ocorrencias2List = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.ConRelacaoConManutencoesResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias) 

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param vOcorrencias1
     */
    public void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias1List.addElement(vOcorrencias1);
    } //-- void addOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1) 

    /**
     * Method addOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void addOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias1List.insertElementAt(vOcorrencias1, index);
    } //-- void addOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param vOcorrencias2
     */
    public void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias2List.addElement(vOcorrencias2);
    } //-- void addOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2) 

    /**
     * Method addOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void addOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrencias2List.insertElementAt(vOcorrencias2, index);
    } //-- void addOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2) 

    /**
     * Method deleteCdFormaAutorizacaoPagamento
     * 
     */
    public void deleteCdFormaAutorizacaoPagamento()
    {
        this._has_cdFormaAutorizacaoPagamento= false;
    } //-- void deleteCdFormaAutorizacaoPagamento() 

    /**
     * Method deleteCdFuncionarioGerenteContrato
     * 
     */
    public void deleteCdFuncionarioGerenteContrato()
    {
        this._has_cdFuncionarioGerenteContrato= false;
    } //-- void deleteCdFuncionarioGerenteContrato() 

    /**
     * Method deleteCdUnidadeOrganizacional
     * 
     */
    public void deleteCdUnidadeOrganizacional()
    {
        this._has_cdUnidadeOrganizacional= false;
    } //-- void deleteCdUnidadeOrganizacional() 

    /**
     * Method deleteNumeroOcorrenciasParticipante
     * 
     */
    public void deleteNumeroOcorrenciasParticipante()
    {
        this._has_numeroOcorrenciasParticipante= false;
    } //-- void deleteNumeroOcorrenciasParticipante() 

    /**
     * Method deleteNumeroOcorrenciasServico
     * 
     */
    public void deleteNumeroOcorrenciasServico()
    {
        this._has_numeroOcorrenciasServico= false;
    } //-- void deleteNumeroOcorrenciasServico() 

    /**
     * Method deleteNumeroOcorrenciasVinculacao
     * 
     */
    public void deleteNumeroOcorrenciasVinculacao()
    {
        this._has_numeroOcorrenciasVinculacao= false;
    } //-- void deleteNumeroOcorrenciasVinculacao() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Method enumerateOcorrencias1
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias1()
    {
        return _ocorrencias1List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias1() 

    /**
     * Method enumerateOcorrencias2
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias2()
    {
        return _ocorrencias2List.elements();
    } //-- java.util.Enumeration enumerateOcorrencias2() 

    /**
     * Returns the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdFormaAutorizacaoPagamento'.
     */
    public int getCdFormaAutorizacaoPagamento()
    {
        return this._cdFormaAutorizacaoPagamento;
    } //-- int getCdFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'cdFuncionarioGerenteContrato'.
     * 
     * @return long
     * @return the value of field 'cdFuncionarioGerenteContrato'.
     */
    public long getCdFuncionarioGerenteContrato()
    {
        return this._cdFuncionarioGerenteContrato;
    } //-- long getCdFuncionarioGerenteContrato() 

    /**
     * Returns the value of field 'cdUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'cdUnidadeOrganizacional'.
     */
    public int getCdUnidadeOrganizacional()
    {
        return this._cdUnidadeOrganizacional;
    } //-- int getCdUnidadeOrganizacional() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaAutorizacaoPagamento'.
     */
    public java.lang.String getDsFormaAutorizacaoPagamento()
    {
        return this._dsFormaAutorizacaoPagamento;
    } //-- java.lang.String getDsFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field 'dsFuncionarioGerenteContrato'.
     * 
     * @return String
     * @return the value of field 'dsFuncionarioGerenteContrato'.
     */
    public java.lang.String getDsFuncionarioGerenteContrato()
    {
        return this._dsFuncionarioGerenteContrato;
    } //-- java.lang.String getDsFuncionarioGerenteContrato() 

    /**
     * Returns the value of field 'dsUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeOrganizacional'.
     */
    public java.lang.String getDsUnidadeOrganizacional()
    {
        return this._dsUnidadeOrganizacional;
    } //-- java.lang.String getDsUnidadeOrganizacional() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'numeroOcorrenciasParticipante'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrenciasParticipante'.
     */
    public int getNumeroOcorrenciasParticipante()
    {
        return this._numeroOcorrenciasParticipante;
    } //-- int getNumeroOcorrenciasParticipante() 

    /**
     * Returns the value of field 'numeroOcorrenciasServico'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrenciasServico'.
     */
    public int getNumeroOcorrenciasServico()
    {
        return this._numeroOcorrenciasServico;
    } //-- int getNumeroOcorrenciasServico() 

    /**
     * Returns the value of field 'numeroOcorrenciasVinculacao'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrenciasVinculacao'.
     */
    public int getNumeroOcorrenciasVinculacao()
    {
        return this._numeroOcorrenciasVinculacao;
    } //-- int getNumeroOcorrenciasVinculacao() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1 getOcorrencias1(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias1: Index value '"+index+"' not in range [0.."+(_ocorrencias1List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1) _ocorrencias1List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1 getOcorrencias1(int) 

    /**
     * Method getOcorrencias1
     * 
     * 
     * 
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1[] getOcorrencias1()
    {
        int size = _ocorrencias1List.size();
        br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1) _ocorrencias1List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1[] getOcorrencias1() 

    /**
     * Method getOcorrencias1Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias1Count()
    {
        return _ocorrencias1List.size();
    } //-- int getOcorrencias1Count() 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2 getOcorrencias2(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias2: Index value '"+index+"' not in range [0.."+(_ocorrencias2List.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2 getOcorrencias2(int) 

    /**
     * Method getOcorrencias2
     * 
     * 
     * 
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2[] getOcorrencias2()
    {
        int size = _ocorrencias2List.size();
        br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2) _ocorrencias2List.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2[] getOcorrencias2() 

    /**
     * Method getOcorrencias2Count
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrencias2Count()
    {
        return _ocorrencias2List.size();
    } //-- int getOcorrencias2Count() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Method hasCdFormaAutorizacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFormaAutorizacaoPagamento()
    {
        return this._has_cdFormaAutorizacaoPagamento;
    } //-- boolean hasCdFormaAutorizacaoPagamento() 

    /**
     * Method hasCdFuncionarioGerenteContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFuncionarioGerenteContrato()
    {
        return this._has_cdFuncionarioGerenteContrato;
    } //-- boolean hasCdFuncionarioGerenteContrato() 

    /**
     * Method hasCdUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdUnidadeOrganizacional()
    {
        return this._has_cdUnidadeOrganizacional;
    } //-- boolean hasCdUnidadeOrganizacional() 

    /**
     * Method hasNumeroOcorrenciasParticipante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrenciasParticipante()
    {
        return this._has_numeroOcorrenciasParticipante;
    } //-- boolean hasNumeroOcorrenciasParticipante() 

    /**
     * Method hasNumeroOcorrenciasServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrenciasServico()
    {
        return this._has_numeroOcorrenciasServico;
    } //-- boolean hasNumeroOcorrenciasServico() 

    /**
     * Method hasNumeroOcorrenciasVinculacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrenciasVinculacao()
    {
        return this._has_numeroOcorrenciasVinculacao;
    } //-- boolean hasNumeroOcorrenciasVinculacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeAllOcorrencias1
     * 
     */
    public void removeAllOcorrencias1()
    {
        _ocorrencias1List.removeAllElements();
    } //-- void removeAllOcorrencias1() 

    /**
     * Method removeAllOcorrencias2
     * 
     */
    public void removeAllOcorrencias2()
    {
        _ocorrencias2List.removeAllElements();
    } //-- void removeAllOcorrencias2() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Method removeOcorrencias1
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias1
     */
    public br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1 removeOcorrencias1(int index)
    {
        java.lang.Object obj = _ocorrencias1List.elementAt(index);
        _ocorrencias1List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1 removeOcorrencias1(int) 

    /**
     * Method removeOcorrencias2
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias2
     */
    public br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2 removeOcorrencias2(int index)
    {
        java.lang.Object obj = _ocorrencias2List.elementAt(index);
        _ocorrencias2List.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2 removeOcorrencias2(int) 

    /**
     * Sets the value of field 'cdFormaAutorizacaoPagamento'.
     * 
     * @param cdFormaAutorizacaoPagamento the value of field
     * 'cdFormaAutorizacaoPagamento'.
     */
    public void setCdFormaAutorizacaoPagamento(int cdFormaAutorizacaoPagamento)
    {
        this._cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
        this._has_cdFormaAutorizacaoPagamento = true;
    } //-- void setCdFormaAutorizacaoPagamento(int) 

    /**
     * Sets the value of field 'cdFuncionarioGerenteContrato'.
     * 
     * @param cdFuncionarioGerenteContrato the value of field
     * 'cdFuncionarioGerenteContrato'.
     */
    public void setCdFuncionarioGerenteContrato(long cdFuncionarioGerenteContrato)
    {
        this._cdFuncionarioGerenteContrato = cdFuncionarioGerenteContrato;
        this._has_cdFuncionarioGerenteContrato = true;
    } //-- void setCdFuncionarioGerenteContrato(long) 

    /**
     * Sets the value of field 'cdUnidadeOrganizacional'.
     * 
     * @param cdUnidadeOrganizacional the value of field
     * 'cdUnidadeOrganizacional'.
     */
    public void setCdUnidadeOrganizacional(int cdUnidadeOrganizacional)
    {
        this._cdUnidadeOrganizacional = cdUnidadeOrganizacional;
        this._has_cdUnidadeOrganizacional = true;
    } //-- void setCdUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaAutorizacaoPagamento'.
     * 
     * @param dsFormaAutorizacaoPagamento the value of field
     * 'dsFormaAutorizacaoPagamento'.
     */
    public void setDsFormaAutorizacaoPagamento(java.lang.String dsFormaAutorizacaoPagamento)
    {
        this._dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    } //-- void setDsFormaAutorizacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsFuncionarioGerenteContrato'.
     * 
     * @param dsFuncionarioGerenteContrato the value of field
     * 'dsFuncionarioGerenteContrato'.
     */
    public void setDsFuncionarioGerenteContrato(java.lang.String dsFuncionarioGerenteContrato)
    {
        this._dsFuncionarioGerenteContrato = dsFuncionarioGerenteContrato;
    } //-- void setDsFuncionarioGerenteContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeOrganizacional'.
     * 
     * @param dsUnidadeOrganizacional the value of field
     * 'dsUnidadeOrganizacional'.
     */
    public void setDsUnidadeOrganizacional(java.lang.String dsUnidadeOrganizacional)
    {
        this._dsUnidadeOrganizacional = dsUnidadeOrganizacional;
    } //-- void setDsUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'numeroOcorrenciasParticipante'.
     * 
     * @param numeroOcorrenciasParticipante the value of field
     * 'numeroOcorrenciasParticipante'.
     */
    public void setNumeroOcorrenciasParticipante(int numeroOcorrenciasParticipante)
    {
        this._numeroOcorrenciasParticipante = numeroOcorrenciasParticipante;
        this._has_numeroOcorrenciasParticipante = true;
    } //-- void setNumeroOcorrenciasParticipante(int) 

    /**
     * Sets the value of field 'numeroOcorrenciasServico'.
     * 
     * @param numeroOcorrenciasServico the value of field
     * 'numeroOcorrenciasServico'.
     */
    public void setNumeroOcorrenciasServico(int numeroOcorrenciasServico)
    {
        this._numeroOcorrenciasServico = numeroOcorrenciasServico;
        this._has_numeroOcorrenciasServico = true;
    } //-- void setNumeroOcorrenciasServico(int) 

    /**
     * Sets the value of field 'numeroOcorrenciasVinculacao'.
     * 
     * @param numeroOcorrenciasVinculacao the value of field
     * 'numeroOcorrenciasVinculacao'.
     */
    public void setNumeroOcorrenciasVinculacao(int numeroOcorrenciasVinculacao)
    {
        this._numeroOcorrenciasVinculacao = numeroOcorrenciasVinculacao;
        this._has_numeroOcorrenciasVinculacao = true;
    } //-- void setNumeroOcorrenciasVinculacao(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias1
     */
    public void setOcorrencias1(int index, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1 vOcorrencias1)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias1List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias1: Index value '"+index+"' not in range [0.." + (_ocorrencias1List.size() - 1) + "]");
        }
        _ocorrencias1List.setElementAt(vOcorrencias1, index);
    } //-- void setOcorrencias1(int, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1) 

    /**
     * Method setOcorrencias1
     * 
     * 
     * 
     * @param ocorrencias1Array
     */
    public void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1[] ocorrencias1Array)
    {
        //-- copy array
        _ocorrencias1List.removeAllElements();
        for (int i = 0; i < ocorrencias1Array.length; i++) {
            _ocorrencias1List.addElement(ocorrencias1Array[i]);
        }
    } //-- void setOcorrencias1(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias1) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias2
     */
    public void setOcorrencias2(int index, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2 vOcorrencias2)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrencias2List.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias2: Index value '"+index+"' not in range [0.." + (_ocorrencias2List.size() - 1) + "]");
        }
        _ocorrencias2List.setElementAt(vOcorrencias2, index);
    } //-- void setOcorrencias2(int, br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2) 

    /**
     * Method setOcorrencias2
     * 
     * 
     * 
     * @param ocorrencias2Array
     */
    public void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2[] ocorrencias2Array)
    {
        //-- copy array
        _ocorrencias2List.removeAllElements();
        for (int i = 0; i < ocorrencias2Array.length; i++) {
            _ocorrencias2List.addElement(ocorrencias2Array[i]);
        }
    } //-- void setOcorrencias2(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.Ocorrencias2) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConRelacaoConManutencoesResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.ConRelacaoConManutencoesResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.ConRelacaoConManutencoesResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.ConRelacaoConManutencoesResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.conrelacaoconmanutencoes.response.ConRelacaoConManutencoesResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
