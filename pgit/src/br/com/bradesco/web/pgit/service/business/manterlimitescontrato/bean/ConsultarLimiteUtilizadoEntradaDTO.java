/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;

import java.util.Date;

/**
 * Nome: ConsultarLimiteUtilizadoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarLimiteUtilizadoEntradaDTO {
//ATRIBUTOS
	/** Atributo dataInicio. */
private Date dataInicio;
	
	/** Atributo dataFim. */
	private Date dataFim;
	
	/** Atributo dataGrid. */
	private Date dataGrid;
	
	/** Atributo qtdePagamentosGrid. */
	private int qtdePagamentosGrid;
	
	/** Atributo valorGrid. */
	private int valorGrid;
//GET/SET
	/**
 * Get: dataFim.
 *
 * @return dataFim
 */
public Date getDataFim() {
		return dataFim;
	}
	
	/**
	 * Set: dataFim.
	 *
	 * @param dataFim the data fim
	 */
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	
	/**
	 * Get: dataGrid.
	 *
	 * @return dataGrid
	 */
	public Date getDataGrid() {
		return dataGrid;
	}
	
	/**
	 * Set: dataGrid.
	 *
	 * @param dataGrid the data grid
	 */
	public void setDataGrid(Date dataGrid) {
		this.dataGrid = dataGrid;
	}
	
	/**
	 * Get: dataInicio.
	 *
	 * @return dataInicio
	 */
	public Date getDataInicio() {
		return dataInicio;
	}
	
	/**
	 * Set: dataInicio.
	 *
	 * @param dataInicio the data inicio
	 */
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	/**
	 * Get: qtdePagamentosGrid.
	 *
	 * @return qtdePagamentosGrid
	 */
	public int getQtdePagamentosGrid() {
		return qtdePagamentosGrid;
	}
	
	/**
	 * Set: qtdePagamentosGrid.
	 *
	 * @param qtdePagamentosGrid the qtde pagamentos grid
	 */
	public void setQtdePagamentosGrid(int qtdePagamentosGrid) {
		this.qtdePagamentosGrid = qtdePagamentosGrid;
	}
	
	/**
	 * Get: valorGrid.
	 *
	 * @return valorGrid
	 */
	public int getValorGrid() {
		return valorGrid;
	}
	
	/**
	 * Set: valorGrid.
	 *
	 * @param valorGrid the valor grid
	 */
	public void setValorGrid(int valorGrid) {
		this.valorGrid = valorGrid;
	}
	
	
}
