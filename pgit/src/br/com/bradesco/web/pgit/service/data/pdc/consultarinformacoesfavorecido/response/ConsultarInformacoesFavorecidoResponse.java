/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarInformacoesFavorecidoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarInformacoesFavorecidoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _nmFavorecido
     */
    private java.lang.String _nmFavorecido;

    /**
     * Field _cdFavorecido
     */
    private long _cdFavorecido = 0;

    /**
     * keeps track of state for field: _cdFavorecido
     */
    private boolean _has_cdFavorecido;

    /**
     * Field _inscricaoFavorecido
     */
    private java.lang.String _inscricaoFavorecido;

    /**
     * Field _dsTipoFavorecido
     */
    private java.lang.String _dsTipoFavorecido;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarInformacoesFavorecidoResponse() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.response.ConsultarInformacoesFavorecidoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFavorecido
     * 
     */
    public void deleteCdFavorecido()
    {
        this._has_cdFavorecido= false;
    } //-- void deleteCdFavorecido() 

    /**
     * Returns the value of field 'cdFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdFavorecido'.
     */
    public long getCdFavorecido()
    {
        return this._cdFavorecido;
    } //-- long getCdFavorecido() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsTipoFavorecido'.
     * 
     * @return String
     * @return the value of field 'dsTipoFavorecido'.
     */
    public java.lang.String getDsTipoFavorecido()
    {
        return this._dsTipoFavorecido;
    } //-- java.lang.String getDsTipoFavorecido() 

    /**
     * Returns the value of field 'inscricaoFavorecido'.
     * 
     * @return String
     * @return the value of field 'inscricaoFavorecido'.
     */
    public java.lang.String getInscricaoFavorecido()
    {
        return this._inscricaoFavorecido;
    } //-- java.lang.String getInscricaoFavorecido() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nmFavorecido'.
     * 
     * @return String
     * @return the value of field 'nmFavorecido'.
     */
    public java.lang.String getNmFavorecido()
    {
        return this._nmFavorecido;
    } //-- java.lang.String getNmFavorecido() 

    /**
     * Method hasCdFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecido()
    {
        return this._has_cdFavorecido;
    } //-- boolean hasCdFavorecido() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFavorecido'.
     * 
     * @param cdFavorecido the value of field 'cdFavorecido'.
     */
    public void setCdFavorecido(long cdFavorecido)
    {
        this._cdFavorecido = cdFavorecido;
        this._has_cdFavorecido = true;
    } //-- void setCdFavorecido(long) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoFavorecido'.
     * 
     * @param dsTipoFavorecido the value of field 'dsTipoFavorecido'
     */
    public void setDsTipoFavorecido(java.lang.String dsTipoFavorecido)
    {
        this._dsTipoFavorecido = dsTipoFavorecido;
    } //-- void setDsTipoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'inscricaoFavorecido'.
     * 
     * @param inscricaoFavorecido the value of field
     * 'inscricaoFavorecido'.
     */
    public void setInscricaoFavorecido(java.lang.String inscricaoFavorecido)
    {
        this._inscricaoFavorecido = inscricaoFavorecido;
    } //-- void setInscricaoFavorecido(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nmFavorecido'.
     * 
     * @param nmFavorecido the value of field 'nmFavorecido'.
     */
    public void setNmFavorecido(java.lang.String nmFavorecido)
    {
        this._nmFavorecido = nmFavorecido;
    } //-- void setNmFavorecido(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarInformacoesFavorecidoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.response.ConsultarInformacoesFavorecidoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.response.ConsultarInformacoesFavorecidoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.response.ConsultarInformacoesFavorecidoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.response.ConsultarInformacoesFavorecidoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
