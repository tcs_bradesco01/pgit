/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarOrdemPagtoAgePagEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarOrdemPagtoAgePagEntradaDTO {
	
    /** Atributo numeroOcorrencias. */
    private Integer numeroOcorrencias; //default 0
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia; //default="0" 
    
    /** Atributo dataInicio. */
    private String dataInicio;
    
    /** Atributo dataFim. */
    private String dataFim;
    
    /** Atributo cdCnpjCpf. */
    private Long cdCnpjCpf; //default="0" />
    
    /** Atributo cdFilialCnpjCpf. */
    private Integer cdFilialCnpjCpf; //default="0" />
    
    /** Atributo cdControleCnpjCpf. */
    private Integer cdControleCnpjCpf; //default="0" />
    
    /** Atributo vlPagamentoClienteInicio. */
    private BigDecimal vlPagamentoClienteInicio; //default="0" />
    
    /** Atributo vlPagamentoClienteFim. */
    private BigDecimal vlPagamentoClienteFim; //default="0" />
    
    /** Atributo cdTipoInscricao. */
    private Integer cdTipoInscricao;
    
    
    
    /** Atributo cdSituacaoPagamento. */
    private Integer cdSituacaoPagamento;
    
    /** Atributo cdTipoRetornoPagamento. */
    private Integer cdTipoRetornoPagamento;    
    
	/**
	 * Get: cdSituacaoPagamento.
	 *
	 * @return cdSituacaoPagamento
	 */
	public Integer getCdSituacaoPagamento() {
		return cdSituacaoPagamento;
	}
	
	/**
	 * Get: cdTipoInscricao.
	 *
	 * @return cdTipoInscricao
	 */
	public Integer getCdTipoInscricao() {
		return cdTipoInscricao;
	}
	
	/**
	 * Set: cdTipoInscricao.
	 *
	 * @param cdTipoInscricao the cd tipo inscricao
	 */
	public void setCdTipoInscricao(Integer cdTipoInscricao) {
		this.cdTipoInscricao = cdTipoInscricao;
	}
	
	/**
	 * Set: cdSituacaoPagamento.
	 *
	 * @param cdSituacaoPagamento the cd situacao pagamento
	 */
	public void setCdSituacaoPagamento(Integer cdSituacaoPagamento) {
		this.cdSituacaoPagamento = cdSituacaoPagamento;
	}
	
	/**
	 * Get: cdTipoRetornoPagamento.
	 *
	 * @return cdTipoRetornoPagamento
	 */
	public Integer getCdTipoRetornoPagamento() {
		return cdTipoRetornoPagamento;
	}
	
	/**
	 * Set: cdTipoRetornoPagamento.
	 *
	 * @param cdTipoRetornoPagamento the cd tipo retorno pagamento
	 */
	public void setCdTipoRetornoPagamento(Integer cdTipoRetornoPagamento) {
		this.cdTipoRetornoPagamento = cdTipoRetornoPagamento;
	}
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdCnpjCpf.
	 *
	 * @return cdCnpjCpf
	 */
	public Long getCdCnpjCpf() {
		return cdCnpjCpf;
	}
	
	/**
	 * Set: cdCnpjCpf.
	 *
	 * @param cdCnpjCpf the cd cnpj cpf
	 */
	public void setCdCnpjCpf(Long cdCnpjCpf) {
		this.cdCnpjCpf = cdCnpjCpf;
	}
	
	/**
	 * Get: cdControleCnpjCpf.
	 *
	 * @return cdControleCnpjCpf
	 */
	public Integer getCdControleCnpjCpf() {
		return cdControleCnpjCpf;
	}
	
	/**
	 * Set: cdControleCnpjCpf.
	 *
	 * @param cdControleCnpjCpf the cd controle cnpj cpf
	 */
	public void setCdControleCnpjCpf(Integer cdControleCnpjCpf) {
		this.cdControleCnpjCpf = cdControleCnpjCpf;
	}
	
	/**
	 * Get: cdFilialCnpjCpf.
	 *
	 * @return cdFilialCnpjCpf
	 */
	public Integer getCdFilialCnpjCpf() {
		return cdFilialCnpjCpf;
	}
	
	/**
	 * Set: cdFilialCnpjCpf.
	 *
	 * @param cdFilialCnpjCpf the cd filial cnpj cpf
	 */
	public void setCdFilialCnpjCpf(Integer cdFilialCnpjCpf) {
		this.cdFilialCnpjCpf = cdFilialCnpjCpf;
	}
	
	/**
	 * Get: dataFim.
	 *
	 * @return dataFim
	 */
	public String getDataFim() {
		return dataFim;
	}
	
	/**
	 * Set: dataFim.
	 *
	 * @param dataFim the data fim
	 */
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	
	/**
	 * Get: dataInicio.
	 *
	 * @return dataInicio
	 */
	public String getDataInicio() {
		return dataInicio;
	}
	
	/**
	 * Set: dataInicio.
	 *
	 * @param dataInicio the data inicio
	 */
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}
	
	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
	
	/**
	 * Get: vlPagamentoClienteFim.
	 *
	 * @return vlPagamentoClienteFim
	 */
	public BigDecimal getVlPagamentoClienteFim() {
		return vlPagamentoClienteFim;
	}
	
	/**
	 * Set: vlPagamentoClienteFim.
	 *
	 * @param vlPagamentoClienteFim the vl pagamento cliente fim
	 */
	public void setVlPagamentoClienteFim(BigDecimal vlPagamentoClienteFim) {
		this.vlPagamentoClienteFim = vlPagamentoClienteFim;
	}
	
	/**
	 * Get: vlPagamentoClienteInicio.
	 *
	 * @return vlPagamentoClienteInicio
	 */
	public BigDecimal getVlPagamentoClienteInicio() {
		return vlPagamentoClienteInicio;
	}
	
	/**
	 * Set: vlPagamentoClienteInicio.
	 *
	 * @param vlPagamentoClienteInicio the vl pagamento cliente inicio
	 */
	public void setVlPagamentoClienteInicio(BigDecimal vlPagamentoClienteInicio) {
		this.vlPagamentoClienteInicio = vlPagamentoClienteInicio;
	}
}