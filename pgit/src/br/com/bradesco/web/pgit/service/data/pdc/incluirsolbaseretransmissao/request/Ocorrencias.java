/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdClienteTransferenciaArquivo
     */
    private long _cdClienteTransferenciaArquivo = 0;

    /**
     * keeps track of state for field: _cdClienteTransferenciaArquiv
     */
    private boolean _has_cdClienteTransferenciaArquivo;

    /**
     * Field _nrSequenciaArquivoRetorno
     */
    private long _nrSequenciaArquivoRetorno = 0;

    /**
     * keeps track of state for field: _nrSequenciaArquivoRetorno
     */
    private boolean _has_nrSequenciaArquivoRetorno;

    /**
     * Field _hrInclusaoArquivoRetorno
     */
    private java.lang.String _hrInclusaoArquivoRetorno;

    /**
     * Field _dsArquivoRetorno
     */
    private java.lang.String _dsArquivoRetorno;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdClienteTransferenciaArquivo
     * 
     */
    public void deleteCdClienteTransferenciaArquivo()
    {
        this._has_cdClienteTransferenciaArquivo= false;
    } //-- void deleteCdClienteTransferenciaArquivo() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrSequenciaArquivoRetorno
     * 
     */
    public void deleteNrSequenciaArquivoRetorno()
    {
        this._has_nrSequenciaArquivoRetorno= false;
    } //-- void deleteNrSequenciaArquivoRetorno() 

    /**
     * Returns the value of field 'cdClienteTransferenciaArquivo'.
     * 
     * @return long
     * @return the value of field 'cdClienteTransferenciaArquivo'.
     */
    public long getCdClienteTransferenciaArquivo()
    {
        return this._cdClienteTransferenciaArquivo;
    } //-- long getCdClienteTransferenciaArquivo() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dsArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsArquivoRetorno'.
     */
    public java.lang.String getDsArquivoRetorno()
    {
        return this._dsArquivoRetorno;
    } //-- java.lang.String getDsArquivoRetorno() 

    /**
     * Returns the value of field 'hrInclusaoArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoArquivoRetorno'.
     */
    public java.lang.String getHrInclusaoArquivoRetorno()
    {
        return this._hrInclusaoArquivoRetorno;
    } //-- java.lang.String getHrInclusaoArquivoRetorno() 

    /**
     * Returns the value of field 'nrSequenciaArquivoRetorno'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaArquivoRetorno'.
     */
    public long getNrSequenciaArquivoRetorno()
    {
        return this._nrSequenciaArquivoRetorno;
    } //-- long getNrSequenciaArquivoRetorno() 

    /**
     * Method hasCdClienteTransferenciaArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClienteTransferenciaArquivo()
    {
        return this._has_cdClienteTransferenciaArquivo;
    } //-- boolean hasCdClienteTransferenciaArquivo() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrSequenciaArquivoRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaArquivoRetorno()
    {
        return this._has_nrSequenciaArquivoRetorno;
    } //-- boolean hasNrSequenciaArquivoRetorno() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdClienteTransferenciaArquivo'.
     * 
     * @param cdClienteTransferenciaArquivo the value of field
     * 'cdClienteTransferenciaArquivo'.
     */
    public void setCdClienteTransferenciaArquivo(long cdClienteTransferenciaArquivo)
    {
        this._cdClienteTransferenciaArquivo = cdClienteTransferenciaArquivo;
        this._has_cdClienteTransferenciaArquivo = true;
    } //-- void setCdClienteTransferenciaArquivo(long) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'dsArquivoRetorno'.
     * 
     * @param dsArquivoRetorno the value of field 'dsArquivoRetorno'
     */
    public void setDsArquivoRetorno(java.lang.String dsArquivoRetorno)
    {
        this._dsArquivoRetorno = dsArquivoRetorno;
    } //-- void setDsArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoArquivoRetorno'.
     * 
     * @param hrInclusaoArquivoRetorno the value of field
     * 'hrInclusaoArquivoRetorno'.
     */
    public void setHrInclusaoArquivoRetorno(java.lang.String hrInclusaoArquivoRetorno)
    {
        this._hrInclusaoArquivoRetorno = hrInclusaoArquivoRetorno;
    } //-- void setHrInclusaoArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaArquivoRetorno'.
     * 
     * @param nrSequenciaArquivoRetorno the value of field
     * 'nrSequenciaArquivoRetorno'.
     */
    public void setNrSequenciaArquivoRetorno(long nrSequenciaArquivoRetorno)
    {
        this._nrSequenciaArquivoRetorno = nrSequenciaArquivoRetorno;
        this._has_nrSequenciaArquivoRetorno = true;
    } //-- void setNrSequenciaArquivoRetorno(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolbaseretransmissao.request.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
