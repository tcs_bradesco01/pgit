/*
 * =========================================================================
 * 
 * Client:       Bradesco (BR)
 * Project:      Arquitetura Bradesco Canal Internet
 * Development:  GFT Iberia (http://www.gft.com)
 * -------------------------------------------------------------------------
 * Revision - Last:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/constants.ftl,v $
 * $Id: constants.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revision - History:
 * $Log: constants.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantercontrato;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface de constantes do adaptador: ManterContrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterContratoServiceConstants {

	/** Atributo INC_SERV_MANT_CONTR_MAX_MODALIDADE. */
	int INC_SERV_MANT_CONTR_MAX_MODALIDADE = 10;

	/** Atributo OCORRENCIAS_LISTAR_HISTORICO_CONTAS. */
	int OCORRENCIAS_LISTAR_HISTORICO_CONTAS = 70;

	/** Atributo OCORRENCIAS_LISTAR_HISTORICO_LAYOYT. */
	int OCORRENCIAS_LISTAR_HISTORICO_LAYOYT = 100;

	/** Atributo OCORRENCIAS_LISTAR_PARTICIPANTES_CONTRATO. */
	int OCORRENCIAS_LISTAR_PARTICIPANTES_CONTRATO = 50;

	/** Atributo TIPO_PARTICIPACAO_PESSOA. */
	int TIPO_PARTICIPACAO_PESSOA = 7;

	/** Atributo PARAMETRO_TELA. */
	Integer PARAMETRO_TELA = 19;
	
	/** Atributo SEM_CONSULTA. */
	Long SEM_CONSULTA = 4L;
	
	/** Atributo DISPONIBILIZACAO_PAGAMENTO. */
	Long DISPONIBILIZACAO_PAGAMENTO = 1L;
	
	/** Atributo COD_IND_MOMENTO_DEBITO_PGTO_S. */
	String COD_IND_MOMENTO_DEBITO_PGTO_S = "S";  
}