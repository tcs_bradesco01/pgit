/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartarifascontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdServico
     */
    private int _cdServico = 0;

    /**
     * keeps track of state for field: _cdServico
     */
    private boolean _has_cdServico;

    /**
     * Field _dsServico
     */
    private java.lang.String _dsServico;

    /**
     * Field _cdModalidade
     */
    private int _cdModalidade = 0;

    /**
     * keeps track of state for field: _cdModalidade
     */
    private boolean _has_cdModalidade;

    /**
     * Field _dsModalidade
     */
    private java.lang.String _dsModalidade;

    /**
     * Field _cdRelacionamentoProduto
     */
    private int _cdRelacionamentoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionamentoProduto
     */
    private boolean _has_cdRelacionamentoProduto;

    /**
     * Field _dtInclusaoServico
     */
    private java.lang.String _dtInclusaoServico;

    /**
     * Field _cdSituacaoServicoRelacionado
     */
    private int _cdSituacaoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdSituacaoServicoRelacionado
     */
    private boolean _has_cdSituacaoServicoRelacionado;

    /**
     * Field _dtSituacaoServicoRelacionado
     */
    private java.lang.String _dtSituacaoServicoRelacionado;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartarifascontrato.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdModalidade
     * 
     */
    public void deleteCdModalidade()
    {
        this._has_cdModalidade= false;
    } //-- void deleteCdModalidade() 

    /**
     * Method deleteCdRelacionamentoProduto
     * 
     */
    public void deleteCdRelacionamentoProduto()
    {
        this._has_cdRelacionamentoProduto= false;
    } //-- void deleteCdRelacionamentoProduto() 

    /**
     * Method deleteCdServico
     * 
     */
    public void deleteCdServico()
    {
        this._has_cdServico= false;
    } //-- void deleteCdServico() 

    /**
     * Method deleteCdSituacaoServicoRelacionado
     * 
     */
    public void deleteCdSituacaoServicoRelacionado()
    {
        this._has_cdSituacaoServicoRelacionado= false;
    } //-- void deleteCdSituacaoServicoRelacionado() 

    /**
     * Returns the value of field 'cdModalidade'.
     * 
     * @return int
     * @return the value of field 'cdModalidade'.
     */
    public int getCdModalidade()
    {
        return this._cdModalidade;
    } //-- int getCdModalidade() 

    /**
     * Returns the value of field 'cdRelacionamentoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionamentoProduto'.
     */
    public int getCdRelacionamentoProduto()
    {
        return this._cdRelacionamentoProduto;
    } //-- int getCdRelacionamentoProduto() 

    /**
     * Returns the value of field 'cdServico'.
     * 
     * @return int
     * @return the value of field 'cdServico'.
     */
    public int getCdServico()
    {
        return this._cdServico;
    } //-- int getCdServico() 

    /**
     * Returns the value of field 'cdSituacaoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoServicoRelacionado'.
     */
    public int getCdSituacaoServicoRelacionado()
    {
        return this._cdSituacaoServicoRelacionado;
    } //-- int getCdSituacaoServicoRelacionado() 

    /**
     * Returns the value of field 'dsModalidade'.
     * 
     * @return String
     * @return the value of field 'dsModalidade'.
     */
    public java.lang.String getDsModalidade()
    {
        return this._dsModalidade;
    } //-- java.lang.String getDsModalidade() 

    /**
     * Returns the value of field 'dsServico'.
     * 
     * @return String
     * @return the value of field 'dsServico'.
     */
    public java.lang.String getDsServico()
    {
        return this._dsServico;
    } //-- java.lang.String getDsServico() 

    /**
     * Returns the value of field 'dtInclusaoServico'.
     * 
     * @return String
     * @return the value of field 'dtInclusaoServico'.
     */
    public java.lang.String getDtInclusaoServico()
    {
        return this._dtInclusaoServico;
    } //-- java.lang.String getDtInclusaoServico() 

    /**
     * Returns the value of field 'dtSituacaoServicoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dtSituacaoServicoRelacionado'.
     */
    public java.lang.String getDtSituacaoServicoRelacionado()
    {
        return this._dtSituacaoServicoRelacionado;
    } //-- java.lang.String getDtSituacaoServicoRelacionado() 

    /**
     * Method hasCdModalidade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdModalidade()
    {
        return this._has_cdModalidade;
    } //-- boolean hasCdModalidade() 

    /**
     * Method hasCdRelacionamentoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionamentoProduto()
    {
        return this._has_cdRelacionamentoProduto;
    } //-- boolean hasCdRelacionamentoProduto() 

    /**
     * Method hasCdServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdServico()
    {
        return this._has_cdServico;
    } //-- boolean hasCdServico() 

    /**
     * Method hasCdSituacaoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoServicoRelacionado()
    {
        return this._has_cdSituacaoServicoRelacionado;
    } //-- boolean hasCdSituacaoServicoRelacionado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdModalidade'.
     * 
     * @param cdModalidade the value of field 'cdModalidade'.
     */
    public void setCdModalidade(int cdModalidade)
    {
        this._cdModalidade = cdModalidade;
        this._has_cdModalidade = true;
    } //-- void setCdModalidade(int) 

    /**
     * Sets the value of field 'cdRelacionamentoProduto'.
     * 
     * @param cdRelacionamentoProduto the value of field
     * 'cdRelacionamentoProduto'.
     */
    public void setCdRelacionamentoProduto(int cdRelacionamentoProduto)
    {
        this._cdRelacionamentoProduto = cdRelacionamentoProduto;
        this._has_cdRelacionamentoProduto = true;
    } //-- void setCdRelacionamentoProduto(int) 

    /**
     * Sets the value of field 'cdServico'.
     * 
     * @param cdServico the value of field 'cdServico'.
     */
    public void setCdServico(int cdServico)
    {
        this._cdServico = cdServico;
        this._has_cdServico = true;
    } //-- void setCdServico(int) 

    /**
     * Sets the value of field 'cdSituacaoServicoRelacionado'.
     * 
     * @param cdSituacaoServicoRelacionado the value of field
     * 'cdSituacaoServicoRelacionado'.
     */
    public void setCdSituacaoServicoRelacionado(int cdSituacaoServicoRelacionado)
    {
        this._cdSituacaoServicoRelacionado = cdSituacaoServicoRelacionado;
        this._has_cdSituacaoServicoRelacionado = true;
    } //-- void setCdSituacaoServicoRelacionado(int) 

    /**
     * Sets the value of field 'dsModalidade'.
     * 
     * @param dsModalidade the value of field 'dsModalidade'.
     */
    public void setDsModalidade(java.lang.String dsModalidade)
    {
        this._dsModalidade = dsModalidade;
    } //-- void setDsModalidade(java.lang.String) 

    /**
     * Sets the value of field 'dsServico'.
     * 
     * @param dsServico the value of field 'dsServico'.
     */
    public void setDsServico(java.lang.String dsServico)
    {
        this._dsServico = dsServico;
    } //-- void setDsServico(java.lang.String) 

    /**
     * Sets the value of field 'dtInclusaoServico'.
     * 
     * @param dtInclusaoServico the value of field
     * 'dtInclusaoServico'.
     */
    public void setDtInclusaoServico(java.lang.String dtInclusaoServico)
    {
        this._dtInclusaoServico = dtInclusaoServico;
    } //-- void setDtInclusaoServico(java.lang.String) 

    /**
     * Sets the value of field 'dtSituacaoServicoRelacionado'.
     * 
     * @param dtSituacaoServicoRelacionado the value of field
     * 'dtSituacaoServicoRelacionado'.
     */
    public void setDtSituacaoServicoRelacionado(java.lang.String dtSituacaoServicoRelacionado)
    {
        this._dtSituacaoServicoRelacionado = dtSituacaoServicoRelacionado;
    } //-- void setDtSituacaoServicoRelacionado(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartarifascontrato.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartarifascontrato.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartarifascontrato.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartarifascontrato.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
