/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarConfContaContratoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarConfContaContratoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdPessoaJuridicaVinculo
     */
    private long _cdPessoaJuridicaVinculo = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaVinculo
     */
    private boolean _has_cdPessoaJuridicaVinculo;

    /**
     * Field _cdTipoContratoNegocioVinculo
     */
    private int _cdTipoContratoNegocioVinculo = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocioVinculo
     */
    private boolean _has_cdTipoContratoNegocioVinculo;

    /**
     * Field _nrSequenciaContratoVinculo
     */
    private long _nrSequenciaContratoVinculo = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoVinculo
     */
    private boolean _has_nrSequenciaContratoVinculo;

    /**
     * Field _cdTipoVinculoContrato
     */
    private int _cdTipoVinculoContrato = 0;

    /**
     * keeps track of state for field: _cdTipoVinculoContrato
     */
    private boolean _has_cdTipoVinculoContrato;

    /**
     * Field _cdSituacaoVinculacaoConta
     */
    private int _cdSituacaoVinculacaoConta = 0;

    /**
     * keeps track of state for field: _cdSituacaoVinculacaoConta
     */
    private boolean _has_cdSituacaoVinculacaoConta;

    /**
     * Field _cdMotivoSituacaoConta
     */
    private int _cdMotivoSituacaoConta = 0;

    /**
     * keeps track of state for field: _cdMotivoSituacaoConta
     */
    private boolean _has_cdMotivoSituacaoConta;

    /**
     * Field _cdMunicipio
     */
    private long _cdMunicipio = 0;

    /**
     * keeps track of state for field: _cdMunicipio
     */
    private boolean _has_cdMunicipio;

    /**
     * Field _cdMunicipioFeriadoLocal
     */
    private int _cdMunicipioFeriadoLocal = 0;

    /**
     * keeps track of state for field: _cdMunicipioFeriadoLocal
     */
    private boolean _has_cdMunicipioFeriadoLocal;

    /**
     * Field _descApelido
     */
    private java.lang.String _descApelido;

    /**
     * Field _numOcorrenciasEntrada
     */
    private int _numOcorrenciasEntrada = 0;

    /**
     * keeps track of state for field: _numOcorrenciasEntrada
     */
    private boolean _has_numOcorrenciasEntrada;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarConfContaContratoRequest() 
     {
        super();
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.AlterarConfContaContratoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias) 

    /**
     * Method deleteCdMotivoSituacaoConta
     * 
     */
    public void deleteCdMotivoSituacaoConta()
    {
        this._has_cdMotivoSituacaoConta= false;
    } //-- void deleteCdMotivoSituacaoConta() 

    /**
     * Method deleteCdMunicipio
     * 
     */
    public void deleteCdMunicipio()
    {
        this._has_cdMunicipio= false;
    } //-- void deleteCdMunicipio() 

    /**
     * Method deleteCdMunicipioFeriadoLocal
     * 
     */
    public void deleteCdMunicipioFeriadoLocal()
    {
        this._has_cdMunicipioFeriadoLocal= false;
    } //-- void deleteCdMunicipioFeriadoLocal() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdPessoaJuridicaVinculo
     * 
     */
    public void deleteCdPessoaJuridicaVinculo()
    {
        this._has_cdPessoaJuridicaVinculo= false;
    } //-- void deleteCdPessoaJuridicaVinculo() 

    /**
     * Method deleteCdSituacaoVinculacaoConta
     * 
     */
    public void deleteCdSituacaoVinculacaoConta()
    {
        this._has_cdSituacaoVinculacaoConta= false;
    } //-- void deleteCdSituacaoVinculacaoConta() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoContratoNegocioVinculo
     * 
     */
    public void deleteCdTipoContratoNegocioVinculo()
    {
        this._has_cdTipoContratoNegocioVinculo= false;
    } //-- void deleteCdTipoContratoNegocioVinculo() 

    /**
     * Method deleteCdTipoVinculoContrato
     * 
     */
    public void deleteCdTipoVinculoContrato()
    {
        this._has_cdTipoVinculoContrato= false;
    } //-- void deleteCdTipoVinculoContrato() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Method deleteNrSequenciaContratoVinculo
     * 
     */
    public void deleteNrSequenciaContratoVinculo()
    {
        this._has_nrSequenciaContratoVinculo= false;
    } //-- void deleteNrSequenciaContratoVinculo() 

    /**
     * Method deleteNumOcorrenciasEntrada
     * 
     */
    public void deleteNumOcorrenciasEntrada()
    {
        this._has_numOcorrenciasEntrada= false;
    } //-- void deleteNumOcorrenciasEntrada() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'cdMotivoSituacaoConta'.
     * 
     * @return int
     * @return the value of field 'cdMotivoSituacaoConta'.
     */
    public int getCdMotivoSituacaoConta()
    {
        return this._cdMotivoSituacaoConta;
    } //-- int getCdMotivoSituacaoConta() 

    /**
     * Returns the value of field 'cdMunicipio'.
     * 
     * @return long
     * @return the value of field 'cdMunicipio'.
     */
    public long getCdMunicipio()
    {
        return this._cdMunicipio;
    } //-- long getCdMunicipio() 

    /**
     * Returns the value of field 'cdMunicipioFeriadoLocal'.
     * 
     * @return int
     * @return the value of field 'cdMunicipioFeriadoLocal'.
     */
    public int getCdMunicipioFeriadoLocal()
    {
        return this._cdMunicipioFeriadoLocal;
    } //-- int getCdMunicipioFeriadoLocal() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaVinculo'.
     */
    public long getCdPessoaJuridicaVinculo()
    {
        return this._cdPessoaJuridicaVinculo;
    } //-- long getCdPessoaJuridicaVinculo() 

    /**
     * Returns the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoVinculacaoConta'.
     */
    public int getCdSituacaoVinculacaoConta()
    {
        return this._cdSituacaoVinculacaoConta;
    } //-- int getCdSituacaoVinculacaoConta() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoContratoNegocioVinculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocioVinculo'.
     */
    public int getCdTipoContratoNegocioVinculo()
    {
        return this._cdTipoContratoNegocioVinculo;
    } //-- int getCdTipoContratoNegocioVinculo() 

    /**
     * Returns the value of field 'cdTipoVinculoContrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculoContrato'.
     */
    public int getCdTipoVinculoContrato()
    {
        return this._cdTipoVinculoContrato;
    } //-- int getCdTipoVinculoContrato() 

    /**
     * Returns the value of field 'descApelido'.
     * 
     * @return String
     * @return the value of field 'descApelido'.
     */
    public java.lang.String getDescApelido()
    {
        return this._descApelido;
    } //-- java.lang.String getDescApelido() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoVinculo'.
     */
    public long getNrSequenciaContratoVinculo()
    {
        return this._nrSequenciaContratoVinculo;
    } //-- long getNrSequenciaContratoVinculo() 

    /**
     * Returns the value of field 'numOcorrenciasEntrada'.
     * 
     * @return int
     * @return the value of field 'numOcorrenciasEntrada'.
     */
    public int getNumOcorrenciasEntrada()
    {
        return this._numOcorrenciasEntrada;
    } //-- int getNumOcorrenciasEntrada() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Method hasCdMotivoSituacaoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMotivoSituacaoConta()
    {
        return this._has_cdMotivoSituacaoConta;
    } //-- boolean hasCdMotivoSituacaoConta() 

    /**
     * Method hasCdMunicipio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMunicipio()
    {
        return this._has_cdMunicipio;
    } //-- boolean hasCdMunicipio() 

    /**
     * Method hasCdMunicipioFeriadoLocal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMunicipioFeriadoLocal()
    {
        return this._has_cdMunicipioFeriadoLocal;
    } //-- boolean hasCdMunicipioFeriadoLocal() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdPessoaJuridicaVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaVinculo()
    {
        return this._has_cdPessoaJuridicaVinculo;
    } //-- boolean hasCdPessoaJuridicaVinculo() 

    /**
     * Method hasCdSituacaoVinculacaoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoVinculacaoConta()
    {
        return this._has_cdSituacaoVinculacaoConta;
    } //-- boolean hasCdSituacaoVinculacaoConta() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoContratoNegocioVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocioVinculo()
    {
        return this._has_cdTipoContratoNegocioVinculo;
    } //-- boolean hasCdTipoContratoNegocioVinculo() 

    /**
     * Method hasCdTipoVinculoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculoContrato()
    {
        return this._has_cdTipoVinculoContrato;
    } //-- boolean hasCdTipoVinculoContrato() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method hasNrSequenciaContratoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoVinculo()
    {
        return this._has_nrSequenciaContratoVinculo;
    } //-- boolean hasNrSequenciaContratoVinculo() 

    /**
     * Method hasNumOcorrenciasEntrada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumOcorrenciasEntrada()
    {
        return this._has_numOcorrenciasEntrada;
    } //-- boolean hasNumOcorrenciasEntrada() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'cdMotivoSituacaoConta'.
     * 
     * @param cdMotivoSituacaoConta the value of field
     * 'cdMotivoSituacaoConta'.
     */
    public void setCdMotivoSituacaoConta(int cdMotivoSituacaoConta)
    {
        this._cdMotivoSituacaoConta = cdMotivoSituacaoConta;
        this._has_cdMotivoSituacaoConta = true;
    } //-- void setCdMotivoSituacaoConta(int) 

    /**
     * Sets the value of field 'cdMunicipio'.
     * 
     * @param cdMunicipio the value of field 'cdMunicipio'.
     */
    public void setCdMunicipio(long cdMunicipio)
    {
        this._cdMunicipio = cdMunicipio;
        this._has_cdMunicipio = true;
    } //-- void setCdMunicipio(long) 

    /**
     * Sets the value of field 'cdMunicipioFeriadoLocal'.
     * 
     * @param cdMunicipioFeriadoLocal the value of field
     * 'cdMunicipioFeriadoLocal'.
     */
    public void setCdMunicipioFeriadoLocal(int cdMunicipioFeriadoLocal)
    {
        this._cdMunicipioFeriadoLocal = cdMunicipioFeriadoLocal;
        this._has_cdMunicipioFeriadoLocal = true;
    } //-- void setCdMunicipioFeriadoLocal(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaVinculo'.
     * 
     * @param cdPessoaJuridicaVinculo the value of field
     * 'cdPessoaJuridicaVinculo'.
     */
    public void setCdPessoaJuridicaVinculo(long cdPessoaJuridicaVinculo)
    {
        this._cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
        this._has_cdPessoaJuridicaVinculo = true;
    } //-- void setCdPessoaJuridicaVinculo(long) 

    /**
     * Sets the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @param cdSituacaoVinculacaoConta the value of field
     * 'cdSituacaoVinculacaoConta'.
     */
    public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta)
    {
        this._cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
        this._has_cdSituacaoVinculacaoConta = true;
    } //-- void setCdSituacaoVinculacaoConta(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocioVinculo'.
     * 
     * @param cdTipoContratoNegocioVinculo the value of field
     * 'cdTipoContratoNegocioVinculo'.
     */
    public void setCdTipoContratoNegocioVinculo(int cdTipoContratoNegocioVinculo)
    {
        this._cdTipoContratoNegocioVinculo = cdTipoContratoNegocioVinculo;
        this._has_cdTipoContratoNegocioVinculo = true;
    } //-- void setCdTipoContratoNegocioVinculo(int) 

    /**
     * Sets the value of field 'cdTipoVinculoContrato'.
     * 
     * @param cdTipoVinculoContrato the value of field
     * 'cdTipoVinculoContrato'.
     */
    public void setCdTipoVinculoContrato(int cdTipoVinculoContrato)
    {
        this._cdTipoVinculoContrato = cdTipoVinculoContrato;
        this._has_cdTipoVinculoContrato = true;
    } //-- void setCdTipoVinculoContrato(int) 

    /**
     * Sets the value of field 'descApelido'.
     * 
     * @param descApelido the value of field 'descApelido'.
     */
    public void setDescApelido(java.lang.String descApelido)
    {
        this._descApelido = descApelido;
    } //-- void setDescApelido(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoVinculo'.
     * 
     * @param nrSequenciaContratoVinculo the value of field
     * 'nrSequenciaContratoVinculo'.
     */
    public void setNrSequenciaContratoVinculo(long nrSequenciaContratoVinculo)
    {
        this._nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
        this._has_nrSequenciaContratoVinculo = true;
    } //-- void setNrSequenciaContratoVinculo(long) 

    /**
     * Sets the value of field 'numOcorrenciasEntrada'.
     * 
     * @param numOcorrenciasEntrada the value of field
     * 'numOcorrenciasEntrada'.
     */
    public void setNumOcorrenciasEntrada(int numOcorrenciasEntrada)
    {
        this._numOcorrenciasEntrada = numOcorrenciasEntrada;
        this._has_numOcorrenciasEntrada = true;
    } //-- void setNumOcorrenciasEntrada(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.Ocorrencias) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarConfContaContratoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.AlterarConfContaContratoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.AlterarConfContaContratoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.AlterarConfContaContratoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarconfcontacontrato.request.AlterarConfContaContratoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
