/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarHistoricoOrgaoPagadorRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarHistoricoOrgaoPagadorRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdOrgaoPagador
     */
    private int _cdOrgaoPagador = 0;

    /**
     * keeps track of state for field: _cdOrgaoPagador
     */
    private boolean _has_cdOrgaoPagador;

    /**
     * Field _dtDe
     */
    private java.lang.String _dtDe;

    /**
     * Field _dtAte
     */
    private java.lang.String _dtAte;

    /**
     * Field _qtConsultas
     */
    private int _qtConsultas = 0;

    /**
     * keeps track of state for field: _qtConsultas
     */
    private boolean _has_qtConsultas;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarHistoricoOrgaoPagadorRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.request.ListarHistoricoOrgaoPagadorRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdOrgaoPagador
     * 
     */
    public void deleteCdOrgaoPagador()
    {
        this._has_cdOrgaoPagador= false;
    } //-- void deleteCdOrgaoPagador() 

    /**
     * Method deleteQtConsultas
     * 
     */
    public void deleteQtConsultas()
    {
        this._has_qtConsultas= false;
    } //-- void deleteQtConsultas() 

    /**
     * Returns the value of field 'cdOrgaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdOrgaoPagador'.
     */
    public int getCdOrgaoPagador()
    {
        return this._cdOrgaoPagador;
    } //-- int getCdOrgaoPagador() 

    /**
     * Returns the value of field 'dtAte'.
     * 
     * @return String
     * @return the value of field 'dtAte'.
     */
    public java.lang.String getDtAte()
    {
        return this._dtAte;
    } //-- java.lang.String getDtAte() 

    /**
     * Returns the value of field 'dtDe'.
     * 
     * @return String
     * @return the value of field 'dtDe'.
     */
    public java.lang.String getDtDe()
    {
        return this._dtDe;
    } //-- java.lang.String getDtDe() 

    /**
     * Returns the value of field 'qtConsultas'.
     * 
     * @return int
     * @return the value of field 'qtConsultas'.
     */
    public int getQtConsultas()
    {
        return this._qtConsultas;
    } //-- int getQtConsultas() 

    /**
     * Method hasCdOrgaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOrgaoPagador()
    {
        return this._has_cdOrgaoPagador;
    } //-- boolean hasCdOrgaoPagador() 

    /**
     * Method hasQtConsultas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtConsultas()
    {
        return this._has_qtConsultas;
    } //-- boolean hasQtConsultas() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOrgaoPagador'.
     * 
     * @param cdOrgaoPagador the value of field 'cdOrgaoPagador'.
     */
    public void setCdOrgaoPagador(int cdOrgaoPagador)
    {
        this._cdOrgaoPagador = cdOrgaoPagador;
        this._has_cdOrgaoPagador = true;
    } //-- void setCdOrgaoPagador(int) 

    /**
     * Sets the value of field 'dtAte'.
     * 
     * @param dtAte the value of field 'dtAte'.
     */
    public void setDtAte(java.lang.String dtAte)
    {
        this._dtAte = dtAte;
    } //-- void setDtAte(java.lang.String) 

    /**
     * Sets the value of field 'dtDe'.
     * 
     * @param dtDe the value of field 'dtDe'.
     */
    public void setDtDe(java.lang.String dtDe)
    {
        this._dtDe = dtDe;
    } //-- void setDtDe(java.lang.String) 

    /**
     * Sets the value of field 'qtConsultas'.
     * 
     * @param qtConsultas the value of field 'qtConsultas'.
     */
    public void setQtConsultas(int qtConsultas)
    {
        this._qtConsultas = qtConsultas;
        this._has_qtConsultas = true;
    } //-- void setQtConsultas(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarHistoricoOrgaoPagadorRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.request.ListarHistoricoOrgaoPagadorRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.request.ListarHistoricoOrgaoPagadorRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.request.ListarHistoricoOrgaoPagadorRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarhistoricoorgaopagador.request.ListarHistoricoOrgaoPagadorRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
