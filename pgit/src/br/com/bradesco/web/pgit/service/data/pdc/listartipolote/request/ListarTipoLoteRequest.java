/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipolote.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarTipoLoteRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarTipoLoteRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _cdSituacaoVinculacaoConta
     */
    private int _cdSituacaoVinculacaoConta = 0;

    /**
     * keeps track of state for field: _cdSituacaoVinculacaoConta
     */
    private boolean _has_cdSituacaoVinculacaoConta;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarTipoLoteRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipolote.request.ListarTipoLoteRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoVinculacaoConta
     * 
     */
    public void deleteCdSituacaoVinculacaoConta()
    {
        this._has_cdSituacaoVinculacaoConta= false;
    } //-- void deleteCdSituacaoVinculacaoConta() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoVinculacaoConta'.
     */
    public int getCdSituacaoVinculacaoConta()
    {
        return this._cdSituacaoVinculacaoConta;
    } //-- int getCdSituacaoVinculacaoConta() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasCdSituacaoVinculacaoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoVinculacaoConta()
    {
        return this._has_cdSituacaoVinculacaoConta;
    } //-- boolean hasCdSituacaoVinculacaoConta() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoVinculacaoConta'.
     * 
     * @param cdSituacaoVinculacaoConta the value of field
     * 'cdSituacaoVinculacaoConta'.
     */
    public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta)
    {
        this._cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
        this._has_cdSituacaoVinculacaoConta = true;
    } //-- void setCdSituacaoVinculacaoConta(int) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarTipoLoteRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipolote.request.ListarTipoLoteRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipolote.request.ListarTipoLoteRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipolote.request.ListarTipoLoteRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipolote.request.ListarTipoLoteRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
