/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

import java.util.List;

/**
 * Nome: AnexoComprovanteSalarial
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AnexoComprovanteSalarial {

    /** Atributo dadosEmpresaParticipantes. */
    private List<EmpresaParticipante> dadosEmpresaParticipantes = null;
    
    /** Atributo midiaAutoAtendimento. */
    private String midiaAutoAtendimento = null;

    /** Atributo midiaInternet. */
    private String midiaInternet = null;

    /** Atributo midiaAutoAtendimentoInternet. */
    private String midiaAutoAtendimentoInternet = null;

    /** Atributo qtdeComprovantesEmissao. */
    private String qtdeComprovantesEmissao = null;

    /** Atributo qtdeEmissoesPaga. */
    private String qtdeEmissoesPaga = null;
    
    /** The nr quantidade limite cobranca impressao. */
    private String nrQuantidadeLimiteCobrancaImpressao = null;  
    
    /** The hr limite apresenta carta. */
    private String hrLimiteApresentaCarta = null;    

    /** The ds frase padrao caput. */
    private String dsFrasePadraoCaput = null;
    
    /** The ds Disponibilizacao Comprovante Banco. */
    private String dsDisponibilizacaoComprovanteBanco = null;

    /**
     * Instancia um novo AnexoComprovanteSalarial
     *
     * @see
     */
    public AnexoComprovanteSalarial() {
        super();
    }

    /**
     * Nome: getDadosEmpresaParticipantes.
     *
     * @return dadosEmpresaParticipantes
     */
    public List<EmpresaParticipante> getDadosEmpresaParticipantes() {
        return dadosEmpresaParticipantes;
    }

    /**
     * Nome: setDadosEmpresaParticipantes.
     *
     * @param dadosEmpresaParticipantes the dados empresa participantes
     */
    public void setDadosEmpresaParticipantes(List<EmpresaParticipante> dadosEmpresaParticipantes) {
        this.dadosEmpresaParticipantes = dadosEmpresaParticipantes;
    }

    /**
     * Nome: getMidiaAutoAtendimento.
     *
     * @return midiaAutoAtendimento
     */
    public String getMidiaAutoAtendimento() {
        return midiaAutoAtendimento;
    }

    /**
     * Nome: setMidiaAutoAtendimento.
     *
     * @param midiaAutoAtendimento the midia auto atendimento
     */
    public void setMidiaAutoAtendimento(String midiaAutoAtendimento) {
        this.midiaAutoAtendimento = midiaAutoAtendimento;
    }

    /**
     * Nome: getMidiaInternet.
     *
     * @return midiaInternet
     */
    public String getMidiaInternet() {
        return midiaInternet;
    }

    /**
     * Nome: setMidiaInternet.
     *
     * @param midiaInternet the midia internet
     */
    public void setMidiaInternet(String midiaInternet) {
        this.midiaInternet = midiaInternet;
    }

    /**
     * Nome: getMidiaAutoAtendimentoInternet.
     *
     * @return midiaAutoAtendimentoInternet
     */
    public String getMidiaAutoAtendimentoInternet() {
        return midiaAutoAtendimentoInternet;
    }

    /**
     * Nome: setMidiaAutoAtendimentoInternet.
     *
     * @param midiaAutoAtendimentoInternet the midia auto atendimento internet
     */
    public void setMidiaAutoAtendimentoInternet(String midiaAutoAtendimentoInternet) {
        this.midiaAutoAtendimentoInternet = midiaAutoAtendimentoInternet;
    }

    /**
     * Nome: getQtdeComprovantesEmissao.
     *
     * @return qtdeComprovantesEmissao
     */
    public String getQtdeComprovantesEmissao() {
        return qtdeComprovantesEmissao;
    }

    /**
     * Nome: setQtdeComprovantesEmissao.
     *
     * @param qtdeComprovantesEmissao the qtde comprovantes emissao
     */
    public void setQtdeComprovantesEmissao(String qtdeComprovantesEmissao) {
        this.qtdeComprovantesEmissao = qtdeComprovantesEmissao;
    }

    /**
     * Nome: getQtdeEmissoesPaga.
     *
     * @return qtdeEmissoesPaga
     */
    public String getQtdeEmissoesPaga() {
        return qtdeEmissoesPaga;
    }

    /**
     * Nome: setQtdeEmissoesPaga.
     *
     * @param qtdeEmissoesPaga the qtde emissoes paga
     */
    public void setQtdeEmissoesPaga(String qtdeEmissoesPaga) {
        this.qtdeEmissoesPaga = qtdeEmissoesPaga;
    }

	/**
	 * Gets the nr quantidade limite cobranca impressao.
	 *
	 * @return the nr quantidade limite cobranca impressao
	 */
	public String getNrQuantidadeLimiteCobrancaImpressao() {
		return nrQuantidadeLimiteCobrancaImpressao;
	}

	/**
	 * Sets the nr quantidade limite cobranca impressao.
	 *
	 * @param nrQuantidadeLimiteCobrancaImpressao the new nr quantidade limite cobranca impressao
	 */
	public void setNrQuantidadeLimiteCobrancaImpressao(
			String nrQuantidadeLimiteCobrancaImpressao) {
		this.nrQuantidadeLimiteCobrancaImpressao = nrQuantidadeLimiteCobrancaImpressao;
	}

	/**
	 * Gets the hr limite apresenta carta.
	 *
	 * @return the hr limite apresenta carta
	 */
	public String getHrLimiteApresentaCarta() {
		return hrLimiteApresentaCarta;
	}

	/**
	 * Sets the hr limite apresenta carta.
	 *
	 * @param hrLimiteApresentaCarta the new hr limite apresenta carta
	 */
	public void setHrLimiteApresentaCarta(String hrLimiteApresentaCarta) {
		this.hrLimiteApresentaCarta = hrLimiteApresentaCarta;
	}

	/**
	 * Gets the ds frase padrao caput.
	 *
	 * @return the ds frase padrao caput
	 */
	public String getDsFrasePadraoCaput() {
		return dsFrasePadraoCaput;
	}

	/**
	 * Sets the ds frase padrao caput.
	 *
	 * @param dsFrasePadraoCaput the new ds frase padrao caput
	 */
	public void setDsFrasePadraoCaput(String dsFrasePadraoCaput) {
		this.dsFrasePadraoCaput = dsFrasePadraoCaput;
	}

    /**
     * Nome: setDsDisponibilizacaoComprovanteBanco
     *
     * @param dsDisponibilizacaoComprovanteBanco
     */
    public void setDsDisponibilizacaoComprovanteBanco(String dsDisponibilizacaoComprovanteBanco) {
        this.dsDisponibilizacaoComprovanteBanco = dsDisponibilizacaoComprovanteBanco;
    }

    /**
     * Nome: getDsDisponibilizacaoComprovanteBanco
     *
     * @return dsDisponibilizacaoComprovanteBanco
     */
    public String getDsDisponibilizacaoComprovanteBanco() {
        return dsDisponibilizacaoComprovanteBanco;
    }
}
