/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivorecebido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivorecebido.bean;

/**
 * Nome: ArquivoRecebidoInconsistentesDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ArquivoRecebidoInconsistentesDTO {
	
	//Response
	/** Atributo sistemaLegado. */
	private String sistemaLegado;
	
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo codigoFilialCnpj. */
	private String codigoFilialCnpj;
	
	/** Atributo codigoControleCpfCnpj. */
	private String codigoControleCpfCnpj;
	
	/** Atributo perfil. */
	private String perfil;
	
	/** Atributo numeroArquivoRemessa. */
	private String numeroArquivoRemessa;
	
	/** Atributo numeroLoteRemessa. */
	private String numeroLoteRemessa;
	
	/** Atributo numeroSequenciaRemessa. */
	private String numeroSequenciaRemessa;
	
	/** Atributo tipoPesquisa. */
	private String tipoPesquisa;
	
	/** Atributo recurso. */
	private String recurso;
	
	/** Atributo filler. */
	private String filler;
	
	/** Atributo totalRegistros. */
	private int totalRegistros;
	
	/** Atributo dataInclusaoRemessa. */
	private String dataInclusaoRemessa;
	
	//Request
	/** Atributo numeroSequenciaRegistroRemessa. */
	private String numeroSequenciaRegistroRemessa;
	
	/** Atributo codigoInstrucaoMovimento. */
	private String codigoInstrucaoMovimento;
	
	/** Atributo descricaoInstrucaoMovimento. */
	private String descricaoInstrucaoMovimento;
	
	/** Atributo codigoInstrucaoMovimentoStr. */
	private String codigoInstrucaoMovimentoStr;
	
	/** Atributo controlePagamento. */
	private String controlePagamento;
	
	
	
	
	
	/**
	 * Get: totalRegistros.
	 *
	 * @return totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}
	
	/**
	 * Set: totalRegistros.
	 *
	 * @param totalRegistros the total registros
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	
	/**
	 * Get: codigoInstrucaoMovimentoStr.
	 *
	 * @return codigoInstrucaoMovimentoStr
	 */
	public String getCodigoInstrucaoMovimentoStr() {
		return codigoInstrucaoMovimentoStr;
	}
	
	/**
	 * Set: codigoInstrucaoMovimentoStr.
	 *
	 * @param codigoInstrucaoMovimentoStr the codigo instrucao movimento str
	 */
	public void setCodigoInstrucaoMovimentoStr(String codigoInstrucaoMovimentoStr) {
		this.codigoInstrucaoMovimentoStr = codigoInstrucaoMovimentoStr;
	}
	
	/**
	 * Get: codigoControleCpfCnpj.
	 *
	 * @return codigoControleCpfCnpj
	 */
	public String getCodigoControleCpfCnpj() {
		return codigoControleCpfCnpj;
	}
	
	/**
	 * Set: codigoControleCpfCnpj.
	 *
	 * @param codigoControleCpfCnpj the codigo controle cpf cnpj
	 */
	public void setCodigoControleCpfCnpj(String codigoControleCpfCnpj) {
		this.codigoControleCpfCnpj = codigoControleCpfCnpj;
	}
	
	/**
	 * Get: codigoFilialCnpj.
	 *
	 * @return codigoFilialCnpj
	 */
	public String getCodigoFilialCnpj() {
		return codigoFilialCnpj;
	}
	
	/**
	 * Set: codigoFilialCnpj.
	 *
	 * @param codigoFilialCnpj the codigo filial cnpj
	 */
	public void setCodigoFilialCnpj(String codigoFilialCnpj) {
		this.codigoFilialCnpj = codigoFilialCnpj;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	
	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	/**
	 * Get: filler.
	 *
	 * @return filler
	 */
	public String getFiller() {
		return filler;
	}
	
	/**
	 * Set: filler.
	 *
	 * @param filler the filler
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
	/**
	 * Get: numeroArquivoRemessa.
	 *
	 * @return numeroArquivoRemessa
	 */
	public String getNumeroArquivoRemessa() {
		return numeroArquivoRemessa;
	}
	
	/**
	 * Set: numeroArquivoRemessa.
	 *
	 * @param numeroArquivoRemessa the numero arquivo remessa
	 */
	public void setNumeroArquivoRemessa(String numeroArquivoRemessa) {
		this.numeroArquivoRemessa = numeroArquivoRemessa;
	}
	
	/**
	 * Get: numeroLoteRemessa.
	 *
	 * @return numeroLoteRemessa
	 */
	public String getNumeroLoteRemessa() {
		return numeroLoteRemessa;
	}
	
	/**
	 * Set: numeroLoteRemessa.
	 *
	 * @param numeroLoteRemessa the numero lote remessa
	 */
	public void setNumeroLoteRemessa(String numeroLoteRemessa) {
		this.numeroLoteRemessa = numeroLoteRemessa;
	}
	
	/**
	 * Get: numeroSequenciaRemessa.
	 *
	 * @return numeroSequenciaRemessa
	 */
	public String getNumeroSequenciaRemessa() {
		return numeroSequenciaRemessa;
	}
	
	/**
	 * Set: numeroSequenciaRemessa.
	 *
	 * @param numeroSequenciaRemessa the numero sequencia remessa
	 */
	public void setNumeroSequenciaRemessa(String numeroSequenciaRemessa) {
		this.numeroSequenciaRemessa = numeroSequenciaRemessa;
	}
	
	/**
	 * Get: perfil.
	 *
	 * @return perfil
	 */
	public String getPerfil() {
		return perfil;
	}
	
	/**
	 * Set: perfil.
	 *
	 * @param perfil the perfil
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public String getRecurso() {
		return recurso;
	}
	
	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}
	
	/**
	 * Get: sistemaLegado.
	 *
	 * @return sistemaLegado
	 */
	public String getSistemaLegado() {
		return sistemaLegado;
	}
	
	/**
	 * Set: sistemaLegado.
	 *
	 * @param sistemaLegado the sistema legado
	 */
	public void setSistemaLegado(String sistemaLegado) {
		this.sistemaLegado = sistemaLegado;
	}
	
	/**
	 * Get: tipoPesquisa.
	 *
	 * @return tipoPesquisa
	 */
	public String getTipoPesquisa() {
		return tipoPesquisa;
	}
	
	/**
	 * Set: tipoPesquisa.
	 *
	 * @param tipoPesquisa the tipo pesquisa
	 */
	public void setTipoPesquisa(String tipoPesquisa) {
		this.tipoPesquisa = tipoPesquisa;
	}
	
	/**
	 * Get: codigoInstrucaoMovimento.
	 *
	 * @return codigoInstrucaoMovimento
	 */
	public String getCodigoInstrucaoMovimento() {
		return codigoInstrucaoMovimento;
	}
	
	/**
	 * Set: codigoInstrucaoMovimento.
	 *
	 * @param codigoInstrucaoMovimento the codigo instrucao movimento
	 */
	public void setCodigoInstrucaoMovimento(String codigoInstrucaoMovimento) {
		this.codigoInstrucaoMovimento = codigoInstrucaoMovimento;
	}
	
	/**
	 * Get: controlePagamento.
	 *
	 * @return controlePagamento
	 */
	public String getControlePagamento() {
		return controlePagamento;
	}
	
	/**
	 * Set: controlePagamento.
	 *
	 * @param controlePagamento the controle pagamento
	 */
	public void setControlePagamento(String controlePagamento) {
		this.controlePagamento = controlePagamento;
	}
	
	/**
	 * Get: numeroSequenciaRegistroRemessa.
	 *
	 * @return numeroSequenciaRegistroRemessa
	 */
	public String getNumeroSequenciaRegistroRemessa() {
		return numeroSequenciaRegistroRemessa;
	}
	
	/**
	 * Set: numeroSequenciaRegistroRemessa.
	 *
	 * @param numeroSequenciaRegistroRemessa the numero sequencia registro remessa
	 */
	public void setNumeroSequenciaRegistroRemessa(
			String numeroSequenciaRegistroRemessa) {
		this.numeroSequenciaRegistroRemessa = numeroSequenciaRegistroRemessa;
	}
	
	/**
	 * Get: dataInclusaoRemessa.
	 *
	 * @return dataInclusaoRemessa
	 */
	public String getDataInclusaoRemessa() {
		return dataInclusaoRemessa;
	}
	
	/**
	 * Set: dataInclusaoRemessa.
	 *
	 * @param dataInclusaoRemessa the data inclusao remessa
	 */
	public void setDataInclusaoRemessa(String dataInclusaoRemessa) {
		this.dataInclusaoRemessa = dataInclusaoRemessa;
	}
	
	/**
	 * Get: descricaoInstrucaoMovimento.
	 *
	 * @return descricaoInstrucaoMovimento
	 */
	public String getDescricaoInstrucaoMovimento() {
		return descricaoInstrucaoMovimento;
	}
	
	/**
	 * Set: descricaoInstrucaoMovimento.
	 *
	 * @param descricaoInstrucaoMovimento the descricao instrucao movimento
	 */
	public void setDescricaoInstrucaoMovimento(String descricaoInstrucaoMovimento) {
		this.descricaoInstrucaoMovimento = descricaoInstrucaoMovimento;
	}

}
