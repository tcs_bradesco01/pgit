package br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Arquivo criado em 22/09/15.
 */
public class ConsultarLimiteHistoricoOcorrenciasSaidaDTO {

    private BigDecimal vlAdicionalContratoConta;

    private String dtInicioValorAdicional;

    private String dtFimValorAdicional;

    private BigDecimal vlAdicContrCtaTed;

    private String dtIniVlrAdicionalTed;

    private String dtFimVlrAdicionalTed;

    private String cdUsuarioInclusao;

    private String cdUsuarioInclusaoExterno;

    private String hrInclusaoRegistro;

    private String cdOperacaoCanalInclusao;

    private Integer cdTipoCanalInclusao;
    
    private String dsTipoCanalInclusao;

    private String cdUsuarioManutencao;

    private String cdUsuarioManutencaoExterno;

    private String hrManutencaoRegistro;

    private String cdOperacaoCanalManutencao;

    private Integer cdTipoCanalManutencao;
    
    private String dsTipoCanalManutencao;
    
    private String hora;
    
    private String data;
    
    private String horaManutencaoRegistro;
    
    private String dataManutencaoRegistro;
    
    public String getUsuarioFormatado(){
    	if("".equals(getCdUsuarioInclusao()) || "".equals(getCdUsuarioInclusaoExterno())){
    		return getCdUsuarioInclusao() + " " + getCdUsuarioInclusaoExterno();
    	}
    	return getCdUsuarioInclusao() + "/" + getCdUsuarioInclusaoExterno();
    }
    
    public String getUsuarioManutencaoFormatado(){
    	if("".equals(getCdUsuarioManutencao()) || "".equals(getCdUsuarioManutencaoExterno())){
    		return getCdUsuarioManutencao() + " " + getCdUsuarioManutencaoExterno();
    	}
    	return getCdUsuarioManutencao() + "/" + getCdUsuarioManutencaoExterno();
    }
    
    public String getPrazoValidade(){
    	if("".equals(getDtInicioValorAdicional()) || "".equals(getDtFimValorAdicional())){
    		return getDtInicioValorAdicional() + " " + getDtFimValorAdicional();
    	}
    	return getDtInicioValorAdicional() + "/" + getDtFimValorAdicional();
    }
    
    public String getPrazoValidadeTED(){
    	if("".equals(getDtIniVlrAdicionalTed()) || "".equals(getDtFimVlrAdicionalTed())){
    		return getDtIniVlrAdicionalTed() + " " + getDtFimVlrAdicionalTed();
    	}
    	return getDtIniVlrAdicionalTed() + "/" + getDtFimVlrAdicionalTed();
    }
    
    public String getCdTipoCanalInclusaoFormatado() {
        if(getCdTipoCanalInclusao() == null || getCdTipoCanalInclusao() == 0){
            return "";
        }
        return getCdTipoCanalInclusao().toString() + " - " + getDsTipoCanalInclusao();
    }
    
    public String getCdTipoCanalManutencaoFormatado() {
        if(getCdTipoCanalManutencao() == null || getCdTipoCanalManutencao() == 0){
            return "";
        }
        return getCdTipoCanalManutencao().toString() + " - " + getDsTipoCanalManutencao();
    }

    public void setVlAdicionalContratoConta(BigDecimal vlAdicionalContratoConta) {
        this.vlAdicionalContratoConta = vlAdicionalContratoConta;
    }

    public BigDecimal getVlAdicionalContratoConta() {
        return this.vlAdicionalContratoConta;
    }

    public void setDtInicioValorAdicional(String dtInicioValorAdicional) {
        this.dtInicioValorAdicional = dtInicioValorAdicional;
    }

    public String getDtInicioValorAdicional() {
        return this.dtInicioValorAdicional;
    }

    public void setDtFimValorAdicional(String dtFimValorAdicional) {
        this.dtFimValorAdicional = dtFimValorAdicional;
    }

    public String getDtFimValorAdicional() {
        return this.dtFimValorAdicional;
    }

    public void setVlAdicContrCtaTed(BigDecimal vlAdicContrCtaTed) {
        this.vlAdicContrCtaTed = vlAdicContrCtaTed;
    }

    public BigDecimal getVlAdicContrCtaTed() {
        return this.vlAdicContrCtaTed;
    }

    public void setDtIniVlrAdicionalTed(String dtIniVlrAdicionalTed) {
        this.dtIniVlrAdicionalTed = dtIniVlrAdicionalTed;
    }

    public String getDtIniVlrAdicionalTed() {
        return this.dtIniVlrAdicionalTed;
    }

    public void setDtFimVlrAdicionalTed(String dtFimVlrAdicionalTed) {
        this.dtFimVlrAdicionalTed = dtFimVlrAdicionalTed;
    }

    public String getDtFimVlrAdicionalTed() {
        return this.dtFimVlrAdicionalTed;
    }

    public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
        this.cdUsuarioInclusao = cdUsuarioInclusao;
    }

    public String getCdUsuarioInclusao() {
        return this.cdUsuarioInclusao;
    }

    public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
        this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    }

    public String getCdUsuarioInclusaoExterno() {
        return this.cdUsuarioInclusaoExterno;
    }

    public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
        this.hrInclusaoRegistro = hrInclusaoRegistro;
    }

    public String getHrInclusaoRegistro() {
        return this.hrInclusaoRegistro;
    }

    public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
        this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    }

    public String getCdOperacaoCanalInclusao() {
        return this.cdOperacaoCanalInclusao;
    }

    public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
        this.cdTipoCanalInclusao = cdTipoCanalInclusao;
    }

    public Integer getCdTipoCanalInclusao() {
        return this.cdTipoCanalInclusao;
    }

    public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
        this.cdUsuarioManutencao = cdUsuarioManutencao;
    }

    public String getCdUsuarioManutencao() {
        return this.cdUsuarioManutencao;
    }

    public void setCdUsuarioManutencaoExterno(String cdUsuarioManutencaoExterno) {
        this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    }

    public String getCdUsuarioManutencaoExterno() {
        return this.cdUsuarioManutencaoExterno;
    }

    public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
        this.hrManutencaoRegistro = hrManutencaoRegistro;
    }

    public String getHrManutencaoRegistro() {
        return this.hrManutencaoRegistro;
    }

    public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
        this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    }

    public String getCdOperacaoCanalManutencao() {
        return this.cdOperacaoCanalManutencao;
    }

    public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
        this.cdTipoCanalManutencao = cdTipoCanalManutencao;
    }

    public Integer getCdTipoCanalManutencao() {
        return this.cdTipoCanalManutencao;
    }

	/**
	 * @return the hora
	 */
	public String getHora() {
		return hora;
	}

	/**
	 * @param hora the hora to set
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	public String getHoraManutencaoRegistro() {
		return horaManutencaoRegistro;
	}

	public void setHoraManutencaoRegistro(String horaManutencaoRegistro) {
		this.horaManutencaoRegistro = horaManutencaoRegistro;
	}

	public String getDataManutencaoRegistro() {
		return dataManutencaoRegistro;
	}

	public void setDataManutencaoRegistro(String dataManutencaoRegistro) {
		this.dataManutencaoRegistro = dataManutencaoRegistro;
	}

    /**
     * Nome: getDsTipoCanalInclusao
     *
     * @return dsTipoCanalInclusao
     */
    public String getDsTipoCanalInclusao() {
        return dsTipoCanalInclusao;
    }

    /**
     * Nome: setDsTipoCanalInclusao
     *
     * @param dsTipoCanalInclusao
     */
    public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
        this.dsTipoCanalInclusao = dsTipoCanalInclusao;
    }

    /**
     * Nome: getDsTipoCanalManutencao
     *
     * @return dsTipoCanalManutencao
     */
    public String getDsTipoCanalManutencao() {
        return dsTipoCanalManutencao;
    }

    /**
     * Nome: setDsTipoCanalManutencao
     *
     * @param dsTipoCanalManutencao
     */
    public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
        this.dsTipoCanalManutencao = dsTipoCanalManutencao;
    }

}