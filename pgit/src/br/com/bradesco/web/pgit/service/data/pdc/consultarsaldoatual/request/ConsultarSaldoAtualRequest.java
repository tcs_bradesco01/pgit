/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarSaldoAtualRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarSaldoAtualRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _nrCnpjCpf
     */
    private long _nrCnpjCpf = 0;

    /**
     * keeps track of state for field: _nrCnpjCpf
     */
    private boolean _has_nrCnpjCpf;

    /**
     * Field _cdCnpjCpfFilial
     */
    private int _cdCnpjCpfFilial = 0;

    /**
     * keeps track of state for field: _cdCnpjCpfFilial
     */
    private boolean _has_cdCnpjCpfFilial;

    /**
     * Field _cdCnpjCpfDigito
     */
    private int _cdCnpjCpfDigito = 0;

    /**
     * keeps track of state for field: _cdCnpjCpfDigito
     */
    private boolean _has_cdCnpjCpfDigito;

    /**
     * Field _dsComplemento
     */
    private java.lang.String _dsComplemento;

    /**
     * Field _cdPessoaContratoDebito
     */
    private long _cdPessoaContratoDebito = 0;

    /**
     * keeps track of state for field: _cdPessoaContratoDebito
     */
    private boolean _has_cdPessoaContratoDebito;

    /**
     * Field _cdTipoContratoDebito
     */
    private int _cdTipoContratoDebito = 0;

    /**
     * keeps track of state for field: _cdTipoContratoDebito
     */
    private boolean _has_cdTipoContratoDebito;

    /**
     * Field _nrSequenciaContratoDebito
     */
    private long _nrSequenciaContratoDebito = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoDebito
     */
    private boolean _has_nrSequenciaContratoDebito;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarSaldoAtualRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.request.ConsultarSaldoAtualRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjCpfDigito
     * 
     */
    public void deleteCdCnpjCpfDigito()
    {
        this._has_cdCnpjCpfDigito= false;
    } //-- void deleteCdCnpjCpfDigito() 

    /**
     * Method deleteCdCnpjCpfFilial
     * 
     */
    public void deleteCdCnpjCpfFilial()
    {
        this._has_cdCnpjCpfFilial= false;
    } //-- void deleteCdCnpjCpfFilial() 

    /**
     * Method deleteCdPessoaContratoDebito
     * 
     */
    public void deleteCdPessoaContratoDebito()
    {
        this._has_cdPessoaContratoDebito= false;
    } //-- void deleteCdPessoaContratoDebito() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdTipoContratoDebito
     * 
     */
    public void deleteCdTipoContratoDebito()
    {
        this._has_cdTipoContratoDebito= false;
    } //-- void deleteCdTipoContratoDebito() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrCnpjCpf
     * 
     */
    public void deleteNrCnpjCpf()
    {
        this._has_nrCnpjCpf= false;
    } //-- void deleteNrCnpjCpf() 

    /**
     * Method deleteNrSequenciaContratoDebito
     * 
     */
    public void deleteNrSequenciaContratoDebito()
    {
        this._has_nrSequenciaContratoDebito= false;
    } //-- void deleteNrSequenciaContratoDebito() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdCnpjCpfDigito'.
     * 
     * @return int
     * @return the value of field 'cdCnpjCpfDigito'.
     */
    public int getCdCnpjCpfDigito()
    {
        return this._cdCnpjCpfDigito;
    } //-- int getCdCnpjCpfDigito() 

    /**
     * Returns the value of field 'cdCnpjCpfFilial'.
     * 
     * @return int
     * @return the value of field 'cdCnpjCpfFilial'.
     */
    public int getCdCnpjCpfFilial()
    {
        return this._cdCnpjCpfFilial;
    } //-- int getCdCnpjCpfFilial() 

    /**
     * Returns the value of field 'cdPessoaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'cdPessoaContratoDebito'.
     */
    public long getCdPessoaContratoDebito()
    {
        return this._cdPessoaContratoDebito;
    } //-- long getCdPessoaContratoDebito() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdTipoContratoDebito'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoDebito'.
     */
    public int getCdTipoContratoDebito()
    {
        return this._cdTipoContratoDebito;
    } //-- int getCdTipoContratoDebito() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dsComplemento'.
     * 
     * @return String
     * @return the value of field 'dsComplemento'.
     */
    public java.lang.String getDsComplemento()
    {
        return this._dsComplemento;
    } //-- java.lang.String getDsComplemento() 

    /**
     * Returns the value of field 'nrCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'nrCnpjCpf'.
     */
    public long getNrCnpjCpf()
    {
        return this._nrCnpjCpf;
    } //-- long getNrCnpjCpf() 

    /**
     * Returns the value of field 'nrSequenciaContratoDebito'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoDebito'.
     */
    public long getNrSequenciaContratoDebito()
    {
        return this._nrSequenciaContratoDebito;
    } //-- long getNrSequenciaContratoDebito() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdCnpjCpfDigito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCpfDigito()
    {
        return this._has_cdCnpjCpfDigito;
    } //-- boolean hasCdCnpjCpfDigito() 

    /**
     * Method hasCdCnpjCpfFilial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCpfFilial()
    {
        return this._has_cdCnpjCpfFilial;
    } //-- boolean hasCdCnpjCpfFilial() 

    /**
     * Method hasCdPessoaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaContratoDebito()
    {
        return this._has_cdPessoaContratoDebito;
    } //-- boolean hasCdPessoaContratoDebito() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdTipoContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoDebito()
    {
        return this._has_cdTipoContratoDebito;
    } //-- boolean hasCdTipoContratoDebito() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrCnpjCpf()
    {
        return this._has_nrCnpjCpf;
    } //-- boolean hasNrCnpjCpf() 

    /**
     * Method hasNrSequenciaContratoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoDebito()
    {
        return this._has_nrSequenciaContratoDebito;
    } //-- boolean hasNrSequenciaContratoDebito() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCnpjCpfDigito'.
     * 
     * @param cdCnpjCpfDigito the value of field 'cdCnpjCpfDigito'.
     */
    public void setCdCnpjCpfDigito(int cdCnpjCpfDigito)
    {
        this._cdCnpjCpfDigito = cdCnpjCpfDigito;
        this._has_cdCnpjCpfDigito = true;
    } //-- void setCdCnpjCpfDigito(int) 

    /**
     * Sets the value of field 'cdCnpjCpfFilial'.
     * 
     * @param cdCnpjCpfFilial the value of field 'cdCnpjCpfFilial'.
     */
    public void setCdCnpjCpfFilial(int cdCnpjCpfFilial)
    {
        this._cdCnpjCpfFilial = cdCnpjCpfFilial;
        this._has_cdCnpjCpfFilial = true;
    } //-- void setCdCnpjCpfFilial(int) 

    /**
     * Sets the value of field 'cdPessoaContratoDebito'.
     * 
     * @param cdPessoaContratoDebito the value of field
     * 'cdPessoaContratoDebito'.
     */
    public void setCdPessoaContratoDebito(long cdPessoaContratoDebito)
    {
        this._cdPessoaContratoDebito = cdPessoaContratoDebito;
        this._has_cdPessoaContratoDebito = true;
    } //-- void setCdPessoaContratoDebito(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdTipoContratoDebito'.
     * 
     * @param cdTipoContratoDebito the value of field
     * 'cdTipoContratoDebito'.
     */
    public void setCdTipoContratoDebito(int cdTipoContratoDebito)
    {
        this._cdTipoContratoDebito = cdTipoContratoDebito;
        this._has_cdTipoContratoDebito = true;
    } //-- void setCdTipoContratoDebito(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'dsComplemento'.
     * 
     * @param dsComplemento the value of field 'dsComplemento'.
     */
    public void setDsComplemento(java.lang.String dsComplemento)
    {
        this._dsComplemento = dsComplemento;
    } //-- void setDsComplemento(java.lang.String) 

    /**
     * Sets the value of field 'nrCnpjCpf'.
     * 
     * @param nrCnpjCpf the value of field 'nrCnpjCpf'.
     */
    public void setNrCnpjCpf(long nrCnpjCpf)
    {
        this._nrCnpjCpf = nrCnpjCpf;
        this._has_nrCnpjCpf = true;
    } //-- void setNrCnpjCpf(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoDebito'.
     * 
     * @param nrSequenciaContratoDebito the value of field
     * 'nrSequenciaContratoDebito'.
     */
    public void setNrSequenciaContratoDebito(long nrSequenciaContratoDebito)
    {
        this._nrSequenciaContratoDebito = nrSequenciaContratoDebito;
        this._has_nrSequenciaContratoDebito = true;
    } //-- void setNrSequenciaContratoDebito(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarSaldoAtualRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.request.ConsultarSaldoAtualRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.request.ConsultarSaldoAtualRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.request.ConsultarSaldoAtualRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarsaldoatual.request.ConsultarSaldoAtualRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
