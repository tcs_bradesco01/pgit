/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.ICanLibPagtoConsolidadoSemConSaldoService;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.ICanLibPagtoConsolidadoSemConSaldoServiceConstants;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.ConsultarCanLibPagtosConsSemConsSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.DetalharCanLibPagtosConsSemConsSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.DetalharCanLibPagtosConsSemConsSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarliberacaointegral.request.CancelarLiberacaoIntegralRequest;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarliberacaointegral.response.CancelarLiberacaoIntegralResponse;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarliberacaoparcial.request.CancelarLiberacaoParcialRequest;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarliberacaoparcial.response.CancelarLiberacaoParcialResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcanlibpagtosconssemconssaldo.request.ConsultarCanLibPagtosConsSemConsSaldoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcanlibpagtosconssemconssaldo.response.ConsultarCanLibPagtosConsSemConsSaldoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcanlibpagtosconssemconssaldo.request.DetalharCanLibPagtosConsSemConsSaldoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharcanlibpagtosconssemconssaldo.response.DetalharCanLibPagtosConsSemConsSaldoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: CanLibPagtoConsolidadoSemConSaldo
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class CanLibPagtoConsolidadoSemConSaldoServiceImpl implements ICanLibPagtoConsolidadoSemConSaldoService {
    
    /** Atributo factoryAdapter. */
    private FactoryAdapter factoryAdapter;

    /** Atributo nf. */
    private java.text.NumberFormat nf = java.text.NumberFormat.getInstance(new Locale("pt_br"));

    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
	return factoryAdapter;
    }

    /**
     * Set: factoryAdapter.
     *
     * @param factoryAdapter the factory adapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
	this.factoryAdapter = factoryAdapter;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.ICanLibPagtoConsolidadoSemConSaldoService#detalharCanLibPagtosConsSemConsSaldo(br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.DetalharCanLibPagtosConsSemConsSaldoEntradaDTO)
     */
    public DetalharCanLibPagtosConsSemConsSaldoSaidaDTO detalharCanLibPagtosConsSemConsSaldo(DetalharCanLibPagtosConsSemConsSaldoEntradaDTO entradaDTO) {
	DetalharCanLibPagtosConsSemConsSaldoRequest request = new DetalharCanLibPagtosConsSemConsSaldoRequest();
	DetalharCanLibPagtosConsSemConsSaldoResponse response = new DetalharCanLibPagtosConsSemConsSaldoResponse();

	request.setNrOcorrencias(ICanLibPagtoConsolidadoSemConSaldoServiceConstants.NUMERO_OCORRENCIAS_DETALHAR_CONSULTA);

	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
	request.setNrCnpjCpf(PgitUtil.verificaLongNulo(entradaDTO.getNrCnpjCpf()));
	request.setNrFilialcnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrFilialcnpjCpf()));
	request.setNrControleCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getNrControleCnpjCpf()));
	request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemessaPagamento()));
	request.setDtCreditoPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamento()));
	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
	request.setCdDigitoConta(PgitUtil.complementaDigitoConta(entradaDTO.getCdDigitoConta()));
	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
	request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
	request.setCdAgendadoPagaoNaoPago(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadoPagaoNaoPago()));
	request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
	request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
	request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
	request.setCdTituloPgtoRastreado(0);
	request.setCdIndicadorAutorizacaoPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndicadorAutorizacaoPagador()));
	request.setNrLoteInternoPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrLoteInternoPagamento()));

	response = getFactoryAdapter().getDetalharCanLibPagtosConsSemConsSaldoPDCAdapter().invokeProcess(request);

	DetalharCanLibPagtosConsSemConsSaldoSaidaDTO saidaDTO = new DetalharCanLibPagtosConsSemConsSaldoSaidaDTO();
	saidaDTO.setCodMensagem(response.getCodMensagem());
	saidaDTO.setMensagem(response.getMensagem());
	saidaDTO.setNumeroConsultas(response.getNumeroConsultas());
	saidaDTO.setDsBancoDebito(response.getDsBancoDebito());
	saidaDTO.setDsAgenciaDebito(response.getDsAgenciaDebito());
	saidaDTO.setDsTipoContaDebito(response.getDsTipoContaDebito());
	saidaDTO.setDsContrato(response.getDsContrato());
	saidaDTO.setDsSituacaoContrato(response.getDsSituacaoContrato());
	saidaDTO.setNmCliente(response.getNmCliente());

	List<OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO> listaOcorrencias = new ArrayList<OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO>();

	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO o = new OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO();
	    o.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
	    o.setCdBanco(response.getOcorrencias(i).getCdBanco());
	    o.setCdCnpjCpfFavorecido(response.getOcorrencias(i).getCdCnpjCpfFavorecido());
	    o.setCdConta(response.getOcorrencias(i).getCdConta());
	    o.setCdControleCnpjCpfFavorecido(response.getOcorrencias(i).getCdControleCnpjCpfFavorecido());
	    o.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
	    o.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
	    o.setCdFavorecido(response.getOcorrencias(i).getCdFavorecido() == 0L ? "" : String.valueOf(response.getOcorrencias(i).getCdFavorecido()));
	    o.setCdFilialCnpjCpfFavorecido(response.getOcorrencias(i).getCdFilialCnpjCpfFavorecido());
	    o.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
	    o.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
	    o.setDsBeneficio(response.getOcorrencias(i).getDsBeneficio());
	    o.setNrPagamento(response.getOcorrencias(i).getNrPagamento());
	    o.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
	    o.setFavorecidoBeneficiario(response.getOcorrencias(i).getDsBeneficio());
	    o.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
	    o.setBancoFormatado(PgitUtil.formatBanco(response.getOcorrencias(i).getCdBanco(), false));
	    o.setAgenciaDigitoFormatada(PgitUtil.formatAgencia(response.getOcorrencias(i).getCdAgencia(), response.getOcorrencias(i)
		    .getCdDigitoAgencia(), false));
	    o.setContaDigitoFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdConta(), response.getOcorrencias(i).getCdDigitoConta(),
		    false));
	    o.setContaCreditoFormatada(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBanco(), response.getOcorrencias(i)
		    .getCdAgencia(), response.getOcorrencias(i).getCdDigitoAgencia(), response.getOcorrencias(i).getCdConta(), response
		    .getOcorrencias(i).getCdDigitoConta(), true));
	    o.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
	    o.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());		
	    listaOcorrencias.add(o);
	}
	saidaDTO.setListaDetalharCanLibPagtosConsSemConsSaldo(listaOcorrencias);
	return saidaDTO;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.ICanLibPagtoConsolidadoSemConSaldoService#cancelarLiberacaoIntegral(br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoIntegralEntradaDTO)
     */
    public CancelarLiberacaoIntegralSaidaDTO cancelarLiberacaoIntegral(CancelarLiberacaoIntegralEntradaDTO entradaDTO) {
        CancelarLiberacaoIntegralSaidaDTO saidaDTO = new CancelarLiberacaoIntegralSaidaDTO();
        CancelarLiberacaoIntegralRequest request = new CancelarLiberacaoIntegralRequest();
        CancelarLiberacaoIntegralResponse response = new CancelarLiberacaoIntegralResponse();

        request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaDebito()));
        request.setCdBancoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoDebito()));
        request.setCdContaDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdContaDebito()));
        request.setCdControleCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjCpf()));
        request.setCdCnpjCpf(PgitUtil.verificaLongNulo(entradaDTO.getCdCorpoCnpjCpf()));
        request.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjCpf()));
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(PgitUtil
            .verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setCdSituacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacao()));
        request.setDtAgendamentoPagamento(PgitUtil.verificaStringNula(entradaDTO.getDtAgendamentoPagamento()));
        request.setHrInclusaoRemessaSistema(PgitUtil.verificaStringNula(entradaDTO.getHrInclusaoRemessaSistema()));
        request.setNrSequenciaArquivoRemesa(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaArquivoRemessa()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));

        response = getFactoryAdapter().getCancelarLiberacaoIntegralPDCAdapter().invokeProcess(request);
        
        while ("PGIT0009".equals(response.getCodMensagem())) {
            response = getFactoryAdapter().getCancelarLiberacaoIntegralPDCAdapter().invokeProcess(request);
        }
        
        saidaDTO.setCodMensagem(response.getCodMensagem());
        saidaDTO.setMensagem(response.getMensagem());

        return saidaDTO;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.ICanLibPagtoConsolidadoSemConSaldoService#cancelarLiberacaoParcial(java.util.List)
     */
    public CancelarLiberacaoParcialSaidaDTO cancelarLiberacaoParcial(List<CancelarLiberacaoParcialEntradaDTO> listaEntradaDTO) {
	CancelarLiberacaoParcialRequest request = new CancelarLiberacaoParcialRequest();
	CancelarLiberacaoParcialResponse response = new CancelarLiberacaoParcialResponse();
	CancelarLiberacaoParcialSaidaDTO saidaDTO = new CancelarLiberacaoParcialSaidaDTO();

	request.setNumeroConsultas(listaEntradaDTO.size());
	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.cancelarliberacaoparcial.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.cancelarliberacaoparcial.request.Ocorrencias>();

	for (int i = 0; i < request.getNumeroConsultas(); i++) {
	    br.com.bradesco.web.pgit.service.data.pdc.cancelarliberacaoparcial.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.cancelarliberacaoparcial.request.Ocorrencias();

	    ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaEntradaDTO.get(i).getCdPessoaJuridicaContrato()));
	    ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdTipoContratoNegocio()));
	    ocorrencia.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaEntradaDTO.get(i).getNrSequenciaContratoNegocio()));
	    ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdTipoCanal())));
	    ocorrencia.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdProdutoServicoOperacao()));
	    ocorrencia.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdOperacaoProdutoServico()));
	    ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(listaEntradaDTO.get(i).getCdControlePagamento()));

	    ocorrencias.add(ocorrencia);
	}

	request.setOcorrencias(ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.cancelarliberacaoparcial.request.Ocorrencias[0]));

	response = getFactoryAdapter().getCancelarLiberacaoParcialPDCAdapter().invokeProcess(request);

	saidaDTO.setCodMensagem(response.getCodMensagem());
	saidaDTO.setMensagem(response.getMensagem());

	return saidaDTO;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.ICanLibPagtoConsolidadoSemConSaldoService#consultarCanLibPagtosConsSemConsSaldo(br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.ConsultarCanLibPagtosConsSemConsSaldoEntradaDTO)
     */
    public List<ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO> consultarCanLibPagtosConsSemConsSaldo(
	    ConsultarCanLibPagtosConsSemConsSaldoEntradaDTO entradaDTO) {
	List<ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO> listaRetorno = new ArrayList<ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO>();
	ConsultarCanLibPagtosConsSemConsSaldoRequest request = new ConsultarCanLibPagtosConsSemConsSaldoRequest();
	ConsultarCanLibPagtosConsSemConsSaldoResponse response = new ConsultarCanLibPagtosConsSemConsSaldoResponse();

	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
	request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
	request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
	request.setNrArquivoRemessaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemessaPagamento()));
	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
	request.setCdAgendadosPagosNaoPagos(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadosPagosNaoPagos()));
	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
	request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
	request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
	request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
	request.setNrOcorrencias(ICanLibPagtoConsolidadoSemConSaldoServiceConstants.NUMERO_OCORRENCIAS_CONSULTA);
	request.setCdTituloPgtoRastreado(0);
	
	response = getFactoryAdapter().getConsultarCanLibPagtosConsSemConsSaldoPDCAdapter().invokeProcess(request);

	ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO saidaDTO;

	for (int i = 0; i < response.getOcorrenciasCount(); i++) {
	    saidaDTO = new ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO();

	    saidaDTO.setCodMensagem(response.getCodMensagem());
	    saidaDTO.setNumeroConsultas(response.getNumeroConsultas());
	    saidaDTO.setMensagem(response.getMensagem());
	    saidaDTO.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
	    saidaDTO.setCdBanco(response.getOcorrencias(i).getCdBanco());
	    saidaDTO.setCdConta(response.getOcorrencias(i).getCdConta());
	    saidaDTO.setCdDigitoAgencia(response.getOcorrencias(i).getCdDigitoAgencia());
	    saidaDTO.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
	    saidaDTO.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
	    saidaDTO.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
	    saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
	    saidaDTO.setNrContratoOrigem(response.getOcorrencias(i).getNrContratoOrigem());
	    saidaDTO.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
	    saidaDTO.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
	    saidaDTO.setNrDigitoCnpjCpf(response.getOcorrencias(i).getNrDigitoCnpjCpf());
	    saidaDTO.setNrArquivoRemessaPagamento(response.getOcorrencias(i).getNrArquivoRemessaPagamento());
	    saidaDTO.setDtCreditoPagamento(response.getOcorrencias(i).getDtCreditoPagamento());
	    saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
	    saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
	    saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
	    saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
	    saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
	    saidaDTO.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
	    saidaDTO.setQtPagamentoFormatado(nf.format(new java.math.BigDecimal(response.getOcorrencias(i).getQtPagamento())));
	    saidaDTO.setQtPagamento(response.getOcorrencias(i).getQtPagamento());
	    saidaDTO.setVlEfetivoPagamentoCliente(response.getOcorrencias(i).getVlEfetivoPagamentoCliente());
	    saidaDTO.setCdPessoaContratoDebito(response.getOcorrencias(i).getCdPessoaContratoDebito());
	    saidaDTO.setCdTipoContratoDebito(response.getOcorrencias(i).getCdTipoContratoDebito());
	    saidaDTO.setNrSequenciaContratoDebito(response.getOcorrencias(i).getNrSequenciaContratoDebito());

	    saidaDTO.setCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(response.getOcorrencias(i).getNrCnpjCpf(), response.getOcorrencias(i)
		    .getNrFilialCnpjCpf(), response.getOcorrencias(i).getNrDigitoCnpjCpf()));
	    saidaDTO.setContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(saidaDTO.getCdBanco(), saidaDTO.getCdAgencia(), saidaDTO
		    .getCdDigitoAgencia(), saidaDTO.getCdConta(), saidaDTO.getCdDigitoConta(), true));
	    listaRetorno.add(saidaDTO);
	}

	return listaRetorno;
    }
}