package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

public class ValidarUsuarioSaidaDTO {
	
    private String codMensagem;
    private String mensagem;
    private Integer nrSequenciaUnidadeOrganizacional;
    private Integer cdAcessoDepartamento;
    private String dsAcessoDepartamento;
    
	public String getCodMensagem() {
		return codMensagem;
	}
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public Integer getNrSequenciaUnidadeOrganizacional() {
		return nrSequenciaUnidadeOrganizacional;
	}
	public void setNrSequenciaUnidadeOrganizacional(
			Integer nrSequenciaUnidadeOrganizacional) {
		this.nrSequenciaUnidadeOrganizacional = nrSequenciaUnidadeOrganizacional;
	}
	public Integer getCdAcessoDepartamento() {
		return cdAcessoDepartamento;
	}
	public void setCdAcessoDepartamento(Integer cdAcessoDepartamento) {
		this.cdAcessoDepartamento = cdAcessoDepartamento;
	}
	public String getDsAcessoDepartamento() {
		return dsAcessoDepartamento;
	}
	public void setDsAcessoDepartamento(String dsAcessoDepartamento) {
		this.dsAcessoDepartamento = dsAcessoDepartamento;
	}

}
