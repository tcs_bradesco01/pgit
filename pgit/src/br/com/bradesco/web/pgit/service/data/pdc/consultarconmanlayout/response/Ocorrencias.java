/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dsTipoLayoutArquivo
     */
    private java.lang.String _dsTipoLayoutArquivo;

    /**
     * Field _cdAplicacaoTransmissaoArquivo
     */
    private int _cdAplicacaoTransmissaoArquivo = 0;

    /**
     * keeps track of state for field: _cdAplicacaoTransmissaoArquiv
     */
    private boolean _has_cdAplicacaoTransmissaoArquivo;

    /**
     * Field _dsAplicacaoTransmissaoArquivo
     */
    private java.lang.String _dsAplicacaoTransmissaoArquivo;

    /**
     * Field _cdSituacao
     */
    private java.lang.String _cdSituacao;

    /**
     * Field _dsNomeArquivoRemessa
     */
    private java.lang.String _dsNomeArquivoRemessa;

    /**
     * Field _dsNomeArquivoRetorno
     */
    private java.lang.String _dsNomeArquivoRetorno;

    /**
     * Field _cdNivelControleRemessa
     */
    private int _cdNivelControleRemessa = 0;

    /**
     * keeps track of state for field: _cdNivelControleRemessa
     */
    private boolean _has_cdNivelControleRemessa;

    /**
     * Field _dsNivelControleRemessa
     */
    private java.lang.String _dsNivelControleRemessa;

    /**
     * Field _cdControleNumeroRemessa
     */
    private int _cdControleNumeroRemessa = 0;

    /**
     * keeps track of state for field: _cdControleNumeroRemessa
     */
    private boolean _has_cdControleNumeroRemessa;

    /**
     * Field _dsControleNumeroRemessa
     */
    private java.lang.String _dsControleNumeroRemessa;

    /**
     * Field _nrMaximoContagemRemessa
     */
    private long _nrMaximoContagemRemessa = 0;

    /**
     * keeps track of state for field: _nrMaximoContagemRemessa
     */
    private boolean _has_nrMaximoContagemRemessa;

    /**
     * Field _cdPerdcContagemRemessa
     */
    private int _cdPerdcContagemRemessa = 0;

    /**
     * keeps track of state for field: _cdPerdcContagemRemessa
     */
    private boolean _has_cdPerdcContagemRemessa;

    /**
     * Field _dsPerdcContagemRemessa
     */
    private java.lang.String _dsPerdcContagemRemessa;

    /**
     * Field _cdRejeicaoAcolhimentoRemessa
     */
    private int _cdRejeicaoAcolhimentoRemessa = 0;

    /**
     * keeps track of state for field: _cdRejeicaoAcolhimentoRemessa
     */
    private boolean _has_cdRejeicaoAcolhimentoRemessa;

    /**
     * Field _dsRejeicaoAcolhimentoRemessa
     */
    private java.lang.String _dsRejeicaoAcolhimentoRemessa;

    /**
     * Field _cdPercentualReheicaoRemessa
     */
    private java.math.BigDecimal _cdPercentualReheicaoRemessa = new java.math.BigDecimal("0");

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExter
     */
    private java.lang.String _cdUsuarioInclusaoExter;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExter
     */
    private java.lang.String _cdUsuarioManutencaoExter;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
        setCdPercentualReheicaoRemessa(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAplicacaoTransmissaoArquivo
     * 
     */
    public void deleteCdAplicacaoTransmissaoArquivo()
    {
        this._has_cdAplicacaoTransmissaoArquivo= false;
    } //-- void deleteCdAplicacaoTransmissaoArquivo() 

    /**
     * Method deleteCdControleNumeroRemessa
     * 
     */
    public void deleteCdControleNumeroRemessa()
    {
        this._has_cdControleNumeroRemessa= false;
    } //-- void deleteCdControleNumeroRemessa() 

    /**
     * Method deleteCdNivelControleRemessa
     * 
     */
    public void deleteCdNivelControleRemessa()
    {
        this._has_cdNivelControleRemessa= false;
    } //-- void deleteCdNivelControleRemessa() 

    /**
     * Method deleteCdPerdcContagemRemessa
     * 
     */
    public void deleteCdPerdcContagemRemessa()
    {
        this._has_cdPerdcContagemRemessa= false;
    } //-- void deleteCdPerdcContagemRemessa() 

    /**
     * Method deleteCdRejeicaoAcolhimentoRemessa
     * 
     */
    public void deleteCdRejeicaoAcolhimentoRemessa()
    {
        this._has_cdRejeicaoAcolhimentoRemessa= false;
    } //-- void deleteCdRejeicaoAcolhimentoRemessa() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrMaximoContagemRemessa
     * 
     */
    public void deleteNrMaximoContagemRemessa()
    {
        this._has_nrMaximoContagemRemessa= false;
    } //-- void deleteNrMaximoContagemRemessa() 

    /**
     * Returns the value of field 'cdAplicacaoTransmissaoArquivo'.
     * 
     * @return int
     * @return the value of field 'cdAplicacaoTransmissaoArquivo'.
     */
    public int getCdAplicacaoTransmissaoArquivo()
    {
        return this._cdAplicacaoTransmissaoArquivo;
    } //-- int getCdAplicacaoTransmissaoArquivo() 

    /**
     * Returns the value of field 'cdControleNumeroRemessa'.
     * 
     * @return int
     * @return the value of field 'cdControleNumeroRemessa'.
     */
    public int getCdControleNumeroRemessa()
    {
        return this._cdControleNumeroRemessa;
    } //-- int getCdControleNumeroRemessa() 

    /**
     * Returns the value of field 'cdNivelControleRemessa'.
     * 
     * @return int
     * @return the value of field 'cdNivelControleRemessa'.
     */
    public int getCdNivelControleRemessa()
    {
        return this._cdNivelControleRemessa;
    } //-- int getCdNivelControleRemessa() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdPercentualReheicaoRemessa'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualReheicaoRemessa'.
     */
    public java.math.BigDecimal getCdPercentualReheicaoRemessa()
    {
        return this._cdPercentualReheicaoRemessa;
    } //-- java.math.BigDecimal getCdPercentualReheicaoRemessa() 

    /**
     * Returns the value of field 'cdPerdcContagemRemessa'.
     * 
     * @return int
     * @return the value of field 'cdPerdcContagemRemessa'.
     */
    public int getCdPerdcContagemRemessa()
    {
        return this._cdPerdcContagemRemessa;
    } //-- int getCdPerdcContagemRemessa() 

    /**
     * Returns the value of field 'cdRejeicaoAcolhimentoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdRejeicaoAcolhimentoRemessa'.
     */
    public int getCdRejeicaoAcolhimentoRemessa()
    {
        return this._cdRejeicaoAcolhimentoRemessa;
    } //-- int getCdRejeicaoAcolhimentoRemessa() 

    /**
     * Returns the value of field 'cdSituacao'.
     * 
     * @return String
     * @return the value of field 'cdSituacao'.
     */
    public java.lang.String getCdSituacao()
    {
        return this._cdSituacao;
    } //-- java.lang.String getCdSituacao() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExter'.
     */
    public java.lang.String getCdUsuarioInclusaoExter()
    {
        return this._cdUsuarioInclusaoExter;
    } //-- java.lang.String getCdUsuarioInclusaoExter() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExter'.
     */
    public java.lang.String getCdUsuarioManutencaoExter()
    {
        return this._cdUsuarioManutencaoExter;
    } //-- java.lang.String getCdUsuarioManutencaoExter() 

    /**
     * Returns the value of field 'dsAplicacaoTransmissaoArquivo'.
     * 
     * @return String
     * @return the value of field 'dsAplicacaoTransmissaoArquivo'.
     */
    public java.lang.String getDsAplicacaoTransmissaoArquivo()
    {
        return this._dsAplicacaoTransmissaoArquivo;
    } //-- java.lang.String getDsAplicacaoTransmissaoArquivo() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsControleNumeroRemessa'.
     * 
     * @return String
     * @return the value of field 'dsControleNumeroRemessa'.
     */
    public java.lang.String getDsControleNumeroRemessa()
    {
        return this._dsControleNumeroRemessa;
    } //-- java.lang.String getDsControleNumeroRemessa() 

    /**
     * Returns the value of field 'dsNivelControleRemessa'.
     * 
     * @return String
     * @return the value of field 'dsNivelControleRemessa'.
     */
    public java.lang.String getDsNivelControleRemessa()
    {
        return this._dsNivelControleRemessa;
    } //-- java.lang.String getDsNivelControleRemessa() 

    /**
     * Returns the value of field 'dsNomeArquivoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsNomeArquivoRemessa'.
     */
    public java.lang.String getDsNomeArquivoRemessa()
    {
        return this._dsNomeArquivoRemessa;
    } //-- java.lang.String getDsNomeArquivoRemessa() 

    /**
     * Returns the value of field 'dsNomeArquivoRetorno'.
     * 
     * @return String
     * @return the value of field 'dsNomeArquivoRetorno'.
     */
    public java.lang.String getDsNomeArquivoRetorno()
    {
        return this._dsNomeArquivoRetorno;
    } //-- java.lang.String getDsNomeArquivoRetorno() 

    /**
     * Returns the value of field 'dsPerdcContagemRemessa'.
     * 
     * @return String
     * @return the value of field 'dsPerdcContagemRemessa'.
     */
    public java.lang.String getDsPerdcContagemRemessa()
    {
        return this._dsPerdcContagemRemessa;
    } //-- java.lang.String getDsPerdcContagemRemessa() 

    /**
     * Returns the value of field 'dsRejeicaoAcolhimentoRemessa'.
     * 
     * @return String
     * @return the value of field 'dsRejeicaoAcolhimentoRemessa'.
     */
    public java.lang.String getDsRejeicaoAcolhimentoRemessa()
    {
        return this._dsRejeicaoAcolhimentoRemessa;
    } //-- java.lang.String getDsRejeicaoAcolhimentoRemessa() 

    /**
     * Returns the value of field 'dsTipoLayoutArquivo'.
     * 
     * @return String
     * @return the value of field 'dsTipoLayoutArquivo'.
     */
    public java.lang.String getDsTipoLayoutArquivo()
    {
        return this._dsTipoLayoutArquivo;
    } //-- java.lang.String getDsTipoLayoutArquivo() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'nrMaximoContagemRemessa'.
     * 
     * @return long
     * @return the value of field 'nrMaximoContagemRemessa'.
     */
    public long getNrMaximoContagemRemessa()
    {
        return this._nrMaximoContagemRemessa;
    } //-- long getNrMaximoContagemRemessa() 

    /**
     * Method hasCdAplicacaoTransmissaoArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAplicacaoTransmissaoArquivo()
    {
        return this._has_cdAplicacaoTransmissaoArquivo;
    } //-- boolean hasCdAplicacaoTransmissaoArquivo() 

    /**
     * Method hasCdControleNumeroRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleNumeroRemessa()
    {
        return this._has_cdControleNumeroRemessa;
    } //-- boolean hasCdControleNumeroRemessa() 

    /**
     * Method hasCdNivelControleRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelControleRemessa()
    {
        return this._has_cdNivelControleRemessa;
    } //-- boolean hasCdNivelControleRemessa() 

    /**
     * Method hasCdPerdcContagemRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerdcContagemRemessa()
    {
        return this._has_cdPerdcContagemRemessa;
    } //-- boolean hasCdPerdcContagemRemessa() 

    /**
     * Method hasCdRejeicaoAcolhimentoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRejeicaoAcolhimentoRemessa()
    {
        return this._has_cdRejeicaoAcolhimentoRemessa;
    } //-- boolean hasCdRejeicaoAcolhimentoRemessa() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrMaximoContagemRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrMaximoContagemRemessa()
    {
        return this._has_nrMaximoContagemRemessa;
    } //-- boolean hasNrMaximoContagemRemessa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAplicacaoTransmissaoArquivo'.
     * 
     * @param cdAplicacaoTransmissaoArquivo the value of field
     * 'cdAplicacaoTransmissaoArquivo'.
     */
    public void setCdAplicacaoTransmissaoArquivo(int cdAplicacaoTransmissaoArquivo)
    {
        this._cdAplicacaoTransmissaoArquivo = cdAplicacaoTransmissaoArquivo;
        this._has_cdAplicacaoTransmissaoArquivo = true;
    } //-- void setCdAplicacaoTransmissaoArquivo(int) 

    /**
     * Sets the value of field 'cdControleNumeroRemessa'.
     * 
     * @param cdControleNumeroRemessa the value of field
     * 'cdControleNumeroRemessa'.
     */
    public void setCdControleNumeroRemessa(int cdControleNumeroRemessa)
    {
        this._cdControleNumeroRemessa = cdControleNumeroRemessa;
        this._has_cdControleNumeroRemessa = true;
    } //-- void setCdControleNumeroRemessa(int) 

    /**
     * Sets the value of field 'cdNivelControleRemessa'.
     * 
     * @param cdNivelControleRemessa the value of field
     * 'cdNivelControleRemessa'.
     */
    public void setCdNivelControleRemessa(int cdNivelControleRemessa)
    {
        this._cdNivelControleRemessa = cdNivelControleRemessa;
        this._has_cdNivelControleRemessa = true;
    } //-- void setCdNivelControleRemessa(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPercentualReheicaoRemessa'.
     * 
     * @param cdPercentualReheicaoRemessa the value of field
     * 'cdPercentualReheicaoRemessa'.
     */
    public void setCdPercentualReheicaoRemessa(java.math.BigDecimal cdPercentualReheicaoRemessa)
    {
        this._cdPercentualReheicaoRemessa = cdPercentualReheicaoRemessa;
    } //-- void setCdPercentualReheicaoRemessa(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdPerdcContagemRemessa'.
     * 
     * @param cdPerdcContagemRemessa the value of field
     * 'cdPerdcContagemRemessa'.
     */
    public void setCdPerdcContagemRemessa(int cdPerdcContagemRemessa)
    {
        this._cdPerdcContagemRemessa = cdPerdcContagemRemessa;
        this._has_cdPerdcContagemRemessa = true;
    } //-- void setCdPerdcContagemRemessa(int) 

    /**
     * Sets the value of field 'cdRejeicaoAcolhimentoRemessa'.
     * 
     * @param cdRejeicaoAcolhimentoRemessa the value of field
     * 'cdRejeicaoAcolhimentoRemessa'.
     */
    public void setCdRejeicaoAcolhimentoRemessa(int cdRejeicaoAcolhimentoRemessa)
    {
        this._cdRejeicaoAcolhimentoRemessa = cdRejeicaoAcolhimentoRemessa;
        this._has_cdRejeicaoAcolhimentoRemessa = true;
    } //-- void setCdRejeicaoAcolhimentoRemessa(int) 

    /**
     * Sets the value of field 'cdSituacao'.
     * 
     * @param cdSituacao the value of field 'cdSituacao'.
     */
    public void setCdSituacao(java.lang.String cdSituacao)
    {
        this._cdSituacao = cdSituacao;
    } //-- void setCdSituacao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExter'.
     * 
     * @param cdUsuarioInclusaoExter the value of field
     * 'cdUsuarioInclusaoExter'.
     */
    public void setCdUsuarioInclusaoExter(java.lang.String cdUsuarioInclusaoExter)
    {
        this._cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
    } //-- void setCdUsuarioInclusaoExter(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExter'.
     * 
     * @param cdUsuarioManutencaoExter the value of field
     * 'cdUsuarioManutencaoExter'.
     */
    public void setCdUsuarioManutencaoExter(java.lang.String cdUsuarioManutencaoExter)
    {
        this._cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
    } //-- void setCdUsuarioManutencaoExter(java.lang.String) 

    /**
     * Sets the value of field 'dsAplicacaoTransmissaoArquivo'.
     * 
     * @param dsAplicacaoTransmissaoArquivo the value of field
     * 'dsAplicacaoTransmissaoArquivo'.
     */
    public void setDsAplicacaoTransmissaoArquivo(java.lang.String dsAplicacaoTransmissaoArquivo)
    {
        this._dsAplicacaoTransmissaoArquivo = dsAplicacaoTransmissaoArquivo;
    } //-- void setDsAplicacaoTransmissaoArquivo(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsControleNumeroRemessa'.
     * 
     * @param dsControleNumeroRemessa the value of field
     * 'dsControleNumeroRemessa'.
     */
    public void setDsControleNumeroRemessa(java.lang.String dsControleNumeroRemessa)
    {
        this._dsControleNumeroRemessa = dsControleNumeroRemessa;
    } //-- void setDsControleNumeroRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsNivelControleRemessa'.
     * 
     * @param dsNivelControleRemessa the value of field
     * 'dsNivelControleRemessa'.
     */
    public void setDsNivelControleRemessa(java.lang.String dsNivelControleRemessa)
    {
        this._dsNivelControleRemessa = dsNivelControleRemessa;
    } //-- void setDsNivelControleRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeArquivoRemessa'.
     * 
     * @param dsNomeArquivoRemessa the value of field
     * 'dsNomeArquivoRemessa'.
     */
    public void setDsNomeArquivoRemessa(java.lang.String dsNomeArquivoRemessa)
    {
        this._dsNomeArquivoRemessa = dsNomeArquivoRemessa;
    } //-- void setDsNomeArquivoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsNomeArquivoRetorno'.
     * 
     * @param dsNomeArquivoRetorno the value of field
     * 'dsNomeArquivoRetorno'.
     */
    public void setDsNomeArquivoRetorno(java.lang.String dsNomeArquivoRetorno)
    {
        this._dsNomeArquivoRetorno = dsNomeArquivoRetorno;
    } //-- void setDsNomeArquivoRetorno(java.lang.String) 

    /**
     * Sets the value of field 'dsPerdcContagemRemessa'.
     * 
     * @param dsPerdcContagemRemessa the value of field
     * 'dsPerdcContagemRemessa'.
     */
    public void setDsPerdcContagemRemessa(java.lang.String dsPerdcContagemRemessa)
    {
        this._dsPerdcContagemRemessa = dsPerdcContagemRemessa;
    } //-- void setDsPerdcContagemRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsRejeicaoAcolhimentoRemessa'.
     * 
     * @param dsRejeicaoAcolhimentoRemessa the value of field
     * 'dsRejeicaoAcolhimentoRemessa'.
     */
    public void setDsRejeicaoAcolhimentoRemessa(java.lang.String dsRejeicaoAcolhimentoRemessa)
    {
        this._dsRejeicaoAcolhimentoRemessa = dsRejeicaoAcolhimentoRemessa;
    } //-- void setDsRejeicaoAcolhimentoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoLayoutArquivo'.
     * 
     * @param dsTipoLayoutArquivo the value of field
     * 'dsTipoLayoutArquivo'.
     */
    public void setDsTipoLayoutArquivo(java.lang.String dsTipoLayoutArquivo)
    {
        this._dsTipoLayoutArquivo = dsTipoLayoutArquivo;
    } //-- void setDsTipoLayoutArquivo(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nrMaximoContagemRemessa'.
     * 
     * @param nrMaximoContagemRemessa the value of field
     * 'nrMaximoContagemRemessa'.
     */
    public void setNrMaximoContagemRemessa(long nrMaximoContagemRemessa)
    {
        this._nrMaximoContagemRemessa = nrMaximoContagemRemessa;
        this._has_nrMaximoContagemRemessa = true;
    } //-- void setNrMaximoContagemRemessa(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarconmanlayout.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
