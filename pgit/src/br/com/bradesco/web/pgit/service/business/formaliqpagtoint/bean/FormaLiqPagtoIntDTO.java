/*
 * Nome: br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.formaliqpagtoint.bean;

/**
 * Nome: FormaLiqPagtoIntDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FormaLiqPagtoIntDTO {

	/** Atributo totalRegistros. */
	private int totalRegistros;
	
	/** Atributo codigo. */
	private String codigo;
	
	/** Atributo descricao. */
	private String descricao;
	
	/** Atributo descricaoResumida. */
	private String descricaoResumida;
	
	/** Atributo prioridadeDebito. */
	private String prioridadeDebito;
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo horarioLimite. */
	private String horarioLimite;
	
	/** Atributo margemSeguranca. */
	private String margemSeguranca;
	
	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}
	
	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	
	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * Get: descricao.
	 *
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	
	/**
	 * Set: descricao.
	 *
	 * @param descricao the descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Get: descricaoResumida.
	 *
	 * @return descricaoResumida
	 */
	public String getDescricaoResumida() {
		return descricaoResumida;
	}
	
	/**
	 * Set: descricaoResumida.
	 *
	 * @param descricaoResumida the descricao resumida
	 */
	public void setDescricaoResumida(String descricaoResumida) {
		this.descricaoResumida = descricaoResumida;
	}
	
	/**
	 * Get: horarioLimite.
	 *
	 * @return horarioLimite
	 */
	public String getHorarioLimite() {
		return horarioLimite;
	}
	
	/**
	 * Set: horarioLimite.
	 *
	 * @param horarioLimite the horario limite
	 */
	public void setHorarioLimite(String horarioLimite) {
		this.horarioLimite = horarioLimite;
	}
	
	/**
	 * Get: margemSeguranca.
	 *
	 * @return margemSeguranca
	 */
	public String getMargemSeguranca() {
		return margemSeguranca;
	}
	
	/**
	 * Set: margemSeguranca.
	 *
	 * @param margemSeguranca the margem seguranca
	 */
	public void setMargemSeguranca(String margemSeguranca) {
		this.margemSeguranca = margemSeguranca;
	}
	
	/**
	 * Get: prioridadeDebito.
	 *
	 * @return prioridadeDebito
	 */
	public String getPrioridadeDebito() {
		return prioridadeDebito;
	}
	
	/**
	 * Set: prioridadeDebito.
	 *
	 * @param prioridadeDebito the prioridade debito
	 */
	public void setPrioridadeDebito(String prioridadeDebito) {
		this.prioridadeDebito = prioridadeDebito;
	}
	
	/**
	 * Get: totalRegistros.
	 *
	 * @return totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}
	
	/**
	 * Set: totalRegistros.
	 *
	 * @param totalRegistros the total registros
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	
	
}
