/*
 * Nome: br.com.bradesco.web.pgit.service.business.endereco.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.endereco.bean;

import br.com.bradesco.web.pgit.utils.SiteUtil;



/**
 * Nome: DatalharEnderecoParticipanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DatalharEnderecoParticipanteSaidaDTO {

    /** Atributo codMensagem. */
    private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo cdEspeceEndereco. */
    private Integer cdEspeceEndereco;

    /** Atributo dsEspecieEndereco. */
    private String dsEspecieEndereco;

    /** Atributo cdNomeRazao. */
    private String cdNomeRazao;

    /** Atributo dsLogradouroPagador. */
    private String dsLogradouroPagador;

    /** Atributo dsNumeroLogradouroPagador. */
    private String dsNumeroLogradouroPagador;

    /** Atributo dsComplementoLogradouroPagador. */
    private String dsComplementoLogradouroPagador;

    /** Atributo dsBairroClientePagador. */
    private String dsBairroClientePagador;

    /** Atributo dsMunicipioClientePagador. */
    private String dsMunicipioClientePagador;

    /** Atributo cdSiglaUfPagador. */
    private String cdSiglaUfPagador;

    /** Atributo dsPais. */
    private String dsPais;

    /** Atributo cdCepPagador. */
    private Integer cdCepPagador;

    /** Atributo cdCepComplementoPagador. */
    private Integer cdCepComplementoPagador;

    /** Atributo dsCpfCnpjRecebedor. */
    private Long dsCpfCnpjRecebedor;

    /** Atributo dsFilialCnpjRecebedor. */
    private Integer dsFilialCnpjRecebedor;

    /** Atributo cdUsoPostal. */
    private Integer cdUsoPostal;

    /** Atributo dsControleCpfRecebedor. */
    private Integer dsControleCpfRecebedor;

    /** Atributo dsNomeRazao. */
    private String dsNomeRazao;

    /** Atributo cdEnderecoEletronico. */
    private String cdEnderecoEletronico;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;

    /** Atributo cdUsuarioInclusaoExterno. */
    private String cdUsuarioInclusaoExterno;

    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;

    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;

    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;

    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;

    /** Atributo cdUsuarioManutencaoexterno. */
    private String cdUsuarioManutencaoexterno;

    /** Atributo hrManutencaoRegistro. */
    private String hrManutencaoRegistro;

    /** Atributo cdOperacaoCanalManutencao. */
    private String cdOperacaoCanalManutencao;

    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo nrSequencialEndereco. */
    private Integer nrSequencialEndereco;

    /** Atributo cdTipo. */
    private String cdTipo;

    /** Atributo dsTipoCanalInclusao. */
    private String dsTipoCanalInclusao;

    /** Atributo dsTipoCanalManutencao. */
    private String dsTipoCanalManutencao;

    /**
     * Set: codMensagem.
     *
     * @param codMensagem the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Get: codMensagem.
     *
     * @return codMensagem
     */
    public String getCodMensagem() {
        return this.codMensagem;
    }

    /**
     * Set: mensagem.
     *
     * @param mensagem the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get: mensagem.
     *
     * @return mensagem
     */
    public String getMensagem() {
        return this.mensagem;
    }

    /**
     * Set: cdUsuarioInclusao.
     *
     * @param cdUsuarioInclusao the cd usuario inclusao
     */
    public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
        this.cdUsuarioInclusao = cdUsuarioInclusao;
    }

    /**
     * Get: cdUsuarioInclusao.
     *
     * @return cdUsuarioInclusao
     */
    public String getCdUsuarioInclusao() {
        return this.cdUsuarioInclusao;
    }

    /**
     * Set: cdUsuarioInclusaoExterno.
     *
     * @param cdUsuarioInclusaoExterno the cd usuario inclusao externo
     */
    public void setCdUsuarioInclusaoExterno(String cdUsuarioInclusaoExterno) {
        this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    }

    /**
     * Get: cdUsuarioInclusaoExterno.
     *
     * @return cdUsuarioInclusaoExterno
     */
    public String getCdUsuarioInclusaoExterno() {
        return this.cdUsuarioInclusaoExterno;
    }

    /**
     * Set: hrInclusaoRegistro.
     *
     * @param hrInclusaoRegistro the hr inclusao registro
     */
    public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
        this.hrInclusaoRegistro = hrInclusaoRegistro;
    }

    /**
     * Get: hrInclusaoRegistro.
     *
     * @return hrInclusaoRegistro
     */
    public String getHrInclusaoRegistro() {
        return this.hrInclusaoRegistro;
    }

    /**
     * Set: cdOperacaoCanalInclusao.
     *
     * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
     */
    public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
        this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    }

    /**
     * Get: cdOperacaoCanalInclusao.
     *
     * @return cdOperacaoCanalInclusao
     */
    public String getCdOperacaoCanalInclusao() {
        return this.cdOperacaoCanalInclusao;
    }

    /**
     * Set: cdTipoCanalInclusao.
     *
     * @param cdTipoCanalInclusao the cd tipo canal inclusao
     */
    public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
        this.cdTipoCanalInclusao = cdTipoCanalInclusao;
    }

    /**
     * Get: cdTipoCanalInclusao.
     *
     * @return cdTipoCanalInclusao
     */
    public Integer getCdTipoCanalInclusao() {
        return this.cdTipoCanalInclusao;
    }

    /**
     * Set: cdUsuarioManutencao.
     *
     * @param cdUsuarioManutencao the cd usuario manutencao
     */
    public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
        this.cdUsuarioManutencao = cdUsuarioManutencao;
    }

    /**
     * Get: cdUsuarioManutencao.
     *
     * @return cdUsuarioManutencao
     */
    public String getCdUsuarioManutencao() {
        return this.cdUsuarioManutencao;
    }

    /**
     * Set: cdUsuarioManutencaoexterno.
     *
     * @param cdUsuarioManutencaoexterno the cd usuario manutencaoexterno
     */
    public void setCdUsuarioManutencaoexterno(String cdUsuarioManutencaoexterno) {
        this.cdUsuarioManutencaoexterno = cdUsuarioManutencaoexterno;
    }

    /**
     * Get: cdUsuarioManutencaoexterno.
     *
     * @return cdUsuarioManutencaoexterno
     */
    public String getCdUsuarioManutencaoexterno() {
        return this.cdUsuarioManutencaoexterno;
    }

    /**
     * Set: hrManutencaoRegistro.
     *
     * @param hrManutencaoRegistro the hr manutencao registro
     */
    public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
        this.hrManutencaoRegistro = hrManutencaoRegistro;
    }

    /**
     * Get: hrManutencaoRegistro.
     *
     * @return hrManutencaoRegistro
     */
    public String getHrManutencaoRegistro() {
        return this.hrManutencaoRegistro;
    }

    /**
     * Set: cdOperacaoCanalManutencao.
     *
     * @param cdOperacaoCanalManutencao the cd operacao canal manutencao
     */
    public void setCdOperacaoCanalManutencao(String cdOperacaoCanalManutencao) {
        this.cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    }

    /**
     * Get: cdOperacaoCanalManutencao.
     *
     * @return cdOperacaoCanalManutencao
     */
    public String getCdOperacaoCanalManutencao() {
        return this.cdOperacaoCanalManutencao;
    }

    /**
     * Set: cdTipoCanalManutencao.
     *
     * @param cdTipoCanalManutencao the cd tipo canal manutencao
     */
    public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
        this.cdTipoCanalManutencao = cdTipoCanalManutencao;
    }

    /**
     * Get: cdTipoCanalManutencao.
     *
     * @return cdTipoCanalManutencao
     */
    public Integer getCdTipoCanalManutencao() {
        return this.cdTipoCanalManutencao;
    }

	/**
	 * Get: cdEspeceEndereco.
	 *
	 * @return cdEspeceEndereco
	 */
	public Integer getCdEspeceEndereco() {
		return cdEspeceEndereco;
	}

	/**
	 * Set: cdEspeceEndereco.
	 *
	 * @param cdEspeceEndereco the cd espece endereco
	 */
	public void setCdEspeceEndereco(Integer cdEspeceEndereco) {
		this.cdEspeceEndereco = cdEspeceEndereco;
	}

	/**
	 * Get: dsEspecieEndereco.
	 *
	 * @return dsEspecieEndereco
	 */
	public String getDsEspecieEndereco() {
		return dsEspecieEndereco;
	}

	/**
	 * Set: dsEspecieEndereco.
	 *
	 * @param dsEspecieEndereco the ds especie endereco
	 */
	public void setDsEspecieEndereco(String dsEspecieEndereco) {
		this.dsEspecieEndereco = dsEspecieEndereco;
	}

	/**
	 * Get: cdNomeRazao.
	 *
	 * @return cdNomeRazao
	 */
	public String getCdNomeRazao() {
		return cdNomeRazao;
	}

	/**
	 * Set: cdNomeRazao.
	 *
	 * @param cdNomeRazao the cd nome razao
	 */
	public void setCdNomeRazao(String cdNomeRazao) {
		this.cdNomeRazao = cdNomeRazao;
	}

	/**
	 * Get: dsLogradouroPagador.
	 *
	 * @return dsLogradouroPagador
	 */
	public String getDsLogradouroPagador() {
		return dsLogradouroPagador;
	}

	/**
	 * Set: dsLogradouroPagador.
	 *
	 * @param dsLogradouroPagador the ds logradouro pagador
	 */
	public void setDsLogradouroPagador(String dsLogradouroPagador) {
		this.dsLogradouroPagador = dsLogradouroPagador;
	}

	/**
	 * Get: dsNumeroLogradouroPagador.
	 *
	 * @return dsNumeroLogradouroPagador
	 */
	public String getDsNumeroLogradouroPagador() {
		return dsNumeroLogradouroPagador;
	}

	/**
	 * Set: dsNumeroLogradouroPagador.
	 *
	 * @param dsNumeroLogradouroPagador the ds numero logradouro pagador
	 */
	public void setDsNumeroLogradouroPagador(String dsNumeroLogradouroPagador) {
		this.dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
	}

	/**
	 * Get: dsComplementoLogradouroPagador.
	 *
	 * @return dsComplementoLogradouroPagador
	 */
	public String getDsComplementoLogradouroPagador() {
		return dsComplementoLogradouroPagador;
	}

	/**
	 * Set: dsComplementoLogradouroPagador.
	 *
	 * @param dsComplementoLogradouroPagador the ds complemento logradouro pagador
	 */
	public void setDsComplementoLogradouroPagador(
			String dsComplementoLogradouroPagador) {
		this.dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
	}

	/**
	 * Get: dsBairroClientePagador.
	 *
	 * @return dsBairroClientePagador
	 */
	public String getDsBairroClientePagador() {
		return dsBairroClientePagador;
	}

	/**
	 * Set: dsBairroClientePagador.
	 *
	 * @param dsBairroClientePagador the ds bairro cliente pagador
	 */
	public void setDsBairroClientePagador(String dsBairroClientePagador) {
		this.dsBairroClientePagador = dsBairroClientePagador;
	}

	/**
	 * Get: dsMunicipioClientePagador.
	 *
	 * @return dsMunicipioClientePagador
	 */
	public String getDsMunicipioClientePagador() {
		return dsMunicipioClientePagador;
	}

	/**
	 * Set: dsMunicipioClientePagador.
	 *
	 * @param dsMunicipioClientePagador the ds municipio cliente pagador
	 */
	public void setDsMunicipioClientePagador(String dsMunicipioClientePagador) {
		this.dsMunicipioClientePagador = dsMunicipioClientePagador;
	}

	/**
	 * Get: cdSiglaUfPagador.
	 *
	 * @return cdSiglaUfPagador
	 */
	public String getCdSiglaUfPagador() {
		return cdSiglaUfPagador;
	}

	/**
	 * Set: cdSiglaUfPagador.
	 *
	 * @param cdSiglaUfPagador the cd sigla uf pagador
	 */
	public void setCdSiglaUfPagador(String cdSiglaUfPagador) {
		this.cdSiglaUfPagador = cdSiglaUfPagador;
	}

	/**
	 * Get: dsPais.
	 *
	 * @return dsPais
	 */
	public String getDsPais() {
		return dsPais;
	}

	/**
	 * Set: dsPais.
	 *
	 * @param dsPais the ds pais
	 */
	public void setDsPais(String dsPais) {
		this.dsPais = dsPais;
	}

	/**
	 * Get: cdCepPagador.
	 *
	 * @return cdCepPagador
	 */
	public Integer getCdCepPagador() {
		return cdCepPagador;
	}

	/**
	 * Set: cdCepPagador.
	 *
	 * @param cdCepPagador the cd cep pagador
	 */
	public void setCdCepPagador(Integer cdCepPagador) {
		this.cdCepPagador = cdCepPagador;
	}

	/**
	 * Get: cdCepComplementoPagador.
	 *
	 * @return cdCepComplementoPagador
	 */
	public Integer getCdCepComplementoPagador() {
		return cdCepComplementoPagador;
	}

	/**
	 * Set: cdCepComplementoPagador.
	 *
	 * @param cdCepComplementoPagador the cd cep complemento pagador
	 */
	public void setCdCepComplementoPagador(Integer cdCepComplementoPagador) {
		this.cdCepComplementoPagador = cdCepComplementoPagador;
	}

	/**
	 * Get: dsCpfCnpjRecebedor.
	 *
	 * @return dsCpfCnpjRecebedor
	 */
	public Long getDsCpfCnpjRecebedor() {
		return dsCpfCnpjRecebedor;
	}

	/**
	 * Set: dsCpfCnpjRecebedor.
	 *
	 * @param dsCpfCnpjRecebedor the ds cpf cnpj recebedor
	 */
	public void setDsCpfCnpjRecebedor(Long dsCpfCnpjRecebedor) {
		this.dsCpfCnpjRecebedor = dsCpfCnpjRecebedor;
	}

	/**
	 * Get: dsFilialCnpjRecebedor.
	 *
	 * @return dsFilialCnpjRecebedor
	 */
	public Integer getDsFilialCnpjRecebedor() {
		return dsFilialCnpjRecebedor;
	}

	/**
	 * Set: dsFilialCnpjRecebedor.
	 *
	 * @param dsFilialCnpjRecebedor the ds filial cnpj recebedor
	 */
	public void setDsFilialCnpjRecebedor(Integer dsFilialCnpjRecebedor) {
		this.dsFilialCnpjRecebedor = dsFilialCnpjRecebedor;
	}

	/**
	 * Get: dsControleCpfRecebedor.
	 *
	 * @return dsControleCpfRecebedor
	 */
	public Integer getDsControleCpfRecebedor() {
		return dsControleCpfRecebedor;
	}

	/**
	 * Set: dsControleCpfRecebedor.
	 *
	 * @param dsControleCpfRecebedor the ds controle cpf recebedor
	 */
	public void setDsControleCpfRecebedor(Integer dsControleCpfRecebedor) {
		this.dsControleCpfRecebedor = dsControleCpfRecebedor;
	}

	/**
	 * Get: dsNomeRazao.
	 *
	 * @return dsNomeRazao
	 */
	public String getDsNomeRazao() {
		return dsNomeRazao;
	}

	/**
	 * Set: dsNomeRazao.
	 *
	 * @param dsNomeRazao the ds nome razao
	 */
	public void setDsNomeRazao(String dsNomeRazao) {
		this.dsNomeRazao = dsNomeRazao;
	}

	/**
	 * Get: cdEnderecoEletronico.
	 *
	 * @return cdEnderecoEletronico
	 */
	public String getCdEnderecoEletronico() {
		return cdEnderecoEletronico;
	}

	/**
	 * Set: cdEnderecoEletronico.
	 *
	 * @param cdEnderecoEletronico the cd endereco eletronico
	 */
	public void setCdEnderecoEletronico(String cdEnderecoEletronico) {
		this.cdEnderecoEletronico = cdEnderecoEletronico;
	}

	/**
	 * Get: cdTipo.
	 *
	 * @return cdTipo
	 */
	public String getCdTipo() {
		return cdTipo;
	}

	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(String cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Get: dsEndereco.
	 *
	 * @return dsEndereco
	 */
	public String getDsEndereco() {
		StringBuilder dsEndereco = new StringBuilder();
		if (TipoEnderecoEnum.EMAIL.toString().equals(getCdTipo())) {
			dsEndereco.append(getCdEnderecoEletronico());
		} else if (TipoEnderecoEnum.CORREIO.toString().equals(getCdTipo())) {
			dsEndereco.append(getDsEnderecoEnd());
		}
		return dsEndereco.toString();
	}

	/**
	 * Get: dsEnderecoEnd.
	 *
	 * @return dsEnderecoEnd
	 */
	private String getDsEnderecoEnd() {
		StringBuilder end = new StringBuilder();
    	if (!SiteUtil.isEmptyOrNull(dsLogradouroPagador)) {
    		end.append(dsLogradouroPagador);
        	end.append(" ");
    	}
    	if (!SiteUtil.isEmptyOrNull(dsNumeroLogradouroPagador)) {
    		end.append(dsNumeroLogradouroPagador);
        	end.append(" ");
    	}
    	if (!SiteUtil.isEmptyOrNull(dsComplementoLogradouroPagador)) {
    		end.append(dsComplementoLogradouroPagador);
        	end.append(" ");
    	}
    	if (!SiteUtil.isEmptyOrNull(dsBairroClientePagador)) {
    		end.append(dsBairroClientePagador);
        	end.append(" ");	
    	}
    	if (!SiteUtil.isEmptyOrNull(dsMunicipioClientePagador)) {
    		end.append(dsMunicipioClientePagador);
        	end.append(" ");	
    	}
    	if (!SiteUtil.isEmptyOrNull(cdSiglaUfPagador)) {
    		end.append(cdSiglaUfPagador);
        	end.append(" ");	
    	}
    	if (cdCepPagador != null && cdCepPagador != 0) {
    		end.append(cdCepPagador);
        	end.append(" ");	
    	}
    	if (!SiteUtil.isEmptyOrNull(dsPais)) {
    		end.append(dsPais);
    	}
		return end.toString();
	}

	/**
	 * Get: cdUsoPostal.
	 *
	 * @return cdUsoPostal
	 */
	public Integer getCdUsoPostal() {
		return cdUsoPostal;
	}

	/**
	 * Set: cdUsoPostal.
	 *
	 * @param cdUsoPostal the cd uso postal
	 */
	public void setCdUsoPostal(Integer cdUsoPostal) {
		this.cdUsoPostal = cdUsoPostal;
	}

	/**
	 * Get: nrSequencialEndereco.
	 *
	 * @return nrSequencialEndereco
	 */
	public Integer getNrSequencialEndereco() {
		return nrSequencialEndereco;
	}

	/**
	 * Set: nrSequencialEndereco.
	 *
	 * @param nrSequencialEndereco the nr sequencial endereco
	 */
	public void setNrSequencialEndereco(Integer nrSequencialEndereco) {
		this.nrSequencialEndereco = nrSequencialEndereco;
	}

	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}

	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	/**
	 * Get: dsTipoCanalManutencao.
	 *
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}

	/**
	 * Set: dsTipoCanalManutencao.
	 *
	 * @param dsTipoCanalManutencao the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}
}