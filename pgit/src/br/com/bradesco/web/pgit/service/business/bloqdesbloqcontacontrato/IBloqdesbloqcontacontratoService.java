/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato;

import br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.bean.BloquearDesbloquearContaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.bean.BloquearDesbloquearContaContratoSaidaDTO;
 
/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: Bloqdesbloqcontacontrato
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IBloqdesbloqcontacontratoService {
		
	/**
	 * Bloquear desbloquear conta contrato.
	 *
	 * @param entradaDTO the entrada dto
	 * @return the bloquear desbloquear conta contrato saida dto
	 */
	BloquearDesbloquearContaContratoSaidaDTO bloquearDesbloquearContaContrato(BloquearDesbloquearContaContratoEntradaDTO entradaDTO);

}

