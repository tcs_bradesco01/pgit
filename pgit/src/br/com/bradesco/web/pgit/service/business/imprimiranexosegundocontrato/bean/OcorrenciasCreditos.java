/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * Nome: OcorrenciasCreditos
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasCreditos {
	
	/** Atributo dsCretipoServico. */
	private String dsCretipoServico;
    
    /** Atributo dsCreTipoConsultaSaldo. */
    private String dsCreTipoConsultaSaldo;
    
    /** Atributo dsCreTipoProcessamento. */
    private String dsCreTipoProcessamento;
    
    /** Atributo dsCreTipoTratamentoFeriado. */
    private String dsCreTipoTratamentoFeriado;
    
    /** Atributo dsCreTipoRejeicaoEfetivacao. */
    private String dsCreTipoRejeicaoEfetivacao;
    
    /** Atributo dsCreTipoRejeicaoAgendada. */
    private String dsCreTipoRejeicaoAgendada;
    
    /** Atributo cdCreIndicadorPercentualMaximo. */
    private String cdCreIndicadorPercentualMaximo;
    
    /** Atributo pcCreMaximoInconsistente. */
    private Integer pcCreMaximoInconsistente;
    
    /** Atributo cdCreIndicadorQuantidadeMaxima. */
    private String cdCreIndicadorQuantidadeMaxima;
    
    /** Atributo qtCreMaximaInconsistencia. */
    private Integer qtCreMaximaInconsistencia;
    
    /** Atributo cdCreIndicadorValorMaximo. */
    private String cdCreIndicadorValorMaximo;
    
    /** Atributo vlCreMaximoFavorecidoNao. */
    private BigDecimal vlCreMaximoFavorecidoNao;
    
    /** Atributo dsCrePrioridadeDebito. */
    private String dsCrePrioridadeDebito;
    
    /** Atributo cdCreIndicadorValorDia. */
    private String cdCreIndicadorValorDia;
    
    /** Atributo vlCreLimiteDiario. */
    private BigDecimal vlCreLimiteDiario;
    
    /** Atributo cdCreIndicadorValorIndividual. */
    private String cdCreIndicadorValorIndividual;
    
    /** Atributo vlCreLimiteIndividual. */
    private BigDecimal vlCreLimiteIndividual;
    
    /** Atributo cdCreIndicadorQuantidadeFloating. */
    private String cdCreIndicadorQuantidadeFloating;
    
    /** Atributo qtCreDiaFloating. */
    private Integer qtCreDiaFloating;
    
    /** Atributo cdCreIndicadorCadastroFavorecido. */
    private String cdCreIndicadorCadastroFavorecido;
    
    /** Atributo cdCreUtilizacaoCadastroFavorecido. */
    private String cdCreUtilizacaoCadastroFavorecido;
    
    /** Atributo cdCreIndicadorQuantidadeRepique. */
    private String cdCreIndicadorQuantidadeRepique;
    
    /** Atributo qtCreDiaRepique. */
    private Integer qtCreDiaRepique;
    
    /** Atributo dsCreTipoConsistenciaFavorecido. */
    private String dsCreTipoConsistenciaFavorecido;
    
    /** The cd cre cferi loc. */
    private String cdCreCferiLoc;
    
    /** The cd cre contrato transf. */
    private String cdCreContratoTransf;
    
    /** The cd lcto pgmd. */
    private String cdLctoPgmd;
    
	/**
	 * Get: dsCretipoServico.
	 *
	 * @return dsCretipoServico
	 */
	public String getDsCretipoServico() {
		return dsCretipoServico;
	}
	
	/**
	 * Set: dsCretipoServico.
	 *
	 * @param dsCretipoServico the ds cretipo servico
	 */
	public void setDsCretipoServico(String dsCretipoServico) {
		this.dsCretipoServico = dsCretipoServico;
	}
	
	/**
	 * Get: dsCreTipoConsultaSaldo.
	 *
	 * @return dsCreTipoConsultaSaldo
	 */
	public String getDsCreTipoConsultaSaldo() {
		return dsCreTipoConsultaSaldo;
	}
	
	/**
	 * Set: dsCreTipoConsultaSaldo.
	 *
	 * @param dsCreTipoConsultaSaldo the ds cre tipo consulta saldo
	 */
	public void setDsCreTipoConsultaSaldo(String dsCreTipoConsultaSaldo) {
		this.dsCreTipoConsultaSaldo = dsCreTipoConsultaSaldo;
	}
	
	/**
	 * Get: dsCreTipoProcessamento.
	 *
	 * @return dsCreTipoProcessamento
	 */
	public String getDsCreTipoProcessamento() {
		return dsCreTipoProcessamento;
	}
	
	/**
	 * Set: dsCreTipoProcessamento.
	 *
	 * @param dsCreTipoProcessamento the ds cre tipo processamento
	 */
	public void setDsCreTipoProcessamento(String dsCreTipoProcessamento) {
		this.dsCreTipoProcessamento = dsCreTipoProcessamento;
	}
	
	/**
	 * Get: dsCreTipoTratamentoFeriado.
	 *
	 * @return dsCreTipoTratamentoFeriado
	 */
	public String getDsCreTipoTratamentoFeriado() {
		return dsCreTipoTratamentoFeriado;
	}
	
	/**
	 * Set: dsCreTipoTratamentoFeriado.
	 *
	 * @param dsCreTipoTratamentoFeriado the ds cre tipo tratamento feriado
	 */
	public void setDsCreTipoTratamentoFeriado(String dsCreTipoTratamentoFeriado) {
		this.dsCreTipoTratamentoFeriado = dsCreTipoTratamentoFeriado;
	}
	
	/**
	 * Get: dsCreTipoRejeicaoEfetivacao.
	 *
	 * @return dsCreTipoRejeicaoEfetivacao
	 */
	public String getDsCreTipoRejeicaoEfetivacao() {
		return dsCreTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Set: dsCreTipoRejeicaoEfetivacao.
	 *
	 * @param dsCreTipoRejeicaoEfetivacao the ds cre tipo rejeicao efetivacao
	 */
	public void setDsCreTipoRejeicaoEfetivacao(String dsCreTipoRejeicaoEfetivacao) {
		this.dsCreTipoRejeicaoEfetivacao = dsCreTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Get: dsCreTipoRejeicaoAgendada.
	 *
	 * @return dsCreTipoRejeicaoAgendada
	 */
	public String getDsCreTipoRejeicaoAgendada() {
		return dsCreTipoRejeicaoAgendada;
	}
	
	/**
	 * Set: dsCreTipoRejeicaoAgendada.
	 *
	 * @param dsCreTipoRejeicaoAgendada the ds cre tipo rejeicao agendada
	 */
	public void setDsCreTipoRejeicaoAgendada(String dsCreTipoRejeicaoAgendada) {
		this.dsCreTipoRejeicaoAgendada = dsCreTipoRejeicaoAgendada;
	}
	
	/**
	 * Get: cdCreIndicadorPercentualMaximo.
	 *
	 * @return cdCreIndicadorPercentualMaximo
	 */
	public String getCdCreIndicadorPercentualMaximo() {
		return cdCreIndicadorPercentualMaximo;
	}
	
	/**
	 * Set: cdCreIndicadorPercentualMaximo.
	 *
	 * @param cdCreIndicadorPercentualMaximo the cd cre indicador percentual maximo
	 */
	public void setCdCreIndicadorPercentualMaximo(
			String cdCreIndicadorPercentualMaximo) {
		this.cdCreIndicadorPercentualMaximo = cdCreIndicadorPercentualMaximo;
	}
	
	/**
	 * Get: pcCreMaximoInconsistente.
	 *
	 * @return pcCreMaximoInconsistente
	 */
	public Integer getPcCreMaximoInconsistente() {
		return pcCreMaximoInconsistente;
	}
	
	/**
	 * Set: pcCreMaximoInconsistente.
	 *
	 * @param pcCreMaximoInconsistente the pc cre maximo inconsistente
	 */
	public void setPcCreMaximoInconsistente(Integer pcCreMaximoInconsistente) {
		this.pcCreMaximoInconsistente = pcCreMaximoInconsistente;
	}
	
	/**
	 * Get: cdCreIndicadorQuantidadeMaxima.
	 *
	 * @return cdCreIndicadorQuantidadeMaxima
	 */
	public String getCdCreIndicadorQuantidadeMaxima() {
		return cdCreIndicadorQuantidadeMaxima;
	}
	
	/**
	 * Set: cdCreIndicadorQuantidadeMaxima.
	 *
	 * @param cdCreIndicadorQuantidadeMaxima the cd cre indicador quantidade maxima
	 */
	public void setCdCreIndicadorQuantidadeMaxima(
			String cdCreIndicadorQuantidadeMaxima) {
		this.cdCreIndicadorQuantidadeMaxima = cdCreIndicadorQuantidadeMaxima;
	}
	
	/**
	 * Get: qtCreMaximaInconsistencia.
	 *
	 * @return qtCreMaximaInconsistencia
	 */
	public Integer getQtCreMaximaInconsistencia() {
		return qtCreMaximaInconsistencia;
	}
	
	/**
	 * Set: qtCreMaximaInconsistencia.
	 *
	 * @param qtCreMaximaInconsistencia the qt cre maxima inconsistencia
	 */
	public void setQtCreMaximaInconsistencia(Integer qtCreMaximaInconsistencia) {
		this.qtCreMaximaInconsistencia = qtCreMaximaInconsistencia;
	}
	
	/**
	 * Get: cdCreIndicadorValorMaximo.
	 *
	 * @return cdCreIndicadorValorMaximo
	 */
	public String getCdCreIndicadorValorMaximo() {
		return cdCreIndicadorValorMaximo;
	}
	
	/**
	 * Set: cdCreIndicadorValorMaximo.
	 *
	 * @param cdCreIndicadorValorMaximo the cd cre indicador valor maximo
	 */
	public void setCdCreIndicadorValorMaximo(String cdCreIndicadorValorMaximo) {
		this.cdCreIndicadorValorMaximo = cdCreIndicadorValorMaximo;
	}
	
	/**
	 * Get: vlCreMaximoFavorecidoNao.
	 *
	 * @return vlCreMaximoFavorecidoNao
	 */
	public BigDecimal getVlCreMaximoFavorecidoNao() {
		return vlCreMaximoFavorecidoNao;
	}
	
	/**
	 * Set: vlCreMaximoFavorecidoNao.
	 *
	 * @param vlCreMaximoFavorecidoNao the vl cre maximo favorecido nao
	 */
	public void setVlCreMaximoFavorecidoNao(BigDecimal vlCreMaximoFavorecidoNao) {
		this.vlCreMaximoFavorecidoNao = vlCreMaximoFavorecidoNao;
	}
	
	/**
	 * Get: dsCrePrioridadeDebito.
	 *
	 * @return dsCrePrioridadeDebito
	 */
	public String getDsCrePrioridadeDebito() {
		return dsCrePrioridadeDebito;
	}
	
	/**
	 * Set: dsCrePrioridadeDebito.
	 *
	 * @param dsCrePrioridadeDebito the ds cre prioridade debito
	 */
	public void setDsCrePrioridadeDebito(String dsCrePrioridadeDebito) {
		this.dsCrePrioridadeDebito = dsCrePrioridadeDebito;
	}
	
	/**
	 * Get: cdCreIndicadorValorDia.
	 *
	 * @return cdCreIndicadorValorDia
	 */
	public String getCdCreIndicadorValorDia() {
		return cdCreIndicadorValorDia;
	}
	
	/**
	 * Set: cdCreIndicadorValorDia.
	 *
	 * @param cdCreIndicadorValorDia the cd cre indicador valor dia
	 */
	public void setCdCreIndicadorValorDia(String cdCreIndicadorValorDia) {
		this.cdCreIndicadorValorDia = cdCreIndicadorValorDia;
	}
	
	/**
	 * Get: vlCreLimiteDiario.
	 *
	 * @return vlCreLimiteDiario
	 */
	public BigDecimal getVlCreLimiteDiario() {
		return vlCreLimiteDiario;
	}
	
	/**
	 * Set: vlCreLimiteDiario.
	 *
	 * @param vlCreLimiteDiario the vl cre limite diario
	 */
	public void setVlCreLimiteDiario(BigDecimal vlCreLimiteDiario) {
		this.vlCreLimiteDiario = vlCreLimiteDiario;
	}
	
	/**
	 * Get: cdCreIndicadorValorIndividual.
	 *
	 * @return cdCreIndicadorValorIndividual
	 */
	public String getCdCreIndicadorValorIndividual() {
		return cdCreIndicadorValorIndividual;
	}
	
	/**
	 * Set: cdCreIndicadorValorIndividual.
	 *
	 * @param cdCreIndicadorValorIndividual the cd cre indicador valor individual
	 */
	public void setCdCreIndicadorValorIndividual(
			String cdCreIndicadorValorIndividual) {
		this.cdCreIndicadorValorIndividual = cdCreIndicadorValorIndividual;
	}
	
	/**
	 * Get: vlCreLimiteIndividual.
	 *
	 * @return vlCreLimiteIndividual
	 */
	public BigDecimal getVlCreLimiteIndividual() {
		return vlCreLimiteIndividual;
	}
	
	/**
	 * Set: vlCreLimiteIndividual.
	 *
	 * @param vlCreLimiteIndividual the vl cre limite individual
	 */
	public void setVlCreLimiteIndividual(BigDecimal vlCreLimiteIndividual) {
		this.vlCreLimiteIndividual = vlCreLimiteIndividual;
	}
	
	/**
	 * Get: cdCreIndicadorQuantidadeFloating.
	 *
	 * @return cdCreIndicadorQuantidadeFloating
	 */
	public String getCdCreIndicadorQuantidadeFloating() {
		return cdCreIndicadorQuantidadeFloating;
	}
	
	/**
	 * Set: cdCreIndicadorQuantidadeFloating.
	 *
	 * @param cdCreIndicadorQuantidadeFloating the cd cre indicador quantidade floating
	 */
	public void setCdCreIndicadorQuantidadeFloating(
			String cdCreIndicadorQuantidadeFloating) {
		this.cdCreIndicadorQuantidadeFloating = cdCreIndicadorQuantidadeFloating;
	}
	
	/**
	 * Get: qtCreDiaFloating.
	 *
	 * @return qtCreDiaFloating
	 */
	public Integer getQtCreDiaFloating() {
		return qtCreDiaFloating;
	}
	
	/**
	 * Set: qtCreDiaFloating.
	 *
	 * @param qtCreDiaFloating the qt cre dia floating
	 */
	public void setQtCreDiaFloating(Integer qtCreDiaFloating) {
		this.qtCreDiaFloating = qtCreDiaFloating;
	}
	
	/**
	 * Get: cdCreIndicadorCadastroFavorecido.
	 *
	 * @return cdCreIndicadorCadastroFavorecido
	 */
	public String getCdCreIndicadorCadastroFavorecido() {
		return cdCreIndicadorCadastroFavorecido;
	}
	
	/**
	 * Set: cdCreIndicadorCadastroFavorecido.
	 *
	 * @param cdCreIndicadorCadastroFavorecido the cd cre indicador cadastro favorecido
	 */
	public void setCdCreIndicadorCadastroFavorecido(
			String cdCreIndicadorCadastroFavorecido) {
		this.cdCreIndicadorCadastroFavorecido = cdCreIndicadorCadastroFavorecido;
	}
	
	/**
	 * Get: cdCreUtilizacaoCadastroFavorecido.
	 *
	 * @return cdCreUtilizacaoCadastroFavorecido
	 */
	public String getCdCreUtilizacaoCadastroFavorecido() {
		return cdCreUtilizacaoCadastroFavorecido;
	}
	
	/**
	 * Set: cdCreUtilizacaoCadastroFavorecido.
	 *
	 * @param cdCreUtilizacaoCadastroFavorecido the cd cre utilizacao cadastro favorecido
	 */
	public void setCdCreUtilizacaoCadastroFavorecido(
			String cdCreUtilizacaoCadastroFavorecido) {
		this.cdCreUtilizacaoCadastroFavorecido = cdCreUtilizacaoCadastroFavorecido;
	}
	
	/**
	 * Get: cdCreIndicadorQuantidadeRepique.
	 *
	 * @return cdCreIndicadorQuantidadeRepique
	 */
	public String getCdCreIndicadorQuantidadeRepique() {
		return cdCreIndicadorQuantidadeRepique;
	}
	
	/**
	 * Set: cdCreIndicadorQuantidadeRepique.
	 *
	 * @param cdCreIndicadorQuantidadeRepique the cd cre indicador quantidade repique
	 */
	public void setCdCreIndicadorQuantidadeRepique(
			String cdCreIndicadorQuantidadeRepique) {
		this.cdCreIndicadorQuantidadeRepique = cdCreIndicadorQuantidadeRepique;
	}
	
	/**
	 * Get: qtCreDiaRepique.
	 *
	 * @return qtCreDiaRepique
	 */
	public Integer getQtCreDiaRepique() {
		return qtCreDiaRepique;
	}
	
	/**
	 * Set: qtCreDiaRepique.
	 *
	 * @param qtCreDiaRepique the qt cre dia repique
	 */
	public void setQtCreDiaRepique(Integer qtCreDiaRepique) {
		this.qtCreDiaRepique = qtCreDiaRepique;
	}
	
	/**
	 * Get: dsCreTipoConsistenciaFavorecido.
	 *
	 * @return dsCreTipoConsistenciaFavorecido
	 */
	public String getDsCreTipoConsistenciaFavorecido() {
		return dsCreTipoConsistenciaFavorecido;
	}
	
	/**
	 * Set: dsCreTipoConsistenciaFavorecido.
	 *
	 * @param dsCreTipoConsistenciaFavorecido the ds cre tipo consistencia favorecido
	 */
	public void setDsCreTipoConsistenciaFavorecido(
			String dsCreTipoConsistenciaFavorecido) {
		this.dsCreTipoConsistenciaFavorecido = dsCreTipoConsistenciaFavorecido;
	}

	/**
	 * Gets the cd cre cferi loc.
	 *
	 * @return the cdCreCferiLoc
	 */
	public String getCdCreCferiLoc() {
		return cdCreCferiLoc;
	}

	/**
	 * Sets the cd cre cferi loc.
	 *
	 * @param cdCreCferiLoc the cdCreCferiLoc to set
	 */
	public void setCdCreCferiLoc(String cdCreCferiLoc) {
		this.cdCreCferiLoc = cdCreCferiLoc;
	}

	/**
	 * Gets the cd cre contrato transf.
	 *
	 * @return the cdCreContratoTransf
	 */
	public String getCdCreContratoTransf() {
		return cdCreContratoTransf;
	}

	/**
	 * Sets the cd cre contrato transf.
	 *
	 * @param cdCreContratoTransf the cdCreContratoTransf to set
	 */
	public void setCdCreContratoTransf(String cdCreContratoTransf) {
		this.cdCreContratoTransf = cdCreContratoTransf;
	}

	/**
	 * Gets the cd lcto pgmd.
	 *
	 * @return the cdLctoPgmd
	 */
	public String getCdLctoPgmd() {
		return cdLctoPgmd;
	}

	/**
	 * Sets the cd lcto pgmd.
	 *
	 * @param cdLctoPgmd the cdLctoPgmd to set
	 */
	public void setCdLctoPgmd(String cdLctoPgmd) {
		this.cdLctoPgmd = cdLctoPgmd;
	}
    
}
