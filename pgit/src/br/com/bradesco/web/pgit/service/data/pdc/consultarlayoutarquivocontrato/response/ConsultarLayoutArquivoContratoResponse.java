/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarLayoutArquivoContratoResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarLayoutArquivoContratoResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _cdSituacaoLayoutContrato
     */
    private int _cdSituacaoLayoutContrato = 0;

    /**
     * keeps track of state for field: _cdSituacaoLayoutContrato
     */
    private boolean _has_cdSituacaoLayoutContrato;

    /**
     * Field _dsSituacaoLayoutContrato
     */
    private java.lang.String _dsSituacaoLayoutContrato;

    /**
     * Field _cdRespostaCustoEmpr
     */
    private int _cdRespostaCustoEmpr = 0;

    /**
     * keeps track of state for field: _cdRespostaCustoEmpr
     */
    private boolean _has_cdRespostaCustoEmpr;

    /**
     * Field _dsRespostaCustoEmpr
     */
    private java.lang.String _dsRespostaCustoEmpr;

    /**
     * Field _cdPercentualOrgnzTransmissao
     */
    private java.math.BigDecimal _cdPercentualOrgnzTransmissao = new java.math.BigDecimal("0");

    /**
     * Field _cdMensagemLinhaExtrato
     */
    private int _cdMensagemLinhaExtrato = 0;

    /**
     * keeps track of state for field: _cdMensagemLinhaExtrato
     */
    private boolean _has_cdMensagemLinhaExtrato;

    /**
     * Field _nrVersaoLayoutArquivo
     */
    private int _nrVersaoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _nrVersaoLayoutArquivo
     */
    private boolean _has_nrVersaoLayoutArquivo;

    /**
     * Field _cdTipoControlePagamento
     */
    private int _cdTipoControlePagamento = 0;

    /**
     * keeps track of state for field: _cdTipoControlePagamento
     */
    private boolean _has_cdTipoControlePagamento;

    /**
     * Field _dsTipoControlePagamento
     */
    private java.lang.String _dsTipoControlePagamento;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;

    /**
     * Field _cdUsuarioInclusaoExterno
     */
    private java.lang.String _cdUsuarioInclusaoExterno;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdOperacaoCanalInclusao
     */
    private java.lang.String _cdOperacaoCanalInclusao;

    /**
     * Field _cdTipoCanalInclusao
     */
    private int _cdTipoCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalInclusao
     */
    private boolean _has_cdTipoCanalInclusao;

    /**
     * Field _dsTipoCanalInclusao
     */
    private java.lang.String _dsTipoCanalInclusao;

    /**
     * Field _cdUsuarioManutencao
     */
    private java.lang.String _cdUsuarioManutencao;

    /**
     * Field _cdUsuarioManutencaoExterno
     */
    private java.lang.String _cdUsuarioManutencaoExterno;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;

    /**
     * Field _cdOperacaoCanalManutencao
     */
    private java.lang.String _cdOperacaoCanalManutencao;

    /**
     * Field _cdTipoCanalManutencao
     */
    private int _cdTipoCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoCanalManutencao
     */
    private boolean _has_cdTipoCanalManutencao;

    /**
     * Field _dsTipoCanalManutencao
     */
    private java.lang.String _dsTipoCanalManutencao;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarLayoutArquivoContratoResponse() 
     {
        super();
        setCdPercentualOrgnzTransmissao(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response.ConsultarLayoutArquivoContratoResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdMensagemLinhaExtrato
     * 
     */
    public void deleteCdMensagemLinhaExtrato()
    {
        this._has_cdMensagemLinhaExtrato= false;
    } //-- void deleteCdMensagemLinhaExtrato() 

    /**
     * Method deleteCdRespostaCustoEmpr
     * 
     */
    public void deleteCdRespostaCustoEmpr()
    {
        this._has_cdRespostaCustoEmpr= false;
    } //-- void deleteCdRespostaCustoEmpr() 

    /**
     * Method deleteCdSituacaoLayoutContrato
     * 
     */
    public void deleteCdSituacaoLayoutContrato()
    {
        this._has_cdSituacaoLayoutContrato= false;
    } //-- void deleteCdSituacaoLayoutContrato() 

    /**
     * Method deleteCdTipoCanalInclusao
     * 
     */
    public void deleteCdTipoCanalInclusao()
    {
        this._has_cdTipoCanalInclusao= false;
    } //-- void deleteCdTipoCanalInclusao() 

    /**
     * Method deleteCdTipoCanalManutencao
     * 
     */
    public void deleteCdTipoCanalManutencao()
    {
        this._has_cdTipoCanalManutencao= false;
    } //-- void deleteCdTipoCanalManutencao() 

    /**
     * Method deleteCdTipoControlePagamento
     * 
     */
    public void deleteCdTipoControlePagamento()
    {
        this._has_cdTipoControlePagamento= false;
    } //-- void deleteCdTipoControlePagamento() 

    /**
     * Method deleteNrVersaoLayoutArquivo
     * 
     */
    public void deleteNrVersaoLayoutArquivo()
    {
        this._has_nrVersaoLayoutArquivo= false;
    } //-- void deleteNrVersaoLayoutArquivo() 

    /**
     * Returns the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @return int
     * @return the value of field 'cdMensagemLinhaExtrato'.
     */
    public int getCdMensagemLinhaExtrato()
    {
        return this._cdMensagemLinhaExtrato;
    } //-- int getCdMensagemLinhaExtrato() 

    /**
     * Returns the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalInclusao'.
     */
    public java.lang.String getCdOperacaoCanalInclusao()
    {
        return this._cdOperacaoCanalInclusao;
    } //-- java.lang.String getCdOperacaoCanalInclusao() 

    /**
     * Returns the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'cdOperacaoCanalManutencao'.
     */
    public java.lang.String getCdOperacaoCanalManutencao()
    {
        return this._cdOperacaoCanalManutencao;
    } //-- java.lang.String getCdOperacaoCanalManutencao() 

    /**
     * Returns the value of field 'cdPercentualOrgnzTransmissao'.
     * 
     * @return BigDecimal
     * @return the value of field 'cdPercentualOrgnzTransmissao'.
     */
    public java.math.BigDecimal getCdPercentualOrgnzTransmissao()
    {
        return this._cdPercentualOrgnzTransmissao;
    } //-- java.math.BigDecimal getCdPercentualOrgnzTransmissao() 

    /**
     * Returns the value of field 'cdRespostaCustoEmpr'.
     * 
     * @return int
     * @return the value of field 'cdRespostaCustoEmpr'.
     */
    public int getCdRespostaCustoEmpr()
    {
        return this._cdRespostaCustoEmpr;
    } //-- int getCdRespostaCustoEmpr() 

    /**
     * Returns the value of field 'cdSituacaoLayoutContrato'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoLayoutContrato'.
     */
    public int getCdSituacaoLayoutContrato()
    {
        return this._cdSituacaoLayoutContrato;
    } //-- int getCdSituacaoLayoutContrato() 

    /**
     * Returns the value of field 'cdTipoCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalInclusao'.
     */
    public int getCdTipoCanalInclusao()
    {
        return this._cdTipoCanalInclusao;
    } //-- int getCdTipoCanalInclusao() 

    /**
     * Returns the value of field 'cdTipoCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoCanalManutencao'.
     */
    public int getCdTipoCanalManutencao()
    {
        return this._cdTipoCanalManutencao;
    } //-- int getCdTipoCanalManutencao() 

    /**
     * Returns the value of field 'cdTipoControlePagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoControlePagamento'.
     */
    public int getCdTipoControlePagamento()
    {
        return this._cdTipoControlePagamento;
    } //-- int getCdTipoControlePagamento() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusaoExterno'.
     */
    public java.lang.String getCdUsuarioInclusaoExterno()
    {
        return this._cdUsuarioInclusaoExterno;
    } //-- java.lang.String getCdUsuarioInclusaoExterno() 

    /**
     * Returns the value of field 'cdUsuarioManutencao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencao'.
     */
    public java.lang.String getCdUsuarioManutencao()
    {
        return this._cdUsuarioManutencao;
    } //-- java.lang.String getCdUsuarioManutencao() 

    /**
     * Returns the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioManutencaoExterno'.
     */
    public java.lang.String getCdUsuarioManutencaoExterno()
    {
        return this._cdUsuarioManutencaoExterno;
    } //-- java.lang.String getCdUsuarioManutencaoExterno() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'dsRespostaCustoEmpr'.
     * 
     * @return String
     * @return the value of field 'dsRespostaCustoEmpr'.
     */
    public java.lang.String getDsRespostaCustoEmpr()
    {
        return this._dsRespostaCustoEmpr;
    } //-- java.lang.String getDsRespostaCustoEmpr() 

    /**
     * Returns the value of field 'dsSituacaoLayoutContrato'.
     * 
     * @return String
     * @return the value of field 'dsSituacaoLayoutContrato'.
     */
    public java.lang.String getDsSituacaoLayoutContrato()
    {
        return this._dsSituacaoLayoutContrato;
    } //-- java.lang.String getDsSituacaoLayoutContrato() 

    /**
     * Returns the value of field 'dsTipoCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalInclusao'.
     */
    public java.lang.String getDsTipoCanalInclusao()
    {
        return this._dsTipoCanalInclusao;
    } //-- java.lang.String getDsTipoCanalInclusao() 

    /**
     * Returns the value of field 'dsTipoCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsTipoCanalManutencao'.
     */
    public java.lang.String getDsTipoCanalManutencao()
    {
        return this._dsTipoCanalManutencao;
    } //-- java.lang.String getDsTipoCanalManutencao() 

    /**
     * Returns the value of field 'dsTipoControlePagamento'.
     * 
     * @return String
     * @return the value of field 'dsTipoControlePagamento'.
     */
    public java.lang.String getDsTipoControlePagamento()
    {
        return this._dsTipoControlePagamento;
    } //-- java.lang.String getDsTipoControlePagamento() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'nrVersaoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'nrVersaoLayoutArquivo'.
     */
    public int getNrVersaoLayoutArquivo()
    {
        return this._nrVersaoLayoutArquivo;
    } //-- int getNrVersaoLayoutArquivo() 

    /**
     * Method hasCdMensagemLinhaExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemLinhaExtrato()
    {
        return this._has_cdMensagemLinhaExtrato;
    } //-- boolean hasCdMensagemLinhaExtrato() 

    /**
     * Method hasCdRespostaCustoEmpr
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRespostaCustoEmpr()
    {
        return this._has_cdRespostaCustoEmpr;
    } //-- boolean hasCdRespostaCustoEmpr() 

    /**
     * Method hasCdSituacaoLayoutContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoLayoutContrato()
    {
        return this._has_cdSituacaoLayoutContrato;
    } //-- boolean hasCdSituacaoLayoutContrato() 

    /**
     * Method hasCdTipoCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalInclusao()
    {
        return this._has_cdTipoCanalInclusao;
    } //-- boolean hasCdTipoCanalInclusao() 

    /**
     * Method hasCdTipoCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCanalManutencao()
    {
        return this._has_cdTipoCanalManutencao;
    } //-- boolean hasCdTipoCanalManutencao() 

    /**
     * Method hasCdTipoControlePagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoControlePagamento()
    {
        return this._has_cdTipoControlePagamento;
    } //-- boolean hasCdTipoControlePagamento() 

    /**
     * Method hasNrVersaoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrVersaoLayoutArquivo()
    {
        return this._has_nrVersaoLayoutArquivo;
    } //-- boolean hasNrVersaoLayoutArquivo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdMensagemLinhaExtrato'.
     * 
     * @param cdMensagemLinhaExtrato the value of field
     * 'cdMensagemLinhaExtrato'.
     */
    public void setCdMensagemLinhaExtrato(int cdMensagemLinhaExtrato)
    {
        this._cdMensagemLinhaExtrato = cdMensagemLinhaExtrato;
        this._has_cdMensagemLinhaExtrato = true;
    } //-- void setCdMensagemLinhaExtrato(int) 

    /**
     * Sets the value of field 'cdOperacaoCanalInclusao'.
     * 
     * @param cdOperacaoCanalInclusao the value of field
     * 'cdOperacaoCanalInclusao'.
     */
    public void setCdOperacaoCanalInclusao(java.lang.String cdOperacaoCanalInclusao)
    {
        this._cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
    } //-- void setCdOperacaoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdOperacaoCanalManutencao'.
     * 
     * @param cdOperacaoCanalManutencao the value of field
     * 'cdOperacaoCanalManutencao'.
     */
    public void setCdOperacaoCanalManutencao(java.lang.String cdOperacaoCanalManutencao)
    {
        this._cdOperacaoCanalManutencao = cdOperacaoCanalManutencao;
    } //-- void setCdOperacaoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdPercentualOrgnzTransmissao'.
     * 
     * @param cdPercentualOrgnzTransmissao the value of field
     * 'cdPercentualOrgnzTransmissao'.
     */
    public void setCdPercentualOrgnzTransmissao(java.math.BigDecimal cdPercentualOrgnzTransmissao)
    {
        this._cdPercentualOrgnzTransmissao = cdPercentualOrgnzTransmissao;
    } //-- void setCdPercentualOrgnzTransmissao(java.math.BigDecimal) 

    /**
     * Sets the value of field 'cdRespostaCustoEmpr'.
     * 
     * @param cdRespostaCustoEmpr the value of field
     * 'cdRespostaCustoEmpr'.
     */
    public void setCdRespostaCustoEmpr(int cdRespostaCustoEmpr)
    {
        this._cdRespostaCustoEmpr = cdRespostaCustoEmpr;
        this._has_cdRespostaCustoEmpr = true;
    } //-- void setCdRespostaCustoEmpr(int) 

    /**
     * Sets the value of field 'cdSituacaoLayoutContrato'.
     * 
     * @param cdSituacaoLayoutContrato the value of field
     * 'cdSituacaoLayoutContrato'.
     */
    public void setCdSituacaoLayoutContrato(int cdSituacaoLayoutContrato)
    {
        this._cdSituacaoLayoutContrato = cdSituacaoLayoutContrato;
        this._has_cdSituacaoLayoutContrato = true;
    } //-- void setCdSituacaoLayoutContrato(int) 

    /**
     * Sets the value of field 'cdTipoCanalInclusao'.
     * 
     * @param cdTipoCanalInclusao the value of field
     * 'cdTipoCanalInclusao'.
     */
    public void setCdTipoCanalInclusao(int cdTipoCanalInclusao)
    {
        this._cdTipoCanalInclusao = cdTipoCanalInclusao;
        this._has_cdTipoCanalInclusao = true;
    } //-- void setCdTipoCanalInclusao(int) 

    /**
     * Sets the value of field 'cdTipoCanalManutencao'.
     * 
     * @param cdTipoCanalManutencao the value of field
     * 'cdTipoCanalManutencao'.
     */
    public void setCdTipoCanalManutencao(int cdTipoCanalManutencao)
    {
        this._cdTipoCanalManutencao = cdTipoCanalManutencao;
        this._has_cdTipoCanalManutencao = true;
    } //-- void setCdTipoCanalManutencao(int) 

    /**
     * Sets the value of field 'cdTipoControlePagamento'.
     * 
     * @param cdTipoControlePagamento the value of field
     * 'cdTipoControlePagamento'.
     */
    public void setCdTipoControlePagamento(int cdTipoControlePagamento)
    {
        this._cdTipoControlePagamento = cdTipoControlePagamento;
        this._has_cdTipoControlePagamento = true;
    } //-- void setCdTipoControlePagamento(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioInclusaoExterno'.
     * 
     * @param cdUsuarioInclusaoExterno the value of field
     * 'cdUsuarioInclusaoExterno'.
     */
    public void setCdUsuarioInclusaoExterno(java.lang.String cdUsuarioInclusaoExterno)
    {
        this._cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
    } //-- void setCdUsuarioInclusaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencao'.
     * 
     * @param cdUsuarioManutencao the value of field
     * 'cdUsuarioManutencao'.
     */
    public void setCdUsuarioManutencao(java.lang.String cdUsuarioManutencao)
    {
        this._cdUsuarioManutencao = cdUsuarioManutencao;
    } //-- void setCdUsuarioManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdUsuarioManutencaoExterno'.
     * 
     * @param cdUsuarioManutencaoExterno the value of field
     * 'cdUsuarioManutencaoExterno'.
     */
    public void setCdUsuarioManutencaoExterno(java.lang.String cdUsuarioManutencaoExterno)
    {
        this._cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
    } //-- void setCdUsuarioManutencaoExterno(java.lang.String) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'dsRespostaCustoEmpr'.
     * 
     * @param dsRespostaCustoEmpr the value of field
     * 'dsRespostaCustoEmpr'.
     */
    public void setDsRespostaCustoEmpr(java.lang.String dsRespostaCustoEmpr)
    {
        this._dsRespostaCustoEmpr = dsRespostaCustoEmpr;
    } //-- void setDsRespostaCustoEmpr(java.lang.String) 

    /**
     * Sets the value of field 'dsSituacaoLayoutContrato'.
     * 
     * @param dsSituacaoLayoutContrato the value of field
     * 'dsSituacaoLayoutContrato'.
     */
    public void setDsSituacaoLayoutContrato(java.lang.String dsSituacaoLayoutContrato)
    {
        this._dsSituacaoLayoutContrato = dsSituacaoLayoutContrato;
    } //-- void setDsSituacaoLayoutContrato(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalInclusao'.
     * 
     * @param dsTipoCanalInclusao the value of field
     * 'dsTipoCanalInclusao'.
     */
    public void setDsTipoCanalInclusao(java.lang.String dsTipoCanalInclusao)
    {
        this._dsTipoCanalInclusao = dsTipoCanalInclusao;
    } //-- void setDsTipoCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoCanalManutencao'.
     * 
     * @param dsTipoCanalManutencao the value of field
     * 'dsTipoCanalManutencao'.
     */
    public void setDsTipoCanalManutencao(java.lang.String dsTipoCanalManutencao)
    {
        this._dsTipoCanalManutencao = dsTipoCanalManutencao;
    } //-- void setDsTipoCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoControlePagamento'.
     * 
     * @param dsTipoControlePagamento the value of field
     * 'dsTipoControlePagamento'.
     */
    public void setDsTipoControlePagamento(java.lang.String dsTipoControlePagamento)
    {
        this._dsTipoControlePagamento = dsTipoControlePagamento;
    } //-- void setDsTipoControlePagamento(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'nrVersaoLayoutArquivo'.
     * 
     * @param nrVersaoLayoutArquivo the value of field
     * 'nrVersaoLayoutArquivo'.
     */
    public void setNrVersaoLayoutArquivo(int nrVersaoLayoutArquivo)
    {
        this._nrVersaoLayoutArquivo = nrVersaoLayoutArquivo;
        this._has_nrVersaoLayoutArquivo = true;
    } //-- void setNrVersaoLayoutArquivo(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarLayoutArquivoContratoResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response.ConsultarLayoutArquivoContratoResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response.ConsultarLayoutArquivoContratoResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response.ConsultarLayoutArquivoContratoResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlayoutarquivocontrato.response.ConsultarLayoutArquivoContratoResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
