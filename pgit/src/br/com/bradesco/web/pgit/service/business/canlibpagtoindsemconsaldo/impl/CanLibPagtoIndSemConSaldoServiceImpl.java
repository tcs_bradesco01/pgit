/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.ICanLibPagtoIndSemConSaldoService;
import br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.ICanLibPagtoIndSemConSaldoServiceConstants;
import br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.bean.CancelarLibPagtosIndSemConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.bean.CancelarLibPagtosIndSemConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.bean.ConsultarCanLibPagtosindSemConsSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.bean.ConsultarCanLibPagtosindSemConsSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarlibpagtosindsemconsultasaldo.request.CancelarLibPagtosIndSemConsultaSaldoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarlibpagtosindsemconsultasaldo.response.CancelarLibPagtosIndSemConsultaSaldoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcanlibpagtosindsemconssaldo.request.ConsultarCanLibPagtosIndSemConsSaldoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarcanlibpagtosindsemconssaldo.response.ConsultarCanLibPagtosIndSemConsSaldoResponse;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: CanLibPagtoIndSemConSaldo
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class CanLibPagtoIndSemConSaldoServiceImpl implements ICanLibPagtoIndSemConSaldoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.ICanLibPagtoIndSemConSaldoService#consultarCanLibPagtosindSemConsSaldo(br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.bean.ConsultarCanLibPagtosindSemConsSaldoEntradaDTO)
	 */
	public List<ConsultarCanLibPagtosindSemConsSaldoSaidaDTO> consultarCanLibPagtosindSemConsSaldo(ConsultarCanLibPagtosindSemConsSaldoEntradaDTO entradaDTO){
    	ConsultarCanLibPagtosIndSemConsSaldoRequest request = new ConsultarCanLibPagtosIndSemConsSaldoRequest();
    	ConsultarCanLibPagtosIndSemConsSaldoResponse response = new ConsultarCanLibPagtosIndSemConsSaldoResponse();
    	List<ConsultarCanLibPagtosindSemConsSaldoSaidaDTO> listaSaida = new ArrayList<ConsultarCanLibPagtosindSemConsSaldoSaidaDTO>();
    	
    	request.setCdTipoPesquisa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoPesquisa()));
    	request.setCdAgendadosPagosNaoPagos(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgendadosPagosNaoPagos()));
    	request.setNrOcorrencias(ICanLibPagtoIndSemConSaldoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
    	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
    	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
    	request.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
    	request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
    	request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
    	request.setNrArquivoRemssaPagamento(PgitUtil.verificaLongNulo(entradaDTO.getNrArquivoRemssaPagamento()));
    	request.setCdParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdParticipante()));
    	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
    	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
    	request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoDe()));
    	request.setCdControlePagamentoAte(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoAte()));
    	request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoDe()));
    	request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoAte()));
    	request.setCdSituacaoOperacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoOperacaoPagamento()));
    	request.setCdMotivoSituacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdMotivoSituacaoPagamento()));
        request.setCdBarraDocumento(PgitUtil.verificaStringNula(entradaDTO.getCdBarraDocumento()));
        request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
        request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));
        request.setCdIdentificacaoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIdentificacaoInscricaoFavorecido()));
        request.setCdBancoBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoBeneficiario()));
        request.setCdAgenciaBeneficiario(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgenciaBeneficiario()));
        request.setCdContaBeneficiario(PgitUtil.verificaLongNulo(entradaDTO.getCdContaBeneficiario()));
        request.setCdDigitoContaBeneficiario(PgitUtil.DIGITO_CONTA);
        request.setNrUnidadeOrganizacional(PgitUtil.verificaIntegerNulo(entradaDTO.getNrUnidadeOrganizacional()));
        request.setCdBeneficio(PgitUtil.verificaLongNulo(entradaDTO.getCdBeneficio()));
        request.setCdEspecieBeneficioInss(PgitUtil.verificaIntegerNulo(entradaDTO.getCdEspecieBeneficioInss()));
        request.setCdClassificacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdClassificacao()));
        request.setCdEmpresaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdEmpresaContrato()));
        request.setCdTipoConta(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoConta()));
        request.setNrContrato(PgitUtil.verificaLongNulo(entradaDTO.getNrContrato()));
        request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjParticipante()));
        request.setCdFilialCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjParticipante()));
        request.setCdControleCnpjParticipante(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjParticipante()));
        request.setCdPessoaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoDebito()));
        request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoDebito()));
        request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoDebito()));
        request.setCdPessoaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContratoCredt()));
        request.setCdTipoContratoCredt(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoCredt()));
        request.setNrSequenciaContratoCredt(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoCredt()));
        request.setCdIndiceSimulaPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIndiceSimulaPagamento()));
        request.setCdOrigemRecebidoEfetivacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOrigemRecebidoEfetivacao()));        
        request.setCdTituloPgtoRastreado(0); 
        request.setCdBancoSalarial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBancoSalarial()));
        request.setCdAgencaSalarial(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencaSalarial()));
        request.setCdContaSalarial(PgitUtil.verificaLongNulo(entradaDTO.getCdContaSalarial()));
		if("".equals(PgitUtil.verificaStringNula(entradaDTO.getCdIspbPagtoDestino()))){
			request.setCdIspbPagtoDestino("");
		}else{
			request.setCdIspbPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getCdIspbPagtoDestino(), 8));
		}
		request.setContaPagtoDestino(PgitUtil.preencherZerosAEsquerda(entradaDTO.getContaPagtoDestino(), 20));

        response = getFactoryAdapter().getConsultarCanLibPagtosIndSemConsSaldoPDCAdapter().invokeProcess(request);
        
        ConsultarCanLibPagtosindSemConsSaldoSaidaDTO saida;
        
        for(int i=0;i<response.getOcorrenciasCount();i++){
        	saida = new ConsultarCanLibPagtosindSemConsSaldoSaidaDTO();
        	
        	saida.setCodMensagem(response.getCodMensagem());
    		saida.setMensagem(response.getMensagem());
    		
    		saida.setNrCnpjCpf(response.getOcorrencias(i).getNrCnpjCpf());
    		saida.setNrFilialCnpjCpf(response.getOcorrencias(i).getNrFilialCnpjCpf());
    		saida.setNrDigitoCnpjCpf(response.getOcorrencias(i).getNrDigitoCnpjCpf());
    		saida.setCdPessoaJuridicaContrato(response.getOcorrencias(i).getCdPessoaJuridicaContrato());
    		saida.setDsRazaoSocial(response.getOcorrencias(i).getDsRazaoSocial());
    		saida.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
    		saida.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
    		saida.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
    		saida.setNrContrato(response.getOcorrencias(i).getNrContrato());
    		saida.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
    		saida.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
    		saida.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
    		saida.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
    		saida.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
    		saida.setDtCreditoPagamento((response.getOcorrencias(i).getDtCreditoPagamento() != null && !response.getOcorrencias(i).getDtCreditoPagamento().equals("")
 	    		   ? FormatarData.formatarData(response.getOcorrencias(i).getDtCreditoPagamento()) : ""));
    		saida.setVlEfetivoPagamento(response.getOcorrencias(i).getVlEfetivoPagamento());
    		saida.setVlEfetivoPagamentoFormatado(NumberUtils.format(response.getOcorrencias(i).getVlEfetivoPagamento(),"#,##0.00"));
    		saida.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido() == 0L ? "" : String.valueOf(response.getOcorrencias(i).getCdInscricaoFavorecido()));
    		saida.setDsFavorecido(response.getOcorrencias(i).getDsFavorecido());
    		saida.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
    		saida.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
    		saida.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
    		saida.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
    		saida.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
    		saida.setCdBancoCredito(response.getOcorrencias(i).getCdBancoCredito());
    		saida.setCdAgenciaCredito(response.getOcorrencias(i).getCdAgenciaCredito());
    		saida.setCdDigitoAgenciaCredito(response.getOcorrencias(i).getCdDigitoAgenciaCredito());
    		saida.setCdContaCredito(response.getOcorrencias(i).getCdContaCredito());
    		saida.setCdDigitoContaCredito(response.getOcorrencias(i).getCdDigitoContaCredito());
    		saida.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
    		saida.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
    		saida.setCdMotivoSituacaoPagamento(response.getOcorrencias(i).getCdMotivoSituacaoPagamento());
    		saida.setDsMotivoSituacaoPagamento(response.getOcorrencias(i).getDsMotivoSituacaoPagamento());
    		saida.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
    		saida.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
    		saida.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
    		
    		saida.setCdCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(response.getOcorrencias(i).getNrCnpjCpf(),response.getOcorrencias(i).getNrFilialCnpjCpf(),response.getOcorrencias(i).getNrDigitoCnpjCpf()));
    		saida.setFavorecidoBeneficiarioFormatado(response.getOcorrencias(i).getDsFavorecido());
    		saida.setBancoAgenciaContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoDebito(), response.getOcorrencias(i).getCdAgenciaDebito(), response.getOcorrencias(i).getCdDigitoAgenciaDebito(), response.getOcorrencias(i).getCdContaDebito(), response.getOcorrencias(i).getCdDigitoContaDebito(), true));
    		saida.setBancoAgenciaContaCreditoFormatado(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoCredito(), response.getOcorrencias(i).getCdAgenciaCredito(), response.getOcorrencias(i).getCdDigitoAgenciaCredito(), response.getOcorrencias(i).getCdContaCredito(), response.getOcorrencias(i).getCdDigitoContaCredito(), true));
    		
    		saida.setContaCreditoFormatada(PgitUtil.formatConta(response.getOcorrencias(i).getCdContaCredito(), response.getOcorrencias(i).getCdDigitoContaCredito(), false));
    		saida.setDsIndicadorPagamento(response.getOcorrencias(i).getDsIndicadorPagamento());
    		
    		saida.setCdIspbPagtoDestino(response.getOcorrencias(i).getCdIspbPagtoDestino());
    		saida.setContaPagtoDestino(response.getOcorrencias(i).getContaPagtoDestino());

    		listaSaida.add(saida);
        }
        
    	return listaSaida;
    }   
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.canlibpagtoindsemconsaldo.ICanLibPagtoIndSemConSaldoService#cancelarLibPagtosIndSemConsultaSaldo(java.util.List)
     */
    public CancelarLibPagtosIndSemConsultaSaldoSaidaDTO cancelarLibPagtosIndSemConsultaSaldo(List<CancelarLibPagtosIndSemConsultaSaldoEntradaDTO> listaEntradaDTO){
    	CancelarLibPagtosIndSemConsultaSaldoRequest request = new CancelarLibPagtosIndSemConsultaSaldoRequest();
    	CancelarLibPagtosIndSemConsultaSaldoResponse response = new CancelarLibPagtosIndSemConsultaSaldoResponse();
    	CancelarLibPagtosIndSemConsultaSaldoSaidaDTO saidaDTO = new CancelarLibPagtosIndSemConsultaSaldoSaidaDTO();
    	
    	request.setNumeroConsultas(listaEntradaDTO.size());
    	ArrayList<br.com.bradesco.web.pgit.service.data.pdc.cancelarlibpagtosindsemconsultasaldo.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.cancelarlibpagtosindsemconsultasaldo.request.Ocorrencias>();
    	
    	for(int i=0; i<request.getNumeroConsultas(); i++){
    		br.com.bradesco.web.pgit.service.data.pdc.cancelarlibpagtosindsemconsultasaldo.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.cancelarlibpagtosindsemconsultasaldo.request.Ocorrencias();
    	
    		ocorrencia.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaEntradaDTO.get(i).getCdPessoaJuridicaContrato()));
    		ocorrencia.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdTipoContratoNegocio()));
    		ocorrencia.setNrContratoNegocio(PgitUtil.verificaLongNulo(listaEntradaDTO.get(i).getNrSequenciaContratoNegocio()));
    		ocorrencia.setCdTipoCanal(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdTipoCanal()));
    		ocorrencia.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdProdutoServicoOperacao()));
    		ocorrencia.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(listaEntradaDTO.get(i).getCdOperacaoProdutoServico()));
    		ocorrencia.setCdControlePagamento(PgitUtil.verificaStringNula(listaEntradaDTO.get(i).getCdControlePagamento()));
    		
    		ocorrencias.add(ocorrencia);
    	}
    	
    	request.setOcorrencias(ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.cancelarlibpagtosindsemconsultasaldo.request.Ocorrencias[0]));
    	
    	response = getFactoryAdapter().getCancelarLibPagtosIndSemConsultaSaldoPDCAdapter().invokeProcess(request);
    	
    	saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
    	
    	return saidaDTO;
    }
}