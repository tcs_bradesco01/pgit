/*
 * Nome: br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean;

/**
 * Nome: OcorrenciasBloqDesbEmisAutAvisCompEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasBloqDesbEmisAutAvisCompEntradaDTO {
	
    
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;
	
	/** Atributo cdRelacionamentoProdutoProduto. */
	private Integer cdRelacionamentoProdutoProduto;
	
	/** Atributo cdSituacaoVinculacaoAviso. */
	private Integer cdSituacaoVinculacaoAviso;
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
	    return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
	    this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdRelacionamentoProdutoProduto.
	 *
	 * @return cdRelacionamentoProdutoProduto
	 */
	public Integer getCdRelacionamentoProdutoProduto() {
		return cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Set: cdRelacionamentoProdutoProduto.
	 *
	 * @param cdRelacionamentoProdutoProduto the cd relacionamento produto produto
	 */
	public void setCdRelacionamentoProdutoProduto(
			Integer cdRelacionamentoProdutoProduto) {
		this.cdRelacionamentoProdutoProduto = cdRelacionamentoProdutoProduto;
	}
	
	/**
	 * Get: cdSituacaoVinculacaoAviso.
	 *
	 * @return cdSituacaoVinculacaoAviso
	 */
	public Integer getCdSituacaoVinculacaoAviso() {
		return cdSituacaoVinculacaoAviso;
	}
	
	/**
	 * Set: cdSituacaoVinculacaoAviso.
	 *
	 * @param cdSituacaoVinculacaoAviso the cd situacao vinculacao aviso
	 */
	public void setCdSituacaoVinculacaoAviso(Integer cdSituacaoVinculacaoAviso) {
		this.cdSituacaoVinculacaoAviso = cdSituacaoVinculacaoAviso;
	}
	
	
	
}
