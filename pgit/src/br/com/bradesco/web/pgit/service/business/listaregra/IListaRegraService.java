/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PilotoIntranet/JavaSource/br/com/bradesco/web/pgit/service/business/listaregra/IListaRegraService.java,v $
 * $Id: IListaRegraService.java,v 1.1 2009/05/22 18:35:57 corporate\marcio.alves Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: IListaRegraService.java,v $
 * Revision 1.1  2009/05/22 18:35:57  corporate\marcio.alves
 * Adicionando o servico listaregra para teste do pdc
 *
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.listaregra;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.listaregra.bean.ListaRegraVO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ListaRegra
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IListaRegraService {

    /**
     * M�todo de exemplo.
     */
	List<ListaRegraVO> listaRegras(ListaRegraVO vo);

}

