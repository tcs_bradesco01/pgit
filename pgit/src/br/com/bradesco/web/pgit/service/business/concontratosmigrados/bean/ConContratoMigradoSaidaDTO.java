/*
 * Nome: br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean;

import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;


/**
 * Nome: ConContratoMigradoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConContratoMigradoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
	
	/** Atributo centroCustoOrigem. */
	private String centroCustoOrigem;
	
	/** Atributo dtMigracao. */
	private String dtMigracao;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdClub. */
    private Long cdClub;
    
    /** Atributo cdCorpoCpfCnpj. */
    private Long cdCorpoCpfCnpj;
    
    /** Atributo filiaCnpj. */
    private Integer filiaCnpj;
    
    /** Atributo digitoCnpj. */
    private Integer digitoCnpj;
    
    /** Atributo dsNome. */
    private String dsNome;
    
    /** Atributo cdPerfil. */
    private Long cdPerfil;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo dsAgencia. */
    private String dsAgencia;
    
    /** Atributo razao. */
    private Integer razao;
    
    /** Atributo dsRazao. */
    private String dsRazao;
    
    /** Atributo cdConta. */
    private Integer cdConta;
    
    /** Atributo digitoConta. */
    private Integer digitoConta;
    
    /** Atributo cdLancamento. */
    private Integer cdLancamento;
    
    /** Atributo dsLancamento. */
    private String dsLancamento;
    
    /** Atributo cpfCnpjFormatado. */
    private String cpfCnpjFormatado;
    
    /** Atributo cdCpfCnpjRepresentante. */
    private Long cdCpfCnpjRepresentante;
    
    /** Atributo cdFilialRepresentante. */
    private Integer cdFilialRepresentante;
    
    /** Atributo cdCpfDigitoRepresentante. */
    private Integer cdCpfDigitoRepresentante;
    
    /** Atributo dsRezaoSocialRepresentente. */
    private String dsRezaoSocialRepresentente;
    
    /** Atributo dsPessoaJuridicaContrato. */
    private String dsPessoaJuridicaContrato;
    
    /** Atributo dsTipoContratoNegocio. */
    private String dsTipoContratoNegocio;
    
    /** Atributo cpfCnpjRepresentanteFormatado. */
    private String cpfCnpjRepresentanteFormatado;
    
    /** Atributo cdSituacaoContrato. */
    private Integer cdSituacaoContrato;

    /** Atributo contaFormatada. */
    private String contaFormatada;
    
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public Long getCdClub() {
		return cdClub;
	}
	
	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Integer getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Integer cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}
	
	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}
	
	/**
	 * Get: cdLancamento.
	 *
	 * @return cdLancamento
	 */
	public Integer getCdLancamento() {
		return cdLancamento;
	}
	
	/**
	 * Set: cdLancamento.
	 *
	 * @param cdLancamento the cd lancamento
	 */
	public void setCdLancamento(Integer cdLancamento) {
		this.cdLancamento = cdLancamento;
	}
	
	/**
	 * Get: cdPerfil.
	 *
	 * @return cdPerfil
	 */
	public Long getCdPerfil() {
		return cdPerfil;
	}
	
	/**
	 * Set: cdPerfil.
	 *
	 * @param cdPerfil the cd perfil
	 */
	public void setCdPerfil(Long cdPerfil) {
		this.cdPerfil = cdPerfil;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: centroCustoOrigem.
	 *
	 * @return centroCustoOrigem
	 */
	public String getCentroCustoOrigem() {
		return centroCustoOrigem;
	}
	
	/**
	 * Set: centroCustoOrigem.
	 *
	 * @param centroCustoOrigem the centro custo origem
	 */
	public void setCentroCustoOrigem(String centroCustoOrigem) {
		this.centroCustoOrigem = centroCustoOrigem;
	}
	
	/**
	 * Get: digitoCnpj.
	 *
	 * @return digitoCnpj
	 */
	public Integer getDigitoCnpj() {
		return digitoCnpj;
	}
	
	/**
	 * Set: digitoCnpj.
	 *
	 * @param digitoCnpj the digito cnpj
	 */
	public void setDigitoCnpj(Integer digitoCnpj) {
		this.digitoCnpj = digitoCnpj;
	}
	
	/**
	 * Get: digitoConta.
	 *
	 * @return digitoConta
	 */
	public Integer getDigitoConta() {
		return digitoConta;
	}
	
	/**
	 * Set: digitoConta.
	 *
	 * @param digitoConta the digito conta
	 */
	public void setDigitoConta(Integer digitoConta) {
		this.digitoConta = digitoConta;
	}
	
	/**
	 * Get: dsNome.
	 *
	 * @return dsNome
	 */
	public String getDsNome() {
		return dsNome;
	}
	
	/**
	 * Set: dsNome.
	 *
	 * @param dsNome the ds nome
	 */
	public void setDsNome(String dsNome) {
		this.dsNome = dsNome;
	}
	
	/**
	 * Get: dtMigracao.
	 *
	 * @return dtMigracao
	 */
	public String getDtMigracao() {
		return dtMigracao;
	}
	
	/**
	 * Set: dtMigracao.
	 *
	 * @param dtMigracao the dt migracao
	 */
	public void setDtMigracao(String dtMigracao) {
		this.dtMigracao = dtMigracao;
	}
	
	/**
	 * Get: filiaCnpj.
	 *
	 * @return filiaCnpj
	 */
	public Integer getFiliaCnpj() {
		return filiaCnpj;
	}
	
	/**
	 * Set: filiaCnpj.
	 *
	 * @param filiaCnpj the filia cnpj
	 */
	public void setFiliaCnpj(Integer filiaCnpj) {
		this.filiaCnpj = filiaCnpj;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: razao.
	 *
	 * @return razao
	 */
	public Integer getRazao() {
		return razao;
	}
	
	/**
	 * Set: razao.
	 *
	 * @param razao the razao
	 */
	public void setRazao(Integer razao) {
		this.razao = razao;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return cpfCnpjFormatado;
	}
	
	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}
	
	/**
	 * Get: cdCpfCnpjRepresentante.
	 *
	 * @return cdCpfCnpjRepresentante
	 */
	public Long getCdCpfCnpjRepresentante() {
		return cdCpfCnpjRepresentante;
	}
	
	/**
	 * Set: cdCpfCnpjRepresentante.
	 *
	 * @param cdCpfCnpjRepresentante the cd cpf cnpj representante
	 */
	public void setCdCpfCnpjRepresentante(Long cdCpfCnpjRepresentante) {
		this.cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
	}
	
	/**
	 * Get: cdCpfDigitoRepresentante.
	 *
	 * @return cdCpfDigitoRepresentante
	 */
	public Integer getCdCpfDigitoRepresentante() {
		return cdCpfDigitoRepresentante;
	}
	
	/**
	 * Set: cdCpfDigitoRepresentante.
	 *
	 * @param cdCpfDigitoRepresentante the cd cpf digito representante
	 */
	public void setCdCpfDigitoRepresentante(Integer cdCpfDigitoRepresentante) {
		this.cdCpfDigitoRepresentante = cdCpfDigitoRepresentante;
	}
	
	/**
	 * Get: cdFilialRepresentante.
	 *
	 * @return cdFilialRepresentante
	 */
	public Integer getCdFilialRepresentante() {
		return cdFilialRepresentante;
	}
	
	/**
	 * Set: cdFilialRepresentante.
	 *
	 * @param cdFilialRepresentante the cd filial representante
	 */
	public void setCdFilialRepresentante(Integer cdFilialRepresentante) {
		this.cdFilialRepresentante = cdFilialRepresentante;
	}
	
	/**
	 * Get: dsPessoaJuridicaContrato.
	 *
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
		return dsPessoaJuridicaContrato;
	}
	
	/**
	 * Set: dsPessoaJuridicaContrato.
	 *
	 * @param dsPessoaJuridicaContrato the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(String dsPessoaJuridicaContrato) {
		this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}
	
	/**
	 * Get: dsRezaoSocialRepresentente.
	 *
	 * @return dsRezaoSocialRepresentente
	 */
	public String getDsRezaoSocialRepresentente() {
		return dsRezaoSocialRepresentente;
	}
	
	/**
	 * Set: dsRezaoSocialRepresentente.
	 *
	 * @param dsRezaoSocialRepresentente the ds rezao social representente
	 */
	public void setDsRezaoSocialRepresentente(String dsRezaoSocialRepresentente) {
		this.dsRezaoSocialRepresentente = dsRezaoSocialRepresentente;
	}
	
	/**
	 * Get: dsTipoContratoNegocio.
	 *
	 * @return dsTipoContratoNegocio
	 */
	public String getDsTipoContratoNegocio() {
		return dsTipoContratoNegocio;
	}
	
	/**
	 * Set: dsTipoContratoNegocio.
	 *
	 * @param dsTipoContratoNegocio the ds tipo contrato negocio
	 */
	public void setDsTipoContratoNegocio(String dsTipoContratoNegocio) {
		this.dsTipoContratoNegocio = dsTipoContratoNegocio;
	}
	
	/**
	 * Get: cpfCnpjRepresentanteFormatado.
	 *
	 * @return cpfCnpjRepresentanteFormatado
	 */
	public String getCpfCnpjRepresentanteFormatado() {
		return cpfCnpjRepresentanteFormatado;
	}
	
	/**
	 * Set: cpfCnpjRepresentanteFormatado.
	 *
	 * @param cpfCnpjRepresentanteFormatado the cpf cnpj representante formatado
	 */
	public void setCpfCnpjRepresentanteFormatado(
			String cpfCnpjRepresentanteFormatado) {
		this.cpfCnpjRepresentanteFormatado = cpfCnpjRepresentanteFormatado;
	}
	
	/**
	 * Get: contaFormatada.
	 *
	 * @return contaFormatada
	 */
	public String getContaFormatada() {
		return contaFormatada;
	}
	
	/**
	 * Set: contaFormatada.
	 *
	 * @param contaFormatada the conta formatada
	 */
	public void setContaFormatada(String contaFormatada) {
		this.contaFormatada = contaFormatada;
	}
	
	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}
	
	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	
	/**
	 * Get: dsLancamento.
	 *
	 * @return dsLancamento
	 */
	public String getDsLancamento() {
		return dsLancamento;
	}
	
	/**
	 * Set: dsLancamento.
	 *
	 * @param dsLancamento the ds lancamento
	 */
	public void setDsLancamento(String dsLancamento) {
		this.dsLancamento = dsLancamento;
	}
	
	/**
	 * Get: dsRazao.
	 *
	 * @return dsRazao
	 */
	public String getDsRazao() {
		return dsRazao;
	}
	
	/**
	 * Set: dsRazao.
	 *
	 * @param dsRazao the ds razao
	 */
	public void setDsRazao(String dsRazao) {
		this.dsRazao = dsRazao;
	}
	
	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}
	
	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}
	
	/**
	 * Get: dsSituacaoContrato.
	 *
	 * @return dsSituacaoContrato
	 */
	public String getDsSituacaoContrato() {
		
		if(getCdSituacaoContrato().equals(1)){
			return  MessageHelperUtils.getI18nMessage("label_migrado");
		}else if(getCdSituacaoContrato().equals(2)) {
			return  MessageHelperUtils.getI18nMessage("label_simulacao");
		}else if(getCdSituacaoContrato().equals(3)){
			return MessageHelperUtils.getI18nMessage("label_migrado_reversao");
		}else if(getCdSituacaoContrato().equals(4)){		
			return MessageHelperUtils.getI18nMessage("label_simulacao_reversao");
		}else if(getCdSituacaoContrato().equals(5)){
			return MessageHelperUtils.getI18nMessage("label_migrado_prod");
		}else if(getCdSituacaoContrato().equals(6)){
			return MessageHelperUtils.getI18nMessage("label_migracao_teste");			
		}else{
			return  "";
		}	
		
	}

	
}
