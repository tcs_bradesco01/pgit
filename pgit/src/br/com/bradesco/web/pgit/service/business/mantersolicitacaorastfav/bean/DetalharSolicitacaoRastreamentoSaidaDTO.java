/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean;

import java.math.BigDecimal;


/**
 * Nome: DetalharSolicitacaoRastreamentoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DetalharSolicitacaoRastreamentoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdPessoaJuridContrato. */
    private Long cdPessoaJuridContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSeqContratoNegocio. */
    private Long nrSeqContratoNegocio;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo dsProdutoServicoOperacao. */
    private String dsProdutoServicoOperacao;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    
    /** Atributo dsProdutoOperacaoRelacionado. */
    private  String dsProdutoOperacaoRelacionado;
    
    /** Atributo cdRelacionamentoProduto. */
    private Integer cdRelacionamentoProduto;
    
    /** Atributo cdBancoDeb. */
    private Integer cdBancoDeb;
    
    /** Atributo dsBancoDeb. */
    private String dsBancoDeb;
    
    /** Atributo cdAgenciaDeb. */
    private Integer cdAgenciaDeb;
    
    /** Atributo cdDigitoAgenciaDeb. */
    private Integer cdDigitoAgenciaDeb;
    
    /** Atributo dsAgenciaDeb. */
    private String dsAgenciaDeb;
    
    /** Atributo cdContaDeb. */
    private Long cdContaDeb;
    
    /** Atributo cdDigitoContaDeb. */
    private String cdDigitoContaDeb;
    
    /** Atributo dtInicioRastreabilidadeFavorecido. */
    private String dtInicioRastreabilidadeFavorecido;
    
    /** Atributo dtFimRastreabilidadeFavorecido. */
    private String dtFimRastreabilidadeFavorecido;
    
    /** Atributo cdIndicadorInclusaoFavorecido. */
    private Integer cdIndicadorInclusaoFavorecido;
    
    /** Atributo dsIndicadorInclusaoFavorecido. */
    private String dsIndicadorInclusaoFavorecido;
    
    /** Atributo cdIdDestinoRetorno. */
    private Integer cdIdDestinoRetorno;
    
    /** Atributo dsIdDestinoRetorno. */
    private String dsIdDestinoRetorno;
    
    /** Atributo cdSituacaoSolicitacaoPagamento. */
    private Integer cdSituacaoSolicitacaoPagamento;
    
    /** Atributo dsSituacaoSolicitacaoPagamento. */
    private String dsSituacaoSolicitacaoPagamento;
    
    /** Atributo cdMotivoSituacaoSolicitacao. */
    private Integer cdMotivoSituacaoSolicitacao;
    
    /** Atributo dsMotivoSituacaoSolicitacao. */
    private String dsMotivoSituacaoSolicitacao;
    
    /** Atributo dtSolicitacao. */
    private String dtSolicitacao;
    
    /** Atributo hrSolicitacao. */
    private String hrSolicitacao;
    
    /** Atributo dtAtendimento. */
    private String dtAtendimento;
    
    /** Atributo hrAtendimento. */
    private String hrAtendimento;
    
    /** Atributo nrTarifaBonificacao. */
    private BigDecimal nrTarifaBonificacao;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo cdUsuarioInclusaoExter. */
    private String cdUsuarioInclusaoExter;
    
    /** Atributo dtInclusao. */
    private String dtInclusao;
    
    /** Atributo hrInclusao. */
    private String hrInclusao;
    
    /** Atributo cdOperCanalInclusao. */
    private String cdOperCanalInclusao;
    
    /** Atributo cdTipoCanalInclusao. */
    private Integer cdTipoCanalInclusao;
    
    /** Atributo dsCanalInclusao. */
    private String dsCanalInclusao;
    
    /** Atributo cdUsuarioManutencao. */
    private String cdUsuarioManutencao;
    
    /** Atributo cdUsuarioManutencaoExter. */
    private String cdUsuarioManutencaoExter;
    
    /** Atributo dtManutencao. */
    private String dtManutencao;
    
    /** Atributo hrManutencao. */
    private String hrManutencao;
    
    /** Atributo cdOperCanalManutencao. */
    private String cdOperCanalManutencao;
    
    /** Atributo cdTipoCanalManutencao. */
    private Integer cdTipoCanalManutencao;
    
    /** Atributo dsCanalManutencao. */
    private String dsCanalManutencao;
    
    /** Atributo cdFormaCadastroFavorecidos. */
    private Integer cdFormaCadastroFavorecidos;
    
    /** Atributo dsFormaCadastroFavorecidos. */
    private String dsFormaCadastroFavorecidos;
    
    /** Atributo vlTarifaPadrao. */
    private BigDecimal vlTarifaPadrao;
    
    /** Atributo numPercentualDescTarifa. */
    private BigDecimal numPercentualDescTarifa;
    
    /** Atributo vlrTarifaAtual. */
    private BigDecimal vlrTarifaAtual;
	
    /**
     * Detalhar solicitacao rastreamento saida dto.
     */
    public DetalharSolicitacaoRastreamentoSaidaDTO() {
		super();
	}


	/**
	 * Detalhar solicitacao rastreamento saida dto.
	 *
	 * @param codMensagem the cod mensagem
	 * @param mensagem the mensagem
	 * @param cdPessoaJuridContrato the cd pessoa jurid contrato
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 * @param cdBancoDeb the cd banco deb
	 * @param dsBancoDeb the ds banco deb
	 * @param cdAgenciaDeb the cd agencia deb
	 * @param cdDigitoAgenciaDeb the cd digito agencia deb
	 * @param dsAgenciaDeb the ds agencia deb
	 * @param cdContaDeb the cd conta deb
	 * @param cdDigitoContaDeb the cd digito conta deb
	 * @param dtInicioRastreabilidadeFavorecido the dt inicio rastreabilidade favorecido
	 * @param dtFimRastreabilidadeFavorecido the dt fim rastreabilidade favorecido
	 * @param cdIndicadorInclusaoFavorecido the cd indicador inclusao favorecido
	 * @param dsIndicadorInclusaoFavorecido the ds indicador inclusao favorecido
	 * @param cdIdDestinoRetorno the cd id destino retorno
	 * @param dsIdDestinoRetorno the ds id destino retorno
	 * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
	 * @param dsSituacaoSolicitacaoPagamento the ds situacao solicitacao pagamento
	 * @param cdMotivoSituacaoSolicitacao the cd motivo situacao solicitacao
	 * @param dsMotivoSituacaoSolicitacao the ds motivo situacao solicitacao
	 * @param dtSolicitacao the dt solicitacao
	 * @param hrSolicitacao the hr solicitacao
	 * @param dtAtendimento the dt atendimento
	 * @param hrAtendimento the hr atendimento
	 * @param nrTarifaBonificacao the nr tarifa bonificacao
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 * @param cdUsuarioInclusaoExter the cd usuario inclusao exter
	 * @param dtInclusao the dt inclusao
	 * @param hrInclusao the hr inclusao
	 * @param cdOperCanalInclusao the cd oper canal inclusao
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 * @param dsCanalInclusao the ds canal inclusao
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 * @param cdUsuarioManutencaoExter the cd usuario manutencao exter
	 * @param dtManutencao the dt manutencao
	 * @param hrManutencao the hr manutencao
	 * @param cdOperCanalManutencao the cd oper canal manutencao
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 * @param dsCanalManutencao the ds canal manutencao
	 * @param cdFormaCadastroFavorecidos the cd forma cadastro favorecidos
	 * @param dsFormaCadastroFavorecidos the ds forma cadastro favorecidos
	 */
	public DetalharSolicitacaoRastreamentoSaidaDTO(String codMensagem, String mensagem, Long cdPessoaJuridContrato, Integer cdTipoContratoNegocio, Long nrSeqContratoNegocio, Integer cdProdutoServicoOperacao, String dsProdutoServicoOperacao, Integer cdProdutoOperacaoRelacionado, String dsProdutoOperacaoRelacionado, Integer cdRelacionamentoProduto, Integer cdBancoDeb, String dsBancoDeb, Integer cdAgenciaDeb, Integer cdDigitoAgenciaDeb, String dsAgenciaDeb, Long cdContaDeb, String cdDigitoContaDeb, String dtInicioRastreabilidadeFavorecido, String dtFimRastreabilidadeFavorecido, Integer cdIndicadorInclusaoFavorecido, String dsIndicadorInclusaoFavorecido, Integer cdIdDestinoRetorno, String dsIdDestinoRetorno, Integer cdSituacaoSolicitacaoPagamento, String dsSituacaoSolicitacaoPagamento, Integer cdMotivoSituacaoSolicitacao, String dsMotivoSituacaoSolicitacao, String dtSolicitacao, String hrSolicitacao, String dtAtendimento, String hrAtendimento, BigDecimal nrTarifaBonificacao, String cdUsuarioInclusao, String cdUsuarioInclusaoExter, String dtInclusao, String hrInclusao, String cdOperCanalInclusao, Integer cdTipoCanalInclusao, String dsCanalInclusao, String cdUsuarioManutencao, String cdUsuarioManutencaoExter, String dtManutencao, String hrManutencao, String cdOperCanalManutencao, Integer cdTipoCanalManutencao, String dsCanalManutencao, Integer cdFormaCadastroFavorecidos, String dsFormaCadastroFavorecidos) {
		super();
		this.codMensagem = codMensagem;
		this.mensagem = mensagem;
		this.cdPessoaJuridContrato = cdPessoaJuridContrato;
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
		this.cdBancoDeb = cdBancoDeb;
		this.dsBancoDeb = dsBancoDeb;
		this.cdAgenciaDeb = cdAgenciaDeb;
		this.cdDigitoAgenciaDeb = cdDigitoAgenciaDeb;
		this.dsAgenciaDeb = dsAgenciaDeb;
		this.cdContaDeb = cdContaDeb;
		this.cdDigitoContaDeb = cdDigitoContaDeb;
		this.dtInicioRastreabilidadeFavorecido = dtInicioRastreabilidadeFavorecido;
		this.dtFimRastreabilidadeFavorecido = dtFimRastreabilidadeFavorecido;
		this.cdIndicadorInclusaoFavorecido = cdIndicadorInclusaoFavorecido;
		this.dsIndicadorInclusaoFavorecido = dsIndicadorInclusaoFavorecido;
		this.cdIdDestinoRetorno = cdIdDestinoRetorno;
		this.dsIdDestinoRetorno = dsIdDestinoRetorno;
		this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
		this.dsSituacaoSolicitacaoPagamento = dsSituacaoSolicitacaoPagamento;
		this.cdMotivoSituacaoSolicitacao = cdMotivoSituacaoSolicitacao;
		this.dsMotivoSituacaoSolicitacao = dsMotivoSituacaoSolicitacao;
		this.dtSolicitacao = dtSolicitacao;
		this.hrSolicitacao = hrSolicitacao;
		this.dtAtendimento = dtAtendimento;
		this.hrAtendimento = hrAtendimento;
		this.nrTarifaBonificacao = nrTarifaBonificacao;
		this.cdUsuarioInclusao = cdUsuarioInclusao;
		this.cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
		this.dtInclusao = dtInclusao;
		this.hrInclusao = hrInclusao;
		this.cdOperCanalInclusao = cdOperCanalInclusao;
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
		this.dsCanalInclusao = dsCanalInclusao;
		this.cdUsuarioManutencao = cdUsuarioManutencao;
		this.cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
		this.dtManutencao = dtManutencao;
		this.hrManutencao = hrManutencao;
		this.cdOperCanalManutencao = cdOperCanalManutencao;
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
		this.dsCanalManutencao = dsCanalManutencao;
		this.cdFormaCadastroFavorecidos = cdFormaCadastroFavorecidos;
		this.dsFormaCadastroFavorecidos = dsFormaCadastroFavorecidos;
	}




	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco(){
		if(this.cdBancoDeb!=null && this.cdBancoDeb!=0){
			return this.dsBancoDeb.equals("") ? String.valueOf(this.cdBancoDeb):this.cdBancoDeb + " " + this.dsBancoDeb;
		}else if(this.cdBancoDeb ==0){ 
			return String.valueOf(this.cdBancoDeb==0?"":this.cdBancoDeb);
		}else{
			return String.valueOf(this.cdBancoDeb==null?"":this.cdBancoDeb);
		}
	}
	
	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia(){
		
		if (this.cdDigitoAgenciaDeb != null && this.dsAgenciaDeb != null && !this.dsAgenciaDeb.trim().equals("")) {
			return this.cdAgenciaDeb == 0 ? "" : this.cdAgenciaDeb + "-" + this.cdDigitoAgenciaDeb + " " + this.dsAgenciaDeb;
		} else if (this.cdDigitoAgenciaDeb == null && dsAgenciaDeb != null && !dsAgenciaDeb.trim().equals("")){
			return  this.cdAgenciaDeb == 0 ? "" : this.cdAgenciaDeb + " " + this.dsAgenciaDeb;
		} else if (this.cdDigitoAgenciaDeb != null ){
			return  this.cdAgenciaDeb == 0 ? "" : this.cdAgenciaDeb + "-" + this.cdDigitoAgenciaDeb;
		}else{
			return String.valueOf(this.cdAgenciaDeb==0?"":this.cdAgenciaDeb);
			
		}
	}
	
	/**
	 * Get: digitoConta.
	 *
	 * @return digitoConta
	 */
	public String getDigitoConta() {
		if (this.cdDigitoContaDeb != null && !this.cdDigitoContaDeb.trim().equals("")) {
			return this.cdContaDeb == 0 ? "" : this.cdContaDeb + "-" +  this.cdDigitoContaDeb;
		}else {
			return String.valueOf(this.cdContaDeb==null?"":this.cdContaDeb);
		}
	}
	
	/**
	 * Get: tipoCanalInclusaoFormatado.
	 *
	 * @return tipoCanalInclusaoFormatado
	 */
	public String gettipoCanalInclusaoFormatado(){
		if(this.cdTipoCanalInclusao != null && this.dsCanalInclusao != null){
			return cdTipoCanalInclusao + " - " + dsCanalInclusao;
		}else{
			return "";
		}
	}
	
	/**
	 * Get: tipoCanalManutencaoFormatado.
	 *
	 * @return tipoCanalManutencaoFormatado
	 */
	public String getTipoCanalManutencaoFormatado(){
		if(this.cdTipoCanalManutencao != null && this.dsCanalManutencao != null){
			return cdTipoCanalManutencao + " - " + dsCanalManutencao;
		}else{
			return "";
		}
	}
	

	/**
	 * Get: cdAgenciaDeb.
	 *
	 * @return cdAgenciaDeb
	 */
	public Integer getCdAgenciaDeb() {
		return cdAgenciaDeb;
	}


	/**
	 * Set: cdAgenciaDeb.
	 *
	 * @param cdAgenciaDeb the cd agencia deb
	 */
	public void setCdAgenciaDeb(Integer cdAgenciaDeb) {
		this.cdAgenciaDeb = cdAgenciaDeb;
	}


	/**
	 * Get: cdBancoDeb.
	 *
	 * @return cdBancoDeb
	 */
	public Integer getCdBancoDeb() {
		return cdBancoDeb;
	}


	/**
	 * Set: cdBancoDeb.
	 *
	 * @param cdBancoDeb the cd banco deb
	 */
	public void setCdBancoDeb(Integer cdBancoDeb) {
		this.cdBancoDeb = cdBancoDeb;
	}


	/**
	 * Get: cdContaDeb.
	 *
	 * @return cdContaDeb
	 */
	public Long getCdContaDeb() {
		return cdContaDeb;
	}


	/**
	 * Set: cdContaDeb.
	 *
	 * @param cdContaDeb the cd conta deb
	 */
	public void setCdContaDeb(Long cdContaDeb) {
		this.cdContaDeb = cdContaDeb;
	}


	/**
	 * Get: cdDigitoAgenciaDeb.
	 *
	 * @return cdDigitoAgenciaDeb
	 */
	public Integer getCdDigitoAgenciaDeb() {
		return cdDigitoAgenciaDeb;
	}


	/**
	 * Set: cdDigitoAgenciaDeb.
	 *
	 * @param cdDigitoAgenciaDeb the cd digito agencia deb
	 */
	public void setCdDigitoAgenciaDeb(Integer cdDigitoAgenciaDeb) {
		this.cdDigitoAgenciaDeb = cdDigitoAgenciaDeb;
	}


	/**
	 * Get: cdDigitoContaDeb.
	 *
	 * @return cdDigitoContaDeb
	 */
	public String getCdDigitoContaDeb() {
		return cdDigitoContaDeb;
	}


	/**
	 * Set: cdDigitoContaDeb.
	 *
	 * @param cdDigitoContaDeb the cd digito conta deb
	 */
	public void setCdDigitoContaDeb(String cdDigitoContaDeb) {
		this.cdDigitoContaDeb = cdDigitoContaDeb;
	}


	/**
	 * Get: cdIdDestinoRetorno.
	 *
	 * @return cdIdDestinoRetorno
	 */
	public Integer getCdIdDestinoRetorno() {
		return cdIdDestinoRetorno;
	}


	/**
	 * Set: cdIdDestinoRetorno.
	 *
	 * @param cdIdDestinoRetorno the cd id destino retorno
	 */
	public void setCdIdDestinoRetorno(Integer cdIdDestinoRetorno) {
		this.cdIdDestinoRetorno = cdIdDestinoRetorno;
	}


	/**
	 * Get: cdIndicadorInclusaoFavorecido.
	 *
	 * @return cdIndicadorInclusaoFavorecido
	 */
	public Integer getCdIndicadorInclusaoFavorecido() {
		return cdIndicadorInclusaoFavorecido;
	}


	/**
	 * Set: cdIndicadorInclusaoFavorecido.
	 *
	 * @param cdIndicadorInclusaoFavorecido the cd indicador inclusao favorecido
	 */
	public void setCdIndicadorInclusaoFavorecido(Integer cdIndicadorInclusaoFavorecido) {
		this.cdIndicadorInclusaoFavorecido = cdIndicadorInclusaoFavorecido;
	}


	/**
	 * Get: cdMotivoSituacaoSolicitacao.
	 *
	 * @return cdMotivoSituacaoSolicitacao
	 */
	public Integer getCdMotivoSituacaoSolicitacao() {
		return cdMotivoSituacaoSolicitacao;
	}


	/**
	 * Set: cdMotivoSituacaoSolicitacao.
	 *
	 * @param cdMotivoSituacaoSolicitacao the cd motivo situacao solicitacao
	 */
	public void setCdMotivoSituacaoSolicitacao(Integer cdMotivoSituacaoSolicitacao) {
		this.cdMotivoSituacaoSolicitacao = cdMotivoSituacaoSolicitacao;
	}


	/**
	 * Get: cdOperCanalInclusao.
	 *
	 * @return cdOperCanalInclusao
	 */
	public String getCdOperCanalInclusao() {
		return cdOperCanalInclusao;
	}


	/**
	 * Set: cdOperCanalInclusao.
	 *
	 * @param cdOperCanalInclusao the cd oper canal inclusao
	 */
	public void setCdOperCanalInclusao(String cdOperCanalInclusao) {
		this.cdOperCanalInclusao = cdOperCanalInclusao;
	}


	/**
	 * Get: cdOperCanalManutencao.
	 *
	 * @return cdOperCanalManutencao
	 */
	public String getCdOperCanalManutencao() {
		return cdOperCanalManutencao;
	}


	/**
	 * Set: cdOperCanalManutencao.
	 *
	 * @param cdOperCanalManutencao the cd oper canal manutencao
	 */
	public void setCdOperCanalManutencao(String cdOperCanalManutencao) {
		this.cdOperCanalManutencao = cdOperCanalManutencao;
	}


	/**
	 * Get: cdPessoaJuridContrato.
	 *
	 * @return cdPessoaJuridContrato
	 */
	public Long getCdPessoaJuridContrato() {
		return cdPessoaJuridContrato;
	}


	/**
	 * Set: cdPessoaJuridContrato.
	 *
	 * @param cdPessoaJuridContrato the cd pessoa jurid contrato
	 */
	public void setCdPessoaJuridContrato(Long cdPessoaJuridContrato) {
		this.cdPessoaJuridContrato = cdPessoaJuridContrato;
	}


	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}


	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}


	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}


	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}


	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public Integer getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}


	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(Integer cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}


	/**
	 * Get: cdSituacaoSolicitacaoPagamento.
	 *
	 * @return cdSituacaoSolicitacaoPagamento
	 */
	public Integer getCdSituacaoSolicitacaoPagamento() {
		return cdSituacaoSolicitacaoPagamento;
	}


	/**
	 * Set: cdSituacaoSolicitacaoPagamento.
	 *
	 * @param cdSituacaoSolicitacaoPagamento the cd situacao solicitacao pagamento
	 */
	public void setCdSituacaoSolicitacaoPagamento(Integer cdSituacaoSolicitacaoPagamento) {
		this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
	}


	/**
	 * Get: cdTipoCanalInclusao.
	 *
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}


	/**
	 * Set: cdTipoCanalInclusao.
	 *
	 * @param cdTipoCanalInclusao the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}


	/**
	 * Get: cdTipoCanalManutencao.
	 *
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}


	/**
	 * Set: cdTipoCanalManutencao.
	 *
	 * @param cdTipoCanalManutencao the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}


	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}


	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}


	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}


	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}


	/**
	 * Get: cdUsuarioInclusaoExter.
	 *
	 * @return cdUsuarioInclusaoExter
	 */
	public String getCdUsuarioInclusaoExter() {
		return cdUsuarioInclusaoExter;
	}


	/**
	 * Set: cdUsuarioInclusaoExter.
	 *
	 * @param cdUsuarioInclusaoExter the cd usuario inclusao exter
	 */
	public void setCdUsuarioInclusaoExter(String cdUsuarioInclusaoExter) {
		this.cdUsuarioInclusaoExter = cdUsuarioInclusaoExter;
	}


	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}


	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}


	/**
	 * Get: cdUsuarioManutencaoExter.
	 *
	 * @return cdUsuarioManutencaoExter
	 */
	public String getCdUsuarioManutencaoExter() {
		return cdUsuarioManutencaoExter;
	}


	/**
	 * Set: cdUsuarioManutencaoExter.
	 *
	 * @param cdUsuarioManutencaoExter the cd usuario manutencao exter
	 */
	public void setCdUsuarioManutencaoExter(String cdUsuarioManutencaoExter) {
		this.cdUsuarioManutencaoExter = cdUsuarioManutencaoExter;
	}


	/**
	 * Get: dsAgenciaDeb.
	 *
	 * @return dsAgenciaDeb
	 */
	public String getDsAgenciaDeb() {
		return dsAgenciaDeb;
	}


	/**
	 * Set: dsAgenciaDeb.
	 *
	 * @param dsAgenciaDeb the ds agencia deb
	 */
	public void setDsAgenciaDeb(String dsAgenciaDeb) {
		this.dsAgenciaDeb = dsAgenciaDeb;
	}


	/**
	 * Get: dsBancoDeb.
	 *
	 * @return dsBancoDeb
	 */
	public String getDsBancoDeb() {
		return dsBancoDeb;
	}


	/**
	 * Set: dsBancoDeb.
	 *
	 * @param dsBancoDeb the ds banco deb
	 */
	public void setDsBancoDeb(String dsBancoDeb) {
		this.dsBancoDeb = dsBancoDeb;
	}


	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}


	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}


	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}


	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}


	/**
	 * Get: dsIdDestinoRetorno.
	 *
	 * @return dsIdDestinoRetorno
	 */
	public String getDsIdDestinoRetorno() {
		return dsIdDestinoRetorno;
	}


	/**
	 * Set: dsIdDestinoRetorno.
	 *
	 * @param dsIdDestinoRetorno the ds id destino retorno
	 */
	public void setDsIdDestinoRetorno(String dsIdDestinoRetorno) {
		this.dsIdDestinoRetorno = dsIdDestinoRetorno;
	}


	/**
	 * Get: dsIndicadorInclusaoFavorecido.
	 *
	 * @return dsIndicadorInclusaoFavorecido
	 */
	public String getDsIndicadorInclusaoFavorecido() {
		return dsIndicadorInclusaoFavorecido;
	}


	/**
	 * Set: dsIndicadorInclusaoFavorecido.
	 *
	 * @param dsIndicadorInclusaoFavorecido the ds indicador inclusao favorecido
	 */
	public void setDsIndicadorInclusaoFavorecido(String dsIndicadorInclusaoFavorecido) {
		this.dsIndicadorInclusaoFavorecido = dsIndicadorInclusaoFavorecido;
	}


	/**
	 * Get: dsMotivoSituacaoSolicitacao.
	 *
	 * @return dsMotivoSituacaoSolicitacao
	 */
	public String getDsMotivoSituacaoSolicitacao() {
		return dsMotivoSituacaoSolicitacao;
	}


	/**
	 * Set: dsMotivoSituacaoSolicitacao.
	 *
	 * @param dsMotivoSituacaoSolicitacao the ds motivo situacao solicitacao
	 */
	public void setDsMotivoSituacaoSolicitacao(String dsMotivoSituacaoSolicitacao) {
		this.dsMotivoSituacaoSolicitacao = dsMotivoSituacaoSolicitacao;
	}


	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}


	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}


	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}


	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}


	/**
	 * Get: dsSituacaoSolicitacaoPagamento.
	 *
	 * @return dsSituacaoSolicitacaoPagamento
	 */
	public String getDsSituacaoSolicitacaoPagamento() {
		return dsSituacaoSolicitacaoPagamento;
	}


	/**
	 * Set: dsSituacaoSolicitacaoPagamento.
	 *
	 * @param dsSituacaoSolicitacaoPagamento the ds situacao solicitacao pagamento
	 */
	public void setDsSituacaoSolicitacaoPagamento(String dsSituacaoSolicitacaoPagamento) {
		this.dsSituacaoSolicitacaoPagamento = dsSituacaoSolicitacaoPagamento;
	}


	/**
	 * Get: dtAtendimento.
	 *
	 * @return dtAtendimento
	 */
	public String getDtAtendimento() {
		return dtAtendimento;
	}


	/**
	 * Set: dtAtendimento.
	 *
	 * @param dtAtendimento the dt atendimento
	 */
	public void setDtAtendimento(String dtAtendimento) {
		this.dtAtendimento = dtAtendimento;
	}


	/**
	 * Get: dtFimRastreabilidadeFavorecido.
	 *
	 * @return dtFimRastreabilidadeFavorecido
	 */
	public String getDtFimRastreabilidadeFavorecido() {
		return dtFimRastreabilidadeFavorecido;
	}


	/**
	 * Set: dtFimRastreabilidadeFavorecido.
	 *
	 * @param dtFimRastreabilidadeFavorecido the dt fim rastreabilidade favorecido
	 */
	public void setDtFimRastreabilidadeFavorecido(String dtFimRastreabilidadeFavorecido) {
		this.dtFimRastreabilidadeFavorecido = dtFimRastreabilidadeFavorecido;
	}


	/**
	 * Get: dtInclusao.
	 *
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}


	/**
	 * Set: dtInclusao.
	 *
	 * @param dtInclusao the dt inclusao
	 */
	public void setDtInclusao(String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}


	/**
	 * Get: dtInicioRastreabilidadeFavorecido.
	 *
	 * @return dtInicioRastreabilidadeFavorecido
	 */
	public String getDtInicioRastreabilidadeFavorecido() {
		return dtInicioRastreabilidadeFavorecido;
	}


	/**
	 * Set: dtInicioRastreabilidadeFavorecido.
	 *
	 * @param dtInicioRastreabilidadeFavorecido the dt inicio rastreabilidade favorecido
	 */
	public void setDtInicioRastreabilidadeFavorecido(String dtInicioRastreabilidadeFavorecido) {
		this.dtInicioRastreabilidadeFavorecido = dtInicioRastreabilidadeFavorecido;
	}


	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}


	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}


	/**
	 * Get: dtSolicitacao.
	 *
	 * @return dtSolicitacao
	 */
	public String getDtSolicitacao() {
		return dtSolicitacao;
	}


	/**
	 * Set: dtSolicitacao.
	 *
	 * @param dtSolicitacao the dt solicitacao
	 */
	public void setDtSolicitacao(String dtSolicitacao) {
		this.dtSolicitacao = dtSolicitacao;
	}


	/**
	 * Get: hrAtendimento.
	 *
	 * @return hrAtendimento
	 */
	public String getHrAtendimento() {
		return hrAtendimento;
	}


	/**
	 * Set: hrAtendimento.
	 *
	 * @param hrAtendimento the hr atendimento
	 */
	public void setHrAtendimento(String hrAtendimento) {
		this.hrAtendimento = hrAtendimento;
	}


	/**
	 * Get: hrInclusao.
	 *
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}


	/**
	 * Set: hrInclusao.
	 *
	 * @param hrInclusao the hr inclusao
	 */
	public void setHrInclusao(String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}


	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}


	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}


	/**
	 * Get: hrSolicitacao.
	 *
	 * @return hrSolicitacao
	 */
	public String getHrSolicitacao() {
		return hrSolicitacao;
	}


	/**
	 * Set: hrSolicitacao.
	 *
	 * @param hrSolicitacao the hr solicitacao
	 */
	public void setHrSolicitacao(String hrSolicitacao) {
		this.hrSolicitacao = hrSolicitacao;
	}


	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public Long getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}


	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}


	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}


	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}


	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}


	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}


	/**
	 * Get: nrTarifaBonificacao.
	 *
	 * @return nrTarifaBonificacao
	 */
	public BigDecimal getNrTarifaBonificacao() {
		return nrTarifaBonificacao;
	}


	/**
	 * Set: nrTarifaBonificacao.
	 *
	 * @param nrTarifaBonificacao the nr tarifa bonificacao
	 */
	public void setNrTarifaBonificacao(BigDecimal nrTarifaBonificacao) {
		this.nrTarifaBonificacao = nrTarifaBonificacao;
	}


	/**
	 * Get: cdFormaCadastroFavorecidos.
	 *
	 * @return cdFormaCadastroFavorecidos
	 */
	public Integer getCdFormaCadastroFavorecidos() {
		return cdFormaCadastroFavorecidos;
	}


	/**
	 * Set: cdFormaCadastroFavorecidos.
	 *
	 * @param cdFormaCadastroFavorecidos the cd forma cadastro favorecidos
	 */
	public void setCdFormaCadastroFavorecidos(Integer cdFormaCadastroFavorecidos) {
		this.cdFormaCadastroFavorecidos = cdFormaCadastroFavorecidos;
	}


	/**
	 * Get: dsFormaCadastroFavorecidos.
	 *
	 * @return dsFormaCadastroFavorecidos
	 */
	public String getDsFormaCadastroFavorecidos() {
		return dsFormaCadastroFavorecidos;
	}


	/**
	 * Set: dsFormaCadastroFavorecidos.
	 *
	 * @param dsFormaCadastroFavorecidos the ds forma cadastro favorecidos
	 */
	public void setDsFormaCadastroFavorecidos(String dsFormaCadastroFavorecidos) {
		this.dsFormaCadastroFavorecidos = dsFormaCadastroFavorecidos;
	}


	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}


	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}


	/**
	 * Get: numPercentualDescTarifa.
	 *
	 * @return numPercentualDescTarifa
	 */
	public BigDecimal getNumPercentualDescTarifa() {
		return numPercentualDescTarifa;
	}


	/**
	 * Set: numPercentualDescTarifa.
	 *
	 * @param numPercentualDescTarifa the num percentual desc tarifa
	 */
	public void setNumPercentualDescTarifa(BigDecimal numPercentualDescTarifa) {
		this.numPercentualDescTarifa = numPercentualDescTarifa;
	}


	/**
	 * Get: vlrTarifaAtual.
	 *
	 * @return vlrTarifaAtual
	 */
	public BigDecimal getVlrTarifaAtual() {
		return vlrTarifaAtual;
	}


	/**
	 * Set: vlrTarifaAtual.
	 *
	 * @param vlrTarifaAtual the vlr tarifa atual
	 */
	public void setVlrTarifaAtual(BigDecimal vlrTarifaAtual) {
		this.vlrTarifaAtual = vlrTarifaAtual;
	}

}
