/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ListarEstatisticaProcessoMassivoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ListarEstatisticaProcessoMassivoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSistema
     */
    private java.lang.String _cdSistema;

    /**
     * Field _cdTipoProcessoSistema
     */
    private int _cdTipoProcessoSistema = 0;

    /**
     * keeps track of state for field: _cdTipoProcessoSistema
     */
    private boolean _has_cdTipoProcessoSistema;

    /**
     * Field _dtInicioEstatisticaProcessoInicio
     */
    private java.lang.String _dtInicioEstatisticaProcessoInicio;

    /**
     * Field _hrInicioEstatisticaProcessoInicio
     */
    private java.lang.String _hrInicioEstatisticaProcessoInicio;

    /**
     * Field _dtInicioEstatisticaProcessoFim
     */
    private java.lang.String _dtInicioEstatisticaProcessoFim;

    /**
     * Field _hrInicioEstatisticaProcessoFim
     */
    private java.lang.String _hrInicioEstatisticaProcessoFim;

    /**
     * Field _cdSituacaoEstatisticaSistema
     */
    private int _cdSituacaoEstatisticaSistema = 0;

    /**
     * keeps track of state for field: _cdSituacaoEstatisticaSistema
     */
    private boolean _has_cdSituacaoEstatisticaSistema;

    /**
     * Field _qtOcorrencias
     */
    private int _qtOcorrencias = 0;

    /**
     * keeps track of state for field: _qtOcorrencias
     */
    private boolean _has_qtOcorrencias;


      //----------------/
     //- Constructors -/
    //----------------/

    public ListarEstatisticaProcessoMassivoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.request.ListarEstatisticaProcessoMassivoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoEstatisticaSistema
     * 
     */
    public void deleteCdSituacaoEstatisticaSistema()
    {
        this._has_cdSituacaoEstatisticaSistema= false;
    } //-- void deleteCdSituacaoEstatisticaSistema() 

    /**
     * Method deleteCdTipoProcessoSistema
     * 
     */
    public void deleteCdTipoProcessoSistema()
    {
        this._has_cdTipoProcessoSistema= false;
    } //-- void deleteCdTipoProcessoSistema() 

    /**
     * Method deleteQtOcorrencias
     * 
     */
    public void deleteQtOcorrencias()
    {
        this._has_qtOcorrencias= false;
    } //-- void deleteQtOcorrencias() 

    /**
     * Returns the value of field 'cdSistema'.
     * 
     * @return String
     * @return the value of field 'cdSistema'.
     */
    public java.lang.String getCdSistema()
    {
        return this._cdSistema;
    } //-- java.lang.String getCdSistema() 

    /**
     * Returns the value of field 'cdSituacaoEstatisticaSistema'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoEstatisticaSistema'.
     */
    public int getCdSituacaoEstatisticaSistema()
    {
        return this._cdSituacaoEstatisticaSistema;
    } //-- int getCdSituacaoEstatisticaSistema() 

    /**
     * Returns the value of field 'cdTipoProcessoSistema'.
     * 
     * @return int
     * @return the value of field 'cdTipoProcessoSistema'.
     */
    public int getCdTipoProcessoSistema()
    {
        return this._cdTipoProcessoSistema;
    } //-- int getCdTipoProcessoSistema() 

    /**
     * Returns the value of field 'dtInicioEstatisticaProcessoFim'.
     * 
     * @return String
     * @return the value of field 'dtInicioEstatisticaProcessoFim'.
     */
    public java.lang.String getDtInicioEstatisticaProcessoFim()
    {
        return this._dtInicioEstatisticaProcessoFim;
    } //-- java.lang.String getDtInicioEstatisticaProcessoFim() 

    /**
     * Returns the value of field
     * 'dtInicioEstatisticaProcessoInicio'.
     * 
     * @return String
     * @return the value of field
     * 'dtInicioEstatisticaProcessoInicio'.
     */
    public java.lang.String getDtInicioEstatisticaProcessoInicio()
    {
        return this._dtInicioEstatisticaProcessoInicio;
    } //-- java.lang.String getDtInicioEstatisticaProcessoInicio() 

    /**
     * Returns the value of field 'hrInicioEstatisticaProcessoFim'.
     * 
     * @return String
     * @return the value of field 'hrInicioEstatisticaProcessoFim'.
     */
    public java.lang.String getHrInicioEstatisticaProcessoFim()
    {
        return this._hrInicioEstatisticaProcessoFim;
    } //-- java.lang.String getHrInicioEstatisticaProcessoFim() 

    /**
     * Returns the value of field
     * 'hrInicioEstatisticaProcessoInicio'.
     * 
     * @return String
     * @return the value of field
     * 'hrInicioEstatisticaProcessoInicio'.
     */
    public java.lang.String getHrInicioEstatisticaProcessoInicio()
    {
        return this._hrInicioEstatisticaProcessoInicio;
    } //-- java.lang.String getHrInicioEstatisticaProcessoInicio() 

    /**
     * Returns the value of field 'qtOcorrencias'.
     * 
     * @return int
     * @return the value of field 'qtOcorrencias'.
     */
    public int getQtOcorrencias()
    {
        return this._qtOcorrencias;
    } //-- int getQtOcorrencias() 

    /**
     * Method hasCdSituacaoEstatisticaSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoEstatisticaSistema()
    {
        return this._has_cdSituacaoEstatisticaSistema;
    } //-- boolean hasCdSituacaoEstatisticaSistema() 

    /**
     * Method hasCdTipoProcessoSistema
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoProcessoSistema()
    {
        return this._has_cdTipoProcessoSistema;
    } //-- boolean hasCdTipoProcessoSistema() 

    /**
     * Method hasQtOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtOcorrencias()
    {
        return this._has_qtOcorrencias;
    } //-- boolean hasQtOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSistema'.
     * 
     * @param cdSistema the value of field 'cdSistema'.
     */
    public void setCdSistema(java.lang.String cdSistema)
    {
        this._cdSistema = cdSistema;
    } //-- void setCdSistema(java.lang.String) 

    /**
     * Sets the value of field 'cdSituacaoEstatisticaSistema'.
     * 
     * @param cdSituacaoEstatisticaSistema the value of field
     * 'cdSituacaoEstatisticaSistema'.
     */
    public void setCdSituacaoEstatisticaSistema(int cdSituacaoEstatisticaSistema)
    {
        this._cdSituacaoEstatisticaSistema = cdSituacaoEstatisticaSistema;
        this._has_cdSituacaoEstatisticaSistema = true;
    } //-- void setCdSituacaoEstatisticaSistema(int) 

    /**
     * Sets the value of field 'cdTipoProcessoSistema'.
     * 
     * @param cdTipoProcessoSistema the value of field
     * 'cdTipoProcessoSistema'.
     */
    public void setCdTipoProcessoSistema(int cdTipoProcessoSistema)
    {
        this._cdTipoProcessoSistema = cdTipoProcessoSistema;
        this._has_cdTipoProcessoSistema = true;
    } //-- void setCdTipoProcessoSistema(int) 

    /**
     * Sets the value of field 'dtInicioEstatisticaProcessoFim'.
     * 
     * @param dtInicioEstatisticaProcessoFim the value of field
     * 'dtInicioEstatisticaProcessoFim'.
     */
    public void setDtInicioEstatisticaProcessoFim(java.lang.String dtInicioEstatisticaProcessoFim)
    {
        this._dtInicioEstatisticaProcessoFim = dtInicioEstatisticaProcessoFim;
    } //-- void setDtInicioEstatisticaProcessoFim(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioEstatisticaProcessoInicio'.
     * 
     * @param dtInicioEstatisticaProcessoInicio the value of field
     * 'dtInicioEstatisticaProcessoInicio'.
     */
    public void setDtInicioEstatisticaProcessoInicio(java.lang.String dtInicioEstatisticaProcessoInicio)
    {
        this._dtInicioEstatisticaProcessoInicio = dtInicioEstatisticaProcessoInicio;
    } //-- void setDtInicioEstatisticaProcessoInicio(java.lang.String) 

    /**
     * Sets the value of field 'hrInicioEstatisticaProcessoFim'.
     * 
     * @param hrInicioEstatisticaProcessoFim the value of field
     * 'hrInicioEstatisticaProcessoFim'.
     */
    public void setHrInicioEstatisticaProcessoFim(java.lang.String hrInicioEstatisticaProcessoFim)
    {
        this._hrInicioEstatisticaProcessoFim = hrInicioEstatisticaProcessoFim;
    } //-- void setHrInicioEstatisticaProcessoFim(java.lang.String) 

    /**
     * Sets the value of field 'hrInicioEstatisticaProcessoInicio'.
     * 
     * @param hrInicioEstatisticaProcessoInicio the value of field
     * 'hrInicioEstatisticaProcessoInicio'.
     */
    public void setHrInicioEstatisticaProcessoInicio(java.lang.String hrInicioEstatisticaProcessoInicio)
    {
        this._hrInicioEstatisticaProcessoInicio = hrInicioEstatisticaProcessoInicio;
    } //-- void setHrInicioEstatisticaProcessoInicio(java.lang.String) 

    /**
     * Sets the value of field 'qtOcorrencias'.
     * 
     * @param qtOcorrencias the value of field 'qtOcorrencias'.
     */
    public void setQtOcorrencias(int qtOcorrencias)
    {
        this._qtOcorrencias = qtOcorrencias;
        this._has_qtOcorrencias = true;
    } //-- void setQtOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ListarEstatisticaProcessoMassivoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.request.ListarEstatisticaProcessoMassivoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.request.ListarEstatisticaProcessoMassivoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.request.ListarEstatisticaProcessoMassivoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarestatisticaprocessomassivo.request.ListarEstatisticaProcessoMassivoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
