/*
 * Nome: br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean;

import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * Nome: OcorrenciasOperacoes
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciasOperacoes {
	
	/** Atributo dsOpgTipoServico. */
	private String dsOpgTipoServico;
	
	/** Atributo dsOpgTipoConsultaSaldo. */
	private String dsOpgTipoConsultaSaldo;
    
    /** Atributo dsOpgTipoProcessamento. */
    private String dsOpgTipoProcessamento;
    
    /** Atributo dsOpgTipoTratamentoFeriado. */
    private String dsOpgTipoTratamentoFeriado;
    
    /** Atributo dsOpgTipoRejeicaoEfetivacao. */
    private String dsOpgTipoRejeicaoEfetivacao;
    
    /** Atributo dsOpgTiporejeicaoAgendado. */
    private String dsOpgTiporejeicaoAgendado;
    
    /** Atributo cdOpgIndicadorPercentualMaximo. */
    private String cdOpgIndicadorPercentualMaximo;
    
    /** Atributo pcOpgMaximoInconsistencia. */
    private Integer pcOpgMaximoInconsistencia;
    
    /** Atributo cdOpgIndicadorQuantidadeMaximo. */
    private String cdOpgIndicadorQuantidadeMaximo;
    
    /** Atributo qtOpgMaximoInconsistencia. */
    private Integer qtOpgMaximoInconsistencia;
    
    /** Atributo cdOpgIndicadorValorDia. */
    private String cdOpgIndicadorValorDia;
    
    /** Atributo vlOpgLimiteDiario. */
    private BigDecimal vlOpgLimiteDiario;
    
    /** Atributo cdOpgIndicadorValorIndividual. */
    private String cdOpgIndicadorValorIndividual;
    
    /** Atributo vlOpgLimiteIndividual. */
    private BigDecimal vlOpgLimiteIndividual;
    
    /** Atributo cdOpgIndicadorQuantidadeFloating. */
    private String cdOpgIndicadorQuantidadeFloating;
    
    /** Atributo qtOpgDiaFloating. */
    private Integer qtOpgDiaFloating;
    
    /** Atributo dsOpgPrioridadeDebito. */
    private String dsOpgPrioridadeDebito;
    
    /** Atributo cdOpgIndicadorValorMaximo. */
    private String cdOpgIndicadorValorMaximo;
    
    /** Atributo vlOpgMaximoFavorecidoNao. */
    private BigDecimal vlOpgMaximoFavorecidoNao;
    
    /** Atributo cdOpgIndicadorCadastroFavorecido. */
    private String cdOpgIndicadorCadastroFavorecido;
    
    /** Atributo cdOpgUtilizacaoCadastroFavorecido. */
    private String cdOpgUtilizacaoCadastroFavorecido;
    
    /** Atributo dsOpgOcorrenciasDebito. */
    private String dsOpgOcorrenciasDebito;
    
    /** Atributo dsOpgTipoInscricao. */
    private String dsOpgTipoInscricao;
    
    /** Atributo cdOpgIndicadorQuantidadeRepique. */
    private String cdOpgIndicadorQuantidadeRepique;
    
    /** Atributo qtOpgDiaRepique. */
    private Integer qtOpgDiaRepique;
    
    /** Atributo qtOpgDiaExpiracao. */
    private Integer qtOpgDiaExpiracao;
    
    /** The ds tipo cons favorecido. */
    private String dsTipoConsFavorecido;
    
    /** The cd feri loc. */
    private String cdFeriLoc;       
    
    /** The o pg emissao. */
    private String oPgEmissao;          
	/**
	 * Get: dsOpgTipoServico.
	 *
	 * @return dsOpgTipoServico
	 */
	public String getDsOpgTipoServico() {
		return dsOpgTipoServico;
	}
	
	/**
	 * Set: dsOpgTipoServico.
	 *
	 * @param dsOpgTipoServico the ds opg tipo servico
	 */
	public void setDsOpgTipoServico(String dsOpgTipoServico) {
		this.dsOpgTipoServico = dsOpgTipoServico;
	}
	
	/**
	 * Get: dsOpgTipoConsultaSaldo.
	 *
	 * @return dsOpgTipoConsultaSaldo
	 */
	public String getDsOpgTipoConsultaSaldo() {
		return dsOpgTipoConsultaSaldo;
	}
	
	/**
	 * Set: dsOpgTipoConsultaSaldo.
	 *
	 * @param dsOpgTipoConsultaSaldo the ds opg tipo consulta saldo
	 */
	public void setDsOpgTipoConsultaSaldo(String dsOpgTipoConsultaSaldo) {
		this.dsOpgTipoConsultaSaldo = dsOpgTipoConsultaSaldo;
	}
	
	/**
	 * Get: dsOpgTipoProcessamento.
	 *
	 * @return dsOpgTipoProcessamento
	 */
	public String getDsOpgTipoProcessamento() {
		return dsOpgTipoProcessamento;
	}
	
	/**
	 * Set: dsOpgTipoProcessamento.
	 *
	 * @param dsOpgTipoProcessamento the ds opg tipo processamento
	 */
	public void setDsOpgTipoProcessamento(String dsOpgTipoProcessamento) {
		this.dsOpgTipoProcessamento = dsOpgTipoProcessamento;
	}
	
	/**
	 * Get: dsOpgTipoTratamentoFeriado.
	 *
	 * @return dsOpgTipoTratamentoFeriado
	 */
	public String getDsOpgTipoTratamentoFeriado() {
		return dsOpgTipoTratamentoFeriado;
	}
	
	/**
	 * Set: dsOpgTipoTratamentoFeriado.
	 *
	 * @param dsOpgTipoTratamentoFeriado the ds opg tipo tratamento feriado
	 */
	public void setDsOpgTipoTratamentoFeriado(String dsOpgTipoTratamentoFeriado) {
		this.dsOpgTipoTratamentoFeriado = dsOpgTipoTratamentoFeriado;
	}
	
	/**
	 * Get: dsOpgTipoRejeicaoEfetivacao.
	 *
	 * @return dsOpgTipoRejeicaoEfetivacao
	 */
	public String getDsOpgTipoRejeicaoEfetivacao() {
		return dsOpgTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Set: dsOpgTipoRejeicaoEfetivacao.
	 *
	 * @param dsOpgTipoRejeicaoEfetivacao the ds opg tipo rejeicao efetivacao
	 */
	public void setDsOpgTipoRejeicaoEfetivacao(String dsOpgTipoRejeicaoEfetivacao) {
		this.dsOpgTipoRejeicaoEfetivacao = dsOpgTipoRejeicaoEfetivacao;
	}
	
	/**
	 * Get: dsOpgTiporejeicaoAgendado.
	 *
	 * @return dsOpgTiporejeicaoAgendado
	 */
	public String getDsOpgTiporejeicaoAgendado() {
		return dsOpgTiporejeicaoAgendado;
	}
	
	/**
	 * Set: dsOpgTiporejeicaoAgendado.
	 *
	 * @param dsOpgTiporejeicaoAgendado the ds opg tiporejeicao agendado
	 */
	public void setDsOpgTiporejeicaoAgendado(String dsOpgTiporejeicaoAgendado) {
		this.dsOpgTiporejeicaoAgendado = dsOpgTiporejeicaoAgendado;
	}
	
	/**
	 * Get: cdOpgIndicadorPercentualMaximo.
	 *
	 * @return cdOpgIndicadorPercentualMaximo
	 */
	public String getCdOpgIndicadorPercentualMaximo() {
		return cdOpgIndicadorPercentualMaximo;
	}
	
	/**
	 * Set: cdOpgIndicadorPercentualMaximo.
	 *
	 * @param cdOpgIndicadorPercentualMaximo the cd opg indicador percentual maximo
	 */
	public void setCdOpgIndicadorPercentualMaximo(
			String cdOpgIndicadorPercentualMaximo) {
		this.cdOpgIndicadorPercentualMaximo = cdOpgIndicadorPercentualMaximo;
	}
	
	/**
	 * Get: pcOpgMaximoInconsistencia.
	 *
	 * @return pcOpgMaximoInconsistencia
	 */
	public Integer getPcOpgMaximoInconsistencia() {
		return pcOpgMaximoInconsistencia;
	}
	
	/**
	 * Set: pcOpgMaximoInconsistencia.
	 *
	 * @param pcOpgMaximoInconsistencia the pc opg maximo inconsistencia
	 */
	public void setPcOpgMaximoInconsistencia(Integer pcOpgMaximoInconsistencia) {
		this.pcOpgMaximoInconsistencia = pcOpgMaximoInconsistencia;
	}
	
	/**
	 * Get: cdOpgIndicadorQuantidadeMaximo.
	 *
	 * @return cdOpgIndicadorQuantidadeMaximo
	 */
	public String getCdOpgIndicadorQuantidadeMaximo() {
		return cdOpgIndicadorQuantidadeMaximo;
	}
	
	/**
	 * Set: cdOpgIndicadorQuantidadeMaximo.
	 *
	 * @param cdOpgIndicadorQuantidadeMaximo the cd opg indicador quantidade maximo
	 */
	public void setCdOpgIndicadorQuantidadeMaximo(
			String cdOpgIndicadorQuantidadeMaximo) {
		this.cdOpgIndicadorQuantidadeMaximo = cdOpgIndicadorQuantidadeMaximo;
	}
	
	/**
	 * Get: qtOpgMaximoInconsistencia.
	 *
	 * @return qtOpgMaximoInconsistencia
	 */
	public Integer getQtOpgMaximoInconsistencia() {
		return qtOpgMaximoInconsistencia;
	}
	
	/**
	 * Set: qtOpgMaximoInconsistencia.
	 *
	 * @param qtOpgMaximoInconsistencia the qt opg maximo inconsistencia
	 */
	public void setQtOpgMaximoInconsistencia(Integer qtOpgMaximoInconsistencia) {
		this.qtOpgMaximoInconsistencia = qtOpgMaximoInconsistencia;
	}
	
	/**
	 * Get: cdOpgIndicadorValorDia.
	 *
	 * @return cdOpgIndicadorValorDia
	 */
	public String getCdOpgIndicadorValorDia() {
		return cdOpgIndicadorValorDia;
	}
	
	/**
	 * Set: cdOpgIndicadorValorDia.
	 *
	 * @param cdOpgIndicadorValorDia the cd opg indicador valor dia
	 */
	public void setCdOpgIndicadorValorDia(String cdOpgIndicadorValorDia) {
		this.cdOpgIndicadorValorDia = cdOpgIndicadorValorDia;
	}
	
	/**
	 * Get: vlOpgLimiteDiario.
	 *
	 * @return vlOpgLimiteDiario
	 */
	public BigDecimal getVlOpgLimiteDiario() {
		return vlOpgLimiteDiario;
	}
	
	/**
	 * Set: vlOpgLimiteDiario.
	 *
	 * @param vlOpgLimiteDiario the vl opg limite diario
	 */
	public void setVlOpgLimiteDiario(BigDecimal vlOpgLimiteDiario) {
		this.vlOpgLimiteDiario = vlOpgLimiteDiario;
	}
	
	/**
	 * Get: cdOpgIndicadorValorIndividual.
	 *
	 * @return cdOpgIndicadorValorIndividual
	 */
	public String getCdOpgIndicadorValorIndividual() {
		return cdOpgIndicadorValorIndividual;
	}
	
	/**
	 * Set: cdOpgIndicadorValorIndividual.
	 *
	 * @param cdOpgIndicadorValorIndividual the cd opg indicador valor individual
	 */
	public void setCdOpgIndicadorValorIndividual(
			String cdOpgIndicadorValorIndividual) {
		this.cdOpgIndicadorValorIndividual = cdOpgIndicadorValorIndividual;
	}
	
	/**
	 * Get: vlOpgLimiteIndividual.
	 *
	 * @return vlOpgLimiteIndividual
	 */
	public BigDecimal getVlOpgLimiteIndividual() {
		return vlOpgLimiteIndividual;
	}
	
	/**
	 * Set: vlOpgLimiteIndividual.
	 *
	 * @param vlOpgLimiteIndividual the vl opg limite individual
	 */
	public void setVlOpgLimiteIndividual(BigDecimal vlOpgLimiteIndividual) {
		this.vlOpgLimiteIndividual = vlOpgLimiteIndividual;
	}
	
	/**
	 * Get: cdOpgIndicadorQuantidadeFloating.
	 *
	 * @return cdOpgIndicadorQuantidadeFloating
	 */
	public String getCdOpgIndicadorQuantidadeFloating() {
		return cdOpgIndicadorQuantidadeFloating;
	}
	
	/**
	 * Set: cdOpgIndicadorQuantidadeFloating.
	 *
	 * @param cdOpgIndicadorQuantidadeFloating the cd opg indicador quantidade floating
	 */
	public void setCdOpgIndicadorQuantidadeFloating(
			String cdOpgIndicadorQuantidadeFloating) {
		this.cdOpgIndicadorQuantidadeFloating = cdOpgIndicadorQuantidadeFloating;
	}
	
	/**
	 * Get: qtOpgDiaFloating.
	 *
	 * @return qtOpgDiaFloating
	 */
	public Integer getQtOpgDiaFloating() {
		return qtOpgDiaFloating;
	}
	
	/**
	 * Set: qtOpgDiaFloating.
	 *
	 * @param qtOpgDiaFloating the qt opg dia floating
	 */
	public void setQtOpgDiaFloating(Integer qtOpgDiaFloating) {
		this.qtOpgDiaFloating = qtOpgDiaFloating;
	}
	
	/**
	 * Get: dsOpgPrioridadeDebito.
	 *
	 * @return dsOpgPrioridadeDebito
	 */
	public String getDsOpgPrioridadeDebito() {
		return dsOpgPrioridadeDebito;
	}
	
	/**
	 * Set: dsOpgPrioridadeDebito.
	 *
	 * @param dsOpgPrioridadeDebito the ds opg prioridade debito
	 */
	public void setDsOpgPrioridadeDebito(String dsOpgPrioridadeDebito) {
		this.dsOpgPrioridadeDebito = dsOpgPrioridadeDebito;
	}
	
	/**
	 * Get: cdOpgIndicadorValorMaximo.
	 *
	 * @return cdOpgIndicadorValorMaximo
	 */
	public String getCdOpgIndicadorValorMaximo() {
		return cdOpgIndicadorValorMaximo;
	}
	
	/**
	 * Set: cdOpgIndicadorValorMaximo.
	 *
	 * @param cdOpgIndicadorValorMaximo the cd opg indicador valor maximo
	 */
	public void setCdOpgIndicadorValorMaximo(String cdOpgIndicadorValorMaximo) {
		this.cdOpgIndicadorValorMaximo = cdOpgIndicadorValorMaximo;
	}
	
	/**
	 * Get: vlOpgMaximoFavorecidoNao.
	 *
	 * @return vlOpgMaximoFavorecidoNao
	 */
	public BigDecimal getVlOpgMaximoFavorecidoNao() {
		return vlOpgMaximoFavorecidoNao;
	}
	
	/**
	 * Set: vlOpgMaximoFavorecidoNao.
	 *
	 * @param vlOpgMaximoFavorecidoNao the vl opg maximo favorecido nao
	 */
	public void setVlOpgMaximoFavorecidoNao(BigDecimal vlOpgMaximoFavorecidoNao) {
		this.vlOpgMaximoFavorecidoNao = vlOpgMaximoFavorecidoNao;
	}
	
	/**
	 * Get: cdOpgIndicadorCadastroFavorecido.
	 *
	 * @return cdOpgIndicadorCadastroFavorecido
	 */
	public String getCdOpgIndicadorCadastroFavorecido() {
		return cdOpgIndicadorCadastroFavorecido;
	}
	
	/**
	 * Set: cdOpgIndicadorCadastroFavorecido.
	 *
	 * @param cdOpgIndicadorCadastroFavorecido the cd opg indicador cadastro favorecido
	 */
	public void setCdOpgIndicadorCadastroFavorecido(
			String cdOpgIndicadorCadastroFavorecido) {
		this.cdOpgIndicadorCadastroFavorecido = cdOpgIndicadorCadastroFavorecido;
	}
	
	/**
	 * Get: cdOpgUtilizacaoCadastroFavorecido.
	 *
	 * @return cdOpgUtilizacaoCadastroFavorecido
	 */
	public String getCdOpgUtilizacaoCadastroFavorecido() {
		return cdOpgUtilizacaoCadastroFavorecido;
	}
	
	/**
	 * Set: cdOpgUtilizacaoCadastroFavorecido.
	 *
	 * @param cdOpgUtilizacaoCadastroFavorecido the cd opg utilizacao cadastro favorecido
	 */
	public void setCdOpgUtilizacaoCadastroFavorecido(
			String cdOpgUtilizacaoCadastroFavorecido) {
		this.cdOpgUtilizacaoCadastroFavorecido = cdOpgUtilizacaoCadastroFavorecido;
	}
	
	/**
	 * Get: dsOpgOcorrenciasDebito.
	 *
	 * @return dsOpgOcorrenciasDebito
	 */
	public String getDsOpgOcorrenciasDebito() {
		return dsOpgOcorrenciasDebito;
	}
	
	/**
	 * Set: dsOpgOcorrenciasDebito.
	 *
	 * @param dsOpgOcorrenciasDebito the ds opg ocorrencias debito
	 */
	public void setDsOpgOcorrenciasDebito(String dsOpgOcorrenciasDebito) {
		this.dsOpgOcorrenciasDebito = dsOpgOcorrenciasDebito;
	}
	
	/**
	 * Get: dsOpgTipoInscricao.
	 *
	 * @return dsOpgTipoInscricao
	 */
	public String getDsOpgTipoInscricao() {
		return dsOpgTipoInscricao;
	}
	
	/**
	 * Set: dsOpgTipoInscricao.
	 *
	 * @param dsOpgTipoInscricao the ds opg tipo inscricao
	 */
	public void setDsOpgTipoInscricao(String dsOpgTipoInscricao) {
		this.dsOpgTipoInscricao = dsOpgTipoInscricao;
	}
	
	/**
	 * Get: cdOpgIndicadorQuantidadeRepique.
	 *
	 * @return cdOpgIndicadorQuantidadeRepique
	 */
	public String getCdOpgIndicadorQuantidadeRepique() {
		return cdOpgIndicadorQuantidadeRepique;
	}
	
	/**
	 * Set: cdOpgIndicadorQuantidadeRepique.
	 *
	 * @param cdOpgIndicadorQuantidadeRepique the cd opg indicador quantidade repique
	 */
	public void setCdOpgIndicadorQuantidadeRepique(
			String cdOpgIndicadorQuantidadeRepique) {
		this.cdOpgIndicadorQuantidadeRepique = cdOpgIndicadorQuantidadeRepique;
	}
	
	/**
	 * Get: qtOpgDiaRepique.
	 *
	 * @return qtOpgDiaRepique
	 */
	public Integer getQtOpgDiaRepique() {
		return qtOpgDiaRepique;
	}
	
	/**
	 * Set: qtOpgDiaRepique.
	 *
	 * @param qtOpgDiaRepique the qt opg dia repique
	 */
	public void setQtOpgDiaRepique(Integer qtOpgDiaRepique) {
		this.qtOpgDiaRepique = qtOpgDiaRepique;
	}
	
	/**
	 * Get: qtOpgDiaExpiracao.
	 *
	 * @return qtOpgDiaExpiracao
	 */
	public Integer getQtOpgDiaExpiracao() {
		return qtOpgDiaExpiracao;
	}
	
	/**
	 * Set: qtOpgDiaExpiracao.
	 *
	 * @param qtOpgDiaExpiracao the qt opg dia expiracao
	 */
	public void setQtOpgDiaExpiracao(Integer qtOpgDiaExpiracao) {
		this.qtOpgDiaExpiracao = qtOpgDiaExpiracao;
	}

	/**
	 * Gets the ds tipo cons favorecido.
	 *
	 * @return the dsTipoConsFavorecido
	 */
	public String getDsTipoConsFavorecido() {
		return dsTipoConsFavorecido;
	}

	/**
	 * Sets the ds tipo cons favorecido.
	 *
	 * @param dsTipoConsFavorecido the dsTipoConsFavorecido to set
	 */
	public void setDsTipoConsFavorecido(String dsTipoConsFavorecido) {
		this.dsTipoConsFavorecido = dsTipoConsFavorecido;
	}

	/**
	 * Gets the cd feri loc.
	 *
	 * @return the cdFeriLoc
	 */
	public String getCdFeriLoc() {
		return cdFeriLoc;
	}

	/**
	 * Sets the cd feri loc.
	 *
	 * @param cdFeriLoc the cdFeriLoc to set
	 */
	public void setCdFeriLoc(String cdFeriLoc) {
		this.cdFeriLoc = cdFeriLoc;
	}

	/**
	 * Gets the o pg emissao.
	 *
	 * @return the oPgEmissao
	 */
	public String getoPgEmissao() {
		return oPgEmissao;
	}

	/**
	 * Sets the o pg emissao.
	 *
	 * @param oPgEmissao the oPgEmissao to set
	 */
	public void setoPgEmissao(String oPgEmissao) {
		this.oPgEmissao = oPgEmissao;
	}
    
}
