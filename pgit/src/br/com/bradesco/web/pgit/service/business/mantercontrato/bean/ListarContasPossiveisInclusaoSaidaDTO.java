/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarContasPossiveisInclusaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ListarContasPossiveisInclusaoSaidaDTO implements Cloneable {

    /** Atributo codMensagem. */
    private String codMensagem;

    /** Atributo mensagem. */
    private String mensagem;

    /** Atributo cdPessoaJuridicaVinculo. */
    private Long cdPessoaJuridicaVinculo;

    /** Atributo cdTipoContratoVinculo. */
    private Integer cdTipoContratoVinculo;

    /** Atributo nrSequenciaContratoVinculo. */
    private Long nrSequenciaContratoVinculo;

    /** Atributo cdCpfCnpj. */
    private String cdCpfCnpj;

    /** Atributo nmParticipante. */
    private String nmParticipante;

    /** Atributo cdBanco. */
    private Integer cdBanco;

    /** Atributo dsBanco. */
    private String dsBanco;

    /** Atributo cdAgencia. */
    private Integer cdAgencia;

    /** Atributo dsAgencia. */
    private String dsAgencia;

    /** Atributo cdConta. */
    private Long cdConta;

    /** Atributo cdSituacaoConta. */
    private Integer cdSituacaoConta;

    /** Atributo dsSituacaoConta. */
    private String dsSituacaoConta;

    /** Atributo cdPessoaVinc. */
    private Long cdPessoaVinc;

    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;

    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;

    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;

    /** Atributo dsTipoConta. */
    private String dsTipoConta;

    /** Atributo bancoFormatado. */
    private String bancoFormatado;

    /** Atributo agenciaFormatada. */
    private String agenciaFormatada;

    /** Atributo contaFormatada. */
    private String contaFormatada;

    private boolean check;

    /**
     * Get: agenciaFormatada.
     * 
     * @return agenciaFormatada
     */
    public String getAgenciaFormatada() {
        return agenciaFormatada;
    }

    /**
     * Set: agenciaFormatada.
     * 
     * @param agenciaFormatada
     *            the agencia formatada
     */
    public void setAgenciaFormatada(String agenciaFormatada) {
        this.agenciaFormatada = agenciaFormatada;
    }

    /**
     * Get: bancoFormatado.
     * 
     * @return bancoFormatado
     */
    public String getBancoFormatado() {
        return bancoFormatado;
    }

    /**
     * Set: bancoFormatado.
     * 
     * @param bancoFormatado
     *            the banco formatado
     */
    public void setBancoFormatado(String bancoFormatado) {
        this.bancoFormatado = bancoFormatado;
    }

    /**
     * Get: contaFormatada.
     * 
     * @return contaFormatada
     */
    public String getContaFormatada() {
        return contaFormatada;
    }

    /**
     * Set: contaFormatada.
     * 
     * @param contaFormatada
     *            the conta formatada
     */
    public void setContaFormatada(String contaFormatada) {
        this.contaFormatada = contaFormatada;
    }

    /**
     * Get: cdDigitoAgencia.
     * 
     * @return cdDigitoAgencia
     */
    public Integer getCdDigitoAgencia() {
        return cdDigitoAgencia;
    }

    /**
     * Set: cdDigitoAgencia.
     * 
     * @param cdDigitoAgencia
     *            the cd digito agencia
     */
    public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
        this.cdDigitoAgencia = cdDigitoAgencia;
    }

    /**
     * Get: cdDigitoConta.
     * 
     * @return cdDigitoConta
     */
    public String getCdDigitoConta() {
        return cdDigitoConta;
    }

    /**
     * Set: cdDigitoConta.
     * 
     * @param cdDigitoConta
     *            the cd digito conta
     */
    public void setCdDigitoConta(String cdDigitoConta) {
        this.cdDigitoConta = cdDigitoConta;
    }

    /**
     * Get: cdTipoConta.
     * 
     * @return cdTipoConta
     */
    public Integer getCdTipoConta() {
        return cdTipoConta;
    }

    /**
     * Set: cdTipoConta.
     * 
     * @param cdTipoConta
     *            the cd tipo conta
     */
    public void setCdTipoConta(Integer cdTipoConta) {
        this.cdTipoConta = cdTipoConta;
    }

    /**
     * Get: dsTipoConta.
     * 
     * @return dsTipoConta
     */
    public String getDsTipoConta() {
        return dsTipoConta;
    }

    /**
     * Set: dsTipoConta.
     * 
     * @param dsTipoConta
     *            the ds tipo conta
     */
    public void setDsTipoConta(String dsTipoConta) {
        this.dsTipoConta = dsTipoConta;
    }

    /**
     * Get: cdSituacaoConta.
     * 
     * @return cdSituacaoConta
     */
    public Integer getCdSituacaoConta() {
        return cdSituacaoConta;
    }

    /**
     * Set: cdSituacaoConta.
     * 
     * @param cdSituacaoConta
     *            the cd situacao conta
     */
    public void setCdSituacaoConta(Integer cdSituacaoConta) {
        this.cdSituacaoConta = cdSituacaoConta;
    }

    /**
     * Get: dsSituacaoConta.
     * 
     * @return dsSituacaoConta
     */
    public String getDsSituacaoConta() {
        return dsSituacaoConta;
    }

    /**
     * Set: dsSituacaoConta.
     * 
     * @param dsSituacaoConta
     *            the ds situacao conta
     */
    public void setDsSituacaoConta(String dsSituacaoConta) {
        this.dsSituacaoConta = dsSituacaoConta;
    }

    /**
     * Get: cdAgencia.
     * 
     * @return cdAgencia
     */
    public Integer getCdAgencia() {
        return cdAgencia;
    }

    /**
     * Set: cdAgencia.
     * 
     * @param cdAgencia
     *            the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
        this.cdAgencia = cdAgencia;
    }

    /**
     * Get: cdBanco.
     * 
     * @return cdBanco
     */
    public Integer getCdBanco() {
        return cdBanco;
    }

    /**
     * Set: cdBanco.
     * 
     * @param cdBanco
     *            the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
        this.cdBanco = cdBanco;
    }

    /**
     * Get: cdConta.
     * 
     * @return cdConta
     */
    public Long getCdConta() {
        return cdConta;
    }

    /**
     * Set: cdConta.
     * 
     * @param cdConta
     *            the cd conta
     */
    public void setCdConta(Long cdConta) {
        this.cdConta = cdConta;
    }

    /**
     * Get: cdCpfCnpj.
     * 
     * @return cdCpfCnpj
     */
    public String getCdCpfCnpj() {
        return cdCpfCnpj;
    }

    /**
     * Set: cdCpfCnpj.
     * 
     * @param cdCpfCnpj
     *            the cd cpf cnpj
     */
    public void setCdCpfCnpj(String cdCpfCnpj) {
        this.cdCpfCnpj = cdCpfCnpj;
    }

    /**
     * Get: cdPessoaJuridicaVinculo.
     * 
     * @return cdPessoaJuridicaVinculo
     */
    public Long getCdPessoaJuridicaVinculo() {
        return cdPessoaJuridicaVinculo;
    }

    /**
     * Set: cdPessoaJuridicaVinculo.
     * 
     * @param cdPessoaJuridicaVinculo
     *            the cd pessoa juridica vinculo
     */
    public void setCdPessoaJuridicaVinculo(Long cdPessoaJuridicaVinculo) {
        this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
    }

    /**
     * Get: cdTipoContratoVinculo.
     * 
     * @return cdTipoContratoVinculo
     */
    public Integer getCdTipoContratoVinculo() {
        return cdTipoContratoVinculo;
    }

    /**
     * Set: cdTipoContratoVinculo.
     * 
     * @param cdTipoContratoVinculo
     *            the cd tipo contrato vinculo
     */
    public void setCdTipoContratoVinculo(Integer cdTipoContratoVinculo) {
        this.cdTipoContratoVinculo = cdTipoContratoVinculo;
    }

    /**
     * Get: codMensagem.
     * 
     * @return codMensagem
     */
    public String getCodMensagem() {
        return codMensagem;
    }

    /**
     * Set: codMensagem.
     * 
     * @param codMensagem
     *            the cod mensagem
     */
    public void setCodMensagem(String codMensagem) {
        this.codMensagem = codMensagem;
    }

    /**
     * Get: dsAgencia.
     * 
     * @return dsAgencia
     */
    public String getDsAgencia() {
        return dsAgencia;
    }

    /**
     * Set: dsAgencia.
     * 
     * @param dsAgencia
     *            the ds agencia
     */
    public void setDsAgencia(String dsAgencia) {
        this.dsAgencia = dsAgencia;
    }

    /**
     * Get: dsBanco.
     * 
     * @return dsBanco
     */
    public String getDsBanco() {
        return dsBanco;
    }

    /**
     * Set: dsBanco.
     * 
     * @param dsBanco
     *            the ds banco
     */
    public void setDsBanco(String dsBanco) {
        this.dsBanco = dsBanco;
    }

    /**
     * Get: mensagem.
     * 
     * @return mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Set: mensagem.
     * 
     * @param mensagem
     *            the mensagem
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * Get: nmParticipante.
     * 
     * @return nmParticipante
     */
    public String getNmParticipante() {
        return nmParticipante;
    }

    /**
     * Set: nmParticipante.
     * 
     * @param nmParticipante
     *            the nm participante
     */
    public void setNmParticipante(String nmParticipante) {
        this.nmParticipante = nmParticipante;
    }

    /**
     * Get: nrSequenciaContratoVinculo.
     * 
     * @return nrSequenciaContratoVinculo
     */
    public Long getNrSequenciaContratoVinculo() {
        return nrSequenciaContratoVinculo;
    }

    /**
     * Set: nrSequenciaContratoVinculo.
     * 
     * @param nrSequenciaContratoVinculo
     *            the nr sequencia contrato vinculo
     */
    public void setNrSequenciaContratoVinculo(Long nrSequenciaContratoVinculo) {
        this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
    }

    /**
     * Get: cdPessoaVinc.
     * 
     * @return cdPessoaVinc
     */
    public Long getCdPessoaVinc() {
        return cdPessoaVinc;
    }

    /**
     * Set: cdPessoaVinc.
     * 
     * @param cdPessoaVinc
     *            the cd pessoa vinc
     */
    public void setCdPessoaVinc(Long cdPessoaVinc) {
        this.cdPessoaVinc = cdPessoaVinc;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    @Override public ListarContasPossiveisInclusaoSaidaDTO clone() {
        try {
            final ListarContasPossiveisInclusaoSaidaDTO result = (ListarContasPossiveisInclusaoSaidaDTO) super.clone();
            // copy fields that need to be copied here!
            return result;
        } catch (final CloneNotSupportedException ex) {
            throw new AssertionError();
        }
    }

    @Override
    public boolean equals(Object obj ) {
        ListarContasPossiveisInclusaoSaidaDTO listaContas = (ListarContasPossiveisInclusaoSaidaDTO)obj;
        
        if (getCdPessoaJuridicaVinculo() == listaContas.getCdPessoaJuridicaVinculo() &&
            getCdTipoContratoVinculo() == listaContas.getCdTipoContratoVinculo() &&
            getNrSequenciaContratoVinculo() == listaContas.getNrSequenciaContratoVinculo()){
            return true;
        }
        
        return false;
    }
    
    
    
}
