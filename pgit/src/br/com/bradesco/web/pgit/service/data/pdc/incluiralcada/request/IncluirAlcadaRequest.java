/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluiralcada.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class IncluirAlcadaRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirAlcadaRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCist
     */
    private java.lang.String _cdCist;

    /**
     * Field _cdProgPagamentoIntegrado
     */
    private java.lang.String _cdProgPagamentoIntegrado;

    /**
     * Field _cdTipoAcaoPagamento
     */
    private int _cdTipoAcaoPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoAcaoPagamento
     */
    private boolean _has_cdTipoAcaoPagamento;

    /**
     * Field _cdTipoAcessoAlcada
     */
    private int _cdTipoAcessoAlcada = 0;

    /**
     * keeps track of state for field: _cdTipoAcessoAlcada
     */
    private boolean _has_cdTipoAcessoAlcada;

    /**
     * Field _cdTipoRegraAlcada
     */
    private int _cdTipoRegraAlcada = 0;

    /**
     * keeps track of state for field: _cdTipoRegraAlcada
     */
    private boolean _has_cdTipoRegraAlcada;

    /**
     * Field _cdTipoTratoAlcada
     */
    private int _cdTipoTratoAlcada = 0;

    /**
     * keeps track of state for field: _cdTipoTratoAlcada
     */
    private boolean _has_cdTipoTratoAlcada;

    /**
     * Field _cdSistemaRetor
     */
    private java.lang.String _cdSistemaRetor;

    /**
     * Field _cdProRetornoPagamento
     */
    private java.lang.String _cdProRetornoPagamento;

    /**
     * Field _cdProdutoOperacaoDeflt
     */
    private int _cdProdutoOperacaoDeflt = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoDeflt
     */
    private boolean _has_cdProdutoOperacaoDeflt;

    /**
     * Field _cdOperProdutoServico
     */
    private int _cdOperProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperProdutoServico
     */
    private boolean _has_cdOperProdutoServico;

    /**
     * Field _cdRegraAlcada
     */
    private int _cdRegraAlcada = 0;

    /**
     * keeps track of state for field: _cdRegraAlcada
     */
    private boolean _has_cdRegraAlcada;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirAlcadaRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluiralcada.request.IncluirAlcadaRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdOperProdutoServico
     * 
     */
    public void deleteCdOperProdutoServico()
    {
        this._has_cdOperProdutoServico= false;
    } //-- void deleteCdOperProdutoServico() 

    /**
     * Method deleteCdProdutoOperacaoDeflt
     * 
     */
    public void deleteCdProdutoOperacaoDeflt()
    {
        this._has_cdProdutoOperacaoDeflt= false;
    } //-- void deleteCdProdutoOperacaoDeflt() 

    /**
     * Method deleteCdRegraAlcada
     * 
     */
    public void deleteCdRegraAlcada()
    {
        this._has_cdRegraAlcada= false;
    } //-- void deleteCdRegraAlcada() 

    /**
     * Method deleteCdTipoAcaoPagamento
     * 
     */
    public void deleteCdTipoAcaoPagamento()
    {
        this._has_cdTipoAcaoPagamento= false;
    } //-- void deleteCdTipoAcaoPagamento() 

    /**
     * Method deleteCdTipoAcessoAlcada
     * 
     */
    public void deleteCdTipoAcessoAlcada()
    {
        this._has_cdTipoAcessoAlcada= false;
    } //-- void deleteCdTipoAcessoAlcada() 

    /**
     * Method deleteCdTipoRegraAlcada
     * 
     */
    public void deleteCdTipoRegraAlcada()
    {
        this._has_cdTipoRegraAlcada= false;
    } //-- void deleteCdTipoRegraAlcada() 

    /**
     * Method deleteCdTipoTratoAlcada
     * 
     */
    public void deleteCdTipoTratoAlcada()
    {
        this._has_cdTipoTratoAlcada= false;
    } //-- void deleteCdTipoTratoAlcada() 

    /**
     * Returns the value of field 'cdCist'.
     * 
     * @return String
     * @return the value of field 'cdCist'.
     */
    public java.lang.String getCdCist()
    {
        return this._cdCist;
    } //-- java.lang.String getCdCist() 

    /**
     * Returns the value of field 'cdOperProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperProdutoServico'.
     */
    public int getCdOperProdutoServico()
    {
        return this._cdOperProdutoServico;
    } //-- int getCdOperProdutoServico() 

    /**
     * Returns the value of field 'cdProRetornoPagamento'.
     * 
     * @return String
     * @return the value of field 'cdProRetornoPagamento'.
     */
    public java.lang.String getCdProRetornoPagamento()
    {
        return this._cdProRetornoPagamento;
    } //-- java.lang.String getCdProRetornoPagamento() 

    /**
     * Returns the value of field 'cdProdutoOperacaoDeflt'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoDeflt'.
     */
    public int getCdProdutoOperacaoDeflt()
    {
        return this._cdProdutoOperacaoDeflt;
    } //-- int getCdProdutoOperacaoDeflt() 

    /**
     * Returns the value of field 'cdProgPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'cdProgPagamentoIntegrado'.
     */
    public java.lang.String getCdProgPagamentoIntegrado()
    {
        return this._cdProgPagamentoIntegrado;
    } //-- java.lang.String getCdProgPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdRegraAlcada'.
     * 
     * @return int
     * @return the value of field 'cdRegraAlcada'.
     */
    public int getCdRegraAlcada()
    {
        return this._cdRegraAlcada;
    } //-- int getCdRegraAlcada() 

    /**
     * Returns the value of field 'cdSistemaRetor'.
     * 
     * @return String
     * @return the value of field 'cdSistemaRetor'.
     */
    public java.lang.String getCdSistemaRetor()
    {
        return this._cdSistemaRetor;
    } //-- java.lang.String getCdSistemaRetor() 

    /**
     * Returns the value of field 'cdTipoAcaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcaoPagamento'.
     */
    public int getCdTipoAcaoPagamento()
    {
        return this._cdTipoAcaoPagamento;
    } //-- int getCdTipoAcaoPagamento() 

    /**
     * Returns the value of field 'cdTipoAcessoAlcada'.
     * 
     * @return int
     * @return the value of field 'cdTipoAcessoAlcada'.
     */
    public int getCdTipoAcessoAlcada()
    {
        return this._cdTipoAcessoAlcada;
    } //-- int getCdTipoAcessoAlcada() 

    /**
     * Returns the value of field 'cdTipoRegraAlcada'.
     * 
     * @return int
     * @return the value of field 'cdTipoRegraAlcada'.
     */
    public int getCdTipoRegraAlcada()
    {
        return this._cdTipoRegraAlcada;
    } //-- int getCdTipoRegraAlcada() 

    /**
     * Returns the value of field 'cdTipoTratoAlcada'.
     * 
     * @return int
     * @return the value of field 'cdTipoTratoAlcada'.
     */
    public int getCdTipoTratoAlcada()
    {
        return this._cdTipoTratoAlcada;
    } //-- int getCdTipoTratoAlcada() 

    /**
     * Method hasCdOperProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperProdutoServico()
    {
        return this._has_cdOperProdutoServico;
    } //-- boolean hasCdOperProdutoServico() 

    /**
     * Method hasCdProdutoOperacaoDeflt
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoDeflt()
    {
        return this._has_cdProdutoOperacaoDeflt;
    } //-- boolean hasCdProdutoOperacaoDeflt() 

    /**
     * Method hasCdRegraAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRegraAlcada()
    {
        return this._has_cdRegraAlcada;
    } //-- boolean hasCdRegraAlcada() 

    /**
     * Method hasCdTipoAcaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcaoPagamento()
    {
        return this._has_cdTipoAcaoPagamento;
    } //-- boolean hasCdTipoAcaoPagamento() 

    /**
     * Method hasCdTipoAcessoAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAcessoAlcada()
    {
        return this._has_cdTipoAcessoAlcada;
    } //-- boolean hasCdTipoAcessoAlcada() 

    /**
     * Method hasCdTipoRegraAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRegraAlcada()
    {
        return this._has_cdTipoRegraAlcada;
    } //-- boolean hasCdTipoRegraAlcada() 

    /**
     * Method hasCdTipoTratoAlcada
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoTratoAlcada()
    {
        return this._has_cdTipoTratoAlcada;
    } //-- boolean hasCdTipoTratoAlcada() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCist'.
     * 
     * @param cdCist the value of field 'cdCist'.
     */
    public void setCdCist(java.lang.String cdCist)
    {
        this._cdCist = cdCist;
    } //-- void setCdCist(java.lang.String) 

    /**
     * Sets the value of field 'cdOperProdutoServico'.
     * 
     * @param cdOperProdutoServico the value of field
     * 'cdOperProdutoServico'.
     */
    public void setCdOperProdutoServico(int cdOperProdutoServico)
    {
        this._cdOperProdutoServico = cdOperProdutoServico;
        this._has_cdOperProdutoServico = true;
    } //-- void setCdOperProdutoServico(int) 

    /**
     * Sets the value of field 'cdProRetornoPagamento'.
     * 
     * @param cdProRetornoPagamento the value of field
     * 'cdProRetornoPagamento'.
     */
    public void setCdProRetornoPagamento(java.lang.String cdProRetornoPagamento)
    {
        this._cdProRetornoPagamento = cdProRetornoPagamento;
    } //-- void setCdProRetornoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'cdProdutoOperacaoDeflt'.
     * 
     * @param cdProdutoOperacaoDeflt the value of field
     * 'cdProdutoOperacaoDeflt'.
     */
    public void setCdProdutoOperacaoDeflt(int cdProdutoOperacaoDeflt)
    {
        this._cdProdutoOperacaoDeflt = cdProdutoOperacaoDeflt;
        this._has_cdProdutoOperacaoDeflt = true;
    } //-- void setCdProdutoOperacaoDeflt(int) 

    /**
     * Sets the value of field 'cdProgPagamentoIntegrado'.
     * 
     * @param cdProgPagamentoIntegrado the value of field
     * 'cdProgPagamentoIntegrado'.
     */
    public void setCdProgPagamentoIntegrado(java.lang.String cdProgPagamentoIntegrado)
    {
        this._cdProgPagamentoIntegrado = cdProgPagamentoIntegrado;
    } //-- void setCdProgPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'cdRegraAlcada'.
     * 
     * @param cdRegraAlcada the value of field 'cdRegraAlcada'.
     */
    public void setCdRegraAlcada(int cdRegraAlcada)
    {
        this._cdRegraAlcada = cdRegraAlcada;
        this._has_cdRegraAlcada = true;
    } //-- void setCdRegraAlcada(int) 

    /**
     * Sets the value of field 'cdSistemaRetor'.
     * 
     * @param cdSistemaRetor the value of field 'cdSistemaRetor'.
     */
    public void setCdSistemaRetor(java.lang.String cdSistemaRetor)
    {
        this._cdSistemaRetor = cdSistemaRetor;
    } //-- void setCdSistemaRetor(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoAcaoPagamento'.
     * 
     * @param cdTipoAcaoPagamento the value of field
     * 'cdTipoAcaoPagamento'.
     */
    public void setCdTipoAcaoPagamento(int cdTipoAcaoPagamento)
    {
        this._cdTipoAcaoPagamento = cdTipoAcaoPagamento;
        this._has_cdTipoAcaoPagamento = true;
    } //-- void setCdTipoAcaoPagamento(int) 

    /**
     * Sets the value of field 'cdTipoAcessoAlcada'.
     * 
     * @param cdTipoAcessoAlcada the value of field
     * 'cdTipoAcessoAlcada'.
     */
    public void setCdTipoAcessoAlcada(int cdTipoAcessoAlcada)
    {
        this._cdTipoAcessoAlcada = cdTipoAcessoAlcada;
        this._has_cdTipoAcessoAlcada = true;
    } //-- void setCdTipoAcessoAlcada(int) 

    /**
     * Sets the value of field 'cdTipoRegraAlcada'.
     * 
     * @param cdTipoRegraAlcada the value of field
     * 'cdTipoRegraAlcada'.
     */
    public void setCdTipoRegraAlcada(int cdTipoRegraAlcada)
    {
        this._cdTipoRegraAlcada = cdTipoRegraAlcada;
        this._has_cdTipoRegraAlcada = true;
    } //-- void setCdTipoRegraAlcada(int) 

    /**
     * Sets the value of field 'cdTipoTratoAlcada'.
     * 
     * @param cdTipoTratoAlcada the value of field
     * 'cdTipoTratoAlcada'.
     */
    public void setCdTipoTratoAlcada(int cdTipoTratoAlcada)
    {
        this._cdTipoTratoAlcada = cdTipoTratoAlcada;
        this._has_cdTipoTratoAlcada = true;
    } //-- void setCdTipoTratoAlcada(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirAlcadaRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluiralcada.request.IncluirAlcadaRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluiralcada.request.IncluirAlcadaRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluiralcada.request.IncluirAlcadaRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluiralcada.request.IncluirAlcadaRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
