/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosfavorecido.bean;

/**
 * Nome: ConsultarPagamentoFavorecidoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagamentoFavorecidoEntradaDTO {
    
    /** Atributo agePagoNaoPago. */
    private Integer agePagoNaoPago;
    
    /** Atributo dtCreditoPagamentoInicial. */
    private String dtCreditoPagamentoInicial;
    
    /** Atributo dtCreditoPagamentoFinal. */
    private String dtCreditoPagamentoFinal;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdProdutoServicoRelacionado. */
    private Integer cdProdutoServicoRelacionado;
    
    /** Atributo cdFavorecidoClientePagador. */
    private Long cdFavorecidoClientePagador;
    
    /** Atributo cdInscricaoFavorecidoCliente. */
    private Long cdInscricaoFavorecidoCliente;
    
    /** Atributo cdIdentificacaoInscricaoFavorecido. */
    private Integer cdIdentificacaoInscricaoFavorecido;
    
    /** Atributo cdBanco. */
    private Integer cdBanco;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo cdConta. */
    private Long cdConta;
    
    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;
    
    /** Atributo nrUnidadeOrganizacional. */
    private Integer nrUnidadeOrganizacional;
    
    /** Atributo cdBeneficiario. */
    private Long cdBeneficiario;
    
    /** Atributo cdEspecieBeneficioINSS. */
    private Integer cdEspecieBeneficioINSS;
    
    /** Atributo cdClassificacao. */
    private String cdClassificacao;
    
    /** Atributo cdEmpresaContrato. */
    private Long cdEmpresaContrato;
    
    /** Atributo cdTipoConta. */
    private Integer cdTipoConta;
    
    /** Atributo nrContrato. */
    private Long nrContrato;
    
    /** The cd banco salarial. */
    private Integer cdBancoSalarial;
    
    /** The cd agenca salarial. */
    private Integer cdAgencaSalarial;
    
    /** The cd conta salarial. */
    private Long cdContaSalarial;
    
	private String cdIspbPagtoDestino;
	
	private String contaPagtoDestino;
	/**
	 * Get: cdEmpresaContrato.
	 *
	 * @return cdEmpresaContrato
	 */
	public Long getCdEmpresaContrato() {
		return cdEmpresaContrato;
	}
	
	/**
	 * Set: cdEmpresaContrato.
	 *
	 * @param cdEmpresaContrato the cd empresa contrato
	 */
	public void setCdEmpresaContrato(Long cdEmpresaContrato) {
		this.cdEmpresaContrato = cdEmpresaContrato;
	}
	
	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}
	
	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}
	
	/**
	 * Get: nrContrato.
	 *
	 * @return nrContrato
	 */
	public Long getNrContrato() {
		return nrContrato;
	}
	
	/**
	 * Set: nrContrato.
	 *
	 * @param nrContrato the nr contrato
	 */
	public void setNrContrato(Long nrContrato) {
		this.nrContrato = nrContrato;
	}
	
	/**
	 * Get: agePagoNaoPago.
	 *
	 * @return agePagoNaoPago
	 */
	public Integer getAgePagoNaoPago() {
		return agePagoNaoPago;
	}
	
	/**
	 * Set: agePagoNaoPago.
	 *
	 * @param agePagoNaoPago the age pago nao pago
	 */
	public void setAgePagoNaoPago(Integer agePagoNaoPago) {
		this.agePagoNaoPago = agePagoNaoPago;
	}
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdBeneficiario.
	 *
	 * @return cdBeneficiario
	 */
	public Long getCdBeneficiario() {
		return cdBeneficiario;
	}
	
	/**
	 * Set: cdBeneficiario.
	 *
	 * @param cdBeneficiario the cd beneficiario
	 */
	public void setCdBeneficiario(Long cdBeneficiario) {
		this.cdBeneficiario = cdBeneficiario;
	}
	
	/**
	 * Get: cdClassificacao.
	 *
	 * @return cdClassificacao
	 */
	public String getCdClassificacao() {
		return cdClassificacao;
	}
	
	/**
	 * Set: cdClassificacao.
	 *
	 * @param cdClassificacao the cd classificacao
	 */
	public void setCdClassificacao(String cdClassificacao) {
		this.cdClassificacao = cdClassificacao;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdEspecieBeneficioINSS.
	 *
	 * @return cdEspecieBeneficioINSS
	 */
	public Integer getCdEspecieBeneficioINSS() {
		return cdEspecieBeneficioINSS;
	}
	
	/**
	 * Set: cdEspecieBeneficioINSS.
	 *
	 * @param cdEspecieBeneficioINSS the cd especie beneficio inss
	 */
	public void setCdEspecieBeneficioINSS(Integer cdEspecieBeneficioINSS) {
		this.cdEspecieBeneficioINSS = cdEspecieBeneficioINSS;
	}
	
	/**
	 * Get: cdFavorecidoClientePagador.
	 *
	 * @return cdFavorecidoClientePagador
	 */
	public Long getCdFavorecidoClientePagador() {
		return cdFavorecidoClientePagador;
	}
	
	/**
	 * Set: cdFavorecidoClientePagador.
	 *
	 * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
	 */
	public void setCdFavorecidoClientePagador(Long cdFavorecidoClientePagador) {
		this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
	}
	
	/**
	 * Get: cdIdentificacaoInscricaoFavorecido.
	 *
	 * @return cdIdentificacaoInscricaoFavorecido
	 */
	public Integer getCdIdentificacaoInscricaoFavorecido() {
		return cdIdentificacaoInscricaoFavorecido;
	}
	
	/**
	 * Set: cdIdentificacaoInscricaoFavorecido.
	 *
	 * @param cdIdentificacaoInscricaoFavorecido the cd identificacao inscricao favorecido
	 */
	public void setCdIdentificacaoInscricaoFavorecido(
			Integer cdIdentificacaoInscricaoFavorecido) {
		this.cdIdentificacaoInscricaoFavorecido = cdIdentificacaoInscricaoFavorecido;
	}
	
	/**
	 * Get: cdInscricaoFavorecidoCliente.
	 *
	 * @return cdInscricaoFavorecidoCliente
	 */
	public Long getCdInscricaoFavorecidoCliente() {
		return cdInscricaoFavorecidoCliente;
	}
	
	/**
	 * Set: cdInscricaoFavorecidoCliente.
	 *
	 * @param cdInscricaoFavorecidoCliente the cd inscricao favorecido cliente
	 */
	public void setCdInscricaoFavorecidoCliente(Long cdInscricaoFavorecidoCliente) {
		this.cdInscricaoFavorecidoCliente = cdInscricaoFavorecidoCliente;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: dtCreditoPagamentoFinal.
	 *
	 * @return dtCreditoPagamentoFinal
	 */
	public String getDtCreditoPagamentoFinal() {
		return dtCreditoPagamentoFinal;
	}
	
	/**
	 * Set: dtCreditoPagamentoFinal.
	 *
	 * @param dtCreditoPagamentoFinal the dt credito pagamento final
	 */
	public void setDtCreditoPagamentoFinal(String dtCreditoPagamentoFinal) {
		this.dtCreditoPagamentoFinal = dtCreditoPagamentoFinal;
	}
	
	/**
	 * Get: dtCreditoPagamentoInicial.
	 *
	 * @return dtCreditoPagamentoInicial
	 */
	public String getDtCreditoPagamentoInicial() {
		return dtCreditoPagamentoInicial;
	}
	
	/**
	 * Set: dtCreditoPagamentoInicial.
	 *
	 * @param dtCreditoPagamentoInicial the dt credito pagamento inicial
	 */
	public void setDtCreditoPagamentoInicial(String dtCreditoPagamentoInicial) {
		this.dtCreditoPagamentoInicial = dtCreditoPagamentoInicial;
	}
	
	/**
	 * Get: nrUnidadeOrganizacional.
	 *
	 * @return nrUnidadeOrganizacional
	 */
	public Integer getNrUnidadeOrganizacional() {
		return nrUnidadeOrganizacional;
	}
	
	/**
	 * Set: nrUnidadeOrganizacional.
	 *
	 * @param nrUnidadeOrganizacional the nr unidade organizacional
	 */
	public void setNrUnidadeOrganizacional(Integer nrUnidadeOrganizacional) {
		this.nrUnidadeOrganizacional = nrUnidadeOrganizacional;
	}

	/**
	 * @return the cdBancoSalarial
	 */
	public Integer getCdBancoSalarial() {
		return cdBancoSalarial;
	}

	/**
	 * @param cdBancoSalarial the cdBancoSalarial to set
	 */
	public void setCdBancoSalarial(Integer cdBancoSalarial) {
		this.cdBancoSalarial = cdBancoSalarial;
	}

	/**
	 * @return the cdAgencaSalarial
	 */
	public Integer getCdAgencaSalarial() {
		return cdAgencaSalarial;
	}

	/**
	 * @param cdAgencaSalarial the cdAgencaSalarial to set
	 */
	public void setCdAgencaSalarial(Integer cdAgencaSalarial) {
		this.cdAgencaSalarial = cdAgencaSalarial;
	}

	/**
	 * @return the cdContaSalarial
	 */
	public Long getCdContaSalarial() {
		return cdContaSalarial;
	}

	/**
	 * @param cdContaSalarial the cdContaSalarial to set
	 */
	public void setCdContaSalarial(Long cdContaSalarial) {
		this.cdContaSalarial = cdContaSalarial;
	}

	public String getCdIspbPagtoDestino() {
		return cdIspbPagtoDestino;
	}

	public void setCdIspbPagtoDestino(String cdIspbPagtoDestino) {
		this.cdIspbPagtoDestino = cdIspbPagtoDestino;
	}

	public String getContaPagtoDestino() {
		return contaPagtoDestino;
	}

	public void setContaPagtoDestino(String contaPagtoDestino) {
		this.contaPagtoDestino = contaPagtoDestino;
	}    

}
