/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.solrelcontratos.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.DetalharSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.DetalharSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncSoliRelatorioPagamentoValidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncSoliRelatorioPagamentoValidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncluirSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncluirSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosConstants;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.CancelarSolRelContratosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.CancelarSolRelContratosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.DetalharSolRelContratosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.DetalharSolRelContratosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ExcluirSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ExcluirSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.IncluirSolRelContratosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.IncluirSolRelContratosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ListarSolRelContratosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ListarSolRelContratosSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarrelatorioscontratos.request.CancelarRelatorioContratosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.cancelarrelatorioscontratos.response.CancelarRelatorioContratosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.request.DetalharRelatorioContratosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharrelatoriocontratos.response.DetalharRelatorioContratosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolirelatoriopagamento.request.DetalharSoliRelatorioPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolirelatoriopagamento.response.DetalharSoliRelatorioPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolirelatoriopagamento.request.ExcluirSoliRelatorioPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolirelatoriopagamento.response.ExcluirSoliRelatorioPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamento.request.IncluirSoliRelatorioPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamento.response.IncluirSoliRelatorioPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamentovalidacao.request.IncluirSoliRelatorioPagamentoValidacaoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolirelatoriopagamentovalidacao.response.IncluirSoliRelatorioPagamentoValidacaoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.request.ListarRelatorioContratosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.ListarRelatorioContratosResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.request.ListarSoliRelatorioPagamentoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolirelatoriopagamento.response.ListarSoliRelatorioPagamentoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.solicitarrelatoriocontratos.request.SolicitarRelatorioContratosRequest;
import br.com.bradesco.web.pgit.service.data.pdc.solicitarrelatoriocontratos.response.SolicitarRelatorioContratosResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterSolicitacaoRelatoriosContratos
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class SolRelContratosServiceImpl implements ISolRelContratosService {

	private String DS_ANALITICO_PDC = "ANALITICO";
	private String DS_ANALITICO_WEB = "ANAL�TICO";
	private String DS_ESTATISTICO_WEB = "ESTAT�STICO";

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 * 
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 * 
	 * @param factoryAdapter
	 *            the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService#listarSolicitacaoRelatorioContratos(br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ListarSolRelContratosEntradaDTO)
	 */
	public List<ListarSolRelContratosSaidaDTO> listarSolicitacaoRelatorioContratos(
			ListarSolRelContratosEntradaDTO listarSolRelContratosEntradaDTO) {

		List<ListarSolRelContratosSaidaDTO> listaRetorno = new ArrayList<ListarSolRelContratosSaidaDTO>();
		ListarRelatorioContratosRequest request = new ListarRelatorioContratosRequest();
		ListarRelatorioContratosResponse response = new ListarRelatorioContratosResponse();

		request.setCdSolicitacaoPagamentoIntegrado(0);
		request.setNrSolicitacaoPagamentoIntegrado(0);
		request.setCdTipoRelatPagamento(listarSolRelContratosEntradaDTO
				.getTipoRelatorio());
		request.setDtInicio(listarSolRelContratosEntradaDTO.getDataInicial());
		request.setDtFim(listarSolRelContratosEntradaDTO.getDataFinal());
		request
				.setQtOcorrencias(ISolRelContratosConstants.QTDE_OCORRENCIAS_LISTAR);

		response = getFactoryAdapter().getListarRelatorioContratosPDCAdapter()
				.invokeProcess(request);

		ListarSolRelContratosSaidaDTO saidaDTO;
		for (int i = 0; i < response.getOcorenciasCount(); i++) {
			saidaDTO = new ListarSolRelContratosSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setTipoSolicitacao(response.getOcorencias(i)
					.getCdSolicitacaoPagamentoIntegrado());
			saidaDTO.setSolicitacao(response.getOcorencias(i)
					.getNrSolicitacaoPagamentoIntegrado());
			saidaDTO.setTipoRelatorio(response.getOcorencias(i)
					.getCdTipoRelacao());
			saidaDTO.setDataHoraSolicitacao(PgitUtil.concatenarCampos(
					FormatarData.formatarData(response.getOcorencias(i)
							.getDtSolicitacao()), response.getOcorencias(i)
							.getHrSolicitacaoSolicitacao()));

			/*
			 * if (response.getOcorencias(i).getHrSolicitacaoSolicitacao() !=
			 * null &&
			 * response.getOcorencias(i).getHrSolicitacaoSolicitacao().length()
			 * >=19 ){
			 * 
			 * String stringData =
			 * response.getOcorencias(i).getHrSolicitacaoSolicitacao
			 * ().substring(0, 19);
			 * 
			 * try {
			 * saidaDTO.setDataHoraSolicitacao(formato2.format(formato1.parse
			 * (stringData))); } catch (ParseException e) {
			 * saidaDTO.setDataHoraSolicitacao(""); } }else{
			 * saidaDTO.setDataHoraSolicitacao(""); }
			 */

			saidaDTO.setSituacao(response.getOcorencias(i).getCdSituacao());

			listaRetorno.add(saidaDTO);
		}

		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService#listarRelatorioPagamento(br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarSoliRelatorioPagamentoEntradaDTO)
	 */
	public List<ListarSoliRelatorioPagamentoSaidaDTO> listarRelatorioPagamento(
			ListarSoliRelatorioPagamentoEntradaDTO entrada) {

		List<ListarSoliRelatorioPagamentoSaidaDTO> listaRetorno = new ArrayList<ListarSoliRelatorioPagamentoSaidaDTO>();
		ListarSoliRelatorioPagamentoRequest request = new ListarSoliRelatorioPagamentoRequest();
		ListarSoliRelatorioPagamentoResponse response = new ListarSoliRelatorioPagamentoResponse();

		request.setCdTipoSolicitacao(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoSolicitacao()));
		request.setDtInicioSolicitacao(PgitUtil.verificaStringNula(entrada
				.getDtInicioSolicitacao()));
		request.setDtFimSolicitacao(PgitUtil.verificaStringNula(entrada
				.getDtFimSolicitacao()));
		request.setNumeroOcorrencias(100);

		response = getFactoryAdapter()
				.getListarSoliRelatorioPagamentoPDCAdapter().invokeProcess(
						request);

		ListarSoliRelatorioPagamentoSaidaDTO saidaDTO;
		for (int i = 0; i < response.getOcorrenciasCount(); i++) {
			saidaDTO = new ListarSoliRelatorioPagamentoSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());

			saidaDTO.setNrSolicitacao(response.getOcorrencias(i)
					.getNrSolicitacao());
			saidaDTO.setCdUsuario(response.getOcorrencias(i).getCdUsuario());
			saidaDTO.setCdSolicitacaoPagamento(response.getOcorrencias(i)
					.getCdSolicitacaoPagamento());
			saidaDTO.setCdSituacao(response.getOcorrencias(i).getCdSituacao());

			String dsTipoRelatorio = response.getOcorrencias(i)
					.getDsTipoRelatorio();
			saidaDTO.setDsTipoRelatorio(DS_ESTATISTICO_WEB);
			if (dsTipoRelatorio.equalsIgnoreCase(DS_ANALITICO_PDC)) {
				saidaDTO.setDsTipoRelatorio(DS_ANALITICO_WEB);
			}

			saidaDTO.setDataHoraSolicitacao(PgitUtil.concatenarCampos(
					FormatarData.formatarData(response.getOcorrencias(i)
							.getDsDataSolicitacao()), response
							.getOcorrencias(i).getDsHoraSolicitacao()));

			listaRetorno.add(saidaDTO);
		}

		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService#detalharRelatorioPagamento(br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.DetalharSoliRelatorioPagamentoEntradaDTO)
	 */
	public DetalharSoliRelatorioPagamentoSaidaDTO detalharRelatorioPagamento(
			DetalharSoliRelatorioPagamentoEntradaDTO entrada) {

		DetalharSoliRelatorioPagamentoSaidaDTO saidaDTO = new DetalharSoliRelatorioPagamentoSaidaDTO();

		DetalharSoliRelatorioPagamentoRequest request = new DetalharSoliRelatorioPagamentoRequest();
		DetalharSoliRelatorioPagamentoResponse response = new DetalharSoliRelatorioPagamentoResponse();

		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada
				.getCdSolicitacaoPagamento()));
		request.setNrSolicitacao(PgitUtil.verificaIntegerNulo(entrada
				.getNrSolicitacao()));

		response = getFactoryAdapter()
				.getDetalharSoliRelatorioPagamentoPDCAdapter().invokeProcess(
						request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdpessoaJuridicaContrato(response
				.getCdpessoaJuridicaContrato());
		saidaDTO.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
		saidaDTO.setNrSequenciaContratoNegocio(response
				.getNrSequenciaContratoNegocio());
		saidaDTO.setCdTipoRelatorio(response.getCdTipoRelatorio());

		String dsTipoRelatorio = response.getDsTipoRelatorio();
		saidaDTO.setDsTipoRelatorio(DS_ESTATISTICO_WEB);
		if (dsTipoRelatorio.equalsIgnoreCase(DS_ANALITICO_PDC)) {
			saidaDTO.setDsTipoRelatorio(DS_ANALITICO_WEB);
		}
		saidaDTO.setCdProdutoServicoOperaca(response
				.getCdProdutoServicoOperacao());
		saidaDTO.setDsProdutoServicoOperacao(response
				.getDsProdutoServicoOperacao());
		saidaDTO.setCdProdutoOperacaoRelacionado(response
				.getCdProdutoOperacaoRelacionado());
		saidaDTO.setDsProdutoOperRelacionado(response
				.getDsProdutoOperRelacionado());
		saidaDTO.setCdRelacionamentoProduto(response
				.getCdRelacionamentoProduto());
		saidaDTO.setCdCpfCnpjParticipante(response.getCdCpfCnpjParticipante());
		saidaDTO.setCdCflialCnpjParticipante(response
				.getCdCflialCnpjParticipante());
		saidaDTO
				.setCdCctrlCpfParticipante(response.getCdCctrlCpfParticipante());
		saidaDTO.setDsNomeParticipante(response.getDsNomeParticipante());
		saidaDTO.setDtInicioPerMovimentacao(response
				.getDtInicioPerMovimentacao());
		saidaDTO.setDsFimPerMovimentacao(response.getDsFimPerMovimentacao());
		saidaDTO.setDtInclusao(response.getDtInclusao());
		saidaDTO.setHrInclusao(response.getHrInclusao());
		saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setCdUsuarioInclusaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setCdOperacaoCanalInclusao(response
				.getCdOperacaoCanalInclusao());
		saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setCdUsuarioManutencao(response.getCdUsuarioManutencao());
		saidaDTO.setCdUsuarioManutencaoExterno(response
				.getCdUsuarioInclusaoExterno());
		saidaDTO.setDtManutencao(response.getDtManutencao());
		saidaDTO.setHrManutencao(response.getHrManutencao());
		saidaDTO.setCdOperacaoCanalManutencao(response
				.getCdOperacaoCanalManutencao());
		saidaDTO.setCdTipoCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());

		saidaDTO.setDataHoraInclusao(PgitUtil.concatenarCampos(response
				.getDtInclusao(), response.getHrInclusao(), "-"));
		saidaDTO.setDataHoraManut(PgitUtil.concatenarCampos(response
				.getDtManutencao(), response.getHrManutencao(), "-"));

		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setDsNomeAgenciaDebito(response.getDsNomeAgenciaDebito());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setCdDigitoContaDebito(response.getCdDigitoContaDebito());

		saidaDTO.setCodAgenciaDesc(PgitUtil.concatenarCamposAgenciaConta(
				response.getCdAgenciaDebito(), response
						.getDsNomeAgenciaDebito(), "-"));
		saidaDTO.setContaDigito(PgitUtil.concatenarCamposAgenciaConta(response
				.getCdContaDebito(), response.getCdDigitoContaDebito(), "-"));
		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService#validarIncluirPagamento(br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncSoliRelatorioPagamentoValidacaoEntradaDTO)
	 */
	public IncSoliRelatorioPagamentoValidacaoSaidaDTO validarIncluirPagamento(
			IncSoliRelatorioPagamentoValidacaoEntradaDTO entrada) {

		IncSoliRelatorioPagamentoValidacaoSaidaDTO saidaDTO = new IncSoliRelatorioPagamentoValidacaoSaidaDTO();
		IncluirSoliRelatorioPagamentoValidacaoRequest request = new IncluirSoliRelatorioPagamentoValidacaoRequest();
		IncluirSoliRelatorioPagamentoValidacaoResponse response = new IncluirSoliRelatorioPagamentoValidacaoResponse();

		request.setTpRelatorio(PgitUtil.verificaIntegerNulo(entrada
				.getTpRelatorio()));
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setDtIniPagamento(PgitUtil.verificaStringNula(entrada
				.getDtIniPagamento()));
		request.setDtFimPagamento(PgitUtil.verificaStringNula(entrada
				.getDtFimPagamento()));
		request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entrada
				.getCdCpfCnpjParticipante()));
		request.setCdFilialCnpjParticipante(PgitUtil
				.verificaIntegerNulo(entrada.getCdFilialCnpjParticipante()));
		request.setCdControleCpfParticipante(PgitUtil
				.verificaIntegerNulo(entrada.getCdControleCpfParticipante()));
		request.setCdPessoa(PgitUtil.verificaLongNulo(entrada.getCdPessoa()));
		request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entrada
				.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil.verificaLongNulo(entrada
				.getCdContaDebito()));
		request.setCdProdutoServicoOperacao(PgitUtil
				.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request
				.setCdProdutoOperacaoRelacionado(PgitUtil
						.verificaIntegerNulo(entrada
								.getCdProdutoOperacaoRelacionado()));
		request.setCdRelacionamentoProduto(PgitUtil.verificaIntegerNulo(entrada
				.getCdRelacionamentoProduto()));

		response = getFactoryAdapter()
				.getIncluirSoliRelatorioPagamentoValidacaoPDCAdapter()
				.invokeProcess(request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO
				.setCdPessoaJuridicaDebito(response.getCdPessoaJuridicaDebito());
		saidaDTO.setCdTipoContratoDebito(response.getCdTipoContratoDebito());
		saidaDTO.setNrSequenciaContratoDebito(response
				.getNrSequenciaContratoDebito());

		saidaDTO.setCdAgenciaDebito(response.getCdAgenciaDebito());
		saidaDTO.setDsNomeAgenciaDebito(response.getDsNomeAgenciaDebito());
		saidaDTO.setCdContaDebito(response.getCdContaDebito());
		saidaDTO.setCdDigitoContaDebito(response.getCdDigitoContaDebito());

		saidaDTO.setCodAgenciaDesc(PgitUtil.concatenarCamposAgenciaConta(
				response.getCdAgenciaDebito(), response
						.getDsNomeAgenciaDebito(), "-"));
		saidaDTO.setContaDigito(PgitUtil.concatenarCamposAgenciaConta(response
				.getCdContaDebito(), response.getCdDigitoContaDebito(), "-"));

		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService#incluirRelatorioPagamento(br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncluirSoliRelatorioPagamentoEntradaDTO)
	 */
	public IncluirSoliRelatorioPagamentoSaidaDTO incluirRelatorioPagamento(
			IncluirSoliRelatorioPagamentoEntradaDTO entrada) {

		IncluirSoliRelatorioPagamentoSaidaDTO saidaDTO = new IncluirSoliRelatorioPagamentoSaidaDTO();
		IncluirSoliRelatorioPagamentoRequest request = new IncluirSoliRelatorioPagamentoRequest();
		IncluirSoliRelatorioPagamentoResponse response = new IncluirSoliRelatorioPagamentoResponse();

		request.setCdTipoRelatorio(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoRelatorio()));
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada
				.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada
				.getNrSequenciaContratoNegocio()));
		request.setCdProdutoServicoOperacao(PgitUtil
				.verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
		request
				.setCdProdutoOperacaoRelacionado(PgitUtil
						.verificaIntegerNulo(entrada
								.getCdProdutoOperacaoRelacionado()));
		request.setCdRelacionamentoProduto(PgitUtil.verificaIntegerNulo(entrada
				.getCdRelacionamentoProduto()));
		request.setCdPessoaParticipante(PgitUtil.verificaLongNulo(entrada
				.getCdPessoaParticipante()));
		request.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(entrada
				.getCdCpfCnpjParticipante()));
		request.setCdFilialCnpjParticipante(PgitUtil
				.verificaIntegerNulo(entrada.getCdFilialCnpjParticipante()));
		request.setCdControleCpfParticipante(PgitUtil
				.verificaIntegerNulo(entrada.getCdControleCpfParticipante()));
		request.setCdPessoaJuridicaDebito(PgitUtil.verificaLongNulo(entrada
				.getCdPessoaJuridicaDebito()));
		request.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(entrada
				.getCdTipoContratoDebito()));
		request.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(entrada
				.getNrSequenciaContratoDebito()));
		request.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(entrada
				.getCdAgenciaDebito()));
		request.setCdContaDebito(PgitUtil.verificaLongNulo(entrada
				.getCdContaDebito()));
		request.setDtIniPagamento(PgitUtil.verificaStringNula(entrada
				.getDtIniPagamento()));
		request.setDtFimPagamento(PgitUtil.verificaStringNula(entrada
				.getDtFimPagamento()));

		response = getFactoryAdapter()
				.getIncluirSoliRelatorioPagamentoPDCAdapter().invokeProcess(
						request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService#excluirRelatorioPagamento(br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ExcluirSoliRelatorioPagamentoEntradaDTO)
	 */
	public ExcluirSoliRelatorioPagamentoSaidaDTO excluirRelatorioPagamento(
			ExcluirSoliRelatorioPagamentoEntradaDTO entrada) {

		ExcluirSoliRelatorioPagamentoSaidaDTO saidaDTO = new ExcluirSoliRelatorioPagamentoSaidaDTO();
		ExcluirSoliRelatorioPagamentoRequest request = new ExcluirSoliRelatorioPagamentoRequest();
		ExcluirSoliRelatorioPagamentoResponse response = new ExcluirSoliRelatorioPagamentoResponse();

		request.setCdSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entrada
				.getCdSolicitacaoPagamento()));
		request.setNrSolicitacao(PgitUtil.verificaIntegerNulo(entrada
				.getNrSolicitacao()));

		response = getFactoryAdapter()
				.getExcluirSoliRelatorioPagamentoPDCAdapter().invokeProcess(
						request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService#detalharSolicitacaoRelatorioContratos(br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.DetalharSolRelContratosEntradaDTO)
	 */
	public DetalharSolRelContratosSaidaDTO detalharSolicitacaoRelatorioContratos(
			DetalharSolRelContratosEntradaDTO detalharSolRelContratosEntradaDTO) {

		DetalharSolRelContratosSaidaDTO saidaDTO = new DetalharSolRelContratosSaidaDTO();

		DetalharRelatorioContratosRequest request = new DetalharRelatorioContratosRequest();
		DetalharRelatorioContratosResponse response = new DetalharRelatorioContratosResponse();

		request
				.setCdSolicitacaoPagamentoIntegrado(detalharSolRelContratosEntradaDTO
						.getCdSolicitacaoPagamentoIntegrado());
		request
				.setNrSolicitacaoPagamentoIntegrado(detalharSolRelContratosEntradaDTO
						.getNrSolicitacaoPagamentoIntegrado());
		request.setCdTipoRelatPagamento(detalharSolRelContratosEntradaDTO
				.getTipoRelatorio());
		request
				.setDtInicio(detalharSolRelContratosEntradaDTO.getDataInicio() != null ? detalharSolRelContratosEntradaDTO
						.getDataInicio()
						: "");
		request
				.setDtFim(detalharSolRelContratosEntradaDTO.getDataFim() != null ? detalharSolRelContratosEntradaDTO
						.getDataFim()
						: "");
		request.setQtOcorrencias(0);

		response = getFactoryAdapter()
				.getDetalharRelatorioContratosPDCAdapter().invokeProcess(
						request);

		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());

		saidaDTO.setCdSolicitacaoPagamentoIntegrado(response
				.getCdSituacaoSolicitacaoPagamento());
		// saidaDTO.setNrSolicitacaoPagamentoIntegrado(response.getNrSolicitacaoPagamentoIntegrado());
		saidaDTO.setCdTipoServico(response.getCdProdutoServicoOper());
		saidaDTO.setDsTipoServico(response.getDsProdutoServicoOperacao());
		saidaDTO.setCdModalidadeServico(response.getCdProdutoOperRelacionado());
		saidaDTO.setDsModalidadeServico(response.getDsProdutoOperRelacionado());
		saidaDTO.setCdTipoRelacionamento(response.getCdRelacionamentoProduto());

		SimpleDateFormat formato1 = new SimpleDateFormat(
				"yyyy-MM-dd-HH.mm.ss.SSSSSS");
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		try {
			saidaDTO.setDataHoraSolicitacao(formato2.format(formato1
					.parse(response.getHrSolicitacao())));
		} catch (ParseException e) {
			saidaDTO.setDataHoraSolicitacao("");
		}
		saidaDTO.setCdSituacao(response.getCdSituacaoSolicitacaoPagamento());
		saidaDTO.setCdEmpresaConglomeradoDiretoriaRegional(response
				.getCdPessoaJuridDir());
		saidaDTO.setDsEmpresaConglomeradoDiretoriaRegional(response
				.getDsUnidadeOrganizacionalDir());
		saidaDTO.setCdDiretoriaRegional(response.getNrSeqUnidadeOrgnzDir()); // NSEQ-UND-ORGNZ-DIR
		// saidaDTO.setDsDiretoriaRegional(response.getDsSeqUnidadeOrgnzDir());
		saidaDTO.setCdEmpresaConglomeradoGerenciaRegional(response
				.getCdPessoaJuridGerc());
		saidaDTO.setDsEmpresaConglomeradoGerenciaRegional(response
				.getDsUnidadeOrganizacionalDir());
		saidaDTO.setCdGerenciaRegional(response.getNrUnidadeOrgnzGerc());
		saidaDTO.setDsGerenciaRegional(response.getDsOrganizacionalGerc());
		saidaDTO.setCdEmpresaConglomeradoAgenciaOperadora(response
				.getCdPessoaJuridNegocio());
		// saidaDTO.setDsEmpresaConglomeradoAgenciaOperadora(response.getDsPessoaJuridNegocio());
		saidaDTO.setCdAgenciaOperadora(response.getNrUnidadeOrgnzOper());
		saidaDTO.setDsAgenciaOperadora(response.getDsOrganizacaoOperacional());
		saidaDTO.setTipoRelatorio(response.getCdTipoRelatPagamento());
		saidaDTO.setDsTipoRelatorio(response.getDsTipoRelatorio());
		saidaDTO.setCdSegmento(response.getCdSegmentoCliente());
		saidaDTO.setDsSegmento(response.getDsSegmentoCliente());
		saidaDTO.setCdParticipanteGrupoEconomico(response
				.getCdGrpEconomicoCli());
		saidaDTO
				.setDsParticipanteGrupoEconomico(response.getDsGrupoEconomico());

		saidaDTO.setCdClassAtividadeEconomica(response
				.getCdClassAtividadeEconomica());
		saidaDTO.setCdRamoAtividadeEconomica(response
				.getCdRamoAtividadeEconomica());
		saidaDTO.setCdSubRamoAtividadeEconomica(response
				.getCdSubRamoAtividadeEconomica());

		saidaDTO.setCdAtividadeEconomica(response.getCdAtividadeEconomica()); // PGICW619-CATVDD-ECONC
		saidaDTO.setDsAtividadeEconomica(response.getDsAtividadeEconomica());

		saidaDTO.setCdCanalInclusao(response.getCdTipoCanalInclusao());
		saidaDTO.setDsCanalInclusao(response.getDsCanalInclusao());
		saidaDTO.setCdCanalManutencao(response.getCdTipoCanalManutencao());
		saidaDTO.setDsCanalManutencao(response.getDsCanalManutencao());

		saidaDTO.setUsuarioInclusao(response.getCdUsuarioInclusao());
		saidaDTO.setUsuarioManutencao(response.getCdUsuarioManutencao());

		saidaDTO.setComplementoInclusao(response.getCdOperacaoCanalInclusao());
		saidaDTO.setComplementoManutencao(response
				.getCdOperacaoCanalManutencao());

		saidaDTO.setDataHoraInclusao(PgitUtil.concatenarCampos(response
				.getDtInclusao(), response.getHrInclusao()));
		saidaDTO.setDataHoraManutencao(PgitUtil.concatenarCampos(response
				.getDtManutencao(), response.getHrManutencao()));

		saidaDTO.setDsRamo(response.getDsRamoAtividade());
		saidaDTO.setDsSubRamo(response.getDsRamoAtividadeEconomica());
		saidaDTO.setDsEmpresa(response.getDsEmpresa());

		return saidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService#solicitarRelatorioContratos(br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.IncluirSolRelContratosEntradaDTO)
	 */
	public IncluirSolRelContratosSaidaDTO solicitarRelatorioContratos(
			IncluirSolRelContratosEntradaDTO incluirSolRelContratosEntradaDTO) {

		IncluirSolRelContratosSaidaDTO incluirSolRelContratosSaidaDTO = new IncluirSolRelContratosSaidaDTO();
		SolicitarRelatorioContratosRequest solicitarRelatorioContratosRequest = new SolicitarRelatorioContratosRequest();
		SolicitarRelatorioContratosResponse solicitarRelatorioContratosResponse = new SolicitarRelatorioContratosResponse();

		solicitarRelatorioContratosRequest
				.setCdSolicitacaoPagamentoIntegrado(4);
		solicitarRelatorioContratosRequest
				.setCdProdutoServicoOper(incluirSolRelContratosEntradaDTO
						.getCdTipoServico());
		solicitarRelatorioContratosRequest
				.setCdProdutoOperRelacionado(incluirSolRelContratosEntradaDTO
						.getCdModalidadeServico());
		solicitarRelatorioContratosRequest
				.setCdRlctoProduto(incluirSolRelContratosEntradaDTO
						.getCdTipoRelacionamento());
		solicitarRelatorioContratosRequest
				.setCdPessoaJuridDir(incluirSolRelContratosEntradaDTO
						.getCdEmpresaConglomeradoDiretoriaRegional());
		solicitarRelatorioContratosRequest
				.setNrSequenciaUnidadeDir(incluirSolRelContratosEntradaDTO
						.getCdDiretoriaRegional());
		solicitarRelatorioContratosRequest
				.setCdPessoaJuridGerc(incluirSolRelContratosEntradaDTO
						.getCdEmpresaConglomeradoGerenciaRegional());
		solicitarRelatorioContratosRequest
				.setNrSequenciaUnidadeGer(incluirSolRelContratosEntradaDTO
						.getCdGerenciaRegional());
		solicitarRelatorioContratosRequest
				.setCdPessoaJuridNegocio(incluirSolRelContratosEntradaDTO
						.getCdEmpresaConglomeradoAgenciaOperadora());
		solicitarRelatorioContratosRequest
				.setNrSequenciaUnidadeOper(incluirSolRelContratosEntradaDTO
						.getCdAgenciaOperadora());
		solicitarRelatorioContratosRequest
				.setCdTipoRelatorioPagamento(incluirSolRelContratosEntradaDTO
						.getTipoRelatorio());
		solicitarRelatorioContratosRequest
				.setNrTipoClasfPessoa(incluirSolRelContratosEntradaDTO
						.getCdSegmento());
		solicitarRelatorioContratosRequest
				.setCdGrpEconomicoCliente(incluirSolRelContratosEntradaDTO
						.getCdParticipanteGrupoEconomico());
		solicitarRelatorioContratosRequest
				.setCdClassAtividadeEconomica(incluirSolRelContratosEntradaDTO
						.getCdClassAtividadeEconomica());
		solicitarRelatorioContratosRequest
				.setCdRamoAtividadeEconomica(incluirSolRelContratosEntradaDTO
						.getCdRamoAtividadeEconomica());
		solicitarRelatorioContratosRequest
				.setCdSubRamoAtividadeEconomica(incluirSolRelContratosEntradaDTO
						.getCdSubRamoAtividadeEconomica());
		solicitarRelatorioContratosRequest
				.setCdAtividadeEconomica(incluirSolRelContratosEntradaDTO
						.getCdAtividadeEconomica());

		solicitarRelatorioContratosResponse = getFactoryAdapter()
				.getSolicitarRelatorioContratosPDCAdapter().invokeProcess(
						solicitarRelatorioContratosRequest);

		incluirSolRelContratosSaidaDTO
				.setCodMensagem(solicitarRelatorioContratosResponse
						.getCodMensagem());
		incluirSolRelContratosSaidaDTO
				.setMensagem(solicitarRelatorioContratosResponse.getMensagem());

		return incluirSolRelContratosSaidaDTO;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService#cancelarRelatorioContratos(br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.CancelarSolRelContratosEntradaDTO)
	 */
	public CancelarSolRelContratosSaidaDTO cancelarRelatorioContratos(
			CancelarSolRelContratosEntradaDTO cancelarSolRelContratosEntradaDTO) {

		CancelarSolRelContratosSaidaDTO cancelarSolRelContratosSaidaDTO = new CancelarSolRelContratosSaidaDTO();
		CancelarRelatorioContratosRequest cancelarRelatorioContratosRequest = new CancelarRelatorioContratosRequest();
		CancelarRelatorioContratosResponse cancelarRelatorioContratosResponse = new CancelarRelatorioContratosResponse();

		cancelarRelatorioContratosRequest
				.setCdSolicitacaoPagamentoIntegrado(cancelarSolRelContratosEntradaDTO
						.getCdSolicitacaoPagamentoIntegrado());
		cancelarRelatorioContratosRequest
				.setNrSolicitacaoPagamentoIntegrado(cancelarSolRelContratosEntradaDTO
						.getNrSolicitacaoPagamentoIntegrado());
		cancelarRelatorioContratosRequest
				.setCdTipoRelatPagamento(cancelarSolRelContratosEntradaDTO
						.getCdTipoRelatorio());
		cancelarRelatorioContratosRequest
				.setDtFim(cancelarSolRelContratosEntradaDTO.getDataFinal());
		cancelarRelatorioContratosRequest
				.setDtInicio(cancelarSolRelContratosEntradaDTO.getDataInicial());
		cancelarRelatorioContratosRequest.setQtOcorrencias(0);

		cancelarRelatorioContratosResponse = getFactoryAdapter()
				.getCancelarRelatoriosContratosPDCAdapter().invokeProcess(
						cancelarRelatorioContratosRequest);

		cancelarSolRelContratosSaidaDTO
				.setCodMensagem(cancelarRelatorioContratosResponse
						.getCodMensagem());
		cancelarSolRelContratosSaidaDTO
				.setMensagem(cancelarRelatorioContratosResponse.getMensagem());

		return cancelarSolRelContratosSaidaDTO;

	}
}
