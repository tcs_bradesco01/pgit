/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean;

/**
 * Nome: ListarPerfilTrocaArquivoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarPerfilTrocaArquivoSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo numeroLinhas. */
    private Integer numeroLinhas;
    
    /** Atributo cdPessoa. */
    private Long cdPessoa;
    
    /** Atributo cdCpfCnpjPessoa. */
    private String cdCpfCnpjPessoa;
    
    /** Atributo dsPessoa. */
    private String dsPessoa;
    
    /** Atributo cdPerfilTrocaArquivo. */
    private Long cdPerfilTrocaArquivo;
    
    /** Atributo dsSituacaoPerfil. */
    private String dsSituacaoPerfil;
    
	/**
	 * Get: cdCpfCnpjPessoa.
	 *
	 * @return cdCpfCnpjPessoa
	 */
	public String getCdCpfCnpjPessoa() {
		return cdCpfCnpjPessoa;
	}
	
	/**
	 * Set: cdCpfCnpjPessoa.
	 *
	 * @param cdCpfCnpjPessoa the cd cpf cnpj pessoa
	 */
	public void setCdCpfCnpjPessoa(String cdCpfCnpjPessoa) {
		this.cdCpfCnpjPessoa = cdCpfCnpjPessoa;
	}
	
	/**
	 * Get: cdPerfilTrocaArquivo.
	 *
	 * @return cdPerfilTrocaArquivo
	 */
	public Long getCdPerfilTrocaArquivo() {
		return cdPerfilTrocaArquivo;
	}
	
	/**
	 * Set: cdPerfilTrocaArquivo.
	 *
	 * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
	 */
	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo) {
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}
	
	/**
	 * Get: cdPessoa.
	 *
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}
	
	/**
	 * Set: cdPessoa.
	 *
	 * @param cdPessoa the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsPessoa.
	 *
	 * @return dsPessoa
	 */
	public String getDsPessoa() {
		return dsPessoa;
	}
	
	/**
	 * Set: dsPessoa.
	 *
	 * @param dsPessoa the ds pessoa
	 */
	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}
	
	/**
	 * Get: dsSituacaoPerfil.
	 *
	 * @return dsSituacaoPerfil
	 */
	public String getDsSituacaoPerfil() {
		return dsSituacaoPerfil;
	}
	
	/**
	 * Set: dsSituacaoPerfil.
	 *
	 * @param dsSituacaoPerfil the ds situacao perfil
	 */
	public void setDsSituacaoPerfil(String dsSituacaoPerfil) {
		this.dsSituacaoPerfil = dsSituacaoPerfil;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}
	
	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
    
}
