/*
 * Nome: br.com.bradesco.web.pgit.service.business.arquivorecebido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.arquivorecebido.bean;

/**
 * Nome: OcorrenciaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OcorrenciaDTO {

	/** Atributo codigoSistemaLegado. */
	private String codigoSistemaLegado;
	
	/** Atributo controleCpfCnpj. */
	private String controleCpfCnpj;
	
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo filialCnpj. */
	private String filialCnpj;
	
	/** Atributo filler. */
	private String filler;
	
	/** Atributo numeroArquivoRemessa. */
	private String numeroArquivoRemessa;
	
	/** Atributo numeroLoteRemessa. */
	private String numeroLoteRemessa;
	
	/** Atributo numeroSequenciaRemessa. */
	private String numeroSequenciaRemessa;
	
	/** Atributo perfil. */
	private String perfil;
	
	/** Atributo recurso. */
	private String recurso;
	
	/** Atributo tipoPesquisa. */
	private String tipoPesquisa;
	
	/** Atributo sistemaLegado. */
	private String sistemaLegado;
	
	/** Atributo dataRecepcaoInclusao. */
	private String dataRecepcaoInclusao;
	
	/** Atributo ocorrencia. */
	private String ocorrencia;
	
	/** Atributo conteudoEnviado. */
	private String conteudoEnviado;
	
	/**
	 * Get: codigoSistemaLegado.
	 *
	 * @return codigoSistemaLegado
	 */
	public String getCodigoSistemaLegado() {
		return codigoSistemaLegado;
	}
	
	/**
	 * Set: codigoSistemaLegado.
	 *
	 * @param codigoSistemaLegado the codigo sistema legado
	 */
	public void setCodigoSistemaLegado(String codigoSistemaLegado) {
		this.codigoSistemaLegado = codigoSistemaLegado;
	}
	
	/**
	 * Get: controleCpfCnpj.
	 *
	 * @return controleCpfCnpj
	 */
	public String getControleCpfCnpj() {
		return controleCpfCnpj;
	}
	
	/**
	 * Set: controleCpfCnpj.
	 *
	 * @param controleCpfCnpj the controle cpf cnpj
	 */
	public void setControleCpfCnpj(String controleCpfCnpj) {
		this.controleCpfCnpj = controleCpfCnpj;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	
	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	/**
	 * Get: filialCnpj.
	 *
	 * @return filialCnpj
	 */
	public String getFilialCnpj() {
		return filialCnpj;
	}
	
	/**
	 * Set: filialCnpj.
	 *
	 * @param filialCnpj the filial cnpj
	 */
	public void setFilialCnpj(String filialCnpj) {
		this.filialCnpj = filialCnpj;
	}
	
	/**
	 * Get: filler.
	 *
	 * @return filler
	 */
	public String getFiller() {
		return filler;
	}
	
	/**
	 * Set: filler.
	 *
	 * @param filler the filler
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}
	
	/**
	 * Get: numeroArquivoRemessa.
	 *
	 * @return numeroArquivoRemessa
	 */
	public String getNumeroArquivoRemessa() {
		return numeroArquivoRemessa;
	}
	
	/**
	 * Set: numeroArquivoRemessa.
	 *
	 * @param numeroArquivoRemessa the numero arquivo remessa
	 */
	public void setNumeroArquivoRemessa(String numeroArquivoRemessa) {
		this.numeroArquivoRemessa = numeroArquivoRemessa;
	}
	
	/**
	 * Get: numeroLoteRemessa.
	 *
	 * @return numeroLoteRemessa
	 */
	public String getNumeroLoteRemessa() {
		return numeroLoteRemessa;
	}
	
	/**
	 * Set: numeroLoteRemessa.
	 *
	 * @param numeroLoteRemessa the numero lote remessa
	 */
	public void setNumeroLoteRemessa(String numeroLoteRemessa) {
		this.numeroLoteRemessa = numeroLoteRemessa;
	}
	
	/**
	 * Get: numeroSequenciaRemessa.
	 *
	 * @return numeroSequenciaRemessa
	 */
	public String getNumeroSequenciaRemessa() {
		return numeroSequenciaRemessa;
	}
	
	/**
	 * Set: numeroSequenciaRemessa.
	 *
	 * @param numeroSequenciaRemessa the numero sequencia remessa
	 */
	public void setNumeroSequenciaRemessa(String numeroSequenciaRemessa) {
		this.numeroSequenciaRemessa = numeroSequenciaRemessa;
	}
	
	/**
	 * Get: perfil.
	 *
	 * @return perfil
	 */
	public String getPerfil() {
		return perfil;
	}
	
	/**
	 * Set: perfil.
	 *
	 * @param perfil the perfil
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public String getRecurso() {
		return recurso;
	}
	
	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}
	
	/**
	 * Get: tipoPesquisa.
	 *
	 * @return tipoPesquisa
	 */
	public String getTipoPesquisa() {
		return tipoPesquisa;
	}
	
	/**
	 * Set: tipoPesquisa.
	 *
	 * @param tipoPesquisa the tipo pesquisa
	 */
	public void setTipoPesquisa(String tipoPesquisa) {
		this.tipoPesquisa = tipoPesquisa;
	}
	
	/**
	 * Get: conteudoEnviado.
	 *
	 * @return conteudoEnviado
	 */
	public String getConteudoEnviado() {
		return conteudoEnviado;
	}
	
	/**
	 * Set: conteudoEnviado.
	 *
	 * @param conteudoEnviado the conteudo enviado
	 */
	public void setConteudoEnviado(String conteudoEnviado) {
		this.conteudoEnviado = conteudoEnviado;
	}
	
	/**
	 * Get: ocorrencia.
	 *
	 * @return ocorrencia
	 */
	public String getOcorrencia() {
		return ocorrencia;
	}
	
	/**
	 * Set: ocorrencia.
	 *
	 * @param ocorrencia the ocorrencia
	 */
	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}
	
	/**
	 * Get: sistemaLegado.
	 *
	 * @return sistemaLegado
	 */
	public String getSistemaLegado() {
		return sistemaLegado;
	}
	
	/**
	 * Set: sistemaLegado.
	 *
	 * @param sistemaLegado the sistema legado
	 */
	public void setSistemaLegado(String sistemaLegado) {
		this.sistemaLegado = sistemaLegado;
	}
	
	/**
	 * Get: dataRecepcaoInclusao.
	 *
	 * @return dataRecepcaoInclusao
	 */
	public String getDataRecepcaoInclusao() {
		return dataRecepcaoInclusao;
	}
	
	/**
	 * Set: dataRecepcaoInclusao.
	 *
	 * @param dataRecepcaoInclusao the data recepcao inclusao
	 */
	public void setDataRecepcaoInclusao(String dataRecepcaoInclusao) {
		this.dataRecepcaoInclusao = dataRecepcaoInclusao;
	}
}
