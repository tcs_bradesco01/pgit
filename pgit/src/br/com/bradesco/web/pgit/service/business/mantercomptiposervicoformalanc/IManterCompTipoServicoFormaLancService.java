/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ConsultarServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ConsultarServicoLancamentoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.DetalharServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.DetalharServicoLancamentoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ExcluirServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ExcluirServicoLancamentoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.IncluirServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.IncluirServicoLancamentoCnabSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterCompTipoServicoFormaLanc
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterCompTipoServicoFormaLancService {
	
	/**
	 * Consultar servico lancamento cnab.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar servico lancamento cnab saida dt o>
	 */
	List<ConsultarServicoLancamentoCnabSaidaDTO> consultarServicoLancamentoCnab(ConsultarServicoLancamentoCnabEntradaDTO entrada);
	
	/**
	 * Incluir servico lancamento cnab.
	 *
	 * @param entrada the entrada
	 * @return the incluir servico lancamento cnab saida dto
	 */
	IncluirServicoLancamentoCnabSaidaDTO incluirServicoLancamentoCnab(IncluirServicoLancamentoCnabEntradaDTO entrada);
	
	/**
	 * Detalhar servico lancamento cnab.
	 *
	 * @param entrada the entrada
	 * @return the detalhar servico lancamento cnab saida dto
	 */
	DetalharServicoLancamentoCnabSaidaDTO detalharServicoLancamentoCnab(DetalharServicoLancamentoCnabEntradaDTO entrada);
	
	/**
	 * Excluir servico lancamento cnab.
	 *
	 * @param entrada the entrada
	 * @return the excluir servico lancamento cnab saida dto
	 */
	ExcluirServicoLancamentoCnabSaidaDTO excluirServicoLancamentoCnab(ExcluirServicoLancamentoCnabEntradaDTO entrada);
}

