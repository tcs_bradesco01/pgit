/*
 * Nome: ${package_name}
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: ${date}
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean;

import java.math.BigDecimal;

/**
 * The Class DetalharPiramideOcorrencias5SaidaDTO.
 */
public class DetalharPiramideOcorrencias5SaidaDTO {

    /** The cd banco fl quest. */
    private Integer cdBancoFlQuest;

    /** The ds banco fl quest. */
    private String dsBancoFlQuest;

    /** The pct banco fl quest. */
    private BigDecimal pctBancoFlQuest;

    /**
     * Set cd banco fl quest.
     * 
     * @param cdBancoFlQuest
     *            the cd banco fl quest
     */
    public void setCdBancoFlQuest(Integer cdBancoFlQuest) {
        this.cdBancoFlQuest = cdBancoFlQuest;
    }

    /**
     * Get cd banco fl quest.
     * 
     * @return the cd banco fl quest
     */
    public Integer getCdBancoFlQuest() {
        return this.cdBancoFlQuest;
    }

    /**
     * Set ds banco fl quest.
     * 
     * @param dsBancoFlQuest
     *            the ds banco fl quest
     */
    public void setDsBancoFlQuest(String dsBancoFlQuest) {
        this.dsBancoFlQuest = dsBancoFlQuest;
    }

    /**
     * Get ds banco fl quest.
     * 
     * @return the ds banco fl quest
     */
    public String getDsBancoFlQuest() {
        return this.dsBancoFlQuest;
    }

    /**
     * Set pct banco fl quest.
     * 
     * @param pctBancoFlQuest
     *            the pct banco fl quest
     */
    public void setPctBancoFlQuest(BigDecimal pctBancoFlQuest) {
        this.pctBancoFlQuest = pctBancoFlQuest;
    }

    /**
     * Get pct banco fl quest.
     * 
     * @return the pct banco fl quest
     */
    public BigDecimal getPctBancoFlQuest() {
        return this.pctBancoFlQuest;
    }
}