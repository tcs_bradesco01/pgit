/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorelatoriofavorecidos.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class IncluirSolicitacaoRelatorioFavorecidosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class IncluirSolicitacaoRelatorioFavorecidosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _tpRelatorio
     */
    private int _tpRelatorio = 0;

    /**
     * keeps track of state for field: _tpRelatorio
     */
    private boolean _has_tpRelatorio;

    /**
     * Field _cdEmpresaConglomerado
     */
    private long _cdEmpresaConglomerado = 0;

    /**
     * keeps track of state for field: _cdEmpresaConglomerado
     */
    private boolean _has_cdEmpresaConglomerado;

    /**
     * Field _cdDiretoriaRegional
     */
    private int _cdDiretoriaRegional = 0;

    /**
     * keeps track of state for field: _cdDiretoriaRegional
     */
    private boolean _has_cdDiretoriaRegional;

    /**
     * Field _cdGerenciaRegional
     */
    private int _cdGerenciaRegional = 0;

    /**
     * keeps track of state for field: _cdGerenciaRegional
     */
    private boolean _has_cdGerenciaRegional;

    /**
     * Field _cdAgenciaOperadora
     */
    private int _cdAgenciaOperadora = 0;

    /**
     * keeps track of state for field: _cdAgenciaOperadora
     */
    private boolean _has_cdAgenciaOperadora;

    /**
     * Field _cdSegmento
     */
    private int _cdSegmento = 0;

    /**
     * keeps track of state for field: _cdSegmento
     */
    private boolean _has_cdSegmento;

    /**
     * Field _cdGrupoEconomico
     */
    private long _cdGrupoEconomico = 0;

    /**
     * keeps track of state for field: _cdGrupoEconomico
     */
    private boolean _has_cdGrupoEconomico;

    /**
     * Field _cdClassAtividade
     */
    private java.lang.String _cdClassAtividade;

    /**
     * Field _cdRamoAtividade
     */
    private int _cdRamoAtividade = 0;

    /**
     * keeps track of state for field: _cdRamoAtividade
     */
    private boolean _has_cdRamoAtividade;

    /**
     * Field _cdSubRamoAtividade
     */
    private int _cdSubRamoAtividade = 0;

    /**
     * keeps track of state for field: _cdSubRamoAtividade
     */
    private boolean _has_cdSubRamoAtividade;

    /**
     * Field _cdAtividadeEconomica
     */
    private int _cdAtividadeEconomica = 0;

    /**
     * keeps track of state for field: _cdAtividadeEconomica
     */
    private boolean _has_cdAtividadeEconomica;


      //----------------/
     //- Constructors -/
    //----------------/

    public IncluirSolicitacaoRelatorioFavorecidosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorelatoriofavorecidos.request.IncluirSolicitacaoRelatorioFavorecidosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaOperadora
     * 
     */
    public void deleteCdAgenciaOperadora()
    {
        this._has_cdAgenciaOperadora= false;
    } //-- void deleteCdAgenciaOperadora() 

    /**
     * Method deleteCdAtividadeEconomica
     * 
     */
    public void deleteCdAtividadeEconomica()
    {
        this._has_cdAtividadeEconomica= false;
    } //-- void deleteCdAtividadeEconomica() 

    /**
     * Method deleteCdDiretoriaRegional
     * 
     */
    public void deleteCdDiretoriaRegional()
    {
        this._has_cdDiretoriaRegional= false;
    } //-- void deleteCdDiretoriaRegional() 

    /**
     * Method deleteCdEmpresaConglomerado
     * 
     */
    public void deleteCdEmpresaConglomerado()
    {
        this._has_cdEmpresaConglomerado= false;
    } //-- void deleteCdEmpresaConglomerado() 

    /**
     * Method deleteCdGerenciaRegional
     * 
     */
    public void deleteCdGerenciaRegional()
    {
        this._has_cdGerenciaRegional= false;
    } //-- void deleteCdGerenciaRegional() 

    /**
     * Method deleteCdGrupoEconomico
     * 
     */
    public void deleteCdGrupoEconomico()
    {
        this._has_cdGrupoEconomico= false;
    } //-- void deleteCdGrupoEconomico() 

    /**
     * Method deleteCdRamoAtividade
     * 
     */
    public void deleteCdRamoAtividade()
    {
        this._has_cdRamoAtividade= false;
    } //-- void deleteCdRamoAtividade() 

    /**
     * Method deleteCdSegmento
     * 
     */
    public void deleteCdSegmento()
    {
        this._has_cdSegmento= false;
    } //-- void deleteCdSegmento() 

    /**
     * Method deleteCdSubRamoAtividade
     * 
     */
    public void deleteCdSubRamoAtividade()
    {
        this._has_cdSubRamoAtividade= false;
    } //-- void deleteCdSubRamoAtividade() 

    /**
     * Method deleteTpRelatorio
     * 
     */
    public void deleteTpRelatorio()
    {
        this._has_tpRelatorio= false;
    } //-- void deleteTpRelatorio() 

    /**
     * Returns the value of field 'cdAgenciaOperadora'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaOperadora'.
     */
    public int getCdAgenciaOperadora()
    {
        return this._cdAgenciaOperadora;
    } //-- int getCdAgenciaOperadora() 

    /**
     * Returns the value of field 'cdAtividadeEconomica'.
     * 
     * @return int
     * @return the value of field 'cdAtividadeEconomica'.
     */
    public int getCdAtividadeEconomica()
    {
        return this._cdAtividadeEconomica;
    } //-- int getCdAtividadeEconomica() 

    /**
     * Returns the value of field 'cdClassAtividade'.
     * 
     * @return String
     * @return the value of field 'cdClassAtividade'.
     */
    public java.lang.String getCdClassAtividade()
    {
        return this._cdClassAtividade;
    } //-- java.lang.String getCdClassAtividade() 

    /**
     * Returns the value of field 'cdDiretoriaRegional'.
     * 
     * @return int
     * @return the value of field 'cdDiretoriaRegional'.
     */
    public int getCdDiretoriaRegional()
    {
        return this._cdDiretoriaRegional;
    } //-- int getCdDiretoriaRegional() 

    /**
     * Returns the value of field 'cdEmpresaConglomerado'.
     * 
     * @return long
     * @return the value of field 'cdEmpresaConglomerado'.
     */
    public long getCdEmpresaConglomerado()
    {
        return this._cdEmpresaConglomerado;
    } //-- long getCdEmpresaConglomerado() 

    /**
     * Returns the value of field 'cdGerenciaRegional'.
     * 
     * @return int
     * @return the value of field 'cdGerenciaRegional'.
     */
    public int getCdGerenciaRegional()
    {
        return this._cdGerenciaRegional;
    } //-- int getCdGerenciaRegional() 

    /**
     * Returns the value of field 'cdGrupoEconomico'.
     * 
     * @return long
     * @return the value of field 'cdGrupoEconomico'.
     */
    public long getCdGrupoEconomico()
    {
        return this._cdGrupoEconomico;
    } //-- long getCdGrupoEconomico() 

    /**
     * Returns the value of field 'cdRamoAtividade'.
     * 
     * @return int
     * @return the value of field 'cdRamoAtividade'.
     */
    public int getCdRamoAtividade()
    {
        return this._cdRamoAtividade;
    } //-- int getCdRamoAtividade() 

    /**
     * Returns the value of field 'cdSegmento'.
     * 
     * @return int
     * @return the value of field 'cdSegmento'.
     */
    public int getCdSegmento()
    {
        return this._cdSegmento;
    } //-- int getCdSegmento() 

    /**
     * Returns the value of field 'cdSubRamoAtividade'.
     * 
     * @return int
     * @return the value of field 'cdSubRamoAtividade'.
     */
    public int getCdSubRamoAtividade()
    {
        return this._cdSubRamoAtividade;
    } //-- int getCdSubRamoAtividade() 

    /**
     * Returns the value of field 'tpRelatorio'.
     * 
     * @return int
     * @return the value of field 'tpRelatorio'.
     */
    public int getTpRelatorio()
    {
        return this._tpRelatorio;
    } //-- int getTpRelatorio() 

    /**
     * Method hasCdAgenciaOperadora
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaOperadora()
    {
        return this._has_cdAgenciaOperadora;
    } //-- boolean hasCdAgenciaOperadora() 

    /**
     * Method hasCdAtividadeEconomica
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAtividadeEconomica()
    {
        return this._has_cdAtividadeEconomica;
    } //-- boolean hasCdAtividadeEconomica() 

    /**
     * Method hasCdDiretoriaRegional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDiretoriaRegional()
    {
        return this._has_cdDiretoriaRegional;
    } //-- boolean hasCdDiretoriaRegional() 

    /**
     * Method hasCdEmpresaConglomerado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresaConglomerado()
    {
        return this._has_cdEmpresaConglomerado;
    } //-- boolean hasCdEmpresaConglomerado() 

    /**
     * Method hasCdGerenciaRegional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGerenciaRegional()
    {
        return this._has_cdGerenciaRegional;
    } //-- boolean hasCdGerenciaRegional() 

    /**
     * Method hasCdGrupoEconomico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdGrupoEconomico()
    {
        return this._has_cdGrupoEconomico;
    } //-- boolean hasCdGrupoEconomico() 

    /**
     * Method hasCdRamoAtividade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRamoAtividade()
    {
        return this._has_cdRamoAtividade;
    } //-- boolean hasCdRamoAtividade() 

    /**
     * Method hasCdSegmento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSegmento()
    {
        return this._has_cdSegmento;
    } //-- boolean hasCdSegmento() 

    /**
     * Method hasCdSubRamoAtividade
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSubRamoAtividade()
    {
        return this._has_cdSubRamoAtividade;
    } //-- boolean hasCdSubRamoAtividade() 

    /**
     * Method hasTpRelatorio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasTpRelatorio()
    {
        return this._has_tpRelatorio;
    } //-- boolean hasTpRelatorio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaOperadora'.
     * 
     * @param cdAgenciaOperadora the value of field
     * 'cdAgenciaOperadora'.
     */
    public void setCdAgenciaOperadora(int cdAgenciaOperadora)
    {
        this._cdAgenciaOperadora = cdAgenciaOperadora;
        this._has_cdAgenciaOperadora = true;
    } //-- void setCdAgenciaOperadora(int) 

    /**
     * Sets the value of field 'cdAtividadeEconomica'.
     * 
     * @param cdAtividadeEconomica the value of field
     * 'cdAtividadeEconomica'.
     */
    public void setCdAtividadeEconomica(int cdAtividadeEconomica)
    {
        this._cdAtividadeEconomica = cdAtividadeEconomica;
        this._has_cdAtividadeEconomica = true;
    } //-- void setCdAtividadeEconomica(int) 

    /**
     * Sets the value of field 'cdClassAtividade'.
     * 
     * @param cdClassAtividade the value of field 'cdClassAtividade'
     */
    public void setCdClassAtividade(java.lang.String cdClassAtividade)
    {
        this._cdClassAtividade = cdClassAtividade;
    } //-- void setCdClassAtividade(java.lang.String) 

    /**
     * Sets the value of field 'cdDiretoriaRegional'.
     * 
     * @param cdDiretoriaRegional the value of field
     * 'cdDiretoriaRegional'.
     */
    public void setCdDiretoriaRegional(int cdDiretoriaRegional)
    {
        this._cdDiretoriaRegional = cdDiretoriaRegional;
        this._has_cdDiretoriaRegional = true;
    } //-- void setCdDiretoriaRegional(int) 

    /**
     * Sets the value of field 'cdEmpresaConglomerado'.
     * 
     * @param cdEmpresaConglomerado the value of field
     * 'cdEmpresaConglomerado'.
     */
    public void setCdEmpresaConglomerado(long cdEmpresaConglomerado)
    {
        this._cdEmpresaConglomerado = cdEmpresaConglomerado;
        this._has_cdEmpresaConglomerado = true;
    } //-- void setCdEmpresaConglomerado(long) 

    /**
     * Sets the value of field 'cdGerenciaRegional'.
     * 
     * @param cdGerenciaRegional the value of field
     * 'cdGerenciaRegional'.
     */
    public void setCdGerenciaRegional(int cdGerenciaRegional)
    {
        this._cdGerenciaRegional = cdGerenciaRegional;
        this._has_cdGerenciaRegional = true;
    } //-- void setCdGerenciaRegional(int) 

    /**
     * Sets the value of field 'cdGrupoEconomico'.
     * 
     * @param cdGrupoEconomico the value of field 'cdGrupoEconomico'
     */
    public void setCdGrupoEconomico(long cdGrupoEconomico)
    {
        this._cdGrupoEconomico = cdGrupoEconomico;
        this._has_cdGrupoEconomico = true;
    } //-- void setCdGrupoEconomico(long) 

    /**
     * Sets the value of field 'cdRamoAtividade'.
     * 
     * @param cdRamoAtividade the value of field 'cdRamoAtividade'.
     */
    public void setCdRamoAtividade(int cdRamoAtividade)
    {
        this._cdRamoAtividade = cdRamoAtividade;
        this._has_cdRamoAtividade = true;
    } //-- void setCdRamoAtividade(int) 

    /**
     * Sets the value of field 'cdSegmento'.
     * 
     * @param cdSegmento the value of field 'cdSegmento'.
     */
    public void setCdSegmento(int cdSegmento)
    {
        this._cdSegmento = cdSegmento;
        this._has_cdSegmento = true;
    } //-- void setCdSegmento(int) 

    /**
     * Sets the value of field 'cdSubRamoAtividade'.
     * 
     * @param cdSubRamoAtividade the value of field
     * 'cdSubRamoAtividade'.
     */
    public void setCdSubRamoAtividade(int cdSubRamoAtividade)
    {
        this._cdSubRamoAtividade = cdSubRamoAtividade;
        this._has_cdSubRamoAtividade = true;
    } //-- void setCdSubRamoAtividade(int) 

    /**
     * Sets the value of field 'tpRelatorio'.
     * 
     * @param tpRelatorio the value of field 'tpRelatorio'.
     */
    public void setTpRelatorio(int tpRelatorio)
    {
        this._tpRelatorio = tpRelatorio;
        this._has_tpRelatorio = true;
    } //-- void setTpRelatorio(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return IncluirSolicitacaoRelatorioFavorecidosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorelatoriofavorecidos.request.IncluirSolicitacaoRelatorioFavorecidosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorelatoriofavorecidos.request.IncluirSolicitacaoRelatorioFavorecidosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorelatoriofavorecidos.request.IncluirSolicitacaoRelatorioFavorecidosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitacaorelatoriofavorecidos.request.IncluirSolicitacaoRelatorioFavorecidosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
