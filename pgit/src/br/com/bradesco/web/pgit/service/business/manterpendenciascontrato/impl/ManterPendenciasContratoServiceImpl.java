/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.SimpleTimeZone;

import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.IManterPendenciasContratoService;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarListaPendenciasContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarListaPendenciasContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarPendenciaContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarPendenciaContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ReenviarEmailEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ReenviarEmailSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.RegistrarPendenciaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.RegistrarPendenciaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.request.ConsultarListaPendenciaContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistapendenciacontrato.response.ConsultarListaPendenciaContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpendenciacontratopgit.request.ConsultarPendenciaContratoPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpendenciacontratopgit.response.ConsultarPendenciaContratoPgitResponse;
import br.com.bradesco.web.pgit.service.data.pdc.reenviaremail.request.ReenviarEmailRequest;
import br.com.bradesco.web.pgit.service.data.pdc.reenviaremail.response.ReenviarEmailResponse;
import br.com.bradesco.web.pgit.service.data.pdc.registrarpendenciacontratopgit.request.RegistrarPendenciaContratoPgitRequest;
import br.com.bradesco.web.pgit.service.data.pdc.registrarpendenciacontratopgit.response.RegistrarPendenciaContratoPgitResponse;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ManterPendenciasContrato
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ManterPendenciasContratoServiceImpl implements IManterPendenciasContratoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.IManterPendenciasContratoService#consultarListaPendenciaContrato(br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarListaPendenciasContratoEntradaDTO)
	 */
	public List<ConsultarListaPendenciasContratoSaidaDTO> consultarListaPendenciaContrato(ConsultarListaPendenciasContratoEntradaDTO consultarListaPendenciasContratoEntradaDTO) {
	
		List<ConsultarListaPendenciasContratoSaidaDTO>  listaRetorno =  new ArrayList<ConsultarListaPendenciasContratoSaidaDTO>();
		ConsultarListaPendenciaContratoRequest request = new ConsultarListaPendenciaContratoRequest();
		ConsultarListaPendenciaContratoResponse response = new ConsultarListaPendenciaContratoResponse();
		
		
		request.setCdPendenciaPagamentoIntegrado(consultarListaPendenciasContratoEntradaDTO.getCodPendenciaPagamentoIntegrado());
		request.setCdPessoaJuridica(consultarListaPendenciasContratoEntradaDTO.getCodPessoaJuridica());
		request.setCdPessoaJuridicaContrato(consultarListaPendenciasContratoEntradaDTO.getCodPessoaJuridicaContrato());
		request.setCdSituacaoPendenciaContrato(consultarListaPendenciasContratoEntradaDTO.getCodSituacaoPendenciaContrato());
		request.setCdTipoBaixa(consultarListaPendenciasContratoEntradaDTO.getCodTipoBaixa());
		request.setCdTipoContratoNegocio(consultarListaPendenciasContratoEntradaDTO.getCodTipoContratoNegocio());
		request.setDtFimBaixa(consultarListaPendenciasContratoEntradaDTO.getDataFimBaixa());
		request.setDtInicioBaixa(consultarListaPendenciasContratoEntradaDTO.getDataInicioBaixa());		
		request.setNrSequenciaContratoNegocio(consultarListaPendenciasContratoEntradaDTO.getNumSequenciaContratoNegocio());
		//request.setNrSequencialUnidadeOrganizacional(consultarListaPendenciasContratoEntradaDTO.getNumSequenciaUnidadeOrg()); -- foi retirado quando houve a regera��o do pdc
		request.setCdTipoUnidade(consultarListaPendenciasContratoEntradaDTO.getCdTipoUnidade());
		request.setCdUnidadeOrganizacional(consultarListaPendenciasContratoEntradaDTO.getNumSequenciaUnidadeOrg());		
		request.setCdAcesso(consultarListaPendenciasContratoEntradaDTO.getCodAcesso());
		request.setNumeroOcorrencias(30);
		
		
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formato1 = new SimpleDateFormat("dd.MM.yyyy");
		
		response  = getFactoryAdapter().getConsultarListaPendenciaContratoPDCAdapter().invokeProcess(request);
		ConsultarListaPendenciasContratoSaidaDTO saidaDTO;
		
		for(int i = 0; i < response.getOcorrenciasCount(); i++){
			
			saidaDTO =  new ConsultarListaPendenciasContratoSaidaDTO();
			saidaDTO.setCodMensagem(response.getCodMensagem());
			saidaDTO.setMensagem(response.getMensagem());
			saidaDTO.setCodPendenciaPagInt(response.getOcorrencias(i).getCdPendencia());
			saidaDTO.setCodPessoaJuridica(response.getOcorrencias(i).getCdPessoaJuridica());
			saidaDTO.setCodSituacaoPendContrato(response.getOcorrencias(i).getCdSituacaoPendenciaContrato());
			saidaDTO.setCodTipoBaixa(response.getOcorrencias(i).getCdTipoBaixa());
			saidaDTO.setCosTipoUnidOrg(response.getOcorrencias(i).getCdUnidadeOrganizacional());
			
			saidaDTO.setDsPendenciaPagInt(response.getOcorrencias(i).getDsPendenciaPagamentoIntegrado());
			saidaDTO.setDsSituacaoPendContrato(response.getOcorrencias(i).getDsSituacaoContrato());
			saidaDTO.setDsTipoBaixa(response.getOcorrencias(i).getDsTipoBaixa());
			saidaDTO.setDsTipoUnidOrg(response.getOcorrencias(i).getDsTipoUnidadeOrganizacional());
			saidaDTO.setDsUnidadeOrg(response.getOcorrencias(i).getCdUnidadeOrganizacional() != 0 ?response.getOcorrencias(i).getCdUnidadeOrganizacional()+ " - " + response.getOcorrencias(i).getDsUnidadeOrganizacional():"");
			saidaDTO.setNumPendenciaContratoNeg(response.getOcorrencias(i).getNrPendenciaContratoNegocio());
			saidaDTO.setNumSeqUnidadeOrg(response.getOcorrencias(i).getNrSequenciaUnidadeOrganizacional());
			saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsPessoaJuridica());
			saidaDTO.setDsTipo(response.getOcorrencias(i).getCdPendencia() != 0 ? response.getOcorrencias(i).getCdPendencia() + " - " + response.getOcorrencias(i).getDsPendenciaPagamentoIntegrado() : "");
			saidaDTO.setHrBaixaPendencia(NumberUtils.formatarHorario(response.getOcorrencias(i).getHrBaixaPendencia()));
			saidaDTO.setHrGeracaoPendencia(NumberUtils.formatarHorario(response.getOcorrencias(i).getHrGeracaoPendencia()));

			try{
				saidaDTO.setDataGeracaoPendencia(String.valueOf(formato2.format(formato1.parse(response.getOcorrencias(i).getDtGeracaoPendencia()))));
			}catch(ParseException e){
				saidaDTO.setDataGeracaoPendencia("");
			}
			
			try{
				saidaDTO.setDataBaixaPendContrato(String.valueOf(formato2.format(formato1.parse(response.getOcorrencias(i).getDtBaixaPendenciaContrato()))));
			}catch(ParseException e){
				saidaDTO.setDataBaixaPendContrato("");
			}
			

			listaRetorno.add(saidaDTO);
		}
		
		
		
		return listaRetorno;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.IManterPendenciasContratoService#registrarPendenciaContrato(br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.RegistrarPendenciaContratoEntradaDTO)
	 */
	public RegistrarPendenciaContratoSaidaDTO registrarPendenciaContrato(RegistrarPendenciaContratoEntradaDTO registrarPendenciaContratoEntradaDTO) {
			RegistrarPendenciaContratoSaidaDTO registrarPendenciaContratoSaidaDTO = new RegistrarPendenciaContratoSaidaDTO();
			RegistrarPendenciaContratoPgitRequest request = new RegistrarPendenciaContratoPgitRequest();
			RegistrarPendenciaContratoPgitResponse response = new RegistrarPendenciaContratoPgitResponse();

			request.setCdMotivoBaixaPendencia(registrarPendenciaContratoEntradaDTO.getCodMotivoBaixaPendencia());
			request.setCdPendenciaPagamentoIntegrado(registrarPendenciaContratoEntradaDTO.getCdPendenciaPagamentoIntegrado());
			request.setCdPessoaJuridicaContrato(registrarPendenciaContratoEntradaDTO.getCdPessoaJuridicaContrato());
			request.setCdSituacaoPendenciaContrato(registrarPendenciaContratoEntradaDTO.getCodSituacaoPendenciaContrato());
			request.setCdTipoBaixaPendencia(registrarPendenciaContratoEntradaDTO.getCodTipoBaixaPendencia());
			request.setCdTipoContratoNegocio(registrarPendenciaContratoEntradaDTO.getCdTipoContratoNegocio());

			request.setDtAtendimentoPendenciaContrato(FormatarData.formataDiaMesAnoToPdc(registrarPendenciaContratoEntradaDTO.getDtAtendimentoPendenciaContrato()));
			request.setNrPendenciaPagamentoIntegrado(registrarPendenciaContratoEntradaDTO.getNrPendenciaPagamentoIntegrado());
			request.setNrSequenciaContratoNegocio(registrarPendenciaContratoEntradaDTO.getNrSequenciaContratoNegocio());

			response = getFactoryAdapter().getRegistrarPendenciaContratoPgitPDCAdapter().invokeProcess(request);

			registrarPendenciaContratoSaidaDTO.setCodMensagem(response.getCodMensagem());
			registrarPendenciaContratoSaidaDTO.setMensagem(response.getMensagem());

			return registrarPendenciaContratoSaidaDTO;
		}
	
	// detalhar
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.IManterPendenciasContratoService#consultarPendenciaContratoPgit(br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarPendenciaContratoPgitEntradaDTO)
	 */
	public ConsultarPendenciaContratoPgitSaidaDTO consultarPendenciaContratoPgit(ConsultarPendenciaContratoPgitEntradaDTO consultarPendenciaContratoPgitEntradaDTO) {
	      
		ConsultarPendenciaContratoPgitSaidaDTO saidaDTO = new ConsultarPendenciaContratoPgitSaidaDTO();
		
		ConsultarPendenciaContratoPgitRequest request = new ConsultarPendenciaContratoPgitRequest();
		ConsultarPendenciaContratoPgitResponse response = new ConsultarPendenciaContratoPgitResponse();
		
		request.setCdPendenciaPagamentoIntegrado(PgitUtil.verificaLongNulo(consultarPendenciaContratoPgitEntradaDTO.getCodPendenciaPagamentoIntegrado()));
		request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(consultarPendenciaContratoPgitEntradaDTO.getCodPessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(consultarPendenciaContratoPgitEntradaDTO.getCodTipoContratoNegocio()));
		request.setNrPendenciaContratoNegocio(PgitUtil.verificaLongNulo(consultarPendenciaContratoPgitEntradaDTO.getNrPendenciaContratoNegocio().longValue()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(consultarPendenciaContratoPgitEntradaDTO.getNrSeqContratoNegocio()));
		
		response = getFactoryAdapter().getConsultarPendenciaContratoPgitPDCAdapter().invokeProcess(request);
		
		saidaDTO = new ConsultarPendenciaContratoPgitSaidaDTO();
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		
		
		saidaDTO.setCodDigitoUnidadeOrganizacinal(response.getOcorrencias(0).getCdDigitoUnidadeOrganizacional());
		saidaDTO.setCodOperacaoCanalManutencao(response.getOcorrencias(0).getCdOperacaoCanalManutencao());
		saidaDTO.setCodOperCanalInclusao(response.getOcorrencias(0).getCdOperacaoCanalInclusao());
		saidaDTO.setCodPessoaUnidadeOrg(response.getOcorrencias(0).getCdPessoaUnidadeOrganizacional());
		saidaDTO.setCodTipoCanalInclusao(response.getOcorrencias(0).getCdTipoCanalInclusao());
		saidaDTO.setCodTipoCanalManutencao(response.getOcorrencias(0).getCdTipoCanalManutencao());
		saidaDTO.setCodUsuarioInclusao(response.getOcorrencias(0).getCdUsuarioInclusao());
		saidaDTO.setCodUsuarioInclusaoExterno(response.getOcorrencias(0).getCdUsuarioInclusaoExter());
		saidaDTO.setCodUsuarioManutencao(response.getOcorrencias(0).getCdUsuarioManutencao());
		saidaDTO.setCodUsuarioManutencaoExterno(response.getOcorrencias(0).getCdUsuarioManutencaoExter());
		saidaDTO.setDataAtendimentoPendenciaContrato(response.getOcorrencias(0).getDtAtendimentoPendenciaContrato());
		saidaDTO.setDsCanalInclusao(response.getOcorrencias(0).getDsTipoCanalInclusao());
		saidaDTO.setDsCanalManutencao(response.getOcorrencias(0).getDsTipoCanalManutencao());
		saidaDTO.setDsPendenciaPagIntegrado(response.getOcorrencias(0).getDsPendenciaPagamentoIntegrado());
		saidaDTO.setDsSituacaoPendenciaContrato(response.getOcorrencias(0).getDsSituacaoPendenciaContrato());
		saidaDTO.setDsTipoBaixa(response.getOcorrencias(0).getDsTipoBaixa());
		saidaDTO.setDsTipoUnidadeOrg(response.getOcorrencias(0).getDsTipoUnidadeOrganizacional());
		saidaDTO.setDsUnidadeOrg(response.getOcorrencias(0).getDsUnidadeOrganizacional());
		saidaDTO.setCodMotivoSituacaoPendencia(response.getOcorrencias(0).getCdMotivoSituacaoPendencia());
		
		
		//saidaDTO.setHoraBaixaPendenciaContrato(response.getOcorrencias(0).getHrBaixaPendenciaContrato());
		
		//saidaDTO.setHorarioManutencaoRegistro(response.getOcorrencias(0).getHrManutencaoRegistro());
		
		SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");		
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		try {	
			saidaDTO.setHoraBaixaPendenciaContrato(String.valueOf(formato2.format(formato1.parse(response.getOcorrencias(0).getHrBaixaPendenciaContrato()))));						
		}  catch (ParseException e) {
			saidaDTO.setHoraBaixaPendenciaContrato("");
		}
		
		if (response.getOcorrencias(0).getHrInclusaoRegistro()!=null ){
			
			String stringData = response.getOcorrencias(0).getHrInclusaoRegistro().substring(0, 19);
			try {
				saidaDTO.setHoraInclusaoRegistro(formato2.format(formato1.parse(stringData)));
			}  catch (ParseException e) {
				saidaDTO.setHoraInclusaoRegistro("");
			}
		}	
		
		if (response.getOcorrencias(0).getHrManutencaoRegistro()!= null && !"".equals(response.getOcorrencias(0).getHrManutencaoRegistro())){
			
			String stringData = response.getOcorrencias(0).getHrManutencaoRegistro().substring(0, 19);
			try {
				saidaDTO.setHorarioManutencaoRegistro(formato2.format(formato1.parse(stringData)));
			}  catch (ParseException e) {
				saidaDTO.setHorarioManutencaoRegistro("");
			}
		}	
		
		formato1 = new SimpleDateFormat("dd.MM.yyyy");		
		formato2 = new SimpleDateFormat("dd/MM/yyyy");
		
		try{
			saidaDTO.setDataAtendimentoPendenciaContrato(String.valueOf(formato2.format(formato1.parse(response.getOcorrencias(0).getDtAtendimentoPendenciaContrato()))));
		}catch(ParseException e){
			saidaDTO.setDataAtendimentoPendenciaContrato("");
		}

		return saidaDTO;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.IManterPendenciasContratoService#reenviarEmail(br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ReenviarEmailEntradaDTO)
	 */
	public ReenviarEmailSaidaDTO reenviarEmail(ReenviarEmailEntradaDTO entrada) {
		ReenviarEmailRequest request = new ReenviarEmailRequest();
		request.setCdPendenciaPagamentoIntegrado(PgitUtil.verificaLongNulo(entrada.getCdPendenciaPagamentoIntegrado()));
		request.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setNrPendenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrPendenciaContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));

		ReenviarEmailResponse response  =  getFactoryAdapter().getReenviarEmailPDCAdapter().invokeProcess(request);

		return new ReenviarEmailSaidaDTO(response.getCodMensagem(), response.getMensagem());
	}
}