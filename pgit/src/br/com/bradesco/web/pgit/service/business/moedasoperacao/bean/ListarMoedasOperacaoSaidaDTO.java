/*
 * Nome: br.com.bradesco.web.pgit.service.business.moedasoperacao.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.moedasoperacao.bean;

/**
 * Nome: ListarMoedasOperacaoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarMoedasOperacaoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;	
	
	/** Atributo cdTipoServico. */
	private int cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo cdModalidadeServico. */
	private int cdModalidadeServico;
	
	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;
	
	/** Atributo cdRelacionadoProduto. */
	private int cdRelacionadoProduto;
	
	/** Atributo codigoMoedaIndice. */
	private int codigoMoedaIndice;
	
	/** Atributo dsEconomicoMoeda. */
	private String dsEconomicoMoeda;
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdModalidadeServico.
	 *
	 * @return cdModalidadeServico
	 */
	public int getCdModalidadeServico() {
		return cdModalidadeServico;
	}
	
	/**
	 * Set: cdModalidadeServico.
	 *
	 * @param cdModalidadeServico the cd modalidade servico
	 */
	public void setCdModalidadeServico(int cdModalidadeServico) {
		this.cdModalidadeServico = cdModalidadeServico;
	}
	
	/**
	 * Get: cdRelacionadoProduto.
	 *
	 * @return cdRelacionadoProduto
	 */
	public int getCdRelacionadoProduto() {
		return cdRelacionadoProduto;
	}
	
	/**
	 * Set: cdRelacionadoProduto.
	 *
	 * @param cdRelacionadoProduto the cd relacionado produto
	 */
	public void setCdRelacionadoProduto(int cdRelacionadoProduto) {
		this.cdRelacionadoProduto = cdRelacionadoProduto;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public int getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(int cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: codigoMoedaIndice.
	 *
	 * @return codigoMoedaIndice
	 */
	public int getCodigoMoedaIndice() {
		return codigoMoedaIndice;
	}
	
	/**
	 * Set: codigoMoedaIndice.
	 *
	 * @param codigoMoedaIndice the codigo moeda indice
	 */
	public void setCodigoMoedaIndice(int codigoMoedaIndice) {
		this.codigoMoedaIndice = codigoMoedaIndice;
	}
	
	/**
	 * Get: dsEconomicoMoeda.
	 *
	 * @return dsEconomicoMoeda
	 */
	public String getDsEconomicoMoeda() {
		return dsEconomicoMoeda;
	}
	
	/**
	 * Set: dsEconomicoMoeda.
	 *
	 * @param dsEconomicoMoeda the ds economico moeda
	 */
	public void setDsEconomicoMoeda(String dsEconomicoMoeda) {
		this.dsEconomicoMoeda = dsEconomicoMoeda;
	}
	
	/**
	 * Get: dsModalidadeServico.
	 *
	 * @return dsModalidadeServico
	 */
	public String getDsModalidadeServico() {
		return dsModalidadeServico;
	}
	
	/**
	 * Set: dsModalidadeServico.
	 *
	 * @param dsModalidadeServico the ds modalidade servico
	 */
	public void setDsModalidadeServico(String dsModalidadeServico) {
		this.dsModalidadeServico = dsModalidadeServico;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}
	
	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	

}
