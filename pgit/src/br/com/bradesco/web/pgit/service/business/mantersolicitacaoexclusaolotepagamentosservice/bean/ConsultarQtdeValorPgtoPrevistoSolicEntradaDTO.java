package br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.bean;

import java.util.Date;

public class ConsultarQtdeValorPgtoPrevistoSolicEntradaDTO {

	private Integer cdSolicitacaoPagamentoIntegrado;
	private Long cdpessoaJuridicaContrato;
	private Integer cdTipoContratoNegocio;
	private Long nrSequenciaContratoNegocio;
	private Long nrLoteInterno;
	private Integer cdTipoLayoutArquivo;
	private Date dtPgtoInicial;
	private Date dtPgtoFinal;
	private Integer cdProdutoServicoOperacao;
	private Integer cdProdutoServicoRelacionado;
	private Integer cdBancoDebito;
	private Integer cdAgenciaDebito;
	private Long cdContaDebito;
	private Integer cdTipoInscricaoPagador;
	private Long cdCpfCnpjPagador;
	private Integer cdFilialCpfCnpjPagador;
	private Integer cdControleCpfCnpjPagador;
	private Integer cdTipoCtaRecebedor;
	private Integer cdBcoRecebedor;
	private Integer cdAgenciaRecebedor;
	private Long cdContaRecebedor;
	private Integer cdTipoInscricaoRecebedor;
	private Long cdCpfCnpjRecebedor;
	private Integer cdFilialCnpjRecebedor;
	private Integer cdControleCpfRecebedor;

	public Integer getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}

	public void setCdSolicitacaoPagamentoIntegrado(
			Integer cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}

	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public Long getNrLoteInterno() {
		return nrLoteInterno;
	}

	public void setNrLoteInterno(Long nrLoteInterno) {
		this.nrLoteInterno = nrLoteInterno;
	}

	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	public Date getDtPgtoInicial() {
		return dtPgtoInicial;
	}

	public void setDtPgtoInicial(Date dtPgtoInicial) {
		this.dtPgtoInicial = dtPgtoInicial;
	}

	public Date getDtPgtoFinal() {
		return dtPgtoFinal;
	}

	public void setDtPgtoFinal(Date dtPgtoFinal) {
		this.dtPgtoFinal = dtPgtoFinal;
	}

	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}

	public void setCdProdutoServicoRelacionado(
			Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}

	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}

	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}

	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	public Long getCdContaDebito() {
		return cdContaDebito;
	}

	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	public Integer getCdTipoInscricaoPagador() {
		return cdTipoInscricaoPagador;
	}

	public void setCdTipoInscricaoPagador(Integer cdTipoInscricaoPagador) {
		this.cdTipoInscricaoPagador = cdTipoInscricaoPagador;
	}

	public Long getCdCpfCnpjPagador() {
		return cdCpfCnpjPagador;
	}

	public void setCdCpfCnpjPagador(Long cdCpfCnpjPagador) {
		this.cdCpfCnpjPagador = cdCpfCnpjPagador;
	}

	public Integer getCdFilialCpfCnpjPagador() {
		return cdFilialCpfCnpjPagador;
	}

	public void setCdFilialCpfCnpjPagador(Integer cdFilialCpfCnpjPagador) {
		this.cdFilialCpfCnpjPagador = cdFilialCpfCnpjPagador;
	}

	public Integer getCdControleCpfCnpjPagador() {
		return cdControleCpfCnpjPagador;
	}

	public void setCdControleCpfCnpjPagador(Integer cdControleCpfCnpjPagador) {
		this.cdControleCpfCnpjPagador = cdControleCpfCnpjPagador;
	}

	public Integer getCdTipoCtaRecebedor() {
		return cdTipoCtaRecebedor;
	}

	public void setCdTipoCtaRecebedor(Integer cdTipoCtaRecebedor) {
		this.cdTipoCtaRecebedor = cdTipoCtaRecebedor;
	}

	public Integer getCdBcoRecebedor() {
		return cdBcoRecebedor;
	}

	public void setCdBcoRecebedor(Integer cdBcoRecebedor) {
		this.cdBcoRecebedor = cdBcoRecebedor;
	}

	public Integer getCdAgenciaRecebedor() {
		return cdAgenciaRecebedor;
	}

	public void setCdAgenciaRecebedor(Integer cdAgenciaRecebedor) {
		this.cdAgenciaRecebedor = cdAgenciaRecebedor;
	}

	public Long getCdContaRecebedor() {
		return cdContaRecebedor;
	}

	public void setCdContaRecebedor(Long cdContaRecebedor) {
		this.cdContaRecebedor = cdContaRecebedor;
	}

	public Integer getCdTipoInscricaoRecebedor() {
		return cdTipoInscricaoRecebedor;
	}

	public void setCdTipoInscricaoRecebedor(Integer cdTipoInscricaoRecebedor) {
		this.cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
	}

	public Long getCdCpfCnpjRecebedor() {
		return cdCpfCnpjRecebedor;
	}

	public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor) {
		this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
	}

	public Integer getCdFilialCnpjRecebedor() {
		return cdFilialCnpjRecebedor;
	}

	public void setCdFilialCnpjRecebedor(Integer cdFilialCnpjRecebedor) {
		this.cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
	}

	public Integer getCdControleCpfRecebedor() {
		return cdControleCpfRecebedor;
	}

	public void setCdControleCpfRecebedor(Integer cdControleCpfRecebedor) {
		this.cdControleCpfRecebedor = cdControleCpfRecebedor;
	}

}
