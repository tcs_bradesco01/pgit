/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterconultremessarecebida;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.AlterarUltimoNumeroRemessaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.AlterarUltimoNumeroRemessaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.DetalharUltimoNumeroRemessaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.DetalharUltimoNumeroRemessaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.ListarUltimoNumeroRemessaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.ListarUltimoNumeroRemessaSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterConUltRemessaRecebida
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterConUltRemessaRecebidaService {

    
    /**
     * Listar ultimo numero remessa.
     *
     * @param listarUltimoNumeroRemessaEntradaDTO the listar ultimo numero remessa entrada dto
     * @return the list< listar ultimo numero remessa saida dt o>
     */
    List<ListarUltimoNumeroRemessaSaidaDTO> listarUltimoNumeroRemessa(ListarUltimoNumeroRemessaEntradaDTO  listarUltimoNumeroRemessaEntradaDTO);
    
    /**
     * Detalhar ultimo numero remessa.
     *
     * @param detalharUltimoNumeroRemessaEntradaDTO the detalhar ultimo numero remessa entrada dto
     * @return the detalhar ultimo numero remessa saida dto
     */
    DetalharUltimoNumeroRemessaSaidaDTO detalharUltimoNumeroRemessa(DetalharUltimoNumeroRemessaEntradaDTO  detalharUltimoNumeroRemessaEntradaDTO);
    
    /**
     * Alterar ultimo numero remessa.
     *
     * @param alterarUltimoNumeroRemessaEntradaDTO the alterar ultimo numero remessa entrada dto
     * @return the alterar ultimo numero remessa saida dto
     */
    AlterarUltimoNumeroRemessaSaidaDTO alterarUltimoNumeroRemessa(AlterarUltimoNumeroRemessaEntradaDTO  alterarUltimoNumeroRemessaEntradaDTO);

}

