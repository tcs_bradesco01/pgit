/**
 * Introduzir aqui os coment�rios oportunos para este arquivo. 
 * 
 * @author Nome Sobrenome1 Sobrenome2
 * @date 18/07/2008
 */
package br.com.bradesco.web.pgit.service.business.confrontodep.bean;

/**
 * <p><b>T�tulo:</b>        Arquitetura Bradesco Canal Web.</p>
 * <p><b>Descri��o:</b></p>
 * <p>VO utilizado para representar um confronto empresa x dependencia.</p>
 * 
 * @author nailton.santos  CPM Braxis / GFT </br>
 * Copyright (c) 2006 </br>
 * created   18/07/2008 </br>
 * @version      1.0
 */
public class ConfrontoDepVO {

    /**
     * Guarda o tipo da conta.
     */
    private String tipoConta;
    
    /**
     * Guarda o maximo de ocorrencias a ser listado.
     */
    private String maxOcorrencia;
    
    /**
     * Guarda o codigo da empresa selecionada.
     */
    private String codEmpresa;
    
    /**
     * Guarda a descri��o da empresa selecionada.
     */
    private String descEmpresa;    
    
    /**
     * Guarda o codigo da dependencia selecionada.
     */
    private String codDependencia;
    
    /**
     * Guarda a data de referencia de.
     */
    private String dtReferenciaDe;
    
    /**
     * Guarda a data de referencia at�.
     */
    private String dtReferenciaAte;
    
        
    /**
     * Guarda o codigo da conta.
     */
    private String cdConta;
    
    /**
     * Guarda a descri��o da conta.
     */
    private String descConta;
    
    /**
     * Guarda o codigo do tipo de saldo.
     */
    private String codTipoSaldo;
    
    /**
     * Guarda o valor do saldo contabil.
     */
    private String vlSaldoContabil;
    
    /**
     * Guarda o valor do saldo do produto.
     */
    private String vlSaldoProduto;
    
    /**
     * Guarda o valor do saldo da diferencia.
     */
    private String vlSaldoDiferenca;
    
    /**
     * Guarda a data cont�bil 
     */
    private String dtContabil;
    
    /**
     * Construtor padr�o.
     */
    public ConfrontoDepVO() {
        
    }

    /**
     * @return Retorna o valor do campo 'cdConta'.
     */
    public String getCdConta() {
        return cdConta;
    }

    /**
     * @param pCdConta - O valor do campo 'cdConta' a determinar.
     */
    public void setCdConta(String pCdConta) {
        cdConta = pCdConta;
    }


    /**
     * @return Retorna o valor do campo 'codTipoSaldo'.
     */
    public String getCodTipoSaldo() {
        return codTipoSaldo;
    }

    /**
     * @param pCodTipoSaldo - O valor do campo 'codTipoSaldo' a determinar.
     */
    public void setCodTipoSaldo(String pCodTipoSaldo) {
        codTipoSaldo = pCodTipoSaldo;
    }

    /**
     * @return Retorna o valor do campo 'descConta'.
     */
    public String getDescConta() {
        return descConta;
    }

    /**
     * @param pDescConta - O valor do campo 'descConta' a determinar.
     */
    public void setDescConta(String pDescConta) {
        descConta = pDescConta;
    }

    /**
     * @return Retorna o valor do campo 'vlSaldoContabil'.
     */
    public String getVlSaldoContabil() {
        return vlSaldoContabil;
    }

    /**
     * @param pVlSaldoContabil - O valor do campo 'vlSaldoContabil' a determinar.
     */
    public void setVlSaldoContabil(String pVlSaldoContabil) {
        vlSaldoContabil = pVlSaldoContabil;
    }

    /**
     * @return Retorna o valor do campo 'vlSaldoDiferenca'.
     */
    public String getVlSaldoDiferenca() {
        return vlSaldoDiferenca;
    }

    /**
     * @param pVlSaldoDiferenca - O valor do campo 'vlSaldoDiferenca' a determinar.
     */
    public void setVlSaldoDiferenca(String pVlSaldoDiferenca) {
        vlSaldoDiferenca = pVlSaldoDiferenca;
    }

    /**
     * @return Retorna o valor do campo 'vlSaldoProduto'.
     */
    public String getVlSaldoProduto() {
        return vlSaldoProduto;
    }

    /**
     * @param pVlSaldoProduto - O valor do campo 'vlSaldoProduto' a determinar.
     */
    public void setVlSaldoProduto(String pVlSaldoProduto) {
        vlSaldoProduto = pVlSaldoProduto;
    }

    /**
     * @return Retorna o valor do campo 'codDependencia'.
     */
    public String getCodDependencia() {
        return codDependencia;
    }

    /**
     * @param pCodDependencia - O valor do campo 'codDependencia' a determinar.
     */
    public void setCodDependencia(String pCodDependencia) {
        codDependencia = pCodDependencia;
    }

    /**
     * @return Retorna o valor do campo 'codEmpresa'.
     */
    public String getCodEmpresa() {
        return codEmpresa;
    }

    /**
     * @param pCodEmpresa - O valor do campo 'codEmpresa' a determinar.
     */
    public void setCodEmpresa(String pCodEmpresa) {
        codEmpresa = pCodEmpresa;
    }

    /**
     * @return Retorna o valor do campo 'dtReferenciaAte'.
     */
    public String getDtReferenciaAte() {
        return dtReferenciaAte;
    }

    /**
     * @param pDtReferenciaAte - O valor do campo 'dtReferenciaAte' a determinar.
     */
    public void setDtReferenciaAte(String pDtReferenciaAte) {
        dtReferenciaAte = pDtReferenciaAte;
    }

    /**
     * @return Retorna o valor do campo 'dtReferenciaDe'.
     */
    public String getDtReferenciaDe() {
        return dtReferenciaDe;
    }

    /**
     * @param pDtReferenciaDe - O valor do campo 'dtReferenciaDe' a determinar.
     */
    public void setDtReferenciaDe(String pDtReferenciaDe) {
        dtReferenciaDe = pDtReferenciaDe;
    }

    /**
     * @return Retorna o valor do campo 'maxOcorrencia'.
     */
    public String getMaxOcorrencia() {
        return maxOcorrencia;
    }

    /**
     * @param pMaxOcorrencia - O valor do campo 'maxOcorrencia' a determinar.
     */
    public void setMaxOcorrencia(String pMaxOcorrencia) {
        maxOcorrencia = pMaxOcorrencia;
    }

    /**
     * @return Retorna o valor do campo 'dtContabil'.
     */
    public String getDtContabil() {
        return dtContabil;
    }

    /**
     * @param pDtContabil - O valor do campo 'dtContabil' a determinar.
     */
    public void setDtContabil(String pDtContabil) {
        dtContabil = pDtContabil;
    }

    /**
     * @return Retorna o valor do campo 'tipoConta'.
     */
    public String getTipoConta() {
        return tipoConta;
    }

    /**
     * @param pTipoConta -
     *            O valor do campo 'tipoConta' a determinar.
     */
    public void setTipoConta(String pTipoConta) {
        this.tipoConta = pTipoConta;
    }

    /**
     * @return Retorna o valor do campo 'descEmpresa'.
     */
    public String getDescEmpresa() {
        return descEmpresa;
    }

    /**
     * @param pDescEmpresa - O valor do campo 'descEmpresa' a determinar.
     */
    public void setDescEmpresa(String pDescEmpresa) {
        this.descEmpresa = pDescEmpresa;
    }
}
