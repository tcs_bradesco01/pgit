/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarServicoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarServicoEntradaDTO {
	
	/** Atributo numeroOcorrencias. */
	private Integer numeroOcorrencias;
    
    /** Atributo cdNaturezaServico. */
    private Integer cdNaturezaServico;
    
	/**
	 * Listar servico entrada dto.
	 */
	public ListarServicoEntradaDTO() {
		super() ;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Get: cdNaturezaServico.
	 *
	 * @return cdNaturezaServico
	 */
	public Integer getCdNaturezaServico() {
		return cdNaturezaServico;
	}

	/**
	 * Set: cdNaturezaServico.
	 *
	 * @param cdNaturezaServico the cd natureza servico
	 */
	public void setCdNaturezaServico(Integer cdNaturezaServico) {
		this.cdNaturezaServico = cdNaturezaServico;
	}

	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}

	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}

	
}
