/*
 * Nome: br.com.bradesco.web.pgit.service.report.contrato.data
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 05/02/2016
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato.contrato.data;

/**
 * Nome: Participante
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class Participante {

    /** Atributo nome. */
    private String nome = null;

    /** Atributo cnpjCpf. */
    private String cnpjCpf = null;
    
    /**
     * Participante.
     * 
     * @param nomeParticipante
     *            the nome participante
     * @param cnpjCpf
     *            the cnpj cpf
     */
    public Participante(String nomeParticipante, String cnpjCpf) {
        super();
        this.nome = nomeParticipante;
        this.cnpjCpf = cnpjCpf;
    }

    /**
     * Nome: getNome.
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Nome: getCnpjCpf
     *
     * @return cnpjCpf
     */
    public String getCnpjCpf() {
        return cnpjCpf;
    }
}
