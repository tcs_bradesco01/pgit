/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ConsultarDadosBasicoContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarDadosBasicoContratoEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContrato. */
	private int cdTipoContrato;
	
	/** Atributo numeroSequenciaContrato. */
	private long numeroSequenciaContrato;
	
	/** Atributo cdClub. */
	private long cdClub;
	
	/** Atributo cdCgcCpf. */
	private long cdCgcCpf;
	
	/** Atributo cdFilialCgcCpf. */
	private int cdFilialCgcCpf;
	
	/** Atributo cdControleCgcCpf. */
	private int cdControleCgcCpf;
	
	/** Atributo cdAgencia. */
	private int cdAgencia;
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public int getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(int cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdCgcCpf.
	 *
	 * @return cdCgcCpf
	 */
	public long getCdCgcCpf() {
		return cdCgcCpf;
	}
	
	/**
	 * Set: cdCgcCpf.
	 *
	 * @param cdCgcCpf the cd cgc cpf
	 */
	public void setCdCgcCpf(long cdCgcCpf) {
		this.cdCgcCpf = cdCgcCpf;
	}
	
	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public long getCdClub() {
		return cdClub;
	}
	
	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(long cdClub) {
		this.cdClub = cdClub;
	}
	
	/**
	 * Get: cdControleCgcCpf.
	 *
	 * @return cdControleCgcCpf
	 */
	public int getCdControleCgcCpf() {
		return cdControleCgcCpf;
	}
	
	/**
	 * Set: cdControleCgcCpf.
	 *
	 * @param cdControleCgcCpf the cd controle cgc cpf
	 */
	public void setCdControleCgcCpf(int cdControleCgcCpf) {
		this.cdControleCgcCpf = cdControleCgcCpf;
	}
	
	/**
	 * Get: cdFilialCgcCpf.
	 *
	 * @return cdFilialCgcCpf
	 */
	public int getCdFilialCgcCpf() {
		return cdFilialCgcCpf;
	}
	
	/**
	 * Set: cdFilialCgcCpf.
	 *
	 * @param cdFilialCgcCpf the cd filial cgc cpf
	 */
	public void setCdFilialCgcCpf(int cdFilialCgcCpf) {
		this.cdFilialCgcCpf = cdFilialCgcCpf;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public int getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(int cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: numeroSequenciaContrato.
	 *
	 * @return numeroSequenciaContrato
	 */
	public long getNumeroSequenciaContrato() {
		return numeroSequenciaContrato;
	}
	
	/**
	 * Set: numeroSequenciaContrato.
	 *
	 * @param numeroSequenciaContrato the numero sequencia contrato
	 */
	public void setNumeroSequenciaContrato(long numeroSequenciaContrato) {
		this.numeroSequenciaContrato = numeroSequenciaContrato;
	}
	
	
	
	
}

 

