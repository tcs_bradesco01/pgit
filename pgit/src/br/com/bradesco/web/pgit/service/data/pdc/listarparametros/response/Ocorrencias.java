/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarparametros.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdParametro
     */
    private int _cdParametro = 0;

    /**
     * keeps track of state for field: _cdParametro
     */
    private boolean _has_cdParametro;

    /**
     * Field _dsParametro
     */
    private java.lang.String _dsParametro;

    /**
     * Field _cdIndicadorUtilizaDescricao
     */
    private int _cdIndicadorUtilizaDescricao = 0;

    /**
     * keeps track of state for field: _cdIndicadorUtilizaDescricao
     */
    private boolean _has_cdIndicadorUtilizaDescricao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparametros.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorUtilizaDescricao
     * 
     */
    public void deleteCdIndicadorUtilizaDescricao()
    {
        this._has_cdIndicadorUtilizaDescricao= false;
    } //-- void deleteCdIndicadorUtilizaDescricao() 

    /**
     * Method deleteCdParametro
     * 
     */
    public void deleteCdParametro()
    {
        this._has_cdParametro= false;
    } //-- void deleteCdParametro() 

    /**
     * Returns the value of field 'cdIndicadorUtilizaDescricao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorUtilizaDescricao'.
     */
    public int getCdIndicadorUtilizaDescricao()
    {
        return this._cdIndicadorUtilizaDescricao;
    } //-- int getCdIndicadorUtilizaDescricao() 

    /**
     * Returns the value of field 'cdParametro'.
     * 
     * @return int
     * @return the value of field 'cdParametro'.
     */
    public int getCdParametro()
    {
        return this._cdParametro;
    } //-- int getCdParametro() 

    /**
     * Returns the value of field 'dsParametro'.
     * 
     * @return String
     * @return the value of field 'dsParametro'.
     */
    public java.lang.String getDsParametro()
    {
        return this._dsParametro;
    } //-- java.lang.String getDsParametro() 

    /**
     * Method hasCdIndicadorUtilizaDescricao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorUtilizaDescricao()
    {
        return this._has_cdIndicadorUtilizaDescricao;
    } //-- boolean hasCdIndicadorUtilizaDescricao() 

    /**
     * Method hasCdParametro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdParametro()
    {
        return this._has_cdParametro;
    } //-- boolean hasCdParametro() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorUtilizaDescricao'.
     * 
     * @param cdIndicadorUtilizaDescricao the value of field
     * 'cdIndicadorUtilizaDescricao'.
     */
    public void setCdIndicadorUtilizaDescricao(int cdIndicadorUtilizaDescricao)
    {
        this._cdIndicadorUtilizaDescricao = cdIndicadorUtilizaDescricao;
        this._has_cdIndicadorUtilizaDescricao = true;
    } //-- void setCdIndicadorUtilizaDescricao(int) 

    /**
     * Sets the value of field 'cdParametro'.
     * 
     * @param cdParametro the value of field 'cdParametro'.
     */
    public void setCdParametro(int cdParametro)
    {
        this._cdParametro = cdParametro;
        this._has_cdParametro = true;
    } //-- void setCdParametro(int) 

    /**
     * Sets the value of field 'dsParametro'.
     * 
     * @param dsParametro the value of field 'dsParametro'.
     */
    public void setDsParametro(java.lang.String dsParametro)
    {
        this._dsParametro = dsParametro;
    } //-- void setDsParametro(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarparametros.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarparametros.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarparametros.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarparametros.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
