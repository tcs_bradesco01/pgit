package br.com.bradesco.web.pgit.service.business.combo.bean;


// TODO: Auto-generated Javadoc
/**
 * Arquivo criado em 11/12/15.
 */
public class ListarServicoRelacionadoPgitEntradaDTO {

    /** The cd natureza servico. */
    private Integer cdNaturezaServico;

    /** The max ocorrencias. */
    private Integer maxOcorrencias;

    /**
     * Sets the cd natureza servico.
     *
     * @param cdNaturezaServico the cd natureza servico
     */
    public void setCdNaturezaServico(Integer cdNaturezaServico) {
        this.cdNaturezaServico = cdNaturezaServico;
    }

    /**
     * Gets the cd natureza servico.
     *
     * @return the cd natureza servico
     */
    public Integer getCdNaturezaServico() {
        return this.cdNaturezaServico;
    }

    /**
     * Sets the max ocorrencias.
     *
     * @param maxOcorrencias the max ocorrencias
     */
    public void setMaxOcorrencias(Integer maxOcorrencias) {
        this.maxOcorrencias = maxOcorrencias;
    }

    /**
     * Gets the max ocorrencias.
     *
     * @return the max ocorrencias
     */
    public Integer getMaxOcorrencias() {
        return this.maxOcorrencias;
    }
}