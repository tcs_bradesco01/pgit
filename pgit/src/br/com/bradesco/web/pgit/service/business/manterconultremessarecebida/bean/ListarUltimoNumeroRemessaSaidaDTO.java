/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean;

/**
 * Nome: ListarUltimoNumeroRemessaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ListarUltimoNumeroRemessaSaidaDTO {

	private Integer cdTipoLayoutArquivo;

	private String dsTipoLayoutArquivo;

	private Integer cdTipoLoteLayout;

	private String dsTipoLoteLayout;

	private Long cdPerfilTrocaArquivo;

	private Long nrUltimoArquivo;

	private Long cdClub;

	private String cdCorpoCpfCnpj;

	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	public Integer getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}

	public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}

	public String getDsTipoLoteLayout() {
		return dsTipoLoteLayout;
	}

	public void setDsTipoLoteLayout(String dsTipoLoteLayout) {
		this.dsTipoLoteLayout = dsTipoLoteLayout;
	}

	public Long getCdPerfilTrocaArquivo() {
		return cdPerfilTrocaArquivo;
	}

	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo) {
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	public Long getNrUltimoArquivo() {
		return nrUltimoArquivo;
	}

	public void setNrUltimoArquivo(Long nrUltimoArquivo) {
		this.nrUltimoArquivo = nrUltimoArquivo;
	}

	public Long getCdClub() {
		return cdClub;
	}

	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

	public String getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}

	public void setCdCorpoCpfCnpj(String cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}

}
