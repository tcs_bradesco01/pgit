/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.ImprimirAnexoPrimeiroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.ImprimirAnexoPrimeiroContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ImprimirAnexoSegundoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ImprimirAnexoSegundoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.AlterarPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.AlterarPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarHistoricoPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarHistoricoPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharHistoricoPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharHistoricoPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ExcluirPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ExcluirPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.IncluirPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.IncluirPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ListaContratosVinculadosPerfilEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ListaContratosVinculadosPerfilOcorrencias;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterPerfilTrocaArqLayout
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterPerfilTrocaArqLayoutService {
	
	/**
	 * Consultar lista perfil troca arquivo.
	 *
	 * @param entrada the entrada
	 * @return the consultar lista perfil troca arquivo saida dto
	 */
	ConsultarListaPerfilTrocaArquivoSaidaDTO consultarListaPerfilTrocaArquivo(ConsultarListaPerfilTrocaArquivoEntradaDTO entrada);
	
	/**
	 * Detalhar perfil troca arquivo.
	 *
	 * @param entrada the entrada
	 * @return the detalhar perfil troca arquivo saida dto
	 */
	DetalharPerfilTrocaArquivoSaidaDTO detalharPerfilTrocaArquivo(DetalharPerfilTrocaArquivoEntradaDTO entrada);
	
	/**
	 * Incluir perfil troca arquivo.
	 *
	 * @param entrada the entrada
	 * @return the incluir perfil troca arquivo saida dto
	 */
	IncluirPerfilTrocaArquivoSaidaDTO incluirPerfilTrocaArquivo(IncluirPerfilTrocaArquivoEntradaDTO entrada);
	
	/**
	 * Alterar perfil troca arquivo.
	 *
	 * @param entrada the entrada
	 * @return the alterar perfil troca arquivo saida dto
	 */
	AlterarPerfilTrocaArquivoSaidaDTO alterarPerfilTrocaArquivo(AlterarPerfilTrocaArquivoEntradaDTO entrada);
	
	/**
	 * Excluir perfil troca arquivo.
	 *
	 * @param entrada the entrada
	 * @return the excluir perfil troca arquivo saida dto
	 */
	ExcluirPerfilTrocaArquivoSaidaDTO excluirPerfilTrocaArquivo(ExcluirPerfilTrocaArquivoEntradaDTO entrada);
	
	/**
	 * Consultar historico perfil troca arquivo.
	 *
	 * @param entrada the entrada
	 * @return the list< consultar historico perfil troca arquivo saida dt o>
	 */
	List<ConsultarHistoricoPerfilTrocaArquivoSaidaDTO> consultarHistoricoPerfilTrocaArquivo(ConsultarHistoricoPerfilTrocaArquivoEntradaDTO entrada);
	
	/**
	 * Detalhar historico perfil troca arquivo.
	 *
	 * @param entrada the entrada
	 * @return the detalhar historico perfil troca arquivo saida dto
	 */
	DetalharHistoricoPerfilTrocaArquivoSaidaDTO detalharHistoricoPerfilTrocaArquivo(DetalharHistoricoPerfilTrocaArquivoEntradaDTO entrada);
	
	/**
	 * Listar contratos vinculados perfil.
	 *
	 * @param entrada the entrada
	 * @return the list< lista contratos vinculados perfil ocorrencias>
	 */
	List<ListaContratosVinculadosPerfilOcorrencias> listarContratosVinculadosPerfil(ListaContratosVinculadosPerfilEntradaDTO entrada);
	
}

