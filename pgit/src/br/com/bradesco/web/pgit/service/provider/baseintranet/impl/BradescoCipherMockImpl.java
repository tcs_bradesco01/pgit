/*
 * Nome: br.com.bradesco.web.pgit.service.provider.baseintranet.impl
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.provider.baseintranet.impl;

import java.util.HashMap;
import java.util.Map;

import br.com.bradesco.web.aq.application.security.encryption.IBradescoCipher;
import br.com.bradesco.web.aq.application.security.exception.BradescoCipherException;

/**
 * Nome: BradescoCipherMockImpl
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class BradescoCipherMockImpl implements IBradescoCipher {

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.aq.application.security.encryption.IBradescoCipher#decript(byte[])
	 */
	public byte[] decript(byte[] paramArrayOfByte) throws BradescoCipherException {
		return paramArrayOfByte;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.aq.application.security.encryption.IBradescoCipher#decript(java.lang.String)
	 */
	public String decript(String paramString) throws BradescoCipherException {
		return paramString;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.aq.application.security.encryption.IBradescoCipher#encript(byte[])
	 */
	public byte[] encript(byte[] paramArrayOfByte) throws BradescoCipherException {
		return paramArrayOfByte;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.aq.application.security.encryption.IBradescoCipher#encript(java.lang.String)
	 */
	public String encript(String paramString) throws BradescoCipherException {
		return paramString;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.aq.application.security.encryption.IBradescoCipher#getAlgorithm()
	 */
	public String getAlgorithm() throws BradescoCipherException {
		return "";
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.aq.application.security.encryption.IBradescoCipher#getProviderInfo()
	 */
	public Map<String, String> getProviderInfo() throws BradescoCipherException {
		return new HashMap<String, String>();
	}

}
