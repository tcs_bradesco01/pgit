/**
 * Nome: br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.impl
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.impl;

import static br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.
IManterSolicLiberacaoLotePgtoServiceConstants.QTDE_MAXIMA_OCORRENCIAS;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaBigDecimalNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringNula;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formatarDataTrilha;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.OcorrenciasLotePagamentosDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.IManterSolicLiberacaoLotePgtoService;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicliberacaolotepgto.
request.ConsultarSolicLiberacaoLotePgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolicliberacaolotepgto.
response.ConsultarSolicLiberacaoLotePgtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicliberacaolotepgto.
request.DetalharSolicLiberacaoLotePgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicliberacaolotepgto.
response.DetalharSolicLiberacaoLotePgtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicliberacaolotepgto.
request.ExcluirSolicLiberacaoLotePgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicliberacaolotepgto.
response.ExcluirSolicLiberacaoLotePgtoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicliberacaolotepgto.
request.IncluirSolicLiberacaoLotePgtoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicliberacaolotepgto.
response.IncluirSolicLiberacaoLotePgtoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ManterSolicLiberacaoLotePgtoServiceImpl
 * <p>
 * Prop�sito: Implementa��o do adaptador ManterSolicLiberacaoLotePgto
 * </p>
 * 
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IManterSolicLiberacaoLotePgtoService
 */
public class ManterSolicLiberacaoLotePgtoServiceImpl implements IManterSolicLiberacaoLotePgtoService {

    /**
     * Atributo FactoryAdapter
     */
    private FactoryAdapter factoryAdapter;

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.
     * IManterSolicLiberacaoLotePgtoService#consultarSolicLiberacaoLotePgto(br.com.bradesco.web.pgit.service.
     * business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO)
     * @param entrada
     */
    public ConsultarLotePagamentosSaidaDTO consultarSolicLiberacaoLotePgto(ConsultarLotePagamentosEntradaDTO entrada) {

        ConsultarLotePagamentosSaidaDTO saida = new ConsultarLotePagamentosSaidaDTO();
        ConsultarSolicLiberacaoLotePgtoRequest request = new ConsultarSolicLiberacaoLotePgtoRequest();
        ConsultarSolicLiberacaoLotePgtoResponse response = null;

        request.setNrOcorrencias(QTDE_MAXIMA_OCORRENCIAS);
        request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setNrLoteInterno(verificaLongNulo(entrada.getNrLoteInterno()));
        request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
        request.setDtPgtoInicial(verificaStringNula(entrada.getDtPgtoInicial()));
        request.setDtPgtoFinal(verificaStringNula(entrada.getDtPgtoFinal()));
        request.setCdSituacaoSolicitacaoPagamento(verificaIntegerNulo(entrada.getCdSituacaoSolicitacaoPagamento()));
        request.setCdMotivoSolicitacao(verificaIntegerNulo(entrada.getCdMotivoSolicitacao()));

        response = getFactoryAdapter().getConsultarSolicLiberacaoLotePgtoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setNumeroLinhas(response.getNumeroLinhas());

        saida.setOcorrencias(new ArrayList<OcorrenciasLotePagamentosDTO>());

        for (int i = 0; i < response.getOcorrenciasCount(); i++) {
            OcorrenciasLotePagamentosDTO ocorrencia = new OcorrenciasLotePagamentosDTO();

            ocorrencia.setCdSolicitacaoPagamentoIntegrado(response.getOcorrencias(i)
                .getCdSolicitacaoPagamentoIntegrado());
            ocorrencia.setNrSolicitacaoPagamentoIntegrado(response.getOcorrencias(i)
                .getNrSolicitacaoPagamentoIntegrado());
            ocorrencia.setCdpessoaJuridicaContrato(response.getOcorrencias(i).getCdpessoaJuridicaContrato());
            ocorrencia.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
            ocorrencia.setNrSequenciaContratoNegocio(response.getOcorrencias(i).getNrSequenciaContratoNegocio());
            ocorrencia.setNrLoteInterno(response.getOcorrencias(i).getNrLoteInterno());
            ocorrencia.setCdTipoLayoutArquivo(response.getOcorrencias(i).getCdTipoLayoutArquivo());
            ocorrencia.setDsTipoLayoutArquivo(response.getOcorrencias(i).getDsTipoLayoutArquivo());
            ocorrencia
                .setCdSituacaoSolicitacaoPagamento(response.getOcorrencias(i).getCdSituacaoSolicitacaoPagamento());
            ocorrencia.setDsSituacaoSolicitaoPgto(response.getOcorrencias(i).getDsSituacaoSolicitaoPgto());
            ocorrencia.setCdMotivoSolicitacao(response.getOcorrencias(i).getCdMotivoSolicitacao());
            ocorrencia.setDsMotivoSolicitacao(response.getOcorrencias(i).getDsMotivoSolicitacao());
            ocorrencia.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
            ocorrencia.setDsSolicitacao(response.getOcorrencias(i).getDsSolicitacao());
            ocorrencia.setHrSolicitacao(response.getOcorrencias(i).getHrSolicitacao());
            ocorrencia.setDsProdutoServicoOperacao(response.getOcorrencias(i).getDsProdutoServicoOperacao());
            ocorrencia.setDataHoraFormatada(PgitUtil.concatenarCampos(response.getOcorrencias(i).getDsSolicitacao(),
                response.getOcorrencias(i).getHrSolicitacao(), "-"));

            saida.getOcorrencias().add(ocorrencia);
        }

        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.
     * IManterSolicLiberacaoLotePgtoService#detalharSolicLiberacaoLotePgto(br.com.bradesco.web.pgit.service.business.
     * lotepagamentos.bean.DetalharLotePagamentosEntradaDTO)
     * @param entrada
     */
    public DetalharLotePagamentosSaidaDTO detalharSolicLiberacaoLotePgto(DetalharLotePagamentosEntradaDTO entrada) {

        DetalharLotePagamentosSaidaDTO saida = new DetalharLotePagamentosSaidaDTO();
        DetalharSolicLiberacaoLotePgtoRequest request = new DetalharSolicLiberacaoLotePgtoRequest();
        DetalharSolicLiberacaoLotePgtoResponse response = null;

        request.setCdSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entrada
            .getCdSolicitacaoPagamentoIntegrado()));
        request.setNrSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entrada
            .getNrSolicitacaoPagamentoIntegrado()));

        response = getFactoryAdapter().getDetalharSolicLiberacaoLotePgtoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());
        saida.setCdpessoaJuridicaContrato(response.getCdpessoaJuridicaContrato());
        saida.setCdTipoContratoNegocio(response.getCdTipoContratoNegocio());
        saida.setNrSequenciaContratoNegocio(response.getNrSequenciaContratoNegocio());
        saida.setCdSolicitacaoPagamentoIntegrado(response.getCdSolicitacaoPagamentoIntegrado());
        saida.setNrSolicitacaoPagamentoIntegrado(response.getNrSolicitacaoPagamentoIntegrado());
        saida.setDsSituacaoSolicitaoPgto(response.getDsSituacaoSolicitaoPgto());
        saida.setDsMotivoSolicitacao(response.getDsMotivoSolicitacao());
        saida.setDsSolicitacao(response.getDsSolicitacao());
        saida.setHrSolicitacao(response.getHrSolicitacao());
        saida.setFormatDataHora(response.getDsSolicitacao() + response.getHrSolicitacao());
        if(saida.getDsSituacaoSolicitaoPgto().toUpperCase().equals("PENDENTE")){
        	saida.setFormatDataHora(null);        	
        }    
        if(StringUtils.isNotEmpty(saida.getFormatDataHora())){
        	saida.setFormatDataHora(String.format("%s - %s", response.getDsSolicitacao(), response.getHrSolicitacao()));	
        }
        saida.setNrLoteInterno(response.getNrLoteInterno());
        saida.setDsTipoLayoutArquivo(response.getDsTipoLayoutArquivo());
        saida.setDtPgtoInicial(response.getDtPgtoInicial());
        saida.setDtPgtoFinal(response.getDtPgtoFinal());
        saida.setCdBancoDebito(response.getCdBancoDebito());
        saida.setCdAgenciaDebito(response.getCdAgenciaDebito());
        saida.setCdContaDebito(response.getCdContaDebito());
        saida.setDsProdutoServicoOperacao(response.getDsProdutoServicoOperacao());
        saida.setDsModalidadePgtoCliente(response.getDsModalidadePgtoCliente());

        saida.setQtdeTotalPagtoPrevistoSoltc(response.getQtdeTotalPagtoPrevistoSoltc());
        saida.setVlrTotPagtoPrevistoSolicitacao(response.getVlrTotPagtoPrevistoSolicitacao());

        saida.setQtTotalPgtoEfetivadoSolicitacao(response.getQtTotalPgtoEfetivadoSolicitacao());
        saida.setVlTotalPgtoEfetuadoSolicitacao(response.getVlTotalPgtoEfetuadoSolicitacao());
        saida.setCdCanalInclusao(response.getCdCanalInclusao());
        saida.setDsCanalInclusao(response.getDsCanalInclusao());
        saida.setCdAutenticacaoSegurancaInclusao(response.getCdAutenticacaoSegurancaInclusao());
        saida.setHrInclusaoRegistro(formatarDataTrilha(response.getHrInclusaoRegistro()));
        saida.setCdCanalManutencao(response.getCdCanalManutencao());
        saida.setDsCanalManutencao(response.getDsCanalManutencao());
        saida.setCdAutenticacaoSegurancaManutencao(response.getCdAutenticacaoSegurancaManutencao());
        saida.setNmOperacaoFluxoManutencao(response.getNmOperacaoFluxoManutencao());
        saida.setHrManutencaoRegistro(formatarDataTrilha(response.getHrManutencaoRegistro()));

        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.
     * IManterSolicLiberacaoLotePgtoService#excluirSolicLiberacaoLotePgto(br.com.bradesco.web.pgit.service.business.
     * lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO)
     * @param entrada
     */
    public ExcluirLotePagamentosSaidaDTO excluirSolicLiberacaoLotePgto(ExcluirLotePagamentosEntradaDTO entrada) {

        ExcluirLotePagamentosSaidaDTO saida = new ExcluirLotePagamentosSaidaDTO();
        ExcluirSolicLiberacaoLotePgtoRequest request = new ExcluirSolicLiberacaoLotePgtoRequest();
        ExcluirSolicLiberacaoLotePgtoResponse response = null;

        request.setCdSolicitacaoPagamentoIntegrado(entrada.getCdSolicitacaoPagamentoIntegrado());
        request.setNrSolicitacaoPagamentoIntegrado(entrada.getNrSolicitacaoPagamentoIntegrado());

        response = getFactoryAdapter().getExcluirSolicLiberacaoLotePgtoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());

        return saida;
    }

    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.
     * IManterSolicLiberacaoLotePgtoService#incluirSolicLiberacaoLotePgto(br.com.bradesco.web.pgit.service.
     * business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO)
     */
    public IncluirLotePagamentosSaidaDTO incluirSolicLiberacaoLotePgto(IncluirLotePagamentosEntradaDTO entrada) {

        IncluirLotePagamentosSaidaDTO saida = new IncluirLotePagamentosSaidaDTO();
        IncluirSolicLiberacaoLotePgtoRequest request = new IncluirSolicLiberacaoLotePgtoRequest();
        IncluirSolicLiberacaoLotePgtoResponse response = null;

        request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
        request.setNrLoteInterno(verificaLongNulo(entrada.getNrLoteInterno()));
        request.setCdTipoLayoutArquivo(verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
        request.setDtPgtoInicial(verificaStringNula(entrada.getDtPgtoInicial()));
        request.setDtPgtoFinal(verificaStringNula(entrada.getDtPgtoFinal()));
        request.setCdProdutoServicoOperacao(verificaIntegerNulo(entrada.getCdProdutoServicoOperacao()));
        request.setCdProdutoServicoRelacionado(verificaIntegerNulo(entrada.getCdProdutoServicoRelacionado()));
        request.setCdBancoDebito(verificaIntegerNulo(entrada.getCdBancoDebito()));
        request.setCdAgenciaDebito(verificaIntegerNulo(entrada.getCdAgenciaDebito()));
        request.setCdContaDebito(verificaLongNulo(entrada.getCdContaDebito()));
        request.setQtdeTotalPagtoPrevistoSoltc(verificaLongNulo(entrada.getQtdeTotalPagtoPrevistoSoltc()));
        request.setVlrTotPagtoPrevistoSolicitacao(verificaBigDecimalNulo(entrada.getVlrTotPagtoPrevistoSolicitacao()));
        request.setCdTipoInscricaoPagador(verificaIntegerNulo(entrada.getCdTipoInscricaoPagador()));
        request.setCdCpfCnpjPagador(verificaLongNulo(entrada.getCdCpfCnpjPagador()));
        request.setCdFilialCpfCnpjPagador(verificaIntegerNulo(entrada.getCdFilialCpfCnpjPagador()));
        request.setCdControleCpfCnpjPagador(verificaIntegerNulo(entrada.getCdControleCpfCnpjPagador()));
        request.setCdTipoCtaRecebedor(verificaIntegerNulo(entrada.getCdTipoCtaRecebedor()));
        request.setCdBcoRecebedor(verificaIntegerNulo(entrada.getCdBcoRecebedor()));
        request.setCdAgenciaRecebedor(verificaIntegerNulo(entrada.getCdAgenciaRecebedor()));
        request.setCdContaRecebedor(verificaLongNulo(entrada.getCdContaRecebedor()));
        request.setCdTipoInscricaoRecebedor(verificaIntegerNulo(entrada.getCdTipoInscricaoRecebedor()));
        request.setCdCpfCnpjRecebedor(PgitUtil.verificaLongNulo(entrada.getCdCpfCnpjRecebedor()));
        request.setCdFilialCnpjRecebedor(verificaIntegerNulo(entrada.getCdFilialCnpjRecebedor()));
        request.setCdControleCpfRecebedor(verificaIntegerNulo(entrada.getCdControleCpfRecebedor()));

        response = getFactoryAdapter().getIncluirSolicLiberacaoLotePgtoPDCAdapter().invokeProcess(request);

        saida.setCodMensagem(response.getCodMensagem());
        saida.setMensagem(response.getMensagem());

        return saida;
    }

    /**
     * Nome: getFactoryAdapter
     * 
     * @return
     */
    public FactoryAdapter getFactoryAdapter() {
        return factoryAdapter;
    }

    /**
     * Nome: setFactoryAdapter
     * 
     * @param factoryAdapter
     */
    public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
        this.factoryAdapter = factoryAdapter;
    }
}