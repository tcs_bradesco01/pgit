/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean;

/**
 * Nome: manterServicosRelacionadosDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class manterServicosRelacionadosDTO {
	
	/** Atributo servico. */
	private String servico;
	
	/** Atributo servicoRelacionado. */
	private String  servicoRelacionado;
	
	/** Atributo servicoComposto. */
	private String servicoComposto;
	
	/** Atributo naturezaServico. */
	private String naturezaServico;
	
	
	
	/**
	 * Get: servico.
	 *
	 * @return servico
	 */
	public String getServico() {
		return servico;
	}
	
	/**
	 * Set: servico.
	 *
	 * @param servico the servico
	 */
	public void setServico(String servico) {
		this.servico = servico;
	}
	
	/**
	 * Get: servicoRelacionado.
	 *
	 * @return servicoRelacionado
	 */
	public String getServicoRelacionado() {
		return servicoRelacionado;
	}
	
	/**
	 * Set: servicoRelacionado.
	 *
	 * @param servicoRelacionado the servico relacionado
	 */
	public void setServicoRelacionado(String servicoRelacionado) {
		this.servicoRelacionado = servicoRelacionado;
	}
	
	/**
	 * Get: naturezaServico.
	 *
	 * @return naturezaServico
	 */
	public String getNaturezaServico() {
		return naturezaServico;
	}
	
	/**
	 * Set: naturezaServico.
	 *
	 * @param naturezaServico the natureza servico
	 */
	public void setNaturezaServico(String naturezaServico) {
		this.naturezaServico = naturezaServico;
	}
	
	/**
	 * Get: servicoComposto.
	 *
	 * @return servicoComposto
	 */
	public String getServicoComposto() {
		return servicoComposto;
	}
	
	/**
	 * Set: servicoComposto.
	 *
	 * @param servicoComposto the servico composto
	 */
	public void setServicoComposto(String servicoComposto) {
		this.servicoComposto = servicoComposto;
	}
}
