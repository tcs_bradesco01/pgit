/*
 * Nome: br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.emissaoautcomp.bean;

/**
 * Nome: EmissaoAutCompHistDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EmissaoAutCompHistDTO {

	/** Atributo dataManutencao. */
	private String dataManutencao;

	/** Atributo responsavel. */
	private String responsavel;

	/** Atributo tipoManutencao. */
	private String tipoManutencao;

	/**
	 * Get: dataManutencao.
	 *
	 * @return dataManutencao
	 */
	public String getDataManutencao() {
		return dataManutencao;
	}

	/**
	 * Set: dataManutencao.
	 *
	 * @param dataManutencao the data manutencao
	 */
	public void setDataManutencao(String dataManutencao) {
		this.dataManutencao = dataManutencao;
	}

	/**
	 * Get: responsavel.
	 *
	 * @return responsavel
	 */
	public String getResponsavel() {
		return responsavel;
	}

	/**
	 * Set: responsavel.
	 *
	 * @param responsavel the responsavel
	 */
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	/**
	 * Get: tipoManutencao.
	 *
	 * @return tipoManutencao
	 */
	public String getTipoManutencao() {
		return tipoManutencao;
	}

	/**
	 * Set: tipoManutencao.
	 *
	 * @param tipoManutencao the tipo manutencao
	 */
	public void setTipoManutencao(String tipoManutencao) {
		this.tipoManutencao = tipoManutencao;
	}

}
