/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.arquivorecebido;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ArquivoRecebidoDTO;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ArquivoRecebidoInconsistentesDTO;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ComboProdutoDTO;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.EstatisticaLoteDTO;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.LoteDTO;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.OcorrenciaDTO;
import br.com.bradesco.web.pgit.service.business.cmpi0000.bean.ListarClientesDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ArquivoRemessaRecebido
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IArquivoRecebidoService {

   
    
     /**
      * Lista grid.
      *
      * @param pArquivoRemessaRecebidoDTO the p arquivo remessa recebido dto
      * @return the list< arquivo recebido dt o>
      */
     List<ArquivoRecebidoDTO> listaGrid(ArquivoRecebidoDTO pArquivoRemessaRecebidoDTO);
    
     /**
      * Lista ocorrencia.
      *
      * @param pOcorrenciaDTO the p ocorrencia dto
      * @return the list< ocorrencia dt o>
      */
     List<OcorrenciaDTO> listaOcorrencia(OcorrenciaDTO pOcorrenciaDTO);
    
     /**
      * Lista lote.
      *
      * @param pLoteDTO the p lote dto
      * @return the list< lote dt o>
      */
     List<LoteDTO> listaLote(LoteDTO pLoteDTO);
    
     /**
      * Lista combo produto.
      *
      * @return the list< combo produto dt o>
      */
     List<ComboProdutoDTO> listaComboProduto();
    
     /**
      * Lista grid estatisticas lote.
      *
      * @param pEstatisticaLoteDTO the p estatistica lote dto
      * @return the list< estatistica lote dt o>
      */
     List<EstatisticaLoteDTO> listaGridEstatisticasLote(EstatisticaLoteDTO pEstatisticaLoteDTO) ;
    
     /**
      * Lista grid incosistentes.
      *
      * @param pArquivoRemessaRecebidoInconsistentesDTO the p arquivo remessa recebido inconsistentes dto
      * @return the list< arquivo recebido inconsistentes dt o>
      */
     List<ArquivoRecebidoInconsistentesDTO> listaGridIncosistentes(ArquivoRecebidoInconsistentesDTO pArquivoRemessaRecebidoInconsistentesDTO);
    
     /**
      * Lista grid clientes.
      *
      * @param pListarClientesDTO the p listar clientes dto
      * @return the list< listar clientes dt o>
      */
     List<ListarClientesDTO> listaGridClientes(ListarClientesDTO pListarClientesDTO);

}