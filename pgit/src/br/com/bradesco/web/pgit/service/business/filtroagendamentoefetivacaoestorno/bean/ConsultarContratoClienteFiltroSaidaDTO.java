/*
 * Nome: br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean;

import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ConsultarContratoClienteFiltroSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarContratoClienteFiltroSaidaDTO{
    
    /** Atributo codMensagem. */
    private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdCorpoCnpjRepresentante. */
    private Long cdCorpoCnpjRepresentante; 
    
    /** Atributo cdFilialCnpjRepresentante. */
    private Integer cdFilialCnpjRepresentante; 
    
    /** Atributo cdControleCnpjRepresentante. */
    private Integer cdControleCnpjRepresentante; 
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;  
    
    /** Atributo dsPessoaJuridica. */
    private String dsPessoaJuridica; 
    
    /** Atributo cdTipoContrato. */
    private Integer cdTipoContrato; 
    
    /** Atributo dsTipoContrato. */
    private String dsTipoContrato; 
    
    /** Atributo nrSequenciaContrato. */
    private Long nrSequenciaContrato; 
    
    /** Atributo dsContrato. */
    private String dsContrato; 
    
    /** Atributo cdSituacaoContrato. */
    private Integer cdSituacaoContrato; 
    
    /** Atributo dsSituacaoContrato. */
    private String dsSituacaoContrato; 
    
    /** Atributo cdTipoParticipacaoPessoa. */
    private Integer cdTipoParticipacaoPessoa;
    
    /** Atributo dsTipoParticipacaoPessoa. */
    private String dsTipoParticipacaoPessoa;
    
    /** Atributo cdPessoaParticipante. */
    private Long cdPessoaParticipante;
    
    /** Atributo dsPessoaParticipante. */
    private String dsPessoaParticipante;
    
    /**
     * Get: descricaoPessoaJuridicaFormatada.
     *
     * @return descricaoPessoaJuridicaFormatada
     */
    public String getDescricaoPessoaJuridicaFormatada(){
    	StringBuilder descricaoFormatada = new StringBuilder();
    	descricaoFormatada.append(cdPessoaJuridica);
    	descricaoFormatada.append(" - ");
    	descricaoFormatada.append(dsPessoaJuridica);
    	
    	return descricaoFormatada.toString();
    }
    
    /**
     * Get: cpfCnpjFormatado.
     *
     * @return cpfCnpjFormatado
     */
    public String getCpfCnpjFormatado(){
    	return PgitUtil.aplicaMascara(cdCorpoCnpjRepresentante.toString(), cdFilialCnpjRepresentante.toString(), cdControleCnpjRepresentante.toString());
    }
    
    /**
     * Get: cdControleCnpjRepresentante.
     *
     * @return cdControleCnpjRepresentante
     */
    public Integer getCdControleCnpjRepresentante() {
		return cdControleCnpjRepresentante;
	}
	
	/**
	 * Set: cdControleCnpjRepresentante.
	 *
	 * @param cdControleCnpjRepresentante the cd controle cnpj representante
	 */
	public void setCdControleCnpjRepresentante(Integer cdControleCnpjRepresentante) {
		this.cdControleCnpjRepresentante = cdControleCnpjRepresentante;
	}
	
	/**
	 * Get: cdCorpoCnpjRepresentante.
	 *
	 * @return cdCorpoCnpjRepresentante
	 */
	public Long getCdCorpoCnpjRepresentante() {
		return cdCorpoCnpjRepresentante;
	}
	
	/**
	 * Set: cdCorpoCnpjRepresentante.
	 *
	 * @param cdCorpoCnpjRepresentante the cd corpo cnpj representante
	 */
	public void setCdCorpoCnpjRepresentante(Long cdCorpoCnpjRepresentante) {
		this.cdCorpoCnpjRepresentante = cdCorpoCnpjRepresentante;
	}
	
	/**
	 * Get: cdFilialCnpjRepresentante.
	 *
	 * @return cdFilialCnpjRepresentante
	 */
	public Integer getCdFilialCnpjRepresentante() {
		return cdFilialCnpjRepresentante;
	}
	
	/**
	 * Set: cdFilialCnpjRepresentante.
	 *
	 * @param cdFilialCnpjRepresentante the cd filial cnpj representante
	 */
	public void setCdFilialCnpjRepresentante(Integer cdFilialCnpjRepresentante) {
		this.cdFilialCnpjRepresentante = cdFilialCnpjRepresentante;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdPessoaParticipante.
	 *
	 * @return cdPessoaParticipante
	 */
	public Long getCdPessoaParticipante() {
		return cdPessoaParticipante;
	}
	
	/**
	 * Set: cdPessoaParticipante.
	 *
	 * @param cdPessoaParticipante the cd pessoa participante
	 */
	public void setCdPessoaParticipante(Long cdPessoaParticipante) {
		this.cdPessoaParticipante = cdPessoaParticipante;
	}
	
	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public Integer getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}
	
	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(Integer cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsPessoaJuridica.
	 *
	 * @return dsPessoaJuridica
	 */
	public String getDsPessoaJuridica() {
		return dsPessoaJuridica;
	}
	
	/**
	 * Set: dsPessoaJuridica.
	 *
	 * @param dsPessoaJuridica the ds pessoa juridica
	 */
	public void setDsPessoaJuridica(String dsPessoaJuridica) {
		this.dsPessoaJuridica = dsPessoaJuridica;
	}
	
	/**
	 * Get: dsPessoaParticipante.
	 *
	 * @return dsPessoaParticipante
	 */
	public String getDsPessoaParticipante() {
		return dsPessoaParticipante;
	}
	
	/**
	 * Set: dsPessoaParticipante.
	 *
	 * @param dsPessoaParticipante the ds pessoa participante
	 */
	public void setDsPessoaParticipante(String dsPessoaParticipante) {
		this.dsPessoaParticipante = dsPessoaParticipante;
	}
	
	/**
	 * Get: dsSituacaoContrato.
	 *
	 * @return dsSituacaoContrato
	 */
	public String getDsSituacaoContrato() {
		return dsSituacaoContrato;
	}
	
	/**
	 * Set: dsSituacaoContrato.
	 *
	 * @param dsSituacaoContrato the ds situacao contrato
	 */
	public void setDsSituacaoContrato(String dsSituacaoContrato) {
		this.dsSituacaoContrato = dsSituacaoContrato;
	}
	
	/**
	 * Get: dsTipoContrato.
	 *
	 * @return dsTipoContrato
	 */
	public String getDsTipoContrato() {
		return dsTipoContrato;
	}
	
	/**
	 * Set: dsTipoContrato.
	 *
	 * @param dsTipoContrato the ds tipo contrato
	 */
	public void setDsTipoContrato(String dsTipoContrato) {
		this.dsTipoContrato = dsTipoContrato;
	}
	
	/**
	 * Get: dsTipoParticipacaoPessoa.
	 *
	 * @return dsTipoParticipacaoPessoa
	 */
	public String getDsTipoParticipacaoPessoa() {
		return dsTipoParticipacaoPessoa;
	}
	
	/**
	 * Set: dsTipoParticipacaoPessoa.
	 *
	 * @param dsTipoParticipacaoPessoa the ds tipo participacao pessoa
	 */
	public void setDsTipoParticipacaoPessoa(String dsTipoParticipacaoPessoa) {
		this.dsTipoParticipacaoPessoa = dsTipoParticipacaoPessoa;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
    
}
