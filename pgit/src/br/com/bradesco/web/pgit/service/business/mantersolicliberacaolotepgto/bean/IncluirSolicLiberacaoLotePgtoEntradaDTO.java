package br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.bean;

import java.math.BigDecimal;

public class IncluirSolicLiberacaoLotePgtoEntradaDTO {

	private Long cdpessoaJuridicaContrato;
	private Integer cdTipoContratoNegocio;
	private Long nrSequenciaContratoNegocio;
	private Long nrLoteInterno;
	private Integer cdTipoLayoutArquivo;
	private String dtPgtoInicial;
	private String dtPgtoFinal;
	private Integer cdProdutoServicoOperacao;
	private Integer cdProdutoServicoRelacionado;
	private Integer cdBancoDebito;
	private Integer cdAgenciaDebito;
	private Long cdContaDebito;
	private Long qtdeTotalPagtoPrevistoSoltc;
	private BigDecimal vlrTotPagtoPrevistoSolicitacao;
	private Integer cdTipoInscricaoPagador;
	private Long cdCpfCnpjPagador;
	private Integer cdFilialCpfCnpjPagador;
	private Integer cdControleCpfCnpjPagador;
	private Integer cdTipoCtaRecebedor;
	private Integer cdBcoRecebedor;
	private Integer cdAgenciaRecebedor;
	private Long cdContaRecebedor;
	private Integer cdTipoInscricaoRecebedor;
	private Long cdCpfCnpjRecebedor;
	private Integer cdFilialCnpjRecebedor;
	private Integer cdControleCpfRecebedor;

	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	public Long getNrLoteInterno() {
		return nrLoteInterno;
	}

	public void setNrLoteInterno(Long nrLoteInterno) {
		this.nrLoteInterno = nrLoteInterno;
	}

	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	public String getDtPgtoInicial() {
		return dtPgtoInicial;
	}

	public void setDtPgtoInicial(String dtPgtoInicial) {
		this.dtPgtoInicial = dtPgtoInicial;
	}

	public String getDtPgtoFinal() {
		return dtPgtoFinal;
	}

	public void setDtPgtoFinal(String dtPgtoFinal) {
		this.dtPgtoFinal = dtPgtoFinal;
	}

	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}

	public void setCdProdutoServicoRelacionado(
			Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}

	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}

	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}

	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	public Long getCdContaDebito() {
		return cdContaDebito;
	}

	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	public Long getQtdeTotalPagtoPrevistoSoltc() {
		return qtdeTotalPagtoPrevistoSoltc;
	}

	public void setQtdeTotalPagtoPrevistoSoltc(Long qtdeTotalPagtoPrevistoSoltc) {
		this.qtdeTotalPagtoPrevistoSoltc = qtdeTotalPagtoPrevistoSoltc;
	}

	public BigDecimal getVlrTotPagtoPrevistoSolicitacao() {
		return vlrTotPagtoPrevistoSolicitacao;
	}

	public void setVlrTotPagtoPrevistoSolicitacao(
			BigDecimal vlrTotPagtoPrevistoSolicitacao) {
		this.vlrTotPagtoPrevistoSolicitacao = vlrTotPagtoPrevistoSolicitacao;
	}

	public Integer getCdTipoInscricaoPagador() {
		return cdTipoInscricaoPagador;
	}

	public void setCdTipoInscricaoPagador(Integer cdTipoInscricaoPagador) {
		this.cdTipoInscricaoPagador = cdTipoInscricaoPagador;
	}

	public Long getCdCpfCnpjPagador() {
		return cdCpfCnpjPagador;
	}

	public void setCdCpfCnpjPagador(Long cdCpfCnpjPagador) {
		this.cdCpfCnpjPagador = cdCpfCnpjPagador;
	}

	public Integer getCdFilialCpfCnpjPagador() {
		return cdFilialCpfCnpjPagador;
	}

	public void setCdFilialCpfCnpjPagador(Integer cdFilialCpfCnpjPagador) {
		this.cdFilialCpfCnpjPagador = cdFilialCpfCnpjPagador;
	}

	public Integer getCdControleCpfCnpjPagador() {
		return cdControleCpfCnpjPagador;
	}

	public void setCdControleCpfCnpjPagador(Integer cdControleCpfCnpjPagador) {
		this.cdControleCpfCnpjPagador = cdControleCpfCnpjPagador;
	}

	public Integer getCdTipoCtaRecebedor() {
		return cdTipoCtaRecebedor;
	}

	public void setCdTipoCtaRecebedor(Integer cdTipoCtaRecebedor) {
		this.cdTipoCtaRecebedor = cdTipoCtaRecebedor;
	}

	public Integer getCdBcoRecebedor() {
		return cdBcoRecebedor;
	}

	public void setCdBcoRecebedor(Integer cdBcoRecebedor) {
		this.cdBcoRecebedor = cdBcoRecebedor;
	}

	public Integer getCdAgenciaRecebedor() {
		return cdAgenciaRecebedor;
	}

	public void setCdAgenciaRecebedor(Integer cdAgenciaRecebedor) {
		this.cdAgenciaRecebedor = cdAgenciaRecebedor;
	}

	public Long getCdContaRecebedor() {
		return cdContaRecebedor;
	}

	public void setCdContaRecebedor(Long cdContaRecebedor) {
		this.cdContaRecebedor = cdContaRecebedor;
	}

	public Integer getCdTipoInscricaoRecebedor() {
		return cdTipoInscricaoRecebedor;
	}

	public void setCdTipoInscricaoRecebedor(Integer cdTipoInscricaoRecebedor) {
		this.cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
	}

	public Long getCdCpfCnpjRecebedor() {
		return cdCpfCnpjRecebedor;
	}

	public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor) {
		this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
	}

	public Integer getCdFilialCnpjRecebedor() {
		return cdFilialCnpjRecebedor;
	}

	public void setCdFilialCnpjRecebedor(Integer cdFilialCnpjRecebedor) {
		this.cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
	}

	public Integer getCdControleCpfRecebedor() {
		return cdControleCpfRecebedor;
	}

	public void setCdControleCpfRecebedor(Integer cdControleCpfRecebedor) {
		this.cdControleCpfRecebedor = cdControleCpfRecebedor;
	}

}
