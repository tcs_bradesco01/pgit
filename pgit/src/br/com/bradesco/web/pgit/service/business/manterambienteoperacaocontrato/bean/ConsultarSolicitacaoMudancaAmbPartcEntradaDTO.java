/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

/**
 * Nome: ConsultarSolicitacaoMudancaAmbPartcEntradaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ConsultarSolicitacaoMudancaAmbPartcEntradaDTO {

	/** Atributo nrOcorrencias. */
	private Integer nrOcorrencias;

	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;

	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;

	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;

	/** Atributo cdPessoa. */
	private Long cdPessoa;

	/** Atributo dtInicioSolicitacao. */
	private String dtInicioSolicitacao;

	/** Atributo dtFimSolicitacao. */
	private String dtFimSolicitacao;

	private String dsAmbiente;

	/**
	 * Get: nrOcorrencias.
	 * 
	 * @return nrOcorrencias
	 */
	public Integer getNrOcorrencias() {
		return nrOcorrencias;
	}

	/**
	 * Set: nrOcorrencias.
	 * 
	 * @param nrOcorrencias
	 *            the nr ocorrencias
	 */
	public void setNrOcorrencias(Integer nrOcorrencias) {
		this.nrOcorrencias = nrOcorrencias;
	}

	/**
	 * Get: cdpessoaJuridicaContrato.
	 * 
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}

	/**
	 * Set: cdpessoaJuridicaContrato.
	 * 
	 * @param cdpessoaJuridicaContrato
	 *            the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 * 
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 * 
	 * @param cdTipoContratoNegocio
	 *            the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 * 
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 * 
	 * @param nrSequenciaContratoNegocio
	 *            the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 * 
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 * 
	 * @param cdTipoParticipacaoPessoa
	 *            the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: cdPessoa.
	 * 
	 * @return cdPessoa
	 */
	public Long getCdPessoa() {
		return cdPessoa;
	}

	/**
	 * Set: cdPessoa.
	 * 
	 * @param cdPessoa
	 *            the cd pessoa
	 */
	public void setCdPessoa(Long cdPessoa) {
		this.cdPessoa = cdPessoa;
	}

	/**
	 * Get: dtInicioSolicitacao.
	 * 
	 * @return dtInicioSolicitacao
	 */
	public String getDtInicioSolicitacao() {
		return dtInicioSolicitacao;
	}

	/**
	 * Set: dtInicioSolicitacao.
	 * 
	 * @param dtInicioSolicitacao
	 *            the dt inicio solicitacao
	 */
	public void setDtInicioSolicitacao(String dtInicioSolicitacao) {
		this.dtInicioSolicitacao = dtInicioSolicitacao;
	}

	/**
	 * Get: dtFimSolicitacao.
	 * 
	 * @return dtFimSolicitacao
	 */
	public String getDtFimSolicitacao() {
		return dtFimSolicitacao;
	}

	/**
	 * Set: dtFimSolicitacao.
	 * 
	 * @param dtFimSolicitacao
	 *            the dt fim solicitacao
	 */
	public void setDtFimSolicitacao(String dtFimSolicitacao) {
		this.dtFimSolicitacao = dtFimSolicitacao;
	}

	public String getDsAmbiente() {
		return dsAmbiente;
	}

	public void setDsAmbiente(String dsAmbiente) {
		this.dsAmbiente = dsAmbiente;
	}

}
