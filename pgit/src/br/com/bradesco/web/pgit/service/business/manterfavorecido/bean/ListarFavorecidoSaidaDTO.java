/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterfavorecido.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterfavorecido.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarfavorecidos.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ListarFavorecidoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarFavorecidoSaidaDTO {
	
	/** Atributo cdFavorecidoClientePagador. */
	private Long cdFavorecidoClientePagador;
    
    /** Atributo nmFavorecido. */
    private String nmFavorecido;
    
    /** Atributo cdTipoFavorecido. */
    private Integer cdTipoFavorecido;
    
    /** Atributo dsTipoFavorecido. */
    private String dsTipoFavorecido;
    
    /** Atributo cdInscricaoFavorecidoCliente. */
    private Long cdInscricaoFavorecidoCliente;
    
    /** Atributo dsInscricaoFavorecidoCliente. */
    private String dsInscricaoFavorecidoCliente;
    
    /** Atributo cdTipoInscricaoFavorecido. */
    private Integer cdTipoInscricaoFavorecido;
    
    /** Atributo dsTipoInscricaoFavorecido. */
    private String dsTipoInscricaoFavorecido;
    
    /** Atributo stFavorecido. */
    private Integer stFavorecido;
    
    /** Atributo favorecido. */
    private String favorecido;
    
    /**
     * Listar favorecido saida dto.
     */
    public ListarFavorecidoSaidaDTO() {
	
	}
    
    /**
     * Listar favorecido saida dto.
     *
     * @param saida the saida
     */
    public ListarFavorecidoSaidaDTO(Ocorrencias saida) {
    	this.cdFavorecidoClientePagador = saida.getCdFavorecidoClientePagador();
        this.nmFavorecido = saida.getNmFavorecido();
        this.cdTipoFavorecido = saida.getCdTipoFavorecido();
        this.dsTipoFavorecido = saida.getDsTipoFavorecido();
        this.cdInscricaoFavorecidoCliente = saida.getCdInscricaoFavorecidoCliente();
        this.cdTipoInscricaoFavorecido = saida.getCdTipoInscricaoFavorecido();
        this.dsTipoInscricaoFavorecido = saida.getDsTipoInscricaoFavorecido();
        this.stFavorecido = Integer.parseInt(saida.getStFavorecido());
        this.dsInscricaoFavorecidoCliente = CpfCnpjUtils.formatCpfCnpj(this.cdInscricaoFavorecidoCliente, this.cdTipoInscricaoFavorecido);
        this.favorecido = PgitUtil.concatenarCampos(saida.getCdFavorecidoClientePagador(), saida.getNmFavorecido());
	}

	/**
	 * Get: cdFavorecidoClientePagador.
	 *
	 * @return cdFavorecidoClientePagador
	 */
	public Long getCdFavorecidoClientePagador() {	    	
		return cdFavorecidoClientePagador;
	}

	/**
	 * Set: cdFavorecidoClientePagador.
	 *
	 * @param cdFavorecidoClientePagador the cd favorecido cliente pagador
	 */
	public void setCdFavorecidoClientePagador(Long cdFavorecidoClientePagador) {	    	 
		this.cdFavorecidoClientePagador = cdFavorecidoClientePagador;
	}

	/**
	 * Get: cdInscricaoFavorecidoCliente.
	 *
	 * @return cdInscricaoFavorecidoCliente
	 */
	public Long getCdInscricaoFavorecidoCliente() {
		return cdInscricaoFavorecidoCliente;
	}

	/**
	 * Set: cdInscricaoFavorecidoCliente.
	 *
	 * @param cdInscricaoFavorecidoCliente the cd inscricao favorecido cliente
	 */
	public void setCdInscricaoFavorecidoCliente(Long cdInscricaoFavorecidoCliente) {
		this.cdInscricaoFavorecidoCliente = cdInscricaoFavorecidoCliente;
	}

	/**
	 * Get: cdTipoFavorecido.
	 *
	 * @return cdTipoFavorecido
	 */
	public Integer getCdTipoFavorecido() {
		return cdTipoFavorecido;
	}

	/**
	 * Set: cdTipoFavorecido.
	 *
	 * @param cdTipoFavorecido the cd tipo favorecido
	 */
	public void setCdTipoFavorecido(Integer cdTipoFavorecido) {
		this.cdTipoFavorecido = cdTipoFavorecido;
	}

	/**
	 * Get: cdTipoInscricaoFavorecido.
	 *
	 * @return cdTipoInscricaoFavorecido
	 */
	public Integer getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}

	/**
	 * Set: cdTipoInscricaoFavorecido.
	 *
	 * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(Integer cdTipoInscricaoFavorecido) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
	}

	/**
	 * Get: dsTipoFavorecido.
	 *
	 * @return dsTipoFavorecido
	 */
	public String getDsTipoFavorecido() {
		return dsTipoFavorecido;
	}

	/**
	 * Set: dsTipoFavorecido.
	 *
	 * @param dsTipoFavorecido the ds tipo favorecido
	 */
	public void setDsTipoFavorecido(String dsTipoFavorecido) {
		this.dsTipoFavorecido = dsTipoFavorecido;
	}

	/**
	 * Get: dsTipoInscricaoFavorecido.
	 *
	 * @return dsTipoInscricaoFavorecido
	 */
	public String getDsTipoInscricaoFavorecido() {
		return dsTipoInscricaoFavorecido;
	}

	/**
	 * Set: dsTipoInscricaoFavorecido.
	 *
	 * @param dsTipoInscricaoFavorecido the ds tipo inscricao favorecido
	 */
	public void setDsTipoInscricaoFavorecido(String dsTipoInscricaoFavorecido) {
		this.dsTipoInscricaoFavorecido = dsTipoInscricaoFavorecido;
	}

	/**
	 * Get: nmFavorecido.
	 *
	 * @return nmFavorecido
	 */
	public String getNmFavorecido() {
		return nmFavorecido;
	}

	/**
	 * Set: nmFavorecido.
	 *
	 * @param nmFavorecido the nm favorecido
	 */
	public void setNmFavorecido(String nmFavorecido) {
		this.nmFavorecido = nmFavorecido;
	}

	/**
	 * Get: stFavorecido.
	 *
	 * @return stFavorecido
	 */
	public Integer getStFavorecido() {
		return stFavorecido;
	}

	/**
	 * Set: stFavorecido.
	 *
	 * @param stFavorecido the st favorecido
	 */
	public void setStFavorecido(Integer stFavorecido) {
		this.stFavorecido = stFavorecido;
	}
	
	/**
	 * Get: dsInscricaoFavorecidoCliente.
	 *
	 * @return dsInscricaoFavorecidoCliente
	 */
	public String getDsInscricaoFavorecidoCliente() {
		return dsInscricaoFavorecidoCliente;
	}

	/**
	 * Set: dsInscricaoFavorecidoCliente.
	 *
	 * @param dsInscricaoFavorecidoCliente the ds inscricao favorecido cliente
	 */
	public void setDsInscricaoFavorecidoCliente(String dsInscricaoFavorecidoCliente) {
		this.dsInscricaoFavorecidoCliente = dsInscricaoFavorecidoCliente;
	}

	/**
	 * Get: dsSituacaoFavorecido.
	 *
	 * @return dsSituacaoFavorecido
	 */
	public String getDsSituacaoFavorecido(){
		
		String dsSituacaoParticipacaoContrato;
		
		switch (this.stFavorecido) {
		case 1:
			dsSituacaoParticipacaoContrato = "ATIVO";
			break;
		case 2:
			dsSituacaoParticipacaoContrato = "BLOQUEADO";
			break;
		case 3:
			dsSituacaoParticipacaoContrato = "INATIVO";
			break;
		default:
			dsSituacaoParticipacaoContrato = "EXCLUIDO";
			break;
		}
		
		return dsSituacaoParticipacaoContrato;
	}

	/**
	 * Get: dsFavorecidoClientePagador.
	 *
	 * @return dsFavorecidoClientePagador
	 */
	public String getDsFavorecidoClientePagador() {
	    if (getCdFavorecidoClientePagador().equals(0l)){
		return "";
	    }
	    return getCdFavorecidoClientePagador().toString();
	}

	/**
	 * Get: favorecido.
	 *
	 * @return favorecido
	 */
	public String getFavorecido() {
	    return favorecido;
	}

	/**
	 * Set: favorecido.
	 *
	 * @param favorecido the favorecido
	 */
	public void setFavorecido(String favorecido) {
	    this.favorecido = favorecido;
	}

	
	
	
	
}
