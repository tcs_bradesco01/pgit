/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean;

/**
 * Nome: DetalharServicoRelacionadoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class DetalharServicoRelacionadoSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo codTipoServico. */
	private int codTipoServico;

	/** Atributo dsTipoServico. */
	private String dsTipoServico;

	/** Atributo codModalidadeServico. */
	private int codModalidadeServico;

	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;

	/** Atributo codTipoRelacionamento. */
	private int codTipoRelacionamento;

	/** Atributo codServicoComposto. */
	private long codServicoComposto;

	/** Atributo dsServicoComposto. */
	private String dsServicoComposto;

	/** Atributo codNaturezaServico. */
	private int codNaturezaServico;

	/** Atributo codTipoCanalInclusao. */
	private int codTipoCanalInclusao;

	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;

	/** Atributo codUsuarioInclusao. */
	private String codUsuarioInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo dataHraInclusao. */
	private String dataHraInclusao;

	/** Atributo codTipoCanalManutencao. */
	private int codTipoCanalManutencao;

	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;

	/** Atributo codUsuarioManutencao. */
	private String codUsuarioManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHraManutencao. */
	private String dataHraManutencao;

	private Integer nrOrdenacaoServicoRelacionado;
	private String cdIndicadorServicoNegociavel;
	private String cdIndicadorNegociavelServico;
	private String cdIndicadorContigenciaServico;
	private String cdIndicadorManutencaoServico;

	public String getCodMensagem() {
		return codMensagem;
	}

	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public int getCodTipoServico() {
		return codTipoServico;
	}

	public void setCodTipoServico(int codTipoServico) {
		this.codTipoServico = codTipoServico;
	}

	public String getDsTipoServico() {
		return dsTipoServico;
	}

	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	public int getCodModalidadeServico() {
		return codModalidadeServico;
	}

	public void setCodModalidadeServico(int codModalidadeServico) {
		this.codModalidadeServico = codModalidadeServico;
	}

	public String getDsModalidadeServico() {
		return dsModalidadeServico;
	}

	public void setDsModalidadeServico(String dsModalidadeServico) {
		this.dsModalidadeServico = dsModalidadeServico;
	}

	public int getCodTipoRelacionamento() {
		return codTipoRelacionamento;
	}

	public void setCodTipoRelacionamento(int codTipoRelacionamento) {
		this.codTipoRelacionamento = codTipoRelacionamento;
	}

	public long getCodServicoComposto() {
		return codServicoComposto;
	}

	public void setCodServicoComposto(long codServicoComposto) {
		this.codServicoComposto = codServicoComposto;
	}

	public String getDsServicoComposto() {
		return dsServicoComposto;
	}

	public void setDsServicoComposto(String dsServicoComposto) {
		this.dsServicoComposto = dsServicoComposto;
	}

	public int getCodNaturezaServico() {
		return codNaturezaServico;
	}

	public void setCodNaturezaServico(int codNaturezaServico) {
		this.codNaturezaServico = codNaturezaServico;
	}

	public int getCodTipoCanalInclusao() {
		return codTipoCanalInclusao;
	}

	public void setCodTipoCanalInclusao(int codTipoCanalInclusao) {
		this.codTipoCanalInclusao = codTipoCanalInclusao;
	}

	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}

	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	public String getCodUsuarioInclusao() {
		return codUsuarioInclusao;
	}

	public void setCodUsuarioInclusao(String codUsuarioInclusao) {
		this.codUsuarioInclusao = codUsuarioInclusao;
	}

	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	public String getDataHraInclusao() {
		return dataHraInclusao;
	}

	public void setDataHraInclusao(String dataHraInclusao) {
		this.dataHraInclusao = dataHraInclusao;
	}

	public int getCodTipoCanalManutencao() {
		return codTipoCanalManutencao;
	}

	public void setCodTipoCanalManutencao(int codTipoCanalManutencao) {
		this.codTipoCanalManutencao = codTipoCanalManutencao;
	}

	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}

	public void setDsTipoCanalManutencao(String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}

	public String getCodUsuarioManutencao() {
		return codUsuarioManutencao;
	}

	public void setCodUsuarioManutencao(String codUsuarioManutencao) {
		this.codUsuarioManutencao = codUsuarioManutencao;
	}

	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	public String getDataHraManutencao() {
		return dataHraManutencao;
	}

	public void setDataHraManutencao(String dataHraManutencao) {
		this.dataHraManutencao = dataHraManutencao;
	}

	public Integer getNrOrdenacaoServicoRelacionado() {
		return nrOrdenacaoServicoRelacionado;
	}

	public void setNrOrdenacaoServicoRelacionado(
			Integer nrOrdenacaoServicoRelacionado) {
		this.nrOrdenacaoServicoRelacionado = nrOrdenacaoServicoRelacionado;
	}

	public String getCdIndicadorServicoNegociavel() {
		return cdIndicadorServicoNegociavel;
	}

	public void setCdIndicadorServicoNegociavel(
			String cdIndicadorServicoNegociavel) {
		this.cdIndicadorServicoNegociavel = cdIndicadorServicoNegociavel;
	}

	public String getCdIndicadorNegociavelServico() {
		return cdIndicadorNegociavelServico;
	}

	public void setCdIndicadorNegociavelServico(
			String cdIndicadorNegociavelServico) {
		this.cdIndicadorNegociavelServico = cdIndicadorNegociavelServico;
	}

	public String getCdIndicadorContigenciaServico() {
		return cdIndicadorContigenciaServico;
	}

	public void setCdIndicadorContigenciaServico(
			String cdIndicadorContigenciaServico) {
		this.cdIndicadorContigenciaServico = cdIndicadorContigenciaServico;
	}

	public String getCdIndicadorManutencaoServico() {
		return cdIndicadorManutencaoServico;
	}

	public void setCdIndicadorManutencaoServico(
			String cdIndicadorManutencaoServico) {
		this.cdIndicadorManutencaoServico = cdIndicadorManutencaoServico;
	}

}
