/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias8.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias8 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdComprovanteTipoComprovante
     */
    private java.lang.String _cdComprovanteTipoComprovante;

    /**
     * Field _dsComprovantePeriodicidadeEmissao
     */
    private java.lang.String _dsComprovantePeriodicidadeEmissao;

    /**
     * Field _dsComprovanteDestino
     */
    private java.lang.String _dsComprovanteDestino;

    /**
     * Field _dsComprovantePermissaoCorrespondenteAbertura
     */
    private java.lang.String _dsComprovantePermissaoCorrespondenteAbertura;

    /**
     * Field _dsDemonstrativoInformacaoReservada
     */
    private java.lang.String _dsDemonstrativoInformacaoReservada;

    /**
     * Field _qtViasEmitir
     */
    private int _qtViasEmitir = 0;

    /**
     * keeps track of state for field: _qtViasEmitir
     */
    private boolean _has_qtViasEmitir;

    /**
     * Field _dsAgrupamentoCorrespondente
     */
    private java.lang.String _dsAgrupamentoCorrespondente;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias8() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteQtViasEmitir
     * 
     */
    public void deleteQtViasEmitir()
    {
        this._has_qtViasEmitir= false;
    } //-- void deleteQtViasEmitir() 

    /**
     * Returns the value of field 'cdComprovanteTipoComprovante'.
     * 
     * @return String
     * @return the value of field 'cdComprovanteTipoComprovante'.
     */
    public java.lang.String getCdComprovanteTipoComprovante()
    {
        return this._cdComprovanteTipoComprovante;
    } //-- java.lang.String getCdComprovanteTipoComprovante() 

    /**
     * Returns the value of field 'dsAgrupamentoCorrespondente'.
     * 
     * @return String
     * @return the value of field 'dsAgrupamentoCorrespondente'.
     */
    public java.lang.String getDsAgrupamentoCorrespondente()
    {
        return this._dsAgrupamentoCorrespondente;
    } //-- java.lang.String getDsAgrupamentoCorrespondente() 

    /**
     * Returns the value of field 'dsComprovanteDestino'.
     * 
     * @return String
     * @return the value of field 'dsComprovanteDestino'.
     */
    public java.lang.String getDsComprovanteDestino()
    {
        return this._dsComprovanteDestino;
    } //-- java.lang.String getDsComprovanteDestino() 

    /**
     * Returns the value of field
     * 'dsComprovantePeriodicidadeEmissao'.
     * 
     * @return String
     * @return the value of field
     * 'dsComprovantePeriodicidadeEmissao'.
     */
    public java.lang.String getDsComprovantePeriodicidadeEmissao()
    {
        return this._dsComprovantePeriodicidadeEmissao;
    } //-- java.lang.String getDsComprovantePeriodicidadeEmissao() 

    /**
     * Returns the value of field
     * 'dsComprovantePermissaoCorrespondenteAbertura'.
     * 
     * @return String
     * @return the value of field
     * 'dsComprovantePermissaoCorrespondenteAbertura'.
     */
    public java.lang.String getDsComprovantePermissaoCorrespondenteAbertura()
    {
        return this._dsComprovantePermissaoCorrespondenteAbertura;
    } //-- java.lang.String getDsComprovantePermissaoCorrespondenteAbertura() 

    /**
     * Returns the value of field
     * 'dsDemonstrativoInformacaoReservada'.
     * 
     * @return String
     * @return the value of field
     * 'dsDemonstrativoInformacaoReservada'.
     */
    public java.lang.String getDsDemonstrativoInformacaoReservada()
    {
        return this._dsDemonstrativoInformacaoReservada;
    } //-- java.lang.String getDsDemonstrativoInformacaoReservada() 

    /**
     * Returns the value of field 'qtViasEmitir'.
     * 
     * @return int
     * @return the value of field 'qtViasEmitir'.
     */
    public int getQtViasEmitir()
    {
        return this._qtViasEmitir;
    } //-- int getQtViasEmitir() 

    /**
     * Method hasQtViasEmitir
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQtViasEmitir()
    {
        return this._has_qtViasEmitir;
    } //-- boolean hasQtViasEmitir() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdComprovanteTipoComprovante'.
     * 
     * @param cdComprovanteTipoComprovante the value of field
     * 'cdComprovanteTipoComprovante'.
     */
    public void setCdComprovanteTipoComprovante(java.lang.String cdComprovanteTipoComprovante)
    {
        this._cdComprovanteTipoComprovante = cdComprovanteTipoComprovante;
    } //-- void setCdComprovanteTipoComprovante(java.lang.String) 

    /**
     * Sets the value of field 'dsAgrupamentoCorrespondente'.
     * 
     * @param dsAgrupamentoCorrespondente the value of field
     * 'dsAgrupamentoCorrespondente'.
     */
    public void setDsAgrupamentoCorrespondente(java.lang.String dsAgrupamentoCorrespondente)
    {
        this._dsAgrupamentoCorrespondente = dsAgrupamentoCorrespondente;
    } //-- void setDsAgrupamentoCorrespondente(java.lang.String) 

    /**
     * Sets the value of field 'dsComprovanteDestino'.
     * 
     * @param dsComprovanteDestino the value of field
     * 'dsComprovanteDestino'.
     */
    public void setDsComprovanteDestino(java.lang.String dsComprovanteDestino)
    {
        this._dsComprovanteDestino = dsComprovanteDestino;
    } //-- void setDsComprovanteDestino(java.lang.String) 

    /**
     * Sets the value of field 'dsComprovantePeriodicidadeEmissao'.
     * 
     * @param dsComprovantePeriodicidadeEmissao the value of field
     * 'dsComprovantePeriodicidadeEmissao'.
     */
    public void setDsComprovantePeriodicidadeEmissao(java.lang.String dsComprovantePeriodicidadeEmissao)
    {
        this._dsComprovantePeriodicidadeEmissao = dsComprovantePeriodicidadeEmissao;
    } //-- void setDsComprovantePeriodicidadeEmissao(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsComprovantePermissaoCorrespondenteAbertura'.
     * 
     * @param dsComprovantePermissaoCorrespondenteAbertura the
     * value of field 'dsComprovantePermissaoCorrespondenteAbertura'
     */
    public void setDsComprovantePermissaoCorrespondenteAbertura(java.lang.String dsComprovantePermissaoCorrespondenteAbertura)
    {
        this._dsComprovantePermissaoCorrespondenteAbertura = dsComprovantePermissaoCorrespondenteAbertura;
    } //-- void setDsComprovantePermissaoCorrespondenteAbertura(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsDemonstrativoInformacaoReservada'.
     * 
     * @param dsDemonstrativoInformacaoReservada the value of field
     * 'dsDemonstrativoInformacaoReservada'.
     */
    public void setDsDemonstrativoInformacaoReservada(java.lang.String dsDemonstrativoInformacaoReservada)
    {
        this._dsDemonstrativoInformacaoReservada = dsDemonstrativoInformacaoReservada;
    } //-- void setDsDemonstrativoInformacaoReservada(java.lang.String) 

    /**
     * Sets the value of field 'qtViasEmitir'.
     * 
     * @param qtViasEmitir the value of field 'qtViasEmitir'.
     */
    public void setQtViasEmitir(int qtViasEmitir)
    {
        this._qtViasEmitir = qtViasEmitir;
        this._has_qtViasEmitir = true;
    } //-- void setQtViasEmitir(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias8
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias8 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
