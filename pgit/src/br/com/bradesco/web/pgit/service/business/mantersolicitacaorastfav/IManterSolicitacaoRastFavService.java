/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterSolicitacaoRastFav
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
import java.util.List;

import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.DetalharSolicitacaoRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.DetalharSolicitacaoRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ExcluirSolicRastFavEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ExcluirSolicRastFavSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.IncluirSolicRastFavEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.IncluirSolicRastFavSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ManterSolicRastFavEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ManterSolicRastFavSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcSaidaDTO;

/**
 * Nome: IManterSolicitacaoRastFavService
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface IManterSolicitacaoRastFavService {

    /**
     * Pesquisar solic rast favorecidos.
     *
     * @param entrada the entrada
     * @return the list< manter solic rast fav saida dt o>
     */
    List<ManterSolicRastFavSaidaDTO> pesquisarSolicRastFavorecidos(ManterSolicRastFavEntradaDTO entrada);
    
    /**
     * Incluir solic rast fav.
     *
     * @param entrada the entrada
     * @return the incluir solic rast fav saida dto
     */
    IncluirSolicRastFavSaidaDTO incluirSolicRastFav(IncluirSolicRastFavEntradaDTO entrada);
    
    /**
     * Excluir solic rast fav.
     *
     * @param entrada the entrada
     * @return the excluir solic rast fav saida dto
     */
    ExcluirSolicRastFavSaidaDTO excluirSolicRastFav (ExcluirSolicRastFavEntradaDTO entrada);
    
    /**
     * Detalhar solic rast fav.
     *
     * @param entrada the entrada
     * @return the detalhar solicitacao rastreamento saida dto
     */
    DetalharSolicitacaoRastreamentoSaidaDTO detalharSolicRastFav (DetalharSolicitacaoRastreamentoEntradaDTO entrada);
    
    /**
     * Consultar descricao banco agencia.
     *
     * @param entradaConsultarBancoAgencia the entrada consultar banco agencia
     * @return the consultar banco agencia saida dto
     */
    ConsultarBancoAgenciaSaidaDTO consultarDescricaoBancoAgencia(ConsultarBancoAgenciaEntradaDTO entradaConsultarBancoAgencia);
    
    /**
     * Verificara atributo soltc.
     *
     * @param entrada the entrada
     * @return the verificara atributo soltc saida dto
     */
    VerificaraAtributoSoltcSaidaDTO verificaraAtributoSoltc(VerificaraAtributoSoltcEntradaDTO entrada);
}

