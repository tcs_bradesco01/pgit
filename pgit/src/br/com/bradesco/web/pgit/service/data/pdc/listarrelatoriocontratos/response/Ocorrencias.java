/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSolicitacaoPagamentoIntegrado
     */
    private int _cdSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _cdSolicitacaoPagamentoIntegrado
     */
    private boolean _has_cdSolicitacaoPagamentoIntegrado;

    /**
     * Field _nrSolicitacaoPagamentoIntegrado
     */
    private int _nrSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _nrSolicitacaoPagamentoIntegrado
     */
    private boolean _has_nrSolicitacaoPagamentoIntegrado;

    /**
     * Field _cdTipoRelatPagamento
     */
    private int _cdTipoRelatPagamento = 0;

    /**
     * keeps track of state for field: _cdTipoRelatPagamento
     */
    private boolean _has_cdTipoRelatPagamento;

    /**
     * Field _hrSolicitacaoPagamentoIntegrado
     */
    private java.lang.String _hrSolicitacaoPagamentoIntegrado;

    /**
     * Field _cdSituacaoSolicitacaoPagamento
     */
    private int _cdSituacaoSolicitacaoPagamento = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoSolicitacaoPagamento
     */
    private boolean _has_cdSituacaoSolicitacaoPagamento;

    /**
     * Field _cdUsuarioInclusao
     */
    private java.lang.String _cdUsuarioInclusao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdSituacaoSolicitacaoPagamento
     * 
     */
    public void deleteCdSituacaoSolicitacaoPagamento()
    {
        this._has_cdSituacaoSolicitacaoPagamento= false;
    } //-- void deleteCdSituacaoSolicitacaoPagamento() 

    /**
     * Method deleteCdSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteCdSolicitacaoPagamentoIntegrado()
    {
        this._has_cdSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteCdTipoRelatPagamento
     * 
     */
    public void deleteCdTipoRelatPagamento()
    {
        this._has_cdTipoRelatPagamento= false;
    } //-- void deleteCdTipoRelatPagamento() 

    /**
     * Method deleteNrSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteNrSolicitacaoPagamentoIntegrado()
    {
        this._has_nrSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteNrSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoSolicitacaoPagamento'.
     */
    public int getCdSituacaoSolicitacaoPagamento()
    {
        return this._cdSituacaoSolicitacaoPagamento;
    } //-- int getCdSituacaoSolicitacaoPagamento() 

    /**
     * Returns the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamentoIntegrado'.
     */
    public int getCdSolicitacaoPagamentoIntegrado()
    {
        return this._cdSolicitacaoPagamentoIntegrado;
    } //-- int getCdSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdTipoRelatPagamento'.
     * 
     * @return int
     * @return the value of field 'cdTipoRelatPagamento'.
     */
    public int getCdTipoRelatPagamento()
    {
        return this._cdTipoRelatPagamento;
    } //-- int getCdTipoRelatPagamento() 

    /**
     * Returns the value of field 'cdUsuarioInclusao'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioInclusao'.
     */
    public java.lang.String getCdUsuarioInclusao()
    {
        return this._cdUsuarioInclusao;
    } //-- java.lang.String getCdUsuarioInclusao() 

    /**
     * Returns the value of field
     * 'hrSolicitacaoPagamentoIntegrado'.
     * 
     * @return String
     * @return the value of field 'hrSolicitacaoPagamentoIntegrado'.
     */
    public java.lang.String getHrSolicitacaoPagamentoIntegrado()
    {
        return this._hrSolicitacaoPagamentoIntegrado;
    } //-- java.lang.String getHrSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'nrSolicitacaoPagamentoIntegrado'.
     */
    public int getNrSolicitacaoPagamentoIntegrado()
    {
        return this._nrSolicitacaoPagamentoIntegrado;
    } //-- int getNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasCdSituacaoSolicitacaoPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoSolicitacaoPagamento()
    {
        return this._has_cdSituacaoSolicitacaoPagamento;
    } //-- boolean hasCdSituacaoSolicitacaoPagamento() 

    /**
     * Method hasCdSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamentoIntegrado()
    {
        return this._has_cdSolicitacaoPagamentoIntegrado;
    } //-- boolean hasCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasCdTipoRelatPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoRelatPagamento()
    {
        return this._has_cdTipoRelatPagamento;
    } //-- boolean hasCdTipoRelatPagamento() 

    /**
     * Method hasNrSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSolicitacaoPagamentoIntegrado()
    {
        return this._has_nrSolicitacaoPagamentoIntegrado;
    } //-- boolean hasNrSolicitacaoPagamentoIntegrado() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdSituacaoSolicitacaoPagamento'.
     * 
     * @param cdSituacaoSolicitacaoPagamento the value of field
     * 'cdSituacaoSolicitacaoPagamento'.
     */
    public void setCdSituacaoSolicitacaoPagamento(int cdSituacaoSolicitacaoPagamento)
    {
        this._cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
        this._has_cdSituacaoSolicitacaoPagamento = true;
    } //-- void setCdSituacaoSolicitacaoPagamento(int) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @param cdSolicitacaoPagamentoIntegrado the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     */
    public void setCdSolicitacaoPagamentoIntegrado(int cdSolicitacaoPagamentoIntegrado)
    {
        this._cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
        this._has_cdSolicitacaoPagamentoIntegrado = true;
    } //-- void setCdSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'cdTipoRelatPagamento'.
     * 
     * @param cdTipoRelatPagamento the value of field
     * 'cdTipoRelatPagamento'.
     */
    public void setCdTipoRelatPagamento(int cdTipoRelatPagamento)
    {
        this._cdTipoRelatPagamento = cdTipoRelatPagamento;
        this._has_cdTipoRelatPagamento = true;
    } //-- void setCdTipoRelatPagamento(int) 

    /**
     * Sets the value of field 'cdUsuarioInclusao'.
     * 
     * @param cdUsuarioInclusao the value of field
     * 'cdUsuarioInclusao'.
     */
    public void setCdUsuarioInclusao(java.lang.String cdUsuarioInclusao)
    {
        this._cdUsuarioInclusao = cdUsuarioInclusao;
    } //-- void setCdUsuarioInclusao(java.lang.String) 

    /**
     * Sets the value of field 'hrSolicitacaoPagamentoIntegrado'.
     * 
     * @param hrSolicitacaoPagamentoIntegrado the value of field
     * 'hrSolicitacaoPagamentoIntegrado'.
     */
    public void setHrSolicitacaoPagamentoIntegrado(java.lang.String hrSolicitacaoPagamentoIntegrado)
    {
        this._hrSolicitacaoPagamentoIntegrado = hrSolicitacaoPagamentoIntegrado;
    } //-- void setHrSolicitacaoPagamentoIntegrado(java.lang.String) 

    /**
     * Sets the value of field 'nrSolicitacaoPagamentoIntegrado'.
     * 
     * @param nrSolicitacaoPagamentoIntegrado the value of field
     * 'nrSolicitacaoPagamentoIntegrado'.
     */
    public void setNrSolicitacaoPagamentoIntegrado(int nrSolicitacaoPagamentoIntegrado)
    {
        this._nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
        this._has_nrSolicitacaoPagamentoIntegrado = true;
    } //-- void setNrSolicitacaoPagamentoIntegrado(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarrelatoriocontratos.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
