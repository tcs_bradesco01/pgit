/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listartipovinculo.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoVinculo
     */
    private int _cdTipoVinculo = 0;

    /**
     * keeps track of state for field: _cdTipoVinculo
     */
    private boolean _has_cdTipoVinculo;

    /**
     * Field _dsTipoVinculo
     */
    private java.lang.String _dsTipoVinculo;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipovinculo.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoVinculo
     * 
     */
    public void deleteCdTipoVinculo()
    {
        this._has_cdTipoVinculo= false;
    } //-- void deleteCdTipoVinculo() 

    /**
     * Returns the value of field 'cdTipoVinculo'.
     * 
     * @return int
     * @return the value of field 'cdTipoVinculo'.
     */
    public int getCdTipoVinculo()
    {
        return this._cdTipoVinculo;
    } //-- int getCdTipoVinculo() 

    /**
     * Returns the value of field 'dsTipoVinculo'.
     * 
     * @return String
     * @return the value of field 'dsTipoVinculo'.
     */
    public java.lang.String getDsTipoVinculo()
    {
        return this._dsTipoVinculo;
    } //-- java.lang.String getDsTipoVinculo() 

    /**
     * Method hasCdTipoVinculo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoVinculo()
    {
        return this._has_cdTipoVinculo;
    } //-- boolean hasCdTipoVinculo() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoVinculo'.
     * 
     * @param cdTipoVinculo the value of field 'cdTipoVinculo'.
     */
    public void setCdTipoVinculo(int cdTipoVinculo)
    {
        this._cdTipoVinculo = cdTipoVinculo;
        this._has_cdTipoVinculo = true;
    } //-- void setCdTipoVinculo(int) 

    /**
     * Sets the value of field 'dsTipoVinculo'.
     * 
     * @param dsTipoVinculo the value of field 'dsTipoVinculo'.
     */
    public void setDsTipoVinculo(java.lang.String dsTipoVinculo)
    {
        this._dsTipoVinculo = dsTipoVinculo;
    } //-- void setDsTipoVinculo(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listartipovinculo.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listartipovinculo.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listartipovinculo.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listartipovinculo.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
