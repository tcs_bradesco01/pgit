/*
 * Nome: br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.bean;



import java.math.BigDecimal;

// TODO: Auto-generated Javadoc
/**
 * Nome: ConsultarAutenticacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarAutenticacaoEntradaDTO {

	/** Atributo cdBanco. */
	private int cdBanco;
	
	/** Atributo cdAgencia. */
	private int cdAgencia;
	
	/** Atributo cdConta. */
	private long cdConta;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo cdModalidade. */
	private int cdModalidade;
	
	/** Atributo cdServico. */
	private int cdServico;
	
	/** Atributo dtPagamento. */
	private String dtPagamento;
	
	/** Atributo valorPagamento. */
	private BigDecimal valorPagamento;
	
	/** The Cd flag modalidade. */
	private Integer CdFlagModalidade;
	
	/** The Cd fl codigo barras. */
	private String CdFlCodigoBarras;
	
	/** The Nr pagamento. */
	private String NrPagamento;
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public int getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(int cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public int getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(int cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(long cdConta) {
		this.cdConta = cdConta;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public int getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(int cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
	
	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public int getCdServico() {
		return cdServico;
	}
	
	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(int cdServico) {
		this.cdServico = cdServico;
	}
	
	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}
	
	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
	
	/**
	 * Get: valorPagamento.
	 *
	 * @return valorPagamento
	 */
	public BigDecimal getValorPagamento() {
		return valorPagamento;
	}
	
	/**
	 * Set: valorPagamento.
	 *
	 * @param valorPagamento the valor pagamento
	 */
	public void setValorPagamento(BigDecimal valorPagamento) {
		this.valorPagamento = valorPagamento;
	}

    /**
     * Nome: getCdFlagModalidade.
     *
     * @return cdFlagModalidade
     */
    public Integer getCdFlagModalidade() {
        return CdFlagModalidade;
    }

    /**
     * Nome: setCdFlagModalidade.
     *
     * @param cdFlagModalidade the cd flag modalidade
     */
    public void setCdFlagModalidade(Integer cdFlagModalidade) {
        CdFlagModalidade = cdFlagModalidade;
    }

    /**
     * Nome: getCdFlCodigoBarras.
     *
     * @return cdFlCodigoBarras
     */
    public String getCdFlCodigoBarras() {
        return CdFlCodigoBarras;
    }

    /**
     * Nome: setCdFlCodigoBarras.
     *
     * @param cdFlCodigoBarras the cd fl codigo barras
     */
    public void setCdFlCodigoBarras(String cdFlCodigoBarras) {
        CdFlCodigoBarras = cdFlCodigoBarras;
    }

    /**
     * Nome: getNrPagamento.
     *
     * @return nrPagamento
     */
    public String getNrPagamento() {
        return NrPagamento;
    }

    /**
     * Nome: setNrPagamento.
     *
     * @param nrPagamento the nr pagamento
     */
    public void setNrPagamento(String nrPagamento) {
        NrPagamento = nrPagamento;
    }
	
	
	
}
