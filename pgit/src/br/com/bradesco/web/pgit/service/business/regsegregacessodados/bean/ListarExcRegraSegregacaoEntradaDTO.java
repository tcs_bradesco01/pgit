/*
 * Nome: br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean;

/**
 * Nome: ListarExcRegraSegregacaoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarExcRegraSegregacaoEntradaDTO {
	
	/** Atributo cdRegraAcessoDado. */
	private int cdRegraAcessoDado;
	
	/** Atributo cdExcecaoAcessoDado. */
	private int cdExcecaoAcessoDado;
	
	/** Atributo tipoUnidadeOrganizacionalUsuario. */
	private int tipoUnidadeOrganizacionalUsuario;
	
	/** Atributo empresa. */
	private long empresa;
	
	/** Atributo unidadeOrganizacional. */
	private int unidadeOrganizacional;
	
	/** Atributo tipoExcecao. */
	private int tipoExcecao;
	
	/** Atributo tipoAbrangenciaAcesso. */
	private int tipoAbrangenciaAcesso;
	
	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public long getEmpresa() {
		return empresa;
	}
	
	/**
	 * Set: empresa.
	 *
	 * @param cdEmpresa the empresa
	 */
	public void setEmpresa(long cdEmpresa) {
		this.empresa = cdEmpresa;
	}
	
	/**
	 * Get: cdExcecaoAcessoDado.
	 *
	 * @return cdExcecaoAcessoDado
	 */
	public int getCdExcecaoAcessoDado() {
		return cdExcecaoAcessoDado;
	}
	
	/**
	 * Set: cdExcecaoAcessoDado.
	 *
	 * @param cdExcecaoAcessoDado the cd excecao acesso dado
	 */
	public void setCdExcecaoAcessoDado(int cdExcecaoAcessoDado) {
		this.cdExcecaoAcessoDado = cdExcecaoAcessoDado;
	}
	
	/**
	 * Get: cdRegraAcessoDado.
	 *
	 * @return cdRegraAcessoDado
	 */
	public int getCdRegraAcessoDado() {
		return cdRegraAcessoDado;
	}
	
	/**
	 * Set: cdRegraAcessoDado.
	 *
	 * @param cdRegraAcessoDado the cd regra acesso dado
	 */
	public void setCdRegraAcessoDado(int cdRegraAcessoDado) {
		this.cdRegraAcessoDado = cdRegraAcessoDado;
	}
	
	/**
	 * Get: tipoAbrangenciaAcesso.
	 *
	 * @return tipoAbrangenciaAcesso
	 */
	public int getTipoAbrangenciaAcesso() {
		return tipoAbrangenciaAcesso;
	}
	
	/**
	 * Set: tipoAbrangenciaAcesso.
	 *
	 * @param cdTipoAbrangenciaAcesso the tipo abrangencia acesso
	 */
	public void setTipoAbrangenciaAcesso(int cdTipoAbrangenciaAcesso) {
		this.tipoAbrangenciaAcesso = cdTipoAbrangenciaAcesso;
	}
	
	/**
	 * Get: tipoExcecao.
	 *
	 * @return tipoExcecao
	 */
	public int getTipoExcecao() {
		return tipoExcecao;
	}
	
	/**
	 * Set: tipoExcecao.
	 *
	 * @param cdTipoExcecao the tipo excecao
	 */
	public void setTipoExcecao(int cdTipoExcecao) {
		this.tipoExcecao = cdTipoExcecao;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @return tipoUnidadeOrganizacionalUsuario
	 */
	public int getTipoUnidadeOrganizacionalUsuario() {
		return tipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalUsuario.
	 *
	 * @param cdTipoUnidadeOrganizacionalUsuario the tipo unidade organizacional usuario
	 */
	public void setTipoUnidadeOrganizacionalUsuario(
			int cdTipoUnidadeOrganizacionalUsuario) {
		this.tipoUnidadeOrganizacionalUsuario = cdTipoUnidadeOrganizacionalUsuario;
	}
	
	/**
	 * Get: unidadeOrganizacional.
	 *
	 * @return unidadeOrganizacional
	 */
	public int getUnidadeOrganizacional() {
		return unidadeOrganizacional;
	}
	
	/**
	 * Set: unidadeOrganizacional.
	 *
	 * @param cdUnidadeOrganizacional the unidade organizacional
	 */
	public void setUnidadeOrganizacional(int cdUnidadeOrganizacional) {
		this.unidadeOrganizacional = cdUnidadeOrganizacional;
	}
	
	

}
