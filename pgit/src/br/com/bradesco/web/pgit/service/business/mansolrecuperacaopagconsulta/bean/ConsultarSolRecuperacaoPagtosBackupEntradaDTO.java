/*
 * Nome: br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean;

/**
 * Nome: ConsultarSolRecuperacaoPagtosBackupEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolRecuperacaoPagtosBackupEntradaDTO {
	
	 /** Atributo nrOcorrencias. */
 	private Integer nrOcorrencias;
	    
    	/** Atributo cdFinalidadeRecuperacao. */
    	private Integer cdFinalidadeRecuperacao;
	    
    	/** Atributo cdSelecaoRecuperacao. */
    	private Integer cdSelecaoRecuperacao;
	    
    	/** Atributo cdSolicitacaoPagamento. */
    	private Integer cdSolicitacaoPagamento;
	    
    	/** Atributo cdPessoaJuridicaContrato. */
    	private Long cdPessoaJuridicaContrato;
	    
    	/** Atributo cdTipoContratoNegocio. */
    	private Integer cdTipoContratoNegocio;
	    
    	/** Atributo nrSequenciaContratoNegocio. */
    	private Long nrSequenciaContratoNegocio;
	    
    	/** Atributo cdBancoDebito. */
    	private Integer cdBancoDebito;
	    
    	/** Atributo cdAgenciaDebito. */
    	private Integer cdAgenciaDebito;
	    
    	/** Atributo cdDigitoAgenciaDebito. */
    	private Integer cdDigitoAgenciaDebito;
	    
    	/** Atributo cdContaDebito. */
    	private Long cdContaDebito;
	    
    	/** Atributo cdDigitoContaDebito. */
    	private String cdDigitoContaDebito;
	    
    	/** Atributo cdTipoContaDebito. */
    	private Integer cdTipoContaDebito;
	    
    	/** Atributo dtInicioPesquisa. */
    	private String dtInicioPesquisa;
	    
    	/** Atributo dtFimPesquisa. */
    	private String dtFimPesquisa;
	    
    	/** Atributo cdProduto. */
    	private Integer cdProduto;
	    
    	/** Atributo cdProdutoRelacionado. */
    	private Integer cdProdutoRelacionado;
	    
    	/** Atributo cdSituacaoSolicitacaoPagamento. */
    	private Integer cdSituacaoSolicitacaoPagamento;
	    
    	/** Atributo cdMotivoSolicitacao. */
    	private Integer cdMotivoSolicitacao;
    	
    
		private Integer nmrSoltcPgto;


		public Integer getNrOcorrencias() {
			return nrOcorrencias;
		}


		public void setNrOcorrencias(Integer nrOcorrencias) {
			this.nrOcorrencias = nrOcorrencias;
		}


		public Integer getCdFinalidadeRecuperacao() {
			return cdFinalidadeRecuperacao;
		}


		public void setCdFinalidadeRecuperacao(Integer cdFinalidadeRecuperacao) {
			this.cdFinalidadeRecuperacao = cdFinalidadeRecuperacao;
		}


		public Integer getCdSelecaoRecuperacao() {
			return cdSelecaoRecuperacao;
		}


		public void setCdSelecaoRecuperacao(Integer cdSelecaoRecuperacao) {
			this.cdSelecaoRecuperacao = cdSelecaoRecuperacao;
		}


		public Integer getCdSolicitacaoPagamento() {
			return cdSolicitacaoPagamento;
		}


		public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
			this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
		}


		public Long getCdPessoaJuridicaContrato() {
			return cdPessoaJuridicaContrato;
		}


		public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
			this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
		}


		public Integer getCdTipoContratoNegocio() {
			return cdTipoContratoNegocio;
		}


		public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
			this.cdTipoContratoNegocio = cdTipoContratoNegocio;
		}


		public Long getNrSequenciaContratoNegocio() {
			return nrSequenciaContratoNegocio;
		}


		public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
			this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
		}


		public Integer getCdBancoDebito() {
			return cdBancoDebito;
		}


		public void setCdBancoDebito(Integer cdBancoDebito) {
			this.cdBancoDebito = cdBancoDebito;
		}


		public Integer getCdAgenciaDebito() {
			return cdAgenciaDebito;
		}


		public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
			this.cdAgenciaDebito = cdAgenciaDebito;
		}


		public Integer getCdDigitoAgenciaDebito() {
			return cdDigitoAgenciaDebito;
		}


		public void setCdDigitoAgenciaDebito(Integer cdDigitoAgenciaDebito) {
			this.cdDigitoAgenciaDebito = cdDigitoAgenciaDebito;
		}


		public Long getCdContaDebito() {
			return cdContaDebito;
		}


		public void setCdContaDebito(Long cdContaDebito) {
			this.cdContaDebito = cdContaDebito;
		}


		public String getCdDigitoContaDebito() {
			return cdDigitoContaDebito;
		}


		public void setCdDigitoContaDebito(String cdDigitoContaDebito) {
			this.cdDigitoContaDebito = cdDigitoContaDebito;
		}


		public Integer getCdTipoContaDebito() {
			return cdTipoContaDebito;
		}


		public void setCdTipoContaDebito(Integer cdTipoContaDebito) {
			this.cdTipoContaDebito = cdTipoContaDebito;
		}


		public String getDtInicioPesquisa() {
			return dtInicioPesquisa;
		}


		public void setDtInicioPesquisa(String dtInicioPesquisa) {
			this.dtInicioPesquisa = dtInicioPesquisa;
		}


		public String getDtFimPesquisa() {
			return dtFimPesquisa;
		}


		public void setDtFimPesquisa(String dtFimPesquisa) {
			this.dtFimPesquisa = dtFimPesquisa;
		}


		public Integer getCdProduto() {
			return cdProduto;
		}


		public void setCdProduto(Integer cdProduto) {
			this.cdProduto = cdProduto;
		}


		public Integer getCdProdutoRelacionado() {
			return cdProdutoRelacionado;
		}


		public void setCdProdutoRelacionado(Integer cdProdutoRelacionado) {
			this.cdProdutoRelacionado = cdProdutoRelacionado;
		}


		public Integer getCdSituacaoSolicitacaoPagamento() {
			return cdSituacaoSolicitacaoPagamento;
		}


		public void setCdSituacaoSolicitacaoPagamento(
				Integer cdSituacaoSolicitacaoPagamento) {
			this.cdSituacaoSolicitacaoPagamento = cdSituacaoSolicitacaoPagamento;
		}


		public Integer getCdMotivoSolicitacao() {
			return cdMotivoSolicitacao;
		}


		public void setCdMotivoSolicitacao(Integer cdMotivoSolicitacao) {
			this.cdMotivoSolicitacao = cdMotivoSolicitacao;
		}


		public Integer getNmrSoltcPgto() {
			return nmrSoltcPgto;
		}


		public void setNmrSoltcPgto(Integer nmrSoltcPgto) {
			this.nmrSoltcPgto = nmrSoltcPgto;
		}
	    

	    
    
    
}
