/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarassoctrocaarqparticipante.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdClub
     */
    private long _cdClub = 0;

    /**
     * keeps track of state for field: _cdClub
     */
    private boolean _has_cdClub;

    /**
     * Field _cdCpfCnpj
     */
    private long _cdCpfCnpj = 0;

    /**
     * keeps track of state for field: _cdCpfCnpj
     */
    private boolean _has_cdCpfCnpj;

    /**
     * Field _cdCnpjContrato
     */
    private int _cdCnpjContrato = 0;

    /**
     * keeps track of state for field: _cdCnpjContrato
     */
    private boolean _has_cdCnpjContrato;

    /**
     * Field _cdCpfCnpjDigito
     */
    private int _cdCpfCnpjDigito = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjDigito
     */
    private boolean _has_cdCpfCnpjDigito;

    /**
     * Field _cdRazaoSocial
     */
    private java.lang.String _cdRazaoSocial;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _cdPerfilTrocaArq
     */
    private long _cdPerfilTrocaArq = 0;

    /**
     * keeps track of state for field: _cdPerfilTrocaArq
     */
    private boolean _has_cdPerfilTrocaArq;

    /**
     * Field _cdTipoParticipacaoPessoa
     */
    private int _cdTipoParticipacaoPessoa = 0;

    /**
     * keeps track of state for field: _cdTipoParticipacaoPessoa
     */
    private boolean _has_cdTipoParticipacaoPessoa;

    /**
     * Field _dsTipoParticipante
     */
    private java.lang.String _dsTipoParticipante;

    /**
     * Field _dsLayout
     */
    private java.lang.String _dsLayout;

    /**
     * Field _dsLayoutProprio
     */
    private java.lang.String _dsLayoutProprio;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdAplicacaoTransmPagamento
     */
    private int _cdAplicacaoTransmPagamento = 0;

    /**
     * keeps track of state for field: _cdAplicacaoTransmPagamento
     */
    private boolean _has_cdAplicacaoTransmPagamento;

    /**
     * Field _dsProdutoServico
     */
    private java.lang.String _dsProdutoServico;

    /**
     * Field _dsAplicacaoTransmicaoPagamento
     */
    private java.lang.String _dsAplicacaoTransmicaoPagamento;

    /**
     * Field _cdMeioPrincipalRemessa
     */
    private int _cdMeioPrincipalRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRemessa
     */
    private boolean _has_cdMeioPrincipalRemessa;

    /**
     * Field _dsMeioPrincRemss
     */
    private java.lang.String _dsMeioPrincRemss;

    /**
     * Field _cdMeioAlternRemessa
     */
    private int _cdMeioAlternRemessa = 0;

    /**
     * keeps track of state for field: _cdMeioAlternRemessa
     */
    private boolean _has_cdMeioAlternRemessa;

    /**
     * Field _dsMeioAltrnRemss
     */
    private java.lang.String _dsMeioAltrnRemss;

    /**
     * Field _cdMeioPrincipalRetorno
     */
    private int _cdMeioPrincipalRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioPrincipalRetorno
     */
    private boolean _has_cdMeioPrincipalRetorno;

    /**
     * Field _dsMeioPrincRetor
     */
    private java.lang.String _dsMeioPrincRetor;

    /**
     * Field _cdMeioAltrnRetorno
     */
    private int _cdMeioAltrnRetorno = 0;

    /**
     * keeps track of state for field: _cdMeioAltrnRetorno
     */
    private boolean _has_cdMeioAltrnRetorno;

    /**
     * Field _dsMeioAltrnRetor
     */
    private java.lang.String _dsMeioAltrnRetor;

    /**
     * Field _cdPessoaJuridicaParceiro
     */
    private long _cdPessoaJuridicaParceiro = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaParceiro
     */
    private boolean _has_cdPessoaJuridicaParceiro;

    /**
     * Field _dsRzScialPcero
     */
    private java.lang.String _dsRzScialPcero;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarassoctrocaarqparticipante.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAplicacaoTransmPagamento
     * 
     */
    public void deleteCdAplicacaoTransmPagamento()
    {
        this._has_cdAplicacaoTransmPagamento= false;
    } //-- void deleteCdAplicacaoTransmPagamento() 

    /**
     * Method deleteCdClub
     * 
     */
    public void deleteCdClub()
    {
        this._has_cdClub= false;
    } //-- void deleteCdClub() 

    /**
     * Method deleteCdCnpjContrato
     * 
     */
    public void deleteCdCnpjContrato()
    {
        this._has_cdCnpjContrato= false;
    } //-- void deleteCdCnpjContrato() 

    /**
     * Method deleteCdCpfCnpj
     * 
     */
    public void deleteCdCpfCnpj()
    {
        this._has_cdCpfCnpj= false;
    } //-- void deleteCdCpfCnpj() 

    /**
     * Method deleteCdCpfCnpjDigito
     * 
     */
    public void deleteCdCpfCnpjDigito()
    {
        this._has_cdCpfCnpjDigito= false;
    } //-- void deleteCdCpfCnpjDigito() 

    /**
     * Method deleteCdMeioAlternRemessa
     * 
     */
    public void deleteCdMeioAlternRemessa()
    {
        this._has_cdMeioAlternRemessa= false;
    } //-- void deleteCdMeioAlternRemessa() 

    /**
     * Method deleteCdMeioAltrnRetorno
     * 
     */
    public void deleteCdMeioAltrnRetorno()
    {
        this._has_cdMeioAltrnRetorno= false;
    } //-- void deleteCdMeioAltrnRetorno() 

    /**
     * Method deleteCdMeioPrincipalRemessa
     * 
     */
    public void deleteCdMeioPrincipalRemessa()
    {
        this._has_cdMeioPrincipalRemessa= false;
    } //-- void deleteCdMeioPrincipalRemessa() 

    /**
     * Method deleteCdMeioPrincipalRetorno
     * 
     */
    public void deleteCdMeioPrincipalRetorno()
    {
        this._has_cdMeioPrincipalRetorno= false;
    } //-- void deleteCdMeioPrincipalRetorno() 

    /**
     * Method deleteCdPerfilTrocaArq
     * 
     */
    public void deleteCdPerfilTrocaArq()
    {
        this._has_cdPerfilTrocaArq= false;
    } //-- void deleteCdPerfilTrocaArq() 

    /**
     * Method deleteCdPessoaJuridicaParceiro
     * 
     */
    public void deleteCdPessoaJuridicaParceiro()
    {
        this._has_cdPessoaJuridicaParceiro= false;
    } //-- void deleteCdPessoaJuridicaParceiro() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdTipoParticipacaoPessoa
     * 
     */
    public void deleteCdTipoParticipacaoPessoa()
    {
        this._has_cdTipoParticipacaoPessoa= false;
    } //-- void deleteCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'cdAplicacaoTransmPagamento'.
     * 
     * @return int
     * @return the value of field 'cdAplicacaoTransmPagamento'.
     */
    public int getCdAplicacaoTransmPagamento()
    {
        return this._cdAplicacaoTransmPagamento;
    } //-- int getCdAplicacaoTransmPagamento() 

    /**
     * Returns the value of field 'cdClub'.
     * 
     * @return long
     * @return the value of field 'cdClub'.
     */
    public long getCdClub()
    {
        return this._cdClub;
    } //-- long getCdClub() 

    /**
     * Returns the value of field 'cdCnpjContrato'.
     * 
     * @return int
     * @return the value of field 'cdCnpjContrato'.
     */
    public int getCdCnpjContrato()
    {
        return this._cdCnpjContrato;
    } //-- int getCdCnpjContrato() 

    /**
     * Returns the value of field 'cdCpfCnpj'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpj'.
     */
    public long getCdCpfCnpj()
    {
        return this._cdCpfCnpj;
    } //-- long getCdCpfCnpj() 

    /**
     * Returns the value of field 'cdCpfCnpjDigito'.
     * 
     * @return int
     * @return the value of field 'cdCpfCnpjDigito'.
     */
    public int getCdCpfCnpjDigito()
    {
        return this._cdCpfCnpjDigito;
    } //-- int getCdCpfCnpjDigito() 

    /**
     * Returns the value of field 'cdMeioAlternRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioAlternRemessa'.
     */
    public int getCdMeioAlternRemessa()
    {
        return this._cdMeioAlternRemessa;
    } //-- int getCdMeioAlternRemessa() 

    /**
     * Returns the value of field 'cdMeioAltrnRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioAltrnRetorno'.
     */
    public int getCdMeioAltrnRetorno()
    {
        return this._cdMeioAltrnRetorno;
    } //-- int getCdMeioAltrnRetorno() 

    /**
     * Returns the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRemessa'.
     */
    public int getCdMeioPrincipalRemessa()
    {
        return this._cdMeioPrincipalRemessa;
    } //-- int getCdMeioPrincipalRemessa() 

    /**
     * Returns the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @return int
     * @return the value of field 'cdMeioPrincipalRetorno'.
     */
    public int getCdMeioPrincipalRetorno()
    {
        return this._cdMeioPrincipalRetorno;
    } //-- int getCdMeioPrincipalRetorno() 

    /**
     * Returns the value of field 'cdPerfilTrocaArq'.
     * 
     * @return long
     * @return the value of field 'cdPerfilTrocaArq'.
     */
    public long getCdPerfilTrocaArq()
    {
        return this._cdPerfilTrocaArq;
    } //-- long getCdPerfilTrocaArq() 

    /**
     * Returns the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaParceiro'.
     */
    public long getCdPessoaJuridicaParceiro()
    {
        return this._cdPessoaJuridicaParceiro;
    } //-- long getCdPessoaJuridicaParceiro() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'cdRazaoSocial'.
     */
    public java.lang.String getCdRazaoSocial()
    {
        return this._cdRazaoSocial;
    } //-- java.lang.String getCdRazaoSocial() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @return int
     * @return the value of field 'cdTipoParticipacaoPessoa'.
     */
    public int getCdTipoParticipacaoPessoa()
    {
        return this._cdTipoParticipacaoPessoa;
    } //-- int getCdTipoParticipacaoPessoa() 

    /**
     * Returns the value of field 'dsAplicacaoTransmicaoPagamento'.
     * 
     * @return String
     * @return the value of field 'dsAplicacaoTransmicaoPagamento'.
     */
    public java.lang.String getDsAplicacaoTransmicaoPagamento()
    {
        return this._dsAplicacaoTransmicaoPagamento;
    } //-- java.lang.String getDsAplicacaoTransmicaoPagamento() 

    /**
     * Returns the value of field 'dsLayout'.
     * 
     * @return String
     * @return the value of field 'dsLayout'.
     */
    public java.lang.String getDsLayout()
    {
        return this._dsLayout;
    } //-- java.lang.String getDsLayout() 

    /**
     * Returns the value of field 'dsLayoutProprio'.
     * 
     * @return String
     * @return the value of field 'dsLayoutProprio'.
     */
    public java.lang.String getDsLayoutProprio()
    {
        return this._dsLayoutProprio;
    } //-- java.lang.String getDsLayoutProprio() 

    /**
     * Returns the value of field 'dsMeioAltrnRemss'.
     * 
     * @return String
     * @return the value of field 'dsMeioAltrnRemss'.
     */
    public java.lang.String getDsMeioAltrnRemss()
    {
        return this._dsMeioAltrnRemss;
    } //-- java.lang.String getDsMeioAltrnRemss() 

    /**
     * Returns the value of field 'dsMeioAltrnRetor'.
     * 
     * @return String
     * @return the value of field 'dsMeioAltrnRetor'.
     */
    public java.lang.String getDsMeioAltrnRetor()
    {
        return this._dsMeioAltrnRetor;
    } //-- java.lang.String getDsMeioAltrnRetor() 

    /**
     * Returns the value of field 'dsMeioPrincRemss'.
     * 
     * @return String
     * @return the value of field 'dsMeioPrincRemss'.
     */
    public java.lang.String getDsMeioPrincRemss()
    {
        return this._dsMeioPrincRemss;
    } //-- java.lang.String getDsMeioPrincRemss() 

    /**
     * Returns the value of field 'dsMeioPrincRetor'.
     * 
     * @return String
     * @return the value of field 'dsMeioPrincRetor'.
     */
    public java.lang.String getDsMeioPrincRetor()
    {
        return this._dsMeioPrincRetor;
    } //-- java.lang.String getDsMeioPrincRetor() 

    /**
     * Returns the value of field 'dsProdutoServico'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServico'.
     */
    public java.lang.String getDsProdutoServico()
    {
        return this._dsProdutoServico;
    } //-- java.lang.String getDsProdutoServico() 

    /**
     * Returns the value of field 'dsRzScialPcero'.
     * 
     * @return String
     * @return the value of field 'dsRzScialPcero'.
     */
    public java.lang.String getDsRzScialPcero()
    {
        return this._dsRzScialPcero;
    } //-- java.lang.String getDsRzScialPcero() 

    /**
     * Returns the value of field 'dsTipoParticipante'.
     * 
     * @return String
     * @return the value of field 'dsTipoParticipante'.
     */
    public java.lang.String getDsTipoParticipante()
    {
        return this._dsTipoParticipante;
    } //-- java.lang.String getDsTipoParticipante() 

    /**
     * Method hasCdAplicacaoTransmPagamento
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAplicacaoTransmPagamento()
    {
        return this._has_cdAplicacaoTransmPagamento;
    } //-- boolean hasCdAplicacaoTransmPagamento() 

    /**
     * Method hasCdClub
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdClub()
    {
        return this._has_cdClub;
    } //-- boolean hasCdClub() 

    /**
     * Method hasCdCnpjContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjContrato()
    {
        return this._has_cdCnpjContrato;
    } //-- boolean hasCdCnpjContrato() 

    /**
     * Method hasCdCpfCnpj
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpj()
    {
        return this._has_cdCpfCnpj;
    } //-- boolean hasCdCpfCnpj() 

    /**
     * Method hasCdCpfCnpjDigito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjDigito()
    {
        return this._has_cdCpfCnpjDigito;
    } //-- boolean hasCdCpfCnpjDigito() 

    /**
     * Method hasCdMeioAlternRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAlternRemessa()
    {
        return this._has_cdMeioAlternRemessa;
    } //-- boolean hasCdMeioAlternRemessa() 

    /**
     * Method hasCdMeioAltrnRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioAltrnRetorno()
    {
        return this._has_cdMeioAltrnRetorno;
    } //-- boolean hasCdMeioAltrnRetorno() 

    /**
     * Method hasCdMeioPrincipalRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRemessa()
    {
        return this._has_cdMeioPrincipalRemessa;
    } //-- boolean hasCdMeioPrincipalRemessa() 

    /**
     * Method hasCdMeioPrincipalRetorno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMeioPrincipalRetorno()
    {
        return this._has_cdMeioPrincipalRetorno;
    } //-- boolean hasCdMeioPrincipalRetorno() 

    /**
     * Method hasCdPerfilTrocaArq
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPerfilTrocaArq()
    {
        return this._has_cdPerfilTrocaArq;
    } //-- boolean hasCdPerfilTrocaArq() 

    /**
     * Method hasCdPessoaJuridicaParceiro
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaParceiro()
    {
        return this._has_cdPessoaJuridicaParceiro;
    } //-- boolean hasCdPessoaJuridicaParceiro() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdTipoParticipacaoPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoParticipacaoPessoa()
    {
        return this._has_cdTipoParticipacaoPessoa;
    } //-- boolean hasCdTipoParticipacaoPessoa() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAplicacaoTransmPagamento'.
     * 
     * @param cdAplicacaoTransmPagamento the value of field
     * 'cdAplicacaoTransmPagamento'.
     */
    public void setCdAplicacaoTransmPagamento(int cdAplicacaoTransmPagamento)
    {
        this._cdAplicacaoTransmPagamento = cdAplicacaoTransmPagamento;
        this._has_cdAplicacaoTransmPagamento = true;
    } //-- void setCdAplicacaoTransmPagamento(int) 

    /**
     * Sets the value of field 'cdClub'.
     * 
     * @param cdClub the value of field 'cdClub'.
     */
    public void setCdClub(long cdClub)
    {
        this._cdClub = cdClub;
        this._has_cdClub = true;
    } //-- void setCdClub(long) 

    /**
     * Sets the value of field 'cdCnpjContrato'.
     * 
     * @param cdCnpjContrato the value of field 'cdCnpjContrato'.
     */
    public void setCdCnpjContrato(int cdCnpjContrato)
    {
        this._cdCnpjContrato = cdCnpjContrato;
        this._has_cdCnpjContrato = true;
    } //-- void setCdCnpjContrato(int) 

    /**
     * Sets the value of field 'cdCpfCnpj'.
     * 
     * @param cdCpfCnpj the value of field 'cdCpfCnpj'.
     */
    public void setCdCpfCnpj(long cdCpfCnpj)
    {
        this._cdCpfCnpj = cdCpfCnpj;
        this._has_cdCpfCnpj = true;
    } //-- void setCdCpfCnpj(long) 

    /**
     * Sets the value of field 'cdCpfCnpjDigito'.
     * 
     * @param cdCpfCnpjDigito the value of field 'cdCpfCnpjDigito'.
     */
    public void setCdCpfCnpjDigito(int cdCpfCnpjDigito)
    {
        this._cdCpfCnpjDigito = cdCpfCnpjDigito;
        this._has_cdCpfCnpjDigito = true;
    } //-- void setCdCpfCnpjDigito(int) 

    /**
     * Sets the value of field 'cdMeioAlternRemessa'.
     * 
     * @param cdMeioAlternRemessa the value of field
     * 'cdMeioAlternRemessa'.
     */
    public void setCdMeioAlternRemessa(int cdMeioAlternRemessa)
    {
        this._cdMeioAlternRemessa = cdMeioAlternRemessa;
        this._has_cdMeioAlternRemessa = true;
    } //-- void setCdMeioAlternRemessa(int) 

    /**
     * Sets the value of field 'cdMeioAltrnRetorno'.
     * 
     * @param cdMeioAltrnRetorno the value of field
     * 'cdMeioAltrnRetorno'.
     */
    public void setCdMeioAltrnRetorno(int cdMeioAltrnRetorno)
    {
        this._cdMeioAltrnRetorno = cdMeioAltrnRetorno;
        this._has_cdMeioAltrnRetorno = true;
    } //-- void setCdMeioAltrnRetorno(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRemessa'.
     * 
     * @param cdMeioPrincipalRemessa the value of field
     * 'cdMeioPrincipalRemessa'.
     */
    public void setCdMeioPrincipalRemessa(int cdMeioPrincipalRemessa)
    {
        this._cdMeioPrincipalRemessa = cdMeioPrincipalRemessa;
        this._has_cdMeioPrincipalRemessa = true;
    } //-- void setCdMeioPrincipalRemessa(int) 

    /**
     * Sets the value of field 'cdMeioPrincipalRetorno'.
     * 
     * @param cdMeioPrincipalRetorno the value of field
     * 'cdMeioPrincipalRetorno'.
     */
    public void setCdMeioPrincipalRetorno(int cdMeioPrincipalRetorno)
    {
        this._cdMeioPrincipalRetorno = cdMeioPrincipalRetorno;
        this._has_cdMeioPrincipalRetorno = true;
    } //-- void setCdMeioPrincipalRetorno(int) 

    /**
     * Sets the value of field 'cdPerfilTrocaArq'.
     * 
     * @param cdPerfilTrocaArq the value of field 'cdPerfilTrocaArq'
     */
    public void setCdPerfilTrocaArq(long cdPerfilTrocaArq)
    {
        this._cdPerfilTrocaArq = cdPerfilTrocaArq;
        this._has_cdPerfilTrocaArq = true;
    } //-- void setCdPerfilTrocaArq(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaParceiro'.
     * 
     * @param cdPessoaJuridicaParceiro the value of field
     * 'cdPessoaJuridicaParceiro'.
     */
    public void setCdPessoaJuridicaParceiro(long cdPessoaJuridicaParceiro)
    {
        this._cdPessoaJuridicaParceiro = cdPessoaJuridicaParceiro;
        this._has_cdPessoaJuridicaParceiro = true;
    } //-- void setCdPessoaJuridicaParceiro(long) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRazaoSocial'.
     * 
     * @param cdRazaoSocial the value of field 'cdRazaoSocial'.
     */
    public void setCdRazaoSocial(java.lang.String cdRazaoSocial)
    {
        this._cdRazaoSocial = cdRazaoSocial;
    } //-- void setCdRazaoSocial(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdTipoParticipacaoPessoa'.
     * 
     * @param cdTipoParticipacaoPessoa the value of field
     * 'cdTipoParticipacaoPessoa'.
     */
    public void setCdTipoParticipacaoPessoa(int cdTipoParticipacaoPessoa)
    {
        this._cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
        this._has_cdTipoParticipacaoPessoa = true;
    } //-- void setCdTipoParticipacaoPessoa(int) 

    /**
     * Sets the value of field 'dsAplicacaoTransmicaoPagamento'.
     * 
     * @param dsAplicacaoTransmicaoPagamento the value of field
     * 'dsAplicacaoTransmicaoPagamento'.
     */
    public void setDsAplicacaoTransmicaoPagamento(java.lang.String dsAplicacaoTransmicaoPagamento)
    {
        this._dsAplicacaoTransmicaoPagamento = dsAplicacaoTransmicaoPagamento;
    } //-- void setDsAplicacaoTransmicaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsLayout'.
     * 
     * @param dsLayout the value of field 'dsLayout'.
     */
    public void setDsLayout(java.lang.String dsLayout)
    {
        this._dsLayout = dsLayout;
    } //-- void setDsLayout(java.lang.String) 

    /**
     * Sets the value of field 'dsLayoutProprio'.
     * 
     * @param dsLayoutProprio the value of field 'dsLayoutProprio'.
     */
    public void setDsLayoutProprio(java.lang.String dsLayoutProprio)
    {
        this._dsLayoutProprio = dsLayoutProprio;
    } //-- void setDsLayoutProprio(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioAltrnRemss'.
     * 
     * @param dsMeioAltrnRemss the value of field 'dsMeioAltrnRemss'
     */
    public void setDsMeioAltrnRemss(java.lang.String dsMeioAltrnRemss)
    {
        this._dsMeioAltrnRemss = dsMeioAltrnRemss;
    } //-- void setDsMeioAltrnRemss(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioAltrnRetor'.
     * 
     * @param dsMeioAltrnRetor the value of field 'dsMeioAltrnRetor'
     */
    public void setDsMeioAltrnRetor(java.lang.String dsMeioAltrnRetor)
    {
        this._dsMeioAltrnRetor = dsMeioAltrnRetor;
    } //-- void setDsMeioAltrnRetor(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioPrincRemss'.
     * 
     * @param dsMeioPrincRemss the value of field 'dsMeioPrincRemss'
     */
    public void setDsMeioPrincRemss(java.lang.String dsMeioPrincRemss)
    {
        this._dsMeioPrincRemss = dsMeioPrincRemss;
    } //-- void setDsMeioPrincRemss(java.lang.String) 

    /**
     * Sets the value of field 'dsMeioPrincRetor'.
     * 
     * @param dsMeioPrincRetor the value of field 'dsMeioPrincRetor'
     */
    public void setDsMeioPrincRetor(java.lang.String dsMeioPrincRetor)
    {
        this._dsMeioPrincRetor = dsMeioPrincRetor;
    } //-- void setDsMeioPrincRetor(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServico'.
     * 
     * @param dsProdutoServico the value of field 'dsProdutoServico'
     */
    public void setDsProdutoServico(java.lang.String dsProdutoServico)
    {
        this._dsProdutoServico = dsProdutoServico;
    } //-- void setDsProdutoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsRzScialPcero'.
     * 
     * @param dsRzScialPcero the value of field 'dsRzScialPcero'.
     */
    public void setDsRzScialPcero(java.lang.String dsRzScialPcero)
    {
        this._dsRzScialPcero = dsRzScialPcero;
    } //-- void setDsRzScialPcero(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoParticipante'.
     * 
     * @param dsTipoParticipante the value of field
     * 'dsTipoParticipante'.
     */
    public void setDsTipoParticipante(java.lang.String dsTipoParticipante)
    {
        this._dsTipoParticipante = dsTipoParticipante;
    } //-- void setDsTipoParticipante(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarassoctrocaarqparticipante.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarassoctrocaarqparticipante.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarassoctrocaarqparticipante.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarassoctrocaarqparticipante.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
