/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdTipoManutencao
     */
    private int _cdTipoManutencao = 0;

    /**
     * keeps track of state for field: _cdTipoManutencao
     */
    private boolean _has_cdTipoManutencao;

    /**
     * Field _nrManutencaoContrato
     */
    private long _nrManutencaoContrato = 0;

    /**
     * keeps track of state for field: _nrManutencaoContrato
     */
    private boolean _has_nrManutencaoContrato;

    /**
     * Field _tpManutencao
     */
    private java.lang.String _tpManutencao;

    /**
     * Field _cdTipoAcao
     */
    private java.lang.String _cdTipoAcao;

    /**
     * Field _dtManutencao
     */
    private java.lang.String _dtManutencao;

    /**
     * Field _hrManutencao
     */
    private java.lang.String _hrManutencao;

    /**
     * Field _dsSituacao
     */
    private java.lang.String _dsSituacao;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdTipoManutencao
     * 
     */
    public void deleteCdTipoManutencao()
    {
        this._has_cdTipoManutencao= false;
    } //-- void deleteCdTipoManutencao() 

    /**
     * Method deleteNrManutencaoContrato
     * 
     */
    public void deleteNrManutencaoContrato()
    {
        this._has_nrManutencaoContrato= false;
    } //-- void deleteNrManutencaoContrato() 

    /**
     * Returns the value of field 'cdTipoAcao'.
     * 
     * @return String
     * @return the value of field 'cdTipoAcao'.
     */
    public java.lang.String getCdTipoAcao()
    {
        return this._cdTipoAcao;
    } //-- java.lang.String getCdTipoAcao() 

    /**
     * Returns the value of field 'cdTipoManutencao'.
     * 
     * @return int
     * @return the value of field 'cdTipoManutencao'.
     */
    public int getCdTipoManutencao()
    {
        return this._cdTipoManutencao;
    } //-- int getCdTipoManutencao() 

    /**
     * Returns the value of field 'dsSituacao'.
     * 
     * @return String
     * @return the value of field 'dsSituacao'.
     */
    public java.lang.String getDsSituacao()
    {
        return this._dsSituacao;
    } //-- java.lang.String getDsSituacao() 

    /**
     * Returns the value of field 'dtManutencao'.
     * 
     * @return String
     * @return the value of field 'dtManutencao'.
     */
    public java.lang.String getDtManutencao()
    {
        return this._dtManutencao;
    } //-- java.lang.String getDtManutencao() 

    /**
     * Returns the value of field 'hrManutencao'.
     * 
     * @return String
     * @return the value of field 'hrManutencao'.
     */
    public java.lang.String getHrManutencao()
    {
        return this._hrManutencao;
    } //-- java.lang.String getHrManutencao() 

    /**
     * Returns the value of field 'nrManutencaoContrato'.
     * 
     * @return long
     * @return the value of field 'nrManutencaoContrato'.
     */
    public long getNrManutencaoContrato()
    {
        return this._nrManutencaoContrato;
    } //-- long getNrManutencaoContrato() 

    /**
     * Returns the value of field 'tpManutencao'.
     * 
     * @return String
     * @return the value of field 'tpManutencao'.
     */
    public java.lang.String getTpManutencao()
    {
        return this._tpManutencao;
    } //-- java.lang.String getTpManutencao() 

    /**
     * Method hasCdTipoManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoManutencao()
    {
        return this._has_cdTipoManutencao;
    } //-- boolean hasCdTipoManutencao() 

    /**
     * Method hasNrManutencaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrManutencaoContrato()
    {
        return this._has_nrManutencaoContrato;
    } //-- boolean hasNrManutencaoContrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdTipoAcao'.
     * 
     * @param cdTipoAcao the value of field 'cdTipoAcao'.
     */
    public void setCdTipoAcao(java.lang.String cdTipoAcao)
    {
        this._cdTipoAcao = cdTipoAcao;
    } //-- void setCdTipoAcao(java.lang.String) 

    /**
     * Sets the value of field 'cdTipoManutencao'.
     * 
     * @param cdTipoManutencao the value of field 'cdTipoManutencao'
     */
    public void setCdTipoManutencao(int cdTipoManutencao)
    {
        this._cdTipoManutencao = cdTipoManutencao;
        this._has_cdTipoManutencao = true;
    } //-- void setCdTipoManutencao(int) 

    /**
     * Sets the value of field 'dsSituacao'.
     * 
     * @param dsSituacao the value of field 'dsSituacao'.
     */
    public void setDsSituacao(java.lang.String dsSituacao)
    {
        this._dsSituacao = dsSituacao;
    } //-- void setDsSituacao(java.lang.String) 

    /**
     * Sets the value of field 'dtManutencao'.
     * 
     * @param dtManutencao the value of field 'dtManutencao'.
     */
    public void setDtManutencao(java.lang.String dtManutencao)
    {
        this._dtManutencao = dtManutencao;
    } //-- void setDtManutencao(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencao'.
     * 
     * @param hrManutencao the value of field 'hrManutencao'.
     */
    public void setHrManutencao(java.lang.String hrManutencao)
    {
        this._hrManutencao = hrManutencao;
    } //-- void setHrManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrManutencaoContrato'.
     * 
     * @param nrManutencaoContrato the value of field
     * 'nrManutencaoContrato'.
     */
    public void setNrManutencaoContrato(long nrManutencaoContrato)
    {
        this._nrManutencaoContrato = nrManutencaoContrato;
        this._has_nrManutencaoContrato = true;
    } //-- void setNrManutencaoContrato(long) 

    /**
     * Sets the value of field 'tpManutencao'.
     * 
     * @param tpManutencao the value of field 'tpManutencao'.
     */
    public void setTpManutencao(java.lang.String tpManutencao)
    {
        this._tpManutencao = tpManutencao;
    } //-- void setTpManutencao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarlistacontratosmanutencao.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
