/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarlimdiaopepgit.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.math.BigDecimal;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class AlterarLimDIaOpePgitRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarLimDIaOpePgitRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _vlLimiteDiaPagamento
     */
    private java.math.BigDecimal _vlLimiteDiaPagamento = new java.math.BigDecimal("0");

    /**
     * Field _vlLimiteIndvdPagamento
     */
    private java.math.BigDecimal _vlLimiteIndvdPagamento = new java.math.BigDecimal("0");

    /**
     * Field _cdusuario
     */
    private java.lang.String _cdusuario;

    /**
     * Field _cdUsuarioExterno
     */
    private java.lang.String _cdUsuarioExterno;

    /**
     * Field _cdCanal
     */
    private int _cdCanal = 0;

    /**
     * keeps track of state for field: _cdCanal
     */
    private boolean _has_cdCanal;

    /**
     * Field _nmOperacaoFluxo
     */
    private java.lang.String _nmOperacaoFluxo;

    /**
     * Field _cdEmpresaOperante
     */
    private long _cdEmpresaOperante = 0;

    /**
     * keeps track of state for field: _cdEmpresaOperante
     */
    private boolean _has_cdEmpresaOperante;

    /**
     * Field _cdDependenteOperante
     */
    private int _cdDependenteOperante = 0;

    /**
     * keeps track of state for field: _cdDependenteOperante
     */
    private boolean _has_cdDependenteOperante;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarLimDIaOpePgitRequest() 
     {
        super();
        setVlLimiteDiaPagamento(new java.math.BigDecimal("0"));
        setVlLimiteIndvdPagamento(new java.math.BigDecimal("0"));
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarlimdiaopepgit.request.AlterarLimDIaOpePgitRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanal
     * 
     */
    public void deleteCdCanal()
    {
        this._has_cdCanal= false;
    } //-- void deleteCdCanal() 

    /**
     * Method deleteCdDependenteOperante
     * 
     */
    public void deleteCdDependenteOperante()
    {
        this._has_cdDependenteOperante= false;
    } //-- void deleteCdDependenteOperante() 

    /**
     * Method deleteCdEmpresaOperante
     * 
     */
    public void deleteCdEmpresaOperante()
    {
        this._has_cdEmpresaOperante= false;
    } //-- void deleteCdEmpresaOperante() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdCanal'.
     * 
     * @return int
     * @return the value of field 'cdCanal'.
     */
    public int getCdCanal()
    {
        return this._cdCanal;
    } //-- int getCdCanal() 

    /**
     * Returns the value of field 'cdDependenteOperante'.
     * 
     * @return int
     * @return the value of field 'cdDependenteOperante'.
     */
    public int getCdDependenteOperante()
    {
        return this._cdDependenteOperante;
    } //-- int getCdDependenteOperante() 

    /**
     * Returns the value of field 'cdEmpresaOperante'.
     * 
     * @return long
     * @return the value of field 'cdEmpresaOperante'.
     */
    public long getCdEmpresaOperante()
    {
        return this._cdEmpresaOperante;
    } //-- long getCdEmpresaOperante() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdUsuarioExterno'.
     * 
     * @return String
     * @return the value of field 'cdUsuarioExterno'.
     */
    public java.lang.String getCdUsuarioExterno()
    {
        return this._cdUsuarioExterno;
    } //-- java.lang.String getCdUsuarioExterno() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdusuario'.
     * 
     * @return String
     * @return the value of field 'cdusuario'.
     */
    public java.lang.String getCdusuario()
    {
        return this._cdusuario;
    } //-- java.lang.String getCdusuario() 

    /**
     * Returns the value of field 'nmOperacaoFluxo'.
     * 
     * @return String
     * @return the value of field 'nmOperacaoFluxo'.
     */
    public java.lang.String getNmOperacaoFluxo()
    {
        return this._nmOperacaoFluxo;
    } //-- java.lang.String getNmOperacaoFluxo() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'vlLimiteDiaPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlLimiteDiaPagamento'.
     */
    public java.math.BigDecimal getVlLimiteDiaPagamento()
    {
        return this._vlLimiteDiaPagamento;
    } //-- java.math.BigDecimal getVlLimiteDiaPagamento() 

    /**
     * Returns the value of field 'vlLimiteIndvdPagamento'.
     * 
     * @return BigDecimal
     * @return the value of field 'vlLimiteIndvdPagamento'.
     */
    public java.math.BigDecimal getVlLimiteIndvdPagamento()
    {
        return this._vlLimiteIndvdPagamento;
    } //-- java.math.BigDecimal getVlLimiteIndvdPagamento() 

    /**
     * Method hasCdCanal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanal()
    {
        return this._has_cdCanal;
    } //-- boolean hasCdCanal() 

    /**
     * Method hasCdDependenteOperante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdDependenteOperante()
    {
        return this._has_cdDependenteOperante;
    } //-- boolean hasCdDependenteOperante() 

    /**
     * Method hasCdEmpresaOperante
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresaOperante()
    {
        return this._has_cdEmpresaOperante;
    } //-- boolean hasCdEmpresaOperante() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCanal'.
     * 
     * @param cdCanal the value of field 'cdCanal'.
     */
    public void setCdCanal(int cdCanal)
    {
        this._cdCanal = cdCanal;
        this._has_cdCanal = true;
    } //-- void setCdCanal(int) 

    /**
     * Sets the value of field 'cdDependenteOperante'.
     * 
     * @param cdDependenteOperante the value of field
     * 'cdDependenteOperante'.
     */
    public void setCdDependenteOperante(int cdDependenteOperante)
    {
        this._cdDependenteOperante = cdDependenteOperante;
        this._has_cdDependenteOperante = true;
    } //-- void setCdDependenteOperante(int) 

    /**
     * Sets the value of field 'cdEmpresaOperante'.
     * 
     * @param cdEmpresaOperante the value of field
     * 'cdEmpresaOperante'.
     */
    public void setCdEmpresaOperante(long cdEmpresaOperante)
    {
        this._cdEmpresaOperante = cdEmpresaOperante;
        this._has_cdEmpresaOperante = true;
    } //-- void setCdEmpresaOperante(long) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdUsuarioExterno'.
     * 
     * @param cdUsuarioExterno the value of field 'cdUsuarioExterno'
     */
    public void setCdUsuarioExterno(java.lang.String cdUsuarioExterno)
    {
        this._cdUsuarioExterno = cdUsuarioExterno;
    } //-- void setCdUsuarioExterno(java.lang.String) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdusuario'.
     * 
     * @param cdusuario the value of field 'cdusuario'.
     */
    public void setCdusuario(java.lang.String cdusuario)
    {
        this._cdusuario = cdusuario;
    } //-- void setCdusuario(java.lang.String) 

    /**
     * Sets the value of field 'nmOperacaoFluxo'.
     * 
     * @param nmOperacaoFluxo the value of field 'nmOperacaoFluxo'.
     */
    public void setNmOperacaoFluxo(java.lang.String nmOperacaoFluxo)
    {
        this._nmOperacaoFluxo = nmOperacaoFluxo;
    } //-- void setNmOperacaoFluxo(java.lang.String) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Sets the value of field 'vlLimiteDiaPagamento'.
     * 
     * @param vlLimiteDiaPagamento the value of field
     * 'vlLimiteDiaPagamento'.
     */
    public void setVlLimiteDiaPagamento(java.math.BigDecimal vlLimiteDiaPagamento)
    {
        this._vlLimiteDiaPagamento = vlLimiteDiaPagamento;
    } //-- void setVlLimiteDiaPagamento(java.math.BigDecimal) 

    /**
     * Sets the value of field 'vlLimiteIndvdPagamento'.
     * 
     * @param vlLimiteIndvdPagamento the value of field
     * 'vlLimiteIndvdPagamento'.
     */
    public void setVlLimiteIndvdPagamento(java.math.BigDecimal vlLimiteIndvdPagamento)
    {
        this._vlLimiteIndvdPagamento = vlLimiteIndvdPagamento;
    } //-- void setVlLimiteIndvdPagamento(java.math.BigDecimal) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarLimDIaOpePgitRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarlimdiaopepgit.request.AlterarLimDIaOpePgitRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarlimdiaopepgit.request.AlterarLimDIaOpePgitRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarlimdiaopepgit.request.AlterarLimDIaOpePgitRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarlimdiaopepgit.request.AlterarLimDIaOpePgitRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
