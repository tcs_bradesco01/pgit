/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdRegraAcessoDado
     */
    private int _cdRegraAcessoDado = 0;

    /**
     * keeps track of state for field: _cdRegraAcessoDado
     */
    private boolean _has_cdRegraAcessoDado;

    /**
     * Field _cdExcecaoAcessoDado
     */
    private int _cdExcecaoAcessoDado = 0;

    /**
     * keeps track of state for field: _cdExcecaoAcessoDado
     */
    private boolean _has_cdExcecaoAcessoDado;

    /**
     * Field _cdTipoUnidadeUsuario
     */
    private int _cdTipoUnidadeUsuario = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeUsuario
     */
    private boolean _has_cdTipoUnidadeUsuario;

    /**
     * Field _dsTipoUnidadeUsuario
     */
    private java.lang.String _dsTipoUnidadeUsuario;

    /**
     * Field _cdPessoaJuridicaUsuario
     */
    private long _cdPessoaJuridicaUsuario = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaUsuario
     */
    private boolean _has_cdPessoaJuridicaUsuario;

    /**
     * Field _dsPessoaJuridicaUsuario
     */
    private java.lang.String _dsPessoaJuridicaUsuario;

    /**
     * Field _nrUnidadeOrganizacional
     */
    private int _nrUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _nrUnidadeOrganizacional
     */
    private boolean _has_nrUnidadeOrganizacional;

    /**
     * Field _dsUnidadeOrganizacional
     */
    private java.lang.String _dsUnidadeOrganizacional;

    /**
     * Field _cdTipoUnidadeProprietario
     */
    private int _cdTipoUnidadeProprietario = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeProprietario
     */
    private boolean _has_cdTipoUnidadeProprietario;

    /**
     * Field _dsTipoUnidadeProprietario
     */
    private java.lang.String _dsTipoUnidadeProprietario;

    /**
     * Field _cdPessoaJuridicaProprietario
     */
    private long _cdPessoaJuridicaProprietario = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaProprietario
     */
    private boolean _has_cdPessoaJuridicaProprietario;

    /**
     * Field _dsPessoaJuridicaProprietario
     */
    private java.lang.String _dsPessoaJuridicaProprietario;

    /**
     * Field _nrUnidadeOrganizacionalProprietario
     */
    private int _nrUnidadeOrganizacionalProprietario = 0;

    /**
     * keeps track of state for field:
     * _nrUnidadeOrganizacionalProprietario
     */
    private boolean _has_nrUnidadeOrganizacionalProprietario;

    /**
     * Field _dsUnidadeOrganizacionalProprietario
     */
    private java.lang.String _dsUnidadeOrganizacionalProprietario;

    /**
     * Field _cdTipoExcecaoAcesso
     */
    private int _cdTipoExcecaoAcesso = 0;

    /**
     * keeps track of state for field: _cdTipoExcecaoAcesso
     */
    private boolean _has_cdTipoExcecaoAcesso;

    /**
     * Field _cdTipoAbrangenciaExcecao
     */
    private int _cdTipoAbrangenciaExcecao = 0;

    /**
     * keeps track of state for field: _cdTipoAbrangenciaExcecao
     */
    private boolean _has_cdTipoAbrangenciaExcecao;

    /**
     * Field _cdCanalInclusao
     */
    private int _cdCanalInclusao = 0;

    /**
     * keeps track of state for field: _cdCanalInclusao
     */
    private boolean _has_cdCanalInclusao;

    /**
     * Field _dsCanalInclusao
     */
    private java.lang.String _dsCanalInclusao;

    /**
     * Field _cdAutenSegrcInclusao
     */
    private java.lang.String _cdAutenSegrcInclusao;

    /**
     * Field _nmOperFluxoInclusao
     */
    private java.lang.String _nmOperFluxoInclusao;

    /**
     * Field _hrInclusaoRegistro
     */
    private java.lang.String _hrInclusaoRegistro;

    /**
     * Field _cdCanalManutencao
     */
    private int _cdCanalManutencao = 0;

    /**
     * keeps track of state for field: _cdCanalManutencao
     */
    private boolean _has_cdCanalManutencao;

    /**
     * Field _dsCanalManutencao
     */
    private java.lang.String _dsCanalManutencao;

    /**
     * Field _cdAutenSegrcManutencao
     */
    private java.lang.String _cdAutenSegrcManutencao;

    /**
     * Field _nmOperFluxoManutencao
     */
    private java.lang.String _nmOperFluxoManutencao;

    /**
     * Field _hrManutencaoRegistro
     */
    private java.lang.String _hrManutencaoRegistro;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCanalInclusao
     * 
     */
    public void deleteCdCanalInclusao()
    {
        this._has_cdCanalInclusao= false;
    } //-- void deleteCdCanalInclusao() 

    /**
     * Method deleteCdCanalManutencao
     * 
     */
    public void deleteCdCanalManutencao()
    {
        this._has_cdCanalManutencao= false;
    } //-- void deleteCdCanalManutencao() 

    /**
     * Method deleteCdExcecaoAcessoDado
     * 
     */
    public void deleteCdExcecaoAcessoDado()
    {
        this._has_cdExcecaoAcessoDado= false;
    } //-- void deleteCdExcecaoAcessoDado() 

    /**
     * Method deleteCdPessoaJuridicaProprietario
     * 
     */
    public void deleteCdPessoaJuridicaProprietario()
    {
        this._has_cdPessoaJuridicaProprietario= false;
    } //-- void deleteCdPessoaJuridicaProprietario() 

    /**
     * Method deleteCdPessoaJuridicaUsuario
     * 
     */
    public void deleteCdPessoaJuridicaUsuario()
    {
        this._has_cdPessoaJuridicaUsuario= false;
    } //-- void deleteCdPessoaJuridicaUsuario() 

    /**
     * Method deleteCdRegraAcessoDado
     * 
     */
    public void deleteCdRegraAcessoDado()
    {
        this._has_cdRegraAcessoDado= false;
    } //-- void deleteCdRegraAcessoDado() 

    /**
     * Method deleteCdTipoAbrangenciaExcecao
     * 
     */
    public void deleteCdTipoAbrangenciaExcecao()
    {
        this._has_cdTipoAbrangenciaExcecao= false;
    } //-- void deleteCdTipoAbrangenciaExcecao() 

    /**
     * Method deleteCdTipoExcecaoAcesso
     * 
     */
    public void deleteCdTipoExcecaoAcesso()
    {
        this._has_cdTipoExcecaoAcesso= false;
    } //-- void deleteCdTipoExcecaoAcesso() 

    /**
     * Method deleteCdTipoUnidadeProprietario
     * 
     */
    public void deleteCdTipoUnidadeProprietario()
    {
        this._has_cdTipoUnidadeProprietario= false;
    } //-- void deleteCdTipoUnidadeProprietario() 

    /**
     * Method deleteCdTipoUnidadeUsuario
     * 
     */
    public void deleteCdTipoUnidadeUsuario()
    {
        this._has_cdTipoUnidadeUsuario= false;
    } //-- void deleteCdTipoUnidadeUsuario() 

    /**
     * Method deleteNrUnidadeOrganizacional
     * 
     */
    public void deleteNrUnidadeOrganizacional()
    {
        this._has_nrUnidadeOrganizacional= false;
    } //-- void deleteNrUnidadeOrganizacional() 

    /**
     * Method deleteNrUnidadeOrganizacionalProprietario
     * 
     */
    public void deleteNrUnidadeOrganizacionalProprietario()
    {
        this._has_nrUnidadeOrganizacionalProprietario= false;
    } //-- void deleteNrUnidadeOrganizacionalProprietario() 

    /**
     * Returns the value of field 'cdAutenSegrcInclusao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcInclusao'.
     */
    public java.lang.String getCdAutenSegrcInclusao()
    {
        return this._cdAutenSegrcInclusao;
    } //-- java.lang.String getCdAutenSegrcInclusao() 

    /**
     * Returns the value of field 'cdAutenSegrcManutencao'.
     * 
     * @return String
     * @return the value of field 'cdAutenSegrcManutencao'.
     */
    public java.lang.String getCdAutenSegrcManutencao()
    {
        return this._cdAutenSegrcManutencao;
    } //-- java.lang.String getCdAutenSegrcManutencao() 

    /**
     * Returns the value of field 'cdCanalInclusao'.
     * 
     * @return int
     * @return the value of field 'cdCanalInclusao'.
     */
    public int getCdCanalInclusao()
    {
        return this._cdCanalInclusao;
    } //-- int getCdCanalInclusao() 

    /**
     * Returns the value of field 'cdCanalManutencao'.
     * 
     * @return int
     * @return the value of field 'cdCanalManutencao'.
     */
    public int getCdCanalManutencao()
    {
        return this._cdCanalManutencao;
    } //-- int getCdCanalManutencao() 

    /**
     * Returns the value of field 'cdExcecaoAcessoDado'.
     * 
     * @return int
     * @return the value of field 'cdExcecaoAcessoDado'.
     */
    public int getCdExcecaoAcessoDado()
    {
        return this._cdExcecaoAcessoDado;
    } //-- int getCdExcecaoAcessoDado() 

    /**
     * Returns the value of field 'cdPessoaJuridicaProprietario'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaProprietario'.
     */
    public long getCdPessoaJuridicaProprietario()
    {
        return this._cdPessoaJuridicaProprietario;
    } //-- long getCdPessoaJuridicaProprietario() 

    /**
     * Returns the value of field 'cdPessoaJuridicaUsuario'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaUsuario'.
     */
    public long getCdPessoaJuridicaUsuario()
    {
        return this._cdPessoaJuridicaUsuario;
    } //-- long getCdPessoaJuridicaUsuario() 

    /**
     * Returns the value of field 'cdRegraAcessoDado'.
     * 
     * @return int
     * @return the value of field 'cdRegraAcessoDado'.
     */
    public int getCdRegraAcessoDado()
    {
        return this._cdRegraAcessoDado;
    } //-- int getCdRegraAcessoDado() 

    /**
     * Returns the value of field 'cdTipoAbrangenciaExcecao'.
     * 
     * @return int
     * @return the value of field 'cdTipoAbrangenciaExcecao'.
     */
    public int getCdTipoAbrangenciaExcecao()
    {
        return this._cdTipoAbrangenciaExcecao;
    } //-- int getCdTipoAbrangenciaExcecao() 

    /**
     * Returns the value of field 'cdTipoExcecaoAcesso'.
     * 
     * @return int
     * @return the value of field 'cdTipoExcecaoAcesso'.
     */
    public int getCdTipoExcecaoAcesso()
    {
        return this._cdTipoExcecaoAcesso;
    } //-- int getCdTipoExcecaoAcesso() 

    /**
     * Returns the value of field 'cdTipoUnidadeProprietario'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeProprietario'.
     */
    public int getCdTipoUnidadeProprietario()
    {
        return this._cdTipoUnidadeProprietario;
    } //-- int getCdTipoUnidadeProprietario() 

    /**
     * Returns the value of field 'cdTipoUnidadeUsuario'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeUsuario'.
     */
    public int getCdTipoUnidadeUsuario()
    {
        return this._cdTipoUnidadeUsuario;
    } //-- int getCdTipoUnidadeUsuario() 

    /**
     * Returns the value of field 'dsCanalInclusao'.
     * 
     * @return String
     * @return the value of field 'dsCanalInclusao'.
     */
    public java.lang.String getDsCanalInclusao()
    {
        return this._dsCanalInclusao;
    } //-- java.lang.String getDsCanalInclusao() 

    /**
     * Returns the value of field 'dsCanalManutencao'.
     * 
     * @return String
     * @return the value of field 'dsCanalManutencao'.
     */
    public java.lang.String getDsCanalManutencao()
    {
        return this._dsCanalManutencao;
    } //-- java.lang.String getDsCanalManutencao() 

    /**
     * Returns the value of field 'dsPessoaJuridicaProprietario'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridicaProprietario'.
     */
    public java.lang.String getDsPessoaJuridicaProprietario()
    {
        return this._dsPessoaJuridicaProprietario;
    } //-- java.lang.String getDsPessoaJuridicaProprietario() 

    /**
     * Returns the value of field 'dsPessoaJuridicaUsuario'.
     * 
     * @return String
     * @return the value of field 'dsPessoaJuridicaUsuario'.
     */
    public java.lang.String getDsPessoaJuridicaUsuario()
    {
        return this._dsPessoaJuridicaUsuario;
    } //-- java.lang.String getDsPessoaJuridicaUsuario() 

    /**
     * Returns the value of field 'dsTipoUnidadeProprietario'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeProprietario'.
     */
    public java.lang.String getDsTipoUnidadeProprietario()
    {
        return this._dsTipoUnidadeProprietario;
    } //-- java.lang.String getDsTipoUnidadeProprietario() 

    /**
     * Returns the value of field 'dsTipoUnidadeUsuario'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeUsuario'.
     */
    public java.lang.String getDsTipoUnidadeUsuario()
    {
        return this._dsTipoUnidadeUsuario;
    } //-- java.lang.String getDsTipoUnidadeUsuario() 

    /**
     * Returns the value of field 'dsUnidadeOrganizacional'.
     * 
     * @return String
     * @return the value of field 'dsUnidadeOrganizacional'.
     */
    public java.lang.String getDsUnidadeOrganizacional()
    {
        return this._dsUnidadeOrganizacional;
    } //-- java.lang.String getDsUnidadeOrganizacional() 

    /**
     * Returns the value of field
     * 'dsUnidadeOrganizacionalProprietario'.
     * 
     * @return String
     * @return the value of field
     * 'dsUnidadeOrganizacionalProprietario'.
     */
    public java.lang.String getDsUnidadeOrganizacionalProprietario()
    {
        return this._dsUnidadeOrganizacionalProprietario;
    } //-- java.lang.String getDsUnidadeOrganizacionalProprietario() 

    /**
     * Returns the value of field 'hrInclusaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrInclusaoRegistro'.
     */
    public java.lang.String getHrInclusaoRegistro()
    {
        return this._hrInclusaoRegistro;
    } //-- java.lang.String getHrInclusaoRegistro() 

    /**
     * Returns the value of field 'hrManutencaoRegistro'.
     * 
     * @return String
     * @return the value of field 'hrManutencaoRegistro'.
     */
    public java.lang.String getHrManutencaoRegistro()
    {
        return this._hrManutencaoRegistro;
    } //-- java.lang.String getHrManutencaoRegistro() 

    /**
     * Returns the value of field 'nmOperFluxoInclusao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoInclusao'.
     */
    public java.lang.String getNmOperFluxoInclusao()
    {
        return this._nmOperFluxoInclusao;
    } //-- java.lang.String getNmOperFluxoInclusao() 

    /**
     * Returns the value of field 'nmOperFluxoManutencao'.
     * 
     * @return String
     * @return the value of field 'nmOperFluxoManutencao'.
     */
    public java.lang.String getNmOperFluxoManutencao()
    {
        return this._nmOperFluxoManutencao;
    } //-- java.lang.String getNmOperFluxoManutencao() 

    /**
     * Returns the value of field 'nrUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrUnidadeOrganizacional'.
     */
    public int getNrUnidadeOrganizacional()
    {
        return this._nrUnidadeOrganizacional;
    } //-- int getNrUnidadeOrganizacional() 

    /**
     * Returns the value of field
     * 'nrUnidadeOrganizacionalProprietario'.
     * 
     * @return int
     * @return the value of field
     * 'nrUnidadeOrganizacionalProprietario'.
     */
    public int getNrUnidadeOrganizacionalProprietario()
    {
        return this._nrUnidadeOrganizacionalProprietario;
    } //-- int getNrUnidadeOrganizacionalProprietario() 

    /**
     * Method hasCdCanalInclusao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalInclusao()
    {
        return this._has_cdCanalInclusao;
    } //-- boolean hasCdCanalInclusao() 

    /**
     * Method hasCdCanalManutencao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCanalManutencao()
    {
        return this._has_cdCanalManutencao;
    } //-- boolean hasCdCanalManutencao() 

    /**
     * Method hasCdExcecaoAcessoDado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdExcecaoAcessoDado()
    {
        return this._has_cdExcecaoAcessoDado;
    } //-- boolean hasCdExcecaoAcessoDado() 

    /**
     * Method hasCdPessoaJuridicaProprietario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaProprietario()
    {
        return this._has_cdPessoaJuridicaProprietario;
    } //-- boolean hasCdPessoaJuridicaProprietario() 

    /**
     * Method hasCdPessoaJuridicaUsuario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaUsuario()
    {
        return this._has_cdPessoaJuridicaUsuario;
    } //-- boolean hasCdPessoaJuridicaUsuario() 

    /**
     * Method hasCdRegraAcessoDado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRegraAcessoDado()
    {
        return this._has_cdRegraAcessoDado;
    } //-- boolean hasCdRegraAcessoDado() 

    /**
     * Method hasCdTipoAbrangenciaExcecao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoAbrangenciaExcecao()
    {
        return this._has_cdTipoAbrangenciaExcecao;
    } //-- boolean hasCdTipoAbrangenciaExcecao() 

    /**
     * Method hasCdTipoExcecaoAcesso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoExcecaoAcesso()
    {
        return this._has_cdTipoExcecaoAcesso;
    } //-- boolean hasCdTipoExcecaoAcesso() 

    /**
     * Method hasCdTipoUnidadeProprietario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeProprietario()
    {
        return this._has_cdTipoUnidadeProprietario;
    } //-- boolean hasCdTipoUnidadeProprietario() 

    /**
     * Method hasCdTipoUnidadeUsuario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeUsuario()
    {
        return this._has_cdTipoUnidadeUsuario;
    } //-- boolean hasCdTipoUnidadeUsuario() 

    /**
     * Method hasNrUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrganizacional()
    {
        return this._has_nrUnidadeOrganizacional;
    } //-- boolean hasNrUnidadeOrganizacional() 

    /**
     * Method hasNrUnidadeOrganizacionalProprietario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrganizacionalProprietario()
    {
        return this._has_nrUnidadeOrganizacionalProprietario;
    } //-- boolean hasNrUnidadeOrganizacionalProprietario() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAutenSegrcInclusao'.
     * 
     * @param cdAutenSegrcInclusao the value of field
     * 'cdAutenSegrcInclusao'.
     */
    public void setCdAutenSegrcInclusao(java.lang.String cdAutenSegrcInclusao)
    {
        this._cdAutenSegrcInclusao = cdAutenSegrcInclusao;
    } //-- void setCdAutenSegrcInclusao(java.lang.String) 

    /**
     * Sets the value of field 'cdAutenSegrcManutencao'.
     * 
     * @param cdAutenSegrcManutencao the value of field
     * 'cdAutenSegrcManutencao'.
     */
    public void setCdAutenSegrcManutencao(java.lang.String cdAutenSegrcManutencao)
    {
        this._cdAutenSegrcManutencao = cdAutenSegrcManutencao;
    } //-- void setCdAutenSegrcManutencao(java.lang.String) 

    /**
     * Sets the value of field 'cdCanalInclusao'.
     * 
     * @param cdCanalInclusao the value of field 'cdCanalInclusao'.
     */
    public void setCdCanalInclusao(int cdCanalInclusao)
    {
        this._cdCanalInclusao = cdCanalInclusao;
        this._has_cdCanalInclusao = true;
    } //-- void setCdCanalInclusao(int) 

    /**
     * Sets the value of field 'cdCanalManutencao'.
     * 
     * @param cdCanalManutencao the value of field
     * 'cdCanalManutencao'.
     */
    public void setCdCanalManutencao(int cdCanalManutencao)
    {
        this._cdCanalManutencao = cdCanalManutencao;
        this._has_cdCanalManutencao = true;
    } //-- void setCdCanalManutencao(int) 

    /**
     * Sets the value of field 'cdExcecaoAcessoDado'.
     * 
     * @param cdExcecaoAcessoDado the value of field
     * 'cdExcecaoAcessoDado'.
     */
    public void setCdExcecaoAcessoDado(int cdExcecaoAcessoDado)
    {
        this._cdExcecaoAcessoDado = cdExcecaoAcessoDado;
        this._has_cdExcecaoAcessoDado = true;
    } //-- void setCdExcecaoAcessoDado(int) 

    /**
     * Sets the value of field 'cdPessoaJuridicaProprietario'.
     * 
     * @param cdPessoaJuridicaProprietario the value of field
     * 'cdPessoaJuridicaProprietario'.
     */
    public void setCdPessoaJuridicaProprietario(long cdPessoaJuridicaProprietario)
    {
        this._cdPessoaJuridicaProprietario = cdPessoaJuridicaProprietario;
        this._has_cdPessoaJuridicaProprietario = true;
    } //-- void setCdPessoaJuridicaProprietario(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaUsuario'.
     * 
     * @param cdPessoaJuridicaUsuario the value of field
     * 'cdPessoaJuridicaUsuario'.
     */
    public void setCdPessoaJuridicaUsuario(long cdPessoaJuridicaUsuario)
    {
        this._cdPessoaJuridicaUsuario = cdPessoaJuridicaUsuario;
        this._has_cdPessoaJuridicaUsuario = true;
    } //-- void setCdPessoaJuridicaUsuario(long) 

    /**
     * Sets the value of field 'cdRegraAcessoDado'.
     * 
     * @param cdRegraAcessoDado the value of field
     * 'cdRegraAcessoDado'.
     */
    public void setCdRegraAcessoDado(int cdRegraAcessoDado)
    {
        this._cdRegraAcessoDado = cdRegraAcessoDado;
        this._has_cdRegraAcessoDado = true;
    } //-- void setCdRegraAcessoDado(int) 

    /**
     * Sets the value of field 'cdTipoAbrangenciaExcecao'.
     * 
     * @param cdTipoAbrangenciaExcecao the value of field
     * 'cdTipoAbrangenciaExcecao'.
     */
    public void setCdTipoAbrangenciaExcecao(int cdTipoAbrangenciaExcecao)
    {
        this._cdTipoAbrangenciaExcecao = cdTipoAbrangenciaExcecao;
        this._has_cdTipoAbrangenciaExcecao = true;
    } //-- void setCdTipoAbrangenciaExcecao(int) 

    /**
     * Sets the value of field 'cdTipoExcecaoAcesso'.
     * 
     * @param cdTipoExcecaoAcesso the value of field
     * 'cdTipoExcecaoAcesso'.
     */
    public void setCdTipoExcecaoAcesso(int cdTipoExcecaoAcesso)
    {
        this._cdTipoExcecaoAcesso = cdTipoExcecaoAcesso;
        this._has_cdTipoExcecaoAcesso = true;
    } //-- void setCdTipoExcecaoAcesso(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeProprietario'.
     * 
     * @param cdTipoUnidadeProprietario the value of field
     * 'cdTipoUnidadeProprietario'.
     */
    public void setCdTipoUnidadeProprietario(int cdTipoUnidadeProprietario)
    {
        this._cdTipoUnidadeProprietario = cdTipoUnidadeProprietario;
        this._has_cdTipoUnidadeProprietario = true;
    } //-- void setCdTipoUnidadeProprietario(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeUsuario'.
     * 
     * @param cdTipoUnidadeUsuario the value of field
     * 'cdTipoUnidadeUsuario'.
     */
    public void setCdTipoUnidadeUsuario(int cdTipoUnidadeUsuario)
    {
        this._cdTipoUnidadeUsuario = cdTipoUnidadeUsuario;
        this._has_cdTipoUnidadeUsuario = true;
    } //-- void setCdTipoUnidadeUsuario(int) 

    /**
     * Sets the value of field 'dsCanalInclusao'.
     * 
     * @param dsCanalInclusao the value of field 'dsCanalInclusao'.
     */
    public void setDsCanalInclusao(java.lang.String dsCanalInclusao)
    {
        this._dsCanalInclusao = dsCanalInclusao;
    } //-- void setDsCanalInclusao(java.lang.String) 

    /**
     * Sets the value of field 'dsCanalManutencao'.
     * 
     * @param dsCanalManutencao the value of field
     * 'dsCanalManutencao'.
     */
    public void setDsCanalManutencao(java.lang.String dsCanalManutencao)
    {
        this._dsCanalManutencao = dsCanalManutencao;
    } //-- void setDsCanalManutencao(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridicaProprietario'.
     * 
     * @param dsPessoaJuridicaProprietario the value of field
     * 'dsPessoaJuridicaProprietario'.
     */
    public void setDsPessoaJuridicaProprietario(java.lang.String dsPessoaJuridicaProprietario)
    {
        this._dsPessoaJuridicaProprietario = dsPessoaJuridicaProprietario;
    } //-- void setDsPessoaJuridicaProprietario(java.lang.String) 

    /**
     * Sets the value of field 'dsPessoaJuridicaUsuario'.
     * 
     * @param dsPessoaJuridicaUsuario the value of field
     * 'dsPessoaJuridicaUsuario'.
     */
    public void setDsPessoaJuridicaUsuario(java.lang.String dsPessoaJuridicaUsuario)
    {
        this._dsPessoaJuridicaUsuario = dsPessoaJuridicaUsuario;
    } //-- void setDsPessoaJuridicaUsuario(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoUnidadeProprietario'.
     * 
     * @param dsTipoUnidadeProprietario the value of field
     * 'dsTipoUnidadeProprietario'.
     */
    public void setDsTipoUnidadeProprietario(java.lang.String dsTipoUnidadeProprietario)
    {
        this._dsTipoUnidadeProprietario = dsTipoUnidadeProprietario;
    } //-- void setDsTipoUnidadeProprietario(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoUnidadeUsuario'.
     * 
     * @param dsTipoUnidadeUsuario the value of field
     * 'dsTipoUnidadeUsuario'.
     */
    public void setDsTipoUnidadeUsuario(java.lang.String dsTipoUnidadeUsuario)
    {
        this._dsTipoUnidadeUsuario = dsTipoUnidadeUsuario;
    } //-- void setDsTipoUnidadeUsuario(java.lang.String) 

    /**
     * Sets the value of field 'dsUnidadeOrganizacional'.
     * 
     * @param dsUnidadeOrganizacional the value of field
     * 'dsUnidadeOrganizacional'.
     */
    public void setDsUnidadeOrganizacional(java.lang.String dsUnidadeOrganizacional)
    {
        this._dsUnidadeOrganizacional = dsUnidadeOrganizacional;
    } //-- void setDsUnidadeOrganizacional(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsUnidadeOrganizacionalProprietario'.
     * 
     * @param dsUnidadeOrganizacionalProprietario the value of
     * field 'dsUnidadeOrganizacionalProprietario'.
     */
    public void setDsUnidadeOrganizacionalProprietario(java.lang.String dsUnidadeOrganizacionalProprietario)
    {
        this._dsUnidadeOrganizacionalProprietario = dsUnidadeOrganizacionalProprietario;
    } //-- void setDsUnidadeOrganizacionalProprietario(java.lang.String) 

    /**
     * Sets the value of field 'hrInclusaoRegistro'.
     * 
     * @param hrInclusaoRegistro the value of field
     * 'hrInclusaoRegistro'.
     */
    public void setHrInclusaoRegistro(java.lang.String hrInclusaoRegistro)
    {
        this._hrInclusaoRegistro = hrInclusaoRegistro;
    } //-- void setHrInclusaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'hrManutencaoRegistro'.
     * 
     * @param hrManutencaoRegistro the value of field
     * 'hrManutencaoRegistro'.
     */
    public void setHrManutencaoRegistro(java.lang.String hrManutencaoRegistro)
    {
        this._hrManutencaoRegistro = hrManutencaoRegistro;
    } //-- void setHrManutencaoRegistro(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoInclusao'.
     * 
     * @param nmOperFluxoInclusao the value of field
     * 'nmOperFluxoInclusao'.
     */
    public void setNmOperFluxoInclusao(java.lang.String nmOperFluxoInclusao)
    {
        this._nmOperFluxoInclusao = nmOperFluxoInclusao;
    } //-- void setNmOperFluxoInclusao(java.lang.String) 

    /**
     * Sets the value of field 'nmOperFluxoManutencao'.
     * 
     * @param nmOperFluxoManutencao the value of field
     * 'nmOperFluxoManutencao'.
     */
    public void setNmOperFluxoManutencao(java.lang.String nmOperFluxoManutencao)
    {
        this._nmOperFluxoManutencao = nmOperFluxoManutencao;
    } //-- void setNmOperFluxoManutencao(java.lang.String) 

    /**
     * Sets the value of field 'nrUnidadeOrganizacional'.
     * 
     * @param nrUnidadeOrganizacional the value of field
     * 'nrUnidadeOrganizacional'.
     */
    public void setNrUnidadeOrganizacional(int nrUnidadeOrganizacional)
    {
        this._nrUnidadeOrganizacional = nrUnidadeOrganizacional;
        this._has_nrUnidadeOrganizacional = true;
    } //-- void setNrUnidadeOrganizacional(int) 

    /**
     * Sets the value of field
     * 'nrUnidadeOrganizacionalProprietario'.
     * 
     * @param nrUnidadeOrganizacionalProprietario the value of
     * field 'nrUnidadeOrganizacionalProprietario'.
     */
    public void setNrUnidadeOrganizacionalProprietario(int nrUnidadeOrganizacionalProprietario)
    {
        this._nrUnidadeOrganizacionalProprietario = nrUnidadeOrganizacionalProprietario;
        this._has_nrUnidadeOrganizacionalProprietario = true;
    } //-- void setNrUnidadeOrganizacionalProprietario(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.detalharexcecaoregrasegregacaoacesso.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
