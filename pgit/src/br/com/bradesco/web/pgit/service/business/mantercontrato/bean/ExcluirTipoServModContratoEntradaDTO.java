/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ExcluirTipoServModContratoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirTipoServModContratoEntradaDTO {
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo nrSequenciaContrato. */
	private Long nrSequenciaContrato;
	
	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;
	
	/** Atributo cdModalidade1. */
	private Integer cdModalidade1;
	
	/** Atributo cdModalidade2. */
	private Integer cdModalidade2;
	
	/** Atributo cdModalidade3. */
	private Integer cdModalidade3;
	
	/** Atributo cdModalidade4. */
	private Integer cdModalidade4;
	
	/** Atributo cdModalidade5. */
	private Integer cdModalidade5;
	
	/** Atributo cdModalidade6. */
	private Integer cdModalidade6;
	
	/** Atributo cdModalidade7. */
	private Integer cdModalidade7;
	
	/** Atributo cdModalidade8. */
	private Integer cdModalidade8;
	
	/** Atributo cdModalidade9. */
	private Integer cdModalidade9;
	
	/** Atributo cdModalidade10. */
	private Integer cdModalidade10;
	
	/** Atributo cdExerServicoModalidade. */
	private Integer cdExerServicoModalidade;
	
	/**
	 * Get: cdExerServicoModalidade.
	 *
	 * @return cdExerServicoModalidade
	 */
	public Integer getCdExerServicoModalidade() {
	    return cdExerServicoModalidade;
	}
	
	/**
	 * Set: cdExerServicoModalidade.
	 *
	 * @param cdExerServicoModalidade the cd exer servico modalidade
	 */
	public void setCdExerServicoModalidade(Integer cdExerServicoModalidade) {
	    this.cdExerServicoModalidade = cdExerServicoModalidade;
	}
	
	/**
	 * Get: cdModalidade1.
	 *
	 * @return cdModalidade1
	 */
	public Integer getCdModalidade1() {
		return cdModalidade1;
	}
	
	/**
	 * Set: cdModalidade1.
	 *
	 * @param cdModalidade1 the cd modalidade1
	 */
	public void setCdModalidade1(Integer cdModalidade1) {
		this.cdModalidade1 = cdModalidade1;
	}
	
	/**
	 * Get: cdModalidade10.
	 *
	 * @return cdModalidade10
	 */
	public Integer getCdModalidade10() {
		return cdModalidade10;
	}
	
	/**
	 * Set: cdModalidade10.
	 *
	 * @param cdModalidade10 the cd modalidade10
	 */
	public void setCdModalidade10(Integer cdModalidade10) {
		this.cdModalidade10 = cdModalidade10;
	}
	
	/**
	 * Get: cdModalidade2.
	 *
	 * @return cdModalidade2
	 */
	public Integer getCdModalidade2() {
		return cdModalidade2;
	}
	
	/**
	 * Set: cdModalidade2.
	 *
	 * @param cdModalidade2 the cd modalidade2
	 */
	public void setCdModalidade2(Integer cdModalidade2) {
		this.cdModalidade2 = cdModalidade2;
	}
	
	/**
	 * Get: cdModalidade3.
	 *
	 * @return cdModalidade3
	 */
	public Integer getCdModalidade3() {
		return cdModalidade3;
	}
	
	/**
	 * Set: cdModalidade3.
	 *
	 * @param cdModalidade3 the cd modalidade3
	 */
	public void setCdModalidade3(Integer cdModalidade3) {
		this.cdModalidade3 = cdModalidade3;
	}
	
	/**
	 * Get: cdModalidade4.
	 *
	 * @return cdModalidade4
	 */
	public Integer getCdModalidade4() {
		return cdModalidade4;
	}
	
	/**
	 * Set: cdModalidade4.
	 *
	 * @param cdModalidade4 the cd modalidade4
	 */
	public void setCdModalidade4(Integer cdModalidade4) {
		this.cdModalidade4 = cdModalidade4;
	}
	
	/**
	 * Get: cdModalidade5.
	 *
	 * @return cdModalidade5
	 */
	public Integer getCdModalidade5() {
		return cdModalidade5;
	}
	
	/**
	 * Set: cdModalidade5.
	 *
	 * @param cdModalidade5 the cd modalidade5
	 */
	public void setCdModalidade5(Integer cdModalidade5) {
		this.cdModalidade5 = cdModalidade5;
	}
	
	/**
	 * Get: cdModalidade6.
	 *
	 * @return cdModalidade6
	 */
	public Integer getCdModalidade6() {
		return cdModalidade6;
	}
	
	/**
	 * Set: cdModalidade6.
	 *
	 * @param cdModalidade6 the cd modalidade6
	 */
	public void setCdModalidade6(Integer cdModalidade6) {
		this.cdModalidade6 = cdModalidade6;
	}
	
	/**
	 * Get: cdModalidade7.
	 *
	 * @return cdModalidade7
	 */
	public Integer getCdModalidade7() {
		return cdModalidade7;
	}
	
	/**
	 * Set: cdModalidade7.
	 *
	 * @param cdModalidade7 the cd modalidade7
	 */
	public void setCdModalidade7(Integer cdModalidade7) {
		this.cdModalidade7 = cdModalidade7;
	}
	
	/**
	 * Get: cdModalidade8.
	 *
	 * @return cdModalidade8
	 */
	public Integer getCdModalidade8() {
		return cdModalidade8;
	}
	
	/**
	 * Set: cdModalidade8.
	 *
	 * @param cdModalidade8 the cd modalidade8
	 */
	public void setCdModalidade8(Integer cdModalidade8) {
		this.cdModalidade8 = cdModalidade8;
	}
	
	/**
	 * Get: cdModalidade9.
	 *
	 * @return cdModalidade9
	 */
	public Integer getCdModalidade9() {
		return cdModalidade9;
	}
	
	/**
	 * Set: cdModalidade9.
	 *
	 * @param cdModalidade9 the cd modalidade9
	 */
	public void setCdModalidade9(Integer cdModalidade9) {
		this.cdModalidade9 = cdModalidade9;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}
	
	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}
	
	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}
	
	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}
}
