/*
 * Nome: br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean;
			 
/**
 * Nome: LiberarPagtoPendConParcialEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class LiberarPagtoPendConParcialEntradaDTO {
	
	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo nrSequencialContrato. */
	private Long nrSequencialContrato;
	
	/** Atributo cdTipoCanal. */
	private Integer cdTipoCanal;
	
	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;
	
	/** Atributo cdModalidade. */
	private Integer cdModalidade;
	
	
	/**
	 * Get: cdControlePagamento.
	 *
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}
	
	/**
	 * Set: cdControlePagamento.
	 *
	 * @param cdControlePagamento the cd controle pagamento
	 */
	public void setCdControlePagamento(String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}
	
	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public Integer getCdModalidade() {
		return cdModalidade;
	}
	
	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(Integer cdModalidade) {
		this.cdModalidade = cdModalidade;
	}
	
	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}
	
	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}
	
	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}
	
	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}
	
	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}
	
	/**
	 * Get: nrSequencialContrato.
	 *
	 * @return nrSequencialContrato
	 */
	public Long getNrSequencialContrato() {
		return nrSequencialContrato;
	}
	
	/**
	 * Set: nrSequencialContrato.
	 *
	 * @param nrSequencialContrato the nr sequencial contrato
	 */
	public void setNrSequencialContrato(Long nrSequencialContrato) {
		this.nrSequencialContrato = nrSequencialContrato;
	}
	
	
}
