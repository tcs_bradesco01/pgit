/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias6.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias6 implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _dsServicoTipoServico
     */
    private java.lang.String _dsServicoTipoServico;

    /**
     * Field _dsFormaEnvioPagamento
     */
    private java.lang.String _dsFormaEnvioPagamento;

    /**
     * Field _dsServicoFormaAutorizacaoPagamento
     */
    private java.lang.String _dsServicoFormaAutorizacaoPagamento;

    /**
     * Field _dsIndicadorGeradorRetorno
     */
    private java.lang.String _dsIndicadorGeradorRetorno;

    /**
     * Field _dsServicoIndicadorRetornoSeparado
     */
    private java.lang.String _dsServicoIndicadorRetornoSeparado;

    /**
     * Field _cdServicoIndicadorListaDebito
     */
    private java.lang.String _cdServicoIndicadorListaDebito;

    /**
     * Field _dsServicoListaDebito
     */
    private java.lang.String _dsServicoListaDebito;

    /**
     * Field _dsServicoTratamentoListaDebito
     */
    private java.lang.String _dsServicoTratamentoListaDebito;

    /**
     * Field _dsServicoAutorizacaoComplementoAgencia
     */
    private java.lang.String _dsServicoAutorizacaoComplementoAgencia;

    /**
     * Field _dsServicoPreAutorizacaoCliente
     */
    private java.lang.String _dsServicoPreAutorizacaoCliente;

    /**
     * Field _dsServicoTipoDataFloat
     */
    private java.lang.String _dsServicoTipoDataFloat;

    /**
     * Field _cdServicoFloating
     */
    private java.lang.String _cdServicoFloating;

    /**
     * Field _cdServicoTipoConsolidado
     */
    private java.lang.String _cdServicoTipoConsolidado;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias6() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'cdServicoFloating'.
     * 
     * @return String
     * @return the value of field 'cdServicoFloating'.
     */
    public java.lang.String getCdServicoFloating()
    {
        return this._cdServicoFloating;
    } //-- java.lang.String getCdServicoFloating() 

    /**
     * Returns the value of field 'cdServicoIndicadorListaDebito'.
     * 
     * @return String
     * @return the value of field 'cdServicoIndicadorListaDebito'.
     */
    public java.lang.String getCdServicoIndicadorListaDebito()
    {
        return this._cdServicoIndicadorListaDebito;
    } //-- java.lang.String getCdServicoIndicadorListaDebito() 

    /**
     * Returns the value of field 'cdServicoTipoConsolidado'.
     * 
     * @return String
     * @return the value of field 'cdServicoTipoConsolidado'.
     */
    public java.lang.String getCdServicoTipoConsolidado()
    {
        return this._cdServicoTipoConsolidado;
    } //-- java.lang.String getCdServicoTipoConsolidado() 

    /**
     * Returns the value of field 'dsFormaEnvioPagamento'.
     * 
     * @return String
     * @return the value of field 'dsFormaEnvioPagamento'.
     */
    public java.lang.String getDsFormaEnvioPagamento()
    {
        return this._dsFormaEnvioPagamento;
    } //-- java.lang.String getDsFormaEnvioPagamento() 

    /**
     * Returns the value of field 'dsIndicadorGeradorRetorno'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorGeradorRetorno'.
     */
    public java.lang.String getDsIndicadorGeradorRetorno()
    {
        return this._dsIndicadorGeradorRetorno;
    } //-- java.lang.String getDsIndicadorGeradorRetorno() 

    /**
     * Returns the value of field
     * 'dsServicoAutorizacaoComplementoAgencia'.
     * 
     * @return String
     * @return the value of field
     * 'dsServicoAutorizacaoComplementoAgencia'.
     */
    public java.lang.String getDsServicoAutorizacaoComplementoAgencia()
    {
        return this._dsServicoAutorizacaoComplementoAgencia;
    } //-- java.lang.String getDsServicoAutorizacaoComplementoAgencia() 

    /**
     * Returns the value of field
     * 'dsServicoFormaAutorizacaoPagamento'.
     * 
     * @return String
     * @return the value of field
     * 'dsServicoFormaAutorizacaoPagamento'.
     */
    public java.lang.String getDsServicoFormaAutorizacaoPagamento()
    {
        return this._dsServicoFormaAutorizacaoPagamento;
    } //-- java.lang.String getDsServicoFormaAutorizacaoPagamento() 

    /**
     * Returns the value of field
     * 'dsServicoIndicadorRetornoSeparado'.
     * 
     * @return String
     * @return the value of field
     * 'dsServicoIndicadorRetornoSeparado'.
     */
    public java.lang.String getDsServicoIndicadorRetornoSeparado()
    {
        return this._dsServicoIndicadorRetornoSeparado;
    } //-- java.lang.String getDsServicoIndicadorRetornoSeparado() 

    /**
     * Returns the value of field 'dsServicoListaDebito'.
     * 
     * @return String
     * @return the value of field 'dsServicoListaDebito'.
     */
    public java.lang.String getDsServicoListaDebito()
    {
        return this._dsServicoListaDebito;
    } //-- java.lang.String getDsServicoListaDebito() 

    /**
     * Returns the value of field 'dsServicoPreAutorizacaoCliente'.
     * 
     * @return String
     * @return the value of field 'dsServicoPreAutorizacaoCliente'.
     */
    public java.lang.String getDsServicoPreAutorizacaoCliente()
    {
        return this._dsServicoPreAutorizacaoCliente;
    } //-- java.lang.String getDsServicoPreAutorizacaoCliente() 

    /**
     * Returns the value of field 'dsServicoTipoDataFloat'.
     * 
     * @return String
     * @return the value of field 'dsServicoTipoDataFloat'.
     */
    public java.lang.String getDsServicoTipoDataFloat()
    {
        return this._dsServicoTipoDataFloat;
    } //-- java.lang.String getDsServicoTipoDataFloat() 

    /**
     * Returns the value of field 'dsServicoTipoServico'.
     * 
     * @return String
     * @return the value of field 'dsServicoTipoServico'.
     */
    public java.lang.String getDsServicoTipoServico()
    {
        return this._dsServicoTipoServico;
    } //-- java.lang.String getDsServicoTipoServico() 

    /**
     * Returns the value of field 'dsServicoTratamentoListaDebito'.
     * 
     * @return String
     * @return the value of field 'dsServicoTratamentoListaDebito'.
     */
    public java.lang.String getDsServicoTratamentoListaDebito()
    {
        return this._dsServicoTratamentoListaDebito;
    } //-- java.lang.String getDsServicoTratamentoListaDebito() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdServicoFloating'.
     * 
     * @param cdServicoFloating the value of field
     * 'cdServicoFloating'.
     */
    public void setCdServicoFloating(java.lang.String cdServicoFloating)
    {
        this._cdServicoFloating = cdServicoFloating;
    } //-- void setCdServicoFloating(java.lang.String) 

    /**
     * Sets the value of field 'cdServicoIndicadorListaDebito'.
     * 
     * @param cdServicoIndicadorListaDebito the value of field
     * 'cdServicoIndicadorListaDebito'.
     */
    public void setCdServicoIndicadorListaDebito(java.lang.String cdServicoIndicadorListaDebito)
    {
        this._cdServicoIndicadorListaDebito = cdServicoIndicadorListaDebito;
    } //-- void setCdServicoIndicadorListaDebito(java.lang.String) 

    /**
     * Sets the value of field 'cdServicoTipoConsolidado'.
     * 
     * @param cdServicoTipoConsolidado the value of field
     * 'cdServicoTipoConsolidado'.
     */
    public void setCdServicoTipoConsolidado(java.lang.String cdServicoTipoConsolidado)
    {
        this._cdServicoTipoConsolidado = cdServicoTipoConsolidado;
    } //-- void setCdServicoTipoConsolidado(java.lang.String) 

    /**
     * Sets the value of field 'dsFormaEnvioPagamento'.
     * 
     * @param dsFormaEnvioPagamento the value of field
     * 'dsFormaEnvioPagamento'.
     */
    public void setDsFormaEnvioPagamento(java.lang.String dsFormaEnvioPagamento)
    {
        this._dsFormaEnvioPagamento = dsFormaEnvioPagamento;
    } //-- void setDsFormaEnvioPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsIndicadorGeradorRetorno'.
     * 
     * @param dsIndicadorGeradorRetorno the value of field
     * 'dsIndicadorGeradorRetorno'.
     */
    public void setDsIndicadorGeradorRetorno(java.lang.String dsIndicadorGeradorRetorno)
    {
        this._dsIndicadorGeradorRetorno = dsIndicadorGeradorRetorno;
    } //-- void setDsIndicadorGeradorRetorno(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsServicoAutorizacaoComplementoAgencia'.
     * 
     * @param dsServicoAutorizacaoComplementoAgencia the value of
     * field 'dsServicoAutorizacaoComplementoAgencia'.
     */
    public void setDsServicoAutorizacaoComplementoAgencia(java.lang.String dsServicoAutorizacaoComplementoAgencia)
    {
        this._dsServicoAutorizacaoComplementoAgencia = dsServicoAutorizacaoComplementoAgencia;
    } //-- void setDsServicoAutorizacaoComplementoAgencia(java.lang.String) 

    /**
     * Sets the value of field
     * 'dsServicoFormaAutorizacaoPagamento'.
     * 
     * @param dsServicoFormaAutorizacaoPagamento the value of field
     * 'dsServicoFormaAutorizacaoPagamento'.
     */
    public void setDsServicoFormaAutorizacaoPagamento(java.lang.String dsServicoFormaAutorizacaoPagamento)
    {
        this._dsServicoFormaAutorizacaoPagamento = dsServicoFormaAutorizacaoPagamento;
    } //-- void setDsServicoFormaAutorizacaoPagamento(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoIndicadorRetornoSeparado'.
     * 
     * @param dsServicoIndicadorRetornoSeparado the value of field
     * 'dsServicoIndicadorRetornoSeparado'.
     */
    public void setDsServicoIndicadorRetornoSeparado(java.lang.String dsServicoIndicadorRetornoSeparado)
    {
        this._dsServicoIndicadorRetornoSeparado = dsServicoIndicadorRetornoSeparado;
    } //-- void setDsServicoIndicadorRetornoSeparado(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoListaDebito'.
     * 
     * @param dsServicoListaDebito the value of field
     * 'dsServicoListaDebito'.
     */
    public void setDsServicoListaDebito(java.lang.String dsServicoListaDebito)
    {
        this._dsServicoListaDebito = dsServicoListaDebito;
    } //-- void setDsServicoListaDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoPreAutorizacaoCliente'.
     * 
     * @param dsServicoPreAutorizacaoCliente the value of field
     * 'dsServicoPreAutorizacaoCliente'.
     */
    public void setDsServicoPreAutorizacaoCliente(java.lang.String dsServicoPreAutorizacaoCliente)
    {
        this._dsServicoPreAutorizacaoCliente = dsServicoPreAutorizacaoCliente;
    } //-- void setDsServicoPreAutorizacaoCliente(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoTipoDataFloat'.
     * 
     * @param dsServicoTipoDataFloat the value of field
     * 'dsServicoTipoDataFloat'.
     */
    public void setDsServicoTipoDataFloat(java.lang.String dsServicoTipoDataFloat)
    {
        this._dsServicoTipoDataFloat = dsServicoTipoDataFloat;
    } //-- void setDsServicoTipoDataFloat(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoTipoServico'.
     * 
     * @param dsServicoTipoServico the value of field
     * 'dsServicoTipoServico'.
     */
    public void setDsServicoTipoServico(java.lang.String dsServicoTipoServico)
    {
        this._dsServicoTipoServico = dsServicoTipoServico;
    } //-- void setDsServicoTipoServico(java.lang.String) 

    /**
     * Sets the value of field 'dsServicoTratamentoListaDebito'.
     * 
     * @param dsServicoTratamentoListaDebito the value of field
     * 'dsServicoTratamentoListaDebito'.
     */
    public void setDsServicoTratamentoListaDebito(java.lang.String dsServicoTratamentoListaDebito)
    {
        this._dsServicoTratamentoListaDebito = dsServicoTratamentoListaDebito;
    } //-- void setDsServicoTratamentoListaDebito(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias6
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6 unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.imprimiranexosegundocontrato.response.Ocorrencias6 unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
