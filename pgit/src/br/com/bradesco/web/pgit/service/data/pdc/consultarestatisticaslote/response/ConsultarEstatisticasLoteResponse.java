/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Vector;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarEstatisticasLoteResponse.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarEstatisticasLoteResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _codMensagem
     */
    private java.lang.String _codMensagem;

    /**
     * Field _mensagem
     */
    private java.lang.String _mensagem;

    /**
     * Field _quantidadeConsistente
     */
    private long _quantidadeConsistente = 0;

    /**
     * keeps track of state for field: _quantidadeConsistente
     */
    private boolean _has_quantidadeConsistente;

    /**
     * Field _valorConsistente
     */
    private double _valorConsistente = 0;

    /**
     * keeps track of state for field: _valorConsistente
     */
    private boolean _has_valorConsistente;

    /**
     * Field _quantidadeInconsistente
     */
    private long _quantidadeInconsistente = 0;

    /**
     * keeps track of state for field: _quantidadeInconsistente
     */
    private boolean _has_quantidadeInconsistente;

    /**
     * Field _valorIncosistente
     */
    private double _valorIncosistente = 0;

    /**
     * keeps track of state for field: _valorIncosistente
     */
    private boolean _has_valorIncosistente;

    /**
     * Field _quantidadeTotal
     */
    private long _quantidadeTotal = 0;

    /**
     * keeps track of state for field: _quantidadeTotal
     */
    private boolean _has_quantidadeTotal;

    /**
     * Field _valorTotal
     */
    private double _valorTotal = 0;

    /**
     * keeps track of state for field: _valorTotal
     */
    private boolean _has_valorTotal;

    /**
     * Field _numeroLinhas
     */
    private int _numeroLinhas = 0;

    /**
     * keeps track of state for field: _numeroLinhas
     */
    private boolean _has_numeroLinhas;

    /**
     * Field _ocorrenciasList
     */
    private java.util.Vector _ocorrenciasList;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarEstatisticasLoteResponse() 
     {
        super();
        _ocorrenciasList = new Vector();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.ConsultarEstatisticasLoteResponse()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param vOcorrencias
     */
    public void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.addElement(vOcorrencias);
    } //-- void addOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias) 

    /**
     * Method addOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void addOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        _ocorrenciasList.insertElementAt(vOcorrencias, index);
    } //-- void addOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias) 

    /**
     * Method deleteNumeroLinhas
     * 
     */
    public void deleteNumeroLinhas()
    {
        this._has_numeroLinhas= false;
    } //-- void deleteNumeroLinhas() 

    /**
     * Method deleteQuantidadeConsistente
     * 
     */
    public void deleteQuantidadeConsistente()
    {
        this._has_quantidadeConsistente= false;
    } //-- void deleteQuantidadeConsistente() 

    /**
     * Method deleteQuantidadeInconsistente
     * 
     */
    public void deleteQuantidadeInconsistente()
    {
        this._has_quantidadeInconsistente= false;
    } //-- void deleteQuantidadeInconsistente() 

    /**
     * Method deleteQuantidadeTotal
     * 
     */
    public void deleteQuantidadeTotal()
    {
        this._has_quantidadeTotal= false;
    } //-- void deleteQuantidadeTotal() 

    /**
     * Method deleteValorConsistente
     * 
     */
    public void deleteValorConsistente()
    {
        this._has_valorConsistente= false;
    } //-- void deleteValorConsistente() 

    /**
     * Method deleteValorIncosistente
     * 
     */
    public void deleteValorIncosistente()
    {
        this._has_valorIncosistente= false;
    } //-- void deleteValorIncosistente() 

    /**
     * Method deleteValorTotal
     * 
     */
    public void deleteValorTotal()
    {
        this._has_valorTotal= false;
    } //-- void deleteValorTotal() 

    /**
     * Method enumerateOcorrencias
     * 
     * 
     * 
     * @return Enumeration
     */
    public java.util.Enumeration enumerateOcorrencias()
    {
        return _ocorrenciasList.elements();
    } //-- java.util.Enumeration enumerateOcorrencias() 

    /**
     * Returns the value of field 'codMensagem'.
     * 
     * @return String
     * @return the value of field 'codMensagem'.
     */
    public java.lang.String getCodMensagem()
    {
        return this._codMensagem;
    } //-- java.lang.String getCodMensagem() 

    /**
     * Returns the value of field 'mensagem'.
     * 
     * @return String
     * @return the value of field 'mensagem'.
     */
    public java.lang.String getMensagem()
    {
        return this._mensagem;
    } //-- java.lang.String getMensagem() 

    /**
     * Returns the value of field 'numeroLinhas'.
     * 
     * @return int
     * @return the value of field 'numeroLinhas'.
     */
    public int getNumeroLinhas()
    {
        return this._numeroLinhas;
    } //-- int getNumeroLinhas() 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias getOcorrencias(int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("getOcorrencias: Index value '"+index+"' not in range [0.."+(_ocorrenciasList.size() - 1) + "]");
        }
        
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias) _ocorrenciasList.elementAt(index);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias getOcorrencias(int) 

    /**
     * Method getOcorrencias
     * 
     * 
     * 
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias[] getOcorrencias()
    {
        int size = _ocorrenciasList.size();
        br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias[] mArray = new br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias[size];
        for (int index = 0; index < size; index++) {
            mArray[index] = (br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias) _ocorrenciasList.elementAt(index);
        }
        return mArray;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias[] getOcorrencias() 

    /**
     * Method getOcorrenciasCount
     * 
     * 
     * 
     * @return int
     */
    public int getOcorrenciasCount()
    {
        return _ocorrenciasList.size();
    } //-- int getOcorrenciasCount() 

    /**
     * Returns the value of field 'quantidadeConsistente'.
     * 
     * @return long
     * @return the value of field 'quantidadeConsistente'.
     */
    public long getQuantidadeConsistente()
    {
        return this._quantidadeConsistente;
    } //-- long getQuantidadeConsistente() 

    /**
     * Returns the value of field 'quantidadeInconsistente'.
     * 
     * @return long
     * @return the value of field 'quantidadeInconsistente'.
     */
    public long getQuantidadeInconsistente()
    {
        return this._quantidadeInconsistente;
    } //-- long getQuantidadeInconsistente() 

    /**
     * Returns the value of field 'quantidadeTotal'.
     * 
     * @return long
     * @return the value of field 'quantidadeTotal'.
     */
    public long getQuantidadeTotal()
    {
        return this._quantidadeTotal;
    } //-- long getQuantidadeTotal() 

    /**
     * Returns the value of field 'valorConsistente'.
     * 
     * @return double
     * @return the value of field 'valorConsistente'.
     */
    public double getValorConsistente()
    {
        return this._valorConsistente;
    } //-- double getValorConsistente() 

    /**
     * Returns the value of field 'valorIncosistente'.
     * 
     * @return double
     * @return the value of field 'valorIncosistente'.
     */
    public double getValorIncosistente()
    {
        return this._valorIncosistente;
    } //-- double getValorIncosistente() 

    /**
     * Returns the value of field 'valorTotal'.
     * 
     * @return double
     * @return the value of field 'valorTotal'.
     */
    public double getValorTotal()
    {
        return this._valorTotal;
    } //-- double getValorTotal() 

    /**
     * Method hasNumeroLinhas
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroLinhas()
    {
        return this._has_numeroLinhas;
    } //-- boolean hasNumeroLinhas() 

    /**
     * Method hasQuantidadeConsistente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeConsistente()
    {
        return this._has_quantidadeConsistente;
    } //-- boolean hasQuantidadeConsistente() 

    /**
     * Method hasQuantidadeInconsistente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeInconsistente()
    {
        return this._has_quantidadeInconsistente;
    } //-- boolean hasQuantidadeInconsistente() 

    /**
     * Method hasQuantidadeTotal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasQuantidadeTotal()
    {
        return this._has_quantidadeTotal;
    } //-- boolean hasQuantidadeTotal() 

    /**
     * Method hasValorConsistente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorConsistente()
    {
        return this._has_valorConsistente;
    } //-- boolean hasValorConsistente() 

    /**
     * Method hasValorIncosistente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorIncosistente()
    {
        return this._has_valorIncosistente;
    } //-- boolean hasValorIncosistente() 

    /**
     * Method hasValorTotal
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasValorTotal()
    {
        return this._has_valorTotal;
    } //-- boolean hasValorTotal() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Method removeAllOcorrencias
     * 
     */
    public void removeAllOcorrencias()
    {
        _ocorrenciasList.removeAllElements();
    } //-- void removeAllOcorrencias() 

    /**
     * Method removeOcorrencias
     * 
     * 
     * 
     * @param index
     * @return Ocorrencias
     */
    public br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias removeOcorrencias(int index)
    {
        java.lang.Object obj = _ocorrenciasList.elementAt(index);
        _ocorrenciasList.removeElementAt(index);
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias) obj;
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias removeOcorrencias(int) 

    /**
     * Sets the value of field 'codMensagem'.
     * 
     * @param codMensagem the value of field 'codMensagem'.
     */
    public void setCodMensagem(java.lang.String codMensagem)
    {
        this._codMensagem = codMensagem;
    } //-- void setCodMensagem(java.lang.String) 

    /**
     * Sets the value of field 'mensagem'.
     * 
     * @param mensagem the value of field 'mensagem'.
     */
    public void setMensagem(java.lang.String mensagem)
    {
        this._mensagem = mensagem;
    } //-- void setMensagem(java.lang.String) 

    /**
     * Sets the value of field 'numeroLinhas'.
     * 
     * @param numeroLinhas the value of field 'numeroLinhas'.
     */
    public void setNumeroLinhas(int numeroLinhas)
    {
        this._numeroLinhas = numeroLinhas;
        this._has_numeroLinhas = true;
    } //-- void setNumeroLinhas(int) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param index
     * @param vOcorrencias
     */
    public void setOcorrencias(int index, br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias vOcorrencias)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index >= _ocorrenciasList.size())) {
            throw new IndexOutOfBoundsException("setOcorrencias: Index value '"+index+"' not in range [0.." + (_ocorrenciasList.size() - 1) + "]");
        }
        _ocorrenciasList.setElementAt(vOcorrencias, index);
    } //-- void setOcorrencias(int, br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias) 

    /**
     * Method setOcorrencias
     * 
     * 
     * 
     * @param ocorrenciasArray
     */
    public void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias[] ocorrenciasArray)
    {
        //-- copy array
        _ocorrenciasList.removeAllElements();
        for (int i = 0; i < ocorrenciasArray.length; i++) {
            _ocorrenciasList.addElement(ocorrenciasArray[i]);
        }
    } //-- void setOcorrencias(br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.Ocorrencias) 

    /**
     * Sets the value of field 'quantidadeConsistente'.
     * 
     * @param quantidadeConsistente the value of field
     * 'quantidadeConsistente'.
     */
    public void setQuantidadeConsistente(long quantidadeConsistente)
    {
        this._quantidadeConsistente = quantidadeConsistente;
        this._has_quantidadeConsistente = true;
    } //-- void setQuantidadeConsistente(long) 

    /**
     * Sets the value of field 'quantidadeInconsistente'.
     * 
     * @param quantidadeInconsistente the value of field
     * 'quantidadeInconsistente'.
     */
    public void setQuantidadeInconsistente(long quantidadeInconsistente)
    {
        this._quantidadeInconsistente = quantidadeInconsistente;
        this._has_quantidadeInconsistente = true;
    } //-- void setQuantidadeInconsistente(long) 

    /**
     * Sets the value of field 'quantidadeTotal'.
     * 
     * @param quantidadeTotal the value of field 'quantidadeTotal'.
     */
    public void setQuantidadeTotal(long quantidadeTotal)
    {
        this._quantidadeTotal = quantidadeTotal;
        this._has_quantidadeTotal = true;
    } //-- void setQuantidadeTotal(long) 

    /**
     * Sets the value of field 'valorConsistente'.
     * 
     * @param valorConsistente the value of field 'valorConsistente'
     */
    public void setValorConsistente(double valorConsistente)
    {
        this._valorConsistente = valorConsistente;
        this._has_valorConsistente = true;
    } //-- void setValorConsistente(double) 

    /**
     * Sets the value of field 'valorIncosistente'.
     * 
     * @param valorIncosistente the value of field
     * 'valorIncosistente'.
     */
    public void setValorIncosistente(double valorIncosistente)
    {
        this._valorIncosistente = valorIncosistente;
        this._has_valorIncosistente = true;
    } //-- void setValorIncosistente(double) 

    /**
     * Sets the value of field 'valorTotal'.
     * 
     * @param valorTotal the value of field 'valorTotal'.
     */
    public void setValorTotal(double valorTotal)
    {
        this._valorTotal = valorTotal;
        this._has_valorTotal = true;
    } //-- void setValorTotal(double) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarEstatisticasLoteResponse
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.ConsultarEstatisticasLoteResponse unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.ConsultarEstatisticasLoteResponse) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.ConsultarEstatisticasLoteResponse.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarestatisticaslote.response.ConsultarEstatisticasLoteResponse unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
