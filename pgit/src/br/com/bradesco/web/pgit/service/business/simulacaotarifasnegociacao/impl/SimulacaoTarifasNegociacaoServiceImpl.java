/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.ISimulacaoTarifasNegociacaoService;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ConsultarConfigPadraoAtribTipoServModEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ConsultarConfigPadraoAtribTipoServModSaidaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ConsultarConfigTipoServicoModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ConsultarConfigTipoServicoModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.DetalharSimulacaoTarifaNegociacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.DetalharSimulacaoTarifaNegociacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.EfetuarValidacaoConfigAtribModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.EfetuarValidacaoConfigAtribModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconfigpadraoatribtiposervmod.request.ConsultarConfigPadraoAtribTipoServModRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconfigpadraoatribtiposervmod.response.ConsultarConfigPadraoAtribTipoServModResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconfigtiposervicomodalidade.request.ConsultarConfigTipoServicoModalidadeRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarconfigtiposervicomodalidade.response.ConsultarConfigTipoServicoModalidadeResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.request.ConsultarListaOperacaoTarifaTipoServicoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarlistaoperacaotarifatiposervico.response.ConsultarListaOperacaoTarifaTipoServicoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.EfetuarCalcOperReceitaTarifasRequest;
import br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.response.EfetuarCalcOperReceitaTarifasResponse;
import br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaoconfigatribmodalidade.request.EfetuarValidacaoConfigAtribModalidadeRequest;
import br.com.bradesco.web.pgit.service.data.pdc.efetuarvalidacaoconfigatribmodalidade.response.EfetuarValidacaoConfigAtribModalidadeResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: SimulacaoTarifasNegociacao
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class SimulacaoTarifasNegociacaoServiceImpl implements ISimulacaoTarifasNegociacaoService {

	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.ISimulacaoTarifasNegociacaoService#consultarListaOperacaoTarifaTipoServico(br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoEntradaDTO)
	 */
	public List<ListaOperacaoTarifaTipoServicoSaidaDTO> consultarListaOperacaoTarifaTipoServico(ListaOperacaoTarifaTipoServicoEntradaDTO entradaDTO) {
		
		List<ListaOperacaoTarifaTipoServicoSaidaDTO>  listaRetorno = new ArrayList<ListaOperacaoTarifaTipoServicoSaidaDTO>();
		ConsultarListaOperacaoTarifaTipoServicoRequest request = new ConsultarListaOperacaoTarifaTipoServicoRequest();
		ConsultarListaOperacaoTarifaTipoServicoResponse response = new ConsultarListaOperacaoTarifaTipoServicoResponse();
		
		request.setNumeroOcorrencias(PgitUtil.verificaIntegerNulo(entradaDTO.getNumeroOcorrencias()));
		request.setCdFluxoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFluxoNegocio()));
		request.setCdGrupoInfoFluxo(PgitUtil.verificaIntegerNulo(entradaDTO.getCdGrupoInfoFluxo()));
		request.setCdProdutoOperacaoDeflt(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoOperacaoDeflt()));
		request.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(entradaDTO.getCdOperacaoProdutoServico()));
		request.setCdTipoCanal(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoCanal()));
		request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
		request.setNrOcorGrupoProposta(PgitUtil.verificaIntegerNulo(entradaDTO.getNrOcorGrupoProposta()));
		
		//Campos retirados do pdc e novos campos inseridos conforme solicitado via email por Andre Fernandes Vieira dia 02/05/2011
		
		//request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		//request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		//request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		//request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoOperacaoRelacionado()));
		request.setCdNegocioRenegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdNegocioRenegocio()));
		request.setCdPessoaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaContrato()));
		request.setCdPessoaJuridicaProposta(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaProposta()));
		request.setCdTipoContrato(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContrato()));
		request.setCdTipoContratoProposta(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoProposta()));
		request.setNrSequenciaContrato(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContrato()));
		
		response = getFactoryAdapter().getConsultarListaOperacaoTarifaTipoServicoPDCAdapter().invokeProcess(request);
		ListaOperacaoTarifaTipoServicoSaidaDTO saidaDTO;
		for(int i=0;i<response.getOcorrenciasCount();i++){
			saidaDTO = new ListaOperacaoTarifaTipoServicoSaidaDTO();		
			saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdOperacaoProdutoServico());
			saidaDTO.setDsProdutoServicoOperacao(response.getOcorrencias(i).getDsProdutoServicoOperacao());
			saidaDTO.setCdProdutoOperacaoRelacionado(response.getOcorrencias(i).getCdProdutoOperacaoRelacionado());
			saidaDTO.setDsProdutoOperacaoRelacionado(response.getOcorrencias(i).getDsProdutoOperacaoRelacionado());
			saidaDTO.setCdRelacionamentoProdutoProduto(response.getOcorrencias(i).getCdRelacionamentoProdutoProduto());
			saidaDTO.setCdOperacaoProdutoServico(response.getOcorrencias(i).getCdOperacaoProdutoServico());
			saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
			saidaDTO.setVlrTarifaContratoMinimo(response.getOcorrencias(i).getVlTarifaContratoMinimo());
			saidaDTO.setVlrTarifaContratoMaximo(response.getOcorrencias(i).getVlTarifaContratoMaximo());
			saidaDTO.setVlrTarifaContrato(response.getOcorrencias(i).getVlTarifaContratoNegocio());
			listaRetorno.add(saidaDTO);
			
		}
		return listaRetorno;
	}

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.ISimulacaoTarifasNegociacaoService#detalharSimulacaoTarifaNegociacao(br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.DetalharSimulacaoTarifaNegociacaoEntradaDTO)
	 */
	public DetalharSimulacaoTarifaNegociacaoSaidaDTO detalharSimulacaoTarifaNegociacao(DetalharSimulacaoTarifaNegociacaoEntradaDTO entradaDTO) {
		
		DetalharSimulacaoTarifaNegociacaoSaidaDTO saidaDTO = new  DetalharSimulacaoTarifaNegociacaoSaidaDTO();
		
		EfetuarCalcOperReceitaTarifasRequest request = new EfetuarCalcOperReceitaTarifasRequest();
		
		request.setQtOcorrencias(entradaDTO.getOcorrencias().size());
		request.setVlTarifaTotalPad(entradaDTO.getVlTarifaTotalPad());
		request.setVlTarifaTotalPro(entradaDTO.getVlTarifaTotalPro());
		
		ArrayList<br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias> listaOcorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias>(); 
		
		br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias();
		
		
		br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.OcorrenciasDTO ocorrenciaDTO;
		for (int i = 0; i < entradaDTO.getOcorrencias().size(); i++) {
			 ocorrenciaDTO =  entradaDTO.getOcorrencias().get(i);
			 ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias();
			 ocorrencia.setCdOperacaoProdutoServico(ocorrenciaDTO.getCdOperacaoProdutoServico());
			 ocorrencia.setCdProdutoOperacaoRelacionado(ocorrencia.getCdProdutoOperacaoRelacionado());
			 ocorrencia.setCdProdutoServicoOperacao(ocorrenciaDTO.getCdProdutoServicoOperacao());
			 ocorrencia.setCdRelacionamentoProduto(ocorrenciaDTO.getCdRelacionamentoProduto());
			 ocorrencia.setQtDiasFloat(ocorrenciaDTO.getQtDiasFloat());
			 ocorrencia.setQtEstimada(ocorrenciaDTO.getQtEstimada());
			 ocorrencia.setVlTarifaPadrao(ocorrenciaDTO.getVlTarifaPadrao());
			 ocorrencia.setVlTarifaProposta(ocorrenciaDTO.getVlTarifaProposta());
			 
			 listaOcorrencias.add(ocorrencia);
			 
			
		}
		
		request.setOcorrencias(listaOcorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.efetuarcalcoperreceitatarifas.request.Ocorrencias[0]));
		
		
		EfetuarCalcOperReceitaTarifasResponse response = getFactoryAdapter().getEfetuarCalcOperReceitaTarifasPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setPercentualFlexibilidade(response.getPercentualFlexibilidade());
		saidaDTO.setVlFloat(response.getVlFloat());
		saidaDTO.setVlTarifaDesconto(response.getVlTarifaDesconto());
		saidaDTO.setVlTarifaTotalPad(response.getVlTarifaTotalPad());
		saidaDTO.setVlTarifaTotalPro(response.getVlTarifaTotalPro());
		
		return saidaDTO;
	}
	
	public ConsultarConfigPadraoAtribTipoServModSaidaDTO consultarConfigPadraoAtribTipoServMod(
			ConsultarConfigPadraoAtribTipoServModEntradaDTO entradaDTO) {
		ConsultarConfigPadraoAtribTipoServModRequest request = new ConsultarConfigPadraoAtribTipoServModRequest();

		request.setCdProdutoOperacaoRelacionado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdProdutoOperacaoRelacionado()));
		request.setCdProdutoServicoOperacao(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdRelacionamentoProduto(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdRelacionamentoProduto()));

		ConsultarConfigPadraoAtribTipoServModResponse response = getFactoryAdapter()
				.getConsultarConfigPadraoAtribTipoServModPDCAdapter()
				.invokeProcess(request);

		return new ConsultarConfigPadraoAtribTipoServModSaidaDTO(response);
	}

	public ConsultarConfigTipoServicoModalidadeSaidaDTO consultarConfigTipoServicoModalidade(
			ConsultarConfigTipoServicoModalidadeEntradaDTO entradaDTO) {
		ConsultarConfigTipoServicoModalidadeRequest request = new ConsultarConfigTipoServicoModalidadeRequest();

		request.setCdParametro(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdParametro()));
		request.setCdPessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
		request.setCdProdutoOperacaoRelacionado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdProdutoOperacaoRelacionado()));
		request.setCdProdutoServicoOperacao(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
		request.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
		request.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));

		ConsultarConfigTipoServicoModalidadeResponse response = getFactoryAdapter()
				.getConsultarConfigTipoServicoModalidadePDCAdapter()
				.invokeProcess(request);

		return new ConsultarConfigTipoServicoModalidadeSaidaDTO(response);
	}

	public EfetuarValidacaoConfigAtribModalidadeSaidaDTO efetuarValidacaoConfigAtribModalidade(
			EfetuarValidacaoConfigAtribModalidadeEntradaDTO entradaDTO) {
		EfetuarValidacaoConfigAtribModalidadeRequest request = new EfetuarValidacaoConfigAtribModalidadeRequest();
		
		request.setCdServico(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdServico()));
		request.setCdModalidade(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdModalidade()));
		request.setCdIndicadorModalidade(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdIndicadorModalidade()));
		request.setCdTipoSaldo(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdTipoSaldo()));
		request.setCdTipoProcessamento(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdTipoProcessamento()));
		request.setCdTratamentoFeriadoDataPagto(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdTratamentoFeriadoDataPagto()));
		request.setCdPermissaoFavorecidoConsultaPagto(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdPermissaoFavorecidoConsultaPagto()));
		request
				.setCdTipoRejeicaoAgendamento(PgitUtil
						.verificaIntegerNulo(entradaDTO
								.getCdTipoRejeicaoAgendamento()));
		request.setCdTipoRejeicaoEfetivacao(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoRejeicaoEfetivacao()));
		request.setQtdeMaxRegInconsistenteLote(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getQtdeMaxRegInconsistenteLote()));
		request.setPercentualMaxRegInconsistenteLote(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getPercentualMaxRegInconsistenteLote()));
		request.setCdPrioridadeDebito(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdPrioridadeDebito()));
		request.setCdGeracaoLancamentoFuturoDebito(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdGeracaoLancamentoFuturoDebito()));
		request.setCdUtilizacaoFavorecidoControlePagto(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdUtilizacaoFavorecidoControlePagto()));
		request.setVlrMaximoPagtoFavorecidoNaoCadastrado(PgitUtil
				.verificaBigDecimalNulo(entradaDTO
						.getVlrMaximoPagtoFavorecidoNaoCadastrado()));
		request.setVlrLimiteIndividual(PgitUtil
				.verificaBigDecimalNulo(entradaDTO.getVlrLimiteIndividual()));
		request.setVlrLimiteDiario(PgitUtil.verificaBigDecimalNulo(entradaDTO
				.getVlrLimiteDiario()));
		request.setQtdeDiaRepique(PgitUtil.verificaIntegerNulo(entradaDTO
				.getQtdeDiaRepique()));
		request.setQtdeDiaFloating(PgitUtil.verificaIntegerNulo(entradaDTO
				.getQtdeDiaFloating()));
		request.setCdTipoInscricaoFavorecidoAceita(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdTipoInscricaoFavorecidoAceita()));
		request.setCdOcorrenciaDiaExpiracao(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdOcorrenciaDiaExpiracao()));
		request.setQtdeDiaExpiracao(PgitUtil.verificaIntegerNulo(entradaDTO
				.getQtdeDiaExpiracao()));
		request.setCdGeracaoLancamentoFuturoCredito(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdGeracaoLancamentoFuturoCredito()));
		request.setCdGeracaoLancamentoProgramado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdGeracaoLancamentoProgramado()));
		request.setCdTratoContaTransf(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdTratoContaTransf()));
		request.setCdConsistenciaCpfCnpj(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdConsistenciaCpfCnpj()));
		request.setCdTipoConsistenciaCnpjFavorecido(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdTipoConsistenciaCnpjFavorecido()));
		request.setCdTipoContaCredito(PgitUtil.verificaLongNulo(entradaDTO
				.getCdTipoContaCredito()));
		request.setCdEfetuaConsistenciaEspBeneficiario(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdEfetuaConsistenciaEspBeneficiario()));
		request.setCdUtilizacaoCodigoLancamentoFixo(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdUtilizacaoCodigoLancamentoFixo()));
		request.setCdLancamentoFixo(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdLancamentoFixo()));
		request.setCdPermissaoPagtoVencido(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdPermissaoPagtoVencido()));
		request.setQtdeLimiteDiarioPagtoVencido(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getQtdeLimiteDiarioPagtoVencido()));
		request.setCdPermissaoPagtoMenor(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdPermissaoPagtoMenor()));
		request.setCdTipoConsultaSaldo(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdTipoConsultaSaldo()));
		request.setCdTratamentoFeriado(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdTratamentoFeriado()));
		request.setCdPermissaoContingencia(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdPermissaoContingencia()));
		request.setCdTratamentoValorDivergente(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdTratamentoValorDivergente()));
		request.setCdTipoConsistenciaCnpjProposta(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdTipoConsistenciaCnpjProposta()));
		request.setCdTipoComprovacaoVida(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoComprovacaoVida()));
		request.setQtdeMesesComprovacaoVida(PgitUtil
				.verificaIntegerNulo(entradaDTO.getQtdeMesesComprovacaoVida()));
		request.setCdIndicadorEmissaoComprovanteVida(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdIndicadorEmissaoComprovanteVida()));
		request.setQtdeDiaAvisoComprovanteVidaInicial(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getQtdeDiaAvisoComprovanteVidaInicial()));
		request.setQtdeDiaAvisoComprovanteVidaVencimento(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getQtdeDiaAvisoComprovanteVidaVencimento()));
		request.setCdUtilizacaoMensagemPersonalizada(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdUtilizacaoMensagemPersonalizada()));
		request.setCdDestino(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdDestino()));
		request.setCdCadastroEnderecoUtilizado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdCadastroEnderecoUtilizado()));
		request.setCdutilizacaoFormularioPersonalizado(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdutilizacaoFormularioPersonalizado()));
		request.setCdFormulario(PgitUtil.verificaLongNulo(entradaDTO
				.getCdFormulario()));
		request.setCdCriterioRstrbTitulo(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdCriterioRstrbTitulo()));
		request.setCdRastreabTituloTerc(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdRastreabTituloTerc()));
		request.setCdRastreabNotaFiscal(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdRastreabNotaFiscal()));
		request.setCdCaptuTituloRegistro(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdCaptuTituloRegistro()));
		request.setCdIndicadorAgendaTitulo(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdIndicadorAgendaTitulo()));
		request.setCdAgendaRastreabilidadeFilial(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdAgendaRastreabilidadeFilial()));
		request.setCdBloqueioEmissaoPplta(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdBloqueioEmissaoPplta()));
		request.setDtInicioRastreabTitulo(PgitUtil
				.verificaStringNula(entradaDTO.getDtInicioRastreabTitulo()));
		request.setDtInicioBloqueioPplta(PgitUtil.verificaStringNula(entradaDTO
				.getDtInicioBloqueioPplta()));
		request.setDtInicioRegTitulo(PgitUtil.verificaStringNula(entradaDTO
				.getDtInicioRegTitulo()));
		request.setCdMomentoAvisoRecadastramento(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getCdMomentoAvisoRecadastramento()));
		request.setQtdeAntecipacaoInicioRecadastramento(PgitUtil
				.verificaIntegerNulo(entradaDTO
						.getQtdeAntecipacaoInicioRecadastramento()));
		request.setCdAgruparCorrespondencia(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdAgruparCorrespondencia()));
		request.setCdAdesaoSacado(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdAdesaoSacado()));
		request
				.setCdValidacaoNomeFavorecido(PgitUtil
						.verificaIntegerNulo(entradaDTO
								.getCdValidacaoNomeFavorecido()));
		request.setCdPermissaoDebitoOnline(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdPermissaoDebitoOnline()));
		request.setCdLiberacaoLoteProcs(PgitUtil.verificaIntegerNulo(entradaDTO
				.getCdLiberacaoLoteProcs()));
		request.setCdTipoSaldoValorSuperior(PgitUtil
				.verificaIntegerNulo(entradaDTO.getCdTipoSaldoValorSuperior()));

		EfetuarValidacaoConfigAtribModalidadeResponse response = getFactoryAdapter()
				.getEfetuarValidacaoConfigAtribModalidadePDCAdapter()
				.invokeProcess(request);
		
		return new EfetuarValidacaoConfigAtribModalidadeSaidaDTO(response);
	}
	
	

}

