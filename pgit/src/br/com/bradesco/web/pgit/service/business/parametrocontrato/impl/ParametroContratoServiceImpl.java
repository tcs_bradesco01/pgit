package br.com.bradesco.web.pgit.service.business.parametrocontrato.impl;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.parametrocontrato.IParametroContratoService;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ExcluirParametroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ExcluirParametroContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.IncluirParametroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.IncluirParametroContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ListarParametroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ListarParametroContratoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ListarParametroContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.excluirparametrocontrato.request.ExcluirParametroContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirparametrocontrato.response.ExcluirParametroContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirparametrocontrato.request.IncluirParametroContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirparametrocontrato.response.IncluirParametroContratoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarparametrocontrato.request.ListarParametroContratoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarparametrocontrato.response.ListarParametroContratoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


// TODO: Auto-generated Javadoc
/**
 * The Class ParametroContratoServiceImpl.
 */
public class ParametroContratoServiceImpl implements IParametroContratoService {

	/**
	 * The Constructor.
	 */
	public ParametroContratoServiceImpl() {
		super();
	}

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;

	/**
	 * Get: factoryAdapter.
	 *
	 * @return factoryAdapter
	 */
	public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}

	/* (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.parametrocontrato.IParametroContratoService
	 * #excluirParametroContrato(br.com.bradesco.web.pgit.service.business.parametrocontrato.
	 * bean.ExcluirParametroContratoEntradaDTO)
	 */
	public ExcluirParametroContratoSaidaDTO excluirParametroContrato(ExcluirParametroContratoEntradaDTO entrada) {

		ExcluirParametroContratoRequest request =  new ExcluirParametroContratoRequest();

		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setCdParametro(verificaIntegerNulo(entrada.getCdParametro()));

		ExcluirParametroContratoResponse response = 
			getFactoryAdapter().getExcluirParametroContratoPDCAdapter().invokeProcess(request);

		ExcluirParametroContratoSaidaDTO saida =  new ExcluirParametroContratoSaidaDTO();

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	/* (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.parametrocontrato.IParametroContratoService
	 * #incluirParametroContrato(br.com.bradesco.web.pgit.service.business.parametrocontrato.
	 * bean.IncluirParametroContratoEntradaDTO)
	 */
	public IncluirParametroContratoSaidaDTO incluirParametroContrato(IncluirParametroContratoEntradaDTO entrada) {

		IncluirParametroContratoRequest request =  new IncluirParametroContratoRequest();

		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setCdParametro(verificaIntegerNulo(entrada.getCdParametro()));
		request.setCdIndicadorUtilizaDescricao(verificaIntegerNulo(entrada.getCdIndicadorUtilizaDescricao()));
		request.setDsParametroContrato(PgitUtil.verificaStringNula(entrada.getDsParametroContrato()));

		IncluirParametroContratoResponse response = 
			getFactoryAdapter().getIncluirParametroContratoPDCAdapter().invokeProcess(request);  

		IncluirParametroContratoSaidaDTO saida =  new IncluirParametroContratoSaidaDTO();

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());

		return saida;
	}

	/* (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.business.parametrocontrato.IParametroContratoService
	 * #listarParametroContrato(br.com.bradesco.web.pgit.service.business.parametrocontrato.
	 * bean.ListarParametroContratoEntradaDTO)
	 */
	public ListarParametroContratoSaidaDTO listarParametroContrato(ListarParametroContratoEntradaDTO entrada) {

		ListarParametroContratoRequest request = new ListarParametroContratoRequest();

		request.setNrMaximoOcorrencias(verificaIntegerNulo(entrada.getNrMaximoOcorrencias()));
		request.setCdTipoContratoNegocio(verificaIntegerNulo(entrada.getCdTipoContratoNegocio()));
		request.setCdpessoaJuridicaContrato(verificaLongNulo(entrada.getCdpessoaJuridicaContrato()));
		request.setNrSequenciaContratoNegocio(verificaLongNulo(entrada.getNrSequenciaContratoNegocio()));

		ListarParametroContratoResponse response = 
			getFactoryAdapter().getListarParametroContratoPDCAdapter().invokeProcess(request);  


		ListarParametroContratoSaidaDTO saida = new ListarParametroContratoSaidaDTO();

		saida.setCodMensagem(response.getCodMensagem());
		saida.setMensagem(response.getMensagem());
		saida.setNrLinhas(response.getNrLinhas());

		List<ListarParametroContratoOcorrenciasSaidaDTO> listParametroContratoOcorrencias = 
			new ArrayList<ListarParametroContratoOcorrenciasSaidaDTO>(response.getNrLinhas());

		for (int i = 0; i < response.getNrLinhas(); i++) {
			ListarParametroContratoOcorrenciasSaidaDTO ocorrencia = new ListarParametroContratoOcorrenciasSaidaDTO();

			ocorrencia.setCdParametro(response.getOcorrencias(i).getCdParametro());
			ocorrencia.setDsParametro(response.getOcorrencias(i).getDsParametro());
			ocorrencia.setDsParametroContrato(response.getOcorrencias(i).getDsParametroContrato());
			ocorrencia.setCdUsuarioInclusao(response.getOcorrencias(i).getCdUsuarioInclusao());
			ocorrencia.setHrInclusaoRegistro(response.getOcorrencias(i).getHrInclusaoRegistro());
			ocorrencia.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
			ocorrencia.setDsTipoCanal(response.getOcorrencias(i).getDsTipoCanal());

			listParametroContratoOcorrencias.add(ocorrencia);
		}

		saida.setOcorrencias(listParametroContratoOcorrencias);

		return saida;
	}
}