/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import br.com.bradesco.web.pgit.service.data.pdc.listarempresagestora.response.Ocorrencias;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: EmpresaGestoraSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EmpresaGestoraSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo dsCnpj. */
	private String dsCnpj;
	
	/** Atributo codDescEmpresaGestora. */
	private String codDescEmpresaGestora;
	
	/**
	 * Empresa gestora saida dto.
	 */
	public EmpresaGestoraSaidaDTO() {
	}

	/**
	 * Empresa gestora saida dto.
	 *
	 * @param saida the saida
	 */
	public EmpresaGestoraSaidaDTO(Ocorrencias saida){
		this.cdPessoaJuridicaContrato = saida.getCdPessoaJuridicaContrato();
		this.dsCnpj = PgitUtil.concatenarCampos(saida.getCdPessoaJuridicaContrato(), saida.getDsCnpj());
		
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsCnpj.
	 *
	 * @return dsCnpj
	 */
	public String getDsCnpj() {
		return dsCnpj;
	}

	/**
	 * Set: dsCnpj.
	 *
	 * @param dsCnpj the ds cnpj
	 */
	public void setDsCnpj(String dsCnpj) {
		this.dsCnpj = dsCnpj;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: codDescEmpresaGestora.
	 *
	 * @return codDescEmpresaGestora
	 */
	public String getCodDescEmpresaGestora() {
		return codDescEmpresaGestora;
	}

	/**
	 * Set: codDescEmpresaGestora.
	 *
	 * @param codDescEmpresaGestora the cod desc empresa gestora
	 */
	public void setCodDescEmpresaGestora(String codDescEmpresaGestora) {
		this.codDescEmpresaGestora = codDescEmpresaGestora;
	}
	
	

}
