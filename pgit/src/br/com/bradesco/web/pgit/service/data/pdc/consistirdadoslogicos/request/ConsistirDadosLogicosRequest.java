/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consistirdadoslogicos.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsistirDadosLogicosRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsistirDadosLogicosRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdCnpjCpf
     */
    private long _cdCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdCnpjCpf
     */
    private boolean _has_cdCnpjCpf;

    /**
     * Field _cdFilialCnpjCpf
     */
    private int _cdFilialCnpjCpf = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjCpf
     */
    private boolean _has_cdFilialCnpjCpf;

    /**
     * Field _cdCtrlCnpjCPf
     */
    private int _cdCtrlCnpjCPf = 0;

    /**
     * keeps track of state for field: _cdCtrlCnpjCPf
     */
    private boolean _has_cdCtrlCnpjCPf;

    /**
     * Field _dsRazaoSocial
     */
    private java.lang.String _dsRazaoSocial;

    /**
     * Field _cdJuncaoAgencia
     */
    private int _cdJuncaoAgencia = 0;

    /**
     * keeps track of state for field: _cdJuncaoAgencia
     */
    private boolean _has_cdJuncaoAgencia;

    /**
     * Field _cdContaCorrente
     */
    private int _cdContaCorrente = 0;

    /**
     * keeps track of state for field: _cdContaCorrente
     */
    private boolean _has_cdContaCorrente;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdConveCtaSalarial
     */
    private long _cdConveCtaSalarial = 0;

    /**
     * keeps track of state for field: _cdConveCtaSalarial
     */
    private boolean _has_cdConveCtaSalarial;

    /**
     * Field _cdIndicadorPrevidencia
     */
    private java.lang.String _cdIndicadorPrevidencia;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsistirDadosLogicosRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consistirdadoslogicos.request.ConsistirDadosLogicosRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCnpjCpf
     * 
     */
    public void deleteCdCnpjCpf()
    {
        this._has_cdCnpjCpf= false;
    } //-- void deleteCdCnpjCpf() 

    /**
     * Method deleteCdContaCorrente
     * 
     */
    public void deleteCdContaCorrente()
    {
        this._has_cdContaCorrente= false;
    } //-- void deleteCdContaCorrente() 

    /**
     * Method deleteCdConveCtaSalarial
     * 
     */
    public void deleteCdConveCtaSalarial()
    {
        this._has_cdConveCtaSalarial= false;
    } //-- void deleteCdConveCtaSalarial() 

    /**
     * Method deleteCdCtrlCnpjCPf
     * 
     */
    public void deleteCdCtrlCnpjCPf()
    {
        this._has_cdCtrlCnpjCPf= false;
    } //-- void deleteCdCtrlCnpjCPf() 

    /**
     * Method deleteCdFilialCnpjCpf
     * 
     */
    public void deleteCdFilialCnpjCpf()
    {
        this._has_cdFilialCnpjCpf= false;
    } //-- void deleteCdFilialCnpjCpf() 

    /**
     * Method deleteCdJuncaoAgencia
     * 
     */
    public void deleteCdJuncaoAgencia()
    {
        this._has_cdJuncaoAgencia= false;
    } //-- void deleteCdJuncaoAgencia() 

    /**
     * Returns the value of field 'cdCnpjCpf'.
     * 
     * @return long
     * @return the value of field 'cdCnpjCpf'.
     */
    public long getCdCnpjCpf()
    {
        return this._cdCnpjCpf;
    } //-- long getCdCnpjCpf() 

    /**
     * Returns the value of field 'cdContaCorrente'.
     * 
     * @return int
     * @return the value of field 'cdContaCorrente'.
     */
    public int getCdContaCorrente()
    {
        return this._cdContaCorrente;
    } //-- int getCdContaCorrente() 

    /**
     * Returns the value of field 'cdConveCtaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdConveCtaSalarial'.
     */
    public long getCdConveCtaSalarial()
    {
        return this._cdConveCtaSalarial;
    } //-- long getCdConveCtaSalarial() 

    /**
     * Returns the value of field 'cdCtrlCnpjCPf'.
     * 
     * @return int
     * @return the value of field 'cdCtrlCnpjCPf'.
     */
    public int getCdCtrlCnpjCPf()
    {
        return this._cdCtrlCnpjCPf;
    } //-- int getCdCtrlCnpjCPf() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdFilialCnpjCpf'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjCpf'.
     */
    public int getCdFilialCnpjCpf()
    {
        return this._cdFilialCnpjCpf;
    } //-- int getCdFilialCnpjCpf() 

    /**
     * Returns the value of field 'cdIndicadorPrevidencia'.
     * 
     * @return String
     * @return the value of field 'cdIndicadorPrevidencia'.
     */
    public java.lang.String getCdIndicadorPrevidencia()
    {
        return this._cdIndicadorPrevidencia;
    } //-- java.lang.String getCdIndicadorPrevidencia() 

    /**
     * Returns the value of field 'cdJuncaoAgencia'.
     * 
     * @return int
     * @return the value of field 'cdJuncaoAgencia'.
     */
    public int getCdJuncaoAgencia()
    {
        return this._cdJuncaoAgencia;
    } //-- int getCdJuncaoAgencia() 

    /**
     * Returns the value of field 'dsRazaoSocial'.
     * 
     * @return String
     * @return the value of field 'dsRazaoSocial'.
     */
    public java.lang.String getDsRazaoSocial()
    {
        return this._dsRazaoSocial;
    } //-- java.lang.String getDsRazaoSocial() 

    /**
     * Method hasCdCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCnpjCpf()
    {
        return this._has_cdCnpjCpf;
    } //-- boolean hasCdCnpjCpf() 

    /**
     * Method hasCdContaCorrente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaCorrente()
    {
        return this._has_cdContaCorrente;
    } //-- boolean hasCdContaCorrente() 

    /**
     * Method hasCdConveCtaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConveCtaSalarial()
    {
        return this._has_cdConveCtaSalarial;
    } //-- boolean hasCdConveCtaSalarial() 

    /**
     * Method hasCdCtrlCnpjCPf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCtrlCnpjCPf()
    {
        return this._has_cdCtrlCnpjCPf;
    } //-- boolean hasCdCtrlCnpjCPf() 

    /**
     * Method hasCdFilialCnpjCpf
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjCpf()
    {
        return this._has_cdFilialCnpjCpf;
    } //-- boolean hasCdFilialCnpjCpf() 

    /**
     * Method hasCdJuncaoAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdJuncaoAgencia()
    {
        return this._has_cdJuncaoAgencia;
    } //-- boolean hasCdJuncaoAgencia() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCnpjCpf'.
     * 
     * @param cdCnpjCpf the value of field 'cdCnpjCpf'.
     */
    public void setCdCnpjCpf(long cdCnpjCpf)
    {
        this._cdCnpjCpf = cdCnpjCpf;
        this._has_cdCnpjCpf = true;
    } //-- void setCdCnpjCpf(long) 

    /**
     * Sets the value of field 'cdContaCorrente'.
     * 
     * @param cdContaCorrente the value of field 'cdContaCorrente'.
     */
    public void setCdContaCorrente(int cdContaCorrente)
    {
        this._cdContaCorrente = cdContaCorrente;
        this._has_cdContaCorrente = true;
    } //-- void setCdContaCorrente(int) 

    /**
     * Sets the value of field 'cdConveCtaSalarial'.
     * 
     * @param cdConveCtaSalarial the value of field
     * 'cdConveCtaSalarial'.
     */
    public void setCdConveCtaSalarial(long cdConveCtaSalarial)
    {
        this._cdConveCtaSalarial = cdConveCtaSalarial;
        this._has_cdConveCtaSalarial = true;
    } //-- void setCdConveCtaSalarial(long) 

    /**
     * Sets the value of field 'cdCtrlCnpjCPf'.
     * 
     * @param cdCtrlCnpjCPf the value of field 'cdCtrlCnpjCPf'.
     */
    public void setCdCtrlCnpjCPf(int cdCtrlCnpjCPf)
    {
        this._cdCtrlCnpjCPf = cdCtrlCnpjCPf;
        this._has_cdCtrlCnpjCPf = true;
    } //-- void setCdCtrlCnpjCPf(int) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdFilialCnpjCpf'.
     * 
     * @param cdFilialCnpjCpf the value of field 'cdFilialCnpjCpf'.
     */
    public void setCdFilialCnpjCpf(int cdFilialCnpjCpf)
    {
        this._cdFilialCnpjCpf = cdFilialCnpjCpf;
        this._has_cdFilialCnpjCpf = true;
    } //-- void setCdFilialCnpjCpf(int) 

    /**
     * Sets the value of field 'cdIndicadorPrevidencia'.
     * 
     * @param cdIndicadorPrevidencia the value of field
     * 'cdIndicadorPrevidencia'.
     */
    public void setCdIndicadorPrevidencia(java.lang.String cdIndicadorPrevidencia)
    {
        this._cdIndicadorPrevidencia = cdIndicadorPrevidencia;
    } //-- void setCdIndicadorPrevidencia(java.lang.String) 

    /**
     * Sets the value of field 'cdJuncaoAgencia'.
     * 
     * @param cdJuncaoAgencia the value of field 'cdJuncaoAgencia'.
     */
    public void setCdJuncaoAgencia(int cdJuncaoAgencia)
    {
        this._cdJuncaoAgencia = cdJuncaoAgencia;
        this._has_cdJuncaoAgencia = true;
    } //-- void setCdJuncaoAgencia(int) 

    /**
     * Sets the value of field 'dsRazaoSocial'.
     * 
     * @param dsRazaoSocial the value of field 'dsRazaoSocial'.
     */
    public void setDsRazaoSocial(java.lang.String dsRazaoSocial)
    {
        this._dsRazaoSocial = dsRazaoSocial;
    } //-- void setDsRazaoSocial(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsistirDadosLogicosRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consistirdadoslogicos.request.ConsistirDadosLogicosRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consistirdadoslogicos.request.ConsistirDadosLogicosRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consistirdadoslogicos.request.ConsistirDadosLogicosRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consistirdadoslogicos.request.ConsistirDadosLogicosRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
