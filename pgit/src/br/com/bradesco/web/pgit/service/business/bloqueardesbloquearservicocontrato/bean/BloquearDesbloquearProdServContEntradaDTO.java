/*
 * Nome: br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean;

import java.util.ArrayList;
import java.util.List;


/**
 * Nome: BloquearDesbloquearProdServContEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class BloquearDesbloquearProdServContEntradaDTO {
   
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo cdSituacaoServicoRelacionado. */
	private Integer cdSituacaoServicoRelacionado;
	
	/** Atributo cdMotivoServicoRelacionado. */
	private Integer cdMotivoServicoRelacionado;
	
	/** Atributo cdParametro. */
	private Integer cdParametro;
	
	/** Atributo listaOcorrencias. */
	private List<OcorrenciaBloquearDesbloquearProdServContDTO> listaOcorrencias = new ArrayList<OcorrenciaBloquearDesbloquearProdServContDTO>();
	
	
	/**
	 * Get: cdMotivoServicoRelacionado.
	 *
	 * @return cdMotivoServicoRelacionado
	 */
	public Integer getCdMotivoServicoRelacionado() {
		return cdMotivoServicoRelacionado;
	}
	
	/**
	 * Set: cdMotivoServicoRelacionado.
	 *
	 * @param cdMotivoServicoRelacionado the cd motivo servico relacionado
	 */
	public void setCdMotivoServicoRelacionado(Integer cdMotivoServicoRelacionado) {
		this.cdMotivoServicoRelacionado = cdMotivoServicoRelacionado;
	}
	
	/**
	 * Get: cdParametro.
	 *
	 * @return cdParametro
	 */
	public Integer getCdParametro() {
		return cdParametro;
	}
	
	/**
	 * Set: cdParametro.
	 *
	 * @param cdParametro the cd parametro
	 */
	public void setCdParametro(Integer cdParametro) {
		this.cdParametro = cdParametro;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdSituacaoServicoRelacionado.
	 *
	 * @return cdSituacaoServicoRelacionado
	 */
	public Integer getCdSituacaoServicoRelacionado() {
		return cdSituacaoServicoRelacionado;
	}
	
	/**
	 * Set: cdSituacaoServicoRelacionado.
	 *
	 * @param cdSituacaoServicoRelacionado the cd situacao servico relacionado
	 */
	public void setCdSituacaoServicoRelacionado(Integer cdSituacaoServicoRelacionado) {
		this.cdSituacaoServicoRelacionado = cdSituacaoServicoRelacionado;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: listaOcorrencias.
	 *
	 * @return listaOcorrencias
	 */
	public List<OcorrenciaBloquearDesbloquearProdServContDTO> getListaOcorrencias() {
		return listaOcorrencias;
	}
	
	/**
	 * Set: listaOcorrencias.
	 *
	 * @param listaOcorrencias the lista ocorrencias
	 */
	public void setListaOcorrencias(
			List<OcorrenciaBloquearDesbloquearProdServContDTO> listaOcorrencias) {
		this.listaOcorrencias = listaOcorrencias;
	}
	
	
	
}
