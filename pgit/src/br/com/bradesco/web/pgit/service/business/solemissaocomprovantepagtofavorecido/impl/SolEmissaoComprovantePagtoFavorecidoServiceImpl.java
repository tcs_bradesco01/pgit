/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.ISolEmissaoComprovantePagtoFavorecidoService;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.ISolEmissaoComprovantePagtoFavorecidoServiceConstants;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarPagtoSolEmiCompFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarPagtoSolEmiCompFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarSolEmiComprovanteFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.DetalharSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.DetalharSolEmiComprovanteFavorecidoOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.DetalharSolEmiComprovanteFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ExcluirSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ExcluirSolEmiComprovanteFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.IncluirSolEmiComprovanteFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.IncluirSolEmiComprovanteFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosolemicompfavorecido.request.ConsultarPagtoSolEmiCompFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarpagtosolemicompfavorecido.response.ConsultarPagtoSolEmiCompFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantefavorecido.request.ConsultarSolEmiComprovanteFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.consultarsolemicomprovantefavorecido.response.ConsultarSolEmiComprovanteFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemicomprovantefavorecido.request.DetalharSolEmiComprovanteFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolemicomprovantefavorecido.response.DetalharSolEmiComprovanteFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantefavorecido.request.ExcluirSolEmiComprovanteFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantefavorecido.response.ExcluirSolEmiComprovanteFavorecidoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantefavorecido.request.IncluirSolEmiComprovanteFavorecidoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantefavorecido.response.IncluirSolEmiComprovanteFavorecidoResponse;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: SolEmissaoComprovantePagtoFavorecido
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class SolEmissaoComprovantePagtoFavorecidoServiceImpl implements ISolEmissaoComprovantePagtoFavorecidoService {
	
	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}
	
	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	/**
     * Construtor.
     */
    public SolEmissaoComprovantePagtoFavorecidoServiceImpl() {
        // TODO: Implementa��o
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.ISolEmissaoComprovantePagtoFavorecidoService#consultarSolEmiComprovanteFavorecido(br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarSolEmiComprovanteFavorecidoEntradaDTO)
     */
    public List<ConsultarSolEmiComprovanteFavorecidoSaidaDTO> consultarSolEmiComprovanteFavorecido(ConsultarSolEmiComprovanteFavorecidoEntradaDTO entradaDTO){
    	List<ConsultarSolEmiComprovanteFavorecidoSaidaDTO> listaSaida = new ArrayList<ConsultarSolEmiComprovanteFavorecidoSaidaDTO>();
    	ConsultarSolEmiComprovanteFavorecidoRequest request = new ConsultarSolEmiComprovanteFavorecidoRequest();
    	ConsultarSolEmiComprovanteFavorecidoResponse response = new ConsultarSolEmiComprovanteFavorecidoResponse();
    	
    	request.setNrOcorrencias(ISolEmissaoComprovantePagtoFavorecidoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR);
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setDtInicioSolicitacao(PgitUtil.verificaStringNula(entradaDTO.getDtInicioSolicitacao()));
    	request.setDtFimSolicitacao(PgitUtil.verificaStringNula(entradaDTO.getDtFimSolicitacao()));
    	request.setCdSituacaoSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSituacaoSolicitacao()));
    	
    	response = getFactoryAdapter().getConsultarSolEmiComprovanteFavorecidoPDCAdapter().invokeProcess(request);
    	
    	ConsultarSolEmiComprovanteFavorecidoSaidaDTO saida;
    	
    	for(int i=0;i<response.getOcorrenciasCount(); i++){
    		saida = new ConsultarSolEmiComprovanteFavorecidoSaidaDTO();
    		
    		saida.setCodMensagem(response.getCodMensagem());
    		saida.setMensagem(response.getMensagem());
    		
    		saida.setCdTipoSolicitacao(response.getOcorrencias(i).getCdTipoSolicitacao());
    		saida.setNrSolicitacao(response.getOcorrencias(i).getNrSolicitacao());
    		saida.setDtHoraSolicitacao(response.getOcorrencias(i).getDtHoraSolicitacao());
    		saida.setDtHoraAtendimento(response.getOcorrencias(i).getDtHoraAtendimento());
    		saida.setCdSituacaoSolicitacao(response.getOcorrencias(i).getCdSituacaoSolicitacao());
    		saida.setDsSituacaoSolicitacao(response.getOcorrencias(i).getDsSituacaoSolicitacao());
    		saida.setCdMotivoSituacao(response.getOcorrencias(i).getCdMotivoSituacao());
    		saida.setDsMotivoSituacao(response.getOcorrencias(i).getDsMotivoSituacao());
    		saida.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
    		saida.setDsTipoCanal(response.getOcorrencias(i).getDsTipoCanal());
    		saida.setCdPessoaJuridica(response.getOcorrencias(i).getCdPessoaJuridica());
    		saida.setCdTipoContrato(response.getOcorrencias(i).getCdTipoContrato());
    		saida.setNrSequenciaContrato(response.getOcorrencias(i).getNrSequenciaContrato());
    		saida.setCdUsuarioInclusao(response.getOcorrencias(i).getCdUsuarioInclusao());
    		saida.setDtHoraSolicitacaoFormatada(FormatarData.formatarDataTrilha(response.getOcorrencias(i).getDtHoraSolicitacao()));
    		saida.setSituacaoSolicitacao(PgitUtil.concatenarCampos(response.getOcorrencias(i).getCdSituacaoSolicitacao(), response.getOcorrencias(i).getDsSituacaoSolicitacao()));
    		saida.setMotivoSituacao(PgitUtil.concatenarCampos(response.getOcorrencias(i).getCdMotivoSituacao(), response.getOcorrencias(i).getDsMotivoSituacao()));
    		
    		listaSaida.add(saida);
    	}
    	
    	return listaSaida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.ISolEmissaoComprovantePagtoFavorecidoService#excluirSolEmiComprovanteFavorecido(br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ExcluirSolEmiComprovanteFavorecidoEntradaDTO)
     */
    public ExcluirSolEmiComprovanteFavorecidoSaidaDTO excluirSolEmiComprovanteFavorecido(ExcluirSolEmiComprovanteFavorecidoEntradaDTO entradaDTO){
    	ExcluirSolEmiComprovanteFavorecidoSaidaDTO saidaDTO = new ExcluirSolEmiComprovanteFavorecidoSaidaDTO();
    	ExcluirSolEmiComprovanteFavorecidoRequest request = new ExcluirSolEmiComprovanteFavorecidoRequest();
    	ExcluirSolEmiComprovanteFavorecidoResponse response = new ExcluirSolEmiComprovanteFavorecidoResponse();
    	
    	request.setCdSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacaoPagamentoIntegrado()));
    	request.setNrSolicitacaoPagamentoIntegrado(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSolicitacaoPagamentoIntegrado()));
        request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setNrRegistrosEntrada(PgitUtil.verificaIntegerNulo(entradaDTO.getComprovantes().size()));
        
        
        ArrayList<br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantefavorecido.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantefavorecido.request.Ocorrencias>();
       
    	for (int i=0; i<entradaDTO.getComprovantes().size(); i++){
    		br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantefavorecido.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantefavorecido.request.Ocorrencias();
    		ocorrencia.setCdTipoCanal(entradaDTO.getComprovantes().get(i).getCdTipoCanal());
    		ocorrencia.setCdControlePagamento(entradaDTO.getComprovantes().get(i).getCdControlePagamento());
     	   
    		ocorrencias.add(ocorrencia);
     	}
    	
    	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantefavorecido.request.Ocorrencias[]) ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.excluirsolemicomprovantefavorecido.request.Ocorrencias[0]));
    	
        response = getFactoryAdapter().getExcluirSolEmiComprovanteFavorecidoPDCAdapter().invokeProcess(request);
        
    	saidaDTO.setCodMensagem(response.getCodMensagem());
	    saidaDTO.setMensagem(response.getMensagem());
	    
	    return saidaDTO;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.ISolEmissaoComprovantePagtoFavorecidoService#incluirSolEmiComprovanteFavorecido(br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.IncluirSolEmiComprovanteFavorecidoEntradaDTO)
     */
    public IncluirSolEmiComprovanteFavorecidoSaidaDTO incluirSolEmiComprovanteFavorecido(IncluirSolEmiComprovanteFavorecidoEntradaDTO entradaDTO){
    	IncluirSolEmiComprovanteFavorecidoSaidaDTO saidaDTO = new IncluirSolEmiComprovanteFavorecidoSaidaDTO();
    	IncluirSolEmiComprovanteFavorecidoRequest request = new IncluirSolEmiComprovanteFavorecidoRequest();
    	IncluirSolEmiComprovanteFavorecidoResponse response = new IncluirSolEmiComprovanteFavorecidoResponse();
    	
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
        request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
        request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
        request.setDtInicioPeriodoMovimentacao(PgitUtil.verificaStringNula(entradaDTO.getDtInicioPeriodoMovimentacao()));
        request.setDtFimPeriodoMovimentacao(PgitUtil.verificaStringNula(entradaDTO.getDtFimPeriodoMovimentacao()));
        request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
        request.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoOperacaoRelacionado()));
        request.setCdRecebedorCredito(PgitUtil.verificaLongNulo(entradaDTO.getCdRecebedorCredito()));
        request.setCdTipoInscricaoRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoInscricaoRecebedor()));
        request.setCdCpfCnpjRecebedor(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjRecebedor()));
        request.setCdFilialCnpjRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjRecebedor()));
        request.setCdControleCpfRecebedor(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCpfRecebedor()));
        request.setCdDestinoCorrespSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDestinoCorrespSolicitacao()));
        request.setCdTipoPostagemSolicitacao(PgitUtil.verificaStringNula(entradaDTO.getCdTipoPostagemSolicitacao()));
        request.setDsLogradouroPagador(PgitUtil.verificaStringNula(entradaDTO.getDsLogradouroPagador()));
        request.setDsNumeroLogradouroPagador(PgitUtil.verificaStringNula(entradaDTO.getDsNumeroLogradouroPagador()));
        request.setDsComplementoLogradouroPagador(PgitUtil.verificaStringNula(entradaDTO.getDsComplementoLogradouroPagador()));
        request.setDsBairroClientePagador(PgitUtil.verificaStringNula(entradaDTO.getDsBairroClientePagador()));
        request.setDsMunicipioClientePagador(PgitUtil.verificaStringNula(entradaDTO.getDsMunicipioClientePagador()));
        request.setCdSiglaUfPagador(PgitUtil.verificaStringNula(entradaDTO.getCdSiglaUfPagador()));
        request.setCdCepPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCepPagador()));
        request.setCdCepComplementoPagador(PgitUtil.verificaIntegerNulo(entradaDTO.getCdCepComplementoPagador()));
        request.setDsEmailClientePagador(PgitUtil.verificaStringNula(entradaDTO.getDsEmailClientePagador()));
        request.setCdPessoaJuridicaDepto(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaDepto()));
        request.setNrSequenciaUnidadeDepto(PgitUtil.verificaIntegerNulo(entradaDTO.getNrSequenciaUnidadeDepto()));
        request.setCdPercentualBonifTarifaSolicitacao(PgitUtil.verificaBigDecimalNulo(entradaDTO.getCdPercentualBonifTarifaSolicitacao()));
        request.setVlTarifaNegocioSolicitacao(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlTarifaNegocioSolicitacao()));
        request.setNrOcorrencias(entradaDTO.getComprovantes().size());
        request.setCdDepartamentoUnidade(PgitUtil.verificaIntegerNulo(entradaDTO.getCdDepartamentoUnidade()));
        
        ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantefavorecido.request.Ocorrencias> ocorrencias = new ArrayList<br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantefavorecido.request.Ocorrencias>();

    	for (int i=0; i<entradaDTO.getComprovantes().size(); i++){
    		br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantefavorecido.request.Ocorrencias ocorrencia = new br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantefavorecido.request.Ocorrencias();
    		ocorrencia.setCdTipoCanal(entradaDTO.getComprovantes().get(i).getCdTipoCanal());
    		ocorrencia.setCdControlePagamento(entradaDTO.getComprovantes().get(i).getCdControlePagamento());
     	    ocorrencia.setCdModalidadePagamentoCliente(entradaDTO.getComprovantes().get(i).getCdModalidadePagamentoCliente());
    		ocorrencias.add(ocorrencia);
     	}
    	
    	request.setOcorrencias((br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantefavorecido.request.Ocorrencias[]) ocorrencias.toArray(new br.com.bradesco.web.pgit.service.data.pdc.incluirsolemicomprovantefavorecido.request.Ocorrencias[0]));
    	
    	response = getFactoryAdapter().getIncluirSolEmiComprovanteFavorecidoPDCAdapter().invokeProcess(request);
    	
    	saidaDTO.setCodMensagem(response.getCodMensagem());
	    saidaDTO.setMensagem(response.getMensagem());
	    
    	return saidaDTO;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.ISolEmissaoComprovantePagtoFavorecidoService#detalharSolEmiComprovanteFavorecido(br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.DetalharSolEmiComprovanteFavorecidoEntradaDTO)
     */
    public DetalharSolEmiComprovanteFavorecidoSaidaDTO detalharSolEmiComprovanteFavorecido(DetalharSolEmiComprovanteFavorecidoEntradaDTO entradaDTO){
    	DetalharSolEmiComprovanteFavorecidoSaidaDTO saidaDTO = new DetalharSolEmiComprovanteFavorecidoSaidaDTO();
    	DetalharSolEmiComprovanteFavorecidoRequest request = new DetalharSolEmiComprovanteFavorecidoRequest();
    	DetalharSolEmiComprovanteFavorecidoResponse response = new DetalharSolEmiComprovanteFavorecidoResponse();
    	
    	request.setNrOcorrencias(ISolEmissaoComprovantePagtoFavorecidoServiceConstants.NUMERO_OCORRENCIAS_DETALHAR);
    	request.setCdPessoaJuridicaEmpr(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaEmpr()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setCdTipoSolicitacaoPagamento(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoSolicitacaoPagamento()));
    	request.setCdSolicitacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSolicitacao()));
    	
    	response = getFactoryAdapter().getDetalharSolEmiComprovanteFavorecidoPDCAdapter().invokeProcess(request);
		
		saidaDTO.setCodMensagem(response.getCodMensagem());
		saidaDTO.setMensagem(response.getMensagem());
		saidaDTO.setCdSolicitacaoPagamentoIntegrado(response.getCdSolicitacaoPagamentoIntegrado());
	    saidaDTO.setNrSolicitacaoPagamentoIntegrado(response.getNrSolicitacaoPagamentoIntegrado());
	    saidaDTO.setCdSituacaoSolicitacao(response.getCdSituacaoSolicitacao());
		saidaDTO.setDsSituacaoSolicitacao(response.getDsSituacaoSolicitacao());
	    saidaDTO.setDtInicioPeriodoMovimentacao(DateUtils.formartarDataPDCparaPadraPtBr(response.getDtInicioPeriodoMovimentacao(), "dd.MM.yyyy", "dd/MM/yyyy"));
	    saidaDTO.setDtFimPeriodoMovimentacao(DateUtils.formartarDataPDCparaPadraPtBr(response.getDtFimPeriodoMovimentacao(), "dd.MM.yyyy", "dd/MM/yyyy"));
	    saidaDTO.setCdProdutoServicoOperacao(response.getCdProdutoServicoOperacao());
	    saidaDTO.setDsProdutoServicoOperacao(response.getDsProdutoServicoOperacao());
	    saidaDTO.setCdProdutoOperacaoRelacionado(response.getCdProdutoOperacaoRelacionado());
	    saidaDTO.setDsProdutoOperacaoRelacionado(response.getDsProdutoOperacaoRelacionado());
	    saidaDTO.setCdRecebedorCredito(response.getCdRecebedorCredito());
	    saidaDTO.setCdTipoInscricaoRecebedor(response.getCdTipoInscricaoRecebedor());
	    saidaDTO.setCdCpfCnpjRecebedor(response.getCdCpfCnpjRecebedor());
	    saidaDTO.setCdFilialCnpjRecebedor(response.getCdFilialCnpjRecebedor());
	    saidaDTO.setCdControleCpfRecebedor(response.getCdControleCpfRecebedor());
	    saidaDTO.setCdDestinoCorrespSolicitacao(response.getCdDestinoCorrespSolicitacao());
	    saidaDTO.setCdTipoPostagemSolicitacao(response.getCdTipoPostagemSolicitacao());
	    saidaDTO.setDsLogradouroPagador(response.getDsLogradouroPagador());
	    saidaDTO.setDsNumeroLogradouroPagador(response.getDsNumeroLogradouroPagador());
	    saidaDTO.setDsComplementoLogradouroPagador(response.getDsComplementoLogradouroPagador());
	    saidaDTO.setDsBairroClientePagador(response.getDsBairroClientePagador());
	    saidaDTO.setDsMunicipioClientePagador(response.getDsMunicipioClientePagador());
	    saidaDTO.setCdSiglaUfPagador(response.getCdSiglaUfPagador());
	    saidaDTO.setCdCepPagador(response.getCdCepPagador());
	    saidaDTO.setCdCepComplementoPagador(response.getCdCepComplementoPagador());
	    saidaDTO.setDsEmailClientePagador(response.getDsEmailClientePagador());
	    saidaDTO.setCdAgenciaOperadora(response.getCdAgenciaOperadora());
	    saidaDTO.setDsAgencia(response.getDsAgencia());
	    saidaDTO.setCdDepartamentoUnidade(response.getCdDepartamentoUnidade());
	    saidaDTO.setVlTarifa(response.getVlTarifa());
	    saidaDTO.setCdPercentualDescTarifa(response.getCdPercentualDescTarifa());
	    saidaDTO.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
	    saidaDTO.setHrInclusaoRegistro(response.getHrInclusaoRegistro());
	    saidaDTO.setCdOperacaoCanalInclusao(response.getCdOperacaoCanalInclusao().equals("0") ? "" :  response.getCdOperacaoCanalInclusao());
	    saidaDTO.setCdTipoCanalInclusao(response.getCdTipoCanalInclusao());
	    saidaDTO.setDsTipoCanalInclusao(response.getDsTipoCanalInclusao());
	    saidaDTO.setNrLinhas(response.getOcorrenciasCount());
	    
	    saidaDTO.setCepFormatado(PgitUtil.formatCep(response.getCdCepPagador(), response.getCdCepComplementoPagador()));
	    
	    
	    List<DetalharSolEmiComprovanteFavorecidoOcorrenciasDTO> listaComprovantes = new ArrayList<DetalharSolEmiComprovanteFavorecidoOcorrenciasDTO>();
	    DetalharSolEmiComprovanteFavorecidoOcorrenciasDTO comprovante;
	    
    	for(int i=0; i<response.getNrLinhas(); i++){
    		comprovante = new DetalharSolEmiComprovanteFavorecidoOcorrenciasDTO();
    		
    		comprovante.setCdTipoCanal(response.getOcorrencias(i).getCdTipoCanal());
    		comprovante.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
    	    comprovante.setDtPagamento(DateUtils.formartarDataPDCparaPadraPtBr(response.getOcorrencias(i).getDtPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
    	    comprovante.setVlPagamento(response.getOcorrencias(i).getVlPagamento());
    	    comprovante.setCdFavorecido(response.getOcorrencias(i).getCdFavorecido());
    	    comprovante.setDsFavorecido(response.getOcorrencias(i).getDsFavorecido());
    	    comprovante.setFavorecidoFormatado(PgitUtil.concatenarCampos(response.getOcorrencias(i).getCdFavorecido(), response.getOcorrencias(i).getDsFavorecido()));
    	    comprovante.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
    	    comprovante.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
    	    comprovante.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());    	    
    	    comprovante.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
    	    comprovante.setCdDigitoContaDebito(String.valueOf(response.getOcorrencias(i).getCdDigitoContaDebito()));    
    	    comprovante.setContaDebitoFormatada(PgitUtil.formatBancoAgenciaConta(response.getOcorrencias(i).getCdBancoDebito(), response.getOcorrencias(i).getCdAgenciaDebito(),
    	    		comprovante.getCdDigitoAgenciaDebito(), response.getOcorrencias(i).getCdContaDebito(), String.valueOf(response.getOcorrencias(i).getCdDigitoContaDebito()), true));
    	    comprovante.setCdProdutoServico(response.getOcorrencias(i).getCdProdutoServico());
    	    comprovante.setCdSituacaoPagamento(response.getOcorrencias(i).getCdSituacaoPagamento());
    	    comprovante.setDsSituacaoPagamento(response.getOcorrencias(i).getDsSituacaoPagamento());
    	    comprovante.setDsCodigoProdutoServicoOperacao(response.getOcorrencias(i).getDsCodigoProdutoServicoOperacao());
    		comprovante.setCdProdutoRelacionado(response.getOcorrencias(i).getCdProdutoRelacionado());
    		comprovante.setDsCodigoProdutoServicoRelacionado(response.getOcorrencias(i).getDsCodigoProdutoServicoRelacionado());
    	    
    	    listaComprovantes.add(comprovante);
    	}
    	
    	saidaDTO.setComprovantes(listaComprovantes);
    	
    	return saidaDTO;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.ISolEmissaoComprovantePagtoFavorecidoService#consultarPagtoSolEmiCompFavorecido(br.com.bradesco.web.pgit.service.business.solemissaocomprovantepagtofavorecido.bean.ConsultarPagtoSolEmiCompFavorecidoEntradaDTO)
     */
    public List<ConsultarPagtoSolEmiCompFavorecidoSaidaDTO> consultarPagtoSolEmiCompFavorecido(ConsultarPagtoSolEmiCompFavorecidoEntradaDTO entradaDTO){
    	
    	List<ConsultarPagtoSolEmiCompFavorecidoSaidaDTO> listaSaida = new ArrayList<ConsultarPagtoSolEmiCompFavorecidoSaidaDTO>();
    	ConsultarPagtoSolEmiCompFavorecidoRequest request = new ConsultarPagtoSolEmiCompFavorecidoRequest();
    	ConsultarPagtoSolEmiCompFavorecidoResponse response = new ConsultarPagtoSolEmiCompFavorecidoResponse();
    	
    	request.setNrOcorrencias(ISolEmissaoComprovantePagtoFavorecidoServiceConstants.NUMERO_OCORRENCIAS_CONSULTAR_INCLUIR);
    	request.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoaJuridicaContrato()));
    	request.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(entradaDTO.getCdTipoContratoNegocio()));
    	request.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(entradaDTO.getNrSequenciaContratoNegocio()));
    	request.setDtCreditoPagamentoInicio(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoInicio()));
    	request.setDtCreditoPagamentoFim(PgitUtil.verificaStringNula(entradaDTO.getDtCreditoPagamentoFim()));
    	request.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoOperacao()));
    	request.setCdProdutoServicoRelacionado(PgitUtil.verificaIntegerNulo(entradaDTO.getCdProdutoServicoRelacionado()));
    	request.setCdControlePagamentoDe(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoDe()));
    	request.setCdControlePagamentoAte(PgitUtil.verificaStringNula(entradaDTO.getCdControlePagamentoAte()));
    	request.setVlPagamentoDe(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoDe()));
    	request.setVlPagamentoAte(PgitUtil.verificaBigDecimalNulo(entradaDTO.getVlPagamentoAte()));
    	request.setCdFavorecidoClientePagador(PgitUtil.verificaLongNulo(entradaDTO.getCdFavorecidoClientePagador()));
    	request.setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(entradaDTO.getCdInscricaoFavorecido()));
    	request.setCdIdentificacaoInscricaoFavorecido(PgitUtil.verificaIntegerNulo(entradaDTO.getCdIdentificacaoInscricaoFavorecido()));
    
    	response = getFactoryAdapter().getConsultarPagtoSolEmiCompFavorecidoPDCAdapter().invokeProcess(request);
    	
    	ConsultarPagtoSolEmiCompFavorecidoSaidaDTO saidaDTO;
    	
    	for(int i=0;i<response.getOcorrenciasCount(); i++){
    		saidaDTO = new ConsultarPagtoSolEmiCompFavorecidoSaidaDTO();
    		saidaDTO.setCodMensagem(response.getCodMensagem());
    		saidaDTO.setMensagem(response.getMensagem());
    		saidaDTO.setCdEmpresa(response.getOcorrencias(i).getCdEmpresa());
    		saidaDTO.setDsEmpresa(response.getOcorrencias(i).getDsEmpresa());
    		saidaDTO.setCdTipoContratoNegocio(response.getOcorrencias(i).getCdTipoContratoNegocio());
    		saidaDTO.setNrContrato(response.getOcorrencias(i).getNrContrato());
    		saidaDTO.setCdProdutoServicoOperacao(response.getOcorrencias(i).getCdProdutoServicoOperacao());
    		saidaDTO.setDsResumoProdutoServico(response.getOcorrencias(i).getDsResumoProdutoServico());
    		saidaDTO.setCdProdutoServicoRelacionado(response.getOcorrencias(i).getCdProdutoServicoRelacionado());
    		saidaDTO.setDsOperacaoProdutoServico(response.getOcorrencias(i).getDsOperacaoProdutoServico());
    		saidaDTO.setCdControlePagamento(response.getOcorrencias(i).getCdControlePagamento());
    		saidaDTO.setDtCreditoPagamento(response.getOcorrencias(i).getDtCreditoPagamento());
    		saidaDTO.setVlEfetivoPagamento(response.getOcorrencias(i).getVlEfetivoPagamento());
    		saidaDTO.setCdInscricaoFavorecido(response.getOcorrencias(i).getCdInscricaoFavorecido());
    		saidaDTO.setDsFavorecido(response.getOcorrencias(i).getDsFavorecido());
    		saidaDTO.setCdBancoDebito(response.getOcorrencias(i).getCdBancoDebito());
    		saidaDTO.setCdAgenciaDebito(response.getOcorrencias(i).getCdAgenciaDebito());
    		saidaDTO.setCdDigitoAgenciaDebito(response.getOcorrencias(i).getCdDigitoAgenciaDebito());
    		saidaDTO.setCdContaDebito(response.getOcorrencias(i).getCdContaDebito());
    		saidaDTO.setCdDigitoContaDebito(response.getOcorrencias(i).getCdDigitoContaDebito());
    		saidaDTO.setCdSituacaoOperacaoPagamento(response.getOcorrencias(i).getCdSituacaoOperacaoPagamento());
    		saidaDTO.setDsSituacaoOperacaoPagamento(response.getOcorrencias(i).getDsSituacaoOperacaoPagamento());
    		saidaDTO.setCdTipoTela(response.getOcorrencias(i).getCdTipoTela());
    		
    		saidaDTO.setDtCreditoPagamentoFormatada(DateUtils.formartarDataPDCparaPadraPtBr(response.getOcorrencias(i).getDtCreditoPagamento(), "dd.MM.yyyy", "dd/MM/yyyy"));
    		saidaDTO.setFavorecidoFormatado(PgitUtil.concatenarCampos(response.getOcorrencias(i).getCdInscricaoFavorecido(), response.getOcorrencias(i).getDsFavorecido()));
    		saidaDTO.setContaDebitoFormatada(PgitUtil.formatBancoAgenciaConta(saidaDTO.getCdBancoDebito(), saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigitoAgenciaDebito(), saidaDTO.getCdContaDebito(), saidaDTO.getCdDigitoContaDebito(), true));
    		saidaDTO.setDsEfetivacaoPagamento(response.getOcorrencias(i).getDsEfetivacaoPagamento());
    		
    		listaSaida.add(saidaDTO);
    	}
    	
    	return listaSaida;
    }
}