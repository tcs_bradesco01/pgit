/*
 * Nome: br.com.bradesco.web.pgit.service.report.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.report.mantercontrato;

import java.awt.Color;
import java.util.Date;

import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.report.base.BasicPdfReport;
import br.com.bradesco.web.pgit.utils.DateUtils;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Nome: PagamentoOnLineReport
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class PagamentoOnLineReport extends BasicPdfReport {
	
	/** Atributo fTexto. */
	private Font fTexto = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
	
	/** Atributo fTextoTabela. */
	private Font fTextoTabela = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
	
	/** Atributo fTextoTabelaCinza. */
	private Font fTextoTabelaCinza = new Font(Font.TIMES_ROMAN, 8, Font.BOLD,Color.GRAY);
	
	/** Atributo imagemRelatorio. */
	private Image imagemRelatorio = null;
	
	/** Atributo saidaImprimirContrato. */
	private ImprimirContratoSaidaDTO saidaImprimirContrato;
	
	/**
	 * Pagamento on line report.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public PagamentoOnLineReport(ImprimirContratoSaidaDTO saidaImprimirContrato) {
		super();
		this.saidaImprimirContrato = saidaImprimirContrato;
		this.imagemRelatorio = getImagemRelatorio(); 
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.service.report.base.BasicPdfReport#popularPdf(com.lowagie.text.Document)
	 */
	@Override
	protected void popularPdf(Document document) throws DocumentException {

		preencherCabec(document);
		document.add(Chunk.NEWLINE);
		preencherAnexo(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherTitulo(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaCondicoes(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaPrimeira(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaSegunda(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoPrimeiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoSegundo(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiroA(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiroB(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiroC(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoTerceiroD(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoQuarto(document);
		
		document.add(Chunk.NEWLINE);
		preencherParagrafoQuinto(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaTerceira(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuarta(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaPrimeiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaSegundo(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaTerceiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaQuarto(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuartaQuinto(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuinta(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuintaPrimeiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuintaSegundo(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaQuintaTerceiro(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaSexta(document);
		
		document.add(Chunk.NEWLINE);
		preencherClausulaCondicao(document);
		
		document.add(Chunk.NEXTPAGE);
		preencherData(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaUm(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		preencherAssinaturas3(document);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		assinaturaDois(document);
		
				
	}
	
	/**
	 * Preencher cabec.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherCabec(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		tabela.setWidths(new float[] {30.0f, 70.0f});
		
		Paragraph pLinha1Esq = new Paragraph(new Chunk("ANEXO AO CONTRATO DE PRESTA��O DE SERVI�OS DE PAGAMENTOS.", fTextoTabelaCinza));
		pLinha1Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfPCell.NO_BORDER);
		celula1.addElement(imagemRelatorio);
		
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher anexo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAnexo(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("ANEXO 3", fTextoTabelaCinza));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher titulo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherTitulo(Document document) throws DocumentException {
		Paragraph titulo = new Paragraph(new Chunk("Pagamento Eletr�nico de Tributos (Taxas, Impostos e Contribui��es) Pagamento on-line.", fTextoTabela));
		titulo.setAlignment(Element.ALIGN_CENTER);
		document.add(titulo);
	}
	
	/**
	 * Preencher clausula condicoes.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaCondicoes(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("As partes nomeadas e qualificadas no pre�mbulo do Contrato, ", fTexto));
		pLinha.add(new Chunk("Contrato, ", fTextoTabela));
		pLinha.add(new Chunk("ao qual este Anexo faz parte, denominadas ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("e ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", resolvem estabelecer as condi��es operacionais ", fTexto));
		pLinha.add(new Chunk("e comerciais espec�ficas � presta��o dos servi�os contratados.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula primeira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaPrimeira(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Primeira: ", fTextoTabela));
		pLinha.add(new Chunk("Este Anexo tem por objeto a presta��o ", fTexto));
		pLinha.add(new Chunk("pelo ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("ao ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", dos servi�os de Pagamento Eletr�nico de Tributos, por interm�dio de Software, ", fTexto));
		pLinha.add(new Chunk("exclusivo, fornecido pelo ", fTexto));
		pLinha.add(new Chunk("Banco.", fTextoTabela));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula segunda.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaSegunda(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Segunda: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("fornecer� ao ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("todas as informa��es ", fTexto));
		pLinha.add(new Chunk("necess�rias � realiza��o dos pagamentos de seus Tributos, Taxas e Contribui��es, ", fTexto));
		pLinha.add(new Chunk("de compet�ncia dos respectivos �rg�os.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo primeiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoPrimeiro(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("dever� tomar as provid�ncias necess�rias  ", fTexto));
		pLinha.add(new Chunk("para a correta transcri��o de todos os dados dos pagamentos a serem realizados com base neste Anexo, ", fTexto));
		pLinha.add(new Chunk("isentando o ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", neste ato, de toda e qualquer responsabilidade relativa a eventuais reclama��es, ", fTexto));
		pLinha.add(new Chunk("preju�zos, perdas e danos, lucros cessantes e/ou emergentes, inclusive perante terceiros, decorrentes de erros, ", fTexto));
		pLinha.add(new Chunk("falhas, irregularidades e omiss�es dos dados constantes de cada pagamento.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo segundo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoSegundo(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Segundo: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente  ", fTextoTabela));
		pLinha.add(new Chunk("transmitir� os arquivos ao ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("(Centro de Processamento de Dados � na Cidade de Deus � Osasco � S�o Paulo), ", fTexto));
		pLinha.add(new Chunk("obrigatoriamente, at� as 18h00min (Hor�rio de Bras�lia), do dia do pagamento, ", fTexto));
		pLinha.add(new Chunk("para que se viabilize o cumprimento dos pagamentos.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo terceiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoTerceiro(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Terceiro: ", fTextoTabela));
		pLinha.add(new Chunk("Caber� ao ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("provisionar  na conta ", fTexto));
		pLinha.add(new Chunk(saidaImprimirContrato.getCdContaAgenciaGestor() + "-" + saidaImprimirContrato.getCdDigitoContaGestor() + ", ag�ncia ", fTexto));
		pLinha.add(new Chunk(saidaImprimirContrato.getCdAgenciaGestorContrato() + "-" + saidaImprimirContrato.getCdDigitoAgenciaGestor() + " do Banco Bradesco S.A. ", fTexto));
		pLinha.add(new Chunk("de sua titularidade valor suficiente para efetiva��o dos pagamentos, ", fTexto));
		pLinha.add(new Chunk("ficando o ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("autorizado a nela(s) efetivar os d�bitos respectivos, sendo que ele n�o se responsabilizar� ", fTexto));
		pLinha.add(new Chunk("pela n�o realiza��o dos pagamentos nos seguintes casos:", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo terceiro a.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoTerceiroA(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("a) ", fTextoTabela));
		pLinha.add(new Chunk("falhas nas informa��es prestadas;", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo terceiro b.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoTerceiroB(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("b) ", fTextoTabela));
		pLinha.add(new Chunk("atraso na entrega de informa��es;", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo terceiro c.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoTerceiroC(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("c) ", fTextoTabela));
		pLinha.add(new Chunk("aus�ncia da autoriza��o de d�bito do documento por parte do ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk("; e,", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo terceiro d.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoTerceiroD(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("d) ", fTextoTabela));
		pLinha.add(new Chunk("inexist�ncia de saldo suficiente e dispon�vel para os pagamentos agendados, ", fTexto));
		pLinha.add(new Chunk("em conformidade com o previsto no par�grafo primeiro da cl�usula quarta.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo quarto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoQuarto(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Quarto: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("dever� responsabilizar-se por eventuais perdas e danos, ", fTexto));
		pLinha.add(new Chunk("preju�zos, que venham a causar a si pr�pria, ao ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("e a terceiros, e que decorra do acesso ", fTexto));
		pLinha.add(new Chunk("e utiliza��o inadequada ou impr�pria ao sistema por pessoas n�o autorizadas ou credenciadas.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher paragrafo quinto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherParagrafoQuinto(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Quinto: ", fTextoTabela));
		pLinha.add(new Chunk("Caber� ainda, ao ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", responsabilizar-se por pagamentos agendados ", fTexto));
		pLinha.add(new Chunk("ap�s a data de vencimento sem os devidos acr�scimos, ", fTexto));
		pLinha.add(new Chunk("bem como quanto � respectiva regulariza��o junto ao �rg�o envolvido.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula terceira.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaTerceira(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Terceira: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("dever� processar os arquivos recebidos do ", fTexto));
		pLinha.add(new Chunk("Cliente, ", fTextoTabela));
		pLinha.add(new Chunk("efetuando a quita��o dos pagamentos agendados para o dia, a d�bito da(s) conta(s) corrente(s) ", fTexto));
		pLinha.add(new Chunk("indicada(s) no par�grafo terceiro da cl�usula segunda, desde que exista ", fTexto));
		pLinha.add(new Chunk("saldo suficiente e dispon�vel nesta(s) conta(s).", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula quarta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuarta(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Quarta: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Banco", fTextoTabela));
		pLinha.add(new Chunk(", ap�s conclu�da a transmiss�o/processamento ", fTexto));
		pLinha.add(new Chunk("tornar� dispon�vel o arquivo retorno, contendo o resultado do processamento, ", fTexto));
		pLinha.add(new Chunk("ficando sob a exclusiva responsabilidade do ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("realizar a confer�ncia e ", fTexto));
		pLinha.add(new Chunk("verificar/corrigir os dados dos registros inconsistentes, at� a data programada para o d�bito.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula quarta primeiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaPrimeiro(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		pLinha.add(new Chunk("Para os agendamentos efetuados em datas anteriores ", fTexto));
		pLinha.add(new Chunk("a data indicada para o d�bito, � consulta de saldo ser� feita �s 21h00 (hor�rio de Bras�lia), ", fTexto));
		pLinha.add(new Chunk("do dia indicado para o pagamento.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula quarta segundo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaSegundo(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Segundo: ", fTextoTabela));
		pLinha.add(new Chunk("Quando o agendamento for efetuado na mesma data prevista para o d�bito, ", fTexto));
		pLinha.add(new Chunk("o sistema far� a consulta de saldo imediatamente ap�s a recep��o do arquivo remessa e ", fTexto));
		pLinha.add(new Chunk("processamento por parte do ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("e caso n�o haja saldo suficiente e dispon�vel naquele momento, ", fTexto));
		pLinha.add(new Chunk("ser� feita nova consulta �s 21h00.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula quarta terceiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaTerceiro(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Terceiro: ", fTextoTabela));
		pLinha.add(new Chunk("O saldo dispon�vel ser� representado por: a) disponibilidade real ", fTexto));
		pLinha.add(new Chunk("na conta corrente; b) aplica��es financeiras com baixa autom�tica, registrados naquela conta; e, c) ", fTexto));
		pLinha.add(new Chunk("limite dispon�vel decorrente de contrato de: (I) Saque - F�cil ou (II) Conta Corrente Garantida ou (III) ", fTexto));
		pLinha.add(new Chunk("Cr�dito Rotativo com Garantia de Cheques (CRGC).", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula quarta quarto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaQuarto(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Quarto: ", fTextoTabela));
		pLinha.add(new Chunk("Na hip�tese do saldo dispon�vel ser insuficiente para a quita��o total ", fTexto));
		pLinha.add(new Chunk("das obriga��es agendadas, o pagamento ser� efetuado at� o montante do saldo dispon�vel, observada ", fTexto));
		pLinha.add(new Chunk("a ordem decrescente dos valores dos pagamentos a serem efetuados, com a finalidade de evitar nesses casos, ", fTexto));
		pLinha.add(new Chunk("a cobran�a dos acr�scimos devidos pelo ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", dos Tributos, Taxas e Contribui��es pagos com atraso.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula quarta quinto.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuartaQuinto(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Quinto: ", fTextoTabela));
		pLinha.add(new Chunk("Caso o ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("envie agendamentos para datas de d�bito que recaiam ", fTexto));
		pLinha.add(new Chunk("em feriados locais onde s�o mantidas as contas correntes dos clientes, o sistema rejeitar� o arquivo. ", fTexto));
		pLinha.add(new Chunk("Nesse caso o ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("dever� enviar novo arquivo para 1 (um) dia �til.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula quinta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuinta(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Quinta: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("comprovar� os pagamentos efetuados, mediante ", fTexto));
		pLinha.add(new Chunk("remessa ao ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("de arquivo retorno, contendo dados dos documentos pagos e n�o pagos.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula quinta primeiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuintaPrimeiro(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Primeiro: ", fTextoTabela));
		pLinha.add(new Chunk("Para os pagamentos realizados, o ", fTexto));
		pLinha.add(new Chunk("Cliente", fTextoTabela));
		pLinha.add(new Chunk(", por interm�dio de Software ", fTexto));
		pLinha.add(new Chunk("fornecido pelo Banco, poder� emitir comprovante(s) de pagamento, com autentica��o criptografada, ", fTexto));
		pLinha.add(new Chunk("contendo todos os dados informados no documento quitado, conforme as exig�ncias do �rg�o competente.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula quinta segundo.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuintaSegundo(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Segundo: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("disponibilizar� ap�s a efetiva��o do pagamento, o arquivo retorno ", fTexto));
		pLinha.add(new Chunk("com o(s) comprovante(s) de pagamento, que ficar� a disposi��o do ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("pelo prazo m�ximo de 10 (dez) ", fTexto));
		pLinha.add(new Chunk("dias �teis, sendo que ap�s este per�odo, os arquivos n�o retirados ser�o automaticamente exclu�dos.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula quinta terceiro.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaQuintaTerceiro(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Par�grafo Terceiro: ", fTextoTabela));
		pLinha.add(new Chunk("Para todos os fins e efeitos de direito, o ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("reconhecer� como l�quido e certo, ", fTexto));
		pLinha.add(new Chunk("o valor de todos os lan�amentos de d�bito, efetuados em sua(s) conta(s) corrente(s), ", fTexto));
		pLinha.add(new Chunk("desde que autorizados nos termos deste Anexo, identificado como Pagamento Eletr�nico de Tributos.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula sexta.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaSexta(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Cl�usula Sexta: ", fTextoTabela));
		pLinha.add(new Chunk("O ", fTexto));
		pLinha.add(new Chunk("Cliente ", fTextoTabela));
		pLinha.add(new Chunk("n�o pagar� ao ", fTexto));
		pLinha.add(new Chunk("Banco ", fTextoTabela));
		pLinha.add(new Chunk("quaisquer valores pelos servi�os de Pagamento Eletr�nico ", fTexto));
		pLinha.add(new Chunk("de Tributos ora contratados, por�m, n�o estar� isento do pagamento das tarifas de movimenta��o ", fTexto));
		pLinha.add(new Chunk("que venham a incidir em sua(s) conta(s) corrente(s) decorrentes dos pagamentos efetuados.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	/**
	 * Preencher clausula condicao.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherClausulaCondicao(Document document) throws DocumentException {
		Paragraph pLinha = new Paragraph();
		pLinha.add(new Chunk("Por se acharem de pleno acordo, com tudo aqui pactuado, ", fTexto));
		pLinha.add(new Chunk("as partes firmam este Anexo em 03 (tr�s) vias de igual ", fTexto));
		pLinha.add(new Chunk("teor e forma, juntamente com as testemunhas abaixo.", fTexto));
		pLinha.setAlignment(Element.ALIGN_JUSTIFIED);
		document.add(pLinha);
	}
	
	
	/**
	 * Preencher data.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherData(Document document) throws DocumentException {
		Paragraph data = new Paragraph(new Chunk("Osasco, " + DateUtils.formatarDataPorExtenso(new Date()) + ".", fTexto));
		data.setAlignment(Paragraph.ALIGN_RIGHT);
		document.add(data);
	}
	
	/**
	 * Assinatura um.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaUm(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  CLIENTE:", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  BANCO BRADESCO S/A:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  Ag�ncia:", fTexto));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Esq);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}
	
	/**
	 * Preencher assinaturas3.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void preencherAssinaturas3(Document document) throws DocumentException {
		Paragraph testemunha = new Paragraph(new Chunk("  TESTEMUNHAS:"));
		testemunha.setAlignment(Element.ALIGN_LEFT);
		document.add(testemunha);
	}
	
	/**
	 * Assinatura dois.
	 *
	 * @param document the document
	 * @throws DocumentException the document exception
	 */
	public void assinaturaDois(Document document) throws DocumentException {
		
		PdfPTable tabela = new PdfPTable(2);
		tabela.setWidthPercentage(100f);
		
		Paragraph pLinha1Dir = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Dir.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha1Esq = new Paragraph(new Chunk("_________________________________________", fTextoTabela));
		pLinha1Esq.setAlignment(Element.ALIGN_CENTER);
		Paragraph pLinha2Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha2Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Dir = new Paragraph(new Chunk("  NOME:", fTextoTabela));
		pLinha3Dir.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha2Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha2Esq.setAlignment(Element.ALIGN_LEFT);
		Paragraph pLinha3Esq = new Paragraph(new Chunk("  CPF:", fTextoTabela));
		pLinha3Esq.setAlignment(Element.ALIGN_LEFT);
				
		PdfPCell celula1 = new PdfPCell();
		celula1.setBorder(PdfCell.NO_BORDER);
		
		celula1.addElement(pLinha1Dir);
		celula1.addElement(pLinha2Dir);
		celula1.addElement(pLinha3Esq);
				
		PdfPCell celula2 = new PdfPCell();
		celula2.setBorder(PdfCell.NO_BORDER);
		
		celula2.addElement(pLinha1Esq);
		celula2.addElement(pLinha2Esq);
		celula2.addElement(pLinha3Dir);
				
		tabela.addCell(celula1);
		tabela.addCell(celula2);
		
		document.add(tabela);
		
	}

	/**
	 * Get: saidaImprimirContrato.
	 *
	 * @return saidaImprimirContrato
	 */
	public ImprimirContratoSaidaDTO getSaidaImprimirContrato() {
		return saidaImprimirContrato;
	}

	/**
	 * Set: saidaImprimirContrato.
	 *
	 * @param saidaImprimirContrato the saida imprimir contrato
	 */
	public void setSaidaImprimirContrato(
			ImprimirContratoSaidaDTO saidaImprimirContrato) {
		this.saidaImprimirContrato = saidaImprimirContrato;
	}

}
