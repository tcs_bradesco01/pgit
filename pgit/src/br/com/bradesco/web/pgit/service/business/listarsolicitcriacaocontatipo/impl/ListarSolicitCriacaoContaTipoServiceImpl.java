/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/implementation.ftl,v $
 * $Id: implementation.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: implementation.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */
 
package br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.IListarSolicitCriacaoContaTipoService;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.DetalharSolicCriacaoContaTipoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.DetalharSolicCriacaoContaTipoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.ExcluirSolicCriacaoContaTipoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.ExcluirSolicCriacaoContaTipoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.IncluirSolicCriacaoContaTipoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.IncluirSolicCriacaoContaTipoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.ListarSolicCriacaoContaTipoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.ListarSolicCriacaoContaTipoSaidaDTO;
import br.com.bradesco.web.pgit.service.data.pdc.FactoryAdapter;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.request.DetalharSolicitCriacaoContaTipoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.detalharsolicitcriacaocontatipo.response.DetalharSolicitCriacaoContaTipoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitcriacaocontatipo.request.ExcluirSolicitCriacaoContaTipoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.excluirsolicitcriacaocontatipo.response.ExcluirSolicitCriacaoContaTipoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitcriacaocontatipo.request.IncluirSolicitCriacaoContaTipoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.incluirsolicitcriacaocontatipo.response.IncluirSolicitCriacaoContaTipoResponse;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolicitcriacaocontatipo.request.ListarSolicitCriacaoContaTipoRequest;
import br.com.bradesco.web.pgit.service.data.pdc.listarsolicitcriacaocontatipo.response.ListarSolicitCriacaoContaTipoResponse;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Implementa��o do adaptador: ListarSolicitCriacaoContaTipo
 * </p>
 * 
 * @comment C�DIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public class ListarSolicitCriacaoContaTipoServiceImpl implements IListarSolicitCriacaoContaTipoService {

	/** Atributo factoryAdapter. */
	private FactoryAdapter factoryAdapter;
	
    /**
     * Get: factoryAdapter.
     *
     * @return factoryAdapter
     */
    public FactoryAdapter getFactoryAdapter() {
		return factoryAdapter;
	}

	/**
	 * Set: factoryAdapter.
	 *
	 * @param factoryAdapter the factory adapter
	 */
	public void setFactoryAdapter(FactoryAdapter factoryAdapter) {
		this.factoryAdapter = factoryAdapter;
	}
	
    /**
     * Construtor.
     */
    public ListarSolicitCriacaoContaTipoServiceImpl() {
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.IListarSolicitCriacaoContaTipoService#consultarSolicitacaoContaTipo998(br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.ListarSolicCriacaoContaTipoEntradaDTO)
     */
    public List<ListarSolicCriacaoContaTipoSaidaDTO> consultarSolicitacaoContaTipo998(ListarSolicCriacaoContaTipoEntradaDTO entradaDTO){
    	ListarSolicitCriacaoContaTipoRequest request = new ListarSolicitCriacaoContaTipoRequest();
    	ListarSolicitCriacaoContaTipoResponse response = new ListarSolicitCriacaoContaTipoResponse();
    	List<ListarSolicCriacaoContaTipoSaidaDTO> listaSaida = new ArrayList<ListarSolicCriacaoContaTipoSaidaDTO>();

    	request.setNrOcorrencias(PgitUtil.verificaIntegerNulo(entradaDTO.getNrOcorrencias()));
    	request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
    	request.setCdCpfCnpjPssoa(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjPssoa()));
    	request.setCdFilialCnpjPssoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjPssoa()));
    	request.setCdControleCnpjPssoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjPssoa()));
    	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
    	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
    	request.setCdGrupoContabil(PgitUtil.verificaIntegerNulo(entradaDTO.getCdGrupoContabil()));
    	request.setCdSubGrupoContabil(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSubGrupoContabil()));
    	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
    	request.setCdDigitoConta(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoConta()));

    	response = getFactoryAdapter().getListarSolicitCriacaoContaTipoPDCAdapter().invokeProcess(request);
    	
    	ListarSolicCriacaoContaTipoSaidaDTO saida;
    	 
    	for(int i=0;i<response.getOcorrenciasCount();i++){
    		saida = new ListarSolicCriacaoContaTipoSaidaDTO();

    		saida.setNrSolicitContaDesp(response.getOcorrencias(i).getNrSolicitContaDesp());
    		saida.setCdBanco(response.getOcorrencias(i).getCdBanco());
    		saida.setDsBanco(response.getOcorrencias(i).getDsBanco());
    		saida.setCdAgencia(response.getOcorrencias(i).getCdAgencia());
    		saida.setDsAgencia(response.getOcorrencias(i).getDsAgencia());
    		saida.setCdGrupoContabil(response.getOcorrencias(i).getCdGrupoContabil());
    		saida.setCdSubGrupoContabil(response.getOcorrencias(i).getCdSubGrupoContabil());
    		saida.setCdConta(response.getOcorrencias(i).getCdConta());
    		saida.setCdDigitoConta(response.getOcorrencias(i).getCdDigitoConta());
    		saida.setCdSituacaoSolicitacao(response.getOcorrencias(i).getCdSituacaoSolicitacao());
    		saida.setDsSituacaoSolicitacao(response.getOcorrencias(i).getDsSituacaoSolicitacao());
    		saida.setHrInclusaoRegistro(response.getOcorrencias(i).getHrInclusaoRegistro());

    		listaSaida.add(saida);
    	}
    	
    	return listaSaida;
    }
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.IListarSolicitCriacaoContaTipoService#detalharSolicitacaoContaTipo998(br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.DetalharSolicCriacaoContaTipoEntradaDTO)
     */
    public DetalharSolicCriacaoContaTipoSaidaDTO detalharSolicitacaoContaTipo998(DetalharSolicCriacaoContaTipoEntradaDTO entradaDTO) {
    	DetalharSolicCriacaoContaTipoSaidaDTO saida = new DetalharSolicCriacaoContaTipoSaidaDTO();
    	DetalharSolicitCriacaoContaTipoRequest request = new DetalharSolicitCriacaoContaTipoRequest();
    	DetalharSolicitCriacaoContaTipoResponse response = new DetalharSolicitCriacaoContaTipoResponse();

    	request.setNrSolicitContaDesp(PgitUtil.verificaLongNulo(entradaDTO.getNrSolicitContaDesp()));
    	request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
    	request.setCdCpfCnpjPssoa(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjPssoa()));
    	request.setCdFilialCnpjPssoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjPssoa()));
    	request.setCdControleCnpjPssoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjPssoa()));

    	response = getFactoryAdapter().getDetalharSolicitCriacaoContaTipoPDCAdapter().invokeProcess(request);

    	saida.setCodMensagem(response.getCodMensagem());
    	saida.setMensagem(response.getMensagem());
    	saida.setCdCpfCnpjPssoa(response.getCdCpfCnpjPssoa());
    	saida.setCdFilialCnpjPssoa(response.getCdFilialCnpjPssoa());
    	saida.setCdControleCnpjPssoa(response.getCdControleCnpjPssoa());
    	saida.setDsNomePssoa(response.getDsNomePssoa());
    	saida.setCdSituacaoSolicitacao(response.getCdSituacaoSolicitacao());
    	saida.setDsSituacaoSolicitacao(response.getDsSituacaoSolicitacao());
    	saida.setCdMotvoSolicit(response.getCdMotvoSolicit());
    	saida.setDsMotvoSolicit(response.getDsMotvoSolicit());
    	saida.setCdBanco(response.getCdBanco());
    	saida.setDsBanco(response.getDsBanco());
    	saida.setCdAgencia(response.getCdAgencia());
    	saida.setDsAgencia(response.getDsAgencia());
    	saida.setCdGrupoContabil(response.getCdGrupoContabil());
    	saida.setCdSubGrupoContabil(response.getCdSubGrupoContabil());
    	saida.setDsGrupoContabil(response.getDsGrupoContabil());
    	saida.setCdConta(response.getCdConta());
    	saida.setCdDigitoConta(response.getCdDigitoConta());
    	saida.setCdUsuarioInclusao(response.getCdUsuarioInclusao());
    	saida.setDtHoraInclusao(response.getHrInclusaoRegistro());
    	saida.setDsTipoCanal(response.getCdTipoCanal() + " - " + response.getDtTipoCanal());

		return saida;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.IListarSolicitCriacaoContaTipoService#incluirSolicitacaoContaTipo998(br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.IncluirSolicCriacaoContaTipoEntradaDTO)
     */
    public IncluirSolicCriacaoContaTipoSaidaDTO incluirSolicitacaoContaTipo998(IncluirSolicCriacaoContaTipoEntradaDTO entradaDTO) {
		
    	IncluirSolicCriacaoContaTipoSaidaDTO saida = new IncluirSolicCriacaoContaTipoSaidaDTO();
		
    	IncluirSolicitCriacaoContaTipoRequest request = new IncluirSolicitCriacaoContaTipoRequest();
    	IncluirSolicitCriacaoContaTipoResponse response = new IncluirSolicitCriacaoContaTipoResponse();
	
    	request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
    	request.setCdCpfCnpjPssoa(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjPssoa()));
    	request.setCdFilialCnpjPssoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjPssoa()));
    	request.setCdControleCnpjPssoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjPssoa()));
    	request.setCdBanco(PgitUtil.verificaIntegerNulo(entradaDTO.getCdBanco()));
    	request.setCdAgencia(PgitUtil.verificaIntegerNulo(entradaDTO.getCdAgencia()));
    	request.setCdGrupoContabil(PgitUtil.verificaIntegerNulo(entradaDTO.getCdGrupoContabil()));
    	request.setCdSubGrupoContabil(PgitUtil.verificaIntegerNulo(entradaDTO.getCdSubGrupoContabil()));
    	request.setCdConta(PgitUtil.verificaLongNulo(entradaDTO.getCdConta()));
    	request.setCdDigitoConta(PgitUtil.verificaStringNula(entradaDTO.getCdDigitoConta()));

    	response = getFactoryAdapter().getIncluirSolicitCriacaoContaTipoPDCAdapter().invokeProcess(request);
		
    	saida.setCodMensagem(response.getCodMensagem());
    	saida.setMensagem(response.getMensagem());
	
		return saida;
	}
    
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.IListarSolicitCriacaoContaTipoService#excluirSolicitacaoContaTipo998(br.com.bradesco.web.pgit.service.business.listarsolicitcriacaocontatipo.bean.ExcluirSolicCriacaoContaTipoEntradaDTO)
     */
    public ExcluirSolicCriacaoContaTipoSaidaDTO excluirSolicitacaoContaTipo998(ExcluirSolicCriacaoContaTipoEntradaDTO entradaDTO) {
		
    	ExcluirSolicCriacaoContaTipoSaidaDTO saida = new ExcluirSolicCriacaoContaTipoSaidaDTO();
		
    	ExcluirSolicitCriacaoContaTipoRequest request = new ExcluirSolicitCriacaoContaTipoRequest();
    	ExcluirSolicitCriacaoContaTipoResponse response = new ExcluirSolicitCriacaoContaTipoResponse();
	
    	request.setNrSolicitContaDesp(PgitUtil.verificaLongNulo(entradaDTO.getNrSolicitContaDesp()));
    	request.setCdPessoa(PgitUtil.verificaLongNulo(entradaDTO.getCdPessoa()));
    	request.setCdCpfCnpjPssoa(PgitUtil.verificaLongNulo(entradaDTO.getCdCpfCnpjPssoa()));
    	request.setCdFilialCnpjPssoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdFilialCnpjPssoa()));
    	request.setCdControleCnpjPssoa(PgitUtil.verificaIntegerNulo(entradaDTO.getCdControleCnpjPssoa()));

    	response = getFactoryAdapter().getExcluirSolicitCriacaoContaTipoPDCAdapter().invokeProcess(request);
		
    	saida.setCodMensagem(response.getCodMensagem());
    	saida.setMensagem(response.getMensagem());
	
		return saida;
	}
    
}