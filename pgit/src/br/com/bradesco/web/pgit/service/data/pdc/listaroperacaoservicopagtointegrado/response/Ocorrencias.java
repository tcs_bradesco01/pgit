/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listaroperacaoservicopagtointegrado.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdprodutoServicoOperacao
     */
    private int _cdprodutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdprodutoServicoOperacao
     */
    private boolean _has_cdprodutoServicoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _cdOperacaoProdutoServico
     */
    private int _cdOperacaoProdutoServico = 0;

    /**
     * keeps track of state for field: _cdOperacaoProdutoServico
     */
    private boolean _has_cdOperacaoProdutoServico;

    /**
     * Field _cdOperacaoServicoIntegrado
     */
    private int _cdOperacaoServicoIntegrado = 0;

    /**
     * keeps track of state for field: _cdOperacaoServicoIntegrado
     */
    private boolean _has_cdOperacaoServicoIntegrado;

    /**
     * Field _dsTipoServico
     */
    private java.lang.String _dsTipoServico;

    /**
     * Field _dsModalidadeServico
     */
    private java.lang.String _dsModalidadeServico;

    /**
     * Field _dsOperacaoCatalogo
     */
    private java.lang.String _dsOperacaoCatalogo;

    /**
     * Field _dsNatureza
     */
    private java.lang.String _dsNatureza;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaoservicopagtointegrado.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdOperacaoProdutoServico
     * 
     */
    public void deleteCdOperacaoProdutoServico()
    {
        this._has_cdOperacaoProdutoServico= false;
    } //-- void deleteCdOperacaoProdutoServico() 

    /**
     * Method deleteCdOperacaoServicoIntegrado
     * 
     */
    public void deleteCdOperacaoServicoIntegrado()
    {
        this._has_cdOperacaoServicoIntegrado= false;
    } //-- void deleteCdOperacaoServicoIntegrado() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdprodutoServicoOperacao
     * 
     */
    public void deleteCdprodutoServicoOperacao()
    {
        this._has_cdprodutoServicoOperacao= false;
    } //-- void deleteCdprodutoServicoOperacao() 

    /**
     * Returns the value of field 'cdOperacaoProdutoServico'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoProdutoServico'.
     */
    public int getCdOperacaoProdutoServico()
    {
        return this._cdOperacaoProdutoServico;
    } //-- int getCdOperacaoProdutoServico() 

    /**
     * Returns the value of field 'cdOperacaoServicoIntegrado'.
     * 
     * @return int
     * @return the value of field 'cdOperacaoServicoIntegrado'.
     */
    public int getCdOperacaoServicoIntegrado()
    {
        return this._cdOperacaoServicoIntegrado;
    } //-- int getCdOperacaoServicoIntegrado() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdprodutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdprodutoServicoOperacao'.
     */
    public int getCdprodutoServicoOperacao()
    {
        return this._cdprodutoServicoOperacao;
    } //-- int getCdprodutoServicoOperacao() 

    /**
     * Returns the value of field 'dsModalidadeServico'.
     * 
     * @return String
     * @return the value of field 'dsModalidadeServico'.
     */
    public java.lang.String getDsModalidadeServico()
    {
        return this._dsModalidadeServico;
    } //-- java.lang.String getDsModalidadeServico() 

    /**
     * Returns the value of field 'dsNatureza'.
     * 
     * @return String
     * @return the value of field 'dsNatureza'.
     */
    public java.lang.String getDsNatureza()
    {
        return this._dsNatureza;
    } //-- java.lang.String getDsNatureza() 

    /**
     * Returns the value of field 'dsOperacaoCatalogo'.
     * 
     * @return String
     * @return the value of field 'dsOperacaoCatalogo'.
     */
    public java.lang.String getDsOperacaoCatalogo()
    {
        return this._dsOperacaoCatalogo;
    } //-- java.lang.String getDsOperacaoCatalogo() 

    /**
     * Returns the value of field 'dsTipoServico'.
     * 
     * @return String
     * @return the value of field 'dsTipoServico'.
     */
    public java.lang.String getDsTipoServico()
    {
        return this._dsTipoServico;
    } //-- java.lang.String getDsTipoServico() 

    /**
     * Method hasCdOperacaoProdutoServico
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoProdutoServico()
    {
        return this._has_cdOperacaoProdutoServico;
    } //-- boolean hasCdOperacaoProdutoServico() 

    /**
     * Method hasCdOperacaoServicoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdOperacaoServicoIntegrado()
    {
        return this._has_cdOperacaoServicoIntegrado;
    } //-- boolean hasCdOperacaoServicoIntegrado() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdprodutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdprodutoServicoOperacao()
    {
        return this._has_cdprodutoServicoOperacao;
    } //-- boolean hasCdprodutoServicoOperacao() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdOperacaoProdutoServico'.
     * 
     * @param cdOperacaoProdutoServico the value of field
     * 'cdOperacaoProdutoServico'.
     */
    public void setCdOperacaoProdutoServico(int cdOperacaoProdutoServico)
    {
        this._cdOperacaoProdutoServico = cdOperacaoProdutoServico;
        this._has_cdOperacaoProdutoServico = true;
    } //-- void setCdOperacaoProdutoServico(int) 

    /**
     * Sets the value of field 'cdOperacaoServicoIntegrado'.
     * 
     * @param cdOperacaoServicoIntegrado the value of field
     * 'cdOperacaoServicoIntegrado'.
     */
    public void setCdOperacaoServicoIntegrado(int cdOperacaoServicoIntegrado)
    {
        this._cdOperacaoServicoIntegrado = cdOperacaoServicoIntegrado;
        this._has_cdOperacaoServicoIntegrado = true;
    } //-- void setCdOperacaoServicoIntegrado(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdprodutoServicoOperacao'.
     * 
     * @param cdprodutoServicoOperacao the value of field
     * 'cdprodutoServicoOperacao'.
     */
    public void setCdprodutoServicoOperacao(int cdprodutoServicoOperacao)
    {
        this._cdprodutoServicoOperacao = cdprodutoServicoOperacao;
        this._has_cdprodutoServicoOperacao = true;
    } //-- void setCdprodutoServicoOperacao(int) 

    /**
     * Sets the value of field 'dsModalidadeServico'.
     * 
     * @param dsModalidadeServico the value of field
     * 'dsModalidadeServico'.
     */
    public void setDsModalidadeServico(java.lang.String dsModalidadeServico)
    {
        this._dsModalidadeServico = dsModalidadeServico;
    } //-- void setDsModalidadeServico(java.lang.String) 

    /**
     * Sets the value of field 'dsNatureza'.
     * 
     * @param dsNatureza the value of field 'dsNatureza'.
     */
    public void setDsNatureza(java.lang.String dsNatureza)
    {
        this._dsNatureza = dsNatureza;
    } //-- void setDsNatureza(java.lang.String) 

    /**
     * Sets the value of field 'dsOperacaoCatalogo'.
     * 
     * @param dsOperacaoCatalogo the value of field
     * 'dsOperacaoCatalogo'.
     */
    public void setDsOperacaoCatalogo(java.lang.String dsOperacaoCatalogo)
    {
        this._dsOperacaoCatalogo = dsOperacaoCatalogo;
    } //-- void setDsOperacaoCatalogo(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoServico'.
     * 
     * @param dsTipoServico the value of field 'dsTipoServico'.
     */
    public void setDsTipoServico(java.lang.String dsTipoServico)
    {
        this._dsTipoServico = dsTipoServico;
    } //-- void setDsTipoServico(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listaroperacaoservicopagtointegrado.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listaroperacaoservicopagtointegrado.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listaroperacaoservicopagtointegrado.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listaroperacaoservicopagtointegrado.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
