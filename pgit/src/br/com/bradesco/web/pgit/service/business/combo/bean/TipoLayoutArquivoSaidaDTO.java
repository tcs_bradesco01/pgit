/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

import java.util.List;

/**
 * Nome: TipoLayoutArquivoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoLayoutArquivoSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;

	/** Atributo numeroVersaoLayoutArquivo. */
	private Integer numeroVersaoLayoutArquivo;

	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	private List<TipoLayoutArquivoSaidaDTO> ocorrencias;
	
	/**
	 * Tipo layout arquivo saida dto.
	 *
	 */
	public TipoLayoutArquivoSaidaDTO() {
		super();
	}

	/**
	 * Tipo layout arquivo saida dto.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public TipoLayoutArquivoSaidaDTO(Integer cdTipoLayoutArquivo, String dsTipoLayoutArquivo) {
		super();
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}
	
	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Nome: setNumeroVersaoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @param numeroVersaoLayoutArquivo
	 */
	public void setNumeroVersaoLayoutArquivo(Integer numeroVersaoLayoutArquivo) {
		this.numeroVersaoLayoutArquivo = numeroVersaoLayoutArquivo;
	}

	/**
	 * Nome: getNumeroVersaoLayoutArquivo
	 *
	 * @exception
	 * @throws
	 * @return numeroVersaoLayoutArquivo
	 */
	public Integer getNumeroVersaoLayoutArquivo() {
		return numeroVersaoLayoutArquivo;
	}

	/**
	 * Nome: setNumeroLinhas
	 *
	 * @exception
	 * @throws
	 * @param numeroLinhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Nome: getNumeroLinhas
	 *
	 * @exception
	 * @throws
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}

	/**
	 * Nome: setOcorrencias
	 *
	 * @exception
	 * @throws
	 * @param ocorrencias
	 */
	public void setOcorrencias(List<TipoLayoutArquivoSaidaDTO> ocorrencias) {
		this.ocorrencias = ocorrencias;
	}

	/**
	 * Nome: getOcorrencias
	 *
	 * @exception
	 * @throws
	 * @return ocorrencias
	 */
	public List<TipoLayoutArquivoSaidaDTO> getOcorrencias() {
		return ocorrencias;
	}
}