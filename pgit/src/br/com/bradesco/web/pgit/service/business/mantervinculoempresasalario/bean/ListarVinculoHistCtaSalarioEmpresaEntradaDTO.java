/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean;

import java.util.Date;

import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ListarVinculoHistCtaSalarioEmpresaEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarVinculoHistCtaSalarioEmpresaEntradaDTO {

    /** Atributo cdPessoaJuridContrato. */
    private Long cdPessoaJuridContrato;

    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;

    /** Atributo nrSeqContratoNegocio. */
    private Long nrSeqContratoNegocio;

    /** Atributo cdTipoVinculoContrato. */
    private Integer cdTipoVinculoContrato;

    /** Atributo dtDe. */
    private String dtDe;

    /** Atributo dtAte. */
    private String dtAte;

    /** Atributo dataDe. */
    private Date dataDe = new Date();

    /** Atributo dataAte. */
    private Date dataAte = new Date();

    /** Atributo cdPessoaJuridVinc. */
    private Long cdPessoaJuridVinc;

    /** Atributo cdTipoContratoVinc. */
    private Integer cdTipoContratoVinc;

    /** Atributo nrSeqContratoVinc. */
    private Long nrSeqContratoVinc;

    /** Atributo cdBanco. */
    private Integer cdBanco;

    /** Atributo cdAgencia. */
    private Integer cdAgencia;

    /** Atributo cdDigitoAgencia. */
    private Integer cdDigitoAgencia;

    /** Atributo cdConta. */
    private Long cdConta;

    /** Atributo cdDigitoConta. */
    private String cdDigitoConta;

    /** Atributo cdPesquisaLista. */
    private Integer cdPesquisaLista;

    /**
     * Listar vinculo hist cta salario empresa entrada dto.
     */
    public ListarVinculoHistCtaSalarioEmpresaEntradaDTO() {
	super();
    }

    /**
     * Get: dataAte.
     *
     * @return dataAte
     */
    public Date getDataAte() {
	return dataAte;
    }

    /**
     * Set: dataAte.
     *
     * @param dataAte the data ate
     */
    public void setDataAte(Date dataAte) {
	this.dataAte = dataAte;
	this.dtAte = FormatarData.formataDiaMesAnoToPdc(this.dataAte);
    }

    /**
     * Get: dataDe.
     *
     * @return dataDe
     */
    public Date getDataDe() {
	return dataDe;
    }

    /**
     * Set: dataDe.
     *
     * @param dataDe the data de
     */
    public void setDataDe(Date dataDe) {
	this.dataDe = dataDe;
	this.dtDe = FormatarData.formataDiaMesAnoToPdc(this.dataDe);
    }

    /**
     * Get: cdPessoaJuridContrato.
     *
     * @return cdPessoaJuridContrato
     */
    public Long getCdPessoaJuridContrato() {
	return cdPessoaJuridContrato;
    }

    /**
     * Set: cdPessoaJuridContrato.
     *
     * @param cdPessoaJuridContrato the cd pessoa jurid contrato
     */
    public void setCdPessoaJuridContrato(Long cdPessoaJuridContrato) {
	this.cdPessoaJuridContrato = cdPessoaJuridContrato;
    }

    /**
     * Get: dtAte.
     *
     * @return dtAte
     */
    public String getDtAte() {
	return dtAte;
    }

    /**
     * Set: dtAte.
     *
     * @param dtAte the dt ate
     */
    public void setDtAte(String dtAte) {
	this.dtAte = dtAte;
    }

    /**
     * Get: dtDe.
     *
     * @return dtDe
     */
    public String getDtDe() {
	return dtDe;
    }

    /**
     * Set: dtDe.
     *
     * @param dtDe the dt de
     */
    public void setDtDe(String dtDe) {
	this.dtDe = dtDe;
    }

    /**
     * Get: cdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
	return cdTipoContratoNegocio;
    }

    /**
     * Set: cdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
	this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Get: nrSeqContratoNegocio.
     *
     * @return nrSeqContratoNegocio
     */
    public Long getNrSeqContratoNegocio() {
	return nrSeqContratoNegocio;
    }

    /**
     * Set: nrSeqContratoNegocio.
     *
     * @param nrSeqContratoNegocio the nr seq contrato negocio
     */
    public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
	this.nrSeqContratoNegocio = nrSeqContratoNegocio;
    }

    /**
     * Get: cdTipoVinculoContrato.
     *
     * @return cdTipoVinculoContrato
     */
    public Integer getCdTipoVinculoContrato() {
	return cdTipoVinculoContrato;
    }

    /**
     * Set: cdTipoVinculoContrato.
     *
     * @param cdTipoVinculoContrato the cd tipo vinculo contrato
     */
    public void setCdTipoVinculoContrato(Integer cdTipoVinculoContrato) {
	this.cdTipoVinculoContrato = cdTipoVinculoContrato;
    }

    /**
     * Get: cdAgencia.
     *
     * @return cdAgencia
     */
    public Integer getCdAgencia() {
	return cdAgencia;
    }

    /**
     * Set: cdAgencia.
     *
     * @param cdAgencia the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
	this.cdAgencia = cdAgencia;
    }

    /**
     * Get: cdBanco.
     *
     * @return cdBanco
     */
    public Integer getCdBanco() {
	return cdBanco;
    }

    /**
     * Set: cdBanco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
	this.cdBanco = cdBanco;
    }

    /**
     * Get: cdConta.
     *
     * @return cdConta
     */
    public Long getCdConta() {
	return cdConta;
    }

    /**
     * Set: cdConta.
     *
     * @param cdConta the cd conta
     */
    public void setCdConta(Long cdConta) {
	this.cdConta = cdConta;
    }

    /**
     * Get: cdDigitoAgencia.
     *
     * @return cdDigitoAgencia
     */
    public Integer getCdDigitoAgencia() {
	return cdDigitoAgencia;
    }

    /**
     * Set: cdDigitoAgencia.
     *
     * @param cdDigitoAgencia the cd digito agencia
     */
    public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
	this.cdDigitoAgencia = cdDigitoAgencia;
    }

    /**
     * Get: cdDigitoConta.
     *
     * @return cdDigitoConta
     */
    public String getCdDigitoConta() {
	return cdDigitoConta;
    }

    /**
     * Set: cdDigitoConta.
     *
     * @param cdDigitoConta the cd digito conta
     */
    public void setCdDigitoConta(String cdDigitoConta) {
	this.cdDigitoConta = cdDigitoConta;
    }

    /**
     * Get: cdPesquisaLista.
     *
     * @return cdPesquisaLista
     */
    public Integer getCdPesquisaLista() {
	return cdPesquisaLista;
    }

    /**
     * Set: cdPesquisaLista.
     *
     * @param cdPesquisaLista the cd pesquisa lista
     */
    public void setCdPesquisaLista(Integer cdPesquisaLista) {
	this.cdPesquisaLista = cdPesquisaLista;
    }

    /**
     * Get: cdPessoaJuridVinc.
     *
     * @return cdPessoaJuridVinc
     */
    public Long getCdPessoaJuridVinc() {
	return cdPessoaJuridVinc;
    }

    /**
     * Set: cdPessoaJuridVinc.
     *
     * @param cdPessoaJuridVinc the cd pessoa jurid vinc
     */
    public void setCdPessoaJuridVinc(Long cdPessoaJuridVinc) {
	this.cdPessoaJuridVinc = cdPessoaJuridVinc;
    }

    /**
     * Get: cdTipoContratoVinc.
     *
     * @return cdTipoContratoVinc
     */
    public Integer getCdTipoContratoVinc() {
	return cdTipoContratoVinc;
    }

    /**
     * Set: cdTipoContratoVinc.
     *
     * @param cdTipoContratoVinc the cd tipo contrato vinc
     */
    public void setCdTipoContratoVinc(Integer cdTipoContratoVinc) {
	this.cdTipoContratoVinc = cdTipoContratoVinc;
    }

    /**
     * Get: nrSeqContratoVinc.
     *
     * @return nrSeqContratoVinc
     */
    public Long getNrSeqContratoVinc() {
	return nrSeqContratoVinc;
    }

    /**
     * Set: nrSeqContratoVinc.
     *
     * @param nrSeqContratoVinc the nr seq contrato vinc
     */
    public void setNrSeqContratoVinc(Long nrSeqContratoVinc) {
	this.nrSeqContratoVinc = nrSeqContratoVinc;
    }

}
