/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean;

/**
 * Nome: ListarCnpjFicticioOcorrencias
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarCnpjFicticioOcorrencias {
	
   /** Atributo cdContratoSaida. */
   private Long cdContratoSaida;
   
   /** Atributo cdCpfCnpjSaida. */
   private Long cdCpfCnpjSaida;
   
   /** Atributo cdFilialCpfCnpjSaida. */
   private Integer cdFilialCpfCnpjSaida;
   
   /** Atributo cdCtrlCpfCnpjSaida. */
   private Integer cdCtrlCpfCnpjSaida;
   
   /** Atributo dsRazaoSocial. */
   private String dsRazaoSocial;
   
   /** Atributo cdCpfCnpjOriginal. */
   private Long cdCpfCnpjOriginal;
   
   /** Atributo cdFilialCpfCnpjOriginal. */
   private Integer cdFilialCpfCnpjOriginal;
   
   /** Atributo cdCtrlCpfCnpjOriginal. */
   private Integer cdCtrlCpfCnpjOriginal;
   
   /** Atributo cdNumeroContrato. */
   private Long cdNumeroContrato;
   
   /** Atributo cpfCnpjFormatado. */
   private String cpfCnpjFormatado;
   
   /** Atributo cpfCnpjOriginalFormatado. */
   private String cpfCnpjOriginalFormatado;
   
	/**
	 * Get: cdContratoSaida.
	 *
	 * @return cdContratoSaida
	 */
	public Long getCdContratoSaida() {
		return cdContratoSaida;
	}
	
	/**
	 * Set: cdContratoSaida.
	 *
	 * @param cdContratoSaida the cd contrato saida
	 */
	public void setCdContratoSaida(Long cdContratoSaida) {
		this.cdContratoSaida = cdContratoSaida;
	}
	
	/**
	 * Get: cdCpfCnpjSaida.
	 *
	 * @return cdCpfCnpjSaida
	 */
	public Long getCdCpfCnpjSaida() {
		return cdCpfCnpjSaida;
	}
	
	/**
	 * Set: cdCpfCnpjSaida.
	 *
	 * @param cdCpfCnpjSaida the cd cpf cnpj saida
	 */
	public void setCdCpfCnpjSaida(Long cdCpfCnpjSaida) {
		this.cdCpfCnpjSaida = cdCpfCnpjSaida;
	}
	
	/**
	 * Get: cdFilialCpfCnpjSaida.
	 *
	 * @return cdFilialCpfCnpjSaida
	 */
	public Integer getCdFilialCpfCnpjSaida() {
		return cdFilialCpfCnpjSaida;
	}
	
	/**
	 * Set: cdFilialCpfCnpjSaida.
	 *
	 * @param cdFilialCpfCnpjSaida the cd filial cpf cnpj saida
	 */
	public void setCdFilialCpfCnpjSaida(Integer cdFilialCpfCnpjSaida) {
		this.cdFilialCpfCnpjSaida = cdFilialCpfCnpjSaida;
	}
	
	/**
	 * Get: cdCtrlCpfCnpjSaida.
	 *
	 * @return cdCtrlCpfCnpjSaida
	 */
	public Integer getCdCtrlCpfCnpjSaida() {
		return cdCtrlCpfCnpjSaida;
	}
	
	/**
	 * Set: cdCtrlCpfCnpjSaida.
	 *
	 * @param cdCtrlCpfCnpjSaida the cd ctrl cpf cnpj saida
	 */
	public void setCdCtrlCpfCnpjSaida(Integer cdCtrlCpfCnpjSaida) {
		this.cdCtrlCpfCnpjSaida = cdCtrlCpfCnpjSaida;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: cdCpfCnpjOriginal.
	 *
	 * @return cdCpfCnpjOriginal
	 */
	public Long getCdCpfCnpjOriginal() {
		return cdCpfCnpjOriginal;
	}
	
	/**
	 * Set: cdCpfCnpjOriginal.
	 *
	 * @param cdCpfCnpjOriginal the cd cpf cnpj original
	 */
	public void setCdCpfCnpjOriginal(Long cdCpfCnpjOriginal) {
		this.cdCpfCnpjOriginal = cdCpfCnpjOriginal;
	}
	
	/**
	 * Get: cdFilialCpfCnpjOriginal.
	 *
	 * @return cdFilialCpfCnpjOriginal
	 */
	public Integer getCdFilialCpfCnpjOriginal() {
		return cdFilialCpfCnpjOriginal;
	}
	
	/**
	 * Set: cdFilialCpfCnpjOriginal.
	 *
	 * @param cdFilialCpfCnpjOriginal the cd filial cpf cnpj original
	 */
	public void setCdFilialCpfCnpjOriginal(Integer cdFilialCpfCnpjOriginal) {
		this.cdFilialCpfCnpjOriginal = cdFilialCpfCnpjOriginal;
	}
	
	/**
	 * Get: cdCtrlCpfCnpjOriginal.
	 *
	 * @return cdCtrlCpfCnpjOriginal
	 */
	public Integer getCdCtrlCpfCnpjOriginal() {
		return cdCtrlCpfCnpjOriginal;
	}
	
	/**
	 * Set: cdCtrlCpfCnpjOriginal.
	 *
	 * @param cdCtrlCpfCnpjOriginal the cd ctrl cpf cnpj original
	 */
	public void setCdCtrlCpfCnpjOriginal(Integer cdCtrlCpfCnpjOriginal) {
		this.cdCtrlCpfCnpjOriginal = cdCtrlCpfCnpjOriginal;
	}
	
	/**
	 * Get: cdNumeroContrato.
	 *
	 * @return cdNumeroContrato
	 */
	public Long getCdNumeroContrato() {
		return cdNumeroContrato;
	}
	
	/**
	 * Set: cdNumeroContrato.
	 *
	 * @param cdNumeroContrato the cd numero contrato
	 */
	public void setCdNumeroContrato(Long cdNumeroContrato) {
		this.cdNumeroContrato = cdNumeroContrato;
	}
	
	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return cpfCnpjFormatado;
	}
	
	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}
	
	/**
	 * Get: cpfCnpjOriginalFormatado.
	 *
	 * @return cpfCnpjOriginalFormatado
	 */
	public String getCpfCnpjOriginalFormatado() {
		return cpfCnpjOriginalFormatado;
	}
	
	/**
	 * Set: cpfCnpjOriginalFormatado.
	 *
	 * @param cpfCnpjOriginalFormatado the cpf cnpj original formatado
	 */
	public void setCpfCnpjOriginalFormatado(String cpfCnpjOriginalFormatado) {
		this.cpfCnpjOriginalFormatado = cpfCnpjOriginalFormatado;
	}
}
