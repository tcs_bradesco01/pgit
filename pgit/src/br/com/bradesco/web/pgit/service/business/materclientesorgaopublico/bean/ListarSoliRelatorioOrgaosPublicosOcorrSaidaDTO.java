/*
 * Nome: br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean;

/**
 * Nome: ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO{
	
	/** Atributo cdSolicitacaoPagamento. */
	private Integer cdSolicitacaoPagamento;
	
	/** Atributo nrSolicitacao. */
	private Integer nrSolicitacao;
	
	/** Atributo dsDataSolicitacao. */
	private String dsDataSolicitacao;
	
	/** Atributo dsHoraSolicitacao. */
	private String dsHoraSolicitacao;
	
	/** Atributo cdSituacao. */
	private String cdSituacao;
	
	/** Atributo cdUsuario. */
	private String cdUsuario;
	
	/** Atributo dsNomeUsuario. */
	private String dsNomeUsuario;
	
	
	/**
	 * Get: dataHoraFormatada.
	 *
	 * @return dataHoraFormatada
	 */
	public String getDataHoraFormatada() {
		StringBuilder dataHoraFormatada = new StringBuilder();
		dataHoraFormatada.append(dsDataSolicitacao.replace('.', '/'));
		dataHoraFormatada.append(" - ");
		dataHoraFormatada.append(dsHoraSolicitacao);
		return dataHoraFormatada.toString();
	}
	
	/**
	 * Get: cdSolicitacaoPagamento.
	 *
	 * @return cdSolicitacaoPagamento
	 */
	public Integer getCdSolicitacaoPagamento() {
		return cdSolicitacaoPagamento;
	}
	
	/**
	 * Set: cdSolicitacaoPagamento.
	 *
	 * @param cdSolicitacaoPagamento the cd solicitacao pagamento
	 */
	public void setCdSolicitacaoPagamento(Integer cdSolicitacaoPagamento) {
		this.cdSolicitacaoPagamento = cdSolicitacaoPagamento;
	}
	
	/**
	 * Get: nrSolicitacao.
	 *
	 * @return nrSolicitacao
	 */
	public Integer getNrSolicitacao() {
		return nrSolicitacao;
	}
	
	/**
	 * Set: nrSolicitacao.
	 *
	 * @param nrSolicitacao the nr solicitacao
	 */
	public void setNrSolicitacao(Integer nrSolicitacao) {
		this.nrSolicitacao = nrSolicitacao;
	}
	
	/**
	 * Get: dsDataSolicitacao.
	 *
	 * @return dsDataSolicitacao
	 */
	public String getDsDataSolicitacao() {
		return dsDataSolicitacao;
	}
	
	/**
	 * Set: dsDataSolicitacao.
	 *
	 * @param dsDataSolicitacao the ds data solicitacao
	 */
	public void setDsDataSolicitacao(String dsDataSolicitacao) {
		this.dsDataSolicitacao = dsDataSolicitacao;
	}
	
	/**
	 * Get: dsHoraSolicitacao.
	 *
	 * @return dsHoraSolicitacao
	 */
	public String getDsHoraSolicitacao() {
		return dsHoraSolicitacao;
	}
	
	/**
	 * Set: dsHoraSolicitacao.
	 *
	 * @param dsHoraSolicitacao the ds hora solicitacao
	 */
	public void setDsHoraSolicitacao(String dsHoraSolicitacao) {
		this.dsHoraSolicitacao = dsHoraSolicitacao;
	}
	
	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public String getCdSituacao() {
		return cdSituacao;
	}
	
	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(String cdSituacao) {
		this.cdSituacao = cdSituacao;
	}
	
	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}
	
	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}
	
	/**
	 * Get: dsNomeUsuario.
	 *
	 * @return dsNomeUsuario
	 */
	public String getDsNomeUsuario() {
		return dsNomeUsuario;
	}
	
	/**
	 * Set: dsNomeUsuario.
	 *
	 * @param dsNomeUsuario the ds nome usuario
	 */
	public void setDsNomeUsuario(String dsNomeUsuario) {
		this.dsNomeUsuario = dsNomeUsuario;
	}
}