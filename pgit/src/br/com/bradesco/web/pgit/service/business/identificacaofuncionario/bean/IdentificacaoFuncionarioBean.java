/*
 * Nome: br.com.bradesco.web.pgit.service.business.identificacaofuncionario.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.identificacaofuncionario.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import br.com.bradesco.web.aq.application.log.ILogManager;

/**
 * Nome: IdentificacaoFuncionarioBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IdentificacaoFuncionarioBean {
	
	/** Atributo itemSelecionadoPesquisa. */
	private String itemSelecionadoPesquisa;
	
	/** Atributo codigo. */
	private String codigo;
	
	/** Atributo nome. */
	private String nome;
	
	/** Atributo codigoFuncionarioFiltro. */
	private String codigoFuncionarioFiltro;
	
	/** Atributo codigoJuncaoFiltro. */
	private String codigoJuncaoFiltro;
	
	/** Atributo codigoSecaoFiltro. */
	private String codigoSecaoFiltro;
	
	/** Atributo cpfFiltro1. */
	private String cpfFiltro1;
	
	/** Atributo cpfFiltro2. */
	private String cpfFiltro2;
	
	/** Atributo listaFuncionarios. */
	private List<FuncionarioDTO> listaFuncionarios;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio;
	
	/** Atributo mostraBotoes. */
	private boolean mostraBotoes;
	
	/** Atributo itemSelecionadoGrid. */
	private Integer itemSelecionadoGrid;
	
	/** Atributo tela. */
	private String tela;
	
	/** Atributo logger. */
	private ILogManager logger;
	
	
	/**
	 * Limpar.
	 */
	public void limpar(){
		limparDados();
		setListaFuncionarios(null);	
		setItemSelecionadoGrid(null);
		
	}
	
	/**
	 * Limpar campos pesquisa.
	 */
	public void limparCamposPesquisa(){
		setCodigoFuncionarioFiltro("");
		setCodigoJuncaoFiltro("");
		setCodigoSecaoFiltro("");
		setCpfFiltro1("");
		setCpfFiltro2("");
	}
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados(){
		setCodigoFuncionarioFiltro("");
		setCodigoJuncaoFiltro("");
		setCodigoSecaoFiltro("");
		setCpfFiltro1("");
		setCpfFiltro2("");
		
		setItemSelecionadoPesquisa(null);
		
		return "LIMPAR";
	}
	
	/**
	 * Get: logger.
	 *
	 * @return logger
	 */
	public ILogManager getLogger() {
		return logger;
	}

	/**
	 * Set: logger.
	 *
	 * @param logger the logger
	 */
	public void setLogger(ILogManager logger) {
		this.logger = logger;
	}

	/**
	 * Get: itemSelecionadoPesquisa.
	 *
	 * @return itemSelecionadoPesquisa
	 */
	public String getItemSelecionadoPesquisa() {
		return itemSelecionadoPesquisa;
	}

	/**
	 * Set: itemSelecionadoPesquisa.
	 *
	 * @param itemSelecionadoPesquisa the item selecionado pesquisa
	 */
	public void setItemSelecionadoPesquisa(String itemSelecionadoPesquisa) {
		this.itemSelecionadoPesquisa = itemSelecionadoPesquisa;
	}

	/**
	 * Get: nome.
	 *
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Set: nome.
	 *
	 * @param nome the nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Get: mostraBotoes.
	 *
	 * @return mostraBotoes
	 */
	public boolean getMostraBotoes() {
		return mostraBotoes;
	}

	/**
	 * Set: mostraBotoes.
	 *
	 * @param mostraBotoes the mostra botoes
	 */
	public void setMostraBotoes(boolean mostraBotoes) {
		this.mostraBotoes = mostraBotoes;
	}

	/**
	 * Get: itemSelecionadoGrid.
	 *
	 * @return itemSelecionadoGrid
	 */
	public Integer getItemSelecionadoGrid() {
		return itemSelecionadoGrid;
	}

	/**
	 * Set: itemSelecionadoGrid.
	 *
	 * @param itemSelecionadoGrid the item selecionado grid
	 */
	public void setItemSelecionadoGrid(Integer itemSelecionadoGrid) {
		this.itemSelecionadoGrid = itemSelecionadoGrid;
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){		
		return tela;
	}
	
	/**
	 * Limpar lista.
	 *
	 * @return the string
	 */
	public String limparLista(){
		setListaFuncionarios(null);
		return "";
	}

	/**
	 * Get: tela.
	 *
	 * @return tela
	 */
	public String getTela() {
		return tela;
	}

	/**
	 * Set: tela.
	 *
	 * @param tela the tela
	 */
	public void setTela(String tela) {
		this.tela = tela;
	}
	
	/**
	 * Confirmar.
	 *
	 * @return the string
	 */
	public String confirmar(){
		FuncionarioDTO funcionarioDTO = getListaFuncionarios().get(getItemSelecionadoGrid()-1);
		setCodigo(funcionarioDTO.getCodigo());
		setNome(funcionarioDTO.getNome());
		
		return tela;
	}
	
	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista(){
		
		listaFuncionarios = new ArrayList<FuncionarioDTO>();
		listaControleRadio = new ArrayList<SelectItem>();
		FuncionarioDTO funcionarioDTO = new FuncionarioDTO();
		funcionarioDTO.setCodigo("0000000001");
		funcionarioDTO.setNome("Jo�o Nogueira dos Santos");
		funcionarioDTO.setCodigoJuncao("1382");
		funcionarioDTO.setCodigoSecao("1468");
		funcionarioDTO.setCpfCnpj("999.999.999-99");
		listaControleRadio.add(new SelectItem(Integer.parseInt("1"),""));
		listaFuncionarios.add(funcionarioDTO);
		setItemSelecionadoGrid(null);
		
		setMostraBotoes(listaFuncionarios.size() > 10);
		
		return "";
		
	}
	
	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}
	
	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}
	
	/**
	 * Get: listaFuncionarios.
	 *
	 * @return listaFuncionarios
	 */
	public List<FuncionarioDTO> getListaFuncionarios() {
		return listaFuncionarios;
	}
	
	/**
	 * Set: listaFuncionarios.
	 *
	 * @param listaFuncionarios the lista funcionarios
	 */
	public void setListaFuncionarios(List<FuncionarioDTO> listaFuncionarios) {
		this.listaFuncionarios = listaFuncionarios;
	}
	
	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	
	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * Get: codigoFuncionarioFiltro.
	 *
	 * @return codigoFuncionarioFiltro
	 */
	public String getCodigoFuncionarioFiltro() {
		return codigoFuncionarioFiltro;
	}
	
	/**
	 * Set: codigoFuncionarioFiltro.
	 *
	 * @param codigoFuncionarioFiltro the codigo funcionario filtro
	 */
	public void setCodigoFuncionarioFiltro(String codigoFuncionarioFiltro) {
		this.codigoFuncionarioFiltro = codigoFuncionarioFiltro;
	}
	
	/**
	 * Get: codigoJuncaoFiltro.
	 *
	 * @return codigoJuncaoFiltro
	 */
	public String getCodigoJuncaoFiltro() {
		return codigoJuncaoFiltro;
	}
	
	/**
	 * Set: codigoJuncaoFiltro.
	 *
	 * @param codigoJuncaoFiltro the codigo juncao filtro
	 */
	public void setCodigoJuncaoFiltro(String codigoJuncaoFiltro) {
		this.codigoJuncaoFiltro = codigoJuncaoFiltro;
	}
	
	/**
	 * Get: codigoSecaoFiltro.
	 *
	 * @return codigoSecaoFiltro
	 */
	public String getCodigoSecaoFiltro() {
		return codigoSecaoFiltro;
	}
	
	/**
	 * Set: codigoSecaoFiltro.
	 *
	 * @param codigoSecaoFiltro the codigo secao filtro
	 */
	public void setCodigoSecaoFiltro(String codigoSecaoFiltro) {
		this.codigoSecaoFiltro = codigoSecaoFiltro;
	}
	
	/**
	 * Get: cpfFiltro1.
	 *
	 * @return cpfFiltro1
	 */
	public String getCpfFiltro1() {
		return cpfFiltro1;
	}
	
	/**
	 * Set: cpfFiltro1.
	 *
	 * @param cpfFiltro1 the cpf filtro1
	 */
	public void setCpfFiltro1(String cpfFiltro1) {
		this.cpfFiltro1 = cpfFiltro1;
	}
	
	/**
	 * Get: cpfFiltro2.
	 *
	 * @return cpfFiltro2
	 */
	public String getCpfFiltro2() {
		return cpfFiltro2;
	}
	
	/**
	 * Set: cpfFiltro2.
	 *
	 * @param cpfFiltro2 the cpf filtro2
	 */
	public void setCpfFiltro2(String cpfFiltro2) {
		this.cpfFiltro2 = cpfFiltro2;
	}


}
