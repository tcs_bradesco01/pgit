/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.listarregrasegregacaoacesso.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdRegraAcessoDado
     */
    private int _cdRegraAcessoDado = 0;

    /**
     * keeps track of state for field: _cdRegraAcessoDado
     */
    private boolean _has_cdRegraAcessoDado;

    /**
     * Field _cdTipoUnidadeUsuario
     */
    private int _cdTipoUnidadeUsuario = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeUsuario
     */
    private boolean _has_cdTipoUnidadeUsuario;

    /**
     * Field _dsTipoUnidadeUsuario
     */
    private java.lang.String _dsTipoUnidadeUsuario;

    /**
     * Field _cdTipoUnidadeProprietario
     */
    private int _cdTipoUnidadeProprietario = 0;

    /**
     * keeps track of state for field: _cdTipoUnidadeProprietario
     */
    private boolean _has_cdTipoUnidadeProprietario;

    /**
     * Field _dsTipoUnidadeProprietario
     */
    private java.lang.String _dsTipoUnidadeProprietario;

    /**
     * Field _cdIndicadorAcao
     */
    private int _cdIndicadorAcao = 0;

    /**
     * keeps track of state for field: _cdIndicadorAcao
     */
    private boolean _has_cdIndicadorAcao;

    /**
     * Field _dsIndicadorAcao
     */
    private java.lang.String _dsIndicadorAcao;

    /**
     * Field _cdNivelPermissaoAcesso
     */
    private int _cdNivelPermissaoAcesso = 0;

    /**
     * keeps track of state for field: _cdNivelPermissaoAcesso
     */
    private boolean _has_cdNivelPermissaoAcesso;

    /**
     * Field _dsNivelPermissaoAcesso
     */
    private java.lang.String _dsNivelPermissaoAcesso;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarregrasegregacaoacesso.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIndicadorAcao
     * 
     */
    public void deleteCdIndicadorAcao()
    {
        this._has_cdIndicadorAcao= false;
    } //-- void deleteCdIndicadorAcao() 

    /**
     * Method deleteCdNivelPermissaoAcesso
     * 
     */
    public void deleteCdNivelPermissaoAcesso()
    {
        this._has_cdNivelPermissaoAcesso= false;
    } //-- void deleteCdNivelPermissaoAcesso() 

    /**
     * Method deleteCdRegraAcessoDado
     * 
     */
    public void deleteCdRegraAcessoDado()
    {
        this._has_cdRegraAcessoDado= false;
    } //-- void deleteCdRegraAcessoDado() 

    /**
     * Method deleteCdTipoUnidadeProprietario
     * 
     */
    public void deleteCdTipoUnidadeProprietario()
    {
        this._has_cdTipoUnidadeProprietario= false;
    } //-- void deleteCdTipoUnidadeProprietario() 

    /**
     * Method deleteCdTipoUnidadeUsuario
     * 
     */
    public void deleteCdTipoUnidadeUsuario()
    {
        this._has_cdTipoUnidadeUsuario= false;
    } //-- void deleteCdTipoUnidadeUsuario() 

    /**
     * Returns the value of field 'cdIndicadorAcao'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorAcao'.
     */
    public int getCdIndicadorAcao()
    {
        return this._cdIndicadorAcao;
    } //-- int getCdIndicadorAcao() 

    /**
     * Returns the value of field 'cdNivelPermissaoAcesso'.
     * 
     * @return int
     * @return the value of field 'cdNivelPermissaoAcesso'.
     */
    public int getCdNivelPermissaoAcesso()
    {
        return this._cdNivelPermissaoAcesso;
    } //-- int getCdNivelPermissaoAcesso() 

    /**
     * Returns the value of field 'cdRegraAcessoDado'.
     * 
     * @return int
     * @return the value of field 'cdRegraAcessoDado'.
     */
    public int getCdRegraAcessoDado()
    {
        return this._cdRegraAcessoDado;
    } //-- int getCdRegraAcessoDado() 

    /**
     * Returns the value of field 'cdTipoUnidadeProprietario'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeProprietario'.
     */
    public int getCdTipoUnidadeProprietario()
    {
        return this._cdTipoUnidadeProprietario;
    } //-- int getCdTipoUnidadeProprietario() 

    /**
     * Returns the value of field 'cdTipoUnidadeUsuario'.
     * 
     * @return int
     * @return the value of field 'cdTipoUnidadeUsuario'.
     */
    public int getCdTipoUnidadeUsuario()
    {
        return this._cdTipoUnidadeUsuario;
    } //-- int getCdTipoUnidadeUsuario() 

    /**
     * Returns the value of field 'dsIndicadorAcao'.
     * 
     * @return String
     * @return the value of field 'dsIndicadorAcao'.
     */
    public java.lang.String getDsIndicadorAcao()
    {
        return this._dsIndicadorAcao;
    } //-- java.lang.String getDsIndicadorAcao() 

    /**
     * Returns the value of field 'dsNivelPermissaoAcesso'.
     * 
     * @return String
     * @return the value of field 'dsNivelPermissaoAcesso'.
     */
    public java.lang.String getDsNivelPermissaoAcesso()
    {
        return this._dsNivelPermissaoAcesso;
    } //-- java.lang.String getDsNivelPermissaoAcesso() 

    /**
     * Returns the value of field 'dsTipoUnidadeProprietario'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeProprietario'.
     */
    public java.lang.String getDsTipoUnidadeProprietario()
    {
        return this._dsTipoUnidadeProprietario;
    } //-- java.lang.String getDsTipoUnidadeProprietario() 

    /**
     * Returns the value of field 'dsTipoUnidadeUsuario'.
     * 
     * @return String
     * @return the value of field 'dsTipoUnidadeUsuario'.
     */
    public java.lang.String getDsTipoUnidadeUsuario()
    {
        return this._dsTipoUnidadeUsuario;
    } //-- java.lang.String getDsTipoUnidadeUsuario() 

    /**
     * Method hasCdIndicadorAcao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorAcao()
    {
        return this._has_cdIndicadorAcao;
    } //-- boolean hasCdIndicadorAcao() 

    /**
     * Method hasCdNivelPermissaoAcesso
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdNivelPermissaoAcesso()
    {
        return this._has_cdNivelPermissaoAcesso;
    } //-- boolean hasCdNivelPermissaoAcesso() 

    /**
     * Method hasCdRegraAcessoDado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRegraAcessoDado()
    {
        return this._has_cdRegraAcessoDado;
    } //-- boolean hasCdRegraAcessoDado() 

    /**
     * Method hasCdTipoUnidadeProprietario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeProprietario()
    {
        return this._has_cdTipoUnidadeProprietario;
    } //-- boolean hasCdTipoUnidadeProprietario() 

    /**
     * Method hasCdTipoUnidadeUsuario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoUnidadeUsuario()
    {
        return this._has_cdTipoUnidadeUsuario;
    } //-- boolean hasCdTipoUnidadeUsuario() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIndicadorAcao'.
     * 
     * @param cdIndicadorAcao the value of field 'cdIndicadorAcao'.
     */
    public void setCdIndicadorAcao(int cdIndicadorAcao)
    {
        this._cdIndicadorAcao = cdIndicadorAcao;
        this._has_cdIndicadorAcao = true;
    } //-- void setCdIndicadorAcao(int) 

    /**
     * Sets the value of field 'cdNivelPermissaoAcesso'.
     * 
     * @param cdNivelPermissaoAcesso the value of field
     * 'cdNivelPermissaoAcesso'.
     */
    public void setCdNivelPermissaoAcesso(int cdNivelPermissaoAcesso)
    {
        this._cdNivelPermissaoAcesso = cdNivelPermissaoAcesso;
        this._has_cdNivelPermissaoAcesso = true;
    } //-- void setCdNivelPermissaoAcesso(int) 

    /**
     * Sets the value of field 'cdRegraAcessoDado'.
     * 
     * @param cdRegraAcessoDado the value of field
     * 'cdRegraAcessoDado'.
     */
    public void setCdRegraAcessoDado(int cdRegraAcessoDado)
    {
        this._cdRegraAcessoDado = cdRegraAcessoDado;
        this._has_cdRegraAcessoDado = true;
    } //-- void setCdRegraAcessoDado(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeProprietario'.
     * 
     * @param cdTipoUnidadeProprietario the value of field
     * 'cdTipoUnidadeProprietario'.
     */
    public void setCdTipoUnidadeProprietario(int cdTipoUnidadeProprietario)
    {
        this._cdTipoUnidadeProprietario = cdTipoUnidadeProprietario;
        this._has_cdTipoUnidadeProprietario = true;
    } //-- void setCdTipoUnidadeProprietario(int) 

    /**
     * Sets the value of field 'cdTipoUnidadeUsuario'.
     * 
     * @param cdTipoUnidadeUsuario the value of field
     * 'cdTipoUnidadeUsuario'.
     */
    public void setCdTipoUnidadeUsuario(int cdTipoUnidadeUsuario)
    {
        this._cdTipoUnidadeUsuario = cdTipoUnidadeUsuario;
        this._has_cdTipoUnidadeUsuario = true;
    } //-- void setCdTipoUnidadeUsuario(int) 

    /**
     * Sets the value of field 'dsIndicadorAcao'.
     * 
     * @param dsIndicadorAcao the value of field 'dsIndicadorAcao'.
     */
    public void setDsIndicadorAcao(java.lang.String dsIndicadorAcao)
    {
        this._dsIndicadorAcao = dsIndicadorAcao;
    } //-- void setDsIndicadorAcao(java.lang.String) 

    /**
     * Sets the value of field 'dsNivelPermissaoAcesso'.
     * 
     * @param dsNivelPermissaoAcesso the value of field
     * 'dsNivelPermissaoAcesso'.
     */
    public void setDsNivelPermissaoAcesso(java.lang.String dsNivelPermissaoAcesso)
    {
        this._dsNivelPermissaoAcesso = dsNivelPermissaoAcesso;
    } //-- void setDsNivelPermissaoAcesso(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoUnidadeProprietario'.
     * 
     * @param dsTipoUnidadeProprietario the value of field
     * 'dsTipoUnidadeProprietario'.
     */
    public void setDsTipoUnidadeProprietario(java.lang.String dsTipoUnidadeProprietario)
    {
        this._dsTipoUnidadeProprietario = dsTipoUnidadeProprietario;
    } //-- void setDsTipoUnidadeProprietario(java.lang.String) 

    /**
     * Sets the value of field 'dsTipoUnidadeUsuario'.
     * 
     * @param dsTipoUnidadeUsuario the value of field
     * 'dsTipoUnidadeUsuario'.
     */
    public void setDsTipoUnidadeUsuario(java.lang.String dsTipoUnidadeUsuario)
    {
        this._dsTipoUnidadeUsuario = dsTipoUnidadeUsuario;
    } //-- void setDsTipoUnidadeUsuario(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.listarregrasegregacaoacesso.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.listarregrasegregacaoacesso.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.listarregrasegregacaoacesso.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.listarregrasegregacaoacesso.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
