/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarInformacoesFavorecidoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarInformacoesFavorecidoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _cdFavorecido
     */
    private long _cdFavorecido = 0;

    /**
     * keeps track of state for field: _cdFavorecido
     */
    private boolean _has_cdFavorecido;

    /**
     * Field _cdTipoIsncricaoFavorecido
     */
    private int _cdTipoIsncricaoFavorecido = 0;

    /**
     * keeps track of state for field: _cdTipoIsncricaoFavorecido
     */
    private boolean _has_cdTipoIsncricaoFavorecido;

    /**
     * Field _nrInscricaoFavorecido
     */
    private long _nrInscricaoFavorecido = 0;

    /**
     * keeps track of state for field: _nrInscricaoFavorecido
     */
    private boolean _has_nrInscricaoFavorecido;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarInformacoesFavorecidoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.request.ConsultarInformacoesFavorecidoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdFavorecido
     * 
     */
    public void deleteCdFavorecido()
    {
        this._has_cdFavorecido= false;
    } //-- void deleteCdFavorecido() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoIsncricaoFavorecido
     * 
     */
    public void deleteCdTipoIsncricaoFavorecido()
    {
        this._has_cdTipoIsncricaoFavorecido= false;
    } //-- void deleteCdTipoIsncricaoFavorecido() 

    /**
     * Method deleteNrInscricaoFavorecido
     * 
     */
    public void deleteNrInscricaoFavorecido()
    {
        this._has_nrInscricaoFavorecido= false;
    } //-- void deleteNrInscricaoFavorecido() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdFavorecido'.
     * 
     * @return long
     * @return the value of field 'cdFavorecido'.
     */
    public long getCdFavorecido()
    {
        return this._cdFavorecido;
    } //-- long getCdFavorecido() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @return int
     * @return the value of field 'cdTipoIsncricaoFavorecido'.
     */
    public int getCdTipoIsncricaoFavorecido()
    {
        return this._cdTipoIsncricaoFavorecido;
    } //-- int getCdTipoIsncricaoFavorecido() 

    /**
     * Returns the value of field 'nrInscricaoFavorecido'.
     * 
     * @return long
     * @return the value of field 'nrInscricaoFavorecido'.
     */
    public long getNrInscricaoFavorecido()
    {
        return this._nrInscricaoFavorecido;
    } //-- long getNrInscricaoFavorecido() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecido()
    {
        return this._has_cdFavorecido;
    } //-- boolean hasCdFavorecido() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoIsncricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoIsncricaoFavorecido()
    {
        return this._has_cdTipoIsncricaoFavorecido;
    } //-- boolean hasCdTipoIsncricaoFavorecido() 

    /**
     * Method hasNrInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrInscricaoFavorecido()
    {
        return this._has_nrInscricaoFavorecido;
    } //-- boolean hasNrInscricaoFavorecido() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdFavorecido'.
     * 
     * @param cdFavorecido the value of field 'cdFavorecido'.
     */
    public void setCdFavorecido(long cdFavorecido)
    {
        this._cdFavorecido = cdFavorecido;
        this._has_cdFavorecido = true;
    } //-- void setCdFavorecido(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoIsncricaoFavorecido'.
     * 
     * @param cdTipoIsncricaoFavorecido the value of field
     * 'cdTipoIsncricaoFavorecido'.
     */
    public void setCdTipoIsncricaoFavorecido(int cdTipoIsncricaoFavorecido)
    {
        this._cdTipoIsncricaoFavorecido = cdTipoIsncricaoFavorecido;
        this._has_cdTipoIsncricaoFavorecido = true;
    } //-- void setCdTipoIsncricaoFavorecido(int) 

    /**
     * Sets the value of field 'nrInscricaoFavorecido'.
     * 
     * @param nrInscricaoFavorecido the value of field
     * 'nrInscricaoFavorecido'.
     */
    public void setNrInscricaoFavorecido(long nrInscricaoFavorecido)
    {
        this._nrInscricaoFavorecido = nrInscricaoFavorecido;
        this._has_nrInscricaoFavorecido = true;
    } //-- void setNrInscricaoFavorecido(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarInformacoesFavorecidoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.request.ConsultarInformacoesFavorecidoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.request.ConsultarInformacoesFavorecidoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.request.ConsultarInformacoesFavorecidoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarinformacoesfavorecido.request.ConsultarInformacoesFavorecidoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
