/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

/**
 * Nome: ListarContasRelacionadasHistSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarContasRelacionadasHistSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo cdCorpoCpfCnpjParticipante. */
	private Long cdCorpoCpfCnpjParticipante;

	/** Atributo cdFilialCpfCnpjParticipante. */
	private Integer cdFilialCpfCnpjParticipante;

	/** Atributo cdDigitoCpfCnpjParticipante. */
	private Integer cdDigitoCpfCnpjParticipante;

	/** Atributo cdBancoRelacionamento. */
	private Integer cdBancoRelacionamento;

	/** Atributo dsBancoRelacionamento. */
	private String dsBancoRelacionamento;

	/** Atributo cdAgenciaRelacionamento. */
	private Integer cdAgenciaRelacionamento;

	/** Atributo dsAgenciaRelacionamento. */
	private String dsAgenciaRelacionamento;

	/** Atributo cdContaRelacionamento. */
	private Long cdContaRelacionamento;

	/** Atributo dsTipoContaRelacionamentoConta. */
	private String dsTipoContaRelacionamentoConta;

	/** Atributo cdDigitoContaRelacionamento. */
	private String cdDigitoContaRelacionamento;

	/** Atributo cdTipoConta. */
	private Integer cdTipoConta;

	/** Atributo dsTipoConta. */
	private String dsTipoConta;

	/** Atributo cdPessoaJuridicaRelacionamento. */
	private Long cdPessoaJuridicaRelacionamento;

	/** Atributo cdTipoContratoRelacionamento. */
	private Integer cdTipoContratoRelacionamento;

	/** Atributo nrSequenciaContratoRelacionamento. */
	private Long nrSequenciaContratoRelacionamento;

	/** Atributo hrInclusaoManutencao. */
	private String hrInclusaoManutencao;

	/** Atributo cdUsuarioManutencao. */
	private String cdUsuarioManutencao;

	/** Atributo dtManutencao. */
	private String dtManutencao;

	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;

	/** Atributo hrManutencao. */
	private String hrManutencao;

	/** Atributo nmParticipante. */
	private String nmParticipante;

	/** Atributo cpfCnpjParticipanteFormatado. */
	private String cpfCnpjParticipanteFormatado;

	/** Atributo bancoFormatado. */
	private String bancoFormatado;

	/** Atributo agenciaFormatada. */
	private String agenciaFormatada;

	/** Atributo contaFormatada. */
	private String contaFormatada;

	/** Atributo dataHoraManutencaoFormatada. */
	private String dataHoraManutencaoFormatada;
	
	/** Atributo cdTipoRelacionamento. */
	private Integer cdTipoRelacionamento;
	
	/** Atributo cPssoa. */
	private Long cPssoa;

	/**
	 * Get: cdTipoRelacionamento.
	 *
	 * @return cdTipoRelacionamento
	 */
	public Integer getCdTipoRelacionamento() {
		return cdTipoRelacionamento;
	}

	/**
	 * Set: cdTipoRelacionamento.
	 *
	 * @param cdTipoRelacionamento the cd tipo relacionamento
	 */
	public void setCdTipoRelacionamento(Integer cdTipoRelacionamento) {
		this.cdTipoRelacionamento = cdTipoRelacionamento;
	}

	/**
	 * Get: dataHoraManutencaoFormatada.
	 *
	 * @return dataHoraManutencaoFormatada
	 */
	public String getDataHoraManutencaoFormatada() {
		return dataHoraManutencaoFormatada;
	}

	/**
	 * Set: dataHoraManutencaoFormatada.
	 *
	 * @param dataHoraManutencaoFormatada the data hora manutencao formatada
	 */
	public void setDataHoraManutencaoFormatada(String dataHoraManutencaoFormatada) {
		this.dataHoraManutencaoFormatada = dataHoraManutencaoFormatada;
	}

	/**
	 * Get: cdAgenciaRelacionamento.
	 *
	 * @return cdAgenciaRelacionamento
	 */
	public Integer getCdAgenciaRelacionamento() {
		return cdAgenciaRelacionamento;
	}

	/**
	 * Set: cdAgenciaRelacionamento.
	 *
	 * @param cdAgenciaRelacionamento the cd agencia relacionamento
	 */
	public void setCdAgenciaRelacionamento(Integer cdAgenciaRelacionamento) {
		this.cdAgenciaRelacionamento = cdAgenciaRelacionamento;
	}

	/**
	 * Get: cdBancoRelacionamento.
	 *
	 * @return cdBancoRelacionamento
	 */
	public Integer getCdBancoRelacionamento() {
		return cdBancoRelacionamento;
	}

	/**
	 * Set: cdBancoRelacionamento.
	 *
	 * @param cdBancoRelacionamento the cd banco relacionamento
	 */
	public void setCdBancoRelacionamento(Integer cdBancoRelacionamento) {
		this.cdBancoRelacionamento = cdBancoRelacionamento;
	}

	/**
	 * Get: cdContaRelacionamento.
	 *
	 * @return cdContaRelacionamento
	 */
	public Long getCdContaRelacionamento() {
		return cdContaRelacionamento;
	}

	/**
	 * Set: cdContaRelacionamento.
	 *
	 * @param cdContaRelacionamento the cd conta relacionamento
	 */
	public void setCdContaRelacionamento(Long cdContaRelacionamento) {
		this.cdContaRelacionamento = cdContaRelacionamento;
	}

	/**
	 * Get: cdCorpoCpfCnpjParticipante.
	 *
	 * @return cdCorpoCpfCnpjParticipante
	 */
	public Long getCdCorpoCpfCnpjParticipante() {
		return cdCorpoCpfCnpjParticipante;
	}

	/**
	 * Set: cdCorpoCpfCnpjParticipante.
	 *
	 * @param cdCorpoCpfCnpjParticipante the cd corpo cpf cnpj participante
	 */
	public void setCdCorpoCpfCnpjParticipante(Long cdCorpoCpfCnpjParticipante) {
		this.cdCorpoCpfCnpjParticipante = cdCorpoCpfCnpjParticipante;
	}

	/**
	 * Get: cdDigitoContaRelacionamento.
	 *
	 * @return cdDigitoContaRelacionamento
	 */
	public String getCdDigitoContaRelacionamento() {
		return cdDigitoContaRelacionamento;
	}

	/**
	 * Set: cdDigitoContaRelacionamento.
	 *
	 * @param cdDigitoContaRelacionamento the cd digito conta relacionamento
	 */
	public void setCdDigitoContaRelacionamento(String cdDigitoContaRelacionamento) {
		this.cdDigitoContaRelacionamento = cdDigitoContaRelacionamento;
	}

	/**
	 * Get: cdDigitoCpfCnpjParticipante.
	 *
	 * @return cdDigitoCpfCnpjParticipante
	 */
	public Integer getCdDigitoCpfCnpjParticipante() {
		return cdDigitoCpfCnpjParticipante;
	}

	/**
	 * Set: cdDigitoCpfCnpjParticipante.
	 *
	 * @param cdDigitoCpfCnpjParticipante the cd digito cpf cnpj participante
	 */
	public void setCdDigitoCpfCnpjParticipante(Integer cdDigitoCpfCnpjParticipante) {
		this.cdDigitoCpfCnpjParticipante = cdDigitoCpfCnpjParticipante;
	}

	/**
	 * Get: cdFilialCpfCnpjParticipante.
	 *
	 * @return cdFilialCpfCnpjParticipante
	 */
	public Integer getCdFilialCpfCnpjParticipante() {
		return cdFilialCpfCnpjParticipante;
	}

	/**
	 * Set: cdFilialCpfCnpjParticipante.
	 *
	 * @param cdFilialCpfCnpjParticipante the cd filial cpf cnpj participante
	 */
	public void setCdFilialCpfCnpjParticipante(Integer cdFilialCpfCnpjParticipante) {
		this.cdFilialCpfCnpjParticipante = cdFilialCpfCnpjParticipante;
	}

	/**
	 * Get: cdPessoaJuridicaRelacionamento.
	 *
	 * @return cdPessoaJuridicaRelacionamento
	 */
	public Long getCdPessoaJuridicaRelacionamento() {
		return cdPessoaJuridicaRelacionamento;
	}

	/**
	 * Set: cdPessoaJuridicaRelacionamento.
	 *
	 * @param cdPessoaJuridicaRelacionamento the cd pessoa juridica relacionamento
	 */
	public void setCdPessoaJuridicaRelacionamento(Long cdPessoaJuridicaRelacionamento) {
		this.cdPessoaJuridicaRelacionamento = cdPessoaJuridicaRelacionamento;
	}

	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}

	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}

	/**
	 * Get: cdTipoContratoRelacionamento.
	 *
	 * @return cdTipoContratoRelacionamento
	 */
	public Integer getCdTipoContratoRelacionamento() {
		return cdTipoContratoRelacionamento;
	}

	/**
	 * Set: cdTipoContratoRelacionamento.
	 *
	 * @param cdTipoContratoRelacionamento the cd tipo contrato relacionamento
	 */
	public void setCdTipoContratoRelacionamento(Integer cdTipoContratoRelacionamento) {
		this.cdTipoContratoRelacionamento = cdTipoContratoRelacionamento;
	}

	/**
	 * Get: cdUsuarioManutencao.
	 *
	 * @return cdUsuarioManutencao
	 */
	public String getCdUsuarioManutencao() {
		return cdUsuarioManutencao;
	}

	/**
	 * Set: cdUsuarioManutencao.
	 *
	 * @param cdUsuarioManutencao the cd usuario manutencao
	 */
	public void setCdUsuarioManutencao(String cdUsuarioManutencao) {
		this.cdUsuarioManutencao = cdUsuarioManutencao;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsAgenciaRelacionamento.
	 *
	 * @return dsAgenciaRelacionamento
	 */
	public String getDsAgenciaRelacionamento() {
		return dsAgenciaRelacionamento;
	}

	/**
	 * Set: dsAgenciaRelacionamento.
	 *
	 * @param dsAgenciaRelacionamento the ds agencia relacionamento
	 */
	public void setDsAgenciaRelacionamento(String dsAgenciaRelacionamento) {
		this.dsAgenciaRelacionamento = dsAgenciaRelacionamento;
	}

	/**
	 * Get: dsBancoRelacionamento.
	 *
	 * @return dsBancoRelacionamento
	 */
	public String getDsBancoRelacionamento() {
		return dsBancoRelacionamento;
	}

	/**
	 * Set: dsBancoRelacionamento.
	 *
	 * @param dsBancoRelacionamento the ds banco relacionamento
	 */
	public void setDsBancoRelacionamento(String dsBancoRelacionamento) {
		this.dsBancoRelacionamento = dsBancoRelacionamento;
	}

	/**
	 * Get: dtManutencao.
	 *
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 *
	 * @param dtManutencao the dt manutencao
	 */
	public void setDtManutencao(String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: hrInclusaoManutencao.
	 *
	 * @return hrInclusaoManutencao
	 */
	public String getHrInclusaoManutencao() {
		return hrInclusaoManutencao;
	}

	/**
	 * Set: hrInclusaoManutencao.
	 *
	 * @param hrInclusaoManutencao the hr inclusao manutencao
	 */
	public void setHrInclusaoManutencao(String hrInclusaoManutencao) {
		this.hrInclusaoManutencao = hrInclusaoManutencao;
	}

	/**
	 * Get: hrManutencao.
	 *
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 *
	 * @param hrManutencao the hr manutencao
	 */
	public void setHrManutencao(String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: nmParticipante.
	 *
	 * @return nmParticipante
	 */
	public String getNmParticipante() {
		return nmParticipante;
	}

	/**
	 * Set: nmParticipante.
	 *
	 * @param nmParticipante the nm participante
	 */
	public void setNmParticipante(String nmParticipante) {
		this.nmParticipante = nmParticipante;
	}

	/**
	 * Get: nrSequenciaContratoRelacionamento.
	 *
	 * @return nrSequenciaContratoRelacionamento
	 */
	public Long getNrSequenciaContratoRelacionamento() {
		return nrSequenciaContratoRelacionamento;
	}

	/**
	 * Set: nrSequenciaContratoRelacionamento.
	 *
	 * @param nrSequenciaContratoRelacionamento the nr sequencia contrato relacionamento
	 */
	public void setNrSequenciaContratoRelacionamento(Long nrSequenciaContratoRelacionamento) {
		this.nrSequenciaContratoRelacionamento = nrSequenciaContratoRelacionamento;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: agenciaFormatada.
	 *
	 * @return agenciaFormatada
	 */
	public String getAgenciaFormatada() {
		return agenciaFormatada;
	}

	/**
	 * Set: agenciaFormatada.
	 *
	 * @param agenciaFormatada the agencia formatada
	 */
	public void setAgenciaFormatada(String agenciaFormatada) {
		this.agenciaFormatada = agenciaFormatada;
	}

	/**
	 * Get: bancoFormatado.
	 *
	 * @return bancoFormatado
	 */
	public String getBancoFormatado() {
		return bancoFormatado;
	}

	/**
	 * Set: bancoFormatado.
	 *
	 * @param bancoFormatado the banco formatado
	 */
	public void setBancoFormatado(String bancoFormatado) {
		this.bancoFormatado = bancoFormatado;
	}

	/**
	 * Get: contaFormatada.
	 *
	 * @return contaFormatada
	 */
	public String getContaFormatada() {
		return contaFormatada;
	}

	/**
	 * Set: contaFormatada.
	 *
	 * @param contaFormatada the conta formatada
	 */
	public void setContaFormatada(String contaFormatada) {
		this.contaFormatada = contaFormatada;
	}

	/**
	 * Get: cpfCnpjParticipanteFormatado.
	 *
	 * @return cpfCnpjParticipanteFormatado
	 */
	public String getCpfCnpjParticipanteFormatado() {
		return cpfCnpjParticipanteFormatado;
	}

	/**
	 * Set: cpfCnpjParticipanteFormatado.
	 *
	 * @param cpfCnpjParticipanteFormatado the cpf cnpj participante formatado
	 */
	public void setCpfCnpjParticipanteFormatado(String cpfCnpjParticipanteFormatado) {
		this.cpfCnpjParticipanteFormatado = cpfCnpjParticipanteFormatado;
	}

	/**
	 * Get: dsTipoContaRelacionamentoConta.
	 *
	 * @return dsTipoContaRelacionamentoConta
	 */
	public String getDsTipoContaRelacionamentoConta() {
		return dsTipoContaRelacionamentoConta;
	}

	/**
	 * Set: dsTipoContaRelacionamentoConta.
	 *
	 * @param dsTipoContaRelacionamentoConta the ds tipo conta relacionamento conta
	 */
	public void setDsTipoContaRelacionamentoConta(String dsTipoContaRelacionamentoConta) {
		this.dsTipoContaRelacionamentoConta = dsTipoContaRelacionamentoConta;
	}

	/**
	 * Get: dsTipoConta.
	 *
	 * @return dsTipoConta
	 */
	public String getDsTipoConta() {
		return dsTipoConta;
	}

	/**
	 * Set: dsTipoConta.
	 *
	 * @param dsTipoConta the ds tipo conta
	 */
	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}

	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	public Long getcPssoa() {
		return cPssoa;
	}

	public void setcPssoa(Long cPssoa) {
		this.cPssoa = cPssoa;
	}

}