/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarEmpresaTransmissaoArquivoVanSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarEmpresaTransmissaoArquivoVanSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;

	/** Atributo cdTipoParticipacao. */
	private Long cdTipoParticipacao;

	/** Atributo dsTipoParticipacao. */
	private String dsTipoParticipacao;
	
	/**
	 * Listar empresa transmissao arquivo van saida dto.
	 */
	public ListarEmpresaTransmissaoArquivoVanSaidaDTO() {
		super();
	}

	/**
	 * Listar empresa transmissao arquivo van saida dto.
	 *
	 * @param cdTipoParticipacao the cd tipo participacao
	 * @param dsTipoParticipacao the ds tipo participacao
	 */
	public ListarEmpresaTransmissaoArquivoVanSaidaDTO(Long cdTipoParticipacao, String dsTipoParticipacao) {
		super();
		this.cdTipoParticipacao = cdTipoParticipacao;
		this.dsTipoParticipacao = dsTipoParticipacao;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdTipoParticipacao.
	 *
	 * @return cdTipoParticipacao
	 */
	public Long getCdTipoParticipacao() {
		return cdTipoParticipacao;
	}

	/**
	 * Set: cdTipoParticipacao.
	 *
	 * @param cdTipoParticipacao the cd tipo participacao
	 */
	public void setCdTipoParticipacao(Long cdTipoParticipacao) {
		this.cdTipoParticipacao = cdTipoParticipacao;
	}

	/**
	 * Get: dsTipoParticipacao.
	 *
	 * @return dsTipoParticipacao
	 */
	public String getDsTipoParticipacao() {
		return dsTipoParticipacao;
	}

	/**
	 * Set: dsTipoParticipacao.
	 *
	 * @param dsTipoParticipacao the ds tipo participacao
	 */
	public void setDsTipoParticipacao(String dsTipoParticipacao) {
		this.dsTipoParticipacao = dsTipoParticipacao;
	}

}
