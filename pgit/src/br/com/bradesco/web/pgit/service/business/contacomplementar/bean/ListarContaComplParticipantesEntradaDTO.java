package br.com.bradesco.web.pgit.service.business.contacomplementar.bean;

public class ListarContaComplParticipantesEntradaDTO {
	
	private Integer maxOcorrencias;
	private Long cdpessoaJuridicaContrato;
	private Integer cdTipoContratoNegocio;
	private Long nrSequenciaContratoNegocio;
	private Long cdCorpoCpfCnpj;
	private Integer cdFilialCpfCnpj;
	private Integer cdControleCpfCnpj;
	
	public Integer getMaxOcorrencias() {
		return maxOcorrencias;
	}
	public void setMaxOcorrencias(Integer maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}
	public Integer getCdFilialCpfCnpj() {
		return cdFilialCpfCnpj;
	}
	public void setCdFilialCpfCnpj(Integer cdFilialCpfCnpj) {
		this.cdFilialCpfCnpj = cdFilialCpfCnpj;
	}
	public Integer getCdControleCpfCnpj() {
		return cdControleCpfCnpj;
	}
	public void setCdControleCpfCnpj(Integer cdControleCpfCnpj) {
		this.cdControleCpfCnpj = cdControleCpfCnpj;
	}
}