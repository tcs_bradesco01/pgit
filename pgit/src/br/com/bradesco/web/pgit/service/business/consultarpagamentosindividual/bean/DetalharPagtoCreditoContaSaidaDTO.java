/*
 * Nome: br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean;

import java.math.BigDecimal;

/**
 * Nome: DetalharPagtoCreditoContaSaidaDTO
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class DetalharPagtoCreditoContaSaidaDTO {

	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo vlDesconto. */
	private BigDecimal vlDesconto;

	/** Atributo vlAbatimento. */
	private BigDecimal vlAbatimento;

	/** Atributo vlMulta. */
	private BigDecimal vlMulta;

	/** Atributo vlMora. */
	private BigDecimal vlMora;

	/** Atributo vlDeducao. */
	private BigDecimal vlDeducao;

	/** Atributo vlAcrescimo. */
	private BigDecimal vlAcrescimo;

	/** Atributo vlImpostoRenda. */
	private BigDecimal vlImpostoRenda;

	/** Atributo vlIss. */
	private BigDecimal vlIss;

	/** Atributo vlIof. */
	private BigDecimal vlIof;

	/** Atributo vlInss. */
	private BigDecimal vlInss;

	/** Atributo cdBancoDestino. */
	private Integer cdBancoDestino;

	/** Atributo cdAgenciaDestino. */
	private Integer cdAgenciaDestino;

	/** Atributo cdDigitoAgenciaDestino. */
	private String cdDigitoAgenciaDestino;

	/** Atributo cdContaDestino. */
	private long cdContaDestino;

	/** Atributo numControleInternoLote. */
	private Long numControleInternoLote;

	/** Atributo cdDigitoContaDestino. */
	private String cdDigitoContaDestino;

	/** Atributo dsBancoDestino. */
	private String dsBancoDestino;

	/** Atributo dsAgenciaDestino. */
	private String dsAgenciaDestino;

	/** Atributo dsTipoContaDestino. */
	private String dsTipoContaDestino;

	/** Atributo cdIndicadorModalidade. */
	private Integer cdIndicadorModalidade; // campoOculto

	/** Atributo dsPagamento. */
	private String dsPagamento;

	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;

	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;

	/** Atributo dsTipoContaDebito. */
	private String dsTipoContaDebito;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo dtDevolucaoEstorno. */
	private String dtDevolucaoEstorno;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo dtAgendamento. */
	private String dtAgendamento;

	/** Atributo vlrAgendamento. */
	private BigDecimal vlrAgendamento;

	/** Atributo vlrEfetivacao. */
	private BigDecimal vlrEfetivacao;

	/** Atributo cdIndicadorEconomicoMoeda. */
	private Integer cdIndicadorEconomicoMoeda;

	/** Atributo qtMoeda. */
	private BigDecimal qtMoeda;

	/** Atributo dtVencimento. */
	private String dtVencimento;

	/** Atributo dsMensagemPrimeiraLinha. */
	private String dsMensagemPrimeiraLinha;

	/** Atributo dsMensagemSegundaLinha. */
	private String dsMensagemSegundaLinha;

	/** Atributo dsUsoEmpresa. */
	private String dsUsoEmpresa;

	/** Atributo cdSituacaoOperacaoPagamento. */
	private Integer cdSituacaoOperacaoPagamento;

	/** Atributo cdMotivoSituacaoPagamento. */
	private Integer cdMotivoSituacaoPagamento; // campoOculto

	/** Atributo cdListaDebito. */
	private String cdListaDebito;

	/** Atributo nrDocumento. */
	private String nrDocumento;

	/** Atributo cdSerieDocumento. */
	private String cdSerieDocumento;

	/** Atributo cdTipoDocumento. */
	private int cdTipoDocumento;

	/** Atributo dsTipoDocumento. */
	private String dsTipoDocumento;

	/** Atributo vlDocumento. */
	private BigDecimal vlDocumento;

	/** Atributo dtEmissaoDocumento. */
	private String dtEmissaoDocumento;

	/** Atributo nrSequenciaArquivoRemessa. */
	private String nrSequenciaArquivoRemessa;

	/** Atributo nrLoteArquivoRemessa. */
	private String nrLoteArquivoRemessa;

	/** Atributo cdFavorecido. */
	private String cdFavorecido;

	/** Atributo dsBancoFavorecido. */
	private String dsBancoFavorecido;

	/** Atributo dsAgenciaFavorecido. */
	private String dsAgenciaFavorecido;

	/** Atributo cdTipoContaFavorecido. */
	private Integer cdTipoContaFavorecido; // campoOculto

	/** Atributo dsTipoContaFavorecido. */
	private String dsTipoContaFavorecido;

	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;

	/** Atributo cdTipoManutencao. */
	private Integer cdTipoManutencao; // campoOculto

	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao; // campoOculto

	/** Atributo dtInclusao. */
	private String dtInclusao;
	
	/** Atributo dsIdentificadorTransferenciaPagto. */
	private String dsIdentificadorTransferenciaPagto;

	/** Atributo hrInclusao. */
	private String hrInclusao;

	/** Atributo cdUsuarioInclusaoInterno. */
	private String cdUsuarioInclusaoInterno;

	/** Atributo cdUsuarioInclusaoExterno. */
	private String cdUsuarioInclusaoExterno;

	/** Atributo cdTipoCanalInclusao. */
	private Integer cdTipoCanalInclusao;

	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;

	/** Atributo cdFluxoInclusao. */
	private String cdFluxoInclusao;

	/** Atributo dtManutencao. */
	private String dtManutencao;

	/** Atributo hrManutencao. */
	private String hrManutencao;

	/** Atributo cdUsuarioManutencaoInterno. */
	private String cdUsuarioManutencaoInterno;

	/** Atributo cdUsuarioManutencaoExterno. */
	private String cdUsuarioManutencaoExterno;

	/** Atributo cdTipoCanalManutencao. */
	private Integer cdTipoCanalManutencao;

	/** Atributo dsTipoCanalManutencao. */
	private String dsTipoCanalManutencao;

	/** Atributo cdFluxoManutencao. */
	private String cdFluxoManutencao;

	/** Atributo cdSituacaoTranferenciaAutomatica. */
	private String cdSituacaoTranferenciaAutomatica;

	/** Atributo contaCreditoFormatada. */
	private String contaCreditoFormatada;

	/** Atributo bancoDestinoFormatado. */
	private String bancoDestinoFormatado;

	/** Atributo agenciaDestinoFormatada. */
	private String agenciaDestinoFormatada;

	/** Atributo contaDestinoFormatada. */
	private String contaDestinoFormatada;

	/** Atributo dataHoraInclusaoFormatada. */
	private String dataHoraInclusaoFormatada;

	/** Atributo usuarioInclusaoFormatado. */
	private String usuarioInclusaoFormatado;

	/** Atributo tipoCanalInclusaoFormatado. */
	private String tipoCanalInclusaoFormatado;

	/** Atributo complementoInclusaoFormatado. */
	private String complementoInclusaoFormatado;

	/** Atributo dataHoraManutencaoFormatada. */
	private String dataHoraManutencaoFormatada;

	/** Atributo usuarioManutencaoFormatado. */
	private String usuarioManutencaoFormatado;

	/** Atributo tipoCanalManutencaoFormatado. */
	private String tipoCanalManutencaoFormatado;

	/** Atributo complementoManutencaoFormatado. */
	private String complementoManutencaoFormatado;

	/** Atributo dsMoeda. */
	private String dsMoeda;

	/** Atributo cdCpfCnpjCliente. */
	private Long cdCpfCnpjCliente;

	/** Atributo nomeCliente. */
	private String nomeCliente;

	/** Atributo cdBancoDebito. */
	private Integer cdBancoDebito;

	/** Atributo cdAgenciaDebito. */
	private Integer cdAgenciaDebito;

	/** Atributo cdDigAgenciaDebto. */
	private String cdDigAgenciaDebto;

	/** Atributo cdContaDebito. */
	private Long cdContaDebito;

	/** Atributo dsDigAgenciaDebito. */
	private String dsDigAgenciaDebito;

	/** Atributo nmInscriFavorecido. */
	private String nmInscriFavorecido;

	/** Atributo dsNomeFavorecido. */
	private String dsNomeFavorecido;

	/** Atributo cdBancoCredito. */
	private Integer cdBancoCredito;

	/** Atributo cdAgenciaCredito. */
	private Integer cdAgenciaCredito;

	/** Atributo cdDigAgenciaCredito. */
	private String cdDigAgenciaCredito;

	/** Atributo cdContaCredito. */
	private Long cdContaCredito;

	/** Atributo cdDigContaCredito. */
	private String cdDigContaCredito;

	/** Atributo nmInscriBeneficio. */
	private String nmInscriBeneficio;

	/** Atributo cdTipoInscriBeneficio. */
	private String cdTipoInscriBeneficio;

	/** Atributo cdNomeBeneficio. */
	private String cdNomeBeneficio;

	/** Atributo dtPagamento. */
	private String dtPagamento;

	/** Atributo cdSituacaoPagamento. */
	private String cdSituacaoPagamento;

	/** Atributo cdIndicadorAuto. */
	private Integer cdIndicadorAuto;

	/** Atributo cdIndicadorSemConsulta. */
	private Integer cdIndicadorSemConsulta;

	/** Atributo cdTipoInscricaoFavorecido. */
	private Integer cdTipoInscricaoFavorecido;

	/** Atributo dsPessoaJuridicaContrato. */
	private String dsPessoaJuridicaContrato;

	/** Atributo nrSequenciaContratoNegocio. */
	private String nrSequenciaContratoNegocio;

	/** Atributo dsTipoServicoOperacao. */
	private String dsTipoServicoOperacao;

	/** Atributo dsModalidadeRelacionado. */
	private String dsModalidadeRelacionado;

	/** Atributo cdControlePagamento. */
	private String cdControlePagamento;

	/** Atributo cdBancoOriginal. */
	private Integer cdBancoOriginal;

	/** Atributo dsBancoOriginal. */
	private String dsBancoOriginal;

	/** Atributo cdAgenciaBancariaOriginal. */
	private Integer cdAgenciaBancariaOriginal;

	/** Atributo cdDigitoAgenciaOriginal. */
	private String cdDigitoAgenciaOriginal;

	/** Atributo dsAgenciaOriginal. */
	private String dsAgenciaOriginal;

	/** Atributo cdContaBancariaOriginal. */
	private Long cdContaBancariaOriginal;

	/** Atributo cdDigitoContaOriginal. */
	private String cdDigitoContaOriginal;

	/** Atributo dsTipoContaOriginal. */
	private String dsTipoContaOriginal;

	/** Atributo dtFloatingPagamento. */
	private String dtFloatingPagamento;

	/** Atributo dtEfetivFloatPgto. */
	private String dtEfetivFloatPgto;

	/** Atributo vlFloatingPagamento. */
	private BigDecimal vlFloatingPagamento;

	/** Atributo cdOperacaoDcom. */
	private Long cdOperacaoDcom;

	/** Atributo dsSituacaoDcom. */
	private String dsSituacaoDcom;

	/** Atributo dsEstornoPagamento. */
	private String dsEstornoPagamento;

	/** The cd lote interno. */
	private Long cdLoteInterno;
	
	/** The ds indicador autorizacao. */
	private String dsIndicadorAutorizacao;
	
	/** The ds tipo layout. */
	private String dsTipoLayout;
	
	/** The vl efetivacao debito pagamento. */
	private BigDecimal vlEfetivacaoDebitoPagamento;		
	
	/** The vl efetivacao credito pagamento. */
	private BigDecimal vlEfetivacaoCreditoPagamento;	
	
	/** The vl desconto pagamento. */
	private BigDecimal vlDescontoPagamento;				
	
	/** The hr efetivacao credito pagamento. */
	private String hrEfetivacaoCreditoPagamento;		
	
	/** The hr envio credito pagamento. */
	private String hrEnvioCreditoPagamento;			
	
	/** The cd identificador transferencia pagto. */
	private Integer cdIdentificadorTransferenciaPagto;	
	
	/** The cd mensagem lin extrato. */
	private Integer cdMensagemLinExtrato;				
	
	/** The cd conve cta salarial. */
	private Long cdConveCtaSalarial;				
	
	private String cdIspbPagtoDestino;
	
	private String contaPagtoDestino;
	
	/**
	 * Get: dsMoeda.
	 * 
	 * @return dsMoeda
	 */
	public String getDsMoeda() {
		return dsMoeda;
	}

	/**
	 * Set: dsMoeda.
	 * 
	 * @param dsMoeda
	 *            the ds moeda
	 */
	public void setDsMoeda(final String dsMoeda) {
		this.dsMoeda = dsMoeda;
	}

	/**
	 * Get: cdAgenciaDestino.
	 * 
	 * @return cdAgenciaDestino
	 */
	public Integer getCdAgenciaDestino() {
		return cdAgenciaDestino;
	}

	/**
	 * Set: cdAgenciaDestino.
	 * 
	 * @param cdAgenciaDestino
	 *            the cd agencia destino
	 */
	public void setCdAgenciaDestino(final Integer cdAgenciaDestino) {
		this.cdAgenciaDestino = cdAgenciaDestino;
	}

	/**
	 * Get: cdBancoDestino.
	 * 
	 * @return cdBancoDestino
	 */
	public Integer getCdBancoDestino() {
		return cdBancoDestino;
	}

	/**
	 * Set: cdBancoDestino.
	 * 
	 * @param cdBancoDestino
	 *            the cd banco destino
	 */
	public void setCdBancoDestino(final Integer cdBancoDestino) {
		this.cdBancoDestino = cdBancoDestino;
	}

	/**
	 * Get: cdContaDestino.
	 * 
	 * @return cdContaDestino
	 */
	public long getCdContaDestino() {
		return cdContaDestino;
	}

	/**
	 * Set: cdContaDestino.
	 * 
	 * @param cdContaDestino
	 *            the cd conta destino
	 */
	public void setCdContaDestino(final long cdContaDestino) {
		this.cdContaDestino = cdContaDestino;
	}

	/**
	 * Get: cdDigitoAgenciaDestino.
	 * 
	 * @return cdDigitoAgenciaDestino
	 */
	public String getCdDigitoAgenciaDestino() {
		return cdDigitoAgenciaDestino;
	}

	/**
	 * Set: cdDigitoAgenciaDestino.
	 * 
	 * @param cdDigitoAgenciaDestino
	 *            the cd digito agencia destino
	 */
	public void setCdDigitoAgenciaDestino(final String cdDigitoAgenciaDestino) {
		this.cdDigitoAgenciaDestino = cdDigitoAgenciaDestino;
	}

	/**
	 * Get: cdDigitoContaDestino.
	 * 
	 * @return cdDigitoContaDestino
	 */
	public String getCdDigitoContaDestino() {
		return cdDigitoContaDestino;
	}

	/**
	 * Set: cdDigitoContaDestino.
	 * 
	 * @param cdDigitoContaDestino
	 *            the cd digito conta destino
	 */
	public void setCdDigitoContaDestino(final String cdDigitoContaDestino) {
		this.cdDigitoContaDestino = cdDigitoContaDestino;
	}

	/**
	 * Get: cdFavorecido.
	 * 
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 * 
	 * @param cdFavorecido
	 *            the cd favorecido
	 */
	public void setCdFavorecido(final String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdFluxoInclusao.
	 * 
	 * @return cdFluxoInclusao
	 */
	public String getCdFluxoInclusao() {
		return cdFluxoInclusao;
	}

	/**
	 * Set: cdFluxoInclusao.
	 * 
	 * @param cdFluxoInclusao
	 *            the cd fluxo inclusao
	 */
	public void setCdFluxoInclusao(final String cdFluxoInclusao) {
		this.cdFluxoInclusao = cdFluxoInclusao;
	}

	/**
	 * Get: cdFluxoManutencao.
	 * 
	 * @return cdFluxoManutencao
	 */
	public String getCdFluxoManutencao() {
		return cdFluxoManutencao;
	}

	/**
	 * Set: cdFluxoManutencao.
	 * 
	 * @param cdFluxoManutencao
	 *            the cd fluxo manutencao
	 */
	public void setCdFluxoManutencao(final String cdFluxoManutencao) {
		this.cdFluxoManutencao = cdFluxoManutencao;
	}

	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 * 
	 * @return cdIndicadorEconomicoMoeda
	 */
	public Integer getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}

	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 * 
	 * @param cdIndicadorEconomicoMoeda
	 *            the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(final Integer cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}

	/**
	 * Get: cdIndicadorModalidade.
	 * 
	 * @return cdIndicadorModalidade
	 */
	public Integer getCdIndicadorModalidade() {
		return cdIndicadorModalidade;
	}

	/**
	 * Set: cdIndicadorModalidade.
	 * 
	 * @param cdIndicadorModalidade
	 *            the cd indicador modalidade
	 */
	public void setCdIndicadorModalidade(final Integer cdIndicadorModalidade) {
		this.cdIndicadorModalidade = cdIndicadorModalidade;
	}

	/**
	 * Get: cdListaDebito.
	 * 
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}

	/**
	 * Set: cdListaDebito.
	 * 
	 * @param cdListaDebito
	 *            the cd lista debito
	 */
	public void setCdListaDebito(final String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}

	/**
	 * Get: cdMotivoSituacaoPagamento.
	 * 
	 * @return cdMotivoSituacaoPagamento
	 */
	public Integer getCdMotivoSituacaoPagamento() {
		return cdMotivoSituacaoPagamento;
	}

	/**
	 * Set: cdMotivoSituacaoPagamento.
	 * 
	 * @param cdMotivoSituacaoPagamento
	 *            the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(final Integer cdMotivoSituacaoPagamento) {
		this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}

	/**
	 * Get: cdSerieDocumento.
	 * 
	 * @return cdSerieDocumento
	 */
	public String getCdSerieDocumento() {
		return cdSerieDocumento;
	}

	/**
	 * Set: cdSerieDocumento.
	 * 
	 * @param cdSerieDocumento
	 *            the cd serie documento
	 */
	public void setCdSerieDocumento(final String cdSerieDocumento) {
		this.cdSerieDocumento = cdSerieDocumento;
	}

	/**
	 * Get: cdSituacaoContrato.
	 * 
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 * 
	 * @param cdSituacaoContrato
	 *            the cd situacao contrato
	 */
	public void setCdSituacaoContrato(final String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 * 
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 * 
	 * @param cdSituacaoOperacaoPagamento
	 *            the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(
			final Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 * 
	 * @return cdTipoCanalInclusao
	 */
	public Integer getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 * 
	 * @param cdTipoCanalInclusao
	 *            the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(final Integer cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoCanalManutencao.
	 * 
	 * @return cdTipoCanalManutencao
	 */
	public Integer getCdTipoCanalManutencao() {
		return cdTipoCanalManutencao;
	}

	/**
	 * Set: cdTipoCanalManutencao.
	 * 
	 * @param cdTipoCanalManutencao
	 *            the cd tipo canal manutencao
	 */
	public void setCdTipoCanalManutencao(final Integer cdTipoCanalManutencao) {
		this.cdTipoCanalManutencao = cdTipoCanalManutencao;
	}

	/**
	 * Get: cdTipoContaFavorecido.
	 * 
	 * @return cdTipoContaFavorecido
	 */
	public Integer getCdTipoContaFavorecido() {
		return cdTipoContaFavorecido;
	}

	/**
	 * Set: cdTipoContaFavorecido.
	 * 
	 * @param cdTipoContaFavorecido
	 *            the cd tipo conta favorecido
	 */
	public void setCdTipoContaFavorecido(final Integer cdTipoContaFavorecido) {
		this.cdTipoContaFavorecido = cdTipoContaFavorecido;
	}

	/**
	 * Get: cdTipoManutencao.
	 * 
	 * @return cdTipoManutencao
	 */
	public Integer getCdTipoManutencao() {
		return cdTipoManutencao;
	}

	/**
	 * Set: cdTipoManutencao.
	 * 
	 * @param cdTipoManutencao
	 *            the cd tipo manutencao
	 */
	public void setCdTipoManutencao(final Integer cdTipoManutencao) {
		this.cdTipoManutencao = cdTipoManutencao;
	}

	/**
	 * Get: cdUsuarioInclusaoExterno.
	 * 
	 * @return cdUsuarioInclusaoExterno
	 */
	public String getCdUsuarioInclusaoExterno() {
		return cdUsuarioInclusaoExterno;
	}

	/**
	 * Set: cdUsuarioInclusaoExterno.
	 * 
	 * @param cdUsuarioInclusaoExterno
	 *            the cd usuario inclusao externo
	 */
	public void setCdUsuarioInclusaoExterno(final String cdUsuarioInclusaoExterno) {
		this.cdUsuarioInclusaoExterno = cdUsuarioInclusaoExterno;
	}

	/**
	 * Get: cdUsuarioManutencaoExterno.
	 * 
	 * @return cdUsuarioManutencaoExterno
	 */
	public String getCdUsuarioManutencaoExterno() {
		return cdUsuarioManutencaoExterno;
	}

	/**
	 * Set: cdUsuarioManutencaoExterno.
	 * 
	 * @param cdUsuarioManutencaoExterno
	 *            the cd usuario manutencao externo
	 */
	public void setCdUsuarioManutencaoExterno(final String cdUsuarioManutencaoExterno) {
		this.cdUsuarioManutencaoExterno = cdUsuarioManutencaoExterno;
	}

	/**
	 * Get: codMensagem.
	 * 
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 * 
	 * @param codMensagem
	 *            the cod mensagem
	 */
	public void setCodMensagem(final String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsAgenciaDebito.
	 * 
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 * 
	 * @param dsAgenciaDebito
	 *            the ds agencia debito
	 */
	public void setDsAgenciaDebito(final String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsAgenciaDestino.
	 * 
	 * @return dsAgenciaDestino
	 */
	public String getDsAgenciaDestino() {
		return dsAgenciaDestino;
	}

	/**
	 * Set: dsAgenciaDestino.
	 * 
	 * @param dsAgenciaDestino
	 *            the ds agencia destino
	 */
	public void setDsAgenciaDestino(final String dsAgenciaDestino) {
		this.dsAgenciaDestino = dsAgenciaDestino;
	}

	/**
	 * Get: dsAgenciaFavorecido.
	 * 
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}

	/**
	 * Set: dsAgenciaFavorecido.
	 * 
	 * @param dsAgenciaFavorecido
	 *            the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(final String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}

	/**
	 * Get: dsBancoDebito.
	 * 
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 * 
	 * @param dsBancoDebito
	 *            the ds banco debito
	 */
	public void setDsBancoDebito(final String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: dsBancoDestino.
	 * 
	 * @return dsBancoDestino
	 */
	public String getDsBancoDestino() {
		return dsBancoDestino;
	}

	/**
	 * Set: dsBancoDestino.
	 * 
	 * @param dsBancoDestino
	 *            the ds banco destino
	 */
	public void setDsBancoDestino(final String dsBancoDestino) {
		this.dsBancoDestino = dsBancoDestino;
	}

	/**
	 * Get: dsBancoFavorecido.
	 * 
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}

	/**
	 * Set: dsBancoFavorecido.
	 * 
	 * @param dsBancoFavorecido
	 *            the ds banco favorecido
	 */
	public void setDsBancoFavorecido(final String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}

	/**
	 * Get: dsContrato.
	 * 
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 * 
	 * @param dsContrato
	 *            the ds contrato
	 */
	public void setDsContrato(final String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsMensagemPrimeiraLinha.
	 * 
	 * @return dsMensagemPrimeiraLinha
	 */
	public String getDsMensagemPrimeiraLinha() {
		return dsMensagemPrimeiraLinha;
	}

	/**
	 * Set: dsMensagemPrimeiraLinha.
	 * 
	 * @param dsMensagemPrimeiraLinha
	 *            the ds mensagem primeira linha
	 */
	public void setDsMensagemPrimeiraLinha(final String dsMensagemPrimeiraLinha) {
		this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
	}

	/**
	 * Get: dsMensagemSegundaLinha.
	 * 
	 * @return dsMensagemSegundaLinha
	 */
	public String getDsMensagemSegundaLinha() {
		return dsMensagemSegundaLinha;
	}

	/**
	 * Set: dsMensagemSegundaLinha.
	 * 
	 * @param dsMensagemSegundaLinha
	 *            the ds mensagem segunda linha
	 */
	public void setDsMensagemSegundaLinha(final String dsMensagemSegundaLinha) {
		this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
	}

	/**
	 * Get: dsMotivoSituacao.
	 * 
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 * 
	 * @param dsMotivoSituacao
	 *            the ds motivo situacao
	 */
	public void setDsMotivoSituacao(final String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	/**
	 * Get: dsPagamento.
	 * 
	 * @return dsPagamento
	 */
	public String getDsPagamento() {
		return dsPagamento;
	}

	/**
	 * Set: dsPagamento.
	 * 
	 * @param dsPagamento
	 *            the ds pagamento
	 */
	public void setDsPagamento(final String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}

	/**
	 * Get: dsTipoCanalInclusao.
	 * 
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}

	/**
	 * Set: dsTipoCanalInclusao.
	 * 
	 * @param dsTipoCanalInclusao
	 *            the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(final String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	/**
	 * Get: dsTipoCanalManutencao.
	 * 
	 * @return dsTipoCanalManutencao
	 */
	public String getDsTipoCanalManutencao() {
		return dsTipoCanalManutencao;
	}

	/**
	 * Set: dsTipoCanalManutencao.
	 * 
	 * @param dsTipoCanalManutencao
	 *            the ds tipo canal manutencao
	 */
	public void setDsTipoCanalManutencao(final String dsTipoCanalManutencao) {
		this.dsTipoCanalManutencao = dsTipoCanalManutencao;
	}

	/**
	 * Get: dsTipoContaDebito.
	 * 
	 * @return dsTipoContaDebito
	 */
	public String getDsTipoContaDebito() {
		return dsTipoContaDebito;
	}

	/**
	 * Set: dsTipoContaDebito.
	 * 
	 * @param dsTipoContaDebito
	 *            the ds tipo conta debito
	 */
	public void setDsTipoContaDebito(final String dsTipoContaDebito) {
		this.dsTipoContaDebito = dsTipoContaDebito;
	}

	/**
	 * Get: dsTipoContaDestino.
	 * 
	 * @return dsTipoContaDestino
	 */
	public String getDsTipoContaDestino() {
		return dsTipoContaDestino;
	}

	/**
	 * Set: dsTipoContaDestino.
	 * 
	 * @param dsTipoContaDestino
	 *            the ds tipo conta destino
	 */
	public void setDsTipoContaDestino(final String dsTipoContaDestino) {
		this.dsTipoContaDestino = dsTipoContaDestino;
	}

	/**
	 * Get: dsTipoContaFavorecido.
	 * 
	 * @return dsTipoContaFavorecido
	 */
	public String getDsTipoContaFavorecido() {
		return dsTipoContaFavorecido;
	}

	/**
	 * Set: dsTipoContaFavorecido.
	 * 
	 * @param dsTipoContaFavorecido
	 *            the ds tipo conta favorecido
	 */
	public void setDsTipoContaFavorecido(final String dsTipoContaFavorecido) {
		this.dsTipoContaFavorecido = dsTipoContaFavorecido;
	}

	/**
	 * Get: dsTipoManutencao.
	 * 
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 * 
	 * @param dsTipoManutencao
	 *            the ds tipo manutencao
	 */
	public void setDsTipoManutencao(final String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	/**
	 * Get: dsUsoEmpresa.
	 * 
	 * @return dsUsoEmpresa
	 */
	public String getDsUsoEmpresa() {
		return dsUsoEmpresa;
	}

	/**
	 * Set: dsUsoEmpresa.
	 * 
	 * @param dsUsoEmpresa
	 *            the ds uso empresa
	 */
	public void setDsUsoEmpresa(final String dsUsoEmpresa) {
		this.dsUsoEmpresa = dsUsoEmpresa;
	}

	/**
	 * Get: dtAgendamento.
	 * 
	 * @return dtAgendamento
	 */
	public String getDtAgendamento() {
		return dtAgendamento;
	}

	/**
	 * Set: dtAgendamento.
	 * 
	 * @param dtAgendamento
	 *            the dt agendamento
	 */
	public void setDtAgendamento(final String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	/**
	 * Get: dtEmissaoDocumento.
	 * 
	 * @return dtEmissaoDocumento
	 */
	public String getDtEmissaoDocumento() {
		return dtEmissaoDocumento;
	}

	/**
	 * Set: dtEmissaoDocumento.
	 * 
	 * @param dtEmissaoDocumento
	 *            the dt emissao documento
	 */
	public void setDtEmissaoDocumento(final String dtEmissaoDocumento) {
		this.dtEmissaoDocumento = dtEmissaoDocumento;
	}

	/**
	 * Get: dtInclusao.
	 * 
	 * @return dtInclusao
	 */
	public String getDtInclusao() {
		return dtInclusao;
	}

	/**
	 * Set: dtInclusao.
	 * 
	 * @param dtInclusao
	 *            the dt inclusao
	 */
	public void setDtInclusao(final String dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	/**
	 * Get: dtManutencao.
	 * 
	 * @return dtManutencao
	 */
	public String getDtManutencao() {
		return dtManutencao;
	}

	/**
	 * Set: dtManutencao.
	 * 
	 * @param dtManutencao
	 *            the dt manutencao
	 */
	public void setDtManutencao(final String dtManutencao) {
		this.dtManutencao = dtManutencao;
	}

	/**
	 * Get: dtVencimento.
	 * 
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 * 
	 * @param dtVencimento
	 *            the dt vencimento
	 */
	public void setDtVencimento(final String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: hrInclusao.
	 * 
	 * @return hrInclusao
	 */
	public String getHrInclusao() {
		return hrInclusao;
	}

	/**
	 * Set: hrInclusao.
	 * 
	 * @param hrInclusao
	 *            the hr inclusao
	 */
	public void setHrInclusao(final String hrInclusao) {
		this.hrInclusao = hrInclusao;
	}

	/**
	 * Get: hrManutencao.
	 * 
	 * @return hrManutencao
	 */
	public String getHrManutencao() {
		return hrManutencao;
	}

	/**
	 * Set: hrManutencao.
	 * 
	 * @param hrManutencao
	 *            the hr manutencao
	 */
	public void setHrManutencao(final String hrManutencao) {
		this.hrManutencao = hrManutencao;
	}

	/**
	 * Get: mensagem.
	 * 
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 * 
	 * @param mensagem
	 *            the mensagem
	 */
	public void setMensagem(final String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nrDocumento.
	 * 
	 * @return nrDocumento
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}

	/**
	 * Set: nrDocumento.
	 * 
	 * @param nrDocumento
	 *            the nr documento
	 */
	public void setNrDocumento(final String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	/**
	 * Get: nrLoteArquivoRemessa.
	 * 
	 * @return nrLoteArquivoRemessa
	 */
	public String getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}

	/**
	 * Set: nrLoteArquivoRemessa.
	 * 
	 * @param nrLoteArquivoRemessa
	 *            the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(final String nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}

	/**
	 * Get: nrSequenciaArquivoRemessa.
	 * 
	 * @return nrSequenciaArquivoRemessa
	 */
	public String getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}

	/**
	 * Set: nrSequenciaArquivoRemessa.
	 * 
	 * @param nrSequenciaArquivoRemessa
	 *            the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(final String nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}

	/**
	 * Get: qtMoeda.
	 * 
	 * @return qtMoeda
	 */
	public BigDecimal getQtMoeda() {
		return qtMoeda;
	}

	/**
	 * Set: qtMoeda.
	 * 
	 * @param qtMoeda
	 *            the qt moeda
	 */
	public void setQtMoeda(final BigDecimal qtMoeda) {
		this.qtMoeda = qtMoeda;
	}

	/**
	 * Get: vlAbatimento.
	 * 
	 * @return vlAbatimento
	 */
	public BigDecimal getVlAbatimento() {
		return vlAbatimento;
	}

	/**
	 * Set: vlAbatimento.
	 * 
	 * @param vlAbatimento
	 *            the vl abatimento
	 */
	public void setVlAbatimento(final BigDecimal vlAbatimento) {
		this.vlAbatimento = vlAbatimento;
	}

	/**
	 * Get: vlAcrescimo.
	 * 
	 * @return vlAcrescimo
	 */
	public BigDecimal getVlAcrescimo() {
		return vlAcrescimo;
	}

	/**
	 * Set: vlAcrescimo.
	 * 
	 * @param vlAcrescimo
	 *            the vl acrescimo
	 */
	public void setVlAcrescimo(final BigDecimal vlAcrescimo) {
		this.vlAcrescimo = vlAcrescimo;
	}

	/**
	 * Get: vlDeducao.
	 * 
	 * @return vlDeducao
	 */
	public BigDecimal getVlDeducao() {
		return vlDeducao;
	}

	/**
	 * Set: vlDeducao.
	 * 
	 * @param vlDeducao
	 *            the vl deducao
	 */
	public void setVlDeducao(final BigDecimal vlDeducao) {
		this.vlDeducao = vlDeducao;
	}

	/**
	 * Get: vlDesconto.
	 * 
	 * @return vlDesconto
	 */
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}

	/**
	 * Set: vlDesconto.
	 * 
	 * @param vlDesconto
	 *            the vl desconto
	 */
	public void setVlDesconto(final BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	/**
	 * Get: vlDocumento.
	 * 
	 * @return vlDocumento
	 */
	public BigDecimal getVlDocumento() {
		return vlDocumento;
	}

	/**
	 * Set: vlDocumento.
	 * 
	 * @param vlDocumento
	 *            the vl documento
	 */
	public void setVlDocumento(final BigDecimal vlDocumento) {
		this.vlDocumento = vlDocumento;
	}

	/**
	 * Get: vlImpostoRenda.
	 * 
	 * @return vlImpostoRenda
	 */
	public BigDecimal getVlImpostoRenda() {
		return vlImpostoRenda;
	}

	/**
	 * Set: vlImpostoRenda.
	 * 
	 * @param vlImpostoRenda
	 *            the vl imposto renda
	 */
	public void setVlImpostoRenda(final BigDecimal vlImpostoRenda) {
		this.vlImpostoRenda = vlImpostoRenda;
	}

	/**
	 * Get: vlInss.
	 * 
	 * @return vlInss
	 */
	public BigDecimal getVlInss() {
		return vlInss;
	}

	/**
	 * Set: vlInss.
	 * 
	 * @param vlInss
	 *            the vl inss
	 */
	public void setVlInss(final BigDecimal vlInss) {
		this.vlInss = vlInss;
	}

	/**
	 * Get: vlIof.
	 * 
	 * @return vlIof
	 */
	public BigDecimal getVlIof() {
		return vlIof;
	}

	/**
	 * Set: vlIof.
	 * 
	 * @param vlIof
	 *            the vl iof
	 */
	public void setVlIof(final BigDecimal vlIof) {
		this.vlIof = vlIof;
	}

	/**
	 * Get: vlIss.
	 * 
	 * @return vlIss
	 */
	public BigDecimal getVlIss() {
		return vlIss;
	}

	/**
	 * Set: vlIss.
	 * 
	 * @param vlIss
	 *            the vl iss
	 */
	public void setVlIss(final BigDecimal vlIss) {
		this.vlIss = vlIss;
	}

	/**
	 * Get: vlMora.
	 * 
	 * @return vlMora
	 */
	public BigDecimal getVlMora() {
		return vlMora;
	}

	/**
	 * Set: vlMora.
	 * 
	 * @param vlMora
	 *            the vl mora
	 */
	public void setVlMora(final BigDecimal vlMora) {
		this.vlMora = vlMora;
	}

	/**
	 * Get: vlMulta.
	 * 
	 * @return vlMulta
	 */
	public BigDecimal getVlMulta() {
		return vlMulta;
	}

	/**
	 * Set: vlMulta.
	 * 
	 * @param vlMulta
	 *            the vl multa
	 */
	public void setVlMulta(final BigDecimal vlMulta) {
		this.vlMulta = vlMulta;
	}

	/**
	 * Get: vlrAgendamento.
	 * 
	 * @return vlrAgendamento
	 */
	public BigDecimal getVlrAgendamento() {
		return vlrAgendamento;
	}

	/**
	 * Set: vlrAgendamento.
	 * 
	 * @param vlrAgendamento
	 *            the vlr agendamento
	 */
	public void setVlrAgendamento(final BigDecimal vlrAgendamento) {
		this.vlrAgendamento = vlrAgendamento;
	}

	/**
	 * Get: vlrEfetivacao.
	 * 
	 * @return vlrEfetivacao
	 */
	public BigDecimal getVlrEfetivacao() {
		return vlrEfetivacao;
	}

	/**
	 * Set: vlrEfetivacao.
	 * 
	 * @param vlrEfetivacao
	 *            the vlr efetivacao
	 */
	public void setVlrEfetivacao(final BigDecimal vlrEfetivacao) {
		this.vlrEfetivacao = vlrEfetivacao;
	}

	/**
	 * Get: cdUsuarioInclusaoInterno.
	 * 
	 * @return cdUsuarioInclusaoInterno
	 */
	public String getCdUsuarioInclusaoInterno() {
		return cdUsuarioInclusaoInterno;
	}

	/**
	 * Set: cdUsuarioInclusaoInterno.
	 * 
	 * @param cdUsuarioInclusaoInterno
	 *            the cd usuario inclusao interno
	 */
	public void setCdUsuarioInclusaoInterno(final String cdUsuarioInclusaoInterno) {
		this.cdUsuarioInclusaoInterno = cdUsuarioInclusaoInterno;
	}

	/**
	 * Get: cdUsuarioManutencaoInterno.
	 * 
	 * @return cdUsuarioManutencaoInterno
	 */
	public String getCdUsuarioManutencaoInterno() {
		return cdUsuarioManutencaoInterno;
	}

	/**
	 * Set: cdUsuarioManutencaoInterno.
	 * 
	 * @param cdUsuarioManutencaoInterno
	 *            the cd usuario manutencao interno
	 */
	public void setCdUsuarioManutencaoInterno(final String cdUsuarioManutencaoInterno) {
		this.cdUsuarioManutencaoInterno = cdUsuarioManutencaoInterno;
	}

	/**
	 * Get: cdTipoDocumento.
	 * 
	 * @return cdTipoDocumento
	 */
	public int getCdTipoDocumento() {
		return cdTipoDocumento;
	}

	/**
	 * Set: cdTipoDocumento.
	 * 
	 * @param cdTipoDocumento
	 *            the cd tipo documento
	 */
	public void setCdTipoDocumento(final int cdTipoDocumento) {
		this.cdTipoDocumento = cdTipoDocumento;
	}

	/**
	 * Get: dsTipoDocumento.
	 * 
	 * @return dsTipoDocumento
	 */
	public String getDsTipoDocumento() {
		return dsTipoDocumento;
	}

	/**
	 * Set: dsTipoDocumento.
	 * 
	 * @param dsTipoDocumento
	 *            the ds tipo documento
	 */
	public void setDsTipoDocumento(final String dsTipoDocumento) {
		this.dsTipoDocumento = dsTipoDocumento;
	}

	/**
	 * Get: agenciaDestinoFormatada.
	 * 
	 * @return agenciaDestinoFormatada
	 */
	public String getAgenciaDestinoFormatada() {
		return agenciaDestinoFormatada;
	}

	/**
	 * Set: agenciaDestinoFormatada.
	 * 
	 * @param agenciaDestinoFormatada
	 *            the agencia destino formatada
	 */
	public void setAgenciaDestinoFormatada(final String agenciaDestinoFormatada) {
		this.agenciaDestinoFormatada = agenciaDestinoFormatada;
	}

	/**
	 * Get: bancoDestinoFormatado.
	 * 
	 * @return bancoDestinoFormatado
	 */
	public String getBancoDestinoFormatado() {
		return bancoDestinoFormatado;
	}

	/**
	 * Set: bancoDestinoFormatado.
	 * 
	 * @param bancoDestinoFormatado
	 *            the banco destino formatado
	 */
	public void setBancoDestinoFormatado(final String bancoDestinoFormatado) {
		this.bancoDestinoFormatado = bancoDestinoFormatado;
	}

	/**
	 * Get: complementoInclusaoFormatado.
	 * 
	 * @return complementoInclusaoFormatado
	 */
	public String getComplementoInclusaoFormatado() {
		return complementoInclusaoFormatado;
	}

	/**
	 * Set: complementoInclusaoFormatado.
	 * 
	 * @param complementoInclusaoFormatado
	 *            the complemento inclusao formatado
	 */
	public void setComplementoInclusaoFormatado(
			final String complementoInclusaoFormatado) {
		this.complementoInclusaoFormatado = complementoInclusaoFormatado;
	}

	/**
	 * Get: complementoManutencaoFormatado.
	 * 
	 * @return complementoManutencaoFormatado
	 */
	public String getComplementoManutencaoFormatado() {
		return complementoManutencaoFormatado;
	}

	/**
	 * Set: complementoManutencaoFormatado.
	 * 
	 * @param complementoManutencaoFormatado
	 *            the complemento manutencao formatado
	 */
	public void setComplementoManutencaoFormatado(
			final String complementoManutencaoFormatado) {
		this.complementoManutencaoFormatado = complementoManutencaoFormatado;
	}

	/**
	 * Get: contaDestinoFormatada.
	 * 
	 * @return contaDestinoFormatada
	 */
	public String getContaDestinoFormatada() {
		return contaDestinoFormatada;
	}

	/**
	 * Set: contaDestinoFormatada.
	 * 
	 * @param contaDestinoFormatada
	 *            the conta destino formatada
	 */
	public void setContaDestinoFormatada(final String contaDestinoFormatada) {
		this.contaDestinoFormatada = contaDestinoFormatada;
	}

	/**
	 * Get: dataHoraInclusaoFormatada.
	 * 
	 * @return dataHoraInclusaoFormatada
	 */
	public String getDataHoraInclusaoFormatada() {
		return dataHoraInclusaoFormatada;
	}

	/**
	 * Set: dataHoraInclusaoFormatada.
	 * 
	 * @param dataHoraInclusaoFormatada
	 *            the data hora inclusao formatada
	 */
	public void setDataHoraInclusaoFormatada(final String dataHoraInclusaoFormatada) {
		this.dataHoraInclusaoFormatada = dataHoraInclusaoFormatada;
	}

	/**
	 * Get: dataHoraManutencaoFormatada.
	 * 
	 * @return dataHoraManutencaoFormatada
	 */
	public String getDataHoraManutencaoFormatada() {
		return dataHoraManutencaoFormatada;
	}

	/**
	 * Set: dataHoraManutencaoFormatada.
	 * 
	 * @param dataHoraManutencaoFormatada
	 *            the data hora manutencao formatada
	 */
	public void setDataHoraManutencaoFormatada(
			final String dataHoraManutencaoFormatada) {
		this.dataHoraManutencaoFormatada = dataHoraManutencaoFormatada;
	}

	/**
	 * Get: tipoCanalInclusaoFormatado.
	 * 
	 * @return tipoCanalInclusaoFormatado
	 */
	public String getTipoCanalInclusaoFormatado() {
		return tipoCanalInclusaoFormatado;
	}

	/**
	 * Set: tipoCanalInclusaoFormatado.
	 * 
	 * @param tipoCanalInclusaoFormatado
	 *            the tipo canal inclusao formatado
	 */
	public void setTipoCanalInclusaoFormatado(final String tipoCanalInclusaoFormatado) {
		this.tipoCanalInclusaoFormatado = tipoCanalInclusaoFormatado;
	}

	/**
	 * Get: tipoCanalManutencaoFormatado.
	 * 
	 * @return tipoCanalManutencaoFormatado
	 */
	public String getTipoCanalManutencaoFormatado() {
		return tipoCanalManutencaoFormatado;
	}

	/**
	 * Set: tipoCanalManutencaoFormatado.
	 * 
	 * @param tipoCanalManutencaoFormatado
	 *            the tipo canal manutencao formatado
	 */
	public void setTipoCanalManutencaoFormatado(
			final String tipoCanalManutencaoFormatado) {
		this.tipoCanalManutencaoFormatado = tipoCanalManutencaoFormatado;
	}

	/**
	 * Get: usuarioInclusaoFormatado.
	 * 
	 * @return usuarioInclusaoFormatado
	 */
	public String getUsuarioInclusaoFormatado() {
		return usuarioInclusaoFormatado;
	}

	/**
	 * Set: usuarioInclusaoFormatado.
	 * 
	 * @param usuarioInclusaoFormatado
	 *            the usuario inclusao formatado
	 */
	public void setUsuarioInclusaoFormatado(final String usuarioInclusaoFormatado) {
		this.usuarioInclusaoFormatado = usuarioInclusaoFormatado;
	}

	/**
	 * Get: usuarioManutencaoFormatado.
	 * 
	 * @return usuarioManutencaoFormatado
	 */
	public String getUsuarioManutencaoFormatado() {
		return usuarioManutencaoFormatado;
	}

	/**
	 * Set: usuarioManutencaoFormatado.
	 * 
	 * @param usuarioManutencaoFormatado
	 *            the usuario manutencao formatado
	 */
	public void setUsuarioManutencaoFormatado(final String usuarioManutencaoFormatado) {
		this.usuarioManutencaoFormatado = usuarioManutencaoFormatado;
	}

	/**
	 * Get: contaCreditoFormatada.
	 * 
	 * @return contaCreditoFormatada
	 */
	public String getContaCreditoFormatada() {
		return contaCreditoFormatada;
	}

	/**
	 * Set: contaCreditoFormatada.
	 * 
	 * @param contaCreditoFormatada
	 *            the conta credito formatada
	 */
	public void setContaCreditoFormatada(final String contaCreditoFormatada) {
		this.contaCreditoFormatada = contaCreditoFormatada;
	}

	/**
	 * Get: cdSituacaoTranferenciaAutomatica.
	 * 
	 * @return cdSituacaoTranferenciaAutomatica
	 */
	public String getCdSituacaoTranferenciaAutomatica() {
		return cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Set: cdSituacaoTranferenciaAutomatica.
	 * 
	 * @param cdSituacaoTranferenciaAutomatica
	 *            the cd situacao tranferencia automatica
	 */
	public void setCdSituacaoTranferenciaAutomatica(
			final String cdSituacaoTranferenciaAutomatica) {
		this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Get: cdAgenciaCredito.
	 * 
	 * @return cdAgenciaCredito
	 */
	public Integer getCdAgenciaCredito() {
		return cdAgenciaCredito;
	}

	/**
	 * Set: cdAgenciaCredito.
	 * 
	 * @param cdAgenciaCredito
	 *            the cd agencia credito
	 */
	public void setCdAgenciaCredito(final Integer cdAgenciaCredito) {
		this.cdAgenciaCredito = cdAgenciaCredito;
	}

	/**
	 * Get: cdAgenciaDebito.
	 * 
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	/**
	 * Set: cdAgenciaDebito.
	 * 
	 * @param cdAgenciaDebito
	 *            the cd agencia debito
	 */
	public void setCdAgenciaDebito(final Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	/**
	 * Get: cdBancoCredito.
	 * 
	 * @return cdBancoCredito
	 */
	public Integer getCdBancoCredito() {
		return cdBancoCredito;
	}

	/**
	 * Set: cdBancoCredito.
	 * 
	 * @param cdBancoCredito
	 *            the cd banco credito
	 */
	public void setCdBancoCredito(final Integer cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}

	/**
	 * Get: cdBancoDebito.
	 * 
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}

	/**
	 * Set: cdBancoDebito.
	 * 
	 * @param cdBancoDebito
	 *            the cd banco debito
	 */
	public void setCdBancoDebito(final Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}

	/**
	 * Get: cdContaCredito.
	 * 
	 * @return cdContaCredito
	 */
	public Long getCdContaCredito() {
		return cdContaCredito;
	}

	/**
	 * Set: cdContaCredito.
	 * 
	 * @param cdContaCredito
	 *            the cd conta credito
	 */
	public void setCdContaCredito(final Long cdContaCredito) {
		this.cdContaCredito = cdContaCredito;
	}

	/**
	 * Get: cdContaDebito.
	 * 
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}

	/**
	 * Set: cdContaDebito.
	 * 
	 * @param cdContaDebito
	 *            the cd conta debito
	 */
	public void setCdContaDebito(final Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	/**
	 * Get: cdCpfCnpjCliente.
	 * 
	 * @return cdCpfCnpjCliente
	 */
	public Long getCdCpfCnpjCliente() {
		return cdCpfCnpjCliente;
	}

	/**
	 * Set: cdCpfCnpjCliente.
	 * 
	 * @param cdCpfCnpjCliente
	 *            the cd cpf cnpj cliente
	 */
	public void setCdCpfCnpjCliente(final Long cdCpfCnpjCliente) {
		this.cdCpfCnpjCliente = cdCpfCnpjCliente;
	}

	/**
	 * Get: cdDigAgenciaCredito.
	 * 
	 * @return cdDigAgenciaCredito
	 */
	public String getCdDigAgenciaCredito() {
		return cdDigAgenciaCredito;
	}

	/**
	 * Set: cdDigAgenciaCredito.
	 * 
	 * @param cdDigAgenciaCredito
	 *            the cd dig agencia credito
	 */
	public void setCdDigAgenciaCredito(final String cdDigAgenciaCredito) {
		this.cdDigAgenciaCredito = cdDigAgenciaCredito;
	}

	/**
	 * Get: cdDigAgenciaDebto.
	 * 
	 * @return cdDigAgenciaDebto
	 */
	public String getCdDigAgenciaDebto() {
		return cdDigAgenciaDebto;
	}

	/**
	 * Set: cdDigAgenciaDebto.
	 * 
	 * @param cdDigAgenciaDebto
	 *            the cd dig agencia debto
	 */
	public void setCdDigAgenciaDebto(final String cdDigAgenciaDebto) {
		this.cdDigAgenciaDebto = cdDigAgenciaDebto;
	}

	/**
	 * Get: cdDigContaCredito.
	 * 
	 * @return cdDigContaCredito
	 */
	public String getCdDigContaCredito() {
		return cdDigContaCredito;
	}

	/**
	 * Set: cdDigContaCredito.
	 * 
	 * @param cdDigContaCredito
	 *            the cd dig conta credito
	 */
	public void setCdDigContaCredito(final String cdDigContaCredito) {
		this.cdDigContaCredito = cdDigContaCredito;
	}

	/**
	 * Get: cdIndicadorAuto.
	 * 
	 * @return cdIndicadorAuto
	 */
	public Integer getCdIndicadorAuto() {
		return cdIndicadorAuto;
	}

	/**
	 * Set: cdIndicadorAuto.
	 * 
	 * @param cdIndicadorAuto
	 *            the cd indicador auto
	 */
	public void setCdIndicadorAuto(final Integer cdIndicadorAuto) {
		this.cdIndicadorAuto = cdIndicadorAuto;
	}

	/**
	 * Get: cdIndicadorSemConsulta.
	 * 
	 * @return cdIndicadorSemConsulta
	 */
	public Integer getCdIndicadorSemConsulta() {
		return cdIndicadorSemConsulta;
	}

	/**
	 * Set: cdIndicadorSemConsulta.
	 * 
	 * @param cdIndicadorSemConsulta
	 *            the cd indicador sem consulta
	 */
	public void setCdIndicadorSemConsulta(final Integer cdIndicadorSemConsulta) {
		this.cdIndicadorSemConsulta = cdIndicadorSemConsulta;
	}

	/**
	 * Get: cdNomeBeneficio.
	 * 
	 * @return cdNomeBeneficio
	 */
	public String getCdNomeBeneficio() {
		return cdNomeBeneficio;
	}

	/**
	 * Set: cdNomeBeneficio.
	 * 
	 * @param cdNomeBeneficio
	 *            the cd nome beneficio
	 */
	public void setCdNomeBeneficio(final String cdNomeBeneficio) {
		this.cdNomeBeneficio = cdNomeBeneficio;
	}

	/**
	 * Get: cdSituacaoPagamento.
	 * 
	 * @return cdSituacaoPagamento
	 */
	public String getCdSituacaoPagamento() {
		return cdSituacaoPagamento;
	}

	/**
	 * Set: cdSituacaoPagamento.
	 * 
	 * @param cdSituacaoPagamento
	 *            the cd situacao pagamento
	 */
	public void setCdSituacaoPagamento(final String cdSituacaoPagamento) {
		this.cdSituacaoPagamento = cdSituacaoPagamento;
	}

	/**
	 * Get: cdTipoInscriBeneficio.
	 * 
	 * @return cdTipoInscriBeneficio
	 */
	public String getCdTipoInscriBeneficio() {
		return cdTipoInscriBeneficio;
	}

	/**
	 * Set: cdTipoInscriBeneficio.
	 * 
	 * @param cdTipoInscriBeneficio
	 *            the cd tipo inscri beneficio
	 */
	public void setCdTipoInscriBeneficio(final String cdTipoInscriBeneficio) {
		this.cdTipoInscriBeneficio = cdTipoInscriBeneficio;
	}

	/**
	 * Get: dsDigAgenciaDebito.
	 * 
	 * @return dsDigAgenciaDebito
	 */
	public String getDsDigAgenciaDebito() {
		return dsDigAgenciaDebito;
	}

	/**
	 * Set: dsDigAgenciaDebito.
	 * 
	 * @param dsDigAgenciaDebito
	 *            the ds dig agencia debito
	 */
	public void setDsDigAgenciaDebito(final String dsDigAgenciaDebito) {
		this.dsDigAgenciaDebito = dsDigAgenciaDebito;
	}

	/**
	 * Get: dsNomeFavorecido.
	 * 
	 * @return dsNomeFavorecido
	 */
	public String getDsNomeFavorecido() {
		return dsNomeFavorecido;
	}

	/**
	 * Set: dsNomeFavorecido.
	 * 
	 * @param dsNomeFavorecido
	 *            the ds nome favorecido
	 */
	public void setDsNomeFavorecido(final String dsNomeFavorecido) {
		this.dsNomeFavorecido = dsNomeFavorecido;
	}

	/**
	 * Get: dtPagamento.
	 * 
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 * 
	 * @param dtPagamento
	 *            the dt pagamento
	 */
	public void setDtPagamento(final String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	/**
	 * Get: cdTipoInscricaoFavorecido.
	 * 
	 * @return cdTipoInscricaoFavorecido
	 */
	public Integer getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}

	/**
	 * Set: cdTipoInscricaoFavorecido.
	 * 
	 * @param cdTipoInscricaoFavorecido
	 *            the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(final Integer cdTipoInscricaoFavorecido) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
	}

	/**
	 * Get: nmInscriBeneficio.
	 * 
	 * @return nmInscriBeneficio
	 */
	public String getNmInscriBeneficio() {
		return nmInscriBeneficio;
	}

	/**
	 * Set: nmInscriBeneficio.
	 * 
	 * @param nmInscriBeneficio
	 *            the nm inscri beneficio
	 */
	public void setNmInscriBeneficio(final String nmInscriBeneficio) {
		this.nmInscriBeneficio = nmInscriBeneficio;
	}

	/**
	 * Get: nmInscriFavorecido.
	 * 
	 * @return nmInscriFavorecido
	 */
	public String getNmInscriFavorecido() {
		return nmInscriFavorecido;
	}

	/**
	 * Set: nmInscriFavorecido.
	 * 
	 * @param nmInscriFavorecido
	 *            the nm inscri favorecido
	 */
	public void setNmInscriFavorecido(final String nmInscriFavorecido) {
		this.nmInscriFavorecido = nmInscriFavorecido;
	}

	/**
	 * Get: nomeCliente.
	 * 
	 * @return nomeCliente
	 */
	public String getNomeCliente() {
		return nomeCliente;
	}

	/**
	 * Set: nomeCliente.
	 * 
	 * @param nomeCliente
	 *            the nome cliente
	 */
	public void setNomeCliente(final String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	/**
	 * Get: cdControlePagamento.
	 * 
	 * @return cdControlePagamento
	 */
	public String getCdControlePagamento() {
		return cdControlePagamento;
	}

	/**
	 * Set: cdControlePagamento.
	 * 
	 * @param cdControlePagamento
	 *            the cd controle pagamento
	 */
	public void setCdControlePagamento(final String cdControlePagamento) {
		this.cdControlePagamento = cdControlePagamento;
	}

	/**
	 * Get: dsModalidadeRelacionado.
	 * 
	 * @return dsModalidadeRelacionado
	 */
	public String getDsModalidadeRelacionado() {
		return dsModalidadeRelacionado;
	}

	/**
	 * Set: dsModalidadeRelacionado.
	 * 
	 * @param dsModalidadeRelacionado
	 *            the ds modalidade relacionado
	 */
	public void setDsModalidadeRelacionado(final String dsModalidadeRelacionado) {
		this.dsModalidadeRelacionado = dsModalidadeRelacionado;
	}

	/**
	 * Get: dsPessoaJuridicaContrato.
	 * 
	 * @return dsPessoaJuridicaContrato
	 */
	public String getDsPessoaJuridicaContrato() {
		return dsPessoaJuridicaContrato;
	}

	/**
	 * Set: dsPessoaJuridicaContrato.
	 * 
	 * @param dsPessoaJuridicaContrato
	 *            the ds pessoa juridica contrato
	 */
	public void setDsPessoaJuridicaContrato(final String dsPessoaJuridicaContrato) {
		this.dsPessoaJuridicaContrato = dsPessoaJuridicaContrato;
	}

	/**
	 * Get: dsTipoServicoOperacao.
	 * 
	 * @return dsTipoServicoOperacao
	 */
	public String getDsTipoServicoOperacao() {
		return dsTipoServicoOperacao;
	}

	/**
	 * Set: dsTipoServicoOperacao.
	 * 
	 * @param dsTipoServicoOperacao
	 *            the ds tipo servico operacao
	 */
	public void setDsTipoServicoOperacao(final String dsTipoServicoOperacao) {
		this.dsTipoServicoOperacao = dsTipoServicoOperacao;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 * 
	 * @return nrSequenciaContratoNegocio
	 */
	public String getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 * 
	 * @param nrSequenciaContratoNegocio
	 *            the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(final String nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdBancoOriginal.
	 * 
	 * @return cdBancoOriginal
	 */
	public Integer getCdBancoOriginal() {
		return cdBancoOriginal;
	}

	/**
	 * Set: cdBancoOriginal.
	 * 
	 * @param cdBancoOriginal
	 *            the cd banco original
	 */
	public void setCdBancoOriginal(final Integer cdBancoOriginal) {
		this.cdBancoOriginal = cdBancoOriginal;
	}

	/**
	 * Get: dsBancoOriginal.
	 * 
	 * @return dsBancoOriginal
	 */
	public String getDsBancoOriginal() {
		return dsBancoOriginal;
	}

	/**
	 * Set: dsBancoOriginal.
	 * 
	 * @param dsBancoOriginal
	 *            the ds banco original
	 */
	public void setDsBancoOriginal(final String dsBancoOriginal) {
		this.dsBancoOriginal = dsBancoOriginal;
	}

	/**
	 * Get: cdAgenciaBancariaOriginal.
	 * 
	 * @return cdAgenciaBancariaOriginal
	 */
	public Integer getCdAgenciaBancariaOriginal() {
		return cdAgenciaBancariaOriginal;
	}

	/**
	 * Set: cdAgenciaBancariaOriginal.
	 * 
	 * @param cdAgenciaBancariaOriginal
	 *            the cd agencia bancaria original
	 */
	public void setCdAgenciaBancariaOriginal(final Integer cdAgenciaBancariaOriginal) {
		this.cdAgenciaBancariaOriginal = cdAgenciaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoAgenciaOriginal.
	 * 
	 * @return cdDigitoAgenciaOriginal
	 */
	public String getCdDigitoAgenciaOriginal() {
		return cdDigitoAgenciaOriginal;
	}

	/**
	 * Set: cdDigitoAgenciaOriginal.
	 * 
	 * @param cdDigitoAgenciaOriginal
	 *            the cd digito agencia original
	 */
	public void setCdDigitoAgenciaOriginal(final String cdDigitoAgenciaOriginal) {
		this.cdDigitoAgenciaOriginal = cdDigitoAgenciaOriginal;
	}

	/**
	 * Get: dsAgenciaOriginal.
	 * 
	 * @return dsAgenciaOriginal
	 */
	public String getDsAgenciaOriginal() {
		return dsAgenciaOriginal;
	}

	/**
	 * Set: dsAgenciaOriginal.
	 * 
	 * @param dsAgenciaOriginal
	 *            the ds agencia original
	 */
	public void setDsAgenciaOriginal(final String dsAgenciaOriginal) {
		this.dsAgenciaOriginal = dsAgenciaOriginal;
	}

	/**
	 * Get: cdContaBancariaOriginal.
	 * 
	 * @return cdContaBancariaOriginal
	 */
	public Long getCdContaBancariaOriginal() {
		return cdContaBancariaOriginal;
	}

	/**
	 * Set: cdContaBancariaOriginal.
	 * 
	 * @param cdContaBancariaOriginal
	 *            the cd conta bancaria original
	 */
	public void setCdContaBancariaOriginal(final Long cdContaBancariaOriginal) {
		this.cdContaBancariaOriginal = cdContaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoContaOriginal.
	 * 
	 * @return cdDigitoContaOriginal
	 */
	public String getCdDigitoContaOriginal() {
		return cdDigitoContaOriginal;
	}

	/**
	 * Set: cdDigitoContaOriginal.
	 * 
	 * @param cdDigitoContaOriginal
	 *            the cd digito conta original
	 */
	public void setCdDigitoContaOriginal(final String cdDigitoContaOriginal) {
		this.cdDigitoContaOriginal = cdDigitoContaOriginal;
	}

	/**
	 * Get: dsTipoContaOriginal.
	 * 
	 * @return dsTipoContaOriginal
	 */
	public String getDsTipoContaOriginal() {
		return dsTipoContaOriginal;
	}

	/**
	 * Set: dsTipoContaOriginal.
	 * 
	 * @param dsTipoContaOriginal
	 *            the ds tipo conta original
	 */
	public void setDsTipoContaOriginal(final String dsTipoContaOriginal) {
		this.dsTipoContaOriginal = dsTipoContaOriginal;
	}

	/**
	 * Get: dtDevolucaoEstorno.
	 * 
	 * @return dtDevolucaoEstorno
	 */
	public String getDtDevolucaoEstorno() {
		return dtDevolucaoEstorno;
	}

	/**
	 * Set: dtDevolucaoEstorno.
	 * 
	 * @param dtDevolucaoEstorno
	 *            the dt devolucao estorno
	 */
	public void setDtDevolucaoEstorno(final String dtDevolucaoEstorno) {
		this.dtDevolucaoEstorno = dtDevolucaoEstorno;
	}

	/**
	 * Get: dtFloatingPagamento.
	 * 
	 * @return dtFloatingPagamento
	 */
	public String getDtFloatingPagamento() {
		return dtFloatingPagamento;
	}

	/**
	 * Set: dtFloatingPagamento.
	 * 
	 * @param dtFloatingPagamento
	 *            the dt floating pagamento
	 */
	public void setDtFloatingPagamento(final String dtFloatingPagamento) {
		this.dtFloatingPagamento = dtFloatingPagamento;
	}

	/**
	 * Get: dtEfetivFloatPgto.
	 * 
	 * @return dtEfetivFloatPgto
	 */
	public String getDtEfetivFloatPgto() {
		return dtEfetivFloatPgto;
	}

	/**
	 * Set: dtEfetivFloatPgto.
	 * 
	 * @param dtEfetivFloatPgto
	 *            the dt efetiv float pgto
	 */
	public void setDtEfetivFloatPgto(final String dtEfetivFloatPgto) {
		this.dtEfetivFloatPgto = dtEfetivFloatPgto;
	}

	/**
	 * Get: vlFloatingPagamento.
	 * 
	 * @return vlFloatingPagamento
	 */
	public BigDecimal getVlFloatingPagamento() {
		return vlFloatingPagamento;
	}

	/**
	 * Set: vlFloatingPagamento.
	 * 
	 * @param vlFloatingPagamento
	 *            the vl floating pagamento
	 */
	public void setVlFloatingPagamento(final BigDecimal vlFloatingPagamento) {
		this.vlFloatingPagamento = vlFloatingPagamento;
	}

	/**
	 * Get: cdOperacaoDcom.
	 * 
	 * @return cdOperacaoDcom
	 */
	public Long getCdOperacaoDcom() {
		return cdOperacaoDcom;
	}

	/**
	 * Set: cdOperacaoDcom.
	 * 
	 * @param cdOperacaoDcom
	 *            the cd operacao dcom
	 */
	public void setCdOperacaoDcom(final Long cdOperacaoDcom) {
		this.cdOperacaoDcom = cdOperacaoDcom;
	}

	/**
	 * Get: dsSituacaoDcom.
	 * 
	 * @return dsSituacaoDcom
	 */
	public String getDsSituacaoDcom() {
		return dsSituacaoDcom;
	}

	/**
	 * Set: dsSituacaoDcom.
	 * 
	 * @param dsSituacaoDcom
	 *            the ds situacao dcom
	 */
	public void setDsSituacaoDcom(final String dsSituacaoDcom) {
		this.dsSituacaoDcom = dsSituacaoDcom;
	}

	/**
	 * Get: numControleInternoLote.
	 * 
	 * @return numControleInternoLote
	 */
	public Long getNumControleInternoLote() {
		return numControleInternoLote;
	}

	/**
	 * Set: numControleInternoLote.
	 * 
	 * @param numControleInternoLote
	 *            the num controle interno lote
	 */
	public void setNumControleInternoLote(final Long numControleInternoLote) {
		this.numControleInternoLote = numControleInternoLote;
	}

	/**
	 * Set: dsEstornoPagamento.
	 * 
	 * @param dsEstornoPagamento
	 *            the ds estorno pagamento
	 */
	public void setDsEstornoPagamento(final String dsEstornoPagamento) {
		this.dsEstornoPagamento = dsEstornoPagamento;
	}

	/**
	 * Get: dsEstornoPagamento.
	 * 
	 * @return dsEstornoPagamento
	 */
	public String getDsEstornoPagamento() {
		return dsEstornoPagamento;
	}

	/**
	 * @return the cdLoteInterno
	 */
	public Long getCdLoteInterno() {
		return cdLoteInterno;
	}

	/**
	 * @param cdLoteInterno
	 *            the cdLoteInterno to set
	 */
	public void setCdLoteInterno(final Long cdLoteInterno) {
		this.cdLoteInterno = cdLoteInterno;
	}

	/**
	 * @return the dsIndicadorAutorizacao
	 */
	public String getDsIndicadorAutorizacao() {
		return dsIndicadorAutorizacao;
	}

	/**
	 * @param dsIndicadorAutorizacao
	 *            the dsIndicadorAutorizacao to set
	 */
	public void setDsIndicadorAutorizacao(final String dsIndicadorAutorizacao) {
		this.dsIndicadorAutorizacao = dsIndicadorAutorizacao;
	}

	public String getDsTipoLayout() {
		return dsTipoLayout;
	}

	public void setDsTipoLayout(final String dsTipoLayout) {
		this.dsTipoLayout = dsTipoLayout;
	}

	/**
	 * @return the vlEfetivacaoDebitoPagamento
	 */
	public BigDecimal getVlEfetivacaoDebitoPagamento() {
		return vlEfetivacaoDebitoPagamento;
	}

	/**
	 * @param vlEfetivacaoDebitoPagamento the vlEfetivacaoDebitoPagamento to set
	 */
	public void setVlEfetivacaoDebitoPagamento(
			BigDecimal vlEfetivacaoDebitoPagamento) {
		this.vlEfetivacaoDebitoPagamento = vlEfetivacaoDebitoPagamento;
	}

	/**
	 * @return the vlEfetivacaoCreditoPagamento
	 */
	public BigDecimal getVlEfetivacaoCreditoPagamento() {
		return vlEfetivacaoCreditoPagamento;
	}

	/**
	 * @param vlEfetivacaoCreditoPagamento the vlEfetivacaoCreditoPagamento to set
	 */
	public void setVlEfetivacaoCreditoPagamento(
			BigDecimal vlEfetivacaoCreditoPagamento) {
		this.vlEfetivacaoCreditoPagamento = vlEfetivacaoCreditoPagamento;
	}

	/**
	 * @return the vlDescontoPagamento
	 */
	public BigDecimal getVlDescontoPagamento() {
		return vlDescontoPagamento;
	}

	/**
	 * @param vlDescontoPagamento the vlDescontoPagamento to set
	 */
	public void setVlDescontoPagamento(BigDecimal vlDescontoPagamento) {
		this.vlDescontoPagamento = vlDescontoPagamento;
	}

	/**
	 * @return the hrEfetivacaoCreditoPagamento
	 */
	public String getHrEfetivacaoCreditoPagamento() {
		return hrEfetivacaoCreditoPagamento;
	}

	/**
	 * @param hrEfetivacaoCreditoPagamento the hrEfetivacaoCreditoPagamento to set
	 */
	public void setHrEfetivacaoCreditoPagamento(String hrEfetivacaoCreditoPagamento) {
		this.hrEfetivacaoCreditoPagamento = hrEfetivacaoCreditoPagamento;
	}

	/**
	 * @return the hrEnvioCreditoPagamento
	 */
	public String getHrEnvioCreditoPagamento() {
		return hrEnvioCreditoPagamento;
	}

	/**
	 * @param hrEnvioCreditoPagamento the hrEnvioCreditoPagamento to set
	 */
	public void setHrEnvioCreditoPagamento(String hrEnvioCreditoPagamento) {
		this.hrEnvioCreditoPagamento = hrEnvioCreditoPagamento;
	}

	/**
	 * @return the cdIdentificadorTransferenciaPagto
	 */
	public Integer getCdIdentificadorTransferenciaPagto() {
		return cdIdentificadorTransferenciaPagto;
	}

	/**
	 * @param cdIdentificadorTransferenciaPagto the cdIdentificadorTransferenciaPagto to set
	 */
	public void setCdIdentificadorTransferenciaPagto(
			Integer cdIdentificadorTransferenciaPagto) {
		this.cdIdentificadorTransferenciaPagto = cdIdentificadorTransferenciaPagto;
	}

	/**
	 * @return the cdMensagemLinExtrato
	 */
	public Integer getCdMensagemLinExtrato() {
		return cdMensagemLinExtrato;
	}

	/**
	 * @param cdMensagemLinExtrato the cdMensagemLinExtrato to set
	 */
	public void setCdMensagemLinExtrato(Integer cdMensagemLinExtrato) {
		this.cdMensagemLinExtrato = cdMensagemLinExtrato;
	}

	/**
	 * @return the cdConveCtaSalarial
	 */
	public Long getCdConveCtaSalarial() {
		return cdConveCtaSalarial;
	}

	/**
	 * @param cdConveCtaSalarial the cdConveCtaSalarial to set
	 */
	public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
		this.cdConveCtaSalarial = cdConveCtaSalarial;
	}

	public void setDsIdentificadorTransferenciaPagto(
			String dsIdentificadorTransferenciaPagto) {
		this.dsIdentificadorTransferenciaPagto = dsIdentificadorTransferenciaPagto;
	}

	public String getDsIdentificadorTransferenciaPagto() {
		return dsIdentificadorTransferenciaPagto;
	}

	public String getCdIspbPagtoDestino() {
		return cdIspbPagtoDestino;
	}

	public void setCdIspbPagtoDestino(String cdIspbPagtoDestino) {
		this.cdIspbPagtoDestino = cdIspbPagtoDestino;
	}

	public String getContaPagtoDestino() {
		return contaPagtoDestino;
	}

	public void setContaPagtoDestino(String contaPagtoDestino) {
		this.contaPagtoDestino = contaPagtoDestino;
	}

}