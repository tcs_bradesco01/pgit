/*
 * Nome: br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean;

import java.math.BigDecimal;

/**
 * Nome: ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO {
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo cdMensagem. */
	private String cdMensagem;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial ;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrContratoOrigem. */
	private Long nrContratoOrigem ;
	
	/** Atributo nrCnpjCpf. */
	private Long nrCnpjCpf; 
	
	/** Atributo nrFilialCnpjCpf. */
	private Integer nrFilialCnpjCpf; 
	
	/** Atributo nrDigitoCnpjCpf. */
	private Integer nrDigitoCnpjCpf;
	
	/** Atributo nrArquivoRemessaPagamento. */
	private Long nrArquivoRemessaPagamento ;
	
	/** Atributo dtCreditoPagamento. */
	private String dtCreditoPagamento; 
	
	/** Atributo cdBanco. */
	private Integer cdBanco ;
	
	/** Atributo cdAgencia. */
	private Integer cdAgencia ;
	
	/** Atributo cdDigitoAgencia. */
	private Integer cdDigitoAgencia;
	
	/** Atributo cdConta. */
	private Long cdConta;
	
	/** Atributo cdDigitoConta. */
	private String cdDigitoConta;
	
	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;
	
	/** Atributo dsResumoProdutoServico. */
	private String dsResumoProdutoServico;
	
	/** Atributo cdProdutoServicoRelacionado. */
	private Integer cdProdutoServicoRelacionado;
	
	/** Atributo dsOperacaoProdutoServico. */
	private String dsOperacaoProdutoServico;
	
	/** Atributo cdSituacaoOperacaoPagamento. */
	private Integer cdSituacaoOperacaoPagamento;
	
	/** Atributo dsSituacaoOperacaoPagamento. */
	private String dsSituacaoOperacaoPagamento;
	
	/** Atributo qtPagamento. */
	private Long qtPagamento;
	
	/** Atributo vlEfetivoPagamentoCliente. */
	private BigDecimal vlEfetivoPagamentoCliente;
	
	/** Atributo cdPessoaContratoDebito. */
	private Long cdPessoaContratoDebito;
	
	/** Atributo cdTipoContratoDebito. */
	private Integer cdTipoContratoDebito;
	
	/** Atributo nrSequenciaContratoDebito. */
	private Long nrSequenciaContratoDebito;
	
	/** Atributo cnpjCpfFormatado. */
	private String cnpjCpfFormatado;
	
	/** Atributo bancoAgenciaContaDebitoFormatado. */
	private String bancoAgenciaContaDebitoFormatado;
	
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdBanco.
	 *
	 * @return cdBanco
	 */
	public Integer getCdBanco() {
		return cdBanco;
	}
	
	/**
	 * Set: cdBanco.
	 *
	 * @param cdBanco the cd banco
	 */
	public void setCdBanco(Integer cdBanco) {
		this.cdBanco = cdBanco;
	}
	
	/**
	 * Get: cdConta.
	 *
	 * @return cdConta
	 */
	public Long getCdConta() {
		return cdConta;
	}
	
	/**
	 * Set: cdConta.
	 *
	 * @param cdConta the cd conta
	 */
	public void setCdConta(Long cdConta) {
		this.cdConta = cdConta;
	}
	
	
	/**
	 * Get: cdDigitoAgencia.
	 *
	 * @return cdDigitoAgencia
	 */
	public Integer getCdDigitoAgencia() {
		return cdDigitoAgencia;
	}
	
	/**
	 * Set: cdDigitoAgencia.
	 *
	 * @param cdDigitoAgencia the cd digito agencia
	 */
	public void setCdDigitoAgencia(Integer cdDigitoAgencia) {
		this.cdDigitoAgencia = cdDigitoAgencia;
	}
	
	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public String getCdDigitoConta() {
		return cdDigitoConta;
	}
	
	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(String cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}
	
	/**
	 * Get: cdMensagem.
	 *
	 * @return cdMensagem
	 */
	public String getCdMensagem() {
		return cdMensagem;
	}
	
	/**
	 * Set: cdMensagem.
	 *
	 * @param cdMensagem the cd mensagem
	 */
	public void setCdMensagem(String cdMensagem) {
		this.cdMensagem = cdMensagem;
	}
	
	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdProdutoServicoRelacionado.
	 *
	 * @return cdProdutoServicoRelacionado
	 */
	public Integer getCdProdutoServicoRelacionado() {
		return cdProdutoServicoRelacionado;
	}
	
	/**
	 * Set: cdProdutoServicoRelacionado.
	 *
	 * @param cdProdutoServicoRelacionado the cd produto servico relacionado
	 */
	public void setCdProdutoServicoRelacionado(Integer cdProdutoServicoRelacionado) {
		this.cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
	}
	
	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public Integer getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(Integer cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: dsOperacaoProdutoServico.
	 *
	 * @return dsOperacaoProdutoServico
	 */
	public String getDsOperacaoProdutoServico() {
		return dsOperacaoProdutoServico;
	}
	
	/**
	 * Set: dsOperacaoProdutoServico.
	 *
	 * @param dsOperacaoProdutoServico the ds operacao produto servico
	 */
	public void setDsOperacaoProdutoServico(String dsOperacaoProdutoServico) {
		this.dsOperacaoProdutoServico = dsOperacaoProdutoServico;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: dsResumoProdutoServico.
	 *
	 * @return dsResumoProdutoServico
	 */
	public String getDsResumoProdutoServico() {
		return dsResumoProdutoServico;
	}
	
	/**
	 * Set: dsResumoProdutoServico.
	 *
	 * @param dsResumoProdutoServico the ds resumo produto servico
	 */
	public void setDsResumoProdutoServico(String dsResumoProdutoServico) {
		this.dsResumoProdutoServico = dsResumoProdutoServico;
	}
	
	/**
	 * Get: dsSituacaoOperacaoPagamento.
	 *
	 * @return dsSituacaoOperacaoPagamento
	 */
	public String getDsSituacaoOperacaoPagamento() {
		return dsSituacaoOperacaoPagamento;
	}
	
	/**
	 * Set: dsSituacaoOperacaoPagamento.
	 *
	 * @param dsSituacaoOperacaoPagamento the ds situacao operacao pagamento
	 */
	public void setDsSituacaoOperacaoPagamento(String dsSituacaoOperacaoPagamento) {
		this.dsSituacaoOperacaoPagamento = dsSituacaoOperacaoPagamento;
	}
	
	/**
	 * Get: dtCreditoPagamento.
	 *
	 * @return dtCreditoPagamento
	 */
	public String getDtCreditoPagamento() {
		return dtCreditoPagamento;
	}
	
	/**
	 * Set: dtCreditoPagamento.
	 *
	 * @param dtCreditoPagamento the dt credito pagamento
	 */
	public void setDtCreditoPagamento(String dtCreditoPagamento) {
		this.dtCreditoPagamento = dtCreditoPagamento;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrArquivoRemessaPagamento.
	 *
	 * @return nrArquivoRemessaPagamento
	 */
	public Long getNrArquivoRemessaPagamento() {
		return nrArquivoRemessaPagamento;
	}
	
	/**
	 * Set: nrArquivoRemessaPagamento.
	 *
	 * @param nrArquivoRemessaPagamento the nr arquivo remessa pagamento
	 */
	public void setNrArquivoRemessaPagamento(Long nrArquivoRemessaPagamento) {
		this.nrArquivoRemessaPagamento = nrArquivoRemessaPagamento;
	}
	
	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public Long getNrCnpjCpf() {
		return nrCnpjCpf;
	}
	
	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(Long nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}
	
	/**
	 * Get: nrContratoOrigem.
	 *
	 * @return nrContratoOrigem
	 */
	public Long getNrContratoOrigem() {
		return nrContratoOrigem;
	}
	
	/**
	 * Set: nrContratoOrigem.
	 *
	 * @param nrContratoOrigem the nr contrato origem
	 */
	public void setNrContratoOrigem(Long nrContratoOrigem) {
		this.nrContratoOrigem = nrContratoOrigem;
	}
	
	/**
	 * Get: nrDigitoCnpjCpf.
	 *
	 * @return nrDigitoCnpjCpf
	 */
	public Integer getNrDigitoCnpjCpf() {
		return nrDigitoCnpjCpf;
	}
	
	/**
	 * Set: nrDigitoCnpjCpf.
	 *
	 * @param nrDigitoCnpjCpf the nr digito cnpj cpf
	 */
	public void setNrDigitoCnpjCpf(Integer nrDigitoCnpjCpf) {
		this.nrDigitoCnpjCpf = nrDigitoCnpjCpf;
	}
	
	/**
	 * Get: nrFilialCnpjCpf.
	 *
	 * @return nrFilialCnpjCpf
	 */
	public Integer getNrFilialCnpjCpf() {
		return nrFilialCnpjCpf;
	}
	
	/**
	 * Set: nrFilialCnpjCpf.
	 *
	 * @param nrFilialCnpjCpf the nr filial cnpj cpf
	 */
	public void setNrFilialCnpjCpf(Integer nrFilialCnpjCpf) {
		this.nrFilialCnpjCpf = nrFilialCnpjCpf;
	}
	
	/**
	 * Get: qtPagamento.
	 *
	 * @return qtPagamento
	 */
	public Long getQtPagamento() {
		return qtPagamento;
	}
	
	/**
	 * Set: qtPagamento.
	 *
	 * @param qtPagamento the qt pagamento
	 */
	public void setQtPagamento(Long qtPagamento) {
		this.qtPagamento = qtPagamento;
	}
	
	/**
	 * Get: vlEfetivoPagamentoCliente.
	 *
	 * @return vlEfetivoPagamentoCliente
	 */
	public BigDecimal getVlEfetivoPagamentoCliente() {
		return vlEfetivoPagamentoCliente;
	}
	
	/**
	 * Set: vlEfetivoPagamentoCliente.
	 *
	 * @param vlEfetivoPagamentoCliente the vl efetivo pagamento cliente
	 */
	public void setVlEfetivoPagamentoCliente(BigDecimal vlEfetivoPagamentoCliente) {
		this.vlEfetivoPagamentoCliente = vlEfetivoPagamentoCliente;
	}
	
	/**
	 * Get: cnpjCpfFormatado.
	 *
	 * @return cnpjCpfFormatado
	 */
	public String getCnpjCpfFormatado() {
		return cnpjCpfFormatado;
	}
	
	/**
	 * Set: cnpjCpfFormatado.
	 *
	 * @param cnpjCpfFormatado the cnpj cpf formatado
	 */
	public void setCnpjCpfFormatado(String cnpjCpfFormatado) {
		this.cnpjCpfFormatado = cnpjCpfFormatado;
	}
	
	/**
	 * Get: bancoAgenciaContaDebitoFormatado.
	 *
	 * @return bancoAgenciaContaDebitoFormatado
	 */
	public String getBancoAgenciaContaDebitoFormatado() {
		return bancoAgenciaContaDebitoFormatado;
	}
	
	/**
	 * Set: bancoAgenciaContaDebitoFormatado.
	 *
	 * @param bancoAgenciaContaDebitoFormatado the banco agencia conta debito formatado
	 */
	public void setBancoAgenciaContaDebitoFormatado(String bancoAgenciaContaDebitoFormatado) {
		this.bancoAgenciaContaDebitoFormatado = bancoAgenciaContaDebitoFormatado;
	}
	
	/**
	 * Get: cdPessoaContratoDebito.
	 *
	 * @return cdPessoaContratoDebito
	 */
	public Long getCdPessoaContratoDebito() {
		return cdPessoaContratoDebito;
	}
	
	/**
	 * Set: cdPessoaContratoDebito.
	 *
	 * @param cdPessoaContratoDebito the cd pessoa contrato debito
	 */
	public void setCdPessoaContratoDebito(Long cdPessoaContratoDebito) {
		this.cdPessoaContratoDebito = cdPessoaContratoDebito;
	}
	
	/**
	 * Get: cdTipoContratoDebito.
	 *
	 * @return cdTipoContratoDebito
	 */
	public Integer getCdTipoContratoDebito() {
		return cdTipoContratoDebito;
	}
	
	/**
	 * Set: cdTipoContratoDebito.
	 *
	 * @param cdTipoContratoDebito the cd tipo contrato debito
	 */
	public void setCdTipoContratoDebito(Integer cdTipoContratoDebito) {
		this.cdTipoContratoDebito = cdTipoContratoDebito;
	}
	
	/**
	 * Get: nrSequenciaContratoDebito.
	 *
	 * @return nrSequenciaContratoDebito
	 */
	public Long getNrSequenciaContratoDebito() {
		return nrSequenciaContratoDebito;
	}
	
	/**
	 * Set: nrSequenciaContratoDebito.
	 *
	 * @param nrSequenciaContratoDebito the nr sequencia contrato debito
	 */
	public void setNrSequenciaContratoDebito(Long nrSequenciaContratoDebito) {
		this.nrSequenciaContratoDebito = nrSequenciaContratoDebito;
	}
	
}
