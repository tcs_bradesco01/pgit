/*
 * Nome: br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean;

/**
 * Nome: IncluirContratoPgitSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirContratoPgitSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdRetorno. */
    private Integer cdRetorno;
    
    /** Atributo cdFaseSaida. */
    private Integer cdFaseSaida;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: cdFaseSaida.
	 *
	 * @return cdFaseSaida
	 */
	public Integer getCdFaseSaida() {
		return cdFaseSaida;
	}
	
	/**
	 * Set: cdFaseSaida.
	 *
	 * @param cdFaseSaida the cd fase saida
	 */
	public void setCdFaseSaida(Integer cdFaseSaida) {
		this.cdFaseSaida = cdFaseSaida;
	}
	
	/**
	 * Get: cdRetorno.
	 *
	 * @return cdRetorno
	 */
	public Integer getCdRetorno() {
		return cdRetorno;
	}
	
	/**
	 * Set: cdRetorno.
	 *
	 * @param cdRetorno the cd retorno
	 */
	public void setCdRetorno(Integer cdRetorno) {
		this.cdRetorno = cdRetorno;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
}