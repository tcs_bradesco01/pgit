/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.alterarmsglancamentopersonalizado.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class AlterarMsgLancamentoPersonalizadoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class AlterarMsgLancamentoPersonalizadoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMensagemLinhasExtrato
     */
    private int _cdMensagemLinhasExtrato = 0;

    /**
     * keeps track of state for field: _cdMensagemLinhasExtrato
     */
    private boolean _has_cdMensagemLinhasExtrato;

    /**
     * Field _cdIdentificadorLancamentoDebito
     */
    private int _cdIdentificadorLancamentoDebito = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificadorLancamentoDebito
     */
    private boolean _has_cdIdentificadorLancamentoDebito;

    /**
     * Field _cdIdentificadorLancamentoCredito
     */
    private int _cdIdentificadorLancamentoCredito = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificadorLancamentoCredito
     */
    private boolean _has_cdIdentificadorLancamentoCredito;

    /**
     * Field _cdIndicadorRestricaoContrato
     */
    private int _cdIndicadorRestricaoContrato = 0;

    /**
     * keeps track of state for field: _cdIndicadorRestricaoContrato
     */
    private boolean _has_cdIndicadorRestricaoContrato;

    /**
     * Field _cdTipoMensagemExtrato
     */
    private int _cdTipoMensagemExtrato = 0;

    /**
     * keeps track of state for field: _cdTipoMensagemExtrato
     */
    private boolean _has_cdTipoMensagemExtrato;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoOperRelacionado
     */
    private int _cdProdutoOperRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperRelacionado
     */
    private boolean _has_cdProdutoOperRelacionado;

    /**
     * Field _cdRelacionadoProduto
     */
    private int _cdRelacionadoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionadoProduto
     */
    private boolean _has_cdRelacionadoProduto;


      //----------------/
     //- Constructors -/
    //----------------/

    public AlterarMsgLancamentoPersonalizadoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarmsglancamentopersonalizado.request.AlterarMsgLancamentoPersonalizadoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIdentificadorLancamentoCredito
     * 
     */
    public void deleteCdIdentificadorLancamentoCredito()
    {
        this._has_cdIdentificadorLancamentoCredito= false;
    } //-- void deleteCdIdentificadorLancamentoCredito() 

    /**
     * Method deleteCdIdentificadorLancamentoDebito
     * 
     */
    public void deleteCdIdentificadorLancamentoDebito()
    {
        this._has_cdIdentificadorLancamentoDebito= false;
    } //-- void deleteCdIdentificadorLancamentoDebito() 

    /**
     * Method deleteCdIndicadorRestricaoContrato
     * 
     */
    public void deleteCdIndicadorRestricaoContrato()
    {
        this._has_cdIndicadorRestricaoContrato= false;
    } //-- void deleteCdIndicadorRestricaoContrato() 

    /**
     * Method deleteCdMensagemLinhasExtrato
     * 
     */
    public void deleteCdMensagemLinhasExtrato()
    {
        this._has_cdMensagemLinhasExtrato= false;
    } //-- void deleteCdMensagemLinhasExtrato() 

    /**
     * Method deleteCdProdutoOperRelacionado
     * 
     */
    public void deleteCdProdutoOperRelacionado()
    {
        this._has_cdProdutoOperRelacionado= false;
    } //-- void deleteCdProdutoOperRelacionado() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdRelacionadoProduto
     * 
     */
    public void deleteCdRelacionadoProduto()
    {
        this._has_cdRelacionadoProduto= false;
    } //-- void deleteCdRelacionadoProduto() 

    /**
     * Method deleteCdTipoMensagemExtrato
     * 
     */
    public void deleteCdTipoMensagemExtrato()
    {
        this._has_cdTipoMensagemExtrato= false;
    } //-- void deleteCdTipoMensagemExtrato() 

    /**
     * Returns the value of field
     * 'cdIdentificadorLancamentoCredito'.
     * 
     * @return int
     * @return the value of field 'cdIdentificadorLancamentoCredito'
     */
    public int getCdIdentificadorLancamentoCredito()
    {
        return this._cdIdentificadorLancamentoCredito;
    } //-- int getCdIdentificadorLancamentoCredito() 

    /**
     * Returns the value of field
     * 'cdIdentificadorLancamentoDebito'.
     * 
     * @return int
     * @return the value of field 'cdIdentificadorLancamentoDebito'.
     */
    public int getCdIdentificadorLancamentoDebito()
    {
        return this._cdIdentificadorLancamentoDebito;
    } //-- int getCdIdentificadorLancamentoDebito() 

    /**
     * Returns the value of field 'cdIndicadorRestricaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRestricaoContrato'.
     */
    public int getCdIndicadorRestricaoContrato()
    {
        return this._cdIndicadorRestricaoContrato;
    } //-- int getCdIndicadorRestricaoContrato() 

    /**
     * Returns the value of field 'cdMensagemLinhasExtrato'.
     * 
     * @return int
     * @return the value of field 'cdMensagemLinhasExtrato'.
     */
    public int getCdMensagemLinhasExtrato()
    {
        return this._cdMensagemLinhasExtrato;
    } //-- int getCdMensagemLinhasExtrato() 

    /**
     * Returns the value of field 'cdProdutoOperRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperRelacionado'.
     */
    public int getCdProdutoOperRelacionado()
    {
        return this._cdProdutoOperRelacionado;
    } //-- int getCdProdutoOperRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdRelacionadoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionadoProduto'.
     */
    public int getCdRelacionadoProduto()
    {
        return this._cdRelacionadoProduto;
    } //-- int getCdRelacionadoProduto() 

    /**
     * Returns the value of field 'cdTipoMensagemExtrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoMensagemExtrato'.
     */
    public int getCdTipoMensagemExtrato()
    {
        return this._cdTipoMensagemExtrato;
    } //-- int getCdTipoMensagemExtrato() 

    /**
     * Method hasCdIdentificadorLancamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificadorLancamentoCredito()
    {
        return this._has_cdIdentificadorLancamentoCredito;
    } //-- boolean hasCdIdentificadorLancamentoCredito() 

    /**
     * Method hasCdIdentificadorLancamentoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificadorLancamentoDebito()
    {
        return this._has_cdIdentificadorLancamentoDebito;
    } //-- boolean hasCdIdentificadorLancamentoDebito() 

    /**
     * Method hasCdIndicadorRestricaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRestricaoContrato()
    {
        return this._has_cdIndicadorRestricaoContrato;
    } //-- boolean hasCdIndicadorRestricaoContrato() 

    /**
     * Method hasCdMensagemLinhasExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemLinhasExtrato()
    {
        return this._has_cdMensagemLinhasExtrato;
    } //-- boolean hasCdMensagemLinhasExtrato() 

    /**
     * Method hasCdProdutoOperRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperRelacionado()
    {
        return this._has_cdProdutoOperRelacionado;
    } //-- boolean hasCdProdutoOperRelacionado() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdRelacionadoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionadoProduto()
    {
        return this._has_cdRelacionadoProduto;
    } //-- boolean hasCdRelacionadoProduto() 

    /**
     * Method hasCdTipoMensagemExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMensagemExtrato()
    {
        return this._has_cdTipoMensagemExtrato;
    } //-- boolean hasCdTipoMensagemExtrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIdentificadorLancamentoCredito'.
     * 
     * @param cdIdentificadorLancamentoCredito the value of field
     * 'cdIdentificadorLancamentoCredito'.
     */
    public void setCdIdentificadorLancamentoCredito(int cdIdentificadorLancamentoCredito)
    {
        this._cdIdentificadorLancamentoCredito = cdIdentificadorLancamentoCredito;
        this._has_cdIdentificadorLancamentoCredito = true;
    } //-- void setCdIdentificadorLancamentoCredito(int) 

    /**
     * Sets the value of field 'cdIdentificadorLancamentoDebito'.
     * 
     * @param cdIdentificadorLancamentoDebito the value of field
     * 'cdIdentificadorLancamentoDebito'.
     */
    public void setCdIdentificadorLancamentoDebito(int cdIdentificadorLancamentoDebito)
    {
        this._cdIdentificadorLancamentoDebito = cdIdentificadorLancamentoDebito;
        this._has_cdIdentificadorLancamentoDebito = true;
    } //-- void setCdIdentificadorLancamentoDebito(int) 

    /**
     * Sets the value of field 'cdIndicadorRestricaoContrato'.
     * 
     * @param cdIndicadorRestricaoContrato the value of field
     * 'cdIndicadorRestricaoContrato'.
     */
    public void setCdIndicadorRestricaoContrato(int cdIndicadorRestricaoContrato)
    {
        this._cdIndicadorRestricaoContrato = cdIndicadorRestricaoContrato;
        this._has_cdIndicadorRestricaoContrato = true;
    } //-- void setCdIndicadorRestricaoContrato(int) 

    /**
     * Sets the value of field 'cdMensagemLinhasExtrato'.
     * 
     * @param cdMensagemLinhasExtrato the value of field
     * 'cdMensagemLinhasExtrato'.
     */
    public void setCdMensagemLinhasExtrato(int cdMensagemLinhasExtrato)
    {
        this._cdMensagemLinhasExtrato = cdMensagemLinhasExtrato;
        this._has_cdMensagemLinhasExtrato = true;
    } //-- void setCdMensagemLinhasExtrato(int) 

    /**
     * Sets the value of field 'cdProdutoOperRelacionado'.
     * 
     * @param cdProdutoOperRelacionado the value of field
     * 'cdProdutoOperRelacionado'.
     */
    public void setCdProdutoOperRelacionado(int cdProdutoOperRelacionado)
    {
        this._cdProdutoOperRelacionado = cdProdutoOperRelacionado;
        this._has_cdProdutoOperRelacionado = true;
    } //-- void setCdProdutoOperRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionadoProduto'.
     * 
     * @param cdRelacionadoProduto the value of field
     * 'cdRelacionadoProduto'.
     */
    public void setCdRelacionadoProduto(int cdRelacionadoProduto)
    {
        this._cdRelacionadoProduto = cdRelacionadoProduto;
        this._has_cdRelacionadoProduto = true;
    } //-- void setCdRelacionadoProduto(int) 

    /**
     * Sets the value of field 'cdTipoMensagemExtrato'.
     * 
     * @param cdTipoMensagemExtrato the value of field
     * 'cdTipoMensagemExtrato'.
     */
    public void setCdTipoMensagemExtrato(int cdTipoMensagemExtrato)
    {
        this._cdTipoMensagemExtrato = cdTipoMensagemExtrato;
        this._has_cdTipoMensagemExtrato = true;
    } //-- void setCdTipoMensagemExtrato(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return AlterarMsgLancamentoPersonalizadoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.alterarmsglancamentopersonalizado.request.AlterarMsgLancamentoPersonalizadoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.alterarmsglancamentopersonalizado.request.AlterarMsgLancamentoPersonalizadoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.alterarmsglancamentopersonalizado.request.AlterarMsgLancamentoPersonalizadoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.alterarmsglancamentopersonalizado.request.AlterarMsgLancamentoPersonalizadoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
