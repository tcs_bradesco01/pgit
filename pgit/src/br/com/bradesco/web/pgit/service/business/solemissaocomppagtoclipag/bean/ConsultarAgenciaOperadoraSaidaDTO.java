/*
 * Nome: br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean;

/**
 * Nome: ConsultarAgenciaOperadoraSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarAgenciaOperadoraSaidaDTO {
	
	/** Atributo codMensagem. */
	private String codMensagem;
    
    /** Atributo mensagem. */
    private String mensagem;
    
    /** Atributo cdPessoaJuridicaDepartamento. */
    private Long cdPessoaJuridicaDepartamento;
    
    /** Atributo nrSequenciaDepartamento. */
    private Integer nrSequenciaDepartamento;
    
    /** Atributo cdAgencia. */
    private Integer cdAgencia;
    
    /** Atributo dsAgencia. */
    private String dsAgencia;
    
	/**
	 * Get: cdAgencia.
	 *
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}
	
	/**
	 * Set: cdAgencia.
	 *
	 * @param cdAgencia the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}
	
	/**
	 * Get: cdPessoaJuridicaDepartamento.
	 *
	 * @return cdPessoaJuridicaDepartamento
	 */
	public Long getCdPessoaJuridicaDepartamento() {
		return cdPessoaJuridicaDepartamento;
	}
	
	/**
	 * Set: cdPessoaJuridicaDepartamento.
	 *
	 * @param cdPessoaJuridicaDepartamento the cd pessoa juridica departamento
	 */
	public void setCdPessoaJuridicaDepartamento(Long cdPessoaJuridicaDepartamento) {
		this.cdPessoaJuridicaDepartamento = cdPessoaJuridicaDepartamento;
	}
	
	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}
	
	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}
	
	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}
	
	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}
	
	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}
	
	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	/**
	 * Get: nrSequenciaDepartamento.
	 *
	 * @return nrSequenciaDepartamento
	 */
	public Integer getNrSequenciaDepartamento() {
		return nrSequenciaDepartamento;
	}
	
	/**
	 * Set: nrSequenciaDepartamento.
	 *
	 * @param nrSequenciaDepartamento the nr sequencia departamento
	 */
	public void setNrSequenciaDepartamento(Integer nrSequenciaDepartamento) {
		this.nrSequenciaDepartamento = nrSequenciaDepartamento;
	}
}
