/*
 * =========================================================================
 * 
 * Cliente:      	Bradesco 
 * Projeto:      	INSERIR AQUI O NOME DO PROJETO
 * Desenvolvimento: BRADESCO
 * -------------------------------------------------------------------------
 * �ltima revisao:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/PluginWDE/templates/ServiceGeneration/interfaz.ftl,v $
 * $Id: interfaz.ftl,v 1.1 2009/03/12 13:31:10 cpm.com.br\edwin.costa Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revisao - Hist�rico:
 * $Log: interfaz.ftl,v $
 * Revision 1.1  2009/03/12 13:31:10  cpm.com.br\edwin.costa
 * Plugin WDE para o eclipse
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.service.business.manterlimitescontrato;

import java.util.List;

import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimDIaOpePgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimDIaOpePgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimiteIntraPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimiteIntraPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimDiarioUtilizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimDiarioUtilizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteHistoricoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteHistoricoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteIntraPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteIntraPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarListaServLimDiaIndvEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarListaServLimDiaIndvSaidaDTO;

/**
 * 
 * <p>
 * <b>T�tulo:</b>
 * </p>
 * <p>
 * <b>Descri�ao:</b>
 * </p>
 * <p>
 * Interface do adaptador: ManterLimitesContrato
 * </p>
 * 
 * @comment CODIGO GERADO AUTOMATICAMENTE DO PLUGIN WDE
 * @author CPM S/A
 * @version 1.0
 */
public interface IManterLimitesContratoService {

    /**
     * M�todo de exemplo.
     */
    void sampleManterLimitesContrato();

    
    /**
     * Consultar limite intra pgit.
     *
     * @param entrada the entrada
     * @return the list< consultar limite intra pgit saida dt o>
     */
    List<ConsultarLimiteIntraPgitSaidaDTO> consultarLimiteIntraPgit(ConsultarLimiteIntraPgitEntradaDTO entrada);
    
    /**
     * Consultar lista serv lim dia indv.
     *
     * @param entrada the entrada
     * @return the list< consultar lista serv lim dia indv saida dt o>
     */
    List<ConsultarListaServLimDiaIndvSaidaDTO> consultarListaServLimDiaIndv(ConsultarListaServLimDiaIndvEntradaDTO entrada);
    
    /**
     * Alterar limite intra pgit.
     *
     * @param entrada the entrada
     * @return the alterar limite intra pgit saida dto
     */
    AlterarLimiteIntraPgitSaidaDTO alterarLimiteIntraPgit(AlterarLimiteIntraPgitEntradaDTO entrada);
    
    /**
     * Alterar lim d ia ope pgit.
     *
     * @param entrada the entrada
     * @return the alterar lim d ia ope pgit saida dto
     */
    AlterarLimDIaOpePgitSaidaDTO alterarLimDIaOpePgit(AlterarLimDIaOpePgitEntradaDTO entrada);
    
    /**
     * Consultar lim diario utilizado.
     *
     * @param entrada the entrada
     * @return the list< consultar lim diario utilizado saida dt o>
     */
    List<ConsultarLimDiarioUtilizadoSaidaDTO> consultarLimDiarioUtilizado(ConsultarLimDiarioUtilizadoEntradaDTO entrada);

    /**
     * Consultar limite conta.
     *
     * @param entrada the entrada
     * @return the consultar limite conta saida dto
     */
    ConsultarLimiteContaSaidaDTO consultarLimiteConta(ConsultarLimiteContaEntradaDTO entrada);
    
    /**
     * Consultar limite historico.
     *
     * @param entrada the entrada
     * @return the consultar limite historico saida dto
     */
    ConsultarLimiteHistoricoSaidaDTO consultarLimiteHistorico(ConsultarLimiteHistoricoEntradaDTO entrada);
    
    
}

