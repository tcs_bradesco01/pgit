/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarQtdeValorPgtoPrevistoSolicRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarQtdeValorPgtoPrevistoSolicRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdSolicitacaoPagamentoIntegrado
     */
    private int _cdSolicitacaoPagamentoIntegrado = 0;

    /**
     * keeps track of state for field:
     * _cdSolicitacaoPagamentoIntegrado
     */
    private boolean _has_cdSolicitacaoPagamentoIntegrado;

    /**
     * Field _cdpessoaJuridicaContrato
     */
    private long _cdpessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdpessoaJuridicaContrato
     */
    private boolean _has_cdpessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _nrLoteInterno
     */
    private long _nrLoteInterno = 0;

    /**
     * keeps track of state for field: _nrLoteInterno
     */
    private boolean _has_nrLoteInterno;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _dtPgtoInicial
     */
    private java.lang.String _dtPgtoInicial;

    /**
     * Field _dtPgtoFinal
     */
    private java.lang.String _dtPgtoFinal;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _cdBancoDebito
     */
    private int _cdBancoDebito = 0;

    /**
     * keeps track of state for field: _cdBancoDebito
     */
    private boolean _has_cdBancoDebito;

    /**
     * Field _cdAgenciaDebito
     */
    private int _cdAgenciaDebito = 0;

    /**
     * keeps track of state for field: _cdAgenciaDebito
     */
    private boolean _has_cdAgenciaDebito;

    /**
     * Field _cdContaDebito
     */
    private long _cdContaDebito = 0;

    /**
     * keeps track of state for field: _cdContaDebito
     */
    private boolean _has_cdContaDebito;

    /**
     * Field _cdTipoInscricaoPagador
     */
    private int _cdTipoInscricaoPagador = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoPagador
     */
    private boolean _has_cdTipoInscricaoPagador;

    /**
     * Field _cdCpfCnpjPagador
     */
    private long _cdCpfCnpjPagador = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjPagador
     */
    private boolean _has_cdCpfCnpjPagador;

    /**
     * Field _cdFilialCpfCnpjPagador
     */
    private int _cdFilialCpfCnpjPagador = 0;

    /**
     * keeps track of state for field: _cdFilialCpfCnpjPagador
     */
    private boolean _has_cdFilialCpfCnpjPagador;

    /**
     * Field _cdControleCpfCnpjPagador
     */
    private int _cdControleCpfCnpjPagador = 0;

    /**
     * keeps track of state for field: _cdControleCpfCnpjPagador
     */
    private boolean _has_cdControleCpfCnpjPagador;

    /**
     * Field _cdTipoCtaRecebedor
     */
    private int _cdTipoCtaRecebedor = 0;

    /**
     * keeps track of state for field: _cdTipoCtaRecebedor
     */
    private boolean _has_cdTipoCtaRecebedor;

    /**
     * Field _cdBcoRecebedor
     */
    private int _cdBcoRecebedor = 0;

    /**
     * keeps track of state for field: _cdBcoRecebedor
     */
    private boolean _has_cdBcoRecebedor;

    /**
     * Field _cdAgenciaRecebedor
     */
    private int _cdAgenciaRecebedor = 0;

    /**
     * keeps track of state for field: _cdAgenciaRecebedor
     */
    private boolean _has_cdAgenciaRecebedor;

    /**
     * Field _cdContaRecebedor
     */
    private long _cdContaRecebedor = 0;

    /**
     * keeps track of state for field: _cdContaRecebedor
     */
    private boolean _has_cdContaRecebedor;

    /**
     * Field _cdTipoInscricaoRecebedor
     */
    private int _cdTipoInscricaoRecebedor = 0;

    /**
     * keeps track of state for field: _cdTipoInscricaoRecebedor
     */
    private boolean _has_cdTipoInscricaoRecebedor;

    /**
     * Field _cdCpfCnpjRecebedor
     */
    private long _cdCpfCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdCpfCnpjRecebedor
     */
    private boolean _has_cdCpfCnpjRecebedor;

    /**
     * Field _cdFilialCnpjRecebedor
     */
    private int _cdFilialCnpjRecebedor = 0;

    /**
     * keeps track of state for field: _cdFilialCnpjRecebedor
     */
    private boolean _has_cdFilialCnpjRecebedor;

    /**
     * Field _cdControleCpfRecebedor
     */
    private int _cdControleCpfRecebedor = 0;

    /**
     * keeps track of state for field: _cdControleCpfRecebedor
     */
    private boolean _has_cdControleCpfRecebedor;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarQtdeValorPgtoPrevistoSolicRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.request.ConsultarQtdeValorPgtoPrevistoSolicRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdAgenciaDebito
     * 
     */
    public void deleteCdAgenciaDebito()
    {
        this._has_cdAgenciaDebito= false;
    } //-- void deleteCdAgenciaDebito() 

    /**
     * Method deleteCdAgenciaRecebedor
     * 
     */
    public void deleteCdAgenciaRecebedor()
    {
        this._has_cdAgenciaRecebedor= false;
    } //-- void deleteCdAgenciaRecebedor() 

    /**
     * Method deleteCdBancoDebito
     * 
     */
    public void deleteCdBancoDebito()
    {
        this._has_cdBancoDebito= false;
    } //-- void deleteCdBancoDebito() 

    /**
     * Method deleteCdBcoRecebedor
     * 
     */
    public void deleteCdBcoRecebedor()
    {
        this._has_cdBcoRecebedor= false;
    } //-- void deleteCdBcoRecebedor() 

    /**
     * Method deleteCdContaDebito
     * 
     */
    public void deleteCdContaDebito()
    {
        this._has_cdContaDebito= false;
    } //-- void deleteCdContaDebito() 

    /**
     * Method deleteCdContaRecebedor
     * 
     */
    public void deleteCdContaRecebedor()
    {
        this._has_cdContaRecebedor= false;
    } //-- void deleteCdContaRecebedor() 

    /**
     * Method deleteCdControleCpfCnpjPagador
     * 
     */
    public void deleteCdControleCpfCnpjPagador()
    {
        this._has_cdControleCpfCnpjPagador= false;
    } //-- void deleteCdControleCpfCnpjPagador() 

    /**
     * Method deleteCdControleCpfRecebedor
     * 
     */
    public void deleteCdControleCpfRecebedor()
    {
        this._has_cdControleCpfRecebedor= false;
    } //-- void deleteCdControleCpfRecebedor() 

    /**
     * Method deleteCdCpfCnpjPagador
     * 
     */
    public void deleteCdCpfCnpjPagador()
    {
        this._has_cdCpfCnpjPagador= false;
    } //-- void deleteCdCpfCnpjPagador() 

    /**
     * Method deleteCdCpfCnpjRecebedor
     * 
     */
    public void deleteCdCpfCnpjRecebedor()
    {
        this._has_cdCpfCnpjRecebedor= false;
    } //-- void deleteCdCpfCnpjRecebedor() 

    /**
     * Method deleteCdFilialCnpjRecebedor
     * 
     */
    public void deleteCdFilialCnpjRecebedor()
    {
        this._has_cdFilialCnpjRecebedor= false;
    } //-- void deleteCdFilialCnpjRecebedor() 

    /**
     * Method deleteCdFilialCpfCnpjPagador
     * 
     */
    public void deleteCdFilialCpfCnpjPagador()
    {
        this._has_cdFilialCpfCnpjPagador= false;
    } //-- void deleteCdFilialCpfCnpjPagador() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdSolicitacaoPagamentoIntegrado
     * 
     */
    public void deleteCdSolicitacaoPagamentoIntegrado()
    {
        this._has_cdSolicitacaoPagamentoIntegrado= false;
    } //-- void deleteCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoCtaRecebedor
     * 
     */
    public void deleteCdTipoCtaRecebedor()
    {
        this._has_cdTipoCtaRecebedor= false;
    } //-- void deleteCdTipoCtaRecebedor() 

    /**
     * Method deleteCdTipoInscricaoPagador
     * 
     */
    public void deleteCdTipoInscricaoPagador()
    {
        this._has_cdTipoInscricaoPagador= false;
    } //-- void deleteCdTipoInscricaoPagador() 

    /**
     * Method deleteCdTipoInscricaoRecebedor
     * 
     */
    public void deleteCdTipoInscricaoRecebedor()
    {
        this._has_cdTipoInscricaoRecebedor= false;
    } //-- void deleteCdTipoInscricaoRecebedor() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteCdpessoaJuridicaContrato
     * 
     */
    public void deleteCdpessoaJuridicaContrato()
    {
        this._has_cdpessoaJuridicaContrato= false;
    } //-- void deleteCdpessoaJuridicaContrato() 

    /**
     * Method deleteNrLoteInterno
     * 
     */
    public void deleteNrLoteInterno()
    {
        this._has_nrLoteInterno= false;
    } //-- void deleteNrLoteInterno() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdAgenciaDebito'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaDebito'.
     */
    public int getCdAgenciaDebito()
    {
        return this._cdAgenciaDebito;
    } //-- int getCdAgenciaDebito() 

    /**
     * Returns the value of field 'cdAgenciaRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdAgenciaRecebedor'.
     */
    public int getCdAgenciaRecebedor()
    {
        return this._cdAgenciaRecebedor;
    } //-- int getCdAgenciaRecebedor() 

    /**
     * Returns the value of field 'cdBancoDebito'.
     * 
     * @return int
     * @return the value of field 'cdBancoDebito'.
     */
    public int getCdBancoDebito()
    {
        return this._cdBancoDebito;
    } //-- int getCdBancoDebito() 

    /**
     * Returns the value of field 'cdBcoRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdBcoRecebedor'.
     */
    public int getCdBcoRecebedor()
    {
        return this._cdBcoRecebedor;
    } //-- int getCdBcoRecebedor() 

    /**
     * Returns the value of field 'cdContaDebito'.
     * 
     * @return long
     * @return the value of field 'cdContaDebito'.
     */
    public long getCdContaDebito()
    {
        return this._cdContaDebito;
    } //-- long getCdContaDebito() 

    /**
     * Returns the value of field 'cdContaRecebedor'.
     * 
     * @return long
     * @return the value of field 'cdContaRecebedor'.
     */
    public long getCdContaRecebedor()
    {
        return this._cdContaRecebedor;
    } //-- long getCdContaRecebedor() 

    /**
     * Returns the value of field 'cdControleCpfCnpjPagador'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfCnpjPagador'.
     */
    public int getCdControleCpfCnpjPagador()
    {
        return this._cdControleCpfCnpjPagador;
    } //-- int getCdControleCpfCnpjPagador() 

    /**
     * Returns the value of field 'cdControleCpfRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdControleCpfRecebedor'.
     */
    public int getCdControleCpfRecebedor()
    {
        return this._cdControleCpfRecebedor;
    } //-- int getCdControleCpfRecebedor() 

    /**
     * Returns the value of field 'cdCpfCnpjPagador'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjPagador'.
     */
    public long getCdCpfCnpjPagador()
    {
        return this._cdCpfCnpjPagador;
    } //-- long getCdCpfCnpjPagador() 

    /**
     * Returns the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @return long
     * @return the value of field 'cdCpfCnpjRecebedor'.
     */
    public long getCdCpfCnpjRecebedor()
    {
        return this._cdCpfCnpjRecebedor;
    } //-- long getCdCpfCnpjRecebedor() 

    /**
     * Returns the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdFilialCnpjRecebedor'.
     */
    public int getCdFilialCnpjRecebedor()
    {
        return this._cdFilialCnpjRecebedor;
    } //-- int getCdFilialCnpjRecebedor() 

    /**
     * Returns the value of field 'cdFilialCpfCnpjPagador'.
     * 
     * @return int
     * @return the value of field 'cdFilialCpfCnpjPagador'.
     */
    public int getCdFilialCpfCnpjPagador()
    {
        return this._cdFilialCpfCnpjPagador;
    } //-- int getCdFilialCpfCnpjPagador() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @return int
     * @return the value of field 'cdSolicitacaoPagamentoIntegrado'.
     */
    public int getCdSolicitacaoPagamentoIntegrado()
    {
        return this._cdSolicitacaoPagamentoIntegrado;
    } //-- int getCdSolicitacaoPagamentoIntegrado() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoCtaRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdTipoCtaRecebedor'.
     */
    public int getCdTipoCtaRecebedor()
    {
        return this._cdTipoCtaRecebedor;
    } //-- int getCdTipoCtaRecebedor() 

    /**
     * Returns the value of field 'cdTipoInscricaoPagador'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoPagador'.
     */
    public int getCdTipoInscricaoPagador()
    {
        return this._cdTipoInscricaoPagador;
    } //-- int getCdTipoInscricaoPagador() 

    /**
     * Returns the value of field 'cdTipoInscricaoRecebedor'.
     * 
     * @return int
     * @return the value of field 'cdTipoInscricaoRecebedor'.
     */
    public int getCdTipoInscricaoRecebedor()
    {
        return this._cdTipoInscricaoRecebedor;
    } //-- int getCdTipoInscricaoRecebedor() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdpessoaJuridicaContrato'.
     */
    public long getCdpessoaJuridicaContrato()
    {
        return this._cdpessoaJuridicaContrato;
    } //-- long getCdpessoaJuridicaContrato() 

    /**
     * Returns the value of field 'dtPgtoFinal'.
     * 
     * @return String
     * @return the value of field 'dtPgtoFinal'.
     */
    public java.lang.String getDtPgtoFinal()
    {
        return this._dtPgtoFinal;
    } //-- java.lang.String getDtPgtoFinal() 

    /**
     * Returns the value of field 'dtPgtoInicial'.
     * 
     * @return String
     * @return the value of field 'dtPgtoInicial'.
     */
    public java.lang.String getDtPgtoInicial()
    {
        return this._dtPgtoInicial;
    } //-- java.lang.String getDtPgtoInicial() 

    /**
     * Returns the value of field 'nrLoteInterno'.
     * 
     * @return long
     * @return the value of field 'nrLoteInterno'.
     */
    public long getNrLoteInterno()
    {
        return this._nrLoteInterno;
    } //-- long getNrLoteInterno() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdAgenciaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaDebito()
    {
        return this._has_cdAgenciaDebito;
    } //-- boolean hasCdAgenciaDebito() 

    /**
     * Method hasCdAgenciaRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgenciaRecebedor()
    {
        return this._has_cdAgenciaRecebedor;
    } //-- boolean hasCdAgenciaRecebedor() 

    /**
     * Method hasCdBancoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoDebito()
    {
        return this._has_cdBancoDebito;
    } //-- boolean hasCdBancoDebito() 

    /**
     * Method hasCdBcoRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBcoRecebedor()
    {
        return this._has_cdBcoRecebedor;
    } //-- boolean hasCdBcoRecebedor() 

    /**
     * Method hasCdContaDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaDebito()
    {
        return this._has_cdContaDebito;
    } //-- boolean hasCdContaDebito() 

    /**
     * Method hasCdContaRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaRecebedor()
    {
        return this._has_cdContaRecebedor;
    } //-- boolean hasCdContaRecebedor() 

    /**
     * Method hasCdControleCpfCnpjPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfCnpjPagador()
    {
        return this._has_cdControleCpfCnpjPagador;
    } //-- boolean hasCdControleCpfCnpjPagador() 

    /**
     * Method hasCdControleCpfRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdControleCpfRecebedor()
    {
        return this._has_cdControleCpfRecebedor;
    } //-- boolean hasCdControleCpfRecebedor() 

    /**
     * Method hasCdCpfCnpjPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjPagador()
    {
        return this._has_cdCpfCnpjPagador;
    } //-- boolean hasCdCpfCnpjPagador() 

    /**
     * Method hasCdCpfCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCpfCnpjRecebedor()
    {
        return this._has_cdCpfCnpjRecebedor;
    } //-- boolean hasCdCpfCnpjRecebedor() 

    /**
     * Method hasCdFilialCnpjRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCnpjRecebedor()
    {
        return this._has_cdFilialCnpjRecebedor;
    } //-- boolean hasCdFilialCnpjRecebedor() 

    /**
     * Method hasCdFilialCpfCnpjPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFilialCpfCnpjPagador()
    {
        return this._has_cdFilialCpfCnpjPagador;
    } //-- boolean hasCdFilialCpfCnpjPagador() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdSolicitacaoPagamentoIntegrado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSolicitacaoPagamentoIntegrado()
    {
        return this._has_cdSolicitacaoPagamentoIntegrado;
    } //-- boolean hasCdSolicitacaoPagamentoIntegrado() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoCtaRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoCtaRecebedor()
    {
        return this._has_cdTipoCtaRecebedor;
    } //-- boolean hasCdTipoCtaRecebedor() 

    /**
     * Method hasCdTipoInscricaoPagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoPagador()
    {
        return this._has_cdTipoInscricaoPagador;
    } //-- boolean hasCdTipoInscricaoPagador() 

    /**
     * Method hasCdTipoInscricaoRecebedor
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoInscricaoRecebedor()
    {
        return this._has_cdTipoInscricaoRecebedor;
    } //-- boolean hasCdTipoInscricaoRecebedor() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasCdpessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdpessoaJuridicaContrato()
    {
        return this._has_cdpessoaJuridicaContrato;
    } //-- boolean hasCdpessoaJuridicaContrato() 

    /**
     * Method hasNrLoteInterno
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrLoteInterno()
    {
        return this._has_nrLoteInterno;
    } //-- boolean hasNrLoteInterno() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdAgenciaDebito'.
     * 
     * @param cdAgenciaDebito the value of field 'cdAgenciaDebito'.
     */
    public void setCdAgenciaDebito(int cdAgenciaDebito)
    {
        this._cdAgenciaDebito = cdAgenciaDebito;
        this._has_cdAgenciaDebito = true;
    } //-- void setCdAgenciaDebito(int) 

    /**
     * Sets the value of field 'cdAgenciaRecebedor'.
     * 
     * @param cdAgenciaRecebedor the value of field
     * 'cdAgenciaRecebedor'.
     */
    public void setCdAgenciaRecebedor(int cdAgenciaRecebedor)
    {
        this._cdAgenciaRecebedor = cdAgenciaRecebedor;
        this._has_cdAgenciaRecebedor = true;
    } //-- void setCdAgenciaRecebedor(int) 

    /**
     * Sets the value of field 'cdBancoDebito'.
     * 
     * @param cdBancoDebito the value of field 'cdBancoDebito'.
     */
    public void setCdBancoDebito(int cdBancoDebito)
    {
        this._cdBancoDebito = cdBancoDebito;
        this._has_cdBancoDebito = true;
    } //-- void setCdBancoDebito(int) 

    /**
     * Sets the value of field 'cdBcoRecebedor'.
     * 
     * @param cdBcoRecebedor the value of field 'cdBcoRecebedor'.
     */
    public void setCdBcoRecebedor(int cdBcoRecebedor)
    {
        this._cdBcoRecebedor = cdBcoRecebedor;
        this._has_cdBcoRecebedor = true;
    } //-- void setCdBcoRecebedor(int) 

    /**
     * Sets the value of field 'cdContaDebito'.
     * 
     * @param cdContaDebito the value of field 'cdContaDebito'.
     */
    public void setCdContaDebito(long cdContaDebito)
    {
        this._cdContaDebito = cdContaDebito;
        this._has_cdContaDebito = true;
    } //-- void setCdContaDebito(long) 

    /**
     * Sets the value of field 'cdContaRecebedor'.
     * 
     * @param cdContaRecebedor the value of field 'cdContaRecebedor'
     */
    public void setCdContaRecebedor(long cdContaRecebedor)
    {
        this._cdContaRecebedor = cdContaRecebedor;
        this._has_cdContaRecebedor = true;
    } //-- void setCdContaRecebedor(long) 

    /**
     * Sets the value of field 'cdControleCpfCnpjPagador'.
     * 
     * @param cdControleCpfCnpjPagador the value of field
     * 'cdControleCpfCnpjPagador'.
     */
    public void setCdControleCpfCnpjPagador(int cdControleCpfCnpjPagador)
    {
        this._cdControleCpfCnpjPagador = cdControleCpfCnpjPagador;
        this._has_cdControleCpfCnpjPagador = true;
    } //-- void setCdControleCpfCnpjPagador(int) 

    /**
     * Sets the value of field 'cdControleCpfRecebedor'.
     * 
     * @param cdControleCpfRecebedor the value of field
     * 'cdControleCpfRecebedor'.
     */
    public void setCdControleCpfRecebedor(int cdControleCpfRecebedor)
    {
        this._cdControleCpfRecebedor = cdControleCpfRecebedor;
        this._has_cdControleCpfRecebedor = true;
    } //-- void setCdControleCpfRecebedor(int) 

    /**
     * Sets the value of field 'cdCpfCnpjPagador'.
     * 
     * @param cdCpfCnpjPagador the value of field 'cdCpfCnpjPagador'
     */
    public void setCdCpfCnpjPagador(long cdCpfCnpjPagador)
    {
        this._cdCpfCnpjPagador = cdCpfCnpjPagador;
        this._has_cdCpfCnpjPagador = true;
    } //-- void setCdCpfCnpjPagador(long) 

    /**
     * Sets the value of field 'cdCpfCnpjRecebedor'.
     * 
     * @param cdCpfCnpjRecebedor the value of field
     * 'cdCpfCnpjRecebedor'.
     */
    public void setCdCpfCnpjRecebedor(long cdCpfCnpjRecebedor)
    {
        this._cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
        this._has_cdCpfCnpjRecebedor = true;
    } //-- void setCdCpfCnpjRecebedor(long) 

    /**
     * Sets the value of field 'cdFilialCnpjRecebedor'.
     * 
     * @param cdFilialCnpjRecebedor the value of field
     * 'cdFilialCnpjRecebedor'.
     */
    public void setCdFilialCnpjRecebedor(int cdFilialCnpjRecebedor)
    {
        this._cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
        this._has_cdFilialCnpjRecebedor = true;
    } //-- void setCdFilialCnpjRecebedor(int) 

    /**
     * Sets the value of field 'cdFilialCpfCnpjPagador'.
     * 
     * @param cdFilialCpfCnpjPagador the value of field
     * 'cdFilialCpfCnpjPagador'.
     */
    public void setCdFilialCpfCnpjPagador(int cdFilialCpfCnpjPagador)
    {
        this._cdFilialCpfCnpjPagador = cdFilialCpfCnpjPagador;
        this._has_cdFilialCpfCnpjPagador = true;
    } //-- void setCdFilialCpfCnpjPagador(int) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdSolicitacaoPagamentoIntegrado'.
     * 
     * @param cdSolicitacaoPagamentoIntegrado the value of field
     * 'cdSolicitacaoPagamentoIntegrado'.
     */
    public void setCdSolicitacaoPagamentoIntegrado(int cdSolicitacaoPagamentoIntegrado)
    {
        this._cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
        this._has_cdSolicitacaoPagamentoIntegrado = true;
    } //-- void setCdSolicitacaoPagamentoIntegrado(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoCtaRecebedor'.
     * 
     * @param cdTipoCtaRecebedor the value of field
     * 'cdTipoCtaRecebedor'.
     */
    public void setCdTipoCtaRecebedor(int cdTipoCtaRecebedor)
    {
        this._cdTipoCtaRecebedor = cdTipoCtaRecebedor;
        this._has_cdTipoCtaRecebedor = true;
    } //-- void setCdTipoCtaRecebedor(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoPagador'.
     * 
     * @param cdTipoInscricaoPagador the value of field
     * 'cdTipoInscricaoPagador'.
     */
    public void setCdTipoInscricaoPagador(int cdTipoInscricaoPagador)
    {
        this._cdTipoInscricaoPagador = cdTipoInscricaoPagador;
        this._has_cdTipoInscricaoPagador = true;
    } //-- void setCdTipoInscricaoPagador(int) 

    /**
     * Sets the value of field 'cdTipoInscricaoRecebedor'.
     * 
     * @param cdTipoInscricaoRecebedor the value of field
     * 'cdTipoInscricaoRecebedor'.
     */
    public void setCdTipoInscricaoRecebedor(int cdTipoInscricaoRecebedor)
    {
        this._cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
        this._has_cdTipoInscricaoRecebedor = true;
    } //-- void setCdTipoInscricaoRecebedor(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'cdpessoaJuridicaContrato'.
     * 
     * @param cdpessoaJuridicaContrato the value of field
     * 'cdpessoaJuridicaContrato'.
     */
    public void setCdpessoaJuridicaContrato(long cdpessoaJuridicaContrato)
    {
        this._cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
        this._has_cdpessoaJuridicaContrato = true;
    } //-- void setCdpessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'dtPgtoFinal'.
     * 
     * @param dtPgtoFinal the value of field 'dtPgtoFinal'.
     */
    public void setDtPgtoFinal(java.lang.String dtPgtoFinal)
    {
        this._dtPgtoFinal = dtPgtoFinal;
    } //-- void setDtPgtoFinal(java.lang.String) 

    /**
     * Sets the value of field 'dtPgtoInicial'.
     * 
     * @param dtPgtoInicial the value of field 'dtPgtoInicial'.
     */
    public void setDtPgtoInicial(java.lang.String dtPgtoInicial)
    {
        this._dtPgtoInicial = dtPgtoInicial;
    } //-- void setDtPgtoInicial(java.lang.String) 

    /**
     * Sets the value of field 'nrLoteInterno'.
     * 
     * @param nrLoteInterno the value of field 'nrLoteInterno'.
     */
    public void setNrLoteInterno(long nrLoteInterno)
    {
        this._nrLoteInterno = nrLoteInterno;
        this._has_nrLoteInterno = true;
    } //-- void setNrLoteInterno(long) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarQtdeValorPgtoPrevistoSolicRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.request.ConsultarQtdeValorPgtoPrevistoSolicRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.request.ConsultarQtdeValorPgtoPrevistoSolicRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.request.ConsultarQtdeValorPgtoPrevistoSolicRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarqtdevalorpgtoprevistosolic.request.ConsultarQtdeValorPgtoPrevistoSolicRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
