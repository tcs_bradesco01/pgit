/*
 * Nome: br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean;

/**
 * Nome: ExcluirAlteracaoAmbientePartcEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ExcluirAlteracaoAmbientePartcEntradaDTO {
	
	/** Atributo cdpessoaJuridicaContrato. */
	private Long cdpessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo cdSolicitacaoPagamentoIntegrado. */
    private Integer cdSolicitacaoPagamentoIntegrado;
    
    /** Atributo nrSolicitacaoPagamentoIntegrado. */
    private Integer nrSolicitacaoPagamentoIntegrado;
	
    /**
     * Excluir alteracao ambiente partc entrada dto.
     */
    public ExcluirAlteracaoAmbientePartcEntradaDTO() {
    	super();
    }
	
	/**
	 * Excluir alteracao ambiente partc entrada dto.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
	 */
	public ExcluirAlteracaoAmbientePartcEntradaDTO(Long cdpessoaJuridicaContrato,
			Integer cdTipoContratoNegocio, Long nrSequenciaContratoNegocio,
			Integer cdSolicitacaoPagamentoIntegrado,
			Integer nrSolicitacaoPagamentoIntegrado) {
		super();
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}

	/**
	 * Get: cdpessoaJuridicaContrato.
	 *
	 * @return cdpessoaJuridicaContrato
	 */
	public Long getCdpessoaJuridicaContrato() {
		return cdpessoaJuridicaContrato;
	}
	
	/**
	 * Set: cdpessoaJuridicaContrato.
	 *
	 * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
	 */
	public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
		this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}
	
	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}
	
	/**
	 * Get: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @return cdSolicitacaoPagamentoIntegrado
	 */
	public Integer getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(
			Integer cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @return nrSolicitacaoPagamentoIntegrado
	 */
	public Integer getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
	 */
	public void setNrSolicitacaoPagamentoIntegrado(
			Integer nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}
	
	

}
