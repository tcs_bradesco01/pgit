/*
 * Nome: br.com.bradesco.web.pgit.service.business.combo.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.combo.bean;

/**
 * Nome: ListarIndiceEconomicoSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarIndiceEconomicoSaidaDTO {
	
	/** Atributo cdIndicadorEconomico. */
	private int cdIndicadorEconomico;
	
	/** Atributo dsIndicadorEconomico. */
	private String dsIndicadorEconomico;
	
	/** Atributo dsSiglaEconomico. */
	private int dsSiglaEconomico;
	
	
	/**
	 * Get: cdIndicadorEconomico.
	 *
	 * @return cdIndicadorEconomico
	 */
	public int getCdIndicadorEconomico() {
		return cdIndicadorEconomico;
	}
	
	/**
	 * Set: cdIndicadorEconomico.
	 *
	 * @param cdIndicadorEconomico the cd indicador economico
	 */
	public void setCdIndicadorEconomico(int cdIndicadorEconomico) {
		this.cdIndicadorEconomico = cdIndicadorEconomico;
	}
	
	/**
	 * Get: dsIndicadorEconomico.
	 *
	 * @return dsIndicadorEconomico
	 */
	public String getDsIndicadorEconomico() {
		return dsIndicadorEconomico;
	}
	
	/**
	 * Set: dsIndicadorEconomico.
	 *
	 * @param dsIndicadorEconomico the ds indicador economico
	 */
	public void setDsIndicadorEconomico(String dsIndicadorEconomico) {
		this.dsIndicadorEconomico = dsIndicadorEconomico;
	}
	
	/**
	 * Get: dsSiglaEconomico.
	 *
	 * @return dsSiglaEconomico
	 */
	public int getDsSiglaEconomico() {
		return dsSiglaEconomico;
	}
	
	/**
	 * Set: dsSiglaEconomico.
	 *
	 * @param dsSiglaEconomico the ds sigla economico
	 */
	public void setDsSiglaEconomico(int dsSiglaEconomico) {
		this.dsSiglaEconomico = dsSiglaEconomico;
	}
	
	/**
	 * Listar indice economico saida dto.
	 *
	 * @param cdIndicadorEconomico the cd indicador economico
	 * @param dsIndicadorEconomico the ds indicador economico
	 * @param dsSiglaEconomico the ds sigla economico
	 */
	public ListarIndiceEconomicoSaidaDTO (int cdIndicadorEconomico, String dsIndicadorEconomico, int dsSiglaEconomico) {
		super();
		this.cdIndicadorEconomico = cdIndicadorEconomico;
		this.dsIndicadorEconomico = dsIndicadorEconomico;
		this.dsSiglaEconomico = dsSiglaEconomico;
	}
	
	
	
	

}
