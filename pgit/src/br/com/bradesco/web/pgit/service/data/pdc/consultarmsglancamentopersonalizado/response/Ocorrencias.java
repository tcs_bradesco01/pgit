/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarmsglancamentopersonalizado.response;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class Ocorrencias.
 * 
 * @version $Revision$ $Date$
 */
public class Ocorrencias implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _cdMensagemLinhasExtrato
     */
    private int _cdMensagemLinhasExtrato = 0;

    /**
     * keeps track of state for field: _cdMensagemLinhasExtrato
     */
    private boolean _has_cdMensagemLinhasExtrato;

    /**
     * Field _cdIndicadorRestricaoContrato
     */
    private int _cdIndicadorRestricaoContrato = 0;

    /**
     * keeps track of state for field: _cdIndicadorRestricaoContrato
     */
    private boolean _has_cdIndicadorRestricaoContrato;

    /**
     * Field _cdTipoMensagemExtrato
     */
    private int _cdTipoMensagemExtrato = 0;

    /**
     * keeps track of state for field: _cdTipoMensagemExtrato
     */
    private boolean _has_cdTipoMensagemExtrato;

    /**
     * Field _cdIdentificadorLancamentoDebito
     */
    private int _cdIdentificadorLancamentoDebito = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificadorLancamentoDebito
     */
    private boolean _has_cdIdentificadorLancamentoDebito;

    /**
     * Field _dsIdentificadorLancamentoDebito
     */
    private java.lang.String _dsIdentificadorLancamentoDebito;

    /**
     * Field _cdIdentificadorLancamentoCredito
     */
    private int _cdIdentificadorLancamentoCredito = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificadorLancamentoCredito
     */
    private boolean _has_cdIdentificadorLancamentoCredito;

    /**
     * Field _dsIdentificadorLancamentoCredito
     */
    private java.lang.String _dsIdentificadorLancamentoCredito;

    /**
     * Field _cdProdutoServidoOperacao
     */
    private int _cdProdutoServidoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServidoOperacao
     */
    private boolean _has_cdProdutoServidoOperacao;

    /**
     * Field _dsProdutoServidoOperacao
     */
    private java.lang.String _dsProdutoServidoOperacao;

    /**
     * Field _cdProdutoOperacaoRelacionado
     */
    private int _cdProdutoOperacaoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoOperacaoRelacionado
     */
    private boolean _has_cdProdutoOperacaoRelacionado;

    /**
     * Field _dsProdutoOperacaoRelacionado
     */
    private java.lang.String _dsProdutoOperacaoRelacionado;

    /**
     * Field _cdRelacionadoProduto
     */
    private int _cdRelacionadoProduto = 0;

    /**
     * keeps track of state for field: _cdRelacionadoProduto
     */
    private boolean _has_cdRelacionadoProduto;


      //----------------/
     //- Constructors -/
    //----------------/

    public Ocorrencias() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmsglancamentopersonalizado.response.Ocorrencias()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdIdentificadorLancamentoCredito
     * 
     */
    public void deleteCdIdentificadorLancamentoCredito()
    {
        this._has_cdIdentificadorLancamentoCredito= false;
    } //-- void deleteCdIdentificadorLancamentoCredito() 

    /**
     * Method deleteCdIdentificadorLancamentoDebito
     * 
     */
    public void deleteCdIdentificadorLancamentoDebito()
    {
        this._has_cdIdentificadorLancamentoDebito= false;
    } //-- void deleteCdIdentificadorLancamentoDebito() 

    /**
     * Method deleteCdIndicadorRestricaoContrato
     * 
     */
    public void deleteCdIndicadorRestricaoContrato()
    {
        this._has_cdIndicadorRestricaoContrato= false;
    } //-- void deleteCdIndicadorRestricaoContrato() 

    /**
     * Method deleteCdMensagemLinhasExtrato
     * 
     */
    public void deleteCdMensagemLinhasExtrato()
    {
        this._has_cdMensagemLinhasExtrato= false;
    } //-- void deleteCdMensagemLinhasExtrato() 

    /**
     * Method deleteCdProdutoOperacaoRelacionado
     * 
     */
    public void deleteCdProdutoOperacaoRelacionado()
    {
        this._has_cdProdutoOperacaoRelacionado= false;
    } //-- void deleteCdProdutoOperacaoRelacionado() 

    /**
     * Method deleteCdProdutoServidoOperacao
     * 
     */
    public void deleteCdProdutoServidoOperacao()
    {
        this._has_cdProdutoServidoOperacao= false;
    } //-- void deleteCdProdutoServidoOperacao() 

    /**
     * Method deleteCdRelacionadoProduto
     * 
     */
    public void deleteCdRelacionadoProduto()
    {
        this._has_cdRelacionadoProduto= false;
    } //-- void deleteCdRelacionadoProduto() 

    /**
     * Method deleteCdTipoMensagemExtrato
     * 
     */
    public void deleteCdTipoMensagemExtrato()
    {
        this._has_cdTipoMensagemExtrato= false;
    } //-- void deleteCdTipoMensagemExtrato() 

    /**
     * Returns the value of field
     * 'cdIdentificadorLancamentoCredito'.
     * 
     * @return int
     * @return the value of field 'cdIdentificadorLancamentoCredito'
     */
    public int getCdIdentificadorLancamentoCredito()
    {
        return this._cdIdentificadorLancamentoCredito;
    } //-- int getCdIdentificadorLancamentoCredito() 

    /**
     * Returns the value of field
     * 'cdIdentificadorLancamentoDebito'.
     * 
     * @return int
     * @return the value of field 'cdIdentificadorLancamentoDebito'.
     */
    public int getCdIdentificadorLancamentoDebito()
    {
        return this._cdIdentificadorLancamentoDebito;
    } //-- int getCdIdentificadorLancamentoDebito() 

    /**
     * Returns the value of field 'cdIndicadorRestricaoContrato'.
     * 
     * @return int
     * @return the value of field 'cdIndicadorRestricaoContrato'.
     */
    public int getCdIndicadorRestricaoContrato()
    {
        return this._cdIndicadorRestricaoContrato;
    } //-- int getCdIndicadorRestricaoContrato() 

    /**
     * Returns the value of field 'cdMensagemLinhasExtrato'.
     * 
     * @return int
     * @return the value of field 'cdMensagemLinhasExtrato'.
     */
    public int getCdMensagemLinhasExtrato()
    {
        return this._cdMensagemLinhasExtrato;
    } //-- int getCdMensagemLinhasExtrato() 

    /**
     * Returns the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoOperacaoRelacionado'.
     */
    public int getCdProdutoOperacaoRelacionado()
    {
        return this._cdProdutoOperacaoRelacionado;
    } //-- int getCdProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'cdProdutoServidoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServidoOperacao'.
     */
    public int getCdProdutoServidoOperacao()
    {
        return this._cdProdutoServidoOperacao;
    } //-- int getCdProdutoServidoOperacao() 

    /**
     * Returns the value of field 'cdRelacionadoProduto'.
     * 
     * @return int
     * @return the value of field 'cdRelacionadoProduto'.
     */
    public int getCdRelacionadoProduto()
    {
        return this._cdRelacionadoProduto;
    } //-- int getCdRelacionadoProduto() 

    /**
     * Returns the value of field 'cdTipoMensagemExtrato'.
     * 
     * @return int
     * @return the value of field 'cdTipoMensagemExtrato'.
     */
    public int getCdTipoMensagemExtrato()
    {
        return this._cdTipoMensagemExtrato;
    } //-- int getCdTipoMensagemExtrato() 

    /**
     * Returns the value of field
     * 'dsIdentificadorLancamentoCredito'.
     * 
     * @return String
     * @return the value of field 'dsIdentificadorLancamentoCredito'
     */
    public java.lang.String getDsIdentificadorLancamentoCredito()
    {
        return this._dsIdentificadorLancamentoCredito;
    } //-- java.lang.String getDsIdentificadorLancamentoCredito() 

    /**
     * Returns the value of field
     * 'dsIdentificadorLancamentoDebito'.
     * 
     * @return String
     * @return the value of field 'dsIdentificadorLancamentoDebito'.
     */
    public java.lang.String getDsIdentificadorLancamentoDebito()
    {
        return this._dsIdentificadorLancamentoDebito;
    } //-- java.lang.String getDsIdentificadorLancamentoDebito() 

    /**
     * Returns the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @return String
     * @return the value of field 'dsProdutoOperacaoRelacionado'.
     */
    public java.lang.String getDsProdutoOperacaoRelacionado()
    {
        return this._dsProdutoOperacaoRelacionado;
    } //-- java.lang.String getDsProdutoOperacaoRelacionado() 

    /**
     * Returns the value of field 'dsProdutoServidoOperacao'.
     * 
     * @return String
     * @return the value of field 'dsProdutoServidoOperacao'.
     */
    public java.lang.String getDsProdutoServidoOperacao()
    {
        return this._dsProdutoServidoOperacao;
    } //-- java.lang.String getDsProdutoServidoOperacao() 

    /**
     * Method hasCdIdentificadorLancamentoCredito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificadorLancamentoCredito()
    {
        return this._has_cdIdentificadorLancamentoCredito;
    } //-- boolean hasCdIdentificadorLancamentoCredito() 

    /**
     * Method hasCdIdentificadorLancamentoDebito
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificadorLancamentoDebito()
    {
        return this._has_cdIdentificadorLancamentoDebito;
    } //-- boolean hasCdIdentificadorLancamentoDebito() 

    /**
     * Method hasCdIndicadorRestricaoContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIndicadorRestricaoContrato()
    {
        return this._has_cdIndicadorRestricaoContrato;
    } //-- boolean hasCdIndicadorRestricaoContrato() 

    /**
     * Method hasCdMensagemLinhasExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdMensagemLinhasExtrato()
    {
        return this._has_cdMensagemLinhasExtrato;
    } //-- boolean hasCdMensagemLinhasExtrato() 

    /**
     * Method hasCdProdutoOperacaoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoOperacaoRelacionado()
    {
        return this._has_cdProdutoOperacaoRelacionado;
    } //-- boolean hasCdProdutoOperacaoRelacionado() 

    /**
     * Method hasCdProdutoServidoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServidoOperacao()
    {
        return this._has_cdProdutoServidoOperacao;
    } //-- boolean hasCdProdutoServidoOperacao() 

    /**
     * Method hasCdRelacionadoProduto
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdRelacionadoProduto()
    {
        return this._has_cdRelacionadoProduto;
    } //-- boolean hasCdRelacionadoProduto() 

    /**
     * Method hasCdTipoMensagemExtrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoMensagemExtrato()
    {
        return this._has_cdTipoMensagemExtrato;
    } //-- boolean hasCdTipoMensagemExtrato() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdIdentificadorLancamentoCredito'.
     * 
     * @param cdIdentificadorLancamentoCredito the value of field
     * 'cdIdentificadorLancamentoCredito'.
     */
    public void setCdIdentificadorLancamentoCredito(int cdIdentificadorLancamentoCredito)
    {
        this._cdIdentificadorLancamentoCredito = cdIdentificadorLancamentoCredito;
        this._has_cdIdentificadorLancamentoCredito = true;
    } //-- void setCdIdentificadorLancamentoCredito(int) 

    /**
     * Sets the value of field 'cdIdentificadorLancamentoDebito'.
     * 
     * @param cdIdentificadorLancamentoDebito the value of field
     * 'cdIdentificadorLancamentoDebito'.
     */
    public void setCdIdentificadorLancamentoDebito(int cdIdentificadorLancamentoDebito)
    {
        this._cdIdentificadorLancamentoDebito = cdIdentificadorLancamentoDebito;
        this._has_cdIdentificadorLancamentoDebito = true;
    } //-- void setCdIdentificadorLancamentoDebito(int) 

    /**
     * Sets the value of field 'cdIndicadorRestricaoContrato'.
     * 
     * @param cdIndicadorRestricaoContrato the value of field
     * 'cdIndicadorRestricaoContrato'.
     */
    public void setCdIndicadorRestricaoContrato(int cdIndicadorRestricaoContrato)
    {
        this._cdIndicadorRestricaoContrato = cdIndicadorRestricaoContrato;
        this._has_cdIndicadorRestricaoContrato = true;
    } //-- void setCdIndicadorRestricaoContrato(int) 

    /**
     * Sets the value of field 'cdMensagemLinhasExtrato'.
     * 
     * @param cdMensagemLinhasExtrato the value of field
     * 'cdMensagemLinhasExtrato'.
     */
    public void setCdMensagemLinhasExtrato(int cdMensagemLinhasExtrato)
    {
        this._cdMensagemLinhasExtrato = cdMensagemLinhasExtrato;
        this._has_cdMensagemLinhasExtrato = true;
    } //-- void setCdMensagemLinhasExtrato(int) 

    /**
     * Sets the value of field 'cdProdutoOperacaoRelacionado'.
     * 
     * @param cdProdutoOperacaoRelacionado the value of field
     * 'cdProdutoOperacaoRelacionado'.
     */
    public void setCdProdutoOperacaoRelacionado(int cdProdutoOperacaoRelacionado)
    {
        this._cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
        this._has_cdProdutoOperacaoRelacionado = true;
    } //-- void setCdProdutoOperacaoRelacionado(int) 

    /**
     * Sets the value of field 'cdProdutoServidoOperacao'.
     * 
     * @param cdProdutoServidoOperacao the value of field
     * 'cdProdutoServidoOperacao'.
     */
    public void setCdProdutoServidoOperacao(int cdProdutoServidoOperacao)
    {
        this._cdProdutoServidoOperacao = cdProdutoServidoOperacao;
        this._has_cdProdutoServidoOperacao = true;
    } //-- void setCdProdutoServidoOperacao(int) 

    /**
     * Sets the value of field 'cdRelacionadoProduto'.
     * 
     * @param cdRelacionadoProduto the value of field
     * 'cdRelacionadoProduto'.
     */
    public void setCdRelacionadoProduto(int cdRelacionadoProduto)
    {
        this._cdRelacionadoProduto = cdRelacionadoProduto;
        this._has_cdRelacionadoProduto = true;
    } //-- void setCdRelacionadoProduto(int) 

    /**
     * Sets the value of field 'cdTipoMensagemExtrato'.
     * 
     * @param cdTipoMensagemExtrato the value of field
     * 'cdTipoMensagemExtrato'.
     */
    public void setCdTipoMensagemExtrato(int cdTipoMensagemExtrato)
    {
        this._cdTipoMensagemExtrato = cdTipoMensagemExtrato;
        this._has_cdTipoMensagemExtrato = true;
    } //-- void setCdTipoMensagemExtrato(int) 

    /**
     * Sets the value of field 'dsIdentificadorLancamentoCredito'.
     * 
     * @param dsIdentificadorLancamentoCredito the value of field
     * 'dsIdentificadorLancamentoCredito'.
     */
    public void setDsIdentificadorLancamentoCredito(java.lang.String dsIdentificadorLancamentoCredito)
    {
        this._dsIdentificadorLancamentoCredito = dsIdentificadorLancamentoCredito;
    } //-- void setDsIdentificadorLancamentoCredito(java.lang.String) 

    /**
     * Sets the value of field 'dsIdentificadorLancamentoDebito'.
     * 
     * @param dsIdentificadorLancamentoDebito the value of field
     * 'dsIdentificadorLancamentoDebito'.
     */
    public void setDsIdentificadorLancamentoDebito(java.lang.String dsIdentificadorLancamentoDebito)
    {
        this._dsIdentificadorLancamentoDebito = dsIdentificadorLancamentoDebito;
    } //-- void setDsIdentificadorLancamentoDebito(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoOperacaoRelacionado'.
     * 
     * @param dsProdutoOperacaoRelacionado the value of field
     * 'dsProdutoOperacaoRelacionado'.
     */
    public void setDsProdutoOperacaoRelacionado(java.lang.String dsProdutoOperacaoRelacionado)
    {
        this._dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
    } //-- void setDsProdutoOperacaoRelacionado(java.lang.String) 

    /**
     * Sets the value of field 'dsProdutoServidoOperacao'.
     * 
     * @param dsProdutoServidoOperacao the value of field
     * 'dsProdutoServidoOperacao'.
     */
    public void setDsProdutoServidoOperacao(java.lang.String dsProdutoServidoOperacao)
    {
        this._dsProdutoServidoOperacao = dsProdutoServidoOperacao;
    } //-- void setDsProdutoServidoOperacao(java.lang.String) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return Ocorrencias
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarmsglancamentopersonalizado.response.Ocorrencias unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarmsglancamentopersonalizado.response.Ocorrencias) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarmsglancamentopersonalizado.response.Ocorrencias.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarmsglancamentopersonalizado.response.Ocorrencias unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
