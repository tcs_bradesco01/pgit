/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.ContentHandler;

/**
 * Class ConsultarPagamentoFavorecidoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarPagamentoFavorecidoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _agePagoNaoPago
     */
    private int _agePagoNaoPago = 0;

    /**
     * keeps track of state for field: _agePagoNaoPago
     */
    private boolean _has_agePagoNaoPago;

    /**
     * Field _numeroOcorrencias
     */
    private int _numeroOcorrencias = 0;

    /**
     * keeps track of state for field: _numeroOcorrencias
     */
    private boolean _has_numeroOcorrencias;

    /**
     * Field _dtCreditoPagamentoInicial
     */
    private java.lang.String _dtCreditoPagamentoInicial;

    /**
     * Field _dtCreditoPagamentoFinal
     */
    private java.lang.String _dtCreditoPagamentoFinal;

    /**
     * Field _cdProdutoServicoOperacao
     */
    private int _cdProdutoServicoOperacao = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoOperacao
     */
    private boolean _has_cdProdutoServicoOperacao;

    /**
     * Field _cdProdutoServicoRelacionado
     */
    private int _cdProdutoServicoRelacionado = 0;

    /**
     * keeps track of state for field: _cdProdutoServicoRelacionado
     */
    private boolean _has_cdProdutoServicoRelacionado;

    /**
     * Field _cdFavorecidoClientePagador
     */
    private long _cdFavorecidoClientePagador = 0;

    /**
     * keeps track of state for field: _cdFavorecidoClientePagador
     */
    private boolean _has_cdFavorecidoClientePagador;

    /**
     * Field _cdInscricaoFavorecidoCliente
     */
    private long _cdInscricaoFavorecidoCliente = 0;

    /**
     * keeps track of state for field: _cdInscricaoFavorecidoCliente
     */
    private boolean _has_cdInscricaoFavorecidoCliente;

    /**
     * Field _cdIdentificacaoInscricaoFavorecido
     */
    private int _cdIdentificacaoInscricaoFavorecido = 0;

    /**
     * keeps track of state for field:
     * _cdIdentificacaoInscricaoFavorecido
     */
    private boolean _has_cdIdentificacaoInscricaoFavorecido;

    /**
     * Field _cdBanco
     */
    private int _cdBanco = 0;

    /**
     * keeps track of state for field: _cdBanco
     */
    private boolean _has_cdBanco;

    /**
     * Field _cdAgencia
     */
    private int _cdAgencia = 0;

    /**
     * keeps track of state for field: _cdAgencia
     */
    private boolean _has_cdAgencia;

    /**
     * Field _cdConta
     */
    private long _cdConta = 0;

    /**
     * keeps track of state for field: _cdConta
     */
    private boolean _has_cdConta;

    /**
     * Field _cdDigitoConta
     */
    private java.lang.String _cdDigitoConta;

    /**
     * Field _cdEmpresaContrato
     */
    private long _cdEmpresaContrato = 0;

    /**
     * keeps track of state for field: _cdEmpresaContrato
     */
    private boolean _has_cdEmpresaContrato;

    /**
     * Field _cdTipoConta
     */
    private int _cdTipoConta = 0;

    /**
     * keeps track of state for field: _cdTipoConta
     */
    private boolean _has_cdTipoConta;

    /**
     * Field _nrContrato
     */
    private long _nrContrato = 0;

    /**
     * keeps track of state for field: _nrContrato
     */
    private boolean _has_nrContrato;

    /**
     * Field _nrUnidadeOrganizacional
     */
    private int _nrUnidadeOrganizacional = 0;

    /**
     * keeps track of state for field: _nrUnidadeOrganizacional
     */
    private boolean _has_nrUnidadeOrganizacional;

    /**
     * Field _cdBeneficiario
     */
    private long _cdBeneficiario = 0;

    /**
     * keeps track of state for field: _cdBeneficiario
     */
    private boolean _has_cdBeneficiario;

    /**
     * Field _cdEspecieBeneficioINSS
     */
    private int _cdEspecieBeneficioINSS = 0;

    /**
     * keeps track of state for field: _cdEspecieBeneficioINSS
     */
    private boolean _has_cdEspecieBeneficioINSS;

    /**
     * Field _cdClassificacao
     */
    private java.lang.String _cdClassificacao;

    /**
     * Field _cdBancoSalarial
     */
    private int _cdBancoSalarial = 0;

    /**
     * keeps track of state for field: _cdBancoSalarial
     */
    private boolean _has_cdBancoSalarial;

    /**
     * Field _cdAgencaSalarial
     */
    private int _cdAgencaSalarial = 0;

    /**
     * keeps track of state for field: _cdAgencaSalarial
     */
    private boolean _has_cdAgencaSalarial;

    /**
     * Field _cdContaSalarial
     */
    private long _cdContaSalarial = 0;

    /**
     * keeps track of state for field: _cdContaSalarial
     */
    private boolean _has_cdContaSalarial;

    /**
     * Field _cdIspbPagtoDestino
     */
    private java.lang.String _cdIspbPagtoDestino;

    /**
     * Field _contaPagtoDestino
     */
    private java.lang.String _contaPagtoDestino = "0";


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarPagamentoFavorecidoRequest() 
     {
        super();
        setContaPagtoDestino("0");
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.request.ConsultarPagamentoFavorecidoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteAgePagoNaoPago
     * 
     */
    public void deleteAgePagoNaoPago()
    {
        this._has_agePagoNaoPago= false;
    } //-- void deleteAgePagoNaoPago() 

    /**
     * Method deleteCdAgencaSalarial
     * 
     */
    public void deleteCdAgencaSalarial()
    {
        this._has_cdAgencaSalarial= false;
    } //-- void deleteCdAgencaSalarial() 

    /**
     * Method deleteCdAgencia
     * 
     */
    public void deleteCdAgencia()
    {
        this._has_cdAgencia= false;
    } //-- void deleteCdAgencia() 

    /**
     * Method deleteCdBanco
     * 
     */
    public void deleteCdBanco()
    {
        this._has_cdBanco= false;
    } //-- void deleteCdBanco() 

    /**
     * Method deleteCdBancoSalarial
     * 
     */
    public void deleteCdBancoSalarial()
    {
        this._has_cdBancoSalarial= false;
    } //-- void deleteCdBancoSalarial() 

    /**
     * Method deleteCdBeneficiario
     * 
     */
    public void deleteCdBeneficiario()
    {
        this._has_cdBeneficiario= false;
    } //-- void deleteCdBeneficiario() 

    /**
     * Method deleteCdConta
     * 
     */
    public void deleteCdConta()
    {
        this._has_cdConta= false;
    } //-- void deleteCdConta() 

    /**
     * Method deleteCdContaSalarial
     * 
     */
    public void deleteCdContaSalarial()
    {
        this._has_cdContaSalarial= false;
    } //-- void deleteCdContaSalarial() 

    /**
     * Method deleteCdEmpresaContrato
     * 
     */
    public void deleteCdEmpresaContrato()
    {
        this._has_cdEmpresaContrato= false;
    } //-- void deleteCdEmpresaContrato() 

    /**
     * Method deleteCdEspecieBeneficioINSS
     * 
     */
    public void deleteCdEspecieBeneficioINSS()
    {
        this._has_cdEspecieBeneficioINSS= false;
    } //-- void deleteCdEspecieBeneficioINSS() 

    /**
     * Method deleteCdFavorecidoClientePagador
     * 
     */
    public void deleteCdFavorecidoClientePagador()
    {
        this._has_cdFavorecidoClientePagador= false;
    } //-- void deleteCdFavorecidoClientePagador() 

    /**
     * Method deleteCdIdentificacaoInscricaoFavorecido
     * 
     */
    public void deleteCdIdentificacaoInscricaoFavorecido()
    {
        this._has_cdIdentificacaoInscricaoFavorecido= false;
    } //-- void deleteCdIdentificacaoInscricaoFavorecido() 

    /**
     * Method deleteCdInscricaoFavorecidoCliente
     * 
     */
    public void deleteCdInscricaoFavorecidoCliente()
    {
        this._has_cdInscricaoFavorecidoCliente= false;
    } //-- void deleteCdInscricaoFavorecidoCliente() 

    /**
     * Method deleteCdProdutoServicoOperacao
     * 
     */
    public void deleteCdProdutoServicoOperacao()
    {
        this._has_cdProdutoServicoOperacao= false;
    } //-- void deleteCdProdutoServicoOperacao() 

    /**
     * Method deleteCdProdutoServicoRelacionado
     * 
     */
    public void deleteCdProdutoServicoRelacionado()
    {
        this._has_cdProdutoServicoRelacionado= false;
    } //-- void deleteCdProdutoServicoRelacionado() 

    /**
     * Method deleteCdTipoConta
     * 
     */
    public void deleteCdTipoConta()
    {
        this._has_cdTipoConta= false;
    } //-- void deleteCdTipoConta() 

    /**
     * Method deleteNrContrato
     * 
     */
    public void deleteNrContrato()
    {
        this._has_nrContrato= false;
    } //-- void deleteNrContrato() 

    /**
     * Method deleteNrUnidadeOrganizacional
     * 
     */
    public void deleteNrUnidadeOrganizacional()
    {
        this._has_nrUnidadeOrganizacional= false;
    } //-- void deleteNrUnidadeOrganizacional() 

    /**
     * Method deleteNumeroOcorrencias
     * 
     */
    public void deleteNumeroOcorrencias()
    {
        this._has_numeroOcorrencias= false;
    } //-- void deleteNumeroOcorrencias() 

    /**
     * Returns the value of field 'agePagoNaoPago'.
     * 
     * @return int
     * @return the value of field 'agePagoNaoPago'.
     */
    public int getAgePagoNaoPago()
    {
        return this._agePagoNaoPago;
    } //-- int getAgePagoNaoPago() 

    /**
     * Returns the value of field 'cdAgencaSalarial'.
     * 
     * @return int
     * @return the value of field 'cdAgencaSalarial'.
     */
    public int getCdAgencaSalarial()
    {
        return this._cdAgencaSalarial;
    } //-- int getCdAgencaSalarial() 

    /**
     * Returns the value of field 'cdAgencia'.
     * 
     * @return int
     * @return the value of field 'cdAgencia'.
     */
    public int getCdAgencia()
    {
        return this._cdAgencia;
    } //-- int getCdAgencia() 

    /**
     * Returns the value of field 'cdBanco'.
     * 
     * @return int
     * @return the value of field 'cdBanco'.
     */
    public int getCdBanco()
    {
        return this._cdBanco;
    } //-- int getCdBanco() 

    /**
     * Returns the value of field 'cdBancoSalarial'.
     * 
     * @return int
     * @return the value of field 'cdBancoSalarial'.
     */
    public int getCdBancoSalarial()
    {
        return this._cdBancoSalarial;
    } //-- int getCdBancoSalarial() 

    /**
     * Returns the value of field 'cdBeneficiario'.
     * 
     * @return long
     * @return the value of field 'cdBeneficiario'.
     */
    public long getCdBeneficiario()
    {
        return this._cdBeneficiario;
    } //-- long getCdBeneficiario() 

    /**
     * Returns the value of field 'cdClassificacao'.
     * 
     * @return String
     * @return the value of field 'cdClassificacao'.
     */
    public java.lang.String getCdClassificacao()
    {
        return this._cdClassificacao;
    } //-- java.lang.String getCdClassificacao() 

    /**
     * Returns the value of field 'cdConta'.
     * 
     * @return long
     * @return the value of field 'cdConta'.
     */
    public long getCdConta()
    {
        return this._cdConta;
    } //-- long getCdConta() 

    /**
     * Returns the value of field 'cdContaSalarial'.
     * 
     * @return long
     * @return the value of field 'cdContaSalarial'.
     */
    public long getCdContaSalarial()
    {
        return this._cdContaSalarial;
    } //-- long getCdContaSalarial() 

    /**
     * Returns the value of field 'cdDigitoConta'.
     * 
     * @return String
     * @return the value of field 'cdDigitoConta'.
     */
    public java.lang.String getCdDigitoConta()
    {
        return this._cdDigitoConta;
    } //-- java.lang.String getCdDigitoConta() 

    /**
     * Returns the value of field 'cdEmpresaContrato'.
     * 
     * @return long
     * @return the value of field 'cdEmpresaContrato'.
     */
    public long getCdEmpresaContrato()
    {
        return this._cdEmpresaContrato;
    } //-- long getCdEmpresaContrato() 

    /**
     * Returns the value of field 'cdEspecieBeneficioINSS'.
     * 
     * @return int
     * @return the value of field 'cdEspecieBeneficioINSS'.
     */
    public int getCdEspecieBeneficioINSS()
    {
        return this._cdEspecieBeneficioINSS;
    } //-- int getCdEspecieBeneficioINSS() 

    /**
     * Returns the value of field 'cdFavorecidoClientePagador'.
     * 
     * @return long
     * @return the value of field 'cdFavorecidoClientePagador'.
     */
    public long getCdFavorecidoClientePagador()
    {
        return this._cdFavorecidoClientePagador;
    } //-- long getCdFavorecidoClientePagador() 

    /**
     * Returns the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     * 
     * @return int
     * @return the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     */
    public int getCdIdentificacaoInscricaoFavorecido()
    {
        return this._cdIdentificacaoInscricaoFavorecido;
    } //-- int getCdIdentificacaoInscricaoFavorecido() 

    /**
     * Returns the value of field 'cdInscricaoFavorecidoCliente'.
     * 
     * @return long
     * @return the value of field 'cdInscricaoFavorecidoCliente'.
     */
    public long getCdInscricaoFavorecidoCliente()
    {
        return this._cdInscricaoFavorecidoCliente;
    } //-- long getCdInscricaoFavorecidoCliente() 

    /**
     * Returns the value of field 'cdIspbPagtoDestino'.
     * 
     * @return String
     * @return the value of field 'cdIspbPagtoDestino'.
     */
    public java.lang.String getCdIspbPagtoDestino()
    {
        return this._cdIspbPagtoDestino;
    } //-- java.lang.String getCdIspbPagtoDestino() 

    /**
     * Returns the value of field 'cdProdutoServicoOperacao'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoOperacao'.
     */
    public int getCdProdutoServicoOperacao()
    {
        return this._cdProdutoServicoOperacao;
    } //-- int getCdProdutoServicoOperacao() 

    /**
     * Returns the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @return int
     * @return the value of field 'cdProdutoServicoRelacionado'.
     */
    public int getCdProdutoServicoRelacionado()
    {
        return this._cdProdutoServicoRelacionado;
    } //-- int getCdProdutoServicoRelacionado() 

    /**
     * Returns the value of field 'cdTipoConta'.
     * 
     * @return int
     * @return the value of field 'cdTipoConta'.
     */
    public int getCdTipoConta()
    {
        return this._cdTipoConta;
    } //-- int getCdTipoConta() 

    /**
     * Returns the value of field 'contaPagtoDestino'.
     * 
     * @return String
     * @return the value of field 'contaPagtoDestino'.
     */
    public java.lang.String getContaPagtoDestino()
    {
        return this._contaPagtoDestino;
    } //-- java.lang.String getContaPagtoDestino() 

    /**
     * Returns the value of field 'dtCreditoPagamentoFinal'.
     * 
     * @return String
     * @return the value of field 'dtCreditoPagamentoFinal'.
     */
    public java.lang.String getDtCreditoPagamentoFinal()
    {
        return this._dtCreditoPagamentoFinal;
    } //-- java.lang.String getDtCreditoPagamentoFinal() 

    /**
     * Returns the value of field 'dtCreditoPagamentoInicial'.
     * 
     * @return String
     * @return the value of field 'dtCreditoPagamentoInicial'.
     */
    public java.lang.String getDtCreditoPagamentoInicial()
    {
        return this._dtCreditoPagamentoInicial;
    } //-- java.lang.String getDtCreditoPagamentoInicial() 

    /**
     * Returns the value of field 'nrContrato'.
     * 
     * @return long
     * @return the value of field 'nrContrato'.
     */
    public long getNrContrato()
    {
        return this._nrContrato;
    } //-- long getNrContrato() 

    /**
     * Returns the value of field 'nrUnidadeOrganizacional'.
     * 
     * @return int
     * @return the value of field 'nrUnidadeOrganizacional'.
     */
    public int getNrUnidadeOrganizacional()
    {
        return this._nrUnidadeOrganizacional;
    } //-- int getNrUnidadeOrganizacional() 

    /**
     * Returns the value of field 'numeroOcorrencias'.
     * 
     * @return int
     * @return the value of field 'numeroOcorrencias'.
     */
    public int getNumeroOcorrencias()
    {
        return this._numeroOcorrencias;
    } //-- int getNumeroOcorrencias() 

    /**
     * Method hasAgePagoNaoPago
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasAgePagoNaoPago()
    {
        return this._has_agePagoNaoPago;
    } //-- boolean hasAgePagoNaoPago() 

    /**
     * Method hasCdAgencaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencaSalarial()
    {
        return this._has_cdAgencaSalarial;
    } //-- boolean hasCdAgencaSalarial() 

    /**
     * Method hasCdAgencia
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdAgencia()
    {
        return this._has_cdAgencia;
    } //-- boolean hasCdAgencia() 

    /**
     * Method hasCdBanco
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBanco()
    {
        return this._has_cdBanco;
    } //-- boolean hasCdBanco() 

    /**
     * Method hasCdBancoSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBancoSalarial()
    {
        return this._has_cdBancoSalarial;
    } //-- boolean hasCdBancoSalarial() 

    /**
     * Method hasCdBeneficiario
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdBeneficiario()
    {
        return this._has_cdBeneficiario;
    } //-- boolean hasCdBeneficiario() 

    /**
     * Method hasCdConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdConta()
    {
        return this._has_cdConta;
    } //-- boolean hasCdConta() 

    /**
     * Method hasCdContaSalarial
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdContaSalarial()
    {
        return this._has_cdContaSalarial;
    } //-- boolean hasCdContaSalarial() 

    /**
     * Method hasCdEmpresaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEmpresaContrato()
    {
        return this._has_cdEmpresaContrato;
    } //-- boolean hasCdEmpresaContrato() 

    /**
     * Method hasCdEspecieBeneficioINSS
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdEspecieBeneficioINSS()
    {
        return this._has_cdEspecieBeneficioINSS;
    } //-- boolean hasCdEspecieBeneficioINSS() 

    /**
     * Method hasCdFavorecidoClientePagador
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdFavorecidoClientePagador()
    {
        return this._has_cdFavorecidoClientePagador;
    } //-- boolean hasCdFavorecidoClientePagador() 

    /**
     * Method hasCdIdentificacaoInscricaoFavorecido
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdIdentificacaoInscricaoFavorecido()
    {
        return this._has_cdIdentificacaoInscricaoFavorecido;
    } //-- boolean hasCdIdentificacaoInscricaoFavorecido() 

    /**
     * Method hasCdInscricaoFavorecidoCliente
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdInscricaoFavorecidoCliente()
    {
        return this._has_cdInscricaoFavorecidoCliente;
    } //-- boolean hasCdInscricaoFavorecidoCliente() 

    /**
     * Method hasCdProdutoServicoOperacao
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoOperacao()
    {
        return this._has_cdProdutoServicoOperacao;
    } //-- boolean hasCdProdutoServicoOperacao() 

    /**
     * Method hasCdProdutoServicoRelacionado
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdProdutoServicoRelacionado()
    {
        return this._has_cdProdutoServicoRelacionado;
    } //-- boolean hasCdProdutoServicoRelacionado() 

    /**
     * Method hasCdTipoConta
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoConta()
    {
        return this._has_cdTipoConta;
    } //-- boolean hasCdTipoConta() 

    /**
     * Method hasNrContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrContrato()
    {
        return this._has_nrContrato;
    } //-- boolean hasNrContrato() 

    /**
     * Method hasNrUnidadeOrganizacional
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrUnidadeOrganizacional()
    {
        return this._has_nrUnidadeOrganizacional;
    } //-- boolean hasNrUnidadeOrganizacional() 

    /**
     * Method hasNumeroOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNumeroOcorrencias()
    {
        return this._has_numeroOcorrencias;
    } //-- boolean hasNumeroOcorrencias() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'agePagoNaoPago'.
     * 
     * @param agePagoNaoPago the value of field 'agePagoNaoPago'.
     */
    public void setAgePagoNaoPago(int agePagoNaoPago)
    {
        this._agePagoNaoPago = agePagoNaoPago;
        this._has_agePagoNaoPago = true;
    } //-- void setAgePagoNaoPago(int) 

    /**
     * Sets the value of field 'cdAgencaSalarial'.
     * 
     * @param cdAgencaSalarial the value of field 'cdAgencaSalarial'
     */
    public void setCdAgencaSalarial(int cdAgencaSalarial)
    {
        this._cdAgencaSalarial = cdAgencaSalarial;
        this._has_cdAgencaSalarial = true;
    } //-- void setCdAgencaSalarial(int) 

    /**
     * Sets the value of field 'cdAgencia'.
     * 
     * @param cdAgencia the value of field 'cdAgencia'.
     */
    public void setCdAgencia(int cdAgencia)
    {
        this._cdAgencia = cdAgencia;
        this._has_cdAgencia = true;
    } //-- void setCdAgencia(int) 

    /**
     * Sets the value of field 'cdBanco'.
     * 
     * @param cdBanco the value of field 'cdBanco'.
     */
    public void setCdBanco(int cdBanco)
    {
        this._cdBanco = cdBanco;
        this._has_cdBanco = true;
    } //-- void setCdBanco(int) 

    /**
     * Sets the value of field 'cdBancoSalarial'.
     * 
     * @param cdBancoSalarial the value of field 'cdBancoSalarial'.
     */
    public void setCdBancoSalarial(int cdBancoSalarial)
    {
        this._cdBancoSalarial = cdBancoSalarial;
        this._has_cdBancoSalarial = true;
    } //-- void setCdBancoSalarial(int) 

    /**
     * Sets the value of field 'cdBeneficiario'.
     * 
     * @param cdBeneficiario the value of field 'cdBeneficiario'.
     */
    public void setCdBeneficiario(long cdBeneficiario)
    {
        this._cdBeneficiario = cdBeneficiario;
        this._has_cdBeneficiario = true;
    } //-- void setCdBeneficiario(long) 

    /**
     * Sets the value of field 'cdClassificacao'.
     * 
     * @param cdClassificacao the value of field 'cdClassificacao'.
     */
    public void setCdClassificacao(java.lang.String cdClassificacao)
    {
        this._cdClassificacao = cdClassificacao;
    } //-- void setCdClassificacao(java.lang.String) 

    /**
     * Sets the value of field 'cdConta'.
     * 
     * @param cdConta the value of field 'cdConta'.
     */
    public void setCdConta(long cdConta)
    {
        this._cdConta = cdConta;
        this._has_cdConta = true;
    } //-- void setCdConta(long) 

    /**
     * Sets the value of field 'cdContaSalarial'.
     * 
     * @param cdContaSalarial the value of field 'cdContaSalarial'.
     */
    public void setCdContaSalarial(long cdContaSalarial)
    {
        this._cdContaSalarial = cdContaSalarial;
        this._has_cdContaSalarial = true;
    } //-- void setCdContaSalarial(long) 

    /**
     * Sets the value of field 'cdDigitoConta'.
     * 
     * @param cdDigitoConta the value of field 'cdDigitoConta'.
     */
    public void setCdDigitoConta(java.lang.String cdDigitoConta)
    {
        this._cdDigitoConta = cdDigitoConta;
    } //-- void setCdDigitoConta(java.lang.String) 

    /**
     * Sets the value of field 'cdEmpresaContrato'.
     * 
     * @param cdEmpresaContrato the value of field
     * 'cdEmpresaContrato'.
     */
    public void setCdEmpresaContrato(long cdEmpresaContrato)
    {
        this._cdEmpresaContrato = cdEmpresaContrato;
        this._has_cdEmpresaContrato = true;
    } //-- void setCdEmpresaContrato(long) 

    /**
     * Sets the value of field 'cdEspecieBeneficioINSS'.
     * 
     * @param cdEspecieBeneficioINSS the value of field
     * 'cdEspecieBeneficioINSS'.
     */
    public void setCdEspecieBeneficioINSS(int cdEspecieBeneficioINSS)
    {
        this._cdEspecieBeneficioINSS = cdEspecieBeneficioINSS;
        this._has_cdEspecieBeneficioINSS = true;
    } //-- void setCdEspecieBeneficioINSS(int) 

    /**
     * Sets the value of field 'cdFavorecidoClientePagador'.
     * 
     * @param cdFavorecidoClientePagador the value of field
     * 'cdFavorecidoClientePagador'.
     */
    public void setCdFavorecidoClientePagador(long cdFavorecidoClientePagador)
    {
        this._cdFavorecidoClientePagador = cdFavorecidoClientePagador;
        this._has_cdFavorecidoClientePagador = true;
    } //-- void setCdFavorecidoClientePagador(long) 

    /**
     * Sets the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     * 
     * @param cdIdentificacaoInscricaoFavorecido the value of field
     * 'cdIdentificacaoInscricaoFavorecido'.
     */
    public void setCdIdentificacaoInscricaoFavorecido(int cdIdentificacaoInscricaoFavorecido)
    {
        this._cdIdentificacaoInscricaoFavorecido = cdIdentificacaoInscricaoFavorecido;
        this._has_cdIdentificacaoInscricaoFavorecido = true;
    } //-- void setCdIdentificacaoInscricaoFavorecido(int) 

    /**
     * Sets the value of field 'cdInscricaoFavorecidoCliente'.
     * 
     * @param cdInscricaoFavorecidoCliente the value of field
     * 'cdInscricaoFavorecidoCliente'.
     */
    public void setCdInscricaoFavorecidoCliente(long cdInscricaoFavorecidoCliente)
    {
        this._cdInscricaoFavorecidoCliente = cdInscricaoFavorecidoCliente;
        this._has_cdInscricaoFavorecidoCliente = true;
    } //-- void setCdInscricaoFavorecidoCliente(long) 

    /**
     * Sets the value of field 'cdIspbPagtoDestino'.
     * 
     * @param cdIspbPagtoDestino the value of field
     * 'cdIspbPagtoDestino'.
     */
    public void setCdIspbPagtoDestino(java.lang.String cdIspbPagtoDestino)
    {
        this._cdIspbPagtoDestino = cdIspbPagtoDestino;
    } //-- void setCdIspbPagtoDestino(java.lang.String) 

    /**
     * Sets the value of field 'cdProdutoServicoOperacao'.
     * 
     * @param cdProdutoServicoOperacao the value of field
     * 'cdProdutoServicoOperacao'.
     */
    public void setCdProdutoServicoOperacao(int cdProdutoServicoOperacao)
    {
        this._cdProdutoServicoOperacao = cdProdutoServicoOperacao;
        this._has_cdProdutoServicoOperacao = true;
    } //-- void setCdProdutoServicoOperacao(int) 

    /**
     * Sets the value of field 'cdProdutoServicoRelacionado'.
     * 
     * @param cdProdutoServicoRelacionado the value of field
     * 'cdProdutoServicoRelacionado'.
     */
    public void setCdProdutoServicoRelacionado(int cdProdutoServicoRelacionado)
    {
        this._cdProdutoServicoRelacionado = cdProdutoServicoRelacionado;
        this._has_cdProdutoServicoRelacionado = true;
    } //-- void setCdProdutoServicoRelacionado(int) 

    /**
     * Sets the value of field 'cdTipoConta'.
     * 
     * @param cdTipoConta the value of field 'cdTipoConta'.
     */
    public void setCdTipoConta(int cdTipoConta)
    {
        this._cdTipoConta = cdTipoConta;
        this._has_cdTipoConta = true;
    } //-- void setCdTipoConta(int) 

    /**
     * Sets the value of field 'contaPagtoDestino'.
     * 
     * @param contaPagtoDestino the value of field
     * 'contaPagtoDestino'.
     */
    public void setContaPagtoDestino(java.lang.String contaPagtoDestino)
    {
        this._contaPagtoDestino = contaPagtoDestino;
    } //-- void setContaPagtoDestino(java.lang.String) 

    /**
     * Sets the value of field 'dtCreditoPagamentoFinal'.
     * 
     * @param dtCreditoPagamentoFinal the value of field
     * 'dtCreditoPagamentoFinal'.
     */
    public void setDtCreditoPagamentoFinal(java.lang.String dtCreditoPagamentoFinal)
    {
        this._dtCreditoPagamentoFinal = dtCreditoPagamentoFinal;
    } //-- void setDtCreditoPagamentoFinal(java.lang.String) 

    /**
     * Sets the value of field 'dtCreditoPagamentoInicial'.
     * 
     * @param dtCreditoPagamentoInicial the value of field
     * 'dtCreditoPagamentoInicial'.
     */
    public void setDtCreditoPagamentoInicial(java.lang.String dtCreditoPagamentoInicial)
    {
        this._dtCreditoPagamentoInicial = dtCreditoPagamentoInicial;
    } //-- void setDtCreditoPagamentoInicial(java.lang.String) 

    /**
     * Sets the value of field 'nrContrato'.
     * 
     * @param nrContrato the value of field 'nrContrato'.
     */
    public void setNrContrato(long nrContrato)
    {
        this._nrContrato = nrContrato;
        this._has_nrContrato = true;
    } //-- void setNrContrato(long) 

    /**
     * Sets the value of field 'nrUnidadeOrganizacional'.
     * 
     * @param nrUnidadeOrganizacional the value of field
     * 'nrUnidadeOrganizacional'.
     */
    public void setNrUnidadeOrganizacional(int nrUnidadeOrganizacional)
    {
        this._nrUnidadeOrganizacional = nrUnidadeOrganizacional;
        this._has_nrUnidadeOrganizacional = true;
    } //-- void setNrUnidadeOrganizacional(int) 

    /**
     * Sets the value of field 'numeroOcorrencias'.
     * 
     * @param numeroOcorrencias the value of field
     * 'numeroOcorrencias'.
     */
    public void setNumeroOcorrencias(int numeroOcorrencias)
    {
        this._numeroOcorrencias = numeroOcorrencias;
        this._has_numeroOcorrencias = true;
    } //-- void setNumeroOcorrencias(int) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarPagamentoFavorecidoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.request.ConsultarPagamentoFavorecidoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.request.ConsultarPagamentoFavorecidoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.request.ConsultarPagamentoFavorecidoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultarpagamentofavorecido.request.ConsultarPagamentoFavorecidoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
