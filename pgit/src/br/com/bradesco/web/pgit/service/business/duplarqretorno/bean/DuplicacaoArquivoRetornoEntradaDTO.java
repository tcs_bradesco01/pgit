/*
 * Nome: br.com.bradesco.web.pgit.service.business.duplarqretorno.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.duplarqretorno.bean;

/**
 * Nome: DuplicacaoArquivoRetornoEntradaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DuplicacaoArquivoRetornoEntradaDTO {

	/** Atributo cdPessoaJuridContrato. */
	private Long cdPessoaJuridContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSeqContratoNegocio. */
	private Long nrSeqContratoNegocio;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdTipoArquivoRetorno. */
	private Integer cdTipoArquivoRetorno;
	
	/** Atributo cdClub. */
	private Long cdClub;
	
	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public Long getCdClub() {
		return cdClub;
	}
	
	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}
	
	/**
	 * Get: cdPessoaJuridContrato.
	 *
	 * @return cdPessoaJuridContrato
	 */
	public Long getCdPessoaJuridContrato() {
		return cdPessoaJuridContrato;
	}
	
	/**
	 * Set: cdPessoaJuridContrato.
	 *
	 * @param cdPessoaJuridContrato the cd pessoa jurid contrato
	 */
	public void setCdPessoaJuridContrato(Long cdPessoaJuridContrato) {
		this.cdPessoaJuridContrato = cdPessoaJuridContrato;
	}
	
	/**
	 * Get: cdTipoArquivoRetorno.
	 *
	 * @return cdTipoArquivoRetorno
	 */
	public Integer getCdTipoArquivoRetorno() {
		return cdTipoArquivoRetorno;
	}
	
	/**
	 * Set: cdTipoArquivoRetorno.
	 *
	 * @param cdTipoArquivoRetorno the cd tipo arquivo retorno
	 */
	public void setCdTipoArquivoRetorno(Integer cdTipoArquivoRetorno) {
		this.cdTipoArquivoRetorno = cdTipoArquivoRetorno;
	}
	
	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}
	
	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}
	
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}
	
	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}
	
	/**
	 * Get: nrSeqContratoNegocio.
	 *
	 * @return nrSeqContratoNegocio
	 */
	public Long getNrSeqContratoNegocio() {
		return nrSeqContratoNegocio;
	}
	
	/**
	 * Set: nrSeqContratoNegocio.
	 *
	 * @param nrSeqContratoNegocio the nr seq contrato negocio
	 */
	public void setNrSeqContratoNegocio(Long nrSeqContratoNegocio) {
		this.nrSeqContratoNegocio = nrSeqContratoNegocio;
	}
	
	
	
}
