/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.0</a>, using an XML
 * Schema.
 * $Id$
 */

package br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.request;

  //---------------------------------/
 //- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class ConsultarArquivoRecuperacaoRequest.
 * 
 * @version $Revision$ $Date$
 */
public class ConsultarArquivoRecuperacaoRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _nrOcorrencias
     */
    private int _nrOcorrencias = 0;

    /**
     * keeps track of state for field: _nrOcorrencias
     */
    private boolean _has_nrOcorrencias;

    /**
     * Field _cdPessoa
     */
    private long _cdPessoa = 0;

    /**
     * keeps track of state for field: _cdPessoa
     */
    private boolean _has_cdPessoa;

    /**
     * Field _cdPessoaJuridicaContrato
     */
    private long _cdPessoaJuridicaContrato = 0;

    /**
     * keeps track of state for field: _cdPessoaJuridicaContrato
     */
    private boolean _has_cdPessoaJuridicaContrato;

    /**
     * Field _cdTipoContratoNegocio
     */
    private int _cdTipoContratoNegocio = 0;

    /**
     * keeps track of state for field: _cdTipoContratoNegocio
     */
    private boolean _has_cdTipoContratoNegocio;

    /**
     * Field _nrSequenciaContratoNegocio
     */
    private long _nrSequenciaContratoNegocio = 0;

    /**
     * keeps track of state for field: _nrSequenciaContratoNegocio
     */
    private boolean _has_nrSequenciaContratoNegocio;

    /**
     * Field _dtInicioInclusaoRemessa
     */
    private java.lang.String _dtInicioInclusaoRemessa;

    /**
     * Field _dtFinalInclusaoRemessa
     */
    private java.lang.String _dtFinalInclusaoRemessa;

    /**
     * Field _cdTipoLayoutArquivo
     */
    private int _cdTipoLayoutArquivo = 0;

    /**
     * keeps track of state for field: _cdTipoLayoutArquivo
     */
    private boolean _has_cdTipoLayoutArquivo;

    /**
     * Field _nrArquivoRemessa
     */
    private long _nrArquivoRemessa = 0;

    /**
     * keeps track of state for field: _nrArquivoRemessa
     */
    private boolean _has_nrArquivoRemessa;

    /**
     * Field _cdSituacaoProcessamentoRemessa
     */
    private int _cdSituacaoProcessamentoRemessa = 0;

    /**
     * keeps track of state for field:
     * _cdSituacaoProcessamentoRemessa
     */
    private boolean _has_cdSituacaoProcessamentoRemessa;

    /**
     * Field _cdCondicaoProcessamentoRemessa
     */
    private int _cdCondicaoProcessamentoRemessa = 0;

    /**
     * keeps track of state for field:
     * _cdCondicaoProcessamentoRemessa
     */
    private boolean _has_cdCondicaoProcessamentoRemessa;


      //----------------/
     //- Constructors -/
    //----------------/

    public ConsultarArquivoRecuperacaoRequest() 
     {
        super();
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.request.ConsultarArquivoRecuperacaoRequest()


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Method deleteCdCondicaoProcessamentoRemessa
     * 
     */
    public void deleteCdCondicaoProcessamentoRemessa()
    {
        this._has_cdCondicaoProcessamentoRemessa= false;
    } //-- void deleteCdCondicaoProcessamentoRemessa() 

    /**
     * Method deleteCdPessoa
     * 
     */
    public void deleteCdPessoa()
    {
        this._has_cdPessoa= false;
    } //-- void deleteCdPessoa() 

    /**
     * Method deleteCdPessoaJuridicaContrato
     * 
     */
    public void deleteCdPessoaJuridicaContrato()
    {
        this._has_cdPessoaJuridicaContrato= false;
    } //-- void deleteCdPessoaJuridicaContrato() 

    /**
     * Method deleteCdSituacaoProcessamentoRemessa
     * 
     */
    public void deleteCdSituacaoProcessamentoRemessa()
    {
        this._has_cdSituacaoProcessamentoRemessa= false;
    } //-- void deleteCdSituacaoProcessamentoRemessa() 

    /**
     * Method deleteCdTipoContratoNegocio
     * 
     */
    public void deleteCdTipoContratoNegocio()
    {
        this._has_cdTipoContratoNegocio= false;
    } //-- void deleteCdTipoContratoNegocio() 

    /**
     * Method deleteCdTipoLayoutArquivo
     * 
     */
    public void deleteCdTipoLayoutArquivo()
    {
        this._has_cdTipoLayoutArquivo= false;
    } //-- void deleteCdTipoLayoutArquivo() 

    /**
     * Method deleteNrArquivoRemessa
     * 
     */
    public void deleteNrArquivoRemessa()
    {
        this._has_nrArquivoRemessa= false;
    } //-- void deleteNrArquivoRemessa() 

    /**
     * Method deleteNrOcorrencias
     * 
     */
    public void deleteNrOcorrencias()
    {
        this._has_nrOcorrencias= false;
    } //-- void deleteNrOcorrencias() 

    /**
     * Method deleteNrSequenciaContratoNegocio
     * 
     */
    public void deleteNrSequenciaContratoNegocio()
    {
        this._has_nrSequenciaContratoNegocio= false;
    } //-- void deleteNrSequenciaContratoNegocio() 

    /**
     * Returns the value of field 'cdCondicaoProcessamentoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdCondicaoProcessamentoRemessa'.
     */
    public int getCdCondicaoProcessamentoRemessa()
    {
        return this._cdCondicaoProcessamentoRemessa;
    } //-- int getCdCondicaoProcessamentoRemessa() 

    /**
     * Returns the value of field 'cdPessoa'.
     * 
     * @return long
     * @return the value of field 'cdPessoa'.
     */
    public long getCdPessoa()
    {
        return this._cdPessoa;
    } //-- long getCdPessoa() 

    /**
     * Returns the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @return long
     * @return the value of field 'cdPessoaJuridicaContrato'.
     */
    public long getCdPessoaJuridicaContrato()
    {
        return this._cdPessoaJuridicaContrato;
    } //-- long getCdPessoaJuridicaContrato() 

    /**
     * Returns the value of field 'cdSituacaoProcessamentoRemessa'.
     * 
     * @return int
     * @return the value of field 'cdSituacaoProcessamentoRemessa'.
     */
    public int getCdSituacaoProcessamentoRemessa()
    {
        return this._cdSituacaoProcessamentoRemessa;
    } //-- int getCdSituacaoProcessamentoRemessa() 

    /**
     * Returns the value of field 'cdTipoContratoNegocio'.
     * 
     * @return int
     * @return the value of field 'cdTipoContratoNegocio'.
     */
    public int getCdTipoContratoNegocio()
    {
        return this._cdTipoContratoNegocio;
    } //-- int getCdTipoContratoNegocio() 

    /**
     * Returns the value of field 'cdTipoLayoutArquivo'.
     * 
     * @return int
     * @return the value of field 'cdTipoLayoutArquivo'.
     */
    public int getCdTipoLayoutArquivo()
    {
        return this._cdTipoLayoutArquivo;
    } //-- int getCdTipoLayoutArquivo() 

    /**
     * Returns the value of field 'dtFinalInclusaoRemessa'.
     * 
     * @return String
     * @return the value of field 'dtFinalInclusaoRemessa'.
     */
    public java.lang.String getDtFinalInclusaoRemessa()
    {
        return this._dtFinalInclusaoRemessa;
    } //-- java.lang.String getDtFinalInclusaoRemessa() 

    /**
     * Returns the value of field 'dtInicioInclusaoRemessa'.
     * 
     * @return String
     * @return the value of field 'dtInicioInclusaoRemessa'.
     */
    public java.lang.String getDtInicioInclusaoRemessa()
    {
        return this._dtInicioInclusaoRemessa;
    } //-- java.lang.String getDtInicioInclusaoRemessa() 

    /**
     * Returns the value of field 'nrArquivoRemessa'.
     * 
     * @return long
     * @return the value of field 'nrArquivoRemessa'.
     */
    public long getNrArquivoRemessa()
    {
        return this._nrArquivoRemessa;
    } //-- long getNrArquivoRemessa() 

    /**
     * Returns the value of field 'nrOcorrencias'.
     * 
     * @return int
     * @return the value of field 'nrOcorrencias'.
     */
    public int getNrOcorrencias()
    {
        return this._nrOcorrencias;
    } //-- int getNrOcorrencias() 

    /**
     * Returns the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @return long
     * @return the value of field 'nrSequenciaContratoNegocio'.
     */
    public long getNrSequenciaContratoNegocio()
    {
        return this._nrSequenciaContratoNegocio;
    } //-- long getNrSequenciaContratoNegocio() 

    /**
     * Method hasCdCondicaoProcessamentoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdCondicaoProcessamentoRemessa()
    {
        return this._has_cdCondicaoProcessamentoRemessa;
    } //-- boolean hasCdCondicaoProcessamentoRemessa() 

    /**
     * Method hasCdPessoa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoa()
    {
        return this._has_cdPessoa;
    } //-- boolean hasCdPessoa() 

    /**
     * Method hasCdPessoaJuridicaContrato
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdPessoaJuridicaContrato()
    {
        return this._has_cdPessoaJuridicaContrato;
    } //-- boolean hasCdPessoaJuridicaContrato() 

    /**
     * Method hasCdSituacaoProcessamentoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdSituacaoProcessamentoRemessa()
    {
        return this._has_cdSituacaoProcessamentoRemessa;
    } //-- boolean hasCdSituacaoProcessamentoRemessa() 

    /**
     * Method hasCdTipoContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoContratoNegocio()
    {
        return this._has_cdTipoContratoNegocio;
    } //-- boolean hasCdTipoContratoNegocio() 

    /**
     * Method hasCdTipoLayoutArquivo
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasCdTipoLayoutArquivo()
    {
        return this._has_cdTipoLayoutArquivo;
    } //-- boolean hasCdTipoLayoutArquivo() 

    /**
     * Method hasNrArquivoRemessa
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrArquivoRemessa()
    {
        return this._has_nrArquivoRemessa;
    } //-- boolean hasNrArquivoRemessa() 

    /**
     * Method hasNrOcorrencias
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrOcorrencias()
    {
        return this._has_nrOcorrencias;
    } //-- boolean hasNrOcorrencias() 

    /**
     * Method hasNrSequenciaContratoNegocio
     * 
     * 
     * 
     * @return boolean
     */
    public boolean hasNrSequenciaContratoNegocio()
    {
        return this._has_nrSequenciaContratoNegocio;
    } //-- boolean hasNrSequenciaContratoNegocio() 

    /**
     * Method isValid
     * 
     * 
     * 
     * @return boolean
     */
    public boolean isValid()
    {
        try {
            validate();
        }
        catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    } //-- boolean isValid() 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param out
     */
    public void marshal(java.io.Writer out)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, out);
    } //-- void marshal(java.io.Writer) 

    /**
     * Method marshal
     * 
     * 
     * 
     * @param handler
     */
    public void marshal(org.xml.sax.ContentHandler handler)
        throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        
        Marshaller.marshal(this, handler);
    } //-- void marshal(org.xml.sax.ContentHandler) 

    /**
     * Sets the value of field 'cdCondicaoProcessamentoRemessa'.
     * 
     * @param cdCondicaoProcessamentoRemessa the value of field
     * 'cdCondicaoProcessamentoRemessa'.
     */
    public void setCdCondicaoProcessamentoRemessa(int cdCondicaoProcessamentoRemessa)
    {
        this._cdCondicaoProcessamentoRemessa = cdCondicaoProcessamentoRemessa;
        this._has_cdCondicaoProcessamentoRemessa = true;
    } //-- void setCdCondicaoProcessamentoRemessa(int) 

    /**
     * Sets the value of field 'cdPessoa'.
     * 
     * @param cdPessoa the value of field 'cdPessoa'.
     */
    public void setCdPessoa(long cdPessoa)
    {
        this._cdPessoa = cdPessoa;
        this._has_cdPessoa = true;
    } //-- void setCdPessoa(long) 

    /**
     * Sets the value of field 'cdPessoaJuridicaContrato'.
     * 
     * @param cdPessoaJuridicaContrato the value of field
     * 'cdPessoaJuridicaContrato'.
     */
    public void setCdPessoaJuridicaContrato(long cdPessoaJuridicaContrato)
    {
        this._cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
        this._has_cdPessoaJuridicaContrato = true;
    } //-- void setCdPessoaJuridicaContrato(long) 

    /**
     * Sets the value of field 'cdSituacaoProcessamentoRemessa'.
     * 
     * @param cdSituacaoProcessamentoRemessa the value of field
     * 'cdSituacaoProcessamentoRemessa'.
     */
    public void setCdSituacaoProcessamentoRemessa(int cdSituacaoProcessamentoRemessa)
    {
        this._cdSituacaoProcessamentoRemessa = cdSituacaoProcessamentoRemessa;
        this._has_cdSituacaoProcessamentoRemessa = true;
    } //-- void setCdSituacaoProcessamentoRemessa(int) 

    /**
     * Sets the value of field 'cdTipoContratoNegocio'.
     * 
     * @param cdTipoContratoNegocio the value of field
     * 'cdTipoContratoNegocio'.
     */
    public void setCdTipoContratoNegocio(int cdTipoContratoNegocio)
    {
        this._cdTipoContratoNegocio = cdTipoContratoNegocio;
        this._has_cdTipoContratoNegocio = true;
    } //-- void setCdTipoContratoNegocio(int) 

    /**
     * Sets the value of field 'cdTipoLayoutArquivo'.
     * 
     * @param cdTipoLayoutArquivo the value of field
     * 'cdTipoLayoutArquivo'.
     */
    public void setCdTipoLayoutArquivo(int cdTipoLayoutArquivo)
    {
        this._cdTipoLayoutArquivo = cdTipoLayoutArquivo;
        this._has_cdTipoLayoutArquivo = true;
    } //-- void setCdTipoLayoutArquivo(int) 

    /**
     * Sets the value of field 'dtFinalInclusaoRemessa'.
     * 
     * @param dtFinalInclusaoRemessa the value of field
     * 'dtFinalInclusaoRemessa'.
     */
    public void setDtFinalInclusaoRemessa(java.lang.String dtFinalInclusaoRemessa)
    {
        this._dtFinalInclusaoRemessa = dtFinalInclusaoRemessa;
    } //-- void setDtFinalInclusaoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'dtInicioInclusaoRemessa'.
     * 
     * @param dtInicioInclusaoRemessa the value of field
     * 'dtInicioInclusaoRemessa'.
     */
    public void setDtInicioInclusaoRemessa(java.lang.String dtInicioInclusaoRemessa)
    {
        this._dtInicioInclusaoRemessa = dtInicioInclusaoRemessa;
    } //-- void setDtInicioInclusaoRemessa(java.lang.String) 

    /**
     * Sets the value of field 'nrArquivoRemessa'.
     * 
     * @param nrArquivoRemessa the value of field 'nrArquivoRemessa'
     */
    public void setNrArquivoRemessa(long nrArquivoRemessa)
    {
        this._nrArquivoRemessa = nrArquivoRemessa;
        this._has_nrArquivoRemessa = true;
    } //-- void setNrArquivoRemessa(long) 

    /**
     * Sets the value of field 'nrOcorrencias'.
     * 
     * @param nrOcorrencias the value of field 'nrOcorrencias'.
     */
    public void setNrOcorrencias(int nrOcorrencias)
    {
        this._nrOcorrencias = nrOcorrencias;
        this._has_nrOcorrencias = true;
    } //-- void setNrOcorrencias(int) 

    /**
     * Sets the value of field 'nrSequenciaContratoNegocio'.
     * 
     * @param nrSequenciaContratoNegocio the value of field
     * 'nrSequenciaContratoNegocio'.
     */
    public void setNrSequenciaContratoNegocio(long nrSequenciaContratoNegocio)
    {
        this._nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
        this._has_nrSequenciaContratoNegocio = true;
    } //-- void setNrSequenciaContratoNegocio(long) 

    /**
     * Method unmarshal
     * 
     * 
     * 
     * @param reader
     * @return ConsultarArquivoRecuperacaoRequest
     */
    public static br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.request.ConsultarArquivoRecuperacaoRequest unmarshal(java.io.Reader reader)
        throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException
    {
        return (br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.request.ConsultarArquivoRecuperacaoRequest) Unmarshaller.unmarshal(br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.request.ConsultarArquivoRecuperacaoRequest.class, reader);
    } //-- br.com.bradesco.web.pgit.service.data.pdc.consultararquivorecuperacao.request.ConsultarArquivoRecuperacaoRequest unmarshal(java.io.Reader) 

    /**
     * Method validate
     * 
     */
    public void validate()
        throws org.exolab.castor.xml.ValidationException
    {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    } //-- void validate() 

}
