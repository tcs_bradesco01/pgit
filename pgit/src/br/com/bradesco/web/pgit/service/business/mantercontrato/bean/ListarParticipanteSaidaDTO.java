/*
 * Nome: br.com.bradesco.web.pgit.service.business.mantercontrato.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.service.business.mantercontrato.bean;

import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;


/**
 * Nome: ListarParticipanteSaidaDTO
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ListarParticipanteSaidaDTO {


	/** Atributo codMensagem. */
	private String codMensagem;

	/** Atributo mensagem. */
	private String mensagem;

	/** Atributo numeroLinhas. */
	private Integer numeroLinhas;
	
	/** Atributo cdClub. */
	private Long cdClub;

	/** Atributo cdCorpoCpfCnpj. */
	private Long cdCorpoCpfCnpj;

	/** Atributo cdCnpjContrato. */
	private Integer cdCnpjContrato;
	
	/** Atributo cdCpfCnpjDigito. */
	private Integer cdCpfCnpjDigito;
	
	/** Atributo cdRazaoSocial. */
	private String cdRazaoSocial;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdPerfilTrocaArquivo. */
	private Long cdPerfilTrocaArquivo;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo dsTipoParticipante. */
	private String dsTipoParticipante;
	
	/** Atributo dsLayout. */
	private String dsLayout;

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: numeroLinhas.
	 *
	 * @return numeroLinhas
	 */
	public Integer getNumeroLinhas() {
		return numeroLinhas;
	}

	/**
	 * Set: numeroLinhas.
	 *
	 * @param numeroLinhas the numero linhas
	 */
	public void setNumeroLinhas(Integer numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}

	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public Long getCdClub() {
		return cdClub;
	}

	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

	/**
	 * Get: cdCorpoCpfCnpj.
	 *
	 * @return cdCorpoCpfCnpj
	 */
	public Long getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}

	/**
	 * Set: cdCorpoCpfCnpj.
	 *
	 * @param cdCorpoCpfCnpj the cd corpo cpf cnpj
	 */
	public void setCdCorpoCpfCnpj(Long cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}

	/**
	 * Get: cdCnpjContrato.
	 *
	 * @return cdCnpjContrato
	 */
	public Integer getCdCnpjContrato() {
		return cdCnpjContrato;
	}

	/**
	 * Set: cdCnpjContrato.
	 *
	 * @param cdCnpjContrato the cd cnpj contrato
	 */
	public void setCdCnpjContrato(Integer cdCnpjContrato) {
		this.cdCnpjContrato = cdCnpjContrato;
	}

	/**
	 * Get: cdCpfCnpjDigito.
	 *
	 * @return cdCpfCnpjDigito
	 */
	public Integer getCdCpfCnpjDigito() {
		return cdCpfCnpjDigito;
	}

	/**
	 * Set: cdCpfCnpjDigito.
	 *
	 * @param cdCpfCnpjDigito the cd cpf cnpj digito
	 */
	public void setCdCpfCnpjDigito(Integer cdCpfCnpjDigito) {
		this.cdCpfCnpjDigito = cdCpfCnpjDigito;
	}
	
	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return CpfCnpjUtils.formatarCpfCnpj(cdCorpoCpfCnpj, cdCnpjContrato, cdCpfCnpjDigito);
	}

	/**
	 * Get: cdRazaoSocial.
	 *
	 * @return cdRazaoSocial
	 */
	public String getCdRazaoSocial() {
		return cdRazaoSocial;
	}

	/**
	 * Set: cdRazaoSocial.
	 *
	 * @param cdRazaoSocial the cd razao social
	 */
	public void setCdRazaoSocial(String cdRazaoSocial) {
		this.cdRazaoSocial = cdRazaoSocial;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdPerfilTrocaArquivo.
	 *
	 * @return cdPerfilTrocaArquivo
	 */
	public Long getCdPerfilTrocaArquivo() {
		return cdPerfilTrocaArquivo;
	}

	/**
	 * Set: cdPerfilTrocaArquivo.
	 *
	 * @param cdPerfilTrocaArquivo the cd perfil troca arquivo
	 */
	public void setCdPerfilTrocaArquivo(Long cdPerfilTrocaArquivo) {
		this.cdPerfilTrocaArquivo = cdPerfilTrocaArquivo;
	}

	/**
	 * Get: cdTipoParticipacaoPessoa.
	 *
	 * @return cdTipoParticipacaoPessoa
	 */
	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}

	/**
	 * Set: cdTipoParticipacaoPessoa.
	 *
	 * @param cdTipoParticipacaoPessoa the cd tipo participacao pessoa
	 */
	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	/**
	 * Get: dsTipoParticipante.
	 *
	 * @return dsTipoParticipante
	 */
	public String getDsTipoParticipante() {
		return dsTipoParticipante;
	}

	/**
	 * Set: dsTipoParticipante.
	 *
	 * @param dsTipoParticipante the ds tipo participante
	 */
	public void setDsTipoParticipante(String dsTipoParticipante) {
		this.dsTipoParticipante = dsTipoParticipante;
	}

	/**
	 * Get: dsLayout.
	 *
	 * @return dsLayout
	 */
	public String getDsLayout() {
		return dsLayout;
	}

	/**
	 * Set: dsLayout.
	 *
	 * @param dsLayout the ds layout
	 */
	public void setDsLayout(String dsLayout) {
		this.dsLayout = dsLayout;
	}
	

}
