/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

import java.math.BigDecimal;
import java.text.ParseException;

import br.com.bradesco.web.pgit.exception.PgitException;

/**
 * Nome: NumberPdcUtils
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public final class NumberPdcUtils {

	/** Atributo VALOR_TO_PDC_PATTERN. */
	private static final String VALOR_TO_PDC_PATTERN = "000000000000000.00";

	/** Atributo INDICE_TO_PDC_PATTERN. */
	private static final String INDICE_TO_PDC_PATTERN = "000000000.00000000";

	/** Atributo INTEIRO_TO_PDC_PATTERN. */
	public static final String INTEIRO_TO_PDC_PATTERN = "00000000000000000";

	/** Atributo INTEIRO_FROM_PDC_PATTERN. */
	public static final String INTEIRO_FROM_PDC_PATTERN = "0";

	/** Atributo DIVISOR_VALOR. */
	private static final BigDecimal DIVISOR_VALOR = new BigDecimal(100);

	/** Atributo DIVISOR_INDICE. */
	private static final BigDecimal DIVISOR_INDICE = new BigDecimal(100000000);

	/** Atributo DIVISOR_INTEIRO. */
	private static final BigDecimal DIVISOR_INTEIRO = BigDecimal.ONE;

	/**
	 * Number pdc utils.
	 */
	private NumberPdcUtils() {
		super();
	}

	/**
	 * Formata valor to pdc.
	 *
	 * @param valor the valor
	 * @return the string
	 */
	public static String formataValorToPdc(BigDecimal valor) {
		return formataToPdc(valor, VALOR_TO_PDC_PATTERN);
	}

	/**
	 * Formata indice to pdc.
	 *
	 * @param valor the valor
	 * @return the string
	 */
	public static String formataIndiceToPdc(BigDecimal valor) {
		return formataToPdc(valor, INDICE_TO_PDC_PATTERN);
	}

	/**
	 * Formata inteiro to pdc.
	 *
	 * @param valor the valor
	 * @return the string
	 */
	public static String formataInteiroToPdc(Number valor) {
		return formataToPdc(valor, INTEIRO_TO_PDC_PATTERN);
	}

	/**
	 * Formata to pdc.
	 *
	 * @param valor the valor
	 * @param formatador the formatador
	 * @return the string
	 */
	private static String formataToPdc(Number valor, String formatador) {
		Number valorAux = valor;
		if (valorAux == null) {
			valorAux = BigDecimal.ZERO;
		}

		return NumberUtils.format(valorAux, formatador).replaceAll("\\,", "");
	}

	/**
	 * Formata valor from pdc.
	 *
	 * @param valor the valor
	 * @return the string
	 */
	public static String formataValorFromPdc(String valor) {
		return formataFromPdc(valor, DIVISOR_VALOR, NumberUtils.DEFAULT_DECIMAL_PATTERN);
	}

	/**
	 * Formata indice from pdc.
	 *
	 * @param valor the valor
	 * @return the string
	 */
	public static String formataIndiceFromPdc(String valor) {
		return formataFromPdc(valor, DIVISOR_INDICE, NumberUtils.INDICE_PATTERN);
	}

	/**
	 * Formata inteiro from pdc.
	 *
	 * @param valor the valor
	 * @return the string
	 */
	public static String formataInteiroFromPdc(String valor) {
		return formataFromPdc(valor, DIVISOR_INTEIRO, INTEIRO_FROM_PDC_PATTERN);
	}

	/**
	 * Formata from pdc.
	 *
	 * @param valor the valor
	 * @param divisor the divisor
	 * @param formatador the formatador
	 * @return the string
	 */
	private static String formataFromPdc(String valor, BigDecimal divisor, String formatador) {
		BigDecimal valorNum = null;
		try {
			valorNum = NumberUtils.convert(valor, formatador);
		} catch (ParseException e) {
			throw new PgitException("Erro parseando valor num�rico!", e, "");
		}

		if (valorNum == null) {
			return "";
		}

		return NumberUtils.format(valorNum.divide(divisor), formatador);
	}

}
