/*
 * Nome: br.com.bradesco.web.pgit.utils.constants
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils.constants;

/**
 * Nome: ErrorMessageConstants
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface ErrorMessageConstants {

	/** Atributo PGIT0003. */
	String PGIT0003 = "PGIT0003";

	/** Atributo PGIT0009. */
	String PGIT0009 = "PGIT0009";
}
