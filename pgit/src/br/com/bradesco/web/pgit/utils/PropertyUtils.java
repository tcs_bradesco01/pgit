/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

import java.lang.reflect.InvocationTargetException;

import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;

/**
 * Nome: PropertyUtils
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public final class PropertyUtils {

	/**
	 * Property utils.
	 */
	private PropertyUtils() {
		super();
	}

	/**
	 * Copy properties.
	 *
	 * @param dest the dest
	 * @param orig the orig
	 */
	public static void copyProperties(Object dest, Object orig) {
		if (dest == null || orig == null) {
			return;
		}

		try {
			org.apache.commons.beanutils.PropertyUtils.copyProperties(dest, orig);
		} catch (IllegalAccessException e) {
			BradescoCommonServiceFactory.getLogManager().error(PropertyUtils.class, e.getMessage(), e);
		} catch (InvocationTargetException e) {
			BradescoCommonServiceFactory.getLogManager().error(PropertyUtils.class, e.getMessage(), e);
		} catch (NoSuchMethodException e) {
			BradescoCommonServiceFactory.getLogManager().error(PropertyUtils.class, e.getMessage(), e);
		}
	}
}
