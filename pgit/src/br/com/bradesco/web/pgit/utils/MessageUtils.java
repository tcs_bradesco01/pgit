/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

import br.com.bradesco.web.aq.application.error.BradescoBaseException;

/**
 * Classe utilit�ria para tratamento de mensagens.
 */
public class MessageUtils {
	/**
	 * Construtor
	 */
	private MessageUtils() {
		super();
	}

	/**
	 * Devolve o c�digo da mensagem.
	 * @param exception exce��o
	 * @return c�digo da mensagem
	 */
	public static String getMessageCode(BradescoBaseException exception) {
		if (exception != null && exception.getCode() != null) {
			int lastIndex = exception.getCode().lastIndexOf(".");
			if (lastIndex >= 0 && (lastIndex + 1) < exception.getCode().length()) {
				return exception.getCode().substring(lastIndex + 1);
			}
		}
		return "";
	}
}