/*
 * Nome: br.com.bradesco.web.pgit.utils.constants
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils.constants;

/**
 * Nome: DataConstants
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface DataConstants {

	/** Atributo DATA_PDC. */
	String DATA_PDC = "yyyy-MM-dd-HH.mm.ss.SSSSSS";
	
	/** Atributo DATA_PADRA. */
	String DATA_PADRA = "dd/MM/yyyy HH:mm:ss";
	
}
