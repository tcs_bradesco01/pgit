/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

/**
 * Nome: BancoAgenciaContaDigitoUtils
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public final class BancoAgenciaContaDigitoUtils {

	/**
	 * Banco agencia conta digito utils.
	 */
	private BancoAgenciaContaDigitoUtils() {
		super();
	}

	/**
	 * Format banco agencia conta digito.
	 *
	 * @param banco the banco
	 * @param agencia the agencia
	 * @param numeroConta the numero conta
	 * @param digitoConta the digito conta
	 * @return the string
	 */
	public static String formatBancoAgenciaContaDigito(Integer banco, Integer agencia, Long numeroConta, Integer digitoConta) {
		StringBuilder bancoAgenciaConta = new StringBuilder();
		bancoAgenciaConta.append(NumberUtils.format(banco, "000"));
		bancoAgenciaConta.append("/");
		bancoAgenciaConta.append(NumberUtils.format(agencia, "000"));
		bancoAgenciaConta.append("/");
		bancoAgenciaConta.append(NumberUtils.format(numeroConta, "000000"));
		bancoAgenciaConta.append("-");
		bancoAgenciaConta.append(NumberUtils.format(numeroConta, "00"));
		return bancoAgenciaConta.toString();
	}
}
