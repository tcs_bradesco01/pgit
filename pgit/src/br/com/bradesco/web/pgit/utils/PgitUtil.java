/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;

/**
 * @author ffranco
 * 
 * Para alterar o gabarito para este coment�rio do tipo gerado v� para Janela&gt;Prefer�ncias&gt;Java&gt;Gera��o de
 * C�digos&gt;C�digo e Coment�rios
 */
public final class PgitUtil {

	// Digito conta utilizado na fase 3 conforme e-mail da Kelli Cristina Caldato - quinta-feira, 14 de abril de 2011
    // 11:09
	/** Atributo DIGITO_CONTA. */
	public static final String DIGITO_CONTA = "00";

	/** Atributo defaultInputNumberGroupSeparator. */
	private static String defaultInputNumberGroupSeparator = ".";

	/**
	 * Pgit util.
	 */
	private PgitUtil() {
		super();
	}
	
    /**
     * Nome: isNullBigDecimal.
     *
     * @param value the value
     * @return the big decimal
     * @see
     */  
     public static BigDecimal isNullBigDecimal(BigDecimal value) {
           return value != null ? value : BigDecimal.ZERO;
     }
	

	/**
	 * Formatar campo de valor monetario.
	 *
	 * @param valorini the valorini
	 * @return the string
	 */
	public static String formatarCampoDeValorMonetario(Double valorini) {
		return formatNumber(valorini, 2, ",", true);
	}
	
    /**
     * Nome: isStringEmpty.
     *
     * @param str the str
     * @return true, if is string empty
     * @see
     */  
     public static boolean isStringEmpty(String str) {
           return str == null || str.trim().length() == 0;
     }

     /**
      * Nome: isBigDecimalMaior.
      *
      * @param d1 the d1
      * @param d2 the d2
      * @return true, if is big decimal maior
      * @see
      */  
      public static boolean isBigDecimalMaior(BigDecimal d1, BigDecimal d2) {
            if (d1 == null) {
                  return false;
            }
 
            if (d2 == null) {
                  return true;
            }
 
            return d1.compareTo(d2) == 1;
      }


	/**
	 * Valida codigo agencia banco.
	 *
	 * @param nro the nro
	 * @return the string
	 */
	public static String validaCodigoAgenciaBanco(Integer nro) {

		if (nro == null || nro.intValue() == 0) {
			return "";
		} else {
			return String.valueOf(nro.intValue());
		}

	}

	/**
	 * Format number.
	 *
	 * @param value the value
	 * @param decimals the decimals
	 * @param decimalSeparator the decimal separator
	 * @param showGroups the show groups
	 * @return the string
	 */
	public static String formatNumber(double value, int decimals, String decimalSeparator, boolean showGroups) {

		String valueCopy = new DecimalFormat("0.00").format(value);
		valueCopy = replaceAll(valueCopy, ".", decimalSeparator);
		return formatNumber(valueCopy, decimals, decimalSeparator, showGroups);
	}

	/**
	 * Format number.
	 *
	 * @param value the value
	 * @param decimals the decimals
	 * @param decimalSeparator the decimal separator
	 * @param showGroups the show groups
	 * @return the string
	 */
	public static String formatNumber(String value, int decimals, String decimalSeparator, boolean showGroups) {

		String ret = value;
		String integerNumbers;
		String decimalNumbers;
		int position = 0;

		if (Long.parseLong(replaceAll(replaceAll(ret, ".", ""), ",", "")) == 0) {
			if (decimals == 0) {
				return "0";
			} else {
				return "0" + decimalSeparator + preencherZerosAEsquerda("", decimals);
			}
		}

		ret = replaceAll(ret, ".", "");
		if (decimals == 0) {
			return ret;
		}

		if (ret.indexOf(decimalSeparator) == -1) {
			integerNumbers = ret.substring(0, ret.length());
			decimalNumbers = preencherZerosAEsquerda("", decimals);
		} else {
			integerNumbers = ret.substring(0, ret.indexOf(decimalSeparator));
			decimalNumbers = preencherZerosAEsquerda(ret.substring(ret.indexOf(decimalSeparator) + 1, ret.length()),
							decimals).substring(0, decimals);
		}
		ret = integerNumbers + decimalSeparator + decimalNumbers;

		// ret = roundNumber(ret, decimals, decimalSeparator);

		if (ret.indexOf(decimalSeparator) == -1) {
			integerNumbers = ret.substring(0, ret.length());
			decimalNumbers = preencherZerosAEsquerda("", decimals);
		} else {
			integerNumbers = ret.substring(0, ret.indexOf(decimalSeparator));
			if (integerNumbers.equals("")) {
				integerNumbers = "0";
			}

			decimalNumbers = preencherZerosAEsquerda(ret.substring(ret.indexOf(decimalSeparator) + 1, ret.length()),
							decimals);
		}

		int count = 0;
		for (position = integerNumbers.length(); position > 0; position--) {
			if (count == 3) {
				integerNumbers = integerNumbers.substring(0, position) + defaultInputNumberGroupSeparator
								+ integerNumbers.substring(position, integerNumbers.length());
				count = 1;
				position--;
			}
			count += 1;
		}
		return integerNumbers + decimalSeparator + decimalNumbers;
	}

	/*
     * completa um valor com zeros a esquerda at� completar o tamanho m�ximo
     */
	/**
	 * Preencher zeros a esquerda.
	 *
	 * @param valor the valor
	 * @param tamanhoMaximo the tamanho maximo
	 * @return the string
	 */
	public static String preencherZerosAEsquerda(String valor, int tamanhoMaximo) {
		String retorno = valor != null ? valor : "";
		for (int i = retorno.length(); i < tamanhoMaximo; i++) {
			retorno = "0".concat(retorno);
		}
		return retorno;
	}

	/**
	 * Replace all.
	 *
	 * @param sourceText the source text
	 * @param find the find
	 * @param replacement the replacement
	 * @return the string
	 */
	public static String replaceAll(String sourceText, String find, String replacement) {
		String value = sourceText;
		while (value.indexOf(find) != -1) {
			value = value.replace(find, replacement);
		}
		return value;

	}

	/**
	 * Formatar campo quantidade.
	 *
	 * @param cnpj the cnpj
	 * @return the string
	 */
	public static String formatarCampoQuantidade(String cnpj) {

		String cnpjformatado = "";
		String bloco = "   ";
		String cnpjAux = cnpj;

		while (cnpjAux.length() < 9) {
			cnpjAux = bloco.concat(cnpjAux);
		}

		String bloco3 = cnpjAux.substring(cnpjAux.length() - 9, cnpjAux.length() - 6);
		String bloco2 = cnpjAux.substring(cnpjAux.length() - 6, cnpjAux.length() - 3);
		String bloco1 = cnpjAux.substring(cnpjAux.length() - 3, cnpjAux.length());

		if (!bloco3.equals(bloco)) {
			cnpjformatado += bloco3 + ".";
		}
		if (!bloco2.equals(bloco)) {
			cnpjformatado += bloco2 + ".";
		}
		if (!bloco1.equals(bloco)) {
			cnpjformatado += bloco1;
		}

		return cnpjformatado.trim();
	}

	/**
	 * Formatar cnpj e cpf.
	 *
	 * @param cnpj the cnpj
	 * @return the string
	 */
	private static String formatarCnpjECpf(String cnpj) {

		String cnpjformatado = "";
		String bloco = "   ";
		String cnpjAux = cnpj;

		while (cnpjAux.length() < 9) {
			cnpjAux = bloco.concat(cnpjAux);
		}

		String bloco3 = cnpjAux.substring(cnpjAux.length() - 9, cnpjAux.length() - 6);
		String bloco2 = cnpjAux.substring(cnpjAux.length() - 6, cnpjAux.length() - 3);
		String bloco1 = cnpjAux.substring(cnpjAux.length() - 3, cnpjAux.length());

		if (!bloco3.equals(bloco)) {
			cnpjformatado += bloco3 + ".";
		}
		if (!bloco2.equals(bloco)) {
			cnpjformatado += bloco2 + ".";
		}
		if (!bloco1.equals(bloco)) {
			cnpjformatado += bloco1;
		}

		return cnpjformatado.trim();
	}

	/**
	 * Complementa digito.
	 *
	 * @param campo the campo
	 * @param num the num
	 * @return the string
	 */
	public static String complementaDigito(String campo, int num) {
		String campoAux = campo;
		while (campoAux.length() < num) {
			campoAux = "0".concat(campoAux);

		}

		return campoAux;
	}

	/**
	 * Complementa digito conta.
	 *
	 * @param campo the campo
	 * @return the string
	 */
	public static String complementaDigitoConta(String campo) {
		String campoAux = campo;

		if (campoAux.length() == 1) {
			if (Character.isDigit(campoAux.charAt(0))) {
				campoAux = "0".concat(campoAux);
			} else {
				campoAux = campoAux.concat(" ");
			}
		}

		return campoAux;
	}

	/**
	 * Formatar cnpj com mascara.
	 *
	 * @param cnpj the cnpj
	 * @param filial the filial
	 * @param controle the controle
	 * @return the string
	 */
	public static String formatarCNPJComMascara(String cnpj, String filial, String controle) {

		String cnpjFormatada = formatarCnpjECpf(cnpj);

		String filialFormatada = complementaDigito(filial, 4);

		String controleFormatada = complementaDigito(controle, 2);

		return cnpjFormatada + "/" + filialFormatada + "-" + controleFormatada;
	}

	/**
	 * Formatar cpf com mascara.
	 *
	 * @param cnpj the cnpj
	 * @param controle the controle
	 * @return the string
	 */
	public static String formatarCPFComMascara(String cnpj, String controle) {

		String cnpjFormatada = formatarCnpjECpf(cnpj);

		String controleFormatada = complementaDigito(controle, 2);

		return cnpjFormatada + "-" + controleFormatada;
	}

	/**
	 * Aplica mascara.
	 *
	 * @param cnpj the cnpj
	 * @param filial the filial
	 * @param controle the controle
	 * @return the string
	 */
	public static String aplicaMascara(String cnpj, String filial, String controle) {
		String cnpjFinal = addZero(cnpj, 9) + addZero(filial, 4) + addZero(controle, 2);
		return formatar(cnpjFinal);
	}

	/**
	 * Formatar.
	 *
	 * @param cnpj the cnpj
	 * @return the string
	 */
	private static String formatar(String cnpj) {
		StringBuffer cnpjFormatado = new StringBuffer(cnpj);
		cnpjFormatado.insert(3, '.').insert(7, '.').insert(11, '/').insert(16, '-');
		return cnpjFormatado.toString();
	}

	/**
	 * Formatar cnpj.
	 *
	 * @param cnpj the cnpj
	 * @return the string
	 */
	public static String formatarCnpj(String cnpj) {
		StringBuffer cnpjFormatado = new StringBuffer(addZero(cnpj, 15));
		cnpjFormatado.insert(3, '.').insert(7, '.').insert(11, '/').insert(16, '-');
		return cnpjFormatado.toString();
	}

	/**
	 * Formatar cpf.
	 *
	 * @param cpf the cpf
	 * @return the string
	 */
	public static String formatarCPF(String cpf) {
		StringBuffer cpfFormatado = new StringBuffer(addZero(cpf, 9));
		cpfFormatado.insert(3, '.').insert(7, '.');
		return cpfFormatado.toString();
	}

	/**
	 * Get: digitoCPF.
	 *
	 * @param cpf the cpf
	 * @return digitoCPF
	 */
	public static String getDigitoCPF(String cpf) {
		StringBuffer cpfFormatado = new StringBuffer(addZero(cpf, 9));
		cpfFormatado.insert(3, '.').insert(7, '.');

		int soma1 = 0;
		int soma2 = 0;
		int resto = 0;

		String digito1 = "";
		String digito2 = "";

		if (cpf.length() == 9) {
			soma1 = (Integer.parseInt(cpf.substring(0, 1)) * 10) + (Integer.parseInt(cpf.substring(1, 2)) * 9)
							+ (Integer.parseInt(cpf.substring(2, 3)) * 8) + (Integer.parseInt(cpf.substring(3, 4)) * 7)
							+ (Integer.parseInt(cpf.substring(4, 5)) * 6) + (Integer.parseInt(cpf.substring(5, 6)) * 5)
							+ (Integer.parseInt(cpf.substring(6, 7)) * 4) + (Integer.parseInt(cpf.substring(7, 8)) * 3)
							+ (Integer.parseInt(cpf.substring(8, 9)) * 2);

			resto = soma1 % 11;
			digito1 = "" + (resto < 2 ? 0 : 11 - resto);

			soma2 = (Integer.parseInt(cpf.substring(0, 1)) * 11) + (Integer.parseInt(cpf.substring(1, 2)) * 10)
							+ (Integer.parseInt(cpf.substring(2, 3)) * 9) + (Integer.parseInt(cpf.substring(3, 4)) * 8)
							+ (Integer.parseInt(cpf.substring(4, 5)) * 7) + (Integer.parseInt(cpf.substring(5, 6)) * 6)
							+ (Integer.parseInt(cpf.substring(6, 7)) * 5) + (Integer.parseInt(cpf.substring(7, 8)) * 4)
							+ (Integer.parseInt(cpf.substring(8, 9)) * 3) + (Integer.parseInt(digito1) * 2);

			resto = soma2 % 11;
			digito2 = "" + (resto < 2 ? 0 : 11 - resto);
		}

		return (digito1 + digito2);
	}

	/**
	 * Add zero.
	 *
	 * @param vr the vr
	 * @param size the size
	 * @return the string
	 */
	private static String addZero(String vr, int size) {
		String resultado = "";
		int vrLength = vr.length();
		if (vrLength <= size) {
			int dif = size - vrLength;
			for (int i = 0; i < dif; i++) {
				resultado += "0";
			}
			return resultado + vr;
		}
		throw new IllegalArgumentException("Valor maior que o intervalo informado.");
	}

	/**
	 * Gera relatorio pdf.
	 *
	 * @param pathRelatorio the path relatorio
	 * @param nomeRelatorio the nome relatorio
	 * @param listaDTO the lista dto
	 * @param parametros the parametros
	 * @throws JRException the JR exception
	 * @throws IOException the IO exception
	 */
	public static void geraRelatorioPdf(String pathRelatorio, String nomeRelatorio, List<?> listaDTO, Map parametros)
					throws JRException, IOException {

		/*
         * ------------ GERA O PDF E O MOSTRA NO LEITOR PADRAO. EXIBE OP��O PARA ABRIR E SALVAR. ------------
         * 
         * pathRelatorio: caminho onde est�o os .jasper no projeto. Geralmente embaixo de WebContent/relatorios O
         * arquivo fonte do relatorio *.JRXML tamb�m deve estar disponivel nesse mesmo caminho. nomeRelatorio: nome do
         * arquivo .jasper (sem a extens�o); listaDTO: lista devidamente preenchida com os dados do PDC. parametros:
         * Mapa de parametros a ser passado para o relatorio. Deve existir, mesmo vazio.
         * 
         * Para funcionar no projeto: 1. O arquivo POM.XML deve ter as referencias as bibliotecas do jasperreports e do
         * Itext. 2. Marcar as respectivas librarys na em TomCat/DevLoaderClassPath. 4. Criar pasta para guardar os
         * relat�rios criados pelo iReport abaixo de /WebContent/ 5. Copiar os .jasper e o .jrxml para a pasta criada.
         * 
         * valdecir.reghini@dedicgpti - out/2010.
         */

		// Arquivos
		String arquivoJasper = pathRelatorio + "/" + nomeRelatorio + ".jasper";
		String arquivoPDF = nomeRelatorio + ".pdf";

		// Passa a lista de saida Populada com os dados do PDC
		JRBeanCollectionDataSource jrds = new JRBeanCollectionDataSource(listaDTO);

		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
		// recupera path completo da aplicacao
		String pathRel = servletContext.getRealPath(arquivoJasper);

		try {
			parametros.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));

			// ------------- gera PDF, abre no leitor padr�o, mostra mas congela a jsp ---------------
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
							.getResponse();
			ServletOutputStream servletOutputStream = response.getOutputStream();
			byte[] bytes = JasperRunManager.runReportToPdf(pathRel, parametros, jrds);
			if (bytes != null && bytes.length > 0) {
				// Abre no leitor pdf padr�o da maquina cliente
				response.setHeader("Content-Disposition", "attachment; filename=" + arquivoPDF);
				response.setContentType("application/pdf");
				response.setContentLength(bytes.length);
				servletOutputStream.write(bytes, 0, bytes.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			}
			facesContext.renderResponse();
			facesContext.responseComplete();

		} catch (IOException e) {
			BradescoFacesUtils.addInfoModalMessage(e.getMessage(), "", BradescoViewExceptionActionType.ACTION, false);

		} catch (JRException e) {
			BradescoFacesUtils.addInfoModalMessage(e.getMessage(), "", BradescoViewExceptionActionType.ACTION, false);
		}

	}

	/**
	 * Gera relatorio pdf view.
	 *
	 * @param pathRelatorio the path relatorio
	 * @param nomeRelatorio the nome relatorio
	 * @param listaDTO the lista dto
	 * @param parametros the parametros
	 * @throws JRException the JR exception
	 */
	public static void geraRelatorioPdfView(String pathRelatorio, String nomeRelatorio, List<?> listaDTO, Map parametros)
					throws JRException {

		/*
         * ------------- GERA O PDF E O EXIBE NO VIEW DO JASPER/IREPORT.-------------
         * 
         * pathRelatorio: caminho onde est�o os .jasper no projeto. Geralmente embaixo de WebContent/relatorios O
         * arquivo fonte do relatorio *.JRXML tamb�m deve estar disponivel nesse mesmo caminho. nomeRelatorio: nome do
         * arquivo .jasper (sem a extens�o); listaDTO: lista devidamente preenchida com os dados do PDC. parametros:
         * Mapa de parametros a ser passado para o relatorio. Deve existir, mesmo vazio.
         * 
         * Para funcionar no projeto: 1. O arquivo POM.XML deve ter as referencias as bibliotecas do jasperreports e do
         * Itext. 2. Marcar as respectivas librarys na em TomCat/DevLoaderClassPath. 4. Criar pasta para guardar os
         * relat�rios criados pelo iReport abaixo de /WebContent/ 5. Copiar os .jasper e o .jrxml para a pasta criada.
         * 
         * valdecir.reghini@dedicgpti - out/2010.
         */

		// Arquivo
		String arquivoJasper = pathRelatorio + "/" + nomeRelatorio + ".jasper";

		// Passa a lista de saida Populada com os dados do PDC
		JRBeanCollectionDataSource jrds = new JRBeanCollectionDataSource(listaDTO);

		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
		String pathRel = servletContext.getRealPath(arquivoJasper);

		try {
			parametros.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));

			JasperPrint jp = JasperFillManager.fillReport(pathRel, parametros, jrds);
			JasperViewer view = new JasperViewer(jp, false);
			view.setExtendedState(JasperViewer.MAXIMIZED_BOTH);
			view.setTitle(nomeRelatorio);
			view.setVisible(true);

		} catch (JRException e) {
			BradescoFacesUtils.addInfoModalMessage(e.getMessage(), "", BradescoViewExceptionActionType.ACTION, false);
		}
	}

	/**
	 * Verifica string nula.
	 *
	 * @param str the str
	 * @return the string
	 */
	public static String verificaStringNula(String str) {
		if (str == null) {
			return "";
		} else {
			return str;
		}

	}

	/**
	 * Is string vazio.
	 *
	 * @param valor the valor
	 * @return true, if is string vazio
	 */
	public static boolean isStringVazio(String valor) {
		return valor == null || valor.trim().equals("");
	}

	/**
	 * Verifica string to integer nula.
	 *
	 * @param str the str
	 * @return the integer
	 */
	public static Integer verificaStringToIntegerNula(String str) {
		if (str == null) {
			return 0;
		} else {
			return Integer.valueOf(str);
		}
		
	}

	/**
	 * Verifica integer nulo.
	 *
	 * @param nro the nro
	 * @return the integer
	 */
	public static Integer verificaIntegerNulo(Integer nro) {
		if (nro == null) {
			return 0;
		} else {
			return nro.intValue();
		}
	}

	/**
	 * Verifica big decimal nulo.
	 *
	 * @param nro the nro
	 * @return the big decimal
	 */
	public static BigDecimal verificaBigDecimalNulo(BigDecimal nro) {
		if (nro == null) {
			return BigDecimal.ZERO;
		} else {
			return nro;
		}

	}
	
	public static BigInteger verificaBigIntegerNulo(BigInteger nro) {
		if (nro == null) {
			return BigInteger.ZERO;
		} else {
			return nro;
		}
		
	}

	/**
	 * Verifica long nulo.
	 *
	 * @param nro the nro
	 * @return the long
	 */
	public static long verificaLongNulo(Long nro) {
		if (nro == null) {
			return 0l;
		} else {
			return nro.longValue();
		}

	}
	
	   /**
     * Nome: isStringNullLong.
     *
     * @param value the value
     * @return the long
     * @see
     */  
     public static Long isStringNullLong(String value) {
           if (value != null && !value.equals("")) {
                 return Long.valueOf(value);
           }

           return new Long(0);
     }

	/**
	 * Verifica usuario interno externo.
	 *
	 * @param usuarioInterno the usuario interno
	 * @param usuarioExterno the usuario externo
	 * @return the string
	 */
	public static String verificaUsuarioInternoExterno(String usuarioInterno, String usuarioExterno) {
		return (usuarioExterno == null || usuarioExterno.equals("") ? usuarioInterno == null ? "" : usuarioInterno
						: usuarioExterno);
	}

	/**
	 * Set session attribute.
	 *
	 * @param attributeName the attribute name
	 * @param attribute the attribute
	 */
	public static void setSessionAttribute(String attributeName, Object attribute) {
		FacesUtils.getContext().getExternalContext().getSessionMap().put(attributeName, attribute);
	}

	/**
	 * Get: sessionAttribute.
	 *
	 * @param attributeName the attribute name
	 * @return sessionAttribute
	 */
	public static Object getSessionAttribute(String attributeName) {
		return FacesUtils.getContext().getExternalContext().getSessionMap().get(attributeName);
	}

	/**
	 * Concatenar campos.
	 *
	 * @param valor1 the valor1
	 * @param valor2 the valor2
	 * @return the string
	 */
	public static String concatenarCampos(Object valor1, Object valor2) {
		return concatenarCampos(valor1, valor2, " - ");
	}

	/**
	 * Concatenar campos.
	 *
	 * @param valor1 the valor1
	 * @param valor2 the valor2
	 * @param separador the separador
	 * @return the string
	 */
	public static String concatenarCampos(Object valor1, Object valor2, String separador) {
		String valorFinal = "";
		if (!CamposUtils.validarCampo(valor1) && !CamposUtils.validarCampo(valor2)) {
			valorFinal = valor1 + separador + valor2;
		} else {
			valorFinal = (!CamposUtils.validarCampo(valor1) ? String.valueOf(valor1) : "")
							+ (!CamposUtils.validarCampo(valor2) ? String.valueOf(valor2) : "");
		}
		return valorFinal;
	}

	/**
	 * Concatenar campos agencia conta.
	 *
	 * @param valor1 the valor1
	 * @param valor2 the valor2
	 * @param separador the separador
	 * @return the string
	 */
	public static String concatenarCamposAgenciaConta(Object valor1, Object valor2, String separador) {
		String valorFinal = "";

		if (valor1 != null && !valor1.equals("") && valor2 != null && !valor2.equals("")) {
			valorFinal = valor1 + separador + valor2;
		} else {
			valorFinal = (valor1 != null ? String.valueOf(valor1) : "")
							+ (valor2 != null ? String.valueOf(valor2) : "");
		}
		return valorFinal;
	}

	/*******************************************************************************************************************
     * M�todo que formata banco. Banco s� sera exibido se for diferente de nulo, vazio ou 0. Se inserirZeroEsquerda =
     * true ent�o formata campo
     * 
     * @param banco -
     *            C�digo do banco
     * @param inserirZeroEsquerda -
     *            Flag para indicar se insere zero a esquerda
     * @return
     */
	public static String formatBanco(Integer banco, boolean inserirZeroEsquerda) {
		String valorFinal = "";
		if (!CamposUtils.validarCampo(banco)) {
			if (inserirZeroEsquerda) {
				valorFinal = SiteUtil.formatNumber(String.valueOf(banco), 3);
			} else {
				valorFinal = String.valueOf(banco);
			}
		}

		return valorFinal;
	}

	/*******************************************************************************************************************
     * M�todo que formata banco e concatena com descri��o. Cada campo s� sera exibido se for diferente de nulo, vazio ou
     * 0. Se inserirZeroEsquerda = true ent�o formata campo
     * 
     * @param banco -
     *            C�digo do banco
     * @param bancoDescricao -
     *            Descri��o do banco
     * @param inserirZeroEsquerda -
     *            Flag para indicar se insere zero a esquerda
     * @return
     */
	public static String formatBanco(Integer banco, String bancoDescricao, boolean inserirZeroEsquerda) {
		return PgitUtil.concatenarCampos(formatBanco(banco, inserirZeroEsquerda), bancoDescricao);
	}

	/*******************************************************************************************************************
     * M�todo que formata ag�ncia e digito da ag�ncia. Cada campo s� sera exibido se for diferente de nulo, vazio ou 0.
     * Se inserirZeroEsquerda = true ent�o formata campo
     * 
     * @param agencia -
     *            C�digo da ag�ncia
     * @param agenciaDigito -
     *            Digito da ag�ncia
     * @param inserirZeroEsquerda -
     *            Flag para indicar se insere zero a esquerda
     * @return
     */
	public static String formatAgencia(Integer agencia, Object agenciaDigito, boolean inserirZeroEsquerda) {
		String valorFinal = "";
		if (PgitUtil.verificaIntegerNulo(agencia) != 0) {
			/*
             * if(CamposUtils.validarCampo(agenciaDigito)){ if(inserirZeroEsquerda){ valorFinal =
             * SiteUtil.formatNumber(String.valueOf(agencia), 5); } else{ valorFinal = String.valueOf(agencia); } }
             * else{
             */
			if (inserirZeroEsquerda) {
				valorFinal = PgitUtil.concatenarCamposAgenciaConta(SiteUtil.formatNumber(String.valueOf(agencia), 5),
								agenciaDigito, "-");
			} else {
				valorFinal = PgitUtil.concatenarCamposAgenciaConta(agencia, agenciaDigito == null ? "" : String
								.valueOf(agenciaDigito), "-");
			}
			// }
		}

		return valorFinal;
	}

	/*******************************************************************************************************************
     * M�todo que formata ag�ncia, digito da ag�ncia e concatena com descri��o. Cada campo s� sera exibido se for
     * diferente de nulo, vazio ou 0. Se inserirZeroEsquerda = true ent�o formata campo
     * 
     * @param agencia -
     *            C�digo da ag�ncia
     * @param agenciaDigito -
     *            Digito da ag�ncia
     * @param agenciaDescricao
     * @param inserirZeroEsquerda -
     *            Flag para indicar se insere zero a esquerda
     * @return
     */
	public static String formatAgencia(Integer agencia, Object agenciaDigito, String agenciaDescricao,
					boolean inserirZeroEsquerda) {
		return PgitUtil.concatenarCampos(formatAgencia(agencia, agenciaDigito, inserirZeroEsquerda), agenciaDescricao);
	}

	/*******************************************************************************************************************
     * M�todo que formata conta e digito da conta. Cada campo s� sera exibido se for diferente de nulo, vazio ou 0. Se
     * inserirZeroEsquerda = true ent�o formata campo
     * 
     * @param conta -
     *            C�digo da conta
     * @param digitoConta -
     *            Digito da conta
     * @param inserirZeroEsquerda -
     *            Flag para indicar se insere zero a esquerda
     * @return
     */
	public static String formatConta(Long conta, String digitoConta, boolean inserirZeroEsquerda) {
		String valorFinal = "";
		if (PgitUtil.verificaLongNulo(conta) != 0) {
			/*
             * if(PgitUtil.verificaStringNula(digitoConta).equals("") ||
             * PgitUtil.verificaStringNula(digitoConta).equals("0") ||
             * PgitUtil.verificaStringNula(digitoConta).equals("00")){ if(inserirZeroEsquerda){ valorFinal =
             * SiteUtil.formatNumber(String.valueOf(conta), 13); } else{ valorFinal = String.valueOf(conta); } } else{
             */
			if (inserirZeroEsquerda) {
				valorFinal = PgitUtil.concatenarCamposAgenciaConta(SiteUtil.formatNumber(String.valueOf(conta), 13),
								digitoConta, "-");
			} else {
				valorFinal = PgitUtil
								.concatenarCamposAgenciaConta(conta, PgitUtil.verificaStringNula(digitoConta), "-");
			}
			// }
		}

		return valorFinal;
	}

	/*******************************************************************************************************************
     * M�todo que retorna banco, ag�ncia e conta formatados. Cada campo s� sera exibido se for diferente de nulo, vazio
     * ou 0. Exemplo: 237 / 1382-1 / 222-1
     * 
     * @param banco -
     *            C�digo do banco
     * @param agencia -
     *            C�digo da ag�ncia
     * @param agenciaDigito -
     *            Digito da ag�ncia
     * @param conta -
     *            C�digo da conta
     * @param digitoConta -
     *            Digito da conta
     * @param inserirZeroEsquerda -
     *            Flag para indicar se insere zero a esquerda
     * @return
     */
	public static String formatBancoAgenciaConta(Integer banco, Integer agencia, Object agenciaDigito, Long conta,
					String digitoConta, boolean inserirZeroEsquerda) {
		String bancoFormatado = formatBanco(banco, inserirZeroEsquerda);
		String agenciaFormatada = formatAgencia(agencia, agenciaDigito, inserirZeroEsquerda);
		String contaFormatada = formatConta(conta, digitoConta, inserirZeroEsquerda);

		return concatenarCampos(concatenarCampos(bancoFormatado, agenciaFormatada, " / "), contaFormatada, " / ");
	}
	
	public static String formatBancoIspbConta(Integer banco, String ispb, String conta, boolean inserirZeroEsquerda) {
		String bancoFormatado = formatBanco(banco, inserirZeroEsquerda);
		String ispbFormatada =  preencherZerosAEsquerda(ispb, 8);
		String contaFormatada = preencherZerosAEsquerda(conta, 20);
		
		return concatenarCampos(concatenarCampos(bancoFormatado, ispbFormatada, " / "), contaFormatada, " / ");
	}

	/**
	 * Calcular valor tarifa atualizada.
	 *
	 * @param valorTarifa the valor tarifa
	 * @param percentualDesconto the percentual desconto
	 * @return the big decimal
	 */
	public static BigDecimal calcularValorTarifaAtualizada(BigDecimal valorTarifa, BigDecimal percentualDesconto) {
		BigDecimal cem = BigDecimal.TEN.multiply(BigDecimal.TEN);
		BigDecimal retorno = BigDecimal.ZERO;

		// SE VALOR DA TARIFA N�O FOR NULO E NEM ZERO REALIZAR CALCULOS
		if (valorTarifa != null && valorTarifa.compareTo(BigDecimal.ZERO) > 0) {
			// SE O PERCENTUAL N�O FOR ZERO E NEM NULO CALCULO SER� FEITO
			if (percentualDesconto != null && percentualDesconto.compareTo(BigDecimal.ZERO) > 0) {
				// SE PERCENTUAL N�O FOR MAIOR QUE 100 ENT�O REALIZA CALCULO
				if (percentualDesconto.compareTo(cem) < 0) {
					retorno = valorTarifa.multiply((BigDecimal.ONE.subtract(percentualDesconto.divide(cem))));
				}
			}
			// SEN�O RETORNA VALOR ORIGINAL
			else {
				retorno = valorTarifa;
			}
		}
		return retorno;
	}

	/*******************************************************************************************************************
     * M�todo que ir� converter uma Data em inteiro no formato DDMMYYYY
     * 
     * @param data
     * @return
     */
	public static Integer converterData(Date data) {
		if (data != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(data);

			String temp = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
			// soma 1 no m�s pois ele vai de 0 a 11
			temp += complementaDigito(String.valueOf(calendar.get(Calendar.MONTH) + 1), 2);
			temp += String.valueOf(calendar.get(Calendar.YEAR));
			return Integer.valueOf(temp);
		} else {
			return null;
		}
	}

	/*******************************************************************************************************************
     * M�todo que ir� formatar uma string conforme c�digo de barras, se a string n�o tiver a quantidade necess�ria de
     * caracter complementa com 0 (zero).
     * 
     * @param linhaDigitavel -
     *            linha digit�vel sem formata��o. Ex.: 23793114066000140218300017643800349560001666153
     * @return linha digit�vel formatada. Ex.: 23793.11406 60001.402183 00017.643800 3 49560001666153
     */
	public static String formatarLinhaDigitavel(String linhaDigitavel) {
		if (linhaDigitavel == null || linhaDigitavel.equals("")) {
			return "";
		}

		String linha = linhaDigitavel.replace(".", "");
		linha = linhaDigitavel.replace(" ", "");

		StringBuffer linhaDigitavelFormatada = new StringBuffer(addZero(linha, 47));
		linhaDigitavelFormatada.insert(5, '.').insert(11, ' ');
		linhaDigitavelFormatada.insert(17, '.').insert(24, ' ');
		linhaDigitavelFormatada.insert(30, '.').insert(37, ' ').insert(39, ' ');

		return linhaDigitavelFormatada.toString();
	}

	/**
     * M�todo que retornar� uma descri��o para cada n�mero do tipo de favorecido.
     * 
     * @param tipoFavorecido
     * @return descricao do tipo de favorecido
     */
	public static String validaTipoFavorecido(Integer tipoFavorecido) {

		String descTipoFavorerido = "";

		if (tipoFavorecido == 0) {
			// Altera��o Solicitada pela kelli quarta-feira, 15 de junho de 2011 10:31
			// descTipoFavorerido = MessageHelperUtils.getI18nMessage("label_isento_nao_informado");
			descTipoFavorerido = "";
		} else if (tipoFavorecido == 1) {
			descTipoFavorerido = MessageHelperUtils.getI18nMessage("label_cpf");
		} else if (tipoFavorecido == 2) {
			descTipoFavorerido = MessageHelperUtils.getI18nMessage("label_cnpj");
		} else if (tipoFavorecido == 3) {
			//descTipoFavorerido = MessageHelperUtils.getI18nMessage("label_pis_pasep"); Altera��o pedida pelo Guilherme, ter�a-feira, 07/08/2012 11:28
			descTipoFavorerido = MessageHelperUtils.getI18nMessage("label_outros_maiusc");
		} else {
			descTipoFavorerido = MessageHelperUtils.getI18nMessage("label_outros_maiusc");
		}

		return descTipoFavorerido;
	}

	/**
	 * Formata favorecido.
	 *
	 * @param tipoFavorecido the tipo favorecido
	 * @param cdFavorecido the cd favorecido
	 * @return the string
	 */
	public static String formataFavorecido(Integer tipoFavorecido, String cdFavorecido) {
		int cdFavorecidoNum = 0;
		try {
			cdFavorecidoNum = Integer.parseInt(cdFavorecido);
		} catch (NumberFormatException e) {
			cdFavorecidoNum = 1;
		}

		if (cdFavorecido == null || cdFavorecido.equals("") || cdFavorecidoNum == 0) {
			return "";
		}

		String numInscricaoFavorecido = "";
		Integer tipo = tipoFavorecido == null ? 0 : tipoFavorecido;

		if (tipo == 1 || tipo == 2) {
			numInscricaoFavorecido = formatCpfCnpj(cdFavorecido);
		} else {
			numInscricaoFavorecido = verificaStringNula(cdFavorecido);
		}
		return numInscricaoFavorecido;

	}

	/*
     * public static String formataBeneficiario(String tipoBeneficiario, String cdBeneficiario){ if(cdBeneficiario ==
     * null || cdBeneficiario.equals("")){ return ""; }
     * 
     * String numInscricaoBeneficiario=""; tipoBeneficiario = tipoBeneficiario == null ? "" : tipoBeneficiario;
     * 
     * if (tipoBeneficiario.equals("1") || tipoBeneficiario.equals("2")){ numInscricaoBeneficiario =
     * formatCpfCnpj(cdBeneficiario); }else{ numInscricaoBeneficiario = verificaStringNula(cdBeneficiario); } return
     * numInscricaoBeneficiario;
     *  }
     */

	/*******************************************************************************************************************
     * Formata um n�mero, inserindo . (ponto) quando necess�rio
     * 
     * @param numero -
     *            n�mero que deseja formatar
     * @param retornaZero -
     *            flag que indica se caso esteja zerado ou nulo deseja que retorna 0 ou em branco
     * @return Ex.: 1000 = 1.000;
     */
	public static String formatarNumero(Object numero, boolean retornaZero) {
		if (CamposUtils.validarCampo(numero)) {
			if (retornaZero) {
				return "0";
			} else {
				return "";
			}
		}

		NumberFormat nf = NumberFormat.getInstance(new Locale("pt_br"));
		return nf.format(numero);
	}

	/*******************************************************************************************************************
     * Verifica se o n�mero s� possui zeros
     * 
     * @param numero -
     *            n�mero que deseja verificar se s� possui zeros
     * @param retorna
     *            null ou branco - quando s� houver zeros no n�mero
     * @return Ex.: 0.00 = null;
     */
	public static Object verificaZero(Object numero) {
		if (numero == null) {
			return null;
		}
		if (numero instanceof BigDecimal && ((BigDecimal) numero).compareTo(BigDecimal.ZERO) == 0) {
			return null;
		}
		if (numero instanceof Integer && numero.equals(0)) {
			return null;
		}
		if (numero instanceof String && numero.equals("0")) {
			return "";
		}
		if (numero instanceof Long && numero.equals(0L)) {
			return null;
		}

		return numero;
	}

	/**
	 * Integer to string.
	 *
	 * @param valor the valor
	 * @return the string
	 */
	public static String integerToString(Integer valor) {
		if (verificaZero(verificaIntegerNulo(valor)) != null) {
			return String.valueOf(valor);
		}

		return ""; 
	}

	/**
	 * Long to string.
	 *
	 * @param valor the valor
	 * @return the string
	 */
	public static String longToString(Long valor) {
		if (verificaZero(verificaLongNulo(valor)) != null) {
			return String.valueOf(valor);
		}

		return ""; 
	}

	/**
	 * Integer to long.
	 *
	 * @param valor the valor
	 * @return the long
	 */
	public static Long integerToLong(Integer valor) {
		return verificaIntegerNulo(valor).longValue();
	}

	/**
	 * Format cpf cnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 * @return the string
	 */
	public static String formatCpfCnpj(String cpfCnpj) {

		String auxCpfCnpj = SiteUtil.formatNumber((cpfCnpj != null ? cpfCnpj : ""), 15);

		if (!auxCpfCnpj.equals("000000000000000")) {
			if (auxCpfCnpj.substring(9, 13).equals("0000")) {
				auxCpfCnpj = CpfCnpjUtils.formatCpfCnpj(auxCpfCnpj, 1);
			} else {
				auxCpfCnpj = CpfCnpjUtils.formatCpfCnpj(auxCpfCnpj, 2);
			}
		} else {
			auxCpfCnpj = "";
		}

		return auxCpfCnpj;
	}

	/*******************************************************************************************************************
     * M�todo que retorna CPF ou CNPJ formatado, se campo for menor ele complementa com 0 a esquerda
     * 
     * @param identificador
     *            CPF ou CNPJ
     * @param tipo
     *            1 = CPF, 2 = CNPJ
     * @return
     */
	public static String formatCpfCnpj(Long identificador, Integer tipo) {
		StringBuilder sb = new StringBuilder("");
		if (identificador != null && tipo != null) {
			if (tipo.intValue() == 1) {
				String cpf = SiteUtil.formatNumber(String.valueOf(identificador), 11);
				sb.append(cpf.substring(0, 3));
				sb.append(".");
				sb.append(cpf.substring(3, 6));
				sb.append(".");
				sb.append(cpf.substring(6, 9));
				sb.append("-");
				sb.append(cpf.substring(9, 11));
			} else {
				if (tipo.intValue() == 2) {
					String cnpj = SiteUtil.formatNumber(String.valueOf(identificador), 14);
					sb.append(cnpj.substring(0, 2));
					sb.append(".");
					sb.append(cnpj.substring(2, 5));
					sb.append(".");
					sb.append(cnpj.substring(5, 8));
					sb.append("/");
					sb.append(cnpj.substring(8, 12));
					sb.append("-");
					sb.append(cnpj.substring(12, 14));
					return sb.toString();
				}
			}
		}

		return sb.toString();
	}

	/*******************************************************************************************************************
     * M�todo que formata CEP
     * 
     * @param cep1 -
     *            Primeira parte do CEP
     * @param cep2 -
     *            Segunda parte do CEP
     * @return
     */
	public static String formatCep(Integer cep1, Integer cep2) {
		if (cep1 == null || cep1 == 0) {
			return "";
		}

		String cep1Aux = complementaDigito(String.valueOf(cep1), 5);
		String cep2Aux = complementaDigito(String.valueOf(cep2), 3);

		return concatenarCampos(cep1Aux, cep2Aux, "-");
	}

	/*******************************************************************************************************************
     * M�todo que verifica se digito conta possui dois bytes se sim retorna apenas o �ltimo byte
     * 
     * @param digito -
     *            digito conta
     * @return
     */
	public static String verificaBytesDigitoConta(String digito) {

		if (digito == null || digito.equals("")) {
			return "";
		}

		int tamanho = digito.length();

		if (tamanho > 1) {
			return digito.substring(tamanho - 1, tamanho);
		} else {
			return digito;
		}

	}

	/*******************************************************************************************************************
     * M�todo que formata o campo "Nosso N�mero", das telas de Detalhe de "Outros Bancos" e "Bradesco"
     * 
     * @param nossoNumero -
     *            Nosso N�mero
     * @return
     */
	public static String formatarNossoNumero(String nossoNumero) {
		// Remover espa�os Inicio/Final
		String numero = nossoNumero.trim();
		try {
			Double.valueOf((numero));
		} catch (NumberFormatException e) {
			return numero;
		}

		// Remover Zeros a esquerda
		return numero.replaceAll("^0*", "");

	}

	/**
	 * Obter select item selecionado.
	 *
	 * @param itens the itens
	 * @param codItem the cod item
	 * @return the string
	 */
	public static String obterSelectItemSelecionado(List<SelectItem> itens, Integer codItem) {
		if (codItem != null) {
			for (SelectItem i : itens) {
				if (i.getValue().equals(codItem)) {
					return i.getLabel();
				}
			}
		}

		return "";
	}
	
	/**
	 * Obter select item selecionado.
	 *
	 * @param itens the itens
	 * @param codItem the cod item
	 * @return the string
	 */
	public static String obterSelectItemSelecionado(List<SelectItem> itens, Long codItem) {
		if (codItem != null) {
			for (SelectItem i : itens) {
				if (i.getValue().equals(codItem)) {
					return i.getLabel();
				}
			}
		}
		
		return "";
	}

	/**
	 * Is number.
	 *
	 * @param s the s
	 * @return true, if is number
	 */
	public static boolean isNumber(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	/**
	 * @param string a ser separada
	 * @return xxx - descri��o 
	 */
	public static String separadorCodigoDescricao(String valorNaoFormatado) {
		String valorFormatado = "";

		for (int i = 1; i <= valorNaoFormatado.length(); i++) {
			if (PgitUtil.isNumber(valorNaoFormatado.substring(i-1, i))) {
				valorFormatado += valorNaoFormatado.substring(i-1, i);
			} else {
				valorFormatado += " - " + valorNaoFormatado.substring(i-1);
				break;
			}
		}
	   
		return valorFormatado;
	}

	/**
	 * Format number percent.
	 *
	 * @param number the number
	 * @return the string
	 */
	public static String formatNumberPercent(Number number) {
		String numberFormat = NumberUtils.format(number);
		if ("".equals(numberFormat)) {
			return "";
		}

		return new StringBuilder(numberFormat).append(" %").toString();
	}

	/**
     * Formatar valor cifrao.
     *
     * @param valor the valor
     * @return the string
     */
    public static String formatarValorDinheiro(BigDecimal valor) {
        if (valor == null) {
            return "";
        }
        return String.format("R$ %s", NumberUtils.format(valor));
    }

    /**
     * Formatar cep.
     * 
     * @param cep
     *            the cep
     * @param cepComplemento
     *            the cep complemento
     * @return the string
     */
    public static String formatarCep(Integer cep, Integer cepComplemento) {
        if (cep != null && cepComplemento != null) {
            return String.format("%05d-%03d", cep, cepComplemento);
        }

        return "";
    }
    
    /**
     * Formatar telefone.
     * 
     * @param ddd
     *            the ddd
     * @param numero
     *            the numero
     * @return the string
     */
    public static String formatarTelefone(Integer ddd, Long numero) {
        StringBuilder telefoneFormatado = new StringBuilder();
        if (ddd != null && numero != null) {
            telefoneFormatado.append(String.format("(%d) ", ddd));
            if (numero.toString().length() > 4) {
                telefoneFormatado.append(numero.toString().substring(0, numero.toString().length() - 4));
                telefoneFormatado.append("-");
                telefoneFormatado.append(numero.toString().substring(numero.toString().length() - 4));
            } else {
                telefoneFormatado.append(numero);
            }
        }

        return telefoneFormatado.toString();
    }
    
	public static boolean validarEmail(String email) {
		boolean isEmailIdValid = false;
		if (email != null && email.length() > 0) {
			String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
			Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(email);
			if (matcher.matches()) {
				isEmailIdValid = true;
			}
		}
		return isEmailIdValid;
	}

	/**
	 * Verifica se a objeto est� vazio.
	 * 
	 * @param pObject
	 * @return true, se a objeto est� nulo ou vazio. Se n�o, false.
	 */
	public static boolean isEmptyOrNull(Object pObject) {
		return ((pObject == null) || pObject.toString().trim().length() == 0);
	}
	
	public static String rtrim(String s) {
		if (s == null || s.equals("")) {
			return "";
		}
		return s.replaceAll("\\s+$", "");
	}
	
	public static String ltrim (String s) { 
		if(s == null || s.equals("")){
			return "";
		}
		return s.replaceAll("^\\s+", ""); 
	} 
	
	public static String tirarEspacoEsquerdaDireita(String s) {
		if (PgitUtil.isEmptyOrNull(s)) {
			return "";
		}
		return PgitUtil.rtrim(PgitUtil.ltrim(s));
	}
	
	/**
	 * Checks if is email valido.
	 * 
	 * @param email
	 *            the email
	 * @return true, if is email valido
	 */
	public static boolean isEmailValido(String email) {
		if (SiteUtil.isEmptyOrNull(email)) {
			return false;
		}
		return ValidadorEmail.validar(email);
	}
	
	/**
	 * <b>Nome:</b> IntValue<br>
	 * <b>Proposito:</b>
	 * <p>
	 * Da get de IntValue.
	 * </p>
	 * 
	 * @param value
	 *            - String
	 * @return IntValue - int
	 */
	public static Integer getIntValueWithNull(Object value) {
		Integer valor = null;
		
		try {
			if (value instanceof String) {
				if ((value != null) && (((String) value).length() > 0)) {
					valor = Integer.parseInt(((String) value));
				}
			}
			if (value instanceof Integer) {
				if (value != null) {
					valor = ((Integer) value).intValue();
				}
			}
			if (value instanceof Long) {
				if (value != null) {
					valor = ((Long) value).intValue();
				}
			}
		} catch (Exception e) {
		}
		
		return valor;
	}
}