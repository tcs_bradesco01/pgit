/*
 * =========================================================================
 * 
 * Client:       Bradesco (BR)
 * Project:      Arquitectura Bradesco Canal Web
 * Development:  GFT Iberia (http://www.gft.com)
 * -------------------------------------------------------------------------
 * Revision - Last:
 * $Source: /Repositorio/TIMelhorias_AQ/Projetos/pgitIntranet/JavaSource/br/com/bradesco/web/pgit/utils/SiteUtil.java,v $
 * $Id: SiteUtil.java,v 1.2 2009/05/13 19:39:27 cpm.com.br\heslei.silva Exp $
 * $State: Exp $
 * -------------------------------------------------------------------------
 * Revision - History:
 * $Log: SiteUtil.java,v $
 * Revision 1.2  2009/05/13 19:39:27  cpm.com.br\heslei.silva
 * voltando para vers�o do branch 2.0
 *
 * Revision 1.1.2.1  2009/05/05 14:24:44  corporate\marcio.alves
 * Adicionando pagina��o ao pgit intranet
 *
 * Revision 1.5  2008/11/18 22:40:55  cpm.com.br\tiago.lopes
 * *** empty log message ***
 *
 * Revision 1.4  2008/11/18 22:00:29  cpm.com.br\tiago.lopes
 * *** empty log message ***
 *
 * Revision 1.3  2008/07/17 15:45:22  cpm.com.br\nailton.santos
 * Tela de Consulta Confronto empresa
 *
 * Revision 1.1  2008/01/25 12:57:27  cpm.com.br\alessandro.antunes
 * Inclus�o da hierarquia de classes do novo projeto CTBL.
 *
 * Revision 1.1  2008/01/10 11:12:43  cpm.com.br\edwin.costa
 * Cria��o do projeto CTBL para a Arquitetura Funcional.
 *
 * Revision 1.3  2007/05/23 19:46:56  cpm.com.br\edwin.costa
 * Configura��o de projeto
 *
 * Revision 1.2  2007/05/23 18:29:42  cpm.com.br\edwin.costa
 * Configura��o de projeto
 *
 * Revision 1.1  2007/05/17 19:36:21  cpm.com.br\edwin.costa
 * Altera��o do Menu
 *
 *
 * =========================================================================
 */

package br.com.bradesco.web.pgit.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.FactoryFinder;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.config.RuntimeConfig;
import org.apache.myfaces.config.element.NavigationCase;
import org.apache.myfaces.config.element.NavigationRule;
import org.apache.myfaces.util.HashMapUtils;
import org.apache.myfaces.util.MessageUtils;

import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.view.util.FacesUtils;

/**
 * 
 * <p>
 * <b>Title:</b> Bradesco Cartoes.
 * </p>
 * <p>
 * <b>Description:</b>
 * </p>
 * <p>
 * Fun��es uteis que poderao ser utilizadas por todos as classes.
 * </p>
 * 
 * @author GFT Iberia Solutions / Emagine <BR/>
 *         copyright Copyright (c) 2006 <BR/>
 *         created 13/10/2006 <BR/>
 * @version 1.0
 */
public final class SiteUtil {

	/**
	 * Representa uma String vazia.
	 */
	public static final String STRING_EMPTY = "";

	/**
	 * 
	 * Construtor privado da classe SiteUtil.
	 * 
	 */
	private SiteUtil() {
	}

	/**
	 * Obter o contexto Faces mediante o ServletRequest. (por exemplo, para
	 * obter o faces context do Servlet Filter)
	 * 
	 * @param request
	 *            ServletRequest.
	 * @param respose
	 *            ServletResponse.
	 * @param servletContext
	 *            ServletContext.
	 * 
	 * @return FacesContext instancia do contexto Faces atual.
	 */
	public static FacesContext getFacesContext(ServletRequest request,
			ServletResponse respose, ServletContext servletContext) {

		FacesContext facesContext = FacesContext.getCurrentInstance();

		if (facesContext != null) {
			return facesContext;
		}

		FacesContextFactory contextFactory = (FacesContextFactory) FactoryFinder
				.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
		LifecycleFactory lifecycleFactory = (LifecycleFactory) FactoryFinder
				.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
		Lifecycle lifecycle = lifecycleFactory
				.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);

		return contextFactory.getFacesContext(servletContext, request, respose,
				lifecycle);
	}

	/**
	 * Obter o atributo 'toViewId', configurado no caso da navega��o, para os
	 * "outcome" e "fromAction" passados.
	 * 
	 * @param facesContext
	 *            Contexto Faces.
	 * @param outcome
	 *            O atributo 'outcome' do caso da navega��o.
	 * @param fromAction
	 *            O atributo 'from-action' do caso da navega��o.
	 * @return String O atributo 'toViewId' do caso da navega��o.
	 */
	public static String getNavigationCaseToViewId(FacesContext facesContext,
			String outcome, String fromAction) {

		if (facesContext == null) {
			return null;
		}

		String viewId = "*";

		if (facesContext.getViewRoot() != null) {
			viewId = facesContext.getViewRoot().getViewId();
		}

		Map casesMap = getNavigationCases(facesContext);
		NavigationCase navigationCase = null;

		List casesList = (List) casesMap.get(viewId);
		if (casesList != null) {
			// Exact match?
			navigationCase = calculaMapeamentoNavigationCase(casesList,
					fromAction, outcome);
		}

		if (navigationCase != null) {
			return navigationCase.getToViewId();
		}

		return null;
	}

	/**
	 * Retorna o mapeamento do caso da navega��o para os parametros passados.
	 * 
	 * @param casesList
	 *            Listagem dos casos da navega��o.
	 * @param actionRef
	 *            Referencia da a��o realizada.
	 * @param outcome
	 *            O String 'outcome' para mapeamento.
	 * @return NavigationCase O caso da navega��o mapeado
	 */
	public static NavigationCase calculaMapeamentoNavigationCase(
			List casesList, String actionRef, String outcome) {
		for (int i = 0, size = casesList.size(); i < size; i++) {
			NavigationCase caze = (NavigationCase) casesList.get(i);
			String cazeOutcome = caze.getFromOutcome();
			String cazeActionRef = caze.getFromAction();
			if ((cazeOutcome == null || cazeOutcome.equals(outcome))
					&& (cazeActionRef == null || cazeActionRef
							.equals(actionRef))) {
				return caze;
			}
		}
		return null;
	}

	/**
	 * Retorna um Map com todos os casos da navega��o configurados.
	 * 
	 * @param facesContext
	 *            O contexto Faces.
	 * @return Map com os casos da navega��o.
	 */
	public static Map getNavigationCases(FacesContext facesContext) {

		ExternalContext externalContext = facesContext.getExternalContext();
		RuntimeConfig runtimeConfig = RuntimeConfig
				.getCurrentInstance(externalContext);

		Collection rules = runtimeConfig.getNavigationRules();
		int rulesSize = rules.size();
		Map cases = new HashMap(HashMapUtils.calcCapacity(rulesSize));

		for (Iterator iterator = rules.iterator(); iterator.hasNext();) {
			NavigationRule rule = (NavigationRule) iterator.next();
			String fromViewId = rule.getFromViewId();

			if (fromViewId == null) {
				fromViewId = "*";
			} else {
				fromViewId = fromViewId.trim();
			}

			List list = (List) cases.get(fromViewId);
			if (list == null) {
				list = new ArrayList(rule.getNavigationCases());
				cases.put(fromViewId, list);
			} else {
				list.addAll(rule.getNavigationCases());
			}

		}
		return cases;
	}

	/**
	 * 
	 * Obtem o valor do atributo messageKey.
	 * 
	 * @param messageKey
	 *            - Chave da mensagem
	 * @return Mensagem definida pelo atributo messageKey.
	 */
	public static String getFacesMessage(String messageKey) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		FacesMessage facesMessage = MessageUtils.getMessage(facesContext,
				messageKey);
		return facesMessage.getSummary();
	}

	/**
	 * 
	 * Obtem o valor do atributo messageKey, com o valor das variaveis.
	 * 
	 * @param messageKey
	 *            - Chave da mensagem
	 * @param params
	 *            - Array de parametros
	 * @return Mensagem definida pelo atributo messageKey, com as variaveis
	 *         preenchidas.
	 */
	public static String getFacesMessage(String messageKey, Object[] params) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		FacesMessage facesMessage = MessageUtils.getMessage(facesContext,
				messageKey, params);
		return facesMessage.getSummary();
	}

	/**
	 * Insere um novo mensagem de erro GLOBAL no FacesMessages. O mensagem �
	 * obtenido pela sua chave.
	 * 
	 * @param messageKey
	 *            - Chave da mensagem
	 */
	public static void addMessageErrorMessage(String messageKey) {
		addMessageErrorMessage(null, messageKey);
	}

	/**
	 * Insere um novo mensagem de erro no FacesMessages. O mensagem � obtenido
	 * pela sua chave, e associado no compenente correspondente.
	 * 
	 * @param clientId
	 *            - id do componente
	 * @param messageKey
	 *            - Chave da mensagem
	 */
	public static void addMessageErrorMessage(String clientId, String messageKey) {
		String msg = getFacesMessage(messageKey);
		FacesUtils.addErrorMessage(clientId, msg);
	}

	/**
	 * Insere um novo mensagem de erro GLOBAL no FacesMessages. O mensagem �
	 * obtenido pela sua chave, com o valor das variaveis.
	 * 
	 * @param messageKey
	 *            - Chave da mensagem
	 * @param params
	 *            - Array de parametros
	 */
	public static void addMessageErrorMessage(String messageKey, Object[] params) {
		addMessageErrorMessage(null, messageKey, params);
	}

	/**
	 * Insere um novo mensagem de erro no FacesMessages. O mensagem � obtenido
	 * pela sua chave, com o valor das variaveis, e associado no compenente
	 * correspondente.
	 * 
	 * @param clientId
	 *            - id do componente
	 * @param messageKey
	 *            - Chave da mensagem
	 * @param params
	 *            - Array de parametros
	 */
	public static void addMessageErrorMessage(String clientId,
			String messageKey, Object[] params) {
		String msg = getFacesMessage(messageKey, params);
		FacesUtils.addErrorMessage(clientId, msg);
	}

	/**
	 * <p>
	 * Recupera o objeto HttpServletRequest.
	 * </p>
	 * 
	 * @return HttpServletRequest
	 */
	public static HttpServletRequest getServletRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
	}

	/**
	 * Formatar uma string com o n�mero 0 a esquerda de acordo com o tamanho.
	 * 
	 * @param value
	 *            - Valor a ser formatado.
	 * @param size
	 *            - Tamanho do campo.
	 * @return Retorna uma string com zeros � esquerda.
	 */
	public static String formatNumber(String value, int size) {
		String ret = null;
		String valueAux = value;
		if (valueAux == null) {
			valueAux = "";
		}
		if (valueAux.length() > size) {
			ret = valueAux.substring(0, size);
		} else {
			ret = valueAux;
			if (valueAux.length() < size) {
				for (int i = valueAux.length(); i < size; i++) {
					ret = "0" + ret;
				}
			}
		}
		return ret;
	}

	/**
	 * Formatar uma string com o caracter " " a direita de acordo com o tamanho.
	 * 
	 * @param value
	 *            - Valor a ser formatado.
	 * @param size
	 *            - Tamanho do campo.
	 * @return Retorna uma string foramtada.
	 */
	public static String formatString(String value, int size) {
		String ret = null;
		String valueAux = value;
		if (valueAux == null) {
			valueAux = "";
		}
		if (valueAux.length() > size) {
			ret = valueAux.substring(0, size);
		} else {
			ret = valueAux;
			if (valueAux.length() < size) {
				for (int i = valueAux.length(); i < size; i++) {
					ret += " ";
				}
			}
		}
		return ret;
	}

	/**
	 * Formata um bloco de texto autenticado para exibi��o.
	 * 
	 * @param autenticada
	 *            Valor da String de autentica��o.
	 * @return Retorna um objeto da classe <tt>String</tt> com os dados de
	 *         autentica��o prontos para exibi��o.
	 */
	public static String formatarTextoAutenticado2(String autenticada) {
		StringBuffer ret = new StringBuffer();
		ret.append(autenticada.charAt(0));
		for (int i = 1; i < (autenticada.length() - 1); i++) {
			ret.append(autenticada.charAt(i));
		}
		ret.append(autenticada.charAt(autenticada.length() - 1));
		return ret.toString();
	}

	/**
	 * 
	 * Cria um objeto Date a partir de uma String e um padr�o. Exemplo de
	 * pattern: ddMMyyyy.
	 * 
	 * @param date
	 *            Data a ser criada.
	 * @param pattern
	 *            padr�o.
	 * @return Data criada
	 * @throws ParseException
	 *             Caso ocorra algum erro.
	 */
	public static Date stringToDate(String date, String pattern)
			throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.parse(date);
	}

	/**
	 * 
	 * Transforma uma data (Date) em String. Exemplo de pattern: ddMMyyyy.
	 * 
	 * @param date
	 *            Data.
	 * @param pattern
	 *            padr�o desejado.
	 * @return Data formato String.
	 */
	public static String dateToString(Date date, String pattern) {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(date);
	}

	/**
	 * Reseteamos el managed bean
	 * 
	 * @param mBean
	 *            Nome do managed bean.
	 */
	public static void liberaManagedBean(String mBean) {
		try {
			FacesContext.getCurrentInstance().getApplication()
					.createValueBinding("#{" + mBean + "}").setValue(
							FacesContext.getCurrentInstance(), null);
		} catch (Exception e) {
			BradescoCommonServiceFactory.getLogManager().warn(
					SiteUtil.class,
					"No ha sido posible liberamos el managed Bean[" + mBean
							+ "]");
		}

	}

	/**
	 * Objetivo: Verificar se a String � nula ou vazia.
	 * 
	 * @param pSString
	 *            String a ser comparada.
	 * @return <i>true</i>, se a String for nula ou vazia; <i>false</i>, caso
	 *         contr�rio.
	 */
	public static boolean isEmptyOrNull(String pSString) {

		return (pSString == null || pSString.trim().equals(STRING_EMPTY));
	}

	/**
	 * 
	 * M�todo que retorna uma Data ou Hora em um formato espec�fico, definido
	 * pelo cliente, a partir de uma vari�vel de entrada Data e/ou Hora.
	 * 
	 * @param pDateString
	 *            Data ou Hora a ter o formato modificado
	 * @param pPadraoOrigem
	 *            Formato inicial da Data ou Hora. Ex: yyyy/MM/dd-HH:mm:ss
	 * @param pPadraoDestino
	 *            Formato da Data ou Hora a ser retornada
	 * @return String com o formato de data ou hora
	 * @throws ParseException
	 *             caso ocorra um erro na formata��o
	 */
	public static String changeStringDateFormat(String pDateString,
			String pPadraoOrigem, String pPadraoDestino) throws ParseException {

		if (SiteUtil.isEmptyOrNull(pDateString)) {
			return pDateString;
		}

		return SiteUtil.dateToString(SiteUtil.stringToDate(pDateString,
				pPadraoOrigem), pPadraoDestino);
	}

	/**
	 * Nome: isNotNull
	 * 
	 * Verifica se � not null.
	 * 
	 * @param object
	 *            the object
	 * @return true, se for not null
	 * @see
	 */
	public static boolean isNull(Object object) {
		return object == null;
	}

	/**
	 * Nome: isNotNull
	 * 
	 * Verifica se � not null.
	 * 
	 * @param object
	 *            the object
	 * @return true, se for not null
	 * @see
	 */
	public static boolean isNotNull(Object object) {
		return object != null;
	}

	/**
	 * Checks if is equals.
	 * 
	 * @param primeiro
	 *            the primeiro
	 * @param segundo
	 *            the segundo
	 * @return true, if is equals
	 */
	public static boolean isEquals(Object primeiro, Object segundo) {
		return not(isNotEquals(primeiro, segundo));
	}

	/**
	 * Nome: isNotEquals
	 * 
	 * Verifica se � not equals.
	 * 
	 * @param primeiro
	 *            the primeiro
	 * @param segundo
	 *            the segundo
	 * @return true, se for not equals
	 * @see
	 */
	public static boolean isNotEquals(Object primeiro, Object segundo) {

		// decisao
		if ((SiteUtil.isNotNull(primeiro)) && (SiteUtil.isNull(segundo))) {
			return Boolean.TRUE;
		}

		// decisao
		if ((SiteUtil.isNull(primeiro)) && (SiteUtil.isNotNull(segundo))) {
			return Boolean.TRUE;
		}

		// decis�o
		if ((SiteUtil.isNull(primeiro)) && (SiteUtil.isNull(segundo))) {
			return Boolean.FALSE;
		}

		return !primeiro.equals(segundo);
	}
	
	/**
	 * Not.
	 *
	 * @param arg the arg
	 * @return true, if successful
	 */
	public static boolean not(boolean arg) {
		return !arg;
	}
}
