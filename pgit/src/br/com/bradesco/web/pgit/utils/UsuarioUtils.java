/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

/**
 * Nome: UsuarioUtils
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public final class UsuarioUtils {

	/**
	 * Usuario utils.
	 */
	private UsuarioUtils() {
		super();
	}

	/**
	 * Usuario rede to integer.
	 *
	 * @param user the user
	 * @return the int
	 */
	public static int usuarioRedeToInteger(String user){
		return Integer.parseInt(usuarioRedeToString(user));
	}

	/**
	 * Usuario rede to string.
	 *
	 * @param user the user
	 * @return the string
	 */
	public static String usuarioRedeToString(String user){
		String saida = null;
		int a = 'A';
			
		char c = user.toUpperCase().charAt(0);
		
		if (!Character.isDigit(c)){
			int i = c - a + 1;
			saida = i + user.substring(1);
		} else {
			saida = user;
		}
		
		
		return saida;
		
	}

}