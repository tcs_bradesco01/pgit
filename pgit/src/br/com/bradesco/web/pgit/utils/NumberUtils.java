/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;

import br.com.bradesco.web.pgit.utils.constants.LocaleConstants;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: NumberUtils
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public final class NumberUtils {

	/** Atributo DEFAULT_DECIMAL_PATTERN. */
	public static final String DEFAULT_DECIMAL_PATTERN = "#,##0.00";

	/** Atributo INDICE_PATTERN. */
	public static final String INDICE_PATTERN = "#,########0.00000000";

	/** Atributo PHONE_PATTERN. */
	public static final String PHONE_PATTERN = "#00000000";

	/** Atributo HORARIO_PATTERN. */
	public static final String HORARIO_PATTERN = "HH:mm:ss";

	/** Atributo MINIMO_DIGITOS_TELEFONE. */
	private static final int MINIMO_DIGITOS_TELEFONE = 8;
	
	/** Atributo PIS_PASEP_PATTERN. */
	private static final String PIS_PASEP_PATTERN = "000000000000";
	
	/**
	 * Number utils.
	 */
	private NumberUtils() {
		super();
	}
	
	/**
	 * Is number.
	 *
	 * @param number the number
	 * @return true, if is number
	 */
	public static boolean isNumber(String number) {
		return org.apache.commons.lang.math.NumberUtils.isNumber(number);
	}

	/**
	 * Create integer.
	 *
	 * @param number the number
	 * @return the integer
	 */
	public static Integer createInteger(String number) {
		if (number == null || number.trim().equals("") || !isNumber(number)) {
			return null;
		}
		return Integer.parseInt(number);
	}
	
	/**
     * Create big decimal.
     *
     * @param number the number
     * @return the big decimal
     */
     public static BigDecimal createBigDecimal(String number) {
           if (PgitUtil.isStringEmpty(number)) {
                 return BigDecimal.ZERO;
           }

           if (number.contains(",")) {
                 number = number.replace(",", ".");
           }

           return new BigDecimal(number);
     }

     /**
     * Create big decimal.
     *
     * @param number the number
     * @return the big decimal
     */
     public static BigDecimal createBigDecimal(Number number) {
           if (number == null) {
                 return null;
           }

           return createBigDecimal(number.toString());
     }


	
	/**
	 * Create long.
	 *
	 * @param number the number
	 * @return the long
	 */
	public static Long createLong(String number) {
		if (number == null || number.trim().equals("") || !isNumber(number)) {
			return null;
		}
		return Long.parseLong(number);
	}

	/**
	 * Format.
	 *
	 * @param number the number
	 * @param pattern the pattern
	 * @return the string
	 */
	public static String format(Number number, String pattern) {
		if (number == null) {
			return "";
		}
		DecimalFormat formatter = getBrazilianNumberFormat();
		formatter.applyPattern(pattern);
		return formatter.format(number);
	}

	/**
	 * Format.
	 *
	 * @param number the number
	 * @return the string
	 */
	public static String format(Number number) {
		return format(number, DEFAULT_DECIMAL_PATTERN);
	}

	/**
	 * Format diferente zero.
	 *
	 * @param number the number
	 * @return the string
	 */
	public static String formatDiferenteZero(BigDecimal number) {
		if (number.compareTo(BigDecimal.ZERO) == 0) {
			return "";
		}
		return format(number, DEFAULT_DECIMAL_PATTERN);
	}

	/**
	 * Convert.
	 *
	 * @param number the number
	 * @return the big decimal
	 * @throws ParseException the parse exception
	 */
	public static BigDecimal convert(String number) throws ParseException {
		return convert(number, DEFAULT_DECIMAL_PATTERN);
	}

	/**
	 * Convert.
	 *
	 * @param number the number
	 * @param pattern the pattern
	 * @return the big decimal
	 * @throws ParseException the parse exception
	 */
	public static BigDecimal convert(String number, String pattern) throws ParseException {
		if (number == null || number.trim().equals("")) {
			return null;
		}
		DecimalFormat formatter = getBrazilianNumberFormat();
		formatter.applyPattern(pattern);
		formatter.setParseBigDecimal(true);
		return (BigDecimal) formatter.parse(number);
	}

	/**
	 * Parse long.
	 *
	 * @param number the number
	 * @return the long
	 */
	public static Long parseLong(String number) {
		if (number == null || number.trim().equals("")) {
			return null;
		}

		return Long.parseLong(number);
	}

	/**
	 * Parse integer.
	 *
	 * @param number the number
	 * @return the integer
	 */
	public static Integer parseInteger(String number) {
		if (number == null || number.trim().equals("")) {
			return null;
		}

		return Integer.parseInt(number);
	}

	/**
	 * Validate hora.
	 *
	 * @param hora the hora
	 * @return true, if validate hora
	 */
	public static boolean validateHora(String hora) {
		if (hora == null || hora.trim().equals("")) {
			return false;
		}

		return hora.matches("^([01]?[0-9]|2[0-3]):[0-5][0-9]:$");
	}
	
	/**
	 * Get: brazilianNumberFormat.
	 *
	 * @return brazilianNumberFormat
	 */
	private static DecimalFormat getBrazilianNumberFormat() {
		return (DecimalFormat) NumberFormat.getNumberInstance(LocaleConstants.PT_BR);
	}
	
	/**
	 * Formatar fone.
	 *
	 * @param area the area
	 * @param fone the fone
	 * @return the string
	 */
	public static String formatarFone(Integer area, String fone) {
		if (area == null || fone == null) {
			return "";
		}
	    
		if (area.equals(0) || fone.equals("")) {
			return "";	
		} else {
			StringBuffer stringBuffer = new StringBuffer();
			
			String foneFormatado = SiteUtil.formatNumber((fone != null ? fone : ""), 8);
			
			stringBuffer.append("(");
			stringBuffer.append(area.toString());
			stringBuffer.append(") ");
			stringBuffer.append(foneFormatado.substring(0,4));
			stringBuffer.append("-");
			stringBuffer.append(foneFormatado.substring(4,8));
			
			if (area.intValue() == 0 && foneFormatado.equals("00000000")){
			    return "";
			} 

			return stringBuffer.toString();
		}
	}

	/**
	 * Formtar fone sem zero.
	 *
	 * @param telefone the telefone
	 * @return the string
	 */
	public static String formtarFoneSemZero(String telefone) {
		return formtarFoneSemZero(null, telefone);
	}

	/**
	 * Formtar fone sem zero.
	 *
	 * @param ddd the ddd
	 * @param telefone the telefone
	 * @return the string
	 */
	public static String formtarFoneSemZero(Integer ddd, String telefone) {
		if (telefone != null && !telefone.trim().equals("")) {
			
			Long telefoneConvertido = Long.valueOf(telefone);
			int qtdDigitos = telefoneConvertido.toString().length();
			int qtdDigitosMenos4 = qtdDigitos - 4;

			if (qtdDigitos >= MINIMO_DIGITOS_TELEFONE) {
				StringBuilder telefoneFormatado = new StringBuilder();

				if (ddd != null && ddd != 0) {
					telefoneFormatado.append("(");
					telefoneFormatado.append(ddd);
					telefoneFormatado.append(") ");
				}

				telefoneFormatado.append(telefoneConvertido.toString().substring(0, qtdDigitosMenos4));
				telefoneFormatado.append("-");
				telefoneFormatado.append(telefoneConvertido.toString().substring(qtdDigitosMenos4));

				return telefoneFormatado.toString();
			}
		}

		return "";
	}

	/**
	 * Formatar fone.
	 *
	 * @param fone the fone
	 * @return the string
	 */
	public static String formatarFone(String fone) {
		StringBuffer stringBuffer = new StringBuffer();
		
		String foneFormatado = SiteUtil.formatNumber((fone != null ? fone : ""), 8);
		
		stringBuffer.append(foneFormatado.substring(0,4));
		stringBuffer.append("-");
		stringBuffer.append(foneFormatado.substring(4,8));

		return stringBuffer.toString();
	}

	/**
	 * Convert telefone sem ddd.
	 *
	 * @param telefone the telefone
	 * @return the long
	 */
	public static Long convertTelefoneSemDdd(String telefone) {
		if (telefone == null || telefone.trim().equals("")) {
			return null;
		}

		return createLong(telefone.replace("-", ""));
	}

	/**
	 * Format telefone sem ddd.
	 *
	 * @param telefone the telefone
	 * @return the string
	 */
	public static String formatTelefoneSemDdd(Long telefone) {
		if (telefone == null || telefone.equals(0L)) {
			return "";
		}

		String result = format(telefone, PHONE_PATTERN);

		StringBuilder numTelefoneFormatado = new StringBuilder();
		numTelefoneFormatado.append(result.substring(0, result.length() - 4));
		numTelefoneFormatado.append("-");
		numTelefoneFormatado.append(result.substring(result.length() - 4));
		return numTelefoneFormatado.toString();
	}

	/**
	 * Format telefone sem ddd.
	 *
	 * @param telefone the telefone
	 * @return the string
	 */
	public static String formatTelefoneSemDdd(String telefone) {
		return formatTelefoneSemDdd(createLong(telefone));
	}

	/**
	 * Convert volume.
	 *
	 * @param volume the volume
	 * @return the long
	 */
	public static Long convertVolume(String volume) {
		if (volume == null || volume.trim().equals("")) {
			return null;
		}

		return createLong(volume.replace(".", ""));
	}

	/**
	 * Convert volume.
	 *
	 * @param volume the volume
	 * @return the Integer
	 */
    public static Integer convertIntegerVolume(String volume) {
        if (volume == null || volume.trim().equals("")) {
              return null;
        }
        return createInteger(volume.replaceAll("[^0123456789]", ""));
  }
	
	/**
	 * Format volume.
	 *
	 * @param volume the volume
	 * @return the string
	 */
	public static String formatVolume(Long volume) {
		String result = format(volume, "");

		if ("0".equals(result)) {
			return "";
		}

		return result.replace(".", ",");
	}

	/**
	 * Format volume.
	 *
	 * @param volume the volume
	 * @return the string
	 */
	public static String formatVolume(String volume) {
		if(volume == null) {
			return "";
		}
		return formatVolume(createLong(volume.replace(".", "")));
	}
	
	/**
	 * Format telefone9 digitos.
	 *
	 * @param ddd the ddd
	 * @param numero the numero
	 * @return the string
	 */
	public static String formatTelefone9Digitos(String ddd, String numero) {
		if (ddd == null || ddd.trim().equals("") || numero == null || numero.trim().equals("")) {
			return null;
		}

		return formatTelefone9Digitos(createInteger(ddd), createLong(numero));
	}

	/**
	 * Format telefone9 digitos.
	 *
	 * @param ddd the ddd
	 * @param numero the numero
	 * @return the string
	 */
	public static String formatTelefone9Digitos(Integer ddd, Long numero) {
		if (ddd == null || numero == null || numero <= 0) {
			return null;
		}

		String numTelefoneFormatado = format(numero, PHONE_PATTERN);

		StringBuilder telefone = new StringBuilder();
		telefone.append("(");
		telefone.append(ddd);
		telefone.append(") ");
		telefone.append(numTelefoneFormatado.substring(0, numTelefoneFormatado.length() - 4));
		telefone.append("-");
		telefone.append(numTelefoneFormatado.substring(numTelefoneFormatado.length() - 4));
		return telefone.toString();
	}
	
	/**
	 * Formatar numero pis pasep.
	 *
	 * @param nrPisPasep the nr pis pasep
	 * @return the string
	 */
	public static String formatarNumeroPisPasep(Long nrPisPasep){
	    if(nrPisPasep != null && nrPisPasep != 0L){
	        String strPisPasep = format(nrPisPasep, PIS_PASEP_PATTERN);
	        
	        StringBuilder nrPisPasepFormatado = new StringBuilder();
	        
	        nrPisPasepFormatado.append(strPisPasep.substring(0, strPisPasep.length() - 9));
	        nrPisPasepFormatado.append(".");
	        nrPisPasepFormatado.append(strPisPasep.substring(strPisPasep.length() - 9, strPisPasep.length() - 4));
	        nrPisPasepFormatado.append(".");
	        nrPisPasepFormatado.append(strPisPasep.substring(strPisPasep.length() - 4, strPisPasep.length() - 2));
	        nrPisPasepFormatado.append("-");
	        nrPisPasepFormatado.append(strPisPasep.substring(strPisPasep.length() - 2));
	        return nrPisPasepFormatado.toString();
	    }
	    return "";
	}

	/**
	 * Formatar horario.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String formatarHorario(String data) {
		return DateUtils.formartarData(FormatarData.formataTimestampFromPdc(data), HORARIO_PATTERN);
	}

	/**
	 * Formatar horario.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String formatarHorario(Date data) {
		return DateUtils.formartarData(data, HORARIO_PATTERN);
	}
	
	/**
     * Checks if is number not zero.
     *
     * @param number the number
     * @return true, if is number not zero
     */
    public static boolean isNumberNotZero(Number number) {
        return number != null && number.doubleValue() != 0;
    }
}