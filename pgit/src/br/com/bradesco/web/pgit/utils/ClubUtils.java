/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

/**
 * Nome: ClubUtils
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public final class ClubUtils {

	/**
	 * Club utils.
	 */
	private ClubUtils() {
		super();
	}

	/**
	 * Format club.
	 *
	 * @param club the club
	 * @return the string
	 */
	public static String formatClub(Integer club) {
		StringBuffer clubFormatado = new StringBuffer();
		clubFormatado.append(NumberUtils.format(club, "00000000000"));
		return club.toString();
	}

}
