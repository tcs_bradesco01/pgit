/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

/**
 * Nome: OrdenarLista
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public abstract class OrdenarLista {
	
	/** Atributo fieldSort. */
	private String fieldSort;
    
    /** Atributo fieldAscending. */
    private boolean fieldAscending;

    /**
     * Ordenar lista.
     *
     * @param defaultSortColumn the default sort column
     */
    protected OrdenarLista(String defaultSortColumn)
    {
        fieldSort = defaultSortColumn;
        fieldAscending = isDefaultAscending(defaultSortColumn);
    }

    /**
     * Sort the list.
     */
    protected abstract void sort(String column, boolean ascending);

    /**
     * Is the default sort direction for the given column "ascending" ?
     */
    protected abstract boolean isDefaultAscending(String sortColumn);


    /**
     * Sort.
     *
     * @param sortColumn the sort column
     */
    public void sort(String sortColumn)
    {
        if (sortColumn == null)
        {
            throw new IllegalArgumentException("Argument sortColumn must not be null.");
        }

        if (fieldSort.equals(sortColumn))
        {
            //current sort equals new sortColumn -> reverse sort order
            fieldAscending = !fieldAscending;
        }
        else
        {
            //sort new column in default direction
            fieldSort = sortColumn;
            fieldAscending = isDefaultAscending(fieldSort);
        }

        sort(fieldSort, fieldAscending);
    }

    /**
     * Get: sort.
     *
     * @return sort
     */
    public String getSort()
    {
        return fieldSort;
    }

    /**
     * Set: sort.
     *
     * @param sort the sort
     */
    public void setSort(String sort)
    {
        fieldSort = sort;
    }

    /**
     * Is ascending.
     *
     * @return true, if is ascending
     */
    public boolean isAscending()
    {
        return fieldAscending;
    }

    /**
     * Set: ascending.
     *
     * @param ascending the ascending
     */
    public void setAscending(boolean ascending)
    {
        fieldAscending = ascending;
    }
}
