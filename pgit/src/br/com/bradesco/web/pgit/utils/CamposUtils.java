/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

/**
 * Nome: CamposUtils
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public final class CamposUtils {
	
	/**
	 * Campos utils.
	 */
	private CamposUtils() {
		super();
	}

	/**
	 * Validar campo.
	 *
	 * @param value the value
	 * @return true, if validar campo
	 */
	public static boolean validarCampo (Object value){
		
		if( value == null ){
			return true;
		}
		
		if ((value instanceof String) && (value.equals("") )) {
			return true;
		}
		
		if ((value instanceof Integer) && (value.equals(0) )) {
			return true;
		}
		
		if ((value instanceof Long) && (value.equals(0L) )) {
			return true;
		}
		
		return false;
		
	}
	
	/**
	 * Formatador campos.
	 *
	 * @param cod the cod
	 * @param descricao the descricao
	 * @return the string
	 */
	public static String formatadorCampos(Integer cod, String descricao) {
		
		if ( cod == null || cod.equals(0)) {
			return "";
		}
		
		return cod.toString() + " - " + descricao;
	}
	

}
