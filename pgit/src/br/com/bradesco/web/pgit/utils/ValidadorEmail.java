/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 03/06/2015
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Nome: ValidadorEmail
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public final class ValidadorEmail {

	/** Atributo EMAIL_PATTERN. */
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * Validador email.
	 */
	private ValidadorEmail() {
		super();
	}

	/**
	 * Validar.
	 * 
	 * @param email
	 *            the email
	 * @return true, if validar
	 */
	public static boolean validar(final String email) {
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
}