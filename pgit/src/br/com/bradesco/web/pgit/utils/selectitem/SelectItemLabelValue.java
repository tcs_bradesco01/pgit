/*
 * Nome: br.com.bradesco.web.pgit.utils.selectitem
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils.selectitem;

/**
 * Nome: SelectItemLabelValue
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @param <T> the generic type
 * @author : todo!
 * @version :
 */
public interface SelectItemLabelValue<T> {

	/**
	 * Get: label.
	 *
	 * @param item the item
	 * @return label
	 */
	String getLabel(T item);

	/**
	 * Get: value.
	 *
	 * @param item the item
	 * @return value
	 */
	Object getValue(T item);
}
