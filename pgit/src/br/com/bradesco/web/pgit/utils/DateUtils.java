/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.bradesco.web.pgit.utils.constants.LocaleConstants;
import br.com.bradesco.web.pgit.utils.exception.PgitFormatException;

/**
 * Nome: DateUtils
 * <p>
 * Prop�sito:
 * </p>.
 * @param :
 * @return:
 * @throws:
 * @see DateUtils
 * @author : todo!
 * @version :
 */
public final class DateUtils implements LocaleConstants {

	/** Atributo PATTERN_POR_EXTENSO. */
	private static final String PATTERN_POR_EXTENSO = "dd 'de' MMMMM 'de' yyyy";
	
    /** The Constant LOCALE_BR. */
    private static final Locale LOCALE_BR = new Locale("pt", "BR");

	/**
	 * Date utils.
	 */
	private DateUtils() {
		super();
	}

	/**
	 * Formartar data pd cpara padra pt br.
	 *
	 * @param vlData the vl data
	 * @param formatoPdc the formato pdc
	 * @param formatoPtBr the formato pt br
	 * @return the string
	 */
	public static String formartarDataPDCparaPadraPtBr(String vlData,
			String formatoPdc, String formatoPtBr) {
		SimpleDateFormat format = new SimpleDateFormat(formatoPdc, PT_BR);

		Date data = null;

		try {
			data = format.parse(vlData);
		} catch (ParseException e) {
			return "";
		}

		format = new SimpleDateFormat(formatoPtBr, PT_BR);

		return format.format(data);
	}

	/**
	 * Formartar data.
	 *
	 * @param vlData the vl data
	 * @param formato the formato
	 * @return the string
	 */
	public static String formartarData(Date vlData, String formato) {
		if (vlData != null) {
			SimpleDateFormat format = new SimpleDateFormat(formato, PT_BR);
			return format.format(vlData);
		}
		return "";
	}

	/**
	 * Trocar separador de data.
	 *
	 * @param vlData the vl data
	 * @param atual the atual
	 * @param nova the nova
	 * @return the string
	 */
	public static String trocarSeparadorDeData(String vlData, String atual,
			String nova) {
		return vlData.replace(atual, nova);
	}

	/**
	 * Formatar data por extenso.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String formatarDataPorExtenso(Date data) {
		SimpleDateFormat formatoSaida = new SimpleDateFormat(
				PATTERN_POR_EXTENSO, LocaleConstants.PT_BR);
		return formatoSaida.format(data);
	}
	
    /**
     * Nome: formataData.
     * 
     * @param data
     *            the data
     * @param mask
     *            the mask
     * @return the date
     * @exception
     * @see
     */
    public static Date formataData(String data, String mascara) {
        Date date = null;
        DateFormat formatter = null;

        // verifica se data � diferente de null e vazio
        if (data == null || data.trim().equals("")) {
            return null;
        }

        try {
            formatter = new SimpleDateFormat(mascara, LOCALE_BR);
            date = (java.util.Date) formatter.parse(data);
        } catch (ParseException e) {
            throw new PgitFormatException(e.getMessage(), e);
        }
        return date;
    }
    
    /**
     * Nome: formataData.
     * 
     * @param data
     *            the data
     * @param mascara
     *            the mask
     * @return the string
     * @see
     */
    public static String formataData(Date data, String mascara) {
        SimpleDateFormat formatoSaida = null;
        if (data != null) {
            formatoSaida = new SimpleDateFormat(mascara, LOCALE_BR);
            return formatoSaida.format(data);
        }
        return "";
    }



}
