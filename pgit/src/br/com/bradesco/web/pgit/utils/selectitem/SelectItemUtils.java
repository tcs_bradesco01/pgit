/*
 * Nome: br.com.bradesco.web.pgit.utils.selectitem
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils.selectitem;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

/**
 * Nome: SelectItemUtils
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public final class SelectItemUtils {

	/**
	 * Select item utils.
	 */
	private SelectItemUtils() {
		super();
	}

	/**
	 * Obter indice selec item.
	 *
	 * @param lista the lista
	 * @param valor the valor
	 * @return the int
	 */
	public static int obterIndiceSelecItem(List<SelectItem> lista, Object valor) {
		if (lista != null && valor != null) {
			for (int i = 0; i < lista.size(); i++) {
				SelectItem item = lista.get(i);
				if (valor.equals(item.getValue())) {
					return i;
				}
			}
		}

		return -1;
	}

	/**
	 * Obter label selec item.
	 *
	 * @param lista the lista
	 * @param valor the valor
	 * @return the string
	 */
	public static String obterLabelSelecItem(List<SelectItem> lista, Object valor) {
		int indice = obterIndiceSelecItem(lista, valor);
		if (indice >= 0) {
			return lista.get(indice).getLabel();
		}
		return "";
	}

	/**
	 * Obter value select item.
	 *
	 * @param lista the lista
	 * @param label the label
	 * @return the object
	 */
	public static Object obterValueSelectItem(List<SelectItem> lista, String label) {
		if(lista != null && label != null) {
			for(SelectItem item : lista) {
				if(label.equals(item.getLabel())) {
					return item.getValue();
				}
			}
		}
		return null;
	}
	
	/**
	 * Criar select items.
	 *
	 * @param lista the lista
	 * @return the list< select item>
	 */
	public static List<SelectItem> criarSelectItems(List<?> lista) {
		List<SelectItem> listaSelectItems = new ArrayList<SelectItem>();
		if (lista != null) {
			for (int i = 0; i < lista.size(); i++) {
				listaSelectItems.add(new SelectItem(i, ""));
			}
		}
		return listaSelectItems;
	}

	/**
	 * Criar select items.
	 *
	 * @param <T> the generic type
	 * @param lista the lista
	 * @param item the item
	 * @return the list< select item>
	 */
	public static <T> List<SelectItem> criarSelectItems(List<T> lista, SelectItemLabel<T> item) {
		List<SelectItem> listaSelectItems = new ArrayList<SelectItem>();
		if (lista != null) {
			for (int i = 0; i < lista.size(); i++) {
				T current = lista.get(i);
				listaSelectItems.add(new SelectItem(i, item.getLabel(current)));
			}
		}
		return listaSelectItems;
	}

	/**
	 * Criar select items.
	 *
	 * @param <T> the generic type
	 * @param lista the lista
	 * @param item the item
	 * @return the list< select item>
	 */
	public static <T> List<SelectItem> criarSelectItems(List<T> lista, SelectItemLabelValue<T> item) {
		List<SelectItem> listaSelectItems = new ArrayList<SelectItem>();
		if (lista != null) {
			for (T current : lista) {
				listaSelectItems.add(new SelectItem(item.getValue(current), item.getLabel(current)));
			}
		}
		return listaSelectItems;
	}
}
