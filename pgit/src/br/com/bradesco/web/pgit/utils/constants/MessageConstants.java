/*
 * Nome: br.com.bradesco.web.pgit.utils.constants
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils.constants;

/**
 * Nome: MessageConstants
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface MessageConstants {

	/** Atributo COD_MENSAGEM_SUCESSO. */
	String COD_MENSAGEM_SUCESSO = "";

	/** Atributo INCLUSAO_SUCESSO. */
	String INCLUSAO_SUCESSO = "inclusao_sucesso";

	/** Atributo ALTERACAO_SUCESSO. */
	String ALTERACAO_SUCESSO = "alteracao_sucesso";

	/** Atributo EXCLUSAO_SUCESSO. */
	String EXCLUSAO_SUCESSO = "exclusao_sucesso";
}
