/*
 * Nome: br.com.bradesco.web.pgit.utils.constants
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils.constants;

import java.util.Locale;

/**
 * Nome: LocaleConstants
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public interface LocaleConstants {

	/** Atributo PT_BR. */
	Locale PT_BR = new Locale("pt", "BR"); 
}
