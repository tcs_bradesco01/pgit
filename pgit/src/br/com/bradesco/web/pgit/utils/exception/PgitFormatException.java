/*
 * Nome: br.com.bradesco.web.pgit.utils.exception
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils.exception;

import br.com.bradesco.web.aq.application.error.BradescoApplicationException;

/**
 * Lan�ada quando ocorre erro de formata��o.
 */
public class PgitFormatException extends BradescoApplicationException {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = 4187282486532644969L;

	/**
	 * Pgit format exception.
	 *
	 * @param message the message
	 * @param exception the exception
	 */
	public PgitFormatException(String message, Throwable exception) {
		super(message, exception);
	}

	/**
	 * Pgit format exception.
	 *
	 * @param message the message
	 */
	public PgitFormatException(String message) {
		super(message);
	}
}
