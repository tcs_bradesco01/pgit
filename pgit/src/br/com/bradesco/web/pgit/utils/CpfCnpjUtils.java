/*
 * Nome: br.com.bradesco.web.pgit.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.utils;

import java.text.NumberFormat;

import br.com.bradesco.web.pgit.utils.constants.LocaleConstants;

/**
 * Nome: CpfCnpjUtils
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public final class CpfCnpjUtils {
	
	/** Atributo FILIAL. */
	private static final String FILIAL = "0000";

	/** Atributo CONTROLE. */
	private static final String CONTROLE = "00";
	
	/** Atributo PRINCIPAL_CPF. */
	private static final String PRINCIPAL_CPF = "000,000,000";

	/** Atributo PRINCIPAL_CNPJ. */
	private static final String PRINCIPAL_CNPJ = "#00,000,000";

	/**
	 * Cpf cnpj utils.
	 */
	private CpfCnpjUtils() {
		super();
	}

	/**
	 * Formatar cpf cnpj.
	 *
	 * @param cpfCpnj the cpf cpnj
	 * @param cpfCpnjFilial the cpf cpnj filial
	 * @param cpfCpnjControle the cpf cpnj controle
	 * @return the string
	 */
	public static String formatarCpfCnpj(Long cpfCpnj, Integer cpfCpnjFilial, Integer cpfCpnjControle) {
		if (cpfCpnj == null && cpfCpnjFilial == null && cpfCpnjControle == null) {
			return "";
		}

		StringBuilder cStr = new StringBuilder();
		if (cpfCpnjFilial != null && cpfCpnjFilial == 0) { // CPF
			cStr.append(NumberUtils.format(cpfCpnj, PRINCIPAL_CPF));
			cStr.append("-");
			cStr.append(NumberUtils.format(cpfCpnjControle, CONTROLE));
		} else { // CNPJ
			cStr.append(NumberUtils.format(cpfCpnj, PRINCIPAL_CNPJ));
			cStr.append("/");
			cStr.append(NumberUtils.format(cpfCpnjFilial, FILIAL));
			cStr.append("-");
			cStr.append(NumberUtils.format(cpfCpnjControle, CONTROLE));
		}
		return cStr.toString();
	}
	
	/**
	 * Format cpf cnpj.
	 *
	 * @param identificador the identificador
	 * @param codigo the codigo
	 * @return the string
	 */
	public static String formatCpfCnpj(Long identificador, Integer codigo) {
	    	if (codigo == null || identificador == null){
	    	    return "";
	    	}
	    	
		StringBuffer cnpjOuCpf = new StringBuffer();

		NumberFormat numberFormat = NumberFormat.getIntegerInstance(LocaleConstants.PT_BR);
		
		if (codigo.equals(1)) {
			String cpf = identificador.toString();
			cpf = SiteUtil.formatNumber((identificador != null ? identificador.toString() : ""), 11);
			String parte1 = cpf.substring(0, 9);
			String controle = cpf.substring(9, 11);
			
			cnpjOuCpf.append(numberFormat.format(new Long(parte1)));
			cnpjOuCpf.append("-");
			cnpjOuCpf.append(controle);
			return cnpjOuCpf.toString();
		} else {
		    	
		    	String cnpjCompleto = SiteUtil.formatNumber((identificador != null ? identificador.toString() : ""), 15);
		    	
			String cnpj = cnpjCompleto.substring(0, 9);
			String filial = cnpjCompleto.substring(9, 13);
			String controle = cnpjCompleto.substring(13, 15);
			
			cnpjOuCpf.append(numberFormat.format(new Long(cnpj)));
			cnpjOuCpf.append("/");
			cnpjOuCpf.append(filial);
			cnpjOuCpf.append("-");
			cnpjOuCpf.append(controle);
			return cnpjOuCpf.toString();
		}
		
		
	}
	
	/**
	 * Format cpf cnpj completo.
	 *
	 * @param cnpjCpf the cnpj cpf
	 * @param filial the filial
	 * @param controle the controle
	 * @return the string
	 */
	public static String formatCpfCnpjCompleto(Long cnpjCpf, Integer filial, Integer controle) {
		StringBuffer cnpjOuCpf = new StringBuffer();
		
		if (cnpjCpf == null || controle == null){
			return "";
		}
		
		String base = PgitUtil.complementaDigito(String.valueOf(cnpjCpf), 9);
		
		base = base.substring(0, 3).concat(".").concat(base.substring(3,6)).concat(".").concat(base.substring(6,9));
		
		
		/*NumberFormat numberFormat = NumberFormat.getIntegerInstance(LocaleConstants.PT_BR);	
		
		
		String cnpjCpfAux = numberFormat.format(cnpjCpf);
		
		String[] partesCnpj = cnpjCpfAux.split("\\.");
		
		
		if (partesCnpj.length >= 3 && partesCnpj[2] != null){
			cnpjCpfAux = cnpjCpfAux.concat(".").concat(String.format("%03d",new Long(partesCnpj[2].trim())));
		}else{
			cnpjCpfAux = cnpjCpfAux.concat(".").concat("000");
		}
		
		if (partesCnpj.length >= 1 && partesCnpj[0] != null){
			cnpjCpfAux = String.format("%03d",new Long(partesCnpj[0].trim()));
		}else{
			cnpjCpfAux = "000";
		}
		
		if (partesCnpj.length >= 2 &&  partesCnpj[1] != null){
			cnpjCpfAux = cnpjCpfAux.concat(".").concat(String.format("%03d",new Long(partesCnpj[1].trim())));
		}else{
			cnpjCpfAux = cnpjCpfAux.concat(".").concat("000");
		}*/
		
		
		
		
		
			
		if (filial.equals(0)) {
			cnpjOuCpf.append(base);
			cnpjOuCpf.append("-");
			cnpjOuCpf.append(String.format("%02d", controle));
			return cnpjOuCpf.toString();
		} else {
			cnpjOuCpf.append(base);
			cnpjOuCpf.append("/");
			cnpjOuCpf.append(String.format("%04d", filial));
			cnpjOuCpf.append("-");
			cnpjOuCpf.append(String.format("%02d", controle));
			return cnpjOuCpf.toString();
		}
	}
	
	/**
	 * Format cpf cnpj.
	 *
	 * @param identificador the identificador
	 * @param codigo the codigo
	 * @return the string
	 */
	public static String formatCpfCnpj(String identificador, Integer codigo) {
		StringBuffer cnpjOuCpf = new StringBuffer();

		//NumberFormat numberFormat = NumberFormat.getIntegerInstance(LocaleConstants.PT_BR);
		
		if (codigo.equals(1)) {
			String cpf1 = identificador.substring(0, 3);
			String cpf2 = identificador.substring(3, 6);
			String cpf3 = identificador.substring(6, 9);
			String controle = identificador.substring(13, 15);
			
			cnpjOuCpf.append(cpf1);
			cnpjOuCpf.append(".");
			cnpjOuCpf.append(cpf2);
			cnpjOuCpf.append(".");
			cnpjOuCpf.append(cpf3);
			cnpjOuCpf.append("-");
			cnpjOuCpf.append(controle);
			return cnpjOuCpf.toString();
		} else {
			String cnpj1 = identificador.substring(0, 3);
			String cnpj2 = identificador.substring(3,6);
			String cnpj3 = identificador.substring(6, 9);
			String filial = identificador.substring(9, 13);
			String controle = identificador.substring(13, 15);
			
			cnpjOuCpf.append(cnpj1);
			cnpjOuCpf.append(".");
			cnpjOuCpf.append(cnpj2);
			cnpjOuCpf.append(".");
			cnpjOuCpf.append(cnpj3);
			cnpjOuCpf.append("/");
			cnpjOuCpf.append(filial);
			cnpjOuCpf.append("-");
			cnpjOuCpf.append(controle);
			return cnpjOuCpf.toString();
		}
	}
}
