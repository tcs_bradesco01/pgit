/*
exibeOrigemSegundaViaExtrato * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

import static br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoServiceConstants.COD_IND_MOMENTO_DEBITO_PGTO_S;
import static br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoServiceConstants.DISPONIBILIZACAO_PAGAMENTO;
import static br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoServiceConstants.INC_SERV_MANT_CONTR_MAX_MODALIDADE;
import static br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoServiceConstants.SEM_CONSULTA;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;
import static br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.FeriadoNacional.ACATAR;
import static br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.FeriadoNacional.ANTECIPAR;
import static br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.FeriadoNacional.POSTERGAR;
import static br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.FeriadoNacional.REJEITAR;
import static br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.TipoProcessamento.CICLICO;
import static br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.TipoProcessamento.DIARIO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.IComboServiceConstants;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarListaValoresDiscretosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarListaValoresDiscretosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListaTipoServicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListaTipoServicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServModEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoProcessamentoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoProcessamentoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.PeriodicidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.PeriodicidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.DetalharAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.DetalharAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ListarAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ListarAmbienteOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarConfiguracaoTipoServModContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarConfiguracaoTipoServModContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManAmbPartEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManAmbPartSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManServicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.DetalharTipoServModContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.DetalharTipoServModContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirTipoServModContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirTipoServModContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirRepresentanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirRepresentanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirTipoServModContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirTipoServModContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarAgregadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarAgregadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManAmbPartEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManAmbPartOcorrenciatSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarModalidadeTipoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarModalidadeTipoServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipanteAgregadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipanteAgregadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarRepresentanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarRepresentanteOcorrenciaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarRepresentanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarServicoRelacionadoCdpsEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarServicoRelacionadoCdpsSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarVinculacaoConvenioContaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarVinculacaoConvenioContaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.VerificarAtributosServicoModalidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.VerificarAtributosServicoModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirPagamentoSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirPagamentoSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.IManterVinculacaoConvenioContaSalarioService;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnListaOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.selectitem.SelectItemUtils;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ServicosBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ServicosBean {

    /** Atributo TIPO_SERVICO_PAGAMENTO_FORNECEDOR. */
    private static final Integer TIPO_SERVICO_PAGAMENTO_FORNECEDOR = 1;

    /** Atributo TIPO_SERVICO_PAGAMENTO_SALARIO. */
    private static final Integer TIPO_SERVICO_PAGAMENTO_SALARIO = 2;

    /** Atributo TIPO_SERVICO_PAGAMENTO_TRIBUTOS. */
    private static final Integer TIPO_SERVICO_PAGAMENTO_TRIBUTOS = 3;

    /** Atributo TIPO_SERVICO_PAGAMENTO_TRIBUTOS. */
    private static final Integer TIPO_SERVICO_PAGAMENTO_BENEFICIOS = 4;
    
    private static final String DOIS_STR = "2";

    /** Atributo comboService. */
    private IComboService comboService = null;

    /** Atributo manterContratoService. */
    private IManterContratoService manterContratoService = null;

    /** Atributo manterVinculacaoConveioSalarioService. */
    private IManterVinculacaoConvenioContaSalarioService manterVinculacaoConvenioContaSalarioService = null;

    /** Atributo identificacaoClienteContratoBean. */
    private IdentificacaoClienteContratoBean identificacaoClienteContratoBean = new IdentificacaoClienteContratoBean();

    /** Atributo manterAmbienteOperacaoContratoServiceImpl. */
    private IManterAmbienteOperacaoContratoService manterAmbienteOperacaoContratoServiceImpl = null;

    /** Atributo manterVincPerfilTrocaArqPartImpl. */
    private IManterVincPerfilTrocaArqPartService manterVincPerfilTrocaArqPartImpl = null;

    /** Atributo manterContratoBean. */
    private ManterContratoBean manterContratoBean = new ManterContratoBean();

    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica = null;

    /** Atributo cdTipoContrato. */
    private Integer cdTipoContrato = null;

    /** Atributo nrSequenciaContrato. */
    private Long nrSequenciaContrato = null;

    /** Atributo listaGridPesquisa. */
    private List<ListarModalidadeTipoServicoSaidaDTO> listaGridPesquisa = null;

    /** Atributo listaGridPesquisa. */
    private List<ListarContratosPgitSaidaDTO> listaContratosPgit;

    /** Atributo itemSelecionadoLista. */
    private Integer itemSelecionadoLista = null;

    /** Atributo itemSelecionadoSacadoEletronico. */
    private Integer itemSelecionadoSacadoEletronico = null;

    /** Atributo listaControleSacadoEletronico. */
    private List<SelectItem> listaControleSacadoEletronico = null;

    /** Atributo listaGridSacadoEletronico. */
    private List<ListarParticipanteAgregadoSaidaDTO> listaGridSacadoEletronico = new ArrayList<ListarParticipanteAgregadoSaidaDTO>();

    /** Atributo listaGridAgregados. */
    private List<ListarAgregadoSaidaDTO> listaGridAgregados = new ArrayList<ListarAgregadoSaidaDTO>();

    /** Atributo listaControleRadio. */
    private List<SelectItem> listaControleRadio = null;

    /** Atributo listaControleRadio. */
    private List<SelectItem> listaControleRadioDadosConta = null;

    /** Atributo modalidadeTipoServicoSaidaDTO. */
    private ListarModalidadeTipoServicoSaidaDTO modalidadeTipoServicoSaidaDTO = null;

    /** The entrada listar dados cta convn. */
    private ListarDadosCtaConvnEntradaDTO entradaListarDadosCtaConvn = null;

    /** The saida listar dados cta convn. */
    private ListarDadosCtaConvnSaidaDTO saidaListarDadosCtaConvn = null;

    /** The lista saida listar dados cta convn. */
    private List<ListarDadosCtaConvnListaOcorrenciasDTO> listaSaidaListarDadosCtaConvn = null;

    /** Atributo cdTipoServico. */
    private Integer cdTipoServico = null;

    /** Atributo cdModalidade. */
    private int cdModalidade = 0;

    /** Atributo cdParametroTela. */
    private int cdParametroTela = 0;

    /** Atributo cdParametroTela2. */
    private Integer cdParametroTela2 = null;

    /** Atributo cdFormaEnvioPagamento. */
    private Integer cdFormaEnvioPagamento = null;

    /** Atributo cdFormaEnvioPagamento. */
    private Integer codTela = null;

    /** Atributo cdFormaAutPagamento. */
    private Integer cdFormaAutPagamento = null;

    /** Atributo cdUtilizaPreAut. */
    private Integer cdUtilizaPreAut = null;

    /** Atributo cdTipoContFloating. */
    private Integer cdTipoContFloating = null;

    /** Atributo cdEmiteCartao. */
    private Integer cdEmiteCartao = null;

    /** Atributo cdTipoCartao. */
    private Integer cdTipoCartao = null;

    /** Atributo qtddLimite. */
    private String qtddLimite = null;

    /** Atributo dataLimite. */
    private Date dataLimite = null;

    /** Atributo cdUtilizacaoRastreamentoDebito. */
    private Integer cdUtilizacaoRastreamentoDebito = null;

    /** Atributo cdPeriodicidadeRastreamento. */
    private Integer cdPeriodicidadeRastreamento = null;

    /** Atributo dsPeriodicidadeRastreamento. */
    private String dsPeriodicidadeRastreamento = null;

    /** Atributo cdAgendDebito. */
    private Integer cdAgendDebito = null;

    /** Atributo cdPeriodicidadeCobranca. */
    private Integer cdPeriodicidadeCobranca = null;

    /** Atributo dsPeriodicidadeCobranca. */
    private String dsPeriodicidadeCobranca = null;

    /** Atributo diaFechamentoApuracao. */
    private String diaFechamentoApuracao = null;

    /** Atributo qtddDiasCobrancaTarifa. */
    private String qtddDiasCobrancaTarifa = null;

    /** Atributo cdPeriodicidadeReajusteTarifa. */
    private Integer cdPeriodicidadeReajusteTarifa = null;

    /** Atributo dsPeriodicidadeReajusteTarifa. */
    private String dsPeriodicidadeReajusteTarifa = null;

    /** Atributo cdTipoReajusteTarifa. */
    private Integer cdTipoReajusteTarifa = null;

    /** Atributo qtddMesesReajuste. */
    private String qtddMesesReajuste = null;

    /** Atributo indiceEconomicoReajuste. */
    private String indiceEconomicoReajuste = null;

    /** Atributo tipoAlimentacao. */
    private String tipoAlimentacao = null;

    /** Atributo cdTipoAutorizacaoPagamento. */
    private Integer cdTipoAutorizacaoPagamento = null;

    /** Atributo cdInformaAgenciaContaCredito. */
    private Integer cdInformaAgenciaContaCredito = null;

    /** Atributo cdTipoIdenBeneficio. */
    private Integer cdTipoIdenBeneficio = null;

    /** Atributo cdUtilizaCadOrgPagadores. */
    private Integer cdUtilizaCadOrgPagadores = null;

    /** Atributo cdManCadastroProcuradores. */
    private Integer cdManCadastroProcuradores = null;

    /** Atributo cdPeriodicidadeManutencao. */
    private Integer cdPeriodicidadeManutencao = null;

    /** Atributo dsPeriodicidadeManutencao. */
    private String dsPeriodicidadeManutencao = null;

    /** Atributo cdFormaManutencaoCadastroProcurador. */
    private Integer cdFormaManutencaoCadastroProcurador = null;

    /** Atributo cdPossuiExpiracaoCredito. */
    private Integer cdPossuiExpiracaoCredito = null;

    /** Atributo cdFormaControleExpiracao. */
    private Integer cdFormaControleExpiracao = null;

    /** Atributo qtddDiasExpiracao. */
    private String qtddDiasExpiracao = null;

    /** Atributo cdTratFeriadoFimVigencia. */
    private Integer cdTratFeriadoFimVigencia = null;

    /** Atributo cdTipoPrestContaCreditoPago. */
    private Integer cdTipoPrestContaCreditoPago = null;

    /** Atributo cdFormaManutencaoCadastro. */
    private Integer cdFormaManutencaoCadastro = null;

    /** Atributo qtddDiasInativacao. */
    private String qtddDiasInativacao = null;

    /** Atributo cdConsistenciaCpfCnpfFavorecido. */
    private Integer cdConsistenciaCpfCnpfFavorecido = null;

    /** Atributo cpfCnpj. */
    private String cpfCnpj = null;

    /** Atributo nomeRazaoSocial. */
    private String nomeRazaoSocial = null;

    /** Atributo cdPeriodicidade. */
    private Integer cdPeriodicidade = null;

    /** Atributo dsPeriodicidade. */
    private String dsPeriodicidade = null;

    /** Atributo cdDestino. */
    private Integer cdDestino = null;

    /** Atributo cdPermiteCorrespAberta. */
    private Integer cdPermiteCorrespAberta = null;

    /** Atributo cdDemonsInfReservada. */
    private Integer cdDemonsInfReservada = null;

    /** Atributo cdAgruparCorrespondencia. */
    private Integer cdAgruparCorrespondencia = null;

    /** Atributo quantidadeVias. */
    private String quantidadeVias = null;

    /** Atributo cdTipoRejeicao. */
    private Integer cdTipoRejeicao = null;

    /** Atributo cdTipoEmissao. */
    private Integer cdTipoEmissao = null;

    /** Atributo cdMidiaDisponivelCorrentista. */
    private Integer cdMidiaDisponivelCorrentista = null;

    /** Atributo cdMidiaDisponivelNaoCorrentista. */
    private Integer cdMidiaDisponivelNaoCorrentista = null;

    /** Atributo cdTipoEnvioCorrespondencia. */
    private Integer cdTipoEnvioCorrespondencia = null;

    /** Atributo cdFrasesPreCadastradas. */
    private Integer cdFrasesPreCadastradas = null;

    /** Atributo cdCobrarTarifasFuncionarios. */
    private Integer cdCobrarTarifasFuncionarios = null;

    /** Atributo qtddMesesEmissao. */
    private String qtddMesesEmissao = null;

    /** Atributo qtddLinhas. */
    private String qtddLinhas = null;

    /** Atributo qtddVias. */
    private String qtddVias = null;

    /** Atributo qtddViasPagas. */
    private String qtddViasPagas = null;

    /** Atributo cdFormularioImpressao. */
    private Integer cdFormularioImpressao = null;

    /** Atributo dsFormularioImpressao. */
    private String dsFormularioImpressao = null;

    /** Atributo cdPeriodicidadeCobrancaTarifa. */
    private Integer cdPeriodicidadeCobrancaTarifa = null;

    /** Atributo dsPeriodicidadeCobrancaTarifa. */
    private String dsPeriodicidadeCobrancaTarifa = null;

    /** Atributo fechamentoApuracao. */
    private String fechamentoApuracao = null;

    /** Atributo diasCobrancaAposApuracao. */
    private String diasCobrancaAposApuracao = null;

    /** Atributo cdTipoReajuste. */
    private Integer cdTipoReajuste = null;

    /** Atributo mesesReajusteAutomatico. */
    private String mesesReajusteAutomatico = null;

    /** Atributo cdIndiceReajuste. */
    private Integer cdIndiceReajuste = null;

    /** Atributo dsIndiceReajuste. */
    private String dsIndiceReajuste = null;

    /** Atributo percentualReajuste. */
    private BigDecimal percentualReajuste = null;

    /** Atributo bonificacaoTarifa. */
    private BigDecimal bonificacaoTarifa = null;

    /** Atributo cdTipoIdentificadorBeneficiario. */
    private Integer cdTipoIdentificadorBeneficiario = null;

    /** Atributo cdTipoConsistenciaIdentificador. */
    private Integer cdTipoConsistenciaIdentificador = null;

    /** Atributo cdCondicaoEnquadramento. */
    private Integer cdCondicaoEnquadramento = null;

    /** Atributo cdCriterioPrincipalEnquadramento. */
    private Integer cdCriterioPrincipalEnquadramento = null;

    /** Atributo cdCriterioCompostoEnquadramento. */
    private Integer cdCriterioCompostoEnquadramento = null;

    /** Atributo qtddEtapasRecadastramento. */
    private String qtddEtapasRecadastramento = null;

    /** Atributo qtddFasesEtapa. */
    private String qtddFasesEtapa = null;

    /** Atributo qtddMesesPrazoFase. */
    private String qtddMesesPrazoFase = null;

    /** Atributo qtddMesesPrazoEtapa. */
    private String qtddMesesPrazoEtapa = null;

    /** Atributo cdPermiteEnvioRemessa. */
    private Integer cdPermiteEnvioRemessa = null;

    /** Atributo cdPeriodicidadeEnvioRemessa. */
    private Integer cdPeriodicidadeEnvioRemessa = null;

    /** Atributo dsPeriodicidadeEnvioRemessa. */
    private String dsPeriodicidadeEnvioRemessa = null;

    /** Atributo cdBaseDadosUtilizada. */
    private Integer cdBaseDadosUtilizada = null;

    /** Atributo cdTipoCargaBaseDadosUtilizada. */
    private Integer cdTipoCargaBaseDadosUtilizada = null;

    /** Atributo cdPermiteAnteciparRecadastramento. */
    private Integer cdPermiteAnteciparRecadastramento = null;

    /** Atributo dataLimiteInicioVinculoCargaBase. */
    private Date dataLimiteInicioVinculoCargaBase = null;

    /** Atributo dataInicioRecadastramento. */
    private Date dataInicioRecadastramento = null;

    /** Atributo dataFimRecadastramento. */
    private Date dataFimRecadastramento = null;

    /** Atributo cdPermiteAcertosDados. */
    private Integer cdPermiteAcertosDados = null;

    /** Atributo dataInicioPeriodoAcertoDados. */
    private Date dataInicioPeriodoAcertoDados = null;

    /** Atributo dataFimPeriodoAcertoDados. */
    private Date dataFimPeriodoAcertoDados = null;

    /** Atributo cdEmiteMsgRecadastramentoMidiaOnline. */
    private Integer cdEmiteMsgRecadastramentoMidiaOnline = null;

    /** Atributo cdMidiaMsgRecadastramentoOnline. */
    private Integer cdMidiaMsgRecadastramentoOnline = null;

    /** Atributo diaFechamentoApuracaoTarifaMensal. */
    private String diaFechamentoApuracaoTarifaMensal = null;

    /** Atributo qtddDiasCobrancaTarifaApuracao. */
    private String qtddDiasCobrancaTarifaApuracao = null;

    /** Atributo qtddMesesReajusteAutomaticoTarifa. */
    private String qtddMesesReajusteAutomaticoTarifa = null;

    /** Atributo cdIndiceEconomicoReajusteTarifa. */
    private Integer cdIndiceEconomicoReajusteTarifa = null;

    /** Atributo dsIndiceEconomicoReajusteTarifa. */
    private String dsIndiceEconomicoReajusteTarifa = null;

    /** Atributo percentualIndiceEconomicoReajusteTarifa. */
    private BigDecimal percentualIndiceEconomicoReajusteTarifa = null;

    /** Atributo percentualBonificacaoTarifaPadrao. */
    private BigDecimal percentualBonificacaoTarifaPadrao = null;

    /** Atributo indiceReajuste. */
    private Integer indiceReajuste = null;

    /** Atributo cdAmbienteServicoContrato. */
    private String cdAmbienteServicoContrato = null;

    /** Atributo listaModalidadeTipoServicoSaidaDTO. */
    private ListarModalidadeTipoServicoSaidaDTO listaModalidadeTipoServicoSaidaDTO = new ListarModalidadeTipoServicoSaidaDTO();

    /** Atributo listaAcaoNaoComprovacao. */
    private List<SelectItem> listaAcaoNaoComprovacao = new ArrayList<SelectItem>();

    /** Atributo listaAcaoNaoComprovacaoHash. */
    private Map<Long, String> listaAcaoNaoComprovacaoHash = new HashMap<Long, String>();

    /** Atributo listaCadastroConsultaEnderecoEnvio. */
    private List<SelectItem> listaCadastroConsultaEnderecoEnvio = new ArrayList<SelectItem>();

    /** Atributo listaCadastroConsultaEnderecoEnvioHash. */
    private Map<Long, String> listaCadastroConsultaEnderecoEnvioHash = new HashMap<Long, String>();

    /** Atributo listaCadastroUtilizadoRecadastramento. */
    private List<SelectItem> listaCadastroUtilizadoRecadastramento = new ArrayList<SelectItem>();

    /** Atributo listaCadastroUtilizadoRecadastramentoHash. */
    private Map<Long, String> listaCadastroUtilizadoRecadastramentoHash = new HashMap<Long, String>();

    /** Atributo listaCondicaoEnquadramento. */
    private List<SelectItem> listaCondicaoEnquadramento = new ArrayList<SelectItem>();

    /** Atributo listaCondicaoEnquadramentoHash. */
    private Map<Long, String> listaCondicaoEnquadramentoHash = new HashMap<Long, String>();

    /** Atributo listaControleExpiracaoCredito. */
    private List<SelectItem> listaControleExpiracaoCredito = new ArrayList<SelectItem>();

    /** Atributo listaControleExpiracaoCreditoHash. */
    private Map<Long, String> listaControleExpiracaoCreditoHash = new HashMap<Long, String>();

    /** Atributo listaCriterioCompostoEnquadramento. */
    private List<SelectItem> listaCriterioCompostoEnquadramento = new ArrayList<SelectItem>();

    /** Atributo listaCriterioCompostoEnquadramentoHash. */
    private Map<Long, String> listaCriterioCompostoEnquadramentoHash = new HashMap<Long, String>();

    /** Atributo listaCriterioPrincipalEnquadramento. */
    private List<SelectItem> listaCriterioPrincipalEnquadramento = new ArrayList<SelectItem>();

    /** Atributo listaCriterioPrincipalEnquadramentoHash. */
    private Map<Long, String> listaCriterioPrincipalEnquadramentoHash = new HashMap<Long, String>();

    /** Atributo listaDestinoEnvio. */
    private List<SelectItem> listaDestinoEnvio = new ArrayList<SelectItem>();

    /** Atributo listaDestinoEnvioHash. */
    private Map<Long, String> listaDestinoEnvioHash = new HashMap<Long, String>();

    /** Atributo listaDestinoEnvioCorrespondencia. */
    private List<SelectItem> listaDestinoEnvioCorrespondencia = new ArrayList<SelectItem>();

    /** Atributo listaDestinoEnvioCorrespondenciaHash. */
    private Map<Long, String> listaDestinoEnvioCorrespondenciaHash = new HashMap<Long, String>();

    /** Atributo listaFormaManutencaoCadastroProcuradores. */
    private List<SelectItem> listaFormaManutencaoCadastroProcuradores = new ArrayList<SelectItem>();

    /** Atributo listaFormaManutencaoCadastroProcuradoresHash. */
    private Map<Long, String> listaFormaManutencaoCadastroProcuradoresHash = new HashMap<Long, String>();

    /** Atributo listaFormaAutorizacaoPagamentos. */
    private List<SelectItem> listaFormaAutorizacaoPagamentos = new ArrayList<SelectItem>();

    /** Atributo listaFormaAutorizacaoPagamentosHash. */
    private Map<Long, String> listaFormaAutorizacaoPagamentosHash = new HashMap<Long, String>();

    /** Atributo listaFormaEnvioPagamento. */
    private List<SelectItem> listaFormaEnvioPagamento = new ArrayList<SelectItem>();

    /** Atributo listaFormaEnvioPagamentoHash. */
    private Map<Long, String> listaFormaEnvioPagamentoHash = new HashMap<Long, String>();

    /** Atributo listaFormaManutencaoCadastroFavorecido. */
    private List<SelectItem> listaFormaManutencaoCadastroFavorecido = new ArrayList<SelectItem>();

    /** Atributo listaFormaManutencaoCadastroFavorecidoHash. */
    private Map<Long, String> listaFormaManutencaoCadastroFavorecidoHash = new HashMap<Long, String>();

    /** Atributo listaMeioDisponibilizacaoNaoCorrentistas. */
    private List<SelectItem> listaMeioDisponibilizacaoNaoCorrentistas = new ArrayList<SelectItem>();

    /** Atributo listaMeioDisponibilizacaoNaoCorrentistasHash. */
    private Map<Long, String> listaMeioDisponibilizacaoNaoCorrentistasHash = new HashMap<Long, String>();

    /** Atributo listaMeioDisponibilizacaoCorrentistas. */
    private List<SelectItem> listaMeioDisponibilizacaoCorrentistas = new ArrayList<SelectItem>();

    /** Atributo listaMeioDisponibilizacaoCorrentistasHash. */
    private Map<Long, String> listaMeioDisponibilizacaoCorrentistasHash = new HashMap<Long, String>();

    /** Atributo listaMomentoIndicacaoCreditoEfetivado. */
    private List<SelectItem> listaMomentoIndicacaoCreditoEfetivado = new ArrayList<SelectItem>();

    /** Atributo listaMomentoIndicacaoCreditoEfetivadoHash. */
    private Map<Long, String> listaMomentoIndicacaoCreditoEfetivadoHash = new HashMap<Long, String>();

    /** Atributo listaMomentoEnvio. */
    private List<SelectItem> listaMomentoEnvio = new ArrayList<SelectItem>();
    
    private Map<Long, String> listaMomentoEnvioHash = new HashMap<Long, String>();
    
    private List<SelectItem> listaOrigem = new ArrayList<SelectItem>();
    
    private Map<Long, String> listaOrigemHash = new HashMap<Long, String>();

    /** Atributo listaMomentoEnvioHash. */
   

    /** Atributo listaMidiaMensagemRecadastramento. */
    private List<SelectItem> listaMidiaMensagemRecadastramento = new ArrayList<SelectItem>();

    /** Atributo listaMidiaMensagemRecadastramentoHash. */
    private Map<Long, String> listaMidiaMensagemRecadastramentoHash = new HashMap<Long, String>();

    /** Atributo listaMidiaDisponibilizacaoCorrentistas. */
    private List<SelectItem> listaMidiaDisponibilizacaoCorrentistas = new ArrayList<SelectItem>();

    /** Atributo listaMidiaDisponibilizacaoCorrentistasHash. */
    private Map<Long, String> listaMidiaDisponibilizacaoCorrentistasHash = new HashMap<Long, String>();

    /** Atributo listaOcorrenciaDebito. */
    private List<SelectItem> listaOcorrenciaDebito = new ArrayList<SelectItem>();

    /** Atributo listaOcorrenciaDebitoHash. */
    private Map<Long, String> listaOcorrenciaDebitoHash = new HashMap<Long, String>();

    /** Atributo listaPermiteEstornoPagamento. */
    private List<SelectItem> listaPermiteEstornoPagamento = new ArrayList<SelectItem>();

    /** Atributo listaPermiteEstornoPagamentoHash. */
    private Map<Long, String> listaPermiteEstornoPagamentoHash = new HashMap<Long, String>();

    /** Atributo listaPrioridadeDebito. */
    private List<SelectItem> listaPrioridadeDebito = new ArrayList<SelectItem>();

    /** Atributo listaPrioridadeDebitoHash. */
    private Map<Long, String> listaPrioridadeDebitoHash = new HashMap<Long, String>();

    /** Atributo listaProcessamentoEfetivacaoPagamento. */
    private List<SelectItem> listaProcessamentoEfetivacaoPagamento = new ArrayList<SelectItem>();

    /** Atributo listaProcessamentoEfetivacaoPagamentoHash. */
    private Map<Long, String> listaProcessamentoEfetivacaoPagamentoHash = new HashMap<Long, String>();

    /** Atributo listaTipoCartaoContaSalario. */
    private List<SelectItem> listaTipoCartaoContaSalario = new ArrayList<SelectItem>();

    /** Atributo listaTipoCartaoContaSalarioHash. */
    private Map<Long, String> listaTipoCartaoContaSalarioHash = new HashMap<Long, String>();

    /** Atributo listaTipoConsistenciaIdentificacaoBeneficiario. */
    private List<SelectItem> listaTipoConsistenciaIdentificacaoBeneficiario = new ArrayList<SelectItem>();

    /** Atributo listaTipoConsistenciaIdentificacaoBeneficiarioHash. */
    private Map<Long, String> listaTipoConsistenciaIdentificacaoBeneficiarioHash = new HashMap<Long, String>();

    /** Atributo listaTipoIdentificacaoBeneficiario. */
    private List<SelectItem> listaTipoIdentificacaoBeneficiario = new ArrayList<SelectItem>();

    /** Atributo listaTipoIdentificacaoBeneficiarioHash. */
    private Map<Long, String> listaTipoIdentificacaoBeneficiarioHash = new HashMap<Long, String>();

    /** Atributo listaTipoCargaCadastroBeneficiarios. */
    private List<SelectItem> listaTipoCargaCadastroBeneficiarios = new ArrayList<SelectItem>();

    /** Atributo listaTipoCargaCadastroBeneficiariosHash. */
    private Map<Long, String> listaTipoCargaCadastroBeneficiariosHash = new HashMap<Long, String>();

    /** Atributo listaTipoConsistenciaCpfCnpjProprietario. */
    private List<SelectItem> listaTipoConsistenciaCpfCnpjProprietario = new ArrayList<SelectItem>();

    /** Atributo listaTipoConsistenciaCpfCnpjProprietarioHash. */
    private Map<Long, String> listaTipoConsistenciaCpfCnpjProprietarioHash = new HashMap<Long, String>();

    /** Atributo listaTipoConsistenciaInscricaoFavorecido. */
    private List<SelectItem> listaTipoConsistenciaInscricaoFavorecido = new ArrayList<SelectItem>();

    /** Atributo listaTipoConsistenciaInscricaoFavorecidoHash. */
    private Map<Long, String> listaTipoConsistenciaInscricaoFavorecidoHash = new HashMap<Long, String>();

    /** Atributo listaTipoConsistenciaCpfCnpjFavorecido. */
    private List<SelectItem> listaTipoConsistenciaCpfCnpjFavorecido = new ArrayList<SelectItem>();

    /** Atributo listaTipoConsistenciaCpfCnpjFavorecidoHash. */
    private Map<Long, String> listaTipoConsistenciaCpfCnpjFavorecidoHash = new HashMap<Long, String>();

    /** Atributo listaTipoConsolidacaoPagamentosComprovante. */
    private List<SelectItem> listaTipoConsolidacaoPagamentosComprovante = new ArrayList<SelectItem>();

    /** Atributo listaTipoConsolidacaoPagamentosComprovanteHash. */
    private Map<Long, String> listaTipoConsolidacaoPagamentosComprovanteHash = new HashMap<Long, String>();

    /** Atributo listaTipoConsultaSaldo. */
    private List<SelectItem> listaTipoConsultaSaldo = new ArrayList<SelectItem>();

    /** Atributo listaTipoConsultaSaldoHash. */
    private Map<Long, String> listaTipoConsultaSaldoHash = new HashMap<Long, String>();

    /** Atributo listaTipoContaCredito. */
    private List<SelectItem> listaTipoContaCredito = new ArrayList<SelectItem>();

    /** Atributo listaTipoContaCreditoHash. */
    private Map<Long, String> listaTipoContaCreditoHash = new HashMap<Long, String>();

    /** Atributo listaTipoDataControleFloating. */
    private List<SelectItem> listaTipoDataControleFloating = new ArrayList<SelectItem>();

    /** Atributo listaTipoDataControleFloatingHash. */
    private Map<Long, String> listaTipoDataControleFloatingHash = new HashMap<Long, String>();

    /** Atributo listaTipoFormacaoListaDebito. */
    private List<SelectItem> listaTipoFormacaoListaDebito = new ArrayList<SelectItem>();

    /** Atributo listaTipoFormacaoListaDebitoHash. */
    private Map<Long, String> listaTipoFormacaoListaDebitoHash = new HashMap<Long, String>();

    /** Atributo listaTipoInscricaoFavorecido. */
    private List<SelectItem> listaTipoInscricaoFavorecido = new ArrayList<SelectItem>();

    /** Atributo listaTipoInscricaoFavorecidoHash. */
    private Map<Long, String> listaTipoInscricaoFavorecidoHash = new HashMap<Long, String>();

    /** Atributo listaTipoRastreamentoTitulos. */
    private List<SelectItem> listaTipoRastreamentoTitulos = new ArrayList<SelectItem>();

    /** Atributo listaTipoRastreamentoTitulosHash. */
    private Map<Long, String> listaTipoRastreamentoTitulosHash = new HashMap<Long, String>();

    /** Atributo listaTipoRejeicaoLote. */
    private List<SelectItem> listaTipoRejeicaoLote = new ArrayList<SelectItem>();

    /** Atributo listaTipoRejeicaoLoteHash. */
    private Map<Long, String> listaTipoRejeicaoLoteHash = new HashMap<Long, String>();

    /** Atributo listaTipoRejeicaoEfetivacao. */
    private List<SelectItem> listaTipoRejeicaoEfetivacao = new ArrayList<SelectItem>();

    /** Atributo listaTipoRejeicaoEfetivacaoHash. */
    private Map<Long, String> listaTipoRejeicaoEfetivacaoHash = new HashMap<Long, String>();

    /** Atributo listaTipoRejeicaoAgendamento. */
    private List<SelectItem> listaTipoRejeicaoAgendamento = new ArrayList<SelectItem>();

    /** Atributo listaTipoRejeicaoAgendamentoHash. */
    private Map<Long, String> listaTipoRejeicaoAgendamentoHash = new HashMap<Long, String>();

    /** Atributo listaTipoTratamentoContaTransferida. */
    private List<SelectItem> listaTipoTratamentoContaTransferida = new ArrayList<SelectItem>();

    /** Atributo listaTipoTratamentoContaTransferidaHash. */
    private Map<Long, String> listaTipoTratamentoContaTransferidaHash = new HashMap<Long, String>();

    /** Atributo listaTratamentoFeriadoFimVigenciaCredito. */
    private List<SelectItem> listaDemonstra2LinhaExtrato = new ArrayList<SelectItem>();
    
    /** Atributo listaTratamentoFeriadoFimVigenciaCreditoHash. */
    private Map<Long, String> listaDemonstra2LinhaExtratoHash = new HashMap<Long, String>();
    
    /** Atributo listaTratamentoFeriadoFimVigenciaCredito. */
    private List<SelectItem> listaTratamentoFeriadoFimVigenciaCredito = new ArrayList<SelectItem>();

    /** Atributo listaTratamentoFeriadoFimVigenciaCreditoHash. */
    private Map<Long, String> listaTratamentoFeriadoFimVigenciaCreditoHash = new HashMap<Long, String>();

    /** Atributo listaTratamentoListaDebitoNumeracao. */
    private List<SelectItem> listaTratamentoListaDebitoNumeracao = new ArrayList<SelectItem>();

    /** Atributo listaTratamentoListaDebitoNumeracaoHash. */
    private Map<Long, String> listaTratamentoListaDebitoNumeracaoHash = new HashMap<Long, String>();

    /** Atributo listaTratamentoValorDivergente. */
    private List<SelectItem> listaTratamentoValorDivergente = new ArrayList<SelectItem>();

    /** Atributo listaTratamentoValorDivergenteHash. */
    private Map<Long, String> listaTratamentoValorDivergenteHash = new HashMap<Long, String>();

    /** Atributo listaTratamentoFeriadosDataPagamento. */
    private List<SelectItem> listaTratamentoFeriadosDataPagamento = new ArrayList<SelectItem>();

    /** Atributo listaTratamentoFeriadosDataPagamentoHash. */
    private Map<Long, String> listaTratamentoFeriadosDataPagamentoHash = new HashMap<Long, String>();

    /** Atributo feriadosLocaisHash. */
    private Map<Integer, String> feriadosLocaisHash = new HashMap<Integer, String>();

    /** Atributo listaPeriodicidade. */
    private List<SelectItem> listaPeriodicidade = new ArrayList<SelectItem>();

    /** Atributo listaPeriodicidadeHash. */
    private Map<Integer, String> listaPeriodicidadeHash = new HashMap<Integer, String>();

    /** Atributo listaIndiceEconomico. */
    private List<SelectItem> listaIndiceEconomico = new ArrayList<SelectItem>();

    /** Atributo listaIndiceEconomicoHash. */
    private Map<Integer, String> listaIndiceEconomicoHash = new HashMap<Integer, String>();

    /** Atributo listaTipoServico. */
    private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();

    /** Atributo listaTipoServicoHash. */
    private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();

    /** Atributo listaTipoLayoutArquivo. */
    private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();

    /** Atributo listaTipoLayoutArquivoHash. */
    private Map<Integer, String> listaTipoLayoutArquivoHash = new HashMap<Integer, String>();

    /** Atributo listaGridTipoServico. */
    private List<ListarServicosSaidaDTO> listaGridTipoServico = null;

    /** Atributo entradaServicosModalidades. */
    private ListarServModEntradaDTO entradaServicosModalidades = new ListarServModEntradaDTO();

    /** Atributo listaControleRadioTipoServico. */
    private List<SelectItem> listaControleRadioTipoServico = null;

    /** Atributo itemSelecionadoListaTipoServico. */
    private Integer itemSelecionadoListaTipoServico = null;

    /** Atributo dsTipoServicoSelecionado. */
    private String dsTipoServicoSelecionado = null;

    /** Atributo dsSituacaoServico. */
    private String dsSituacaoServico = null;

    /** Atributo dsSituacaoServicoRelacionado. */
    private String dsSituacaoServicoRelacionado = null;

    /** Atributo dsSituacaoModalidade. */
    private String dsSituacaoModalidade = null;

    /** Atributo countCheckedModalidade. */
    private Integer countCheckedModalidade = null;

    /** Atributo btnIncluir. */
    private boolean btnIncluir = false;

    /** Atributo permiteAlteracaoComboFloating. */
    private boolean permiteAlteracaoComboFloating = false;

    /** Atributo permiteAlteracaoRadioFloating. */
    private boolean permiteAlteracaoRadioFloating = false;

    /** Atributo permiteAlteracaoCombo. */
    private boolean permiteAlteracaoCombo = false;

    /** Atributo .habilitaTipoDataControleFloating. */
    private boolean habilitaTipoDataControleFloating = false;

    /** Atributo listaGridModalidade. */
    private List<ListarServicosSaidaDTO> listaGridModalidade = null;

    /** Atributo listaGridModalidadeAux. */
    private List<ListarServicosSaidaDTO> listaGridModalidadeAux = null;

    /** Atributo listaControleCheckModalidade. */
    private List<SelectItem> listaControleCheckModalidade = null;

    /** Atributo listaCheckedGridModalidade. */
    private List<ListarServicosSaidaDTO> listaCheckedGridModalidade = null;

    /** Atributo cdTipoServicoSelecionado. */
    private Integer cdTipoServicoSelecionado = null;

    /** Atributo listaGridServicoRelacionado. */
    private List<ListarModalidadeTipoServicoSaidaDTO> listaGridServicoRelacionado = null;

    /** Atributo saidaDTO. */
    private DetalharTipoServModContratoSaidaDTO saidaDTO = null;

    /** Atributo dsFormaAutorizacaoPagamento. */
    private String dsFormaAutorizacaoPagamento = null;

    /** Atributo dsUtilizaPreAutorizacaoPagamentos. */
    private String dsUtilizaPreAutorizacaoPagamentos = null;

    /** Atributo dsTipoControleFloating. */
    private String dsTipoControleFloating = null;


    /** Atributo desabilitarComboPrioridadeDebito. */
    private boolean desabilitarComboPrioridadeDebito = false;

    /** Atributo desabilitarComboTipoConsultaSaldo. */
    private boolean desabilitarComboTipoConsultaSaldo = false;

    /** Atributo desabilitaIndicadorAutorizacaoComplemento. */
    private boolean desabilitaIndicadorAutorizacaoComplemento;

    /** Atributo desabilitaCdIndicadorListaDebito. */
    private boolean desabilitaCdIndicadorListaDebito;

    /** Atributo desabilitaCdIndicadorTipoFormacaoLista. */
    private boolean desabilitaCdIndicadorTipoFormacaoLista;

    /** Atributo desabilitaCdIndicadorTipoConsistenciaLista. */
    private boolean desabilitaCdIndicadorTipoConsistenciaLista;

    /** Atributo desabilitaGerarLancamentoProgramado. */
    private boolean desabilitaGerarLancamentoProgramado;

    /** Atributo desabilitaIndicadorRejeicaoAgendaLote. */
    private boolean desabilitaIndicadorRejeicaoAgendaLote;

    /** Atributo desabilitaIndicadorRejeicaoEfetivacaoLote. */
    private boolean desabilitaIndicadorRejeicaoEfetivacaoLote;

    /** Atributo desabilitaIndicadorMaximaInconLote. */
    private boolean desabilitaIndicadorMaximaInconLote;

    /** Atributo desabilitaIndicadorPercentualMaximoInconLote. */
    private boolean desabilitaIndicadorPercentualMaximoInconLote;

    /** Atributo renderizaCamposDebitosPendentesDeVeiculos. */
    private boolean renderizaCamposDebitosPendentesDeVeiculos = false;

    /** Atributo para a Consist�ncia do CPF/CNPJ do Benefici�rio/Sacador Avalista � NPC */
    private Long cboConsistenciaCpfCnpjBenefAvalNpc = 0l;
	private List<SelectItem> listaConsistenciaCpfCnpjBenefAvalNpc = new ArrayList<SelectItem>();
    private Map<Long, String> listaConsistenciaCpfCnpjBenefAvalNpcHash = new HashMap<Long, String>();	
    private String dsConsistenciaCpfCnpjBenefAvalNpc = null;
    private boolean desabilitarCboConsistenciaCpfCnpjBenefAvalNpc = false;    
    private long cdConsistenciaCpfCnpjBenefAvalNpc = 0l;

    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao = null;

    /** Atributo usuarioInclusao. */
    private String usuarioInclusao = null;

    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao = null;

    /** Atributo complementoInclusao. */
    private String complementoInclusao = null;

    /** Atributo dataHoraManutencao. */
    private String dataHoraManutencao = null;

    /** Atributo usuarioManutencao. */
    private String usuarioManutencao = null;

    /** Atributo tipoCanalManutencao. */
    private String tipoCanalManutencao = null;

    /** Atributo complementoManutencao. */
    private String complementoManutencao = null;

    /** Atributo dsTipoConsultaSaldo. */
    private String dsTipoConsultaSaldo = null;

    /** Atributo dsQuantDiasRepique. */
    private String dsQuantDiasRepique = null;

    /** Atributo dsUtilizaCadFavControlPag. */
    private String dsUtilizaCadFavControlPag = null;

    /** Atributo dsValorMaximoPagFavNaoCad. */
    private String dsValorMaximoPagFavNaoCad = null;

    /** Atributo dsTratamentoFeriadosDataPagamento. */
    private String dsTratamentoFeriadosDataPagamento = null;

    /** Atributo dsTipoProcessamento. */
    private String dsTipoProcessamento = null;

    /** Atributo dsTipoRejeicaoAgendamento. */
    private String dsTipoRejeicaoAgendamento = null;

    /** Atributo dsTipoRejeicaoEfetivacao. */
    private String dsTipoRejeicaoEfetivacao = null;

    /** Atributo dsPrioridadeDebito. */
    private String dsPrioridadeDebito = null;

    /** Atributo dsGerarLancFuturoDebito. */
    private String dsGerarLancFuturoDebito = null;

    /** Atributo dsGerarLancFuturoCredito. */
    private String dsGerarLancFuturoCredito = null;

    /** Atributo dsPermiteFavorecidoConsulPag. */
    private String dsPermiteFavorecidoConsulPag = null;

    /** Atributo dsValorLimiteIndividual. */
    private String dsValorLimiteIndividual = null;

    /** Atributo dsValorLimiteDiario. */
    private String dsValorLimiteDiario = null;

    /** Atributo listaGridPesquisa2. */
    private List<ListarModalidadeTipoServicoSaidaDTO> listaGridPesquisa2 = null;

    /** Atributo itemSelecionadoLista2. */
    private Integer itemSelecionadoLista2 = null;

    /** Atributo listaControleRadio2. */
    private List<SelectItem> listaControleRadio2 = null;

    /** Atributo dsModalidade. */
    private String dsModalidade = null;

    /** Atributo dsTipoServico. */
    private String dsTipoServico = null;

    /** Atributo cdPeriodicidadeEnvio. */
    private Integer cdPeriodicidadeEnvio = null;

    /** Atributo dsPeriodicidadeEnvio. */
    private String dsPeriodicidadeEnvio = null;

    /** Atributo cdEnderecoEnvio. */
    private Integer cdEnderecoEnvio = null;

    /** Atributo cdPermitirAgrupamento. */
    private Integer cdPermitirAgrupamento = null;

    /** Atributo qtddDiasAntecipacao. */
    private String qtddDiasAntecipacao = null;

    /** Atributo cdTipoConsultaSaldoFiltro. */
    private Integer cdTipoConsultaSaldoFiltro = null;

    /** Atributo quantDiasRepiqueFiltro. */
    private String quantDiasRepiqueFiltro = null;

    /** Atributo cdUtilizaCadFavorFiltro. */
    private Integer cdUtilizaCadFavorFiltro = null;

    /** Atributo valorMaxPagFavorecidoFiltro. */
    private String valorMaxPagFavorecidoFiltro = null;

    /** Atributo cdTratamentoFeriadosFiltro. */
    private Integer cdTratamentoFeriadosFiltro = null;

    /** Atributo cdTipoProcessamentoFiltro. */
    private Integer cdTipoProcessamentoFiltro = null;

    /** Atributo cdTipoRejAgendamentoFiltro. */
    private Integer cdTipoRejAgendamentoFiltro = null;

    /** Atributo cdTipoRejEfetivacaoFiltro. */
    private Integer cdTipoRejEfetivacaoFiltro = null;

    /** Atributo cdPrioridadeDebitoFiltro. */
    private Integer cdPrioridadeDebitoFiltro = null;

    /** Atributo cdGerLancFutDebitoFiltro. */
    private Integer cdGerLancFutDebitoFiltro = null;

    /** Atributo cdGerLancFutCreditoFiltro. */
    private Integer cdGerLancFutCreditoFiltro = null;

    /** Atributo cdPermiteFavConsultPagFiltro. */
    private Integer cdPermiteFavConsultPagFiltro = null;

    /** Atributo valorLimiteIndividualFiltro. */
    private BigDecimal valorLimiteIndividualFiltro = null;

    /** Atributo valorLimiteDiarioFiltro. */
    private BigDecimal valorLimiteDiarioFiltro = null;

    /** Atributo cdPermiteOutrosTipos. */
    private String cdPermiteOutrosTipos = null;

    /** Atributo cdOcorrenciaDebito. */
    private Integer cdOcorrenciaDebito = null;

    /** Atributo cdGerarLancamentoProgramado. */
    private String cdGerarLancamentoProgramado = null;

    /** Atributo cdEfetuaConsistencia. */
    private String cdEfetuaConsistencia = null;

    /** Atributo cdTipoTratamento. */
    private Integer cdTipoTratamento = null;

    /** Atributo cdPermiteEstorno. */
    private Integer cdPermiteEstorno = null;

    /** Atributo qtddMaximaRegistro. */
    private String qtddMaximaRegistro = null;

    /** Atributo percentualMaximoRegistro. */
    private String percentualMaximoRegistro = null;

    /** Atributo cdPermiteDebitoOnline. */
    private String cdPermiteDebitoOnline = null;

    /** Atributo permitePagarVencido. */
    private String permitePagarVencido = null;

    /** Atributo qtddLimiteDiasPagamentoVencido. */
    private String qtddLimiteDiasPagamentoVencido = null;

    /** Atributo permitePagarMenor. */
    private String permitePagarMenor = null;

    /** Atributo cdTipoRastreamentoTitulos. */
    private Integer cdTipoRastreamentoTitulos = null;

    /** Atributo rastrearTitulo. */
    private String rastrearTitulo = null;

    /** Atributo rastrearNotas. */
    private String rastrearNotas = null;

    /** Atributo capturarTitulos. */
    private String capturarTitulos = null;

    /** Atributo agendarTituloProprio. */
    private String agendarTituloProprio = null;

    /** Atributo agendarTituloFilial. */
    private String agendarTituloFilial = null;

    /** Atributo bloquearEmissaoPapeleta. */
    private String bloquearEmissaoPapeleta = null;

    /** Atributo dataInicioBloqueioPapeleta. */
    private Date dataInicioBloqueioPapeleta = null;

    /** Atributo dsDataInicioBloqueioPapeleta. */
    private String dsDataInicioBloqueioPapeleta = null;

    /** Atributo dataInicioRastreamento. */
    private Date dataInicioRastreamento = null;

    /** Atributo dsDataInicioRastreamento. */
    private String dsDataInicioRastreamento = null;

    /** Atributo dataRegistroTitulo. */
    private Date dataRegistroTitulo = null;

    /** Atributo dsDataRegistroTitulo. */
    private String dsDataRegistroTitulo = null;

    /** Atributo cdTipoComprovacao. */
    private Integer cdTipoComprovacao = null;

    /** Atributo qtddMesesPeriodicidade. */
    private String qtddMesesPeriodicidade = null;

    /** Atributo qtddDiasAvisoVenc. */
    private String qtddDiasAvisoVenc = null;

    /** Atributo cdUtilizarMsgPerOnline. */
    private Integer cdUtilizarMsgPerOnline = null;

    /** Atributo qtddDiasAntecedeInicio. */
    private String qtddDiasAntecedeInicio = null;

    /** Atributo cdEfetivacao. */
    private Integer cdEfetivacao = null;

    /** Atributo cdPermiteConsulFavorecido. */
    private Integer cdPermiteConsulFavorecido = null;

    /** Atributo cdControlePagFavorecido. */
    private Integer cdControlePagFavorecido = null;

    /** Atributo valorMaxPagFavNaoCadastrado. */
    private BigDecimal valorMaxPagFavNaoCadastrado = null;

    /** Atributo valorMaxPagFavNaoCadastradoDesc. */
    private String valorMaxPagFavNaoCadastradoDesc = null;

    /** Atributo percentualInconsLote. */
    private String percentualInconsLote = null;

    /** Atributo qtddMaxInconsLote. */
    private String qtddMaxInconsLote = null;

    /** Atributo cdPermiteContingenciaPag. */
    private Integer cdPermiteContingenciaPag = null;

    /** Atributo cdPermiteConsultaFavorecido. */
    private Integer cdPermiteConsultaFavorecido = null;

    /** Atributo cdTratValorDivergente. */
    private Integer cdTratValorDivergente = null;

    /** Atributo cdTipoConsisProprietario. */
    private Integer cdTipoConsisProprietario = null;

    /** Atributo dsTipoConsisProprietario. */
    private String dsTipoConsisProprietario = null;

    /** Atributo listaGridModalidadeRelacionada. */
    private List<ListarServicoRelacionadoCdpsSaidaDTO> listaGridModalidadeRelacionada = null;

    /** Atributo listaControleCheckModalidadeRelacionada. */
    private List<SelectItem> listaControleCheckModalidadeRelacionada = null;

    /** Atributo listaCheckedGridModalidadeRelacionada. */
    private List<ListarServicoRelacionadoCdpsSaidaDTO> listaCheckedGridModalidadeRelacionada = null;

    /** Atributo desabilitarCombo. */
    private boolean desabilitarCombo = false;

    /** Atributo btoAcionado. */
    private boolean btoAcionado = false;

    /** Atributo btnConsultarHist. */
    private boolean btnConsultarHist = false;

    /** Atributo filtroDataDe. */
    private Date filtroDataDe = null;

    /** Atributo filtroDataAte. */
    private Date filtroDataAte = null;

    /** Atributo listaGridPesquisa3. */
    private List<ListarConManServicoSaidaDTO> listaGridPesquisa3 = null;

    /** Atributo itemSelecionadoLista3. */
    private Integer itemSelecionadoLista3 = null;

    /** Atributo listaControleRadio3. */
    private List<SelectItem> listaControleRadio3 = null;

    /** Atributo cdTipoModalidadeHistorico. */
    private Integer cdTipoModalidadeHistorico = null;

    /** Atributo listaModalidadeHistorico. */
    private List<SelectItem> listaModalidadeHistorico = new ArrayList<SelectItem>();

    /** Atributo listaModalidadeHistoricoHash. */
    private Map<Integer, String> listaModalidadeHistoricoHash = new HashMap<Integer, String>();

    /** Atributo listaGridPesquisaHistoricoModalidade. */
    private List<ListarConManServicoSaidaDTO> listaGridPesquisaHistoricoModalidade = null;

    /** Atributo listaControleRadioHistoricoModalidade. */
    private List<SelectItem> listaControleRadioHistoricoModalidade = null;

    /** Atributo itemSelecionadoListaHistoricoModalidade. */
    private Integer itemSelecionadoListaHistoricoModalidade = null;

    /** Atributo checkAll. */
    private boolean checkAll = false;

    /** Atributo habilitaCampo. */
    private boolean habilitaCampo = false;

    /** Atributo habilitaCampo2. */
    private boolean habilitaCampo2 = false;

    /** Atributo dsAgruparCorrespondencia. */
    private String dsAgruparCorrespondencia = null;

    /** Atributo rdoAbertContaBancoPostBradSeg. */
    private String rdoAbertContaBancoPostBradSeg = null;

    /** Atributo cboAcaoParaNaoComprovacao. */
    private Long cboAcaoParaNaoComprovacao = null;

    /** Atributo dsAcaoParaNaoComprovacao. */
    private String dsAcaoParaNaoComprovacao = null;

    /** Atributo rdoAdesaoSacadoEletronico. */
    private String rdoAdesaoSacadoEletronico = null;

    /** Atributo cboAgendamentoDebitosPendentesVeiculos. */
    private Long cboAgendamentoDebitosPendentesVeiculos = null;

    /** Atributo dsAgendamentoDebitosPendentesVeiculos. */
    private String dsAgendamentoDebitosPendentesVeiculos = null;

    /** Atributo rdoAgendarTituloRastreadoFilial. */
    private String rdoAgendarTituloRastreadoFilial = null;

    /** Atributo rdoAgendarTituloRastProp. */
    private String rdoAgendarTituloRastProp = null;

    /** Atributo rdoAgruparCorresp. */
    private String rdoAgruparCorresp = null;

    /** Atributo rdoAutComplementarAg. */
    private String rdoAutComplementarAg = null;

    /** Atributo rdoExigeCpfCnpj. */
    private String rdoExigeCpfCnpj = null;

    /** Atributo rdoExigeCpfCnpj. */
    private String rdoValidaCamara = null;

    /** Atributo rdoExigeCpfCnpj. */
    private String rdoConsisteConta = null;

    /** Atributo rdoBloqEmissaoPapeleta. */
    private String rdoBloqEmissaoPapeleta = null;

    /** Atributo cboCadUtilizadoRec. */
    private Long cboCadUtilizadoRec = null;

    /** Atributo dsCadUtilizadoRec. */
    private String dsCadUtilizadoRec = null;

    /** Atributo cboCadConsultaEndEnvio. */
    private Long cboCadConsultaEndEnvio = null;

    /** Atributo dsCadConsultaEndEnvio. */
    private String dsCadConsultaEndEnvio = null;

    /** Atributo rdoCapTitulosDtReg. */
    private String rdoCapTitulosDtReg = null;

    /** Atributo dsCapituloTituloRegistro. */
    private String dsCapituloTituloRegistro = null;

    /** Atributo cobrarTarifasFunc. */
    private String cobrarTarifasFunc = null;

    /** Atributo codigoFormulario. */
    private String codigoFormulario = null;

    /** Atributo cboCondicaoEnquadramento. */
    private Long cboCondicaoEnquadramento = null;

    /** Atributo dsCondicaoEnquadramento. */
    private String dsCondicaoEnquadramento = null;

    /** Atributo cboContExpCredConta. */
    private Long cboContExpCredConta = null;

    /** Atributo dsContExpCredConta. */
    private String dsContExpCredConta = null;

    /** Atributo cboCritCompEnquadramento. */
    private Long cboCritCompEnquadramento = null;

    /** Atributo dsCritCompEnquadramento. */
    private String dsCritCompEnquadramento = null;

    /** Atributo cboCritPrincEnquadramento. */
    private Long cboCritPrincEnquadramento = null;

    /** Atributo dsCritPrincEnquadramento. */
    private String dsCritPrincEnquadramento = null;

    /** Atributo dtInicioBloqPapeleta. */
    private Date dtInicioBloqPapeleta = null;

    /** Atributo dtInicioRastreamento. */
    private Date dtInicioRastreamento = null;

    /** Atributo dtFimPerAcertoDados. */
    private Date dtFimPerAcertoDados = null;

    /** Atributo dtFimRecadastramento. */
    private Date dtFimRecadastramento = null;

    /** Atributo dtInicioPerAcertoDados. */
    private Date dtInicioPerAcertoDados = null;

    /** Atributo dtInicioRecadastramento. */
    private Date dtInicioRecadastramento = null;

    /** Atributo dtLimiteEnqConvContaSal. */
    private Date dtLimiteEnqConvContaSal = null;

    /** Atributo dtLimiteVincClienteBenef. */
    private Date dtLimiteVincClienteBenef = null;

    /** Atributo rdoDemonstraInfAreaRes. */
    private String rdoDemonstraInfAreaRes = null;

    /** Atributo cboDestinoEnvio. */
    private Long cboDestinoEnvio = null;// Destino de Envio

    /** Atributo dsDestinoEnvio. */
    private String dsDestinoEnvio = null;

    /** Atributo cboDestinoEnvioCorresp. */
    private Long cboDestinoEnvioCorresp = null;

    /** Atributo dsDestinoEnvioCorresp. */
    private String dsDestinoEnvioCorresp = null;

    /** Atributo rdoEfetuaConEspBenef. */
    private String rdoEfetuaConEspBenef = null;

    /** Atributo rdoEmissaoAntCartaoContSal. */
    private String rdoEmissaoAntCartaoContSal = null;

    /** Atributo rdoEmiteAvitoCompVida. */
    private String rdoEmiteAvitoCompVida = null;

    /** Atributo rdoEmiteMsgRecadMidia. */
    private String rdoEmiteMsgRecadMidia = null;

    /** Atributo rdoExigeLibLoteProcessado. */
    private String rdoExigeLibLoteProcessado = null;

    /** Atributo cboFormaAutPagamentos. */
    private Long cboFormaAutPagamentos = null;

    /** Atributo dsFormaAutPagamentos. */
    private String dsFormaAutPagamentos = null;

    /** Atributo cboFormaEnvioPagamento. */
    private Long cboFormaEnvioPagamento = null;

    /** Atributo dsFormaEnvioPagamento. */
    private String dsFormaEnvioPagamento = null;

    /** Atributo cboFormaManCadFavorecido. */
    private Long cboFormaManCadFavorecido = null;

    /** Atributo dsFormaManCadFavorecido. */
    private String dsFormaManCadFavorecido = null;

    /** Atributo cboFormaManCadProcurador. */
    private Long cboFormaManCadProcurador = null;

    /** Atributo dsFormaManCadProcurador. */
    private String dsFormaManCadProcurador = null;

    /** Atributo formularioImpressao. */
    private String formularioImpressao = null;

    /** Atributo rdoGerarLancFutCred. */
    private String rdoGerarLancFutCred = null;

    /** Atributo rdoGerarLancFutDeb. */
    private String rdoGerarLancFutDeb = null;

    /** Atributo rdoGerarLancProgramado. */
    private String rdoGerarLancProgramado = null;

    /** Atributo rdoUtilizaLancPersonalizado. */
    private String rdoUtilizaLancPersonalizado = null;

    /** Atributo rdoGerarRetornoOperRealizadaInternet. */
    private String rdoGerarRetornoOperRealizadaInternet = null;

    /** Atributo rdoGerarRetornoSeparadoCanal. */
    private String rdoGerarRetornoSeparadoCanal = null;

    /** Atributo rdtoCadastroFloating. */
    private Integer rdtoCadastroFloating = null;

    /** Atributo descRdtoCadastroFloating. */
    private String descRdtoCadastroFloating = null;

    /** Atributo rdoEfetuarManutencaoCadastroProcuradores. */
    private String rdoEfetuarManutencaoCadastroProcuradores = null;

    /** Atributo rdoInformaAgenciaContaCred. */
    private String rdoInformaAgenciaContaCred = null;

    /** Atributo cboMeioDisponibilizacaoCorrentista. */
    private Long cboMeioDisponibilizacaoCorrentista = null;

    /** Atributo dsMeioDisponibilizacaoCorrenstista. */
    private String dsMeioDisponibilizacaoCorrenstista = null;

    /** Atributo cboMeioDisponibilizacaoNaoCorrentista. */
    private Long cboMeioDisponibilizacaoNaoCorrentista = null;

    /** Atributo dsMeioDisponibilizacaoNaoCorrenstista. */
    private String dsMeioDisponibilizacaoNaoCorrenstista = null;

    /** Atributo cboMidiaDispCorrentista. */
    private Long cboMidiaDispCorrentista = null;

    /** Atributo dsMidiaDispCorrentista. */
    private String dsMidiaDispCorrentista = null;

    /** Atributo cboMidiaMsgRecad. */
    private Long cboMidiaMsgRecad = null;

    /** Atributo dsMidiaMsgRecad. */
    private String dsMidiaMsgRecad = null;

    /** Atributo cboMomentoEnvio. */
    private Long cboMomentoEnvio = null;

    /** Atributo dsMomentoEnvio. */
    private String dsMomentoEnvio = null;

    /** Atributo cboMomentoIndCredEfetivado. */
    private Long cboMomentoIndCredEfetivado = null;

    /** Atributo dsMomentoIndCredEfetivado. */
    private String dsMomentoIndCredEfetivado = null;

    /** Atributo cboOcorrenciaDebito. */
    private Long cboOcorrenciaDebito = null;

    /** Atributo dsOcorrenciaDebito. */
    private String dsOcorrenciaDebito = null;

    /** Atributo percMaxRegInconsistente. */
    private Integer percMaxRegInconsistente = null;

    /** Atributo cboPeriodicidadeEmissao. */
    private Integer cboPeriodicidadeEmissao = null;

    /** Atributo dsPeriodicidadeEmissao. */
    private String dsPeriodicidadeEmissao = null;// Periocidade de Emiss�o

    /** Atributo cboPeriocidadeManCadProc. */
    private Integer cboPeriocidadeManCadProc = null;

    /** Atributo itemSelecionadListaBanco. */
    private Integer itemSelecionadListaBanco = null;

    /** Atributo dsPeriocidadeManCadProc. */
    private String dsPeriocidadeManCadProc = null;

    /** Atributo cboPeriodicidadeEnvioRemessaManutencao. */
    private Integer cboPeriodicidadeEnvioRemessaManutencao = null;

    /** Atributo dsPeriodicidadeEnvioRemessaManutencao. */
    private String dsPeriodicidadeEnvioRemessaManutencao = null;

    /** Atributo cboPeriodicidadePesquisaDebitosPendentesVeiculos. */
    private Integer cboPeriodicidadePesquisaDebitosPendentesVeiculos = null;

    /** Atributo dsPeriodicidadePesquisaDebitosPendentesVeiculos. */
    private String dsPeriodicidadePesquisaDebitosPendentesVeiculos = null;

    /** Atributo rdoPermiteAcertosDados. */
    private String rdoPermiteAcertosDados = null;

    /** Atributo rdoPermiteAnteciparRecadastramento. */
    private String rdoPermiteAnteciparRecadastramento = null;

    /** Atributo rdoPermiteContigenciaPagamento. */
    private String rdoPermiteContigenciaPagamento = null;

    /** Atributo rdoPermiteCorrespondenciaAberta. */
    private String rdoPermiteCorrespondenciaAberta = null;

    /** Atributo rdoPermiteDebitoOnline. */
    private String rdoPermiteDebitoOnline = null;

    /** Atributo rdoPermiteEnvioRemessaManutencaoCadastro. */
    private String rdoPermiteEnvioRemessaManutencaoCadastro = null;

    /** Atributo cboPermiteEstornoPagamento. */
    private Long cboPermiteEstornoPagamento = null;

    /** Atributo dsPermiteEstornoPagamento. */
    private String dsPermiteEstornoPagamento = null;

    /** Atributo rdoPermiteFavorecidoConsultarPagamento. */
    private String rdoPermiteFavorecidoConsultarPagamento = null;

    /** Atributo rdoPermitePagarMenor. */
    private String rdoPermitePagarMenor = null;

    /** Atributo rdoPermitePagarVencido. */
    private String rdoPermitePagarVencido = null;

    /** Atributo rdoPermitirAgrupamentoCorrespondencia. */
    private String rdoPermitirAgrupamentoCorrespondencia = null;

    /** Atributo rdoPesquisaDe. */
    private String rdoPesquisaDe = null;

    /** Atributo rdoPossuiExpiracaoCredito. */
    private String rdoPossuiExpiracaoCredito = null;

    /** Atributo rdoPreAutorizacaoCliente. */
    private String rdoPreAutorizacaoCliente = null;

    /** Atributo cboPrioridadeDebito. */
    private Long cboPrioridadeDebito = null;

    /** Atributo dsPrioridDebito. */
    private String dsPrioridDebito = null;

    /** Atributo cboProcessamentoEfetivacaoPagamento. */
    private Long cboProcessamentoEfetivacaoPagamento = null;
    
    private Long testeOrigem = null;
    
    private String dsOrigem = null;
    
    private String dsIndicador;
	
	private int dsOrigemIndicador;

    /** Atributo dsProcessamentoEfetivacaoPagamento. */
    private String dsProcessamentoEfetivacaoPagamento = null;
    
    /** Atributo qtddDiasFloating. */
    private Integer qtddDiasFloating = null;

    /** Atributo qtddDiasRepique. */
    private Integer qtddDiasRepique = null;

    /** Atributo qtddDiasEmissaoAvisoAntesInicioComprovacao. */
    private Integer qtddDiasEmissaoAvisoAntesInicioComprovacao = null;

    /** Atributo qtddDiasEmissaoAvisoAntesVencimento. */
    private Integer qtddDiasEmissaoAvisoAntesVencimento = null;

    /** Atributo qtddDiasExpiracaoCredito. */
    private String qtddDiasExpiracaoCredito = null;

    /** Atributo qtddDiasAvisoAntecipado. */
    private Integer qtddDiasAvisoAntecipado = null;

    /** Atributo qtddDiasEnvioAntecipadoFormulario. */
    private Integer qtddDiasEnvioAntecipadoFormulario = null;

    /** Atributo quantidadeDiasExpiracao. */
    private Integer quantidadeDiasExpiracao = null;

    /** Atributo dsLocalEmissao. */
    private String dsLocalEmissao = null;

    /** Atributo cdLocalEmissao. */
    private Integer cdLocalEmissao = null;

    /** Atributo qtddDiasInativacaoFavorecido. */
    private Integer qtddDiasInativacaoFavorecido = null;

    /** Atributo qtddFasesPorEtapa. */
    private Integer qtddFasesPorEtapa = null;

    /** Atributo qtddLinhasPorComprovante. */
    private Integer qtddLinhasPorComprovante = null;

    /** Atributo quantidadeMesesEmissao. */
    private Integer quantidadeMesesEmissao = null;

    /** Atributo qtddMesesPeriodicidadeComprovacao. */
    private Integer qtddMesesPeriodicidadeComprovacao = null;

    /** Atributo qtddMesesPorEtapa. */
    private Integer qtddMesesPorEtapa = null;

    /** Atributo qtddMesesPorFase. */
    private Integer qtddMesesPorFase = null;

    /** Atributo qtddViasEmitir. */
    private Integer qtddViasEmitir = null;

    /** Atributo qtddViasPagasRegistro. */
    private Integer qtddViasPagasRegistro = null;

    /** Atributo quantidadeEtapasRecadastramento. */
    private Integer quantidadeEtapasRecadastramento = null;

    /** Atributo qtddLimiteCartaoContaSalarioSolicitacao. */
    private Integer qtddLimiteCartaoContaSalarioSolicitacao = null;

    /** Atributo quantidadeLimiteDiasPagamentoVencido. */
    private Integer quantidadeLimiteDiasPagamentoVencido = null;
    
    /** Atributo qtddMaximaRegistrosInconsistentesLote. */
    private Integer qtddMaximaRegistrosInconsistentesLote = null;

    /** Atributo qtdDiasReutilizacaoControlePgto. */
    private Integer qtdDiasReutilizacaoControlePgto = null;

    /** Atributo rdoRastrearNotasFiscais. */
    private String rdoRastrearNotasFiscais = null;

    /** Atributo rdoRastrearTituloTerceiros. */
    private String rdoRastrearTituloTerceiros = null;

    /** Atributo textoInformacaoAreaReservada. */
    private String textoInformacaoAreaReservada = null;

    /** Atributo cboTipoCartaoContaSalario. */
    private Long cboTipoCartaoContaSalario = null;

    /** Atributo dsTipoCartaoContaSalario. */
    private String dsTipoCartaoContaSalario = null;

    /** Atributo cboTipoConsistenciaCpfCnpjProprietario. */
    private Long cboTipoConsistenciaCpfCnpjProprietario = null;

    /** Atributo dsTipoConsistenciaCpfCnpjProprietario. */
    private String dsTipoConsistenciaCpfCnpjProprietario = null;

    /** Atributo cboTipoConsistenciaIdentificacaoBeneficiario. */
    private Long cboTipoConsistenciaIdentificacaoBeneficiario = null;

    /** Atributo dsTipoConsistenciaIdentificacaoBeneficiario. */
    private String dsTipoConsistenciaIdentificacaoBeneficiario = null;

    /** Atributo cboTipoCargaCadastroBeneficiario. */
    private Long cboTipoCargaCadastroBeneficiario = null;

    /** Atributo dsTipoCargaCadastroBeneficiario. */
    private String dsTipoCargaCadastroBeneficiario = null;

    /** Atributo cboTipoConsistenciaInscricaoFavorecido. */
    private Long cboTipoConsistenciaInscricaoFavorecido = null;

    /** Atributo dsTipoConsistenciaInscricaoFavorecido. */
    private String dsTipoConsistenciaInscricaoFavorecido = null;

    /** Atributo cboTipoConsistenciaCpfCnpjFavorecido. */
    private Long cboTipoConsistenciaCpfCnpjFavorecido = null;

    /** Atributo dsTipoConsistenciaCpfCnpjFavorecido. */
    private String dsTipoConsistenciaCpfCnpjFavorecido = null;

    /** Atributo cboTipoConsolidacaoPagamentosComprovante. */
    private Long cboTipoConsolidacaoPagamentosComprovante = null;

    /** Atributo dsTipoConsolidacaoPagamentosComprovante. */
    private String dsTipoConsolidacaoPagamentosComprovante = null;

    /** Atributo dsIndicadorUtilizaMora. */
    private String dsIndicadorUtilizaMora = null;

    /** Atributo cboTipoConsultaSaldo. */
    private Long cboTipoConsultaSaldo = null;

    /** Atributo dsTipoConsSaldo. */
    private String dsTipoConsSaldo = null;

    /** Atributo cboTipoContaCredito. */
    private Long cboTipoContaCredito = null;

    /** Atributo dsTipoContaCredito. */
    private String dsTipoContaCredito = null;

    /** Atributo cboTipoDataControleFloating. */
    private Long cboTipoDataControleFloating = null;

    /** Atributo dsTipoDataControleFloating. */
    private String dsTipoDataControleFloating = null;

    /** Atributo cboTipoFormacaoListaDebito. */
    private Long cboTipoFormacaoListaDebito = null;

    /** Atributo dsTipoFormacaoListaDebito. */
    private String dsTipoFormacaoListaDebito = null;

    /** Atributo cboTipoInscricaoFavorecido. */
    private Long cboTipoInscricaoFavorecido = null;

    /** Atributo dsTipoInscricaoFavorecido. */
    private String dsTipoInscricaoFavorecido = null;

    /** Atributo cboTipoRastreamentoTitulo. */
    private Long cboTipoRastreamentoTitulo = null;

    /** Atributo dsTipoRastreamentoTitulo. */
    private String dsTipoRastreamentoTitulo = null;

    /** Atributo cboTipoRejeicaoLote. */
    private Long cboTipoRejeicaoLote = null;

    /** Atributo dsTipoRejeicaoLote. */
    private String dsTipoRejeicaoLote = null;

    /** Atributo cboTipoRejeicaoEfetivacao. */
    private Long cboTipoRejeicaoEfetivacao = null;

    /** Atributo dsTipoRejEfetivacao. */
    private String dsTipoRejEfetivacao = null;

    /** Atributo cboTipoRejeicaoAgendamento. */
    private Long cboTipoRejeicaoAgendamento = null;

    /** Atributo dsTipoRejAgendamento. */
    private String dsTipoRejAgendamento = null;

    /** Atributo cboTipoTratamentoContaTransferida. */
    private Long cboTipoTratamentoContaTransferida = null;

    /** Atributo dsTipoTratamentoContaTransferida. */
    private String dsTipoTratamentoContaTransferida = null;

    /** Atributo cboTipoIdentificacaoBeneficiario. */
    private Long cboTipoIdentificacaoBeneficiario = null;

    /** Atributo dsTipoIdentificacaoBeneficiario. */
    private String dsTipoIdentificacaoBeneficiario = null;

    /** Atributo cboTratamentoFeriadosDataPagamento. */
    private Long cboTratamentoFeriadosDataPagamento = null;

    /** Atributo dsTratFeriadosDataPagamento. */
    private String dsTratFeriadosDataPagamento = null;

    /** Atributo cboTratamentoFeriadoFimVigenciaCredito. */
    private Long cboTratamentoFeriadoFimVigenciaCredito = null;

    /** Atributo dsTratamentoFeriadoFimVigenciaCredito. */
    private String dsTratamentoFeriadoFimVigenciaCredito = null;

    /** Atributo cboTratamentoListaDebitoSemNumeracao. */
    private Long cboTratamentoListaDebitoSemNumeracao = null;

    /** Atributo dsTratamentoListaDebitoSemNumeracao. */
    private String dsTratamentoListaDebitoSemNumeracao = null;

    /** Atributo cboTratamentoValorDivergente. */
    private Long cboTratamentoValorDivergente = null;

    /** Atributo dsTratamentoValorDivergente. */
    private String dsTratamentoValorDivergente = null;

    /** Atributo rdoUtilizaCadastroFavorecidoControlePagamentos. */
    private String rdoUtilizaCadastroFavorecidoControlePagamentos = null;

    /** Atributo rdoUtilizaCadastroOrgaosPagadores. */
    private String rdoUtilizaCadastroOrgaosPagadores = null;

    /** Atributo rdoUtilizaCadastroProcuradores. */
    private String rdoUtilizaCadastroProcuradores = null;

    /** Atributo rdoUtilizaFrasesPreCadastradas. */
    private String rdoUtilizaFrasesPreCadastradas = null;

    /** Atributo rdoUtilizaListaDebitos. */
    private String rdoUtilizaListaDebitos = null;

    /** Atributo rdoUtilizaMora. */
    private String rdoUtilizaMora = null;

    /** Atributo rdoUtilizaMensagemPersonalizada. */
    private String rdoUtilizaMensagemPersonalizada = null;

    /** Atributo rdoValidarNomeFavorecidoReceitaFederal. */
    private String rdoValidarNomeFavorecidoReceitaFederal = null;

    /** Atributo valorLimiteDiario. */
    private BigDecimal valorLimiteDiario = null;

    /** Atributo valorLimiteIndividual. */
    private BigDecimal valorLimiteIndividual = null;

    /** Atributo valorMaximoPagamentoFavorecidoNaoCadastrado. */
    private BigDecimal valorMaximoPagamentoFavorecidoNaoCadastrado = null;

    /** Atributo rdoAgendamentoDebitosPendentesVeiculos. */
    private String rdoAgendamentoDebitosPendentesVeiculos = null;

    /** Atributo rdoIndicadorSegundaLinhaExtrato. */
    private Integer rdoIndicadorSegundaLinhaExtrato = null;
    
    private String dsRdoIndicadorSegundaLinhaExtrato = null;
    
    /** Atributo rdoTipoFormatacaoPrimeiraLinha. */
    private String rdoTipoFormatacaoPrimeiraLinha = null;

    /** Atributo rdoIndicadorDdaRetorno. */
    private String rdoIndicadorDdaRetorno = null;

    /** Atributo rdoPermiteAgAutGrade. */
    private String rdoPermiteAgAutGrade = null;

    /** Atributo dsIndicadorSegundaLinhaExtrato. */
    private String dsIndicadorSegundaLinhaExtrato = null;

    /** Atributo possuiModalidade. */
    private Boolean possuiModalidade = false;

    /** Atributo desabilitaTipoFormListaDebito. */
    private boolean desabilitaTipoFormListaDebito = false;

    /** Atributo desabilitaTratamentoListaDebitoSemNumeracao. */
    private boolean desabilitaTratamentoListaDebitoSemNumeracao = false;

    /** Atributo desabilitaPeriodicidadePesquisaDebPendVeiculos. */
    private boolean desabilitaPeriodicidadePesquisaDebPendVeiculos = false;

    /** Atributo desabilitaGerarRetornoSeparado. */
    private boolean desabilitaGerarRetornoSeparado = false;

    /** Atributo desabilitaTpCartaoQtdeLimite. */
    private boolean desabilitaTpCartaoQtdeLimite = false;

    /** Atributo desabilitaCamposProcuradores. */
    private boolean desabilitaCamposProcuradores = false;

    /** Atributo desabilitaCamposManCadProcuradores. */
    private boolean desabilitaCamposManCadProcuradores = false;

    /** Atributo desabilitaCamposExpiracaoCredito. */
    private boolean desabilitaCamposExpiracaoCredito = false;

    /** Atributo desabilitaCamposEnvioRemessaMan. */
    private boolean desabilitaCamposEnvioRemessaMan = false;

    /** Atributo desabilitaCamposDataAcertoDados. */
    private boolean desabilitaCamposDataAcertoDados = false;

    /** Atributo desabilitaCamposMsgOnline. */
    private boolean desabilitaCamposMsgOnline = false;

    /** Atributo desabilitarCamposLote. */
    private boolean desabilitarCamposLote = false;

    /** Atributo desabilitarCamposPercLote. */
    private boolean desabilitarCamposPercLote = false;

    /** Atributo desabilitarCamposQtdeLote. */
    private boolean desabilitarCamposQtdeLote = false;

    /** Atributo desabilitaValorMaximoFavNaoCad. */
    private boolean desabilitaValorMaximoFavNaoCad = false;

    /** Atributo desabilitarCamposEmiteAvisos. */
    private boolean desabilitarCamposEmiteAvisos = false;

    /** Atributo desabilitarCampoCadastroEndereco. */
    private boolean desabilitarCampoCadastroEndereco = false;

    /** Atributo desabilitarCampoQtdeLimDiasPgtoVenc. */
    private boolean desabilitarCampoQtdeLimDiasPgtoVenc = false;

    /** Atributo saidaVerificarAtributo. */
    private VerificarAtributosServicoModalidadeSaidaDTO saidaVerificarAtributo = null;

    /** Atributo opcaoChecarTodos. */
    private boolean opcaoChecarTodos = false;

    /** Atributo listaAmbiente. */
    private List<ListarAmbienteOcorrenciaSaidaDTO> listaAmbiente = null;

    /** Atributo itemSelecionadoGridAmbiente. */
    private Integer itemSelecionadoGridAmbiente = null;

    /** Atributo listaAmbienteControle. */
    private List<SelectItem> listaAmbienteControle = null;

    /** Atributo tipoServicoSelecionado. */
    private Integer tipoServicoSelecionado = null;

    /** Atributo listaComboTipoServicoAmbiente. */
    private List<SelectItem> listaComboTipoServicoAmbiente = new ArrayList<SelectItem>();

    /** The entrada validar vinculacao convenio conta. */
    private ValidarVinculacaoConvenioContaSalarioEntradaDTO entradaValidarVinculacaoConvenioConta;

    /** The entrada incluir pagamento salario. */
    private IncluirPagamentoSalarioEntradaDTO entradaIncluirPagamentoSalario;

    /** The saida incluir pagamento salario. */
    private IncluirPagamentoSalarioSaidaDTO saidaIncluirPagamentoSalario;

    /** The saida validar vinculacao convenio conta. */
    private ValidarVinculacaoConvenioContaSalarioSaidaDTO saidaValidarVinculacaoConvenioConta =  new ValidarVinculacaoConvenioContaSalarioSaidaDTO();

    /** Atributo listaComboTipoServicoAmbienteHash. */
    private Map<Integer, ListarModalidadeTipoServicoSaidaDTO> listaComboTipoServicoAmbienteHash = new HashMap<Integer, ListarModalidadeTipoServicoSaidaDTO>();

    /** Atributo listaComboTipoServicoHash. */
    private Map<Integer, ListaTipoServicoContratoSaidaDTO> listaComboTipoServicoHash = new HashMap<Integer, ListaTipoServicoContratoSaidaDTO>();

    /** Atributo cpfCnpjParticipanteSelecionado. */
    private String cpfCnpjParticipanteSelecionado = null;

    /** Atributo nomeRazaoParticipanteSelecionado. */
    private String nomeRazaoParticipanteSelecionado = null;

    /** Atributo dsTipoServicoAmbiente. */
    private String dsTipoServicoAmbiente = null;

    /** Atributo dsAmbienteServico. */
    private String dsAmbienteServico = null;

    /** Atributo dsNomeRazaoSocilaAmbiente. */
    private String dsNomeRazaoSocilaAmbiente = null;

    /** Atributo cpfCnpjAmbiente. */
    private String cpfCnpjAmbiente = null;

    /** Atributo listaGridParticipantes. */
    private List<ListarParticipantesSaidaDTO> listaGridParticipantes = null;

    /** Atributo listaGridControleParticipantes. */
    private List<SelectItem> listaGridControleParticipantes = null;

    /** Atributo itemSelecionadoGridParticipantes. */
    private Integer itemSelecionadoGridParticipantes = null;

    /** Atributo participanteSelecionado. */
    private ListarParticipantesSaidaDTO participanteSelecionado = null;

    /** Atributo participanteSelecionadoHistorico. */
    private ListarParticipantesSaidaDTO participanteSelecionadoHistorico = null;

    /** Atributo tipoServicoSelecionadoHistoricoAmbiente. */
    private Integer tipoServicoSelecionadoHistoricoAmbiente = null;

    /** Atributo itemSelecionadoListaHistoricoManutencaoAmbiente. */
    private Integer itemSelecionadoListaHistoricoManutencaoAmbiente = null;

    /** Atributo listaHistoricoManutencaoAmbiente. */
    private List<ListarConManAmbPartOcorrenciatSaidaDTO> listaHistoricoManutencaoAmbiente = null;

    /** Atributo listaControleHistoricoManutencaoAmbiente. */
    private List<SelectItem> listaControleHistoricoManutencaoAmbiente = null;

    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao = null;

    /** Atributo cdConsultaSaldoValorSuperior. */
    private Long cdConsultaSaldoValorSuperior = null;

    /** Atributo dsConsultaSaldoValorSuperior. */
    private String dsConsultaSaldoValorSuperior = null;

    /** Atributo feriadosLocais. */
    private List<SelectItem> feriadosLocais = null;

    /** Atributo cdIndicadorFeriadoLocal. */
    private Integer cdIndicadorFeriadoLocal = null;

    /** Atributo dsIndicadorFeriadoLocal. */
    private String dsIndicadorFeriadoLocal = null;

    /** Atributo habilitaCampoTipServicoForncSalTrib. */
    private boolean habilitaCampoTipServicoForncSalTrib = false;

    /** Atributo listaFeriadoNacional. */
    private List<ConsultarListaValoresDiscretosSaidaDTO> listaFeriadoNacional = null;
    
    private List<ConsultarListaValoresDiscretosSaidaDTO> listaComboTeste = null;

    /** Atributo saidaTipoLayoutArquivoSaidaDTO. */
    private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO = null;

    /** Atributo cdFlagRepresentante. */
    private String cdFlagRepresentante = null;

    /** Atributo cdMidiaDisponComprovante. */
    private String cdMidiaDisponComprovante = null;

    /** Atributo qtFuncionarios. */
    private Long qtFuncionarios = null;

    /** Atributo qtMesesEmissao. */
    private Integer qtMesesEmissao = null;

    /** Atributo qtEmissoesPagasEmpresa. */
    private String qtEmissoesPagasEmpresa = null;

    /** The cd produto operacao relacionado. */
    private Integer cdProdutoOperacaoRelacionado;

    /** The cd produto servico operacao. */
    private Integer cdProdutoServicoOperacao;

    /** The permite alteracoes titulo rastreado. */
    private boolean permiteAlteracoesTituloRastreado;

    /** The habilita qtd dias floating. */
    private boolean habilitaQtdDiasFloating;

    /** The habilita rdo gerar lancamento. */
    private boolean habilitaRdoGerarLancamento;

    /** The rdo exige identificacao filial autorizacao. */
    private String rdoExigeIdentificacaoFilialAutorizacao;

    /** The flag exibe campos. */
    private String flagExibeCampos = "N";

    /** Atributo descricaoSegundaModalidade. */
    private String descricaoSegundaModalidade ="";

    /** Atributo aberturaContaSalario. */
    private String aberturaContaSalario;

    /** Atributo chkLoteQuinzenal. */
    private boolean chkLoteQuinzenal;

    /** Atributo chkLoteMensal. */
    private boolean chkLoteMensal;

    /** Atributo txtValorFolhaPgamento. */
    private BigDecimal txtValorFolhaPgamento;

    /** Atributo txtQtdFuncionarios. */
    private String txtQtdFuncionarios;

    /** Atributo txtMediaSalarial. */
    private BigDecimal txtMediaSalarial;

    /** Atributo txtMesAnoCorrespondente. */
    private String txtMesAnoCorrespondente;

    /** Atributo aberturaContaSalarioConf. */
    private String aberturaContaSalarioConf;

    /** Atributo tipoLotePagamento. */
    private String tipoLotePagamentoConf;

    /** Atributo txtValorFolhaPgamentoConf. */
    private BigDecimal txtValorFolhaPgamentoConf;

    /** Atributo txtQtdFuncionariosConf. */
    private String txtQtdFuncionariosConf;

    /** Atributo txtMediaSalarialConf. */
    private BigDecimal txtMediaSalarialConf;

    /** Atributo txtMesAnoCorrespondenteConf. */
    private String txtMesAnoCorrespondenteConf;

    /** Atributo dsPreenchimentoLancamentoPersonalizado. */
    private String dsPreenchimentoLancamentoPersonalizado;

    /** Atributo dsTituloDdaRetorno. */
    private String dsTituloDdaRetorno;

    /** Atributo dsIndicadorAgendaGrade. */
    private String dsIndicadorAgendaGrade;

    /** Atributo listaRepresentantes. */
    private ListarRepresentanteSaidaDTO listaRepresentantes = new ListarRepresentanteSaidaDTO();

    /** The lista representantes ocorrencias selecionadas. */
    private List<ListarRepresentanteOcorrenciaDTO> listaRepresentantesOcorrenciasSelecionadas = new ArrayList<ListarRepresentanteOcorrenciaDTO>();

    /** Atributo foco. */
    private String foco;

    /** Atributo urlRenegociacaoNovo. */
    private String urlRenegociacaoNovo;

    /** Atributo qtDiaUtilPgto. */
    private Integer qtDiaUtilPgto;
    
    private Long comboRetornoOperacoesSelecionado = 0l;
    private List<SelectItem> listaRetornoOperacoes = new ArrayList<SelectItem>();
    private String dsRetornoOperacoes = null;

    /** Atributo vlPercentualDiferencaTolerada. */
    private BigDecimal vlPercentualDiferencaTolerada = null;
    
    /** Atributo cdFloatServicoContrato. */
    private Integer cdFloatServicoContrato = null;
    
    /** Atributo dsFloatServicoContrato. */
    private String dsTipoDataFloating  = null;
    
    private boolean exibeOrigemSegundaViaExtrato;
    
    private String radioFidelize = null;
    
    private String dsEmailEmpresa = null;
    
    private String dsEmailAgencia = null;
    
    /** Atributo fidelizeConf. */
    private String fidelizeConf = null;
     
    private String altoTurnover = null;
    
    private String ofertaCartaoPre = null;

	private String peloApp = null;

	private String altoTurnoverConf = null;
	
	private String ofertaCartaoPreConf = null;
	
	private String peloAppConf = null;
	
	private Long comboCodIndicadorTipoRetornoInternet = null;

	private List<String> listaTipoRetornoInternet = new ArrayList<String>();
    
	private List<SelectItem> listaCodIndicadorTipoRetornoInternet = new ArrayList<SelectItem>();
    private String dsCodIndicadorTipoRetornoInternet = null;
    public String descTipoManutencao = null;
    public Integer renderizaCombo;
   
    
	/**
     * Limpar data papeleta.
     */
    public void limparDataPapeleta() {
        setDtInicioBloqPapeleta(null);
        if (getRdoBloqEmissaoPapeleta().equals("1")) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(GregorianCalendar.DAY_OF_MONTH, +1);
            setDtInicioBloqPapeleta(calendar.getTime());
        }
    }

    /**
     * Checa todos.
     */
    public void checaTodos() {
        // Marca/Desmarcar todos de uma s� vez.
        for (int i = 0; i < getListaGridModalidade().size(); i++) {
            getListaGridModalidade().get(i).setCheck(isCheckAll());
        }
        setBtnIncluir(isCheckAll());
    }

    /**
     * Checa todos relacionada.
     */
    public void checaTodosRelacionada() {
        for (int i = 0; i < listaGridModalidadeRelacionada.size(); i++) {
            listaGridModalidadeRelacionada.get(i).setCheck(isCheckAll());
        }
        atualizarCountCheckedModalidadeRelacionada();
    }

    /**
     * Carrega lista tipo layout arquivo.
     */
    private void carregaListaTipoLayoutArquivo() {
        listaTipoLayoutArquivo = new ArrayList<SelectItem>();
        List<TipoLayoutArquivoSaidaDTO> listaTipoLayout = new ArrayList<TipoLayoutArquivoSaidaDTO>();
        TipoLayoutArquivoEntradaDTO tipoLoteEntradaDTO = new TipoLayoutArquivoEntradaDTO();
        tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);

        saidaTipoLayoutArquivoSaidaDTO = comboService
        .listarTipoLayoutArquivo(tipoLoteEntradaDTO);
        listaTipoLayout = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
        listaTipoLayoutArquivoHash.clear();
        for (TipoLayoutArquivoSaidaDTO combo : listaTipoLayout) {
            listaTipoLayoutArquivoHash.put(combo.getCdTipoLayoutArquivo(),
                combo.getDsTipoLayoutArquivo());
            listaTipoLayoutArquivo.add(new SelectItem(combo
                .getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo()));
        }
    }

    /**
     * Carrega lista.
     */
    public void carregaLista() {
        listaGridPesquisa = new ArrayList<ListarModalidadeTipoServicoSaidaDTO>();

		ListarModalidadeTipoServicoEntradaDTO entradaDTO = new ListarModalidadeTipoServicoEntradaDTO();
		entradaDTO.setCdPessoaJuridica(getCdPessoaJuridica());
		entradaDTO.setCdTipoContrato(getCdTipoContrato());
		entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
		entradaDTO.setCdTipoServico(0);
		entradaDTO.setCdModalidade(0);

		setListaGridPesquisa(getManterContratoService().listarModalidadeTipoServico(entradaDTO));
		listaControleRadio = new ArrayList<SelectItem>();

		for (int i = 0; i < getListaGridPesquisa().size(); i++) {
			listaControleRadio.add(new SelectItem(i, " "));
		}
		setItemSelecionadoLista(null);
	}

	/**
	 * Calcular media salarial.
	 */
	public void calcularMediaSalarial() {
		Integer txtQtdFuncionarios = NumberUtils.convertIntegerVolume(this.txtQtdFuncionarios);

		if (txtQtdFuncionarios != null && txtQtdFuncionarios != 0 && getTxtValorFolhaPgamento() != null
				&& getTxtValorFolhaPgamento().compareTo(BigDecimal.ZERO) != 0) {

			BigDecimal valorItem = PgitUtil.isNullBigDecimal(getTxtValorFolhaPgamento());
			BigDecimal qtdeBem = PgitUtil.isNullBigDecimal(NumberUtils.createBigDecimal(txtQtdFuncionarios));

			if (PgitUtil.isBigDecimalMaior(qtdeBem, BigDecimal.ZERO)) {
				setTxtMediaSalarial(valorItem.divide(qtdeBem, 2, RoundingMode.FLOOR));
			} else {
				setTxtMediaSalarial(BigDecimal.ZERO);
			}
		}
	}

    /**
     * Permite alteracoes titulo.
     */
    public void permiteAlteracoesTitulo(){
        if ("2".equals(getCboTipoRastreamentoTitulo().toString())){
            setPermiteAlteracoesTituloRastreado(false);
        }else{
            setPermiteAlteracoesTituloRastreado(true);
        }
    }



    // Carrega combo Periodicidade
    /**
     * Carrega lista periodicidade.
     */
    public void carregaListaPeriodicidade() {

        this.listaPeriodicidade = new ArrayList<SelectItem>();

        List<PeriodicidadeSaidaDTO> saidaDTO = new ArrayList<PeriodicidadeSaidaDTO>();
        PeriodicidadeEntradaDTO entradaDTO = new PeriodicidadeEntradaDTO();
        entradaDTO.setCdSituacaoVinculacaoConta(0);

        saidaDTO = comboService.consultarListaPeriodicidade(entradaDTO);
        listaPeriodicidadeHash.clear();

        for (PeriodicidadeSaidaDTO combo : saidaDTO) {
            listaPeriodicidadeHash.put(combo.getCdPeriodicidade(), combo
                .getDsPeriodicidade());
            this.listaPeriodicidade.add(new SelectItem(combo
                .getCdPeriodicidade(), combo.getDsPeriodicidade()));
        }
    }

    // Carrega combo Tipo de Servico
    /**
     * Carrega lista tipo servico.
     */
    public void carregaListaTipoServico() {
        this.listaTipoServico = new ArrayList<SelectItem>();
        ListarServicosEntradaDTO entrada = new ListarServicosEntradaDTO();
        List<ListarServicosSaidaDTO> listaSaida = new ArrayList<ListarServicosSaidaDTO>();

        entrada.setCdNaturezaServico(0);
        entrada.setNumeroOcorrencias(30);

        listaSaida = getComboService().listarTipoServico(entrada);
        listaTipoServicoHash.clear();

        for (ListarServicosSaidaDTO combo : listaSaida) {
            listaTipoServicoHash
            .put(combo.getCdServico(), combo.getDsServico());
            listaTipoServico.add(new SelectItem(combo.getCdServico(), combo
                .getDsServico()));
        }
    }

    /**
     * Carregar combo tipo servico ambiente.
     */
    public void carregarComboTipoServicoAmbiente() {

        this.listaComboTipoServicoAmbiente = new ArrayList<SelectItem>();

        ListarModalidadeTipoServicoEntradaDTO entrada = new ListarModalidadeTipoServicoEntradaDTO();

        entrada.setCdPessoaJuridica(getCdPessoaJuridica());
        entrada.setCdTipoContrato(getCdTipoContrato());
        entrada.setNrSequenciaContrato(getNrSequenciaContrato());
        entrada.setCdModalidade(0);
        entrada.setCdTipoServico(0);

        List<ListarModalidadeTipoServicoSaidaDTO> saidaCombo = manterContratoService.listarModalidadeTipoServico(entrada);

        listaComboTipoServicoAmbienteHash.clear();

        for (ListarModalidadeTipoServicoSaidaDTO combo : saidaCombo) {
            listaComboTipoServicoAmbienteHash.put(combo
                .getCdProdutoOperacaoRelacionado(), combo);
            listaComboTipoServicoAmbiente.add(new SelectItem(combo
                .getCdProdutoOperacaoRelacionado(), combo
                .getDsProdutoOperacaoRelacionado()));
        }

    }

    /**
     * Carregar combo tipo servico.
     */
    private void carregarComboTipoServico() {
        listaComboTipoServicoAmbiente = new ArrayList<SelectItem>();

        ListaTipoServicoContratoEntradaDTO entrada = new ListaTipoServicoContratoEntradaDTO();
        entrada
        .setNumeroOcorrencias(IComboServiceConstants.NUMERO_OCORRENCIAS_LISTAR_CENTRO_CUSTO);
        entrada.setCdPessoaJuridicaContrato(cdPessoaJuridica);
        entrada.setCdTipoContrato(cdTipoContrato);
        entrada.setNrSequenciaContrato(nrSequenciaContrato);
        entrada.setCdprodutoServicoOperacao(null);
        entrada.setCdProdutoOperacaoRelacionado(null);
        entrada
        .setCdNaturezaOperacaoPagamento(IComboServiceConstants.NATUREZA_OPERACAO_PGTO);

        List<ListaTipoServicoContratoSaidaDTO> lista = comboService
        .listarTipoServicoContrato(entrada);
        listaComboTipoServicoHash.clear();
        for (ListaTipoServicoContratoSaidaDTO element : lista) {
            listaComboTipoServicoHash.put(element
                .getCdProdutoOperacaoRelacionado(), element);
            listaComboTipoServicoAmbiente.add(new SelectItem(element
                .getCdProdutoOperacaoRelacionado(), element
                .getDsProdutoOperacaoRelacionado()));
        }
    }

    /**
     * Preenche dados.
     */
    
    public void preencheDados() {
        setPermiteAlteracaoComboFloating(true);
        
        listaModalidadeTipoServicoSaidaDTO = new ListarModalidadeTipoServicoSaidaDTO();
        listaModalidadeTipoServicoSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());

        setFlagExibeCampos("N");
        
        if (listaModalidadeTipoServicoSaidaDTO.getCdModalidade() != 90200026) {
            setFlagExibeCampos("S");
        }

        DetalharTipoServModContratoEntradaDTO entradaDTO = new DetalharTipoServModContratoEntradaDTO();
        VerificarAtributosServicoModalidadeEntradaDTO entradaVerificarAtributo = new VerificarAtributosServicoModalidadeEntradaDTO();
        saidaVerificarAtributo = new VerificarAtributosServicoModalidadeSaidaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
        entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato());
        entradaDTO.setNrSequenciaContratoNegocio(getNrSequenciaContrato());
        entradaDTO.setCdServico(listaModalidadeTipoServicoSaidaDTO.getCdServico());
        entradaDTO.setCdModalidade(listaModalidadeTipoServicoSaidaDTO.getCdModalidade());
        entradaDTO.setCdParametroPesquisa(1);
        entradaDTO.setCdParametroTela(listaModalidadeTipoServicoSaidaDTO.getCdParametroTela());

        entradaVerificarAtributo.setCdpessoaJuridicaContrato(getCdPessoaJuridica());
        entradaVerificarAtributo.setCdTipoContratoNegocio(getCdTipoContrato());
        entradaVerificarAtributo.setNrSequenciaContratoNegocio(getNrSequenciaContrato());
        entradaVerificarAtributo.setCdProdutoServicoOperacao(listaModalidadeTipoServicoSaidaDTO.getCdServico());
        entradaVerificarAtributo.setCdProdutoOperacaoRelacionado(listaModalidadeTipoServicoSaidaDTO.getCdModalidade());
        entradaVerificarAtributo.setCdParametro(1);

        setCdParametroTela(listaModalidadeTipoServicoSaidaDTO.getCdParametroTela());
        setDsTipoServicoSelecionado(listaModalidadeTipoServicoSaidaDTO.getDsModalidade());
        setDsSituacaoServico(listaModalidadeTipoServicoSaidaDTO.getDsSituacaoServicoRelacionado());

        saidaDTO = getManterContratoService().detalharTipoServModContrato(entradaDTO);
        
        if(saidaDTO.getCdIndicadorTipoRetornoInternet() == 0){
        	setComboCodIndicadorTipoRetornoInternet(Long.parseLong(saidaDTO.getCdIndicadorTipoRetornoInternet().toString()));
        }else if(saidaDTO.getCdIndicadorTipoRetornoInternet() == 1){
        	setComboCodIndicadorTipoRetornoInternet(Long.parseLong(saidaDTO.getCdIndicadorTipoRetornoInternet().toString()));
        }else if(saidaDTO.getCdIndicadorTipoRetornoInternet() == 2){
        	setComboCodIndicadorTipoRetornoInternet(Long.parseLong(saidaDTO.getCdIndicadorTipoRetornoInternet().toString()));
        }else if(saidaDTO.getCdIndicadorTipoRetornoInternet() == 3){
        	setComboCodIndicadorTipoRetornoInternet(Long.parseLong(saidaDTO.getCdIndicadorTipoRetornoInternet().toString()));
        }
        
        saidaVerificarAtributo = getManterContratoService().verificarAtributoServicoModalidade(entradaVerificarAtributo);

        setRdtoCadastroFloating(PgitUtil.verificaIntegerNulo(saidaDTO
            .getCdFloatServicoContrato()));
        
        setDsOrigemIndicador(saidaDTO.getDsOrigemIndicador());
		setDsIndicador(saidaDTO.getDsIndicador());
		
		String teste = String.valueOf(saidaDTO.getDsOrigemIndicador());
		
		setTesteOrigem(Long.parseLong(teste));

        if (getRdtoCadastroFloating() != null && getRdtoCadastroFloating() == 1) {
            setDescRdtoCadastroFloating("Sim");
        } else {
            setDescRdtoCadastroFloating("N�o");
        }

        setQtDiaUtilPgto(saidaDTO.getQtDiaUtilPgto());
        setQtdDiasReutilizacaoControlePgto(saidaDTO.getQtDiaUtilPgto());

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 1) {
            setCdFormaEnvioPagamento(saidaDTO.getCdFormaEnvioPagamento());
            setCdFormaAutPagamento(saidaDTO.getCdFormaAutorizacaoPagamento());
            setCdUtilizaPreAut(saidaDTO.getCdUtilizaPreAutorizacaoPagamentos());
            setCdTipoContFloating(saidaDTO.getCdTipoControleFloating());
            setCboFormaEnvioPagamento(Long.valueOf(String.valueOf(saidaDTO
                .getCdFormaEnvioPagamento())));
            setDsFormaEnvioPagamento(saidaDTO.getDsFormaEnvioPagamento());
            setCboFormaAutPagamentos(Long.valueOf(String.valueOf(saidaDTO
                .getCdFormaAutorizacaoPagamento())));
            setDsFormaAutPagamentos(saidaDTO.getDsFormaAutorizacaoPagamento());
            setRdoAutComplementarAg(saidaDTO
                .getCdIndicadorAutorizacaoComplemento().toString());
            setCboTipoDataControleFloating(Long.valueOf(String.valueOf(saidaDTO
                .getCdTipoDataFloat())));
            setDsTipoDataControleFloating(saidaDTO.getDsTipoDataFloat());
            setRdoUtilizaListaDebitos(saidaDTO.getCdIndicadorListaDebito()
                .toString());
            setRdoUtilizaMora(saidaDTO.getCdIndicadorUtilizaMora().toString());
            setCboTipoFormacaoListaDebito(Long.valueOf(String.valueOf(saidaDTO
                .getCdTipoFormacaoLista())));
            setDsTipoFormacaoListaDebito(saidaDTO.getDsTipoFormacaoLista());
            setCboTratamentoListaDebitoSemNumeracao(Long.valueOf(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTratamentoListaDebitoSemNumeracao(saidaDTO
                .getDsTipoConsistenciaLista());
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTO.getCdIndicadorRetornoInternet()));
            
            

            setCboTipoConsolidacaoPagamentosComprovante(Long.valueOf(String
                .valueOf(saidaDTO.getCdTipoConsultaComprovante())));
            setDsTipoConsolidacaoPagamentosComprovante(saidaDTO
                .getDsTipoConsolidacaoComprovante());

            setDsIndicadorUtilizaMora(saidaDTO.getDsIndicadorUtilizaMora());

            if (!String.valueOf(saidaDTO.getCdFormaManutencaoCadastroProcurador()).equalsIgnoreCase("4")) {
                setRdoEfetuarManutencaoCadastroProcuradores("1");
            }

            if(saidaDTO.getCdIndicadorRetornoInternet() != 2 && saidaDTO.getCdIndicadorRetornoInternet() != 4){
            	renderizaCombo = 1;
            }else{
            	renderizaCombo = 0;
            	Integer naoRenderizaCombo = 0;
            	setComboCodIndicadorTipoRetornoInternet(Long.parseLong(naoRenderizaCombo.toString()));
            }
            
            setDsCodIndicadorTipoRetornoInternet(saidaDTO.getDsCodIndicadorTipoRetornoInternet());
            
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 2) {
            setCdEmiteCartao(saidaDTO.getEmiteCartaoAntecipadoContaSalario());
            setCdTipoCartao(saidaDTO.getCdTipoCartao());
            setQtddLimite(String.valueOf(saidaDTO
                .getQtdeLimiteSolicitacaoCartaoContaSalario()));
            SimpleDateFormat formatDiaMesAno = new SimpleDateFormat(
            "dd.MM.yyyy");
            try {
                setDataLimite(formatDiaMesAno.parse(saidaDTO
                    .getDtLimiteEnquadramentoConvenioContaSalario()));
            } catch (ParseException e) {
                setDataLimite(null);
            }

            setCboFormaEnvioPagamento(Long.valueOf(String.valueOf(saidaDTO
                .getCdFormaEnvioPagamento())));
            setDsFormaEnvioPagamento(saidaDTO.getDsFormaEnvioPagamento());
            setCboFormaAutPagamentos(Long.valueOf(String.valueOf(saidaDTO
                .getCdFormaAutorizacaoPagamento())));
            setDsFormaAutPagamentos(saidaDTO.getDsFormaAutorizacaoPagamento());
            setRdoAutComplementarAg(saidaDTO
                .getCdIndicadorAutorizacaoComplemento().toString());
            setCboTipoDataControleFloating(Long.valueOf(String.valueOf(saidaDTO
                .getCdTipoDataFloat())));
            setDsTipoDataControleFloating(saidaDTO.getDsTipoDataFloat());
            setRdoUtilizaListaDebitos(saidaDTO.getCdIndicadorListaDebito()
                .toString());
            setRdoUtilizaMora(saidaDTO.getCdIndicadorUtilizaMora().toString());
            setCboTipoFormacaoListaDebito(Long.valueOf(String.valueOf(saidaDTO
                .getCdTipoFormacaoLista())));
            setDsTipoFormacaoListaDebito(saidaDTO.getDsTipoFormacaoLista());
            setCboTratamentoListaDebitoSemNumeracao(Long.valueOf(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTratamentoListaDebitoSemNumeracao(saidaDTO
                .getDsTipoConsistenciaLista());

            setComboRetornoOperacoesSelecionado(Long.parseLong(saidaDTO.getCdIndicadorRetornoInternet().toString()));

            setDtLimiteEnqConvContaSal(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTO.getDtEnquaContaSalario()));
            setQtddLimiteCartaoContaSalarioSolicitacao(saidaDTO
                .getQtLimiteSolicitacaoCatao());
            setCboTipoConsolidacaoPagamentosComprovante(Long.valueOf(String
                .valueOf(saidaDTO.getCdTipoConsultaComprovante())));
            setDsTipoConsolidacaoPagamentosComprovante(saidaDTO
                .getDsTipoConsolidacaoComprovante());
            setCboTipoCartaoContaSalario(Long.parseLong(saidaDTO
                .getCdTipoCataoSalario().toString()));
            setDsTipoCartaoContaSalario(saidaDTO.getDsTipoCataoSalario());
            setRdoEmissaoAntCartaoContSal(saidaDTO.getCdIndicadorCataoSalario()
                .toString());
            setRdoAbertContaBancoPostBradSeg(saidaDTO
                .getCdIndicadorBancoPostal().toString());
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTO
                .getCdIndicadorRetornoInternet()));
            setDsIndicadorUtilizaMora(saidaDTO.getDsIndicadorUtilizaMora());

            if (!String.valueOf(
                saidaDTO.getCdFormaManutencaoCadastroProcurador())
                .equalsIgnoreCase("4")) {
                setRdoEfetuarManutencaoCadastroProcuradores("1");
            }
            
            if(saidaDTO.getCdIndicadorRetornoInternet() != 2 && saidaDTO.getCdIndicadorRetornoInternet() != 4){
            	renderizaCombo = 1;
            }else{
            	renderizaCombo = 0;
            	Integer naoRenderizaCombo = 0;
            	setComboCodIndicadorTipoRetornoInternet(Long.parseLong(naoRenderizaCombo.toString()));
            }
            
            setDsCodIndicadorTipoRetornoInternet(saidaDTO.getDsCodIndicadorTipoRetornoInternet());

        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 3) {
            setCboFormaEnvioPagamento(Long.parseLong(String.valueOf(saidaDTO
                .getCdFormaEnvioPagamento())));
            setDsFormaEnvioPagamento(saidaDTO.getDsFormaEnvioPagamento());
            setCboFormaAutPagamentos(Long.parseLong(String.valueOf(saidaDTO
                .getCdFormaAutorizacaoPagamento())));
            setDsFormaAutPagamentos(saidaDTO.getDsFormaAutorizacaoPagamento());
            setRdoAutComplementarAg(saidaDTO
                .getCdIndicadorAutorizacaoComplemento().toString());

            setComboRetornoOperacoesSelecionado(Long.parseLong(saidaDTO.getCdIndicadorRetornoInternet().toString()));

            setCboTipoDataControleFloating(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoDataFloat())));
            setDsTipoDataControleFloating(saidaDTO.getDsTipoDataFloat());
            setRdoUtilizaListaDebitos(saidaDTO.getCdIndicadorListaDebito()
                .toString());
            setRdoUtilizaMora(saidaDTO.getCdIndicadorUtilizaMora().toString());
            setCboTipoFormacaoListaDebito(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoFormacaoLista())));
            setDsTipoFormacaoListaDebito(saidaDTO.getDsTipoFormacaoLista());
            setCboTratamentoListaDebitoSemNumeracao(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTratamentoListaDebitoSemNumeracao(saidaDTO
                .getDsTipoConsistenciaLista());
            setCboTipoConsolidacaoPagamentosComprovante(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoConsultaComprovante())));
            setDsTipoConsolidacaoPagamentosComprovante(saidaDTO
                .getDsTipoConsolidacaoComprovante());
            setRdoPesquisaDe(saidaDTO.getCdConsDebitoVeiculo().toString());
            setCboPeriodicidadePesquisaDebitosPendentesVeiculos(saidaDTO
                .getCdPerdcConsultaVeiculo());
            setDsPeriodicidadePesquisaDebitosPendentesVeiculos(saidaDTO
                .getDsPerdcConsultaVeiculo());
            setRdoAgendamentoDebitosPendentesVeiculos(saidaDTO
                .getCdAgendaDebitoVeiculo().toString());
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTO
                .getCdIndicadorRetornoInternet()));
            setDsIndicadorUtilizaMora(saidaDTO.getDsIndicadorUtilizaMora());

            if (!String.valueOf(saidaDTO.getCdFormaManutencaoCadastroProcurador()).equalsIgnoreCase("4")) {
                setRdoEfetuarManutencaoCadastroProcuradores("1");
            }
            
            if(saidaDTO.getCdIndicadorRetornoInternet() != 2 && saidaDTO.getCdIndicadorRetornoInternet() != 4){
            	renderizaCombo = 1;
            }else{
            	renderizaCombo = 0;
            	Integer naoRenderizaCombo = 0;
            	setComboCodIndicadorTipoRetornoInternet(Long.parseLong(naoRenderizaCombo.toString()));
            }
            
            setDsCodIndicadorTipoRetornoInternet(saidaDTO.getDsCodIndicadorTipoRetornoInternet());
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 4) {
            setCdInformaAgenciaContaCredito(saidaDTO
                .getCdInformaAgenciaContaCredito());
            setCdTipoIdenBeneficio(saidaDTO.getCdTipoIdentificacaoBeneficio());
            setCdUtilizaCadOrgPagadores(saidaDTO
                .getCdUtilizaCadastroOrgaosPagadores());
            setCdManCadastroProcuradores(saidaDTO
                .getCdMantemCadastroProcuradores());
            setDsPeriodicidadeManutencao(saidaDTO
                .getDsPeriodicidadeManutencaoCadastroProcurador());
            setCdPeriodicidadeManutencao(saidaDTO
                .getCdPeriodicidadeManutencaoCadastroProcurador());
            setCdFormaManutencaoCadastroProcurador(saidaDTO
                .getCdFormaManutencaoCadastroProcurador());
            setCdPossuiExpiracaoCredito(saidaDTO.getCdPossuiExpiracaoCredito());
            setCdFormaControleExpiracao(saidaDTO
                .getCdFormaControleExpiracaoCredito());
            setQtddDiasExpiracao(String.valueOf(saidaDTO
                .getQtdeDiasExpiracaoCredito()));
            setCdTratFeriadoFimVigencia(saidaDTO
                .getCdTratamentoFeriadoFimVigencia());
            setCdTipoPrestContaCreditoPago(saidaDTO
                .getCdTipoPrestacaoContaCreditoPago());
            setCboFormaEnvioPagamento(Long.valueOf(String.valueOf(saidaDTO
                .getCdFormaEnvioPagamento())));
            setDsFormaEnvioPagamento(saidaDTO.getDsFormaEnvioPagamento());
            setCboFormaAutPagamentos(Long.valueOf(String.valueOf(saidaDTO
                .getCdFormaAutorizacaoPagamento())));
            setDsFormaAutPagamentos(saidaDTO.getDsFormaAutorizacaoPagamento());
            setRdoAutComplementarAg(String.valueOf(saidaDTO
                .getCdIndicadorAutorizacaoComplemento()));
            setCboTipoDataControleFloating(Long.valueOf(String.valueOf(saidaDTO
                .getCdTipoDataFloat())));
            setDsTipoDataControleFloating(saidaDTO.getDsTipoDataFloat());
            setRdoUtilizaListaDebitos(String.valueOf(saidaDTO
                .getCdIndicadorListaDebito()));
            setCboTipoFormacaoListaDebito(Long.valueOf(String.valueOf(saidaDTO
                .getCdTipoFormacaoLista())));
            setDsTipoFormacaoListaDebito(saidaDTO.getDsTipoFormacaoLista());
            setCboTratamentoListaDebitoSemNumeracao(Long.valueOf(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTratamentoListaDebitoSemNumeracao(saidaDTO
                .getDsTipoConsistenciaLista());

            setComboRetornoOperacoesSelecionado(Long.parseLong(saidaDTO.getCdIndicadorRetornoInternet().toString()));

            setCboTipoConsolidacaoPagamentosComprovante(Long.valueOf(String
                .valueOf(saidaDTO.getCdTipoConsultaComprovante())));
            setDsTipoConsolidacaoPagamentosComprovante(saidaDTO
                .getDsTipoConsolidacaoComprovante());
            setCboTipoIdentificacaoBeneficiario(Long.valueOf(String
                .valueOf(saidaDTO.getCdTipoIdBeneficio())));
            setDsTipoIdentificacaoBeneficiario(saidaDTO.getDsTipoIdBeneficio());
            setRdoUtilizaCadastroOrgaosPagadores(String.valueOf(saidaDTO
                .getCdIndicadorCadastroOrg()));
            setRdoUtilizaCadastroProcuradores(String.valueOf(saidaDTO
                .getCdIndicadorCadastroProcd()));
            setCboFormaManCadProcurador(Long.valueOf(String.valueOf(saidaDTO
                .getCdFormaManutencao())));
            setDsFormaManCadProcurador(saidaDTO.getDsFormaManutencao());
            setCboPeriocidadeManCadProc(saidaDTO.getCdPerdcManutencaoProcd());
            setDsPeriocidadeManCadProc(saidaDTO.getDsPerdcManutencaoProcd());
            setRdoInformaAgenciaContaCred(String.valueOf(saidaDTO
                .getCdDispzContaCredito()));
            setRdoPossuiExpiracaoCredito(String.valueOf(saidaDTO
                .getCdIndicadorExpiracaoCredito()));
            setCboContExpCredConta(Long.valueOf(String.valueOf(saidaDTO
                .getCdFormaExpiracaoCredito())));
            setDsContExpCredConta(saidaDTO.getDsFormaExpiracaoCredito());
            setQtddDiasExpiracaoCredito(String.valueOf(saidaDTO
                .getQtDiaExpiracao()));
            setCboMomentoIndCredEfetivado(Long.valueOf(String.valueOf(saidaDTO
                .getCdMomentoCreditoEfetivacao())));
            setDsMomentoIndCredEfetivado(saidaDTO
                .getDsMomentoCreditoEfetivacao());
            setCboTratamentoFeriadoFimVigenciaCredito(Long.valueOf(String
                .valueOf(saidaDTO.getCdCreditoNaoUtilizado())));
            setDsTratamentoFeriadoFimVigenciaCredito(saidaDTO
                .getDsCreditoNaoUtilizado());
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTO
                .getCdIndicadorRetornoInternet()));
            setDsIndicadorUtilizaMora(saidaDTO.getDsIndicadorUtilizaMora());
            if (!String.valueOf(
                saidaDTO.getCdFormaManutencaoCadastroProcurador())
                .equalsIgnoreCase("4")) {
                setRdoEfetuarManutencaoCadastroProcuradores("1");
            }
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 5) {
            setCdFormaManutencaoCadastro(saidaDTO
                .getCdFormaManutencaoCadastro());
            setQtddDiasInativacao(String.valueOf(saidaDTO
                .getQtdeDiasInativacaoFavorecido()));
            setCdConsistenciaCpfCnpfFavorecido(saidaDTO
                .getCdCtciaInscricaoFavorecido());
            setCboFormaManCadFavorecido(Long.valueOf(saidaDTO
                .getCdFormaManutencao()));
            setDsFormaManCadFavorecido(saidaDTO.getDsFormaManutencao());
            setQtddDiasInativacaoFavorecido(saidaDTO
                .getQtDiaInatividadeFavorecido());
            setCboTipoConsistenciaInscricaoFavorecido(Long.valueOf(saidaDTO
                .getCdTipoContaFavorecido()));
            setDsTipoConsistenciaInscricaoFavorecido(saidaDTO
                .getDsTipoConsistenciaFavorecido());
            setComboRetornoOperacoesSelecionado(Long.parseLong(saidaDTO.getCdIndicadorRetornoInternet().toString()));
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTO
                .getCdIndicadorRetornoInternet()));
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() >= 6
                        && listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() <= 9) {
            setCdPeriodicidade(saidaDTO.getCdPeriodicidade());
            setDsPeriodicidade(saidaDTO.getDsPeriodicidade());
            setCdDestino(saidaDTO.getCdDestino());
            setCdPermiteCorrespAberta(saidaDTO
                .getCdPermiteCorrespondenciaAberta());
            setCdDemonsInfReservada(saidaDTO
                .getCdDemonstraInformacoesAreaReservada());
            setCdAgruparCorrespondencia(saidaDTO.getCdAgruparCorrespondencia());
            setQuantidadeVias(String.valueOf(saidaDTO.getQtdeVias()));
            setCboPeriodicidadeEmissao(Integer.valueOf(String.valueOf(saidaDTO
                .getCdPeriodicidadeAviso())));
            setDsPeriodicidadeEmissao(saidaDTO.getDsPeriodicidadeAviso());
            setQtddViasEmitir(saidaDTO.getQtViaAviso());
            setCboDestinoEnvio(Long.valueOf(String.valueOf(saidaDTO
                .getCdDestinoAviso())));
            setDsDestinoEnvio(saidaDTO.getDsDestinoAviso());
            setCboCadConsultaEndEnvio(Long.valueOf(String.valueOf(saidaDTO
                .getCdConsEndereco())));
            setDsCadConsultaEndEnvio(saidaDTO.getDsConsEndereco());
            setRdoPermiteCorrespondenciaAberta(String.valueOf(saidaDTO
                .getCdEnvelopeAberto()));
            setRdoAgruparCorresp(String.valueOf(saidaDTO
                .getCdAgrupamentoAviso()));
            setTextoInformacaoAreaReservada(saidaDTO.getDsAreaResrd());
            setRdoDemonstraInfAreaRes(String.valueOf(saidaDTO
                .getCdAreaReservada()));
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTO
                .getCdIndicadorRetornoInternet()));
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 10
                        || listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 11) {
            setCdPeriodicidade(saidaDTO.getCdPeriodicidade());
            setDsPeriodicidade(saidaDTO.getDsPeriodicidade());
            setCdDestino(saidaDTO.getCdDestino());
            setCdPermiteCorrespAberta(saidaDTO
                .getCdPermiteCorrespondenciaAberta());
            setCdDemonsInfReservada(saidaDTO
                .getCdDemonstraInformacoesAreaReservada());
            setCdAgruparCorrespondencia(saidaDTO.getCdAgruparCorrespondencia());
            setQuantidadeVias(String.valueOf(saidaDTO.getQtdeVias()));
            setCboPeriodicidadeEmissao(Integer.valueOf(String.valueOf(saidaDTO
                .getCdPerdcComprovante())));
            setDsPeriodicidadeEmissao(saidaDTO.getDsPerdcComprovante());
            setQtddViasEmitir(saidaDTO.getQtViaComprovante());
            setCboDestinoEnvio(Long.valueOf(String.valueOf(saidaDTO
                .getCdDestinoComprovante())));
            setDsDestinoEnvio(saidaDTO.getDsDestinoComprovante());
            setCboCadConsultaEndEnvio(Long.valueOf(String.valueOf(saidaDTO
                .getCdConsEndereco())));
            setDsCadConsultaEndEnvio(saidaDTO.getDsConsEndereco());
            setRdoPermiteCorrespondenciaAberta(String.valueOf(saidaDTO
                .getCdEnvelopeAberto()));
            setRdoAgruparCorresp(String.valueOf(saidaDTO
                .getCdAgrupamentoComprovado()));
            setTextoInformacaoAreaReservada(saidaDTO.getDsAreaResrd());
            setRdoDemonstraInfAreaRes(String.valueOf(saidaDTO
                .getCdAreaReservada()));
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTO
                .getCdIndicadorRetornoInternet()));
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 12
                        || listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 13) {
            setCdTipoRejeicao(saidaDTO.getCdTipoRejeicao());
            setCdTipoEmissao(saidaDTO.getCdTipoEmissao());
            setCdMidiaDisponivelCorrentista(saidaDTO
                .getCdMidiaDisponivelCorrentista());
            setCdMidiaDisponivelNaoCorrentista(saidaDTO
                .getCdMidiaDisponivelNaoCorrentista());
            setCdTipoEnvioCorrespondencia(saidaDTO
                .getCdTipoEnvioCorrespondencia());
            setCdFrasesPreCadastradas(saidaDTO.getCdFrasesPreCadastratadas());
            setCdCobrarTarifasFuncionarios(saidaDTO
                .getCdCobrarTarifasFuncionarios());
            setQtddMesesEmissao(String.valueOf(saidaDTO.getQtdeMesesEmissao()));
            setQtddLinhas(String.valueOf(saidaDTO.getQtdeLinhas()));
            setQtddVias(String.valueOf(saidaDTO
                .getQtdeViasEmissaoComprovantes()));
            setQtddViasPagas(String.valueOf(saidaDTO.getQtdeViasPagas()));
            setCdFormularioImpressao(saidaDTO.getCdFormularioParaImpressao());
            if (getCdFormularioImpressao() != null) {
                setDsFormularioImpressao(listaTipoLayoutArquivoHash
                    .get(getCdFormularioImpressao()));
            }
            setCdPeriodicidadeCobrancaTarifa(saidaDTO
                .getCdPeriodicidadeCobrancaTarifaEmissaoComprovantes());
            setDsPeriodicidadeCobrancaTarifa(saidaDTO
                .getDsPeriodicidadeCobrancaTarifaEmissaoComprovantes());
            setFechamentoApuracao(String.valueOf(saidaDTO
                .getCdFechamentoApuracao()));
            setDiasCobrancaAposApuracao(String.valueOf(saidaDTO
                .getQtdeDiasCobrancaAposApuracao()));
            setCdTipoReajuste(saidaDTO.getCdTipoReajuste());
            setDsPeriodicidadeReajusteTarifa(saidaDTO
                .getDsPeriodicidadeReajusteTarifa());
            setCdPeriodicidadeReajusteTarifa(saidaDTO
                .getCdPeriodicidadeReajusteTarifa());
            setMesesReajusteAutomatico(String.valueOf(saidaDTO
                .getQtdeMesesReajusteAutomatico()));
            setCdIndiceReajuste(saidaDTO.getCdIndiceReajuste());
            if (getCdIndiceReajuste() != null) {
                setDsIndiceReajuste(listaIndiceEconomicoHash
                    .get(getCdIndiceReajuste()));
            }
            setPercentualReajuste(saidaDTO.getPercentualReajuste());
            setBonificacaoTarifa(saidaDTO.getPorcentagemBonificacaoTarifa());
            setCboTipoRejeicaoLote(Long.parseLong(saidaDTO.getCdRejeicaoLote()
                .toString()));
            setDsTipoRejeicaoLote(saidaDTO.getDsRejeicaoLote());
            setCboMeioDisponibilizacaoCorrentista(new Long(saidaDTO
                .getCdDispzDiversarCrrtt()));
            setDsMeioDisponibilizacaoCorrenstista(saidaDTO
                .getDsDispzDiversarCrrtt());
            setCboMidiaDispCorrentista(Long.parseLong(saidaDTO
                .getCdMidiaDisponivel().toString()));
            setDsMidiaDispCorrentista(saidaDTO.getDsMidiaDisponivel());
            setCboMeioDisponibilizacaoNaoCorrentista(new Long(saidaDTO
                .getCdDispzDiversasNao()));
            setDsMeioDisponibilizacaoNaoCorrenstista(saidaDTO
                .getDsDispzDiversasNao());
            setCboDestinoEnvioCorresp(Long.parseLong(saidaDTO
                .getCdDestinoComprovante().toString()));
            setDsDestinoEnvioCorresp(saidaDTO.getDsDestinoComprovante());
            setCboCadConsultaEndEnvio(Long.parseLong(saidaDTO
                .getCdConsEndereco().toString()));
            setDsCadConsultaEndEnvio(saidaDTO.getDsConsEndereco());
            setRdoUtilizaFrasesPreCadastradas(saidaDTO.getCdFrasePreCadastro()
                .toString());
            setCobrarTarifasFunc(saidaDTO.getCdCobrancaTarifa().toString());
            setQuantidadeMesesEmissao(saidaDTO.getQtMesComprovante());
            setQtddViasEmitir(saidaDTO.getQtViaComprovante());
            setQtddViasPagasRegistro(saidaDTO.getQtViaCobranca());
            setQtddLinhasPorComprovante(saidaDTO.getQtLimiteLinha());
            setFormularioImpressao(String.valueOf(saidaDTO
                .getCdFormularioContratoCliente()));
            setComboRetornoOperacoesSelecionado(Long.parseLong(saidaDTO.getCdIndicadorRetornoInternet().toString()));
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTO
                .getCdIndicadorRetornoInternet()));
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 14) {
            setCdTipoIdentificadorBeneficiario(saidaDTO
                .getCdTipoIdentificacaoFuncionario());
            setCdTipoConsistenciaIdentificador(saidaDTO
                .getCdTipoConsistenciaIdentificador());
            setCdCondicaoEnquadramento(saidaDTO.getCdCondicaoEnquadramento());
            setCdCriterioPrincipalEnquadramento(saidaDTO
                .getCdCriterioPrincipalEnquadramento());
            setCdCriterioCompostoEnquadramento(saidaDTO
                .getCdCriterioCompostoEnquadramento());
            setQuantidadeEtapasRecadastramento(saidaDTO
                .getQtEtapasRecadastroBeneficio());
            setQtddFasesPorEtapa(saidaDTO.getQtFaseRecadastroBeneficio());
            setQtddMesesPorFase(saidaDTO.getQtMesFaseRecadastro());
            setQtddMesesPorEtapa(saidaDTO.getQtMesEtapaRecadastro());
            setQtddEtapasRecadastramento(String.valueOf(saidaDTO
                .getQtdeEtapasRecadastramento()));
            setQtddFasesEtapa(String.valueOf(saidaDTO.getQtdeFasesPorEtapa()));
            setQtddMesesPrazoFase(String.valueOf(saidaDTO
                .getQtdeMesesPrazoPorFase()));
            setQtddMesesPrazoEtapa(String.valueOf(saidaDTO
                .getQtdeMesesPrazoPorEtapa()));
            setCdPermiteEnvioRemessa(saidaDTO
                .getCdPermiteEnvioRemessaManutencao());
            setCdPeriodicidadeEnvioRemessa(saidaDTO
                .getCdPeriodicidadeEnvioRemessa());
            setDsPeriodicidadeEnvioRemessa(saidaDTO
                .getDsPeriodicidadeEnvioRemessa());
            setCdBaseDadosUtilizada(saidaDTO.getCdBaseDadosUtilizada());
            setCdTipoCargaBaseDadosUtilizada(saidaDTO
                .getCdTipoCargaBaseDadosUtilizada());
            setCdPermiteAnteciparRecadastramento(saidaDTO
                .getCdPermiteAnteciparRecadastramento());
            SimpleDateFormat formatDiaMesAno = new SimpleDateFormat(
            "dd.MM.yyyy");
            try {
                setDataLimiteInicioVinculoCargaBase(formatDiaMesAno
                    .parse(saidaDTO.getDtLimiteInicioVinculoCargaBase()));
            } catch (ParseException e) {
                setDataLimiteInicioVinculoCargaBase(null);
            }
            try {
                setDataInicioRecadastramento(formatDiaMesAno.parse(saidaDTO
                    .getDtInicioRecadastramento()));
            } catch (ParseException e) {
                setDataInicioRecadastramento(null);
            }
            try {
                setDataFimRecadastramento(formatDiaMesAno.parse(saidaDTO
                    .getDtFimRecadastramento()));
            } catch (ParseException e) {
                setDataFimRecadastramento(null);
            }
            setCdPermiteAcertosDados(saidaDTO.getCdPermiteAcertosDados());
            try {
                setDataInicioPeriodoAcertoDados(formatDiaMesAno.parse(saidaDTO
                    .getDtInicioPeriodoAcertoDados()));
            } catch (ParseException e) {
                setDataInicioPeriodoAcertoDados(null);
            }
            try {
                setDataFimPeriodoAcertoDados(formatDiaMesAno.parse(saidaDTO
                    .getDtFimPeriodoAcertoDados()));
            } catch (ParseException e) {
                setDataFimPeriodoAcertoDados(null);
            }
            setCdEmiteMsgRecadastramentoMidiaOnline(saidaDTO
                .getCdEmiteMensagemRecadastramentoMidiaOnLine());
            setCdMidiaMsgRecadastramentoOnline(saidaDTO
                .getCdMidiaMensagemRecastramentoOnLine());
            setDsPeriodicidadeCobrancaTarifa(saidaDTO
                .getDsPeriodicidadeCobrancaTarifa());
            setCdPeriodicidadeCobrancaTarifa(saidaDTO
                .getCdPeriodicidadeCobrancaTarifa());
            setDiaFechamentoApuracaoTarifaMensal(String.valueOf(saidaDTO
                .getDiaFechamentoApuracaoTarifaMensal()));
            setQtddDiasCobrancaTarifaApuracao(String.valueOf(saidaDTO
                .getQtdeDiasCobrancaTarifaAposApuracao()));
            setDsPeriodicidadeReajusteTarifa(saidaDTO
                .getDsPeriodicidadeReajusteTarifa());
            setCdPeriodicidadeReajusteTarifa(saidaDTO
                .getCdPeriodicidadeReajusteTarifa());
            setCdTipoReajusteTarifa(saidaDTO.getCdTipoReajusteTarifa());
            setQtddMesesReajusteAutomaticoTarifa(String.valueOf(saidaDTO
                .getQtdeMesesReajusteAutomaticoTarifa()));
            setDsIndiceEconomicoReajusteTarifa(saidaDTO
                .getDsIndiceEconomicoReajusteTarifa());
            setCdIndiceEconomicoReajusteTarifa(saidaDTO
                .getCdIndiceEconomicoReajusteTarifa());
            setPercentualIndiceEconomicoReajusteTarifa(saidaDTO
                .getPercentualIndiceEconomicoReajusteTarifa());
            setPercentualBonificacaoTarifaPadrao(saidaDTO
                .getPercentualBonificacaoTarifaPadrao());
            setCboTipoIdentificacaoBeneficiario(Long.valueOf(String
                .valueOf(saidaDTO.getCdTipoIdBeneficio())));
            setDsTipoIdentificacaoBeneficiario(saidaDTO.getDsTipoIdBeneficio());
            setCboTipoConsistenciaIdentificacaoBeneficiario(Long.valueOf(String
                .valueOf(saidaDTO.getCdCtciaIdentificacaoBeneficio())));
            setDsTipoConsistenciaIdentificacaoBeneficiario(saidaDTO
                .getDsCtciaIdentificacaoBeneficio());
            setCboCondicaoEnquadramento(Long.valueOf(String.valueOf(saidaDTO
                .getCdCriterioEnquaRecadastro())));
            setDsCondicaoEnquadramento(saidaDTO.getDsCriterioEnquaRecadastro());
            setCboCritPrincEnquadramento(Long.valueOf(String.valueOf(saidaDTO
                .getCdPrincipalEnquaRecadastro())));
            setDsCritPrincEnquadramento(saidaDTO
                .getDsPrincipalEnquaRecadastro());
            setCboCritCompEnquadramento(Long.valueOf(String.valueOf(saidaDTO
                .getCdCriterioEnquaBeneficio())));
            setDsCritCompEnquadramento(saidaDTO.getDsCriterioEnquaBeneficio());
            setQuantidadeEtapasRecadastramento(saidaDTO
                .getQtEtapasRecadastroBeneficio());
            setQtddFasesPorEtapa(saidaDTO.getQtFaseRecadastroBeneficio());
            setQtddMesesPorEtapa(saidaDTO.getQtMesEtapaRecadastro());
            setQtddMesesPorFase(saidaDTO.getQtMesFaseRecadastro());
            setCboCadUtilizadoRec(Long.valueOf(String.valueOf(saidaDTO
                .getCdBaseRecadastroBeneficio())));
            setDsCadUtilizadoRec(saidaDTO.getDsBaseRecadastroBeneficio());
            setDtInicioRecadastramento(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTO
                    .getDtInicioRecadastroBeneficio()));
            setDtFimRecadastramento(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTO
                    .getDtFimRecadastroBeneficio()));
            setRdoPermiteEnvioRemessaManutencaoCadastro(String.valueOf(saidaDTO
                .getCdManutencaoBaseRecadastro()));
            setCboPeriodicidadeEnvioRemessaManutencao(saidaDTO
                .getCdPerdcEnvioRemessa());
            setDsPeriodicidadeEnvioRemessaManutencao(saidaDTO
                .getDsPerdcEnvioRemessa());
            setCboTipoCargaCadastroBeneficiario(Long.valueOf(String
                .valueOf(saidaDTO.getCdTipoCargaRecadastro())));
            setDsTipoCargaCadastroBeneficiario(saidaDTO
                .getDsTipoCargaRecadastro());
            setDtLimiteVincClienteBenef(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTO.getDtLimiteVinculoCarga()));
            setRdoPermiteAcertosDados(String.valueOf(saidaDTO
                .getCdAcertoDadoRecadastro()));
            setRdoPermiteAnteciparRecadastramento(saidaDTO
                .getCdAntecRecadastroBeneficio().toString());
            setDtInicioPerAcertoDados(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTO
                    .getDtinicioAcertoRecadastro()));
            setDtFimPerAcertoDados(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTO
                    .getDtFimAcertoRecadastro()));
            setRdoEmiteMsgRecadMidia(saidaDTO.getCdMensagemRecadastroMidia()
                .toString());
            setCboMidiaMsgRecad(Long.valueOf(String.valueOf(saidaDTO
                .getCdMidiaMensagemRecadastro())));
            setDsMidiaMsgRecad(saidaDTO.getDsMidiaMensagemRecadastro());
            setComboRetornoOperacoesSelecionado(Long.parseLong(saidaDTO.getCdIndicadorRetornoInternet().toString()));
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTO
                .getCdIndicadorRetornoInternet()));
        }

        setComboRetornoOperacoesSelecionado(Long.parseLong(saidaDTO.getCdIndicadorRetornoInternet().toString()));
        setRdoGerarRetornoSeparadoCanal(saidaDTO
            .getCdIndicadorRetornoSeparado().toString());
        setCdAmbienteServicoContrato(PgitUtil
            .verificaStringNula(listaModalidadeTipoServicoSaidaDTO
                .getCdAmbienteServicoContrato()));
        setUsuarioInclusao(saidaDTO.getUsuarioInclusao());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencao());
        setComplementoInclusao(saidaDTO.getComplementoInclusao() == null
            || saidaDTO.getComplementoInclusao().equals("0")
            ? ""
                : saidaDTO.getComplementoInclusao());
        setComplementoManutencao(saidaDTO.getComplementoManutencao() == null
            || saidaDTO.getComplementoManutencao().equals("0")
            ? ""
                : saidaDTO.getComplementoManutencao());
        setDataHoraInclusao(saidaDTO.getHoraInclusao());
        setDataHoraManutencao(saidaDTO.getHoraManutencao());
        setTipoCanalInclusao(saidaDTO.getCdCanalInclusao() == 0 ? "" : saidaDTO
            .getCdCanalInclusao()
            + " - " + saidaDTO.getDsCanalInclusao());
        setTipoCanalManutencao(saidaDTO.getCdCanalManutencao() == 0
            ? ""
                : saidaDTO.getCdCanalManutencao() + " - "
                + saidaDTO.getDsCanalManutencao());
        setCdConsultaSaldoValorSuperior(PgitUtil.verificaIntegerNulo(
            saidaDTO.getCdConsultaSaldoValorSuperior()).longValue());
        setCodTela(listaModalidadeTipoServicoSaidaDTO.getCdParametroTela());

        if ("S".equals(saidaVerificarAtributo.getCdIndicadorTipoDataFloat())) {
            setPermiteAlteracaoComboFloating(false);
            setHabilitaTipoDataControleFloating(false);
            renderizaComboFloating();
        } else {
            setHabilitaTipoDataControleFloating(true);
            setPermiteAlteracaoComboFloating(true);
            setPermiteAlteracaoCombo(true);
        }


        if("S".equals(saidaVerificarAtributo.getCdIndicadorAutorizacaoComplemento())){
            setDesabilitaIndicadorAutorizacaoComplemento(false);
        } else {
            setDesabilitaIndicadorAutorizacaoComplemento(true);
        }


        if("S".equals(saidaVerificarAtributo.getCdIndicadorListaDebito())){
            setDesabilitaCdIndicadorListaDebito(false);
        } else {
            setDesabilitaCdIndicadorListaDebito(true);
        }


        if("S".equals(saidaVerificarAtributo.getCdIndicadorTipoFormacaoLista())){
            setDesabilitaCdIndicadorTipoFormacaoLista(false);
        } else {
            setDesabilitaCdIndicadorTipoFormacaoLista(true);
        }

        if("S".equals(saidaVerificarAtributo.getCdIndicadorTipoConsistenciaLista())){
            setDesabilitaCdIndicadorTipoConsistenciaLista(false);
        } else {
            setDesabilitaCdIndicadorTipoConsistenciaLista(true);
        }



        if (getCodTela() != 0) {
            setCboTipoDataControleFloating(Long.valueOf(String.valueOf(saidaDTO
                .getCdTipoDataFloat())));
        } else {
            setCboTipoDataControleFloating(0L);
        }
        setPermiteAlteracaoRadioFloating(false);
        if ("N".equals(saidaVerificarAtributo.getCdIndicadorTipoCataoSalario())) {
            setPermiteAlteracaoCombo(true);
            setPermiteAlteracaoRadioFloating(true);
        }

        if (getCodTela() == 3
                        && "N".equals(saidaVerificarAtributo
                            .getCdIndicadorTipoDataFloat())) {
            setPermiteAlteracaoRadioFloating(true);
        }

        setDsRetornoOperacoes(saidaDTO.getDsIndicadorRetornoInternet());
    }

    /**
     * Renderiza Combo Floating.
     */
    public void renderizaComboFloating() {
        if (!isPermiteAlteracaoComboFloating()) {
            setPermiteAlteracaoCombo(true);
            setCboTipoDataControleFloating(0L);
            setHabilitaTipoDataControleFloating(true);
            if (getRdtoCadastroFloating() == 1) {
                setPermiteAlteracaoCombo(false);
                setHabilitaTipoDataControleFloating(false);
            }
        }
        setCboTipoDataControleFloating(0L);
        if (getRdtoCadastroFloating() == 1) {
            if (getCodTela() == 1 || getCodTela() == 3) {
                setCboTipoDataControleFloating(2L);
            } else if (getCodTela() == 2) {
                setCboTipoDataControleFloating(1L);
            }
        }
    }

    /**
     * Preenche dados modalidade.
     */
    public void preencheDadosModalidade() {
        try {

            listaModalidadeTipoServicoSaidaDTO = new ListarModalidadeTipoServicoSaidaDTO();

            Locale brasil = new Locale("pt", "BR");
            DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(brasil)); df.setParseBigDecimal(true);

            listaModalidadeTipoServicoSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());

            ListarModalidadeTipoServicoSaidaDTO modalidadeTipoServicoSaidaDTO = this.listaGridPesquisa2
            .get(this.itemSelecionadoLista2);

            setFlagExibeCampos("N");
            if (modalidadeTipoServicoSaidaDTO.getCdModalidade() != 90200034) {
                setFlagExibeCampos("S");
            }

            DetalharTipoServModContratoEntradaDTO entradaDTO = new DetalharTipoServModContratoEntradaDTO();
            VerificarAtributosServicoModalidadeEntradaDTO entradaVerificarAtributo = 
                new VerificarAtributosServicoModalidadeEntradaDTO();
            saidaVerificarAtributo = new VerificarAtributosServicoModalidadeSaidaDTO();

            entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
            entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato());
            entradaDTO.setNrSequenciaContratoNegocio(getNrSequenciaContrato());
            entradaDTO.setCdServico(modalidadeTipoServicoSaidaDTO.getCdServico());
            entradaDTO.setCdModalidade(modalidadeTipoServicoSaidaDTO.getCdModalidade());
            entradaDTO.setCdParametroPesquisa(2);
            entradaDTO.setCdParametroTela(modalidadeTipoServicoSaidaDTO.getCdParametroTela());

            entradaVerificarAtributo.setCdpessoaJuridicaContrato(getCdPessoaJuridica());
            entradaVerificarAtributo.setCdTipoContratoNegocio(getCdTipoContrato());
            entradaVerificarAtributo.setNrSequenciaContratoNegocio(getNrSequenciaContrato());
            entradaVerificarAtributo.setCdProdutoServicoOperacao(listaModalidadeTipoServicoSaidaDTO.getCdServico());
            entradaVerificarAtributo.setCdProdutoOperacaoRelacionado(listaModalidadeTipoServicoSaidaDTO.getCdModalidade());
            entradaVerificarAtributo.setCdParametro(1);
            
            setDsTipoServicoSelecionado(listaModalidadeTipoServicoSaidaDTO.getDsModalidade());
            setDsModalidade(modalidadeTipoServicoSaidaDTO.getDsModalidade());
            setDsSituacaoModalidade(modalidadeTipoServicoSaidaDTO.getDsSituacaoServicoRelacionado());

            if (listaGridPesquisa.get(itemSelecionadoLista).getCdParametroTela() == 1
                            && listaGridPesquisa2.get(itemSelecionadoLista2).getCdParametroTela() == 20) {
                setRdoGerarLancProgramado("2");
                setHabilitaCampo(true);
                setHabilitaCampo2(false);
            } else if (listaGridPesquisa.get(itemSelecionadoLista)
                            .getCdParametroTela() == 2 && listaGridPesquisa2.get(itemSelecionadoLista2).getCdParametroTela() == 20) {
                setHabilitaCampo(true);
                setHabilitaCampo2(false);
            } else {
                setHabilitaCampo(false);
                setHabilitaCampo2(true);
            }

            DetalharTipoServModContratoSaidaDTO saidaDTO = getManterContratoService()
            .detalharTipoServModContrato(entradaDTO);
            saidaVerificarAtributo = getManterContratoService()
            .verificarAtributoServicoModalidade(entradaVerificarAtributo);
            
            setDsOrigemIndicador(saidaDTO.getDsOrigemIndicador());
			setDsIndicador(saidaDTO.getDsIndicador());
			
			String teste = String.valueOf(saidaDTO.getDsOrigemIndicador());
			
			setTesteOrigem(Long.parseLong(teste));

            setDsModalidade(modalidadeTipoServicoSaidaDTO.getDsModalidade()); 
            dsIndicadorFeriadoLocal = saidaDTO.getDsCodigoIndFeriadoLocal();
            cdIndicadorFeriadoLocal = saidaDTO.getCdIndicadorFeriadoLocal();

            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 15) {
                setCdPeriodicidadeEnvio(saidaDTO.getCdPeriocidadeEnvio());
                if (saidaDTO.getCdPeriocidadeEnvio() != 0) {
                    setDsPeriodicidadeEnvio(listaPeriodicidadeHash.get(getCdPeriodicidadeEnvio()));
                }
                setCdDestino(saidaDTO.getCdDestino());
                setCdEnderecoEnvio(saidaDTO.getCdEnderecoEnvio());
                setCdPermitirAgrupamento(saidaDTO.getCdPermitirAgrupamento());
                setQtddDiasAntecipacao(String.valueOf(saidaDTO.getQntDiasAntecipacao()));
                setCboMomentoEnvio(Long.valueOf(String.valueOf(saidaDTO.getCdMomentoAvisoRacadastro())));
                setDsMomentoEnvio(saidaDTO.getDsMomentoAvisoRacadastro());
                setQtddDiasAvisoAntecipado(saidaDTO.getQtAntecedencia());
                setCboDestinoEnvio(Long.valueOf(String.valueOf(saidaDTO.getCdDestinoAviso())));
                setDsDestinoEnvio(saidaDTO.getDsDestinoAviso());
                setCboCadConsultaEndEnvio(Long.valueOf(String.valueOf(saidaDTO.getCdConsEndereco())));
                setDsCadConsultaEndEnvio(saidaDTO.getDsConsEndereco());
                setRdoPermitirAgrupamentoCorrespondencia(String.valueOf(saidaDTO.getCdAgrupamentoAviso()));
            }

            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 16) {
                setCdPeriodicidadeEnvio(saidaDTO.getCdPeriocidadeEnvio());
                if (saidaDTO.getCdPeriocidadeEnvio() != 0) {
                    setDsPeriodicidadeEnvio(listaPeriodicidadeHash.get(getCdPeriodicidadeEnvio()));
                }
                setCdDestino(saidaDTO.getCdDestino());
                setCdEnderecoEnvio(saidaDTO.getCdEnderecoEnvio());
                setCdPermitirAgrupamento(saidaDTO.getCdPermitirAgrupamento());
                setQtddDiasAntecipacao(String.valueOf(saidaDTO.getQntDiasAntecipacao()));
                setCboMomentoEnvio(Long.parseLong(String.valueOf(saidaDTO.getCdMomentoFormularioRecadastro())));
                setDsMomentoEnvio(saidaDTO.getDsMomentoFormularioRecadastro());
                setQtddDiasEnvioAntecipadoFormulario(saidaDTO.getQtAntecedencia());
                setCboDestinoEnvio(Long.parseLong(String.valueOf(saidaDTO.getCdDestinoFormularioRecadastro())));
                setDsDestinoEnvio(saidaDTO.getDsDestino());
                setCboCadConsultaEndEnvio(Long.parseLong(String.valueOf(saidaDTO.getCdConsEndereco())));
                setDsCadConsultaEndEnvio(saidaDTO.getDsConsEndereco());
                setCodigoFormulario(String.valueOf(saidaDTO.getCdFormularioContratoCliente()));
                setRdoPermitirAgrupamentoCorrespondencia(saidaDTO.getCdAgrupamentoFormularioRecadastro().toString());
            }

            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 17
                            || modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 18
                            || modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 24
                            || modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 25) {

                setCdTipoConsultaSaldoFiltro(saidaDTO.getCdTipoConsultaSaldo());
                setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO.getQuantDiasRepique()));
                setCdUtilizaCadFavorFiltro(saidaDTO.getRdoUtilizaCadFavControlePag());
                setValorMaxPagFavNaoCadastrado(saidaDTO.getValorMaxPagFavNaoCadastrado());
                setValorMaxPagFavNaoCadastradoDesc(df.format(getValorMaxPagFavNaoCadastrado()));
                setCdTratamentoFeriadosFiltro(saidaDTO.getCdTratamentoFeriadosDtaPag());
                setCdTipoProcessamentoFiltro(saidaDTO.getCdTipoProcessamento());
                setCdTipoRejAgendamentoFiltro(saidaDTO.getCdTipoRejeicaoAgendamento());
                setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdTipoRejeicaoEfetivacao());
                setCdPrioridadeDebitoFiltro(saidaDTO.getCdPrioridadeDebito());
                setCdGerLancFutDebitoFiltro(saidaDTO.getRdoGerarLanctoFuturoDeb());
                setCdGerLancFutCreditoFiltro(saidaDTO.getRdoGerarLanctoFuturoCred());
                setCdPermiteFavConsultPagFiltro(saidaDTO.getRdoPermiteFavConsultarPag());
                setValorLimiteIndividualFiltro(saidaDTO.getValorLimiteIndividual());
                setDsValorLimiteIndividual(df.format(getValorLimiteIndividualFiltro()));
                setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiario());
                setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
                setQtddDiasFloating(saidaDTO.getCdDiaFloatPagamento());
                setCboProcessamentoEfetivacaoPagamento(Long.parseLong(
                    String.valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
                setDsProcessamentoEfetivacaoPagamento(saidaDTO.getDsMomentoProcessamentoPagamento());
                setCboTipoRejeicaoAgendamento(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoAgendaLote())));
                setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendaLote());
                setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoEfetivacaoLote())));
                setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
                setQtddMaximaRegistrosInconsistentesLote(saidaDTO.getQtMaximaInconLote());
                setPercMaxRegInconsistente(saidaDTO.getCdPercentualMaximoInconLote());
                setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO.getCdPrioridadeEfetivacaoPagamento())));
                setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
                setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO.getCdValidacaoNomeFavorecido().toString());
                setRdoUtilizaCadastroFavorecidoControlePagamentos(saidaDTO.getCdUtilizacaoFavorecidoControle().toString());
                setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO.getVlFavorecidoNaoCadastro());
                setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO.getCdConsSaldoPagamento())));
                setDsTipoConsSaldo(saidaDTO.getDsConsSaldoPagamento());
                setQtddDiasRepique(saidaDTO.getQtDiaRepiqConsulta());
                setRdoPermiteFavorecidoConsultarPagamento(saidaDTO.getCdFavorecidoConsPagamento().toString());
                setRdoGerarLancFutDeb(saidaDTO.getCdLancamentoFuturoDebito().toString());
                setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(
                    String.valueOf(saidaDTO.getCdTipoContaFavorecido())));
                setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO.getDsTipoConsistenciaFavorecido());
                setCboTratamentoFeriadosDataPagamento(Long.parseLong(String.valueOf(
                    saidaDTO.getCdPagamentoNaoUtilizado())));
                setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtilizado());
                setValorLimiteIndividual(saidaDTO.getVlLimiteIndividualPagamento());
                setValorLimiteDiario(saidaDTO.getVlLimiteDiaPagamento());
                setDsIndicadorAgendaGrade(saidaDTO.getDsIndicadorAgendaGrade());
                setRdoPermiteAgAutGrade(saidaDTO.getCdIndicadorAgendaGrade().toString());
            }

            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 19) {
                setCdTipoConsultaSaldoFiltro(saidaDTO.getCdTipoConsultaSaldo());
                setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO.getQuantDiasRepique()));
                setCdUtilizaCadFavorFiltro(saidaDTO.getRdoUtilizaCadFavControlePag());
                setValorMaxPagFavNaoCadastrado(saidaDTO.getValorMaxPagFavNaoCadastrado());
                setValorMaxPagFavNaoCadastradoDesc(df.format(getValorMaxPagFavNaoCadastrado()));
                setCdTratamentoFeriadosFiltro(saidaDTO.getCdTratamentoFeriadosDtaPag());
                setCdTipoProcessamentoFiltro(saidaDTO.getCdTipoProcessamento());
                setCdTipoRejAgendamentoFiltro(saidaDTO.getCdTipoRejeicaoAgendamento());
                setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdTipoRejeicaoEfetivacao());
                setCdPrioridadeDebitoFiltro(saidaDTO.getCdPrioridadeDebito());
                setCdGerLancFutDebitoFiltro(saidaDTO.getRdoGerarLanctoFuturoDeb());
                setCdGerLancFutCreditoFiltro(saidaDTO.getRdoGerarLanctoFuturoCred());
                setCdPermiteOutrosTipos(String.valueOf(saidaDTO.getRdoPermiteOutrosTiposIncricaoFav()));
                setCdOcorrenciaDebito(saidaDTO.getCdOcorrenciaDebito());
                setCdPermiteFavConsultPagFiltro(saidaDTO.getRdoPermiteFavConsultarPag());
                setValorLimiteIndividualFiltro(saidaDTO.getValorLimiteIndividual());
                setDsValorLimiteIndividual(df.format(getValorLimiteIndividualFiltro()));
                setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiario());
                setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
                setQtddDiasExpiracao(String.valueOf(saidaDTO.getQntdDiasExpiracao()));
                setDsLocalEmissao(saidaDTO.getDsLocalEmissao());
                setQtddDiasFloating(saidaDTO.getCdDiaFloatPagamento());
                setCboProcessamentoEfetivacaoPagamento(Long.parseLong(
                    String.valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
                setDsProcessamentoEfetivacaoPagamento(saidaDTO.getDsMomentoProcessamentoPagamento());
                setCboTipoRejeicaoAgendamento(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoAgendaLote())));
                setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendaLote());
                setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoEfetivacaoLote())));
                setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
                setQtddMaximaRegistrosInconsistentesLote(saidaDTO.getQtMaximaInconLote());
                setPercMaxRegInconsistente(saidaDTO.getCdPercentualMaximoInconLote());
                setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO.getCdPrioridadeEfetivacaoPagamento())));
                setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
                setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO.getCdValidacaoNomeFavorecido().toString());
                setRdoUtilizaCadastroFavorecidoControlePagamentos(saidaDTO.getCdUtilizacaoFavorecidoControle().toString());
                setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO.getVlFavorecidoNaoCadastro());
                setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO.getCdConsSaldoPagamento())));
                setDsTipoConsSaldo(saidaDTO.getDsConsSaldoPagamento());
                setQtddDiasRepique(saidaDTO.getQtDiaRepiqConsulta());
                setRdoPermiteFavorecidoConsultarPagamento(saidaDTO.getCdFavorecidoConsPagamento().toString());
                setRdoGerarLancFutDeb(saidaDTO.getCdLancamentoFuturoDebito().toString());
                setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(
                    String.valueOf(saidaDTO.getCdTipoContaFavorecido())));
                setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO.getDsTipoConsistenciaFavorecido());
                setCboTratamentoFeriadosDataPagamento(Long.parseLong(String.valueOf(
                    saidaDTO.getCdPagamentoNaoUtilizado())));
                setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtilizado());
                setCboOcorrenciaDebito(Long.parseLong(String.valueOf(saidaDTO.getCdMomentoDebitoPagamento())));
                setDsOcorrenciaDebito(saidaDTO.getDsMomentoDebitoPagamento());
                setCboTipoInscricaoFavorecido(Long.parseLong(String.valueOf(saidaDTO.getCdTipoIsncricaoFavorecido())));
                setDsTipoInscricaoFavorecido(saidaDTO.getDsTipoIscricaoFavorecido());
                setQuantidadeDiasExpiracao(saidaDTO.getQtDiaExpiracao());
                setValorLimiteIndividual(saidaDTO.getVlLimiteIndividualPagamento());
                setValorLimiteDiario(saidaDTO.getVlLimiteDiaPagamento());
            }

            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 20) {
                setCdTipoConsultaSaldoFiltro(saidaDTO.getCdTipoConsultaSaldo());
                setCdUtilizaCadFavorFiltro(saidaDTO.getRdoUtilizaCadFavControlePag());
                setValorMaxPagFavNaoCadastrado(saidaDTO.getValorMaxPagFavNaoCadastrado());
                setValorMaxPagFavNaoCadastradoDesc(df.format(getValorMaxPagFavNaoCadastrado()));
                setCdTipoProcessamentoFiltro(saidaDTO.getCdTipoProcessamento());
                setCdTipoRejAgendamentoFiltro(saidaDTO.getCdTipoRejeicaoAgendamento());
                setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdTipoRejeicaoEfetivacao());
                setCdPrioridadeDebitoFiltro(saidaDTO.getCdPrioridadeDebito());
                setCdGerLancFutDebitoFiltro(saidaDTO.getRdoGerarLanctoFuturoDeb());
                setCdGerLancFutCreditoFiltro(saidaDTO.getRdoGerarLanctoFuturoCred());
                setCdGerarLancamentoProgramado(String.valueOf(saidaDTO.getRdoGerarLanctoProgramado()));
                setCdPermiteFavConsultPagFiltro(saidaDTO.getRdoPermiteFavConsultarPag());
                setCdEfetuaConsistencia(String.valueOf(saidaDTO.getCdEfetuaConsistencia()));
                setCdTipoTratamento(saidaDTO.getCdTipoTratamentoContasTransferidas());
                setCdPermiteEstorno(saidaDTO.getCdPermiteEstornoPagamento());
                setQtddMaximaRegistro(String.valueOf(saidaDTO.getQntdMaxRegInconsistentes()));
                setValorLimiteIndividualFiltro(saidaDTO.getValorLimiteIndividual());
                setDsValorLimiteIndividual(df.format(getValorLimiteIndividualFiltro()));
                setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiario());
                setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
                setPercentualMaximoRegistro(String.valueOf(saidaDTO.getPercentualMaxRegInconsistenciaLote()));
                setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO.getQuantDiasRepique()));
                setQtddDiasFloating(saidaDTO.getCdDiaFloatPagamento());
                setCboProcessamentoEfetivacaoPagamento(Long.parseLong(
                    String.valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
                setDsProcessamentoEfetivacaoPagamento(saidaDTO.getDsMomentoProcessamentoPagamento());
                setCboTipoRejeicaoAgendamento(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoAgendaLote())));
                setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendaLote());
                setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoEfetivacaoLote())));
                setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
                setQtddMaximaRegistrosInconsistentesLote(saidaDTO.getQtMaximaInconLote());
                setPercMaxRegInconsistente(saidaDTO.getCdPercentualMaximoInconLote());
                setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO.getCdPrioridadeEfetivacaoPagamento())));
                setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
                setCboPermiteEstornoPagamento(0L);
                setDsPermiteEstornoPagamento("");
                setRdoUtilizaCadastroFavorecidoControlePagamentos(saidaDTO.getCdUtilizacaoFavorecidoControle().toString());
                setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO.getVlFavorecidoNaoCadastro());
                setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO.getCdConsSaldoPagamento())));
                setDsTipoConsSaldo(saidaDTO.getDsConsSaldoPagamento());
                setQtddDiasRepique(saidaDTO.getQtDiaRepiqConsulta());
                setRdoPermiteFavorecidoConsultarPagamento(saidaDTO.getCdFavorecidoConsPagamento().toString());
                setRdoEfetuaConEspBenef(saidaDTO.getCdCtciaEspecieBeneficio().toString());
                setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String.valueOf(
                    saidaDTO.getCdTipoContaFavorecido())));
                setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO.getDsTipoConsistenciaFavorecido());
                setRdoGerarLancFutDeb(saidaDTO.getCdLancamentoFuturoDebito().toString());
                setRdoGerarLancFutCred(saidaDTO.getCdLancamentoFuturoCredito().toString());
                setRdoGerarLancProgramado(saidaDTO.getCdIndicadorLancamentoPagamento().toString());
                setCboTratamentoFeriadosDataPagamento(Long.parseLong(
                    String.valueOf(saidaDTO.getCdPagamentoNaoUtilizado())));
                setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtilizado());
                setCboTipoTratamentoContaTransferida(Long.parseLong(saidaDTO.getCdContratoContaTransferencia().toString()));
                setDsTipoTratamentoContaTransferida(saidaDTO.getDsContratoContaTransferencia());
                setCboTipoContaCredito(Long.parseLong(String.valueOf(saidaDTO.getCdMeioPagamentoCredito())));
                setDsTipoContaCredito(saidaDTO.getDsMeioPagamentoCredito());
                setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO.getCdValidacaoNomeFavorecido().toString());
                setRdoExigeLibLoteProcessado(saidaDTO.getCdLiberacaoLoteProcesso().toString());
                setValorLimiteIndividual(saidaDTO.getVlLimiteIndividualPagamento());
                setValorLimiteDiario(saidaDTO.getVlLimiteDiaPagamento());
                setRdoUtilizaLancPersonalizado(saidaDTO.getCdIndLancamentoPersonalizado().toString());
                setDsPreenchimentoLancamentoPersonalizado(saidaDTO.getDsPreenchimentoLancamentoPersonalizado());
                setRdoTipoFormatacaoPrimeiraLinha(saidaDTO.getCdPreenchimentoLancamentoPersonalizado().toString());
                
                if(getCdParametroTela2().equals(TIPO_SERVICO_PAGAMENTO_FORNECEDOR)){
                	carregaListaDemonstra2LinhaExtrato(60);
                	setExibeOrigemSegundaViaExtrato(true);
                }else if(getCdParametroTela2().equals(TIPO_SERVICO_PAGAMENTO_SALARIO)){
                	carregaListaDemonstra2LinhaExtrato(59);
                	setExibeOrigemSegundaViaExtrato(false);
                }
                                
            }

            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 21) {
                setCdTipoConsultaSaldoFiltro(saidaDTO.getCdTipoConsultaSaldo());
                setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO.getQuantDiasRepique()));
                setCdUtilizaCadFavorFiltro(saidaDTO.getRdoUtilizaCadFavControlePag());
                setCdPrioridadeDebitoFiltro(saidaDTO.getCdPrioridadeDebito());
                setCdGerLancFutDebitoFiltro(saidaDTO.getRdoGerarLanctoFuturoDeb());
                setCdGerLancFutCreditoFiltro(saidaDTO.getRdoGerarLanctoFuturoCred());
                setValorMaxPagFavNaoCadastrado(saidaDTO.getValorMaxPagFavNaoCadastrado());
                setValorMaxPagFavNaoCadastradoDesc(df.format(getValorMaxPagFavNaoCadastrado()));
                setCdTratamentoFeriadosFiltro(saidaDTO.getCdTratamentoFeriadosDtaPag());
                setCdTipoProcessamentoFiltro(saidaDTO.getCdTipoProcessamento());
                setCdTipoRejAgendamentoFiltro(saidaDTO.getCdTipoRejeicaoAgendamento());
                setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdTipoRejeicaoEfetivacao());
                setCdPermiteDebitoOnline(String.valueOf(saidaDTO.getRdoPermiteDebitoOnline()));
                setValorLimiteIndividualFiltro(saidaDTO.getValorLimiteIndividual());
                setDsValorLimiteIndividual(df.format(getValorLimiteIndividualFiltro()));
                setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiario());
                setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
                setQtddDiasFloating(saidaDTO.getCdDiaFloatPagamento());
                setCboProcessamentoEfetivacaoPagamento(Long.parseLong(
                    String.valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
                setDsProcessamentoEfetivacaoPagamento(saidaDTO.getDsMomentoProcessamentoPagamento());
                setCboTipoRejeicaoAgendamento(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoAgendaLote())));
                setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendaLote());
                setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoEfetivacaoLote())));
                setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
                setQtddMaximaRegistrosInconsistentesLote(saidaDTO.getQtMaximaInconLote());
                setPercMaxRegInconsistente(saidaDTO.getCdPercentualMaximoInconLote());
                setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO.getCdPrioridadeEfetivacaoPagamento())));
                setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
                setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO.getCdValidacaoNomeFavorecido().toString());
                setRdoUtilizaCadastroFavorecidoControlePagamentos(saidaDTO.getCdUtilizacaoFavorecidoControle().toString());
                setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO.getVlFavorecidoNaoCadastro());
                setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO.getCdConsSaldoPagamento())));
                setDsTipoConsSaldo(saidaDTO.getDsConsSaldoPagamento());
                setQtddDiasRepique(saidaDTO.getQtDiaRepiqConsulta());
                setRdoPermiteFavorecidoConsultarPagamento(saidaDTO.getCdFavorecidoConsPagamento().toString());
                setCboTratamentoFeriadosDataPagamento(Long.parseLong(String.valueOf(
                    saidaDTO.getCdPagamentoNaoUtilizado())));
                setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtilizado());
                setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String.valueOf(
                    saidaDTO.getCdTipoContaFavorecido())));
                setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO.getDsTipoConsistenciaFavorecido());
                setRdoGerarLancFutDeb(saidaDTO.getCdLancamentoFuturoDebito().toString());
                setRdoGerarLancFutCred(saidaDTO.getCdLancamentoFuturoCredito().toString());
                setRdoPermiteDebitoOnline(saidaDTO.getCdPermissaoDebitoOnline().toString());
                setValorLimiteIndividual(saidaDTO.getVlLimiteIndividualPagamento());
                setValorLimiteDiario(saidaDTO.getVlLimiteDiaPagamento());
            }
            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 22
                            || modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 23) {
                setCdTipoConsultaSaldoFiltro(saidaDTO.getCdTipoConsultaSaldo());
                setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO.getQuantDiasRepique()));
                setCdUtilizaCadFavorFiltro(saidaDTO.getRdoUtilizaCadFavControlePag());
                setValorMaxPagFavNaoCadastrado(saidaDTO.getValorMaxPagFavNaoCadastrado());
                setValorMaxPagFavNaoCadastradoDesc(df.format(getValorMaxPagFavNaoCadastrado()));
                setCdTratamentoFeriadosFiltro(saidaDTO.getCdTratamentoFeriadosDtaPag());
                setCdTipoProcessamentoFiltro(saidaDTO.getCdTipoProcessamento());
                setCdTipoRejAgendamentoFiltro(saidaDTO.getCdTipoRejeicaoAgendamento());
                setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdTipoRejeicaoEfetivacao());
                setCdPrioridadeDebitoFiltro(saidaDTO.getCdPrioridadeDebito());
                setCdGerLancFutDebitoFiltro(saidaDTO.getRdoGerarLanctoFuturoDeb());
                setPermitePagarVencido(String.valueOf(saidaDTO.getRdoPermitePagarVencido()));
                setQtddLimiteDiasPagamentoVencido(String.valueOf(saidaDTO.getQntdLimiteDiasPagVencido()));
                setPermitePagarMenor(String.valueOf(saidaDTO.getRdoPermitePagarMenor()));
                setCdPermiteFavConsultPagFiltro(saidaDTO.getRdoPermiteFavConsultarPag());
                setValorLimiteIndividualFiltro(saidaDTO.getValorLimiteIndividual());
                setDsValorLimiteIndividual(df.format(getValorLimiteIndividualFiltro()));
                setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiario());
                setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
                setQtddDiasFloating(saidaDTO.getCdDiaFloatPagamento());
                setCboProcessamentoEfetivacaoPagamento(Long.parseLong(String.valueOf(
                    saidaDTO.getCdMomentoProcessamentoPagamento())));
                setDsProcessamentoEfetivacaoPagamento(saidaDTO.getDsMomentoProcessamentoPagamento());
                setCboTipoRejeicaoAgendamento(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoAgendaLote())));
                setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendaLote());
                setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoEfetivacaoLote())));
                setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
                setQtddMaximaRegistrosInconsistentesLote(saidaDTO.getQtMaximaInconLote());
                setPercMaxRegInconsistente(saidaDTO.getCdPercentualMaximoInconLote());
                setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO.getCdPrioridadeEfetivacaoPagamento())));
                setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
                setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO.getCdValidacaoNomeFavorecido().toString());
                setRdoUtilizaCadastroFavorecidoControlePagamentos(
                    saidaDTO.getCdUtilizacaoFavorecidoControle().toString());
                setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO.getVlFavorecidoNaoCadastro());
                setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO.getCdConsSaldoPagamento())));
                setDsTipoConsSaldo(saidaDTO.getDsConsSaldoPagamento());
                setQtddDiasRepique(saidaDTO.getQtDiaRepiqConsulta());
                setRdoPermiteFavorecidoConsultarPagamento(saidaDTO.getCdFavorecidoConsPagamento().toString());
                setRdoGerarLancFutDeb(saidaDTO.getCdLancamentoFuturoDebito().toString());
                setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String.valueOf(
                    saidaDTO.getCdTipoContaFavorecido())));
                setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO.getDsTipoConsistenciaFavorecido());
                setCboTratamentoFeriadosDataPagamento(Long.parseLong(String.valueOf(
                    saidaDTO.getCdPagamentoNaoUtilizado())));
                setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtilizado());
                setRdoPermitePagarVencido(saidaDTO.getCdAgendaPagamentoVencido().toString());
                setRdoPermitePagarMenor(saidaDTO.getCdAgendaValorMenor().toString());
                setQuantidadeLimiteDiasPagamentoVencido(saidaDTO.getQtMaximaTituloVencido());
                setValorLimiteIndividual(saidaDTO.getVlLimiteIndividualPagamento());
                setValorLimiteDiario(saidaDTO.getVlLimiteDiaPagamento());
                setDsTituloDdaRetorno(saidaDTO.getDsTituloDdaRetorno());
                setRdoIndicadorDdaRetorno(saidaDTO.getCdTituloDdaRetorno().toString());
                BigDecimal percDiferenca = null;
                if (saidaDTO.getVlPercentualDiferencaTolerada() != null
                    && BigDecimal.ZERO.compareTo(saidaDTO.getVlPercentualDiferencaTolerada()) != 0) {
                    percDiferenca = saidaDTO.getVlPercentualDiferencaTolerada();
                }
                setVlPercentualDiferencaTolerada(percDiferenca);
                setCboConsistenciaCpfCnpjBenefAvalNpc(Long.parseLong(String.valueOf(
                        saidaDTO.getCdConsistenciaCpfCnpjBenefAvalNpc()))); 
                setDsConsistenciaCpfCnpjBenefAvalNpc(saidaDTO.getDsConsistenciaCpfCnpjBenefAvalNpc());
            }

            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 25) {
                setCdTipoConsultaSaldoFiltro(saidaDTO.getCdTipoConsultaSaldo());
                setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO.getQuantDiasRepique()));
                setCdUtilizaCadFavorFiltro(saidaDTO.getRdoUtilizaCadFavControlePag());
                setValorMaxPagFavNaoCadastrado(saidaDTO.getValorMaxPagFavNaoCadastrado());
                setValorMaxPagFavNaoCadastradoDesc(df.format(getValorMaxPagFavNaoCadastrado()));
                setCdTratamentoFeriadosFiltro(saidaDTO.getCdTratamentoFeriadosDtaPag());
                setCdTipoProcessamentoFiltro(saidaDTO.getCdTipoProcessamento());
                setCdTipoRejAgendamentoFiltro(saidaDTO.getCdTipoRejeicaoAgendamento());
                setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdTipoRejeicaoEfetivacao());
                setCdPermiteFavConsultPagFiltro(saidaDTO.getRdoPermiteFavConsultarPag());
                setValorLimiteIndividualFiltro(saidaDTO.getValorLimiteIndividual());
                setDsValorLimiteIndividual(df.format(getValorLimiteIndividualFiltro()));
                setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiario());
                setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
                setQtddDiasFloating(saidaDTO.getCdDiaFloatPagamento());
                setCboProcessamentoEfetivacaoPagamento(Long.parseLong(String.valueOf(
                    saidaDTO.getCdMomentoProcessamentoPagamento())));
                setDsProcessamentoEfetivacaoPagamento(saidaDTO.getDsMomentoProcessamentoPagamento());
                setCboTipoRejeicaoAgendamento(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoAgendaLote())));
                setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendaLote());
                setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoEfetivacaoLote())));
                setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
                setQtddMaximaRegistrosInconsistentesLote(saidaDTO.getQtMaximaInconLote());
                setPercMaxRegInconsistente(saidaDTO.getCdPercentualMaximoInconLote());
                setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO.getCdPrioridadeEfetivacaoPagamento())));
                setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
                setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO.getCdValidacaoNomeFavorecido().toString());
                setRdoUtilizaCadastroFavorecidoControlePagamentos(
                    saidaDTO.getCdUtilizacaoFavorecidoControle().toString());
                setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO.getVlFavorecidoNaoCadastro());
                setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO.getCdConsSaldoPagamento())));
                setDsTipoConsSaldo(saidaDTO.getDsConsSaldoPagamento());
                setQtddDiasRepique(saidaDTO.getQtDiaRepiqConsulta());
                setRdoPermiteFavorecidoConsultarPagamento(saidaDTO.getCdFavorecidoConsPagamento().toString());
                setRdoGerarLancFutDeb(saidaDTO.getCdLancamentoFuturoDebito().toString());
                setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String.valueOf(
                    saidaDTO.getCdTipoContaFavorecido())));
                setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO.getDsTipoConsistenciaFavorecido());
                setCboTratamentoFeriadosDataPagamento(Long.parseLong(String.valueOf(
                    saidaDTO.getCdPagamentoNaoUtilizado())));
                setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtilizado());
                setValorLimiteIndividual(saidaDTO.getVlLimiteIndividualPagamento());
                setValorLimiteDiario(saidaDTO.getVlLimiteDiaPagamento());
            }

            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 26) {
                setCdTipoRastreamentoTitulos(saidaDTO.getCdTipoRastreamentoTitulos());
                setRastrearTitulo(String.valueOf(saidaDTO.getRdoRastrearTitTerceiros()));
                setRastrearNotas(String.valueOf(saidaDTO.getRdoRastrearNotasFiscais()));
                setCapturarTitulos(String.valueOf(saidaDTO.getRdoCapturarTitDtaRegistro()));
                setAgendarTituloProprio(String.valueOf(saidaDTO.getRdoAgendarTitRastProp()));
                setAgendarTituloFilial(String.valueOf(saidaDTO.getRdoRastrearTitRastreadoFilial()));
                setBloquearEmissaoPapeleta(String.valueOf(saidaDTO.getRdoBloquearEmissaoPap()));
                setDsDataInicioBloqueioPapeleta(FormatarData.formatarDataFromPdc(saidaDTO.getDataInicioBloqPapeleta()));
                setDataInicioBloqueioPapeleta(FormatarData.formataDiaMesAnoFromPdc(
                    saidaDTO.getDataInicioBloqPapeleta()));
                setDsDataInicioRastreamento(FormatarData.formatarDataFromPdc(saidaDTO.getDataInicioRastreamento()));
                setDataInicioRastreamento(FormatarData.formataDiaMesAnoFromPdc(saidaDTO.getDataInicioRastreamento()));
                setDsDataRegistroTitulo(null);
                setDataRegistroTitulo(null);
                setCboTipoRastreamentoTitulo(Long.valueOf(String.valueOf(
                    saidaDTO.getCdCriterioRastreabilidadeTitulo())));
                setDsTipoRastreamentoTitulo(saidaDTO.getDsCriterioRastreabilidadeTitulo());
                setRdoRastrearTituloTerceiros(String.valueOf(saidaDTO.getCdRastreabilidadeTituloTerceiro()));
                setRdoRastrearNotasFiscais(String.valueOf(saidaDTO.getCdRastreabilidadeNotaFiscal()));
                setRdoCapTitulosDtReg(String.valueOf(saidaDTO.getCdCapituloTituloRegistro()));
                setDsCapituloTituloRegistro(saidaDTO.getDsCapituloTituloRegistro());
                setRdoAgendarTituloRastProp(String.valueOf(saidaDTO.getCdIndicadorAgendaTitulo()));
                setRdoAgendarTituloRastreadoFilial(String.valueOf(saidaDTO.getCdAgendaRastreabilidadeFilial()));
                setRdoBloqEmissaoPapeleta(String.valueOf(saidaDTO.getCdBloqueioEmissaoPplta()));
                setRdoAdesaoSacadoEletronico(String.valueOf(saidaDTO.getCdIndicadorAdesaoSacador()));
                setDtInicioRastreamento(FormatarData.formataDiaMesAnoFromPdc(saidaDTO
                    .getDtInicioRastreabilidadeTitulo()));
                setDtInicioBloqPapeleta(FormatarData.formataDiaMesAnoFromPdc(saidaDTO
                    .getDtInicioBloqueioPplta()));
                setRdoExigeIdentificacaoFilialAutorizacao(saidaDTO.getCdExigeAutFilial().toString());
                permiteAlteracoesTitulo();
            }

            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 27) {
                setCboAcaoParaNaoComprovacao(Long.parseLong(saidaDTO.getCdAcaoNaoVida().toString()));
                setDsAcaoParaNaoComprovacao(saidaDTO.getDsAcaoNaoVida());
                setQtddMesesPeriodicidadeComprovacao(saidaDTO.getQtMesComprovante());
                setRdoEmiteAvitoCompVida(saidaDTO.getCdIndicadorEmissaoAviso().toString());
                setCboDestinoEnvio(Long.parseLong(saidaDTO.getCdDestinoComprovante().toString()));
                setDsDestinoEnvio(saidaDTO.getDsDestinoComprovante());
                setCboCadConsultaEndEnvio(Long.parseLong(saidaDTO.getCdConsEndereco().toString()));
                setDsCadConsultaEndEnvio(saidaDTO.getDsConsEndereco());
                setRdoUtilizaMensagemPersonalizada(saidaDTO.getCdIndicadorMensagemPerso().toString());
                setQtddDiasEmissaoAvisoAntesInicioComprovacao(saidaDTO.getQtAntecedencia());
                setQtddDiasEmissaoAvisoAntesVencimento(saidaDTO.getQtAnteriorVencimentoComprovado());
                setCodigoFormulario(String.valueOf(saidaDTO.getCdFormularioContratoCliente()));

            }
            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 28
                            || modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 29
                            || modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 30
                            || modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 31) {

                setCdTipoConsultaSaldoFiltro(saidaDTO.getCdTipoConsultaSaldo());
                setValorLimiteIndividualFiltro(saidaDTO.getValorLimiteIndividual());
                setDsValorLimiteIndividual(df.format(getValorLimiteIndividualFiltro()));
                setCdEfetivacao(saidaDTO.getCdTipoEfetivacao());
                setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiario());
                setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
                setCdPermiteConsulFavorecido(saidaDTO.getCdPermiteConsultaFavorecido());
                setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO.getQuantDiasRepique()));
                setCdTratamentoFeriadosFiltro(saidaDTO.getCdTratamentoFeriado());
                setCdTipoProcessamentoFiltro(saidaDTO.getCdTipoProcessamento());
                setCdTipoRejAgendamentoFiltro(saidaDTO.getCdTipoRejeicaoAgendamento());
                setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdTipoRejeicaoEfetivacao());
                setCdPrioridadeDebitoFiltro(saidaDTO.getCdPrioridadeDebito());
                setCdControlePagFavorecido(saidaDTO.getCdControlePagamentoFavorecido());
                setValorMaxPagFavNaoCadastrado(saidaDTO.getValorMaxPagFavNaoCadastrado());
                setValorMaxPagFavNaoCadastradoDesc(df.format(getValorMaxPagFavNaoCadastrado()));
                setPercentualInconsLote(String.valueOf(saidaDTO.getPorcentInconsistenciaLote()));
                setQtddMaxInconsLote(Integer.toString(saidaDTO.getQtdeMaxInconsistenciaPorLote()));
                setCdPermiteContingenciaPag(saidaDTO.getPermiteContingenciaPag());
                setQtddDiasFloating(saidaDTO.getCdDiaFloatPagamento());
                setCboProcessamentoEfetivacaoPagamento(Long.parseLong(String.valueOf(
                    saidaDTO.getCdMomentoProcessamentoPagamento())));
                setDsProcessamentoEfetivacaoPagamento(saidaDTO.getDsMomentoProcessamentoPagamento());
                setCboTipoRejeicaoAgendamento(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoAgendaLote())));
                setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendaLote());
                setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO.getCdRejeicaoEfetivacaoLote())));
                setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
                setQtddMaximaRegistrosInconsistentesLote(saidaDTO.getQtMaximaInconLote());
                setPercMaxRegInconsistente(saidaDTO.getCdPercentualMaximoInconLote());
                setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO.getCdPrioridadeEfetivacaoPagamento())));
                setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
                setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO.getCdValidacaoNomeFavorecido().toString());
                setRdoUtilizaCadastroFavorecidoControlePagamentos(
                    saidaDTO.getCdUtilizacaoFavorecidoControle().toString());
                setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO.getVlFavorecidoNaoCadastro());
                setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO.getCdConsSaldoPagamento())));
                setDsTipoConsSaldo(saidaDTO.getDsConsSaldoPagamento());
                setQtddDiasRepique(saidaDTO.getQtDiaRepiqConsulta());
                setRdoPermiteFavorecidoConsultarPagamento(saidaDTO.getCdFavorecidoConsPagamento().toString());
                setRdoGerarLancFutDeb(saidaDTO.getCdLancamentoFuturoDebito().toString());
                setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String.valueOf(
                    saidaDTO.getCdTipoContaFavorecido())));
                setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO.getDsTipoConsistenciaFavorecido());
                setCboTratamentoFeriadosDataPagamento(Long.parseLong(String.valueOf(
                    saidaDTO.getCdPagamentoNaoUtilizado())));
                setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtilizado());
                setValorLimiteIndividual(saidaDTO.getVlLimiteIndividualPagamento());
                setValorLimiteDiario(saidaDTO.getVlLimiteDiaPagamento());
            }

            if (modalidadeTipoServicoSaidaDTO.getCdParametroTela() == 32) {
                setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiario());
                setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
                setCdEfetivacao(saidaDTO.getCdTipoEfetivacao());
                setValorLimiteIndividualFiltro(saidaDTO.getValorLimiteIndividual());
                setDsValorLimiteIndividual(df.format(getValorLimiteIndividualFiltro()));
                setCdTratamentoFeriadosFiltro(saidaDTO.getCdTratamentoFeriado());
                setCdTipoProcessamentoFiltro(saidaDTO.getCdTipoProcessamento());
                setCdTratValorDivergente(saidaDTO.getCdTratamentoValorDivergente());
                setCdControlePagFavorecido(saidaDTO.getCdControlePagFavorecido());
                setValorMaxPagFavNaoCadastrado(saidaDTO.getValorMaxPagFavNaoCadastrado());
                setValorMaxPagFavNaoCadastradoDesc(df.format(getValorMaxPagFavNaoCadastrado()));
                setCdTipoConsultaSaldoFiltro(saidaDTO.getCdTipoConsultaSaldo());
                setCdTipoConsisProprietario(saidaDTO.getCdTipoConsistenciaCpfCnpjProp());
                setDsTipoConsisProprietario(saidaDTO.getDsTipoConsistenciaCpfCnpjProp());
                setQtddDiasFloating(saidaDTO.getCdDiaFloatPagamento());
                setCboProcessamentoEfetivacaoPagamento(Long.valueOf(String.valueOf(
                    saidaDTO.getCdMomentoProcessamentoPagamento())));
                setDsProcessamentoEfetivacaoPagamento(saidaDTO.getDsMomentoProcessamentoPagamento());
                setCboTipoRejeicaoAgendamento(Long.valueOf(String.valueOf(saidaDTO.getCdRejeicaoAgendaLote())));
                setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendaLote());
                setCboTipoRejeicaoEfetivacao(Long.valueOf(String.valueOf(saidaDTO.getCdRejeicaoEfetivacaoLote())));
                setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
                setQtddMaximaRegistrosInconsistentesLote(saidaDTO.getQtMaximaInconLote());
                setPercMaxRegInconsistente(saidaDTO.getCdPercentualMaximoInconLote());
                setCboPrioridadeDebito(Long.valueOf(String.valueOf(saidaDTO.getCdPrioridadeEfetivacaoPagamento())));
                setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
                setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO.getCdValidacaoNomeFavorecido().toString());
                setRdoUtilizaCadastroFavorecidoControlePagamentos(
                    saidaDTO.getCdUtilizacaoFavorecidoControle().toString());
                setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO.getVlFavorecidoNaoCadastro());
                setCboTipoConsultaSaldo(Long.valueOf(String.valueOf(saidaDTO.getCdConsSaldoPagamento())));
                setDsTipoConsSaldo(saidaDTO.getDsConsSaldoPagamento());
                setQtddDiasRepique(saidaDTO.getQtDiaRepiqConsulta());
                setRdoPermiteFavorecidoConsultarPagamento(saidaDTO.getCdFavorecidoConsPagamento().toString());
                setRdoGerarLancFutDeb(saidaDTO.getCdLancamentoFuturoDebito().toString());
                setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String.valueOf(
                    saidaDTO.getCdTipoContaFavorecido())));
                setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO.getDsTipoConsistenciaFavorecido());
                setCboTratamentoFeriadosDataPagamento(Long.valueOf(String.valueOf(
                    saidaDTO.getCdPagamentoNaoUtilizado())));
                setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtilizado());
                setCboTratamentoValorDivergente(Long.valueOf(String.valueOf(saidaDTO.getCdTipoDivergenciaVeiculo())));
                setDsTratamentoValorDivergente(saidaDTO.getDsTipoDivergenciaVeiculo());
                setCboTipoConsistenciaCpfCnpjProprietario(Long.valueOf(String.valueOf(
                    saidaDTO.getCdCtciaProprietarioVeiculo())));
                setDsTipoConsistenciaCpfCnpjProprietario(saidaDTO.getDsCtciaProprietarioVeiculo());
                setValorLimiteIndividual(saidaDTO.getVlLimiteIndividualPagamento());
                setValorLimiteDiario(saidaDTO.getVlLimiteDiaPagamento());
            }

            setUsuarioInclusao(saidaDTO.getUsuarioInclusao());
            setUsuarioManutencao(saidaDTO.getUsuarioManutencao());
            setComplementoInclusao(saidaDTO.getComplementoInclusao() == null
                || saidaDTO.getComplementoInclusao().equals("0")
                ? ""
                    : saidaDTO.getComplementoInclusao());
            setComplementoManutencao(saidaDTO.getComplementoManutencao() == null
                || saidaDTO.getComplementoManutencao().equals("0")
                ? ""
                    : saidaDTO.getComplementoManutencao());
            setDataHoraInclusao(saidaDTO.getHoraInclusao());
            setDataHoraManutencao(saidaDTO.getHoraManutencao());
            setTipoCanalInclusao(saidaDTO.getCdCanalInclusao() == 0 ? "" : saidaDTO
                .getCdCanalInclusao()
                + " - " + saidaDTO.getDsCanalInclusao());
            setTipoCanalManutencao(saidaDTO.getCdCanalManutencao() == 0
                ? ""
                    : saidaDTO.getCdCanalManutencao() + " - "
                    + saidaDTO.getDsCanalManutencao());
            setCdConsultaSaldoValorSuperior(PgitUtil.verificaIntegerNulo(
                saidaDTO.getCdConsultaSaldoValorSuperior()).longValue());
            setDsConsultaSaldoValorSuperior(SelectItemUtils.obterLabelSelecItem(
                listaTipoConsultaSaldo, cdConsultaSaldoValorSuperior));
            setCdIndicadorFeriadoLocal(saidaDTO.getCdIndicadorFeriadoLocal());
            setRdoIndicadorSegundaLinhaExtrato(saidaDTO
                .getCdIndicadorSegundaLinha());
            setDsIndicadorSegundaLinhaExtrato(saidaDTO.getCdIndicadorSegundaLinha() == 1
                ? "Sim"
                    : "N�o");
            setDsRdoIndicadorSegundaLinhaExtrato(saidaDTO.getDsIndicadorSegundaLinha());

            if(getCdParametroTela2().equals(TIPO_SERVICO_PAGAMENTO_FORNECEDOR)
                            || getCdParametroTela2().equals(TIPO_SERVICO_PAGAMENTO_SALARIO)
                            || getCdParametroTela2().equals(TIPO_SERVICO_PAGAMENTO_TRIBUTOS)
                            || getCdParametroTela2().equals(TIPO_SERVICO_PAGAMENTO_BENEFICIOS)){

                ListarTipoProcessamentoLayoutArquivoEntradaDTO entradaListarProcessamento = 
                    new ListarTipoProcessamentoLayoutArquivoEntradaDTO();

                entradaListarProcessamento.setCdpessoaJuridicaContrato(getCdPessoaJuridica());
                entradaListarProcessamento.setCdTipoContratoNegocio(getCdTipoContrato());
                entradaListarProcessamento.setNrSequenciaContratoNegocio(getNrSequenciaContrato());
                entradaListarProcessamento.setCdParametroPesquisa(2);
                entradaListarProcessamento.setCdParametroTela(modalidadeTipoServicoSaidaDTO.getCdParametroTela());
                entradaListarProcessamento.setCdProdutoServicoOperacao(modalidadeTipoServicoSaidaDTO.getCdServico());
                entradaListarProcessamento.setCdProdutoOperacaoRelacionado(modalidadeTipoServicoSaidaDTO.getCdModalidade());
                entradaDTO.setCdModalidade(modalidadeTipoServicoSaidaDTO.getCdModalidade());
                entradaListarProcessamento.setCdRelacionamentoProduto(1);



                entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
                entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato());
                entradaDTO.setNrSequenciaContratoNegocio(getNrSequenciaContrato());

                entradaDTO.setCdServico(listaModalidadeTipoServicoSaidaDTO.getCdModalidade());
                entradaDTO.setCdModalidade(modalidadeTipoServicoSaidaDTO.getCdModalidade());
                entradaDTO.setCdParametroPesquisa(2);
                entradaDTO.setCdParametroTela(modalidadeTipoServicoSaidaDTO.getCdParametroTela());



                ListarTipoProcessamentoLayoutArquivoSaidaDTO saidaListarProcessamento = 
                    getComboService().listarTipoProcessamentoLayoutArquivo(entradaListarProcessamento);

                setListaProcessamentoEfetivacaoPagamento(new ArrayList<SelectItem>());
                for (int i = 0; i < saidaListarProcessamento.getOcorrencias().size(); i++) {

                    listaProcessamentoEfetivacaoPagamento.add(new SelectItem(Long.parseLong(
                        saidaListarProcessamento.getOcorrencias().get(i).getCdCombo().toString()), 
                        saidaListarProcessamento.getOcorrencias().get(i).getDsCombo()));
                }
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }


        if("S".equals(saidaVerificarAtributo.getCdIndicadorRejeicaoAgendaLote())){
            setDesabilitaIndicadorRejeicaoAgendaLote(false);
        } else {
            setDesabilitaIndicadorRejeicaoAgendaLote(true);
        }

        if("S".equals(saidaVerificarAtributo.getCdIndicadorRejeicaoEfetivacaoLote())){
            setDesabilitaIndicadorRejeicaoEfetivacaoLote(false);
        } else {
            setDesabilitaIndicadorRejeicaoEfetivacaoLote(true);
        }

        if("S".equals(saidaVerificarAtributo.getQtIndicadorMaximaInconLote())){
            setDesabilitaIndicadorMaximaInconLote(false);
        } else {
            setDesabilitaIndicadorMaximaInconLote(true);
        }

        if("S".equals(saidaVerificarAtributo.getCdIndicadorPercentualMaximoInconLote())){
            setDesabilitaIndicadorPercentualMaximoInconLote(false);
        } else {
            setDesabilitaIndicadorPercentualMaximoInconLote(true);
        }


    }

    /**
     * Gets the gerencia combo floating.
     *
     * @return the gerencia combo floating
     */
    public boolean getGerenciaComboFloating() {
        if (rdtoCadastroFloating != null
                        && rdtoCadastroFloating.compareTo(2) == 0) {
            setCboTipoDataControleFloating(0l);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Voltar.
     * 
     * @return the string
     */
    public String voltar() {
        setItemSelecionadoListaTipoServico(null);
        setItemSelecionadoLista3(null);
        setItemSelecionadoListaHistoricoModalidade(null);
        setQtdDiasReutilizacaoControlePgto(null);
        if (getListaGridTipoServico() != null) {
            for (ListarServicosSaidaDTO o : getListaGridTipoServico()) {
                o.setCheck(false);
            }
        }
        
        setOpcaoChecarTodos(false);
        //incluir();
        return "VOLTAR";
    }

    /**
     * Voltar.
     * 
     * @return the string
     */
    public String voltarcontrato2() {
        limparCampos();
        return "VOLTAR";
    }	

    /**
     * Voltar.
     * 
     * @return the string
     */
    public String voltarcontrato5() {
        setTipoLotePagamentoConf(null);
        setRadioFidelize(radioFidelize);  
        setAltoTurnover(altoTurnover);
        setOfertaCartaoPre(ofertaCartaoPre);
        setPeloApp(peloApp);
        
        return "VOLTAR";
    }	
    /**
     * Voltar modalidade configurar.
     * 
     * @return the string
     */
    public String voltarModalidadeConfigurar() {

        configurarModalidade();

        setItemSelecionadoLista2(null);
        setDesabilitarComboPrioridadeDebito(false);
        setDesabilitarComboTipoConsultaSaldo(false);
        setHabilitaQtdDiasFloating(false);
        setHabilitaRdoGerarLancamento(false);
        setDesabilitarCboConsistenciaCpfCnpjBenefAvalNpc(false);
        return "VOLTAR";
    }

    /**
     * Voltar sacado eletronico.
     * 
     * @return the string
     */
    public String voltarSacadoEletronico() {
        return "detModServicosManterContrato2";
    }

    /**
     * Voltar agregados.
     * 
     * @return the string
     */
    public String voltarAgregados() {
        return "conSacadoEletronico";
    }

    /**
     * Voltar incluir.
     * 
     * @return the string
     */
    public String voltarIncluir() {
        return "VOLTAR";
    }

    /**
     * Voltar participante.
     * 
     * @return the string
     */
    public String voltarParticipante() {
        return "VOLTAR";
    }

    /**
     * Voltar incluir modalidade.
     * 
     * @return the string
     */
    public String voltarIncluirModalidade() {
        return "VOLTAR";
    }

    /**
     * Limpar pesquisa.
     */
    public void limparPesquisa() {
        setItemSelecionadoLista(null);
    }

    /**
     * Configurar.
     * 
     * @return the string
     */
    public String configurar() {
        try {
            listarCombosTipoServico();
            carregaListaPeriodicidade();
            carregaListaTipoLayoutArquivo();
            limparConfiguracao();
            ListarModalidadeTipoServicoSaidaDTO listarModalidadeTipoServicoSaidaDTO = this.listaGridPesquisa.get(this.itemSelecionadoLista);
            setCdTipoServico(listarModalidadeTipoServicoSaidaDTO.getCdServico());
            setCdModalidade(listarModalidadeTipoServicoSaidaDTO.getCdModalidade());
            setCdParametroTela(listarModalidadeTipoServicoSaidaDTO.getCdParametroTela());
            controlaRdoCadastroProcuradores();
            controlaRdoCadastroManCadProcuradores();
            controlaCamposExpiracaoCredito();
            preencheDados();
            
            if (getCdParametroTela() > 0 && getCdParametroTela() < 12) {
                controlaRadioListaDebito();
                controlaRadioPesquisaDebitoVeiculos();
                setRdoUtilizaListaDebitos(saidaDTO.getCdIndicadorListaDebito().toString());
                return "CONFIGURAR";
            } else {
                return "CONFIGURAR2";
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }
        return "";
    }

    /**
     * Voltar configuracao.
     * 
     * @return the string
     */
    public String voltarConfiguracao() {
        if (getCdParametroTela() < 12) {
            return "VOLTAR_CONFIGURACAO1";
        } else {
            return "VOLTAR_CONFIGURACAO2";
        }
    }

    /**
     * Limpar configuracao.
     */
    public void limparConfiguracao() {
        setCdFormaEnvioPagamento(0);
        setCdFormaAutPagamento(0);
        setCdUtilizaPreAut(0);
        setCdTipoContFloating(0);
        setCdEmiteCartao(0);
        setCdTipoCartao(0);
        setQtddLimite(null);
        setDataLimite(new Date());
        setCdUtilizacaoRastreamentoDebito(0);
        setCdPeriodicidadeRastreamento(0);
        setCdAgendDebito(0);
        setCdPeriodicidadeCobranca(0);
        setDiaFechamentoApuracao("");
        setQtddDiasCobrancaTarifa(null);
        setCdPeriodicidadeReajusteTarifa(0);
        setCdTipoReajusteTarifa(0);
        setQtddMesesReajuste(null);
        setIndiceEconomicoReajuste("");
        setTipoAlimentacao("");
        setCdTipoAutorizacaoPagamento(0);
        setCdInformaAgenciaContaCredito(0);
        setCdTipoIdenBeneficio(0);
        setCdUtilizaCadOrgPagadores(0);
        setCdManCadastroProcuradores(0);
        setCdPeriodicidadeManutencao(0);
        setCdFormaManutencaoCadastroProcurador(0);
        setCdPossuiExpiracaoCredito(0);
        setCdFormaControleExpiracao(0);
        setQtddDiasExpiracao(null);
        setCdTratFeriadoFimVigencia(0);
        setCdTipoPrestContaCreditoPago(0);
        setCdFormaManutencaoCadastro(0);
        setQtddDiasInativacao(null);
        setCdConsistenciaCpfCnpfFavorecido(0);
        setCdPeriodicidade(0);
        setCdDestino(0);
        setCdPermiteCorrespAberta(0);
        setCdDemonsInfReservada(0);
        setCdAgruparCorrespondencia(0);
        setQuantidadeVias("");
        setCdTipoRejeicao(0);
        setCdTipoEmissao(0);
        setCdMidiaDisponivelCorrentista(0);
        setCdMidiaDisponivelNaoCorrentista(0);
        setCdTipoEnvioCorrespondencia(0);
        setRdtoCadastroFloating(null);

        setCdFrasesPreCadastradas(0);
        setCdCobrarTarifasFuncionarios(0);
        setQtddMesesEmissao(null);
        setQtddLinhas(null);
        setQtddVias(null);
        setQtddViasPagas(null);
        setCdFormularioImpressao(0);
        setCdPeriodicidadeCobrancaTarifa(0);
        setFechamentoApuracao("");
        setDiasCobrancaAposApuracao("");
        setCdTipoReajuste(0);
        setMesesReajusteAutomatico("");
        setCdIndiceReajuste(0);
        setCdTipoIdentificadorBeneficiario(0);
        setCdTipoConsistenciaIdentificador(0);
        setCdCondicaoEnquadramento(0);
        setCdCriterioPrincipalEnquadramento(0);
        setCdCriterioCompostoEnquadramento(0);
        setQtddEtapasRecadastramento(null);
        setQtddFasesEtapa(null);
        setQtddMesesPrazoFase(null);
        setQtddMesesPrazoEtapa(null);
        setCdPermiteEnvioRemessa(0);
        setCdPeriodicidadeEnvioRemessa(0);
        setCdBaseDadosUtilizada(0);
        setCdTipoCargaBaseDadosUtilizada(0);
        setCdPermiteAnteciparRecadastramento(0);
        setDataLimiteInicioVinculoCargaBase(new Date());
        setDataInicioRecadastramento(new Date());
        setDataFimRecadastramento(new Date());
        setCdPermiteAcertosDados(0);
        setDataInicioPeriodoAcertoDados(new Date());
        setDataFimPeriodoAcertoDados(new Date());
        setCdEmiteMsgRecadastramentoMidiaOnline(0);
        setCdMidiaMsgRecadastramentoOnline(0);
        setDiaFechamentoApuracaoTarifaMensal("");
        setQtddDiasCobrancaTarifaApuracao("");
        setQtddMesesReajusteAutomaticoTarifa("");
        setCdIndiceEconomicoReajusteTarifa(0);
        setPercentualReajuste(null);
        setBonificacaoTarifa(null);
        setPercentualIndiceEconomicoReajusteTarifa(null);
        setPercentualBonificacaoTarifaPadrao(null);

        setRdoAbertContaBancoPostBradSeg("");
        setCboAcaoParaNaoComprovacao(0l);
        setDsAcaoParaNaoComprovacao(""); 
        setRdoAdesaoSacadoEletronico(""); 
        setCboAgendamentoDebitosPendentesVeiculos(0l); 
        setDsAgendamentoDebitosPendentesVeiculos(""); 
        setRdoAgendarTituloRastreadoFilial(""); 
        setRdoAgendarTituloRastProp("");
        setRdoAgruparCorresp("");
        setRdoAutComplementarAg("");
        setRdoBloqEmissaoPapeleta(""); 
        setCboCadUtilizadoRec(0l);
        setDsCadUtilizadoRec("");
        setCboCadConsultaEndEnvio(0l);
        setDsCadConsultaEndEnvio("");
        setRdoCapTitulosDtReg("");
        setCobrarTarifasFunc("");
        setCodigoFormulario(""); 
        setCboCondicaoEnquadramento(0l);

        setDsCondicaoEnquadramento("");
        setCboContExpCredConta(0l);
        setDsContExpCredConta("");
        setCboCritCompEnquadramento(0l);
        setDsCritCompEnquadramento("");
        setCboCritPrincEnquadramento(0l);
        setDsCritPrincEnquadramento(""); 
        setDtInicioBloqPapeleta(null);
        setDtInicioRastreamento(new Date());
        setDtFimPerAcertoDados(new Date());
        setDtFimRecadastramento(new Date());
        setDtInicioPerAcertoDados(new Date());
        setDtInicioRecadastramento(new Date());
        setDtLimiteEnqConvContaSal(new Date());
        setDtLimiteVincClienteBenef(new Date());
        setRdoDemonstraInfAreaRes("");
        setCboDestinoEnvio(0l);
        setDsDestinoEnvio(""); 
        setCboDestinoEnvioCorresp(0l); 
        setDsDestinoEnvioCorresp("");
        setRdoEfetuaConEspBenef(""); 
        setRdoEmissaoAntCartaoContSal("");
        setRdoEmiteAvitoCompVida("");
        setRdoEmiteMsgRecadMidia("");
        setRdoExigeLibLoteProcessado("");
        setCboFormaAutPagamentos(0l);
        setDsFormaAutPagamentos("");
        setCboFormaEnvioPagamento(0l);
        setDsFormaEnvioPagamento("");
        setCboFormaManCadFavorecido(0l);
        setDsFormaManCadFavorecido("");
        setCboFormaManCadProcurador(0l);
        setDsFormaManCadProcurador("");
        setFormularioImpressao("");
        setRdoGerarLancFutCred("");
        setRdoGerarLancFutDeb("");
        setRdoGerarLancProgramado("");
        setRdoUtilizaLancPersonalizado("");
        setRdoGerarRetornoOperRealizadaInternet(""); // Gerar Retorno de
        setRdoInformaAgenciaContaCred(""); // Informa Ag�ncia/Conta Cr�dito
        setCboMidiaDispCorrentista(0l);// M�dia de DIsponibiliza��o para
        setDsMidiaDispCorrentista("");// M�dia de DIsponibiliza��o para
        setCboMeioDisponibilizacaoCorrentista(0l);
        setDsMeioDisponibilizacaoCorrenstista("");
        setCboMeioDisponibilizacaoNaoCorrentista(0l);
        setDsMeioDisponibilizacaoNaoCorrenstista("");
        setCboMidiaMsgRecad(0l);// M�dia Mensagem Recadastramento
        setDsMidiaMsgRecad("");// M�dia Mensagem Recadastramento
        setCboMomentoEnvio(0l);// Momento de Envio
        setDsMomentoEnvio("");// Momento de Envio
        setCboMomentoIndCredEfetivado(0l);// Momento Indica��o de Cr�dito
        setDsMomentoIndCredEfetivado("");// Momento Indica��o de Cr�dito
        setCboOcorrenciaDebito(0l);// Ocorr�ncia de D�bito
        setDsOcorrenciaDebito("");// Ocorr�ncia de D�bito
        setPercMaxRegInconsistente(null);// Percentual M�ximo de Registros
        setCboPeriodicidadeEmissao(0);// Periocidade de Emiss�o
        setDsPeriodicidadeEmissao("");// Periocidade de Emiss�o
        setCboPeriocidadeManCadProc(0);// Periocidade Manuten��o Cadastro
        setDsPeriocidadeManCadProc("");// Periocidade Manuten��o Cadastro
        setCboPeriodicidadeEnvioRemessaManutencao(0); // Periodicidade Envio
        setDsPeriodicidadeEnvioRemessaManutencao(""); // Periodicidade Envio
        setCboPeriodicidadePesquisaDebitosPendentesVeiculos(0); // Periodicidade
        setDsPeriodicidadePesquisaDebitosPendentesVeiculos(""); // Periodicidade
        setRdoPermiteAcertosDados(""); // Permite Acertos de Dados- radio
        setRdoPermiteAnteciparRecadastramento(""); // Permite Antecipar
        setRdoPermiteDebitoOnline(""); // Permite D�bito Online - RADIO
        setRdoPermiteEnvioRemessaManutencaoCadastro(""); // Permite Envio de
        setCboPermiteEstornoPagamento(0l); // Permite Estorno de Pagamento -
        setDsPermiteEstornoPagamento(""); // Permite Estorno de Pagamento -
        setRdoPermiteFavorecidoConsultarPagamento(""); // Permite Favorecido
        setRdoPermitePagarMenor(""); // Permite Pagar Menor - RADIO
        setRdoPermitePagarVencido(""); // Permite Pagar Vencido - RADIO
        setRdoPermitirAgrupamentoCorrespondencia(""); // Permitir Agrupamento de
        setRdoPesquisaDe(""); // Pesquisa de - radio
        setRdoPossuiExpiracaoCredito(""); // Possui Expira��o Credito - radio
        setCboPrioridadeDebito(0l); // Prioridade de D�bito - COMBO
        setDsPrioridDebito(""); // Prioridade de D�bito - COMBO
        setCboProcessamentoEfetivacaoPagamento(0l); // Processamento Efetiva��o
        setDsProcessamentoEfetivacaoPagamento(""); // Processamento Efetiva��o
        setQtddDiasFloating(null); // Quantidade de Dias de Floating - INPUT
        setQtddDiasRepique(null);// Quantidade de Dias de Repique - INPUT
        setQtddDiasEmissaoAvisoAntesInicioComprovacao(null); // Quantidade de
        setQtddDiasEmissaoAvisoAntesVencimento(null); // Quantidade de Dias
        setQtddDiasExpiracaoCredito(null); // QUantidade de Dias Expira��o
        setQtddDiasAvisoAntecipado(null); // Quantidade de Dias para Aviso
        setQtddDiasEnvioAntecipadoFormulario(null); // Quantidade de Dias para
        setQuantidadeDiasExpiracao(null); // Quantidade de Dias para Expira��o -
        setQtddDiasInativacaoFavorecido(null); // Quantidade de Dias para
        setQtddFasesPorEtapa(null); // Quantidade de Fases por Etapa - input
        setQtddLinhasPorComprovante(null); // Quantidade de Linhas por
        setQuantidadeMesesEmissao(null); // Quantidade de Meses para EMiss�o-
        setQtddMesesPeriodicidadeComprovacao(null); // Quantidade de Meses
        setQtddMesesPorEtapa(null); // Quantidade de Meses por Etapa- input
        setQtddMesesPorFase(null); // Quantidade de Meses por Fase- input
        setQtddViasEmitir(null); // Quantidade de Vias a Emitir -input
        setQtddViasPagasRegistro(null); // Quantidade de Vias Pagas no Registro-
        setQuantidadeEtapasRecadastramento(null); // Quantidade Etapas
        setQtddLimiteCartaoContaSalarioSolicitacao(null); // Quantidade Limite
        setQuantidadeLimiteDiasPagamentoVencido(null); // Quantidade Limite de
        setQtddMaximaRegistrosInconsistentesLote(null);// Quantidade M�xima de
        setRdoPermiteCorrespondenciaAberta(""); // Permite Correspond�ncia
        setRdoRastrearNotasFiscais(""); // Rastrear Notas Fiscais - RADIO
        setRdoRastrearTituloTerceiros(""); // Rastrear Titulo de Terceiros -
        setTextoInformacaoAreaReservada(""); // Texto INforma��o �rea Reservada
        setCboTipoCartaoContaSalario(0l); // Tipo Cart�o Conta Sal�rio - combo
        setDsTipoCartaoContaSalario(""); // Tipo Cart�o Conta Sal�rio - combo
        setCboTipoConsistenciaCpfCnpjProprietario(0l); // Tipo Consistencia
        setDsTipoConsistenciaCpfCnpjProprietario(""); // Tipo Consistencia
        setCboTipoConsistenciaIdentificacaoBeneficiario(0l); // Tipo
        setDsTipoConsistenciaIdentificacaoBeneficiario(""); // Tipo Consist�ncia
        setCboTipoCargaCadastroBeneficiario(0l); // Tipo de Carga Cadastro de
        setDsTipoCargaCadastroBeneficiario(""); // Tipo de Carga Cadastro de
        setCboTipoConsistenciaInscricaoFavorecido(0l); // Tipo de Consist�ncia
        setDsTipoConsistenciaInscricaoFavorecido(""); // Tipo de Consist�ncia da
        setCboTipoConsistenciaCpfCnpjFavorecido(0l); // Tipo de Consistencia do
        setDsTipoConsistenciaCpfCnpjFavorecido(""); // Tipo de Consistencia do
        setCboTipoConsolidacaoPagamentosComprovante(0l); // Tipo de COnsolida��o
        setDsTipoConsolidacaoPagamentosComprovante(""); // Tipo de COnsolida��o
        setDsIndicadorUtilizaMora(null);
        setCboTipoConsultaSaldo(0l); // Tipo de Consulta de Saldo - COMBO
        setDsTipoConsSaldo(""); // Tipo de Consulta de Saldo - COMBO
        setCboTipoContaCredito(1l); // Tipo de Conta Cr�dito - COMBO
        setDsTipoContaCredito(""); // Tipo de Conta Cr�dito - COMBO
        setCboTipoDataControleFloating(0l); // Tipo de Data de Controle Floating
        setDsTipoDataControleFloating(""); // Tipo de Data de Controle Floating
        setCboTipoFormacaoListaDebito(0l); // Tipo de Forma��o de Lista de
        setDsTipoFormacaoListaDebito(""); // Tipo de Forma��o de Lista de D�bito
        setCboTipoInscricaoFavorecido(0l); // Tipo de Inscri��o do Favorecido -
        setDsTipoInscricaoFavorecido(""); // Tipo de Inscri��o do Favorecido -
        setCboTipoRastreamentoTitulo(0l); // Tipo de Rastreamento de Titulos -
        setDsTipoRastreamentoTitulo(""); // Tipo de Rastreamento de Titulos -
        setCboTipoRejeicaoLote(0l); // Tipo de Rejei��o do Lote - combo
        setDsTipoRejeicaoLote(""); // Tipo de Rejei��o do Lote - combo
        setCboTipoRejeicaoEfetivacao(0l); // Tipo de Rejei��o na Efetiva��o -
        setDsTipoRejEfetivacao(""); // Tipo de Rejei��o na Efetiva��o - COMBO
        setCboTipoRejeicaoAgendamento(0l); // Tipo de Rejei��o no Agendamento -
        setDsTipoRejAgendamento(""); // Tipo de Rejei��o no Agendamento - COMBO
        setCboTipoTratamentoContaTransferida(0l); // Tipo de Tratamento Conta
        setDsTipoTratamentoContaTransferida(""); // Tipo de Tratamento Conta
        setCboTipoIdentificacaoBeneficiario(0l); // Tipo Identitica��o
        setDsTipoIdentificacaoBeneficiario(""); // Tipo Identitica��o
        setCboTratamentoFeriadosDataPagamento(0l); // Tratamento de Feriados na
        setDsTratFeriadosDataPagamento(""); // Tratamento de Feriados na Data do
        setCboTratamentoFeriadoFimVigenciaCredito(0l); // Tratamento Feriado
        setDsTratamentoFeriadoFimVigenciaCredito(""); // Tratamento Feriado para
        setCboTratamentoListaDebitoSemNumeracao(0l); // Tratamento Lista de
        setDsTratamentoListaDebitoSemNumeracao(""); // Tratamento Lista de
        setCboTratamentoValorDivergente(0l); // Tratamento Valor Divergente -
        setDsTratamentoValorDivergente(""); // Tratamento Valor Divergente -
        setRdoUtilizaCadastroFavorecidoControlePagamentos(""); // Utiliza
        setRdoUtilizaCadastroOrgaosPagadores(""); // Utiliza Cadastro Orgaos
        setRdoUtilizaCadastroProcuradores(""); // Utiliza Cadastro Procuradores-
        setRdoUtilizaFrasesPreCadastradas(""); // Utiliza Frases
        setRdoUtilizaListaDebitos(""); // Utiliza Lista de D�bito - radio
        setRdoUtilizaMora("");
        setRdoUtilizaMensagemPersonalizada(""); // Utiliza Mensagem
        setRdoValidarNomeFavorecidoReceitaFederal(""); // Validar Nome do
        setValorLimiteDiario(null); // Valor Limite Di�rio - INPUT
        setValorLimiteIndividual(null); // Valor Limite Individual - INPUT
        setValorMaximoPagamentoFavorecidoNaoCadastrado(null); // Valor M�ximo
        setRdoIndicadorSegundaLinhaExtrato(null); // Demonstra 2.Linha de Extrato
        setRdoAgendamentoDebitosPendentesVeiculos("");
        setDesabilitaTipoFormListaDebito(true);
        setDesabilitaTratamentoListaDebitoSemNumeracao(true);
        setDesabilitaPeriodicidadePesquisaDebPendVeiculos(true);
        setDesabilitaTpCartaoQtdeLimite(false);
        setDesabilitaTipoFormListaDebito(true);
        setDesabilitaTratamentoListaDebitoSemNumeracao(true);
        setRdoTipoFormatacaoPrimeiraLinha("");
        setDsPreenchimentoLancamentoPersonalizado("");
        setRdoIndicadorDdaRetorno("");
        setDsTituloDdaRetorno("");
        setRdoPermiteAgAutGrade("");
        setDsIndicadorAgendaGrade("");
      
        setDsOrigem(null);
        setDsOrigemIndicador(0);
        
        setComboRetornoOperacoesSelecionado(0l);
        setVlPercentualDiferencaTolerada(null);
     // Consist�ncia do CPF/CNPJ do Benefici�rio/Sacador Avalista � NPC - COMBO
        setCboConsistenciaCpfCnpjBenefAvalNpc(0l); 
        setDsConsistenciaCpfCnpjBenefAvalNpc("");        
    }
    
    
    /**     * Salvar configuracao.
     * 
     * @return the string
     */
    public String salvarConfiguracao() {
        listaModalidadeTipoServicoSaidaDTO = new ListarModalidadeTipoServicoSaidaDTO();

        listaModalidadeTipoServicoSaidaDTO = getListaGridPesquisa().get(
            getItemSelecionadoLista());
        
        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 1) {
            setDsFormaEnvioPagamento(listaFormaEnvioPagamentoHash.get(getCboFormaEnvioPagamento()));
            setDsFormaAutPagamentos(listaFormaAutorizacaoPagamentosHash.get(getCboFormaAutPagamentos()));
            setDsTipoDataControleFloating(listaTipoDataControleFloatingHash.get(getCboTipoDataControleFloating()));
            setDsTipoFormacaoListaDebito(listaTipoFormacaoListaDebitoHash.get(getCboTipoFormacaoListaDebito()));
            setDsTratamentoListaDebitoSemNumeracao(listaTratamentoListaDebitoNumeracaoHash.get(getCboTratamentoListaDebitoSemNumeracao()));
            setDsTipoConsolidacaoPagamentosComprovante(listaTipoConsolidacaoPagamentosComprovanteHash.get(getCboTipoConsolidacaoPagamentosComprovante()));
            setDsRetornoOperacoes(SelectItemUtils.obterLabelSelecItem(getListaRetornoOperacoes(), getComboRetornoOperacoesSelecionado()));
            setDsCodIndicadorTipoRetornoInternet(SelectItemUtils.obterLabelSelecItem(getListaCodIndicadorTipoRetornoInternet(), getComboCodIndicadorTipoRetornoInternet()));

        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 2) {
            setDsFormaEnvioPagamento(listaFormaEnvioPagamentoHash.get(getCboFormaEnvioPagamento()));
            setDsFormaAutPagamentos(listaFormaAutorizacaoPagamentosHash.get(getCboFormaAutPagamentos()));
            setDsTipoDataControleFloating(listaTipoDataControleFloatingHash.get(getCboTipoDataControleFloating()));
            setDsTipoFormacaoListaDebito(listaTipoFormacaoListaDebitoHash.get(getCboTipoFormacaoListaDebito()));
            setDsTratamentoListaDebitoSemNumeracao(listaTratamentoListaDebitoNumeracaoHash.get(getCboTratamentoListaDebitoSemNumeracao()));
            setDsTipoConsolidacaoPagamentosComprovante(listaTipoConsolidacaoPagamentosComprovanteHash.get(getCboTipoConsolidacaoPagamentosComprovante()));
            setDsTipoCartaoContaSalario(listaTipoCartaoContaSalarioHash.get(getCboTipoCartaoContaSalario()));
            setDsTipoConsolidacaoPagamentosComprovante(listaTipoConsolidacaoPagamentosComprovanteHash.get(getCboTipoConsolidacaoPagamentosComprovante()));
            setDsRetornoOperacoes(SelectItemUtils.obterLabelSelecItem(getListaRetornoOperacoes(), getComboRetornoOperacoesSelecionado()));
            setDsCodIndicadorTipoRetornoInternet(SelectItemUtils.obterLabelSelecItem(getListaCodIndicadorTipoRetornoInternet(), getComboCodIndicadorTipoRetornoInternet()));
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 3) {
            setDsFormaEnvioPagamento(listaFormaEnvioPagamentoHash.get(getCboFormaEnvioPagamento())); //OK
            setDsFormaAutPagamentos(listaFormaAutorizacaoPagamentosHash.get(getCboFormaAutPagamentos()));
            setDsTipoDataControleFloating(listaTipoDataControleFloatingHash.get(getCboTipoDataControleFloating()));
            setDsTipoFormacaoListaDebito(listaTipoFormacaoListaDebitoHash.get(getCboTipoFormacaoListaDebito())); // Disable
            setDsTratamentoListaDebitoSemNumeracao(listaTratamentoListaDebitoNumeracaoHash.get(getCboTratamentoListaDebitoSemNumeracao())); // Disable
            setDsTipoConsolidacaoPagamentosComprovante(listaTipoConsolidacaoPagamentosComprovanteHash.get(getCboTipoConsolidacaoPagamentosComprovante()));
            setDsTipoCartaoContaSalario(listaTipoCartaoContaSalarioHash.get(getCboTipoCartaoContaSalario()));
            setDsTipoConsolidacaoPagamentosComprovante(listaTipoConsolidacaoPagamentosComprovanteHash.get(getCboTipoConsolidacaoPagamentosComprovante()));
            setDsPeriodicidadePesquisaDebitosPendentesVeiculos(listaPeriodicidadeHash.get(getCboPeriodicidadePesquisaDebitosPendentesVeiculos()));
            setDsRetornoOperacoes(SelectItemUtils.obterLabelSelecItem(getListaRetornoOperacoes(), getComboRetornoOperacoesSelecionado()));
            setDsCodIndicadorTipoRetornoInternet(SelectItemUtils.obterLabelSelecItem(getListaCodIndicadorTipoRetornoInternet(), getComboCodIndicadorTipoRetornoInternet()));
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 4) {
            setDsFormaEnvioPagamento(listaFormaEnvioPagamentoHash
                .get(getCboFormaEnvioPagamento()));
            setDsFormaAutPagamentos(listaFormaAutorizacaoPagamentosHash
                .get(getCboFormaAutPagamentos()));
            setDsTipoDataControleFloating(listaTipoDataControleFloatingHash
                .get(getCboTipoDataControleFloating()));
            setDsTipoFormacaoListaDebito(listaTipoFormacaoListaDebitoHash
                .get(getCboTipoFormacaoListaDebito()));
            setDsTratamentoListaDebitoSemNumeracao(listaTratamentoListaDebitoNumeracaoHash
                .get(getCboTratamentoListaDebitoSemNumeracao()));
            setDsTipoConsolidacaoPagamentosComprovante(listaTipoConsolidacaoPagamentosComprovanteHash
                .get(getCboTipoConsolidacaoPagamentosComprovante()));
            setDsTipoCartaoContaSalario(listaTipoCartaoContaSalarioHash
                .get(getCboTipoCartaoContaSalario()));
            setDsTipoConsolidacaoPagamentosComprovante(listaTipoConsolidacaoPagamentosComprovanteHash
                .get(getCboTipoConsolidacaoPagamentosComprovante()));
            setDsTratamentoFeriadoFimVigenciaCredito(listaTratamentoFeriadoFimVigenciaCreditoHash
                .get(getCboTratamentoFeriadoFimVigenciaCredito()));
            setDsMomentoIndCredEfetivado(listaMomentoIndicacaoCreditoEfetivadoHash
                .get(getCboMomentoIndCredEfetivado()));
            setDsContExpCredConta(listaControleExpiracaoCreditoHash
                .get(getCboContExpCredConta()));
            setDsPeriocidadeManCadProc(listaPeriodicidadeHash
                .get(getCboPeriocidadeManCadProc()));
            setDsFormaManCadProcurador(listaFormaManutencaoCadastroProcuradoresHash
                .get(getCboFormaManCadProcurador()));
            setDsTipoIdentificacaoBeneficiario(listaTipoIdentificacaoBeneficiarioHash
                .get(getCboTipoIdentificacaoBeneficiario()));
            setDsRetornoOperacoes(SelectItemUtils.obterLabelSelecItem(getListaRetornoOperacoes(), getComboRetornoOperacoesSelecionado()));
            setDsCodIndicadorTipoRetornoInternet(SelectItemUtils.obterLabelSelecItem(getListaCodIndicadorTipoRetornoInternet(), getComboCodIndicadorTipoRetornoInternet()));
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 3) {
            setDsPeriodicidadeRastreamento(getListaPeriodicidadeHash().get(
                getCdPeriodicidadeRastreamento()));
            setDsPeriodicidadeCobranca(getListaPeriodicidadeHash().get(
                getCdPeriodicidadeCobranca()));
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 4) {
            setDsPeriodicidadeManutencao(getListaPeriodicidadeHash().get(
                getCdPeriodicidadeManutencao()));
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 5) {
            setDsFormaManCadFavorecido(listaFormaManutencaoCadastroFavorecidoHash
                .get(getCboFormaManCadFavorecido()));
            setDsTipoConsistenciaInscricaoFavorecido(listaTipoConsistenciaInscricaoFavorecidoHash
                .get(getCboTipoConsistenciaInscricaoFavorecido()));
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() >= 6
                        && listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() <= 11) {
            setDsPeriodicidade(getListaPeriodicidadeHash().get(
                getCdPeriodicidade()));

            setDsPeriodicidadeEmissao(getListaPeriodicidadeHash().get(
                getCboPeriodicidadeEmissao()));
            setDsDestinoEnvio(getListaDestinoEnvioHash().get(
                getCboDestinoEnvio()));
            setDsCadConsultaEndEnvio(getListaCadastroConsultaEnderecoEnvioHash()
                .get(getCboCadConsultaEndEnvio()));
        }

        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 12
                        || listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 13) {
            setDsTipoRejeicaoLote(getListaTipoRejeicaoLoteHash().get(
                getCboTipoRejeicaoLote()));
            setDsMeioDisponibilizacaoCorrenstista(getListaMeioDisponibilizacaoCorrentistasHash()
                .get(getCboMeioDisponibilizacaoCorrentista()));
            setDsMidiaDispCorrentista(getListaMidiaDisponibilizacaoCorrentistasHash()
                .get(getCboMidiaDispCorrentista()));
            setDsMeioDisponibilizacaoNaoCorrenstista(getListaMeioDisponibilizacaoNaoCorrentistasHash()
                .get(getCboMeioDisponibilizacaoNaoCorrentista()));
            setDsDestinoEnvioCorresp(getListaDestinoEnvioCorrespondenciaHash()
                .get(getCboDestinoEnvioCorresp()));
            setDsCadConsultaEndEnvio(getListaCadastroConsultaEnderecoEnvioHash()
                .get(getCboCadConsultaEndEnvio()));
        }
        if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 14) {
            setDsTipoIdentificacaoBeneficiario(getListaTipoIdentificacaoBeneficiarioHash()
                .get(getCboTipoIdentificacaoBeneficiario()));
            setDsTipoConsistenciaIdentificacaoBeneficiario(getListaTipoConsistenciaIdentificacaoBeneficiarioHash()
                .get(getCboTipoConsistenciaIdentificacaoBeneficiario()));
            setDsCondicaoEnquadramento(getListaCondicaoEnquadramentoHash().get(
                getCboCondicaoEnquadramento()));
            setDsCritPrincEnquadramento(getListaCriterioPrincipalEnquadramentoHash()
                .get(getCboCritPrincEnquadramento()));
            setDsCritCompEnquadramento(getListaCriterioCompostoEnquadramentoHash()
                .get(getCboCritCompEnquadramento()));
            setDsCadUtilizadoRec(getListaCadastroUtilizadoRecadastramentoHash()
                .get(getCboCadUtilizadoRec()));
            setDsPeriodicidadeEnvioRemessaManutencao(getListaPeriodicidadeHash()
                .get(getCboPeriodicidadeEnvioRemessaManutencao()));
            setDsTipoCargaCadastroBeneficiario(getListaTipoCargaCadastroBeneficiariosHash()
                .get(getCboTipoCargaCadastroBeneficiario()));
            setDsMidiaMsgRecad(getListaMidiaMensagemRecadastramentoHash().get(
                getCboMidiaMsgRecad()));
        }

        if (getRdtoCadastroFloating() != null && getRdtoCadastroFloating() == 1) {
            setDescRdtoCadastroFloating("Sim");
        } else {
            setDescRdtoCadastroFloating("N�o");
        }

        return "SALVAR_CONFIGURACAO";
    }

    /**
     * Confirmar configuracao.
     * 
     * @return the string
     */
    public String confirmarConfiguracao() {
        try {
            listaModalidadeTipoServicoSaidaDTO = new ListarModalidadeTipoServicoSaidaDTO();

            AlterarConfiguracaoTipoServModContratoEntradaDTO entradaDTO = 
                new AlterarConfiguracaoTipoServModContratoEntradaDTO();

            listaModalidadeTipoServicoSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());

            entradaDTO.setCdFloatServicoContrato(PgitUtil.verificaIntegerNulo(getRdtoCadastroFloating()));
            entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
            entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato());
            entradaDTO.setNrSequenciaContratoNegocio(getNrSequenciaContrato());
            entradaDTO.setCdTipoServico(getCdTipoServico());
            entradaDTO.setCdModalidade(getCdModalidade());
            entradaDTO.setCdParametro(1);
            entradaDTO.setCdParametroTela(listaModalidadeTipoServicoSaidaDTO.getCdParametroTela());
            entradaDTO.setQtDiaUtilPgto(getQtdDiasReutilizacaoControlePgto());


            /**************************************** TIPO DE SERVI�O *******************************************/

            if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 1) {

                entradaDTO.setCdFormaEnvioPagamento(Integer.valueOf(String.valueOf(
                    PgitUtil.verificaLongNulo(getCboFormaEnvioPagamento()))));
                
                entradaDTO.setCdFormaAutorizacaoPagamento(0);
                
                entradaDTO.setCdIndicadorAutorizacaoComplemento(getRdoAutComplementarAg() != null
                    && !getRdoAutComplementarAg().equals("") ? Integer.valueOf(getRdoAutComplementarAg()) : 0);
                entradaDTO.setCdTipoDataFloat(Integer.valueOf(String.valueOf(
                    PgitUtil.verificaLongNulo(getCboTipoDataControleFloating()))));
                entradaDTO.setCdIndicadorListaDebito(getRdoUtilizaListaDebitos() != null
                    && !getRdoUtilizaListaDebitos().equals("") ? Integer.valueOf(getRdoUtilizaListaDebitos()): 0);
                entradaDTO.setCdTipoFormacaoLista(Integer.valueOf(String.valueOf(
                    PgitUtil.verificaLongNulo(getCboTipoFormacaoListaDebito()))));
                entradaDTO.setCdTipoConsistenciaLista(Integer.valueOf(String.valueOf(
                    PgitUtil.verificaLongNulo(getCboTratamentoListaDebitoSemNumeracao()))));

                if (getComboRetornoOperacoesSelecionado() != null && getComboRetornoOperacoesSelecionado() != 0l) {
                    entradaDTO.setCdIndicadorRetornoInternet(Integer.parseInt(getComboRetornoOperacoesSelecionado().toString()));
                } else {
                    entradaDTO.setCdIndicadorRetornoInternet(0);
                }

                if (getRdoGerarRetornoSeparadoCanal() != null && !getRdoGerarRetornoSeparadoCanal().equals("")) {
                    entradaDTO.setCdIndicadorSeparaCanal(getRdoGerarRetornoSeparadoCanal().equals("3") ? 1 : 2);
                } else {
                    entradaDTO.setCdIndicadorSeparaCanal(0);
                }

                entradaDTO.setCdTipoConsultaComprovante(Integer.valueOf(String.valueOf(
                    PgitUtil.verificaLongNulo(getCboTipoConsolidacaoPagamentosComprovante()))));
                entradaDTO.setCdIndicadorTipoRetornoInternet(Integer.valueOf(String.valueOf(PgitUtil.verificaLongNulo(getComboCodIndicadorTipoRetornoInternet()))));
            }

            if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 2) {

                entradaDTO.setCdFormaEnvioPagamento(Integer.valueOf(String.valueOf(
                    PgitUtil.verificaLongNulo(getCboFormaEnvioPagamento()))));

                entradaDTO.setCdFormaAutorizacaoPagamento(0);                
                
                entradaDTO.setCdIndicadorAutorizacaoComplemento(getRdoAutComplementarAg() != null
                    && !getRdoAutComplementarAg().equals("") ? Integer.valueOf(getRdoAutComplementarAg()) : 0);
                entradaDTO.setCdTipoDataFloat(Integer.valueOf(String.valueOf(
                    PgitUtil.verificaLongNulo(getCboTipoDataControleFloating()))));
                entradaDTO.setCdIndicadorListaDebito(getRdoUtilizaListaDebitos() != null
                    && !getRdoUtilizaListaDebitos().equals("") ? Integer.valueOf(getRdoUtilizaListaDebitos()) : 0);
                entradaDTO.setCdTipoFormacaoLista(Integer.valueOf(String.valueOf(PgitUtil.verificaLongNulo(
                    getCboTipoFormacaoListaDebito()))));
                entradaDTO.setCdTipoConsistenciaLista(Integer.valueOf(String.valueOf(PgitUtil.verificaLongNulo(
                    getCboTratamentoListaDebitoSemNumeracao()))));

                if (getComboRetornoOperacoesSelecionado() != null && getComboRetornoOperacoesSelecionado() != 0l) {
                    entradaDTO.setCdIndicadorRetornoInternet(Integer.parseInt(getComboRetornoOperacoesSelecionado().toString()));
                } else {
                    entradaDTO.setCdIndicadorRetornoInternet(0);
                }

                if (getRdoGerarRetornoSeparadoCanal() != null && !getRdoGerarRetornoSeparadoCanal().equals("")) {
                    entradaDTO.setCdIndicadorSeparaCanal(getRdoGerarRetornoSeparadoCanal().equals("3") ? 1 : 2);
                } else {
                    entradaDTO.setCdIndicadorSeparaCanal(0);
                }

                entradaDTO
                .setCdTipoConsultaComprovante(Integer
                    .valueOf(String
                        .valueOf(PgitUtil
                            .verificaLongNulo(getCboTipoConsolidacaoPagamentosComprovante()))));
                entradaDTO.setDtEnquaContaSalario(FormatarData
                    .formataDiaMesAnoToPdc(getDtLimiteEnqConvContaSal()));
                entradaDTO
                .setQtLimiteSolicitacaoCatao(getQtddLimiteCartaoContaSalarioSolicitacao());
                entradaDTO
                .setCdTipoCataoSalario(Integer
                    .valueOf(String
                        .valueOf(PgitUtil
                            .verificaLongNulo(getCboTipoCartaoContaSalario()))));
                entradaDTO
                .setCdIndicadorCataoSalario(getRdoEmissaoAntCartaoContSal() != null
                    && !getRdoEmissaoAntCartaoContSal().equals("")
                    ? Integer
                        .valueOf(String
                            .valueOf(getRdoEmissaoAntCartaoContSal()))
                            : 0);
                entradaDTO
                .setCdIndicadorBancoPostal(getRdoAbertContaBancoPostBradSeg() != null
                    && !getRdoAbertContaBancoPostBradSeg().equals(
                    "")
                    ? Integer
                        .valueOf(String
                            .valueOf(getRdoAbertContaBancoPostBradSeg()))
                            : 0);
                entradaDTO.setCdIndicadorTipoRetornoInternet(Integer.valueOf(String.valueOf(PgitUtil.verificaLongNulo(getComboCodIndicadorTipoRetornoInternet()))));

            }

            if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 3) {
                entradaDTO.setCdFormaEnvioPagamento(Integer
                    .parseInt(getCboFormaEnvioPagamento().toString()));
                
                entradaDTO.setCdFormaAutorizacaoPagamento(0);
                
                entradaDTO.setCdIndicadorAutorizacaoComplemento(Integer
                    .valueOf(getRdoAutComplementarAg()));
                if (getComboRetornoOperacoesSelecionado() != null && getComboRetornoOperacoesSelecionado() != 0l) {
                    entradaDTO.setCdIndicadorRetornoInternet(Integer.parseInt(getComboRetornoOperacoesSelecionado().toString()));
                } else {
                    entradaDTO.setCdIndicadorRetornoInternet(0);
                }

                if (getRdoGerarRetornoSeparadoCanal() != null
                                && !getRdoGerarRetornoSeparadoCanal().equals("")) {
                    entradaDTO
                    .setCdIndicadorSeparaCanal(getRdoGerarRetornoSeparadoCanal()
                        .equals("3") ? 1 : 2);
                } else {
                    entradaDTO.setCdIndicadorSeparaCanal(0);
                }
                entradaDTO.setCdTipoDataFloat(Integer
                    .parseInt(getCboTipoDataControleFloating().toString()));
                entradaDTO.setCdIndicadorListaDebito(Integer
                    .valueOf(getRdoUtilizaListaDebitos()));
                entradaDTO.setCdIndicadorUtilizaMora(Integer
                    .valueOf(getRdoUtilizaMora()));
                entradaDTO.setCdTipoFormacaoLista(Integer
                    .parseInt(getCboTipoFormacaoListaDebito().toString()));
                entradaDTO.setCdTipoConsistenciaLista(Integer
                    .parseInt(getCboTratamentoListaDebitoSemNumeracao()
                        .toString()));
                entradaDTO.setCdTipoConsultaComprovante(Integer
                    .parseInt(getCboTipoConsolidacaoPagamentosComprovante()
                        .toString()));
                entradaDTO.setCdConsDebitoVeiculo(Integer
                    .valueOf(getRdoPesquisaDe()));
                entradaDTO
                .setCdPerdcConsultaVeiculo(Integer
                    .parseInt(getCboPeriodicidadePesquisaDebitosPendentesVeiculos()
                        .toString()));
                entradaDTO.setCdAgendaDebitoVeiculo(Integer
                    .valueOf(getRdoAgendamentoDebitosPendentesVeiculos()));
                entradaDTO.setCdIndicadorTipoRetornoInternet(Integer.valueOf(String.valueOf(PgitUtil.verificaLongNulo(getComboCodIndicadorTipoRetornoInternet()))));

            }

            if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 4) {

                entradaDTO
                .setCdFormaEnvioPagamento(Integer
                    .valueOf(String
                        .valueOf(PgitUtil
                            .verificaLongNulo(getCboFormaEnvioPagamento()))));
                
                entradaDTO
                .setCdFormaAutorizacaoPagamento(0);
                
                entradaDTO
                .setCdIndicadorAutorizacaoComplemento(getRdoAutComplementarAg() != null
                    && !getRdoAutComplementarAg().equals("")
                    ? Integer.valueOf(getRdoAutComplementarAg())
                        : 0);
                entradaDTO
                .setCdTipoDataFloat(Integer
                    .valueOf(String
                        .valueOf(PgitUtil
                            .verificaLongNulo(getCboTipoDataControleFloating()))));
                entradaDTO
                .setCdIndicadorListaDebito(getRdoUtilizaListaDebitos() != null
                    && !getRdoUtilizaListaDebitos().equals("")
                    ? Integer.valueOf(getRdoUtilizaListaDebitos())
                        : 0);
                entradaDTO
                .setCdTipoFormacaoLista(Integer
                    .valueOf(String
                        .valueOf(PgitUtil
                            .verificaLongNulo(getCboTipoFormacaoListaDebito()))));
                entradaDTO
                .setCdTipoConsistenciaLista(Integer
                    .valueOf(String
                        .valueOf(PgitUtil
                            .verificaLongNulo(getCboTratamentoListaDebitoSemNumeracao()))));
                
                if (getComboRetornoOperacoesSelecionado() != null && getComboRetornoOperacoesSelecionado() != 0l) {
                    entradaDTO.setCdIndicadorRetornoInternet(Integer.parseInt(getComboRetornoOperacoesSelecionado().toString()));
                } else {
                    entradaDTO.setCdIndicadorRetornoInternet(0);
                }

                if (getRdoGerarRetornoSeparadoCanal() != null
                                && !getRdoGerarRetornoSeparadoCanal().equals("")) {
                    entradaDTO
                    .setCdIndicadorSeparaCanal(getRdoGerarRetornoSeparadoCanal()
                        .equals("3") ? 1 : 2);
                } else {
                    entradaDTO.setCdIndicadorSeparaCanal(0);
                }
                entradaDTO
                .setCdTipoConsultaComprovante(Integer
                    .valueOf(String
                        .valueOf(PgitUtil
                            .verificaLongNulo(getCboTipoConsolidacaoPagamentosComprovante()))));
                entradaDTO
                .setCdTipoIdBeneficio(Integer
                    .valueOf(String
                        .valueOf(PgitUtil
                            .verificaLongNulo(getCboTipoIdentificacaoBeneficiario()))));
                entradaDTO
                .setCdIndicadorCadastroOrg(getRdoUtilizaCadastroOrgaosPagadores() != null
                    && !getRdoUtilizaCadastroOrgaosPagadores()
                    .equals("")
                    ? Integer
                        .valueOf(getRdoUtilizaCadastroOrgaosPagadores())
                        : 0);
                entradaDTO
                .setCdIndicadorCadastroProcd(getRdoUtilizaCadastroProcuradores() != null
                    && !getRdoUtilizaCadastroProcuradores().equals(
                    "")
                    ? Integer
                        .valueOf(getRdoUtilizaCadastroProcuradores())
                        : 0);
                entradaDTO
                .setCdFormaManutencao(Integer
                    .valueOf(String
                        .valueOf(PgitUtil
                            .verificaLongNulo(getCboFormaManCadProcurador()))));
                entradaDTO
                .setCdPerdcManutencaoProcd(getCboPeriocidadeManCadProc());
                entradaDTO
                .setCdDispzContaCredito(getRdoInformaAgenciaContaCred() != null
                    && !getRdoInformaAgenciaContaCred().equals("")
                    ? Integer
                        .valueOf(getRdoInformaAgenciaContaCred())
                        : 0);
                entradaDTO
                .setCdIndicadorExpiracaoCredito(getRdoPossuiExpiracaoCredito() != null
                    && !getRdoPossuiExpiracaoCredito().equals("")
                    ? Integer
                        .valueOf(getRdoPossuiExpiracaoCredito())
                        : 0);
                entradaDTO.setCdFormaExpiracaoCredito(Integer.valueOf(String
                    .valueOf(PgitUtil
                        .verificaLongNulo(getCboContExpCredConta()))));
                entradaDTO
                .setQtDiaExpiracao(getQtddDiasExpiracaoCredito() != null
                    && !getQtddDiasExpiracaoCredito().equals("")
                    ? Integer
                        .valueOf(getQtddDiasExpiracaoCredito())
                        : 0);
                entradaDTO
                .setCdMomentoCreditoEfetivacao(Integer
                    .valueOf(String
                        .valueOf(PgitUtil
                            .verificaLongNulo(getCboMomentoIndCredEfetivado()))));
                entradaDTO
                .setCdCreditoNaoUtilizado(Integer
                    .valueOf(String
                        .valueOf(PgitUtil
                            .verificaLongNulo(getCboTratamentoFeriadoFimVigenciaCredito()))));
            }

            if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 5) {

                entradaDTO.setCdFormaManutencao(Integer
                    .parseInt(getCboFormaManCadFavorecido().toString()));
                entradaDTO
                .setQtDiaInatividadeFavorecido(getQtddDiasInativacaoFavorecido());
                entradaDTO.setCdCtciaInscricaoFavorecido(Integer
                    .parseInt(getCboTipoConsistenciaInscricaoFavorecido()
                        .toString()));
                if (getComboRetornoOperacoesSelecionado() != null && getComboRetornoOperacoesSelecionado() != 0l) {
                    entradaDTO.setCdIndicadorRetornoInternet(Integer.parseInt(getComboRetornoOperacoesSelecionado().toString()));
                } else {
                    entradaDTO.setCdIndicadorRetornoInternet(0);
                }
                
                

            }

            if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() >= 6
                            && listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() <= 9) {
                entradaDTO.setCdPeriodicidadeAviso(Integer.valueOf(String
                    .valueOf(getCboPeriodicidadeEmissao())));
                entradaDTO.setQtViaAviso(getQtddViasEmitir());
                entradaDTO.setCdDestinoAviso(Integer.valueOf(String
                    .valueOf(getCboDestinoEnvio())));
                entradaDTO.setCdConsEndereco(Integer.valueOf(String
                    .valueOf(getCboCadConsultaEndEnvio())));
                entradaDTO.setCdEnvelopeAberto(2);
                entradaDTO.setCdAgrupamentoAviso(2);
                entradaDTO
                .setDsAreaReservada(getTextoInformacaoAreaReservada());
                entradaDTO.setCdAreaReservada(Integer
                    .valueOf(getRdoDemonstraInfAreaRes()));
            }

            if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 10
                            || listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 11) {
                entradaDTO.setCdPerdcComprovante(Integer.valueOf(String
                    .valueOf(getCboPeriodicidadeEmissao())));
                entradaDTO.setQtViaComprovante(getQtddViasEmitir());
                entradaDTO.setCdDestinoComprovante(Integer.valueOf(String
                    .valueOf(getCboDestinoEnvio() == null
                        ? 0
                            : getCboDestinoEnvio())));
                entradaDTO.setCdConsEndereco(Integer.valueOf(String
                    .valueOf(getCboCadConsultaEndEnvio())));
                entradaDTO.setCdEnvelopeAberto(2);
                entradaDTO.setCdAgrupamentoComprovado(2);
                entradaDTO
                .setDsAreaReservada(getTextoInformacaoAreaReservada());
                entradaDTO
                .setCdAreaReservada(getRdoDemonstraInfAreaRes() == null
                    ? 0
                        : Integer.valueOf(getRdoDemonstraInfAreaRes()));
            }

            if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 12
                            || listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 13) {

                entradaDTO.setCdRejeicaoLote(Integer
                    .parseInt(getCboTipoRejeicaoLote().toString()));
                entradaDTO.setCdDIspzSalarioCrrtt(Integer
                    .parseInt(getCboMeioDisponibilizacaoCorrentista()
                        .toString()));
                entradaDTO.setCdMidiaDisponivel(Integer
                    .parseInt(getCboMidiaDispCorrentista().toString()));
                entradaDTO.setCdDispzSalarioNao(Integer
                    .parseInt(getCboMeioDisponibilizacaoNaoCorrentista()
                        .toString()));
                entradaDTO.setCdDestinoComprovante(Integer
                    .parseInt(getCboDestinoEnvioCorresp().toString()));
                entradaDTO.setCdConsEndereco(Integer
                    .parseInt(getCboCadConsultaEndEnvio().toString()));
                entradaDTO.setCdFrasePreCadastro(Integer
                    .parseInt(getRdoUtilizaFrasesPreCadastradas()));
                entradaDTO.setCdCobrancaTarifa(Integer
                    .parseInt(getCobrarTarifasFunc()));
                entradaDTO.setQtMesComprovante(getQuantidadeMesesEmissao());
                entradaDTO.setQtViaComprovante(getQtddViasEmitir());
                entradaDTO.setQtViaCobranca(getQtddViasPagasRegistro());
                entradaDTO.setQtLimiteLinha(getQtddLinhasPorComprovante());
                entradaDTO.setCdFormularioContratoCliente(Integer
                    .parseInt(getFormularioImpressao()));
                if (getComboRetornoOperacoesSelecionado() != null && getComboRetornoOperacoesSelecionado() != 0l) {
                    entradaDTO.setCdIndicadorRetornoInternet(Integer.parseInt(getComboRetornoOperacoesSelecionado().toString()));
                } else {
                    entradaDTO.setCdIndicadorRetornoInternet(0);
                }

            }

            if (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela() == 14) {

                entradaDTO.setCdTipoIdBeneficio(Integer
                    .parseInt(getCboTipoIdentificacaoBeneficiario()
                        .toString()));
                entradaDTO
                .setCdCtciaIdentificacaoBeneficio(Integer
                    .parseInt(getCboTipoConsistenciaIdentificacaoBeneficiario()
                        .toString()));
                entradaDTO.setCdCriterioEnquaRecadastro(Integer
                    .parseInt(getCboCondicaoEnquadramento().toString()));
                entradaDTO.setCdPrincipalEnquaRecadastro(Integer
                    .parseInt(getCboCritPrincEnquadramento().toString()));
                entradaDTO.setCdCriterioEnquaBeneficio(Integer
                    .parseInt(getCboCritCompEnquadramento().toString()));
                entradaDTO
                .setQtEtapasRecadastroBeneficio(getQuantidadeEtapasRecadastramento());
                entradaDTO.setQtFaseRecadastroBeneficio(getQtddFasesPorEtapa());
                entradaDTO.setQtMesEtapaRecadastro(getQtddMesesPorEtapa());
                entradaDTO.setQtMesFaseRecadastro(getQtddMesesPorFase());
                entradaDTO.setCdBaseRecadastroBeneficio(Integer
                    .parseInt(getCboCadUtilizadoRec().toString()));
                entradaDTO.setCdCriterioEnquaBeneficio(Integer
                    .parseInt(getCboCritCompEnquadramento().toString()));
                entradaDTO.setDtInicioRecadastroBeneficio(FormatarData
                    .formataDiaMesAnoToPdc(getDtInicioRecadastramento()));
                entradaDTO.setDtFimRecadastroBeneficio(FormatarData
                    .formataDiaMesAnoToPdc(getDtFimRecadastramento()));
                entradaDTO
                .setCdManutencaoBaseRecadastro(Integer
                    .parseInt(getRdoPermiteEnvioRemessaManutencaoCadastro()));
                entradaDTO.setCdPerdcEnvioRemessa(Integer
                    .parseInt(getCboPeriodicidadeEnvioRemessaManutencao()
                        .toString()));
                entradaDTO.setCdTipoCargaRecadastro(Integer
                    .parseInt(getCboTipoCargaCadastroBeneficiario()
                        .toString()));
                entradaDTO.setDtInicioRastreabilidadeTitulo(FormatarData
                    .formataDiaMesAnoToPdc(getDtLimiteVincClienteBenef()));
                entradaDTO.setCdAcertoDadoRecadastro(Integer
                    .parseInt(getRdoPermiteAcertosDados()));
                entradaDTO.setCdAntecRecadastroBeneficio(Integer
                    .parseInt(getRdoPermiteAnteciparRecadastramento()));
                entradaDTO.setDtInicioAcertoRecadastro(FormatarData
                    .formataDiaMesAnoToPdc(getDtInicioPerAcertoDados()));
                entradaDTO.setDtFimAcertoRecadastro(FormatarData
                    .formataDiaMesAnoToPdc(getDtFimPerAcertoDados()));
                entradaDTO.setDtLimiteVinculoCarga(FormatarData
                    .formataDiaMesAnoToPdc(getDtLimiteVincClienteBenef()));
                entradaDTO.setCdMensagemRecadastroMidia(Integer
                    .parseInt(getRdoEmiteMsgRecadMidia()));
                entradaDTO.setCdMidiaMensagemRecadastro(Integer
                    .parseInt(getCboMidiaMsgRecad().toString()));
                if (getComboRetornoOperacoesSelecionado() != null && getComboRetornoOperacoesSelecionado() != 0l) {
                    entradaDTO.setCdIndicadorRetornoInternet(Integer.parseInt(getComboRetornoOperacoesSelecionado().toString()));
                } else {
                    entradaDTO.setCdIndicadorRetornoInternet(0);
                }
                entradaDTO.setRdoPermiteAcertosDados(Integer
                    .parseInt(getRdoPermiteAcertosDados()));

            }

            if(rdoGerarRetornoOperRealizadaInternet != null){
                entradaDTO.setCdIndicadorRetornoInternet(Integer.parseInt(getComboRetornoOperacoesSelecionado().toString()));
            }

            if(rdoGerarRetornoSeparadoCanal != null){
                entradaDTO.setCdIndicadorSeparaCanal(Integer.parseInt(rdoGerarRetornoSeparadoCanal));
            }

            /**************************************** Modalidade *******************************************/
            entradaDTO.setCdIndicadorEmissaoAviso(""
                .equals(getRdoEmiteAvitoCompVida()) ? 0 : Integer
                    .parseInt(getRdoEmiteAvitoCompVida()));
            entradaDTO
            .setCdIndicadorManutencaoProcurador(""
                .equals(getRdoEfetuarManutencaoCadastroProcuradores())
                ? 0
                    : Integer
                    .parseInt(getRdoEfetuarManutencaoCadastroProcuradores()));
            
            entradaDTO.setCindcdFantsRepas(0);
            
            /************************************ Tipo de Retorno Internet ***************************************/
            entradaDTO.setCdIndicadorTipoRetornoInternet(Integer.parseInt(String.valueOf(getComboCodIndicadorTipoRetornoInternet())));

            AlterarConfiguracaoTipoServModContratoSaidaDTO saidaDTO = getManterContratoService().alterarConfiguracaoTipoServModContrato(entradaDTO);
            
            setDsOrigemIndicador(saidaDTO.getDsOrigemIndicador());
    		setDsIndicador(saidaDTO.getDsIndicador());

            BradescoFacesUtils.addInfoModalMessage(
                "(" + saidaDTO.getCodMensagem() + ") "
                + saidaDTO.getMensagem(),
                "conServicosManterContrato",
                BradescoViewExceptionActionType.ACTION, false);

            carregaLista();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                "VOLTAR_CONFIGURACAO1",
                BradescoViewExceptionActionType.ACTION, 
                false);
            return null;
        }
        return "";
    }

    /**
     * Detalhar.
     * 
     * @return the string
     */
    public String detalhar() {
        try {
            limparConfiguracao();
            preencheDados();
            return "DETALHAR";
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }
        return "";
    }

    /**
     * Incluir.
     * 
     * @return the string
     */
    public String incluir() {
        setItemSelecionadoListaTipoServico(null);
        entradaServicosModalidades = new ListarServModEntradaDTO();

        identificacaoClienteContratoBean = (IdentificacaoClienteContratoBean) FacesUtils
        .getManagedBean("identificacaoClienteContratoBean");
        manterContratoBean = (ManterContratoBean) FacesUtils
        .getManagedBean("manterContratoBean");

        try {
            entradaServicosModalidades
            .setCdPessoaJuridicaContrato(identificacaoClienteContratoBean
                .getEmpresaGestoraFiltro());
            entradaServicosModalidades
            .setCdTipoContrato(identificacaoClienteContratoBean
                .getTipoContratoFiltro());
            entradaServicosModalidades.setNumeroContrato(manterContratoBean
                .getNumeroContrato());
            // alterado o PDC conf. e-mail de Luciana Fernandes Barreira de qui
            // 02/09/2010 15:30
            listaGridTipoServico = manterContratoService
            .listarTipoServicosRelacionadosIncluir(entradaServicosModalidades);
            // listaGridTipoServico = comboService.listarTipoServicos();

            listaControleRadioTipoServico = new ArrayList<SelectItem>();

            for (int i = 0; i < listaGridTipoServico.size(); i++) {
                this.listaControleRadioTipoServico.add(new SelectItem(i, " "));
            }
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }
        return "INCLUIR";
    }

    /**
     * Atualizar count checked modalidade.
     */
    public void atualizarCountCheckedModalidade() {
        this.countCheckedModalidade = 0;
        this.btnIncluir = false;

        for (int i = 0; i < this.listaGridModalidade.size(); i++) {
            if (this.listaGridModalidade.get(i).isCheck()) {
                countCheckedModalidade++;
                btnIncluir = true;
            }
        }
    }

    /**
     * Sacado eletronico.
     * 
     * @return the string
     */
    public String sacadoEletronico() {
        listaGridSacadoEletronico = new ArrayList<ListarParticipanteAgregadoSaidaDTO>();
        listaControleSacadoEletronico = new ArrayList<SelectItem>();
        setItemSelecionadoSacadoEletronico(null);

        ListarParticipanteAgregadoEntradaDTO entradaDTO = new ListarParticipanteAgregadoEntradaDTO();

        entradaDTO.setCdPessoaJuridica(getCdPessoaJuridica());
        entradaDTO.setCdTipoContrato(getCdTipoContrato());
        entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());

        listaGridSacadoEletronico = manterContratoService
        .listarParticipanteAgregado(entradaDTO);

        for (int i = 0; i < listaGridSacadoEletronico.size(); i++) {
            listaControleSacadoEletronico.add(new SelectItem(i, " "));
        }

        return "conSacadoEletronico";
    }

    /**
     * Paginar sacado.
     * 
     * @param avt
     *            the avt
     * @return the string
     */
    public String paginarSacado(ActionEvent avt) {
        sacadoEletronico();
        return "";
    }

    /**
     * Agregados.
     * 
     * @return the string
     */
    public String agregados() {
        listaGridAgregados = new ArrayList<ListarAgregadoSaidaDTO>();

        ListarParticipanteAgregadoSaidaDTO listaDTO = listaGridSacadoEletronico
        .get(itemSelecionadoSacadoEletronico);

        ListarAgregadoEntradaDTO entradaDTO = new ListarAgregadoEntradaDTO();

        entradaDTO.setCdCorpoCpfCnpj(listaDTO.getCdCorpoCpfCnpj());
        entradaDTO.setCdControleCpfCnpj(listaDTO.getCdControleCpfCnpj());
        entradaDTO.setCdFilialCpnj(listaDTO.getCdFilialCnpj());

        setCpfCnpj(CpfCnpjUtils.formatarCpfCnpj(listaDTO.getCdCorpoCpfCnpj(),
            listaDTO.getCdControleCpfCnpj(), listaDTO.getCdFilialCnpj()));
        setNomeRazaoSocial(listaDTO.getDsNomeParticipante());

        listaGridAgregados = manterContratoService.listarAgregado(entradaDTO);

        return "conAgregados";
    }

    /**
     * Paginar agregados.
     * 
     * @param aev
     *            the aev
     * @return the string
     */
    public String paginarAgregados(ActionEvent aev) {
        agregados();
        return "";
    }

    /**
     * Modalidade inc.
     * 
     * @return the string
     */
    public String modalidadeInc() {
        String outcome = "MODALIDADE";

        try {
            listaCheckedGridModalidade = null;

            listaGridModalidade = new ArrayList<ListarServicosSaidaDTO>();
            listaControleCheckModalidade = new ArrayList<SelectItem>();

            if (getItemSelecionadoListaTipoServico() != null) {
                listaGridModalidadeAux = new ArrayList<ListarServicosSaidaDTO>();
                ListarServicosSaidaDTO saidaDTO = listaGridTipoServico
                .get(getItemSelecionadoListaTipoServico());

                // Demanda 15 - 8752
                if (saidaDTO.getCdParametroTela() == 13) {
                    cdFlagRepresentante = "";
                    setListaRepresentantes(new ListarRepresentanteSaidaDTO());
                    setListaRepresentantesOcorrenciasSelecionadas(new ArrayList<ListarRepresentanteOcorrenciaDTO>());
                    setQtMesesEmissao(null);
                    setQtEmissoesPagasEmpresa(null);
                    setCdMidiaDisponComprovante(null);
                    setQtFuncionarios(null);
                    outcome = "COMPROVANTE";
                }

                dsTipoServicoSelecionado = saidaDTO.getDsServico();

                ListarServModEntradaDTO entradaDTO = new ListarServModEntradaDTO();
                entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
                entradaDTO.setCdTipoContrato(getCdTipoContrato());
                entradaDTO.setCdTipoServico(saidaDTO.getCdServico());
                entradaDTO.setNumeroContrato(getNrSequenciaContrato());

                setCdProdutoOperacaoRelacionado(saidaDTO.getCdServico());
                setCdProdutoServicoOperacao(saidaDTO.getCdProdutoServicoOperacao());

                setCountCheckedModalidade(0);
                setBtnIncluir(false);
                setCheckAll(false);

                listaGridModalidadeAux = (manterContratoService
                                .listarTipoServicosRelacionadosIncluir(entradaDTO));

                for (ListarServicosSaidaDTO occur : listaGridModalidadeAux) {
                    listaGridModalidade.add(occur);

                }
            }

            for (int v = 0; v < listaGridModalidade.size(); v++) {
                listaControleCheckModalidade.add(new SelectItem(v, " "));
            }
            getManterContratoBean().getDsServicoSelecionado().clear();
            getManterContratoBean().getDsServicoSelecionado().add(getListaGridTipoServico().get(getItemSelecionadoListaTipoServico()));
            getRenderizaBotaoAvancar();

        } catch (PdcAdapterFunctionalException p) {
            listaGridModalidadeAux = new ArrayList<ListarServicosSaidaDTO>();
            // BradescoFacesUtils.addInfoModalMessage("(" +
            // StringUtils.right(p.getCode(), 8) + ") "+ p.getMessage(),
            // false);
        }
        return outcome;
    }

    /**
     * Habilita campos floating lancamento.
     */
    public void habilitaCamposFloatingLancamento () {

        if("N".equalsIgnoreCase(getSaidaVerificarAtributo().getCdIndicadorDiaFloatPagamento())){
            setHabilitaQtdDiasFloating(true);
        }else{
            if (getQtddDiasFloating() != null && getQtddDiasFloating() > 0) {
                setRdoGerarLancProgramado("2");
                //getSaidaVerificarAtributo().setCdIndicadorDiaFloatPagamento("N");
                setHabilitaRdoGerarLancamento(true);
            }else{
                setHabilitaRdoGerarLancamento(false);

                if("2".equalsIgnoreCase(getRdoGerarLancProgramado())){
                    setHabilitaQtdDiasFloating(false);
                }else{
                    setHabilitaQtdDiasFloating(true);
                }

            }
        }

    }

    /**
     * Habilita radio lancamento.
     */
    public void habilitaRadioLancamento () {

        if("N".equalsIgnoreCase(getSaidaVerificarAtributo().getCdIndicadorDiaFloatPagamento())){
            setHabilitaQtdDiasFloating(true);
        }else{
            if (getQtddDiasFloating() != null && getQtddDiasFloating() > 0) {
                setRdoGerarLancProgramado("2");
                //getSaidaVerificarAtributo().setCdIndicadorDiaFloatPagamento("N");
                setHabilitaRdoGerarLancamento(true);
            }else{
                setHabilitaRdoGerarLancamento(false);

                if("2".equalsIgnoreCase(getRdoGerarLancProgramado())){
                    setHabilitaQtdDiasFloating(false);
                }else{
                    setHabilitaQtdDiasFloating(true);
                }

                if("2".equalsIgnoreCase(getRdoGerarLancProgramado()) && getQtddDiasFloating() == 0){
                    setHabilitaQtdDiasFloating(false);
                    setHabilitaRdoGerarLancamento(false);
                    //setRdoGerarLancProgramado("1");
                }
            }
        }

    }


    /**
     * Gets the renderiza botao avancar.
     *
     * @return the renderiza botao avancar
     */
    public boolean getRenderizaBotaoAvancar(){
        Integer codParametroTela = listaGridTipoServico.get(getItemSelecionadoListaTipoServico()).getCdParametroTela();
        if(codParametroTela != null && codParametroTela == 2){
            for (int i = 0; i < this.listaGridModalidade.size(); i++) {
                if (this.listaGridModalidade.get(i).isCheck()) {
                    if(this.listaGridModalidade.get(i).getCdParametroTela() != null && this.listaGridModalidade.get(i).getCdParametroTela() == 20) {
                        setDescricaoSegundaModalidade(this.listaGridModalidade.get(i).getDsServico());
                        return true;
                    }else{
                        return false;
                    }
                }
            }
        }
        return false;
    }


    /**
     * Checks if is lista representantes desabilitada.
     *
     * @return true, if is lista representantes desabilitada
     */
    public boolean isListaRepresentantesDesabilitada() {
        return this.getListaRepresentantes().getOcorrencias().isEmpty();
    }

    /**
     * Submit lista representantes.
     *
     * @param event the event
     */
    public void submitListaRepresentantes(ActionEvent event) {
        this.listarRepresentantesComprovante();
    }

    /**
     * Modifica opcao representante.
     *
     * @return the string
     */
    public String modificaOpcaoRepresentante() {
        this.setListaRepresentantes(new ListarRepresentanteSaidaDTO());
        this.setListaRepresentantesOcorrenciasSelecionadas(new ArrayList<ListarRepresentanteOcorrenciaDTO>());
        this.listarRepresentantesComprovante();
        return "";
    }

    /**
     * Listar representantes comprovante.
     *
     * @return the string
     */
    public String listarRepresentantesComprovante() {

        try {

            if (this.getCdFlagRepresentante() != null) {

                ListarRepresentanteEntradaDTO entrada = new ListarRepresentanteEntradaDTO();
                entrada.setCdFlagRepresentante(this.getCdFlagRepresentante()
                    .toUpperCase());
                entrada.setCdpessoaJuridicaContrato(this.getCdPessoaJuridica());
                entrada.setCdTipoContratoNegocio(this.getCdTipoContrato());
                entrada.setMaxOcorrencias(100);
                entrada.setNrSequenciaContratoNegocio(this
                    .getNrSequenciaContrato());

                ListarRepresentanteSaidaDTO saida = getManterContratoService()
                .listarRepresentantesComprovante(entrada);

                if (this.getListaRepresentantes() != null
                                && this.getListaRepresentantes().getOcorrencias() != null
                                && !this.getListaRepresentantes().getOcorrencias()
                                .isEmpty()) {

                    for (ListarRepresentanteOcorrenciaDTO ocorrencia : this
                                    .getListaRepresentantes().getOcorrencias()) {
                        if (!this
                                        .getListaRepresentantesOcorrenciasSelecionadas()
                                        .contains(ocorrencia)
                                        && ocorrencia.isSelecionado()) {
                            this
                            .getListaRepresentantesOcorrenciasSelecionadas()
                            .add(ocorrencia);
                        } else if (this
                                        .getListaRepresentantesOcorrenciasSelecionadas()
                                        .contains(ocorrencia)
                                        && !ocorrencia.isSelecionado()) {
                            this
                            .getListaRepresentantesOcorrenciasSelecionadas()
                            .remove(ocorrencia);
                        }
                    }
                }

                this.setListaRepresentantes(saida);

                for(ListarRepresentanteOcorrenciaDTO ocorrencia : this.getListaRepresentantes().getOcorrencias()) {
                    if (this.getListaRepresentantesOcorrenciasSelecionadas().contains(ocorrencia)) {
                        ListarRepresentanteOcorrenciaDTO ocorrSelecionada = 
                            this.getListaRepresentantesOcorrenciasSelecionadas().get(this.getListaRepresentantesOcorrenciasSelecionadas().indexOf(ocorrencia));

                        ocorrencia.setSelecionado(ocorrSelecionada.isSelecionado());
                        ocorrencia.setDsnome(ocorrSelecionada.getDsnome());
                        ocorrencia.setCdAgencia(ocorrSelecionada.getCdAgencia());
                        ocorrencia.setCdConta(ocorrSelecionada.getCdConta());
                        ocorrencia.setCdContaDigito(ocorrSelecionada.getCdContaDigito());
                    }
                }
            }
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }

        return "";
    }

    /**
     * Checks if is incluir representantes habilitado.
     *
     * @return true, if is incluir representantes habilitado
     */
    public boolean isIncluirRepresentantesHabilitado() {
        if (this.getCdMidiaDisponComprovante() == null
                        || "0".equals(this.getCdMidiaDisponComprovante())) {
            return false;
        }

        if (this.getQtFuncionarios() == null || this.getQtFuncionarios() <= 0) {
            return false;
        }

        if (this.getQtMesesEmissao() == null || this.getQtMesesEmissao() <= 0) {
            return false;
        }

        if (this.getQtEmissoesPagasEmpresa() == null
                        || "0".equals(this.getQtEmissoesPagasEmpresa())) {
            return false;
        }

        if (this.getCdFlagRepresentante() == null
                        || "".equals(this.getCdFlagRepresentante())) {
            return false;
        }

        return true;
    }

    /**
     * Incluir servicos representantes confirmar.
     *
     * @return the string
     */
    public String incluirServicosRepresentantesConfirmar() {

        boolean isRepresententeSelecionado = false;

        if (this.getListaRepresentantes() != null
                        && this.getListaRepresentantes().getOcorrencias() != null
                        && !this.getListaRepresentantes().getOcorrencias().isEmpty()) {

            for (ListarRepresentanteOcorrenciaDTO ocorrencia : this
                            .getListaRepresentantes().getOcorrencias()) {
                if (!this.getListaRepresentantesOcorrenciasSelecionadas()
                                .contains(ocorrencia)
                                && ocorrencia.isSelecionado()) {

                    isRepresententeSelecionado = true;

                    if (ocorrencia.getDsnome() == null
                                    || ocorrencia.getDsnome() == null
                                    || ocorrencia.getCdAgencia() == null
                                    || ocorrencia.getCdConta() == null
                                    || ocorrencia.getCdContaDigito() == null) {
                        BradescoFacesUtils
                        .addInfoModalMessage(
                            "FAVOR PREENCHER OS CAMPOS NOME REDUZIDO, AGENCIA, CONTA E D�GITO DE TODOS OS REPRESENTANTES OU PARTICIPANTES SELECIONADOS",
                            false);
                        return "";
                    }

                    this.getListaRepresentantesOcorrenciasSelecionadas().add(
                        ocorrencia);
                } else if (this.getListaRepresentantesOcorrenciasSelecionadas()
                                .contains(ocorrencia)
                                && !ocorrencia.isSelecionado()) {
                    this.getListaRepresentantesOcorrenciasSelecionadas()
                    .remove(ocorrencia);
                } else if (this.getListaRepresentantesOcorrenciasSelecionadas()
                                .contains(ocorrencia)
                                && ocorrencia.isSelecionado()) {

                    isRepresententeSelecionado = true;

                    if (ocorrencia.getDsnome() == null
                                    || ocorrencia.getDsnome() == null
                                    || ocorrencia.getCdAgencia() == null
                                    || ocorrencia.getCdConta() == null
                                    || ocorrencia.getCdContaDigito() == null) {
                        BradescoFacesUtils
                        .addInfoModalMessage(
                            "FAVOR PREENCHER OS CAMPOS NOME REDUZIDO, AGENCIA, CONTA E D�GITO DE TODOS OS REPRESENTANTES OU PARTICIPANTES SELECIONADOS",
                            false);
                        return "";
                    }
                }
            }
        }

        if (!isRepresententeSelecionado) {
            BradescoFacesUtils
            .addInfoModalMessage(
                "AO MENOS UM REPRESENTANTE OU PARTICIPANTE DEVE SER SELECIONADO",
                false);
            return "";
        }


        return "CONFIRMAR";
    }

    /**
     * Gets the ds midia dispon comprovante.
     *
     * @return the ds midia dispon comprovante
     */
    public String getDsMidiaDisponComprovante() {

        String dsMidiaDisponComprovante = "";

        Integer cdMidiaDisponComprovante = Integer.parseInt(this
            .getCdMidiaDisponComprovante());

        switch (cdMidiaDisponComprovante) {
            case 1 :
                dsMidiaDisponComprovante = "BDN";
                break;
            case 2 :
                dsMidiaDisponComprovante = "Internet";
                break;
            case 3 :
                dsMidiaDisponComprovante = "Ambos";
                break;
            default :
                break;
        }

        return dsMidiaDisponComprovante;
    }

    /**
     * Gets the ds via cobrada comprovante.
     *
     * @return the ds via cobrada comprovante
     */
    public String getDsViaCobradaComprovante() {

        Integer qtViaCobradaComprovante = Integer.parseInt(this
            .getQtEmissoesPagasEmpresa());
        String dsViaCobradaComprovante = "";

        switch (qtViaCobradaComprovante) {
            case 1 :
                dsViaCobradaComprovante = "UMA";
                break;
            case 2 :
                dsViaCobradaComprovante = "DUAS";
                break;
            case 3 :
                dsViaCobradaComprovante = "TRES";
                break;
            case 4 :
                dsViaCobradaComprovante = "TODAS";
                break;
            case 99 :
                dsViaCobradaComprovante = "TODAS";
                break;
            default :
                break;
        }

        return dsViaCobradaComprovante;
    }

    /**
     * Incluir servicos representantes.
     *
     * @return the string
     */
    public String incluirServicosRepresentantes() {

        try {

            IncluirRepresentanteEntradaDTO entrada = new IncluirRepresentanteEntradaDTO();
            entrada.setCdCanal(0);
            entrada.setCdDependenciaOperante(0);
            entrada.setCdEmpresaOperante(0L);
            entrada.setCdFinalidadeItemAditivo(0);
            entrada.setCdIndicadorFormaContratacao(0);

            Integer cdMidiaDisponComprovante = Integer.parseInt(this
                .getCdMidiaDisponComprovante());

            entrada.setCdMidiaComprovanteCorrentista(cdMidiaDisponComprovante);
            entrada.setDsMidiaComprovanteCorrentista(this
                .getDsMidiaDisponComprovante());

            entrada.setCdpessoaJuridicaContrato(this.getCdPessoaJuridica());
            entrada.setCdProdutoOperacaoRelacionado(getCdProdutoOperacaoRelacionado());
            entrada.setCdProdutoServicoOperacao(getCdProdutoServicoOperacao());
            entrada
            .setCdRelacionamentoProdutoProduto(this
                .getListaGridPesquisa().get(0)
                .getCdRelacionamentoProduto());
            entrada.setCdTipoContratoNegocio(this.getCdTipoContrato());
            entrada.setCdUsuario("       ");
            entrada.setNmOperacaoFluxo("      ");
            entrada.setNrManutencaoContratoNegocio(0L);
            entrada
            .setNrSequenciaContratoNegocio(this
                .getNrSequenciaContrato());
            entrada.setQtFuncionarioEmpresaPagadora(this.getQtFuncionarios());

            Integer qtViaCobradaComprovante = Integer.parseInt(this
                .getQtEmissoesPagasEmpresa());

            entrada.setQtViaCobradaComprovante(qtViaCobradaComprovante);
            entrada.setDsViaCobradaComprovante(this
                .getDsViaCobradaComprovante());
            entrada.setQtViaComprovante(this.getQtMesesEmissao());

            entrada
            .setOcorrencias(new ArrayList<ListarRepresentanteOcorrenciaDTO>());

            for (ListarRepresentanteOcorrenciaDTO dto : this
                            .getListaRepresentantes().getOcorrencias()) {
                if (this.getListaRepresentantesOcorrenciasSelecionadas()
                                .contains(dto)
                                && dto.isSelecionado()) {
                    entrada.getOcorrencias().add(dto);
                }
            }

            entrada.setQtdeTotalItens(this
                .getListaRepresentantesOcorrenciasSelecionadas().size());

            IncluirRepresentanteSaidaDTO saida = getManterContratoService()
            .incluirRepresentantesComprovante(entrada);

            BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
                + ") " + saida.getMensagem(), "conServicosManterContrato",
                BradescoViewExceptionActionType.ACTION, false);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }

        return "INCLUIDO";
    }

    /**
     * Atualiza dados bean.
     */
    public void atualizaDadosBean() {
        return;
    }

    /**
     * Modalidade inc proximo.
     * 
     * @return the string
     */
    public String modalidadeIncProximo() {
        this.listaCheckedGridModalidade = new ArrayList<ListarServicosSaidaDTO>();
        for (ListarServicosSaidaDTO item : this.listaGridModalidade) {
            if (item.isCheck()) {
                this.listaCheckedGridModalidade.add(item);
            }
        }

        return "MODALIDADE_PROXIMO";
    }


    /**
     * Modalidade inc proximo.
     * 
     * @return the string
     */
    public String modalidadeIncTela() {
        this.listaCheckedGridModalidade = new ArrayList<ListarServicosSaidaDTO>();
        for (ListarServicosSaidaDTO item : this.listaGridModalidade) {
            if (item.isCheck()) {
                this.listaCheckedGridModalidade.add(item);
            }
        }

        return "MODALIDADE_PROXIMO";
    }	

	/**
	 * Incluir vlidar vinc conv salario.
	 * 
	 * @return the string
	 */
	public String incluirVlidarVincConvSalario() {
		entradaValidarVinculacaoConvenioConta = new ValidarVinculacaoConvenioContaSalarioEntradaDTO();

		try {
			if (getListaContratosPgit() != null && getIdentificacaoClienteContratoBean().getItemSelecionadoLista() != null) {
				ListarContratosPgitSaidaDTO itemSelecionado = getListaContratosPgit().get(getIdentificacaoClienteContratoBean().getItemSelecionadoLista());

				entradaValidarVinculacaoConvenioConta.setCdpessoaJuridicaContrato(verificaLongNulo(itemSelecionado.getCdPessoaJuridica()));
				entradaValidarVinculacaoConvenioConta.setCdTipoContratoNegocio(verificaIntegerNulo(itemSelecionado.getCdTipoContrato()));
				entradaValidarVinculacaoConvenioConta.setNrSequenciaContratoNegocio(verificaLongNulo(itemSelecionado.getNrSequenciaContrato()));
				entradaValidarVinculacaoConvenioConta.setCdClubRepresentante(verificaLongNulo(itemSelecionado.getCdClubRepresentante()));
				entradaValidarVinculacaoConvenioConta.setCdCpfCnpjRepresentante(verificaLongNulo(itemSelecionado.getCdCpfCnpjRepresentante()));
				entradaValidarVinculacaoConvenioConta.setCdFilialCnpjRepresentante(verificaIntegerNulo(itemSelecionado.getCdFilialCpfCnpjRepresentante()));
				entradaValidarVinculacaoConvenioConta.setCdControleCpfRepresentante(verificaIntegerNulo(itemSelecionado.getCdControleCpfCnpjRepresentante()));
			}

//			if (itemSelecionadListaBanco == null) {
//				BradescoFacesUtils.addInfoModalMessage("Selecionar um conv�nio � necess�rio", false);
//				return "";
//			}

			if (saidaListarDadosCtaConvn.getOcorrencias() != null && getItemSelecionadListaBanco() != null) {
				ListarDadosCtaConvnListaOcorrenciasDTO itemOcorrencia = saidaListarDadosCtaConvn.getOcorrencias().get(getItemSelecionadListaBanco());

				entradaValidarVinculacaoConvenioConta.setCdInstituicaoBancaria(verificaIntegerNulo(itemOcorrencia.getCdBanco()));
				entradaValidarVinculacaoConvenioConta.setCdUnidadeOrganizacional(verificaIntegerNulo(itemOcorrencia.getCdAgencia()));
				entradaValidarVinculacaoConvenioConta.setNrContaDigito(itemOcorrencia.getCdDigitoConta());
				entradaValidarVinculacaoConvenioConta.setCdIndicadorNumConve(verificaIntegerNulo(itemOcorrencia.getCdConvnNovo()));
				entradaValidarVinculacaoConvenioConta.setCdConveCtaSalarial(itemOcorrencia.getCdConveCtaSalarial());
			}

			if (txtValorFolhaPgamento == null || "".equals(txtValorFolhaPgamento)) {
				BradescoFacesUtils.addInfoModalMessage("Preencher o campo valor da folha de pagamento � necess�rio", false);
				return "";
			} else {
				entradaValidarVinculacaoConvenioConta.setVlPagamentoMes(PgitUtil.isNullBigDecimal(getTxtValorFolhaPgamento()));
			}

			if (txtQtdFuncionarios == null || "".equals(txtQtdFuncionarios)) {
				BradescoFacesUtils.addInfoModalMessage("Preencher o campo quantidade de funcion�rios � necess�rio", false);
				return "";
			} else {
				entradaValidarVinculacaoConvenioConta.setQtdFuncionarioFlPgto(NumberUtils.convertIntegerVolume(getTxtQtdFuncionarios()));
			}

			entradaValidarVinculacaoConvenioConta.setVlMediaSalarialMes(PgitUtil.verificaBigDecimalNulo(getTxtMediaSalarial()));
			entradaValidarVinculacaoConvenioConta.setCdPagamentoSalarialMes(verificaCheckSelecionado(isChkLoteMensal()));
			entradaValidarVinculacaoConvenioConta.setCdPagamentoSalarialQzena(verificaCheckSelecionado(isChkLoteQuinzenal()));

			if (txtMesAnoCorrespondente == null || "".equals(txtMesAnoCorrespondente)) {
				BradescoFacesUtils.addInfoModalMessage("Preencher o campo quantidade de m�s/ano correspondente � necess�rio", false);
				return "";
			} else {
				entradaValidarVinculacaoConvenioConta.setDtReftFlPgto(NumberUtils.convertIntegerVolume(getTxtMesAnoCorrespondente()));
			}

			if (chkLoteMensal == false && chkLoteQuinzenal == false) {
				BradescoFacesUtils.addInfoModalMessage("Selecionar uma forma de pagamento � necess�rio", false);
				return "";
			}

			if (aberturaContaSalario == null) {
				BradescoFacesUtils.addInfoModalMessage("Selecionar uma op��o de abertura de conta sal�rio � necess�rio", false);
				return "";
			}

			if (radioFidelize == null) {
				BradescoFacesUtils.addInfoModalMessage("Selecionar uma op��o de Fidelize � necess�rio", false);
				return "";
			}
			
			if (altoTurnover == null) {
				BradescoFacesUtils.addInfoModalMessage("� obrigat�rio informar uma op��o de Alto Turnover", false);
				return "";
			}
			
			if (ofertaCartaoPre == null) {
				BradescoFacesUtils.addInfoModalMessage("� obrigat�rio informar uma op��o de Oferta Cart�o Pr�-Pago", false);
				return "";
			}
			
			if (peloApp == null) {
				BradescoFacesUtils.addInfoModalMessage("� obrigat�rio informar uma op��o de Ativa��o pelo APP", false);
				return "";
			}
			
			if (PgitUtil.isStringVazio(dsEmailEmpresa)) {
				BradescoFacesUtils.addInfoModalMessage("Preencher o Email Empresa � necess�rio", false);
				return "";
			} else {
				String[] email = dsEmailEmpresa.split(";");
				for (String item : email) {
					if (!PgitUtil.isEmailValido(PgitUtil.tirarEspacoEsquerdaDireita(item))) {
						BradescoFacesUtils.addInfoModalMessage("Email Empresa � inv�lido.", false);
						return "";
					}
				}
			}
			
			if (PgitUtil.isStringVazio(dsEmailAgencia)) {
				BradescoFacesUtils.addInfoModalMessage("Preencher o Email Agencia � necess�rio", false);
				return "";
			} else {
				String[] email = dsEmailAgencia.split(";");
				for (String item : email) {
					if (!PgitUtil.isEmailValido(PgitUtil.tirarEspacoEsquerdaDireita(item))) {
						BradescoFacesUtils.addInfoModalMessage("Email Agencia � inv�lido.", false);
						return "";
					}
				}
			}
			
			entradaValidarVinculacaoConvenioConta
					.setCdLocalContaSalarial(verificaRadioAgenciaSelecionado(getAberturaContaSalario()));
			entradaValidarVinculacaoConvenioConta.setCdPessoaJuridicaConta(0l);
			entradaValidarVinculacaoConvenioConta.setCdTipoContratoConta(0);
			entradaValidarVinculacaoConvenioConta.setNrSequenciaContratoConta(0l);

			String cta = getItemSelecionadListaBanco() == null ? "0" : PgitUtil.longToString(saidaListarDadosCtaConvn
					.getOcorrencias().get(getItemSelecionadListaBanco()).getCdCta());
			entradaValidarVinculacaoConvenioConta.setNrConta(Integer.parseInt(cta));
			saidaValidarVinculacaoConvenioConta = manterContratoService.validarVincConvenioCtaSalario(entradaValidarVinculacaoConvenioConta);

			carregarDadosTelaConfirmar();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
		
		return "OK";
	}

    /**
     * Confirmar inc conv salario.
     */
    public void confirmarIncConvSalario(){

        entradaIncluirPagamentoSalario = new IncluirPagamentoSalarioEntradaDTO();

        try{
            ListarContratosPgitSaidaDTO itemSelecionado = listaContratosPgit.get(identificacaoClienteContratoBean.getItemSelecionadoLista());
            Integer indicadorAlcadaAgencia = getSaidaValidarVinculacaoConvenioConta().getCdIndicadorAlcadaAgencia();

            if(getItemSelecionadoListaTipoServico() != null &&  getListaCheckedGridModalidade() != null ){
                ListarServicosSaidaDTO itemSelecionadoListaTipoServico = getListaCheckedGridModalidade().get(0);
                entradaIncluirPagamentoSalario.setCdProdutoServicoOperacao(itemSelecionadoListaTipoServico.getCdProdutoServicoOperacao());
                entradaIncluirPagamentoSalario.setCdProdutoOperacaoRelacionado(itemSelecionadoListaTipoServico.getCdServico());
            }

//            ListarDadosCtaConvnListaOcorrenciasDTO itemSelecionadoOcorrencia = saidaListarDadosCtaConvn.getOcorrencias().get(getItemSelecionadListaBanco());

            entradaIncluirPagamentoSalario.setNmRazaoRepresentante(getManterContratoBean().getNomeRazaoSocialMaster());	
            entradaIncluirPagamentoSalario.setCdpessoaJuridicaContrato(itemSelecionado.getCdPessoaJuridica());
            entradaIncluirPagamentoSalario.setCdTipoContratoNegocio(itemSelecionado.getCdTipoContrato());
            entradaIncluirPagamentoSalario.setNrSequenciaContratoNegocio(itemSelecionado.getNrSequenciaContrato());
            entradaIncluirPagamentoSalario.setCdClubRepresentante(itemSelecionado.getCdClubRepresentante());
            entradaIncluirPagamentoSalario.setCdCpfCnpjRepresentante(itemSelecionado.getCdCpfCnpjRepresentante());
            entradaIncluirPagamentoSalario.setCdFilialCnpjRepresentante(itemSelecionado.getCdFilialCpfCnpjRepresentante());
            entradaIncluirPagamentoSalario.setCdControleCpfRepresentante(itemSelecionado.getCdControleCpfCnpjRepresentante());
            entradaIncluirPagamentoSalario.setCdPessoaJuridicaConta(0l);
            entradaIncluirPagamentoSalario.setCdTipoContratoConta(0);
            entradaIncluirPagamentoSalario.setNrSequenciaContratoConta(0l);
//            entradaIncluirPagamentoSalario.setCdInstituicaoBancaria(itemSelecionadoOcorrencia.getCdBanco());
//            entradaIncluirPagamentoSalario.setCdUnidadeOrganizacional(itemSelecionadoOcorrencia.getCdAgencia());
//            entradaIncluirPagamentoSalario.setNrContaDigito(itemSelecionadoOcorrencia.getCdDigitoConta());
//            entradaIncluirPagamentoSalario.setCdIndicadorNumConve(itemSelecionadoOcorrencia.getCdConvnNovo());
//            entradaIncluirPagamentoSalario.setCdConveCtaSalarial(itemSelecionadoOcorrencia.getCdConveCtaSalarial());
            entradaIncluirPagamentoSalario.setVlPagamentoMes(PgitUtil.isNullBigDecimal(getTxtValorFolhaPgamento()));
            entradaIncluirPagamentoSalario.setQtdFuncionarioFlPgto(NumberUtils.convertIntegerVolume(getTxtQtdFuncionarios()));
            entradaIncluirPagamentoSalario.setVlMediaSalarialMes(PgitUtil.verificaBigDecimalNulo(getTxtMediaSalarial()));
            entradaIncluirPagamentoSalario.setCdPagamentoSalarialMes(verificaCheckSelecionado(isChkLoteMensal()));
            entradaIncluirPagamentoSalario.setCdPagamentoSalarialQzena(verificaCheckSelecionado(isChkLoteQuinzenal()));
            entradaIncluirPagamentoSalario.setCdLocalContaSalarial(verificaRadioAgenciaSelecionado(getAberturaContaSalario()));
            entradaIncluirPagamentoSalario.setDtReftFlPgto(NumberUtils.convertIntegerVolume(getTxtMesAnoCorrespondente()));
            entradaIncluirPagamentoSalario.setCdIndicadorAlcadaAgencia(indicadorAlcadaAgencia);
            entradaIncluirPagamentoSalario.setCdFidelize(verificaRadioFidelizeSelecionado(getRadioFidelize()));
            entradaIncluirPagamentoSalario.setDsEmailAgencia(getDsEmailAgencia());
            entradaIncluirPagamentoSalario.setDsEmailEmpresa(getDsEmailEmpresa());
            
            entradaIncluirPagamentoSalario.setCdAltoTurnover(verficarRadiosTurnOverCartaoPrePagoOfertaPeloApp(getAltoTurnover()));	
            entradaIncluirPagamentoSalario.setCdCartaoPre(verficarRadiosTurnOverCartaoPrePagoOfertaPeloApp(getOfertaCartaoPre()));
            entradaIncluirPagamentoSalario.setCdPeloApp(verficarRadiosTurnOverCartaoPrePagoOfertaPeloApp(getPeloApp()));

//            String cta = PgitUtil.longToString(saidaListarDadosCtaConvn.getOcorrencias().get(getItemSelecionadListaBanco()).getCdCta());
//            entradaIncluirPagamentoSalario.setNrConta(Integer.parseInt(cta));

            saidaIncluirPagamentoSalario = manterVincPerfilTrocaArqPartImpl.incluirPagamentoSalario(entradaIncluirPagamentoSalario);

            if(indicadorAlcadaAgencia == 2){
                StringBuilder url = new StringBuilder();
                url.append("/fneb/roteador.aditamento.jsf?empresas=");
                url.append(itemSelecionado.getCdPessoaJuridica());
                url.append("&codClub=");
                url.append(itemSelecionado.getCdClubRepresentante());
                url.append("&cpssoaJuridContr=");
                url.append(itemSelecionado.getCdPessoaJuridica());
                url.append("&ctpoContrNegoc=");
                url.append(itemSelecionado.getCdTipoContrato());
                url.append("&nseqContrNegoc=");
                url.append(itemSelecionado.getNrSequenciaContrato());
                url.append("&cprodtServcOper=");
                url.append(getSaidaValidarVinculacaoConvenioConta().getCdProdutoServicoOperacao());
                url.append("&cSist=PGIT&ctpoPpsta=32&indGerarProposta=1");
                setUrlRenegociacaoNovo(url.toString());
            }else{
                BradescoFacesUtils.addInfoModalMessage("(" + saidaIncluirPagamentoSalario.getCodMensagem() + ") " + 
                    saidaIncluirPagamentoSalario.getMensagem(), "CONFIRMAR", BradescoViewExceptionActionType.ACTION, false);
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }

    }

    /**
     * Carregar dados tela confirmar.
     */
    public void carregarDadosTelaConfirmar(){
        listaSaidaListarDadosCtaConvn = new ArrayList<ListarDadosCtaConvnListaOcorrenciasDTO>();

        setAberturaContaSalarioConf("0".equals(getAberturaContaSalario()) ? "Ag�ncia" : "Bradesco Expresso");
        setFidelizeConf("0".equals(getRadioFidelize()) ? "Sim" : "N�o");
        setTipoLotePagamentoConf(verificaCheckSelecionado());
        setTxtValorFolhaPgamentoConf(PgitUtil.isNullBigDecimal(getTxtValorFolhaPgamento()));
        setTxtQtdFuncionariosConf(getTxtQtdFuncionarios());
        setTxtMediaSalarialConf(PgitUtil.verificaBigDecimalNulo(getTxtMediaSalarial()));
        setTxtMesAnoCorrespondenteConf(getTxtMesAnoCorrespondente());
        
        setAltoTurnoverConf("1".equals(getAltoTurnover()) ? "Sim" : "N�o");
        setOfertaCartaoPreConf("1".equals(getOfertaCartaoPre()) ? "Sim" : "N�o");
        setPeloAppConf("1".equals(getPeloApp()) ? "Sim" : "N�o");
        
        ListarDadosCtaConvnListaOcorrenciasDTO obj = new ListarDadosCtaConvnListaOcorrenciasDTO();
//        obj.setBancoAgenciaConta(saidaListarDadosCtaConvn.getOcorrencias().get(getItemSelecionadListaBanco()).getBancoAgConta());
//        obj.setCdConvnNovo(saidaListarDadosCtaConvn.getOcorrencias().get(getItemSelecionadListaBanco()).getCdConvnNovo());
//        obj.setCdConveCtaSalarial(saidaListarDadosCtaConvn.getOcorrencias().get(getItemSelecionadListaBanco()).getCdConveCtaSalarial());
        listaSaidaListarDadosCtaConvn.add(obj);
    }

    /**
     * Verifica check selecionado.
     *
     * @return the string
     */
    private String verificaCheckSelecionado(){
        String ChkMensalQuinzenal = "";
        if(entradaValidarVinculacaoConvenioConta.getCdPagamentoSalarialMes()!=null && entradaValidarVinculacaoConvenioConta.getCdPagamentoSalarialMes()== 1){
            ChkMensalQuinzenal = "Mensal";
        }

        if(entradaValidarVinculacaoConvenioConta.getCdPagamentoSalarialQzena()!=null && entradaValidarVinculacaoConvenioConta.getCdPagamentoSalarialQzena() == 1){
            if(ChkMensalQuinzenal == null || "".equals(ChkMensalQuinzenal)){
                ChkMensalQuinzenal = "Quinzenal";
            }else{
                ChkMensalQuinzenal = ChkMensalQuinzenal + "/Quinzenal";
            }
        }

        return ChkMensalQuinzenal;
    }


    /**
     * Verifica check selecionado.
     *
     * @param check the check
     * @return the integer
     */
    public Integer verificaCheckSelecionado(boolean check){
        if(check){
            return 1;
        }else{
            return 2;
        }
    }


    /**
     * Verifica radio agencia selecionado.
     *
     * @param radio the radio
     * @return the integer
     */
    public Integer verificaRadioAgenciaSelecionado(String radio){
        if(radio != null){
            if("1".equals(radio)){
                return 2;
            }else{
                return 1;
            }
        }	
        return 0;
    }
    
    /**
     * Verifica radio fidelize selecionado.
     *
     * @param radio the radio
     * @return the integer
     */
    public Integer verificaRadioFidelizeSelecionado(String radio){
        if(radio != null){
            if("0".equals(radio)){
                return 1;
            }else{
                return 2;
            }
        }	
        return 0;
    }
    
    public Integer verficarRadiosTurnOverCartaoPrePagoOfertaPeloApp(String radio){
    	if("1".equals(radio)){
    		return 1;
    	}else{
    		return 0;
    	}
    }
    

    /**
     * Limpar campos.
     */
    public void limparCampos(){
        setTxtValorFolhaPgamento(null);
        setTxtQtdFuncionarios(null);
        setTxtMediaSalarial(null);
        setChkLoteMensal(false);
        setChkLoteQuinzenal(false);
        setAberturaContaSalario(null);
        setTxtMesAnoCorrespondente(null);
        setItemSelecionadListaBanco(null);
        setDsEmailAgencia(null);
        setDsEmailEmpresa(null);
        setListaCodIndicadorTipoRetornoInternet(null);
    }


    /**
     * Modalidade avancar.
     * 
     * @return the string
     */
    public String modalidadeAvancar() {
        this.listaCheckedGridModalidade = new ArrayList<ListarServicosSaidaDTO>();
        setEntradaListarDadosCtaConvn(new ListarDadosCtaConvnEntradaDTO());	

        setSaidaListarDadosCtaConvn(new ListarDadosCtaConvnSaidaDTO());

        listaContratosPgit = identificacaoClienteContratoBean.getListaGridPesquisa();

        entradaListarDadosCtaConvn.setCdPessoa(listaContratosPgit.get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getCdPessoaJuridica());
        entradaListarDadosCtaConvn.setCdTipoContratoNegocio(listaContratosPgit.get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getCdTipoContrato());
        entradaListarDadosCtaConvn.setNrSequenciaContratoNegocio(listaContratosPgit.get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getNrSequenciaContrato());
        entradaListarDadosCtaConvn.setCdClubPessoaRepresentante(listaContratosPgit.get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getCdClubRepresentante());
        entradaListarDadosCtaConvn.setCdCpfCnpjRepresentante(listaContratosPgit.get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getCdCpfCnpjRepresentante());
        entradaListarDadosCtaConvn.setCdFilialCnpjRepresentante(listaContratosPgit.get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getCdFilialCpfCnpjRepresentante());
        entradaListarDadosCtaConvn.setCdControleCnpjRepresentante(listaContratosPgit.get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getCdControleCpfCnpjRepresentante());
        entradaListarDadosCtaConvn.setCdParmConvenio(2);

        setSaidaListarDadosCtaConvn(manterVinculacaoConvenioContaSalarioService.listarDadosContaConvenio(entradaListarDadosCtaConvn));

        for (ListarServicosSaidaDTO item : this.listaGridModalidade) {
            if (item.isCheck()) {
                this.listaCheckedGridModalidade.add(item);
            }
        }
        listaControleRadioDadosConta = new ArrayList<SelectItem>();
        for (int i = 0; i < getSaidaListarDadosCtaConvn().getOcorrencias().size(); i++) {
            this.listaControleRadioDadosConta.add(new SelectItem(i, " "));
        }
        
        
        //setando campos radio como NAO, INDICE 2
        setAltoTurnover(DOIS_STR);
        setOfertaCartaoPre(DOIS_STR);
        setPeloApp(DOIS_STR);

        return "MODALIDADE_AVANCAR";
    }	

    /**
     * Confirmar inc.
     * 
     * @return the string
     */
    public String confirmarInc() {
        try {
            Integer qtdServicos = 0;
            List<Integer> servicosSelecionados = new ArrayList<Integer>();
            IncluirTipoServModContratoEntradaDTO entradaDTO = new IncluirTipoServModContratoEntradaDTO();

            entradaDTO.setCdPessoaJuridicaContrato(this.cdPessoaJuridica);
            entradaDTO.setCdTipoContrato(this.cdTipoContrato);
            entradaDTO.setNrSequenciaContrato(this.nrSequenciaContrato);
            entradaDTO.setNrQtdeModalidades(listaCheckedGridModalidade == null
                ? 0
                    : listaCheckedGridModalidade.size());

            if (getItemSelecionadoListaTipoServico() != null) {

                servicosSelecionados.add(listaGridTipoServico.get(
                    itemSelecionadoListaTipoServico).getCdServico());
                qtdServicos++;

            }

            entradaDTO.setNrQtdeServicos(qtdServicos);

            entradaDTO.setListaServicos(servicosSelecionados);

            entradaDTO.setListaModalidade(listaCheckedGridModalidade);

            IncluirTipoServModContratoSaidaDTO saidaDTO = this.manterContratoService
            .incluirTipoServModContrato(entradaDTO);

            BradescoFacesUtils.addInfoModalMessage(
                "(" + saidaDTO.getCodMensagem() + ") "
                + saidaDTO.getMensagem(),
                "conServicosManterContrato",
                BradescoViewExceptionActionType.ACTION, false);
            carregaLista();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return null;
        }

        return "";
    }




    /**
     * Excluir.
     * 
     * @return the string
     */
    public String excluir() {

        try {

            ListarModalidadeTipoServicoSaidaDTO modalidadeTipoServicoSaidaDTO = this.listaGridPesquisa
            .get(this.itemSelecionadoLista);
            // Conforme solicitado pela Luciana, dia 22/julho, no pdc
            // ListarModalidadeTipoServico, quando precisar
            // aprensentar a dsServico pegar o campo
            // no pdc de dsModalidade, mesma coisa para o codigo, porem quando
            // solicitar os dois campos, o cdServico
            // sera ele mesmo e o cdModalidade tambem
            setCdTipoServicoSelecionado(modalidadeTipoServicoSaidaDTO
                .getCdModalidade());
            setDsTipoServicoSelecionado(modalidadeTipoServicoSaidaDTO
                .getDsModalidade());

            ListarModalidadeTipoServicoEntradaDTO entradaDTO = new ListarModalidadeTipoServicoEntradaDTO();
            entradaDTO.setCdModalidade(0);
            entradaDTO.setCdPessoaJuridica(getCdPessoaJuridica());
            entradaDTO.setCdTipoContrato(getCdTipoContrato());
            entradaDTO.setCdTipoServico(getCdTipoServicoSelecionado());
            entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());

            listaGridServicoRelacionado = manterContratoService
            .listarModalidadeTipoServico(entradaDTO);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }

        return "EXCLUIR";
    }

    /**
     * Confirmar exc.
     * 
     * @return the string
     */
    public String confirmarExc() {
        try {
            ExcluirTipoServModContratoEntradaDTO entradaDTO = new ExcluirTipoServModContratoEntradaDTO();
            entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
            entradaDTO.setCdTipoContrato(getCdTipoContrato());
            entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
            entradaDTO.setCdTipoServico(getCdTipoServicoSelecionado());

            if (getListaGridServicoRelacionado().size() > 0) {
                entradaDTO.setCdExerServicoModalidade(2);
                for (int i = 0; i < listaGridServicoRelacionado.size(); i++) {
                    if (i == 0) {
                        entradaDTO.setCdModalidade1(listaGridServicoRelacionado.get(i).getCdModalidade() == null ? 0: 
                            listaGridServicoRelacionado.get(i).getCdModalidade());
                    }
                    if (i == 1) {
                        entradaDTO.setCdModalidade2(listaGridServicoRelacionado.get(i).getCdModalidade() == null
                            ? 0: listaGridServicoRelacionado.get(i).getCdModalidade());
                    }
                    if (i == 2) {
                        entradaDTO.setCdModalidade3(listaGridServicoRelacionado.get(i).getCdModalidade() == null
                            ? 0: listaGridServicoRelacionado.get(i).getCdModalidade());
                    }
                    if (i == 3) {
                        entradaDTO.setCdModalidade4(listaGridServicoRelacionado.get(i).getCdModalidade() == null
                            ? 0: listaGridServicoRelacionado.get(i).getCdModalidade());
                    }
                    if (i == 4) {
                        entradaDTO.setCdModalidade5(listaGridServicoRelacionado.get(i).getCdModalidade() == null ? 0
                                : listaGridServicoRelacionado.get(i).getCdModalidade());
                       }
                    if (i == 5) {
                        entradaDTO.setCdModalidade6(listaGridServicoRelacionado.get(i).getCdModalidade() == null ? 0 :
                            listaGridServicoRelacionado.get(i) .getCdModalidade());
                    }
                    if (i == 6) {
                        entradaDTO.setCdModalidade7(listaGridServicoRelacionado.get(i).getCdModalidade() == null
                            ? 0 : listaGridServicoRelacionado.get(i).getCdModalidade());
                    }
                    if (i == 7) {
                        entradaDTO.setCdModalidade8(listaGridServicoRelacionado.get(i).getCdModalidade() == null
                            ? 0 : listaGridServicoRelacionado.get(i).getCdModalidade());
                    }
                    if (i == 8) {
                        entradaDTO.setCdModalidade9(listaGridServicoRelacionado.get(i).getCdModalidade() == null
                            ? 0: listaGridServicoRelacionado.get(i).getCdModalidade());
                    }
                    if (i == 9) {
                        entradaDTO.setCdModalidade10(listaGridServicoRelacionado.get(i).getCdModalidade() == null
                            ? 0 : listaGridServicoRelacionado.get(i).getCdModalidade());
                    }
                }
            } else {
                entradaDTO.setCdExerServicoModalidade(1);
                entradaDTO.setCdModalidade1(0);
                entradaDTO.setCdModalidade2(0);
                entradaDTO.setCdModalidade3(0);
                entradaDTO.setCdModalidade4(0);
                entradaDTO.setCdModalidade5(0);
                entradaDTO.setCdModalidade6(0);
                entradaDTO.setCdModalidade7(0);
                entradaDTO.setCdModalidade8(0);
                entradaDTO.setCdModalidade9(0);
                entradaDTO.setCdModalidade10(0);
            }

            ExcluirTipoServModContratoSaidaDTO saidaDTO = 
                this.manterContratoService.excluirTipoServModContrato(entradaDTO);

            BradescoFacesUtils.addInfoModalMessage(
                "(" + saidaDTO.getCodMensagem() + ") "
                + saidaDTO.getMensagem(),
                "conServicosManterContrato",
                BradescoViewExceptionActionType.ACTION, false);
            carregaLista();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return null;
        }

        return "";
    }

    /**
     * Historico.
     * 
     * @return the string
     */
    public String historico() {
        setFiltroDataDe(new Date());
        setFiltroDataAte(new Date());
        carregaListaTipoServico();
        setCdTipoServico(0);
        setItemSelecionadoLista3(null);
        setListaGridPesquisa3(null);
        setBtoAcionado(true);
        setDesabilitarCombo(true);
        setBtnConsultarHist(true);

        if (getItemSelecionadoLista() != null) {
            carregaListaTipoServico();
            ListarModalidadeTipoServicoSaidaDTO saidaDTO = getListaGridPesquisa()
            .get(getItemSelecionadoLista());
            setCdTipoServico(saidaDTO.getCdModalidade());
            // consultarHistorico();
        }

        return "HISTORICO";
    }

    /**
     * Limpar dados historico.
     */
    public void limparDadosHistorico() {
        setCdTipoServico(0);
        setFiltroDataDe(new Date());
        setFiltroDataAte(new Date());
        setDesabilitarCombo(true);
        setBtoAcionado(true);
        setBtnConsultarHist(true);

        setListaGridPesquisa3(null);
        setItemSelecionadoLista3(null);
    }

    /**
     * Consultar historico.
     */
    public void consultarHistorico() {
        listaGridPesquisa3 = new ArrayList<ListarConManServicoSaidaDTO>();
        try {
            ListarConManServicoEntradaDTO entrada = new ListarConManServicoEntradaDTO();

            entrada.setCdPessoaJuridicaNegocio(getCdPessoaJuridica() != null
                ? getCdPessoaJuridica()
                    : 0l);
            entrada.setCdTipoContratoNegocio(getCdTipoContrato() != null
                ? getCdTipoContrato()
                    : 0);
            entrada
            .setNrSequenciaContratoNegocio(getNrSequenciaContrato() != null
                ? getNrSequenciaContrato()
                    : 0l);

            entrada.setCdProdutoOperacaoRelacionado(getCdTipoServico() != null
                ? getCdTipoServico()
                    : 0);
            entrada.setCdProdutoServicoOperacao(0);
            entrada.setCdTipoServico(1);
            entrada.setDtInicio(getFiltroDataDe() != null ? FormatarData
                .formataDiaMesAno(getFiltroDataDe()) : "");
            entrada.setDtFim(getFiltroDataAte() != null ? FormatarData
                .formataDiaMesAno(getFiltroDataAte()) : "");

            setListaGridPesquisa3(getManterContratoService()
                .listarConManServico(entrada));
            setBtoAcionado(false);
            setDesabilitarCombo(false);
            setBtnConsultarHist(false);
            listaControleRadio3 = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaGridPesquisa3().size(); i++) {
                listaControleRadio3.add(new SelectItem(i, ""));
            }
            setItemSelecionadoLista3(null);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setItemSelecionadoLista3(null);
            setListaGridPesquisa3(null);
            setBtoAcionado(true);
            setDesabilitarCombo(true);
            setBtnConsultarHist(true);
        }
    }

    /**
     * Detalhar historico.
     * 
     * @return the string
     */
    public String detalharHistorico() {
        try {
            preencheDadosHistorico();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), "histServicosManterContrato",
                BradescoViewExceptionActionType.ACTION, false);
            return null;
        }
        return "DETALHAR";
    }

    /**
     * Preenche dados historico.
     */
    public void preencheDadosHistorico() {
        ListarConManServicoSaidaDTO dto = listaGridPesquisa3 .get(getItemSelecionadoLista3());
        ConsultarConManServicosEntradaDTO entrada = new ConsultarConManServicosEntradaDTO();

        entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
        entrada.setCdTipoContratoNegocio(getCdTipoContrato());
        entrada.setNrSequenciaContratoNegocio(getNrSequenciaContrato());

        entrada.setCdParamentro(1);
        entrada.setCdParametroTela(dto.getCdParametroTela());
        entrada.setCdProdutoOperacaoRelacionado(dto.getCdModalidade());
        entrada.setCdProdutoServicoOperacao(dto.getCdProduto());
        entrada.setHrInclusaoRegistroHistorico(dto
            .getHrInclusaoRegistroHistorico());

        setCdParametroTela(dto.getCdParametroTela());
        setDsTipoServicoSelecionado(dto.getDsProduto());

        ConsultarConManServicosSaidaDTO saidaDTOHistorico = getManterContratoService().consultarConManServico(entrada);
        
        if(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet()== 0){
        	setComboCodIndicadorTipoRetornoInternet(Long.parseLong(String.valueOf(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet())));
        }else if(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet() == 1){
        	setComboCodIndicadorTipoRetornoInternet(Long.parseLong(String.valueOf(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet())));
        }else if(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet() == 2){
        	setComboCodIndicadorTipoRetornoInternet(Long.parseLong(String.valueOf(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet())));
        }else if(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet() == 3){
        	setComboCodIndicadorTipoRetornoInternet(Long.parseLong(String.valueOf(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet())));
        }
    	
        String descricaoTipoManutencao = null;
//        Long cdTipoManutencao = comboCodIndicadorTipoRetornoInternet;

        if(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet()== 0){
        	descricaoTipoManutencao = IdentificacaoTipoRetornoInternet.DS_IDENTIFICADOR_TIPO_RETORNO_INTERNET_NENHUM;
        }else if(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet() == 1){
        	descricaoTipoManutencao = IdentificacaoTipoRetornoInternet.DS_IDENTIFICADOR_TIPO_RETORNO_INTERNET_MANUTENCAO_PAGAMENTOS;
        }else if(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet() == 2){
        	descricaoTipoManutencao = IdentificacaoTipoRetornoInternet.DS_IDENTIFICADOR_TIPO_RETORNO_INTERNET_AUTORIZACAO_DESAUTORIZACAO;
        }else if(saidaDTOHistorico.getCdIdentificadorTipoRetornoInternet() == 3){
        	descricaoTipoManutencao = IdentificacaoTipoRetornoInternet.DS_IDENTIFICADOR_TIPO_RETORNO_INTERNET_AMBOS;
        }
        
        setQtDiaUtilPgto(saidaDTOHistorico.getQtDiaUtilPgto());
        

        // Pagamento de Fornecedores
        if (dto.getCdParametroTela() == 1) {
            setCdFormaEnvioPagamento(saidaDTOHistorico.getCdFormaEnvioPagamento());
            setCdFormaAutPagamento(saidaDTOHistorico.getCdFormaAutorizacaoPagamento());
            setCdUtilizaPreAut(saidaDTOHistorico.getCdIndicadorAutorizacaoCliente());
            setCdTipoContFloating(saidaDTOHistorico.getCdTipoDataFloating());

            // novas
            setCboFormaEnvioPagamento(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdFormaEnvioPagamento())));
            setDsFormaEnvioPagamento(saidaDTOHistorico.getDsFormaEnvioPagamento());
            setCboFormaAutPagamentos(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdFormaAutorizacaoPagamento())));
            setDsFormaAutPagamentos(saidaDTOHistorico.getDsFormaAutorizacaoPagamento());
            //setRdoPreAutorizacaoCliente(String.valueOf(saidaDTO
            //		.getCdIndicadorAutorizacaoCliente()));
            setRdoAutComplementarAg(String.valueOf(saidaDTOHistorico
                .getCdIndicadorAutorizacaoComplemento()));
            setCboTipoDataControleFloating(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdTipoDataFloating())));
            setDsTipoDataControleFloating(saidaDTOHistorico.getDsTipoDataFloating());
            setRdoUtilizaListaDebitos(saidaDTOHistorico.getCdIndicadorListaDebito()
                .toString());
            setCboTipoFormacaoListaDebito(Long.valueOf(String.valueOf(saidaDTOHistorico.getCdTipoFormacaoLista())));
            setDsTipoFormacaoListaDebito(saidaDTOHistorico.getDsTipoFormacaoLista());
            setCboTratamentoListaDebitoSemNumeracao(Long.valueOf(String
                .valueOf(saidaDTOHistorico.getCdTipoConsistenciaLista())));
            setDsTratamentoListaDebitoSemNumeracao(saidaDTOHistorico
                .getDsTipoConsistenciaLista());
            setRdoGerarRetornoOperRealizadaInternet(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));
            setCboTipoConsolidacaoPagamentosComprovante(Long.valueOf(String
                .valueOf(saidaDTOHistorico.getCdTipoConsultaComprovante())));
            setDsTipoConsolidacaoPagamentosComprovante(saidaDTOHistorico
                .getDsTipoConsolidacaoComprovante());
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));

            if (!String.valueOf(saidaDTOHistorico.getCdFormaManutencao())
                            .equalsIgnoreCase("4")) {
                setRdoEfetuarManutencaoCadastroProcuradores("1");
            }
            
            setDsCodIndicadorTipoRetornoInternet(saidaDTOHistorico.getDsCodIdentificadorTipoRetornoInternet());
        }

        // Pagamento de Sal�rios
        if (dto.getCdParametroTela() == 2) {
            setCdEmiteCartao(saidaDTOHistorico.getCdIndicadorCartaoSalario());
            setCdTipoCartao(saidaDTOHistorico.getCdTipoCartaoSalario());
            setQtddLimite(String.valueOf(saidaDTOHistorico
                .getQuantidadeSolicitacaoCartao()));
            SimpleDateFormat formatDiaMesAno = new SimpleDateFormat(
            "dd.MM.yyyy");
            try {
                setDataLimite(formatDiaMesAno.parse(saidaDTOHistorico
                    .getDtEnquaContaSalario()));
            } catch (ParseException e) {
                setDataLimite(null);
            }

            // novas
            setCboFormaEnvioPagamento(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdFormaEnvioPagamento())));
            setDsFormaEnvioPagamento(saidaDTOHistorico.getDsFormaEnvioPagamento());
            setCboFormaAutPagamentos(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdFormaAutorizacaoPagamento())));
            setDsFormaAutPagamentos(saidaDTOHistorico.getDsFormaAutorizacaoPagamento());
            //setRdoPreAutorizacaoCliente(String.valueOf(saidaDTO
            //		.getCdIndicadorAutorizacaoCliente()));
            setRdoAutComplementarAg(String.valueOf(saidaDTOHistorico
                .getCdIndicadorAutorizacaoComplemento()));
            setCboTipoDataControleFloating(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdTipoDataFloating())));
            setDsTipoDataControleFloating(saidaDTOHistorico.getDsTipoDataFloating());
            setRdoUtilizaListaDebitos(saidaDTOHistorico.getCdIndicadorListaDebito()
                .toString());
            setCboTipoFormacaoListaDebito(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdTipoFormacaoLista())));
            setDsTipoFormacaoListaDebito(saidaDTOHistorico.getDsTipoFormacaoLista());
            setCboTratamentoListaDebitoSemNumeracao(Long.valueOf(String
                .valueOf(saidaDTOHistorico.getCdTipoConsistenciaLista())));
            setDsTratamentoListaDebitoSemNumeracao(saidaDTOHistorico
                .getDsTipoConsistenciaLista());
            setRdoGerarRetornoOperRealizadaInternet(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));
            setDtLimiteEnqConvContaSal(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTOHistorico.getDtEnquaContaSalario()));
            setQtddLimiteCartaoContaSalarioSolicitacao(saidaDTOHistorico
                .getQuantidadeSolicitacaoCartao());
            setCboTipoConsolidacaoPagamentosComprovante(Long.valueOf(String
                .valueOf(saidaDTOHistorico.getCdTipoConsultaComprovante())));
            setDsTipoConsolidacaoPagamentosComprovante(saidaDTOHistorico
                .getDsTipoConsolidacaoComprovante());
            setCboTipoCartaoContaSalario(Long.valueOf(saidaDTOHistorico
                .getCdTipoCartaoSalario()));
            setDsTipoCartaoContaSalario(saidaDTOHistorico.getDsTipoCartaoSalario());
            setRdoEmissaoAntCartaoContSal(String.valueOf(saidaDTOHistorico
                .getCdIndicadorCartaoSalario()));
            setRdoAbertContaBancoPostBradSeg(String.valueOf(saidaDTOHistorico
                .getCdIndicadorBancoPostal()));
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));
            setCdFloatServicoContrato(saidaDTOHistorico.getCdFloatServicoContrato());
            setDsTipoDataFloating(saidaDTOHistorico.getDsTipoDataFloating());
            if (!String.valueOf(saidaDTOHistorico.getCdFormaManutencao())
                            .equalsIgnoreCase("4")) {
                setRdoEfetuarManutencaoCadastroProcuradores("1");
            }
            setDsCodIndicadorTipoRetornoInternet(descricaoTipoManutencao);
        }

        // Pagamentos de Tributos e Contas de Consumo
        if (dto.getCdParametroTela() == 3) {
            setCdUtilizacaoRastreamentoDebito(saidaDTOHistorico
                .getCdConsultaDebitoVeiculo());
            setDsPeriodicidadeRastreamento(saidaDTOHistorico
                .getDsPeriodicidadeConsultaVeiculo());
            setCdPeriodicidadeRastreamento(saidaDTOHistorico
                .getCdPeriodicidadeConsultaVeiculo());
            setCdAgendDebito(saidaDTOHistorico.getCdAgendamentoDebitoVeiculo());
            setDsPeriodicidadeCobranca(saidaDTOHistorico
                .getDsPeriodicidadeCobrancaTarifa());
            setCdPeriodicidadeCobranca(saidaDTOHistorico
                .getCdPeriodicidadeCobrancaTarifa());
            setDiaFechamentoApuracao(String.valueOf(saidaDTOHistorico
                .getNrFechamentoApuracaoTarifa()));
            setQtddDiasCobrancaTarifa(String.valueOf(saidaDTOHistorico
                .getQuantidadeDiaCobrancaTarifa()));
            setCdTipoReajusteTarifa(saidaDTOHistorico.getCdTipoReajusteTarifa());
            setQtddMesesReajuste(String.valueOf(saidaDTOHistorico
                .getQuantidadeMesReajusteTarifa()));
            setIndiceEconomicoReajuste(String.valueOf(saidaDTOHistorico
                .getCdIndicadorEconomicoReajuste()));
            setTipoAlimentacao(String.valueOf(saidaDTOHistorico
                .getCdFormaEnvioPagamento()));
            setCdTipoAutorizacaoPagamento(saidaDTOHistorico
                .getCdFormaAutorizacaoPagamento());
            setRdoGerarRetornoOperRealizadaInternet(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));
            setCboTipoDataControleFloating(Long.parseLong(String
                .valueOf(saidaDTOHistorico.getCdTipoDataFloating())));
            setDsTipoDataControleFloating(saidaDTOHistorico.getDsTipoDataFloating());
            setRdoUtilizaListaDebitos(saidaDTOHistorico.getCdIndicadorListaDebito()
                .toString());
            setCboTipoFormacaoListaDebito(Long.parseLong(String
                .valueOf(saidaDTOHistorico.getCdTipoFormacaoLista())));
            setDsTipoFormacaoListaDebito(saidaDTOHistorico.getDsTipoFormacaoLista());
            setCboTratamentoListaDebitoSemNumeracao(Long.parseLong(String
                .valueOf(saidaDTOHistorico.getCdTipoConsistenciaLista())));
            setDsTratamentoListaDebitoSemNumeracao(saidaDTOHistorico
                .getDsTipoConsistenciaLista());
            setCboTipoConsolidacaoPagamentosComprovante(Long.parseLong(String
                .valueOf(saidaDTOHistorico.getCdTipoConsultaComprovante())));
            setDsTipoConsolidacaoPagamentosComprovante(saidaDTOHistorico
                .getDsTipoConsolidacaoComprovante());
            setRdoPesquisaDe(String.valueOf(saidaDTOHistorico
                .getCdConsultaDebitoVeiculo()));
            setCboPeriodicidadePesquisaDebitosPendentesVeiculos(saidaDTOHistorico
                .getCdPeriodicidadeConsultaVeiculo());
            setDsPeriodicidadePesquisaDebitosPendentesVeiculos(saidaDTOHistorico
                .getDsPeriodicidadeConsultaVeiculo());
            setRdoAgendamentoDebitosPendentesVeiculos(String.valueOf(saidaDTOHistorico
                .getCdAgendamentoDebitoVeiculo()));
            setDsFormaEnvioPagamento(saidaDTOHistorico.getDsFormaEnvioPagamento());
            setDsFormaAutPagamentos(saidaDTOHistorico.getDsFormaAutorizacaoPagamento());
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));
            setDsIndicadorUtilizaMora(saidaDTOHistorico.getDsIndicadorUtilizaMora());
            if (!String.valueOf(saidaDTOHistorico.getCdFormaManutencao())
                            .equalsIgnoreCase("4")) {
                setRdoEfetuarManutencaoCadastroProcuradores("1");
            }
            setDsCodIndicadorTipoRetornoInternet(descricaoTipoManutencao);
        }

        // Pagamento de Benef�cios
        if (dto.getCdParametroTela() == 4) {
            setCdInformaAgenciaContaCredito(saidaDTOHistorico
                .getCdDisponibilizacaoContaCredito());
            setCdTipoIdenBeneficio(saidaDTOHistorico.getCdTipoIdentificacaoBeneficio());
            setCdUtilizaCadOrgPagadores(saidaDTOHistorico
                .getCdIndicadorCadastroorganizacao());
            setCdManCadastroProcuradores(saidaDTOHistorico
                .getCdIndicadorCadastroProcurador());
            setDsPeriodicidadeManutencao(saidaDTOHistorico
                .getDsPeriodicidadeManutencaoProcd());
            setCdPeriodicidadeManutencao(saidaDTOHistorico
                .getCdPeriodicidadeManutencaoProcd());
            setCdFormaManutencaoCadastroProcurador(saidaDTOHistorico
                .getCdFormaManutencao());
            setCdPossuiExpiracaoCredito(saidaDTOHistorico.getCdindicadorExpiraCredito());
            setCdFormaControleExpiracao(saidaDTOHistorico.getCdFormaExpiracaoCredito());
            setQtddDiasExpiracao(String.valueOf(saidaDTOHistorico
                .getQuantidadeDiaExpiracao()));
            setCdTratFeriadoFimVigencia(saidaDTOHistorico.getCdCreditoNaoUtilizado());
            setCdTipoPrestContaCreditoPago(saidaDTOHistorico
                .getCdMomentoCreditoEfetivacao());

            // novas
            setCboFormaEnvioPagamento(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdFormaEnvioPagamento())));
            setDsFormaEnvioPagamento(saidaDTOHistorico.getDsFormaEnvioPagamento());
            setCboFormaAutPagamentos(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdFormaAutorizacaoPagamento())));
            setDsFormaAutPagamentos(saidaDTOHistorico.getDsFormaAutorizacaoPagamento());
            //setRdoPreAutorizacaoCliente(String.valueOf(saidaDTO
            //		.getCdIndicadorAutorizacaoCliente()));
            setRdoAutComplementarAg(String.valueOf(saidaDTOHistorico
                .getCdIndicadorAutorizacaoComplemento()));
            setCboTipoDataControleFloating(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdTipoDataFloating())));
            setDsTipoDataControleFloating(saidaDTOHistorico.getDsTipoDataFloating());
            setRdoUtilizaListaDebitos(String.valueOf(saidaDTOHistorico
                .getCdIndicadorListaDebito()));
            setCboTipoFormacaoListaDebito(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdTipoFormacaoLista())));
            setDsTipoFormacaoListaDebito(saidaDTOHistorico.getDsTipoFormacaoLista());
            setCboTratamentoListaDebitoSemNumeracao(Long.valueOf(String
                .valueOf(saidaDTOHistorico.getCdTipoConsistenciaLista())));
            setDsTratamentoListaDebitoSemNumeracao(saidaDTOHistorico
                .getDsTipoConsistenciaLista());
            setRdoGerarRetornoOperRealizadaInternet(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));
            setCboTipoConsolidacaoPagamentosComprovante(Long.valueOf(String
                .valueOf(saidaDTOHistorico.getCdTipoConsultaComprovante())));
            setDsTipoConsolidacaoPagamentosComprovante(saidaDTOHistorico
                .getDsTipoConsolidacaoComprovante());
            setCboTipoIdentificacaoBeneficiario(Long.valueOf(saidaDTOHistorico
                .getCdTipoIdentificacaoBeneficio()));
            setDsTipoIdentificacaoBeneficiario(saidaDTOHistorico
                .getDsTipoIdentificacaoBeneficio());
            setRdoUtilizaCadastroOrgaosPagadores(String.valueOf(saidaDTOHistorico
                .getCdIndicadorCadastroorganizacao()));
            setRdoUtilizaCadastroProcuradores(String.valueOf(saidaDTOHistorico
                .getCdIndicadorCadastroProcurador()));
            setCboFormaManCadProcurador(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdFormaManutencao())));
            setDsFormaManCadProcurador(saidaDTOHistorico.getDsFormaManutencao());
            setCboPeriocidadeManCadProc(saidaDTOHistorico
                .getCdPeriodicidadeManutencaoProcd());
            setDsPeriocidadeManCadProc(saidaDTOHistorico
                .getDsPeriodicidadeManutencaoProcd());
            setRdoInformaAgenciaContaCred(String.valueOf(saidaDTOHistorico
                .getCdDisponibilizacaoContaCredito()));
            setRdoPossuiExpiracaoCredito(String.valueOf(saidaDTOHistorico
                .getCdindicadorExpiraCredito()));
            setCboContExpCredConta(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdFormaExpiracaoCredito())));
            setDsContExpCredConta(saidaDTOHistorico.getDsFormaExpiracaoCredito());
            setQtddDiasExpiracaoCredito(String.valueOf(saidaDTOHistorico
                .getQuantidadeDiaExpiracao()));
            setCboMomentoIndCredEfetivado(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdMomentoCreditoEfetivacao())));
            setDsMomentoIndCredEfetivado(saidaDTOHistorico
                .getDsMomentoCreditoEfetivacao());
            setCboTratamentoFeriadoFimVigenciaCredito(Long.valueOf(String
                .valueOf(saidaDTOHistorico.getCdCreditoNaoUtilizado())));
            setDsTratamentoFeriadoFimVigenciaCredito(saidaDTOHistorico
                .getDsCreditoNaoUtilizado());
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));

            if (!String.valueOf(saidaDTOHistorico.getCdFormaManutencao())
                            .equalsIgnoreCase("4")) {
                setRdoEfetuarManutencaoCadastroProcuradores("1");
            }
        }

        // Cadastro de Favorecido
        if (dto.getCdParametroTela() == 5) {
            setCdFormaManutencaoCadastroProcurador(saidaDTOHistorico
                .getCdFormaManutencao());
            setQtddDiasInativacao(String.valueOf(saidaDTOHistorico
                .getQuantidadeDiaInatividadeFavorecido()));
            setCdConsistenciaCpfCnpfFavorecido(saidaDTOHistorico
                .getCdCctciaInscricaoFavorecido());

            // Novas
            setCboFormaManCadFavorecido(Long.valueOf(saidaDTOHistorico
                .getCdFormaManutencao()));
            setDsFormaManCadFavorecido(saidaDTOHistorico.getDsFormaManutencao());
            setQtddDiasInativacaoFavorecido(saidaDTOHistorico
                .getQuantidadeDiaInatividadeFavorecido());
            setCboTipoConsistenciaInscricaoFavorecido(Long.valueOf(saidaDTOHistorico
                .getCdCctciaInscricaoFavorecido()));
            setDsTipoConsistenciaInscricaoFavorecido(saidaDTOHistorico
                .getDsCctciaInscricaoFavorecido());
            setRdoGerarRetornoOperRealizadaInternet(saidaDTOHistorico
                .getCdIndicadorRetornoInternet() == null ? "" : saidaDTOHistorico
                    .getCdIndicadorRetornoInternet().toString());
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));

        }

        // Aviso de Movimenta��o de D�bito
        // Aviso de Movimenta��o de Cr�dito
        // Aviso de Movimenta��o ao Pagador
        // Aviso de Movimenta��o ao Favorecido
        // Comprovante Pagamento ao Pagador
        // Comprovante Pagamento ao Favorecido
        if (dto.getCdParametroTela() >= 6 && dto.getCdParametroTela() <= 11) {
            setCdPeriodicidade(saidaDTOHistorico.getCdPeriodicidadeAviso());
            setDsPeriodicidade(saidaDTOHistorico.getDsPeriodicidadeAviso());
            setCdDestino(saidaDTOHistorico.getCdDestinoAviso());
            setCdPermiteCorrespAberta(saidaDTOHistorico.getCdEnvelopeAberto());
            setCdDemonsInfReservada(saidaDTOHistorico.getCdAreaReservada());
            setCdAgruparCorrespondencia(saidaDTOHistorico.getCdAgrupamentoAviso());
            setDsAgruparCorrespondencia(saidaDTOHistorico.getDsAgrupamentoAviso());
            setQuantidadeVias(String.valueOf(saidaDTOHistorico.getQuantidadeViaAviso()));

            // novas
            setCboPeriodicidadeEmissao(Integer.valueOf(String.valueOf(saidaDTOHistorico
                .getCdPeriodicidadeAviso())));
            setDsPeriodicidadeEmissao(saidaDTOHistorico.getDsPeriodicidadeAviso());
            setQtddViasEmitir(saidaDTOHistorico.getQuantidadeViaAviso());
            setCboDestinoEnvio(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdDestinoAviso())));
            setDsDestinoEnvio(saidaDTOHistorico.getDsDestinoAviso());
            setCboCadConsultaEndEnvio(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdConsultaEndereco())));
            setDsCadConsultaEndEnvio(saidaDTOHistorico.getDsConsultaEndereco());
            setRdoPermiteCorrespondenciaAberta(String.valueOf(saidaDTOHistorico
                .getCdEnvelopeAberto()));
            setRdoAgruparCorresp(String.valueOf(saidaDTOHistorico
                .getCdAgrupamentoAviso()));
            setTextoInformacaoAreaReservada(saidaDTOHistorico.getDsAreaReservada2());
            setRdoDemonstraInfAreaRes(String.valueOf(saidaDTOHistorico
                .getCdAreaReservada()));
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));
        }

        // Emiss�o Comprovantes Diversos
        // Emiss�o Comprovante Salarial
        if (dto.getCdParametroTela() == 12 || dto.getCdParametroTela() == 13) {
            setCdTipoRejeicao(saidaDTOHistorico.getCdRejeicaoLote());
            setCdTipoEmissao(saidaDTOHistorico.getCdDisponibilizacaoDiversoCriterio());
            setCdMidiaDisponivelCorrentista(saidaDTOHistorico.getCdMidiaDisponivel());
            setCdMidiaDisponivelNaoCorrentista(saidaDTOHistorico
                .getCdDisponibilizacaoDiversoNao());
            setCdTipoEnvioCorrespondencia(saidaDTOHistorico.getCdDestinoComprovante());
            setCdFrasesPreCadastradas(saidaDTOHistorico.getCdFrasePrecadastrada());
            setCdCobrarTarifasFuncionarios(saidaDTOHistorico.getCdCobrancaTarifa());
            setQtddMesesEmissao(String.valueOf(saidaDTOHistorico
                .getQuantidadeMesComprovante()));
            setQtddLinhas(String.valueOf(saidaDTOHistorico.getQuantidadeLimiteLinha()));
            setQtddVias(String.valueOf(saidaDTOHistorico.getQuantidadeViaComprovante()));
            setQtddViasPagas(String.valueOf(saidaDTOHistorico
                .getQuantidadeViaCombranca()));
            setCdFormularioImpressao(saidaDTOHistorico.getCdTipoLayoutArquivo());
            setCdPeriodicidadeCobrancaTarifa(saidaDTOHistorico
                .getCdPeriodicidadeCobrancaTarifa());
            setDsPeriodicidadeCobrancaTarifa(saidaDTOHistorico
                .getDsPeriodicidadeCobrancaTarifa());
            setFechamentoApuracao(String.valueOf(saidaDTOHistorico
                .getNrFechamentoApuracaoTarifa()));
            setDiasCobrancaAposApuracao(String.valueOf(saidaDTOHistorico
                .getQuantidadeDiaCobrancaTarifa()));
            setCdTipoReajuste(saidaDTOHistorico.getCdTipoReajusteTarifa());
            setDsPeriodicidadeReajusteTarifa(saidaDTOHistorico
                .getDsPeriodicidadeReajusteTarifa());
            setCdPeriodicidadeReajusteTarifa(saidaDTOHistorico
                .getCdPeriodicidadeReajusteTarifa());
            setMesesReajusteAutomatico(String.valueOf(saidaDTOHistorico
                .getQuantidadeMesReajusteTarifa()));
            setCdIndiceReajuste(saidaDTOHistorico.getCdIndicadorEconomicoReajuste());
            setPercentualReajuste(saidaDTOHistorico.getPercentualIndiceReajusteTarifa());
            setBonificacaoTarifa(saidaDTOHistorico.getPeriodicidadeTarifaCatalogo());

            if (getCdFormularioImpressao() != null) {
                setDsFormularioImpressao(listaTipoLayoutArquivoHash
                    .get(getCdFormularioImpressao()));
            }
            if (getCdIndiceReajuste() != null) {
                setDsIndiceReajuste(listaIndiceEconomicoHash
                    .get(getCdIndiceReajuste()));
            }

            // novas
            setCboTipoRejeicaoLote(Long.valueOf(saidaDTOHistorico
                .getCdRejeicaoAgendamentoLote()));
            setDsTipoRejeicaoLote(saidaDTOHistorico.getDsRejeicaoLote());
            setCboMeioDisponibilizacaoCorrentista(new Long(saidaDTOHistorico
                .getCdDisponibilizacaoSalarioCriterio()));
            setDsMeioDisponibilizacaoCorrenstista(saidaDTOHistorico
                .getDsDisponibilizacaoSalarioCriterio());
            setCboMidiaDispCorrentista(Long.valueOf(saidaDTOHistorico
                .getCdMidiaDisponivel()));
            setDsMidiaDispCorrentista(saidaDTOHistorico.getDsMidiaDisponivel());
            setCboMeioDisponibilizacaoNaoCorrentista(new Long(saidaDTOHistorico
                .getCdDisponibilizacaoSalarioNao()));
            setDsMeioDisponibilizacaoNaoCorrenstista(saidaDTOHistorico
                .getDsDisponibilizacaoSalarioNao());
            setCboDestinoEnvioCorresp(Long.valueOf(saidaDTOHistorico
                .getCdDestinoComprovante()));
            setDsDestinoEnvioCorresp(saidaDTOHistorico.getDsDestinoComprovante());
            setCboCadConsultaEndEnvio(Long.valueOf(saidaDTOHistorico
                .getCdConsultaEndereco()));
            setDsCadConsultaEndEnvio(saidaDTOHistorico.getDsConsultaEndereco());
            setRdoUtilizaFrasesPreCadastradas(String.valueOf(saidaDTOHistorico
                .getCdFrasePrecadastrada()));
            setCobrarTarifasFunc(String.valueOf(saidaDTOHistorico.getCdCobrancaTarifa()));
            setQuantidadeMesesEmissao(saidaDTOHistorico.getQuantidadeMesComprovante());
            setQtddViasEmitir(saidaDTOHistorico.getQuantidadeViaComprovante());
            setQtddViasPagasRegistro(saidaDTOHistorico.getQuantidadeViaCombranca());
            setQtddLinhasPorComprovante(saidaDTOHistorico.getQuantidadeLimiteLinha());
            setRdoGerarRetornoOperRealizadaInternet(saidaDTOHistorico
                .getCdIndicadorRetornoInternet().toString());
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));

        }

        // Recadastramento Benefici�rio
        if (dto.getCdParametroTela() == 14) {
            setCdTipoIdentificadorBeneficiario(saidaDTOHistorico
                .getCdTipoIdentificacaoBeneficio());
            setCdTipoConsistenciaIdentificador(saidaDTOHistorico
                .getCdCctciaIdentificacaoBeneficio());
            setCdCondicaoEnquadramento(saidaDTOHistorico
                .getCdCriterioEnquandraBeneficio());
            setCdCriterioPrincipalEnquadramento(saidaDTOHistorico
                .getCdPrincipalEnquaRecadastramento());
            setCdCriterioCompostoEnquadramento(saidaDTOHistorico
                .getCdCriterioEnquadraRecadastramento());
            setQuantidadeEtapasRecadastramento(saidaDTOHistorico
                .getQuantidadeEtapaRecadastramentoBeneficio());
            setQtddFasesPorEtapa(saidaDTOHistorico
                .getQuantidadeFaseRecadastramentoBeneficio());
            setQtddMesesPorFase(saidaDTOHistorico.getQuantidadeMesFaseRecadastramento());
            setQtddMesesPorEtapa(saidaDTOHistorico
                .getQuantidadeMesEtapaRecadastramento());
            setCdPermiteEnvioRemessa(saidaDTOHistorico
                .getCdManutencaoBaseRecadastramento());
            setCdPeriodicidadeEnvioRemessa(saidaDTOHistorico
                .getCdPeriodicidadeEnvioRemessa());
            setDsPeriodicidadeEnvioRemessa(saidaDTOHistorico
                .getDsPeriodicidadeEnvioRemessa());
            setCdBaseDadosUtilizada(saidaDTOHistorico
                .getCdBaseRecadastramentoBeneficio());
            setCdTipoCargaBaseDadosUtilizada(saidaDTOHistorico
                .getCdTipoCargaRecadastramento());
            setCdPermiteAnteciparRecadastramento(saidaDTOHistorico
                .getCdAntecipacaoRecadastramentoBeneficiario());
            SimpleDateFormat formatDiaMesAno = new SimpleDateFormat(
            "dd.MM.yyyy");
            try {
                setDataLimiteInicioVinculoCargaBase(formatDiaMesAno
                    .parse(saidaDTOHistorico.getDtLimiteVinculoCarga()));
            } catch (ParseException e) {
                setDataLimiteInicioVinculoCargaBase(null);
            }
            try {
                setDataInicioRecadastramento(formatDiaMesAno.parse(saidaDTOHistorico
                    .getDtInicioRecadastramentoBeneficio()));
            } catch (ParseException e) {
                setDataInicioRecadastramento(null);
            }
            try {
                setDataFimRecadastramento(formatDiaMesAno.parse(saidaDTOHistorico
                    .getDtFimRecadastramentoBeneficio()));
            } catch (ParseException e) {
                setDataFimRecadastramento(null);
            }
            setCdPermiteAcertosDados(saidaDTOHistorico.getCdAcertoDadosRecadastramento());
            try {
                setDataInicioPeriodoAcertoDados(formatDiaMesAno.parse(saidaDTOHistorico
                    .getDtInicioAcertoRecadastramento()));
            } catch (ParseException e) {
                setDataInicioPeriodoAcertoDados(null);
            }
            try {
                setDataFimPeriodoAcertoDados(formatDiaMesAno.parse(saidaDTOHistorico
                    .getDtFimAcertoRecadastramento()));
            } catch (ParseException e) {
                setDataFimPeriodoAcertoDados(null);
            }
            setCdEmiteMsgRecadastramentoMidiaOnline(saidaDTOHistorico
                .getCdMensagemRecadastramentoMidia());
            setCdMidiaMsgRecadastramentoOnline(saidaDTOHistorico
                .getCdMidiaMensagemRecadastramento());
            setDsPeriodicidadeCobrancaTarifa(saidaDTOHistorico
                .getDsPeriodicidadeCobrancaTarifa());
            setCdPeriodicidadeCobrancaTarifa(saidaDTOHistorico
                .getCdPeriodicidadeCobrancaTarifa());
            setDiaFechamentoApuracaoTarifaMensal(String.valueOf(saidaDTOHistorico
                .getNrFechamentoApuracaoTarifa()));
            setQtddDiasCobrancaTarifaApuracao(String.valueOf(saidaDTOHistorico
                .getQuantidadeDiaCobrancaTarifa()));
            setDsPeriodicidadeReajusteTarifa(saidaDTOHistorico
                .getDsPeriodicidadeReajusteTarifa());
            setCdPeriodicidadeReajusteTarifa(saidaDTOHistorico
                .getCdPeriodicidadeReajusteTarifa());
            setCdTipoReajusteTarifa(saidaDTOHistorico.getCdTipoReajusteTarifa());
            setQtddMesesReajusteAutomaticoTarifa(String.valueOf(saidaDTOHistorico
                .getQuantidadeMesReajusteTarifa()));
            setDsIndiceEconomicoReajusteTarifa(saidaDTOHistorico
                .getDsIndicadorEconomicoReajuste());
            setCdIndiceEconomicoReajusteTarifa(saidaDTOHistorico
                .getCdIndicadorEconomicoReajuste());
            setPercentualIndiceEconomicoReajusteTarifa(saidaDTOHistorico
                .getPercentualIndiceReajusteTarifa());
            setPercentualBonificacaoTarifaPadrao(saidaDTOHistorico
                .getPeriodicidadeTarifaCatalogo());

            // novas
            setDsTipoIdentificacaoBeneficiario(saidaDTOHistorico
                .getDsTipoIdentificacaoBeneficio());
            setDsTipoConsistenciaIdentificacaoBeneficiario(saidaDTOHistorico
                .getDsCctciaIdentificacaoBeneficio());
            setCboCondicaoEnquadramento(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdPrincipalEnquaRecadastramento())));
            setDsCondicaoEnquadramento(saidaDTOHistorico
                .getDsPrincipalEnquaRecadastramento());
            setCboCritPrincEnquadramento(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdPrincipalEnquaRecadastramento())));
            setDsCritPrincEnquadramento(saidaDTOHistorico
                .getDsPrincipalEnquaRecadastramento());
            setCboCritCompEnquadramento(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdCriterioEnquandraBeneficio())));
            setDsCritCompEnquadramento(saidaDTOHistorico
                .getDsCriterioEnquandraBeneficio());
            setCboCadUtilizadoRec(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdBaseRecadastramentoBeneficio())));
            setDsCadUtilizadoRec(saidaDTOHistorico.getDsBaseRecadastramentoBeneficio());
            setDtInicioRecadastramento(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTOHistorico
                    .getDtInicioRecadastramentoBeneficio()));
            setDtFimRecadastramento(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTOHistorico
                    .getDtFimRecadastramentoBeneficio()));
            setRdoPermiteEnvioRemessaManutencaoCadastro(String.valueOf(saidaDTOHistorico
                .getCdManutencaoBaseRecadastramento()));
            setCboPeriodicidadeEnvioRemessaManutencao(saidaDTOHistorico
                .getCdPeriodicidadeEnvioRemessa());
            setDsPeriodicidadeEnvioRemessaManutencao(saidaDTOHistorico
                .getDsPeriodicidadeEnvioRemessa());
            setCboTipoCargaCadastroBeneficiario(Long.valueOf(String
                .valueOf(saidaDTOHistorico.getCdTipoCargaRecadastramento())));
            setDsTipoCargaCadastroBeneficiario(saidaDTOHistorico
                .getDsTipoCargaRecadastramento());
            setDtLimiteVincClienteBenef(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTOHistorico
                    .getDtInicioRastreabilidadeTitulo()));
            setRdoPermiteAcertosDados(String.valueOf(saidaDTOHistorico
                .getCdAcertoDadosRecadastramento()));
            setRdoPermiteAnteciparRecadastramento(String.valueOf(saidaDTOHistorico
                .getCdAntecipacaoRecadastramentoBeneficiario()));
            setDtInicioPerAcertoDados(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTOHistorico
                    .getDtInicioAcertoRecadastramento()));
            setDtFimPerAcertoDados(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTOHistorico
                    .getDtFimAcertoRecadastramento()));
            setRdoEmiteMsgRecadMidia(String.valueOf(saidaDTOHistorico
                .getCdMensagemRecadastramentoMidia()));
            setCboMidiaMsgRecad(Long.valueOf(String.valueOf(saidaDTOHistorico
                .getCdMidiaMensagemRecadastramento())));
            setDsMidiaMsgRecad(saidaDTOHistorico.getDsMidiaMensagemRecadastramento());
            setRdoGerarRetornoOperRealizadaInternet(saidaDTOHistorico
                .getCdIndicadorRetornoInternet() == null ? "" : saidaDTOHistorico
                    .getCdIndicadorRetornoInternet().toString());
            setRdoGerarRetornoSeparadoCanal(String.valueOf(saidaDTOHistorico
                .getCdIndicadorRetornoInternet()));
        }

        // TRILHA DE AUDITORIA
        if ("".equals(saidaDTOHistorico.getCdUsuarioInclusao())
                        || saidaDTOHistorico.getCdUsuarioInclusao() == null) {
            setUsuarioInclusao(saidaDTOHistorico.getCdUsuarioExternoInclusao());
        } else {
            setUsuarioInclusao(saidaDTOHistorico.getCdUsuarioInclusao());
        }

        if ("".equals(saidaDTOHistorico.getCdUsuarioAlteracao())
                        || saidaDTOHistorico.getCdUsuarioAlteracao() == null) {
            setUsuarioManutencao(String.valueOf(saidaDTOHistorico
                .getCdUsuarioExternoAlteracao()));
        } else {
            setUsuarioManutencao(saidaDTOHistorico.getCdUsuarioAlteracao());
        }

        setComplementoInclusao(saidaDTOHistorico.getNrOperacaoFluxoInclusao() == null
            || saidaDTOHistorico.getNrOperacaoFluxoInclusao().equals("0")
            ? ""
                : saidaDTOHistorico.getNrOperacaoFluxoInclusao());
        setComplementoManutencao(saidaDTOHistorico.getNrOperacaoFluxoAlteracao() == null
            || saidaDTOHistorico.getNrOperacaoFluxoAlteracao().equals("0")
            ? ""
                : saidaDTOHistorico.getNrOperacaoFluxoAlteracao());
        setDataHoraInclusao(saidaDTOHistorico.getHrManutencaoRegistroInclusao());
        setDataHoraManutencao(saidaDTOHistorico.getHrManutencaoRegistroAlteracao());
        setTipoCanalInclusao(saidaDTOHistorico.getCdCanalInclusao() == 0 ? "" : saidaDTOHistorico
            .getCdCanalInclusao()
            + " - " + saidaDTOHistorico.getDsCanalInclusao());
        setTipoCanalManutencao(saidaDTOHistorico.getCdCanalAlteracao() == 0
            ? ""
                : saidaDTOHistorico.getCdCanalAlteracao() + " - "
                + saidaDTOHistorico.getDsCanalAlteracao());
        setCdAmbienteServicoContrato(saidaDTOHistorico.getCdAmbienteServicoContrato());

        setDsSituacaoServicoRelacionado(saidaDTOHistorico
            .getDsSituacaoServicoRelacionado());

        setRdtoCadastroFloating(PgitUtil.verificaIntegerNulo(saidaDTOHistorico
            .getCdFloatServicoContrato()));

        if (getRdtoCadastroFloating() != null && getRdtoCadastroFloating() == 1) {
            setDescRdtoCadastroFloating("Sim");
        } else {
            setDescRdtoCadastroFloating("N�o");
        }
    }

    /**
     * Checks if is habilita botoes.
     *
     * @return true, if is habilita botoes
     */
    public boolean isHabilitaBotoes(){
        if(getItemSelecionadoLista() != null &&
                        getListaGridPesquisa().get(getItemSelecionadoLista()).getCdParametroTela() == 13){
            return true;
        }else if (getItemSelecionadoLista() != null){
            return false;
        }
        return true;
    }

    /**
     * Verifica modalidade.
     */
    public void verificaModalidade() {
        setPossuiModalidade(false);
        if (getItemSelecionadoLista() != null) {
            if (getListaGridPesquisa().get(getItemSelecionadoLista())
                            .getQtdeModalidade().intValue() > 0) {
                setPossuiModalidade(true);
            }
        }
    }

    /**
     * Modalidade.
     * 
     * @return the string
     */
    public String modalidade() {
        ListarModalidadeTipoServicoSaidaDTO modalidadeTipoServicoSaidaDTO = 
            getListaGridPesquisa().get(getItemSelecionadoLista());

        setCdTipoServicoSelecionado(modalidadeTipoServicoSaidaDTO.getCdModalidade());
        setDsTipoServicoSelecionado(modalidadeTipoServicoSaidaDTO.getDsModalidade());
        setCdParametroTela2(modalidadeTipoServicoSaidaDTO.getCdParametroTela());

        setHabilitaCampoTipServicoForncSalTrib(getCdParametroTela2().equals(TIPO_SERVICO_PAGAMENTO_FORNECEDOR)
            || getCdParametroTela2().equals(TIPO_SERVICO_PAGAMENTO_SALARIO)
            || getCdParametroTela2().equals(TIPO_SERVICO_PAGAMENTO_TRIBUTOS));

        carregaListaModalidade();
        setItemSelecionadoLista2(null);
        return "MODALIDADE";
    }

    /**
     * Ambiente.
     * 
     * @return the string
     */
    public String ambiente() {
        carregarComboTipoServico();
        setListaAmbienteControle(new ArrayList<SelectItem>());
        setItemSelecionadoGridAmbiente(null);
        limparDadosAmbienteOperacao();
        preencherComboTipoServico();

        return "AMBIENTE";
    }

    /**
     * Preencher combo tipo servico.
     */
    private void preencherComboTipoServico() {
        if (listaGridPesquisa != null && !listaGridPesquisa.isEmpty()) {
            if (itemSelecionadoLista != null) {
                setTipoServicoSelecionado(listaGridPesquisa.get(
                    itemSelecionadoLista).getCdModalidade());
            } else {
                setTipoServicoSelecionado(0);
            }
        } else {
            setTipoServicoSelecionado(0);
        }
    }

    /**
     * Pesquisar participantes incluir.
     * 
     * @param e
     *            the e
     */
    public void pesquisarParticipantesIncluir(ActionEvent e) {
        consultarParticipantes();
    }

    /**
     * Consultar participantes.
     * 
     * @return the string
     */
    public String consultarParticipantes() {
        listaGridParticipantes = new ArrayList<ListarParticipantesSaidaDTO>();
        try {

            ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();

            entradaDTO.setCdPessoaJuridica(getCdPessoaJuridica());
            entradaDTO.setCdTipoContrato(getCdTipoContrato());
            entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());

            setListaGridParticipantes(getManterVincPerfilTrocaArqPartImpl()
                .listarParticipantes(entradaDTO));

            this.listaGridControleParticipantes = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaGridParticipantes().size(); i++) {
                listaGridControleParticipantes.add(new SelectItem(i, " "));
            }

            setItemSelecionadoGridParticipantes(null);
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridParticipantes(null);
            setItemSelecionadoGridParticipantes(null);
        }

        return "SEL_PARTICIPANTE";
    }

    /**
     * Selecionar participante.
     * 
     * @return the string
     */
    public String selecionarParticipante() {
        participanteSelecionado = new ListarParticipantesSaidaDTO();
        participanteSelecionado = getListaGridParticipantes().get(
            getItemSelecionadoGridParticipantes());
        setNomeRazaoParticipanteSelecionado(participanteSelecionado
            .getNomeRazao());
        setCpfCnpjParticipanteSelecionado(participanteSelecionado.getCpf());
        return "CONFIRMAR";
    }

    /**
     * Selecionar participante historico.
     * 
     * @return the string
     */
    public String selecionarParticipanteHistorico() {
        participanteSelecionadoHistorico = new ListarParticipantesSaidaDTO();
        participanteSelecionadoHistorico = getListaGridParticipantes().get(
            getItemSelecionadoGridParticipantes());
        return "CONFIRMAR";
    }

    /**
     * Paginar lista ambiente.
     * 
     * @param e
     *            the e
     */
    public void paginarListaAmbiente(ActionEvent e) {
        consultarListaAmbiente();
    }

    /**
     * Consultar lista ambiente.
     */
    public void consultarListaAmbiente() {
        try {

            ListarAmbienteEntradaDTO entradaDTO = new ListarAmbienteEntradaDTO();
            listaAmbiente = new ArrayList<ListarAmbienteOcorrenciaSaidaDTO>();

            // Participante selecionado

            if (participanteSelecionado != null) {
                entradaDTO.setCdPessoa(participanteSelecionado
                    .getCdParticipante());
            }

            if (tipoServicoSelecionado.compareTo(0) == 0) {
                entradaDTO.setCdProdutoOperacaoRelacionamento(0);
            } else {
                entradaDTO
                .setCdProdutoOperacaoRelacionamento(listaComboTipoServicoHash
                    .get(tipoServicoSelecionado)
                    .getCdProdutoOperacaoRelacionado());
            }

            entradaDTO.setCdPessoaJuridica(getCdPessoaJuridica());
            entradaDTO.setCdTipoContrato(getCdTipoContrato());
            entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
            entradaDTO.setDsAmbiente(getDsAmbienteServico());

            setListaAmbiente(getManterAmbienteOperacaoContratoServiceImpl()
                .listarAmbiente(entradaDTO));

            listaAmbienteControle = new ArrayList<SelectItem>();

            for (int i = 0; i < listaAmbiente.size(); i++) {
                listaAmbienteControle.add(new SelectItem(i, ""));
            }

            setItemSelecionadoGridAmbiente(null);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaAmbienteControle(null);
            setItemSelecionadoLista(null);

        }
    }

    /**
     * Carrega lista modalidade.
     */
    public void carregaListaModalidade() {
        listaGridPesquisa2 = new ArrayList<ListarModalidadeTipoServicoSaidaDTO>();
        ListarModalidadeTipoServicoSaidaDTO modalidadeTipoServicoSaidaDTO = 
            getListaGridPesquisa().get(getItemSelecionadoLista());
        setCdTipoServicoSelecionado(modalidadeTipoServicoSaidaDTO.getCdModalidade());

        ListarModalidadeTipoServicoEntradaDTO entradaDTO = new ListarModalidadeTipoServicoEntradaDTO();
        entradaDTO.setCdPessoaJuridica(getCdPessoaJuridica());
        entradaDTO.setCdTipoContrato(getCdTipoContrato());
        entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
        entradaDTO.setCdTipoServico(getCdTipoServicoSelecionado());
        entradaDTO.setCdModalidade(0);

        setListaGridPesquisa2(getManterContratoService().listarModalidadeTipoServico(entradaDTO));
        setListaControleRadio2(SelectItemUtils.criarSelectItems(listaGridPesquisa2));
        setItemSelecionadoLista2(null);
    }

    /**
     * Incluir modalidade.
     * 
     * @return the string
     */
    public String incluirModalidade() {
        ListarServicoRelacionadoCdpsEntradaDTO entradaDTO = new ListarServicoRelacionadoCdpsEntradaDTO();
        entradaDTO.setCdTipoServico(getCdTipoServicoSelecionado());

        setCountCheckedModalidade(0);
        setBtnIncluir(false);

        identificacaoClienteContratoBean = (IdentificacaoClienteContratoBean) FacesUtils
        .getManagedBean("identificacaoClienteContratoBean");
        manterContratoBean = (ManterContratoBean) FacesUtils
        .getManagedBean("manterContratoBean");

        try {
            entradaServicosModalidades
            .setCdPessoaJuridicaContrato(identificacaoClienteContratoBean
                .getEmpresaGestoraFiltro());
            entradaServicosModalidades
            .setCdTipoContrato(identificacaoClienteContratoBean
                .getTipoContratoFiltro());
            entradaServicosModalidades.setNumeroContrato(manterContratoBean
                .getNumeroContrato());
            entradaServicosModalidades.setCdTipoServico(listaGridPesquisa.get(
                itemSelecionadoLista != null ? itemSelecionadoLista : 0)
                .getCdModalidade());

            listaGridModalidadeRelacionada = manterContratoService
            .listarTipoModalidadesRelacionadasIncluir(entradaServicosModalidades);

            listaControleCheckModalidadeRelacionada = new ArrayList<SelectItem>();

            for (int i = 0; i < listaGridModalidadeRelacionada.size(); i++) {
                this.listaControleCheckModalidadeRelacionada
                .add(new SelectItem(i, " "));
            }
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return "";
        }

        return "INCLUIR_MODALIDADE";
    }

    /**
     * Atualizar count checked modalidade relacionada.
     */
    public void atualizarCountCheckedModalidadeRelacionada() {
        this.countCheckedModalidade = 0;
        this.btnIncluir = false;

        for (int i = 0; i < this.listaGridModalidadeRelacionada.size(); i++) {
            if (this.listaGridModalidadeRelacionada.get(i).isCheck()) {
                countCheckedModalidade++;
                btnIncluir = true;
            }
        }
    }

    /**
     * Incluir modalidade avancar.
     * 
     * @return the string
     */
    public String incluirModalidadeAvancar() {
        listaCheckedGridModalidadeRelacionada = new ArrayList<ListarServicoRelacionadoCdpsSaidaDTO>();
        for (int i = 0; i < listaGridModalidadeRelacionada.size(); i++) {
            if (listaGridModalidadeRelacionada.get(i).isCheck()) {
                ListarServicoRelacionadoCdpsSaidaDTO registroSelecionadoDTO = listaGridModalidadeRelacionada
                .get(i);
                this.listaCheckedGridModalidadeRelacionada
                .add(registroSelecionadoDTO);

            }
        }
        if (this.listaCheckedGridModalidadeRelacionada.size() > INC_SERV_MANT_CONTR_MAX_MODALIDADE) {
            return "";
        } else {
            return "INCLUIR_MODALIDADE_AVANCAR";
        }
    }

    /**
     * Confirmar inc mod.
     * 
     * @return the string
     */
    public String confirmarIncMod() {
        try {
            IncluirTipoServModContratoEntradaDTO entradaDTO = new IncluirTipoServModContratoEntradaDTO();

            List<Integer> servicosSelecionados = new ArrayList<Integer>();
            List<ListarServicosSaidaDTO> listaModalidades = new ArrayList<ListarServicosSaidaDTO>();

            entradaDTO.setCdPessoaJuridicaContrato(this.cdPessoaJuridica);
            entradaDTO.setCdTipoContrato(this.cdTipoContrato);
            entradaDTO.setNrSequenciaContrato(this.nrSequenciaContrato);
            entradaDTO
            .setNrQtdeModalidades(listaCheckedGridModalidadeRelacionada == null
                ? 0
                    : listaCheckedGridModalidadeRelacionada.size());

            // sempre ser� 0 quando vier da modalidade
            entradaDTO.setNrQtdeServicos(0);
            entradaDTO.setListaServicos(servicosSelecionados);

            for (ListarServicoRelacionadoCdpsSaidaDTO occur : getListaCheckedGridModalidadeRelacionada()) {
                ListarServicosSaidaDTO modalidade = new ListarServicosSaidaDTO();
                modalidade.setCdTipoServico(occur.getCdModalidade());
                modalidade.setCdServico(occur.getCdRelacionamentoProduto());
                listaModalidades.add(modalidade);
            }

            entradaDTO.setListaModalidade(listaModalidades);

            IncluirTipoServModContratoSaidaDTO saidaDTO = this.manterContratoService
            .incluirTipoServModContrato(entradaDTO);

            entradaDTO.setNrQtdeModalidades(listaCheckedGridModalidade == null
                ? 0
                    : listaCheckedGridModalidade.size());

            BradescoFacesUtils.addInfoModalMessage(
                "(" + saidaDTO.getCodMensagem() + ") "
                + saidaDTO.getMensagem(),
                "modServicosManterContrato",
                BradescoViewExceptionActionType.ACTION, false);
            carregaListaModalidade();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return null;
        }

        return "";
    }

    /**
     * Excluir modalidade.
     * 
     * @return the string
     */
    public String excluirModalidade() {
        ListarModalidadeTipoServicoSaidaDTO modalidadeTipoServicoSaidaDTO = this.listaGridPesquisa2
        .get(this.itemSelecionadoLista2);
        setDsModalidade(modalidadeTipoServicoSaidaDTO.getDsModalidade());
        preencheDadosModalidade();

        return "EXCLUIR_MODALIDADE";
    }

    /**
     * Configurar modalidade.
     * 
     * @return the string
     */
    
    
    public String configurarModalidade() {
        try {
            listarCombosModalidade();
            carregaListaPeriodicidade();
            limparConfiguracaoMod();
            carregarListaConsistenciaCpfCnpjBenefAvalNpc();
            modalidadeTipoServicoSaidaDTO = this.listaGridPesquisa2
            .get(this.itemSelecionadoLista2);
            setCdParametroTela(modalidadeTipoServicoSaidaDTO
                .getCdParametroTela());
            carregaCamposConfigurar();
            controlarCamposLote();
            controlarCampoPercLote();
            controlarCampoQtdeLote();
            controlarCampoValorMaximoFavNaoCad();
            controlarCamposEmiteAvisos();
            controlarCampoQtdeLimDiasPgtoVenc();
            carregaComboFeriadoLocal();

            configuraComboFeriadoNacional();
            habilitaCamposFloatingLancamento();

            if ("S".equals(saidaVerificarAtributo.getCdIndicadorLancamentoPagamento())){
                setDesabilitaGerarLancamentoProgramado(false);
            } else {
                setDesabilitaGerarLancamentoProgramado(true);
            }

            if (saidaVerificarAtributo.getCdIndicadorPrioridadeEfetivacaoPagamento() != null &&
                            !saidaVerificarAtributo.getCdIndicadorPrioridadeEfetivacaoPagamento().equals("S")) {
                setDesabilitarComboPrioridadeDebito(true);
            }

            if (saidaVerificarAtributo.getCdIndicadorConsSaldoPagamento() != null &&
                            !saidaVerificarAtributo.getCdIndicadorConsSaldoPagamento().equals("S")) {
                setDesabilitarComboTipoConsultaSaldo(true);
            }

            if (getCdParametroTela() < 21 || getCdParametroTela() == 24
                            || getCdParametroTela() == 25) {
                return "CONFIGURAR_MODALIDADE1";
            } else if (getCdParametroTela() < 27 && getCdParametroTela() != 25) {
                return "CONFIGURAR_MODALIDADE2";
            } else {
                return "CONFIGURAR_MODALIDADE3";
            }
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }
        return "";
    }

    /**
     * Configura combo feriado nacional.
     */
    private void configuraComboFeriadoNacional() {
        if (cdParametroTela2.equals(1) || cdParametroTela2.equals(2)) {
            if (isCiclicoOrDiario()) {

                reorganizarItensComboFeriadoNacional(ANTECIPAR.getCodigoFeriadoNacional(), 
                    POSTERGAR.getCodigoFeriadoNacional(), null);
            }
        } else if (cdParametroTela2.equals(3)) {
            if (isCiclicoOrDiario()) {
                reorganizarItensComboFeriadoNacional(ANTECIPAR.getCodigoFeriadoNacional(), 
                    POSTERGAR.getCodigoFeriadoNacional(), REJEITAR.getCodigoFeriadoNacional());
            } else {
                reorganizarItensComboFeriadoNacional(REJEITAR.getCodigoFeriadoNacional(), 
                    ACATAR.getCodigoFeriadoNacional(), null);
            }
        }

    }

    /**
     * Checks if is ciclico or diario.
     *
     * @return true, if is ciclico or diario
     */
    private boolean isCiclicoOrDiario() {
        return cboProcessamentoEfetivacaoPagamento.equals(CICLICO
            .getCodigoTipoProcessamento())
            || cboProcessamentoEfetivacaoPagamento.equals(DIARIO
                .getCodigoTipoProcessamento());
    }

    /**
     * Reorganizar itens combo feriado nacional.
     *
     * @param item1 the item1
     * @param item2 the item2
     */
    private void reorganizarItensComboFeriadoNacional(Integer item1,
        Integer item2, Integer item3) {

        List<ConsultarListaValoresDiscretosSaidaDTO> lista = new ArrayList<ConsultarListaValoresDiscretosSaidaDTO>();
        lista.add(listaFeriadoNacional.get(item1 - 1));

        if (item2 != null) {
            lista.add(listaFeriadoNacional.get(item2 - 1));
        }

        if (item3 != null) {
            lista.add(listaFeriadoNacional.get(item3 - 1));
        }

        setListaTratamentoFeriadosDataPagamento(new ArrayList<SelectItem>());
        setListaTratamentoFeriadosDataPagamentoHash(new HashMap<Long, String>());

        adicionaComboFeriadoNacional(lista);

    }

    /**
     * Adiciona combo feriado nacional.
     *
     * @param lista the lista
     */
    private void adicionaComboFeriadoNacional(
        List<ConsultarListaValoresDiscretosSaidaDTO> lista) {
        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTratamentoFeriadosDataPagamento().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTratamentoFeriadosDataPagamentoHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }
    
    private void adicionaComboFeriadoNacional1(
            List<ConsultarListaValoresDiscretosSaidaDTO> lista) {
            for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
                getListaTratamentoFeriadosDataPagamento().add(
                    new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
                getListaTratamentoFeriadosDataPagamentoHash().put(
                    combo.getCdCombo(), combo.getDsCombo());
            }
        }

    /**
     * Carrega campos configurar.
     */
    public void carregaCamposConfigurar() {
    	
        preencheDadosModalidade();

        if ("AGENCIA ESPECIFICA".equalsIgnoreCase(getDsLocalEmissao())) {
            setCdLocalEmissao(1);
        } else if ("QUALQUER AGENCIA".equalsIgnoreCase(getDsLocalEmissao())) {
            setCdLocalEmissao(2);
        } else {
            setCdLocalEmissao(0);
        }
    }

    /**
     * Voltar modalidade.
     * 
     * @return the string
     */
    public String voltarModalidade() {
        if (getCdParametroTela() < 21 || getCdParametroTela() == 24
                        || getCdParametroTela() == 25) {
            return "VOLTAR";
        } else if (getCdParametroTela() < 27 && getCdParametroTela() != 25) {
            return "VOLTAR2";
        } else {
            return "VOLTAR3";
        }
    }

    /**
     * Imprimir relatorio modalidades.
     * 
     * @return the string
     */
    public String imprimirRelatorioModalidades() {

        String caminho = "/images/Bradesco_logo.JPG";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext
        .getExternalContext().getContext();
        String logoPath = servletContext.getRealPath(caminho);

        // Par�metros do Relat�rio
        Map<String, Object> par = new HashMap<String, Object>();

        ManterContratoBean manterContratoBeanAux = (ManterContratoBean) FacesUtils
        .getManagedBean("manterContratoBean");

        par.put("tipo", manterContratoBeanAux.getTipo());
        par.put("empresa", manterContratoBeanAux.getEmpresa());
        par.put("numero", manterContratoBeanAux.getNumero());
        par.put("descContrato", manterContratoBeanAux.getDescricaoContrato());
        par.put("logoBradesco", logoPath);
        par.put("tipoServico", dsTipoServicoSelecionado);

        par.put("titulo", "Pagamento Integrado - Servi�os - Modalidades");

        try {

            // M�todo que gera o relat�rio PDF.
            PgitUtil
            .geraRelatorioPdf(
                "/relatorios/manutencaoContrato/manterContrato/modalidades",
                "imprimirModalidades", getListaGridPesquisa2(), par);

        } /* Exce��o do relat�rio */
        catch (Exception e) {
            throw new BradescoViewException(e.getMessage(), e, "", "",
                BradescoViewExceptionActionType.ACTION);
        }

        return "";
    }

    /**
     * Limpar configuracao mod.
     */
    public void limparConfiguracaoMod() {
        limparConfiguracao();

    }

    /**
     * Detalhar modalidade.
     * 
     * @return the string
     */
    public String detalharModalidade() {
        ListarModalidadeTipoServicoSaidaDTO modalidadeTipoServicoSaidaDTO = this.listaGridPesquisa2
        .get(this.itemSelecionadoLista2);
        setDsModalidade(modalidadeTipoServicoSaidaDTO.getDsModalidade());
        setCdParametroTela(modalidadeTipoServicoSaidaDTO.getCdParametroTela());
        carregaListaTipoConsultaSaldo(false);
        preencheDadosModalidade();
        if (getCdParametroTela() < 26) {
            return "DETALHAR_MODALIDADE";
        } else {
            return "DETALHAR_MODALIDADE2";
        }

    }

    /**
     * Detalhar ambiente operacao.
     * 
     * @return the string
     */
    public String detalharAmbienteOperacao() {
        DetalharAmbienteEntradaDTO entrada = new DetalharAmbienteEntradaDTO();
        DetalharAmbienteSaidaDTO saidaDTO = new DetalharAmbienteSaidaDTO();

        entrada.setCdPessoa(listaAmbiente.get(itemSelecionadoGridAmbiente)
            .getCdPessoa());
        entrada.setCdPessoaJuridica(getCdPessoaJuridica());
        entrada.setCdProdutoOperacaoRelacionamento(listaAmbiente.get(
            itemSelecionadoGridAmbiente).getCdProdutoOperacaoRelacionado());
        entrada.setCdProdutoServicoOperacao(listaAmbiente.get(
            itemSelecionadoGridAmbiente).getCdProdutoServicoOperacao());
        entrada.setCdRelacionamentoProdutoProduto(listaAmbiente.get(
            itemSelecionadoGridAmbiente)
            .getCdRelacionamentoProdutoProduto());
        entrada.setCdTipoContrato(getCdTipoContrato());
        entrada.setCdTipoParticipacaoPessoa(listaAmbiente.get(
            itemSelecionadoGridAmbiente).getCdTipoParticipacaoPessoa());
        entrada.setNrSequenciaContrato(getNrSequenciaContrato());

        saidaDTO = manterAmbienteOperacaoContratoServiceImpl
        .detalharAmbiente(entrada);

        setDsTipoServicoAmbiente(listaAmbiente.get(itemSelecionadoGridAmbiente)
            .getDsTipoServico());
        setDsAmbienteServico(listaAmbiente.get(itemSelecionadoGridAmbiente)
            .getDsAmbienteOperacao());

        setDsNomeRazaoSocilaAmbiente(listaAmbiente.get(
            itemSelecionadoGridAmbiente).getDsNomeRazaoSocial());

        setCpfCnpjAmbiente(listaAmbiente.get(itemSelecionadoGridAmbiente)
            .getCpfCnpjFormatado());

        // TRILHA DE AUDITORIA
        setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
        setUsuarioManutencao(saidaDTO.getCdUsuarioManutencao());

        setComplementoInclusao("0"
            .equals(saidaDTO.getNmOperacaoFluxoInclusao()) ? "" : saidaDTO
                .getNmOperacaoFluxoInclusao());
        setComplementoManutencao("0".equals(saidaDTO
            .getNmOperacaoFluxoManutencao()) ? "" : saidaDTO
                .getNmOperacaoFluxoManutencao());
        setDataHoraInclusao(saidaDTO.getHrManutencaRegistroInclusao());
        setDataHoraManutencao(saidaDTO.getHrManutencaoRegistroManuntencao());
        setTipoCanalInclusao(saidaDTO.getCdCanalInclusao() == 0 ? "" : saidaDTO
            .getCdCanalInclusao()
            + " - " + saidaDTO.getDsCanalInclusao());
        setTipoCanalManutencao(saidaDTO.getCdCanalManutencao() == 0
            ? ""
                : saidaDTO.getCdCanalManutencao() + " - "
                + saidaDTO.getDsCanalManutencao());
        return "DETALHAR";

    }

    /**
     * Limpar modalidade.
     */
    public void limparModalidade() {
        setItemSelecionadoLista2(null);
    }

    /**
     * Salvar configuracao mod.
     * 
     * @return the string
     */
    
   
    public String salvarConfiguracaoMod() {

        Locale brasil = new Locale("pt", "BR");
        DecimalFormat df = new DecimalFormat("#,##0.00",
            new DecimalFormatSymbols(brasil));
        df.setParseBigDecimal(true);

        if (getCdParametroTela() < 26) {
        	setDsOrigem(listaOrigemHash.get(getTesteOrigem()));
            setDsProcessamentoEfetivacaoPagamento(listaProcessamentoEfetivacaoPagamentoHash.get(getCboProcessamentoEfetivacaoPagamento()));
            setDsTipoRejAgendamento(listaTipoRejeicaoAgendamentoHash.get(getCboTipoRejeicaoAgendamento()));
            setDsTipoRejEfetivacao(listaTipoRejeicaoEfetivacaoHash.get(getCboTipoRejeicaoEfetivacao()));
            setDsPrioridDebito(listaPrioridadeDebitoHash.get(getCboPrioridadeDebito()));
            setDsPermiteEstornoPagamento(listaPermiteEstornoPagamentoHash.get(getCboPermiteEstornoPagamento()));
            setDsTipoConsSaldo(listaTipoConsultaSaldoHash.get(getCboTipoConsultaSaldo()));
            setDsTipoConsistenciaCpfCnpjFavorecido(listaTipoConsistenciaCpfCnpjFavorecidoHash.get(getCboTipoConsistenciaCpfCnpjFavorecido()));
            setDsTratFeriadosDataPagamento(listaTratamentoFeriadosDataPagamentoHash.get(getCboTratamentoFeriadosDataPagamento()));
            setDsIndicadorFeriadoLocal(feriadosLocaisHash.get(getCdIndicadorFeriadoLocal()));
            setDsOcorrenciaDebito(listaOcorrenciaDebitoHash.get(getCboOcorrenciaDebito()));
            setDsTipoInscricaoFavorecido(listaTipoInscricaoFavorecidoHash.get(getCboTipoInscricaoFavorecido()));
            setDsDestinoEnvio(listaDestinoEnvioHash.get(getCboDestinoEnvio()));
            setDsMomentoEnvio(listaMomentoEnvioHash.get(getCboMomentoEnvio()));
            setDsCadConsultaEndEnvio(listaCadastroConsultaEnderecoEnvioHash.get(getCboCadConsultaEndEnvio()));
            setDsTipoTratamentoContaTransferida(listaTipoTratamentoContaTransferidaHash.get(getCboTipoTratamentoContaTransferida()));
            setDsRdoIndicadorSegundaLinhaExtrato(listaDemonstra2LinhaExtratoHash.get(Long.valueOf(getRdoIndicadorSegundaLinhaExtrato())));

            setDsPeriodicidadeEnvio(getCdPeriodicidadeEnvio() != null
                && getCdPeriodicidadeEnvio() != 0 ? (String) listaPeriodicidadeHash.get(getCdPeriodicidadeEnvio()) : "");
            setValorMaxPagFavNaoCadastradoDesc(getValorMaxPagFavNaoCadastrado() != null
                ? df.format(getValorMaxPagFavNaoCadastrado()) : "");
            setDsValorLimiteIndividual(getValorLimiteIndividualFiltro() != null
                ? df.format(getValorLimiteIndividualFiltro()) : "");
            setDsValorLimiteDiario(getValorLimiteDiarioFiltro() != null ? df
                .format(getValorLimiteDiarioFiltro()) : "");
            setDsConsultaSaldoValorSuperior(SelectItemUtils
                .obterLabelSelecItem(listaTipoConsultaSaldo,
                    cdConsultaSaldoValorSuperior));
            
            setDsConsistenciaCpfCnpjBenefAvalNpc(listaConsistenciaCpfCnpjBenefAvalNpcHash.get(getCboConsistenciaCpfCnpjBenefAvalNpc()));
         
            return "SALVAR_CONFIGURACAO_MOD";
        } else {
            setDsProcessamentoEfetivacaoPagamento(listaProcessamentoEfetivacaoPagamentoHash
                .get(getCboProcessamentoEfetivacaoPagamento()));
            setDsTipoRejAgendamento(listaTipoRejeicaoAgendamentoHash
                .get(getCboTipoRejeicaoAgendamento()));
            setDsTipoRejEfetivacao(listaTipoRejeicaoEfetivacaoHash
                .get(getCboTipoRejeicaoEfetivacao()));
            setDsPrioridDebito(listaPrioridadeDebitoHash
                .get(getCboPrioridadeDebito()));
            setDsTipoConsSaldo(listaTipoConsultaSaldoHash
                .get(getCboTipoConsultaSaldo()));
            setDsTipoConsistenciaCpfCnpjFavorecido(listaTipoConsistenciaCpfCnpjFavorecidoHash
                .get(getCboTipoConsistenciaCpfCnpjFavorecido()));
            setDsTratFeriadosDataPagamento(listaTratamentoFeriadosDataPagamentoHash
                .get(getCboTratamentoFeriadosDataPagamento()));
            setDsTratamentoValorDivergente(listaTratamentoValorDivergenteHash
                .get(getCboTratamentoValorDivergente()));
            setDsTipoConsistenciaCpfCnpjProprietario(listaTipoConsistenciaCpfCnpjProprietarioHash
                .get(getCboTipoConsistenciaCpfCnpjProprietario()));
            setDsAcaoParaNaoComprovacao(listaAcaoNaoComprovacaoHash
                .get(getCboAcaoParaNaoComprovacao()));
            setDsDestinoEnvio(listaDestinoEnvioHash.get(getCboDestinoEnvio()));
            setDsCadConsultaEndEnvio(listaCadastroConsultaEnderecoEnvioHash
                .get(getCboCadConsultaEndEnvio()));
            setDsTipoRastreamentoTitulo(listaTipoRastreamentoTitulosHash
                .get(getCboTipoRastreamentoTitulo()));
            setDsIndicadorFeriadoLocal(feriadosLocaisHash
                .get(getCdIndicadorFeriadoLocal()));

            setDsDataInicioBloqueioPapeleta(getDataInicioBloqueioPapeleta() != null
                ? FormatarData
                    .formataDiaMesAno(getDataInicioBloqueioPapeleta())
                    : "");
            setDsDataInicioRastreamento(getDataInicioRastreamento() != null
                ? FormatarData
                    .formataDiaMesAno(getDataInicioRastreamento())
                    : "");
            setDsDataRegistroTitulo(getDataRegistroTitulo() != null
                ? FormatarData.formataDiaMesAno(getDataRegistroTitulo())
                    : "");
            setValorMaxPagFavNaoCadastradoDesc(getValorMaxPagFavNaoCadastrado() != null
                ? df.format(getValorMaxPagFavNaoCadastrado())
                    : "");
            setDsValorLimiteIndividual(getValorLimiteIndividualFiltro() != null
                ? df.format(getValorLimiteIndividualFiltro())
                    : "");
            setDsValorLimiteDiario(getValorLimiteDiarioFiltro() != null ? df
                .format(getValorLimiteDiarioFiltro()) : "");
            setDsConsultaSaldoValorSuperior(SelectItemUtils
                .obterLabelSelecItem(listaTipoConsultaSaldo,
                    cdConsultaSaldoValorSuperior));
            return "SALVAR_CONFIGURACAO_MOD2";
        }
    }

    /**
     * Confirmar configuracao mod.
     * 
     * @return the string
     */
    public String confirmarConfiguracaoMod() {
        try {

            AlterarConfiguracaoTipoServModContratoEntradaDTO entradaDTO = new AlterarConfiguracaoTipoServModContratoEntradaDTO();
            ListarModalidadeTipoServicoSaidaDTO modalidadeSaidaDTO = getListaGridPesquisa2()
            .get(getItemSelecionadoLista2());
            ListarModalidadeTipoServicoSaidaDTO tipoServicoSaidaDTO = getListaGridPesquisa()
            .get(getItemSelecionadoLista());

            // Dados comuns tanto de tipo de servi�o quanto de modalidade
            entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
            entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato());
            entradaDTO.setNrSequenciaContratoNegocio(getNrSequenciaContrato());
            entradaDTO.setCdTipoServico(tipoServicoSaidaDTO.getCdModalidade());
            entradaDTO.setCdModalidade(modalidadeSaidaDTO.getCdModalidade());
            entradaDTO.setCdParametro(2);
            entradaDTO.setCdParametroTela(modalidadeSaidaDTO
                .getCdParametroTela());
            
            entradaDTO.setCindcdFantsRepas(Integer.parseInt(String.valueOf(getTesteOrigem())));

            // Aviso de Recadastramento
            if (modalidadeSaidaDTO.getCdParametroTela() == 15) {
                entradaDTO.setCdMomentoAvisoRacadastro(Integer.valueOf(String
                    .valueOf(getCboMomentoEnvio())));
                entradaDTO.setQtAntecedencia(getQtddDiasAvisoAntecipado());
                entradaDTO.setCdDestinoAviso(Integer.valueOf(String
                    .valueOf(getCboDestinoEnvio())));
                entradaDTO.setCdConsEndereco(Integer.valueOf(String
                    .valueOf(getCboCadConsultaEndEnvio())));
                entradaDTO.setCdAgrupamentoAviso(Integer
                    .valueOf(getRdoPermitirAgrupamentoCorrespondencia()));
            }

            // Formul�rio de Recadastramento
            if (modalidadeSaidaDTO.getCdParametroTela() == 16) {
                entradaDTO.setCdMomentoFormularioRecadastro(Integer
                    .parseInt(String.valueOf(getCboMomentoEnvio())));
                entradaDTO
                .setQtAntecedencia(getQtddDiasEnvioAntecipadoFormulario());
                entradaDTO.setCdDestinoFormularioRecadastro(Integer
                    .parseInt(String.valueOf(getCboDestinoEnvio())));
                entradaDTO.setCdConsEndereco(Integer.parseInt(String
                    .valueOf(getCboCadConsultaEndEnvio())));
                entradaDTO.setCdFormularioContratoCliente(Integer
                    .parseInt(getCodigoFormulario()));
                entradaDTO.setCdAgrupamentoFormularioRecadastro(Integer
                    .parseInt(getRdoPermitirAgrupamentoCorrespondencia()));
            }

            // Pagamentos de Fornecedores - DOC
            // Pagamentos de Fornecedores - TED
            // Dep�sito Identificado
            // Ordem de Credito
            if (modalidadeSaidaDTO.getCdParametroTela() == 17
                            || modalidadeSaidaDTO.getCdParametroTela() == 18
                            || modalidadeSaidaDTO.getCdParametroTela() == 24
                            || modalidadeSaidaDTO.getCdParametroTela() == 25) {

                entradaDTO.setCdDiaFloatPagamento(getQtddDiasFloating());
                entradaDTO.setCdMomentoProcessamentoPagamento(Integer
                    .parseInt(getCboProcessamentoEfetivacaoPagamento()
                        .toString()));
                entradaDTO.setCdRejeicaoAgendaLote(Integer
                    .parseInt(getCboTipoRejeicaoAgendamento().toString()));
                entradaDTO.setCdRejeicaoEfetivacaoLote(Integer
                    .parseInt(getCboTipoRejeicaoEfetivacao().toString()));
                entradaDTO
                .setQtMaximaInconLote(getQtddMaximaRegistrosInconsistentesLote());
                entradaDTO
                .setCdPercentualMaximoInconLote(getPercMaxRegInconsistente());
                entradaDTO.setCdPrioridadeEfetivacaoPagamento(Integer
                    .parseInt(getCboPrioridadeDebito().toString()));
                entradaDTO.setCdValidacaoNomeFavorecido(Integer
                    .parseInt(getRdoValidarNomeFavorecidoReceitaFederal()));
                entradaDTO
                .setCdUtilizacaoFavorecidoControle(Integer
                    .parseInt(getRdoUtilizaCadastroFavorecidoControlePagamentos()));
                entradaDTO
                .setVlFavorecidoNaoCadastro(getValorMaximoPagamentoFavorecidoNaoCadastrado());
                entradaDTO.setCdConsSaldoPagamento(Integer
                    .parseInt(getCboTipoConsultaSaldo().toString()));
                entradaDTO.setQtDiaRepiqConsulta(getQtddDiasRepique());
                entradaDTO.setCdFavorecidoConsPagamento(Integer
                    .parseInt(getRdoPermiteFavorecidoConsultarPagamento()));
                entradaDTO.setCdLancamentoFuturoDebito(Integer
                    .parseInt(getRdoGerarLancFutDeb()));
                entradaDTO.setCdTipoContaFavorecido(Integer
                    .parseInt(getCboTipoConsistenciaCpfCnpjFavorecido()
                        .toString()));
                entradaDTO.setCdPagamentoNaoUtilizado(Integer
                    .parseInt(getCboTratamentoFeriadosDataPagamento()
                        .toString()));
                entradaDTO
                .setVlLimiteIndividualPagamento(getValorLimiteIndividual());
                entradaDTO.setVlLimiteDiaPagamento(getValorLimiteDiario());
                entradaDTO.setCdIndicadorSegundaLinha(getRdoIndicadorSegundaLinhaExtrato());
                entradaDTO.setCdIndicadorAgendaGrade(PgitUtil
                    .verificaStringToIntegerNula(getRdoPermiteAgAutGrade()));
            }

            // Pagamentos Via Ordem de Pagamento
            if (modalidadeSaidaDTO.getCdParametroTela() == 19) {

                entradaDTO.setCdDiaFloatPagamento(getQtddDiasFloating());
                entradaDTO.setCdMomentoProcessamentoPagamento(Integer
                    .parseInt(getCboProcessamentoEfetivacaoPagamento()
                        .toString()));
                entradaDTO.setCdRejeicaoAgendaLote(Integer
                    .parseInt(getCboTipoRejeicaoAgendamento().toString()));
                entradaDTO.setCdRejeicaoEfetivacaoLote(Integer
                    .parseInt(getCboTipoRejeicaoEfetivacao().toString()));
                entradaDTO
                .setQtMaximaInconLote(getQtddMaximaRegistrosInconsistentesLote());
                entradaDTO
                .setCdPercentualMaximoInconLote(getPercMaxRegInconsistente());
                entradaDTO.setCdPrioridadeEfetivacaoPagamento(Integer
                    .parseInt(getCboPrioridadeDebito().toString()));
                entradaDTO.setCdValidacaoNomeFavorecido(Integer
                    .parseInt(getRdoValidarNomeFavorecidoReceitaFederal()));
                entradaDTO
                .setCdUtilizacaoFavorecidoControle(Integer
                    .parseInt(getRdoUtilizaCadastroFavorecidoControlePagamentos()));
                entradaDTO
                .setVlFavorecidoNaoCadastro(getValorMaximoPagamentoFavorecidoNaoCadastrado());
                entradaDTO.setCdConsSaldoPagamento(Integer
                    .parseInt(getCboTipoConsultaSaldo().toString()));
                entradaDTO.setQtDiaRepiqConsulta(getQtddDiasRepique());
                entradaDTO.setCdFavorecidoConsPagamento(Integer
                    .parseInt(getRdoPermiteFavorecidoConsultarPagamento()));
                entradaDTO.setCdLancamentoFuturoDebito(Integer
                    .parseInt(getRdoGerarLancFutDeb()));
                entradaDTO.setCdTipoContaFavorecido(Integer
                    .parseInt(getCboTipoConsistenciaCpfCnpjFavorecido()
                        .toString()));
                entradaDTO.setCdPagamentoNaoUtilizado(Integer
                    .parseInt(getCboTratamentoFeriadosDataPagamento()
                        .toString()));
                entradaDTO.setCdMomentoDebitoPagamento(Integer.parseInt(String
                    .valueOf(getCboOcorrenciaDebito())));
                entradaDTO.setCdTipoIsncricaoFavorecido(Integer
                    .parseInt(getCboTipoInscricaoFavorecido().toString()));
                entradaDTO
                .setQtdeDiasExpiracaoCredito(getQuantidadeDiasExpiracao());
                entradaDTO
                .setVlLimiteIndividualPagamento(getValorLimiteIndividual());
                entradaDTO.setVlLimiteDiaPagamento(getValorLimiteDiario());
                entradaDTO.setCdLocalEmissao(getCdLocalEmissao());
                entradaDTO.setCdIndicadorSegundaLinha(getRdoIndicadorSegundaLinhaExtrato());
            }

            // Pagamento Via Cr�dito em Conta
            if (modalidadeSaidaDTO.getCdParametroTela() == 20) {

                entradaDTO.setCdDiaFloatPagamento(getQtddDiasFloating());
                entradaDTO.setCdMomentoProcessamentoPagamento(Integer
                    .parseInt(getCboProcessamentoEfetivacaoPagamento()
                        .toString()));
                entradaDTO.setCdRejeicaoAgendaLote(Integer
                    .parseInt(getCboTipoRejeicaoAgendamento().toString()));
                entradaDTO.setCdRejeicaoEfetivacaoLote(Integer
                    .parseInt(getCboTipoRejeicaoEfetivacao().toString()));
                entradaDTO
                .setQtMaximaInconLote(getQtddMaximaRegistrosInconsistentesLote());
                entradaDTO
                .setCdPercentualMaximoInconLote(getPercMaxRegInconsistente());
                entradaDTO.setCdPrioridadeEfetivacaoPagamento(Integer
                    .parseInt(getCboPrioridadeDebito().toString()));
                entradaDTO.setCdUtilizacaoFavorecidoControle(PgitUtil.verificaStringToIntegerNula(
                    getRdoUtilizaCadastroFavorecidoControlePagamentos()));
                entradaDTO
                .setVlFavorecidoNaoCadastro(getValorMaximoPagamentoFavorecidoNaoCadastrado());
                entradaDTO.setCdConsSaldoPagamento(Integer
                    .parseInt(getCboTipoConsultaSaldo().toString()));
                entradaDTO.setQtDiaRepiqConsulta(getQtddDiasRepique());
                entradaDTO.setCdFavorecidoConsPagamento(Integer
                    .parseInt(getRdoPermiteFavorecidoConsultarPagamento()));
                entradaDTO.setCdCtciaEspecieBeneficio(Integer
                    .parseInt(getRdoEfetuaConEspBenef()));
                entradaDTO.setCdTipoContaFavorecido(Integer
                    .parseInt(getCboTipoConsistenciaCpfCnpjFavorecido()
                        .toString()));
                entradaDTO.setCdLancamentoFuturoDebito(Integer
                    .parseInt(getRdoGerarLancFutDeb()));
                entradaDTO.setCdLancamentoFuturoCredito(Integer
                    .parseInt(getRdoGerarLancFutCred()));
                entradaDTO.setCdIndicadorLancamentoPagamento(isHabilitaCampo()
                    ? Integer.parseInt(getRdoGerarLancProgramado())
                        : 2);
                entradaDTO.setCdPagamentoNaoUtilizado(Integer
                    .parseInt(getCboTratamentoFeriadosDataPagamento()
                        .toString()));
                entradaDTO.setCdContratoContaTransferencia(Integer
                    .parseInt(getCboTipoTratamentoContaTransferida()
                        .toString()));
                entradaDTO.setCdMeioPagamentoCredito(Integer
                    .parseInt(getCboTipoContaCredito().toString()));
                entradaDTO.setCdValidacaoNomeFavorecido(Integer
                    .parseInt(getRdoValidarNomeFavorecidoReceitaFederal()));
                entradaDTO.setCdLiberacaoLoteProcesso(Integer
                    .parseInt(getRdoExigeLibLoteProcessado()));
                entradaDTO
                .setVlLimiteIndividualPagamento(getValorLimiteIndividual());
                entradaDTO.setVlLimiteDiaPagamento(getValorLimiteDiario());
                entradaDTO.setCdIndLancamentoPersonalizado(Integer
                    .parseInt(getRdoUtilizaLancPersonalizado()));
                entradaDTO.setCdIndicadorSegundaLinha(getRdoIndicadorSegundaLinhaExtrato());
                entradaDTO.setCdPreenchimentoLancamentoPersonalizado(PgitUtil
                    .verificaStringToIntegerNula(getRdoTipoFormatacaoPrimeiraLinha()));
            }

            // Pagamento Via D�bito em Conta
            if (modalidadeSaidaDTO.getCdParametroTela() == 21) {
                entradaDTO.setCdDiaFloatPagamento(getQtddDiasFloating());
                entradaDTO.setCdMomentoProcessamentoPagamento(Integer
                    .parseInt(getCboProcessamentoEfetivacaoPagamento()
                        .toString()));
                entradaDTO.setCdRejeicaoAgendaLote(Integer
                    .parseInt(getCboTipoRejeicaoAgendamento().toString()));
                entradaDTO.setCdRejeicaoEfetivacaoLote(Integer
                    .parseInt(getCboTipoRejeicaoEfetivacao().toString()));
                entradaDTO
                .setQtMaximaInconLote(getQtddMaximaRegistrosInconsistentesLote());
                entradaDTO
                .setCdPercentualMaximoInconLote(getPercMaxRegInconsistente());
                entradaDTO.setCdPrioridadeEfetivacaoPagamento(Integer
                    .parseInt(getCboPrioridadeDebito().toString()));
                entradaDTO.setCdValidacaoNomeFavorecido(Integer
                    .parseInt(getRdoValidarNomeFavorecidoReceitaFederal()));
                entradaDTO
                .setCdUtilizacaoFavorecidoControle(Integer
                    .parseInt(getRdoUtilizaCadastroFavorecidoControlePagamentos()));
                entradaDTO
                .setVlFavorecidoNaoCadastro(getValorMaximoPagamentoFavorecidoNaoCadastrado());
                entradaDTO.setCdConsSaldoPagamento(Integer
                    .parseInt(getCboTipoConsultaSaldo().toString()));
                entradaDTO.setQtDiaRepiqConsulta(getQtddDiasRepique());
                entradaDTO.setCdFavorecidoConsPagamento(Integer
                    .parseInt(getRdoPermiteFavorecidoConsultarPagamento()));
                entradaDTO.setCdPagamentoNaoUtilizado(Integer
                    .parseInt(getCboTratamentoFeriadosDataPagamento()
                        .toString()));
                // setRdoEfetuaConCpfCnpjFav(""); // n�o sei ainda com o que
                // preencher
                entradaDTO.setCdTipoContaFavorecido(Integer
                    .parseInt(getCboTipoConsistenciaCpfCnpjFavorecido()
                        .toString()));
                entradaDTO.setCdLancamentoFuturoDebito(Integer
                    .parseInt(getRdoGerarLancFutDeb()));
                entradaDTO.setCdLancamentoFuturoCredito(Integer
                    .parseInt(getRdoGerarLancFutCred()));
                entradaDTO.setRdoPermiteDebitoOnline(Integer
                    .parseInt(getRdoPermiteDebitoOnline()));
                entradaDTO
                .setVlLimiteIndividualPagamento(getValorLimiteIndividual());
                entradaDTO.setVlLimiteDiaPagamento(getValorLimiteDiario());
                entradaDTO.setCdIndicadorSegundaLinha(getRdoIndicadorSegundaLinhaExtrato());
            }

            // Pagamento Via T�tulo Bradesco
            // Pagamento Via T�tulo Outros Bancos
            if (modalidadeSaidaDTO.getCdParametroTela() == 22
                            || modalidadeSaidaDTO.getCdParametroTela() == 23) {

                entradaDTO.setCdDiaFloatPagamento(getQtddDiasFloating());
                entradaDTO.setCdMomentoProcessamentoPagamento(Integer
                    .parseInt(getCboProcessamentoEfetivacaoPagamento()
                        .toString()));
                entradaDTO.setCdRejeicaoAgendaLote(Integer
                    .parseInt(getCboTipoRejeicaoAgendamento().toString()));
                entradaDTO.setCdRejeicaoEfetivacaoLote(Integer
                    .parseInt(getCboTipoRejeicaoEfetivacao().toString()));
                entradaDTO
                .setQtMaximaInconLote(getQtddMaximaRegistrosInconsistentesLote());
                entradaDTO
                .setCdPercentualMaximoInconLote(getPercMaxRegInconsistente());
                entradaDTO.setCdPrioridadeEfetivacaoPagamento(Integer
                    .parseInt(getCboPrioridadeDebito().toString()));
                entradaDTO.setCdValidacaoNomeFavorecido(Integer
                    .parseInt(getRdoValidarNomeFavorecidoReceitaFederal()));
                entradaDTO
                .setCdUtilizacaoFavorecidoControle(Integer
                    .parseInt(getRdoUtilizaCadastroFavorecidoControlePagamentos()));
                entradaDTO
                .setVlFavorecidoNaoCadastro(getValorMaximoPagamentoFavorecidoNaoCadastrado());
                entradaDTO.setCdConsSaldoPagamento(Integer
                    .parseInt(getCboTipoConsultaSaldo().toString()));
                entradaDTO.setQtDiaRepiqConsulta(getQtddDiasRepique());
                entradaDTO.setCdFavorecidoConsPagamento(Integer
                    .parseInt(getRdoPermiteFavorecidoConsultarPagamento()));
                entradaDTO.setCdLancamentoFuturoDebito(Integer
                    .parseInt(getRdoGerarLancFutDeb()));
                entradaDTO.setCdTipoContaFavorecido(Integer
                    .parseInt(getCboTipoConsistenciaCpfCnpjFavorecido()
                        .toString()));
                entradaDTO.setCdPagamentoNaoUtilizado(Integer
                    .parseInt(getCboTratamentoFeriadosDataPagamento()
                        .toString()));
                entradaDTO.setCdAgendaPagamentoVencido(Integer
                    .parseInt(getRdoPermitePagarVencido()));
                entradaDTO.setCdAgendaValorMenor(Integer
                    .parseInt(getRdoPermitePagarMenor()));
                entradaDTO
                .setQtMaximaTituloVencido(getQuantidadeLimiteDiasPagamentoVencido());
                entradaDTO
                .setVlLimiteIndividualPagamento(getValorLimiteIndividual());
                entradaDTO.setVlLimiteDiaPagamento(getValorLimiteDiario());
                entradaDTO.setCdIndicadorSegundaLinha(getRdoIndicadorSegundaLinhaExtrato());
                entradaDTO.setCdTituloDdaRetorno(PgitUtil
                    .verificaStringToIntegerNula(getRdoIndicadorDdaRetorno()));
                entradaDTO.setVlPercentualDiferencaTolerada(getVlPercentualDiferencaTolerada());
                entradaDTO.setCdConsistenciaCpfCnpjBenefAvalNpc( Integer.parseInt(getCboConsistenciaCpfCnpjBenefAvalNpc().toString()));
                
            }

            // Rastreabilidade de T�tulos
            if (modalidadeSaidaDTO.getCdParametroTela() == 26) {
                entradaDTO
                .setCdCriterioRastreabilidadeTitulo(Integer
                    .valueOf(String
                        .valueOf(getCboTipoRastreamentoTitulo())));
                entradaDTO.setCdRastreabilidadeTituloTerceiro(Integer
                    .valueOf(getRdoRastrearTituloTerceiros()));
                entradaDTO.setCdRastreabilidadeNotaFiscal(Integer
                    .valueOf(getRdoRastrearNotasFiscais()));
                entradaDTO.setCdCapituloTituloRegistro(2);
                entradaDTO.setCdIndicadorAgendaTitulo(Integer
                    .valueOf(getRdoAgendarTituloRastProp()));
                entradaDTO.setCdAgendaRastreabilidadeFilial(Integer
                    .valueOf(getRdoAgendarTituloRastreadoFilial()));
                entradaDTO.setCdBloqueioEmissaoPplta(Integer
                    .valueOf(getRdoBloqEmissaoPapeleta()));
                entradaDTO.setCdIndicadorAdesaoSacador(Integer
                    .valueOf(getRdoAdesaoSacadoEletronico()));
                entradaDTO.setDtInicioRastreabilidadeTitulo(FormatarData
                    .formataDiaMesAnoToPdc(getDtInicioRastreamento()));
                entradaDTO.setDtInicioBloqueioPapeleta(FormatarData
                    .formataDiaMesAnoToPdc(getDtInicioBloqPapeleta()));
                entradaDTO.setCdExigeAutFilial(Integer.parseInt(getRdoExigeIdentificacaoFilialAutorizacao()));

            }

            // Pagamento Prova de Vida
            if (modalidadeSaidaDTO.getCdParametroTela() == 27) {

                entradaDTO.setCdAcaoNaoVida(Integer
                    .parseInt(getCboAcaoParaNaoComprovacao().toString()));
                entradaDTO
                .setQtMesComprovante(getQtddMesesPeriodicidadeComprovacao());
                entradaDTO.setCdDestinoComprovante(getCboDestinoEnvio() == null
                    ? 0
                        : Integer.parseInt(getCboDestinoEnvio().toString()));
                entradaDTO.setCdConsEndereco(Integer
                    .parseInt(getCboCadConsultaEndEnvio().toString()));
                entradaDTO.setCdIndicadorMensagemPerso(Integer
                    .parseInt(getRdoUtilizaMensagemPersonalizada()));
                entradaDTO
                .setQtAntecedencia(getQtddDiasEmissaoAvisoAntesInicioComprovacao());
                entradaDTO
                .setQtAnteriorVencimentoComprovado(getQtddDiasEmissaoAvisoAntesVencimento());
                entradaDTO.setCdFormularioContratoCliente(Integer
                    .parseInt(getCodigoFormulario()));

            }

            if (modalidadeSaidaDTO.getCdParametroTela() == 28
                            || modalidadeSaidaDTO.getCdParametroTela() == 29
                            || modalidadeSaidaDTO.getCdParametroTela() == 30
                            || modalidadeSaidaDTO.getCdParametroTela() == 31) {

                entradaDTO.setCdDiaFloatPagamento(getQtddDiasFloating());
                entradaDTO.setCdMomentoProcessamentoPagamento(Integer
                    .parseInt(getCboProcessamentoEfetivacaoPagamento()
                        .toString()));
                entradaDTO.setCdRejeicaoAgendaLote(Integer
                    .parseInt(getCboTipoRejeicaoAgendamento().toString()));
                entradaDTO.setCdRejeicaoEfetivacaoLote(Integer
                    .parseInt(getCboTipoRejeicaoEfetivacao().toString()));
                entradaDTO
                .setQtMaximaInconLote(getQtddMaximaRegistrosInconsistentesLote());
                entradaDTO
                .setCdPercentualMaximoInconLote(getPercMaxRegInconsistente());
                entradaDTO.setCdPrioridadeEfetivacaoPagamento(Integer
                    .parseInt(getCboPrioridadeDebito().toString()));
                entradaDTO.setCdValidacaoNomeFavorecido(Integer
                    .parseInt(getRdoValidarNomeFavorecidoReceitaFederal()));
                entradaDTO
                .setCdUtilizacaoFavorecidoControle(Integer
                    .parseInt(getRdoUtilizaCadastroFavorecidoControlePagamentos()));
                entradaDTO
                .setVlFavorecidoNaoCadastro(getValorMaximoPagamentoFavorecidoNaoCadastrado());
                entradaDTO.setCdConsSaldoPagamento(Integer
                    .parseInt(getCboTipoConsultaSaldo().toString()));
                entradaDTO.setQtDiaRepiqConsulta(getQtddDiasRepique());
                entradaDTO.setCdFavorecidoConsPagamento(Integer
                    .parseInt(getRdoPermiteFavorecidoConsultarPagamento()));
                entradaDTO.setCdLancamentoFuturoDebito(Integer
                    .parseInt(getRdoGerarLancFutDeb()));
                // setRdoEfetuaConCpfCnpjFav(""); // n�o sei ainda com o que
                // preencher
                entradaDTO.setCdTipoContaFavorecido(Integer
                    .parseInt(getCboTipoConsistenciaCpfCnpjFavorecido()
                        .toString()));
                entradaDTO.setCdPagamentoNaoUtilizado(Integer
                    .parseInt(getCboTratamentoFeriadosDataPagamento()
                        .toString()));
                //entradaDTO.setPermiteContingenciaPag(Integer
                //		.parseInt(getRdoPermiteContigenciaPagamento()));
                entradaDTO
                .setVlLimiteIndividualPagamento(getValorLimiteIndividual());
                entradaDTO.setVlLimiteDiaPagamento(getValorLimiteDiario());
                entradaDTO.setCdIndicadorSegundaLinha(getRdoIndicadorSegundaLinhaExtrato());
            }

            // Debito Veiculos
            if (modalidadeSaidaDTO.getCdParametroTela() == 32) {

                entradaDTO.setCdDiaFloatPagamento(getQtddDiasFloating());
                entradaDTO.setCdMomentoProcessamentoPagamento(Integer
                    .parseInt(getCboProcessamentoEfetivacaoPagamento()
                        .toString()));
                entradaDTO.setCdRejeicaoAgendaLote(Integer
                    .parseInt(getCboTipoRejeicaoAgendamento().toString()));
                entradaDTO.setCdRejeicaoEfetivacaoLote(Integer
                    .parseInt(getCboTipoRejeicaoEfetivacao().toString()));
                entradaDTO
                .setQtMaximaInconLote(getQtddMaximaRegistrosInconsistentesLote());
                entradaDTO
                .setCdPercentualMaximoInconLote(getPercMaxRegInconsistente());
                entradaDTO.setCdPrioridadeEfetivacaoPagamento(Integer
                    .parseInt(getCboPrioridadeDebito().toString()));
                entradaDTO.setCdValidacaoNomeFavorecido(Integer
                    .parseInt(getRdoValidarNomeFavorecidoReceitaFederal()));
                entradaDTO
                .setCdUtilizacaoFavorecidoControle(Integer
                    .parseInt(getRdoUtilizaCadastroFavorecidoControlePagamentos()));
                entradaDTO
                .setVlFavorecidoNaoCadastro(getValorMaximoPagamentoFavorecidoNaoCadastrado());
                entradaDTO.setCdConsSaldoPagamento(Integer
                    .parseInt(getCboTipoConsultaSaldo().toString()));
                entradaDTO.setQtDiaRepiqConsulta(getQtddDiasRepique());
                entradaDTO.setCdFavorecidoConsPagamento(Integer
                    .parseInt(getRdoPermiteFavorecidoConsultarPagamento()));
                entradaDTO.setCdLancamentoFuturoDebito(Integer
                    .parseInt(getRdoGerarLancFutDeb()));
                // setRdoEfetuaConCpfCnpjFav(""); // n�o sei ainda com o que
                // preencher
                entradaDTO.setCdTipoContaFavorecido(Integer
                    .parseInt(getCboTipoConsistenciaCpfCnpjFavorecido()
                        .toString()));
                entradaDTO.setCdPagamentoNaoUtilizado(Integer
                    .parseInt(getCboTratamentoFeriadosDataPagamento()
                        .toString()));
                //entradaDTO.setPermiteContingenciaPag(Integer
                //		.parseInt(getRdoPermiteContigenciaPagamento()));
                entradaDTO
                .setVlLimiteIndividualPagamento(getValorLimiteIndividual());
                entradaDTO.setVlLimiteDiaPagamento(getValorLimiteDiario());
                entradaDTO.setCdTipoDivergenciaVeiculo(Integer
                    .valueOf(getCboTratamentoValorDivergente().toString()));
                entradaDTO.setCdCtciaProprietarioVeiculo(Integer
                    .valueOf(getCboTipoConsistenciaCpfCnpjProprietario()
                        .toString()));
            }
            entradaDTO.setCdIndicadorFeriadoLocal(getCdIndicadorFeriadoLocal());
            entradaDTO.setCdIndicadorEmissaoAviso(""
                .equals(getRdoEmiteAvitoCompVida()) ? 0 : Integer
                    .parseInt(getRdoEmiteAvitoCompVida()));
            entradaDTO.setCdConsultaSaldoValorSuperior(Integer
                .parseInt(getCdConsultaSaldoValorSuperior().toString()));

            AlterarConfiguracaoTipoServModContratoSaidaDTO saidaDTO = getManterContratoService()
            .alterarConfiguracaoTipoServModContrato(entradaDTO);


            BradescoFacesUtils.addInfoModalMessage(
                "(" + saidaDTO.getCodMensagem() + ") "
                + saidaDTO.getMensagem(),
                "#{manterContratoServicosBean.consultarAposAlteracao}",
                BradescoViewExceptionActionType.ACTION, false);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return null;
        }

        return "";
    }

    /**
     * Consultar apos alteracao.
     * 
     * @return the string
     */
    public String consultarAposAlteracao() {
        try {
            carregaListaModalidade();
        } catch (PdcAdapterFunctionalException e) {
            setItemSelecionadoLista2(null);
            setListaGridPesquisa2(new ArrayList<ListarModalidadeTipoServicoSaidaDTO>());
        }

        return "modServicosManterContrato";
    }

    /**
     * Confirmar exc mod.
     * 
     * @return the string
     */
    public String confirmarExcMod() {
        try {
            ListarModalidadeTipoServicoSaidaDTO modalidadeSelecionada = getListaGridPesquisa2()
            .get(getItemSelecionadoLista2());

            ExcluirTipoServModContratoEntradaDTO entradaDTO = new ExcluirTipoServModContratoEntradaDTO();
            entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
            entradaDTO.setCdTipoContrato(getCdTipoContrato());
            entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
            entradaDTO.setCdTipoServico(getCdTipoServicoSelecionado());
            entradaDTO
            .setCdModalidade1(modalidadeSelecionada.getCdModalidade());
            entradaDTO.setCdModalidade2(0);
            entradaDTO.setCdModalidade3(0);
            entradaDTO.setCdModalidade4(0);
            entradaDTO.setCdModalidade5(0);
            entradaDTO.setCdModalidade6(0);
            entradaDTO.setCdModalidade7(0);
            entradaDTO.setCdModalidade8(0);
            entradaDTO.setCdModalidade9(0);
            entradaDTO.setCdModalidade10(0);

            entradaDTO.setCdExerServicoModalidade(3);

            ExcluirTipoServModContratoSaidaDTO saidaDTO = this.manterContratoService
            .excluirTipoServModContrato(entradaDTO);

            BradescoFacesUtils.addInfoModalMessage(
                "(" + saidaDTO.getCodMensagem() + ") "
                + saidaDTO.getMensagem(),
                "modServicosManterContrato",
                BradescoViewExceptionActionType.ACTION, false);
            carregaListaModalidade();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            return null;
        }

        return "";

    }

    /**
     * Limpar dados historico modalidade.
     */
    public void limparDadosHistoricoModalidade() {
        setFiltroDataDe(new Date());
        setFiltroDataAte(new Date());
        setCdTipoModalidadeHistorico(null);
        setCdTipoServico(null);
        carregaListaTipoServico();
        listaModalidadeHistorico = new ArrayList<SelectItem>();
        setBtoAcionado(false);
        setListaGridPesquisaHistoricoModalidade(null);
        setItemSelecionadoListaHistoricoModalidade(null);
    }

    /**
     * Limpar dados ambiente operacao.
     */
    public void limparDadosAmbienteOperacao() {
        setListaAmbiente(new ArrayList<ListarAmbienteOcorrenciaSaidaDTO>());
        setListaAmbienteControle(new ArrayList<SelectItem>());
        setTipoServicoSelecionado(0);
        setCpfCnpjParticipanteSelecionado(null);
        setNomeRazaoParticipanteSelecionado(null);
        setItemSelecionadoGridAmbiente(null);
        participanteSelecionado = new ListarParticipantesSaidaDTO();
    }

    /**
     * Voltar modalidades.
     * 
     * @return the string
     */
    public String voltarModalidades() {
        carregaListaModalidade();
        return "VOLTAR_MODALIDADE";
    }

    /**
     * Detalhar historico modalidade.
     * 
     * @return the string
     */
    public String detalharHistoricoModalidade() {
        ListarConManServicoSaidaDTO registroSelecionado = getListaGridPesquisaHistoricoModalidade()
        .get(getItemSelecionadoListaHistoricoModalidade());

        setDsModalidade(registroSelecionado.getDsProduto());
        setCdParametroTela(registroSelecionado.getCdParametroTela());

        preencheDadosHistoricoModalidade();

        if (getCdParametroTela() < 26) {
            return "DETALHAR_MODALIDADE";
        } else {
            return "DETALHAR_MODALIDADE2";
        }

    }

    /**
     * Preenche dados historico modalidade.
     */
    public void preencheDadosHistoricoModalidade() {

        Locale brasil = new Locale("pt", "BR");
        DecimalFormat df = new DecimalFormat("#,##0.00",
            new DecimalFormatSymbols(brasil));
        df.setParseBigDecimal(true);

        ListarConManServicoSaidaDTO dto = listaGridPesquisaHistoricoModalidade
        .get(getItemSelecionadoListaHistoricoModalidade());
        ConsultarConManServicosEntradaDTO entrada = new ConsultarConManServicosEntradaDTO();

        entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
        entrada.setCdTipoContratoNegocio(getCdTipoContrato());
        entrada.setNrSequenciaContratoNegocio(getNrSequenciaContrato());

        entrada.setCdParamentro(2);
        entrada.setCdParametroTela(dto.getCdParametroTela());
        entrada.setCdProdutoOperacaoRelacionado(dto.getCdModalidade());
        entrada.setCdProdutoServicoOperacao(dto.getCdProduto());
        entrada.setHrInclusaoRegistroHistorico(dto
            .getHrInclusaoRegistroHistorico());

        setDsModalidade(dto.getDsProduto());
        if (itemSelecionadoLista2 != null) {
            if (listaGridPesquisa.get(itemSelecionadoLista)
                            .getCdParametroTela() == 1
                            && listaGridPesquisa2.get(itemSelecionadoLista2)
                            .getCdParametroTela() == 20) {
                setRdoGerarLancProgramado("2");
                setHabilitaCampo(false);
                setHabilitaCampo2(false);
            } else if (listaGridPesquisa.get(itemSelecionadoLista)
                            .getCdParametroTela() == 2
                            && listaGridPesquisa2.get(itemSelecionadoLista2)
                            .getCdParametroTela() == 20) {
                setHabilitaCampo(false);
                setHabilitaCampo2(false);
            } else {
                setHabilitaCampo(true);
                setHabilitaCampo2(true);
            }
        }

        ConsultarConManServicosSaidaDTO saidaDTO = getManterContratoService()
        .consultarConManServico(entrada);

        setQtDiaUtilPgto(saidaDTO.getQtDiaUtilPgto());
        setCdIndicadorFeriadoLocal(saidaDTO.getCdIndicadorFeriadoLocal());
        setDsIndicadorFeriadoLocal(saidaDTO.getDsIndicadorFeriadoLocal());
        
        setDsOrigemIndicador(saidaDTO.getDsOrigemIndicador());
		setDsIndicador(saidaDTO.getDsIndicador());

        setDsTipoServico(listaTipoServicoHash.get(cdTipoServico));

        // Aviso de Recadastramento
        if (dto.getCdParametroTela() == 15) {

            setDsPeriodicidadeEnvio(saidaDTO.getDsMomentoAvisoRecadastramento());
            setCdPeriodicidadeEnvio(saidaDTO.getCdMomentoAvisoRecadastramento());
            setCdDestino(saidaDTO.getCdDestinoAviso());
            setCdEnderecoEnvio(saidaDTO.getCdConsultaEndereco());
            setCdPermitirAgrupamento(saidaDTO.getCdAgrupamentoAviso());
            setQtddDiasAntecipacao(String.valueOf(saidaDTO
                .getQuantidadeAntecedencia()));

            // novas
            setCboMomentoEnvio(Long.valueOf(String.valueOf(saidaDTO
                .getCdMomentoAvisoRecadastramento())));
            setDsMomentoEnvio(saidaDTO.getDsMomentoAvisoRecadastramento());
            setQtddDiasAvisoAntecipado(saidaDTO.getQuantidadeAntecedencia());
            setCboDestinoEnvio(Long.valueOf(String.valueOf(saidaDTO
                .getCdDestinoAviso())));
            setDsDestinoEnvio(saidaDTO.getDsDestinoAviso());
            setCboCadConsultaEndEnvio(Long.valueOf(String.valueOf(saidaDTO
                .getCdConsultaEndereco())));
            setDsCadConsultaEndEnvio(saidaDTO.getDsConsultaEndereco());
            setRdoPermitirAgrupamentoCorrespondencia(String.valueOf(saidaDTO
                .getCdAgrupamentoAviso()));
        }

        // Formul�rio de Recadastramento
        if (dto.getCdParametroTela() == 16) {

            setDsPeriodicidadeEnvio(saidaDTO.getDsMomentoAvisoRecadastramento());
            setCdPeriodicidadeEnvio(saidaDTO.getCdMomentoAvisoRecadastramento());
            setCdDestino(saidaDTO.getCdDestinoAviso());
            setCdEnderecoEnvio(saidaDTO.getCdConsultaEndereco());
            setCdPermitirAgrupamento(saidaDTO.getCdAgrupamentoAviso());
            setQtddDiasAntecipacao(String.valueOf(saidaDTO
                .getQuantidadeAntecedencia()));

            // novas
            setCboMomentoEnvio(Long.valueOf(String.valueOf(saidaDTO
                .getCdMomentoFormularioRecadastramento())));
            setDsMomentoEnvio(saidaDTO.getDsMomentoFormularioRecadastramento());
            setQtddDiasAvisoAntecipado(saidaDTO.getQuantidadeAntecedencia());
            setCboDestinoEnvio(Long.valueOf(String.valueOf(saidaDTO
                .getCdDestinoAviso())));
            setDsDestinoEnvio(saidaDTO.getDsDestinoAviso());
            setCboCadConsultaEndEnvio(Long.valueOf(String.valueOf(saidaDTO
                .getCdConsultaEndereco())));
            setDsCadConsultaEndEnvio(saidaDTO.getDsConsultaEndereco());
            setRdoPermitirAgrupamentoCorrespondencia(String.valueOf(saidaDTO
                .getCdAgrupamentoAviso()));
        }

        // Pagamentos de Fornecedores - DOC
        // Pagamentos de Fornecedores - TED
        if (dto.getCdParametroTela() == 17 || dto.getCdParametroTela() == 18) {

            setCdTipoConsultaSaldoFiltro(saidaDTO.getCdConsultaSaldoPagamento());
            setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO
                .getQuantidadeDiaRepiqueConsulta()));
            setCdUtilizaCadFavorFiltro(saidaDTO
                .getCdUtilizacaoFavorecidoControle());
            setValorMaxPagFavNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setValorMaxPagFavNaoCadastradoDesc(df
                .format(getValorMaxPagFavNaoCadastrado()));
            setCdTratamentoFeriadosFiltro(saidaDTO.getCdPagamentoNaoUtil());
            setCdTipoProcessamentoFiltro(saidaDTO
                .getCdMomentoProcessamentoPagamento());
            setCdTipoRejAgendamentoFiltro(saidaDTO
                .getCdRejeicaoAgendamentoLote());
            setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdRejeicaoEfetivacaoLote());
            setCdPrioridadeDebitoFiltro(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento());
            setCdGerLancFutDebitoFiltro(saidaDTO.getCdLancamentoFuturoCredito());
            setCdGerLancFutCreditoFiltro(saidaDTO.getCdLancamentoFuturoDebito());
            setCdPermiteFavConsultPagFiltro(saidaDTO
                .getCdFavorecidoConsultaPagamento());
            setValorLimiteIndividualFiltro(saidaDTO
                .getValorLimiteIndividualPagamento());
            setDsValorLimiteIndividual(df
                .format(getValorLimiteIndividualFiltro()));
            setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiarioPagamento());
            setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));

            // novas
            setQtddDiasFloating(saidaDTO.getQuantidadeDiaFloatingPagamento());
            setCboProcessamentoEfetivacaoPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
            setDsProcessamentoEfetivacaoPagamento(saidaDTO
                .getDsMomentoProcessamentoPagamento());
            setCboTipoRejeicaoAgendamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdRejeicaoAgendamentoLote())));
            setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendamentoLote());
            setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO
                .getCdRejeicaoEfetivacaoLote())));
            setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
            setQtddMaximaRegistrosInconsistentesLote(saidaDTO
                .getQuantidadeMaximaInconsistenteLote());
            setPercMaxRegInconsistente(saidaDTO
                .getPercentualMaximoInconsistenteLote());
            setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento())));
            setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
            setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO
                .getDsOutraidentificacaoFavorecido());
            setRdoUtilizaCadastroFavorecidoControlePagamentos(String
                .valueOf(saidaDTO.getCdUtilizacaoFavorecidoControle()));
            setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO
                .getCdConsultaSaldoPagamento())));
            setDsTipoConsSaldo(saidaDTO.getDsConsultaSaldoPagamento());
            setQtddDiasRepique(saidaDTO.getQuantidadeDiaRepiqueConsulta());
            setRdoPermiteFavorecidoConsultarPagamento(String.valueOf(saidaDTO
                .getCdFavorecidoConsultaPagamento()));
            setRdoGerarLancFutDeb(String.valueOf(saidaDTO
                .getCdLancamentoFuturoDebito()));
            setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO
                .getDsCctciaInscricaoFavorecido());
            setCboTratamentoFeriadosDataPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdPagamentoNaoUtil())));
            setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtil());
            setValorLimiteIndividual(saidaDTO
                .getValorLimiteIndividualPagamento());
            setValorLimiteDiario(saidaDTO.getValorLimiteDiarioPagamento());
            setDsIndicadorAgendaGrade(saidaDTO.getDsIndicadorAgendaGrade());
        }

        // Pagamentos Via Ordem de Pagamento
        if (dto.getCdParametroTela() == 19) {

            setCdTipoConsultaSaldoFiltro(saidaDTO.getCdConsultaSaldoPagamento());
            setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO
                .getQuantidadeDiaRepiqueConsulta()));
            setCdUtilizaCadFavorFiltro(saidaDTO
                .getCdUtilizacaoFavorecidoControle());
            setValorMaxPagFavNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setValorMaxPagFavNaoCadastradoDesc(df
                .format(getValorMaxPagFavNaoCadastrado()));
            setCdTratamentoFeriadosFiltro(saidaDTO.getCdPagamentoNaoUtil());
            setCdTipoProcessamentoFiltro(saidaDTO
                .getCdMomentoProcessamentoPagamento());
            setCdTipoRejAgendamentoFiltro(saidaDTO
                .getCdRejeicaoAgendamentoLote());
            setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdRejeicaoEfetivacaoLote());
            setCdPrioridadeDebitoFiltro(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento());
            setCdGerLancFutDebitoFiltro(saidaDTO.getCdLancamentoFuturoDebito());
            setCdGerLancFutCreditoFiltro(saidaDTO
                .getCdLancamentoFuturoCredito());
            setCdPermiteOutrosTipos(String.valueOf(saidaDTO
                .getCdOutraidentificacaoFavorecido()));
            setCdOcorrenciaDebito(saidaDTO.getCdMomentoDebitoPagamento());
            setCdPermiteFavConsultPagFiltro(saidaDTO
                .getCdFavorecidoConsultaPagamento());
            setValorLimiteIndividualFiltro(saidaDTO
                .getValorLimiteIndividualPagamento());
            setDsValorLimiteIndividual(df
                .format(getValorLimiteIndividualFiltro()));
            setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiarioPagamento());
            setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
            setQtddDiasExpiracao(String.valueOf(saidaDTO
                .getQuantidadeDiaExpiracao()));
            setDsLocalEmissao(saidaDTO.getDsLocalEmissao());

            // novas
            setQtddDiasFloating(saidaDTO.getQuantidadeDiaFloatingPagamento());
            setCboProcessamentoEfetivacaoPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
            setDsProcessamentoEfetivacaoPagamento(saidaDTO
                .getDsMomentoProcessamentoPagamento());
            setCboTipoRejeicaoAgendamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdRejeicaoAgendamentoLote())));
            setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendamentoLote());
            setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO
                .getCdRejeicaoEfetivacaoLote())));
            setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
            setQtddMaximaRegistrosInconsistentesLote(saidaDTO
                .getQuantidadeMaximaInconsistenteLote());
            setPercMaxRegInconsistente(saidaDTO
                .getPercentualMaximoInconsistenteLote());
            setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento())));
            setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
            setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO
                .getDsOutraidentificacaoFavorecido());
            setRdoUtilizaCadastroFavorecidoControlePagamentos(String
                .valueOf(saidaDTO.getCdUtilizacaoFavorecidoControle()));
            setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO
                .getCdConsultaSaldoPagamento())));
            setDsTipoConsSaldo(saidaDTO.getDsConsultaSaldoPagamento());
            setQtddDiasRepique(saidaDTO.getQuantidadeDiaRepiqueConsulta());
            setRdoPermiteFavorecidoConsultarPagamento(String.valueOf(saidaDTO
                .getCdFavorecidoConsultaPagamento()));
            setRdoGerarLancFutDeb(String.valueOf(saidaDTO
                .getCdLancamentoFuturoDebito()));
            setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO
                .getDsCctciaInscricaoFavorecido());
            setCboTratamentoFeriadosDataPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdPagamentoNaoUtil())));
            setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtil());
            setCboOcorrenciaDebito(Long.parseLong(String.valueOf(saidaDTO
                .getCdMomentoDebitoPagamento())));
            setDsOcorrenciaDebito(saidaDTO.getDsMomentoDebitoPagamento());
            setCboTipoInscricaoFavorecido(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoIdentificacaoBeneficio())));
            setDsTipoInscricaoFavorecido(saidaDTO
                .getDsTipoIdentificacaoBeneficio());
            setQuantidadeDiasExpiracao(saidaDTO.getQuantidadeDiaExpiracao());
            setValorLimiteIndividual(saidaDTO
                .getValorLimiteIndividualPagamento());
            setValorLimiteDiario(saidaDTO.getValorLimiteDiarioPagamento());
        }

        // Pagamento Via Cr�dito em Conta
        if (dto.getCdParametroTela() == 20) {

            setCdTipoConsultaSaldoFiltro(saidaDTO.getCdConsultaSaldoPagamento());
            setCdUtilizaCadFavorFiltro(saidaDTO
                .getCdUtilizacaoFavorecidoControle());
            setValorMaxPagFavNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setValorMaxPagFavNaoCadastradoDesc(df
                .format(getValorMaxPagFavNaoCadastrado()));
            setCdTipoProcessamentoFiltro(saidaDTO
                .getCdMomentoProcessamentoPagamento());
            setCdTipoRejAgendamentoFiltro(saidaDTO
                .getCdRejeicaoAgendamentoLote());
            setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdRejeicaoEfetivacaoLote());
            setCdPrioridadeDebitoFiltro(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento());
            setCdGerLancFutDebitoFiltro(saidaDTO.getCdLancamentoFuturoDebito());
            setCdGerLancFutCreditoFiltro(saidaDTO
                .getCdLancamentoFuturoCredito());
            setCdGerarLancamentoProgramado(String.valueOf(saidaDTO
                .getCdindicadorLancamentoProgramado()));
            setCdPermiteFavConsultPagFiltro(saidaDTO
                .getCdFavorecidoConsultaPagamento());
            setCdEfetuaConsistencia(String.valueOf(saidaDTO
                .getCdCctciaEspeBeneficio()));
            setCdTipoTratamento(saidaDTO.getCdTratamentoContaTransferida());
            setCdPermiteEstorno(null);
            setQtddMaximaRegistro(String.valueOf(saidaDTO
                .getQuantidadeMaximaInconsistenteLote()));
            setValorLimiteIndividualFiltro(saidaDTO
                .getValorLimiteIndividualPagamento());
            setDsValorLimiteIndividual(df
                .format(getValorLimiteIndividualFiltro()));
            setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiarioPagamento());
            setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
            setPercentualMaximoRegistro(String.valueOf(saidaDTO
                .getPercentualMaximoInconsistenteLote()));

            // vari�veis novas

            setQtddDiasFloating(saidaDTO.getQuantidadeDiaFloatingPagamento());
            setCboProcessamentoEfetivacaoPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
            setDsProcessamentoEfetivacaoPagamento(saidaDTO
                .getDsMomentoProcessamentoPagamento());
            setCboTipoRejeicaoAgendamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdRejeicaoAgendamentoLote())));
            setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendamentoLote());
            setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO
                .getCdRejeicaoEfetivacaoLote())));
            setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
            setQtddMaximaRegistrosInconsistentesLote(saidaDTO
                .getQuantidadeMaximaInconsistenteLote());
            setPercMaxRegInconsistente(saidaDTO
                .getPercentualMaximoInconsistenteLote());
            setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento())));
            setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
            setCboPermiteEstornoPagamento(0L);
            setDsPermiteEstornoPagamento("");
            setRdoUtilizaCadastroFavorecidoControlePagamentos(String
                .valueOf(saidaDTO.getCdUtilizacaoFavorecidoControle()));
            setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO
                .getCdConsultaSaldoPagamento())));
            setDsTipoConsSaldo(saidaDTO.getDsConsultaSaldoPagamento());
            setQtddDiasRepique(saidaDTO.getQuantidadeDiaRepiqueConsulta());
            setRdoPermiteFavorecidoConsultarPagamento(String.valueOf(saidaDTO
                .getCdFavorecidoConsultaPagamento()));
            setRdoEfetuaConEspBenef(String.valueOf(saidaDTO
                .getCdCctciaEspeBeneficio()));
            setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO
                .getDsCctciaInscricaoFavorecido());
            setRdoGerarLancFutDeb(String.valueOf(saidaDTO
                .getCdLancamentoFuturoDebito()));
            setRdoGerarLancFutCred(String.valueOf(saidaDTO
                .getCdLancamentoFuturoCredito()));
            setRdoGerarLancProgramado(String.valueOf(saidaDTO
                .getCdindicadorLancamentoProgramado()));
            setCboTratamentoFeriadosDataPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdPagamentoNaoUtil())));
            setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtil());
            setCboTipoTratamentoContaTransferida(Long.valueOf(saidaDTO
                .getCdTratamentoContaTransferida()));
            setDsTipoTratamentoContaTransferida(saidaDTO
                .getDsTratamentoContaTransferida());
            // setCboTipoContaCredito(Long.parseLong(String.valueOf(saidaDTO.getCd)));
            setDsTipoContaCredito(saidaDTO.getDsMeioPagamentoDebito());
            setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO
                .getDsOutraidentificacaoFavorecido());
            setRdoExigeLibLoteProcessado(String.valueOf(saidaDTO
                .getCdLiberacaoLoteProcessado()));
            setValorLimiteIndividual(saidaDTO
                .getValorLimiteIndividualPagamento());
            setValorLimiteDiario(saidaDTO.getValorLimiteDiarioPagamento());
            setRdoUtilizaLancPersonalizado(saidaDTO
                .getDsIndicadorMensagemPersonalizada());

            setDsPreenchimentoLancamentoPersonalizado(saidaDTO.getDsPreenchimentoLancamentoPersonalizado());
        }

        // Pagamento Via D�bito em Conta
        if (dto.getCdParametroTela() == 21) {

            setCdTipoConsultaSaldoFiltro(saidaDTO.getCdConsultaSaldoPagamento());
            setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO
                .getQuantidadeDiaRepiqueConsulta()));
            setCdUtilizaCadFavorFiltro(saidaDTO
                .getCdUtilizacaoFavorecidoControle());
            setCdPrioridadeDebitoFiltro(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento());
            setCdGerLancFutDebitoFiltro(saidaDTO.getCdLancamentoFuturoDebito());
            setCdGerLancFutCreditoFiltro(saidaDTO
                .getCdLancamentoFuturoCredito());
            setValorMaxPagFavNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setValorMaxPagFavNaoCadastradoDesc(df
                .format(getValorMaxPagFavNaoCadastrado()));
            setCdTratamentoFeriadosFiltro(saidaDTO.getCdPagamentoNaoUtil());
            setCdTipoProcessamentoFiltro(saidaDTO
                .getCdMomentoProcessamentoPagamento());
            setCdTipoRejAgendamentoFiltro(saidaDTO
                .getCdRejeicaoAgendamentoLote());
            setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdRejeicaoEfetivacaoLote());
            setCdPermiteDebitoOnline(String.valueOf(saidaDTO
                .getCdPermissaoDebitoOnline()));
            setValorLimiteIndividualFiltro(saidaDTO
                .getValorLimiteIndividualPagamento());
            setDsValorLimiteIndividual(df
                .format(getValorLimiteIndividualFiltro()));
            setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiarioPagamento());
            setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));

            // novas
            setQtddDiasFloating(saidaDTO.getQuantidadeDiaFloatingPagamento());
            setCboProcessamentoEfetivacaoPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
            setDsProcessamentoEfetivacaoPagamento(saidaDTO
                .getDsMomentoProcessamentoPagamento());
            setCboTipoRejeicaoAgendamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdRejeicaoAgendamentoLote())));
            setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendamentoLote());
            setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO
                .getCdRejeicaoEfetivacaoLote())));
            setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
            setQtddMaximaRegistrosInconsistentesLote(saidaDTO
                .getQuantidadeMaximaInconsistenteLote());
            setPercMaxRegInconsistente(saidaDTO
                .getPercentualMaximoInconsistenteLote());
            setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento())));
            setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
            setCboPermiteEstornoPagamento(0L);
            setDsPermiteEstornoPagamento(null);
            setRdoUtilizaCadastroFavorecidoControlePagamentos(String
                .valueOf(saidaDTO.getCdUtilizacaoFavorecidoControle()));
            setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO
                .getCdConsultaSaldoPagamento())));
            setDsTipoConsSaldo(saidaDTO.getDsConsultaSaldoPagamento());
            setQtddDiasRepique(saidaDTO.getQuantidadeDiaRepiqueConsulta());
            setRdoPermiteFavorecidoConsultarPagamento(String.valueOf(saidaDTO
                .getCdFavorecidoConsultaPagamento()));
            setRdoEfetuaConEspBenef(String.valueOf(saidaDTO
                .getCdCctciaEspeBeneficio()));
            setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO
                .getDsOutraidentificacaoFavorecido());
            setCboTratamentoFeriadosDataPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdPagamentoNaoUtil())));
            setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtil());
            setValorLimiteIndividual(saidaDTO
                .getValorLimiteIndividualPagamento());
            setValorLimiteDiario(saidaDTO.getValorLimiteDiarioPagamento());
            setRdoUtilizaLancPersonalizado(saidaDTO
                .getDsIndicadorMensagemPersonalizada());
            setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO
                .getDsCctciaInscricaoFavorecido());
            setRdoGerarLancFutDeb(String.valueOf(saidaDTO
                .getCdLancamentoFuturoDebito()));
            setRdoGerarLancFutCred(String.valueOf(saidaDTO
                .getCdLancamentoFuturoCredito()));
            setRdoPermiteDebitoOnline(String.valueOf(saidaDTO
                .getCdPermissaoDebitoOnline()));
        }

        // Pagamento Via T�tulo Bradesco
        // Pagamento Via T�tulo Outros Bancos
        if (dto.getCdParametroTela() == 22 || dto.getCdParametroTela() == 23) {
            setCdTipoConsultaSaldoFiltro(saidaDTO.getCdConsultaSaldoPagamento());
            setCdUtilizaCadFavorFiltro(saidaDTO
                .getCdUtilizacaoFavorecidoControle());
            setValorMaxPagFavNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setValorMaxPagFavNaoCadastradoDesc(df
                .format(getValorMaxPagFavNaoCadastrado()));
            setCdTratamentoFeriadosFiltro(saidaDTO.getCdPagamentoNaoUtil());
            setCdTipoProcessamentoFiltro(saidaDTO
                .getCdMomentoProcessamentoPagamento());
            setCdTipoRejAgendamentoFiltro(saidaDTO
                .getCdRejeicaoAgendamentoLote());
            setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdRejeicaoEfetivacaoLote());
            setCdPrioridadeDebitoFiltro(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento());
            setCdGerLancFutDebitoFiltro(saidaDTO.getCdLancamentoFuturoDebito());
            setPermitePagarVencido(String.valueOf(saidaDTO
                .getCdAgendamentoPagamentoVencido()));
            setPermitePagarMenor(String.valueOf(saidaDTO
                .getCdAgendamentoValorMenor()));

            // novas
            setQtddDiasFloating(saidaDTO.getQuantidadeDiaFloatingPagamento());
            setCboProcessamentoEfetivacaoPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
            setDsProcessamentoEfetivacaoPagamento(saidaDTO
                .getDsMomentoProcessamentoPagamento());
            setCboTipoRejeicaoAgendamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdRejeicaoAgendamentoLote())));
            setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendamentoLote());
            setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO
                .getCdRejeicaoEfetivacaoLote())));
            setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
            setQtddMaximaRegistrosInconsistentesLote(saidaDTO
                .getQuantidadeMaximaInconsistenteLote());
            setPercMaxRegInconsistente(saidaDTO
                .getPercentualMaximoInconsistenteLote());
            setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento())));
            setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
            setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO
                .getDsOutraidentificacaoFavorecido());
            setRdoUtilizaCadastroFavorecidoControlePagamentos(String
                .valueOf(saidaDTO.getCdUtilizacaoFavorecidoControle()));
            setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO
                .getCdConsultaSaldoPagamento())));
            setDsTipoConsSaldo(saidaDTO.getDsConsultaSaldoPagamento());
            setQtddDiasRepique(saidaDTO.getQuantidadeDiaRepiqueConsulta());
            setRdoPermiteFavorecidoConsultarPagamento(String.valueOf(saidaDTO
                .getCdFavorecidoConsultaPagamento()));
            setRdoGerarLancFutDeb(String.valueOf(saidaDTO
                .getCdLancamentoFuturoDebito()));
            setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO
                .getDsCctciaInscricaoFavorecido());
            setCboTratamentoFeriadosDataPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdPagamentoNaoUtil())));
            setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtil());
            setRdoPermitePagarVencido(saidaDTO
                .getDsAgendamentoPagamentoVencido());
            setRdoPermitePagarMenor(saidaDTO.getDsAgendamentoValorMenor());
            setQuantidadeLimiteDiasPagamentoVencido(saidaDTO
                .getQuantidadeMaximaTituloVencido());
            setValorLimiteIndividual(saidaDTO
                .getValorLimiteIndividualPagamento());
            setValorLimiteDiario(saidaDTO.getValorLimiteDiarioPagamento());
            setDsConsultaSaldoValorSuperior(saidaDTO
                .getDsConsultaSaldoValorSuperior());

            setDsTituloDdaRetorno(saidaDTO.getDsTituloDdaRetorno());
            BigDecimal percDiferenca = null;
            if (saidaDTO.getVlPercentualDiferencaTolerada() != null
                && BigDecimal.ZERO.compareTo(saidaDTO.getVlPercentualDiferencaTolerada()) != 0) {
                percDiferenca = saidaDTO.getVlPercentualDiferencaTolerada();
            }
            setVlPercentualDiferencaTolerada(percDiferenca);
            setCboConsistenciaCpfCnpjBenefAvalNpc(Long.parseLong(String.valueOf(
                    saidaDTO.getCdConsistenciaCpfCnpjBenefAvalNpc()))); 
            setDsConsistenciaCpfCnpjBenefAvalNpc(saidaDTO.getDsConsistenciaCpfCnpjBenefAvalNpc());
            
        }

        // Pagamento Via Dep�sito Identificado
        if (dto.getCdParametroTela() == 24) {
            setCdTipoConsultaSaldoFiltro(saidaDTO.getCdConsultaSaldoPagamento());
            setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO
                .getQuantidadeDiaRepiqueConsulta()));
            setCdUtilizaCadFavorFiltro(saidaDTO
                .getCdUtilizacaoFavorecidoControle());
            setValorMaxPagFavNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setValorMaxPagFavNaoCadastradoDesc(df
                .format(getValorMaxPagFavNaoCadastrado()));
            setCdTratamentoFeriadosFiltro(saidaDTO.getCdPagamentoNaoUtil());
            setCdTipoProcessamentoFiltro(saidaDTO
                .getCdMomentoProcessamentoPagamento());
            setCdTipoRejAgendamentoFiltro(saidaDTO
                .getCdRejeicaoAgendamentoLote());
            setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdRejeicaoEfetivacaoLote());
            setCdGerLancFutDebitoFiltro(saidaDTO.getCdLancamentoFuturoDebito());
            setCdPermiteFavConsultPagFiltro(saidaDTO
                .getCdFavorecidoConsultaPagamento());
            setValorLimiteIndividualFiltro(saidaDTO
                .getValorLimiteIndividualPagamento());
            setDsValorLimiteIndividual(df
                .format(getValorLimiteIndividualFiltro()));
            setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiarioPagamento());
            setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
        }

        // Pagamentos de Fornecedores
        if (dto.getCdParametroTela() == 25) {
            setCdTipoConsultaSaldoFiltro(saidaDTO.getCdConsultaSaldoPagamento());
            setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO
                .getQuantidadeDiaRepiqueConsulta()));
            setCdUtilizaCadFavorFiltro(saidaDTO
                .getCdUtilizacaoFavorecidoControle());
            setValorMaxPagFavNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setValorMaxPagFavNaoCadastradoDesc(df
                .format(getValorMaxPagFavNaoCadastrado()));
            setCdTratamentoFeriadosFiltro(saidaDTO.getCdPagamentoNaoUtil());
            setCdTipoProcessamentoFiltro(saidaDTO
                .getCdMomentoProcessamentoPagamento());
            setCdTipoRejAgendamentoFiltro(saidaDTO
                .getCdRejeicaoAgendamentoLote());
            setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdRejeicaoEfetivacaoLote());
            setCdPermiteFavConsultPagFiltro(saidaDTO
                .getCdFavorecidoConsultaPagamento());
            setValorLimiteIndividualFiltro(saidaDTO
                .getValorLimiteIndividualPagamento());
            setDsValorLimiteIndividual(df
                .format(getValorLimiteIndividualFiltro()));
            setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiarioPagamento());
            setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
        }

        // Rastreabilidade de T�tulos
        if (dto.getCdParametroTela() == 26) {
            setCdTipoRastreamentoTitulos(saidaDTO
                .getCdCriterioRastreabilidadeTitulo());
            setRastrearTitulo(String.valueOf(saidaDTO
                .getCdRastreabilidadeTituloTerceiro()));
            setRastrearNotas(String.valueOf(saidaDTO
                .getCdRastreabilidadeNotaFiscal()));
            setCapturarTitulos(String.valueOf(saidaDTO
                .getCdCapturaTituloRegistrado()));
            setAgendarTituloProprio(String.valueOf(saidaDTO
                .getCdIndicadorAgendamentoTitulo()));
            setAgendarTituloFilial(String.valueOf(saidaDTO
                .getCdAgendamentoRastreabilidadeFinal()));
            setBloquearEmissaoPapeleta(String.valueOf(saidaDTO
                .getCdBloqueioEmissaoPapeleta()));
            setDsDataInicioBloqueioPapeleta(FormatarData
                .formatarDataFromPdc(saidaDTO.getDtInicioBloqueioPapeleta()));
            setDataInicioBloqueioPapeleta(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTO
                    .getDtInicioBloqueioPapeleta()));
            setDsDataInicioRastreamento(FormatarData
                .formatarDataFromPdc(saidaDTO
                    .getDtInicioRastreabilidadeTitulo()));
            setDataInicioRastreamento(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTO
                    .getDtInicioRastreabilidadeTitulo()));
            setDsDataRegistroTitulo(FormatarData.formatarDataFromPdc(saidaDTO
                .getDtRegistroTitulo()));
            setDataRegistroTitulo(FormatarData.formataDiaMesAnoFromPdc(saidaDTO
                .getDtRegistroTitulo()));

            // novas
            setCboTipoRastreamentoTitulo(Long.valueOf(String.valueOf(saidaDTO
                .getCdCriterioRastreabilidadeTitulo())));
            setDsTipoRastreamentoTitulo(saidaDTO
                .getDsCriterioRastreabilidadeTitulo());
            setRdoRastrearTituloTerceiros(String.valueOf(saidaDTO
                .getCdRastreabilidadeTituloTerceiro()));
            setRdoRastrearNotasFiscais(String.valueOf(saidaDTO
                .getCdRastreabilidadeNotaFiscal()));
            setRdoCapTitulosDtReg(String.valueOf(saidaDTO
                .getCdCriterioRastreabilidadeTitulo()));
            setRdoAgendarTituloRastProp(String.valueOf(saidaDTO
                .getCdIndicadorAgendamentoTitulo()));
            setRdoAgendarTituloRastreadoFilial(String.valueOf(saidaDTO
                .getCdAgendamentoRastreabilidadeFinal()));
            setRdoBloqEmissaoPapeleta(String.valueOf(saidaDTO
                .getCdBloqueioEmissaoPapeleta()));
            setRdoAdesaoSacadoEletronico(String.valueOf(saidaDTO
                .getDsPermissaoDebitoOnline()));
            setDtInicioRastreamento(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTO
                    .getDtInicioRastreabilidadeTitulo()));
            setDtInicioBloqPapeleta(FormatarData
                .formataDiaMesAnoFromPdc(saidaDTO
                    .getDtInicioBloqueioPapeleta()));
            if("sim".equalsIgnoreCase(saidaDTO.getDsExigeFilial())){
                setRdoExigeIdentificacaoFilialAutorizacao("1");
            }else{
                setRdoExigeIdentificacaoFilialAutorizacao("2");
            }

        }

        // Pagamento Prova de Vida
        if (dto.getCdParametroTela() == 27) {
            setCdTipoComprovacao(saidaDTO.getCdAcaoNaoVida());
            setQtddMesesPeriodicidade(String.valueOf(saidaDTO
                .getQuantidadeMesComprovante()));
            setQtddDiasAvisoVenc(String.valueOf(saidaDTO
                .getQuantidadeAnteriorVencimentoComprovante()));
            setCdUtilizarMsgPerOnline(saidaDTO
                .getCdIndicadorMensagemPersonalizada());
            setQtddDiasAntecedeInicio(String.valueOf(saidaDTO
                .getQuantidadeAntecedencia()));
            setCdEnderecoEnvio(saidaDTO.getCdConsultaEndereco());

            setCboAcaoParaNaoComprovacao(Long.valueOf(saidaDTO
                .getCdAcaoNaoVida()));
            setDsAcaoParaNaoComprovacao(saidaDTO.getDsAcaoNaoVida());
            setQtddMesesPeriodicidadeComprovacao(saidaDTO
                .getQuantidadeMesComprovante());
            setRdoEmiteAvitoCompVida(String.valueOf(saidaDTO
                .getCdDestinoAviso()));
            setCboDestinoEnvio(Long.valueOf(saidaDTO.getCdDestinoAviso()));
            setDsDestinoEnvio(saidaDTO.getDsDestinoAviso());
            setCboCadConsultaEndEnvio(Long.valueOf(saidaDTO
                .getCdConsultaEndereco()));
            setDsCadConsultaEndEnvio(saidaDTO.getDsConsultaEndereco());
            setRdoUtilizaMensagemPersonalizada(String.valueOf(saidaDTO
                .getCdIndicadorMensagemPersonalizada()));
            setQtddDiasEmissaoAvisoAntesInicioComprovacao(saidaDTO
                .getQuantidadeAntecedencia());
            setQtddDiasEmissaoAvisoAntesVencimento(saidaDTO
                .getQuantidadeAnteriorVencimentoComprovante());
            setCodigoFormulario(String.valueOf(saidaDTO
                .getCdDestinoFormularioRecadastramento()));

        }
        if (dto.getCdParametroTela() == 28 || dto.getCdParametroTela() == 29
                        || dto.getCdParametroTela() == 30) {

            setCdTipoConsultaSaldoFiltro(saidaDTO.getCdConsultaSaldoPagamento());
            setValorLimiteIndividualFiltro(saidaDTO
                .getValorLimiteIndividualPagamento());
            setDsValorLimiteIndividual(df
                .format(getValorLimiteIndividualFiltro()));
            setCdEfetivacao(saidaDTO.getCdTipoEfetivacaoPagamento());
            setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiarioPagamento());
            setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
            setCdPermiteConsulFavorecido(saidaDTO
                .getCdFavorecidoConsultaPagamento());
            setQuantDiasRepiqueFiltro(String.valueOf(saidaDTO
                .getQuantidadeDiaRepiqueConsulta()));
            setCdTratamentoFeriadosFiltro(saidaDTO.getCdPagamentoNaoUtil());
            setCdTipoProcessamentoFiltro(saidaDTO
                .getCdMomentoProcessamentoPagamento());
            setCdTipoRejAgendamentoFiltro(saidaDTO
                .getCdRejeicaoAgendamentoLote());
            setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdRejeicaoEfetivacaoLote());
            setCdPrioridadeDebitoFiltro(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento());
            setCdControlePagFavorecido(saidaDTO
                .getCdUtilizacaoFavorecidoControle());
            setValorMaxPagFavNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setValorMaxPagFavNaoCadastradoDesc(df
                .format(getValorMaxPagFavNaoCadastrado()));
            setPercentualInconsLote(Integer.toString(saidaDTO
                .getPercentualMaximoInconsistenteLote()));
            setQtddMaxInconsLote(Integer.toString(saidaDTO
                .getQuantidadeMaximaInconsistenteLote()));
            setCdPermiteContingenciaPag(saidaDTO.getCdContagemConsultaSaldo());

            // novas
            setQtddDiasFloating(saidaDTO.getQuantidadeDiaFloatingPagamento());
            setCboProcessamentoEfetivacaoPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
            setDsProcessamentoEfetivacaoPagamento(saidaDTO
                .getDsMomentoProcessamentoPagamento());
            setCboTipoRejeicaoAgendamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdRejeicaoAgendamentoLote())));
            setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendamentoLote());
            setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO
                .getCdRejeicaoEfetivacaoLote())));
            setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
            setQtddMaximaRegistrosInconsistentesLote(saidaDTO
                .getQuantidadeMaximaInconsistenteLote());
            setPercMaxRegInconsistente(saidaDTO
                .getPercentualMaximoInconsistenteLote());
            setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento())));
            setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
            setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO
                .getDsOutraidentificacaoFavorecido());
            setCboTratamentoFeriadosDataPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdPagamentoNaoUtil())));
            setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtil());
            setValorLimiteIndividual(saidaDTO
                .getValorLimiteIndividualPagamento());
            setValorLimiteDiario(saidaDTO.getValorLimiteDiarioPagamento());
            setRdoUtilizaLancPersonalizado(saidaDTO
                .getDsIndicadorMensagemPersonalizada());
            setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO
                .getDsCctciaInscricaoFavorecido());
            setRdoGerarLancFutDeb(String.valueOf(saidaDTO
                .getCdLancamentoFuturoDebito()));
            setRdoGerarLancFutCred(String.valueOf(saidaDTO
                .getCdLancamentoFuturoCredito()));
            setRdoPermiteDebitoOnline(String.valueOf(saidaDTO
                .getCdPermissaoDebitoOnline()));
            setRdoUtilizaCadastroFavorecidoControlePagamentos(String
                .valueOf(saidaDTO.getCdUtilizacaoFavorecidoControle()));
            setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO
                .getCdConsultaSaldoPagamento())));
            setDsTipoConsSaldo(saidaDTO.getDsConsultaSaldoPagamento());
            setQtddDiasRepique(saidaDTO.getQuantidadeDiaRepiqueConsulta());
            setRdoPermiteFavorecidoConsultarPagamento(String.valueOf(saidaDTO
                .getCdFavorecidoConsultaPagamento()));
        }

        if (dto.getCdParametroTela() == 31) {
            setCdTipoConsultaSaldoFiltro(saidaDTO.getCdConsultaSaldoPagamento());
            setValorLimiteIndividualFiltro(saidaDTO
                .getValorLimiteIndividualPagamento());
            setDsValorLimiteIndividual(df
                .format(getValorLimiteIndividualFiltro()));
            setCdEfetivacao(saidaDTO.getCdTipoEfetivacaoPagamento());
            setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiarioPagamento());
            setDsValorLimiteDiario(df.format(getValorLimiteDiarioFiltro()));
            setQuantDiasRepiqueFiltro(Integer.toString(saidaDTO
                .getQuantidadeDiaRepiqueConsulta()));
            setCdTratamentoFeriadosFiltro(saidaDTO.getCdPagamentoNaoUtil());
            setCdTipoProcessamentoFiltro(saidaDTO
                .getCdMomentoProcessamentoPagamento());
            setCdTipoRejAgendamentoFiltro(saidaDTO
                .getCdRejeicaoAgendamentoLote());
            setCdTipoRejEfetivacaoFiltro(saidaDTO.getCdRejeicaoEfetivacaoLote());
            setCdPrioridadeDebitoFiltro(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento());
            setCdControlePagFavorecido(saidaDTO
                .getCdUtilizacaoFavorecidoControle());
            setValorMaxPagFavNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setValorMaxPagFavNaoCadastradoDesc(df
                .format(getValorMaxPagFavNaoCadastrado()));
            setPercentualInconsLote(Integer.toString(saidaDTO
                .getPercentualMaximoInconsistenteLote()));
            setQtddMaxInconsLote(Integer.toString(saidaDTO
                .getQuantidadeMaximaInconsistenteLote()));
            setCdPermiteContingenciaPag(saidaDTO.getCdContagemConsultaSaldo());

            // novas
            setQtddDiasFloating(saidaDTO.getQuantidadeDiaFloatingPagamento());
            setCboProcessamentoEfetivacaoPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
            setDsProcessamentoEfetivacaoPagamento(saidaDTO
                .getDsMomentoProcessamentoPagamento());
            setCboTipoRejeicaoAgendamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdRejeicaoAgendamentoLote())));
            setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendamentoLote());
            setCboTipoRejeicaoEfetivacao(Long.parseLong(String.valueOf(saidaDTO
                .getCdRejeicaoEfetivacaoLote())));
            setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
            setQtddMaximaRegistrosInconsistentesLote(saidaDTO
                .getQuantidadeMaximaInconsistenteLote());
            setPercMaxRegInconsistente(saidaDTO
                .getPercentualMaximoInconsistenteLote());
            setCboPrioridadeDebito(Long.parseLong(String.valueOf(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento())));
            setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
            setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO
                .getDsOutraidentificacaoFavorecido());
            setRdoUtilizaCadastroFavorecidoControlePagamentos(String
                .valueOf(saidaDTO.getCdUtilizacaoFavorecidoControle()));
            setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO
                .getCdConsultaSaldoPagamento())));
            setDsTipoConsSaldo(saidaDTO.getDsConsultaSaldoPagamento());
            setQtddDiasRepique(saidaDTO.getQuantidadeDiaRepiqueConsulta());
            setRdoPermiteFavorecidoConsultarPagamento(String.valueOf(saidaDTO
                .getCdFavorecidoConsultaPagamento()));
            //setRdoPermiteContigenciaPagamento(saidaDTO
            //		.getDsContagemConsultaSaldo());
            setRdoGerarLancFutDeb(String.valueOf(saidaDTO
                .getCdLancamentoFuturoDebito()));
            setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO
                .getDsCctciaInscricaoFavorecido());
            setCboTratamentoFeriadosDataPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdPagamentoNaoUtil())));
            setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtil());
            setValorLimiteIndividual(saidaDTO
                .getValorLimiteIndividualPagamento());
            setValorLimiteDiario(saidaDTO.getValorLimiteDiarioPagamento());
        }

        if (dto.getCdParametroTela() == 32) {
            setValorLimiteDiarioFiltro(saidaDTO.getValorLimiteDiarioPagamento());
            setCdEfetivacao(saidaDTO.getCdTipoEfetivacaoPagamento());
            setCdTratamentoFeriadosFiltro(saidaDTO.getCdPagamentoNaoUtil());
            setCdTipoProcessamentoFiltro(saidaDTO
                .getCdMomentoProcessamentoPagamento());
            setCdTratValorDivergente(saidaDTO.getCdTipoDivergenciaVeiculo());
            setCdControlePagFavorecido(saidaDTO
                .getCdUtilizacaoFavorecidoControle());
            setValorMaxPagFavNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setValorMaxPagFavNaoCadastradoDesc(df
                .format(getValorMaxPagFavNaoCadastrado()));
            setCdTipoConsultaSaldoFiltro(saidaDTO.getCdConsultaSaldoPagamento());
            setCdTipoConsisProprietario(saidaDTO
                .getCdCctciaProprietarioVeculo());
            setDsTipoConsisProprietario(saidaDTO
                .getDsCctciaProprietarioVeculo());

            // novas
            setQtddDiasFloating(saidaDTO.getQuantidadeDiaFloatingPagamento());
            setCboProcessamentoEfetivacaoPagamento(Long.valueOf(String
                .valueOf(saidaDTO.getCdMomentoProcessamentoPagamento())));
            setDsProcessamentoEfetivacaoPagamento(saidaDTO
                .getDsMomentoProcessamentoPagamento());
            setCboTipoRejeicaoAgendamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdRejeicaoAgendamentoLote())));
            setDsTipoRejAgendamento(saidaDTO.getDsRejeicaoAgendamentoLote());
            setCboTipoRejeicaoEfetivacao(Long.valueOf(String.valueOf(saidaDTO
                .getCdRejeicaoEfetivacaoLote())));
            setDsTipoRejEfetivacao(saidaDTO.getDsRejeicaoEfetivacaoLote());
            setQtddMaximaRegistrosInconsistentesLote(saidaDTO
                .getQuantidadeMaximaInconsistenteLote());
            setPercMaxRegInconsistente(saidaDTO
                .getPercentualMaximoInconsistenteLote());
            setCboPrioridadeDebito(Long.valueOf(String.valueOf(saidaDTO
                .getCdPrioridadeEfetivacaoPagamento())));
            setDsPrioridDebito(saidaDTO.getDsPrioridadeEfetivacaoPagamento());
            setRdoValidarNomeFavorecidoReceitaFederal(saidaDTO
                .getDsOutraidentificacaoFavorecido());
            setRdoUtilizaCadastroFavorecidoControlePagamentos(String
                .valueOf(saidaDTO.getCdUtilizacaoFavorecidoControle()));
            setValorMaximoPagamentoFavorecidoNaoCadastrado(saidaDTO
                .getValorFavorecidoNaoCadastrado());
            setCboTipoConsultaSaldo(Long.parseLong(String.valueOf(saidaDTO
                .getCdConsultaSaldoPagamento())));
            setDsTipoConsSaldo(saidaDTO.getDsConsultaSaldoPagamento());
            setQtddDiasRepique(saidaDTO.getQuantidadeDiaRepiqueConsulta());
            setRdoPermiteFavorecidoConsultarPagamento(String.valueOf(saidaDTO
                .getCdFavorecidoConsultaPagamento()));
            setRdoGerarLancFutDeb(String.valueOf(saidaDTO
                .getCdLancamentoFuturoDebito()));
            setCboTipoConsistenciaCpfCnpjFavorecido(Long.parseLong(String
                .valueOf(saidaDTO.getCdTipoConsistenciaLista())));
            setDsTipoConsistenciaCpfCnpjFavorecido(saidaDTO
                .getDsCctciaInscricaoFavorecido());
            setCboTratamentoFeriadosDataPagamento(Long.parseLong(String
                .valueOf(saidaDTO.getCdPagamentoNaoUtil())));
            setDsTratFeriadosDataPagamento(saidaDTO.getDsPagamentoNaoUtil());
            //setRdoPermiteContigenciaPagamento(saidaDTO
            //		.getDsContagemConsultaSaldo());
            setCboTratamentoValorDivergente(Long.valueOf(String
                .valueOf(saidaDTO.getCdTipoDivergenciaVeiculo())));
            setDsTratamentoValorDivergente(saidaDTO
                .getDsTipoDivergenciaVeiculo());
            setCboTipoConsistenciaCpfCnpjProprietario(Long.valueOf(String
                .valueOf(saidaDTO.getCdCctciaProprietarioVeculo())));
            setDsTipoConsistenciaCpfCnpjProprietario(saidaDTO
                .getDsCctciaProprietarioVeculo());
            setValorLimiteIndividual(saidaDTO
                .getValorLimiteIndividualPagamento());
            setValorLimiteDiario(saidaDTO.getValorLimiteDiarioPagamento());

        }

        // TRILHA DE AUDITORIA

        if ("".equals(saidaDTO.getCdUsuarioInclusao())
                        || saidaDTO.getCdUsuarioInclusao() == null) {
            setUsuarioInclusao(saidaDTO.getCdUsuarioExternoInclusao());
        } else {
            setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
        }

        if ("".equals(saidaDTO.getCdUsuarioAlteracao())
                        || saidaDTO.getCdUsuarioAlteracao() == null) {
            setUsuarioManutencao(String.valueOf(saidaDTO
                .getCdUsuarioExternoAlteracao()));
        } else {
            setUsuarioManutencao(saidaDTO.getCdUsuarioAlteracao());
        }
        setComplementoInclusao(saidaDTO.getNrOperacaoFluxoInclusao() == null
            || saidaDTO.getNrOperacaoFluxoInclusao().equals("0")
            ? ""
                : saidaDTO.getNrOperacaoFluxoInclusao());
        setComplementoManutencao(saidaDTO.getNrOperacaoFluxoAlteracao() == null
            || saidaDTO.getNrOperacaoFluxoAlteracao().equals("0")
            ? ""
                : saidaDTO.getNrOperacaoFluxoAlteracao());
        setDataHoraInclusao(saidaDTO.getHrManutencaoRegistroInclusao());
        setDataHoraManutencao(saidaDTO.getHrManutencaoRegistroAlteracao());
        setTipoCanalInclusao(saidaDTO.getCdCanalInclusao() == 0 ? "" : saidaDTO
            .getCdCanalInclusao()
            + " - " + saidaDTO.getDsCanalInclusao());
        setTipoCanalManutencao(saidaDTO.getCdCanalAlteracao() == 0
            ? ""
                : saidaDTO.getCdCanalAlteracao() + " - "
                + saidaDTO.getDsCanalAlteracao());
        setDsSituacaoServicoRelacionado(saidaDTO
            .getDsSituacaoServicoRelacionado());
        setCdConsultaSaldoValorSuperior(PgitUtil.verificaIntegerNulo(
            saidaDTO.getCdConsultaSaldoValorSuperior()).longValue());
        setDsConsultaSaldoValorSuperior(PgitUtil.verificaStringNula(saidaDTO
            .getDsConsultaSaldoValorSuperior()));

        setRdoIndicadorSegundaLinhaExtrato(saidaDTO
            .getCdIndicadorSegundaLinha());
        setDsIndicadorSegundaLinhaExtrato(saidaDTO.getCdIndicadorSegundaLinha() == 1
            ? "Sim"
                : "N�o");
        setDsRdoIndicadorSegundaLinhaExtrato(saidaDTO.getDsIndicadorSegundaLinha());
        
    }

    /**
     * Consultar historico modalidade.
     */
    public void consultarHistoricoModalidade() {
        listaGridPesquisaHistoricoModalidade = new ArrayList<ListarConManServicoSaidaDTO>();

        try {
            ListarConManServicoEntradaDTO entrada = new ListarConManServicoEntradaDTO();

            entrada.setCdPessoaJuridicaNegocio(getCdPessoaJuridica() != null
                ? getCdPessoaJuridica()
                    : 0l);
            entrada.setCdTipoContratoNegocio(getCdTipoContrato() != null
                ? getCdTipoContrato()
                    : 0);
            entrada
            .setNrSequenciaContratoNegocio(getNrSequenciaContrato() != null
                ? getNrSequenciaContrato()
                    : 0l);

            // TODO PASSAR MODALIDADE
            entrada
            .setCdProdutoOperacaoRelacionado(getCdTipoModalidadeHistorico() != null
                ? getCdTipoModalidadeHistorico()
                    : 0);
            entrada.setCdProdutoServicoOperacao(getCdTipoServico() != null
                ? getCdTipoServico()
                    : 0);
            entrada.setCdTipoServico(2);
            entrada.setDtInicio(getFiltroDataDe() != null ? FormatarData
                .formataDiaMesAno(getFiltroDataDe()) : "");
            entrada.setDtFim(getFiltroDataAte() != null ? FormatarData
                .formataDiaMesAno(getFiltroDataAte()) : "");

            setListaGridPesquisaHistoricoModalidade(getManterContratoService()
                .listarConManServico(entrada));

            listaControleRadioHistoricoModalidade = new ArrayList<SelectItem>();

            for (int i = 0; i < getListaGridPesquisaHistoricoModalidade()
            .size(); i++) {
                listaControleRadioHistoricoModalidade
                .add(new SelectItem(i, ""));
            }

            setItemSelecionadoListaHistoricoModalidade(null);
            setBtoAcionado(true);
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setItemSelecionadoListaHistoricoModalidade(null);
            setListaControleRadioHistoricoModalidade(null);
            setBtoAcionado(false);
        }
    }

    /**
     * Historico modalidade.
     * 
     * @return the string
     */
    public String historicoModalidade() {
        // Se ningu�m foi selecionado na lista
        if (getItemSelecionadoLista2() == null) {
            limparDadosHistoricoModalidade();
            setCdTipoServico(0);
            setCdTipoModalidadeHistorico(0);
        } else {// Algum registro foi selecionado na lista
            setFiltroDataDe(new Date());
            setFiltroDataAte(new Date());

            carregaListaTipoServico();
            setCdTipoServico(getListaGridPesquisa2().get(
                getItemSelecionadoLista2()).getCdServico());

            listarModalidadeHistorico();
            setCdTipoModalidadeHistorico(getListaGridPesquisa2().get(
                getItemSelecionadoLista2()).getCdModalidade());
            listaGridPesquisaHistoricoModalidade = new ArrayList<ListarConManServicoSaidaDTO>();
            setBtoAcionado(false);
            // consultarHistoricoModalidade();
        }

        return "HISTORICO_MODALIDADE";
    }

    /**
     * Historico ambiente.
     * 
     * @return the string
     */
    public String historicoAmbiente() {
        carregaListaTipoServico();
        limparDadosAmbienteOperacaoHist();
        preencherOpcaoComboTipoServico();

        return "HISTORICO";
    }

    /**
     * Preencher opcao combo tipo servico.
     */
    private void preencherOpcaoComboTipoServico() {
        if (listaAmbiente != null && !listaAmbiente.isEmpty()) {
            if (itemSelecionadoGridAmbiente != null) {
                setTipoServicoSelecionadoHistoricoAmbiente(listaAmbiente.get(
                    itemSelecionadoGridAmbiente).getCdTipoServico());
            } else {
                setTipoServicoSelecionadoHistoricoAmbiente(0);
            }
        } else {
            setTipoServicoSelecionadoHistoricoAmbiente(0);
        }
    }

    /**
     * Limpar dados ambiente operacao hist.
     */
    private void limparDadosAmbienteOperacaoHist() {
        setFiltroDataAte(new Date());
        setFiltroDataDe(new Date());
        participanteSelecionadoHistorico = new ListarParticipantesSaidaDTO();
        setItemSelecionadoListaHistoricoManutencaoAmbiente(null);
        setListaHistoricoManutencaoAmbiente(new ArrayList<ListarConManAmbPartOcorrenciatSaidaDTO>());
        setListaControleHistoricoManutencaoAmbiente(new ArrayList<SelectItem>());
    }

    /**
     * Limpar dados ambiente operacao historico.
     */
    public void limparDadosAmbienteOperacaoHistorico() {
        setTipoServicoSelecionadoHistoricoAmbiente(0);
        setFiltroDataAte(new Date());
        setFiltroDataDe(new Date());
        participanteSelecionadoHistorico = new ListarParticipantesSaidaDTO();
        setItemSelecionadoListaHistoricoManutencaoAmbiente(null);
        setListaHistoricoManutencaoAmbiente(new ArrayList<ListarConManAmbPartOcorrenciatSaidaDTO>());
        setListaControleHistoricoManutencaoAmbiente(new ArrayList<SelectItem>());

    }

    /**
     * Paginar consultar historico manuntencao.
     * 
     * @param e
     *            the e
     */
    public void paginarConsultarHistoricoManuntencao(ActionEvent e) {
        consultarHistoricoManuntencao();
    }

    /**
     * Consultar historico manuntencao.
     */
    public void consultarHistoricoManuntencao() {
        try {
            ListarConManAmbPartEntradaDTO entrada = new ListarConManAmbPartEntradaDTO();

            entrada.setCdPessoa(participanteSelecionadoHistorico
                .getCdParticipante() == null
                ? 0L
                    : participanteSelecionadoHistorico.getCdParticipante());
            entrada.setCdPessoaJuridica(cdPessoaJuridica);
            entrada
            .setCdProdutoOperacaoRelacionamento(tipoServicoSelecionadoHistoricoAmbiente
                .compareTo(0) == 0
                ? 0
                    : tipoServicoSelecionadoHistoricoAmbiente);
            entrada.setCdTipoContrato(cdTipoContrato);
            entrada.setDtFimManutencao(FormatarData
                .formataDiaMesAnoToPdc(filtroDataAte));
            entrada.setDtInicioManutencao(FormatarData
                .formataDiaMesAnoToPdc(filtroDataDe));
            entrada.setNrSequenciaContrato(nrSequenciaContrato);

            listaHistoricoManutencaoAmbiente = manterContratoService
            .listarConManAmbPart(entrada);

            setListaControleHistoricoManutencaoAmbiente(SelectItemUtils
                .criarSelectItems(listaHistoricoManutencaoAmbiente));

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            listaHistoricoManutencaoAmbiente = new ArrayList<ListarConManAmbPartOcorrenciatSaidaDTO>();
            listaControleHistoricoManutencaoAmbiente = new ArrayList<SelectItem>();
        }
    }

    /**
     * Detalhar historico manutencao.
     * 
     * @return the string
     */
    public String detalharHistoricoManutencao() {
        try {
            ConsultarConManAmbPartEntradaDTO entrada = new ConsultarConManAmbPartEntradaDTO();

            listaHistoricoManutencaoAmbiente
            .get(itemSelecionadoListaHistoricoManutencaoAmbiente);
            entrada.setCdPessoa(listaHistoricoManutencaoAmbiente.get(
                itemSelecionadoListaHistoricoManutencaoAmbiente)
                .getCdPessoa());
            entrada.setCdPessoaJuridica(cdPessoaJuridica);
            entrada
            .setCdProdutoOperacaoRelacionamento(listaHistoricoManutencaoAmbiente
                .get(
                    itemSelecionadoListaHistoricoManutencaoAmbiente)
                    .getCdProdutoOperacaoRelacionado());
            entrada
            .setCdProdutoServicoOperacao(listaHistoricoManutencaoAmbiente
                .get(
                    itemSelecionadoListaHistoricoManutencaoAmbiente)
                    .getCdProdutoServicoOperacao());
            entrada
            .setCdRelacionamentoProdutoProduto(listaHistoricoManutencaoAmbiente
                .get(
                    itemSelecionadoListaHistoricoManutencaoAmbiente)
                    .getCdRelacionamentoProdutoProduto());
            entrada.setCdTipoContrato(cdTipoContrato);
            entrada
            .setCdTipoParticipacaoPessoa(listaHistoricoManutencaoAmbiente
                .get(
                    itemSelecionadoListaHistoricoManutencaoAmbiente)
                    .getCdTipoParticipacaoPessoa());
            entrada.setHrInclusaoRegistroHist(listaHistoricoManutencaoAmbiente
                .get(itemSelecionadoListaHistoricoManutencaoAmbiente)
                .getDtHoraManutencao());
            entrada.setNrSequenciaContrato(nrSequenciaContrato);

            ConsultarConManAmbPartSaidaDTO saidaDTO = manterContratoService
            .consultarConManAmbPart(entrada);

            setDsTipoServicoAmbiente(listaHistoricoManutencaoAmbiente.get(
                itemSelecionadoListaHistoricoManutencaoAmbiente)
                .getDsTipoServico());
            setDsAmbienteServico(listaHistoricoManutencaoAmbiente.get(
                itemSelecionadoListaHistoricoManutencaoAmbiente)
                .getDsAmbienteOperacao());

            setDsNomeRazaoSocilaAmbiente(listaHistoricoManutencaoAmbiente.get(
                itemSelecionadoListaHistoricoManutencaoAmbiente)
                .getDsNomeRazaoSocial());

            setCpfCnpjAmbiente(listaHistoricoManutencaoAmbiente.get(
                itemSelecionadoListaHistoricoManutencaoAmbiente)
                .getCpfCpnjFormatado());

            setDsTipoManutencao(listaHistoricoManutencaoAmbiente.get(
                itemSelecionadoListaHistoricoManutencaoAmbiente)
                .getCdIndicadorTipoManutencao());
            // TRILHA DE AUDITORIA
            setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
            setUsuarioManutencao(saidaDTO.getCdUsuarioManutencao());

            setComplementoInclusao("0".equals(saidaDTO
                .getNmOperacaoFluxoInclusao()) ? "" : saidaDTO
                    .getNmOperacaoFluxoInclusao());
            setComplementoManutencao("0".equals(saidaDTO
                .getNmOperacaoFluxoManutencao()) ? "" : saidaDTO
                    .getNmOperacaoFluxoManutencao());
            setDataHoraInclusao(saidaDTO.getHrManutencaRegistroInclusao());
            setDataHoraManutencao(saidaDTO.getHrManutencaoRegistroManutencao());
            setTipoCanalInclusao(saidaDTO.getCdCanalInclusao() == 0
                ? ""
                    : saidaDTO.getCdCanalInclusao() + " - "
                    + saidaDTO.getDsCanalInclusao());
            setTipoCanalManutencao(saidaDTO.getCdCanalManutencao() == 0
                ? ""
                    : saidaDTO.getCdCanalManutencao() + " - "
                    + saidaDTO.getDsCanalManutencao());

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }
        return "DETALHAR";
    }

    /**
     * Listar modalidade historico.
     */
    public void listarModalidadeHistorico() {
        this.listaModalidadeHistorico = new ArrayList<SelectItem>();
        ListarModalidadesEntradaDTO entrada = new ListarModalidadesEntradaDTO();
        List<ListarModalidadeSaidaDTO> listaSaida = new ArrayList<ListarModalidadeSaidaDTO>();

        entrada.setCdServico(getCdTipoServico());

        listaSaida = getComboService().listarModalidades(entrada);
        listaModalidadeHistoricoHash.clear();

        for (ListarModalidadeSaidaDTO combo : listaSaida) {
            listaModalidadeHistoricoHash.put(combo.getCdModalidade(), combo
                .getDsModalidade());
            listaModalidadeHistorico.add(new SelectItem(
                combo.getCdModalidade(), combo.getDsModalidade()));
        }

        setCdTipoModalidadeHistorico(0);
    }

    /*
     * Vivian
     * 
     * teste de versionamento
     */

    /**
     * Listar combos modalidade.
     */
    
   
    public void listarCombosModalidade() {
        ListarModalidadeTipoServicoSaidaDTO modalidadeTipoServicoSaidaDTO = getListaGridPesquisa2()
        .get(getItemSelecionadoLista2());

        if (modalidadeTipoServicoSaidaDTO.getCdModalidade() != 90200023
                        && modalidadeTipoServicoSaidaDTO.getCdModalidade() != 90200069
                        && modalidadeTipoServicoSaidaDTO.getCdModalidade() != 90200072
                        && modalidadeTipoServicoSaidaDTO.getCdModalidade() != 90200073){
            setFlagExibeCampos("S");
        }else{
            setFlagExibeCampos("N");
        }

        switch (modalidadeTipoServicoSaidaDTO.getCdParametroTela()) {
            // Aviso de Recadastramento
            case 15 :
                carregaListaMomentoEnvio();
                carregaListaDestinoEnvio();
                carregaListaCadastroConsultaEnderecoEnvio();
                break;
                // Formulario de Recadastramento
            case 16 :
                carregaListaMomentoEnvio();
                carregaListaDestinoEnvio();
                carregaListaCadastroConsultaEnderecoEnvio();
                break;
                // Pagamentos de Fornecedores - DOC
            case 17 :
            	carregarComboOrigem();
                carregaListaTipoRejeicaoAgendamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                break;
                // Pagamentos de Fornecedores - TED
            case 18 :
                carregaListaTipoRejeicaoAgendamento();
                carregarComboOrigem();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                break;
                // Pagamentos Via Ordem de Pagamento
            case 19 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaTipoInscricaoFavorecido();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                carregaListaOcorrenciaDebito();
                break;
                // Pagamento Via Credito em Conta
            case 20 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTipoTratamentoContaTransferida();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaPermiteEstornoPagamento();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaTipoContaCredito();
                break;
                // Pagamento Via Debito em Conta
            case 21 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                break;
                // Pagamento Via Titulo Bradesco
            case 22 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                break;
                // Pagamento Via Titulo Outros Bancos
            case 23 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                break;
                // Pagamento Via Deposito Identificado
            case 24 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                break;
                // Pagamentos de Fornecedores - OCT
            case 25 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                break;
                // Rastreabilidade de Titulos
            case 26 :
                carregaListaTipoRastreamentoTitulos();
                break;
                // Pagamento Prova de Vida
            case 27 :
                carregaListaAcaoNaoComprovacao();
                carregaListaCadastroConsultaEnderecoEnvio();
                carregaListaDestinoEnvio();
                break;
                // Pagamento DARF
            case 28 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                break;
                // Pagamento GARE
            case 29 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                break;
                // Pagamento GPS
            case 30 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                break;
                // Pagamento Codigo de Barras
            case 31 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                break;
                // Pagamento Debito Veiculo
            case 32 :
                carregaListaTipoRejeicaoAgendamento();
                carregaListaPrioridadeDebito();
                carregaListaTipoConsultaSaldo(false);
                carregaListaTratamentoFeriadosDataPagamento();
                carregaListaTratamentoValorDivergente();
                carregaListaProcessamentoEfetivacaoPagamento();
                carregaListaTipoRejeicaoEfetivacao();
                carregaListaTipoConsistenciaCpfCnpjFavorecido();
                carregaListaTipoConsistenciaCpfCnpjProprietario();
                break;
        }
    }

    /**
     * Controla radio lista debito.
     */
    public void controlaRadioListaDebito() {
        if (getRdoUtilizaListaDebitos().equals("1")) {
            listaTipoFormacaoListaDebito.clear();
            setDesabilitaTipoFormListaDebito(false);
            setDesabilitaTratamentoListaDebitoSemNumeracao(false);
            ConsultarListaValoresDiscretosSaidaDTO saida = listarSetor(4).get(1);
            getListaTipoFormacaoListaDebito().add(new SelectItem(saida.getCdCombo(), saida.getDsCombo()));
            setCboTipoFormacaoListaDebito((Long) listaTipoFormacaoListaDebito.get(0).getValue());	
        } else {
            setDesabilitaTipoFormListaDebito(true);
            setCboTipoFormacaoListaDebito(0L);
            setDesabilitaTratamentoListaDebitoSemNumeracao(true);
            setCboTratamentoListaDebitoSemNumeracao(0L);
        }
    }
    
    public void comboDisabledGerarRetornoTipoManutencao(){
    	if(getComboRetornoOperacoesSelecionado() == 1 ||  getComboRetornoOperacoesSelecionado() == 3){
    		renderizaCombo = 1;    		
    	}else{
    		renderizaCombo = 0;
    		setComboCodIndicadorTipoRetornoInternet(0L);
    	}
    }

    /**
     * Controla rdo gerar retorno separado.
     */
    public void controlaRdoGerarRetornoSeparado() {
        if ("2".equals(getRdoGerarRetornoOperRealizadaInternet())) {
            setDesabilitaGerarRetornoSeparado(false);
        } else {
            setDesabilitaGerarRetornoSeparado(true);
            setRdoGerarRetornoSeparadoCanal("");
        }
    }

    /**
     * Controla tp cartao qtde limite.
     */
    public void controlaTpCartaoQtdeLimite() {
        if (getRdoEmissaoAntCartaoContSal().equals("2")) {
            // Limpa e desabiliata os campos Tipo Cart�o e Quantidade Limite
            // Solicita��o Cart�o Conta Sal�rio
            setCboTipoCartaoContaSalario(0L);
            setDesabilitaTpCartaoQtdeLimite(true);

            setQtddLimiteCartaoContaSalarioSolicitacao(null);
        } else {
            setDesabilitaTpCartaoQtdeLimite(false);
        }
    }

    /**
     * Controlar campos lote.
     */
    public void controlarCamposLote() {
        if (getCboTipoRejeicaoAgendamento() == 3L) {
            setDesabilitarCamposLote(false);
        } else {
            setDesabilitarCamposLote(true);
            setQtddMaximaRegistrosInconsistentesLote(null);
            setPercMaxRegInconsistente(null);
        }
    }

    /**
     * Controlar campo perc lote.
     */
    public void controlarCampoPercLote() {
        if (getQtddMaximaRegistrosInconsistentesLote() != null) {
            if (getQtddMaximaRegistrosInconsistentesLote() != 0) {
                setDesabilitarCamposPercLote(true);
                setPercMaxRegInconsistente(null);
            } else {
                setDesabilitarCamposPercLote(false);
            }
        } else {
            setDesabilitarCamposPercLote(false);
        }
    }

    /**
     * Controlar campo qtde lote.
     */
    public void controlarCampoQtdeLote() {
        if (getPercMaxRegInconsistente() != null) {
            if (getPercMaxRegInconsistente() != 0) {
                setDesabilitarCamposQtdeLote(true);
                setQtddMaximaRegistrosInconsistentesLote(null);
            } else {
                setDesabilitarCamposQtdeLote(false);
            }
        } else {
            setDesabilitarCamposQtdeLote(false);
        }

    }

    /**
     * Controlar campo valor maximo fav nao cad.
     */
    public void controlarCampoValorMaximoFavNaoCad() {
        if (getRdoUtilizaCadastroFavorecidoControlePagamentos().equals("2")) {
            setDesabilitaValorMaximoFavNaoCad(true);
            setValorMaximoPagamentoFavorecidoNaoCadastrado(null);
        } else {
            setDesabilitaValorMaximoFavNaoCad(false);
        }
    }

    /**
     * Controla campos expiracao credito.
     */
    public void controlaCamposExpiracaoCredito() {
        if ("1".equals(getRdoPossuiExpiracaoCredito())) {
            setDesabilitaCamposExpiracaoCredito(false);
        } else {
            setDesabilitaCamposExpiracaoCredito(true);
            setQtddDiasExpiracaoCredito("0");
            setCboContExpCredConta(new Long(0));
        }
    }

    /**
     * Controlar campos emite avisos.
     */
    public void controlarCamposEmiteAvisos() {
        if (getRdoEmiteAvitoCompVida().equals("2")) {
            setDesabilitarCamposEmiteAvisos(true);
            setQtddDiasEmissaoAvisoAntesInicioComprovacao(null);
            setQtddDiasEmissaoAvisoAntesVencimento(null);
            setCboDestinoEnvio(0L);
            setDesabilitarCampoCadastroEndereco(true);
            setCboCadConsultaEndEnvio(0L);
        } else {
            setDesabilitarCamposEmiteAvisos(false);
            controlarCampoCadastroConEndEnvio();
        }
    }

    /**
     * Controlar campo cadastro con end envio.
     */
    public void controlarCampoCadastroConEndEnvio() {
        if (getCboDestinoEnvio() == 2 || getCboDestinoEnvio() == 0) {
            setDesabilitarCampoCadastroEndereco(true);
            setCboCadConsultaEndEnvio(0L);
        } else {
            setDesabilitarCampoCadastroEndereco(false);
        }
    }

    /**
     * Controlar campo qtde lim dias pgto venc.
     */
    public void controlarCampoQtdeLimDiasPgtoVenc() {
        if (getRdoPermitePagarVencido().equals("2")) {
            setDesabilitarCampoQtdeLimDiasPgtoVenc(true);
            setQuantidadeLimiteDiasPagamentoVencido(null);
        } else {
            setDesabilitarCampoQtdeLimDiasPgtoVenc(false);
        }
    }

    /**
     * Controla rdo cadastro procuradores.
     */
    public void controlaRdoCadastroProcuradores() {
        if ("1".equals(getRdoUtilizaCadastroProcuradores())) {
            setDesabilitaCamposProcuradores(false);
        } else {
            setDesabilitaCamposProcuradores(true);
            setRdoEfetuarManutencaoCadastroProcuradores("");
            setCboPeriocidadeManCadProc(0);
            setCboFormaManCadProcurador(new Long(0));
        }
    }

    /**
     * Controla rdo cadastro man cad procuradores.
     */
    public void controlaRdoCadastroManCadProcuradores() {
        if ("1".equals(getRdoEfetuarManutencaoCadastroProcuradores())) {
            setDesabilitaCamposManCadProcuradores(false);
        } else {
            setDesabilitaCamposManCadProcuradores(true);
            setCboPeriocidadeManCadProc(0);
            setCboFormaManCadProcurador(new Long(0));
        }
    }

    /**
     * Controla rdo periodicidade envio remessa man.
     */
    public void controlaRdoPeriodicidadeEnvioRemessaMan() {
        if ("1".equals(getRdoPermiteEnvioRemessaManutencaoCadastro())) {
            setDesabilitaCamposEnvioRemessaMan(false);
        } else {
            setDesabilitaCamposEnvioRemessaMan(true);
            setCboPeriodicidadeEnvioRemessaManutencao(0);
        }
    }

    /**
     * Controla rdo emite msg recad midia online.
     */
    public void controlaRdoEmiteMsgRecadMidiaOnline() {
        if ("1".equals(getRdoEmiteMsgRecadMidia())) {
            setDesabilitaCamposMsgOnline(false);
        } else {
            setDesabilitaCamposMsgOnline(true);
            setCboMidiaMsgRecad(new Long(0));
        }
    }

    /**
     * Controla radio pesquisa debito veiculos.
     */
    public void controlaRadioPesquisaDebitoVeiculos() {
        if (getRdoPesquisaDe().equals("1")) {
            setDesabilitaPeriodicidadePesquisaDebPendVeiculos(false);
        } else {
            setDesabilitaPeriodicidadePesquisaDebPendVeiculos(true);
            setCboPeriodicidadePesquisaDebitosPendentesVeiculos(0);
        }
    }

    /**
     * Listar combos tipo servico.
     */
    public void listarCombosTipoServico() {
        listaModalidadeTipoServicoSaidaDTO = new ListarModalidadeTipoServicoSaidaDTO();
        listaModalidadeTipoServicoSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());
        setFlagExibeCampos("N");
        if(listaModalidadeTipoServicoSaidaDTO.getCdModalidade() != 90200026 && listaModalidadeTipoServicoSaidaDTO.getCdModalidade() != 90200025){
            setFlagExibeCampos("S");
        }

        switch (listaModalidadeTipoServicoSaidaDTO.getCdParametroTela()) {
            // Pagamento de Fornecedores
            case 1 :
                carregaListaFormaEnvioPagamento();
                carregaListaTratamentoListaDebitoNumeracao();
                carregaListaFormaAutorizacaoPagamentos();
                carregaListaTipoDataControleFloating();
                carregaListaTipoFormacaoListaDebito();
                carregaListaTipoConsolidacaoPagamentosComprovante();
                carregarListaRetornoOperacoes();
                carregarComboTipoRetornoInternet();
                break;
                // Pagamento de Sal�rios
            case 2 :
                carregaListaFormaEnvioPagamento();
                carregaListaTratamentoListaDebitoNumeracao();
                carregaListaFormaAutorizacaoPagamentos();
                carregaListaTipoDataControleFloating();
                carregaListaTipoFormacaoListaDebito();
                carregaListaTipoCartaoContaSalario();
                carregaListaTipoConsolidacaoPagamentosComprovante();
                carregarListaRetornoOperacoes();
                carregarComboTipoRetornoInternet();
                break;
                // Pagamentos de Tributos e Contas de Consumo
            case 3 :
                carregaListaFormaEnvioPagamento();
                carregaListaTratamentoListaDebitoNumeracao();
                carregaListaFormaAutorizacaoPagamentos();
                carregaListaTipoDataControleFloating();
                carregaListaTipoFormacaoListaDebito();
                carregaListaTipoConsolidacaoPagamentosComprovante();
                carregarListaRetornoOperacoes();
                carregarComboTipoRetornoInternet();
                break;
                // Pagamento de Benef�cios
            case 4 :
                carregaListaFormaEnvioPagamento();
                carregaListaTratamentoListaDebitoNumeracao();
                carregaListaFormaManutencaoCadastroProcuradores();
                carregaListaMomentoIndicacaoCreditoEfetivado();
                carregaListaFormaAutorizacaoPagamentos();
                carregaListaTipoDataControleFloating();
                carregaListaTipoFormacaoListaDebito();
                carregaListaTipoConsolidacaoPagamentosComprovante();
                carregarListaRetornoOperacoes();
                carregarComboTipoRetornoInternet();
                carregaListaTipoIdentificacaoBeneficiario();
                carregaListaControleExpiracaoCredito();
                carregaListaTratamentoFeriadoFimVigenciaCredito();
                break;
                // Cadastro de Favorecido
            case 5 :
                carregaListaFormaManutencaoCadastroFavorecido();
                carregaListaTipoConsistenciaInscricaoFavorecido();
                break;
                // Aviso de Movimenta��o de D�bito
            case 6 :
                break;
                // Aviso de Movimenta��o de Cr�dito
            case 7 :
                break;
                // Aviso de Movimenta��o ao Pagador
            case 8 :
                carregaListaDestinoEnvio();
                carregaListaCadastroConsultaEnderecoEnvio();
                break;
                // Aviso de Movimenta��o ao Favorecido
            case 9 :
                carregaListaDestinoEnvio();
                carregaListaCadastroConsultaEnderecoEnvio();
                break;
                // Comprovante Pagamento ao Pagador
            case 10 :
                carregaListaDestinoEnvio();
                carregaListaCadastroConsultaEnderecoEnvio();
                break;
                // Comprovante Pagamento ao Favorecido
            case 11 :
                carregaListaDestinoEnvio();
                carregaListaCadastroConsultaEnderecoEnvio();
                break;
                // Emiss�o Comprovantes Diverso
            case 12 :
                carregaListaTipoRejeicaoLote();
                carregaListaMeioDisponibilizacaoCorrentistas();
                carregaListaMeioDisponibilizacaoNaoCorrentistas();
                carregaListaCadastroConsultaEnderecoEnvioComprovantesSalariaisDiversos();
                carregaListaMidiaDisponibilizacaoCorrentistas();
                carregaListaDestinoEnvioComprovantesSalariaisDiversos();
                break;
                // Emiss�o Comprovante Salarial
            case 13 :
                carregaListaTipoRejeicaoLote();
                carregaListaMeioDisponibilizacaoCorrentistas();
                carregaListaMeioDisponibilizacaoNaoCorrentistas();
                carregaListaCadastroConsultaEnderecoEnvioComprovantesSalariaisDiversos();
                carregaListaMidiaDisponibilizacaoCorrentistas();
                carregaListaDestinoEnvioComprovantesSalariaisDiversos();
                break;
                // Servi�o Recadastramento de Benefici�rios
            case 14 :
                carregaListaTipoIdentificacaoBeneficiario();
                carregaListaCondicaoEnquadramento();
                carregaListaCriterioCompostoEnquadramento();
                carregaListaTipoCargaCadastroBeneficiarios();
                carregaListaTipoConsistenciaIdentificacaoBeneficiario();
                carregaListaCriterioPrincipalEnquadramento();
                carregaListaCadastroUtilizadoRecadastramento();
                carregaListaMidiaMensagemRecadastramento();
                break;
        }
    }
    
    private void carregarListaRetornoOperacoes() {
    	setListaRetornoOperacoes(new ArrayList<SelectItem>());
        ConsultarListaValoresDiscretosEntradaDTO entradaListaValoresDiscretos  = new ConsultarListaValoresDiscretosEntradaDTO();
        
        entradaListaValoresDiscretos.setNumeroCombo(58);
        
        List<ConsultarListaValoresDiscretosSaidaDTO> listaSaida = getComboService().consultarListaValoresDiscretos(entradaListaValoresDiscretos);
        
        for (ConsultarListaValoresDiscretosSaidaDTO lista : listaSaida) {
            listaRetornoOperacoes.add(new SelectItem(lista.getCdCombo(), lista.getDsCombo()));
        }
        
        setComboRetornoOperacoesSelecionado(0l);
    }
    
    public void carregarComboTipoRetornoInternet(){
    	setListaCodIndicadorTipoRetornoInternet(new ArrayList<SelectItem>());
    	
    	ConsultarListaValoresDiscretosEntradaDTO entradaDTO = new ConsultarListaValoresDiscretosEntradaDTO();
    	entradaDTO.setNumeroCombo(63);
    	
    	List<ConsultarListaValoresDiscretosSaidaDTO> listaSaida = getComboService().consultarListaValoresDiscretos(entradaDTO);
    	
    	for (ConsultarListaValoresDiscretosSaidaDTO lista : listaSaida) {
    		listaCodIndicadorTipoRetornoInternet.add(new SelectItem(lista.getCdCombo(), lista.getDsCombo()));
        }
    }
    
    
    private void carregarComboOrigem() {
        setListaRetornoOperacoes(new ArrayList<SelectItem>());
        getListaOrigemHash().clear();
        getListaOrigem().clear();
        ConsultarListaValoresDiscretosEntradaDTO entradaListaValoresDiscretos  = new ConsultarListaValoresDiscretosEntradaDTO();
        
        entradaListaValoresDiscretos.setNumeroCombo(62);
        
        List<ConsultarListaValoresDiscretosSaidaDTO> listaSaida = getComboService().consultarConfiguracaoServicoModalidade(entradaListaValoresDiscretos);
        
        for (ConsultarListaValoresDiscretosSaidaDTO combo : listaSaida) {
            listaOrigem.add(new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            
            getListaOrigemHash().put(combo.getCdCombo(), combo.getDsCombo());
            
        }
        
    }
    
    

    public void carregarListaConsistenciaCpfCnpjBenefAvalNpc() {
        setListaConsistenciaCpfCnpjBenefAvalNpc(new ArrayList<SelectItem>());
        getListaConsistenciaCpfCnpjBenefAvalNpcHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(61);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaConsistenciaCpfCnpjBenefAvalNpc().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaConsistenciaCpfCnpjBenefAvalNpcHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }    

    
    /**
     * Listar setor.
     * 
     * @param numeroCombo
     *            the numero combo
     * @return the list< consultar lista valores discretos saida dt o>
     */
    public List<ConsultarListaValoresDiscretosSaidaDTO> listarSetor(
        Integer numeroCombo) {
        try {
            ConsultarListaValoresDiscretosEntradaDTO setorEntradaDTO = new ConsultarListaValoresDiscretosEntradaDTO();
            setorEntradaDTO.setNumeroCombo(numeroCombo);

            return getComboService().consultarListaValoresDiscretos(
                setorEntradaDTO);
        } catch (PdcAdapterFunctionalException p) {
            return new ArrayList<ConsultarListaValoresDiscretosSaidaDTO>();
        }
    }

    /**
     * Carrega combo feriado local.
     */
    private void carregaComboFeriadoLocal() {
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = new ArrayList<ConsultarListaValoresDiscretosSaidaDTO>();
        lista = listarSetor(atribuirEntradaComboFeriadoLocal());

        if (cdParametroTela2.equals(1) || cdParametroTela2.equals(2)) {
            if (isCiclicoOrDiario()) {
                for (int i = 0; i < lista.size(); i++) {
                    ConsultarListaValoresDiscretosSaidaDTO consultarListaValoresDiscretosSaidaDTO = lista
                    .get(i);

                    if ("REJEITAR"
                                    .equalsIgnoreCase(consultarListaValoresDiscretosSaidaDTO
                                        .getDsCombo())) {
                        lista.remove(i);

                        break;

                    }
                }
            }
        } else if (cdParametroTela2.equals(3)) {
            for (int i = 0; i < lista.size(); i++) {
                ConsultarListaValoresDiscretosSaidaDTO consultarListaValoresDiscretosSaidaDTO = lista
                .get(i);

                if (!consultarListaValoresDiscretosSaidaDTO.getDsCombo()
                                .equalsIgnoreCase("ACATAR")
                                && !consultarListaValoresDiscretosSaidaDTO.getDsCombo()
                                .equalsIgnoreCase("REJEITAR")) {
                    lista.remove(i);

                }
            }
        }

        feriadosLocais = new ArrayList<SelectItem>();
        feriadosLocaisHash.clear();
        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            feriadosLocais.add(new SelectItem(Integer.valueOf(String
                .valueOf(PgitUtil.verificaLongNulo(combo.getCdCombo()))),
                combo.getDsCombo()));
            feriadosLocaisHash
            .put(Integer.valueOf(String.valueOf(PgitUtil
                .verificaLongNulo(combo.getCdCombo()))), combo
                .getDsCombo());
        }
    }

    /**
     * Recarregar combom feriado.
     */
    public void recarregarCombomFeriado() {
        configuraComboFeriadoNacional();

        carregaComboFeriadoLocal();
    }

    /**
     * Atribuir entrada combo feriado local.
     * 
     * @return the integer
     */
    private Integer atribuirEntradaComboFeriadoLocal() {

        Integer numeroCombo = Integer.valueOf(0);
        if (TIPO_SERVICO_PAGAMENTO_FORNECEDOR.equals(getCdParametroTela2())
                        || TIPO_SERVICO_PAGAMENTO_SALARIO.equals(getCdParametroTela2())) {
            numeroCombo = Integer.valueOf(56);
        } else if (TIPO_SERVICO_PAGAMENTO_TRIBUTOS
                        .equals(getCdParametroTela2())) {
            numeroCombo = Integer.valueOf(57);
        }

        return numeroCombo;
    }

    /**
     * Carrega lista acao nao comprovacao.
     */
    public void carregaListaAcaoNaoComprovacao() {
        setListaAcaoNaoComprovacao(new ArrayList<SelectItem>());
        getListaAcaoNaoComprovacaoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(43);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaAcaoNaoComprovacao().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaAcaoNaoComprovacaoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista cadastro consulta endereco envio comprovantes salariais
     * diversos.
     */
    public void carregaListaCadastroConsultaEnderecoEnvioComprovantesSalariaisDiversos() {
        setListaCadastroConsultaEnderecoEnvio(new ArrayList<SelectItem>());
        getListaCadastroConsultaEnderecoEnvioHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(26);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaCadastroConsultaEnderecoEnvio().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaCadastroConsultaEnderecoEnvioHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    public void carregaListaDemonstra2LinhaExtrato(Integer parametro) {
    	setListaDemonstra2LinhaExtrato(new ArrayList<SelectItem>());
    	getListaDemonstra2LinhaExtratoHash().clear();
    	List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(parametro);
    	
    	for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
    		getListaDemonstra2LinhaExtrato().add(new SelectItem(Integer.valueOf(combo.getCdCombo().toString()), combo.getDsCombo()));
    		getListaDemonstra2LinhaExtratoHash().put(combo.getCdCombo(),combo.getDsCombo());
    	}
    }

    /**
     * Carrega lista cadastro consulta endereco envio.
     */
    public void carregaListaCadastroConsultaEnderecoEnvio() {
        setListaCadastroConsultaEnderecoEnvio(new ArrayList<SelectItem>());
        getListaCadastroConsultaEnderecoEnvioHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(45);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaCadastroConsultaEnderecoEnvio().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaCadastroConsultaEnderecoEnvioHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista cadastro utilizado recadastramento.
     */
    public void carregaListaCadastroUtilizadoRecadastramento() {
        setListaCadastroUtilizadoRecadastramento(new ArrayList<SelectItem>());
        getListaCadastroUtilizadoRecadastramentoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(17);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaCadastroUtilizadoRecadastramento().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaCadastroUtilizadoRecadastramentoHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista condicao enquadramento.
     */
    public void carregaListaCondicaoEnquadramento() {
        setListaCondicaoEnquadramento(new ArrayList<SelectItem>());
        getListaCondicaoEnquadramentoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(14);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaCondicaoEnquadramento().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaCondicaoEnquadramentoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista controle expiracao credito.
     */
    public void carregaListaControleExpiracaoCredito() {
        setListaControleExpiracaoCredito(new ArrayList<SelectItem>());
        getListaControleExpiracaoCreditoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(10);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaControleExpiracaoCredito().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaControleExpiracaoCreditoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista criterio composto enquadramento.
     */
    public void carregaListaCriterioCompostoEnquadramento() {
        setListaCriterioCompostoEnquadramento(new ArrayList<SelectItem>());
        getListaCriterioCompostoEnquadramentoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(16);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaCriterioCompostoEnquadramento().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaCriterioCompostoEnquadramentoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista criterio principal enquadramento.
     */
    public void carregaListaCriterioPrincipalEnquadramento() {
        setListaCriterioPrincipalEnquadramento(new ArrayList<SelectItem>());
        getListaCriterioPrincipalEnquadramentoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(15);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaCriterioPrincipalEnquadramento().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaCriterioPrincipalEnquadramentoHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista destino envio.
     */
    public void carregaListaDestinoEnvio() {
        setListaDestinoEnvio(new ArrayList<SelectItem>());
        getListaDestinoEnvioHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(44);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaDestinoEnvio().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaDestinoEnvioHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista destino envio comprovantes salariais diversos.
     */
    public void carregaListaDestinoEnvioComprovantesSalariaisDiversos() {
        setListaDestinoEnvioCorrespondencia(new ArrayList<SelectItem>());
        getListaDestinoEnvioCorrespondenciaHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(24);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaDestinoEnvioCorrespondencia().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaDestinoEnvioCorrespondenciaHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista forma manutencao cadastro procuradores.
     */
    public void carregaListaFormaManutencaoCadastroProcuradores() {
        setListaFormaManutencaoCadastroProcuradores(new ArrayList<SelectItem>());
        getListaFormaManutencaoCadastroProcuradoresHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(9);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaFormaManutencaoCadastroProcuradores().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaFormaManutencaoCadastroProcuradoresHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista forma autorizacao pagamentos.
     */
    public void carregaListaFormaAutorizacaoPagamentos() {
        setListaFormaAutorizacaoPagamentos(new ArrayList<SelectItem>());
        getListaFormaAutorizacaoPagamentosHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(3);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaFormaAutorizacaoPagamentos().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaFormaAutorizacaoPagamentosHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista forma envio pagamento.
     */
    public void carregaListaFormaEnvioPagamento() {
        setListaFormaEnvioPagamento(new ArrayList<SelectItem>());
        getListaFormaEnvioPagamentoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(2);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaFormaEnvioPagamento().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaFormaEnvioPagamentoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista forma manutencao cadastro favorecido.
     */
    public void carregaListaFormaManutencaoCadastroFavorecido() {
        setListaFormaManutencaoCadastroFavorecido(new ArrayList<SelectItem>());
        getListaFormaManutencaoCadastroFavorecidoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(27);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaFormaManutencaoCadastroFavorecido().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaFormaManutencaoCadastroFavorecidoHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista meio disponibilizacao nao correntistas.
     */
    public void carregaListaMeioDisponibilizacaoNaoCorrentistas() {
        setListaMeioDisponibilizacaoNaoCorrentistas(new ArrayList<SelectItem>());
        getListaMeioDisponibilizacaoNaoCorrentistasHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(23);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaMeioDisponibilizacaoNaoCorrentistas().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaMeioDisponibilizacaoNaoCorrentistasHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista meio disponibilizacao correntistas.
     */
    public void carregaListaMeioDisponibilizacaoCorrentistas() {
        setListaMeioDisponibilizacaoCorrentistas(new ArrayList<SelectItem>());
        getListaMeioDisponibilizacaoCorrentistasHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(25);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaMeioDisponibilizacaoCorrentistas().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaMeioDisponibilizacaoCorrentistasHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista momento indicacao credito efetivado.
     */
    public void carregaListaMomentoIndicacaoCreditoEfetivado() {
        setListaMomentoIndicacaoCreditoEfetivado(new ArrayList<SelectItem>());
        getListaMomentoIndicacaoCreditoEfetivadoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(11);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaMomentoIndicacaoCreditoEfetivado().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaMomentoIndicacaoCreditoEfetivadoHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista momento envio.
     */
    
    
    public void carregaListaMomentoEnvio() {
        setListaMomentoEnvio(new ArrayList<SelectItem>());
        getListaMomentoEnvioHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(46);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {getListaMomentoEnvio().add(new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaMomentoEnvioHash().put(combo.getCdCombo(), combo.getDsCombo());
        }
    }
    

    /**
     * Carrega lista midia mensagem recadastramento.
     */
    public void carregaListaMidiaMensagemRecadastramento() {
        setListaMidiaMensagemRecadastramento(new ArrayList<SelectItem>());
        getListaMidiaMensagemRecadastramentoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(19);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaMidiaMensagemRecadastramento().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaMidiaMensagemRecadastramentoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista midia disponibilizacao correntistas.
     */
    public void carregaListaMidiaDisponibilizacaoCorrentistas() {
        setListaMidiaDisponibilizacaoCorrentistas(new ArrayList<SelectItem>());
        getListaMidiaDisponibilizacaoCorrentistasHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(22);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaMidiaDisponibilizacaoCorrentistas().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaMidiaDisponibilizacaoCorrentistasHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista ocorrencia debito.
     */
    public void carregaListaOcorrenciaDebito() {
        setListaOcorrenciaDebito(new ArrayList<SelectItem>());
        getListaOcorrenciaDebitoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(35);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaOcorrenciaDebito().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaOcorrenciaDebitoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista permite estorno pagamento.
     */
    public void carregaListaPermiteEstornoPagamento() {
        setListaPermiteEstornoPagamento(new ArrayList<SelectItem>());
        getListaPermiteEstornoPagamentoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(36);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaPermiteEstornoPagamento().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaPermiteEstornoPagamentoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista prioridade debito.
     */
    public void carregaListaPrioridadeDebito() {
        setListaPrioridadeDebito(new ArrayList<SelectItem>());
        getListaPrioridadeDebitoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(33);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaPrioridadeDebito().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaPrioridadeDebitoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista processamento efetivacao pagamento.
     */
    public void carregaListaProcessamentoEfetivacaoPagamento() {
        setListaProcessamentoEfetivacaoPagamento(new ArrayList<SelectItem>());
        getListaProcessamentoEfetivacaoPagamentoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista;
        Integer parametroTela = listaGridPesquisa2.get(itemSelecionadoLista2)
        .getCdParametroTela();

        if (parametroTela == 29 || parametroTela == 30) {
            lista = listarSetor(55);
        } else {
            lista = listarSetor(30);
        }

        if (cdParametroTela2.equals(3)) {
            for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
                getListaProcessamentoEfetivacaoPagamento().add(
                    new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
                getListaProcessamentoEfetivacaoPagamentoHash().put(
                    combo.getCdCombo(), combo.getDsCombo());
            }
        } else {
            for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
                if (combo.getCdCombo() != 3 && combo.getCdCombo() != 4) {
                    getListaProcessamentoEfetivacaoPagamento().add(
                        new SelectItem(combo.getCdCombo(), combo
                            .getDsCombo()));
                    getListaProcessamentoEfetivacaoPagamentoHash().put(
                        combo.getCdCombo(), combo.getDsCombo());
                }
            }
        }
    }

    /**
     * Carrega lista tipo cartao conta salario.
     */
    public void carregaListaTipoCartaoContaSalario() {
        setListaTipoCartaoContaSalario(new ArrayList<SelectItem>());
        getListaTipoCartaoContaSalarioHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(7);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoCartaoContaSalario().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoCartaoContaSalarioHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo consistencia identificacao beneficiario.
     */
    public void carregaListaTipoConsistenciaIdentificacaoBeneficiario() {
        setListaTipoConsistenciaIdentificacaoBeneficiario(new ArrayList<SelectItem>());
        getListaTipoConsistenciaIdentificacaoBeneficiarioHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(13);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoConsistenciaIdentificacaoBeneficiario().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoConsistenciaIdentificacaoBeneficiarioHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo identificacao beneficiario.
     */
    public void carregaListaTipoIdentificacaoBeneficiario() {
        setListaTipoIdentificacaoBeneficiario(new ArrayList<SelectItem>());
        getListaTipoIdentificacaoBeneficiarioHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(8);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoIdentificacaoBeneficiario().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoIdentificacaoBeneficiarioHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo carga cadastro beneficiarios.
     */
    public void carregaListaTipoCargaCadastroBeneficiarios() {
        setListaTipoCargaCadastroBeneficiarios(new ArrayList<SelectItem>());
        getListaTipoCargaCadastroBeneficiariosHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(18);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoCargaCadastroBeneficiarios().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoCargaCadastroBeneficiariosHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo consistencia cpf cnpj proprietario.
     */
    public void carregaListaTipoConsistenciaCpfCnpjProprietario() {
        setListaTipoConsistenciaCpfCnpjProprietario(new ArrayList<SelectItem>());
        getListaTipoConsistenciaCpfCnpjProprietarioHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(42);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoConsistenciaCpfCnpjProprietario().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoConsistenciaCpfCnpjProprietarioHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo consistencia inscricao favorecido.
     */
    public void carregaListaTipoConsistenciaInscricaoFavorecido() {
        setListaTipoConsistenciaInscricaoFavorecido(new ArrayList<SelectItem>());
        getListaTipoConsistenciaInscricaoFavorecidoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(28);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoConsistenciaInscricaoFavorecido().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoConsistenciaInscricaoFavorecidoHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo consistencia cpf cnpj favorecido.
     */
    public void carregaListaTipoConsistenciaCpfCnpjFavorecido() {
        setListaTipoConsistenciaCpfCnpjFavorecido(new ArrayList<SelectItem>());
        getListaTipoConsistenciaCpfCnpjFavorecidoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(38);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoConsistenciaCpfCnpjFavorecido().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoConsistenciaCpfCnpjFavorecidoHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo consolidacao pagamentos comprovante.
     */
    public void carregaListaTipoConsolidacaoPagamentosComprovante() {
        setListaTipoConsolidacaoPagamentosComprovante(new ArrayList<SelectItem>());
        getListaTipoConsolidacaoPagamentosComprovanteHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = new ArrayList<ConsultarListaValoresDiscretosSaidaDTO>();

        if (listaGridPesquisa.get(itemSelecionadoLista).getCdParametroTela() == 3) {
            lista = listarSetor(50);
        } else {
            lista = listarSetor(54);
        }

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoConsolidacaoPagamentosComprovante().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoConsolidacaoPagamentosComprovanteHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo consulta saldo.
     * 
     * @param debitoConta
     *            the debito conta
     */
    public void carregaListaTipoConsultaSaldo(boolean debitoConta) {
        setListaTipoConsultaSaldo(new ArrayList<SelectItem>());
        getListaTipoConsultaSaldoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(51);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            if (debitoConta) {
                if (combo.getCdCombo() == 1) {
                    getListaTipoConsultaSaldo().add(
                        new SelectItem(combo.getCdCombo(), combo
                            .getDsCombo()));
                    getListaTipoConsultaSaldoHash().put(combo.getCdCombo(),
                        combo.getDsCombo());
                }
            } else {
                getListaTipoConsultaSaldo().add(
                    new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
                getListaTipoConsultaSaldoHash().put(combo.getCdCombo(),
                    combo.getDsCombo());
            }

        }
    }

    /**
     * Carrega lista tipo conta credito.
     */
    public void carregaListaTipoContaCredito() {
        setListaTipoContaCredito(new ArrayList<SelectItem>());
        getListaTipoContaCreditoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(39);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoContaCredito().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoContaCreditoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo data controle floating.
     */
    public void carregaListaTipoDataControleFloating() {
        setListaTipoDataControleFloating(new ArrayList<SelectItem>());
        getListaTipoDataControleFloatingHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(6);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoDataControleFloating().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoDataControleFloatingHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo formacao lista debito.
     */
    public void carregaListaTipoFormacaoListaDebito() {
        setListaTipoFormacaoListaDebito(new ArrayList<SelectItem>());
        getListaTipoFormacaoListaDebitoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(4);		

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            if (cdParametroTela == 1 || cdParametroTela == 2
                            || cdParametroTela == 3) {			

                if (combo.getCdCombo() == 2) {							
                    getListaTipoFormacaoListaDebito().add(
                        new SelectItem(combo.getCdCombo(), combo
                            .getDsCombo()));
                    getListaTipoFormacaoListaDebitoHash().put(
                        combo.getCdCombo(), combo.getDsCombo());
                }
            } else {			
                getListaTipoFormacaoListaDebito().add(
                    new SelectItem(combo.getCdCombo(), combo
                        .getDsCombo()));
                getListaTipoFormacaoListaDebitoHash().put(combo.getCdCombo(),
                    combo.getDsCombo());
            }
        }
    }

    /**
     * Carrega lista tipo inscricao favorecido.
     */
    public void carregaListaTipoInscricaoFavorecido() {
        setListaTipoInscricaoFavorecido(new ArrayList<SelectItem>());
        getListaTipoInscricaoFavorecidoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(34);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoInscricaoFavorecido().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoInscricaoFavorecidoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo rastreamento titulos.
     */
    public void carregaListaTipoRastreamentoTitulos() {
        setListaTipoRastreamentoTitulos(new ArrayList<SelectItem>());
        getListaTipoRastreamentoTitulosHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(40);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoRastreamentoTitulos().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoRastreamentoTitulosHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo rejeicao lote.
     */
    public void carregaListaTipoRejeicaoLote() {
        setListaTipoRejeicaoLote(new ArrayList<SelectItem>());
        getListaTipoRejeicaoLoteHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(20);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoRejeicaoLote().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoRejeicaoLoteHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo rejeicao efetivacao.
     */
    public void carregaListaTipoRejeicaoEfetivacao() {
        setListaTipoRejeicaoEfetivacao(new ArrayList<SelectItem>());
        getListaTipoRejeicaoEfetivacaoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(32);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoRejeicaoEfetivacao().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoRejeicaoEfetivacaoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo rejeicao agendamento.
     */
    
    public void carregaListaTipoRejeicaoAgendamento() {
        setListaTipoRejeicaoAgendamento(new ArrayList<SelectItem>());
        getListaTipoRejeicaoAgendamentoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(31);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoRejeicaoAgendamento().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoRejeicaoAgendamentoHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tipo tratamento conta transferida.
     */
    public void carregaListaTipoTratamentoContaTransferida() {
        setListaTipoTratamentoContaTransferida(new ArrayList<SelectItem>());
        getListaTipoTratamentoContaTransferidaHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(37);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTipoTratamentoContaTransferida().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTipoTratamentoContaTransferidaHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tratamento feriado fim vigencia credito.
     */
    public void carregaListaTratamentoFeriadoFimVigenciaCredito() {
        setListaTratamentoFeriadoFimVigenciaCredito(new ArrayList<SelectItem>());
        getListaTratamentoFeriadoFimVigenciaCreditoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(12);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTratamentoFeriadoFimVigenciaCredito().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTratamentoFeriadoFimVigenciaCreditoHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tratamento lista debito numeracao.
     */
    public void carregaListaTratamentoListaDebitoNumeracao() {
        setListaTratamentoListaDebitoNumeracao(new ArrayList<SelectItem>());
        getListaTratamentoListaDebitoNumeracaoHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(5);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTratamentoListaDebitoNumeracao().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTratamentoListaDebitoNumeracaoHash().put(
                combo.getCdCombo(), combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tratamento valor divergente.
     */
    public void carregaListaTratamentoValorDivergente() {
        setListaTratamentoValorDivergente(new ArrayList<SelectItem>());
        getListaTratamentoValorDivergenteHash().clear();
        List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(41);

        for (ConsultarListaValoresDiscretosSaidaDTO combo : lista) {
            getListaTratamentoValorDivergente().add(
                new SelectItem(combo.getCdCombo(), combo.getDsCombo()));
            getListaTratamentoValorDivergenteHash().put(combo.getCdCombo(),
                combo.getDsCombo());
        }
    }

    /**
     * Carrega lista tratamento feriados data pagamento.
     */
    public void carregaListaTratamentoFeriadosDataPagamento() {
        setListaTratamentoFeriadosDataPagamento(new ArrayList<SelectItem>());
        getListaTratamentoFeriadosDataPagamentoHash().clear();
        listaFeriadoNacional = listarSetor(12);

        adicionaComboFeriadoNacional(listaFeriadoNacional);
    }
    
    public void carregarTeste() {
        setListaTratamentoFeriadosDataPagamento(new ArrayList<SelectItem>());
        getListaTratamentoFeriadosDataPagamentoHash().clear();
        listaComboTeste = listarSetor(62);

        adicionaComboFeriadoNacional1(listaComboTeste);
    }

    /*
     * Fim Vivian
     */

    // Fim M�todos

    /**
     * Alterar ocorrencia debito.
     */
    public void alterarOcorrenciaDebito() {
        if (SEM_CONSULTA.equals(cboTipoConsultaSaldo)) {
            setCboOcorrenciaDebito(0L);
        } else {
            setCboOcorrenciaDebito(DISPONIBILIZACAO_PAGAMENTO);
        }
    }

    /**
     * Is exibe combo ocorr debito desabilitado.
     * 
     * @return true, if is exibe combo ocorr debito desabilitado
     */
    public boolean isExibeComboOcorrDebitoDesabilitado() {
        return Long.valueOf(0).equals(cboTipoConsultaSaldo)
        || Long.valueOf(4).equals(cboTipoConsultaSaldo);
    }

    /**
     * Is desabilitar combo ocorrencia debito.
     * 
     * @return true, if is desabilitar combo ocorrencia debito
     */
    public boolean isDesabilitarComboOcorrenciaDebito() {
        if (saidaVerificarAtributo != null) {
            if (!COD_IND_MOMENTO_DEBITO_PGTO_S.equals(saidaVerificarAtributo
                .getCdIndicadorMomentoDebitoPagamento())) {
                return true;
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Is habilita tipo cons saldo titulo valor superior.
     * 
     * @return true, if is habilita tipo cons saldo titulo valor superior
     */
    public boolean isHabilitaTipoConsSaldoTituloValorSuperior() {
        final Integer tituloOutrosBanco = 23;

        if (listaGridPesquisa2 != null && !listaGridPesquisa2.isEmpty()
                        && itemSelecionadoLista2 != null) {
            ListarModalidadeTipoServicoSaidaDTO modalidadeSelecionada = listaGridPesquisa2
            .get(itemSelecionadoLista2);

            return tituloOutrosBanco.equals(modalidadeSelecionada
                .getCdParametroTela());
        }

        return false;
    }

    /**
     * Is Habilita Campo Permite Estorno Pagamento.
     * 
     * @return true, if is habilita campo permite estorno pagamento
     */
    public boolean isHabilitaCampoPermiteEstornoPagamento() {
        if (cdTipoModalidadeHistorico != null && cdTipoModalidadeHistorico != 0) {
            if (cdTipoModalidadeHistorico == 90200034) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    // Set's e Get's
    
    public Long getCboConsistenciaCpfCnpjBenefAvalNpc() {
		return cboConsistenciaCpfCnpjBenefAvalNpc;
	}

	public void setCboConsistenciaCpfCnpjBenefAvalNpc(
			Long cboConsistenciaCpfCnpjBenefAvalNpc) {
		this.cboConsistenciaCpfCnpjBenefAvalNpc = cboConsistenciaCpfCnpjBenefAvalNpc;
	}

	public List<SelectItem> getListaConsistenciaCpfCnpjBenefAvalNpc() {
		return listaConsistenciaCpfCnpjBenefAvalNpc;
	}

    public Map<Long, String> getListaConsistenciaCpfCnpjBenefAvalNpcHash() {
        return listaConsistenciaCpfCnpjBenefAvalNpcHash;
    }

    public void setListaConsistenciaCpfCnpjBenefAvalNpcHash(
        Map<Long, String> listaTipoConsistenciaCpfCnpjFavorecidoHash) {
        this.listaConsistenciaCpfCnpjBenefAvalNpcHash = listaConsistenciaCpfCnpjBenefAvalNpcHash;
    }
	
	public void setListaConsistenciaCpfCnpjBenefAvalNpc(
			List<SelectItem> listaConsistenciaCpfCnpjBenefAvalNpc) {
		this.listaConsistenciaCpfCnpjBenefAvalNpc = listaConsistenciaCpfCnpjBenefAvalNpc;
	}

	public String getDsConsistenciaCpfCnpjBenefAvalNpc() {
		return dsConsistenciaCpfCnpjBenefAvalNpc;
	}

	public void setDsConsistenciaCpfCnpjBenefAvalNpc(
			String dsConsistenciaCpfCnpjBenefAvalNpc) {
		this.dsConsistenciaCpfCnpjBenefAvalNpc = dsConsistenciaCpfCnpjBenefAvalNpc;
	}
  
    /**
     * Gets the saida dto.
     *
     * @return the saida dto
     */
    public DetalharTipoServModContratoSaidaDTO getSaidaDTO() {
        return saidaDTO;
    }

    /**
     * Sets the saida dto.
     *
     * @param saidaDTO the new saida dto
     */
    public void setSaidaDTO(DetalharTipoServModContratoSaidaDTO saidaDTO) {
        this.saidaDTO = saidaDTO;
    }
    
    /**
     * Set: comboService.
     * 
     * @param iComboService
     *            the combo service
     */
    public void setComboService(IComboService iComboService) {
        this.comboService = iComboService;
    }

    /**
     * Get: comboService.
     * 
     * @return comboService
     */
    public IComboService getComboService() {
        return comboService;
    }

    /**
     * Set: listaGridPesquisa.
     * 
     * @param lista
     *            the lista grid pesquisa
     */
    public void setListaGridPesquisa(
        List<ListarModalidadeTipoServicoSaidaDTO> lista) {
        this.listaGridPesquisa = lista;
    }

    /**
     * Get: listaGridPesquisa.
     * 
     * @return listaGridPesquisa
     */
    public List<ListarModalidadeTipoServicoSaidaDTO> getListaGridPesquisa() {
        return listaGridPesquisa;
    }

    /**
     * Set: itemSelecionadoLista.
     * 
     * @param itemSelecionado
     *            the item selecionado lista
     */
    public void setItemSelecionadoLista(Integer itemSelecionado) {
        this.itemSelecionadoLista = itemSelecionado;
    }

    /**
     * Get: itemSelecionadoLista.
     * 
     * @return itemSelecionadoLista
     */
    public Integer getItemSelecionadoLista() {
        return itemSelecionadoLista;
    }

    /**
     * Set: listaControleRadio.
     * 
     * @param lista
     *            the lista controle radio
     */
    public void setListaControleRadio(List<SelectItem> lista) {
        this.listaControleRadio = lista;
    }

    /**
     * Get: listaControleRadio.
     * 
     * @return listaControleRadio
     */
    public List<SelectItem> getListaControleRadio() {
        return listaControleRadio;
    }

    /**
     * Pesquisar.
     * 
     * @param actionEvent
     *            the action event
     */
    public void pesquisar(javax.faces.event.ActionEvent actionEvent) {
        consultarHistorico();
    }

    /**
     * Pesquisar servico.
     * 
     * @param actionEvent
     *            the action event
     */
    public void pesquisarServico(javax.faces.event.ActionEvent actionEvent) {
        incluir();
    }

    /**
     * Get: manterContratoService.
     * 
     * @return manterContratoService
     */
    public IManterContratoService getManterContratoService() {
        return manterContratoService;
    }

    /**
     * Set: manterContratoService.
     * 
     * @param contratoService
     *            the manter contrato service
     */
    public void setManterContratoService(IManterContratoService contratoService) {
        this.manterContratoService = contratoService;
    }

    /**
     * Get: cdPessoaJuridica.
     * 
     * @return cdPessoaJuridica
     */
    public Long getCdPessoaJuridica() {
        return cdPessoaJuridica;
    }

    /**
     * Set: cdPessoaJuridica.
     * 
     * @param codPessoaJuridica
     *            the cd pessoa juridica
     */
    public void setCdPessoaJuridica(Long codPessoaJuridica) {
        this.cdPessoaJuridica = codPessoaJuridica;
    }

    /**
     * Get: cdTipoContrato.
     * 
     * @return cdTipoContrato
     */
    public Integer getCdTipoContrato() {
        return cdTipoContrato;
    }

    /**
     * Set: cdTipoContrato.
     * 
     * @param codTipoContrato
     *            the cd tipo contrato
     */
    public void setCdTipoContrato(Integer codTipoContrato) {
        this.cdTipoContrato = codTipoContrato;
    }

    /**
     * Get: nrSequenciaContrato.
     * 
     * @return nrSequenciaContrato
     */
    public Long getNrSequenciaContrato() {
        return nrSequenciaContrato;
    }

    /**
     * Set: nrSequenciaContrato.
     * 
     * @param nrSequencia
     *            the nr sequencia contrato
     */
    public void setNrSequenciaContrato(Long nrSequencia) {
        this.nrSequenciaContrato = nrSequencia;
    }

    /**
     * Get: itemSelecionadoListaTipoServico.
     * 
     * @return itemSelecionadoListaTipoServico
     */
    public Integer getItemSelecionadoListaTipoServico() {
        return itemSelecionadoListaTipoServico;
    }

    /**
     * Set: itemSelecionadoListaTipoServico.
     * 
     * @param itemSelecionado
     *            the item selecionado lista tipo servico
     */
    public void setItemSelecionadoListaTipoServico(Integer itemSelecionado) {
        this.itemSelecionadoListaTipoServico = itemSelecionado;
    }

    /**
     * Get: listaControleRadioTipoServico.
     * 
     * @return listaControleRadioTipoServico
     */
    public List<SelectItem> getListaControleRadioTipoServico() {
        return listaControleRadioTipoServico;
    }

    /**
     * Set: listaControleRadioTipoServico.
     * 
     * @param lista
     *            the lista controle radio tipo servico
     */
    public void setListaControleRadioTipoServico(List<SelectItem> lista) {
        this.listaControleRadioTipoServico = lista;
    }

    /**
     * Get: listaGridTipoServico.
     * 
     * @return listaGridTipoServico
     */
    public List<ListarServicosSaidaDTO> getListaGridTipoServico() {
        return listaGridTipoServico;
    }

    /**
     * Set: listaGridTipoServico.
     * 
     * @param listaGridTipoServico
     *            the lista grid tipo servico
     */
    public void setListaGridTipoServico(
        List<ListarServicosSaidaDTO> listaGridTipoServico) {
        this.listaGridTipoServico = listaGridTipoServico;
    }

    /**
     * Get: listaControleCheckModalidade.
     * 
     * @return listaControleCheckModalidade
     */
    public List<SelectItem> getListaControleCheckModalidade() {
        return listaControleCheckModalidade;
    }

    /**
     * Set: listaControleCheckModalidade.
     * 
     * @param listaControleRadioModalidade
     *            the lista controle check modalidade
     */
    public void setListaControleCheckModalidade(
        List<SelectItem> listaControleRadioModalidade) {
        this.listaControleCheckModalidade = listaControleRadioModalidade;
    }

    /**
     * Get: dsTipoServicoSelecionado.
     * 
     * @return dsTipoServicoSelecionado
     */
    public String getDsTipoServicoSelecionado() {
        return dsTipoServicoSelecionado;
    }

    /**
     * Set: dsTipoServicoSelecionado.
     * 
     * @param tipoServicoSelecionado
     *            the ds tipo servico selecionado
     */
    public void setDsTipoServicoSelecionado(String tipoServicoSelecionado) {
        this.dsTipoServicoSelecionado = tipoServicoSelecionado;
    }

    /**
     * Is btn incluir.
     * 
     * @return true, if is btn incluir
     */
    public boolean isBtnIncluir() {
        return btnIncluir;
    }

    /**
     * Set: btnIncluir.
     * 
     * @param btnIncluir
     *            the btn incluir
     */
    public void setBtnIncluir(boolean btnIncluir) {
        this.btnIncluir = btnIncluir;
    }

    /**
     * Get: countCheckedModalidade.
     * 
     * @return countCheckedModalidade
     */
    public Integer getCountCheckedModalidade() {
        return countCheckedModalidade;
    }

    /**
     * Set: countCheckedModalidade.
     * 
     * @param countCheckedModalidade
     *            the count checked modalidade
     */
    public void setCountCheckedModalidade(Integer countCheckedModalidade) {
        this.countCheckedModalidade = countCheckedModalidade;
    }

    /**
     * Get: listaGridServicoRelacionado.
     * 
     * @return listaGridServicoRelacionado
     */
    public List<ListarModalidadeTipoServicoSaidaDTO> getListaGridServicoRelacionado() {
        return listaGridServicoRelacionado;
    }

    /**
     * Set: listaGridServicoRelacionado.
     * 
     * @param listaGridServicoRelacionado
     *            the lista grid servico relacionado
     */
    public void setListaGridServicoRelacionado(
        List<ListarModalidadeTipoServicoSaidaDTO> listaGridServicoRelacionado) {
        this.listaGridServicoRelacionado = listaGridServicoRelacionado;
    }

    /**
     * Get: complementoInclusao.
     * 
     * @return complementoInclusao
     */
    public String getComplementoInclusao() {
        return complementoInclusao;
    }

    /**
     * Set: complementoInclusao.
     * 
     * @param complementoInclusao
     *            the complemento inclusao
     */
    public void setComplementoInclusao(String complementoInclusao) {
        this.complementoInclusao = complementoInclusao;
    }

    /**
     * Get: complementoManutencao.
     * 
     * @return complementoManutencao
     */
    public String getComplementoManutencao() {
        return complementoManutencao;
    }

    /**
     * Set: complementoManutencao.
     * 
     * @param complementoManutencao
     *            the complemento manutencao
     */
    public void setComplementoManutencao(String complementoManutencao) {
        this.complementoManutencao = complementoManutencao;
    }

    /**
     * Get: dataHoraInclusao.
     * 
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
        return dataHoraInclusao;
    }

    /**
     * Set: dataHoraInclusao.
     * 
     * @param dataHoraInclusao
     *            the data hora inclusao
     */
    public void setDataHoraInclusao(String dataHoraInclusao) {
        this.dataHoraInclusao = dataHoraInclusao;
    }

    /**
     * Get: dataHoraManutencao.
     * 
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
        return dataHoraManutencao;
    }

    /**
     * Set: dataHoraManutencao.
     * 
     * @param dataHoraManutencao
     *            the data hora manutencao
     */
    public void setDataHoraManutencao(String dataHoraManutencao) {
        this.dataHoraManutencao = dataHoraManutencao;
    }

    /**
     * Get: tipoCanalInclusao.
     * 
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
        return tipoCanalInclusao;
    }

    /**
     * Set: tipoCanalInclusao.
     * 
     * @param tipoCanalInclusao
     *            the tipo canal inclusao
     */
    public void setTipoCanalInclusao(String tipoCanalInclusao) {
        this.tipoCanalInclusao = tipoCanalInclusao;
    }

    /**
     * Get: tipoCanalManutencao.
     * 
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
        return tipoCanalManutencao;
    }

    /**
     * Set: tipoCanalManutencao.
     * 
     * @param tipoCanalManutencao
     *            the tipo canal manutencao
     */
    public void setTipoCanalManutencao(String tipoCanalManutencao) {
        this.tipoCanalManutencao = tipoCanalManutencao;
    }

    /**
     * Get: usuarioInclusao.
     * 
     * @return usuarioInclusao
     */
    public String getUsuarioInclusao() {
        return usuarioInclusao;
    }

    /**
     * Set: usuarioInclusao.
     * 
     * @param usuarioInclusao
     *            the usuario inclusao
     */
    public void setUsuarioInclusao(String usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    /**
     * Get: usuarioManutencao.
     * 
     * @return usuarioManutencao
     */
    public String getUsuarioManutencao() {
        return usuarioManutencao;
    }

    /**
     * Set: usuarioManutencao.
     * 
     * @param usuarioManutencao
     *            the usuario manutencao
     */
    public void setUsuarioManutencao(String usuarioManutencao) {
        this.usuarioManutencao = usuarioManutencao;
    }

    /**
     * Get: cdTipoServicoSelecionado.
     * 
     * @return cdTipoServicoSelecionado
     */
    public Integer getCdTipoServicoSelecionado() {
        return cdTipoServicoSelecionado;
    }

    /**
     * Set: cdTipoServicoSelecionado.
     * 
     * @param cdTipoServicoSelecionado
     *            the cd tipo servico selecionado
     */
    public void setCdTipoServicoSelecionado(Integer cdTipoServicoSelecionado) {
        this.cdTipoServicoSelecionado = cdTipoServicoSelecionado;
    }

    /**
     * Get: dsFormaAutorizacaoPagamento.
     * 
     * @return dsFormaAutorizacaoPagamento
     */
    public String getDsFormaAutorizacaoPagamento() {
        return dsFormaAutorizacaoPagamento;
    }

    /**
     * Set: dsFormaAutorizacaoPagamento.
     * 
     * @param dsFormaAutorizacaoPagamento
     *            the ds forma autorizacao pagamento
     */
    public void setDsFormaAutorizacaoPagamento(
        String dsFormaAutorizacaoPagamento) {
        this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    }

    /**
     * Get: dsTipoControleFloating.
     * 
     * @return dsTipoControleFloating
     */
    public String getDsTipoControleFloating() {
        return dsTipoControleFloating;
    }

    /**
     * Set: dsTipoControleFloating.
     * 
     * @param dsTipoControleFloating
     *            the ds tipo controle floating
     */
    public void setDsTipoControleFloating(String dsTipoControleFloating) {
        this.dsTipoControleFloating = dsTipoControleFloating;
    }

    /**
     * Get: dsUtilizaPreAutorizacaoPagamentos.
     * 
     * @return dsUtilizaPreAutorizacaoPagamentos
     */
    public String getDsUtilizaPreAutorizacaoPagamentos() {
        return dsUtilizaPreAutorizacaoPagamentos;
    }

    /**
     * Set: dsUtilizaPreAutorizacaoPagamentos.
     * 
     * @param dsUtilizaPreAutorizacaoPagamentos
     *            the ds utiliza pre autorizacao pagamentos
     */
    public void setDsUtilizaPreAutorizacaoPagamentos(
        String dsUtilizaPreAutorizacaoPagamentos) {
        this.dsUtilizaPreAutorizacaoPagamentos = dsUtilizaPreAutorizacaoPagamentos;
    }

    /**
     * Get: cdModalidade.
     * 
     * @return cdModalidade
     */
    public int getCdModalidade() {
        return cdModalidade;
    }

    /**
     * Set: cdModalidade.
     * 
     * @param cdModalidade
     *            the cd modalidade
     */
    public void setCdModalidade(int cdModalidade) {
        this.cdModalidade = cdModalidade;
    }

    /**
     * Get: cdTipoServico.
     * 
     * @return cdTipoServico
     */
    public Integer getCdTipoServico() {
        return cdTipoServico;
    }

    /**
     * Set: cdTipoServico.
     * 
     * @param cdTipoServico
     *            the cd tipo servico
     */
    public void setCdTipoServico(Integer cdTipoServico) {
        this.cdTipoServico = cdTipoServico;
    }

    /**
     * Get: itemSelecionadoLista2.
     * 
     * @return itemSelecionadoLista2
     */
    public Integer getItemSelecionadoLista2() {
        return itemSelecionadoLista2;
    }

    /**
     * Set: itemSelecionadoLista2.
     * 
     * @param itemSelecionadoLista2
     *            the item selecionado lista2
     */
    public void setItemSelecionadoLista2(Integer itemSelecionadoLista2) {
        this.itemSelecionadoLista2 = itemSelecionadoLista2;
    }

    /**
     * Get: listaControleRadio2.
     * 
     * @return listaControleRadio2
     */
    public List<SelectItem> getListaControleRadio2() {
        return listaControleRadio2;
    }

    /**
     * Set: listaControleRadio2.
     * 
     * @param listaControleRadio2
     *            the lista controle radio2
     */
    public void setListaControleRadio2(List<SelectItem> listaControleRadio2) {
        this.listaControleRadio2 = listaControleRadio2;
    }

    /**
     * Get: listaGridPesquisa2.
     * 
     * @return listaGridPesquisa2
     */
    public List<ListarModalidadeTipoServicoSaidaDTO> getListaGridPesquisa2() {
        return listaGridPesquisa2;
    }

    /**
     * Set: listaGridPesquisa2.
     * 
     * @param listaGridPesquisa2
     *            the lista grid pesquisa2
     */
    public void setListaGridPesquisa2(
        List<ListarModalidadeTipoServicoSaidaDTO> listaGridPesquisa2) {
        this.listaGridPesquisa2 = listaGridPesquisa2;
    }

    /**
     * Get: cdPrioridadeDebitoFiltro.
     * 
     * @return cdPrioridadeDebitoFiltro
     */
    public Integer getCdPrioridadeDebitoFiltro() {
        return cdPrioridadeDebitoFiltro;
    }

    /**
     * Set: cdPrioridadeDebitoFiltro.
     * 
     * @param cdPrioridadeDebitoFiltro
     *            the cd prioridade debito filtro
     */
    public void setCdPrioridadeDebitoFiltro(Integer cdPrioridadeDebitoFiltro) {
        this.cdPrioridadeDebitoFiltro = cdPrioridadeDebitoFiltro;
    }

    /**
     * Get: cdTipoConsultaSaldoFiltro.
     * 
     * @return cdTipoConsultaSaldoFiltro
     */
    public Integer getCdTipoConsultaSaldoFiltro() {
        return cdTipoConsultaSaldoFiltro;
    }

    /**
     * Set: cdTipoConsultaSaldoFiltro.
     * 
     * @param cdTipoConsultaSaldoFiltro
     *            the cd tipo consulta saldo filtro
     */
    public void setCdTipoConsultaSaldoFiltro(Integer cdTipoConsultaSaldoFiltro) {
        this.cdTipoConsultaSaldoFiltro = cdTipoConsultaSaldoFiltro;
    }

    /**
     * Get: cdTipoProcessamentoFiltro.
     * 
     * @return cdTipoProcessamentoFiltro
     */
    public Integer getCdTipoProcessamentoFiltro() {
        return cdTipoProcessamentoFiltro;
    }

    /**
     * Set: cdTipoProcessamentoFiltro.
     * 
     * @param cdTipoProcessamentoFiltro
     *            the cd tipo processamento filtro
     */
    public void setCdTipoProcessamentoFiltro(Integer cdTipoProcessamentoFiltro) {
        this.cdTipoProcessamentoFiltro = cdTipoProcessamentoFiltro;
    }

    /**
     * Get: cdTipoRejAgendamentoFiltro.
     * 
     * @return cdTipoRejAgendamentoFiltro
     */
    public Integer getCdTipoRejAgendamentoFiltro() {
        return cdTipoRejAgendamentoFiltro;
    }

    /**
     * Set: cdTipoRejAgendamentoFiltro.
     * 
     * @param cdTipoRejAgendamentoFiltro
     *            the cd tipo rej agendamento filtro
     */
    public void setCdTipoRejAgendamentoFiltro(Integer cdTipoRejAgendamentoFiltro) {
        this.cdTipoRejAgendamentoFiltro = cdTipoRejAgendamentoFiltro;
    }

    /**
     * Get: cdTipoRejEfetivacaoFiltro.
     * 
     * @return cdTipoRejEfetivacaoFiltro
     */
    public Integer getCdTipoRejEfetivacaoFiltro() {
        return cdTipoRejEfetivacaoFiltro;
    }

    /**
     * Set: cdTipoRejEfetivacaoFiltro.
     * 
     * @param cdTipoRejEfetivacaoFiltro
     *            the cd tipo rej efetivacao filtro
     */
    public void setCdTipoRejEfetivacaoFiltro(Integer cdTipoRejEfetivacaoFiltro) {
        this.cdTipoRejEfetivacaoFiltro = cdTipoRejEfetivacaoFiltro;
    }

    /**
     * Get: cdTratamentoFeriadosFiltro.
     * 
     * @return cdTratamentoFeriadosFiltro
     */
    public Integer getCdTratamentoFeriadosFiltro() {
        return cdTratamentoFeriadosFiltro;
    }

    /**
     * Set: cdTratamentoFeriadosFiltro.
     * 
     * @param cdTratamentoFeriadosFiltro
     *            the cd tratamento feriados filtro
     */
    public void setCdTratamentoFeriadosFiltro(Integer cdTratamentoFeriadosFiltro) {
        this.cdTratamentoFeriadosFiltro = cdTratamentoFeriadosFiltro;
    }

    /**
     * Get: quantDiasRepiqueFiltro.
     * 
     * @return quantDiasRepiqueFiltro
     */
    public String getQuantDiasRepiqueFiltro() {
        return quantDiasRepiqueFiltro;
    }

    /**
     * Set: quantDiasRepiqueFiltro.
     * 
     * @param quantDiasRepiqueFiltro
     *            the quant dias repique filtro
     */
    public void setQuantDiasRepiqueFiltro(String quantDiasRepiqueFiltro) {
        this.quantDiasRepiqueFiltro = quantDiasRepiqueFiltro;
    }

    /**
     * Get: valorMaxPagFavorecidoFiltro.
     * 
     * @return valorMaxPagFavorecidoFiltro
     */
    public String getValorMaxPagFavorecidoFiltro() {
        return valorMaxPagFavorecidoFiltro;
    }

    /**
     * Set: valorMaxPagFavorecidoFiltro.
     * 
     * @param valorMaxPagFavorecidoFiltro
     *            the valor max pag favorecido filtro
     */
    public void setValorMaxPagFavorecidoFiltro(
        String valorMaxPagFavorecidoFiltro) {
        this.valorMaxPagFavorecidoFiltro = valorMaxPagFavorecidoFiltro;
    }

    /**
     * Get: dsGerarLancFuturoCredito.
     * 
     * @return dsGerarLancFuturoCredito
     */
    public String getDsGerarLancFuturoCredito() {
        return dsGerarLancFuturoCredito;
    }

    /**
     * Set: dsGerarLancFuturoCredito.
     * 
     * @param dsGerarLancFuturoCredito
     *            the ds gerar lanc futuro credito
     */
    public void setDsGerarLancFuturoCredito(String dsGerarLancFuturoCredito) {
        this.dsGerarLancFuturoCredito = dsGerarLancFuturoCredito;
    }

    /**
     * Get: dsGerarLancFuturoDebito.
     * 
     * @return dsGerarLancFuturoDebito
     */
    public String getDsGerarLancFuturoDebito() {
        return dsGerarLancFuturoDebito;
    }

    /**
     * Set: dsGerarLancFuturoDebito.
     * 
     * @param dsGerarLancFuturoDebito
     *            the ds gerar lanc futuro debito
     */
    public void setDsGerarLancFuturoDebito(String dsGerarLancFuturoDebito) {
        this.dsGerarLancFuturoDebito = dsGerarLancFuturoDebito;
    }

    /**
     * Get: dsPermiteFavorecidoConsulPag.
     * 
     * @return dsPermiteFavorecidoConsulPag
     */
    public String getDsPermiteFavorecidoConsulPag() {
        return dsPermiteFavorecidoConsulPag;
    }

    /**
     * Set: dsPermiteFavorecidoConsulPag.
     * 
     * @param dsPermiteFavorecidoConsulPag
     *            the ds permite favorecido consul pag
     */
    public void setDsPermiteFavorecidoConsulPag(
        String dsPermiteFavorecidoConsulPag) {
        this.dsPermiteFavorecidoConsulPag = dsPermiteFavorecidoConsulPag;
    }

    /**
     * Get: dsPrioridadeDebito.
     * 
     * @return dsPrioridadeDebito
     */
    public String getDsPrioridadeDebito() {
        return dsPrioridadeDebito;
    }

    /**
     * Set: dsPrioridadeDebito.
     * 
     * @param dsPrioridadeDebito
     *            the ds prioridade debito
     */
    public void setDsPrioridadeDebito(String dsPrioridadeDebito) {
        this.dsPrioridadeDebito = dsPrioridadeDebito;
    }

    /**
     * Get: dsQuantDiasRepique.
     * 
     * @return dsQuantDiasRepique
     */
    public String getDsQuantDiasRepique() {
        return dsQuantDiasRepique;
    }

    /**
     * Set: dsQuantDiasRepique.
     * 
     * @param dsQuantDiasRepique
     *            the ds quant dias repique
     */
    public void setDsQuantDiasRepique(String dsQuantDiasRepique) {
        this.dsQuantDiasRepique = dsQuantDiasRepique;
    }

    /**
     * Get: dsTipoConsultaSaldo.
     * 
     * @return dsTipoConsultaSaldo
     */
    public String getDsTipoConsultaSaldo() {
        return dsTipoConsultaSaldo;
    }

    /**
     * Set: dsTipoConsultaSaldo.
     * 
     * @param dsTipoConsultaSaldo
     *            the ds tipo consulta saldo
     */
    public void setDsTipoConsultaSaldo(String dsTipoConsultaSaldo) {
        this.dsTipoConsultaSaldo = dsTipoConsultaSaldo;
    }

    /**
     * Get: dsTipoProcessamento.
     * 
     * @return dsTipoProcessamento
     */
    public String getDsTipoProcessamento() {
        return dsTipoProcessamento;
    }

    /**
     * Set: dsTipoProcessamento.
     * 
     * @param dsTipoProcessamento
     *            the ds tipo processamento
     */
    public void setDsTipoProcessamento(String dsTipoProcessamento) {
        this.dsTipoProcessamento = dsTipoProcessamento;
    }

    /**
     * Get: dsTipoRejeicaoAgendamento.
     * 
     * @return dsTipoRejeicaoAgendamento
     */
    public String getDsTipoRejeicaoAgendamento() {
        return dsTipoRejeicaoAgendamento;
    }

    /**
     * Set: dsTipoRejeicaoAgendamento.
     * 
     * @param dsTipoRejeicaoAgendamento
     *            the ds tipo rejeicao agendamento
     */
    public void setDsTipoRejeicaoAgendamento(String dsTipoRejeicaoAgendamento) {
        this.dsTipoRejeicaoAgendamento = dsTipoRejeicaoAgendamento;
    }

    /**
     * Get: dsTipoRejeicaoEfetivacao.
     * 
     * @return dsTipoRejeicaoEfetivacao
     */
    public String getDsTipoRejeicaoEfetivacao() {
        return dsTipoRejeicaoEfetivacao;
    }

    /**
     * Set: dsTipoRejeicaoEfetivacao.
     * 
     * @param dsTipoRejeicaoEfetivacao
     *            the ds tipo rejeicao efetivacao
     */
    public void setDsTipoRejeicaoEfetivacao(String dsTipoRejeicaoEfetivacao) {
        this.dsTipoRejeicaoEfetivacao = dsTipoRejeicaoEfetivacao;
    }

    /**
     * Get: dsTratamentoFeriadosDataPagamento.
     * 
     * @return dsTratamentoFeriadosDataPagamento
     */
    public String getDsTratamentoFeriadosDataPagamento() {
        return dsTratamentoFeriadosDataPagamento;
    }

    /**
     * Set: dsTratamentoFeriadosDataPagamento.
     * 
     * @param dsTratamentoFeriadosDataPagamento
     *            the ds tratamento feriados data pagamento
     */
    public void setDsTratamentoFeriadosDataPagamento(
        String dsTratamentoFeriadosDataPagamento) {
        this.dsTratamentoFeriadosDataPagamento = dsTratamentoFeriadosDataPagamento;
    }

    /**
     * Get: dsUtilizaCadFavControlPag.
     * 
     * @return dsUtilizaCadFavControlPag
     */
    public String getDsUtilizaCadFavControlPag() {
        return dsUtilizaCadFavControlPag;
    }

    /**
     * Set: dsUtilizaCadFavControlPag.
     * 
     * @param dsUtilizaCadFavControlPag
     *            the ds utiliza cad fav control pag
     */
    public void setDsUtilizaCadFavControlPag(String dsUtilizaCadFavControlPag) {
        this.dsUtilizaCadFavControlPag = dsUtilizaCadFavControlPag;
    }

    /**
     * Get: dsValorLimiteDiario.
     * 
     * @return dsValorLimiteDiario
     */
    public String getDsValorLimiteDiario() {
        return dsValorLimiteDiario;
    }

    /**
     * Set: dsValorLimiteDiario.
     * 
     * @param dsValorLimiteDiario
     *            the ds valor limite diario
     */
    public void setDsValorLimiteDiario(String dsValorLimiteDiario) {
        this.dsValorLimiteDiario = dsValorLimiteDiario;
    }

    /**
     * Get: dsValorLimiteIndividual.
     * 
     * @return dsValorLimiteIndividual
     */
    public String getDsValorLimiteIndividual() {
        return dsValorLimiteIndividual;
    }

    /**
     * Set: dsValorLimiteIndividual.
     * 
     * @param dsValorLimiteIndividual
     *            the ds valor limite individual
     */
    public void setDsValorLimiteIndividual(String dsValorLimiteIndividual) {
        this.dsValorLimiteIndividual = dsValorLimiteIndividual;
    }

    /**
     * Get: dsValorMaximoPagFavNaoCad.
     * 
     * @return dsValorMaximoPagFavNaoCad
     */
    public String getDsValorMaximoPagFavNaoCad() {
        return dsValorMaximoPagFavNaoCad;
    }

    /**
     * Set: dsValorMaximoPagFavNaoCad.
     * 
     * @param dsValorMaximoPagFavNaoCad
     *            the ds valor maximo pag fav nao cad
     */
    public void setDsValorMaximoPagFavNaoCad(String dsValorMaximoPagFavNaoCad) {
        this.dsValorMaximoPagFavNaoCad = dsValorMaximoPagFavNaoCad;
    }

    /**
     * Get: dsModalidade.
     * 
     * @return dsModalidade
     */
    public String getDsModalidade() {
        return dsModalidade;
    }

    /**
     * Set: dsModalidade.
     * 
     * @param dsModalidade
     *            the ds modalidade
     */
    public void setDsModalidade(String dsModalidade) {
        this.dsModalidade = dsModalidade;
    }

    /**
     * Get: listaControleCheckModalidadeRelacionada.
     * 
     * @return listaControleCheckModalidadeRelacionada
     */
    public List<SelectItem> getListaControleCheckModalidadeRelacionada() {
        return listaControleCheckModalidadeRelacionada;
    }

    /**
     * Set: listaControleCheckModalidadeRelacionada.
     * 
     * @param listaControleCheckModalidadeRelacionada
     *            the lista controle check modalidade relacionada
     */
    public void setListaControleCheckModalidadeRelacionada(
        List<SelectItem> listaControleCheckModalidadeRelacionada) {
        this.listaControleCheckModalidadeRelacionada = listaControleCheckModalidadeRelacionada;
    }

    /**
     * Get: listaGridModalidadeRelacionada.
     * 
     * @return listaGridModalidadeRelacionada
     */
    public List<ListarServicoRelacionadoCdpsSaidaDTO> getListaGridModalidadeRelacionada() {
        return listaGridModalidadeRelacionada;
    }

    /**
     * Set: listaGridModalidadeRelacionada.
     * 
     * @param listaGridModalidadeRelacionada
     *            the lista grid modalidade relacionada
     */
    public void setListaGridModalidadeRelacionada(
        List<ListarServicoRelacionadoCdpsSaidaDTO> listaGridModalidadeRelacionada) {
        this.listaGridModalidadeRelacionada = listaGridModalidadeRelacionada;
    }

    /**
     * Get: listaCheckedGridModalidadeRelacionada.
     * 
     * @return listaCheckedGridModalidadeRelacionada
     */
    public List<ListarServicoRelacionadoCdpsSaidaDTO> getListaCheckedGridModalidadeRelacionada() {
        return listaCheckedGridModalidadeRelacionada;
    }

    /**
     * Set: listaCheckedGridModalidadeRelacionada.
     * 
     * @param listaCheckedGridModalidadeRelacionada
     *            the lista checked grid modalidade relacionada
     */
    public void setListaCheckedGridModalidadeRelacionada(
        List<ListarServicoRelacionadoCdpsSaidaDTO> listaCheckedGridModalidadeRelacionada) {
        this.listaCheckedGridModalidadeRelacionada = listaCheckedGridModalidadeRelacionada;
    }

    /**
     * Get: cdEmiteCartao.
     * 
     * @return cdEmiteCartao
     */
    public Integer getCdEmiteCartao() {
        return cdEmiteCartao;
    }

    /**
     * Set: cdEmiteCartao.
     * 
     * @param cdEmiteCartao
     *            the cd emite cartao
     */
    public void setCdEmiteCartao(Integer cdEmiteCartao) {
        this.cdEmiteCartao = cdEmiteCartao;
    }

    /**
     * Get: cdTipoCartao.
     * 
     * @return cdTipoCartao
     */
    public Integer getCdTipoCartao() {
        return cdTipoCartao;
    }

    /**
     * Set: cdTipoCartao.
     * 
     * @param cdTipoCartao
     *            the cd tipo cartao
     */
    public void setCdTipoCartao(Integer cdTipoCartao) {
        this.cdTipoCartao = cdTipoCartao;
    }

    /**
     * Get: dataLimite.
     * 
     * @return dataLimite
     */
    public Date getDataLimite() {
        return dataLimite;
    }

    /**
     * Set: dataLimite.
     * 
     * @param dataLimite
     *            the data limite
     */
    public void setDataLimite(Date dataLimite) {
        this.dataLimite = dataLimite;
    }

    /**
     * Get: qtddLimite.
     * 
     * @return qtddLimite
     */
    public String getQtddLimite() {
        return qtddLimite;
    }

    /**
     * Set: qtddLimite.
     * 
     * @param qtddLimite
     *            the qtdd limite
     */
    public void setQtddLimite(String qtddLimite) {
        this.qtddLimite = qtddLimite;
    }

    /**
     * Get: cdAgendDebito.
     * 
     * @return cdAgendDebito
     */
    public Integer getCdAgendDebito() {
        return cdAgendDebito;
    }

    /**
     * Set: cdAgendDebito.
     * 
     * @param cdAgendDebito
     *            the cd agend debito
     */
    public void setCdAgendDebito(Integer cdAgendDebito) {
        this.cdAgendDebito = cdAgendDebito;
    }

    /**
     * Get: cdPeriodicidadeCobranca.
     * 
     * @return cdPeriodicidadeCobranca
     */
    public Integer getCdPeriodicidadeCobranca() {
        return cdPeriodicidadeCobranca;
    }

    /**
     * Set: cdPeriodicidadeCobranca.
     * 
     * @param cdPeriodicidadeCobranca
     *            the cd periodicidade cobranca
     */
    public void setCdPeriodicidadeCobranca(Integer cdPeriodicidadeCobranca) {
        this.cdPeriodicidadeCobranca = cdPeriodicidadeCobranca;
    }

    /**
     * Get: cdPeriodicidadeRastreamento.
     * 
     * @return cdPeriodicidadeRastreamento
     */
    public Integer getCdPeriodicidadeRastreamento() {
        return cdPeriodicidadeRastreamento;
    }

    /**
     * Set: cdPeriodicidadeRastreamento.
     * 
     * @param cdPeriodicidadeRastreamento
     *            the cd periodicidade rastreamento
     */
    public void setCdPeriodicidadeRastreamento(
        Integer cdPeriodicidadeRastreamento) {
        this.cdPeriodicidadeRastreamento = cdPeriodicidadeRastreamento;
    }

    /**
     * Get: cdPeriodicidadeReajusteTarifa.
     * 
     * @return cdPeriodicidadeReajusteTarifa
     */
    public Integer getCdPeriodicidadeReajusteTarifa() {
        return cdPeriodicidadeReajusteTarifa;
    }

    /**
     * Set: cdPeriodicidadeReajusteTarifa.
     * 
     * @param cdPeriodicidadeReajusteTarifa
     *            the cd periodicidade reajuste tarifa
     */
    public void setCdPeriodicidadeReajusteTarifa(
        Integer cdPeriodicidadeReajusteTarifa) {
        this.cdPeriodicidadeReajusteTarifa = cdPeriodicidadeReajusteTarifa;
    }

    /**
     * Get: cdTipoAutorizacaoPagamento.
     * 
     * @return cdTipoAutorizacaoPagamento
     */
    public Integer getCdTipoAutorizacaoPagamento() {
        return cdTipoAutorizacaoPagamento;
    }

    /**
     * Set: cdTipoAutorizacaoPagamento.
     * 
     * @param cdTipoAutorizacaoPagamento
     *            the cd tipo autorizacao pagamento
     */
    public void setCdTipoAutorizacaoPagamento(Integer cdTipoAutorizacaoPagamento) {
        this.cdTipoAutorizacaoPagamento = cdTipoAutorizacaoPagamento;
    }

    /**
     * Get: cdTipoReajusteTarifa.
     * 
     * @return cdTipoReajusteTarifa
     */
    public Integer getCdTipoReajusteTarifa() {
        return cdTipoReajusteTarifa;
    }

    /**
     * Set: cdTipoReajusteTarifa.
     * 
     * @param cdTipoReajusteTarifa
     *            the cd tipo reajuste tarifa
     */
    public void setCdTipoReajusteTarifa(Integer cdTipoReajusteTarifa) {
        this.cdTipoReajusteTarifa = cdTipoReajusteTarifa;
    }

    /**
     * Get: cdUtilizacaoRastreamentoDebito.
     * 
     * @return cdUtilizacaoRastreamentoDebito
     */
    public Integer getCdUtilizacaoRastreamentoDebito() {
        return cdUtilizacaoRastreamentoDebito;
    }

    /**
     * Set: cdUtilizacaoRastreamentoDebito.
     * 
     * @param cdUtilizacaoRastreamentoDebito
     *            the cd utilizacao rastreamento debito
     */
    public void setCdUtilizacaoRastreamentoDebito(
        Integer cdUtilizacaoRastreamentoDebito) {
        this.cdUtilizacaoRastreamentoDebito = cdUtilizacaoRastreamentoDebito;
    }

    /**
     * Get: indiceEconomicoReajuste.
     * 
     * @return indiceEconomicoReajuste
     */
    public String getIndiceEconomicoReajuste() {
        return indiceEconomicoReajuste;
    }

    /**
     * Set: indiceEconomicoReajuste.
     * 
     * @param indiceEconomicoReajuste
     *            the indice economico reajuste
     */
    public void setIndiceEconomicoReajuste(String indiceEconomicoReajuste) {
        this.indiceEconomicoReajuste = indiceEconomicoReajuste;
    }

    /**
     * Get: qtddDiasCobrancaTarifa.
     * 
     * @return qtddDiasCobrancaTarifa
     */
    public String getQtddDiasCobrancaTarifa() {
        return qtddDiasCobrancaTarifa;
    }

    /**
     * Set: qtddDiasCobrancaTarifa.
     * 
     * @param qtddDiasCobrancaTarifa
     *            the qtdd dias cobranca tarifa
     */
    public void setQtddDiasCobrancaTarifa(String qtddDiasCobrancaTarifa) {
        this.qtddDiasCobrancaTarifa = qtddDiasCobrancaTarifa;
    }

    /**
     * Get: qtddMesesReajuste.
     * 
     * @return qtddMesesReajuste
     */
    public String getQtddMesesReajuste() {
        return qtddMesesReajuste;
    }

    /**
     * Set: qtddMesesReajuste.
     * 
     * @param qtddMesesReajuste
     *            the qtdd meses reajuste
     */
    public void setQtddMesesReajuste(String qtddMesesReajuste) {
        this.qtddMesesReajuste = qtddMesesReajuste;
    }

    /**
     * Get: tipoAlimentacao.
     * 
     * @return tipoAlimentacao
     */
    public String getTipoAlimentacao() {
        return tipoAlimentacao;
    }

    /**
     * Set: tipoAlimentacao.
     * 
     * @param tipoAlimentacao
     *            the tipo alimentacao
     */
    public void setTipoAlimentacao(String tipoAlimentacao) {
        this.tipoAlimentacao = tipoAlimentacao;
    }

    /**
     * Get: cdFormaControleExpiracao.
     * 
     * @return cdFormaControleExpiracao
     */
    public Integer getCdFormaControleExpiracao() {
        return cdFormaControleExpiracao;
    }

    /**
     * Set: cdFormaControleExpiracao.
     * 
     * @param cdFormaControleExpiracao
     *            the cd forma controle expiracao
     */
    public void setCdFormaControleExpiracao(Integer cdFormaControleExpiracao) {
        this.cdFormaControleExpiracao = cdFormaControleExpiracao;
    }

    /**
     * Get: cdFormaManutencaoCadastro.
     * 
     * @return cdFormaManutencaoCadastro
     */
    public Integer getCdFormaManutencaoCadastro() {
        return cdFormaManutencaoCadastro;
    }

    /**
     * Set: cdFormaManutencaoCadastro.
     * 
     * @param cdFormaManutencaoCadastro
     *            the cd forma manutencao cadastro
     */
    public void setCdFormaManutencaoCadastro(Integer cdFormaManutencaoCadastro) {
        this.cdFormaManutencaoCadastro = cdFormaManutencaoCadastro;
    }

    /**
     * Get: cdInformaAgenciaContaCredito.
     * 
     * @return cdInformaAgenciaContaCredito
     */
    public Integer getCdInformaAgenciaContaCredito() {
        return cdInformaAgenciaContaCredito;
    }

    /**
     * Set: cdInformaAgenciaContaCredito.
     * 
     * @param cdInformaAgenciaContaCredito
     *            the cd informa agencia conta credito
     */
    public void setCdInformaAgenciaContaCredito(
        Integer cdInformaAgenciaContaCredito) {
        this.cdInformaAgenciaContaCredito = cdInformaAgenciaContaCredito;
    }

    /**
     * Get: cdManCadastroProcuradores.
     * 
     * @return cdManCadastroProcuradores
     */
    public Integer getCdManCadastroProcuradores() {
        return cdManCadastroProcuradores;
    }

    /**
     * Set: cdManCadastroProcuradores.
     * 
     * @param cdManCadastroProcuradores
     *            the cd man cadastro procuradores
     */
    public void setCdManCadastroProcuradores(Integer cdManCadastroProcuradores) {
        this.cdManCadastroProcuradores = cdManCadastroProcuradores;
    }

    /**
     * Get: cdPeriodicidadeManutencao.
     * 
     * @return cdPeriodicidadeManutencao
     */
    public Integer getCdPeriodicidadeManutencao() {
        return cdPeriodicidadeManutencao;
    }

    /**
     * Set: cdPeriodicidadeManutencao.
     * 
     * @param cdPeriodicidadeManutencao
     *            the cd periodicidade manutencao
     */
    public void setCdPeriodicidadeManutencao(Integer cdPeriodicidadeManutencao) {
        this.cdPeriodicidadeManutencao = cdPeriodicidadeManutencao;
    }

    /**
     * Get: cdPossuiExpiracaoCredito.
     * 
     * @return cdPossuiExpiracaoCredito
     */
    public Integer getCdPossuiExpiracaoCredito() {
        return cdPossuiExpiracaoCredito;
    }

    /**
     * Set: cdPossuiExpiracaoCredito.
     * 
     * @param cdPossuiExpiracaoCredito
     *            the cd possui expiracao credito
     */
    public void setCdPossuiExpiracaoCredito(Integer cdPossuiExpiracaoCredito) {
        this.cdPossuiExpiracaoCredito = cdPossuiExpiracaoCredito;
    }

    /**
     * Get: cdTipoIdenBeneficio.
     * 
     * @return cdTipoIdenBeneficio
     */
    public Integer getCdTipoIdenBeneficio() {
        return cdTipoIdenBeneficio;
    }

    /**
     * Set: cdTipoIdenBeneficio.
     * 
     * @param cdTipoIdenBeneficio
     *            the cd tipo iden beneficio
     */
    public void setCdTipoIdenBeneficio(Integer cdTipoIdenBeneficio) {
        this.cdTipoIdenBeneficio = cdTipoIdenBeneficio;
    }

    /**
     * Get: cdTipoPrestContaCreditoPago.
     * 
     * @return cdTipoPrestContaCreditoPago
     */
    public Integer getCdTipoPrestContaCreditoPago() {
        return cdTipoPrestContaCreditoPago;
    }

    /**
     * Set: cdTipoPrestContaCreditoPago.
     * 
     * @param cdTipoPrestContaCreditoPago
     *            the cd tipo prest conta credito pago
     */
    public void setCdTipoPrestContaCreditoPago(
        Integer cdTipoPrestContaCreditoPago) {
        this.cdTipoPrestContaCreditoPago = cdTipoPrestContaCreditoPago;
    }

    /**
     * Get: cdTratFeriadoFimVigencia.
     * 
     * @return cdTratFeriadoFimVigencia
     */
    public Integer getCdTratFeriadoFimVigencia() {
        return cdTratFeriadoFimVigencia;
    }

    /**
     * Set: cdTratFeriadoFimVigencia.
     * 
     * @param cdTratFeriadoFimVigencia
     *            the cd trat feriado fim vigencia
     */
    public void setCdTratFeriadoFimVigencia(Integer cdTratFeriadoFimVigencia) {
        this.cdTratFeriadoFimVigencia = cdTratFeriadoFimVigencia;
    }

    /**
     * Get: cdUtilizaCadOrgPagadores.
     * 
     * @return cdUtilizaCadOrgPagadores
     */
    public Integer getCdUtilizaCadOrgPagadores() {
        return cdUtilizaCadOrgPagadores;
    }

    /**
     * Set: cdUtilizaCadOrgPagadores.
     * 
     * @param cdUtilizaCadOrgPagadores
     *            the cd utiliza cad org pagadores
     */
    public void setCdUtilizaCadOrgPagadores(Integer cdUtilizaCadOrgPagadores) {
        this.cdUtilizaCadOrgPagadores = cdUtilizaCadOrgPagadores;
    }

    /**
     * Get: cdConsistenciaCpfCnpfFavorecido.
     * 
     * @return cdConsistenciaCpfCnpfFavorecido
     */
    public Integer getCdConsistenciaCpfCnpfFavorecido() {
        return cdConsistenciaCpfCnpfFavorecido;
    }

    /**
     * Set: cdConsistenciaCpfCnpfFavorecido.
     * 
     * @param cdConsistenciaCpfCnpfFavorecido
     *            the cd consistencia cpf cnpf favorecido
     */
    public void setCdConsistenciaCpfCnpfFavorecido(
        Integer cdConsistenciaCpfCnpfFavorecido) {
        this.cdConsistenciaCpfCnpfFavorecido = cdConsistenciaCpfCnpfFavorecido;
    }

    /**
     * Get: cdFormaManutencaoCadastroProcurador.
     * 
     * @return cdFormaManutencaoCadastroProcurador
     */
    public Integer getCdFormaManutencaoCadastroProcurador() {
        return cdFormaManutencaoCadastroProcurador;
    }

    /**
     * Set: cdFormaManutencaoCadastroProcurador.
     * 
     * @param cdFormaManutencaoCadastroProcurador
     *            the cd forma manutencao cadastro procurador
     */
    public void setCdFormaManutencaoCadastroProcurador(
        Integer cdFormaManutencaoCadastroProcurador) {
        this.cdFormaManutencaoCadastroProcurador = cdFormaManutencaoCadastroProcurador;
    }

    /**
     * Get: qtddDiasInativacao.
     * 
     * @return qtddDiasInativacao
     */
    public String getQtddDiasInativacao() {
        return qtddDiasInativacao;
    }

    /**
     * Set: qtddDiasInativacao.
     * 
     * @param qtddDiasInativacao
     *            the qtdd dias inativacao
     */
    public void setQtddDiasInativacao(String qtddDiasInativacao) {
        this.qtddDiasInativacao = qtddDiasInativacao;
    }

    /**
     * Get: cdAgruparCorrespondencia.
     * 
     * @return cdAgruparCorrespondencia
     */
    public Integer getCdAgruparCorrespondencia() {
        return cdAgruparCorrespondencia;
    }

    /**
     * Set: cdAgruparCorrespondencia.
     * 
     * @param cdAgruparCorrespondencia
     *            the cd agrupar correspondencia
     */
    public void setCdAgruparCorrespondencia(Integer cdAgruparCorrespondencia) {
        this.cdAgruparCorrespondencia = cdAgruparCorrespondencia;
    }

    /**
     * Get: cdDemonsInfReservada.
     * 
     * @return cdDemonsInfReservada
     */
    public Integer getCdDemonsInfReservada() {
        return cdDemonsInfReservada;
    }

    /**
     * Set: cdDemonsInfReservada.
     * 
     * @param cdDemonsInfReservada
     *            the cd demons inf reservada
     */
    public void setCdDemonsInfReservada(Integer cdDemonsInfReservada) {
        this.cdDemonsInfReservada = cdDemonsInfReservada;
    }

    /**
     * Get: cdDestino.
     * 
     * @return cdDestino
     */
    public Integer getCdDestino() {
        return cdDestino;
    }

    /**
     * Set: cdDestino.
     * 
     * @param cdDestino
     *            the cd destino
     */
    public void setCdDestino(Integer cdDestino) {
        this.cdDestino = cdDestino;
    }

    /**
     * Get: cdPeriodicidade.
     * 
     * @return cdPeriodicidade
     */
    public Integer getCdPeriodicidade() {
        return cdPeriodicidade;
    }

    /**
     * Set: cdPeriodicidade.
     * 
     * @param cdPeriodicidade
     *            the cd periodicidade
     */
    public void setCdPeriodicidade(Integer cdPeriodicidade) {
        this.cdPeriodicidade = cdPeriodicidade;
    }

    /**
     * Get: cdPermiteCorrespAberta.
     * 
     * @return cdPermiteCorrespAberta
     */
    public Integer getCdPermiteCorrespAberta() {
        return cdPermiteCorrespAberta;
    }

    /**
     * Set: cdPermiteCorrespAberta.
     * 
     * @param cdPermiteCorrespAberta
     *            the cd permite corresp aberta
     */
    public void setCdPermiteCorrespAberta(Integer cdPermiteCorrespAberta) {
        this.cdPermiteCorrespAberta = cdPermiteCorrespAberta;
    }

    /**
     * Get: quantidadeVias.
     * 
     * @return quantidadeVias
     */
    public String getQuantidadeVias() {
        return quantidadeVias;
    }

    /**
     * Set: quantidadeVias.
     * 
     * @param quantidadeVias
     *            the quantidade vias
     */
    public void setQuantidadeVias(String quantidadeVias) {
        this.quantidadeVias = quantidadeVias;
    }

    /**
     * Get: cdCobrarTarifasFuncionarios.
     * 
     * @return cdCobrarTarifasFuncionarios
     */
    public Integer getCdCobrarTarifasFuncionarios() {
        return cdCobrarTarifasFuncionarios;
    }

    /**
     * Set: cdCobrarTarifasFuncionarios.
     * 
     * @param cdCobrarTarifasFuncionarios
     *            the cd cobrar tarifas funcionarios
     */
    public void setCdCobrarTarifasFuncionarios(
        Integer cdCobrarTarifasFuncionarios) {
        this.cdCobrarTarifasFuncionarios = cdCobrarTarifasFuncionarios;
    }

    /**
     * Get: cdFrasesPreCadastradas.
     * 
     * @return cdFrasesPreCadastradas
     */
    public Integer getCdFrasesPreCadastradas() {
        return cdFrasesPreCadastradas;
    }

    /**
     * Set: cdFrasesPreCadastradas.
     * 
     * @param cdFrasesPreCadastradas
     *            the cd frases pre cadastradas
     */
    public void setCdFrasesPreCadastradas(Integer cdFrasesPreCadastradas) {
        this.cdFrasesPreCadastradas = cdFrasesPreCadastradas;
    }

    /**
     * Get: cdMidiaDisponivelCorrentista.
     * 
     * @return cdMidiaDisponivelCorrentista
     */
    public Integer getCdMidiaDisponivelCorrentista() {
        return cdMidiaDisponivelCorrentista;
    }

    /**
     * Set: cdMidiaDisponivelCorrentista.
     * 
     * @param cdMidiaDisponivelCorrentista
     *            the cd midia disponivel correntista
     */
    public void setCdMidiaDisponivelCorrentista(
        Integer cdMidiaDisponivelCorrentista) {
        this.cdMidiaDisponivelCorrentista = cdMidiaDisponivelCorrentista;
    }

    /**
     * Get: cdMidiaDisponivelNaoCorrentista.
     * 
     * @return cdMidiaDisponivelNaoCorrentista
     */
    public Integer getCdMidiaDisponivelNaoCorrentista() {
        return cdMidiaDisponivelNaoCorrentista;
    }

    /**
     * Set: cdMidiaDisponivelNaoCorrentista.
     * 
     * @param cdMidiaDisponivelNaoCorrentista
     *            the cd midia disponivel nao correntista
     */
    public void setCdMidiaDisponivelNaoCorrentista(
        Integer cdMidiaDisponivelNaoCorrentista) {
        this.cdMidiaDisponivelNaoCorrentista = cdMidiaDisponivelNaoCorrentista;
    }

    /**
     * Get: cdPeriodicidadeCobrancaTarifa.
     * 
     * @return cdPeriodicidadeCobrancaTarifa
     */
    public Integer getCdPeriodicidadeCobrancaTarifa() {
        return cdPeriodicidadeCobrancaTarifa;
    }

    /**
     * Set: cdPeriodicidadeCobrancaTarifa.
     * 
     * @param cdPeriodicidadeCobrancaTarifa
     *            the cd periodicidade cobranca tarifa
     */
    public void setCdPeriodicidadeCobrancaTarifa(
        Integer cdPeriodicidadeCobrancaTarifa) {
        this.cdPeriodicidadeCobrancaTarifa = cdPeriodicidadeCobrancaTarifa;
    }

    /**
     * Get: cdTipoEmissao.
     * 
     * @return cdTipoEmissao
     */
    public Integer getCdTipoEmissao() {
        return cdTipoEmissao;
    }

    /**
     * Set: cdTipoEmissao.
     * 
     * @param cdTipoEmissao
     *            the cd tipo emissao
     */
    public void setCdTipoEmissao(Integer cdTipoEmissao) {
        this.cdTipoEmissao = cdTipoEmissao;
    }

    /**
     * Get: cdTipoEnvioCorrespondencia.
     * 
     * @return cdTipoEnvioCorrespondencia
     */
    public Integer getCdTipoEnvioCorrespondencia() {
        return cdTipoEnvioCorrespondencia;
    }

    /**
     * Set: cdTipoEnvioCorrespondencia.
     * 
     * @param cdTipoEnvioCorrespondencia
     *            the cd tipo envio correspondencia
     */
    public void setCdTipoEnvioCorrespondencia(Integer cdTipoEnvioCorrespondencia) {
        this.cdTipoEnvioCorrespondencia = cdTipoEnvioCorrespondencia;
    }

    /**
     * Get: cdTipoReajuste.
     * 
     * @return cdTipoReajuste
     */
    public Integer getCdTipoReajuste() {
        return cdTipoReajuste;
    }

    /**
     * Set: cdTipoReajuste.
     * 
     * @param cdTipoReajuste
     *            the cd tipo reajuste
     */
    public void setCdTipoReajuste(Integer cdTipoReajuste) {
        this.cdTipoReajuste = cdTipoReajuste;
    }

    /**
     * Get: cdTipoRejeicao.
     * 
     * @return cdTipoRejeicao
     */
    public Integer getCdTipoRejeicao() {
        return cdTipoRejeicao;
    }

    /**
     * Set: cdTipoRejeicao.
     * 
     * @param cdTipoRejeicao
     *            the cd tipo rejeicao
     */
    public void setCdTipoRejeicao(Integer cdTipoRejeicao) {
        this.cdTipoRejeicao = cdTipoRejeicao;
    }

    /**
     * Get: diasCobrancaAposApuracao.
     * 
     * @return diasCobrancaAposApuracao
     */
    public String getDiasCobrancaAposApuracao() {
        return diasCobrancaAposApuracao;
    }

    /**
     * Set: diasCobrancaAposApuracao.
     * 
     * @param diasCobrancaAposApuracao
     *            the dias cobranca apos apuracao
     */
    public void setDiasCobrancaAposApuracao(String diasCobrancaAposApuracao) {
        this.diasCobrancaAposApuracao = diasCobrancaAposApuracao;
    }

    /**
     * Get: fechamentoApuracao.
     * 
     * @return fechamentoApuracao
     */
    public String getFechamentoApuracao() {
        return fechamentoApuracao;
    }

    /**
     * Set: fechamentoApuracao.
     * 
     * @param fechamentoApuracao
     *            the fechamento apuracao
     */
    public void setFechamentoApuracao(String fechamentoApuracao) {
        this.fechamentoApuracao = fechamentoApuracao;
    }

    /**
     * Get: cdFormularioImpressao.
     * 
     * @return cdFormularioImpressao
     */
    public Integer getCdFormularioImpressao() {
        return cdFormularioImpressao;
    }

    /**
     * Get: cdIndiceReajuste.
     * 
     * @return cdIndiceReajuste
     */
    public Integer getCdIndiceReajuste() {
        return cdIndiceReajuste;
    }

    /**
     * Set: cdIndiceReajuste.
     * 
     * @param cdIndiceReajuste
     *            the cd indice reajuste
     */
    public void setCdIndiceReajuste(Integer cdIndiceReajuste) {
        this.cdIndiceReajuste = cdIndiceReajuste;
    }

    /**
     * Get: mesesReajusteAutomatico.
     * 
     * @return mesesReajusteAutomatico
     */
    public String getMesesReajusteAutomatico() {
        return mesesReajusteAutomatico;
    }

    /**
     * Set: mesesReajusteAutomatico.
     * 
     * @param mesesReajusteAutomatico
     *            the meses reajuste automatico
     */
    public void setMesesReajusteAutomatico(String mesesReajusteAutomatico) {
        this.mesesReajusteAutomatico = mesesReajusteAutomatico;
    }

    /**
     * Get: qtddLinhas.
     * 
     * @return qtddLinhas
     */
    public String getQtddLinhas() {
        return qtddLinhas;
    }

    /**
     * Set: qtddLinhas.
     * 
     * @param qtddLinhas
     *            the qtdd linhas
     */
    public void setQtddLinhas(String qtddLinhas) {
        this.qtddLinhas = qtddLinhas;
    }

    /**
     * Get: qtddMesesEmissao.
     * 
     * @return qtddMesesEmissao
     */
    public String getQtddMesesEmissao() {
        return qtddMesesEmissao;
    }

    /**
     * Set: qtddMesesEmissao.
     * 
     * @param qtddMesesEmissao
     *            the qtdd meses emissao
     */
    public void setQtddMesesEmissao(String qtddMesesEmissao) {
        this.qtddMesesEmissao = qtddMesesEmissao;
    }

    /**
     * Get: qtddViasPagas.
     * 
     * @return qtddViasPagas
     */
    public String getQtddViasPagas() {
        return qtddViasPagas;
    }

    /**
     * Set: qtddViasPagas.
     * 
     * @param qtddViasPagas
     *            the qtdd vias pagas
     */
    public void setQtddViasPagas(String qtddViasPagas) {
        this.qtddViasPagas = qtddViasPagas;
    }

    /**
     * Get: qtddVias.
     * 
     * @return qtddVias
     */
    public String getQtddVias() {
        return qtddVias;
    }

    /**
     * Set: qtddVias.
     * 
     * @param qtddVias
     *            the qtdd vias
     */
    public void setQtddVias(String qtddVias) {
        this.qtddVias = qtddVias;
    }

    /**
     * Get: cdBaseDadosUtilizada.
     * 
     * @return cdBaseDadosUtilizada
     */
    public Integer getCdBaseDadosUtilizada() {
        return cdBaseDadosUtilizada;
    }

    /**
     * Set: cdBaseDadosUtilizada.
     * 
     * @param cdBaseDadosUtilizada
     *            the cd base dados utilizada
     */
    public void setCdBaseDadosUtilizada(Integer cdBaseDadosUtilizada) {
        this.cdBaseDadosUtilizada = cdBaseDadosUtilizada;
    }

    /**
     * Get: cdCondicaoEnquadramento.
     * 
     * @return cdCondicaoEnquadramento
     */
    public Integer getCdCondicaoEnquadramento() {
        return cdCondicaoEnquadramento;
    }

    /**
     * Set: cdCondicaoEnquadramento.
     * 
     * @param cdCondicaoEnquadramento
     *            the cd condicao enquadramento
     */
    public void setCdCondicaoEnquadramento(Integer cdCondicaoEnquadramento) {
        this.cdCondicaoEnquadramento = cdCondicaoEnquadramento;
    }

    /**
     * Get: cdCriterioCompostoEnquadramento.
     * 
     * @return cdCriterioCompostoEnquadramento
     */
    public Integer getCdCriterioCompostoEnquadramento() {
        return cdCriterioCompostoEnquadramento;
    }

    /**
     * Set: cdCriterioCompostoEnquadramento.
     * 
     * @param cdCriterioCompostoEnquadramento
     *            the cd criterio composto enquadramento
     */
    public void setCdCriterioCompostoEnquadramento(
        Integer cdCriterioCompostoEnquadramento) {
        this.cdCriterioCompostoEnquadramento = cdCriterioCompostoEnquadramento;
    }

    /**
     * Get: cdCriterioPrincipalEnquadramento.
     * 
     * @return cdCriterioPrincipalEnquadramento
     */
    public Integer getCdCriterioPrincipalEnquadramento() {
        return cdCriterioPrincipalEnquadramento;
    }

    /**
     * Set: cdCriterioPrincipalEnquadramento.
     * 
     * @param cdCriterioPrincipalEnquadramento
     *            the cd criterio principal enquadramento
     */
    public void setCdCriterioPrincipalEnquadramento(
        Integer cdCriterioPrincipalEnquadramento) {
        this.cdCriterioPrincipalEnquadramento = cdCriterioPrincipalEnquadramento;
    }

    /**
     * Get: cdEmiteMsgRecadastramentoMidiaOnline.
     * 
     * @return cdEmiteMsgRecadastramentoMidiaOnline
     */
    public Integer getCdEmiteMsgRecadastramentoMidiaOnline() {
        return cdEmiteMsgRecadastramentoMidiaOnline;
    }

    /**
     * Set: cdEmiteMsgRecadastramentoMidiaOnline.
     * 
     * @param cdEmiteMsgRecadastramentoMidiaOnline
     *            the cd emite msg recadastramento midia online
     */
    public void setCdEmiteMsgRecadastramentoMidiaOnline(
        Integer cdEmiteMsgRecadastramentoMidiaOnline) {
        this.cdEmiteMsgRecadastramentoMidiaOnline = cdEmiteMsgRecadastramentoMidiaOnline;
    }

    /**
     * Get: cdIndiceEconomicoReajusteTarifa.
     * 
     * @return cdIndiceEconomicoReajusteTarifa
     */
    public Integer getCdIndiceEconomicoReajusteTarifa() {
        return cdIndiceEconomicoReajusteTarifa;
    }

    /**
     * Set: cdIndiceEconomicoReajusteTarifa.
     * 
     * @param cdIndiceEconomicoReajusteTarifa
     *            the cd indice economico reajuste tarifa
     */
    public void setCdIndiceEconomicoReajusteTarifa(
        Integer cdIndiceEconomicoReajusteTarifa) {
        this.cdIndiceEconomicoReajusteTarifa = cdIndiceEconomicoReajusteTarifa;
    }

    /**
     * Get: cdMidiaMsgRecadastramentoOnline.
     * 
     * @return cdMidiaMsgRecadastramentoOnline
     */
    public Integer getCdMidiaMsgRecadastramentoOnline() {
        return cdMidiaMsgRecadastramentoOnline;
    }

    /**
     * Set: cdMidiaMsgRecadastramentoOnline.
     * 
     * @param cdMidiaMsgRecadastramentoOnline
     *            the cd midia msg recadastramento online
     */
    public void setCdMidiaMsgRecadastramentoOnline(
        Integer cdMidiaMsgRecadastramentoOnline) {
        this.cdMidiaMsgRecadastramentoOnline = cdMidiaMsgRecadastramentoOnline;
    }

    /**
     * Get: cdPeriodicidadeEnvioRemessa.
     * 
     * @return cdPeriodicidadeEnvioRemessa
     */
    public Integer getCdPeriodicidadeEnvioRemessa() {
        return cdPeriodicidadeEnvioRemessa;
    }

    /**
     * Set: cdPeriodicidadeEnvioRemessa.
     * 
     * @param cdPeriodicidadeEnvioRemessa
     *            the cd periodicidade envio remessa
     */
    public void setCdPeriodicidadeEnvioRemessa(
        Integer cdPeriodicidadeEnvioRemessa) {
        this.cdPeriodicidadeEnvioRemessa = cdPeriodicidadeEnvioRemessa;
    }

    /**
     * Get: cdPermiteAcertosDados.
     * 
     * @return cdPermiteAcertosDados
     */
    public Integer getCdPermiteAcertosDados() {
        return cdPermiteAcertosDados;
    }

    /**
     * Set: cdPermiteAcertosDados.
     * 
     * @param cdPermiteAcertosDados
     *            the cd permite acertos dados
     */
    public void setCdPermiteAcertosDados(Integer cdPermiteAcertosDados) {
        this.cdPermiteAcertosDados = cdPermiteAcertosDados;
    }

    /**
     * Get: cdPermiteAnteciparRecadastramento.
     * 
     * @return cdPermiteAnteciparRecadastramento
     */
    public Integer getCdPermiteAnteciparRecadastramento() {
        return cdPermiteAnteciparRecadastramento;
    }

    /**
     * Set: cdPermiteAnteciparRecadastramento.
     * 
     * @param cdPermiteAnteciparRecadastramento
     *            the cd permite antecipar recadastramento
     */
    public void setCdPermiteAnteciparRecadastramento(
        Integer cdPermiteAnteciparRecadastramento) {
        this.cdPermiteAnteciparRecadastramento = cdPermiteAnteciparRecadastramento;
    }

    /**
     * Get: cdPermiteEnvioRemessa.
     * 
     * @return cdPermiteEnvioRemessa
     */
    public Integer getCdPermiteEnvioRemessa() {
        return cdPermiteEnvioRemessa;
    }

    /**
     * Set: cdPermiteEnvioRemessa.
     * 
     * @param cdPermiteEnvioRemessa
     *            the cd permite envio remessa
     */
    public void setCdPermiteEnvioRemessa(Integer cdPermiteEnvioRemessa) {
        this.cdPermiteEnvioRemessa = cdPermiteEnvioRemessa;
    }

    /**
     * Get: cdTipoCargaBaseDadosUtilizada.
     * 
     * @return cdTipoCargaBaseDadosUtilizada
     */
    public Integer getCdTipoCargaBaseDadosUtilizada() {
        return cdTipoCargaBaseDadosUtilizada;
    }

    /**
     * Set: cdTipoCargaBaseDadosUtilizada.
     * 
     * @param cdTipoCargaBaseDadosUtilizada
     *            the cd tipo carga base dados utilizada
     */
    public void setCdTipoCargaBaseDadosUtilizada(
        Integer cdTipoCargaBaseDadosUtilizada) {
        this.cdTipoCargaBaseDadosUtilizada = cdTipoCargaBaseDadosUtilizada;
    }

    /**
     * Get: cdTipoConsistenciaIdentificador.
     * 
     * @return cdTipoConsistenciaIdentificador
     */
    public Integer getCdTipoConsistenciaIdentificador() {
        return cdTipoConsistenciaIdentificador;
    }

    /**
     * Set: cdTipoConsistenciaIdentificador.
     * 
     * @param cdTipoConsistenciaIdentificador
     *            the cd tipo consistencia identificador
     */
    public void setCdTipoConsistenciaIdentificador(
        Integer cdTipoConsistenciaIdentificador) {
        this.cdTipoConsistenciaIdentificador = cdTipoConsistenciaIdentificador;
    }

    /**
     * Get: cdTipoIdentificadorBeneficiario.
     * 
     * @return cdTipoIdentificadorBeneficiario
     */
    public Integer getCdTipoIdentificadorBeneficiario() {
        return cdTipoIdentificadorBeneficiario;
    }

    /**
     * Set: cdTipoIdentificadorBeneficiario.
     * 
     * @param cdTipoIdentificadorBeneficiario
     *            the cd tipo identificador beneficiario
     */
    public void setCdTipoIdentificadorBeneficiario(
        Integer cdTipoIdentificadorBeneficiario) {
        this.cdTipoIdentificadorBeneficiario = cdTipoIdentificadorBeneficiario;
    }

    /**
     * Get: dataFimPeriodoAcertoDados.
     * 
     * @return dataFimPeriodoAcertoDados
     */
    public Date getDataFimPeriodoAcertoDados() {
        return dataFimPeriodoAcertoDados;
    }

    /**
     * Set: dataFimPeriodoAcertoDados.
     * 
     * @param dataFimPeriodoAcertoDados
     *            the data fim periodo acerto dados
     */
    public void setDataFimPeriodoAcertoDados(Date dataFimPeriodoAcertoDados) {
        this.dataFimPeriodoAcertoDados = dataFimPeriodoAcertoDados;
    }

    /**
     * Get: dataFimRecadastramento.
     * 
     * @return dataFimRecadastramento
     */
    public Date getDataFimRecadastramento() {
        return dataFimRecadastramento;
    }

    /**
     * Set: dataFimRecadastramento.
     * 
     * @param dataFimRecadastramento
     *            the data fim recadastramento
     */
    public void setDataFimRecadastramento(Date dataFimRecadastramento) {
        this.dataFimRecadastramento = dataFimRecadastramento;
    }

    /**
     * Get: dataInicioPeriodoAcertoDados.
     * 
     * @return dataInicioPeriodoAcertoDados
     */
    public Date getDataInicioPeriodoAcertoDados() {
        return dataInicioPeriodoAcertoDados;
    }

    /**
     * Set: dataInicioPeriodoAcertoDados.
     * 
     * @param dataInicioPeriodoAcertoDados
     *            the data inicio periodo acerto dados
     */
    public void setDataInicioPeriodoAcertoDados(
        Date dataInicioPeriodoAcertoDados) {
        this.dataInicioPeriodoAcertoDados = dataInicioPeriodoAcertoDados;
    }

    /**
     * Get: dataInicioRecadastramento.
     * 
     * @return dataInicioRecadastramento
     */
    public Date getDataInicioRecadastramento() {
        return dataInicioRecadastramento;
    }

    /**
     * Set: dataInicioRecadastramento.
     * 
     * @param dataInicioRecadastramento
     *            the data inicio recadastramento
     */
    public void setDataInicioRecadastramento(Date dataInicioRecadastramento) {
        this.dataInicioRecadastramento = dataInicioRecadastramento;
    }

    /**
     * Get: dataLimiteInicioVinculoCargaBase.
     * 
     * @return dataLimiteInicioVinculoCargaBase
     */
    public Date getDataLimiteInicioVinculoCargaBase() {
        return dataLimiteInicioVinculoCargaBase;
    }

    /**
     * Set: dataLimiteInicioVinculoCargaBase.
     * 
     * @param dataLimiteInicioVinculoCargaBase
     *            the data limite inicio vinculo carga base
     */
    public void setDataLimiteInicioVinculoCargaBase(
        Date dataLimiteInicioVinculoCargaBase) {
        this.dataLimiteInicioVinculoCargaBase = dataLimiteInicioVinculoCargaBase;
    }

    /**
     * Get: qtddDiasCobrancaTarifaApuracao.
     * 
     * @return qtddDiasCobrancaTarifaApuracao
     */
    public String getQtddDiasCobrancaTarifaApuracao() {
        return qtddDiasCobrancaTarifaApuracao;
    }

    /**
     * Set: qtddDiasCobrancaTarifaApuracao.
     * 
     * @param qtddDiasCobrancaTarifaApuracao
     *            the qtdd dias cobranca tarifa apuracao
     */
    public void setQtddDiasCobrancaTarifaApuracao(
        String qtddDiasCobrancaTarifaApuracao) {
        this.qtddDiasCobrancaTarifaApuracao = qtddDiasCobrancaTarifaApuracao;
    }

    /**
     * Get: qtddEtapasRecadastramento.
     * 
     * @return qtddEtapasRecadastramento
     */
    public String getQtddEtapasRecadastramento() {
        return qtddEtapasRecadastramento;
    }

    /**
     * Set: qtddEtapasRecadastramento.
     * 
     * @param qtddEtapasRecadastramento
     *            the qtdd etapas recadastramento
     */
    public void setQtddEtapasRecadastramento(String qtddEtapasRecadastramento) {
        this.qtddEtapasRecadastramento = qtddEtapasRecadastramento;
    }

    /**
     * Get: qtddFasesEtapa.
     * 
     * @return qtddFasesEtapa
     */
    public String getQtddFasesEtapa() {
        return qtddFasesEtapa;
    }

    /**
     * Set: qtddFasesEtapa.
     * 
     * @param qtddFasesEtapa
     *            the qtdd fases etapa
     */
    public void setQtddFasesEtapa(String qtddFasesEtapa) {
        this.qtddFasesEtapa = qtddFasesEtapa;
    }

    /**
     * Get: qtddMesesPrazoEtapa.
     * 
     * @return qtddMesesPrazoEtapa
     */
    public String getQtddMesesPrazoEtapa() {
        return qtddMesesPrazoEtapa;
    }

    /**
     * Set: qtddMesesPrazoEtapa.
     * 
     * @param qtddMesesPrazoEtapa
     *            the qtdd meses prazo etapa
     */
    public void setQtddMesesPrazoEtapa(String qtddMesesPrazoEtapa) {
        this.qtddMesesPrazoEtapa = qtddMesesPrazoEtapa;
    }

    /**
     * Get: qtddMesesPrazoFase.
     * 
     * @return qtddMesesPrazoFase
     */
    public String getQtddMesesPrazoFase() {
        return qtddMesesPrazoFase;
    }

    /**
     * Set: qtddMesesPrazoFase.
     * 
     * @param qtddMesesPrazoFase
     *            the qtdd meses prazo fase
     */
    public void setQtddMesesPrazoFase(String qtddMesesPrazoFase) {
        this.qtddMesesPrazoFase = qtddMesesPrazoFase;
    }

    /**
     * Get: qtddMesesReajusteAutomaticoTarifa.
     * 
     * @return qtddMesesReajusteAutomaticoTarifa
     */
    public String getQtddMesesReajusteAutomaticoTarifa() {
        return qtddMesesReajusteAutomaticoTarifa;
    }

    /**
     * Set: qtddMesesReajusteAutomaticoTarifa.
     * 
     * @param qtddMesesReajusteAutomaticoTarifa
     *            the qtdd meses reajuste automatico tarifa
     */
    public void setQtddMesesReajusteAutomaticoTarifa(
        String qtddMesesReajusteAutomaticoTarifa) {
        this.qtddMesesReajusteAutomaticoTarifa = qtddMesesReajusteAutomaticoTarifa;
    }

    /**
     * Get: listaPeriodicidade.
     * 
     * @return listaPeriodicidade
     */
    public List<SelectItem> getListaPeriodicidade() {
        return listaPeriodicidade;
    }

    /**
     * Set: listaPeriodicidade.
     * 
     * @param listaPeriodicidade
     *            the lista periodicidade
     */
    public void setListaPeriodicidade(List<SelectItem> listaPeriodicidade) {
        this.listaPeriodicidade = listaPeriodicidade;
    }

    /**
     * Get: listaPeriodicidadeHash.
     * 
     * @return listaPeriodicidadeHash
     */
    Map<Integer, String> getListaPeriodicidadeHash() {
        return listaPeriodicidadeHash;
    }

    /**
     * Set lista periodicidade hash.
     * 
     * @param listaPeriodicidadeHash
     *            the lista periodicidade hash
     */
    void setListaPeriodicidadeHash(Map<Integer, String> listaPeriodicidadeHash) {
        this.listaPeriodicidadeHash = listaPeriodicidadeHash;
    }

    /**
     * Get: cdFormaAutPagamento.
     * 
     * @return cdFormaAutPagamento
     */
    public Integer getCdFormaAutPagamento() {
        return cdFormaAutPagamento;
    }

    /**
     * Set: cdFormaAutPagamento.
     * 
     * @param cdFormaAutPagamento
     *            the cd forma aut pagamento
     */
    public void setCdFormaAutPagamento(Integer cdFormaAutPagamento) {
        this.cdFormaAutPagamento = cdFormaAutPagamento;
    }

    /**
     * Get: cdFormaEnvioPagamento.
     * 
     * @return cdFormaEnvioPagamento
     */
    public Integer getCdFormaEnvioPagamento() {
        return cdFormaEnvioPagamento;
    }

    /**
     * Set: cdFormaEnvioPagamento.
     * 
     * @param cdFormaEnvioPagamento
     *            the cd forma envio pagamento
     */
    public void setCdFormaEnvioPagamento(Integer cdFormaEnvioPagamento) {
        this.cdFormaEnvioPagamento = cdFormaEnvioPagamento;
    }

    /**
     * Get: cdTipoContFloating.
     * 
     * @return cdTipoContFloating
     */
    public Integer getCdTipoContFloating() {
        return cdTipoContFloating;
    }

    /**
     * Set: cdTipoContFloating.
     * 
     * @param cdTipoContFloating
     *            the cd tipo cont floating
     */
    public void setCdTipoContFloating(Integer cdTipoContFloating) {
        this.cdTipoContFloating = cdTipoContFloating;
    }

    /**
     * Get: cdUtilizaPreAut.
     * 
     * @return cdUtilizaPreAut
     */
    public Integer getCdUtilizaPreAut() {
        return cdUtilizaPreAut;
    }

    /**
     * Set: cdUtilizaPreAut.
     * 
     * @param cdUtilizaPreAut
     *            the cd utiliza pre aut
     */
    public void setCdUtilizaPreAut(Integer cdUtilizaPreAut) {
        this.cdUtilizaPreAut = cdUtilizaPreAut;
    }

    /**
     * Get: cdParametroTela.
     * 
     * @return cdParametroTela
     */
    public int getCdParametroTela() {
        return cdParametroTela;
    }

    /**
     * Set: cdParametroTela.
     * 
     * @param cdParametroTela
     *            the cd parametro tela
     */
    public void setCdParametroTela(int cdParametroTela) {
        this.cdParametroTela = cdParametroTela;
    }

    /**
     * Get: dataLimiteDesc.
     * 
     * @return dataLimiteDesc
     */
    public String getDataLimiteDesc() {
        if (getDataLimite() != null) {
            return FormatarData.formataDiaMesAno(getDataLimite());
        }
        return "";
    }

    /**
     * Get: dsPeriodicidadeRastreamento.
     * 
     * @return dsPeriodicidadeRastreamento
     */
    public String getDsPeriodicidadeRastreamento() {
        return dsPeriodicidadeRastreamento;
    }

    /**
     * Set: dsPeriodicidadeRastreamento.
     * 
     * @param dsPeriodicidadeRastreamento
     *            the ds periodicidade rastreamento
     */
    public void setDsPeriodicidadeRastreamento(
        String dsPeriodicidadeRastreamento) {
        this.dsPeriodicidadeRastreamento = dsPeriodicidadeRastreamento;
    }

    /**
     * Get: dsPeriodicidadeCobranca.
     * 
     * @return dsPeriodicidadeCobranca
     */
    public String getDsPeriodicidadeCobranca() {
        return dsPeriodicidadeCobranca;
    }

    /**
     * Set: dsPeriodicidadeCobranca.
     * 
     * @param dsPeriodicidadeCobranca
     *            the ds periodicidade cobranca
     */
    public void setDsPeriodicidadeCobranca(String dsPeriodicidadeCobranca) {
        this.dsPeriodicidadeCobranca = dsPeriodicidadeCobranca;
    }

    /**
     * Get: dsPeriodicidadeReajusteTarifa.
     * 
     * @return dsPeriodicidadeReajusteTarifa
     */
    public String getDsPeriodicidadeReajusteTarifa() {
        return dsPeriodicidadeReajusteTarifa;
    }

    /**
     * Set: dsPeriodicidadeReajusteTarifa.
     * 
     * @param dsPeriodicidadeReajusteTarifa
     *            the ds periodicidade reajuste tarifa
     */
    public void setDsPeriodicidadeReajusteTarifa(
        String dsPeriodicidadeReajusteTarifa) {
        this.dsPeriodicidadeReajusteTarifa = dsPeriodicidadeReajusteTarifa;
    }

    /**
     * Get: dsPeriodicidadeManutencao.
     * 
     * @return dsPeriodicidadeManutencao
     */
    public String getDsPeriodicidadeManutencao() {
        return dsPeriodicidadeManutencao;
    }

    /**
     * Set: dsPeriodicidadeManutencao.
     * 
     * @param dsPeriodicidadeManutencao
     *            the ds periodicidade manutencao
     */
    public void setDsPeriodicidadeManutencao(String dsPeriodicidadeManutencao) {
        this.dsPeriodicidadeManutencao = dsPeriodicidadeManutencao;
    }

    /**
     * Get: dsPeriodicidade.
     * 
     * @return dsPeriodicidade
     */
    public String getDsPeriodicidade() {
        return dsPeriodicidade;
    }

    /**
     * Set: dsPeriodicidade.
     * 
     * @param dsPeriodicidade
     *            the ds periodicidade
     */
    public void setDsPeriodicidade(String dsPeriodicidade) {
        this.dsPeriodicidade = dsPeriodicidade;
    }

    /**
     * Get: dsPeriodicidadeCobrancaTarifa.
     * 
     * @return dsPeriodicidadeCobrancaTarifa
     */
    public String getDsPeriodicidadeCobrancaTarifa() {
        return dsPeriodicidadeCobrancaTarifa;
    }

    /**
     * Set: dsPeriodicidadeCobrancaTarifa.
     * 
     * @param dsPeriodicidadeCobrancaTarifa
     *            the ds periodicidade cobranca tarifa
     */
    public void setDsPeriodicidadeCobrancaTarifa(
        String dsPeriodicidadeCobrancaTarifa) {
        this.dsPeriodicidadeCobrancaTarifa = dsPeriodicidadeCobrancaTarifa;
    }

    /**
     * Get: listaIndiceEconomico.
     * 
     * @return listaIndiceEconomico
     */
    public List<SelectItem> getListaIndiceEconomico() {
        return listaIndiceEconomico;
    }

    /**
     * Set: listaIndiceEconomico.
     * 
     * @param listaIndiceEconomico
     *            the lista indice economico
     */
    public void setListaIndiceEconomico(List<SelectItem> listaIndiceEconomico) {
        this.listaIndiceEconomico = listaIndiceEconomico;
    }

    /**
     * Get: listaIndiceEconomicoHash.
     * 
     * @return listaIndiceEconomicoHash
     */
    Map<Integer, String> getListaIndiceEconomicoHash() {
        return listaIndiceEconomicoHash;
    }

    /**
     * Set lista indice economico hash.
     * 
     * @param listaIndiceEconomicoHash
     *            the lista indice economico hash
     */
    void setListaIndiceEconomicoHash(
        Map<Integer, String> listaIndiceEconomicoHash) {
        this.listaIndiceEconomicoHash = listaIndiceEconomicoHash;
    }

    /**
     * Get: dsPeriodicidadeEnvioRemessa.
     * 
     * @return dsPeriodicidadeEnvioRemessa
     */
    public String getDsPeriodicidadeEnvioRemessa() {

        return dsPeriodicidadeEnvioRemessa;
    }

    /**
     * Set: dsPeriodicidadeEnvioRemessa.
     * 
     * @param dsPeriodicidadeEnvioRemessa
     *            the ds periodicidade envio remessa
     */
    public void setDsPeriodicidadeEnvioRemessa(
        String dsPeriodicidadeEnvioRemessa) {
        this.dsPeriodicidadeEnvioRemessa = dsPeriodicidadeEnvioRemessa;
    }

    /**
     * Get: dataFimPeriodoAcertoDadosDesc.
     * 
     * @return dataFimPeriodoAcertoDadosDesc
     */
    public String getDataFimPeriodoAcertoDadosDesc() {
        if (getDataFimPeriodoAcertoDados() != null) {
            return FormatarData
            .formataDiaMesAno(getDataFimPeriodoAcertoDados());
        }
        return "";
    }

    /**
     * Get: dataFimRecadastramentoDesc.
     * 
     * @return dataFimRecadastramentoDesc
     */
    public String getDataFimRecadastramentoDesc() {
        if (getDataInicioRecadastramento() != null) {
            return FormatarData
            .formataDiaMesAno(getDataInicioRecadastramento());
        }
        return "";
    }

    /**
     * Get: dataInicioPeriodoAcertoDadosDesc.
     * 
     * @return dataInicioPeriodoAcertoDadosDesc
     */
    public String getDataInicioPeriodoAcertoDadosDesc() {
        if (getDataInicioPeriodoAcertoDados() != null) {
            return FormatarData
            .formataDiaMesAno(getDataInicioPeriodoAcertoDados());
        }
        return "";

    }

    /**
     * Get: dataInicioRecadastramentoDesc.
     * 
     * @return dataInicioRecadastramentoDesc
     */
    public String getDataInicioRecadastramentoDesc() {
        if (getDataInicioRecadastramento() != null) {
            return FormatarData
            .formataDiaMesAno(getDataInicioRecadastramento());
        }
        return "";
    }

    /**
     * Get: dataLimiteInicioVinculoCargaBaseDesc.
     * 
     * @return dataLimiteInicioVinculoCargaBaseDesc
     */
    public String getDataLimiteInicioVinculoCargaBaseDesc() {
        if (getDataLimiteInicioVinculoCargaBase() != null) {
            return FormatarData
            .formataDiaMesAno(getDataLimiteInicioVinculoCargaBase());
        }
        return "";
    }

    /**
     * Get: dsIndiceEconomicoReajusteTarifa.
     * 
     * @return dsIndiceEconomicoReajusteTarifa
     */
    public String getDsIndiceEconomicoReajusteTarifa() {
        return dsIndiceEconomicoReajusteTarifa;
    }

    /**
     * Set: dsIndiceEconomicoReajusteTarifa.
     * 
     * @param dsIndiceEconomicoReajusteTarifa
     *            the ds indice economico reajuste tarifa
     */
    public void setDsIndiceEconomicoReajusteTarifa(
        String dsIndiceEconomicoReajusteTarifa) {
        this.dsIndiceEconomicoReajusteTarifa = dsIndiceEconomicoReajusteTarifa;
    }

    /**
     * Get: diaFechamentoApuracao.
     * 
     * @return diaFechamentoApuracao
     */
    public String getDiaFechamentoApuracao() {
        return diaFechamentoApuracao;
    }

    /**
     * Set: diaFechamentoApuracao.
     * 
     * @param diaFechamentoApuracao
     *            the dia fechamento apuracao
     */
    public void setDiaFechamentoApuracao(String diaFechamentoApuracao) {
        this.diaFechamentoApuracao = diaFechamentoApuracao;
    }

    /**
     * Get: cdEnderecoEnvio.
     * 
     * @return cdEnderecoEnvio
     */
    public Integer getCdEnderecoEnvio() {
        return cdEnderecoEnvio;
    }

    /**
     * Set: cdEnderecoEnvio.
     * 
     * @param cdEnderecoEnvio
     *            the cd endereco envio
     */
    public void setCdEnderecoEnvio(Integer cdEnderecoEnvio) {
        this.cdEnderecoEnvio = cdEnderecoEnvio;
    }

    /**
     * Get: cdPeriodicidadeEnvio.
     * 
     * @return cdPeriodicidadeEnvio
     */
    public Integer getCdPeriodicidadeEnvio() {
        return cdPeriodicidadeEnvio;
    }

    /**
     * Set: cdPeriodicidadeEnvio.
     * 
     * @param cdPeriodicidadeEnvio
     *            the cd periodicidade envio
     */
    public void setCdPeriodicidadeEnvio(Integer cdPeriodicidadeEnvio) {
        this.cdPeriodicidadeEnvio = cdPeriodicidadeEnvio;
    }

    /**
     * Get: cdPermitirAgrupamento.
     * 
     * @return cdPermitirAgrupamento
     */
    public Integer getCdPermitirAgrupamento() {
        return cdPermitirAgrupamento;
    }

    /**
     * Set: cdPermitirAgrupamento.
     * 
     * @param cdPermitirAgrupamento
     *            the cd permitir agrupamento
     */
    public void setCdPermitirAgrupamento(Integer cdPermitirAgrupamento) {
        this.cdPermitirAgrupamento = cdPermitirAgrupamento;
    }

    /**
     * Get: qtddDiasAntecipacao.
     * 
     * @return qtddDiasAntecipacao
     */
    public String getQtddDiasAntecipacao() {
        return qtddDiasAntecipacao;
    }

    /**
     * Set: qtddDiasAntecipacao.
     * 
     * @param qtddDiasAntecipacao
     *            the qtdd dias antecipacao
     */
    public void setQtddDiasAntecipacao(String qtddDiasAntecipacao) {
        this.qtddDiasAntecipacao = qtddDiasAntecipacao;
    }

    /**
     * Get: cdOcorrenciaDebito.
     * 
     * @return cdOcorrenciaDebito
     */
    public Integer getCdOcorrenciaDebito() {
        return cdOcorrenciaDebito;
    }

    /**
     * Set: cdOcorrenciaDebito.
     * 
     * @param cdOcorrenciaDebito
     *            the cd ocorrencia debito
     */
    public void setCdOcorrenciaDebito(Integer cdOcorrenciaDebito) {
        this.cdOcorrenciaDebito = cdOcorrenciaDebito;
    }

    /**
     * Get: cdPermiteOutrosTipos.
     * 
     * @return cdPermiteOutrosTipos
     */
    public String getCdPermiteOutrosTipos() {
        return cdPermiteOutrosTipos;
    }

    /**
     * Set: cdPermiteOutrosTipos.
     * 
     * @param cdPermiteOutrosTipos
     *            the cd permite outros tipos
     */
    public void setCdPermiteOutrosTipos(String cdPermiteOutrosTipos) {
        this.cdPermiteOutrosTipos = cdPermiteOutrosTipos;
    }

    /**
     * Get: cdGerLancFutCreditoFiltro.
     * 
     * @return cdGerLancFutCreditoFiltro
     */
    public Integer getCdGerLancFutCreditoFiltro() {
        return cdGerLancFutCreditoFiltro;
    }

    /**
     * Set: cdGerLancFutCreditoFiltro.
     * 
     * @param cdGerLancFutCreditoFiltro
     *            the cd ger lanc fut credito filtro
     */
    public void setCdGerLancFutCreditoFiltro(Integer cdGerLancFutCreditoFiltro) {
        this.cdGerLancFutCreditoFiltro = cdGerLancFutCreditoFiltro;
    }

    /**
     * Get: cdGerLancFutDebitoFiltro.
     * 
     * @return cdGerLancFutDebitoFiltro
     */
    public Integer getCdGerLancFutDebitoFiltro() {
        return cdGerLancFutDebitoFiltro;
    }

    /**
     * Set: cdGerLancFutDebitoFiltro.
     * 
     * @param cdGerLancFutDebitoFiltro
     *            the cd ger lanc fut debito filtro
     */
    public void setCdGerLancFutDebitoFiltro(Integer cdGerLancFutDebitoFiltro) {
        this.cdGerLancFutDebitoFiltro = cdGerLancFutDebitoFiltro;
    }

    /**
     * Get: cdPermiteFavConsultPagFiltro.
     * 
     * @return cdPermiteFavConsultPagFiltro
     */
    public Integer getCdPermiteFavConsultPagFiltro() {
        return cdPermiteFavConsultPagFiltro;
    }

    /**
     * Set: cdPermiteFavConsultPagFiltro.
     * 
     * @param cdPermiteFavConsultPagFiltro
     *            the cd permite fav consult pag filtro
     */
    public void setCdPermiteFavConsultPagFiltro(
        Integer cdPermiteFavConsultPagFiltro) {
        this.cdPermiteFavConsultPagFiltro = cdPermiteFavConsultPagFiltro;
    }

    /**
     * Get: cdUtilizaCadFavorFiltro.
     * 
     * @return cdUtilizaCadFavorFiltro
     */
    public Integer getCdUtilizaCadFavorFiltro() {
        return cdUtilizaCadFavorFiltro;
    }

    /**
     * Set: cdUtilizaCadFavorFiltro.
     * 
     * @param cdUtilizaCadFavorFiltro
     *            the cd utiliza cad favor filtro
     */
    public void setCdUtilizaCadFavorFiltro(Integer cdUtilizaCadFavorFiltro) {
        this.cdUtilizaCadFavorFiltro = cdUtilizaCadFavorFiltro;
    }

    /**
     * Get: qtddDiasExpiracao.
     * 
     * @return qtddDiasExpiracao
     */
    public String getQtddDiasExpiracao() {
        return qtddDiasExpiracao;
    }

    /**
     * Set: qtddDiasExpiracao.
     * 
     * @param qtddDiasExpiracao
     *            the qtdd dias expiracao
     */
    public void setQtddDiasExpiracao(String qtddDiasExpiracao) {
        this.qtddDiasExpiracao = qtddDiasExpiracao;
    }

    /**
     * Get: cdEfetuaConsistencia.
     * 
     * @return cdEfetuaConsistencia
     */
    public String getCdEfetuaConsistencia() {
        return cdEfetuaConsistencia;
    }

    /**
     * Set: cdEfetuaConsistencia.
     * 
     * @param cdEfetuaConsistencia
     *            the cd efetua consistencia
     */
    public void setCdEfetuaConsistencia(String cdEfetuaConsistencia) {
        this.cdEfetuaConsistencia = cdEfetuaConsistencia;
    }

    /**
     * Get: cdGerarLancamentoProgramado.
     * 
     * @return cdGerarLancamentoProgramado
     */
    public String getCdGerarLancamentoProgramado() {
        return cdGerarLancamentoProgramado;
    }

    /**
     * Set: cdGerarLancamentoProgramado.
     * 
     * @param cdGerarLancamentoProgramado
     *            the cd gerar lancamento programado
     */
    public void setCdGerarLancamentoProgramado(
        String cdGerarLancamentoProgramado) {
        this.cdGerarLancamentoProgramado = cdGerarLancamentoProgramado;
    }

    /**
     * Get: cdTipoTratamento.
     * 
     * @return cdTipoTratamento
     */
    public Integer getCdTipoTratamento() {
        return cdTipoTratamento;
    }

    /**
     * Set: cdTipoTratamento.
     * 
     * @param cdTipoTratamento
     *            the cd tipo tratamento
     */
    public void setCdTipoTratamento(Integer cdTipoTratamento) {
        this.cdTipoTratamento = cdTipoTratamento;
    }

    /**
     * Get: cdPermiteEstorno.
     * 
     * @return cdPermiteEstorno
     */
    public Integer getCdPermiteEstorno() {
        return cdPermiteEstorno;
    }

    /**
     * Set: cdPermiteEstorno.
     * 
     * @param cdPermiteEstorno
     *            the cd permite estorno
     */
    public void setCdPermiteEstorno(Integer cdPermiteEstorno) {
        this.cdPermiteEstorno = cdPermiteEstorno;
    }

    /**
     * Get: percentualMaximoRegistro.
     * 
     * @return percentualMaximoRegistro
     */
    public String getPercentualMaximoRegistro() {
        return percentualMaximoRegistro;
    }

    /**
     * Set: percentualMaximoRegistro.
     * 
     * @param percentualMaximoRegistro
     *            the percentual maximo registro
     */
    public void setPercentualMaximoRegistro(String percentualMaximoRegistro) {
        this.percentualMaximoRegistro = percentualMaximoRegistro;
    }

    /**
     * Get: qtddMaximaRegistro.
     * 
     * @return qtddMaximaRegistro
     */
    public String getQtddMaximaRegistro() {
        return qtddMaximaRegistro;
    }

    /**
     * Set: qtddMaximaRegistro.
     * 
     * @param qtddMaximaRegistro
     *            the qtdd maxima registro
     */
    public void setQtddMaximaRegistro(String qtddMaximaRegistro) {
        this.qtddMaximaRegistro = qtddMaximaRegistro;
    }

    /**
     * Get: cdPermiteDebitoOnline.
     * 
     * @return cdPermiteDebitoOnline
     */
    public String getCdPermiteDebitoOnline() {
        return cdPermiteDebitoOnline;
    }

    /**
     * Set: cdPermiteDebitoOnline.
     * 
     * @param cdPermiteDebitoOnline
     *            the cd permite debito online
     */
    public void setCdPermiteDebitoOnline(String cdPermiteDebitoOnline) {
        this.cdPermiteDebitoOnline = cdPermiteDebitoOnline;
    }

    /**
     * Get: permitePagarMenor.
     * 
     * @return permitePagarMenor
     */
    public String getPermitePagarMenor() {
        return permitePagarMenor;
    }

    /**
     * Set: permitePagarMenor.
     * 
     * @param permitePagarMenor
     *            the permite pagar menor
     */
    public void setPermitePagarMenor(String permitePagarMenor) {
        this.permitePagarMenor = permitePagarMenor;
    }

    /**
     * Get: permitePagarVencido.
     * 
     * @return permitePagarVencido
     */
    public String getPermitePagarVencido() {
        return permitePagarVencido;
    }

    /**
     * Set: permitePagarVencido.
     * 
     * @param permitePagarVencido
     *            the permite pagar vencido
     */
    public void setPermitePagarVencido(String permitePagarVencido) {
        this.permitePagarVencido = permitePagarVencido;
    }

    /**
     * Get: qtddLimiteDiasPagamentoVencido.
     * 
     * @return qtddLimiteDiasPagamentoVencido
     */
    public String getQtddLimiteDiasPagamentoVencido() {
        return qtddLimiteDiasPagamentoVencido;
    }

    /**
     * Set: qtddLimiteDiasPagamentoVencido.
     * 
     * @param qtddLimiteDiasPagamentoVencido
     *            the qtdd limite dias pagamento vencido
     */
    public void setQtddLimiteDiasPagamentoVencido(
        String qtddLimiteDiasPagamentoVencido) {
        this.qtddLimiteDiasPagamentoVencido = qtddLimiteDiasPagamentoVencido;
    }

    /**
     * Get: agendarTituloFilial.
     * 
     * @return agendarTituloFilial
     */
    public String getAgendarTituloFilial() {
        return agendarTituloFilial;
    }

    /**
     * Set: agendarTituloFilial.
     * 
     * @param agendarTituloFilial
     *            the agendar titulo filial
     */
    public void setAgendarTituloFilial(String agendarTituloFilial) {
        this.agendarTituloFilial = agendarTituloFilial;
    }

    /**
     * Get: agendarTituloProprio.
     * 
     * @return agendarTituloProprio
     */
    public String getAgendarTituloProprio() {
        return agendarTituloProprio;
    }

    /**
     * Set: agendarTituloProprio.
     * 
     * @param agendarTituloProprio
     *            the agendar titulo proprio
     */
    public void setAgendarTituloProprio(String agendarTituloProprio) {
        this.agendarTituloProprio = agendarTituloProprio;
    }

    /**
     * Get: bloquearEmissaoPapeleta.
     * 
     * @return bloquearEmissaoPapeleta
     */
    public String getBloquearEmissaoPapeleta() {
        return bloquearEmissaoPapeleta;
    }

    /**
     * Set: bloquearEmissaoPapeleta.
     * 
     * @param bloquearEmissaoPapeleta
     *            the bloquear emissao papeleta
     */
    public void setBloquearEmissaoPapeleta(String bloquearEmissaoPapeleta) {
        this.bloquearEmissaoPapeleta = bloquearEmissaoPapeleta;
    }

    /**
     * Get: capturarTitulos.
     * 
     * @return capturarTitulos
     */
    public String getCapturarTitulos() {
        return capturarTitulos;
    }

    /**
     * Set: capturarTitulos.
     * 
     * @param capturarTitulos
     *            the capturar titulos
     */
    public void setCapturarTitulos(String capturarTitulos) {
        this.capturarTitulos = capturarTitulos;
    }

    /**
     * Get: cdTipoRastreamentoTitulos.
     * 
     * @return cdTipoRastreamentoTitulos
     */
    public Integer getCdTipoRastreamentoTitulos() {
        return cdTipoRastreamentoTitulos;
    }

    /**
     * Set: cdTipoRastreamentoTitulos.
     * 
     * @param cdTipoRastreamentoTitulos
     *            the cd tipo rastreamento titulos
     */
    public void setCdTipoRastreamentoTitulos(Integer cdTipoRastreamentoTitulos) {
        this.cdTipoRastreamentoTitulos = cdTipoRastreamentoTitulos;
    }

    /**
     * Get: dataInicioBloqueioPapeleta.
     * 
     * @return dataInicioBloqueioPapeleta
     */
    public Date getDataInicioBloqueioPapeleta() {
        return dataInicioBloqueioPapeleta;
    }

    /**
     * Set: dataInicioBloqueioPapeleta.
     * 
     * @param dataInicioBloqueioPapeleta
     *            the data inicio bloqueio papeleta
     */
    public void setDataInicioBloqueioPapeleta(Date dataInicioBloqueioPapeleta) {
        this.dataInicioBloqueioPapeleta = dataInicioBloqueioPapeleta;
    }

    /**
     * Get: dataInicioRastreamento.
     * 
     * @return dataInicioRastreamento
     */
    public Date getDataInicioRastreamento() {
        return dataInicioRastreamento;
    }

    /**
     * Set: dataInicioRastreamento.
     * 
     * @param dataInicioRastreamento
     *            the data inicio rastreamento
     */
    public void setDataInicioRastreamento(Date dataInicioRastreamento) {
        this.dataInicioRastreamento = dataInicioRastreamento;
    }

    /**
     * Get: dataRegistroTitulo.
     * 
     * @return dataRegistroTitulo
     */
    public Date getDataRegistroTitulo() {
        return dataRegistroTitulo;
    }

    /**
     * Set: dataRegistroTitulo.
     * 
     * @param dataRegistroTitulo
     *            the data registro titulo
     */
    public void setDataRegistroTitulo(Date dataRegistroTitulo) {
        this.dataRegistroTitulo = dataRegistroTitulo;
    }

    /**
     * Get: rastrearNotas.
     * 
     * @return rastrearNotas
     */
    public String getRastrearNotas() {
        return rastrearNotas;
    }

    /**
     * Set: rastrearNotas.
     * 
     * @param rastrearNotas
     *            the rastrear notas
     */
    public void setRastrearNotas(String rastrearNotas) {
        this.rastrearNotas = rastrearNotas;
    }

    /**
     * Get: rastrearTitulo.
     * 
     * @return rastrearTitulo
     */
    public String getRastrearTitulo() {
        return rastrearTitulo;
    }

    /**
     * Set: rastrearTitulo.
     * 
     * @param rastrearTitulo
     *            the rastrear titulo
     */
    public void setRastrearTitulo(String rastrearTitulo) {
        this.rastrearTitulo = rastrearTitulo;
    }

    /**
     * Get: cdTipoComprovacao.
     * 
     * @return cdTipoComprovacao
     */
    public Integer getCdTipoComprovacao() {
        return cdTipoComprovacao;
    }

    /**
     * Set: cdTipoComprovacao.
     * 
     * @param cdTipoComprovacao
     *            the cd tipo comprovacao
     */
    public void setCdTipoComprovacao(Integer cdTipoComprovacao) {
        this.cdTipoComprovacao = cdTipoComprovacao;
    }

    /**
     * Get: cdUtilizarMsgPerOnline.
     * 
     * @return cdUtilizarMsgPerOnline
     */
    public Integer getCdUtilizarMsgPerOnline() {
        return cdUtilizarMsgPerOnline;
    }

    /**
     * Set: cdUtilizarMsgPerOnline.
     * 
     * @param cdUtilizarMsgPerOnline
     *            the cd utilizar msg per online
     */
    public void setCdUtilizarMsgPerOnline(Integer cdUtilizarMsgPerOnline) {
        this.cdUtilizarMsgPerOnline = cdUtilizarMsgPerOnline;
    }

    /**
     * Get: qtddDiasAntecedeInicio.
     * 
     * @return qtddDiasAntecedeInicio
     */
    public String getQtddDiasAntecedeInicio() {
        return qtddDiasAntecedeInicio;
    }

    /**
     * Set: qtddDiasAntecedeInicio.
     * 
     * @param qtddDiasAntecedeInicio
     *            the qtdd dias antecede inicio
     */
    public void setQtddDiasAntecedeInicio(String qtddDiasAntecedeInicio) {
        this.qtddDiasAntecedeInicio = qtddDiasAntecedeInicio;
    }

    /**
     * Get: qtddDiasAvisoVenc.
     * 
     * @return qtddDiasAvisoVenc
     */
    public String getQtddDiasAvisoVenc() {
        return qtddDiasAvisoVenc;
    }

    /**
     * Set: qtddDiasAvisoVenc.
     * 
     * @param qtddDiasAvisoVenc
     *            the qtdd dias aviso venc
     */
    public void setQtddDiasAvisoVenc(String qtddDiasAvisoVenc) {
        this.qtddDiasAvisoVenc = qtddDiasAvisoVenc;
    }

    /**
     * Get: qtddMesesPeriodicidade.
     * 
     * @return qtddMesesPeriodicidade
     */
    public String getQtddMesesPeriodicidade() {
        return qtddMesesPeriodicidade;
    }

    /**
     * Set: qtddMesesPeriodicidade.
     * 
     * @param qtddMesesPeriodicidade
     *            the qtdd meses periodicidade
     */
    public void setQtddMesesPeriodicidade(String qtddMesesPeriodicidade) {
        this.qtddMesesPeriodicidade = qtddMesesPeriodicidade;
    }

    /**
     * Get: cdControlePagFavorecido.
     * 
     * @return cdControlePagFavorecido
     */
    public Integer getCdControlePagFavorecido() {
        return cdControlePagFavorecido;
    }

    /**
     * Set: cdControlePagFavorecido.
     * 
     * @param cdControlePagFavorecido
     *            the cd controle pag favorecido
     */
    public void setCdControlePagFavorecido(Integer cdControlePagFavorecido) {
        this.cdControlePagFavorecido = cdControlePagFavorecido;
    }

    /**
     * Get: cdEfetivacao.
     * 
     * @return cdEfetivacao
     */
    public Integer getCdEfetivacao() {
        return cdEfetivacao;
    }

    /**
     * Set: cdEfetivacao.
     * 
     * @param cdEfetivacao
     *            the cd efetivacao
     */
    public void setCdEfetivacao(Integer cdEfetivacao) {
        this.cdEfetivacao = cdEfetivacao;
    }

    /**
     * Get: cdPermiteContingenciaPag.
     * 
     * @return cdPermiteContingenciaPag
     */
    public Integer getCdPermiteContingenciaPag() {
        return cdPermiteContingenciaPag;
    }

    /**
     * Set: cdPermiteContingenciaPag.
     * 
     * @param cdPermiteContingenciaPag
     *            the cd permite contingencia pag
     */
    public void setCdPermiteContingenciaPag(Integer cdPermiteContingenciaPag) {
        this.cdPermiteContingenciaPag = cdPermiteContingenciaPag;
    }

    /**
     * Get: percentualInconsLote.
     * 
     * @return percentualInconsLote
     */
    public String getPercentualInconsLote() {
        return percentualInconsLote;
    }

    /**
     * Set: percentualInconsLote.
     * 
     * @param percentualInconsLote
     *            the percentual incons lote
     */
    public void setPercentualInconsLote(String percentualInconsLote) {
        this.percentualInconsLote = percentualInconsLote;
    }

    /**
     * Get: qtddMaxInconsLote.
     * 
     * @return qtddMaxInconsLote
     */
    public String getQtddMaxInconsLote() {
        return qtddMaxInconsLote;
    }

    /**
     * Set: qtddMaxInconsLote.
     * 
     * @param qtddMaxInconsLote
     *            the qtdd max incons lote
     */
    public void setQtddMaxInconsLote(String qtddMaxInconsLote) {
        this.qtddMaxInconsLote = qtddMaxInconsLote;
    }

    /**
     * Get: cdPermiteConsultaFavorecido.
     * 
     * @return cdPermiteConsultaFavorecido
     */
    public Integer getCdPermiteConsultaFavorecido() {
        return cdPermiteConsultaFavorecido;
    }

    /**
     * Set: cdPermiteConsultaFavorecido.
     * 
     * @param cdPermiteConsultaFavorecido
     *            the cd permite consulta favorecido
     */
    public void setCdPermiteConsultaFavorecido(
        Integer cdPermiteConsultaFavorecido) {
        this.cdPermiteConsultaFavorecido = cdPermiteConsultaFavorecido;
    }

    /**
     * Get: diaFechamentoApuracaoTarifaMensal.
     * 
     * @return diaFechamentoApuracaoTarifaMensal
     */
    public String getDiaFechamentoApuracaoTarifaMensal() {
        return diaFechamentoApuracaoTarifaMensal;
    }

    /**
     * Set: diaFechamentoApuracaoTarifaMensal.
     * 
     * @param diaFechamentoApuracaoTarifaMensal
     *            the dia fechamento apuracao tarifa mensal
     */
    public void setDiaFechamentoApuracaoTarifaMensal(
        String diaFechamentoApuracaoTarifaMensal) {
        this.diaFechamentoApuracaoTarifaMensal = diaFechamentoApuracaoTarifaMensal;
    }

    /**
     * Get: cdTipoConsisProprietario.
     * 
     * @return cdTipoConsisProprietario
     */
    public Integer getCdTipoConsisProprietario() {
        return cdTipoConsisProprietario;
    }

    /**
     * Set: cdTipoConsisProprietario.
     * 
     * @param cdTipoConsisProprietario
     *            the cd tipo consis proprietario
     */
    public void setCdTipoConsisProprietario(Integer cdTipoConsisProprietario) {
        this.cdTipoConsisProprietario = cdTipoConsisProprietario;
    }

    /**
     * Get: cdTratValorDivergente.
     * 
     * @return cdTratValorDivergente
     */
    public Integer getCdTratValorDivergente() {
        return cdTratValorDivergente;
    }

    /**
     * Set: cdTratValorDivergente.
     * 
     * @param cdTratValorDivergente
     *            the cd trat valor divergente
     */
    public void setCdTratValorDivergente(Integer cdTratValorDivergente) {
        this.cdTratValorDivergente = cdTratValorDivergente;
    }

    /**
     * Get: dsPeriodicidadeEnvio.
     * 
     * @return dsPeriodicidadeEnvio
     */
    public String getDsPeriodicidadeEnvio() {
        return dsPeriodicidadeEnvio;
    }

    /**
     * Set: dsPeriodicidadeEnvio.
     * 
     * @param dsPeriodicidadeEnvio
     *            the ds periodicidade envio
     */
    public void setDsPeriodicidadeEnvio(String dsPeriodicidadeEnvio) {
        this.dsPeriodicidadeEnvio = dsPeriodicidadeEnvio;
    }

    /**
     * Get: dsDataInicioBloqueioPapeleta.
     * 
     * @return dsDataInicioBloqueioPapeleta
     */
    public String getDsDataInicioBloqueioPapeleta() {
        return dsDataInicioBloqueioPapeleta;
    }

    /**
     * Set: dsDataInicioBloqueioPapeleta.
     * 
     * @param dsDataInicioBloqueioPapeleta
     *            the ds data inicio bloqueio papeleta
     */
    public void setDsDataInicioBloqueioPapeleta(
        String dsDataInicioBloqueioPapeleta) {
        this.dsDataInicioBloqueioPapeleta = dsDataInicioBloqueioPapeleta;
    }

    /**
     * Get: dsDataInicioRastreamento.
     * 
     * @return dsDataInicioRastreamento
     */
    public String getDsDataInicioRastreamento() {
        return dsDataInicioRastreamento;
    }

    /**
     * Set: dsDataInicioRastreamento.
     * 
     * @param dsDataInicioRastreamento
     *            the ds data inicio rastreamento
     */
    public void setDsDataInicioRastreamento(String dsDataInicioRastreamento) {
        this.dsDataInicioRastreamento = dsDataInicioRastreamento;
    }

    /**
     * Get: dsDataRegistroTitulo.
     * 
     * @return dsDataRegistroTitulo
     */
    public String getDsDataRegistroTitulo() {
        return dsDataRegistroTitulo;
    }

    /**
     * Set: dsDataRegistroTitulo.
     * 
     * @param dsDataRegistroTitulo
     *            the ds data registro titulo
     */
    public void setDsDataRegistroTitulo(String dsDataRegistroTitulo) {
        this.dsDataRegistroTitulo = dsDataRegistroTitulo;
    }

    /**
     * Get: cdPermiteConsulFavorecido.
     * 
     * @return cdPermiteConsulFavorecido
     */
    public Integer getCdPermiteConsulFavorecido() {
        return cdPermiteConsulFavorecido;
    }

    /**
     * Set: cdPermiteConsulFavorecido.
     * 
     * @param cdPermiteConsulFavorecido
     *            the cd permite consul favorecido
     */
    public void setCdPermiteConsulFavorecido(Integer cdPermiteConsulFavorecido) {
        this.cdPermiteConsulFavorecido = cdPermiteConsulFavorecido;
    }

    /**
     * Get: dsTipoConsisProprietario.
     * 
     * @return dsTipoConsisProprietario
     */
    public String getDsTipoConsisProprietario() {
        return dsTipoConsisProprietario;
    }

    /**
     * Set: dsTipoConsisProprietario.
     * 
     * @param dsTipoConsisProprietario
     *            the ds tipo consis proprietario
     */
    public void setDsTipoConsisProprietario(String dsTipoConsisProprietario) {
        this.dsTipoConsisProprietario = dsTipoConsisProprietario;
    }

    /**
     * Get: listaTipoServico.
     * 
     * @return listaTipoServico
     */
    public List<SelectItem> getListaTipoServico() {
        return listaTipoServico;
    }

    /**
     * Set: listaTipoServico.
     * 
     * @param listaTipoServico
     *            the lista tipo servico
     */
    public void setListaTipoServico(List<SelectItem> listaTipoServico) {
        this.listaTipoServico = listaTipoServico;
    }

    /**
     * Get: listaTipoServicoHash.
     * 
     * @return listaTipoServicoHash
     */
    Map<Integer, String> getListaTipoServicoHash() {
        return listaTipoServicoHash;
    }

    /**
     * Set lista tipo servico hash.
     * 
     * @param listaTipoServicoHash
     *            the lista tipo servico hash
     */
    void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
        this.listaTipoServicoHash = listaTipoServicoHash;
    }

    /**
     * Is desabilitar combo.
     * 
     * @return true, if is desabilitar combo
     */
    public boolean isDesabilitarCombo() {
        return desabilitarCombo;
    }

    /**
     * Set: desabilitarCombo.
     * 
     * @param desabilitarCombo
     *            the desabilitar combo
     */
    public void setDesabilitarCombo(boolean desabilitarCombo) {
        this.desabilitarCombo = desabilitarCombo;
    }

    /**
     * Is bto acionado.
     * 
     * @return true, if is bto acionado
     */
    public boolean isBtoAcionado() {
        return btoAcionado;
    }

    /**
     * Set: btoAcionado.
     * 
     * @param btoAcionado
     *            the bto acionado
     */
    public void setBtoAcionado(boolean btoAcionado) {
        this.btoAcionado = btoAcionado;
    }

    /**
     * Get: filtroDataAte.
     * 
     * @return filtroDataAte
     */
    public Date getFiltroDataAte() {
        return filtroDataAte;
    }

    /**
     * Set: filtroDataAte.
     * 
     * @param filtroDataAte
     *            the filtro data ate
     */
    public void setFiltroDataAte(Date filtroDataAte) {
        this.filtroDataAte = filtroDataAte;
    }

    /**
     * Get: filtroDataDe.
     * 
     * @return filtroDataDe
     */
    public Date getFiltroDataDe() {
        return filtroDataDe;
    }

    /**
     * Set: filtroDataDe.
     * 
     * @param filtroDataDe
     *            the filtro data de
     */
    public void setFiltroDataDe(Date filtroDataDe) {
        this.filtroDataDe = filtroDataDe;
    }

    /**
     * Is btn consultar hist.
     * 
     * @return true, if is btn consultar hist
     */
    public boolean isBtnConsultarHist() {
        return btnConsultarHist;
    }

    /**
     * Set: btnConsultarHist.
     * 
     * @param btnConsultarHist
     *            the btn consultar hist
     */
    public void setBtnConsultarHist(boolean btnConsultarHist) {
        this.btnConsultarHist = btnConsultarHist;
    }

    /**
     * Get: itemSelecionadoLista3.
     * 
     * @return itemSelecionadoLista3
     */
    public Integer getItemSelecionadoLista3() {
        return itemSelecionadoLista3;
    }

    /**
     * Set: itemSelecionadoLista3.
     * 
     * @param itemSelecionadoLista3
     *            the item selecionado lista3
     */
    public void setItemSelecionadoLista3(Integer itemSelecionadoLista3) {
        this.itemSelecionadoLista3 = itemSelecionadoLista3;
    }

    /**
     * Get: listaControleRadio3.
     * 
     * @return listaControleRadio3
     */
    public List<SelectItem> getListaControleRadio3() {
        return listaControleRadio3;
    }

    /**
     * Set: listaControleRadio3.
     * 
     * @param listaControleRadio3
     *            the lista controle radio3
     */
    public void setListaControleRadio3(List<SelectItem> listaControleRadio3) {
        this.listaControleRadio3 = listaControleRadio3;
    }

    /**
     * Get: listaGridPesquisa3.
     * 
     * @return listaGridPesquisa3
     */
    public List<ListarConManServicoSaidaDTO> getListaGridPesquisa3() {
        return listaGridPesquisa3;
    }

    /**
     * Set: listaGridPesquisa3.
     * 
     * @param listaGridPesquisa3
     *            the lista grid pesquisa3
     */
    public void setListaGridPesquisa3(
        List<ListarConManServicoSaidaDTO> listaGridPesquisa3) {
        this.listaGridPesquisa3 = listaGridPesquisa3;
    }

    /**
     * Set: percentualReajuste.
     * 
     * @param percentualReajuste
     *            the percentual reajuste
     */
    public void setPercentualReajuste(BigDecimal percentualReajuste) {
        this.percentualReajuste = percentualReajuste;
    }

    /**
     * Get: percentualReajuste.
     * 
     * @return percentualReajuste
     */
    public BigDecimal getPercentualReajuste() {
        return percentualReajuste;
    }

    /**
     * Get: bonificacaoTarifa.
     * 
     * @return bonificacaoTarifa
     */
    public BigDecimal getBonificacaoTarifa() {
        return bonificacaoTarifa;
    }

    /**
     * Set: bonificacaoTarifa.
     * 
     * @param bonificacaoTarifa
     *            the bonificacao tarifa
     */
    public void setBonificacaoTarifa(BigDecimal bonificacaoTarifa) {
        this.bonificacaoTarifa = bonificacaoTarifa;
    }

    /**
     * Get: percentualBonificacaoTarifaPadrao.
     * 
     * @return percentualBonificacaoTarifaPadrao
     */
    public BigDecimal getPercentualBonificacaoTarifaPadrao() {
        return percentualBonificacaoTarifaPadrao;
    }

    /**
     * Set: percentualBonificacaoTarifaPadrao.
     * 
     * @param percentualBonificacaoTarifaPadrao
     *            the percentual bonificacao tarifa padrao
     */
    public void setPercentualBonificacaoTarifaPadrao(
        BigDecimal percentualBonificacaoTarifaPadrao) {
        this.percentualBonificacaoTarifaPadrao = percentualBonificacaoTarifaPadrao;
    }

    /**
     * Get: percentualIndiceEconomicoReajusteTarifa.
     * 
     * @return percentualIndiceEconomicoReajusteTarifa
     */
    public BigDecimal getPercentualIndiceEconomicoReajusteTarifa() {
        return percentualIndiceEconomicoReajusteTarifa;
    }

    /**
     * Set: percentualIndiceEconomicoReajusteTarifa.
     * 
     * @param percentualIndiceEconomicoReajusteTarifa
     *            the percentual indice economico reajuste tarifa
     */
    public void setPercentualIndiceEconomicoReajusteTarifa(
        BigDecimal percentualIndiceEconomicoReajusteTarifa) {
        this.percentualIndiceEconomicoReajusteTarifa = percentualIndiceEconomicoReajusteTarifa;
    }

    /**
     * Get: valorMaxPagFavNaoCadastrado.
     * 
     * @return valorMaxPagFavNaoCadastrado
     */
    public BigDecimal getValorMaxPagFavNaoCadastrado() {
        return valorMaxPagFavNaoCadastrado;
    }

    /**
     * Set: valorMaxPagFavNaoCadastrado.
     * 
     * @param valorMaxPagFavNaoCadastrado
     *            the valor max pag fav nao cadastrado
     */
    public void setValorMaxPagFavNaoCadastrado(
        BigDecimal valorMaxPagFavNaoCadastrado) {
        this.valorMaxPagFavNaoCadastrado = valorMaxPagFavNaoCadastrado;
    }

    /**
     * Get: cdTipoModalidadeHistorico.
     * 
     * @return cdTipoModalidadeHistorico
     */
    public Integer getCdTipoModalidadeHistorico() {
        return cdTipoModalidadeHistorico;
    }

    /**
     * Set: cdTipoModalidadeHistorico.
     * 
     * @param cdTipoModalidadeHistorico
     *            the cd tipo modalidade historico
     */
    public void setCdTipoModalidadeHistorico(Integer cdTipoModalidadeHistorico) {
        this.cdTipoModalidadeHistorico = cdTipoModalidadeHistorico;
    }

    /**
     * Get: itemSelecionadoListaHistoricoModalidade.
     * 
     * @return itemSelecionadoListaHistoricoModalidade
     */
    public Integer getItemSelecionadoListaHistoricoModalidade() {
        return itemSelecionadoListaHistoricoModalidade;
    }

    /**
     * Set: itemSelecionadoListaHistoricoModalidade.
     * 
     * @param itemSelecionadoListaHistoricoModalidade
     *            the item selecionado lista historico modalidade
     */
    public void setItemSelecionadoListaHistoricoModalidade(
        Integer itemSelecionadoListaHistoricoModalidade) {
        this.itemSelecionadoListaHistoricoModalidade = itemSelecionadoListaHistoricoModalidade;
    }

    /**
     * Get: listaControleRadioHistoricoModalidade.
     * 
     * @return listaControleRadioHistoricoModalidade
     */
    public List<SelectItem> getListaControleRadioHistoricoModalidade() {
        return listaControleRadioHistoricoModalidade;
    }

    /**
     * Set: listaControleRadioHistoricoModalidade.
     * 
     * @param listaControleRadioHistoricoModalidade
     *            the lista controle radio historico modalidade
     */
    public void setListaControleRadioHistoricoModalidade(
        List<SelectItem> listaControleRadioHistoricoModalidade) {
        this.listaControleRadioHistoricoModalidade = listaControleRadioHistoricoModalidade;
    }

    /**
     * Get: listaGridPesquisaHistoricoModalidade.
     * 
     * @return listaGridPesquisaHistoricoModalidade
     */
    public List<ListarConManServicoSaidaDTO> getListaGridPesquisaHistoricoModalidade() {
        return listaGridPesquisaHistoricoModalidade;
    }

    /**
     * Set: listaGridPesquisaHistoricoModalidade.
     * 
     * @param listaGridPesquisaHistoricoModalidade
     *            the lista grid pesquisa historico modalidade
     */
    public void setListaGridPesquisaHistoricoModalidade(
        List<ListarConManServicoSaidaDTO> listaGridPesquisaHistoricoModalidade) {
        this.listaGridPesquisaHistoricoModalidade = listaGridPesquisaHistoricoModalidade;
    }

    /**
     * Get: listaModalidadeHistorico.
     * 
     * @return listaModalidadeHistorico
     */
    public List<SelectItem> getListaModalidadeHistorico() {
        return listaModalidadeHistorico;
    }

    /**
     * Set: listaModalidadeHistorico.
     * 
     * @param listaModalidadeHistorico
     *            the lista modalidade historico
     */
    public void setListaModalidadeHistorico(
        List<SelectItem> listaModalidadeHistorico) {
        this.listaModalidadeHistorico = listaModalidadeHistorico;
    }

    /**
     * Get: listaTipoLayoutArquivoHash.
     * 
     * @return listaTipoLayoutArquivoHash
     */
    Map<Integer, String> getListaTipoLayoutArquivoHash() {
        return listaTipoLayoutArquivoHash;
    }

    /**
     * Set lista tipo layout arquivo hash.
     * 
     * @param listaTipoLayoutArquivoHash
     *            the lista tipo layout arquivo hash
     */
    void setListaTipoLayoutArquivoHash(
        Map<Integer, String> listaTipoLayoutArquivoHash) {
        this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
    }

    /**
     * Get: valorLimiteDiarioFiltro.
     * 
     * @return valorLimiteDiarioFiltro
     */
    public BigDecimal getValorLimiteDiarioFiltro() {
        return valorLimiteDiarioFiltro;
    }

    /**
     * Set: valorLimiteDiarioFiltro.
     * 
     * @param valorLimiteDiarioFiltro
     *            the valor limite diario filtro
     */
    public void setValorLimiteDiarioFiltro(BigDecimal valorLimiteDiarioFiltro) {
        this.valorLimiteDiarioFiltro = valorLimiteDiarioFiltro;
    }

    /**
     * Get: valorMaxPagFavNaoCadastradoDesc.
     * 
     * @return valorMaxPagFavNaoCadastradoDesc
     */
    public String getValorMaxPagFavNaoCadastradoDesc() {
        return valorMaxPagFavNaoCadastradoDesc;
    }

    /**
     * Set: valorMaxPagFavNaoCadastradoDesc.
     * 
     * @param valorMaxPagFavNaoCadastradoDesc
     *            the valor max pag fav nao cadastrado desc
     */
    public void setValorMaxPagFavNaoCadastradoDesc(
        String valorMaxPagFavNaoCadastradoDesc) {
        this.valorMaxPagFavNaoCadastradoDesc = valorMaxPagFavNaoCadastradoDesc;
    }

    /**
     * Get: valorLimiteIndividualFiltro.
     * 
     * @return valorLimiteIndividualFiltro
     */
    public BigDecimal getValorLimiteIndividualFiltro() {
        return valorLimiteIndividualFiltro;
    }

    /**
     * Set: valorLimiteIndividualFiltro.
     * 
     * @param valorLimiteIndividualFiltro
     *            the valor limite individual filtro
     */
    public void setValorLimiteIndividualFiltro(
        BigDecimal valorLimiteIndividualFiltro) {
        this.valorLimiteIndividualFiltro = valorLimiteIndividualFiltro;
    }

    /**
     * Get: bonificacaoTarifaDesc.
     * 
     * @return bonificacaoTarifaDesc
     */
    public String getBonificacaoTarifaDesc() {
        if (getBonificacaoTarifa() != null) {
            return NumberUtils.format(getBonificacaoTarifa());
        }
        return "";
    }

    /**
     * Get: percentualReajusteDesc.
     * 
     * @return percentualReajusteDesc
     */
    public String getPercentualReajusteDesc() {
        if (getPercentualReajuste() != null) {
            return NumberUtils.format(getPercentualReajuste());
        }
        return "";
    }

    /**
     * Get: percentualBonificacaoTarifaPadraoDesc.
     * 
     * @return percentualBonificacaoTarifaPadraoDesc
     */
    public String getPercentualBonificacaoTarifaPadraoDesc() {
        if (getPercentualBonificacaoTarifaPadrao() != null) {
            return NumberUtils.format(getPercentualBonificacaoTarifaPadrao());
        }
        return "";
    }

    /**
     * Get: percentualIndiceEconomicoReajusteTarifaDesc.
     * 
     * @return percentualIndiceEconomicoReajusteTarifaDesc
     */
    public String getPercentualIndiceEconomicoReajusteTarifaDesc() {
        if (getPercentualIndiceEconomicoReajusteTarifa() != null) {
            return NumberUtils
            .format(getPercentualIndiceEconomicoReajusteTarifa());
        }
        return "";
    }

    /**
     * Get: dsIndiceReajuste.
     * 
     * @return dsIndiceReajuste
     */
    public String getDsIndiceReajuste() {
        return dsIndiceReajuste;
    }

    /**
     * Set: dsIndiceReajuste.
     * 
     * @param dsIndiceReajuste
     *            the ds indice reajuste
     */
    public void setDsIndiceReajuste(String dsIndiceReajuste) {
        this.dsIndiceReajuste = dsIndiceReajuste;
    }

    /**
     * Get: dsFormularioImpressao.
     * 
     * @return dsFormularioImpressao
     */
    public String getDsFormularioImpressao() {
        return dsFormularioImpressao;
    }

    /**
     * Set: dsFormularioImpressao.
     * 
     * @param dsFormularioImpressao
     *            the ds formulario impressao
     */
    public void setDsFormularioImpressao(String dsFormularioImpressao) {
        this.dsFormularioImpressao = dsFormularioImpressao;
    }

    /**
     * Get: listaTipoLayoutArquivo.
     * 
     * @return listaTipoLayoutArquivo
     */
    public List<SelectItem> getListaTipoLayoutArquivo() {
        return listaTipoLayoutArquivo;
    }

    /**
     * Set: listaTipoLayoutArquivo.
     * 
     * @param listaTipoLayoutArquivo
     *            the lista tipo layout arquivo
     */
    public void setListaTipoLayoutArquivo(
        List<SelectItem> listaTipoLayoutArquivo) {
        this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
    }

    /**
     * Set: cdFormularioImpressao.
     * 
     * @param cdFormularioImpressao
     *            the cd formulario impressao
     */
    public void setCdFormularioImpressao(Integer cdFormularioImpressao) {
        this.cdFormularioImpressao = cdFormularioImpressao;
    }

    /**
     * Is check all.
     * 
     * @return true, if is check all
     */
    public boolean isCheckAll() {
        return checkAll;
    }

    /**
     * Set: checkAll.
     * 
     * @param checkAll
     *            the check all
     */
    public void setCheckAll(boolean checkAll) {
        this.checkAll = checkAll;
    }

    /**
     * Get: listaAcaoNaoComprovacao.
     * 
     * @return listaAcaoNaoComprovacao
     */
    public List<SelectItem> getListaAcaoNaoComprovacao() {
        return listaAcaoNaoComprovacao;
    }

    /**
     * Set: listaAcaoNaoComprovacao.
     * 
     * @param listaAcaoNaoComprovacao
     *            the lista acao nao comprovacao
     */
    public void setListaAcaoNaoComprovacao(
        List<SelectItem> listaAcaoNaoComprovacao) {
        this.listaAcaoNaoComprovacao = listaAcaoNaoComprovacao;
    }

    /**
     * Get: listaAcaoNaoComprovacaoHash.
     * 
     * @return listaAcaoNaoComprovacaoHash
     */
    public Map<Long, String> getListaAcaoNaoComprovacaoHash() {
        return listaAcaoNaoComprovacaoHash;
    }

    /**
     * Set lista acao nao comprovacao hash.
     * 
     * @param listaAcaoNaoComprovacaoHash
     *            the lista acao nao comprovacao hash
     */
    public void setListaAcaoNaoComprovacaoHash(
        Map<Long, String> listaAcaoNaoComprovacaoHash) {
        this.listaAcaoNaoComprovacaoHash = listaAcaoNaoComprovacaoHash;
    }

    /**
     * Get: listaCadastroConsultaEnderecoEnvio.
     * 
     * @return listaCadastroConsultaEnderecoEnvio
     */
    public List<SelectItem> getListaCadastroConsultaEnderecoEnvio() {
        return listaCadastroConsultaEnderecoEnvio;
    }

    /**
     * Set: listaCadastroConsultaEnderecoEnvio.
     * 
     * @param listaCadastroConsultaEnderecoEnvio
     *            the lista cadastro consulta endereco envio
     */
    public void setListaCadastroConsultaEnderecoEnvio(
        List<SelectItem> listaCadastroConsultaEnderecoEnvio) {
        this.listaCadastroConsultaEnderecoEnvio = listaCadastroConsultaEnderecoEnvio;
    }

    /**
     * Get: listaCadastroConsultaEnderecoEnvioHash.
     * 
     * @return listaCadastroConsultaEnderecoEnvioHash
     */
    public Map<Long, String> getListaCadastroConsultaEnderecoEnvioHash() {
        return listaCadastroConsultaEnderecoEnvioHash;
    }

    /**
     * Set lista cadastro consulta endereco envio hash.
     * 
     * @param listaCadastroConsultaEnderecoEnvioHash
     *            the lista cadastro consulta endereco envio hash
     */
    public void setListaCadastroConsultaEnderecoEnvioHash(
        Map<Long, String> listaCadastroConsultaEnderecoEnvioHash) {
        this.listaCadastroConsultaEnderecoEnvioHash = listaCadastroConsultaEnderecoEnvioHash;
    }

    /**
     * Get: listaCadastroUtilizadoRecadastramento.
     * 
     * @return listaCadastroUtilizadoRecadastramento
     */
    public List<SelectItem> getListaCadastroUtilizadoRecadastramento() {
        return listaCadastroUtilizadoRecadastramento;
    }

    /**
     * Set: listaCadastroUtilizadoRecadastramento.
     * 
     * @param listaCadastroUtilizadoRecadastramento
     *            the lista cadastro utilizado recadastramento
     */
    public void setListaCadastroUtilizadoRecadastramento(
        List<SelectItem> listaCadastroUtilizadoRecadastramento) {
        this.listaCadastroUtilizadoRecadastramento = listaCadastroUtilizadoRecadastramento;
    }

    /**
     * Get: listaCadastroUtilizadoRecadastramentoHash.
     * 
     * @return listaCadastroUtilizadoRecadastramentoHash
     */
    public Map<Long, String> getListaCadastroUtilizadoRecadastramentoHash() {
        return listaCadastroUtilizadoRecadastramentoHash;
    }

    /**
     * Set lista cadastro utilizado recadastramento hash.
     * 
     * @param listaCadastroUtilizadoRecadastramentoHash
     *            the lista cadastro utilizado recadastramento hash
     */
    public void setListaCadastroUtilizadoRecadastramentoHash(
        Map<Long, String> listaCadastroUtilizadoRecadastramentoHash) {
        this.listaCadastroUtilizadoRecadastramentoHash = listaCadastroUtilizadoRecadastramentoHash;
    }

    /**
     * Get: listaCondicaoEnquadramento.
     * 
     * @return listaCondicaoEnquadramento
     */
    public List<SelectItem> getListaCondicaoEnquadramento() {
        return listaCondicaoEnquadramento;
    }

    /**
     * Set: listaCondicaoEnquadramento.
     * 
     * @param listaCondicaoEnquadramento
     *            the lista condicao enquadramento
     */
    public void setListaCondicaoEnquadramento(
        List<SelectItem> listaCondicaoEnquadramento) {
        this.listaCondicaoEnquadramento = listaCondicaoEnquadramento;
    }

    /**
     * Get: listaCondicaoEnquadramentoHash.
     * 
     * @return listaCondicaoEnquadramentoHash
     */
    public Map<Long, String> getListaCondicaoEnquadramentoHash() {
        return listaCondicaoEnquadramentoHash;
    }

    /**
     * Set lista condicao enquadramento hash.
     * 
     * @param listaCondicaoEnquadramentoHash
     *            the lista condicao enquadramento hash
     */
    public void setListaCondicaoEnquadramentoHash(
        Map<Long, String> listaCondicaoEnquadramentoHash) {
        this.listaCondicaoEnquadramentoHash = listaCondicaoEnquadramentoHash;
    }

    /**
     * Get: listaControleExpiracaoCredito.
     * 
     * @return listaControleExpiracaoCredito
     */
    public List<SelectItem> getListaControleExpiracaoCredito() {
        return listaControleExpiracaoCredito;
    }

    /**
     * Set: listaControleExpiracaoCredito.
     * 
     * @param listaControleExpiracaoCredito
     *            the lista controle expiracao credito
     */
    public void setListaControleExpiracaoCredito(
        List<SelectItem> listaControleExpiracaoCredito) {
        this.listaControleExpiracaoCredito = listaControleExpiracaoCredito;
    }

    /**
     * Get: listaControleExpiracaoCreditoHash.
     * 
     * @return listaControleExpiracaoCreditoHash
     */
    public Map<Long, String> getListaControleExpiracaoCreditoHash() {
        return listaControleExpiracaoCreditoHash;
    }

    /**
     * Set lista controle expiracao credito hash.
     * 
     * @param listaControleExpiracaoCreditoHash
     *            the lista controle expiracao credito hash
     */
    public void setListaControleExpiracaoCreditoHash(
        Map<Long, String> listaControleExpiracaoCreditoHash) {
        this.listaControleExpiracaoCreditoHash = listaControleExpiracaoCreditoHash;
    }

    /**
     * Get: listaCriterioCompostoEnquadramento.
     * 
     * @return listaCriterioCompostoEnquadramento
     */
    public List<SelectItem> getListaCriterioCompostoEnquadramento() {
        return listaCriterioCompostoEnquadramento;
    }

    /**
     * Set: listaCriterioCompostoEnquadramento.
     * 
     * @param listaCriterioCompostoEnquadramento
     *            the lista criterio composto enquadramento
     */
    public void setListaCriterioCompostoEnquadramento(
        List<SelectItem> listaCriterioCompostoEnquadramento) {
        this.listaCriterioCompostoEnquadramento = listaCriterioCompostoEnquadramento;
    }

    /**
     * Get: listaCriterioCompostoEnquadramentoHash.
     * 
     * @return listaCriterioCompostoEnquadramentoHash
     */
    public Map<Long, String> getListaCriterioCompostoEnquadramentoHash() {
        return listaCriterioCompostoEnquadramentoHash;
    }

    /**
     * Set lista criterio composto enquadramento hash.
     * 
     * @param listaCriterioCompostoEnquadramentoHash
     *            the lista criterio composto enquadramento hash
     */
    public void setListaCriterioCompostoEnquadramentoHash(
        Map<Long, String> listaCriterioCompostoEnquadramentoHash) {
        this.listaCriterioCompostoEnquadramentoHash = listaCriterioCompostoEnquadramentoHash;
    }

    /**
     * Get: listaCriterioPrincipalEnquadramento.
     * 
     * @return listaCriterioPrincipalEnquadramento
     */
    public List<SelectItem> getListaCriterioPrincipalEnquadramento() {
        return listaCriterioPrincipalEnquadramento;
    }

    /**
     * Set: listaCriterioPrincipalEnquadramento.
     * 
     * @param listaCriterioPrincipalEnquadramento
     *            the lista criterio principal enquadramento
     */
    public void setListaCriterioPrincipalEnquadramento(
        List<SelectItem> listaCriterioPrincipalEnquadramento) {
        this.listaCriterioPrincipalEnquadramento = listaCriterioPrincipalEnquadramento;
    }

    /**
     * Get: listaCriterioPrincipalEnquadramentoHash.
     * 
     * @return listaCriterioPrincipalEnquadramentoHash
     */
    public Map<Long, String> getListaCriterioPrincipalEnquadramentoHash() {
        return listaCriterioPrincipalEnquadramentoHash;
    }

    /**
     * Set lista criterio principal enquadramento hash.
     * 
     * @param listaCriterioPrincipalEnquadramentoHash
     *            the lista criterio principal enquadramento hash
     */
    public void setListaCriterioPrincipalEnquadramentoHash(
        Map<Long, String> listaCriterioPrincipalEnquadramentoHash) {
        this.listaCriterioPrincipalEnquadramentoHash = listaCriterioPrincipalEnquadramentoHash;
    }

    /**
     * Get: listaDestinoEnvio.
     * 
     * @return listaDestinoEnvio
     */
    public List<SelectItem> getListaDestinoEnvio() {
        return listaDestinoEnvio;
    }

    /**
     * Set: listaDestinoEnvio.
     * 
     * @param listaDestinoEnvio
     *            the lista destino envio
     */
    public void setListaDestinoEnvio(List<SelectItem> listaDestinoEnvio) {
        this.listaDestinoEnvio = listaDestinoEnvio;
    }

    /**
     * Get: listaDestinoEnvioCorrespondencia.
     * 
     * @return listaDestinoEnvioCorrespondencia
     */
    public List<SelectItem> getListaDestinoEnvioCorrespondencia() {
        return listaDestinoEnvioCorrespondencia;
    }

    /**
     * Set: listaDestinoEnvioCorrespondencia.
     * 
     * @param listaDestinoEnvioCorrespondencia
     *            the lista destino envio correspondencia
     */
    public void setListaDestinoEnvioCorrespondencia(
        List<SelectItem> listaDestinoEnvioCorrespondencia) {
        this.listaDestinoEnvioCorrespondencia = listaDestinoEnvioCorrespondencia;
    }

    /**
     * Get: listaDestinoEnvioCorrespondenciaHash.
     * 
     * @return listaDestinoEnvioCorrespondenciaHash
     */
    public Map<Long, String> getListaDestinoEnvioCorrespondenciaHash() {
        return listaDestinoEnvioCorrespondenciaHash;
    }

    /**
     * Set lista destino envio correspondencia hash.
     * 
     * @param listaDestinoEnvioCorrespondenciaHash
     *            the lista destino envio correspondencia hash
     */
    public void setListaDestinoEnvioCorrespondenciaHash(
        Map<Long, String> listaDestinoEnvioCorrespondenciaHash) {
        this.listaDestinoEnvioCorrespondenciaHash = listaDestinoEnvioCorrespondenciaHash;
    }

    /**
     * Get: listaDestinoEnvioHash.
     * 
     * @return listaDestinoEnvioHash
     */
    public Map<Long, String> getListaDestinoEnvioHash() {
        return listaDestinoEnvioHash;
    }

    /**
     * Set lista destino envio hash.
     * 
     * @param listaDestinoEnvioHash
     *            the lista destino envio hash
     */
    public void setListaDestinoEnvioHash(Map<Long, String> listaDestinoEnvioHash) {
        this.listaDestinoEnvioHash = listaDestinoEnvioHash;
    }

    /**
     * Get: listaFormaAutorizacaoPagamentos.
     * 
     * @return listaFormaAutorizacaoPagamentos
     */
    public List<SelectItem> getListaFormaAutorizacaoPagamentos() {
        return listaFormaAutorizacaoPagamentos;
    }

    /**
     * Set: listaFormaAutorizacaoPagamentos.
     * 
     * @param listaFormaAutorizacaoPagamentos
     *            the lista forma autorizacao pagamentos
     */
    public void setListaFormaAutorizacaoPagamentos(
        List<SelectItem> listaFormaAutorizacaoPagamentos) {
        this.listaFormaAutorizacaoPagamentos = listaFormaAutorizacaoPagamentos;
    }

    /**
     * Get: listaFormaAutorizacaoPagamentosHash.
     * 
     * @return listaFormaAutorizacaoPagamentosHash
     */
    public Map<Long, String> getListaFormaAutorizacaoPagamentosHash() {
        return listaFormaAutorizacaoPagamentosHash;
    }

    /**
     * Set lista forma autorizacao pagamentos hash.
     * 
     * @param listaFormaAutorizacaoPagamentosHash
     *            the lista forma autorizacao pagamentos hash
     */
    public void setListaFormaAutorizacaoPagamentosHash(
        Map<Long, String> listaFormaAutorizacaoPagamentosHash) {
        this.listaFormaAutorizacaoPagamentosHash = listaFormaAutorizacaoPagamentosHash;
    }

    /**
     * Get: listaFormaEnvioPagamento.
     * 
     * @return listaFormaEnvioPagamento
     */
    public List<SelectItem> getListaFormaEnvioPagamento() {
        return listaFormaEnvioPagamento;
    }

    /**
     * Set: listaFormaEnvioPagamento.
     * 
     * @param listaFormaEnvioPagamento
     *            the lista forma envio pagamento
     */
    public void setListaFormaEnvioPagamento(
        List<SelectItem> listaFormaEnvioPagamento) {
        this.listaFormaEnvioPagamento = listaFormaEnvioPagamento;
    }

    /**
     * Get: listaFormaEnvioPagamentoHash.
     * 
     * @return listaFormaEnvioPagamentoHash
     */
    public Map<Long, String> getListaFormaEnvioPagamentoHash() {
        return listaFormaEnvioPagamentoHash;
    }

    /**
     * Set lista forma envio pagamento hash.
     * 
     * @param listaFormaEnvioPagamentoHash
     *            the lista forma envio pagamento hash
     */
    public void setListaFormaEnvioPagamentoHash(
        Map<Long, String> listaFormaEnvioPagamentoHash) {
        this.listaFormaEnvioPagamentoHash = listaFormaEnvioPagamentoHash;
    }

    /**
     * Get: listaFormaManutencaoCadastroFavorecido.
     * 
     * @return listaFormaManutencaoCadastroFavorecido
     */
    public List<SelectItem> getListaFormaManutencaoCadastroFavorecido() {
        return listaFormaManutencaoCadastroFavorecido;
    }

    /**
     * Set: listaFormaManutencaoCadastroFavorecido.
     * 
     * @param listaFormaManutencaoCadastroFavorecido
     *            the lista forma manutencao cadastro favorecido
     */
    public void setListaFormaManutencaoCadastroFavorecido(
        List<SelectItem> listaFormaManutencaoCadastroFavorecido) {
        this.listaFormaManutencaoCadastroFavorecido = listaFormaManutencaoCadastroFavorecido;
    }

    /**
     * Get: listaFormaManutencaoCadastroFavorecidoHash.
     * 
     * @return listaFormaManutencaoCadastroFavorecidoHash
     */
    public Map<Long, String> getListaFormaManutencaoCadastroFavorecidoHash() {
        return listaFormaManutencaoCadastroFavorecidoHash;
    }

    /**
     * Set lista forma manutencao cadastro favorecido hash.
     * 
     * @param listaFormaManutencaoCadastroFavorecidoHash
     *            the lista forma manutencao cadastro favorecido hash
     */
    public void setListaFormaManutencaoCadastroFavorecidoHash(
        Map<Long, String> listaFormaManutencaoCadastroFavorecidoHash) {
        this.listaFormaManutencaoCadastroFavorecidoHash = listaFormaManutencaoCadastroFavorecidoHash;
    }

    /**
     * Get: listaFormaManutencaoCadastroProcuradores.
     * 
     * @return listaFormaManutencaoCadastroProcuradores
     */
    public List<SelectItem> getListaFormaManutencaoCadastroProcuradores() {
        return listaFormaManutencaoCadastroProcuradores;
    }

    /**
     * Set: listaFormaManutencaoCadastroProcuradores.
     * 
     * @param listaFormaManutencaoCadastroProcuradores
     *            the lista forma manutencao cadastro procuradores
     */
    public void setListaFormaManutencaoCadastroProcuradores(
        List<SelectItem> listaFormaManutencaoCadastroProcuradores) {
        this.listaFormaManutencaoCadastroProcuradores = listaFormaManutencaoCadastroProcuradores;
    }

    /**
     * Get: listaFormaManutencaoCadastroProcuradoresHash.
     * 
     * @return listaFormaManutencaoCadastroProcuradoresHash
     */
    public Map<Long, String> getListaFormaManutencaoCadastroProcuradoresHash() {
        return listaFormaManutencaoCadastroProcuradoresHash;
    }

    /**
     * Set lista forma manutencao cadastro procuradores hash.
     * 
     * @param listaFormaManutencaoCadastroProcuradoresHash
     *            the lista forma manutencao cadastro procuradores hash
     */
    public void setListaFormaManutencaoCadastroProcuradoresHash(
        Map<Long, String> listaFormaManutencaoCadastroProcuradoresHash) {
        this.listaFormaManutencaoCadastroProcuradoresHash = listaFormaManutencaoCadastroProcuradoresHash;
    }

    /**
     * Get: listaMeioDisponibilizacaoCorrentistas.
     * 
     * @return listaMeioDisponibilizacaoCorrentistas
     */
    public List<SelectItem> getListaMeioDisponibilizacaoCorrentistas() {
        return listaMeioDisponibilizacaoCorrentistas;
    }

    /**
     * Set: listaMeioDisponibilizacaoCorrentistas.
     * 
     * @param listaMeioDisponibilizacaoCorrentistas
     *            the lista meio disponibilizacao correntistas
     */
    public void setListaMeioDisponibilizacaoCorrentistas(
        List<SelectItem> listaMeioDisponibilizacaoCorrentistas) {
        this.listaMeioDisponibilizacaoCorrentistas = listaMeioDisponibilizacaoCorrentistas;
    }

    /**
     * Get: listaMeioDisponibilizacaoCorrentistasHash.
     * 
     * @return listaMeioDisponibilizacaoCorrentistasHash
     */
    public Map<Long, String> getListaMeioDisponibilizacaoCorrentistasHash() {
        return listaMeioDisponibilizacaoCorrentistasHash;
    }

    /**
     * Set lista meio disponibilizacao correntistas hash.
     * 
     * @param listaMeioDisponibilizacaoCorrentistasHash
     *            the lista meio disponibilizacao correntistas hash
     */
    public void setListaMeioDisponibilizacaoCorrentistasHash(
        Map<Long, String> listaMeioDisponibilizacaoCorrentistasHash) {
        this.listaMeioDisponibilizacaoCorrentistasHash = listaMeioDisponibilizacaoCorrentistasHash;
    }

    /**
     * Get: listaMeioDisponibilizacaoNaoCorrentistas.
     * 
     * @return listaMeioDisponibilizacaoNaoCorrentistas
     */
    public List<SelectItem> getListaMeioDisponibilizacaoNaoCorrentistas() {
        return listaMeioDisponibilizacaoNaoCorrentistas;
    }

    /**
     * Set: listaMeioDisponibilizacaoNaoCorrentistas.
     * 
     * @param listaMeioDisponibilizacaoNaoCorrentistas
     *            the lista meio disponibilizacao nao correntistas
     */
    public void setListaMeioDisponibilizacaoNaoCorrentistas(
        List<SelectItem> listaMeioDisponibilizacaoNaoCorrentistas) {
        this.listaMeioDisponibilizacaoNaoCorrentistas = listaMeioDisponibilizacaoNaoCorrentistas;
    }

    /**
     * Get: listaMeioDisponibilizacaoNaoCorrentistasHash.
     * 
     * @return listaMeioDisponibilizacaoNaoCorrentistasHash
     */
    public Map<Long, String> getListaMeioDisponibilizacaoNaoCorrentistasHash() {
        return listaMeioDisponibilizacaoNaoCorrentistasHash;
    }

    /**
     * Set lista meio disponibilizacao nao correntistas hash.
     * 
     * @param listaMeioDisponibilizacaoNaoCorrentistasHash
     *            the lista meio disponibilizacao nao correntistas hash
     */
    public void setListaMeioDisponibilizacaoNaoCorrentistasHash(
        Map<Long, String> listaMeioDisponibilizacaoNaoCorrentistasHash) {
        this.listaMeioDisponibilizacaoNaoCorrentistasHash = listaMeioDisponibilizacaoNaoCorrentistasHash;
    }

    /**
     * Get: listaMidiaDisponibilizacaoCorrentistas.
     * 
     * @return listaMidiaDisponibilizacaoCorrentistas
     */
    public List<SelectItem> getListaMidiaDisponibilizacaoCorrentistas() {
        return listaMidiaDisponibilizacaoCorrentistas;
    }

    /**
     * Set: listaMidiaDisponibilizacaoCorrentistas.
     * 
     * @param listaMidiaDisponibilizacaoCorrentistas
     *            the lista midia disponibilizacao correntistas
     */
    public void setListaMidiaDisponibilizacaoCorrentistas(
        List<SelectItem> listaMidiaDisponibilizacaoCorrentistas) {
        this.listaMidiaDisponibilizacaoCorrentistas = listaMidiaDisponibilizacaoCorrentistas;
    }

    /**
     * Get: listaMidiaDisponibilizacaoCorrentistasHash.
     * 
     * @return listaMidiaDisponibilizacaoCorrentistasHash
     */
    public Map<Long, String> getListaMidiaDisponibilizacaoCorrentistasHash() {
        return listaMidiaDisponibilizacaoCorrentistasHash;
    }

    /**
     * Set lista midia disponibilizacao correntistas hash.
     * 
     * @param listaMidiaDisponibilizacaoCorrentistasHash
     *            the lista midia disponibilizacao correntistas hash
     */
    public void setListaMidiaDisponibilizacaoCorrentistasHash(
        Map<Long, String> listaMidiaDisponibilizacaoCorrentistasHash) {
        this.listaMidiaDisponibilizacaoCorrentistasHash = listaMidiaDisponibilizacaoCorrentistasHash;
    }

    /**
     * Get: listaMidiaMensagemRecadastramento.
     * 
     * @return listaMidiaMensagemRecadastramento
     */
    public List<SelectItem> getListaMidiaMensagemRecadastramento() {
        return listaMidiaMensagemRecadastramento;
    }

    /**
     * Set: listaMidiaMensagemRecadastramento.
     * 
     * @param listaMidiaMensagemRecadastramento
     *            the lista midia mensagem recadastramento
     */
    public void setListaMidiaMensagemRecadastramento(
        List<SelectItem> listaMidiaMensagemRecadastramento) {
        this.listaMidiaMensagemRecadastramento = listaMidiaMensagemRecadastramento;
    }

    /**
     * Get: listaMidiaMensagemRecadastramentoHash.
     * 
     * @return listaMidiaMensagemRecadastramentoHash
     */
    public Map<Long, String> getListaMidiaMensagemRecadastramentoHash() {
        return listaMidiaMensagemRecadastramentoHash;
    }

    /**
     * Set lista midia mensagem recadastramento hash.
     * 
     * @param listaMidiaMensagemRecadastramentoHash
     *            the lista midia mensagem recadastramento hash
     */
    public void setListaMidiaMensagemRecadastramentoHash(
        Map<Long, String> listaMidiaMensagemRecadastramentoHash) {
        this.listaMidiaMensagemRecadastramentoHash = listaMidiaMensagemRecadastramentoHash;
    }

    /**
     * Get: listaMomentoEnvio.
     * 
     * @return listaMomentoEnvio
     */
    public List<SelectItem> getListaMomentoEnvio() {
        return listaMomentoEnvio;
    }

    /**
     * Set: listaMomentoEnvio.
     * 
     * @param listaMomentoEnvio
     *            the lista momento envio
     */
    public void setListaMomentoEnvio(List<SelectItem> listaMomentoEnvio) {
        this.listaMomentoEnvio = listaMomentoEnvio;
    }

    /**
     * Get: listaMomentoEnvioHash.
     * 
     * @return listaMomentoEnvioHash
     */
    public Map<Long, String> getListaMomentoEnvioHash() {
        return listaMomentoEnvioHash;
    }

    /**
     * Set lista momento envio hash.
     * 
     * @param listaMomentoEnvioHash
     *            the lista momento envio hash
     */
    public void setListaMomentoEnvioHash(Map<Long, String> listaMomentoEnvioHash) {
        this.listaMomentoEnvioHash = listaMomentoEnvioHash;
    }

    /**
     * Get: listaMomentoIndicacaoCreditoEfetivado.
     * 
     * @return listaMomentoIndicacaoCreditoEfetivado
     */
    public List<SelectItem> getListaMomentoIndicacaoCreditoEfetivado() {
        return listaMomentoIndicacaoCreditoEfetivado;
    }

    /**
     * Set: listaMomentoIndicacaoCreditoEfetivado.
     * 
     * @param listaMomentoIndicacaoCreditoEfetivado
     *            the lista momento indicacao credito efetivado
     */
    public void setListaMomentoIndicacaoCreditoEfetivado(
        List<SelectItem> listaMomentoIndicacaoCreditoEfetivado) {
        this.listaMomentoIndicacaoCreditoEfetivado = listaMomentoIndicacaoCreditoEfetivado;
    }

    /**
     * Get: listaMomentoIndicacaoCreditoEfetivadoHash.
     * 
     * @return listaMomentoIndicacaoCreditoEfetivadoHash
     */
    public Map<Long, String> getListaMomentoIndicacaoCreditoEfetivadoHash() {
        return listaMomentoIndicacaoCreditoEfetivadoHash;
    }

    /**
     * Set lista momento indicacao credito efetivado hash.
     * 
     * @param listaMomentoIndicacaoCreditoEfetivadoHash
     *            the lista momento indicacao credito efetivado hash
     */
    public void setListaMomentoIndicacaoCreditoEfetivadoHash(
        Map<Long, String> listaMomentoIndicacaoCreditoEfetivadoHash) {
        this.listaMomentoIndicacaoCreditoEfetivadoHash = listaMomentoIndicacaoCreditoEfetivadoHash;
    }

    /**
     * Get: listaOcorrenciaDebito.
     * 
     * @return listaOcorrenciaDebito
     */
    public List<SelectItem> getListaOcorrenciaDebito() {
        return listaOcorrenciaDebito;
    }

    /**
     * Set: listaOcorrenciaDebito.
     * 
     * @param listaOcorrenciaDebito
     *            the lista ocorrencia debito
     */
    public void setListaOcorrenciaDebito(List<SelectItem> listaOcorrenciaDebito) {
        this.listaOcorrenciaDebito = listaOcorrenciaDebito;
    }

    /**
     * Get: listaOcorrenciaDebitoHash.
     * 
     * @return listaOcorrenciaDebitoHash
     */
    public Map<Long, String> getListaOcorrenciaDebitoHash() {
        return listaOcorrenciaDebitoHash;
    }

    /**
     * Set lista ocorrencia debito hash.
     * 
     * @param listaOcorrenciaDebitoHash
     *            the lista ocorrencia debito hash
     */
    public void setListaOcorrenciaDebitoHash(
        Map<Long, String> listaOcorrenciaDebitoHash) {
        this.listaOcorrenciaDebitoHash = listaOcorrenciaDebitoHash;
    }

    /**
     * Get: listaPermiteEstornoPagamento.
     * 
     * @return listaPermiteEstornoPagamento
     */
    public List<SelectItem> getListaPermiteEstornoPagamento() {
        return listaPermiteEstornoPagamento;
    }

    /**
     * Set: listaPermiteEstornoPagamento.
     * 
     * @param listaPermiteEstornoPagamento
     *            the lista permite estorno pagamento
     */
    public void setListaPermiteEstornoPagamento(
        List<SelectItem> listaPermiteEstornoPagamento) {
        this.listaPermiteEstornoPagamento = listaPermiteEstornoPagamento;
    }

    /**
     * Get: listaPermiteEstornoPagamentoHash.
     * 
     * @return listaPermiteEstornoPagamentoHash
     */
    public Map<Long, String> getListaPermiteEstornoPagamentoHash() {
        return listaPermiteEstornoPagamentoHash;
    }

    /**
     * Set lista permite estorno pagamento hash.
     * 
     * @param listaPermiteEstornoPagamentoHash
     *            the lista permite estorno pagamento hash
     */
    public void setListaPermiteEstornoPagamentoHash(
        Map<Long, String> listaPermiteEstornoPagamentoHash) {
        this.listaPermiteEstornoPagamentoHash = listaPermiteEstornoPagamentoHash;
    }

    /**
     * Get: listaPrioridadeDebito.
     * 
     * @return listaPrioridadeDebito
     */
    public List<SelectItem> getListaPrioridadeDebito() {
        return listaPrioridadeDebito;
    }

    /**
     * Set: listaPrioridadeDebito.
     * 
     * @param listaPrioridadeDebito
     *            the lista prioridade debito
     */
    public void setListaPrioridadeDebito(List<SelectItem> listaPrioridadeDebito) {
        this.listaPrioridadeDebito = listaPrioridadeDebito;
    }

    /**
     * Get: listaPrioridadeDebitoHash.
     * 
     * @return listaPrioridadeDebitoHash
     */
    public Map<Long, String> getListaPrioridadeDebitoHash() {
        return listaPrioridadeDebitoHash;
    }

    /**
     * Set lista prioridade debito hash.
     * 
     * @param listaPrioridadeDebitoHash
     *            the lista prioridade debito hash
     */
    public void setListaPrioridadeDebitoHash(
        Map<Long, String> listaPrioridadeDebitoHash) {
        this.listaPrioridadeDebitoHash = listaPrioridadeDebitoHash;
    }

    /**
     * Get: listaProcessamentoEfetivacaoPagamento.
     * 
     * @return listaProcessamentoEfetivacaoPagamento
     */
    public List<SelectItem> getListaProcessamentoEfetivacaoPagamento() {
        return listaProcessamentoEfetivacaoPagamento;
    }

    /**
     * Set: listaProcessamentoEfetivacaoPagamento.
     * 
     * @param listaProcessamentoEfetivacaoPagamento
     *            the lista processamento efetivacao pagamento
     */
    public void setListaProcessamentoEfetivacaoPagamento(
        List<SelectItem> listaProcessamentoEfetivacaoPagamento) {
        this.listaProcessamentoEfetivacaoPagamento = listaProcessamentoEfetivacaoPagamento;
    }

    /**
     * Get: listaProcessamentoEfetivacaoPagamentoHash.
     * 
     * @return listaProcessamentoEfetivacaoPagamentoHash
     */
    public Map<Long, String> getListaProcessamentoEfetivacaoPagamentoHash() {
        return listaProcessamentoEfetivacaoPagamentoHash;
    }

    /**
     * Set lista processamento efetivacao pagamento hash.
     * 
     * @param listaProcessamentoEfetivacaoPagamentoHash
     *            the lista processamento efetivacao pagamento hash
     */
    public void setListaProcessamentoEfetivacaoPagamentoHash(
        Map<Long, String> listaProcessamentoEfetivacaoPagamentoHash) {
        this.listaProcessamentoEfetivacaoPagamentoHash = listaProcessamentoEfetivacaoPagamentoHash;
    }

    /**
     * Get: listaTipoCargaCadastroBeneficiarios.
     * 
     * @return listaTipoCargaCadastroBeneficiarios
     */
    public List<SelectItem> getListaTipoCargaCadastroBeneficiarios() {
        return listaTipoCargaCadastroBeneficiarios;
    }

    /**
     * Set: listaTipoCargaCadastroBeneficiarios.
     * 
     * @param listaTipoCargaCadastroBeneficiarios
     *            the lista tipo carga cadastro beneficiarios
     */
    public void setListaTipoCargaCadastroBeneficiarios(
        List<SelectItem> listaTipoCargaCadastroBeneficiarios) {
        this.listaTipoCargaCadastroBeneficiarios = listaTipoCargaCadastroBeneficiarios;
    }

    /**
     * Get: listaTipoCargaCadastroBeneficiariosHash.
     * 
     * @return listaTipoCargaCadastroBeneficiariosHash
     */
    public Map<Long, String> getListaTipoCargaCadastroBeneficiariosHash() {
        return listaTipoCargaCadastroBeneficiariosHash;
    }

    /**
     * Set lista tipo carga cadastro beneficiarios hash.
     * 
     * @param listaTipoCargaCadastroBeneficiariosHash
     *            the lista tipo carga cadastro beneficiarios hash
     */
    public void setListaTipoCargaCadastroBeneficiariosHash(
        Map<Long, String> listaTipoCargaCadastroBeneficiariosHash) {
        this.listaTipoCargaCadastroBeneficiariosHash = listaTipoCargaCadastroBeneficiariosHash;
    }

    /**
     * Get: listaTipoCartaoContaSalario.
     * 
     * @return listaTipoCartaoContaSalario
     */
    public List<SelectItem> getListaTipoCartaoContaSalario() {
        return listaTipoCartaoContaSalario;
    }

    /**
     * Set: listaTipoCartaoContaSalario.
     * 
     * @param listaTipoCartaoContaSalario
     *            the lista tipo cartao conta salario
     */
    public void setListaTipoCartaoContaSalario(
        List<SelectItem> listaTipoCartaoContaSalario) {
        this.listaTipoCartaoContaSalario = listaTipoCartaoContaSalario;
    }

    /**
     * Get: listaTipoCartaoContaSalarioHash.
     * 
     * @return listaTipoCartaoContaSalarioHash
     */
    public Map<Long, String> getListaTipoCartaoContaSalarioHash() {
        return listaTipoCartaoContaSalarioHash;
    }

    /**
     * Set lista tipo cartao conta salario hash.
     * 
     * @param listaTipoCartaoContaSalarioHash
     *            the lista tipo cartao conta salario hash
     */
    public void setListaTipoCartaoContaSalarioHash(
        Map<Long, String> listaTipoCartaoContaSalarioHash) {
        this.listaTipoCartaoContaSalarioHash = listaTipoCartaoContaSalarioHash;
    }

    /**
     * Get: listaTipoConsistenciaCpfCnpjFavorecido.
     * 
     * @return listaTipoConsistenciaCpfCnpjFavorecido
     */
    public List<SelectItem> getListaTipoConsistenciaCpfCnpjFavorecido() {
        return listaTipoConsistenciaCpfCnpjFavorecido;
    }

    /**
     * Set: listaTipoConsistenciaCpfCnpjFavorecido.
     * 
     * @param listaTipoConsistenciaCpfCnpjFavorecido
     *            the lista tipo consistencia cpf cnpj favorecido
     */
    public void setListaTipoConsistenciaCpfCnpjFavorecido(
        List<SelectItem> listaTipoConsistenciaCpfCnpjFavorecido) {
        this.listaTipoConsistenciaCpfCnpjFavorecido = listaTipoConsistenciaCpfCnpjFavorecido;
    }

    /**
     * Get: listaTipoConsistenciaCpfCnpjFavorecidoHash.
     * 
     * @return listaTipoConsistenciaCpfCnpjFavorecidoHash
     */
    public Map<Long, String> getListaTipoConsistenciaCpfCnpjFavorecidoHash() {
        return listaTipoConsistenciaCpfCnpjFavorecidoHash;
    }

    /**
     * Set lista tipo consistencia cpf cnpj favorecido hash.
     * 
     * @param listaTipoConsistenciaCpfCnpjFavorecidoHash
     *            the lista tipo consistencia cpf cnpj favorecido hash
     */
    public void setListaTipoConsistenciaCpfCnpjFavorecidoHash(
        Map<Long, String> listaTipoConsistenciaCpfCnpjFavorecidoHash) {
        this.listaTipoConsistenciaCpfCnpjFavorecidoHash = listaTipoConsistenciaCpfCnpjFavorecidoHash;
    }

    /**
     * Get: listaTipoConsistenciaCpfCnpjProprietario.
     * 
     * @return listaTipoConsistenciaCpfCnpjProprietario
     */
    public List<SelectItem> getListaTipoConsistenciaCpfCnpjProprietario() {
        return listaTipoConsistenciaCpfCnpjProprietario;
    }

    /**
     * Set: listaTipoConsistenciaCpfCnpjProprietario.
     * 
     * @param listaTipoConsistenciaCpfCnpjProprietario
     *            the lista tipo consistencia cpf cnpj proprietario
     */
    public void setListaTipoConsistenciaCpfCnpjProprietario(
        List<SelectItem> listaTipoConsistenciaCpfCnpjProprietario) {
        this.listaTipoConsistenciaCpfCnpjProprietario = listaTipoConsistenciaCpfCnpjProprietario;
    }

    /**
     * Get: listaTipoConsistenciaCpfCnpjProprietarioHash.
     * 
     * @return listaTipoConsistenciaCpfCnpjProprietarioHash
     */
    public Map<Long, String> getListaTipoConsistenciaCpfCnpjProprietarioHash() {
        return listaTipoConsistenciaCpfCnpjProprietarioHash;
    }

    /**
     * Set lista tipo consistencia cpf cnpj proprietario hash.
     * 
     * @param listaTipoConsistenciaCpfCnpjProprietarioHash
     *            the lista tipo consistencia cpf cnpj proprietario hash
     */
    public void setListaTipoConsistenciaCpfCnpjProprietarioHash(
        Map<Long, String> listaTipoConsistenciaCpfCnpjProprietarioHash) {
        this.listaTipoConsistenciaCpfCnpjProprietarioHash = listaTipoConsistenciaCpfCnpjProprietarioHash;
    }

    /**
     * Get: listaTipoConsistenciaIdentificacaoBeneficiario.
     * 
     * @return listaTipoConsistenciaIdentificacaoBeneficiario
     */
    public List<SelectItem> getListaTipoConsistenciaIdentificacaoBeneficiario() {
        return listaTipoConsistenciaIdentificacaoBeneficiario;
    }

    /**
     * Set: listaTipoConsistenciaIdentificacaoBeneficiario.
     * 
     * @param listaTipoConsistenciaIdentificacaoBeneficiario
     *            the lista tipo consistencia identificacao beneficiario
     */
    public void setListaTipoConsistenciaIdentificacaoBeneficiario(
        List<SelectItem> listaTipoConsistenciaIdentificacaoBeneficiario) {
        this.listaTipoConsistenciaIdentificacaoBeneficiario = listaTipoConsistenciaIdentificacaoBeneficiario;
    }

    /**
     * Get: listaTipoConsistenciaIdentificacaoBeneficiarioHash.
     * 
     * @return listaTipoConsistenciaIdentificacaoBeneficiarioHash
     */
    public Map<Long, String> getListaTipoConsistenciaIdentificacaoBeneficiarioHash() {
        return listaTipoConsistenciaIdentificacaoBeneficiarioHash;
    }

    /**
     * Set lista tipo consistencia identificacao beneficiario hash.
     * 
     * @param listaTipoConsistenciaIdentificacaoBeneficiarioHash
     *            the lista tipo consistencia identificacao beneficiario hash
     */
    public void setListaTipoConsistenciaIdentificacaoBeneficiarioHash(
        Map<Long, String> listaTipoConsistenciaIdentificacaoBeneficiarioHash) {
        this.listaTipoConsistenciaIdentificacaoBeneficiarioHash = listaTipoConsistenciaIdentificacaoBeneficiarioHash;
    }

    /**
     * Get: listaTipoConsistenciaInscricaoFavorecido.
     * 
     * @return listaTipoConsistenciaInscricaoFavorecido
     */
    public List<SelectItem> getListaTipoConsistenciaInscricaoFavorecido() {
        return listaTipoConsistenciaInscricaoFavorecido;
    }

    /**
     * Set: listaTipoConsistenciaInscricaoFavorecido.
     * 
     * @param listaTipoConsistenciaInscricaoFavorecido
     *            the lista tipo consistencia inscricao favorecido
     */
    public void setListaTipoConsistenciaInscricaoFavorecido(
        List<SelectItem> listaTipoConsistenciaInscricaoFavorecido) {
        this.listaTipoConsistenciaInscricaoFavorecido = listaTipoConsistenciaInscricaoFavorecido;
    }

    /**
     * Get: listaTipoConsistenciaInscricaoFavorecidoHash.
     * 
     * @return listaTipoConsistenciaInscricaoFavorecidoHash
     */
    public Map<Long, String> getListaTipoConsistenciaInscricaoFavorecidoHash() {
        return listaTipoConsistenciaInscricaoFavorecidoHash;
    }

    /**
     * Set lista tipo consistencia inscricao favorecido hash.
     * 
     * @param listaTipoConsistenciaInscricaoFavorecidoHash
     *            the lista tipo consistencia inscricao favorecido hash
     */
    public void setListaTipoConsistenciaInscricaoFavorecidoHash(
        Map<Long, String> listaTipoConsistenciaInscricaoFavorecidoHash) {
        this.listaTipoConsistenciaInscricaoFavorecidoHash = listaTipoConsistenciaInscricaoFavorecidoHash;
    }

    /**
     * Get: listaTipoConsolidacaoPagamentosComprovante.
     * 
     * @return listaTipoConsolidacaoPagamentosComprovante
     */
    public List<SelectItem> getListaTipoConsolidacaoPagamentosComprovante() {
        return listaTipoConsolidacaoPagamentosComprovante;
    }

    /**
     * Set: listaTipoConsolidacaoPagamentosComprovante.
     * 
     * @param listaTipoConsolidacaoPagamentosComprovante
     *            the lista tipo consolidacao pagamentos comprovante
     */
    public void setListaTipoConsolidacaoPagamentosComprovante(
        List<SelectItem> listaTipoConsolidacaoPagamentosComprovante) {
        this.listaTipoConsolidacaoPagamentosComprovante = listaTipoConsolidacaoPagamentosComprovante;
    }

    /**
     * Get: listaTipoConsolidacaoPagamentosComprovanteHash.
     * 
     * @return listaTipoConsolidacaoPagamentosComprovanteHash
     */
    public Map<Long, String> getListaTipoConsolidacaoPagamentosComprovanteHash() {
        return listaTipoConsolidacaoPagamentosComprovanteHash;
    }

    /**
     * Set lista tipo consolidacao pagamentos comprovante hash.
     * 
     * @param listaTipoConsolidacaoPagamentosComprovanteHash
     *            the lista tipo consolidacao pagamentos comprovante hash
     */
    public void setListaTipoConsolidacaoPagamentosComprovanteHash(
        Map<Long, String> listaTipoConsolidacaoPagamentosComprovanteHash) {
        this.listaTipoConsolidacaoPagamentosComprovanteHash = listaTipoConsolidacaoPagamentosComprovanteHash;
    }

    /**
     * Get: listaTipoConsultaSaldo.
     * 
     * @return listaTipoConsultaSaldo
     */
    public List<SelectItem> getListaTipoConsultaSaldo() {
        return listaTipoConsultaSaldo;
    }

    /**
     * Set: listaTipoConsultaSaldo.
     * 
     * @param listaTipoConsultaSaldo
     *            the lista tipo consulta saldo
     */
    public void setListaTipoConsultaSaldo(
        List<SelectItem> listaTipoConsultaSaldo) {
        this.listaTipoConsultaSaldo = listaTipoConsultaSaldo;
    }

    /**
     * Get: listaTipoConsultaSaldoHash.
     * 
     * @return listaTipoConsultaSaldoHash
     */
    public Map<Long, String> getListaTipoConsultaSaldoHash() {
        return listaTipoConsultaSaldoHash;
    }

    /**
     * Set lista tipo consulta saldo hash.
     * 
     * @param listaTipoConsultaSaldoHash
     *            the lista tipo consulta saldo hash
     */
    public void setListaTipoConsultaSaldoHash(
        Map<Long, String> listaTipoConsultaSaldoHash) {
        this.listaTipoConsultaSaldoHash = listaTipoConsultaSaldoHash;
    }

    /**
     * Get: listaTipoDataControleFloating.
     * 
     * @return listaTipoDataControleFloating
     */
    public List<SelectItem> getListaTipoDataControleFloating() {
        return listaTipoDataControleFloating;
    }

    /**
     * Set: listaTipoDataControleFloating.
     * 
     * @param listaTipoDataControleFloating
     *            the lista tipo data controle floating
     */
    public void setListaTipoDataControleFloating(
        List<SelectItem> listaTipoDataControleFloating) {
        this.listaTipoDataControleFloating = listaTipoDataControleFloating;
    }

    /**
     * Get: listaTipoDataControleFloatingHash.
     * 
     * @return listaTipoDataControleFloatingHash
     */
    public Map<Long, String> getListaTipoDataControleFloatingHash() {
        return listaTipoDataControleFloatingHash;
    }

    /**
     * Set lista tipo data controle floating hash.
     * 
     * @param listaTipoDataControleFloatingHash
     *            the lista tipo data controle floating hash
     */
    public void setListaTipoDataControleFloatingHash(
        Map<Long, String> listaTipoDataControleFloatingHash) {
        this.listaTipoDataControleFloatingHash = listaTipoDataControleFloatingHash;
    }

    /**
     * Get: listaTipoFormacaoListaDebito.
     * 
     * @return listaTipoFormacaoListaDebito
     */
    public List<SelectItem> getListaTipoFormacaoListaDebito() {
        return listaTipoFormacaoListaDebito;
    }

    /**
     * Set: listaTipoFormacaoListaDebito.
     * 
     * @param listaTipoFormacaoListaDebito
     *            the lista tipo formacao lista debito
     */
    public void setListaTipoFormacaoListaDebito(
        List<SelectItem> listaTipoFormacaoListaDebito) {
        this.listaTipoFormacaoListaDebito = listaTipoFormacaoListaDebito;
    }

    /**
     * Get: listaTipoFormacaoListaDebitoHash.
     * 
     * @return listaTipoFormacaoListaDebitoHash
     */
    public Map<Long, String> getListaTipoFormacaoListaDebitoHash() {
        return listaTipoFormacaoListaDebitoHash;
    }

    /**
     * Set lista tipo formacao lista debito hash.
     * 
     * @param listaTipoFormacaoListaDebitoHash
     *            the lista tipo formacao lista debito hash
     */
    public void setListaTipoFormacaoListaDebitoHash(
        Map<Long, String> listaTipoFormacaoListaDebitoHash) {
        this.listaTipoFormacaoListaDebitoHash = listaTipoFormacaoListaDebitoHash;
    }

    /**
     * Get: listaTipoIdentificacaoBeneficiario.
     * 
     * @return listaTipoIdentificacaoBeneficiario
     */
    public List<SelectItem> getListaTipoIdentificacaoBeneficiario() {
        return listaTipoIdentificacaoBeneficiario;
    }

    /**
     * Set: listaTipoIdentificacaoBeneficiario.
     * 
     * @param listaTipoIdentificacaoBeneficiario
     *            the lista tipo identificacao beneficiario
     */
    public void setListaTipoIdentificacaoBeneficiario(
        List<SelectItem> listaTipoIdentificacaoBeneficiario) {
        this.listaTipoIdentificacaoBeneficiario = listaTipoIdentificacaoBeneficiario;
    }

    /**
     * Get: listaTipoIdentificacaoBeneficiarioHash.
     * 
     * @return listaTipoIdentificacaoBeneficiarioHash
     */
    public Map<Long, String> getListaTipoIdentificacaoBeneficiarioHash() {
        return listaTipoIdentificacaoBeneficiarioHash;
    }

    /**
     * Set lista tipo identificacao beneficiario hash.
     * 
     * @param listaTipoIdentificacaoBeneficiarioHash
     *            the lista tipo identificacao beneficiario hash
     */
    public void setListaTipoIdentificacaoBeneficiarioHash(
        Map<Long, String> listaTipoIdentificacaoBeneficiarioHash) {
        this.listaTipoIdentificacaoBeneficiarioHash = listaTipoIdentificacaoBeneficiarioHash;
    }

    /**
     * Get: listaTipoInscricaoFavorecido.
     * 
     * @return listaTipoInscricaoFavorecido
     */
    public List<SelectItem> getListaTipoInscricaoFavorecido() {
        return listaTipoInscricaoFavorecido;
    }

    /**
     * Set: listaTipoInscricaoFavorecido.
     * 
     * @param listaTipoInscricaoFavorecido
     *            the lista tipo inscricao favorecido
     */
    public void setListaTipoInscricaoFavorecido(
        List<SelectItem> listaTipoInscricaoFavorecido) {
        this.listaTipoInscricaoFavorecido = listaTipoInscricaoFavorecido;
    }

    /**
     * Get: listaTipoInscricaoFavorecidoHash.
     * 
     * @return listaTipoInscricaoFavorecidoHash
     */
    public Map<Long, String> getListaTipoInscricaoFavorecidoHash() {
        return listaTipoInscricaoFavorecidoHash;
    }

    /**
     * Set lista tipo inscricao favorecido hash.
     * 
     * @param listaTipoInscricaoFavorecidoHash
     *            the lista tipo inscricao favorecido hash
     */
    public void setListaTipoInscricaoFavorecidoHash(
        Map<Long, String> listaTipoInscricaoFavorecidoHash) {
        this.listaTipoInscricaoFavorecidoHash = listaTipoInscricaoFavorecidoHash;
    }

    /**
     * Get: listaTipoRastreamentoTitulos.
     * 
     * @return listaTipoRastreamentoTitulos
     */
    public List<SelectItem> getListaTipoRastreamentoTitulos() {
        return listaTipoRastreamentoTitulos;
    }

    /**
     * Set: listaTipoRastreamentoTitulos.
     * 
     * @param listaTipoRastreamentoTitulos
     *            the lista tipo rastreamento titulos
     */
    public void setListaTipoRastreamentoTitulos(
        List<SelectItem> listaTipoRastreamentoTitulos) {
        this.listaTipoRastreamentoTitulos = listaTipoRastreamentoTitulos;
    }

    /**
     * Get: listaTipoRastreamentoTitulosHash.
     * 
     * @return listaTipoRastreamentoTitulosHash
     */
    public Map<Long, String> getListaTipoRastreamentoTitulosHash() {
        return listaTipoRastreamentoTitulosHash;
    }

    /**
     * Set lista tipo rastreamento titulos hash.
     * 
     * @param listaTipoRastreamentoTitulosHash
     *            the lista tipo rastreamento titulos hash
     */
    public void setListaTipoRastreamentoTitulosHash(
        Map<Long, String> listaTipoRastreamentoTitulosHash) {
        this.listaTipoRastreamentoTitulosHash = listaTipoRastreamentoTitulosHash;
    }

    /**
     * Get: listaTipoRejeicaoAgendamento.
     * 
     * @return listaTipoRejeicaoAgendamento
     */
    public List<SelectItem> getListaTipoRejeicaoAgendamento() {
        return listaTipoRejeicaoAgendamento;
    }

    /**
     * Set: listaTipoRejeicaoAgendamento.
     * 
     * @param listaTipoRejeicaoAgendamento
     *            the lista tipo rejeicao agendamento
     */
    public void setListaTipoRejeicaoAgendamento(
        List<SelectItem> listaTipoRejeicaoAgendamento) {
        this.listaTipoRejeicaoAgendamento = listaTipoRejeicaoAgendamento;
    }

    /**
     * Get: listaTipoRejeicaoAgendamentoHash.
     * 
     * @return listaTipoRejeicaoAgendamentoHash
     */
    public Map<Long, String> getListaTipoRejeicaoAgendamentoHash() {
        return listaTipoRejeicaoAgendamentoHash;
    }

    /**
     * Set lista tipo rejeicao agendamento hash.
     * 
     * @param listaTipoRejeicaoAgendamentoHash
     *            the lista tipo rejeicao agendamento hash
     */
    public void setListaTipoRejeicaoAgendamentoHash(
        Map<Long, String> listaTipoRejeicaoAgendamentoHash) {
        this.listaTipoRejeicaoAgendamentoHash = listaTipoRejeicaoAgendamentoHash;
    }

    /**
     * Get: listaTipoRejeicaoEfetivacao.
     * 
     * @return listaTipoRejeicaoEfetivacao
     */
    public List<SelectItem> getListaTipoRejeicaoEfetivacao() {
        return listaTipoRejeicaoEfetivacao;
    }

    /**
     * Set: listaTipoRejeicaoEfetivacao.
     * 
     * @param listaTipoRejeicaoEfetivacao
     *            the lista tipo rejeicao efetivacao
     */
    public void setListaTipoRejeicaoEfetivacao(
        List<SelectItem> listaTipoRejeicaoEfetivacao) {
        this.listaTipoRejeicaoEfetivacao = listaTipoRejeicaoEfetivacao;
    }

    /**
     * Get: listaTipoRejeicaoEfetivacaoHash.
     * 
     * @return listaTipoRejeicaoEfetivacaoHash
     */
    public Map<Long, String> getListaTipoRejeicaoEfetivacaoHash() {
        return listaTipoRejeicaoEfetivacaoHash;
    }

    /**
     * Set lista tipo rejeicao efetivacao hash.
     * 
     * @param listaTipoRejeicaoEfetivacaoHash
     *            the lista tipo rejeicao efetivacao hash
     */
    public void setListaTipoRejeicaoEfetivacaoHash(
        Map<Long, String> listaTipoRejeicaoEfetivacaoHash) {
        this.listaTipoRejeicaoEfetivacaoHash = listaTipoRejeicaoEfetivacaoHash;
    }

    /**
     * Get: listaTipoRejeicaoLote.
     * 
     * @return listaTipoRejeicaoLote
     */
    public List<SelectItem> getListaTipoRejeicaoLote() {
        return listaTipoRejeicaoLote;
    }

    /**
     * Set: listaTipoRejeicaoLote.
     * 
     * @param listaTipoRejeicaoLote
     *            the lista tipo rejeicao lote
     */
    public void setListaTipoRejeicaoLote(List<SelectItem> listaTipoRejeicaoLote) {
        this.listaTipoRejeicaoLote = listaTipoRejeicaoLote;
    }

    /**
     * Get: listaTipoRejeicaoLoteHash.
     * 
     * @return listaTipoRejeicaoLoteHash
     */
    public Map<Long, String> getListaTipoRejeicaoLoteHash() {
        return listaTipoRejeicaoLoteHash;
    }

    /**
     * Set lista tipo rejeicao lote hash.
     * 
     * @param listaTipoRejeicaoLoteHash
     *            the lista tipo rejeicao lote hash
     */
    public void setListaTipoRejeicaoLoteHash(
        Map<Long, String> listaTipoRejeicaoLoteHash) {
        this.listaTipoRejeicaoLoteHash = listaTipoRejeicaoLoteHash;
    }

    /**
     * Get: listaTipoTratamentoContaTransferida.
     * 
     * @return listaTipoTratamentoContaTransferida
     */
    public List<SelectItem> getListaTipoTratamentoContaTransferida() {
        return listaTipoTratamentoContaTransferida;
    }

    /**
     * Set: listaTipoTratamentoContaTransferida.
     * 
     * @param listaTipoTratamentoContaTransferida
     *            the lista tipo tratamento conta transferida
     */
    public void setListaTipoTratamentoContaTransferida(
        List<SelectItem> listaTipoTratamentoContaTransferida) {
        this.listaTipoTratamentoContaTransferida = listaTipoTratamentoContaTransferida;
    }

    /**
     * Get: listaTipoTratamentoContaTransferidaHash.
     * 
     * @return listaTipoTratamentoContaTransferidaHash
     */
    public Map<Long, String> getListaTipoTratamentoContaTransferidaHash() {
        return listaTipoTratamentoContaTransferidaHash;
    }

    /**
     * Set lista tipo tratamento conta transferida hash.
     * 
     * @param listaTipoTratamentoContaTransferidaHash
     *            the lista tipo tratamento conta transferida hash
     */
    public void setListaTipoTratamentoContaTransferidaHash(
        Map<Long, String> listaTipoTratamentoContaTransferidaHash) {
        this.listaTipoTratamentoContaTransferidaHash = listaTipoTratamentoContaTransferidaHash;
    }

    /**
     * Get: listaTratamentoFeriadosDataPagamento.
     * 
     * @return listaTratamentoFeriadosDataPagamento
     */
    public List<SelectItem> getListaTratamentoFeriadosDataPagamento() {
        return listaTratamentoFeriadosDataPagamento;
    }

    /**
     * Set: listaTratamentoFeriadosDataPagamento.
     * 
     * @param listaTratamentoFeriadosDataPagamento
     *            the lista tratamento feriados data pagamento
     */
    public void setListaTratamentoFeriadosDataPagamento(
        List<SelectItem> listaTratamentoFeriadosDataPagamento) {
        this.listaTratamentoFeriadosDataPagamento = listaTratamentoFeriadosDataPagamento;
    }

    /**
     * Get: listaTratamentoFeriadosDataPagamentoHash.
     * 
     * @return listaTratamentoFeriadosDataPagamentoHash
     */
    public Map<Long, String> getListaTratamentoFeriadosDataPagamentoHash() {
        return listaTratamentoFeriadosDataPagamentoHash;
    }

    /**
     * Get: listaTratamentoFeriadoFimVigenciaCredito.
     * 
     * @return listaTratamentoFeriadoFimVigenciaCredito
     */
    public List<SelectItem> getListaTratamentoFeriadoFimVigenciaCredito() {
        return listaTratamentoFeriadoFimVigenciaCredito;
    }

    /**
     * Set: listaTratamentoFeriadoFimVigenciaCredito.
     * 
     * @param listaTratamentoFeriadoFimVigenciaCredito
     *            the lista tratamento feriado fim vigencia credito
     */
    public void setListaTratamentoFeriadoFimVigenciaCredito(
        List<SelectItem> listaTratamentoFeriadoFimVigenciaCredito) {
        this.listaTratamentoFeriadoFimVigenciaCredito = listaTratamentoFeriadoFimVigenciaCredito;
    }

    /**
     * Get: listaTratamentoFeriadoFimVigenciaCreditoHash.
     * 
     * @return listaTratamentoFeriadoFimVigenciaCreditoHash
     */
    public Map<Long, String> getListaTratamentoFeriadoFimVigenciaCreditoHash() {
        return listaTratamentoFeriadoFimVigenciaCreditoHash;
    }

    /**
     * Set lista tratamento feriado fim vigencia credito hash.
     * 
     * @param listaTratamentoFeriadoFimVigenciaCreditoHash
     *            the lista tratamento feriado fim vigencia credito hash
     */
    public void setListaTratamentoFeriadoFimVigenciaCreditoHash(
        Map<Long, String> listaTratamentoFeriadoFimVigenciaCreditoHash) {
        this.listaTratamentoFeriadoFimVigenciaCreditoHash = listaTratamentoFeriadoFimVigenciaCreditoHash;
    }

    /**
     * Get: listaTratamentoListaDebitoNumeracao.
     * 
     * @return listaTratamentoListaDebitoNumeracao
     */
    public List<SelectItem> getListaTratamentoListaDebitoNumeracao() {
        return listaTratamentoListaDebitoNumeracao;
    }

    /**
     * Set: listaTratamentoListaDebitoNumeracao.
     * 
     * @param listaTratamentoListaDebitoNumeracao
     *            the lista tratamento lista debito numeracao
     */
    public void setListaTratamentoListaDebitoNumeracao(
        List<SelectItem> listaTratamentoListaDebitoNumeracao) {
        this.listaTratamentoListaDebitoNumeracao = listaTratamentoListaDebitoNumeracao;
    }

    /**
     * Get: listaTratamentoListaDebitoNumeracaoHash.
     * 
     * @return listaTratamentoListaDebitoNumeracaoHash
     */
    public Map<Long, String> getListaTratamentoListaDebitoNumeracaoHash() {
        return listaTratamentoListaDebitoNumeracaoHash;
    }

    /**
     * Set lista tratamento lista debito numeracao hash.
     * 
     * @param listaTratamentoListaDebitoNumeracaoHash
     *            the lista tratamento lista debito numeracao hash
     */
    public void setListaTratamentoListaDebitoNumeracaoHash(
        Map<Long, String> listaTratamentoListaDebitoNumeracaoHash) {
        this.listaTratamentoListaDebitoNumeracaoHash = listaTratamentoListaDebitoNumeracaoHash;
    }

    /**
     * Get: listaTratamentoValorDivergente.
     * 
     * @return listaTratamentoValorDivergente
     */
    public List<SelectItem> getListaTratamentoValorDivergente() {
        return listaTratamentoValorDivergente;
    }

    /**
     * Set: listaTratamentoValorDivergente.
     * 
     * @param listaTratamentoValorDivergente
     *            the lista tratamento valor divergente
     */
    public void setListaTratamentoValorDivergente(
        List<SelectItem> listaTratamentoValorDivergente) {
        this.listaTratamentoValorDivergente = listaTratamentoValorDivergente;
    }

    /**
     * Get: listaTratamentoValorDivergenteHash.
     * 
     * @return listaTratamentoValorDivergenteHash
     */
    public Map<Long, String> getListaTratamentoValorDivergenteHash() {
        return listaTratamentoValorDivergenteHash;
    }

    /**
     * Set lista tratamento valor divergente hash.
     * 
     * @param listaTratamentoValorDivergenteHash
     *            the lista tratamento valor divergente hash
     */
    public void setListaTratamentoValorDivergenteHash(
        Map<Long, String> listaTratamentoValorDivergenteHash) {
        this.listaTratamentoValorDivergenteHash = listaTratamentoValorDivergenteHash;
    }

    /**
     * Get: cobrarTarifasFunc.
     * 
     * @return cobrarTarifasFunc
     */
    public String getCobrarTarifasFunc() {
        return cobrarTarifasFunc;
    }

    /**
     * Set: cobrarTarifasFunc.
     * 
     * @param cobrarTarifasFunc
     *            the cobrar tarifas func
     */
    public void setCobrarTarifasFunc(String cobrarTarifasFunc) {
        this.cobrarTarifasFunc = cobrarTarifasFunc;
    }

    /**
     * Get: codigoFormulario.
     * 
     * @return codigoFormulario
     */
    public String getCodigoFormulario() {
        return codigoFormulario;
    }

    /**
     * Set: codigoFormulario.
     * 
     * @param codigoFormulario
     *            the codigo formulario
     */
    public void setCodigoFormulario(String codigoFormulario) {
        this.codigoFormulario = codigoFormulario;
    }

    /**
     * Get: dsAcaoParaNaoComprovacao.
     * 
     * @return dsAcaoParaNaoComprovacao
     */
    public String getDsAcaoParaNaoComprovacao() {
        return dsAcaoParaNaoComprovacao;
    }

    /**
     * Set: dsAcaoParaNaoComprovacao.
     * 
     * @param dsAcaoParaNaoComprovacao
     *            the ds acao para nao comprovacao
     */
    public void setDsAcaoParaNaoComprovacao(String dsAcaoParaNaoComprovacao) {
        this.dsAcaoParaNaoComprovacao = dsAcaoParaNaoComprovacao;
    }

    /**
     * Get: dsAgendamentoDebitosPendentesVeiculos.
     * 
     * @return dsAgendamentoDebitosPendentesVeiculos
     */
    public String getDsAgendamentoDebitosPendentesVeiculos() {
        return dsAgendamentoDebitosPendentesVeiculos;
    }

    /**
     * Set: dsAgendamentoDebitosPendentesVeiculos.
     * 
     * @param dsAgendamentoDebitosPendentesVeiculos
     *            the ds agendamento debitos pendentes veiculos
     */
    public void setDsAgendamentoDebitosPendentesVeiculos(
        String dsAgendamentoDebitosPendentesVeiculos) {
        this.dsAgendamentoDebitosPendentesVeiculos = dsAgendamentoDebitosPendentesVeiculos;
    }

    /**
     * Get: dsCadConsultaEndEnvio.
     * 
     * @return dsCadConsultaEndEnvio
     */
    public String getDsCadConsultaEndEnvio() {
        return dsCadConsultaEndEnvio;
    }

    /**
     * Set: dsCadConsultaEndEnvio.
     * 
     * @param dsCadConsultaEndEnvio
     *            the ds cad consulta end envio
     */
    public void setDsCadConsultaEndEnvio(String dsCadConsultaEndEnvio) {
        this.dsCadConsultaEndEnvio = dsCadConsultaEndEnvio;
    }

    /**
     * Get: dsCadUtilizadoRec.
     * 
     * @return dsCadUtilizadoRec
     */
    public String getDsCadUtilizadoRec() {
        return dsCadUtilizadoRec;
    }

    /**
     * Set: dsCadUtilizadoRec.
     * 
     * @param dsCadUtilizadoRec
     *            the ds cad utilizado rec
     */
    public void setDsCadUtilizadoRec(String dsCadUtilizadoRec) {
        this.dsCadUtilizadoRec = dsCadUtilizadoRec;
    }

    /**
     * Get: dsCondicaoEnquadramento.
     * 
     * @return dsCondicaoEnquadramento
     */
    public String getDsCondicaoEnquadramento() {
        return dsCondicaoEnquadramento;
    }

    /**
     * Set: dsCondicaoEnquadramento.
     * 
     * @param dsCondicaoEnquadramento
     *            the ds condicao enquadramento
     */
    public void setDsCondicaoEnquadramento(String dsCondicaoEnquadramento) {
        this.dsCondicaoEnquadramento = dsCondicaoEnquadramento;
    }

    /**
     * Get: dsContExpCredConta.
     * 
     * @return dsContExpCredConta
     */
    public String getDsContExpCredConta() {
        return dsContExpCredConta;
    }

    /**
     * Set: dsContExpCredConta.
     * 
     * @param dsContExpCredConta
     *            the ds cont exp cred conta
     */
    public void setDsContExpCredConta(String dsContExpCredConta) {
        this.dsContExpCredConta = dsContExpCredConta;
    }

    /**
     * Get: dsCritCompEnquadramento.
     * 
     * @return dsCritCompEnquadramento
     */
    public String getDsCritCompEnquadramento() {
        return dsCritCompEnquadramento;
    }

    /**
     * Set: dsCritCompEnquadramento.
     * 
     * @param dsCritCompEnquadramento
     *            the ds crit comp enquadramento
     */
    public void setDsCritCompEnquadramento(String dsCritCompEnquadramento) {
        this.dsCritCompEnquadramento = dsCritCompEnquadramento;
    }

    /**
     * Get: dsCritPrincEnquadramento.
     * 
     * @return dsCritPrincEnquadramento
     */
    public String getDsCritPrincEnquadramento() {
        return dsCritPrincEnquadramento;
    }

    /**
     * Set: dsCritPrincEnquadramento.
     * 
     * @param dsCritPrincEnquadramento
     *            the ds crit princ enquadramento
     */
    public void setDsCritPrincEnquadramento(String dsCritPrincEnquadramento) {
        this.dsCritPrincEnquadramento = dsCritPrincEnquadramento;
    }

    /**
     * Get: dsDestinoEnvio.
     * 
     * @return dsDestinoEnvio
     */
    public String getDsDestinoEnvio() {
        return dsDestinoEnvio;
    }

    /**
     * Set: dsDestinoEnvio.
     * 
     * @param dsDestinoEnvio
     *            the ds destino envio
     */
    public void setDsDestinoEnvio(String dsDestinoEnvio) {
        this.dsDestinoEnvio = dsDestinoEnvio;
    }

    /**
     * Get: dsDestinoEnvioCorresp.
     * 
     * @return dsDestinoEnvioCorresp
     */
    public String getDsDestinoEnvioCorresp() {
        return dsDestinoEnvioCorresp;
    }

    /**
     * Set: dsDestinoEnvioCorresp.
     * 
     * @param dsDestinoEnvioCorresp
     *            the ds destino envio corresp
     */
    public void setDsDestinoEnvioCorresp(String dsDestinoEnvioCorresp) {
        this.dsDestinoEnvioCorresp = dsDestinoEnvioCorresp;
    }

    /**
     * Get: dsFormaAutPagamentos.
     * 
     * @return dsFormaAutPagamentos
     */
    public String getDsFormaAutPagamentos() {
        return dsFormaAutPagamentos;
    }

    /**
     * Set: dsFormaAutPagamentos.
     * 
     * @param dsFormaAutPagamentos
     *            the ds forma aut pagamentos
     */
    public void setDsFormaAutPagamentos(String dsFormaAutPagamentos) {
        this.dsFormaAutPagamentos = dsFormaAutPagamentos;
    }

    /**
     * Get: dsFormaEnvioPagamento.
     * 
     * @return dsFormaEnvioPagamento
     */
    public String getDsFormaEnvioPagamento() {
        return dsFormaEnvioPagamento;
    }

    /**
     * Set: dsFormaEnvioPagamento.
     * 
     * @param dsFormaEnvioPagamento
     *            the ds forma envio pagamento
     */
    public void setDsFormaEnvioPagamento(String dsFormaEnvioPagamento) {
        this.dsFormaEnvioPagamento = dsFormaEnvioPagamento;
    }

    /**
     * Get: dsFormaManCadFavorecido.
     * 
     * @return dsFormaManCadFavorecido
     */
    public String getDsFormaManCadFavorecido() {
        return dsFormaManCadFavorecido;
    }

    /**
     * Set: dsFormaManCadFavorecido.
     * 
     * @param dsFormaManCadFavorecido
     *            the ds forma man cad favorecido
     */
    public void setDsFormaManCadFavorecido(String dsFormaManCadFavorecido) {
        this.dsFormaManCadFavorecido = dsFormaManCadFavorecido;
    }

    /**
     * Get: dsFormaManCadProcurador.
     * 
     * @return dsFormaManCadProcurador
     */
    public String getDsFormaManCadProcurador() {
        return dsFormaManCadProcurador;
    }

    /**
     * Set: dsFormaManCadProcurador.
     * 
     * @param dsFormaManCadProcurador
     *            the ds forma man cad procurador
     */
    public void setDsFormaManCadProcurador(String dsFormaManCadProcurador) {
        this.dsFormaManCadProcurador = dsFormaManCadProcurador;
    }

    /**
     * Get: dsMidiaDispCorrentista.
     * 
     * @return dsMidiaDispCorrentista
     */
    public String getDsMidiaDispCorrentista() {
        return dsMidiaDispCorrentista;
    }

    /**
     * Set: dsMidiaDispCorrentista.
     * 
     * @param dsMidiaDispCorrentista
     *            the ds midia disp correntista
     */
    public void setDsMidiaDispCorrentista(String dsMidiaDispCorrentista) {
        this.dsMidiaDispCorrentista = dsMidiaDispCorrentista;
    }

    /**
     * Get: dsMidiaMsgRecad.
     * 
     * @return dsMidiaMsgRecad
     */
    public String getDsMidiaMsgRecad() {
        return dsMidiaMsgRecad;
    }

    /**
     * Set: dsMidiaMsgRecad.
     * 
     * @param dsMidiaMsgRecad
     *            the ds midia msg recad
     */
    public void setDsMidiaMsgRecad(String dsMidiaMsgRecad) {
        this.dsMidiaMsgRecad = dsMidiaMsgRecad;
    }

    /**
     * Get: dsMomentoEnvio.
     * 
     * @return dsMomentoEnvio
     */
    public String getDsMomentoEnvio() {
        return dsMomentoEnvio;
    }

    /**
     * Set: dsMomentoEnvio.
     * 
     * @param dsMomentoEnvio
     *            the ds momento envio
     */
    public void setDsMomentoEnvio(String dsMomentoEnvio) {
        this.dsMomentoEnvio = dsMomentoEnvio;
    }

    /**
     * Get: dsMomentoIndCredEfetivado.
     * 
     * @return dsMomentoIndCredEfetivado
     */
    public String getDsMomentoIndCredEfetivado() {
        return dsMomentoIndCredEfetivado;
    }

    /**
     * Set: dsMomentoIndCredEfetivado.
     * 
     * @param dsMomentoIndCredEfetivado
     *            the ds momento ind cred efetivado
     */
    public void setDsMomentoIndCredEfetivado(String dsMomentoIndCredEfetivado) {
        this.dsMomentoIndCredEfetivado = dsMomentoIndCredEfetivado;
    }

    /**
     * Get: dsPeriocidadeManCadProc.
     * 
     * @return dsPeriocidadeManCadProc
     */
    public String getDsPeriocidadeManCadProc() {
        return dsPeriocidadeManCadProc;
    }

    /**
     * Set: dsPeriocidadeManCadProc.
     * 
     * @param dsPeriocidadeManCadProc
     *            the ds periocidade man cad proc
     */
    public void setDsPeriocidadeManCadProc(String dsPeriocidadeManCadProc) {
        this.dsPeriocidadeManCadProc = dsPeriocidadeManCadProc;
    }

    /**
     * Get: dsPeriodicidadeEnvioRemessaManutencao.
     * 
     * @return dsPeriodicidadeEnvioRemessaManutencao
     */
    public String getDsPeriodicidadeEnvioRemessaManutencao() {
        return dsPeriodicidadeEnvioRemessaManutencao;
    }

    /**
     * Set: dsPeriodicidadeEnvioRemessaManutencao.
     * 
     * @param dsPeriodicidadeEnvioRemessaManutencao
     *            the ds periodicidade envio remessa manutencao
     */
    public void setDsPeriodicidadeEnvioRemessaManutencao(
        String dsPeriodicidadeEnvioRemessaManutencao) {
        this.dsPeriodicidadeEnvioRemessaManutencao = dsPeriodicidadeEnvioRemessaManutencao;
    }

    /**
     * Get: dsPeriodicidadePesquisaDebitosPendentesVeiculos.
     * 
     * @return dsPeriodicidadePesquisaDebitosPendentesVeiculos
     */
    public String getDsPeriodicidadePesquisaDebitosPendentesVeiculos() {
        return dsPeriodicidadePesquisaDebitosPendentesVeiculos;
    }

    /**
     * Set: dsPeriodicidadePesquisaDebitosPendentesVeiculos.
     * 
     * @param dsPeriodicidadePesquisaDebitosPendentesVeiculos
     *            the ds periodicidade pesquisa debitos pendentes veiculos
     */
    public void setDsPeriodicidadePesquisaDebitosPendentesVeiculos(
        String dsPeriodicidadePesquisaDebitosPendentesVeiculos) {
        this.dsPeriodicidadePesquisaDebitosPendentesVeiculos = dsPeriodicidadePesquisaDebitosPendentesVeiculos;
    }

    /**
     * Get: dsPermiteEstornoPagamento.
     * 
     * @return dsPermiteEstornoPagamento
     */
    public String getDsPermiteEstornoPagamento() {
        return dsPermiteEstornoPagamento;
    }

    /**
     * Set: dsPermiteEstornoPagamento.
     * 
     * @param dsPermiteEstornoPagamento
     *            the ds permite estorno pagamento
     */
    public void setDsPermiteEstornoPagamento(String dsPermiteEstornoPagamento) {
        this.dsPermiteEstornoPagamento = dsPermiteEstornoPagamento;
    }

    /**
     * Get: dsPrioridDebito.
     * 
     * @return dsPrioridDebito
     */
    public String getDsPrioridDebito() {
        return dsPrioridDebito;
    }

    /**
     * Set: dsPrioridDebito.
     * 
     * @param dsPrioridDebito
     *            the ds priorid debito
     */
    public void setDsPrioridDebito(String dsPrioridDebito) {
        this.dsPrioridDebito = dsPrioridDebito;
    }

    /**
     * Get: dsProcessamentoEfetivacaoPagamento.
     * 
     * @return dsProcessamentoEfetivacaoPagamento
     */
    public String getDsProcessamentoEfetivacaoPagamento() {
        return dsProcessamentoEfetivacaoPagamento;
    }

    /**
     * Set: dsProcessamentoEfetivacaoPagamento.
     * 
     * @param dsProcessamentoEfetivacaoPagamento
     *            the ds processamento efetivacao pagamento
     */
    public void setDsProcessamentoEfetivacaoPagamento(
        String dsProcessamentoEfetivacaoPagamento) {
        this.dsProcessamentoEfetivacaoPagamento = dsProcessamentoEfetivacaoPagamento;
    }

    /**
     * Get: dsTipoCargaCadastroBeneficiario.
     * 
     * @return dsTipoCargaCadastroBeneficiario
     */
    public String getDsTipoCargaCadastroBeneficiario() {
        return dsTipoCargaCadastroBeneficiario;
    }

    /**
     * Set: dsTipoCargaCadastroBeneficiario.
     * 
     * @param dsTipoCargaCadastroBeneficiario
     *            the ds tipo carga cadastro beneficiario
     */
    public void setDsTipoCargaCadastroBeneficiario(
        String dsTipoCargaCadastroBeneficiario) {
        this.dsTipoCargaCadastroBeneficiario = dsTipoCargaCadastroBeneficiario;
    }

    /**
     * Get: dsTipoCartaoContaSalario.
     * 
     * @return dsTipoCartaoContaSalario
     */
    public String getDsTipoCartaoContaSalario() {
        return dsTipoCartaoContaSalario;
    }

    /**
     * Set: dsTipoCartaoContaSalario.
     * 
     * @param dsTipoCartaoContaSalario
     *            the ds tipo cartao conta salario
     */
    public void setDsTipoCartaoContaSalario(String dsTipoCartaoContaSalario) {
        this.dsTipoCartaoContaSalario = dsTipoCartaoContaSalario;
    }

    /**
     * Get: dsTipoConsistenciaCpfCnpjFavorecido.
     * 
     * @return dsTipoConsistenciaCpfCnpjFavorecido
     */
    public String getDsTipoConsistenciaCpfCnpjFavorecido() {
        return dsTipoConsistenciaCpfCnpjFavorecido;
    }

    /**
     * Set: dsTipoConsistenciaCpfCnpjFavorecido.
     * 
     * @param dsTipoConsistenciaCpfCnpjFavorecido
     *            the ds tipo consistencia cpf cnpj favorecido
     */
    public void setDsTipoConsistenciaCpfCnpjFavorecido(
        String dsTipoConsistenciaCpfCnpjFavorecido) {
        this.dsTipoConsistenciaCpfCnpjFavorecido = dsTipoConsistenciaCpfCnpjFavorecido;
    }

    /**
     * Get: dsTipoConsistenciaCpfCnpjProprietario.
     * 
     * @return dsTipoConsistenciaCpfCnpjProprietario
     */
    public String getDsTipoConsistenciaCpfCnpjProprietario() {
        return dsTipoConsistenciaCpfCnpjProprietario;
    }

    /**
     * Set: dsTipoConsistenciaCpfCnpjProprietario.
     * 
     * @param dsTipoConsistenciaCpfCnpjProprietario
     *            the ds tipo consistencia cpf cnpj proprietario
     */
    public void setDsTipoConsistenciaCpfCnpjProprietario(
        String dsTipoConsistenciaCpfCnpjProprietario) {
        this.dsTipoConsistenciaCpfCnpjProprietario = dsTipoConsistenciaCpfCnpjProprietario;
    }

    /**
     * Get: dsTipoConsistenciaIdentificacaoBeneficiario.
     * 
     * @return dsTipoConsistenciaIdentificacaoBeneficiario
     */
    public String getDsTipoConsistenciaIdentificacaoBeneficiario() {
        return dsTipoConsistenciaIdentificacaoBeneficiario;
    }

    /**
     * Set: dsTipoConsistenciaIdentificacaoBeneficiario.
     * 
     * @param dsTipoConsistenciaIdentificacaoBeneficiario
     *            the ds tipo consistencia identificacao beneficiario
     */
    public void setDsTipoConsistenciaIdentificacaoBeneficiario(
        String dsTipoConsistenciaIdentificacaoBeneficiario) {
        this.dsTipoConsistenciaIdentificacaoBeneficiario = dsTipoConsistenciaIdentificacaoBeneficiario;
    }

    /**
     * Get: dsTipoConsistenciaInscricaoFavorecido.
     * 
     * @return dsTipoConsistenciaInscricaoFavorecido
     */
    public String getDsTipoConsistenciaInscricaoFavorecido() {
        return dsTipoConsistenciaInscricaoFavorecido;
    }

    /**
     * Set: dsTipoConsistenciaInscricaoFavorecido.
     * 
     * @param dsTipoConsistenciaInscricaoFavorecido
     *            the ds tipo consistencia inscricao favorecido
     */
    public void setDsTipoConsistenciaInscricaoFavorecido(
        String dsTipoConsistenciaInscricaoFavorecido) {
        this.dsTipoConsistenciaInscricaoFavorecido = dsTipoConsistenciaInscricaoFavorecido;
    }

    /**
     * Get: dsTipoConsolidacaoPagamentosComprovante.
     * 
     * @return dsTipoConsolidacaoPagamentosComprovante
     */
    public String getDsTipoConsolidacaoPagamentosComprovante() {
        return dsTipoConsolidacaoPagamentosComprovante;
    }

    /**
     * Set: dsTipoConsolidacaoPagamentosComprovante.
     * 
     * @param dsTipoConsolidacaoPagamentosComprovante
     *            the ds tipo consolidacao pagamentos comprovante
     */
    public void setDsTipoConsolidacaoPagamentosComprovante(
        String dsTipoConsolidacaoPagamentosComprovante) {
        this.dsTipoConsolidacaoPagamentosComprovante = dsTipoConsolidacaoPagamentosComprovante;
    }

    /**
     * Get: dsTipoConsSaldo.
     * 
     * @return dsTipoConsSaldo
     */
    public String getDsTipoConsSaldo() {
        return dsTipoConsSaldo;
    }

    /**
     * Set: dsTipoConsSaldo.
     * 
     * @param dsTipoConsSaldo
     *            the ds tipo cons saldo
     */
    public void setDsTipoConsSaldo(String dsTipoConsSaldo) {
        this.dsTipoConsSaldo = dsTipoConsSaldo;
    }

    /**
     * Get: dsTipoContaCredito.
     * 
     * @return dsTipoContaCredito
     */
    public String getDsTipoContaCredito() {
        return dsTipoContaCredito;
    }

    /**
     * Set: dsTipoContaCredito.
     * 
     * @param dsTipoContaCredito
     *            the ds tipo conta credito
     */
    public void setDsTipoContaCredito(String dsTipoContaCredito) {
        this.dsTipoContaCredito = dsTipoContaCredito;
    }

    /**
     * Get: dsTipoDataControleFloating.
     * 
     * @return dsTipoDataControleFloating
     */
    public String getDsTipoDataControleFloating() {
        return dsTipoDataControleFloating;
    }

    /**
     * Set: dsTipoDataControleFloating.
     * 
     * @param dsTipoDataControleFloating
     *            the ds tipo data controle floating
     */
    public void setDsTipoDataControleFloating(String dsTipoDataControleFloating) {
        this.dsTipoDataControleFloating = dsTipoDataControleFloating;
    }

    /**
     * Get: dsTipoFormacaoListaDebito.
     * 
     * @return dsTipoFormacaoListaDebito
     */
    public String getDsTipoFormacaoListaDebito() {
        return dsTipoFormacaoListaDebito;
    }

    /**
     * Set: dsTipoFormacaoListaDebito.
     * 
     * @param dsTipoFormacaoListaDebito
     *            the ds tipo formacao lista debito
     */
    public void setDsTipoFormacaoListaDebito(String dsTipoFormacaoListaDebito) {
        this.dsTipoFormacaoListaDebito = dsTipoFormacaoListaDebito;
    }

    /**
     * Get: dsTipoIdentificacaoBeneficiario.
     * 
     * @return dsTipoIdentificacaoBeneficiario
     */
    public String getDsTipoIdentificacaoBeneficiario() {
        return dsTipoIdentificacaoBeneficiario;
    }

    /**
     * Set: dsTipoIdentificacaoBeneficiario.
     * 
     * @param dsTipoIdentificacaoBeneficiario
     *            the ds tipo identificacao beneficiario
     */
    public void setDsTipoIdentificacaoBeneficiario(
        String dsTipoIdentificacaoBeneficiario) {
        this.dsTipoIdentificacaoBeneficiario = dsTipoIdentificacaoBeneficiario;
    }

    /**
     * Get: dsTipoInscricaoFavorecido.
     * 
     * @return dsTipoInscricaoFavorecido
     */
    public String getDsTipoInscricaoFavorecido() {
        return dsTipoInscricaoFavorecido;
    }

    /**
     * Set: dsTipoInscricaoFavorecido.
     * 
     * @param dsTipoInscricaoFavorecido
     *            the ds tipo inscricao favorecido
     */
    public void setDsTipoInscricaoFavorecido(String dsTipoInscricaoFavorecido) {
        this.dsTipoInscricaoFavorecido = dsTipoInscricaoFavorecido;
    }

    /**
     * Get: dsTipoRastreamentoTitulo.
     * 
     * @return dsTipoRastreamentoTitulo
     */
    public String getDsTipoRastreamentoTitulo() {
        return dsTipoRastreamentoTitulo;
    }

    /**
     * Set: dsTipoRastreamentoTitulo.
     * 
     * @param dsTipoRastreamentoTitulo
     *            the ds tipo rastreamento titulo
     */
    public void setDsTipoRastreamentoTitulo(String dsTipoRastreamentoTitulo) {
        this.dsTipoRastreamentoTitulo = dsTipoRastreamentoTitulo;
    }

    /**
     * Get: dsTipoRejAgendamento.
     * 
     * @return dsTipoRejAgendamento
     */
    public String getDsTipoRejAgendamento() {
        return dsTipoRejAgendamento;
    }

    /**
     * Set: dsTipoRejAgendamento.
     * 
     * @param dsTipoRejAgendamento
     *            the ds tipo rej agendamento
     */
    public void setDsTipoRejAgendamento(String dsTipoRejAgendamento) {
        this.dsTipoRejAgendamento = dsTipoRejAgendamento;
    }

    /**
     * Get: dsTipoRejEfetivacao.
     * 
     * @return dsTipoRejEfetivacao
     */
    public String getDsTipoRejEfetivacao() {
        return dsTipoRejEfetivacao;
    }

    /**
     * Set: dsTipoRejEfetivacao.
     * 
     * @param dsTipoRejEfetivacao
     *            the ds tipo rej efetivacao
     */
    public void setDsTipoRejEfetivacao(String dsTipoRejEfetivacao) {
        this.dsTipoRejEfetivacao = dsTipoRejEfetivacao;
    }

    /**
     * Get: dsTipoRejeicaoLote.
     * 
     * @return dsTipoRejeicaoLote
     */
    public String getDsTipoRejeicaoLote() {
        return dsTipoRejeicaoLote;
    }

    /**
     * Set: dsTipoRejeicaoLote.
     * 
     * @param dsTipoRejeicaoLote
     *            the ds tipo rejeicao lote
     */
    public void setDsTipoRejeicaoLote(String dsTipoRejeicaoLote) {
        this.dsTipoRejeicaoLote = dsTipoRejeicaoLote;
    }

    /**
     * Get: dsTratamentoFeriadoFimVigenciaCredito.
     * 
     * @return dsTratamentoFeriadoFimVigenciaCredito
     */
    public String getDsTratamentoFeriadoFimVigenciaCredito() {
        return dsTratamentoFeriadoFimVigenciaCredito;
    }

    /**
     * Set: dsTratamentoFeriadoFimVigenciaCredito.
     * 
     * @param dsTratamentoFeriadoFimVigenciaCredito
     *            the ds tratamento feriado fim vigencia credito
     */
    public void setDsTratamentoFeriadoFimVigenciaCredito(
        String dsTratamentoFeriadoFimVigenciaCredito) {
        this.dsTratamentoFeriadoFimVigenciaCredito = dsTratamentoFeriadoFimVigenciaCredito;
    }

    /**
     * Get: dsTratamentoListaDebitoSemNumeracao.
     * 
     * @return dsTratamentoListaDebitoSemNumeracao
     */
    public String getDsTratamentoListaDebitoSemNumeracao() {
        return dsTratamentoListaDebitoSemNumeracao;
    }

    /**
     * Set: dsTratamentoListaDebitoSemNumeracao.
     * 
     * @param dsTratamentoListaDebitoSemNumeracao
     *            the ds tratamento lista debito sem numeracao
     */
    public void setDsTratamentoListaDebitoSemNumeracao(
        String dsTratamentoListaDebitoSemNumeracao) {
        this.dsTratamentoListaDebitoSemNumeracao = dsTratamentoListaDebitoSemNumeracao;
    }

    /**
     * Get: dsTratamentoValorDivergente.
     * 
     * @return dsTratamentoValorDivergente
     */
    public String getDsTratamentoValorDivergente() {
        return dsTratamentoValorDivergente;
    }

    /**
     * Set: dsTratamentoValorDivergente.
     * 
     * @param dsTratamentoValorDivergente
     *            the ds tratamento valor divergente
     */
    public void setDsTratamentoValorDivergente(
        String dsTratamentoValorDivergente) {
        this.dsTratamentoValorDivergente = dsTratamentoValorDivergente;
    }

    /**
     * Get: dsTratFeriadosDataPagamento.
     * 
     * @return dsTratFeriadosDataPagamento
     */
    public String getDsTratFeriadosDataPagamento() {
        return dsTratFeriadosDataPagamento;
    }

    /**
     * Set: dsTratFeriadosDataPagamento.
     * 
     * @param dsTratFeriadosDataPagamento
     *            the ds trat feriados data pagamento
     */
    public void setDsTratFeriadosDataPagamento(
        String dsTratFeriadosDataPagamento) {
        this.dsTratFeriadosDataPagamento = dsTratFeriadosDataPagamento;
    }

    /**
     * Get: dtFimPerAcertoDados.
     * 
     * @return dtFimPerAcertoDados
     */
    public Date getDtFimPerAcertoDados() {
        return dtFimPerAcertoDados;
    }

    /**
     * Set: dtFimPerAcertoDados.
     * 
     * @param dtFimPerAcertoDados
     *            the dt fim per acerto dados
     */
    public void setDtFimPerAcertoDados(Date dtFimPerAcertoDados) {
        this.dtFimPerAcertoDados = dtFimPerAcertoDados;
    }

    /**
     * Get: dtFimRecadastramento.
     * 
     * @return dtFimRecadastramento
     */
    public Date getDtFimRecadastramento() {
        return dtFimRecadastramento;
    }

    /**
     * Set: dtFimRecadastramento.
     * 
     * @param dtFimRecadastramento
     *            the dt fim recadastramento
     */
    public void setDtFimRecadastramento(Date dtFimRecadastramento) {
        this.dtFimRecadastramento = dtFimRecadastramento;
    }

    /**
     * Get: dtInicioBloqPapeleta.
     * 
     * @return dtInicioBloqPapeleta
     */
    public Date getDtInicioBloqPapeleta() {
        return dtInicioBloqPapeleta;
    }

    /**
     * Set: dtInicioBloqPapeleta.
     * 
     * @param dtInicioBloqPapeleta
     *            the dt inicio bloq papeleta
     */
    public void setDtInicioBloqPapeleta(Date dtInicioBloqPapeleta) {
        this.dtInicioBloqPapeleta = dtInicioBloqPapeleta;
    }

    /**
     * Get: dtInicioPerAcertoDados.
     * 
     * @return dtInicioPerAcertoDados
     */
    public Date getDtInicioPerAcertoDados() {
        return dtInicioPerAcertoDados;
    }

    /**
     * Set: dtInicioPerAcertoDados.
     * 
     * @param dtInicioPerAcertoDados
     *            the dt inicio per acerto dados
     */
    public void setDtInicioPerAcertoDados(Date dtInicioPerAcertoDados) {
        this.dtInicioPerAcertoDados = dtInicioPerAcertoDados;
    }

    /**
     * Get: dtInicioRastreamento.
     * 
     * @return dtInicioRastreamento
     */
    public Date getDtInicioRastreamento() {
        return dtInicioRastreamento;
    }

    /**
     * Set: dtInicioRastreamento.
     * 
     * @param dtInicioRastreamento
     *            the dt inicio rastreamento
     */
    public void setDtInicioRastreamento(Date dtInicioRastreamento) {
        this.dtInicioRastreamento = dtInicioRastreamento;
    }

    /**
     * Get: dtInicioRecadastramento.
     * 
     * @return dtInicioRecadastramento
     */
    public Date getDtInicioRecadastramento() {
        return dtInicioRecadastramento;
    }

    /**
     * Set: dtInicioRecadastramento.
     * 
     * @param dtInicioRecadastramento
     *            the dt inicio recadastramento
     */
    public void setDtInicioRecadastramento(Date dtInicioRecadastramento) {
        this.dtInicioRecadastramento = dtInicioRecadastramento;
    }

    /**
     * Get: dtLimiteEnqConvContaSal.
     * 
     * @return dtLimiteEnqConvContaSal
     */
    public Date getDtLimiteEnqConvContaSal() {
        return dtLimiteEnqConvContaSal;
    }

    /**
     * Set: dtLimiteEnqConvContaSal.
     * 
     * @param dtLimiteEnqConvContaSal
     *            the dt limite enq conv conta sal
     */
    public void setDtLimiteEnqConvContaSal(Date dtLimiteEnqConvContaSal) {
        this.dtLimiteEnqConvContaSal = dtLimiteEnqConvContaSal;
    }

    /**
     * Get: dtLimiteVincClienteBenef.
     * 
     * @return dtLimiteVincClienteBenef
     */
    public Date getDtLimiteVincClienteBenef() {
        return dtLimiteVincClienteBenef;
    }

    /**
     * Set: dtLimiteVincClienteBenef.
     * 
     * @param dtLimiteVincClienteBenef
     *            the dt limite vinc cliente benef
     */
    public void setDtLimiteVincClienteBenef(Date dtLimiteVincClienteBenef) {
        this.dtLimiteVincClienteBenef = dtLimiteVincClienteBenef;
    }

    /**
     * Get: formularioImpressao.
     * 
     * @return formularioImpressao
     */
    public String getFormularioImpressao() {
        return formularioImpressao;
    }

    /**
     * Set: formularioImpressao.
     * 
     * @param formularioImpressao
     *            the formulario impressao
     */
    public void setFormularioImpressao(String formularioImpressao) {
        this.formularioImpressao = formularioImpressao;
    }

    /**
     * Get: percMaxRegInconsistente.
     * 
     * @return percMaxRegInconsistente
     */
    public Integer getPercMaxRegInconsistente() {
        return percMaxRegInconsistente;
    }

    /**
     * Set: percMaxRegInconsistente.
     * 
     * @param percMaxRegInconsistente
     *            the perc max reg inconsistente
     */
    public void setPercMaxRegInconsistente(Integer percMaxRegInconsistente) {
        this.percMaxRegInconsistente = percMaxRegInconsistente;
    }

    /**
     * Get: qtddDiasExpiracaoCredito.
     * 
     * @return qtddDiasExpiracaoCredito
     */
    public String getQtddDiasExpiracaoCredito() {
        return qtddDiasExpiracaoCredito;
    }

    /**
     * Set: qtddDiasExpiracaoCredito.
     * 
     * @param qtddDiasExpiracaoCredito
     *            the qtdd dias expiracao credito
     */
    public void setQtddDiasExpiracaoCredito(String qtddDiasExpiracaoCredito) {
        this.qtddDiasExpiracaoCredito = qtddDiasExpiracaoCredito;
    }

    /**
     * Get: rdoAbertContaBancoPostBradSeg.
     * 
     * @return rdoAbertContaBancoPostBradSeg
     */
    public String getRdoAbertContaBancoPostBradSeg() {
        return rdoAbertContaBancoPostBradSeg;
    }

    /**
     * Set: rdoAbertContaBancoPostBradSeg.
     * 
     * @param rdoAbertContaBancoPostBradSeg
     *            the rdo abert conta banco post brad seg
     */
    public void setRdoAbertContaBancoPostBradSeg(
        String rdoAbertContaBancoPostBradSeg) {
        this.rdoAbertContaBancoPostBradSeg = rdoAbertContaBancoPostBradSeg;
    }

    /**
     * Get: rdoAdesaoSacadoEletronico.
     * 
     * @return rdoAdesaoSacadoEletronico
     */
    public String getRdoAdesaoSacadoEletronico() {
        return rdoAdesaoSacadoEletronico;
    }

    /**
     * Set: rdoAdesaoSacadoEletronico.
     * 
     * @param rdoAdesaoSacadoEletronico
     *            the rdo adesao sacado eletronico
     */
    public void setRdoAdesaoSacadoEletronico(String rdoAdesaoSacadoEletronico) {
        this.rdoAdesaoSacadoEletronico = rdoAdesaoSacadoEletronico;
    }

    /**
     * Get: rdoAgendarTituloRastProp.
     * 
     * @return rdoAgendarTituloRastProp
     */
    public String getRdoAgendarTituloRastProp() {
        return rdoAgendarTituloRastProp;
    }

    /**
     * Set: rdoAgendarTituloRastProp.
     * 
     * @param rdoAgendarTituloRastProp
     *            the rdo agendar titulo rast prop
     */
    public void setRdoAgendarTituloRastProp(String rdoAgendarTituloRastProp) {
        this.rdoAgendarTituloRastProp = rdoAgendarTituloRastProp;
    }

    /**
     * Get: rdoAgendarTituloRastreadoFilial.
     * 
     * @return rdoAgendarTituloRastreadoFilial
     */
    public String getRdoAgendarTituloRastreadoFilial() {
        return rdoAgendarTituloRastreadoFilial;
    }

    /**
     * Set: rdoAgendarTituloRastreadoFilial.
     * 
     * @param rdoAgendarTituloRastreadoFilial
     *            the rdo agendar titulo rastreado filial
     */
    public void setRdoAgendarTituloRastreadoFilial(
        String rdoAgendarTituloRastreadoFilial) {
        this.rdoAgendarTituloRastreadoFilial = rdoAgendarTituloRastreadoFilial;
    }

    /**
     * Get: rdoAgruparCorresp.
     * 
     * @return rdoAgruparCorresp
     */
    public String getRdoAgruparCorresp() {
        return rdoAgruparCorresp;
    }

    /**
     * Set: rdoAgruparCorresp.
     * 
     * @param rdoAgruparCorresp
     *            the rdo agrupar corresp
     */
    public void setRdoAgruparCorresp(String rdoAgruparCorresp) {
        this.rdoAgruparCorresp = rdoAgruparCorresp;
    }

    /**
     * Get: rdoAutComplementarAg.
     * 
     * @return rdoAutComplementarAg
     */
    public String getRdoAutComplementarAg() {
        return rdoAutComplementarAg;
    }

    /**
     * Set: rdoAutComplementarAg.
     * 
     * @param rdoAutComplementarAg
     *            the rdo aut complementar ag
     */
    public void setRdoAutComplementarAg(String rdoAutComplementarAg) {
        this.rdoAutComplementarAg = rdoAutComplementarAg;
    }

    /**
     * Get: rdoBloqEmissaoPapeleta.
     * 
     * @return rdoBloqEmissaoPapeleta
     */
    public String getRdoBloqEmissaoPapeleta() {
        return rdoBloqEmissaoPapeleta;
    }

    /**
     * Set: rdoBloqEmissaoPapeleta.
     * 
     * @param rdoBloqEmissaoPapeleta
     *            the rdo bloq emissao papeleta
     */
    public void setRdoBloqEmissaoPapeleta(String rdoBloqEmissaoPapeleta) {
        this.rdoBloqEmissaoPapeleta = rdoBloqEmissaoPapeleta;
    }

    /**
     * Get: rdoCapTitulosDtReg.
     * 
     * @return rdoCapTitulosDtReg
     */
    public String getRdoCapTitulosDtReg() {
        return rdoCapTitulosDtReg;
    }

    /**
     * Set: rdoCapTitulosDtReg.
     * 
     * @param rdoCapTitulosDtReg
     *            the rdo cap titulos dt reg
     */
    public void setRdoCapTitulosDtReg(String rdoCapTitulosDtReg) {
        this.rdoCapTitulosDtReg = rdoCapTitulosDtReg;
    }

    /**
     * Get: rdoDemonstraInfAreaRes.
     * 
     * @return rdoDemonstraInfAreaRes
     */
    public String getRdoDemonstraInfAreaRes() {
        return rdoDemonstraInfAreaRes;
    }

    /**
     * Set: rdoDemonstraInfAreaRes.
     * 
     * @param rdoDemonstraInfAreaRes
     *            the rdo demonstra inf area res
     */
    public void setRdoDemonstraInfAreaRes(String rdoDemonstraInfAreaRes) {
        this.rdoDemonstraInfAreaRes = rdoDemonstraInfAreaRes;
    }

    /**
     * Get: rdoEfetuaConEspBenef.
     * 
     * @return rdoEfetuaConEspBenef
     */
    public String getRdoEfetuaConEspBenef() {
        return rdoEfetuaConEspBenef;
    }

    /**
     * Set: rdoEfetuaConEspBenef.
     * 
     * @param rdoEfetuaConEspBenef
     *            the rdo efetua con esp benef
     */
    public void setRdoEfetuaConEspBenef(String rdoEfetuaConEspBenef) {
        this.rdoEfetuaConEspBenef = rdoEfetuaConEspBenef;
    }

    /**
     * Get: rdoEmissaoAntCartaoContSal.
     * 
     * @return rdoEmissaoAntCartaoContSal
     */
    public String getRdoEmissaoAntCartaoContSal() {
        return rdoEmissaoAntCartaoContSal;
    }

    /**
     * Set: rdoEmissaoAntCartaoContSal.
     * 
     * @param rdoEmissaoAntCartaoContSal
     *            the rdo emissao ant cartao cont sal
     */
    public void setRdoEmissaoAntCartaoContSal(String rdoEmissaoAntCartaoContSal) {
        this.rdoEmissaoAntCartaoContSal = rdoEmissaoAntCartaoContSal;
    }

    /**
     * Get: rdoEmiteAvitoCompVida.
     * 
     * @return rdoEmiteAvitoCompVida
     */
    public String getRdoEmiteAvitoCompVida() {
        return rdoEmiteAvitoCompVida;
    }

    /**
     * Set: rdoEmiteAvitoCompVida.
     * 
     * @param rdoEmiteAvitoCompVida
     *            the rdo emite avito comp vida
     */
    public void setRdoEmiteAvitoCompVida(String rdoEmiteAvitoCompVida) {
        this.rdoEmiteAvitoCompVida = rdoEmiteAvitoCompVida;
    }

    /**
     * Get: rdoEmiteMsgRecadMidia.
     * 
     * @return rdoEmiteMsgRecadMidia
     */
    public String getRdoEmiteMsgRecadMidia() {
        return rdoEmiteMsgRecadMidia;
    }

    /**
     * Set: rdoEmiteMsgRecadMidia.
     * 
     * @param rdoEmiteMsgRecadMidia
     *            the rdo emite msg recad midia
     */
    public void setRdoEmiteMsgRecadMidia(String rdoEmiteMsgRecadMidia) {
        this.rdoEmiteMsgRecadMidia = rdoEmiteMsgRecadMidia;
    }

    /**
     * Get: rdoExigeLibLoteProcessado.
     * 
     * @return rdoExigeLibLoteProcessado
     */
    public String getRdoExigeLibLoteProcessado() {
        return rdoExigeLibLoteProcessado;
    }

    /**
     * Set: rdoExigeLibLoteProcessado.
     * 
     * @param rdoExigeLibLoteProcessado
     *            the rdo exige lib lote processado
     */
    public void setRdoExigeLibLoteProcessado(String rdoExigeLibLoteProcessado) {
        this.rdoExigeLibLoteProcessado = rdoExigeLibLoteProcessado;
    }

    /**
     * Get: rdoGerarLancFutCred.
     * 
     * @return rdoGerarLancFutCred
     */
    public String getRdoGerarLancFutCred() {
        return rdoGerarLancFutCred;
    }

    /**
     * Set: rdoGerarLancFutCred.
     * 
     * @param rdoGerarLancFutCred
     *            the rdo gerar lanc fut cred
     */
    public void setRdoGerarLancFutCred(String rdoGerarLancFutCred) {
        this.rdoGerarLancFutCred = rdoGerarLancFutCred;
    }

    /**
     * Get: rdoGerarLancFutDeb.
     * 
     * @return rdoGerarLancFutDeb
     */
    public String getRdoGerarLancFutDeb() {
        return rdoGerarLancFutDeb;
    }

    /**
     * Set: rdoGerarLancFutDeb.
     * 
     * @param rdoGerarLancFutDeb
     *            the rdo gerar lanc fut deb
     */
    public void setRdoGerarLancFutDeb(String rdoGerarLancFutDeb) {
        this.rdoGerarLancFutDeb = rdoGerarLancFutDeb;
    }

    /**
     * Get: rdoGerarRetornoOperRealizadaInternet.
     * 
     * @return rdoGerarRetornoOperRealizadaInternet
     */
    public String getRdoGerarRetornoOperRealizadaInternet() {
        return rdoGerarRetornoOperRealizadaInternet;
    }

    /**
     * Set: rdoGerarRetornoOperRealizadaInternet.
     * 
     * @param rdoGerarRetornoOperRealizadaInternet
     *            the rdo gerar retorno oper realizada internet
     */
    public void setRdoGerarRetornoOperRealizadaInternet(
        String rdoGerarRetornoOperRealizadaInternet) {
        this.rdoGerarRetornoOperRealizadaInternet = rdoGerarRetornoOperRealizadaInternet;
    }

    /**
     * Get: rdoInformaAgenciaContaCred.
     * 
     * @return rdoInformaAgenciaContaCred
     */
    public String getRdoInformaAgenciaContaCred() {
        return rdoInformaAgenciaContaCred;
    }

    /**
     * Set: rdoInformaAgenciaContaCred.
     * 
     * @param rdoInformaAgenciaContaCred
     *            the rdo informa agencia conta cred
     */
    public void setRdoInformaAgenciaContaCred(String rdoInformaAgenciaContaCred) {
        this.rdoInformaAgenciaContaCred = rdoInformaAgenciaContaCred;
    }

    /**
     * Get: rdoPermiteAcertosDados.
     * 
     * @return rdoPermiteAcertosDados
     */
    public String getRdoPermiteAcertosDados() {
        return rdoPermiteAcertosDados;
    }

    /**
     * Set: rdoPermiteAcertosDados.
     * 
     * @param rdoPermiteAcertosDados
     *            the rdo permite acertos dados
     */
    public void setRdoPermiteAcertosDados(String rdoPermiteAcertosDados) {
        this.rdoPermiteAcertosDados = rdoPermiteAcertosDados;
    }

    /**
     * Get: rdoPermiteAnteciparRecadastramento.
     * 
     * @return rdoPermiteAnteciparRecadastramento
     */
    public String getRdoPermiteAnteciparRecadastramento() {
        return rdoPermiteAnteciparRecadastramento;
    }

    /**
     * Set: rdoPermiteAnteciparRecadastramento.
     * 
     * @param rdoPermiteAnteciparRecadastramento
     *            the rdo permite antecipar recadastramento
     */
    public void setRdoPermiteAnteciparRecadastramento(
        String rdoPermiteAnteciparRecadastramento) {
        this.rdoPermiteAnteciparRecadastramento = rdoPermiteAnteciparRecadastramento;
    }

    /**
     * Get: rdoPermiteContigenciaPagamento.
     * 
     * @return rdoPermiteContigenciaPagamento
     */
    public String getRdoPermiteContigenciaPagamento() {
        return rdoPermiteContigenciaPagamento;
    }

    /**
     * Set: rdoPermiteContigenciaPagamento.
     * 
     * @param rdoPermiteContigenciaPagamento
     *            the rdo permite contigencia pagamento
     */
    public void setRdoPermiteContigenciaPagamento(
        String rdoPermiteContigenciaPagamento) {
        this.rdoPermiteContigenciaPagamento = rdoPermiteContigenciaPagamento;
    }

    /**
     * Get: rdoPermiteDebitoOnline.
     * 
     * @return rdoPermiteDebitoOnline
     */
    public String getRdoPermiteDebitoOnline() {
        return rdoPermiteDebitoOnline;
    }

    /**
     * Set: rdoPermiteDebitoOnline.
     * 
     * @param rdoPermiteDebitoOnline
     *            the rdo permite debito online
     */
    public void setRdoPermiteDebitoOnline(String rdoPermiteDebitoOnline) {
        this.rdoPermiteDebitoOnline = rdoPermiteDebitoOnline;
    }

    /**
     * Get: rdoPermiteEnvioRemessaManutencaoCadastro.
     * 
     * @return rdoPermiteEnvioRemessaManutencaoCadastro
     */
    public String getRdoPermiteEnvioRemessaManutencaoCadastro() {
        return rdoPermiteEnvioRemessaManutencaoCadastro;
    }

    /**
     * Set: rdoPermiteEnvioRemessaManutencaoCadastro.
     * 
     * @param rdoPermiteEnvioRemessaManutencaoCadastro
     *            the rdo permite envio remessa manutencao cadastro
     */
    public void setRdoPermiteEnvioRemessaManutencaoCadastro(
        String rdoPermiteEnvioRemessaManutencaoCadastro) {
        this.rdoPermiteEnvioRemessaManutencaoCadastro = rdoPermiteEnvioRemessaManutencaoCadastro;
    }

    /**
     * Get: rdoPermiteFavorecidoConsultarPagamento.
     * 
     * @return rdoPermiteFavorecidoConsultarPagamento
     */
    public String getRdoPermiteFavorecidoConsultarPagamento() {
        return rdoPermiteFavorecidoConsultarPagamento;
    }

    /**
     * Set: rdoPermiteFavorecidoConsultarPagamento.
     * 
     * @param rdoPermiteFavorecidoConsultarPagamento
     *            the rdo permite favorecido consultar pagamento
     */
    public void setRdoPermiteFavorecidoConsultarPagamento(
        String rdoPermiteFavorecidoConsultarPagamento) {
        this.rdoPermiteFavorecidoConsultarPagamento = rdoPermiteFavorecidoConsultarPagamento;
    }

    /**
     * Get: rdoPermitePagarMenor.
     * 
     * @return rdoPermitePagarMenor
     */
    public String getRdoPermitePagarMenor() {
        return rdoPermitePagarMenor;
    }

    /**
     * Set: rdoPermitePagarMenor.
     * 
     * @param rdoPermitePagarMenor
     *            the rdo permite pagar menor
     */
    public void setRdoPermitePagarMenor(String rdoPermitePagarMenor) {
        this.rdoPermitePagarMenor = rdoPermitePagarMenor;
    }

    /**
     * Get: rdoPermitePagarVencido.
     * 
     * @return rdoPermitePagarVencido
     */
    public String getRdoPermitePagarVencido() {
        return rdoPermitePagarVencido;
    }

    /**
     * Set: rdoPermitePagarVencido.
     * 
     * @param rdoPermitePagarVencido
     *            the rdo permite pagar vencido
     */
    public void setRdoPermitePagarVencido(String rdoPermitePagarVencido) {
        this.rdoPermitePagarVencido = rdoPermitePagarVencido;
    }

    /**
     * Get: rdoPermitirAgrupamentoCorrespondencia.
     * 
     * @return rdoPermitirAgrupamentoCorrespondencia
     */
    public String getRdoPermitirAgrupamentoCorrespondencia() {
        return rdoPermitirAgrupamentoCorrespondencia;
    }

    /**
     * Set: rdoPermitirAgrupamentoCorrespondencia.
     * 
     * @param rdoPermitirAgrupamentoCorrespondencia
     *            the rdo permitir agrupamento correspondencia
     */
    public void setRdoPermitirAgrupamentoCorrespondencia(
        String rdoPermitirAgrupamentoCorrespondencia) {
        this.rdoPermitirAgrupamentoCorrespondencia = rdoPermitirAgrupamentoCorrespondencia;
    }

    /**
     * Get: rdoPesquisaDe.
     * 
     * @return rdoPesquisaDe
     */
    public String getRdoPesquisaDe() {
        return rdoPesquisaDe;
    }

    /**
     * Set: rdoPesquisaDe.
     * 
     * @param rdoPesquisaDe
     *            the rdo pesquisa de
     */
    public void setRdoPesquisaDe(String rdoPesquisaDe) {
        this.rdoPesquisaDe = rdoPesquisaDe;
    }

    /**
     * Get: rdoPossuiExpiracaoCredito.
     * 
     * @return rdoPossuiExpiracaoCredito
     */
    public String getRdoPossuiExpiracaoCredito() {
        return rdoPossuiExpiracaoCredito;
    }

    /**
     * Set: rdoPossuiExpiracaoCredito.
     * 
     * @param rdoPossuiExpiracaoCredito
     *            the rdo possui expiracao credito
     */
    public void setRdoPossuiExpiracaoCredito(String rdoPossuiExpiracaoCredito) {
        this.rdoPossuiExpiracaoCredito = rdoPossuiExpiracaoCredito;
    }

    /**
     * Get: rdoPreAutorizacaoCliente.
     * 
     * @return rdoPreAutorizacaoCliente
     */
    public String getRdoPreAutorizacaoCliente() {
        return rdoPreAutorizacaoCliente;
    }

    /**
     * Set: rdoPreAutorizacaoCliente.
     * 
     * @param rdoPreAutorizacaoCliente
     *            the rdo pre autorizacao cliente
     */
    public void setRdoPreAutorizacaoCliente(String rdoPreAutorizacaoCliente) {
        this.rdoPreAutorizacaoCliente = rdoPreAutorizacaoCliente;
    }

    /**
     * Get: rdoRastrearNotasFiscais.
     * 
     * @return rdoRastrearNotasFiscais
     */
    public String getRdoRastrearNotasFiscais() {
        return rdoRastrearNotasFiscais;
    }

    /**
     * Set: rdoRastrearNotasFiscais.
     * 
     * @param rdoRastrearNotasFiscais
     *            the rdo rastrear notas fiscais
     */
    public void setRdoRastrearNotasFiscais(String rdoRastrearNotasFiscais) {
        this.rdoRastrearNotasFiscais = rdoRastrearNotasFiscais;
    }

    /**
     * Get: rdoRastrearTituloTerceiros.
     * 
     * @return rdoRastrearTituloTerceiros
     */
    public String getRdoRastrearTituloTerceiros() {
        return rdoRastrearTituloTerceiros;
    }

    /**
     * Set: rdoRastrearTituloTerceiros.
     * 
     * @param rdoRastrearTituloTerceiros
     *            the rdo rastrear titulo terceiros
     */
    public void setRdoRastrearTituloTerceiros(String rdoRastrearTituloTerceiros) {
        this.rdoRastrearTituloTerceiros = rdoRastrearTituloTerceiros;
    }

    /**
     * Get: rdoUtilizaCadastroFavorecidoControlePagamentos.
     * 
     * @return rdoUtilizaCadastroFavorecidoControlePagamentos
     */
    public String getRdoUtilizaCadastroFavorecidoControlePagamentos() {
        return rdoUtilizaCadastroFavorecidoControlePagamentos;
    }

    /**
     * Set: rdoUtilizaCadastroFavorecidoControlePagamentos.
     * 
     * @param rdoUtilizaCadastroFavorecidoControlePagamentos
     *            the rdo utiliza cadastro favorecido controle pagamentos
     */
    public void setRdoUtilizaCadastroFavorecidoControlePagamentos(
        String rdoUtilizaCadastroFavorecidoControlePagamentos) {
        this.rdoUtilizaCadastroFavorecidoControlePagamentos = rdoUtilizaCadastroFavorecidoControlePagamentos;
    }

    /**
     * Get: rdoUtilizaCadastroOrgaosPagadores.
     * 
     * @return rdoUtilizaCadastroOrgaosPagadores
     */
    public String getRdoUtilizaCadastroOrgaosPagadores() {
        return rdoUtilizaCadastroOrgaosPagadores;
    }

    /**
     * Set: rdoUtilizaCadastroOrgaosPagadores.
     * 
     * @param rdoUtilizaCadastroOrgaosPagadores
     *            the rdo utiliza cadastro orgaos pagadores
     */
    public void setRdoUtilizaCadastroOrgaosPagadores(
        String rdoUtilizaCadastroOrgaosPagadores) {
        this.rdoUtilizaCadastroOrgaosPagadores = rdoUtilizaCadastroOrgaosPagadores;
    }

    /**
     * Get: rdoUtilizaCadastroProcuradores.
     * 
     * @return rdoUtilizaCadastroProcuradores
     */
    public String getRdoUtilizaCadastroProcuradores() {
        return rdoUtilizaCadastroProcuradores;
    }

    /**
     * Set: rdoUtilizaCadastroProcuradores.
     * 
     * @param rdoUtilizaCadastroProcuradores
     *            the rdo utiliza cadastro procuradores
     */
    public void setRdoUtilizaCadastroProcuradores(
        String rdoUtilizaCadastroProcuradores) {
        this.rdoUtilizaCadastroProcuradores = rdoUtilizaCadastroProcuradores;
    }

    /**
     * Get: rdoUtilizaFrasesPreCadastradas.
     * 
     * @return rdoUtilizaFrasesPreCadastradas
     */
    public String getRdoUtilizaFrasesPreCadastradas() {
        return rdoUtilizaFrasesPreCadastradas;
    }

    /**
     * Set: rdoUtilizaFrasesPreCadastradas.
     * 
     * @param rdoUtilizaFrasesPreCadastradas
     *            the rdo utiliza frases pre cadastradas
     */
    public void setRdoUtilizaFrasesPreCadastradas(
        String rdoUtilizaFrasesPreCadastradas) {
        this.rdoUtilizaFrasesPreCadastradas = rdoUtilizaFrasesPreCadastradas;
    }

    /**
     * Get: rdoUtilizaListaDebitos.
     * 
     * @return rdoUtilizaListaDebitos
     */
    public String getRdoUtilizaListaDebitos() {
        return rdoUtilizaListaDebitos;
    }

    /**
     * Set: rdoUtilizaListaDebitos.
     * 
     * @param rdoUtilizaListaDebitos
     *            the rdo utiliza lista debitos
     */
    public void setRdoUtilizaListaDebitos(String rdoUtilizaListaDebitos) {
        this.rdoUtilizaListaDebitos = rdoUtilizaListaDebitos;
    }

    /**
     * Get: rdoUtilizaMensagemPersonalizada.
     * 
     * @return rdoUtilizaMensagemPersonalizada
     */
    public String getRdoUtilizaMensagemPersonalizada() {
        return rdoUtilizaMensagemPersonalizada;
    }

    /**
     * Set: rdoUtilizaMensagemPersonalizada.
     * 
     * @param rdoUtilizaMensagemPersonalizada
     *            the rdo utiliza mensagem personalizada
     */
    public void setRdoUtilizaMensagemPersonalizada(
        String rdoUtilizaMensagemPersonalizada) {
        this.rdoUtilizaMensagemPersonalizada = rdoUtilizaMensagemPersonalizada;
    }

    /**
     * Get: rdoValidarNomeFavorecidoReceitaFederal.
     * 
     * @return rdoValidarNomeFavorecidoReceitaFederal
     */
    public String getRdoValidarNomeFavorecidoReceitaFederal() {
        return rdoValidarNomeFavorecidoReceitaFederal;
    }

    /**
     * Set: rdoValidarNomeFavorecidoReceitaFederal.
     * 
     * @param rdoValidarNomeFavorecidoReceitaFederal
     *            the rdo validar nome favorecido receita federal
     */
    public void setRdoValidarNomeFavorecidoReceitaFederal(
        String rdoValidarNomeFavorecidoReceitaFederal) {
        this.rdoValidarNomeFavorecidoReceitaFederal = rdoValidarNomeFavorecidoReceitaFederal;
    }

    /**
     * Get: textoInformacaoAreaReservada.
     * 
     * @return textoInformacaoAreaReservada
     */
    public String getTextoInformacaoAreaReservada() {
        return textoInformacaoAreaReservada;
    }

    /**
     * Set: textoInformacaoAreaReservada.
     * 
     * @param textoInformacaoAreaReservada
     *            the texto informacao area reservada
     */
    public void setTextoInformacaoAreaReservada(
        String textoInformacaoAreaReservada) {
        this.textoInformacaoAreaReservada = textoInformacaoAreaReservada;
    }

    /**
     * Get: valorLimiteDiario.
     * 
     * @return valorLimiteDiario
     */
    public BigDecimal getValorLimiteDiario() {
        return valorLimiteDiario;
    }

    /**
     * Set: valorLimiteDiario.
     * 
     * @param valorLimiteDiario
     *            the valor limite diario
     */
    public void setValorLimiteDiario(BigDecimal valorLimiteDiario) {
        this.valorLimiteDiario = valorLimiteDiario;
    }

    /**
     * Get: valorLimiteIndividual.
     * 
     * @return valorLimiteIndividual
     */
    public BigDecimal getValorLimiteIndividual() {
        return valorLimiteIndividual;
    }

    /**
     * Set: valorLimiteIndividual.
     * 
     * @param valorLimiteIndividual
     *            the valor limite individual
     */
    public void setValorLimiteIndividual(BigDecimal valorLimiteIndividual) {
        this.valorLimiteIndividual = valorLimiteIndividual;
    }

    /**
     * Get: valorMaximoPagamentoFavorecidoNaoCadastrado.
     * 
     * @return valorMaximoPagamentoFavorecidoNaoCadastrado
     */
    public BigDecimal getValorMaximoPagamentoFavorecidoNaoCadastrado() {
        return valorMaximoPagamentoFavorecidoNaoCadastrado;
    }

    /**
     * Set: valorMaximoPagamentoFavorecidoNaoCadastrado.
     * 
     * @param valorMaximoPagamentoFavorecidoNaoCadastrado
     *            the valor maximo pagamento favorecido nao cadastrado
     */
    public void setValorMaximoPagamentoFavorecidoNaoCadastrado(
        BigDecimal valorMaximoPagamentoFavorecidoNaoCadastrado) {
        this.valorMaximoPagamentoFavorecidoNaoCadastrado = valorMaximoPagamentoFavorecidoNaoCadastrado;
    }

    /**
     * Get: rdoGerarLancProgramado.
     * 
     * @return rdoGerarLancProgramado
     */
    public String getRdoGerarLancProgramado() {
        return rdoGerarLancProgramado;
    }

    /**
     * Set: rdoGerarLancProgramado.
     * 
     * @param rdoGerarLancProgramado
     *            the rdo gerar lanc programado
     */
    public void setRdoGerarLancProgramado(String rdoGerarLancProgramado) {
        this.rdoGerarLancProgramado = rdoGerarLancProgramado;
    }

    /**
     * Set lista tratamento feriados data pagamento hash.
     * 
     * @param listaTratamentoFeriadosDataPagamentoHash
     *            the lista tratamento feriados data pagamento hash
     */
    public void setListaTratamentoFeriadosDataPagamentoHash(
        Map<Long, String> listaTratamentoFeriadosDataPagamentoHash) {
        this.listaTratamentoFeriadosDataPagamentoHash = listaTratamentoFeriadosDataPagamentoHash;
    }

    /**
     * Get: cboAcaoParaNaoComprovacao.
     * 
     * @return cboAcaoParaNaoComprovacao
     */
    public Long getCboAcaoParaNaoComprovacao() {
        return cboAcaoParaNaoComprovacao;
    }

    /**
     * Set: cboAcaoParaNaoComprovacao.
     * 
     * @param cboAcaoParaNaoComprovacao
     *            the cbo acao para nao comprovacao
     */
    public void setCboAcaoParaNaoComprovacao(Long cboAcaoParaNaoComprovacao) {
        this.cboAcaoParaNaoComprovacao = cboAcaoParaNaoComprovacao;
    }

    /**
     * Get: cboAgendamentoDebitosPendentesVeiculos.
     * 
     * @return cboAgendamentoDebitosPendentesVeiculos
     */
    public Long getCboAgendamentoDebitosPendentesVeiculos() {
        return cboAgendamentoDebitosPendentesVeiculos;
    }

    /**
     * Set: cboAgendamentoDebitosPendentesVeiculos.
     * 
     * @param cboAgendamentoDebitosPendentesVeiculos
     *            the cbo agendamento debitos pendentes veiculos
     */
    public void setCboAgendamentoDebitosPendentesVeiculos(
        Long cboAgendamentoDebitosPendentesVeiculos) {
        this.cboAgendamentoDebitosPendentesVeiculos = cboAgendamentoDebitosPendentesVeiculos;
    }

    /**
     * Get: cboCadConsultaEndEnvio.
     * 
     * @return cboCadConsultaEndEnvio
     */
    public Long getCboCadConsultaEndEnvio() {
        return cboCadConsultaEndEnvio;
    }

    /**
     * Set: cboCadConsultaEndEnvio.
     * 
     * @param cboCadConsultaEndEnvio
     *            the cbo cad consulta end envio
     */
    public void setCboCadConsultaEndEnvio(Long cboCadConsultaEndEnvio) {
        this.cboCadConsultaEndEnvio = cboCadConsultaEndEnvio;
    }

    /**
     * Get: cboCadUtilizadoRec.
     * 
     * @return cboCadUtilizadoRec
     */
    public Long getCboCadUtilizadoRec() {
        return cboCadUtilizadoRec;
    }

    /**
     * Set: cboCadUtilizadoRec.
     * 
     * @param cboCadUtilizadoRec
     *            the cbo cad utilizado rec
     */
    public void setCboCadUtilizadoRec(Long cboCadUtilizadoRec) {
        this.cboCadUtilizadoRec = cboCadUtilizadoRec;
    }

    /**
     * Get: cboCondicaoEnquadramento.
     * 
     * @return cboCondicaoEnquadramento
     */
    public Long getCboCondicaoEnquadramento() {
        return cboCondicaoEnquadramento;
    }

    /**
     * Set: cboCondicaoEnquadramento.
     * 
     * @param cboCondicaoEnquadramento
     *            the cbo condicao enquadramento
     */
    public void setCboCondicaoEnquadramento(Long cboCondicaoEnquadramento) {
        this.cboCondicaoEnquadramento = cboCondicaoEnquadramento;
    }

    /**
     * Get: cboContExpCredConta.
     * 
     * @return cboContExpCredConta
     */
    public Long getCboContExpCredConta() {
        return cboContExpCredConta;
    }

    /**
     * Set: cboContExpCredConta.
     * 
     * @param cboContExpCredConta
     *            the cbo cont exp cred conta
     */
    public void setCboContExpCredConta(Long cboContExpCredConta) {
        this.cboContExpCredConta = cboContExpCredConta;
    }

    /**
     * Get: cboCritCompEnquadramento.
     * 
     * @return cboCritCompEnquadramento
     */
    public Long getCboCritCompEnquadramento() {
        return cboCritCompEnquadramento;
    }

    /**
     * Set: cboCritCompEnquadramento.
     * 
     * @param cboCritCompEnquadramento
     *            the cbo crit comp enquadramento
     */
    public void setCboCritCompEnquadramento(Long cboCritCompEnquadramento) {
        this.cboCritCompEnquadramento = cboCritCompEnquadramento;
    }

    /**
     * Get: cboCritPrincEnquadramento.
     * 
     * @return cboCritPrincEnquadramento
     */
    public Long getCboCritPrincEnquadramento() {
        return cboCritPrincEnquadramento;
    }

    /**
     * Set: cboCritPrincEnquadramento.
     * 
     * @param cboCritPrincEnquadramento
     *            the cbo crit princ enquadramento
     */
    public void setCboCritPrincEnquadramento(Long cboCritPrincEnquadramento) {
        this.cboCritPrincEnquadramento = cboCritPrincEnquadramento;
    }

    /**
     * Get: cboDestinoEnvio.
     * 
     * @return cboDestinoEnvio
     */
    public Long getCboDestinoEnvio() {
        return cboDestinoEnvio;
    }

    /**
     * Set: cboDestinoEnvio.
     * 
     * @param cboDestinoEnvio
     *            the cbo destino envio
     */
    public void setCboDestinoEnvio(Long cboDestinoEnvio) {
        this.cboDestinoEnvio = cboDestinoEnvio;
    }

    /**
     * Get: cboDestinoEnvioCorresp.
     * 
     * @return cboDestinoEnvioCorresp
     */
    public Long getCboDestinoEnvioCorresp() {
        return cboDestinoEnvioCorresp;
    }

    /**
     * Set: cboDestinoEnvioCorresp.
     * 
     * @param cboDestinoEnvioCorresp
     *            the cbo destino envio corresp
     */
    public void setCboDestinoEnvioCorresp(Long cboDestinoEnvioCorresp) {
        this.cboDestinoEnvioCorresp = cboDestinoEnvioCorresp;
    }

    /**
     * Get: cboFormaAutPagamentos.
     * 
     * @return cboFormaAutPagamentos
     */
    public Long getCboFormaAutPagamentos() {
        return cboFormaAutPagamentos;
    }

    /**
     * Set: cboFormaAutPagamentos.
     * 
     * @param cboFormaAutPagamentos
     *            the cbo forma aut pagamentos
     */
    public void setCboFormaAutPagamentos(Long cboFormaAutPagamentos) {
        this.cboFormaAutPagamentos = cboFormaAutPagamentos;
    }

    /**
     * Get: cboFormaEnvioPagamento.
     * 
     * @return cboFormaEnvioPagamento
     */
    public Long getCboFormaEnvioPagamento() {
        return cboFormaEnvioPagamento;
    }

    /**
     * Set: cboFormaEnvioPagamento.
     * 
     * @param cboFormaEnvioPagamento
     *            the cbo forma envio pagamento
     */
    public void setCboFormaEnvioPagamento(Long cboFormaEnvioPagamento) {
        this.cboFormaEnvioPagamento = cboFormaEnvioPagamento;
    }

    /**
     * Get: cboFormaManCadFavorecido.
     * 
     * @return cboFormaManCadFavorecido
     */
    public Long getCboFormaManCadFavorecido() {
        return cboFormaManCadFavorecido;
    }

    /**
     * Set: cboFormaManCadFavorecido.
     * 
     * @param cboFormaManCadFavorecido
     *            the cbo forma man cad favorecido
     */
    public void setCboFormaManCadFavorecido(Long cboFormaManCadFavorecido) {
        this.cboFormaManCadFavorecido = cboFormaManCadFavorecido;
    }

    /**
     * Get: cboFormaManCadProcurador.
     * 
     * @return cboFormaManCadProcurador
     */
    public Long getCboFormaManCadProcurador() {
        return cboFormaManCadProcurador;
    }

    /**
     * Set: cboFormaManCadProcurador.
     * 
     * @param cboFormaManCadProcurador
     *            the cbo forma man cad procurador
     */
    public void setCboFormaManCadProcurador(Long cboFormaManCadProcurador) {
        this.cboFormaManCadProcurador = cboFormaManCadProcurador;
    }

    /**
     * Get: cboMidiaDispCorrentista.
     * 
     * @return cboMidiaDispCorrentista
     */
    public Long getCboMidiaDispCorrentista() {
        return cboMidiaDispCorrentista;
    }

    /**
     * Set: cboMidiaDispCorrentista.
     * 
     * @param cboMidiaDispCorrentista
     *            the cbo midia disp correntista
     */
    public void setCboMidiaDispCorrentista(Long cboMidiaDispCorrentista) {
        this.cboMidiaDispCorrentista = cboMidiaDispCorrentista;
    }

    /**
     * Get: cboMidiaMsgRecad.
     * 
     * @return cboMidiaMsgRecad
     */
    public Long getCboMidiaMsgRecad() {
        return cboMidiaMsgRecad;
    }

    /**
     * Set: cboMidiaMsgRecad.
     * 
     * @param cboMidiaMsgRecad
     *            the cbo midia msg recad
     */
    public void setCboMidiaMsgRecad(Long cboMidiaMsgRecad) {
        this.cboMidiaMsgRecad = cboMidiaMsgRecad;
    }

    /**
     * Get: cboMomentoEnvio.
     * 
     * @return cboMomentoEnvio
     */
    public Long getCboMomentoEnvio() {
        return cboMomentoEnvio;
    }

    /**
     * Set: cboMomentoEnvio.
     * 
     * @param cboMomentoEnvio
     *            the cbo momento envio
     */
    public void setCboMomentoEnvio(Long cboMomentoEnvio) {
        this.cboMomentoEnvio = cboMomentoEnvio;
    }

    /**
     * Get: cboMomentoIndCredEfetivado.
     * 
     * @return cboMomentoIndCredEfetivado
     */
    public Long getCboMomentoIndCredEfetivado() {
        return cboMomentoIndCredEfetivado;
    }

    /**
     * Set: cboMomentoIndCredEfetivado.
     * 
     * @param cboMomentoIndCredEfetivado
     *            the cbo momento ind cred efetivado
     */
    public void setCboMomentoIndCredEfetivado(Long cboMomentoIndCredEfetivado) {
        this.cboMomentoIndCredEfetivado = cboMomentoIndCredEfetivado;
    }

    /**
     * Get: cboPeriocidadeManCadProc.
     * 
     * @return cboPeriocidadeManCadProc
     */
    public Integer getCboPeriocidadeManCadProc() {
        return cboPeriocidadeManCadProc;
    }

    /**
     * Set: cboPeriocidadeManCadProc.
     * 
     * @param cboPeriocidadeManCadProc
     *            the cbo periocidade man cad proc
     */
    public void setCboPeriocidadeManCadProc(Integer cboPeriocidadeManCadProc) {
        this.cboPeriocidadeManCadProc = cboPeriocidadeManCadProc;
    }

    /**
     * Get: cboPeriodicidadeEnvioRemessaManutencao.
     * 
     * @return cboPeriodicidadeEnvioRemessaManutencao
     */
    public Integer getCboPeriodicidadeEnvioRemessaManutencao() {
        return cboPeriodicidadeEnvioRemessaManutencao;
    }

    /**
     * Set: cboPeriodicidadeEnvioRemessaManutencao.
     * 
     * @param cboPeriodicidadeEnvioRemessaManutencao
     *            the cbo periodicidade envio remessa manutencao
     */
    public void setCboPeriodicidadeEnvioRemessaManutencao(
        Integer cboPeriodicidadeEnvioRemessaManutencao) {
        this.cboPeriodicidadeEnvioRemessaManutencao = cboPeriodicidadeEnvioRemessaManutencao;
    }

    /**
     * Get: cboPeriodicidadePesquisaDebitosPendentesVeiculos.
     * 
     * @return cboPeriodicidadePesquisaDebitosPendentesVeiculos
     */
    public Integer getCboPeriodicidadePesquisaDebitosPendentesVeiculos() {
        return cboPeriodicidadePesquisaDebitosPendentesVeiculos;
    }

    /**
     * Set: cboPeriodicidadePesquisaDebitosPendentesVeiculos.
     * 
     * @param cboPeriodicidadePesquisaDebitosPendentesVeiculos
     *            the cbo periodicidade pesquisa debitos pendentes veiculos
     */
    public void setCboPeriodicidadePesquisaDebitosPendentesVeiculos(
        Integer cboPeriodicidadePesquisaDebitosPendentesVeiculos) {
        this.cboPeriodicidadePesquisaDebitosPendentesVeiculos = cboPeriodicidadePesquisaDebitosPendentesVeiculos;
    }

    /**
     * Get: cboPermiteEstornoPagamento.
     * 
     * @return cboPermiteEstornoPagamento
     */
    public Long getCboPermiteEstornoPagamento() {
        return cboPermiteEstornoPagamento;
    }

    /**
     * Set: cboPermiteEstornoPagamento.
     * 
     * @param cboPermiteEstornoPagamento
     *            the cbo permite estorno pagamento
     */
    public void setCboPermiteEstornoPagamento(Long cboPermiteEstornoPagamento) {
        this.cboPermiteEstornoPagamento = cboPermiteEstornoPagamento;
    }

    /**
     * Get: cboPrioridadeDebito.
     * 
     * @return cboPrioridadeDebito
     */
    public Long getCboPrioridadeDebito() {
        return cboPrioridadeDebito;
    }

    /**
     * Set: cboPrioridadeDebito.
     * 
     * @param cboPrioridadeDebito
     *            the cbo prioridade debito
     */
    public void setCboPrioridadeDebito(Long cboPrioridadeDebito) {
        this.cboPrioridadeDebito = cboPrioridadeDebito;
    }

    /**
     * Get: cboProcessamentoEfetivacaoPagamento.
     * 
     * @return cboProcessamentoEfetivacaoPagamento
     */
    public Long getCboProcessamentoEfetivacaoPagamento() {
        return cboProcessamentoEfetivacaoPagamento;
    }

    /**
     * Set: cboProcessamentoEfetivacaoPagamento.
     * 
     * @param cboProcessamentoEfetivacaoPagamento
     *            the cbo processamento efetivacao pagamento
     */
    public void setCboProcessamentoEfetivacaoPagamento(
        Long cboProcessamentoEfetivacaoPagamento) {
        this.cboProcessamentoEfetivacaoPagamento = cboProcessamentoEfetivacaoPagamento;
    }

    /**
     * Get: cboTipoCargaCadastroBeneficiario.
     * 
     * @return cboTipoCargaCadastroBeneficiario
     */
    public Long getCboTipoCargaCadastroBeneficiario() {
        return cboTipoCargaCadastroBeneficiario;
    }

    /**
     * Set: cboTipoCargaCadastroBeneficiario.
     * 
     * @param cboTipoCargaCadastroBeneficiario
     *            the cbo tipo carga cadastro beneficiario
     */
    public void setCboTipoCargaCadastroBeneficiario(
        Long cboTipoCargaCadastroBeneficiario) {
        this.cboTipoCargaCadastroBeneficiario = cboTipoCargaCadastroBeneficiario;
    }

    /**
     * Get: cboTipoCartaoContaSalario.
     * 
     * @return cboTipoCartaoContaSalario
     */
    public Long getCboTipoCartaoContaSalario() {
        return cboTipoCartaoContaSalario;
    }

    /**
     * Set: cboTipoCartaoContaSalario.
     * 
     * @param cboTipoCartaoContaSalario
     *            the cbo tipo cartao conta salario
     */
    public void setCboTipoCartaoContaSalario(Long cboTipoCartaoContaSalario) {
        this.cboTipoCartaoContaSalario = cboTipoCartaoContaSalario;
    }

    /**
     * Get: cboTipoConsistenciaCpfCnpjFavorecido.
     * 
     * @return cboTipoConsistenciaCpfCnpjFavorecido
     */
    public Long getCboTipoConsistenciaCpfCnpjFavorecido() {
        return cboTipoConsistenciaCpfCnpjFavorecido;
    }

    /**
     * Set: cboTipoConsistenciaCpfCnpjFavorecido.
     * 
     * @param cboTipoConsistenciaCpfCnpjFavorecido
     *            the cbo tipo consistencia cpf cnpj favorecido
     */
    public void setCboTipoConsistenciaCpfCnpjFavorecido(
        Long cboTipoConsistenciaCpfCnpjFavorecido) {
        this.cboTipoConsistenciaCpfCnpjFavorecido = cboTipoConsistenciaCpfCnpjFavorecido;
    }

    /**
     * Get: cboTipoConsistenciaCpfCnpjProprietario.
     * 
     * @return cboTipoConsistenciaCpfCnpjProprietario
     */
    public Long getCboTipoConsistenciaCpfCnpjProprietario() {
        return cboTipoConsistenciaCpfCnpjProprietario;
    }

    /**
     * Set: cboTipoConsistenciaCpfCnpjProprietario.
     * 
     * @param cboTipoConsistenciaCpfCnpjProprietario
     *            the cbo tipo consistencia cpf cnpj proprietario
     */
    public void setCboTipoConsistenciaCpfCnpjProprietario(
        Long cboTipoConsistenciaCpfCnpjProprietario) {
        this.cboTipoConsistenciaCpfCnpjProprietario = cboTipoConsistenciaCpfCnpjProprietario;
    }

    /**
     * Get: cboTipoConsistenciaIdentificacaoBeneficiario.
     * 
     * @return cboTipoConsistenciaIdentificacaoBeneficiario
     */
    public Long getCboTipoConsistenciaIdentificacaoBeneficiario() {
        return cboTipoConsistenciaIdentificacaoBeneficiario;
    }

    /**
     * Set: cboTipoConsistenciaIdentificacaoBeneficiario.
     * 
     * @param cboTipoConsistenciaIdentificacaoBeneficiario
     *            the cbo tipo consistencia identificacao beneficiario
     */
    public void setCboTipoConsistenciaIdentificacaoBeneficiario(
        Long cboTipoConsistenciaIdentificacaoBeneficiario) {
        this.cboTipoConsistenciaIdentificacaoBeneficiario = cboTipoConsistenciaIdentificacaoBeneficiario;
    }

    /**
     * Get: cboTipoConsistenciaInscricaoFavorecido.
     * 
     * @return cboTipoConsistenciaInscricaoFavorecido
     */
    public Long getCboTipoConsistenciaInscricaoFavorecido() {
        return cboTipoConsistenciaInscricaoFavorecido;
    }

    /**
     * Set: cboTipoConsistenciaInscricaoFavorecido.
     * 
     * @param cboTipoConsistenciaInscricaoFavorecido
     *            the cbo tipo consistencia inscricao favorecido
     */
    public void setCboTipoConsistenciaInscricaoFavorecido(
        Long cboTipoConsistenciaInscricaoFavorecido) {
        this.cboTipoConsistenciaInscricaoFavorecido = cboTipoConsistenciaInscricaoFavorecido;
    }

    /**
     * Get: cboTipoConsolidacaoPagamentosComprovante.
     * 
     * @return cboTipoConsolidacaoPagamentosComprovante
     */
    public Long getCboTipoConsolidacaoPagamentosComprovante() {
        return cboTipoConsolidacaoPagamentosComprovante;
    }

    /**
     * Set: cboTipoConsolidacaoPagamentosComprovante.
     * 
     * @param cboTipoConsolidacaoPagamentosComprovante
     *            the cbo tipo consolidacao pagamentos comprovante
     */
    public void setCboTipoConsolidacaoPagamentosComprovante(
        Long cboTipoConsolidacaoPagamentosComprovante) {
        this.cboTipoConsolidacaoPagamentosComprovante = cboTipoConsolidacaoPagamentosComprovante;
    }

    /**
     * Get: cboTipoConsultaSaldo.
     * 
     * @return cboTipoConsultaSaldo
     */
    public Long getCboTipoConsultaSaldo() {
        return cboTipoConsultaSaldo;
    }

    /**
     * Set: cboTipoConsultaSaldo.
     * 
     * @param cboTipoConsultaSaldo
     *            the cbo tipo consulta saldo
     */
    public void setCboTipoConsultaSaldo(Long cboTipoConsultaSaldo) {
        this.cboTipoConsultaSaldo = cboTipoConsultaSaldo;
    }

    /**
     * Get: cboTipoContaCredito.
     * 
     * @return cboTipoContaCredito
     */
    public Long getCboTipoContaCredito() {
        return cboTipoContaCredito;
    }

    /**
     * Set: cboTipoContaCredito.
     * 
     * @param cboTipoContaCredito
     *            the cbo tipo conta credito
     */
    public void setCboTipoContaCredito(Long cboTipoContaCredito) {
        this.cboTipoContaCredito = cboTipoContaCredito;
    }

    /**
     * Get: cboTipoDataControleFloating.
     * 
     * @return cboTipoDataControleFloating
     */
    public Long getCboTipoDataControleFloating() {
        return cboTipoDataControleFloating;
    }

    /**
     * Set: cboTipoDataControleFloating.
     * 
     * @param cboTipoDataControleFloating
     *            the cbo tipo data controle floating
     */
    public void setCboTipoDataControleFloating(Long cboTipoDataControleFloating) {
        this.cboTipoDataControleFloating = cboTipoDataControleFloating;
    }

    /**
     * Get: cboTipoFormacaoListaDebito.
     * 
     * @return cboTipoFormacaoListaDebito
     */
    public Long getCboTipoFormacaoListaDebito() {
        return cboTipoFormacaoListaDebito;
    }

    /**
     * Set: cboTipoFormacaoListaDebito.
     * 
     * @param cboTipoFormacaoListaDebito
     *            the cbo tipo formacao lista debito
     */
    public void setCboTipoFormacaoListaDebito(Long cboTipoFormacaoListaDebito) {
        this.cboTipoFormacaoListaDebito = cboTipoFormacaoListaDebito;
    }

    /**
     * Get: cboTipoIdentificacaoBeneficiario.
     * 
     * @return cboTipoIdentificacaoBeneficiario
     */
    public Long getCboTipoIdentificacaoBeneficiario() {
        return cboTipoIdentificacaoBeneficiario;
    }

    /**
     * Set: cboTipoIdentificacaoBeneficiario.
     * 
     * @param cboTipoIdentificacaoBeneficiario
     *            the cbo tipo identificacao beneficiario
     */
    public void setCboTipoIdentificacaoBeneficiario(
        Long cboTipoIdentificacaoBeneficiario) {
        this.cboTipoIdentificacaoBeneficiario = cboTipoIdentificacaoBeneficiario;
    }

    /**
     * Get: cboTipoInscricaoFavorecido.
     * 
     * @return cboTipoInscricaoFavorecido
     */
    public Long getCboTipoInscricaoFavorecido() {
        return cboTipoInscricaoFavorecido;
    }

    /**
     * Set: cboTipoInscricaoFavorecido.
     * 
     * @param cboTipoInscricaoFavorecido
     *            the cbo tipo inscricao favorecido
     */
    public void setCboTipoInscricaoFavorecido(Long cboTipoInscricaoFavorecido) {
        this.cboTipoInscricaoFavorecido = cboTipoInscricaoFavorecido;
    }

    /**
     * Get: cboTipoRastreamentoTitulo.
     * 
     * @return cboTipoRastreamentoTitulo
     */
    public Long getCboTipoRastreamentoTitulo() {
        return cboTipoRastreamentoTitulo;
    }

    /**
     * Set: cboTipoRastreamentoTitulo.
     * 
     * @param cboTipoRastreamentoTitulo
     *            the cbo tipo rastreamento titulo
     */
    public void setCboTipoRastreamentoTitulo(Long cboTipoRastreamentoTitulo) {
        this.cboTipoRastreamentoTitulo = cboTipoRastreamentoTitulo;
    }

    /**
     * Get: cboTipoRejeicaoAgendamento.
     * 
     * @return cboTipoRejeicaoAgendamento
     */
    public Long getCboTipoRejeicaoAgendamento() {
        return cboTipoRejeicaoAgendamento;
    }

    /**
     * Set: cboTipoRejeicaoAgendamento.
     * 
     * @param cboTipoRejeicaoAgendamento
     *            the cbo tipo rejeicao agendamento
     */
    public void setCboTipoRejeicaoAgendamento(Long cboTipoRejeicaoAgendamento) {
        this.cboTipoRejeicaoAgendamento = cboTipoRejeicaoAgendamento;
    }

    /**
     * Get: cboTipoRejeicaoEfetivacao.
     * 
     * @return cboTipoRejeicaoEfetivacao
     */
    public Long getCboTipoRejeicaoEfetivacao() {
        return cboTipoRejeicaoEfetivacao;
    }

    /**
     * Set: cboTipoRejeicaoEfetivacao.
     * 
     * @param cboTipoRejeicaoEfetivacao
     *            the cbo tipo rejeicao efetivacao
     */
    public void setCboTipoRejeicaoEfetivacao(Long cboTipoRejeicaoEfetivacao) {
        this.cboTipoRejeicaoEfetivacao = cboTipoRejeicaoEfetivacao;
    }

    /**
     * Get: cboTipoRejeicaoLote.
     * 
     * @return cboTipoRejeicaoLote
     */
    public Long getCboTipoRejeicaoLote() {
        return cboTipoRejeicaoLote;
    }

    /**
     * Set: cboTipoRejeicaoLote.
     * 
     * @param cboTipoRejeicaoLote
     *            the cbo tipo rejeicao lote
     */
    public void setCboTipoRejeicaoLote(Long cboTipoRejeicaoLote) {
        this.cboTipoRejeicaoLote = cboTipoRejeicaoLote;
    }

    /**
     * Get: cboTratamentoFeriadoFimVigenciaCredito.
     * 
     * @return cboTratamentoFeriadoFimVigenciaCredito
     */
    public Long getCboTratamentoFeriadoFimVigenciaCredito() {
        return cboTratamentoFeriadoFimVigenciaCredito;
    }

    /**
     * Set: cboTratamentoFeriadoFimVigenciaCredito.
     * 
     * @param cboTratamentoFeriadoFimVigenciaCredito
     *            the cbo tratamento feriado fim vigencia credito
     */
    public void setCboTratamentoFeriadoFimVigenciaCredito(
        Long cboTratamentoFeriadoFimVigenciaCredito) {
        this.cboTratamentoFeriadoFimVigenciaCredito = cboTratamentoFeriadoFimVigenciaCredito;
    }

    /**
     * Get: cboTratamentoFeriadosDataPagamento.
     * 
     * @return cboTratamentoFeriadosDataPagamento
     */
    public Long getCboTratamentoFeriadosDataPagamento() {
        return cboTratamentoFeriadosDataPagamento;
    }

    /**
     * Set: cboTratamentoFeriadosDataPagamento.
     * 
     * @param cboTratamentoFeriadosDataPagamento
     *            the cbo tratamento feriados data pagamento
     */
    public void setCboTratamentoFeriadosDataPagamento(
        Long cboTratamentoFeriadosDataPagamento) {
        this.cboTratamentoFeriadosDataPagamento = cboTratamentoFeriadosDataPagamento;
    }

    /**
     * Get: cboTratamentoListaDebitoSemNumeracao.
     * 
     * @return cboTratamentoListaDebitoSemNumeracao
     */
    public Long getCboTratamentoListaDebitoSemNumeracao() {
        return cboTratamentoListaDebitoSemNumeracao;
    }

    /**
     * Set: cboTratamentoListaDebitoSemNumeracao.
     * 
     * @param cboTratamentoListaDebitoSemNumeracao
     *            the cbo tratamento lista debito sem numeracao
     */
    public void setCboTratamentoListaDebitoSemNumeracao(
        Long cboTratamentoListaDebitoSemNumeracao) {
        this.cboTratamentoListaDebitoSemNumeracao = cboTratamentoListaDebitoSemNumeracao;
    }

    /**
     * Get: cboTratamentoValorDivergente.
     * 
     * @return cboTratamentoValorDivergente
     */
    public Long getCboTratamentoValorDivergente() {
        return cboTratamentoValorDivergente;
    }

    /**
     * Set: cboTratamentoValorDivergente.
     * 
     * @param cboTratamentoValorDivergente
     *            the cbo tratamento valor divergente
     */
    public void setCboTratamentoValorDivergente(
        Long cboTratamentoValorDivergente) {
        this.cboTratamentoValorDivergente = cboTratamentoValorDivergente;
    }

    /**
     * Get: qtddDiasAvisoAntecipado.
     * 
     * @return qtddDiasAvisoAntecipado
     */
    public Integer getQtddDiasAvisoAntecipado() {
        return qtddDiasAvisoAntecipado;
    }

    /**
     * Set: qtddDiasAvisoAntecipado.
     * 
     * @param qtddDiasAvisoAntecipado
     *            the qtdd dias aviso antecipado
     */
    public void setQtddDiasAvisoAntecipado(Integer qtddDiasAvisoAntecipado) {
        this.qtddDiasAvisoAntecipado = qtddDiasAvisoAntecipado;
    }

    /**
     * Get: qtddDiasEmissaoAvisoAntesInicioComprovacao.
     * 
     * @return qtddDiasEmissaoAvisoAntesInicioComprovacao
     */
    public Integer getQtddDiasEmissaoAvisoAntesInicioComprovacao() {
        return qtddDiasEmissaoAvisoAntesInicioComprovacao;
    }

    /**
     * Set: qtddDiasEmissaoAvisoAntesInicioComprovacao.
     * 
     * @param qtddDiasEmissaoAvisoAntesInicioComprovacao
     *            the qtdd dias emissao aviso antes inicio comprovacao
     */
    public void setQtddDiasEmissaoAvisoAntesInicioComprovacao(
        Integer qtddDiasEmissaoAvisoAntesInicioComprovacao) {
        this.qtddDiasEmissaoAvisoAntesInicioComprovacao = qtddDiasEmissaoAvisoAntesInicioComprovacao;
    }

    /**
     * Get: qtddDiasEmissaoAvisoAntesVencimento.
     * 
     * @return qtddDiasEmissaoAvisoAntesVencimento
     */
    public Integer getQtddDiasEmissaoAvisoAntesVencimento() {
        return qtddDiasEmissaoAvisoAntesVencimento;
    }

    /**
     * Set: qtddDiasEmissaoAvisoAntesVencimento.
     * 
     * @param qtddDiasEmissaoAvisoAntesVencimento
     *            the qtdd dias emissao aviso antes vencimento
     */
    public void setQtddDiasEmissaoAvisoAntesVencimento(
        Integer qtddDiasEmissaoAvisoAntesVencimento) {
        this.qtddDiasEmissaoAvisoAntesVencimento = qtddDiasEmissaoAvisoAntesVencimento;
    }

    /**
     * Get: qtddDiasEnvioAntecipadoFormulario.
     * 
     * @return qtddDiasEnvioAntecipadoFormulario
     */
    public Integer getQtddDiasEnvioAntecipadoFormulario() {
        return qtddDiasEnvioAntecipadoFormulario;
    }

    /**
     * Set: qtddDiasEnvioAntecipadoFormulario.
     * 
     * @param qtddDiasEnvioAntecipadoFormulario
     *            the qtdd dias envio antecipado formulario
     */
    public void setQtddDiasEnvioAntecipadoFormulario(
        Integer qtddDiasEnvioAntecipadoFormulario) {
        this.qtddDiasEnvioAntecipadoFormulario = qtddDiasEnvioAntecipadoFormulario;
    }

    /**
     * Get: qtddDiasFloating.
     * 
     * @return qtddDiasFloating
     */
    public Integer getQtddDiasFloating() {
        return qtddDiasFloating;
    }

    /**
     * Set: qtddDiasFloating.
     * 
     * @param qtddDiasFloating
     *            the qtdd dias floating
     */
    public void setQtddDiasFloating(Integer qtddDiasFloating) {
        this.qtddDiasFloating = qtddDiasFloating;
    }

    /**
     * Get: qtddDiasInativacaoFavorecido.
     * 
     * @return qtddDiasInativacaoFavorecido
     */
    public Integer getQtddDiasInativacaoFavorecido() {
        return qtddDiasInativacaoFavorecido;
    }

    /**
     * Set: qtddDiasInativacaoFavorecido.
     * 
     * @param qtddDiasInativacaoFavorecido
     *            the qtdd dias inativacao favorecido
     */
    public void setQtddDiasInativacaoFavorecido(
        Integer qtddDiasInativacaoFavorecido) {
        this.qtddDiasInativacaoFavorecido = qtddDiasInativacaoFavorecido;
    }

    /**
     * Get: qtddDiasRepique.
     * 
     * @return qtddDiasRepique
     */
    public Integer getQtddDiasRepique() {
        return qtddDiasRepique;
    }

    /**
     * Set: qtddDiasRepique.
     * 
     * @param qtddDiasRepique
     *            the qtdd dias repique
     */
    public void setQtddDiasRepique(Integer qtddDiasRepique) {
        this.qtddDiasRepique = qtddDiasRepique;
    }

    /**
     * Get: qtddFasesPorEtapa.
     * 
     * @return qtddFasesPorEtapa
     */
    public Integer getQtddFasesPorEtapa() {
        return qtddFasesPorEtapa;
    }

    /**
     * Set: qtddFasesPorEtapa.
     * 
     * @param qtddFasesPorEtapa
     *            the qtdd fases por etapa
     */
    public void setQtddFasesPorEtapa(Integer qtddFasesPorEtapa) {
        this.qtddFasesPorEtapa = qtddFasesPorEtapa;
    }

    /**
     * Get: qtddLimiteCartaoContaSalarioSolicitacao.
     * 
     * @return qtddLimiteCartaoContaSalarioSolicitacao
     */
    public Integer getQtddLimiteCartaoContaSalarioSolicitacao() {
        return qtddLimiteCartaoContaSalarioSolicitacao;
    }

    /**
     * Set: qtddLimiteCartaoContaSalarioSolicitacao.
     * 
     * @param qtddLimiteCartaoContaSalarioSolicitacao
     *            the qtdd limite cartao conta salario solicitacao
     */
    public void setQtddLimiteCartaoContaSalarioSolicitacao(
        Integer qtddLimiteCartaoContaSalarioSolicitacao) {
        this.qtddLimiteCartaoContaSalarioSolicitacao = qtddLimiteCartaoContaSalarioSolicitacao;
    }

    /**
     * Get: qtddLinhasPorComprovante.
     * 
     * @return qtddLinhasPorComprovante
     */
    public Integer getQtddLinhasPorComprovante() {
        return qtddLinhasPorComprovante;
    }

    /**
     * Set: qtddLinhasPorComprovante.
     * 
     * @param qtddLinhasPorComprovante
     *            the qtdd linhas por comprovante
     */
    public void setQtddLinhasPorComprovante(Integer qtddLinhasPorComprovante) {
        this.qtddLinhasPorComprovante = qtddLinhasPorComprovante;
    }

    /**
     * Get: qtddMaximaRegistrosInconsistentesLote.
     * 
     * @return qtddMaximaRegistrosInconsistentesLote
     */
    public Integer getQtddMaximaRegistrosInconsistentesLote() {
        return qtddMaximaRegistrosInconsistentesLote;
    }

    /**
     * Set: qtddMaximaRegistrosInconsistentesLote.
     * 
     * @param qtddMaximaRegistrosInconsistentesLote
     *            the qtdd maxima registros inconsistentes lote
     */
    public void setQtddMaximaRegistrosInconsistentesLote(
        Integer qtddMaximaRegistrosInconsistentesLote) {
        this.qtddMaximaRegistrosInconsistentesLote = qtddMaximaRegistrosInconsistentesLote;
    }

    /**
     * Get: qtddMesesPeriodicidadeComprovacao.
     * 
     * @return qtddMesesPeriodicidadeComprovacao
     */
    public Integer getQtddMesesPeriodicidadeComprovacao() {
        return qtddMesesPeriodicidadeComprovacao;
    }

    /**
     * Set: qtddMesesPeriodicidadeComprovacao.
     * 
     * @param qtddMesesPeriodicidadeComprovacao
     *            the qtdd meses periodicidade comprovacao
     */
    public void setQtddMesesPeriodicidadeComprovacao(
        Integer qtddMesesPeriodicidadeComprovacao) {
        this.qtddMesesPeriodicidadeComprovacao = qtddMesesPeriodicidadeComprovacao;
    }

    /**
     * Get: qtddMesesPorEtapa.
     * 
     * @return qtddMesesPorEtapa
     */
    public Integer getQtddMesesPorEtapa() {
        return qtddMesesPorEtapa;
    }

    /**
     * Set: qtddMesesPorEtapa.
     * 
     * @param qtddMesesPorEtapa
     *            the qtdd meses por etapa
     */
    public void setQtddMesesPorEtapa(Integer qtddMesesPorEtapa) {
        this.qtddMesesPorEtapa = qtddMesesPorEtapa;
    }

    /**
     * Get: qtddMesesPorFase.
     * 
     * @return qtddMesesPorFase
     */
    public Integer getQtddMesesPorFase() {
        return qtddMesesPorFase;
    }

    /**
     * Set: qtddMesesPorFase.
     * 
     * @param qtddMesesPorFase
     *            the qtdd meses por fase
     */
    public void setQtddMesesPorFase(Integer qtddMesesPorFase) {
        this.qtddMesesPorFase = qtddMesesPorFase;
    }

    /**
     * Get: qtddViasEmitir.
     * 
     * @return qtddViasEmitir
     */
    public Integer getQtddViasEmitir() {
        return qtddViasEmitir;
    }

    /**
     * Set: qtddViasEmitir.
     * 
     * @param qtddViasEmitir
     *            the qtdd vias emitir
     */
    public void setQtddViasEmitir(Integer qtddViasEmitir) {
        this.qtddViasEmitir = qtddViasEmitir;
    }

    /**
     * Get: qtddViasPagasRegistro.
     * 
     * @return qtddViasPagasRegistro
     */
    public Integer getQtddViasPagasRegistro() {
        return qtddViasPagasRegistro;
    }

    /**
     * Set: qtddViasPagasRegistro.
     * 
     * @param qtddViasPagasRegistro
     *            the qtdd vias pagas registro
     */
    public void setQtddViasPagasRegistro(Integer qtddViasPagasRegistro) {
        this.qtddViasPagasRegistro = qtddViasPagasRegistro;
    }

    /**
     * Get: quantidadeDiasExpiracao.
     * 
     * @return quantidadeDiasExpiracao
     */
    public Integer getQuantidadeDiasExpiracao() {
        return quantidadeDiasExpiracao;
    }

    /**
     * Set: quantidadeDiasExpiracao.
     * 
     * @param quantidadeDiasExpiracao
     *            the quantidade dias expiracao
     */
    public void setQuantidadeDiasExpiracao(Integer quantidadeDiasExpiracao) {
        this.quantidadeDiasExpiracao = quantidadeDiasExpiracao;
    }

    /**
     * Get: quantidadeEtapasRecadastramento.
     * 
     * @return quantidadeEtapasRecadastramento
     */
    public Integer getQuantidadeEtapasRecadastramento() {
        return quantidadeEtapasRecadastramento;
    }

    /**
     * Set: quantidadeEtapasRecadastramento.
     * 
     * @param quantidadeEtapasRecadastramento
     *            the quantidade etapas recadastramento
     */
    public void setQuantidadeEtapasRecadastramento(
        Integer quantidadeEtapasRecadastramento) {
        this.quantidadeEtapasRecadastramento = quantidadeEtapasRecadastramento;
    }

    /**
     * Get: quantidadeLimiteDiasPagamentoVencido.
     * 
     * @return quantidadeLimiteDiasPagamentoVencido
     */
    public Integer getQuantidadeLimiteDiasPagamentoVencido() {
        return quantidadeLimiteDiasPagamentoVencido;
    }

    /**
     * Set: quantidadeLimiteDiasPagamentoVencido.
     * 
     * @param quantidadeLimiteDiasPagamentoVencido
     *            the quantidade limite dias pagamento vencido
     */
    public void setQuantidadeLimiteDiasPagamentoVencido(
        Integer quantidadeLimiteDiasPagamentoVencido) {
        this.quantidadeLimiteDiasPagamentoVencido = quantidadeLimiteDiasPagamentoVencido;
    }

    /**
     * Get: quantidadeMesesEmissao.
     * 
     * @return quantidadeMesesEmissao
     */
    public Integer getQuantidadeMesesEmissao() {
        return quantidadeMesesEmissao;
    }

    /**
     * Set: quantidadeMesesEmissao.
     * 
     * @param quantidadeMesesEmissao
     *            the quantidade meses emissao
     */
    public void setQuantidadeMesesEmissao(Integer quantidadeMesesEmissao) {
        this.quantidadeMesesEmissao = quantidadeMesesEmissao;
    }

    /**
     * Get: rdoAgendamentoDebitosPendentesVeiculos.
     * 
     * @return rdoAgendamentoDebitosPendentesVeiculos
     */
    public String getRdoAgendamentoDebitosPendentesVeiculos() {
        return rdoAgendamentoDebitosPendentesVeiculos;
    }

    /**
     * Set: rdoAgendamentoDebitosPendentesVeiculos.
     * 
     * @param rdoAgendamentoDebitosPendentesVeiculos
     *            the rdo agendamento debitos pendentes veiculos
     */
    public void setRdoAgendamentoDebitosPendentesVeiculos(
        String rdoAgendamentoDebitosPendentesVeiculos) {
        this.rdoAgendamentoDebitosPendentesVeiculos = rdoAgendamentoDebitosPendentesVeiculos;
    }

    /**
     * Get: cboOcorrenciaDebito.
     * 
     * @return cboOcorrenciaDebito
     */
    public Long getCboOcorrenciaDebito() {
        return cboOcorrenciaDebito;
    }

    /**
     * Set: cboOcorrenciaDebito.
     * 
     * @param cboOcorrenciaDebito
     *            the cbo ocorrencia debito
     */
    public void setCboOcorrenciaDebito(Long cboOcorrenciaDebito) {
        this.cboOcorrenciaDebito = cboOcorrenciaDebito;
    }

    /**
     * Get: listaTipoContaCredito.
     * 
     * @return listaTipoContaCredito
     */
    public List<SelectItem> getListaTipoContaCredito() {
        return listaTipoContaCredito;
    }

    /**
     * Set: listaTipoContaCredito.
     * 
     * @param listaTipoContaCredito
     *            the lista tipo conta credito
     */
    public void setListaTipoContaCredito(List<SelectItem> listaTipoContaCredito) {
        this.listaTipoContaCredito = listaTipoContaCredito;
    }

    /**
     * Get: listaTipoContaCreditoHash.
     * 
     * @return listaTipoContaCreditoHash
     */
    public Map<Long, String> getListaTipoContaCreditoHash() {
        return listaTipoContaCreditoHash;
    }

    /**
     * Set lista tipo conta credito hash.
     * 
     * @param listaTipoContaCreditoHash
     *            the lista tipo conta credito hash
     */
    public void setListaTipoContaCreditoHash(
        Map<Long, String> listaTipoContaCreditoHash) {
        this.listaTipoContaCreditoHash = listaTipoContaCreditoHash;
    }

    /**
     * Get: dsOcorrenciaDebito.
     * 
     * @return dsOcorrenciaDebito
     */
    public String getDsOcorrenciaDebito() {
        return dsOcorrenciaDebito;
    }

    /**
     * Set: dsOcorrenciaDebito.
     * 
     * @param dsOcorrenciaDebito
     *            the ds ocorrencia debito
     */
    public void setDsOcorrenciaDebito(String dsOcorrenciaDebito) {
        this.dsOcorrenciaDebito = dsOcorrenciaDebito;
    }

    /**
     * Get: cboMeioDisponibilizacaoCorrentista.
     * 
     * @return cboMeioDisponibilizacaoCorrentista
     */
    public Long getCboMeioDisponibilizacaoCorrentista() {
        return cboMeioDisponibilizacaoCorrentista;
    }

    /**
     * Set: cboMeioDisponibilizacaoCorrentista.
     * 
     * @param cboMeioDisponibilizacaoCorrentista
     *            the cbo meio disponibilizacao correntista
     */
    public void setCboMeioDisponibilizacaoCorrentista(
        Long cboMeioDisponibilizacaoCorrentista) {
        this.cboMeioDisponibilizacaoCorrentista = cboMeioDisponibilizacaoCorrentista;
    }

    /**
     * Get: dsMeioDisponibilizacaoCorrenstista.
     * 
     * @return dsMeioDisponibilizacaoCorrenstista
     */
    public String getDsMeioDisponibilizacaoCorrenstista() {
        return dsMeioDisponibilizacaoCorrenstista;
    }

    /**
     * Set: dsMeioDisponibilizacaoCorrenstista.
     * 
     * @param dsMeioDisponibilizacaoCorrenstista
     *            the ds meio disponibilizacao correnstista
     */
    public void setDsMeioDisponibilizacaoCorrenstista(
        String dsMeioDisponibilizacaoCorrenstista) {
        this.dsMeioDisponibilizacaoCorrenstista = dsMeioDisponibilizacaoCorrenstista;
    }

    /**
     * Get: cboPeriodicidadeEmissao.
     * 
     * @return cboPeriodicidadeEmissao
     */
    public Integer getCboPeriodicidadeEmissao() {
        return cboPeriodicidadeEmissao;
    }

    /**
     * Set: cboPeriodicidadeEmissao.
     * 
     * @param cboPeriodicidadeEmissao
     *            the cbo periodicidade emissao
     */
    public void setCboPeriodicidadeEmissao(Integer cboPeriodicidadeEmissao) {
        this.cboPeriodicidadeEmissao = cboPeriodicidadeEmissao;
    }

    /**
     * Get: dsPeriodicidadeEmissao.
     * 
     * @return dsPeriodicidadeEmissao
     */
    public String getDsPeriodicidadeEmissao() {
        return dsPeriodicidadeEmissao;
    }

    /**
     * Set: dsPeriodicidadeEmissao.
     * 
     * @param dsPeriodicidadeEmissao
     *            the ds periodicidade emissao
     */
    public void setDsPeriodicidadeEmissao(String dsPeriodicidadeEmissao) {
        this.dsPeriodicidadeEmissao = dsPeriodicidadeEmissao;
    }

    /**
     * Get: rdoPermiteCorrespondenciaAberta.
     * 
     * @return rdoPermiteCorrespondenciaAberta
     */
    public String getRdoPermiteCorrespondenciaAberta() {
        return rdoPermiteCorrespondenciaAberta;
    }

    /**
     * Set: rdoPermiteCorrespondenciaAberta.
     * 
     * @param rdoPermiteCorrespondenciaAberta
     *            the rdo permite correspondencia aberta
     */
    public void setRdoPermiteCorrespondenciaAberta(
        String rdoPermiteCorrespondenciaAberta) {
        this.rdoPermiteCorrespondenciaAberta = rdoPermiteCorrespondenciaAberta;
    }

    /**
     * Get: cboMeioDisponibilizacaoNaoCorrentista.
     * 
     * @return cboMeioDisponibilizacaoNaoCorrentista
     */
    public Long getCboMeioDisponibilizacaoNaoCorrentista() {
        return cboMeioDisponibilizacaoNaoCorrentista;
    }

    /**
     * Set: cboMeioDisponibilizacaoNaoCorrentista.
     * 
     * @param cboMeioDisponibilizacaoNaoCorrentista
     *            the cbo meio disponibilizacao nao correntista
     */
    public void setCboMeioDisponibilizacaoNaoCorrentista(
        Long cboMeioDisponibilizacaoNaoCorrentista) {
        this.cboMeioDisponibilizacaoNaoCorrentista = cboMeioDisponibilizacaoNaoCorrentista;
    }

    /**
     * Get: dsMeioDisponibilizacaoNaoCorrenstista.
     * 
     * @return dsMeioDisponibilizacaoNaoCorrenstista
     */
    public String getDsMeioDisponibilizacaoNaoCorrenstista() {
        return dsMeioDisponibilizacaoNaoCorrenstista;
    }

    /**
     * Set: dsMeioDisponibilizacaoNaoCorrenstista.
     * 
     * @param dsMeioDisponibilizacaoNaoCorrenstista
     *            the ds meio disponibilizacao nao correnstista
     */
    public void setDsMeioDisponibilizacaoNaoCorrenstista(
        String dsMeioDisponibilizacaoNaoCorrenstista) {
        this.dsMeioDisponibilizacaoNaoCorrenstista = dsMeioDisponibilizacaoNaoCorrenstista;
    }

    /**
     * Get: cboTipoTratamentoContaTransferida.
     * 
     * @return cboTipoTratamentoContaTransferida
     */
    public Long getCboTipoTratamentoContaTransferida() {
        return cboTipoTratamentoContaTransferida;
    }

    /**
     * Set: cboTipoTratamentoContaTransferida.
     * 
     * @param cboTipoTratamentoContaTransferida
     *            the cbo tipo tratamento conta transferida
     */
    public void setCboTipoTratamentoContaTransferida(
        Long cboTipoTratamentoContaTransferida) {
        this.cboTipoTratamentoContaTransferida = cboTipoTratamentoContaTransferida;
    }

    /**
     * Get: dsTipoTratamentoContaTransferida.
     * 
     * @return dsTipoTratamentoContaTransferida
     */
    public String getDsTipoTratamentoContaTransferida() {
        return dsTipoTratamentoContaTransferida;
    }

    /**
     * Set: dsTipoTratamentoContaTransferida.
     * 
     * @param dsTipoTratamentoContaTransferida
     *            the ds tipo tratamento conta transferida
     */
    public void setDsTipoTratamentoContaTransferida(
        String dsTipoTratamentoContaTransferida) {
        this.dsTipoTratamentoContaTransferida = dsTipoTratamentoContaTransferida;
    }

    /**
     * Get: possuiModalidade.
     * 
     * @return possuiModalidade
     */
    public Boolean getPossuiModalidade() {
        return possuiModalidade;
    }

    /**
     * Set: possuiModalidade.
     * 
     * @param possuiModalidade
     *            the possui modalidade
     */
    public void setPossuiModalidade(Boolean possuiModalidade) {
        this.possuiModalidade = possuiModalidade;
    }

    /**
     * Is desabilita tipo form lista debito.
     * 
     * @return true, if is desabilita tipo form lista debito
     */
    public boolean isDesabilitaTipoFormListaDebito() {
        return desabilitaTipoFormListaDebito;
    }

    /**
     * Set: desabilitaTipoFormListaDebito.
     * 
     * @param desabilitaTipoFormListaDebito
     *            the desabilita tipo form lista debito
     */
    public void setDesabilitaTipoFormListaDebito(
        boolean desabilitaTipoFormListaDebito) {
        this.desabilitaTipoFormListaDebito = desabilitaTipoFormListaDebito;
    }

    /**
     * Is desabilita tratamento lista debito sem numeracao.
     * 
     * @return true, if is desabilita tratamento lista debito sem numeracao
     */
    public boolean isDesabilitaTratamentoListaDebitoSemNumeracao() {
        return desabilitaTratamentoListaDebitoSemNumeracao;
    }

    /**
     * Set: desabilitaTratamentoListaDebitoSemNumeracao.
     * 
     * @param desabilitaTratamentoListaDebitoSemNumeracao
     *            the desabilita tratamento lista debito sem numeracao
     */
    public void setDesabilitaTratamentoListaDebitoSemNumeracao(
        boolean desabilitaTratamentoListaDebitoSemNumeracao) {
        this.desabilitaTratamentoListaDebitoSemNumeracao = desabilitaTratamentoListaDebitoSemNumeracao;
    }

    /**
     * Is desabilita periodicidade pesquisa deb pend veiculos.
     * 
     * @return true, if is desabilita periodicidade pesquisa deb pend veiculos
     */
    public boolean isDesabilitaPeriodicidadePesquisaDebPendVeiculos() {
        return desabilitaPeriodicidadePesquisaDebPendVeiculos;
    }

    /**
     * Set: desabilitaPeriodicidadePesquisaDebPendVeiculos.
     * 
     * @param desabilitaPeriodicidadePesquisaDebPendVeiculos
     *            the desabilita periodicidade pesquisa deb pend veiculos
     */
    public void setDesabilitaPeriodicidadePesquisaDebPendVeiculos(
        boolean desabilitaPeriodicidadePesquisaDebPendVeiculos) {
        this.desabilitaPeriodicidadePesquisaDebPendVeiculos = desabilitaPeriodicidadePesquisaDebPendVeiculos;
    }

    /**
     * Get: saidaVerificarAtributo.
     * 
     * @return saidaVerificarAtributo
     */
    public VerificarAtributosServicoModalidadeSaidaDTO getSaidaVerificarAtributo() {
        return saidaVerificarAtributo;
    }

    /**
     * Set: saidaVerificarAtributo.
     * 
     * @param saidaVerificarAtributo
     *            the saida verificar atributo
     */
    public void setSaidaVerificarAtributo(
        VerificarAtributosServicoModalidadeSaidaDTO saidaVerificarAtributo) {
        this.saidaVerificarAtributo = saidaVerificarAtributo;
    }

    /**
     * Get: rdoEfetuarManutencaoCadastroProcuradores.
     * 
     * @return rdoEfetuarManutencaoCadastroProcuradores
     */
    public String getRdoEfetuarManutencaoCadastroProcuradores() {
        return rdoEfetuarManutencaoCadastroProcuradores;
    }

    /**
     * Set: rdoEfetuarManutencaoCadastroProcuradores.
     * 
     * @param rdoEfetuarManutencaoCadastroProcuradores
     *            the rdo efetuar manutencao cadastro procuradores
     */
    public void setRdoEfetuarManutencaoCadastroProcuradores(
        String rdoEfetuarManutencaoCadastroProcuradores) {
        this.rdoEfetuarManutencaoCadastroProcuradores = rdoEfetuarManutencaoCadastroProcuradores;
    }

    /**
     * Get: rdoGerarRetornoSeparadoCanal.
     * 
     * @return rdoGerarRetornoSeparadoCanal
     */
    public String getRdoGerarRetornoSeparadoCanal() {
        return rdoGerarRetornoSeparadoCanal;
    }

    /**
     * Set: rdoGerarRetornoSeparadoCanal.
     * 
     * @param rdoGerarRetornoSeparadoCanal
     *            the rdo gerar retorno separado canal
     */
    public void setRdoGerarRetornoSeparadoCanal(
        String rdoGerarRetornoSeparadoCanal) {
        this.rdoGerarRetornoSeparadoCanal = rdoGerarRetornoSeparadoCanal;
    }

    /**
     * Get: rdoUtilizaLancPersonalizado.
     * 
     * @return rdoUtilizaLancPersonalizado
     */
    public String getRdoUtilizaLancPersonalizado() {
        return rdoUtilizaLancPersonalizado;
    }

    /**
     * Set: rdoUtilizaLancPersonalizado.
     * 
     * @param rdoUtilizaLancPersonalizado
     *            the rdo utiliza lanc personalizado
     */
    public void setRdoUtilizaLancPersonalizado(
        String rdoUtilizaLancPersonalizado) {
        this.rdoUtilizaLancPersonalizado = rdoUtilizaLancPersonalizado;
    }

    /**
     * Is desabilita gerar retorno separado.
     * 
     * @return true, if is desabilita gerar retorno separado
     */
    public boolean isDesabilitaGerarRetornoSeparado() {
        return desabilitaGerarRetornoSeparado;
    }

    /**
     * Set: desabilitaGerarRetornoSeparado.
     * 
     * @param desabilitaGerarRetornoSeparado
     *            the desabilita gerar retorno separado
     */
    public void setDesabilitaGerarRetornoSeparado(
        boolean desabilitaGerarRetornoSeparado) {
        this.desabilitaGerarRetornoSeparado = desabilitaGerarRetornoSeparado;
    }

    /**
     * Is desabilita campos procuradores.
     * 
     * @return true, if is desabilita campos procuradores
     */
    public boolean isDesabilitaCamposProcuradores() {
        return desabilitaCamposProcuradores;
    }

    /**
     * Set: desabilitaCamposProcuradores.
     * 
     * @param desabilitaCamposProcuradores
     *            the desabilita campos procuradores
     */
    public void setDesabilitaCamposProcuradores(
        boolean desabilitaCamposProcuradores) {
        this.desabilitaCamposProcuradores = desabilitaCamposProcuradores;
    }

    /**
     * Is desabilita campos man cad procuradores.
     * 
     * @return true, if is desabilita campos man cad procuradores
     */
    public boolean isDesabilitaCamposManCadProcuradores() {
        return desabilitaCamposManCadProcuradores;
    }

    /**
     * Set: desabilitaCamposManCadProcuradores.
     * 
     * @param desabilitaCamposManCadProcuradores
     *            the desabilita campos man cad procuradores
     */
    public void setDesabilitaCamposManCadProcuradores(
        boolean desabilitaCamposManCadProcuradores) {
        this.desabilitaCamposManCadProcuradores = desabilitaCamposManCadProcuradores;
    }

    /**
     * Is desabilita campos expiracao credito.
     * 
     * @return true, if is desabilita campos expiracao credito
     */
    public boolean isDesabilitaCamposExpiracaoCredito() {
        return desabilitaCamposExpiracaoCredito;
    }

    /**
     * Set: desabilitaCamposExpiracaoCredito.
     * 
     * @param desabilitaCamposExpiracaoCredito
     *            the desabilita campos expiracao credito
     */
    public void setDesabilitaCamposExpiracaoCredito(
        boolean desabilitaCamposExpiracaoCredito) {
        this.desabilitaCamposExpiracaoCredito = desabilitaCamposExpiracaoCredito;
    }

    /**
     * Is desabilita campos envio remessa man.
     * 
     * @return true, if is desabilita campos envio remessa man
     */
    public boolean isDesabilitaCamposEnvioRemessaMan() {
        return desabilitaCamposEnvioRemessaMan;
    }

    /**
     * Set: desabilitaCamposEnvioRemessaMan.
     * 
     * @param desabilitaCamposEnvioRemessaMan
     *            the desabilita campos envio remessa man
     */
    public void setDesabilitaCamposEnvioRemessaMan(
        boolean desabilitaCamposEnvioRemessaMan) {
        this.desabilitaCamposEnvioRemessaMan = desabilitaCamposEnvioRemessaMan;
    }

    /**
     * Is desabilita campos data acerto dados.
     * 
     * @return true, if is desabilita campos data acerto dados
     */
    public boolean isDesabilitaCamposDataAcertoDados() {
        return desabilitaCamposDataAcertoDados;
    }

    /**
     * Set: desabilitaCamposDataAcertoDados.
     * 
     * @param desabilitaCamposDataAcertoDados
     *            the desabilita campos data acerto dados
     */
    public void setDesabilitaCamposDataAcertoDados(
        boolean desabilitaCamposDataAcertoDados) {
        this.desabilitaCamposDataAcertoDados = desabilitaCamposDataAcertoDados;
    }

    /**
     * Is desabilita campos msg online.
     * 
     * @return true, if is desabilita campos msg online
     */
    public boolean isDesabilitaCamposMsgOnline() {
        return desabilitaCamposMsgOnline;
    }

    /**
     * Set: desabilitaCamposMsgOnline.
     * 
     * @param desabilitaCamposMsgOnline
     *            the desabilita campos msg online
     */
    public void setDesabilitaCamposMsgOnline(boolean desabilitaCamposMsgOnline) {
        this.desabilitaCamposMsgOnline = desabilitaCamposMsgOnline;
    }

    /**
     * Get: indiceReajuste.
     * 
     * @return indiceReajuste
     */
    public Integer getIndiceReajuste() {
        return indiceReajuste;
    }

    /**
     * Set: indiceReajuste.
     * 
     * @param indiceReajuste
     *            the indice reajuste
     */
    public void setIndiceReajuste(Integer indiceReajuste) {
        this.indiceReajuste = indiceReajuste;
    }

    /**
     * Is desabilita tp cartao qtde limite.
     * 
     * @return true, if is desabilita tp cartao qtde limite
     */
    public boolean isDesabilitaTpCartaoQtdeLimite() {
        return desabilitaTpCartaoQtdeLimite;
    }

    /**
     * Set: desabilitaTpCartaoQtdeLimite.
     * 
     * @param desabilitaTpCartaoQtdeLimite
     *            the desabilita tp cartao qtde limite
     */
    public void setDesabilitaTpCartaoQtdeLimite(
        boolean desabilitaTpCartaoQtdeLimite) {
        this.desabilitaTpCartaoQtdeLimite = desabilitaTpCartaoQtdeLimite;
    }

    /**
     * Get: dsAgruparCorrespondencia.
     * 
     * @return dsAgruparCorrespondencia
     */
    public String getDsAgruparCorrespondencia() {
        return dsAgruparCorrespondencia;
    }

    /**
     * Set: dsAgruparCorrespondencia.
     * 
     * @param dsAgruparCorrespondencia
     *            the ds agrupar correspondencia
     */
    public void setDsAgruparCorrespondencia(String dsAgruparCorrespondencia) {
        this.dsAgruparCorrespondencia = dsAgruparCorrespondencia;
    }

    /**
     * Is desabilitar campos lote.
     * 
     * @return true, if is desabilitar campos lote
     */
    public boolean isDesabilitarCamposLote() {
        return desabilitarCamposLote;
    }

    /**
     * Set: desabilitarCamposLote.
     * 
     * @param desabilitarCamposLote
     *            the desabilitar campos lote
     */
    public void setDesabilitarCamposLote(boolean desabilitarCamposLote) {
        this.desabilitarCamposLote = desabilitarCamposLote;
    }

    /**
     * Is desabilitar campos perc lote.
     * 
     * @return true, if is desabilitar campos perc lote
     */
    public boolean isDesabilitarCamposPercLote() {
        return desabilitarCamposPercLote;
    }

    /**
     * Set: desabilitarCamposPercLote.
     * 
     * @param desabilitarCamposPercLote
     *            the desabilitar campos perc lote
     */
    public void setDesabilitarCamposPercLote(boolean desabilitarCamposPercLote) {
        this.desabilitarCamposPercLote = desabilitarCamposPercLote;
    }

    /**
     * Is desabilitar campos qtde lote.
     * 
     * @return true, if is desabilitar campos qtde lote
     */
    public boolean isDesabilitarCamposQtdeLote() {
        return desabilitarCamposQtdeLote;
    }

    /**
     * Set: desabilitarCamposQtdeLote.
     * 
     * @param desabilitarCamposQtdeLote
     *            the desabilitar campos qtde lote
     */
    public void setDesabilitarCamposQtdeLote(boolean desabilitarCamposQtdeLote) {
        this.desabilitarCamposQtdeLote = desabilitarCamposQtdeLote;
    }

    /**
     * Is desabilita valor maximo fav nao cad.
     * 
     * @return true, if is desabilita valor maximo fav nao cad
     */
    public boolean isDesabilitaValorMaximoFavNaoCad() {
        return desabilitaValorMaximoFavNaoCad;
    }

    /**
     * Set: desabilitaValorMaximoFavNaoCad.
     * 
     * @param desabilitaValorMaximoFavNaoCad
     *            the desabilita valor maximo fav nao cad
     */
    public void setDesabilitaValorMaximoFavNaoCad(
        boolean desabilitaValorMaximoFavNaoCad) {
        this.desabilitaValorMaximoFavNaoCad = desabilitaValorMaximoFavNaoCad;
    }

    /**
     * Is desabilitar campos emite avisos.
     * 
     * @return true, if is desabilitar campos emite avisos
     */
    public boolean isDesabilitarCamposEmiteAvisos() {
        return desabilitarCamposEmiteAvisos;
    }

    /**
     * Set: desabilitarCamposEmiteAvisos.
     * 
     * @param desabilitarCamposEmiteAvisos
     *            the desabilitar campos emite avisos
     */
    public void setDesabilitarCamposEmiteAvisos(
        boolean desabilitarCamposEmiteAvisos) {
        this.desabilitarCamposEmiteAvisos = desabilitarCamposEmiteAvisos;
    }

    /**
     * Is desabilitar campo cadastro endereco.
     * 
     * @return true, if is desabilitar campo cadastro endereco
     */
    public boolean isDesabilitarCampoCadastroEndereco() {
        return desabilitarCampoCadastroEndereco;
    }

    /**
     * Set: desabilitarCampoCadastroEndereco.
     * 
     * @param desabilitarCampoCadastroEndereco
     *            the desabilitar campo cadastro endereco
     */
    public void setDesabilitarCampoCadastroEndereco(
        boolean desabilitarCampoCadastroEndereco) {
        this.desabilitarCampoCadastroEndereco = desabilitarCampoCadastroEndereco;
    }

    /**
     * Is desabilitar campo qtde lim dias pgto venc.
     * 
     * @return true, if is desabilitar campo qtde lim dias pgto venc
     */
    public boolean isDesabilitarCampoQtdeLimDiasPgtoVenc() {
        return desabilitarCampoQtdeLimDiasPgtoVenc;
    }

    /**
     * Set: desabilitarCampoQtdeLimDiasPgtoVenc.
     * 
     * @param desabilitarCampoQtdeLimDiasPgtoVenc
     *            the desabilitar campo qtde lim dias pgto venc
     */
    public void setDesabilitarCampoQtdeLimDiasPgtoVenc(
        boolean desabilitarCampoQtdeLimDiasPgtoVenc) {
        this.desabilitarCampoQtdeLimDiasPgtoVenc = desabilitarCampoQtdeLimDiasPgtoVenc;
    }

    /**
     * Get: listaModalidadeTipoServicoSaidaDTO.
     * 
     * @return listaModalidadeTipoServicoSaidaDTO
     */
    public ListarModalidadeTipoServicoSaidaDTO getListaModalidadeTipoServicoSaidaDTO() {
        return listaModalidadeTipoServicoSaidaDTO;
    }

    /**
     * Set: listaModalidadeTipoServicoSaidaDTO.
     * 
     * @param listaModalidadeTipoServicoSaidaDTO
     *            the lista modalidade tipo servico saida dto
     */
    public void setListaModalidadeTipoServicoSaidaDTO(
        ListarModalidadeTipoServicoSaidaDTO listaModalidadeTipoServicoSaidaDTO) {
        this.listaModalidadeTipoServicoSaidaDTO = listaModalidadeTipoServicoSaidaDTO;
    }

    /**
     * Is habilita campo.
     * 
     * @return true, if is habilita campo
     */
    public boolean isHabilitaCampo() {
        return habilitaCampo;
    }

    /**
     * Set: habilitaCampo.
     * 
     * @param habilitaCampo
     *            the habilita campo
     */
    public void setHabilitaCampo(boolean habilitaCampo) {
        this.habilitaCampo = habilitaCampo;
    }

    /**
     * Get: modalidadeTipoServicoSaidaDTO.
     * 
     * @return modalidadeTipoServicoSaidaDTO
     */
    public ListarModalidadeTipoServicoSaidaDTO getModalidadeTipoServicoSaidaDTO() {
        return modalidadeTipoServicoSaidaDTO;
    }

    /**
     * Set: modalidadeTipoServicoSaidaDTO.
     * 
     * @param modalidadeTipoServicoSaidaDTO
     *            the modalidade tipo servico saida dto
     */
    public void setModalidadeTipoServicoSaidaDTO(
        ListarModalidadeTipoServicoSaidaDTO modalidadeTipoServicoSaidaDTO) {
        this.modalidadeTipoServicoSaidaDTO = modalidadeTipoServicoSaidaDTO;
    }

    /**
     * Is habilita campo2.
     * 
     * @return true, if is habilita campo2
     */
    public boolean isHabilitaCampo2() {
        return habilitaCampo2;
    }

    /**
     * Set: habilitaCampo2.
     * 
     * @param habilitaCampo2
     *            the habilita campo2
     */
    public void setHabilitaCampo2(boolean habilitaCampo2) {
        this.habilitaCampo2 = habilitaCampo2;
    }

    /**
     * Get: entradaServicosModalidades.
     * 
     * @return entradaServicosModalidades
     */
    public ListarServModEntradaDTO getEntradaServicosModalidades() {
        return entradaServicosModalidades;
    }

    /**
     * Set: entradaServicosModalidades.
     * 
     * @param entradaServicosModalidades
     *            the entrada servicos modalidades
     */
    public void setEntradaServicosModalidades(
        ListarServModEntradaDTO entradaServicosModalidades) {
        this.entradaServicosModalidades = entradaServicosModalidades;
    }

    /**
     * Get: identificacaoClienteContratoBean.
     * 
     * @return identificacaoClienteContratoBean
     */
    public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
        return identificacaoClienteContratoBean;
    }

    /**
     * Set: identificacaoClienteContratoBean.
     * 
     * @param identificacaoClienteContratoBean
     *            the identificacao cliente contrato bean
     */
    public void setIdentificacaoClienteContratoBean(
        IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
        this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
    }

    /**
     * Get: manterContratoBean.
     * 
     * @return manterContratoBean
     */
    public ManterContratoBean getManterContratoBean() {
        return manterContratoBean;
    }

    /**
     * Set: manterContratoBean.
     * 
     * @param manterContratoBean
     *            the manter contrato bean
     */
    public void setManterContratoBean(ManterContratoBean manterContratoBean) {
        this.manterContratoBean = manterContratoBean;
    }

    /**
     * Get: cdAmbienteServicoContrato.
     * 
     * @return cdAmbienteServicoContrato
     */
    public String getCdAmbienteServicoContrato() {
        return cdAmbienteServicoContrato;
    }

    /**
     * Set: cdAmbienteServicoContrato.
     * 
     * @param cdAmbienteServicoContrato
     *            the cd ambiente servico contrato
     */
    public void setCdAmbienteServicoContrato(String cdAmbienteServicoContrato) {
        this.cdAmbienteServicoContrato = cdAmbienteServicoContrato;
    }

    /**
     * Get: cdParametroTela2.
     * 
     * @return cdParametroTela2
     */
    public Integer getCdParametroTela2() {
        return cdParametroTela2;
    }

    /**
     * Set: cdParametroTela2.
     * 
     * @param cdParametroTela2
     *            the cd parametro tela2
     */
    public void setCdParametroTela2(Integer cdParametroTela2) {
        this.cdParametroTela2 = cdParametroTela2;
    }

    /**
     * Get: itemSelecionadoSacadoEletronico.
     * 
     * @return itemSelecionadoSacadoEletronico
     */
    public Integer getItemSelecionadoSacadoEletronico() {
        return itemSelecionadoSacadoEletronico;
    }

    /**
     * Set: itemSelecionadoSacadoEletronico.
     * 
     * @param itemSelecionadoSacadoEletronico
     *            the item selecionado sacado eletronico
     */
    public void setItemSelecionadoSacadoEletronico(
        Integer itemSelecionadoSacadoEletronico) {
        this.itemSelecionadoSacadoEletronico = itemSelecionadoSacadoEletronico;
    }

    /**
     * Get: listaControleSacadoEletronico.
     * 
     * @return listaControleSacadoEletronico
     */
    public List<SelectItem> getListaControleSacadoEletronico() {
        return listaControleSacadoEletronico;
    }

    /**
     * Set: listaControleSacadoEletronico.
     * 
     * @param listaControleSacadoEletronico
     *            the lista controle sacado eletronico
     */
    public void setListaControleSacadoEletronico(
        List<SelectItem> listaControleSacadoEletronico) {
        this.listaControleSacadoEletronico = listaControleSacadoEletronico;
    }

    /**
     * Get: listaGridSacadoEletronico.
     * 
     * @return listaGridSacadoEletronico
     */
    public List<ListarParticipanteAgregadoSaidaDTO> getListaGridSacadoEletronico() {
        return listaGridSacadoEletronico;
    }

    /**
     * Set: listaGridSacadoEletronico.
     * 
     * @param listaGridSacadoEletronico
     *            the lista grid sacado eletronico
     */
    public void setListaGridSacadoEletronico(
        List<ListarParticipanteAgregadoSaidaDTO> listaGridSacadoEletronico) {
        this.listaGridSacadoEletronico = listaGridSacadoEletronico;
    }

    /**
     * Get: listaGridAgregados.
     * 
     * @return listaGridAgregados
     */
    public List<ListarAgregadoSaidaDTO> getListaGridAgregados() {
        return listaGridAgregados;
    }

    /**
     * Set: listaGridAgregados.
     * 
     * @param listaGridAgregados
     *            the lista grid agregados
     */
    public void setListaGridAgregados(
        List<ListarAgregadoSaidaDTO> listaGridAgregados) {
        this.listaGridAgregados = listaGridAgregados;
    }

    /**
     * Get: nomeRazaoSocial.
     * 
     * @return nomeRazaoSocial
     */
    public String getNomeRazaoSocial() {
        return nomeRazaoSocial;
    }

    /**
     * Set: nomeRazaoSocial.
     * 
     * @param nomeRazaoSocial
     *            the nome razao social
     */
    public void setNomeRazaoSocial(String nomeRazaoSocial) {
        this.nomeRazaoSocial = nomeRazaoSocial;
    }

    /**
     * Get: cpfCnpj.
     * 
     * @return cpfCnpj
     */
    public String getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Set: cpfCnpj.
     * 
     * @param cpfCnpj
     *            the cpf cnpj
     */
    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    /**
     * Get: dsLocalEmissao.
     * 
     * @return dsLocalEmissao
     */
    public String getDsLocalEmissao() {
        return dsLocalEmissao;
    }

    /**
     * Set: dsLocalEmissao.
     * 
     * @param dsLocalEmissao
     *            the ds local emissao
     */
    public void setDsLocalEmissao(String dsLocalEmissao) {
        this.dsLocalEmissao = dsLocalEmissao;
    }

    /**
     * Get: cdLocalEmissao.
     * 
     * @return cdLocalEmissao
     */
    public Integer getCdLocalEmissao() {
        return cdLocalEmissao;
    }

    /**
     * Set: cdLocalEmissao.
     * 
     * @param cdLocalEmissao
     *            the cd local emissao
     */
    public void setCdLocalEmissao(Integer cdLocalEmissao) {
        this.cdLocalEmissao = cdLocalEmissao;
    }

    /**
     * Get: listaModalidadeHistoricoHash.
     * 
     * @return listaModalidadeHistoricoHash
     */
    public Map<Integer, String> getListaModalidadeHistoricoHash() {
        return listaModalidadeHistoricoHash;
    }

    /**
     * Set lista modalidade historico hash.
     * 
     * @param listaModalidadeHistoricoHash
     *            the lista modalidade historico hash
     */
    public void setListaModalidadeHistoricoHash(
        Map<Integer, String> listaModalidadeHistoricoHash) {
        this.listaModalidadeHistoricoHash = listaModalidadeHistoricoHash;
    }

    /**
     * Get: dsSituacaoServico.
     * 
     * @return dsSituacaoServico
     */
    public String getDsSituacaoServico() {
        return dsSituacaoServico;
    }

    /**
     * Set: dsSituacaoServico.
     * 
     * @param dsSituacaoServico
     *            the ds situacao servico
     */
    public void setDsSituacaoServico(String dsSituacaoServico) {
        this.dsSituacaoServico = dsSituacaoServico;
    }

    /**
     * Get: dsSituacaoModalidade.
     * 
     * @return dsSituacaoModalidade
     */
    public String getDsSituacaoModalidade() {
        return dsSituacaoModalidade;
    }

    /**
     * Set: dsSituacaoModalidade.
     * 
     * @param dsSituacaoModalidade
     *            the ds situacao modalidade
     */
    public void setDsSituacaoModalidade(String dsSituacaoModalidade) {
        this.dsSituacaoModalidade = dsSituacaoModalidade;
    }

    /**
     * Get: dsSituacaoServicoRelacionado.
     * 
     * @return dsSituacaoServicoRelacionado
     */
    public String getDsSituacaoServicoRelacionado() {
        return dsSituacaoServicoRelacionado;
    }

    /**
     * Set: dsSituacaoServicoRelacionado.
     * 
     * @param dsSituacaoServicoRelacionado
     *            the ds situacao servico relacionado
     */
    public void setDsSituacaoServicoRelacionado(
        String dsSituacaoServicoRelacionado) {
        this.dsSituacaoServicoRelacionado = dsSituacaoServicoRelacionado;
    }

    /**
     * Get: dsTipoServico.
     * 
     * @return dsTipoServico
     */
    public String getDsTipoServico() {
        return dsTipoServico;
    }

    /**
     * Set: dsTipoServico.
     * 
     * @param dsTipoServico
     *            the ds tipo servico
     */
    public void setDsTipoServico(String dsTipoServico) {
        this.dsTipoServico = dsTipoServico;
    }

    /**
     * Get: listaCheckedGridModalidade.
     * 
     * @return listaCheckedGridModalidade
     */
    public List<ListarServicosSaidaDTO> getListaCheckedGridModalidade() {
        return listaCheckedGridModalidade;
    }

    /**
     * Set: listaCheckedGridModalidade.
     * 
     * @param listaCheckedGridModalidade
     *            the lista checked grid modalidade
     */
    public void setListaCheckedGridModalidade(
        List<ListarServicosSaidaDTO> listaCheckedGridModalidade) {
        this.listaCheckedGridModalidade = listaCheckedGridModalidade;
    }

    /**
     * Set: listaGridModalidade.
     * 
     * @param listaGridModalidade
     *            the lista grid modalidade
     */
    public void setListaGridModalidade(
        List<ListarServicosSaidaDTO> listaGridModalidade) {
        this.listaGridModalidade = listaGridModalidade;
    }

    /**
     * Get: listaGridModalidade.
     * 
     * @return listaGridModalidade
     */
    public List<ListarServicosSaidaDTO> getListaGridModalidade() {
        return listaGridModalidade;
    }

    /**
     * Get: dsCapituloTituloRegistro.
     * 
     * @return dsCapituloTituloRegistro
     */
    public String getDsCapituloTituloRegistro() {
        return dsCapituloTituloRegistro;
    }

    /**
     * Set: dsCapituloTituloRegistro.
     * 
     * @param dsCapituloTituloRegistro
     *            the ds capitulo titulo registro
     */
    public void setDsCapituloTituloRegistro(String dsCapituloTituloRegistro) {
        this.dsCapituloTituloRegistro = dsCapituloTituloRegistro;
    }

    /**
     * Is opcao checar todos.
     * 
     * @return true, if is opcao checar todos
     */
    public boolean isOpcaoChecarTodos() {
        return opcaoChecarTodos;
    }

    /**
     * Set: opcaoChecarTodos.
     * 
     * @param opcaoChecarTodos
     *            the opcao checar todos
     */
    public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
        this.opcaoChecarTodos = opcaoChecarTodos;
    }

    /**
     * Checar todos.
     */
    public void checarTodos() {
        for (ListarServicosSaidaDTO o : getListaGridTipoServico()) {
            o.setCheck(isOpcaoChecarTodos());
        }

    }

    /**
     * Is desabilita modalildade.
     * 
     * @return true, if is desabilita modalildade
     */
    public boolean isDesabilitaModalildade() {
        boolean valida = true;
        for (ListarServicosSaidaDTO o : getListaGridTipoServico()) {
            if (o.isCheck()) {
                valida = false;
                break;
            }
        }
        return valida;

    }

    /**
     * Get: manterAmbienteOperacaoContratoServiceImpl.
     * 
     * @return manterAmbienteOperacaoContratoServiceImpl
     */
    public IManterAmbienteOperacaoContratoService getManterAmbienteOperacaoContratoServiceImpl() {
        return manterAmbienteOperacaoContratoServiceImpl;
    }

    /**
     * Set: manterAmbienteOperacaoContratoServiceImpl.
     * 
     * @param manterAmbienteOperacaoContratoServiceImpl
     *            the manter ambiente operacao contrato service impl
     */
    public void setManterAmbienteOperacaoContratoServiceImpl(
        IManterAmbienteOperacaoContratoService manterAmbienteOperacaoContratoServiceImpl) {
        this.manterAmbienteOperacaoContratoServiceImpl = manterAmbienteOperacaoContratoServiceImpl;
    }

    /**
     * Get: listaGridModalidadeAux.
     * 
     * @return listaGridModalidadeAux
     */
    public List<ListarServicosSaidaDTO> getListaGridModalidadeAux() {
        return listaGridModalidadeAux;
    }

    /**
     * Set: listaGridModalidadeAux.
     * 
     * @param listaGridModalidadeAux
     *            the lista grid modalidade aux
     */
    public void setListaGridModalidadeAux(
        List<ListarServicosSaidaDTO> listaGridModalidadeAux) {
        this.listaGridModalidadeAux = listaGridModalidadeAux;
    }

    /**
     * Get: itemSelecionadoGridAmbiente.
     * 
     * @return itemSelecionadoGridAmbiente
     */
    public Integer getItemSelecionadoGridAmbiente() {
        return itemSelecionadoGridAmbiente;
    }

    /**
     * Set: itemSelecionadoGridAmbiente.
     * 
     * @param itemSelecionadoGridAmbiente
     *            the item selecionado grid ambiente
     */
    public void setItemSelecionadoGridAmbiente(
        Integer itemSelecionadoGridAmbiente) {
        this.itemSelecionadoGridAmbiente = itemSelecionadoGridAmbiente;
    }

    /**
     * Get: listaAmbienteControle.
     * 
     * @return listaAmbienteControle
     */
    public List<SelectItem> getListaAmbienteControle() {
        return listaAmbienteControle;
    }

    /**
     * Set: listaAmbienteControle.
     * 
     * @param listaAmbienteControle
     *            the lista ambiente controle
     */
    public void setListaAmbienteControle(List<SelectItem> listaAmbienteControle) {
        this.listaAmbienteControle = listaAmbienteControle;
    }

    /**
     * Get: tipoServicoSelecionado.
     * 
     * @return tipoServicoSelecionado
     */
    public Integer getTipoServicoSelecionado() {
        return tipoServicoSelecionado;
    }

    /**
     * Set: tipoServicoSelecionado.
     * 
     * @param tipoServicoSelecionado
     *            the tipo servico selecionado
     */
    public void setTipoServicoSelecionado(Integer tipoServicoSelecionado) {
        this.tipoServicoSelecionado = tipoServicoSelecionado;
    }

    /**
     * Get: listaComboTipoServicoAmbiente.
     * 
     * @return listaComboTipoServicoAmbiente
     */
    public List<SelectItem> getListaComboTipoServicoAmbiente() {
        return listaComboTipoServicoAmbiente;
    }

    /**
     * Set: listaComboTipoServicoAmbiente.
     * 
     * @param listaComboTipoServicoAmbiente
     *            the lista combo tipo servico ambiente
     */
    public void setListaComboTipoServicoAmbiente(
        List<SelectItem> listaComboTipoServicoAmbiente) {
        this.listaComboTipoServicoAmbiente = listaComboTipoServicoAmbiente;
    }

    /**
     * Get: listaComboTipoServicoAmbienteHash.
     * 
     * @return listaComboTipoServicoAmbienteHash
     */
    public Map<Integer, ListarModalidadeTipoServicoSaidaDTO> getListaComboTipoServicoAmbienteHash() {
        return listaComboTipoServicoAmbienteHash;
    }

    /**
     * Set lista combo tipo servico ambiente hash.
     * 
     * @param listaComboTipoServicoAmbienteHash
     *            the lista combo tipo servico ambiente hash
     */
    public void setListaComboTipoServicoAmbienteHash(
        Map<Integer, ListarModalidadeTipoServicoSaidaDTO> listaComboTipoServicoAmbienteHash) {
        this.listaComboTipoServicoAmbienteHash = listaComboTipoServicoAmbienteHash;
    }

    /**
     * Get: cpfCnpjParticipanteSelecionado.
     * 
     * @return cpfCnpjParticipanteSelecionado
     */
    public String getCpfCnpjParticipanteSelecionado() {
        return cpfCnpjParticipanteSelecionado;
    }

    /**
     * Set: cpfCnpjParticipanteSelecionado.
     * 
     * @param cpfCnpjParticipanteSelecionado
     *            the cpf cnpj participante selecionado
     */
    public void setCpfCnpjParticipanteSelecionado(
        String cpfCnpjParticipanteSelecionado) {
        this.cpfCnpjParticipanteSelecionado = cpfCnpjParticipanteSelecionado;
    }

    /**
     * Get: nomeRazaoParticipanteSelecionado.
     * 
     * @return nomeRazaoParticipanteSelecionado
     */
    public String getNomeRazaoParticipanteSelecionado() {
        return nomeRazaoParticipanteSelecionado;
    }

    /**
     * Set: nomeRazaoParticipanteSelecionado.
     * 
     * @param nomeRazaoParticipanteSelecionado
     *            the nome razao participante selecionado
     */
    public void setNomeRazaoParticipanteSelecionado(
        String nomeRazaoParticipanteSelecionado) {
        this.nomeRazaoParticipanteSelecionado = nomeRazaoParticipanteSelecionado;
    }

    /**
     * Set: listaAmbiente.
     * 
     * @param listaAmbiente
     *            the lista ambiente
     */
    public void setListaAmbiente(
        List<ListarAmbienteOcorrenciaSaidaDTO> listaAmbiente) {
        this.listaAmbiente = listaAmbiente;
    }

    /**
     * Get: listaAmbiente.
     * 
     * @return listaAmbiente
     */
    public List<ListarAmbienteOcorrenciaSaidaDTO> getListaAmbiente() {
        return listaAmbiente;
    }

    /**
     * Get: manterVincPerfilTrocaArqPartImpl.
     * 
     * @return manterVincPerfilTrocaArqPartImpl
     */
    public IManterVincPerfilTrocaArqPartService getManterVincPerfilTrocaArqPartImpl() {
        return manterVincPerfilTrocaArqPartImpl;
    }

    /**
     * Set: manterVincPerfilTrocaArqPartImpl.
     * 
     * @param manterVincPerfilTrocaArqPartImpl
     *            the manter vinc perfil troca arq part impl
     */
    public void setManterVincPerfilTrocaArqPartImpl(
        IManterVincPerfilTrocaArqPartService manterVincPerfilTrocaArqPartImpl) {
        this.manterVincPerfilTrocaArqPartImpl = manterVincPerfilTrocaArqPartImpl;
    }

    /**
     * Get: listaGridParticipantes.
     * 
     * @return listaGridParticipantes
     */
    public List<ListarParticipantesSaidaDTO> getListaGridParticipantes() {
        return listaGridParticipantes;
    }

    /**
     * Set: listaGridParticipantes.
     * 
     * @param listaGridParticipantes
     *            the lista grid participantes
     */
    public void setListaGridParticipantes(
        List<ListarParticipantesSaidaDTO> listaGridParticipantes) {
        this.listaGridParticipantes = listaGridParticipantes;
    }

    /**
     * Get: listaGridControleParticipantes.
     * 
     * @return listaGridControleParticipantes
     */
    public List<SelectItem> getListaGridControleParticipantes() {
        return listaGridControleParticipantes;
    }

    /**
     * Set: listaGridControleParticipantes.
     * 
     * @param listaGridControleParticipantes
     *            the lista grid controle participantes
     */
    public void setListaGridControleParticipantes(
        List<SelectItem> listaGridControleParticipantes) {
        this.listaGridControleParticipantes = listaGridControleParticipantes;
    }

    /**
     * Get: itemSelecionadoGridParticipantes.
     * 
     * @return itemSelecionadoGridParticipantes
     */
    public Integer getItemSelecionadoGridParticipantes() {
        return itemSelecionadoGridParticipantes;
    }

    /**
     * Set: itemSelecionadoGridParticipantes.
     * 
     * @param itemSelecionadoGridParticipantes
     *            the item selecionado grid participantes
     */
    public void setItemSelecionadoGridParticipantes(
        Integer itemSelecionadoGridParticipantes) {
        this.itemSelecionadoGridParticipantes = itemSelecionadoGridParticipantes;
    }

    /**
     * Get: participanteSelecionado.
     * 
     * @return participanteSelecionado
     */
    public ListarParticipantesSaidaDTO getParticipanteSelecionado() {
        return participanteSelecionado;
    }

    /**
     * Set: participanteSelecionado.
     * 
     * @param participanteSelecionado
     *            the participante selecionado
     */
    public void setParticipanteSelecionado(
        ListarParticipantesSaidaDTO participanteSelecionado) {
        this.participanteSelecionado = participanteSelecionado;
    }

    /**
     * Get: dsTipoServicoAmbiente.
     * 
     * @return dsTipoServicoAmbiente
     */
    public String getDsTipoServicoAmbiente() {
        return dsTipoServicoAmbiente;
    }

    /**
     * Get: dsAmbienteServico.
     * 
     * @return dsAmbienteServico
     */
    public String getDsAmbienteServico() {
        return dsAmbienteServico;
    }

    /**
     * Set: dsTipoServicoAmbiente.
     * 
     * @param dsTipoServicoAmbiente
     *            the ds tipo servico ambiente
     */
    public void setDsTipoServicoAmbiente(String dsTipoServicoAmbiente) {
        this.dsTipoServicoAmbiente = dsTipoServicoAmbiente;
    }

    /**
     * Set: dsAmbienteServico.
     * 
     * @param dsAmbienteServico
     *            the ds ambiente servico
     */
    public void setDsAmbienteServico(String dsAmbienteServico) {
        this.dsAmbienteServico = dsAmbienteServico;
    }

    /**
     * Get: dsNomeRazaoSocilaAmbiente.
     * 
     * @return dsNomeRazaoSocilaAmbiente
     */
    public String getDsNomeRazaoSocilaAmbiente() {
        return dsNomeRazaoSocilaAmbiente;
    }

    /**
     * Get: cpfCnpjAmbiente.
     * 
     * @return cpfCnpjAmbiente
     */
    public String getCpfCnpjAmbiente() {
        return cpfCnpjAmbiente;
    }

    /**
     * Set: dsNomeRazaoSocilaAmbiente.
     * 
     * @param dsNomeRazaoSocilaAmbiente
     *            the ds nome razao socila ambiente
     */
    public void setDsNomeRazaoSocilaAmbiente(String dsNomeRazaoSocilaAmbiente) {
        this.dsNomeRazaoSocilaAmbiente = dsNomeRazaoSocilaAmbiente;
    }

    /**
     * Set: cpfCnpjAmbiente.
     * 
     * @param cpfCnpjAmbiente
     *            the cpf cnpj ambiente
     */
    public void setCpfCnpjAmbiente(String cpfCnpjAmbiente) {
        this.cpfCnpjAmbiente = cpfCnpjAmbiente;
    }

    /**
     * Get: tipoServicoSelecionadoHistoricoAmbiente.
     * 
     * @return tipoServicoSelecionadoHistoricoAmbiente
     */
    public Integer getTipoServicoSelecionadoHistoricoAmbiente() {
        return tipoServicoSelecionadoHistoricoAmbiente;
    }

    /**
     * Set: tipoServicoSelecionadoHistoricoAmbiente.
     * 
     * @param tipoServicoSelecionadoHistoricoAmbiente
     *            the tipo servico selecionado historico ambiente
     */
    public void setTipoServicoSelecionadoHistoricoAmbiente(
        Integer tipoServicoSelecionadoHistoricoAmbiente) {
        this.tipoServicoSelecionadoHistoricoAmbiente = tipoServicoSelecionadoHistoricoAmbiente;
    }

    /**
     * Get: participanteSelecionadoHistorico.
     * 
     * @return participanteSelecionadoHistorico
     */
    public ListarParticipantesSaidaDTO getParticipanteSelecionadoHistorico() {
        return participanteSelecionadoHistorico;
    }

    /**
     * Set: participanteSelecionadoHistorico.
     * 
     * @param participanteSelecionadoHistorico
     *            the participante selecionado historico
     */
    public void setParticipanteSelecionadoHistorico(
        ListarParticipantesSaidaDTO participanteSelecionadoHistorico) {
        this.participanteSelecionadoHistorico = participanteSelecionadoHistorico;
    }

    /**
     * Get: itemSelecionadoListaHistoricoManutencaoAmbiente.
     * 
     * @return itemSelecionadoListaHistoricoManutencaoAmbiente
     */
    public Integer getItemSelecionadoListaHistoricoManutencaoAmbiente() {
        return itemSelecionadoListaHistoricoManutencaoAmbiente;
    }

    /**
     * Get: listaHistoricoManutencaoAmbiente.
     * 
     * @return listaHistoricoManutencaoAmbiente
     */
    public List<ListarConManAmbPartOcorrenciatSaidaDTO> getListaHistoricoManutencaoAmbiente() {
        return listaHistoricoManutencaoAmbiente;
    }

    /**
     * Get: listaControleHistoricoManutencaoAmbiente.
     * 
     * @return listaControleHistoricoManutencaoAmbiente
     */
    public List<SelectItem> getListaControleHistoricoManutencaoAmbiente() {
        return listaControleHistoricoManutencaoAmbiente;
    }

    /**
     * Set: itemSelecionadoListaHistoricoManutencaoAmbiente.
     * 
     * @param itemSelecionadoListaHistoricoManutencaoAmbiente
     *            the item selecionado lista historico manutencao ambiente
     */
    public void setItemSelecionadoListaHistoricoManutencaoAmbiente(
        Integer itemSelecionadoListaHistoricoManutencaoAmbiente) {
        this.itemSelecionadoListaHistoricoManutencaoAmbiente = itemSelecionadoListaHistoricoManutencaoAmbiente;
    }

    /**
     * Set: listaHistoricoManutencaoAmbiente.
     * 
     * @param listaHistoricoManutencaoAmbiente
     *            the lista historico manutencao ambiente
     */
    public void setListaHistoricoManutencaoAmbiente(
        List<ListarConManAmbPartOcorrenciatSaidaDTO> listaHistoricoManutencaoAmbiente) {
        this.listaHistoricoManutencaoAmbiente = listaHistoricoManutencaoAmbiente;
    }

    /**
     * Set: listaControleHistoricoManutencaoAmbiente.
     * 
     * @param listaControleHistoricoManutencaoAmbiente
     *            the lista controle historico manutencao ambiente
     */
    public void setListaControleHistoricoManutencaoAmbiente(
        List<SelectItem> listaControleHistoricoManutencaoAmbiente) {
        this.listaControleHistoricoManutencaoAmbiente = listaControleHistoricoManutencaoAmbiente;
    }

    /**
     * Get: dsTipoManutencao.
     * 
     * @return dsTipoManutencao
     */
    public String getDsTipoManutencao() {
        return dsTipoManutencao;
    }

    /**
     * Set: dsTipoManutencao.
     * 
     * @param dsTipoManutencao
     *            the ds tipo manutencao
     */
    public void setDsTipoManutencao(String dsTipoManutencao) {
        this.dsTipoManutencao = dsTipoManutencao;
    }

    /**
     * Is desabilita btn ambiente operacao.
     * 
     * @return true, if is desabilita btn ambiente operacao
     */
    public boolean isDesabilitaBtnAmbienteOperacao() {
        if (listaAmbiente != null && listaAmbiente.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Is desabilita btn ambiente operacao historico.
     * 
     * @return true, if is desabilita btn ambiente operacao historico
     */
    public boolean isDesabilitaBtnAmbienteOperacaoHistorico() {
        if (listaHistoricoManutencaoAmbiente != null
                        && listaHistoricoManutencaoAmbiente.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Get: listaComboTipoServicoHash.
     * 
     * @return listaComboTipoServicoHash
     */
    public Map<Integer, ListaTipoServicoContratoSaidaDTO> getListaComboTipoServicoHash() {
        return listaComboTipoServicoHash;
    }

    /**
     * Set lista combo tipo servico hash.
     * 
     * @param listaComboTipoServicoHash
     *            the lista combo tipo servico hash
     */
    public void setListaComboTipoServicoHash(
        Map<Integer, ListaTipoServicoContratoSaidaDTO> listaComboTipoServicoHash) {
        this.listaComboTipoServicoHash = listaComboTipoServicoHash;
    }

    /**
     * Get: cdConsultaSaldoValorSuperior.
     * 
     * @return cdConsultaSaldoValorSuperior
     */
    public Long getCdConsultaSaldoValorSuperior() {
        return cdConsultaSaldoValorSuperior;
    }

    /**
     * Set: cdConsultaSaldoValorSuperior.
     * 
     * @param cdConsultaSaldoValorSuperior
     *            the cd consulta saldo valor superior
     */
    public void setCdConsultaSaldoValorSuperior(
        Long cdConsultaSaldoValorSuperior) {
        this.cdConsultaSaldoValorSuperior = cdConsultaSaldoValorSuperior;
    }

    /**
     * Set: dsConsultaSaldoValorSuperior.
     * 
     * @param dsConsultaSaldoValorSuperior
     *            the ds consulta saldo valor superior
     */
    public void setDsConsultaSaldoValorSuperior(
        String dsConsultaSaldoValorSuperior) {
        this.dsConsultaSaldoValorSuperior = dsConsultaSaldoValorSuperior;
    }

    /**
     * Get: dsConsultaSaldoValorSuperior.
     * 
     * @return dsConsultaSaldoValorSuperior
     */
    public String getDsConsultaSaldoValorSuperior() {
        return dsConsultaSaldoValorSuperior;
    }

    /**
     * Get: feriadosLocais.
     * 
     * @return feriadosLocais
     */
    public List<SelectItem> getFeriadosLocais() {
        return feriadosLocais;
    }

    /**
     * Set: feriadosLocais.
     * 
     * @param feriadosLocais
     *            the feriados locais
     */
    public void setFeriadosLocais(List<SelectItem> feriadosLocais) {
        this.feriadosLocais = feriadosLocais;
    }

    /**
     * Get: cdIndicadorFeriadoLocal.
     * 
     * @return cdIndicadorFeriadoLocal
     */
    public Integer getCdIndicadorFeriadoLocal() {
        return cdIndicadorFeriadoLocal;
    }

    /**
     * Set: cdIndicadorFeriadoLocal.
     * 
     * @param cdIndicadorFeriadoLocal
     *            the cd indicador feriado local
     */
    public void setCdIndicadorFeriadoLocal(Integer cdIndicadorFeriadoLocal) {
        this.cdIndicadorFeriadoLocal = cdIndicadorFeriadoLocal;
    }

    /**
     * Is habilita campo tip servico fornc sal trib.
     * 
     * @return true, if is habilita campo tip servico fornc sal trib
     */
    public boolean isHabilitaCampoTipServicoForncSalTrib() {
        return habilitaCampoTipServicoForncSalTrib;
    }

    /**
     * Set: habilitaCampoTipServicoForncSalTrib.
     * 
     * @param habilitaCampoTipServicoForncSalTrib
     *            the habilita campo tip servico fornc sal trib
     */
    public void setHabilitaCampoTipServicoForncSalTrib(
        boolean habilitaCampoTipServicoForncSalTrib) {
        this.habilitaCampoTipServicoForncSalTrib = habilitaCampoTipServicoForncSalTrib;
    }

    /**
     * Get: feriadosLocaisHash.
     * 
     * @return feriadosLocaisHash
     */
    public Map<Integer, String> getFeriadosLocaisHash() {
        return feriadosLocaisHash;
    }

    /**
     * Set feriados locais hash.
     * 
     * @param feriadosLocaisHash
     *            the feriados locais hash
     */
    public void setFeriadosLocaisHash(Map<Integer, String> feriadosLocaisHash) {
        this.feriadosLocaisHash = feriadosLocaisHash;
    }

    /**
     * Get: dsIndicadorFeriadoLocal.
     * 
     * @return dsIndicadorFeriadoLocal
     */
    public String getDsIndicadorFeriadoLocal() {
        return dsIndicadorFeriadoLocal;
    }

    /**
     * Set: dsIndicadorFeriadoLocal.
     * 
     * @param dsIndicadorFeriadoLocal
     *            the ds indicador feriado local
     */
    public void setDsIndicadorFeriadoLocal(String dsIndicadorFeriadoLocal) {
        this.dsIndicadorFeriadoLocal = dsIndicadorFeriadoLocal;
    }

    /**
     * Set: rdoIndicadorSegundaLinhaExtrato.
     * 
     * @param rdoIndicadorSegundaLinhaExtrato
     *            the rdo indicador segunda linha extrato
     */
    public void setRdoIndicadorSegundaLinhaExtrato(
        Integer rdoIndicadorSegundaLinhaExtrato) {
        this.rdoIndicadorSegundaLinhaExtrato = rdoIndicadorSegundaLinhaExtrato;
    }

    /**
     * Get: rdoIndicadorSegundaLinhaExtrato.
     * 
     * @return rdoIndicadorSegundaLinhaExtrato
     */
    public Integer getRdoIndicadorSegundaLinhaExtrato() {
        return rdoIndicadorSegundaLinhaExtrato;
    }

    /**
     * Nome: getRdoTipoFormatacaoPrimeiraLinha
     *
     * @return rdoTipoFormatacaoPrimeiraLinha
     */
    public String getRdoTipoFormatacaoPrimeiraLinha() {
        return rdoTipoFormatacaoPrimeiraLinha;
    }

    /**
     * Nome: setRdoTipoFormatacaoPrimeiraLinha
     *
     * @param rdoTipoFormatacaoPrimeiraLinha
     */
    public void setRdoTipoFormatacaoPrimeiraLinha(String rdoTipoFormatacaoPrimeiraLinha) {
        this.rdoTipoFormatacaoPrimeiraLinha = rdoTipoFormatacaoPrimeiraLinha;
    }

    /**
     * Set: dsIndicadorSegundaLinhaExtrato.
     * 
     * @param dsIndicadorSegundaLinhaExtrato
     *            the ds indicador segunda linha extrato
     */
    public void setDsIndicadorSegundaLinhaExtrato(
        String dsIndicadorSegundaLinhaExtrato) {
        this.dsIndicadorSegundaLinhaExtrato = dsIndicadorSegundaLinhaExtrato;
    }

    /**
     * Get: dsIndicadorSegundaLinhaExtrato.
     * 
     * @return dsIndicadorSegundaLinhaExtrato
     */
    public String getDsIndicadorSegundaLinhaExtrato() {
        return dsIndicadorSegundaLinhaExtrato;
    }

    /**
     * Gets the lista feriado nacional.
     *
     * @return the lista feriado nacional
     */
    public List<ConsultarListaValoresDiscretosSaidaDTO> getListaFeriadoNacional() {
        return listaFeriadoNacional;
    }

    /**
     * Sets the lista feriado nacional.
     *
     * @param listaFeriadoNacional the new lista feriado nacional
     */
    public void setListaFeriadoNacional(
        List<ConsultarListaValoresDiscretosSaidaDTO> listaFeriadoNacional) {
        this.listaFeriadoNacional = listaFeriadoNacional;
    }

    /**
     * Gets the rdto cadastro floating.
     *
     * @return the rdto cadastro floating
     */
    public Integer getRdtoCadastroFloating() {
        return rdtoCadastroFloating;
    }

    /**
     * Sets the rdto cadastro floating.
     *
     * @param rdtoCadastroFloating the new rdto cadastro floating
     */
    public void setRdtoCadastroFloating(Integer rdtoCadastroFloating) {
        this.rdtoCadastroFloating = rdtoCadastroFloating;
    }

    /**
     * Gets the desc rdto cadastro floating.
     *
     * @return the desc rdto cadastro floating
     */
    public String getDescRdtoCadastroFloating() {
        return descRdtoCadastroFloating;
    }

    /**
     * Sets the desc rdto cadastro floating.
     *
     * @param descRdtoCadastroFloating the new desc rdto cadastro floating
     */
    public void setDescRdtoCadastroFloating(String descRdtoCadastroFloating) {
        this.descRdtoCadastroFloating = descRdtoCadastroFloating;
    }

    /**
     * Nome: getSaidaTipoLayoutArquivoSaidaDTO.
     *
     * @return saidaTipoLayoutArquivoSaidaDTO
     */
    public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
        return saidaTipoLayoutArquivoSaidaDTO;
    }

    /**
     * Nome: setSaidaTipoLayoutArquivoSaidaDTO.
     *
     * @param saidaTipoLayoutArquivoSaidaDTO the new saida tipo layout arquivo saida dto
     */
    public void setSaidaTipoLayoutArquivoSaidaDTO(
        TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
        this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
    }

    /**
     * Checks if is habilita tipo data controle floating.
     *
     * @return the habilitaTipoDataControleFloating
     */
    public boolean isHabilitaTipoDataControleFloating() {
        return habilitaTipoDataControleFloating;
    }

    /**
     * Sets the habilita tipo data controle floating.
     *
     * @param habilitaTipoDataControleFloating the habilitaTipoDataControleFloating to set
     */
    public void setHabilitaTipoDataControleFloating(
        boolean habilitaTipoDataControleFloating) {
        this.habilitaTipoDataControleFloating = habilitaTipoDataControleFloating;
    }

    /**
     * Sets the permite alteracao combo floating.
     *
     * @param permiteAlteracaoComboFloating the permiteAlteracaoComboFloating to set
     */
    public void setPermiteAlteracaoComboFloating(
        boolean permiteAlteracaoComboFloating) {
        this.permiteAlteracaoComboFloating = permiteAlteracaoComboFloating;
    }

    /**
     * Checks if is permite alteracao combo floating.
     *
     * @return the permiteAlteracaoComboFloating
     */
    public boolean isPermiteAlteracaoComboFloating() {
        return permiteAlteracaoComboFloating;
    }

    /**
     * Sets the permite alteracao combo.
     *
     * @param permiteAlteracaoCombo the permiteAlteracaoCombo to set
     */
    public void setPermiteAlteracaoCombo(boolean permiteAlteracaoCombo) {
        this.permiteAlteracaoCombo = permiteAlteracaoCombo;
    }

    /**
     * Checks if is permite alteracao combo.
     *
     * @return the permiteAlteracaoCombo
     */
    public boolean isPermiteAlteracaoCombo() {
        return permiteAlteracaoCombo;
    }

    /**
     * Gets the cod tela.
     *
     * @return the codTela
     */
    public Integer getCodTela() {
        return codTela;
    }

    /**
     * Sets the cod tela.
     *
     * @param codTela the codTela to set
     */
    public void setCodTela(Integer codTela) {
        this.codTela = codTela;
    }

    /**
     * Checks if is permite alteracao radio floating.
     *
     * @return the permiteAlteracaoRadioFloating
     */
    public boolean isPermiteAlteracaoRadioFloating() {
        return permiteAlteracaoRadioFloating;
    }

    /**
     * Sets the permite alteracao radio floating.
     *
     * @param permiteAlteracaoRadioFloating the permiteAlteracaoRadioFloating to set
     */
    public void setPermiteAlteracaoRadioFloating(
        boolean permiteAlteracaoRadioFloating) {
        this.permiteAlteracaoRadioFloating = permiteAlteracaoRadioFloating;
    }

    /**
     * Gets the cd flag representante.
     *
     * @return the cdFlagRepresentante
     */
    public String getCdFlagRepresentante() {
        return cdFlagRepresentante;
    }

    /**
     * Sets the cd flag representante.
     *
     * @param cdFlagRepresentante the cdFlagRepresentante to set
     */
    public void setCdFlagRepresentante(String cdFlagRepresentante) {
        this.cdFlagRepresentante = cdFlagRepresentante;
    }

    /**
     * Gets the cd midia dispon comprovante.
     *
     * @return the cdMidiaDisponComprovante
     */
    public String getCdMidiaDisponComprovante() {
        return cdMidiaDisponComprovante;
    }

    /**
     * Sets the cd midia dispon comprovante.
     *
     * @param cdMidiaDisponComprovante the cdMidiaDisponComprovante to set
     */
    public void setCdMidiaDisponComprovante(String cdMidiaDisponComprovante) {
        this.cdMidiaDisponComprovante = cdMidiaDisponComprovante;
    }

    /**
     * Gets the qt funcionarios.
     *
     * @return the qtFuncionarios
     */
    public Long getQtFuncionarios() {
        return qtFuncionarios;
    }

    /**
     * Sets the qt funcionarios.
     *
     * @param qtFuncionarios the qtFuncionarios to set
     */
    public void setQtFuncionarios(Long qtFuncionarios) {
        this.qtFuncionarios = qtFuncionarios;
    }

    /**
     * Gets the qt meses emissao.
     *
     * @return the qtMesesEmissao
     */
    public Integer getQtMesesEmissao() {
        return qtMesesEmissao;
    }

    /**
     * Sets the qt meses emissao.
     *
     * @param qtMesesEmissao the qtMesesEmissao to set
     */
    public void setQtMesesEmissao(Integer qtMesesEmissao) {
        this.qtMesesEmissao = qtMesesEmissao;
    }

    /**
     * Gets the qt emissoes pagas empresa.
     *
     * @return the qtEmissoesPagasEmpresa
     */
    public String getQtEmissoesPagasEmpresa() {
        return qtEmissoesPagasEmpresa;
    }

    /**
     * Sets the qt emissoes pagas empresa.
     *
     * @param qtEmissoesPagasEmpresa the qtEmissoesPagasEmpresa to set
     */
    public void setQtEmissoesPagasEmpresa(String qtEmissoesPagasEmpresa) {
        this.qtEmissoesPagasEmpresa = qtEmissoesPagasEmpresa;
    }

    /**
     * Gets the lista representantes.
     *
     * @return the listaRepresentantes
     */
    public ListarRepresentanteSaidaDTO getListaRepresentantes() {
        return listaRepresentantes;
    }

    /**
     * Sets the lista representantes.
     *
     * @param listaRepresentantes the listaRepresentantes to set
     */
    public void setListaRepresentantes(
        ListarRepresentanteSaidaDTO listaRepresentantes) {
        this.listaRepresentantes = listaRepresentantes;
    }

    /**
     * Gets the lista representantes ocorrencias selecionadas.
     *
     * @return the listaRepresentantesOcorrenciasSelecionadas
     */
    public List<ListarRepresentanteOcorrenciaDTO> getListaRepresentantesOcorrenciasSelecionadas() {
        return listaRepresentantesOcorrenciasSelecionadas;
    }

    /**
     * Sets the lista representantes ocorrencias selecionadas.
     *
     * @param listaRepresentantesOcorrenciasSelecionadas the listaRepresentantesOcorrenciasSelecionadas to set
     */
    public void setListaRepresentantesOcorrenciasSelecionadas(
        List<ListarRepresentanteOcorrenciaDTO> listaRepresentantesOcorrenciasSelecionadas) {
        this.listaRepresentantesOcorrenciasSelecionadas = listaRepresentantesOcorrenciasSelecionadas;
    }

    /**
     * Nome: getCdProdutoOperacaoRelacionado.
     *
     * @return cdProdutoOperacaoRelacionado
     */
    public Integer getCdProdutoOperacaoRelacionado() {
        return cdProdutoOperacaoRelacionado;
    }

    /**
     * Nome: setCdProdutoOperacaoRelacionado.
     *
     * @param cdProdutoOperacaoRelacionado the new cd produto operacao relacionado
     */
    public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
        this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
    }

    /**
     * Nome: getCdProdutoServicoOperacao.
     *
     * @return cdProdutoServicoOperacao
     */
    public Integer getCdProdutoServicoOperacao() {
        return cdProdutoServicoOperacao;
    }

    /**
     * Nome: setCdProdutoServicoOperacao.
     *
     * @param cdProdutoServicoOperacao the new cd produto servico operacao
     */
    public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
        this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
    }

    /**
     * Sets the permite alteracoes titulo rastreado.
     *
     * @param permiteAlteracoesTituloRastreado the permiteAlteracoesTituloRastreado to set
     */
    public void setPermiteAlteracoesTituloRastreado(
        boolean permiteAlteracoesTituloRastreado) {
        this.permiteAlteracoesTituloRastreado = permiteAlteracoesTituloRastreado;
    }

    /**
     * Checks if is permite alteracoes titulo rastreado.
     *
     * @return the permiteAlteracoesTituloRastreado
     */
    public boolean isPermiteAlteracoesTituloRastreado() {
        return permiteAlteracoesTituloRastreado;
    }

    /**
     * Sets the rdo exige identificacao filial autorizacao.
     *
     * @param rdoExigeIdentificacaoFilialAutorizacao the new rdo exige identificacao filial autorizacao
     */
    public void setRdoExigeIdentificacaoFilialAutorizacao(
        String rdoExigeIdentificacaoFilialAutorizacao) {
        this.rdoExigeIdentificacaoFilialAutorizacao = rdoExigeIdentificacaoFilialAutorizacao;
    }

    /**
     * Gets the rdo exige identificacao filial autorizacao.
     *
     * @return the rdo exige identificacao filial autorizacao
     */
    public String getRdoExigeIdentificacaoFilialAutorizacao() {
        return rdoExigeIdentificacaoFilialAutorizacao;
    }

    /**
     * Sets the flag exibe campos.
     *
     * @param flagExibeCampos the new flag exibe campos
     */
    public void setFlagExibeCampos(String flagExibeCampos) {
        this.flagExibeCampos = flagExibeCampos;
    }

    /**
     * Gets the flag exibe campos.
     *
     * @return the flag exibe campos
     */
    public String getFlagExibeCampos() {
        return flagExibeCampos;
    }

    /**
     * Checks if is habilita qtd dias floating.
     *
     * @return true, if is habilita qtd dias floating
     */
    public boolean isHabilitaQtdDiasFloating() {
        return habilitaQtdDiasFloating;
    }

    /**
     * Sets the habilita qtd dias floating.
     *
     * @param habilitaQtdDiasFloating the new habilita qtd dias floating
     */
    public void setHabilitaQtdDiasFloating(boolean habilitaQtdDiasFloating) {
        this.habilitaQtdDiasFloating = habilitaQtdDiasFloating;
    }

    /**
     * Checks if is habilita rdo gerar lancamento.
     *
     * @return true, if is habilita rdo gerar lancamento
     */
    public boolean isHabilitaRdoGerarLancamento() {
        return habilitaRdoGerarLancamento;
    }

    /**
     * Sets the habilita rdo gerar lancamento.
     *
     * @param habilitaRdoGerarLancamento the new habilita rdo gerar lancamento
     */
    public void setHabilitaRdoGerarLancamento(boolean habilitaRdoGerarLancamento) {
        this.habilitaRdoGerarLancamento = habilitaRdoGerarLancamento;
    }

    /**
     * Gets the descricao segunda modalidade.
     *
     * @return the descricaoSegundaModalidade
     */
    public String getDescricaoSegundaModalidade() {
        return descricaoSegundaModalidade;
    }

    /**
     * Sets the descricao segunda modalidade.
     *
     * @param descricaoSegundaModalidade the descricaoSegundaModalidade to set
     */
    public void setDescricaoSegundaModalidade(String descricaoSegundaModalidade) {
        this.descricaoSegundaModalidade = descricaoSegundaModalidade;
    }

    /**
     * Gets the entrada listar dados cta convn.
     *
     * @return the entradaListarDadosCtaConvn
     */
    public ListarDadosCtaConvnEntradaDTO getEntradaListarDadosCtaConvn() {
        return entradaListarDadosCtaConvn;
    }

    /**
     * Sets the entrada listar dados cta convn.
     *
     * @param entradaListarDadosCtaConvn the entradaListarDadosCtaConvn to set
     */
    public void setEntradaListarDadosCtaConvn(
        ListarDadosCtaConvnEntradaDTO entradaListarDadosCtaConvn) {
        this.entradaListarDadosCtaConvn = entradaListarDadosCtaConvn;
    }

    /**
     * Gets the saida listar dados cta convn.
     *
     * @return the saidaListarDadosCtaConvn
     */
    public ListarDadosCtaConvnSaidaDTO getSaidaListarDadosCtaConvn() {
        return saidaListarDadosCtaConvn;
    }

    /**
     * Sets the saida listar dados cta convn.
     *
     * @param saidaListarDadosCtaConvn the saidaListarDadosCtaConvn to set
     */
    public void setSaidaListarDadosCtaConvn(
        ListarDadosCtaConvnSaidaDTO saidaListarDadosCtaConvn) {
        this.saidaListarDadosCtaConvn = saidaListarDadosCtaConvn;
    }

    /**
     * Gets the lista contratos pgit.
     *
     * @return the listaContratosPgit
     */
    public List<ListarContratosPgitSaidaDTO> getListaContratosPgit() {
        return listaContratosPgit;
    }

    /**
     * Sets the lista contratos pgit.
     *
     * @param listaContratosPgit the listaContratosPgit to set
     */
    public void setListaContratosPgit(
        List<ListarContratosPgitSaidaDTO> listaContratosPgit) {
        this.listaContratosPgit = listaContratosPgit;
    }

    /**
     * Gets the manter vinculacao convenio conta salario service.
     *
     * @return the manterVinculacaoConvenioContaSalarioService
     */
    public IManterVinculacaoConvenioContaSalarioService getManterVinculacaoConvenioContaSalarioService() {
        return manterVinculacaoConvenioContaSalarioService;
    }

    /**
     * Sets the manter vinculacao convenio conta salario service.
     *
     * @param manterVinculacaoConvenioContaSalarioService the manterVinculacaoConvenioContaSalarioService to set
     */
    public void setManterVinculacaoConvenioContaSalarioService(
        IManterVinculacaoConvenioContaSalarioService manterVinculacaoConvenioContaSalarioService) {
        this.manterVinculacaoConvenioContaSalarioService = manterVinculacaoConvenioContaSalarioService;
    }

    /**
     * Gets the abertura conta salario.
     *
     * @return the aberturaContaSalario
     */
    public String getAberturaContaSalario() {
        return aberturaContaSalario;
    }

    /**
     * Sets the abertura conta salario.
     *
     * @param aberturaContaSalario the aberturaContaSalario to set
     */
    public void setAberturaContaSalario(String aberturaContaSalario) {
        this.aberturaContaSalario = aberturaContaSalario;
    }

    /**
     * Gets the txt valor folha pgamento.
     *
     * @return the txtValorFolhaPgamento
     */
    public BigDecimal getTxtValorFolhaPgamento() {
        return txtValorFolhaPgamento;
    }

    /**
     * Sets the txt valor folha pgamento.
     *
     * @param txtValorFolhaPgamento the txtValorFolhaPgamento to set
     */
    public void setTxtValorFolhaPgamento(BigDecimal txtValorFolhaPgamento) {
        this.txtValorFolhaPgamento = txtValorFolhaPgamento;
    }

    /**
     * Gets the txt qtd funcionarios.
     *
     * @return the txtQtdFuncionarios
     */
    public String getTxtQtdFuncionarios() {
        return txtQtdFuncionarios;
    }

    /**
     * Sets the txt qtd funcionarios.
     *
     * @param txtQtdFuncionarios the txtQtdFuncionarios to set
     */
    public void setTxtQtdFuncionarios(String txtQtdFuncionarios) {
        this.txtQtdFuncionarios = txtQtdFuncionarios;
    }

    /**
     * Gets the txt media salarial.
     *
     * @return the txtMediaSalarial
     */
    public BigDecimal getTxtMediaSalarial() {
        return txtMediaSalarial;
    }

    /**
     * Sets the txt media salarial.
     *
     * @param txtMediaSalarial the txtMediaSalarial to set
     */
    public void setTxtMediaSalarial(BigDecimal txtMediaSalarial) {
        this.txtMediaSalarial = txtMediaSalarial;
    }

    /**
     * Gets the txt mes ano correspondente.
     *
     * @return the txtMesAnoCorrespondente
     */
    public String getTxtMesAnoCorrespondente() {
        return txtMesAnoCorrespondente;
    }

    /**
     * Sets the txt mes ano correspondente.
     *
     * @param txtMesAnoCorrespondente the txtMesAnoCorrespondente to set
     */
    public void setTxtMesAnoCorrespondente(String txtMesAnoCorrespondente) {
        this.txtMesAnoCorrespondente = txtMesAnoCorrespondente;
    }

    /**
     * Gets the foco.
     *
     * @return the foco
     */
    public String getFoco() {
        return foco;
    }

    /**
     * Sets the foco.
     *
     * @param foco the foco to set
     */
    public void setFoco(String foco) {
        this.foco = foco;
    }

    /**
     * Checks if is chk lote quinzenal.
     *
     * @return the chkLoteQuinzenal
     */
    public boolean isChkLoteQuinzenal() {
        return chkLoteQuinzenal;
    }

    /**
     * Sets the chk lote quinzenal.
     *
     * @param chkLoteQuinzenal the chkLoteQuinzenal to set
     */
    public void setChkLoteQuinzenal(boolean chkLoteQuinzenal) {
        this.chkLoteQuinzenal = chkLoteQuinzenal;
    }

    /**
     * Checks if is chk lote mensal.
     *
     * @return the chkLoteMensal
     */
    public boolean isChkLoteMensal() {
        return chkLoteMensal;
    }

    /**
     * Sets the chk lote mensal.
     *
     * @param chkLoteMensal the chkLoteMensal to set
     */
    public void setChkLoteMensal(boolean chkLoteMensal) {
        this.chkLoteMensal = chkLoteMensal;
    }

    /**
     * Gets the entrada validar vinculacao convenio conta.
     *
     * @return the entradaValidarVinculacaoConvenioConta
     */
    public ValidarVinculacaoConvenioContaSalarioEntradaDTO getEntradaValidarVinculacaoConvenioConta() {
        return entradaValidarVinculacaoConvenioConta;
    }

    /**
     * Sets the entrada validar vinculacao convenio conta.
     *
     * @param entradaValidarVinculacaoConvenioConta the entradaValidarVinculacaoConvenioConta to set
     */
    public void setEntradaValidarVinculacaoConvenioConta(
        ValidarVinculacaoConvenioContaSalarioEntradaDTO entradaValidarVinculacaoConvenioConta) {
        this.entradaValidarVinculacaoConvenioConta = entradaValidarVinculacaoConvenioConta;
    }

    /**
     * Gets the saida validar vinculacao convenio conta.
     *
     * @return the saidaValidarVinculacaoConvenioConta
     */
    public ValidarVinculacaoConvenioContaSalarioSaidaDTO getSaidaValidarVinculacaoConvenioConta() {
        return saidaValidarVinculacaoConvenioConta;
    }

    /**
     * Sets the saida validar vinculacao convenio conta.
     *
     * @param saidaValidarVinculacaoConvenioConta the saidaValidarVinculacaoConvenioConta to set
     */
    public void setSaidaValidarVinculacaoConvenioConta(
        ValidarVinculacaoConvenioContaSalarioSaidaDTO saidaValidarVinculacaoConvenioConta) {
        this.saidaValidarVinculacaoConvenioConta = saidaValidarVinculacaoConvenioConta;
    }

    /**
     * Gets the item selecionad lista banco.
     *
     * @return the itemSelecionadListaBanco
     */
    public Integer getItemSelecionadListaBanco() {
        return itemSelecionadListaBanco;
    }

    /**
     * Sets the item selecionad lista banco.
     *
     * @param itemSelecionadListaBanco the itemSelecionadListaBanco to set
     */
    public void setItemSelecionadListaBanco(Integer itemSelecionadListaBanco) {
        this.itemSelecionadListaBanco = itemSelecionadListaBanco;
    }

    /**
     * Gets the abertura conta salario conf.
     *
     * @return the aberturaContaSalarioConf
     */
    public String getAberturaContaSalarioConf() {
        return aberturaContaSalarioConf;
    }

    /**
     * Sets the abertura conta salario conf.
     *
     * @param aberturaContaSalarioConf the aberturaContaSalarioConf to set
     */
    public void setAberturaContaSalarioConf(String aberturaContaSalarioConf) {
        this.aberturaContaSalarioConf = aberturaContaSalarioConf;
    }

    /**
     * Gets the tipo lote pagamento conf.
     *
     * @return the tipoLotePagamento
     */
    public String getTipoLotePagamentoConf() {
        return tipoLotePagamentoConf;
    }

    /**
     * Sets the tipo lote pagamento conf.
     *
     * @param tipoLotePagamentoConf the new tipo lote pagamento conf
     */
    public void setTipoLotePagamentoConf(String tipoLotePagamentoConf) {
        this.tipoLotePagamentoConf = tipoLotePagamentoConf;
    }

    /**
     * Gets the txt valor folha pgamento conf.
     *
     * @return the txtValorFolhaPgamentoConf
     */
    public BigDecimal getTxtValorFolhaPgamentoConf() {
        return txtValorFolhaPgamentoConf;
    }

    /**
     * Sets the txt valor folha pgamento conf.
     *
     * @param txtValorFolhaPgamentoConf the txtValorFolhaPgamentoConf to set
     */
    public void setTxtValorFolhaPgamentoConf(BigDecimal txtValorFolhaPgamentoConf) {
        this.txtValorFolhaPgamentoConf = txtValorFolhaPgamentoConf;
    }

    /**
     * Gets the txt qtd funcionarios conf.
     *
     * @return the txtQtdFuncionariosConf
     */
    public String getTxtQtdFuncionariosConf() {
        return txtQtdFuncionariosConf;
    }

    /**
     * Sets the txt qtd funcionarios conf.
     *
     * @param txtQtdFuncionariosConf the txtQtdFuncionariosConf to set
     */
    public void setTxtQtdFuncionariosConf(String txtQtdFuncionariosConf) {
        this.txtQtdFuncionariosConf = txtQtdFuncionariosConf;
    }

    /**
     * Gets the txt media salarial conf.
     *
     * @return the txtMediaSalarialConf
     */
    public BigDecimal getTxtMediaSalarialConf() {
        return txtMediaSalarialConf;
    }

    /**
     * Sets the txt media salarial conf.
     *
     * @param txtMediaSalarialConf the txtMediaSalarialConf to set
     */
    public void setTxtMediaSalarialConf(BigDecimal txtMediaSalarialConf) {
        this.txtMediaSalarialConf = txtMediaSalarialConf;
    }

    /**
     * Gets the txt mes ano correspondente conf.
     *
     * @return the txtMesAnoCorrespondenteConf
     */
    public String getTxtMesAnoCorrespondenteConf() {
        return txtMesAnoCorrespondenteConf;
    }

    /**
     * Sets the txt mes ano correspondente conf.
     *
     * @param txtMesAnoCorrespondenteConf the txtMesAnoCorrespondenteConf to set
     */
    public void setTxtMesAnoCorrespondenteConf(String txtMesAnoCorrespondenteConf) {
        this.txtMesAnoCorrespondenteConf = txtMesAnoCorrespondenteConf;
    }

    /**
     * Gets the lista saida listar dados cta convn.
     *
     * @return the listaSaidaListarDadosCtaConvn
     */
    public List<ListarDadosCtaConvnListaOcorrenciasDTO> getListaSaidaListarDadosCtaConvn() {
        return listaSaidaListarDadosCtaConvn;
    }

    /**
     * Sets the lista saida listar dados cta convn.
     *
     * @param listaSaidaListarDadosCtaConvn the listaSaidaListarDadosCtaConvn to set
     */
    public void setListaSaidaListarDadosCtaConvn(
        List<ListarDadosCtaConvnListaOcorrenciasDTO> listaSaidaListarDadosCtaConvn) {
        this.listaSaidaListarDadosCtaConvn = listaSaidaListarDadosCtaConvn;
    }

    /**
     * Gets the entrada incluir pagamento salario.
     *
     * @return the entradaIncluirPagamentoSalario
     */
    public IncluirPagamentoSalarioEntradaDTO getEntradaIncluirPagamentoSalario() {
        return entradaIncluirPagamentoSalario;
    }

    /**
     * Sets the entrada incluir pagamento salario.
     *
     * @param entradaIncluirPagamentoSalario the entradaIncluirPagamentoSalario to set
     */
    public void setEntradaIncluirPagamentoSalario(
        IncluirPagamentoSalarioEntradaDTO entradaIncluirPagamentoSalario) {
        this.entradaIncluirPagamentoSalario = entradaIncluirPagamentoSalario;
    }

    /**
     * Gets the saida incluir pagamento salario.
     *
     * @return the saidaIncluirPagamentoSalario
     */
    public IncluirPagamentoSalarioSaidaDTO getSaidaIncluirPagamentoSalario() {
        return saidaIncluirPagamentoSalario;
    }

    /**
     * Sets the saida incluir pagamento salario.
     *
     * @param saidaIncluirPagamentoSalario the saidaIncluirPagamentoSalario to set
     */
    public void setSaidaIncluirPagamentoSalario(
        IncluirPagamentoSalarioSaidaDTO saidaIncluirPagamentoSalario) {
        this.saidaIncluirPagamentoSalario = saidaIncluirPagamentoSalario;
    }

    /**
     * Sets the url renegociacao novo.
     *
     * @param urlRenegociacaoNovo the new url renegociacao novo
     */
    public void setUrlRenegociacaoNovo(String urlRenegociacaoNovo) {
        this.urlRenegociacaoNovo = urlRenegociacaoNovo;
    }

    /**
     * Gets the url renegociacao novo.
     *
     * @return the url renegociacao novo
     */
    public String getUrlRenegociacaoNovo() {
        return urlRenegociacaoNovo;
    }

    /**
     * Gets the lista controle radio dados conta.
     *
     * @return the listaControleRadioDadosConta
     */
    public List<SelectItem> getListaControleRadioDadosConta() {
        return listaControleRadioDadosConta;
    }

    /**
     * Sets the lista controle radio dados conta.
     *
     * @param listaControleRadioDadosConta the listaControleRadioDadosConta to set
     */
    public void setListaControleRadioDadosConta(
        List<SelectItem> listaControleRadioDadosConta) {
        this.listaControleRadioDadosConta = listaControleRadioDadosConta;
    }

    public boolean isDesabilitarComboPrioridadeDebito() {
        return desabilitarComboPrioridadeDebito;
    }

    public void setDesabilitarComboPrioridadeDebito(
        boolean desabilitarComboPrioridadeDebito) {
        this.desabilitarComboPrioridadeDebito = desabilitarComboPrioridadeDebito;
    }

    public boolean isDesabilitarComboTipoConsultaSaldo() {
        return desabilitarComboTipoConsultaSaldo;
    }

    public void setDesabilitarComboTipoConsultaSaldo(
        boolean desabilitarComboTipoConsultaSaldo) {
        this.desabilitarComboTipoConsultaSaldo = desabilitarComboTipoConsultaSaldo;
    }

    public boolean isDesabilitaIndicadorAutorizacaoComplemento() {
        return desabilitaIndicadorAutorizacaoComplemento;
    }

    public void setDesabilitaIndicadorAutorizacaoComplemento(
        boolean desabilitaIndicadorAutorizacaoComplemento) {
        this.desabilitaIndicadorAutorizacaoComplemento = desabilitaIndicadorAutorizacaoComplemento;
    }

    public boolean isDesabilitaCdIndicadorListaDebito() {
        return desabilitaCdIndicadorListaDebito;
    }

    public void setDesabilitaCdIndicadorListaDebito(
        boolean desabilitaCdIndicadorListaDebito) {
        this.desabilitaCdIndicadorListaDebito = desabilitaCdIndicadorListaDebito;
    }

    public boolean isDesabilitaCdIndicadorTipoFormacaoLista() {
        return desabilitaCdIndicadorTipoFormacaoLista;
    }

    public void setDesabilitaCdIndicadorTipoFormacaoLista(
        boolean desabilitaCdIndicadorTipoFormacaoLista) {
        this.desabilitaCdIndicadorTipoFormacaoLista = desabilitaCdIndicadorTipoFormacaoLista;
    }

    public boolean isDesabilitaCdIndicadorTipoConsistenciaLista() {
        return desabilitaCdIndicadorTipoConsistenciaLista;
    }

    public void setDesabilitaCdIndicadorTipoConsistenciaLista(
        boolean desabilitaCdIndicadorTipoConsistenciaLista) {
        this.desabilitaCdIndicadorTipoConsistenciaLista = desabilitaCdIndicadorTipoConsistenciaLista;
    }

    public boolean isDesabilitaGerarLancamentoProgramado() {
        return desabilitaGerarLancamentoProgramado;
    }

    public void setDesabilitaGerarLancamentoProgramado(
        boolean desabilitaGerarLancamentoProgramado) {
        this.desabilitaGerarLancamentoProgramado = desabilitaGerarLancamentoProgramado;
    }

    public boolean isDesabilitaIndicadorRejeicaoAgendaLote() {
        return desabilitaIndicadorRejeicaoAgendaLote;
    }

    public void setDesabilitaIndicadorRejeicaoAgendaLote(
        boolean desabilitaIndicadorRejeicaoAgendaLote) {
        this.desabilitaIndicadorRejeicaoAgendaLote = desabilitaIndicadorRejeicaoAgendaLote;
    }

    public boolean isDesabilitaIndicadorRejeicaoEfetivacaoLote() {
        return desabilitaIndicadorRejeicaoEfetivacaoLote;
    }

    public void setDesabilitaIndicadorRejeicaoEfetivacaoLote(
        boolean desabilitaIndicadorRejeicaoEfetivacaoLote) {
        this.desabilitaIndicadorRejeicaoEfetivacaoLote = desabilitaIndicadorRejeicaoEfetivacaoLote;
    }

    public boolean isDesabilitaIndicadorMaximaInconLote() {
        return desabilitaIndicadorMaximaInconLote;
    }

    public void setDesabilitaIndicadorMaximaInconLote(
        boolean desabilitaIndicadorMaximaInconLote) {
        this.desabilitaIndicadorMaximaInconLote = desabilitaIndicadorMaximaInconLote;
    }

    public boolean isDesabilitaIndicadorPercentualMaximoInconLote() {
        return desabilitaIndicadorPercentualMaximoInconLote;
    }

    public void setDesabilitaIndicadorPercentualMaximoInconLote(
        boolean desabilitaIndicadorPercentualMaximoInconLote) {
        this.desabilitaIndicadorPercentualMaximoInconLote = desabilitaIndicadorPercentualMaximoInconLote;
    }

    /**
     * @param renderizaCamposDebitosPendentesDeVeiculos the renderizaCamposDebitosPendentesDeVeiculos to set
     */
    public void setRenderizaCamposDebitosPendentesDeVeiculos(
        boolean renderizaCamposDebitosPendentesDeVeiculos) {
        this.renderizaCamposDebitosPendentesDeVeiculos = renderizaCamposDebitosPendentesDeVeiculos;
    }

    /**
     * @return the renderizaCamposDebitosPendentesDeVeiculos
     */
    public boolean isRenderizaCamposDebitosPendentesDeVeiculos() {
        return renderizaCamposDebitosPendentesDeVeiculos;
    }

    /**
     * Nome: getDsPreenchimentoLancamentoPersonalizado
     *
     * @return dsPreenchimentoLancamentoPersonalizado
     */
    public String getDsPreenchimentoLancamentoPersonalizado() {
        return dsPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: setDsPreenchimentoLancamentoPersonalizado
     *
     * @param dsPreenchimentoLancamentoPersonalizado
     */
    public void setDsPreenchimentoLancamentoPersonalizado(String dsPreenchimentoLancamentoPersonalizado) {
        this.dsPreenchimentoLancamentoPersonalizado = dsPreenchimentoLancamentoPersonalizado;
    }

    /**
     * Nome: getDsTituloDdaRetorno
     *
     * @return dsTituloDdaRetorno
     */
    public String getDsTituloDdaRetorno() {
        return dsTituloDdaRetorno;
    }

    /**
     * Nome: setDsTituloDdaRetorno
     *
     * @param dsTituloDdaRetorno
     */
    public void setDsTituloDdaRetorno(String dsTituloDdaRetorno) {
        this.dsTituloDdaRetorno = dsTituloDdaRetorno;
    }

    /**
     * Nome: getDsIndicadorAgendaGrade
     *
     * @return dsIndicadorAgendaGrade
     */
    public String getDsIndicadorAgendaGrade() {
        return dsIndicadorAgendaGrade;
    }

    /**
     * Nome: setDsIndicadorAgendaGrade
     *
     * @param dsIndicadorAgendaGrade
     */
    public void setDsIndicadorAgendaGrade(String dsIndicadorAgendaGrade) {
        this.dsIndicadorAgendaGrade = dsIndicadorAgendaGrade;
    }

    /**
     * Nome: getRdoIndicadorDdaRetorno
     *
     * @return rdoIndicadorDdaRetorno
     */
    public String getRdoIndicadorDdaRetorno() {
        return rdoIndicadorDdaRetorno;
    }

    /**
     * Nome: setRdoIndicadorDdaRetorno
     *
     * @param rdoIndicadorDdaRetorno
     */
    public void setRdoIndicadorDdaRetorno(String rdoIndicadorDdaRetorno) {
        this.rdoIndicadorDdaRetorno = rdoIndicadorDdaRetorno;
    }

    /**
     * Nome: getRdoPermiteAgAutGrade
     *
     * @return rdoPermiteAgAutGrade
     */
    public String getRdoPermiteAgAutGrade() {
        return rdoPermiteAgAutGrade;
    }

    /**
     * Nome: setRdoPermiteAgAutGrade
     *
     * @param rdoPermiteAgAutGrade
     */
    public void setRdoPermiteAgAutGrade(String rdoPermiteAgAutGrade) {
        this.rdoPermiteAgAutGrade = rdoPermiteAgAutGrade;
    }

    public Integer getQtdDiasReutilizacaoControlePgto() {
        return qtdDiasReutilizacaoControlePgto;
    }

    public void setQtdDiasReutilizacaoControlePgto(
        Integer qtdDiasReutilizacaoControlePgto) {
        this.qtdDiasReutilizacaoControlePgto = qtdDiasReutilizacaoControlePgto;
    }

    public String getRdoExigeCpfCnpj() {
        return rdoExigeCpfCnpj;
    }

    public void setRdoExigeCpfCnpj(String rdoExigeCpfCnpj) {
        this.rdoExigeCpfCnpj = rdoExigeCpfCnpj;
    }

    public String getRdoValidaCamara() {
        return rdoValidaCamara;
    }

    public void setRdoValidaCamara(String rdoValidaCamara) {
        this.rdoValidaCamara = rdoValidaCamara;
    }

    public String getRdoConsisteConta() {
        return rdoConsisteConta;
    }

    public void setRdoConsisteConta(String rdoConsisteConta) {
        this.rdoConsisteConta = rdoConsisteConta;
    }

    public Integer getQtDiaUtilPgto() {
        return qtDiaUtilPgto;
    }

    public void setQtDiaUtilPgto(Integer qtDiaUtilPgto) {
        this.qtDiaUtilPgto = qtDiaUtilPgto;
    }

    public String getDsIndicadorUtilizaMora() {
        return dsIndicadorUtilizaMora;
    }

    public void setDsIndicadorUtilizaMora(String dsIndicadorUtilizaMora) {
        this.dsIndicadorUtilizaMora = dsIndicadorUtilizaMora;
    }

    public String getRdoUtilizaMora() {
        return rdoUtilizaMora;
    }

    public void setRdoUtilizaMora(String rdoUtilizaMora) {
        this.rdoUtilizaMora = rdoUtilizaMora;
    }

    /**
     * Nome: getComboRetornoOperacoesSelecionado
     *
     * @return comboRetornoOperacoesSelecionado
     */
    public Long getComboRetornoOperacoesSelecionado() {
        return comboRetornoOperacoesSelecionado;
    }

    /**
     * Nome: setComboRetornoOperacoesSelecionado
     *
     * @param comboRetornoOperacoesSelecionado
     */
    public void setComboRetornoOperacoesSelecionado(Long comboRetornoOperacoesSelecionado) {
        this.comboRetornoOperacoesSelecionado = comboRetornoOperacoesSelecionado;
    }

    /**
     * Nome: getListaRetornoOperacoes
     *
     * @return listaRetornoOperacoes
     */
    public List<SelectItem> getListaRetornoOperacoes() {
        return listaRetornoOperacoes;
    }

    /**
     * Nome: setListaRetornoOperacoes
     *
     * @param listaRetornoOperacoes
     */
    public void setListaRetornoOperacoes(List<SelectItem> listaRetornoOperacoes) {
        this.listaRetornoOperacoes = listaRetornoOperacoes;
    }

    /**
     * Nome: getDsRetornoOperacoes
     *
     * @return dsRetornoOperacoes
     */
    public String getDsRetornoOperacoes() {
        return dsRetornoOperacoes;
    }

    /**
     * Nome: setDsRetornoOperacoes
     *
     * @param dsRetornoOperacoes
     */
    public void setDsRetornoOperacoes(String dsRetornoOperacoes) {
        this.dsRetornoOperacoes = dsRetornoOperacoes;
    }

    /**
     * Nome: getVlPercentualDiferencaTolerada
     *
     * @return vlPercentualDiferencaTolerada
     */
    public BigDecimal getVlPercentualDiferencaTolerada() {
        return vlPercentualDiferencaTolerada;
    }

    /**
     * Nome: setVlPercentualDiferencaTolerada
     *
     * @param vlPercentualDiferencaTolerada
     */
    public void setVlPercentualDiferencaTolerada(BigDecimal vlPercentualDiferencaTolerada) {
        this.vlPercentualDiferencaTolerada = vlPercentualDiferencaTolerada;
    }

    public Integer getCdFloatServicoContrato() {
        return cdFloatServicoContrato;
    }

    public void setCdFloatServicoContrato(Integer cdFloatServicoContrato) {
        this.cdFloatServicoContrato = cdFloatServicoContrato;
    }

    public String getDsTipoDataFloating() {
        return dsTipoDataFloating;
    }

    public void setDsTipoDataFloating(String dsTipoDataFloating) {
        this.dsTipoDataFloating = dsTipoDataFloating;
    }

	public List<SelectItem> getListaDemonstra2LinhaExtrato() {
		return listaDemonstra2LinhaExtrato;
	}

	public void setListaDemonstra2LinhaExtrato(
			List<SelectItem> listaDemonstra2LinhaExtrato) {
		this.listaDemonstra2LinhaExtrato = listaDemonstra2LinhaExtrato;
	}

	public Map<Long, String> getListaDemonstra2LinhaExtratoHash() {
		return listaDemonstra2LinhaExtratoHash;
	}

	public void setListaDemonstra2LinhaExtratoHash(
			Map<Long, String> listaDemonstra2LinhaExtratoHash) {
		this.listaDemonstra2LinhaExtratoHash = listaDemonstra2LinhaExtratoHash;
	}

	public String getDsRdoIndicadorSegundaLinhaExtrato() {
		return dsRdoIndicadorSegundaLinhaExtrato;
	}

	public void setDsRdoIndicadorSegundaLinhaExtrato(
			String dsRdoIndicadorSegundaLinhaExtrato) {
		this.dsRdoIndicadorSegundaLinhaExtrato = dsRdoIndicadorSegundaLinhaExtrato;
	}

	public boolean isExibeOrigemSegundaViaExtrato() {
		return exibeOrigemSegundaViaExtrato;
	}

	public void setExibeOrigemSegundaViaExtrato(boolean exibeOrigemSegundaViaExtrato) {
		this.exibeOrigemSegundaViaExtrato = exibeOrigemSegundaViaExtrato;
	}

	public void setRadioFidelize(String radioFidelize) {
		this.radioFidelize = radioFidelize;
	}

	public String getRadioFidelize() {
		return radioFidelize;
	}

	public void setDsEmailEmpresa(String dsEmailEmpresa) {
		this.dsEmailEmpresa = dsEmailEmpresa;
	}

	public String getDsEmailEmpresa() {
		return dsEmailEmpresa;
	}

	public void setDsEmailAgencia(String dsEmailAgencia) {
		this.dsEmailAgencia = dsEmailAgencia;
	}

	public String getDsEmailAgencia() {
		return dsEmailAgencia;
	}
	
    public String getFidelizeConf() {
		return fidelizeConf;
	}

	public void setFidelizeConf(String fidelizeConf) {
		this.fidelizeConf = fidelizeConf;
	}
	
    public boolean isDesabilitarCboConsistenciaCpfCnpjBenefAvalNpc() {
        return desabilitarCboConsistenciaCpfCnpjBenefAvalNpc;
    }

    public void setDesabilitarCboConsistenciaCpfCnpjBenefAvalNpc(
        boolean desabilitarCboConsistenciaCpfCnpjBenefAvalNpc) {
        this.desabilitarComboTipoConsultaSaldo = desabilitarCboConsistenciaCpfCnpjBenefAvalNpc;
    }

	public long getCdConsistenciaCpfCnpjBenefAvalNpc() {
		return cdConsistenciaCpfCnpjBenefAvalNpc;
	}

	public void setCdConsistenciaCpfCnpjBenefAvalNpc(
			long cdConsistenciaCpfCnpjBenefAvalNpc) {
		this.cdConsistenciaCpfCnpjBenefAvalNpc = cdConsistenciaCpfCnpjBenefAvalNpc;
	}

	public void setListaComboTeste(List<ConsultarListaValoresDiscretosSaidaDTO> listaComboTeste) {
		this.listaComboTeste = listaComboTeste;
	}

	public List<ConsultarListaValoresDiscretosSaidaDTO> getListaComboTeste() {
		return listaComboTeste;
	}

	public void setListaOrigem(List<SelectItem> listaOrigem) {
		this.listaOrigem = listaOrigem;
	}

	public List<SelectItem> getListaOrigem() {
		return listaOrigem;
	}

	public void setListaOrigemHash(Map<Long, String> listaOrigemHash) {
		this.listaOrigemHash = listaOrigemHash;
	}

	public Map<Long, String> getListaOrigemHash() {
		return listaOrigemHash;
	}

	public void setTesteOrigem(Long testeOrigem) {
		this.testeOrigem = testeOrigem;
	}

	public Long getTesteOrigem() {
		return testeOrigem;
	}

	public void setDsOrigem(String dsOrigem) {
		this.dsOrigem = dsOrigem;
	}

	public String getDsOrigem() {
		return dsOrigem;
	}

	public void setDsIndicador(String dsIndicador) {
		this.dsIndicador = dsIndicador;
	}

	public String getDsIndicador() {
		return dsIndicador;
	}

	public void setDsOrigemIndicador(int dsOrigemIndicador) {
		this.dsOrigemIndicador = dsOrigemIndicador;
	}

	public int getDsOrigemIndicador() {
		return dsOrigemIndicador;
	}	

	public String getAltoTurnover() {
		return altoTurnover;
	}

	public void setAltoTurnover(String altoTurnover) {
		this.altoTurnover = altoTurnover;
	}

	public String getOfertaCartaoPre() {
		return ofertaCartaoPre;
	}

	public void setOfertaCartaoPre(String ofertaCartaoPre) {
		this.ofertaCartaoPre = ofertaCartaoPre;
	}

	public String getPeloApp() {
		return peloApp;
	}

	public void setPeloApp(String peloApp) {
		this.peloApp = peloApp;
	}

	public String getAltoTurnoverConf() {
		return altoTurnoverConf;
	}

	public void setAltoTurnoverConf(String altoTurnoverConf) {
		this.altoTurnoverConf = altoTurnoverConf;
	}

	public String getOfertaCartaoPreConf() {
		return ofertaCartaoPreConf;
	}

	public void setOfertaCartaoPreConf(String ofertaCartaoPreConf) {
		this.ofertaCartaoPreConf = ofertaCartaoPreConf;
	}

	public String getPeloAppConf() {
		return peloAppConf;
	}

	public void setPeloAppConf(String peloAppConf) {
		this.peloAppConf = peloAppConf;
	}

	public List<SelectItem> getListaCodIndicadorTipoRetornoInternet() {
		return listaCodIndicadorTipoRetornoInternet;
	}

	public void setListaCodIndicadorTipoRetornoInternet(List<SelectItem> listaCodIndicadorTipoRetornoInternet) {
		this.listaCodIndicadorTipoRetornoInternet = listaCodIndicadorTipoRetornoInternet;
	}

	public String getDsCodIndicadorTipoRetornoInternet() {
		return dsCodIndicadorTipoRetornoInternet;
	}

	public void setDsCodIndicadorTipoRetornoInternet(
			String dsCodIndicadorTipoRetornoInternet) {
		this.dsCodIndicadorTipoRetornoInternet = dsCodIndicadorTipoRetornoInternet;
	}

	public List<String> getListaTipoRetornoInternet() {
		return listaTipoRetornoInternet;
	}

	public void setListaTipoRetornoInternet(List<String> listaTipoRetornoInternet) {
		this.listaTipoRetornoInternet = listaTipoRetornoInternet;
	}

	public Long getComboCodIndicadorTipoRetornoInternet() {
		return comboCodIndicadorTipoRetornoInternet;
	}

	public void setComboCodIndicadorTipoRetornoInternet(
			Long comboCodIndicadorTipoRetornoInternet) {
		this.comboCodIndicadorTipoRetornoInternet = comboCodIndicadorTipoRetornoInternet;
	}

	public Integer getRenderizaCombo() {
		return renderizaCombo;
	}

	public void setRenderizaCombo(Integer renderizaCombo) {
		this.renderizaCombo = renderizaCombo;
	}

	public String getDescTipoManutencao() {
		return descTipoManutencao;
	}

	public void setDescTipoManutencao(String descTipoManutencao) {
		this.descTipoManutencao = descTipoManutencao;
	}
	
}
