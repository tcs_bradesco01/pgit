/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.alterarValorTarifa
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.alterarValorTarifa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.exception.PgitException;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarTarifaAcimaPadraoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarTarifaAcimaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarTarifaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarTarifaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.
IManterVinculacaoConvenioContaSalarioService;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.
DetalharVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.
DetalharVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.
ListarDadosCtaConvnEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.
ListarDadosCtaConvnListaOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.
filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: AlterarValorTarifaBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AlterarValorTarifaBean {

	/** Atributo manterVinculacaoConvenioContaSalarioService. */
	private IManterVinculacaoConvenioContaSalarioService manterVinculacaoConvenioContaSalarioService = null;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean = null;

	/** Atributo comboService. */
	private IComboService comboService = null;

	/** Atributo identificacaoClienteBean. */
	private IdentificacaoClienteBean identificacaoClienteBean = null;

	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean = null;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo dtoConvenio. */
	private DetalharVincConvnCtaSalarioSaidaDTO dtoConvenio = new DetalharVincConvnCtaSalarioSaidaDTO();

	/** Atributo listaSelConvenio. */
	private List<ListarDadosCtaConvnListaOcorrenciasDTO> listaSelConvenio = 
		new ArrayList<ListarDadosCtaConvnListaOcorrenciasDTO>();

	/** Atributo listaControleSelConvenio. */
	private List<SelectItem> listaControleSelConvenio = new ArrayList<SelectItem>();

	/** Atributo itemSelecionadoListaConvenio. */
	private Integer itemSelecionadoListaConvenio = null;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista = null;

	/** Atributo foco. */
	private String foco = null;

	/** Atributo habilitaBtoCont. */
	private Boolean habilitaBtoCont = null;

	/** Atributo btoAcao. */
	private String btoAcao = null;

	/** Atributo codigoConvenio. */
	private Long codigoConvenio = null;

	/** Atributo decricaoConvenio. */
	private String decricaoConvenio = null;

	/** Atributo situacaoConvenio. */
	private String situacaoConvenio = null;

	/** Atributo bancoAgenciaContaConvenio. */
	private String bancoAgenciaContaConvenio = null;

	/** Atributo dtHoraInclusao. */
	private String dtHoraInclusao = null;

	/** Atributo cdUsuario. */
	private String cdUsuario = null;

	// Cliente Representante
	/** Atributo cpfCnpjRepresentante. */
	private String cpfCnpjRepresentante = null;

	/** Atributo nomeRazaoRepresentante. */
	private String nomeRazaoRepresentante = null;

	/** Atributo grupEconRepresentante. */
	private String grupEconRepresentante = null;

	/** Atributo ativEconRepresentante. */
	private String ativEconRepresentante = null;

	/** Atributo segRepresentante. */
	private String segRepresentante = null;

	/** Atributo subSegRepresentante. */
	private String subSegRepresentante = null;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora = null;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel = null;

	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado = null;

	// Cliente Participante
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante = null;

	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante = null;

	/** Atributo cpfCnpjParticipanteTemp. */
	private String cpfCnpjParticipanteTemp = null;

	/** Atributo nomeRazaoParticipanteTemp. */
	private String nomeRazaoParticipanteTemp = null;

	/** Atributo cpfCnpjParticipanteDet. */
	private String cpfCnpjParticipanteDet = null;

	/** Atributo nomeRazaoParticipanteDet. */
	private String nomeRazaoParticipanteDet = null;

	/** Atributo grupoEconParticipante. */
	private String grupoEconParticipante = null;

	/** Atributo atividadeEconParticipante. */
	private String atividadeEconParticipante = null;

	/** Atributo segmentoParticipante. */
	private String segmentoParticipante = null;

	/** Atributo subSegmentoParticipante. */
	private String subSegmentoParticipante = null;

	/** Atributo dsTipoParticipacao. */
	private String dsTipoParticipacao = null;

	/** Atributo situacaoParticipacao. */
	private String situacaoParticipacao = null;

	/** Atributo dsTipoServico. */
	private String dsTipoServico = null;

	// Contrato
	/** Atributo empresaContrato. */
	private String empresaContrato = null;

	/** Atributo tipoContrato. */
	private String tipoContrato = null;

	/** Atributo numeroContrato. */
	private String numeroContrato = null;

	/** Atributo descricaoContrato. */
	private String descricaoContrato = null;

	/** Atributo situacaoContrato. */
	private String situacaoContrato = null;

	/** Atributo motivoContrato. */
	private String motivoContrato = null;

	/** Atributo participacaoContrato. */
	private String participacaoContrato = null;

	/** Atributo dtManutencaoInicio. */
	private Date dtManutencaoInicio = null;

	/** Atributo dtManutencaoFim. */
	private Date dtManutencaoFim = null;

	/* Trilha de Auditoria */
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao = null;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao = null;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao = null;

	/** Atributo complementoManutencao. */
	private String complementoManutencao = null;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao = null;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao = null;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao = null;

	/** Atributo complementoInclusao. */
	private String complementoInclusao = null;

	private ListarDadosCtaConvnSaidaDTO saidaListarDadosCtaConvn = null;
	
	/** Atributo paginaRetorno. */
	private String paginaRetorno = null;

	/** Atributo saidaAlterarTarifaAcimaPadrao. */
	private AlterarTarifaAcimaPadraoSaidaDTO saidaAlterarTarifaAcimaPadrao = 
		new AlterarTarifaAcimaPadraoSaidaDTO();
	
	/** Atributo listaGridPesquisaAterarValorTarifa. */
	private List<ConsultarTarifaContratoSaidaDTO> listaGridPesquisaAterarValorTarifa = null;
	
	/** Atributo listaControleRadioGridPesquisaAltValTarifa. */
	private List<SelectItem> listaControleRadioGridPesquisaAltValTarifa = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecListaGridPesquisaAltValTarifa. */
	private Integer itemSelecListaGridPesquisaAltValTarifa = null;
	
	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl = null;
	
	/** Atributo tipoServico. */
	private String tipoServico = null;
	
	/** Atributo modalidade. */
	private String modalidade = null;
	
	/** Atributo operacao. */
	private String operacao = null;
	
	/** Atributo tarifaContratada. */
	private String tarifaContratada = null;
	
	/** Atributo tarifaMaximaPadrao. */
	private String tarifaMaximaPadrao = null;
	
	/** Atributo valorRefernciaMinima. */
	private String valorRefernciaMinima = null;
	
	/** Atributo dtInicioVigencia. */
	private String dtInicioVigencia = null;
	
	/** Atributo dtFimVigencia. */
	private String dtFimVigencia = null;
	
	/** Atributo valorTarifa. */
	private BigDecimal valorTarifa = null;

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		this.identificacaoClienteContratoBean.setPaginaRetorno("conAlterarValorTarifa");
		this.identificacaoClienteBean.setPaginaRetorno("conAlterarValorTarifa");

		filtroAgendamentoEfetivacaoEstornoBean.setEntradaConsultarListaClientePessoas(
				new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.setSaidaConsultarListaClientePessoas(
				new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean.setPaginaRetorno("conAlterarValorTarifa");

		// Limpar Variaveis
		setListaControleRadio(null);
		setItemSelecionadoLista(null);

		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
		setHabilitaBtoCont(true);

		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(
				new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(
				new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteManterContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conAlterarValorTarifa");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
	}

	/**
	 * Selecionar.
	 *
	 * @return the string
	 */
	public String selecionar() {
		setBtoAcao("");
		setPaginaRetorno("listAlterarValorTarifa");

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
				identificacaoClienteContratoBean.getItemSelecionadoLista());

		// detalharVinculacao();

		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + 
				saidaDTO.getDescFuncionarioBradesco());

		// Cliente Participante
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ||
				"".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().
						getCnpjOuCpfFormatado())) {

			setCpfCnpjParticipante("");
			setNomeRazaoParticipante("");

		} else {
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().
					getCnpjOuCpfFormatado() == null ? "" : identificacaoClienteContratoBean.
							getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().
					getDsNomeRazao() == null ? "" : identificacaoClienteContratoBean.
							getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}

		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());

		carregaListaConsultar();
		isDesabilitaBtnAlterar();

		return getPaginaRetorno();
	}

	/**
	 * Carrega lista consultar.
	 */
	public void carregaListaConsultar() {
		setListaGridPesquisaAterarValorTarifa(new ArrayList<ConsultarTarifaContratoSaidaDTO>());

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
				identificacaoClienteContratoBean.getItemSelecionadoLista());

		try {
			ConsultarTarifaContratoEntradaDTO entradaDTO = new ConsultarTarifaContratoEntradaDTO();

			/* Dados do Contrato */
			entradaDTO.setCdPessoaJuridica(saidaDTO.getCdPessoaJuridica());
			entradaDTO.setCdTipoContrato(saidaDTO.getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(saidaDTO.getNrSequenciaContrato());

			/* Dados Tarifa */
			entradaDTO.setCdTipoServico(0);
			entradaDTO.setCdModalidade(0);
			entradaDTO.setCdRelacionamentoProduto(0);

			setListaGridPesquisaAterarValorTarifa(getManterContratoImpl().consultarTarifaContrato(entradaDTO));

			this.listaControleRadioGridPesquisaAltValTarifa = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridPesquisaAterarValorTarifa().size(); i++) {
				listaControleRadioGridPesquisaAltValTarifa.add(new SelectItem(i, " "));
			}

			setItemSelecListaGridPesquisaAltValTarifa(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " +
					p.getMessage(), false);
			setListaGridPesquisaAterarValorTarifa(null);
			setItemSelecListaGridPesquisaAltValTarifa(null);
		}
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		ConsultarTarifaContratoSaidaDTO saidaListaDTO = listaGridPesquisaAterarValorTarifa.get(
				itemSelecListaGridPesquisaAltValTarifa);

		setTipoServico(saidaListaDTO.getDsProduto());
		setModalidade(saidaListaDTO.getDsProdutoServicoRelacionado());
		setOperacao(saidaListaDTO.getDsOperacao());
		setTarifaContratada(saidaListaDTO.getVlTarifa());
		setTarifaMaximaPadrao(saidaListaDTO.getVlrReferenciaMaximo());
		setValorRefernciaMinima(saidaListaDTO.getVlrReferenciaMinimo());
		setDtInicioVigencia(saidaListaDTO.getDtInicioVigencia());
		setDtFimVigencia(saidaListaDTO.getDtFimVigencia());
		setValorTarifa(null);

		return "alterarValorTarifa";
	}

	/**
	 * Avancar.
	 *
	 * @return the string
	 */
	public String avancar() {
		try {
			if(isDesabilitaBtnAvancar()){
				throw new PgitException("� necess�rio preencher o campo Valor!", "");
			}
			
			setValorTarifa(getValorTarifa());
			return "confirmarAlterarValorTarifa";
		} catch (Exception  e) {
			BradescoFacesUtils.addInfoModalMessage(e.getMessage(), false);
		}
		return "";
	}

	/**
	 * Confirmar.
	 *
	 * @return the string
	 */
	public String confirmar() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
				identificacaoClienteContratoBean.getItemSelecionadoLista());

		ConsultarTarifaContratoSaidaDTO saidaListaDTO = listaGridPesquisaAterarValorTarifa.get(
				itemSelecListaGridPesquisaAltValTarifa);

		try {
			AlterarTarifaAcimaPadraoEntradaDTO entrada = new AlterarTarifaAcimaPadraoEntradaDTO();

			entrada.setCdOperacao(saidaListaDTO.getCdOperacao());
			entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
			entrada.setCdProduto(saidaListaDTO.getCdProduto());
			entrada.setCdProdutoServicoRelacionado(saidaListaDTO.getCdProdutoServicoRelacionado());
			entrada.setCdRelacionamentoProduto(1);
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
			entrada.setVlrTarifaContratoNegocio(getValorTarifa());

			saidaAlterarTarifaAcimaPadrao = getManterContratoImpl().alterarTarifaAcimaPadrao(entrada);
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(saidaAlterarTarifaAcimaPadrao.
					getCodMensagem(), 8) + ") " + saidaAlterarTarifaAcimaPadrao.getMensagem(), false);

		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + 
					e.getMessage(), false);
			return "";
		}

		carregaListaConsultar();

		return "listAlterarValorTarifa";
	}

	/**
	 * Is desabilita btn alterar.
	 *
	 * @return true, if is desabilita btn alterar
	 */
	public boolean isDesabilitaBtnAlterar() {
		if (itemSelecListaGridPesquisaAltValTarifa != null) {
			return false;
		}
		return true;
	}

	/**
	 * Is desabilita btn avancar.
	 *
	 * @return true, if is desabilita btn avancar
	 */
	public boolean isDesabilitaBtnAvancar() {
		if (getValorTarifa() != null) {
			return false;
		}
		return true;
	}

	/**
	 * Voltar consulta contrato.
	 *
	 * @return the string
	 */
	public String voltarConsultaContrato() {
		return "conAlterarValorTarifa";
	}

	/**
	 * Voltar lista tarifa.
	 *
	 * @return the string
	 */
	public String voltarListaTarifa() {
		return "listAlterarValorTarifa";
	}

	/**
	 * Voltar alterar valor tarifa.
	 *
	 * @return the string
	 */
	public String voltarAlterarValorTarifa() {
		return "alterarValorTarifa";
	}

	/**
	 * Detalhar vinculacao.
	 */
	public void detalharVinculacao() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
				identificacaoClienteContratoBean.getItemSelecionadoLista());

		try {
			DetalharVincConvnCtaSalarioEntradaDTO entrada = new DetalharVincConvnCtaSalarioEntradaDTO();

			entrada.setCdPessoaJuridica(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContrato(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContrato(saidaDTO.getNrSequenciaContrato());

			dtoConvenio = getManterVinculacaoConvenioContaSalarioService().detalharVincConvnCtaSalario(entrada);

			dataHoraManutencao = FormatarData.formatarDataTrilha(dtoConvenio.getDtManutencao() + "-" + 
					dtoConvenio.getHrManutencao());

			usuarioManutencao = dtoConvenio.getCdUsuarioManutencao();

			tipoCanalManutencao = dtoConvenio.getCdCanalManutencao().compareTo(0) == 0 ? "" : 
				dtoConvenio.getCdCanalManutencao().toString() + "-" + (dtoConvenio.getDsTipoCanalManutencao());

			complementoManutencao = "0".equals(dtoConvenio.getCdOperacaoFluxoManutencao()) ? "" : 
				dtoConvenio.getCdOperacaoFluxoManutencao();

			dataHoraInclusao = FormatarData.formatarDataTrilha(dtoConvenio.getDtInclusao() + "-" + 
					dtoConvenio.getHrInclusao());

			usuarioInclusao = dtoConvenio.getCdUsuarioInclusao();

			tipoCanalInclusao = dtoConvenio.getCdTipoCanalInclusao().compareTo(0) == 0 ? "" : 
				dtoConvenio.getCdTipoCanalInclusao().toString() + "-" + (dtoConvenio.getDsTipoCanalInclusao());

			complementoInclusao = "0".equals(dtoConvenio.getCdOperacaoFluxoInclusao()) ? "" : 
				dtoConvenio.getCdOperacaoFluxoInclusao();

			// Dados Convenio
			setCodigoConvenio(dtoConvenio.getCdConveCtaSalarial() == 0 ? null : dtoConvenio.getCdConveCtaSalarial());
			setDecricaoConvenio(dtoConvenio.getDsConvCtaSalarial());
			setSituacaoConvenio(dtoConvenio.getCdSitConvCta() == 1 ? "ATIVO" : 
				dtoConvenio.getCdSitConvCta() == 2 ? "INATIVO" : "");
			if (dtoConvenio.getCdBcoEmprConvn() != 0 || dtoConvenio.getCdAgenciaEmprConvn() != 0 || 
					dtoConvenio.getCdCtaEmprConvn() != 0) {
				setBancoAgenciaContaConvenio(dtoConvenio.getCdBcoEmprConvn() + "/" + 
						dtoConvenio.getCdAgenciaEmprConvn() + "/" + dtoConvenio.getCdCtaEmprConvn() + "-" + 
						dtoConvenio.getCdDigitoEmprConvn());
			} else {
				setBancoAgenciaContaConvenio(null);
			}
			setDtHoraInclusao(dtoConvenio.getDtHoraInclusao());
			setCdUsuario(dtoConvenio.getCdUsuarioInclusao());

			if ("PGIT1092".equalsIgnoreCase(dtoConvenio.getCodMensagem())) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(dtoConvenio.getCodMensagem(), 8) + ") "
						+ dtoConvenio.getMensagem(), false);
			}
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + 
					e.getMessage(), false);
			limparDadosConvenio();
			setPaginaRetorno("");

		}
	}

	/**
	 * Limpar dados convenio.
	 */
	public void limparDadosConvenio() {
		dtoConvenio = new DetalharVincConvnCtaSalarioSaidaDTO();
		setDataHoraManutencao(null);
		setUsuarioManutencao(null);
		setTipoCanalManutencao(null);
		setComplementoManutencao(null);
		setDataHoraInclusao(null);
		setUsuarioInclusao(null);
		setTipoCanalInclusao(null);
		setComplementoInclusao(null);
		setCodigoConvenio(null);
		setDecricaoConvenio(null);
		setSituacaoConvenio(null);
		setBancoAgenciaContaConvenio(null);
		setDtHoraInclusao(null);
		setCdUsuario(null);
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt) {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
				identificacaoClienteContratoBean.getItemSelecionadoLista());

		try {
			ListarDadosCtaConvnEntradaDTO entrada = new ListarDadosCtaConvnEntradaDTO();
			listaSelConvenio = new ArrayList<ListarDadosCtaConvnListaOcorrenciasDTO>();
			listaControleSelConvenio = new ArrayList<SelectItem>();

			entrada.setCdClubPessoaRepresentante(saidaDTO.getCdClubRepresentante());
			entrada.setCdCpfCnpjRepresentante(saidaDTO.getCdCpfCnpjRepresentante());
			entrada.setCdControleCnpjRepresentante(saidaDTO.getCdControleCpfCnpjRepresentante());
			entrada.setCdFilialCnpjRepresentante(saidaDTO.getCdFilialCpfCnpjRepresentante());
			entrada.setCdPessoa(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
			entrada.setCdParmConvenio(1);

			saidaListarDadosCtaConvn = getManterVinculacaoConvenioContaSalarioService().listarDadosContaConvenio(entrada);
			setListaSelConvenio(saidaListarDadosCtaConvn.getOcorrencias());

			for (int i = 0; i < listaSelConvenio.size(); i++) {
				listaControleSelConvenio.add(new SelectItem(i, " "));
			}
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " +
					e.getMessage(), false);
		}
	}

	/**
	 * Is valida dados convenio.
	 *
	 * @return true, if is valida dados convenio
	 */
	public boolean isValidaDadosConvenio() {
		if (dtoConvenio.getCdBcoEmprConvn() != null && dtoConvenio.getCdBcoEmprConvn() != 0) {
			return false;
		}

		return true;
	}

	/**
	 * Get: manterVinculacaoConvenioContaSalarioService.
	 *
	 * @return manterVinculacaoConvenioContaSalarioService
	 */
	public IManterVinculacaoConvenioContaSalarioService getManterVinculacaoConvenioContaSalarioService() {
		return manterVinculacaoConvenioContaSalarioService;
	}

	/**
	 * Set: manterVinculacaoConvenioContaSalarioService.
	 *
	 * @param manterVinculacaoConvenioContaSalarioService the manter vinculacao convenio conta salario service
	 */
	public void setManterVinculacaoConvenioContaSalarioService(IManterVinculacaoConvenioContaSalarioService 
			manterVinculacaoConvenioContaSalarioService) {
		this.manterVinculacaoConvenioContaSalarioService = manterVinculacaoConvenioContaSalarioService;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(IdentificacaoClienteContratoBean 
			identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: identificacaoClienteBean.
	 *
	 * @return identificacaoClienteBean
	 */
	public IdentificacaoClienteBean getIdentificacaoClienteBean() {
		return identificacaoClienteBean;
	}

	/**
	 * Set: identificacaoClienteBean.
	 *
	 * @param identificacaoClienteBean the identificacao cliente bean
	 */
	public void setIdentificacaoClienteBean(IdentificacaoClienteBean identificacaoClienteBean) {
		this.identificacaoClienteBean = identificacaoClienteBean;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(FiltroAgendamentoEfetivacaoEstornoBean 
			filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: habilitaBtoCont.
	 *
	 * @return habilitaBtoCont
	 */
	public Boolean getHabilitaBtoCont() {
		return habilitaBtoCont;
	}

	/**
	 * Set: habilitaBtoCont.
	 *
	 * @param habilitaBtoCont the habilita bto cont
	 */
	public void setHabilitaBtoCont(Boolean habilitaBtoCont) {
		this.habilitaBtoCont = habilitaBtoCont;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: foco.
	 *
	 * @return foco
	 */
	public String getFoco() {
		return foco;
	}

	/**
	 * Set: foco.
	 *
	 * @param foco the foco
	 */
	public void setFoco(String foco) {
		this.foco = foco;
	}

	/**
	 * Get: codigoConvenio.
	 *
	 * @return codigoConvenio
	 */
	public Long getCodigoConvenio() {
		return codigoConvenio;
	}

	/**
	 * Set: codigoConvenio.
	 *
	 * @param codigoConvenio the codigo convenio
	 */
	public void setCodigoConvenio(Long codigoConvenio) {
		this.codigoConvenio = codigoConvenio;
	}

	/**
	 * Get: cpfCnpjRepresentante.
	 *
	 * @return cpfCnpjRepresentante
	 */
	public String getCpfCnpjRepresentante() {
		return cpfCnpjRepresentante;
	}

	/**
	 * Set: cpfCnpjRepresentante.
	 *
	 * @param cpfCnpjRepresentante the cpf cnpj representante
	 */
	public void setCpfCnpjRepresentante(String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	/**
	 * Get: nomeRazaoRepresentante.
	 *
	 * @return nomeRazaoRepresentante
	 */
	public String getNomeRazaoRepresentante() {
		return nomeRazaoRepresentante;
	}

	/**
	 * Set: nomeRazaoRepresentante.
	 *
	 * @param nomeRazaoRepresentante the nome razao representante
	 */
	public void setNomeRazaoRepresentante(String nomeRazaoRepresentante) {
		this.nomeRazaoRepresentante = nomeRazaoRepresentante;
	}

	/**
	 * Get: grupEconRepresentante.
	 *
	 * @return grupEconRepresentante
	 */
	public String getGrupEconRepresentante() {
		return grupEconRepresentante;
	}

	/**
	 * Set: grupEconRepresentante.
	 *
	 * @param grupEconRepresentante the grup econ representante
	 */
	public void setGrupEconRepresentante(String grupEconRepresentante) {
		this.grupEconRepresentante = grupEconRepresentante;
	}

	/**
	 * Get: ativEconRepresentante.
	 *
	 * @return ativEconRepresentante
	 */
	public String getAtivEconRepresentante() {
		return ativEconRepresentante;
	}

	/**
	 * Set: ativEconRepresentante.
	 *
	 * @param ativEconRepresentante the ativ econ representante
	 */
	public void setAtivEconRepresentante(String ativEconRepresentante) {
		this.ativEconRepresentante = ativEconRepresentante;
	}

	/**
	 * Get: segRepresentante.
	 *
	 * @return segRepresentante
	 */
	public String getSegRepresentante() {
		return segRepresentante;
	}

	/**
	 * Set: segRepresentante.
	 *
	 * @param segRepresentante the seg representante
	 */
	public void setSegRepresentante(String segRepresentante) {
		this.segRepresentante = segRepresentante;
	}

	/**
	 * Get: subSegRepresentante.
	 *
	 * @return subSegRepresentante
	 */
	public String getSubSegRepresentante() {
		return subSegRepresentante;
	}

	/**
	 * Set: subSegRepresentante.
	 *
	 * @param subSegRepresentante the sub seg representante
	 */
	public void setSubSegRepresentante(String subSegRepresentante) {
		this.subSegRepresentante = subSegRepresentante;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: gerenteResponsavelFormatado.
	 *
	 * @return gerenteResponsavelFormatado
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}

	/**
	 * Set: gerenteResponsavelFormatado.
	 *
	 * @param gerenteResponsavelFormatado the gerente responsavel formatado
	 */
	public void setGerenteResponsavelFormatado(String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: cpfCnpjParticipanteTemp.
	 *
	 * @return cpfCnpjParticipanteTemp
	 */
	public String getCpfCnpjParticipanteTemp() {
		return cpfCnpjParticipanteTemp;
	}

	/**
	 * Set: cpfCnpjParticipanteTemp.
	 *
	 * @param cpfCnpjParticipanteTemp the cpf cnpj participante temp
	 */
	public void setCpfCnpjParticipanteTemp(String cpfCnpjParticipanteTemp) {
		this.cpfCnpjParticipanteTemp = cpfCnpjParticipanteTemp;
	}

	/**
	 * Get: nomeRazaoParticipanteTemp.
	 *
	 * @return nomeRazaoParticipanteTemp
	 */
	public String getNomeRazaoParticipanteTemp() {
		return nomeRazaoParticipanteTemp;
	}

	/**
	 * Set: nomeRazaoParticipanteTemp.
	 *
	 * @param nomeRazaoParticipanteTemp the nome razao participante temp
	 */
	public void setNomeRazaoParticipanteTemp(String nomeRazaoParticipanteTemp) {
		this.nomeRazaoParticipanteTemp = nomeRazaoParticipanteTemp;
	}

	/**
	 * Get: cpfCnpjParticipanteDet.
	 *
	 * @return cpfCnpjParticipanteDet
	 */
	public String getCpfCnpjParticipanteDet() {
		return cpfCnpjParticipanteDet;
	}

	/**
	 * Set: cpfCnpjParticipanteDet.
	 *
	 * @param cpfCnpjParticipanteDet the cpf cnpj participante det
	 */
	public void setCpfCnpjParticipanteDet(String cpfCnpjParticipanteDet) {
		this.cpfCnpjParticipanteDet = cpfCnpjParticipanteDet;
	}

	/**
	 * Get: nomeRazaoParticipanteDet.
	 *
	 * @return nomeRazaoParticipanteDet
	 */
	public String getNomeRazaoParticipanteDet() {
		return nomeRazaoParticipanteDet;
	}

	/**
	 * Set: nomeRazaoParticipanteDet.
	 *
	 * @param nomeRazaoParticipanteDet the nome razao participante det
	 */
	public void setNomeRazaoParticipanteDet(String nomeRazaoParticipanteDet) {
		this.nomeRazaoParticipanteDet = nomeRazaoParticipanteDet;
	}

	/**
	 * Get: grupoEconParticipante.
	 *
	 * @return grupoEconParticipante
	 */
	public String getGrupoEconParticipante() {
		return grupoEconParticipante;
	}

	/**
	 * Set: grupoEconParticipante.
	 *
	 * @param grupoEconParticipante the grupo econ participante
	 */
	public void setGrupoEconParticipante(String grupoEconParticipante) {
		this.grupoEconParticipante = grupoEconParticipante;
	}

	/**
	 * Get: atividadeEconParticipante.
	 *
	 * @return atividadeEconParticipante
	 */
	public String getAtividadeEconParticipante() {
		return atividadeEconParticipante;
	}

	/**
	 * Set: atividadeEconParticipante.
	 *
	 * @param atividadeEconParticipante the atividade econ participante
	 */
	public void setAtividadeEconParticipante(String atividadeEconParticipante) {
		this.atividadeEconParticipante = atividadeEconParticipante;
	}

	/**
	 * Get: segmentoParticipante.
	 *
	 * @return segmentoParticipante
	 */
	public String getSegmentoParticipante() {
		return segmentoParticipante;
	}

	/**
	 * Set: segmentoParticipante.
	 *
	 * @param segmentoParticipante the segmento participante
	 */
	public void setSegmentoParticipante(String segmentoParticipante) {
		this.segmentoParticipante = segmentoParticipante;
	}

	/**
	 * Get: subSegmentoParticipante.
	 *
	 * @return subSegmentoParticipante
	 */
	public String getSubSegmentoParticipante() {
		return subSegmentoParticipante;
	}

	/**
	 * Set: subSegmentoParticipante.
	 *
	 * @param subSegmentoParticipante the sub segmento participante
	 */
	public void setSubSegmentoParticipante(String subSegmentoParticipante) {
		this.subSegmentoParticipante = subSegmentoParticipante;
	}

	/**
	 * Get: dsTipoParticipacao.
	 *
	 * @return dsTipoParticipacao
	 */
	public String getDsTipoParticipacao() {
		return dsTipoParticipacao;
	}

	/**
	 * Set: dsTipoParticipacao.
	 *
	 * @param dsTipoParticipacao the ds tipo participacao
	 */
	public void setDsTipoParticipacao(String dsTipoParticipacao) {
		this.dsTipoParticipacao = dsTipoParticipacao;
	}

	/**
	 * Get: situacaoParticipacao.
	 *
	 * @return situacaoParticipacao
	 */
	public String getSituacaoParticipacao() {
		return situacaoParticipacao;
	}

	/**
	 * Set: situacaoParticipacao.
	 *
	 * @param situacaoParticipacao the situacao participacao
	 */
	public void setSituacaoParticipacao(String situacaoParticipacao) {
		this.situacaoParticipacao = situacaoParticipacao;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: dtManutencaoInicio.
	 *
	 * @return dtManutencaoInicio
	 */
	public Date getDtManutencaoInicio() {
		return dtManutencaoInicio;
	}

	/**
	 * Set: dtManutencaoInicio.
	 *
	 * @param dtManutencaoInicio the dt manutencao inicio
	 */
	public void setDtManutencaoInicio(Date dtManutencaoInicio) {
		this.dtManutencaoInicio = dtManutencaoInicio;
	}

	/**
	 * Get: dtManutencaoFim.
	 *
	 * @return dtManutencaoFim
	 */
	public Date getDtManutencaoFim() {
		return dtManutencaoFim;
	}

	/**
	 * Set: dtManutencaoFim.
	 *
	 * @param dtManutencaoFim the dt manutencao fim
	 */
	public void setDtManutencaoFim(Date dtManutencaoFim) {
		this.dtManutencaoFim = dtManutencaoFim;
	}

	/**
	 * Get: decricaoConvenio.
	 *
	 * @return decricaoConvenio
	 */
	public String getDecricaoConvenio() {
		return decricaoConvenio;
	}

	/**
	 * Set: decricaoConvenio.
	 *
	 * @param decricaoConvenio the decricao convenio
	 */
	public void setDecricaoConvenio(String decricaoConvenio) {
		this.decricaoConvenio = decricaoConvenio;
	}

	/**
	 * Get: situacaoConvenio.
	 *
	 * @return situacaoConvenio
	 */
	public String getSituacaoConvenio() {
		return situacaoConvenio;
	}

	/**
	 * Set: situacaoConvenio.
	 *
	 * @param situacaoConvenio the situacao convenio
	 */
	public void setSituacaoConvenio(String situacaoConvenio) {
		this.situacaoConvenio = situacaoConvenio;
	}

	/**
	 * Get: bancoAgenciaContaConvenio.
	 *
	 * @return bancoAgenciaContaConvenio
	 */
	public String getBancoAgenciaContaConvenio() {
		return bancoAgenciaContaConvenio;
	}

	/**
	 * Set: bancoAgenciaContaConvenio.
	 *
	 * @param bancoAgenciaContaConvenio the banco agencia conta convenio
	 */
	public void setBancoAgenciaContaConvenio(String bancoAgenciaContaConvenio) {
		this.bancoAgenciaContaConvenio = bancoAgenciaContaConvenio;
	}

	/**
	 * Get: dtoConvenio.
	 *
	 * @return dtoConvenio
	 */
	public DetalharVincConvnCtaSalarioSaidaDTO getDtoConvenio() {
		return dtoConvenio;
	}

	/**
	 * Set: dtoConvenio.
	 *
	 * @param dtoConvenio the dto convenio
	 */
	public void setDtoConvenio(DetalharVincConvnCtaSalarioSaidaDTO dtoConvenio) {
		this.dtoConvenio = dtoConvenio;
	}

	/**
	 * Get: btoAcao.
	 *
	 * @return btoAcao
	 */
	public String getBtoAcao() {
		return btoAcao;
	}

	/**
	 * Set: btoAcao.
	 *
	 * @param btoAcao the bto acao
	 */
	public void setBtoAcao(String btoAcao) {
		this.btoAcao = btoAcao;
	}

	/**
	 * Get: listaSelConvenio.
	 *
	 * @return listaSelConvenio
	 */
	public List<ListarDadosCtaConvnListaOcorrenciasDTO> getListaSelConvenio() {
		return listaSelConvenio;
	}

	/**
	 * Set: listaSelConvenio.
	 *
	 * @param listaSelConvenio the lista sel convenio
	 */
	public void setListaSelConvenio(List<ListarDadosCtaConvnListaOcorrenciasDTO> listaSelConvenio) {
		this.listaSelConvenio = listaSelConvenio;
	}

	/**
	 * Get: listaControleSelConvenio.
	 *
	 * @return listaControleSelConvenio
	 */
	public List<SelectItem> getListaControleSelConvenio() {
		return listaControleSelConvenio;
	}

	/**
	 * Set: listaControleSelConvenio.
	 *
	 * @param listaControleSelConvenio the lista controle sel convenio
	 */
	public void setListaControleSelConvenio(List<SelectItem> listaControleSelConvenio) {
		this.listaControleSelConvenio = listaControleSelConvenio;
	}

	/**
	 * Get: itemSelecionadoListaConvenio.
	 *
	 * @return itemSelecionadoListaConvenio
	 */
	public Integer getItemSelecionadoListaConvenio() {
		return itemSelecionadoListaConvenio;
	}

	/**
	 * Set: itemSelecionadoListaConvenio.
	 *
	 * @param itemSelecionadoListaConvenio the item selecionado lista convenio
	 */
	public void setItemSelecionadoListaConvenio(Integer itemSelecionadoListaConvenio) {
		this.itemSelecionadoListaConvenio = itemSelecionadoListaConvenio;
	}

	/**
	 * Get: dtHoraInclusao.
	 *
	 * @return dtHoraInclusao
	 */
	public String getDtHoraInclusao() {
		return dtHoraInclusao;
	}

	/**
	 * Set: dtHoraInclusao.
	 *
	 * @param dtHoraInclusao the dt hora inclusao
	 */
	public void setDtHoraInclusao(String dtHoraInclusao) {
		this.dtHoraInclusao = dtHoraInclusao;
	}

	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}

	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: paginaRetorno.
	 *
	 * @return paginaRetorno
	 */
	public String getPaginaRetorno() {
		return paginaRetorno;
	}

	/**
	 * Set: paginaRetorno.
	 *
	 * @param paginaRetorno the pagina retorno
	 */
	public void setPaginaRetorno(String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: itemSelecListaGridPesquisaAltValTarifa.
	 *
	 * @param itemSelecListaGridPesquisaAltValTarifa the item selec lista grid pesquisa alt val tarifa
	 */
	public void setItemSelecListaGridPesquisaAltValTarifa(Integer itemSelecListaGridPesquisaAltValTarifa) {
		this.itemSelecListaGridPesquisaAltValTarifa = itemSelecListaGridPesquisaAltValTarifa;
	}

	/**
	 * Get: itemSelecListaGridPesquisaAltValTarifa.
	 *
	 * @return itemSelecListaGridPesquisaAltValTarifa
	 */
	public Integer getItemSelecListaGridPesquisaAltValTarifa() {
		return itemSelecListaGridPesquisaAltValTarifa;
	}

	/**
	 * Get: listaControleRadioGridPesquisaAltValTarifa.
	 *
	 * @return listaControleRadioGridPesquisaAltValTarifa
	 */
	public List<SelectItem> getListaControleRadioGridPesquisaAltValTarifa() {
		return listaControleRadioGridPesquisaAltValTarifa;
	}

	/**
	 * Set: listaControleRadioGridPesquisaAltValTarifa.
	 *
	 * @param listaControleRadioGridPesquisaAltValTarifa the lista controle radio grid pesquisa alt val tarifa
	 */
	public void setListaControleRadioGridPesquisaAltValTarifa(List<SelectItem> 
	listaControleRadioGridPesquisaAltValTarifa) {
		this.listaControleRadioGridPesquisaAltValTarifa = listaControleRadioGridPesquisaAltValTarifa;
	}

	/**
	 * Set: listaGridPesquisaAterarValorTarifa.
	 *
	 * @param listaGridPesquisaAterarValorTarifa the lista grid pesquisa aterar valor tarifa
	 */
	public void setListaGridPesquisaAterarValorTarifa(List<ConsultarTarifaContratoSaidaDTO> 
	listaGridPesquisaAterarValorTarifa) {
		this.listaGridPesquisaAterarValorTarifa = listaGridPesquisaAterarValorTarifa;
	}

	/**
	 * Get: listaGridPesquisaAterarValorTarifa.
	 *
	 * @return listaGridPesquisaAterarValorTarifa
	 */
	public List<ConsultarTarifaContratoSaidaDTO> getListaGridPesquisaAterarValorTarifa() {
		return listaGridPesquisaAterarValorTarifa;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public String getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Get: modalidade.
	 *
	 * @return modalidade
	 */
	public String getModalidade() {
		return modalidade;
	}

	/**
	 * Set: modalidade.
	 *
	 * @param modalidade the modalidade
	 */
	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}

	/**
	 * Get: operacao.
	 *
	 * @return operacao
	 */
	public String getOperacao() {
		return operacao;
	}

	/**
	 * Set: operacao.
	 *
	 * @param operacao the operacao
	 */
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	/**
	 * Set: tarifaContratada.
	 *
	 * @param tarifaContratada the tarifa contratada
	 */
	public void setTarifaContratada(String tarifaContratada) {
		this.tarifaContratada = tarifaContratada;
	}

	/**
	 * Get: tarifaContratada.
	 *
	 * @return tarifaContratada
	 */
	public String getTarifaContratada() {
		return tarifaContratada;
	}

	/**
	 * Set: tarifaMaximaPadrao.
	 *
	 * @param tarifaMaximaPadrao the tarifa maxima padrao
	 */
	public void setTarifaMaximaPadrao(String tarifaMaximaPadrao) {
		this.tarifaMaximaPadrao = tarifaMaximaPadrao;
	}

	/**
	 * Get: tarifaMaximaPadrao.
	 *
	 * @return tarifaMaximaPadrao
	 */
	public String getTarifaMaximaPadrao() {
		return tarifaMaximaPadrao;
	}

	/**
	 * Set: dtInicioVigencia.
	 *
	 * @param dtInicioVigencia the dt inicio vigencia
	 */
	public void setDtInicioVigencia(String dtInicioVigencia) {
		this.dtInicioVigencia = dtInicioVigencia;
	}

	/**
	 * Get: dtInicioVigencia.
	 *
	 * @return dtInicioVigencia
	 */
	public String getDtInicioVigencia() {
		return dtInicioVigencia;
	}

	/**
	 * Set: dtFimVigencia.
	 *
	 * @param dtFimVigencia the dt fim vigencia
	 */
	public void setDtFimVigencia(String dtFimVigencia) {
		this.dtFimVigencia = dtFimVigencia;
	}

	/**
	 * Get: dtFimVigencia.
	 *
	 * @return dtFimVigencia
	 */
	public String getDtFimVigencia() {
		return dtFimVigencia;
	}

	/**
	 * Set: valorTarifa.
	 *
	 * @param valorTarifa the valor tarifa
	 */
	public void setValorTarifa(BigDecimal valorTarifa) {
		this.valorTarifa = valorTarifa;
	}

	/**
	 * Get: valorTarifa.
	 *
	 * @return valorTarifa
	 */
	public BigDecimal getValorTarifa() {
		return valorTarifa;
	}

	/**
	 * Set: valorRefernciaMinima.
	 *
	 * @param valorRefernciaMinima the valor referncia minima
	 */
	public void setValorRefernciaMinima(String valorRefernciaMinima) {
		this.valorRefernciaMinima = valorRefernciaMinima;
	}

	/**
	 * Get: valorRefernciaMinima.
	 *
	 * @return valorRefernciaMinima
	 */
	public String getValorRefernciaMinima() {
		return valorRefernciaMinima;
	}

	/**
	 * Get: saidaAlterarTarifaAcimaPadrao.
	 *
	 * @return saidaAlterarTarifaAcimaPadrao
	 */
	public AlterarTarifaAcimaPadraoSaidaDTO getSaidaAlterarTarifaAcimaPadrao() {
		return saidaAlterarTarifaAcimaPadrao;
	}

	/**
	 * Set: saidaAlterarTarifaAcimaPadrao.
	 *
	 * @param saidaAlterarTarifaAcimaPadrao the saida alterar tarifa acima padrao
	 */
	public void setSaidaAlterarTarifaAcimaPadrao(AlterarTarifaAcimaPadraoSaidaDTO 
			saidaAlterarTarifaAcimaPadrao) {
		this.saidaAlterarTarifaAcimaPadrao = saidaAlterarTarifaAcimaPadrao;
	}

	/**
	 * @return the saidaListarDadosCtaConvn
	 */
	public ListarDadosCtaConvnSaidaDTO getSaidaListarDadosCtaConvn() {
		return saidaListarDadosCtaConvn;
	}

	/**
	 * @param saidaListarDadosCtaConvn the saidaListarDadosCtaConvn to set
	 */
	public void setSaidaListarDadosCtaConvn(
			ListarDadosCtaConvnSaidaDTO saidaListarDadosCtaConvn) {
		this.saidaListarDadosCtaConvn = saidaListarDadosCtaConvn;
	}
}