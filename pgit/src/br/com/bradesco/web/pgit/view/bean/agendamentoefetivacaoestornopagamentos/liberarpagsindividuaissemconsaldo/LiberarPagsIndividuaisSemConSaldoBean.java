/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.liberarpagsindividuaissemconsaldo
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.liberarpagsindividuaissemconsaldo;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.ILiberarPagtosIndSemConsultaSaldoService;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.ConsultarLibPagtosIndSemConsSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.ConsultarLibPagtosIndSemConsSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.LiberarPagtosIndSemConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindsemconsultasaldo.bean.LiberarPagtosIndSemConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;

/**
 * Nome: LiberarPagsIndividuaisSemConSaldoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class LiberarPagsIndividuaisSemConSaldoBean extends PagamentosBean {
	
	/** Atributo liberarPagtosIndSemConsultaSaldoServiceImpl. */
	private ILiberarPagtosIndSemConsultaSaldoService liberarPagtosIndSemConsultaSaldoServiceImpl;

	/** Atributo listaGridLiberarPagsInd. */
	private List<ConsultarLibPagtosIndSemConsSaldoSaidaDTO> listaGridLiberarPagsInd;

	/** Atributo listaGridLiberarPagsIndSelecionados. */
	private List<ConsultarLibPagtosIndSemConsSaldoSaidaDTO> listaGridLiberarPagsIndSelecionados;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio;

	/** Atributo listaControleRadioSelecionados. */
	private List<SelectItem> listaControleRadioSelecionados;

	/** Atributo panelBotoes. */
	private boolean panelBotoes;

	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos;

	/** Atributo tipoCanal. */
	private Integer tipoCanal;

	/** Atributo exibeBeneficiario. */
	private boolean exibeBeneficiario;

	// Variaveis Cliente
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;

	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;

	/** Atributo dsEmpresa. */
	private String dsEmpresa;

	/** Atributo nroContrato. */
	private String nroContrato;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	// Variaveis Cliente - Telas intermedi�rias
	/** Atributo nrCnpjCpfInterm. */
	private String nrCnpjCpfInterm;

	/** Atributo dsRazaoSocialInterm. */
	private String dsRazaoSocialInterm;

	/** Atributo dsEmpresaInterm. */
	private String dsEmpresaInterm;

	/** Atributo nroContratoInterm. */
	private String nroContratoInterm;

	/** Atributo dsContratoInterm. */
	private String dsContratoInterm;

	/** Atributo cdSituacaoContratoInterm. */
	private String cdSituacaoContratoInterm;

	/** Atributo dsTipoServico. */
	private String dsTipoServico;

	/** Atributo dsIndicadorModalidade. */
	private String dsIndicadorModalidade;

	/** Atributo dsQtdePagamentos. */
	private String dsQtdePagamentos;

	/** Atributo dsVlrTotalPagamentos. */
	private String dsVlrTotalPagamentos;

	// Conta de D�bito
	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;

	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;

	/** Atributo contaDebito. */
	private String contaDebito;

	/** Atributo tipoContaDebito. */
	private String tipoContaDebito;

	// Conta de D�bito - Telas intermedi�rias
	/** Atributo dsBancoDebitoInterm. */
	private String dsBancoDebitoInterm;

	/** Atributo dsAgenciaDebitoInterm. */
	private String dsAgenciaDebitoInterm;

	/** Atributo contaDebitoInterm. */
	private String contaDebitoInterm;

	/** Atributo tipoContaDebitoInterm. */
	private String tipoContaDebitoInterm;

	// Favorecido
	/** Atributo numeroInscricaoFavorecido. */
	private String numeroInscricaoFavorecido;

	/** Atributo tipoFavorecido. */
	private Integer tipoFavorecido;

	/** Atributo descTipoFavorecido. */
	private String descTipoFavorecido;

	/** Atributo dsFavorecido. */
	private String dsFavorecido;

	/** Atributo cdFavorecido. */
	private String cdFavorecido;

	/** Atributo dsBancoFavorecido. */
	private String dsBancoFavorecido;

	/** Atributo dsAgenciaFavorecido. */
	private String dsAgenciaFavorecido;

	/** Atributo dsContaFavorecido. */
	private String dsContaFavorecido;

	/** Atributo dsTipoContaFavorecido. */
	private String dsTipoContaFavorecido;

	// Benefici�rio
	/** Atributo numeroInscricaoBeneficiario. */
	private String numeroInscricaoBeneficiario;

	/** Atributo tipoBeneficiario. */
	private String tipoBeneficiario;

	/** Atributo cdTipoBeneficiario. */
	private Integer cdTipoBeneficiario;

	/** Atributo dsTipoBeneficiario. */
	private String dsTipoBeneficiario;

	/** Atributo nomeBeneficiario. */
	private String nomeBeneficiario;

	/** Atributo dtNascimentoBeneficiario. */
	private String dtNascimentoBeneficiario;

	// Pagamento
	/** Atributo numeroPagamento. */
	private String numeroPagamento;

	/** Atributo nrSequenciaArquivoRemessa. */
	private String nrSequenciaArquivoRemessa;

	/** Atributo nrLoteArquivoRemessa. */
	private String nrLoteArquivoRemessa;

	/** Atributo cdListaDebito. */
	private String cdListaDebito;

	/** Atributo dtAgendamento. */
	private String dtAgendamento;

	/** Atributo dtPagamento. */
	private String dtPagamento;
	
	/** Atributo dtDevolucaoEstorno. */
	private String dtDevolucaoEstorno;

	/** Atributo dtVencimento. */
	private String dtVencimento;
	
	/** Atributo cdIndicadorEconomicoMoeda. */
	private String cdIndicadorEconomicoMoeda;

	/** Atributo qtMoeda. */
	private BigDecimal qtMoeda;

	/** Atributo vlrAgendamento. */
	private BigDecimal vlrAgendamento;

	/** Atributo vlrEfetivacao. */
	private BigDecimal vlrEfetivacao;

	/** Atributo cdSituacaoOperacaoPagamento. */
	private String cdSituacaoOperacaoPagamento;

	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;

	/** Atributo dsPagamento. */
	private String dsPagamento;

	/** Atributo nrDocumento. */
	private String nrDocumento;

	/** Atributo tipoDocumento. */
	private String tipoDocumento;

	/** Atributo cdSerieDocumento. */
	private String cdSerieDocumento;

	/** Atributo vlDocumento. */
	private BigDecimal vlDocumento;

	/** Atributo dtEmissaoDocumento. */
	private String dtEmissaoDocumento;

	/** Atributo dsUsoEmpresa. */
	private String dsUsoEmpresa;

	/** Atributo dsMensagemPrimeiraLinha. */
	private String dsMensagemPrimeiraLinha;

	/** Atributo dsMensagemSegundaLinha. */
	private String dsMensagemSegundaLinha;

	/** Atributo dddContribuinte. */
	private String dddContribuinte;

	/** Atributo telefoneContribuinte. */
	private String telefoneContribuinte;

	/** Atributo inscricaoEstadualContribuinte. */
	private String inscricaoEstadualContribuinte;

	/** Atributo codigoBarras. */
	private String codigoBarras;

	/** Atributo codigoReceita. */
	private String codigoReceita;

	/** Atributo agenteArrecadador. */
	private String agenteArrecadador;

	/** Atributo codigoTributo. */
	private String codigoTributo;

	/** Atributo numeroReferencia. */
	private String numeroReferencia;

	/** Atributo numeroCotaParcela. */
	private String numeroCotaParcela;

	/** Atributo percentualReceita. */
	private BigDecimal percentualReceita;

	/** Atributo periodoApuracao. */
	private String periodoApuracao;

	/** Atributo anoExercicio. */
	private String anoExercicio;

	/** Atributo dataReferencia. */
	private String dataReferencia;

	/** Atributo dataCompetencia. */
	private String dataCompetencia;

	/** Atributo codigoCnae. */
	private String codigoCnae;

	/** Atributo placaVeiculo. */
	private String placaVeiculo;

	/** Atributo numeroParcelamento. */
	private String numeroParcelamento;

	/** Atributo codigoOrgaoFavorecido. */
	private String codigoOrgaoFavorecido;

	/** Atributo nomeOrgaoFavorecido. */
	private String nomeOrgaoFavorecido;

	/** Atributo codigoUfFavorecida. */
	private String codigoUfFavorecida;

	/** Atributo descricaoUfFavorecida. */
	private String descricaoUfFavorecida;

	/** Atributo vlReceita. */
	private BigDecimal vlReceita;

	/** Atributo vlPrincipal. */
	private BigDecimal vlPrincipal;

	/** Atributo vlAtualizacaoMonetaria. */
	private BigDecimal vlAtualizacaoMonetaria;

	/** Atributo vlAcrescimoFinanceiro. */
	private BigDecimal vlAcrescimoFinanceiro;

	/** Atributo vlHonorarioAdvogado. */
	private BigDecimal vlHonorarioAdvogado;

	/** Atributo vlDesconto. */
	private BigDecimal vlDesconto;

	/** Atributo vlAbatimento. */
	private BigDecimal vlAbatimento;

	/** Atributo vlMulta. */
	private BigDecimal vlMulta;

	/** Atributo vlMora. */
	private BigDecimal vlMora;

	/** Atributo vlTotal. */
	private BigDecimal vlTotal;

	/** Atributo observacaoTributo. */
	private String observacaoTributo;

	/** Atributo cdBancoCredito. */
	private String cdBancoCredito;

	/** Atributo agenciaDigitoCredito. */
	private String agenciaDigitoCredito;

	/** Atributo contaDigitoCredito. */
	private String contaDigitoCredito;

	/** Atributo tipoContaCredito. */
	private String tipoContaCredito;

	/** Atributo cdBancoDestino. */
	private String cdBancoDestino;

	/** Atributo agenciaDigitoDestino. */
	private String agenciaDigitoDestino;

	/** Atributo contaDigitoDestino. */
	private String contaDigitoDestino;

	/** Atributo tipoContaDestino. */
	private String tipoContaDestino;

	/** Atributo vlDeducao. */
	private BigDecimal vlDeducao;

	/** Atributo vlAcrescimo. */
	private BigDecimal vlAcrescimo;

	/** Atributo vlImpostoRenda. */
	private BigDecimal vlImpostoRenda;

	/** Atributo vlIss. */
	private BigDecimal vlIss;

	/** Atributo vlInss. */
	private BigDecimal vlInss;

	/** Atributo vlIof. */
	private BigDecimal vlIof;

	/** Atributo codigoRenavam. */
	private String codigoRenavam;

	/** Atributo municipioTributo. */
	private String municipioTributo;

	/** Atributo ufTributo. */
	private String ufTributo;

	/** Atributo numeroNsu. */
	private String numeroNsu;

	/** Atributo opcaoTributo. */
	private String opcaoTributo;

	/** Atributo opcaoRetiradaTributo. */
	private String opcaoRetiradaTributo;

	/** Atributo clienteDepositoIdentificado. */
	private String clienteDepositoIdentificado;

	/** Atributo tipoDespositoIdentificado. */
	private String tipoDespositoIdentificado;

	/** Atributo produtoDepositoIdentificado. */
	private String produtoDepositoIdentificado;

	/** Atributo sequenciaProdutoDepositoIdentificado. */
	private String sequenciaProdutoDepositoIdentificado;

	/** Atributo bancoCartaoDepositoIdentificado. */
	private String bancoCartaoDepositoIdentificado;

	/** Atributo cartaoDepositoIdentificado. */
	private String cartaoDepositoIdentificado;

	/** Atributo bimCartaoDepositoIdentificado. */
	private String bimCartaoDepositoIdentificado;

	/** Atributo viaCartaoDepositoIdentificado. */
	private String viaCartaoDepositoIdentificado;

	/** Atributo codigoDepositante. */
	private String codigoDepositante;

	/** Atributo nomeDepositante. */
	private String nomeDepositante;

	/** Atributo camaraCentralizadora. */
	private String camaraCentralizadora;

	/** Atributo finalidadeDoc. */
	private String finalidadeDoc;

	/** Atributo identificacaoTransferenciaDoc. */
	private String identificacaoTransferenciaDoc;

	/** Atributo identificacaoTitularidade. */
	private String identificacaoTitularidade;

	/** Atributo codigoInss. */
	private String codigoInss;

	/** Atributo vlOutrasEntidades. */
	private BigDecimal vlOutrasEntidades;

	/** Atributo codigoPagador. */
	private String codigoPagador;

	/** Atributo codigoOrdemCredito. */
	private String codigoOrdemCredito;

	/** Atributo numeroOp. */
	private String numeroOp;

	/** Atributo dataExpiracaoCredito. */
	private String dataExpiracaoCredito;

	/** Atributo instrucaoPagamento. */
	private String instrucaoPagamento;

	/** Atributo numeroCheque. */
	private String numeroCheque;

	/** Atributo serieCheque. */
	private String serieCheque;

	/** Atributo indicadorAssociadoUnicaAgencia. */
	private String indicadorAssociadoUnicaAgencia;

	/** Atributo identificadorTipoRetirada. */
	private String identificadorTipoRetirada;

	/** Atributo identificadorRetirada. */
	private String identificadorRetirada;

	/** Atributo dataRetirada. */
	private String dataRetirada;

	/** Atributo vlImpostoMovFinanceira. */
	private BigDecimal vlImpostoMovFinanceira;

	/** Atributo finalidadeTed. */
	private String finalidadeTed;

	/** Atributo identificacaoTransferenciaTed. */
	private String identificacaoTransferenciaTed;

	/** Atributo nossoNumero. */
	private String nossoNumero;

	/** Atributo dsOrigemPagamento. */
	private String dsOrigemPagamento;

	/** Atributo cdIndentificadorJudicial. */
	private String cdIndentificadorJudicial;

	/** Atributo dtLimiteDescontoPagamento. */
	private String dtLimiteDescontoPagamento;
	
	/** Atributo dtLimitePagamento. */
	private String dtLimitePagamento;
	
	/** Atributo dsRastreado. */
	private String dsRastreado;

	/** Atributo carteira. */
	private String carteira;

	/** Atributo cdSituacaoTranferenciaAutomatica. */
	private String cdSituacaoTranferenciaAutomatica;
	
	/** Atributo numControleInternoLote. */
	private Long numControleInternoLote;

	// Trilha
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	// Variaveis Inexistentes nos Argumentos de Pesquisa
	/** Atributo cdEmpresaContrato. */
	private Long cdEmpresaContrato;

	/** Atributo cdTipoConta. */
	private Integer cdTipoConta;

	/** Atributo nrContrato. */
	private Long nrContrato;

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;

	/** Atributo checkCont. */
	private Integer checkCont;

	/** Atributo cdBancoOriginal. */
	private Integer cdBancoOriginal;

	/** Atributo dsBancoOriginal. */
	private String dsBancoOriginal;

	/** Atributo cdAgenciaBancariaOriginal. */
	private Integer cdAgenciaBancariaOriginal;

	/** Atributo cdDigitoAgenciaOriginal. */
	private String cdDigitoAgenciaOriginal;

	/** Atributo dsAgenciaOriginal. */
	private String dsAgenciaOriginal;

	/** Atributo cdContaBancariaOriginal. */
	private Long cdContaBancariaOriginal;

	/** Atributo cdDigitoContaOriginal. */
	private String cdDigitoContaOriginal;

	/** Atributo dsTipoContaOriginal. */
	private String dsTipoContaOriginal;

	/** Atributo dtFloatingPagamento. */
	private String dtFloatingPagamento;
	
	/** Atributo dtEfetivFloatPgto. */
	private String dtEfetivFloatPgto;
	
	/** Atributo vlFloatingPagamento. */
	private BigDecimal vlFloatingPagamento;
	
	/** Atributo cdOperacaoDcom. */
	private Long cdOperacaoDcom;

	/** Atributo dsSituacaoDcom. */
	private String dsSituacaoDcom;
	
	// Flag
	/** Atributo exibeDcom. */
	private Boolean exibeDcom;

	/** * M�todos ** */

	public void checarTodos() {
		for (ConsultarLibPagtosIndSemConsSaldoSaidaDTO o : getListaGridLiberarPagsInd()) {
			o.setCheck(isOpcaoChecarTodos());
		}
		setPanelBotoes(isOpcaoChecarTodos());
	}

	/**
	 * Consultar lib pagtos ind sem cons saldo.
	 *
	 * @param evt the evt
	 */
	public void consultarLibPagtosIndSemConsSaldo(ActionEvent evt) {
		consultarLibPagtosIndSemConsSaldo();
	}

	/**
	 * Consultar lib pagtos ind sem cons saldo.
	 *
	 * @return the string
	 */
	public String consultarLibPagtosIndSemConsSaldo() {
		try {
			limparInformacaoConsulta();
			ConsultarLibPagtosIndSemConsSaldoEntradaDTO entrada = new ConsultarLibPagtosIndSemConsSaldoEntradaDTO();

			/*
			 * Se selecionado �Filtrar por Cliente� ou �Filtrar por Contrato� mover �1�. Se selecionado �Filtrar por
			 * Conta D�bito� mover �2�;
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")
							|| getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) {
				entrada.setCdTipoPesquisa(1);
			} else {
				entrada.setCdTipoPesquisa(2);
			}
			entrada.setCdAgendadosPagosNaoPagos(Integer.parseInt(getAgendadosPagosNaoPagos()));

			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")
							|| getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) {
				/*
				 * >> Se Selecionado Filtrar por Cliente Mover Valores vindos da lista >> Se Selecionado Filtrar por
				 * Contrato Passar valor retornado da lista(Verificado por email), na especifica��o definia passar os
				 * combo e inputtext, o que poderia gerar problemas caso o usu�rio altere os valores do combo ap�s ter
				 * feito a pesquisa
				 */
				entrada.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdPessoaJuridicaContrato() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdPessoaJuridicaContrato() : 0L);
				entrada
								.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
												.getCdTipoContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
												.getCdTipoContratoNegocio()
												: 0);
				entrada.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio() : 0L);
			}

			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
				/*
				 * Se selecionado �Filtrar por Conta D�bito� mover zeros para os cambos abaixo
				 */
				entrada.setCdPessoaJuridicaContrato(0L);
				entrada.setCdTipoContratoNegocio(0);
				entrada.setNrSequenciaContratoNegocio(0L);
			}

			/*
			 * Se estiver selecionado "Filtrar por Conta D�bito", passar os campos deste filtro Se estiver selecionado
			 * outro tipo de filtro, verificar se o "Conta D�bito" dos argumentos de pesquisa esta marcado Se estiver
			 * marcado, passar os valores correspondentes da tela,caso contr�rio passar zeros
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
				entrada.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean().getBancoContaDebitoFiltro());
				entrada.setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean().getAgenciaContaDebitoBancariaFiltro());
				entrada.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean().getContaBancariaContaDebitoFiltro());
				entrada.setCdDigitoConta(getFiltroAgendamentoEfetivacaoEstornoBean().getDigitoContaDebitoFiltro());
			} else {
				if (isChkContaDebito()) {
					entrada.setCdBanco(getCdBancoContaDebito());
					entrada.setCdAgencia(getCdAgenciaBancariaContaDebito());
					entrada.setCdConta(getCdContaBancariaContaDebito());
					entrada.setCdDigitoConta(getCdDigitoContaContaDebito() != null
									&& !getCdDigitoContaContaDebito().equals("") ? getCdDigitoContaContaDebito() : "0");
				} else {
					entrada.setCdBanco(0);
					entrada.setCdAgencia(0);
					entrada.setCdConta(0L);
					entrada.setCdDigitoConta("00");
				}
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			entrada.setDtCreditoPagamentoInicio((getDataInicialPagamentoFiltro() == null) ? "" : sdf
							.format(getDataInicialPagamentoFiltro()));
			entrada.setDtCreditoPagamentoFim((getDataFinalPagamentoFiltro() == null) ? "" : sdf
							.format(getDataFinalPagamentoFiltro()));

			entrada.setNrArquivoRemssaPagamento(getNumeroRemessaFiltro().equals("") ? 0L : Long
							.parseLong(getNumeroRemessaFiltro()));

			if (isChkParticipanteContrato()) {
				entrada.setCdParticipante(getFiltroAgendamentoEfetivacaoEstornoBean().getCodigoParticipanteFiltro());
			} else {
				entrada.setCdParticipante(0L);
			}

			entrada.setCdProdutoServicoOperacao(getTipoServicoFiltro() != null ? getTipoServicoFiltro() : 0);
			entrada.setCdProdutoServicoRelacionado(getModalidadeFiltro() != null ? getModalidadeFiltro() : 0);
			entrada.setCdControlePagamentoDe(getNumeroPagamentoDeFiltro() != null ? getNumeroPagamentoDeFiltro() : "");
			entrada.setCdControlePagamentoAte(getNumeroPagamentoDeFiltro() != null ? getNumeroPagamentoDeFiltro() : "");
			entrada.setVlPagamentoDe(getValorPagamentoDeFiltro() != null ? getValorPagamentoDeFiltro()
							: new BigDecimal("0.00"));
			entrada.setVlPagamentoAte(getValorPagamentoAteFiltro() != null ? getValorPagamentoAteFiltro()
							: new BigDecimal("0.00"));
			// Foi pedido para retirar o cambo de situa��o e motivo da tela -
			// email beatriz nogueira de santana dia
			// 29/03/2011 as 13:58
			entrada.setCdSituacaoOperacaoPagamento(0);
			entrada.setCdMotivoSituacaoPagamento(0);

			entrada.setCdBarraDocumento(PgitUtil.verificaStringNula(getLinhaDigitavel1())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel2())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel3())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel4())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel5())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel6())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel7())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel8()));
			entrada.setCdFavorecidoClientePagador(!getCodigoFavorecidoFiltro().equals("") ? Long
							.parseLong(getCodigoFavorecidoFiltro()) : 0L);
			entrada.setCdInscricaoFavorecido(!getInscricaoFiltro().equals("") ? Long.parseLong(getInscricaoFiltro())
							: 0L);
			entrada.setCdIdentificacaoInscricaoFavorecido(getTipoInscricaoFiltro() != null ? getTipoInscricaoFiltro()
							: 0);
			entrada.setCdBancoBeneficiario(getCdBancoContaCredito() != null ? getCdBancoContaCredito() : 0);
			entrada
							.setCdAgenciaBeneficiario(getCdAgenciaBancariaContaCredito() != null ? getCdAgenciaBancariaContaCredito()
											: 0);
			entrada.setCdContaBeneficiario(getCdContaBancariaContaCredito() != null ? getCdContaBancariaContaCredito()
							: 0L);
			entrada.setCdDigitoContaBeneficiario(getCdDigitoContaCredito() != null
							&& !getCdDigitoContaCredito().equals("") ? getCdDigitoContaCredito() : "00");
			entrada.setNrUnidadeOrganizacional(0);
			entrada.setCdBeneficio(0L);
			entrada.setCdEspecieBeneficioInss(0);
			entrada.setCdClassificacao(getClassificacaoFiltro() != null ? (getClassificacaoFiltro() == 0 ? 0
							: getClassificacaoFiltro()) : 0);
			// conforme solicitado via email por Kelli Cristina dia 12/04/2011,
			// retirar o combo Tipo Conta da tela e
			// passar os campos zerados para o pdc
			entrada.setCdEmpresaContrato(0L);
			entrada.setCdTipoConta(0);
			entrada.setNrContrato(0L);

			setListaGridLiberarPagsInd(getLiberarPagtosIndSemConsultaSaldoServiceImpl()
							.consultarLibPagtosIndSemConsSaldo(entrada));
			setListaControleRadio(new ArrayList<SelectItem>());

			for (int i = 0; i < getListaGridLiberarPagsInd().size(); i++) {
				getListaControleRadio().add(new SelectItem(i, " "));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}

		return "";
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#limparFavorecido()
	 */
	public void limparFavorecido() {
		setCodigoFavorecidoFiltro("");
		setInscricaoFiltro("");
		setTipoInscricaoFiltro(null);
		setCdBancoContaCredito(null);
		setCdAgenciaBancariaContaCredito(null);
		setCdContaBancariaContaCredito(null);
		setCdDigitoContaCredito("");
		limparTipoConta();
	}

	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes() {
		setPanelBotoes(false);
		setCheckCont(0);
		for (ConsultarLibPagtosIndSemConsSaldoSaidaDTO o : getListaGridLiberarPagsInd()) {
			if (o.isCheck()) {
				setPanelBotoes(true);
				this.checkCont++;
			}
		}
		setOpcaoChecarTodos(false);
	}

	/**
	 * Limpar selecionados.
	 */
	public void limparSelecionados() {
		for (ConsultarLibPagtosIndSemConsSaldoSaidaDTO o : getListaGridLiberarPagsInd()) {
			o.setCheck(false);
		}
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
	}

	/**
	 * Limpar.
	 */
	public void limpar() {
		limparFiltroPrincipal();
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(null);
		limparInformacaoConsulta();
	}

	/**
	 * Carrega selecionados.
	 */
	private void carregaSelecionados() {
		setListaGridLiberarPagsIndSelecionados(new ArrayList<ConsultarLibPagtosIndSemConsSaldoSaidaDTO>());
		setItemSelecionadoLista(null);
		setListaControleRadioSelecionados(new ArrayList<SelectItem>());

		for (ConsultarLibPagtosIndSemConsSaldoSaidaDTO o : getListaGridLiberarPagsInd()) {
			if (o.isCheck()) {
				getListaGridLiberarPagsIndSelecionados().add(o);
			}
		}

		for (int i = 0; i < getListaGridLiberarPagsIndSelecionados().size(); i++) {
			getListaControleRadioSelecionados().add(new SelectItem(i, ""));
		}
	}

	/**
	 * Autorizar selecionados.
	 *
	 * @return the string
	 */
	public String autorizarSelecionados() {
		carregaSelecionados();
		carregaCabecalho();

		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(new PDCDataBean());

		return "LIBERAR_SELECIONADOS";
	}

	/**
	 * Detalhar selecionados.
	 *
	 * @return the string
	 */
	public String detalharSelecionados() {
		carregaCabecalho();
		carregaSelecionados();

		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(new PDCDataBean());

		return "TELA_DETALHAR";
	}

	/**
	 * Carrega cabecalho.
	 */
	private void carregaCabecalho() {
		limparVariaveisInterm();
		if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")) { // Filtrar por
			// cliente
			setNrCnpjCpfInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getCpfCnpjFormatado());
			setDsRazaoSocialInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getNomeRazaoParticipante());
			setDsEmpresaInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescCliente());
			setNroContratoInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescCliente());
			setDsContratoInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescCliente());
			setCdSituacaoContratoInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescCliente());
		} else {
			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) { // Filtrar por
				// contrato
				setNrCnpjCpfInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getCpfCnpjFormatado());
				setDsRazaoSocialInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getNomeRazaoParticipante());
				setDsEmpresaInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescContrato());
				setNroContratoInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescContrato());
				setDsContratoInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescContrato());
				setCdSituacaoContratoInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescContrato());
			} else {
				if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) { // Filtrar
					// por
					// Conta D�bito
					getFiltroAgendamentoEfetivacaoEstornoBean().carregaDescricaoConta();
					setDsBancoDebitoInterm(PgitUtil.concatenarCampos(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getBancoContaDebitoFiltro(), getFiltroAgendamentoEfetivacaoEstornoBean()
									.getDescricaoBancoContaDebito()));

					setDsAgenciaDebitoInterm(PgitUtil.formatAgencia(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getAgenciaContaDebitoBancariaFiltro(), getListaGridLiberarPagsIndSelecionados()
									.get(0).getCdDigitoAgenciaDebito(), getFiltroAgendamentoEfetivacaoEstornoBean()
									.getDescricaoAgenciaContaDebitoBancaria(), false));
					setContaDebitoInterm(PgitUtil.formatConta(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getContaBancariaContaDebitoFiltro(), getListaGridLiberarPagsIndSelecionados().get(
									0).getCdDigitoContaDebito(), false));

					setTipoContaDebitoInterm(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoTipoContaDebito());
				}
			}
		}
	}

	/**
	 * Carrega cabecalho detalhe.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void carregaCabecalhoDetalhe(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {

		if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")) { // Filtrar por
			// cliente
			setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescCliente());
			setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescCliente());
			setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescCliente());
			setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescCliente());
		} else {
			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) { // Filtrar por
				// contrato
				setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescContrato());
				setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescContrato());
				setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescContrato());
				setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescContrato());
			} else {
				if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) { // Filtrar
					// contaDebito
					setDsEmpresa(registroSelecionado.getDsEmpresa());
					setNroContrato(String.valueOf(registroSelecionado.getNrContrato()));
				}
			}
		}

		setNrCnpjCpf(CpfCnpjUtils.formatCpfCnpjCompleto(registroSelecionado.getNrCnpjCpf(), registroSelecionado
						.getNrFilialCnpjCpf(), registroSelecionado.getNrDigitoCnpjCpf()));
		setDsRazaoSocial(registroSelecionado.getDsRazaoSocial());
	}

	/**
	 * Confirmar liberar selecionados.
	 */
	public void confirmarLiberarSelecionados() {
		try {
			List<LiberarPagtosIndSemConsultaSaldoEntradaDTO> listaOcorrenciasGridSelecionados = new ArrayList<LiberarPagtosIndSemConsultaSaldoEntradaDTO>();

			for (ConsultarLibPagtosIndSemConsSaldoSaidaDTO o : getListaGridLiberarPagsIndSelecionados()) {
				LiberarPagtosIndSemConsultaSaldoEntradaDTO ocorrencia = new LiberarPagtosIndSemConsultaSaldoEntradaDTO();

				ocorrencia.setCdPessoaJuridicaContrato(o.getCdEmpresa());
				ocorrencia.setCdTipoContratoNegocio(o.getCdTipoContratoNegocio());
				ocorrencia.setNrSequenciaContratoNegocio(o.getNrContrato());
				ocorrencia.setCdProdutoServicoOperacao(o.getCdProdutoServicoOperacao());
				ocorrencia.setCdTipoCanal(o.getCdTipoCanal());
				ocorrencia.setCdProdutoServicoOperacao(o.getCdProdutoServicoOperacao());
				ocorrencia.setCdOperacaoProdutoServico(o.getCdProdutoServicoRelacionado());
				ocorrencia.setCdControlePagamento(o.getCdControlePagamento());

				listaOcorrenciasGridSelecionados.add(ocorrencia);
			}

			LiberarPagtosIndSemConsultaSaldoSaidaDTO saida = getLiberarPagtosIndSemConsultaSaldoServiceImpl()
							.liberarPagtosIndSemConsultaSaldo(listaOcorrenciasGridSelecionados);
			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
							"liberarPagsIndividuaisSemConSaldo",
							"#{liberarPagsIndividuaisSemConSaldoBean.consultarLibPagtosIndSemConsSaldo}", false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
	}

	/**
	 * Pesquisar consulta.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarConsulta(ActionEvent evt) {
		consultarLibPagtosIndSemConsSaldo();
		
		return "";
	}

	/**
	 * Pesquisar selecionados.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarSelecionados(ActionEvent evt) {

		return "";
	}

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		limpar();

		// Tela de Filtro
		getFiltroAgendamentoEfetivacaoEstornoBean().setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno("liberarPagsIndividuaisSemConSaldo");

		limparCampos();

		// Carregamento dos combos
		listarTipoServicoPagto();
		listarInscricaoFavorecido();
		listarConsultarSituacaoPagamento();

		setHabilitaArgumentosPesquisa(false);
		getFiltroAgendamentoEfetivacaoEstornoBean().setClienteContratoSelecionado(false);
		getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);

		// Radio de Agendados � Pagos/N�o Pagos, ficara sempre protegido e com a
		// op��o �Agendados� selecionada;
		setAgendadosPagosNaoPagos("1");

		getFiltroAgendamentoEfetivacaoEstornoBean().setVoltarParticipante("liberarPagsIndividuaisSemConSaldo");
	}

	// M�todo sobreposto para n�o limpar os agendados - pagos/nao pagos
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#limparCampos()
	 */
	public void limparCampos() {
		getFiltroAgendamentoEfetivacaoEstornoBean().limparParticipanteContrato();
		setChkContaDebito(false);
		limparContaDebito();
		limparRemessaFavorecidoBeneficiarioLinha();
		limparSituacaoMotivo();
		limparTipoServicoModalidade();

		setDataInicialPagamentoFiltro(new Date());
		setDataFinalPagamentoFiltro(new Date());
		setChkParticipanteContrato(false);
		setChkRemessa(false);
		setChkServicoModalidade(false);
		setChkSituacaoMotivo(false);
		setRadioPesquisaFiltroPrincipal("");
		setRadioFavorecidoFiltro("");
		setRadioBeneficiarioFiltro("");

		setClassificacaoFiltro(null);
	}

	/**
	 * Limpar informacao consulta.
	 */
	public void limparInformacaoConsulta() {
		setListaGridLiberarPagsInd(null);
		setPanelBotoes(false);
		setOpcaoChecarTodos(false);
		setDisableArgumentosConsulta(false);
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(new PDCDataBean());
	}

	/**
	 * Limpar tela principal.
	 *
	 * @return the string
	 */
	public String limparTelaPrincipal() {
		// Limpar Argumentos de Pesquisa e Lista
		limparFiltroPrincipal();
		limparInformacaoConsulta();

		return "";
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
		limparInformacaoConsulta();

		limparCampos();

		return "";

	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		limparCampos();
		limparInformacaoConsulta();
		setOpcaoChecarTodos(false);
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoLista(null);
		limparSelecionados();
		return "VOLTAR";
	}

	/**
	 * Voltar consulta.
	 *
	 * @return the string
	 */
	public String voltarConsulta() {
		habilitarPanelBotoes();
		limparSelecionados();

		return "VOLTAR";
	}

	/** * Preenche Dados ** */
	public void limparVariaveisInterm() {
		setNrCnpjCpfInterm("");
		setDsRazaoSocialInterm("");
		setDsEmpresaInterm("");
		setNroContratoInterm("");
		setDsContratoInterm("");
		setCdSituacaoContratoInterm("");
		setDsBancoDebitoInterm("");
		setDsAgenciaDebitoInterm("");
		setContaDebitoInterm("");
		setTipoContaDebitoInterm("");
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {

		ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado = new ConsultarLibPagtosIndSemConsSaldoSaidaDTO();
		for (int i = 0; i < getListaGridLiberarPagsInd().size(); i++) {
			if (getListaGridLiberarPagsInd().get(i).isCheck()) {
				registroSelecionado = getListaGridLiberarPagsInd().get(i);
			}
		}

		carregaCabecalhoDetalhe(registroSelecionado);

		try {
			if (registroSelecionado.getCdTipoTela() != null) {
				if (registroSelecionado.getCdTipoTela().intValue() == 1) {
					preencheDadosPagtoCreditoConta(registroSelecionado);
					return "DETALHE_CREDITO_CONTA";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 2) {
					preencheDadosPagtoDebitoConta(registroSelecionado);
					return "DETALHE_DEBITO_CONTA";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 3) {
					preencheDadosPagtoDOC(registroSelecionado);
					return "DETALHE_DOC";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 4) {
					preencheDadosPagtoTED(registroSelecionado);
					return "DETALHE_TED";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 5) {
					preencheDadosPagtoOrdemPagto(registroSelecionado);
					return "DETALHE_ORDEM_PAGTO";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 6) {
					preencheDadosPagtoTituloBradesco(registroSelecionado);
					return "DETALHE_TITULOS_BRADESCO";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 7) {
					preencheDadosPagtoTituloOutrosBancos(registroSelecionado);
					return "DETALHE_TITULOS_OUTROS_BANCOS";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 8) {
					preencheDadosPagtoDARF(registroSelecionado);
					return "DETALHE_DARF";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 9) {
					preencheDadosPagtoGARE(registroSelecionado);
					return "DETALHE_GARE";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 10) {
					preencheDadosPagtoGPS(registroSelecionado);
					return "DETALHE_GPS";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 11) {
					preencheDadosPagtoCodigoBarras(registroSelecionado);
					return "DETALHE_CODIGO_BARRAS";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 12) {
					preencheDadosPagtoDebitoVeiculos(registroSelecionado);
					return "DETALHE_DEBITO_VEICULOS";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 13) {
					preencheDadosPagtoDepIdentificado(registroSelecionado);
					return "DETALHE_DEPOSITO_IDENTIFICADO";
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 14) {
					preencheDadosPagtoOrdemCredito(registroSelecionado);
					return "DETALHE_ORDEM_CREDITO";
				}
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}

		return "";
	}

	/**
	 * Limpar campos favorecido beneficiario.
	 */
	public void limparCamposFavorecidoBeneficiario() {
		setTipoBeneficiario("");
		setNumeroInscricaoBeneficiario("");
		setCdTipoBeneficiario(0);
		setNomeBeneficiario("");
		setDtNascimentoBeneficiario("");
		setNumeroInscricaoFavorecido("");
		setTipoFavorecido(0);
		setDsFavorecido("");
		setCdFavorecido("");
		setDsBancoFavorecido("");
		setDsAgenciaFavorecido("");
		setDsContaFavorecido("");
		setDsTipoContaFavorecido("");
		setDsTipoBeneficiario("");
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - Cr�dito
	// em Conta
	/**
	 * Preenche dados pagto credito conta.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoCreditoConta(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoCreditoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoCreditoConta(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
						.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(), saidaDTO.getDsBancoDebito(), false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
						saidaDTO.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(), saidaDTO.getDsDigAgenciaDebito(), false));
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();

		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidade() == 1 || saidaDTO.getCdIndicadorModalidade() == 3) {
			setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
							.getNmInscriFavorecido()));
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(),
							false));
			setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
							.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
			setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
							false));
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidade() == 1 ? true : false);
		} else {
			setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| !PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals("")
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlrAgendamento());
		setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
						.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil
						.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setCdSituacaoTranferenciaAutomatica(saidaDTO.getCdSituacaoTranferenciaAutomatica());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
		setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

		// trilhaAuditoria
		setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
		setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
		setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

		// Favorecidos
		setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
		setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - " + saidaDTO.getDsBancoOriginal());
		setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
		setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
		setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-" + saidaDTO.getCdDigitoAgenciaOriginal()
						+ " - " + saidaDTO.getDsAgenciaOriginal());
		setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
		setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-" + saidaDTO.getCdDigitoContaOriginal());
		setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - D�bito
	// em conta
	/**
	 * Preenche dados pagto debito conta.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoDebitoConta(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoDebitoContaEntradaDTO entradaDTO = new DetalharPagtoDebitoContaEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDebitoConta(entradaDTO);

		// Cabe�alho - Cliente
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
						.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());

		// Sempre Carregar Conta de D�bito
		setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(), saidaDTO.getDsBancoDebito(), false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
						saidaDTO.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(), saidaDTO.getDsDigAgenciaDebito(), false));
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// Favorecido
		setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
						.getNmInscriFavorecido()));
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(),
						saidaDTO.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlrAgendamento());
		setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
						.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil
						.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
		setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
		setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - DOC
	/**
	 * Preenche dados pagto doc.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoDOC(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoDOCEntradaDTO entradaDTO = new DetalharPagtoDOCEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoDOC(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
						.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();

		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
							.getNmInscriFavorecido()));
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			setNumeroInscricaoBeneficiario(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
							.getNmInscriBeneficio()));
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| !PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals("")
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
		setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
		setIdentificacaoTransferenciaDoc(saidaDTO.getDsIdentificacaoTransferenciaPagto());
		setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - TED
	/**
	 * Preenche dados pagto ted.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoTED(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoTEDEntradaDTO entradaDTO = new DetalharPagtoTEDEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTEDSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoTED(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();

		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());

		setFinalidadeTed(PgitUtil.concatenarCampos(saidaDTO.getCdFinalidadeTed(), saidaDTO.getDsFinalidadeDocTed(), " - "));
		setIdentificacaoTransferenciaTed(saidaDTO.getDsIdentificacaoTransferenciaPagto());
		setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());

		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatidoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlIrCredito());
		setVlIss(saidaDTO.getVlIssCredito());
		setVlIof(saidaDTO.getVlIofCredito());
		setVlInss(saidaDTO.getVlInssCredito());
		setCdIndentificadorJudicial(saidaDTO.getCdIndentificadorJudicial());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - Ordem
	// de Pagamento
	/**
	 * Preenche dados pagto ordem pagto.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoOrdemPagto(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharPagtoOrdemPagtoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoOrdemPagtoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoOrdemPagto(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null
							: (saidaDTO.getCdFavorecido()));
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setNumeroOp(saidaDTO.getNrOperacao());
		setDataExpiracaoCredito(saidaDTO.getDtExpedicaoCredito());
		setInstrucaoPagamento(saidaDTO.getCdInsPagamento());
		setNumeroCheque(saidaDTO.getNrCheque());
		setSerieCheque(saidaDTO.getNrSerieCheque());
		setIndicadorAssociadoUnicaAgencia(saidaDTO.getDsUnidadeAgencia());

		setIdentificadorTipoRetirada(saidaDTO.getDsIdentificacaoTipoRetirada());
		setIdentificadorRetirada(saidaDTO.getDsIdentificacaoRetirada());

		setDataRetirada(saidaDTO.getDtRetirada());
		setVlImpostoMovFinanceira(saidaDTO.getVlImpostoMovimentacaoFinanceira());
		setVlDesconto(saidaDTO.getVlDescontoCredito());
		setVlAbatimento(saidaDTO.getVlAbatidoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlIrCredito());
		setVlIss(saidaDTO.getVlIssCredito());
		setVlIof(saidaDTO.getVlIofCredito());
		setVlInss(saidaDTO.getVlInssCredito());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - Titulos
	// Bradesco
	/**
	 * Preenche dados pagto titulo bradesco.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoTituloBradesco(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharPagtoTituloBradescoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoTituloBradesco(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setCarteira(saidaDTO.getCarteira());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCodigoBarras(saidaDTO.getCdBarras());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
		setNossoNumero(saidaDTO.getNossoNumero());
		setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
		setDsRastreado(saidaDTO.getCdRastreado() == 1 ? "Sim":"N�o");
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - Titulos
	// Outros Bancos
	/**
	 * Preenche dados pagto titulo outros bancos.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoTituloOutrosBancos(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharPagtoTituloOutrosBancosEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoTituloOutrosBancos(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setCarteira(saidaDTO.getCarteira());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCodigoBarras(saidaDTO.getCdBarras());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
		setNossoNumero(saidaDTO.getNossoNumero());
		setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
		setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
		setDsRastreado(saidaDTO.getCdRastreado() == 1 ? "Sim":"N�o");
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - DARF
	/**
	 * Preenche dados pagto darf.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoDARF(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoDARFEntradaDTO entradaDTO = new DetalharPagtoDARFEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDARF(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddTelefone());
		setTelefoneContribuinte(saidaDTO.getCdTelefone());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
		setAgenteArrecadador(saidaDTO.getCdAgenteArrecad());
		setCodigoReceita(saidaDTO.getCdReceitaTributo());
		setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcela());
		setPercentualReceita(saidaDTO.getCdPercentualReceita());
		setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
		setAnoExercicio(saidaDTO.getCdDanoExercicio());
		setDataReferencia(saidaDTO.getDtReferenciaTributo());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
		setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlTotal(saidaDTO.getVlTotalTributo());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - GARE
	/**
	 * Preenche dados pagto gare.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoGARE(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoGAREEntradaDTO entradaDTO = new DetalharPagtoGAREEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoGARE(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddContribuinte());
		setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadualContribuinte());
		setCodigoReceita(saidaDTO.getCdReceita());
		setNumeroReferencia(saidaDTO.getNrReferencia());
		setNumeroCotaParcela(saidaDTO.getNrCota());
		setPercentualReceita(saidaDTO.getCdPercentualReceita());
		setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
		setDataReferencia(saidaDTO.getDtReferencia());
		setCodigoCnae(saidaDTO.getCdCnae());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setNumeroParcelamento(saidaDTO.getNrParcela());
		setCodigoOrgaoFavorecido(saidaDTO.getCdOrFavorecido());
		setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
		setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
		setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
		setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setObservacaoTributo(saidaDTO.getDsTributo());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - GPS
	/**
	 * Preenche dados pagto gps.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoGPS(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoGPSEntradaDTO entradaDTO = new DetalharPagtoGPSEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoGPS(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddContribuinte());
		setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
		setCodigoInss(saidaDTO.getCdInss());
		setDataCompetencia(saidaDTO.getDsMesAnoCompt());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - C�digo
	// de Barras
	/**
	 * Preenche dados pagto codigo barras.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoCodigoBarras(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharPagtoCodigoBarrasEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoCodigoBarras(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddTelefone());
		setTelefoneContribuinte(saidaDTO.getNrTelefone());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
		setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
		setCodigoReceita(saidaDTO.getCdReceitaTributo());
		setAgenteArrecadador(saidaDTO.getCdAgenteArrecadador());
		setCodigoTributo(saidaDTO.getCdTributoArrecadado());
		setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTributo());
		setPercentualReceita(saidaDTO.getCdPercentualTributo());
		setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
		setAnoExercicio(saidaDTO.getCdAnoExercicioTributo());
		setDataReferencia(saidaDTO.getDtReferenciaTributo());
		setDataCompetencia(saidaDTO.getDtCompensacao());
		setCodigoCnae(saidaDTO.getCdCnae());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setNumeroParcelamento(saidaDTO.getNrParcelamentoTributo());
		setCodigoOrgaoFavorecido(saidaDTO.getCdOrgaoFavorecido());
		setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
		setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
		setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
		setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setObservacaoTributo(saidaDTO.getDsObservacao());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - D�bito
	// de Veiculos
	/**
	 * Preenche dados pagto debito veiculos.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoDebitoVeiculos(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharPagtoDebitoVeiculosEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDebitoVeiculos(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTrib());
		setAnoExercicio(saidaDTO.getDtAnoExercTributo());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
		setMunicipioTributo(saidaDTO.getCdMunicArrecadTrib());
		setUfTributo(saidaDTO.getCdUfTributo());
		setNumeroNsu(saidaDTO.getNrNsuTributo());
		setOpcaoTributo(saidaDTO.getCdOpcaoTributo());
		setOpcaoRetiradaTributo(saidaDTO.getCdOpcaoRetirada());
		setVlPrincipal(saidaDTO.getVlPrincipalTributo());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlTotal(saidaDTO.getVlTotalTributo());
		setObservacaoTributo(saidaDTO.getDsObservacaoTributo());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo -
	// Deposito Identificado
	/**
	 * Preenche dados pagto dep identificado.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoDepIdentificado(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharPagtoDepIdentificadoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDepIdentificado(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Beneficiario
		setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
						.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil
						.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setClienteDepositoIdentificado(saidaDTO.getCdClienteDepositoId());
		setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
		setProdutoDepositoIdentificado(saidaDTO.getCdProdutoDepositoId());
		setSequenciaProdutoDepositoIdentificado(saidaDTO.getCdSequenciaProdutoDepositoId());
		setBancoCartaoDepositoIdentificado(saidaDTO.getCdBancoCartaoDepositanteId());
		setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
		setBimCartaoDepositoIdentificado(saidaDTO.getCdBinCartaoDepositanteId());
		setViaCartaoDepositoIdentificado(saidaDTO.getCdViaCartaoDepositanteId());
		setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
		setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
		setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
		setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
		setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
		setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
		setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
		setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
		setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
		setVlIss(saidaDTO.getVlIssCreditoPagamento());
		setVlIof(saidaDTO.getVlIofCreditoPagamento());
		setVlInss(saidaDTO.getVlInssCreditoPagamento());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Libera��o Pagamentos Individuais Sem Consulta de Saldo - Ordem
	// de Cr�dito
	/**
	 * Preenche dados pagto ordem credito.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoOrdemCredito(ConsultarLibPagtosIndSemConsSaldoSaidaDTO registroSelecionado) {
		DetalharPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharPagtoOrdemCreditoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoOrdemCredito(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Beneficiario
		setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
						.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil
						.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCodigoPagador(saidaDTO.getCdPagador());
		setCodigoOrdemCredito(saidaDTO.getCdOrdemCreditoPagamento());
		setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
		setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
		setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
		setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
		setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
		setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
		setVlIss(saidaDTO.getVlIssCreditoPagamento());
		setVlIof(saidaDTO.getVlIofCreditoPagamento());
		setVlInss(saidaDTO.getVlInssCreditoPagamento());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// In�cio Relat�rio
	/** Atributo logoPath. */
	private String logoPath;

	/**
	 * Imprimir relatorio.
	 */
	public void imprimirRelatorio() {

		try {

			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			carregaCabecalho();

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par.put("titulo", "Liberar Pagamentos Individuais Sem Consulta de Saldo");

			par.put("cpfCnpj", getFiltroAgendamentoEfetivacaoEstornoBean().getCpfCnpjFormatado());
			par.put("nomeRazaoSocial", getFiltroAgendamentoEfetivacaoEstornoBean().getNomeRazaoParticipante());
			par.put("empresa", getDsEmpresaInterm());
			par.put("numeroPagamento", getNroContratoInterm());
			par.put("descContrato", getDsContratoInterm());
			par.put("situacao", getCdSituacaoContratoInterm());
			par.put("bancoDebito", getDsBancoDebitoInterm());
			par.put("agenciaDebito", getDsAgenciaDebitoInterm());
			par.put("contaDebito", getContaDebitoInterm());
			par.put("tipoContaDebito", getTipoContaDebitoInterm());
			par.put("logoBradesco", getLogoPath());
			par.put("flagBanda", Integer.parseInt(getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado()));
			try {

				// M�todo que gera o relat�rio PDF.
				PgitUtil
								.geraRelatorioPdf(
												"/relatorios/agendamentoEfetivacaoEstorno/liberarPagtoSemConSaldos/liberarPagtoIndividuaisSemConSaldos",
												"imprimirLiberarPagtosSemConSaldoSel",
												getListaGridLiberarPagsIndSelecionados(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
	}

	/** * Getters and Setters ** */

	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	/**
	 * Set: panelBotoes.
	 *
	 * @param panelBotoes the panel botoes
	 */
	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaGridLiberarPagsInd.
	 *
	 * @return listaGridLiberarPagsInd
	 */
	public List<ConsultarLibPagtosIndSemConsSaldoSaidaDTO> getListaGridLiberarPagsInd() {
		return listaGridLiberarPagsInd;
	}

	/**
	 * Set: listaGridLiberarPagsInd.
	 *
	 * @param listaGridLiberarPagsInd the lista grid liberar pags ind
	 */
	public void setListaGridLiberarPagsInd(List<ConsultarLibPagtosIndSemConsSaldoSaidaDTO> listaGridLiberarPagsInd) {
		this.listaGridLiberarPagsInd = listaGridLiberarPagsInd;
	}

	/**
	 * Get: liberarPagtosIndSemConsultaSaldoServiceImpl.
	 *
	 * @return liberarPagtosIndSemConsultaSaldoServiceImpl
	 */
	public ILiberarPagtosIndSemConsultaSaldoService getLiberarPagtosIndSemConsultaSaldoServiceImpl() {
		return liberarPagtosIndSemConsultaSaldoServiceImpl;
	}

	/**
	 * Set: liberarPagtosIndSemConsultaSaldoServiceImpl.
	 *
	 * @param liberarPagtosIndSemConsultaSaldoServiceImpl the liberar pagtos ind sem consulta saldo service impl
	 */
	public void setLiberarPagtosIndSemConsultaSaldoServiceImpl(
					ILiberarPagtosIndSemConsultaSaldoService liberarPagtosIndSemConsultaSaldoServiceImpl) {
		this.liberarPagtosIndSemConsultaSaldoServiceImpl = liberarPagtosIndSemConsultaSaldoServiceImpl;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Get: listaGridLiberarPagsIndSelecionados.
	 *
	 * @return listaGridLiberarPagsIndSelecionados
	 */
	public List<ConsultarLibPagtosIndSemConsSaldoSaidaDTO> getListaGridLiberarPagsIndSelecionados() {
		return listaGridLiberarPagsIndSelecionados;
	}

	/**
	 * Set: listaGridLiberarPagsIndSelecionados.
	 *
	 * @param listaGridLiberarPagsIndSelecionados the lista grid liberar pags ind selecionados
	 */
	public void setListaGridLiberarPagsIndSelecionados(
					List<ConsultarLibPagtosIndSemConsSaldoSaidaDTO> listaGridLiberarPagsIndSelecionados) {
		this.listaGridLiberarPagsIndSelecionados = listaGridLiberarPagsIndSelecionados;
	}

	/**
	 * Get: dsQtdePagamentos.
	 *
	 * @return dsQtdePagamentos
	 */
	public String getDsQtdePagamentos() {
		return dsQtdePagamentos;
	}

	/**
	 * Set: dsQtdePagamentos.
	 *
	 * @param dsQtdePagamentos the ds qtde pagamentos
	 */
	public void setDsQtdePagamentos(String dsQtdePagamentos) {
		this.dsQtdePagamentos = dsQtdePagamentos;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: dsVlrTotalPagamentos.
	 *
	 * @return dsVlrTotalPagamentos
	 */
	public String getDsVlrTotalPagamentos() {
		return dsVlrTotalPagamentos;
	}

	/**
	 * Set: dsVlrTotalPagamentos.
	 *
	 * @param dsVlrTotalPagamentos the ds vlr total pagamentos
	 */
	public void setDsVlrTotalPagamentos(String dsVlrTotalPagamentos) {
		this.dsVlrTotalPagamentos = dsVlrTotalPagamentos;
	}

	/**
	 * Get: agenteArrecadador.
	 *
	 * @return agenteArrecadador
	 */
	public String getAgenteArrecadador() {
		return agenteArrecadador;
	}

	/**
	 * Set: agenteArrecadador.
	 *
	 * @param agenteArrecadador the agente arrecadador
	 */
	public void setAgenteArrecadador(String agenteArrecadador) {
		this.agenteArrecadador = agenteArrecadador;
	}

	/**
	 * Get: anoExercicio.
	 *
	 * @return anoExercicio
	 */
	public String getAnoExercicio() {
		return anoExercicio;
	}

	/**
	 * Set: anoExercicio.
	 *
	 * @param anoExercicio the ano exercicio
	 */
	public void setAnoExercicio(String anoExercicio) {
		this.anoExercicio = anoExercicio;
	}

	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public String getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}

	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(String cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}

	/**
	 * Get: cdListaDebito.
	 *
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}

	/**
	 * Set: cdListaDebito.
	 *
	 * @param cdListaDebito the cd lista debito
	 */
	public void setCdListaDebito(String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}

	/**
	 * Get: cdSerieDocumento.
	 *
	 * @return cdSerieDocumento
	 */
	public String getCdSerieDocumento() {
		return cdSerieDocumento;
	}

	/**
	 * Set: cdSerieDocumento.
	 *
	 * @param cdSerieDocumento the cd serie documento
	 */
	public void setCdSerieDocumento(String cdSerieDocumento) {
		this.cdSerieDocumento = cdSerieDocumento;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public String getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(String cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: codigoBarras.
	 *
	 * @return codigoBarras
	 */
	public String getCodigoBarras() {
		return codigoBarras;
	}

	/**
	 * Set: codigoBarras.
	 *
	 * @param codigoBarras the codigo barras
	 */
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	/**
	 * Get: codigoCnae.
	 *
	 * @return codigoCnae
	 */
	public String getCodigoCnae() {
		return codigoCnae;
	}

	/**
	 * Set: codigoCnae.
	 *
	 * @param codigoCnae the codigo cnae
	 */
	public void setCodigoCnae(String codigoCnae) {
		this.codigoCnae = codigoCnae;
	}

	/**
	 * Get: codigoOrgaoFavorecido.
	 *
	 * @return codigoOrgaoFavorecido
	 */
	public String getCodigoOrgaoFavorecido() {
		return codigoOrgaoFavorecido;
	}

	/**
	 * Set: codigoOrgaoFavorecido.
	 *
	 * @param codigoOrgaoFavorecido the codigo orgao favorecido
	 */
	public void setCodigoOrgaoFavorecido(String codigoOrgaoFavorecido) {
		this.codigoOrgaoFavorecido = codigoOrgaoFavorecido;
	}

	/**
	 * Get: codigoReceita.
	 *
	 * @return codigoReceita
	 */
	public String getCodigoReceita() {
		return codigoReceita;
	}

	/**
	 * Set: codigoReceita.
	 *
	 * @param codigoReceita the codigo receita
	 */
	public void setCodigoReceita(String codigoReceita) {
		this.codigoReceita = codigoReceita;
	}

	/**
	 * Get: codigoTributo.
	 *
	 * @return codigoTributo
	 */
	public String getCodigoTributo() {
		return codigoTributo;
	}

	/**
	 * Set: codigoTributo.
	 *
	 * @param codigoTributo the codigo tributo
	 */
	public void setCodigoTributo(String codigoTributo) {
		this.codigoTributo = codigoTributo;
	}

	/**
	 * Get: codigoUfFavorecida.
	 *
	 * @return codigoUfFavorecida
	 */
	public String getCodigoUfFavorecida() {
		return codigoUfFavorecida;
	}

	/**
	 * Set: codigoUfFavorecida.
	 *
	 * @param codigoUfFavorecida the codigo uf favorecida
	 */
	public void setCodigoUfFavorecida(String codigoUfFavorecida) {
		this.codigoUfFavorecida = codigoUfFavorecida;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataCompetencia.
	 *
	 * @return dataCompetencia
	 */
	public String getDataCompetencia() {
		return dataCompetencia;
	}

	/**
	 * Set: dataCompetencia.
	 *
	 * @param dataCompetencia the data competencia
	 */
	public void setDataCompetencia(String dataCompetencia) {
		this.dataCompetencia = dataCompetencia;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: dataReferencia.
	 *
	 * @return dataReferencia
	 */
	public String getDataReferencia() {
		return dataReferencia;
	}

	/**
	 * Set: dataReferencia.
	 *
	 * @param dataReferencia the data referencia
	 */
	public void setDataReferencia(String dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	/**
	 * Get: dddContribuinte.
	 *
	 * @return dddContribuinte
	 */
	public String getDddContribuinte() {
		return dddContribuinte;
	}

	/**
	 * Set: dddContribuinte.
	 *
	 * @param dddContribuinte the ddd contribuinte
	 */
	public void setDddContribuinte(String dddContribuinte) {
		this.dddContribuinte = dddContribuinte;
	}

	/**
	 * Get: descricaoUfFavorecida.
	 *
	 * @return descricaoUfFavorecida
	 */
	public String getDescricaoUfFavorecida() {
		return descricaoUfFavorecida;
	}

	/**
	 * Set: descricaoUfFavorecida.
	 *
	 * @param descricaoUfFavorecida the descricao uf favorecida
	 */
	public void setDescricaoUfFavorecida(String descricaoUfFavorecida) {
		this.descricaoUfFavorecida = descricaoUfFavorecida;
	}

	/**
	 * Get: dsAgenciaFavorecido.
	 *
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}

	/**
	 * Set: dsAgenciaFavorecido.
	 *
	 * @param dsAgenciaFavorecido the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}

	/**
	 * Get: dsBancoFavorecido.
	 *
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}

	/**
	 * Set: dsBancoFavorecido.
	 *
	 * @param dsBancoFavorecido the ds banco favorecido
	 */
	public void setDsBancoFavorecido(String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}

	/**
	 * Get: dsContaFavorecido.
	 *
	 * @return dsContaFavorecido
	 */
	public String getDsContaFavorecido() {
		return dsContaFavorecido;
	}

	/**
	 * Set: dsContaFavorecido.
	 *
	 * @param dsContaFavorecido the ds conta favorecido
	 */
	public void setDsContaFavorecido(String dsContaFavorecido) {
		this.dsContaFavorecido = dsContaFavorecido;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}

	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}

	/**
	 * Get: dsIndicadorModalidade.
	 *
	 * @return dsIndicadorModalidade
	 */
	public String getDsIndicadorModalidade() {
		return dsIndicadorModalidade;
	}

	/**
	 * Set: dsIndicadorModalidade.
	 *
	 * @param dsIndicadorModalidade the ds indicador modalidade
	 */
	public void setDsIndicadorModalidade(String dsIndicadorModalidade) {
		this.dsIndicadorModalidade = dsIndicadorModalidade;
	}

	/**
	 * Get: dsMensagemPrimeiraLinha.
	 *
	 * @return dsMensagemPrimeiraLinha
	 */
	public String getDsMensagemPrimeiraLinha() {
		return dsMensagemPrimeiraLinha;
	}

	/**
	 * Set: dsMensagemPrimeiraLinha.
	 *
	 * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
	 */
	public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
		this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
	}

	/**
	 * Get: dsMensagemSegundaLinha.
	 *
	 * @return dsMensagemSegundaLinha
	 */
	public String getDsMensagemSegundaLinha() {
		return dsMensagemSegundaLinha;
	}

	/**
	 * Set: dsMensagemSegundaLinha.
	 *
	 * @param dsMensagemSegundaLinha the ds mensagem segunda linha
	 */
	public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
		this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
	}

	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	/**
	 * Get: dsPagamento.
	 *
	 * @return dsPagamento
	 */
	public String getDsPagamento() {
		return dsPagamento;
	}

	/**
	 * Set: dsPagamento.
	 *
	 * @param dsPagamento the ds pagamento
	 */
	public void setDsPagamento(String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: dsTipoContaFavorecido.
	 *
	 * @return dsTipoContaFavorecido
	 */
	public String getDsTipoContaFavorecido() {
		return dsTipoContaFavorecido;
	}

	/**
	 * Set: dsTipoContaFavorecido.
	 *
	 * @param dsTipoContaFavorecido the ds tipo conta favorecido
	 */
	public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
		this.dsTipoContaFavorecido = dsTipoContaFavorecido;
	}

	/**
	 * Get: dsUsoEmpresa.
	 *
	 * @return dsUsoEmpresa
	 */
	public String getDsUsoEmpresa() {
		return dsUsoEmpresa;
	}

	/**
	 * Set: dsUsoEmpresa.
	 *
	 * @param dsUsoEmpresa the ds uso empresa
	 */
	public void setDsUsoEmpresa(String dsUsoEmpresa) {
		this.dsUsoEmpresa = dsUsoEmpresa;
	}

	/**
	 * Get: dtAgendamento.
	 *
	 * @return dtAgendamento
	 */
	public String getDtAgendamento() {
		return dtAgendamento;
	}

	/**
	 * Set: dtAgendamento.
	 *
	 * @param dtAgendamento the dt agendamento
	 */
	public void setDtAgendamento(String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	/**
	 * Get: dtEmissaoDocumento.
	 *
	 * @return dtEmissaoDocumento
	 */
	public String getDtEmissaoDocumento() {
		return dtEmissaoDocumento;
	}

	/**
	 * Set: dtEmissaoDocumento.
	 *
	 * @param dtEmissaoDocumento the dt emissao documento
	 */
	public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
		this.dtEmissaoDocumento = dtEmissaoDocumento;
	}

	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	/**
	 * Get: dtVencimento.
	 *
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 *
	 * @param dtVencimento the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: inscricaoEstadualContribuinte.
	 *
	 * @return inscricaoEstadualContribuinte
	 */
	public String getInscricaoEstadualContribuinte() {
		return inscricaoEstadualContribuinte;
	}

	/**
	 * Set: inscricaoEstadualContribuinte.
	 *
	 * @param inscricaoEstadualContribuinte the inscricao estadual contribuinte
	 */
	public void setInscricaoEstadualContribuinte(String inscricaoEstadualContribuinte) {
		this.inscricaoEstadualContribuinte = inscricaoEstadualContribuinte;
	}

	/**
	 * Get: nomeOrgaoFavorecido.
	 *
	 * @return nomeOrgaoFavorecido
	 */
	public String getNomeOrgaoFavorecido() {
		return nomeOrgaoFavorecido;
	}

	/**
	 * Set: nomeOrgaoFavorecido.
	 *
	 * @param nomeOrgaoFavorecido the nome orgao favorecido
	 */
	public void setNomeOrgaoFavorecido(String nomeOrgaoFavorecido) {
		this.nomeOrgaoFavorecido = nomeOrgaoFavorecido;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nrDocumento.
	 *
	 * @return nrDocumento
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}

	/**
	 * Set: nrDocumento.
	 *
	 * @param nrDocumento the nr documento
	 */
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	/**
	 * Get: nrLoteArquivoRemessa.
	 *
	 * @return nrLoteArquivoRemessa
	 */
	public String getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}

	/**
	 * Set: nrLoteArquivoRemessa.
	 *
	 * @param nrLoteArquivoRemessa the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: nrSequenciaArquivoRemessa.
	 *
	 * @return nrSequenciaArquivoRemessa
	 */
	public String getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}

	/**
	 * Set: nrSequenciaArquivoRemessa.
	 *
	 * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}

	/**
	 * Get: numeroCotaParcela.
	 *
	 * @return numeroCotaParcela
	 */
	public String getNumeroCotaParcela() {
		return numeroCotaParcela;
	}

	/**
	 * Set: numeroCotaParcela.
	 *
	 * @param numeroCotaParcela the numero cota parcela
	 */
	public void setNumeroCotaParcela(String numeroCotaParcela) {
		this.numeroCotaParcela = numeroCotaParcela;
	}

	/**
	 * Get: numeroInscricaoFavorecido.
	 *
	 * @return numeroInscricaoFavorecido
	 */
	public String getNumeroInscricaoFavorecido() {
		return numeroInscricaoFavorecido;
	}

	/**
	 * Set: numeroInscricaoFavorecido.
	 *
	 * @param numeroInscricaoFavorecido the numero inscricao favorecido
	 */
	public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
		this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
	}

	/**
	 * Get: numeroParcelamento.
	 *
	 * @return numeroParcelamento
	 */
	public String getNumeroParcelamento() {
		return numeroParcelamento;
	}

	/**
	 * Set: numeroParcelamento.
	 *
	 * @param numeroParcelamento the numero parcelamento
	 */
	public void setNumeroParcelamento(String numeroParcelamento) {
		this.numeroParcelamento = numeroParcelamento;
	}

	/**
	 * Get: numeroReferencia.
	 *
	 * @return numeroReferencia
	 */
	public String getNumeroReferencia() {
		return numeroReferencia;
	}

	/**
	 * Set: numeroReferencia.
	 *
	 * @param numeroReferencia the numero referencia
	 */
	public void setNumeroReferencia(String numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}

	/**
	 * Get: observacaoTributo.
	 *
	 * @return observacaoTributo
	 */
	public String getObservacaoTributo() {
		return observacaoTributo;
	}

	/**
	 * Set: observacaoTributo.
	 *
	 * @param observacaoTributo the observacao tributo
	 */
	public void setObservacaoTributo(String observacaoTributo) {
		this.observacaoTributo = observacaoTributo;
	}

	/**
	 * Get: percentualReceita.
	 *
	 * @return percentualReceita
	 */
	public BigDecimal getPercentualReceita() {
		return percentualReceita;
	}

	/**
	 * Set: percentualReceita.
	 *
	 * @param percentualReceita the percentual receita
	 */
	public void setPercentualReceita(BigDecimal percentualReceita) {
		this.percentualReceita = percentualReceita;
	}

	/**
	 * Get: periodoApuracao.
	 *
	 * @return periodoApuracao
	 */
	public String getPeriodoApuracao() {
		return periodoApuracao;
	}

	/**
	 * Set: periodoApuracao.
	 *
	 * @param periodoApuracao the periodo apuracao
	 */
	public void setPeriodoApuracao(String periodoApuracao) {
		this.periodoApuracao = periodoApuracao;
	}

	/**
	 * Get: placaVeiculo.
	 *
	 * @return placaVeiculo
	 */
	public String getPlacaVeiculo() {
		return placaVeiculo;
	}

	/**
	 * Set: placaVeiculo.
	 *
	 * @param placaVeiculo the placa veiculo
	 */
	public void setPlacaVeiculo(String placaVeiculo) {
		this.placaVeiculo = placaVeiculo;
	}

	/**
	 * Get: qtMoeda.
	 *
	 * @return qtMoeda
	 */
	public BigDecimal getQtMoeda() {
		return qtMoeda;
	}

	/**
	 * Set: qtMoeda.
	 *
	 * @param qtMoeda the qt moeda
	 */
	public void setQtMoeda(BigDecimal qtMoeda) {
		this.qtMoeda = qtMoeda;
	}

	/**
	 * Get: telefoneContribuinte.
	 *
	 * @return telefoneContribuinte
	 */
	public String getTelefoneContribuinte() {
		return telefoneContribuinte;
	}

	/**
	 * Set: telefoneContribuinte.
	 *
	 * @param telefoneContribuinte the telefone contribuinte
	 */
	public void setTelefoneContribuinte(String telefoneContribuinte) {
		this.telefoneContribuinte = telefoneContribuinte;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: tipoDocumento.
	 *
	 * @return tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Set: tipoDocumento.
	 *
	 * @param tipoDocumento the tipo documento
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Get: tipoFavorecido.
	 *
	 * @return tipoFavorecido
	 */
	public Integer getTipoFavorecido() {
		return tipoFavorecido;
	}

	/**
	 * Set: tipoFavorecido.
	 *
	 * @param tipoFavorecido the tipo favorecido
	 */
	public void setTipoFavorecido(Integer tipoFavorecido) {
		this.tipoFavorecido = tipoFavorecido;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: vlAbatimento.
	 *
	 * @return vlAbatimento
	 */
	public BigDecimal getVlAbatimento() {
		return vlAbatimento;
	}

	/**
	 * Set: vlAbatimento.
	 *
	 * @param vlAbatimento the vl abatimento
	 */
	public void setVlAbatimento(BigDecimal vlAbatimento) {
		this.vlAbatimento = vlAbatimento;
	}

	/**
	 * Get: vlAcrescimoFinanceiro.
	 *
	 * @return vlAcrescimoFinanceiro
	 */
	public BigDecimal getVlAcrescimoFinanceiro() {
		return vlAcrescimoFinanceiro;
	}

	/**
	 * Set: vlAcrescimoFinanceiro.
	 *
	 * @param vlAcrescimoFinanceiro the vl acrescimo financeiro
	 */
	public void setVlAcrescimoFinanceiro(BigDecimal vlAcrescimoFinanceiro) {
		this.vlAcrescimoFinanceiro = vlAcrescimoFinanceiro;
	}

	/**
	 * Get: vlAtualizacaoMonetaria.
	 *
	 * @return vlAtualizacaoMonetaria
	 */
	public BigDecimal getVlAtualizacaoMonetaria() {
		return vlAtualizacaoMonetaria;
	}

	/**
	 * Set: vlAtualizacaoMonetaria.
	 *
	 * @param vlAtualizacaoMonetaria the vl atualizacao monetaria
	 */
	public void setVlAtualizacaoMonetaria(BigDecimal vlAtualizacaoMonetaria) {
		this.vlAtualizacaoMonetaria = vlAtualizacaoMonetaria;
	}

	/**
	 * Get: vlDesconto.
	 *
	 * @return vlDesconto
	 */
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}

	/**
	 * Set: vlDesconto.
	 *
	 * @param vlDesconto the vl desconto
	 */
	public void setVlDesconto(BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	/**
	 * Get: vlDocumento.
	 *
	 * @return vlDocumento
	 */
	public BigDecimal getVlDocumento() {
		return vlDocumento;
	}

	/**
	 * Set: vlDocumento.
	 *
	 * @param vlDocumento the vl documento
	 */
	public void setVlDocumento(BigDecimal vlDocumento) {
		this.vlDocumento = vlDocumento;
	}

	/**
	 * Get: vlHonorarioAdvogado.
	 *
	 * @return vlHonorarioAdvogado
	 */
	public BigDecimal getVlHonorarioAdvogado() {
		return vlHonorarioAdvogado;
	}

	/**
	 * Set: vlHonorarioAdvogado.
	 *
	 * @param vlHonorarioAdvogado the vl honorario advogado
	 */
	public void setVlHonorarioAdvogado(BigDecimal vlHonorarioAdvogado) {
		this.vlHonorarioAdvogado = vlHonorarioAdvogado;
	}

	/**
	 * Get: vlMora.
	 *
	 * @return vlMora
	 */
	public BigDecimal getVlMora() {
		return vlMora;
	}

	/**
	 * Set: vlMora.
	 *
	 * @param vlMora the vl mora
	 */
	public void setVlMora(BigDecimal vlMora) {
		this.vlMora = vlMora;
	}

	/**
	 * Get: vlMulta.
	 *
	 * @return vlMulta
	 */
	public BigDecimal getVlMulta() {
		return vlMulta;
	}

	/**
	 * Set: vlMulta.
	 *
	 * @param vlMulta the vl multa
	 */
	public void setVlMulta(BigDecimal vlMulta) {
		this.vlMulta = vlMulta;
	}

	/**
	 * Get: vlPrincipal.
	 *
	 * @return vlPrincipal
	 */
	public BigDecimal getVlPrincipal() {
		return vlPrincipal;
	}

	/**
	 * Set: vlPrincipal.
	 *
	 * @param vlPrincipal the vl principal
	 */
	public void setVlPrincipal(BigDecimal vlPrincipal) {
		this.vlPrincipal = vlPrincipal;
	}

	/**
	 * Get: vlrAgendamento.
	 *
	 * @return vlrAgendamento
	 */
	public BigDecimal getVlrAgendamento() {
		return vlrAgendamento;
	}

	/**
	 * Set: vlrAgendamento.
	 *
	 * @param vlrAgendamento the vlr agendamento
	 */
	public void setVlrAgendamento(BigDecimal vlrAgendamento) {
		this.vlrAgendamento = vlrAgendamento;
	}

	/**
	 * Get: vlReceita.
	 *
	 * @return vlReceita
	 */
	public BigDecimal getVlReceita() {
		return vlReceita;
	}

	/**
	 * Set: vlReceita.
	 *
	 * @param vlReceita the vl receita
	 */
	public void setVlReceita(BigDecimal vlReceita) {
		this.vlReceita = vlReceita;
	}

	/**
	 * Get: vlrEfetivacao.
	 *
	 * @return vlrEfetivacao
	 */
	public BigDecimal getVlrEfetivacao() {
		return vlrEfetivacao;
	}

	/**
	 * Set: vlrEfetivacao.
	 *
	 * @param vlrEfetivacao the vlr efetivacao
	 */
	public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
		this.vlrEfetivacao = vlrEfetivacao;
	}

	/**
	 * Get: vlTotal.
	 *
	 * @return vlTotal
	 */
	public BigDecimal getVlTotal() {
		return vlTotal;
	}

	/**
	 * Set: vlTotal.
	 *
	 * @param vlTotal the vl total
	 */
	public void setVlTotal(BigDecimal vlTotal) {
		this.vlTotal = vlTotal;
	}

	/**
	 * Get: dtNascimentoBeneficiario.
	 *
	 * @return dtNascimentoBeneficiario
	 */
	public String getDtNascimentoBeneficiario() {
		return dtNascimentoBeneficiario;
	}

	/**
	 * Set: dtNascimentoBeneficiario.
	 *
	 * @param dtNascimentoBeneficiario the dt nascimento beneficiario
	 */
	public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
		this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
	}

	/**
	 * Get: nomeBeneficiario.
	 *
	 * @return nomeBeneficiario
	 */
	public String getNomeBeneficiario() {
		return nomeBeneficiario;
	}

	/**
	 * Set: nomeBeneficiario.
	 *
	 * @param nomeBeneficiario the nome beneficiario
	 */
	public void setNomeBeneficiario(String nomeBeneficiario) {
		this.nomeBeneficiario = nomeBeneficiario;
	}

	/**
	 * Get: numeroInscricaoBeneficiario.
	 *
	 * @return numeroInscricaoBeneficiario
	 */
	public String getNumeroInscricaoBeneficiario() {
		return numeroInscricaoBeneficiario;
	}

	/**
	 * Set: numeroInscricaoBeneficiario.
	 *
	 * @param numeroInscricaoBeneficiario the numero inscricao beneficiario
	 */
	public void setNumeroInscricaoBeneficiario(String numeroInscricaoBeneficiario) {
		this.numeroInscricaoBeneficiario = numeroInscricaoBeneficiario;
	}

	/**
	 * Get: tipoBeneficiario.
	 *
	 * @return tipoBeneficiario
	 */
	public String getTipoBeneficiario() {
		return tipoBeneficiario;
	}

	/**
	 * Set: tipoBeneficiario.
	 *
	 * @param tipoBeneficiario the tipo beneficiario
	 */
	public void setTipoBeneficiario(String tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}

	/**
	 * Get: agenciaDigitoCredito.
	 *
	 * @return agenciaDigitoCredito
	 */
	public String getAgenciaDigitoCredito() {
		return agenciaDigitoCredito;
	}

	/**
	 * Set: agenciaDigitoCredito.
	 *
	 * @param agenciaDigitoCredito the agencia digito credito
	 */
	public void setAgenciaDigitoCredito(String agenciaDigitoCredito) {
		this.agenciaDigitoCredito = agenciaDigitoCredito;
	}

	/**
	 * Get: agenciaDigitoDestino.
	 *
	 * @return agenciaDigitoDestino
	 */
	public String getAgenciaDigitoDestino() {
		return agenciaDigitoDestino;
	}

	/**
	 * Set: agenciaDigitoDestino.
	 *
	 * @param agenciaDigitoDestino the agencia digito destino
	 */
	public void setAgenciaDigitoDestino(String agenciaDigitoDestino) {
		this.agenciaDigitoDestino = agenciaDigitoDestino;
	}

	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public String getCdBancoCredito() {
		return cdBancoCredito;
	}

	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(String cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}

	/**
	 * Get: cdBancoDestino.
	 *
	 * @return cdBancoDestino
	 */
	public String getCdBancoDestino() {
		return cdBancoDestino;
	}

	/**
	 * Set: cdBancoDestino.
	 *
	 * @param cdBancoDestino the cd banco destino
	 */
	public void setCdBancoDestino(String cdBancoDestino) {
		this.cdBancoDestino = cdBancoDestino;
	}

	/**
	 * Get: contaDigitoCredito.
	 *
	 * @return contaDigitoCredito
	 */
	public String getContaDigitoCredito() {
		return contaDigitoCredito;
	}

	/**
	 * Set: contaDigitoCredito.
	 *
	 * @param contaDigitoCredito the conta digito credito
	 */
	public void setContaDigitoCredito(String contaDigitoCredito) {
		this.contaDigitoCredito = contaDigitoCredito;
	}

	/**
	 * Get: contaDigitoDestino.
	 *
	 * @return contaDigitoDestino
	 */
	public String getContaDigitoDestino() {
		return contaDigitoDestino;
	}

	/**
	 * Set: contaDigitoDestino.
	 *
	 * @param contaDigitoDestino the conta digito destino
	 */
	public void setContaDigitoDestino(String contaDigitoDestino) {
		this.contaDigitoDestino = contaDigitoDestino;
	}

	/**
	 * Get: tipoContaCredito.
	 *
	 * @return tipoContaCredito
	 */
	public String getTipoContaCredito() {
		return tipoContaCredito;
	}

	/**
	 * Set: tipoContaCredito.
	 *
	 * @param tipoContaCredito the tipo conta credito
	 */
	public void setTipoContaCredito(String tipoContaCredito) {
		this.tipoContaCredito = tipoContaCredito;
	}

	/**
	 * Get: tipoContaDestino.
	 *
	 * @return tipoContaDestino
	 */
	public String getTipoContaDestino() {
		return tipoContaDestino;
	}

	/**
	 * Set: tipoContaDestino.
	 *
	 * @param tipoContaDestino the tipo conta destino
	 */
	public void setTipoContaDestino(String tipoContaDestino) {
		this.tipoContaDestino = tipoContaDestino;
	}

	/**
	 * Get: vlAcrescimo.
	 *
	 * @return vlAcrescimo
	 */
	public BigDecimal getVlAcrescimo() {
		return vlAcrescimo;
	}

	/**
	 * Set: vlAcrescimo.
	 *
	 * @param vlAcrescimo the vl acrescimo
	 */
	public void setVlAcrescimo(BigDecimal vlAcrescimo) {
		this.vlAcrescimo = vlAcrescimo;
	}

	/**
	 * Get: vlDeducao.
	 *
	 * @return vlDeducao
	 */
	public BigDecimal getVlDeducao() {
		return vlDeducao;
	}

	/**
	 * Set: vlDeducao.
	 *
	 * @param vlDeducao the vl deducao
	 */
	public void setVlDeducao(BigDecimal vlDeducao) {
		this.vlDeducao = vlDeducao;
	}

	/**
	 * Get: vlImpostoRenda.
	 *
	 * @return vlImpostoRenda
	 */
	public BigDecimal getVlImpostoRenda() {
		return vlImpostoRenda;
	}

	/**
	 * Set: vlImpostoRenda.
	 *
	 * @param vlImpostoRenda the vl imposto renda
	 */
	public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
		this.vlImpostoRenda = vlImpostoRenda;
	}

	/**
	 * Get: vlIss.
	 *
	 * @return vlIss
	 */
	public BigDecimal getVlIss() {
		return vlIss;
	}

	/**
	 * Set: vlIss.
	 *
	 * @param vlIss the vl iss
	 */
	public void setVlIss(BigDecimal vlIss) {
		this.vlIss = vlIss;
	}

	/**
	 * Get: vlInss.
	 *
	 * @return vlInss
	 */
	public BigDecimal getVlInss() {
		return vlInss;
	}

	/**
	 * Set: vlInss.
	 *
	 * @param vlInss the vl inss
	 */
	public void setVlInss(BigDecimal vlInss) {
		this.vlInss = vlInss;
	}

	/**
	 * Get: vlIof.
	 *
	 * @return vlIof
	 */
	public BigDecimal getVlIof() {
		return vlIof;
	}

	/**
	 * Set: vlIof.
	 *
	 * @param vlIof the vl iof
	 */
	public void setVlIof(BigDecimal vlIof) {
		this.vlIof = vlIof;
	}

	/**
	 * Get: codigoRenavam.
	 *
	 * @return codigoRenavam
	 */
	public String getCodigoRenavam() {
		return codigoRenavam;
	}

	/**
	 * Set: codigoRenavam.
	 *
	 * @param codigoRenavam the codigo renavam
	 */
	public void setCodigoRenavam(String codigoRenavam) {
		this.codigoRenavam = codigoRenavam;
	}

	/**
	 * Get: municipioTributo.
	 *
	 * @return municipioTributo
	 */
	public String getMunicipioTributo() {
		return municipioTributo;
	}

	/**
	 * Set: municipioTributo.
	 *
	 * @param municipioTributo the municipio tributo
	 */
	public void setMunicipioTributo(String municipioTributo) {
		this.municipioTributo = municipioTributo;
	}

	/**
	 * Get: numeroNsu.
	 *
	 * @return numeroNsu
	 */
	public String getNumeroNsu() {
		return numeroNsu;
	}

	/**
	 * Set: numeroNsu.
	 *
	 * @param numeroNsu the numero nsu
	 */
	public void setNumeroNsu(String numeroNsu) {
		this.numeroNsu = numeroNsu;
	}

	/**
	 * Get: opcaoRetiradaTributo.
	 *
	 * @return opcaoRetiradaTributo
	 */
	public String getOpcaoRetiradaTributo() {
		return opcaoRetiradaTributo;
	}

	/**
	 * Set: opcaoRetiradaTributo.
	 *
	 * @param opcaoRetiradaTributo the opcao retirada tributo
	 */
	public void setOpcaoRetiradaTributo(String opcaoRetiradaTributo) {
		this.opcaoRetiradaTributo = opcaoRetiradaTributo;
	}

	/**
	 * Get: opcaoTributo.
	 *
	 * @return opcaoTributo
	 */
	public String getOpcaoTributo() {
		return opcaoTributo;
	}

	/**
	 * Set: opcaoTributo.
	 *
	 * @param opcaoTributo the opcao tributo
	 */
	public void setOpcaoTributo(String opcaoTributo) {
		this.opcaoTributo = opcaoTributo;
	}

	/**
	 * Get: ufTributo.
	 *
	 * @return ufTributo
	 */
	public String getUfTributo() {
		return ufTributo;
	}

	/**
	 * Set: ufTributo.
	 *
	 * @param ufTributo the uf tributo
	 */
	public void setUfTributo(String ufTributo) {
		this.ufTributo = ufTributo;
	}

	/**
	 * Get: bancoCartaoDepositoIdentificado.
	 *
	 * @return bancoCartaoDepositoIdentificado
	 */
	public String getBancoCartaoDepositoIdentificado() {
		return bancoCartaoDepositoIdentificado;
	}

	/**
	 * Set: bancoCartaoDepositoIdentificado.
	 *
	 * @param bancoCartaoDepositoIdentificado the banco cartao deposito identificado
	 */
	public void setBancoCartaoDepositoIdentificado(String bancoCartaoDepositoIdentificado) {
		this.bancoCartaoDepositoIdentificado = bancoCartaoDepositoIdentificado;
	}

	/**
	 * Get: bimCartaoDepositoIdentificado.
	 *
	 * @return bimCartaoDepositoIdentificado
	 */
	public String getBimCartaoDepositoIdentificado() {
		return bimCartaoDepositoIdentificado;
	}

	/**
	 * Set: bimCartaoDepositoIdentificado.
	 *
	 * @param bimCartaoDepositoIdentificado the bim cartao deposito identificado
	 */
	public void setBimCartaoDepositoIdentificado(String bimCartaoDepositoIdentificado) {
		this.bimCartaoDepositoIdentificado = bimCartaoDepositoIdentificado;
	}

	/**
	 * Get: cartaoDepositoIdentificado.
	 *
	 * @return cartaoDepositoIdentificado
	 */
	public String getCartaoDepositoIdentificado() {
		return cartaoDepositoIdentificado;
	}

	/**
	 * Set: cartaoDepositoIdentificado.
	 *
	 * @param cartaoDepositoIdentificado the cartao deposito identificado
	 */
	public void setCartaoDepositoIdentificado(String cartaoDepositoIdentificado) {
		this.cartaoDepositoIdentificado = cartaoDepositoIdentificado;
	}

	/**
	 * Get: clienteDepositoIdentificado.
	 *
	 * @return clienteDepositoIdentificado
	 */
	public String getClienteDepositoIdentificado() {
		return clienteDepositoIdentificado;
	}

	/**
	 * Set: clienteDepositoIdentificado.
	 *
	 * @param clienteDepositoIdentificado the cliente deposito identificado
	 */
	public void setClienteDepositoIdentificado(String clienteDepositoIdentificado) {
		this.clienteDepositoIdentificado = clienteDepositoIdentificado;
	}

	/**
	 * Get: codigoDepositante.
	 *
	 * @return codigoDepositante
	 */
	public String getCodigoDepositante() {
		return codigoDepositante;
	}

	/**
	 * Set: codigoDepositante.
	 *
	 * @param codigoDepositante the codigo depositante
	 */
	public void setCodigoDepositante(String codigoDepositante) {
		this.codigoDepositante = codigoDepositante;
	}

	/**
	 * Get: nomeDepositante.
	 *
	 * @return nomeDepositante
	 */
	public String getNomeDepositante() {
		return nomeDepositante;
	}

	/**
	 * Set: nomeDepositante.
	 *
	 * @param nomeDepositante the nome depositante
	 */
	public void setNomeDepositante(String nomeDepositante) {
		this.nomeDepositante = nomeDepositante;
	}

	/**
	 * Get: produtoDepositoIdentificado.
	 *
	 * @return produtoDepositoIdentificado
	 */
	public String getProdutoDepositoIdentificado() {
		return produtoDepositoIdentificado;
	}

	/**
	 * Set: produtoDepositoIdentificado.
	 *
	 * @param produtoDepositoIdentificado the produto deposito identificado
	 */
	public void setProdutoDepositoIdentificado(String produtoDepositoIdentificado) {
		this.produtoDepositoIdentificado = produtoDepositoIdentificado;
	}

	/**
	 * Get: sequenciaProdutoDepositoIdentificado.
	 *
	 * @return sequenciaProdutoDepositoIdentificado
	 */
	public String getSequenciaProdutoDepositoIdentificado() {
		return sequenciaProdutoDepositoIdentificado;
	}

	/**
	 * Set: sequenciaProdutoDepositoIdentificado.
	 *
	 * @param sequenciaProdutoDepositoIdentificado the sequencia produto deposito identificado
	 */
	public void setSequenciaProdutoDepositoIdentificado(String sequenciaProdutoDepositoIdentificado) {
		this.sequenciaProdutoDepositoIdentificado = sequenciaProdutoDepositoIdentificado;
	}

	/**
	 * Get: tipoDespositoIdentificado.
	 *
	 * @return tipoDespositoIdentificado
	 */
	public String getTipoDespositoIdentificado() {
		return tipoDespositoIdentificado;
	}

	/**
	 * Set: tipoDespositoIdentificado.
	 *
	 * @param tipoDespositoIdentificado the tipo desposito identificado
	 */
	public void setTipoDespositoIdentificado(String tipoDespositoIdentificado) {
		this.tipoDespositoIdentificado = tipoDespositoIdentificado;
	}

	/**
	 * Get: viaCartaoDepositoIdentificado.
	 *
	 * @return viaCartaoDepositoIdentificado
	 */
	public String getViaCartaoDepositoIdentificado() {
		return viaCartaoDepositoIdentificado;
	}

	/**
	 * Set: viaCartaoDepositoIdentificado.
	 *
	 * @param viaCartaoDepositoIdentificado the via cartao deposito identificado
	 */
	public void setViaCartaoDepositoIdentificado(String viaCartaoDepositoIdentificado) {
		this.viaCartaoDepositoIdentificado = viaCartaoDepositoIdentificado;
	}

	/**
	 * Get: camaraCentralizadora.
	 *
	 * @return camaraCentralizadora
	 */
	public String getCamaraCentralizadora() {
		return camaraCentralizadora;
	}

	/**
	 * Set: camaraCentralizadora.
	 *
	 * @param camaraCentralizadora the camara centralizadora
	 */
	public void setCamaraCentralizadora(String camaraCentralizadora) {
		this.camaraCentralizadora = camaraCentralizadora;
	}

	/**
	 * Get: finalidadeDoc.
	 *
	 * @return finalidadeDoc
	 */
	public String getFinalidadeDoc() {
		return finalidadeDoc;
	}

	/**
	 * Set: finalidadeDoc.
	 *
	 * @param finalidadeDoc the finalidade doc
	 */
	public void setFinalidadeDoc(String finalidadeDoc) {
		this.finalidadeDoc = finalidadeDoc;
	}

	/**
	 * Get: identificacaoTitularidade.
	 *
	 * @return identificacaoTitularidade
	 */
	public String getIdentificacaoTitularidade() {
		return identificacaoTitularidade;
	}

	/**
	 * Set: identificacaoTitularidade.
	 *
	 * @param identificacaoTitularidade the identificacao titularidade
	 */
	public void setIdentificacaoTitularidade(String identificacaoTitularidade) {
		this.identificacaoTitularidade = identificacaoTitularidade;
	}

	/**
	 * Get: identificacaoTransferenciaDoc.
	 *
	 * @return identificacaoTransferenciaDoc
	 */
	public String getIdentificacaoTransferenciaDoc() {
		return identificacaoTransferenciaDoc;
	}

	/**
	 * Set: identificacaoTransferenciaDoc.
	 *
	 * @param identificacaoTransferenciaDoc the identificacao transferencia doc
	 */
	public void setIdentificacaoTransferenciaDoc(String identificacaoTransferenciaDoc) {
		this.identificacaoTransferenciaDoc = identificacaoTransferenciaDoc;
	}

	/**
	 * Get: codigoInss.
	 *
	 * @return codigoInss
	 */
	public String getCodigoInss() {
		return codigoInss;
	}

	/**
	 * Set: codigoInss.
	 *
	 * @param codigoInss the codigo inss
	 */
	public void setCodigoInss(String codigoInss) {
		this.codigoInss = codigoInss;
	}

	/**
	 * Get: vlOutrasEntidades.
	 *
	 * @return vlOutrasEntidades
	 */
	public BigDecimal getVlOutrasEntidades() {
		return vlOutrasEntidades;
	}

	/**
	 * Set: vlOutrasEntidades.
	 *
	 * @param vlOutrasEntidades the vl outras entidades
	 */
	public void setVlOutrasEntidades(BigDecimal vlOutrasEntidades) {
		this.vlOutrasEntidades = vlOutrasEntidades;
	}

	/**
	 * Get: codigoOrdemCredito.
	 *
	 * @return codigoOrdemCredito
	 */
	public String getCodigoOrdemCredito() {
		return codigoOrdemCredito;
	}

	/**
	 * Set: codigoOrdemCredito.
	 *
	 * @param codigoOrdemCredito the codigo ordem credito
	 */
	public void setCodigoOrdemCredito(String codigoOrdemCredito) {
		this.codigoOrdemCredito = codigoOrdemCredito;
	}

	/**
	 * Get: codigoPagador.
	 *
	 * @return codigoPagador
	 */
	public String getCodigoPagador() {
		return codigoPagador;
	}

	/**
	 * Set: codigoPagador.
	 *
	 * @param codigoPagador the codigo pagador
	 */
	public void setCodigoPagador(String codigoPagador) {
		this.codigoPagador = codigoPagador;
	}

	/**
	 * Get: dataExpiracaoCredito.
	 *
	 * @return dataExpiracaoCredito
	 */
	public String getDataExpiracaoCredito() {
		return dataExpiracaoCredito;
	}

	/**
	 * Set: dataExpiracaoCredito.
	 *
	 * @param dataExpiracaoCredito the data expiracao credito
	 */
	public void setDataExpiracaoCredito(String dataExpiracaoCredito) {
		this.dataExpiracaoCredito = dataExpiracaoCredito;
	}

	/**
	 * Get: dataRetirada.
	 *
	 * @return dataRetirada
	 */
	public String getDataRetirada() {
		return dataRetirada;
	}

	/**
	 * Set: dataRetirada.
	 *
	 * @param dataRetirada the data retirada
	 */
	public void setDataRetirada(String dataRetirada) {
		this.dataRetirada = dataRetirada;
	}

	/**
	 * Get: identificadorRetirada.
	 *
	 * @return identificadorRetirada
	 */
	public String getIdentificadorRetirada() {
		return identificadorRetirada;
	}

	/**
	 * Set: identificadorRetirada.
	 *
	 * @param identificadorRetirada the identificador retirada
	 */
	public void setIdentificadorRetirada(String identificadorRetirada) {
		this.identificadorRetirada = identificadorRetirada;
	}

	/**
	 * Get: identificadorTipoRetirada.
	 *
	 * @return identificadorTipoRetirada
	 */
	public String getIdentificadorTipoRetirada() {
		return identificadorTipoRetirada;
	}

	/**
	 * Set: identificadorTipoRetirada.
	 *
	 * @param identificadorTipoRetirada the identificador tipo retirada
	 */
	public void setIdentificadorTipoRetirada(String identificadorTipoRetirada) {
		this.identificadorTipoRetirada = identificadorTipoRetirada;
	}

	/**
	 * Get: indicadorAssociadoUnicaAgencia.
	 *
	 * @return indicadorAssociadoUnicaAgencia
	 */
	public String getIndicadorAssociadoUnicaAgencia() {
		return indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Set: indicadorAssociadoUnicaAgencia.
	 *
	 * @param indicadorAssociadoUnicaAgencia the indicador associado unica agencia
	 */
	public void setIndicadorAssociadoUnicaAgencia(String indicadorAssociadoUnicaAgencia) {
		this.indicadorAssociadoUnicaAgencia = indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Get: instrucaoPagamento.
	 *
	 * @return instrucaoPagamento
	 */
	public String getInstrucaoPagamento() {
		return instrucaoPagamento;
	}

	/**
	 * Set: instrucaoPagamento.
	 *
	 * @param instrucaoPagamento the instrucao pagamento
	 */
	public void setInstrucaoPagamento(String instrucaoPagamento) {
		this.instrucaoPagamento = instrucaoPagamento;
	}

	/**
	 * Get: numeroCheque.
	 *
	 * @return numeroCheque
	 */
	public String getNumeroCheque() {
		return numeroCheque;
	}

	/**
	 * Set: numeroCheque.
	 *
	 * @param numeroCheque the numero cheque
	 */
	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}

	/**
	 * Get: numeroOp.
	 *
	 * @return numeroOp
	 */
	public String getNumeroOp() {
		return numeroOp;
	}

	/**
	 * Set: numeroOp.
	 *
	 * @param numeroOp the numero op
	 */
	public void setNumeroOp(String numeroOp) {
		this.numeroOp = numeroOp;
	}

	/**
	 * Get: serieCheque.
	 *
	 * @return serieCheque
	 */
	public String getSerieCheque() {
		return serieCheque;
	}

	/**
	 * Set: serieCheque.
	 *
	 * @param serieCheque the serie cheque
	 */
	public void setSerieCheque(String serieCheque) {
		this.serieCheque = serieCheque;
	}

	/**
	 * Get: vlImpostoMovFinanceira.
	 *
	 * @return vlImpostoMovFinanceira
	 */
	public BigDecimal getVlImpostoMovFinanceira() {
		return vlImpostoMovFinanceira;
	}

	/**
	 * Set: vlImpostoMovFinanceira.
	 *
	 * @param vlImpostoMovFinanceira the vl imposto mov financeira
	 */
	public void setVlImpostoMovFinanceira(BigDecimal vlImpostoMovFinanceira) {
		this.vlImpostoMovFinanceira = vlImpostoMovFinanceira;
	}

	/**
	 * Get: finalidadeTed.
	 *
	 * @return finalidadeTed
	 */
	public String getFinalidadeTed() {
		return finalidadeTed;
	}

	/**
	 * Set: finalidadeTed.
	 *
	 * @param finalidadeTed the finalidade ted
	 */
	public void setFinalidadeTed(String finalidadeTed) {
		this.finalidadeTed = finalidadeTed;
	}

	/**
	 * Get: identificacaoTransferenciaTed.
	 *
	 * @return identificacaoTransferenciaTed
	 */
	public String getIdentificacaoTransferenciaTed() {
		return identificacaoTransferenciaTed;
	}

	/**
	 * Set: identificacaoTransferenciaTed.
	 *
	 * @param identificacaoTransferenciaTed the identificacao transferencia ted
	 */
	public void setIdentificacaoTransferenciaTed(String identificacaoTransferenciaTed) {
		this.identificacaoTransferenciaTed = identificacaoTransferenciaTed;
	}

	/**
	 * Get: cdTipoBeneficiario.
	 *
	 * @return cdTipoBeneficiario
	 */
	public Integer getCdTipoBeneficiario() {
		return cdTipoBeneficiario;
	}

	/**
	 * Set: cdTipoBeneficiario.
	 *
	 * @param cdTipoBeneficiario the cd tipo beneficiario
	 */
	public void setCdTipoBeneficiario(Integer cdTipoBeneficiario) {
		this.cdTipoBeneficiario = cdTipoBeneficiario;
	}

	/**
	 * Get: tipoCanal.
	 *
	 * @return tipoCanal
	 */
	public Integer getTipoCanal() {
		return tipoCanal;
	}

	/**
	 * Set: tipoCanal.
	 *
	 * @param tipoCanal the tipo canal
	 */
	public void setTipoCanal(Integer tipoCanal) {
		this.tipoCanal = tipoCanal;
	}

	// public String getContaDebito() {
	// return contaDebito;
	// }
	//
	// public void setContaDebito(String contaDebito) {
	// this.contaDebito = contaDebito;
	// }
	//
	// public String getDsAgenciaDebito() {
	// return dsAgenciaDebito;
	// }
	//
	// public void setDsAgenciaDebito(String dsAgenciaDebito) {
	// this.dsAgenciaDebito = dsAgenciaDebito;
	// }
	//
	// public String getDsBancoDebito() {
	// return dsBancoDebito;
	// }
	//
	// public void setDsBancoDebito(String dsBancoDebito) {
	// this.dsBancoDebito = dsBancoDebito;
	// }
	//
	// public String getTipoContaDebito() {
	// return tipoContaDebito;
	// }
	//
	// public void setTipoContaDebito(String tipoContaDebito) {
	// this.tipoContaDebito = tipoContaDebito;
	// }

	/**
	 * Get: cdEmpresaContrato.
	 *
	 * @return cdEmpresaContrato
	 */
	public Long getCdEmpresaContrato() {
		return cdEmpresaContrato;
	}

	/**
	 * Set: cdEmpresaContrato.
	 *
	 * @param cdEmpresaContrato the cd empresa contrato
	 */
	public void setCdEmpresaContrato(Long cdEmpresaContrato) {
		this.cdEmpresaContrato = cdEmpresaContrato;
	}

	/**
	 * Get: cdTipoConta.
	 *
	 * @return cdTipoConta
	 */
	public Integer getCdTipoConta() {
		return cdTipoConta;
	}

	/**
	 * Set: cdTipoConta.
	 *
	 * @param cdTipoConta the cd tipo conta
	 */
	public void setCdTipoConta(Integer cdTipoConta) {
		this.cdTipoConta = cdTipoConta;
	}

	/**
	 * Get: nrContrato.
	 *
	 * @return nrContrato
	 */
	public Long getNrContrato() {
		return nrContrato;
	}

	/**
	 * Set: nrContrato.
	 *
	 * @param nrContrato the nr contrato
	 */
	public void setNrContrato(Long nrContrato) {
		this.nrContrato = nrContrato;
	}

	/**
	 * Get: logoPath.
	 *
	 * @return logoPath
	 */
	public String getLogoPath() {
		return logoPath;
	}

	/**
	 * Set: logoPath.
	 *
	 * @param logoPath the logo path
	 */
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	/**
	 * Get: listaControleRadioSelecionados.
	 *
	 * @return listaControleRadioSelecionados
	 */
	public List<SelectItem> getListaControleRadioSelecionados() {
		return listaControleRadioSelecionados;
	}

	/**
	 * Set: listaControleRadioSelecionados.
	 *
	 * @param listaControleRadioSelecionados the lista controle radio selecionados
	 */
	public void setListaControleRadioSelecionados(List<SelectItem> listaControleRadioSelecionados) {
		this.listaControleRadioSelecionados = listaControleRadioSelecionados;
	}

	/**
	 * Get: contaDebito.
	 *
	 * @return contaDebito
	 */
	public String getContaDebito() {
		return contaDebito;
	}

	/**
	 * Set: contaDebito.
	 *
	 * @param contaDebito the conta debito
	 */
	public void setContaDebito(String contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: tipoContaDebito.
	 *
	 * @return tipoContaDebito
	 */
	public String getTipoContaDebito() {
		return tipoContaDebito;
	}

	/**
	 * Set: tipoContaDebito.
	 *
	 * @param tipoContaDebito the tipo conta debito
	 */
	public void setTipoContaDebito(String tipoContaDebito) {
		this.tipoContaDebito = tipoContaDebito;
	}

	/**
	 * Get: numeroPagamento.
	 *
	 * @return numeroPagamento
	 */
	public String getNumeroPagamento() {
		return numeroPagamento;
	}

	/**
	 * Set: numeroPagamento.
	 *
	 * @param numeroPagamento the numero pagamento
	 */
	public void setNumeroPagamento(String numeroPagamento) {
		this.numeroPagamento = numeroPagamento;
	}

	/**
	 * Get: dsOrigemPagamento.
	 *
	 * @return dsOrigemPagamento
	 */
	public String getDsOrigemPagamento() {
		return dsOrigemPagamento;
	}

	/**
	 * Set: dsOrigemPagamento.
	 *
	 * @param dsOrigemPagamento the ds origem pagamento
	 */
	public void setDsOrigemPagamento(String dsOrigemPagamento) {
		this.dsOrigemPagamento = dsOrigemPagamento;
	}

	/**
	 * Get: nossoNumero.
	 *
	 * @return nossoNumero
	 */
	public String getNossoNumero() {
		return nossoNumero;
	}

	/**
	 * Set: nossoNumero.
	 *
	 * @param nossoNumero the nosso numero
	 */
	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Is exibe beneficiario.
	 *
	 * @return true, if is exibe beneficiario
	 */
	public boolean isExibeBeneficiario() {
		return exibeBeneficiario;
	}

	/**
	 * Set: exibeBeneficiario.
	 *
	 * @param exibeBeneficiario the exibe beneficiario
	 */
	public void setExibeBeneficiario(boolean exibeBeneficiario) {
		this.exibeBeneficiario = exibeBeneficiario;
	}

	/**
	 * Get: cdSituacaoContratoInterm.
	 *
	 * @return cdSituacaoContratoInterm
	 */
	public String getCdSituacaoContratoInterm() {
		return cdSituacaoContratoInterm;
	}

	/**
	 * Set: cdSituacaoContratoInterm.
	 *
	 * @param cdSituacaoContratoInterm the cd situacao contrato interm
	 */
	public void setCdSituacaoContratoInterm(String cdSituacaoContratoInterm) {
		this.cdSituacaoContratoInterm = cdSituacaoContratoInterm;
	}

	/**
	 * Get: contaDebitoInterm.
	 *
	 * @return contaDebitoInterm
	 */
	public String getContaDebitoInterm() {
		return contaDebitoInterm;
	}

	/**
	 * Set: contaDebitoInterm.
	 *
	 * @param contaDebitoInterm the conta debito interm
	 */
	public void setContaDebitoInterm(String contaDebitoInterm) {
		this.contaDebitoInterm = contaDebitoInterm;
	}

	/**
	 * Get: dsAgenciaDebitoInterm.
	 *
	 * @return dsAgenciaDebitoInterm
	 */
	public String getDsAgenciaDebitoInterm() {
		return dsAgenciaDebitoInterm;
	}

	/**
	 * Set: dsAgenciaDebitoInterm.
	 *
	 * @param dsAgenciaDebitoInterm the ds agencia debito interm
	 */
	public void setDsAgenciaDebitoInterm(String dsAgenciaDebitoInterm) {
		this.dsAgenciaDebitoInterm = dsAgenciaDebitoInterm;
	}

	/**
	 * Get: dsBancoDebitoInterm.
	 *
	 * @return dsBancoDebitoInterm
	 */
	public String getDsBancoDebitoInterm() {
		return dsBancoDebitoInterm;
	}

	/**
	 * Set: dsBancoDebitoInterm.
	 *
	 * @param dsBancoDebitoInterm the ds banco debito interm
	 */
	public void setDsBancoDebitoInterm(String dsBancoDebitoInterm) {
		this.dsBancoDebitoInterm = dsBancoDebitoInterm;
	}

	/**
	 * Get: dsContratoInterm.
	 *
	 * @return dsContratoInterm
	 */
	public String getDsContratoInterm() {
		return dsContratoInterm;
	}

	/**
	 * Set: dsContratoInterm.
	 *
	 * @param dsContratoInterm the ds contrato interm
	 */
	public void setDsContratoInterm(String dsContratoInterm) {
		this.dsContratoInterm = dsContratoInterm;
	}

	/**
	 * Get: dsEmpresaInterm.
	 *
	 * @return dsEmpresaInterm
	 */
	public String getDsEmpresaInterm() {
		return dsEmpresaInterm;
	}

	/**
	 * Set: dsEmpresaInterm.
	 *
	 * @param dsEmpresaInterm the ds empresa interm
	 */
	public void setDsEmpresaInterm(String dsEmpresaInterm) {
		this.dsEmpresaInterm = dsEmpresaInterm;
	}

	/**
	 * Get: dsRazaoSocialInterm.
	 *
	 * @return dsRazaoSocialInterm
	 */
	public String getDsRazaoSocialInterm() {
		return dsRazaoSocialInterm;
	}

	/**
	 * Set: dsRazaoSocialInterm.
	 *
	 * @param dsRazaoSocialInterm the ds razao social interm
	 */
	public void setDsRazaoSocialInterm(String dsRazaoSocialInterm) {
		this.dsRazaoSocialInterm = dsRazaoSocialInterm;
	}

	/**
	 * Get: nrCnpjCpfInterm.
	 *
	 * @return nrCnpjCpfInterm
	 */
	public String getNrCnpjCpfInterm() {
		return nrCnpjCpfInterm;
	}

	/**
	 * Set: nrCnpjCpfInterm.
	 *
	 * @param nrCnpjCpfInterm the nr cnpj cpf interm
	 */
	public void setNrCnpjCpfInterm(String nrCnpjCpfInterm) {
		this.nrCnpjCpfInterm = nrCnpjCpfInterm;
	}

	/**
	 * Get: nroContratoInterm.
	 *
	 * @return nroContratoInterm
	 */
	public String getNroContratoInterm() {
		return nroContratoInterm;
	}

	/**
	 * Set: nroContratoInterm.
	 *
	 * @param nroContratoInterm the nro contrato interm
	 */
	public void setNroContratoInterm(String nroContratoInterm) {
		this.nroContratoInterm = nroContratoInterm;
	}

	/**
	 * Get: tipoContaDebitoInterm.
	 *
	 * @return tipoContaDebitoInterm
	 */
	public String getTipoContaDebitoInterm() {
		return tipoContaDebitoInterm;
	}

	/**
	 * Get: carteira.
	 *
	 * @return carteira
	 */
	public String getCarteira() {
		return carteira;
	}

	/**
	 * Set: carteira.
	 *
	 * @param carteira the carteira
	 */
	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}

	/**
	 * Get: cdIndentificadorJudicial.
	 *
	 * @return cdIndentificadorJudicial
	 */
	public String getCdIndentificadorJudicial() {
		return cdIndentificadorJudicial;
	}

	/**
	 * Set: cdIndentificadorJudicial.
	 *
	 * @param cdIndentificadorJudicial the cd indentificador judicial
	 */
	public void setCdIndentificadorJudicial(String cdIndentificadorJudicial) {
		this.cdIndentificadorJudicial = cdIndentificadorJudicial;
	}

	/**
	 * Get: cdSituacaoTranferenciaAutomatica.
	 *
	 * @return cdSituacaoTranferenciaAutomatica
	 */
	public String getCdSituacaoTranferenciaAutomatica() {
		return cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Set: cdSituacaoTranferenciaAutomatica.
	 *
	 * @param cdSituacaoTranferenciaAutomatica the cd situacao tranferencia automatica
	 */
	public void setCdSituacaoTranferenciaAutomatica(String cdSituacaoTranferenciaAutomatica) {
		this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Get: dtLimiteDescontoPagamento.
	 *
	 * @return dtLimiteDescontoPagamento
	 */
	public String getDtLimiteDescontoPagamento() {
		return dtLimiteDescontoPagamento;
	}

	/**
	 * Set: dtLimiteDescontoPagamento.
	 *
	 * @param dtLimiteDescontoPagamento the dt limite desconto pagamento
	 */
	public void setDtLimiteDescontoPagamento(String dtLimiteDescontoPagamento) {
		this.dtLimiteDescontoPagamento = dtLimiteDescontoPagamento;
	}

	/**
	 * Set: tipoContaDebitoInterm.
	 *
	 * @param tipoContaDebitoInterm the tipo conta debito interm
	 */
	public void setTipoContaDebitoInterm(String tipoContaDebitoInterm) {
		this.tipoContaDebitoInterm = tipoContaDebitoInterm;
	}

	/**
	 * Get: descTipoFavorecido.
	 *
	 * @return descTipoFavorecido
	 */
	public String getDescTipoFavorecido() {
		return descTipoFavorecido;
	}

	/**
	 * Set: descTipoFavorecido.
	 *
	 * @param descTipoFavorecido the desc tipo favorecido
	 */
	public void setDescTipoFavorecido(String descTipoFavorecido) {
		this.descTipoFavorecido = descTipoFavorecido;
	}

	/**
	 * Get: checkCont.
	 *
	 * @return checkCont
	 */
	public Integer getCheckCont() {
		return checkCont;
	}

	/**
	 * Set: checkCont.
	 *
	 * @param checkCont the check cont
	 */
	public void setCheckCont(Integer checkCont) {
		this.checkCont = checkCont;
	}

	/**
	 * Get: dsTipoBeneficiario.
	 *
	 * @return dsTipoBeneficiario
	 */
	public String getDsTipoBeneficiario() {
		return dsTipoBeneficiario;
	}

	/**
	 * Set: dsTipoBeneficiario.
	 *
	 * @param dsTipoBeneficiario the ds tipo beneficiario
	 */
	public void setDsTipoBeneficiario(String dsTipoBeneficiario) {
		this.dsTipoBeneficiario = dsTipoBeneficiario;
	}

	/**
	 * Get: cdBancoOriginal.
	 *
	 * @return cdBancoOriginal
	 */
	public Integer getCdBancoOriginal() {
		return cdBancoOriginal;
	}

	/**
	 * Set: cdBancoOriginal.
	 *
	 * @param cdBancoOriginal the cd banco original
	 */
	public void setCdBancoOriginal(Integer cdBancoOriginal) {
		this.cdBancoOriginal = cdBancoOriginal;
	}

	/**
	 * Get: dsBancoOriginal.
	 *
	 * @return dsBancoOriginal
	 */
	public String getDsBancoOriginal() {
		return dsBancoOriginal;
	}

	/**
	 * Set: dsBancoOriginal.
	 *
	 * @param dsBancoOriginal the ds banco original
	 */
	public void setDsBancoOriginal(String dsBancoOriginal) {
		this.dsBancoOriginal = dsBancoOriginal;
	}

	/**
	 * Get: cdAgenciaBancariaOriginal.
	 *
	 * @return cdAgenciaBancariaOriginal
	 */
	public Integer getCdAgenciaBancariaOriginal() {
		return cdAgenciaBancariaOriginal;
	}

	/**
	 * Set: cdAgenciaBancariaOriginal.
	 *
	 * @param cdAgenciaBancariaOriginal the cd agencia bancaria original
	 */
	public void setCdAgenciaBancariaOriginal(Integer cdAgenciaBancariaOriginal) {
		this.cdAgenciaBancariaOriginal = cdAgenciaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoAgenciaOriginal.
	 *
	 * @return cdDigitoAgenciaOriginal
	 */
	public String getCdDigitoAgenciaOriginal() {
		return cdDigitoAgenciaOriginal;
	}

	/**
	 * Set: cdDigitoAgenciaOriginal.
	 *
	 * @param cdDigitoAgenciaOriginal the cd digito agencia original
	 */
	public void setCdDigitoAgenciaOriginal(String cdDigitoAgenciaOriginal) {
		this.cdDigitoAgenciaOriginal = cdDigitoAgenciaOriginal;
	}

	/**
	 * Get: dsAgenciaOriginal.
	 *
	 * @return dsAgenciaOriginal
	 */
	public String getDsAgenciaOriginal() {
		return dsAgenciaOriginal;
	}

	/**
	 * Set: dsAgenciaOriginal.
	 *
	 * @param dsAgenciaOriginal the ds agencia original
	 */
	public void setDsAgenciaOriginal(String dsAgenciaOriginal) {
		this.dsAgenciaOriginal = dsAgenciaOriginal;
	}

	/**
	 * Get: cdContaBancariaOriginal.
	 *
	 * @return cdContaBancariaOriginal
	 */
	public Long getCdContaBancariaOriginal() {
		return cdContaBancariaOriginal;
	}

	/**
	 * Set: cdContaBancariaOriginal.
	 *
	 * @param cdContaBancariaOriginal the cd conta bancaria original
	 */
	public void setCdContaBancariaOriginal(Long cdContaBancariaOriginal) {
		this.cdContaBancariaOriginal = cdContaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoContaOriginal.
	 *
	 * @return cdDigitoContaOriginal
	 */
	public String getCdDigitoContaOriginal() {
		return cdDigitoContaOriginal;
	}

	/**
	 * Set: cdDigitoContaOriginal.
	 *
	 * @param cdDigitoContaOriginal the cd digito conta original
	 */
	public void setCdDigitoContaOriginal(String cdDigitoContaOriginal) {
		this.cdDigitoContaOriginal = cdDigitoContaOriginal;
	}

	/**
	 * Get: dsTipoContaOriginal.
	 *
	 * @return dsTipoContaOriginal
	 */
	public String getDsTipoContaOriginal() {
		return dsTipoContaOriginal;
	}

	/**
	 * Set: dsTipoContaOriginal.
	 *
	 * @param dsTipoContaOriginal the ds tipo conta original
	 */
	public void setDsTipoContaOriginal(String dsTipoContaOriginal) {
		this.dsTipoContaOriginal = dsTipoContaOriginal;
	}

	/**
	 * Get: dtDevolucaoEstorno.
	 *
	 * @return dtDevolucaoEstorno
	 */
	public String getDtDevolucaoEstorno() {
		return dtDevolucaoEstorno;
	}

	/**
	 * Set: dtDevolucaoEstorno.
	 *
	 * @param dtDevolucaoEstorno the dt devolucao estorno
	 */
	public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
		this.dtDevolucaoEstorno = dtDevolucaoEstorno;
	}

	/**
	 * Get: dtLimitePagamento.
	 *
	 * @return dtLimitePagamento
	 */
	public String getDtLimitePagamento() {
		return dtLimitePagamento;
	}

	/**
	 * Set: dtLimitePagamento.
	 *
	 * @param dtLimitePagamento the dt limite pagamento
	 */
	public void setDtLimitePagamento(String dtLimitePagamento) {
		this.dtLimitePagamento = dtLimitePagamento;
	}

	/**
	 * Get: dsRastreado.
	 *
	 * @return dsRastreado
	 */
	public String getDsRastreado() {
		return dsRastreado;
	}

	/**
	 * Set: dsRastreado.
	 *
	 * @param dsRastreado the ds rastreado
	 */
	public void setDsRastreado(String dsRastreado) {
		this.dsRastreado = dsRastreado;
	}

	/**
	 * Get: dtFloatingPagamento.
	 *
	 * @return dtFloatingPagamento
	 */
	public String getDtFloatingPagamento() {
		return dtFloatingPagamento;
	}

	/**
	 * Set: dtFloatingPagamento.
	 *
	 * @param dtFloatingPagamento the dt floating pagamento
	 */
	public void setDtFloatingPagamento(String dtFloatingPagamento) {
		this.dtFloatingPagamento = dtFloatingPagamento;
	}

	/**
	 * Get: dtEfetivFloatPgto.
	 *
	 * @return dtEfetivFloatPgto
	 */
	public String getDtEfetivFloatPgto() {
		return dtEfetivFloatPgto;
	}

	/**
	 * Set: dtEfetivFloatPgto.
	 *
	 * @param dtEfetivFloatPgto the dt efetiv float pgto
	 */
	public void setDtEfetivFloatPgto(String dtEfetivFloatPgto) {
		this.dtEfetivFloatPgto = dtEfetivFloatPgto;
	}

	/**
	 * Get: vlFloatingPagamento.
	 *
	 * @return vlFloatingPagamento
	 */
	public BigDecimal getVlFloatingPagamento() {
		return vlFloatingPagamento;
	}

	/**
	 * Set: vlFloatingPagamento.
	 *
	 * @param vlFloatingPagamento the vl floating pagamento
	 */
	public void setVlFloatingPagamento(BigDecimal vlFloatingPagamento) {
		this.vlFloatingPagamento = vlFloatingPagamento;
	}

	/**
	 * Get: cdOperacaoDcom.
	 *
	 * @return cdOperacaoDcom
	 */
	public Long getCdOperacaoDcom() {
		return cdOperacaoDcom;
	}

	/**
	 * Set: cdOperacaoDcom.
	 *
	 * @param cdOperacaoDcom the cd operacao dcom
	 */
	public void setCdOperacaoDcom(Long cdOperacaoDcom) {
		this.cdOperacaoDcom = cdOperacaoDcom;
	}

	/**
	 * Get: dsSituacaoDcom.
	 *
	 * @return dsSituacaoDcom
	 */
	public String getDsSituacaoDcom() {
		return dsSituacaoDcom;
	}

	/**
	 * Set: dsSituacaoDcom.
	 *
	 * @param dsSituacaoDcom the ds situacao dcom
	 */
	public void setDsSituacaoDcom(String dsSituacaoDcom) {
		this.dsSituacaoDcom = dsSituacaoDcom;
	}

	/**
	 * Get: exibeDcom.
	 *
	 * @return exibeDcom
	 */
	public Boolean getExibeDcom() {
		return exibeDcom;
	}

	/**
	 * Set: exibeDcom.
	 *
	 * @param exibeDcom the exibe dcom
	 */
	public void setExibeDcom(Boolean exibeDcom) {
		this.exibeDcom = exibeDcom;
	}

	/**
	 * Get: numControleInternoLote.
	 *
	 * @return numControleInternoLote
	 */
	public Long getNumControleInternoLote() {
		return numControleInternoLote;
	}

	/**
	 * Set: numControleInternoLote.
	 *
	 * @param numControleInternoLote the num controle interno lote
	 */
	public void setNumControleInternoLote(Long numControleInternoLote) {
		this.numControleInternoLote = numControleInternoLote;
	}
}