/**
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaofavorecido.mantersolicrastfavorecido
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.view.bean.manutencaofavorecido.mantersolicrastfavorecido;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.DetalharSolicitacaoRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.DetalharSolicitacaoRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ExcluirSolicRastFavEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ExcluirSolicRastFavSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.IncluirSolicRastFavEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.IncluirSolicRastFavSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ManterSolicRastFavEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.ManterSolicRastFavSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterSolicRastFavBean
 * <p>
 * Prop�sito: Implementa��o do Bean ManterSolicRastFavBean
 * </p>
 * 
 * @author todo! - Solu��es em Tecnologia / TI Melhorias - Arquitetura
 * @version 1.0
 * @see IdentificacaoClienteBean
 */
public class ManterSolicRastFavBean extends IdentificacaoClienteBean {

	// Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ. */
	private static final String CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ = "conManterSolicitacaoRastreamentoFavorecidosPesq";

	/** Atributo TIPO_FAVORECIDO_CPFCNPJ. */
	private static final Integer TIPO_FAVORECIDO_CPFCNPJ = 2;

	/** Atributo solEmissaoComprovantePagtoClientePagServiceImpl. */
	private ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl;

	/** Atributo atributoSoltc. */
	private VerificaraAtributoSoltcSaidaDTO atributoSoltc;

	/** Atributo entradaManterSolicRastFavorecido. */
	private ManterSolicRastFavEntradaDTO entradaManterSolicRastFavorecido;

	/** Atributo saidaListaManterSolicRastFavorecio. */
	private ManterSolicRastFavSaidaDTO saidaListaManterSolicRastFavorecio;

	/** Atributo listManterSolicRastFav. */
	private List<ManterSolicRastFavSaidaDTO> listManterSolicRastFav;

	/** Atributo entradaIncluirSolicRastFav. */
	private IncluirSolicRastFavEntradaDTO entradaIncluirSolicRastFav;

	/** Atributo retornoIncluirSolicRastFav. */
	private IncluirSolicRastFavSaidaDTO retornoIncluirSolicRastFav;

	/** Atributo manterSolicitacaoRastFavService. */
	private IManterSolicitacaoRastFavService manterSolicitacaoRastFavService;

	/** Atributo entradaExcluirSolicRastFav. */
	private ExcluirSolicRastFavEntradaDTO entradaExcluirSolicRastFav;

	/** Atributo retornoExcluirSolicRastFav. */
	private ExcluirSolicRastFavSaidaDTO retornoExcluirSolicRastFav;

	/** Atributo entradaDetalharSolicRastFav. */
	private DetalharSolicitacaoRastreamentoEntradaDTO entradaDetalharSolicRastFav;

	/** Atributo retornoDetalharSolicRastFav. */
	private DetalharSolicitacaoRastreamentoSaidaDTO retornoDetalharSolicRastFav;

	/** Atributo listaSolicitacao. */
	private List<SelectItem> listaSolicitacao = new ArrayList<SelectItem>();

	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO>();

	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();

	/** Atributo listaModalidadeServicoIncluir. */
	private List<SelectItem> listaModalidadeServicoIncluir = new ArrayList<SelectItem>();

	/** Atributo tipoServicoDesc. */
	private String tipoServicoDesc;

	/** Atributo tipoModalidadeDesc. */
	private String tipoModalidadeDesc;

	/** Atributo tipoServico. */
	private Integer tipoServico;

	/** Atributo modalidadeServico. */
	private Integer modalidadeServico;

	/** Atributo itemSelecionadoListaSolicitacao. */
	private String itemSelecionadoListaSolicitacao;

	/** Atributo filtroTipoServico. */
	private Integer filtroTipoServico;

	/** Atributo filtroModalidadeServico. */
	private Integer filtroModalidadeServico;

	/** Atributo itemCheck. */
	private Boolean itemCheck;

	/** Atributo itemChkDesconto. */
	private Boolean itemChkDesconto = false;

	/** Atributo itemArgumentoPesquisa. */
	private String itemArgumentoPesquisa;

	/** Atributo exibirBotaoPaginacao. */
	private boolean exibirBotaoPaginacao;

	/** Atributo itemBloqSelecionado. */
	private String itemBloqSelecionado;

	/** Atributo motivoBloqueio. */
	private String motivoBloqueio;

	/** Atributo boxCliente. */
	private boolean boxCliente;

	/** Atributo boxDepartamento. */
	private boolean boxDepartamento;

	/** Atributo inclusaoAuto. */
	private Integer inclusaoAuto;

	/** Atributo incluisaoFavorecido. */
	private Integer incluisaoFavorecido;

	/** Atributo destinoAquivoRadio. */
	private Integer destinoAquivoRadio;

	/** Atributo dataInicioRastreamento. */
	private Date dataInicioRastreamento;

	/** Atributo dataFimRastreamento. */
	private Date dataFimRastreamento;

	/** Atributo dsDestinoRetorno. */
	private String dsDestinoRetorno;

	/** Atributo dsInclusaoAuto. */
	private String dsInclusaoAuto;

	/** Atributo dsInclusaoFavorecido. */
	private String dsInclusaoFavorecido;

	/** Atributo vlTarifaBonificacao. */
	private BigDecimal vlTarifaBonificacao;

	/** Atributo checkDesconto. */
	private boolean checkDesconto = false;

	/** Atributo renderizaPorcent. */
	public boolean renderizaPorcent;

	/** Atributo numPercentualDescTarifa. */
	private BigDecimal numPercentualDescTarifa;
	
	/** Atributo indicadorDescontoBloqueio. */
	private boolean indicadorDescontoBloqueio;
	
	/**
	 * Get: incluisaoFavorecido.
	 *
	 * @return incluisaoFavorecido
	 */
	public Integer getIncluisaoFavorecido() {
		return incluisaoFavorecido;
	}

	/**
	 * Set: incluisaoFavorecido.
	 *
	 * @param incluisaoFavorecido the incluisao favorecido
	 */
	public void setIncluisaoFavorecido(Integer incluisaoFavorecido) {
		this.incluisaoFavorecido = incluisaoFavorecido;
	}

	/**
	 * Get: inclusaoAuto.
	 *
	 * @return inclusaoAuto
	 */
	public Integer getInclusaoAuto() {
		return inclusaoAuto;
	}

	/**
	 * Is check desconto.
	 *
	 * @return true, if is check desconto
	 */
	public boolean isCheckDesconto() {
		return checkDesconto;
	}

	/**
	 * Voltar pesquisa.
	 *
	 * @return the string
	 */
	public String voltarPesquisa() {
		setItemSelecionadoListaSolicitacao(null);
		setDestinoAquivoRadio(null);
		return "conManterSolicitacaoRastreamentoFavorecidosPesq";
	}

	/**
	 * Set: checkDesconto.
	 *
	 * @param checkDesconto the check desconto
	 */
	public void setCheckDesconto(boolean checkDesconto) {
		this.checkDesconto = checkDesconto;
	}

	/**
	 * Manter solic rast fav bean.
	 */
	public ManterSolicRastFavBean() {
		super(CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ,
				"identificaoRastreamentoCliente",
				"identificaoRastreamentoContrato");
	}

	/**
	 * Inicializar campos solic rast fav.
	 *
	 * @return the string
	 */
	public String inicializarCamposSolicRastFav() {
		this.inicializarVariaveis();
		return CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ;

	}

	/**
	 * Voltar favorecidos.
	 *
	 * @return the string
	 */
	public String voltarFavorecidos() {
		return CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ;
	}

	/**
	 * Inicializar variaveis.
	 */
	private void inicializarVariaveis() {

		this.setItemArgumentoPesquisa("");
		this.setItemBloqSelecionado("");
		this.setRetornoIncluirSolicRastFav(new IncluirSolicRastFavSaidaDTO());
		this
				.setEntradaManterSolicRastFavorecido(new ManterSolicRastFavEntradaDTO());
		this.setEntradaIncluirSolicRastFav(new IncluirSolicRastFavEntradaDTO());
		this.setEntradaExcluirSolicRastFav(new ExcluirSolicRastFavEntradaDTO());
		this.setItemSelecionadoListaSolicitacao("");
		this.getEntradaManterSolicRastFavorecido().setDtInicioRastreabilidade(
				new Date());
		this.getEntradaManterSolicRastFavorecido().setDtFimRastreabilidade(
				new Date());
		this.setDsEmpresaGestoraContrato(null);

		inicializarVariaveisIdentificacao();
		this.setItemFiltroSelecionado("");
		getEntradaConsultarListaContratosPessoas().setCdPessoaJuridicaContrato(
				2269651L);

		listaSolicitacao = getPreencherComboSituacao();
	}

	/**
	 * Get: preencherComboSituacao.
	 *
	 * @return preencherComboSituacao
	 */
	public List<SelectItem> getPreencherComboSituacao() {
		List<SelectItem> listaSolicitacaoAux = new ArrayList<SelectItem>();

		try {
			List<ListarSituacaoSolicitacaoSaidaDTO> list = getComboService()
					.listarSituacaoSolicitacao();

			for (ListarSituacaoSolicitacaoSaidaDTO combo : list) {
				listaSolicitacaoAux.add(new SelectItem(combo
						.getCdSolicitacaoPagamentoIntegrado(), combo
						.getDsTipoSolicitacaoPagamento()));
			}
		} catch (PdcAdapterFunctionalException e) {
			listaSolicitacaoAux = new ArrayList<SelectItem>();
		}

		return listaSolicitacaoAux;

	}

	/**
	 * Voltar pagina.
	 *
	 * @return the string
	 */
	public String voltarPagina() {
		return "";
	}

	/*
	 * CONSULTA
	 */

	/**
	 * Get: preencherBloqueoDesbloqueo.
	 *
	 * @return preencherBloqueoDesbloqueo
	 */
	public List<SelectItem> getPreencherBloqueoDesbloqueo() {
		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(Integer.parseInt("1"), "Sim"));
		list.add(new SelectItem(Integer.parseInt("2"), "N�o"));
		return list;
	}

	/**
	 * Get: preencherFavorecido.
	 *
	 * @return preencherFavorecido
	 */
	public List<SelectItem> getPreencherFavorecido() {
		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(Integer.parseInt("1"), "Sequencial"));
		list.add(new SelectItem(Integer.parseInt("2"), "CPF/CNPJ"));
		return list;
	}

	/**
	 * Get: preencherDestino.
	 *
	 * @return preencherDestino
	 */
	public List<SelectItem> getPreencherDestino() {
		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(Integer.parseInt("1"), "Cliente"));
		list.add(new SelectItem(Integer.parseInt("2"), "Departamento Gestor"));
		return list;
	}

	/**
	 * Get: preencherTarifa.
	 *
	 * @return preencherTarifa
	 */
	public List<SelectItem> getPreencherTarifa() {
		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(Integer.parseInt("1"), "Desconto Tarifa"));
		return list;
	}

	/**
	 * Limpar pagina.
	 *
	 * @return the string
	 */
	public String limparPagina() {
		this
				.setListManterSolicRastFav(new ArrayList<ManterSolicRastFavSaidaDTO>());
		this.setItemSelecionadoListaSolicitacao("");
		this.entradaManterSolicRastFavorecido
				.setCdSituacaoSolicitacaoPagamento(entradaManterSolicRastFavorecido
						.getCdSituacaoSolicitacaoPagamento());
		this.getEntradaManterSolicRastFavorecido().setDtInicioRastreabilidade(
				getEntradaManterSolicRastFavorecido()
						.getDtInicioRastreabilidade());
		this.getEntradaManterSolicRastFavorecido()
				.setDtFimRastreabilidade(
						getEntradaManterSolicRastFavorecido()
								.getDtFimRastreabilidade());
		habilitaConsultaArgumentosPesquisa();

		return CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ;
	}

	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos() {
		this
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this
				.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this
				.setListManterSolicRastFav(new ArrayList<ManterSolicRastFavSaidaDTO>());
		this.setDesabilataFiltro(false);
		this.setItemArgumentoPesquisa("");
		this.setDesabilatalimpar(true);
		this.setLimpaCamp(true);
		this.inicializarVariaveis();

		return CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ;
	}

	/**
	 * Consultar solicitacao.
	 *
	 * @param event the event
	 */
	public void consultarSolicitacao(ActionEvent event) {
		consultarSolicitacao();
	}

	/**
	 * Consultar solicitacao.
	 *
	 * @return the string
	 */
	public String consultarSolicitacao() {
		try {
			this.buscarSolicitacao();
			this
					.setHabilitaFiltroDeContrato(this
							.getListManterSolicRastFav() != null
							&& this.getListManterSolicRastFav().size() > 10 ? true
							: false);
			this.setBloqueaTipoContrato(true);

			desabilitaConsultaArgumentosPesquisa();

			return CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ;
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					"/conManterSolicitacaoRastreamentoFavorecidosPesq.jsf",
					BradescoViewExceptionActionType.PATH, false);
			this
					.setListManterSolicRastFav(new ArrayList<ManterSolicRastFavSaidaDTO>());
			return null;

		}
	}

	/**
	 * Submit.
	 *
	 * @param event the event
	 */
	public void submit(ActionEvent event) {
		this.buscarSolicitacao();
	}

	/**
	 * Buscar solicitacao.
	 */
	private void buscarSolicitacao() {
		this.getEntradaManterSolicRastFavorecido().setCdPessoaJuridicaContrato(
				this.getSaidaConsultarListaContratosPessoas()
						.getCdPessoaJuridicaContrato());
		this.getEntradaManterSolicRastFavorecido().setCdTipoContratoNegocio(
				this.getSaidaConsultarListaContratosPessoas()
						.getCdTipoContratoNegocio());
		this.getEntradaManterSolicRastFavorecido()
				.setNrSequenciaContratoNegocio(
						this.getSaidaConsultarListaContratosPessoas()
								.getNrSeqContratoNegocio());

		this
				.setListManterSolicRastFav(this.manterSolicitacaoRastFavService
						.pesquisarSolicRastFavorecidos(this.entradaManterSolicRastFavorecido));
	}

	/**
	 * Is list manter solic rast fav empty.
	 *
	 * @return true, if is list manter solic rast fav empty
	 */
	public boolean isListManterSolicRastFavEmpty() {
		return this.listManterSolicRastFav == null
				|| this.listManterSolicRastFav.isEmpty();
	}

	/**
	 * Get: selectItemConSolicRastFav.
	 *
	 * @return selectItemConSolicRastFav
	 */
	public List<SelectItem> getSelectItemConSolicRastFav() {

		if (this.listManterSolicRastFav == null
				|| this.listManterSolicRastFav.isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.listManterSolicRastFav.size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/*
	 * DETALHE
	 */

	/**
	 * Detalhar solic rast fav.
	 *
	 * @return the string
	 */
	public String detalharSolicRastFav() {

		this
				.setRetornoDetalharSolicRastFav(new DetalharSolicitacaoRastreamentoSaidaDTO());
		DetalharSolicitacaoRastreamentoEntradaDTO entrada = new DetalharSolicitacaoRastreamentoEntradaDTO();

		saidaListaManterSolicRastFavorecio = listManterSolicRastFav.get(Integer
				.parseInt(itemSelecionadoListaSolicitacao));

		entrada.setCdSolicitacaoPagamento(saidaListaManterSolicRastFavorecio
				.getCdSolicitacaoPagamento());
		entrada.setNrSolicitacaoPagamento(saidaListaManterSolicRastFavorecio
				.getNrSolicitacaoPagamento());

		try {
			retornoDetalharSolicRastFav = manterSolicitacaoRastFavService
					.detalharSolicRastFav(entrada);

			if (retornoDetalharSolicRastFav.getCdAgenciaDeb() == null) {
				retornoDetalharSolicRastFav.setCdAgenciaDeb(Integer
						.parseInt(""));
			}

			if (retornoDetalharSolicRastFav.getNrTarifaBonificacao() == null
					|| retornoDetalharSolicRastFav.getNrTarifaBonificacao()
							.compareTo(BigDecimal.ZERO) == 0) {
				retornoDetalharSolicRastFav.setNrTarifaBonificacao(null);
				renderizaPorcent = false;
			} else {
				renderizaPorcent = true;
			}

			this.getRetornoDetalharSolicRastFav().setVlrTarifaAtual(
					retornoDetalharSolicRastFav.getVlrTarifaAtual());
			this.getRetornoDetalharSolicRastFav().setVlTarifaPadrao(
					retornoDetalharSolicRastFav.getVlTarifaPadrao());
			this.getRetornoDetalharSolicRastFav().setNumPercentualDescTarifa(
					retornoDetalharSolicRastFav.getNumPercentualDescTarifa());

			this.getRetornoDetalharSolicRastFav()
					.setDtInicioRastreabilidadeFavorecido(
							retornoDetalharSolicRastFav
									.getDtInicioRastreabilidadeFavorecido()
									.replace(".", "/"));
			this.getRetornoDetalharSolicRastFav()
					.setDtFimRastreabilidadeFavorecido(
							retornoDetalharSolicRastFav
									.getDtFimRastreabilidadeFavorecido()
									.replace(".", "/"));

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ,
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "detManterSolicitacaoRastreamentoFavorecidos";
	}

	/*
	 * INCLUS�O
	 */

	/**
	 * Limpar incluir.
	 */
	public void limparIncluir() {
		entradaIncluirSolicRastFav = new IncluirSolicRastFavEntradaDTO();

		setFiltroTipoServico(0);
		setFiltroModalidadeServico(0);
		setDataInicioRastreamento(null);
		setDataFimRastreamento(null);
		setCheckDesconto(false);
		setItemChkDesconto(false);
		setBoxCliente(false);
		setBoxDepartamento(false);
		setInclusaoAuto(2);
		setIncluisaoFavorecido(null);
	}

	/**
	 * Inicia favorecido.
	 */
	public void iniciaFavorecido() {
		if (getInclusaoAuto() == 1) {
			setIncluisaoFavorecido(2);
		} else {
			setIncluisaoFavorecido(null);
		}
	}

	/**
	 * Incluir solic rast fav.
	 *
	 * @return the string
	 */
	public String incluirSolicRastFav() {
		limparIncluir();
		limpaTarifa();
		entradaIncluirSolicRastFav.setCdBanco(237);
		entradaIncluirSolicRastFav.setVlTarifaPadrao(BigDecimal.ZERO);
		entradaIncluirSolicRastFav.setNumPercentualDescTarifa(BigDecimal.ZERO);
		entradaIncluirSolicRastFav.setVlrTarifaAtual(BigDecimal.ZERO);
		setDataInicioRastreamento(new Date());
		setDataFimRastreamento(new Date());

		try {
			atributoSoltc = getManterSolicitacaoRastFavService()
					.verificaraAtributoSoltc(
							new VerificaraAtributoSoltcEntradaDTO(1));
			this.incluirSolicitacao();
			this
					.setHabilitaFiltroDeContrato(this
							.getListManterSolicRastFav() != null
							&& this.getListManterSolicRastFav().size() > 10 ? true
							: false);

			consultarAgenciaOpeTarifaPadrao();

			return "incManterSolicitacaoRastreamentoFavorecidos";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					"/conManterSolicitacaoRastreamentoFavorecidosPesq.jsf",
					BradescoViewExceptionActionType.PATH, false);
			this
					.setListManterSolicRastFav(new ArrayList<ManterSolicRastFavSaidaDTO>());

			return null;
		}
	}

	/**
	 * Calcular tarifa atualizada.
	 */
	public void calcularTarifaAtualizada() {
		entradaIncluirSolicRastFav.setVlrTarifaAtual(entradaIncluirSolicRastFav
				.getVlTarifaPadrao().subtract(
						entradaIncluirSolicRastFav.getVlTarifaPadrao()
								.multiply(
										entradaIncluirSolicRastFav
												.getNumPercentualDescTarifa()
												.divide(new BigDecimal(100)))
								.setScale(2, BigDecimal.ROUND_HALF_UP)));
	}

	/**
	 * Incluir solicitacao.
	 */
	private void incluirSolicitacao() {
		this.getEntradaManterSolicRastFavorecido().setCdPessoaJuridicaContrato(
				this.getSaidaConsultarListaContratosPessoas()
						.getCdPessoaJuridicaContrato());
		this.getEntradaManterSolicRastFavorecido().setCdTipoContratoNegocio(
				this.getSaidaConsultarListaContratosPessoas()
						.getCdTipoContratoNegocio());
		this.getEntradaManterSolicRastFavorecido()
				.setNrSequenciaContratoNegocio(
						this.getSaidaConsultarListaContratosPessoas()
								.getNrSeqContratoNegocio());

		preencheListaTipoServico();
	}

	/**
	 * Preenche lista tipo servico.
	 */
	public void preencheListaTipoServico() {
		try {
			this.listaTipoServico = new ArrayList<SelectItem>();

			List<ListarServicosSaidaDTO> listaServico = new ArrayList<ListarServicosSaidaDTO>();

			listaServico = super.getComboService().listarTipoServicos(1);

			listaTipoServicoHash.clear();
			for (ListarServicosSaidaDTO combo : listaServico) {
				listaTipoServicoHash.put(combo.getCdServico(), combo
						.getDsServico());
				listaTipoServico.add(new SelectItem(combo.getCdServico(), combo
						.getDsServico()));
			}
		} catch (PdcAdapterFunctionalException e) {
			listaTipoServico = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Preenche lista modalidade.
	 */
	public void preencheListaModalidade() {

		if (getFiltroTipoServico() != null && getFiltroTipoServico() != 0) {
			try {

				this.listaModalidadeServico = new ArrayList<SelectItem>();

				List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
				ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();

				listarModalidadeEntradaDTO.setCdServico(getFiltroTipoServico());

				listaModalidades = super.getComboService().listarModalidades(
						listarModalidadeEntradaDTO);

				listaModalidadeServicoHash.clear();

				for (ListarModalidadeSaidaDTO combo : listaModalidades) {
					this.listaModalidadeServico.add(new SelectItem(combo
							.getCdModalidade(), combo.getDsModalidade()));
					listaModalidadeServicoHash.put(combo.getCdModalidade(),
							combo);
				}

			} catch (PdcAdapterException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				this.setListaModalidadeServico(new ArrayList<SelectItem>());
			}
		} else {
			listaModalidadeServico.clear();
		}
	}

	/**
	 * Avancar detalhar incluir.
	 *
	 * @return the string
	 */
	public String avancarDetalharIncluir() {

		ConsultarBancoAgenciaEntradaDTO entradaConsultarBancoAgencia = new ConsultarBancoAgenciaEntradaDTO();

		entradaConsultarBancoAgencia.setCdAgencia(entradaIncluirSolicRastFav
				.getCdAgencia() == null ? 0 : entradaIncluirSolicRastFav
				.getCdAgencia());
		entradaConsultarBancoAgencia.setCdBanco(entradaIncluirSolicRastFav
				.getCdBanco() == null ? 0 : entradaIncluirSolicRastFav
				.getCdBanco());
		entradaConsultarBancoAgencia
				.setCdDigitoAgencia(entradaIncluirSolicRastFav
						.getCdDigitoAgenciaAtualizada() == null ? 0
						: entradaIncluirSolicRastFav
								.getCdDigitoAgenciaAtualizada());

		if (entradaConsultarBancoAgencia.getCdAgencia() > 0
				&& entradaConsultarBancoAgencia.getCdBanco() > 0) {
			try {
				ConsultarBancoAgenciaSaidaDTO saidaConsultarBancoAgencia = manterSolicitacaoRastFavService
						.consultarDescricaoBancoAgencia(entradaConsultarBancoAgencia);
				entradaIncluirSolicRastFav
						.setDsBancoAtualizado(saidaConsultarBancoAgencia
								.getDsBanco());
				entradaIncluirSolicRastFav
						.setDsAgenciaAtualizada(saidaConsultarBancoAgencia
								.getDsAgencia());
			} catch (PdcAdapterFunctionalException e) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(e.getCode(), 8) + ") "
						+ e.getMessage(),
						"/incManterSolicitacaoRastreamentoFavorecidos.jsf",
						BradescoViewExceptionActionType.PATH, false);
				return null;
			}
		}

		return "incConfManterSolicitacaoRastreamentoFavorecidos";
	}

	/**
	 * Avancar inclusao solic rast fav.
	 *
	 * @return the string
	 */
	public String avancarInclusaoSolicRastFav() {
		// Calcula o valor da Tarifa Atualizada
		calcularTarifaAtualizada();

		if (listaTipoServicoHash.get(filtroTipoServico) == null) {
			return null;
		}
		setTipoModalidadeDesc(filtroModalidadeServico == 0 ? ""
				: listaModalidadeServicoHash.get(filtroModalidadeServico)
						.getDsModalidade());
		setTipoServicoDesc(listaTipoServicoHash.get(filtroTipoServico)
				.toString() == null ? "" : listaTipoServicoHash.get(
				filtroTipoServico).toString());
		if (filtroModalidadeServico > 0) {
			this.getEntradaIncluirSolicRastFav().setCdModalidade(
					listaModalidadeServicoHash.get(filtroModalidadeServico)
							.getCdModalidade());
		}

		if (this.inclusaoAuto == 1) {
			this.getEntradaIncluirSolicRastFav()
					.setCdIndicadorInclusaoFavorecido(1);
			this.setDsInclusaoAuto("Sim");
		} else {
			this.getEntradaIncluirSolicRastFav()
					.setCdIndicadorInclusaoFavorecido(2);
			this.setDsInclusaoAuto("N�o");
		}

		if (this.incluisaoFavorecido != null) {
			if (this.incluisaoFavorecido == 1) {
				this.getEntradaIncluirSolicRastFav().setCdFormaFavorecido(1);
				this.setDsInclusaoFavorecido("Sequencial");
			} else if (TIPO_FAVORECIDO_CPFCNPJ.equals(incluisaoFavorecido)) {
				this.getEntradaIncluirSolicRastFav().setCdFormaFavorecido(2);
				this.setDsInclusaoFavorecido("CPF/CNPJ");
			} else {
				this.setIncluisaoFavorecido(0);
				this.setDsInclusaoFavorecido("");
			}
		} else {
			this.setIncluisaoFavorecido(0);
			this.setDsInclusaoFavorecido("");
		}

		if (this.destinoAquivoRadio != null) {
			if (this.destinoAquivoRadio == 1) {
				this.getEntradaIncluirSolicRastFav()
						.setCdIndicadorDestinoRetorno(1);
				this.setDsDestinoRetorno("Cliente");
			} else if (this.destinoAquivoRadio == 2) {
				this.getEntradaIncluirSolicRastFav()
						.setCdIndicadorDestinoRetorno(2);
				this.setDsDestinoRetorno("Departamento Gestor");
			} else {
				this.getEntradaIncluirSolicRastFav()
						.setCdIndicadorDestinoRetorno(0);
			}
		}

		this.getEntradaIncluirSolicRastFav().setCdPessoaJuridicaContrato(
				this.getSaidaConsultarListaContratosPessoas()
						.getCdPessoaJuridicaContrato());
		this.getEntradaIncluirSolicRastFav().setCdTipoContratoNegocio(
				this.getSaidaConsultarListaContratosPessoas()
						.getCdTipoContratoNegocio());
		this.getEntradaIncluirSolicRastFav().setNrSequenciaContratoNegocio(
				this.getSaidaConsultarListaContratosPessoas()
						.getNrSeqContratoNegocio());
		this.getEntradaIncluirSolicRastFav().setDtInicioRastreamento(
				FormatarData.formataDiaMesAnoToPdc(dataInicioRastreamento));
		this.getEntradaIncluirSolicRastFav().setDtFimRastreamento(
				FormatarData.formataDiaMesAnoToPdc(dataFimRastreamento));
		this.getEntradaIncluirSolicRastFav().setCdServico(
				this.getFiltroTipoServico());

		if (this.getEntradaIncluirSolicRastFav().getCdBanco() != null
				&& this.getEntradaIncluirSolicRastFav().getCdAgencia() == null) {
			this.getEntradaIncluirSolicRastFav().setCdBanco(null);
		}

		this.getRetornoIncluirSolicRastFav().setDtInicioRastreamento(
				DateUtils.formartarData(dataInicioRastreamento, "dd/MM/yyyy"));
		this.getRetornoIncluirSolicRastFav().setDtFimRastreamento(
				DateUtils.formartarData(dataFimRastreamento, "dd/MM/yyyy"));
		this.getRetornoIncluirSolicRastFav().setCdServico(
				this.getEntradaIncluirSolicRastFav().getCdServico());
		this.getRetornoIncluirSolicRastFav().setCdModalidade(
				this.getEntradaIncluirSolicRastFav().getCdModalidade());
		this.getRetornoIncluirSolicRastFav().setCdAgencia(
				this.getEntradaIncluirSolicRastFav().getCdAgencia());
		this.getRetornoIncluirSolicRastFav().setCdBanco(
				this.getEntradaIncluirSolicRastFav().getCdBanco());
		this.getRetornoIncluirSolicRastFav().setCdConta(
				this.getEntradaIncluirSolicRastFav().getCdConta());
		this.getRetornoIncluirSolicRastFav().setDgConta(
				this.getEntradaIncluirSolicRastFav().getDgConta());
		this.getRetornoIncluirSolicRastFav().setVlTarifaBonificacao(
				this.getEntradaIncluirSolicRastFav()
						.getNumPercentualDescTarifa());
		this.getRetornoIncluirSolicRastFav().setCdIndicadorInclusaoFavorecido(
				this.getEntradaIncluirSolicRastFav()
						.getCdIndicadorInclusaoFavorecido());
		this.getRetornoIncluirSolicRastFav().setCdIndicadorDestinoRetorno(
				this.getEntradaIncluirSolicRastFav()
						.getCdIndicadorDestinoRetorno());

		return avancarDetalharIncluir();

	}

	/**
	 * Limpa tarifa.
	 */
	public void limpaTarifa() {
		setVlTarifaBonificacao(null);
	}

	/**
	 * Confirmar inclusao solic rast fav.
	 *
	 * @return the string
	 */
	public String confirmarInclusaoSolicRastFav() {

		try {
			IncluirSolicRastFavSaidaDTO saida = manterSolicitacaoRastFavService
					.incluirSolicRastFav(entradaIncluirSolicRastFav);
			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(),
					CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ,
					"#{manterSolicRastFavBean.consultarSolicitacao}", false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					"incManterSolicitacaoRastreamentoFavorecidos",
					BradescoViewExceptionActionType.ACTION, false);
		}

		return "incConfManterSolicitacaoRastreamentoFavorecidos";

	}

	/*
	 * EXCLUS�O
	 */

	/**
	 * Excluir solic rast fav.
	 *
	 * @return the string
	 */
	public String excluirSolicRastFav() {

		this
				.setRetornoDetalharSolicRastFav(new DetalharSolicitacaoRastreamentoSaidaDTO());
		DetalharSolicitacaoRastreamentoEntradaDTO entrada = new DetalharSolicitacaoRastreamentoEntradaDTO();

		saidaListaManterSolicRastFavorecio = listManterSolicRastFav.get(Integer
				.parseInt(itemSelecionadoListaSolicitacao));

		entrada.setCdSolicitacaoPagamento(saidaListaManterSolicRastFavorecio
				.getCdSituacaoSolicitacao());
		entrada.setNrSolicitacaoPagamento(saidaListaManterSolicRastFavorecio
				.getNrSolicitacaoPagamento());

		entradaExcluirSolicRastFav
				.setCdSolicitacaoPagamento(saidaListaManterSolicRastFavorecio
						.getCdSituacaoSolicitacao());
		entradaExcluirSolicRastFav
				.setNrSolicitacaoPagamento(saidaListaManterSolicRastFavorecio
						.getNrSolicitacaoPagamento());

		try {
			retornoDetalharSolicRastFav = manterSolicitacaoRastFavService
					.detalharSolicRastFav(entrada);

			if (retornoDetalharSolicRastFav.getCdAgenciaDeb() == null) {
				retornoDetalharSolicRastFav.setCdAgenciaDeb(Integer
						.parseInt(""));
			}

			if (retornoDetalharSolicRastFav.getNrTarifaBonificacao() == null
					|| retornoDetalharSolicRastFav.getNrTarifaBonificacao()
							.compareTo(BigDecimal.ZERO) == 0) {
				retornoDetalharSolicRastFav.setNrTarifaBonificacao(null);
				renderizaPorcent = false;
			} else {
				renderizaPorcent = true;
			}

			this.getRetornoDetalharSolicRastFav()
					.setDtInicioRastreabilidadeFavorecido(
							retornoDetalharSolicRastFav
									.getDtInicioRastreabilidadeFavorecido()
									.replace(".", "/"));
			this.getRetornoDetalharSolicRastFav()
					.setDtFimRastreabilidadeFavorecido(
							retornoDetalharSolicRastFav
									.getDtFimRastreabilidadeFavorecido()
									.replace(".", "/"));

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ,
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "excManterSolicitacaoRastreamentoFavorecidos";
	}

	/**
	 * Confirmar exclusao solic rast fav.
	 *
	 * @return the string
	 */
	public String confirmarExclusaoSolicRastFav() {

		retornoExcluirSolicRastFav = new ExcluirSolicRastFavSaidaDTO();

		try {
			retornoExcluirSolicRastFav = manterSolicitacaoRastFavService
					.excluirSolicRastFav(entradaExcluirSolicRastFav);
			BradescoFacesUtils.addInfoModalMessage("("
					+ retornoExcluirSolicRastFav.getCodMensagem() + ") "
					+ retornoExcluirSolicRastFav.getMensagem(),
					CON_MANTER_SOLICITACAO_RASTREAMENTO_FAVORECIDO_PESQ,
					"#{manterSolicRastFavBean.consultarSolicitacao}", false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					"excManterSolicitacaoRastreamentoFavorecidos",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "excManterSolicitacaoRastreamentoFavorecidos";
	}

	/**
	 * Consultar identificacao contratos.
	 */
	public void consultarIdentificacaoContratos() {
		consultarContratos();
		setItemFiltroSelecionado(null);
		setBloqueaRadio(true);
	}

	/**
	 * Consultar agencia ope tarifa padrao.
	 */
	public void consultarAgenciaOpeTarifaPadrao() {
		try {
			ConsultarAgenciaOpeTarifaPadraoEntradaDTO entradaDTO = new ConsultarAgenciaOpeTarifaPadraoEntradaDTO();

			entradaDTO
					.setCdPessoaJuridicaEmpresa(getEntradaConsultarListaContratosPessoas()
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(getEntradaConsultarListaContratosPessoas()
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(getEntradaConsultarListaContratosPessoas()
							.getNrSeqContratoNegocio());
			entradaDTO.setCdProdutoServicoOperacao(0);
			entradaDTO.setCdProdutoServicoRelacionado(0);
			entradaDTO.setCdTipoTarifa(7);

			ConsultarAgenciaOpeTarifaPadraoSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl()
					.consultarAgenciaOpeTarifaPadrao(entradaDTO);

			entradaIncluirSolicRastFav.setVlTarifaPadrao(saidaDTO
					.getVlTarifaPadrao());
			
			indicadorDescontoBloqueio = verificaStatusBloqueadoDesconto(saidaDTO
					.getCdIndicadorDescontoBloqueio());
			
			verificarTarifaAtualizada();
			
		} catch (PdcAdapterFunctionalException p) {
			throw p;
		}
	}
	
	
	private void verificarTarifaAtualizada(){
		entradaIncluirSolicRastFav.setVlrTarifaAtual(indicadorDescontoBloqueio ? entradaIncluirSolicRastFav.getVlTarifaPadrao() : BigDecimal.ZERO);
	}
	
	/**
	 * Verifica status bloqueado desconto.
	 * 
	 * @param flag
	 *            the flag
	 * @return true, if verifica status bloqueado desconto
	 */
	private boolean verificaStatusBloqueadoDesconto(String flag) {
		if ("N".equals(flag)) {
			return true;
		}
		return false;
	}

	
	/*
	 * VARIAVEIS
	 */

	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public Integer getModalidadeServico() {
		return modalidadeServico;
	}

	/**
	 * Set: modalidadeServico.
	 *
	 * @param operacao the modalidade servico
	 */
	public void setModalidadeServico(Integer operacao) {
		this.modalidadeServico = operacao;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public Integer getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param produto the tipo servico
	 */
	public void setTipoServico(Integer produto) {
		this.tipoServico = produto;
	}

	/**
	 * Get: listaModalidadeServico.
	 *
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {

		return listaModalidadeServico;
	}

	/**
	 * Set: listaModalidadeServico.
	 *
	 * @param listaModalidaeServico the lista modalidade servico
	 */
	public void setListaModalidadeServico(List<SelectItem> listaModalidaeServico) {
		this.listaModalidadeServico = listaModalidaeServico;
	}

	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {

		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: entradaExcluirSolicRastFav.
	 *
	 * @return entradaExcluirSolicRastFav
	 */
	public ExcluirSolicRastFavEntradaDTO getEntradaExcluirSolicRastFav() {
		return entradaExcluirSolicRastFav;
	}

	/**
	 * Set: entradaExcluirSolicRastFav.
	 *
	 * @param entradaExcluirSolicRastFav the entrada excluir solic rast fav
	 */
	public void setEntradaExcluirSolicRastFav(
			ExcluirSolicRastFavEntradaDTO entradaExcluirSolicRastFav) {
		this.entradaExcluirSolicRastFav = entradaExcluirSolicRastFav;
	}

	/**
	 * Get: entradaIncluirSolicRastFav.
	 *
	 * @return entradaIncluirSolicRastFav
	 */
	public IncluirSolicRastFavEntradaDTO getEntradaIncluirSolicRastFav() {
		return entradaIncluirSolicRastFav;
	}

	/**
	 * Set: entradaIncluirSolicRastFav.
	 *
	 * @param entradaIncluirSolicRastFav the entrada incluir solic rast fav
	 */
	public void setEntradaIncluirSolicRastFav(
			IncluirSolicRastFavEntradaDTO entradaIncluirSolicRastFav) {
		this.entradaIncluirSolicRastFav = entradaIncluirSolicRastFav;
	}

	/**
	 * Get: entradaManterSolicRastFavorecido.
	 *
	 * @return entradaManterSolicRastFavorecido
	 */
	public ManterSolicRastFavEntradaDTO getEntradaManterSolicRastFavorecido() {
		return entradaManterSolicRastFavorecido;
	}

	/**
	 * Set: entradaManterSolicRastFavorecido.
	 *
	 * @param entradaManterSolicRastFavorecido the entrada manter solic rast favorecido
	 */
	public void setEntradaManterSolicRastFavorecido(
			ManterSolicRastFavEntradaDTO entradaManterSolicRastFavorecido) {
		this.entradaManterSolicRastFavorecido = entradaManterSolicRastFavorecido;
	}

	/**
	 * Get: listManterSolicRastFav.
	 *
	 * @return listManterSolicRastFav
	 */
	public List<ManterSolicRastFavSaidaDTO> getListManterSolicRastFav() {
		return listManterSolicRastFav;
	}

	/**
	 * Set: listManterSolicRastFav.
	 *
	 * @param listManterSolicRastFav the list manter solic rast fav
	 */
	public void setListManterSolicRastFav(
			List<ManterSolicRastFavSaidaDTO> listManterSolicRastFav) {
		this.listManterSolicRastFav = listManterSolicRastFav;
	}

	/**
	 * Get: manterSolicitacaoRastFavService.
	 *
	 * @return manterSolicitacaoRastFavService
	 */
	public IManterSolicitacaoRastFavService getManterSolicitacaoRastFavService() {
		return manterSolicitacaoRastFavService;
	}

	/**
	 * Set: manterSolicitacaoRastFavService.
	 *
	 * @param manterSolicitacaoRastFavService the manter solicitacao rast fav service
	 */
	public void setManterSolicitacaoRastFavService(
			IManterSolicitacaoRastFavService manterSolicitacaoRastFavService) {
		this.manterSolicitacaoRastFavService = manterSolicitacaoRastFavService;
	}

	/**
	 * Get: retornoExcluirSolicRastFav.
	 *
	 * @return retornoExcluirSolicRastFav
	 */
	public ExcluirSolicRastFavSaidaDTO getRetornoExcluirSolicRastFav() {
		return retornoExcluirSolicRastFav;
	}

	/**
	 * Set: retornoExcluirSolicRastFav.
	 *
	 * @param retornoExcluirSolicRastFav the retorno excluir solic rast fav
	 */
	public void setRetornoExcluirSolicRastFav(
			ExcluirSolicRastFavSaidaDTO retornoExcluirSolicRastFav) {
		this.retornoExcluirSolicRastFav = retornoExcluirSolicRastFav;
	}

	/**
	 * Get: retornoIncluirSolicRastFav.
	 *
	 * @return retornoIncluirSolicRastFav
	 */
	public IncluirSolicRastFavSaidaDTO getRetornoIncluirSolicRastFav() {
		return retornoIncluirSolicRastFav;
	}

	/**
	 * Set: retornoIncluirSolicRastFav.
	 *
	 * @param retornoIncluirSolicRastFav the retorno incluir solic rast fav
	 */
	public void setRetornoIncluirSolicRastFav(
			IncluirSolicRastFavSaidaDTO retornoIncluirSolicRastFav) {
		this.retornoIncluirSolicRastFav = retornoIncluirSolicRastFav;
	}

	/**
	 * Get: itemSelecionadoListaSolicitacao.
	 *
	 * @return itemSelecionadoListaSolicitacao
	 */
	public String getItemSelecionadoListaSolicitacao() {
		return itemSelecionadoListaSolicitacao;
	}

	/**
	 * Set: itemSelecionadoListaSolicitacao.
	 *
	 * @param itemSelecionadoListaSolicitacao the item selecionado lista solicitacao
	 */
	public void setItemSelecionadoListaSolicitacao(
			String itemSelecionadoListaSolicitacao) {
		this.itemSelecionadoListaSolicitacao = itemSelecionadoListaSolicitacao;
	}

	/**
	 * Get: itemCheck.
	 *
	 * @return itemCheck
	 */
	public Boolean getItemCheck() {
		return itemCheck;
	}

	/**
	 * Get: entradaDetalharSolicRastFav.
	 *
	 * @return entradaDetalharSolicRastFav
	 */
	public DetalharSolicitacaoRastreamentoEntradaDTO getEntradaDetalharSolicRastFav() {
		return entradaDetalharSolicRastFav;
	}

	/**
	 * Set: entradaDetalharSolicRastFav.
	 *
	 * @param entradaDetalharSolicRastFav the entrada detalhar solic rast fav
	 */
	public void setEntradaDetalharSolicRastFav(
			DetalharSolicitacaoRastreamentoEntradaDTO entradaDetalharSolicRastFav) {
		this.entradaDetalharSolicRastFav = entradaDetalharSolicRastFav;
	}

	/**
	 * Get: retornoDetalharSolicRastFav.
	 *
	 * @return retornoDetalharSolicRastFav
	 */
	public DetalharSolicitacaoRastreamentoSaidaDTO getRetornoDetalharSolicRastFav() {
		return retornoDetalharSolicRastFav;
	}

	/**
	 * Set: retornoDetalharSolicRastFav.
	 *
	 * @param retornoDetalharSolicRastFav the retorno detalhar solic rast fav
	 */
	public void setRetornoDetalharSolicRastFav(
			DetalharSolicitacaoRastreamentoSaidaDTO retornoDetalharSolicRastFav) {
		this.retornoDetalharSolicRastFav = retornoDetalharSolicRastFav;
	}

	/**
	 * Set: itemCheck.
	 *
	 * @param itemCheck the item check
	 */
	public void setItemCheck(Boolean itemCheck) {
		this.itemCheck = itemCheck;
	}

	/**
	 * Get: saidaListaManterSolicRastFavorecio.
	 *
	 * @return saidaListaManterSolicRastFavorecio
	 */
	public ManterSolicRastFavSaidaDTO getSaidaListaManterSolicRastFavorecio() {
		return saidaListaManterSolicRastFavorecio;
	}

	/**
	 * Set: saidaListaManterSolicRastFavorecio.
	 *
	 * @param saidaListaManterSolicRastFavorecio the saida lista manter solic rast favorecio
	 */
	public void setSaidaListaManterSolicRastFavorecio(
			ManterSolicRastFavSaidaDTO saidaListaManterSolicRastFavorecio) {
		this.saidaListaManterSolicRastFavorecio = saidaListaManterSolicRastFavorecio;
	}

	/**
	 * Is exibir botao paginacao.
	 *
	 * @return true, if is exibir botao paginacao
	 */
	public boolean isExibirBotaoPaginacao() {
		return exibirBotaoPaginacao;
	}

	/**
	 * Set: exibirBotaoPaginacao.
	 *
	 * @param exibirBotaoPaginacao the exibir botao paginacao
	 */
	public void setExibirBotaoPaginacao(boolean exibirBotaoPaginacao) {
		this.exibirBotaoPaginacao = exibirBotaoPaginacao;
	}

	/**
	 * Get: itemArgumentoPesquisa.
	 *
	 * @return itemArgumentoPesquisa
	 */
	public String getItemArgumentoPesquisa() {
		return itemArgumentoPesquisa;
	}

	/**
	 * Set: itemArgumentoPesquisa.
	 *
	 * @param itemArgumentoPesquisa the item argumento pesquisa
	 */
	public void setItemArgumentoPesquisa(String itemArgumentoPesquisa) {
		this.itemArgumentoPesquisa = itemArgumentoPesquisa;
	}

	/**
	 * Get: itemBloqSelecionado.
	 *
	 * @return itemBloqSelecionado
	 */
	public String getItemBloqSelecionado() {
		return itemBloqSelecionado;
	}

	/**
	 * Set: itemBloqSelecionado.
	 *
	 * @param itemBloqSelecionado the item bloq selecionado
	 */
	public void setItemBloqSelecionado(String itemBloqSelecionado) {
		this.itemBloqSelecionado = itemBloqSelecionado;
	}

	/**
	 * Get: motivoBloqueio.
	 *
	 * @return motivoBloqueio
	 */
	public String getMotivoBloqueio() {
		return motivoBloqueio;
	}

	/**
	 * Set: motivoBloqueio.
	 *
	 * @param motivoBloqueio the motivo bloqueio
	 */
	public void setMotivoBloqueio(String motivoBloqueio) {
		this.motivoBloqueio = motivoBloqueio;
	}

	/**
	 * Get: filtroModalidadeServico.
	 *
	 * @return filtroModalidadeServico
	 */
	public Integer getFiltroModalidadeServico() {
		return filtroModalidadeServico;
	}

	/**
	 * Set: filtroModalidadeServico.
	 *
	 * @param filtroModalidadeServico the filtro modalidade servico
	 */
	public void setFiltroModalidadeServico(Integer filtroModalidadeServico) {
		this.filtroModalidadeServico = filtroModalidadeServico;
	}

	/**
	 * Get: filtroTipoServico.
	 *
	 * @return filtroTipoServico
	 */
	public Integer getFiltroTipoServico() {
		return filtroTipoServico;
	}

	/**
	 * Set: filtroTipoServico.
	 *
	 * @param filtroTipoServico the filtro tipo servico
	 */
	public void setFiltroTipoServico(Integer filtroTipoServico) {
		this.filtroTipoServico = filtroTipoServico;
	}

	/**
	 * Get: listaModalidadeServicoIncluir.
	 *
	 * @return listaModalidadeServicoIncluir
	 */
	public List<SelectItem> getListaModalidadeServicoIncluir() {
		return listaModalidadeServicoIncluir;
	}

	/**
	 * Set: listaModalidadeServicoIncluir.
	 *
	 * @param listaModalidadeServicoIncluir the lista modalidade servico incluir
	 */
	public void setListaModalidadeServicoIncluir(
			List<SelectItem> listaModalidadeServicoIncluir) {
		this.listaModalidadeServicoIncluir = listaModalidadeServicoIncluir;
	}

	/**
	 * Get: tipoModalidadeDesc.
	 *
	 * @return tipoModalidadeDesc
	 */
	public String getTipoModalidadeDesc() {
		return tipoModalidadeDesc;
	}

	/**
	 * Set: tipoModalidadeDesc.
	 *
	 * @param tipoModalidadeDesc the tipo modalidade desc
	 */
	public void setTipoModalidadeDesc(String tipoModalidadeDesc) {
		this.tipoModalidadeDesc = tipoModalidadeDesc;
	}

	/**
	 * Get: tipoServicoDesc.
	 *
	 * @return tipoServicoDesc
	 */
	public String getTipoServicoDesc() {
		return tipoServicoDesc;
	}

	/**
	 * Set: tipoServicoDesc.
	 *
	 * @param tipoServicoDesc the tipo servico desc
	 */
	public void setTipoServicoDesc(String tipoServicoDesc) {
		this.tipoServicoDesc = tipoServicoDesc;
	}

	/**
	 * Is box cliente.
	 *
	 * @return true, if is box cliente
	 */
	public boolean isBoxCliente() {
		return boxCliente;
	}

	/**
	 * Set: boxCliente.
	 *
	 * @param boxCliente the box cliente
	 */
	public void setBoxCliente(boolean boxCliente) {
		this.boxCliente = boxCliente;
	}

	/**
	 * Is box departamento.
	 *
	 * @return true, if is box departamento
	 */
	public boolean isBoxDepartamento() {
		return boxDepartamento;
	}

	/**
	 * Set: boxDepartamento.
	 *
	 * @param boxDepartamento the box departamento
	 */
	public void setBoxDepartamento(boolean boxDepartamento) {
		this.boxDepartamento = boxDepartamento;
	}

	/**
	 * Get: dataFimRastreamento.
	 *
	 * @return dataFimRastreamento
	 */
	public Date getDataFimRastreamento() {
		return dataFimRastreamento;
	}

	/**
	 * Set: dataFimRastreamento.
	 *
	 * @param dataFimRastreamento the data fim rastreamento
	 */
	public void setDataFimRastreamento(Date dataFimRastreamento) {
		this.dataFimRastreamento = dataFimRastreamento;
	}

	/**
	 * Get: dataInicioRastreamento.
	 *
	 * @return dataInicioRastreamento
	 */
	public Date getDataInicioRastreamento() {
		return dataInicioRastreamento;
	}

	/**
	 * Set: dataInicioRastreamento.
	 *
	 * @param dataInicioRastreamento the data inicio rastreamento
	 */
	public void setDataInicioRastreamento(Date dataInicioRastreamento) {
		this.dataInicioRastreamento = dataInicioRastreamento;
	}

	/**
	 * Get: dsDestinoRetorno.
	 *
	 * @return dsDestinoRetorno
	 */
	public String getDsDestinoRetorno() {
		return dsDestinoRetorno;
	}

	/**
	 * Set: dsDestinoRetorno.
	 *
	 * @param dsDestinoRetorno the ds destino retorno
	 */
	public void setDsDestinoRetorno(String dsDestinoRetorno) {
		this.dsDestinoRetorno = dsDestinoRetorno;
	}

	/**
	 * Get: dsInclusaoAuto.
	 *
	 * @return dsInclusaoAuto
	 */
	public String getDsInclusaoAuto() {
		return dsInclusaoAuto;
	}

	/**
	 * Set: dsInclusaoAuto.
	 *
	 * @param dsInclusaoAuto the ds inclusao auto
	 */
	public void setDsInclusaoAuto(String dsInclusaoAuto) {
		this.dsInclusaoAuto = dsInclusaoAuto;
	}

	/**
	 * Get: itemChkDesconto.
	 *
	 * @return itemChkDesconto
	 */
	public Boolean getItemChkDesconto() {
		return itemChkDesconto;
	}

	/**
	 * Set: itemChkDesconto.
	 *
	 * @param itemChkDesconto the item chk desconto
	 */
	public void setItemChkDesconto(Boolean itemChkDesconto) {
		this.itemChkDesconto = itemChkDesconto;
	}

	/**
	 * Get: vlTarifaBonificacao.
	 *
	 * @return vlTarifaBonificacao
	 */
	public BigDecimal getVlTarifaBonificacao() {
		return vlTarifaBonificacao;
	}

	/**
	 * Set: vlTarifaBonificacao.
	 *
	 * @param vlTarifaBonificacao the vl tarifa bonificacao
	 */
	public void setVlTarifaBonificacao(BigDecimal vlTarifaBonificacao) {
		this.vlTarifaBonificacao = vlTarifaBonificacao;
	}

	/**
	 * Get: listaSolicitacao.
	 *
	 * @return listaSolicitacao
	 */
	public List<SelectItem> getListaSolicitacao() {
		return listaSolicitacao;
	}

	/**
	 * Set: listaSolicitacao.
	 *
	 * @param listaSolicitacao the lista solicitacao
	 */
	public void setListaSolicitacao(List<SelectItem> listaSolicitacao) {
		this.listaSolicitacao = listaSolicitacao;
	}

	/**
	 * Set: inclusaoAuto.
	 *
	 * @param inclusaoAuto the inclusao auto
	 */
	public void setInclusaoAuto(Integer inclusaoAuto) {
		this.inclusaoAuto = inclusaoAuto;
	}

	/**
	 * Get: dsInclusaoFavorecido.
	 *
	 * @return dsInclusaoFavorecido
	 */
	public String getDsInclusaoFavorecido() {
		return dsInclusaoFavorecido;
	}

	/**
	 * Set: dsInclusaoFavorecido.
	 *
	 * @param dsInclusaoFavorecido the ds inclusao favorecido
	 */
	public void setDsInclusaoFavorecido(String dsInclusaoFavorecido) {
		this.dsInclusaoFavorecido = dsInclusaoFavorecido;
	}

	/**
	 * Get: atributoSoltc.
	 *
	 * @return atributoSoltc
	 */
	public VerificaraAtributoSoltcSaidaDTO getAtributoSoltc() {
		return atributoSoltc;
	}

	/**
	 * Set: atributoSoltc.
	 *
	 * @param atributoSoltc the atributo soltc
	 */
	public void setAtributoSoltc(VerificaraAtributoSoltcSaidaDTO atributoSoltc) {
		this.atributoSoltc = atributoSoltc;
	}

	/**
	 * Is desabilita desconto tarifa.
	 *
	 * @return true, if is desabilita desconto tarifa
	 */
	public boolean isDesabilitaDescontoTarifa() {
		if (atributoSoltc != null
				&& "S".equalsIgnoreCase(atributoSoltc.getDsTarifaBloqueio())) {
			return true;
		}
		return false;
	}

	/**
	 * Is renderiza porcent.
	 *
	 * @return true, if is renderiza porcent
	 */
	public boolean isRenderizaPorcent() {
		return renderizaPorcent;
	}

	/**
	 * Set: renderizaPorcent.
	 *
	 * @param renderizaPorcent the renderiza porcent
	 */
	public void setRenderizaPorcent(boolean renderizaPorcent) {
		this.renderizaPorcent = renderizaPorcent;
	}

	/**
	 * Get: destinoAquivoRadio.
	 *
	 * @return destinoAquivoRadio
	 */
	public Integer getDestinoAquivoRadio() {
		return destinoAquivoRadio;
	}

	/**
	 * Set: destinoAquivoRadio.
	 *
	 * @param destinoAquivoRadio the destino aquivo radio
	 */
	public void setDestinoAquivoRadio(Integer destinoAquivoRadio) {
		this.destinoAquivoRadio = destinoAquivoRadio;
	}

	/**
	 * Get: numPercentualDescTarifa.
	 *
	 * @return numPercentualDescTarifa
	 */
	public BigDecimal getNumPercentualDescTarifa() {
		return numPercentualDescTarifa;
	}

	/**
	 * Set: numPercentualDescTarifa.
	 *
	 * @param numPercentualDescTarifa the num percentual desc tarifa
	 */
	public void setNumPercentualDescTarifa(BigDecimal numPercentualDescTarifa) {
		this.numPercentualDescTarifa = numPercentualDescTarifa;
	}

	/**
	 * Get: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @return solEmissaoComprovantePagtoClientePagServiceImpl
	 */
	public ISolEmissaoComprovantePagtoClientePagService getSolEmissaoComprovantePagtoClientePagServiceImpl() {
		return solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	/**
	 * Set: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @param solEmissaoComprovantePagtoClientePagServiceImpl the sol emissao comprovante pagto cliente pag service impl
	 */
	public void setSolEmissaoComprovantePagtoClientePagServiceImpl(
			ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl) {
		this.solEmissaoComprovantePagtoClientePagServiceImpl = solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	public boolean isIndicadorDescontoBloqueio() {
		return indicadorDescontoBloqueio;
	}

	public void setIndicadorDescontoBloqueio(boolean indicadorDescontoBloqueio) {
		this.indicadorDescontoBloqueio = indicadorDescontoBloqueio;
	}
}
