/**
 * Nome: br.com.bradesco.web.pgit.mantercontrato
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

import static br.com.bradesco.web.pgit.utils.SiteUtil.isNotNull;
import static br.com.bradesco.web.pgit.utils.SiteUtil.not;
import static br.com.bradesco.web.pgit.view.enuns.TipoMontagemEnum.TIPO_SERVICO;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.confcestastarifas.IConfCestaTarifaService;
import br.com.bradesco.web.pgit.service.business.confcestastarifas.dto.ConfCestaTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.confcestastarifas.dto.ConfCestaTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.confcestastarifas.dto.OcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.confcestastarifas.impl.IConfCestaTarifaServiceImpl;
import br.com.bradesco.web.pgit.service.business.contrato.IContratoService;
import br.com.bradesco.web.pgit.service.business.contrato.IContratoServiceConstants;
import br.com.bradesco.web.pgit.service.business.contrato.impl.ContratoServiceImpl;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.IImprimirAnexoPrimeiroContratoService;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.AnexoContratoDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.ImprimirAnexoPrimeiroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.ImprimirAnexoPrimeiroContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasContas;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasLayout;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasParticipantes;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.IImprimirAnexoSegundoContratoService;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ImprimirAnexoSegundoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ImprimirAnexoSegundoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasAvisoFormularios;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasAvisos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasComprovanteSalariosDiversos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasComprovantes;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasCreditos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModularidades;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasOperacoes;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasServicoPagamentos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasTitulos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasTriTributo;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.PagamentosDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ServicosDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ServicosModalidadesDTO;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.IImprimircontratoService;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarAgenciaGestoraEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarAgenciaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaLayoutArquivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaTipoRetornoLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarTarifaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarTarifaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasVincContEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasVincContSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarPropostaAndamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarPropostaAndamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarUsuarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarUsuarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.confcestastarifas.ConfCestasTarifasBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: ManterContratoBean
 * <p>
 * Prop�sito: Implementa��o de M�todos de a��o e navega��o nas telas do menu
 * Manter Contrato
 * </p>.
 *
 * @author CPM Braxis / TI Melhorias - Arquitetura
 * @version 1.0
 */
public class ManterContratoBean {

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo manterContratoContasBean. */
	private ContasBean manterContratoContasBean;

	/** Atributo manterContratoRepresentanteBean. */
	private RepresentanteBean manterContratoRepresentanteBean;

	/** Atributo manterContratoParticipantesBean. */
	private ParticipantesBean manterContratoParticipantesBean;

	/** Atributo manterContratoServicosBean. */
	private ServicosBean manterContratoServicosBean;

	/** Atributo manterContratoDadosBasicosBean. */
	private DadosBasicosBean manterContratoDadosBasicosBean;

	/** Atributo manterContratoLayoutsArquivosBean. */
	private LayoutsArquivosBean manterContratoLayoutsArquivosBean;

	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

	/** Atributo meiosTransmissaoBean. */
	private MeiosTransmissaoBean meiosTransmissaoBean;

	/** Atributo manterContratoTarifasBean. */
	private TarifasBean manterContratoTarifasBean;

	/** Atributo imprimirContratoService. */
	private IImprimircontratoService imprimirContratoService;

	/** Atributo contratoPgitSelecionado. */
	private ListarContratosPgitSaidaDTO contratoPgitSelecionado = new ListarContratosPgitSaidaDTO();;

	/** Atributo bancoAgenciaDTO. */
	private ConsultarBancoAgenciaSaidaDTO bancoAgenciaDTO = new ConsultarBancoAgenciaSaidaDTO();

	/** Atributo imprimirService. */
	private IImprimirService imprimirService = null;

	/** Atributo imprimirAnexoPrimeiroContratoService. */
	private IImprimirAnexoPrimeiroContratoService imprimirAnexoPrimeiroContratoService;

	/** Atributo manterCadContaDestinoService. */
	private IManterCadContaDestinoService manterCadContaDestinoService;

	/** Atributo imprimirAnexoSegundoContratoService. */
	private IImprimirAnexoSegundoContratoService imprimirAnexoSegundoContratoService;

	/** Atributo manterPerfilTrocaArqLayoutServiceImpl. */
	private IManterPerfilTrocaArqLayoutService manterPerfilTrocaArqLayoutServiceImpl;

	/** Atributo saidaMeioTransmissao. */
	private ListarContratosPgitSaidaDTO saidaMeioTransmissao;

	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado;

	/** Atributo cpfCnpjMaster. */
	private String cpfCnpjMaster;

	/** Atributo cpfCnpjMaster. */
	private List<ListarServicosSaidaDTO> dsServicoSelecionado = new ArrayList<ListarServicosSaidaDTO>(1);


	/** Atributo nomeRazaoSocialMaster. */
	private String nomeRazaoSocialMaster;

	/** Atributo grupoEconomicoMaster. */
	private String grupoEconomicoMaster;

	/** Atributo atividadeEconomicaMaster. */
	private String atividadeEconomicaMaster;

	/** Atributo segmentoMaster. */
	private String segmentoMaster;

	/** Atributo subSegmentoMaster. */
	private String subSegmentoMaster;

	/** Atributo empresa. */
	private String empresa;

	/** Atributo cdEmpresa. */
	private Long cdEmpresa;

	/** Atributo tipo. */
	private String tipo;

	/** Atributo cdTipo. */
	private Integer cdTipo;

	/** Atributo numero. */
	private String numero;

	/** Atributo numeroContrato. */
	private Long numeroContrato;

	/** Atributo motivoDesc. */
	private String motivoDesc;

	/** Atributo situacaoDesc. */
	private String situacaoDesc;

	/** Atributo participacao. */
	private String participacao;

	/** Atributo possuiAditivos. */
	private String possuiAditivos;

	/** Atributo dataHoraCadastramento. */
	private String dataHoraCadastramento;

	/** Atributo inicioVigencia. */
	private String inicioVigencia;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo cdAgenciaGestora. */
	private int cdAgenciaGestora;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;

	/** Atributo cdGerenteResponsavel. */
	private Long cdGerenteResponsavel;

	/** Atributo dsNomeRazaoSocialParticipante. */
	private String dsNomeRazaoSocialParticipante;

	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo cpfCnpj. */
	private String cpfCnpj;

	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;

	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos = false;

	/** Atributo renegociavel. */
	private String renegociavel;

	/** Atributo codigoAgenciaGestora. */
	private Integer codigoAgenciaGestora;

	/** Atributo descAgenciaGestora. */
	private Integer descAgenciaGestora;

	/** Atributo codDescAgenciaGestora. */
	private String codDescAgenciaGestora;

	/** Atributo saidaValidar. */
	private ValidarPropostaAndamentoSaidaDTO saidaValidar;

	/** Atributo urlRenegociacaoNovo. */
	private String urlRenegociacaoNovo;

	/** Atributo saidaValidarUsuario. */
	private ValidarUsuarioSaidaDTO saidaValidarUsuario;

	/** Atributo entradaValidarUsuario. */
	private ValidarUsuarioEntradaDTO entradaValidarUsuario;

	/** Atributo restringirAcessoBotoes. */
	private boolean restringirAcessoBotoes;

	/** Atributo dsServicoSegundaTela. */
	private String dsServicoSegundaTela;

	/** The saida primeiro contrato. */
	private ImprimirAnexoPrimeiroContratoSaidaDTO saidaPrimeiroContrato = null;

	/** The saida primeiro contrato com loop. */
	private ImprimirAnexoPrimeiroContratoSaidaDTO saidaPrimeiroContratoLoop = null;

	/** The saida segundo contrato. */
	private ImprimirAnexoSegundoContratoSaidaDTO saidaSegundoContrato = null;

	/** The saida lista perfil troca arquivo. */
	private ConsultarListaPerfilTrocaArquivoSaidaDTO saidaListaPerfilTrocaArquivo = null;

	/** Atributo listaGridTarifasModalidade. */
	private List<ConsultarTarifaContratoSaidaDTO> listaGridTarifasModalidade = null;

	/** Atributo flagMetodoImprimirAnexo. */
	private boolean flagMetodoImprimirAnexo = false;	

	/** Atributo listaServicoModalidadeTelaResumo. */
	List<PagamentosDTO> listaServicoModalidadeTelaResumo = null;

	/** Atributo listaPagamentosFornec. */
	private List<PagamentosDTO> listaPagamentosFornec = null;

	/** Atributo listaPagamentosBeneficios. */
	private List<PagamentosDTO> listaPagamentosBeneficios = null;

	/** Atributo listaPagamentosSalarios. */
	private List<PagamentosDTO> listaPagamentosSalarios = null;	

	/** Atributo listaBeneficiosModularidades. */
	private List<PagamentosDTO> listaBeneficiosModularidades = null;

	/** Atributo listaServicos. */
	private List<ServicosDTO> listaServicos = null;

	/** Atributo listaServicos. */
	private List<ServicosDTO> listaServicosCompl = null;

	/** Atributo listaPagamentosTri. */
	private List<PagamentosDTO> listaPagamentosTri = null;	
	/** Atributo listaTitulos. */
	private List<PagamentosDTO> listaTitulos = null;

	/** Atributo listaFornModularidades. */
	private List<PagamentosDTO> listaFornModularidades = null;

	/** Atributo listaTri. */
	private List<PagamentosDTO> listaTri = null;	

	/** Atributo listaServicosModalidades. */
	private List<ServicosModalidadesDTO> listaServicosModalidades = null;

	/** Atributo listaFornecedorOutrasModularidades. */
	private List<PagamentosDTO> listaFornecedorOutrasModularidades = null; 

	/** Atributo listaAnexoContrato. */
	private List<AnexoContratoDTO> listaAnexoContrato = null;

	/** Atributo listaSalariosMod. */
	private List<PagamentosDTO> listaSalariosMod = null;

	/** Atributo listaParticipantes. */
	private List<OcorrenciasParticipantes> listaParticipantes = null;

	/** Atributo listaContas. */
	private List<OcorrenciasContas> listaContas = null;

	/** Atributo listaLayouts. */
	private List<OcorrenciasLayout> listaLayouts = null;

	/** Atributo exibePgtoFornecedor. */
	private String exibePgtoFornecedor = null;

	/** Atributo exibePgtoTributos. */
	private String exibePgtoTributos = null;

	/** Atributo exibePgtoSalarios. */
	private String exibePgtoSalarios = null;

	/** Atributo exibePgtoBeneficios. */
	private String exibePgtoBeneficios = null;

	/** Atributo imprimirGridTarifa. */
	private String imprimirGridTarifa = null; 

	/** Atributo nomeTabelaAtual. */
	private String nomeTabelaAtual = null;
	
	/** Atributo omiteServicosResumoContrato. */
	private boolean omiteServicosResumoContrato = false;
	
    /** The contrato bytes. */
    private ByteArrayOutputStream contratoBytes = null;

    /** Atributo contratoServiceImpl. */
    private IContratoService contratoServiceImpl = null;
    
    /** The contrato exception. */
    private Exception contratoException = null;
    
    private ConfCestasTarifasBean confCestasTarifasBean;
    
    private List<OcorrenciasSaidaDTO> listaConfCestasTarifas = null;
    
    private ListarContratosPgitSaidaDTO saidaDTO = null;
    
    private ConfCestaTarifaEntradaDTO entradaCestaTarifa = null;
    
    private IConfCestaTarifaService confCestaTarifaServiceImpl;
    
    private ConfCestaTarifaSaidaDTO saidaCestasTarifasDTO = null;
    
    private boolean visualizaCestaTarifas = false;
    
    /** Atributo exibeCestaTarifas. */
    private String exibeCestaTarifas = null;
    
    /*
     * 0   'NENHUM '                                          
    1   'MANUTENCAO DE PAGAMENTOS'                         
    2   'AUTORIZACAO/DESAUTORIZACAO'                       
    3   'AMBOS'
     * */
//    private static final String COD_IDENTIFICADOR_TIPO_RETORNO_INTERNET_NENHUM = null;
//    private static final String COD_IDENTIFICADOR_TIPO_RETORNO_INTERNET_MANUTENCAO_PAGAMENTOS = null;
//    private static final String COD_IDENTIFICADOR_TIPO_RETORNO_INTERNET_AUTORIZACAO_DESAUTORIZACAO = null;
//    private static final String COD_IDENTIFICADOR_TIPO_RETORNO_INTERNET_AMBOS = null;
    
    private static final int CD_MAX_OCORRENCIAS = 50;
                                                

    
	/**
	 * Nome: iniciarPagina.
	 *
	 * @param evt the evt
	 * @see
	 */
	public void iniciarPagina(ActionEvent evt) {
		this.identificacaoClienteContratoBean
		.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
		.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean
		.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean
		.setPaginaCliente("identificacaoClienteManterContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conManterContrato");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean
		.setHabilitaEmpresaGestoraTipoContrato(true);

		setDsGerenteResponsavel("");
		setCpfCnpjParticipante("");
		setDsNomeRazaoSocialParticipante("");
		setCpfCnpj("");
		setNomeRazaoSocial("");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		setImprimirGridTarifa("N");
		setOmiteServicosResumoContrato(false);
		confCestaTarifaServiceImpl = new IConfCestaTarifaServiceImpl();
		
	}

	/**
	 * Nome: imprimirRelatorioContas.
	 *
	 * @return the string
	 * @see
	 */
	public String imprimirRelatorioContas() {
		try {
			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
			.getExternalContext().getContext();
			String logoPath = servletContext.getRealPath(caminho);

			Map<String, Object> par = new HashMap<String, Object>();

			par.put("titulo", "Pagamento Integrado - Contas");
			par.put("empresa", getEmpresa());
			par.put("numero", getNumero());
			par.put("tipo", getTipo());
			par.put("descricaoContrato", manterContratoContasBean
					.getDescricaoContrato());
			par.put("logoBradesco", logoPath);

			try {
				// M�todo que gera o relat�rio PDF.
				PgitUtil.geraRelatorioPdf(
						"/relatorios/manutencaoContrato/manterContrato/contas",
						"imprimirContasManterContrato",
						manterContratoContasBean.getListaGridPesquisaContas(),
						par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);
		}

		return "";
	}

	/** Atributo cdTipoArquivoRetorno. */
	private Integer cdTipoArquivoRetorno;

	/** Atributo listaTipoArquivoRetorno. */
	private List<SelectItem> listaTipoArquivoRetorno = new ArrayList<SelectItem>();

	/** Atributo listaTipoArquivoRetornoHash. */
	private Map<Integer, String> listaTipoArquivoRetornoHash = new HashMap<Integer, String>();

	/**
	 * Nome: getComboTipoMontagem.
	 *
	 * @return comboTipoMontagem
	 * @see
	 */
	public List<SelectItem> getComboTipoMontagem() {

		ConsultarListaTipoRetornoLayoutSaidaDTO ob = null;

		if (manterContratoLayoutsArquivosBean.getItemSelecionadoListaRetorno() != null) {
			ob = manterContratoLayoutsArquivosBean.getListaRetorno().get(
					manterContratoLayoutsArquivosBean
					.getItemSelecionadoListaRetorno());
		}

		List<SelectItem> combo = null;

		combo = manterContratoLayoutsArquivosBean.getTipoMontagem();

		if (!"PTRB".equals(manterContratoLayoutsArquivosBean.getDsTipoLayout())) {

			if (ob != null) {
				if ((ob.getCdTipoArquivoRetorno() >= 1 && ob
						.getCdTipoArquivoRetorno() <= 5)
						|| ob.getCdTipoArquivoRetorno() == 23) {
					combo.add(new SelectItem(TIPO_SERVICO.getCdTipoMontagem(),
							TIPO_SERVICO.getDsTipoMontagem()));
				}

			}
		}

		return combo;
	}

	/**
	 * Nome: conta.
	 *
	 * @return the string
	 * @see
	 */
	public String conta() {

		saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDataHoraCadastramento(null);
		setCdAgenciaGestora(saidaDTO.getCdAgenciaOperadora());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - "
				+ saidaDTO.getDescFuncionarioBradesco());

		getManterContratoContasBean().setCdCpfCnpjRepresentante(
				saidaDTO.getCdCpfCnpjRepresentante());
		getManterContratoContasBean().setCdFilialCpfCnpjRepresentante(
				saidaDTO.getCdFilialCpfCnpjRepresentante());
		getManterContratoContasBean().setCdControleCpfCnpjRepresentante(
				saidaDTO.getCdControleCpfCnpjRepresentante());

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat(
			"yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1
						.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}

		// setInicioVigencia(inicioVigencia)

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			setDsNomeRazaoSocialParticipante("");
			setCpfCnpjParticipante("");
		} else {
			setDsNomeRazaoSocialParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setCpfCnpjParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}

		manterContratoContasBean.setCdPessoaJuridica(getCdEmpresa());
		manterContratoContasBean.setCdTipoContratoNegocio(getCdTipo());
		manterContratoContasBean.setNrSequenciaContratoNegocio(getNumero());
		manterContratoContasBean.setDescricaoContrato(saidaDTO.getDsContrato());

		manterContratoContasBean.iniciarTela();
		manterContratoContasBean.limparDados();
		//identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		return "CONTAS";
	}

	/**
	 * Nome: imprimirRelatorioParticipantes.
	 *
	 * @return the string
	 * @see
	 */
	public String imprimirRelatorioParticipantes() {
		try {
			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
			.getExternalContext().getContext();
			String logoPath = servletContext.getRealPath(caminho);

			Map<String, Object> par = new HashMap<String, Object>();

			par.put("titulo",
			"Pagamento Integrado - Empresas/Pessoas Participantes");
			par.put("empresa", getEmpresa());
			par.put("numero", getNumero());
			par.put("tipo", getTipo());
			par.put("descricaoContrato", manterContratoParticipantesBean
					.getDescricaoContrato());
			par.put("logoBradesco", logoPath);

			try {
				// M�todo que gera o relat�rio PDF.
				PgitUtil
				.geraRelatorioPdf(
						"/relatorios/manutencaoContrato/manterContrato/participantes",
						"imprimirParticipantesManterContrato",
						manterContratoParticipantesBean.getListaGrid(),
						par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);
		}

		return "";
	}
	
	private List<ListarParticipantesSaidaDTO> carregaListaParticipantesLoop() {
		List<ListarParticipantesSaidaDTO> listaSaida = new ArrayList<ListarParticipantesSaidaDTO>();
        try {
            ListarContratosPgitSaidaDTO contratosPgitSelecionado =
                identificacaoClienteContratoBean.getListaGridPesquisa().get(
                    identificacaoClienteContratoBean.getItemSelecionadoLista());

            ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();

            entradaDTO.setMaxOcorrencias(20);
            entradaDTO.setCodPessoaJuridica(contratosPgitSelecionado.getCdPessoaJuridica());
            entradaDTO.setCodTipoContrato(contratosPgitSelecionado.getCdTipoContrato());
            entradaDTO.setNrSequenciaContrato(contratosPgitSelecionado.getNrSequenciaContrato());

            listaSaida = getManterContratoImpl().listarParticipantes(entradaDTO);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
        }
		return listaSaida;
    }
	
	public String imprimirRelatorioParticipantesComLoop() {
		try {
			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
			.getExternalContext().getContext();
			String logoPath = servletContext.getRealPath(caminho);
			List<ListarParticipantesSaidaDTO> carregaListaParticipantesLoop = this.carregaListaParticipantesLoop();
			
			Map<String, Object> par = new HashMap<String, Object>();

			par.put("titulo",
			"Pagamento Integrado - Empresas/Pessoas Participantes");
			par.put("empresa", getEmpresa());
			par.put("numero", getNumero());
			par.put("tipo", getTipo());
			par.put("descricaoContrato", manterContratoParticipantesBean
					.getDescricaoContrato());
			par.put("logoBradesco", logoPath);

			try {
				// M�todo que gera o relat�rio PDF.
				PgitUtil
				.geraRelatorioPdf(
						"/relatorios/manutencaoContrato/manterContrato/participantes",
						"imprimirParticipantesManterContrato",
						carregaListaParticipantesLoop,
						par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);
		}

		return "";
	}

	/**
	 * Nome: avancarConfirmarAgenciaGestora.
	 *
	 * @return the string
	 * @see
	 */
	public String avancarConfirmarAgenciaGestora() {
		setBancoAgenciaDTO(new ConsultarBancoAgenciaSaidaDTO());

		if (codigoAgenciaGestora != null && codigoAgenciaGestora != 0) {
			try {
				ConsultarBancoAgenciaEntradaDTO entradaDTO = new ConsultarBancoAgenciaEntradaDTO();
				entradaDTO.setCdBanco(237);
				entradaDTO.setCdAgencia(getCodigoAgenciaGestora());
				entradaDTO.setCdDigitoAgencia(00);
				entradaDTO.setCdPessoaJuridicaContrato(getCdEmpresa());

				setBancoAgenciaDTO(getManterCadContaDestinoService()
						.consultarDescricaoBancoAgencia(entradaDTO));

				setCodDescAgenciaGestora(String.format("%s - %s", PgitUtil
						.verificaIntegerNulo(getCodigoAgenciaGestora()),
						PgitUtil.verificaStringNula(getBancoAgenciaDTO()
								.getDsAgencia())));

			} catch (PdcAdapterFunctionalException p) {
				bancoAgenciaDTO.setDsAgencia("");
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), "VOLTAR",
						BradescoViewExceptionActionType.ACTION, false);
			}
		}

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setGerenteResponsavelFormatado(saidaDTO.getCdFuncionarioBradesco()
				+ " - " + saidaDTO.getNmFuncionarioBradesco());
		setDescricaoContrato(saidaDTO.getDsContrato());

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());

		setDataHoraCadastramento(null);

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat(
			"yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1
						.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}

		// setInicioVigencia(inicioVigencia)

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		} else {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getDsNomeRazao());
		}
		return "CONFIRMAR";
	}

	/**
	 * Nome: alterarAgenciaGestora.
	 *
	 * @return the string
	 * @see
	 */
	public String alterarAgenciaGestora() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setNumeroContrato(saidaDTO.getNrSequenciaContrato());
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setGerenteResponsavelFormatado(saidaDTO.getCdFuncionarioBradesco()
				+ " - " + saidaDTO.getNmFuncionarioBradesco());
		setDescricaoContrato(saidaDTO.getDsContrato());
		setCodigoAgenciaGestora(null);
		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());

		setDataHoraCadastramento(null);

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat(
			"yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1
						.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}

		// setInicioVigencia(inicioVigencia)

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		} else {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getDsNomeRazao());
		}

		setCodDescAgenciaGestora("");
		bancoAgenciaDTO.setDsAgencia("");

		return "ALT_EMPRESA_GESTORA";
	}

	/**
	 * Nome: confirmarAlterarAgenciaGestora.
	 *
	 * @return the string
	 * @see
	 */
	public String confirmarAlterarAgenciaGestora() {
		try {

			AlterarAgenciaGestoraEntradaDTO entradaDTO = new AlterarAgenciaGestoraEntradaDTO();

			entradaDTO.setCdpessoaJuridicaContrato(PgitUtil
					.verificaLongNulo(getCdEmpresa()));
			entradaDTO.setCdTipoContratoNegocio(PgitUtil
					.verificaIntegerNulo(getCdTipo()));
			entradaDTO.setNrSequenciaContratoNegocio(PgitUtil
					.verificaLongNulo(getNumeroContrato()));
			entradaDTO.setNuAgencia(PgitUtil
					.verificaIntegerNulo(getCodigoAgenciaGestora()));

			AlterarAgenciaGestoraSaidaDTO saidaDTO = getManterContratoImpl()
			.alterarAgenciaGestora(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
					+ saidaDTO.getMensagem(), "conManterContrato",
					BradescoViewExceptionActionType.ACTION, false);
			identificacaoClienteContratoBean.pesquisar(null);
			// identificacaoClienteContratoBean.limparDadosPesquisaContrato();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);
			return null;
		}
		return "";
	}

	/**
	 * Nome: voltarAgenciaGestora.
	 *
	 * @return the string
	 * @see
	 */
	public String voltarAgenciaGestora() {
		return "VOLTAR";
	}

	/**
	 * Nome: participantes.
	 *
	 * @return the string
	 * @see
	 */
	public String participantes() {

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDataHoraCadastramento(null);
		setDescricaoContrato(saidaDTO.getDsContrato());

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getDescFuncionarioBradesco());
		setGerenteResponsavelFormatado(saidaDTO.getCdFuncionarioBradesco()
				+ " - " + saidaDTO.getDescFuncionarioBradesco());

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat(
			"yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1
						.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}

		// setInicioVigencia(inicioVigencia)

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}
		manterContratoParticipantesBean.inciarTela();

		manterContratoParticipantesBean.setCpfCnpjRepresentante(saidaDTO
				.getCnpjOuCpfFormatado());
		manterContratoParticipantesBean.setNomeRazaoRepresentante(saidaDTO
				.getNmRazaoSocialRepresentante());
		manterContratoParticipantesBean
		.setGrupEconRepresentante(getGrupoEconomicoMaster());
		manterContratoParticipantesBean
		.setAtivEconRepresentante(getAtividadeEconomicaMaster());
		manterContratoParticipantesBean
		.setSegRepresentante(getSegmentoMaster());
		manterContratoParticipantesBean
		.setSubSegRepresentante(getSubSegmentoMaster());
		manterContratoParticipantesBean.setEmpresaContrato(getEmpresa());
		manterContratoParticipantesBean.setTipoContrato(getTipo());
		manterContratoParticipantesBean.setNumeroContrato(getNumero());
		manterContratoParticipantesBean.setSituacaoContrato(getSituacaoDesc());
		manterContratoParticipantesBean.setMotivoContrato(getMotivoDesc());
		manterContratoParticipantesBean
		.setParticipacaoContrato(getParticipacao());
		manterContratoParticipantesBean
		.setAditivosContrato(getPossuiAditivos());
		manterContratoParticipantesBean
		.setDataHoraCadastramento(getDataHoraCadastramento());
		// manterContratoParticipantesBean.setVigenciaContrato(vigenciaContrato);
		manterContratoParticipantesBean
		.setDescricaoContrato(getDescricaoContrato());

		// manterContratoParticipantesBean.setNomeRazaoParticipante(this.getIdentificacaoClienteContratoBean().getDsNomeRazaoSocialParticipante()
		// == null ? getNomeRazaoSocial() :
		// this.getIdentificacaoClienteContratoBean().getDsNomeRazaoSocialParticipante());
		// manterContratoParticipantesBean.setCpfCnpjParticipante(this.getIdentificacaoClienteContratoBean().getCpfCnpjParticipante()
		// == null ? getCpfCnpj() :
		// this.getIdentificacaoClienteContratoBean().getCpfCnpjParticipante());
		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			manterContratoParticipantesBean.setNomeRazaoParticipante("");
			manterContratoParticipantesBean.setCpfCnpjParticipante("");
		} else {
			manterContratoParticipantesBean
			.setNomeRazaoParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getDsNomeRazao());
			manterContratoParticipantesBean
			.setCpfCnpjParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
		}

		manterContratoParticipantesBean.setCdEmpresaContrato(getCdEmpresa());
		manterContratoParticipantesBean.setCdTipoContrato(getCdTipo());
		manterContratoParticipantesBean.setNrSequenciaContrato(getNumero());

		manterContratoParticipantesBean.setCdPessoaJuridica(saidaDTO.getCdPessoaJuridica());
		
		manterContratoParticipantesBean.carregaListaConsultarSemLoop();
		return "PARTICIPANTES";
	}

	/**
	 * Nome: confirmarAlterarRepresentante.
	 *
	 * @return the string
	 * @see
	 */
	public String confirmarAlterarRepresentante() {
		try {
			manterContratoRepresentanteBean.confirmarAlterar();
			identificacaoClienteContratoBean.consultarContrato();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);
			return null;
		}

		return "";
	}

	/**
	 * Nome: alterarRepresentante.
	 *
	 * @return the string
	 * @see
	 */
	public String alterarRepresentante() {

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDataHoraCadastramento(null);
		setDescricaoContrato(saidaDTO.getDsContrato());

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getDescFuncionarioBradesco());
		setCdGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco());
		setGerenteResponsavelFormatado(saidaDTO.getCdFuncionarioBradesco()
				+ " - " + saidaDTO.getDescFuncionarioBradesco());

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat(
			"yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1
						.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}

		// setInicioVigencia(inicioVigencia)

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}
		manterContratoRepresentanteBean.iniciarTela();

		manterContratoRepresentanteBean.setCpfCnpjRepresentante(saidaDTO
				.getCnpjOuCpfFormatado());
		manterContratoRepresentanteBean.setNomeRazaoRepresentante(saidaDTO
				.getNmRazaoSocialRepresentante());
		manterContratoRepresentanteBean
		.setGrupEconRepresentante(getGrupoEconomicoMaster());
		manterContratoRepresentanteBean
		.setAtivEconRepresentante(getAtividadeEconomicaMaster());
		manterContratoRepresentanteBean
		.setSegRepresentante(getSegmentoMaster());
		manterContratoRepresentanteBean
		.setSubSegRepresentante(getSubSegmentoMaster());
		manterContratoRepresentanteBean.setEmpresaContrato(getEmpresa());
		manterContratoRepresentanteBean.setTipoContrato(getTipo());
		manterContratoRepresentanteBean.setNumeroContrato(getNumero());
		manterContratoRepresentanteBean.setSituacaoContrato(getSituacaoDesc());
		manterContratoRepresentanteBean.setMotivoContrato(getMotivoDesc());
		manterContratoRepresentanteBean
		.setParticipacaoContrato(getParticipacao());
		manterContratoRepresentanteBean
		.setDescricaoContrato(getDescricaoContrato());

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			manterContratoRepresentanteBean.setCpfCnpjParticipante("");
			manterContratoRepresentanteBean.setNomeRazaoParticipante("");
		} else {
			manterContratoRepresentanteBean
			.setCpfCnpjParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getCnpjOuCpfFormatado());
			manterContratoRepresentanteBean
			.setNomeRazaoParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getDsNomeRazao() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getDsNomeRazao());
		}

		manterContratoRepresentanteBean.setCdClub(saidaDTO
				.getCdClubRepresentante());
		manterContratoRepresentanteBean.setCdEmpresaContrato(getCdEmpresa());
		manterContratoRepresentanteBean.setCdTipoContrato(getCdTipo());
		manterContratoRepresentanteBean.setNrSequenciaContrato(getNumero());

		manterContratoRepresentanteBean.carregaListaAlterar();
		//identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "REPRESENTANTE";
	}

	/**
	 * Nome: dadosBasicos.
	 *
	 * @return the string
	 * @see
	 */
	public String dadosBasicos() {

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		ConsultarDadosBasicoContratoEntradaDTO entradaDTO = new ConsultarDadosBasicoContratoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
		entradaDTO.setCdTipoContrato(saidaDTO.getCdTipoContrato());
		entradaDTO
		.setNumeroSequenciaContrato(saidaDTO.getNrSequenciaContrato());
		entradaDTO.setCdClub(saidaDTO.getCdClubRepresentante());
		entradaDTO.setCdCgcCpf(saidaDTO.getCdCpfCnpjRepresentante());
		entradaDTO
		.setCdFilialCgcCpf(saidaDTO.getCdFilialCpfCnpjRepresentante());
		entradaDTO.setCdControleCgcCpf(saidaDTO
				.getCdControleCpfCnpjRepresentante());
		// Pdc foi regerado e foi retirado esse campo - 28/03/2011
		// entradaDTO.setCdAgencia(saidaDTO.getCdAgenciaOperadora());
		ConsultarDadosBasicoContratoSaidaDTO consultarDadosBasicoSaidaDTO = getManterContratoImpl()
		.consultarDadosBasicoContrato(entradaDTO);

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getDescFuncionarioBradesco());

		manterContratoDadosBasicosBean.setCpfCnpjClienteMaster(saidaDTO
				.getCnpjOuCpfFormatado());
		manterContratoDadosBasicosBean.setNomeRazaoClienteMaster(saidaDTO
				.getNmRazaoSocialRepresentante());
		manterContratoDadosBasicosBean
		.setCdGrupoEconomicoClienteMaster(consultarDadosBasicoSaidaDTO
				.getCdGrupoEconomicoParticipante());
		manterContratoDadosBasicosBean
		.setGrupoEconomicoClienteMaster(consultarDadosBasicoSaidaDTO
				.getDsGrupoEconomico());
		manterContratoDadosBasicosBean
		.setCdAtividadeEconomicaClienteMaster(consultarDadosBasicoSaidaDTO
				.getCdAtividadeEconomicaParticipante());
		manterContratoDadosBasicosBean
		.setAtividadeEconomicaClienteMaster(consultarDadosBasicoSaidaDTO
				.getDsAtividadeEconomica());
		manterContratoDadosBasicosBean
		.setCdSegmentoClienteMaster(consultarDadosBasicoSaidaDTO
				.getCdSegmentoEconomicoParticipante());
		manterContratoDadosBasicosBean
		.setSegmentoClienteMaster(consultarDadosBasicoSaidaDTO
				.getDsSegmentoEconomico());
		manterContratoDadosBasicosBean
		.setCdSubSegmentoClienteMaster(consultarDadosBasicoSaidaDTO
				.getCdSubSegmentoEconomico());
		manterContratoDadosBasicosBean
		.setSubSegmentoClienteMaster(consultarDadosBasicoSaidaDTO
				.getDsSubSegmentoEconomico());
		manterContratoDadosBasicosBean.setCdSetorContrato(consultarDadosBasicoSaidaDTO.getCdSetorContrato());
		manterContratoDadosBasicosBean
		.setDsSetorContrato(consultarDadosBasicoSaidaDTO
				.getDsSetorContrato());

		manterContratoDadosBasicosBean
		.setCodPessoaJuridica(consultarDadosBasicoSaidaDTO
				.getCdPessoaJuridica());
		manterContratoDadosBasicosBean
		.setNumeroSequencialUnidadeOrganizacional(consultarDadosBasicoSaidaDTO
				.getNumeroSequencialUnidadeOrganizacional());
		manterContratoDadosBasicosBean.setCdClub(saidaDTO
				.getCdClubRepresentante());
		manterContratoDadosBasicosBean.setCdEmpresaGestoraContrato(saidaDTO
				.getCdPessoaJuridica());
		manterContratoDadosBasicosBean.setEmpresaGestoraContrato(PgitUtil
				.concatenarCampos(saidaDTO.getCdPessoaJuridica(), saidaDTO
						.getDsPessoaJuridica()));
		manterContratoDadosBasicosBean.setCdTipoContrato(saidaDTO
				.getCdTipoContrato());
		manterContratoDadosBasicosBean.setTipoContrato(saidaDTO
				.getDsTipoContrato());
		manterContratoDadosBasicosBean.setNumeroContrato(String
				.valueOf(saidaDTO.getNrSequenciaContrato()));
		manterContratoDadosBasicosBean
		.setVlSaldoVirtualTeste(consultarDadosBasicoSaidaDTO
				.getVlSaldoVirtualTeste());
		manterContratoDadosBasicosBean.setDescricaoContrato(saidaDTO
				.getDsContrato());
		manterContratoDadosBasicosBean
		.setCdIdiomaContrato(consultarDadosBasicoSaidaDTO.getCdIdioma());
		manterContratoDadosBasicosBean
		.setIdiomaContrato(consultarDadosBasicoSaidaDTO.getDsIdioma());
		manterContratoDadosBasicosBean
		.setCdMoedaContrato(consultarDadosBasicoSaidaDTO.getCdMoeda());
		manterContratoDadosBasicosBean
		.setMoedaContrato(consultarDadosBasicoSaidaDTO.getDsMoeda());
		manterContratoDadosBasicosBean
		.setCdOrigemContrato(consultarDadosBasicoSaidaDTO.getCdOrigem());
		manterContratoDadosBasicosBean
		.setOrigemContrato(consultarDadosBasicoSaidaDTO.getDsOrigem());
		manterContratoDadosBasicosBean
		.setPossuiAditivosContrato(consultarDadosBasicoSaidaDTO
				.getDsPossuiAditivo());

		// o f�bio de almeida, pediu no email do dia 02/07/2010 �s 18:58 para
		// pegar esses dados do campo
		// cdSituacaoContrato do contrato
		manterContratoDadosBasicosBean.setCdSituacaoContrato(saidaDTO
				.getCdSituacaoContrato());
		manterContratoDadosBasicosBean.setSituacaoContrato(saidaDTO
				.getDsSituacaoContrato());

		// o f�bio de almeida, pediu no email do dia 02/07/2010 �s 18:58 para
		// pegar esses dados do campo
		// cdSituacaoMotivoContrato da consulta de dados b�sicos
		manterContratoDadosBasicosBean
		.setCdMotivoContrato(consultarDadosBasicoSaidaDTO
				.getCdSituacaoContrato());
		manterContratoDadosBasicosBean
		.setMotivoContrato(consultarDadosBasicoSaidaDTO
				.getDsSituacaoContrato());

		manterContratoDadosBasicosBean
		.setInicioVigenciaContrato(consultarDadosBasicoSaidaDTO
				.getDataVigenciaContrato());
		manterContratoDadosBasicosBean
		.setFimVigenciaContrato(consultarDadosBasicoSaidaDTO
				.getDataCadastroContrato());
		manterContratoDadosBasicosBean
		.setNumeroComercialContrato(consultarDadosBasicoSaidaDTO
				.getNumeroComercialContrato());
		manterContratoDadosBasicosBean
		.setParticipacaoContrato(consultarDadosBasicoSaidaDTO
				.getDsTipoParticipacaoContrato());
		manterContratoDadosBasicosBean
		.setDataHoraAssinaturaContrato(consultarDadosBasicoSaidaDTO
				.getDataAssinaturaContrato());
		manterContratoDadosBasicosBean
		.setCdPermiteEncerramentoContrato(consultarDadosBasicoSaidaDTO
				.getCdIndicaPermiteEncerramento());
		manterContratoDadosBasicosBean
		.setCdOperadoraUnidOrg(consultarDadosBasicoSaidaDTO
				.getCdAgenciaOperadora());
		manterContratoDadosBasicosBean
		.setCdContabilUnidOrg(consultarDadosBasicoSaidaDTO
				.getCdAgenciaContabil());
		manterContratoDadosBasicosBean
		.setCdGestoraUnidOrg(consultarDadosBasicoSaidaDTO
				.getCdDeptoGestor());
		if (manterContratoDadosBasicosBean.getCdGestoraUnidOrg() != 0) {
			manterContratoDadosBasicosBean.setGestoraUnidOrg(PgitUtil
					.concatenarCampos(consultarDadosBasicoSaidaDTO
							.getCdDeptoGestor(), consultarDadosBasicoSaidaDTO
							.getDsDeptoGestor()));
		} else {
			manterContratoDadosBasicosBean.setGestoraUnidOrg("");
		}
		manterContratoDadosBasicosBean
		.setCdGerenteResponsavelUnidOrg(consultarDadosBasicoSaidaDTO
				.getCdFuncionarioBradesco());
		manterContratoDadosBasicosBean
		.setGerenteResponsavelUnidOrg(consultarDadosBasicoSaidaDTO
				.getCdFuncionarioBradesco()
				+ " - "
				+ consultarDadosBasicoSaidaDTO
				.getDsFuncionarioBradesco());

		manterContratoDadosBasicosBean
		.setDataHoraInclusao(PgitUtil.concatenarCampos(
				consultarDadosBasicoSaidaDTO.getDataInclusaoContrato(),
				consultarDadosBasicoSaidaDTO.getHoraInclusaoContrato()));
		manterContratoDadosBasicosBean
		.setUsuarioInclusao(consultarDadosBasicoSaidaDTO
				.getUsuarioInclusaoContrato());
		manterContratoDadosBasicosBean
		.setTipoCanalInclusao(consultarDadosBasicoSaidaDTO
				.getCdTipoCanalInclusao() == 0 ? "" : PgitUtil
						.concatenarCampos(consultarDadosBasicoSaidaDTO
								.getCdTipoCanalInclusao(),
								consultarDadosBasicoSaidaDTO
								.getDsTipoCanalInclusao()));
		manterContratoDadosBasicosBean
		.setComplementoInclusao(consultarDadosBasicoSaidaDTO
				.getOperacaoFluxoInclusao() == null
				|| consultarDadosBasicoSaidaDTO
				.getOperacaoFluxoInclusao().equals("0") ? ""
						: consultarDadosBasicoSaidaDTO
						.getOperacaoFluxoInclusao());

		manterContratoDadosBasicosBean.setDataHoraManutencao(PgitUtil
				.concatenarCampos(consultarDadosBasicoSaidaDTO
						.getDataManutencaoContrato(),
						consultarDadosBasicoSaidaDTO
						.getHoraManutencaoContrato()));
		manterContratoDadosBasicosBean
		.setUsuarioManutencao(consultarDadosBasicoSaidaDTO
				.getUsuarioManutencaoContrato());
		manterContratoDadosBasicosBean
		.setTipoCanalManutencao(consultarDadosBasicoSaidaDTO
				.getCdTipoCanalManutencao() == 0 ? "" : PgitUtil
						.concatenarCampos(consultarDadosBasicoSaidaDTO
								.getCdTipoCanalManutencao(),
								consultarDadosBasicoSaidaDTO
								.getDsTipoCanalManutencao()));
		manterContratoDadosBasicosBean
		.setComplementoManutencao(consultarDadosBasicoSaidaDTO
				.getOperacaoFluxoManutencao() == null
				|| consultarDadosBasicoSaidaDTO
				.getOperacaoFluxoManutencao().equals("0") ? ""
						: consultarDadosBasicoSaidaDTO
						.getOperacaoFluxoManutencao());
		manterContratoDadosBasicosBean
		.setDsGerente(consultarDadosBasicoSaidaDTO
				.getDsFuncionarioBradesco());

		// PREENCHE SE USU�RIO TIVER FEITO PESQUISA POR CLIENTE - Denize Almeida
		// De Brito - quarta-feira, 3 de agosto de
		// 2011 16:13

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		} else {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getDsNomeRazao());
		}

		manterContratoDadosBasicosBean.setEmpresaGestoraProposta(PgitUtil
				.concatenarCampos(consultarDadosBasicoSaidaDTO
						.getCdPessoaJuridicaProposta(),
						consultarDadosBasicoSaidaDTO
						.getDsPessoaJuridicaProposta()));
		manterContratoDadosBasicosBean.setTipoProposta(PgitUtil
				.concatenarCampos(consultarDadosBasicoSaidaDTO
						.getCdTipoContratoProposta(),
						consultarDadosBasicoSaidaDTO
						.getDsTipoContratoProposta()));
		manterContratoDadosBasicosBean
		.setNumeroProposta(consultarDadosBasicoSaidaDTO
				.getNrContratoProposta());

		manterContratoDadosBasicosBean.setNrSequenciaContratoOutros(null);
		if (consultarDadosBasicoSaidaDTO.getNrSequenciaContratoOutros() != 0) {
			manterContratoDadosBasicosBean
			.setNrSequenciaContratoOutros(consultarDadosBasicoSaidaDTO
					.getNrSequenciaContratoOutros());
		}

		manterContratoDadosBasicosBean.setNrSequenciaContratoPagamentoSalario(null);
		if (consultarDadosBasicoSaidaDTO
				.getNrSequenciaContratoPagamentoSalario() != 0) {
			manterContratoDadosBasicosBean
			.setNrSequenciaContratoPagamentoSalario(consultarDadosBasicoSaidaDTO
					.getNrSequenciaContratoPagamentoSalario());
		}

        manterContratoDadosBasicosBean.setCdFormaAutorizacaoPagamento(consultarDadosBasicoSaidaDTO
            .getCdFormaAutorizacaoPagamento());
        manterContratoDadosBasicosBean.setDsFormaAutorizacaoPagamento(consultarDadosBasicoSaidaDTO
            .getDsFormaAutorizacaoPagamento());

		return "DADOSBASICOS";

	}

	/**
	 * Nome: servicos.
	 *
	 * @return the string
	 * @see
	 */
	public String servicos() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setGerenteResponsavelFormatado(saidaDTO.getCdFuncionarioBradesco()
				+ " - " + saidaDTO.getNmFuncionarioBradesco());

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}

		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setNumeroContrato(saidaDTO.getNrSequenciaContrato());
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDescricaoContrato(saidaDTO.getDsContrato());

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat(
			"yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1
						.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		} else {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getDsNomeRazao());
		}

		this.manterContratoServicosBean.setCdPessoaJuridica(this.cdEmpresa);
		this.manterContratoServicosBean.setCdTipoContrato(this.cdTipo);
		this.manterContratoServicosBean.setNrSequenciaContrato(Long
				.parseLong(this.numero));
		manterContratoServicosBean
		.setHabilitaCampoTipServicoForncSalTrib(false);

		try{
			manterContratoServicosBean.carregaLista();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);

			manterContratoServicosBean.setPossuiModalidade(false);
			identificacaoClienteContratoBean.setItemSelecionadoLista(null);
			return "";
		}

		manterContratoServicosBean.setPossuiModalidade(false);

		return "SERVICOS";
	}

	/**
	 * Imprimir resumo proposta.
	 */
	public void imprimirResumoProposta(){
		ListarContratosPgitSaidaDTO contratosPgitSelecionado = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

		ImprimirAnexoPrimeiroContratoEntradaDTO entradaPrimeiroContrato = new ImprimirAnexoPrimeiroContratoEntradaDTO();
		entradaPrimeiroContrato.setCdPessoaJuridicaContrato(contratosPgitSelecionado.getCdPessoaJuridica());
		entradaPrimeiroContrato.setCdTipoContratoNegocio(contratosPgitSelecionado.getCdTipoContrato());
		entradaPrimeiroContrato.setNrSequenciaContratoNegocio(contratosPgitSelecionado.getNrSequenciaContrato());

		saidaPrimeiroContrato = getImprimirAnexoPrimeiroContratoService()
		.imprimirAnexoPrimeiroContrato(entradaPrimeiroContrato);

		ImprimirAnexoSegundoContratoEntradaDTO entradaSegundoContrato = new ImprimirAnexoSegundoContratoEntradaDTO();
		entradaSegundoContrato.setCdPessoaJuridicaContrato(contratosPgitSelecionado.getCdPessoaJuridica());
		entradaSegundoContrato.setCdTipoContratoNegocio(contratosPgitSelecionado.getCdTipoContrato());
		entradaSegundoContrato.setNrSequenciaContratoNegocio(contratosPgitSelecionado.getNrSequenciaContrato());

		saidaSegundoContrato = getImprimirAnexoSegundoContratoService()
		.imprimirAnexoPrimeiroContrato(entradaSegundoContrato);

		ConsultarListaPerfilTrocaArquivoEntradaDTO entrada = new ConsultarListaPerfilTrocaArquivoEntradaDTO();
		saidaMeioTransmissao = identificacaoClienteContratoBean.getListaGridPesquisa().get(
				identificacaoClienteContratoBean.getItemSelecionadoLista());

		entrada.setCdPerfilTrocaArquivo(0L);
		entrada.setCdTipoLayoutArquivo(0);
		entrada.setCdpessoaJuridicaContrato(saidaMeioTransmissao.getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(saidaMeioTransmissao.getCdTipoContrato());
		entrada.setNrSequenciaContratoNegocio(saidaMeioTransmissao.getNrSequenciaContrato());

		saidaListaPerfilTrocaArquivo = getManterPerfilTrocaArqLayoutServiceImpl()
		.consultarListaPerfilTrocaArquivo(entrada);

	}


	/**
	 * Nome: tarifas.
	 *
	 * @return the string
	 * @see
	 */
	public String tarifas() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setGerenteResponsavelFormatado(saidaDTO.getCdFuncionarioBradesco()
				+ " - " + saidaDTO.getNmFuncionarioBradesco());

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());

		setDataHoraCadastramento(null);

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat(
			"yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1
						.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}

		// setInicioVigencia(inicioVigencia)

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		} else {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getDsNomeRazao());
		}

		manterContratoTarifasBean.iniciarTela();
		manterContratoTarifasBean.setCpfCnpjRepresentante(getCpfCnpjMaster());
		manterContratoTarifasBean
		.setNomeRazaoRepresentante(getNomeRazaoSocialMaster());
		manterContratoTarifasBean
		.setGrupEconRepresentante(getGrupoEconomicoMaster());
		manterContratoTarifasBean
		.setAtivEconRepresentante(getAtividadeEconomicaMaster());
		manterContratoTarifasBean.setSegRepresentante(getSegmentoMaster());
		manterContratoTarifasBean
		.setSubSegRepresentante(getSubSegmentoMaster());
		manterContratoTarifasBean.setEmpresaContrato(getEmpresa());
		manterContratoTarifasBean.setTipoContrato(getTipo());
		manterContratoTarifasBean.setNumeroContrato(getNumero());
		manterContratoTarifasBean
		.setDescricaoContrato(saidaDTO.getDsContrato());
		manterContratoTarifasBean.setSituacaoContrato(getSituacaoDesc());
		manterContratoTarifasBean.setMotivoContrato(getMotivoDesc());
		manterContratoTarifasBean.setParticipacaoContrato(getParticipacao());

		manterContratoTarifasBean.setCodPessoaJuridica(getCdEmpresa());
		manterContratoTarifasBean.setCdTipoContrato(getCdTipo());
		manterContratoTarifasBean.setNrSequenciaContrato(Long
				.parseLong(getNumero()));

		try{

			manterContratoTarifasBean.carregaListaConsultar();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);

			//manterContratoServicosBean.setPossuiModalidade(false);
			identificacaoClienteContratoBean.setItemSelecionadoLista(null);
			return "";
		}

		//identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "TARIFAS";
	}

    /**
     * Imprimir contrato.
     */
    public void imprimirContrato() {

        contratoBytes = new ByteArrayOutputStream();

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String pathRel = servletContext.getRealPath("/");

        ListarContratosPgitSaidaDTO contratoPgitSelecionado = identificacaoClienteContratoBean.getListaGridPesquisa().get(
                identificacaoClienteContratoBean.getItemSelecionadoLista());
        try {
            getContratoServiceImpl().imprimirContratoPrestacaoServico(contratoPgitSelecionado.getCdTipoContrato(),
                contratoPgitSelecionado.getCdPessoaJuridica(), contratoPgitSelecionado.getNrSequenciaContrato(), 0L, contratoBytes, pathRel);
        } catch (Exception e) {
            BradescoCommonServiceFactory.getLogManager().error(this.getClass(), e.getMessage(), e);
            contratoException = e;
            contratoBytes = null;
        }
    }
    
    /**
     * Imprimir contrato final.
     * 
     * @return the string
     *
     */
    public String imprimirContratoFinal() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        if (contratoBytes != null) {
            try {
                response.getOutputStream().write(contratoBytes.toByteArray());
                response.setHeader("Content-Disposition", "attachment; filename=contrato.pdf");
                response.setContentType("application/pdf");

                facesContext.renderResponse();
                facesContext.responseComplete();
            } catch (Exception e) {
                throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
            }
        } else {
            if (contratoException instanceof PdcAdapterFunctionalException) {
                BradescoFacesUtils.addErrorModalMessage(contratoException.getMessage());
            } else {
                BradescoFacesUtils.addErrorModalMessage(MessageHelperUtils
                    .getI18nMessage(IContratoServiceConstants.ERRO_IMPRESSAO_CONTRATO));
            }
        }

        contratoException = null;
        contratoBytes = null;

        return "";
    }

	/**
	 * Nome: renegociar.
	 *
	 * @see
	 */
	public void renegociar() {

		contratoPgitSelecionado = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		/*
		 * ExternalContext externalContext =
		 * FacesContext.getCurrentInstance().getExternalContext();
		 * 
		 * HttpServletRequest httpServletRequest = (HttpServletRequest)
		 * externalContext.getRequest();
		 * 
		 * String url = externalContext.encodeResourceURL("http://" +
		 * httpServletRequest
		 * .getRemoteHost()+"/pgit_negociacao/proposta.renegociacao.jsf?" +
		 * "nseqContrNegoc=" + contratoPgitSelecionado.getNrSequenciaContrato()
		 * +"?" + "cpssoaJuridContr=" +
		 * contratoPgitSelecionado.getCdPessoaJuridica() + "?" +
		 * "ctpoContrNegoc=" + contratoPgitSelecionado.getCdTipoContrato() );
		 * 
		 * 
		 * 
		 * 
		 * try { externalContext.redirect(url); } catch (Exception error) {
		 * error.printStackTrace(); }
		 */

	}

	/**
	 * Nome: verificarRenegociavel.
	 *
	 * @see
	 */
	public void verificarRenegociavel() {
		try {
			ListarContratosPgitSaidaDTO contrato = identificacaoClienteContratoBean
			.getListaGridPesquisa().get(
					identificacaoClienteContratoBean
					.getItemSelecionadoLista());
			ListaOperacaoTarifaTipoServicoEntradaDTO entradaDTO = new ListaOperacaoTarifaTipoServicoEntradaDTO();
			ValidarPropostaAndamentoEntradaDTO entrada = new ValidarPropostaAndamentoEntradaDTO();

			entradaDTO.setCdPessoaContrato(contrato.getCdPessoaJuridica());
			entradaDTO.setCdTipoContrato(contrato.getCdTipoContrato());
			entradaDTO
			.setNrSequenciaContrato(contrato.getNrSequenciaContrato());
			entradaDTO.setCdNegocioRenegocio(2);
			entradaDTO.setNumeroOcorrencias(1);

			entrada.setCdpessoaJuridicaContrato(contrato.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(contrato.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(contrato
					.getNrSequenciaContrato());

			if (identificacaoClienteContratoBean != null
					&& identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas() != null) {
				entrada.setCdControleCpfPssoa(PgitUtil
						.verificaIntegerNulo(identificacaoClienteContratoBean
								.getSaidaConsultarListaClientePessoas()
								.getCdControleCnpj()));
				entrada.setCdCpfCnpjPssoa(PgitUtil
						.verificaLongNulo(identificacaoClienteContratoBean
								.getSaidaConsultarListaClientePessoas()
								.getCdCpfCnpj()));
				entrada.setCdFilialCnpjPssoa(PgitUtil
						.verificaIntegerNulo(identificacaoClienteContratoBean
								.getSaidaConsultarListaClientePessoas()
								.getCdFilialCnpj()));
			}

			manterContratoImpl.validarPropostaAndamento(entrada);

			ListaOperacaoTarifaTipoServicoSaidaDTO saida = manterContratoImpl
			.consultarListaOperacaoTarifaTipoServico(entradaDTO);
			setRenegociavel("S");

			if ("PGIT0003".equalsIgnoreCase(saida.getCdMensagem())) {
				setRenegociavel("N");
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(saida.getCdMensagem(), 8) + ") "
						+ saida.getMensagem(), false);
			}
		} catch (PdcAdapterFunctionalException e) {
			if ("PGIT0003".equalsIgnoreCase(e.getCode())) {
				setRenegociavel("N");
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(e.getCode(), 8) + ") "
						+ e.getMessage(), false);
			}

			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(e.getCode(), 8) + ") "
					+ e.getMessage(), false);
		}
	}

	/**
	 * Nome: verificarRenegociavelNovo.
	 *
	 * @see
	 */
	public void verificarRenegociavelNovo() {
		try {
			ListarContratosPgitSaidaDTO contrato = identificacaoClienteContratoBean
			.getListaGridPesquisa().get(
					identificacaoClienteContratoBean
					.getItemSelecionadoLista());
			ListaOperacaoTarifaTipoServicoEntradaDTO entradaDTO = new ListaOperacaoTarifaTipoServicoEntradaDTO();
			ValidarPropostaAndamentoEntradaDTO entrada = new ValidarPropostaAndamentoEntradaDTO();

			entradaDTO.setCdPessoaContrato(contrato.getCdPessoaJuridica());
			entradaDTO.setCdTipoContrato(contrato.getCdTipoContrato());
			entradaDTO
			.setNrSequenciaContrato(contrato.getNrSequenciaContrato());
			entradaDTO.setCdNegocioRenegocio(2);
			entradaDTO.setNumeroOcorrencias(1);

			entrada.setCdpessoaJuridicaContrato(contrato.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(contrato.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(contrato
					.getNrSequenciaContrato());

			if (identificacaoClienteContratoBean != null
					&& identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas() != null) {
				entrada.setCdControleCpfPssoa(PgitUtil
						.verificaIntegerNulo(identificacaoClienteContratoBean
								.getSaidaConsultarListaClientePessoas()
								.getCdControleCnpj()));
				entrada.setCdCpfCnpjPssoa(PgitUtil
						.verificaLongNulo(identificacaoClienteContratoBean
								.getSaidaConsultarListaClientePessoas()
								.getCdCpfCnpj()));
				entrada.setCdFilialCnpjPssoa(PgitUtil
						.verificaIntegerNulo(identificacaoClienteContratoBean
								.getSaidaConsultarListaClientePessoas()
								.getCdFilialCnpj()));
			}

			saidaValidar = manterContratoImpl.validarPropostaAndamento(entrada);

			ListaOperacaoTarifaTipoServicoSaidaDTO saida = manterContratoImpl
			.consultarListaOperacaoTarifaTipoServico(entradaDTO);
			setRenegociavel("S");
			criarUrlRenegociarNovo();

			if ("PGIT0003".equalsIgnoreCase(saida.getCdMensagem())) {
				setRenegociavel("N");
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(saida.getCdMensagem(), 8) + ") "
						+ saida.getMensagem(), false);
			}
		} catch (PdcAdapterFunctionalException e) {
			if ("PGIT0003".equalsIgnoreCase(e.getCode())) {
				setRenegociavel("N");
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(e.getCode(), 8) + ") "
						+ e.getMessage(), false);
			}

			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(e.getCode(), 8) + ") "
					+ e.getMessage(), false);
		}
	}

	/**
	 * Nome: criarUrlRenegociarNovo.
	 */
	public void criarUrlRenegociarNovo() {
		ListarContratosPgitSaidaDTO contrato = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		StringBuilder url = new StringBuilder();
		url.append("/fneb/roteador.aditamento.jsf?empresas=");
		url.append(contrato.getCdPessoaJuridica());
		url.append("&codClub=");
		url.append(contrato.getCdClubRepresentante());
		url.append("&cpssoaJuridContr=");
		url.append(contrato.getCdPessoaJuridica());
		url.append("&ctpoContrNegoc=");
		url.append(contrato.getCdTipoContrato());
		url.append("&nseqContrNegoc=");
		url.append(contrato.getNrSequenciaContrato());
		url.append("&cprodtServcOper=");
		url.append(saidaValidar.getCdProdutoServicoOperacao());
		url.append("&cSist=");
		url.append("PGIT");
		url.append("&ctpoPpsta=");
		url.append("33");
		url.append("&indGerarProposta=");
		url.append("1");

		setUrlRenegociacaoNovo(url.toString());
	}

	/**
	 * Nome: pesquisar.
	 *
	 * @param evt the evt
	 * @see
	 */
	public void pesquisar(ActionEvent evt) {
		meioTransmissao();
	}

	/**
	 * Paginar tabela participante.
	 *
	 * @param event the event
	 */
	public void paginarTabelaParticipante(ActionEvent event){
		imprimirContratoPrimeiroAnexo();
	}

	/**
	 * Paginar tabela contas.
	 *
	 * @param event the event
	 */
	public void paginarTabelaContas(ActionEvent event){
		imprimirContratoPrimeiroAnexo();
	}

	/**
	 * Paginar configuracoes layout.
	 *
	 * @param event the event
	 */
	public void paginarConfiguracoesLayout(ActionEvent event){
		imprimirContratoPrimeiroAnexo();
	}

	/**
	 * Paginar tabela tarifas.
	 *
	 * @param event the event
	 */
	public void paginarTabelaTarifas(ActionEvent event){
		listarGridTafifasModalidade();
	}

	/**
	 * Resumo contrato.
	 *
	 * @return the string
	 */
	public String resumoContrato(){	
		setOmiteServicosResumoContrato(true);
		listarGridTafifasModalidade();
		listarLayoutConfigura��es();
		carregarListasParaAnexoContrato();
		imprimirContratoPrimeiroAnexoLoop();
		setImprimirGridTarifa("S");
		
		carregaListaParticipantes();		
		carregaListaContas();
		return "RESUMO_CONTRATO";
	}
	
    private void carregaListaParticipantes() {
        try {
            ListarContratosPgitSaidaDTO contratosPgitSelecionado =
                identificacaoClienteContratoBean.getListaGridPesquisa().get(
                    identificacaoClienteContratoBean.getItemSelecionadoLista());

            ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();

            entradaDTO.setMaxOcorrencias(20);
            entradaDTO.setCodPessoaJuridica(contratosPgitSelecionado.getCdPessoaJuridica());
            entradaDTO.setCodTipoContrato(contratosPgitSelecionado.getCdTipoContrato());
            entradaDTO.setNrSequenciaContrato(contratosPgitSelecionado.getNrSequenciaContrato());

            List<ListarParticipantesSaidaDTO> listaSaida = getManterContratoImpl().listarParticipantes(entradaDTO);

            ArrayList<OcorrenciasParticipantes> listaOcorrencias =
                new ArrayList<OcorrenciasParticipantes>(listaSaida.size());

            for (int i = 0; i < listaSaida.size(); i++) {
                ListarParticipantesSaidaDTO itemLista = listaSaida.get(i);
                OcorrenciasParticipantes ocorrencia = new OcorrenciasParticipantes();

				ocorrencia.setCdCpfCnpjParticipante(CpfCnpjUtils.formatarCpfCnpj(itemLista.getCdCpfCnpj(), itemLista
								.getCdFilialCnpj(), itemLista.getCdControleCnpj()));
                ocorrencia.setNmParticipante(itemLista.getNmRazao());
                ocorrencia.setCdGrupoEconomicoParticipante(itemLista.getCdGrupoEconomico());
                ocorrencia.setDsGrupoEconomicoParticipante(itemLista.getDsGrupoEconomico());

                listaOcorrencias.add(ocorrencia);
            }

            setListaParticipantes(listaOcorrencias);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
        }
    }

    
    private void carregaListaContas() {
        try {
            ListarContratosPgitSaidaDTO contratosPgitSelecionado =
                identificacaoClienteContratoBean.getListaGridPesquisa().get(
                    identificacaoClienteContratoBean.getItemSelecionadoLista());

            ListarContasVincContEntradaDTO entradaDTO = new ListarContasVincContEntradaDTO();

            entradaDTO.setCdPessoaJuridicaNegocio(contratosPgitSelecionado.getCdPessoaJuridica());
            entradaDTO.setCdTipoContratoNegocio(contratosPgitSelecionado.getCdTipoContrato());
            entradaDTO.setNrSequenciaContratoNegocio(contratosPgitSelecionado.getNrSequenciaContrato());

            List<ListarContasVincContSaidaDTO> listaSaida = 
                getManterContratoImpl().listarContasVinculadasContrato(entradaDTO);

            ArrayList<OcorrenciasContas> listaOcorrencias =
                new ArrayList<OcorrenciasContas>(listaSaida.size());

            for (int i = 0; i < listaSaida.size(); i++) {
                ListarContasVincContSaidaDTO itemLista = listaSaida.get(i);
                OcorrenciasContas ocorrencia = new OcorrenciasContas();

                ocorrencia.setCdCpfCnpjConta(itemLista.getCdCpfCnpj());
                ocorrencia.setCdNomeParticipanteConta(itemLista.getNomeParticipante());
                ocorrencia.setCdBancoConta(itemLista.getCdBanco());
                ocorrencia.setCdAgenciaConta(itemLista.getCdAgencia());
                ocorrencia.setDigitoAgenciaConta(itemLista.getCdDigitoAgencia().toString());
                ocorrencia.setCdContaConta(itemLista.getCdConta());
                ocorrencia.setDigitoContaConta(itemLista.getCdDigitoConta());
                ocorrencia.setDsTipoConta(itemLista.getDsTipoConta());
                
                
                listaOcorrencias.add(ocorrencia);
            }

            setListaContas(listaOcorrencias);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
        }
    } 
    
    
    
    

	/**
	 * Listar grid tafifas modalidade.
	 */
	public void listarGridTafifasModalidade(){
		listaGridTarifasModalidade = new ArrayList<ConsultarTarifaContratoSaidaDTO>();	


		try {
			ConsultarTarifaContratoEntradaDTO entradaTarifa = new ConsultarTarifaContratoEntradaDTO();
			entradaTarifa.setCdPessoaJuridica(identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getCdPessoaJuridica());
			entradaTarifa.setCdTipoContrato(identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getCdTipoContrato());
			entradaTarifa.setNrSequenciaContrato(identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getNrSequenciaContrato());	
			setListaGridTarifasModalidade(getManterContratoImpl().consultarTarifaContratoLoop(entradaTarifa));		

		} catch (PdcAdapterFunctionalException e) {
		    BradescoCommonServiceFactory.getLogManager().error(e.getMessage(), e);
//			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
		}

	}	

	/**
	 * Voltar resumo.
	 *
	 * @return the string
	 */
	public String voltarResumo(){
		//identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		setImprimirGridTarifa("N");
		return "VOLTAR";
	}

	/**
	 * Listar layout configura��es.
	 */
	public void listarLayoutConfigura��es(){
		ConsultarListaPerfilTrocaArquivoEntradaDTO entrada = new ConsultarListaPerfilTrocaArquivoEntradaDTO();
		saidaMeioTransmissao = identificacaoClienteContratoBean.getListaGridPesquisa().get(
				identificacaoClienteContratoBean.getItemSelecionadoLista());

		entrada.setCdPerfilTrocaArquivo(0L);
		entrada.setCdTipoLayoutArquivo(0);
		entrada.setCdpessoaJuridicaContrato(saidaMeioTransmissao
				.getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(saidaMeioTransmissao
				.getCdTipoContrato());
		entrada.setNrSequenciaContratoNegocio(saidaMeioTransmissao
				.getNrSequenciaContrato());

		try {
		    meiosTransmissaoBean.setListaMeioTransmissao(getManterPerfilTrocaArqLayoutServiceImpl()
		        .consultarListaPerfilTrocaArquivo(entrada));
		    
		} catch (PdcAdapterFunctionalException e) {
		    
		    String codMensagem = StringUtils.right(e.getCode(), 8);
		    if(not(codMensagem.equalsIgnoreCase("PGIT0003"))){
		        throw e;
		        
		    }
        }

	}

	/**
	 * Carrega lista layout.
	 */
	private void carregaListaLayout(){

		listaLayouts = new ArrayList<OcorrenciasLayout>();
		List <ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO> layout = 
			meiosTransmissaoBean.getListaMeioTransmissao().getOcorrencia();

		for (int i = 0; i < layout.size(); i++) {
			OcorrenciasLayout ocorrencia = new OcorrenciasLayout(); 
			ocorrencia.setCdLayout(layout.get(i).getDsTipoLayoutArquivo());
			ocorrencia.setCdPerfilTrocaArq(layout.get(i).getCdPerfilTrocaArquivo());
			ocorrencia.setDsAplicativoTransmissao(layout.get(i).getDsAplicacaoTransmicaoPagamento());
			ocorrencia.setDsMeioTransmissao(layout.get(i).getDsMeioPrincRemss());
			ocorrencia.setDsServicoLayout(layout.get(i).getDsProdutoServico());

			listaLayouts.add(ocorrencia);
		}
	}


	/**
	 * Nome: meioTransmissao.
	 *
	 * @return the string
	 * @see
	 */
	public String meioTransmissao() {
		ConsultarListaPerfilTrocaArquivoEntradaDTO entrada = new ConsultarListaPerfilTrocaArquivoEntradaDTO();
		saidaMeioTransmissao = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		entrada.setCdPerfilTrocaArquivo(0L);
		entrada.setCdTipoLayoutArquivo(0);
		entrada.setCdpessoaJuridicaContrato(saidaMeioTransmissao
				.getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(saidaMeioTransmissao
				.getCdTipoContrato());
		entrada.setNrSequenciaContratoNegocio(saidaMeioTransmissao
				.getNrSequenciaContrato());

		meiosTransmissaoBean
		.setListaMeioTransmissao(getManterPerfilTrocaArqLayoutServiceImpl()
				.consultarListaPerfilTrocaArquivo(entrada));

		meiosTransmissaoBean.setListaGridMeioTransmissao(meiosTransmissaoBean
				.getListaMeioTransmissao().getOcorrencia());

		meiosTransmissaoBean
		.setListaControleMeioTransmissao(new ArrayList<SelectItem>());

		meiosTransmissaoBean.setContratoSelecionado(saidaMeioTransmissao);

		int size = meiosTransmissaoBean.getListaGridMeioTransmissao().size();

		for (int i = 0; i < size; i++) {
			meiosTransmissaoBean.getListaControleMeioTransmissao().add(
					new SelectItem(i, ""));
		}

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		} else {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
							: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getDsNomeRazao());
		}

		setDsGerenteResponsavel(saidaMeioTransmissao.getCdFuncionarioBradesco()
				+ " - " + saidaMeioTransmissao.getNmFuncionarioBradesco());
		meiosTransmissaoBean.setItemSelecionadoMeioTransmissao(null);

		return "MEIO_TRANSMISSAO";
	}

	/**
	 * Nome: layouts.
	 *
	 * @return the string
	 * @see
	 */
	public String layouts() {
		// Layouts

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDescricaoContrato(saidaDTO.getDsContrato());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - "
				+ saidaDTO.getNmFuncionarioBradesco());

		setDataHoraCadastramento(null);

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat(
			"yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1
						.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}
		}

		// setInicioVigencia(inicioVigencia)
		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}

		// Verifica se consulta e feita por contrato, se nao for, mostra dados
		// participante.
		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			setCpfCnpjParticipante("");
			setDsNomeRazaoSocialParticipante("");
		} else {
			setCpfCnpjParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setDsNomeRazaoSocialParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}

		manterContratoLayoutsArquivosBean.setCdPessoaJuridica(getCdEmpresa());
		manterContratoLayoutsArquivosBean.setCdTipoContratoNegocio(getCdTipo());
		manterContratoLayoutsArquivosBean
		.setNrSequenciaContratoNegocio(getNumero());

		try{

			manterContratoLayoutsArquivosBean.carregaListaLayout();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);

			identificacaoClienteContratoBean.setItemSelecionadoLista(null);
			manterContratoLayoutsArquivosBean.setListaLayout(new ArrayList<ConsultarListaLayoutArquivoContratoSaidaDTO>());
			return "";
		}

		//identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		validarUsuario();

		return "LAYOUT";
	}

	/**
	 * Nome: validarUsuario.
	 *
	 * @see
	 */
	public void validarUsuario() {
		setEntradaValidarUsuario(new ValidarUsuarioEntradaDTO());

		try {
			entradaValidarUsuario.setMaxOcorrencias(PgitUtil.verificaIntegerNulo(0));
			saidaValidarUsuario = manterContratoImpl.validarUsuario(entradaValidarUsuario);

			Integer cdAcessoDepartamento = saidaValidarUsuario.getCdAcessoDepartamento();
			
			if(cdAcessoDepartamento != null){
			    if(cdAcessoDepartamento == 1){
	                setRestringirAcessoBotoes(false);
	            }else if(cdAcessoDepartamento == 2){
	                setRestringirAcessoBotoes(true);
	            }
			}

			getManterContratoLayoutsArquivosBean().setCdAcessoDepartamento(cdAcessoDepartamento);
			
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("("
					+ StringUtils.right(e.getCode(), 8) + ") "
					+ e.getMessage(), false);
		}

	}

	/**
	 * Nome: carregarParametros.
	 *
	 * @return the string
	 * @see
	 */
	public String carregarParametros() {
		iniciarPagina(null);

		Long empresaGestora = 0l;
		Integer tipoContrato = 0;

		try {
			empresaGestora = Long.parseLong(PgitUtil
					.verificaStringNula((String) FacesUtils
							.getRequestParameter("empresaGestora")));
		} catch (NumberFormatException e) {
			empresaGestora = 0L;
		}

		try {
			tipoContrato = Integer.parseInt(PgitUtil
					.verificaStringNula((String) FacesUtils
							.getRequestParameter("tipoContrato")));
		} catch (NumberFormatException e) {
			tipoContrato = 0;
		}

		String numeroContrato = PgitUtil.verificaStringNula((String) FacesUtils
				.getRequestParameter("numeroContrato"));

		if (empresaGestora == 0L || tipoContrato == 0
				|| numeroContrato.equals("0") || numeroContrato.equals("")) {
			identificacaoClienteContratoBean.setEmpresaGestoraFiltro(0l);
			identificacaoClienteContratoBean.setTipoContratoFiltro(0);
			identificacaoClienteContratoBean.setNumeroFiltro("");
			return "conManterContrato";
		}

		identificacaoClienteContratoBean
		.setEmpresaGestoraFiltro(empresaGestora);
		identificacaoClienteContratoBean.setTipoContratoFiltro(tipoContrato);
		identificacaoClienteContratoBean.setNumeroFiltro(numeroContrato);

		identificacaoClienteContratoBean.consultarContrato();

		return "conManterContrato";
	}

	/**
	 * Nome: imprimirRelatorioLayout.
	 *
	 * @return the string
	 * @see
	 */
	public String imprimirRelatorioLayout() {

		String caminho = "/images/Bradesco_logo.JPG";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext
		.getExternalContext().getContext();
		String logoPath = servletContext.getRealPath(caminho);

		// Par�metros do Relat�rio
		Map<String, Object> par = new HashMap<String, Object>();

		par.put("tipo", getTipo());
		par.put("empresa", getEmpresa());
		par.put("numero", getNumero());
		par.put("descContrato", getDescricaoContrato());
		par.put("logoBradesco", logoPath);

		par.put("titulo", "Pagamento Integrado - Layout");

		try {

			// M�todo que gera o relat�rio PDF.
			PgitUtil.geraRelatorioPdf(
					"/relatorios/manutencaoContrato/manterContrato/layout",
					"imprimirLayoutArquivo", manterContratoLayoutsArquivosBean
					.getListaLayout(), par);

		} /* Exce��o do relat�rio */
		catch (Exception e) {
			throw new BradescoViewException(e.getMessage(), e, "", "",
					BradescoViewExceptionActionType.ACTION);
		}

		return "";
	}

	/**
	 * Nome: imprimirAnexoContratoTelaResumo.
	 *
	 * @see
	 */	


	private void imprimirContratoSegundoAnexo(){

		ListarContratosPgitSaidaDTO contratosPgitSelecionado = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		ImprimirAnexoSegundoContratoEntradaDTO entradaSegundoContrato = new ImprimirAnexoSegundoContratoEntradaDTO();	

		entradaSegundoContrato
		.setCdPessoaJuridicaContrato(contratosPgitSelecionado
				.getCdPessoaJuridica());
		entradaSegundoContrato
		.setCdTipoContratoNegocio(contratosPgitSelecionado
				.getCdTipoContrato());
		entradaSegundoContrato
		.setNrSequenciaContratoNegocio(contratosPgitSelecionado
				.getNrSequenciaContrato());
		saidaSegundoContrato = getImprimirAnexoSegundoContratoService()
		.imprimirAnexoPrimeiroContrato(entradaSegundoContrato);
	}

	/**
	 * Imprimir contrato primeiro anexo.
	 */
	private void imprimirContratoPrimeiroAnexo(){		
		saidaPrimeiroContrato = getImprimirAnexoPrimeiroContratoService()
		.imprimirAnexoPrimeiroContrato(popularEntradaPrimeiroAnexo());
	}


	/**
	 * Imprimir contrato primeiro anexo loop.
	 */
	private void imprimirContratoPrimeiroAnexoLoop(){
		saidaPrimeiroContratoLoop = getImprimirAnexoPrimeiroContratoService()
		.imprimirAnexoPrimeiroContratoLoop(popularEntradaPrimeiroAnexo());
	}

	/**
	 * Popular entrada primeiro anexo.
	 *
	 * @return the imprimir anexo primeiro contrato entrada dto
	 */
	private ImprimirAnexoPrimeiroContratoEntradaDTO popularEntradaPrimeiroAnexo(){

		ImprimirAnexoPrimeiroContratoEntradaDTO entradaPrimeiroContrato = new ImprimirAnexoPrimeiroContratoEntradaDTO();

		ListarContratosPgitSaidaDTO contratosPgitSelecionado = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());
		entradaPrimeiroContrato
		.setCdPessoaJuridicaContrato(contratosPgitSelecionado
				.getCdPessoaJuridica());
		entradaPrimeiroContrato
		.setCdTipoContratoNegocio(contratosPgitSelecionado
				.getCdTipoContrato());
		entradaPrimeiroContrato
		.setNrSequenciaContratoNegocio(contratosPgitSelecionado
				.getNrSequenciaContrato());

		return entradaPrimeiroContrato;
	}

	/**
	 * Nome: imprimirAnexoContratoComListaCarregada.
	 *
	 * @throws IOException the IO exception
	 * @see
	 */

	public void imprimirAnexoContratoComListaCarregada() throws IOException{
		carregarListasParaAnexoContrato();		
		imprimirAnexoContrato();
	}

	/**
	 * Carregar listas para anexo contrato.
	 */
	public void carregarListasParaAnexoContrato(){
		//Carrega a lista do anexo primeiro contrato
		imprimirContratoPrimeiroAnexo();

		//Carrega a lista do anexo segundo contrato
		imprimirContratoSegundoAnexo();
		
		//M�todo para carregar m�dulo lista de Cestas de Tarifas
		carregarListaCestasTarifas();

		AnexoContratoDTO anexoContratoDTO = new AnexoContratoDTO();
		try {
			BeanUtils.copyProperties(anexoContratoDTO, saidaPrimeiroContrato);
			BeanUtils.copyProperties(anexoContratoDTO, saidaSegundoContrato);
			anexoContratoDTO.setQtCestasTarifas(saidaCestasTarifasDTO.getNumLinhas());
			anexoContratoDTO.setListaCestasTarifas(saidaCestasTarifasDTO.getListaOcorrenciasSaidaDTO());

		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		listaAnexoContrato = new ArrayList<AnexoContratoDTO>();
		listaAnexoContrato.add(anexoContratoDTO);		

		//M�todo para carregar m�dulo lista de Participantes
		carregarDadosDoParticipante(anexoContratoDTO);

		//M�todo para carregar m�dulo lista de Contas
		carregarDadosContas(anexoContratoDTO);

		//M�todo para carregar m�dulo lista de Configura��o de Layout
		if(isNotNull(meiosTransmissaoBean.getListaMeioTransmissao())){
		    carregaListaLayout();
		}

		//M�todo para carregar m�dulo de pagmaento: DTO de pagamentos, Fornecedores, Salarios, Beneficios e Tributos.
		carregarDadosDoPagamento();

		//M�todo para carregar lista de Servi�os
		carregarDadosListaServicos();	
	}

	/**
	 * Carregar dados do participante.
	 *
	 * @param anexoContratoDTO the anexo contrato dto
	 */
	private void carregarDadosDoParticipante(AnexoContratoDTO anexoContratoDTO){

		listaParticipantes = new ArrayList<OcorrenciasParticipantes>();
		int qtdeParticipantes = 0;

		if (anexoContratoDTO.getQtdeParticipante() != 0) {
			for (OcorrenciasParticipantes ocorrenciasParticipantes : anexoContratoDTO
					.getListaParticipantes()) {
				listaParticipantes.add(ocorrenciasParticipantes);
				qtdeParticipantes++;
				if (qtdeParticipantes == anexoContratoDTO.getQtdeParticipante()) {
					break;
				}
			}			
		}
	}

	/**
	 * Carregar dados contas.
	 *
	 * @param anexoContratoDTO the anexo contrato dto
	 */
	private void carregarDadosContas(AnexoContratoDTO anexoContratoDTO){

		listaContas = new ArrayList<OcorrenciasContas>();
		int qtdeContas = 0;

		if (anexoContratoDTO.getQtdeConta() != 0) {
			for (OcorrenciasContas ocorrenciasContas : anexoContratoDTO
					.getListaContas()) {
				ocorrenciasContas.setDigitoContaConta(ocorrenciasContas
						.getDigitoContaConta().substring(7, 15));
				listaContas.add(ocorrenciasContas);
				qtdeContas++;
				if (qtdeContas == anexoContratoDTO.getQtdeConta()) {
					break;
				}
			}			
		}
	}

	/**
	 * Carregar dados do pagamento.
	 */
	private void carregarDadosDoPagamento(){

		List<PagamentosDTO> listaFornecTitulos = new ArrayList<PagamentosDTO>();
		List<PagamentosDTO> listaFornecModularidades = new ArrayList<PagamentosDTO>();
		List<PagamentosDTO> listaFornecOutrasModularidades = new ArrayList<PagamentosDTO>();

		PagamentosDTO pagamentosFornecedoresDTO = new PagamentosDTO();		
		pagamentosFornecedoresDTO.setDsFtiTipoTitulo("N");
		pagamentosFornecedoresDTO.setDsModTipoServico("N");
		pagamentosFornecedoresDTO.setDsModTipoOutrosServico("N");
		exibePgtoFornecedor = "N";

		List<ServicosModalidadesDTO> listaServModalidades = new ArrayList<ServicosModalidadesDTO>();
		ServicosModalidadesDTO servicosModalidadesDTO = null;

		boolean achouCreditoConta = false;
		boolean achouBradesco = false;
		boolean achouOutros = false;
		boolean achouDOC = false;
		boolean achouTED = false;
		boolean achouDC = false;
		boolean achouOC = false;
		boolean achouDI = false;
		boolean achouOrdemPagamento = false;

		// Pagamento de fornecedores
		int exibeLinhaFornecedor = saidaSegundoContrato.getQtModalidadeFornecedor() / 2;
		pgtoFornecedor: for (int i = 0; i < saidaSegundoContrato.getQtModalidadeFornecedor(); i++) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico(saidaSegundoContrato.getCdServicoFornecedor());
			servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato.getListModFornecedores().get(i)
					.getDsModalidadeFornecedor());

			if (saidaSegundoContrato.getQtModalidadeFornecedor() % 2 == 0) {
				if (i + 1 == exibeLinhaFornecedor) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			} else {
				if (i == exibeLinhaFornecedor) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			}

			listaServModalidades.add(servicosModalidadesDTO);

			PagamentosDTO fornecTitulos = null;
			PagamentosDTO fornecModularidades = null;

			for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato.getListaServicoPagamento()) {
				if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("F")) {
					exibePgtoFornecedor = "S";

					try {
						BeanUtils.copyProperties(pagamentosFornecedoresDTO, ocorrencias);
						pagamentosFornecedoresDTO.setDsServicoTipoServico(saidaSegundoContrato
								.getCdServicoFornecedor().toUpperCase());
						
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}

					if (!achouCreditoConta) {
						for (OcorrenciasCreditos ocorrenciasCreditos : saidaSegundoContrato.getListaCreditos()) {
							if (ocorrenciasCreditos.getDsCretipoServico().equalsIgnoreCase("F")) {
								achouCreditoConta = true;
								try {
									BeanUtils.copyProperties(pagamentosFornecedoresDTO,ocorrenciasCreditos);
								} catch (IllegalAccessException e) {
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									e.printStackTrace();
								}

								pagamentosFornecedoresDTO.setDsCretipoServico(
									saidaSegundoContrato.getListModFornecedores().get(i).getDsModalidadeFornecedor().toUpperCase());
								pagamentosFornecedoresDTO.setPcCreMaximoInconsistente(ocorrenciasCreditos
									.getPcCreMaximoInconsistente().toString());
								pagamentosFornecedoresDTO.setQtCreMaximaInconsistencia(PgitUtil.formatarNumero(
									ocorrenciasCreditos.getQtCreMaximaInconsistencia(),true));
								pagamentosFornecedoresDTO.setVlCreMaximoFavorecidoNao(NumberUtils
									.format(ocorrenciasCreditos.getVlCreMaximoFavorecidoNao()));
								pagamentosFornecedoresDTO.setVlCreLimiteDiario(NumberUtils
									.format(ocorrenciasCreditos.getVlCreLimiteDiario()));
								pagamentosFornecedoresDTO.setVlCreLimiteIndividual(NumberUtils
									.format(ocorrenciasCreditos.getVlCreLimiteIndividual()));
								pagamentosFornecedoresDTO.setQtCreDiaRepique(ocorrenciasCreditos
									.getQtCreDiaRepique().toString());
								pagamentosFornecedoresDTO.setQtCreDiaFloating(ocorrenciasCreditos
									.getQtCreDiaFloating().toString());

								continue pgtoFornecedor;
							}
						}
						achouCreditoConta = true;
						pagamentosFornecedoresDTO.setDsCretipoServico("N");

					}

					if (!achouBradesco || !achouOutros) {
						for (OcorrenciasTitulos ocorrenciasTitulos : saidaSegundoContrato.getListaTitulos()) {
							fornecTitulos = new PagamentosDTO();
							try {
								BeanUtils.copyProperties(fornecTitulos,ocorrenciasTitulos);
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}

							if (!achouBradesco) {
								if (ocorrenciasTitulos.getDsFtiTipoTitulo().equalsIgnoreCase("B")) {
									pagamentosFornecedoresDTO.setDsFtiTipoTitulo("S");
									fornecTitulos.setDsFtiTipoTitulo(saidaSegundoContrato.getListModFornecedores()
										.get(i).getDsModalidadeFornecedor().toUpperCase());
									fornecTitulos.setPcFtiMaximoInconsistente(ocorrenciasTitulos
										.getPcFtiMaximoInconsistente().toString());
									fornecTitulos.setQtFtiMaximaInconsistencia(PgitUtil.formatarNumero(
										ocorrenciasTitulos.getQtFtiMaximaInconsistencia(),true));
									fornecTitulos.setVlFtiMaximoFavorecidoNao(NumberUtils
										.format(ocorrenciasTitulos.getVlFtiMaximoFavorecidoNao()));
									fornecTitulos.setVlFtiLimiteDiario(NumberUtils
										.format(ocorrenciasTitulos.getVlFtiLimiteDiario()));
									fornecTitulos.setVlFtiLimiteIndividual(NumberUtils
										.format(ocorrenciasTitulos.getVlFtiLimiteIndividual()));
									fornecTitulos.setQtFtiDiaFloating(ocorrenciasTitulos
										.getQtFtiDiaFloating().toString());
									fornecTitulos.setQtFtiDiaRepique(ocorrenciasTitulos
										.getQtFtiDiaRepique().toString());
									fornecTitulos.setDsTIpoConsultaSaldoSuperior(ocorrenciasTitulos
										.getDsTIpoConsultaSaldoSuperior());
									fornecTitulos.setExibeDsFtiTipoTitulo("S");
									listaFornecTitulos.add(fornecTitulos);

									achouBradesco = true;
									continue pgtoFornecedor;
								}
							}

							if (!achouOutros) {
								if (ocorrenciasTitulos.getDsFtiTipoTitulo().equalsIgnoreCase("O")) {
									pagamentosFornecedoresDTO.setDsFtiTipoTitulo("S");
									fornecTitulos.setDsFtiTipoTitulo(saidaSegundoContrato.getListModFornecedores()
										.get(i).getDsModalidadeFornecedor().toUpperCase());
									fornecTitulos.setPcFtiMaximoInconsistente(ocorrenciasTitulos
										.getPcFtiMaximoInconsistente().toString());
									fornecTitulos.setQtFtiMaximaInconsistencia(PgitUtil.formatarNumero(
										ocorrenciasTitulos.getQtFtiMaximaInconsistencia(),true));
									fornecTitulos.setVlFtiMaximoFavorecidoNao(NumberUtils.format(
										ocorrenciasTitulos.getVlFtiMaximoFavorecidoNao()));
									fornecTitulos.setVlFtiLimiteDiario(NumberUtils.format(
										ocorrenciasTitulos.getVlFtiLimiteDiario()));
									fornecTitulos.setVlFtiLimiteIndividual(NumberUtils.format(
										ocorrenciasTitulos.getVlFtiLimiteIndividual()));
									fornecTitulos.setQtFtiDiaFloating(ocorrenciasTitulos
										.getQtFtiDiaFloating().toString());
									fornecTitulos.setQtFtiDiaRepique(ocorrenciasTitulos
										.getQtFtiDiaRepique().toString());
									fornecTitulos.setDsTIpoConsultaSaldoSuperior(ocorrenciasTitulos
										.getDsTIpoConsultaSaldoSuperior());
									listaFornecTitulos.add(fornecTitulos);
									fornecTitulos.setExibeDsFtiTipoTitulo("N");
									achouOutros = true;
									continue pgtoFornecedor;
								}
							}
						}
						achouBradesco = true;
						achouOutros = true;
					}

					
					if (!achouDOC || !achouTED || !achouDC) {
						for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
								.getListaModularidades()) {
							if (ocorrenciasModularidades.getDsModTipoServico().equalsIgnoreCase("F")) {
								fornecModularidades = new PagamentosDTO();

								try {
									BeanUtils.copyProperties(fornecModularidades, ocorrenciasModularidades);
								} catch (IllegalAccessException e) {
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									e.printStackTrace();
								}

								if (!achouDOC) {
									if (ocorrenciasModularidades.getDsModTipoModalidade().equalsIgnoreCase("D")) {
										pagamentosFornecedoresDTO.setDsModTipoServico("S");
										fornecModularidades.setDsModTipoServico(saidaSegundoContrato
												.getListModFornecedores().get(i).getDsModalidadeFornecedor().toUpperCase());

										fornecModularidades.setCdModIndicadorPercentualMaximoInconsistente(
											ocorrenciasModularidades.getCdModIndicadorPercentualMaximoInconsistente()
											.toString());
										fornecModularidades
										.setQtModMaximoInconsistencia(PgitUtil
												.formatarNumero(
														ocorrenciasModularidades
														.getQtModMaximoInconsistencia(),
														true));
										fornecModularidades
										.setVlModMaximoFavorecidoNao(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModMaximoFavorecidoNao()));
										fornecModularidades
										.setVlModLimiteDiario(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteDiario()));
										fornecModularidades
										.setVlModLimiteIndividual(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteIndividual()));
										fornecModularidades
										.setQtModDiaFloating(ocorrenciasModularidades
												.getQtModDiaFloating()
												.toString());
										fornecModularidades
										.setQtModDiaRepique(ocorrenciasModularidades
												.getQtModDiaRepique()
												.toString());

										listaFornecModularidades
										.add(fornecModularidades);
										achouDOC = true;
										continue pgtoFornecedor;
									}
								}
								if (!achouTED) {
									if (ocorrenciasModularidades
											.getDsModTipoModalidade()
											.equalsIgnoreCase("T")) {
										pagamentosFornecedoresDTO
										.setDsModTipoServico("S");
										fornecModularidades
										.setDsModTipoServico(saidaSegundoContrato
												.getListModFornecedores()
												.get(i)
												.getDsModalidadeFornecedor().toUpperCase());

										fornecModularidades
										.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
												.getCdModIndicadorPercentualMaximoInconsistente()
												.toString());
										fornecModularidades
										.setQtModMaximoInconsistencia(PgitUtil
												.formatarNumero(
														ocorrenciasModularidades
														.getQtModMaximoInconsistencia(),
														true));
										fornecModularidades
										.setVlModMaximoFavorecidoNao(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModMaximoFavorecidoNao()));
										fornecModularidades
										.setVlModLimiteDiario(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteDiario()));
										fornecModularidades
										.setVlModLimiteIndividual(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteIndividual()));
										fornecModularidades
										.setQtModDiaFloating(ocorrenciasModularidades
												.getQtModDiaFloating()
												.toString());
										fornecModularidades
										.setQtModDiaRepique(ocorrenciasModularidades
												.getQtModDiaRepique()
												.toString());

										listaFornecModularidades
										.add(fornecModularidades);
										achouTED = true;
										continue pgtoFornecedor;
									}
								}
								if (!achouDC) {
									if (ocorrenciasModularidades
											.getDsModTipoModalidade()
											.equalsIgnoreCase("C")) {
										pagamentosFornecedoresDTO
										.setDsModTipoServico("S");
										fornecModularidades
										.setDsModTipoServico(saidaSegundoContrato
												.getListModFornecedores()
												.get(i)
												.getDsModalidadeFornecedor().toUpperCase());

										fornecModularidades
										.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
												.getCdModIndicadorPercentualMaximoInconsistente()
												.toString());
										fornecModularidades
										.setQtModMaximoInconsistencia(PgitUtil
												.formatarNumero(
														ocorrenciasModularidades
														.getQtModMaximoInconsistencia(),
														true));
										fornecModularidades
										.setVlModMaximoFavorecidoNao(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModMaximoFavorecidoNao()));
										fornecModularidades
										.setVlModLimiteDiario(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteDiario()));
										fornecModularidades
										.setVlModLimiteIndividual(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteIndividual()));
										fornecModularidades
										.setQtModDiaFloating(ocorrenciasModularidades
												.getQtModDiaFloating()
												.toString());
										fornecModularidades
										.setQtModDiaRepique(ocorrenciasModularidades
												.getQtModDiaRepique()
												.toString());

										listaFornecModularidades
										.add(fornecModularidades);
										achouDC = true;
										continue pgtoFornecedor;
									}
								}
							}
						}
						achouDOC = true;
						achouTED = true;
						achouDC = true;
					}

					// Busca pela Modalidade Ordem de Pagamento
					if (!achouOrdemPagamento) {
						for (OcorrenciasOperacoes ocorrenciasOperacoes : saidaSegundoContrato
								.getListaOperacoes()) {
							if (ocorrenciasOperacoes.getDsOpgTipoServico()
									.equalsIgnoreCase("F")) {
								achouOrdemPagamento = true;
								try {
									BeanUtils.copyProperties(
											pagamentosFornecedoresDTO,
											ocorrenciasOperacoes);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								pagamentosFornecedoresDTO
								.setDsOpgTipoServico(saidaSegundoContrato
										.getListModFornecedores()
										.get(i)
										.getDsModalidadeFornecedor().toUpperCase());
								pagamentosFornecedoresDTO
								.setPcOpgMaximoInconsistencia(ocorrenciasOperacoes
										.getPcOpgMaximoInconsistencia()
										.toString());
								pagamentosFornecedoresDTO
								.setQtOpgMaximoInconsistencia(PgitUtil
										.formatarNumero(
												ocorrenciasOperacoes
												.getQtOpgMaximoInconsistencia(),
												true));
								pagamentosFornecedoresDTO
								.setVlOpgLimiteDiario(NumberUtils
										.format(ocorrenciasOperacoes
												.getVlOpgLimiteDiario()));
								pagamentosFornecedoresDTO
								.setVlOpgLimiteIndividual(NumberUtils
										.format(ocorrenciasOperacoes
												.getVlOpgLimiteIndividual()));
								pagamentosFornecedoresDTO
								.setQtOpgDiaFloating(ocorrenciasOperacoes
										.getQtOpgDiaFloating()
										.toString());
								pagamentosFornecedoresDTO
								.setVlOpgMaximoFavorecidoNao(NumberUtils
										.format(ocorrenciasOperacoes
												.getVlOpgMaximoFavorecidoNao()));
								pagamentosFornecedoresDTO
								.setQtOpgDiaRepique(ocorrenciasOperacoes
										.getQtOpgDiaRepique()
										.toString());
								pagamentosFornecedoresDTO
								.setQtOpgDiaExpiracao(ocorrenciasOperacoes
										.getQtOpgDiaExpiracao()
										.toString());

								continue pgtoFornecedor;
							}
						}
						achouOrdemPagamento = true;
						pagamentosFornecedoresDTO.setDsOpgTipoServico("N");
					}

					// Busca pela Modalidade Pagemanto de modularidades (Ordem
					// de Credito / Deposito Identificado)
					if (!achouOC || !achouDI) {
						for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
								.getListaModularidades()) {
							if (ocorrenciasModularidades.getDsModTipoServico()
									.equalsIgnoreCase("F")) {
								fornecModularidades = new PagamentosDTO();

								try {
									BeanUtils.copyProperties(
											fornecModularidades,
											ocorrenciasModularidades);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								if (!achouOC) {
									if (ocorrenciasModularidades
											.getDsModTipoModalidade()
											.equalsIgnoreCase("O")) {
										pagamentosFornecedoresDTO
										.setDsModTipoOutrosServico("S");
										fornecModularidades
										.setDsModTipoServico(saidaSegundoContrato
												.getListModFornecedores()
												.get(i)
												.getDsModalidadeFornecedor().toUpperCase());

										fornecModularidades
										.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
												.getCdModIndicadorPercentualMaximoInconsistente()
												.toString());
										fornecModularidades
										.setQtModMaximoInconsistencia(PgitUtil
												.formatarNumero(
														ocorrenciasModularidades
														.getQtModMaximoInconsistencia(),
														true));
										fornecModularidades
										.setVlModMaximoFavorecidoNao(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModMaximoFavorecidoNao()));
										fornecModularidades
										.setVlModLimiteDiario(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteDiario()));
										fornecModularidades
										.setVlModLimiteIndividual(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteIndividual()));
										fornecModularidades
										.setQtModDiaFloating(ocorrenciasModularidades
												.getQtModDiaFloating()
												.toString());
										fornecModularidades
										.setQtModDiaRepique(ocorrenciasModularidades
												.getQtModDiaRepique()
												.toString());

										listaFornecOutrasModularidades
										.add(fornecModularidades);
										achouOC = true;
										continue pgtoFornecedor;
									}
								}

								if (!achouDI) {
									if (ocorrenciasModularidades
											.getDsModTipoModalidade()
											.equalsIgnoreCase("I")) {
										pagamentosFornecedoresDTO
										.setDsModTipoOutrosServico("S");
										fornecModularidades
										.setDsModTipoServico(saidaSegundoContrato
												.getListModFornecedores()
												.get(i)
												.getDsModalidadeFornecedor().toUpperCase());

										fornecModularidades
										.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
												.getCdModIndicadorPercentualMaximoInconsistente()
												.toString());
										fornecModularidades
										.setQtModMaximoInconsistencia(PgitUtil
												.formatarNumero(
														ocorrenciasModularidades
														.getQtModMaximoInconsistencia(),
														true));
										fornecModularidades
										.setVlModMaximoFavorecidoNao(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModMaximoFavorecidoNao()));
										fornecModularidades
										.setVlModLimiteDiario(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteDiario()));
										fornecModularidades
										.setVlModLimiteIndividual(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteIndividual()));
										fornecModularidades
										.setQtModDiaFloating(ocorrenciasModularidades
												.getQtModDiaFloating()
												.toString());
										fornecModularidades
										.setQtModDiaRepique(ocorrenciasModularidades
												.getQtModDiaRepique()
												.toString());

										listaFornecOutrasModularidades
										.add(fornecModularidades);
										achouDI = true;
										continue pgtoFornecedor;
									}
								}
							}
						}
						achouOC = true;
						achouDI = true;
					}

					// Bloco Rastreamentos
					if (saidaSegundoContrato.getDsFraTipoRastreabilidade()
							.trim().equals("")) {
						pagamentosFornecedoresDTO
						.setDsFraTipoRastreabilidade("N");
					} else {
						pagamentosFornecedoresDTO
						.setDsFraTipoRastreabilidade(saidaSegundoContrato
								.getDsFraTipoRastreabilidade());
						pagamentosFornecedoresDTO
						.setDsFraRastreabilidadeTerceiro(saidaSegundoContrato
								.getDsFraRastreabilidadeTerceiro());
						pagamentosFornecedoresDTO
						.setDsFraRastreabilidadeNota(saidaSegundoContrato
								.getDsFraRastreabilidadeNota());
						pagamentosFornecedoresDTO
						.setDsFraCaptacaoTitulo(saidaSegundoContrato
								.getDsFraCaptacaoTitulo());
						pagamentosFornecedoresDTO
						.setDsFraAgendaCliente(saidaSegundoContrato
								.getDsFraAgendaCliente());
						pagamentosFornecedoresDTO
						.setDsFraAgendaFilial(saidaSegundoContrato
								.getDsFraAgendaFilial());
						pagamentosFornecedoresDTO
						.setDsFraBloqueioEmissao(saidaSegundoContrato
								.getDsFraBloqueioEmissao());
						pagamentosFornecedoresDTO
						.setDtFraInicioBloqueio(saidaSegundoContrato
								.getDtFraInicioBloqueio());
						pagamentosFornecedoresDTO
						.setDtFraInicioRastreabilidade(saidaSegundoContrato
								.getDtFraInicioRastreabilidade());
						pagamentosFornecedoresDTO
						.setDsFraAdesaoSacado(saidaSegundoContrato
								.getDsFraAdesaoSacado());
					}
				}
			}
		}

		// Pagamento de tributos
		int exibeLinhaTributos = saidaSegundoContrato.getQtModalidadeTributos() / 2;
		exibePgtoTributos = "N";
		PagamentosDTO pagamentosTributosDTO = new PagamentosDTO();
		PagamentosDTO tributosDTO = null;
		pagamentosTributosDTO
		.setDsTibutoAgendadoDebitoVeicular(saidaSegundoContrato
				.getDsTibutoAgendadoDebitoVeicular());
		pagamentosTributosDTO.setDsTdvTipoConsultaProposta(saidaSegundoContrato
				.getDsTdvTipoConsultaProposta());
		pagamentosTributosDTO.setDsTdvTipoTratamentoValor(saidaSegundoContrato
				.getDsTdvTipoTratamentoValor());

		listaPagamentosTri = new ArrayList<PagamentosDTO>();
		listaTri = new ArrayList<PagamentosDTO>();

		boolean achouDV = false;
		boolean achouGR = false;
		boolean achouDR = false;
		boolean achouGP = false;
		boolean achouCB = false;

		pgtoTributos: for (int i = 0; i < saidaSegundoContrato
		.getQtModalidadeTributos(); i++) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico(saidaSegundoContrato
					.getCdServicoTributos());
			servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato
					.getListaModTributos().get(i).getCdModalidadeTributos());

			if (saidaSegundoContrato.getQtModalidadeTributos() % 2 == 0) {
				if (i + 1 == exibeLinhaTributos) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			} else {
				if (i == exibeLinhaTributos) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			}

			listaServModalidades.add(servicosModalidadesDTO);

			// Bloco de Servi�o de Pagamento de Tributos
			for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato
					.getListaServicoPagamento()) {
				if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("T")) {
					exibePgtoTributos = "S";
					try {
						BeanUtils.copyProperties(pagamentosTributosDTO,
								ocorrencias);
						pagamentosTributosDTO
						.setDsServicoTipoServico(saidaSegundoContrato
								.getCdServicoTributos().toUpperCase());
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Busca pela Modalidades (Debito de Veiculos / GARE / DARF
					// / GPS / C�digo de Barras)
					if (!achouDV || !achouGR || !achouDR || !achouGP
							|| !achouCB) {
						for (OcorrenciasTriTributo ocorrenciasTriTributo : saidaSegundoContrato
								.getListaTriTributos()) {
							tributosDTO = new PagamentosDTO();
							try {
								BeanUtils.copyProperties(tributosDTO,
										ocorrenciasTriTributo);
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							if (!achouDV) {
								if (ocorrenciasTriTributo.getDsTriTipoTributo()
										.equalsIgnoreCase("V")) {
									tributosDTO.setExibeDebitoVeiculo("S");

									tributosDTO
									.setDsTriTipoTributo(saidaSegundoContrato
											.getListaModTributos().get(
													i)
													.getCdModalidadeTributos().toUpperCase());

									tributosDTO
									.setDsTdvTipoTratamentoValor(saidaSegundoContrato
											.getDsTdvTipoTratamentoValor());
									tributosDTO
									.setDsTdvTipoConsultaProposta(saidaSegundoContrato
											.getDsTdvTipoConsultaProposta());
									tributosDTO
									.setVlTriMaximoFavorecidoNao(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriMaximoFavorecidoNao()));
									tributosDTO
									.setVlTriLimiteDiario(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriLimiteDiario()));
									tributosDTO
									.setVlTriLimiteIndividual(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriLimiteIndividual()));
									tributosDTO
									.setQttriDiaFloating(ocorrenciasTriTributo
											.getQttriDiaFloating()
											.toString());
									tributosDTO
									.setQtTriDiaRepique(ocorrenciasTriTributo
											.getQtTriDiaRepique()
											.toString());

									listaTri.add(tributosDTO);

									achouDV = true;
									continue pgtoTributos;
								}
							}
							if (!achouGR) {
								if (ocorrenciasTriTributo.getDsTriTipoTributo()
										.equalsIgnoreCase("G")) {
									tributosDTO.setExibeDebitoVeiculo("N");
									tributosDTO
									.setDsTriTipoTributo(saidaSegundoContrato
											.getListaModTributos().get(
													i)
													.getCdModalidadeTributos().toUpperCase());

									tributosDTO
									.setVlTriMaximoFavorecidoNao(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriMaximoFavorecidoNao()));
									tributosDTO
									.setVlTriLimiteDiario(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriLimiteDiario()));
									tributosDTO
									.setVlTriLimiteIndividual(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriLimiteIndividual()));
									tributosDTO
									.setQttriDiaFloating(ocorrenciasTriTributo
											.getQttriDiaFloating()
											.toString());
									tributosDTO
									.setQtTriDiaRepique(ocorrenciasTriTributo
											.getQtTriDiaRepique()
											.toString());

									listaTri.add(tributosDTO);

									achouGR = true;
									continue pgtoTributos;
								}
							}
							if (!achouDR) {
								if (ocorrenciasTriTributo.getDsTriTipoTributo()
										.equalsIgnoreCase("D")) {
									tributosDTO.setExibeDebitoVeiculo("N");
									tributosDTO
									.setDsTriTipoTributo(saidaSegundoContrato
											.getListaModTributos().get(
													i)
													.getCdModalidadeTributos().toUpperCase());

									tributosDTO
									.setVlTriMaximoFavorecidoNao(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriMaximoFavorecidoNao()));
									tributosDTO
									.setVlTriLimiteDiario(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriLimiteDiario()));
									tributosDTO
									.setVlTriLimiteIndividual(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriLimiteIndividual()));
									tributosDTO
									.setQttriDiaFloating(ocorrenciasTriTributo
											.getQttriDiaFloating()
											.toString());
									tributosDTO
									.setQtTriDiaRepique(ocorrenciasTriTributo
											.getQtTriDiaRepique()
											.toString());

									listaTri.add(tributosDTO);

									achouDR = true;
									continue pgtoTributos;
								}
							}

							if (!achouGP) {
								if (ocorrenciasTriTributo.getDsTriTipoTributo()
										.equalsIgnoreCase("P")) {
									tributosDTO.setExibeDebitoVeiculo("N");
									tributosDTO
									.setDsTriTipoTributo(saidaSegundoContrato
											.getListaModTributos().get(
													i)
													.getCdModalidadeTributos().toUpperCase());

									tributosDTO
									.setVlTriMaximoFavorecidoNao(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriMaximoFavorecidoNao()));
									tributosDTO
									.setVlTriLimiteDiario(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriLimiteDiario()));
									tributosDTO
									.setVlTriLimiteIndividual(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriLimiteIndividual()));
									tributosDTO
									.setQttriDiaFloating(ocorrenciasTriTributo
											.getQttriDiaFloating()
											.toString());
									tributosDTO
									.setQtTriDiaRepique(ocorrenciasTriTributo
											.getQtTriDiaRepique()
											.toString());

									listaTri.add(tributosDTO);

									achouGP = true;
									continue pgtoTributos;
								}
							}
							if (!achouCB) {
								if (ocorrenciasTriTributo.getDsTriTipoTributo()
										.equalsIgnoreCase("B")) {
									tributosDTO.setExibeDebitoVeiculo("N");
									tributosDTO
									.setDsTriTipoTributo(saidaSegundoContrato
											.getListaModTributos().get(
													i)
													.getCdModalidadeTributos().toUpperCase());

									tributosDTO
									.setVlTriMaximoFavorecidoNao(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriMaximoFavorecidoNao()));
									tributosDTO
									.setVlTriLimiteDiario(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriLimiteDiario()));
									tributosDTO
									.setVlTriLimiteIndividual(NumberUtils
											.format(ocorrenciasTriTributo
													.getVlTriLimiteIndividual()));
									tributosDTO
									.setQttriDiaFloating(ocorrenciasTriTributo
											.getQttriDiaFloating()
											.toString());
									tributosDTO
									.setQtTriDiaRepique(ocorrenciasTriTributo
											.getQtTriDiaRepique()
											.toString());

									listaTri.add(tributosDTO);

									achouCB = true;
									continue pgtoTributos;
								}
							}
						}
					}

				}
			}
		}
		listaPagamentosTri.add(pagamentosTributosDTO);


		// Pagamento de sal�rios
		exibePgtoSalarios = "N";
		PagamentosDTO pagamentosSalariosDTO = new PagamentosDTO();
		pagamentosSalariosDTO
		.setDsSalarioEmissaoAntecipadoCartao(saidaSegundoContrato
				.getDsSalarioEmissaoAntecipadoCartao());
		pagamentosSalariosDTO
		.setCdSalarioQuantidadeLimiteSolicitacaoCartao(saidaSegundoContrato
				.getCdSalarioQuantidadeLimiteSolicitacaoCartao()
				.toString());
		pagamentosSalariosDTO
		.setDsSalarioAberturaContaExpressa(saidaSegundoContrato
				.getDsSalarioAberturaContaExpressa());

		List<PagamentosDTO> listaSalariosModularidades = new ArrayList<PagamentosDTO>();

		achouCreditoConta = false;
		achouDOC = false;
		achouTED = false;
		achouOrdemPagamento = false;

		int exibeLinhaSalario = saidaSegundoContrato.getQtModalidadeSalario() / 2;
		pgtoSalarios: for (int i = 0; i < saidaSegundoContrato
		.getQtModalidadeSalario(); i++) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico(saidaSegundoContrato
					.getCdServicoSalarial());
			servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato
					.getListaModSalario().get(i).getCdModalidadeSalarial());

			if (saidaSegundoContrato.getQtModalidadeSalario() % 2 == 0) {
				if (i + 1 == exibeLinhaSalario) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			} else {
				if (i == exibeLinhaSalario) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			}

			listaServModalidades.add(servicosModalidadesDTO);

			// Bloco de Servi�o de Pagamento de Sal�rios
			PagamentosDTO salariosModularidades = null;
			for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato
					.getListaServicoPagamento()) {
				if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("S")) {
					exibePgtoSalarios = "S";
					try {
						BeanUtils.copyProperties(pagamentosSalariosDTO,
								ocorrencias);
						pagamentosSalariosDTO
						.setDsServicoTipoServico(saidaSegundoContrato
								.getCdServicoSalarial().toUpperCase());
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Busca pela Modalidade Cr�dito em conta
					if (!achouCreditoConta) {
						for (OcorrenciasCreditos ocorrenciasCreditos : saidaSegundoContrato
								.getListaCreditos()) {
							if (ocorrenciasCreditos.getDsCretipoServico()
									.equalsIgnoreCase("S")) {
								achouCreditoConta = true;
								try {
									BeanUtils.copyProperties(
											pagamentosSalariosDTO,
											ocorrenciasCreditos);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								pagamentosSalariosDTO
								.setDsCretipoServico(saidaSegundoContrato
										.getListaModSalario().get(i)
										.getCdModalidadeSalarial().toUpperCase());
								pagamentosSalariosDTO
								.setPcCreMaximoInconsistente(ocorrenciasCreditos
										.getPcCreMaximoInconsistente()
										.toString());
								pagamentosSalariosDTO
								.setQtCreMaximaInconsistencia(PgitUtil
										.formatarNumero(
												ocorrenciasCreditos
												.getQtCreMaximaInconsistencia(),
												true));
								pagamentosSalariosDTO
								.setVlCreMaximoFavorecidoNao(NumberUtils
										.format(ocorrenciasCreditos
												.getVlCreMaximoFavorecidoNao()));
								pagamentosSalariosDTO
								.setVlCreLimiteDiario(NumberUtils
										.format(ocorrenciasCreditos
												.getVlCreLimiteDiario()));
								pagamentosSalariosDTO
								.setVlCreLimiteIndividual(NumberUtils
										.format(ocorrenciasCreditos
												.getVlCreLimiteIndividual()));
								pagamentosSalariosDTO
								.setQtCreDiaRepique(ocorrenciasCreditos
										.getQtCreDiaRepique()
										.toString());
								pagamentosSalariosDTO
								.setQtCreDiaFloating(ocorrenciasCreditos
										.getQtCreDiaFloating()
										.toString());

								continue pgtoSalarios;
							}
						}
						achouCreditoConta = true;
						pagamentosSalariosDTO.setDsCretipoServico("N");
					}

					// Busca pela Modalidade Pagemanto de modularidades (DOC /
					// TED)
					if (!achouDOC || !achouTED) {
						for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
								.getListaModularidades()) {
							if (ocorrenciasModularidades.getDsModTipoServico()
									.equalsIgnoreCase("S")) {
								salariosModularidades = new PagamentosDTO();

								try {
									BeanUtils.copyProperties(
											salariosModularidades,
											ocorrenciasModularidades);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								if (!achouDOC) {
									if (ocorrenciasModularidades
											.getDsModTipoModalidade()
											.equalsIgnoreCase("D")) {
										pagamentosSalariosDTO
										.setDsModTipoServico("S");
										salariosModularidades
										.setDsModTipoServico(saidaSegundoContrato
												.getListaModSalario()
												.get(i)
												.getCdModalidadeSalarial().toUpperCase());

										salariosModularidades
										.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
												.getCdModIndicadorPercentualMaximoInconsistente()
												.toString());
										salariosModularidades
										.setQtModMaximoInconsistencia(PgitUtil
												.formatarNumero(
														ocorrenciasModularidades
														.getQtModMaximoInconsistencia(),
														true));
										salariosModularidades
										.setVlModMaximoFavorecidoNao(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModMaximoFavorecidoNao()));
										salariosModularidades
										.setVlModLimiteDiario(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteDiario()));
										salariosModularidades
										.setVlModLimiteIndividual(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteIndividual()));
										salariosModularidades
										.setQtModDiaFloating(ocorrenciasModularidades
												.getQtModDiaFloating()
												.toString());
										salariosModularidades
										.setQtModDiaRepique(ocorrenciasModularidades
												.getQtModDiaRepique()
												.toString());

										listaSalariosModularidades
										.add(salariosModularidades);
										achouDOC = true;
										continue pgtoSalarios;
									}
								}
								if (!achouTED) {
									if (ocorrenciasModularidades
											.getDsModTipoModalidade()
											.equalsIgnoreCase("T")) {
										pagamentosSalariosDTO
										.setDsModTipoServico("S");
										salariosModularidades
										.setDsModTipoServico(saidaSegundoContrato
												.getListaModSalario()
												.get(i)
												.getCdModalidadeSalarial().toUpperCase());

										salariosModularidades
										.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
												.getCdModIndicadorPercentualMaximoInconsistente()
												.toString());
										salariosModularidades
										.setQtModMaximoInconsistencia(PgitUtil
												.formatarNumero(
														ocorrenciasModularidades
														.getQtModMaximoInconsistencia(),
														true));
										salariosModularidades
										.setVlModMaximoFavorecidoNao(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModMaximoFavorecidoNao()));
										salariosModularidades
										.setVlModLimiteDiario(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteDiario()));
										salariosModularidades
										.setVlModLimiteIndividual(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteIndividual()));
										salariosModularidades
										.setQtModDiaFloating(ocorrenciasModularidades
												.getQtModDiaFloating()
												.toString());
										salariosModularidades
										.setQtModDiaRepique(ocorrenciasModularidades
												.getQtModDiaRepique()
												.toString());

										listaSalariosModularidades
										.add(salariosModularidades);
										achouTED = true;
										continue pgtoSalarios;
									}
								}
							}
						}
						achouDOC = true;
						achouTED = true;
						pagamentosSalariosDTO.setDsModTipoServico("N");
					}

					// Busca pela Modalidade Ordem de Pagamento
					if (!achouOrdemPagamento) {
						for (OcorrenciasOperacoes ocorrenciasOperacoes : saidaSegundoContrato
								.getListaOperacoes()) {
							if (ocorrenciasOperacoes.getDsOpgTipoServico()
									.equalsIgnoreCase("S")) {
								achouOrdemPagamento = true;
								try {
									BeanUtils.copyProperties(
											pagamentosSalariosDTO,
											ocorrenciasOperacoes);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								pagamentosSalariosDTO
								.setDsOpgTipoServico(saidaSegundoContrato
										.getListaModSalario().get(i)
										.getCdModalidadeSalarial().toUpperCase());
								pagamentosSalariosDTO
								.setPcOpgMaximoInconsistencia(ocorrenciasOperacoes
										.getPcOpgMaximoInconsistencia()
										.toString());
								pagamentosSalariosDTO
								.setQtOpgMaximoInconsistencia(PgitUtil
										.formatarNumero(
												ocorrenciasOperacoes
												.getQtOpgMaximoInconsistencia(),
												true));
								pagamentosSalariosDTO
								.setVlOpgLimiteDiario(NumberUtils
										.format(ocorrenciasOperacoes
												.getVlOpgLimiteDiario()));
								pagamentosSalariosDTO
								.setVlOpgLimiteIndividual(NumberUtils
										.format(ocorrenciasOperacoes
												.getVlOpgLimiteIndividual()));
								pagamentosSalariosDTO
								.setQtOpgDiaFloating(ocorrenciasOperacoes
										.getQtOpgDiaFloating()
										.toString());
								pagamentosSalariosDTO
								.setVlOpgMaximoFavorecidoNao(NumberUtils
										.format(ocorrenciasOperacoes
												.getVlOpgMaximoFavorecidoNao()));
								pagamentosSalariosDTO
								.setQtOpgDiaRepique(ocorrenciasOperacoes
										.getQtOpgDiaRepique()
										.toString());
								pagamentosSalariosDTO
								.setQtOpgDiaExpiracao(ocorrenciasOperacoes
										.getQtOpgDiaExpiracao()
										.toString());

								continue pgtoSalarios;
							}
						}
						achouOrdemPagamento = true;
						pagamentosSalariosDTO.setDsOpgTipoServico("N");
					}
				}
			}
		}

		// Pagamento de benef�cios
		exibePgtoBeneficios = "N";
		PagamentosDTO pagamentosBeneficiosDTO = new PagamentosDTO();
		pagamentosBeneficiosDTO.setDsBprTipoAcao(saidaSegundoContrato
				.getDsBprTipoAcao().trim().equals("") ? "N"
						: saidaSegundoContrato.getDsBprTipoAcao());
		pagamentosBeneficiosDTO
		.setDsBeneficioInformadoAgenciaConta(saidaSegundoContrato
				.getDsBeneficioInformadoAgenciaConta());
		pagamentosBeneficiosDTO
		.setDsBeneficioTipoIdentificacaoBeneficio(saidaSegundoContrato
				.getDsBeneficioTipoIdentificacaoBeneficio());
		pagamentosBeneficiosDTO
		.setDsBeneficioUtilizacaoCadastroOrganizacao(saidaSegundoContrato
				.getDsBeneficioUtilizacaoCadastroOrganizacao());
		pagamentosBeneficiosDTO
		.setDsBeneficioUtilizacaoCadastroProcuradores(saidaSegundoContrato
				.getDsBeneficioUtilizacaoCadastroProcuradores());
		pagamentosBeneficiosDTO
		.setDsBeneficioPossuiExpiracaoCredito(saidaSegundoContrato
				.getDsBeneficioPossuiExpiracaoCredito());
		pagamentosBeneficiosDTO
		.setCdBeneficioQuantidadeDiasExpiracaoCredito(saidaSegundoContrato
				.getCdBeneficioQuantidadeDiasExpiracaoCredito()
				.toString());
		pagamentosBeneficiosDTO
		.setCdBeneficioQuantidadeDiasExpiracaoCredito(saidaSegundoContrato
				.getCdBeneficioQuantidadeDiasExpiracaoCredito()
				.toString());
		pagamentosBeneficiosDTO.setQtBprMesProvisorio(saidaSegundoContrato
				.getQtBprMesProvisorio().toString());
		pagamentosBeneficiosDTO
		.setCdBprIndicadorEmissaoAviso(saidaSegundoContrato
				.getCdBprIndicadorEmissaoAviso());
		pagamentosBeneficiosDTO.setQtBprDiaAnteriorAviso(saidaSegundoContrato
				.getQtBprDiaAnteriorAviso().toString());
		pagamentosBeneficiosDTO
		.setQtBprDiaAnteriorVencimento(saidaSegundoContrato
				.getQtBprDiaAnteriorVencimento().toString());
		pagamentosBeneficiosDTO
		.setCdBprUtilizadoMensagemPersonalizada(saidaSegundoContrato
				.getCdBprUtilizadoMensagemPersonalizada());
		pagamentosBeneficiosDTO.setDsBprDestino(saidaSegundoContrato
				.getDsBprDestino());
		pagamentosBeneficiosDTO
		.setDsBprCadastroEnderecoUtilizado(saidaSegundoContrato
				.getDsBprCadastroEnderecoUtilizado());

		List<PagamentosDTO> listaBeneficiosModularidades = new ArrayList<PagamentosDTO>();

		achouCreditoConta = false;
		achouDOC = false;
		achouTED = false;
		achouOrdemPagamento = false;

		int exibeLinhaBeneficio = saidaSegundoContrato
		.getQtModalidadeBeneficio() / 2;
		pgtoBeneficios: for (int i = 0; i < saidaSegundoContrato
		.getQtModalidadeBeneficio(); i++) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico(saidaSegundoContrato
					.getCdServicoBeneficio());
			servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato
					.getListaModBeneficio().get(i).getCdModalidadeBeneficio());

			if (saidaSegundoContrato.getQtModalidadeBeneficio() % 2 == 0) {
				if (i + 1 == exibeLinhaBeneficio) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			} else {
				if (i == exibeLinhaBeneficio) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			}

			listaServModalidades.add(servicosModalidadesDTO);

			// Bloco de Servi�o de Pagamento de Benef�cios
			PagamentosDTO beneficiosModularidades = null;
			for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato
					.getListaServicoPagamento()) {
				if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("B")) {
					exibePgtoBeneficios = "S";
					try {
						BeanUtils.copyProperties(pagamentosBeneficiosDTO,
								ocorrencias);
						pagamentosBeneficiosDTO
						.setDsServicoTipoServico(saidaSegundoContrato
								.getCdServicoBeneficio().toUpperCase());
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Busca pela Modalidade Cr�dito em conta
					if (!achouCreditoConta) {
						for (OcorrenciasCreditos ocorrenciasCreditos : saidaSegundoContrato
								.getListaCreditos()) {
							if (ocorrenciasCreditos.getDsCretipoServico()
									.equalsIgnoreCase("B")) {
								achouCreditoConta = true;
								try {
									BeanUtils.copyProperties(
											pagamentosBeneficiosDTO,
											ocorrenciasCreditos);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								pagamentosBeneficiosDTO
								.setDsCretipoServico(saidaSegundoContrato
										.getListaModBeneficio().get(i)
										.getCdModalidadeBeneficio().toUpperCase());
								pagamentosBeneficiosDTO
								.setPcCreMaximoInconsistente(ocorrenciasCreditos
										.getPcCreMaximoInconsistente()
										.toString());
								pagamentosBeneficiosDTO
								.setQtCreMaximaInconsistencia(PgitUtil
										.formatarNumero(
												ocorrenciasCreditos
												.getQtCreMaximaInconsistencia(),
												true));
								pagamentosBeneficiosDTO
								.setVlCreMaximoFavorecidoNao(NumberUtils
										.format(ocorrenciasCreditos
												.getVlCreMaximoFavorecidoNao()));
								pagamentosBeneficiosDTO
								.setVlCreLimiteDiario(NumberUtils
										.format(ocorrenciasCreditos
												.getVlCreLimiteDiario()));
								pagamentosBeneficiosDTO
								.setVlCreLimiteIndividual(NumberUtils
										.format(ocorrenciasCreditos
												.getVlCreLimiteIndividual()));
								pagamentosBeneficiosDTO
								.setQtCreDiaRepique(ocorrenciasCreditos
										.getQtCreDiaRepique()
										.toString());
								pagamentosBeneficiosDTO
								.setQtCreDiaFloating(ocorrenciasCreditos
										.getQtCreDiaFloating()
										.toString());

								continue pgtoBeneficios;
							}
						}
						achouCreditoConta = true;
						pagamentosBeneficiosDTO.setDsCretipoServico("N");
					}

					// Busca pela Modalidade Pagemanto de modularidades (DOC /
					// TED)
					if (!achouDOC || !achouTED) {
						for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
								.getListaModularidades()) {
							if (ocorrenciasModularidades.getDsModTipoServico()
									.equalsIgnoreCase("B")) {
								beneficiosModularidades = new PagamentosDTO();

								try {
									BeanUtils.copyProperties(
											beneficiosModularidades,
											ocorrenciasModularidades);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								if (!achouDOC) {
									if (ocorrenciasModularidades
											.getDsModTipoModalidade()
											.equalsIgnoreCase("D")) {
										pagamentosBeneficiosDTO
										.setDsModTipoServico("S");

										beneficiosModularidades
										.setDsModTipoServico(saidaSegundoContrato
												.getListaModBeneficio()
												.get(i)
												.getCdModalidadeBeneficio().toUpperCase());
										beneficiosModularidades
										.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
												.getCdModIndicadorPercentualMaximoInconsistente()
												.toString());
										beneficiosModularidades
										.setQtModMaximoInconsistencia(PgitUtil
												.formatarNumero(
														ocorrenciasModularidades
														.getQtModMaximoInconsistencia(),
														true));
										beneficiosModularidades
										.setVlModMaximoFavorecidoNao(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModMaximoFavorecidoNao()));
										beneficiosModularidades
										.setVlModLimiteDiario(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteDiario()));
										beneficiosModularidades
										.setVlModLimiteIndividual(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteIndividual()));
										beneficiosModularidades
										.setQtModDiaFloating(ocorrenciasModularidades
												.getQtModDiaFloating()
												.toString());
										beneficiosModularidades
										.setQtModDiaRepique(ocorrenciasModularidades
												.getQtModDiaRepique()
												.toString());

										listaBeneficiosModularidades
										.add(beneficiosModularidades);
										achouDOC = true;
										continue pgtoBeneficios;
									}
								}
								if (!achouTED) {
									if (ocorrenciasModularidades
											.getDsModTipoModalidade()
											.equalsIgnoreCase("T")) {
										pagamentosSalariosDTO
										.setDsModTipoServico("S");
										pagamentosBeneficiosDTO
										.setDsModTipoServico("S");

										beneficiosModularidades
										.setDsModTipoServico(saidaSegundoContrato
												.getListaModBeneficio()
												.get(i)
												.getCdModalidadeBeneficio().toUpperCase());
										beneficiosModularidades
										.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
												.getCdModIndicadorPercentualMaximoInconsistente()
												.toString());
										beneficiosModularidades
										.setQtModMaximoInconsistencia(PgitUtil
												.formatarNumero(
														ocorrenciasModularidades
														.getQtModMaximoInconsistencia(),
														true));
										beneficiosModularidades
										.setVlModMaximoFavorecidoNao(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModMaximoFavorecidoNao()));
										beneficiosModularidades
										.setVlModLimiteDiario(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteDiario()));
										beneficiosModularidades
										.setVlModLimiteIndividual(NumberUtils
												.format(ocorrenciasModularidades
														.getVlModLimiteIndividual()));
										beneficiosModularidades
										.setQtModDiaFloating(ocorrenciasModularidades
												.getQtModDiaFloating()
												.toString());
										beneficiosModularidades
										.setQtModDiaRepique(ocorrenciasModularidades
												.getQtModDiaRepique()
												.toString());

										listaBeneficiosModularidades
										.add(beneficiosModularidades);
										achouTED = true;
										continue pgtoBeneficios;
									}
								}
							}
						}
						achouDOC = true;
						achouTED = true;
						pagamentosBeneficiosDTO.setDsModTipoServico("N");
					}

					// Busca pela Modalidade Ordem de Pagamento
					if (!achouOrdemPagamento) {
						for (OcorrenciasOperacoes ocorrenciasOperacoes : saidaSegundoContrato
								.getListaOperacoes()) {
							if (ocorrenciasOperacoes.getDsOpgTipoServico()
									.equalsIgnoreCase("B")) {
								achouOrdemPagamento = true;
								try {
									BeanUtils.copyProperties(
											pagamentosBeneficiosDTO,
											ocorrenciasOperacoes);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								pagamentosBeneficiosDTO
								.setDsOpgTipoServico(saidaSegundoContrato
										.getListaModBeneficio().get(i)
										.getCdModalidadeBeneficio().toUpperCase());
								pagamentosBeneficiosDTO
								.setPcOpgMaximoInconsistencia(ocorrenciasOperacoes
										.getPcOpgMaximoInconsistencia()
										.toString());
								pagamentosBeneficiosDTO
								.setQtOpgMaximoInconsistencia(PgitUtil
										.formatarNumero(
												ocorrenciasOperacoes
												.getQtOpgMaximoInconsistencia(),
												true));
								pagamentosBeneficiosDTO
								.setVlOpgLimiteDiario(NumberUtils
										.format(ocorrenciasOperacoes
												.getVlOpgLimiteDiario()));
								pagamentosBeneficiosDTO
								.setVlOpgLimiteIndividual(NumberUtils
										.format(ocorrenciasOperacoes
												.getVlOpgLimiteIndividual()));
								pagamentosBeneficiosDTO
								.setQtOpgDiaFloating(ocorrenciasOperacoes
										.getQtOpgDiaFloating()
										.toString());
								pagamentosBeneficiosDTO
								.setVlOpgMaximoFavorecidoNao(NumberUtils
										.format(ocorrenciasOperacoes
												.getVlOpgMaximoFavorecidoNao()));
								pagamentosBeneficiosDTO
								.setQtOpgDiaRepique(ocorrenciasOperacoes
										.getQtOpgDiaRepique()
										.toString());
								pagamentosBeneficiosDTO
								.setQtOpgDiaExpiracao(ocorrenciasOperacoes
										.getQtOpgDiaExpiracao()
										.toString());

								continue pgtoBeneficios;
							}
						}
						achouOrdemPagamento = true;
						pagamentosBeneficiosDTO.setDsOpgTipoServico("N");
					}
				}
			}

		}
		if(!isOmiteServicosResumoContrato()){
		// Emiss�o de aviso de movimenta��o
		if (saidaSegundoContrato.getCdIndicadorAviso().equalsIgnoreCase("S")) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico("AVISO DE MOVIMENTA��O");
			StringBuilder bufferMovimentacao = new StringBuilder();
			bufferMovimentacao.append("(");
			bufferMovimentacao.append(saidaSegundoContrato
					.getCdIndicadorAvisoPagador());
			bufferMovimentacao.append(")PAGADOR (");
			bufferMovimentacao.append(saidaSegundoContrato
					.getCdIndicadorAvisoFavorecido());
			bufferMovimentacao.append(")FAVORECIDO");
			servicosModalidadesDTO.setDsModalidade(bufferMovimentacao
					.toString());
			servicosModalidadesDTO.setImprimeLinhaServico("S");

			listaServModalidades.add(servicosModalidadesDTO);
		}

		// Emiss�o de comprovante de pagamento
		if (saidaSegundoContrato.getCdIndicadorComplemento().equalsIgnoreCase(
		"S")) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico("COMPROVANTE DE PAGAMENTO");
			StringBuilder bufferComplemento = new StringBuilder();
			bufferComplemento.append("(");
			bufferComplemento.append(saidaSegundoContrato
					.getCdIndicadorComplementoPagador());
			bufferComplemento.append(")PAGADOR (");
			bufferComplemento.append(saidaSegundoContrato
					.getCdIndicadorComplementoFavorecido());
			bufferComplemento.append(")FAVORECIDO (");
			bufferComplemento.append(saidaSegundoContrato
					.getCdIndicadorComplementoSalarial());
			bufferComplemento.append(")SALARIAL (");
			bufferComplemento.append(saidaSegundoContrato
					.getCdIndicadorComplementoDivergente());
			bufferComplemento.append(")DIVERSOS");
			servicosModalidadesDTO
			.setDsModalidade(bufferComplemento.toString());
			servicosModalidadesDTO.setImprimeLinhaServico("S");

			listaServModalidades.add(servicosModalidadesDTO);
		}

		// Cadastro de Favorecidos
		StringBuilder bufferCadFavorecido = new StringBuilder();
		servicosModalidadesDTO = new ServicosModalidadesDTO();
		servicosModalidadesDTO.setDsServico("CADASTRO DE FAVORECIDOS");
		bufferCadFavorecido
		.append(saidaSegundoContrato.getCdIndcadorCadastroFavorecido()
				.equalsIgnoreCase("S") ? "(X)SIM ()N�O"
						: "()SIM (X)N�O");
		servicosModalidadesDTO.setDsModalidade(bufferCadFavorecido.toString());
		servicosModalidadesDTO.setImprimeLinhaServico("S");

		listaServModalidades.add(servicosModalidadesDTO);

	}
		
		// Recadastramento de Benefici�rios
		int exibeLinhaRecadastramento = saidaSegundoContrato
		.getQtModalidadeRecadastro() / 2;
		for (int i = 0; i < saidaSegundoContrato.getQtModalidadeRecadastro(); i++) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico(saidaSegundoContrato
					.getDsServicoRecadastro());
			servicosModalidadesDTO
			.setDsModalidade(saidaSegundoContrato
					.getListaModRecadastro().get(i)
					.getCdModalidadeRecadastro());
			if (saidaSegundoContrato.getQtModalidadeRecadastro() % 2 == 0) {
				if (i + 1 == exibeLinhaRecadastramento) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			} else {
				if (i == exibeLinhaRecadastramento) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			}

			listaServModalidades.add(servicosModalidadesDTO);
		}

		if(!"".equals(getSaidaSegundoContrato().getCdIndicadorAvisoPagador())){
			listaServModalidades.add(new ServicosModalidadesDTO(getSaidaSegundoContrato().getCdIndicadorAvisoPagador(), 
					"", "S"));
		}
		
		if(!"".equals(getSaidaSegundoContrato().getCdIndicadorAvisoFavorecido())){
			listaServModalidades.add(new ServicosModalidadesDTO(getSaidaSegundoContrato().getCdIndicadorAvisoFavorecido(), 
					"", "S"));
		}
		
		if(!"".equals(getSaidaSegundoContrato().getCdIndicadorComplementoSalarial())){
			listaServModalidades.add(new ServicosModalidadesDTO(getSaidaSegundoContrato().getCdIndicadorComplementoSalarial(), 
					"", "S"));
		}
		
		if(!"".equals(getSaidaSegundoContrato().getCdIndicadorComplementoPagador())){
			listaServModalidades.add(new ServicosModalidadesDTO(getSaidaSegundoContrato().getCdIndicadorComplementoPagador(), 
					"", "S"));
		}
		
		if(!"".equals(getSaidaSegundoContrato().getCdIndicadorComplementoFavorecido())){
			listaServModalidades.add(new ServicosModalidadesDTO(getSaidaSegundoContrato().getCdIndicadorComplementoFavorecido(), 
					"", "S"));
		}
		
		if(!"".equals(getSaidaSegundoContrato().getCdIndicadorComplementoDivergente())){
			listaServModalidades.add(new ServicosModalidadesDTO(getSaidaSegundoContrato().getCdIndicadorComplementoDivergente(), 
					"", "S"));
		}
		
		if(!"".equals(getSaidaSegundoContrato().getCdIndcadorCadastroFavorecido())){
			listaServModalidades.add(new ServicosModalidadesDTO(getSaidaSegundoContrato().getCdIndcadorCadastroFavorecido(), 
					"", "S"));
		}
		
		
		setListaServicosModalidades(listaServModalidades);	
		setListaTitulos(listaFornecTitulos);
		setListaFornModularidades(listaFornecModularidades);
		setListaFornecedorOutrasModularidades(listaFornecOutrasModularidades);
		setListaSalariosMod(listaSalariosModularidades);

		listaPagamentosFornec = new ArrayList<PagamentosDTO>();
		listaPagamentosFornec.add(pagamentosFornecedoresDTO);

		listaPagamentosSalarios = new ArrayList<PagamentosDTO>();
		listaPagamentosSalarios.add(pagamentosSalariosDTO);

	}

	/**
	 * Carregar dados lista servicos.
	 */
	private void carregarDadosListaServicos(){
		ServicosDTO servicosDTO = new ServicosDTO();
		boolean avisoFavorecido = false;
		boolean avisoPagador = false;
		boolean comprovanteFavorecido = false;
		boolean comprovantePagador = false;

		for (OcorrenciasAvisos ocorrenciasAvisos : saidaSegundoContrato
				.getListaAvisos()) {

			if (!ocorrenciasAvisos.getDsAvisoTipoAviso().trim().equals("")) {
				if (ocorrenciasAvisos.getDsAvisoTipoAviso().equalsIgnoreCase(
				"F")) {
					avisoFavorecido = true;
					servicosDTO
					.setDsAvisoTipoAvisoFavorecido("SERVI�O: AVISO MOVIMENTA��O FAVORECIDO");

					servicosDTO
					.setDsAvisoPeriodicidadeEmissaoFavorecido(ocorrenciasAvisos
							.getDsAvisoPeriodicidadeEmissao());
					servicosDTO.setDsAvisoDestinoFavorecido(ocorrenciasAvisos
							.getDsAvisoDestino());
					servicosDTO
					.setDsAvisoPermissaoCorrespondenciaAberturaFavorecido(ocorrenciasAvisos
							.getDsAvisoPermissaoCorrespondenciaAbertura());
					servicosDTO
					.setDsAvisoDemontracaoInformacaoReservadaFavorecido(ocorrenciasAvisos
							.getDsAvisoDemontracaoInformacaoReservada());
					servicosDTO
					.setCdAvisoQuantidadeViasEmitirFavorecido(ocorrenciasAvisos
							.getCdAvisoQuantidadeViasEmitir()
							.toString());
					servicosDTO
					.setDsAvisoAgrupamentoCorrespondenciaFavorecido(ocorrenciasAvisos
							.getDsAvisoAgrupamentoCorrespondencia());

				} else {
					avisoPagador = true;
					servicosDTO
					.setDsAvisoTipoAvisoPagador("SERVI�O: AVISO MOVIMENTA��O PAGADOR");

					servicosDTO
					.setDsAvisoPeriodicidadeEmissaoPagador(ocorrenciasAvisos
							.getDsAvisoPeriodicidadeEmissao());
					servicosDTO.setDsAvisoDestinoPagador(ocorrenciasAvisos
							.getDsAvisoDestino());
					servicosDTO
					.setDsAvisoPermissaoCorrespondenciaAberturaPagador(ocorrenciasAvisos
							.getDsAvisoPermissaoCorrespondenciaAbertura());
					servicosDTO
					.setDsAvisoDemontracaoInformacaoReservadaPagador(ocorrenciasAvisos
							.getDsAvisoDemontracaoInformacaoReservada());
					servicosDTO
					.setCdAvisoQuantidadeViasEmitirPagador(ocorrenciasAvisos
							.getCdAvisoQuantidadeViasEmitir()
							.toString());
					servicosDTO
					.setDsAvisoAgrupamentoCorrespondenciaPagador(ocorrenciasAvisos
							.getDsAvisoAgrupamentoCorrespondencia());
				}
			} else {
				if (!avisoFavorecido) {
					servicosDTO.setDsAvisoTipoAvisoFavorecido("N");
				}

				if (!avisoPagador) {
					servicosDTO.setDsAvisoTipoAvisoPagador("N");
				}
			}

		}

		for (OcorrenciasComprovantes ocorrenciasComprovantes : saidaSegundoContrato
				.getListaComprovantes()) {
			if (!comprovanteFavorecido || !comprovantePagador) {
				if (!ocorrenciasComprovantes.getCdComprovanteTipoComprovante()
						.trim().equals("")) {
					if (ocorrenciasComprovantes
							.getCdComprovanteTipoComprovante()
							.equalsIgnoreCase("F")) {
						comprovanteFavorecido = true;

						servicosDTO
						.setCdComprovanteTipoComprovanteFavorecido("SERVI�O: COMPROVANTE PAGAMENTO FAVORECIDO");

						servicosDTO
						.setDsComprovantePeriodicidadeEmissaoFavorecido(ocorrenciasComprovantes
								.getDsComprovantePeriodicidadeEmissao());
						servicosDTO
						.setDsComprovanteDestinoFavorecido(ocorrenciasComprovantes
								.getDsComprovanteDestino());
						servicosDTO
						.setDsComprovantePermissaoCorrespondenteAberturaFavorecido(ocorrenciasComprovantes
								.getDsComprovantePermissaoCorrespondenteAbertura());
						servicosDTO
						.setDsDemonstrativoInformacaoReservadaFavorecido(ocorrenciasComprovantes
								.getDsDemonstrativoInformacaoReservada());
						servicosDTO
						.setQtViasEmitirFavorecido(ocorrenciasComprovantes
								.getQtViasEmitir().toString());
						servicosDTO
						.setDsAgrupamentoCorrespondenteFavorecido(ocorrenciasComprovantes
								.getDsAgrupamentoCorrespondente());
					} else {
						comprovantePagador = true;

						servicosDTO
						.setCdComprovanteTipoComprovantePagador("SERVI�O: COMPROVANTE PAGAMENTO PAGADOR");

						servicosDTO
						.setDsComprovantePeriodicidadeEmissaoPagador(ocorrenciasComprovantes
								.getDsComprovantePeriodicidadeEmissao());
						servicosDTO
						.setDsComprovanteDestinoPagador(ocorrenciasComprovantes
								.getDsComprovanteDestino());
						servicosDTO
						.setDsComprovantePermissaoCorrespondenteAberturaPagador(ocorrenciasComprovantes
								.getDsComprovantePermissaoCorrespondenteAbertura());
						servicosDTO
						.setDsDemonstrativoInformacaoReservadaPagador(ocorrenciasComprovantes
								.getDsDemonstrativoInformacaoReservada());
						servicosDTO
						.setQtViasEmitirPagador(ocorrenciasComprovantes
								.getQtViasEmitir().toString());
						servicosDTO
						.setDsAgrupamentoCorrespondentePagador(ocorrenciasComprovantes
								.getDsAgrupamentoCorrespondente());
					}
				} else {
					if (!comprovanteFavorecido) {
						servicosDTO
						.setCdComprovanteTipoComprovanteFavorecido("N");
					}

					if (!comprovantePagador) {
						servicosDTO.setCdComprovanteTipoComprovantePagador("N");
					}
				}
			}
		}

		boolean compSalarial = false;
		boolean compDiversos = false;
		for (OcorrenciasComprovanteSalariosDiversos ocorrenciasComprovanteSalariosDiversos : saidaSegundoContrato
				.getListaComprovanteSalarioDiversos()) {

			if (!compSalarial || !compDiversos) {
				if (!ocorrenciasComprovanteSalariosDiversos
						.getDsCsdTipoComprovante().trim().equals("")) {
					if (ocorrenciasComprovanteSalariosDiversos
							.getDsCsdTipoComprovante().equalsIgnoreCase("S")) {
						compSalarial = true;

						servicosDTO
						.setDsCsdTipoComprovanteSalarial("SERVI�O: COMPROVANTE SALARIAL");

						servicosDTO
						.setDsCsdTipoRejeicaoLoteSalarial(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdTipoRejeicaoLote());
						servicosDTO
						.setDsCsdMeioDisponibilizacaoCorrentistaSalarial(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdMeioDisponibilizacaoCorrentista());
						servicosDTO
						.setDsCsdMediaDisponibilidadeCorrentistaSalarial(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdMediaDisponibilidadeCorrentista());
						servicosDTO
						.setDsCsdMediaDisponibilidadeNumeroCorrentistasSalarial(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdMediaDisponibilidadeNumeroCorrentistas());
						servicosDTO
						.setDsCsdDestinoSalarial(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdDestino());
						servicosDTO
						.setQtCsdMesEmissaoSalarial(ocorrenciasComprovanteSalariosDiversos
								.getQtCsdMesEmissao().toString());
						servicosDTO
						.setDsCsdCadastroConservadorEnderecoSalarial(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdCadastroConservadorEndereco());
						servicosDTO
						.setCdCsdQuantidadeViasEmitirSalarial(ocorrenciasComprovanteSalariosDiversos
								.getCdCsdQuantidadeViasEmitir() == 0 ? "N"
										: ocorrenciasComprovanteSalariosDiversos
										.getCdCsdQuantidadeViasEmitir()
										.toString());
					} else {
						compDiversos = true;

						servicosDTO
						.setDsCsdTipoComprovanteDiversos("SERVI�O: COMPROVANTE DIVERSOS");

						servicosDTO
						.setDsCsdTipoRejeicaoLoteDiversos(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdTipoRejeicaoLote());
						servicosDTO
						.setDsCsdMeioDisponibilizacaoCorrentistaDiversos(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdMeioDisponibilizacaoCorrentista());
						servicosDTO
						.setDsCsdMediaDisponibilidadeCorrentistaDiversos(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdMediaDisponibilidadeCorrentista());
						servicosDTO
						.setDsCsdMediaDisponibilidadeNumeroCorrentistasDiversos(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdMediaDisponibilidadeNumeroCorrentistas());
						servicosDTO
						.setDsCsdDestinoDiversos(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdDestino());
						servicosDTO
						.setQtCsdMesEmissaoDiversos(ocorrenciasComprovanteSalariosDiversos
								.getQtCsdMesEmissao().toString());
						servicosDTO
						.setDsCsdCadastroConservadorEnderecoDiversos(ocorrenciasComprovanteSalariosDiversos
								.getDsCsdCadastroConservadorEndereco());
						servicosDTO
						.setCdCsdQuantidadeViasEmitirDiversos(ocorrenciasComprovanteSalariosDiversos
								.getCdCsdQuantidadeViasEmitir() == 0 ? "N"
										: ocorrenciasComprovanteSalariosDiversos
										.getCdCsdQuantidadeViasEmitir()
										.toString());
					}
				} else {
					if (!compSalarial) {
						servicosDTO.setDsCsdTipoComprovanteSalarial("N");
					}

					if (!compDiversos) {
						servicosDTO.setDsCsdTipoComprovanteDiversos("N");
					}
				}
			}
		}

		servicosDTO
		.setCdIndcadorCadastroFavorecido(saidaSegundoContrato
				.getCdIndcadorCadastroFavorecido()
				.equalsIgnoreCase("S") ? saidaSegundoContrato
						.getCdIndcadorCadastroFavorecido() : "N");

		servicosDTO.setDsCadastroFormaManutencao(saidaSegundoContrato
				.getDsCadastroFormaManutencao());
		servicosDTO.setCdCadastroQuantidadeDiasInativos(saidaSegundoContrato
				.getCdCadastroQuantidadeDiasInativos().toString());
		servicosDTO.setDsCadastroTipoConsistenciaInscricao(saidaSegundoContrato
				.getDsCadastroTipoConsistenciaInscricao());

		servicosDTO.setDsServicoRecadastro(saidaSegundoContrato
				.getDsServicoRecadastro().trim().equals("") ? "N"
						: saidaSegundoContrato.getDsServicoRecadastro());
		servicosDTO.setDsRecTipoIdentificacaoBeneficio(saidaSegundoContrato
				.getDsRecTipoIdentificacaoBeneficio());
		servicosDTO.setDsRecTipoConsistenciaIdentificacao(saidaSegundoContrato
				.getDsRecTipoConsistenciaIdentificacao());
		servicosDTO.setDsRecCondicaoEnquadramento(saidaSegundoContrato
				.getDsRecCondicaoEnquadramento());
		servicosDTO.setDsRecCriterioPrincipalEnquadramento(saidaSegundoContrato
				.getDsRecCriterioPrincipalEnquadramento());
		servicosDTO
		.setDsRecTipoCriterioCompostoEnquadramento(saidaSegundoContrato
				.getDsRecTipoCriterioCompostoEnquadramento());
		servicosDTO.setQtRecEtapaRecadastramento(saidaSegundoContrato
				.getQtRecEtapaRecadastramento().toString());
		servicosDTO.setQtRecFaseEtapaRecadastramento(saidaSegundoContrato
				.getQtRecFaseEtapaRecadastramento().toString());
		servicosDTO.setDtRecInicioRecadastramento(saidaSegundoContrato
				.getDtRecInicioRecadastramento());
		servicosDTO.setDtRecFinalRecadastramento(saidaSegundoContrato
				.getDtRecFinalRecadastramento());
		servicosDTO.setDsRecGeradorRetornoInternet(saidaSegundoContrato
				.getDsRecGeradorRetornoInternet());

		boolean aviso = false;
		boolean formulario = false;

		for (OcorrenciasAvisoFormularios ocorrenciasAvisoFormularios : saidaSegundoContrato
				.getListaAvisoFormularios()) {
			if (!aviso || !formulario) {
				if (!ocorrenciasAvisoFormularios.getDsRecTipoModalidade()
						.trim().equals("")) {
					if (ocorrenciasAvisoFormularios.getDsRecTipoModalidade()
							.equalsIgnoreCase("A")) {
						aviso = true;

						servicosDTO
						.setDsRecTipoModalidadeAviso("AVISO DE RECADASTRAMENTO");

						servicosDTO
						.setDsRecMomentoEnvioAviso(ocorrenciasAvisoFormularios
								.getDsRecMomentoEnvio());
						servicosDTO
						.setDsRecDestinoAviso(ocorrenciasAvisoFormularios
								.getDsRecDestino());
						servicosDTO
						.setDsRecCadastroEnderecoUtilizadoAviso(ocorrenciasAvisoFormularios
								.getDsRecCadastroEnderecoUtilizado());
						servicosDTO
						.setQtRecDiaAvisoAntecipadoAviso(ocorrenciasAvisoFormularios
								.getQtRecDiaAvisoAntecipado()
								.toString());
						servicosDTO
						.setDsRecPermissaoAgrupamentoAviso(ocorrenciasAvisoFormularios
								.getDsRecPermissaoAgrupamento());
					} else {
						formulario = true;

						servicosDTO
						.setDsRecTipoModalidadeFormulario("FORMUL�RIO DE RECADASTRAMENTO");

						servicosDTO
						.setDsRecMomentoEnvioFormulario(ocorrenciasAvisoFormularios
								.getDsRecMomentoEnvio());
						servicosDTO
						.setDsRecDestinoFormulario(ocorrenciasAvisoFormularios
								.getDsRecDestino());
						servicosDTO
						.setDsRecCadastroEnderecoUtilizadoFormulario(ocorrenciasAvisoFormularios
								.getDsRecCadastroEnderecoUtilizado());
						servicosDTO
						.setQtRecDiaAntecipadoFormulario(ocorrenciasAvisoFormularios
								.getQtRecDiaAvisoAntecipado()
								.toString());
						servicosDTO
						.setDsRecPermissaoAgrupamentoFormulario(ocorrenciasAvisoFormularios
								.getDsRecPermissaoAgrupamento());
					}
				} else {
					if (!aviso) {
						servicosDTO.setDsRecTipoModalidadeAviso("N");
					}

					if (!formulario) {
						servicosDTO.setDsRecTipoModalidadeFormulario("N");
					}
				}
			}
		}

		listaServicos = new ArrayList<ServicosDTO>();
		listaServicos.add(servicosDTO);

		listaServicosCompl = new ArrayList<ServicosDTO>();
		listaServicosCompl.add(servicosDTO);
	}


	/**
	 * Imprimir anexo contrato.
	 *
	 * @throws IOException the IO exception
	 */
	public void imprimirAnexoContrato() throws IOException {

		String caminho = "/images/Bradesco_logo.JPG";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
		String logoPath = servletContext.getRealPath(caminho);

		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");

		// Dados de Participantes
		String pathArquivos = "/relatorios/manutencaoContrato/manterContrato/anexoContrato";
		String arquivoParticipantes = pathArquivos + "/relParticipantes.jasper";
		String pathSubRelParticipantes = servletContext.getRealPath(arquivoParticipantes);

		// Dados de contas
		String arquivoContas = pathArquivos + "/relContas.jasper";
		String pathSubRelContas = servletContext.getRealPath(arquivoContas);

		// Configura��o de Layout
		String arquivoLayouts = pathArquivos + "/relLayouts.jasper";
		String pathSubRelLayouts = servletContext.getRealPath(arquivoLayouts);
		
		// Configura��o de Cesta de Tarifas
		String arquivoCestasDeTarifas = pathArquivos + "/reCestasDeTarifas.jasper";
		String pathSubRelCestasTarifas = servletContext.getRealPath(arquivoCestasDeTarifas);

		String arquivoServModalidades = pathArquivos + "/relServicosModalidades.jasper";
		String pathSubServModalidades = servletContext.getRealPath(arquivoServModalidades);

		// Pagamentos
		String arquivoPagamentosFornecedores = pathArquivos + "/relPagamentosFornecedores.jasper";
		String pathSubRelPagamentosFornecedores = servletContext.getRealPath(arquivoPagamentosFornecedores);

		String arquivoPagamentosFornecedoresTitulos = pathArquivos + "/relPagamentosFornecedoresTitulos.jasper";
		String pathSubRelPagamentosFornecedoresTitulos = 
			servletContext.getRealPath(arquivoPagamentosFornecedoresTitulos);

		String arquivoPagamentosFornecedoresModularidades = 
			pathArquivos + "/relPagamentosFornecedoresModularidades.jasper";
		String pathSubRelPagamentosFornecedoresModularidades = 
			servletContext.getRealPath(arquivoPagamentosFornecedoresModularidades);

		String arquivoPagamentosFornecedoresOutrasModularidades = 
			pathArquivos + "/relPagamentosFornecedoresOutrasModularidades.jasper";
		String pathSubRelPagamentosFornecedoresOutrasModularidades = 
			servletContext.getRealPath(arquivoPagamentosFornecedoresOutrasModularidades);

		String arquivoPagamentosFornecedoresCompl = pathArquivos + "/relPagamentosFornecedoresCompl.jasper";
		String pathSubRelPagamentosFornecedoresCompl = servletContext.getRealPath(arquivoPagamentosFornecedoresCompl);

		String arquivoPagamentosTributos = pathArquivos + "/relPagamentosTributos.jasper";
		String pathSubRelPagamentosTributos = servletContext.getRealPath(arquivoPagamentosTributos);

		String arquivoTributos = pathArquivos + "/relPagamentosTributosModalidades.jasper";
		String pathSubRelTributos = servletContext.getRealPath(arquivoTributos);

		String arquivoPagamentosSalarios = pathArquivos + "/relPagamentosSalarios.jasper";
		String pathSubRelPagamentosSalarios = servletContext.getRealPath(arquivoPagamentosSalarios);

		String arquivoPagamentosSalariosModularidades = pathArquivos + "/relPagamentosSalariosModularidades.jasper";
		String pathSubRelPagamentosSalariosModularidades = 
			servletContext.getRealPath(arquivoPagamentosSalariosModularidades);

		String arquivoPagamentosBeneficios = pathArquivos + "/relPagamentosBeneficios.jasper";
		String pathSubRelPagamentosBeneficios = servletContext.getRealPath(arquivoPagamentosBeneficios);

		String arquivoPagamentosBeneficiosModularidades = pathArquivos + "/relPagamentosBeneficiosModularidades.jasper";
		String pathSubRelPagamentosBeneficiosModularidades = 
			servletContext.getRealPath(arquivoPagamentosBeneficiosModularidades);			

		//Servicos
		String servicos = pathArquivos + "/relServicos.jasper";
		String pathSubRelServicos = servletContext.getRealPath(servicos);

		String servicosCompl = pathArquivos + "/relServicosCompl.jasper";
		String pathSubRelServicosCompl = servletContext.getRealPath(servicosCompl);

		//Tarifas

		String tarifas = pathArquivos + "/relTarifas.jasper";
		String pathSubRelTarifas = servletContext.getRealPath(tarifas);


		JRBeanCollectionDataSource beanParamServModalidades = new JRBeanCollectionDataSource(
				getListaServicosModalidades());
		JRBeanCollectionDataSource beanParamFornecTitulos = new JRBeanCollectionDataSource(
				getListaTitulos());
		JRBeanCollectionDataSource beanParamFornecModularidades = new JRBeanCollectionDataSource(
				getListaFornModularidades());
		JRBeanCollectionDataSource beanParamFornecOutrasModularidades = new JRBeanCollectionDataSource(
				getListaFornecedorOutrasModularidades());
		JRBeanCollectionDataSource beanParamPagamentosTributos = new JRBeanCollectionDataSource(
				listaPagamentosTri);
		JRBeanCollectionDataSource beanParamTributos = new JRBeanCollectionDataSource(
				listaTri);					
		JRBeanCollectionDataSource beanParamPagamentosFornecedores = new JRBeanCollectionDataSource(
				listaPagamentosFornec);
		JRBeanCollectionDataSource beanParamPagamentosFornecedoresCompl = new JRBeanCollectionDataSource(
				listaPagamentosFornec);		
		JRBeanCollectionDataSource beanParamPagamentosSalarios = new JRBeanCollectionDataSource(
				listaPagamentosSalarios);
		JRBeanCollectionDataSource beanParamSalariosModularidades = new JRBeanCollectionDataSource(
				getListaSalariosMod());		
		JRBeanCollectionDataSource beanParamPagamentosBeneficios = new JRBeanCollectionDataSource(
				listaPagamentosBeneficios);
		JRBeanCollectionDataSource beanParamPagamentosBeneficiosModularidades = new JRBeanCollectionDataSource(
				listaBeneficiosModularidades);	 		
		JRBeanCollectionDataSource beanParamServicos = new JRBeanCollectionDataSource(
				listaServicos);		
		JRBeanCollectionDataSource beanParamServicosCompl = new JRBeanCollectionDataSource(
				listaServicosCompl);			
		JRBeanCollectionDataSource beanParamParticipantes = new JRBeanCollectionDataSource(
				listaParticipantes);
		JRBeanCollectionDataSource beanParamContas = new JRBeanCollectionDataSource(
				getListaContas());
		JRBeanCollectionDataSource beanParamLayout = new JRBeanCollectionDataSource(
				listaLayouts);
		JRBeanCollectionDataSource beanParamCestasTarifas = new JRBeanCollectionDataSource(
				listaConfCestasTarifas);
		JRBeanCollectionDataSource beanParamTarifas = new JRBeanCollectionDataSource(
				listaGridTarifasModalidade);

		// Par�metros do Relat�rio
		Map<String, Object> par = new HashMap<String, Object>();
		par.put("exibePgtoFornecedor", exibePgtoFornecedor);
		par.put("exibePgtoTributos", exibePgtoTributos);
		par.put("exibePgtoSalarios", exibePgtoSalarios);
		par.put("exibePgtoBeneficios", exibePgtoBeneficios);		

		par.put("data", formatoData.format(new Date()));
		par.put("hora", formatoHora.format(new Date()));
		par.put("logoBradesco", logoPath);
		par.put("participantes", pathSubRelParticipantes);
		par.put("contas", pathSubRelContas);
		par.put("layouts", pathSubRelLayouts);
		par.put("cestaTarifas", pathSubRelCestasTarifas);
		par.put("servModal", pathSubServModalidades);
		par.put("pagamentosFornecedores", pathSubRelPagamentosFornecedores);
		par.put("pagamentosFornecedoresTitulos", pathSubRelPagamentosFornecedoresTitulos);
		par.put("pagamentosFornecedoresModularidades", pathSubRelPagamentosFornecedoresModularidades);
		par.put("pagamentosFornecedoresOutrasModularidades", pathSubRelPagamentosFornecedoresOutrasModularidades);
		par.put("pagamentosFornecedoresCompl", pathSubRelPagamentosFornecedoresCompl);
		par.put("pagamentosTributos", pathSubRelPagamentosTributos);
		par.put("tributos", pathSubRelTributos);
		par.put("pagamentosSalarios", pathSubRelPagamentosSalarios);
		par.put("pagamentosSalariosModularidades", pathSubRelPagamentosSalariosModularidades);
		par.put("pagamentosBeneficios", pathSubRelPagamentosBeneficios);
		par.put("pagamentosBeneficiosModularidades", pathSubRelPagamentosBeneficiosModularidades);
		par.put("servicos", pathSubRelServicos);
		par.put("tarifas", pathSubRelTarifas);
		par.put("servicosCompl", pathSubRelServicosCompl);
		par.put("listaParticipantes", beanParamParticipantes);
		par.put("listaContas", beanParamContas);
		par.put("listaLayouts", beanParamLayout);
		par.put("listaCestasTarifas", beanParamCestasTarifas);
		par.put("listaServModal", beanParamServModalidades);
		par.put("listaPagamentosFornecedores", beanParamPagamentosFornecedores);
		par.put("listaFornecTitulos", beanParamFornecTitulos);
		par.put("listaFornecModularidades", beanParamFornecModularidades);
		par.put("listaFornecOutrasModularidades", beanParamFornecOutrasModularidades);
		par.put("listaPagamentosFornecedoresCompl", beanParamPagamentosFornecedoresCompl);
		par.put("listaPagamentosTributos", beanParamPagamentosTributos);
		par.put("listaTributos", beanParamTributos);
		par.put("listaPagamentosSalarios", beanParamPagamentosSalarios);
		par.put("listaSalariosModularidades", beanParamSalariosModularidades);
		par.put("listaPagamentosBeneficios", beanParamPagamentosBeneficios);
		par.put("listaPagamentosBeneficiosModularidades", beanParamPagamentosBeneficiosModularidades);
		par.put("listaServicos", beanParamServicos);
		par.put("listaServicosCompl", beanParamServicosCompl);
		par.put("listaGridTarifasModalidade", beanParamTarifas);
		par.put("titulo", "ANEXO I - PAGAMENTO INTEGRADO BRADESCO");
		par.put("imprimirGridTarifa", getImprimirGridTarifa());
		par.put("exibeCestaTarifas", getExibeCestaTarifas());

		try {

			PgitUtil.geraRelatorioPdf("/relatorios/manutencaoContrato/manterContrato/anexoContrato/",
					"imprimirAnexoContrato", listaAnexoContrato, par);

		} catch (Exception e) {
			throw new BradescoViewException(e.getMessage(), e, "", "",
					BradescoViewExceptionActionType.ACTION);
		}			
	}
	
	
	/**
	 * Imprimir resumo contrato.
	 *
	 * @throws IOException the IO exception
	 */
	public void imprimirResumoContrato() throws IOException {
		
		String caminho = "/images/Bradesco_logo.JPG";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
		String logoPath = servletContext.getRealPath(caminho);
		
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
		
		// Dados de Participantes
		String pathArquivos = "/relatorios/manutencaoContrato/manterContrato/resumoContrato";
		String arquivoParticipantes = pathArquivos + "/relParticipantes.jasper";
		String pathSubRelParticipantes = servletContext.getRealPath(arquivoParticipantes);
		
		// Dados de contas
		String arquivoContas = pathArquivos + "/relContas.jasper";
		String pathSubRelContas = servletContext.getRealPath(arquivoContas);
		
		// Configura��o de Layout
		String arquivoLayouts = pathArquivos + "/relLayouts.jasper";
		String pathSubRelLayouts = servletContext.getRealPath(arquivoLayouts);
		
		// Configura��o de Cestas de Tarifas		
		String arquivoCestasTarifas = pathArquivos + "/relCestasDeTarifas.jasper";
		String pathSubRelCestasTarifas = servletContext.getRealPath(arquivoCestasTarifas);
		
		String arquivoServModalidades = pathArquivos + "/relServicosModalidades.jasper";
		String pathSubServModalidades = servletContext.getRealPath(arquivoServModalidades);
		
		// Pagamentos
		String arquivoPagamentosFornecedores = pathArquivos + "/relPagamentosFornecedores.jasper";
		String pathSubRelPagamentosFornecedores = servletContext.getRealPath(arquivoPagamentosFornecedores);
		
		String arquivoPagamentosFornecedoresTitulos = pathArquivos + "/relPagamentosFornecedoresTitulos.jasper";
		String pathSubRelPagamentosFornecedoresTitulos = 
			servletContext.getRealPath(arquivoPagamentosFornecedoresTitulos);
		
		String arquivoPagamentosFornecedoresModularidades = 
			pathArquivos + "/relPagamentosFornecedoresModularidades.jasper";
		String pathSubRelPagamentosFornecedoresModularidades = 
			servletContext.getRealPath(arquivoPagamentosFornecedoresModularidades);
		
		String arquivoPagamentosFornecedoresOutrasModularidades = 
			pathArquivos + "/relPagamentosFornecedoresOutrasModularidades.jasper";
		String pathSubRelPagamentosFornecedoresOutrasModularidades = 
			servletContext.getRealPath(arquivoPagamentosFornecedoresOutrasModularidades);
		
		String arquivoPagamentosFornecedoresCompl = pathArquivos + "/relPagamentosFornecedoresCompl.jasper";
		String pathSubRelPagamentosFornecedoresCompl = servletContext.getRealPath(arquivoPagamentosFornecedoresCompl);
		
		String arquivoPagamentosTributos = pathArquivos + "/relPagamentosTributos.jasper";
		String pathSubRelPagamentosTributos = servletContext.getRealPath(arquivoPagamentosTributos);
		
		String arquivoTributos = pathArquivos + "/relPagamentosTributosModalidades.jasper";
		String pathSubRelTributos = servletContext.getRealPath(arquivoTributos);
		
		String arquivoPagamentosSalarios = pathArquivos + "/relPagamentosSalarios.jasper";
		String pathSubRelPagamentosSalarios = servletContext.getRealPath(arquivoPagamentosSalarios);
		
		String arquivoPagamentosSalariosModularidades = pathArquivos + "/relPagamentosSalariosModularidades.jasper";
		String pathSubRelPagamentosSalariosModularidades = 
			servletContext.getRealPath(arquivoPagamentosSalariosModularidades);
		
		String arquivoPagamentosBeneficios = pathArquivos + "/relPagamentosBeneficios.jasper";
		String pathSubRelPagamentosBeneficios = servletContext.getRealPath(arquivoPagamentosBeneficios);
		
		String arquivoPagamentosBeneficiosModularidades = pathArquivos + "/relPagamentosBeneficiosModularidades.jasper";
		String pathSubRelPagamentosBeneficiosModularidades = 
			servletContext.getRealPath(arquivoPagamentosBeneficiosModularidades);			
		
		//Servicos
		String servicos = pathArquivos + "/relServicos.jasper";
		String pathSubRelServicos = servletContext.getRealPath(servicos);
		
		String servicosCompl = pathArquivos + "/relServicosCompl.jasper";
		String pathSubRelServicosCompl = servletContext.getRealPath(servicosCompl);
		
		//Tarifas
		
		String tarifas = pathArquivos + "/relTarifas.jasper";
		String pathSubRelTarifas = servletContext.getRealPath(tarifas);
		
		
		JRBeanCollectionDataSource beanParamServModalidades = new JRBeanCollectionDataSource(
				getListaServicosModalidades());
		JRBeanCollectionDataSource beanParamFornecTitulos = new JRBeanCollectionDataSource(
				getListaTitulos());
		JRBeanCollectionDataSource beanParamFornecModularidades = new JRBeanCollectionDataSource(
				getListaFornModularidades());

		List<PagamentosDTO> listaFornecedorOutrasModularidadesRelatorio = new ArrayList<PagamentosDTO>();
		
		for (int i = 0; i < getListaFornecedorOutrasModularidades().size(); i++){
			if (!getListaFornecedorOutrasModularidades().get(i).getDsModTipoServico().equalsIgnoreCase("PAGAMENTO VIA ORDEM DE CR�DITO") 
					&& !getListaFornecedorOutrasModularidades().get(i).getDsModTipoServico().equalsIgnoreCase("PAGAMENTO DEP�SITO IDENTIFICAD")) {
				listaFornecedorOutrasModularidadesRelatorio.add(getListaFornecedorOutrasModularidades().get(i));
			}
		}
		JRBeanCollectionDataSource beanParamFornecOutrasModularidades = new JRBeanCollectionDataSource(
				listaFornecedorOutrasModularidadesRelatorio);
//		JRBeanCollectionDataSource beanParamFornecOutrasModularidades = new JRBeanCollectionDataSource(
//				getListaFornecedorOutrasModularidades());
		JRBeanCollectionDataSource beanParamPagamentosTributos = new JRBeanCollectionDataSource(
				listaPagamentosTri);
		JRBeanCollectionDataSource beanParamTributos = new JRBeanCollectionDataSource(
				listaTri);					
		JRBeanCollectionDataSource beanParamPagamentosFornecedores = new JRBeanCollectionDataSource(
				listaPagamentosFornec);
		JRBeanCollectionDataSource beanParamPagamentosFornecedoresCompl = new JRBeanCollectionDataSource(
				listaPagamentosFornec);		
		JRBeanCollectionDataSource beanParamPagamentosSalarios = new JRBeanCollectionDataSource(
				listaPagamentosSalarios);
		JRBeanCollectionDataSource beanParamSalariosModularidades = new JRBeanCollectionDataSource(
				getListaSalariosMod());		
		JRBeanCollectionDataSource beanParamPagamentosBeneficios = new JRBeanCollectionDataSource(
				listaPagamentosBeneficios);
		JRBeanCollectionDataSource beanParamPagamentosBeneficiosModularidades = new JRBeanCollectionDataSource(
				listaBeneficiosModularidades);	 		
		JRBeanCollectionDataSource beanParamServicos = new JRBeanCollectionDataSource(
				listaServicos);		
		JRBeanCollectionDataSource beanParamServicosCompl = new JRBeanCollectionDataSource(
				listaServicosCompl);			
		JRBeanCollectionDataSource beanParamParticipantes = new JRBeanCollectionDataSource(
				listaParticipantes);
		JRBeanCollectionDataSource beanParamContas = new JRBeanCollectionDataSource(
				getListaContas());
		JRBeanCollectionDataSource beanParamLayout = new JRBeanCollectionDataSource(
				listaLayouts);
		JRBeanCollectionDataSource beanParamCestaTarifas = new JRBeanCollectionDataSource(
				listaConfCestasTarifas);
		JRBeanCollectionDataSource beanParamTarifas = new JRBeanCollectionDataSource(
				listaGridTarifasModalidade);		
		
		// Par�metros do Relat�rio
		Map<String, Object> par = new HashMap<String, Object>();
		par.put("exibePgtoFornecedor", exibePgtoFornecedor);
		par.put("exibePgtoTributos", exibePgtoTributos);
		par.put("exibePgtoSalarios", exibePgtoSalarios);
		par.put("exibePgtoBeneficios", exibePgtoBeneficios);		
		
		par.put("data", formatoData.format(new Date()));
		par.put("hora", formatoHora.format(new Date()));
		par.put("logoBradesco", logoPath);
		par.put("participantes", pathSubRelParticipantes);
		par.put("contas", pathSubRelContas);
		par.put("layouts", pathSubRelLayouts);
		par.put("cestaTarifas", pathSubRelCestasTarifas);
		par.put("servModal", pathSubServModalidades);
		par.put("pagamentosFornecedores", pathSubRelPagamentosFornecedores);
		par.put("pagamentosFornecedoresTitulos", pathSubRelPagamentosFornecedoresTitulos);
		par.put("pagamentosFornecedoresModularidades", pathSubRelPagamentosFornecedoresModularidades);
		par.put("pagamentosFornecedoresOutrasModularidades", pathSubRelPagamentosFornecedoresOutrasModularidades);
		par.put("pagamentosFornecedoresCompl", pathSubRelPagamentosFornecedoresCompl);
		par.put("pagamentosTributos", pathSubRelPagamentosTributos);
		par.put("tributos", pathSubRelTributos);
		par.put("pagamentosSalarios", pathSubRelPagamentosSalarios);
		par.put("pagamentosSalariosModularidades", pathSubRelPagamentosSalariosModularidades);
		par.put("pagamentosBeneficios", pathSubRelPagamentosBeneficios);
		par.put("pagamentosBeneficiosModularidades", pathSubRelPagamentosBeneficiosModularidades);
		par.put("servicos", pathSubRelServicos);
		par.put("tarifas", pathSubRelTarifas);
		par.put("servicosCompl", pathSubRelServicosCompl);
		par.put("listaParticipantes", beanParamParticipantes);
		par.put("listaContas", beanParamContas);
		par.put("listaLayouts", beanParamLayout);
		par.put("listaCestasTarifas", beanParamCestaTarifas);		
		par.put("listaServModal", beanParamServModalidades);
		par.put("listaPagamentosFornecedores", beanParamPagamentosFornecedores);
		par.put("listaFornecTitulos", beanParamFornecTitulos);
		par.put("listaFornecModularidades", beanParamFornecModularidades);
		
		par.put("dsServicoFormaAutorizacaoPagamento", beanParamFornecModularidades);
		
		par.put("listaFornecOutrasModularidades", beanParamFornecOutrasModularidades);
		par.put("listaPagamentosFornecedoresCompl", beanParamPagamentosFornecedoresCompl);
		par.put("listaPagamentosTributos", beanParamPagamentosTributos);
		par.put("listaTributos", beanParamTributos);
		par.put("listaPagamentosSalarios", beanParamPagamentosSalarios);
		par.put("listaSalariosModularidades", beanParamSalariosModularidades);
		par.put("listaPagamentosBeneficios", beanParamPagamentosBeneficios);
		par.put("listaPagamentosBeneficiosModularidades", beanParamPagamentosBeneficiosModularidades);
		par.put("listaServicos", beanParamServicos);
		par.put("listaServicosCompl", beanParamServicosCompl);
		par.put("listaGridTarifasModalidade", beanParamTarifas);
		par.put("titulo", "ANEXO I - PAGAMENTO INTEGRADO BRADESCO");
		par.put("imprimirGridTarifa", getImprimirGridTarifa());
		par.put("exibeCestaTarifas", getExibeCestaTarifas());
		
		par.put("dsServicoTipoServicoTributos", getListaPagamentosTri() == null ? "" : getListaPagamentosTri().get(0).getDsServicoTipoServico());
		par.put("dsFormaEnvioPagamentoTributos", getListaPagamentosTri() == null ? "" : getListaPagamentosTri().get(0).getDsFormaEnvioPagamento());
		par.put("cdServicoFloatingTributos", getListaPagamentosTri() == null ? "" : getListaPagamentosTri().get(0).getCdServicoFloating());
		par.put("dsServicoListaDebitoTributos", getListaPagamentosTri() == null ? "" : getListaPagamentosTri().get(0).getDsServicoListaDebito());
		par.put("dsServicoFormaAutorizacaoPagamentoTributos", getListaPagamentosTri() == null ? "" : getListaPagamentosTri().get(0).getDsServicoFormaAutorizacaoPagamento());
		par.put("dsServicoTipoDataFloatTributos", getListaPagamentosTri() == null ? "" : getListaPagamentosTri().get(0).getDsServicoTipoDataFloat());
		par.put("cdServicoTipoConsolidadoTributos", getListaPagamentosTri() == null ? "" : getListaPagamentosTri().get(0).getCdServicoTipoConsolidado());
		
		if(getListaPagamentosSalarios() != null ){
			par.put("dsServicoTipoServicoSalarios", getListaPagamentosSalarios().get(0).getDsServicoTipoServico());
			par.put("dsFormaEnvioPagamentoSalarios", getListaPagamentosSalarios().get(0).getDsFormaEnvioPagamento());
			par.put("cdServicoFloatingSalarios", getListaPagamentosSalarios().get(0).getCdServicoFloating());
			par.put("dsServicoListaDebitoSalarios", getListaPagamentosSalarios().get(0).getDsServicoListaDebito());
			par.put("dsServicoFormaAutorizacaoPagamentoSalarios", getListaPagamentosSalarios().get(0).getDsServicoFormaAutorizacaoPagamento());
			par.put("dsServicoTipoDataFloatSalarios", getListaPagamentosSalarios().get(0).getDsServicoTipoDataFloat());
			par.put("cdServicoTipoConsolidadoSalarios", getListaPagamentosSalarios().get(0).getCdServicoTipoConsolidado());
			
			par.put("dsCretipoServicoSalario",  getListaPagamentosSalarios().get(0).getDsCretipoServico());
			par.put("dsServicoTipoServicoSalario", getListaPagamentosSalarios().get(0).getDsCretipoServico());
			par.put("dsCretipoServicoSalario", getListaPagamentosSalarios().get(0).getDsCretipoServico());
			par.put("dsCreTipoConsultaSaldoSalario", getListaPagamentosSalarios().get(0).getDsCreTipoConsultaSaldo());
			par.put("dsCreTipoTratamentoFeriadoSalario", getListaPagamentosSalarios().get(0).getDsCreTipoTratamentoFeriado());
			par.put("dsCreTipoRejeicaoAgendadaSalario", getListaPagamentosSalarios().get(0).getDsCreTipoRejeicaoAgendada());
			par.put("dsCrePrioridadeDebitoSalario", getListaPagamentosSalarios().get(0).getDsCrePrioridadeDebito());
			par.put("vlCreLimiteIndividualSalario", getListaPagamentosSalarios().get(0).getVlCreLimiteIndividual());
			par.put("qtCreDiaRepiqueSalario", getListaPagamentosSalarios().get(0).getQtCreDiaRepique());
			par.put("cdCreContratoTransfSalario", getListaPagamentosSalarios().get(0).getCdCreContratoTransf());
			par.put("cdLctoPgmdSalario", getListaPagamentosSalarios().get(0).getCdLctoPgmd());
			par.put("dsCreTipoProcessamentoSalario", getListaPagamentosSalarios().get(0).getDsCreTipoProcessamento());
			par.put("cdCreCferiLocSalario", getListaPagamentosSalarios().get(0).getCdCreCferiLoc());
			par.put("dsCreTipoRejeicaoEfetivacaoSalario", getListaPagamentosSalarios().get(0).getDsCreTipoRejeicaoEfetivacao());
			if("1".equals(getListaPagamentosSalarios().get(0).getQtCreMaximaInconsistencia())){
				par.put("qtCreMaximaInconsistenciaSalario", "N�O");
			}else{
				par.put("qtCreMaximaInconsistenciaSalario", "SIM");
			}
			par.put("vlCreLimiteDiarioSalario", getListaPagamentosSalarios().get(0).getVlCreLimiteDiario());
			par.put("qtCreDiaFloatingSalario", getListaPagamentosSalarios().get(0).getQtCreDiaFloating());
			par.put("dsCreTipoConsistenciaFavorecidoSalario", getListaPagamentosSalarios().get(0).getDsCreTipoConsistenciaFavorecido());
			par.put("cdCreUtilizacaoCadastroFavorecido", getListaPagamentosSalarios().get(0).getCdCreUtilizacaoCadastroFavorecido());
			
			par.put("dsOpgTipoServicoSalario",  getListaPagamentosSalarios().get(0).getDsOpgTipoServico());
			par.put("dsOpgTipoServicoSalario", getListaPagamentosSalarios().get(0).getDsOpgTipoServico());
			par.put("dsOpgTipoConsultaSaldoSalario", getListaPagamentosSalarios().get(0).getDsOpgTipoConsultaSaldo());
			par.put("dsOpgTipoTratamentoFeriadoSalario", getListaPagamentosSalarios().get(0).getDsOpgTipoTratamentoFeriado());
			par.put("dsOpgTiporejeicaoAgendadoSalario", getListaPagamentosSalarios().get(0).getDsOpgTiporejeicaoAgendado());
			par.put("dsOpgPrioridadeDebitoSalario", getListaPagamentosSalarios().get(0).getDsOpgPrioridadeDebito());
			par.put("vlOpgLimiteIndividualSalario", getListaPagamentosSalarios().get(0).getVlOpgLimiteIndividual());
			par.put("qtOpgDiaRepiqueSalario", getListaPagamentosSalarios().get(0).getQtOpgDiaRepique());
			par.put("oPgEmissaoSalario", getListaPagamentosSalarios().get(0).getoPgEmissao());
			par.put("dsOpgTipoProcessamentoSalario", getListaPagamentosSalarios().get(0).getDsOpgTipoProcessamento());
			par.put("cdFeriLocSalario", getListaPagamentosSalarios().get(0).getCdFeriLoc());
			par.put("dsOpgTipoRejeicaoEfetivacaoSalario", getListaPagamentosSalarios().get(0).getDsOpgTipoRejeicaoEfetivacao());
			par.put("cdOpgUtilizacaoCadastroFavorecidoSalario", getListaPagamentosSalarios().get(0).getCdOpgUtilizacaoCadastroFavorecido());
			par.put("vlOpgLimiteDiarioSalario", getListaPagamentosSalarios().get(0).getVlOpgLimiteDiario());
			par.put("dsOpgOcorrenciasDebitoSalario", getListaPagamentosSalarios().get(0).getDsOpgOcorrenciasDebito());
			par.put("qtOpgDiaExpiracaoSalario", getListaPagamentosSalarios().get(0).getQtOpgDiaExpiracao());
			par.put("cdOpgIndicadorQuantidadeMaximoSalario", getListaPagamentosSalarios().get(0).getCdOpgIndicadorQuantidadeMaximo());
			par.put("qtOpgDiaFloatingSalario", getListaPagamentosSalarios().get(0).getQtOpgDiaFloating());
		}else{
			par.put("dsOpgTipoServicoSalario", "N");
			par.put("dsCretipoServicoSalario", "N");
			par.put("exibePgtoSalarios", "N");
		}
		
		try {
			
			PgitUtil.geraRelatorioPdf("/relatorios/manutencaoContrato/manterContrato/resumoContrato/",
					"imprimirResumoContrato", listaAnexoContrato, par);
			
		} catch (Exception e) {
			throw new BradescoViewException(e.getMessage(), e, "", "",
					BradescoViewExceptionActionType.ACTION);
		}			
	}
	
	/**
	 * Imprimir anexo contrato um.
	 *
	 * @throws IOException the IO exception
	 */
	public void imprimirAnexoContratoUm() throws IOException {
	    imprimirContratoPrimeiroAnexo();
        imprimirContratoSegundoAnexo();     

        AnexoContratoDTO anexoContratoDTO = new AnexoContratoDTO();
        try {
            BeanUtils.copyProperties(anexoContratoDTO, saidaPrimeiroContrato);
            BeanUtils.copyProperties(anexoContratoDTO, saidaSegundoContrato);

        } catch (IllegalAccessException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (InvocationTargetException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        List<AnexoContratoDTO> listaAnexoContrato = new ArrayList<AnexoContratoDTO>();
        listaAnexoContrato.add(anexoContratoDTO);

        String caminho = "/images/Bradesco_logo.JPG";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext
                .getExternalContext().getContext();
        String logoPath = servletContext.getRealPath(caminho);

        SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");

        // Dados de Participantes
        String pathArquivos = "/relatorios/manutencaoContrato/manterContrato/anexoContrato";
        String arquivoParticipantes = pathArquivos + "/relParticipantes.jasper";
        String pathSubRelParticipantes = servletContext
                .getRealPath(arquivoParticipantes);
        List<OcorrenciasParticipantes> listaParticipantes = new ArrayList<OcorrenciasParticipantes>();
        int qtdeParticipantes = 0;
        JRBeanCollectionDataSource beanParamParticipantes = null;
        if (anexoContratoDTO.getQtdeParticipante() != 0) {
            for (OcorrenciasParticipantes ocorrenciasParticipantes : anexoContratoDTO
                    .getListaParticipantes()) {
                listaParticipantes.add(ocorrenciasParticipantes);
                qtdeParticipantes++;
                if (qtdeParticipantes == anexoContratoDTO.getQtdeParticipante()) {
                    break;
                }
            }
            beanParamParticipantes = new JRBeanCollectionDataSource(
                    listaParticipantes);
        }

        // Dados de contas
        String arquivoContas = pathArquivos + "/relContas.jasper";
        String pathSubRelContas = servletContext.getRealPath(arquivoContas);
        List<OcorrenciasContas> listaContas = new ArrayList<OcorrenciasContas>();
        int qtdeContas = 0;
        JRBeanCollectionDataSource beanParamContas = null;
        if (anexoContratoDTO.getQtdeConta() != 0) {
            for (OcorrenciasContas ocorrenciasContas : anexoContratoDTO
                    .getListaContas()) {
                ocorrenciasContas.setDigitoContaConta(ocorrenciasContas
                        .getDigitoContaConta().substring(7, 15));
                listaContas.add(ocorrenciasContas);
                qtdeContas++;
                if (qtdeContas == anexoContratoDTO.getQtdeConta()) {
                    break;
                }
            }
            beanParamContas = new JRBeanCollectionDataSource(listaContas);
        }

        // Configura��o de Layout
        String arquivoLayouts = pathArquivos + "/relLayouts.jasper";
        String pathSubRelLayouts = servletContext.getRealPath(arquivoLayouts);
        List<OcorrenciasLayout> listaLayouts = new ArrayList<OcorrenciasLayout>();
        int qtdeLayouts = 0;
        JRBeanCollectionDataSource beanParamLayout = null;
        if (anexoContratoDTO.getQtLayout() != 0) {
            for (OcorrenciasLayout ocorrenciasLayout : anexoContratoDTO
                    .getListaLayouts()) {
                listaLayouts.add(ocorrenciasLayout);
                qtdeLayouts++;
                if (qtdeLayouts == anexoContratoDTO.getQtLayout()) {
                    break;
                }
            }
            beanParamLayout = new JRBeanCollectionDataSource(listaLayouts);
        }
        
        //Configura��o Cestas Tarifas
        String arquivoCestaTarifas = pathArquivos + "relCestasDeTarifas.jasper";
        String pathSubRelCestasDeTarifas = servletContext.getRealPath(arquivoCestaTarifas);
        List<OcorrenciasSaidaDTO> listaCestaTarifas = new ArrayList<OcorrenciasSaidaDTO>();
        String exibeCestaTarifas = "N";
        int qtdeCestaTarifas = 0;
        
        JRBeanCollectionDataSource beanParamCestasTarifas = null;
        if(anexoContratoDTO.getQtCestasTarifas() != 0){
        	exibeCestaTarifas = "S";
        	for(OcorrenciasSaidaDTO ocorrenciaCestaTarifa : anexoContratoDTO.getListaCestasTarifas()){
        		listaCestaTarifas.add(ocorrenciaCestaTarifa);
        		qtdeCestaTarifas++;
        		if (qtdeCestaTarifas == anexoContratoDTO.getQtCestasTarifas()) {
                    break;
                }
        	}
        	beanParamCestasTarifas = new JRBeanCollectionDataSource(listaCestaTarifas);
        }
        
        
        String arquivoServModalidades = pathArquivos
                + "/relServicosModalidades.jasper";
        String pathSubServModalidades = servletContext
                .getRealPath(arquivoServModalidades);

        // Pagamentos
        String arquivoPagamentosFornecedores = pathArquivos
                + "/relPagamentosFornecedores.jasper";
        String pathSubRelPagamentosFornecedores = servletContext
                .getRealPath(arquivoPagamentosFornecedores);

        String arquivoPagamentosFornecedoresTitulos = pathArquivos
                + "/relPagamentosFornecedoresTitulos.jasper";
        String pathSubRelPagamentosFornecedoresTitulos = servletContext
                .getRealPath(arquivoPagamentosFornecedoresTitulos);

        String arquivoPagamentosFornecedoresModularidades = pathArquivos
                + "/relPagamentosFornecedoresModularidades.jasper";
        String pathSubRelPagamentosFornecedoresModularidades = servletContext
                .getRealPath(arquivoPagamentosFornecedoresModularidades);

        String arquivoPagamentosFornecedoresOutrasModularidades = pathArquivos
                + "/relPagamentosFornecedoresOutrasModularidades.jasper";
        String pathSubRelPagamentosFornecedoresOutrasModularidades = servletContext
                .getRealPath(arquivoPagamentosFornecedoresOutrasModularidades);

        String arquivoPagamentosFornecedoresCompl = pathArquivos
                + "/relPagamentosFornecedoresCompl.jasper";
        String pathSubRelPagamentosFornecedoresCompl = servletContext
                .getRealPath(arquivoPagamentosFornecedoresCompl);

        String arquivoPagamentosTributos = pathArquivos
                + "/relPagamentosTributos.jasper";
        String pathSubRelPagamentosTributos = servletContext
                .getRealPath(arquivoPagamentosTributos);

        String arquivoTributos = pathArquivos
                + "/relPagamentosTributosModalidades.jasper";
        String pathSubRelTributos = servletContext.getRealPath(arquivoTributos);

        String arquivoPagamentosSalarios = pathArquivos
                + "/relPagamentosSalarios.jasper";
        String pathSubRelPagamentosSalarios = servletContext
                .getRealPath(arquivoPagamentosSalarios);

        String arquivoPagamentosSalariosModularidades = pathArquivos
                + "/relPagamentosSalariosModularidades.jasper";
        String pathSubRelPagamentosSalariosModularidades = servletContext
                .getRealPath(arquivoPagamentosSalariosModularidades);

        String arquivoPagamentosBeneficios = pathArquivos
                + "/relPagamentosBeneficios.jasper";
        String pathSubRelPagamentosBeneficios = servletContext
                .getRealPath(arquivoPagamentosBeneficios);

        String arquivoPagamentosBeneficiosModularidades = pathArquivos
                + "/relPagamentosBeneficiosModularidades.jasper";
        String pathSubRelPagamentosBeneficiosModularidades = servletContext
                .getRealPath(arquivoPagamentosBeneficiosModularidades);

        List<PagamentosDTO> listaFornecTitulos = new ArrayList<PagamentosDTO>();
        List<PagamentosDTO> listaFornecModularidades = new ArrayList<PagamentosDTO>();
        List<PagamentosDTO> listaFornecOutrasModularidades = new ArrayList<PagamentosDTO>();
        
        PagamentosDTO pagamentosFornecedoresDTO = new PagamentosDTO();      
        pagamentosFornecedoresDTO.setDsFtiTipoTitulo("N");
        pagamentosFornecedoresDTO.setDsModTipoServico("N");
        pagamentosFornecedoresDTO.setDsModTipoOutrosServico("N");
        String exibePgtoFornecedor = "N";

        List<ServicosModalidadesDTO> listaServModalidades = new ArrayList<ServicosModalidadesDTO>();
        ServicosModalidadesDTO servicosModalidadesDTO = null;

        boolean achouCreditoConta = false;
        boolean achouBradesco = false;
        boolean achouOutros = false;
        boolean achouDOC = false;
        boolean achouTED = false;
        boolean achouDC = false;
        boolean achouOC = false;
        boolean achouDI = false;
        boolean achouOrdemPagamento = false;

        // Pagamento de fornecedores
        int exibeLinhaFornecedor = saidaSegundoContrato
                .getQtModalidadeFornecedor() / 2;
        pgtoFornecedor: for (int i = 0; i < saidaSegundoContrato
                .getQtModalidadeFornecedor(); i++) {
            servicosModalidadesDTO = new ServicosModalidadesDTO();
            servicosModalidadesDTO.setDsServico(saidaSegundoContrato
                    .getCdServicoFornecedor().toUpperCase());
            servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato
                    .getListModFornecedores().get(i)
                    .getDsModalidadeFornecedor());

            if (saidaSegundoContrato.getQtModalidadeFornecedor() % 2 == 0) {
                if (i + 1 == exibeLinhaFornecedor) {
                    servicosModalidadesDTO.setImprimeLinhaServico("S");
                }
            } else {
                if (i == exibeLinhaFornecedor) {
                    servicosModalidadesDTO.setImprimeLinhaServico("S");
                }
            }

            listaServModalidades.add(servicosModalidadesDTO);
            

            // Bloco de Servi�o de Pagamento de Fornecedores

            PagamentosDTO fornecTitulos = null;
            PagamentosDTO fornecModularidades = null;

            for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato
                    .getListaServicoPagamento()) {
                if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("F")) {
                    exibePgtoFornecedor = "S";

                    try {
                        BeanUtils.copyProperties(pagamentosFornecedoresDTO,
                                ocorrencias);
                        pagamentosFornecedoresDTO
                                .setDsServicoTipoServico(saidaSegundoContrato
                                        .getCdServicoFornecedor());
                    } catch (IllegalAccessException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    // Busca pela Modalidade Cr�dito em conta
                    if (!achouCreditoConta) {
                        for (OcorrenciasCreditos ocorrenciasCreditos : saidaSegundoContrato
                                .getListaCreditos()) {
                            if (ocorrenciasCreditos.getDsCretipoServico()
                                    .equalsIgnoreCase("F")) {
                                achouCreditoConta = true;
                                try {
                                    BeanUtils.copyProperties(
                                            pagamentosFornecedoresDTO,
                                            ocorrenciasCreditos);
                                } catch (IllegalAccessException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                pagamentosFornecedoresDTO
                                        .setDsCretipoServico(saidaSegundoContrato
                                                .getListModFornecedores()
                                                .get(i)
                                                .getDsModalidadeFornecedor());
                                pagamentosFornecedoresDTO
                                        .setPcCreMaximoInconsistente(ocorrenciasCreditos
                                                .getPcCreMaximoInconsistente()
                                                .toString());
                                pagamentosFornecedoresDTO
                                        .setQtCreMaximaInconsistencia(PgitUtil
                                                .formatarNumero(
                                                        ocorrenciasCreditos
                                                                .getQtCreMaximaInconsistencia(),
                                                        true));
                                pagamentosFornecedoresDTO
                                        .setVlCreMaximoFavorecidoNao(NumberUtils
                                                .format(ocorrenciasCreditos
                                                        .getVlCreMaximoFavorecidoNao()));
                                pagamentosFornecedoresDTO
                                        .setVlCreLimiteDiario(NumberUtils
                                                .format(ocorrenciasCreditos
                                                        .getVlCreLimiteDiario()));
                                pagamentosFornecedoresDTO
                                        .setVlCreLimiteIndividual(NumberUtils
                                                .format(ocorrenciasCreditos
                                                        .getVlCreLimiteIndividual()));
                                pagamentosFornecedoresDTO
                                        .setQtCreDiaRepique(ocorrenciasCreditos
                                                .getQtCreDiaRepique()
                                                .toString());
                                pagamentosFornecedoresDTO
                                        .setQtCreDiaFloating(ocorrenciasCreditos
                                                .getQtCreDiaFloating()
                                                .toString());

                                continue pgtoFornecedor;
                            }
                        }
                        achouCreditoConta = true;
                        pagamentosFornecedoresDTO.setDsCretipoServico("N");
                        
                    }
                    

                    // Busca pela Modalidade Pagemanto de titulos (Bradesco /
                    // Outros)
                    if (!achouBradesco || !achouOutros) {
                        for (OcorrenciasTitulos ocorrenciasTitulos : saidaSegundoContrato
                                .getListaTitulos()) {
                            fornecTitulos = new PagamentosDTO();
                            try {
                                BeanUtils.copyProperties(fornecTitulos,
                                        ocorrenciasTitulos);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }

                            if (!achouBradesco) {
                                if (ocorrenciasTitulos.getDsFtiTipoTitulo()
                                        .equalsIgnoreCase("B")) {
                                    pagamentosFornecedoresDTO
                                            .setDsFtiTipoTitulo("S");
                                    fornecTitulos
                                            .setDsFtiTipoTitulo(saidaSegundoContrato
                                                    .getListModFornecedores()
                                                    .get(i)
                                                    .getDsModalidadeFornecedor());
                                    fornecTitulos
                                            .setPcFtiMaximoInconsistente(ocorrenciasTitulos
                                                    .getPcFtiMaximoInconsistente()
                                                    .toString());
                                    fornecTitulos
                                            .setQtFtiMaximaInconsistencia(PgitUtil
                                                    .formatarNumero(
                                                            ocorrenciasTitulos
                                                                    .getQtFtiMaximaInconsistencia(),
                                                            true));
                                    fornecTitulos
                                            .setVlFtiMaximoFavorecidoNao(NumberUtils
                                                    .format(ocorrenciasTitulos
                                                            .getVlFtiMaximoFavorecidoNao()));
                                    fornecTitulos
                                            .setVlFtiLimiteDiario(NumberUtils
                                                    .format(ocorrenciasTitulos
                                                            .getVlFtiLimiteDiario()));
                                    fornecTitulos
                                            .setVlFtiLimiteIndividual(NumberUtils
                                                    .format(ocorrenciasTitulos
                                                            .getVlFtiLimiteIndividual()));
                                    fornecTitulos
                                            .setQtFtiDiaFloating(ocorrenciasTitulos
                                                    .getQtFtiDiaFloating()
                                                    .toString());
                                    fornecTitulos
                                            .setQtFtiDiaRepique(ocorrenciasTitulos
                                                    .getQtFtiDiaRepique()
                                                    .toString());
                                    fornecTitulos
                                            .setDsTIpoConsultaSaldoSuperior(ocorrenciasTitulos
                                                    .getDsTIpoConsultaSaldoSuperior());
                                    listaFornecTitulos.add(fornecTitulos);

                                    achouBradesco = true;
                                    continue pgtoFornecedor;
                                }
                            }

                            if (!achouOutros) {
                                if (ocorrenciasTitulos.getDsFtiTipoTitulo()
                                        .equalsIgnoreCase("O")) {
                                    pagamentosFornecedoresDTO
                                            .setDsFtiTipoTitulo("S");
                                    fornecTitulos
                                            .setDsFtiTipoTitulo(saidaSegundoContrato
                                                    .getListModFornecedores()
                                                    .get(i)
                                                    .getDsModalidadeFornecedor());
                                    fornecTitulos
                                            .setPcFtiMaximoInconsistente(ocorrenciasTitulos
                                                    .getPcFtiMaximoInconsistente()
                                                    .toString());
                                    fornecTitulos
                                            .setQtFtiMaximaInconsistencia(PgitUtil
                                                    .formatarNumero(
                                                            ocorrenciasTitulos
                                                                    .getQtFtiMaximaInconsistencia(),
                                                            true));
                                    fornecTitulos
                                            .setVlFtiMaximoFavorecidoNao(NumberUtils
                                                    .format(ocorrenciasTitulos
                                                            .getVlFtiMaximoFavorecidoNao()));
                                    fornecTitulos
                                            .setVlFtiLimiteDiario(NumberUtils
                                                    .format(ocorrenciasTitulos
                                                            .getVlFtiLimiteDiario()));
                                    fornecTitulos
                                            .setVlFtiLimiteIndividual(NumberUtils
                                                    .format(ocorrenciasTitulos
                                                            .getVlFtiLimiteIndividual()));
                                    fornecTitulos
                                            .setQtFtiDiaFloating(ocorrenciasTitulos
                                                    .getQtFtiDiaFloating()
                                                    .toString());
                                    fornecTitulos
                                            .setQtFtiDiaRepique(ocorrenciasTitulos
                                                    .getQtFtiDiaRepique()
                                                    .toString());
                                    fornecTitulos
                                            .setDsTIpoConsultaSaldoSuperior(ocorrenciasTitulos
                                                    .getDsTIpoConsultaSaldoSuperior());
                                    listaFornecTitulos.add(fornecTitulos);

                                    achouOutros = true;
                                    continue pgtoFornecedor;
                                }
                            }
                        }

                        achouBradesco = true;
                        achouOutros = true;
                    }

                    // Busca pela Modalidade Pagemanto de modularidades (DOC /
                    // TED / Debito em Conta)
                    if (!achouDOC || !achouTED || !achouDC) {
                        for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
                                .getListaModularidades()) {
                            if (ocorrenciasModularidades.getDsModTipoServico()
                                    .equalsIgnoreCase("F")) {
                                fornecModularidades = new PagamentosDTO();

                                try {
                                    BeanUtils.copyProperties(
                                            fornecModularidades,
                                            ocorrenciasModularidades);
                                } catch (IllegalAccessException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                if (!achouDOC) {
                                    if (ocorrenciasModularidades
                                            .getDsModTipoModalidade()
                                            .equalsIgnoreCase("D")) {
                                        pagamentosFornecedoresDTO
                                                .setDsModTipoServico("S");
                                        fornecModularidades
                                                .setDsModTipoServico(saidaSegundoContrato
                                                        .getListModFornecedores()
                                                        .get(i)
                                                        .getDsModalidadeFornecedor());

                                        fornecModularidades
                                                .setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
                                                        .getCdModIndicadorPercentualMaximoInconsistente()
                                                        .toString());
                                        fornecModularidades
                                                .setQtModMaximoInconsistencia(PgitUtil
                                                        .formatarNumero(
                                                                ocorrenciasModularidades
                                                                        .getQtModMaximoInconsistencia(),
                                                                true));
                                        fornecModularidades
                                                .setVlModMaximoFavorecidoNao(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModMaximoFavorecidoNao()));
                                        fornecModularidades
                                                .setVlModLimiteDiario(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteDiario()));
                                        fornecModularidades
                                                .setVlModLimiteIndividual(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteIndividual()));
                                        fornecModularidades
                                                .setQtModDiaFloating(ocorrenciasModularidades
                                                        .getQtModDiaFloating()
                                                        .toString());
                                        fornecModularidades
                                                .setQtModDiaRepique(ocorrenciasModularidades
                                                        .getQtModDiaRepique()
                                                        .toString());

                                        listaFornecModularidades
                                                .add(fornecModularidades);
                                        achouDOC = true;
                                        continue pgtoFornecedor;
                                    }
                                }
                                if (!achouTED) {
                                    if (ocorrenciasModularidades
                                            .getDsModTipoModalidade()
                                            .equalsIgnoreCase("T")) {
                                        pagamentosFornecedoresDTO
                                                .setDsModTipoServico("S");
                                        fornecModularidades
                                                .setDsModTipoServico(saidaSegundoContrato
                                                        .getListModFornecedores()
                                                        .get(i)
                                                        .getDsModalidadeFornecedor());

                                        fornecModularidades
                                                .setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
                                                        .getCdModIndicadorPercentualMaximoInconsistente()
                                                        .toString());
                                        fornecModularidades
                                                .setQtModMaximoInconsistencia(PgitUtil
                                                        .formatarNumero(
                                                                ocorrenciasModularidades
                                                                        .getQtModMaximoInconsistencia(),
                                                                true));
                                        fornecModularidades
                                                .setVlModMaximoFavorecidoNao(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModMaximoFavorecidoNao()));
                                        fornecModularidades
                                                .setVlModLimiteDiario(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteDiario()));
                                        fornecModularidades
                                                .setVlModLimiteIndividual(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteIndividual()));
                                        fornecModularidades
                                                .setQtModDiaFloating(ocorrenciasModularidades
                                                        .getQtModDiaFloating()
                                                        .toString());
                                        fornecModularidades
                                                .setQtModDiaRepique(ocorrenciasModularidades
                                                        .getQtModDiaRepique()
                                                        .toString());

                                        listaFornecModularidades
                                                .add(fornecModularidades);
                                        achouTED = true;
                                        continue pgtoFornecedor;
                                    }
                                }
                                if (!achouDC) {
                                    if (ocorrenciasModularidades
                                            .getDsModTipoModalidade()
                                            .equalsIgnoreCase("C")) {
                                        pagamentosFornecedoresDTO
                                                .setDsModTipoServico("S");
                                        fornecModularidades
                                                .setDsModTipoServico(saidaSegundoContrato
                                                        .getListModFornecedores()
                                                        .get(i)
                                                        .getDsModalidadeFornecedor());

                                        fornecModularidades
                                                .setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
                                                        .getCdModIndicadorPercentualMaximoInconsistente()
                                                        .toString());
                                        fornecModularidades
                                                .setQtModMaximoInconsistencia(PgitUtil
                                                        .formatarNumero(
                                                                ocorrenciasModularidades
                                                                        .getQtModMaximoInconsistencia(),
                                                                true));
                                        fornecModularidades
                                                .setVlModMaximoFavorecidoNao(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModMaximoFavorecidoNao()));
                                        fornecModularidades
                                                .setVlModLimiteDiario(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteDiario()));
                                        fornecModularidades
                                                .setVlModLimiteIndividual(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteIndividual()));
                                        fornecModularidades
                                                .setQtModDiaFloating(ocorrenciasModularidades
                                                        .getQtModDiaFloating()
                                                        .toString());
                                        fornecModularidades
                                                .setQtModDiaRepique(ocorrenciasModularidades
                                                        .getQtModDiaRepique()
                                                        .toString());

                                        listaFornecModularidades
                                                .add(fornecModularidades);
                                        achouDC = true;
                                        continue pgtoFornecedor;
                                    }
                                }
                            }
                        }
                        achouDOC = true;
                        achouTED = true;
                        achouDC = true;
                    }

                    // Busca pela Modalidade Ordem de Pagamento
                    if (!achouOrdemPagamento) {
                        for (OcorrenciasOperacoes ocorrenciasOperacoes : saidaSegundoContrato
                                .getListaOperacoes()) {
                            if (ocorrenciasOperacoes.getDsOpgTipoServico()
                                    .equalsIgnoreCase("F")) {
                                achouOrdemPagamento = true;
                                try {
                                    BeanUtils.copyProperties(
                                            pagamentosFornecedoresDTO,
                                            ocorrenciasOperacoes);
                                } catch (IllegalAccessException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                pagamentosFornecedoresDTO
                                        .setDsOpgTipoServico(saidaSegundoContrato
                                                .getListModFornecedores()
                                                .get(i)
                                                .getDsModalidadeFornecedor());
                                pagamentosFornecedoresDTO
                                        .setPcOpgMaximoInconsistencia(ocorrenciasOperacoes
                                                .getPcOpgMaximoInconsistencia()
                                                .toString());
                                pagamentosFornecedoresDTO
                                        .setQtOpgMaximoInconsistencia(PgitUtil
                                                .formatarNumero(
                                                        ocorrenciasOperacoes
                                                                .getQtOpgMaximoInconsistencia(),
                                                        true));
                                pagamentosFornecedoresDTO
                                        .setVlOpgLimiteDiario(NumberUtils
                                                .format(ocorrenciasOperacoes
                                                        .getVlOpgLimiteDiario()));
                                pagamentosFornecedoresDTO
                                        .setVlOpgLimiteIndividual(NumberUtils
                                                .format(ocorrenciasOperacoes
                                                        .getVlOpgLimiteIndividual()));
                                pagamentosFornecedoresDTO
                                        .setQtOpgDiaFloating(ocorrenciasOperacoes
                                                .getQtOpgDiaFloating()
                                                .toString());
                                pagamentosFornecedoresDTO
                                        .setVlOpgMaximoFavorecidoNao(NumberUtils
                                                .format(ocorrenciasOperacoes
                                                        .getVlOpgMaximoFavorecidoNao()));
                                pagamentosFornecedoresDTO
                                        .setQtOpgDiaRepique(ocorrenciasOperacoes
                                                .getQtOpgDiaRepique()
                                                .toString());
                                pagamentosFornecedoresDTO
                                        .setQtOpgDiaExpiracao(ocorrenciasOperacoes
                                                .getQtOpgDiaExpiracao()
                                                .toString());

                                continue pgtoFornecedor;
                            }
                        }
                        achouOrdemPagamento = true;
                        pagamentosFornecedoresDTO.setDsOpgTipoServico("N");
                    }

                    // Busca pela Modalidade Pagemanto de modularidades (Ordem
                    // de Credito / Deposito Identificado)
                    if (!achouOC || !achouDI) {
                        for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
                                .getListaModularidades()) {
                            if (ocorrenciasModularidades.getDsModTipoServico()
                                    .equalsIgnoreCase("F")) {
                                fornecModularidades = new PagamentosDTO();

                                try {
                                    BeanUtils.copyProperties(
                                            fornecModularidades,
                                            ocorrenciasModularidades);
                                } catch (IllegalAccessException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                if (!achouOC) {
                                    if (ocorrenciasModularidades
                                            .getDsModTipoModalidade()
                                            .equalsIgnoreCase("O")) {
                                        pagamentosFornecedoresDTO
                                                .setDsModTipoOutrosServico("S");
                                        fornecModularidades
                                                .setDsModTipoServico(saidaSegundoContrato
                                                        .getListModFornecedores()
                                                        .get(i)
                                                        .getDsModalidadeFornecedor());

                                        fornecModularidades
                                                .setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
                                                        .getCdModIndicadorPercentualMaximoInconsistente()
                                                        .toString());
                                        fornecModularidades
                                                .setQtModMaximoInconsistencia(PgitUtil
                                                        .formatarNumero(
                                                                ocorrenciasModularidades
                                                                        .getQtModMaximoInconsistencia(),
                                                                true));
                                        fornecModularidades
                                                .setVlModMaximoFavorecidoNao(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModMaximoFavorecidoNao()));
                                        fornecModularidades
                                                .setVlModLimiteDiario(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteDiario()));
                                        fornecModularidades
                                                .setVlModLimiteIndividual(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteIndividual()));
                                        fornecModularidades
                                                .setQtModDiaFloating(ocorrenciasModularidades
                                                        .getQtModDiaFloating()
                                                        .toString());
                                        fornecModularidades
                                                .setQtModDiaRepique(ocorrenciasModularidades
                                                        .getQtModDiaRepique()
                                                        .toString());

                                        listaFornecOutrasModularidades
                                                .add(fornecModularidades);
                                        achouOC = true;
                                        continue pgtoFornecedor;
                                    }
                                }

                                if (!achouDI) {
                                    if (ocorrenciasModularidades
                                            .getDsModTipoModalidade()
                                            .equalsIgnoreCase("I")) {
                                        pagamentosFornecedoresDTO
                                                .setDsModTipoOutrosServico("S");
                                        fornecModularidades
                                                .setDsModTipoServico(saidaSegundoContrato
                                                        .getListModFornecedores()
                                                        .get(i)
                                                        .getDsModalidadeFornecedor());

                                        fornecModularidades
                                                .setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
                                                        .getCdModIndicadorPercentualMaximoInconsistente()
                                                        .toString());
                                        fornecModularidades
                                                .setQtModMaximoInconsistencia(PgitUtil
                                                        .formatarNumero(
                                                                ocorrenciasModularidades
                                                                        .getQtModMaximoInconsistencia(),
                                                                true));
                                        fornecModularidades
                                                .setVlModMaximoFavorecidoNao(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModMaximoFavorecidoNao()));
                                        fornecModularidades
                                                .setVlModLimiteDiario(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteDiario()));
                                        fornecModularidades
                                                .setVlModLimiteIndividual(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteIndividual()));
                                        fornecModularidades
                                                .setQtModDiaFloating(ocorrenciasModularidades
                                                        .getQtModDiaFloating()
                                                        .toString());
                                        fornecModularidades
                                                .setQtModDiaRepique(ocorrenciasModularidades
                                                        .getQtModDiaRepique()
                                                        .toString());

                                        listaFornecOutrasModularidades
                                                .add(fornecModularidades);
                                        achouDI = true;
                                        continue pgtoFornecedor;
                                    }
                                }
                            }
                        }
                        achouOC = true;
                        achouDI = true;
                    }

                    // Bloco Rastreamentos
                    if (saidaSegundoContrato.getDsFraTipoRastreabilidade()
                            .trim().equals("")) {
                        pagamentosFornecedoresDTO
                                .setDsFraTipoRastreabilidade("N");
                    } else {
                        pagamentosFornecedoresDTO
                                .setDsFraTipoRastreabilidade(saidaSegundoContrato
                                        .getDsFraTipoRastreabilidade());
                        pagamentosFornecedoresDTO
                                .setDsFraRastreabilidadeTerceiro(saidaSegundoContrato
                                        .getDsFraRastreabilidadeTerceiro());
                        pagamentosFornecedoresDTO
                                .setDsFraRastreabilidadeNota(saidaSegundoContrato
                                        .getDsFraRastreabilidadeNota());
                        pagamentosFornecedoresDTO
                                .setDsFraCaptacaoTitulo(saidaSegundoContrato
                                        .getDsFraCaptacaoTitulo());
                        pagamentosFornecedoresDTO
                                .setDsFraAgendaCliente(saidaSegundoContrato
                                        .getDsFraAgendaCliente());
                        pagamentosFornecedoresDTO
                                .setDsFraAgendaFilial(saidaSegundoContrato
                                        .getDsFraAgendaFilial());
                        pagamentosFornecedoresDTO
                                .setDsFraBloqueioEmissao(saidaSegundoContrato
                                        .getDsFraBloqueioEmissao());
                        pagamentosFornecedoresDTO
                                .setDtFraInicioBloqueio(saidaSegundoContrato
                                        .getDtFraInicioBloqueio());
                        pagamentosFornecedoresDTO
                                .setDtFraInicioRastreabilidade(saidaSegundoContrato
                                        .getDtFraInicioRastreabilidade());
                        pagamentosFornecedoresDTO
                                .setDsFraAdesaoSacado(saidaSegundoContrato
                                        .getDsFraAdesaoSacado());
                    }
                }
            }
        }

        // Pagamento de tributos
        int exibeLinhaTributos = saidaSegundoContrato.getQtModalidadeTributos() / 2;
        String exibePgtoTributos = "N";
        PagamentosDTO pagamentosTributosDTO = new PagamentosDTO();
        PagamentosDTO tributosDTO = null;
        pagamentosTributosDTO
                .setDsTibutoAgendadoDebitoVeicular(saidaSegundoContrato
                        .getDsTibutoAgendadoDebitoVeicular());
        pagamentosTributosDTO.setDsTdvTipoConsultaProposta(saidaSegundoContrato
                .getDsTdvTipoConsultaProposta());
        pagamentosTributosDTO.setDsTdvTipoTratamentoValor(saidaSegundoContrato
                .getDsTdvTipoTratamentoValor());

        listaPagamentosTri = new ArrayList<PagamentosDTO>();
        listaTri = new ArrayList<PagamentosDTO>();

        boolean achouDV = false;
        boolean achouGR = false;
        boolean achouDR = false;
        boolean achouGP = false;
        boolean achouCB = false;

        pgtoTributos: for (int i = 0; i < saidaSegundoContrato
                .getQtModalidadeTributos(); i++) {
            servicosModalidadesDTO = new ServicosModalidadesDTO();
            servicosModalidadesDTO.setDsServico(saidaSegundoContrato
                    .getCdServicoTributos());
            servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato
                    .getListaModTributos().get(i).getCdModalidadeTributos());

            if (saidaSegundoContrato.getQtModalidadeTributos() % 2 == 0) {
                if (i + 1 == exibeLinhaTributos) {
                    servicosModalidadesDTO.setImprimeLinhaServico("S");
                }
            } else {
                if (i == exibeLinhaTributos) {
                    servicosModalidadesDTO.setImprimeLinhaServico("S");
                }
            }

            listaServModalidades.add(servicosModalidadesDTO);

            // Bloco de Servi�o de Pagamento de Tributos
            for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato
                    .getListaServicoPagamento()) {
                if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("T")) {
                    exibePgtoTributos = "S";
                    try {
                        BeanUtils.copyProperties(pagamentosTributosDTO,
                                ocorrencias);
                        pagamentosTributosDTO
                                .setDsServicoTipoServico(saidaSegundoContrato
                                        .getCdServicoTributos());
                    } catch (IllegalAccessException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    // Busca pela Modalidades (Debito de Veiculos / GARE / DARF
                    // / GPS / C�digo de Barras)
                    if (!achouDV || !achouGR || !achouDR || !achouGP
                            || !achouCB) {
                        for (OcorrenciasTriTributo ocorrenciasTriTributo : saidaSegundoContrato
                                .getListaTriTributos()) {
                            tributosDTO = new PagamentosDTO();
                            try {
                                BeanUtils.copyProperties(tributosDTO,
                                        ocorrenciasTriTributo);
                            } catch (IllegalAccessException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                            if (!achouDV) {
                                if (ocorrenciasTriTributo.getDsTriTipoTributo()
                                        .equalsIgnoreCase("V")) {
                                    tributosDTO.setExibeDebitoVeiculo("S");

                                    tributosDTO
                                            .setDsTriTipoTributo(saidaSegundoContrato
                                                    .getListaModTributos().get(
                                                            i)
                                                    .getCdModalidadeTributos());

                                    tributosDTO
                                            .setDsTdvTipoTratamentoValor(saidaSegundoContrato
                                                    .getDsTdvTipoTratamentoValor());
                                    tributosDTO
                                            .setDsTdvTipoConsultaProposta(saidaSegundoContrato
                                                    .getDsTdvTipoConsultaProposta());
                                    tributosDTO
                                            .setVlTriMaximoFavorecidoNao(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriMaximoFavorecidoNao()));
                                    tributosDTO
                                            .setVlTriLimiteDiario(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriLimiteDiario()));
                                    tributosDTO
                                            .setVlTriLimiteIndividual(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriLimiteIndividual()));
                                    tributosDTO
                                            .setQttriDiaFloating(ocorrenciasTriTributo
                                                    .getQttriDiaFloating()
                                                    .toString());
                                    tributosDTO
                                            .setQtTriDiaRepique(ocorrenciasTriTributo
                                                    .getQtTriDiaRepique()
                                                    .toString());

                                    listaTri.add(tributosDTO);

                                    achouDV = true;
                                    continue pgtoTributos;
                                }
                            }
                            if (!achouGR) {
                                if (ocorrenciasTriTributo.getDsTriTipoTributo()
                                        .equalsIgnoreCase("G")) {
                                    tributosDTO.setExibeDebitoVeiculo("N");
                                    tributosDTO
                                            .setDsTriTipoTributo(saidaSegundoContrato
                                                    .getListaModTributos().get(
                                                            i)
                                                    .getCdModalidadeTributos());

                                    tributosDTO
                                            .setVlTriMaximoFavorecidoNao(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriMaximoFavorecidoNao()));
                                    tributosDTO
                                            .setVlTriLimiteDiario(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriLimiteDiario()));
                                    tributosDTO
                                            .setVlTriLimiteIndividual(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriLimiteIndividual()));
                                    tributosDTO
                                            .setQttriDiaFloating(ocorrenciasTriTributo
                                                    .getQttriDiaFloating()
                                                    .toString());
                                    tributosDTO
                                            .setQtTriDiaRepique(ocorrenciasTriTributo
                                                    .getQtTriDiaRepique()
                                                    .toString());

                                    listaTri.add(tributosDTO);

                                    achouGR = true;
                                    continue pgtoTributos;
                                }
                            }
                            if (!achouDR) {
                                if (ocorrenciasTriTributo.getDsTriTipoTributo()
                                        .equalsIgnoreCase("D")) {
                                    tributosDTO.setExibeDebitoVeiculo("N");
                                    tributosDTO
                                            .setDsTriTipoTributo(saidaSegundoContrato
                                                    .getListaModTributos().get(
                                                            i)
                                                    .getCdModalidadeTributos());

                                    tributosDTO
                                            .setVlTriMaximoFavorecidoNao(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriMaximoFavorecidoNao()));
                                    tributosDTO
                                            .setVlTriLimiteDiario(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriLimiteDiario()));
                                    tributosDTO
                                            .setVlTriLimiteIndividual(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriLimiteIndividual()));
                                    tributosDTO
                                            .setQttriDiaFloating(ocorrenciasTriTributo
                                                    .getQttriDiaFloating()
                                                    .toString());
                                    tributosDTO
                                            .setQtTriDiaRepique(ocorrenciasTriTributo
                                                    .getQtTriDiaRepique()
                                                    .toString());

                                    listaTri.add(tributosDTO);

                                    achouDR = true;
                                    continue pgtoTributos;
                                }
                            }

                            if (!achouGP) {
                                if (ocorrenciasTriTributo.getDsTriTipoTributo()
                                        .equalsIgnoreCase("P")) {
                                    tributosDTO.setExibeDebitoVeiculo("N");
                                    tributosDTO
                                            .setDsTriTipoTributo(saidaSegundoContrato
                                                    .getListaModTributos().get(
                                                            i)
                                                    .getCdModalidadeTributos());

                                    tributosDTO
                                            .setVlTriMaximoFavorecidoNao(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriMaximoFavorecidoNao()));
                                    tributosDTO
                                            .setVlTriLimiteDiario(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriLimiteDiario()));
                                    tributosDTO
                                            .setVlTriLimiteIndividual(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriLimiteIndividual()));
                                    tributosDTO
                                            .setQttriDiaFloating(ocorrenciasTriTributo
                                                    .getQttriDiaFloating()
                                                    .toString());
                                    tributosDTO
                                            .setQtTriDiaRepique(ocorrenciasTriTributo
                                                    .getQtTriDiaRepique()
                                                    .toString());

                                    listaTri.add(tributosDTO);

                                    achouGP = true;
                                    continue pgtoTributos;
                                }
                            }
                            if (!achouCB) {
                                if (ocorrenciasTriTributo.getDsTriTipoTributo()
                                        .equalsIgnoreCase("B")) {
                                    tributosDTO.setExibeDebitoVeiculo("N");
                                    tributosDTO
                                            .setDsTriTipoTributo(saidaSegundoContrato
                                                    .getListaModTributos().get(
                                                            i)
                                                    .getCdModalidadeTributos());

                                    tributosDTO
                                            .setVlTriMaximoFavorecidoNao(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriMaximoFavorecidoNao()));
                                    tributosDTO
                                            .setVlTriLimiteDiario(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriLimiteDiario()));
                                    tributosDTO
                                            .setVlTriLimiteIndividual(NumberUtils
                                                    .format(ocorrenciasTriTributo
                                                            .getVlTriLimiteIndividual()));
                                    tributosDTO
                                            .setQttriDiaFloating(ocorrenciasTriTributo
                                                    .getQttriDiaFloating()
                                                    .toString());
                                    tributosDTO
                                            .setQtTriDiaRepique(ocorrenciasTriTributo
                                                    .getQtTriDiaRepique()
                                                    .toString());

                                    listaTri.add(tributosDTO);

                                    achouCB = true;
                                    continue pgtoTributos;
                                }
                            }
                        }
                    }

                }
            }
        }
        listaPagamentosTri.add(pagamentosTributosDTO);

        // Pagamento de sal�rios
        String exibePgtoSalarios = "N";
        PagamentosDTO pagamentosSalariosDTO = new PagamentosDTO();
        pagamentosSalariosDTO
                .setDsSalarioEmissaoAntecipadoCartao(saidaSegundoContrato
                        .getDsSalarioEmissaoAntecipadoCartao());
        pagamentosSalariosDTO
                .setCdSalarioQuantidadeLimiteSolicitacaoCartao(saidaSegundoContrato
                        .getCdSalarioQuantidadeLimiteSolicitacaoCartao()
                        .toString());
        pagamentosSalariosDTO
                .setDsSalarioAberturaContaExpressa(saidaSegundoContrato
                        .getDsSalarioAberturaContaExpressa());

        List<PagamentosDTO> listaSalariosModularidades = new ArrayList<PagamentosDTO>();

        achouCreditoConta = false;
        achouDOC = false;
        achouTED = false;
        achouOrdemPagamento = false;

        int exibeLinhaSalario = saidaSegundoContrato.getQtModalidadeSalario() / 2;
        pgtoSalarios: for (int i = 0; i < saidaSegundoContrato
                .getQtModalidadeSalario(); i++) {
            servicosModalidadesDTO = new ServicosModalidadesDTO();
            servicosModalidadesDTO.setDsServico(saidaSegundoContrato
                    .getCdServicoSalarial());
            servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato
                    .getListaModSalario().get(i).getCdModalidadeSalarial());

            if (saidaSegundoContrato.getQtModalidadeSalario() % 2 == 0) {
                if (i + 1 == exibeLinhaSalario) {
                    servicosModalidadesDTO.setImprimeLinhaServico("S");
                }
            } else {
                if (i == exibeLinhaSalario) {
                    servicosModalidadesDTO.setImprimeLinhaServico("S");
                }
            }

            listaServModalidades.add(servicosModalidadesDTO);

            // Bloco de Servi�o de Pagamento de Sal�rios
            PagamentosDTO salariosModularidades = null;
            for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato
                    .getListaServicoPagamento()) {
                if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("S")) {
                    exibePgtoSalarios = "S";
                    try {
                        BeanUtils.copyProperties(pagamentosSalariosDTO,
                                ocorrencias);
                        pagamentosSalariosDTO
                                .setDsServicoTipoServico(saidaSegundoContrato
                                        .getCdServicoSalarial());
                    } catch (IllegalAccessException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    // Busca pela Modalidade Cr�dito em conta
                    if (!achouCreditoConta) {
                        for (OcorrenciasCreditos ocorrenciasCreditos : saidaSegundoContrato
                                .getListaCreditos()) {
                            if (ocorrenciasCreditos.getDsCretipoServico()
                                    .equalsIgnoreCase("S")) {
                                achouCreditoConta = true;
                                try {
                                    BeanUtils.copyProperties(
                                            pagamentosSalariosDTO,
                                            ocorrenciasCreditos);
                                } catch (IllegalAccessException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                pagamentosSalariosDTO
                                        .setDsCretipoServico(saidaSegundoContrato
                                                .getListaModSalario().get(i)
                                                .getCdModalidadeSalarial());
                                pagamentosSalariosDTO
                                        .setPcCreMaximoInconsistente(ocorrenciasCreditos
                                                .getPcCreMaximoInconsistente()
                                                .toString());
                                pagamentosSalariosDTO
                                        .setQtCreMaximaInconsistencia(PgitUtil
                                                .formatarNumero(
                                                        ocorrenciasCreditos
                                                                .getQtCreMaximaInconsistencia(),
                                                        true));
                                pagamentosSalariosDTO
                                        .setVlCreMaximoFavorecidoNao(NumberUtils
                                                .format(ocorrenciasCreditos
                                                        .getVlCreMaximoFavorecidoNao()));
                                pagamentosSalariosDTO
                                        .setVlCreLimiteDiario(NumberUtils
                                                .format(ocorrenciasCreditos
                                                        .getVlCreLimiteDiario()));
                                pagamentosSalariosDTO
                                        .setVlCreLimiteIndividual(NumberUtils
                                                .format(ocorrenciasCreditos
                                                        .getVlCreLimiteIndividual()));
                                pagamentosSalariosDTO
                                        .setQtCreDiaRepique(ocorrenciasCreditos
                                                .getQtCreDiaRepique()
                                                .toString());
                                pagamentosSalariosDTO
                                        .setQtCreDiaFloating(ocorrenciasCreditos
                                                .getQtCreDiaFloating()
                                                .toString());

                                continue pgtoSalarios;
                            }
                        }
                        achouCreditoConta = true;
                        pagamentosSalariosDTO.setDsCretipoServico("N");
                    }

                    // Busca pela Modalidade Pagemanto de modularidades (DOC /
                    // TED)
                    if (!achouDOC || !achouTED) {
                        for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
                                .getListaModularidades()) {
                            if (ocorrenciasModularidades.getDsModTipoServico()
                                    .equalsIgnoreCase("S")) {
                                salariosModularidades = new PagamentosDTO();

                                try {
                                    BeanUtils.copyProperties(
                                            salariosModularidades,
                                            ocorrenciasModularidades);
                                } catch (IllegalAccessException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                if (!achouDOC) {
                                    if (ocorrenciasModularidades
                                            .getDsModTipoModalidade()
                                            .equalsIgnoreCase("D")) {
                                        pagamentosSalariosDTO
                                                .setDsModTipoServico("S");
                                        salariosModularidades
                                                .setDsModTipoServico(saidaSegundoContrato
                                                        .getListaModSalario()
                                                        .get(i)
                                                        .getCdModalidadeSalarial());

                                        salariosModularidades
                                                .setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
                                                        .getCdModIndicadorPercentualMaximoInconsistente()
                                                        .toString());
                                        salariosModularidades
                                                .setQtModMaximoInconsistencia(PgitUtil
                                                        .formatarNumero(
                                                                ocorrenciasModularidades
                                                                        .getQtModMaximoInconsistencia(),
                                                                true));
                                        salariosModularidades
                                                .setVlModMaximoFavorecidoNao(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModMaximoFavorecidoNao()));
                                        salariosModularidades
                                                .setVlModLimiteDiario(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteDiario()));
                                        salariosModularidades
                                                .setVlModLimiteIndividual(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteIndividual()));
                                        salariosModularidades
                                                .setQtModDiaFloating(ocorrenciasModularidades
                                                        .getQtModDiaFloating()
                                                        .toString());
                                        salariosModularidades
                                                .setQtModDiaRepique(ocorrenciasModularidades
                                                        .getQtModDiaRepique()
                                                        .toString());

                                        listaSalariosModularidades
                                                .add(salariosModularidades);
                                        achouDOC = true;
                                        continue pgtoSalarios;
                                    }
                                }
                                if (!achouTED) {
                                    if (ocorrenciasModularidades
                                            .getDsModTipoModalidade()
                                            .equalsIgnoreCase("T")) {
                                        pagamentosSalariosDTO
                                                .setDsModTipoServico("S");
                                        salariosModularidades
                                                .setDsModTipoServico(saidaSegundoContrato
                                                        .getListaModSalario()
                                                        .get(i)
                                                        .getCdModalidadeSalarial());

                                        salariosModularidades
                                                .setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
                                                        .getCdModIndicadorPercentualMaximoInconsistente()
                                                        .toString());
                                        salariosModularidades
                                                .setQtModMaximoInconsistencia(PgitUtil
                                                        .formatarNumero(
                                                                ocorrenciasModularidades
                                                                        .getQtModMaximoInconsistencia(),
                                                                true));
                                        salariosModularidades
                                                .setVlModMaximoFavorecidoNao(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModMaximoFavorecidoNao()));
                                        salariosModularidades
                                                .setVlModLimiteDiario(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteDiario()));
                                        salariosModularidades
                                                .setVlModLimiteIndividual(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteIndividual()));
                                        salariosModularidades
                                                .setQtModDiaFloating(ocorrenciasModularidades
                                                        .getQtModDiaFloating()
                                                        .toString());
                                        salariosModularidades
                                                .setQtModDiaRepique(ocorrenciasModularidades
                                                        .getQtModDiaRepique()
                                                        .toString());

                                        listaSalariosModularidades
                                                .add(salariosModularidades);
                                        achouTED = true;
                                        continue pgtoSalarios;
                                    }
                                }
                            }
                        }
                        achouDOC = true;
                        achouTED = true;
                        pagamentosSalariosDTO.setDsModTipoServico("N");
                    }

                    // Busca pela Modalidade Ordem de Pagamento
                    if (!achouOrdemPagamento) {
                        for (OcorrenciasOperacoes ocorrenciasOperacoes : saidaSegundoContrato
                                .getListaOperacoes()) {
                            if (ocorrenciasOperacoes.getDsOpgTipoServico()
                                    .equalsIgnoreCase("S")) {
                                achouOrdemPagamento = true;
                                try {
                                    BeanUtils.copyProperties(
                                            pagamentosSalariosDTO,
                                            ocorrenciasOperacoes);
                                } catch (IllegalAccessException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                pagamentosSalariosDTO
                                        .setDsOpgTipoServico(saidaSegundoContrato
                                                .getListaModSalario().get(i)
                                                .getCdModalidadeSalarial());
                                pagamentosSalariosDTO
                                        .setPcOpgMaximoInconsistencia(ocorrenciasOperacoes
                                                .getPcOpgMaximoInconsistencia()
                                                .toString());
                                pagamentosSalariosDTO
                                        .setQtOpgMaximoInconsistencia(PgitUtil
                                                .formatarNumero(
                                                        ocorrenciasOperacoes
                                                                .getQtOpgMaximoInconsistencia(),
                                                        true));
                                pagamentosSalariosDTO
                                        .setVlOpgLimiteDiario(NumberUtils
                                                .format(ocorrenciasOperacoes
                                                        .getVlOpgLimiteDiario()));
                                pagamentosSalariosDTO
                                        .setVlOpgLimiteIndividual(NumberUtils
                                                .format(ocorrenciasOperacoes
                                                        .getVlOpgLimiteIndividual()));
                                pagamentosSalariosDTO
                                        .setQtOpgDiaFloating(ocorrenciasOperacoes
                                                .getQtOpgDiaFloating()
                                                .toString());
                                pagamentosSalariosDTO
                                        .setVlOpgMaximoFavorecidoNao(NumberUtils
                                                .format(ocorrenciasOperacoes
                                                        .getVlOpgMaximoFavorecidoNao()));
                                pagamentosSalariosDTO
                                        .setQtOpgDiaRepique(ocorrenciasOperacoes
                                                .getQtOpgDiaRepique()
                                                .toString());
                                pagamentosSalariosDTO
                                        .setQtOpgDiaExpiracao(ocorrenciasOperacoes
                                                .getQtOpgDiaExpiracao()
                                                .toString());

                                continue pgtoSalarios;
                            }
                        }
                        achouOrdemPagamento = true;
                        pagamentosSalariosDTO.setDsOpgTipoServico("N");
                    }
                }
            }
        }

        // Pagamento de benef�cios
        String exibePgtoBeneficios = "N";
        PagamentosDTO pagamentosBeneficiosDTO = new PagamentosDTO();
        pagamentosBeneficiosDTO.setDsBprTipoAcao(saidaSegundoContrato
                .getDsBprTipoAcao().trim().equals("") ? "N"
                : saidaSegundoContrato.getDsBprTipoAcao());
        pagamentosBeneficiosDTO
                .setDsBeneficioInformadoAgenciaConta(saidaSegundoContrato
                        .getDsBeneficioInformadoAgenciaConta());
        pagamentosBeneficiosDTO
                .setDsBeneficioTipoIdentificacaoBeneficio(saidaSegundoContrato
                        .getDsBeneficioTipoIdentificacaoBeneficio());
        pagamentosBeneficiosDTO
                .setDsBeneficioUtilizacaoCadastroOrganizacao(saidaSegundoContrato
                        .getDsBeneficioUtilizacaoCadastroOrganizacao());
        pagamentosBeneficiosDTO
                .setDsBeneficioUtilizacaoCadastroProcuradores(saidaSegundoContrato
                        .getDsBeneficioUtilizacaoCadastroProcuradores());
        pagamentosBeneficiosDTO
                .setDsBeneficioPossuiExpiracaoCredito(saidaSegundoContrato
                        .getDsBeneficioPossuiExpiracaoCredito());
        pagamentosBeneficiosDTO
                .setCdBeneficioQuantidadeDiasExpiracaoCredito(saidaSegundoContrato
                        .getCdBeneficioQuantidadeDiasExpiracaoCredito()
                        .toString());
        pagamentosBeneficiosDTO
                .setCdBeneficioQuantidadeDiasExpiracaoCredito(saidaSegundoContrato
                        .getCdBeneficioQuantidadeDiasExpiracaoCredito()
                        .toString());
        pagamentosBeneficiosDTO.setQtBprMesProvisorio(saidaSegundoContrato
                .getQtBprMesProvisorio().toString());
        pagamentosBeneficiosDTO
                .setCdBprIndicadorEmissaoAviso(saidaSegundoContrato
                        .getCdBprIndicadorEmissaoAviso());
        pagamentosBeneficiosDTO.setQtBprDiaAnteriorAviso(saidaSegundoContrato
                .getQtBprDiaAnteriorAviso().toString());
        pagamentosBeneficiosDTO
                .setQtBprDiaAnteriorVencimento(saidaSegundoContrato
                        .getQtBprDiaAnteriorVencimento().toString());
        pagamentosBeneficiosDTO
                .setCdBprUtilizadoMensagemPersonalizada(saidaSegundoContrato
                        .getCdBprUtilizadoMensagemPersonalizada());
        pagamentosBeneficiosDTO.setDsBprDestino(saidaSegundoContrato
                .getDsBprDestino());
        pagamentosBeneficiosDTO
                .setDsBprCadastroEnderecoUtilizado(saidaSegundoContrato
                        .getDsBprCadastroEnderecoUtilizado());

        List<PagamentosDTO> listaBeneficiosModularidades = new ArrayList<PagamentosDTO>();

        achouCreditoConta = false;
        achouDOC = false;
        achouTED = false;
        achouOrdemPagamento = false;

        int exibeLinhaBeneficio = saidaSegundoContrato
                .getQtModalidadeBeneficio() / 2;
        pgtoBeneficios: for (int i = 0; i < saidaSegundoContrato
                .getQtModalidadeBeneficio(); i++) {
            servicosModalidadesDTO = new ServicosModalidadesDTO();
            servicosModalidadesDTO.setDsServico(saidaSegundoContrato
                    .getCdServicoBeneficio());
            servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato
                    .getListaModBeneficio().get(i).getCdModalidadeBeneficio());

            if (saidaSegundoContrato.getQtModalidadeBeneficio() % 2 == 0) {
                if (i + 1 == exibeLinhaBeneficio) {
                    servicosModalidadesDTO.setImprimeLinhaServico("S");
                }
            } else {
                if (i == exibeLinhaBeneficio) {
                    servicosModalidadesDTO.setImprimeLinhaServico("S");
                }
            }

            listaServModalidades.add(servicosModalidadesDTO);

            // Bloco de Servi�o de Pagamento de Benef�cios
            PagamentosDTO beneficiosModularidades = null;
            for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato
                    .getListaServicoPagamento()) {
                if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("B")) {
                    exibePgtoBeneficios = "S";
                    try {
                        BeanUtils.copyProperties(pagamentosBeneficiosDTO,
                                ocorrencias);
                        pagamentosBeneficiosDTO
                                .setDsServicoTipoServico(saidaSegundoContrato
                                        .getCdServicoBeneficio());
                    } catch (IllegalAccessException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    // Busca pela Modalidade Cr�dito em conta
                    if (!achouCreditoConta) {
                        for (OcorrenciasCreditos ocorrenciasCreditos : saidaSegundoContrato
                                .getListaCreditos()) {
                            if (ocorrenciasCreditos.getDsCretipoServico()
                                    .equalsIgnoreCase("B")) {
                                achouCreditoConta = true;
                                try {
                                    BeanUtils.copyProperties(
                                            pagamentosBeneficiosDTO,
                                            ocorrenciasCreditos);
                                } catch (IllegalAccessException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                pagamentosBeneficiosDTO
                                        .setDsCretipoServico(saidaSegundoContrato
                                                .getListaModBeneficio().get(i)
                                                .getCdModalidadeBeneficio());
                                pagamentosBeneficiosDTO
                                        .setPcCreMaximoInconsistente(ocorrenciasCreditos
                                                .getPcCreMaximoInconsistente()
                                                .toString());
                                pagamentosBeneficiosDTO
                                        .setQtCreMaximaInconsistencia(PgitUtil
                                                .formatarNumero(
                                                        ocorrenciasCreditos
                                                                .getQtCreMaximaInconsistencia(),
                                                        true));
                                pagamentosBeneficiosDTO
                                        .setVlCreMaximoFavorecidoNao(NumberUtils
                                                .format(ocorrenciasCreditos
                                                        .getVlCreMaximoFavorecidoNao()));
                                pagamentosBeneficiosDTO
                                        .setVlCreLimiteDiario(NumberUtils
                                                .format(ocorrenciasCreditos
                                                        .getVlCreLimiteDiario()));
                                pagamentosBeneficiosDTO
                                        .setVlCreLimiteIndividual(NumberUtils
                                                .format(ocorrenciasCreditos
                                                        .getVlCreLimiteIndividual()));
                                pagamentosBeneficiosDTO
                                        .setQtCreDiaRepique(ocorrenciasCreditos
                                                .getQtCreDiaRepique()
                                                .toString());
                                pagamentosBeneficiosDTO
                                        .setQtCreDiaFloating(ocorrenciasCreditos
                                                .getQtCreDiaFloating()
                                                .toString());

                                continue pgtoBeneficios;
                            }
                        }
                        achouCreditoConta = true;
                        pagamentosBeneficiosDTO.setDsCretipoServico("N");
                    }

                    // Busca pela Modalidade Pagemanto de modularidades (DOC /
                    // TED)
                    if (!achouDOC || !achouTED) {
                        for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
                                .getListaModularidades()) {
                            if (ocorrenciasModularidades.getDsModTipoServico()
                                    .equalsIgnoreCase("B")) {
                                beneficiosModularidades = new PagamentosDTO();

                                try {
                                    BeanUtils.copyProperties(
                                            beneficiosModularidades,
                                            ocorrenciasModularidades);
                                } catch (IllegalAccessException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                if (!achouDOC) {
                                    if (ocorrenciasModularidades
                                            .getDsModTipoModalidade()
                                            .equalsIgnoreCase("D")) {
                                        pagamentosBeneficiosDTO
                                                .setDsModTipoServico("S");

                                        beneficiosModularidades
                                                .setDsModTipoServico(saidaSegundoContrato
                                                        .getListaModBeneficio()
                                                        .get(i)
                                                        .getCdModalidadeBeneficio());
                                        beneficiosModularidades
                                                .setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
                                                        .getCdModIndicadorPercentualMaximoInconsistente()
                                                        .toString());
                                        beneficiosModularidades
                                                .setQtModMaximoInconsistencia(PgitUtil
                                                        .formatarNumero(
                                                                ocorrenciasModularidades
                                                                        .getQtModMaximoInconsistencia(),
                                                                true));
                                        beneficiosModularidades
                                                .setVlModMaximoFavorecidoNao(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModMaximoFavorecidoNao()));
                                        beneficiosModularidades
                                                .setVlModLimiteDiario(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteDiario()));
                                        beneficiosModularidades
                                                .setVlModLimiteIndividual(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteIndividual()));
                                        beneficiosModularidades
                                                .setQtModDiaFloating(ocorrenciasModularidades
                                                        .getQtModDiaFloating()
                                                        .toString());
                                        beneficiosModularidades
                                                .setQtModDiaRepique(ocorrenciasModularidades
                                                        .getQtModDiaRepique()
                                                        .toString());

                                        listaBeneficiosModularidades
                                                .add(beneficiosModularidades);
                                        achouDOC = true;
                                        continue pgtoBeneficios;
                                    }
                                }
                                if (!achouTED) {
                                    if (ocorrenciasModularidades
                                            .getDsModTipoModalidade()
                                            .equalsIgnoreCase("T")) {
                                        pagamentosSalariosDTO
                                                .setDsModTipoServico("S");
                                        pagamentosBeneficiosDTO
                                                .setDsModTipoServico("S");

                                        beneficiosModularidades
                                                .setDsModTipoServico(saidaSegundoContrato
                                                        .getListaModBeneficio()
                                                        .get(i)
                                                        .getCdModalidadeBeneficio());
                                        beneficiosModularidades
                                                .setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
                                                        .getCdModIndicadorPercentualMaximoInconsistente()
                                                        .toString());
                                        beneficiosModularidades
                                                .setQtModMaximoInconsistencia(PgitUtil
                                                        .formatarNumero(
                                                                ocorrenciasModularidades
                                                                        .getQtModMaximoInconsistencia(),
                                                                true));
                                        beneficiosModularidades
                                                .setVlModMaximoFavorecidoNao(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModMaximoFavorecidoNao()));
                                        beneficiosModularidades
                                                .setVlModLimiteDiario(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteDiario()));
                                        beneficiosModularidades
                                                .setVlModLimiteIndividual(NumberUtils
                                                        .format(ocorrenciasModularidades
                                                                .getVlModLimiteIndividual()));
                                        beneficiosModularidades
                                                .setQtModDiaFloating(ocorrenciasModularidades
                                                        .getQtModDiaFloating()
                                                        .toString());
                                        beneficiosModularidades
                                                .setQtModDiaRepique(ocorrenciasModularidades
                                                        .getQtModDiaRepique()
                                                        .toString());

                                        listaBeneficiosModularidades
                                                .add(beneficiosModularidades);
                                        achouTED = true;
                                        continue pgtoBeneficios;
                                    }
                                }
                            }
                        }
                        achouDOC = true;
                        achouTED = true;
                        pagamentosBeneficiosDTO.setDsModTipoServico("N");
                    }

                    // Busca pela Modalidade Ordem de Pagamento
                    if (!achouOrdemPagamento) {
                        for (OcorrenciasOperacoes ocorrenciasOperacoes : saidaSegundoContrato
                                .getListaOperacoes()) {
                            if (ocorrenciasOperacoes.getDsOpgTipoServico()
                                    .equalsIgnoreCase("B")) {
                                achouOrdemPagamento = true;
                                try {
                                    BeanUtils.copyProperties(
                                            pagamentosBeneficiosDTO,
                                            ocorrenciasOperacoes);
                                } catch (IllegalAccessException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                pagamentosBeneficiosDTO
                                        .setDsOpgTipoServico(saidaSegundoContrato
                                                .getListaModBeneficio().get(i)
                                                .getCdModalidadeBeneficio());
                                pagamentosBeneficiosDTO
                                        .setPcOpgMaximoInconsistencia(ocorrenciasOperacoes
                                                .getPcOpgMaximoInconsistencia()
                                                .toString());
                                pagamentosBeneficiosDTO
                                        .setQtOpgMaximoInconsistencia(PgitUtil
                                                .formatarNumero(
                                                        ocorrenciasOperacoes
                                                                .getQtOpgMaximoInconsistencia(),
                                                        true));
                                pagamentosBeneficiosDTO
                                        .setVlOpgLimiteDiario(NumberUtils
                                                .format(ocorrenciasOperacoes
                                                        .getVlOpgLimiteDiario()));
                                pagamentosBeneficiosDTO
                                        .setVlOpgLimiteIndividual(NumberUtils
                                                .format(ocorrenciasOperacoes
                                                        .getVlOpgLimiteIndividual()));
                                pagamentosBeneficiosDTO
                                        .setQtOpgDiaFloating(ocorrenciasOperacoes
                                                .getQtOpgDiaFloating()
                                                .toString());
                                pagamentosBeneficiosDTO
                                        .setVlOpgMaximoFavorecidoNao(NumberUtils
                                                .format(ocorrenciasOperacoes
                                                        .getVlOpgMaximoFavorecidoNao()));
                                pagamentosBeneficiosDTO
                                        .setQtOpgDiaRepique(ocorrenciasOperacoes
                                                .getQtOpgDiaRepique()
                                                .toString());
                                pagamentosBeneficiosDTO
                                        .setQtOpgDiaExpiracao(ocorrenciasOperacoes
                                                .getQtOpgDiaExpiracao()
                                                .toString());

                                continue pgtoBeneficios;
                            }
                        }
                        achouOrdemPagamento = true;
                        pagamentosBeneficiosDTO.setDsOpgTipoServico("N");
                    }
                }
            }

        }

        // Emiss�o de aviso de movimenta��o
        if (saidaSegundoContrato.getCdIndicadorAviso().equalsIgnoreCase("S")) {
            servicosModalidadesDTO = new ServicosModalidadesDTO();
            servicosModalidadesDTO.setDsServico("AVISO DE MOVIMENTA��O");
            StringBuilder bufferMovimentacao = new StringBuilder();
            bufferMovimentacao.append("(");
            bufferMovimentacao.append(saidaSegundoContrato
                    .getCdIndicadorAvisoPagador());
            bufferMovimentacao.append(")PAGADOR (");
            bufferMovimentacao.append(saidaSegundoContrato
                    .getCdIndicadorAvisoFavorecido());
            bufferMovimentacao.append(")FAVORECIDO");
            servicosModalidadesDTO.setDsModalidade(bufferMovimentacao
                    .toString());
            servicosModalidadesDTO.setImprimeLinhaServico("S");

            listaServModalidades.add(servicosModalidadesDTO);
        }

        // Emiss�o de comprovante de pagamento
        if (saidaSegundoContrato.getCdIndicadorComplemento().equalsIgnoreCase(
                "S")) {
            servicosModalidadesDTO = new ServicosModalidadesDTO();
            servicosModalidadesDTO.setDsServico("COMPROVANTE DE PAGAMENTO");
            StringBuilder bufferComplemento = new StringBuilder();
            bufferComplemento.append("(");
            bufferComplemento.append(saidaSegundoContrato
                    .getCdIndicadorComplementoPagador());
            bufferComplemento.append(")PAGADOR (");
            bufferComplemento.append(saidaSegundoContrato
                    .getCdIndicadorComplementoFavorecido());
            bufferComplemento.append(")FAVORECIDO (");
            bufferComplemento.append(saidaSegundoContrato
                    .getCdIndicadorComplementoSalarial());
            bufferComplemento.append(")SALARIAL (");
            bufferComplemento.append(saidaSegundoContrato
                    .getCdIndicadorComplementoDivergente());
            bufferComplemento.append(")DIVERSOS");
            servicosModalidadesDTO
                    .setDsModalidade(bufferComplemento.toString());
            servicosModalidadesDTO.setImprimeLinhaServico("S");

            listaServModalidades.add(servicosModalidadesDTO);
        }

        // Cadastro de Favorecidos
        StringBuilder bufferCadFavorecido = new StringBuilder();
        servicosModalidadesDTO = new ServicosModalidadesDTO();
        servicosModalidadesDTO.setDsServico("CADASTRO DE FAVORECIDOS");
        bufferCadFavorecido
                .append(saidaSegundoContrato.getCdIndcadorCadastroFavorecido()
                        .equalsIgnoreCase("S") ? "(X)SIM ()N�O"
                        : "()SIM (X)N�O");
        servicosModalidadesDTO.setDsModalidade(bufferCadFavorecido.toString());
        servicosModalidadesDTO.setImprimeLinhaServico("S");

        listaServModalidades.add(servicosModalidadesDTO);

        // Recadastramento de Benefici�rios
        int exibeLinhaRecadastramento = saidaSegundoContrato
                .getQtModalidadeRecadastro() / 2;
        for (int i = 0; i < saidaSegundoContrato.getQtModalidadeRecadastro(); i++) {
            servicosModalidadesDTO = new ServicosModalidadesDTO();
            servicosModalidadesDTO.setDsServico(saidaSegundoContrato
                    .getDsServicoRecadastro());
            servicosModalidadesDTO
                    .setDsModalidade(saidaSegundoContrato
                            .getListaModRecadastro().get(i)
                            .getCdModalidadeRecadastro());
            if (saidaSegundoContrato.getQtModalidadeRecadastro() % 2 == 0) {
                if (i + 1 == exibeLinhaRecadastramento) {
                    servicosModalidadesDTO.setImprimeLinhaServico("S");
                }
            } else {
                if (i == exibeLinhaRecadastramento) {
                    servicosModalidadesDTO.setImprimeLinhaServico("S");
                }
            }

            listaServModalidades.add(servicosModalidadesDTO);
        }

        JRBeanCollectionDataSource beanParamServModalidades = new JRBeanCollectionDataSource(
                listaServModalidades);
        JRBeanCollectionDataSource beanParamFornecTitulos = new JRBeanCollectionDataSource(
                listaFornecTitulos);
        JRBeanCollectionDataSource beanParamFornecModularidades = new JRBeanCollectionDataSource(
                listaFornecModularidades);
        JRBeanCollectionDataSource beanParamFornecOutrasModularidades = new JRBeanCollectionDataSource(
                listaFornecOutrasModularidades);
        JRBeanCollectionDataSource beanParamPagamentosTributos = new JRBeanCollectionDataSource(
                listaPagamentosTri);
        JRBeanCollectionDataSource beanParamTributos = new JRBeanCollectionDataSource(
                listaTri);
        
        
        listaTitulos = listaFornecTitulos;
        listaFornModularidades = listaFornecModularidades;
        
        listaPagamentosFornec = new ArrayList<PagamentosDTO>();
        listaPagamentosFornec.add(pagamentosFornecedoresDTO);
        
        
        JRBeanCollectionDataSource beanParamPagamentosFornecedores = new JRBeanCollectionDataSource(
                listaPagamentosFornec);

        JRBeanCollectionDataSource beanParamPagamentosFornecedoresCompl = new JRBeanCollectionDataSource(
                listaPagamentosFornec);

        listaPagamentosSalarios = new ArrayList<PagamentosDTO>();
        listaPagamentosSalarios.add(pagamentosSalariosDTO);
        
        
        JRBeanCollectionDataSource beanParamPagamentosSalarios = new JRBeanCollectionDataSource(
                listaPagamentosSalarios);
        JRBeanCollectionDataSource beanParamSalariosModularidades = new JRBeanCollectionDataSource(
                listaSalariosModularidades);

        listaPagamentosBeneficios = new ArrayList<PagamentosDTO>();
        listaPagamentosBeneficios.add(pagamentosBeneficiosDTO);
        
        
        JRBeanCollectionDataSource beanParamPagamentosBeneficios = new JRBeanCollectionDataSource(
                listaPagamentosBeneficios);
        JRBeanCollectionDataSource beanParamPagamentosBeneficiosModularidades = new JRBeanCollectionDataSource(
                listaBeneficiosModularidades);      
        
        
        String servicos = pathArquivos + "/relServicos.jasper";
        String pathSubRelServicos = servletContext.getRealPath(servicos);

        String servicosCompl = pathArquivos + "/relServicosCompl.jasper";
        String pathSubRelServicosCompl = servletContext
                .getRealPath(servicosCompl);

        ServicosDTO servicosDTO = new ServicosDTO();
        boolean avisoFavorecido = false;
        boolean avisoPagador = false;
        boolean comprovanteFavorecido = false;
        boolean comprovantePagador = false;

        for (OcorrenciasAvisos ocorrenciasAvisos : saidaSegundoContrato
                .getListaAvisos()) {

            if (!ocorrenciasAvisos.getDsAvisoTipoAviso().trim().equals("")) {
                if (ocorrenciasAvisos.getDsAvisoTipoAviso().equalsIgnoreCase(
                        "F")) {
                    avisoFavorecido = true;
                    servicosDTO
                            .setDsAvisoTipoAvisoFavorecido("SERVI�O: Aviso Movimenta��o Favorecido");

                    servicosDTO
                            .setDsAvisoPeriodicidadeEmissaoFavorecido(ocorrenciasAvisos
                                    .getDsAvisoPeriodicidadeEmissao());
                    servicosDTO.setDsAvisoDestinoFavorecido(ocorrenciasAvisos
                            .getDsAvisoDestino());
                    servicosDTO
                            .setDsAvisoPermissaoCorrespondenciaAberturaFavorecido(ocorrenciasAvisos
                                    .getDsAvisoPermissaoCorrespondenciaAbertura());
                    servicosDTO
                            .setDsAvisoDemontracaoInformacaoReservadaFavorecido(ocorrenciasAvisos
                                    .getDsAvisoDemontracaoInformacaoReservada());
                    servicosDTO
                            .setCdAvisoQuantidadeViasEmitirFavorecido(ocorrenciasAvisos
                                    .getCdAvisoQuantidadeViasEmitir()
                                    .toString());
                    servicosDTO
                            .setDsAvisoAgrupamentoCorrespondenciaFavorecido(ocorrenciasAvisos
                                    .getDsAvisoAgrupamentoCorrespondencia());

                } else {
                    avisoPagador = true;
                    servicosDTO
                            .setDsAvisoTipoAvisoPagador("SERVI�O: Aviso Movimenta��o Pagador");

                    servicosDTO
                            .setDsAvisoPeriodicidadeEmissaoPagador(ocorrenciasAvisos
                                    .getDsAvisoPeriodicidadeEmissao());
                    servicosDTO.setDsAvisoDestinoPagador(ocorrenciasAvisos
                            .getDsAvisoDestino());
                    servicosDTO
                            .setDsAvisoPermissaoCorrespondenciaAberturaPagador(ocorrenciasAvisos
                                    .getDsAvisoPermissaoCorrespondenciaAbertura());
                    servicosDTO
                            .setDsAvisoDemontracaoInformacaoReservadaPagador(ocorrenciasAvisos
                                    .getDsAvisoDemontracaoInformacaoReservada());
                    servicosDTO
                            .setCdAvisoQuantidadeViasEmitirPagador(ocorrenciasAvisos
                                    .getCdAvisoQuantidadeViasEmitir()
                                    .toString());
                    servicosDTO
                            .setDsAvisoAgrupamentoCorrespondenciaPagador(ocorrenciasAvisos
                                    .getDsAvisoAgrupamentoCorrespondencia());
                }
            } else {
                if (!avisoFavorecido) {
                    servicosDTO.setDsAvisoTipoAvisoFavorecido("N");
                }

                if (!avisoPagador) {
                    servicosDTO.setDsAvisoTipoAvisoPagador("N");
                }
            }

        }

        for (OcorrenciasComprovantes ocorrenciasComprovantes : saidaSegundoContrato
                .getListaComprovantes()) {
            if (!comprovanteFavorecido || !comprovantePagador) {
                if (!ocorrenciasComprovantes.getCdComprovanteTipoComprovante()
                        .trim().equals("")) {
                    if (ocorrenciasComprovantes
                            .getCdComprovanteTipoComprovante()
                            .equalsIgnoreCase("F")) {
                        comprovanteFavorecido = true;

                        servicosDTO
                                .setCdComprovanteTipoComprovanteFavorecido("SERVI�O: Comprovante Pagamento Favorecido");

                        servicosDTO
                                .setDsComprovantePeriodicidadeEmissaoFavorecido(ocorrenciasComprovantes
                                        .getDsComprovantePeriodicidadeEmissao());
                        servicosDTO
                                .setDsComprovanteDestinoFavorecido(ocorrenciasComprovantes
                                        .getDsComprovanteDestino());
                        servicosDTO
                                .setDsComprovantePermissaoCorrespondenteAberturaFavorecido(ocorrenciasComprovantes
                                        .getDsComprovantePermissaoCorrespondenteAbertura());
                        servicosDTO
                                .setDsDemonstrativoInformacaoReservadaFavorecido(ocorrenciasComprovantes
                                        .getDsDemonstrativoInformacaoReservada());
                        servicosDTO
                                .setQtViasEmitirFavorecido(ocorrenciasComprovantes
                                        .getQtViasEmitir().toString());
                        servicosDTO
                                .setDsAgrupamentoCorrespondenteFavorecido(ocorrenciasComprovantes
                                        .getDsAgrupamentoCorrespondente());
                    } else {
                        comprovantePagador = true;

                        servicosDTO
                                .setCdComprovanteTipoComprovantePagador("SERVI�O: Comprovante Pagamento Pagador");

                        servicosDTO
                                .setDsComprovantePeriodicidadeEmissaoPagador(ocorrenciasComprovantes
                                        .getDsComprovantePeriodicidadeEmissao());
                        servicosDTO
                                .setDsComprovanteDestinoPagador(ocorrenciasComprovantes
                                        .getDsComprovanteDestino());
                        servicosDTO
                                .setDsComprovantePermissaoCorrespondenteAberturaPagador(ocorrenciasComprovantes
                                        .getDsComprovantePermissaoCorrespondenteAbertura());
                        servicosDTO
                                .setDsDemonstrativoInformacaoReservadaPagador(ocorrenciasComprovantes
                                        .getDsDemonstrativoInformacaoReservada());
                        servicosDTO
                                .setQtViasEmitirPagador(ocorrenciasComprovantes
                                        .getQtViasEmitir().toString());
                        servicosDTO
                                .setDsAgrupamentoCorrespondentePagador(ocorrenciasComprovantes
                                        .getDsAgrupamentoCorrespondente());
                    }
                } else {
                    if (!comprovanteFavorecido) {
                        servicosDTO
                                .setCdComprovanteTipoComprovanteFavorecido("N");
                    }

                    if (!comprovantePagador) {
                        servicosDTO.setCdComprovanteTipoComprovantePagador("N");
                    }
                }
            }
        }

        boolean compSalarial = false;
        boolean compDiversos = false;
        for (OcorrenciasComprovanteSalariosDiversos ocorrenciasComprovanteSalariosDiversos : saidaSegundoContrato
                .getListaComprovanteSalarioDiversos()) {

            if (!compSalarial || !compDiversos) {
                if (!ocorrenciasComprovanteSalariosDiversos
                        .getDsCsdTipoComprovante().trim().equals("")) {
                    if (ocorrenciasComprovanteSalariosDiversos
                            .getDsCsdTipoComprovante().equalsIgnoreCase("S")) {
                        compSalarial = true;

                        servicosDTO
                                .setDsCsdTipoComprovanteSalarial("SERVI�O: Comprovante Salarial");

                        servicosDTO
                                .setDsCsdTipoRejeicaoLoteSalarial(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdTipoRejeicaoLote());
                        servicosDTO
                                .setDsCsdMeioDisponibilizacaoCorrentistaSalarial(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdMeioDisponibilizacaoCorrentista());
                        servicosDTO
                                .setDsCsdMediaDisponibilidadeCorrentistaSalarial(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdMediaDisponibilidadeCorrentista());
                        servicosDTO
                                .setDsCsdMediaDisponibilidadeNumeroCorrentistasSalarial(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdMediaDisponibilidadeNumeroCorrentistas());
                        servicosDTO
                                .setDsCsdDestinoSalarial(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdDestino());
                        servicosDTO
                                .setQtCsdMesEmissaoSalarial(ocorrenciasComprovanteSalariosDiversos
                                        .getQtCsdMesEmissao().toString());
                        servicosDTO
                                .setDsCsdCadastroConservadorEnderecoSalarial(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdCadastroConservadorEndereco());
                        servicosDTO
                                .setCdCsdQuantidadeViasEmitirSalarial(ocorrenciasComprovanteSalariosDiversos
                                        .getCdCsdQuantidadeViasEmitir() == 0 ? "N"
                                        : ocorrenciasComprovanteSalariosDiversos
                                                .getCdCsdQuantidadeViasEmitir()
                                                .toString());
                    } else {
                        compDiversos = true;

                        servicosDTO
                                .setDsCsdTipoComprovanteDiversos("SERVI�O: Comprovante Diversos");

                        servicosDTO
                                .setDsCsdTipoRejeicaoLoteDiversos(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdTipoRejeicaoLote());
                        servicosDTO
                                .setDsCsdMeioDisponibilizacaoCorrentistaDiversos(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdMeioDisponibilizacaoCorrentista());
                        servicosDTO
                                .setDsCsdMediaDisponibilidadeCorrentistaDiversos(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdMediaDisponibilidadeCorrentista());
                        servicosDTO
                                .setDsCsdMediaDisponibilidadeNumeroCorrentistasDiversos(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdMediaDisponibilidadeNumeroCorrentistas());
                        servicosDTO
                                .setDsCsdDestinoDiversos(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdDestino());
                        servicosDTO
                                .setQtCsdMesEmissaoDiversos(ocorrenciasComprovanteSalariosDiversos
                                        .getQtCsdMesEmissao().toString());
                        servicosDTO
                                .setDsCsdCadastroConservadorEnderecoDiversos(ocorrenciasComprovanteSalariosDiversos
                                        .getDsCsdCadastroConservadorEndereco());
                        servicosDTO
                                .setCdCsdQuantidadeViasEmitirDiversos(ocorrenciasComprovanteSalariosDiversos
                                        .getCdCsdQuantidadeViasEmitir() == 0 ? "N"
                                        : ocorrenciasComprovanteSalariosDiversos
                                                .getCdCsdQuantidadeViasEmitir()
                                                .toString());
                    }
                } else {
                    if (!compSalarial) {
                        servicosDTO.setDsCsdTipoComprovanteSalarial("N");
                    }

                    if (!compDiversos) {
                        servicosDTO.setDsCsdTipoComprovanteDiversos("N");
                    }
                }
            }
        }

        servicosDTO
                .setCdIndcadorCadastroFavorecido(saidaSegundoContrato
                        .getCdIndcadorCadastroFavorecido()
                        .equalsIgnoreCase("S") ? saidaSegundoContrato
                        .getCdIndcadorCadastroFavorecido() : "N");

        servicosDTO.setDsCadastroFormaManutencao(saidaSegundoContrato
                .getDsCadastroFormaManutencao());
        servicosDTO.setCdCadastroQuantidadeDiasInativos(saidaSegundoContrato
                .getCdCadastroQuantidadeDiasInativos().toString());
        servicosDTO.setDsCadastroTipoConsistenciaInscricao(saidaSegundoContrato
                .getDsCadastroTipoConsistenciaInscricao());

        servicosDTO.setDsServicoRecadastro(saidaSegundoContrato
                .getDsServicoRecadastro().trim().equals("") ? "N"
                : saidaSegundoContrato.getDsServicoRecadastro());
        servicosDTO.setDsRecTipoIdentificacaoBeneficio(saidaSegundoContrato
                .getDsRecTipoIdentificacaoBeneficio());
        servicosDTO.setDsRecTipoConsistenciaIdentificacao(saidaSegundoContrato
                .getDsRecTipoConsistenciaIdentificacao());
        servicosDTO.setDsRecCondicaoEnquadramento(saidaSegundoContrato
                .getDsRecCondicaoEnquadramento());
        servicosDTO.setDsRecCriterioPrincipalEnquadramento(saidaSegundoContrato
                .getDsRecCriterioPrincipalEnquadramento());
        servicosDTO
                .setDsRecTipoCriterioCompostoEnquadramento(saidaSegundoContrato
                        .getDsRecTipoCriterioCompostoEnquadramento());
        servicosDTO.setQtRecEtapaRecadastramento(saidaSegundoContrato
                .getQtRecEtapaRecadastramento().toString());
        servicosDTO.setQtRecFaseEtapaRecadastramento(saidaSegundoContrato
                .getQtRecFaseEtapaRecadastramento().toString());
        servicosDTO.setDtRecInicioRecadastramento(saidaSegundoContrato
                .getDtRecInicioRecadastramento());
        servicosDTO.setDtRecFinalRecadastramento(saidaSegundoContrato
                .getDtRecFinalRecadastramento());
        servicosDTO.setDsRecGeradorRetornoInternet(saidaSegundoContrato
                .getDsRecGeradorRetornoInternet());

        boolean aviso = false;
        boolean formulario = false;

        for (OcorrenciasAvisoFormularios ocorrenciasAvisoFormularios : saidaSegundoContrato
                .getListaAvisoFormularios()) {
            if (!aviso || !formulario) {
                if (!ocorrenciasAvisoFormularios.getDsRecTipoModalidade()
                        .trim().equals("")) {
                    if (ocorrenciasAvisoFormularios.getDsRecTipoModalidade()
                            .equalsIgnoreCase("A")) {
                        aviso = true;

                        servicosDTO
                                .setDsRecTipoModalidadeAviso("Aviso de Recadastramento");

                        servicosDTO
                                .setDsRecMomentoEnvioAviso(ocorrenciasAvisoFormularios
                                        .getDsRecMomentoEnvio());
                        servicosDTO
                                .setDsRecDestinoAviso(ocorrenciasAvisoFormularios
                                        .getDsRecDestino());
                        servicosDTO
                                .setDsRecCadastroEnderecoUtilizadoAviso(ocorrenciasAvisoFormularios
                                        .getDsRecCadastroEnderecoUtilizado());
                        servicosDTO
                                .setQtRecDiaAvisoAntecipadoAviso(ocorrenciasAvisoFormularios
                                        .getQtRecDiaAvisoAntecipado()
                                        .toString());
                        servicosDTO
                                .setDsRecPermissaoAgrupamentoAviso(ocorrenciasAvisoFormularios
                                        .getDsRecPermissaoAgrupamento());
                    } else {
                        formulario = true;

                        servicosDTO
                                .setDsRecTipoModalidadeFormulario("Formul�rio de Recadastramento");

                        servicosDTO
                                .setDsRecMomentoEnvioFormulario(ocorrenciasAvisoFormularios
                                        .getDsRecMomentoEnvio());
                        servicosDTO
                                .setDsRecDestinoFormulario(ocorrenciasAvisoFormularios
                                        .getDsRecDestino());
                        servicosDTO
                                .setDsRecCadastroEnderecoUtilizadoFormulario(ocorrenciasAvisoFormularios
                                        .getDsRecCadastroEnderecoUtilizado());
                        servicosDTO
                                .setQtRecDiaAntecipadoFormulario(ocorrenciasAvisoFormularios
                                        .getQtRecDiaAvisoAntecipado()
                                        .toString());
                        servicosDTO
                                .setDsRecPermissaoAgrupamentoFormulario(ocorrenciasAvisoFormularios
                                        .getDsRecPermissaoAgrupamento());
                    }
                } else {
                    if (!aviso) {
                        servicosDTO.setDsRecTipoModalidadeAviso("N");
                    }

                    if (!formulario) {
                        servicosDTO.setDsRecTipoModalidadeFormulario("N");
                    }
                }
            }
        }

        listaServicos = new ArrayList<ServicosDTO>();
        listaServicos.add(servicosDTO);
        
        listaServicosCompl = new ArrayList<ServicosDTO>();
        listaServicosCompl.add(servicosDTO);
        
        JRBeanCollectionDataSource beanParamServicos = new JRBeanCollectionDataSource(
                listaServicos);
        
        JRBeanCollectionDataSource beanParamServicosCompl = new JRBeanCollectionDataSource(
                listaServicosCompl);        
    
        
        // Par�metros do Relat�rio
        Map<String, Object> par = new HashMap<String, Object>();
        par.put("exibePgtoFornecedor", exibePgtoFornecedor);
        par.put("exibePgtoTributos", exibePgtoTributos);
        par.put("exibePgtoSalarios", exibePgtoSalarios);
        par.put("exibePgtoBeneficios", exibePgtoBeneficios);
        par.put("exibeCestaTarifas", exibeCestaTarifas);

        par.put("data", formatoData.format(new Date()));
        par.put("hora", formatoHora.format(new Date()));
        par.put("logoBradesco", logoPath);
        par.put("participantes", pathSubRelParticipantes);
        par.put("contas", pathSubRelContas);
        par.put("layouts", pathSubRelLayouts);
        par.put("cestaTarifas", pathSubRelCestasDeTarifas);
        par.put("servModal", pathSubServModalidades);
        par.put("pagamentosFornecedores", pathSubRelPagamentosFornecedores);
        par.put("pagamentosFornecedoresTitulos",
                pathSubRelPagamentosFornecedoresTitulos);
        par.put("pagamentosFornecedoresModularidades",
                pathSubRelPagamentosFornecedoresModularidades);
        par.put("pagamentosFornecedoresOutrasModularidades",
                pathSubRelPagamentosFornecedoresOutrasModularidades);
        par.put("pagamentosFornecedoresCompl",
                pathSubRelPagamentosFornecedoresCompl);
        par.put("pagamentosTributos", pathSubRelPagamentosTributos);
        par.put("tributos", pathSubRelTributos);
        par.put("pagamentosSalarios", pathSubRelPagamentosSalarios);
        par.put("pagamentosSalariosModularidades",
                pathSubRelPagamentosSalariosModularidades);
        par.put("pagamentosBeneficios", pathSubRelPagamentosBeneficios);
        par.put("pagamentosBeneficiosModularidades",
                pathSubRelPagamentosBeneficiosModularidades);
        par.put("servicos", pathSubRelServicos);
        par.put("servicosCompl", pathSubRelServicosCompl);
        par.put("listaParticipantes", beanParamParticipantes);
        par.put("listaContas", beanParamContas);
        par.put("listaLayouts", beanParamLayout);
        par.put("listaCestasTarifas", beanParamCestasTarifas);
        par.put("listaServModal", beanParamServModalidades);
        par.put("listaPagamentosFornecedores", beanParamPagamentosFornecedores);
        par.put("listaFornecTitulos", beanParamFornecTitulos);
        par.put("listaFornecModularidades", beanParamFornecModularidades);
        par.put("listaFornecOutrasModularidades",
                beanParamFornecOutrasModularidades);
        par.put("listaPagamentosFornecedoresCompl",
                beanParamPagamentosFornecedoresCompl);
        par.put("listaPagamentosTributos", beanParamPagamentosTributos);
        par.put("listaTributos", beanParamTributos);
        par.put("listaPagamentosSalarios", beanParamPagamentosSalarios);
        par.put("listaSalariosModularidades", beanParamSalariosModularidades);
        par.put("listaPagamentosBeneficios", beanParamPagamentosBeneficios);
        par.put("listaPagamentosBeneficiosModularidades",
                beanParamPagamentosBeneficiosModularidades);
        par.put("listaServicos", beanParamServicos);
        par.put("listaServicosCompl", beanParamServicosCompl);
        par.put("titulo", "ANEXO I - PAGAMENTO INTEGRADO BRADESCO");

        // getImprimirService().imprimirArquivo(FacesUtils.getServletResponse().getOutputStream(),
        // protocolo);

        // PgitFacesUtils.generateReportResponse("contrato");
        
        if (!flagMetodoImprimirAnexo) {
            try {
                
                // M�todo que gera o relat�rio PDF.
                PgitUtil
                .geraRelatorioPdf(
                        "/relatorios/manutencaoContrato/manterContrato/anexoContrato/",
                        "imprimirAnexoContrato", listaAnexoContrato, par);
                
            } /* Exce��o do relat�rio */
            catch (Exception e) {
                throw new BradescoViewException(e.getMessage(), e, "", "",
                        BradescoViewExceptionActionType.ACTION);
            }           
        }


    }

	/**
	 * Nome: imprimirRelatorioServicos.
	 *
	 * @return the string
	 * @see
	 */
	public String imprimirRelatorioServicos() {

		String caminho = "/images/Bradesco_logo.JPG";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext
		.getExternalContext().getContext();
		String logoPath = servletContext.getRealPath(caminho);

		// Par�metros do Relat�rio
		Map<String, Object> par = new HashMap<String, Object>();

		par.put("tipo", getTipo());
		par.put("empresa", getEmpresa());
		par.put("numero", getNumero());
		par.put("descContrato", getDescricaoContrato());
		par.put("logoBradesco", logoPath);

		par.put("titulo", "Pagamento Integrado - Servi�os");

		try {

			// M�todo que gera o relat�rio PDF.
			PgitUtil.geraRelatorioPdf(
					"/relatorios/manutencaoContrato/manterContrato/servicos",
					"imprimirServicos", manterContratoServicosBean
					.getListaGridPesquisa(), par);

		} /* Exce��o do relat�rio */
		catch (Exception e) {
			throw new BradescoViewException(e.getMessage(), e, "", "",
					BradescoViewExceptionActionType.ACTION);
		}

		return "";
	}

	/**
	 * Nome: imprimirRelatoriosTarifas.
	 *
	 * @return the string
	 * @see
	 */
	public String imprimirRelatoriosTarifas() {

		String caminho = "/images/Bradesco_logo.JPG";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext
		.getExternalContext().getContext();
		String logoPath = servletContext.getRealPath(caminho);

		// Par�metros do Relat�rio
		Map<String, Object> par = new HashMap<String, Object>();

		par.put("tipo", manterContratoTarifasBean.getTipoContrato());
		par.put("empresa", manterContratoTarifasBean.getEmpresaContrato());
		par.put("numero", manterContratoTarifasBean.getNumeroContrato());
		par.put("descContrato", manterContratoTarifasBean
				.getDescricaoContrato());
		par.put("logoBradesco", logoPath);

		par.put("titulo", "Pagamento Integrado - Tarifas");

		try {

			// M�todo que gera o relat�rio PDF.
			PgitUtil.geraRelatorioPdf(
					"/relatorios/manutencaoContrato/manterContrato/tarifas",
					"imprimirTarifas", manterContratoTarifasBean
					.getListaGridTarifasModalidade(), par);

		} /* Exce��o do relat�rio */
		catch (Exception e) {
			throw new BradescoViewException(e.getMessage(), e, "", "",
					BradescoViewExceptionActionType.ACTION);
		}

		return "";
	}
	
	private List<OcorrenciasSaidaDTO> carregarListaCestasTarifas(){
		
		ListarContratosPgitSaidaDTO contratosPgitSelecionado =identificacaoClienteContratoBean.getListaGridPesquisa().get(
                identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		ConfCestaTarifaEntradaDTO entrada = new ConfCestaTarifaEntradaDTO();
		
		entrada.setMaxOcorrencias(CD_MAX_OCORRENCIAS);
		entrada.setCdPessoasJuridicaContrato(contratosPgitSelecionado.getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(contratosPgitSelecionado.getCdTipoContrato());
		entrada.setNumSequenciaNegocio(contratosPgitSelecionado.getNrSequenciaContrato());
		
		saidaCestasTarifasDTO = new ConfCestaTarifaSaidaDTO();
		
		listaConfCestasTarifas = new ArrayList<OcorrenciasSaidaDTO>();
		
		saidaCestasTarifasDTO = confCestaTarifaServiceImpl.consultarConfCestaTarifa(entrada);
		
		if(saidaCestasTarifasDTO.getNumLinhas() != 0){
			setVisualizaCestaTarifas(true);
			setExibeCestaTarifas("S");			
			for(int i = 0; i < saidaCestasTarifasDTO.getListaOcorrenciasSaidaDTO().size(); i++){
				OcorrenciasSaidaDTO occurs = new OcorrenciasSaidaDTO();
				occurs.setCdCestaTariafaFlexbilizacao(saidaCestasTarifasDTO.getListaOcorrenciasSaidaDTO().get(i).getCdCestaTariafaFlexbilizacao());
				occurs.setDataVigenciaFim(saidaCestasTarifasDTO.getListaOcorrenciasSaidaDTO().get(i).getDataVigenciaFim());
				occurs.setDataVigenciaInicio(saidaCestasTarifasDTO.getListaOcorrenciasSaidaDTO().get(i).getDataVigenciaInicio());
				occurs.setDsCestaTariafaFlexbilizacao(saidaCestasTarifasDTO.getListaOcorrenciasSaidaDTO().get(i).getDsCestaTariafaFlexbilizacao());
				occurs.setPorcentagemTariafaFlexbilizacaoContrato(saidaCestasTarifasDTO.getListaOcorrenciasSaidaDTO().get(i).getPorcentagemTariafaFlexbilizacaoContrato());
				occurs.setVlTariafaFlexbilizacaoContrato(saidaCestasTarifasDTO.getListaOcorrenciasSaidaDTO().get(i).getVlTariafaFlexbilizacaoContrato());
				occurs.setVlTarifaPadrao(saidaCestasTarifasDTO.getListaOcorrenciasSaidaDTO().get(i).getVlTarifaPadrao());
				listaConfCestasTarifas.add(occurs);
			}
		}else{
			setVisualizaCestaTarifas(false);
			setExibeCestaTarifas("N");
		}
				
		return listaConfCestasTarifas;
	}

	/**
	 * Nome: voltar.
	 *
	 * @return the string
	 * @see
	 */
	public String voltar() {
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		setCodigoAgenciaGestora(null);
		bancoAgenciaDTO.setDsAgencia("");
		return "conManterContrato";
	}
	// Getters-Setters

	/**
	 * Nome: getDescricaoContrato.
	 *
	 * @return descricaoContrato
	 * @see
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Nome: setDescricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 * @see
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Nome: getCpfCnpj.
	 *
	 * @return cpfCnpj
	 * @see
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Nome: setCpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 * @see
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Nome: getNomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 * @see
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Nome: setNomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 * @see
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Nome: getDataHoraCadastramento.
	 *
	 * @return dataHoraCadastramento
	 * @see
	 */
	public String getDataHoraCadastramento() {
		return dataHoraCadastramento;
	}

	/**
	 * Nome: setDataHoraCadastramento.
	 *
	 * @param dataHoraCadastramento the data hora cadastramento
	 * @see
	 */
	public void setDataHoraCadastramento(String dataHoraCadastramento) {
		this.dataHoraCadastramento = dataHoraCadastramento;
	}

	/**
	 * Nome: getInicioVigencia.
	 *
	 * @return inicioVigencia
	 * @see
	 */
	public String getInicioVigencia() {
		return inicioVigencia;
	}

	/**
	 * Nome: setInicioVigencia.
	 *
	 * @param inicioVigencia the inicio vigencia
	 * @see
	 */
	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	/**
	 * Nome: getParticipacao.
	 *
	 * @return participacao
	 * @see
	 */
	public String getParticipacao() {
		return participacao;
	}

	/**
	 * Nome: setParticipacao.
	 *
	 * @param participacao the participacao
	 * @see
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	/**
	 * Nome: getPossuiAditivos.
	 *
	 * @return possuiAditivos
	 * @see
	 */
	public String getPossuiAditivos() {
		return possuiAditivos;
	}

	/**
	 * Nome: setPossuiAditivos.
	 *
	 * @param possuiAditivos the possui aditivos
	 * @see
	 */
	public void setPossuiAditivos(String possuiAditivos) {
		this.possuiAditivos = possuiAditivos;
	}

	/**
	 * Nome: getCdEmpresa.
	 *
	 * @return cdEmpresa
	 * @see
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Nome: setCdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 * @see
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Nome: getCdTipo.
	 *
	 * @return cdTipo
	 * @see
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}

	/**
	 * Nome: setCdTipo.
	 *
	 * @param cdTipo the cd tipo
	 * @see
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Nome: getEmpresa.
	 *
	 * @return empresa
	 * @see
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Nome: setEmpresa.
	 *
	 * @param empresa the empresa
	 * @see
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Nome: getMotivoDesc.
	 *
	 * @return motivoDesc
	 * @see
	 */
	public String getMotivoDesc() {
		return motivoDesc;
	}

	/**
	 * Nome: setMotivoDesc.
	 *
	 * @param motivoDesc the motivo desc
	 * @see
	 */
	public void setMotivoDesc(String motivoDesc) {
		this.motivoDesc = motivoDesc;
	}

	/**
	 * Nome: getNumero.
	 *
	 * @return numero
	 * @see
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Nome: setNumero.
	 *
	 * @param numero the numero
	 * @see
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Nome: getSituacaoDesc.
	 *
	 * @return situacaoDesc
	 * @see
	 */
	public String getSituacaoDesc() {
		return situacaoDesc;
	}

	/**
	 * Nome: setSituacaoDesc.
	 *
	 * @param situacaoDesc the situacao desc
	 * @see
	 */
	public void setSituacaoDesc(String situacaoDesc) {
		this.situacaoDesc = situacaoDesc;
	}

	/**
	 * Nome: getTipo.
	 *
	 * @return tipo
	 * @see
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Nome: setTipo.
	 *
	 * @param tipo the tipo
	 * @see
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Nome: getAtividadeEconomicaMaster.
	 *
	 * @return atividadeEconomicaMaster
	 * @see
	 */
	public String getAtividadeEconomicaMaster() {
		return atividadeEconomicaMaster;
	}

	/**
	 * Nome: setAtividadeEconomicaMaster.
	 *
	 * @param atividadeEconomicaMaster the atividade economica master
	 * @see
	 */
	public void setAtividadeEconomicaMaster(String atividadeEconomicaMaster) {
		this.atividadeEconomicaMaster = atividadeEconomicaMaster;
	}

	/**
	 * Nome: getCpfCnpjMaster.
	 *
	 * @return cpfCnpjMaster
	 * @see
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}

	/**
	 * Nome: setCpfCnpjMaster.
	 *
	 * @param cpfCnpjMaster the cpf cnpj master
	 * @see
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}

	/**
	 * Nome: getCdAgenciaGestora.
	 *
	 * @return cdAgenciaGestora
	 * @see
	 */
	public int getCdAgenciaGestora() {
		return cdAgenciaGestora;
	}

	/**
	 * Nome: setCdAgenciaGestora.
	 *
	 * @param cdAgenciaGestora the cd agencia gestora
	 * @see
	 */
	public void setCdAgenciaGestora(int cdAgenciaGestora) {
		this.cdAgenciaGestora = cdAgenciaGestora;
	}

	/**
	 * Nome: getDsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 * @see
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Nome: setDsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 * @see
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Nome: getGrupoEconomicoMaster.
	 *
	 * @return grupoEconomicoMaster
	 * @see
	 */
	public String getGrupoEconomicoMaster() {
		return grupoEconomicoMaster;
	}

	/**
	 * Nome: setGrupoEconomicoMaster.
	 *
	 * @param grupoEconomicoMaster the grupo economico master
	 * @see
	 */
	public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
		this.grupoEconomicoMaster = grupoEconomicoMaster;
	}

	/**
	 * Nome: getNomeRazaoSocialMaster.
	 *
	 * @return nomeRazaoSocialMaster
	 * @see
	 */
	public String getNomeRazaoSocialMaster() {
		return nomeRazaoSocialMaster;
	}

	/**
	 * Nome: setNomeRazaoSocialMaster.
	 *
	 * @param nomeRazaoSocialMaster the nome razao social master
	 * @see
	 */
	public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
		this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
	}

	/**
	 * Nome: getSegmentoMaster.
	 *
	 * @return segmentoMaster
	 * @see
	 */
	public String getSegmentoMaster() {
		return segmentoMaster;
	}

	/**
	 * Nome: setSegmentoMaster.
	 *
	 * @param segmentoMaster the segmento master
	 * @see
	 */
	public void setSegmentoMaster(String segmentoMaster) {
		this.segmentoMaster = segmentoMaster;
	}

	/**
	 * Nome: getSubSegmentoMaster.
	 *
	 * @return subSegmentoMaster
	 * @see
	 */
	public String getSubSegmentoMaster() {
		return subSegmentoMaster;
	}

	/**
	 * Nome: setSubSegmentoMaster.
	 *
	 * @param subSegmentoMaster the sub segmento master
	 * @see
	 */
	public void setSubSegmentoMaster(String subSegmentoMaster) {
		this.subSegmentoMaster = subSegmentoMaster;
	}

	/**
	 * Nome: getIdentificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 * @see
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Nome: setIdentificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 * @see
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Nome: getManterContratoContasBean.
	 *
	 * @return manterContratoContasBean
	 * @see
	 */
	public ContasBean getManterContratoContasBean() {
		return manterContratoContasBean;
	}

	/**
	 * Nome: setManterContratoContasBean.
	 *
	 * @param manterContratoContasBean the manter contrato contas bean
	 * @see
	 */
	public void setManterContratoContasBean(ContasBean manterContratoContasBean) {
		this.manterContratoContasBean = manterContratoContasBean;
	}

	/**
	 * Nome: getManterContratoDadosBasicosBean.
	 *
	 * @return manterContratoDadosBasicosBean
	 * @see
	 */
	public DadosBasicosBean getManterContratoDadosBasicosBean() {
		return manterContratoDadosBasicosBean;
	}

	/**
	 * Nome: setManterContratoDadosBasicosBean.
	 *
	 * @param manterContratoDadosBasicosBean the manter contrato dados basicos bean
	 * @see
	 */
	public void setManterContratoDadosBasicosBean(
			DadosBasicosBean manterContratoDadosBasicosBean) {
		this.manterContratoDadosBasicosBean = manterContratoDadosBasicosBean;
	}

	/**
	 * Nome: getManterContratoParticipantesBean.
	 *
	 * @return manterContratoParticipantesBean
	 * @see
	 */
	public ParticipantesBean getManterContratoParticipantesBean() {
		return manterContratoParticipantesBean;
	}

	/**
	 * Nome: setManterContratoParticipantesBean.
	 *
	 * @param manterContratoParticipantesBean the manter contrato participantes bean
	 * @see
	 */
	public void setManterContratoParticipantesBean(
			ParticipantesBean manterContratoParticipantesBean) {
		this.manterContratoParticipantesBean = manterContratoParticipantesBean;
	}

	/**
	 * Nome: getComboService.
	 *
	 * @return comboService
	 * @see
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Nome: setComboService.
	 *
	 * @param comboService the combo service
	 * @see
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Nome: getManterContratoImpl.
	 *
	 * @return manterContratoImpl
	 * @see
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Nome: setManterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 * @see
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Nome: getManterContratoLayoutsArquivosBean.
	 *
	 * @return manterContratoLayoutsArquivosBean
	 * @see
	 */
	public LayoutsArquivosBean getManterContratoLayoutsArquivosBean() {
		return manterContratoLayoutsArquivosBean;
	}

	/**
	 * Nome: setManterContratoLayoutsArquivosBean.
	 *
	 * @param manterContratoLayoutsArquivosBean the manter contrato layouts arquivos bean
	 * @see
	 */
	public void setManterContratoLayoutsArquivosBean(
			LayoutsArquivosBean manterContratoLayoutsArquivosBean) {
		this.manterContratoLayoutsArquivosBean = manterContratoLayoutsArquivosBean;
	}

	/**
	 * Nome: getDsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 * @see
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Nome: setDsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 * @see
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/*
	 * public IImprimircontratoService getImprimirContratoService() { return
	 * imprimirContratoService; }
	 * 
	 * public void setImprimirContratoService( IImprimircontratoService
	 * imprimirContratoService) { this.imprimirContratoService =
	 * imprimirContratoService; }
	 */

	/**
	 * Nome: getImprimirContratoService.
	 *
	 * @return imprimirContratoService
	 * @see
	 */
	public IImprimircontratoService getImprimirContratoService() {
		return imprimirContratoService;
	}

	/**
	 * Nome: setImprimirContratoService.
	 *
	 * @param imprimirContratoService the imprimir contrato service
	 * @see
	 */
	public void setImprimirContratoService(
			IImprimircontratoService imprimirContratoService) {
		this.imprimirContratoService = imprimirContratoService;
	}

	/**
	 * Nome: getContratoPgitSelecionado.
	 *
	 * @return contratoPgitSelecionado
	 * @see
	 */
	public ListarContratosPgitSaidaDTO getContratoPgitSelecionado() {
		return contratoPgitSelecionado;
	}

	/**
	 * Nome: setContratoPgitSelecionado.
	 *
	 * @param contratoPgitSelecionado the contrato pgit selecionado
	 * @see
	 */
	public void setContratoPgitSelecionado(
			ListarContratosPgitSaidaDTO contratoPgitSelecionado) {
		this.contratoPgitSelecionado = contratoPgitSelecionado;
	}

	/**
	 * Nome: getManterContratoRepresentanteBean.
	 *
	 * @return manterContratoRepresentanteBean
	 * @see
	 */
	public RepresentanteBean getManterContratoRepresentanteBean() {
		return manterContratoRepresentanteBean;
	}

	/**
	 * Nome: setManterContratoRepresentanteBean.
	 *
	 * @param manterContratoRepresentanteBean the manter contrato representante bean
	 * @see
	 */
	public void setManterContratoRepresentanteBean(
			RepresentanteBean manterContratoRepresentanteBean) {
		this.manterContratoRepresentanteBean = manterContratoRepresentanteBean;
	}

	/**
	 * Nome: isOpcaoChecarTodos.
	 *
	 * @return true, if is opcao checar todos
	 * @see
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Nome: setOpcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 * @see
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Nome: getImprimirService.
	 *
	 * @return imprimirService
	 * @see
	 */
	public IImprimirService getImprimirService() {
		return imprimirService;
	}

	/**
	 * Nome: setImprimirService.
	 *
	 * @param imprimirService the imprimir service
	 * @see
	 */
	public void setImprimirService(IImprimirService imprimirService) {
		this.imprimirService = imprimirService;
	}

	/**
	 * Nome: getImprimirAnexoPrimeiroContratoService.
	 *
	 * @return imprimirAnexoPrimeiroContratoService
	 * @see
	 */
	public IImprimirAnexoPrimeiroContratoService getImprimirAnexoPrimeiroContratoService() {
		return imprimirAnexoPrimeiroContratoService;
	}

	/**
	 * Nome: setImprimirAnexoPrimeiroContratoService.
	 *
	 * @param imprimirAnexoPrimeiroContratoService the imprimir anexo primeiro contrato service
	 * @see
	 */
	public void setImprimirAnexoPrimeiroContratoService(
			IImprimirAnexoPrimeiroContratoService imprimirAnexoPrimeiroContratoService) {
		this.imprimirAnexoPrimeiroContratoService = imprimirAnexoPrimeiroContratoService;
	}

	/**
	 * Nome: getImprimirAnexoSegundoContratoService.
	 *
	 * @return imprimirAnexoSegundoContratoService
	 * @see
	 */
	public IImprimirAnexoSegundoContratoService getImprimirAnexoSegundoContratoService() {
		return imprimirAnexoSegundoContratoService;
	}

	/**
	 * Nome: setImprimirAnexoSegundoContratoService.
	 *
	 * @param imprimirAnexoSegundoContratoService the imprimir anexo segundo contrato service
	 * @see
	 */
	public void setImprimirAnexoSegundoContratoService(
			IImprimirAnexoSegundoContratoService imprimirAnexoSegundoContratoService) {
		this.imprimirAnexoSegundoContratoService = imprimirAnexoSegundoContratoService;
	}

	/**
	 * Nome: getCdGerenteResponsavel.
	 *
	 * @return cdGerenteResponsavel
	 * @see
	 */
	public Long getCdGerenteResponsavel() {
		return cdGerenteResponsavel;
	}

	/**
	 * Nome: setCdGerenteResponsavel.
	 *
	 * @param cdGerenteResponsavel the cd gerente responsavel
	 * @see
	 */
	public void setCdGerenteResponsavel(Long cdGerenteResponsavel) {
		this.cdGerenteResponsavel = cdGerenteResponsavel;
	}

	/**
	 * Nome: getGerenteResponsavelFormatado.
	 *
	 * @return gerenteResponsavelFormatado
	 * @see
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}

	/**
	 * Nome: setGerenteResponsavelFormatado.
	 *
	 * @param gerenteResponsavelFormatado the gerente responsavel formatado
	 * @see
	 */
	public void setGerenteResponsavelFormatado(
			String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}

	/**
	 * Nome: getDsNomeRazaoSocialParticipante.
	 *
	 * @return dsNomeRazaoSocialParticipante
	 * @see
	 */
	public String getDsNomeRazaoSocialParticipante() {
		return dsNomeRazaoSocialParticipante;
	}

	/**
	 * Nome: setDsNomeRazaoSocialParticipante.
	 *
	 * @param dsNomeRazaoSocialParticipante the ds nome razao social participante
	 * @see
	 */
	public void setDsNomeRazaoSocialParticipante(
			String dsNomeRazaoSocialParticipante) {
		this.dsNomeRazaoSocialParticipante = dsNomeRazaoSocialParticipante;
	}

	/**
	 * Nome: getCpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 * @see
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Nome: setCpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 * @see
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Nome: getNumeroContrato.
	 *
	 * @return numeroContrato
	 * @see
	 */
	public Long getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Nome: setNumeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 * @see
	 */
	public void setNumeroContrato(Long numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Nome: getRenegociavel.
	 *
	 * @return renegociavel
	 * @see
	 */
	public String getRenegociavel() {
		return renegociavel;
	}

	/**
	 * Nome: setRenegociavel.
	 *
	 * @param renegociavel the renegociavel
	 * @see
	 */
	public void setRenegociavel(String renegociavel) {
		this.renegociavel = renegociavel;
	}

	/**
	 * Nome: getManterPerfilTrocaArqLayoutServiceImpl.
	 *
	 * @return manterPerfilTrocaArqLayoutServiceImpl
	 * @see
	 */
	public IManterPerfilTrocaArqLayoutService getManterPerfilTrocaArqLayoutServiceImpl() {
		return manterPerfilTrocaArqLayoutServiceImpl;
	}

	/**
	 * Nome: setManterPerfilTrocaArqLayoutServiceImpl.
	 *
	 * @param manterPerfilTrocaArqLayoutServiceImpl the manter perfil troca arq layout service impl
	 * @see
	 */
	public void setManterPerfilTrocaArqLayoutServiceImpl(
			IManterPerfilTrocaArqLayoutService manterPerfilTrocaArqLayoutServiceImpl) {
		this.manterPerfilTrocaArqLayoutServiceImpl = manterPerfilTrocaArqLayoutServiceImpl;
	}

	/**
	 * Nome: getSaidaMeioTransmissao.
	 *
	 * @return saidaMeioTransmissao
	 * @see
	 */
	public ListarContratosPgitSaidaDTO getSaidaMeioTransmissao() {
		return saidaMeioTransmissao;
	}

	/**
	 * Nome: setSaidaMeioTransmissao.
	 *
	 * @param saidaMeioTransmissao the saida meio transmissao
	 * @see
	 */
	public void setSaidaMeioTransmissao(
			ListarContratosPgitSaidaDTO saidaMeioTransmissao) {
		this.saidaMeioTransmissao = saidaMeioTransmissao;
	}

	/**
	 * Nome: getMeiosTransmissaoBean.
	 *
	 * @return meiosTransmissaoBean
	 * @see
	 */
	public MeiosTransmissaoBean getMeiosTransmissaoBean() {
		return meiosTransmissaoBean;
	}

	/**
	 * Nome: getMeiosTransmissaoBean.
	 *
	 * @param meiosTransmissaoBean the meios transmissao bean
	 * @see
	 */
	public void setMeiosTransmissaoBean(
			MeiosTransmissaoBean meiosTransmissaoBean) {
		this.meiosTransmissaoBean = meiosTransmissaoBean;
	}

	/**
	 * Nome: getDescAgenciaGestora.
	 *
	 * @return descAgenciaGestora
	 * @see
	 */
	public Integer getDescAgenciaGestora() {
		return descAgenciaGestora;
	}

	/**
	 * Nome: setDescAgenciaGestora.
	 *
	 * @param descAgenciaGestora the desc agencia gestora
	 * @see
	 */
	public void setDescAgenciaGestora(Integer descAgenciaGestora) {
		this.descAgenciaGestora = descAgenciaGestora;
	}

	/**
	 * Nome: getCodigoAgenciaGestora.
	 *
	 * @return codigoAgenciaGestora
	 * @see
	 */
	public Integer getCodigoAgenciaGestora() {
		return codigoAgenciaGestora;
	}

	/**
	 * Nome: setCodigoAgenciaGestora.
	 *
	 * @param codigoAgenciaGestora the codigo agencia gestora
	 * @see
	 */
	public void setCodigoAgenciaGestora(Integer codigoAgenciaGestora) {
		this.codigoAgenciaGestora = codigoAgenciaGestora;
	}

	/**
	 * Nome: getBancoAgenciaDTO.
	 *
	 * @return bancoAgenciaDTO
	 * @see
	 */
	public ConsultarBancoAgenciaSaidaDTO getBancoAgenciaDTO() {
		return bancoAgenciaDTO;
	}

	/**
	 * Nome: setBancoAgenciaDTO.
	 *
	 * @param bancoAgenciaDTO the banco agencia dto
	 * @see
	 */
	public void setBancoAgenciaDTO(ConsultarBancoAgenciaSaidaDTO bancoAgenciaDTO) {
		this.bancoAgenciaDTO = bancoAgenciaDTO;
	}

	/**
	 * Nome: getManterCadContaDestinoService.
	 *
	 * @return manterCadContaDestinoService
	 * @see
	 */
	public IManterCadContaDestinoService getManterCadContaDestinoService() {
		return manterCadContaDestinoService;
	}

	/**
	 * Nome: setManterCadContaDestinoService.
	 *
	 * @param manterCadContaDestinoService the manter cad conta destino service
	 * @see
	 */
	public void setManterCadContaDestinoService(
			IManterCadContaDestinoService manterCadContaDestinoService) {
		this.manterCadContaDestinoService = manterCadContaDestinoService;
	}

	/**
	 * Nome: getCodDescAgenciaGestora.
	 *
	 * @return codDescAgenciaGestora
	 * @see
	 */
	public String getCodDescAgenciaGestora() {
		return codDescAgenciaGestora;
	}

	/**
	 * Nome: setCodDescAgenciaGestora.
	 *
	 * @param codDescAgenciaGestora the cod desc agencia gestora
	 * @see
	 */
	public void setCodDescAgenciaGestora(String codDescAgenciaGestora) {
		this.codDescAgenciaGestora = codDescAgenciaGestora;
	}

	/**
	 * Nome: getManterContratoTarifasBean.
	 *
	 * @return manterContratoTarifasBean
	 * @see
	 */
	public TarifasBean getManterContratoTarifasBean() {
		return manterContratoTarifasBean;
	}

	/**
	 * Nome: setManterContratoTarifasBean.
	 *
	 * @param manterContratoTarifasBean the manter contrato tarifas bean
	 * @see
	 */
	public void setManterContratoTarifasBean(
			TarifasBean manterContratoTarifasBean) {
		this.manterContratoTarifasBean = manterContratoTarifasBean;
	}

	/**
	 * Nome: setManterContratoServicosBean.
	 *
	 * @param manterContratoServicosBean the manter contrato servicos bean
	 * @see
	 */
	public void setManterContratoServicosBean(
			ServicosBean manterContratoServicosBean) {
		this.manterContratoServicosBean = manterContratoServicosBean;
	}

	/**
	 * Nome: getManterContratoServicosBean.
	 *
	 * @return manterContratoServicosBean
	 * @see
	 */
	public ServicosBean getManterContratoServicosBean() {
		return manterContratoServicosBean;
	}

	/**
	 * Nome: getSaidaValidar.
	 *
	 * @return saidaValidar
	 */
	public ValidarPropostaAndamentoSaidaDTO getSaidaValidar() {
		return saidaValidar;
	}

	/**
	 * Nome: setSaidaValidar.
	 *
	 * @param saidaValidar the saida validar
	 */
	public void setSaidaValidar(ValidarPropostaAndamentoSaidaDTO saidaValidar) {
		this.saidaValidar = saidaValidar;
	}

	/**
	 * Nome: setUrlRenegociacaoNovo.
	 *
	 * @param urlRenegociacaoNovo the url renegociacao novo
	 */
	public void setUrlRenegociacaoNovo(String urlRenegociacaoNovo) {
		this.urlRenegociacaoNovo = urlRenegociacaoNovo;
	}

	/**
	 * Nome: getUrlRenegociacaoNovo.
	 *
	 * @return urlRenegociacaoNovo
	 */
	public String getUrlRenegociacaoNovo() {
		return urlRenegociacaoNovo;
	}

	/**
	 * Get: dsServicoSelecionado.
	 *
	 * @return the dsServicoSelecionado
	 */
	public List<ListarServicosSaidaDTO> getDsServicoSelecionado() {
		return dsServicoSelecionado;
	}

	/**
	 * Set: dsServicoSelecionado.
	 *
	 * @param dsServicoSelecionado the dsServicoSelecionado to set
	 */
	public void setDsServicoSelecionado(
			List<ListarServicosSaidaDTO> dsServicoSelecionado) {
		this.dsServicoSelecionado = dsServicoSelecionado;
	}

	/**
	 * Set: cdTipoArquivoRetorno.
	 *
	 * @param cdTipoArquivoRetorno the cdTipoArquivoRetorno to set
	 */
	public void setCdTipoArquivoRetorno(Integer cdTipoArquivoRetorno) {
		this.cdTipoArquivoRetorno = cdTipoArquivoRetorno;
	}

	/**
	 * Get: cdTipoArquivoRetorno.
	 *
	 * @return the cdTipoArquivoRetorno
	 */
	public Integer getCdTipoArquivoRetorno() {
		return cdTipoArquivoRetorno;
	}

	/**
	 * Get: saidaValidarUsuario.
	 *
	 * @return saidaValidarUsuario
	 */
	public ValidarUsuarioSaidaDTO getSaidaValidarUsuario() {
		return saidaValidarUsuario;
	}

	/**
	 * Set: saidaValidarUsuario.
	 *
	 * @param saidaValidarUsuario the saida validar usuario
	 */
	public void setSaidaValidarUsuario(ValidarUsuarioSaidaDTO saidaValidarUsuario) {
		this.saidaValidarUsuario = saidaValidarUsuario;
	}

	/**
	 * Get: entradaValidarUsuario.
	 *
	 * @return entradaValidarUsuario
	 */
	public ValidarUsuarioEntradaDTO getEntradaValidarUsuario() {
		return entradaValidarUsuario;
	}

	/**
	 * Set: entradaValidarUsuario.
	 *
	 * @param entradaValidarUsuario the entrada validar usuario
	 */
	public void setEntradaValidarUsuario(
			ValidarUsuarioEntradaDTO entradaValidarUsuario) {
		this.entradaValidarUsuario = entradaValidarUsuario;
	}

	/**
	 * Is restringir acesso botoes.
	 *
	 * @return true, if is restringir acesso botoes
	 */
	public boolean isRestringirAcessoBotoes() {
		return restringirAcessoBotoes;
	}

	/**
	 * Set: restringirAcessoBotoes.
	 *
	 * @param restringirAcessoBotoes the restringir acesso botoes
	 */
	public void setRestringirAcessoBotoes(boolean restringirAcessoBotoes) {
		this.restringirAcessoBotoes = restringirAcessoBotoes;
	}

	/**
	 * Get: saidaPrimeiroContrato.
	 *
	 * @return the saidaPrimeiroContrato
	 */
	public ImprimirAnexoPrimeiroContratoSaidaDTO getSaidaPrimeiroContrato() {
		return saidaPrimeiroContrato;
	}

	/**
	 * Set: saidaPrimeiroContrato.
	 *
	 * @param saidaPrimeiroContrato the saidaPrimeiroContrato to set
	 */
	public void setSaidaPrimeiroContrato(
			ImprimirAnexoPrimeiroContratoSaidaDTO saidaPrimeiroContrato) {
		this.saidaPrimeiroContrato = saidaPrimeiroContrato;
	}

	/**
	 * Get: saidaSegundoContrato.
	 *
	 * @return the saidaSegundoContrato
	 */
	public ImprimirAnexoSegundoContratoSaidaDTO getSaidaSegundoContrato() {
		return saidaSegundoContrato;
	}

	/**
	 * Set: saidaSegundoContrato.
	 *
	 * @param saidaSegundoContrato the saidaSegundoContrato to set
	 */
	public void setSaidaSegundoContrato(
			ImprimirAnexoSegundoContratoSaidaDTO saidaSegundoContrato) {
		this.saidaSegundoContrato = saidaSegundoContrato;
	}

	/**
	 * Get: saidaListaPerfilTrocaArquivo.
	 *
	 * @return the saidaListaPerfilTrocaArquivo
	 */
	public ConsultarListaPerfilTrocaArquivoSaidaDTO getSaidaListaPerfilTrocaArquivo() {
		return saidaListaPerfilTrocaArquivo;
	}

	/**
	 * Set: saidaListaPerfilTrocaArquivo.
	 *
	 * @param saidaListaPerfilTrocaArquivo the saidaListaPerfilTrocaArquivo to set
	 */
	public void setSaidaListaPerfilTrocaArquivo(
			ConsultarListaPerfilTrocaArquivoSaidaDTO saidaListaPerfilTrocaArquivo) {
		this.saidaListaPerfilTrocaArquivo = saidaListaPerfilTrocaArquivo;
	}
	
	/**
	 * Get: dsServicoSegundaTela.
	 *
	 * @return the dsServicoSegundaTela
	 */
	public String getDsServicoSegundaTela() {
		return dsServicoSegundaTela;
	}

	/**
	 * Set: dsServicoSegundaTela.
	 *
	 * @param dsServicoSegundaTela the dsServicoSegundaTela to set
	 */
	public void setDsServicoSegundaTela(String dsServicoSegundaTela) {
		this.dsServicoSegundaTela = dsServicoSegundaTela;
	}

	/**
	 * Get: listaGridTarifasModalidade.
	 *
	 * @return listaGridTarifasModalidade
	 */
	public List<ConsultarTarifaContratoSaidaDTO> getListaGridTarifasModalidade() {
		return listaGridTarifasModalidade;
	}

	/**
	 * Set: listaGridTarifasModalidade.
	 *
	 * @param listaGridTarifasModalidade the lista grid tarifas modalidade
	 */
	public void setListaGridTarifasModalidade(
			List<ConsultarTarifaContratoSaidaDTO> listaGridTarifasModalidade) {
		this.listaGridTarifasModalidade = listaGridTarifasModalidade;
	}

	/**
	 * Is flag metodo imprimir anexo.
	 *
	 * @return true, if is flag metodo imprimir anexo
	 */
	public boolean isFlagMetodoImprimirAnexo() {
		return flagMetodoImprimirAnexo;
	}

	/**
	 * Set: flagMetodoImprimirAnexo.
	 *
	 * @param flagMetodoImprimirAnexo the flag metodo imprimir anexo
	 */
	public void setFlagMetodoImprimirAnexo(boolean flagMetodoImprimirAnexo) {
		this.flagMetodoImprimirAnexo = flagMetodoImprimirAnexo;
	}

	/**
	 * Get: listaTipoArquivoRetorno.
	 *
	 * @return listaTipoArquivoRetorno
	 */
	public List<SelectItem> getListaTipoArquivoRetorno() {
		return listaTipoArquivoRetorno;
	}

	/**
	 * Set: listaTipoArquivoRetorno.
	 *
	 * @param listaTipoArquivoRetorno the lista tipo arquivo retorno
	 */
	public void setListaTipoArquivoRetorno(List<SelectItem> listaTipoArquivoRetorno) {
		this.listaTipoArquivoRetorno = listaTipoArquivoRetorno;
	}

	/**
	 * Get: listaTipoArquivoRetornoHash.
	 *
	 * @return listaTipoArquivoRetornoHash
	 */
	public Map<Integer, String> getListaTipoArquivoRetornoHash() {
		return listaTipoArquivoRetornoHash;
	}

	/**
	 * Set lista tipo arquivo retorno hash.
	 *
	 * @param listaTipoArquivoRetornoHash the lista tipo arquivo retorno hash
	 */
	public void setListaTipoArquivoRetornoHash(
			Map<Integer, String> listaTipoArquivoRetornoHash) {
		this.listaTipoArquivoRetornoHash = listaTipoArquivoRetornoHash;
	}

	/**
	 * Get: listaServicoModalidadeTelaResumo.
	 *
	 * @return listaServicoModalidadeTelaResumo
	 */
	public List<PagamentosDTO> getListaServicoModalidadeTelaResumo() {
		return listaServicoModalidadeTelaResumo;
	}

	/**
	 * Set: listaServicoModalidadeTelaResumo.
	 *
	 * @param listaServicoModalidadeTelaResumo the lista servico modalidade tela resumo
	 */
	public void setListaServicoModalidadeTelaResumo(
			List<PagamentosDTO> listaServicoModalidadeTelaResumo) {
		this.listaServicoModalidadeTelaResumo = listaServicoModalidadeTelaResumo;
	}

	/**
	 * Get: listaPagamentosFornec.
	 *
	 * @return listaPagamentosFornec
	 */
	public List<PagamentosDTO> getListaPagamentosFornec() {
		return listaPagamentosFornec;
	}

	/**
	 * Set: listaPagamentosFornec.
	 *
	 * @param listaPagamentosFornec the lista pagamentos fornec
	 */
	public void setListaPagamentosFornec(List<PagamentosDTO> listaPagamentosFornec) {
		this.listaPagamentosFornec = listaPagamentosFornec;
	}

	/**
	 * Get: listaPagamentosBeneficios.
	 *
	 * @return listaPagamentosBeneficios
	 */
	public List<PagamentosDTO> getListaPagamentosBeneficios() {
		return listaPagamentosBeneficios;
	}

	/**
	 * Set: listaPagamentosBeneficios.
	 *
	 * @param listaPagamentosBeneficios the lista pagamentos beneficios
	 */
	public void setListaPagamentosBeneficios(
			List<PagamentosDTO> listaPagamentosBeneficios) {
		this.listaPagamentosBeneficios = listaPagamentosBeneficios;
	}

	/**
	 * Get: listaBeneficiosModularidades.
	 *
	 * @return listaBeneficiosModularidades
	 */
	public List<PagamentosDTO> getListaBeneficiosModularidades() {
		return listaBeneficiosModularidades;
	}

	/**
	 * Set: listaBeneficiosModularidades.
	 *
	 * @param listaBeneficiosModularidades the lista beneficios modularidades
	 */
	public void setListaBeneficiosModularidades(
			List<PagamentosDTO> listaBeneficiosModularidades) {
		this.listaBeneficiosModularidades = listaBeneficiosModularidades;
	}

	/**
	 * Get: listaServicos.
	 *
	 * @return listaServicos
	 */
	public List<ServicosDTO> getListaServicos() {
		return listaServicos;
	}

	/**
	 * Set: listaServicos.
	 *
	 * @param listaServicos the lista servicos
	 */
	public void setListaServicos(List<ServicosDTO> listaServicos) {
		this.listaServicos = listaServicos;
	}

	/**
	 * Get: listaPagamentosSalarios.
	 *
	 * @return listaPagamentosSalarios
	 */
	public List<PagamentosDTO> getListaPagamentosSalarios() {
		return listaPagamentosSalarios;
	}

	/**
	 * Set: listaPagamentosSalarios.
	 *
	 * @param listaPagamentosSalarios the lista pagamentos salarios
	 */
	public void setListaPagamentosSalarios(
			List<PagamentosDTO> listaPagamentosSalarios) {
		this.listaPagamentosSalarios = listaPagamentosSalarios;
	}

	/**
	 * Get: listaPagamentosTri.
	 *
	 * @return listaPagamentosTri
	 */
	public List<PagamentosDTO> getListaPagamentosTri() {
		return listaPagamentosTri;
	}

	/**
	 * Set: listaPagamentosTri.
	 *
	 * @param listaPagamentosTri the lista pagamentos tri
	 */
	public void setListaPagamentosTri(List<PagamentosDTO> listaPagamentosTri) {
		this.listaPagamentosTri = listaPagamentosTri;
	}

	/**
	 * Get: listaTitulos.
	 *
	 * @return listaTitulos
	 */
	public List<PagamentosDTO> getListaTitulos() {
		return listaTitulos;
	}

	/**
	 * Set: listaTitulos.
	 *
	 * @param listaTitulos the lista titulos
	 */
	public void setListaTitulos(List<PagamentosDTO> listaTitulos) {
		this.listaTitulos = listaTitulos;
	}

	/**
	 * Get: listaFornModularidades.
	 *
	 * @return listaFornModularidades
	 */
	public List<PagamentosDTO> getListaFornModularidades() {
		return listaFornModularidades;
	}

	/**
	 * Set: listaFornModularidades.
	 *
	 * @param listaFornModularidades the lista forn modularidades
	 */
	public void setListaFornModularidades(List<PagamentosDTO> listaFornModularidades) {
		this.listaFornModularidades = listaFornModularidades;
	}

	/**
	 * Get: listaTri.
	 *
	 * @return listaTri
	 */
	public List<PagamentosDTO> getListaTri() {
		return listaTri;
	}

	/**
	 * Set: listaTri.
	 *
	 * @param listaTri the lista tri
	 */
	public void setListaTri(List<PagamentosDTO> listaTri) {
		this.listaTri = listaTri;
	}

	/**
	 * Get: listaServicosCompl.
	 *
	 * @return listaServicosCompl
	 */
	public List<ServicosDTO> getListaServicosCompl() {
		return listaServicosCompl;
	}

	/**
	 * Set: listaServicosCompl.
	 *
	 * @param listaServicosCompl the lista servicos compl
	 */
	public void setListaServicosCompl(List<ServicosDTO> listaServicosCompl) {
		this.listaServicosCompl = listaServicosCompl;
	}

	/**
	 * Get: listaServicosModalidades.
	 *
	 * @return listaServicosModalidades
	 */
	public List<ServicosModalidadesDTO> getListaServicosModalidades() {
		return listaServicosModalidades;
	}

	/**
	 * Set: listaServicosModalidades.
	 *
	 * @param listaServicosModalidades the lista servicos modalidades
	 */
	public void setListaServicosModalidades(
			List<ServicosModalidadesDTO> listaServicosModalidades) {
		this.listaServicosModalidades = listaServicosModalidades;
	}

	/**
	 * Get: listaFornecedorOutrasModularidades.
	 *
	 * @return listaFornecedorOutrasModularidades
	 */
	public List<PagamentosDTO> getListaFornecedorOutrasModularidades() {
		return listaFornecedorOutrasModularidades;
	}

	/**
	 * Set: listaFornecedorOutrasModularidades.
	 *
	 * @param listaFornecedorOutrasModularidades the lista fornecedor outras modularidades
	 */
	public void setListaFornecedorOutrasModularidades(
			List<PagamentosDTO> listaFornecedorOutrasModularidades) {
		this.listaFornecedorOutrasModularidades = listaFornecedorOutrasModularidades;
	}

	/**
	 * Get: listaAnexoContrato.
	 *
	 * @return listaAnexoContrato
	 */
	public List<AnexoContratoDTO> getListaAnexoContrato() {
		return listaAnexoContrato;
	}

	/**
	 * Set: listaAnexoContrato.
	 *
	 * @param listaAnexoContrato the lista anexo contrato
	 */
	public void setListaAnexoContrato(List<AnexoContratoDTO> listaAnexoContrato) {
		this.listaAnexoContrato = listaAnexoContrato;
	}

	/**
	 * Get: listaSalariosMod.
	 *
	 * @return listaSalariosMod
	 */
	public List<PagamentosDTO> getListaSalariosMod() {
		return listaSalariosMod;
	}

	/**
	 * Set: listaSalariosMod.
	 *
	 * @param listaSalariosMod the lista salarios mod
	 */
	public void setListaSalariosMod(List<PagamentosDTO> listaSalariosMod) {
		this.listaSalariosMod = listaSalariosMod;
	}

	/**
	 * Get: listaParticipantes.
	 *
	 * @return listaParticipantes
	 */
	public List<OcorrenciasParticipantes> getListaParticipantes() {
		return listaParticipantes;
	}

	/**
	 * Set: listaParticipantes.
	 *
	 * @param listaParticipantes the lista participantes
	 */
	public void setListaParticipantes(
			List<OcorrenciasParticipantes> listaParticipantes) {
		this.listaParticipantes = listaParticipantes;
	}

	/**
	 * Get: listaContas.
	 *
	 * @return listaContas
	 */
	public List<OcorrenciasContas> getListaContas() {
		return listaContas;
	}

	/**
	 * Set: listaContas.
	 *
	 * @param listaContas the lista contas
	 */
	public void setListaContas(List<OcorrenciasContas> listaContas) {
		this.listaContas = listaContas;
	}

	/**
	 * Get: listaLayouts.
	 *
	 * @return listaLayouts
	 */
	public List<OcorrenciasLayout> getListaLayouts() {
		return listaLayouts;
	}

	/**
	 * Set: listaLayouts.
	 *
	 * @param listaLayouts the lista layouts
	 */
	public void setListaLayouts(List<OcorrenciasLayout> listaLayouts) {
		this.listaLayouts = listaLayouts;
	}

	/**
	 * Get: exibePgtoFornecedor.
	 *
	 * @return exibePgtoFornecedor
	 */
	public String getExibePgtoFornecedor() {
		return exibePgtoFornecedor;
	}

	/**
	 * Set: exibePgtoFornecedor.
	 *
	 * @param exibePgtoFornecedor the exibe pgto fornecedor
	 */
	public void setExibePgtoFornecedor(String exibePgtoFornecedor) {
		this.exibePgtoFornecedor = exibePgtoFornecedor;
	}

	/**
	 * Get: exibePgtoTributos.
	 *
	 * @return exibePgtoTributos
	 */
	public String getExibePgtoTributos() {
		return exibePgtoTributos;
	}

	/**
	 * Set: exibePgtoTributos.
	 *
	 * @param exibePgtoTributos the exibe pgto tributos
	 */
	public void setExibePgtoTributos(String exibePgtoTributos) {
		this.exibePgtoTributos = exibePgtoTributos;
	}

	/**
	 * Get: exibePgtoSalarios.
	 *
	 * @return exibePgtoSalarios
	 */
	public String getExibePgtoSalarios() {
		return exibePgtoSalarios;
	}

	/**
	 * Set: exibePgtoSalarios.
	 *
	 * @param exibePgtoSalarios the exibe pgto salarios
	 */
	public void setExibePgtoSalarios(String exibePgtoSalarios) {
		this.exibePgtoSalarios = exibePgtoSalarios;
	}

	/**
	 * Get: exibePgtoBeneficios.
	 *
	 * @return exibePgtoBeneficios
	 */
	public String getExibePgtoBeneficios() {
		return exibePgtoBeneficios;
	}

	/**
	 * Set: exibePgtoBeneficios.
	 *
	 * @param exibePgtoBeneficios the exibe pgto beneficios
	 */
	public void setExibePgtoBeneficios(String exibePgtoBeneficios) {
		this.exibePgtoBeneficios = exibePgtoBeneficios;
	}

	/**
	 * Get: imprimirGridTarifa.
	 *
	 * @return imprimirGridTarifa
	 */
	public String getImprimirGridTarifa() {
		return imprimirGridTarifa;
	}

	/**
	 * Set: imprimirGridTarifa.
	 *
	 * @param imprimirGridTarifa the imprimir grid tarifa
	 */
	public void setImprimirGridTarifa(String imprimirGridTarifa) {
		this.imprimirGridTarifa = imprimirGridTarifa;
	}

	/**
	 * Get: saidaPrimeiroContratoLoop.
	 *
	 * @return saidaPrimeiroContratoLoop
	 */
	public ImprimirAnexoPrimeiroContratoSaidaDTO getSaidaPrimeiroContratoLoop() {
		return saidaPrimeiroContratoLoop;
	}

	/**
	 * Set: saidaPrimeiroContratoLoop.
	 *
	 * @param saidaPrimeiroContratoLoop the saida primeiro contrato loop
	 */
	public void setSaidaPrimeiroContratoLoop(
			ImprimirAnexoPrimeiroContratoSaidaDTO saidaPrimeiroContratoLoop) {
		this.saidaPrimeiroContratoLoop = saidaPrimeiroContratoLoop;
	}

	/**
	 * Get: nomeTabelaAtual.
	 *
	 * @return nomeTabelaAtual
	 */
	public String getNomeTabelaAtual() {
		return nomeTabelaAtual;
	}

	/**
	 * Set: nomeTabelaAtual.
	 *
	 * @param nomeTabelaAtual the nome tabela atual
	 */
	public void setNomeTabelaAtual(String nomeTabelaAtual) {
		this.nomeTabelaAtual = nomeTabelaAtual;
	}

	/**
	 * Is omite servicos resumo contrato.
	 *
	 * @return true, if is omite servicos resumo contrato
	 */
	public boolean isOmiteServicosResumoContrato() {
		return omiteServicosResumoContrato;
	}

	/**
	 * Set: omiteServicosResumoContrato.
	 *
	 * @param omiteServicosResumoContrato the omite servicos resumo contrato
	 */
	public void setOmiteServicosResumoContrato(boolean omiteServicosResumoContrato) {
		this.omiteServicosResumoContrato = omiteServicosResumoContrato;
	}

    /**
     * Nome: setContratoServiceImpl.
     *
     * @param contratoServiceImpl the contrato service impl
     */
    public void setContratoServiceImpl(IContratoService contratoServiceImpl) {
        this.contratoServiceImpl = contratoServiceImpl;
    }

    /**
     * Nome: getContratoServiceImpl.
     *
     * @return contratoServiceImpl
     */
    public IContratoService getContratoServiceImpl() {
        return contratoServiceImpl;
    }

	public ConfCestasTarifasBean getConfCestasTarifasBean() {
		return confCestasTarifasBean;
	}

	public void setConfCestasTarifasBean(ConfCestasTarifasBean confCestasTarifasBean) {
		this.confCestasTarifasBean = confCestasTarifasBean;
	}

	public List<OcorrenciasSaidaDTO> getListaConfCestasTarifas() {
		return listaConfCestasTarifas;
	}

	public void setListaConfCestasTarifas(
			List<OcorrenciasSaidaDTO> listaConfCestasTarifas) {
		this.listaConfCestasTarifas = listaConfCestasTarifas;
	}

	public ConfCestaTarifaEntradaDTO getEntradaCestaTarifa() {
		return entradaCestaTarifa;
	}

	public void setEntradaCestaTarifa(ConfCestaTarifaEntradaDTO entradaCestaTarifa) {
		this.entradaCestaTarifa = entradaCestaTarifa;
	}

	public IConfCestaTarifaService getConfCestaTarifaServiceImpl() {
		return confCestaTarifaServiceImpl;
	}

	public void setConfCestaTarifaServiceImpl(
			IConfCestaTarifaService confCestaTarifaServiceImpl) {
		this.confCestaTarifaServiceImpl = confCestaTarifaServiceImpl;
	}

	public boolean isVisualizaCestaTarifas() {
		return visualizaCestaTarifas;
	}

	public void setVisualizaCestaTarifas(boolean visualizaCestaTarifas) {
		this.visualizaCestaTarifas = visualizaCestaTarifas;
	}

	public String getExibeCestaTarifas() {
		return exibeCestaTarifas;
	}

	public void setExibeCestaTarifas(String exibeCestaTarifas) {
		this.exibeCestaTarifas = exibeCestaTarifas;
	}

		
}	
