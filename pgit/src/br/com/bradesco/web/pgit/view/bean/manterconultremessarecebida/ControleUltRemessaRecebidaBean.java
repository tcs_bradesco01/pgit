/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manterconultremessarecebida
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manterconultremessarecebida;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarAssLoteLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarAssLoteLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.IManterConUltRemessaRecebidaService;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.AlterarUltimoNumeroRemessaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.AlterarUltimoNumeroRemessaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.DetalharUltimoNumeroRemessaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.DetalharUltimoNumeroRemessaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.ListarUltimoNumeroRemessaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconultremessarecebida.bean.ListarUltimoNumeroRemessaSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;

/**
 * Nome: ControleUltRemessaRecebidaBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ControleUltRemessaRecebidaBean {

	/** Atributo consultasService. */
	private IConsultasService consultasService;

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;

	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo controleUltRemessaRecebidaImpl. */
	private IManterConUltRemessaRecebidaService controleUltRemessaRecebidaImpl;

	// Cont�m os campos da remessa na tela de altera��o/confirma��o
	/** Atributo dadosRemessa. */
	private DetalharUltimoNumeroRemessaSaidaDTO dadosRemessa;

	// Combos
	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();

	/** Atributo listaTipoLote. */
	private List<SelectItem> listaTipoLote = new ArrayList<SelectItem>();

	/** Atributo layoutFiltro. */
	private Integer layoutFiltro;

	/** Atributo tipoLoteFiltro. */
	private Integer tipoLoteFiltro;

	// Grid Consultar
	/** Atributo listaGrid. */
	private List<ListarUltimoNumeroRemessaSaidaDTO> listaGrid;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;

	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;

	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;

	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;

	/** Atributo dsEmpresa. */
	private String dsEmpresa;

	/** Atributo nroContrato. */
	private String nroContrato;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo cdPerfilTrocaArquivoPerfil. */
	private Long cdPerfilTrocaArquivoPerfil;

	/** Atributo dsSituacaoPerfil. */
	private String dsSituacaoPerfil;

	private Long cdClub;

	/**
	 * Iniciar tela.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		limpar();
		// Tela de Filtro
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean
				.setPaginaRetorno("conUltimoNumeroRemessaRecebida");
		filtroAgendamentoEfetivacaoEstornoBean
				.setClienteContratoSelecionado(false);
		filtroAgendamentoEfetivacaoEstornoBean
				.setEmpresaGestoraFiltro(2269651L);

		listarTipoLayout();

		setDisableArgumentosConsulta(false);

	}

	/**
	 * Limpar cliente.
	 */
	public void limparCliente() {
		filtroAgendamentoEfetivacaoEstornoBean.limparCamposCliente();
		setListaGrid(null);
		setLayoutFiltro(null);
		setTipoLoteFiltro(null);
		setItemSelecionadoLista(null);
		setListaTipoLote(new ArrayList<SelectItem>());
	}

	/**
	 * Limpar.
	 * 
	 * @return the string
	 */
	public String limpar() {
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		dadosRemessa = new DetalharUltimoNumeroRemessaSaidaDTO();
		setListaGrid(null);
		setLayoutFiltro(null);
		setTipoLoteFiltro(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		setListaTipoLote(new ArrayList<SelectItem>());

		return "";
	}

	/**
	 * Limpar campos.
	 * 
	 * @return the string
	 */
	public String limparCampos() {
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);
		dadosRemessa = new DetalharUltimoNumeroRemessaSaidaDTO();
		setListaGrid(null);
		setLayoutFiltro(null);
		setTipoLoteFiltro(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		setListaTipoLote(new ArrayList<SelectItem>());

		return "";
	}

	/**
	 * Limpar grid.
	 * 
	 * @return the string
	 */
	public String limparGrid() {
		setListaGrid(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 * 
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		setListaGrid(null);
		setItemSelecionadoLista(null);
		setLayoutFiltro(null);
		setTipoLoteFiltro(null);
		dadosRemessa = new DetalharUltimoNumeroRemessaSaidaDTO();
		setListaTipoLote(new ArrayList<SelectItem>());
		return "";

	}

	/**
	 * Voltar consultar.
	 * 
	 * @return the string
	 */
	public String voltarConsultar() {
		setItemSelecionadoLista(null);

		return "conUltimoNumeroRemessaRecebida";
	}

	/**
	 * Avancar confirmar.
	 * 
	 * @return the string
	 */
	public String avancarConfirmar() {
		return "confUltimoNumeroRemessaRecebida";
	}

	/**
	 * Voltar alterar.
	 * 
	 * @return the string
	 */
	public String voltarAlterar() {
		return "altUltimoNumeroRemessaRecebida";
	}

	/**
	 * Confirmar.
	 * 
	 * @return the string
	 */
	public String confirmar() {
		try {
			AlterarUltimoNumeroRemessaEntradaDTO entrada = new AlterarUltimoNumeroRemessaEntradaDTO();
			entrada.setCdTipoLayoutArquivo(getDadosRemessa()
					.getCdTipoLayoutArquivo());
			entrada.setCdPerfilTrocaArquivo(getCdPerfilTrocaArquivoPerfil());
			entrada
					.setCdTipoLoteLayout(getDadosRemessa()
							.getCdTipoLoteLayout());
			entrada.setNrUltimoArquivo(getDadosRemessa()
					.getNrUltimoNumeroArquivoRemessa());
			entrada.setNrMaximoArquivo(getDadosRemessa()
					.getNrMaximoArquivoRemessa());
			entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridicaContrato());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada
					.setNrSequenciaContratoNegocio(getNrSequenciaContratoNegocio());
			entrada.setCdClub(getCdClub());

			AlterarUltimoNumeroRemessaSaidaDTO saida = getControleUltRemessaRecebidaImpl()
					.alterarUltimoNumeroRemessa(entrada);
			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(),
					"conUltimoNumeroRemessaRecebida",
					BradescoViewExceptionActionType.ACTION, false);
			consultar();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}

		return "";
	}

	/**
	 * Carregar cabecalho.
	 */
	public void carregarCabecalho() {
		limparVariaveis();
		setCdClub(getListaGrid().get(getItemSelecionadoLista()).getCdClub());

		setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getCdPessoaJuridicaContrato());
		setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getCdTipoContratoNegocio());
		setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getNrSequenciaContratoNegocio());

		if (filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado()
				.equals("0")) {
			setNrCnpjCpf(filtroAgendamentoEfetivacaoEstornoBean
					.getNrCpfCnpjCliente());
			setDsRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoRazaoSocialCliente());

			setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
					.getEmpresaGestoraDescCliente());
			setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getNumeroDescCliente());
			setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoContratoDescCliente());
			setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getSituacaoDescCliente());

			setCdPerfilTrocaArquivoPerfil(filtroAgendamentoEfetivacaoEstornoBean
					.getCdPerfilTrocaArquivoPerfilCliente());
			setDsSituacaoPerfil(filtroAgendamentoEfetivacaoEstornoBean
					.getDsSituacaoPerfilCliente());

		} else {

			try {
				DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
				entradaDTO
						.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
								.getEmpresaGestoraFiltro());
				entradaDTO
						.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
								.getCdTipoContratoNegocio());
				entradaDTO
						.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
								.getNrSequenciaContratoNegocio());
				DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService()
						.detalharDadosContrato(entradaDTO);
				setNrCnpjCpf(saidaDTO.getCdCnpjCpfTitular());
				setDsRazaoSocial(saidaDTO.getDsParticipanteTitular());
			} catch (PdcAdapterFunctionalException p) {
				setNrCnpjCpf("");
				setDsRazaoSocial("");
			}
			setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
					.getEmpresaGestoraDescContrato());
			setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getNumeroDescContrato());
			setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoContratoDescContrato());
			setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getSituacaoDescContrato());

			setCdPerfilTrocaArquivoPerfil(filtroAgendamentoEfetivacaoEstornoBean
					.getCdPerfilTrocaArquivoPerfilContrato());
			setDsSituacaoPerfil(filtroAgendamentoEfetivacaoEstornoBean
					.getDsSituacaoPerfilContrato());
		}

	}

	/**
	 * Limpar variaveis.
	 */
	public void limparVariaveis() {
		setCdPessoaJuridicaContrato(null);
		setCdTipoContratoNegocio(null);
		setNrSequenciaContratoNegocio(null);
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");
		setCdPerfilTrocaArquivoPerfil(null);
		setDsSituacaoPerfil("");
		setCdClub(null);
	}

	/**
	 * Alterar numero.
	 * 
	 * @return the string
	 */
	public String alterarNumero() {
		try {
			DetalharUltimoNumeroRemessaEntradaDTO entrada = new DetalharUltimoNumeroRemessaEntradaDTO();
			entrada.setCdPerfilTrocaArquivo(getListaGrid().get(
					getItemSelecionadoLista()).getCdPerfilTrocaArquivo());
			entrada.setCdTipoLayoutArquivo(getListaGrid().get(
					getItemSelecionadoLista()).getCdTipoLayoutArquivo());
			entrada.setCdTipoLoteLayout(getListaGrid().get(
					getItemSelecionadoLista()).getCdTipoLoteLayout());
			entrada.setCdClub(getListaGrid().get(getItemSelecionadoLista())
					.getCdClub());

			dadosRemessa = getControleUltRemessaRecebidaImpl()
					.detalharUltimoNumeroRemessa(entrada);

			carregarCabecalho();

			return "altUltimoNumeroRemessaRecebida";

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			dadosRemessa = new DetalharUltimoNumeroRemessaSaidaDTO();
		}
		return "";
	}

	/**
	 * Limpar evento.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void limparEvento(ActionEvent evt) {
		setListaGrid(null);
		setLayoutFiltro(null);
		setListaTipoLote(new ArrayList<SelectItem>());
		setTipoLoteFiltro(null);
		setItemSelecionadoLista(null);
	}

	/**
	 * Listar tipo layout.
	 */
	public void listarTipoLayout() {
		try {
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
			List<TipoLayoutArquivoSaidaDTO> listaTipoLayout = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			TipoLayoutArquivoEntradaDTO tipoLoteEntradaDTO = new TipoLayoutArquivoEntradaDTO();
			tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);

			listaTipoLayout = getComboService().listarTipoLayoutArquivoCem(
					tipoLoteEntradaDTO);

			for (TipoLayoutArquivoSaidaDTO combo : listaTipoLayout) {
				listaTipoLayoutArquivo.add(new SelectItem(combo
						.getCdTipoLayoutArquivo(), combo
						.getDsTipoLayoutArquivo()));
			}
		} catch (PdcAdapterFunctionalException e) {
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar tipo lote.
	 */
	public void listarTipoLote() {
		try {
			listaTipoLote = new ArrayList<SelectItem>();
			if (getLayoutFiltro() != null && getLayoutFiltro() != 0) {
				List<ListarAssLoteLayoutArquivoSaidaDTO> listaTipoLoteAux = new ArrayList<ListarAssLoteLayoutArquivoSaidaDTO>();
				ListarAssLoteLayoutArquivoEntradaDTO entrada = new ListarAssLoteLayoutArquivoEntradaDTO();
				entrada.setCdTipoLayoutArquivo(getLayoutFiltro());
				entrada.setCdTipoLoteLayout(0);

				listaTipoLoteAux = getComboService()
						.listarAssLoteLayoutArquivo(entrada);

				for (ListarAssLoteLayoutArquivoSaidaDTO combo : listaTipoLoteAux) {
					listaTipoLote
							.add(new SelectItem(combo.getCdTipoLoteLayout(),
									combo.getDsTipoLoteLayout()));
				}
			}
		} catch (PdcAdapterFunctionalException p) {
			listaTipoLote = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Consultar.
	 * 
	 * @return the string
	 */
	public String consultar() {
		try {
			ListarUltimoNumeroRemessaEntradaDTO entrada = new ListarUltimoNumeroRemessaEntradaDTO();
			entrada.setCdTipoLayoutArquivo(getLayoutFiltro());
			entrada.setCdTipoLoteLayout(getTipoLoteFiltro());

			entrada
					.setCdPerfilTrocaArquivo(filtroAgendamentoEfetivacaoEstornoBean
							.getTipoFiltroSelecionado().equals("0") ? filtroAgendamentoEfetivacaoEstornoBean
							.getCdPerfilTrocaArquivoPerfilCliente()
							: filtroAgendamentoEfetivacaoEstornoBean
									.getCdPerfilTrocaArquivoPerfilContrato());

			setListaGrid(getControleUltRemessaRecebidaImpl()
					.listarUltimoNumeroRemessa(entrada));

			setItemSelecionadoLista(null);
			this.listaControleRadio = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGrid().size(); i++) {
				this.listaControleRadio.add(new SelectItem(i, " "));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setDisableArgumentosConsulta(false);
		}

		return "";
	}

	/**
	 * Submit pesquisar.
	 * 
	 * @param evt
	 *            the evt
	 * @return the string
	 */
	public String submitPesquisar(ActionEvent evt) {
		consultar();
		return "";
	}

	/**
	 * Get: comboService.
	 * 
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 * 
	 * @param comboService
	 *            the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 * 
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 * 
	 * @param filtroAgendamentoEfetivacaoEstornoBean
	 *            the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 * 
	 * @return listaTipoLayoutArquivo
	 */
	public List<SelectItem> getListaTipoLayoutArquivo() {
		return listaTipoLayoutArquivo;
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 * 
	 * @param listaTipoLayoutArquivo
	 *            the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(
			List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	/**
	 * Get: listaTipoLote.
	 * 
	 * @return listaTipoLote
	 */
	public List<SelectItem> getListaTipoLote() {
		return listaTipoLote;
	}

	/**
	 * Set: listaTipoLote.
	 * 
	 * @param listaTipoLote
	 *            the lista tipo lote
	 */
	public void setListaTipoLote(List<SelectItem> listaTipoLote) {
		this.listaTipoLote = listaTipoLote;
	}

	/**
	 * Get: layoutFiltro.
	 * 
	 * @return layoutFiltro
	 */
	public Integer getLayoutFiltro() {
		return layoutFiltro;
	}

	/**
	 * Set: layoutFiltro.
	 * 
	 * @param layoutFiltro
	 *            the layout filtro
	 */
	public void setLayoutFiltro(Integer layoutFiltro) {
		this.layoutFiltro = layoutFiltro;
	}

	/**
	 * Get: tipoLoteFiltro.
	 * 
	 * @return tipoLoteFiltro
	 */
	public Integer getTipoLoteFiltro() {
		return tipoLoteFiltro;
	}

	/**
	 * Set: tipoLoteFiltro.
	 * 
	 * @param tipoLoteFiltro
	 *            the tipo lote filtro
	 */
	public void setTipoLoteFiltro(Integer tipoLoteFiltro) {
		this.tipoLoteFiltro = tipoLoteFiltro;
	}

	/**
	 * Get: itemSelecionadoLista.
	 * 
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 * 
	 * @param itemSelecionadoLista
	 *            the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 * 
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 * 
	 * @param listaControleRadio
	 *            the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaGrid.
	 * 
	 * @return listaGrid
	 */
	public List<ListarUltimoNumeroRemessaSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 * 
	 * @param listaGrid
	 *            the lista grid
	 */
	public void setListaGrid(List<ListarUltimoNumeroRemessaSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: controleUltRemessaRecebidaImpl.
	 * 
	 * @return controleUltRemessaRecebidaImpl
	 */
	public IManterConUltRemessaRecebidaService getControleUltRemessaRecebidaImpl() {
		return controleUltRemessaRecebidaImpl;
	}

	/**
	 * Set: controleUltRemessaRecebidaImpl.
	 * 
	 * @param controleUltRemessaRecebidaImpl
	 *            the controle ult remessa recebida impl
	 */
	public void setControleUltRemessaRecebidaImpl(
			IManterConUltRemessaRecebidaService controleUltRemessaRecebidaImpl) {
		this.controleUltRemessaRecebidaImpl = controleUltRemessaRecebidaImpl;
	}

	/**
	 * Get: cdSituacaoContrato.
	 * 
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 * 
	 * @param cdSituacaoContrato
	 *            the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: dsContrato.
	 * 
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 * 
	 * @param dsContrato
	 *            the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 * 
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 * 
	 * @param dsEmpresa
	 *            the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 * 
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 * 
	 * @param dsRazaoSocial
	 *            the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: nrCnpjCpf.
	 * 
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 * 
	 * @param nrCnpjCpf
	 *            the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 * 
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 * 
	 * @param nroContrato
	 *            the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: dadosRemessa.
	 * 
	 * @return dadosRemessa
	 */
	public DetalharUltimoNumeroRemessaSaidaDTO getDadosRemessa() {
		return dadosRemessa;
	}

	/**
	 * Set: dadosRemessa.
	 * 
	 * @param dadosRemessa
	 *            the dados remessa
	 */
	public void setDadosRemessa(DetalharUltimoNumeroRemessaSaidaDTO dadosRemessa) {
		this.dadosRemessa = dadosRemessa;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 * 
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 * 
	 * @param cdPessoaJuridicaContrato
	 *            the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 * 
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 * 
	 * @param cdTipoContratoNegocio
	 *            the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 * 
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 * 
	 * @param nrSequenciaContratoNegocio
	 *            the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdPerfilTrocaArquivoPerfil.
	 * 
	 * @return cdPerfilTrocaArquivoPerfil
	 */
	public Long getCdPerfilTrocaArquivoPerfil() {
		return cdPerfilTrocaArquivoPerfil;
	}

	/**
	 * Set: cdPerfilTrocaArquivoPerfil.
	 * 
	 * @param cdPerfilTrocaArquivoPerfil
	 *            the cd perfil troca arquivo perfil
	 */
	public void setCdPerfilTrocaArquivoPerfil(Long cdPerfilTrocaArquivoPerfil) {
		this.cdPerfilTrocaArquivoPerfil = cdPerfilTrocaArquivoPerfil;
	}

	/**
	 * Get: dsSituacaoPerfil.
	 * 
	 * @return dsSituacaoPerfil
	 */
	public String getDsSituacaoPerfil() {
		return dsSituacaoPerfil;
	}

	/**
	 * Set: dsSituacaoPerfil.
	 * 
	 * @param dsSituacaoPerfil
	 *            the ds situacao perfil
	 */
	public void setDsSituacaoPerfil(String dsSituacaoPerfil) {
		this.dsSituacaoPerfil = dsSituacaoPerfil;
	}

	/**
	 * Is disable argumentos consulta.
	 * 
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 * 
	 * @param disableArgumentosConsulta
	 *            the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: consultasService.
	 * 
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 * 
	 * @param consultasService
	 *            the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
		this.consultasService = consultasService;
	}

	public Long getCdClub() {
		return cdClub;
	}

	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

}
