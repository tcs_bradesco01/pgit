/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.estatisticaproc
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.estatisticaproc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;

import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.EstatisticaProcessamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.EstatisticaProcessamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoProcessoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoProcessoSaidaDTO;

/**
 * Nome: EstatisticaProcBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EstatisticaProcBean 
{
    
    /** Atributo estatisticaProcessamentoFiltro. */
    private EstatisticaProcessamentoEntradaDTO estatisticaProcessamentoFiltro = new EstatisticaProcessamentoEntradaDTO();

    /** Atributo listaTipoProcesso. */
    private List<TipoProcessoSaidaDTO> listaTipoProcesso = new ArrayList<TipoProcessoSaidaDTO>();
    
    /** Atributo listaEstatisticaProcessamento. */
    private List<EstatisticaProcessamentoSaidaDTO> listaEstatisticaProcessamento = new ArrayList<EstatisticaProcessamentoSaidaDTO>();

    /** Atributo listaCentroCusto. */
    private List<CentroCustoSaidaDTO> listaCentroCusto = new ArrayList<CentroCustoSaidaDTO>();
    
    /** Atributo centroCustoLupaFiltro. */
    private String centroCustoLupaFiltro;

    /** Atributo comboService. */
    private IComboService comboService;

    /** Atributo cadProcControleService. */
    private ICadProcControleService cadProcControleService;
    
    /** Atributo btoAcionado. */
    private Boolean btoAcionado;
    
	/**
	 * Get: listaEstatisticaProcessamento.
	 *
	 * @return listaEstatisticaProcessamento
	 */
	public List<EstatisticaProcessamentoSaidaDTO> getListaEstatisticaProcessamento() {
		return listaEstatisticaProcessamento;
	}
	
	/**
	 * Set: listaEstatisticaProcessamento.
	 *
	 * @param listaEstatisticaProcessamento the lista estatistica processamento
	 */
	public void setListaEstatisticaProcessamento(
			List<EstatisticaProcessamentoSaidaDTO> listaEstatisticaProcessamento) {
		this.listaEstatisticaProcessamento = listaEstatisticaProcessamento;
	}

	/**
	 * Navegar tela inicial.
	 *
	 * @return the string
	 */
	public String navegarTelaInicial() {
		try{
			setListaTipoProcesso(getComboService().listarTipoProcesso(new TipoProcessoEntradaDTO()));
		}catch(PdcAdapterFunctionalException e){
			listaTipoProcesso = new ArrayList<TipoProcessoSaidaDTO>();
		}
		estatisticaProcessamentoFiltro.setDtInicioEstatisticaProcessoInicio(new Date());
		estatisticaProcessamentoFiltro.setDtInicioEstatisticaProcessoFim(new Date());
		setBtoAcionado(false);
		return "PGIC0053";
	}

	/**
	 * Limpar dados.
	 */
	public void limparDados(){	
		setEstatisticaProcessamentoFiltro(new EstatisticaProcessamentoEntradaDTO());
		setCentroCustoLupaFiltro("");
		setListaCentroCusto(new ArrayList<CentroCustoSaidaDTO>());
		setListaEstatisticaProcessamento(new ArrayList<EstatisticaProcessamentoSaidaDTO>());
		setBtoAcionado(false);
		estatisticaProcessamentoFiltro.setDtInicioEstatisticaProcessoFim(new Date());
		estatisticaProcessamentoFiltro.setDtInicioEstatisticaProcessoInicio(new Date());
	}

	/**
	 * Limpar.
	 */
	public void limpar() {
		setListaEstatisticaProcessamento(new ArrayList<EstatisticaProcessamentoSaidaDTO>());
		setBtoAcionado(false);
	}

    /**
     * Pesquisa.
     *
     * @param event the event
     * @return the string
     */
    public String pesquisa(ActionEvent event) {
    	try {
			setListaEstatisticaProcessamento(getCadProcControleService().listarEstatisticaProcessamento(getEstatisticaProcessamentoFiltro()));
		} catch (PdcAdapterException p) {
			setListaEstatisticaProcessamento(new ArrayList<EstatisticaProcessamentoSaidaDTO>());
			throw p;
		}

    	return "ok";
    }

	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista() {
		if (!validaFormatacaoHorario(getEstatisticaProcessamentoFiltro().getHrInicioEstatisticaProcessoInicio())) {
			return null;
		}

		if (!validaFormatacaoHorario(getEstatisticaProcessamentoFiltro().getHrInicioEstatisticaProcessoFim())) {
			return null;
		}

		if (getEstatisticaProcessamentoFiltro().getHrInicioEstatisticaProcessoFim() != null && !getEstatisticaProcessamentoFiltro().getHrInicioEstatisticaProcessoFim().trim().equals("")) {
			if (getEstatisticaProcessamentoFiltro().getHrInicioEstatisticaProcessoInicio().length() < 5) {
				BradescoFacesUtils.addInfoModalMessage(MessageHelperUtils.getI18nMessage("error.hora.pattern"), false);
				return null;
			}	
		}

		try {
			setListaEstatisticaProcessamento(getCadProcControleService().listarEstatisticaProcessamento(getEstatisticaProcessamentoFiltro()));
			setBtoAcionado(true);
		} catch (PdcAdapterException p) {
			setListaEstatisticaProcessamento(new ArrayList<EstatisticaProcessamentoSaidaDTO>());
			throw p;
		}
    	return null;
    }
	
	/**
	 * Valida formatacao horario.
	 *
	 * @param horario the horario
	 * @return true, if valida formatacao horario
	 */
	private boolean validaFormatacaoHorario(String horario) {
		if (horario != null && !horario.trim().equals("")) {
			if (horario.length() < 5) {
				BradescoFacesUtils.addInfoModalMessage(MessageHelperUtils.getI18nMessage("error.hora.pattern"), false);
				return false;
			}
		}
		return true;
	}

	/**
	 * Obter centro custo filtro.
	 */
	public void obterCentroCustoFiltro() {
		setListaCentroCusto(new ArrayList<CentroCustoSaidaDTO>());
		if (getCentroCustoLupaFiltro() != null && !getCentroCustoLupaFiltro().trim().equals("")) {
			setListaCentroCusto(getComboService().listarCentroCusto(new CentroCustoEntradaDTO(getCentroCustoLupaFiltro())));
		}
	}

	/**
	 * Get: listaTipoProcesso.
	 *
	 * @return listaTipoProcesso
	 */
	public List<TipoProcessoSaidaDTO> getListaTipoProcesso() {
		return listaTipoProcesso;
	}
	
	/**
	 * Set: listaTipoProcesso.
	 *
	 * @param listaTipoProcesso the lista tipo processo
	 */
	public void setListaTipoProcesso(List<TipoProcessoSaidaDTO> listaTipoProcesso) {
		this.listaTipoProcesso = listaTipoProcesso;
	}

	/**
	 * Get: cadProcControleService.
	 *
	 * @return cadProcControleService
	 */
	public ICadProcControleService getCadProcControleService() {
		return cadProcControleService;
	}
	
	/**
	 * Set: cadProcControleService.
	 *
	 * @param cadProcControleService the cad proc controle service
	 */
	public void setCadProcControleService(
			ICadProcControleService cadProcControleService) {
		this.cadProcControleService = cadProcControleService;
	}
	
	/**
	 * Get: centroCustoLupaFiltro.
	 *
	 * @return centroCustoLupaFiltro
	 */
	public String getCentroCustoLupaFiltro() {
		return centroCustoLupaFiltro;
	}
	
	/**
	 * Set: centroCustoLupaFiltro.
	 *
	 * @param centroCustoLupaFiltro the centro custo lupa filtro
	 */
	public void setCentroCustoLupaFiltro(String centroCustoLupaFiltro) {
		this.centroCustoLupaFiltro = centroCustoLupaFiltro;
	}
	
	/**
	 * Get: listaCentroCusto.
	 *
	 * @return listaCentroCusto
	 */
	public List<CentroCustoSaidaDTO> getListaCentroCusto() {
		return listaCentroCusto;
	}
	
	/**
	 * Set: listaCentroCusto.
	 *
	 * @param listaCentroCusto the lista centro custo
	 */
	public void setListaCentroCusto(List<CentroCustoSaidaDTO> listaCentroCusto) {
		this.listaCentroCusto = listaCentroCusto;
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: estatisticaProcessamentoFiltro.
	 *
	 * @return estatisticaProcessamentoFiltro
	 */
	public EstatisticaProcessamentoEntradaDTO getEstatisticaProcessamentoFiltro() {
		return estatisticaProcessamentoFiltro;
	}
	
	/**
	 * Set: estatisticaProcessamentoFiltro.
	 *
	 * @param estatisticaProcessamentoFiltro the estatistica processamento filtro
	 */
	public void setEstatisticaProcessamentoFiltro(
			EstatisticaProcessamentoEntradaDTO estatisticaProcessamentoFiltro) {
		this.estatisticaProcessamentoFiltro = estatisticaProcessamentoFiltro;
	}
	
	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}
	
	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

}
