/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.registrarassinaturacontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.registrarassinaturacontrato;

import static br.com.bradesco.web.pgit.view.converters.FormatarData.formataDiaMesAno;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoAditivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contrato.IContratoService;
import br.com.bradesco.web.pgit.service.business.contrato.IContratoServiceConstants;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.IManterformalizacaoalteracaocontratoService;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConsultarManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConsultarManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.DetalharManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.DetalharManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.impl.RegistrarAssinaturaContratoServiceImpl;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.IRegistrarAssinaturaFormAltContratoService;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarFormalizacaoManContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarFormalizacaoManContratoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: RegistrarAssinaturaContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RegistrarAssinaturaContratoBean {

    /** Atributo identificacaoClienteContratoBean. */
    private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

    /** Atributo comboService. */
    private IComboService comboService;

    /** Atributo registrarAssinaturaContratoServiceImpl. */
    private RegistrarAssinaturaContratoServiceImpl registrarAssinaturaContratoServiceImpl;
    
    /** Atributo registrarAssinaturaFormAltContratoServiceImpl. */
    private IRegistrarAssinaturaFormAltContratoService registrarAssinaturaFormAltContratoServiceImpl;
    
    /** Atributo formalizacaoAltContratoImpl. */
    private IManterformalizacaoalteracaocontratoService formalizacaoAltContratoImpl;
    
    /** Atributo contratoServiceImpl. */
    private IContratoService contratoServiceImpl = null;
    
    /** The saida detalhar manter form alt contrato. */
    private DetalharManterFormAltContratoSaidaDTO saidaDetalharManterFormAltContrato;
    
    /** The consultar manter form alt contrato saida dto. */
    private ConsultarManterFormAltContratoSaidaDTO consultarManterFormAltContratoSaidaDTO;
    
    /** The contrato bytes. */
    private ByteArrayOutputStream contratoBytes = null;
    
    /** The contrato exception. */
    private Exception contratoException = null;

    /** Atributo listaMotivoAtual. */
    private List<SelectItem> listaMotivoAtual = new ArrayList<SelectItem>();    
    
    /** Atributo listaSituacaoFiltro. */
    private List<SelectItem> listaSituacaoFiltro = new ArrayList<SelectItem>();
    
    /** Atributo listaControleRadio. */
    private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
    
    /** Atributo listaGridPesquisa. */
    private List<ConsultarManterFormAltContratoSaidaDTO> listaGridPesquisa;
    
    /** Atributo cnpjCpfMaster. */
    private String cnpjCpfMaster;

    /** Atributo nomeRazaoSocialMaster. */
    private String nomeRazaoSocialMaster;

    /** Atributo grupoEconomico. */
    private String grupoEconomico;

    /** Atributo segmento. */
    private String segmento;

    /** Atributo subSegmento. */
    private String subSegmento;

    /** Atributo atividadeEconomico. */
    private String atividadeEconomico;

    /** Atributo dsAgenciaGestora. */
    private String dsAgenciaGestora;

    /** Atributo gerenteResponsavel. */
    private String gerenteResponsavel;

    /** Atributo empresaGestora. */
    private String empresaGestora;

    /** Atributo situacao. */
    private String situacao;
    
    /** Atributo tipo. */
    private String tipo;

    /** Atributo numero. */
    private String numero;
    
    /** Atributo motivo. */
    private String motivo;
    
    /** Atributo participacao. */
    private String participacao;
    
    /** Atributo dsContrato. */
    private String dsContrato;
    
    /** Atributo numContratoFisico. */
    private String numContratoFisico;
    
    /** Atributo dataAssinatura. */
    private String dataAssinatura;
    
    /** Atributo obrigatoriedade. */
    private String obrigatoriedade;

    /** Atributo motivoAtivacaoDesc. */
    private String motivoAtivacaoDesc;
    
    /** Atributo radioSelecaoFiltro. */
    private String radioSelecaoFiltro;
    
    /** Atributo numeroFiltro. */
    private String numeroFiltro;
    
    /** Atributo cdEmpresa. */
    private Long cdEmpresa;

    /** Atributo cdTipo. */
    private Integer cdTipo;

    /** Atributo codigoSituacaoFiltro. */
    private Integer codigoSituacaoFiltro;
    
    /** Atributo itemSelecionadoSolicitacao. */
    private Integer itemSelecionadoSolicitacao;
    
    /** Atributo itemSelecionadoLista. */
    private Integer itemSelecionadoLista;
    
    /** The item selecionado lista identificacao cliente. */
    private Integer itemSelecionadoListaIdentificacaoCliente;

    /** Atributo dataDeFiltro. */
    private Date dataDeFiltro;
    
    /** Atributo dataInicialPagamentoFiltro. */
    private Date dataInicialPagamentoFiltro;

    /** Atributo dataFinalPagamentoFiltro. */
    private Date dataFinalPagamentoFiltro;
    
    /** Atributo desabilitaConsulta. */
    private boolean desabilitaConsulta;


    /**
     * Limpar pagina.
     *
     * @param evt the evt
     */
    
    public void limparPagina(ActionEvent evt) {

        this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
        this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
        this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

        identificacaoClienteContratoBean.listarEmpresaGestora();
        identificacaoClienteContratoBean.listarTipoContrato();
        identificacaoClienteContratoBean.listarSituacaoContrato();

        this.setItemSelecionadoSolicitacao(null);

        identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
        identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteRegAssinaturaContrato");
        identificacaoClienteContratoBean.setPaginaRetorno("conRegistrarAssinaturaContrato");
        identificacaoClienteContratoBean.setItemClienteSelecionado(null);
        identificacaoClienteContratoBean.setItemSelecionadoLista(null);

        this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
        this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

        identificacaoClienteContratoBean.limparDadosPesquisaContrato();		
        identificacaoClienteContratoBean.limparDadosCliente();
        identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

        identificacaoClienteContratoBean.setObrigatoriedade("T");
        identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);

        limparCampos();
    }

    /**
     * Limpar retorno.
     *
     * @return the string
     */
    public String limparRetorno() {

        this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(
            new ConsultarListaClientePessoasEntradaDTO());
        this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(
            new ConsultarListaClientePessoasSaidaDTO());
        this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

        identificacaoClienteContratoBean.listarEmpresaGestora();
        identificacaoClienteContratoBean.listarTipoContrato();
        identificacaoClienteContratoBean.listarSituacaoContrato();

        this.setItemSelecionadoSolicitacao(null);

        identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
        identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteRegAssinaturaContrato");
        identificacaoClienteContratoBean.setPaginaRetorno("conRegistrarAssinaturaContrato");
        identificacaoClienteContratoBean.setItemClienteSelecionado(null);
        identificacaoClienteContratoBean.setItemSelecionadoLista(null);

        this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
        this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

        identificacaoClienteContratoBean.limparDadosPesquisaContrato();     
        identificacaoClienteContratoBean.limparDadosCliente();
        identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

        identificacaoClienteContratoBean.setObrigatoriedade("T");
        identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);

        limparCampos();

        return "conRegistrarAssinaturaContrato";
    }

    /**
     * Registrar.
     *
     * @return the string
     */
    public String versoes(){

        setItemSelecionadoListaIdentificacaoCliente(identificacaoClienteContratoBean.getItemSelecionadoLista());
        ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
            getItemSelecionadoListaIdentificacaoCliente());

        setCnpjCpfMaster(saidaDTO.getCnpjOuCpfFormatado());
        setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
        setGrupoEconomico(saidaDTO.getDsGrupoEconomico());
        setAtividadeEconomico(saidaDTO.getDsAtividadeEconomica());
        setSegmento(saidaDTO.getDsSegmentoCliente());
        setSubSegmento(saidaDTO.getDsSubSegmentoCliente());
        setEmpresaGestora(String.valueOf(saidaDTO.getDsPessoaJuridica()));
        setCdEmpresa(saidaDTO.getCdPessoaJuridica());
        setTipo(saidaDTO.getDsTipoContrato());
        setCdTipo(saidaDTO.getCdTipoContrato());
        setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
        setSituacao(saidaDTO.getDsSituacaoContrato());
        setMotivo(saidaDTO.getDsMotivoSituacao());
        setParticipacao(saidaDTO.getCdTipoParticipacao());
        setDsContrato(saidaDTO.getDsContrato());
        setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
        setGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
        
        limparCampos();
        carregaListaSituacaoFiltro();
        
        return "VERSOES";
    }

   
    
    /**
     * Consultar.
     *
     * @return the string
     */
    public String consultar() {
        carregaLista();
        return "ok";

    }
    
    /**
     * Cosultar.
     *
     * @param evt the evt
     */
	public void paginacaoRegistrarAssinaturaContrato(ActionEvent evt) {
		consultar();
	}    
    
    /**
     * Detalhar.
     *
     * @return the string
     */
    public String detalhar() {

        try {
            preencheDados();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                "", BradescoViewExceptionActionType.ACTION, false);
            return null;
        }
        return "DETALHAR";
    }
    
    /**
     * Preenche dados.
     */
    private void preencheDados() {
        
        consultarManterFormAltContratoSaidaDTO = 
            getListaGridPesquisa().get(getItemSelecionadoLista());

        DetalharManterFormAltContratoEntradaDTO entradaDTO = new DetalharManterFormAltContratoEntradaDTO();

        entradaDTO.setNrAditivoContratoNegocio(
            consultarManterFormAltContratoSaidaDTO.getNrAditivoContratoNegocio());

        ListarContratosPgitSaidaDTO saidaDTOContrato = identificacaoClienteContratoBean.getListaGridPesquisa().get(
            getItemSelecionadoListaIdentificacaoCliente());

        entradaDTO.setCdPessoaJuridicaContrato(saidaDTOContrato.getCdPessoaJuridica());
        entradaDTO.setCdTipoContratoNegocio(saidaDTOContrato.getCdTipoContrato());
        entradaDTO.setNrSequenciaContratoNegocio(saidaDTOContrato.getNrSequenciaContrato());

        setSaidaDetalharManterFormAltContrato(new DetalharManterFormAltContratoSaidaDTO());
        saidaDetalharManterFormAltContrato = 
            getFormalizacaoAltContratoImpl().detalharManterFormAltContratoSaidaDTO(entradaDTO);

    }
    
    /**
     * Registrar.
     *
     * @return the string
     */
    public String registrar(){

        consultarManterFormAltContratoSaidaDTO = 
            getListaGridPesquisa().get(getItemSelecionadoLista());
        
        return "REGISTRAR";
    }
    
    /**
     * Confirmar registro.
     *
     * @return the string
     */
    public String confirmarRegistro(){
        try{
            RegistrarFormalizacaoManContratoEntradaDTO entrada = new RegistrarFormalizacaoManContratoEntradaDTO();
                        
            entrada.setCdContratoNegocioPagamento(getNumContratoFisico());
            entrada.setCdPessoaJuridicaContrato(getCdEmpresa());
            entrada.setCdTipoContratoNegocio(getCdTipo());
            entrada.setNrSequenciaContratoNegocio(PgitUtil.isStringNullLong(getNumero()));
            entrada.setCdContratoNegocioPagamento("0");
            entrada.setDtAssinaturaAditivo(consultarManterFormAltContratoSaidaDTO.getDataHoraGeracao().substring(0, 10));
            entrada.setNrAditivoContratoNegocio( consultarManterFormAltContratoSaidaDTO.getNrAditivoContratoNegocio());
            
            RegistrarFormalizacaoManContratoSaidaDTO saida = 
                getRegistrarAssinaturaFormAltContratoServiceImpl().registrarFormalizacaoManContrato(entrada);
            
            BradescoFacesUtils.addInfoModalMessage("(" +saida.getCodMensagem() + ") " + saida.getMensagem(), 
                "", BradescoViewExceptionActionType.ACTION, false);              
            
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
            return null;
        }

        consultar();
        return "VOLTAR";
    }   
    
    /**
     * Imprimir contrato.
     */
    public void imprimirContrato() {

        contratoBytes = new ByteArrayOutputStream();

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String pathRel = servletContext.getRealPath("/");

        ListarContratosPgitSaidaDTO contratoPgitSelecionado =
            identificacaoClienteContratoBean.getListaGridPesquisa().get(
                getItemSelecionadoListaIdentificacaoCliente());
        try {
            getContratoServiceImpl().imprimirContratoPrestacaoServico(contratoPgitSelecionado.getCdTipoContrato(),
                contratoPgitSelecionado.getCdPessoaJuridica(), contratoPgitSelecionado.getNrSequenciaContrato(), 
                getListaGridPesquisa().get(getItemSelecionadoLista()).getNrAditivoContratoNegocio(),
                contratoBytes, pathRel);
        } catch (Exception e) {
            BradescoCommonServiceFactory.getLogManager().error(this.getClass(), e.getMessage(), e);
            contratoException = e;
            contratoBytes = null;
        }
    }
    
    /**
     * Imprimir contrato final.
     * 
     * @return the string
     *
     */
    public String imprimirContratoFinal() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        if (contratoBytes != null) {
            try {
                response.getOutputStream().write(contratoBytes.toByteArray());
                response.setHeader("Content-Disposition", "attachment; filename=contrato.pdf");
                response.setContentType("application/pdf");

                facesContext.renderResponse();
                facesContext.responseComplete();
            } catch (Exception e) {
                throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
            }
        } else {
            if (contratoException instanceof PdcAdapterFunctionalException) {
                BradescoFacesUtils.addErrorModalMessage(contratoException.getMessage());
            } else {
                BradescoFacesUtils.addErrorModalMessage(MessageHelperUtils
                    .getI18nMessage(IContratoServiceConstants.ERRO_IMPRESSAO_CONTRATO));
            }
        }

        contratoException = null;
        contratoBytes = null;

        return "";
    }
    
    /**
     * Carrega lista.
     */
    public void carregaLista() {
        setDesabilitaConsulta(false);
        listaGridPesquisa = new ArrayList<ConsultarManterFormAltContratoSaidaDTO>();
        setItemSelecionadoLista(null);

        try {
            ConsultarManterFormAltContratoEntradaDTO entrada = new ConsultarManterFormAltContratoEntradaDTO();

            entrada.setCdPessoaJuridicaContrato(getCdEmpresa());
            entrada.setCdTipoContratoNegocio(getCdTipo());
            entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNumero()));

            if (getRadioSelecaoFiltro() == null || getRadioSelecaoFiltro().equals("")) {
                entrada.setCdAcesso(0);
                entrada.setDtInicioInclusaoContrato("");
                entrada.setDtFimInclusaoContrato("");
            } else if (getRadioSelecaoFiltro().equals("0")) {
                entrada.setCdAcesso(1);
                entrada.setDtInicioInclusaoContrato("");
                entrada.setDtFimInclusaoContrato("");
            } else if (getRadioSelecaoFiltro().equals("1")) {
                entrada.setCdAcesso(2);
                entrada.setDtInicioInclusaoContrato("");
                entrada.setDtFimInclusaoContrato("");
            }else if (getRadioSelecaoFiltro().equals("2")) {
                entrada.setCdAcesso(3);
                entrada.setDtInicioInclusaoContrato(formataDiaMesAno(getDataInicialPagamentoFiltro()));
                entrada.setDtFimInclusaoContrato(formataDiaMesAno(getDataFinalPagamentoFiltro()));
            }

            entrada.setNrAditivoContratoNegocio(PgitUtil.isStringNullLong(getNumeroFiltro()));
            entrada.setCdSituacaoAditivoContrato(PgitUtil.verificaIntegerNulo(getCodigoSituacaoFiltro()));

            setListaGridPesquisa(getFormalizacaoAltContratoImpl().consultarListaManterFormAltContrato(entrada));

            setListaControleRadio(new ArrayList<SelectItem>());
            for (int i = 0; i < getListaGridPesquisa().size(); i++) {
                this.listaControleRadio.add(new SelectItem(i, " "));
            }

            setItemSelecionadoLista(null);
            setDesabilitaConsulta(true);
        } catch (PdcAdapterFunctionalException p) {
            setListaGridPesquisa(new ArrayList<ConsultarManterFormAltContratoSaidaDTO>());
            setItemSelecionadoLista(null);
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + 
                p.getMessage(), false);
        }
    }
    
    /**
     * Pesquisar.
     *
     * @param evt the evt
     */
    public void pesquisar(ActionEvent evt) {
        identificacaoClienteContratoBean.consultarContrato();
    }
    
    /**
     * Carrega lista situacao filtro.
     */
    public void carregaListaSituacaoFiltro() {
        setListaSituacaoFiltro(new ArrayList<SelectItem>());
        List<ListarSituacaoAditivoContratoSaidaDTO> listaSaida = new ArrayList<ListarSituacaoAditivoContratoSaidaDTO>();

        listaSaida = comboService.listarSituacaoAditivoContrato();

        for (ListarSituacaoAditivoContratoSaidaDTO combo : listaSaida) {
            listaSituacaoFiltro.add(new SelectItem(combo.getCdSituacao(), combo.getDsSituacao()));
        }
    }

    
    /**
     * Limpar campos.
     *
     * @return the string
     */
    public String limparCampos() {
        setRadioSelecaoFiltro("");
        setItemSelecionadoLista(null);
        setListaGridPesquisa(null);
        setDesabilitaConsulta(false);
        setDataInicialPagamentoFiltro(new Date());
        setDataFinalPagamentoFiltro(new Date());
        limparDados();
        
        return "";
    }
    
    /**
     * Limpar grid pesquisar.
     *
     * @return the string
     */
    public String limparGridPesquisar() {
        setListaGridPesquisa(new ArrayList<ConsultarManterFormAltContratoSaidaDTO>());
        setListaControleRadio(new ArrayList<SelectItem>());
        setItemSelecionadoLista(null);        
        return "";
    }
    
    
    
    /**
     * Limpar dados.
     */
    public void limparDados() {
        setNumeroFiltro("");
        setCodigoSituacaoFiltro(0);

        if (getRadioSelecaoFiltro() != null) {
            setDesabilitaConsulta(false);               
        }
    }
    
    /**
     * Checks if is habilita botao registrar.
     *
     * @return true, if is habilita botao registrar
     */
    public boolean isHabilitaBotaoRegistrar(){
        if (getListaGridPesquisa() != null && getItemSelecionadoLista() != null ) {
            if("Pendente".equalsIgnoreCase(getListaGridPesquisa().get(
                getItemSelecionadoLista()).getDsSitucaoAditivoContrato())){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Limpar item selecionado.
     *
     * @param actionEvent the action event
     */
    public void limparItemSelecionado(ActionEvent actionEvent) {
        setItemSelecionadoLista(null);
    }

    /**
     * Voltar ativar assinatura formalizacao alt contrato.
     *
     * @return the string
     */
    public String voltarAtivarAssinaturaFormalizacaoAltContrato(){
        return "VOLTAR";
    }

    /**
     * Avancar ativar registrar alt contrato.
     *
     * @return the string
     */
    public String avancarAtivarRegistrarAltContrato(){
        return "AVANCAR_ALTERAR_CONFIRMAR";
    }

    /**
     * Voltar ativar registrar alt contrato.
     *
     * @return the string
     */
    public String voltarAtivarRegistrarAltContrato(){
        return "VOLTAR_ALTERAR_CONFIRMAR";
    }

    /**
     * Avancar ativar registrar formalizacao alt contrato.
     *
     * @return the string
     */
    public String avancarAtivarRegistrarFormalizacaoAltContrato(){
        return "AVANCAR";
    }

    /**
     * Voltar.
     *
     * @return the string
     */
    public String voltar(){
        return "VOLTAR";
    }

    /**
     * Voltar con registrar assinatura contrato.
     *
     * @return the string
     */
    public String voltarConRegistrarAssinaturaContrato(){
        identificacaoClienteContratoBean.setItemSelecionadoLista(null);
        return "VOLTAR";
    }

    /**
     * Voltar registrar assinatura contrato.
     *
     * @return the string
     */
    public String voltarRegistrarAssinaturaContrato(){
        return "VOLTAR";
    }

    /**
     * Get: itemSelecionadoSolicitacao.
     *
     * @return itemSelecionadoSolicitacao
     */
    public Integer getItemSelecionadoSolicitacao() {
        return itemSelecionadoSolicitacao;
    }

    /**
     * Set: itemSelecionadoSolicitacao.
     *
     * @param itemSelecionadoSolicitacao the item selecionado solicitacao
     */
    public void setItemSelecionadoSolicitacao(Integer itemSelecionadoSolicitacao) {
        this.itemSelecionadoSolicitacao = itemSelecionadoSolicitacao;
    }	

    /**
     * Get: listaMotivoAtual.
     *
     * @return listaMotivoAtual
     */
    public List<SelectItem> getListaMotivoAtual() {
        return listaMotivoAtual;
    }

    /**
     * Set: listaMotivoAtual.
     *
     * @param listaMotivoAtual the lista motivo atual
     */
    public void setListaMotivoAtual(List<SelectItem> listaMotivoAtual) {
        this.listaMotivoAtual = listaMotivoAtual;
    }

    /**
     * Get: dataDeFiltro.
     *
     * @return dataDeFiltro
     */
    public Date getDataDeFiltro() {
        return dataDeFiltro;
    }

    /**
     * Set: dataDeFiltro.
     *
     * @param dataDeFiltro the data de filtro
     */
    public void setDataDeFiltro(Date dataDeFiltro) {
        this.dataDeFiltro = dataDeFiltro;
    }

    /**
     * Get: atividadeEconomico.
     *
     * @return atividadeEconomico
     */
    public String getAtividadeEconomico() {
        return atividadeEconomico;
    }

    /**
     * Set: atividadeEconomico.
     *
     * @param atividadeEconomico the atividade economico
     */
    public void setAtividadeEconomico(String atividadeEconomico) {
        this.atividadeEconomico = atividadeEconomico;
    }

    /**
     * Get: cnpjCpfMaster.
     *
     * @return cnpjCpfMaster
     */
    public String getCnpjCpfMaster() {
        return cnpjCpfMaster;
    }

    /**
     * Set: cnpjCpfMaster.
     *
     * @param cnpjCpfMaster the cnpj cpf master
     */
    public void setCnpjCpfMaster(String cnpjCpfMaster) {
        this.cnpjCpfMaster = cnpjCpfMaster;
    }

    /**
     * Get: empresaGestora.
     *
     * @return empresaGestora
     */
    public String getEmpresaGestora() {
        return empresaGestora;
    }

    /**
     * Set: empresaGestora.
     *
     * @param empresaGestora the empresa gestora
     */
    public void setEmpresaGestora(String empresaGestora) {
        this.empresaGestora = empresaGestora;
    }

    /**
     * Get: grupoEconomico.
     *
     * @return grupoEconomico
     */
    public String getGrupoEconomico() {
        return grupoEconomico;
    }

    /**
     * Set: grupoEconomico.
     *
     * @param grupoEconomico the grupo economico
     */
    public void setGrupoEconomico(String grupoEconomico) {
        this.grupoEconomico = grupoEconomico;
    }

    /**
     * Get: motivo.
     *
     * @return motivo
     */
    public String getMotivo() {
        return motivo;
    }

    /**
     * Set: motivo.
     *
     * @param motivo the motivo
     */
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    /**
     * Get: nomeRazaoSocialMaster.
     *
     * @return nomeRazaoSocialMaster
     */
    public String getNomeRazaoSocialMaster() {
        return nomeRazaoSocialMaster;
    }

    /**
     * Set: nomeRazaoSocialMaster.
     *
     * @param nomeRazaoSocialMaster the nome razao social master
     */
    public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
        this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
    }

    /**
     * Get: numero.
     *
     * @return numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Set: numero.
     *
     * @param numero the numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Get: participacao.
     *
     * @return participacao
     */
    public String getParticipacao() {
        return participacao;
    }

    /**
     * Set: participacao.
     *
     * @param participacao the participacao
     */
    public void setParticipacao(String participacao) {
        this.participacao = participacao;
    }

    /**
     * Get: segmento.
     *
     * @return segmento
     */
    public String getSegmento() {
        return segmento;
    }

    /**
     * Set: segmento.
     *
     * @param segmento the segmento
     */
    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    /**
     * Get: situacao.
     *
     * @return situacao
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * Set: situacao.
     *
     * @param situacao the situacao
     */
    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    /**
     * Get: subSegmento.
     *
     * @return subSegmento
     */
    public String getSubSegmento() {
        return subSegmento;
    }

    /**
     * Set: subSegmento.
     *
     * @param subSegmento the sub segmento
     */
    public void setSubSegmento(String subSegmento) {
        this.subSegmento = subSegmento;
    }

    /**
     * Get: tipo.
     *
     * @return tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Set: tipo.
     *
     * @param tipo the tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }





    /**
     * Get: comboService.
     *
     * @return comboService
     */
    public IComboService getComboService() {
        return comboService;
    }

    /**
     * Set: comboService.
     *
     * @param comboService the combo service
     */
    public void setComboService(IComboService comboService) {
        this.comboService = comboService;
    }



    /**
     * Get: identificacaoClienteContratoBean.
     *
     * @return identificacaoClienteContratoBean
     */
    public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
        return identificacaoClienteContratoBean;
    }

    /**
     * Set: identificacaoClienteContratoBean.
     *
     * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
     */
    public void setIdentificacaoClienteContratoBean(
        IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
        this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
    }

    /**
     * Get: cdEmpresa.
     *
     * @return cdEmpresa
     */
    public Long getCdEmpresa() {
        return cdEmpresa;
    }

    /**
     * Set: cdEmpresa.
     *
     * @param cdEmpresa the cd empresa
     */
    public void setCdEmpresa(Long cdEmpresa) {
        this.cdEmpresa = cdEmpresa;
    }

    /**
     * Get: cdTipo.
     *
     * @return cdTipo
     */
    public Integer getCdTipo() {
        return cdTipo;
    }

    /**
     * Set: cdTipo.
     *
     * @param cdTipo the cd tipo
     */
    public void setCdTipo(Integer cdTipo) {
        this.cdTipo = cdTipo;
    }

    /**
     * Get: registrarAssinaturaContratoServiceImpl.
     *
     * @return registrarAssinaturaContratoServiceImpl
     */
    public RegistrarAssinaturaContratoServiceImpl getRegistrarAssinaturaContratoServiceImpl() {
        return registrarAssinaturaContratoServiceImpl;
    }

    /**
     * Set: registrarAssinaturaContratoServiceImpl.
     *
     * @param registrarAssinaturaContratoServiceImpl the registrar assinatura contrato service impl
     */
    public void setRegistrarAssinaturaContratoServiceImpl(
        RegistrarAssinaturaContratoServiceImpl registrarAssinaturaContratoServiceImpl) {
        this.registrarAssinaturaContratoServiceImpl = registrarAssinaturaContratoServiceImpl;
    }

    /**
     * Get: dsAgenciaGestora.
     *
     * @return dsAgenciaGestora
     */
    public String getDsAgenciaGestora() {
        return dsAgenciaGestora;
    }


    /**
     * Set: dsAgenciaGestora.
     *
     * @param dsAgenciaGestora the ds agencia gestora
     */
    public void setDsAgenciaGestora(String dsAgenciaGestora) {
        this.dsAgenciaGestora = dsAgenciaGestora;
    }


    /**
     * Get: dataAssinatura.
     *
     * @return dataAssinatura
     */
    public String getDataAssinatura() {
        return dataAssinatura;
    }

    /**
     * Set: dataAssinatura.
     *
     * @param dataAssinatura the data assinatura
     */
    public void setDataAssinatura(String dataAssinatura) {
        this.dataAssinatura = dataAssinatura;
    }

    /**
     * Get: dsContrato.
     *
     * @return dsContrato
     */
    public String getDsContrato() {
        return dsContrato;
    }

    /**
     * Set: dsContrato.
     *
     * @param dsContrato the ds contrato
     */
    public void setDsContrato(String dsContrato) {
        this.dsContrato = dsContrato;
    }

    /**
     * Get: obrigatoriedade.
     *
     * @return obrigatoriedade
     */
    public String getObrigatoriedade() {
        return obrigatoriedade;
    }

    /**
     * Set: obrigatoriedade.
     *
     * @param obrigatoriedade the obrigatoriedade
     */
    public void setObrigatoriedade(String obrigatoriedade) {
        this.obrigatoriedade = obrigatoriedade;
    }

    /**
     * Get: motivoAtivacaoDesc.
     *
     * @return motivoAtivacaoDesc
     */
    public String getMotivoAtivacaoDesc() {
        return motivoAtivacaoDesc;
    }

    /**
     * Set: motivoAtivacaoDesc.
     *
     * @param motivoAtivacaoDesc the motivo ativacao desc
     */
    public void setMotivoAtivacaoDesc(String motivoAtivacaoDesc) {
        this.motivoAtivacaoDesc = motivoAtivacaoDesc;
    }


    /**
     * Get: numContratoFisico.
     *
     * @return numContratoFisico
     */
    public String getNumContratoFisico() {
        return numContratoFisico;
    }


    /**
     * Set: numContratoFisico.
     *
     * @param numContratoFisico the num contrato fisico
     */
    public void setNumContratoFisico(String numContratoFisico) {
        this.numContratoFisico = numContratoFisico;
    }


    /**
     * Get: gerenteResponsavel.
     *
     * @return gerenteResponsavel
     */
    public String getGerenteResponsavel() {
        return gerenteResponsavel;
    }


    /**
     * Set: gerenteResponsavel.
     *
     * @param gerenteResponsavel the gerente responsavel
     */
    public void setGerenteResponsavel(String gerenteResponsavel) {
        this.gerenteResponsavel = gerenteResponsavel;
    }

    /**
     * Gets the radio selecao filtro.
     *
     * @return the radio selecao filtro
     */
    public String getRadioSelecaoFiltro() {
        return radioSelecaoFiltro;
    }

    /**
     * Sets the radio selecao filtro.
     *
     * @param radioSelecaoFiltro the new radio selecao filtro
     */
    public void setRadioSelecaoFiltro(String radioSelecaoFiltro) {
        this.radioSelecaoFiltro = radioSelecaoFiltro;
    }

    /**
     * Gets the numero filtro.
     *
     * @return the numero filtro
     */
    public String getNumeroFiltro() {
        return numeroFiltro;
    }

    /**
     * Sets the numero filtro.
     *
     * @param numeroFiltro the new numero filtro
     */
    public void setNumeroFiltro(String numeroFiltro) {
        this.numeroFiltro = numeroFiltro;
    }

    /**
     * Gets the codigo situacao filtro.
     *
     * @return the codigo situacao filtro
     */
    public Integer getCodigoSituacaoFiltro() {
        return codigoSituacaoFiltro;
    }

    /**
     * Sets the codigo situacao filtro.
     *
     * @param codigoSituacaoFiltro the new codigo situacao filtro
     */
    public void setCodigoSituacaoFiltro(Integer codigoSituacaoFiltro) {
        this.codigoSituacaoFiltro = codigoSituacaoFiltro;
    }

    /**
     * Checks if is desabilita consulta.
     *
     * @return true, if is desabilita consulta
     */
    public boolean isDesabilitaConsulta() {
        return desabilitaConsulta;
    }

    /**
     * Sets the desabilita consulta.
     *
     * @param desabilitaConsulta the new desabilita consulta
     */
    public void setDesabilitaConsulta(boolean desabilitaConsulta) {
        this.desabilitaConsulta = desabilitaConsulta;
    }

    /**
     * Gets the lista situacao filtro.
     *
     * @return the lista situacao filtro
     */
    public List<SelectItem> getListaSituacaoFiltro() {
        return listaSituacaoFiltro;
    }

    /**
     * Sets the lista situacao filtro.
     *
     * @param listaSituacaoFiltro the new lista situacao filtro
     */
    public void setListaSituacaoFiltro(List<SelectItem> listaSituacaoFiltro) {
        this.listaSituacaoFiltro = listaSituacaoFiltro;
    }

    /**
     * Gets the lista grid pesquisa.
     *
     * @return the lista grid pesquisa
     */
    public List<ConsultarManterFormAltContratoSaidaDTO> getListaGridPesquisa() {
        return listaGridPesquisa;
    }

    /**
     * Sets the lista grid pesquisa.
     *
     * @param listaGridPesquisa the new lista grid pesquisa
     */
    public void setListaGridPesquisa(List<ConsultarManterFormAltContratoSaidaDTO> listaGridPesquisa) {
        this.listaGridPesquisa = listaGridPesquisa;
    }

    /**
     * Gets the item selecionado lista.
     *
     * @return the item selecionado lista
     */
    public Integer getItemSelecionadoLista() {
        return itemSelecionadoLista;
    }

    /**
     * Sets the item selecionado lista.
     *
     * @param itemSelecionadoLista the new item selecionado lista
     */
    public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
        this.itemSelecionadoLista = itemSelecionadoLista;
    }

    /**
     * Gets the data inicial pagamento filtro.
     *
     * @return the data inicial pagamento filtro
     */
    public Date getDataInicialPagamentoFiltro() {
        return dataInicialPagamentoFiltro;
    }

    /**
     * Sets the data inicial pagamento filtro.
     *
     * @param dataInicialPagamentoFiltro the new data inicial pagamento filtro
     */
    public void setDataInicialPagamentoFiltro(Date dataInicialPagamentoFiltro) {
        this.dataInicialPagamentoFiltro = dataInicialPagamentoFiltro;
    }

    /**
     * Gets the data final pagamento filtro.
     *
     * @return the data final pagamento filtro
     */
    public Date getDataFinalPagamentoFiltro() {
        return dataFinalPagamentoFiltro;
    }

    /**
     * Sets the data final pagamento filtro.
     *
     * @param dataFinalPagamentoFiltro the new data final pagamento filtro
     */
    public void setDataFinalPagamentoFiltro(Date dataFinalPagamentoFiltro) {
        this.dataFinalPagamentoFiltro = dataFinalPagamentoFiltro;
    }

    /**
     * Nome: setFormalizacaoAltContratoImpl.
     *
     * @param formalizacaoAltContratoImpl the new formalizacao alt contrato impl
     */
    public void setFormalizacaoAltContratoImpl(IManterformalizacaoalteracaocontratoService formalizacaoAltContratoImpl) {
        this.formalizacaoAltContratoImpl = formalizacaoAltContratoImpl;
    }

    /**
     * Nome: getFormalizacaoAltContratoImpl.
     *
     * @return formalizacaoAltContratoImpl
     */
    public IManterformalizacaoalteracaocontratoService getFormalizacaoAltContratoImpl() {
        return formalizacaoAltContratoImpl;
    }

    /**
     * Gets the lista controle radio.
     *
     * @return the lista controle radio
     */
    public List<SelectItem> getListaControleRadio() {
        return listaControleRadio;
    }

    /**
     * Sets the lista controle radio.
     *
     * @param listaControleRadio the new lista controle radio
     */
    public void setListaControleRadio(List<SelectItem> listaControleRadio) {
        this.listaControleRadio = listaControleRadio;
    }

    /**
     * Gets the saida detalhar manter form alt contrato.
     *
     * @return the saida detalhar manter form alt contrato
     */
    public DetalharManterFormAltContratoSaidaDTO getSaidaDetalharManterFormAltContrato() {
        return saidaDetalharManterFormAltContrato;
    }

    /**
     * Sets the saida detalhar manter form alt contrato.
     *
     * @param saidaDetalharManterFormAltContrato the new saida detalhar manter form alt contrato
     */
    public void setSaidaDetalharManterFormAltContrato(
        DetalharManterFormAltContratoSaidaDTO saidaDetalharManterFormAltContrato) {
        this.saidaDetalharManterFormAltContrato = saidaDetalharManterFormAltContrato;
    }

    /**
     * Gets the consultar manter form alt contrato saida dto.
     *
     * @return the consultar manter form alt contrato saida dto
     */
    public ConsultarManterFormAltContratoSaidaDTO getConsultarManterFormAltContratoSaidaDTO() {
        return consultarManterFormAltContratoSaidaDTO;
    }

    /**
     * Sets the consultar manter form alt contrato saida dto.
     *
     * @param consultarManterFormAltContratoSaidaDTO the new consultar manter form alt contrato saida dto
     */
    public void setConsultarManterFormAltContratoSaidaDTO(
        ConsultarManterFormAltContratoSaidaDTO consultarManterFormAltContratoSaidaDTO) {
        this.consultarManterFormAltContratoSaidaDTO = consultarManterFormAltContratoSaidaDTO;
    }

    /**
     * Gets the registrar assinatura form alt contrato service impl.
     *
     * @return the registrar assinatura form alt contrato service impl
     */
    public IRegistrarAssinaturaFormAltContratoService getRegistrarAssinaturaFormAltContratoServiceImpl() {
        return registrarAssinaturaFormAltContratoServiceImpl;
    }

    /**
     * Sets the registrar assinatura form alt contrato service impl.
     *
     * @param registrarAssinaturaFormAltContratoServiceImpl the new registrar assinatura form alt contrato service impl
     */
    public void setRegistrarAssinaturaFormAltContratoServiceImpl(
        IRegistrarAssinaturaFormAltContratoService registrarAssinaturaFormAltContratoServiceImpl) {
        this.registrarAssinaturaFormAltContratoServiceImpl = registrarAssinaturaFormAltContratoServiceImpl;
    }

    /**
     * Nome: setContratoServiceImpl.
     *
     * @param contratoServiceImpl the new contrato service impl
     */
    public void setContratoServiceImpl(IContratoService contratoServiceImpl) {
        this.contratoServiceImpl = contratoServiceImpl;
    }

    /**
     * Nome: getContratoServiceImpl.
     *
     * @return contratoServiceImpl
     */
    public IContratoService getContratoServiceImpl() {
        return contratoServiceImpl;
    }

    /**
     * Gets the item selecionado lista identificacao cliente.
     *
     * @return the item selecionado lista identificacao cliente
     */
    public Integer getItemSelecionadoListaIdentificacaoCliente() {
        return itemSelecionadoListaIdentificacaoCliente;
    }

    /**
     * Sets the item selecionado lista identificacao cliente.
     *
     * @param itemSelecionadoListaIdentificacaoCliente the new item selecionado lista identificacao cliente
     */
    public void setItemSelecionadoListaIdentificacaoCliente(Integer itemSelecionadoListaIdentificacaoCliente) {
        this.itemSelecionadoListaIdentificacaoCliente = itemSelecionadoListaIdentificacaoCliente;
    }
    
}
