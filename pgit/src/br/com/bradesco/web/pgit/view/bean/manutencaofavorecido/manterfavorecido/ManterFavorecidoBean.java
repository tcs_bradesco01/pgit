/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaofavorecido.manterfavorecido
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaofavorecido.manterfavorecido;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.bean.InscricaoFavorecidosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.IManterFavorecidoService;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.BloquearFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheHistoricoContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarDetalheHistoricoContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoContaFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoContaFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarListaHistoricoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ConsultarMotivoBloqueioContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.DetalharHistoricoFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.DetalharHistoricoFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ListarFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ListarFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterfavorecido.bean.ListarMotivoBloqueioFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterFavorecidoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterFavorecidoBean extends IdentificacaoClienteBean {

	
	//  Constantes utilizadas pelo Construtor (Super) para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_MANTER_FAVORECIDO. */
	private static final String CON_MANTER_FAVORECIDO =  "conManterFavorecido";
	
	//  Constantes utilizadas no m�todo addInfoModalMessage do BradescoFacesUtils  para evitar redund�ncia apontada pelo Sonar
	/** Atributo MANTER_FAVORECIDO_JSP_ACESSO. */
	private static final String MANTER_FAVORECIDO_JSP_ACESSO = "/conManterFavorecido.jsf";
	
	//  Constantes utilizadas no m�todo addInfoModalMessage do BradescoFacesUtils  para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_HIST_PESQ_MANTER_FAVORECIDO_JSP_ACESSO. */
	private static final String CON_HIST_PESQ_MANTER_FAVORECIDO_JSP_ACESSO = "/contPesqManterFavorecido.jsf";
	
	
	
	/** Atributo entradaListarFavorecido. */
	private ListarFavorecidoEntradaDTO entradaListarFavorecido;
	
	/** Atributo saidaListarFavorecido. */
	private ListarFavorecidoSaidaDTO saidaListarFavorecido;
	
	/** Atributo listaFavorecido. */
	private List<ListarFavorecidoSaidaDTO> listaFavorecido;
	
	/** Atributo cdSituacao. */
	private Integer cdSituacao;
	
	/** Atributo dsSituacao. */
	private String dsSituacao;

	/** Atributo itemArgumentoPesquisa. */
	private String itemArgumentoPesquisa;
	
	/** Atributo itemFavorecidoSelecionado. */
	private Integer itemFavorecidoSelecionado;
	
	/** Atributo itemSelecionadoListaFavorecidos. */
	private Integer itemSelecionadoListaFavorecidos;

	/** Atributo saidaConsultarDetalheFavorecido. */
	private ConsultarDetalheFavorecidoSaidaDTO saidaConsultarDetalheFavorecido;
	
	/** Atributo saidaConsultarHistoricoFavorecido. */
	private ConsultarListaHistoricoFavorecidoSaidaDTO saidaConsultarHistoricoFavorecido;
	
	/** Atributo listaConsultarHistoricoFavorecido. */
	private List<ConsultarListaHistoricoFavorecidoSaidaDTO> listaConsultarHistoricoFavorecido;
	
	/** Atributo saidaDetalharHistoricoFavorecido. */
	private DetalharHistoricoFavorecidoSaidaDTO saidaDetalharHistoricoFavorecido;

	/** Atributo itemSelecionadoListaHistorico. */
	private Integer itemSelecionadoListaHistorico;
	
	/** Atributo listaConsultarListaContaFavorecido. */
	private List<ConsultarListaContaFavorecidoSaidaDTO> listaConsultarListaContaFavorecido;
	
	/** Atributo saidaConsultarListaContaFavorecido. */
	private ConsultarListaContaFavorecidoSaidaDTO saidaConsultarListaContaFavorecido;
	
	/** Atributo saidaConsultarDetalheContaFavorecido. */
	private ConsultarDetalheContaFavorecidoSaidaDTO saidaConsultarDetalheContaFavorecido;
	
	/** Atributo saidaConsultarListaHistoricoContaFavorecido. */
	private ConsultarListaHistoricoContaFavorecidoSaidaDTO saidaConsultarListaHistoricoContaFavorecido;	
	
	/** Atributo listaConsultarListaHistoricoContaFavorecido. */
	private List<ConsultarListaHistoricoContaFavorecidoSaidaDTO> listaConsultarListaHistoricoContaFavorecido;
	
	/** Atributo saidaConsultarDetalheHistoricoContaFavorecido. */
	private ConsultarDetalheHistoricoContaFavorecidoSaidaDTO saidaConsultarDetalheHistoricoContaFavorecido;

	/** Atributo entradaBloquearContaFavorecidoEntrada. */
	private BloquearContaFavorecidoEntradaDTO entradaBloquearContaFavorecidoEntrada;
	
	/** Atributo motivoBloqueioConta. */
	private String motivoBloqueioConta;
	
	/** Atributo itemContaSelecionada. */
	private Integer itemContaSelecionada = -1;
	
	/** Atributo itemContaHistorico. */
	private Integer itemContaHistorico = -1 ;
	
	/** Atributo entradaBloquearFavorecido. */
	private BloquearFavorecidoEntradaDTO entradaBloquearFavorecido;
	
	/** Atributo motivoBloqueio. */
	private String motivoBloqueio;

	/** Atributo manterFavorecidoService. */
	private IManterFavorecidoService manterFavorecidoService;
	
	/** Atributo preencherTipoInscricao. */
	private List<SelectItem> preencherTipoInscricao;
	
	/** Atributo preencherTipoFavorecido. */
	private List<SelectItem> preencherTipoFavorecido;
	
	/** Atributo preencherMotivoBloqueoFavorecido. */
	private List<SelectItem> preencherMotivoBloqueoFavorecido;
	
	/** Atributo preencherMotivoBloqueContaFavorecido. */
	private List<SelectItem> preencherMotivoBloqueContaFavorecido;
	
	//Filtro Hist�rico
	/** Atributo itemFiltroHistoricoSelecionado. */
	private String itemFiltroHistoricoSelecionado;
	
	/** Atributo cdFavorecidoFiltroHistorico. */
	private Long  cdFavorecidoFiltroHistorico;
	
	/** Atributo cdInscricaoFavorecidoFiltroHistorico. */
	private Long cdInscricaoFavorecidoFiltroHistorico;
	
	/** Atributo tipoInscricaoFavorecidoFiltroHistorico. */
	private Integer tipoInscricaoFavorecidoFiltroHistorico;
	
	/** Atributo disableArgumentosConsultaHistorico. */
	private boolean disableArgumentosConsultaHistorico;
	
	/** Atributo periodoInicialHistorico. */
	private Date periodoInicialHistorico;
	
	/** Atributo periodoFinalHistorico. */
	private Date periodoFinalHistorico;
	
	/** Atributo cdPesquisaLista. */
	private Integer cdPesquisaLista;
	
	//Filtro Hist�rico Conta
	/** Atributo bancoHistoricoConta. */
	private Integer bancoHistoricoConta;
	
	/** Atributo agenciaHistoricoConta. */
	private Integer agenciaHistoricoConta;
	
	/** Atributo contaHistoricoConta. */
	private Long contaHistoricoConta;
	
	/** Atributo tipoContaHistoricoConta. */
	private Integer tipoContaHistoricoConta;
	
	/** Atributo periodoInicialHistoricoConta. */
	private Date periodoInicialHistoricoConta;
	
	/** Atributo periodoFinalHistoricoConta. */
	private Date periodoFinalHistoricoConta;
	
	/** Atributo disableArgumentosConsultaHistoricoConta. */
	private boolean disableArgumentosConsultaHistoricoConta;
	
	/** Atributo cdPesquisaListaConta. */
	private Integer cdPesquisaListaConta;
	
	/** Atributo preencherTipoConta. */
	private List<SelectItem> preencherTipoConta;

	/**
	 * Manter favorecido bean.
	 */
	public ManterFavorecidoBean() {
		super(CON_MANTER_FAVORECIDO ,"identificaoFavorecidosCliente","identificaoFavorecidosContrato");
	}
	
	/**
	 * Limpar inscricao.
	 */
	public void limparInscricao(){
	    this.getEntradaListarFavorecido().setCdInscricaoFavorecidoFormatado(null);
	}

	/**
	 * Inicializar variaveis.
	 */
	private void inicializarVariaveis() {
		this.setItemFiltroSelecionado(null);
		this.setDesabilatalimpar(true);
		this.setItemArgumentoPesquisa(null);
		this.setItemFavorecidoSelecionado(-1);
		this.setItemContaHistorico(0);
		this.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.setEntradaListarFavorecido(new ListarFavorecidoEntradaDTO());
		this.setEntradaBloquearFavorecido(new BloquearFavorecidoEntradaDTO());
		this.setEntradaBloquearContaFavorecidoEntrada(new BloquearContaFavorecidoEntradaDTO());
		this.setSaidaConsultarDetalheFavorecido(new ConsultarDetalheFavorecidoSaidaDTO());
		this.setSaidaDetalharHistoricoFavorecido(new DetalharHistoricoFavorecidoSaidaDTO());
		this.setSaidaConsultarListaContaFavorecido(new ConsultarListaContaFavorecidoSaidaDTO());
		this.setSaidaConsultarDetalheContaFavorecido(new ConsultarDetalheContaFavorecidoSaidaDTO());
		this.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this.setPreencherListaEmpresaGestora(new ArrayList<SelectItem>());
		this.setPreencherMotivoBloqueContaFavorecido(new ArrayList<SelectItem>());
		this.setPreencherMotivoBloqueoFavorecido(new ArrayList<SelectItem>());
		this.setPreencherTipoContrato(new ArrayList<SelectItem>());
		this.setPreencherTipoFavorecido(new ArrayList<SelectItem>());
		this.setPreencherTipoInscricao(new ArrayList<SelectItem>());
		this.setHabilitaFiltroDeContrato(true);
		this.setBloqueaTipoContrato(true);
		this.setDesabilataFiltro(true);
		setContratoSelecionado(false);
		setDsEmpresaGestoraContrato(null);
		this.setBloqueaRadio(false);
		this.loadEmpresaGestora();
		this.loadTipoContrato();
		this.loadTipoFavorecido();
		this.loadTipoInscricao();
		this.inicializarRadios();
		getEntradaConsultarListaContratosPessoas().setCdPessoaJuridicaContrato(2269651L);
	}

	/**
	 * Voltar favorecidos.
	 *
	 * @return the string
	 */
	public String voltarFavorecidos() {
		return CON_MANTER_FAVORECIDO ;
	}
	
	/**
	 * Inicializacao de campos favorecidos.
	 *
	 * @return the string
	 */
	public String inicializacaoDeCamposFavorecidos() {
		this.inicializarVariaveis();
		return CON_MANTER_FAVORECIDO ;
	}

	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos() {
		this.setEntradaListarFavorecido(new ListarFavorecidoEntradaDTO());
		this.setListaFavorecido(null);
		this.setDesabilataFiltro(false);
		setContratoSelecionado(true);
		this.setItemArgumentoPesquisa(null);
		this.setDesabilatalimpar(true);
		setItemSelecionadoListaFavorecidos(null);
		return CON_MANTER_FAVORECIDO ;
	}

	/**
	 * Pesquisar lista de favorecidos.
	 */
	public void pesquisarListaDeFavorecidos() {
		this.getEntradaListarFavorecido().setCdPessoaJuridicaContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		this.getEntradaListarFavorecido().setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		this.getEntradaListarFavorecido().setNrSequencialContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		
		if(this.getEntradaListarFavorecido().getCdInscricaoFavorecidoFormatado() != null){
        		String inscricaoFavorecido = this.getEntradaListarFavorecido().getCdInscricaoFavorecidoFormatado().replace(".", "");
        		inscricaoFavorecido = inscricaoFavorecido.replace("/", "");
        		inscricaoFavorecido = inscricaoFavorecido.replace("-", "");
        		this.getEntradaListarFavorecido().setCdInscricaoFavorecidoCliente(inscricaoFavorecido != null && !inscricaoFavorecido.equals("")?Long.parseLong(inscricaoFavorecido):0L);
		}
		this.setListaFavorecido(this.getManterFavorecidoService().pesquisarFavorecidos(this.getEntradaListarFavorecido()));
	}

	/**
	 * Consultar favorecidos.
	 *
	 * @return the string
	 */
	public String consultarFavorecidos() {
		try {
			this.pesquisarListaDeFavorecidos();
			this.setHabilitaFiltroDeContrato(this.getListaFavorecido() != null && this.getListaFavorecido().size() > 10 ? true : false);
			this.setBloqueaTipoContrato(true);
			this.setDesabilataFiltro(true);
			this.setDesabilatalimpar(false);
			return CON_MANTER_FAVORECIDO ;
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), MANTER_FAVORECIDO_JSP_ACESSO, BradescoViewExceptionActionType.PATH, false);
			setListaFavorecido(new ArrayList<ListarFavorecidoSaidaDTO>());
			return null;
		}
	}

	/**
	 * Voltar consulta favorecido.
	 *
	 * @return the string
	 */
	public String voltarConsultaFavorecido() {
		this.inicializarRadios();
		this.setItemSelecionadoListaFavorecidos(null);
		return CON_MANTER_FAVORECIDO ;
	}

	/**
	 * Submit lista de favorecidos.
	 *
	 * @param event the event
	 */
	public void submitListaDeFavorecidos(ActionEvent event) {
		this.pesquisarListaDeFavorecidos();
	}
	
	/**
	 * Inicializar radios.
	 *
	 * @return the string
	 */
	private String inicializarRadios() {
		this.setItemContaHistorico(-1);
		this.setItemContaSelecionada(-1);
		return null;
		
	}

	/**
	 * Limpar pagina.
	 *
	 * @return the string
	 */
	public String limparPagina() {
	    limparInscricao();
		inicializacaoDeCamposFavorecidos();
		limparLabel();
		this.setListaFavorecido(null);
		return CON_MANTER_FAVORECIDO ;
	}

	/**
	 * Detalhar favorecido.
	 *
	 * @return the string
	 */
	public String detalharFavorecido() {
		try {
			this.setSaidaListarFavorecido(this.getListaFavorecido().get(this.getItemSelecionadoListaFavorecidos()));

			ConsultarDetalheFavorecidoEntradaDTO entrada = new ConsultarDetalheFavorecidoEntradaDTO();

			entrada.setCdFavorecidoClientePagador(this.getListaFavorecido().get(this.getItemSelecionadoListaFavorecidos()).getCdFavorecidoClientePagador());
			entrada.setCdPessoaJuridicaContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			entrada.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());

			this.setSaidaConsultarDetalheFavorecido(this.getManterFavorecidoService().consultarDetalheFavorecido(entrada));

			return "detManterFavorecido";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "/detManterFavorecido.jsf", BradescoViewExceptionActionType.PATH, false);
			return "";
		}
	}

	/**
	 * Bloquear desbloquear favorecido.
	 *
	 * @return the string
	 */
	public String bloquearDesbloquearFavorecido() {
		this.setSaidaListarFavorecido(this.getListaFavorecido().get(this.getItemSelecionadoListaFavorecidos()));
		this.getEntradaBloquearFavorecido().setCdSituacaoFavorecido(null);
		this.getEntradaBloquearFavorecido().setCdMotivoBloqueioFavorecido(0);
		this.loadMotivoBloqueoFavorecido();
		return "bloDesbManterFavorecido";
	}

	/**
	 * Pesquisar lista historicos.
	 */
	private void pesquisarListaHistoricos() {
	    	setDisableArgumentosConsultaHistorico(false);
	    	
		ConsultarListaHistoricoFavorecidoEntradaDTO entrada = new ConsultarListaHistoricoFavorecidoEntradaDTO();

		entrada.setCdPessoaJuridicaContrato(getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		entrada.setCdTipoContratoNegocio(getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		entrada.setNrSequenciaContratoNegocio(getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
    		entrada.setCdFavorecidoClientePagador(getCdFavorecidoFiltroHistorico());		
    		entrada.setCdTipoIsncricaoFavorecido(getTipoInscricaoFavorecidoFiltroHistorico());
    		entrada.setCdInscricaoFavorecido(getCdInscricaoFavorecidoFiltroHistorico());
		entrada.setDataInicio(FormatarData.formataDiaMesAno(getPeriodoInicialHistorico()));
		entrada.setDataFim(FormatarData.formataDiaMesAno(getPeriodoFinalHistorico()));
		entrada.setCdPesquisaLista(cdPesquisaLista);			

		this.setListaConsultarHistoricoFavorecido(this.getManterFavorecidoService().consultarHistoricosFavorecidos(entrada));
		
	    	setDisableArgumentosConsultaHistorico(true);		
	}

	/**
	 * Voltar historico favorecido.
	 *
	 * @return the string
	 */
	public String voltarHistoricoFavorecido() {
		this.setItemSelecionadoListaHistorico(null);
		this.pesquisarListaHistoricos();
		return "histManterFavorecido";
	}

	/**
	 * Submit lista historicos.
	 *
	 * @param event the event
	 */
	public void submitListaHistoricos(ActionEvent event) {
		this.pesquisarListaHistoricos();
	}

	/**
	 * Limpar pagina historico.
	 */
	public void limparPaginaHistorico(){
        	setItemFiltroHistoricoSelecionado(null);
        	setCdPesquisaLista(2);
        	setListaConsultarHistoricoFavorecido(null);
        	limparCamposPesquisaHistorico();
	}
	
	/**
	 * Abrir consultar historico de favorecido.
	 *
	 * @return the string
	 */
	public String abrirConsultarHistoricoDeFavorecido(){
	    try {
        	    	limparPaginaHistorico();
        	    	
        	    	// SE O USU�RIO TIVER SELECIONADO UM FAVORECIDO ENT�O CONSULTA � REALIZADA COM FILTROS SELECIONADOS
        	    	if(getItemSelecionadoListaFavorecidos() != null){
        	    	    	setSaidaListarFavorecido(getListaFavorecido().get(getItemSelecionadoListaFavorecidos()));
        	    	    	setCdPesquisaLista(1);
        	        	setItemFiltroHistoricoSelecionado("0");
        	        	setCdFavorecidoFiltroHistorico(getSaidaListarFavorecido().getCdFavorecidoClientePagador());
        	        	setCdInscricaoFavorecidoFiltroHistorico(null);
        	        	setTipoInscricaoFavorecidoFiltroHistorico(null);
        	        	setPeriodoInicialHistorico(new Date());
        	        	setPeriodoFinalHistorico(new Date());
        	        	pesquisarListaHistoricos();
        	    	}
        	    	
        	} catch (PdcAdapterFunctionalException p) {
	    	    	setCdPesquisaLista(2);
        	    	BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
        	    	return "";
        	}
		return "histManterFavorecido";        	
	}
	
	/**
	 * Consultar historico de favorecido.
	 *
	 * @return the string
	 */
	public String consultarHistoricoDeFavorecido() {
	    	try {
       	        	pesquisarListaHistoricos();
        	} catch (PdcAdapterFunctionalException p) {
        	    	BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
        	}
		return null;        	
	}

	/**
	 * Detalhar historico favorecido.
	 *
	 * @return the string
	 */
	public String detalharHistoricoFavorecido() {
		try {
		    	setSaidaConsultarHistoricoFavorecido(getListaConsultarHistoricoFavorecido().get(getItemSelecionadoListaHistorico()));

			DetalharHistoricoFavorecidoEntradaDTO entrada = new DetalharHistoricoFavorecidoEntradaDTO();

			entrada.setCdFavorecidoClientePagador(getSaidaConsultarHistoricoFavorecido().getCdFavorecidoClientePagador());
			entrada.setCdPessoaJuridicaContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			entrada.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			entrada.setHrInclusaoRegistro(getSaidaConsultarHistoricoFavorecido().getHrInclusaoRegistro());

			this.setSaidaDetalharHistoricoFavorecido(this.getManterFavorecidoService().consultarDetalheHistoricoFavorecido(entrada));
			return "histDetManterFavorecido";
		} catch (PdcAdapterFunctionalException p) {
		    	BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
	}

	/**
	 * Pesquisar conta favorecidos.
	 */
	public void pesquisarContaFavorecidos() {
		ConsultarListaContaFavorecidoEntradaDTO entrada = new ConsultarListaContaFavorecidoEntradaDTO();

		entrada.setCdFavorecidoClientePagador(this.getSaidaListarFavorecido().getCdFavorecidoClientePagador());
		entrada.setCdPessoaJuridContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		entrada.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		entrada.setNrSeqContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());

		this.setListaConsultarListaContaFavorecido(this.getManterFavorecidoService().consultarContaFavorecido(entrada));
	}

	/**
	 * Consultar contas de favorecido.
	 *
	 * @return the string
	 */
	public String consultarContasDeFavorecido() {
		this.inicializarRadios();
		try {
			this.setSaidaListarFavorecido(this.getListaFavorecido().get(this.getItemSelecionadoListaFavorecidos()));
			this.pesquisarContaFavorecidos();
			return "contPesqManterFavorecido";
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), MANTER_FAVORECIDO_JSP_ACESSO, BradescoViewExceptionActionType.PATH, false);
			return null;
		}
	}

	/**
	 * Submit conta favorecido.
	 *
	 * @param event the event
	 */
	public void submitContaFavorecido(ActionEvent event) {
		this.pesquisarContaFavorecidos();
	}
	
	/**
	 * Limpar pagina historico conta.
	 */
	public void limparPaginaHistoricoConta(){
    		setDisableArgumentosConsultaHistoricoConta(false);
    	
        	setCdPesquisaListaConta(2);
        	
        	setItemContaHistorico(null);        	
        	setListaConsultarListaHistoricoContaFavorecido(null);
        	
        	setBancoHistoricoConta(null);
        	setAgenciaHistoricoConta(null);
        	setContaHistoricoConta(null);
        	setTipoContaHistoricoConta(null);
        	setPeriodoInicialHistoricoConta(new Date());
        	setPeriodoFinalHistoricoConta(new Date());        	
	}	
	
	/**
	 * Submit lista hist conta de favorecidos.
	 *
	 * @param event the event
	 */
	public void submitListaHistContaDeFavorecidos(ActionEvent event) {
		this.pesquisarHistoricoDeContasFavorecidos();
	}

	/**
	 * Pesquisar historico de contas favorecidos.
	 */
	public void pesquisarHistoricoDeContasFavorecidos() {
		ConsultarListaHistoricoContaFavorecidoEntradaDTO entrada = new ConsultarListaHistoricoContaFavorecidoEntradaDTO();

		entrada.setCdFavorecidoClientePagador(this.getSaidaListarFavorecido().getCdFavorecidoClientePagador());
		entrada.setCdPessoaJuridicaContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		entrada.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		entrada.setNrSequenciaContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		entrada.setNrOcorrenciaContaFavorecido(getCdPesquisaListaConta() == 1 && getSaidaConsultarListaContaFavorecido()!= null && getSaidaConsultarListaContaFavorecido().getNrOcorrenciaContaFavorecido() != null ? this.getSaidaConsultarListaContaFavorecido().getNrOcorrenciaContaFavorecido() : 0);
		entrada.setCdBanco(getBancoHistoricoConta());
		entrada.setCdAgencia(getAgenciaHistoricoConta());
		entrada.setCdDigitoAgencia(0);
		entrada.setCdConta(getContaHistoricoConta());
		entrada.setCdDigitoConta("00");
		entrada.setCdTipoConta(getTipoContaHistoricoConta());
		entrada.setDataInicio(FormatarData.formataDiaMesAno(getPeriodoInicialHistoricoConta()));
		entrada.setDataFim(FormatarData.formataDiaMesAno(getPeriodoFinalHistoricoConta()));
		entrada.setCdPesquisaLista(getCdPesquisaListaConta());	
		
		this.setListaConsultarListaHistoricoContaFavorecido(this.getManterFavorecidoService().consultarHistoricosContaFavorecido(entrada));
		
		setDisableArgumentosConsultaHistoricoConta(true);
	}
	
	/**
	 * Voltar conta historico.
	 *
	 * @return the string
	 */
	public String voltarContaHistorico(){
		this.pesquisarHistoricoDeContasFavorecidos();
		this.inicializarRadios();
		return "contHistPesqManterFavorecido";
	}
	
	/**
	 * Abrir consultar historico de contas favorecido.
	 *
	 * @return the string
	 */
	public String abrirConsultarHistoricoDeContasFavorecido() {	
	    	try {
        	    	limparPaginaHistoricoConta();
	        	loadTipoConta();
        	    	
        	    	// SE O USU�RIO TIVER SELECIONADO UM FAVORECIDO ENT�O CONSULTA � REALIZADA COM FILTROS SELECIONADOS
        	    	if(getItemContaSelecionada() != null){
        	    	    	setSaidaConsultarListaContaFavorecido(this.getListaConsultarListaContaFavorecido().get(this.getItemContaSelecionada()));
        	    	    	setCdPesquisaListaConta(1);
        	    	    	
        	        	setCdFavorecidoFiltroHistorico(getSaidaListarFavorecido().getCdFavorecidoClientePagador());
        	        	setCdInscricaoFavorecidoFiltroHistorico(null);
        	        	setTipoInscricaoFavorecidoFiltroHistorico(null);
        	        	setPeriodoInicialHistorico(new Date());
        	        	setPeriodoFinalHistorico(new Date());
        	        	
        	        	setBancoHistoricoConta(getSaidaConsultarListaContaFavorecido().getCdBanco());
        	        	setAgenciaHistoricoConta(getSaidaConsultarListaContaFavorecido().getCdAgencia());
        	        	setContaHistoricoConta(getSaidaConsultarListaContaFavorecido().getNrContaFavorecido());
        	        	setTipoContaHistoricoConta(getSaidaConsultarListaContaFavorecido().getCdTipoConta());
        	        	setPeriodoInicialHistoricoConta(new Date());
        	        	setPeriodoFinalHistoricoConta(new Date());       	        	
        	        	
        	        	pesquisarHistoricoDeContasFavorecidos();
        	    	}
        	    	else{
        	    	    setBancoHistoricoConta(237);
        	    	}
        	} catch (PdcAdapterFunctionalException p) {
            	    	setCdPesquisaListaConta(2);
        	    	BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
        	    	return ""; 
        	}
        	return "contHistPesqManterFavorecido";     
	}
	
	/**
	 * Load tipo conta.
	 */
	private void loadTipoConta() {
		try{
		    	setPreencherTipoConta(new ArrayList<SelectItem>());		    
			List<ListarTipoContaSaidaDTO> listSaida = this.getComboService().listarTipoConta();
	
			for (ListarTipoContaSaidaDTO saida : listSaida){
			    getPreencherTipoConta().add(new SelectItem(saida.getCdTipoConta(), saida.getDsTipoConta()));
			}
		}catch(PdcAdapterFunctionalException e){
		    	setPreencherTipoConta(new ArrayList<SelectItem>());
		}

	}
	
	/**
	 * Consultar historico de contas favorecido.
	 *
	 * @return the string
	 */
	public String consultarHistoricoDeContasFavorecido() {
	    	try {
	    	    pesquisarHistoricoDeContasFavorecidos();
        	} catch (PdcAdapterFunctionalException p) {
        	    	BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
        	}
		return null; 		
	}

	/**
	 * Bloquear desbloquear conta favorecido.
	 *
	 * @return the string
	 */
	public String bloquearDesbloquearContaFavorecido() {
		this.setSaidaConsultarListaContaFavorecido(this.getListaConsultarListaContaFavorecido().get(this.getItemContaSelecionada()));
		this.getEntradaBloquearContaFavorecidoEntrada().setCdSituacaoContrato(null);
		this.getEntradaBloquearContaFavorecidoEntrada().setCdMotivoBloqueioContrato(0);
		this.loadMotivoBloqueoContaFavorecido();
		return "contBloqDesbManterFavorecido";
	}
	
	/**
	 * Avancar bloq desbloq favorecido.
	 *
	 * @return the string
	 */
	public String avancarBloqDesbloqFavorecido() {
		if (this.getPreencherMotivoBloqueoFavorecido() != null) {
			if (this.getEntradaBloquearFavorecido().getCdSituacaoFavorecido().equals(1)) {
				this.setMotivoBloqueio("");
				this.getEntradaBloquearFavorecido().setCdMotivoBloqueioFavorecido(0);
			} else {
				for (SelectItem element : this.getPreencherMotivoBloqueoFavorecido()) {
					if (element.getValue().equals(this.getEntradaBloquearFavorecido().getCdMotivoBloqueioFavorecido())) {
						this.setMotivoBloqueio(element.getLabel());
						break;
					}
				}
			}
		}

		return "bloqDesbConfManterFavorecido";
	}

	/**
	 * Voltar bloq desbloq favorecido.
	 *
	 * @return the string
	 */
	public String voltarBloqDesbloqFavorecido() {
		return "bloDesbManterFavorecido";
	}

	/**
	 * Confirmar bloq desbloq favorecido.
	 *
	 * @return the string
	 */
	public String confirmarBloqDesbloqFavorecido() {
		try {
			this.getEntradaBloquearFavorecido().setCdFavoritoClientePagador(this.getSaidaListarFavorecido().getCdFavorecidoClientePagador());
			this.getEntradaBloquearFavorecido().setCdPessoaJuridicaContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			this.getEntradaBloquearFavorecido().setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			this.getEntradaBloquearFavorecido().setNrSequencialContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());

			BloquearFavorecidoSaidaDTO saida = this.getManterFavorecidoService().confirmarBloquearFavorecido(this.getEntradaBloquearFavorecido());

			this.pesquisarListaDeFavorecidos();
			
			BradescoFacesUtils.addInfoModalMessage("( " + saida.getCodMensagem()  + " ) " + saida.getMensagem(), MANTER_FAVORECIDO_JSP_ACESSO, BradescoViewExceptionActionType.PATH, false);
			return null;
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "/bloDesbManterFavorecido.jsf", BradescoViewExceptionActionType.PATH, false);
			return null;
		}
	}

	/**
	 * Get: preencherBloqueoDesbloqueo.
	 *
	 * @return preencherBloqueoDesbloqueo
	 */
	public List<SelectItem> getPreencherBloqueoDesbloqueo() {
		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(Integer.parseInt("2"), "Bloquear"));
		list.add(new SelectItem(Integer.parseInt("1"), "Desbloquear"));
		return list;
	}

	/**
	 * Detalhar conta favorecido.
	 *
	 * @return the string
	 */
	public String detalharContaFavorecido() {
		try {
			this.setSaidaConsultarListaContaFavorecido(this.getListaConsultarListaContaFavorecido().get(this.getItemContaSelecionada()));

			ConsultarDetalheContaFavorecidoEntradaDTO entrada = new ConsultarDetalheContaFavorecidoEntradaDTO();

			entrada.setCdFavorecidoClientePagador(this.getSaidaListarFavorecido().getCdFavorecidoClientePagador());
			entrada.setCdPessoaJuridicaContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			entrada.setNrSequenciaContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			entrada.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			entrada.setNrOcorrenciaContaFavorecido(this.getListaConsultarListaContaFavorecido().get(this.getItemContaSelecionada()).getNrOcorrenciaContaFavorecido());
			entrada.setCdTipoContaFavorecido(this.getListaConsultarListaContaFavorecido().get(this.getItemContaSelecionada()).getCdTipoConta());

			this.setSaidaConsultarDetalheContaFavorecido(this.getManterFavorecidoService().consultarDetalheContaFavorecido(entrada));

			return "contDetManterFavorecido";
		} catch (PdcAdapterFunctionalException p) {
		    	BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
	}

	/**
	 * Avancar bloquear desbloquear conta favorecido.
	 *
	 * @return the string
	 */
	public String avancarBloquearDesbloquearContaFavorecido() {
		if (this.getPreencherMotivoBloqueContaFavorecido() != null) {
			if (this.getEntradaBloquearContaFavorecidoEntrada().getCdSituacaoContrato().equals(1)) {
				this.setMotivoBloqueioConta("");
				this.getEntradaBloquearContaFavorecidoEntrada().setCdMotivoBloqueioContrato(0);
			} else {
				for (SelectItem element : this.getPreencherMotivoBloqueContaFavorecido()) {
					if (element.getValue().equals(this.getEntradaBloquearContaFavorecidoEntrada().getCdMotivoBloqueioContrato())) {
						this.setMotivoBloqueioConta(element.getLabel());
						break;
					}
				}
			}
		}
		
		return "contBloqDesbConfManterFavorecido";
	}

	/**
	 * Voltar bloq desbloq conta favorecido.
	 *
	 * @return the string
	 */
	public String voltarBloqDesbloqContaFavorecido() {
		return "contBloqDesbManterFavorecido";
	}
	
	/**
	 * Confirmar bloq desbloq conta favorecido.
	 *
	 * @return the string
	 */
	public String confirmarBloqDesbloqContaFavorecido() {
		try {
			this.getEntradaBloquearContaFavorecidoEntrada().setCdFavorecidoClientePagador(this.getSaidaListarFavorecido().getCdFavorecidoClientePagador());
			this.getEntradaBloquearContaFavorecidoEntrada().setCdPessoaJuridicaContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			this.getEntradaBloquearContaFavorecidoEntrada().setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			this.getEntradaBloquearContaFavorecidoEntrada().setNrSequenciaContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			this.getEntradaBloquearContaFavorecidoEntrada().setNrOcorrenciaContaFavorecido(this.getSaidaConsultarListaContaFavorecido().getNrOcorrenciaContaFavorecido());
			
			BloquearContaFavorecidoSaidaDTO saida = this.getManterFavorecidoService().confirmarBloqueoContaFavorecido(this.getEntradaBloquearContaFavorecidoEntrada());

			this.pesquisarContaFavorecidos();
			
			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(), CON_HIST_PESQ_MANTER_FAVORECIDO_JSP_ACESSO, BradescoViewExceptionActionType.PATH, false);
			return null;
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "/contBloqDesbManterFavorecido.jsf", BradescoViewExceptionActionType.PATH, false);
			return null;
		}
	}
	
	/**
	 * Situacao.
	 *
	 * @return the string
	 */
	public String situacao(){
		
		this.setCdSituacao(saidaConsultarDetalheHistoricoContaFavorecido.getCdSituacaoContaFavorecido());
		if (this.getCdSituacao() == 1){
			this.setDsSituacao("LIBERADA");
		}
		if (this.getCdSituacao() == 2){
			this.setDsSituacao("BLOQUEADA");
		}
		if (this.getCdSituacao() == 3){
			this.setDsSituacao("ENCERRADA");
		}
		if (this.getCdSituacao() == 4){
			this.setDsSituacao("EXCLUIDA");
		}
		
		return null;
	}

	/**
	 * Detalhar historico de conta favorecido.
	 *
	 * @return the string
	 */
	public String detalharHistoricoDeContaFavorecido() {
		
		try {
		    	setSaidaConsultarListaHistoricoContaFavorecido(getListaConsultarListaHistoricoContaFavorecido().get(getItemContaHistorico()));
		    	
			ConsultarDetalheHistoricoContaFavorecidoEntradaDTO entrada = new ConsultarDetalheHistoricoContaFavorecidoEntradaDTO();

			entrada.setCdFavorecidoClientePagador(this.getSaidaListarFavorecido().getCdFavorecidoClientePagador());
			entrada.setCdPessoaJuridicaContrato(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			entrada.setCdTipoContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			entrada.setHrInclusaoRegistro(this.getListaConsultarListaHistoricoContaFavorecido().get(this.getItemContaHistorico()).getHrInclusaoRegistro());
			entrada.setNrOcorrenciaContaFavorecido(getSaidaConsultarListaHistoricoContaFavorecido().getNrOcorrenciaConta());
			
			this.setSaidaConsultarDetalheHistoricoContaFavorecido(this.getManterFavorecidoService().consultarDetalheHistoricoContaFavorecido(entrada));			
			situacao();
			
			return "contHistDetManterFavorecido";
			
		} catch (PdcAdapterFunctionalException p) {
		    	setItemContaHistorico(null);
		    	BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}			
		
	}

	/**
	 * Get: selectItemFavorecido.
	 *
	 * @return selectItemFavorecido
	 */
	public List<SelectItem> getSelectItemFavorecido() {
		if (this.getListaFavorecido() == null || this.getListaFavorecido().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.getListaFavorecido().size(); index++){
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/**
	 * Get: selectItemHistoricoFavorido.
	 *
	 * @return selectItemHistoricoFavorido
	 */
	public List<SelectItem> getSelectItemHistoricoFavorido() {
		if (this.getListaConsultarHistoricoFavorecido() == null || this.getListaConsultarHistoricoFavorecido().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.getListaConsultarHistoricoFavorecido().size(); index++){
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/**
	 * Get: selectItemContasFavorido.
	 *
	 * @return selectItemContasFavorido
	 */
	public List<SelectItem> getSelectItemContasFavorido() {
		if (this.getListaConsultarListaContaFavorecido() == null || this.getListaConsultarListaContaFavorecido().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.getListaConsultarListaContaFavorecido().size(); index++){
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}
	
	/**
	 * Get: selectItemHistoricoCotaFavorecido.
	 *
	 * @return selectItemHistoricoCotaFavorecido
	 */
	public List<SelectItem> getSelectItemHistoricoCotaFavorecido() {
		if (this.getListaConsultarListaHistoricoContaFavorecido() == null || this.getListaConsultarListaHistoricoContaFavorecido().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.getListaConsultarListaHistoricoContaFavorecido().size(); index++){
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/**
	 * Load tipo inscricao.
	 */
	private void loadTipoInscricao() {
		try{
			List<InscricaoFavorecidosSaidaDTO> listSaida = this.getComboService().listarTipoInscricao();
	
			for (InscricaoFavorecidosSaidaDTO saida : listSaida){
				preencherTipoInscricao.add(new SelectItem(saida.getCdTipoInscricaoFavorecidos(), saida.getDsTipoInscricaoFavorecidos()));
			}
		}catch(PdcAdapterFunctionalException e){
			preencherTipoInscricao = new ArrayList<SelectItem>();
		}

	}

	/**
	 * Get: preencherTipoInscricao.
	 *
	 * @return preencherTipoInscricao
	 */
	public List<SelectItem> getPreencherTipoInscricao() {
		return preencherTipoInscricao;
	}

	/**
	 * Load tipo favorecido.
	 */
	private void loadTipoFavorecido() {
		try{
			List<TipoFavorecidoSaidaDTO> listSaida = this.getComboService().listarTipoFavorecido();
	
			for (TipoFavorecidoSaidaDTO saida : listSaida){
				preencherTipoFavorecido.add(new SelectItem(saida.getCdTipoFavorecidos(), saida.getDsTipoFavorecidos()));
			}
		}catch(PdcAdapterFunctionalException e){
			preencherTipoFavorecido = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Get: preencherTipoFavorecido.
	 *
	 * @return preencherTipoFavorecido
	 */
	public List<SelectItem> getPreencherTipoFavorecido() {
		return preencherTipoFavorecido;
	}

	/**
	 * Load motivo bloqueo favorecido.
	 */
	private void loadMotivoBloqueoFavorecido() {
		List<ListarMotivoBloqueioFavorecidoSaidaDTO> listSaida = this.getComboService().listarMotivoBloqueioFavorecido();

		preencherMotivoBloqueoFavorecido = new ArrayList<SelectItem>();
		
		for (ListarMotivoBloqueioFavorecidoSaidaDTO saida : listSaida){
			preencherMotivoBloqueoFavorecido.add(new SelectItem(saida.getCdMotivoBloqueioFavorecido(), saida.getDtMotivoBloqueioFavorecido()));
		}
	}
	
	/**
	 * Load motivo bloqueo conta favorecido.
	 */
	private void loadMotivoBloqueoContaFavorecido() {
		List<ConsultarMotivoBloqueioContaSaidaDTO> listSaida = this.getComboService().listarMotivoBloqueioContaFavorecido();

		preencherMotivoBloqueContaFavorecido = new ArrayList<SelectItem>();
		
		for (ConsultarMotivoBloqueioContaSaidaDTO saida : listSaida){
			preencherMotivoBloqueContaFavorecido.add(new SelectItem(saida.getCdMotivoBloqueioConta(), saida.getDsMotivoBloqueioConta()));
		}
	}

	/**
	 * Inicializa campos.
	 */
	public void inicializaCampos(){
		if (this.getEntradaBloquearFavorecido().getCdSituacaoFavorecido().equals(1)) {
			this.getEntradaBloquearFavorecido().setCdMotivoBloqueioFavorecido(0);
		}
	}
	
	/**
	 * Inicializa campos contas.
	 */
	public void inicializaCamposContas(){
		if (this.getEntradaBloquearContaFavorecidoEntrada().getCdSituacaoContrato().equals(1)) {
			this.getEntradaBloquearContaFavorecidoEntrada().setCdMotivoBloqueioContrato(0);
		}
	}
	
	/**
	 * Limpar campos filtro favorecidos.
	 */
	public void limparCamposFiltroFavorecidos () {
		if (getItemArgumentoPesquisa()!= null && this.getItemArgumentoPesquisa().equals("0")) {
			this.getEntradaListarFavorecido().setCdFavorecidoClientePagador(null);
			this.getEntradaListarFavorecido().setCdInscricaoFavorecidoCliente(null);
			this.getEntradaListarFavorecido().setCdTipoInscricaoFavorecido(null);
		}else{

        		if (this.getItemArgumentoPesquisa().equals("1")) {
        			this.getEntradaListarFavorecido().setNmFavorecido(null);
        			this.getEntradaListarFavorecido().setCdInscricaoFavorecidoCliente(null);
        			this.getEntradaListarFavorecido().setCdTipoInscricaoFavorecido(null);
        		}
        
        		if (this.getItemArgumentoPesquisa().equals("2")) {
        			this.getEntradaListarFavorecido().setNmFavorecido(null);
        			this.getEntradaListarFavorecido().setCdFavorecidoClientePagador(null);
        		}
		}
	}
	
	/**
	 * Limpar campos pesquisa historico.
	 */
	public void limparCamposPesquisaHistorico(){
            	setCdFavorecidoFiltroHistorico(null);
            	setCdInscricaoFavorecidoFiltroHistorico(null);
            	setTipoInscricaoFavorecidoFiltroHistorico(null);
            	setDisableArgumentosConsultaHistorico(false);
            	setPeriodoInicialHistorico(new Date());
            	setPeriodoFinalHistorico(new Date());     		
	}
	
	/**
	 * Get: entradaBloquearFavorecido.
	 *
	 * @return entradaBloquearFavorecido
	 */
	public BloquearFavorecidoEntradaDTO getEntradaBloquearFavorecido() {
		return entradaBloquearFavorecido;
	}

	/**
	 * Set: entradaBloquearFavorecido.
	 *
	 * @param entradaBloquearFavorecido the entrada bloquear favorecido
	 */
	public void setEntradaBloquearFavorecido(BloquearFavorecidoEntradaDTO entradaBloquearFavorecido) {
		this.entradaBloquearFavorecido = entradaBloquearFavorecido;
	}

	/**
	 * Get: entradaListarFavorecido.
	 *
	 * @return entradaListarFavorecido
	 */
	public ListarFavorecidoEntradaDTO getEntradaListarFavorecido() {
		return entradaListarFavorecido;
	}

	/**
	 * Set: entradaListarFavorecido.
	 *
	 * @param entradaListarFavorecido the entrada listar favorecido
	 */
	public void setEntradaListarFavorecido(ListarFavorecidoEntradaDTO entradaListarFavorecido) {
		this.entradaListarFavorecido = entradaListarFavorecido;
	}

	/**
	 * Get: itemArgumentoPesquisa.
	 *
	 * @return itemArgumentoPesquisa
	 */
	public String getItemArgumentoPesquisa() {
		return itemArgumentoPesquisa;
	}

	/**
	 * Set: itemArgumentoPesquisa.
	 *
	 * @param itemArgumentoPesquisa the item argumento pesquisa
	 */
	public void setItemArgumentoPesquisa(String itemArgumentoPesquisa) {
		this.itemArgumentoPesquisa = itemArgumentoPesquisa;
	}

	/**
	 * Get: itemContaSelecionada.
	 *
	 * @return itemContaSelecionada
	 */
	public Integer getItemContaSelecionada() {
		return itemContaSelecionada;
	}

	/**
	 * Set: itemContaSelecionada.
	 *
	 * @param itemContaSelecionada the item conta selecionada
	 */
	public void setItemContaSelecionada(Integer itemContaSelecionada) {
		this.itemContaSelecionada = itemContaSelecionada;
	}

	/**
	 * Get: itemFavorecidoSelecionado.
	 *
	 * @return itemFavorecidoSelecionado
	 */
	public Integer getItemFavorecidoSelecionado() {
		return itemFavorecidoSelecionado;
	}

	/**
	 * Set: itemFavorecidoSelecionado.
	 *
	 * @param itemFavorecidoSelecionado the item favorecido selecionado
	 */
	public void setItemFavorecidoSelecionado(Integer itemFavorecidoSelecionado) {
		this.itemFavorecidoSelecionado = itemFavorecidoSelecionado;
	}

	/**
	 * Get: itemSelecionadoListaFavorecidos.
	 *
	 * @return itemSelecionadoListaFavorecidos
	 */
	public Integer getItemSelecionadoListaFavorecidos() {
		return itemSelecionadoListaFavorecidos;
	}

	/**
	 * Set: itemSelecionadoListaFavorecidos.
	 *
	 * @param itemSelecionadoListaFavorecidos the item selecionado lista favorecidos
	 */
	public void setItemSelecionadoListaFavorecidos(Integer itemSelecionadoListaFavorecidos) {
		this.itemSelecionadoListaFavorecidos = itemSelecionadoListaFavorecidos;
	}

	/**
	 * Get: itemSelecionadoListaHistorico.
	 *
	 * @return itemSelecionadoListaHistorico
	 */
	public Integer getItemSelecionadoListaHistorico() {
		return itemSelecionadoListaHistorico;
	}

	/**
	 * Set: itemSelecionadoListaHistorico.
	 *
	 * @param itemSelecionadoListaHistorico the item selecionado lista historico
	 */
	public void setItemSelecionadoListaHistorico(Integer itemSelecionadoListaHistorico) {
		this.itemSelecionadoListaHistorico = itemSelecionadoListaHistorico;
	}

	/**
	 * Get: listaConsultarHistoricoFavorecido.
	 *
	 * @return listaConsultarHistoricoFavorecido
	 */
	public List<ConsultarListaHistoricoFavorecidoSaidaDTO> getListaConsultarHistoricoFavorecido() {
		return listaConsultarHistoricoFavorecido;
	}

	/**
	 * Set: listaConsultarHistoricoFavorecido.
	 *
	 * @param listaConsultarHistoricoFavorecido the lista consultar historico favorecido
	 */
	public void setListaConsultarHistoricoFavorecido(List<ConsultarListaHistoricoFavorecidoSaidaDTO> listaConsultarHistoricoFavorecido) {
		this.listaConsultarHistoricoFavorecido = listaConsultarHistoricoFavorecido;
	}

	/**
	 * Get: listaConsultarListaContaFavorecido.
	 *
	 * @return listaConsultarListaContaFavorecido
	 */
	public List<ConsultarListaContaFavorecidoSaidaDTO> getListaConsultarListaContaFavorecido() {
		return listaConsultarListaContaFavorecido;
	}

	/**
	 * Set: listaConsultarListaContaFavorecido.
	 *
	 * @param listaConsultarListaContaFavorecido the lista consultar lista conta favorecido
	 */
	public void setListaConsultarListaContaFavorecido(List<ConsultarListaContaFavorecidoSaidaDTO> listaConsultarListaContaFavorecido) {
		this.listaConsultarListaContaFavorecido = listaConsultarListaContaFavorecido;
	}

	/**
	 * Get: listaFavorecido.
	 *
	 * @return listaFavorecido
	 */
	public List<ListarFavorecidoSaidaDTO> getListaFavorecido() {
		return listaFavorecido;
	}

	/**
	 * Set: listaFavorecido.
	 *
	 * @param listaFavorecido the lista favorecido
	 */
	public void setListaFavorecido(List<ListarFavorecidoSaidaDTO> listaFavorecido) {
		this.listaFavorecido = listaFavorecido;
	}

	/**
	 * Get: manterFavorecidoService.
	 *
	 * @return manterFavorecidoService
	 */
	public IManterFavorecidoService getManterFavorecidoService() {
		return manterFavorecidoService;
	}

	/**
	 * Set: manterFavorecidoService.
	 *
	 * @param manterFavorecidoService the manter favorecido service
	 */
	public void setManterFavorecidoService(IManterFavorecidoService manterFavorecidoService) {
		this.manterFavorecidoService = manterFavorecidoService;
	}

	/**
	 * Get: motivoBloqueio.
	 *
	 * @return motivoBloqueio
	 */
	public String getMotivoBloqueio() {
		return motivoBloqueio;
	}

	/**
	 * Set: motivoBloqueio.
	 *
	 * @param motivoBloqueio the motivo bloqueio
	 */
	public void setMotivoBloqueio(String motivoBloqueio) {
		this.motivoBloqueio = motivoBloqueio;
	}

	/**
	 * Get: preencherMotivoBloqueContaFavorecido.
	 *
	 * @return preencherMotivoBloqueContaFavorecido
	 */
	public List<SelectItem> getPreencherMotivoBloqueContaFavorecido() {
		return preencherMotivoBloqueContaFavorecido;
	}

	/**
	 * Set: preencherMotivoBloqueContaFavorecido.
	 *
	 * @param preencherMotivoBloqueContaFavorecido the preencher motivo bloque conta favorecido
	 */
	public void setPreencherMotivoBloqueContaFavorecido(List<SelectItem> preencherMotivoBloqueContaFavorecido) {
		this.preencherMotivoBloqueContaFavorecido = preencherMotivoBloqueContaFavorecido;
	}

	/**
	 * Get: preencherMotivoBloqueoFavorecido.
	 *
	 * @return preencherMotivoBloqueoFavorecido
	 */
	public List<SelectItem> getPreencherMotivoBloqueoFavorecido() {
		return preencherMotivoBloqueoFavorecido;
	}

	/**
	 * Set: preencherMotivoBloqueoFavorecido.
	 *
	 * @param preencherMotivoBloqueoFavorecido the preencher motivo bloqueo favorecido
	 */
	public void setPreencherMotivoBloqueoFavorecido(List<SelectItem> preencherMotivoBloqueoFavorecido) {
		this.preencherMotivoBloqueoFavorecido = preencherMotivoBloqueoFavorecido;
	}

	/**
	 * Get: saidaConsultarDetalheContaFavorecido.
	 *
	 * @return saidaConsultarDetalheContaFavorecido
	 */
	public ConsultarDetalheContaFavorecidoSaidaDTO getSaidaConsultarDetalheContaFavorecido() {
		return saidaConsultarDetalheContaFavorecido;
	}

	/**
	 * Set: saidaConsultarDetalheContaFavorecido.
	 *
	 * @param saidaConsultarDetalheContaFavorecido the saida consultar detalhe conta favorecido
	 */
	public void setSaidaConsultarDetalheContaFavorecido(ConsultarDetalheContaFavorecidoSaidaDTO saidaConsultarDetalheContaFavorecido) {
		this.saidaConsultarDetalheContaFavorecido = saidaConsultarDetalheContaFavorecido;
	}

	/**
	 * Get: saidaConsultarDetalheFavorecido.
	 *
	 * @return saidaConsultarDetalheFavorecido
	 */
	public ConsultarDetalheFavorecidoSaidaDTO getSaidaConsultarDetalheFavorecido() {
		return saidaConsultarDetalheFavorecido;
	}

	/**
	 * Set: saidaConsultarDetalheFavorecido.
	 *
	 * @param saidaConsultarDetalheFavorecido the saida consultar detalhe favorecido
	 */
	public void setSaidaConsultarDetalheFavorecido(ConsultarDetalheFavorecidoSaidaDTO saidaConsultarDetalheFavorecido) {
		this.saidaConsultarDetalheFavorecido = saidaConsultarDetalheFavorecido;
	}

	/**
	 * Get: saidaConsultarListaContaFavorecido.
	 *
	 * @return saidaConsultarListaContaFavorecido
	 */
	public ConsultarListaContaFavorecidoSaidaDTO getSaidaConsultarListaContaFavorecido() {
		return saidaConsultarListaContaFavorecido;
	}

	/**
	 * Set: saidaConsultarListaContaFavorecido.
	 *
	 * @param saidaConsultarListaContaFavorecido the saida consultar lista conta favorecido
	 */
	public void setSaidaConsultarListaContaFavorecido(ConsultarListaContaFavorecidoSaidaDTO saidaConsultarListaContaFavorecido) {
		this.saidaConsultarListaContaFavorecido = saidaConsultarListaContaFavorecido;
	}

	/**
	 * Get: saidaDetalharHistoricoFavorecido.
	 *
	 * @return saidaDetalharHistoricoFavorecido
	 */
	public DetalharHistoricoFavorecidoSaidaDTO getSaidaDetalharHistoricoFavorecido() {
		return saidaDetalharHistoricoFavorecido;
	}

	/**
	 * Set: saidaDetalharHistoricoFavorecido.
	 *
	 * @param saidaDetalharHistoricoFavorecido the saida detalhar historico favorecido
	 */
	public void setSaidaDetalharHistoricoFavorecido(DetalharHistoricoFavorecidoSaidaDTO saidaDetalharHistoricoFavorecido) {
		this.saidaDetalharHistoricoFavorecido = saidaDetalharHistoricoFavorecido;
	}

	/**
	 * Get: saidaListarFavorecido.
	 *
	 * @return saidaListarFavorecido
	 */
	public ListarFavorecidoSaidaDTO getSaidaListarFavorecido() {
		return saidaListarFavorecido;
	}

	/**
	 * Set: saidaListarFavorecido.
	 *
	 * @param saidaListarFavorecido the saida listar favorecido
	 */
	public void setSaidaListarFavorecido(ListarFavorecidoSaidaDTO saidaListarFavorecido) {
		this.saidaListarFavorecido = saidaListarFavorecido;
	}

	/**
	 * Set: preencherTipoFavorecido.
	 *
	 * @param preencherTipoFavorecido the preencher tipo favorecido
	 */
	public void setPreencherTipoFavorecido(List<SelectItem> preencherTipoFavorecido) {
		this.preencherTipoFavorecido = preencherTipoFavorecido;
	}

	/**
	 * Set: preencherTipoInscricao.
	 *
	 * @param preencherTipoInscricao the preencher tipo inscricao
	 */
	public void setPreencherTipoInscricao(List<SelectItem> preencherTipoInscricao) {
		this.preencherTipoInscricao = preencherTipoInscricao;
	}

	/**
	 * Get: listaConsultarListaHistoricoContaFavorecido.
	 *
	 * @return listaConsultarListaHistoricoContaFavorecido
	 */
	public List<ConsultarListaHistoricoContaFavorecidoSaidaDTO> getListaConsultarListaHistoricoContaFavorecido() {
		return listaConsultarListaHistoricoContaFavorecido;
	}

	/**
	 * Set: listaConsultarListaHistoricoContaFavorecido.
	 *
	 * @param listaConsultarListaHistoricoContaFavorecido the lista consultar lista historico conta favorecido
	 */
	public void setListaConsultarListaHistoricoContaFavorecido(List<ConsultarListaHistoricoContaFavorecidoSaidaDTO> listaConsultarListaHistoricoContaFavorecido) {
		this.listaConsultarListaHistoricoContaFavorecido = listaConsultarListaHistoricoContaFavorecido;
	}

	/**
	 * Get: saidaConsultarDetalheHistoricoContaFavorecido.
	 *
	 * @return saidaConsultarDetalheHistoricoContaFavorecido
	 */
	public ConsultarDetalheHistoricoContaFavorecidoSaidaDTO getSaidaConsultarDetalheHistoricoContaFavorecido() {
		return saidaConsultarDetalheHistoricoContaFavorecido;
	}

	/**
	 * Set: saidaConsultarDetalheHistoricoContaFavorecido.
	 *
	 * @param saidaConsultarDetalheHistoricoContaFavorecido the saida consultar detalhe historico conta favorecido
	 */
	public void setSaidaConsultarDetalheHistoricoContaFavorecido(ConsultarDetalheHistoricoContaFavorecidoSaidaDTO saidaConsultarDetalheHistoricoContaFavorecido) {
		this.saidaConsultarDetalheHistoricoContaFavorecido = saidaConsultarDetalheHistoricoContaFavorecido;
	}

	/**
	 * Get: itemContaHistorico.
	 *
	 * @return itemContaHistorico
	 */
	public Integer getItemContaHistorico() {
		return itemContaHistorico;
	}

	/**
	 * Set: itemContaHistorico.
	 *
	 * @param itemContaHistorico the item conta historico
	 */
	public void setItemContaHistorico(Integer itemContaHistorico) {
		this.itemContaHistorico = itemContaHistorico;
	}

	/**
	 * Get: entradaBloquearContaFavorecidoEntrada.
	 *
	 * @return entradaBloquearContaFavorecidoEntrada
	 */
	public BloquearContaFavorecidoEntradaDTO getEntradaBloquearContaFavorecidoEntrada() {
		return entradaBloquearContaFavorecidoEntrada;
	}

	/**
	 * Set: entradaBloquearContaFavorecidoEntrada.
	 *
	 * @param entradaBloquearContaFavorecidoEntrada the entrada bloquear conta favorecido entrada
	 */
	public void setEntradaBloquearContaFavorecidoEntrada(BloquearContaFavorecidoEntradaDTO entradaBloquearContaFavorecidoEntrada) {
		this.entradaBloquearContaFavorecidoEntrada = entradaBloquearContaFavorecidoEntrada;
	}

	/**
	 * Get: motivoBloqueioConta.
	 *
	 * @return motivoBloqueioConta
	 */
	public String getMotivoBloqueioConta() {
		return motivoBloqueioConta;
	}

	/**
	 * Set: motivoBloqueioConta.
	 *
	 * @param motivoBloqueioConta the motivo bloqueio conta
	 */
	public void setMotivoBloqueioConta(String motivoBloqueioConta) {
		this.motivoBloqueioConta = motivoBloqueioConta;
	}

	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}

	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * Set: cdSituacao.
	 *
	 * @param cdSituacao the cd situacao
	 */
	public void setCdSituacao(Integer cdSituacao) {
		this.cdSituacao = cdSituacao;
	}

	/**
	 * Get: cdFavorecidoFiltroHistorico.
	 *
	 * @return cdFavorecidoFiltroHistorico
	 */
	public Long getCdFavorecidoFiltroHistorico() {
		return cdFavorecidoFiltroHistorico;
	}

	/**
	 * Set: cdFavorecidoFiltroHistorico.
	 *
	 * @param cdFavorecidoFiltroHistorico the cd favorecido filtro historico
	 */
	public void setCdFavorecidoFiltroHistorico(Long cdFavorecidoFiltroHistorico) {
		this.cdFavorecidoFiltroHistorico = cdFavorecidoFiltroHistorico;
	}

	/**
	 * Get: itemFiltroHistoricoSelecionado.
	 *
	 * @return itemFiltroHistoricoSelecionado
	 */
	public String getItemFiltroHistoricoSelecionado() {
		return itemFiltroHistoricoSelecionado;
	}

	/**
	 * Set: itemFiltroHistoricoSelecionado.
	 *
	 * @param itemFiltroHistoricoSelecionado the item filtro historico selecionado
	 */
	public void setItemFiltroHistoricoSelecionado(
			String itemFiltroHistoricoSelecionado) {
		this.itemFiltroHistoricoSelecionado = itemFiltroHistoricoSelecionado;
	}

	/**
	 * Get: cdInscricaoFavorecidoFiltroHistorico.
	 *
	 * @return cdInscricaoFavorecidoFiltroHistorico
	 */
	public Long getCdInscricaoFavorecidoFiltroHistorico() {
		return cdInscricaoFavorecidoFiltroHistorico;
	}

	/**
	 * Set: cdInscricaoFavorecidoFiltroHistorico.
	 *
	 * @param cdInscricaoFavorecidoFiltroHistorico the cd inscricao favorecido filtro historico
	 */
	public void setCdInscricaoFavorecidoFiltroHistorico(
		Long cdInscricaoFavorecidoFiltroHistorico) {
		this.cdInscricaoFavorecidoFiltroHistorico = cdInscricaoFavorecidoFiltroHistorico;
	}

	/**
	 * Get: tipoInscricaoFavorecidoFiltroHistorico.
	 *
	 * @return tipoInscricaoFavorecidoFiltroHistorico
	 */
	public Integer getTipoInscricaoFavorecidoFiltroHistorico() {
		return tipoInscricaoFavorecidoFiltroHistorico;
	}

	/**
	 * Set: tipoInscricaoFavorecidoFiltroHistorico.
	 *
	 * @param tipoInscricaoFavorecidoFiltroHistorico the tipo inscricao favorecido filtro historico
	 */
	public void setTipoInscricaoFavorecidoFiltroHistorico(
			Integer tipoInscricaoFavorecidoFiltroHistorico) {
		this.tipoInscricaoFavorecidoFiltroHistorico = tipoInscricaoFavorecidoFiltroHistorico;
	}

	/**
	 * Is disable argumentos consulta historico.
	 *
	 * @return true, if is disable argumentos consulta historico
	 */
	public boolean isDisableArgumentosConsultaHistorico() {
		return disableArgumentosConsultaHistorico;
	}

	/**
	 * Set: disableArgumentosConsultaHistorico.
	 *
	 * @param disableArgumentosConsultaHistorico the disable argumentos consulta historico
	 */
	public void setDisableArgumentosConsultaHistorico(
			boolean disableArgumentosConsultaHistorico) {
		this.disableArgumentosConsultaHistorico = disableArgumentosConsultaHistorico;
	}

	/**
	 * Get: periodoFinalHistorico.
	 *
	 * @return periodoFinalHistorico
	 */
	public Date getPeriodoFinalHistorico() {
		return periodoFinalHistorico;
	}

	/**
	 * Set: periodoFinalHistorico.
	 *
	 * @param periodoFinalHistorico the periodo final historico
	 */
	public void setPeriodoFinalHistorico(Date periodoFinalHistorico) {
		this.periodoFinalHistorico = periodoFinalHistorico;
	}

	/**
	 * Get: periodoInicialHistorico.
	 *
	 * @return periodoInicialHistorico
	 */
	public Date getPeriodoInicialHistorico() {
		return periodoInicialHistorico;
	}

	/**
	 * Set: periodoInicialHistorico.
	 *
	 * @param periodoInicialHistorico the periodo inicial historico
	 */
	public void setPeriodoInicialHistorico(Date periodoInicialHistorico) {
		this.periodoInicialHistorico = periodoInicialHistorico;
	}

	/**
	 * Get: cdPesquisaLista.
	 *
	 * @return cdPesquisaLista
	 */
	public Integer getCdPesquisaLista() {
	    return cdPesquisaLista;
	}

	/**
	 * Set: cdPesquisaLista.
	 *
	 * @param cdPesquisaLista the cd pesquisa lista
	 */
	public void setCdPesquisaLista(Integer cdPesquisaLista) {
	    this.cdPesquisaLista = cdPesquisaLista;
	}

	/**
	 * Get: agenciaHistoricoConta.
	 *
	 * @return agenciaHistoricoConta
	 */
	public Integer getAgenciaHistoricoConta() {
	    return agenciaHistoricoConta;
	}

	/**
	 * Set: agenciaHistoricoConta.
	 *
	 * @param agenciaHistoricoConta the agencia historico conta
	 */
	public void setAgenciaHistoricoConta(Integer agenciaHistoricoConta) {
	    this.agenciaHistoricoConta = agenciaHistoricoConta;
	}

	/**
	 * Get: bancoHistoricoConta.
	 *
	 * @return bancoHistoricoConta
	 */
	public Integer getBancoHistoricoConta() {
	    return bancoHistoricoConta;
	}

	/**
	 * Set: bancoHistoricoConta.
	 *
	 * @param bancoHistoricoConta the banco historico conta
	 */
	public void setBancoHistoricoConta(Integer bancoHistoricoConta) {
	    this.bancoHistoricoConta = bancoHistoricoConta;
	}

	/**
	 * Get: cdPesquisaListaConta.
	 *
	 * @return cdPesquisaListaConta
	 */
	public Integer getCdPesquisaListaConta() {
	    return cdPesquisaListaConta;
	}

	/**
	 * Set: cdPesquisaListaConta.
	 *
	 * @param cdPesquisaListaConta the cd pesquisa lista conta
	 */
	public void setCdPesquisaListaConta(Integer cdPesquisaListaConta) {
	    this.cdPesquisaListaConta = cdPesquisaListaConta;
	}

	/**
	 * Get: contaHistoricoConta.
	 *
	 * @return contaHistoricoConta
	 */
	public Long getContaHistoricoConta() {
	    return contaHistoricoConta;
	}

	/**
	 * Set: contaHistoricoConta.
	 *
	 * @param contaHistoricoConta the conta historico conta
	 */
	public void setContaHistoricoConta(Long contaHistoricoConta) {
	    this.contaHistoricoConta = contaHistoricoConta;
	}

	/**
	 * Is disable argumentos consulta historico conta.
	 *
	 * @return true, if is disable argumentos consulta historico conta
	 */
	public boolean isDisableArgumentosConsultaHistoricoConta() {
	    return disableArgumentosConsultaHistoricoConta;
	}

	/**
	 * Set: disableArgumentosConsultaHistoricoConta.
	 *
	 * @param disableArgumentosConsultaHistoricoConta the disable argumentos consulta historico conta
	 */
	public void setDisableArgumentosConsultaHistoricoConta(boolean disableArgumentosConsultaHistoricoConta) {
	    this.disableArgumentosConsultaHistoricoConta = disableArgumentosConsultaHistoricoConta;
	}

	/**
	 * Get: periodoFinalHistoricoConta.
	 *
	 * @return periodoFinalHistoricoConta
	 */
	public Date getPeriodoFinalHistoricoConta() {
	    return periodoFinalHistoricoConta;
	}

	/**
	 * Set: periodoFinalHistoricoConta.
	 *
	 * @param periodoFinalHistoricoConta the periodo final historico conta
	 */
	public void setPeriodoFinalHistoricoConta(Date periodoFinalHistoricoConta) {
	    this.periodoFinalHistoricoConta = periodoFinalHistoricoConta;
	}

	/**
	 * Get: periodoInicialHistoricoConta.
	 *
	 * @return periodoInicialHistoricoConta
	 */
	public Date getPeriodoInicialHistoricoConta() {
	    return periodoInicialHistoricoConta;
	}

	/**
	 * Set: periodoInicialHistoricoConta.
	 *
	 * @param periodoInicialHistoricoConta the periodo inicial historico conta
	 */
	public void setPeriodoInicialHistoricoConta(Date periodoInicialHistoricoConta) {
	    this.periodoInicialHistoricoConta = periodoInicialHistoricoConta;
	}

	/**
	 * Get: preencherTipoConta.
	 *
	 * @return preencherTipoConta
	 */
	public List<SelectItem> getPreencherTipoConta() {
	    return preencherTipoConta;
	}

	/**
	 * Set: preencherTipoConta.
	 *
	 * @param preencherTipoConta the preencher tipo conta
	 */
	public void setPreencherTipoConta(List<SelectItem> preencherTipoConta) {
	    this.preencherTipoConta = preencherTipoConta;
	}

	/**
	 * Get: tipoContaHistoricoConta.
	 *
	 * @return tipoContaHistoricoConta
	 */
	public Integer getTipoContaHistoricoConta() {
	    return tipoContaHistoricoConta;
	}

	/**
	 * Set: tipoContaHistoricoConta.
	 *
	 * @param tipoContaHistoricoConta the tipo conta historico conta
	 */
	public void setTipoContaHistoricoConta(Integer tipoContaHistoricoConta) {
	    this.tipoContaHistoricoConta = tipoContaHistoricoConta;
	}

	/**
	 * Get: saidaConsultarHistoricoFavorecido.
	 *
	 * @return saidaConsultarHistoricoFavorecido
	 */
	public ConsultarListaHistoricoFavorecidoSaidaDTO getSaidaConsultarHistoricoFavorecido() {
	    return saidaConsultarHistoricoFavorecido;
	}

	/**
	 * Set: saidaConsultarHistoricoFavorecido.
	 *
	 * @param saidaConsultarHistoricoFavorecido the saida consultar historico favorecido
	 */
	public void setSaidaConsultarHistoricoFavorecido(ConsultarListaHistoricoFavorecidoSaidaDTO saidaConsultarHistoricoFavorecido) {
	    this.saidaConsultarHistoricoFavorecido = saidaConsultarHistoricoFavorecido;
	}

	/**
	 * Get: saidaConsultarListaHistoricoContaFavorecido.
	 *
	 * @return saidaConsultarListaHistoricoContaFavorecido
	 */
	public ConsultarListaHistoricoContaFavorecidoSaidaDTO getSaidaConsultarListaHistoricoContaFavorecido() {
	    return saidaConsultarListaHistoricoContaFavorecido;
	}

	/**
	 * Set: saidaConsultarListaHistoricoContaFavorecido.
	 *
	 * @param saidaConsultarListaHistoricoContaFavorecido the saida consultar lista historico conta favorecido
	 */
	public void setSaidaConsultarListaHistoricoContaFavorecido(ConsultarListaHistoricoContaFavorecidoSaidaDTO saidaConsultarListaHistoricoContaFavorecido) {
	    this.saidaConsultarListaHistoricoContaFavorecido = saidaConsultarListaHistoricoContaFavorecido;
	}
}