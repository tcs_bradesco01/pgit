/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercadastroterceiroparticipante
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercadastroterceiroparticipante;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.IManterCadastroTerceiroParticipanteService;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.AlterarTercParticiContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.AlterarTercParticiContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ConsultarTercPartContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ConsultarTercPartContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ExcluirTerceiroPartContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ExcluirTerceiroPartContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.IncluirTerceiroPartContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.IncluirTerceiroPartContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.ListarTercParticipanteContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroterceiroparticipante.bean.OcorrenciasListarTercParticipanteContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.DateUtils;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManCadCpfCnpjTerParticipanteBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManCadCpfCnpjTerParticipanteBean {

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo manterCadastroTerceiroParticipanteServiceImpl. */
	private IManterCadastroTerceiroParticipanteService manterCadastroTerceiroParticipanteServiceImpl;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;

	/** Atributo filtroIdentificaoImpl. */
	private IFiltroIdentificaoService filtroIdentificaoImpl;

	// Cliente Representante
	/** Atributo cpfCnpjRepresentante. */
	private String cpfCnpjRepresentante;

	/** Atributo nomeRazaoRepresentante. */
	private String nomeRazaoRepresentante;

	/** Atributo grupEconRepresentante. */
	private String grupEconRepresentante;

	/** Atributo ativEconRepresentante. */
	private String ativEconRepresentante;

	/** Atributo segRepresentante. */
	private String segRepresentante;

	/** Atributo subSegRepresentante. */
	private String subSegRepresentante;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;

	// Cliente Participante
	/** Atributo cpfCnpj. */
	private String cpfCnpj;

	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;

	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;
	
	/** Atributo cpfCnpjParticipanteTemp. */
	private String cpfCnpjParticipanteTemp;

	/** Atributo nomeRazaoParticipanteTemp. */
	private String nomeRazaoParticipanteTemp;

	/** Atributo cpfCnpjParticipanteDet. */
	private String cpfCnpjParticipanteDet;

	/** Atributo nomeRazaoParticipanteDet. */
	private String nomeRazaoParticipanteDet;

	/** Atributo grupoEconParticipante. */
	private String grupoEconParticipante;

	/** Atributo atividadeEconParticipante. */
	private String atividadeEconParticipante;

	/** Atributo segmentoParticipante. */
	private String segmentoParticipante;

	/** Atributo subSegmentoParticipante. */
	private String subSegmentoParticipante;

	// Contrato
	/** Atributo empresaContrato. */
	private String empresaContrato;

	/** Atributo tipoContrato. */
	private String tipoContrato;

	/** Atributo numeroContrato. */
	private String numeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo situacaoContrato. */
	private String situacaoContrato;

	/** Atributo motivoContrato. */
	private String motivoContrato;

	/** Atributo participacaoContrato. */
	private String participacaoContrato;

	// tela Participantes

	/** Atributo itemSelecionadoParticipantes. */
	private Integer itemSelecionadoParticipantes;

	/** Atributo listaGridParticipantes. */
	private List<ListarParticipantesSaidaDTO> listaGridParticipantes;

	/** Atributo listaGridControleParticipantes. */
	private List<SelectItem> listaGridControleParticipantes;

	// tela Terceiros
	/** Atributo itemFiltroTerceiroSelecionado. */
	private String itemFiltroTerceiroSelecionado;

	/** Atributo bloqueiaRdoTerceiro. */
	private boolean bloqueiaRdoTerceiro;

	/** Atributo cdCpfCnpjFiltro. */
	private Long cdCpfCnpjFiltro;

	/** Atributo cdFilialCnpjFiltro. */
	private Integer cdFilialCnpjFiltro;

	/** Atributo cdControleCnpjFiltro. */
	private Integer cdControleCnpjFiltro;

	/** Atributo cdCpfFiltro. */
	private Long cdCpfFiltro;

	/** Atributo cdControleCpfFiltro. */
	private Integer cdControleCpfFiltro;

	/** Atributo listaGridTerceiros. */
	private List<OcorrenciasListarTercParticipanteContratoSaidaDTO> listaGridTerceiros;

	/** Atributo listaGridControleTerceiros. */
	private List<SelectItem> listaGridControleTerceiros;

	/** Atributo itemSelecionadoTerceiros. */
	private Integer itemSelecionadoTerceiros;

	// tela detalhe
	/** Atributo inscricaoRastreada. */
	private String inscricaoRastreada;

	/** Atributo dtInicioRastreamento. */
	private String dtInicioRastreamento;

	/** Atributo agendarTitulosRastreados. */
	private String agendarTitulosRastreados;

	/** Atributo rastrearNotasFiscais. */
	private String rastrearNotasFiscais;

	/** Atributo bloquearEmissaoParcelas. */
	private String bloquearEmissaoParcelas;

	/** Atributo dtInicioBloqEmissaoPapeleta. */
	private String dtInicioBloqEmissaoPapeleta;

	// trilha de auditoria
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	// inclus�o, altera��o
	/** Atributo cdCpfCnpj. */
	private Long cdCpfCnpj;

	/** Atributo cdFilialCnpj. */
	private Integer cdFilialCnpj;

	/** Atributo cdControleCnpj. */
	private Integer cdControleCnpj;

	/** Atributo cdCpf. */
	private Long cdCpf;

	/** Atributo cdControleCpf. */
	private Integer cdControleCpf;

	/** Atributo itemFiltroIncluir. */
	private String itemFiltroIncluir;

	/** Atributo dataInicioRastreamento. */
	private Date dataInicioRastreamento;

	/** Atributo dataInicioBloqEmissaoPapeleta. */
	private Date dataInicioBloqEmissaoPapeleta;

	/** Atributo rdoAgendarTitRastreados. */
	private String rdoAgendarTitRastreados;

	/** Atributo rdoRastrearNotasFiscais. */
	private String rdoRastrearNotasFiscais;

	/** Atributo rdoBloqEmissaoPapeleta. */
	private String rdoBloqEmissaoPapeleta;

	/** Atributo valorRadio. */
	private boolean valorRadio;

	/** Atributo dataHoje. */
	private Date dataHoje =  new Date();
	
	/** Atributo saidaDetalhar. */
	private ConsultarTercPartContratoSaidaDTO saidaDetalhar = new ConsultarTercPartContratoSaidaDTO();

	/**
	 * Get: saidaDetalhar.
	 *
	 * @return saidaDetalhar
	 */
	public ConsultarTercPartContratoSaidaDTO getSaidaDetalhar() {
		return saidaDetalhar;
	}

	/**
	 * Set: saidaDetalhar.
	 *
	 * @param saidaDetalhar the saida detalhar
	 */
	public void setSaidaDetalhar(ConsultarTercPartContratoSaidaDTO saidaDetalhar) {
		this.saidaDetalhar = saidaDetalhar;
	}

	// M�todos
	/**
	 * Iniciar pagina.
	 *
	 * @param evt the evt
	 */
	public void iniciarPagina(ActionEvent evt) {

		this.identificacaoClienteContratoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean
				.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean
				.setPaginaCliente("identificacaoClienteManCadCpfCnpjTerPart");
		identificacaoClienteContratoBean
				.setPaginaRetorno("conManCadCpfCnpjTerPart");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		
		setItemFiltroTerceiroSelecionado("");
	}

	/**
	 * Participantes.
	 *
	 * @return the string
	 */
	public String participantes() {
		try {
			carregaListaParticipantes();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridParticipantes(null);
			setItemSelecionadoParticipantes(null);
			return "";
		}

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(
						identificacaoClienteContratoBean
								.getItemSelecionadoLista());

		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());

		// Cliente Participante
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		
		} else {
			setCpfCnpjParticipante("");
			setNomeRazaoParticipante("");
		}

		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());

		return "PARTICIPANTES";
	}

	/**
	 * Limpar rdo incluir.
	 */
	public void limparRdoIncluir() {

		if (getItemFiltroIncluir().equals("0")) {
			setCdControleCnpj(null);
			setCdFilialCnpj(null);
			setCdCpfCnpj(null);

		} else {
			if (getItemFiltroIncluir().equals("1")) {
				setCdCpf(null);
				setCdControleCpf(null);
			}
		}
	}

	/**
	 * Limpar rdo terceiro.
	 */
	public void limparRdoTerceiro() {

		if (getItemFiltroTerceiroSelecionado().equals("0")) {
			setCdCpfFiltro(null);
			setCdControleCpfFiltro(null);

		} else {
			if (getItemFiltroTerceiroSelecionado().equals("1")) {
				setCdControleCnpjFiltro(null);
				setCdFilialCnpjFiltro(null);
				setCdCpfCnpjFiltro(null);
			}
		}

	}

	/**
	 * Pesquisar participante.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarParticipante(ActionEvent evt) {
		try {
			carregaListaParticipantes();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridParticipantes(null);
			setItemSelecionadoParticipantes(null);
			return "";
		}
		return "";
	}

	/**
	 * Pesquisar terceiro.
	 *
	 * @param evt the evt
	 */
	public void pesquisarTerceiro(ActionEvent evt) {
		carregaListaTerceiros();
	}

	/**
	 * Voltar tela participante.
	 *
	 * @return the string
	 */
	public String voltarTelaParticipante() {		
		setCpfCnpjParticipante(cpfCnpjParticipanteTemp);
		setNomeRazaoParticipante(nomeRazaoParticipanteTemp);
		cpfCnpjParticipanteTemp = null;
		nomeRazaoParticipanteTemp = null;
		
		return "VOLTAR";

	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		return "VOLTAR";

	}

	/**
	 * Voltar tela terceiro.
	 *
	 * @return the string
	 */
	public String voltarTelaTerceiro() {
		return "VOLTAR";
	}

	/**
	 * Terceiros.
	 *
	 * @return the string
	 */
	public String terceiros() {
		ListarParticipantesSaidaDTO participantes = getListaGridParticipantes().get(getItemSelecionadoParticipantes());
		if(participantes.getCdTipoParticipacao() != 1){
			cpfCnpjParticipanteTemp = getCpfCnpjParticipante();
			nomeRazaoParticipanteTemp = getNomeRazaoParticipante();
			setCpfCnpjParticipante(participantes.getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(participantes.getNomeRazao());
		}
		
		setBloqueiaRdoTerceiro(false);
		setItemFiltroTerceiroSelecionado("");
		setCdCpfFiltro(null);
		setCdControleCnpjFiltro(null);
		setCdCpfCnpjFiltro(null);
		setCdFilialCnpjFiltro(null);
		setCdControleCpfFiltro(null);
		setListaGridTerceiros(null);
		setListaGridControleTerceiros(null);
		setItemSelecionadoTerceiros(null);

		return "TERCEIROS";
	}

	/**
	 * Carrega lista terceiros.
	 *
	 * @return the string
	 */
	public String carregaListaTerceiros() {
		setBloqueiaRdoTerceiro(false);

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(
						identificacaoClienteContratoBean
								.getItemSelecionadoLista());
		ListarParticipantesSaidaDTO saidaParticipantes = getListaGridParticipantes()
				.get(getItemSelecionadoParticipantes());

		listaGridTerceiros = new ArrayList<OcorrenciasListarTercParticipanteContratoSaidaDTO>();
		try {

			ListarTercParticipanteContratoEntradaDTO entrada = new ListarTercParticipanteContratoEntradaDTO();

			entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO
					.getNrSequenciaContrato());
			entrada.setCdTipoParticipacaoPessoa(saidaParticipantes
					.getCdTipoParticipacao());
			entrada.setCdPessoa(saidaParticipantes.getCdPessoa());
			entrada.setCdFilialCnpjTerceiro(0);
			entrada.setCdFilialCnpjTerceiro(0);
			entrada.setCdCpfCnpjTerceiro(0L);
			entrada.setNrRastreaTerceiro(0);

			if (getItemFiltroTerceiroSelecionado() != null
					&& !getItemFiltroTerceiroSelecionado().equals("")) {
				if (getItemFiltroTerceiroSelecionado().equals("0")) {
					entrada.setCdCpfCnpjTerceiro(getCdCpfCnpjFiltro());
					entrada.setCdFilialCnpjTerceiro(getCdFilialCnpjFiltro());
					entrada
							.setCdControleCnpjTerceiro(getCdControleCnpjFiltro());

				} else {
					if (getItemFiltroTerceiroSelecionado().equals("1")) {
						entrada.setCdCpfCnpjTerceiro(getCdCpfFiltro());
						entrada
								.setCdControleCnpjTerceiro(getCdControleCpfFiltro());
					}
				}
			}

			setListaGridTerceiros(getManterCadastroTerceiroParticipanteServiceImpl()
					.listarTercParticipanteContrato(entrada)
					.getListaOcorrencias());
			this.listaGridControleTerceiros = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridTerceiros().size(); i++) {
				listaGridControleTerceiros.add(new SelectItem(i, " "));
			}

			setBloqueiaRdoTerceiro(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridTerceiros(null);
			return "";

		}

		return "";

	}

	/**
	 * Carrega lista terceiros excluir.
	 *
	 * @return the string
	 */
	public String carregaListaTerceirosExcluir() {
		setBloqueiaRdoTerceiro(false);

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(
						identificacaoClienteContratoBean
								.getItemSelecionadoLista());
		ListarParticipantesSaidaDTO saidaParticipantes = getListaGridParticipantes()
				.get(getItemSelecionadoParticipantes());

		listaGridTerceiros = new ArrayList<OcorrenciasListarTercParticipanteContratoSaidaDTO>();
		try {

			ListarTercParticipanteContratoEntradaDTO entrada = new ListarTercParticipanteContratoEntradaDTO();

			entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO
					.getNrSequenciaContrato());
			entrada.setCdTipoParticipacaoPessoa(saidaParticipantes
					.getCdTipoParticipacao());
			entrada.setCdPessoa(saidaParticipantes.getCdPessoa());
			entrada.setCdFilialCnpjTerceiro(0);
			entrada.setCdFilialCnpjTerceiro(0);
			entrada.setCdCpfCnpjTerceiro(0L);
			entrada.setNrRastreaTerceiro(0);

			if (getItemFiltroTerceiroSelecionado() != null
					&& !getItemFiltroTerceiroSelecionado().equals("")) {
				if (getItemFiltroTerceiroSelecionado().equals("0")) {
					entrada.setCdCpfCnpjTerceiro(getCdCpfCnpjFiltro());
					entrada.setCdFilialCnpjTerceiro(getCdFilialCnpjFiltro());
					entrada
							.setCdControleCnpjTerceiro(getCdControleCnpjFiltro());

				} else {
					if (getItemFiltroTerceiroSelecionado().equals("1")) {
						entrada.setCdCpfCnpjTerceiro(getCdCpfFiltro());
						entrada
								.setCdControleCnpjTerceiro(getCdControleCpfFiltro());
					}
				}
			}

			setListaGridTerceiros(getManterCadastroTerceiroParticipanteServiceImpl()
					.listarTercParticipanteContrato(entrada)
					.getListaOcorrencias());
			this.listaGridControleTerceiros = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridTerceiros().size(); i++) {
				listaGridControleTerceiros.add(new SelectItem(i, " "));
			}

			setBloqueiaRdoTerceiro(true);
		} catch (PdcAdapterFunctionalException p) {
			setListaGridTerceiros(null);
			throw p;
		}

		return "";
	}

	/**
	 * Limpar tela terceiros.
	 */
	public void limparTelaTerceiros() {

		setListaGridTerceiros(null);
		setListaGridControleTerceiros(null);
		setItemSelecionadoTerceiros(null);
		setBloqueiaRdoTerceiro(false);
	}

	/**
	 * Limpar campos terceiros.
	 */
	public void limparCamposTerceiros() {
		limparTelaTerceiros();
		setCdControleCnpjFiltro(null);
		setCdControleCpfFiltro(null);
		setCdCpfFiltro(null);
		setCdCpfCnpjFiltro(null);
		setCdFilialCnpjFiltro(null);
		setBloqueiaRdoTerceiro(false);

		setItemFiltroTerceiroSelecionado("");
	}

	/**
	 * Limpar dados incluir.
	 */
	public void limparDadosIncluir() {

		setCdCpfCnpj(null);
		setCdFilialCnpj(null);
		setCdControleCpf(null);
		setCdCpf(null);
		setCdControleCnpj(null);
		setItemFiltroIncluir(null);
		setRdoAgendarTitRastreados(null);
		setRdoRastrearNotasFiscais(null);
		setRdoBloqEmissaoPapeleta(null);
		setDataInicioBloqEmissaoPapeleta(new Date());
		setDataInicioRastreamento(new Date());
	}

	/**
	 * Carrega lista participantes.
	 *
	 * @return the string
	 */
	public String carregaListaParticipantes() {

		listaGridParticipantes = new ArrayList<ListarParticipantesSaidaDTO>();

		ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(
						identificacaoClienteContratoBean
								.getItemSelecionadoLista());

		entradaDTO.setMaxOcorrencias(20);
		entradaDTO.setCodPessoaJuridica(saidaDTO.getCdPessoaJuridica());
		entradaDTO.setCodTipoContrato(saidaDTO.getCdTipoContrato());
		entradaDTO.setNrSequenciaContrato(saidaDTO.getNrSequenciaContrato());

		setListaGridParticipantes(getManterContratoImpl().listarParticipantes(
				entradaDTO));

		this.listaGridControleParticipantes = new ArrayList<SelectItem>();
		for (int i = 0; i < getListaGridParticipantes().size(); i++) {
			listaGridControleParticipantes.add(new SelectItem(i, " "));
		}
		setItemSelecionadoParticipantes(null);

		return "";

	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(
						identificacaoClienteContratoBean
								.getItemSelecionadoLista());
		ListarParticipantesSaidaDTO saidaParticipantes = getListaGridParticipantes()
				.get(getItemSelecionadoParticipantes());
		OcorrenciasListarTercParticipanteContratoSaidaDTO saidaTerceiros = getListaGridTerceiros()
				.get(getItemSelecionadoTerceiros());

		ConsultarTercPartContratoEntradaDTO entrada = new ConsultarTercPartContratoEntradaDTO(); // tela
																									// Detalhar

		entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
		entrada
				.setNrSequenciaContratoNegocio(saidaDTO
						.getNrSequenciaContrato());
		entrada.setCdTipoParticipacaoPessoa(saidaParticipantes
				.getCdTipoParticipacao());
		entrada.setCdPessoa(saidaParticipantes.getCdPessoa());
		entrada.setNrRastreaTerceiro(saidaTerceiros.getNrRastreaTerceiro());
		entrada.setCdCpfCnpjTerceiro(saidaTerceiros.getCdCpfCnpjTerceiro());
		entrada.setCdFilialCnpjTerceiro(saidaTerceiros
				.getCdFilialCnpjTerceiro());
		entrada.setCdControleCnpjTerceiro(saidaTerceiros
				.getCdControleCnpjTerceiro());

		saidaDetalhar = getManterCadastroTerceiroParticipanteServiceImpl()
				.consultarTercPartContrato(entrada);

		setDtInicioRastreamento(saidaDetalhar.getDtInicioRastreaTerceiro()
				.replace(".", "/"));
		setDtInicioBloqEmissaoPapeleta(saidaDetalhar
				.getDtInicioPapeletaTerceiro().replace(".", "/"));
		setInscricaoRastreada(CpfCnpjUtils.formatCpfCnpjCompleto(saidaDetalhar
				.getCdCpfCnpjTerceiro(), saidaDetalhar
				.getCdFilialCnpjTerceiro(), saidaDetalhar
				.getCdControleCnpjTerceiro()));

		setRdoAgendarTitRastreados(saidaDetalhar.getCdIndicadorAgendaTitulo()
				.toString());
		setRdoRastrearNotasFiscais(saidaDetalhar.getCdIndicadorNotaTerceiro()
				.toString());
		setRdoBloqEmissaoPapeleta(saidaDetalhar
				.getCdIndicadorPapeletaTerceiro().toString());

		try {
			setDataInicioBloqEmissaoPapeleta(FormatarData
					.formataDiaMesAnoFromPdc(saidaDetalhar
							.getDtInicioPapeletaTerceiro()));
		} catch (Exception e) {
			if (getRdoBloqEmissaoPapeleta().equals("2")) {
				setDataInicioBloqEmissaoPapeleta(null);
			} else {
				setDataInicioBloqEmissaoPapeleta(new Date());
			}
			setDtInicioBloqEmissaoPapeleta("");
		}

		try {
			setDataInicioRastreamento(FormatarData
					.formataDiaMesAnoFromPdc(saidaDetalhar
							.getDtInicioRastreaTerceiro()));
		} catch (Exception e) {
			if (getRdoBloqEmissaoPapeleta().equals("2")) {
				setDataInicioRastreamento(null);
			} else {
				setDataInicioRastreamento(new Date());
			}
			setDtInicioRastreamento("");
		}

		// Trilha de Auditoria
		setDataHoraInclusao(saidaDetalhar.getHrInclusaoRegistro());
		setUsuarioInclusao(saidaDetalhar.getCdAutenticacaoSegurancaInclusao());
		setTipoCanalInclusao(saidaDetalhar.getCdCanalInclusao() != 0 ? saidaDetalhar
				.getCdCanalInclusao()
				+ " - " + saidaDetalhar.getDsCanalInclusao()
				: "");
		setComplementoInclusao(saidaDetalhar.getNmOperacaoFluxoInclusao() == null
				|| saidaDetalhar.getNmOperacaoFluxoInclusao().equals("0") ? ""
				: saidaDetalhar.getNmOperacaoFluxoInclusao());

		setDataHoraManutencao(saidaDetalhar.getHrManutencaoRegistro());
		setUsuarioManutencao(saidaDetalhar
				.getCdAutenticacaoSegurancaManutencao());
		setTipoCanalManutencao(saidaDetalhar.getCdCanalManutencao() != 0 ? saidaDetalhar
				.getCdCanalManutencao()
				+ " - " + saidaDetalhar.getDsCanalManutencao()
				: "");
		setComplementoManutencao(saidaDetalhar.getNmOperacaoFluxoManutencao() == null
				|| saidaDetalhar.getNmOperacaoFluxoManutencao().equals("0") ? ""
				: saidaDetalhar.getNmOperacaoFluxoManutencao());

	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {

		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), "conManCadTerceiros",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "DETALHAR";
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), "conManCadTerceiros",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "ALTERAR";
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {

		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), "conManCadTerceiros",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "EXCLUIR";
	}

	/**
	 * Nova data.
	 */
	public void novaData() {

		if (getRdoBloqEmissaoPapeleta().equals("1")) {
			setDataInicioBloqEmissaoPapeleta(new Date());
		} else {
			setDataInicioBloqEmissaoPapeleta(null);
		}
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		limparDadosIncluir();
		return "INCLUIR";
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {
		ExcluirTerceiroPartContratoEntradaDTO entrada = new ExcluirTerceiroPartContratoEntradaDTO();

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(
						identificacaoClienteContratoBean
								.getItemSelecionadoLista());
		ListarParticipantesSaidaDTO saidaParticipantes = getListaGridParticipantes()
				.get(getItemSelecionadoParticipantes());
		OcorrenciasListarTercParticipanteContratoSaidaDTO saidaTerceiros = getListaGridTerceiros()
				.get(getItemSelecionadoTerceiros());

		entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
		entrada
				.setNrSequenciaContratoNegocio(saidaDTO
						.getNrSequenciaContrato());
		entrada.setCdTipoParticipacaoPessoa(saidaParticipantes
				.getCdTipoParticipacao());
		entrada.setCdPessoa(saidaParticipantes.getCdPessoa());
		entrada.setNrRastreaTerceiro(saidaTerceiros.getNrRastreaTerceiro());

		ExcluirTerceiroPartContratoSaidaDTO saida = getManterCadastroTerceiroParticipanteServiceImpl()
				.excluirTerceiroPartContrato(entrada);
		BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
				+ ") " + saida.getMensagem(), "conManCadTerceiros",
				BradescoViewExceptionActionType.ACTION, false);

		// se houver sucesso na exclus�o carrega novamente a lista da tela de
		// terceiros
		try {
			carregaListaTerceirosExcluir();			
		} catch (PdcAdapterFunctionalException e) {
		}

		return "";
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {

		IncluirTerceiroPartContratoEntradaDTO entrada = new IncluirTerceiroPartContratoEntradaDTO();

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(
						identificacaoClienteContratoBean
								.getItemSelecionadoLista());
		ListarParticipantesSaidaDTO saidaParticipantes = getListaGridParticipantes()
				.get(getItemSelecionadoParticipantes());

		entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
		entrada
				.setNrSequenciaContratoNegocio(saidaDTO
						.getNrSequenciaContrato());
		entrada.setCdTipoParticipacaoPessoa(saidaParticipantes
				.getCdTipoParticipacao());
		entrada.setCdPessoa(saidaParticipantes.getCdPessoa());
		entrada.setCdFilialCnpjTerceiro(0);
		entrada.setCdFilialCnpjTerceiro(0);
		entrada.setCdCpfCnpjTerceiro(0L);

		if (getItemFiltroIncluir().equals("2")) { // CNPJ
			entrada.setCdCpfCnpjTerceiro(getCdCpfCnpj());
			entrada.setCdFilialCnpjTerceiro(getCdFilialCnpj());
			entrada.setCdControleCnpjTerceiro(getCdControleCnpj());

		} else {
			if (getItemFiltroIncluir().equals("1")) { // CPF
				entrada.setCdCpfCnpjTerceiro(getCdCpf());
				entrada.setCdControleCnpjTerceiro(getCdControleCpf());
			}
		}

		entrada.setCdIndicadorAgendaTitulo(Integer
				.parseInt(getRdoAgendarTitRastreados()));
		entrada.setCdIndicadorTerceiro(Integer
				.parseInt(getRdoRastrearNotasFiscais()));
		entrada.setCdIndicadorPapeletaTerceiro(Integer
				.parseInt(getRdoBloqEmissaoPapeleta()));

		if (getRdoBloqEmissaoPapeleta().equals("2")) {
			entrada.setDtInicioPapeletaTerceiro("");
		} else {
			entrada.setDtInicioPapeletaTerceiro(DateUtils.formartarData(
					getDataInicioBloqEmissaoPapeleta(), "dd/MM/yyyy"));
		}

		entrada.setDtInicioRastreaTerceiro(DateUtils.formartarData(
				getDataInicioRastreamento(), "dd/MM/yyyy"));

		IncluirTerceiroPartContratoSaidaDTO saida = getManterCadastroTerceiroParticipanteServiceImpl()
				.incluirTerceiroPartContrato(entrada);

		BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
				+ ") " + saida.getMensagem(), "conManCadTerceiros",
				BradescoViewExceptionActionType.ACTION, false);

		if (getListaGridTerceiros() != null) {
			carregaListaTerceiros();
		}
		return "";
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {

		AlterarTercParticiContratoEntradaDTO entrada = new AlterarTercParticiContratoEntradaDTO();

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(
						identificacaoClienteContratoBean
								.getItemSelecionadoLista());
		ListarParticipantesSaidaDTO saidaParticipantes = getListaGridParticipantes()
				.get(getItemSelecionadoParticipantes());
		OcorrenciasListarTercParticipanteContratoSaidaDTO saidaTerceiros = getListaGridTerceiros()
				.get(getItemSelecionadoTerceiros());

		entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
		entrada
				.setNrSequenciaContratoNegocio(saidaDTO
						.getNrSequenciaContrato());
		entrada.setCdTipoParticipacaoPessoa(saidaParticipantes
				.getCdTipoParticipacao());
		entrada.setCdPessoa(saidaParticipantes.getCdPessoa());
		entrada.setNrRastreaTerceiro(saidaTerceiros.getNrRastreaTerceiro());
		entrada.setCdCpfCnpjTerceiro(saidaTerceiros.getCdCpfCnpjTerceiro());
		entrada.setCdFilialCnpjTerceiro(saidaTerceiros
				.getCdFilialCnpjTerceiro());
		entrada.setCdControleCnpjTerceiro(saidaTerceiros
				.getCdControleCnpjTerceiro());
		entrada.setCdIndicadorAgendaTitulo(Integer
				.parseInt(getRdoAgendarTitRastreados()));
		entrada.setCdIndicadorTerceiro(Integer
				.parseInt(getRdoRastrearNotasFiscais()));
		entrada.setCdIndicadorPapeletaTerceiro(Integer
				.parseInt(getRdoBloqEmissaoPapeleta()));
		entrada.setCdIndicadorBloqueioRastreabilidade(saidaDetalhar.getCdIndicadorBloqueioRastreabilidade());
		entrada.setDsIndicadorBloqueioRastreabilidade(saidaDetalhar.getDsIndicadorBloqueioRastreabilidade());

		if (getRdoBloqEmissaoPapeleta().equals("2")) {
			entrada.setDtInicioPapeletaTerceiro("");
		} else {
			entrada.setDtInicioPapeletaTerceiro(DateUtils.formartarData(
					getDataInicioBloqEmissaoPapeleta(), "dd/MM/yyyy"));
		}

		entrada.setDtInicioRastreaTerceiro(DateUtils.formartarData(
				getDataInicioRastreamento(), "dd/MM/yyyy"));

		AlterarTercParticiContratoSaidaDTO saida = getManterCadastroTerceiroParticipanteServiceImpl()
				.alterarTercParticiContrato(entrada);
		BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
				+ ") " + saida.getMensagem(), "conManCadTerceiros",
				BradescoViewExceptionActionType.ACTION, false);

		// se houver sucesso na altera��o carrega novamente a lista da tela de
		// terceiros
		carregaListaTerceiros();
		return "";
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {

		setDtInicioRastreamento(FormatarData
				.formataDiaMesAno(getDataInicioRastreamento()));
		setDtInicioBloqEmissaoPapeleta(FormatarData
				.formataDiaMesAno(getDataInicioBloqEmissaoPapeleta()));

		if (getItemFiltroIncluir().equals("1")) { // CPF
			setInscricaoRastreada(CpfCnpjUtils.formatarCpfCnpj(getCdCpf(), 0,
					getCdControleCpf()));
		} else {
			if (getItemFiltroIncluir().equals("2")) { // CNPJ
				setInscricaoRastreada(CpfCnpjUtils.formatarCpfCnpj(
						getCdCpfCnpj(), getCdFilialCnpj(), getCdControleCnpj()));
			}
		}

		return "CONFIRMAR";
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {

		setDtInicioRastreamento(FormatarData
				.formataDiaMesAno(getDataInicioRastreamento()));

		if (!getRdoBloqEmissaoPapeleta().equals("1")) {
			setDtInicioBloqEmissaoPapeleta("");
		} else {
			setDtInicioBloqEmissaoPapeleta(FormatarData
					.formataDiaMesAno(getDataInicioBloqEmissaoPapeleta()));
		}

		return "CONFIRMAR";
	}

	/* GETTERS E SETTERS */
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: manterCadastroTerceiroParticipanteServiceImpl.
	 *
	 * @return manterCadastroTerceiroParticipanteServiceImpl
	 */
	public IManterCadastroTerceiroParticipanteService getManterCadastroTerceiroParticipanteServiceImpl() {
		return manterCadastroTerceiroParticipanteServiceImpl;
	}

	/**
	 * Set: manterCadastroTerceiroParticipanteServiceImpl.
	 *
	 * @param manterCadastroTerceiroParticipanteServiceImpl the manter cadastro terceiro participante service impl
	 */
	public void setManterCadastroTerceiroParticipanteServiceImpl(
			IManterCadastroTerceiroParticipanteService manterCadastroTerceiroParticipanteServiceImpl) {
		this.manterCadastroTerceiroParticipanteServiceImpl = manterCadastroTerceiroParticipanteServiceImpl;
	}

	/**
	 * Get: ativEconRepresentante.
	 *
	 * @return ativEconRepresentante
	 */
	public String getAtivEconRepresentante() {
		return ativEconRepresentante;
	}

	/**
	 * Set: ativEconRepresentante.
	 *
	 * @param ativEconRepresentante the ativ econ representante
	 */
	public void setAtivEconRepresentante(String ativEconRepresentante) {
		this.ativEconRepresentante = ativEconRepresentante;
	}

	/**
	 * Get: cpfCnpjRepresentante.
	 *
	 * @return cpfCnpjRepresentante
	 */
	public String getCpfCnpjRepresentante() {
		return cpfCnpjRepresentante;
	}

	/**
	 * Set: cpfCnpjRepresentante.
	 *
	 * @param cpfCnpjRepresentante the cpf cnpj representante
	 */
	public void setCpfCnpjRepresentante(String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: grupEconRepresentante.
	 *
	 * @return grupEconRepresentante
	 */
	public String getGrupEconRepresentante() {
		return grupEconRepresentante;
	}

	/**
	 * Set: grupEconRepresentante.
	 *
	 * @param grupEconRepresentante the grup econ representante
	 */
	public void setGrupEconRepresentante(String grupEconRepresentante) {
		this.grupEconRepresentante = grupEconRepresentante;
	}

	/**
	 * Get: itemSelecionadoParticipantes.
	 *
	 * @return itemSelecionadoParticipantes
	 */
	public Integer getItemSelecionadoParticipantes() {
		return itemSelecionadoParticipantes;
	}

	/**
	 * Set: itemSelecionadoParticipantes.
	 *
	 * @param itemSelecionadoParticipantes the item selecionado participantes
	 */
	public void setItemSelecionadoParticipantes(
			Integer itemSelecionadoParticipantes) {
		this.itemSelecionadoParticipantes = itemSelecionadoParticipantes;
	}

	/**
	 * Get: listaGridControleParticipantes.
	 *
	 * @return listaGridControleParticipantes
	 */
	public List<SelectItem> getListaGridControleParticipantes() {
		return listaGridControleParticipantes;
	}

	/**
	 * Set: listaGridControleParticipantes.
	 *
	 * @param listaGridControleParticipantes the lista grid controle participantes
	 */
	public void setListaGridControleParticipantes(
			List<SelectItem> listaGridControleParticipantes) {
		this.listaGridControleParticipantes = listaGridControleParticipantes;
	}

	/**
	 * Get: listaGridParticipantes.
	 *
	 * @return listaGridParticipantes
	 */
	public List<ListarParticipantesSaidaDTO> getListaGridParticipantes() {
		return listaGridParticipantes;
	}

	/**
	 * Set: listaGridParticipantes.
	 *
	 * @param listaGridParticipantes the lista grid participantes
	 */
	public void setListaGridParticipantes(
			List<ListarParticipantesSaidaDTO> listaGridParticipantes) {
		this.listaGridParticipantes = listaGridParticipantes;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: nomeRazaoRepresentante.
	 *
	 * @return nomeRazaoRepresentante
	 */
	public String getNomeRazaoRepresentante() {
		return nomeRazaoRepresentante;
	}

	/**
	 * Set: nomeRazaoRepresentante.
	 *
	 * @param nomeRazaoRepresentante the nome razao representante
	 */
	public void setNomeRazaoRepresentante(String nomeRazaoRepresentante) {
		this.nomeRazaoRepresentante = nomeRazaoRepresentante;
	}

	/**
	 * Get: segRepresentante.
	 *
	 * @return segRepresentante
	 */
	public String getSegRepresentante() {
		return segRepresentante;
	}

	/**
	 * Set: segRepresentante.
	 *
	 * @param segRepresentante the seg representante
	 */
	public void setSegRepresentante(String segRepresentante) {
		this.segRepresentante = segRepresentante;
	}

	/**
	 * Get: subSegRepresentante.
	 *
	 * @return subSegRepresentante
	 */
	public String getSubSegRepresentante() {
		return subSegRepresentante;
	}

	/**
	 * Get: atividadeEconParticipante.
	 *
	 * @return atividadeEconParticipante
	 */
	public String getAtividadeEconParticipante() {
		return atividadeEconParticipante;
	}

	/**
	 * Set: atividadeEconParticipante.
	 *
	 * @param atividadeEconParticipante the atividade econ participante
	 */
	public void setAtividadeEconParticipante(String atividadeEconParticipante) {
		this.atividadeEconParticipante = atividadeEconParticipante;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: cpfCnpjParticipanteDet.
	 *
	 * @return cpfCnpjParticipanteDet
	 */
	public String getCpfCnpjParticipanteDet() {
		return cpfCnpjParticipanteDet;
	}

	/**
	 * Set: cpfCnpjParticipanteDet.
	 *
	 * @param cpfCnpjParticipanteDet the cpf cnpj participante det
	 */
	public void setCpfCnpjParticipanteDet(String cpfCnpjParticipanteDet) {
		this.cpfCnpjParticipanteDet = cpfCnpjParticipanteDet;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: grupoEconParticipante.
	 *
	 * @return grupoEconParticipante
	 */
	public String getGrupoEconParticipante() {
		return grupoEconParticipante;
	}

	/**
	 * Set: grupoEconParticipante.
	 *
	 * @param grupoEconParticipante the grupo econ participante
	 */
	public void setGrupoEconParticipante(String grupoEconParticipante) {
		this.grupoEconParticipante = grupoEconParticipante;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: nomeRazaoParticipanteDet.
	 *
	 * @return nomeRazaoParticipanteDet
	 */
	public String getNomeRazaoParticipanteDet() {
		return nomeRazaoParticipanteDet;
	}

	/**
	 * Set: nomeRazaoParticipanteDet.
	 *
	 * @param nomeRazaoParticipanteDet the nome razao participante det
	 */
	public void setNomeRazaoParticipanteDet(String nomeRazaoParticipanteDet) {
		this.nomeRazaoParticipanteDet = nomeRazaoParticipanteDet;
	}

	/**
	 * Get: segmentoParticipante.
	 *
	 * @return segmentoParticipante
	 */
	public String getSegmentoParticipante() {
		return segmentoParticipante;
	}

	/**
	 * Set: segmentoParticipante.
	 *
	 * @param segmentoParticipante the segmento participante
	 */
	public void setSegmentoParticipante(String segmentoParticipante) {
		this.segmentoParticipante = segmentoParticipante;
	}

	/**
	 * Get: subSegmentoParticipante.
	 *
	 * @return subSegmentoParticipante
	 */
	public String getSubSegmentoParticipante() {
		return subSegmentoParticipante;
	}

	/**
	 * Set: subSegmentoParticipante.
	 *
	 * @param subSegmentoParticipante the sub segmento participante
	 */
	public void setSubSegmentoParticipante(String subSegmentoParticipante) {
		this.subSegmentoParticipante = subSegmentoParticipante;
	}

	/**
	 * Set: subSegRepresentante.
	 *
	 * @param subSegRepresentante the sub seg representante
	 */
	public void setSubSegRepresentante(String subSegRepresentante) {
		this.subSegRepresentante = subSegRepresentante;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: itemFiltroTerceiroSelecionado.
	 *
	 * @return itemFiltroTerceiroSelecionado
	 */
	public String getItemFiltroTerceiroSelecionado() {
		return itemFiltroTerceiroSelecionado;
	}

	/**
	 * Set: itemFiltroTerceiroSelecionado.
	 *
	 * @param itemFiltroTerceiroSelecionado the item filtro terceiro selecionado
	 */
	public void setItemFiltroTerceiroSelecionado(
			String itemFiltroTerceiroSelecionado) {
		this.itemFiltroTerceiroSelecionado = itemFiltroTerceiroSelecionado;
	}

	/**
	 * Is bloqueia rdo terceiro.
	 *
	 * @return true, if is bloqueia rdo terceiro
	 */
	public boolean isBloqueiaRdoTerceiro() {
		return bloqueiaRdoTerceiro;
	}

	/**
	 * Set: bloqueiaRdoTerceiro.
	 *
	 * @param bloqueiaRdoTerceiro the bloqueia rdo terceiro
	 */
	public void setBloqueiaRdoTerceiro(boolean bloqueiaRdoTerceiro) {
		this.bloqueiaRdoTerceiro = bloqueiaRdoTerceiro;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: cdControleCnpj.
	 *
	 * @return cdControleCnpj
	 */
	public Integer getCdControleCnpj() {
		return cdControleCnpj;
	}

	/**
	 * Set: cdControleCnpj.
	 *
	 * @param cdControleCnpj the cd controle cnpj
	 */
	public void setCdControleCnpj(Integer cdControleCnpj) {
		this.cdControleCnpj = cdControleCnpj;
	}

	/**
	 * Get: cdCpfCnpj.
	 *
	 * @return cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}

	/**
	 * Set: cdCpfCnpj.
	 *
	 * @param cdCpfCnpj the cd cpf cnpj
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}

	/**
	 * Get: cdFilialCnpj.
	 *
	 * @return cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}

	/**
	 * Set: cdFilialCnpj.
	 *
	 * @param cdFilialCnpj the cd filial cnpj
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}

	/**
	 * Get: cdControleCpf.
	 *
	 * @return cdControleCpf
	 */
	public Integer getCdControleCpf() {
		return cdControleCpf;
	}

	/**
	 * Set: cdControleCpf.
	 *
	 * @param cdControleCpf the cd controle cpf
	 */
	public void setCdControleCpf(Integer cdControleCpf) {
		this.cdControleCpf = cdControleCpf;
	}

	/**
	 * Get: cdCpf.
	 *
	 * @return cdCpf
	 */
	public Long getCdCpf() {
		return cdCpf;
	}

	/**
	 * Set: cdCpf.
	 *
	 * @param cdCpf the cd cpf
	 */
	public void setCdCpf(Long cdCpf) {
		this.cdCpf = cdCpf;
	}

	/**
	 * Get: inscricaoRastreada.
	 *
	 * @return inscricaoRastreada
	 */
	public String getInscricaoRastreada() {
		return inscricaoRastreada;
	}

	/**
	 * Set: inscricaoRastreada.
	 *
	 * @param inscricaoRastreada the inscricao rastreada
	 */
	public void setInscricaoRastreada(String inscricaoRastreada) {
		this.inscricaoRastreada = inscricaoRastreada;
	}

	/**
	 * Get: agendarTitulosRastreados.
	 *
	 * @return agendarTitulosRastreados
	 */
	public String getAgendarTitulosRastreados() {
		return agendarTitulosRastreados;
	}

	/**
	 * Set: agendarTitulosRastreados.
	 *
	 * @param agendarTitulosRastreados the agendar titulos rastreados
	 */
	public void setAgendarTitulosRastreados(String agendarTitulosRastreados) {
		this.agendarTitulosRastreados = agendarTitulosRastreados;
	}

	/**
	 * Get: bloquearEmissaoParcelas.
	 *
	 * @return bloquearEmissaoParcelas
	 */
	public String getBloquearEmissaoParcelas() {
		return bloquearEmissaoParcelas;
	}

	/**
	 * Set: bloquearEmissaoParcelas.
	 *
	 * @param bloquearEmissaoParcelas the bloquear emissao parcelas
	 */
	public void setBloquearEmissaoParcelas(String bloquearEmissaoParcelas) {
		this.bloquearEmissaoParcelas = bloquearEmissaoParcelas;
	}

	/**
	 * Get: dtInicioBloqEmissaoPapeleta.
	 *
	 * @return dtInicioBloqEmissaoPapeleta
	 */
	public String getDtInicioBloqEmissaoPapeleta() {
		return dtInicioBloqEmissaoPapeleta;
	}

	/**
	 * Set: dtInicioBloqEmissaoPapeleta.
	 *
	 * @param dtInicioBloqEmissaoPapeleta the dt inicio bloq emissao papeleta
	 */
	public void setDtInicioBloqEmissaoPapeleta(
			String dtInicioBloqEmissaoPapeleta) {
		this.dtInicioBloqEmissaoPapeleta = dtInicioBloqEmissaoPapeleta;
	}

	/**
	 * Get: dtInicioRastreamento.
	 *
	 * @return dtInicioRastreamento
	 */
	public String getDtInicioRastreamento() {
		return dtInicioRastreamento;
	}

	/**
	 * Set: dtInicioRastreamento.
	 *
	 * @param dtInicioRastreamento the dt inicio rastreamento
	 */
	public void setDtInicioRastreamento(String dtInicioRastreamento) {
		this.dtInicioRastreamento = dtInicioRastreamento;
	}

	/**
	 * Get: rastrearNotasFiscais.
	 *
	 * @return rastrearNotasFiscais
	 */
	public String getRastrearNotasFiscais() {
		return rastrearNotasFiscais;
	}

	/**
	 * Set: rastrearNotasFiscais.
	 *
	 * @param rastrearNotasFiscais the rastrear notas fiscais
	 */
	public void setRastrearNotasFiscais(String rastrearNotasFiscais) {
		this.rastrearNotasFiscais = rastrearNotasFiscais;
	}

	/**
	 * Get: filtroIdentificaoImpl.
	 *
	 * @return filtroIdentificaoImpl
	 */
	public IFiltroIdentificaoService getFiltroIdentificaoImpl() {
		return filtroIdentificaoImpl;
	}

	/**
	 * Set: filtroIdentificaoImpl.
	 *
	 * @param filtroIdentificaoImpl the filtro identificao impl
	 */
	public void setFiltroIdentificaoImpl(
			IFiltroIdentificaoService filtroIdentificaoImpl) {
		this.filtroIdentificaoImpl = filtroIdentificaoImpl;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: cdControleCnpjFiltro.
	 *
	 * @return cdControleCnpjFiltro
	 */
	public Integer getCdControleCnpjFiltro() {
		return cdControleCnpjFiltro;
	}

	/**
	 * Set: cdControleCnpjFiltro.
	 *
	 * @param cdControleCnpjFiltro the cd controle cnpj filtro
	 */
	public void setCdControleCnpjFiltro(Integer cdControleCnpjFiltro) {
		this.cdControleCnpjFiltro = cdControleCnpjFiltro;
	}

	/**
	 * Get: cdControleCpfFiltro.
	 *
	 * @return cdControleCpfFiltro
	 */
	public Integer getCdControleCpfFiltro() {
		return cdControleCpfFiltro;
	}

	/**
	 * Set: cdControleCpfFiltro.
	 *
	 * @param cdControleCpfFiltro the cd controle cpf filtro
	 */
	public void setCdControleCpfFiltro(Integer cdControleCpfFiltro) {
		this.cdControleCpfFiltro = cdControleCpfFiltro;
	}

	/**
	 * Get: cdCpfCnpjFiltro.
	 *
	 * @return cdCpfCnpjFiltro
	 */
	public Long getCdCpfCnpjFiltro() {
		return cdCpfCnpjFiltro;
	}

	/**
	 * Set: cdCpfCnpjFiltro.
	 *
	 * @param cdCpfCnpjFiltro the cd cpf cnpj filtro
	 */
	public void setCdCpfCnpjFiltro(Long cdCpfCnpjFiltro) {
		this.cdCpfCnpjFiltro = cdCpfCnpjFiltro;
	}

	/**
	 * Get: cdCpfFiltro.
	 *
	 * @return cdCpfFiltro
	 */
	public Long getCdCpfFiltro() {
		return cdCpfFiltro;
	}

	/**
	 * Set: cdCpfFiltro.
	 *
	 * @param cdCpfFiltro the cd cpf filtro
	 */
	public void setCdCpfFiltro(Long cdCpfFiltro) {
		this.cdCpfFiltro = cdCpfFiltro;
	}

	/**
	 * Get: cdFilialCnpjFiltro.
	 *
	 * @return cdFilialCnpjFiltro
	 */
	public Integer getCdFilialCnpjFiltro() {
		return cdFilialCnpjFiltro;
	}

	/**
	 * Set: cdFilialCnpjFiltro.
	 *
	 * @param cdFilialCnpjFiltro the cd filial cnpj filtro
	 */
	public void setCdFilialCnpjFiltro(Integer cdFilialCnpjFiltro) {
		this.cdFilialCnpjFiltro = cdFilialCnpjFiltro;
	}

	/**
	 * Get: itemFiltroIncluir.
	 *
	 * @return itemFiltroIncluir
	 */
	public String getItemFiltroIncluir() {
		return itemFiltroIncluir;
	}

	/**
	 * Set: itemFiltroIncluir.
	 *
	 * @param itemFiltroIncluir the item filtro incluir
	 */
	public void setItemFiltroIncluir(String itemFiltroIncluir) {
		this.itemFiltroIncluir = itemFiltroIncluir;
	}

	/**
	 * Get: dataInicioBloqEmissaoPapeleta.
	 *
	 * @return dataInicioBloqEmissaoPapeleta
	 */
	public Date getDataInicioBloqEmissaoPapeleta() {
		return dataInicioBloqEmissaoPapeleta;
	}

	/**
	 * Set: dataInicioBloqEmissaoPapeleta.
	 *
	 * @param dataInicioBloqEmissaoPapeleta the data inicio bloq emissao papeleta
	 */
	public void setDataInicioBloqEmissaoPapeleta(
			Date dataInicioBloqEmissaoPapeleta) {
		this.dataInicioBloqEmissaoPapeleta = dataInicioBloqEmissaoPapeleta;
	}

	/**
	 * Get: dataInicioRastreamento.
	 *
	 * @return dataInicioRastreamento
	 */
	public Date getDataInicioRastreamento() {
		return dataInicioRastreamento;
	}

	/**
	 * Set: dataInicioRastreamento.
	 *
	 * @param dataInicioRastreamento the data inicio rastreamento
	 */
	public void setDataInicioRastreamento(Date dataInicioRastreamento) {
		this.dataInicioRastreamento = dataInicioRastreamento;
	}

	/**
	 * Is valor radio.
	 *
	 * @return true, if is valor radio
	 */
	public boolean isValorRadio() {
		return valorRadio;
	}

	/**
	 * Get: listaGridControleTerceiros.
	 *
	 * @return listaGridControleTerceiros
	 */
	public List<SelectItem> getListaGridControleTerceiros() {
		return listaGridControleTerceiros;
	}

	/**
	 * Set: listaGridControleTerceiros.
	 *
	 * @param listaGridControleTerceiros the lista grid controle terceiros
	 */
	public void setListaGridControleTerceiros(
			List<SelectItem> listaGridControleTerceiros) {
		this.listaGridControleTerceiros = listaGridControleTerceiros;
	}

	/**
	 * Get: listaGridTerceiros.
	 *
	 * @return listaGridTerceiros
	 */
	public List<OcorrenciasListarTercParticipanteContratoSaidaDTO> getListaGridTerceiros() {
		return listaGridTerceiros;
	}

	/**
	 * Set: listaGridTerceiros.
	 *
	 * @param listaGridTerceiros the lista grid terceiros
	 */
	public void setListaGridTerceiros(
			List<OcorrenciasListarTercParticipanteContratoSaidaDTO> listaGridTerceiros) {
		this.listaGridTerceiros = listaGridTerceiros;
	}

	/**
	 * Set: valorRadio.
	 *
	 * @param valorRadio the valor radio
	 */
	public void setValorRadio(boolean valorRadio) {
		this.valorRadio = valorRadio;
	}

	/**
	 * Get: itemSelecionadoTerceiros.
	 *
	 * @return itemSelecionadoTerceiros
	 */
	public Integer getItemSelecionadoTerceiros() {
		return itemSelecionadoTerceiros;
	}

	/**
	 * Set: itemSelecionadoTerceiros.
	 *
	 * @param itemSelecionadoTerceiros the item selecionado terceiros
	 */
	public void setItemSelecionadoTerceiros(Integer itemSelecionadoTerceiros) {
		this.itemSelecionadoTerceiros = itemSelecionadoTerceiros;
	}

	/**
	 * Get: rdoAgendarTitRastreados.
	 *
	 * @return rdoAgendarTitRastreados
	 */
	public String getRdoAgendarTitRastreados() {
		return rdoAgendarTitRastreados;
	}

	/**
	 * Set: rdoAgendarTitRastreados.
	 *
	 * @param rdoAgendarTitRastreados the rdo agendar tit rastreados
	 */
	public void setRdoAgendarTitRastreados(String rdoAgendarTitRastreados) {
		this.rdoAgendarTitRastreados = rdoAgendarTitRastreados;
	}

	/**
	 * Get: rdoBloqEmissaoPapeleta.
	 *
	 * @return rdoBloqEmissaoPapeleta
	 */
	public String getRdoBloqEmissaoPapeleta() {
		return rdoBloqEmissaoPapeleta;
	}

	/**
	 * Set: rdoBloqEmissaoPapeleta.
	 *
	 * @param rdoBloqEmissaoPapeleta the rdo bloq emissao papeleta
	 */
	public void setRdoBloqEmissaoPapeleta(String rdoBloqEmissaoPapeleta) {
		this.rdoBloqEmissaoPapeleta = rdoBloqEmissaoPapeleta;
	}

	/**
	 * Get: rdoRastrearNotasFiscais.
	 *
	 * @return rdoRastrearNotasFiscais
	 */
	public String getRdoRastrearNotasFiscais() {
		return rdoRastrearNotasFiscais;
	}

	/**
	 * Set: rdoRastrearNotasFiscais.
	 *
	 * @param rdoRastrearNotasFiscais the rdo rastrear notas fiscais
	 */
	public void setRdoRastrearNotasFiscais(String rdoRastrearNotasFiscais) {
		this.rdoRastrearNotasFiscais = rdoRastrearNotasFiscais;
	}

	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Get: dataHoje.
	 *
	 * @return dataHoje
	 */
	public Date getDataHoje() {
		return dataHoje;
	}

	/**
	 * Set: dataHoje.
	 *
	 * @param dataHoje the data hoje
	 */
	public void setDataHoje(Date dataHoje) {
		this.dataHoje = dataHoje;
	}

}
