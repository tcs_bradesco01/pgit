/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.manterdataultpagtoefetivado
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.manterdataultpagtoefetivado;


import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.IConDataUltPagamentoEfetivadoService;
import br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.bean.ListarUltimaDataPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.condataultpagamentoefetivado.bean.ListarUltimaDataPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;

/**
 * Nome: ConDataUltPagamentoEfetivadoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConDataUltPagamentoEfetivadoBean {
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo comboService. */
	private IComboService comboService;	
	
	/** Atributo conDataUltPagamentoEfetivadoServiceImpl. */
	private IConDataUltPagamentoEfetivadoService conDataUltPagamentoEfetivadoServiceImpl;
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;
	
	/** Atributo consultarPagamentosIndividualServiceImpl. */
	private IConsultarPagamentosIndividualService consultarPagamentosIndividualServiceImpl;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();	
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaGrid. */
	private List<ListarUltimaDataPagtoSaidaDTO> listaGrid;
	
	/** Atributo listaServico. */
	private List<SelectItem> listaServico =  new ArrayList<SelectItem>();
	
	/** Atributo listaModalidade. */
	private List<SelectItem> listaModalidade =  new ArrayList<SelectItem>();
	
	/** Atributo servicoFiltro. */
	private Integer servicoFiltro;
	
	/** Atributo modalidadeFiltro. */
	private Integer modalidadeFiltro;

	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	/** Atributo manutencao. */
	private boolean manutencao;
	
	/** Atributo habilitaConsultaDebito. */
	private boolean habilitaConsultaDebito;
	
	/** Atributo foco. */
	private String foco;
	
	/** Atributo tipoTela. */
	private Integer tipoTela;
	
	
	
	
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt){
		consultar();
		return "";
	}
	
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		//Tela de Filtro
		filtroAgendamentoEfetivacaoEstornoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean.setPaginaRetorno("conDataUltPagamentoEfetivado");
		filtroAgendamentoEfetivacaoEstornoBean.setEmpresaGestoraFiltro(2269651L);
		
		//Limpar Variaveis
		setListaControleRadio(null);
		setListaGrid(null);
		setItemSelecionadoLista(null);
		
		//Carregamento dos combos
		listarTipoServico();
		listarModalidades();

		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
		setDisableArgumentosConsulta(false);
	}
	
	/**
	 * Controlar argumentos pesquisa.
	 */
	public void controlarArgumentosPesquisa(){
		
		if (filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro() == null || filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro().equals("") ||
				filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro() == null || filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro().equals("") ||
				filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro() == null || filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro().equals("") ||
				filtroAgendamentoEfetivacaoEstornoBean.getDigitoContaDebitoFiltro() == null || filtroAgendamentoEfetivacaoEstornoBean.getDigitoContaDebitoFiltro().equals("")){
			setHabilitaConsultaDebito(false);
		}else{
			setHabilitaConsultaDebito(true);
		}
	}
	
	/**
	 * Limpar.
	 */
	public void limpar(){
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);		
		setServicoFiltro(0);		
		listarTipoServico();
		listarModalidades();
		setListaGrid(null);
		setHabilitaArgumentosPesquisa(false);
		setHabilitaConsultaDebito(false);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
	}
	
	/**
	 * Limpar grid consultar.
	 */
	public void limparGridConsultar(){
		setListaControleRadio(null);
		setListaGrid(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
	}
	
	/**
	 * Listar tipo servico.
	 */
	public void listarTipoServico(){
		try{
		    listaServico = new ArrayList<SelectItem>();
		
		List<ConsultarServicoPagtoSaidaDTO> list = comboService.consultarServicoPagto();

		for (ConsultarServicoPagtoSaidaDTO saida : list){
			listaServico.add(new SelectItem(saida.getCdServico(), saida.getDsServico()));
		}
		}catch(PdcAdapterFunctionalException p){
			listaServico = new ArrayList<SelectItem>();
		}
	}	
	
	/**
	 * Listar modalidades.
	 */
	public void listarModalidades(){
		try{
		setModalidadeFiltro(0);
		if ( getServicoFiltro() != null && getServicoFiltro() != 0){ 
			listaModalidade = new ArrayList<SelectItem>();
			
			List<ConsultarModalidadePagtoSaidaDTO> list = comboService.consultarModalidadePagto(getServicoFiltro());
	
			for (ConsultarModalidadePagtoSaidaDTO saida : list){
				listaModalidade.add(new SelectItem(saida.getCdProdutoOperacaoRelacionado(), saida.getDsProdutoOperacaoRelacionado()));
			}
		}else{
			listaModalidade = new ArrayList<SelectItem>();
		}
		}catch(PdcAdapterFunctionalException p){
			listaModalidade = new ArrayList<SelectItem>();
		}
	}
	
	
	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar(){
		try{
			ListarUltimaDataPagtoEntradaDTO entrada = new ListarUltimaDataPagtoEntradaDTO();
			
			/*  Se selecionado �Filtrar por Cliente� ou �Filtrar por Contrato� mover �1�.
			 *  Se selecionado �Filtrar por Conta D�bito� mover �2�;*/
			/*
			if (filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("0") ||
					filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("1")){ 	
				entrada.setCdTipoPesquisa(1);
			}	
			else{
				entrada.setCdTipoPesquisa(2);
			}	
			*/
	
			if ( filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("0") ||  filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("1")){
				entrada.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato() != null ? filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato()	: 0L);
				entrada.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio() != null ? filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio() : 0);
				entrada.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio() != null ? filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio() : 0L);				
			}
		
			if ( filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("2") ){
				/* Se selecionado �Filtrar por Conta D�bito� mover zeros para os cambos abaixo */
				entrada.setCdPessoaJuridicaContrato(0L);
				entrada.setCdTipoContratoNegocio(0);
				entrada.setNrSequenciaContratoNegocio(0L); 
			}	
			
			/* Se estiver selecionado "Filtrar por Conta D�bito", passar os campos deste filtro
			   Se estiver selecionado outro tipo de filtro, verificar se o "Conta D�bito" dos argumentos de pesquisa esta marcado
			   Se estiver marcado, passar os valores correspondentes da tela,caso contr�rio passar zeros
			*/
			/*
			if ( filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("2") ){
				entrada.setCdBancoDebito(filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro());
				entrada.setCdAgenciaDebito(filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro());
				entrada.setCdContaDebito(filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro());
				entrada.setCdDigitoContaDebito(filtroAgendamentoEfetivacaoEstornoBean.getDigitoContaDebitoFiltro());			
			}
			else
			{				
					entrada.setCdBancoDebito(0);
					entrada.setCdAgenciaDebito(0);
					entrada.setCdContaDebito(0L);
					entrada.setCdDigitoContaDebito("00");				
			}
			*/
		
			entrada.setCdProdutoServicoOperacao(getServicoFiltro());
			entrada.setCdProdutoServicoRelacionado(getModalidadeFiltro());
			
	
			setListaGrid(getConDataUltPagamentoEfetivadoServiceImpl().listarUltimaDataPagto(entrada));
			
			setItemSelecionadoLista(null);			
			this.listaControleRadio =  new ArrayList<SelectItem>();
			
			for (int i = 0; i < getListaGrid().size();i++) {
				this.listaControleRadio.add(new SelectItem(i," "));
			}
			setDisableArgumentosConsulta(true);
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setDisableArgumentosConsulta(false);
		}	
		
		return "";
	}
		
	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt){
		setListaGrid(null);
		setHabilitaArgumentosPesquisa(false);	
		setModalidadeFiltro(0);
		setServicoFiltro(0);
		listarModalidades();
		setHabilitaConsultaDebito(false);
		setItemSelecionadoLista(null);
	}	
		
	/**
	 * Limpar radios principais.
	 */
	public void limparRadiosPrincipais(){
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();	
		setListaGrid(null);
		setHabilitaArgumentosPesquisa(false);	
		setModalidadeFiltro(0);
		setServicoFiltro(0);
		listarModalidades();
		setHabilitaConsultaDebito(false);
		setItemSelecionadoLista(null);
		
	} 
	
	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente(){
		//Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		setListaGrid(null);
		setHabilitaArgumentosPesquisa(false);	
		setModalidadeFiltro(0);
		setServicoFiltro(0);
		listarModalidades();
		setHabilitaConsultaDebito(false);
		setItemSelecionadoLista(null);
		
		return "";

	}	
	

	
	/**
	 * Voltar detalhe.
	 *
	 * @return the string
	 */
	public String voltarDetalhe(){
		setItemSelecionadoLista(null);
		
		return "VOLTAR_CONSULTAR";
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: conDataUltPagamentoEfetivadoServiceImpl.
	 *
	 * @return conDataUltPagamentoEfetivadoServiceImpl
	 */
	public IConDataUltPagamentoEfetivadoService getConDataUltPagamentoEfetivadoServiceImpl() {
		return conDataUltPagamentoEfetivadoServiceImpl;
	}

	/**
	 * Set: conDataUltPagamentoEfetivadoServiceImpl.
	 *
	 * @param conDataUltPagamentoEfetivadoServiceImpl the con data ult pagamento efetivado service impl
	 */
	public void setConDataUltPagamentoEfetivadoServiceImpl(
			IConDataUltPagamentoEfetivadoService conDataUltPagamentoEfetivadoServiceImpl) {
		this.conDataUltPagamentoEfetivadoServiceImpl = conDataUltPagamentoEfetivadoServiceImpl;
	}

	/**
	 * Get: consultarPagamentosIndividualServiceImpl.
	 *
	 * @return consultarPagamentosIndividualServiceImpl
	 */
	public IConsultarPagamentosIndividualService getConsultarPagamentosIndividualServiceImpl() {
		return consultarPagamentosIndividualServiceImpl;
	}

	/**
	 * Set: consultarPagamentosIndividualServiceImpl.
	 *
	 * @param consultarPagamentosIndividualServiceImpl the consultar pagamentos individual service impl
	 */
	public void setConsultarPagamentosIndividualServiceImpl(
			IConsultarPagamentosIndividualService consultarPagamentosIndividualServiceImpl) {
		this.consultarPagamentosIndividualServiceImpl = consultarPagamentosIndividualServiceImpl;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Get: foco.
	 *
	 * @return foco
	 */
	public String getFoco() {
		return foco;
	}

	/**
	 * Set: foco.
	 *
	 * @param foco the foco
	 */
	public void setFoco(String foco) {
		this.foco = foco;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}

	/**
	 * Is habilita consulta debito.
	 *
	 * @return true, if is habilita consulta debito
	 */
	public boolean isHabilitaConsultaDebito() {
		return habilitaConsultaDebito;
	}

	/**
	 * Set: habilitaConsultaDebito.
	 *
	 * @param habilitaConsultaDebito the habilita consulta debito
	 */
	public void setHabilitaConsultaDebito(boolean habilitaConsultaDebito) {
		this.habilitaConsultaDebito = habilitaConsultaDebito;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarUltimaDataPagtoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarUltimaDataPagtoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: listaModalidade.
	 *
	 * @return listaModalidade
	 */
	public List<SelectItem> getListaModalidade() {
		return listaModalidade;
	}

	/**
	 * Set: listaModalidade.
	 *
	 * @param listaModalidade the lista modalidade
	 */
	public void setListaModalidade(List<SelectItem> listaModalidade) {
		this.listaModalidade = listaModalidade;
	}

	/**
	 * Get: listaServico.
	 *
	 * @return listaServico
	 */
	public List<SelectItem> getListaServico() {
		return listaServico;
	}

	/**
	 * Set: listaServico.
	 *
	 * @param listaServico the lista servico
	 */
	public void setListaServico(List<SelectItem> listaServico) {
		this.listaServico = listaServico;
	}

	/**
	 * Is manutencao.
	 *
	 * @return true, if is manutencao
	 */
	public boolean isManutencao() {
		return manutencao;
	}

	/**
	 * Set: manutencao.
	 *
	 * @param manutencao the manutencao
	 */
	public void setManutencao(boolean manutencao) {
		this.manutencao = manutencao;
	}

	/**
	 * Get: modalidadeFiltro.
	 *
	 * @return modalidadeFiltro
	 */
	public Integer getModalidadeFiltro() {
		return modalidadeFiltro;
	}

	/**
	 * Set: modalidadeFiltro.
	 *
	 * @param modalidadeFiltro the modalidade filtro
	 */
	public void setModalidadeFiltro(Integer modalidadeFiltro) {
		this.modalidadeFiltro = modalidadeFiltro;
	}

	/**
	 * Get: servicoFiltro.
	 *
	 * @return servicoFiltro
	 */
	public Integer getServicoFiltro() {
		return servicoFiltro;
	}

	/**
	 * Set: servicoFiltro.
	 *
	 * @param servicoFiltro the servico filtro
	 */
	public void setServicoFiltro(Integer servicoFiltro) {
		this.servicoFiltro = servicoFiltro;
	}

	/**
	 * Get: tipoTela.
	 *
	 * @return tipoTela
	 */
	public Integer getTipoTela() {
		return tipoTela;
	}

	/**
	 * Set: tipoTela.
	 *
	 * @param tipoTela the tipo tela
	 */
	public void setTipoTela(Integer tipoTela) {
		this.tipoTela = tipoTela;
	}
	


	
}