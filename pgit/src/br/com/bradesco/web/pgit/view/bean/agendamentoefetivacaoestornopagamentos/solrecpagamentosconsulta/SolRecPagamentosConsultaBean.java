/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.solrecpagamentosconsulta
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.solrecpagamentosconsulta;

import java.math.BigDecimal;
import java.util.Date;

import javax.faces.event.ActionEvent;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.ISolRecPagamentosConsultaService;
import br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.bean.SolicitarRecuparacaoPagtosConsultaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagamentosconsulta.bean.SolicitarRecuparacaoPagtosConsultaSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: SolRecPagamentosConsultaBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class SolRecPagamentosConsultaBean extends PagamentosBean {

	/** Atributo consultasServiceImpl. */
	private IConsultasService consultasServiceImpl;

	/** Atributo solRecPagamentosConServiceImpl. */
	private ISolRecPagamentosConsultaService solRecPagamentosConServiceImpl;

	// incSolRecPagamentosConsulta
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo panelBotoes. */
	private boolean panelBotoes;

	/** Atributo chkNumeroPagamento. */
	private boolean chkNumeroPagamento;

	private boolean chkinfoContas;

	private boolean chkDestino;

	private Integer chkCntCredito = 0;

	private boolean chkCntCreditob;

	private Integer chkCntPagamento = 0;

	private Integer chkCntTemp = 0;

	private Integer chkCntSalario = 0;

	/** Atributo chkValorPagamento. */
	private boolean chkValorPagamento;

	/** Atributo chkLinhaDigitavel. */
	private boolean chkLinhaDigitavel;

	/** Atributo chkIdentificacaoRecebedor. */
	private boolean chkIdentificacaoRecebedor;

	/** Atributo btnConsultar. */
	private boolean btnConsultar;

	/** Atributo dtProgramadaRecuperacao. */
	private Date dtProgramadaRecuperacao;

	// confirma��o
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;

	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;

	/** Atributo dsEmpresa. */
	private String dsEmpresa;

	/** Atributo dsTipoContrato. */
	private String dsTipoContrato;

	/** Atributo nroContrato. */
	private String nroContrato;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo dsTipoServico. */
	private String dsTipoServico;

	/** Atributo dsModalidade. */
	private String dsModalidade;

	/** Atributo dsSituacaoPagamento. */
	private String dsSituacaoPagamento;

	/** Atributo dsMotivoSituacaoPagamento. */
	private String dsMotivoSituacaoPagamento;

	/** Atributo dsTipoInscricao. */
	private String dsTipoInscricao;

	/** Atributo dsTipoConta. */
	private String dsTipoConta;

	/** Atributo dsTipoContaDebito. */
	private String dsTipoContaDebito;

	/** Atributo data1. */
	private String data1;

	/** Atributo data2. */
	private String data2;

	/** Atributo dsInscricaoFavorecio. */
	private String dsInscricaoFavorecio;

	/** Atributo dsInscricaoBeneficiario. */
	private String dsInscricaoBeneficiario;

	/** Atributo contaCredFormatado. */
	private String contaCredFormatado;

	/** Atributo contaDebFormatado. */
	private String contaDebFormatado;

	/** Atributo descricaoAgenciaContaDebitoBancaria. */
	private String descricaoAgenciaContaDebitoBancaria;

	/** Atributo descricaoTipoContaDebito. */
	private String descricaoTipoContaDebito;

	/** Atributo descricaoAgenciaDebito. */
	private String descricaoAgenciaDebito;

	/** Atributo descricaoBancoDebito. */
	private String descricaoBancoDebito;

	/** Atributo descricaoContaDebito. */
	private String descricaoContaDebito;

	/** Atributo descricaoAgenciaCredito. */
	private String descricaoAgenciaCredito;

	/** Atributo descricaoBancoCredito. */
	private String descricaoBancoCredito;

	/** Atributo descricaoContaCredito. */
	private String descricaoContaCredito;

	/** Atributo descricaoTipoContaCredito. */
	private String descricaoTipoContaCredito;


	private Integer bancoSalario;
	private Integer agenciaSalario;
	private Long contaSalario;
	private String digitoSalario;

	private Integer destinoPagamento;
	private String descDestinoPagamento;

	private Integer bancoPgtoDestino;
	private String ispbPgtoDestino;
	private Long contaPgtoDestino;
	
	private Long loteInterno;
	private Integer numeroSolicitacao;
	private Integer cdDigitoBancariaContaCredito;

	// M�TODOS

	/**
	 * Iniciar tela.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		// Tela de Filtro
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
				"incSolRecPagamentosConsulta");

		// Carregamento dos combos
		listarTipoServicoPagto();
		listarInscricaoFavorecido();
		listarConsultarSituacaoPagamento();

		// inicializando variaveis
		setDtProgramadaRecuperacao(new Date());
		setHabilitaArgumentosPesquisa(false);
		setDisableArgumentosConsulta(false);
		setBtnConsultar(true);

		limparCampos();
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setClienteContratoSelecionado(false);
		getFiltroAgendamentoEfetivacaoEstornoBean().setVoltarParticipante(
				"incSolRecPagamentosConsulta");
		getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);
	}

	/**
	 * Limpar tela principal.
	 *
	 * @return the string
	 */
	public String limparTelaPrincipal() {
		// Limpar Argumentos de Pesquisa e Lista
		limparFiltroPrincipal();
		setHabilitaArgumentosPesquisa(false);
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(
				null);
		limparFavorecido();
		limparBeneficiario();
		setDisableArgumentosConsulta(false);
		setDtProgramadaRecuperacao(new Date());
		setChkNumeroPagamento(false);
		setChkValorPagamento(false);
		setChkLinhaDigitavel(false);
		setChkIdentificacaoRecebedor(false);
		setChkinfoContas(false);
		
		setCdBancoContaCredito(null);
		setCdAgenciaBancariaContaCredito(null);
		setCdContaBancariaContaCredito(null);
		setCdDigitoContaCredito(null);
		setChkCntCredito(null);

		setBancoSalario(null);
		setAgenciaSalario(null);
		setContaSalario(null);
		setDigitoSalario(null);
		setChkCntSalario(null);
		
		setBancoPgtoDestino(null);
		setIspbPgtoDestino(null);
		setContaPgtoDestino(null);
		setChkCntPagamento(null);
		
		setDestinoPagamento(null);

		return "";
	}

	/**
	 * Pesquisar.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void pesquisar(ActionEvent evt) {
		return;
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 * 
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		getFiltroAgendamentoEfetivacaoEstornoBean().limpar();

		return "";
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		limparCampos();
	}

	/**
	 * Avancar.
	 * 
	 * @return the string
	 */
	public String avancar() {
		limparVariaveis();
		limparCredito();
		limparDebito();
		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado() != null) {
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("0")) { // Filtrar por
				// cliente
				setNrCnpjCpf(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getNrCpfCnpjCliente());
				setDsRazaoSocial(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getDescricaoRazaoSocialCliente());
				setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getEmpresaGestoraDescCliente());
				setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getNumeroDescCliente());
				setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getDescricaoContratoDescCliente());
				setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getSituacaoDescCliente());
			} else {
				if (getFiltroAgendamentoEfetivacaoEstornoBean()
						.getTipoFiltroSelecionado().equals("1")) { // Filtrar
					// por
					// contrato
					setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getEmpresaGestoraDescContrato());
					setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNumeroDescContrato());
					setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getDescricaoContratoDescContrato());
					setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getSituacaoDescContrato());
				}
			}
		}
		if (getTipoServicoFiltro() != null
				&& getTipoServicoFiltro().intValue() != 0) {
			setDsTipoServico(getListaTipoServicoHash().get(
					getTipoServicoFiltro()));
		}

		if (getModalidadeFiltro() != null
				&& getModalidadeFiltro().intValue() != 0) {
			setDsModalidade(getListaModalidadeHash().get(getModalidadeFiltro()));
		}

		if (isChkSituacaoMotivo()) {
			if (getCboSituacaoPagamentoFiltro() != null
					&& getCboSituacaoPagamentoFiltro().intValue() != 0) {
				setDsSituacaoPagamento(getListaConsultarSituacaoPagamentoHash()
						.get(getCboSituacaoPagamentoFiltro()));
			}

			if (getCboMotivoSituacaoFiltro() != null
					&& getCboMotivoSituacaoFiltro().intValue() != 0) {
				setDsMotivoSituacaoPagamento(getListaConsultarMotivoSituacaoPagamentoHash()
						.get(getCboMotivoSituacaoFiltro()));
			}
		}

		if (isChkIdentificacaoRecebedor()) {
			if (getRadioPesquisaFiltroPrincipal().equals("3")) { // Favorecido
				if (getTipoInscricaoFiltro() != null
						&& getTipoInscricaoFiltro().intValue() != 0) {

					String aux = "";
					setDsTipoInscricao(getListaInscricaoFavorecidoHash().get(
							getTipoInscricaoFiltro()));

					if (getTipoInscricaoFiltro() == 1) {
						// cpf
						aux = PgitUtil.complementaDigito(getInscricaoFiltro(),
								11);
						setDsInscricaoFavorecio(CpfCnpjUtils
								.formatCpfCnpjCompleto(Long.parseLong(aux
										.substring(0, 9)), 0,
										Integer.parseInt(aux.substring(aux
												.length() - 2, aux.length()))));
					} else {
						// cnpj
						aux = PgitUtil.complementaDigito(getInscricaoFiltro(),
								15);
						setDsInscricaoFavorecio(CpfCnpjUtils
								.formatCpfCnpjCompleto(Long.parseLong(aux
										.substring(0, 9)), Integer.parseInt(aux
										.substring(9, 13)),
										Integer.parseInt(aux.substring(aux
												.length() - 2, aux.length()))));
					}

				}
			} else { // Benefici�rio
				if (getTipoBeneficiarioFiltro() != null
						&& getTipoBeneficiarioFiltro().intValue() != 0) {

					String aux = "";

					if (getTipoBeneficiarioFiltro() == 1) {
						// cpf
						aux = PgitUtil.complementaDigito(
								getInscricaoFiltroBeneficiario(), 11);
						setDsInscricaoFavorecio(CpfCnpjUtils
								.formatCpfCnpjCompleto(Long.parseLong(aux
										.substring(0, 8)), 0,
										Integer.parseInt(aux.substring(aux
												.length() - 2, aux.length()))));
					} else {
						// cnpj
						aux = PgitUtil.complementaDigito(
								getInscricaoFiltroBeneficiario(), 15);
						setDsInscricaoBeneficiario(CpfCnpjUtils
								.formatCpfCnpjCompleto(Long.parseLong(aux
										.substring(0, 8)), Integer.parseInt(aux
										.substring(8, 12)),
										Integer.parseInt(aux.substring(aux
												.length() - 2, aux.length()))));
					}

					setDsTipoInscricao(getListaInscricaoFavorecidoHash().get(
							getTipoBeneficiarioFiltro()));

				}
			}

		}

		if (isChkContaDebito()) {
			limparDebito();
			carregaDescricaoContaDebito(getCdAgenciaBancariaContaDebito(),
					getCdBancoContaDebito(), getCdContaBancariaContaDebito());
		}
		
		if (isChkHabilitaCredito()) {
			setCdBancoContaCredito(getCdBancoContaCredito());
			setCdAgenciaBancariaContaCredito(getCdAgenciaBancariaContaCredito());
			setCdContaBancariaContaCredito(getCdContaBancariaContaCredito());
			setCdDigitoBancariaContaCredito(getCdDigitoBancariaContaCredito());
		}

		if (isChkHabilitaSalario()) {
			setBancoSalario(getBancoSalario());
			setAgenciaSalario(getAgenciaSalario());
			setContaSalario(getContaSalario());
			setDigitoSalario(getDigitoSalario());
		}
		
		if (isChkHabilitaPagamento()) {
			setBancoPgtoDestino(getBancoPgtoDestino());
			setIspbPgtoDestino(getIspbPgtoDestino());
			setContaPgtoDestino(getContaPgtoDestino());
		}
		
		setData2(FormatarData.formataDiaMesAno(getDtProgramadaRecuperacao()));

		setContaCredFormatado(PgitUtil.formatConta(
				getCdContaBancariaContaCredito(), getCdDigitoContaCredito(),
				false));
		setContaDebFormatado(PgitUtil.formatConta(
				getCdContaBancariaContaDebito(), getCdDigitoContaContaDebito(),
				false));

		if (destinoPagamento == null) {

			BradescoFacesUtils.addInfoModalMessage(
					"FAVOR PREENCHER O CAMPO 'Destino'! ", false);
			return "";
		}
		if (destinoPagamento == 1) {
			setDescDestinoPagamento("Relat�rio");
		}
		if (destinoPagamento == 2) {
			setDescDestinoPagamento("Arquivo ISD");
		}
		if (destinoPagamento == 3) {
			setDescDestinoPagamento("Base Online");
		}

		return "AVANCAR";
	}

	/**
	 * Limpar credito.
	 */
	public void limparCredito() {
		setDescricaoTipoContaCredito("");
		setDescricaoAgenciaCredito("");
		setDescricaoBancoCredito("");
		setDescricaoContaCredito("");
	}

	/**
	 * Limpar debito.
	 */
	public void limparDebito() {
		setDescricaoTipoContaDebito("");
		setDescricaoAgenciaDebito("");
		setDescricaoBancoDebito("");
		setDescricaoContaDebito("");
	}
	
	/**
	 * Limpar salario.
	 */
	public void limparSalario() {
		setBancoSalario(null);
		setAgenciaSalario(null);
		setContaSalario(null);
		setDigitoSalario(null);
	}

	/**
	 * Limpar pagamento.
	 */
	public void limparPagamento() {
		setBancoPgtoDestino(null);
		setIspbPgtoDestino(null);
		setContaPgtoDestino(null);
	}
	
	/**
	 * Carrega descricao conta credito.
	 * 
	 * @param agencia
	 *            the agencia
	 * @param banco
	 *            the banco
	 * @param conta
	 *            the conta
	 */
	public void carregaDescricaoContaCredito(Integer agencia, Integer banco,
			Long conta) {
		
		try {
			ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO = new ConsultarDescBancoAgenciaContaEntradaDTO();
			consultarDescBancoAgenciaContaEntradaDTO.setCdBanco(banco);
			consultarDescBancoAgenciaContaEntradaDTO.setCdAgencia(agencia);
			consultarDescBancoAgenciaContaEntradaDTO.setCdConta(conta);
			consultarDescBancoAgenciaContaEntradaDTO
			.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
			ConsultarDescBancoAgenciaContaSaidaDTO consultarDescBancoAgenciaContaSaidaDTO = getConsultasServiceImpl()
			.consultarDescBancoAgenciaConta(
					consultarDescBancoAgenciaContaEntradaDTO);
			setDescricaoTipoContaCredito(consultarDescBancoAgenciaContaSaidaDTO
					.getDsTipoConta());
			setDescricaoAgenciaCredito(PgitUtil
					.formatAgencia(consultarDescBancoAgenciaContaSaidaDTO
							.getCdAgencia(),
							consultarDescBancoAgenciaContaSaidaDTO
							.getCdDigitoAgencia(),
							consultarDescBancoAgenciaContaSaidaDTO
							.getDsAgencia(), false));
			setDescricaoBancoCredito(PgitUtil.formatBanco(
					consultarDescBancoAgenciaContaSaidaDTO.getCdBanco(),
					consultarDescBancoAgenciaContaSaidaDTO.getDsBanco(), false));
			setDescricaoContaCredito(PgitUtil.formatConta(
					consultarDescBancoAgenciaContaSaidaDTO.getCdConta(),
					consultarDescBancoAgenciaContaSaidaDTO.getCdDigitoConta(),
					false));
		} catch (PdcAdapterFunctionalException e) {
			setDescricaoTipoContaCredito("");
			setDescricaoAgenciaCredito("");
			setDescricaoBancoCredito("");
			setDescricaoContaCredito("");
		}
	}

	/**
	 * Carrega descricao conta debito.
	 * 
	 * @param agencia
	 *            the agencia
	 * @param banco
	 *            the banco
	 * @param conta
	 *            the conta
	 */
	public void carregaDescricaoContaDebito(Integer agencia, Integer banco,
			Long conta) {

		try {
			ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO = new ConsultarDescBancoAgenciaContaEntradaDTO();
			consultarDescBancoAgenciaContaEntradaDTO.setCdBanco(banco);
			consultarDescBancoAgenciaContaEntradaDTO.setCdAgencia(agencia);
			consultarDescBancoAgenciaContaEntradaDTO.setCdConta(conta);
			consultarDescBancoAgenciaContaEntradaDTO
					.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
			ConsultarDescBancoAgenciaContaSaidaDTO consultarDescBancoAgenciaContaSaidaDTO = getConsultasServiceImpl()
					.consultarDescBancoAgenciaConta(
							consultarDescBancoAgenciaContaEntradaDTO);
			setDescricaoTipoContaDebito(consultarDescBancoAgenciaContaSaidaDTO
					.getDsTipoConta());
			setDescricaoAgenciaDebito(PgitUtil
					.formatAgencia(consultarDescBancoAgenciaContaSaidaDTO
							.getCdAgencia(),
							consultarDescBancoAgenciaContaSaidaDTO
									.getCdDigitoAgencia(),
							consultarDescBancoAgenciaContaSaidaDTO
									.getDsAgencia(), false));
			setDescricaoBancoDebito(PgitUtil.formatBanco(
					consultarDescBancoAgenciaContaSaidaDTO.getCdBanco(),
					consultarDescBancoAgenciaContaSaidaDTO.getDsBanco(), false));
			setDescricaoContaDebito(PgitUtil.formatConta(
					consultarDescBancoAgenciaContaSaidaDTO.getCdConta(),
					consultarDescBancoAgenciaContaSaidaDTO.getCdDigitoConta(),
					false));
		} catch (PdcAdapterFunctionalException e) {
			setDescricaoTipoContaDebito("");
			setDescricaoAgenciaDebito("");
			setDescricaoBancoDebito("");
			setDescricaoContaDebito("");
		}
	}

	/**
	 * Confirmar.
	 * 
	 * @return the string
	 */
	public String confirmar() {

		try {

			SolicitarRecuparacaoPagtosConsultaEntradaDTO entradaDTO = new SolicitarRecuparacaoPagtosConsultaEntradaDTO();

			entradaDTO.setCdFinalidadeRecuperacao(2);
			entradaDTO.setCdSelecaoRecuperacao(0);
			entradaDTO.setCdSolicitacaoPagamento(9);
			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado() != null) {
				if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0") || getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) {
					entradaDTO.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getCdPessoaJuridicaContrato());
					entradaDTO.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean().getCdTipoContratoNegocio());
					entradaDTO.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean().getNrSequenciaContratoNegocio());
				} else {
					entradaDTO.setCdPessoaJuridicaContrato(0L);
					entradaDTO.setCdTipoContratoNegocio(0);
					entradaDTO.setNrSequenciaContratoNegocio(0L);
				}
			}
			entradaDTO.setCdBancoDebito(getCdBancoContaDebito());
			entradaDTO.setCdAgenciaDebito(getCdAgenciaBancariaContaDebito());
			entradaDTO.setCdContaDebito(getCdContaBancariaContaDebito());
			entradaDTO.setCdDigitoContaDebito(getCdDigitoContaContaDebito());
			entradaDTO.setCdTipoContaDebito(getCmbTipoContaDebito());
			entradaDTO.setDtInicioPeriodoPesquisa(FormatarData.formataDiaMesAnoToPdc(getDataInicialPagamentoFiltro()));
			entradaDTO.setDtFimPeriodoPesquisa(FormatarData.formataDiaMesAnoToPdc(getDataFinalPagamentoFiltro()));
			entradaDTO.setCdProduto(getTipoServicoFiltro());
			entradaDTO.setCdProdutoRelacionado(getModalidadeFiltro());
			entradaDTO.setNrSequenciaArquivoRemessa(getNumeroRemessaFiltro() != null && !getNumeroRemessaFiltro().equals("") ? Long.parseLong(getNumeroRemessaFiltro()) : 0L);
			entradaDTO.setLoteInterno(getLoteInterno() != null && !getLoteInterno().equals("") ? getLoteInterno() : 0L);
			entradaDTO.setCdPessoa(getFiltroAgendamentoEfetivacaoEstornoBean().getCodigoParticipanteFiltro());
			entradaDTO.setCdControlePagamentoInicial(getNumeroPagamentoDeFiltro());
			entradaDTO.setCdControlePagamentoFinal(getNumeroPagamentoAteFiltro());
			entradaDTO.setVlrInicio(getValorPagamentoDeFiltro());
			entradaDTO.setVlFim(getValorPagamentoAteFiltro());
			entradaDTO.setCdSituacaoPagamento(getCboSituacaoPagamentoFiltro());
			entradaDTO.setCdMotivoSituacaoPagamento(getCboMotivoSituacaoFiltro());
			entradaDTO.setLinhaDigitavel(getLinhaDigitavel1()
					+ getLinhaDigitavel2() + getLinhaDigitavel3()
					+ getLinhaDigitavel4() + getLinhaDigitavel5()
					+ getLinhaDigitavel6() + getLinhaDigitavel7()
					+ getLinhaDigitavel8());

			// Filtrar por FAVORECIDO
			if (getRadioPesquisaFiltroPrincipal().equals("3")) {
				entradaDTO
						.setCdFavorecidoCliente(getCodigoFavorecidoFiltro() != null
								&& !getCodigoFavorecidoFiltro().equals("") ? Long
								.parseLong(getCodigoFavorecidoFiltro())
								: 0L);
				entradaDTO
						.setCdTipoInscricaoRecebedor(getTipoInscricaoFiltro());
				entradaDTO.setCdInscricaoRecebedor(0L);

				String aux = "";
				// Se cdTipoInscricaoRecebedor = 1 (CPF), Se Outro (CNPJ)
				if (getInscricaoFiltro() != null
						&& !getInscricaoFiltro().equals("")) {
					if (entradaDTO.getCdTipoInscricaoRecebedor() == 1) {
						// cpf
						aux = PgitUtil.complementaDigito(getInscricaoFiltro(),
								11);
						entradaDTO.setCdFilialCnpjRecebedor(0);
					} else {
						// cnpj
						aux = PgitUtil.complementaDigito(getInscricaoFiltro(),
								15);
						entradaDTO.setCdFilialCnpjRecebedor(Integer
								.parseInt(aux.substring(9, 13)));

					}
					entradaDTO.setCdCpfCnpjRecebedor(Long.parseLong(aux
							.substring(0, 9)));
					entradaDTO.setCdControleCpfRecebedor(Integer.parseInt(aux
							.substring(aux.length() - 2, aux.length())));
				} else {
					entradaDTO.setCdFilialCnpjRecebedor(0);
					entradaDTO.setCdCpfCnpjRecebedor(0L);
					entradaDTO.setCdControleCpfRecebedor(0);
				}

				entradaDTO
						.setCdInscricaoFavorecido((getInscricaoFiltro() != null && !getInscricaoFiltro()
								.equals("")) ? Long
								.parseLong(getInscricaoFiltro()) : 0L);
			}

			// Filtrar por BENEFICIARIO
			if (getRadioPesquisaFiltroPrincipal().equals("2")) {
				entradaDTO.setCdFavorecidoCliente(0L); // conforme solicitado
				// por Rodrigo via email
				// 01/03/2011
				entradaDTO
						.setCdTipoInscricaoRecebedor(getTipoBeneficiarioFiltro());
				entradaDTO.setCdInscricaoRecebedor(0L);

				String aux = "";
				// Se cdTipoInscricaoRecebedor = 1 (CPF), Se Outro (CNPJ)
				if (getInscricaoFiltroBeneficiario() != null
						&& !getInscricaoFiltroBeneficiario().equals("")) {
					if (entradaDTO.getCdTipoInscricaoRecebedor() == 1) {
						// cpf
						aux = PgitUtil.complementaDigito(
								getInscricaoFiltroBeneficiario(), 11);
						entradaDTO.setCdFilialCnpjRecebedor(0);
					} else {
						// cnpj
						aux = PgitUtil.complementaDigito(
								getInscricaoFiltroBeneficiario(), 15);
						entradaDTO.setCdFilialCnpjRecebedor(Integer
								.parseInt(aux.substring(9, 13)));

					}
					entradaDTO.setCdCpfCnpjRecebedor(Long.parseLong(aux
							.substring(0, 9)));
					entradaDTO.setCdControleCpfRecebedor(Integer.parseInt(aux
							.substring(aux.length() - 2, aux.length())));
				} else {
					entradaDTO.setCdFilialCnpjRecebedor(0);
					entradaDTO.setCdCpfCnpjRecebedor(0L);
					entradaDTO.setCdControleCpfRecebedor(0);
				}
				entradaDTO
						.setCdInscricaoFavorecido((getInscricaoFiltroBeneficiario() != null && !getInscricaoFiltroBeneficiario()
								.equals("")) ? Long
								.parseLong(getInscricaoFiltroBeneficiario())
								: 0L);
			}

			entradaDTO.setCdBancoCredito(PgitUtil.verificaIntegerNulo(getCdBancoContaCredito()));
			entradaDTO.setCdAgenciaCredito(PgitUtil.verificaIntegerNulo(getCdAgenciaBancariaContaCredito()));
			entradaDTO.setCdContaCredito(PgitUtil.verificaLongNulo(getCdContaBancariaContaCredito()));
			entradaDTO.setDigitoContaCredito(PgitUtil.verificaStringNula(getCdDigitoContaCredito()));
			entradaDTO.setCdTipoContaCredito(PgitUtil.verificaIntegerNulo(getCmbTipoConta()));

			entradaDTO.setDtRecuperacaoPagamento(FormatarData.formataDiaMesAno(getDtProgramadaRecuperacao()));
			entradaDTO.setVlTarifaPadrao(BigDecimal.ZERO);
			entradaDTO.setVlTarifaNegociacao(BigDecimal.ZERO);
			entradaDTO.setPercentualBonificacaoTarifa(BigDecimal.ZERO);
			
			entradaDTO.setBancoSalario(PgitUtil.verificaIntegerNulo(getBancoSalario()));
			entradaDTO.setAgenciaSalario(PgitUtil.verificaIntegerNulo(getAgenciaSalario()));
			entradaDTO.setContaSalario(PgitUtil.verificaLongNulo(getContaSalario()));	
			entradaDTO.setDigitoSalario(PgitUtil.verificaStringNula(getDigitoSalario()));
			
			entradaDTO.setBancoPgtoDestino(PgitUtil.verificaIntegerNulo(getBancoPgtoDestino()));			
			entradaDTO.setIspbPgtoDestino(PgitUtil.verificaStringNula(getIspbPgtoDestino()));
			entradaDTO.setContaPgtoDestino(PgitUtil.verificaLongNulo(getContaPgtoDestino()));
			
			entradaDTO.setDestinoPagamento(PgitUtil.verificaIntegerNulo(getDestinoPagamento()));

			SolicitarRecuparacaoPagtosConsultaSaidaDTO solicitarRecuparacaoPagtosConsultaSaidaDTO = getSolRecPagamentosConServiceImpl()
					.solicitarRecuparacaoPagtosConsulta(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("("
					+ solicitarRecuparacaoPagtosConsultaSaidaDTO
							.getCodMensagem() + ") "
					+ solicitarRecuparacaoPagtosConsultaSaidaDTO.getMensagem(),
					"incSolRecPagamentosConsulta",
					BradescoViewExceptionActionType.ACTION, false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}

		return "";
	}

	/**
	 * Limpar numero pagamento.
	 */
	public void limparNumeroPagamento() {
		setNumeroPagamentoDeFiltro("");
		setNumeroPagamentoAteFiltro("");
	}

	/**
	 * Limpar valor pagamento.
	 */
	public void limparValorPagamento() {
		setValorPagamentoDeFiltro(null);
		setValorPagamentoAteFiltro(null);
	}

	/**
	 * Limpar identificador recebedor.
	 */
	public void limparIdentificadorRecebedor() {
		limparFavorecidoBeneficiario();
		setRadioPesquisaFiltroPrincipal(null);
	}

	/**
	 * Limpar favorecido beneficiario.
	 */
	public void limparFavorecidoBeneficiario() {
		limparFavorecido();
		limparBeneficiario();
		setRadioFavorecidoFiltro(null);
	}

	/**
	 * Limpar linhas digitaveis.
	 */
	public void limparLinhasDigitaveis() {
		setLinhaDigitavel1("");
		setLinhaDigitavel2("");
		setLinhaDigitavel3("");
		setLinhaDigitavel4("");
		setLinhaDigitavel5("");
		setLinhaDigitavel6("");
		setLinhaDigitavel7("");
		setLinhaDigitavel8("");
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato consultar.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void limparArgsListaAposPesquisaClienteContratoConsultar(
			ActionEvent evt) {
		limparCamposConsultar();
	}

	/**
	 * Limpar campos consultar.
	 */
	public void limparCamposConsultar() {

	}

	/**
	 * Limpar conta credito.
	 */
	public void limparIndcConta() {

		if (chkCntCredito == 1) {
			chkCntPagamento = null;
			chkCntSalario = null;

			setChkHabilitaCredito(true);
			setChkHabilitaSalario(false);
			setChkHabilitaPagamento(false);

			bancoSalario = null;
			contaSalario = null;
			agenciaSalario = null;
			digitoSalario = null;
			
			bancoPgtoDestino= null;
			contaPgtoDestino= null;
			ispbPgtoDestino= null;
		}

	}

	public void limparIndcpag() {

		if (chkCntPagamento == 3) {
			chkCntCredito = null;
			chkCntSalario = null;

			setChkHabilitaCredito(false);
			setChkHabilitaSalario(false);
			setChkHabilitaPagamento(true);
			
			bancoSalario = null;
			contaSalario = null;
			agenciaSalario = null;
			digitoSalario = null;
			
			setCdBancoContaCredito(null);
			setCdContaBancariaContaCredito(null);
			setCdAgenciaBancariaContaCredito(null);
			setCdDigitoContaCredito(null);

		}
	}

	public void limparIndsalario() {

		if (chkCntSalario == 2) {
			chkCntPagamento = null;
			chkCntCredito = null;
			
			setChkHabilitaCredito(false);
			setChkHabilitaSalario(true);
			setChkHabilitaPagamento(false);
			
			bancoPgtoDestino= null;
			contaPgtoDestino= null;
			ispbPgtoDestino= null;
			
			setCdBancoContaCredito(null);
			setCdContaBancariaContaCredito(null);
			setCdAgenciaBancariaContaCredito(null);
			setCdDigitoContaCredito(null);
		}
	}

	public void limparInformacao() {
		chkCntSalario = 0;
		chkCntPagamento = 0;
		chkCntCredito = 0;

		setCdBancoContaCredito(null);
		setCdContaBancariaContaCredito(null);
		setCdAgenciaBancariaContaCredito(null);
		setCdDigitoContaCredito(null);
		
		bancoSalario = null;
		contaSalario = null;
		agenciaSalario = null;
		digitoSalario = null;
		
		bancoPgtoDestino= null;
		contaPgtoDestino= null;
		ispbPgtoDestino= null;
	}

	/**
	 * Habilita tipo conta.
	 * 
	 * @return the string
	 */
	public String habilitaTipoConta() {
		limparTipoConta();
		if (getCdBancoContaCredito() != null
				&& getCdAgenciaBancariaContaCredito() != null
				&& getCdContaBancariaContaCredito() != null) {
			setBtnConsultar(false);
		} else {
			setBtnConsultar(true);
		}

		return "";
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#limparBeneficiario()
	 */
	public void limparBeneficiario() {
		setCodigoBeneficiarioFiltro("");
		setTipoBeneficiarioFiltro(null);
		setOrgaoPagadorFiltro(null);
		setInscricaoFiltroBeneficiario("");
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#limparFavorecido()
	 */
	public void limparFavorecido() {
		setCodigoFavorecidoFiltro("");
		setInscricaoFiltro("");
		setTipoInscricaoFiltro(null);
	}

	/**
	 * Limpar variaveis.
	 */
	private void limparVariaveis() {
		setNrCnpjCpf(null);
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");
	}

	// NAVEGA��O

	/**
	 * Voltar.
	 * 
	 * @return the string
	 */
	public String voltar() {
		return "VOLTAR";
	}

	// GETTERS E SETTERS

	/**
	 * Get: consultasServiceImpl.
	 * 
	 * @return consultasServiceImpl
	 */
	public IConsultasService getConsultasServiceImpl() {
		return consultasServiceImpl;
	}

	/**
	 * Set: consultasServiceImpl.
	 * 
	 * @param consultasServiceImpl
	 *            the consultas service impl
	 */
	public void setConsultasServiceImpl(IConsultasService consultasServiceImpl) {
		this.consultasServiceImpl = consultasServiceImpl;
	}

	/**
	 * Is disable argumentos consulta.
	 * 
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 * 
	 * @param disableArgumentosConsulta
	 *            the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: itemSelecionadoLista.
	 * 
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 * 
	 * @param itemSelecionadoLista
	 *            the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Is panel botoes.
	 * 
	 * @return true, if is panel botoes
	 */
	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	/**
	 * Set: panelBotoes.
	 * 
	 * @param panelBotoes
	 *            the panel botoes
	 */
	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	/**
	 * Get: solRecPagamentosConServiceImpl.
	 * 
	 * @return solRecPagamentosConServiceImpl
	 */
	public ISolRecPagamentosConsultaService getSolRecPagamentosConServiceImpl() {
		return solRecPagamentosConServiceImpl;
	}

	/**
	 * Set: solRecPagamentosConServiceImpl.
	 * 
	 * @param solRecPagamentosConServiceImpl
	 *            the sol rec pagamentos con service impl
	 */
	public void setSolRecPagamentosConServiceImpl(
			ISolRecPagamentosConsultaService solRecPagamentosConServiceImpl) {
		this.solRecPagamentosConServiceImpl = solRecPagamentosConServiceImpl;
	}

	/**
	 * Get: dsContrato.
	 * 
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 * 
	 * @param dsContrato
	 *            the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 * 
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 * 
	 * @param dsEmpresa
	 *            the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 * 
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 * 
	 * @param dsRazaoSocial
	 *            the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: dsTipoContrato.
	 * 
	 * @return dsTipoContrato
	 */
	public String getDsTipoContrato() {
		return dsTipoContrato;
	}

	/**
	 * Set: dsTipoContrato.
	 * 
	 * @param dsTipoContrato
	 *            the ds tipo contrato
	 */
	public void setDsTipoContrato(String dsTipoContrato) {
		this.dsTipoContrato = dsTipoContrato;
	}

	/**
	 * Get: nrCnpjCpf.
	 * 
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 * 
	 * @param nrCnpjCpf
	 *            the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 * 
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 * 
	 * @param nroContrato
	 *            the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: cdSituacaoContrato.
	 * 
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 * 
	 * @param situacaoContrato
	 *            the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String situacaoContrato) {
		this.cdSituacaoContrato = situacaoContrato;
	}

	/**
	 * Get: dsPeriodoPagamento.
	 * 
	 * @return dsPeriodoPagamento
	 */
	public String getDsPeriodoPagamento() {
		if (getDataInicialPagamentoFiltro() != null
				&& getDataFinalPagamentoFiltro() != null) {
			return FormatarData
					.formataDiaMesAno(getDataInicialPagamentoFiltro())
					+ " a "
					+ FormatarData
							.formataDiaMesAno(getDataFinalPagamentoFiltro());
		}

		return "";

	}

	public boolean isChkNumeroPagamento() {
		return chkNumeroPagamento;
	}

	public void setChkNumeroPagamento(boolean chkNumeroPagamento) {
		this.chkNumeroPagamento = chkNumeroPagamento;
	}

	public boolean isChkinfoContas() {
		return chkinfoContas;
	}

	public void setChkinfoContas(boolean chkinfoContas) {
		this.chkinfoContas = chkinfoContas;
	}

	public boolean isChkDestino() {
		return chkDestino;
	}

	public void setChkDestino(boolean chkDestino) {
		this.chkDestino = chkDestino;
	}

	public Integer getChkCntCredito() {
		return chkCntCredito;
	}

	public void setChkCntCredito(Integer chkCntCredito) {
		this.chkCntCredito = chkCntCredito;
	}

	public boolean isChkCntCreditob() {
		return chkCntCreditob;
	}

	public void setChkCntCreditob(boolean chkCntCreditob) {
		this.chkCntCreditob = chkCntCreditob;
	}

	public Integer getChkCntPagamento() {
		return chkCntPagamento;
	}

	public void setChkCntPagamento(Integer chkCntPagamento) {
		this.chkCntPagamento = chkCntPagamento;
	}

	public Integer getChkCntTemp() {
		return chkCntTemp;
	}

	public void setChkCntTemp(Integer chkCntTemp) {
		this.chkCntTemp = chkCntTemp;
	}

	public Integer getChkCntSalario() {
		return chkCntSalario;
	}

	public void setChkCntSalario(Integer chkCntSalario) {
		this.chkCntSalario = chkCntSalario;
	}

	public boolean isChkValorPagamento() {
		return chkValorPagamento;
	}

	public void setChkValorPagamento(boolean chkValorPagamento) {
		this.chkValorPagamento = chkValorPagamento;
	}

	public boolean isChkLinhaDigitavel() {
		return chkLinhaDigitavel;
	}

	public void setChkLinhaDigitavel(boolean chkLinhaDigitavel) {
		this.chkLinhaDigitavel = chkLinhaDigitavel;
	}

	public boolean isChkIdentificacaoRecebedor() {
		return chkIdentificacaoRecebedor;
	}

	public void setChkIdentificacaoRecebedor(boolean chkIdentificacaoRecebedor) {
		this.chkIdentificacaoRecebedor = chkIdentificacaoRecebedor;
	}

	public boolean isBtnConsultar() {
		return btnConsultar;
	}

	public void setBtnConsultar(boolean btnConsultar) {
		this.btnConsultar = btnConsultar;
	}

	public Date getDtProgramadaRecuperacao() {
		return dtProgramadaRecuperacao;
	}

	public void setDtProgramadaRecuperacao(Date dtProgramadaRecuperacao) {
		this.dtProgramadaRecuperacao = dtProgramadaRecuperacao;
	}

	public String getDsTipoServico() {
		return dsTipoServico;
	}

	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	public String getDsModalidade() {
		return dsModalidade;
	}

	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}

	public String getDsSituacaoPagamento() {
		return dsSituacaoPagamento;
	}

	public void setDsSituacaoPagamento(String dsSituacaoPagamento) {
		this.dsSituacaoPagamento = dsSituacaoPagamento;
	}

	public String getDsMotivoSituacaoPagamento() {
		return dsMotivoSituacaoPagamento;
	}

	public void setDsMotivoSituacaoPagamento(String dsMotivoSituacaoPagamento) {
		this.dsMotivoSituacaoPagamento = dsMotivoSituacaoPagamento;
	}

	public String getDsTipoInscricao() {
		return dsTipoInscricao;
	}

	public void setDsTipoInscricao(String dsTipoInscricao) {
		this.dsTipoInscricao = dsTipoInscricao;
	}

	public String getDsTipoConta() {
		return dsTipoConta;
	}

	public void setDsTipoConta(String dsTipoConta) {
		this.dsTipoConta = dsTipoConta;
	}

	public String getDsTipoContaDebito() {
		return dsTipoContaDebito;
	}

	public void setDsTipoContaDebito(String dsTipoContaDebito) {
		this.dsTipoContaDebito = dsTipoContaDebito;
	}

	public String getData1() {
		return data1;
	}

	public void setData1(String data1) {
		this.data1 = data1;
	}

	public String getData2() {
		return data2;
	}

	public void setData2(String data2) {
		this.data2 = data2;
	}

	public String getDsInscricaoFavorecio() {
		return dsInscricaoFavorecio;
	}

	public void setDsInscricaoFavorecio(String dsInscricaoFavorecio) {
		this.dsInscricaoFavorecio = dsInscricaoFavorecio;
	}

	public String getDsInscricaoBeneficiario() {
		return dsInscricaoBeneficiario;
	}

	public void setDsInscricaoBeneficiario(String dsInscricaoBeneficiario) {
		this.dsInscricaoBeneficiario = dsInscricaoBeneficiario;
	}

	public String getContaCredFormatado() {
		return contaCredFormatado;
	}

	public void setContaCredFormatado(String contaCredFormatado) {
		this.contaCredFormatado = contaCredFormatado;
	}

	public String getContaDebFormatado() {
		return contaDebFormatado;
	}

	public void setContaDebFormatado(String contaDebFormatado) {
		this.contaDebFormatado = contaDebFormatado;
	}

	public String getDescricaoAgenciaContaDebitoBancaria() {
		return descricaoAgenciaContaDebitoBancaria;
	}

	public void setDescricaoAgenciaContaDebitoBancaria(
			String descricaoAgenciaContaDebitoBancaria) {
		this.descricaoAgenciaContaDebitoBancaria = descricaoAgenciaContaDebitoBancaria;
	}

	public String getDescricaoTipoContaDebito() {
		return descricaoTipoContaDebito;
	}

	public void setDescricaoTipoContaDebito(String descricaoTipoContaDebito) {
		this.descricaoTipoContaDebito = descricaoTipoContaDebito;
	}

	public String getDescricaoAgenciaDebito() {
		return descricaoAgenciaDebito;
	}

	public void setDescricaoAgenciaDebito(String descricaoAgenciaDebito) {
		this.descricaoAgenciaDebito = descricaoAgenciaDebito;
	}

	public String getDescricaoBancoDebito() {
		return descricaoBancoDebito;
	}

	public void setDescricaoBancoDebito(String descricaoBancoDebito) {
		this.descricaoBancoDebito = descricaoBancoDebito;
	}

	public String getDescricaoContaDebito() {
		return descricaoContaDebito;
	}

	public void setDescricaoContaDebito(String descricaoContaDebito) {
		this.descricaoContaDebito = descricaoContaDebito;
	}

	public String getDescricaoAgenciaCredito() {
		return descricaoAgenciaCredito;
	}

	public void setDescricaoAgenciaCredito(String descricaoAgenciaCredito) {
		this.descricaoAgenciaCredito = descricaoAgenciaCredito;
	}

	public String getDescricaoBancoCredito() {
		return descricaoBancoCredito;
	}

	public void setDescricaoBancoCredito(String descricaoBancoCredito) {
		this.descricaoBancoCredito = descricaoBancoCredito;
	}

	public String getDescricaoContaCredito() {
		return descricaoContaCredito;
	}

	public void setDescricaoContaCredito(String descricaoContaCredito) {
		this.descricaoContaCredito = descricaoContaCredito;
	}

	public String getDescricaoTipoContaCredito() {
		return descricaoTipoContaCredito;
	}

	public void setDescricaoTipoContaCredito(String descricaoTipoContaCredito) {
		this.descricaoTipoContaCredito = descricaoTipoContaCredito;
	}

	public Long getContaSalario() {
		return contaSalario;
	}

	public void setContaSalario(Long contaSalario) {
		this.contaSalario = contaSalario;
	}

	public Integer getBancoSalario() {
		return bancoSalario;
	}

	public void setBancoSalario(Integer bancoSalario) {
		this.bancoSalario = bancoSalario;
	}

	public Integer getAgenciaSalario() {
		return agenciaSalario;
	}

	public void setAgenciaSalario(Integer agenciaSalario) {
		this.agenciaSalario = agenciaSalario;
	}

	public String getDigitoSalario() {
		return digitoSalario;
	}

	public void setDigitoSalario(String digitoSalario) {
		this.digitoSalario = digitoSalario;
	}

	public Integer getDestinoPagamento() {
		return destinoPagamento;
	}

	public void setDestinoPagamento(Integer destinoPagamento) {
		this.destinoPagamento = destinoPagamento;
	}

	public Long getContaPgtoDestino() {
		return contaPgtoDestino;
	}

	public void setContaPgtoDestino(Long contaPgtoDestino) {
		this.contaPgtoDestino = contaPgtoDestino;
	}

	public String getIspbPgtoDestino() {
		return ispbPgtoDestino;
	}

	public void setIspbPgtoDestino(String ispbPgtoDestino) {
		this.ispbPgtoDestino = ispbPgtoDestino;
	}

	public Long getLoteInterno() {
		return loteInterno;
	}

	public void setLoteInterno(Long loteInterno) {
		this.loteInterno = loteInterno;
	}

	public Integer getNumeroSolicitacao() {
		return numeroSolicitacao;
	}

	public void setNumeroSolicitacao(Integer numeroSolicitacao) {
		this.numeroSolicitacao = numeroSolicitacao;
	}

	public String getDescDestinoPagamento() {
		return descDestinoPagamento;
	}

	public void setDescDestinoPagamento(String descDestinoPagamento) {
		this.descDestinoPagamento = descDestinoPagamento;
	}

	public Integer getCdDigitoBancariaContaCredito() {
		return cdDigitoBancariaContaCredito;
	}

	public void setCdDigitoBancariaContaCredito(Integer cdDigitoBancariaContaCredito) {
		this.cdDigitoBancariaContaCredito = cdDigitoBancariaContaCredito;
	}

	public Integer getBancoPgtoDestino() {
		return bancoPgtoDestino;
	}

	public void setBancoPgtoDestino(Integer bancoPgtoDestino) {
		this.bancoPgtoDestino = bancoPgtoDestino;
	}

}
