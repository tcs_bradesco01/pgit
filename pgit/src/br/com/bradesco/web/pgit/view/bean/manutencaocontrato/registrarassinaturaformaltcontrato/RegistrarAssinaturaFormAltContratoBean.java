/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.registrarassinaturaformaltcontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.registrarassinaturaformaltcontrato;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.IRegistrarAssinaturaFormAltContratoService;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.ConsultarAditivoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.ConsultarAditivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarFormalizacaoManContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturaformaltcontrato.bean.RegistrarFormalizacaoManContratoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: RegistrarAssinaturaFormAltContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RegistrarAssinaturaFormAltContratoBean {
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo registrarAssinaturaFormAltContratoServiceImpl. */
	private IRegistrarAssinaturaFormAltContratoService registrarAssinaturaFormAltContratoServiceImpl;
	
	//atributos dados
	/** Atributo cnpjCpfMaster. */
	private String cnpjCpfMaster;
	
	/** Atributo nomeRazaoSocialMaster. */
	private String nomeRazaoSocialMaster;
	
	/** Atributo grupoEconomico. */
	private String grupoEconomico;
	
	/** Atributo segmento. */
	private String segmento;
	
	/** Atributo subSegmento. */
	private String subSegmento;
	
	/** Atributo atividadeEconomico. */
	private String atividadeEconomico;
	
	/** Atributo cnpjCpf. */
	private String cnpjCpf;
	
	/** Atributo empresa. */
	private String empresa;
	
	/** Atributo cdEmpresa. */
	private Long cdEmpresa;
	
	/** Atributo situacao. */
	private String situacao;
	
	/** Atributo tipo. */
	private String tipo;
	
	/** Atributo cdTipo. */
	private Integer cdTipo;
	
	/** Atributo numero. */
	private String numero;
	
	/** Atributo motivo. */
	private String motivo;
	
	/** Atributo participacao. */
	private String participacao;
	
	/** Atributo possuiAditivos. */
	private String possuiAditivos;
	
	/** Atributo dataHoraCad. */
	private String dataHoraCad;
	
	/** Atributo inicioVigencia. */
	private String inicioVigencia;
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;
	
	/** Atributo motivoAtivacao. */
	private String motivoAtivacao;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;
	
	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;

	/** Atributo numeroAditivo. */
	private String numeroAditivo;
	
	/** Atributo dataHoraCadAditivo. */
	private String dataHoraCadAditivo;
	
	/** Atributo situacaoAditivo. */
	private String situacaoAditivo;
 
	/** Atributo numContratoFisico. */
	private String numContratoFisico;
	
	/** Atributo itemSelecionadoSolicitacao. */
	private Integer itemSelecionadoSolicitacao;
	
	/** Atributo dataAssinatura. */
	private Date dataAssinatura;
	
	/** Atributo dataAssinaturaDesc. */
	private String dataAssinaturaDesc;
	
	/** Atributo listaGrid. */
	private List<ConsultarAditivoContratoSaidaDTO> listaGrid;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;	
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();
	
	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo nrSequenciaContrato. */
	private Long nrSequenciaContrato;	
	
	/** Atributo hiddenConfirma. */
	private boolean hiddenConfirma;
	
	/** Atributo assinaturaAnexoConfirmada. */
	private boolean assinaturaAnexoConfirmada;

	//metodos
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
		
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		//carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		
		this.setItemSelecionadoSolicitacao(null);
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteRegAssinaturaFormAltContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conRegistrarAssinaturaFormalizacaoAltContrato");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		
		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(false);
		
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);

	}
	
	/**
	 * Limpar dados pesquisa contrato registrar assinatura.
	 */
	public void limparDadosPesquisaContratoRegistrarAssinatura() {
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(0l);
		identificacaoClienteContratoBean.setTipoContratoFiltro(0);
		identificacaoClienteContratoBean.setNumeroFiltro("");
		identificacaoClienteContratoBean.setAgenciaOperadoraFiltro("");
		identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		identificacaoClienteContratoBean.setCodigoFunc("");
		identificacaoClienteContratoBean.setSituacaoFiltro(0);		
		identificacaoClienteContratoBean.listarMotivoSituacao();
		identificacaoClienteContratoBean.setHabilitaCampos(false);
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setListaGridPesquisa(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		identificacaoClienteContratoBean.setEmpresaGestoraContrato(null);
		identificacaoClienteContratoBean.setNumeroContrato(null);
		identificacaoClienteContratoBean.setDescricaoContrato(null);
		identificacaoClienteContratoBean.setSituacaoContrato(null);
		identificacaoClienteContratoBean.limparRadioFiltro();
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(false);
	}
	
	


	/**
	 * Avancar registrar contrato confirmar.
	 *
	 * @return the string
	 */
	public String avancarRegistrarContratoConfirmar(){
		return "AVANCAR_CONFIRMAR";
	}
	
	/**
	 * Avancar ativar registrar alt contrato.
	 *
	 * @return the string
	 */
	public String avancarAtivarRegistrarAltContrato(){
		setDataAssinatura(new Date());
		setNumContratoFisico("");

		ConsultarAditivoContratoSaidaDTO dto = listaGrid.get(itemSelecionadoLista);
		setNumeroAditivo(String.valueOf(dto.getNrAditivoContratoNegocio()));
		setDataHoraCadAditivo(dto.getDataHoraInclusaoFormatada());
		setSituacaoAditivo(dto.getDsSituacaoAditivoContrato());		
		
		setDataAssinaturaDesc(FormatarData.formataDiaMesAno(getDataAssinatura()));
		
		return "AVANCAR_ALTERAR_CONFIRMAR";
	}
	
	/**
	 * Limpar registrar.
	 *
	 * @return the string
	 */
	public String limparRegistrar(){
		setDataAssinatura(new Date());
		setNumContratoFisico("");		
		return "";
	}

	/**
	 * Voltar ativar registrar alt contrato.
	 *
	 * @return the string
	 */
	public String voltarAtivarRegistrarAltContrato(){
		setItemSelecionadoLista(null);		
		return "VOLTAR_ALTERAR_CONFIRMAR";
	}

	/**
	 * Avancar ativar registrar formalizacao alt contrato.
	 *
	 * @return the string
	 */
	public String avancarAtivarRegistrarFormalizacaoAltContrato(){
		ConsultarAditivoContratoSaidaDTO dto = listaGrid.get(itemSelecionadoLista);
		setNumeroAditivo(String.valueOf(dto.getNrAditivoContratoNegocio()));
		setDataHoraCadAditivo(dto.getDataHoraInclusaoFormatada());
		setSituacaoAditivo(dto.getDsSituacaoAditivoContrato());		
		
		setDataAssinaturaDesc(FormatarData.formataDiaMesAno(getDataAssinatura()));

		return "AVANCAR";
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		return "VOLTAR_REGISTRAR";
	}
	
	/**
	 * Registrar.
	 *
	 * @return the string
	 */
	public String registrar(){
		return "REGISTRAR";
	}
	
	/**
	 * Voltar con registrar assinatura contrato.
	 *
	 * @return the string
	 */
	public String voltarConRegistrarAssinaturaContrato(){
		return "VOLTAR";
	}
	
	/**
	 * Voltar registrar assinatura contrato.
	 *
	 * @return the string
	 */
	public String voltarRegistrarAssinaturaContrato(){
		return "VOLTAR";
	}
	
	/**
	 * Formalizar.
	 *
	 * @return the string
	 */
	public String formalizar(){
		
		
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		setCnpjCpfMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomico(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomico(saidaDTO.getDsAtividadeEconomica());
		setSegmento(saidaDTO.getDsSegmentoCliente());
		setSubSegmento(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacao(saidaDTO.getDsSituacaoContrato());
		setMotivo(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDescricaoContrato(saidaDTO.getDsContrato());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
		//setDataHoraCadastramento(null);
		
		setCdPessoaJuridica(saidaDTO.getCdPessoaJuridica());
		setCdTipoContrato(saidaDTO.getCdTipoContrato());
		setNrSequenciaContrato(saidaDTO.getNrSequenciaContrato());
		
		if (getDataHoraCad()!=null ){
			
			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");		
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			
			String stringData = getDataHoraCad().substring(0, 19);
			
			try {
				setDataHoraCad(formato2.format(formato1.parse(stringData)));
			}  catch (ParseException e) {
				setDataHoraCad("");
			}
			
		}
		
		//setInicioVigencia(inicioVigencia)
		
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null && !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado().equals("")){
			setCnpjCpf(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
			
		}else{
			setCnpjCpf(getCnpjCpfMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}
		
		limparRegistrar();
		carregaListaConsultar();
		return "FORMALIZAR";
	}
	
	/**
	 * Voltar ativar assinatura formalizacao alt contrato.
	 *
	 * @return the string
	 */
	public String voltarAtivarAssinaturaFormalizacaoAltContrato(){
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Confirmar registrar.
	 *
	 * @return the string
	 */
	public String confirmarRegistrar(){
		setAssinaturaAnexoConfirmada(true);
		if (isHiddenConfirma()){
			try{
				RegistrarFormalizacaoManContratoEntradaDTO entrada = new RegistrarFormalizacaoManContratoEntradaDTO();
				ConsultarAditivoContratoSaidaDTO dto = listaGrid.get(getItemSelecionadoLista());
				
				entrada.setCdContratoNegocioPagamento(getNumContratoFisico());
				entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
				entrada.setCdTipoContratoNegocio(getCdTipoContrato());
				entrada.setDtAssinaturaAditivo(FormatarData.formataDiaMesAno(getDataAssinatura()));
				entrada.setNrAditivoContratoNegocio(dto.getNrAditivoContratoNegocio());
				entrada.setNrSequenciaContratoNegocio(getNrSequenciaContrato());
				
				RegistrarFormalizacaoManContratoSaidaDTO saida = getRegistrarAssinaturaFormAltContratoServiceImpl().registrarFormalizacaoManContrato(entrada);
				
				BradescoFacesUtils.addInfoModalMessage("(" +saida.getCodMensagem() + ") " + saida.getMensagem(), "conRegistrarAssinaturaFormalizacaoAltContrato2", BradescoViewExceptionActionType.ACTION, false);				
				
				carregaListaConsultar();
				setAssinaturaAnexoConfirmada(false);
		
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				return null;
			}		
		}
		
		return "";
	}
	
	/**
	 * Carrega lista consultar.
	 */
	public void carregaListaConsultar(){
		listaGrid = new ArrayList<ConsultarAditivoContratoSaidaDTO>();
		try{
			ConsultarAditivoContratoEntradaDTO entradaDTO = new ConsultarAditivoContratoEntradaDTO();
			
			entradaDTO.setCdAcesso(0);			
			entradaDTO.setCdSituacaoAditivoContrato(1);
			entradaDTO.setDtFimInclusaoContrato("");
			entradaDTO.setDtInicioInclusaoContrato("");
			entradaDTO.setNrAditivoContratoNegocio(0);		
			
			entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica() != null? getCdPessoaJuridica():0);
			entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato() !=  null?getCdTipoContrato():0);
			entradaDTO.setNrSequenciaContratoNegocio(getNrSequenciaContrato() != null?getNrSequenciaContrato():0);
			
			List<ConsultarAditivoContratoSaidaDTO> listaTemp = getRegistrarAssinaturaFormAltContratoServiceImpl().consultarAditivoContrato(entradaDTO);			
			setListaGrid(new ArrayList<ConsultarAditivoContratoSaidaDTO>());
			
			for (ConsultarAditivoContratoSaidaDTO consultarAditivoContratoSaidaDTO : listaTemp) {
				if(consultarAditivoContratoSaidaDTO.getCdSituacaoAditivoContrato() == 1 || consultarAditivoContratoSaidaDTO.getCdSituacaoAditivoContrato() == 3){
					getListaGrid().add(consultarAditivoContratoSaidaDTO);
				}
			}
			
			listaControle = new ArrayList<SelectItem>();
			for(int i=0;i<getListaGrid().size();i++){
				listaControle.add(new SelectItem(i,""));
			}
			setItemSelecionadoLista(null);
		}catch(PdcAdapterFunctionalException p){
			if (!assinaturaAnexoConfirmada) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			}
			setListaGrid(null);
			setItemSelecionadoLista(null);
		}
	}
	
	/*Sets e Gets*/	
	/**
	 * Get: itemSelecionadoSolicitacao.
	 *
	 * @return itemSelecionadoSolicitacao
	 */
	public Integer getItemSelecionadoSolicitacao() {
		return itemSelecionadoSolicitacao;
	}

	/**
	 * Set: itemSelecionadoSolicitacao.
	 *
	 * @param itemSelecionadoSolicitacao the item selecionado solicitacao
	 */
	public void setItemSelecionadoSolicitacao(Integer itemSelecionadoSolicitacao) {
		this.itemSelecionadoSolicitacao = itemSelecionadoSolicitacao;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}
	
	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}
	
	/**
	 * Get: atividadeEconomico.
	 *
	 * @return atividadeEconomico
	 */
	public String getAtividadeEconomico() {
		return atividadeEconomico;
	}
	
	/**
	 * Set: atividadeEconomico.
	 *
	 * @param atividadeEconomico the atividade economico
	 */
	public void setAtividadeEconomico(String atividadeEconomico) {
		this.atividadeEconomico = atividadeEconomico;
	}
	
	/**
	 * Get: cnpjCpf.
	 *
	 * @return cnpjCpf
	 */
	public String getCnpjCpf() {
		return cnpjCpf;
	}
	
	/**
	 * Set: cnpjCpf.
	 *
	 * @param cnpjCpf the cnpj cpf
	 */
	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}
	
	/**
	 * Get: cnpjCpfMaster.
	 *
	 * @return cnpjCpfMaster
	 */
	public String getCnpjCpfMaster() {
		return cnpjCpfMaster;
	}
	
	/**
	 * Set: cnpjCpfMaster.
	 *
	 * @param cnpjCpfMaster the cnpj cpf master
	 */
	public void setCnpjCpfMaster(String cnpjCpfMaster) {
		this.cnpjCpfMaster = cnpjCpfMaster;
	}
	
	/**
	 * Get: dataHoraCad.
	 *
	 * @return dataHoraCad
	 */
	public String getDataHoraCad() {
		return dataHoraCad;
	}
	
	/**
	 * Set: dataHoraCad.
	 *
	 * @param dataHoraCad the data hora cad
	 */
	public void setDataHoraCad(String dataHoraCad) {
		this.dataHoraCad = dataHoraCad;
	}
	
	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}
	
	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	
	/**
	 * Get: grupoEconomico.
	 *
	 * @return grupoEconomico
	 */
	public String getGrupoEconomico() {
		return grupoEconomico;
	}
	
	/**
	 * Set: grupoEconomico.
	 *
	 * @param grupoEconomico the grupo economico
	 */
	public void setGrupoEconomico(String grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}
	
	/**
	 * Get: inicioVigencia.
	 *
	 * @return inicioVigencia
	 */
	public String getInicioVigencia() {
		return inicioVigencia;
	}
	
	/**
	 * Set: inicioVigencia.
	 *
	 * @param inicioVigencia the inicio vigencia
	 */
	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}
	
	/**
	 * Get: motivo.
	 *
	 * @return motivo
	 */
	public String getMotivo() {
		return motivo;
	}
	
	/**
	 * Set: motivo.
	 *
	 * @param motivo the motivo
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	/**
	 * Get: nomeRazaoSocialMaster.
	 *
	 * @return nomeRazaoSocialMaster
	 */
	public String getNomeRazaoSocialMaster() {
		return nomeRazaoSocialMaster;
	}
	
	/**
	 * Set: nomeRazaoSocialMaster.
	 *
	 * @param nomeRazaoSocialMaster the nome razao social master
	 */
	public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
		this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
	}
	
	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}
	
	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	/**
	 * Get: participacao.
	 *
	 * @return participacao
	 */
	public String getParticipacao() {
		return participacao;
	}
	
	/**
	 * Set: participacao.
	 *
	 * @param participacao the participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}
	
	/**
	 * Get: possuiAditivos.
	 *
	 * @return possuiAditivos
	 */
	public String getPossuiAditivos() {
		return possuiAditivos;
	}
	
	/**
	 * Set: possuiAditivos.
	 *
	 * @param possuiAditivos the possui aditivos
	 */
	public void setPossuiAditivos(String possuiAditivos) {
		this.possuiAditivos = possuiAditivos;
	}
	
	/**
	 * Get: segmento.
	 *
	 * @return segmento
	 */
	public String getSegmento() {
		return segmento;
	}
	
	/**
	 * Set: segmento.
	 *
	 * @param segmento the segmento
	 */
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}
	
	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}
	
	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	/**
	 * Get: subSegmento.
	 *
	 * @return subSegmento
	 */
	public String getSubSegmento() {
		return subSegmento;
	}
	
	/**
	 * Set: subSegmento.
	 *
	 * @param subSegmento the sub segmento
	 */
	public void setSubSegmento(String subSegmento) {
		this.subSegmento = subSegmento;
	}
	
	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}
	
	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	/**
	 * Get: motivoAtivacao.
	 *
	 * @return motivoAtivacao
	 */
	public String getMotivoAtivacao() {
		return motivoAtivacao;
	}
	
	/**
	 * Set: motivoAtivacao.
	 *
	 * @param motivoAtivacao the motivo ativacao
	 */
	public void setMotivoAtivacao(String motivoAtivacao) {
		this.motivoAtivacao = motivoAtivacao;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Get: cdTipo.
	 *
	 * @return cdTipo
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}

	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ConsultarAditivoContratoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ConsultarAditivoContratoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}
	
	/**
	 * Get: dataAssinatura.
	 *
	 * @return dataAssinatura
	 */
	public Date getDataAssinatura() {
		return dataAssinatura;
	}

	/**
	 * Set: dataAssinatura.
	 *
	 * @param dataAssinatura the data assinatura
	 */
	public void setDataAssinatura(Date dataAssinatura) {
		this.dataAssinatura = dataAssinatura;
	}

	/**
	 * Get: dataAssinaturaDesc.
	 *
	 * @return dataAssinaturaDesc
	 */
	public String getDataAssinaturaDesc() {
		return dataAssinaturaDesc;
	}

	/**
	 * Set: dataAssinaturaDesc.
	 *
	 * @param dataAssinaturaDesc the data assinatura desc
	 */
	public void setDataAssinaturaDesc(String dataAssinaturaDesc) {
		this.dataAssinaturaDesc = dataAssinaturaDesc;
	}

	/**
	 * Get: registrarAssinaturaFormAltContratoServiceImpl.
	 *
	 * @return registrarAssinaturaFormAltContratoServiceImpl
	 */
	public IRegistrarAssinaturaFormAltContratoService getRegistrarAssinaturaFormAltContratoServiceImpl() {
		return registrarAssinaturaFormAltContratoServiceImpl;
	}

	/**
	 * Set: registrarAssinaturaFormAltContratoServiceImpl.
	 *
	 * @param registrarAssinaturaFormAltContratoServiceImpl the registrar assinatura form alt contrato service impl
	 */
	public void setRegistrarAssinaturaFormAltContratoServiceImpl(
			IRegistrarAssinaturaFormAltContratoService registrarAssinaturaFormAltContratoServiceImpl) {
		this.registrarAssinaturaFormAltContratoServiceImpl = registrarAssinaturaFormAltContratoServiceImpl;
	}


	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}


	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}


	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}


	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}


	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}


	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}


	/**
	 * Is hidden confirma.
	 *
	 * @return true, if is hidden confirma
	 */
	public boolean isHiddenConfirma() {
		return hiddenConfirma;
	}


	/**
	 * Set: hiddenConfirma.
	 *
	 * @param hiddenConfirma the hidden confirma
	 */
	public void setHiddenConfirma(boolean hiddenConfirma) {
		this.hiddenConfirma = hiddenConfirma;
	}


	/**
	 * Get: dataHoraCadAditivo.
	 *
	 * @return dataHoraCadAditivo
	 */
	public String getDataHoraCadAditivo() {
		return dataHoraCadAditivo;
	}


	/**
	 * Set: dataHoraCadAditivo.
	 *
	 * @param dataHoraCadAditivo the data hora cad aditivo
	 */
	public void setDataHoraCadAditivo(String dataHoraCadAditivo) {
		this.dataHoraCadAditivo = dataHoraCadAditivo;
	}


	/**
	 * Get: numeroAditivo.
	 *
	 * @return numeroAditivo
	 */
	public String getNumeroAditivo() {
		return numeroAditivo;
	}


	/**
	 * Set: numeroAditivo.
	 *
	 * @param numeroAditivo the numero aditivo
	 */
	public void setNumeroAditivo(String numeroAditivo) {
		this.numeroAditivo = numeroAditivo;
	}


	/**
	 * Get: situacaoAditivo.
	 *
	 * @return situacaoAditivo
	 */
	public String getSituacaoAditivo() {
		return situacaoAditivo;
	}


	/**
	 * Set: situacaoAditivo.
	 *
	 * @param situacaoAditivo the situacao aditivo
	 */
	public void setSituacaoAditivo(String situacaoAditivo) {
		this.situacaoAditivo = situacaoAditivo;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}
	
	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: numContratoFisico.
	 *
	 * @return numContratoFisico
	 */
	public String getNumContratoFisico() {
		return numContratoFisico;
	}


	/**
	 * Set: numContratoFisico.
	 *
	 * @param numContratoFisico the num contrato fisico
	 */
	public void setNumContratoFisico(String numContratoFisico) {
		this.numContratoFisico = numContratoFisico;
	}




	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}




	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * @param assinaturaAnexoConfirmada the assinaturaAnexoConfirmada to set
	 */
	public void setAssinaturaAnexoConfirmada(boolean assinaturaAnexoConfirmada) {
		this.assinaturaAnexoConfirmada = assinaturaAnexoConfirmada;
	}

	/**
	 * @return the assinaturaAnexoConfirmada
	 */
	public boolean isAssinaturaAnexoConfirmada() {
		return assinaturaAnexoConfirmada;
	}
	
	
	
}
