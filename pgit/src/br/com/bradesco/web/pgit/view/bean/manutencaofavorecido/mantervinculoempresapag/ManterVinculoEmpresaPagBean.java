/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaofavorecido.mantervinculoempresapag
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaofavorecido.mantervinculoempresapag;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.IManterVinculoEmpresaSalarioService;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetHistCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetHistCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ExcluirVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ExcluirVinculoCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.IncluirVinculoEmprPagContSalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.IncluirVinculoEmprPagContSalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoHistCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.ListarVinculoHistCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.SubstituirVinculoCtaSalarioEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculoempresasalario.bean.SubstituirVinculoCtaSalarioEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;

/**
 * @author lrribeiro
 *
 */
public class ManterVinculoEmpresaPagBean extends IdentificacaoClienteBean{
	
	//Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO. */
	private static final String CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO = "conManterVinculoEmpresaPagadoraContaSalario";
	
	/** Atributo manterVinculoEmpresaSalarioService. */
	private IManterVinculoEmpresaSalarioService manterVinculoEmpresaSalarioService;
	
	//Entrada
	/** Atributo entradaVincContaSalarioEmpresa. */
	private ListarVinculoCtaSalarioEmpresaEntradaDTO entradaVincContaSalarioEmpresa;
	
	/** Atributo entradaListaVincHistoricoCtaSalario. */
	private ListarVinculoHistCtaSalarioEmpresaEntradaDTO entradaListaVincHistoricoCtaSalario;
	
	/** Atributo entradaSubstituirVincCtaSalarioEmp. */
	private SubstituirVinculoCtaSalarioEmpresaEntradaDTO entradaSubstituirVincCtaSalarioEmp;
	
	/** Atributo entradaIncluirVinculoEmprPagContSal. */
	private IncluirVinculoEmprPagContSalEntradaDTO entradaIncluirVinculoEmprPagContSal;
	
	//Saida
	/** Atributo listaVincContaSalarioEmpresa. */
	private List<ListarVinculoCtaSalarioEmpresaSaidaDTO> listaVincContaSalarioEmpresa;
	
	/** Atributo saidaVincContaSalarioEmpresa. */
	private ListarVinculoCtaSalarioEmpresaSaidaDTO saidaVincContaSalarioEmpresa;
	
	/** Atributo saidaConsultarDetVinculoCtaSalarioEmpresa. */
	private ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO saidaConsultarDetVinculoCtaSalarioEmpresa;
	
	/** Atributo listaHistVincContaSalarioEmpresa. */
	private List<ListarVinculoHistCtaSalarioEmpresaSaidaDTO> listaHistVincContaSalarioEmpresa;
	
	/** Atributo saidaHistoricoVincContaSalarioEmpresa. */
	private ListarVinculoHistCtaSalarioEmpresaSaidaDTO saidaHistoricoVincContaSalarioEmpresa;
	
	/** Atributo saidaDetHistVinculoCtaSalarioEmpresa. */
	private ConsultarDetHistCtaSalarioEmpresaSaidaDTO saidaDetHistVinculoCtaSalarioEmpresa;
	
	/** Atributo chkDadosContaSalarioHistorico. */
	private boolean chkDadosContaSalarioHistorico;
        
        /** Atributo bancoSalarioHistorico. */
        private Integer bancoSalarioHistorico;
        
        /** Atributo agenciaSalarioHistorico. */
        private Integer agenciaSalarioHistorico;
        
        /** Atributo contaSalarioHistorico. */
        private Long contaSalarioHistorico;
        
        /** Atributo digitoContaSalarioHistorico. */
        private String digitoContaSalarioHistorico;
        
        /** Atributo disableArgumentosConsultaHistorico. */
        private boolean disableArgumentosConsultaHistorico;
        
        /** Atributo periodoInicialHistorico. */
        private Date periodoInicialHistorico;
        
        /** Atributo periodoFinalHistorico. */
        private Date periodoFinalHistorico;
	
	/** Atributo itemSelecionadoLista. */
	private String itemSelecionadoLista;
	
	/** Atributo itemSelecionadoListaHistorico. */
	private String itemSelecionadoListaHistorico;
	
	/** Atributo itemSelecionadoListaRadio. */
	private String itemSelecionadoListaRadio;

	/**
	 * Manter vinculo empresa pag bean.
	 */
	public ManterVinculoEmpresaPagBean() {
		super(CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO, "identificaoEmpPagadoraCliente", "identificaoEmpPagadoraContrato");
	}
	
	/**
	 * Limpar pagina.
	 *
	 * @return the string
	 */
	public String limparPagina() {
		listaVincContaSalarioEmpresa = new ArrayList<ListarVinculoCtaSalarioEmpresaSaidaDTO>();
		this.setItemSelecionadoLista(null);
		this.setHabilitaLimpa(false);
		return CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO;
	}
	
	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos() {
		inicializarCamposManterVinculoEmpresaPag();
		setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		listaVincContaSalarioEmpresa = new ArrayList<ListarVinculoCtaSalarioEmpresaSaidaDTO>();
		this.setLimpaCamp(true);
		this.setHabilitaLimpa(false);
		this.setBloqueaAgConta(true);
		return CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO;
	}
	
	/**
	 * Inicializar campos manter vinculo empresa pag.
	 *
	 * @return the string
	 */
	public String inicializarCamposManterVinculoEmpresaPag(){
		
		this.setEntradaVincContaSalarioEmpresa(new ListarVinculoCtaSalarioEmpresaEntradaDTO());
		this.setItemFiltroSelecionado(null);
		this.setItemContratoSelecionado(-1) ;
		this.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.setPreencherListaEmpresaGestora(new ArrayList<SelectItem>());
		this.setPreencherTipoContrato(new ArrayList<SelectItem>());
		this.setHabilitaFiltroDeContrato(true);
		this.setDesabilataFiltro(true);
		this.loadEmpresaGestora();
		this.loadTipoContrato();
		this.setItemSelecionadoLista(null);
		this.setItemSelecionadoListaRadio("0");
		this.setBloqueaRadio(false);
		this.setBloqueaTipoContrato(true);
		this.setBloqueaFiltro(true);
		this.entradaVincContaSalarioEmpresa.setCdBancoSalario(237);
		
		return CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO;
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		return "incManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/**
	 * Voltar substituir.
	 *
	 * @return the string
	 */
	public String voltarSubstituir() {
		return "substManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar() {
		setItemSelecionadoLista(null);
		return CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO;
	}
	
	/**
	 * Voltar historico.
	 *
	 * @return the string
	 */
	public String voltarHistorico() {
		setItemSelecionadoListaHistorico(null);
		return "histManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/**
	 * Voltar home.
	 *
	 * @return the string
	 */
	public String voltarHome() {
		this.setItemContratoSelecionado(-1);
		return CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO;
	}
	
	/*
	 * Consulta 
	 */
	/**
	 * Consultar solicitacao.
	 *
	 * @return the string
	 */
	public String consultarSolicitacao(){
		this.buscarSolicitacao();
		setBloqueaFiltro(false);
		this.setHabilitaLimpa(true);
		return CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO;
	}
	
	/**
	 * Submit.
	 *
	 * @param event the event
	 */
	public void submit(ActionEvent event){
		this.consultarSolicitacao();
	}
	
	/**
	 * Submit pesquisar historico.
	 *
	 * @param event the event
	 */
	public void submitPesquisarHistorico(ActionEvent event){
		this.pesquisarHistorico();
	}
	
	/**
	 * Buscar solicitacao.
	 *
	 * @return the string
	 */
	public String buscarSolicitacao(){
		entradaVincContaSalarioEmpresa.setCdPessoaJuridNegocio(getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		entradaVincContaSalarioEmpresa.setCdTipoContratoNegocio(getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		entradaVincContaSalarioEmpresa.setNrSeqContratoNegocio(getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		entradaVincContaSalarioEmpresa.setNrOcorrencias(50);
		this.setBloqueaTipoContrato(true);
		
		try {
		    this.listaVincContaSalarioEmpresa = this.manterVinculoEmpresaSalarioService.pesquisaManterVinculoEmpresaSalario(entradaVincContaSalarioEmpresa);
		} catch(PdcAdapterException p) {
			this.listaVincContaSalarioEmpresa = new ArrayList<ListarVinculoCtaSalarioEmpresaSaidaDTO>();
		    	BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO, BradescoViewExceptionActionType.ACTION, false);
		}

		return CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO;
	}
	
	/*
	 * Detalhar 
	 */
	/**
	 * Avancar detalhar.
	 *
	 * @return the string
	 */
	public String avancarDetalhar() {
		detalharVincContaEmpresaSalario();
		return "detManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/**
	 * Detalhar vinc conta empresa salario.
	 */
	public void detalharVincContaEmpresaSalario() {
		ConsultarDetVinculoCtaSalarioEmpresaEntradaDTO entradaDetalheVincContaSalarioEmp = new ConsultarDetVinculoCtaSalarioEmpresaEntradaDTO();
		saidaVincContaSalarioEmpresa = new ListarVinculoCtaSalarioEmpresaSaidaDTO();
		
		setSaidaVincContaSalarioEmpresa(this.listaVincContaSalarioEmpresa.get(Integer.parseInt(this.itemSelecionadoLista)));
		
		entradaDetalheVincContaSalarioEmp.setCdPessoaJuridNegocio(this.entradaVincContaSalarioEmpresa.getCdPessoaJuridNegocio());
		entradaDetalheVincContaSalarioEmp.setCdPessoaJuridVinc(this.saidaVincContaSalarioEmpresa.getCdPessoaJuridVinc());
		entradaDetalheVincContaSalarioEmp.setCdTipoContratoNegocio(this.entradaVincContaSalarioEmpresa.getCdTipoContratoNegocio());
		entradaDetalheVincContaSalarioEmp.setCdTipoContratoVinc(this.saidaVincContaSalarioEmpresa.getCdTipoContratoVinc());
		entradaDetalheVincContaSalarioEmp.setCdTipoVinculoContrato(this.saidaVincContaSalarioEmpresa.getCdTipoVinculoContrato());
		entradaDetalheVincContaSalarioEmp.setNrSeqContratoNegocio(this.entradaVincContaSalarioEmpresa.getNrSeqContratoNegocio());
		entradaDetalheVincContaSalarioEmp.setNrSeqContratoVinc(this.saidaVincContaSalarioEmpresa.getNrSeqContratoVinc());
		
		saidaConsultarDetVinculoCtaSalarioEmpresa = this.manterVinculoEmpresaSalarioService.detalheContaSalarioEmp(entradaDetalheVincContaSalarioEmp);
	}
	
	/*
	 * Incluir
	 */
	/**
	 * Incluir vinc conta empresa salario.
	 *
	 * @return the string
	 */
	public String incluirVincContaEmpresaSalario() {
		entradaIncluirVinculoEmprPagContSal = new IncluirVinculoEmprPagContSalEntradaDTO();
		entradaIncluirVinculoEmprPagContSal.setCdBancoSalario(237);
		
		return "incManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		
		ConsultarBancoAgenciaEntradaDTO entradaConsultarBancoAgencia = new ConsultarBancoAgenciaEntradaDTO();
		
		
		entradaConsultarBancoAgencia.setCdAgencia(entradaIncluirVinculoEmprPagContSal.getCdAgenciaSalario());
		entradaConsultarBancoAgencia.setCdBanco(entradaIncluirVinculoEmprPagContSal.getCdBancoSalario());
		entradaConsultarBancoAgencia.setCdDigitoAgencia(entradaIncluirVinculoEmprPagContSal.getCdDigitoAgenciaSalario());
		
		ConsultarBancoAgenciaSaidaDTO saidaConsultarBancoAgencia = manterVinculoEmpresaSalarioService.consultarDescricaoBancoAgencia(entradaConsultarBancoAgencia);
		
		entradaIncluirVinculoEmprPagContSal.setDsBancoSalario(saidaConsultarBancoAgencia.getDsBanco());
		entradaIncluirVinculoEmprPagContSal.setDsAgenciaSalario(saidaConsultarBancoAgencia.getDsAgencia());
		
		return "incConfManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/**
	 * Confirmar inclusao.
	 *
	 * @return the string
	 */
	public String confirmarInclusao() {
	    	entradaIncluirVinculoEmprPagContSal.setCdPessoaJuridicaNegocio(getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		entradaIncluirVinculoEmprPagContSal.setCdTipoContratoNegocio(getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		entradaIncluirVinculoEmprPagContSal.setNrSequenciaContratoNegocio(getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		
		try {
		    	IncluirVinculoEmprPagContSalSaidaDTO retornoIncluir = getManterVinculoEmpresaSalarioService().incluirVinculoEmprPagContSal(entradaIncluirVinculoEmprPagContSal);
			BradescoFacesUtils.addInfoModalMessage("(" + retornoIncluir.getCodMensagem() + ") " + retornoIncluir.getMensagem(), CON_MANTER_VINCULO_EMPRESA_PAGADORA_CONTA_SALARIO, BradescoViewExceptionActionType.ACTION, false);
			if(getListaVincContaSalarioEmpresa() != null && !getListaVincContaSalarioEmpresa().isEmpty()){
			    buscarSolicitacao();
			    this.setItemSelecionadoLista(null);
			    this.setHabilitaLimpa(false);
			}
		}catch(PdcAdapterException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "incManterVinculoEmpresaPagadoraContaSalario", BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		
		return "incConfManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/*
	 * Substituir
	 */
	/**
	 * Substituir vinc conta empresa salario.
	 *
	 * @return the string
	 */
	public String substituirVincContaEmpresaSalario() {
		entradaSubstituirVincCtaSalarioEmp = new SubstituirVinculoCtaSalarioEmpresaEntradaDTO();
		saidaVincContaSalarioEmpresa = listaVincContaSalarioEmpresa.get(Integer.parseInt(itemSelecionadoLista));
		entradaSubstituirVincCtaSalarioEmp.setCdBancoAtualizado(237);
		
		return "substManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/**
	 * Avancar substituir.
	 *
	 * @return the string
	 */
	public String avancarSubstituir() {
		
		ConsultarBancoAgenciaEntradaDTO entradaConsultarBancoAgencia = new ConsultarBancoAgenciaEntradaDTO();
		
		
		entradaConsultarBancoAgencia.setCdAgencia(entradaSubstituirVincCtaSalarioEmp.getCdAgenciaAtualizada());
		entradaConsultarBancoAgencia.setCdBanco(entradaSubstituirVincCtaSalarioEmp.getCdBancoAtualizado());
		entradaConsultarBancoAgencia.setCdDigitoAgencia(entradaSubstituirVincCtaSalarioEmp.getCdDigitoAgenciaAtualizada());
		
		ConsultarBancoAgenciaSaidaDTO saidaConsultarBancoAgencia = manterVinculoEmpresaSalarioService.consultarDescricaoBancoAgencia(entradaConsultarBancoAgencia);
		
		entradaSubstituirVincCtaSalarioEmp.setDsBancoAtualizado(saidaConsultarBancoAgencia.getDsBanco());
		entradaSubstituirVincCtaSalarioEmp.setDsAgenciaAtualizada(saidaConsultarBancoAgencia.getDsAgencia());
		
		return "substConfManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/**
	 * Confirmar substituicao.
	 *
	 * @return the string
	 */
	public String confirmarSubstituicao() {

		entradaSubstituirVincCtaSalarioEmp.setCdPessoa(entradaVincContaSalarioEmpresa.getCdPessoaJuridNegocio());
		entradaSubstituirVincCtaSalarioEmp.setCdPessoaJuridicaVinculo(saidaVincContaSalarioEmpresa.getCdPessoaJuridVinc());
		entradaSubstituirVincCtaSalarioEmp.setCdTipoContrato(entradaVincContaSalarioEmpresa.getCdTipoContratoNegocio());
		entradaSubstituirVincCtaSalarioEmp.setCdTipoContratoVinculo(saidaVincContaSalarioEmpresa.getCdTipoContratoVinc());
		entradaSubstituirVincCtaSalarioEmp.setCdTipoVinculoContrato(saidaVincContaSalarioEmpresa.getCdTipoVinculoContrato());
		entradaSubstituirVincCtaSalarioEmp.setNrSeqContrato(entradaVincContaSalarioEmpresa.getNrSeqContratoNegocio());
		entradaSubstituirVincCtaSalarioEmp.setNrSeqContratoVinculo(saidaVincContaSalarioEmpresa.getNrSeqContratoVinc());
		
		SubstituirVinculoCtaSalarioEmpresaSaidaDTO retornoSubstituir = new SubstituirVinculoCtaSalarioEmpresaSaidaDTO();
		try {
			retornoSubstituir = manterVinculoEmpresaSalarioService.substituirManterVincCtaSalarioEmpr(entradaSubstituirVincCtaSalarioEmp);
			BradescoFacesUtils.addInfoModalMessage("(" + retornoSubstituir.getCodMensagem() + ") " + retornoSubstituir.getMensagem(), "#{manterVinculoEmpresaPagBean.buscarSolicitacao}", BradescoViewExceptionActionType.ACTION, false);
		}catch(PdcAdapterException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "substManterVinculoEmpresaPagadoraContaSalario", BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		
		return "substConfManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/*
	 * Excluir
	 */
	/**
	 * Excluir vinc conta empresa salario.
	 *
	 * @return the string
	 */
	public String excluirVincContaEmpresaSalario() {
		detalharVincContaEmpresaSalario();
		return "excManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/**
	 * Confirmar exclusao.
	 *
	 * @return the string
	 */
	public String confirmarExclusao() {
		ExcluirVinculoCtaSalarioEmpresaEntradaDTO entradaExcluir = new ExcluirVinculoCtaSalarioEmpresaEntradaDTO();
		
		saidaVincContaSalarioEmpresa = listaVincContaSalarioEmpresa.get(Integer.parseInt(itemSelecionadoLista));
		
		entradaExcluir.setCdPessoa(entradaVincContaSalarioEmpresa.getCdPessoaJuridNegocio());
		entradaExcluir.setNrSeqContrato(entradaVincContaSalarioEmpresa.getNrSeqContratoNegocio());
		entradaExcluir.setNrSeqContratoVinc(saidaVincContaSalarioEmpresa.getNrSeqContratoVinc());
		entradaExcluir.setCdTipoContratoVinc(saidaVincContaSalarioEmpresa.getCdTipoContratoVinc());
		entradaExcluir.setCdPessoVinc(saidaVincContaSalarioEmpresa.getCdPessoaJuridVinc());
		entradaExcluir.setCdTipoPessoa(entradaVincContaSalarioEmpresa.getCdTipoContratoNegocio());
		entradaExcluir.setCdTipoVincContrato(saidaVincContaSalarioEmpresa.getCdTipoVinculoContrato());
		
		ExcluirVinculoCtaSalarioEmpresaSaidaDTO retornoExcluir = new ExcluirVinculoCtaSalarioEmpresaSaidaDTO();
		try {
			retornoExcluir = manterVinculoEmpresaSalarioService.excluirManterVincCtaSalarioEmp(entradaExcluir);
			BradescoFacesUtils.addInfoModalMessage("(" + retornoExcluir.getCodMensagem() + ") " + retornoExcluir.getMensagem(), "#{manterVinculoEmpresaPagBean.buscarSolicitacao}", BradescoViewExceptionActionType.ACTION, false);
		}catch(PdcAdapterException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "excManterVinculoEmpresaPagadoraContaSalario", BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return null;
	}
	
	/*
	 * Historico
	 */
	
        /**
	 * Limpar pagina historico.
	 */
	public void limparPaginaHistorico() {
            setDisableArgumentosConsultaHistorico(false);
            
            setChkDadosContaSalarioHistorico(false);
            
            setListaHistVincContaSalarioEmpresa(null);
            setItemSelecionadoListaHistorico(null);
            
            limparDadosContaSalario();
            setPeriodoInicialHistorico(new Date());
            setPeriodoFinalHistorico(new Date());
        }
        
        /**
         * Limpar dados conta salario.
         */
        public void limparDadosContaSalario(){
            setBancoSalarioHistorico(null);
            setAgenciaSalarioHistorico(null);
            setContaSalarioHistorico(null);
            setDigitoContaSalarioHistorico("");
        }

	/**
	 * Avancar historico.
	 *
	 * @return the string
	 */
	public String avancarHistorico(){
            	setSaidaVincContaSalarioEmpresa(new ListarVinculoCtaSalarioEmpresaSaidaDTO());
            	limparPaginaHistorico();
            
            	// SE O USU�RIO TIVER SELECIONADO UM ITEM ENT�O CONSULTA � REALIZADA COM FILTROS SELECIONADOS
            	if (getItemSelecionadoLista() != null) {
            	    setSaidaVincContaSalarioEmpresa(getListaVincContaSalarioEmpresa().get(Integer.parseInt(getItemSelecionadoLista())));
            
            	setChkDadosContaSalarioHistorico(true);
            	    setBancoSalarioHistorico(getSaidaVincContaSalarioEmpresa().getCdBancoSalario());
            	    setAgenciaSalarioHistorico(getSaidaVincContaSalarioEmpresa().getCdAgenciaSalario());
            	    setContaSalarioHistorico(getSaidaVincContaSalarioEmpresa().getCdContaSalario());
            	    setDigitoContaSalarioHistorico(getSaidaVincContaSalarioEmpresa().getCdDigitoContaSalario());
            	    setPeriodoInicialHistorico(new Date());
            	    setPeriodoFinalHistorico(new Date());
            	    // pesquisarHistorico();
            	}
		
		return "histManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/**
	 * Pesquisar historico.
	 */
	private void pesquisarHistorico(){	
	    	entradaListaVincHistoricoCtaSalario = new ListarVinculoHistCtaSalarioEmpresaEntradaDTO();
	    	
		entradaListaVincHistoricoCtaSalario.setCdPessoaJuridContrato(getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		entradaListaVincHistoricoCtaSalario.setCdTipoContratoNegocio(getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		entradaListaVincHistoricoCtaSalario.setNrSeqContratoNegocio(getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());	
		
		//Campos inseridos ao regerarmos o adaptor do pdc dia 31/08
		entradaListaVincHistoricoCtaSalario.setCdBanco(getBancoSalarioHistorico());
		entradaListaVincHistoricoCtaSalario.setCdAgencia(getAgenciaSalarioHistorico());
		entradaListaVincHistoricoCtaSalario.setCdDigitoAgencia(0);		
		entradaListaVincHistoricoCtaSalario.setCdConta(getContaSalarioHistorico());
		entradaListaVincHistoricoCtaSalario.setCdDigitoConta(PgitUtil.complementaDigito(getDigitoContaSalarioHistorico(), 2));
		entradaListaVincHistoricoCtaSalario.setDataDe(getPeriodoInicialHistorico());
		entradaListaVincHistoricoCtaSalario.setDataAte(getPeriodoFinalHistorico());
		
		if(isChkDadosContaSalarioHistorico()){
		    entradaListaVincHistoricoCtaSalario.setCdPesquisaLista(2);		    
		}
		else{
		    entradaListaVincHistoricoCtaSalario.setCdPesquisaLista(1);		    
		}
		
		entradaListaVincHistoricoCtaSalario.setCdTipoVinculoContrato(0);
		entradaListaVincHistoricoCtaSalario.setCdPessoaJuridVinc(0l);
		entradaListaVincHistoricoCtaSalario.setNrSeqContratoVinc(0l);
		entradaListaVincHistoricoCtaSalario.setCdTipoContratoVinc(0);			
		
		setListaHistVincContaSalarioEmpresa(manterVinculoEmpresaSalarioService.pesquisarHistoricoVincCtaSalarioEmp(entradaListaVincHistoricoCtaSalario));
		setDisableArgumentosConsultaHistorico(true);
	}
	
	/**
	 * Consultar historico.
	 *
	 * @return the string
	 */
	public String consultarHistorico(){
		try {
		    pesquisarHistorico();
		} catch (PdcAdapterFunctionalException p) {
		    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
		
		return "";		
	}
	
	/*
	 * Detalhe Historico
	 */
	/**
	 * Detalhar historico.
	 *
	 * @return the string
	 */
	public String detalharHistorico(){
		ConsultarDetHistCtaSalarioEmpresaEntradaDTO entradaDetalheHistorico = new ConsultarDetHistCtaSalarioEmpresaEntradaDTO();
		
		saidaHistoricoVincContaSalarioEmpresa = listaHistVincContaSalarioEmpresa.get(Integer.parseInt(itemSelecionadoListaHistorico));
		
		entradaDetalheHistorico.setCdPessoa(entradaListaVincHistoricoCtaSalario.getCdPessoaJuridContrato());
		entradaDetalheHistorico.setCdPessoaVinc(saidaHistoricoVincContaSalarioEmpresa.getCdPessoaJuridVinc());
		entradaDetalheHistorico.setCdTipoContrato(entradaListaVincHistoricoCtaSalario.getCdTipoContratoNegocio());
		entradaDetalheHistorico.setCdTipoContratoVinc(saidaHistoricoVincContaSalarioEmpresa.getCdTipoContratoVinc());
		entradaDetalheHistorico.setCdTipoVincContrato(0);
		entradaDetalheHistorico.setHrInclusaoRegistro(saidaHistoricoVincContaSalarioEmpresa.getHrInclusaoRegistro());
		entradaDetalheHistorico.setNrSeqContrato(entradaListaVincHistoricoCtaSalario.getNrSeqContratoNegocio());
		entradaDetalheHistorico.setNrSeqContratoVinc(saidaHistoricoVincContaSalarioEmpresa.getNrSeqContratoVinc());
		
		saidaDetHistVinculoCtaSalarioEmpresa = manterVinculoEmpresaSalarioService.detalharHistoricoVincCtaSalarioEmp(entradaDetalheHistorico);
		verificaUsuario();
		return "histDetManterVinculoEmpresaPagadoraContaSalario";
	}
	
	/**
	 * Verifica usuario.
	 *
	 * @return the string
	 */
	public String verificaUsuario(){
		this.saidaDetHistVinculoCtaSalarioEmpresa.setCdUsuarioDeInclusao(this.saidaDetHistVinculoCtaSalarioEmpresa.getCdUsuarioInclusao());
		if (this.saidaDetHistVinculoCtaSalarioEmpresa.getCdUsuarioDeInclusao() == null) {
			this.saidaDetHistVinculoCtaSalarioEmpresa.setCdUsuarioDeInclusao(this.saidaDetHistVinculoCtaSalarioEmpresa.getCdUsuarioInclusaoExterno());
		}
		this.saidaDetHistVinculoCtaSalarioEmpresa.setCdUsuarioDeManutencao(this.saidaDetHistVinculoCtaSalarioEmpresa.getCdUsuarioManutencao());
		if (this.saidaDetHistVinculoCtaSalarioEmpresa.getCdUsuarioDeManutencao() == null) {
			this.saidaDetHistVinculoCtaSalarioEmpresa.setCdUsuarioDeManutencao(this.saidaDetHistVinculoCtaSalarioEmpresa.getCdUsuarioManutencaoExterno());
		}
		return null;
	}
			
	/**
	 * Is list manter det vinculo cta salario empty.
	 *
	 * @return true, if is list manter det vinculo cta salario empty
	 */
	public boolean isListManterDetVinculoCtaSalarioEmpty(){
		return this.listaVincContaSalarioEmpresa == null || this.listaVincContaSalarioEmpresa.isEmpty();
	}
	
	/**
	 * Get: selectItemConSolicRastFav.
	 *
	 * @return selectItemConSolicRastFav
	 */
	public List<SelectItem> getSelectItemConSolicRastFav(){
		 
		 if(this.listaVincContaSalarioEmpresa == null || this.listaVincContaSalarioEmpresa.isEmpty()){
			 return new ArrayList<SelectItem>();
		 }else{
			 List<SelectItem> list = new ArrayList<SelectItem>();
			 
			 for (int index = 0; index < this.listaVincContaSalarioEmpresa.size(); index++) {
				 list.add(new SelectItem(String.valueOf(index), ""));
			 }
			 
			 return list;
		 }
	 }
	
	/**
	 * Is lista hist vinc conta salario empresa empty.
	 *
	 * @return true, if is lista hist vinc conta salario empresa empty
	 */
	public boolean isListaHistVincContaSalarioEmpresaEmpty(){
		return this.listaHistVincContaSalarioEmpresa == null || this.listaHistVincContaSalarioEmpresa.isEmpty();
	}
	
	/**
	 * Get: selectItemListaHitorico.
	 *
	 * @return selectItemListaHitorico
	 */
	public List<SelectItem> getSelectItemListaHitorico(){
		 
		 if(this.listaHistVincContaSalarioEmpresa == null || this.listaHistVincContaSalarioEmpresa.isEmpty()){
			 return new ArrayList<SelectItem>();
		 }else{
			 List<SelectItem> list = new ArrayList<SelectItem>();
			 
			 for (int index = 0; index < this.listaHistVincContaSalarioEmpresa.size(); index++) {
				 list.add(new SelectItem(String.valueOf(index), ""));
			 }
			 
			 return list;
		 }
	 }

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public String getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(
			String itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: manterVinculoEmpresaSalarioService.
	 *
	 * @return manterVinculoEmpresaSalarioService
	 */
	public IManterVinculoEmpresaSalarioService getManterVinculoEmpresaSalarioService() {
		return manterVinculoEmpresaSalarioService;
	}

	/**
	 * Set: manterVinculoEmpresaSalarioService.
	 *
	 * @param manterVinculoEmpresaSalarioService the manter vinculo empresa salario service
	 */
	public void setManterVinculoEmpresaSalarioService(
			IManterVinculoEmpresaSalarioService manterVinculoEmpresaSalarioService) {
		this.manterVinculoEmpresaSalarioService = manterVinculoEmpresaSalarioService;
	}

	/**
	 * Get: entradaVincContaSalarioEmpresa.
	 *
	 * @return entradaVincContaSalarioEmpresa
	 */
	public ListarVinculoCtaSalarioEmpresaEntradaDTO getEntradaVincContaSalarioEmpresa() {
		return entradaVincContaSalarioEmpresa;
	}

	/**
	 * Set: entradaVincContaSalarioEmpresa.
	 *
	 * @param entradaVincContaSalarioEmpresa the entrada vinc conta salario empresa
	 */
	public void setEntradaVincContaSalarioEmpresa(
			ListarVinculoCtaSalarioEmpresaEntradaDTO entradaVincContaSalarioEmpresa) {
		this.entradaVincContaSalarioEmpresa = entradaVincContaSalarioEmpresa;
	}

	/**
	 * Get: listaVincContaSalarioEmpresa.
	 *
	 * @return listaVincContaSalarioEmpresa
	 */
	public List<ListarVinculoCtaSalarioEmpresaSaidaDTO> getListaVincContaSalarioEmpresa() {
		return listaVincContaSalarioEmpresa;
	}

	/**
	 * Set: listaVincContaSalarioEmpresa.
	 *
	 * @param listaVincContaSalarioEmpresa the lista vinc conta salario empresa
	 */
	public void setListaVincContaSalarioEmpresa(
			List<ListarVinculoCtaSalarioEmpresaSaidaDTO> listaVincContaSalarioEmpresa) {
		this.listaVincContaSalarioEmpresa = listaVincContaSalarioEmpresa;
	}

	/**
	 * Get: saidaConsultarDetVinculoCtaSalarioEmpresa.
	 *
	 * @return saidaConsultarDetVinculoCtaSalarioEmpresa
	 */
	public ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO getSaidaConsultarDetVinculoCtaSalarioEmpresa() {
		return saidaConsultarDetVinculoCtaSalarioEmpresa;
	}

	/**
	 * Set: saidaConsultarDetVinculoCtaSalarioEmpresa.
	 *
	 * @param saidaConsultarDetVinculoCtaSalarioEmpresa the saida consultar det vinculo cta salario empresa
	 */
	public void setSaidaConsultarDetVinculoCtaSalarioEmpresa(
			ConsultarDetVinculoCtaSalarioEmpresaSaidaDTO saidaConsultarDetVinculoCtaSalarioEmpresa) {
		this.saidaConsultarDetVinculoCtaSalarioEmpresa = saidaConsultarDetVinculoCtaSalarioEmpresa;
	}

	/**
	 * Get: saidaVincContaSalarioEmpresa.
	 *
	 * @return saidaVincContaSalarioEmpresa
	 */
	public ListarVinculoCtaSalarioEmpresaSaidaDTO getSaidaVincContaSalarioEmpresa() {
		return saidaVincContaSalarioEmpresa;
	}

	/**
	 * Set: saidaVincContaSalarioEmpresa.
	 *
	 * @param saidaVincContaSalarioEmpresa the saida vinc conta salario empresa
	 */
	public void setSaidaVincContaSalarioEmpresa(
			ListarVinculoCtaSalarioEmpresaSaidaDTO saidaVincContaSalarioEmpresa) {
		this.saidaVincContaSalarioEmpresa = saidaVincContaSalarioEmpresa;
	}

	/**
	 * Get: listaHistVincContaSalarioEmpresa.
	 *
	 * @return listaHistVincContaSalarioEmpresa
	 */
	public List<ListarVinculoHistCtaSalarioEmpresaSaidaDTO> getListaHistVincContaSalarioEmpresa() {
		return listaHistVincContaSalarioEmpresa;
	}

	/**
	 * Set: listaHistVincContaSalarioEmpresa.
	 *
	 * @param listaHistVincContaSalarioEmpresa the lista hist vinc conta salario empresa
	 */
	public void setListaHistVincContaSalarioEmpresa(
			List<ListarVinculoHistCtaSalarioEmpresaSaidaDTO> listaHistVincContaSalarioEmpresa) {
		this.listaHistVincContaSalarioEmpresa = listaHistVincContaSalarioEmpresa;
	}

	/**
	 * Get: saidaDetHistVinculoCtaSalarioEmpresa.
	 *
	 * @return saidaDetHistVinculoCtaSalarioEmpresa
	 */
	public ConsultarDetHistCtaSalarioEmpresaSaidaDTO getSaidaDetHistVinculoCtaSalarioEmpresa() {
		return saidaDetHistVinculoCtaSalarioEmpresa;
	}

	/**
	 * Set: saidaDetHistVinculoCtaSalarioEmpresa.
	 *
	 * @param saidaDetHistVinculoCtaSalarioEmpresa the saida det hist vinculo cta salario empresa
	 */
	public void setSaidaDetHistVinculoCtaSalarioEmpresa(
			ConsultarDetHistCtaSalarioEmpresaSaidaDTO saidaDetHistVinculoCtaSalarioEmpresa) {
		this.saidaDetHistVinculoCtaSalarioEmpresa = saidaDetHistVinculoCtaSalarioEmpresa;
	}

	/**
	 * Get: entradaListaVincHistoricoCtaSalario.
	 *
	 * @return entradaListaVincHistoricoCtaSalario
	 */
	public ListarVinculoHistCtaSalarioEmpresaEntradaDTO getEntradaListaVincHistoricoCtaSalario() {
		return entradaListaVincHistoricoCtaSalario;
	}

	/**
	 * Set: entradaListaVincHistoricoCtaSalario.
	 *
	 * @param entradaListaVincHistoricoCtaSalario the entrada lista vinc historico cta salario
	 */
	public void setEntradaListaVincHistoricoCtaSalario(
			ListarVinculoHistCtaSalarioEmpresaEntradaDTO entradaListaVincHistoricoCtaSalario) {
		this.entradaListaVincHistoricoCtaSalario = entradaListaVincHistoricoCtaSalario;
	}

	/**
	 * Get: itemSelecionadoListaHistorico.
	 *
	 * @return itemSelecionadoListaHistorico
	 */
	public String getItemSelecionadoListaHistorico() {
		return itemSelecionadoListaHistorico;
	}

	/**
	 * Set: itemSelecionadoListaHistorico.
	 *
	 * @param itemSelecionadoListaHistorico the item selecionado lista historico
	 */
	public void setItemSelecionadoListaHistorico(
			String itemSelecionadoListaHistorico) {
		this.itemSelecionadoListaHistorico = itemSelecionadoListaHistorico;
	}

	/**
	 * Get: itemSelecionadoListaRadio.
	 *
	 * @return itemSelecionadoListaRadio
	 */
	public String getItemSelecionadoListaRadio() {
		return itemSelecionadoListaRadio;
	}

	/**
	 * Set: itemSelecionadoListaRadio.
	 *
	 * @param itemSelecionadoListaListaRadio the item selecionado lista radio
	 */
	public void setItemSelecionadoListaRadio(
			String itemSelecionadoListaListaRadio) {
		this.itemSelecionadoListaRadio = itemSelecionadoListaListaRadio;
	}

	/**
	 * Get: saidaHistoricoVincContaSalarioEmpresa.
	 *
	 * @return saidaHistoricoVincContaSalarioEmpresa
	 */
	public ListarVinculoHistCtaSalarioEmpresaSaidaDTO getSaidaHistoricoVincContaSalarioEmpresa() {
		return saidaHistoricoVincContaSalarioEmpresa;
	}

	/**
	 * Set: saidaHistoricoVincContaSalarioEmpresa.
	 *
	 * @param saidaHistoricoVincContaSalarioEmpresa the saida historico vinc conta salario empresa
	 */
	public void setSaidaHistoricoVincContaSalarioEmpresa(
			ListarVinculoHistCtaSalarioEmpresaSaidaDTO saidaHistoricoVincContaSalarioEmpresa) {
		this.saidaHistoricoVincContaSalarioEmpresa = saidaHistoricoVincContaSalarioEmpresa;
	}
	
	/**
	 * Get: entradaSubstituirVincCtaSalarioEmp.
	 *
	 * @return entradaSubstituirVincCtaSalarioEmp
	 */
	public SubstituirVinculoCtaSalarioEmpresaEntradaDTO getEntradaSubstituirVincCtaSalarioEmp() {
		return entradaSubstituirVincCtaSalarioEmp;
	}

	/**
	 * Set: entradaSubstituirVincCtaSalarioEmp.
	 *
	 * @param entradaSubstituirVincCtaSalarioEmp the entrada substituir vinc cta salario emp
	 */
	public void setEntradaSubstituirVincCtaSalarioEmp(
			SubstituirVinculoCtaSalarioEmpresaEntradaDTO entradaSubstituirVincCtaSalarioEmp) {
		this.entradaSubstituirVincCtaSalarioEmp = entradaSubstituirVincCtaSalarioEmp;
	}

	/**
	 * Get: agenciaSalarioHistorico.
	 *
	 * @return agenciaSalarioHistorico
	 */
	public Integer getAgenciaSalarioHistorico() {
	    return agenciaSalarioHistorico;
	}

	/**
	 * Set: agenciaSalarioHistorico.
	 *
	 * @param agenciaSalarioHistorico the agencia salario historico
	 */
	public void setAgenciaSalarioHistorico(Integer agenciaSalarioHistorico) {
	    this.agenciaSalarioHistorico = agenciaSalarioHistorico;
	}

	/**
	 * Get: bancoSalarioHistorico.
	 *
	 * @return bancoSalarioHistorico
	 */
	public Integer getBancoSalarioHistorico() {
	    return bancoSalarioHistorico;
	}

	/**
	 * Set: bancoSalarioHistorico.
	 *
	 * @param bancoSalarioHistorico the banco salario historico
	 */
	public void setBancoSalarioHistorico(Integer bancoSalarioHistorico) {
	    this.bancoSalarioHistorico = bancoSalarioHistorico;
	}

	/**
	 * Get: contaSalarioHistorico.
	 *
	 * @return contaSalarioHistorico
	 */
	public Long getContaSalarioHistorico() {
	    return contaSalarioHistorico;
	}

	/**
	 * Set: contaSalarioHistorico.
	 *
	 * @param contaSalarioHistorico the conta salario historico
	 */
	public void setContaSalarioHistorico(Long contaSalarioHistorico) {
	    this.contaSalarioHistorico = contaSalarioHistorico;
	}

	/**
	 * Get: digitoContaSalarioHistorico.
	 *
	 * @return digitoContaSalarioHistorico
	 */
	public String getDigitoContaSalarioHistorico() {
	    return digitoContaSalarioHistorico;
	}

	/**
	 * Set: digitoContaSalarioHistorico.
	 *
	 * @param digitoContaSalarioHistorico the digito conta salario historico
	 */
	public void setDigitoContaSalarioHistorico(String digitoContaSalarioHistorico) {
	    this.digitoContaSalarioHistorico = digitoContaSalarioHistorico;
	}

	/**
	 * Is disable argumentos consulta historico.
	 *
	 * @return true, if is disable argumentos consulta historico
	 */
	public boolean isDisableArgumentosConsultaHistorico() {
	    return disableArgumentosConsultaHistorico;
	}

	/**
	 * Set: disableArgumentosConsultaHistorico.
	 *
	 * @param disableArgumentosConsultaHistorico the disable argumentos consulta historico
	 */
	public void setDisableArgumentosConsultaHistorico(boolean disableArgumentosConsultaHistorico) {
	    this.disableArgumentosConsultaHistorico = disableArgumentosConsultaHistorico;
	}

	/**
	 * Get: periodoFinalHistorico.
	 *
	 * @return periodoFinalHistorico
	 */
	public Date getPeriodoFinalHistorico() {
	    return periodoFinalHistorico;
	}

	/**
	 * Set: periodoFinalHistorico.
	 *
	 * @param periodoFinalHistorico the periodo final historico
	 */
	public void setPeriodoFinalHistorico(Date periodoFinalHistorico) {
	    this.periodoFinalHistorico = periodoFinalHistorico;
	}

	/**
	 * Get: periodoInicialHistorico.
	 *
	 * @return periodoInicialHistorico
	 */
	public Date getPeriodoInicialHistorico() {
	    return periodoInicialHistorico;
	}

	/**
	 * Set: periodoInicialHistorico.
	 *
	 * @param periodoInicialHistorico the periodo inicial historico
	 */
	public void setPeriodoInicialHistorico(Date periodoInicialHistorico) {
	    this.periodoInicialHistorico = periodoInicialHistorico;
	}

	/**
	 * Is chk dados conta salario historico.
	 *
	 * @return true, if is chk dados conta salario historico
	 */
	public boolean isChkDadosContaSalarioHistorico() {
	    return chkDadosContaSalarioHistorico;
	}

	/**
	 * Set: chkDadosContaSalarioHistorico.
	 *
	 * @param chkDadosContaSalarioHistorico the chk dados conta salario historico
	 */
	public void setChkDadosContaSalarioHistorico(boolean chkDadosContaSalarioHistorico) {
	    this.chkDadosContaSalarioHistorico = chkDadosContaSalarioHistorico;
	}

	/**
	 * Get: entradaIncluirVinculoEmprPagContSal.
	 *
	 * @return entradaIncluirVinculoEmprPagContSal
	 */
	public IncluirVinculoEmprPagContSalEntradaDTO getEntradaIncluirVinculoEmprPagContSal() {
	    return entradaIncluirVinculoEmprPagContSal;
	}

	/**
	 * Set: entradaIncluirVinculoEmprPagContSal.
	 *
	 * @param entradaIncluirVinculoEmprPagContSal the entrada incluir vinculo empr pag cont sal
	 */
	public void setEntradaIncluirVinculoEmprPagContSal(IncluirVinculoEmprPagContSalEntradaDTO entradaIncluirVinculoEmprPagContSal) {
	    this.entradaIncluirVinculoEmprPagContSal = entradaIncluirVinculoEmprPagContSal;
	}
}