/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarRepresentanteContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarRepresentanteContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesContratoSaidaDTO;


/**
 * Nome: RepresentanteBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RepresentanteBean {
	
	/** Atributo TELA_CONSULTAR_CONTRATO. */
	private static final String TELA_CONSULTAR_CONTRATO = "TELA_CONSULTAR_CONTRATO";
	
	/** Atributo TELA_ALTERAR_CONTRATO. */
	private static final String TELA_ALTERAR_CONTRATO = "TELA_ALTERAR_CONTRATO";
	
	/** Atributo TELA_CONFIRMAR_ALTERACAO. */
	private static final String TELA_CONFIRMAR_ALTERACAO = "TELA_CONFIRMAR_ALTERACAO";
	
	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;
	
	//Cabe�alho
	/** Atributo listaGrid. */
	private List<ListarParticipantesContratoSaidaDTO> listaGrid;
	
	/** Atributo listaGridControle. */
	private List<SelectItem> listaGridControle;
	
	/** Atributo itemSelecionadoGrid. */
	private Integer itemSelecionadoGrid;
	
	/** Atributo cpfCnpjRepresentante. */
	private String cpfCnpjRepresentante;
	
	/** Atributo nomeRazaoRepresentante. */
	private String nomeRazaoRepresentante;
	
	/** Atributo grupEconRepresentante. */
	private String grupEconRepresentante;
	
	/** Atributo ativEconRepresentante. */
	private String ativEconRepresentante;
	
	/** Atributo segRepresentante. */
	private String segRepresentante;
	
	/** Atributo subSegRepresentante. */
	private String subSegRepresentante; 
	
	/** Atributo empresaContrato. */
	private String empresaContrato;
	
	/** Atributo tipoContrato. */
	private String tipoContrato;
	
	/** Atributo numeroContrato. */
	private String numeroContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo situacaoContrato. */
	private String situacaoContrato;
	
	/** Atributo motivoContrato. */
	private String motivoContrato;	
	
	/** Atributo participacaoContrato. */
	private String participacaoContrato;
	
	/** Atributo cdClub. */
	private Long cdClub;
	
	/** Atributo cdEmpresaContrato. */
	private Long cdEmpresaContrato;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo nrSequenciaContrato. */
	private String nrSequenciaContrato;
	
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;
	
	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;
	
	//Fim Cabe�alho
	
	//Alterar
	/** Atributo cpfCnpjRepresentanteAlterar. */
	private String cpfCnpjRepresentanteAlterar;
	
	/** Atributo nomeRazaoRepresentanteAlterar. */
	private String nomeRazaoRepresentanteAlterar;
	
	/** Atributo nivelRepresentanteAlterar. */
	private String nivelRepresentanteAlterar;
	
	/** Atributo situacaoRepresentanteAlterar. */
	private String situacaoRepresentanteAlterar;
	//Fim Alterar
	
	//Metodos
	/**
	 * Iniciar tela.
	 */
	public void iniciarTela(){
		setListaGrid(null);
		setListaGridControle(null);
		setItemSelecionadoGrid(null);
		setCpfCnpjRepresentante("");
		setNomeRazaoRepresentante("");
		setAtivEconRepresentante("");
		setSegRepresentante("");
		setSubSegRepresentante("");
		setEmpresaContrato("");
		setTipoContrato("");
		setNumeroContrato("");
		setSituacaoContrato("");
		setMotivoContrato("");
		setParticipacaoContrato("");
		
		setCdEmpresaContrato(0l);
		setCdTipoContrato(null);
		setNrSequenciaContrato("");
		setCdClub(0l);

		setCpfCnpjParticipante("");
		setNomeRazaoParticipante("");
	}
	
	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar(){
		AlterarRepresentanteContratoPgitEntradaDTO entradaDTO = new AlterarRepresentanteContratoPgitEntradaDTO();
		ListarParticipantesContratoSaidaDTO registroSelecionado = getListaGrid().get(getItemSelecionadoGrid());

		entradaDTO.setCdpessoaJuridicaContrato(registroSelecionado.getCdpessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrSequenciaContratoNegocio());
		entradaDTO.setCdPessoa(registroSelecionado.getCdClub());
		
		AlterarRepresentanteContratoPgitSaidaDTO saidaDTO = getManterContratoImpl().alterarRepresentanteContratoPgit(entradaDTO);
		
		BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conManterContrato", BradescoViewExceptionActionType.ACTION,false);
		
		return "";
	}
	
	/**
	 * Carrega lista alterar.
	 */
	public void carregaListaAlterar(){
		setListaGrid(new ArrayList<ListarParticipantesContratoSaidaDTO>());
		try{
			ListarParticipantesContratoEntradaDTO entradaDTO = new ListarParticipantesContratoEntradaDTO();		

			entradaDTO.setCdClub(getCdEmpresaContrato());
			entradaDTO.setCdpessoaJuridicaContrato(getCdEmpresaContrato());
			entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContrato()));
			
			setListaGrid(getManterContratoImpl().listarParticipantesContrato(entradaDTO));
			
			setListaGridControle(new ArrayList<SelectItem>());
			for(int i = 0; i < getListaGrid().size();i++){
				getListaGridControle().add(new SelectItem(i," "));
			}
			
			setItemSelecionadoGrid(null);			
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);
			setItemSelecionadoGrid(null);	
		}
	}
	
	/**
	 * Selecionar.
	 *
	 * @return the string
	 */
	public String selecionar(){
		ListarParticipantesContratoSaidaDTO registroSelecionado = getListaGrid().get(getItemSelecionadoGrid());
		
		setCpfCnpjRepresentanteAlterar(registroSelecionado.getCpfCnpjFormatado());
		setNomeRazaoRepresentanteAlterar(registroSelecionado.getCdRazaoSocial());
		setNivelRepresentanteAlterar(registroSelecionado.getDsTipoParticipante());
		setSituacaoRepresentanteAlterar(registroSelecionado.getCdDescricaoSituacaoParticapante());		
		return TELA_CONFIRMAR_ALTERACAO;
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt){
		return;
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		setItemSelecionadoGrid(null);
		return TELA_CONSULTAR_CONTRATO;
	}
	
	/**
	 * Voltar alterar.
	 *
	 * @return the string
	 */
	public String voltarAlterar(){
		setItemSelecionadoGrid(null);
		return TELA_ALTERAR_CONTRATO;
	}

	//gets sets
	/**
	 * Get: ativEconRepresentante.
	 *
	 * @return ativEconRepresentante
	 */
	public String getAtivEconRepresentante() {
		return ativEconRepresentante;
	}

	/**
	 * Set: ativEconRepresentante.
	 *
	 * @param ativEconRepresentante the ativ econ representante
	 */
	public void setAtivEconRepresentante(String ativEconRepresentante) {
		this.ativEconRepresentante = ativEconRepresentante;
	}

	/**
	 * Get: cdEmpresaContrato.
	 *
	 * @return cdEmpresaContrato
	 */
	public Long getCdEmpresaContrato() {
		return cdEmpresaContrato;
	}

	/**
	 * Set: cdEmpresaContrato.
	 *
	 * @param cdEmpresaContrato the cd empresa contrato
	 */
	public void setCdEmpresaContrato(Long cdEmpresaContrato) {
		this.cdEmpresaContrato = cdEmpresaContrato;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: cpfCnpjRepresentante.
	 *
	 * @return cpfCnpjRepresentante
	 */
	public String getCpfCnpjRepresentante() {
		return cpfCnpjRepresentante;
	}

	/**
	 * Set: cpfCnpjRepresentante.
	 *
	 * @param cpfCnpjRepresentante the cpf cnpj representante
	 */
	public void setCpfCnpjRepresentante(String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: grupEconRepresentante.
	 *
	 * @return grupEconRepresentante
	 */
	public String getGrupEconRepresentante() {
		return grupEconRepresentante;
	}

	/**
	 * Set: grupEconRepresentante.
	 *
	 * @param grupEconRepresentante the grup econ representante
	 */
	public void setGrupEconRepresentante(String grupEconRepresentante) {
		this.grupEconRepresentante = grupEconRepresentante;
	}

	/**
	 * Get: itemSelecionadoGrid.
	 *
	 * @return itemSelecionadoGrid
	 */
	public Integer getItemSelecionadoGrid() {
		return itemSelecionadoGrid;
	}

	/**
	 * Set: itemSelecionadoGrid.
	 *
	 * @param itemSelecionadoGrid the item selecionado grid
	 */
	public void setItemSelecionadoGrid(Integer itemSelecionadoGrid) {
		this.itemSelecionadoGrid = itemSelecionadoGrid;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarParticipantesContratoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarParticipantesContratoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: listaGridControle.
	 *
	 * @return listaGridControle
	 */
	public List<SelectItem> getListaGridControle() {
		return listaGridControle;
	}

	/**
	 * Set: listaGridControle.
	 *
	 * @param listaGridControle the lista grid controle
	 */
	public void setListaGridControle(List<SelectItem> listaGridControle) {
		this.listaGridControle = listaGridControle;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: nomeRazaoRepresentante.
	 *
	 * @return nomeRazaoRepresentante
	 */
	public String getNomeRazaoRepresentante() {
		return nomeRazaoRepresentante;
	}

	/**
	 * Set: nomeRazaoRepresentante.
	 *
	 * @param nomeRazaoRepresentante the nome razao representante
	 */
	public void setNomeRazaoRepresentante(String nomeRazaoRepresentante) {
		this.nomeRazaoRepresentante = nomeRazaoRepresentante;
	}

	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public String getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(String nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: segRepresentante.
	 *
	 * @return segRepresentante
	 */
	public String getSegRepresentante() {
		return segRepresentante;
	}

	/**
	 * Set: segRepresentante.
	 *
	 * @param segRepresentante the seg representante
	 */
	public void setSegRepresentante(String segRepresentante) {
		this.segRepresentante = segRepresentante;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: subSegRepresentante.
	 *
	 * @return subSegRepresentante
	 */
	public String getSubSegRepresentante() {
		return subSegRepresentante;
	}

	/**
	 * Set: subSegRepresentante.
	 *
	 * @param subSegRepresentante the sub seg representante
	 */
	public void setSubSegRepresentante(String subSegRepresentante) {
		this.subSegRepresentante = subSegRepresentante;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public Long getCdClub() {
		return cdClub;
	}

	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

	/**
	 * Get: cpfCnpjRepresentanteAlterar.
	 *
	 * @return cpfCnpjRepresentanteAlterar
	 */
	public String getCpfCnpjRepresentanteAlterar() {
		return cpfCnpjRepresentanteAlterar;
	}

	/**
	 * Set: cpfCnpjRepresentanteAlterar.
	 *
	 * @param cpfCnpjRepresentanteAlterar the cpf cnpj representante alterar
	 */
	public void setCpfCnpjRepresentanteAlterar(String cpfCnpjRepresentanteAlterar) {
		this.cpfCnpjRepresentanteAlterar = cpfCnpjRepresentanteAlterar;
	}

	/**
	 * Get: nivelRepresentanteAlterar.
	 *
	 * @return nivelRepresentanteAlterar
	 */
	public String getNivelRepresentanteAlterar() {
		return nivelRepresentanteAlterar;
	}

	/**
	 * Set: nivelRepresentanteAlterar.
	 *
	 * @param nivelRepresentanteAlterar the nivel representante alterar
	 */
	public void setNivelRepresentanteAlterar(String nivelRepresentanteAlterar) {
		this.nivelRepresentanteAlterar = nivelRepresentanteAlterar;
	}

	/**
	 * Get: nomeRazaoRepresentanteAlterar.
	 *
	 * @return nomeRazaoRepresentanteAlterar
	 */
	public String getNomeRazaoRepresentanteAlterar() {
		return nomeRazaoRepresentanteAlterar;
	}

	/**
	 * Set: nomeRazaoRepresentanteAlterar.
	 *
	 * @param nomeRazaoRepresentanteAlterar the nome razao representante alterar
	 */
	public void setNomeRazaoRepresentanteAlterar(
			String nomeRazaoRepresentanteAlterar) {
		this.nomeRazaoRepresentanteAlterar = nomeRazaoRepresentanteAlterar;
	}

	/**
	 * Get: situacaoRepresentanteAlterar.
	 *
	 * @return situacaoRepresentanteAlterar
	 */
	public String getSituacaoRepresentanteAlterar() {
		return situacaoRepresentanteAlterar;
	}

	/**
	 * Set: situacaoRepresentanteAlterar.
	 *
	 * @param situacaoRepresentanteAlterar the situacao representante alterar
	 */
	public void setSituacaoRepresentanteAlterar(String situacaoRepresentanteAlterar) {
		this.situacaoRepresentanteAlterar = situacaoRepresentanteAlterar;
	}	
}