/*
 * Nome: br.com.bradesco.web.pgit.view.converters
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 20/05/2015
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.converters;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

// TODO: Auto-generated Javadoc
/**
 * Nome: MilharConverter
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 * @see Converter
 */
public class MilharConverter implements Converter {

	/** Atributo PATTERN. */
	private static final String PATTERN = "###,##0";
	
	/**
	 * Instantiates a new milhar converter.
	 */
	public MilharConverter(){
		super();		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext
	 * , javax.faces.component.UIComponent, java.lang.String)
	 */
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) throws ConverterException {
	    // verifica se arg2 � igual a null ou est� vazio.
	    if (arg2 == null || arg2.trim().equals("")) {
			return null;
		}

		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(new Locale("pt", "BR"));
		df.applyPattern(PATTERN);
		df.setParseBigDecimal(true);

		try {
			return df.parse(arg2);
		} catch (ParseException e) {
			throw new ConverterException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext
	 * , javax.faces.component.UIComponent, java.lang.Object)
	 */
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) throws ConverterException {
		if (arg2 == null) {
			return null;
		}

		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(new Locale("pt", "BR"));
		df.applyPattern(PATTERN);

		return df.format(arg2);
	}
}
