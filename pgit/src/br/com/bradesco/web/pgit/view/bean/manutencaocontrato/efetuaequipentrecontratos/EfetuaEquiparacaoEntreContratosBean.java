/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.efetuaequipentrecontratos
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.efetuaequipentrecontratos;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.IManterEquiparacaoEntreContratosService;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.IncluirEquiparacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.IncluirEquiparacaoContratoOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.IncluirEquiparacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarModalidadeEquiparacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarModalidadeEquiparacaoContratoOcorrencias;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarModalidadeEquiparacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarServicoEquiparacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterequiparacaoentrecontratos.bean.ListarServicoEquiparacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: EfetuaEquiparacaoEntreContratosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EfetuaEquiparacaoEntreContratosBean {

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean = null;

	/** Atributo identificacaoClienteBean. */
	private IdentificacaoClienteBean identificacaoClienteBean = null;

	/** Atributo listaGridTipoServico. */
	private List<ListarServicosSaidaDTO> listaGridTipoServico;
	
	/** Atributo gridListaInclusao. */
	private List<ListarModalidadeEquiparacaoContratoOcorrencias> gridListaInclusao;

	/** Atributo manterEquiparacaoEntreContratosService. */
	private IManterEquiparacaoEntreContratosService manterEquiparacaoEntreContratosService;

	/** Atributo comboService. */
	private IComboService comboService = null;

	/** Atributo listarServicoEquiparacaoContratoSaida. */
	private ListarServicoEquiparacaoContratoSaidaDTO listarServicoEquiparacaoContratoSaida;

	/** Atributo listarModalidadeEquiparacaoContratoSaida. */
	private ListarModalidadeEquiparacaoContratoSaidaDTO listarModalidadeEquiparacaoContratoSaida;

	/** Atributo empresaGestoraFiltroA. */
	private Long empresaGestoraFiltroA;

	/** Atributo tipoContratoFiltroA. */
	private Integer tipoContratoFiltroA;

	/** Atributo numeroFiltroA. */
	private Long numeroFiltroA;

	/** Atributo empresaGestoraFiltroB. */
	private Long empresaGestoraFiltroB;

	/** Atributo tipoContratoFiltroB. */
	private Integer tipoContratoFiltroB;

	/** Atributo numeroFiltroB. */
	private Long numeroFiltroB;

	/** Atributo empresaGestoraContratoA. */
	private String empresaGestoraContratoA;

	/** Atributo tipoContratoA. */
	private String tipoContratoA;

	/** Atributo numeroContratoA. */
	private Long numeroContratoA;

	/** Atributo empresaGestoraContratoB. */
	private String empresaGestoraContratoB;

	/** Atributo tipoContratoB. */
	private String tipoContratoB;

	/** Atributo numeroContratoB. */
	private Long numeroContratoB;

	/** Atributo habilitaCamposContratoB. */
	private boolean habilitaCamposContratoB;

	/** Atributo habilitaCamposContratoA. */
	private boolean habilitaCamposContratoA;
	
	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos = false;

	/** Atributo opcaoChecarTodosMod. */
	private boolean opcaoChecarTodosMod = false;

	/** Atributo habilitarServicoAvancar. */
	private boolean habilitarServicoAvancar;

	/** Atributo habilitarServicoAvancarModalidade. */
	private boolean habilitarServicoAvancarModalidade;
	
	/** Atributo itemFiltroSelecionado. */
	private String itemFiltroSelecionado;

	/** Atributo CONMANTEREQUIPARACAOENTRECONTRATOS. */
	private final static String CONMANTEREQUIPARACAOENTRECONTRATOS = "conManterEquiparacaoEntreContratos";

	/** Atributo MODEQUIPARACAOENTRECONTRATOS. */
	private final static String MODEQUIPARACAOENTRECONTRATOS = "modEquiparacaoEntreContratos";

	/** Atributo INCEQUIPARACAOENTRECONTRATOS. */
	private final static String INCEQUIPARACAOENTRECONTRATOS = "incEquiparacaoEntreContratos";
	
	/** Atributo pagRetorno. */
	private String pagRetorno = "";
	
	
	/**
	 * Inicializar pagina.
	 *
	 * @param actionEvent the action event
	 */
	public void inicializarPagina(ActionEvent actionEvent) {
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		listarTipoServico();

		this.identificacaoClienteContratoBean.setPaginaRetorno(CONMANTEREQUIPARACAOENTRECONTRATOS);
		this.identificacaoClienteBean.setPaginaRetorno(CONMANTEREQUIPARACAOENTRECONTRATOS);

		this.setHabilitaCamposContratoB(true);
		this.setHabilitaCamposContratoA(false);
		this.setHabilitarServicoAvancar(true);
		this.setHabilitarServicoAvancarModalidade(true);
		
		setEmpresaGestoraFiltroA(2269651L);
		setEmpresaGestoraFiltroB(2269651L);
	}
	
	/**
	 * Inicializar pagina.
	 */
	public void inicializarPagina() {
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		listarTipoServico();
		
		this.identificacaoClienteContratoBean.setPaginaRetorno(CONMANTEREQUIPARACAOENTRECONTRATOS);
		this.identificacaoClienteBean.setPaginaRetorno(CONMANTEREQUIPARACAOENTRECONTRATOS);
		
		this.setHabilitaCamposContratoB(true);
		this.setHabilitaCamposContratoA(false);
		this.setHabilitarServicoAvancar(true);
		this.setHabilitarServicoAvancarModalidade(true);
		

	}

	/**
	 * Listar tipo servico.
	 */
	private void listarTipoServico() {
		try {
			listaGridTipoServico = comboService.listarTipoServicosRelacionados();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
	}

	/**
	 * Consultar contrato a.
	 *
	 * @return the string
	 */
	public String consultarContratoA() {
		this.setHabilitaCamposContratoB(false);
		this.setHabilitaCamposContratoA(true);
		String pagRetorno = "";
		try {
			identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().setCdPessoaJuridicaContrato(
							this.getEmpresaGestoraFiltroA());
			identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().setCdTipoContratoNegocio(
							this.getTipoContratoFiltroA());
			identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().setNrSeqContratoNegocio(
							this.getNumeroFiltroA());

			pagRetorno = this.identificacaoClienteBean.consultarContratos();

			this.setEmpresaGestoraContratoA(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas()
							.getDsPessoaJuridicaContrato());
			this.setTipoContratoA(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas()
							.getDsTipoContratoNegocio());
			this.setNumeroContratoA(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas()
							.getNrSeqContratoNegocio());
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			this.setHabilitaCamposContratoA(false);
		}
		return pagRetorno;
	}

	/**
	 * Consultar contrato b.
	 *
	 * @return the string
	 */
	public String consultarContratoB() {
		setPagRetorno("");
		try {
			setHabilitaCamposContratoB(true);
			identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().setCdPessoaJuridicaContrato(
							this.getEmpresaGestoraFiltroB());
			identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().setCdTipoContratoNegocio(
							this.getTipoContratoFiltroB());
			identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().setNrSeqContratoNegocio(
							this.getNumeroFiltroB());

			pagRetorno = this.identificacaoClienteBean.consultarContratos();
			
			if(pagRetorno != null){

				this.setEmpresaGestoraContratoB(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas()
								.getDsPessoaJuridicaContrato());
				this.setTipoContratoB(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas()
								.getDsTipoContratoNegocio());
				this.setNumeroContratoB(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas()
								.getNrSeqContratoNegocio());
	
				ListarServicoEquiparacaoContratoEntradaDTO entrada = new ListarServicoEquiparacaoContratoEntradaDTO();
				entrada.setCdPessoaJuridicaContrato1(this.getEmpresaGestoraFiltroA());
				entrada.setCdTipoContratoNegocio1(this.getTipoContratoFiltroA());
				entrada.setNrSequenciaContratoNegocio1(this.getNumeroFiltroA());
				entrada.setCdPessoaJuridicaContrato2(this.getEmpresaGestoraFiltroB());
				entrada.setCdTipoContratoNegocio2(this.getTipoContratoFiltroB());
				entrada.setNrSequenciaContratoNegocio2(this.getNumeroFiltroB());
	
				listarServicoEquiparacaoContratoSaida = getManterEquiparacaoEntreContratosService()
								.listarServicoEquiparacaoContrato(entrada);
			}else{
				setEmpresaGestoraContratoB("");
				setTipoContratoB("");
				setNumeroContratoB(null);
				setHabilitaCamposContratoB(false);
			}

		} catch (PdcAdapterException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ")" +   p.getMessage(), "#{efetuaEquiparacaoEntreContratosBean.pagRetorno}",BradescoViewExceptionActionType.ACTION,false);
					
				setEmpresaGestoraContratoB("");
				setTipoContratoB("");
				setNumeroContratoB(null);
				setHabilitaCamposContratoB(false);
		}
		
		return "";
	}
	
	
	
	/**
	 * Pag retorno.
	 *
	 * @return the string
	 */
	public String pagRetorno(){
		return pagRetorno;
	}
	
	/**
	 * Limpar dados pesquisa contrato a.
	 *
	 * @return the string
	 */
	public String limparDadosPesquisaContratoA() {
		
		this.setEmpresaGestoraFiltroA(2269651L);
		this.setTipoContratoFiltroA(null);
		this.setNumeroFiltroA(null);
		setEmpresaGestoraContratoA(null);
		setTipoContratoA(null);
		setNumeroContratoA(null);
		this.setHabilitaCamposContratoA(false);
		
		limparDadosPesquisaContratoB();

		return CONMANTEREQUIPARACAOENTRECONTRATOS;
	}

	/**
	 * Limpar dados.
	 */
	public void limparDados() {
		limparDadosPesquisaContratoA();
		listarModalidadeEquiparacaoContratoSaida =  new ListarModalidadeEquiparacaoContratoSaidaDTO();	
		this.setHabilitaCamposContratoB(true);
		this.setHabilitaCamposContratoA(false);
		setItemFiltroSelecionado(null);
		
		
	}

	/**
	 * Limpar dados pesquisa contrato b.
	 *
	 * @return the string
	 */
	public String limparDadosPesquisaContratoB() {
		this.setEmpresaGestoraFiltroB(2269651L);
		this.setTipoContratoFiltroB(null);
		this.setNumeroFiltroB(null);
		setEmpresaGestoraContratoB(null);
		setTipoContratoB(null);
		setNumeroContratoB(null);
		setHabilitaCamposContratoB(false);
		listarServicoEquiparacaoContratoSaida =  new ListarServicoEquiparacaoContratoSaidaDTO();

		return CONMANTEREQUIPARACAOENTRECONTRATOS;
	}

	/**
	 * Limpar lista servico.
	 *
	 * @return the string
	 */
	public String limparListaServico() {
		
		if(getListarServicoEquiparacaoContratoSaida() != null){
			for (int i = 0; i < getListarServicoEquiparacaoContratoSaida().getOcorrencias().size(); i++) {
				if (getListarServicoEquiparacaoContratoSaida().getOcorrencias().get(i).isCheck()) {
					getListarServicoEquiparacaoContratoSaida().getOcorrencias().get(i).setCheck(false);
				}
			}
		}
		

		setHabilitarServicoAvancar(true);

		return CONMANTEREQUIPARACAOENTRECONTRATOS;
	}

	/**
	 * Limpar lista modalidade.
	 *
	 * @return the string
	 */
	public String limparListaModalidade() {
	
		for (int i = 0; i < getListarModalidadeEquiparacaoContratoSaida().getOcorrencias().size(); i++) {
			if (getListarModalidadeEquiparacaoContratoSaida().getOcorrencias().get(i).isCheck()) {
				getListarModalidadeEquiparacaoContratoSaida().getOcorrencias().get(i).setCheck(false);
			}
		}

		setHabilitarServicoAvancarModalidade(true);

		return MODEQUIPARACAOENTRECONTRATOS;
	}

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {

		return CONMANTEREQUIPARACAOENTRECONTRATOS;
	}

	/**
	 * Voltar modalidade.
	 *
	 * @return the string
	 */
	public String voltarModalidade() {
		gridListaInclusao = new ArrayList<ListarModalidadeEquiparacaoContratoOcorrencias>();
		
		return MODEQUIPARACAOENTRECONTRATOS;
		
	}
	
	/**
	 * Voltar confirmar.
	 */
	public void voltarConfirmar(){
		voltarConsultar();
	}

	/**
	 * Get: selectItemListaTipoServico.
	 *
	 * @return selectItemListaTipoServico
	 */
	public List<SelectItem> getSelectItemListaTipoServico() {
		if (this.listarServicoEquiparacaoContratoSaida.getOcorrencias() == null
						|| this.listarServicoEquiparacaoContratoSaida.getOcorrencias().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.listarServicoEquiparacaoContratoSaida.getOcorrencias().size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}
			return list;
		}
	}

	/**
	 * Get: selectItemListaModalidades.
	 *
	 * @return selectItemListaModalidades
	 */
	public List<SelectItem> getSelectItemListaModalidades() {
		if (this.listarModalidadeEquiparacaoContratoSaida.getOcorrencias() == null
						|| this.listarModalidadeEquiparacaoContratoSaida.getOcorrencias().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.listarModalidadeEquiparacaoContratoSaida.getOcorrencias().size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}
			return list;
		}
	}
	
	/**
	 * Avancar.
	 *
	 * @return the string
	 */
	public String avancar() {
		if(getItemFiltroSelecionado().equals("0")){
			return avancarModalidades();
						
		}
		else{
			return avancarConfirmar();
		}
	}
	
	/**
	 * Avancar modalidades.
	 *
	 * @return the string
	 */
	private String avancarModalidades() {
		try {
			ListarModalidadeEquiparacaoContratoEntradaDTO entrada = new ListarModalidadeEquiparacaoContratoEntradaDTO();
			entrada.setCdPessoaJuridicaContrato1(this.getEmpresaGestoraFiltroA());
			entrada.setCdTipoContratoNegocio1(this.getTipoContratoFiltroA());
			entrada.setNrSequenciaContratoNegocio1(this.getNumeroFiltroA());
			entrada.setCdPessoaJuridicaContrato2(this.getEmpresaGestoraFiltroB());
			entrada.setCdTipoContratoNegocio2(this.getTipoContratoFiltroB());
			entrada.setNrSequenciaContratoNegocio2(this.getNumeroFiltroB());

			listarModalidadeEquiparacaoContratoSaida = getManterEquiparacaoEntreContratosService()
							.listarModalidadeEquiparacaoContrato(entrada);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		
		return MODEQUIPARACAOENTRECONTRATOS;
		
	}
	
	/**
	 * Pesquisar contrato b.
	 *
	 * @param evt the evt
	 */
	public void pesquisarContratoB(ActionEvent evt){
		consultarContratoB();
	}
	
	/**
	 * Pesquisar modalidade.
	 *
	 * @param evt the evt
	 */
	public void pesquisarModalidade(ActionEvent evt){
		avancarModalidades();
	}

	/**
	 * Avancar confirmar.
	 *
	 * @return the string
	 */
	public String avancarConfirmar() {
		ListarModalidadeEquiparacaoContratoOcorrencias ocorrencia = null;
		gridListaInclusao = new ArrayList<ListarModalidadeEquiparacaoContratoOcorrencias>();
		
		for (int i = 0; i < listarServicoEquiparacaoContratoSaida.getNumeroLinhas(); i++) {
			if (listarServicoEquiparacaoContratoSaida.getOcorrencias().get(i).isCheck()) {
				ocorrencia = new ListarModalidadeEquiparacaoContratoOcorrencias();
				ocorrencia.setDsProdutoServicoOperacao(listarServicoEquiparacaoContratoSaida.getOcorrencias()
									.get(i).getDsProdutoOperRelacionado());
				ocorrencia.setDsProdutoOperRelacionado("");
				
				gridListaInclusao.add(ocorrencia);
			}
		}
		
		if (listarModalidadeEquiparacaoContratoSaida != null) {
			for (int i = 0; i < listarModalidadeEquiparacaoContratoSaida.getNumeroLinhas(); i++) {
				if (listarModalidadeEquiparacaoContratoSaida.getOcorrencias().get(i).isCheck()) {
					ocorrencia = new ListarModalidadeEquiparacaoContratoOcorrencias();
					ocorrencia.setDsProdutoOperRelacionado(listarModalidadeEquiparacaoContratoSaida.getOcorrencias()
										.get(i).getDsProdutoOperRelacionado());
					ocorrencia.setDsProdutoServicoOperacao(listarModalidadeEquiparacaoContratoSaida.getOcorrencias()
									.get(i).getDsProdutoServicoOperacao());
					
					gridListaInclusao.add(ocorrencia);
				}
			}
		}
			
		return INCEQUIPARACAOENTRECONTRATOS;
	}

	/**
	 * Confirmar.
	 *
	 * @return the string
	 */
	public String confirmar() {
		try {
			IncluirEquiparacaoContratoEntradaDTO entrada = new IncluirEquiparacaoContratoEntradaDTO();
			List<IncluirEquiparacaoContratoOcorrenciasDTO> ocorrencias = new ArrayList<IncluirEquiparacaoContratoOcorrenciasDTO>();
			IncluirEquiparacaoContratoOcorrenciasDTO ocorrencia = null;

			int qtdeOcorrencias = 0;

			for (int i = 0; i < listarServicoEquiparacaoContratoSaida.getNumeroLinhas(); i++) {
				if (listarServicoEquiparacaoContratoSaida.getOcorrencias().get(i).isCheck()) {
					qtdeOcorrencias++;
					ocorrencia = new IncluirEquiparacaoContratoOcorrenciasDTO();
					ocorrencia.setCdTipoSelecao(1);
					ocorrencia.setCdPessoaJuridicaContrato1(getEmpresaGestoraFiltroA());
					ocorrencia.setCdTipoContratoNegocio1(getTipoContratoFiltroA());
					ocorrencia.setNrSequenciaContratoNegocio1(getNumeroFiltroA());
					ocorrencia.setCdPessoaJuridicaContrato2(getEmpresaGestoraFiltroB());
					ocorrencia.setCdTipoContratoNegocio2(getTipoContratoFiltroB());
					ocorrencia.setNrSequenciaContratoNegocio2(getNumeroFiltroB());
					ocorrencia.setCdProdutoServicoOperacao(listarServicoEquiparacaoContratoSaida.getOcorrencias()
									.get(i).getCdProdutoOperacaoRelacionado());
					ocorrencia.setCdProdutoOperacaoRelacionado(0);

					ocorrencias.add(ocorrencia);
				}
			}

			if (listarModalidadeEquiparacaoContratoSaida != null) {
				for (int i = 0; i < listarModalidadeEquiparacaoContratoSaida.getNumeroLinhas(); i++) {
					if (listarModalidadeEquiparacaoContratoSaida.getOcorrencias().get(i).isCheck()) {
						qtdeOcorrencias++;
						ocorrencia = new IncluirEquiparacaoContratoOcorrenciasDTO();
						ocorrencia.setCdTipoSelecao(2);
						ocorrencia.setCdPessoaJuridicaContrato1(getEmpresaGestoraFiltroA());
						ocorrencia.setCdTipoContratoNegocio1(getTipoContratoFiltroA());
						ocorrencia.setNrSequenciaContratoNegocio1(getNumeroFiltroA());
						ocorrencia.setCdPessoaJuridicaContrato2(getEmpresaGestoraFiltroB());
						ocorrencia.setCdTipoContratoNegocio2(getTipoContratoFiltroB());
						ocorrencia.setNrSequenciaContratoNegocio2(getNumeroFiltroB());
						ocorrencia.setCdProdutoServicoOperacao(listarModalidadeEquiparacaoContratoSaida
										.getOcorrencias().get(i).getCdProdutoServicoOperacao());
						ocorrencia.setCdProdutoOperacaoRelacionado(listarModalidadeEquiparacaoContratoSaida
										.getOcorrencias().get(i).getCdProdutoOperacaoRelacionado());

						ocorrencias.add(ocorrencia);
					}
				}
			}

			entrada.setMaxOcorrencias(qtdeOcorrencias);
			entrada.setOcorrencias(ocorrencias);

			IncluirEquiparacaoContratoSaidaDTO saida = getManterEquiparacaoEntreContratosService()
							.incluirEquiparacaoContrato(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
					"#{efetuaEquiparacaoEntreContratosBean.confirmaInclusao}", BradescoViewExceptionActionType.ACTION, false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		
		return "";
	}
	
	/**
	 * Confirma inclusao.
	 *
	 * @return the string
	 */
	public String confirmaInclusao(){
		limparDados();
	
		inicializarPagina();
		
		return CONMANTEREQUIPARACAOENTRECONTRATOS;
	}
	
	/**
	 * Abre popup.
	 *
	 * @return the string
	 */
	public String abrePopup() {
		
		return "ABREPOPUP";
	}
	
	/**
	 * Habilita botao avancar.
	 */
	public void habilitaBotaoAvancar() {
		setHabilitarServicoAvancar(true);
		
		if(getItemFiltroSelecionado() != null){
			for (int i = 0; i < getListarServicoEquiparacaoContratoSaida().getOcorrencias().size(); i++) {
				if (getListarServicoEquiparacaoContratoSaida().getOcorrencias().get(i).isCheck()) {
					setHabilitarServicoAvancar(false);
				}
			}
		}
		
	}

	/**
	 * Habilita botao avancar modalidade.
	 */
	public void habilitaBotaoAvancarModalidade() {
		setHabilitarServicoAvancarModalidade(true);

		for (int i = 0; i < getListarModalidadeEquiparacaoContratoSaida().getOcorrencias().size(); i++) {
			if (getListarModalidadeEquiparacaoContratoSaida().getOcorrencias().get(i).isCheck()) {
				setHabilitarServicoAvancarModalidade(false);
			}
		}
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: listaGridTipoServico.
	 *
	 * @return listaGridTipoServico
	 */
	public List<ListarServicosSaidaDTO> getListaGridTipoServico() {
		return listaGridTipoServico;
	}

	/**
	 * Set: listaGridTipoServico.
	 *
	 * @param listaGridTipoServico the lista grid tipo servico
	 */
	public void setListaGridTipoServico(List<ListarServicosSaidaDTO> listaGridTipoServico) {
		this.listaGridTipoServico = listaGridTipoServico;
	}

	/**
	 * Get: empresaGestoraContratoA.
	 *
	 * @return empresaGestoraContratoA
	 */
	public String getEmpresaGestoraContratoA() {
		return empresaGestoraContratoA;
	}

	/**
	 * Set: empresaGestoraContratoA.
	 *
	 * @param empresaGestoraContratoA the empresa gestora contrato a
	 */
	public void setEmpresaGestoraContratoA(String empresaGestoraContratoA) {
		this.empresaGestoraContratoA = empresaGestoraContratoA;
	}

	/**
	 * Get: tipoContratoA.
	 *
	 * @return tipoContratoA
	 */
	public String getTipoContratoA() {
		return tipoContratoA;
	}

	/**
	 * Set: tipoContratoA.
	 *
	 * @param tipoContratoA the tipo contrato a
	 */
	public void setTipoContratoA(String tipoContratoA) {
		this.tipoContratoA = tipoContratoA;
	}

	/**
	 * Get: numeroContratoA.
	 *
	 * @return numeroContratoA
	 */
	public Long getNumeroContratoA() {
		return numeroContratoA;
	}

	/**
	 * Set: numeroContratoA.
	 *
	 * @param numeroContratoA the numero contrato a
	 */
	public void setNumeroContratoA(Long numeroContratoA) {
		this.numeroContratoA = numeroContratoA;
	}

	/**
	 * Get: empresaGestoraContratoB.
	 *
	 * @return empresaGestoraContratoB
	 */
	public String getEmpresaGestoraContratoB() {
		return empresaGestoraContratoB;
	}

	/**
	 * Set: empresaGestoraContratoB.
	 *
	 * @param empresaGestoraContratoB the empresa gestora contrato b
	 */
	public void setEmpresaGestoraContratoB(String empresaGestoraContratoB) {
		this.empresaGestoraContratoB = empresaGestoraContratoB;
	}

	/**
	 * Get: tipoContratoB.
	 *
	 * @return tipoContratoB
	 */
	public String getTipoContratoB() {
		return tipoContratoB;
	}

	/**
	 * Set: tipoContratoB.
	 *
	 * @param tipoContratoB the tipo contrato b
	 */
	public void setTipoContratoB(String tipoContratoB) {
		this.tipoContratoB = tipoContratoB;
	}

	/**
	 * Get: numeroContratoB.
	 *
	 * @return numeroContratoB
	 */
	public Long getNumeroContratoB() {
		return numeroContratoB;
	}

	/**
	 * Set: numeroContratoB.
	 *
	 * @param numeroContratoB the numero contrato b
	 */
	public void setNumeroContratoB(Long numeroContratoB) {
		this.numeroContratoB = numeroContratoB;
	}

	/**
	 * Get: empresaGestoraFiltroA.
	 *
	 * @return empresaGestoraFiltroA
	 */
	public Long getEmpresaGestoraFiltroA() {
		return empresaGestoraFiltroA;
	}

	/**
	 * Set: empresaGestoraFiltroA.
	 *
	 * @param empresaGestoraFiltroA the empresa gestora filtro a
	 */
	public void setEmpresaGestoraFiltroA(Long empresaGestoraFiltroA) {
		this.empresaGestoraFiltroA = empresaGestoraFiltroA;
	}

	/**
	 * Get: tipoContratoFiltroA.
	 *
	 * @return tipoContratoFiltroA
	 */
	public Integer getTipoContratoFiltroA() {
		return tipoContratoFiltroA;
	}

	/**
	 * Set: tipoContratoFiltroA.
	 *
	 * @param tipoContratoFiltroA the tipo contrato filtro a
	 */
	public void setTipoContratoFiltroA(Integer tipoContratoFiltroA) {
		this.tipoContratoFiltroA = tipoContratoFiltroA;
	}

	/**
	 * Get: numeroFiltroA.
	 *
	 * @return numeroFiltroA
	 */
	public Long getNumeroFiltroA() {
		return numeroFiltroA;
	}

	/**
	 * Set: numeroFiltroA.
	 *
	 * @param numeroFiltroA the numero filtro a
	 */
	public void setNumeroFiltroA(Long numeroFiltroA) {
		this.numeroFiltroA = numeroFiltroA;
	}

	/**
	 * Get: empresaGestoraFiltroB.
	 *
	 * @return empresaGestoraFiltroB
	 */
	public Long getEmpresaGestoraFiltroB() {
		return empresaGestoraFiltroB;
	}

	/**
	 * Set: empresaGestoraFiltroB.
	 *
	 * @param empresaGestoraFiltroB the empresa gestora filtro b
	 */
	public void setEmpresaGestoraFiltroB(Long empresaGestoraFiltroB) {
		this.empresaGestoraFiltroB = empresaGestoraFiltroB;
	}

	/**
	 * Get: tipoContratoFiltroB.
	 *
	 * @return tipoContratoFiltroB
	 */
	public Integer getTipoContratoFiltroB() {
		return tipoContratoFiltroB;
	}

	/**
	 * Set: tipoContratoFiltroB.
	 *
	 * @param tipoContratoFiltroB the tipo contrato filtro b
	 */
	public void setTipoContratoFiltroB(Integer tipoContratoFiltroB) {
		this.tipoContratoFiltroB = tipoContratoFiltroB;
	}

	/**
	 * Get: numeroFiltroB.
	 *
	 * @return numeroFiltroB
	 */
	public Long getNumeroFiltroB() {
		return numeroFiltroB;
	}

	/**
	 * Set: numeroFiltroB.
	 *
	 * @param numeroFiltroB the numero filtro b
	 */
	public void setNumeroFiltroB(Long numeroFiltroB) {
		this.numeroFiltroB = numeroFiltroB;
	}

	/**
	 * Get: identificacaoClienteBean.
	 *
	 * @return identificacaoClienteBean
	 */
	public IdentificacaoClienteBean getIdentificacaoClienteBean() {
		return identificacaoClienteBean;
	}

	/**
	 * Set: identificacaoClienteBean.
	 *
	 * @param identificacaoClienteBean the identificacao cliente bean
	 */
	public void setIdentificacaoClienteBean(IdentificacaoClienteBean identificacaoClienteBean) {
		this.identificacaoClienteBean = identificacaoClienteBean;
	}

	/**
	 * Is habilita campos contrato b.
	 *
	 * @return true, if is habilita campos contrato b
	 */
	public boolean isHabilitaCamposContratoB() {
		return habilitaCamposContratoB;
	}

	/**
	 * Set: habilitaCamposContratoB.
	 *
	 * @param habilitaCamposContratoB the habilita campos contrato b
	 */
	public void setHabilitaCamposContratoB(boolean habilitaCamposContratoB) {
		this.habilitaCamposContratoB = habilitaCamposContratoB;
	}

	/**
	 * Get: manterEquiparacaoEntreContratosService.
	 *
	 * @return manterEquiparacaoEntreContratosService
	 */
	public IManterEquiparacaoEntreContratosService getManterEquiparacaoEntreContratosService() {
		return manterEquiparacaoEntreContratosService;
	}

	/**
	 * Set: manterEquiparacaoEntreContratosService.
	 *
	 * @param manterEquiparacaoEntreContratosService the manter equiparacao entre contratos service
	 */
	public void setManterEquiparacaoEntreContratosService(
					IManterEquiparacaoEntreContratosService manterEquiparacaoEntreContratosService) {
		this.manterEquiparacaoEntreContratosService = manterEquiparacaoEntreContratosService;
	}

	/**
	 * Get: listarServicoEquiparacaoContratoSaida.
	 *
	 * @return listarServicoEquiparacaoContratoSaida
	 */
	public ListarServicoEquiparacaoContratoSaidaDTO getListarServicoEquiparacaoContratoSaida() {
		return listarServicoEquiparacaoContratoSaida;
	}

	/**
	 * Set: listarServicoEquiparacaoContratoSaida.
	 *
	 * @param listarServicoEquiparacaoContratoSaida the listar servico equiparacao contrato saida
	 */
	public void setListarServicoEquiparacaoContratoSaida(
					ListarServicoEquiparacaoContratoSaidaDTO listarServicoEquiparacaoContratoSaida) {
		this.listarServicoEquiparacaoContratoSaida = listarServicoEquiparacaoContratoSaida;
	}

	/**
	 * Get: listarModalidadeEquiparacaoContratoSaida.
	 *
	 * @return listarModalidadeEquiparacaoContratoSaida
	 */
	public ListarModalidadeEquiparacaoContratoSaidaDTO getListarModalidadeEquiparacaoContratoSaida() {
		return listarModalidadeEquiparacaoContratoSaida;
	}

	/**
	 * Set: listarModalidadeEquiparacaoContratoSaida.
	 *
	 * @param listarModalidadeEquiparacaoContratoSaida the listar modalidade equiparacao contrato saida
	 */
	public void setListarModalidadeEquiparacaoContratoSaida(
					ListarModalidadeEquiparacaoContratoSaidaDTO listarModalidadeEquiparacaoContratoSaida) {
		this.listarModalidadeEquiparacaoContratoSaida = listarModalidadeEquiparacaoContratoSaida;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Is habilitar servico avancar.
	 *
	 * @return true, if is habilitar servico avancar
	 */
	public boolean isHabilitarServicoAvancar() {
		return habilitarServicoAvancar;
	}

	/**
	 * Set: habilitarServicoAvancar.
	 *
	 * @param habilitarServicoAvancar the habilitar servico avancar
	 */
	public void setHabilitarServicoAvancar(boolean habilitarServicoAvancar) {
		this.habilitarServicoAvancar = habilitarServicoAvancar;
	}

	/**
	 * Is opcao checar todos mod.
	 *
	 * @return true, if is opcao checar todos mod
	 */
	public boolean isOpcaoChecarTodosMod() {
		return opcaoChecarTodosMod;
	}

	/**
	 * Set: opcaoChecarTodosMod.
	 *
	 * @param opcaoChecarTodosMod the opcao checar todos mod
	 */
	public void setOpcaoChecarTodosMod(boolean opcaoChecarTodosMod) {
		this.opcaoChecarTodosMod = opcaoChecarTodosMod;
	}

	/**
	 * Is habilitar servico avancar modalidade.
	 *
	 * @return true, if is habilitar servico avancar modalidade
	 */
	public boolean isHabilitarServicoAvancarModalidade() {
		return habilitarServicoAvancarModalidade;
	}

	/**
	 * Set: habilitarServicoAvancarModalidade.
	 *
	 * @param habilitarServicoAvancarModalidade the habilitar servico avancar modalidade
	 */
	public void setHabilitarServicoAvancarModalidade(boolean habilitarServicoAvancarModalidade) {
		this.habilitarServicoAvancarModalidade = habilitarServicoAvancarModalidade;
	}

	/**
	 * Get: gridListaInclusao.
	 *
	 * @return gridListaInclusao
	 */
	public List<ListarModalidadeEquiparacaoContratoOcorrencias> getGridListaInclusao() {
		return gridListaInclusao;
	}

	/**
	 * Set: gridListaInclusao.
	 *
	 * @param gridListaInclusao the grid lista inclusao
	 */
	public void setGridListaInclusao(List<ListarModalidadeEquiparacaoContratoOcorrencias> gridListaInclusao) {
		this.gridListaInclusao = gridListaInclusao;
	}

	/**
	 * Get: itemFiltroSelecionado.
	 *
	 * @return itemFiltroSelecionado
	 */
	public String getItemFiltroSelecionado() {
		return itemFiltroSelecionado;
	}

	/**
	 * Set: itemFiltroSelecionado.
	 *
	 * @param itemFiltroSelecionado the item filtro selecionado
	 */
	public void setItemFiltroSelecionado(String itemFiltroSelecionado) {
		this.itemFiltroSelecionado = itemFiltroSelecionado;
	}
	
	/**
	 * Is habilita campos contrato a.
	 *
	 * @return true, if is habilita campos contrato a
	 */
	public boolean isHabilitaCamposContratoA() {
		return habilitaCamposContratoA;
	}
	
	/**
	 * Set: habilitaCamposContratoA.
	 *
	 * @param habilitaCamposContratoA the habilita campos contrato a
	 */
	public void setHabilitaCamposContratoA(boolean habilitaCamposContratoA) {
		this.habilitaCamposContratoA = habilitaCamposContratoA;
	}
	
	/**
	 * Is disabled copia modalidade.
	 *
	 * @return true, if is disabled copia modalidade
	 */
	public boolean isDisabledCopiaModalidade(){
		if(habilitaCamposContratoA && habilitaCamposContratoB){
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * Get: pagRetorno.
	 *
	 * @return pagRetorno
	 */
	public String getPagRetorno() {
		return pagRetorno;
	}
	
	/**
	 * Set: pagRetorno.
	 *
	 * @param pagRetorno the pag retorno
	 */
	public void setPagRetorno(String pagRetorno) {
		this.pagRetorno = pagRetorno;
	}
}
