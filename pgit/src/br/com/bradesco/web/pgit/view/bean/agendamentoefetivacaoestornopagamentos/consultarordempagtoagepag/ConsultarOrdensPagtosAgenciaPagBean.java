/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.consultarordempagtoagepag
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.consultarordempagtoagepag;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.IConsultarOrdemPagtoAgePagService;
import br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.bean.ConsultarOrdemPagtoAgePagEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarordempagtoagepag.bean.ConsultarOrdemPagtoAgePagSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ConsultarOrdensPagtosAgenciaPagBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarOrdensPagtosAgenciaPagBean {
	
	/** Atributo TELA_DETALHE. */
	private static final String TELA_DETALHE = "DETALHE_ORDEM_PAGTO";

	/** Atributo TELA_DETALHE_RETIRADA. */
	private static final String TELA_DETALHE_RETIRADA = "DETALHE_RETIRADA";

	/** Atributo TELA_CONSULTA. */
	private static final String TELA_CONSULTA = "CONSULTA_ORDEM_PAGTO";

	/** Atributo CODIGO_IDENTIFICACAO_DIVERSOS. */
	private static final String CODIGO_IDENTIFICACAO_DIVERSOS = "4";

	/** Atributo consultasServiceImpl. */
	private IConsultasService consultasServiceImpl;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo consultarOrdemPagtoAgePagImpl. */
	private IConsultarOrdemPagtoAgePagService consultarOrdemPagtoAgePagImpl;
	
	/** Atributo consultarPagamentosIndividualImpl. */
	private IConsultarPagamentosIndividualService consultarPagamentosIndividualImpl;	
	
	// ATRIBUTOS CONSULTA
	/** Atributo agencia. */
	private String agencia;
	
	/** Atributo valorInicial. */
	private BigDecimal valorInicial;
	
	/** Atributo valorFinal. */
	private BigDecimal valorFinal;
	
	/** Atributo dataCreditoOpDe. */
	private Date dataCreditoOpDe;
	
	/** Atributo dataCreditoOpAte. */
	private Date dataCreditoOpAte;
	
	/** Atributo cmbSituacaoPagamento. */
	private Integer cmbSituacaoPagamento;
	
	/** Atributo listaCmbSituacaoPagamento. */
	private List<SelectItem> listaCmbSituacaoPagamento = new ArrayList<SelectItem>();
	
	/** Atributo cmbFormaPagamento. */
	private Integer cmbFormaPagamento;
	
	/** Atributo inscricaoFavorecido. */
	private String inscricaoFavorecido;
	
	/** Atributo cmbTipoFavorecido. */
	private Integer cmbTipoFavorecido;
	
	/** Atributo listaCmbTipoFavorecido. */
	private List<SelectItem> listaCmbTipoFavorecido = new ArrayList<SelectItem>();	
	
	/** Atributo cnpjCorpo. */
	private String cnpjCorpo;
	
	/** Atributo cnpjFilial. */
	private String cnpjFilial;
	
	/** Atributo cnpjControle. */
	private String cnpjControle;
	
	/** Atributo cpfCorpo. */
	private String cpfCorpo;
	
	/** Atributo cpfControle. */
	private String cpfControle;
	
	/** Atributo radioCpfCnpj. */
	private String radioCpfCnpj;
	
	/** Atributo itemSelecionado. */
	private Integer itemSelecionado;
	
	/** Atributo listaGridOrdemPagtoAgePag. */
	private List<ConsultarOrdemPagtoAgePagSaidaDTO> listaGridOrdemPagtoAgePag;
	
	/** Atributo listaControleOrdemPagtoAgePag. */
	private List<SelectItem> listaControleOrdemPagtoAgePag;
	
	// ATRIBUTOS DETALHE
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo nroContrato. */
	private Long nroContrato;
	
	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;
	
	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;
	
	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;
	
	/** Atributo tipoContaDebito. */
	private String tipoContaDebito;
	
	/** Atributo cdFavorecido. */
	private Long cdFavorecido;
	
	/** Atributo dsBancoFavorecido. */
	private String dsBancoFavorecido;
	
	/** Atributo dsContaFavorecido. */
	private String dsContaFavorecido;
	
	/** Atributo dsAgenciaFavorecido. */
	private String dsAgenciaFavorecido;
	
	/** Atributo cdTipoContaFavorecido. */
	private Integer cdTipoContaFavorecido;
	
	/** Atributo dsTipoContaFavorecido. */
	private String dsTipoContaFavorecido;
	
	/** Atributo dtNascimentoBeneficiario. */
	private String dtNascimentoBeneficiario;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dsIndicadorModalidade. */
	private String dsIndicadorModalidade;
	
	/** Atributo nrSequenciaArquivoRemessa. */
	private String nrSequenciaArquivoRemessa;
	
	/** Atributo nrLoteArquivoRemessa. */
	private String nrLoteArquivoRemessa;
	
	/** Atributo numeroPagamento. */
	private String numeroPagamento;
	
	/** Atributo cdListaDebito. */
	private String cdListaDebito;
	
	/** Atributo dtAgendamento. */
	private String dtAgendamento;
	
	/** Atributo dtPagamento. */
	private String dtPagamento;
	
	/** Atributo dtVencimento. */
	private String dtVencimento;
	
	/** Atributo cdIndicadorEconomicoMoeda. */
	private String cdIndicadorEconomicoMoeda;
	
	/** Atributo qtMoeda. */
	private BigDecimal qtMoeda;
	
	/** Atributo vlrAgendamento. */
	private BigDecimal vlrAgendamento;
	
	/** Atributo vlrEfetivacao. */
	private BigDecimal vlrEfetivacao;
	
	/** Atributo cdSituacaoOperacaoPagamento. */
	private String cdSituacaoOperacaoPagamento;
	
	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;
	
	/** Atributo dsPagamento. */
	private String dsPagamento;
	
	/** Atributo nrDocumento. */
	private String nrDocumento;
	
	/** Atributo tipoDocumento. */
	private String tipoDocumento;
	
	/** Atributo cdSerieDocumento. */
	private String cdSerieDocumento;
	
	/** Atributo vlDocumento. */
	private BigDecimal vlDocumento;
	
	/** Atributo dtEmissaoDocumento. */
	private String dtEmissaoDocumento;
	
	/** Atributo dsUsoEmpresa. */
	private String dsUsoEmpresa;
	
	/** Atributo dsMensagemPrimeiraLinha. */
	private String dsMensagemPrimeiraLinha;
	
	/** Atributo dsMensagemSegundaLinha. */
	private String dsMensagemSegundaLinha;
	
	/** Atributo cdBancoCredito. */
	private String cdBancoCredito;
	
	/** Atributo agenciaDigitoCredito. */
	private String agenciaDigitoCredito;
	
	/** Atributo contaDigitoCredito. */
	private String contaDigitoCredito;
	
	/** Atributo tipoContaCredito. */
	private String tipoContaCredito;	
	
	/** Atributo cdTipoBeneficiario. */
	private Integer cdTipoBeneficiario;
	
	/** Atributo dsTipoBeneficiario. */
	private String dsTipoBeneficiario;
	
	/** Atributo vlDesconto. */
	private BigDecimal vlDesconto;
	
	/** Atributo vlAbatimento. */
	private BigDecimal vlAbatimento;
	
	/** Atributo vlMulta. */
	private BigDecimal vlMulta;
	
	/** Atributo vlMora. */
	private BigDecimal vlMora;
	
	/** Atributo vlDeducao. */
	private BigDecimal vlDeducao;
	
	/** Atributo vlAcrescimo. */
	private BigDecimal vlAcrescimo;
	
	/** Atributo vlImpostoRenda. */
	private BigDecimal vlImpostoRenda;
	
	/** Atributo vlIss. */
	private BigDecimal vlIss;
	
	/** Atributo vlIof. */
	private BigDecimal vlIof;
	
	/** Atributo vlInss. */
	private BigDecimal vlInss;
	
	/** Atributo numeroOp. */
	private String numeroOp;
	
	/** Atributo dataExpiracaoCredito. */
	private String dataExpiracaoCredito;
	
	/** Atributo instrucaoPagamento. */
	private String instrucaoPagamento;
	
	/** Atributo numeroCheque. */
	private String numeroCheque;
	
	/** Atributo serieCheque. */
	private String serieCheque;
	
	/** Atributo indicadorAssociadoUnicaAgencia. */
	private String indicadorAssociadoUnicaAgencia;
	
	/** Atributo identificadorTipoRetirada. */
	private String identificadorTipoRetirada;
	
	/** Atributo identificadorRetirada. */
	private String identificadorRetirada;
	
	/** Atributo dataRetirada. */
	private String dataRetirada;
	
	/** Atributo vlImpostoMovFinanceira. */
	private BigDecimal vlImpostoMovFinanceira;
	
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo numeroPagamentoCliente. */
	private String numeroPagamentoCliente;
	
	/** Atributo contaDebito. */
	private String contaDebito;
	
	/** Atributo numeroInscricaoFavorecido. */
	private String numeroInscricaoFavorecido;
	
	/** Atributo dsFavorecido. */
	private String dsFavorecido;
	
	/** Atributo tipoFavorecido. */
	private Integer tipoFavorecido;
	
	/** Atributo descTipoFavorecido. */
	private String descTipoFavorecido;
	
	/** Atributo numeroInscricaoBeneficiario. */
	private String numeroInscricaoBeneficiario;
	
	/** Atributo tipoBeneficiario. */
	private String tipoBeneficiario;
	
	/** Atributo nomeBeneficiario. */
	private String nomeBeneficiario;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;	
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo logoPath. */
	private String logoPath;
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo exibeBeneficiario. */
	private boolean exibeBeneficiario;
	
	/** Atributo exibeInscricaoFavorevido. */
	private boolean exibeInscricaoFavorevido;
	
	/** Atributo dtDevolucaoEstorno. */
	private String dtDevolucaoEstorno;
	
	/** Atributo listaFormaPagamento. */
	private List<ListarTipoRetiradaOcorrenciasSaidaDTO> listaFormaPagamento = null;
	
	/** Atributo detalhePagamento. */
	private DetalharPagtoOrdemPagtoSaidaDTO detalhePagamento = null;
	
	// M�TODOS
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		setDisableArgumentosConsulta(false);
		setExibeInscricaoFavorevido(false);
		limparArgumentos();
		limparPesquisa();
		carregaComboSituacao();
		carregaComboTipo();
	}
	
	/**
	 * Carrega combo situacao.
	 */
	public void carregaComboSituacao(){
		/*
		try{
			setCmbSituacaoPagamento(null);
			setListaCmbSituacaoPagamento(new ArrayList<SelectItem>());
			ConsultarSituacaoPagamentoEntradaDTO entrada = new ConsultarSituacaoPagamentoEntradaDTO();
			entrada.setCdSituacao(0);
			
			List<ConsultarSituacaoPagamentoSaidaDTO> list = getComboService().consultarSituacaoPagamento(entrada);
			
			for(ConsultarSituacaoPagamentoSaidaDTO combo : list){
				getListaCmbSituacaoPagamento().add(new SelectItem(combo.getCodigo(),combo.getDescricao()));
			}
			
		}catch(PdcAdapterFunctionalException p){
			setCmbSituacaoPagamento(null);
			setListaCmbSituacaoPagamento(new ArrayList<SelectItem>());
		}
		*/
		getListaCmbSituacaoPagamento().add(new SelectItem(1,"1 - " + MessageHelperUtils.getI18nMessage("label_agendado")));
		getListaCmbSituacaoPagamento().add(new SelectItem(2,"2 - " + MessageHelperUtils.getI18nMessage("label_pendente")));
		getListaCmbSituacaoPagamento().add(new SelectItem(4,"4 - " + MessageHelperUtils.getI18nMessage("label_estornado")));
		getListaCmbSituacaoPagamento().add(new SelectItem(5,"5 - " + MessageHelperUtils.getI18nMessage("label_excluido_cancelado")));
		getListaCmbSituacaoPagamento().add(new SelectItem(6,"6 - " + MessageHelperUtils.getI18nMessage("label_pago")));
		getListaCmbSituacaoPagamento().add(new SelectItem(7,"7 - " + MessageHelperUtils.getI18nMessage("label_nao_pago")));
		getListaCmbSituacaoPagamento().add(new SelectItem(8,"8 - " + MessageHelperUtils.getI18nMessage("label_antecipado")));
		getListaCmbSituacaoPagamento().add(new SelectItem(9,"9 - " + MessageHelperUtils.getI18nMessage("label_em_processamento")));
		getListaCmbSituacaoPagamento().add(new SelectItem(10,"10 - " + MessageHelperUtils.getI18nMessage("label_disponivel_op")));
		getListaCmbSituacaoPagamento().add(new SelectItem(11,"11 - " + MessageHelperUtils.getI18nMessage("label_expirado_op")));
	}
	
	/**
	 * Carrega combo tipo.
	 */
	public void carregaComboTipo(){
		listaCmbTipoFavorecido.add(new SelectItem(1,MessageHelperUtils.getI18nMessage("label_cpf")));
		listaCmbTipoFavorecido.add(new SelectItem(2,MessageHelperUtils.getI18nMessage("label_cnpj")));
		listaCmbTipoFavorecido.add(new SelectItem(8,MessageHelperUtils.getI18nMessage("label_isento_nao_informado")));
	}
	
	/**
	 * Limpar cpf cnpj.
	 */
	public void limparCpfCnpj(){
		setCnpjCorpo("");
		setCnpjFilial("");
		setCnpjControle("");
		setCpfCorpo("");
		setCpfControle("");		
	}
	
	/**
	 * Verifica isento nao informado.
	 */
	public void verificaIsentoNaoInformado(){
		
		if(getCmbTipoFavorecido() == 8){
			setInscricaoFavorecido("");
			setExibeInscricaoFavorevido(true);
		}else{
			setExibeInscricaoFavorevido(false);
		}
	}
	
	/**
	 * Limpar argumentos.
	 */
	public void limparArgumentos(){
		setAgencia("");
		setValorInicial(null);
		setValorFinal(null);
		setDataCreditoOpDe(new Date());
		setDataCreditoOpAte(new Date());
		setCnpjCorpo("");
		setCnpjFilial("");
		setCnpjControle("");
		setCpfCorpo("");
		setCpfControle("");		
		setRadioCpfCnpj(null);
		setListaGridOrdemPagtoAgePag(null);
		setItemSelecionado(null);
		setDisableArgumentosConsulta(false);
		setExibeInscricaoFavorevido(false);
		setCmbSituacaoPagamento(null);		
		setCmbFormaPagamento(null);
		setInscricaoFavorecido(null);
		setCmbTipoFavorecido(null);			
	}
	
	/**
	 * Limpar pesquisa.
	 */
	public void limparPesquisa(){
		setListaGridOrdemPagtoAgePag(null);
		setItemSelecionado(null);
		setDisableArgumentosConsulta(false);
		setExibeInscricaoFavorevido(false);
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		setItemSelecionado(null);
		
		return TELA_CONSULTA; 
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param e the e
	 * @return the string
	 */
	public String pesquisar(ActionEvent e){		
		consultar();
		return "";
	}
	
	/**
	 * Consultar.
	 */
	public void consultar(){	
		try {
			ConsultarOrdemPagtoAgePagEntradaDTO entradaDTO = new ConsultarOrdemPagtoAgePagEntradaDTO();
			entradaDTO.setCdAgencia((getAgencia() != null && !getAgencia().equals("") ? Integer.valueOf(getAgencia()) : Integer.parseInt("0")));
			
			String cnpj = "";
			String filial = "";
			String controle = "";
			
			if(getInscricaoFavorecido() != null && !getInscricaoFavorecido().equals("")){
				if(getCmbTipoFavorecido() == 1){
					String aux = SiteUtil.formatNumber((getInscricaoFavorecido()), 11);
					cnpj = aux.substring(0, 9);
					filial = "0";
					controle = aux.substring(9, 11);
				}else{
					if(getCmbTipoFavorecido() == 2){
						String aux = SiteUtil.formatNumber((getInscricaoFavorecido()), 15);
						cnpj = aux.substring(0, 9);
						filial = aux.substring(9, 13);
						controle = aux.substring(13, 15);
					}
				}
			}else{				
				cnpj = "";
				filial = "";
				controle = "";
			}
			
			entradaDTO.setCdCnpjCpf(!cnpj.equals("") && cnpj != null ? Long.parseLong(cnpj):0L);
			entradaDTO.setCdControleCnpjCpf(!controle.equals("") && controle != null ? Integer.parseInt(controle) : 0);
			entradaDTO.setCdFilialCnpjCpf(!filial.equals("") && filial != null ? Integer.parseInt(filial):0);				
			
			/*
			if(getRadioCpfCnpj() != null && !"".equals(getRadioCpfCnpj())){
				// SE FOR CNPJ
				if(getRadioCpfCnpj().equals("0")){
					entradaDTO.setCdCnpjCpf((getCnpjCorpo() != null && !getCnpjCorpo().equals("") ? Long.valueOf(getCnpjCorpo()) : new Long("0")));
					entradaDTO.setCdControleCnpjCpf((getCnpjControle() != null && !getCnpjControle().equals("") ? Integer.valueOf(getCnpjControle()) : Integer.parseInt("0")));
					entradaDTO.setCdFilialCnpjCpf((getCnpjFilial() != null && !getCnpjFilial().equals("") ? Integer.valueOf(getCnpjFilial()) : Integer.parseInt("0")));
				}
				// SE FOR CPF
				else if(getRadioCpfCnpj().equals("1")){
					entradaDTO.setCdCnpjCpf((getCpfCorpo() != null && !getCpfCorpo().equals("") ? Long.valueOf(getCpfCorpo()) : new Long("0")));
					entradaDTO.setCdControleCnpjCpf((getCpfControle() != null && !getCpfControle().equals("") ? Integer.valueOf(getCpfControle()) : Integer.parseInt("0")));				
				}
			}
			*/
			entradaDTO.setDataInicio(FormatarData.formataDiaMesAno(getDataCreditoOpDe()));
			entradaDTO.setDataFim(FormatarData.formataDiaMesAno(getDataCreditoOpAte()));
			
			entradaDTO.setVlPagamentoClienteInicio(getValorInicial());
			entradaDTO.setVlPagamentoClienteFim(getValorFinal());
			
			entradaDTO.setCdSituacaoPagamento(getCmbSituacaoPagamento());
			entradaDTO.setCdTipoRetornoPagamento(getCmbFormaPagamento());
			entradaDTO.setCdTipoInscricao(getCmbTipoFavorecido());
		
			setListaGridOrdemPagtoAgePag(consultarOrdemPagtoAgePagImpl.consultarOrdemPagtoAgePag(entradaDTO));
			
		    setListaControleOrdemPagtoAgePag(new ArrayList<SelectItem>());
		    for(int i=0;i<getListaGridOrdemPagtoAgePag().size();i++){
		    	getListaControleOrdemPagtoAgePag().add(new SelectItem(i,""));
		    }

		    setDisableArgumentosConsulta(true);
		    setExibeInscricaoFavorevido(true);
		    setItemSelecionado(null);
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridOrdemPagtoAgePag(null);
			setItemSelecionado(null);
		}
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try{
			ConsultarOrdemPagtoAgePagSaidaDTO registroSelecionado = getListaGridOrdemPagtoAgePag().get(getItemSelecionado());	
			DetalharPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharPagtoOrdemPagtoEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
			entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrSequenciaContratoNegocio());
			entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
			entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
			entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
			entradaDTO.setCdContaDebito(registroSelecionado.getContaDebito());
			entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
			entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
			entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
			
			//conforme email enviado por Kelli Cristina (30/08/2011)  valorizar o campo cdAgendadosPagoNaoPago com o cdAgenciaVencimento da sa�da do pdc pgit.ageEfeEst.conOrdPagtoAgePag.consultarOrdemPagtoPorAgePagadora
			entradaDTO.setCdAgendadosPagoNaoPago(registroSelecionado.getCdAgenciaVencimento());
			
			//conforme email enviado por Kelli Cristina (30/08/2011)valorizar o campo dsEfetivacao com o dsEfetivacaoPagamento da sa�da do pdc pgit.ageEfeEst.conOrdPagtoAgePag.consultarOrdemPagtoPorAgePagadora 
			entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
			
			/* E-mail da Beatriz do dia 25/11/2010 indicando para passar fixo por ser modalidade ordem de pagamento */
			entradaDTO.setCdProdutoServicoRelacionado(200033);
			entradaDTO.setDtHoraManutencao("");
			
			detalhePagamento = getConsultarPagamentosIndividualImpl().detalharPagtoOrdemPagto(entradaDTO);
			
			//Cliente			
			setNrCnpjCpf(detalhePagamento.getCpfCnpjClienteFormatado());
			setDsRazaoSocial(detalhePagamento.getNomeCliente());
			setDsEmpresa(detalhePagamento.getDsPessoaJuridicaContrato());
            setNroContrato(detalhePagamento.getNrSequenciaContratoNegocio() != null
                && !detalhePagamento.getNrSequenciaContratoNegocio().equals("") ? Long.parseLong(detalhePagamento
                .getNrSequenciaContratoNegocio()) : 0L);
			setDsContrato(detalhePagamento.getDsContrato());
			setCdSituacaoContrato(detalhePagamento.getCdSituacaoContrato());
			
			// CONTA D�BITO
			setDsBancoDebito(detalhePagamento.getBancoDebitoFormatado());
			setDsAgenciaDebito(detalhePagamento.getAgenciaDebitoFormatada());
			setContaDebito(detalhePagamento.getContaDebitoFormatada());
			setTipoContaDebito(detalhePagamento.getDsTipoContaDebito());
		
			limparFavorecidoBeneficiario();
			exibeBeneficiario = false;

            if (detalhePagamento.getCdIndicadorModalidadePgit() == 1
                || detalhePagamento.getCdIndicadorModalidadePgit() == 3) {
                setNumeroInscricaoFavorecido(detalhePagamento.getNumeroInscricaoFavorecidoFormatado());
                setTipoFavorecido(detalhePagamento.getCdTipoInscricaoFavorecido());
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
                setDsFavorecido(detalhePagamento.getDsNomeFavorecido());
                setCdFavorecido(detalhePagamento.getCdFavorecido() == null
                    || detalhePagamento.getCdFavorecido().equals("") ? null : Long.parseLong(detalhePagamento
                    .getCdFavorecido()));
                setDsBancoFavorecido(detalhePagamento.getBancoCreditoFormatado());
                setDsAgenciaFavorecido(detalhePagamento.getAgenciaCreditoFormatada());
                setDsContaFavorecido(detalhePagamento.getContaCreditoFormatada());
                setDsTipoContaFavorecido(detalhePagamento.getDsTipoContaFavorecido());
            } else {
				setNumeroInscricaoBeneficiario(detalhePagamento.getNumeroInscricaoBeneficiarioFormatado());
				setDsTipoBeneficiario(detalhePagamento.getCdTipoInscriBeneficio());
				setNomeBeneficiario(detalhePagamento.getCdNomeBeneficio());
				
				exibeBeneficiario = !PgitUtil.verificaStringNula(
					getNumeroInscricaoBeneficiario()).equals("")
					|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
							.equals(""))
					|| !PgitUtil.verificaStringNula(getNomeBeneficiario())
							.equals("");
			}
			
			//Pagamento
			setDsTipoServico(detalhePagamento.getDsTipoServicoOperacao()); 
			setDsIndicadorModalidade(detalhePagamento.getDsModalidadeRelacionado()); 
			setNrSequenciaArquivoRemessa(detalhePagamento.getNrSequenciaArquivoRemessa());
			setNrLoteArquivoRemessa(detalhePagamento.getNrLoteArquivoRemessa());
			setNumeroPagamento(detalhePagamento.getCdControlePagamento());
			setCdListaDebito(detalhePagamento.getCdListaDebito());
			setDtAgendamento(detalhePagamento.getDtAgendamento());
			setDtPagamento(detalhePagamento.getDtPagamento());
			setDtVencimento(detalhePagamento.getDtVencimento());
			setCdIndicadorEconomicoMoeda(detalhePagamento.getDsMoeda());
			setQtMoeda(detalhePagamento.getQtMoeda());
			setVlrAgendamento(detalhePagamento.getVlAgendado());
			setVlrEfetivacao(detalhePagamento.getVlEfetivo());
			setCdSituacaoOperacaoPagamento(detalhePagamento.getCdSituacaoPagamento());
			setDsMotivoSituacao(detalhePagamento.getDsMotivoSituacao());
			setDsPagamento(detalhePagamento.getDsPagamento());
			setNrDocumento(detalhePagamento.getNrDocumento());
			setTipoDocumento(detalhePagamento.getDsTipoDocumento());
			setCdSerieDocumento(detalhePagamento.getCdSerieDocumento());
			setVlDocumento(detalhePagamento.getVlDocumento());
			setDtEmissaoDocumento(detalhePagamento.getDtEmissaoDocumento());
			setDsUsoEmpresa(detalhePagamento.getDsUsoEmpresa());
			setDsMensagemPrimeiraLinha(detalhePagamento.getDsMensagemPrimeiraLinha());
			setDsMensagemSegundaLinha(detalhePagamento.getDsMensagemSegundaLinha());

            setCdBancoCredito(detalhePagamento.getBancoCreditoFormatado());
            setAgenciaDigitoCredito(detalhePagamento.getAgenciaCreditoFormatada());
            setTipoContaCredito(detalhePagamento.getDsTipoContaFavorecido());
            setContaDigitoCredito(detalhePagamento.getContaCreditoFormatada());
			
			setNumeroOp(detalhePagamento.getNrOperacao());
			setDataExpiracaoCredito(detalhePagamento.getDtExpedicaoCredito());
			setInstrucaoPagamento(detalhePagamento.getCdInsPagamento());
			setNumeroCheque(detalhePagamento.getNrCheque());
			setSerieCheque(detalhePagamento.getNrSerieCheque());
			setIndicadorAssociadoUnicaAgencia(detalhePagamento.getDsUnidadeAgencia());
			setIdentificadorTipoRetirada(detalhePagamento.getDsIdentificacaoTipoRetirada());
			setIdentificadorRetirada(detalhePagamento.getDsIdentificacaoRetirada());
			setDataRetirada(detalhePagamento.getDtRetirada());
			setVlImpostoMovFinanceira(detalhePagamento.getVlImpostoMovimentacaoFinanceira());
			setVlDesconto(detalhePagamento.getVlDescontoCredito());
			setVlAbatimento(detalhePagamento.getVlAbatidoCredito());
			setVlMulta(detalhePagamento.getVlMultaCredito());
			setVlMora(detalhePagamento.getVlMoraJuros());
			setVlDeducao(detalhePagamento.getVlOutrasDeducoes());
			setVlAcrescimo(detalhePagamento.getVlOutroAcrescimo());
			setVlImpostoRenda(detalhePagamento.getVlIrCredito());
			setVlIss(detalhePagamento.getVlIssCredito());
			setVlIof(detalhePagamento.getVlIofCredito());
			setVlInss(detalhePagamento.getVlInssCredito());
			setDtDevolucaoEstorno(detalhePagamento.getDtDevolucaoEstorno());
			
			//trilhaAuditoria
			setDataHoraInclusao(detalhePagamento.getDataHoraInclusaoFormatada()); 
			setUsuarioInclusao(detalhePagamento.getUsuarioInclusaoFormatado());      
			setTipoCanalInclusao(detalhePagamento.getTipoCanalInclusaoFormatado()); 
			setComplementoInclusao(detalhePagamento.getComplementoInclusaoFormatado()); 
			setDataHoraManutencao(detalhePagamento.getDataHoraManutencaoFormatada()); 
			setUsuarioManutencao(detalhePagamento.getUsuarioManutencaoFormatado()); 
			setTipoCanalManutencao(detalhePagamento.getTipoCanalManutencaoFormatado()); 
			setComplementoManutencao(detalhePagamento.getComplementoManutencaoFormatado()); 
			
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}	
		return TELA_DETALHE;
	}
	
	/**
	 * Limpar favorecido beneficiario.
	 */
	public void limparFavorecidoBeneficiario(){
		setNumeroInscricaoFavorecido("");	
		setTipoFavorecido(0);
		setDsFavorecido("");
		setCdFavorecido(0L);
		setDsBancoFavorecido("");
		setDsAgenciaFavorecido("");
		setDsContaFavorecido("");
		setDsTipoContaFavorecido("");
		setNumeroInscricaoBeneficiario("");
		setCdTipoBeneficiario(0);
		setNomeBeneficiario("");
		setDtNascimentoBeneficiario("");
	}	
	
	/**
	 * Imprimir relatorio.
	 */
	public void imprimirRelatorio(){
		try {		
			String caminho ="/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance(); 
			ServletContext servletContext = (ServletContext)facesContext.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);
			
	//		Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par.put("titulo", "Consultar Ordens de Pagamento");
			
			par.put("cpfCnpj", getNrCnpjCpf());
			par.put("nomeRazaoSocial", getDsRazaoSocial());
			par.put("empresa", getDsEmpresa());
			
			par.put("descContrato", getDsContrato());
			par.put("situacao", getCdSituacaoContrato());
			par.put("bancoDebito", getDsBancoDebito());
			par.put("agenciaDebito", getDsAgenciaDebito());
			par.put("contaDebito", getContaDebito());
			par.put("tipoContaDebito", getTipoContaDebito());
			par.put("logoBradesco", getLogoPath());
			try {
				
				// M�todo que gera o relat�rio PDF.
				PgitUtil.geraRelatorioPdf("/relatorios/agendamentoEfetivacaoEstorno/manterOrdemPagtoAgPag", "imprimirOrdensPagtosAgPag", getListaGridOrdemPagtoAgePag(), par);
								
			} /* Exce��o do relat�rio */
				catch (Exception e){
					throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
			}
		
		} /* Exce��o do PDC */
			catch (PdcAdapterFunctionalException p) { 
			  BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}	  

	
	}

	/**
     * Is botao detalhar retirada visivel.
     * 
     * @return true, if is botao detalhar retirada visivel
     */
	public boolean isBotaoDetalharRetiradaVisivel() {
        return detalhePagamento != null && detalhePagamento.getCdIdentificacaoTipoRetirada() != null
            && detalhePagamento.getCdIdentificacaoTipoRetirada().equals(CODIGO_IDENTIFICACAO_DIVERSOS); 
	}

	/**
     * Detalhar retirada.
     * 
     * @return the string
     */
	public String detalharRetirada() {
        carregarListaFormaPagamento();

	    return TELA_DETALHE_RETIRADA;
	}

	/**
     * Carregar lista forma pagamento.
     */
	private void carregarListaFormaPagamento() {
        ConsultarOrdemPagtoAgePagSaidaDTO registroSelecionado =
            getListaGridOrdemPagtoAgePag().get(getItemSelecionado());
	    
	    ListarTipoRetiradaEntradaDTO entrada = new ListarTipoRetiradaEntradaDTO();
	    entrada.setNrSequenciaContratoNegocio(registroSelecionado.getNrSequenciaContratoNegocio());
        entrada.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
        entrada.setCdProdutoServicoOperacao(registroSelecionado.getCdServido());
        entrada.setCdProdutoOperacaoRelacionado(registroSelecionado.getCdModalidade());
        entrada.setCdBancoPagador(detalhePagamento.getCdBancoDebito());
        entrada.setCdAgenciaBancariaPagador(detalhePagamento.getCdAgenciaDebito());
        entrada.setCdDigitoAgenciaPagador(detalhePagamento.getCdDigAgenciaDebto());
        entrada.setCdContaBancariaPagador(detalhePagamento.getCdContaDebito());
        entrada.setCdDigitoContaPagador(detalhePagamento.getDsDigAgenciaDebito());
        entrada.setCdBancoFavorecido(detalhePagamento.getCdBancoCredito());
        entrada.setCdAgenciaFavorecido(detalhePagamento.getCdAgenciaCredito());
        entrada.setCdDigitoAgenciaFavorecido(detalhePagamento.getCdDigAgenciaCredito());
        entrada.setCdContaFavorecido(detalhePagamento.getCdContaCredito());
        entrada.setCdDigitoContaFavorecido(detalhePagamento.getCdDigContaCredito());
        entrada.setDtPagamentoDe(detalhePagamento.getDtAgendamento());
        entrada.setDtPagamentoAte(detalhePagamento.getDtVencimento());

        ListarTipoRetiradaSaidaDTO saidaDetalheRetirada = consultasServiceImpl.listarTipoRetirada(entrada);
        listaFormaPagamento = saidaDetalheRetirada.getOcorrencias();
	}

	/**
     * Paginar lista forma pagamento.
     * 
     * @param actionEvent
     *            the action event
     */
	public void paginarListaFormaPagamento(ActionEvent actionEvent) {
	    carregarListaFormaPagamento();
	}

	/**
     * Voltar detalhe retirada.
     * 
     * @return the string
     */
	public String voltarDetalheRetirada() {
	    return TELA_DETALHE;
	}

	// GETTERS E SETTERS
	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * Set: agencia.
	 *
	 * @param agencia the agencia
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * Get: logoPath.
	 *
	 * @return logoPath
	 */
	public String getLogoPath() {
		return logoPath;
	}

	/**
	 * Set: logoPath.
	 *
	 * @param logoPath the logo path
	 */
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	/**
	 * Get: dataCreditoOpDe.
	 *
	 * @return dataCreditoOpDe
	 */
	public Date getDataCreditoOpDe() {
		if(dataCreditoOpDe == null){
			dataCreditoOpDe = new Date();
		}
		return dataCreditoOpDe;
	}

	/**
	 * Set: dataCreditoOpDe.
	 *
	 * @param dataCreditoOpDe the data credito op de
	 */
	public void setDataCreditoOpDe(Date dataCreditoOpDe) {
		this.dataCreditoOpDe = dataCreditoOpDe;
	}
	
	/**
	 * Get: dataCreditoOpAte.
	 *
	 * @return dataCreditoOpAte
	 */
	public Date getDataCreditoOpAte() {
		if(dataCreditoOpAte == null){
			dataCreditoOpAte = new Date();
		}
		return dataCreditoOpAte;
	}

	/**
	 * Set: dataCreditoOpAte.
	 *
	 * @param dataCreditoOpAte the data credito op ate
	 */
	public void setDataCreditoOpAte(Date dataCreditoOpAte) {
		this.dataCreditoOpAte = dataCreditoOpAte;
	}

	/**
	 * Get: valorFinal.
	 *
	 * @return valorFinal
	 */
	public BigDecimal getValorFinal() {
		return valorFinal;
	}

	/**
	 * Set: valorFinal.
	 *
	 * @param valorFinal the valor final
	 */
	public void setValorFinal(BigDecimal valorFinal) {
		this.valorFinal = valorFinal;
	}

	/**
	 * Get: valorInicial.
	 *
	 * @return valorInicial
	 */
	public BigDecimal getValorInicial() {
		return valorInicial;
	}

	/**
	 * Set: valorInicial.
	 *
	 * @param valorInicial the valor inicial
	 */
	public void setValorInicial(BigDecimal valorInicial) {
		this.valorInicial = valorInicial;
	}

	/**
	 * Get: cnpjControle.
	 *
	 * @return cnpjControle
	 */
	public String getCnpjControle() {
		return cnpjControle;
	}

	/**
	 * Set: cnpjControle.
	 *
	 * @param cnpjControle the cnpj controle
	 */
	public void setCnpjControle(String cnpjControle) {
		this.cnpjControle = cnpjControle;
	}

	/**
	 * Get: cnpjCorpo.
	 *
	 * @return cnpjCorpo
	 */
	public String getCnpjCorpo() {
		return cnpjCorpo;
	}

	/**
	 * Set: cnpjCorpo.
	 *
	 * @param cnpjCorpo the cnpj corpo
	 */
	public void setCnpjCorpo(String cnpjCorpo) {
		this.cnpjCorpo = cnpjCorpo;
	}

	/**
	 * Get: cnpjFilial.
	 *
	 * @return cnpjFilial
	 */
	public String getCnpjFilial() {
		return cnpjFilial;
	}

	/**
	 * Set: cnpjFilial.
	 *
	 * @param cnpjFilial the cnpj filial
	 */
	public void setCnpjFilial(String cnpjFilial) {
		this.cnpjFilial = cnpjFilial;
	}

	/**
	 * Get: cpfControle.
	 *
	 * @return cpfControle
	 */
	public String getCpfControle() {
		return cpfControle;
	}

	/**
	 * Set: cpfControle.
	 *
	 * @param cpfControle the cpf controle
	 */
	public void setCpfControle(String cpfControle) {
		this.cpfControle = cpfControle;
	}

	/**
	 * Get: cpfCorpo.
	 *
	 * @return cpfCorpo
	 */
	public String getCpfCorpo() {
		return cpfCorpo;
	}

	/**
	 * Set: cpfCorpo.
	 *
	 * @param cpfCorpo the cpf corpo
	 */
	public void setCpfCorpo(String cpfCorpo) {
		this.cpfCorpo = cpfCorpo;
	}

	/**
	 * Get: radioCpfCnpj.
	 *
	 * @return radioCpfCnpj
	 */
	public String getRadioCpfCnpj() {
		return radioCpfCnpj;
	}

	/**
	 * Set: radioCpfCnpj.
	 *
	 * @param radioCpfCnpj the radio cpf cnpj
	 */
	public void setRadioCpfCnpj(String radioCpfCnpj) {
		this.radioCpfCnpj = radioCpfCnpj;
	}

	/**
	 * Get: consultarOrdemPagtoAgePagImpl.
	 *
	 * @return consultarOrdemPagtoAgePagImpl
	 */
	public IConsultarOrdemPagtoAgePagService getConsultarOrdemPagtoAgePagImpl() {
		return consultarOrdemPagtoAgePagImpl;
	}

	/**
	 * Set: consultarOrdemPagtoAgePagImpl.
	 *
	 * @param consultarOrdemPagtoAgePagImpl the consultar ordem pagto age pag impl
	 */
	public void setConsultarOrdemPagtoAgePagImpl(IConsultarOrdemPagtoAgePagService consultarOrdemPagtoAgePagImpl) {
		this.consultarOrdemPagtoAgePagImpl = consultarOrdemPagtoAgePagImpl;
	}

	/**
	 * Get: consultarPagamentosIndividualImpl.
	 *
	 * @return consultarPagamentosIndividualImpl
	 */
	public IConsultarPagamentosIndividualService getConsultarPagamentosIndividualImpl() {
		return consultarPagamentosIndividualImpl;
	}

	/**
	 * Set: consultarPagamentosIndividualImpl.
	 *
	 * @param consultarPagamentosIndividualImpl the consultar pagamentos individual impl
	 */
	public void setConsultarPagamentosIndividualImpl(
			IConsultarPagamentosIndividualService consultarPagamentosIndividualImpl) {
		this.consultarPagamentosIndividualImpl = consultarPagamentosIndividualImpl;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public Integer getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(Integer itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: listaControleOrdemPagtoAgePag.
	 *
	 * @return listaControleOrdemPagtoAgePag
	 */
	public List<SelectItem> getListaControleOrdemPagtoAgePag() {
		return listaControleOrdemPagtoAgePag;
	}

	/**
	 * Set: listaControleOrdemPagtoAgePag.
	 *
	 * @param listaControleOrdemPagtoAgePag the lista controle ordem pagto age pag
	 */
	public void setListaControleOrdemPagtoAgePag(List<SelectItem> listaControleOrdemPagtoAgePag) {
		this.listaControleOrdemPagtoAgePag = listaControleOrdemPagtoAgePag;
	}

	/**
	 * Get: listaGridOrdemPagtoAgePag.
	 *
	 * @return listaGridOrdemPagtoAgePag
	 */
	public List<ConsultarOrdemPagtoAgePagSaidaDTO> getListaGridOrdemPagtoAgePag() {
		return listaGridOrdemPagtoAgePag;
	}

	/**
	 * Set: listaGridOrdemPagtoAgePag.
	 *
	 * @param listaGridOrdemPagtoAgePag the lista grid ordem pagto age pag
	 */
	public void setListaGridOrdemPagtoAgePag(List<ConsultarOrdemPagtoAgePagSaidaDTO> listaGridOrdemPagtoAgePag) {
		this.listaGridOrdemPagtoAgePag = listaGridOrdemPagtoAgePag;
	}

	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public Long getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public String getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}

	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(String cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}

	/**
	 * Get: cdListaDebito.
	 *
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}

	/**
	 * Set: cdListaDebito.
	 *
	 * @param cdListaDebito the cd lista debito
	 */
	public void setCdListaDebito(String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}

	/**
	 * Get: cdSerieDocumento.
	 *
	 * @return cdSerieDocumento
	 */
	public String getCdSerieDocumento() {
		return cdSerieDocumento;
	}

	/**
	 * Set: cdSerieDocumento.
	 *
	 * @param cdSerieDocumento the cd serie documento
	 */
	public void setCdSerieDocumento(String cdSerieDocumento) {
		this.cdSerieDocumento = cdSerieDocumento;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public String getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(String cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: cdTipoContaFavorecido.
	 *
	 * @return cdTipoContaFavorecido
	 */
	public Integer getCdTipoContaFavorecido() {
		return cdTipoContaFavorecido;
	}
	
	/**
	 * Set: cdTipoContaFavorecido.
	 *
	 * @param cdTipoContaFavorecido the cd tipo conta favorecido
	 */
	public void setCdTipoContaFavorecido(Integer cdTipoContaFavorecido) {
		this.cdTipoContaFavorecido = cdTipoContaFavorecido;
	}

	/**
	 * Get: contaDebito.
	 *
	 * @return contaDebito
	 */
	public String getContaDebito() {
		return contaDebito;
	}

	/**
	 * Set: contaDebito.
	 *
	 * @param contaDebito the conta debito
	 */
	public void setContaDebito(String contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * Get: dataExpiracaoCredito.
	 *
	 * @return dataExpiracaoCredito
	 */
	public String getDataExpiracaoCredito() {
		return dataExpiracaoCredito;
	}

	/**
	 * Set: dataExpiracaoCredito.
	 *
	 * @param dataExpiracaoCredito the data expiracao credito
	 */
	public void setDataExpiracaoCredito(String dataExpiracaoCredito) {
		this.dataExpiracaoCredito = dataExpiracaoCredito;
	}

	/**
	 * Get: dataRetirada.
	 *
	 * @return dataRetirada
	 */
	public String getDataRetirada() {
		return dataRetirada;
	}

	/**
	 * Set: dataRetirada.
	 *
	 * @param dataRetirada the data retirada
	 */
	public void setDataRetirada(String dataRetirada) {
		this.dataRetirada = dataRetirada;
	}

	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsAgenciaFavorecido.
	 *
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}

	/**
	 * Set: dsAgenciaFavorecido.
	 *
	 * @param dsAgenciaFavorecido the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}

	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: dsBancoFavorecido.
	 *
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}

	/**
	 * Set: dsBancoFavorecido.
	 *
	 * @param dsBancoFavorecido the ds banco favorecido
	 */
	public void setDsBancoFavorecido(String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}
	
	/**
	 * Get: dsContaFavorecido.
	 *
	 * @return dsContaFavorecido
	 */
	public String getDsContaFavorecido() {
		return dsContaFavorecido;
	}

	/**
	 * Set: dsContaFavorecido.
	 *
	 * @param dsContaFavorecido the ds conta favorecido
	 */
	public void setDsContaFavorecido(String dsContaFavorecido) {
		this.dsContaFavorecido = dsContaFavorecido;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}

	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}
	
	/**
	 * Get: tipoFavorecido.
	 *
	 * @return tipoFavorecido
	 */
	public Integer getTipoFavorecido() {
		return tipoFavorecido;
	}

	/**
	 * Set: tipoFavorecido.
	 *
	 * @param tipoFavorecido the tipo favorecido
	 */
	public void setTipoFavorecido(Integer tipoFavorecido) {
		this.tipoFavorecido = tipoFavorecido;
	}
	
	/**
	 * Get: numeroInscricaoBeneficiario.
	 *
	 * @return numeroInscricaoBeneficiario
	 */
	public String getNumeroInscricaoBeneficiario() {
		return numeroInscricaoBeneficiario;
	}

	/**
	 * Set: numeroInscricaoBeneficiario.
	 *
	 * @param numeroInscricaoBeneficiario the numero inscricao beneficiario
	 */
	public void setNumeroInscricaoBeneficiario(String numeroInscricaoBeneficiario) {
		this.numeroInscricaoBeneficiario = numeroInscricaoBeneficiario;
	}
	
	/**
	 * Get: nomeBeneficiario.
	 *
	 * @return nomeBeneficiario
	 */
	public String getNomeBeneficiario() {
		return nomeBeneficiario;
	}

	/**
	 * Set: nomeBeneficiario.
	 *
	 * @param nomeBeneficiario the nome beneficiario
	 */
	public void setNomeBeneficiario(String nomeBeneficiario) {
		this.nomeBeneficiario = nomeBeneficiario;
	}

	/**
	 * Get: tipoBeneficiario.
	 *
	 * @return tipoBeneficiario
	 */
	public String getTipoBeneficiario() {
		return tipoBeneficiario;
	}

	/**
	 * Set: tipoBeneficiario.
	 *
	 * @param tipoBeneficiario the tipo beneficiario
	 */
	public void setTipoBeneficiario(String tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}

	/**
	 * Get: dsIndicadorModalidade.
	 *
	 * @return dsIndicadorModalidade
	 */
	public String getDsIndicadorModalidade() {
		return dsIndicadorModalidade;
	}

	/**
	 * Set: dsIndicadorModalidade.
	 *
	 * @param dsIndicadorModalidade the ds indicador modalidade
	 */
	public void setDsIndicadorModalidade(String dsIndicadorModalidade) {
		this.dsIndicadorModalidade = dsIndicadorModalidade;
	}

	/**
	 * Get: dsMensagemPrimeiraLinha.
	 *
	 * @return dsMensagemPrimeiraLinha
	 */
	public String getDsMensagemPrimeiraLinha() {
		return dsMensagemPrimeiraLinha;
	}

	/**
	 * Set: dsMensagemPrimeiraLinha.
	 *
	 * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
	 */
	public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
		this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
	}

	/**
	 * Get: dsMensagemSegundaLinha.
	 *
	 * @return dsMensagemSegundaLinha
	 */
	public String getDsMensagemSegundaLinha() {
		return dsMensagemSegundaLinha;
	}

	/**
	 * Set: dsMensagemSegundaLinha.
	 *
	 * @param dsMensagemSegundaLinha the ds mensagem segunda linha
	 */
	public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
		this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
	}
	
	/**
	 * Get: agenciaDigitoCredito.
	 *
	 * @return agenciaDigitoCredito
	 */
	public String getAgenciaDigitoCredito() {
		return agenciaDigitoCredito;
	}

	/**
	 * Set: agenciaDigitoCredito.
	 *
	 * @param agenciaDigitoCredito the agencia digito credito
	 */
	public void setAgenciaDigitoCredito(String agenciaDigitoCredito) {
		this.agenciaDigitoCredito = agenciaDigitoCredito;
	}

	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public String getCdBancoCredito() {
		return cdBancoCredito;
	}

	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(String cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}

	/**
	 * Get: contaDigitoCredito.
	 *
	 * @return contaDigitoCredito
	 */
	public String getContaDigitoCredito() {
		return contaDigitoCredito;
	}

	/**
	 * Set: contaDigitoCredito.
	 *
	 * @param contaDigitoCredito the conta digito credito
	 */
	public void setContaDigitoCredito(String contaDigitoCredito) {
		this.contaDigitoCredito = contaDigitoCredito;
	}

	/**
	 * Get: tipoContaCredito.
	 *
	 * @return tipoContaCredito
	 */
	public String getTipoContaCredito() {
		return tipoContaCredito;
	}

	/**
	 * Set: tipoContaCredito.
	 *
	 * @param tipoContaCredito the tipo conta credito
	 */
	public void setTipoContaCredito(String tipoContaCredito) {
		this.tipoContaCredito = tipoContaCredito;
	}

	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	/**
	 * Get: dsPagamento.
	 *
	 * @return dsPagamento
	 */
	public String getDsPagamento() {
		return dsPagamento;
	}

	/**
	 * Set: dsPagamento.
	 *
	 * @param dsPagamento the ds pagamento
	 */
	public void setDsPagamento(String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: dsTipoContaFavorecido.
	 *
	 * @return dsTipoContaFavorecido
	 */
	public String getDsTipoContaFavorecido() {
		return dsTipoContaFavorecido;
	}

	/**
	 * Set: dsTipoContaFavorecido.
	 *
	 * @param dsTipoContaFavorecido the ds tipo conta favorecido
	 */
	public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
		this.dsTipoContaFavorecido = dsTipoContaFavorecido;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: dsUsoEmpresa.
	 *
	 * @return dsUsoEmpresa
	 */
	public String getDsUsoEmpresa() {
		return dsUsoEmpresa;
	}

	/**
	 * Set: dsUsoEmpresa.
	 *
	 * @param dsUsoEmpresa the ds uso empresa
	 */
	public void setDsUsoEmpresa(String dsUsoEmpresa) {
		this.dsUsoEmpresa = dsUsoEmpresa;
	}

	/**
	 * Get: dtAgendamento.
	 *
	 * @return dtAgendamento
	 */
	public String getDtAgendamento() {
		return dtAgendamento;
	}

	/**
	 * Set: dtAgendamento.
	 *
	 * @param dtAgendamento the dt agendamento
	 */
	public void setDtAgendamento(String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	/**
	 * Get: dtEmissaoDocumento.
	 *
	 * @return dtEmissaoDocumento
	 */
	public String getDtEmissaoDocumento() {
		return dtEmissaoDocumento;
	}

	/**
	 * Set: dtEmissaoDocumento.
	 *
	 * @param dtEmissaoDocumento the dt emissao documento
	 */
	public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
		this.dtEmissaoDocumento = dtEmissaoDocumento;
	}

	/**
	 * Get: dtNascimentoBeneficiario.
	 *
	 * @return dtNascimentoBeneficiario
	 */
	public String getDtNascimentoBeneficiario() {
		return dtNascimentoBeneficiario;
	}

	/**
	 * Set: dtNascimentoBeneficiario.
	 *
	 * @param dtNascimentoBeneficiario the dt nascimento beneficiario
	 */
	public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
		this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
	}

	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	/**
	 * Get: dtVencimento.
	 *
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 *
	 * @param dtVencimento the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: identificadorRetirada.
	 *
	 * @return identificadorRetirada
	 */
	public String getIdentificadorRetirada() {
		return identificadorRetirada;
	}

	/**
	 * Set: identificadorRetirada.
	 *
	 * @param identificadorRetirada the identificador retirada
	 */
	public void setIdentificadorRetirada(String identificadorRetirada) {
		this.identificadorRetirada = identificadorRetirada;
	}

	/**
	 * Get: identificadorTipoRetirada.
	 *
	 * @return identificadorTipoRetirada
	 */
	public String getIdentificadorTipoRetirada() {
		return identificadorTipoRetirada;
	}

	/**
	 * Set: identificadorTipoRetirada.
	 *
	 * @param identificadorTipoRetirada the identificador tipo retirada
	 */
	public void setIdentificadorTipoRetirada(String identificadorTipoRetirada) {
		this.identificadorTipoRetirada = identificadorTipoRetirada;
	}

	/**
	 * Get: indicadorAssociadoUnicaAgencia.
	 *
	 * @return indicadorAssociadoUnicaAgencia
	 */
	public String getIndicadorAssociadoUnicaAgencia() {
		return indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Set: indicadorAssociadoUnicaAgencia.
	 *
	 * @param indicadorAssociadoUnicaAgencia the indicador associado unica agencia
	 */
	public void setIndicadorAssociadoUnicaAgencia(String indicadorAssociadoUnicaAgencia) {
		this.indicadorAssociadoUnicaAgencia = indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Get: instrucaoPagamento.
	 *
	 * @return instrucaoPagamento
	 */
	public String getInstrucaoPagamento() {
		return instrucaoPagamento;
	}

	/**
	 * Set: instrucaoPagamento.
	 *
	 * @param instrucaoPagamento the instrucao pagamento
	 */
	public void setInstrucaoPagamento(String instrucaoPagamento) {
		this.instrucaoPagamento = instrucaoPagamento;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nrDocumento.
	 *
	 * @return nrDocumento
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}

	/**
	 * Set: nrDocumento.
	 *
	 * @param nrDocumento the nr documento
	 */
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	/**
	 * Get: nrLoteArquivoRemessa.
	 *
	 * @return nrLoteArquivoRemessa
	 */
	public String getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}

	/**
	 * Set: nrLoteArquivoRemessa.
	 *
	 * @param nrLoteArquivoRemessa the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}

	/**
	 * Get: nrSequenciaArquivoRemessa.
	 *
	 * @return nrSequenciaArquivoRemessa
	 */
	public String getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}

	/**
	 * Set: nrSequenciaArquivoRemessa.
	 *
	 * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}

	/**
	 * Get: numeroCheque.
	 *
	 * @return numeroCheque
	 */
	public String getNumeroCheque() {
		return numeroCheque;
	}

	/**
	 * Set: numeroCheque.
	 *
	 * @param numeroCheque the numero cheque
	 */
	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}

	/**
	 * Get: numeroInscricaoFavorecido.
	 *
	 * @return numeroInscricaoFavorecido
	 */
	public String getNumeroInscricaoFavorecido() {
		return numeroInscricaoFavorecido;
	}

	/**
	 * Set: numeroInscricaoFavorecido.
	 *
	 * @param numeroInscricaoFavorecido the numero inscricao favorecido
	 */
	public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
		this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
	}

	/**
	 * Get: numeroOp.
	 *
	 * @return numeroOp
	 */
	public String getNumeroOp() {
		return numeroOp;
	}

	/**
	 * Set: numeroOp.
	 *
	 * @param numeroOp the numero op
	 */
	public void setNumeroOp(String numeroOp) {
		this.numeroOp = numeroOp;
	}

	/**
	 * Get: numeroPagamento.
	 *
	 * @return numeroPagamento
	 */
	public String getNumeroPagamento() {
		return numeroPagamento;
	}

	/**
	 * Set: numeroPagamento.
	 *
	 * @param numeroPagamento the numero pagamento
	 */
	public void setNumeroPagamento(String numeroPagamento) {
		this.numeroPagamento = numeroPagamento;
	}

	/**
	 * Get: numeroPagamentoCliente.
	 *
	 * @return numeroPagamentoCliente
	 */
	public String getNumeroPagamentoCliente() {
		return numeroPagamentoCliente;
	}

	/**
	 * Set: numeroPagamentoCliente.
	 *
	 * @param numeroPagamentoCliente the numero pagamento cliente
	 */
	public void setNumeroPagamentoCliente(String numeroPagamentoCliente) {
		this.numeroPagamentoCliente = numeroPagamentoCliente;
	}

	/**
	 * Get: qtMoeda.
	 *
	 * @return qtMoeda
	 */
	public BigDecimal getQtMoeda() {
		return qtMoeda;
	}

	/**
	 * Set: qtMoeda.
	 *
	 * @param qtMoeda the qt moeda
	 */
	public void setQtMoeda(BigDecimal qtMoeda) {
		this.qtMoeda = qtMoeda;
	}

	/**
	 * Get: serieCheque.
	 *
	 * @return serieCheque
	 */
	public String getSerieCheque() {
		return serieCheque;
	}

	/**
	 * Set: serieCheque.
	 *
	 * @param serieCheque the serie cheque
	 */
	public void setSerieCheque(String serieCheque) {
		this.serieCheque = serieCheque;
	}

	/**
	 * Get: tipoContaDebito.
	 *
	 * @return tipoContaDebito
	 */
	public String getTipoContaDebito() {
		return tipoContaDebito;
	}

	/**
	 * Set: tipoContaDebito.
	 *
	 * @param tipoContaDebito the tipo conta debito
	 */
	public void setTipoContaDebito(String tipoContaDebito) {
		this.tipoContaDebito = tipoContaDebito;
	}

	/**
	 * Get: tipoDocumento.
	 *
	 * @return tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Set: tipoDocumento.
	 *
	 * @param tipoDocumento the tipo documento
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Get: vlAbatimento.
	 *
	 * @return vlAbatimento
	 */
	public BigDecimal getVlAbatimento() {
		return vlAbatimento;
	}

	/**
	 * Set: vlAbatimento.
	 *
	 * @param vlAbatimento the vl abatimento
	 */
	public void setVlAbatimento(BigDecimal vlAbatimento) {
		this.vlAbatimento = vlAbatimento;
	}

	/**
	 * Get: vlAcrescimo.
	 *
	 * @return vlAcrescimo
	 */
	public BigDecimal getVlAcrescimo() {
		return vlAcrescimo;
	}

	/**
	 * Set: vlAcrescimo.
	 *
	 * @param vlAcrescimo the vl acrescimo
	 */
	public void setVlAcrescimo(BigDecimal vlAcrescimo) {
		this.vlAcrescimo = vlAcrescimo;
	}

	/**
	 * Get: vlDeducao.
	 *
	 * @return vlDeducao
	 */
	public BigDecimal getVlDeducao() {
		return vlDeducao;
	}

	/**
	 * Set: vlDeducao.
	 *
	 * @param vlDeducao the vl deducao
	 */
	public void setVlDeducao(BigDecimal vlDeducao) {
		this.vlDeducao = vlDeducao;
	}

	/**
	 * Get: vlDesconto.
	 *
	 * @return vlDesconto
	 */
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}

	/**
	 * Set: vlDesconto.
	 *
	 * @param vlDesconto the vl desconto
	 */
	public void setVlDesconto(BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	/**
	 * Get: vlDocumento.
	 *
	 * @return vlDocumento
	 */
	public BigDecimal getVlDocumento() {
		return vlDocumento;
	}

	/**
	 * Set: vlDocumento.
	 *
	 * @param vlDocumento the vl documento
	 */
	public void setVlDocumento(BigDecimal vlDocumento) {
		this.vlDocumento = vlDocumento;
	}

	/**
	 * Get: vlImpostoMovFinanceira.
	 *
	 * @return vlImpostoMovFinanceira
	 */
	public BigDecimal getVlImpostoMovFinanceira() {
		return vlImpostoMovFinanceira;
	}

	/**
	 * Set: vlImpostoMovFinanceira.
	 *
	 * @param vlImpostoMovFinanceira the vl imposto mov financeira
	 */
	public void setVlImpostoMovFinanceira(BigDecimal vlImpostoMovFinanceira) {
		this.vlImpostoMovFinanceira = vlImpostoMovFinanceira;
	}

	/**
	 * Get: vlImpostoRenda.
	 *
	 * @return vlImpostoRenda
	 */
	public BigDecimal getVlImpostoRenda() {
		return vlImpostoRenda;
	}

	/**
	 * Set: vlImpostoRenda.
	 *
	 * @param vlImpostoRenda the vl imposto renda
	 */
	public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
		this.vlImpostoRenda = vlImpostoRenda;
	}

	/**
	 * Get: vlInss.
	 *
	 * @return vlInss
	 */
	public BigDecimal getVlInss() {
		return vlInss;
	}

	/**
	 * Set: vlInss.
	 *
	 * @param vlInss the vl inss
	 */
	public void setVlInss(BigDecimal vlInss) {
		this.vlInss = vlInss;
	}

	/**
	 * Get: vlIof.
	 *
	 * @return vlIof
	 */
	public BigDecimal getVlIof() {
		return vlIof;
	}

	/**
	 * Set: vlIof.
	 *
	 * @param vlIof the vl iof
	 */
	public void setVlIof(BigDecimal vlIof) {
		this.vlIof = vlIof;
	}

	/**
	 * Get: vlIss.
	 *
	 * @return vlIss
	 */
	public BigDecimal getVlIss() {
		return vlIss;
	}

	/**
	 * Set: vlIss.
	 *
	 * @param vlIss the vl iss
	 */
	public void setVlIss(BigDecimal vlIss) {
		this.vlIss = vlIss;
	}

	/**
	 * Get: vlMora.
	 *
	 * @return vlMora
	 */
	public BigDecimal getVlMora() {
		return vlMora;
	}

	/**
	 * Set: vlMora.
	 *
	 * @param vlMora the vl mora
	 */
	public void setVlMora(BigDecimal vlMora) {
		this.vlMora = vlMora;
	}

	/**
	 * Get: vlMulta.
	 *
	 * @return vlMulta
	 */
	public BigDecimal getVlMulta() {
		return vlMulta;
	}

	/**
	 * Set: vlMulta.
	 *
	 * @param vlMulta the vl multa
	 */
	public void setVlMulta(BigDecimal vlMulta) {
		this.vlMulta = vlMulta;
	}

	/**
	 * Get: vlrAgendamento.
	 *
	 * @return vlrAgendamento
	 */
	public BigDecimal getVlrAgendamento() {
		return vlrAgendamento;
	}

	/**
	 * Set: vlrAgendamento.
	 *
	 * @param vlrAgendamento the vlr agendamento
	 */
	public void setVlrAgendamento(BigDecimal vlrAgendamento) {
		this.vlrAgendamento = vlrAgendamento;
	}

	/**
	 * Get: vlrEfetivacao.
	 *
	 * @return vlrEfetivacao
	 */
	public BigDecimal getVlrEfetivacao() {
		return vlrEfetivacao;
	}

	/**
	 * Set: vlrEfetivacao.
	 *
	 * @param vlrEfetivacao the vlr efetivacao
	 */
	public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
		this.vlrEfetivacao = vlrEfetivacao;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: cdTipoBeneficiario.
	 *
	 * @return cdTipoBeneficiario
	 */
	public Integer getCdTipoBeneficiario() {
		return cdTipoBeneficiario;
	}

	/**
	 * Set: cdTipoBeneficiario.
	 *
	 * @param cdTipoBeneficiario the cd tipo beneficiario
	 */
	public void setCdTipoBeneficiario(Integer cdTipoBeneficiario) {
		this.cdTipoBeneficiario = cdTipoBeneficiario;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public Long getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(Long nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: consultasServiceImpl.
	 *
	 * @return consultasServiceImpl
	 */
	public IConsultasService getConsultasServiceImpl() {
		return consultasServiceImpl;
	}

	/**
	 * Set: consultasServiceImpl.
	 *
	 * @param consultasServiceImpl the consultas service impl
	 */
	public void setConsultasServiceImpl(IConsultasService consultasServiceImpl) {
		this.consultasServiceImpl = consultasServiceImpl;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Is exibe beneficiario.
	 *
	 * @return true, if is exibe beneficiario
	 */
	public boolean isExibeBeneficiario() {
		return exibeBeneficiario;
	}

	/**
	 * Set: exibeBeneficiario.
	 *
	 * @param exibeBeneficiario the exibe beneficiario
	 */
	public void setExibeBeneficiario(boolean exibeBeneficiario) {
		this.exibeBeneficiario = exibeBeneficiario;
	}

	/**
	 * Get: descTipoFavorecido.
	 *
	 * @return descTipoFavorecido
	 */
	public String getDescTipoFavorecido() {
		return descTipoFavorecido;
	}

	/**
	 * Set: descTipoFavorecido.
	 *
	 * @param descTipoFavorecido the desc tipo favorecido
	 */
	public void setDescTipoFavorecido(String descTipoFavorecido) {
		this.descTipoFavorecido = descTipoFavorecido;
	}

	/**
	 * Get: cmbSituacaoPagamento.
	 *
	 * @return cmbSituacaoPagamento
	 */
	public Integer getCmbSituacaoPagamento() {
		return cmbSituacaoPagamento;
	}

	/**
	 * Set: cmbSituacaoPagamento.
	 *
	 * @param cmbSituacaoPagamento the cmb situacao pagamento
	 */
	public void setCmbSituacaoPagamento(Integer cmbSituacaoPagamento) {
		this.cmbSituacaoPagamento = cmbSituacaoPagamento;
	}

	/**
	 * Get: listaCmbSituacaoPagamento.
	 *
	 * @return listaCmbSituacaoPagamento
	 */
	public List<SelectItem> getListaCmbSituacaoPagamento() {
		return listaCmbSituacaoPagamento;
	}

	/**
	 * Set: listaCmbSituacaoPagamento.
	 *
	 * @param listaCmbSituacaoPagamento the lista cmb situacao pagamento
	 */
	public void setListaCmbSituacaoPagamento(
			List<SelectItem> listaCmbSituacaoPagamento) {
		this.listaCmbSituacaoPagamento = listaCmbSituacaoPagamento;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: cmbFormaPagamento.
	 *
	 * @return cmbFormaPagamento
	 */
	public Integer getCmbFormaPagamento() {
		return cmbFormaPagamento;
	}

	/**
	 * Set: cmbFormaPagamento.
	 *
	 * @param cmbFormaPagamento the cmb forma pagamento
	 */
	public void setCmbFormaPagamento(Integer cmbFormaPagamento) {
		this.cmbFormaPagamento = cmbFormaPagamento;
	}

	/**
	 * Get: cmbTipoFavorecido.
	 *
	 * @return cmbTipoFavorecido
	 */
	public Integer getCmbTipoFavorecido() {
		return cmbTipoFavorecido;
	}

	/**
	 * Set: cmbTipoFavorecido.
	 *
	 * @param cmbTipoFavorecido the cmb tipo favorecido
	 */
	public void setCmbTipoFavorecido(Integer cmbTipoFavorecido) {
		this.cmbTipoFavorecido = cmbTipoFavorecido;
	}

	/**
	 * Get: inscricaoFavorecido.
	 *
	 * @return inscricaoFavorecido
	 */
	public String getInscricaoFavorecido() {
		return inscricaoFavorecido;
	}

	/**
	 * Set: inscricaoFavorecido.
	 *
	 * @param inscricaoFavorecido the inscricao favorecido
	 */
	public void setInscricaoFavorecido(String inscricaoFavorecido) {
		this.inscricaoFavorecido = inscricaoFavorecido;
	}

	/**
	 * Get: listaCmbTipoFavorecido.
	 *
	 * @return listaCmbTipoFavorecido
	 */
	public List<SelectItem> getListaCmbTipoFavorecido() {
		return listaCmbTipoFavorecido;
	}

	/**
	 * Set: listaCmbTipoFavorecido.
	 *
	 * @param listaCmbTipoFavorecido the lista cmb tipo favorecido
	 */
	public void setListaCmbTipoFavorecido(List<SelectItem> listaCmbTipoFavorecido) {
		this.listaCmbTipoFavorecido = listaCmbTipoFavorecido;
	}

	/**
	 * Is exibe inscricao favorevido.
	 *
	 * @return true, if is exibe inscricao favorevido
	 */
	public boolean isExibeInscricaoFavorevido() {
		return exibeInscricaoFavorevido;
	}

	/**
	 * Set: exibeInscricaoFavorevido.
	 *
	 * @param exibeInscricaoFavorevido the exibe inscricao favorevido
	 */
	public void setExibeInscricaoFavorevido(boolean exibeInscricaoFavorevido) {
		this.exibeInscricaoFavorevido = exibeInscricaoFavorevido;
	}

	/**
	 * Get: dsTipoBeneficiario.
	 *
	 * @return dsTipoBeneficiario
	 */
	public String getDsTipoBeneficiario() {
	    return dsTipoBeneficiario;
	}

	/**
	 * Set: dsTipoBeneficiario.
	 *
	 * @param dsTipoBeneficiario the ds tipo beneficiario
	 */
	public void setDsTipoBeneficiario(String dsTipoBeneficiario) {
	    this.dsTipoBeneficiario = dsTipoBeneficiario;
	}

	/**
	 * Get: dtDevolucaoEstorno.
	 *
	 * @return dtDevolucaoEstorno
	 */
	public String getDtDevolucaoEstorno() {
		return dtDevolucaoEstorno;
	}

	/**
	 * Set: dtDevolucaoEstorno.
	 *
	 * @param dtDevolucaoEstorno the dt devolucao estorno
	 */
	public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
		this.dtDevolucaoEstorno = dtDevolucaoEstorno;
	}

    /**
     * Nome: getListaFormaPagamento
     *
     * @return listaFormaPagamento
     */
    public List<ListarTipoRetiradaOcorrenciasSaidaDTO> getListaFormaPagamento() {
        return listaFormaPagamento;
    }

    /**
     * Nome: setListaFormaPagamento
     *
     * @param listaFormaPagamento
     */
    public void setListaFormaPagamento(List<ListarTipoRetiradaOcorrenciasSaidaDTO> listaFormaPagamento) {
        this.listaFormaPagamento = listaFormaPagamento;
    }
	
}