/*
 * Nome: br.com.bradesco.web.pgit.view.bean.model
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 31/03/2015
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Nome: CalculadorValorPercentual
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public final class CalculadorValorPercentual {

	/**
	 * Instantiates a new calculador valor percentual.
	 */
	private CalculadorValorPercentual() {
		super();
	}

	/**
	 * Calcular valor.
	 *
	 * @param valorBase the valor base
	 * @param percentual the percentual
	 * @return the big decimal
	 */
	public static BigDecimal calcularValor(BigDecimal valorBase, BigDecimal percentual) {
		BigDecimal valor = null;
		if (isValoresValidos(valorBase, percentual)) {
			valor = percentual.multiply(valorBase).divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
		}
		return valor;
	}

	/**
	 * Calcular percentual.
	 *
	 * @param valorBase the valor base
	 * @param valor the valor
	 * @return the big decimal
	 */
	public static BigDecimal calcularPercentual(BigDecimal valorBase, BigDecimal valor) {
		BigDecimal percentual = null;
		if (isValoresValidos(valorBase, valor)) {
			percentual = valor.divide(valorBase, 4, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
		}
		return percentual;
	}

	/**
	 * Checks if is valores validos.
	 *
	 * @param valorBase the valor base
	 * @param valor the valor
	 * @return true, if is valores validos
	 */
	private static boolean isValoresValidos(BigDecimal valorBase, BigDecimal valor) {
		return valor != null && valorBase != null && valorBase.compareTo(BigDecimal.ZERO) > 0;
	}
}