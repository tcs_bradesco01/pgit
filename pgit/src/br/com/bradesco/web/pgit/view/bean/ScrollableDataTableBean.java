/*
 * Nome: br.com.bradesco.web.pgit.view.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean;

import java.util.List;

import javax.faces.event.ActionEvent;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.pgit.service.business.listaregra.IListaRegraService;
import br.com.bradesco.web.pgit.service.business.listaregra.bean.ListaRegraVO;

/**
 * Nome: ScrollableDataTableBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ScrollableDataTableBean {

    // Vari�vel indGestorMaster
    /** Atributo indGestorMaster. */
    private String indGestorMaster;
    
    // Vari�vel tipoConsulta
    /** Atributo tipoConsulta. */
    private Integer tipoConsulta;
    
    // Vari�vel codRegraAlcada
    /** Atributo codRegraAlcada. */
    private Integer codRegraAlcada;
    
    // Vari�vel codSituacao
    /** Atributo codSituacao. */
    private Integer codSituacao;
    
    // Vari�vel dataPesquisaInic
    /** Atributo dataPesquisaInic. */
    private String dataPesquisaInic;
    
    // Vari�vel dataPesquisaFim
    /** Atributo dataPesquisaFim. */
    private String dataPesquisaFim;
    
    // Vari�vel codPessoaJuridica
    /** Atributo codPessoaJuridica. */
    private Long codPessoaJuridica;
    
    // Vari�vel codDepartamento
    /** Atributo codDepartamento. */
    private Integer codDepartamento;
    
    // Vari�vel codTipoUorg
    /** Atributo codTipoUorg. */
    private Integer codTipoUorg;
	
    /** Atributo dependencias. */
    private List<ListaRegraVO> dependencias;
    
	/** Atributo listaRegraService. */
	private IListaRegraService listaRegraService;
		
	/**
	 * Nome: setDependencias
	 * <p>Prop�sito: M�todo set para o atributo dependencias. </p>
	 * @param List<ListaRegraVO> dependencias
	 */
	public void setDependencias(List<ListaRegraVO> dependencias) {
		this.dependencias = dependencias;
	}

	/**
	 * Carrega lista.
	 *
	 * @param evt the evt
	 */
	public void carregaLista(ActionEvent evt) {
		ListaRegraVO vo = new ListaRegraVO();
		
		vo.setIndGestorMaster(this.getIndGestorMaster());
		vo.setTipoConsulta(this.getTipoConsulta());
		vo.setCodRegraAlcada(this.getCodRegraAlcada());
		vo.setCodSituacao(this.getCodSituacao());
		vo.setDataPesquisaInic(this.getDataPesquisaInic());
		vo.setDataPesquisaFim(this.getDataPesquisaFim());
		vo.setCodPessoaJuridica(this.getCodPessoaJuridica());
		vo.setCodDepartamento(this.getCodDepartamento());
		vo.setCodTipoUorg(this.getCodTipoUorg());
		
		try {
			this.setDependencias(this.listaRegraService.listaRegras(vo));
		} catch(Exception e) {
			throw new BradescoViewException(e.getMessage(), e, null);
		}
	}
	
	/**
	 * Get: dependencias.
	 *
	 * @return dependencias
	 */
	public List<ListaRegraVO> getDependencias() {
		return this.dependencias;
	}

	/**
	 * Nome: getCodDepartamento
	 * <p>Prop�sito: M�todo get para o atributo codDepartamento. </p>
	 * @return Integer codDepartamento
	 */
	public Integer getCodDepartamento() {
		return codDepartamento;
	}

	/**
	 * Nome: setCodDepartamento
	 * <p>Prop�sito: M�todo set para o atributo codDepartamento. </p>
	 * @param Integer codDepartamento
	 */
	public void setCodDepartamento(Integer codDepartamento) {
		this.codDepartamento = codDepartamento;
	}

	/**
	 * Nome: getCodPessoaJuridica
	 * <p>Prop�sito: M�todo get para o atributo codPessoaJuridica. </p>
	 * @return Long codPessoaJuridica
	 */
	public Long getCodPessoaJuridica() {
		return codPessoaJuridica;
	}

	/**
	 * Nome: setCodPessoaJuridica
	 * <p>Prop�sito: M�todo set para o atributo codPessoaJuridica. </p>
	 * @param Long codPessoaJuridica
	 */
	public void setCodPessoaJuridica(Long codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}

	/**
	 * Nome: getCodRegraAlcada
	 * <p>Prop�sito: M�todo get para o atributo codRegraAlcada. </p>
	 * @return Integer codRegraAlcada
	 */
	public Integer getCodRegraAlcada() {
		return codRegraAlcada;
	}

	/**
	 * Nome: setCodRegraAlcada
	 * <p>Prop�sito: M�todo set para o atributo codRegraAlcada. </p>
	 * @param Integer codRegraAlcada
	 */
	public void setCodRegraAlcada(Integer codRegraAlcada) {
		this.codRegraAlcada = codRegraAlcada;
	}

	/**
	 * Nome: getCodSituacao
	 * <p>Prop�sito: M�todo get para o atributo codSituacao. </p>
	 * @return Integer codSituacao
	 */
	public Integer getCodSituacao() {
		return codSituacao;
	}

	/**
	 * Nome: setCodSituacao
	 * <p>Prop�sito: M�todo set para o atributo codSituacao. </p>
	 * @param Integer codSituacao
	 */
	public void setCodSituacao(Integer codSituacao) {
		this.codSituacao = codSituacao;
	}

	/**
	 * Nome: getCodTipoUorg
	 * <p>Prop�sito: M�todo get para o atributo codTipoUorg. </p>
	 * @return Integer codTipoUorg
	 */
	public Integer getCodTipoUorg() {
		return codTipoUorg;
	}

	/**
	 * Nome: setCodTipoUorg
	 * <p>Prop�sito: M�todo set para o atributo codTipoUorg. </p>
	 * @param Integer codTipoUorg
	 */
	public void setCodTipoUorg(Integer codTipoUorg) {
		this.codTipoUorg = codTipoUorg;
	}

	/**
	 * Nome: getDataPesquisaFim
	 * <p>Prop�sito: M�todo get para o atributo dataPesquisaFim. </p>
	 * @return String dataPesquisaFim
	 */
	public String getDataPesquisaFim() {
		return dataPesquisaFim;
	}

	/**
	 * Nome: setDataPesquisaFim
	 * <p>Prop�sito: M�todo set para o atributo dataPesquisaFim. </p>
	 * @param String dataPesquisaFim
	 */
	public void setDataPesquisaFim(String dataPesquisaFim) {
		this.dataPesquisaFim = dataPesquisaFim;
	}

	/**
	 * Nome: getDataPesquisaInic
	 * <p>Prop�sito: M�todo get para o atributo dataPesquisaInic. </p>
	 * @return String dataPesquisaInic
	 */
	public String getDataPesquisaInic() {
		return dataPesquisaInic;
	}

	/**
	 * Nome: setDataPesquisaInic
	 * <p>Prop�sito: M�todo set para o atributo dataPesquisaInic. </p>
	 * @param String dataPesquisaInic
	 */
	public void setDataPesquisaInic(String dataPesquisaInic) {
		this.dataPesquisaInic = dataPesquisaInic;
	}

	/**
	 * Nome: getIndGestorMaster
	 * <p>Prop�sito: M�todo get para o atributo indGestorMaster. </p>
	 * @return String indGestorMaster
	 */
	public String getIndGestorMaster() {
		return indGestorMaster;
	}

	/**
	 * Nome: setIndGestorMaster
	 * <p>Prop�sito: M�todo set para o atributo indGestorMaster. </p>
	 * @param String indGestorMaster
	 */
	public void setIndGestorMaster(String indGestorMaster) {
		this.indGestorMaster = indGestorMaster;
	}

	/**
	 * Nome: getListaRegraService
	 * <p>Prop�sito: M�todo get para o atributo listaRegraService. </p>
	 * @return IListaRegraService listaRegraService
	 */
	public IListaRegraService getListaRegraService() {
		return listaRegraService;
	}

	/**
	 * Nome: setListaRegraService
	 * <p>Prop�sito: M�todo set para o atributo listaRegraService. </p>
	 * @param IListaRegraService listaRegraService
	 */
	public void setListaRegraService(IListaRegraService listaRegraService) {
		this.listaRegraService = listaRegraService;
	}

	/**
	 * Nome: getTipoConsulta
	 * <p>Prop�sito: M�todo get para o atributo tipoConsulta. </p>
	 * @return Integer tipoConsulta
	 */
	public Integer getTipoConsulta() {
		return tipoConsulta;
	}

	/**
	 * Nome: setTipoConsulta
	 * <p>Prop�sito: M�todo set para o atributo tipoConsulta. </p>
	 * @param Integer tipoConsulta
	 */
	public void setTipoConsulta(Integer tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

}