/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarAplicativoFormatacaoArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarAplicativoFormatacaoArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarEmpresaTransmissaoArquivoVanEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarEmpresaTransmissaoArquivoVanSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMeioTransmissaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMeioTransmissaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.AlterarPerfilContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.AlterarPerfilContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarAssocTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: MeiosTransmissaoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class MeiosTransmissaoBean {

	/** Atributo MEIO_TRANSMISSAO_WEB_TA. */
	private static final Integer MEIO_TRANSMISSAO_WEB_TA = 2;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo manterPerfilTrocaArqLayoutServiceImpl. */
	private IManterPerfilTrocaArqLayoutService manterPerfilTrocaArqLayoutServiceImpl;
	
	/** Atributo comboServiceImpl. */
	private IComboService comboServiceImpl;
	
	/** Atributo manterVincPerfilTrocaArqPartImpl. */
	private IManterVincPerfilTrocaArqPartService manterVincPerfilTrocaArqPartImpl;

	/** Atributo saidaDetalhesPerfilTrocaArquivo. */
	private DetalharPerfilTrocaArquivoSaidaDTO saidaDetalhesPerfilTrocaArquivo;
	
	/** Atributo listaMeioTransmissao. */
	private ConsultarListaPerfilTrocaArquivoSaidaDTO listaMeioTransmissao;
	
	/** Atributo listaGridParticipantesVinculados. */
	private List<ListarAssocTrocaArqParticipanteSaidaDTO> listaGridParticipantesVinculados;
	
	/** Atributo listaGridParticipante. */
	private List<ListarParticipanteSaidaDTO> listaGridParticipante;
	
	/** Atributo listaGridMeioTransmissao. */
	private List<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO> listaGridMeioTransmissao;
	
	/** Atributo listaControleMeioTransmissao. */
	private List<SelectItem> listaControleMeioTransmissao;
	
	/** Atributo itemSelecionadoMeioTransmissao. */
	private Integer itemSelecionadoMeioTransmissao;

	/** Atributo rdoAplicFormProprio. */
	private String rdoAplicFormProprio;
	
	/** Atributo rdoVan. */
	private String rdoVan;
	
	/** Atributo dsLayoutProprio. */
	private String dsLayoutProprio;
	
	/** Atributo dsAplicFormat. */
	private String dsAplicFormat;
	
	/** Atributo listaAplicativoFormatacao. */
	private List<SelectItem> listaAplicativoFormatacao = new ArrayList<SelectItem>();
	
	/** Atributo listaAplicativoFormatacaoHash. */
	private Map<Long, String> listaAplicativoFormatacaoHash = new HashMap<Long, String>();
	
	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer, String> listaTipoLayoutArquivoHash = new HashMap<Integer, String>();
	
	/** Atributo listarMeioTransmissao. */
	private List<SelectItem> listarMeioTransmissao = new ArrayList<SelectItem>();
	
	/** Atributo listarMeioTransmissaoHash. */
	private Map<Integer, String> listarMeioTransmissaoHash = new HashMap<Integer, String>();
	
	/** Atributo listaEmpresaResponsavelTransmissao. */
	private List<SelectItem> listaEmpresaResponsavelTransmissao = new ArrayList<SelectItem>();
	
	/** Atributo listaEmpresaResponsavelTransmissaoHash. */
	private Map<Long, String> listaEmpresaResponsavelTransmissaoHash = new HashMap<Long, String>();
	
	/** Atributo listaResponsavelCustoTransArq. */
	private List<SelectItem> listaResponsavelCustoTransArq = new ArrayList<SelectItem>();
	
	/** Atributo listaResponsavelCustoTransArqHash. */
	private Map<Integer, String> listaResponsavelCustoTransArqHash = new HashMap<Integer, String>();

	/** Atributo telaAlteracao. */
	private boolean telaAlteracao;

	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;

	// Dados contrato

	/** Atributo contratoSelecionado. */
	private ListarContratosPgitSaidaDTO contratoSelecionado;

	//flags
	/** Atributo flagMeioTransmissao. */
	private boolean flagMeioTransmissao;

	/** Atributo flagDadosControle. */
	private boolean flagDadosControle;

	/** Atributo flagClienteSolicitante. */
	private boolean flagClienteSolicitante;

	/** Atributo percentualCliente. */
	private BigDecimal percentualCliente = null;

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		setTelaAlteracao(true);
		setPercentualCliente(null);
		listarTipoLayoutArquivo();
		listarAplicativoFormatacao();
		listarMeioTransmissao();
		listarEmpresaResponsavelTransmissao();
		listarResponsavelCustoTransmissaorquivo();
		preencheDados();
		calcularPercentCliente();

		return "ALTERAR";
	}
	
	/**
	 * Calcular percent cliente.
	 */
	private void calcularPercentCliente() {
		BigDecimal pcCustoOrganizacaoTransmissao = saidaDetalhesPerfilTrocaArquivo.getPcCustoOrganizacaoTransmissao();

		BigDecimal percentualDiferenca = null;
		if ("CLIENTE".equalsIgnoreCase(saidaDetalhesPerfilTrocaArquivo.getDsResponsavelCustoEmpresa())) {
			percentualDiferenca = new BigDecimal("100");
		} else {
			if(pcCustoOrganizacaoTransmissao != null) {
				percentualDiferenca = new BigDecimal("100").subtract(pcCustoOrganizacaoTransmissao);
			}
		}

		setPercentualCliente(percentualDiferenca);
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {

		saidaDetalhesPerfilTrocaArquivo.setDsCodMeioPrincipalRemessa(listarMeioTransmissaoHash.get(saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRemessa()));
		saidaDetalhesPerfilTrocaArquivo.setDsCodMeioAlternRemessa(listarMeioTransmissaoHash.get(saidaDetalhesPerfilTrocaArquivo.getCdMeioAlternRemessa()));
		saidaDetalhesPerfilTrocaArquivo.setCdCodMeioPrincipalRetorno(listarMeioTransmissaoHash.get(saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRetorno()));
		saidaDetalhesPerfilTrocaArquivo.setDsMeioAltrnRetorno(listarMeioTransmissaoHash.get(saidaDetalhesPerfilTrocaArquivo.getCdMeioAltrnRetorno()));
		saidaDetalhesPerfilTrocaArquivo.setDsEmpresa(listaEmpresaResponsavelTransmissaoHash.get(saidaDetalhesPerfilTrocaArquivo.getCdPessoaJuridicaParceiro()));
		saidaDetalhesPerfilTrocaArquivo.setDsResponsavelCustoEmpresa(saidaDetalhesPerfilTrocaArquivo.getCdResponsavelCustoEmpresa() != null ? listaResponsavelCustoTransArqHash.get(saidaDetalhesPerfilTrocaArquivo.getCdResponsavelCustoEmpresa()) : "");
		saidaDetalhesPerfilTrocaArquivo.setDsUtilizacaoEmpresaVan(verificaStatusRdoVan(rdoVan));
		saidaDetalhesPerfilTrocaArquivo.setDsAplicFormat(saidaDetalhesPerfilTrocaArquivo.getCdAplicacaoTransmPagamento() != null ? listaAplicativoFormatacaoHash.get(saidaDetalhesPerfilTrocaArquivo.getCdAplicacaoTransmPagamento()): "");
		
		
		if(saidaDetalhesPerfilTrocaArquivo.getCdUnidadeOrganizacional() == null ){
			saidaDetalhesPerfilTrocaArquivo.setCdPessoaJuridica(null);
		}else{
			saidaDetalhesPerfilTrocaArquivo.setCdPessoaJuridica(cdPessoaJuridica);
		}

		String clienteFormatado = "";
		if ("CLIENTE".equalsIgnoreCase(saidaDetalhesPerfilTrocaArquivo.getDsResponsavelCustoEmpresa())) {
			clienteFormatado = PgitUtil.formatNumberPercent(new BigDecimal(100));
		} else if (saidaDetalhesPerfilTrocaArquivo.getPcCustoOrganizacaoTransmissao() != null) {
			BigDecimal valorCliente = new BigDecimal("100").subtract(saidaDetalhesPerfilTrocaArquivo.getPcCustoOrganizacaoTransmissao());
			clienteFormatado = PgitUtil.formatNumberPercent((BigDecimal) PgitUtil.verificaZero(valorCliente));
		}

		saidaDetalhesPerfilTrocaArquivo.setClienteFormatado(clienteFormatado);
		saidaDetalhesPerfilTrocaArquivo.setPcCustoOrganizacaoTransmissao((BigDecimal) PgitUtil.verificaZero(saidaDetalhesPerfilTrocaArquivo.getPcCustoOrganizacaoTransmissao()));

		if ("1".equals(rdoAplicFormProprio)) {
			setDsLayoutProprio("SIM");
		} else {
			setDsLayoutProprio("NAO");
		}

		return "AVANCAR_ALTERAR";

	}
	
	/**
	 * Verifica status rdo van.
	 *
	 * @param rdoVan the rdo van
	 * @return the string
	 */
	private String verificaStatusRdoVan(String rdoVan) {

		if ("1".equals(rdoVan)) {
			return "Sim";
		} else {
			return "N�o";
		}
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {
		ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO selecaoMeioTransmissao = getListaGridMeioTransmissao().get(getItemSelecionadoMeioTransmissao());
		AlterarPerfilContratoEntradaDTO entrada = new AlterarPerfilContratoEntradaDTO();

		entrada.setCdPerfilTrocaArquivo(saidaDetalhesPerfilTrocaArquivo.getCdPerfilTrocaArq());
		entrada.setCdTipoLayoutArquivo(saidaDetalhesPerfilTrocaArquivo.getCdTipoLayoutArquivo());
		entrada.setCdMeioPrincipalRemessa(saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRemessa());
		entrada.setCodigoMeioAlternativoRemessa(saidaDetalhesPerfilTrocaArquivo.getCdMeioAlternRemessa());
		entrada.setCdMeioPrincipalRetorno(saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRetorno());
		entrada.setCdMeioAlternativoRetorno(saidaDetalhesPerfilTrocaArquivo.getCdMeioAltrnRetorno());
		entrada.setCdPessoaJuridicaParceiro(saidaDetalhesPerfilTrocaArquivo.getCdPessoaJuridicaParceiro());
		entrada.setCdEmpresaResponsavel(saidaDetalhesPerfilTrocaArquivo.getCdResponsavelCustoEmpresa());
		entrada.setCdCustoTransmicao(saidaDetalhesPerfilTrocaArquivo.getPcCustoOrganizacaoTransmissao());
		entrada.setCdApliFormat(saidaDetalhesPerfilTrocaArquivo.getCdAplicacaoTransmPagamento() == null ? 0 : saidaDetalhesPerfilTrocaArquivo.getCdAplicacaoTransmPagamento().longValue());
		entrada.setCdPessoaJuridica(saidaDetalhesPerfilTrocaArquivo.getCdPessoaJuridica());
		entrada.setCdUnidadeOrganizacional(saidaDetalhesPerfilTrocaArquivo.getCdUnidadeOrganizacional());
		entrada.setDsNomeContatoCliente(saidaDetalhesPerfilTrocaArquivo.getDsNomeContatoCliente());
		entrada.setCdAreaFone(saidaDetalhesPerfilTrocaArquivo.getCdAreaFoneCliente());
		entrada.setCdFoneContatoCliente(NumberUtils.convertTelefoneSemDdd(saidaDetalhesPerfilTrocaArquivo.getCdFoneContatoCliente()));
		entrada.setCdRamalContatoCliente(saidaDetalhesPerfilTrocaArquivo.getCdRamalContatoCliente());
		entrada.setDsEmailContatoCliente(saidaDetalhesPerfilTrocaArquivo.getDsEmailContatoCliente());
		entrada.setDsSolicitacaoPendente(saidaDetalhesPerfilTrocaArquivo.getDsSolicitacaoPendente());
		entrada.setCdAreaFonePend(saidaDetalhesPerfilTrocaArquivo.getCdAreaFonePend());
		entrada.setCdFoneSolicitacaoPend(NumberUtils.convertTelefoneSemDdd(saidaDetalhesPerfilTrocaArquivo.getCdFoneSolicitacaoPend()));
		entrada.setCdRamalSolctPend(saidaDetalhesPerfilTrocaArquivo.getCdRamalSolctPend());
		entrada.setDsEmailSolctPend(saidaDetalhesPerfilTrocaArquivo.getDsEmailSolctPend());
		entrada.setQtMesRegistroTrafg(NumberUtils.convertVolume(saidaDetalhesPerfilTrocaArquivo.getQtMesRegistroTrafg()));
		entrada.setDsObsGeralPerfil(saidaDetalhesPerfilTrocaArquivo.getDsObsGeralPerfil());
		
		entrada.setCdProdutoServicoOperacao(selecaoMeioTransmissao.getCdProdutoServicoOperacao());
		entrada.setCdProdutoOperacaoRelacionado(selecaoMeioTransmissao.getCdProdutoOperacaoRelacionado());
		entrada.setCdRelacionamentoProduto(selecaoMeioTransmissao.getCdRelacionamentoProduto());
		

		try {
			AlterarPerfilContratoSaidaDTO saida = manterVincPerfilTrocaArqPartImpl.alterarPerfilContrato(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(), "MEIO_TRANSMISSAO", BradescoViewExceptionActionType.ACTION, false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}

		return "";
	}

	/**
	 * Participantes vinculados.
	 *
	 * @return the string
	 */
	public String participantesVinculados() {
		setTelaAlteracao(false);
		preencheDados();
		
		IdentificacaoClienteContratoBean identificacaoClienteContratoBean = (IdentificacaoClienteContratoBean)FacesUtils.getManagedBean("identificacaoClienteContratoBean");
		ListarParticipanteEntradaDTO entrada = new ListarParticipanteEntradaDTO();
	
		entrada.setCdPerfilTrocaArquivo(listaGridMeioTransmissao.get(itemSelecionadoMeioTransmissao).getCdPerfilTrocaArquivo());
		entrada.setCdpessoaJuridicaContrato(contratoSelecionado.getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(contratoSelecionado.getCdTipoContrato());
		entrada.setCdTipoLayoutArquivo(listaGridMeioTransmissao.get(itemSelecionadoMeioTransmissao).getCdTipoLayoutArquivo());
		entrada.setNrSequenciaContratoNegocio(contratoSelecionado.getNrSequenciaContrato());
		if(identificacaoClienteContratoBean.isFlagPesquisouPorIdentCliente()){
			entrada.setCdClub(contratoSelecionado.getCdClubRepresentante());
		}else{
			entrada.setCdClub(0L);
		}
		
		listaGridParticipante = manterVincPerfilTrocaArqPartImpl.listarParticipante(entrada);
		

		return "PARTICIPANTES_VINCULADOS";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		setTelaAlteracao(false);

		preencheDados();

		String clienteFormatado = "";
		if ("CLIENTE".equalsIgnoreCase(saidaDetalhesPerfilTrocaArquivo.getDsResponsavelCustoEmpresa())) {
			clienteFormatado = PgitUtil.formatNumberPercent(new BigDecimal(100));
		} else if (saidaDetalhesPerfilTrocaArquivo.getPcCustoOrganizacaoTransmissao() != null) {
			BigDecimal valorCliente = new BigDecimal("100").subtract(saidaDetalhesPerfilTrocaArquivo.getPcCustoOrganizacaoTransmissao());
			clienteFormatado = PgitUtil.formatNumberPercent((BigDecimal) PgitUtil.verificaZero(valorCliente));
		}
		saidaDetalhesPerfilTrocaArquivo.setClienteFormatado(clienteFormatado);
		
		if (saidaDetalhesPerfilTrocaArquivo.getCdIndicadorLayoutProprio() != null){
			if (saidaDetalhesPerfilTrocaArquivo.getCdIndicadorLayoutProprio() == 1){
				setDsLayoutProprio("SIM");
				setDsAplicFormat("");
			}else{
				setDsLayoutProprio("N�O");
				setDsAplicFormat(saidaDetalhesPerfilTrocaArquivo.getDsAplicFormat());
			}
		}

		return "DETALHAR";
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoMeioTransmissao(null);
		return "VOLTAR";
	}

	/**
	 * Voltar alterar.
	 *
	 * @return the string
	 */
	public String voltarAlterar() {

		return "VOLTAR_ALTERAR";
	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados() {
		DetalharPerfilTrocaArquivoEntradaDTO entrada = new DetalharPerfilTrocaArquivoEntradaDTO();

		ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO selecaoMeioTransmissao = getListaGridMeioTransmissao().get(getItemSelecionadoMeioTransmissao());

		entrada.setCdPerfilTrocaArquivo(selecaoMeioTransmissao.getCdPerfilTrocaArquivo());
		entrada.setCdTipoLayoutArquivo(selecaoMeioTransmissao.getCdTipoLayoutArquivo());
		entrada.setCdProdutoServicoOperacao(selecaoMeioTransmissao.getCdProdutoServicoOperacao());
		entrada.setCdProdutoOperacaoRelacionado(selecaoMeioTransmissao.getCdProdutoOperacaoRelacionado());
		entrada.setCdRelacionamentoProduto(selecaoMeioTransmissao.getCdRelacionamentoProduto());

		saidaDetalhesPerfilTrocaArquivo = getManterPerfilTrocaArqLayoutServiceImpl().detalharPerfilTrocaArquivo(entrada);
		
		setDsLayoutProprio(selecaoMeioTransmissao.getDsLayoutProprio());
		setCdPessoaJuridica(saidaDetalhesPerfilTrocaArquivo.getCdPessoaJuridica());
		
		if (isTelaAlteracao()) {
			
			if ("Sim".equalsIgnoreCase(selecaoMeioTransmissao.getDsLayoutProprio())) {
				setRdoAplicFormProprio("1");
			} else {
				setRdoAplicFormProprio("2");
			}

			if ("sim".equalsIgnoreCase(saidaDetalhesPerfilTrocaArquivo.getDsUtilizacaoEmpresaVan())) {
				setRdoVan("1");
			} else {
				setRdoVan("2");
				saidaDetalhesPerfilTrocaArquivo.setQtMesRegistroTrafg(null);
				saidaDetalhesPerfilTrocaArquivo.setCdUnidadeOrganizacional(null);
			}

			if (saidaDetalhesPerfilTrocaArquivo.getPcCustoOrganizacaoTransmissao() != null
					&& saidaDetalhesPerfilTrocaArquivo.getPcCustoOrganizacaoTransmissao().compareTo(BigDecimal.ZERO) == 0) {
				saidaDetalhesPerfilTrocaArquivo.setPcCustoOrganizacaoTransmissao(null);
			}

			if (saidaDetalhesPerfilTrocaArquivo.getCdUnidadeOrganizacional() != null
					&& saidaDetalhesPerfilTrocaArquivo.getCdUnidadeOrganizacional() == 0) {
				 saidaDetalhesPerfilTrocaArquivo.setCdUnidadeOrganizacional(null);
			 } 
			if (saidaDetalhesPerfilTrocaArquivo.getCdAreaFoneCliente() != null
					&& saidaDetalhesPerfilTrocaArquivo.getCdAreaFoneCliente() == 0) {
				saidaDetalhesPerfilTrocaArquivo.setCdAreaFoneCliente(null);
			}

			if ("0".equals(saidaDetalhesPerfilTrocaArquivo.getCdFoneContatoCliente())) {
				saidaDetalhesPerfilTrocaArquivo.setCdFoneContatoCliente(null);
			}

			if (saidaDetalhesPerfilTrocaArquivo.getCdAreaFonePend() != null
					&& saidaDetalhesPerfilTrocaArquivo.getCdAreaFonePend() == 0) {
				saidaDetalhesPerfilTrocaArquivo.setCdAreaFonePend(null);
			}

			if ("0".equals(saidaDetalhesPerfilTrocaArquivo.getCdFoneSolicitacaoPend())) {
				saidaDetalhesPerfilTrocaArquivo.setCdFoneSolicitacaoPend(null);
			}
			
			if (saidaDetalhesPerfilTrocaArquivo.getCdIndicadorLayoutProprio() != null){
				if (saidaDetalhesPerfilTrocaArquivo.getCdIndicadorLayoutProprio() == 1){
					setRdoAplicFormProprio("1");
				}else{
					setRdoAplicFormProprio("2");
				}
			}

			habilitarFiltroMeioTransmissaoAlterar();
		}
	}

	/**
	 * Listar tipo layout arquivo.
	 */
	private void listarTipoLayoutArquivo() {
		try {
			setListaTipoLayoutArquivo(new ArrayList<SelectItem>());
			setListaTipoLayoutArquivoHash(new HashMap<Integer, String>());

			TipoLayoutArquivoEntradaDTO entrada = new TipoLayoutArquivoEntradaDTO();
			entrada.setCdSituacaoVinculacaoConta(0);

			List<TipoLayoutArquivoSaidaDTO> lista = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			lista = getComboServiceImpl().listarTipoLayoutArquivoCem(entrada);

			for (TipoLayoutArquivoSaidaDTO combo : lista) {
				getListaTipoLayoutArquivo().add(new SelectItem(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo()));
				getListaTipoLayoutArquivoHash().put(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaTipoLayoutArquivo(new ArrayList<SelectItem>());
			setListaTipoLayoutArquivoHash(new HashMap<Integer, String>());
		}
	}

	/**
	 * Listar aplicativo formatacao.
	 */
	private void listarAplicativoFormatacao() {
		setListaAplicativoFormatacao(new ArrayList<SelectItem>());
		setListaAplicativoFormatacaoHash(new HashMap<Long, String>());

		List<ListarAplicativoFormatacaoArquivoSaidaDTO> lista = listarAplicativoFormatacaoArquivo(48);

		for (ListarAplicativoFormatacaoArquivoSaidaDTO combo : lista) {
			getListaAplicativoFormatacao().add(new SelectItem(combo.getCodigo().longValue(), combo.getDescricao()));
			getListaAplicativoFormatacaoHash().put(combo.getCodigo().longValue(), combo.getDescricao());
		}
	}

	/**
	 * Listar aplicativo formatacao arquivo.
	 *
	 * @param codSituacao the cod situacao
	 * @return the list< listar aplicativo formatacao arquivo saida dt o>
	 */
	private List<ListarAplicativoFormatacaoArquivoSaidaDTO> listarAplicativoFormatacaoArquivo(Integer codSituacao) {
		try {
			ListarAplicativoFormatacaoArquivoEntradaDTO entrada = new ListarAplicativoFormatacaoArquivoEntradaDTO();

			entrada.setCdSituacao(codSituacao);
			entrada.setCdAplictvTransmPgto(0);
			entrada.setCdTipoLayoutArquivo(0);

			return getComboServiceImpl().listarAplicativoFormatacaoArquivo(entrada);
		} catch (PdcAdapterFunctionalException p) {
			return new ArrayList<ListarAplicativoFormatacaoArquivoSaidaDTO>();
		}
	}

	/**
	 * Listar meio transmissao.
	 */
	private void listarMeioTransmissao() {
		try {
			setListarMeioTransmissao(new ArrayList<SelectItem>());
			setListarMeioTransmissaoHash(new HashMap<Integer, String>());

			ListarMeioTransmissaoPagamentoEntradaDTO entrada = new ListarMeioTransmissaoPagamentoEntradaDTO();
			entrada.setCdSituacaoVinculacaoConta(0);

			ListarMeioTransmissaoPagamentoSaidaDTO saida = new ListarMeioTransmissaoPagamentoSaidaDTO();
			List<ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO> lista = new ArrayList<ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO>();

			saida = getComboServiceImpl().listarMeioTransmissaoPagamento(entrada);

			lista = saida.getOcorrencias();

			for (ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO combo : lista) {
				getListarMeioTransmissao().add(new SelectItem(combo.getCdTipoMeioTransmissao(), combo.getDsTipoMeioTransmissao()));
				getListarMeioTransmissaoHash().put(combo.getCdTipoMeioTransmissao(), combo.getDsTipoMeioTransmissao());
			}
		} catch (PdcAdapterFunctionalException e) {
			setListarMeioTransmissao(new ArrayList<SelectItem>());
			setListarMeioTransmissaoHash(new HashMap<Integer, String>());
		}
	}

	/**
	 * Habilitar filtro meio transmissao.
	 */
	public void habilitarFiltroMeioTransmissao() {
		boolean flagMeioTransmissaoPrincipalRemessa = true;
		boolean flagMeioTransmissaoAlternativoRemessa = true;
		boolean flagMeioTransmissaoAlternativoRetorno = true;
		boolean flagMeioTransmissaoPrincipalRetorno = true;
		boolean flagMeioTransmissao14 = true;
		setFlagDadosControle(true);
		setFlagClienteSolicitante(true);

		if (saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRemessa() != 0) {
			switch (saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRemessa()) {
			case 7:
				flagMeioTransmissaoPrincipalRemessa = false;
				break;
			case 8:
				flagMeioTransmissaoPrincipalRemessa = false;
				break;
			case 9:
				flagMeioTransmissaoPrincipalRemessa = false;
				break;
			default:
				flagMeioTransmissaoPrincipalRemessa = true;
			}
		}
		if (saidaDetalhesPerfilTrocaArquivo.getCdMeioAlternRemessa() != 0) {
			switch (saidaDetalhesPerfilTrocaArquivo.getCdMeioAlternRemessa()) {
			case 7:
				flagMeioTransmissaoAlternativoRemessa = false;
				break;
			case 8:
				flagMeioTransmissaoAlternativoRemessa = false;
				break;
			case 9:
				flagMeioTransmissaoAlternativoRemessa = false;
				break;
			default:
				flagMeioTransmissaoAlternativoRemessa = true;
			}
		}
		if (saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRetorno() != 0) {
			switch (saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRetorno()) {
			case 7:
				flagMeioTransmissaoAlternativoRetorno = false;
				break;
			case 8:
				flagMeioTransmissaoAlternativoRetorno = false;
				break;
			case 9:
				flagMeioTransmissaoAlternativoRetorno = false;
				break;
			default:
				flagMeioTransmissaoAlternativoRetorno = true;
			}
		}
		if (saidaDetalhesPerfilTrocaArquivo.getCdMeioAltrnRetorno() != 0) {
			switch (saidaDetalhesPerfilTrocaArquivo.getCdMeioAltrnRetorno()) {
			case 7:
				flagMeioTransmissaoPrincipalRetorno = false;
				break;
			case 8:
				flagMeioTransmissaoPrincipalRetorno = false;
				break;
			case 9:
				flagMeioTransmissaoPrincipalRetorno = false;
				break;
			default:
				flagMeioTransmissaoPrincipalRetorno = true;
			}
		}

		if (saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRemessa() == 14 
			|| saidaDetalhesPerfilTrocaArquivo.getCdMeioAlternRemessa()  == 14 
			|| saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRetorno() == 14 
			|| saidaDetalhesPerfilTrocaArquivo.getCdMeioAltrnRetorno() == 14) 
		{
				flagMeioTransmissao14 = false;
		}

		if (flagMeioTransmissao14 == false) {
			setFlagMeioTransmissao(false);
			setFlagDadosControle(false);
			setFlagClienteSolicitante(false);
		} else if (flagMeioTransmissaoPrincipalRemessa == false
				|| flagMeioTransmissaoAlternativoRemessa == false
				|| flagMeioTransmissaoAlternativoRetorno == false
				|| flagMeioTransmissaoPrincipalRetorno == false) {
			setFlagMeioTransmissao(false);
			setFlagDadosControle(true);
			setFlagClienteSolicitante(false);
			limparCombosDadosControle();
		} else {
			setFlagMeioTransmissao(true);
			setFlagDadosControle(true);
			setFlagClienteSolicitante(true);
			limparCombosDadosControle();
			limparClienteSolicitante();
		}

		if (MEIO_TRANSMISSAO_WEB_TA.equals(saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRetorno())
				&& MEIO_TRANSMISSAO_WEB_TA.equals(saidaDetalhesPerfilTrocaArquivo.getCdMeioAltrnRetorno())
				&& MEIO_TRANSMISSAO_WEB_TA.equals(saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRemessa())
				&& MEIO_TRANSMISSAO_WEB_TA.equals(saidaDetalhesPerfilTrocaArquivo.getCdMeioAlternRemessa())) {
			saidaDetalhesPerfilTrocaArquivo.setPcCustoOrganizacaoTransmissao(null);
			setPercentualCliente(null);
		}
	}

	/**
	 * Habilitar filtro meio transmissao alterar.
	 */
	public void habilitarFiltroMeioTransmissaoAlterar() {
		boolean flagMeioTransmissaoPrincipalRemessa = true;
		boolean flagMeioTransmissaoAlternativoRemessa = true;
		boolean flagMeioTransmissaoAlternativoRetorno = true;
		boolean flagMeioTransmissaoPrincipalRetorno = true;
		boolean flagMeioTransmissao14 = true;
		setFlagDadosControle(true);
		setFlagClienteSolicitante(true);

		if (saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRemessa() != 0) {
			switch (saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRemessa()) {
			case 7:
				flagMeioTransmissaoPrincipalRemessa = false;
				break;
			case 8:
				flagMeioTransmissaoPrincipalRemessa = false;
				break;
			case 9:
				flagMeioTransmissaoPrincipalRemessa = false;
				break;
			default:
				flagMeioTransmissaoPrincipalRemessa = true;
			}
		}
		if (saidaDetalhesPerfilTrocaArquivo.getCdMeioAlternRemessa() != 0) {
			switch (saidaDetalhesPerfilTrocaArquivo.getCdMeioAlternRemessa()) {
			case 7:
				flagMeioTransmissaoAlternativoRemessa = false;
				break;
			case 8:
				flagMeioTransmissaoAlternativoRemessa = false;
				break;
			case 9:
				flagMeioTransmissaoAlternativoRemessa = false;
				break;
			default:
				flagMeioTransmissaoAlternativoRemessa = true;
			}
		}
		if (saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRetorno() != 0) {
			switch (saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRetorno()) {
			case 7:
				flagMeioTransmissaoAlternativoRetorno = false;
				break;
			case 8:
				flagMeioTransmissaoAlternativoRetorno = false;
				break;
			case 9:
				flagMeioTransmissaoAlternativoRetorno = false;
				break;
			default:
				flagMeioTransmissaoAlternativoRetorno = true;
			}
		}
		if (saidaDetalhesPerfilTrocaArquivo.getCdMeioAltrnRetorno() != 0) {
			switch (saidaDetalhesPerfilTrocaArquivo.getCdMeioAltrnRetorno()) {
			case 7:
				flagMeioTransmissaoPrincipalRetorno = false;
				break;
			case 8:
				flagMeioTransmissaoPrincipalRetorno = false;
				break;
			case 9:
				flagMeioTransmissaoPrincipalRetorno = false;
				break;
			default:
				flagMeioTransmissaoPrincipalRetorno = true;
			}
		}

		if (saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRemessa() == 14 
			|| saidaDetalhesPerfilTrocaArquivo.getCdMeioAlternRemessa()  == 14 
			|| saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRetorno() == 14 
			|| saidaDetalhesPerfilTrocaArquivo.getCdMeioAltrnRetorno() == 14) 
		{
				flagMeioTransmissao14 = false;
		}
		
				
		if(flagMeioTransmissao14 == false){
			setFlagMeioTransmissao(false);
			setFlagDadosControle(false);
			setFlagClienteSolicitante(false);
		}else if (flagMeioTransmissaoPrincipalRemessa == false 
				  || flagMeioTransmissaoAlternativoRemessa == false 
				  || flagMeioTransmissaoAlternativoRetorno == false 
				  || flagMeioTransmissaoPrincipalRetorno == false ) {
						setFlagMeioTransmissao(false);
						setFlagDadosControle(true);
						setFlagClienteSolicitante(false);
					}else{
						  setFlagMeioTransmissao(true);
						  setFlagDadosControle(true);
						  setFlagClienteSolicitante(true);
		}
	}

	/**
	 * Limpa combos apl format proprio.
	 */
	public void limpaCombosAplFormatProprio() {
		saidaDetalhesPerfilTrocaArquivo.setCdAplicacaoTransmPagamento(0L);

		if ("2".equals(rdoAplicFormProprio)) {
			saidaDetalhesPerfilTrocaArquivo.setCdAplicacaoTransmPagamento(2L);
		}
	}

	/**
	 * Limpar cliente solicitante.
	 */
	public void limparClienteSolicitante(){
		saidaDetalhesPerfilTrocaArquivo.setDsNomeContatoCliente(null);
		saidaDetalhesPerfilTrocaArquivo.setCdAreaFoneCliente(null);
		saidaDetalhesPerfilTrocaArquivo.setCdFoneContatoCliente(null);
		saidaDetalhesPerfilTrocaArquivo.setCdRamalContatoCliente(null);
		saidaDetalhesPerfilTrocaArquivo.setDsEmailContatoCliente(null);
		saidaDetalhesPerfilTrocaArquivo.setDsSolicitacaoPendente(null);
		saidaDetalhesPerfilTrocaArquivo.setCdAreaFonePend(null);
		saidaDetalhesPerfilTrocaArquivo.setCdFoneSolicitacaoPend(null);
		saidaDetalhesPerfilTrocaArquivo.setCdRamalSolctPend(null);
		saidaDetalhesPerfilTrocaArquivo.setDsEmailSolctPend(null);
		saidaDetalhesPerfilTrocaArquivo.setDsObsGeralPerfil(null);		
	}
	
	/**
	 * Listar empresa responsavel transmissao.
	 */
	private void listarEmpresaResponsavelTransmissao() {
		setListaEmpresaResponsavelTransmissao(new ArrayList<SelectItem>());
		setListaEmpresaResponsavelTransmissaoHash(new HashMap<Long, String>());

		List<ListarEmpresaTransmissaoArquivoVanSaidaDTO> lista = consultarEmpresaTransmissaoArquivoVan(50);

		for (ListarEmpresaTransmissaoArquivoVanSaidaDTO combo : lista) {
			getListaEmpresaResponsavelTransmissao().add(new SelectItem(combo.getCdTipoParticipacao(), combo.getDsTipoParticipacao()));
			getListaEmpresaResponsavelTransmissaoHash().put(combo.getCdTipoParticipacao(), combo.getDsTipoParticipacao());
		}
	}

	/**
	 * Consultar empresa transmissao arquivo van.
	 *
	 * @param numeroCombo the numero combo
	 * @return the list< listar empresa transmissao arquivo van saida dt o>
	 */
	private List<ListarEmpresaTransmissaoArquivoVanSaidaDTO> consultarEmpresaTransmissaoArquivoVan(Integer numeroCombo) {
		try {
			ListarEmpresaTransmissaoArquivoVanEntradaDTO entrada = new ListarEmpresaTransmissaoArquivoVanEntradaDTO();
			entrada.setNrOcorrencias(numeroCombo);
			entrada.setCdSituacaoVinculacaoConta(0);

			return getComboServiceImpl().consultarEmpresaTransmissaoArquivoVan(entrada);
		} catch (PdcAdapterFunctionalException p) {
			return new ArrayList<ListarEmpresaTransmissaoArquivoVanSaidaDTO>();
		}
	}

	/**
	 * Listar responsavel custo transmissaorquivo.
	 */
	public void listarResponsavelCustoTransmissaorquivo() {
		setListaResponsavelCustoTransArq(new ArrayList<SelectItem>());
		getListaResponsavelCustoTransArq().add(new SelectItem(1, MessageHelperUtils.getI18nMessage("label_organizacao_bradesco")));
		getListaResponsavelCustoTransArq().add(new SelectItem(2, MessageHelperUtils.getI18nMessage("label_cliente")));
		getListaResponsavelCustoTransArq().add(new SelectItem(3, MessageHelperUtils.getI18nMessage("label_rateio")));

		setListaResponsavelCustoTransArqHash(new HashMap<Integer, String>());
		getListaResponsavelCustoTransArqHash().put(1, MessageHelperUtils.getI18nMessage("label_organizacao_bradesco"));
		getListaResponsavelCustoTransArqHash().put(2, MessageHelperUtils.getI18nMessage("label_cliente"));
		getListaResponsavelCustoTransArqHash().put(3, MessageHelperUtils.getI18nMessage("label_rateio"));
	}

	/**
	 * Limpar percentual dados controle.
	 */
	public void limparPercentualDadosControle() {

		if(saidaDetalhesPerfilTrocaArquivo.getCdResponsavelCustoEmpresa() != 1 || saidaDetalhesPerfilTrocaArquivo.getCdResponsavelCustoEmpresa() != 3 ){
			saidaDetalhesPerfilTrocaArquivo.setCdUnidadeOrganizacional(null);
		}
		if(saidaDetalhesPerfilTrocaArquivo.getCdResponsavelCustoEmpresa() != 3){
			saidaDetalhesPerfilTrocaArquivo.setPcCustoOrganizacaoTransmissao(null);
		}
	}

	/**
	 * Calcular percentual cliente.
	 */
	public void calcularPercentualCliente() {
		BigDecimal pcCustoOrganizacaoTransmissao = saidaDetalhesPerfilTrocaArquivo.getPcCustoOrganizacaoTransmissao();

		if (pcCustoOrganizacaoTransmissao != null) {
			BigDecimal percentualDiferenca = new BigDecimal("100").subtract(pcCustoOrganizacaoTransmissao);

			setPercentualCliente(percentualDiferenca);
		} else {
			setPercentualCliente(null);
		}
	}

	/**
	 * Atribuir percentual banco cliente.
	 */
	public void atribuirPercentualBancoCliente() {
		Integer cdResponsavelCustoEmpresa = saidaDetalhesPerfilTrocaArquivo.getCdResponsavelCustoEmpresa();

		if (!Integer.valueOf(0).equals(cdResponsavelCustoEmpresa) && !Integer.valueOf(3).equals(cdResponsavelCustoEmpresa)) {
			BigDecimal percentualCem = new BigDecimal("100.00"), percentualZero = new BigDecimal("0.00");

			if (Integer.valueOf(1).equals(cdResponsavelCustoEmpresa)) {
				saidaDetalhesPerfilTrocaArquivo.setPcCustoOrganizacaoTransmissao(percentualCem);
				setPercentualCliente(percentualZero);
			} else if (Integer.valueOf(2).equals(cdResponsavelCustoEmpresa)) {
				saidaDetalhesPerfilTrocaArquivo.setPcCustoOrganizacaoTransmissao(percentualZero);
				setPercentualCliente(percentualCem);
			}
		} else {
			saidaDetalhesPerfilTrocaArquivo.setPcCustoOrganizacaoTransmissao(null);
			setPercentualCliente(null);
		}
	}

	/**
	 * Limpar combos dados controle.
	 */
	public void limparCombosDadosControle() {
		saidaDetalhesPerfilTrocaArquivo.setCdPessoaJuridicaParceiro(null);
		saidaDetalhesPerfilTrocaArquivo.setCdResponsavelCustoEmpresa(null);
		saidaDetalhesPerfilTrocaArquivo.setPcCustoOrganizacaoTransmissao(null);
		saidaDetalhesPerfilTrocaArquivo.setCdUnidadeOrganizacional(null);
		saidaDetalhesPerfilTrocaArquivo.setQtMesRegistroTrafg(null);
		setPercentualCliente(null);
	}

	/**
	 * Is desabilita custo transmissao.
	 *
	 * @return true, if is desabilita custo transmissao
	 */
	public boolean isDesabilitaCustoTransmissao() {
		Integer cdResponsavelCustoEmpresa = saidaDetalhesPerfilTrocaArquivo.getCdResponsavelCustoEmpresa();
		
		if (!Integer.valueOf(0).equals(cdResponsavelCustoEmpresa) && cdResponsavelCustoEmpresa != null){
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Is desabilita percentual banco.
	 *
	 * @return true, if is desabilita percentual banco
	 */
	public boolean isDesabilitaPercentualBanco() {
		Integer van = Integer.valueOf(14);
		Integer rvs = Integer.valueOf(7);
		Integer iterpel = Integer.valueOf(8);
		Integer conect = Integer.valueOf(9);

		Integer cboMeioTransmissaoPrincipalRemessa = saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRemessa();
		Integer cboMeioTransmissaoAlternativoRemessa = saidaDetalhesPerfilTrocaArquivo.getCdMeioAlternRemessa();
		Integer cboMeioTransmissaoAlternativoRetorno = saidaDetalhesPerfilTrocaArquivo.getCdMeioPrincipalRetorno();
		Integer cboMeioTransmissaoPrincipalRetorno = saidaDetalhesPerfilTrocaArquivo.getCdMeioAltrnRetorno();

		if(van.equals(cboMeioTransmissaoPrincipalRemessa) || van.equals(cboMeioTransmissaoAlternativoRemessa) 
				|| van.equals(cboMeioTransmissaoAlternativoRetorno) || van.equals(cboMeioTransmissaoPrincipalRetorno) ||
				rvs.equals(cboMeioTransmissaoPrincipalRemessa) || rvs.equals(cboMeioTransmissaoAlternativoRemessa) 
				|| rvs.equals(cboMeioTransmissaoAlternativoRetorno) || rvs.equals(cboMeioTransmissaoPrincipalRetorno) ||
				iterpel.equals(cboMeioTransmissaoPrincipalRemessa) || iterpel.equals(cboMeioTransmissaoAlternativoRemessa) 
				|| iterpel.equals(cboMeioTransmissaoAlternativoRetorno) || iterpel.equals(cboMeioTransmissaoPrincipalRetorno) ||
				conect.equals(cboMeioTransmissaoPrincipalRemessa) || conect.equals(cboMeioTransmissaoAlternativoRemessa) 
				|| conect.equals(cboMeioTransmissaoAlternativoRetorno) || conect.equals(cboMeioTransmissaoPrincipalRetorno)) {
			return !Integer.valueOf(3).equals(saidaDetalhesPerfilTrocaArquivo.getCdResponsavelCustoEmpresa());
		}

		return true;
	}

	// Gets e Sets
	/**
	 * Get: manterPerfilTrocaArqLayoutServiceImpl.
	 *
	 * @return manterPerfilTrocaArqLayoutServiceImpl
	 */
	public IManterPerfilTrocaArqLayoutService getManterPerfilTrocaArqLayoutServiceImpl() {
		return manterPerfilTrocaArqLayoutServiceImpl;
	}

	/**
	 * Set: manterPerfilTrocaArqLayoutServiceImpl.
	 *
	 * @param manterPerfilTrocaArqLayoutServiceImpl the manter perfil troca arq layout service impl
	 */
	public void setManterPerfilTrocaArqLayoutServiceImpl(IManterPerfilTrocaArqLayoutService manterPerfilTrocaArqLayoutServiceImpl) {
		this.manterPerfilTrocaArqLayoutServiceImpl = manterPerfilTrocaArqLayoutServiceImpl;
	}

	/**
	 * Get: saidaDetalhesPerfilTrocaArquivo.
	 *
	 * @return saidaDetalhesPerfilTrocaArquivo
	 */
	public DetalharPerfilTrocaArquivoSaidaDTO getSaidaDetalhesPerfilTrocaArquivo() {
		return saidaDetalhesPerfilTrocaArquivo;
	}

	/**
	 * Set: saidaDetalhesPerfilTrocaArquivo.
	 *
	 * @param saidaDetalhesPerfilTrocaArquivo the saida detalhes perfil troca arquivo
	 */
	public void setSaidaDetalhesPerfilTrocaArquivo(DetalharPerfilTrocaArquivoSaidaDTO saidaDetalhesPerfilTrocaArquivo) {
		this.saidaDetalhesPerfilTrocaArquivo = saidaDetalhesPerfilTrocaArquivo;
	}

	/**
	 * Get: listaMeioTransmissao.
	 *
	 * @return listaMeioTransmissao
	 */
	public ConsultarListaPerfilTrocaArquivoSaidaDTO getListaMeioTransmissao() {
		return listaMeioTransmissao;
	}

	/**
	 * Set: listaMeioTransmissao.
	 *
	 * @param listaMeioTransmissao the lista meio transmissao
	 */
	public void setListaMeioTransmissao(ConsultarListaPerfilTrocaArquivoSaidaDTO listaMeioTransmissao) {
		this.listaMeioTransmissao = listaMeioTransmissao;
	}

	/**
	 * Get: listaGridMeioTransmissao.
	 *
	 * @return listaGridMeioTransmissao
	 */
	public List<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO> getListaGridMeioTransmissao() {
		return listaGridMeioTransmissao;
	}

	/**
	 * Set: listaGridMeioTransmissao.
	 *
	 * @param listaGridMeioTransmissao the lista grid meio transmissao
	 */
	public void setListaGridMeioTransmissao(List<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO> listaGridMeioTransmissao) {
		this.listaGridMeioTransmissao = listaGridMeioTransmissao;
	}

	/**
	 * Get: listaControleMeioTransmissao.
	 *
	 * @return listaControleMeioTransmissao
	 */
	public List<SelectItem> getListaControleMeioTransmissao() {
		return listaControleMeioTransmissao;
	}

	/**
	 * Set: listaControleMeioTransmissao.
	 *
	 * @param listaControleMeioTransmissao the lista controle meio transmissao
	 */
	public void setListaControleMeioTransmissao(List<SelectItem> listaControleMeioTransmissao) {
		this.listaControleMeioTransmissao = listaControleMeioTransmissao;
	}

	/**
	 * Get: itemSelecionadoMeioTransmissao.
	 *
	 * @return itemSelecionadoMeioTransmissao
	 */
	public Integer getItemSelecionadoMeioTransmissao() {
		return itemSelecionadoMeioTransmissao;
	}

	/**
	 * Set: itemSelecionadoMeioTransmissao.
	 *
	 * @param itemSelecionadoMeioTransmissao the item selecionado meio transmissao
	 */
	public void setItemSelecionadoMeioTransmissao(Integer itemSelecionadoMeioTransmissao) {
		this.itemSelecionadoMeioTransmissao = itemSelecionadoMeioTransmissao;
	}

	/**
	 * Get: rdoAplicFormProprio.
	 *
	 * @return rdoAplicFormProprio
	 */
	public String getRdoAplicFormProprio() {
		return rdoAplicFormProprio;
	}

	/**
	 * Set: rdoAplicFormProprio.
	 *
	 * @param rdoAplicFormProprio the rdo aplic form proprio
	 */
	public void setRdoAplicFormProprio(String rdoAplicFormProprio) {
		this.rdoAplicFormProprio = rdoAplicFormProprio;
	}

	/**
	 * Get: listaAplicativoFormatacao.
	 *
	 * @return listaAplicativoFormatacao
	 */
	public List<SelectItem> getListaAplicativoFormatacao() {
		return listaAplicativoFormatacao;
	}

	/**
	 * Set: listaAplicativoFormatacao.
	 *
	 * @param listaAplicativoFormatacao the lista aplicativo formatacao
	 */
	public void setListaAplicativoFormatacao(List<SelectItem> listaAplicativoFormatacao) {
		this.listaAplicativoFormatacao = listaAplicativoFormatacao;
	}

	/**
	 * Get: listaAplicativoFormatacaoHash.
	 *
	 * @return listaAplicativoFormatacaoHash
	 */
	public Map<Long, String> getListaAplicativoFormatacaoHash() {
		return listaAplicativoFormatacaoHash;
	}

	/**
	 * Set lista aplicativo formatacao hash.
	 *
	 * @param listaAplicativoFormatacaoHash the lista aplicativo formatacao hash
	 */
	public void setListaAplicativoFormatacaoHash(Map<Long, String> listaAplicativoFormatacaoHash) {
		this.listaAplicativoFormatacaoHash = listaAplicativoFormatacaoHash;
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 *
	 * @return listaTipoLayoutArquivo
	 */
	public List<SelectItem> getListaTipoLayoutArquivo() {
		return listaTipoLayoutArquivo;
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 *
	 * @param listaTipoLayoutArquivo the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	/**
	 * Get: listaTipoLayoutArquivoHash.
	 *
	 * @return listaTipoLayoutArquivoHash
	 */
	public Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}

	/**
	 * Set lista tipo layout arquivo hash.
	 *
	 * @param listaTipoLayoutArquivoHash the lista tipo layout arquivo hash
	 */
	public void setListaTipoLayoutArquivoHash(Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}

	/**
	 * Get: comboServiceImpl.
	 *
	 * @return comboServiceImpl
	 */
	public IComboService getComboServiceImpl() {
		return comboServiceImpl;
	}

	/**
	 * Set: comboServiceImpl.
	 *
	 * @param comboServiceImpl the combo service impl
	 */
	public void setComboServiceImpl(IComboService comboServiceImpl) {
		this.comboServiceImpl = comboServiceImpl;
	}

	/**
	 * Get: listarMeioTransmissao.
	 *
	 * @return listarMeioTransmissao
	 */
	public List<SelectItem> getListarMeioTransmissao() {
		return listarMeioTransmissao;
	}

	/**
	 * Set: listarMeioTransmissao.
	 *
	 * @param listarMeioTransmissao the listar meio transmissao
	 */
	public void setListarMeioTransmissao(List<SelectItem> listarMeioTransmissao) {
		this.listarMeioTransmissao = listarMeioTransmissao;
	}

	/**
	 * Get: listarMeioTransmissaoHash.
	 *
	 * @return listarMeioTransmissaoHash
	 */
	public Map<Integer, String> getListarMeioTransmissaoHash() {
		return listarMeioTransmissaoHash;
	}

	/**
	 * Set listar meio transmissao hash.
	 *
	 * @param listarMeioTransmissaoHash the listar meio transmissao hash
	 */
	public void setListarMeioTransmissaoHash(Map<Integer, String> listarMeioTransmissaoHash) {
		this.listarMeioTransmissaoHash = listarMeioTransmissaoHash;
	}

	/**
	 * Is tela alteracao.
	 *
	 * @return true, if is tela alteracao
	 */
	public boolean isTelaAlteracao() {
		return telaAlteracao;
	}

	/**
	 * Set: telaAlteracao.
	 *
	 * @param telaAlteracao the tela alteracao
	 */
	public void setTelaAlteracao(boolean telaAlteracao) {
		this.telaAlteracao = telaAlteracao;
	}

	/**
	 * Get: contratoSelecionado.
	 *
	 * @return contratoSelecionado
	 */
	public ListarContratosPgitSaidaDTO getContratoSelecionado() {
		return contratoSelecionado;
	}

	/**
	 * Set: contratoSelecionado.
	 *
	 * @param contratoSelecionado the contrato selecionado
	 */
	public void setContratoSelecionado(ListarContratosPgitSaidaDTO contratoSelecionado) {
		this.contratoSelecionado = contratoSelecionado;
	}

	/**
	 * Get: rdoVan.
	 *
	 * @return rdoVan
	 */
	public String getRdoVan() {
		return rdoVan;
	}

	/**
	 * Set: rdoVan.
	 *
	 * @param rdoVan the rdo van
	 */
	public void setRdoVan(String rdoVan) {
		this.rdoVan = rdoVan;
	}

	/**
	 * Get: listaEmpresaResponsavelTransmissao.
	 *
	 * @return listaEmpresaResponsavelTransmissao
	 */
	public List<SelectItem> getListaEmpresaResponsavelTransmissao() {
		return listaEmpresaResponsavelTransmissao;
	}

	/**
	 * Set: listaEmpresaResponsavelTransmissao.
	 *
	 * @param listaEmpresaResponsavelTransmissao the lista empresa responsavel transmissao
	 */
	public void setListaEmpresaResponsavelTransmissao(List<SelectItem> listaEmpresaResponsavelTransmissao) {
		this.listaEmpresaResponsavelTransmissao = listaEmpresaResponsavelTransmissao;
	}

	/**
	 * Get: listaEmpresaResponsavelTransmissaoHash.
	 *
	 * @return listaEmpresaResponsavelTransmissaoHash
	 */
	public Map<Long, String> getListaEmpresaResponsavelTransmissaoHash() {
		return listaEmpresaResponsavelTransmissaoHash;
	}

	/**
	 * Set lista empresa responsavel transmissao hash.
	 *
	 * @param listaEmpresaResponsavelTransmissaoHash the lista empresa responsavel transmissao hash
	 */
	public void setListaEmpresaResponsavelTransmissaoHash(Map<Long, String> listaEmpresaResponsavelTransmissaoHash) {
		this.listaEmpresaResponsavelTransmissaoHash = listaEmpresaResponsavelTransmissaoHash;
	}

	/**
	 * Get: listaResponsavelCustoTransArq.
	 *
	 * @return listaResponsavelCustoTransArq
	 */
	public List<SelectItem> getListaResponsavelCustoTransArq() {
		return listaResponsavelCustoTransArq;
	}

	/**
	 * Set: listaResponsavelCustoTransArq.
	 *
	 * @param listaResponsavelCustoTransArq the lista responsavel custo trans arq
	 */
	public void setListaResponsavelCustoTransArq(List<SelectItem> listaResponsavelCustoTransArq) {
		this.listaResponsavelCustoTransArq = listaResponsavelCustoTransArq;
	}

	/**
	 * Get: listaResponsavelCustoTransArqHash.
	 *
	 * @return listaResponsavelCustoTransArqHash
	 */
	public Map<Integer, String> getListaResponsavelCustoTransArqHash() {
		return listaResponsavelCustoTransArqHash;
	}

	/**
	 * Set lista responsavel custo trans arq hash.
	 *
	 * @param listaResponsavelCustoTransArqHash the lista responsavel custo trans arq hash
	 */
	public void setListaResponsavelCustoTransArqHash(Map<Integer, String> listaResponsavelCustoTransArqHash) {
		this.listaResponsavelCustoTransArqHash = listaResponsavelCustoTransArqHash;
	}

	/**
	 * Is flag meio transmissao.
	 *
	 * @return true, if is flag meio transmissao
	 */
	public boolean isFlagMeioTransmissao() {
		return flagMeioTransmissao;
	}

	/**
	 * Set: flagMeioTransmissao.
	 *
	 * @param flagMeioTransmissao the flag meio transmissao
	 */
	public void setFlagMeioTransmissao(boolean flagMeioTransmissao) {
		this.flagMeioTransmissao = flagMeioTransmissao;
	}

	/**
	 * Get: manterVincPerfilTrocaArqPartImpl.
	 *
	 * @return manterVincPerfilTrocaArqPartImpl
	 */
	public IManterVincPerfilTrocaArqPartService getManterVincPerfilTrocaArqPartImpl() {
		return manterVincPerfilTrocaArqPartImpl;
	}

	/**
	 * Set: manterVincPerfilTrocaArqPartImpl.
	 *
	 * @param manterVincPerfilTrocaArqPartImpl the manter vinc perfil troca arq part impl
	 */
	public void setManterVincPerfilTrocaArqPartImpl(IManterVincPerfilTrocaArqPartService manterVincPerfilTrocaArqPartImpl) {
		this.manterVincPerfilTrocaArqPartImpl = manterVincPerfilTrocaArqPartImpl;
	}

	/**
	 * Get: listaGridParticipantesVinculados.
	 *
	 * @return listaGridParticipantesVinculados
	 */
	public List<ListarAssocTrocaArqParticipanteSaidaDTO> getListaGridParticipantesVinculados() {
		return listaGridParticipantesVinculados;
	}

	/**
	 * Set: listaGridParticipantesVinculados.
	 *
	 * @param listaGridParticipantesVinculados the lista grid participantes vinculados
	 */
	public void setListaGridParticipantesVinculados(List<ListarAssocTrocaArqParticipanteSaidaDTO> listaGridParticipantesVinculados) {
		this.listaGridParticipantesVinculados = listaGridParticipantesVinculados;
	}

	/**
	 * Get: dsLayoutProprio.
	 *
	 * @return dsLayoutProprio
	 */
	public String getDsLayoutProprio() {
		return dsLayoutProprio;
	}

	/**
	 * Set: dsLayoutProprio.
	 *
	 * @param dsLayoutProprio the ds layout proprio
	 */
	public void setDsLayoutProprio(String dsLayoutProprio) {
		this.dsLayoutProprio = dsLayoutProprio;
	}

	/**
	 * Is flag dados controle.
	 *
	 * @return true, if is flag dados controle
	 */
	public boolean isFlagDadosControle() {
		return flagDadosControle;
	}

	/**
	 * Set: flagDadosControle.
	 *
	 * @param flagDadosControle the flag dados controle
	 */
	public void setFlagDadosControle(boolean flagDadosControle) {
		this.flagDadosControle = flagDadosControle;
	}

	/**
	 * Is flag cliente solicitante.
	 *
	 * @return true, if is flag cliente solicitante
	 */
	public boolean isFlagClienteSolicitante() {
		return flagClienteSolicitante;
	}

	/**
	 * Set: flagClienteSolicitante.
	 *
	 * @param flagClienteSolicitante the flag cliente solicitante
	 */
	public void setFlagClienteSolicitante(boolean flagClienteSolicitante) {
		this.flagClienteSolicitante = flagClienteSolicitante;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: percentualCliente.
	 *
	 * @return percentualCliente
	 */
	public BigDecimal getPercentualCliente() {
		return percentualCliente;
	}

	/**
	 * Set: percentualCliente.
	 *
	 * @param percentualCliente the percentual cliente
	 */
	public void setPercentualCliente(BigDecimal percentualCliente) {
		this.percentualCliente = percentualCliente;
	}

	/**
	 * Get: listaGridParticipante.
	 *
	 * @return listaGridParticipante
	 */
	public List<ListarParticipanteSaidaDTO> getListaGridParticipante() {
		return listaGridParticipante;
	}

	/**
	 * Set: listaGridParticipante.
	 *
	 * @param listaGridParticipante the lista grid participante
	 */
	public void setListaGridParticipante(
			List<ListarParticipanteSaidaDTO> listaGridParticipante) {
		this.listaGridParticipante = listaGridParticipante;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * @param dsAplicFormat the dsAplicFormat to set
	 */
	public void setDsAplicFormat(String dsAplicFormat) {
		this.dsAplicFormat = dsAplicFormat;
	}

	/**
	 * @return the dsAplicFormat
	 */
	public String getDsAplicFormat() {
		return dsAplicFormat;
	}

}