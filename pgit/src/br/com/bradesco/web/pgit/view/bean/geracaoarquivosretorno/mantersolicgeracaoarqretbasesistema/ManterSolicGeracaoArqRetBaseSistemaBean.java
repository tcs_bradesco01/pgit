/**
 * Nome: br.com.bradesco.web.pgit.view.bean.geracaoarquivosretorno.mantersolicgeracaoarqretbasesistema
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.view.bean.geracaoarquivosretorno.mantersolicgeracaoarqretbasesistema;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.IManterSolicGeracaoArqRetBaseSistemaService;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.DetalharSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.DetalharSolBaseSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ExcluirSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ExcluirSolBaseSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.IncluirSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.IncluirSolBaseSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ListarSolBaseSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasesistema.bean.ListarSolBaseSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;

/**
 * Nome: ManterSolicGeracaoArqRetBaseSistemaBean
 * <p>
 * Prop�sito: Implementa��o do Bean ManterSolicGeracaoArqRetBaseSistemaBean
 * </p>
 * 
 * @author todo! - Solu��es em Tecnologia / TI Melhorias - Arquitetura
 * @version 1.0
 */
public class ManterSolicGeracaoArqRetBaseSistemaBean {

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo consultasService. */
	private IConsultasService consultasService;

	// Variaveis Consulta
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;
	
	/** Atributo solEmissaoComprovantePagtoClientePagServiceImpl. */
	private ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	/** Atributo manterSolicGeracaoArqRetBaseSistemaServiceImpl. */
	private IManterSolicGeracaoArqRetBaseSistemaService manterSolicGeracaoArqRetBaseSistemaServiceImpl;
	
	/** Atributo itemSelecionadoListaConsultar. */
	private Integer itemSelecionadoListaConsultar;
	
	/** Atributo cboSituacaoSolicitacao. */
	private Integer cboSituacaoSolicitacao;
	
	/** Atributo cboOrigemSolicitacao. */
	private Integer cboOrigemSolicitacao;
	
	/** Atributo dataSolicitacaoDe. */
	private Date dataSolicitacaoDe;
	
	/** Atributo dataSolicitacaoAte. */
	private Date dataSolicitacaoAte;
	
	/** Atributo numeroSolicitacao. */
	private Integer numeroSolicitacao;
	
	/** Atributo listaSituacaoSolicitacao. */
	private List<SelectItem> listaSituacaoSolicitacao;
	
	/** Atributo radioGridConsultar. */
	private List<SelectItem> radioGridConsultar;
	
	/** Atributo listaGridConsultar. */
	private List<ListarSolBaseSistemaSaidaDTO> listaGridConsultar;
	
	/** Atributo manterSolicitacaoRastFavService. */
	private IManterSolicitacaoRastFavService manterSolicitacaoRastFavService;
	
	/** Atributo atributoSoltc. */
	private VerificaraAtributoSoltcSaidaDTO atributoSoltc;

	// Variaveis Detalhar

	/** Atributo codMensagem. */
	private String codMensagem;
	
	/** Atributo mensagem. */
	private String mensagem;
	
	/** Atributo nrSolicitacaoPagamento. */
	private String nrSolicitacaoPagamento;
	
	/** Atributo hrSolicitacao. */
	private String hrSolicitacao;
	
	/** Atributo dsSituacaoSolicitacao. */
	private String dsSituacaoSolicitacao;
	
	/** Atributo resultadoProcessamento. */
	private String resultadoProcessamento;
	
	/** Atributo hrAtendimentoSolicitacao. */
	private String hrAtendimentoSolicitacao;
	
	/** Atributo qtdeRegistrosSolicitacao. */
	private String qtdeRegistrosSolicitacao;
	
	/** Atributo cdBaseSistema. */
	private String cdBaseSistema;
	
	/** Atributo dsBaseSistema. */
	private String dsBaseSistema;
	
	/** Atributo taxaBonificacao. */
	private String taxaBonificacao;

	/** Atributo cdCanalInclusao. */
	private String cdCanalInclusao;
	
	/** Atributo dsCanalInclusao. */
	private String dsCanalInclusao;
	
	/** Atributo cdAutenticacaoSegurancaInclusao. */
	private String cdAutenticacaoSegurancaInclusao;
	
	/** Atributo nmOperacaoFluxoInlcusao. */
	private String nmOperacaoFluxoInlcusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo cdCanalManutencao. */
	private String cdCanalManutencao;
	
	/** Atributo dsCanalManutencao. */
	private String dsCanalManutencao;
	
	/** Atributo cdAutenticacaoSegurancaManutencao. */
	private String cdAutenticacaoSegurancaManutencao;
	
	/** Atributo nmOperacaoFluxoManutencao. */
	private String nmOperacaoFluxoManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo nroContrato. */
	private String nroContrato;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo vlTarifaPadrao. */
	private BigDecimal vlTarifaPadrao;
	
	/** Atributo vlDescontoTarifa. */
	private BigDecimal vlDescontoTarifa;
	
	/** Atributo vlTarifaAtualizada. */
	private BigDecimal vlTarifaAtualizada;

	/** Atributo cboTipoSistema. */
	private Integer cboTipoSistema;
	
	/** Atributo listarTipoSistema. */
	private List<SelectItem> listarTipoSistema;
	
	/** Atributo listaBaseSistemaHash. */
	private Map<Integer, String> listaBaseSistemaHash = new HashMap<Integer, String>();

	/** Atributo tipoSistema. */
	private String tipoSistema;
	// Metodos

	// Variaveis Trilha Auditoria
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo indicadorDescontoBloqueio. */
	private boolean indicadorDescontoBloqueio;

	/**
	 * Nome: preencheDados
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void preencheDados() {

		carregaCabecalho();

		try {
			DetalharSolBaseSistemaEntradaDTO entradaDTO = new DetalharSolBaseSistemaEntradaDTO();

			entradaDTO.setNrSolicitacaoPagamento(getListaGridConsultar().get(
					getItemSelecionadoListaConsultar())
					.getNrSolicitacaoPagamento());

			DetalharSolBaseSistemaSaidaDTO saidaDTO = getManterSolicGeracaoArqRetBaseSistemaServiceImpl()
					.detalharSolBaseSistema(entradaDTO);

			setCdBaseSistema(String.valueOf(saidaDTO.getCdBaseSistema()));
			setCodMensagem(saidaDTO.getCodMensagem());
			setDsBaseSistema(saidaDTO.getDsBaseSistema());
			setDsCanalManutencao(saidaDTO.getDsCanalManutencao());
			setDsSituacaoSolicitacao(saidaDTO.getDsSituacaoSolicitacao());
			setMensagem(saidaDTO.getMensagem());
			setNrSolicitacaoPagamento(String.valueOf(saidaDTO
					.getNrSolicitacaoPagamento()));
			setQtdeRegistrosSolicitacao(String.valueOf(saidaDTO
					.getQtdeRegistrosSolicitacao()));
			setResultadoProcessamento(saidaDTO.getResultadoProcessamento());
			setHrSolicitacao(saidaDTO.getHrSolicitacaoFormatada());
			setHrInclusaoRegistro(saidaDTO.getHrInclusaoRegistroFormatada());
			setHrAtendimentoSolicitacao(saidaDTO
					.getHrAtendimentoSolicitacaoFormatada());

			setVlTarifaPadrao(saidaDTO.getVlTarifaPadrao());
			setVlDescontoTarifa(saidaDTO.getNumPercentualDescTarifa());
			setVlTarifaAtualizada(saidaDTO.getVlrTarifaAtual());

			setDataHoraInclusao(saidaDTO.getHrInclusaoRegistroFormatada());
			setUsuarioInclusao(saidaDTO.getCdAutenticacaoSegurancaInclusao());
			setTipoCanalInclusao((saidaDTO.getCdCanalInclusao() == 0 ? ""
					: saidaDTO.getCdCanalInclusao() + " - "
							+ saidaDTO.getDsCanalInclusao()));
			setComplementoInclusao(saidaDTO.getNmOperacaoFluxoInlcusao() == null
					|| saidaDTO.getNmOperacaoFluxoInlcusao().equals("0") ? ""
					: saidaDTO.getNmOperacaoFluxoInlcusao());
			setDataHoraManutencao(saidaDTO.getHrManutencaoRegistroFormatada());
			setUsuarioManutencao(saidaDTO
					.getCdAutenticacaoSegurancaManutencao());
			setTipoCanalManutencao((saidaDTO.getCdCanalManutencao() == 0 ? ""
					: saidaDTO.getCdCanalManutencao() + " - "
							+ saidaDTO.getCdCanalManutencao()));
			setComplementoManutencao(saidaDTO.getNmOperacaoFluxoManutencao() == null
					|| saidaDTO.getNmOperacaoFluxoManutencao().equals("0") ? ""
					: saidaDTO.getNmOperacaoFluxoManutencao());

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Nome: carregaTipoSistema
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void carregaTipoSistema() {

		try {
			listarTipoSistema = new ArrayList<SelectItem>();

			List<TipoSistemaSaidaDTO> listaSaida = comboService
					.listarTipoSistema();
			listaBaseSistemaHash.clear();

			for (int i = 0; i < listaSaida.size(); i++) {
				listaBaseSistemaHash.put(listaSaida.get(i).getCdBaseSistema(),
						listaSaida.get(i).getDsBaseSistema());
				listarTipoSistema.add(new SelectItem(listaSaida.get(i)
						.getCdBaseSistema(), listaSaida.get(i)
						.getDsBaseSistema()));
			}

		} catch (PdcAdapterFunctionalException p) {
			listarTipoSistema = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Nome: voltarConsulta
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String voltarConsulta() {
		setItemSelecionadoListaConsultar(null);

		return "VOLTAR_CONSULTA";
	}

	/**
	 * Nome: detalhar
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String detalhar() {
		preencheDados();

		return "DETALHAR";
	}

	/**
	 * Nome: excluir
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String excluir() {
		preencheDados();

		return "EXCLUIR";
	}

	/**
	 * Nome: limpar
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String limpar() {

		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
		setNumeroSolicitacao(null);
		setCboSituacaoSolicitacao(null);
		setCboOrigemSolicitacao(null);

		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();

		setListaGridConsultar(null);
		setItemSelecionadoListaConsultar(null);
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Nome: limparGridConsultar
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String limparGridConsultar() {
		setListaGridConsultar(null);
		setItemSelecionadoListaConsultar(null);
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Nome: avancarIncluir
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String avancarIncluir() {
		setTipoSistema((String) listaBaseSistemaHash.get(getCboTipoSistema()));
		calcularTarifaAtualizada();

		return "AVANCAR_INCLUIR";
	}

	/**
	 * Nome: carregaCabecalho
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void carregaCabecalho() {
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");

		if (filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado()
				.equals("0")) { // Filtrar por cliente
			setNrCnpjCpf(String.valueOf(filtroAgendamentoEfetivacaoEstornoBean
					.getNrCpfCnpjCliente()));
			setDsRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoRazaoSocialCliente());
			setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
					.getEmpresaGestoraDescCliente());
			setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getNumeroDescCliente());
			setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoContratoDescCliente());
			setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getSituacaoDescCliente());
		} else {
			if (filtroAgendamentoEfetivacaoEstornoBean
					.getTipoFiltroSelecionado().equals("1")) { // Filtrar por
				// contrato
				try {
					DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
					entradaDTO
							.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
									.getEmpresaGestoraFiltro());
					entradaDTO
							.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
									.getCdTipoContratoNegocio());
					entradaDTO
							.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
									.getNrSequenciaContratoNegocio());
					DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService()
							.detalharDadosContrato(entradaDTO);
					setNrCnpjCpf(saidaDTO.getCdCnpjCpfTitular());
					setDsRazaoSocial(saidaDTO.getDsParticipanteTitular());
				} catch (PdcAdapterFunctionalException p) {
					setNrCnpjCpf("");
					setDsRazaoSocial("");
				}
				setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
						.getEmpresaGestoraDescContrato());
				setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getNumeroDescContrato());
				setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getDescricaoContratoDescContrato());
				setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getSituacaoDescContrato());
			}
		}
	}

	/**
	 * Nome: consultar
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void consultar(ActionEvent evt) {
		consultar();
	}

	/**
	 * Nome: consultar
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void consultar() {
		setItemSelecionadoListaConsultar(null);
		setRadioGridConsultar(null);
		listaGridConsultar = new ArrayList<ListarSolBaseSistemaSaidaDTO>();
		try {
			ListarSolBaseSistemaEntradaDTO entrada = new ListarSolBaseSistemaEntradaDTO();
			entrada
					.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdPessoaJuridicaContrato());
			entrada
					.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdTipoContratoNegocio());
			entrada
					.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrSequenciaContratoNegocio());
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			entrada
					.setDtSolicitacaoInicio((getDataSolicitacaoDe() == null) ? ""
							: sdf.format(getDataSolicitacaoDe()));
			entrada.setDtSolicitacaoFim((getDataSolicitacaoAte() == null) ? ""
					: sdf.format(getDataSolicitacaoAte()));
			entrada.setNrSolicitacaoPagamento(getNumeroSolicitacao());
			entrada.setCdSituacaoSolicitacao(getCboSituacaoSolicitacao());
			entrada.setCdorigemSolicitacao(getCboOrigemSolicitacao());

			setListaGridConsultar(getManterSolicGeracaoArqRetBaseSistemaServiceImpl()
					.listarSolBaseSistema(entrada));

			radioGridConsultar = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridConsultar().size(); i++) {
				this.radioGridConsultar.add(new SelectItem(i, " "));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridConsultar(null);
			setItemSelecionadoListaConsultar(null);
			setDisableArgumentosConsulta(false);
		}
	}

	/**
	 * Nome: confirmarExclusao
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void confirmarExclusao() {
		try {
			ExcluirSolBaseSistemaEntradaDTO entrada = new ExcluirSolBaseSistemaEntradaDTO();

			entrada.setNrSolicitacaoGeracaoRetorno(getListaGridConsultar().get(
					getItemSelecionadoListaConsultar())
					.getNrSolicitacaoPagamento());
			entrada
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entrada
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entrada
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());

			ExcluirSolBaseSistemaSaidaDTO saida = getManterSolicGeracaoArqRetBaseSistemaServiceImpl()
					.excluirSolBaseSistema(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(),
					"conManterSolicGeracaoArqRetBaseSistema",
					"#{manterSolicGeracaoArqRetBaseSistemaBean.consultar}",
					false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

	}

	/**
	 * Nome: confirmarIncluir
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void confirmarIncluir() {
		try {

			IncluirSolBaseSistemaEntradaDTO entrada = new IncluirSolBaseSistemaEntradaDTO();

			entrada
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entrada
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entrada
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entrada.setCdBaseSistema(getCboTipoSistema());
			entrada.setVlTarifaPadrao(vlTarifaPadrao);
			entrada.setNumPercentualDescTarifa(vlDescontoTarifa);
			entrada.setVlrTarifaAtual(vlTarifaAtualizada);

			IncluirSolBaseSistemaSaidaDTO saida = getManterSolicGeracaoArqRetBaseSistemaServiceImpl()
					.incluirSolBaseSistema(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(),
					"conManterSolicGeracaoArqRetBaseSistema",
					BradescoViewExceptionActionType.ACTION, false);

			if (getListaGridConsultar() != null
					&& getListaGridConsultar().size() > 0) {
				consultar();
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Nome: incluir
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String incluir() {
		vlTarifaPadrao = BigDecimal.ZERO;
		atributoSoltc = getManterSolicitacaoRastFavService()
				.verificaraAtributoSoltc(
						new VerificaraAtributoSoltcEntradaDTO(4));

		try {
			carregaCabecalho();
			carregaTipoSistema();
			consultarAgenciaOpeTarifaPadrao();
			limparRadiosPrincipaisIncluir();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}

		return "INCLUIR";
	}
	
	public boolean isDesabilitaCampoDescontoTarifa(){		
		
		if (indicadorDescontoBloqueio) {
			setVlDescontoTarifa(new BigDecimal(0));
			return true;
		}		
		
		return false;
	}

	/**
	 * Nome: calcularTarifaAtualizada
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void calcularTarifaAtualizada() {
		vlTarifaAtualizada = vlTarifaPadrao.subtract(vlTarifaPadrao.multiply(
				vlDescontoTarifa.divide(new BigDecimal(100))).setScale(2,
				BigDecimal.ROUND_HALF_UP));
	}

	/**
	 * Nome: limparAtributos
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void limparAtributos() {

		setNrCnpjCpf(null);
		setDsRazaoSocial(null);
		setDsEmpresa(null);
		setNroContrato(null);
		setDsContrato(null);
		setCdSituacaoContrato(null);

		setVlTarifaPadrao(null);
		setVlDescontoTarifa(null);
		setVlTarifaAtualizada(null);
		setNumeroSolicitacao(null);
		setHrSolicitacao(null);
		setDsSituacaoSolicitacao(null);
		setHrAtendimentoSolicitacao(null);
		setResultadoProcessamento(null);
		setQtdeRegistrosSolicitacao(null);

		setDataHoraInclusao(null);
		setUsuarioInclusao(null);
		setTipoCanalInclusao(null);
		setComplementoInclusao(null);

		setDataHoraManutencao(null);
		setUsuarioManutencao(null);
		setTipoCanalManutencao(null);
		setComplementoManutencao(null);
	}

	/**
	 * Nome: inicializaTela
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void inicializaTela(ActionEvent evt) {// metodo utilizado como action
		// inicial
		filtroAgendamentoEfetivacaoEstornoBean
				.setClienteContratoSelecionado(false);
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());

		setItemSelecionadoListaConsultar(null);
		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);
		// ////////////carregaListaSituacaoSolicitacao();
		// limpar todos os campos da tela

		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setPaginaRetorno("conManterSolicGeracaoArqRetBaseSistema");
		filtroAgendamentoEfetivacaoEstornoBean
				.setEmpresaGestoraFiltro(2269651L);

		carregaListaSituacaoSolicitacao();
		setDisableArgumentosConsulta(false);

	}

	/**
	 * Nome: carregaListaSituacaoSolicitacao
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void carregaListaSituacaoSolicitacao() {

		try {
			listaSituacaoSolicitacao = new ArrayList<SelectItem>();

			List<ListarSituacaoSolicitacaoSaidaDTO> listaSaida = comboService
					.listarSituacaoSolicitacao();

			for (int i = 0; i < listaSaida.size(); i++) {
				listaSituacaoSolicitacao.add(new SelectItem(listaSaida.get(i)
						.getCdSolicitacaoPagamentoIntegrado(), listaSaida
						.get(i).getDsTipoSolicitacaoPagamento()));
			}

		} catch (PdcAdapterFunctionalException p) {
			listaSituacaoSolicitacao = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Nome: limparRadiosPrincipais
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void limparRadiosPrincipais() {
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		setHabilitaArgumentosPesquisa(false);
		setListaGridConsultar(null);
		setItemSelecionadoListaConsultar(null);

		limparCamposConsultar();
	}

	/**
	 * Nome: limparAposAlterarOpcaoCliente
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		setListaGridConsultar(null);
		setItemSelecionadoListaConsultar(null);
		setHabilitaArgumentosPesquisa(false);

		limparCamposConsultar();

		return "";

	}

	/**
	 * Nome: limparRadiosPrincipaisIncluir
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void limparRadiosPrincipaisIncluir() {
		vlDescontoTarifa = BigDecimal.ZERO;

		if (indicadorDescontoBloqueio) {
			vlTarifaAtualizada = vlTarifaPadrao;
		} else {
			vlTarifaAtualizada = BigDecimal.ZERO;
		}

		setCboTipoSistema(0);
	}

	/**
	 * Nome: limparAposAlterarOpcaoClienteIncluir
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String limparAposAlterarOpcaoClienteIncluir() {
		// Limpar Argumentos de Pesquisa e Lista
		setCboTipoSistema(0);

		return "";

	}

	/**
	 * Nome: limparArgsListaAposPesquisaClienteContratoConsultar
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void limparArgsListaAposPesquisaClienteContratoConsultar(
			ActionEvent evt) {
		limparCamposConsultar();
		setListaGridConsultar(null);
		setItemSelecionadoListaConsultar(null);
	}

	/**
	 * Nome: limparArgsListaAposPesquisaClienteContratoConsultarIncluir
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void limparArgsListaAposPesquisaClienteContratoConsultarIncluir(
			ActionEvent evt) {
		setCboTipoSistema(0);
	}

	/**
	 * Nome: limparCamposConsultar
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void limparCamposConsultar() {

		setItemSelecionadoListaConsultar(null);
		setCboSituacaoSolicitacao(0);
		// setListaGridConsultar(null);
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
	}

	/**
	 * Nome: pesquisar
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String pesquisar(ActionEvent evt) {
		consultar();
		return "";
	}

	/**
	 * Nome: consultarAgenciaOpeTarifaPadrao
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void consultarAgenciaOpeTarifaPadrao() {
		try {
			ConsultarAgenciaOpeTarifaPadraoEntradaDTO entradaDTO = new ConsultarAgenciaOpeTarifaPadraoEntradaDTO();

			entradaDTO
					.setCdPessoaJuridicaEmpresa(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entradaDTO.setCdProdutoServicoOperacao(0);
			entradaDTO.setCdProdutoServicoRelacionado(0);
			entradaDTO.setCdTipoTarifa(10);

			ConsultarAgenciaOpeTarifaPadraoSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl()
					.consultarAgenciaOpeTarifaPadrao(entradaDTO);

			indicadorDescontoBloqueio = "N".equals(saidaDTO
					.getCdIndicadorDescontoBloqueio());

			setVlTarifaPadrao(saidaDTO.getVlTarifaPadrao());
		} catch (PdcAdapterFunctionalException p) {
			throw p;
		}
	}

	/**
	 * Nome: voltarIncluir
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String voltarIncluir() {
		return "VOLTAR_INCLUIR";
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}

	/**
	 * Get: manterSolicGeracaoArqRetBaseSistemaServiceImpl.
	 *
	 * @return manterSolicGeracaoArqRetBaseSistemaServiceImpl
	 */
	public IManterSolicGeracaoArqRetBaseSistemaService getManterSolicGeracaoArqRetBaseSistemaServiceImpl() {
		return manterSolicGeracaoArqRetBaseSistemaServiceImpl;
	}

	/**
	 * Set: manterSolicGeracaoArqRetBaseSistemaServiceImpl.
	 *
	 * @param manterSolicGeracaoArqRetBaseSistemaServiceImpl the manter solic geracao arq ret base sistema service impl
	 */
	public void setManterSolicGeracaoArqRetBaseSistemaServiceImpl(
			IManterSolicGeracaoArqRetBaseSistemaService manterSolicGeracaoArqRetBaseSistemaServiceImpl) {
		this.manterSolicGeracaoArqRetBaseSistemaServiceImpl = manterSolicGeracaoArqRetBaseSistemaServiceImpl;
	}

	/**
	 * Get: cboSituacaoSolicitacao.
	 *
	 * @return cboSituacaoSolicitacao
	 */
	public Integer getCboSituacaoSolicitacao() {
		return cboSituacaoSolicitacao;
	}

	/**
	 * Set: cboSituacaoSolicitacao.
	 *
	 * @param cboSituacaoSolicitacao the cbo situacao solicitacao
	 */
	public void setCboSituacaoSolicitacao(Integer cboSituacaoSolicitacao) {
		this.cboSituacaoSolicitacao = cboSituacaoSolicitacao;
	}

	/**
	 * Get: dataSolicitacaoAte.
	 *
	 * @return dataSolicitacaoAte
	 */
	public Date getDataSolicitacaoAte() {
		return dataSolicitacaoAte;
	}

	/**
	 * Set: dataSolicitacaoAte.
	 *
	 * @param dataSolicitacaoAte the data solicitacao ate
	 */
	public void setDataSolicitacaoAte(Date dataSolicitacaoAte) {
		this.dataSolicitacaoAte = dataSolicitacaoAte;
	}

	/**
	 * Get: dataSolicitacaoDe.
	 *
	 * @return dataSolicitacaoDe
	 */
	public Date getDataSolicitacaoDe() {
		return dataSolicitacaoDe;
	}

	/**
	 * Set: dataSolicitacaoDe.
	 *
	 * @param dataSolicitacaoDe the data solicitacao de
	 */
	public void setDataSolicitacaoDe(Date dataSolicitacaoDe) {
		this.dataSolicitacaoDe = dataSolicitacaoDe;
	}

	/**
	 * Get: itemSelecionadoListaConsultar.
	 *
	 * @return itemSelecionadoListaConsultar
	 */
	public Integer getItemSelecionadoListaConsultar() {
		return itemSelecionadoListaConsultar;
	}

	/**
	 * Set: itemSelecionadoListaConsultar.
	 *
	 * @param itemSelecionadoListaConsultar the item selecionado lista consultar
	 */
	public void setItemSelecionadoListaConsultar(
			Integer itemSelecionadoListaConsultar) {
		this.itemSelecionadoListaConsultar = itemSelecionadoListaConsultar;
	}

	/**
	 * Get: numeroSolicitacao.
	 *
	 * @return numeroSolicitacao
	 */
	public Integer getNumeroSolicitacao() {
		return numeroSolicitacao;
	}

	/**
	 * Set: numeroSolicitacao.
	 *
	 * @param numeroSolicitacao the numero solicitacao
	 */
	public void setNumeroSolicitacao(Integer numeroSolicitacao) {
		this.numeroSolicitacao = numeroSolicitacao;
	}

	/**
	 * Get: cboOrigemSolicitacao.
	 *
	 * @return cboOrigemSolicitacao
	 */
	public Integer getCboOrigemSolicitacao() {
		return cboOrigemSolicitacao;
	}

	/**
	 * Set: cboOrigemSolicitacao.
	 *
	 * @param cboOrigemSolicitacao the cbo origem solicitacao
	 */
	public void setCboOrigemSolicitacao(Integer cboOrigemSolicitacao) {
		this.cboOrigemSolicitacao = cboOrigemSolicitacao;
	}

	/**
	 * Get: listaSituacaoSolicitacao.
	 *
	 * @return listaSituacaoSolicitacao
	 */
	public List<SelectItem> getListaSituacaoSolicitacao() {
		return listaSituacaoSolicitacao;
	}

	/**
	 * Set: listaSituacaoSolicitacao.
	 *
	 * @param listaSituacaoSolicitacao the lista situacao solicitacao
	 */
	public void setListaSituacaoSolicitacao(
			List<SelectItem> listaSituacaoSolicitacao) {
		this.listaSituacaoSolicitacao = listaSituacaoSolicitacao;
	}

	/**
	 * Get: listaGridConsultar.
	 *
	 * @return listaGridConsultar
	 */
	public List<ListarSolBaseSistemaSaidaDTO> getListaGridConsultar() {
		return listaGridConsultar;
	}

	/**
	 * Get: radioGridConsultar.
	 *
	 * @return radioGridConsultar
	 */
	public List<SelectItem> getRadioGridConsultar() {
		return radioGridConsultar;
	}

	/**
	 * Set: radioGridConsultar.
	 *
	 * @param radioGridConsultar the radio grid consultar
	 */
	public void setRadioGridConsultar(List<SelectItem> radioGridConsultar) {
		this.radioGridConsultar = radioGridConsultar;
	}

	/**
	 * Set: listaGridConsultar.
	 *
	 * @param listaGridConsultar the lista grid consultar
	 */
	public void setListaGridConsultar(
			List<ListarSolBaseSistemaSaidaDTO> listaGridConsultar) {
		this.listaGridConsultar = listaGridConsultar;
	}

	/**
	 * Get: cdAutenticacaoSegurancaInclusao.
	 *
	 * @return cdAutenticacaoSegurancaInclusao
	 */
	public String getCdAutenticacaoSegurancaInclusao() {
		return cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaInclusao.
	 *
	 * @param cdAutenticacaoSegurancaInclusao the cd autenticacao seguranca inclusao
	 */
	public void setCdAutenticacaoSegurancaInclusao(
			String cdAutenticacaoSegurancaInclusao) {
		this.cdAutenticacaoSegurancaInclusao = cdAutenticacaoSegurancaInclusao;
	}

	/**
	 * Get: cdAutenticacaoSegurancaManutencao.
	 *
	 * @return cdAutenticacaoSegurancaManutencao
	 */
	public String getCdAutenticacaoSegurancaManutencao() {
		return cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Set: cdAutenticacaoSegurancaManutencao.
	 *
	 * @param cdAutenticacaoSegurancaManutencao the cd autenticacao seguranca manutencao
	 */
	public void setCdAutenticacaoSegurancaManutencao(
			String cdAutenticacaoSegurancaManutencao) {
		this.cdAutenticacaoSegurancaManutencao = cdAutenticacaoSegurancaManutencao;
	}

	/**
	 * Get: cdBaseSistema.
	 *
	 * @return cdBaseSistema
	 */
	public String getCdBaseSistema() {
		return cdBaseSistema;
	}

	/**
	 * Set: cdBaseSistema.
	 *
	 * @param cdBaseSistema the cd base sistema
	 */
	public void setCdBaseSistema(String cdBaseSistema) {
		this.cdBaseSistema = cdBaseSistema;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public String getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(String cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public String getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(String cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: codMensagem.
	 *
	 * @return codMensagem
	 */
	public String getCodMensagem() {
		return codMensagem;
	}

	/**
	 * Set: codMensagem.
	 *
	 * @param codMensagem the cod mensagem
	 */
	public void setCodMensagem(String codMensagem) {
		this.codMensagem = codMensagem;
	}

	/**
	 * Get: dsBaseSistema.
	 *
	 * @return dsBaseSistema
	 */
	public String getDsBaseSistema() {
		return dsBaseSistema;
	}

	/**
	 * Set: dsBaseSistema.
	 *
	 * @param dsBaseSistema the ds base sistema
	 */
	public void setDsBaseSistema(String dsBaseSistema) {
		this.dsBaseSistema = dsBaseSistema;
	}

	/**
	 * Get: dsCanalInclusao.
	 *
	 * @return dsCanalInclusao
	 */
	public String getDsCanalInclusao() {
		return dsCanalInclusao;
	}

	/**
	 * Set: dsCanalInclusao.
	 *
	 * @param dsCanalInclusao the ds canal inclusao
	 */
	public void setDsCanalInclusao(String dsCanalInclusao) {
		this.dsCanalInclusao = dsCanalInclusao;
	}

	/**
	 * Get: dsCanalManutencao.
	 *
	 * @return dsCanalManutencao
	 */
	public String getDsCanalManutencao() {
		return dsCanalManutencao;
	}

	/**
	 * Set: dsCanalManutencao.
	 *
	 * @param dsCanalManutencao the ds canal manutencao
	 */
	public void setDsCanalManutencao(String dsCanalManutencao) {
		this.dsCanalManutencao = dsCanalManutencao;
	}

	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}

	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}

	/**
	 * Get: hrAtendimentoSolicitacao.
	 *
	 * @return hrAtendimentoSolicitacao
	 */
	public String getHrAtendimentoSolicitacao() {
		return hrAtendimentoSolicitacao;
	}

	/**
	 * Set: hrAtendimentoSolicitacao.
	 *
	 * @param hrAtendimentoSolicitacao the hr atendimento solicitacao
	 */
	public void setHrAtendimentoSolicitacao(String hrAtendimentoSolicitacao) {
		this.hrAtendimentoSolicitacao = hrAtendimentoSolicitacao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: hrSolicitacao.
	 *
	 * @return hrSolicitacao
	 */
	public String getHrSolicitacao() {
		return hrSolicitacao;
	}

	/**
	 * Set: hrSolicitacao.
	 *
	 * @param hrSolicitacao the hr solicitacao
	 */
	public void setHrSolicitacao(String hrSolicitacao) {
		this.hrSolicitacao = hrSolicitacao;
	}

	/**
	 * Get: mensagem.
	 *
	 * @return mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Set: mensagem.
	 *
	 * @param mensagem the mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Get: nmOperacaoFluxoInlcusao.
	 *
	 * @return nmOperacaoFluxoInlcusao
	 */
	public String getNmOperacaoFluxoInlcusao() {
		return nmOperacaoFluxoInlcusao;
	}

	/**
	 * Set: nmOperacaoFluxoInlcusao.
	 *
	 * @param nmOperacaoFluxoInlcusao the nm operacao fluxo inlcusao
	 */
	public void setNmOperacaoFluxoInlcusao(String nmOperacaoFluxoInlcusao) {
		this.nmOperacaoFluxoInlcusao = nmOperacaoFluxoInlcusao;
	}

	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}

	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}

	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public String getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}

	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(String nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}

	/**
	 * Get: qtdeRegistrosSolicitacao.
	 *
	 * @return qtdeRegistrosSolicitacao
	 */
	public String getQtdeRegistrosSolicitacao() {
		return qtdeRegistrosSolicitacao;
	}

	/**
	 * Set: qtdeRegistrosSolicitacao.
	 *
	 * @param qtdeRegistrosSolicitacao the qtde registros solicitacao
	 */
	public void setQtdeRegistrosSolicitacao(String qtdeRegistrosSolicitacao) {
		this.qtdeRegistrosSolicitacao = qtdeRegistrosSolicitacao;
	}

	/**
	 * Get: resultadoProcessamento.
	 *
	 * @return resultadoProcessamento
	 */
	public String getResultadoProcessamento() {
		return resultadoProcessamento;
	}

	/**
	 * Set: resultadoProcessamento.
	 *
	 * @param resultadoProcessamento the resultado processamento
	 */
	public void setResultadoProcessamento(String resultadoProcessamento) {
		this.resultadoProcessamento = resultadoProcessamento;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: listaBaseSistemaHash.
	 *
	 * @return listaBaseSistemaHash
	 */
	public Map<Integer, String> getListaBaseSistemaHash() {
		return listaBaseSistemaHash;
	}

	/**
	 * Set lista base sistema hash.
	 *
	 * @param listaBaseSistemaHash the lista base sistema hash
	 */
	public void setListaBaseSistemaHash(
			Map<Integer, String> listaBaseSistemaHash) {
		this.listaBaseSistemaHash = listaBaseSistemaHash;
	}

	/**
	 * Get: cboTipoSistema.
	 *
	 * @return cboTipoSistema
	 */
	public Integer getCboTipoSistema() {
		return cboTipoSistema;
	}

	/**
	 * Set: cboTipoSistema.
	 *
	 * @param cboTipoSistema the cbo tipo sistema
	 */
	public void setCboTipoSistema(Integer cboTipoSistema) {
		this.cboTipoSistema = cboTipoSistema;
	}

	/**
	 * Get: listarTipoSistema.
	 *
	 * @return listarTipoSistema
	 */
	public List<SelectItem> getListarTipoSistema() {
		return listarTipoSistema;
	}

	/**
	 * Set: listarTipoSistema.
	 *
	 * @param listarTipoSistema the listar tipo sistema
	 */
	public void setListarTipoSistema(List<SelectItem> listarTipoSistema) {
		this.listarTipoSistema = listarTipoSistema;
	}

	/**
	 * Get: vlDescontoTarifa.
	 *
	 * @return vlDescontoTarifa
	 */
	public BigDecimal getVlDescontoTarifa() {
		return vlDescontoTarifa;
	}

	/**
	 * Set: vlDescontoTarifa.
	 *
	 * @param vlDescontoTarifa the vl desconto tarifa
	 */
	public void setVlDescontoTarifa(BigDecimal vlDescontoTarifa) {
		this.vlDescontoTarifa = vlDescontoTarifa;
	}

	/**
	 * Get: taxaBonificacao.
	 *
	 * @return taxaBonificacao
	 */
	public String getTaxaBonificacao() {
		return taxaBonificacao;
	}

	/**
	 * Set: taxaBonificacao.
	 *
	 * @param taxaBonificacao the taxa bonificacao
	 */
	public void setTaxaBonificacao(String taxaBonificacao) {
		this.taxaBonificacao = taxaBonificacao;
	}

	/**
	 * Get: tipoSistema.
	 *
	 * @return tipoSistema
	 */
	public String getTipoSistema() {
		return tipoSistema;
	}

	/**
	 * Set: tipoSistema.
	 *
	 * @param tipoSistema the tipo sistema
	 */
	public void setTipoSistema(String tipoSistema) {
		this.tipoSistema = tipoSistema;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasService the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
		this.consultasService = consultasService;
	}

	/**
	 * Is desabilita desconto tarifa.
	 *
	 * @return true, if is desabilita desconto tarifa
	 */
	public boolean isDesabilitaDescontoTarifa() {
		if (atributoSoltc != null
				&& "S".equalsIgnoreCase(atributoSoltc.getDsTarifaBloqueio())) {
			return true;
		}
		return false;
	}

	/**
	 * Get: manterSolicitacaoRastFavService.
	 *
	 * @return manterSolicitacaoRastFavService
	 */
	public IManterSolicitacaoRastFavService getManterSolicitacaoRastFavService() {
		return manterSolicitacaoRastFavService;
	}

	/**
	 * Set: manterSolicitacaoRastFavService.
	 *
	 * @param manterSolicitacaoRastFavService the manter solicitacao rast fav service
	 */
	public void setManterSolicitacaoRastFavService(
			IManterSolicitacaoRastFavService manterSolicitacaoRastFavService) {
		this.manterSolicitacaoRastFavService = manterSolicitacaoRastFavService;
	}

	/**
	 * Get: atributoSoltc.
	 *
	 * @return atributoSoltc
	 */
	public VerificaraAtributoSoltcSaidaDTO getAtributoSoltc() {
		return atributoSoltc;
	}

	/**
	 * Set: atributoSoltc.
	 *
	 * @param atributoSoltc the atributo soltc
	 */
	public void setAtributoSoltc(VerificaraAtributoSoltcSaidaDTO atributoSoltc) {
		this.atributoSoltc = atributoSoltc;
	}

	/**
	 * Get: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @return solEmissaoComprovantePagtoClientePagServiceImpl
	 */
	public ISolEmissaoComprovantePagtoClientePagService getSolEmissaoComprovantePagtoClientePagServiceImpl() {
		return solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	/**
	 * Set: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @param solEmissaoComprovantePagtoClientePagServiceImpl the sol emissao comprovante pagto cliente pag service impl
	 */
	public void setSolEmissaoComprovantePagtoClientePagServiceImpl(
			ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl) {
		this.solEmissaoComprovantePagtoClientePagServiceImpl = solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	/**
	 * Is indicador desconto bloqueio.
	 *
	 * @return true, if is indicador desconto bloqueio
	 */
	public boolean isIndicadorDescontoBloqueio() {
		return indicadorDescontoBloqueio;
	}

	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}

	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}

	/**
	 * Get: vlTarifaAtualizada.
	 *
	 * @return vlTarifaAtualizada
	 */
	public BigDecimal getVlTarifaAtualizada() {
		return vlTarifaAtualizada;
	}

	/**
	 * Set: vlTarifaAtualizada.
	 *
	 * @param vlTarifaAtualizada the vl tarifa atualizada
	 */
	public void setVlTarifaAtualizada(BigDecimal vlTarifaAtualizada) {
		this.vlTarifaAtualizada = vlTarifaAtualizada;
	}

}
