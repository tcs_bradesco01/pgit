/*
 * Nome: br.com.bradesco.web.pgit.view.converters
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.converters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import br.com.bradesco.web.aq.application.log.ILogManager;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;

/**
 * Nome: DateBrazillianConverter
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DateBrazillianConverter implements Converter {

	/** Atributo logManager. */
	private ILogManager logManager = BradescoCommonServiceFactory.getLogManager();
	
	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2)
			throws ConverterException {
		if(arg2 == null || arg2.trim().equals("") || arg2.trim().equals("//")){
			return null;
		}
		
		SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy",new Locale("pt","BR"));
		Calendar dataFormatada = Calendar.getInstance();
		try {
			dataFormatada.setTime(data.parse(arg2));
			dataFormatada.set(Calendar.HOUR_OF_DAY, 0);
			dataFormatada.set(Calendar.MINUTE, 0);
			dataFormatada.set(Calendar.SECOND, 0);
			dataFormatada.set(Calendar.MILLISECOND, 0);
			
		} catch (ParseException e) {
			logManager.error(DateBrazillianConverter.class, e);
		}
		
		return dataFormatada.getTime();
	}

	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2)
			throws ConverterException {
		
		if(arg2 == null){
			return null;
		}
		
		SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy",new Locale("pt","BR"));
		
		return data.format(arg2);
	}

}
