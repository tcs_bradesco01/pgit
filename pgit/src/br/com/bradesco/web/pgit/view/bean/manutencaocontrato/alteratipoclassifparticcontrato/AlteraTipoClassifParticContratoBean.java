/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.alteratipoclassifparticcontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.alteratipoclassifparticcontrato;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosEmissaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosEmissaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.IEnderecoService;
import br.com.bradesco.web.pgit.service.business.endereco.IEnderecoServiceConstants;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DatalharEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DatalharEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DetalharHistoricoEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DetalharHistoricoEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.DetalheEnderecoDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ExcluirEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ExcluirEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.IncluirEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.IncluirEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoEmailEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoEmailOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarEnderecoParticipanteOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarHistoricoEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.SubstituirVincEnderecoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.endereco.bean.SubstituirVincEnderecoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: AlteraTipoClassifParticContratoBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class AlteraTipoClassifParticContratoBean {

	private static final int ESPECIE_ENDERECO = 1;

	/** Atributo PAGINA_INICIAL_CONSULTA. */
	private static final String PAGINA_INICIAL_CONSULTA = "conAlteraTpClassifParticContrato";

	/** Atributo PAGINA_INCLUSAO. */
	private static final String PAGINA_INCLUSAO = "incAlteraTpClassifParticContrato";

	/** Atributo PAGINA_ALTERACAO. */
	private static final String PAGINA_ALTERACAO = "altAlteraTpClassifParticContrato";

	/** Atributo PAGINA_EXCLUSAO. */
	private static final String PAGINA_EXCLUSAO = "excConfAlteraTpClassifParticContrato";

	/** Atributo PAGINA_HISTORICO. */
	private static final String PAGINA_HISTORICO = "histAlteraTpClassifParticContrato";

	/** Atributo VINCULACAO. */
	private static final String VINCULACAO = "vincAlteraTpClassifParticContrato";

	/** Atributo ACAO_INCLUIR. */
	private static final String ACAO_INCLUIR = "I";

	/** Atributo ACAO_ALTERAR. */
	private static final String ACAO_ALTERAR = "A";

	/** Atributo ACAO_EXCLUIR. */
	private static final String ACAO_EXCLUIR = "E";

	/** Atributo ACAO_HISTORICO. */
	private static final String ACAO_HISTORICO = "H";

	/** Atributo listarServicosEmissaoSaida. */
	private List<ListarServicosEmissaoSaidaDTO> listarServicosEmissaoSaida = new ArrayList<ListarServicosEmissaoSaidaDTO>();

	/** Atributo listaEmpresaGestora. */
	private List<SelectItem> listaEmpresaGestora = new ArrayList<SelectItem>();

	/** Atributo listaTipoContrato. */
	private List<SelectItem> listaTipoContrato = new ArrayList<SelectItem>();

	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico = new ArrayList<SelectItem>();

	/** Atributo listaOperacao. */
	private List<SelectItem> listaOperacao = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeServicoInc. */
	private List<SelectItem> listaModalidadeServicoInc = new ArrayList<SelectItem>();

	/** Atributo listaOperacaoInc. */
	private List<SelectItem> listaOperacaoInc = new ArrayList<SelectItem>();

	/** Atributo listaPesquisa. */
	private List<ListarEnderecoParticipanteOcorrenciasSaidaDTO> listaPesquisa = new ArrayList<ListarEnderecoParticipanteOcorrenciasSaidaDTO>();

	/** Atributo listaPesquisaSelectItems. */
	private List<SelectItem> listaPesquisaSelectItems = new ArrayList<SelectItem>();

	/** Atributo listaHistoricoPesquisa. */
	private List<ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO> listaHistoricoPesquisa = new ArrayList<ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO>();

	/** Atributo listaHistPesquisaSelectItems. */
	private List<SelectItem> listaHistPesquisaSelectItems = new ArrayList<SelectItem>();

	/** Atributo listaParticipantes. */
	private List<ListarParticipantesSaidaDTO> listaParticipantes = new ArrayList<ListarParticipantesSaidaDTO>();

	/** Atributo dtoListaParticipantes. */
	private ListarParticipantesSaidaDTO dtoListaParticipantes = new ListarParticipantesSaidaDTO();

	/** Atributo listaParticipantesItems. */
	private List<SelectItem> listaParticipantesItems = new ArrayList<SelectItem>();

	/** Atributo listaEndereco. */
	private List<ListarEnderecoEmailOcorrenciasSaidaDTO> listaEndereco = new ArrayList<ListarEnderecoEmailOcorrenciasSaidaDTO>();

	/** Atributo listaEnderecoItems. */
	private List<SelectItem> listaEnderecoItems = new ArrayList<SelectItem>();

	/** Atributo detalhe. */
	private DetalheEnderecoDTO detalhe = new DetalheEnderecoDTO();

	/** Atributo filtroPesquisaEndereco. */
	private ListarEnderecoParticipanteEntradaDTO filtroPesquisaEndereco = new ListarEnderecoParticipanteEntradaDTO();

	/** Atributo filtroHistPesquisaEndereco. */
	private ListarHistoricoEnderecoParticipanteEntradaDTO filtroHistPesquisaEndereco = new ListarHistoricoEnderecoParticipanteEntradaDTO();

	/** Atributo participantePesquisa. */
	private ListarParticipantesSaidaDTO participantePesquisa = new ListarParticipantesSaidaDTO();

	/** Atributo participanteInclusao. */
	private ListarParticipantesSaidaDTO participanteInclusao = new ListarParticipantesSaidaDTO();

	/** Atributo indiceParticipanteSelecionado. */
	private Integer indiceParticipanteSelecionado = null;

	/** Atributo indiceEnderecoSelecionado. */
	private Integer indiceEnderecoSelecionado = null;

	/** Atributo indicePesquisaSelecionado. */
	private Integer indicePesquisaSelecionado = null;

	/** Atributo indicePesquisaHistSelecionado. */
	private Integer indicePesquisaHistSelecionado = null;

	/** Atributo radioArgPesquisa. */
	private String radioArgPesquisa = null;

	/** Atributo incEndereco. */
	private IncluirEnderecoParticipanteEntradaDTO incEndereco = new IncluirEnderecoParticipanteEntradaDTO();

	/** Atributo enderecoSelecionado. */
	private ListarEnderecoEmailOcorrenciasSaidaDTO enderecoSelecionado = new ListarEnderecoEmailOcorrenciasSaidaDTO();

	/** Atributo consultaContratoEfetuada. */
	private boolean consultaContratoEfetuada = false;

	/** Atributo pesquisaParticipanteEfetuada. */
	private boolean pesquisaParticipanteEfetuada = false;

	/** Atributo pesquisaParticipanteInclusaoEfetuada. */
	private boolean pesquisaParticipanteInclusaoEfetuada = false;

	/** Atributo pesquisaEnderecoInclusaoEfetuada. */
	private boolean pesquisaEnderecoInclusaoEfetuada = false;

	/** Atributo btoAcao. */
	private String btoAcao = "";

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean = null;

	/** Atributo identificacaoClienteBean. */
	private IdentificacaoClienteBean identificacaoClienteBean = null;

	/** Atributo comboService. */
	private IComboService comboService = null;

	/** Atributo enderecoService. */
	private IEnderecoService enderecoService = null;

	/** Atributo manterContratoService. */
	private IManterContratoService manterContratoService = null;

	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo habilitaBtoCont. */
	private Boolean habilitaBtoCont;

	/** Atributo habilitaQtdDias. */
	private boolean habilitaQtdDias;

	/** Atributo foco. */
	private String foco;

	// Cliente Representante
	/** Atributo cpfCnpjRepresentante. */
	private String cpfCnpjRepresentante;

	/** Atributo nomeRazaoRepresentante. */
	private String nomeRazaoRepresentante;

	/** Atributo grupEconRepresentante. */
	private String grupEconRepresentante;

	/** Atributo ativEconRepresentante. */
	private String ativEconRepresentante;

	/** Atributo segRepresentante. */
	private String segRepresentante;

	/** Atributo subSegRepresentante. */
	private String subSegRepresentante;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;

	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado;

	// Cliente Participante
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;

	/** Atributo cpfCnpjParticipanteTemp. */
	private String cpfCnpjParticipanteTemp;

	/** Atributo nomeRazaoParticipanteTemp. */
	private String nomeRazaoParticipanteTemp;

	/** Atributo cpfCnpjParticipanteDet. */
	private String cpfCnpjParticipanteDet;

	/** Atributo nomeRazaoParticipanteDet. */
	private String nomeRazaoParticipanteDet;

	/** Atributo grupoEconParticipante. */
	private String grupoEconParticipante;

	/** Atributo atividadeEconParticipante. */
	private String atividadeEconParticipante;

	/** Atributo segmentoParticipante. */
	private String segmentoParticipante;

	/** Atributo subSegmentoParticipante. */
	private String subSegmentoParticipante;

	/** Atributo dsTipoParticipacao. */
	private String dsTipoParticipacao;

	/** Atributo situacaoParticipacao. */
	private String situacaoParticipacao;

	/** Atributo dsTipoServico. */
	private String dsTipoServico;

	// Contrato
	/** Atributo empresaContrato. */
	private String empresaContrato;

	/** Atributo tipoContrato. */
	private String tipoContrato;

	/** Atributo numeroContrato. */
	private String numeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo situacaoContrato. */
	private String situacaoContrato;

	/** Atributo motivoContrato. */
	private String motivoContrato;

	/** Atributo participacaoContrato. */
	private String participacaoContrato;

	/** Atributo dtManutencaoInicio. */
	private Date dtManutencaoInicio;

	/** Atributo dtManutencaoFim. */
	private Date dtManutencaoFim;

	/** Atributo dsLogradouro. */
	private String dsLogradouro;

	/** Atributo dsEndereco. */
	private String dsEndereco;

	/**
	 * Inicializar pagina.
	 * 
	 * @param actionEvent
	 *            the action event
	 */
	public void inicializarPagina(ActionEvent actionEvent) {
		this.identificacaoClienteContratoBean
				.setPaginaRetorno(PAGINA_INICIAL_CONSULTA);
		this.identificacaoClienteBean.setPaginaRetorno(PAGINA_INICIAL_CONSULTA);

		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean
				.setPaginaRetorno(PAGINA_INICIAL_CONSULTA);

		// Limpar Variaveis
		setListaControleRadio(null);
		setItemSelecionadoLista(null);

		filtroAgendamentoEfetivacaoEstornoBean
				.setClienteContratoSelecionado(false);
		setHabilitaBtoCont(true);

		this.identificacaoClienteContratoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean
				.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean
				.setPaginaCliente("identificacaoClienteManterContrato");
		identificacaoClienteContratoBean
				.setPaginaRetorno(PAGINA_INICIAL_CONSULTA);
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean
				.setHabilitaEmpresaGestoraTipoContrato(true);
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
	}

	/**
	 * Consultar contrato.
	 * 
	 * @return the string
	 */
	public String consultarContrato() {
		String pagRetorno = this.identificacaoClienteBean.consultarContratos();

		if (pagRetorno != null) {
			setConsultaContratoEfetuada(true);
		} else {
			this.identificacaoClienteBean
					.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
			setConsultaContratoEfetuada(false);
			limparParticipante();
		}

		if (ACAO_HISTORICO.equals(getBtoAcao())) {
			pagRetorno = PAGINA_HISTORICO;
		}

		return pagRetorno;
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 * 
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		identificacaoClienteContratoBean.limparCampos();
		setItemSelecionadoLista(null);

		return "";
	}

	/**
	 * Limpar dados cliente.
	 */
	public void limparDadosCliente() {
		this.identificacaoClienteContratoBean.limparDadosCliente();
	}

	/**
	 * Limpar contrato.
	 */
	public void limparContrato() {
		this.identificacaoClienteBean.limparContrato();
		this.identificacaoClienteBean
				.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		setConsultaContratoEfetuada(false);
		limparParticipante();
	}

	/**
	 * Limpar participante.
	 */
	public void limparParticipante() {
		setParticipantePesquisa(new ListarParticipantesSaidaDTO());
		setPesquisaParticipanteEfetuada(false);
		setRadioArgPesquisa(null);
		setDtoListaParticipantes(new ListarParticipantesSaidaDTO());
		listaPesquisa = new ArrayList<ListarEnderecoParticipanteOcorrenciasSaidaDTO>();
		filtroPesquisaEndereco = new ListarEnderecoParticipanteEntradaDTO();
		setDsTipoServico("");
		setCpfCnpjParticipante("");
		setNomeRazaoParticipante("");
		setDsTipoParticipacao("");
		setSituacaoParticipacao("");
	}

	/**
	 * Limpar grid.
	 */
	public void limparGrid() {
		setIndicePesquisaSelecionado(null);
		setListaPesquisa(new ArrayList<ListarEnderecoParticipanteOcorrenciasSaidaDTO>());
		setListaPesquisaSelectItems(new ArrayList<SelectItem>());
	}

	/**
	 * Pesquisar participante.
	 * 
	 * @return the string
	 */
	public String pesquisarParticipante() {
		pesquisarParticipantePesquisa();

		setDetalhe(new DetalheEnderecoDTO(getIdentificacaoClienteContratoBean()
				.getSaidaConsultarListaClientePessoas(),
				getIdentificacaoClienteBean()
						.getSaidaConsultarListaContratosPessoas()));

		return "conParticipantesAlteraTpClassifParticContrato";
	}

	/**
	 * Pesquisar participante paginacao.
	 * 
	 * @param event
	 *            the event
	 */
	public void pesquisarParticipantePaginacao(ActionEvent event) {
		pesquisarParticipantePesquisa();
	}

	/**
	 * Pesquisar participante pesquisa.
	 */
	private void pesquisarParticipantePesquisa() {
		setIndiceParticipanteSelecionado(null);

		ListarContratosPgitSaidaDTO dto = getIdentificacaoClienteContratoBean()
				.getListaGridPesquisa().get(
						getIdentificacaoClienteContratoBean()
								.getItemSelecionadoLista());
		ListarParticipantesEntradaDTO entParticipante = new ListarParticipantesEntradaDTO();
		entParticipante.setMaxOcorrencias(25);
		entParticipante.setCodPessoaJuridica(dto.getCdPessoaJuridica());
		entParticipante.setCodTipoContrato(dto.getCdTipoContrato());
		entParticipante.setNrSequenciaContrato(dto.getNrSequenciaContrato());

		try {
			setListaParticipantes(getManterContratoService()
					.listarParticipantes(entParticipante));
		} catch (PdcAdapterFunctionalException e) {
			setListaParticipantes(new ArrayList<ListarParticipantesSaidaDTO>());
			throw e;
		} finally {
			setListaParticipantesItems(preencherSelectItems(getListaParticipantes()));
		}
	}

	/**
	 * Preencher select items.
	 * 
	 * @param list
	 *            the list
	 * @return the list< select item>
	 */
	private List<SelectItem> preencherSelectItems(List<? extends Object> list) {
		List<SelectItem> listSelectItem = new ArrayList<SelectItem>();
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				listSelectItem.add(new SelectItem(i, ""));
			}
		}
		return listSelectItem;
	}

	/**
	 * Consultar participante.
	 */
	public void consultarParticipante() {
		ListarContratosPgitSaidaDTO dto = getIdentificacaoClienteContratoBean()
				.getListaGridPesquisa().get(
						getIdentificacaoClienteContratoBean()
								.getItemSelecionadoLista());
		getFiltroPesquisaEndereco().setCdpessoaJuridicaContrato(
				dto.getCdPessoaJuridica());
		getFiltroPesquisaEndereco().setCdTipoContratoNegocio(
				dto.getCdTipoContrato());
		getFiltroPesquisaEndereco().setNrSequenciaContratoNegocio(
				dto.getNrSequenciaContrato());
		getFiltroPesquisaEndereco().setCdClub(
				PgitUtil.verificaLongNulo(dtoListaParticipantes.getCdPessoa()));

		getFiltroPesquisaEndereco()
				.setCdProdutoOperacaoRelacionado(
						filtroPesquisaEndereco.getCdProdutoServicoOperacao() == null ? 0
								: filtroPesquisaEndereco
										.getCdProdutoServicoOperacao());

		if ("1".equals(getRadioArgPesquisa())) {
			for (int i = 0; i < listarServicosEmissaoSaida.size(); i++) {
				if (filtroPesquisaEndereco.getCdProdutoServicoOperacao()
						.equals(
								listarServicosEmissaoSaida.get(i)
										.getCdProdutoRelacionado())) {
					filtroPesquisaEndereco
							.setCdTipoServico(listarServicosEmissaoSaida.get(i)
									.getCdProdutoOperacao());
					break;
				}
			}
		}

		consultarParticipantePesquisa();
	}

	/**
	 * Consultar participante voltar.
	 * 
	 * @return the string
	 */
	public String consultarParticipanteVoltar() {
		ListarContratosPgitSaidaDTO dto = getIdentificacaoClienteContratoBean()
				.getListaGridPesquisa().get(
						getIdentificacaoClienteContratoBean()
								.getItemSelecionadoLista());
		getFiltroPesquisaEndereco().setCdpessoaJuridicaContrato(
				dto.getCdPessoaJuridica());
		getFiltroPesquisaEndereco().setCdTipoContratoNegocio(
				dto.getCdTipoContrato());
		getFiltroPesquisaEndereco().setNrSequenciaContratoNegocio(
				dto.getNrSequenciaContrato());
		getFiltroPesquisaEndereco().setCdClub(0L);

		getFiltroPesquisaEndereco()
				.setCdProdutoOperacaoRelacionado(
						filtroPesquisaEndereco.getCdProdutoServicoOperacao() == null ? 0
								: filtroPesquisaEndereco
										.getCdProdutoServicoOperacao());

		if ("1".equals(getRadioArgPesquisa())) {
			for (int i = 0; i < listarServicosEmissaoSaida.size(); i++) {
				if (filtroPesquisaEndereco.getCdProdutoServicoOperacao()
						.equals(
								listarServicosEmissaoSaida.get(i)
										.getCdProdutoRelacionado())) {
					filtroPesquisaEndereco
							.setCdTipoServico(listarServicosEmissaoSaida.get(i)
									.getCdProdutoOperacao());
					break;
				}
			}
		}

		return consultarVoltarParticipantePesquisa();
	}

	/**
	 * Consultar participante alterar.
	 * 
	 * @return the string
	 */
	public String consultarParticipanteAlterar() {
		consultarParticipante();

		return VINCULACAO;
	}

	/**
	 * Vinculacao.
	 * 
	 * @return the string
	 */
	public String vinculacao() {
		try {
			listarTipoServico();
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(e.getCode(), 8) + ") "
							+ e.getMessage(), false);
			return "";
		}

		setParticipantePesquisa(new ListarParticipantesSaidaDTO());
		setFiltroPesquisaEndereco(new ListarEnderecoParticipanteEntradaDTO());
		setRadioArgPesquisa(null);
		setRadioArgPesquisa(null);
		setListaPesquisa(new ArrayList<ListarEnderecoParticipanteOcorrenciasSaidaDTO>());
		setPesquisaParticipanteEfetuada(false);
		setIndicePesquisaSelecionado(null);

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(
						identificacaoClienteContratoBean
								.getItemSelecionadoLista());

		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - "
				+ saidaDTO.getDescFuncionarioBradesco());

		// Cliente Participante
		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			setCpfCnpjParticipante("");
			setNomeRazaoParticipante("");
		} else {
			setCpfCnpjParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado() == null ? ""
					: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
					: identificacaoClienteContratoBean
							.getSaidaConsultarListaClientePessoas()
							.getDsNomeRazao());
		}

		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());

		setBtoAcao("");

		return VINCULACAO;
	}

	/**
	 * Consultar participante paginacao.
	 * 
	 * @param event
	 *            the event
	 */
	public void consultarParticipantePaginacao(ActionEvent event) {
		consultarParticipantePesquisa();
	}

	/**
	 * Consultar participante pesquisa.
	 * 
	 * @return the string
	 */
	public String consultarParticipantePesquisa() {
		setIndicePesquisaSelecionado(null);

		try {
			setListaPesquisa(getEnderecoService().listarEnderecoParticipante(
					getFiltroPesquisaEndereco()));

			listaPesquisaSelectItems = new ArrayList<SelectItem>();

			for (int i = 0; i < listaPesquisa.size(); i++) {
				listaPesquisaSelectItems.add(new SelectItem(i, " "));
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaPesquisa(new ArrayList<ListarEnderecoParticipanteOcorrenciasSaidaDTO>());
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(e.getCode(), 8) + ") "
							+ e.getMessage(), false);
		}

		setBtoAcao("");

		return VINCULACAO;
	}

	/**
	 * Consultar voltar participante pesquisa.
	 * 
	 * @return the string
	 */
	public String consultarVoltarParticipantePesquisa() {
		setIndicePesquisaSelecionado(null);

		try {
			setListaPesquisa(getEnderecoService().listarEnderecoParticipante(
					getFiltroPesquisaEndereco()));

			listaPesquisaSelectItems = new ArrayList<SelectItem>();

			for (int i = 0; i < listaPesquisa.size(); i++) {
				listaPesquisaSelectItems.add(new SelectItem(i, " "));
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaPesquisa(new ArrayList<ListarEnderecoParticipanteOcorrenciasSaidaDTO>());
		}

		setBtoAcao("");

		return VINCULACAO;
	}

	/**
	 * Selecionar participante.
	 * 
	 * @return the string
	 */
	public String selecionarParticipante() {
		if (getIndiceParticipanteSelecionado() != null) {
			ListarParticipantesSaidaDTO partic = getListaParticipantes().get(
					getIndiceParticipanteSelecionado());
			if (ACAO_INCLUIR.equals(getBtoAcao())) {
				setParticipanteInclusao(partic);
				setPesquisaParticipanteInclusaoEfetuada(true);
			} else {
				setParticipantePesquisa(partic);
				setPesquisaParticipanteEfetuada(true);
			}
		}

		setDtoListaParticipantes(listaParticipantes
				.get(indiceParticipanteSelecionado));

		return redirecionarVoltaParticipante();
	}

	/**
	 * Selecionar radio arg pesquisa.
	 */
	public void selecionarRadioArgPesquisa() {
		if (getRadioArgPesquisa() != null) {
			if ("1".equals(getRadioArgPesquisa())) {
				setParticipantePesquisa(new ListarParticipantesSaidaDTO());
				setPesquisaParticipanteEfetuada(false);
				setFiltroPesquisaEndereco(new ListarEnderecoParticipanteEntradaDTO());
				setDtoListaParticipantes(new ListarParticipantesSaidaDTO());
			} else {
				setFiltroPesquisaEndereco(new ListarEnderecoParticipanteEntradaDTO());
				setDtoListaParticipantes(new ListarParticipantesSaidaDTO());
			}
		}
	}

	/**
	 * Pesquisar endereco.
	 * 
	 * @return the string
	 */
	public String pesquisarEndereco() {

		pesquisarEnderecoPesq();

		return "conEnderecoAlteraTpClassifParticContrato";
	}

	/**
	 * Pesquisar endereco paginacao.
	 * 
	 * @param event
	 *            the event
	 */
	public void pesquisarEnderecoPaginacao(ActionEvent event) {
		pesquisarEnderecoPesq();
	}

	/**
	 * Pesquisar endereco pesq.
	 */
	private void pesquisarEnderecoPesq() {
		setIndiceEnderecoSelecionado(null);

		ListarContratosPgitSaidaDTO dto = getIdentificacaoClienteContratoBean()
				.getListaGridPesquisa().get(
						getIdentificacaoClienteContratoBean()
								.getItemSelecionadoLista());
		ListarEnderecoEmailEntradaDTO filtroEndEmail = new ListarEnderecoEmailEntradaDTO();
		filtroEndEmail.setCdCorpoCpfCnpj(getParticipanteInclusao()
				.getCdCpfCnpj());
		filtroEndEmail.setCdCnpjContrato(getParticipanteInclusao()
				.getCdFilialCnpj());
		filtroEndEmail.setCdCpfCnpjDigito(getParticipanteInclusao()
				.getCdControleCnpj());
		filtroEndEmail.setCdEmpresaContrato(dto.getCdPessoaJuridica());
		Long cdClub = null;
		if (ACAO_INCLUIR.equals(getBtoAcao())) {
			cdClub = getParticipanteInclusao().getCdPessoa();
		} else {
			cdClub = getDetalhe().getParticipante().getCdClub();
		}
		filtroEndEmail.setCdClub(cdClub);
		filtroEndEmail.setCdEspecieEndereco(ESPECIE_ENDERECO);

		try {
			setListaEndereco(getEnderecoService().listarEnderecoEmail(
					filtroEndEmail));
		} catch (PdcAdapterFunctionalException e) {
			setListaEndereco(new ArrayList<ListarEnderecoEmailOcorrenciasSaidaDTO>());
			throw e;
		} finally {
			setListaEnderecoItems(preencherSelectItems(getListaEndereco()));
		}
	}

	/**
	 * Limpar endereco.
	 * 
	 * @return the string
	 */
	public String limparEndereco() {
		setIndiceEnderecoSelecionado(null);
		return null;
	}

	/**
	 * Selecionar endereco.
	 * 
	 * @return the string
	 */
	public String selecionarEndereco() {
		if (getIndiceEnderecoSelecionado() != null) {
			setEnderecoSelecionado(getListaEndereco().get(
					getIndiceEnderecoSelecionado()));

			Integer cdEspecie = getIncEndereco().getCdEspeceEndereco();
			String dsEspecie = PgitUtil.obterSelectItemSelecionado(
					getListaEspecieEndereco(), cdEspecie);

			DatalharEnderecoParticipanteSaidaDTO endereco = getDetalhe()
					.getManutencao();
			endereco.setCdEspeceEndereco(cdEspecie);
			endereco.setDsEspecieEndereco(dsEspecie);
			endereco.setDsLogradouroPagador(getEnderecoSelecionado()
					.getDsLogradouroPagador());
			endereco.setDsNumeroLogradouroPagador(getEnderecoSelecionado()
					.getDsNumeroLogradouroPagador());
			endereco.setDsComplementoLogradouroPagador(getEnderecoSelecionado()
					.getDsComplementoLogradouroPagador());
			endereco.setDsBairroClientePagador(getEnderecoSelecionado()
					.getDsBairroClientePagador());
			endereco.setDsMunicipioClientePagador(getEnderecoSelecionado()
					.getDsMunicipioClientePagador());
			endereco.setCdSiglaUfPagador(getEnderecoSelecionado()
					.getCdSiglaUfPagador());
			endereco.setDsPais(getEnderecoSelecionado().getDsPais());
			endereco
					.setCdCepPagador(getEnderecoSelecionado().getCdCepPagador());
			endereco.setCdCepComplementoPagador(getEnderecoSelecionado()
					.getCdCepComplementoPagador());
			endereco.setCdEnderecoEletronico(getEnderecoSelecionado()
					.getCdEnderecoEletronico());
			endereco.setCdTipo(getEnderecoSelecionado().getCdTipo());
			endereco.setNrSequencialEndereco(getEnderecoSelecionado()
					.getNrSequenciaEndereco());

			setPesquisaEnderecoInclusaoEfetuada(true);

			setDsEndereco(getEnderecoSelecionado().getDsEndereco());
		}

		return redirecionarVoltarEndereco();
	}

	/**
	 * Selecionar especie endereco combo.
	 */
	public void selecionarEspecieEnderecoCombo() {
		setPesquisaEnderecoInclusaoEfetuada(false);
		setEnderecoSelecionado(new ListarEnderecoEmailOcorrenciasSaidaDTO());
		getDetalhe().setManutencao(new DatalharEnderecoParticipanteSaidaDTO());
	}

	/**
	 * Consultar historico participante.
	 */
	public void consultarHistoricoParticipante() {
		ListarContratosPgitSaidaDTO entContratoPessoa = getIdentificacaoClienteContratoBean()
				.getListaGridPesquisa().get(
						getIdentificacaoClienteContratoBean()
								.getItemSelecionadoLista());

		// ListarHistoricoEnderecoParticipanteEntradaDTO filtroHist =
		// getFiltroHistPesquisaEndereco();

		filtroHistPesquisaEndereco
				.setCdpessoaJuridicaContrato(entContratoPessoa
						.getCdPessoaJuridica());
		filtroHistPesquisaEndereco.setCdTipoContratoNegocio(entContratoPessoa
				.getCdTipoContrato());
		filtroHistPesquisaEndereco
				.setNrSequenciaContratoNegocio(entContratoPessoa
						.getNrSequenciaContrato());
		filtroHistPesquisaEndereco
				.setCdClub(indiceParticipanteSelecionado == null ? indicePesquisaSelecionado == null ? 0L
						: getListaPesquisa()
								.get(getIndicePesquisaSelecionado())
								.getCdClub()
						: PgitUtil.verificaLongNulo(dtoListaParticipantes
								.getCdPessoa()));
		filtroHistPesquisaEndereco.setCdOperacaoProdutoServico(0);
		filtroHistPesquisaEndereco
				.setDtManutencaoInicio(getDtManutencaoInicio());
		filtroHistPesquisaEndereco.setDtManutencaoFim(getDtManutencaoFim());

		if ("1".equals(getRadioArgPesquisa())) {
			for (int i = 0; i < listarServicosEmissaoSaida.size(); i++) {
				if (getFiltroHistPesquisaEndereco()
						.getCdProdutoOperacaoRelacionado().equals(
								listarServicosEmissaoSaida.get(i)
										.getCdProdutoRelacionado())) {
					filtroHistPesquisaEndereco
							.setCdProdutoServicoOperacao(listarServicosEmissaoSaida
									.get(i).getCdProdutoOperacao());
					break;
				}
			}
		}

		consultarHistoricoParticipantePesq();
	}

	/**
	 * Consultar historico participante paginacao.
	 * 
	 * @param event
	 *            the event
	 */
	public void consultarHistoricoParticipantePaginacao(ActionEvent event) {
		consultarHistoricoParticipantePesq();
	}

	/**
	 * Consultar historico participante pesq.
	 */
	private void consultarHistoricoParticipantePesq() {
		setIndicePesquisaHistSelecionado(null);

		try {
			setListaHistoricoPesquisa(getEnderecoService()
					.listarHistoricoEnderecoParticipante(
							getFiltroHistPesquisaEndereco()));
		} catch (PdcAdapterFunctionalException e) {
			setListaHistoricoPesquisa(new ArrayList<ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO>());
			throw e;
		} finally {
			setListaHistPesquisaSelectItems(preencherSelectItems(getListaHistoricoPesquisa()));
		}
	}

	/**
	 * Is acao historico.
	 * 
	 * @return true, if is acao historico
	 */
	public boolean isAcaoHistorico() {
		return ACAO_HISTORICO.equals(getBtoAcao());
	}

	/**
	 * Is pesquisa tipo servico desabilitada.
	 * 
	 * @return true, if is pesquisa tipo servico desabilitada
	 */
	public boolean isPesquisaTipoServicoDesabilitada() {
		return !isConsultaContratoEfetuada() || getRadioArgPesquisa() == null
				|| !getRadioArgPesquisa().equals("1");
	}

	/**
	 * Is combo arg pesquisa desabilitado.
	 * 
	 * @return true, if is combo arg pesquisa desabilitado
	 */
	public boolean isComboArgPesquisaDesabilitado() {
		Integer cdTipoServico = getFiltroPesquisaEndereco()
				.getCdProdutoServicoOperacao();
		return isPesquisaTipoServicoDesabilitada() || cdTipoServico == null
				|| cdTipoServico == 0;
	}

	/**
	 * Is consulta participante desabilitado.
	 * 
	 * @return true, if is consulta participante desabilitado
	 */
	public boolean isConsultaParticipanteDesabilitado() {
		return !isConsultaContratoEfetuada()
				|| radioArgPesquisa == null
				|| ("2".equals(radioArgPesquisa) && !isPesquisaParticipanteEfetuada());
	}

	/**
	 * Is pesquisa participante desabilitado.
	 * 
	 * @return true, if is pesquisa participante desabilitado
	 */
	public boolean isPesquisaParticipanteDesabilitado() {
		return !isConsultaContratoEfetuada() || radioArgPesquisa == null
				|| "1".equals(radioArgPesquisa);
	}

	/**
	 * Is combos tipo servico inc desabilitados.
	 * 
	 * @return true, if is combos tipo servico inc desabilitados
	 */
	public boolean isCombosTipoServicoIncDesabilitados() {
		Integer cdProdServ = getIncEndereco().getCdProdutoServicoOperacao();
		return cdProdServ == null || cdProdServ == 0;
	}

	/**
	 * Is cd uso endereco transacional.
	 * 
	 * @return true, if is cd uso endereco transacional
	 */
	public boolean isCdUsoEnderecoTransacional() {
		Integer cdUsoEnderecoSelecionado = getIncEndereco()
				.getCdUsoEnderecoPessoa();
		return cdUsoEnderecoSelecionado != null
				&& IEnderecoServiceConstants.CD_USO_ENDERECO_TRANSACIONAL
						.equals(cdUsoEnderecoSelecionado);
	}

	/**
	 * Selecionar tipo endereco.
	 */
	public void selecionarTipoEndereco() {
		if (!isCdUsoEnderecoTransacional() && getIncEndereco() != null) {
			getIncEndereco().setCdUsoPostal(null);
		}
	}

	/**
	 * Limpar qtd dias.
	 */
	public void limparQtdDias() {
		getIncEndereco().setCdUsoPostal(null);
	}

	/**
	 * Is validar selec endereco.
	 * 
	 * @return true, if is validar selec endereco
	 */
	public boolean isValidarSelecEndereco() {
		if ("2".equals(getIncEndereco().getRdoEnderecoPessoa())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Is verifica tipo endereco.
	 * 
	 * @return true, if is verifica tipo endereco
	 */
	public boolean isVerificaTipoEndereco() {
		if (incEndereco.getCdUsoEnderecoPessoa() == null
				|| incEndereco.getCdUsoEnderecoPessoa() < 2) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Voltar vinculacao.
	 * 
	 * @return the string
	 */
	public String voltarVinculacao() {
		return "conAlteraTpClassifParticContrato";
	}

	/**
	 * Voltar consulta.
	 * 
	 * @return the string
	 */
	public String voltarConsulta() {
		setParticipantePesquisa(new ListarParticipantesSaidaDTO());
		setBtoAcao("");
		consultarParticipanteVoltar();
		setIndicePesquisaSelecionado(null);

		return VINCULACAO;
	}

	/**
	 * Voltar.
	 * 
	 * @return the string
	 */
	public String voltar() {
		setBtoAcao("");
		return VINCULACAO;
	}

	/**
	 * Voltar confirmar.
	 * 
	 * @return the string
	 */
	public String voltarConfirmar() {
		if (ACAO_INCLUIR.equals(getBtoAcao())) {
			return PAGINA_INCLUSAO;
		} else if (ACAO_ALTERAR.equals(getBtoAcao())) {
			return PAGINA_ALTERACAO;
		} else {
			return voltarConsulta();
		}
	}

	/**
	 * Voltar detalhe.
	 * 
	 * @return the string
	 */
	public String voltarDetalhe() {
		if (isAcaoHistorico()) {
			return PAGINA_HISTORICO;
		} else {
			return voltar();
		}
	}

	/**
	 * Voltar participante.
	 * 
	 * @return the string
	 */
	public String voltarParticipante() {
		return redirecionarVoltaParticipante();
	}

	/**
	 * Redirecionar volta participante.
	 * 
	 * @return the string
	 */
	private String redirecionarVoltaParticipante() {
		if (ACAO_INCLUIR.equals(getBtoAcao())) {
			return PAGINA_INCLUSAO;
		} else if (ACAO_HISTORICO.equals(getBtoAcao())) {
			return PAGINA_HISTORICO;
		}

		return voltar();
	}

	/**
	 * Get: listaRadioEndereco.
	 * 
	 * @return listaRadioEndereco
	 */
	public List<SelectItem> getListaRadioEndereco() {
		List<SelectItem> listaRadioEndereco = new ArrayList<SelectItem>();
		listaRadioEndereco.add(new SelectItem(
				IEnderecoServiceConstants.CD_USO_ENDERECO_FIXO, ""));
		listaRadioEndereco.add(new SelectItem(
				IEnderecoServiceConstants.CD_USO_ENDERECO_TRANSACIONAL, ""));
		return listaRadioEndereco;
	}

	/**
	 * Get: listaEspecieEndereco.
	 * 
	 * @return listaEspecieEndereco
	 */
	public List<SelectItem> getListaEspecieEndereco() {
		List<SelectItem> listaEspecie = new ArrayList<SelectItem>();
		listaEspecie.add(new SelectItem(
				IEnderecoServiceConstants.CD_ESPECIE_ENDERECO, "Endere�o"));
		listaEspecie.add(new SelectItem(
				IEnderecoServiceConstants.CD_ESPECIE_EMAIL, "Email"));
		return listaEspecie;
	}

	/**
	 * Incluir.
	 * 
	 * @return the string
	 */
	public String incluir() {
		setPesquisaParticipanteInclusaoEfetuada(false);
		setPesquisaEnderecoInclusaoEfetuada(false);

		setIncEndereco(new IncluirEnderecoParticipanteEntradaDTO());
		setParticipanteInclusao(new ListarParticipantesSaidaDTO());
		setEnderecoSelecionado(new ListarEnderecoEmailOcorrenciasSaidaDTO());

		setDetalhe(new DetalheEnderecoDTO(getIdentificacaoClienteContratoBean()
				.getSaidaConsultarListaClientePessoas(),
				getIdentificacaoClienteBean()
						.getSaidaConsultarListaContratosPessoas()));

		setBtoAcao(ACAO_INCLUIR);

		return PAGINA_INCLUSAO;
	}

	/**
	 * Avancar incluir.
	 * 
	 * @return the string
	 */
	public String avancarIncluir() {
		ListarEnderecoParticipanteOcorrenciasSaidaDTO endParticipante = new ListarEnderecoParticipanteOcorrenciasSaidaDTO();
		endParticipante.setDsTipoServico(PgitUtil.obterSelectItemSelecionado(
				getListaTipoServico(), getIncEndereco()
						.getCdProdutoServicoOperacao()));
		endParticipante.setDsModalidadeServico(PgitUtil
				.obterSelectItemSelecionado(getListaModalidadeServicoInc(),
						getIncEndereco().getCdProdutoOperacaoRelacionado()));
		endParticipante.setDsOperacaoCatalogo(PgitUtil
				.obterSelectItemSelecionado(getListaOperacaoInc(),
						getIncEndereco().getCdOperacaoProdutoServico()));
		endParticipante.setCdCpfCnpj(getParticipanteInclusao().getCdCpfCnpj());
		endParticipante.setCdCnpjContrato(getParticipanteInclusao()
				.getCdFilialCnpj());
		endParticipante.setCdCpfCnpjDigito(getParticipanteInclusao()
				.getCdControleCnpj());
		endParticipante
				.setDsRazaoSocial(getParticipanteInclusao().getNmRazao());
		endParticipante.setCdTipoParticipacaoPessoa(getParticipanteInclusao()
				.getCdTipoParticipacao());
		endParticipante.setDsTipoParticipante(getParticipanteInclusao()
				.getDsTipoParticipacao());
		endParticipante
				.setCdDescricaoSituacaoParticapante(getParticipanteInclusao()
						.getDsSituacaoParticipacao());
		endParticipante.setCdUsoEnderecoPessoa(getIncEndereco()
				.getCdUsoEnderecoPessoa());
		endParticipante.setCdUsoPostal(getIncEndereco().getCdUsoPostal());

		getDetalhe().setParticipante(endParticipante);

		return "incConfAlteraTpClassifParticContrato";
	}

	/**
	 * Confirmar.
	 * 
	 * @return the string
	 */
	public String confirmar() {
		if (ACAO_INCLUIR.equals(getBtoAcao())) {
			return confirmarIncluir();
		} else if (ACAO_ALTERAR.equals(getBtoAcao())) {
			return confirmarAlterar();
		} else {
			return confirmarExcluir();
		}
	}

	/**
	 * Confirmar incluir.
	 * 
	 * @return the string
	 */
	private String confirmarIncluir() {
		ListarContratosPgitSaidaDTO dto = getIdentificacaoClienteContratoBean()
				.getListaGridPesquisa().get(
						getIdentificacaoClienteContratoBean()
								.getItemSelecionadoLista());

		incEndereco.setCdpessoaJuridicaContrato(dto.getCdPessoaJuridica());
		incEndereco.setCdTipoContratoNegocio(dto.getCdTipoContrato());
		incEndereco.setNrSequenciaContratoNegocio(dto.getNrSequenciaContrato());
		incEndereco.setCdPessoaJuridica(dto.getCdPessoaJuridica());

		incEndereco.setCdPessoa(getParticipanteInclusao().getCdPessoa());
		incEndereco.setCdTipoParticipacaoPessoa(getParticipanteInclusao()
				.getCdTipoParticipacao());

		incEndereco.setCdPessoaEndereco(getEnderecoSelecionado().getCdClub());
		incEndereco.setNrSequenciaEndereco(getEnderecoSelecionado()
				.getNrSequenciaEndereco());

		incEndereco.setCdProdutoOperacaoRelacionado(incEndereco
				.getCdProdutoServicoOperacao());

		for (int i = 0; i < listarServicosEmissaoSaida.size(); i++) {
			if (incEndereco.getCdProdutoOperacaoRelacionado()
					.equals(
							listarServicosEmissaoSaida.get(i)
									.getCdProdutoRelacionado())) {
				incEndereco.setCdTipoServico(listarServicosEmissaoSaida.get(i)
						.getCdProdutoOperacao());
				break;
			}
		}

		incEndereco.setCdEspeceEndereco(ESPECIE_ENDERECO);

		IncluirEnderecoParticipanteSaidaDTO msg = getEnderecoService()
				.incluirEnderecoParticipante(incEndereco);
		BradescoFacesUtils
				.addInfoModalMessage(
						"(" + msg.getCodMensagem() + ") " + msg.getMensagem(),
						"#{alteraTipoClassifParticContratoBean.consultarParticipanteVoltar}",
						BradescoViewExceptionActionType.ACTION, false);

		return "";
	}

	/**
	 * Volta pag consulta.
	 * 
	 * @return the string
	 */
	public String voltaPagConsulta() {
		setBtoAcao("");

		return VINCULACAO;
	}

	/**
	 * Confirmar alterar.
	 * 
	 * @return the string
	 */
	private String confirmarAlterar() {
		ListarEnderecoParticipanteOcorrenciasSaidaDTO participante = getDetalhe()
				.getParticipante();

		SubstituirVincEnderecoParticipanteEntradaDTO endAlterado = new SubstituirVincEnderecoParticipanteEntradaDTO();
		endAlterado.setCdpessoaJuridicaContrato(participante
				.getCdPessoaJuridicaContrato());
		endAlterado.setCdTipoContratoNegocio(participante
				.getCdTipoContratoNegocio());
		endAlterado.setNrSequenciaContratoNegocio(participante
				.getNrSequenciaContratoNegocio());
		endAlterado.setCdTipoParticipacaoPessoa(participante
				.getCdTipoParticipacaoPessoa());
		endAlterado.setCdPessoa(participante.getCdClub());
		endAlterado.setCdFinalidadeEnderecoContrato(participante
				.getCdFinalidadeEnderecoContrato());
		endAlterado.setCdPessoaEndereco(participante.getCdClub());
		endAlterado.setCdPessoaJuridicaEndereco(participante
				.getCdPessoaJuridicaContrato());
		endAlterado.setNrSequenciaEndereco(participante
				.getNrSequenciaEnderecoOpcional());
		endAlterado.setCdUsoEnderecoPessoa(Integer.parseInt(getIncEndereco()
				.getRdoEnderecoPessoa()));
		endAlterado.setCdUsoPostal(PgitUtil
				.verificaIntegerNulo(getIncEndereco().getCdUsoPostal()));
		endAlterado
				.setCdEspecieEndereco(getIncEndereco().getCdEspeceEndereco());
		endAlterado.setNrSequenciaEndereco(PgitUtil
				.verificaIntegerNulo(getEnderecoSelecionado()
						.getNrSequenciaEndereco()));

		SubstituirVincEnderecoParticipanteSaidaDTO msg = getEnderecoService()
				.alterarEnderecoParticipante(endAlterado);
		PgitFacesUtils
				.addInfoModalMessage(msg.getCodMensagem(), msg.getMensagem(),
						"#{alteraTipoClassifParticContratoBean.consultarParticipanteAlterar}");

		return null;
	}

	/**
	 * Confirmar excluir.
	 * 
	 * @return the string
	 */
	private String confirmarExcluir() {
		try {
			ListarEnderecoParticipanteOcorrenciasSaidaDTO participante = getDetalhe()
					.getParticipante();

			ExcluirEnderecoParticipanteEntradaDTO endExcluido = new ExcluirEnderecoParticipanteEntradaDTO();
			endExcluido.setCdpessoaJuridicaContrato(participante
					.getCdPessoaJuridicaContrato());
			endExcluido.setCdTipoContratoNegocio(participante
					.getCdTipoContratoNegocio());
			endExcluido.setNrSequenciaContratoNegocio(participante
					.getNrSequenciaContratoNegocio());
			endExcluido.setCdTipoParticipacaoPessoa(participante
					.getCdTipoParticipacaoPessoa());
			endExcluido.setCdPessoa(participante.getCdClub());
			endExcluido.setCdFinalidadeEnderecoContrato(participante
					.getCdFinalidadeEnderecoContrato());

			ExcluirEnderecoParticipanteSaidaDTO msg = getEnderecoService()
					.excluirEnderecoParticipante(endExcluido);
			BradescoFacesUtils
					.addInfoModalMessage(
							"(" + msg.getCodMensagem() + ") "
									+ msg.getMensagem(),
							"#{alteraTipoClassifParticContratoBean.consultarParticipanteVoltar}",
							BradescoViewExceptionActionType.ACTION, false);
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(e.getCode(), 8) + ") "
							+ e.getMessage(), false);
			return "";
		}

		return "";
	}

	/**
	 * Detalhar.
	 * 
	 * @return the string
	 */
	public String detalhar() {
		DetalheEnderecoDTO detalheEndereco = obterDetalheEndereco();

		if (detalheEndereco == null) {
			return null;
		}

		setDetalhe(detalheEndereco);

		preencheDadosParticipante();

		return "detAlteraTpClassifParticContrato";
	}

	/**
	 * Obter detalhe endereco.
	 * 
	 * @return the detalhe endereco dto
	 */
	private DetalheEnderecoDTO obterDetalheEndereco() {
		if (getIndicePesquisaSelecionado() == null) {
			return null;
		}

		ListarEnderecoParticipanteOcorrenciasSaidaDTO endSelecionado = getListaPesquisa()
				.get(getIndicePesquisaSelecionado());

		DatalharEnderecoParticipanteEntradaDTO entradaDetalhe = atribuirEntradaDetalhe(endSelecionado);

		DatalharEnderecoParticipanteSaidaDTO det = getEnderecoService()
				.detalharEnderecoParticipante(entradaDetalhe);

		return new DetalheEnderecoDTO(getIdentificacaoClienteContratoBean()
				.getSaidaConsultarListaClientePessoas(),
				getIdentificacaoClienteBean()
						.getSaidaConsultarListaContratosPessoas(),
				endSelecionado, det);
	}

	/**
	 * Atribuir entrada detalhe.
	 * 
	 * @param enderecoSelecionado
	 *            the endereco selecionado
	 * @return the datalhar endereco participante entrada dto
	 */
	private DatalharEnderecoParticipanteEntradaDTO atribuirEntradaDetalhe(
			ListarEnderecoParticipanteOcorrenciasSaidaDTO enderecoSelecionado) {
		DatalharEnderecoParticipanteEntradaDTO entradaDetalhe = new DatalharEnderecoParticipanteEntradaDTO();
		entradaDetalhe.setCdpessoaJuridicaContrato(enderecoSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDetalhe.setCdTipoContratoNegocio(enderecoSelecionado
				.getCdTipoContratoNegocio());
		entradaDetalhe.setNrSequenciaContratoNegocio(enderecoSelecionado
				.getNrSequenciaContratoNegocio());
		entradaDetalhe.setCdTipoParticipacaoPessoa(enderecoSelecionado
				.getCdTipoParticipacaoPessoa());
		entradaDetalhe.setCdCorpoCpfCnpj(enderecoSelecionado.getCdCpfCnpj());
		entradaDetalhe.setCdCnpjContrato(enderecoSelecionado
				.getCdCnpjContrato());
		entradaDetalhe.setCdCpfCnpjDigito(enderecoSelecionado
				.getCdCpfCnpjDigito());
		entradaDetalhe.setCdPessoa(enderecoSelecionado.getCdClub());
		entradaDetalhe.setCdFinalidadeEnderecoContrato(enderecoSelecionado
				.getCdFinalidadeEnderecoContrato());
		entradaDetalhe.setCdEspeceEndereco(enderecoSelecionado
				.getCdEspecieEndereco());
		entradaDetalhe.setNrSequenciaEnderecoOpcional(enderecoSelecionado
				.getNrSequenciaEnderecoOpcional());
		return entradaDetalhe;
	}

	/**
	 * Alterar.
	 * 
	 * @return the string
	 */
	public String alterar() {
		DetalheEnderecoDTO detalheEndereco = obterDetalheEndereco();

		if (detalheEndereco == null) {
			return null;
		}

		setDetalhe(detalheEndereco);

		IncluirEnderecoParticipanteEntradaDTO inclusao = new IncluirEnderecoParticipanteEntradaDTO();
		inclusao.setCdEspeceEndereco(PgitUtil.verificaIntegerNulo(getDetalhe()
				.getParticipante().getCdEspecieEndereco()));
		inclusao.setRdoEnderecoPessoa(PgitUtil.verificaStringNula(getDetalhe()
				.getParticipante().getCdUsoEnderecoPessoa().toString()));
		inclusao
				.setCdUsoPostal(inclusao.getRdoEnderecoPessoa().equals("2") ? PgitUtil
						.verificaIntegerNulo(getDetalhe().getManutencao()
								.getCdUsoPostal())
						: null);
		enderecoSelecionado.setNrSequenciaEndereco(listaPesquisa.get(
				getIndicePesquisaSelecionado())
				.getNrSequenciaEnderecoOpcional());

		setIncEndereco(inclusao);

		preencheDadosParticipante();

		setPesquisaEnderecoInclusaoEfetuada(true);
		setBtoAcao(ACAO_ALTERAR);

		setDsEndereco(getDetalhe().getManutencao().getDsEndereco());

		return PAGINA_ALTERACAO;
	}

	/**
	 * Avancar alterar.
	 * 
	 * @return the string
	 */
	public String avancarAlterar() {
		preencheDadosParticipante();

		return "altConfAlteraTpClassifParticContrato";
	}

	/**
	 * Preenche dados participante.
	 */
	public void preencheDadosParticipante() {
		// limparParticipante();

		if (listaPesquisa != null && !listaPesquisa.isEmpty()) {
			ListarEnderecoParticipanteOcorrenciasSaidaDTO lista = listaPesquisa
					.get(indicePesquisaSelecionado != null ? indicePesquisaSelecionado
							: 0);
			setDsTipoServico(lista.getDsModalidadeServico());
			setCpfCnpjParticipante(lista.getCpfCnpjFormatado());
			setNomeRazaoParticipante(lista.getDsRazaoSocial());
			setDsTipoParticipacao(lista.getCdTipoParticipacaoPessoa() + " - "
					+ lista.getDsTipoParticipante());
			setSituacaoParticipacao(lista.getCdDescricaoSituacaoParticapante());
		}
	}

	/**
	 * Preenche dados participante hist.
	 */
	public void preencheDadosParticipanteHist() {
		limparParticipante();

		if (listaHistoricoPesquisa != null && !listaHistoricoPesquisa.isEmpty()) {
			ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO lista = listaHistoricoPesquisa
					.get(indicePesquisaHistSelecionado != null ? indicePesquisaHistSelecionado
							: 0);
			setDsTipoServico(lista.getDsModalidadeServico());
			setCpfCnpjParticipante(lista.getCpfCnpjFormatado());
			setNomeRazaoParticipante(lista.getCdRazaoSocial());
			setDsTipoParticipacao(lista.getCdTipoParticipacaoPessoa() + " - "
					+ lista.getDsTipoParticipante());
			setSituacaoParticipacao(lista.getCdDescricaoSituacaoParticapante());
		}
	}

	/**
	 * Excluir.
	 * 
	 * @return the string
	 */
	public String excluir() {
		DetalheEnderecoDTO detalheEndereco = obterDetalheEndereco();

		if (detalheEndereco == null) {
			return null;
		}

		setDetalhe(detalheEndereco);

		IncluirEnderecoParticipanteEntradaDTO exclusao = new IncluirEnderecoParticipanteEntradaDTO();
		exclusao.setCdEspeceEndereco(PgitUtil.verificaIntegerNulo(getDetalhe()
				.getParticipante().getCdEspecieEndereco()));
		exclusao.setRdoEnderecoPessoa(PgitUtil.verificaStringNula(getDetalhe()
				.getParticipante().getCdUsoEnderecoPessoa().toString()));
		exclusao
				.setCdUsoPostal(exclusao.getRdoEnderecoPessoa().equals("2") ? PgitUtil
						.verificaIntegerNulo(getDetalhe().getManutencao()
								.getCdUsoPostal())
						: null);

		setIncEndereco(exclusao);

		preencheDadosParticipante();

		setBtoAcao(ACAO_EXCLUIR);

		return PAGINA_EXCLUSAO;
	}

	/**
	 * Historico.
	 * 
	 * @return the string
	 */
	public String historico() {
		setIndicePesquisaHistSelecionado(null);
		setRadioArgPesquisa(null);
		setFiltroHistPesquisaEndereco(new ListarHistoricoEnderecoParticipanteEntradaDTO());
		setParticipantePesquisa(new ListarParticipantesSaidaDTO());
		setListaHistoricoPesquisa(new ArrayList<ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO>());
		setDtManutencaoInicio(new Date());
		setDtManutencaoFim(new Date());
		setBtoAcao(ACAO_HISTORICO);
		if (indiceParticipanteSelecionado != null
				&& indicePesquisaSelecionado != null) {
			setRadioArgPesquisa("2");
			getParticipantePesquisa()
					.setCdCpfCnpj(
							listaPesquisa.get(indicePesquisaSelecionado)
									.getCdCpfCnpj());
			getParticipantePesquisa().setCdFilialCnpj(
					listaPesquisa.get(indicePesquisaSelecionado)
							.getCdCnpjContrato());
			getParticipantePesquisa().setCdControleCnpj(
					listaPesquisa.get(indicePesquisaSelecionado)
							.getCdCpfCnpjDigito());
			getParticipantePesquisa().setNmRazao(
					indicePesquisaSelecionado == null ? "" : listaPesquisa.get(
							indicePesquisaSelecionado).getDsRazaoSocial());
		}

		if (indiceParticipanteSelecionado == null
				&& indicePesquisaSelecionado != null) {
			setRadioArgPesquisa("2");
			getParticipantePesquisa()
					.setCdCpfCnpj(
							listaPesquisa.get(indicePesquisaSelecionado)
									.getCdCpfCnpj());
			getParticipantePesquisa().setCdFilialCnpj(
					listaPesquisa.get(indicePesquisaSelecionado)
							.getCdCnpjContrato());
			getParticipantePesquisa().setCdControleCnpj(
					listaPesquisa.get(indicePesquisaSelecionado)
							.getCdCpfCnpjDigito());
			getParticipantePesquisa().setNmRazao(
					indicePesquisaSelecionado == null ? "" : listaPesquisa.get(
							indicePesquisaSelecionado).getDsRazaoSocial());
		}
		return PAGINA_HISTORICO;
	}

	/**
	 * Detalhar historico.
	 * 
	 * @return the string
	 */
	public String detalharHistorico() {
		if (getIndicePesquisaHistSelecionado() == null) {
			return null;
		}

		ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO histSelecionado = getListaHistoricoPesquisa()
				.get(getIndicePesquisaHistSelecionado());

		DetalharHistoricoEnderecoParticipanteEntradaDTO filtroDetHist = atribuirEntradaDetalheHistorico(histSelecionado);

		DetalharHistoricoEnderecoParticipanteSaidaDTO detHist = getEnderecoService()
				.detalharHistoricoEnderecoParticipante(filtroDetHist);

		DatalharEnderecoParticipanteSaidaDTO det = atribuirSaidaDetalheHistorico(detHist);

		ListarEnderecoParticipanteOcorrenciasSaidaDTO end = atribuirSaidaPerticipanteHistorico(histSelecionado);

		preencheDadosParticipanteHist();

		setDetalhe(new DetalheEnderecoDTO(getIdentificacaoClienteContratoBean()
				.getSaidaConsultarListaClientePessoas(),
				getIdentificacaoClienteBean()
						.getSaidaConsultarListaContratosPessoas(), end, det));

		return "detHistAlteraTpClassifParticContrato";
	}

	/**
	 * Atribuir saida detalhe historico.
	 * 
	 * @param detHist
	 *            the det hist
	 * @return the datalhar endereco participante saida dto
	 */
	private DatalharEnderecoParticipanteSaidaDTO atribuirSaidaDetalheHistorico(
			DetalharHistoricoEnderecoParticipanteSaidaDTO detHist) {
		DatalharEnderecoParticipanteSaidaDTO det = new DatalharEnderecoParticipanteSaidaDTO();
		det.setCdEspeceEndereco(detHist.getCdEspeceEndereco());
		det.setDsEspecieEndereco(detHist.getDsEspecieEndereco());
		det.setCdNomeRazao(detHist.getCdNomeRazao());
		det.setDsLogradouroPagador(detHist.getDsLogradouroPagador());
		det
				.setDsNumeroLogradouroPagador(detHist
						.getDsNumeroLogradouroPagador());
		det.setDsComplementoLogradouroPagador(detHist
				.getDsComplementoLogradouroPagador());
		det.setDsBairroClientePagador(detHist.getDsBairroClientePagador());
		det
				.setDsMunicipioClientePagador(detHist
						.getDsMunicipioClientePagador());
		det.setCdSiglaUfPagador(detHist.getCdSiglaUfPagador());
		det.setDsPais(detHist.getDsPais());
		det.setCdCepPagador(detHist.getCdCepPagador());
		det.setCdCepComplementoPagador(detHist.getCdCepComplementoPagador());
		det.setDsCpfCnpjRecebedor(detHist.getDsCpfCnpjRecebedor());
		det.setDsFilialCnpjRecebedor(detHist.getDsFilialCnpjRecebedor());
		det.setDsControleCpfRecebedor(detHist.getDsControleCpfRecebedor());
		det.setDsNomeRazao(detHist.getDsNomeRazao());
		det.setCdEnderecoEletronico(detHist.getCdEnderecoEletronico());
		det.setCdUsuarioInclusao(detHist.getCdUsuarioInclusao());
		det.setHrInclusaoRegistro(detHist.getHrInclusaoRegistro());
		det.setCdOperacaoCanalInclusao(detHist.getCdOperacaoCanalInclusao());
		det.setCdTipoCanalInclusao(detHist.getCdTipoCanalInclusao());
		det.setDsTipoCanalInclusao(detHist.getDsTipoCanalInclusao());

		det.setCdUsuarioManutencao(detHist.getCdUsuarioManutencao());
		det.setHrManutencaoRegistro(detHist.getHrManutencaoRegistro());
		det
				.setCdOperacaoCanalManutencao(detHist
						.getCdOperacaoCanalManutencao());
		det.setCdTipoCanalManutencao(detHist.getCdTipoCanalManutencao());
		det.setDsTipoCanalManutencao(detHist.getDsTipoCanalManutencao());

		det.setCdTipo(detHist.getCdTipo());
		det.setCdUsoPostal(detHist.getCdUsoPostal());
		return det;
	}

	/**
	 * Atribuir saida perticipante historico.
	 * 
	 * @param histSelecionado
	 *            the hist selecionado
	 * @return the listar endereco participante ocorrencias saida dto
	 */
	private ListarEnderecoParticipanteOcorrenciasSaidaDTO atribuirSaidaPerticipanteHistorico(
			ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO histSelecionado) {
		ListarEnderecoParticipanteOcorrenciasSaidaDTO end = new ListarEnderecoParticipanteOcorrenciasSaidaDTO();
		end.setDsTipoServico(histSelecionado.getDsTipoServico());
		end.setDsModalidadeServico(histSelecionado.getDsModalidadeServico());
		end.setDsOperacaoCatalogo(histSelecionado.getDsOperacaoCatalogo());
		end.setCdCpfCnpj(histSelecionado.getCdCpfCnpj());
		end.setCdCnpjContrato(histSelecionado.getCdCnpjContrato());
		end.setCdCpfCnpjDigito(histSelecionado.getCdCpfCnpjDigito());
		end.setDsRazaoSocial(histSelecionado.getCdRazaoSocial());
		end.setCdTipoParticipacaoPessoa(histSelecionado
				.getCdTipoParticipacaoPessoa());
		end.setDsTipoParticipante(histSelecionado.getDsTipoParticipante());
		end.setCdDescricaoSituacaoParticapante(histSelecionado
				.getCdDescricaoSituacaoParticapante());
		end.setCdUsoEnderecoPessoa(histSelecionado.getCdUsoEnderecoPessoa());
		return end;
	}

	/**
	 * Atribuir entrada detalhe historico.
	 * 
	 * @param histSelecionado
	 *            the hist selecionado
	 * @return the detalhar historico endereco participante entrada dto
	 */
	private DetalharHistoricoEnderecoParticipanteEntradaDTO atribuirEntradaDetalheHistorico(
			ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO histSelecionado) {
		DetalharHistoricoEnderecoParticipanteEntradaDTO filtroDetHist = new DetalharHistoricoEnderecoParticipanteEntradaDTO();
		filtroDetHist.setCdpessoaJuridicaContrato(histSelecionado
				.getCdPessoaJuridicaContrato());
		filtroDetHist.setCdTipoContratoNegocio(histSelecionado
				.getCdTipoContratoNegocio());
		filtroDetHist.setNrSequenciaContratoNegocio(histSelecionado
				.getNrSequenciaContratoNegocio());
		filtroDetHist.setCdTipoParticipacaoPessoa(histSelecionado
				.getCdTipoParticipacaoPessoa());
		filtroDetHist.setCdCorpoCpfCnpj(histSelecionado.getCdCpfCnpj());
		filtroDetHist.setCdCnpjContrato(histSelecionado.getCdCnpjContrato());
		filtroDetHist.setCdCpfCnpjDigito(histSelecionado.getCdCpfCnpjDigito());
		filtroDetHist.setCdPessoa(histSelecionado.getCdClub());
		filtroDetHist.setCdFinalidadeEnderecoContrato(histSelecionado
				.getCdFinalidadeEnderecoContrato());
		filtroDetHist.setHrInclusaoRegistroHist(histSelecionado
				.getHrInclusaoRegistro());
		filtroDetHist
				.setCdEspeceEndereco(histSelecionado.getCdEspeceEndereco());
		filtroDetHist.setNrSequenciaEnderecoOpcional(histSelecionado
				.getNrSequenciaEndereco());
		return filtroDetHist;
	}

	/**
	 * Voltar endereco.
	 * 
	 * @return the string
	 */
	public String voltarEndereco() {
		return redirecionarVoltarEndereco();
	}

	/**
	 * Redirecionar voltar endereco.
	 * 
	 * @return the string
	 */
	private String redirecionarVoltarEndereco() {
		if (ACAO_INCLUIR.equals(getBtoAcao())) {
			return PAGINA_INCLUSAO;
		}
		return PAGINA_ALTERACAO;
	}

	/**
	 * Listar tipo servico.
	 */
	private void listarTipoServico() {
		this.listaTipoServico = new ArrayList<SelectItem>();
		ListarServicosEmissaoEntradaDTO entrada = new ListarServicosEmissaoEntradaDTO();
		ListarContratosPgitSaidaDTO dto = getIdentificacaoClienteContratoBean()
				.getListaGridPesquisa().get(
						getIdentificacaoClienteContratoBean()
								.getItemSelecionadoLista());

		entrada.setCdPessoaJuridicaContrato(dto.getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(dto.getCdTipoContrato());
		entrada.setNumSequenciaContratoNegocio(dto.getNrSequenciaContrato());

		listarServicosEmissaoSaida = comboService
				.listarServicosEmissao(entrada);

		for (ListarServicosEmissaoSaidaDTO saida : listarServicosEmissaoSaida) {
			this.listaTipoServico.add(new SelectItem(saida
					.getCdProdutoRelacionado(), saida.getDsServico()));
		}
	}

	/**
	 * Get: listaPesquisa.
	 * 
	 * @return listaPesquisa
	 */
	public List<ListarEnderecoParticipanteOcorrenciasSaidaDTO> getListaPesquisa() {
		return listaPesquisa;
	}

	/**
	 * Set: listaPesquisa.
	 * 
	 * @param listaPesquisa
	 *            the lista pesquisa
	 */
	public void setListaPesquisa(
			List<ListarEnderecoParticipanteOcorrenciasSaidaDTO> listaPesquisa) {
		this.listaPesquisa = listaPesquisa;
	}

	/**
	 * Get: listaParticipantes.
	 * 
	 * @return listaParticipantes
	 */
	public List<ListarParticipantesSaidaDTO> getListaParticipantes() {
		return listaParticipantes;
	}

	/**
	 * Set: listaParticipantes.
	 * 
	 * @param listaParticipantes
	 *            the lista participantes
	 */
	public void setListaParticipantes(
			List<ListarParticipantesSaidaDTO> listaParticipantes) {
		this.listaParticipantes = listaParticipantes;
	}

	/**
	 * Get: listaEmpresaGestora.
	 * 
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 * 
	 * @param listaEmpresaGestora
	 *            the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: listaTipoContrato.
	 * 
	 * @return listaTipoContrato
	 */
	public List<SelectItem> getListaTipoContrato() {
		return listaTipoContrato;
	}

	/**
	 * Set: listaTipoContrato.
	 * 
	 * @param tipoContrato
	 *            the lista tipo contrato
	 */
	public void setListaTipoContrato(List<SelectItem> tipoContrato) {
		this.listaTipoContrato = tipoContrato;
	}

	/**
	 * Get: listaModalidadeServico.
	 * 
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {
		return listaModalidadeServico;
	}

	/**
	 * Set: listaModalidadeServico.
	 * 
	 * @param listaModalidadeServico
	 *            the lista modalidade servico
	 */
	public void setListaModalidadeServico(
			List<SelectItem> listaModalidadeServico) {
		this.listaModalidadeServico = listaModalidadeServico;
	}

	/**
	 * Get: listaOperacao.
	 * 
	 * @return listaOperacao
	 */
	public List<SelectItem> getListaOperacao() {
		return listaOperacao;
	}

	/**
	 * Set: listaOperacao.
	 * 
	 * @param listaOperacao
	 *            the lista operacao
	 */
	public void setListaOperacao(List<SelectItem> listaOperacao) {
		this.listaOperacao = listaOperacao;
	}

	/**
	 * Get: listaTipoServico.
	 * 
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 * 
	 * @param listaTipoServico
	 *            the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 * 
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 * 
	 * @param identificacaoClienteContratoBean
	 *            the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: identificacaoClienteBean.
	 * 
	 * @return identificacaoClienteBean
	 */
	public IdentificacaoClienteBean getIdentificacaoClienteBean() {
		return identificacaoClienteBean;
	}

	/**
	 * Set: identificacaoClienteBean.
	 * 
	 * @param identificacaoClienteBean
	 *            the identificacao cliente bean
	 */
	public void setIdentificacaoClienteBean(
			IdentificacaoClienteBean identificacaoClienteBean) {
		this.identificacaoClienteBean = identificacaoClienteBean;
	}

	/**
	 * Get: comboService.
	 * 
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 * 
	 * @param comboService
	 *            the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: enderecoService.
	 * 
	 * @return enderecoService
	 */
	public IEnderecoService getEnderecoService() {
		return enderecoService;
	}

	/**
	 * Set: enderecoService.
	 * 
	 * @param enderecoService
	 *            the endereco service
	 */
	public void setEnderecoService(IEnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}

	/**
	 * Get: filtroPesquisaEndereco.
	 * 
	 * @return filtroPesquisaEndereco
	 */
	public ListarEnderecoParticipanteEntradaDTO getFiltroPesquisaEndereco() {
		return filtroPesquisaEndereco;
	}

	/**
	 * Set: filtroPesquisaEndereco.
	 * 
	 * @param filtroPesquisaEndereco
	 *            the filtro pesquisa endereco
	 */
	public void setFiltroPesquisaEndereco(
			ListarEnderecoParticipanteEntradaDTO filtroPesquisaEndereco) {
		this.filtroPesquisaEndereco = filtroPesquisaEndereco;
	}

	/**
	 * Get: participantePesquisa.
	 * 
	 * @return participantePesquisa
	 */
	public ListarParticipantesSaidaDTO getParticipantePesquisa() {
		return participantePesquisa;
	}

	/**
	 * Set: participantePesquisa.
	 * 
	 * @param participantePesquisa
	 *            the participante pesquisa
	 */
	public void setParticipantePesquisa(
			ListarParticipantesSaidaDTO participantePesquisa) {
		this.participantePesquisa = participantePesquisa;
	}

	/**
	 * Is consulta contrato efetuada.
	 * 
	 * @return true, if is consulta contrato efetuada
	 */
	public boolean isConsultaContratoEfetuada() {
		return consultaContratoEfetuada;
	}

	/**
	 * Set: consultaContratoEfetuada.
	 * 
	 * @param consultaContratoEfetuada
	 *            the consulta contrato efetuada
	 */
	public void setConsultaContratoEfetuada(boolean consultaContratoEfetuada) {
		this.consultaContratoEfetuada = consultaContratoEfetuada;
	}

	/**
	 * Is pesquisa participante efetuada.
	 * 
	 * @return true, if is pesquisa participante efetuada
	 */
	public boolean isPesquisaParticipanteEfetuada() {
		return pesquisaParticipanteEfetuada;
	}

	/**
	 * Set: pesquisaParticipanteEfetuada.
	 * 
	 * @param pesquisaParticipanteEfetuada
	 *            the pesquisa participante efetuada
	 */
	public void setPesquisaParticipanteEfetuada(
			boolean pesquisaParticipanteEfetuada) {
		this.pesquisaParticipanteEfetuada = pesquisaParticipanteEfetuada;
	}

	/**
	 * Get: listaParticipantesItems.
	 * 
	 * @return listaParticipantesItems
	 */
	public List<SelectItem> getListaParticipantesItems() {
		return listaParticipantesItems;
	}

	/**
	 * Set: listaParticipantesItems.
	 * 
	 * @param listaParticipantesItems
	 *            the lista participantes items
	 */
	public void setListaParticipantesItems(
			List<SelectItem> listaParticipantesItems) {
		this.listaParticipantesItems = listaParticipantesItems;
	}

	/**
	 * Get: detalhe.
	 * 
	 * @return detalhe
	 */
	public DetalheEnderecoDTO getDetalhe() {
		return detalhe;
	}

	/**
	 * Set: detalhe.
	 * 
	 * @param detalhe
	 *            the detalhe
	 */
	public void setDetalhe(DetalheEnderecoDTO detalhe) {
		this.detalhe = detalhe;
	}

	/**
	 * Get: indiceParticipanteSelecionado.
	 * 
	 * @return indiceParticipanteSelecionado
	 */
	public Integer getIndiceParticipanteSelecionado() {
		return indiceParticipanteSelecionado;
	}

	/**
	 * Set: indiceParticipanteSelecionado.
	 * 
	 * @param indiceParticipanteSelecionado
	 *            the indice participante selecionado
	 */
	public void setIndiceParticipanteSelecionado(
			Integer indiceParticipanteSelecionado) {
		this.indiceParticipanteSelecionado = indiceParticipanteSelecionado;
	}

	/**
	 * Get: radioArgPesquisa.
	 * 
	 * @return radioArgPesquisa
	 */
	public String getRadioArgPesquisa() {
		return radioArgPesquisa;
	}

	/**
	 * Set: radioArgPesquisa.
	 * 
	 * @param radioArgPesquisa
	 *            the radio arg pesquisa
	 */
	public void setRadioArgPesquisa(String radioArgPesquisa) {
		this.radioArgPesquisa = radioArgPesquisa;
	}

	/**
	 * Get: manterContratoService.
	 * 
	 * @return manterContratoService
	 */
	public IManterContratoService getManterContratoService() {
		return manterContratoService;
	}

	/**
	 * Set: manterContratoService.
	 * 
	 * @param manterContratoService
	 *            the manter contrato service
	 */
	public void setManterContratoService(
			IManterContratoService manterContratoService) {
		this.manterContratoService = manterContratoService;
	}

	/**
	 * Get: listaPesquisaSelectItems.
	 * 
	 * @return listaPesquisaSelectItems
	 */
	public List<SelectItem> getListaPesquisaSelectItems() {
		return listaPesquisaSelectItems;
	}

	/**
	 * Set: listaPesquisaSelectItems.
	 * 
	 * @param listaPesquisaSelectItems
	 *            the lista pesquisa select items
	 */
	public void setListaPesquisaSelectItems(
			List<SelectItem> listaPesquisaSelectItems) {
		this.listaPesquisaSelectItems = listaPesquisaSelectItems;
	}

	/**
	 * Get: indicePesquisaSelecionado.
	 * 
	 * @return indicePesquisaSelecionado
	 */
	public Integer getIndicePesquisaSelecionado() {
		return indicePesquisaSelecionado;
	}

	/**
	 * Set: indicePesquisaSelecionado.
	 * 
	 * @param indicePesquisaSelecionado
	 *            the indice pesquisa selecionado
	 */
	public void setIndicePesquisaSelecionado(Integer indicePesquisaSelecionado) {
		this.indicePesquisaSelecionado = indicePesquisaSelecionado;
	}

	/**
	 * Get: listaModalidadeServicoInc.
	 * 
	 * @return listaModalidadeServicoInc
	 */
	public List<SelectItem> getListaModalidadeServicoInc() {
		return listaModalidadeServicoInc;
	}

	/**
	 * Set: listaModalidadeServicoInc.
	 * 
	 * @param listaModalidadeServicoInc
	 *            the lista modalidade servico inc
	 */
	public void setListaModalidadeServicoInc(
			List<SelectItem> listaModalidadeServicoInc) {
		this.listaModalidadeServicoInc = listaModalidadeServicoInc;
	}

	/**
	 * Get: listaOperacaoInc.
	 * 
	 * @return listaOperacaoInc
	 */
	public List<SelectItem> getListaOperacaoInc() {
		return listaOperacaoInc;
	}

	/**
	 * Set: listaOperacaoInc.
	 * 
	 * @param listaOperacaoInc
	 *            the lista operacao inc
	 */
	public void setListaOperacaoInc(List<SelectItem> listaOperacaoInc) {
		this.listaOperacaoInc = listaOperacaoInc;
	}

	/**
	 * Get: incEndereco.
	 * 
	 * @return incEndereco
	 */
	public IncluirEnderecoParticipanteEntradaDTO getIncEndereco() {
		return incEndereco;
	}

	/**
	 * Set: incEndereco.
	 * 
	 * @param incEndereco
	 *            the inc endereco
	 */
	public void setIncEndereco(IncluirEnderecoParticipanteEntradaDTO incEndereco) {
		this.incEndereco = incEndereco;
	}

	/**
	 * Get: participanteInclusao.
	 * 
	 * @return participanteInclusao
	 */
	public ListarParticipantesSaidaDTO getParticipanteInclusao() {
		return participanteInclusao;
	}

	/**
	 * Set: participanteInclusao.
	 * 
	 * @param participanteInclusao
	 *            the participante inclusao
	 */
	public void setParticipanteInclusao(
			ListarParticipantesSaidaDTO participanteInclusao) {
		this.participanteInclusao = participanteInclusao;
	}

	/**
	 * Get: listaEnderecoItems.
	 * 
	 * @return listaEnderecoItems
	 */
	public List<SelectItem> getListaEnderecoItems() {
		return listaEnderecoItems;
	}

	/**
	 * Set: listaEnderecoItems.
	 * 
	 * @param listaEnderecoItems
	 *            the lista endereco items
	 */
	public void setListaEnderecoItems(List<SelectItem> listaEnderecoItems) {
		this.listaEnderecoItems = listaEnderecoItems;
	}

	/**
	 * Set: listaEndereco.
	 * 
	 * @param listaEndereco
	 *            the lista endereco
	 */
	public void setListaEndereco(
			List<ListarEnderecoEmailOcorrenciasSaidaDTO> listaEndereco) {
		this.listaEndereco = listaEndereco;
	}

	/**
	 * Get: listaEndereco.
	 * 
	 * @return listaEndereco
	 */
	public List<ListarEnderecoEmailOcorrenciasSaidaDTO> getListaEndereco() {
		return listaEndereco;
	}

	/**
	 * Get: listaHistoricoPesquisa.
	 * 
	 * @return listaHistoricoPesquisa
	 */
	public List<ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO> getListaHistoricoPesquisa() {
		return listaHistoricoPesquisa;
	}

	/**
	 * Set: listaHistoricoPesquisa.
	 * 
	 * @param listaHistoricoPesquisa
	 *            the lista historico pesquisa
	 */
	public void setListaHistoricoPesquisa(
			List<ListarHistoricoEnderecoParticipanteOcorrenciasSaidaDTO> listaHistoricoPesquisa) {
		this.listaHistoricoPesquisa = listaHistoricoPesquisa;
	}

	/**
	 * Get: listaHistPesquisaSelectItems.
	 * 
	 * @return listaHistPesquisaSelectItems
	 */
	public List<SelectItem> getListaHistPesquisaSelectItems() {
		return listaHistPesquisaSelectItems;
	}

	/**
	 * Set: listaHistPesquisaSelectItems.
	 * 
	 * @param listaHistPesquisaSelectItems
	 *            the lista hist pesquisa select items
	 */
	public void setListaHistPesquisaSelectItems(
			List<SelectItem> listaHistPesquisaSelectItems) {
		this.listaHistPesquisaSelectItems = listaHistPesquisaSelectItems;
	}

	/**
	 * Get: enderecoSelecionado.
	 * 
	 * @return enderecoSelecionado
	 */
	public ListarEnderecoEmailOcorrenciasSaidaDTO getEnderecoSelecionado() {
		return enderecoSelecionado;
	}

	/**
	 * Set: enderecoSelecionado.
	 * 
	 * @param enderecoSelecionado
	 *            the endereco selecionado
	 */
	public void setEnderecoSelecionado(
			ListarEnderecoEmailOcorrenciasSaidaDTO enderecoSelecionado) {
		this.enderecoSelecionado = enderecoSelecionado;
	}

	/**
	 * Is pesquisa endereco inclusao efetuada.
	 * 
	 * @return true, if is pesquisa endereco inclusao efetuada
	 */
	public boolean isPesquisaEnderecoInclusaoEfetuada() {
		return pesquisaEnderecoInclusaoEfetuada;
	}

	/**
	 * Set: pesquisaEnderecoInclusaoEfetuada.
	 * 
	 * @param pesquisaEnderecoInclusaoEfetuada
	 *            the pesquisa endereco inclusao efetuada
	 */
	public void setPesquisaEnderecoInclusaoEfetuada(
			boolean pesquisaEnderecoInclusaoEfetuada) {
		this.pesquisaEnderecoInclusaoEfetuada = pesquisaEnderecoInclusaoEfetuada;
	}

	/**
	 * Is pesquisa participante inclusao efetuada.
	 * 
	 * @return true, if is pesquisa participante inclusao efetuada
	 */
	public boolean isPesquisaParticipanteInclusaoEfetuada() {
		return pesquisaParticipanteInclusaoEfetuada;
	}

	/**
	 * Set: pesquisaParticipanteInclusaoEfetuada.
	 * 
	 * @param pesquisaParticipanteInclusaoEfetuada
	 *            the pesquisa participante inclusao efetuada
	 */
	public void setPesquisaParticipanteInclusaoEfetuada(
			boolean pesquisaParticipanteInclusaoEfetuada) {
		this.pesquisaParticipanteInclusaoEfetuada = pesquisaParticipanteInclusaoEfetuada;
	}

	/**
	 * Get: filtroHistPesquisaEndereco.
	 * 
	 * @return filtroHistPesquisaEndereco
	 */
	public ListarHistoricoEnderecoParticipanteEntradaDTO getFiltroHistPesquisaEndereco() {
		return filtroHistPesquisaEndereco;
	}

	/**
	 * Set: filtroHistPesquisaEndereco.
	 * 
	 * @param filtroHistPesquisaEndereco
	 *            the filtro hist pesquisa endereco
	 */
	public void setFiltroHistPesquisaEndereco(
			ListarHistoricoEnderecoParticipanteEntradaDTO filtroHistPesquisaEndereco) {
		this.filtroHistPesquisaEndereco = filtroHistPesquisaEndereco;
	}

	/**
	 * Get: btoAcao.
	 * 
	 * @return btoAcao
	 */
	public String getBtoAcao() {
		return btoAcao;
	}

	/**
	 * Set: btoAcao.
	 * 
	 * @param btoAcao
	 *            the bto acao
	 */
	public void setBtoAcao(String btoAcao) {
		this.btoAcao = btoAcao;
	}

	/**
	 * Get: indicePesquisaHistSelecionado.
	 * 
	 * @return indicePesquisaHistSelecionado
	 */
	public Integer getIndicePesquisaHistSelecionado() {
		return indicePesquisaHistSelecionado;
	}

	/**
	 * Set: indicePesquisaHistSelecionado.
	 * 
	 * @param indicePesquisaHistSelecionado
	 *            the indice pesquisa hist selecionado
	 */
	public void setIndicePesquisaHistSelecionado(
			Integer indicePesquisaHistSelecionado) {
		this.indicePesquisaHistSelecionado = indicePesquisaHistSelecionado;
	}

	/**
	 * Get: indiceEnderecoSelecionado.
	 * 
	 * @return indiceEnderecoSelecionado
	 */
	public Integer getIndiceEnderecoSelecionado() {
		return indiceEnderecoSelecionado;
	}

	/**
	 * Set: indiceEnderecoSelecionado.
	 * 
	 * @param indiceEnderecoSelecionado
	 *            the indice endereco selecionado
	 */
	public void setIndiceEnderecoSelecionado(Integer indiceEnderecoSelecionado) {
		this.indiceEnderecoSelecionado = indiceEnderecoSelecionado;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 * 
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 * 
	 * @param filtroAgendamentoEfetivacaoEstornoBean
	 *            the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Get: listaControleRadio.
	 * 
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 * 
	 * @param listaControleRadio
	 *            the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 * 
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 * 
	 * @param itemSelecionadoLista
	 *            the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: habilitaBtoCont.
	 * 
	 * @return habilitaBtoCont
	 */
	public Boolean getHabilitaBtoCont() {
		return habilitaBtoCont;
	}

	/**
	 * Set: habilitaBtoCont.
	 * 
	 * @param habilitaBtoCont
	 *            the habilita bto cont
	 */
	public void setHabilitaBtoCont(Boolean habilitaBtoCont) {
		this.habilitaBtoCont = habilitaBtoCont;
	}

	/**
	 * Get: foco.
	 * 
	 * @return foco
	 */
	public String getFoco() {
		return foco;
	}

	/**
	 * Set: foco.
	 * 
	 * @param foco
	 *            the foco
	 */
	public void setFoco(String foco) {
		this.foco = foco;
	}

	/**
	 * Get: cpfCnpjRepresentante.
	 * 
	 * @return cpfCnpjRepresentante
	 */
	public String getCpfCnpjRepresentante() {
		return cpfCnpjRepresentante;
	}

	/**
	 * Set: cpfCnpjRepresentante.
	 * 
	 * @param cpfCnpjRepresentante
	 *            the cpf cnpj representante
	 */
	public void setCpfCnpjRepresentante(String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	/**
	 * Get: nomeRazaoRepresentante.
	 * 
	 * @return nomeRazaoRepresentante
	 */
	public String getNomeRazaoRepresentante() {
		return nomeRazaoRepresentante;
	}

	/**
	 * Set: nomeRazaoRepresentante.
	 * 
	 * @param nomeRazaoRepresentante
	 *            the nome razao representante
	 */
	public void setNomeRazaoRepresentante(String nomeRazaoRepresentante) {
		this.nomeRazaoRepresentante = nomeRazaoRepresentante;
	}

	/**
	 * Get: grupEconRepresentante.
	 * 
	 * @return grupEconRepresentante
	 */
	public String getGrupEconRepresentante() {
		return grupEconRepresentante;
	}

	/**
	 * Set: grupEconRepresentante.
	 * 
	 * @param grupEconRepresentante
	 *            the grup econ representante
	 */
	public void setGrupEconRepresentante(String grupEconRepresentante) {
		this.grupEconRepresentante = grupEconRepresentante;
	}

	/**
	 * Get: ativEconRepresentante.
	 * 
	 * @return ativEconRepresentante
	 */
	public String getAtivEconRepresentante() {
		return ativEconRepresentante;
	}

	/**
	 * Set: ativEconRepresentante.
	 * 
	 * @param ativEconRepresentante
	 *            the ativ econ representante
	 */
	public void setAtivEconRepresentante(String ativEconRepresentante) {
		this.ativEconRepresentante = ativEconRepresentante;
	}

	/**
	 * Get: segRepresentante.
	 * 
	 * @return segRepresentante
	 */
	public String getSegRepresentante() {
		return segRepresentante;
	}

	/**
	 * Set: segRepresentante.
	 * 
	 * @param segRepresentante
	 *            the seg representante
	 */
	public void setSegRepresentante(String segRepresentante) {
		this.segRepresentante = segRepresentante;
	}

	/**
	 * Get: subSegRepresentante.
	 * 
	 * @return subSegRepresentante
	 */
	public String getSubSegRepresentante() {
		return subSegRepresentante;
	}

	/**
	 * Set: subSegRepresentante.
	 * 
	 * @param subSegRepresentante
	 *            the sub seg representante
	 */
	public void setSubSegRepresentante(String subSegRepresentante) {
		this.subSegRepresentante = subSegRepresentante;
	}

	/**
	 * Get: dsAgenciaGestora.
	 * 
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 * 
	 * @param dsAgenciaGestora
	 *            the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 * 
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 * 
	 * @param dsGerenteResponsavel
	 *            the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: gerenteResponsavelFormatado.
	 * 
	 * @return gerenteResponsavelFormatado
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}

	/**
	 * Set: gerenteResponsavelFormatado.
	 * 
	 * @param gerenteResponsavelFormatado
	 *            the gerente responsavel formatado
	 */
	public void setGerenteResponsavelFormatado(
			String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 * 
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 * 
	 * @param cpfCnpjParticipante
	 *            the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 * 
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 * 
	 * @param nomeRazaoParticipante
	 *            the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: cpfCnpjParticipanteTemp.
	 * 
	 * @return cpfCnpjParticipanteTemp
	 */
	public String getCpfCnpjParticipanteTemp() {
		return cpfCnpjParticipanteTemp;
	}

	/**
	 * Set: cpfCnpjParticipanteTemp.
	 * 
	 * @param cpfCnpjParticipanteTemp
	 *            the cpf cnpj participante temp
	 */
	public void setCpfCnpjParticipanteTemp(String cpfCnpjParticipanteTemp) {
		this.cpfCnpjParticipanteTemp = cpfCnpjParticipanteTemp;
	}

	/**
	 * Get: nomeRazaoParticipanteTemp.
	 * 
	 * @return nomeRazaoParticipanteTemp
	 */
	public String getNomeRazaoParticipanteTemp() {
		return nomeRazaoParticipanteTemp;
	}

	/**
	 * Set: nomeRazaoParticipanteTemp.
	 * 
	 * @param nomeRazaoParticipanteTemp
	 *            the nome razao participante temp
	 */
	public void setNomeRazaoParticipanteTemp(String nomeRazaoParticipanteTemp) {
		this.nomeRazaoParticipanteTemp = nomeRazaoParticipanteTemp;
	}

	/**
	 * Get: cpfCnpjParticipanteDet.
	 * 
	 * @return cpfCnpjParticipanteDet
	 */
	public String getCpfCnpjParticipanteDet() {
		return cpfCnpjParticipanteDet;
	}

	/**
	 * Set: cpfCnpjParticipanteDet.
	 * 
	 * @param cpfCnpjParticipanteDet
	 *            the cpf cnpj participante det
	 */
	public void setCpfCnpjParticipanteDet(String cpfCnpjParticipanteDet) {
		this.cpfCnpjParticipanteDet = cpfCnpjParticipanteDet;
	}

	/**
	 * Get: nomeRazaoParticipanteDet.
	 * 
	 * @return nomeRazaoParticipanteDet
	 */
	public String getNomeRazaoParticipanteDet() {
		return nomeRazaoParticipanteDet;
	}

	/**
	 * Set: nomeRazaoParticipanteDet.
	 * 
	 * @param nomeRazaoParticipanteDet
	 *            the nome razao participante det
	 */
	public void setNomeRazaoParticipanteDet(String nomeRazaoParticipanteDet) {
		this.nomeRazaoParticipanteDet = nomeRazaoParticipanteDet;
	}

	/**
	 * Get: grupoEconParticipante.
	 * 
	 * @return grupoEconParticipante
	 */
	public String getGrupoEconParticipante() {
		return grupoEconParticipante;
	}

	/**
	 * Set: grupoEconParticipante.
	 * 
	 * @param grupoEconParticipante
	 *            the grupo econ participante
	 */
	public void setGrupoEconParticipante(String grupoEconParticipante) {
		this.grupoEconParticipante = grupoEconParticipante;
	}

	/**
	 * Get: atividadeEconParticipante.
	 * 
	 * @return atividadeEconParticipante
	 */
	public String getAtividadeEconParticipante() {
		return atividadeEconParticipante;
	}

	/**
	 * Set: atividadeEconParticipante.
	 * 
	 * @param atividadeEconParticipante
	 *            the atividade econ participante
	 */
	public void setAtividadeEconParticipante(String atividadeEconParticipante) {
		this.atividadeEconParticipante = atividadeEconParticipante;
	}

	/**
	 * Get: segmentoParticipante.
	 * 
	 * @return segmentoParticipante
	 */
	public String getSegmentoParticipante() {
		return segmentoParticipante;
	}

	/**
	 * Set: segmentoParticipante.
	 * 
	 * @param segmentoParticipante
	 *            the segmento participante
	 */
	public void setSegmentoParticipante(String segmentoParticipante) {
		this.segmentoParticipante = segmentoParticipante;
	}

	/**
	 * Get: subSegmentoParticipante.
	 * 
	 * @return subSegmentoParticipante
	 */
	public String getSubSegmentoParticipante() {
		return subSegmentoParticipante;
	}

	/**
	 * Set: subSegmentoParticipante.
	 * 
	 * @param subSegmentoParticipante
	 *            the sub segmento participante
	 */
	public void setSubSegmentoParticipante(String subSegmentoParticipante) {
		this.subSegmentoParticipante = subSegmentoParticipante;
	}

	/**
	 * Get: empresaContrato.
	 * 
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 * 
	 * @param empresaContrato
	 *            the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: tipoContrato.
	 * 
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 * 
	 * @param tipoContrato
	 *            the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: numeroContrato.
	 * 
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 * 
	 * @param numeroContrato
	 *            the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: descricaoContrato.
	 * 
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 * 
	 * @param descricaoContrato
	 *            the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: situacaoContrato.
	 * 
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 * 
	 * @param situacaoContrato
	 *            the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: motivoContrato.
	 * 
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 * 
	 * @param motivoContrato
	 *            the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: participacaoContrato.
	 * 
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 * 
	 * @param participacaoContrato
	 *            the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: dtoListaParticipantes.
	 * 
	 * @return dtoListaParticipantes
	 */
	public ListarParticipantesSaidaDTO getDtoListaParticipantes() {
		return dtoListaParticipantes;
	}

	/**
	 * Set: dtoListaParticipantes.
	 * 
	 * @param dtoListaParticipantes
	 *            the dto lista participantes
	 */
	public void setDtoListaParticipantes(
			ListarParticipantesSaidaDTO dtoListaParticipantes) {
		this.dtoListaParticipantes = dtoListaParticipantes;
	}

	/**
	 * Get: listarServicosEmissaoSaida.
	 * 
	 * @return listarServicosEmissaoSaida
	 */
	public List<ListarServicosEmissaoSaidaDTO> getListarServicosEmissaoSaida() {
		return listarServicosEmissaoSaida;
	}

	/**
	 * Set: listarServicosEmissaoSaida.
	 * 
	 * @param listarServicosEmissaoSaida
	 *            the listar servicos emissao saida
	 */
	public void setListarServicosEmissaoSaida(
			List<ListarServicosEmissaoSaidaDTO> listarServicosEmissaoSaida) {
		this.listarServicosEmissaoSaida = listarServicosEmissaoSaida;
	}

	/**
	 * Is habilita qtd dias.
	 * 
	 * @return true, if is habilita qtd dias
	 */
	public boolean isHabilitaQtdDias() {
		return habilitaQtdDias;
	}

	/**
	 * Set: habilitaQtdDias.
	 * 
	 * @param habilitaQtdDias
	 *            the habilita qtd dias
	 */
	public void setHabilitaQtdDias(boolean habilitaQtdDias) {
		this.habilitaQtdDias = habilitaQtdDias;
	}

	/**
	 * Get: dtManutencaoInicio.
	 * 
	 * @return dtManutencaoInicio
	 */
	public Date getDtManutencaoInicio() {
		return dtManutencaoInicio;
	}

	/**
	 * Set: dtManutencaoInicio.
	 * 
	 * @param dtManutencaoInicio
	 *            the dt manutencao inicio
	 */
	public void setDtManutencaoInicio(Date dtManutencaoInicio) {
		this.dtManutencaoInicio = dtManutencaoInicio;
	}

	/**
	 * Get: dtManutencaoFim.
	 * 
	 * @return dtManutencaoFim
	 */
	public Date getDtManutencaoFim() {
		return dtManutencaoFim;
	}

	/**
	 * Set: dtManutencaoFim.
	 * 
	 * @param dtManutencaoFim
	 *            the dt manutencao fim
	 */
	public void setDtManutencaoFim(Date dtManutencaoFim) {
		this.dtManutencaoFim = dtManutencaoFim;
	}

	/**
	 * Get: situacaoParticipacao.
	 * 
	 * @return situacaoParticipacao
	 */
	public String getSituacaoParticipacao() {
		return situacaoParticipacao;
	}

	/**
	 * Set: situacaoParticipacao.
	 * 
	 * @param situacaoParticipacao
	 *            the situacao participacao
	 */
	public void setSituacaoParticipacao(String situacaoParticipacao) {
		this.situacaoParticipacao = situacaoParticipacao;
	}

	/**
	 * Get: dsTipoParticipacao.
	 * 
	 * @return dsTipoParticipacao
	 */
	public String getDsTipoParticipacao() {
		return dsTipoParticipacao;
	}

	/**
	 * Set: dsTipoParticipacao.
	 * 
	 * @param dsTipoParticipacao
	 *            the ds tipo participacao
	 */
	public void setDsTipoParticipacao(String dsTipoParticipacao) {
		this.dsTipoParticipacao = dsTipoParticipacao;
	}

	/**
	 * Get: dsTipoServico.
	 * 
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 * 
	 * @param dsTipoServico
	 *            the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * @return the dsLogradouro
	 */
	public String getDsLogradouro() {
		return dsLogradouro;
	}

	/**
	 * @param dsLogradouro
	 *            the dsLogradouro to set
	 */
	public void setDsLogradouro(String dsLogradouro) {
		this.dsLogradouro = dsLogradouro;
	}

	/**
	 * @return the dsEndereco
	 */
	public String getDsEndereco() {
		return dsEndereco;
	}

	/**
	 * @param dsEndereco
	 *            the dsEndereco to set
	 */
	public void setDsEndereco(String dsEndereco) {
		this.dsEndereco = dsEndereco;
	}

}