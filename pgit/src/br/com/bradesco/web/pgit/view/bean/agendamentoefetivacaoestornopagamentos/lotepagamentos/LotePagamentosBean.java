package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.lotepagamentos;

import static br.com.bradesco.web.pgit.utils.NumberUtils.createLong;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formataDiaMesAno;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.OcorrenciasLotePagamentosDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaopostergarlotepagamento.IManterSolicAntecipacaoPostergarLotePagamentoService;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.IManterSolicAntecipacaoProcLotePagamentoService;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.bean.ConsultarLotesIncSolicEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.bean.ConsultarLotesIncSolicSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersoliccancelamentolotepgto.IManterSolicCancelamentoLotePgtoService;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.IManterSolicitacaoExclusaoLotePagamentosService;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.bean.ConsultarOcorrenciasSolicLoteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.bean.OcorrenciasSolicLoteDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicliberacaolotepgto.IManterSolicLiberacaoLotePgtoService;
import br.com.bradesco.web.pgit.utils.selectitem.SelectItemUtils;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.exclusaolotepagamentos.ConfimarInclusaoLotePagamentosBean;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;

public abstract class LotePagamentosBean {

	private static final Integer DOIS = 2;
	private static final Integer SITUACAO_SOLICITACAO_UM = 1;
	private static final Integer ZERO = 0;
	private static final String MENSAGEM_LOTE = "ESTE LOTE CONT�M UM OU MAIS PAGAMENTOS QUE N�O ATENDEM AOS REQUISITOS DE MANUTEN��O POR LOTE. OPERA��O N�O PERMITIDA.";

	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;
	private LotePagamentosBean manterSolicAntecipacaoProcLotePagamentoBean;
	private PagamentosBean pagamentosBean;
	private IComboService comboService;
	private IManterSolicitacaoExclusaoLotePagamentosService manterSolicitacaoExclusaoLotePagamentosService;
	private IManterSolicAntecipacaoPostergarLotePagamentoService manterSolicAntecipacaoPostergarLotePagamentoService;
	private IManterSolicLiberacaoLotePgtoService manterSolicLiberacaoLotePgtoService;
	private IManterSolicCancelamentoLotePgtoService manterSolicCancelamentoLotePgtoService;
	private IManterSolicAntecipacaoProcLotePagamentoService manterSolicAntecipacaoProcLotePagamentoService;

	private List<OcorrenciasLotePagamentosDTO> listaGridLotePagto;
	private Integer itemSelecionadoLista;
	private List<SelectItem> listaControleRadios;

	private DetalharLotePagamentosSaidaDTO saidaDetalhe;
	private List<OcorrenciasSolicLoteDTO> listaOcorrenciasDetalhe;

	private ConfimarInclusaoLotePagamentosBean confirmaInclusaoBean;

	private ConsultarLotesIncSolicSaidaDTO loteSelecionado = new ConsultarLotesIncSolicSaidaDTO();
	private Integer itemSelecao;
	protected List<ConsultarLotesIncSolicSaidaDTO> listaLote = new ArrayList<ConsultarLotesIncSolicSaidaDTO>();
	private boolean gerenciaTxtLote = false;
	protected Integer tipoAgendamento;

	public void iniciarTela(ActionEvent evt) {

		pagamentosBean.limparCampos();

		// Carregamento dos combos
		pagamentosBean.listarConsultarSituacaoSolicitacao();
		pagamentosBean.preencheTipoLayoutArquivo();

		// Tela de Filtro
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
				getTelaConsulta());
		getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(
				2269651L);
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(
				null);

		pagamentosBean.setHabilitaArgumentosPesquisa(false);
		pagamentosBean.setPanelBotoes(false);
	}

	protected OcorrenciasLotePagamentosDTO getItemSelecionado() {
		final OcorrenciasLotePagamentosDTO itemSelecionado = getListaGridLotePagto()
				.get(getItemSelecionadoLista());

		return itemSelecionado;
	}

	public ConsultarLotePagamentosEntradaDTO preparaBuscaListaLotePagamento() {
		ConsultarLotePagamentosEntradaDTO entrada = new ConsultarLotePagamentosEntradaDTO();

		entrada.setCdpessoaJuridicaContrato(0L);
		entrada.setCdTipoContratoNegocio(0);
		entrada.setNrSequenciaContratoNegocio(0L);

		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("0")
				|| getFiltroAgendamentoEfetivacaoEstornoBean()
						.getTipoFiltroSelecionado().equals("1")) {

			entrada
					.setCdpessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdPessoaJuridicaContrato());
			entrada
					.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdTipoContratoNegocio());
			entrada
					.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrSequenciaContratoNegocio());
		}

		entrada.setNrLoteInterno(pagamentosBean.getLoteInterno());
		entrada.setCdTipoLayoutArquivo(pagamentosBean
				.getFiltroTipoLayoutArquivo());
		entrada.setDtPgtoInicial(formataDiaMesAno(pagamentosBean
				.getDataInicialPagamentoFiltro()));
		entrada.setDtPgtoFinal(formataDiaMesAno(pagamentosBean
				.getDataFinalPagamentoFiltro()));
		entrada.setCdSituacaoSolicitacaoPagamento(pagamentosBean
				.getCboSituacaoSolicitacao());
		entrada.setCdMotivoSolicitacao(pagamentosBean.getCboMotivoSituacao());

		return entrada;
	}

	protected DetalharLotePagamentosEntradaDTO prepararBuscaInformacoesDetalhe() {
		DetalharLotePagamentosEntradaDTO entrada = new DetalharLotePagamentosEntradaDTO();
		OcorrenciasLotePagamentosDTO itemSelecionado = getListaGridLotePagto()
				.get(getItemSelecionadoLista());

		entrada.setCdSolicitacaoPagamentoIntegrado(itemSelecionado
				.getCdSolicitacaoPagamentoIntegrado());
		entrada.setNrSolicitacaoPagamentoIntegrado(itemSelecionado
				.getNrSolicitacaoPagamentoIntegrado());

		return entrada;

	}

	public boolean getDesabilitaBtnExcluir() {
		if (getListaGridLotePagto() != null) {
			if (getItemSelecionadoLista() != null) {
				if (getListaGridLotePagto().get(getItemSelecionadoLista())
						.getCdSituacaoSolicitacaoPagamento() != null
						&& getListaGridLotePagto().get(
								getItemSelecionadoLista())
								.getCdSituacaoSolicitacaoPagamento() == 1) {
					return false;
				}
			}
		}
		return true;
	}

	protected void consolidaInformacoesDetalhe(
			DetalharLotePagamentosSaidaDTO saida) {
		if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")) {
			saida.setNrCnpjCpf(String.valueOf(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrCpfCnpjCliente()));
			saida.setDsRazaoSocial(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getDescricaoRazaoSocialCliente());
			saida.setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getEmpresaGestoraDescCliente());
			saida.setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getNumeroDescCliente());
			saida.setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getDescricaoContratoDescCliente());
			saida
					.setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getSituacaoDescCliente());

		} else if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("1")) {
			saida.setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getEmpresaGestoraDescContrato());
			saida.setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getNumeroDescContrato());
			saida.setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getDescricaoContratoDescContrato());
			saida
					.setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getSituacaoDescContrato());
		}

		setSaidaDetalhe(saida);
	}

	public ConsultarOcorrenciasSolicLoteEntradaDTO prepararBuscaOcorrenciasDetalhe() {
		ConsultarOcorrenciasSolicLoteEntradaDTO entrada = new ConsultarOcorrenciasSolicLoteEntradaDTO();

		entrada.setCdSolicitacaoPagamentoIntegrado(saidaDetalhe
				.getCdSolicitacaoPagamentoIntegrado());
		entrada.setNrSolicitacaoPagamentoIntegrado(saidaDetalhe
				.getNrSolicitacaoPagamentoIntegrado());

		return entrada;
	}

	public IncluirLotePagamentosEntradaDTO prepararInclusaoLotePagamento() {
		IncluirLotePagamentosEntradaDTO entrada = new IncluirLotePagamentosEntradaDTO();

		entrada
				.setCdpessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getCdPessoaJuridicaContrato());
		entrada
				.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getCdTipoContratoNegocio());
		entrada
				.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getNrSequenciaContratoNegocio());
		entrada.setNrLoteInterno(pagamentosBean.getLoteInterno());
		entrada.setCdTipoLayoutArquivo(pagamentosBean
				.getFiltroTipoLayoutArquivo());
		entrada.setDtPgtoInicial(pagamentosBean
				.getDataInicialPagamentoFormatada());
		entrada.setDtPgtoFinal(pagamentosBean.getDataFinalPagamentoFormatada());
		entrada.setCdProdutoServicoOperacao(pagamentosBean
				.getTipoServicoFiltro());
		entrada.setCdProdutoServicoRelacionado(pagamentosBean
				.getModalidadeFiltro());
		entrada.setCdBancoDebito(pagamentosBean.getCdBancoContaDebito());
		entrada.setCdAgenciaDebito(pagamentosBean
				.getCdAgenciaBancariaContaDebito());
		entrada
				.setCdContaDebito(pagamentosBean
						.getCdContaBancariaContaDebito());
		entrada.setQtdeTotalPagtoPrevistoSoltc(getLoteSelecionado()
				.getQtTotalPagamentoLote());
		entrada.setVlrTotPagtoPrevistoSolicitacao(getLoteSelecionado()
				.getVlTotalPagamentoLote());

		return entrada;
	}

	public ExcluirLotePagamentosEntradaDTO prepararExclusaoLotePagamento() {
		ExcluirLotePagamentosEntradaDTO entrada = new ExcluirLotePagamentosEntradaDTO();

		OcorrenciasLotePagamentosDTO itemSelecionado = getItemSelecionado();

		entrada.setCdSolicitacaoPagamentoIntegrado(itemSelecionado
				.getCdSolicitacaoPagamentoIntegrado());
		entrada.setNrSolicitacaoPagamentoIntegrado(itemSelecionado
				.getNrSolicitacaoPagamentoIntegrado());

		return entrada;
	}

	public String pesquisarConsultar(final ActionEvent evt) {
		carregaListaLotePagamento();

		return "";
	}

	public String pesquisarOcorrenciasDetalhe(final ActionEvent evt) {
		ocorrenciasDetalhe();

		return "";
	}

	public String ocorrenciasDetalhe() {
		ConsultarOcorrenciasSolicLoteEntradaDTO entrada = prepararBuscaOcorrenciasDetalhe();

		try {

			setListaOcorrenciasDetalhe(getManterSolicitacaoExclusaoLotePagamentosService()
					.consultarOcorrenciasSolicLote(entrada).getOcorrencias());

			return getTelaDetalheOcorrencias();

		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);

			getPagamentosBean().setPanelBotoes(false);

			return "";
		}
	}

	public boolean isHabilitaBotaoOcorrencias() {
		return saidaDetalhe.getQtdeTotalPagtoPrevistoSoltc().compareTo(
				saidaDetalhe.getQtTotalPgtoEfetivadoSolicitacao()) == 0;
	}

	public boolean isDisabledBotaoExcluir() {
		return getItemSelecionadoLista() == null
				|| getListaGridLotePagto().get(getItemSelecionadoLista())
						.getCdSituacaoSolicitacaoPagamento().compareTo(
								SITUACAO_SOLICITACAO_UM) != ZERO;
	}

	public String incluir() {
		getFiltroAgendamentoEfetivacaoEstornoBean().limparFiltroPrincipal();
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setTipoFiltroSelecionado("");
		limparInformacaoConsulta();
		limparCamposTelaInclusao();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
				getTelaInclusao());
		setListaLote(new ArrayList<ConsultarLotesIncSolicSaidaDTO>());
		setTipoAgendamento(null);

		return getTelaInclusao();
	}

	public String avancarIncluir() {
		loteSelecionado = getListaLote().get(getItemSelecao());

		if (loteSelecionado.getCdIndicadorLoteApto().compareTo(DOIS) == ZERO) {

			BradescoFacesUtils.addInfoModalMessage(MENSAGEM_LOTE,
					getTelaInclusao(),BradescoViewExceptionActionType.ACTION, false);

			return null;

		}

		consolidarCamposInclusaoLotePagamentos();
			
		return getTelaInclusaoConfirmacao();
	}

	protected void consolidarCamposInclusaoLotePagamentos() {
		confirmaInclusaoBean = new ConfimarInclusaoLotePagamentosBean();

		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("0")) {
			confirmaInclusaoBean.setNrCnpjCpf(String
					.valueOf(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrCpfCnpjCliente()));
			confirmaInclusaoBean
					.setDsRazaoSocial(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getDescricaoRazaoSocialCliente());
			confirmaInclusaoBean
					.setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getEmpresaGestoraDescCliente());
			confirmaInclusaoBean
					.setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNumeroDescCliente());
			confirmaInclusaoBean
					.setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getDescricaoContratoDescCliente());
			confirmaInclusaoBean
					.setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getSituacaoDescCliente());

		} else if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("1")) {
			confirmaInclusaoBean
					.setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getEmpresaGestoraDescContrato());
			confirmaInclusaoBean
					.setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNumeroDescContrato());
			confirmaInclusaoBean
					.setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getDescricaoContratoDescContrato());
			confirmaInclusaoBean
					.setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getSituacaoDescContrato());
		}
	}

	public String excluir() {
		try {

			buscaInformacoesDetalhe();

			return getTelaExclusao();

		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);

			return "";
		}
	}

	public String detalhar() {
		try {

			buscaInformacoesDetalhe();

			return getTelaDetalhe();

		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);

			return "";
		}
	}

	public abstract String carregaListaLotePagamento();

	public abstract void buscaInformacoesDetalhe();

	public abstract String confirmarExcluir();

	public abstract String confirmarIncluir();

	public abstract String getTelaConsulta();

	public abstract String getTelaDetalhe();

	public abstract String getTelaDetalheOcorrencias();

	public abstract String getTelaExclusao();

	public abstract String getTelaInclusao();

	public abstract String getTelaInclusaoConfirmacao();

	public abstract String getBean();

	public abstract Integer getCodigoSolicitacao();

	public String voltar() {
		setItemSelecionadoLista(null);

		return getTelaConsulta();
	}

	public String voltarDetalhar() {

		return getTelaDetalhe();
	}

	public String voltarExclusao() {

		return voltar();
	}
	
	public String voltarInclusaoConsulta() {
		limparCamposTelaInclusao();
		limparInformacaoConsulta();
		pagamentosBean.limpaTelaPrincipal();
		setGerenciaTxtLote(false);

		return voltar();
	}

	public String voltarInclusao() {
		setItemSelecao(null);
		return getTelaInclusao();
	}

	public PagamentosBean getPagamentosBean() {
		return pagamentosBean;
	}

	public void limparInformacaoConsulta() {
		setListaGridLotePagto(null);
		setItemSelecionadoLista(null);
		pagamentosBean.setDisableArgumentosConsulta(false);
	}

	public void limpaListaLote(ActionEvent evt) {
		setListaGridLotePagto(null);
		setItemSelecionadoLista(null);
	}

	public void limparCamposTelaInclusao() {
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
				getTelaConsulta());
		getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(
				2269651L);
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(
				null);

		pagamentosBean.limpaTelaPrincipal();
		pagamentosBean.limparFiltroPrincipal();
		pagamentosBean.limparCamposLoteInterno();

		pagamentosBean.setDataInicialPagamentoFiltro(null);
		pagamentosBean.setDataFinalPagamentoFiltro(null);

		pagamentosBean.setChkDataPagamento(false);
		pagamentosBean.setChkTipoServico(false);
		pagamentosBean.setChkContaDebito(false);

		setItemSelecao(null);
		pagamentosBean.setDisableArgumentosConsulta(false);
		setGerenciaTxtLote(false);
		setListaLote(new ArrayList<ConsultarLotesIncSolicSaidaDTO>());
		setTipoAgendamento(null);
	}

	protected void carregaListaControleRadios() {
		setListaControleRadios(new ArrayList<SelectItem>());
		for (int i = 0; i < getListaGridLotePagto().size(); i++) {
			getListaControleRadios().add(new SelectItem(i, ""));
		}
	}

	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	public IComboService getComboService() {
		return comboService;
	}

	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	public IManterSolicitacaoExclusaoLotePagamentosService getManterSolicitacaoExclusaoLotePagamentosService() {
		return manterSolicitacaoExclusaoLotePagamentosService;
	}

	public void setManterSolicitacaoExclusaoLotePagamentosService(
			IManterSolicitacaoExclusaoLotePagamentosService manterSolicitacaoExclusaoLotePagamentosService) {
		this.manterSolicitacaoExclusaoLotePagamentosService = manterSolicitacaoExclusaoLotePagamentosService;
	}

	public IManterSolicAntecipacaoPostergarLotePagamentoService getManterSolicAntecipacaoPostergarLotePagamentoService() {
		return manterSolicAntecipacaoPostergarLotePagamentoService;
	}

	public void setManterSolicAntecipacaoPostergarLotePagamentoService(
			IManterSolicAntecipacaoPostergarLotePagamentoService manterSolicAntecipacaoPostergarLotePagamentoService) {
		this.manterSolicAntecipacaoPostergarLotePagamentoService = manterSolicAntecipacaoPostergarLotePagamentoService;
	}

	public List<OcorrenciasLotePagamentosDTO> getListaGridLotePagto() {
		return listaGridLotePagto;
	}

	public void setListaGridLotePagto(
			List<OcorrenciasLotePagamentosDTO> listaGridLotePagto) {
		this.listaGridLotePagto = listaGridLotePagto;
	}

	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	public List<SelectItem> getListaControleRadios() {
		return listaControleRadios;
	}

	public void setListaControleRadios(List<SelectItem> listaControleRadios) {
		this.listaControleRadios = listaControleRadios;
	}

	public void setPagamentosBean(PagamentosBean pagamentosBean) {
		this.pagamentosBean = pagamentosBean;
	}

	public DetalharLotePagamentosSaidaDTO getSaidaDetalhe() {
		return saidaDetalhe;
	}

	public void setSaidaDetalhe(DetalharLotePagamentosSaidaDTO saidaDetalhe) {
		this.saidaDetalhe = saidaDetalhe;
	}

	public List<OcorrenciasSolicLoteDTO> getListaOcorrenciasDetalhe() {
		return listaOcorrenciasDetalhe;
	}

	public void setListaOcorrenciasDetalhe(
			List<OcorrenciasSolicLoteDTO> listaOcorrenciasDetalhe) {
		this.listaOcorrenciasDetalhe = listaOcorrenciasDetalhe;
	}

	public ConfimarInclusaoLotePagamentosBean getConfirmaInclusaoBean() {
		return confirmaInclusaoBean;
	}

	public void setConfirmaInclusaoBean(
			ConfimarInclusaoLotePagamentosBean confirmaInclusaoBean) {
		this.confirmaInclusaoBean = confirmaInclusaoBean;
	}

	public IManterSolicLiberacaoLotePgtoService getManterSolicLiberacaoLotePgtoService() {
		return manterSolicLiberacaoLotePgtoService;
	}

	public void setManterSolicLiberacaoLotePgtoService(
			IManterSolicLiberacaoLotePgtoService manterSolicLiberacaoLotePgtoService) {
		this.manterSolicLiberacaoLotePgtoService = manterSolicLiberacaoLotePgtoService;
	}

	public IManterSolicCancelamentoLotePgtoService getManterSolicCancelamentoLotePgtoService() {
		return manterSolicCancelamentoLotePgtoService;
	}

	public void setManterSolicCancelamentoLotePgtoService(
			IManterSolicCancelamentoLotePgtoService manterSolicCancelamentoLotePgtoService) {
		this.manterSolicCancelamentoLotePgtoService = manterSolicCancelamentoLotePgtoService;
	}

	public IManterSolicAntecipacaoProcLotePagamentoService getManterSolicAntecipacaoProcLotePagamentoService() {
		return manterSolicAntecipacaoProcLotePagamentoService;
	}

	public void setManterSolicAntecipacaoProcLotePagamentoService(
			IManterSolicAntecipacaoProcLotePagamentoService manterSolicAntecipacaoProcLotePagamentoService) {
		this.manterSolicAntecipacaoProcLotePagamentoService = manterSolicAntecipacaoProcLotePagamentoService;
	}

	public LotePagamentosBean getManterSolicAntecipacaoProcLotePagamentoBean() {
		return manterSolicAntecipacaoProcLotePagamentoBean;
	}

	public void setManterSolicAntecipacaoProcLotePagamentoBean(
			LotePagamentosBean manterSolicAntecipacaoProcLotePagamentoBean) {
		this.manterSolicAntecipacaoProcLotePagamentoBean = manterSolicAntecipacaoProcLotePagamentoBean;
	}

	public List<ConsultarLotesIncSolicSaidaDTO> getListaLote() {
		return listaLote;
	}

	public void setListaLote(List<ConsultarLotesIncSolicSaidaDTO> listaLote) {
		this.listaLote = listaLote;
	}

	public Integer getItemSelecao() {
		return itemSelecao;
	}

	public void setItemSelecao(Integer itemSelecao) {
		this.itemSelecao = itemSelecao;
	}

	public ConsultarLotesIncSolicSaidaDTO getLoteSelecionado() {
		return loteSelecionado;
	}

	public void setLoteSelecionado(
			ConsultarLotesIncSolicSaidaDTO loteSelecionado) {
		this.loteSelecionado = loteSelecionado;
	}

	public List<SelectItem> getListaItensSelecao() {
		return SelectItemUtils.criarSelectItems(getListaLote());
	}

	public boolean isDesabilitaConsultarLote() {
		if (getPagamentosBean().getLoteInterno() != null
				&& getPagamentosBean().getLoteInterno() != 0l) {
			return false;
		} else {
			return true;
		}
	}

	public String consultaLotesInclusaoSolicitacao() {

		ConsultarLotesIncSolicEntradaDTO entrada = new ConsultarLotesIncSolicEntradaDTO();

		try {

			entrada
					.setCdpessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getEmpresaGestoraFiltro());
			entrada.setCdSolicitacaoPagamentoIntegrado(getCodigoSolicitacao());
			entrada
					.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdTipoContratoNegocio());
			entrada.setNrLoteInterno(getPagamentosBean().getLoteInterno());
			
			if (getFiltroAgendamentoEfetivacaoEstornoBean()!= null) {
				if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")) {
					entrada
							.setNrSequenciaContratoNegocio(createLong(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getNumeroDescCliente()));
				} else {
					entrada
							.setNrSequenciaContratoNegocio(createLong(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getNumeroDescContrato()));
				}
			}
			
			setListaLote(getManterSolicAntecipacaoProcLotePagamentoService()
					.consultarLotes(entrada));
			
			setGerenciaTxtLote(true);
			getPagamentosBean().setDisableArgumentosConsulta(true);

		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);

			return "";
		}

		return null;
	}

	public void paginarListaLoteIncluir(final ActionEvent evt) {
		consultaLotesInclusaoSolicitacao();
	}
	
	public void consultaLotesInclusaoSolicitacao(ActionEvent evt) {
		consultaLotesInclusaoSolicitacao();
	}

	public boolean isGerenciaTxtLote() {
		return gerenciaTxtLote;
	}

	public void setGerenciaTxtLote(boolean gerenciaTxtLote) {
		this.gerenciaTxtLote = gerenciaTxtLote;
	}

	public void limpaLista() {
		setListaLote(new ArrayList<ConsultarLotesIncSolicSaidaDTO>());
		getPagamentosBean().setDisableArgumentosConsulta(false);
		setItemSelecao(null);
		setGerenciaTxtLote(false);
	}
	
	public Integer getTipoAgendamento() {
		return tipoAgendamento;
	}

	public void setTipoAgendamento(Integer tipoAgendamento) {
		this.tipoAgendamento = tipoAgendamento;
	}
}
