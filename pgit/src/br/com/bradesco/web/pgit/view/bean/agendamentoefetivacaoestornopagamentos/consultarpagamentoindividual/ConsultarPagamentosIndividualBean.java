/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.consultarpagamentoindividual
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.consultarpagamentoindividual;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaLongNulo;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.exception.PgitException;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.IConsultaAutorizanteService;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesNetEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesNetEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarHistConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarHistConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarManutencaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarManutencaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarPagamentosIndividuaisEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarPagamentosIndividuaisSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharHistConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharHistConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService;
import br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;
import br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterclientesorgaopublicofederal.ManterClientesOrgaoPublicoFederalBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: ConsultarPagamentosIndividualBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarPagamentosIndividualBean extends PagamentosBean {
    
    /**
     * Atributo ConsultarPagamentosIndividuaisEntradaDTO
     */
    ConsultarPagamentosIndividuaisEntradaDTO entradaConsultaPgtos = new ConsultarPagamentosIndividuaisEntradaDTO();
    
    /** Atributo CODIGO_IDENTIFICACAO_DIVERSOS. */
    private static final String CODIGO_IDENTIFICACAO_DIVERSOS = "4";
    private static final Integer NUMERO_ZERO = 0;
    private static final String VAZIO = "";
    private static final Long NUMERO_ZERO_LONG = 0l;

    /** Atributo consultaAutorizanteService. */
    private IConsultaAutorizanteService consultaAutorizanteService;

    /** Atributo consultasServiceImpl. */
    private IConsultasService consultasServiceImpl;

    /** Atributo manterClientesOrgaoPublicoFederalBean. */
    private ManterClientesOrgaoPublicoFederalBean manterClientesOrgaoPublicoFederalBean = new ManterClientesOrgaoPublicoFederalBean();	

    // hisConsultaSaldo
    /** Atributo listaHistoricoConsulta. */
    private List<ConsultarHistConsultaSaldoSaidaDTO> listaHistoricoConsulta;

    /** Atributo itemSelecionadoListaHistorico. */
    private Integer itemSelecionadoListaHistorico;

    /** Atributo listaPesquisaHistoricoControle. */
    private List<SelectItem> listaPesquisaHistoricoControle;

    /** Atributo digAgenciaDebito. */
    private Integer digAgenciaDebito;

    // detCOnsultaSaldo
    /** Atributo dataHoraConsultaSaldo. */
    private String dataHoraConsultaSaldo;

    /** Atributo valorPagamento. */
    private BigDecimal valorPagamento;

    /** Atributo valorPagamentoFormatado. */
    private String valorPagamentoFormatado;

    /** Atributo sinalMomentoLancamento. */
    private String sinalMomentoLancamento;

    /** Atributo saldoMomentoLancamento. */
    private BigDecimal saldoMomentoLancamento;

    /** Atributo saldoOperacional. */
    private BigDecimal saldoOperacional;

    /** Atributo saldoVincSemReserva. */
    private BigDecimal saldoVincSemReserva;

    /** Atributo saldoVincComReserva. */
    private BigDecimal saldoVincComReserva;

    /** The ds identificador transferencia pagto. */
    private String dsIdentificadorTransferenciaPagto;

    /** Atributo saldoVincJudicial. */
    private BigDecimal saldoVincJudicial;

    /** Atributo saldoVincAdm. */
    private BigDecimal saldoVincAdm;

    /** Atributo saldoVincSeguranca. */
    private BigDecimal saldoVincSeguranca;

    /** Atributo saldoVincAdmLp. */
    private BigDecimal saldoVincAdmLp;

    /** Atributo saldoAgregadoFundo. */
    private BigDecimal saldoAgregadoFundo;

    /** Atributo saldoAgregadoCDB. */
    private BigDecimal saldoAgregadoCDB;

    /** Atributo saldoAgregadoPoupanca. */
    private BigDecimal saldoAgregadoPoupanca;

    /** Atributo saldoAgregadoCredito. */
    private BigDecimal saldoAgregadoCredito;

    /** Atributo saldoBonusCPMF. */
    private BigDecimal saldoBonusCPMF;

    // trilhaAuditoria
    /** Atributo dataHoraManutencao. */
    private String dataHoraManutencao;

    /** Atributo usuarioManutencao. */
    private String usuarioManutencao;

    /** Atributo tipoCanalManutencao. */
    private String tipoCanalManutencao;

    /** Atributo complementoManutencao. */
    private String complementoManutencao;

    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao;

    /** Atributo usuarioInclusao. */
    private String usuarioInclusao;

    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;

    /** Atributo complementoInclusao. */
    private String complementoInclusao;

    // conManutencao
    /** Atributo listaConsultarManutencao. */
    private List<ConsultarManutencaoPagamentoSaidaDTO> listaConsultarManutencao;

    /** Atributo itemSelecionadoListaManutencao. */
    private Integer itemSelecionadoListaManutencao;

    /** Atributo listaManutencaoControle. */
    private List<SelectItem> listaManutencaoControle;

    /** Atributo situacaoPagamento. */
    private String situacaoPagamento;

    /** Atributo motivoPagamento. */
    private String motivoPagamento;

    /** Atributo inscricaoFavorecido. */
    private String inscricaoFavorecido;

    /** Atributo cdTipoInscricaoFavorecido. */
    private String cdTipoInscricaoFavorecido;

    /** Atributo tipoContaFavorecido. */
    private String tipoContaFavorecido;

    /** Atributo bancoFavorecido. */
    private String bancoFavorecido;

    /** Atributo agenciaFavorecido. */
    private String agenciaFavorecido;

    /** Atributo contaFavorecido. */
    private String contaFavorecido;

    // telasDetalhe
    /** Atributo tipoCanal. */
    private String tipoCanal;

    /** Atributo canal. */
    private String canal;

    /** Atributo orgaoPagador. */
    private String orgaoPagador;

    /** Atributo manutencao. */
    private boolean manutencao;

    /** Atributo dsContrato. */
    private String dsContrato;

    /** Atributo cdSituacaoContrato. */
    private String cdSituacaoContrato;

    /** Atributo dsBancoDebito. */
    private String dsBancoDebito;

    /** Atributo dsAgenciaDebito. */
    private String dsAgenciaDebito;

    /** Atributo nroContrato. */
    private String nroContrato;

    /** Atributo tipoContaDebito. */
    private String tipoContaDebito;

    /** Atributo cdFavorecido. */
    private Long cdFavorecido;

    /** Atributo dsBancoFavorecido. */
    private String dsBancoFavorecido;

    /** Atributo dsContaFavorecido. */
    private String dsContaFavorecido;

    /** Atributo dsAgenciaFavorecido. */
    private String dsAgenciaFavorecido;

    /** Atributo cdTipoContaFavorecido. */
    private Integer cdTipoContaFavorecido;

    /** Atributo dsTipoContaFavorecido. */
    private String dsTipoContaFavorecido;

    /** Atributo dtNascimentoBeneficiario. */
    private String dtNascimentoBeneficiario;

    /** Atributo dsTipoServico. */
    private String dsTipoServico;

    /** Atributo dsEstornoPagamento. */
    private String dsEstornoPagamento;

    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;

    /** Atributo dsIndicadorModalidade. */
    private String dsIndicadorModalidade;

    /** Atributo nrSequenciaArquivoRemessa. */
    private String nrSequenciaArquivoRemessa;

    /** Atributo nrLoteArquivoRemessa. */
    private String nrLoteArquivoRemessa;

    /** Atributo nrLote. */
    private String nrLote;

    /** Atributo numeroPagamento. */
    private String numeroPagamento;

    /** Atributo cdListaDebito. */
    private String cdListaDebito;

    /** Atributo dtAgendamento. */
    private String dtAgendamento;

    /** Atributo dtPagamento. */
    private String dtPagamento;

    /** Atributo dtPagamentoGare. */
    private Date dtPagamentoGare;

    /** Atributo dtPagamentoManutencao. */
    private Date dtPagamentoManutencao;

    /** Atributo dtVencimento. */
    private String dtVencimento;

    /** Atributo cdIndicadorEconomicoMoeda. */
    private String cdIndicadorEconomicoMoeda;

    /** Atributo qtMoeda. */
    private BigDecimal qtMoeda;

    /** Atributo vlrAgendamento. */
    private BigDecimal vlrAgendamento;

    /** Atributo vlrEfetivacao. */
    private BigDecimal vlrEfetivacao;

    /** Atributo cdSituacaoOperacaoPagamento. */
    private String cdSituacaoOperacaoPagamento;

    /** Atributo cdMotivoSituacaoPagamento. */
    private Integer cdMotivoSituacaoPagamento;

    /** Atributo dsMotivoSituacao. */
    private String dsMotivoSituacao;

    /** Atributo dsPagamento. */
    private String dsPagamento;

    /** Atributo nrDocumento. */
    private String nrDocumento;

    /** Atributo tipoDocumento. */
    private String tipoDocumento;

    /** Atributo cdSerieDocumento. */
    private String cdSerieDocumento;

    /** Atributo vlDocumento. */
    private BigDecimal vlDocumento;

    /** Atributo dtEmissaoDocumento. */
    private String dtEmissaoDocumento;

    /** Atributo dsUsoEmpresa. */
    private String dsUsoEmpresa;

    /** Atributo dsMensagemPrimeiraLinha. */
    private String dsMensagemPrimeiraLinha;

    /** Atributo dsMensagemSegundaLinha. */
    private String dsMensagemSegundaLinha;

    /** Atributo cdBancoCredito. */
    private String cdBancoCredito;

    /** Atributo dsBancoCredito. */
    private String dsBancoCredito;

    /** Atributo cdIspbPagamento. */
    private String cdIspbPagamento;

    /** Atributo agenciaDigitoCredito. */
    private String agenciaDigitoCredito;

    /** Atributo contaDigitoCredito. */
    private String contaDigitoCredito;

    /** Atributo tipoContaCredito. */
    private String tipoContaCredito;

    /** AtribuTipoContaDestinoPagamentoto cdBancoDestino. */
    private String cdBancoDestino;
    
    private String cdBancoDestinoPagamento;
    
    private String dsISPB;
    private String cdContaPagamentoDet;

    /** Atributo agenciaDigitoDestino. */
    private String agenciaDigitoDestino;

    /** Atributo contaDigitoDestino. */
    private String contaDigitoDestino;
    
    private String contaDigitoDestinoPagamento;

    /** Atributo tipoContaDestino. */
    private String tipoContaDestino;
    
    /** Atributo tipoContaDestino. */
    private String tipoContaDestinoPagamento;

    /** Atributo vlDesconto. */
    private BigDecimal vlDesconto;

    /** Atributo vlAbatimento. */
    private BigDecimal vlAbatimento;

    /** Atributo vlMulta. */
    private BigDecimal vlMulta;

    /** Atributo vlMora. */
    private BigDecimal vlMora;

    /** Atributo vlDeducao. */
    private BigDecimal vlDeducao;

    /** Atributo vlAcrescimo. */
    private BigDecimal vlAcrescimo;

    /** Atributo vlImpostoRenda. */
    private BigDecimal vlImpostoRenda;

    /** Atributo vlIss. */
    private BigDecimal vlIss;

    /** Atributo vlIof. */
    private BigDecimal vlIof;

    /** Atributo vlInss. */
    private BigDecimal vlInss;

    /** Atributo camaraCentralizadora. */
    private String camaraCentralizadora;

    /** Atributo finalidadeTed. */
    private String finalidadeTed;

    /** Atributo finalidadeDoc. */
    private String finalidadeDoc;

    /** Atributo identificacaoTransferenciaTed. */
    private String identificacaoTransferenciaTed;

    /** Atributo identificacaoTransferenciaDoc. */
    private String identificacaoTransferenciaDoc;

    /** Atributo identificacaoTitularidade. */
    private String identificacaoTitularidade;

    /** Atributo numeroOp. */
    private String numeroOp;

    /** Atributo dataExpiracaoCredito. */
    private String dataExpiracaoCredito;

    /** Atributo instrucaoPagamento. */
    private String instrucaoPagamento;

    /** Atributo numeroCheque. */
    private String numeroCheque;

    /** Atributo serieCheque. */
    private String serieCheque;

    /** Atributo indicadorAssociadoUnicaAgencia. */
    private String indicadorAssociadoUnicaAgencia;

    /** Atributo identificadorTipoRetirada. */
    private String identificadorTipoRetirada;

    /** Atributo identificadorRetirada. */
    private String identificadorRetirada;

    /** Atributo dataRetirada. */
    private String dataRetirada;

    /** Atributo vlImpostoMovFinanceira. */
    private BigDecimal vlImpostoMovFinanceira;

    /** Atributo codigoBarras. */
    private String codigoBarras;

    /** Atributo dddContribuinte. */
    private String dddContribuinte;

    /** Atributo telefoneContribuinte. */
    private String telefoneContribuinte;

    /** Atributo inscricaoEstadualContribuinte. */
    private String inscricaoEstadualContribuinte;

    /** Atributo agenteArrecadador. */
    private String agenteArrecadador;

    /** Atributo codigoReceita. */
    private String codigoReceita;

    /** Atributo numeroReferencia. */
    private String numeroReferencia;

    /** Atributo numeroCotaParcela. */
    private String numeroCotaParcela;

    /** Atributo percentualReceita. */
    private BigDecimal percentualReceita;

    /** Atributo periodoApuracao. */
    private String periodoApuracao;

    /** Atributo anoExercicio. */
    private String anoExercicio;

    /** Atributo dataReferencia. */
    private String dataReferencia;

    /** Atributo vlReceita. */
    private BigDecimal vlReceita;

    /** Atributo vlPrincipal. */
    private BigDecimal vlPrincipal;

    /** Atributo vlAtualizacaoMonetaria. */
    private BigDecimal vlAtualizacaoMonetaria;

    /** Atributo vlTotal. */
    private BigDecimal vlTotal;

    /** Atributo codigoCnae. */
    private String codigoCnae;

    /** Atributo placaVeiculo. */
    private String placaVeiculo;

    /** Atributo numeroParcelamento. */
    private String numeroParcelamento;

    /** Atributo codigoOrgaoFavorecido. */
    private String codigoOrgaoFavorecido;

    /** Atributo nomeOrgaoFavorecido. */
    private String nomeOrgaoFavorecido;

    /** Atributo codigoUfFavorecida. */
    private String codigoUfFavorecida;

    /** Atributo descricaoUfFavorecida. */
    private String descricaoUfFavorecida;

    /** Atributo vlAcrescimoFinanceiro. */
    private BigDecimal vlAcrescimoFinanceiro;

    /** Atributo vlHonorarioAdvogado. */
    private BigDecimal vlHonorarioAdvogado;

    /** Atributo observacaoTributo. */
    private String observacaoTributo;

    /** Atributo codigoInss. */
    private String codigoInss;

    /** Atributo dataCompetencia. */
    private String dataCompetencia;

    /** Atributo vlOutrasEntidades. */
    private BigDecimal vlOutrasEntidades;

    /** Atributo codigoTributo. */
    private String codigoTributo;

    /** Atributo codigoRenavam. */
    private String codigoRenavam;

    /** Atributo municipioTributo. */
    private String municipioTributo;

    /** Atributo ufTributo. */
    private String ufTributo;

    /** Atributo numeroNsu. */
    private String numeroNsu;

    /** Atributo opcaoTributo. */
    private String opcaoTributo;

    /** Atributo opcaoRetiradaTributo. */
    private String opcaoRetiradaTributo;

    /** Atributo clienteDepositoIdentificado. */
    private String clienteDepositoIdentificado;

    /** Atributo tipoDespositoIdentificado. */
    private String tipoDespositoIdentificado;

    /** Atributo produtoDepositoIdentificado. */
    private String produtoDepositoIdentificado;

    /** Atributo sequenciaProdutoDepositoIdentificado. */
    private String sequenciaProdutoDepositoIdentificado;

    /** Atributo bancoCartaoDepositoIdentificado. */
    private String bancoCartaoDepositoIdentificado;

    /** Atributo cartaoDepositoIdentificado. */
    private String cartaoDepositoIdentificado;

    /** Atributo bimCartaoDepositoIdentificado. */
    private String bimCartaoDepositoIdentificado;

    /** Atributo viaCartaoDepositoIdentificado. */
    private String viaCartaoDepositoIdentificado;

    /** Atributo codigoDepositante. */
    private String codigoDepositante;

    /** Atributo nomeDepositante. */
    private String nomeDepositante;

    /** Atributo codigoPagador. */
    private String codigoPagador;

    /** Atributo codigoOrdemCredito. */
    private String codigoOrdemCredito;

    /** Atributo nrCnpjCpf. */
    private String nrCnpjCpf;

    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;

    /** Atributo dsEmpresa. */
    private String dsEmpresa;

    /** Atributo contaDebito. */
    private String contaDebito;

    /** Atributo numeroInscricaoFavorecido. */
    private String numeroInscricaoFavorecido;

    /** Atributo dsFavorecido. */
    private String dsFavorecido;

    /** Atributo tipoFavorecido. */
    private Integer tipoFavorecido;

    /** Atributo numeroInscricaoBeneficiario. */
    private String numeroInscricaoBeneficiario;

    /** Atributo tipoBeneficiario. */
    private String tipoBeneficiario;

    /** Atributo cdTipoBeneficiario. */
    private Integer cdTipoBeneficiario;

    /** Atributo dsTipoBeneficiario. */
    private String dsTipoBeneficiario;

    /** Atributo nomeBeneficiario. */
    private String nomeBeneficiario;

    /** Atributo nossoNumero. */
    private String nossoNumero;

    /** Atributo dsOrigemPagamento. */
    private String dsOrigemPagamento;

    /** Atributo cdIndentificadorJudicial. */
    private String cdIndentificadorJudicial;

    /** Atributo dtLimiteDescontoPagamento. */
    private String dtLimiteDescontoPagamento;

    /** Atributo dtLimitePagamento. */
    private String dtLimitePagamento;

    /** Atributo cdRastreado. */
    private Integer cdRastreado;

    /** Atributo dsRastreado. */
    private String dsRastreado;

    /** Atributo carteira. */
    private String carteira;

    /** Atributo cdSituacaoTranferenciaAutomatica. */
    private String cdSituacaoTranferenciaAutomatica;

    /** Atributo descTipoFavorecido. */
    private String descTipoFavorecido;

    /** Atributo cdBancoOriginal. */
    private Integer cdBancoOriginal;

    /** Atributo dsBancoOriginal. */
    private String dsBancoOriginal;

    /** Atributo cdAgenciaBancariaOriginal. */
    private Integer cdAgenciaBancariaOriginal;

    /** Atributo cdDigitoAgenciaOriginal. */
    private String cdDigitoAgenciaOriginal;

    /** Atributo dsAgenciaOriginal. */
    private String dsAgenciaOriginal;

    /** Atributo cdContaBancariaOriginal. */
    private Long cdContaBancariaOriginal;

    /** Atributo cdDigitoContaOriginal. */
    private String cdDigitoContaOriginal;

    /** Atributo dsTipoContaOriginal. */
    private String dsTipoContaOriginal;

    /** Atributo listaGridPagamentosIndividuais. */
    private List<ConsultarPagamentosIndividuaisSaidaDTO> listaGridPagamentosIndividuais;

    /** Atributo itemSelecionadoLista. */
    private Integer itemSelecionadoLista;

    /** Atributo listaControleRadio. */
    private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

    /** Atributo listaGridPagamentosIndividuaisImprimir. */
    private List<ConsultarPagamentosIndividuaisSaidaDTO> listaGridPagamentosIndividuaisImprimir;

    /** Atributo listaPagamentosIndividuaisSaidaDTO. */
    private List<ConsultarPagamentosIndividuaisSaidaDTO> listaPagamentosIndividuaisSaidaDTO;

    /** Atributo tpoServicoGridHist. */
    private String tpoServicoGridHist;

    /** Atributo dsModalidadeGridHist. */
    private String dsModalidadeGridHist;

    /** Atributo numeroPagamentoGridHist. */
    private String numeroPagamentoGridHist;

    /** Atributo contaDebitoGridHist. */
    private String contaDebitoGridHist;

    /** Atributo dtDevolucaoEstorno. */
    private String dtDevolucaoEstorno;

    /** Atributo registroListaPagIndividuaisSelecionado. */
    private ConsultarPagamentosIndividuaisSaidaDTO registroListaPagIndividuaisSelecionado = null;

    /** Atributo saidaIndividualDTO. */
    private DetalharPagtoCreditoContaSaidaDTO saidaIndividualDTO;

    /** Atributo saidaDebitoContaDTO. */
    private DetalharManPagtoDebitoContaSaidaDTO saidaDebitoContaDTO;

    /** Atributo saidaPagtoTedDTO. */
    private DetalharManPagtoTEDSaidaDTO saidaPagtoTedDTO;

    /** Atributo saidaPagtoDocDTO. */
    private DetalharManPagtoDOCSaidaDTO saidaPagtoDocDTO;

    /** Atributo saidaOrdemPgtoDTO. */
    private DetalharManPagtoOrdemPagtoSaidaDTO saidaOrdemPgtoDTO;

    /** Atributo saidaPagtoTituloBardescoDTO. */
    private DetalharManPagtoTituloBradescoSaidaDTO saidaPagtoTituloBardescoDTO;

    /** Atributo saidaTituloOutrosBancosDTO. */
    private DetalharManPagtoTituloOutrosBancosSaidaDTO saidaTituloOutrosBancosDTO;

    /** Atributo saidaPagtoDarfDTO. */
    private DetalharManPagtoDARFSaidaDTO saidaPagtoDarfDTO;

    /** Atributo saidaPagtoGareDTO. */
    private DetalharManPagtoGARESaidaDTO saidaPagtoGareDTO;

    /** Atributo saidaPagtoGpsDTO. */
    private DetalharManPagtoGPSSaidaDTO saidaPagtoGpsDTO;

    /** Atributo saidaPagtoCodigoBarrasDTO. */
    private DetalharManPagtoCodigoBarrasSaidaDTO saidaPagtoCodigoBarrasDTO;

    /** Atributo saidaPagtoDebitoVeiculosDTO. */
    private DetalharManPagtoDebitoVeiculosSaidaDTO saidaPagtoDebitoVeiculosDTO;

    /** Atributo saidaPagtoDepIdentificadoDTO. */
    private DetalharManPagtoDepIdentificadoSaidaDTO saidaPagtoDepIdentificadoDTO;

    /** Atributo saidaPagtoOrdemCreditoDTO. */
    private DetalharManPagtoOrdemCreditoSaidaDTO saidaPagtoOrdemCreditoDTO;

    /** Atributo imprimirService. */
    private IImprimirService imprimirService = null;

    /** Atributo disableArgumentosConsulta. */
    private boolean disableArgumentosConsulta;

    /** Atributo exibeBeneficiario. */
    private boolean exibeBeneficiario;

    /** Atributo dtFloatingPagamento. */
    private String dtFloatingPagamento;

    /** Atributo dtEfetivFloatPgto. */
    private String dtEfetivFloatPgto;

    /** Atributo vlFloatingPagamento. */
    private BigDecimal vlFloatingPagamento;

    /** Atributo vlBonfcao. */
    private BigDecimal vlBonfcao;

    /** Atributo vlBonificacao. */
    private BigDecimal vlBonificacao;

    /** Atributo cdOperacaoDcom. */
    private Long cdOperacaoDcom;

    /** Atributo numControleInternoLote. */
    private Long numControleInternoLote;

    /** Atributo dsSituacaoDcom. */
    private String dsSituacaoDcom;

    /** Atributo cdFuncEmissaoOp. */
    private String cdFuncEmissaoOp;

    /** Atributo cdTerminalCaixaEmissaoOp. */
    private String cdTerminalCaixaEmissaoOp;

    /** Atributo dtEmissaoOp. */
    private String dtEmissaoOp;

    /** Atributo hrEmissaoOp. */
    private String hrEmissaoOp;

    /** Atributo tipoTela. */
    private Integer tipoTela;

    /** The vl efetivacao debito pagamento. */
    private BigDecimal vlEfetivacaoDebitoPagamento;

    /** The vl efetivacao credito pagamento. */
    private BigDecimal vlEfetivacaoCreditoPagamento;

    /** The vl desconto pagamento. */
    private BigDecimal vlDescontoPagamento;

    /** The hr efetivacao credito pagamento. */
    private String hrEfetivacaoCreditoPagamento;

    /** The hr envio credito pagamento. */
    private String hrEnvioCreditoPagamento;

    /** The cd identificador transferencia pagto. */
    private Integer cdIdentificadorTransferenciaPagto;

    /** The cd mensagem lin extrato. */
    private Integer cdMensagemLinExtrato;

    /** The cd conve cta salarial. */
    private Long cdConveCtaSalarial;

    /** The flag exibe campos. */
    private boolean flagExibeCampos; 

    private boolean exibeBancoCredito; 

    // flag

    /** Atributo exibeDcom. */
    private Boolean exibeDcom;

    /** Atributo habilitaImprimirComprovante. */
    private Boolean  habilitaImprimirComprovante;
    
    /** Atributo consultarAutorizantesOcorrencia. */
    private ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO> consultarAutorizantesOcorrencia =  
        new ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO>();
    
    /** Atributo itemSelecionadoListaAutorizante. */
    private Integer itemSelecionadoListaAutorizante;
    
    /** Atributo listaRadioAutorizante. */
    private ArrayList<SelectItem> listaRadioAutorizante;
    
    /** Atributo detalharPagtoTed. */
    private DetalharPagtoTEDSaidaDTO detalharPagtoTed = null;
    
    /** Atributo dsNomeReclamante. */
    private String dsNomeReclamante = null;
    
    /** Atributo nrProcessoDepositoJudicial. */
    private String nrProcessoDepositoJudicial = null;
    
    /** Atributo nrPisPasep. */
    private String nrPisPasep = null; 
    
    /** Atributo cdFinalidadeTedDepositoJudicial. */
    private boolean cdFinalidadeTedDepositoJudicial = false;

    /** Atributo detalheOrdemPagamento. */
    private DetalharPagtoOrdemPagtoSaidaDTO detalheOrdemPagamento = null;
    
    /** Atributo listaFormaPagamento. */
    private List<ListarTipoRetiradaOcorrenciasSaidaDTO> listaFormaPagamento = null;
    
    private Long cdLoteInterno = null;
    
    /** Atributo numeroInscricaoSacadorAvalista. */
    private String numeroInscricaoSacadorAvalista;

    /** Atributo dsSacadorAvalista. */
    private String dsSacadorAvalista;

    /** Atributo dsTipoSacadorAvalista. */
    private String dsTipoSacadorAvalista;
    
    
    // In�cio - M�todos
    /**
     * Limpar variaveis.
     */
    private void limparVariaveis() {
        setNrCnpjCpf(null);
        setDsRazaoSocial("");
        setDsEmpresa("");
        setNroContrato("");
        setDsContrato("");
        setCdSituacaoContrato("");
        setDsBancoDebito("");
        setDsAgenciaDebito("");
        setContaDebito(null);
        setTipoContaDebito("");
    }

    /**
     * Carrega cabecalho.
     */
    private void carregaCabecalho() {
        limparVariaveis();
        if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")) { // Filtrar por
            // cliente
            setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescCliente());
            setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescCliente());
            setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescCliente());
            setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescCliente());
        } else {
            if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) { // Filtrar por
                // contrato
                setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescContrato());
                setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescContrato());
                setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescContrato());
                setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescContrato());
            }
        }

        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());

        setNrCnpjCpf(CpfCnpjUtils.formatCpfCnpjCompleto(registroSelecionado.getNrCnpjCpf(), registroSelecionado
            .getNrFilialCnpjCpf(), registroSelecionado.getNrDigitoCnpjCpf()));
        setDsRazaoSocial(registroSelecionado.getDsRazaoSocial());

    }

    /**
     * Voltar historico.
     *
     * @return the string
     */
    public String voltarHistorico() {
        setItemSelecionadoListaHistorico(null);
        return "VOLTAR";
    }

    /**
     * Voltar consultar historico.
     *
     * @return the string
     */
    public String voltarConsultarHistorico() {
        setItemSelecionadoLista(null);

        return "VOLTAR";
    }

    /**
     * Detalhar historico.
     *
     * @return the string
     */
    public String detalharHistorico() {
        try {
            ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
                getItemSelecionadoLista());
            ConsultarHistConsultaSaldoSaidaDTO registroSelecionadoHistorico = getListaHistoricoConsulta().get(
                getItemSelecionadoListaHistorico());

            DetalharHistConsultaSaldoEntradaDTO entradaDTO = new DetalharHistConsultaSaldoEntradaDTO();

            /* Dados de Entrada */
            entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
            entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
            entradaDTO.setCdTipoCanal(registroSelecionado.getCdTipoCanal());
            
            String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    		retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    		entradaDTO.setCdControlePagamento(retiraChaves);
    		
            entradaDTO.setHrConsultasSaldoPagamento(registroSelecionadoHistorico.getHrConsultasSaldoPagamento());
            entradaDTO.setCdBanco(registroSelecionado.getCdBancoDebito());
            entradaDTO.setCdAgencia(registroSelecionado.getCdAgenciaDebito());
            entradaDTO.setCdConta(registroSelecionado.getCdContaDebito());
            entradaDTO.setCdModalidade(registroSelecionado.getCdProdutoServicoRelacionado());
            entradaDTO.setDtPagamento(registroSelecionado.getDtCreditoPagamento());
            
            DetalharHistConsultaSaldoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
            .detalharHistConsultaSaldo(entradaDTO);

            // Pagamento
            setDsTipoServico(registroSelecionado.getDsResumoProdutoServico()); // �
            // com
            // esse
            // campo
            // que
            // preenche?
            setDsIndicadorModalidade(registroSelecionado.getDsOperacaoProdutoServico()); // � com esse campo que
            // preenche?
            setNrSequenciaArquivoRemessa(saidaDTO.getNrArquivoRemessaPagamento());
            setNrLoteArquivoRemessa(saidaDTO.getCdPessoaLoteRemessa());
            setValorPagamento(saidaDTO.getVlPagamento());
           		
        	setNumeroPagamento(retiraChaves);
        	
            setCdListaDebito(saidaDTO.getCdListaDebitoPagamento());
            setCanal(saidaDTO.getDsCanal());
            setDtPagamento(saidaDTO.getDtCreditoPagamento());
            setDtVencimento(saidaDTO.getDtVencimento());

            // Consulta de Saldo
            setDataHoraConsultaSaldo(registroSelecionadoHistorico.getDataHoraFormatada());
            setSinalMomentoLancamento(saidaDTO.getSinal());
            setSaldoMomentoLancamento(saidaDTO.getVlSaldoAnteriorLancamento());
            setSaldoOperacional(saidaDTO.getVlSaldoOperacional());
            setSaldoVincSemReserva(saidaDTO.getVlSaldoSemReserva());
            setSaldoVincComReserva(saidaDTO.getVlSaldoComReserva());
            setSaldoVincJudicial(saidaDTO.getVlSaldoVinculadoJudicial());
            setSaldoVincAdm(saidaDTO.getVlSaldoVinculadoAdministrativo());
            setSaldoVincSeguranca(saidaDTO.getVlSaldoVinculadoSeguranca());
            setSaldoVincAdmLp(saidaDTO.getVlSaldoVinculadoAdministrativoLp());
            setSaldoAgregadoFundo(saidaDTO.getVlSaldoAgregadoFundo());
            setSaldoAgregadoCDB(saidaDTO.getVlSaldoAgregadoCdb());
            setSaldoAgregadoPoupanca(saidaDTO.getVlSaldoAgregadoPoupanca());
            setSaldoAgregadoCredito(saidaDTO.getVlSaldoCreditoRotativo());
            setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

            // Trilha de Auditoria
            setDataHoraInclusao(saidaDTO.getDtInclusao() + " " + saidaDTO.getHrInclusao());
            setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
            setTipoCanalInclusao(saidaDTO.getCdTipoCanalInclusao() != 0 ? saidaDTO.getCdTipoCanalInclusao() + " - "
                + saidaDTO.getDsTipoCanalInclusao() : "");
            setComplementoInclusao(saidaDTO.getCdFluxoInclusao() == null || saidaDTO.getCdFluxoInclusao().equals("0") ? ""
                : saidaDTO.getCdFluxoInclusao());

            setDataHoraManutencao(saidaDTO.getDtManutencao() + " " + saidaDTO.getHrManutencao());
            setUsuarioManutencao(saidaDTO.getCdUsuarioManutencao());
            setTipoCanalManutencao(saidaDTO.getCdTipoCanalManutencao() == 0 ? "" : saidaDTO.getCdTipoCanalManutencao()
                + " - " + saidaDTO.getDsTipoCanalManutencao());
            setComplementoManutencao(saidaDTO.getCdFluxoManutencao() == null
                || saidaDTO.getCdFluxoManutencao().equals("0") ? "" : saidaDTO.getCdFluxoManutencao());
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            return "";
        }

        return "DETALHAR";
    }

    /**
     * Detalhar manutencao.
     *
     * @return the string
     */
    public String detalharManutencao() {
        try {
            setManutencao(true);
            ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
                getItemSelecionadoLista());
            if (registroSelecionado.getCdTipoTela() == 1) {
                preencheDadosManPagtoCreditoConta();
                return "DETALHE_MANUTENCAO_CREDITO_CONTA";
            }
            if (registroSelecionado.getCdTipoTela() == 2) {
                preencheDadosManPagtoDebitoConta();
                return "DETALHE_MANUTENCAO_DEBITO_CONTA";
            }
            if (registroSelecionado.getCdTipoTela() == 3) {
                preencheDadosManPagtoDOC();
                return "DETALHE_MANUTENCAO_DOC";
            }
            if (registroSelecionado.getCdTipoTela() == 4) {
                preencheDadosManPagtoTED();
                return "DETALHE_MANUTENCAO_TED";
            }
            if (registroSelecionado.getCdTipoTela() == 5) {
                preencheDadosManPagtoOrdemPagto();
                return "DETALHE_MANUTENCAO_ORDEM_PAGTO";
            }
            if (registroSelecionado.getCdTipoTela() == 6) {
                preencheDadosManPagtoTituloBradesco();
                return "DETALHE_MANUTENCAO_TITULOS_BRADESCO";
            }
            if (registroSelecionado.getCdTipoTela() == 7) {
                preencheDadosManPagtoTituloOutrosBancos();
                return "DETALHE_MANUTENCAO_TITULOS_OUTROS_BANCOS";
            }
            if (registroSelecionado.getCdTipoTela() == 8) {
                preencheDadosManPagtoDARF();
                return "DETALHE_MANUTENCAO_DARF";
            }
            if (registroSelecionado.getCdTipoTela() == 9) {
                preencheDadosManPagtoGARE();
                return "DETALHE_MANUTENCAO_GARE";
            }
            if (registroSelecionado.getCdTipoTela() == 10) {
                preencheDadosManPagtoGPS();
                return "DETALHE_MANUTENCAO_GPS";
            }
            if (registroSelecionado.getCdTipoTela() == 11) {
                preencheDadosManPagtoCodigoBarras();
                return "DETALHE_MANUTENCAO_CODIGO_BARRAS";
            }
            if (registroSelecionado.getCdTipoTela() == 12) {
                preencheDadosManPagtoDebitoVeiculos();
                return "DETALHE_DEBITO_VEICULOS";
            }
            if (registroSelecionado.getCdTipoTela() == 13) {
                preencheDadosManPagtoDepIdentificado();
                return "DETALHE_MANUTENCAO_DEPOSITO_IDENTIFICADO";
            }
            if (registroSelecionado.getCdTipoTela() == 14) {
                preencheDadosManPagtoOrdemCredito();
                return "DETALHE_MANUTENCAO_ORDEM_CREDITO";
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
        }

        return "";
    }

    /**
     * Voltar manutencao.
     *
     * @return the string
     */
    public String voltarManutencao() {
        setItemSelecionadoLista(null);

        return "VOLTAR";
    }

    /**
     * Voltar detalhe.
     *
     * @return the string
     */
    public String voltarDetalhe() {
        setHrEnvioCreditoPagamento(null);
        if (!isManutencao()) {
            setItemSelecionadoLista(null);
            consultarPagamentosIndividuais();
            return "VOLTAR1";
        } else {
            if (isManutencao()) {
                setItemSelecionadoListaManutencao(null);
                return "VOLTAR2";
            }
        }
        return "";
    }

    /**
     * Limpar favorecido beneficiario.
     */
    public void limparFavorecidoBeneficiario() {
        setNumeroInscricaoFavorecido("");
        setTipoFavorecido(0);
        setDsFavorecido("");
        setCdFavorecido(null);
        setDsBancoFavorecido("");
        setDsAgenciaFavorecido("");
        setDsContaFavorecido("");
        setDsTipoContaFavorecido("");
        setDescTipoFavorecido("");
        setNumeroInscricaoBeneficiario("");
        setCdTipoBeneficiario(0);
        setNomeBeneficiario("");
        setDtNascimentoBeneficiario("");
        setDsTipoBeneficiario("");
        setNumeroInscricaoSacadorAvalista("");
        setDsTipoSacadorAvalista("");
        setDsSacadorAvalista("");        
    }

    // Detalhar Pagamentos Individuais - Cr�dito em Conta
    /**
     * Preenche dados pagto credito conta.
     */
    public void preencheDadosPagtoCreditoConta() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoCreditoContaSaidaDTO saidaDTO = 
        	getConsultarPagamentosIndividualServiceImpl().detalharPagtoCreditoConta(entradaDTO);

        // Cabe�alho
        setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
            .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
        setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(), saidaDTO.getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
            saidaDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(), saidaDTO.getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidade() == 1 || saidaDTO.getCdIndicadorModalidade() == 3) {
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
            /*
             * Preenchendo banco/ag�ncia/conta da parte de favorecidos com os dados da lista da tela anterior (campos
             * Banco/ag�ncia/conta de Cr�dito) conforme solicitado por Fabio De Almeida via email ter�a-feira, 15 de
             * fevereiro de 2011
             */

            setDsBancoOriginal(saidaDTO.getBancoDestinoFormatado());
            setDsAgenciaOriginal(saidaDTO.getAgenciaDestinoFormatada());
            setCdDigitoContaOriginal(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

            setExibeDcom(saidaDTO.getCdIndicadorModalidade() == 1 ? true : false);
        } else {

            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
            || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
            || !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
    	
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlrAgendamento());
        setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
            .getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil
            .formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
        
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setCdSituacaoTranferenciaAutomatica(saidaDTO.getCdSituacaoTranferenciaAutomatica());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
        setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

        // trilhaAuditoria
        setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
        setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
        setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());        
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        //Favorecidos
        setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
        setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - " + saidaDTO.getDsBancoOriginal());
        setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
        setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
        setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-" + saidaDTO.getCdDigitoAgenciaOriginal() + " - " + saidaDTO.getDsAgenciaOriginal());
        setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
        setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-" + saidaDTO.getCdDigitoContaOriginal());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());
        setDsEstornoPagamento(saidaDTO.getDsEstornoPagamento());

        SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
        SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        setVlEfetivacaoDebitoPagamento(saidaDTO.getVlEfetivacaoDebitoPagamento());
        setVlEfetivacaoCreditoPagamento(saidaDTO.getVlEfetivacaoCreditoPagamento());
        setVlDescontoPagamento(saidaDTO.getVlDescontoPagamento());
        try {
            setHrEfetivacaoCreditoPagamento(formato2.format(formato1.parse(saidaDTO.getHrEfetivacaoCreditoPagamento())));
            setHrEnvioCreditoPagamento(formato2.format(formato1.parse(saidaDTO.getHrEnvioCreditoPagamento())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        setCdIdentificadorTransferenciaPagto(saidaDTO.getCdIdentificadorTransferenciaPagto());
        if ("".equals(saidaDTO.getDsIdentificadorTransferenciaPagto()) || 
                        "".equals(saidaDTO.getCdIdentificadorTransferenciaPagto())){
            setDsIdentificadorTransferenciaPagto(saidaDTO.getCdIdentificadorTransferenciaPagto() 
                + saidaDTO.getDsIdentificadorTransferenciaPagto());
        }else{
            setDsIdentificadorTransferenciaPagto(saidaDTO.getCdIdentificadorTransferenciaPagto() 
                +" - "+ saidaDTO.getDsIdentificadorTransferenciaPagto());
        }
        setCdMensagemLinExtrato(saidaDTO.getCdMensagemLinExtrato());
        setCdConveCtaSalarial(saidaDTO.getCdConveCtaSalarial());
        
        setTipoContaDestinoPagamento(saidaDTO.getDsTipoContaDestino());
        setCdBancoDestinoPagamento(saidaDTO.getCdBancoDestino().toString());
        setContaDigitoDestinoPagamento(saidaDTO.getContaDestinoFormatada());
        setDsISPB(saidaDTO.getCdIspbPagtoDestino() + " - " + saidaDTO.getDsBancoDestino());
        setCdContaPagamentoDet(saidaDTO.getContaPagtoDestino());
        
        if(saidaDTO.getCdIndicadorModalidade() == 3){
        	setExibeBancoCredito(true);
        	if(saidaDTO.getContaPagtoDestino().equals("0")){
        		setCdBancoDestinoPagamento(null);
        		setContaDigitoDestinoPagamento(null);
        		setTipoContaDestinoPagamento(null);
        		setDsISPB(null);
        		setCdContaPagamentoDet(null);
        	}else {
        		setCdBancoDestino(null);
        		setAgenciaDigitoDestino(null);
        		setContaDigitoDestino(null);
        		setTipoContaDestino(null);
        	}
        }else{
        	setExibeBancoCredito(false);
        }
        
        setCdLoteInterno(saidaDTO.getCdLoteInterno());
        
    }

    // Detalhar Pagamentos Individuais - D�bito em conta
    /**
     * Preenche dados pagto debito conta.
     */
    public void preencheDadosPagtoDebitoConta() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoDebitoContaEntradaDTO entradaDTO = new DetalharPagtoDebitoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDebitoConta(entradaDTO);

        // Cabe�alho - Cliente
        setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
            .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setDsRazaoSocial(saidaDTO.getNomeCliente());

        // Sempre Carregar Conta de D�bito
        setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(), saidaDTO.getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
            saidaDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(), saidaDTO.getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // Favorecido
        setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
            .getNmInscriFavorecido()));
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
            .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoOriginal(saidaDTO.getBancoDestinoFormatado());
        setDsAgenciaOriginal(saidaDTO.getAgenciaDestinoFormatada());
        setCdDigitoContaOriginal(saidaDTO.getContaDestinoFormatada());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

        // Sacador/Avalista
      //  setNumeroInscricaoSacadorAvalista(saidaDTO.getNu);
      //  setDsTipoSacadorAvalista("");
      //  setDsSacadorAvalista("");        
        
        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlrAgendamento());
        setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
            .getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil
            .formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        //setDtDevolucaoEstorno(FormatarData.formataDiaMesAno(saidaDTO.getDtDevolucaoEstorno()));
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
        setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
        setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Pagamentos Individuais - DOC
    /**
     * Preenche dados pagto doc.
     */
    public void preencheDadosPagtoDOC() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoDOCEntradaDTO entradaDTO = new DetalharPagtoDOCEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoDOC(entradaDTO);

        // Cabe�alho
        setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
            .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;

        if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaDTO.getCdFavorecido()));
            setDsBancoOriginal(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaOriginal(saidaDTO.getAgenciaCreditoFormatada());
            setCdDigitoContaOriginal(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

            setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
            || !PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals("")
            || !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
        setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
        setIdentificacaoTransferenciaDoc(saidaDTO.getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
        
        setCdLoteInterno(saidaDTO.getCdLoteInterno());
    }

    // Detalhar Pagamentos Individuais - TED
    /**
     * Preenche dados pagto ted.
     */
    public void preencheDadosPagtoTED() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoTEDEntradaDTO entradaDTO = new DetalharPagtoTEDEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        detalharPagtoTed = getConsultarPagamentosIndividualServiceImpl().detalharPagtoTED(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(detalharPagtoTed.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(detalharPagtoTed.getNomeCliente());
        setDsEmpresa(detalharPagtoTed.getDsPessoaJuridicaContrato());
        setNroContrato(detalharPagtoTed.getNrSequenciaContratoNegocio());
        setDsContrato(detalharPagtoTed.getDsContrato());
        setCdSituacaoContrato(detalharPagtoTed.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(detalharPagtoTed.getBancoDebitoFormatado());
        setDsAgenciaDebito(detalharPagtoTed.getAgenciaDebitoFormatada());
        setContaDebito(detalharPagtoTed.getContaDebitoFormatada());
        setTipoContaDebito(detalharPagtoTed.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (detalharPagtoTed.getCdIndicadorModalidadePgit() == 1 || detalharPagtoTed.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(detalharPagtoTed.getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(detalharPagtoTed.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(detalharPagtoTed.getDsNomeFavorecido());
            setCdFavorecido(detalharPagtoTed.getCdFavorecido() == null || detalharPagtoTed.getCdFavorecido().equals("") ? null : Long.parseLong(detalharPagtoTed.getCdFavorecido()));
            setDsBancoOriginal(detalharPagtoTed.getBancoCreditoFormatado());
            setDsAgenciaOriginal(detalharPagtoTed.getAgenciaCreditoFormatada());
            setCdDigitoContaOriginal(detalharPagtoTed.getContaCreditoFormatada());
            setDsTipoContaOriginal(detalharPagtoTed.getDsTipoContaFavorecido());

            setExibeDcom(detalharPagtoTed.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(detalharPagtoTed.getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(detalharPagtoTed.getCdTipoInscriBeneficio());
            setNomeBeneficiario(detalharPagtoTed.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
            || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
            || !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(detalharPagtoTed.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(detalharPagtoTed.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(detalharPagtoTed.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(detalharPagtoTed.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(detalharPagtoTed.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(detalharPagtoTed.getCdListaDebito());
        setDtAgendamento(detalharPagtoTed.getDtAgendamento());
        setDtPagamento(detalharPagtoTed.getDtPagamento());
        setDtVencimento(detalharPagtoTed.getDtVencimento());
        setCdIndicadorEconomicoMoeda(detalharPagtoTed.getDsMoeda());
        setQtMoeda(detalharPagtoTed.getQtMoeda());
        setVlrAgendamento(detalharPagtoTed.getVlAgendado());
        setVlrEfetivacao(detalharPagtoTed.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(detalharPagtoTed.getCdSituacaoPagamento());
        setDsMotivoSituacao(detalharPagtoTed.getDsMotivoSituacao());
        setDsPagamento(detalharPagtoTed.getDsPagamento());
        setNrDocumento(detalharPagtoTed.getNrDocumento());
        setTipoDocumento(detalharPagtoTed.getDsTipoDocumento());
        setCdSerieDocumento(detalharPagtoTed.getCdSerieDocumento());
        setVlDocumento(detalharPagtoTed.getVlDocumento());
        setDtEmissaoDocumento(detalharPagtoTed.getDtEmissaoDocumento());
        setDsUsoEmpresa(detalharPagtoTed.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(detalharPagtoTed.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(detalharPagtoTed.getDsMensagemSegundaLinha());
        setCdBancoCredito(String.valueOf(detalharPagtoTed.getCdBancoCredito()));
        setDsBancoCredito(detalharPagtoTed.getDsBancoCredito());
        setCdIspbPagamento(detalharPagtoTed.getCdIspbPagamento());
        setDsNomeReclamante(detalharPagtoTed.getDsNomeReclamante());
        setNrProcessoDepositoJudicial(detalharPagtoTed.getNrProcessoDepositoJudicial());
        setNrPisPasep(NumberUtils.formatarNumeroPisPasep(detalharPagtoTed.getNrPisPasep()));
        setCdFinalidadeTedDepositoJudicial(detalharPagtoTed.isCdFinalidadeTedDepositoJudicial());
        setAgenciaDigitoCredito(detalharPagtoTed.getAgenciaCreditoFormatada());
        setContaDigitoCredito(detalharPagtoTed.getContaCreditoFormatada());
        setTipoContaCredito(detalharPagtoTed.getDsTipoContaFavorecido());
        setCamaraCentralizadora(detalharPagtoTed.getCdCamaraCentral());
        setDtFloatingPagamento(detalharPagtoTed.getDtFloatingPagamento());
        setDtEfetivFloatPgto(detalharPagtoTed.getDtEfetivFloatPgto());
        setVlFloatingPagamento(detalharPagtoTed.getVlFloatingPagamento());
        setCdOperacaoDcom(detalharPagtoTed.getCdOperacaoDcom());
        setDsSituacaoDcom(detalharPagtoTed.getDsSituacaoDcom());

        setFinalidadeTed(PgitUtil.concatenarCampos(detalharPagtoTed.getCdFinalidadeTed(),detalharPagtoTed.getDsFinalidadeDocTed(), " - "));
        setIdentificacaoTransferenciaTed(detalharPagtoTed.getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(detalharPagtoTed.getDsidentificacaoTitularPagto());

        setVlDesconto(detalharPagtoTed.getVlDesconto());
        setVlAbatimento(detalharPagtoTed.getVlAbatidoCredito());
        setVlMulta(detalharPagtoTed.getVlMultaCredito());
        setVlMora(detalharPagtoTed.getVlMoraJuros());
        setVlDeducao(detalharPagtoTed.getVlOutrasDeducoes());
        setVlAcrescimo(detalharPagtoTed.getVlOutroAcrescimo());
        setVlImpostoRenda(detalharPagtoTed.getVlIrCredito());
        setVlIss(detalharPagtoTed.getVlIssCredito());
        setVlIof(detalharPagtoTed.getVlIofCredito());
        setVlInss(detalharPagtoTed.getVlInssCredito());
        setCdIndentificadorJudicial(detalharPagtoTed.getCdIndentificadorJudicial());
        setDtDevolucaoEstorno(detalharPagtoTed.getDtDevolucaoEstorno());

        // trilhaAuditoria
        setDataHoraInclusao(detalharPagtoTed.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(detalharPagtoTed.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(detalharPagtoTed.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(detalharPagtoTed.getComplementoInclusaoFormatado());
        setDataHoraManutencao(detalharPagtoTed.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(detalharPagtoTed.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(detalharPagtoTed.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(detalharPagtoTed.getComplementoManutencaoFormatado());
        
        setCdLoteInterno(detalharPagtoTed.getCdLoteInterno());

    }

    // Detalhar Pagamentos Individuais - Ordem de Pagamento
    /**
     * Preenche dados pagto ordem pagto.
     */
    public void preencheDadosPagtoOrdemPagto() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharPagtoOrdemPagtoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
		
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        detalheOrdemPagamento = getConsultarPagamentosIndividualServiceImpl().detalharPagtoOrdemPagto(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(detalheOrdemPagamento.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(detalheOrdemPagamento.getNomeCliente());
        setDsEmpresa(detalheOrdemPagamento.getDsPessoaJuridicaContrato());
        setNroContrato(detalheOrdemPagamento.getNrSequenciaContratoNegocio());
        setDsContrato(detalheOrdemPagamento.getDsContrato());
        setCdSituacaoContrato(detalheOrdemPagamento.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(detalheOrdemPagamento.getBancoDebitoFormatado());
        setDsAgenciaDebito(detalheOrdemPagamento.getAgenciaDebitoFormatada());
        setContaDebito(detalheOrdemPagamento.getContaDebitoFormatada());
        setTipoContaDebito(detalheOrdemPagamento.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;

        if (detalheOrdemPagamento.getCdIndicadorModalidadePgit() == 1
            || detalheOrdemPagamento.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(detalheOrdemPagamento.getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(detalheOrdemPagamento.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(detalheOrdemPagamento.getDsNomeFavorecido());
            setCdFavorecido(detalheOrdemPagamento.getCdFavorecido() == null
                || detalheOrdemPagamento.getCdFavorecido().equals("") ? null : Long.parseLong(detalheOrdemPagamento
                .getCdFavorecido()));
            setDsBancoOriginal(detalheOrdemPagamento.getBancoCreditoFormatado());
            setDsAgenciaOriginal(detalheOrdemPagamento.getAgenciaCreditoFormatada());
            setCdDigitoContaOriginal(detalheOrdemPagamento.getContaCreditoFormatada());
            setDsTipoContaOriginal(detalheOrdemPagamento.getDsTipoContaFavorecido());
        } else {
            setNumeroInscricaoBeneficiario(detalheOrdemPagamento.getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(detalheOrdemPagamento.getCdTipoInscriBeneficio());
            setNomeBeneficiario(detalheOrdemPagamento.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
            || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
            || !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
        }

        if (detalheOrdemPagamento.getCdSituacaoOperacaoPagamento() == 6
            || detalheOrdemPagamento.getCdSituacaoOperacaoPagamento() == 10) {
            setHabilitaImprimirComprovante(false);
        } else {
            setHabilitaImprimirComprovante(true);
        }

        // Pagamento
        setDsTipoServico(detalheOrdemPagamento.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(detalheOrdemPagamento.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(detalheOrdemPagamento.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(detalheOrdemPagamento.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(detalheOrdemPagamento.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(detalheOrdemPagamento.getCdListaDebito());
        setDtAgendamento(detalheOrdemPagamento.getDtAgendamento());
        setDtPagamento(detalheOrdemPagamento.getDtPagamento());
        setDtVencimento(detalheOrdemPagamento.getDtVencimento());
        setCdIndicadorEconomicoMoeda(detalheOrdemPagamento.getDsMoeda());
        setQtMoeda(detalheOrdemPagamento.getQtMoeda());
        setVlrAgendamento(detalheOrdemPagamento.getVlAgendado());
        setVlrEfetivacao(detalheOrdemPagamento.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(detalheOrdemPagamento.getCdSituacaoPagamento());
        setDsMotivoSituacao(detalheOrdemPagamento.getDsMotivoSituacao());
        setDsPagamento(detalheOrdemPagamento.getDsPagamento());
        setNrDocumento(detalheOrdemPagamento.getNrDocumento());
        setTipoDocumento(detalheOrdemPagamento.getDsTipoDocumento());
        setCdSerieDocumento(detalheOrdemPagamento.getCdSerieDocumento());
        setVlDocumento(detalheOrdemPagamento.getVlDocumento());
        setDtEmissaoDocumento(detalheOrdemPagamento.getDtEmissaoDocumento());
        setDsUsoEmpresa(detalheOrdemPagamento.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(detalheOrdemPagamento.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(detalheOrdemPagamento.getDsMensagemSegundaLinha());
        setCdBancoCredito(detalheOrdemPagamento.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(detalheOrdemPagamento.getAgenciaCreditoFormatada());
        setContaDigitoCredito(detalheOrdemPagamento.getContaCreditoFormatada());
        setTipoContaCredito(detalheOrdemPagamento.getDsTipoContaFavorecido());
        setNumeroOp(detalheOrdemPagamento.getNrOperacao());
        setDataExpiracaoCredito(detalheOrdemPagamento.getDtExpedicaoCredito());
        setInstrucaoPagamento(detalheOrdemPagamento.getCdInsPagamento());
        setNumeroCheque(detalheOrdemPagamento.getNrCheque());
        setSerieCheque(detalheOrdemPagamento.getNrSerieCheque());
        setIndicadorAssociadoUnicaAgencia(detalheOrdemPagamento.getDsUnidadeAgencia());
        setDtDevolucaoEstorno(detalheOrdemPagamento.getDtDevolucaoEstorno());
        setDtFloatingPagamento(detalheOrdemPagamento.getDtFloatingPagamento());
        setDtEfetivFloatPgto(detalheOrdemPagamento.getDtEfetivFloatPgto());
        setVlFloatingPagamento(detalheOrdemPagamento.getVlFloatingPagamento());
        setCdOperacaoDcom(detalheOrdemPagamento.getCdOperacaoDcom());
        setDsSituacaoDcom(detalheOrdemPagamento.getDsSituacaoDcom());

        setIdentificadorTipoRetirada(detalheOrdemPagamento.getDsIdentificacaoTipoRetirada());
        setIdentificadorRetirada(detalheOrdemPagamento.getDsIdentificacaoRetirada());

        setDataRetirada(detalheOrdemPagamento.getDtRetirada());
        setVlImpostoMovFinanceira(detalheOrdemPagamento.getVlImpostoMovimentacaoFinanceira());
        setVlDesconto(detalheOrdemPagamento.getVlDescontoCredito());
        setVlAbatimento(detalheOrdemPagamento.getVlAbatidoCredito());
        setVlMulta(detalheOrdemPagamento.getVlMultaCredito());
        setVlMora(detalheOrdemPagamento.getVlMoraJuros());
        setVlDeducao(detalheOrdemPagamento.getVlOutrasDeducoes());
        setVlAcrescimo(detalheOrdemPagamento.getVlOutroAcrescimo());
        setVlImpostoRenda(detalheOrdemPagamento.getVlIrCredito());
        setVlIss(detalheOrdemPagamento.getVlIssCredito());
        setVlIof(detalheOrdemPagamento.getVlIofCredito());
        setVlInss(detalheOrdemPagamento.getVlInssCredito());
        setDtDevolucaoEstorno(detalheOrdemPagamento.getDtDevolucaoEstorno());

        // trilhaAuditoria
        setCdFuncEmissaoOp(detalheOrdemPagamento.getCdFuncEmissaoOp());
        setCdTerminalCaixaEmissaoOp(detalheOrdemPagamento.getCdTerminalCaixaEmissaoOp());
        setDtEmissaoOp(detalheOrdemPagamento.getDtEmissaoOp());
        setHrEmissaoOp(detalheOrdemPagamento.getHrEmissaoOp());
        setDataHoraInclusao(detalheOrdemPagamento.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(detalheOrdemPagamento.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(detalheOrdemPagamento.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(detalheOrdemPagamento.getComplementoInclusaoFormatado());
        setDataHoraManutencao(detalheOrdemPagamento.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(detalheOrdemPagamento.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(detalheOrdemPagamento.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(detalheOrdemPagamento.getComplementoManutencaoFormatado());
        setCdLoteInterno(detalheOrdemPagamento.getCdLoteInterno());
    }

    // Detalhar Pagamentos Individuais - Titulos Bradesco
    /**
     * Preenche dados pagto titulo bradesco.
     */
    public void preencheDadosPagtoTituloBradesco() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharPagtoTituloBradescoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoTituloBradesco(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
            .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoOriginal(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaOriginal(saidaDTO.getAgenciaCreditoFormatada());
        setCdDigitoContaOriginal(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

        // SACADOR/AVALISTA
        setNumeroInscricaoSacadorAvalista(saidaDTO.getNumeroInscricaoSacadorAvalista());
        setDsSacadorAvalista(saidaDTO.getDsSacadorAvalista());
        setDsTipoSacadorAvalista(saidaDTO.getDsTipoSacadorAvalista());        
        
        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setCarteira(saidaDTO.getCarteira());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
        setDsRastreado(saidaDTO.getCdRastreado()==1?"Sim":"N�o");
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
        setVlBonfcao(saidaDTO.getVlBonfcao());
        setVlBonificacao(saidaDTO.getVlBonificacao());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setCdLoteInterno(saidaDTO.getCdLoteInterno());
    }

    // Detalhar Pagamentos Individuais - Titulos Outros Bancos
    /**
     * Preenche dados pagto titulo outros bancos.
     */
    public void preencheDadosPagtoTituloOutrosBancos() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharPagtoTituloOutrosBancosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoTituloOutrosBancos(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        
        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
            .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoOriginal(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaOriginal(saidaDTO.getAgenciaCreditoFormatada());
        setCdDigitoContaOriginal(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

        // SACADOR/AVALISTA
        setNumeroInscricaoSacadorAvalista(saidaDTO.getNumeroInscricaoSacadorAvalista());
        setDsSacadorAvalista(saidaDTO.getDsSacadorAvalista());
        setDsTipoSacadorAvalista(saidaDTO.getDsTipoSacadorAvalista());        
        
        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setCarteira(saidaDTO.getCarteira());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
        setDsRastreado(saidaDTO.getCdRastreado()==1?"Sim":"N�o");
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
        setVlBonfcao(saidaDTO.getVlBonfcao());
        setVlBonificacao(saidaDTO.getVlBonificacao());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setCdLoteInterno(saidaDTO.getCdLoteInterno());
    }

    // Detalhar Pagamentos Individuais - DARF
    /**
     * Preenche dados pagto darf.
     */
    public void preencheDadosPagtoDARF() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(getItemSelecionadoLista());
        DetalharPagtoDARFEntradaDTO entradaDTO = new DetalharPagtoDARFEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0 : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoDARF(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
            .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoOriginal(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaOriginal(saidaDTO.getAgenciaCreditoFormatada());
        setCdDigitoContaOriginal(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaDTO.getCdTelefone());
        setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
        setAgenteArrecadador(saidaDTO.getCdAgenteArrecad());
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcela());
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
        setAnoExercicio(saidaDTO.getCdDanoExercicio());
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
        setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
        setVlMulta(saidaDTO.getVlMultaCredito());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
        
        setCdLoteInterno(saidaDTO.getCdLoteInterno());

    }

    // Detalhar Pagamentos Individuais - GARE
    /**
     * Preenche dados pagto gare.
     */
    public void preencheDadosPagtoGARE() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoGAREEntradaDTO entradaDTO = new DetalharPagtoGAREEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoGARE(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
            .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoOriginal(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaOriginal(saidaDTO.getAgenciaCreditoFormatada());
        setCdDigitoContaOriginal(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtPagamentoGare(saidaDTO.getDtPagamentoGare());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
        setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadualContribuinte());
        setCodigoReceita(saidaDTO.getCdReceita());
        setNumeroReferencia(saidaDTO.getNrReferencia());
        setNumeroCotaParcela(saidaDTO.getNrCota());
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
        setDataReferencia(saidaDTO.getDtReferencia());
        setCodigoCnae(saidaDTO.getCdCnae());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaDTO.getNrParcela());
        setCodigoOrgaoFavorecido(saidaDTO.getCdOrFavorecido());
        setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsTributo());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setCdLoteInterno(saidaDTO.getCdLoteInterno());
    }

    // Detalhar Pagamentos Individuais - GPS
    /**
     * Preenche dados pagto gps.
     */
    public void preencheDadosPagtoGPS() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoGPSEntradaDTO entradaDTO = new DetalharPagtoGPSEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoGPS(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
            .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoOriginal(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaOriginal(saidaDTO.getAgenciaCreditoFormatada());
        setCdDigitoContaOriginal(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
        setCodigoInss(saidaDTO.getCdInss());
        setDataCompetencia(saidaDTO.getDsMesAnoCompt());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setCdLoteInterno(saidaDTO.getCdLoteInterno());
    }

    // Detalhar Pagamentos Individuais - C�digo de Barras
    /**
     * Preenche dados pagto codigo barras.
     */
    public void preencheDadosPagtoCodigoBarras() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharPagtoCodigoBarrasEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoCodigoBarras(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
            .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoOriginal(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaOriginal(saidaDTO.getAgenciaCreditoFormatada());
        setCdDigitoContaOriginal(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaDTO.getNrTelefone());
        setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
        setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setAgenteArrecadador(saidaDTO.getCdAgenteArrecadador());
        setCodigoTributo(saidaDTO.getCdTributoArrecadado());
        setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTributo());
        setPercentualReceita(saidaDTO.getCdPercentualTributo());
        setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
        setAnoExercicio(saidaDTO.getCdAnoExercicioTributo());
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setDataCompetencia(saidaDTO.getDtCompensacao());
        setCodigoCnae(saidaDTO.getCdCnae());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaDTO.getNrParcelamentoTributo());
        setCodigoOrgaoFavorecido(saidaDTO.getCdOrgaoFavorecido());
        setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsObservacao());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setCdLoteInterno(saidaDTO.getCdLoteInterno());
    }

    // Detalhar Pagamentos Individuais - D�bito de Veiculos
    /**
     * Preenche dados pagto debito veiculos.
     */
    public void preencheDadosPagtoDebitoVeiculos() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharPagtoDebitoVeiculosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDebitoVeiculos(entradaDTO);
        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
            .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoOriginal(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaOriginal(saidaDTO.getAgenciaCreditoFormatada());
        setCdDigitoContaOriginal(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTrib());
        setAnoExercicio(saidaDTO.getDtAnoExercTributo());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
        setMunicipioTributo(saidaDTO.getCdMunicArrecadTrib());
        setUfTributo(saidaDTO.getCdUfTributo());
        setNumeroNsu(saidaDTO.getNrNsuTributo());
        setOpcaoTributo(saidaDTO.getCdOpcaoTributo());
        setOpcaoRetiradaTributo(saidaDTO.getCdOpcaoRetirada());
        setVlPrincipal(saidaDTO.getVlPrincipalTributo());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setObservacaoTributo(saidaDTO.getDsObservacaoTributo());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

    }

    // Detalhar Pagamentos Individuais - Deposito Identificado
    /**
     * Preenche dados pagto dep identificado.
     */
    public void preencheDadosPagtoDepIdentificado() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharPagtoDepIdentificadoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDepIdentificado(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
            .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoOriginal(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaOriginal(saidaDTO.getAgenciaCreditoFormatada());
        setCdDigitoContaOriginal(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
            .getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil
            .formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setClienteDepositoIdentificado(saidaDTO.getCdClienteDepositoId());
        setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
        setProdutoDepositoIdentificado(saidaDTO.getCdProdutoDepositoId());
        setSequenciaProdutoDepositoIdentificado(saidaDTO.getCdSequenciaProdutoDepositoId());
        setBancoCartaoDepositoIdentificado(saidaDTO.getCdBancoCartaoDepositanteId());
        setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
        setBimCartaoDepositoIdentificado(saidaDTO.getCdBinCartaoDepositanteId());
        setViaCartaoDepositoIdentificado(saidaDTO.getCdViaCartaoDepositanteId());
        setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
        setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

    }

    // Detalhar Pagamentos Individuais - Ordem de Cr�dito
    /**
     * Preenche dados pagto ordem credito.
     */
    public void preencheDadosPagtoOrdemCredito() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        DetalharPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharPagtoOrdemCreditoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : Integer.parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoOrdemCredito(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : Long
            .parseLong(saidaDTO.getCdFavorecido()));
        setDsBancoOriginal(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaOriginal(saidaDTO.getAgenciaCreditoFormatada());
        setCdDigitoContaOriginal(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
            .getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil
            .formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCodigoPagador(saidaDTO.getCdPagador());
        setCodigoOrdemCredito(saidaDTO.getCdOrdemCreditoPagamento());
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

    }

    // Detalhar Manuten��o Pagamentos Individuais - Cr�dito em Conta (Alterado
    // para ultilizar
    // detalharPagtoCreditoConta)
    /**
     * Preenche dados man pagto credito conta.
     */
    public void preencheDadosManPagtoCreditoConta() { //tyla
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaIndividualDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoCreditoConta(entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaIndividualDTO.getDsTipoManutencao());

        // Cabe�alho
        setNrCnpjCpf(saidaIndividualDTO.getCdCpfCnpjCliente() == null || saidaIndividualDTO.getCdCpfCnpjCliente() == 0L ? ""
            : PgitUtil.formatCpfCnpj(saidaIndividualDTO.getCdCpfCnpjCliente().toString()));
        setDsRazaoSocial(saidaIndividualDTO.getNomeCliente());
        setDsEmpresa(saidaIndividualDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaIndividualDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaIndividualDTO.getDsContrato());
        setCdSituacaoContrato(saidaIndividualDTO.getCdSituacaoContrato());
        setDsBancoDebito(PgitUtil.formatBanco(saidaIndividualDTO.getCdBancoDebito(), saidaIndividualDTO
            .getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(saidaIndividualDTO.getCdAgenciaDebito(), saidaIndividualDTO
            .getCdDigAgenciaDebto(), saidaIndividualDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaIndividualDTO.getCdContaDebito(), saidaIndividualDTO
            .getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaIndividualDTO.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaIndividualDTO.getCdIndicadorModalidade() == 1 || saidaIndividualDTO.getCdIndicadorModalidade() == 3) {
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaIndividualDTO.getCdTipoInscricaoFavorecido(),
                saidaIndividualDTO.getNmInscriFavorecido()));
            setTipoFavorecido(saidaIndividualDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaIndividualDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaIndividualDTO.getCdFavorecido() == null
                || saidaIndividualDTO.getCdFavorecido().equals("") ? null : Long
                    .parseLong(saidaIndividualDTO.getCdFavorecido()));
            setDsBancoFavorecido(PgitUtil.formatBanco(saidaIndividualDTO.getCdBancoCredito(), saidaIndividualDTO
                .getDsBancoFavorecido(), false));
            setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaIndividualDTO.getCdAgenciaCredito(), saidaIndividualDTO
                .getCdDigAgenciaCredito(), saidaIndividualDTO.getDsAgenciaFavorecido(), false));
            setDsContaFavorecido(PgitUtil.formatConta(saidaIndividualDTO.getCdContaCredito(), saidaIndividualDTO
                .getCdDigContaCredito(), false));
            setDsTipoContaFavorecido(saidaIndividualDTO.getDsTipoContaFavorecido());
        } else {
            setNumeroInscricaoBeneficiario(saidaIndividualDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaIndividualDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaIndividualDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
            || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
            || !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
        }

        // Pagamento
        setDsTipoServico(saidaIndividualDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaIndividualDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaIndividualDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaIndividualDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaIndividualDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaIndividualDTO.getCdListaDebito());
        setDtAgendamento(saidaIndividualDTO.getDtAgendamento());
        // setDtPagamento(saidaIndividualDTO.getDtPagamento());
        setDtVencimento(saidaIndividualDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaIndividualDTO.getDsMoeda());
        setQtMoeda(saidaIndividualDTO.getQtMoeda());
        setVlrAgendamento(saidaIndividualDTO.getVlrAgendamento());
        setVlrEfetivacao(saidaIndividualDTO.getVlrEfetivacao());
        setCdSituacaoOperacaoPagamento(saidaIndividualDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaIndividualDTO.getDsMotivoSituacao());
        setDsPagamento(saidaIndividualDTO.getDsPagamento());
        setNrDocumento(saidaIndividualDTO.getNrDocumento());
        setTipoDocumento(saidaIndividualDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaIndividualDTO.getCdSerieDocumento());
        setVlDocumento(saidaIndividualDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaIndividualDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaIndividualDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaIndividualDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaIndividualDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaIndividualDTO.getCdBancoCredito(), saidaIndividualDTO
            .getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaIndividualDTO.getCdAgenciaCredito(), saidaIndividualDTO
            .getCdDigAgenciaCredito(), saidaIndividualDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(saidaIndividualDTO.getCdContaCredito(), saidaIndividualDTO
            .getCdDigContaCredito(), false));
        setTipoContaCredito(saidaIndividualDTO.getDsTipoContaFavorecido());
        setCdBancoDestino(saidaIndividualDTO.getBancoDestinoFormatado());
        setAgenciaDigitoDestino(saidaIndividualDTO.getAgenciaDestinoFormatada());
        setContaDigitoDestino(saidaIndividualDTO.getContaDestinoFormatada());
        setTipoContaDestino(saidaIndividualDTO.getDsTipoContaDestino());
        setVlDesconto(saidaIndividualDTO.getVlDesconto());
        setVlAbatimento(saidaIndividualDTO.getVlAbatimento());
        setVlMulta(saidaIndividualDTO.getVlMulta());
        setVlMora(saidaIndividualDTO.getVlMora());
        setVlDeducao(saidaIndividualDTO.getVlDeducao());
        setVlAcrescimo(saidaIndividualDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaIndividualDTO.getVlImpostoRenda());
        setVlIss(saidaIndividualDTO.getVlIss());
        setVlIof(saidaIndividualDTO.getVlIof());
        setVlInss(saidaIndividualDTO.getVlInss());
        setCdSituacaoTranferenciaAutomatica(saidaIndividualDTO.getCdSituacaoTranferenciaAutomatica());
        setDtDevolucaoEstorno(saidaIndividualDTO.getDtDevolucaoEstorno());
        setNumControleInternoLote(saidaIndividualDTO.getNumControleInternoLote());

        // trilhaAuditoria
        setDataHoraInclusao(saidaIndividualDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaIndividualDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaIndividualDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaIndividualDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaIndividualDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaIndividualDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaIndividualDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaIndividualDTO.getComplementoManutencaoFormatado());

        //Favorecido
        //Favorecidos
        setCdBancoOriginal(saidaIndividualDTO.getCdBancoOriginal());
        setDsBancoOriginal(saidaIndividualDTO.getCdBancoOriginal() + " - " + saidaIndividualDTO.getDsBancoOriginal());
        setCdAgenciaBancariaOriginal(saidaIndividualDTO.getCdAgenciaBancariaOriginal());
        setCdDigitoAgenciaOriginal(saidaIndividualDTO.getCdDigitoAgenciaOriginal());
        setDsAgenciaOriginal(saidaIndividualDTO.getCdAgenciaBancariaOriginal() + "-" + saidaIndividualDTO.getCdDigitoAgenciaOriginal() + " - " + saidaIndividualDTO.getDsAgenciaOriginal());
        setCdContaBancariaOriginal(saidaIndividualDTO.getCdContaBancariaOriginal());
        setCdDigitoContaOriginal(saidaIndividualDTO.getCdContaBancariaOriginal() + "-" + saidaIndividualDTO.getCdDigitoContaOriginal());
        setDsTipoContaOriginal(saidaIndividualDTO.getDsTipoContaOriginal());
                
        setTipoContaDestinoPagamento(saidaIndividualDTO.getDsTipoContaDestino());
        setCdBancoDestinoPagamento(saidaIndividualDTO.getCdBancoDestino().toString());
        setContaDigitoDestinoPagamento(saidaIndividualDTO.getContaDestinoFormatada());
        setDsISPB(saidaIndividualDTO.getCdIspbPagtoDestino() + " - " + saidaIndividualDTO.getDsBancoDestino());
        setCdContaPagamentoDet(saidaIndividualDTO.getContaPagtoDestino());
        
        if(saidaIndividualDTO.getCdIndicadorModalidade() == 3){
        	setExibeBancoCredito(true);
        	if(saidaIndividualDTO.getContaPagtoDestino().equals("0")){
        		setCdBancoDestinoPagamento(null);
        		setContaDigitoDestinoPagamento(null);
        		setTipoContaDestinoPagamento(null);
        		setDsISPB(null);
        		setCdContaPagamentoDet(null);
        	}else {
        		setCdBancoDestino(null);
        		setAgenciaDigitoDestino(null);
        		setContaDigitoDestino(null);
        		setTipoContaDestino(null);
        	}
        	
        }else{
        	setExibeBancoCredito(false);
        }
    }

    // Detalhar Manuten��o Pagamentos Individuais - D�bito em conta
    /**
     * Preenche dados man pagto debito conta.
     */
    public void preencheDadosManPagtoDebitoConta() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoDebitoContaEntradaDTO entradaDTO = new DetalharManPagtoDebitoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaDebitoContaDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoDebitoConta(entradaDTO);

        // Cabe�alho - Cliente
        setNrCnpjCpf(saidaDebitoContaDTO.getCdCpfCnpjCliente() == null
            || saidaDebitoContaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
                .formatCpfCnpj(saidaDebitoContaDTO.getCdCpfCnpjCliente().toString()));
        setDsRazaoSocial(saidaDebitoContaDTO.getNomeCliente());

        // Sempre Carregar Conta de D�bito
        setDsBancoDebito(PgitUtil.formatBanco(saidaDebitoContaDTO.getCdBancoDebito(), saidaDebitoContaDTO
            .getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(saidaDebitoContaDTO.getCdAgenciaDebito(), saidaDebitoContaDTO
            .getCdDigAgenciaDebto(), saidaDebitoContaDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaDebitoContaDTO.getCdContaDebito(), saidaDebitoContaDTO
            .getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaDebitoContaDTO.getDsTipoContaDebito());

        setDsEmpresa(saidaDebitoContaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDebitoContaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDebitoContaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDebitoContaDTO.getCdSituacaoContrato());

        // Manutencao
        setDsTipoManutencao(saidaDebitoContaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDebitoContaDTO.getCdTipoInscricaoFavorecido(),
            saidaDebitoContaDTO.getNmInscriFavorecido()));
        setTipoFavorecido(saidaDebitoContaDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaDebitoContaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDebitoContaDTO.getCdFavorecido() == null
            || saidaDebitoContaDTO.getCdFavorecido().equals("") ? null : Long.parseLong(saidaDebitoContaDTO
                .getCdFavorecido()));
        setDsBancoFavorecido(PgitUtil.formatBanco(saidaDebitoContaDTO.getCdBancoCredito(), saidaDebitoContaDTO
            .getDsBancoFavorecido(), false));
        setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDebitoContaDTO.getCdAgenciaCredito(), saidaDebitoContaDTO
            .getCdDigAgenciaCredito(), saidaDebitoContaDTO.getDsAgenciaFavorecido(), false));
        setDsContaFavorecido(PgitUtil.formatConta(saidaDebitoContaDTO.getCdContaCredito(), saidaDebitoContaDTO
            .getCdDigContaCredito(), false));
        setDsTipoContaFavorecido(saidaDebitoContaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDebitoContaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDebitoContaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDebitoContaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDebitoContaDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaDebitoContaDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaDebitoContaDTO.getCdListaDebito());
        setDtAgendamento(saidaDebitoContaDTO.getDtAgendamento());
        setDtPagamento(saidaDebitoContaDTO.getDtPagamento());
        setDtVencimento(saidaDebitoContaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDebitoContaDTO.getDsMoeda());
        setQtMoeda(saidaDebitoContaDTO.getQtMoeda());
        setVlrAgendamento(saidaDebitoContaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDebitoContaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDebitoContaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDebitoContaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDebitoContaDTO.getDsPagamento());
        setNrDocumento(saidaDebitoContaDTO.getNrDocumento());
        setTipoDocumento(saidaDebitoContaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDebitoContaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDebitoContaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDebitoContaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDebitoContaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDebitoContaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDebitoContaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDebitoContaDTO.getCdBancoCredito(), saidaDebitoContaDTO
            .getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDebitoContaDTO.getCdAgenciaCredito(), saidaDebitoContaDTO
            .getCdDigAgenciaCredito(), saidaDebitoContaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(saidaDebitoContaDTO.getCdContaCredito(), saidaDebitoContaDTO
            .getCdDigContaCredito(), false));
        setTipoContaCredito(saidaDebitoContaDTO.getDsTipoContaFavorecido());
        setVlDesconto(saidaDebitoContaDTO.getVlDesconto());
        setVlAbatimento(saidaDebitoContaDTO.getVlAbatimento());
        setVlMulta(saidaDebitoContaDTO.getVlMulta());
        setVlMora(saidaDebitoContaDTO.getVlMora());
        setVlDeducao(saidaDebitoContaDTO.getVlDeducao());
        setVlAcrescimo(saidaDebitoContaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDebitoContaDTO.getVlImpostoRenda());
        setVlIss(saidaDebitoContaDTO.getVlIss());
        setVlIof(saidaDebitoContaDTO.getVlIof());
        setVlInss(saidaDebitoContaDTO.getVlInss());
        setDtDevolucaoEstorno(FormatarData.formataDiaMesAno(saidaDebitoContaDTO.getDtDevolucaoEstorno()));
        setDtFloatingPagamento(saidaDebitoContaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDebitoContaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDebitoContaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDebitoContaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDebitoContaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDebitoContaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDebitoContaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDebitoContaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDebitoContaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDebitoContaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDebitoContaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDebitoContaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDebitoContaDTO.getComplementoManutencaoFormatado());

    }

    // Detalhar Manuten��o Pagamentos Individuais - DOC
    /**
     * Preenche dados man pagto doc.
     */
    public void preencheDadosManPagtoDOC() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoDOCEntradaDTO entradaDTO = new DetalharManPagtoDOCEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaPagtoDocDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoDOC(entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaPagtoDocDTO.getDsTipoManutencao());

        // Cabe�alho
        setNrCnpjCpf(saidaPagtoDocDTO.getCdCpfCnpjCliente() == null || saidaPagtoDocDTO.getCdCpfCnpjCliente() == 0L ? ""
            : PgitUtil.formatCpfCnpj(saidaPagtoDocDTO.getCdCpfCnpjCliente().toString()));
        setDsRazaoSocial(saidaPagtoDocDTO.getNomeCliente());
        setDsEmpresa(saidaPagtoDocDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaPagtoDocDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaPagtoDocDTO.getDsContrato());
        setCdSituacaoContrato(saidaPagtoDocDTO.getCdSituacaoContrato());
        setDsBancoDebito(saidaPagtoDocDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaPagtoDocDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaPagtoDocDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaPagtoDocDTO.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaPagtoDocDTO.getCdIndicadorModalidadePgit() == 1 || saidaPagtoDocDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaPagtoDocDTO.getCdTipoInscricaoFavorecido(),
                saidaPagtoDocDTO.getNmInscriFavorecido()));
            setTipoFavorecido(saidaPagtoDocDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaPagtoDocDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaPagtoDocDTO.getCdFavorecido() == null || saidaPagtoDocDTO.getCdFavorecido().equals("") ? null
                : Long.parseLong(saidaPagtoDocDTO.getCdFavorecido()));
            setDsBancoFavorecido(saidaPagtoDocDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaPagtoDocDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaPagtoDocDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaPagtoDocDTO.getDsTipoContaFavorecido());
        } else {
            setNumeroInscricaoBeneficiario(saidaPagtoDocDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaPagtoDocDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaPagtoDocDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
            || !PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals("")
            || !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
        }

        // Pagamento
        setDsTipoServico(saidaPagtoDocDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaPagtoDocDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaPagtoDocDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaPagtoDocDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaPagtoDocDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaPagtoDocDTO.getCdListaDebito());
        setDtAgendamento(saidaPagtoDocDTO.getDtAgendamento());
        setDtPagamento(saidaPagtoDocDTO.getDtPagamento());
        setDtVencimento(saidaPagtoDocDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaPagtoDocDTO.getDsMoeda());
        setQtMoeda(saidaPagtoDocDTO.getQtMoeda());
        setVlrAgendamento(saidaPagtoDocDTO.getVlAgendado());
        setVlrEfetivacao(saidaPagtoDocDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaPagtoDocDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaPagtoDocDTO.getDsMotivoSituacao());
        setDsPagamento(saidaPagtoDocDTO.getDsPagamento());
        setNrDocumento(saidaPagtoDocDTO.getNrDocumento());
        setTipoDocumento(saidaPagtoDocDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaPagtoDocDTO.getCdSerieDocumento());
        setVlDocumento(saidaPagtoDocDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaPagtoDocDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaPagtoDocDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaPagtoDocDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaPagtoDocDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaPagtoDocDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaPagtoDocDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaPagtoDocDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaPagtoDocDTO.getDsTipoContaFavorecido());
        setCamaraCentralizadora(saidaPagtoDocDTO.getCdCamaraCentral());
        setDtDevolucaoEstorno(saidaPagtoDocDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaPagtoDocDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaPagtoDocDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaPagtoDocDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaPagtoDocDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaPagtoDocDTO.getDsSituacaoDcom());

        setFinalidadeDoc(saidaPagtoDocDTO.getDsFinalidadeDocTed());
        setIdentificacaoTransferenciaDoc(saidaPagtoDocDTO.getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(saidaPagtoDocDTO.getDsidentificacaoTitularPagto());

        setVlDesconto(saidaPagtoDocDTO.getVlDesconto());
        setVlAbatimento(saidaPagtoDocDTO.getVlAbatimento());
        setVlMulta(saidaPagtoDocDTO.getVlMulta());
        setVlMora(saidaPagtoDocDTO.getVlMora());
        setVlDeducao(saidaPagtoDocDTO.getVlDeducao());
        setVlAcrescimo(saidaPagtoDocDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaPagtoDocDTO.getVlImpostoRenda());
        setVlIss(saidaPagtoDocDTO.getVlIss());
        setVlIof(saidaPagtoDocDTO.getVlIof());
        setVlInss(saidaPagtoDocDTO.getVlInss());

        // trilhaAuditoria
        setDataHoraInclusao(saidaPagtoDocDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaPagtoDocDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaPagtoDocDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaPagtoDocDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaPagtoDocDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaPagtoDocDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaPagtoDocDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaPagtoDocDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - TED
    /**
     * Preenche dados man pagto ted.
     */
    public void preencheDadosManPagtoTED() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoTEDEntradaDTO entradaDTO = new DetalharManPagtoTEDEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());

        entradaDTO.setCdAgenPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaPagtoTedDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoTED(entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaPagtoTedDTO.getDsTipoManutencao());

        // CLIENTE
        setNrCnpjCpf(saidaPagtoTedDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaPagtoTedDTO.getNomeCliente());
        setDsEmpresa(saidaPagtoTedDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaPagtoTedDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaPagtoTedDTO.getDsContrato());
        setCdSituacaoContrato(saidaPagtoTedDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaPagtoTedDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaPagtoTedDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaPagtoTedDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaPagtoTedDTO.getDsTipoContaDebito());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaPagtoTedDTO.getCdIndicadorModalidadePgit() == 1 || saidaPagtoTedDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(saidaPagtoTedDTO.getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(saidaPagtoTedDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaPagtoTedDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaPagtoTedDTO.getCdFavorecido() == null || saidaPagtoTedDTO.getCdFavorecido().equals("") ? null
                : Long.parseLong(saidaPagtoTedDTO.getCdFavorecido()));
            setDsBancoFavorecido(saidaPagtoTedDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaPagtoTedDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaPagtoTedDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaPagtoTedDTO.getDsTipoContaFavorecido());
        } else {
            setNumeroInscricaoBeneficiario(saidaPagtoTedDTO.getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(saidaPagtoTedDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaPagtoTedDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
            || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
            || !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
        }

        // Pagamento
        setCdIndentificadorJudicial(saidaPagtoTedDTO.getCdIndentificadorJudicial());
        setDsTipoServico(saidaPagtoTedDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaPagtoTedDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaPagtoTedDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaPagtoTedDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaPagtoTedDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaPagtoTedDTO.getCdListaDebito());
        setDtAgendamento(saidaPagtoTedDTO.getDtAgendamento());
        setDtPagamento(saidaPagtoTedDTO.getDtPagamento());
        setDtVencimento(saidaPagtoTedDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaPagtoTedDTO.getDsMoeda());
        setQtMoeda(saidaPagtoTedDTO.getQtMoeda());
        setVlrAgendamento(saidaPagtoTedDTO.getVlAgendado());
        setVlrEfetivacao(saidaPagtoTedDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaPagtoTedDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaPagtoTedDTO.getDsMotivoSituacao());
        setDsPagamento(saidaPagtoTedDTO.getDsPagamento());
        setNrDocumento(saidaPagtoTedDTO.getNrDocumento());
        setTipoDocumento(saidaPagtoTedDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaPagtoTedDTO.getCdSerieDocumento());
        setVlDocumento(saidaPagtoTedDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaPagtoTedDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaPagtoTedDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaPagtoTedDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaPagtoTedDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(String.valueOf(saidaPagtoTedDTO.getCdBancoCredito()));
        setDsBancoCredito(saidaPagtoTedDTO.getDsBancoCredito());
        setCdIspbPagamento(saidaPagtoTedDTO.getCdIspbPagamento());
        setDsNomeReclamante(saidaPagtoTedDTO.getDsNomeReclamante());
        setNrProcessoDepositoJudicial(saidaPagtoTedDTO.getNrProcessoDepositoJudicial());
        setNrPisPasep(NumberUtils.formatarNumeroPisPasep(saidaPagtoTedDTO.getNrPisPasep()));
        setCdFinalidadeTedDepositoJudicial(saidaPagtoTedDTO.isCdFinalidadeTedDepositoJudicial());
        setAgenciaDigitoCredito(saidaPagtoTedDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaPagtoTedDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaPagtoTedDTO.getDsTipoContaFavorecido());
        setCamaraCentralizadora(saidaPagtoTedDTO.getCdCamaraCentral());
        setDtFloatingPagamento(saidaPagtoTedDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaPagtoTedDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaPagtoTedDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaPagtoTedDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaPagtoTedDTO.getDsSituacaoDcom());

        setFinalidadeTed(saidaPagtoTedDTO.getFinalidadeTedFormatada());
        setIdentificacaoTransferenciaTed(saidaPagtoTedDTO.getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(saidaPagtoTedDTO.getDsidentificacaoTitularPagto());

        setVlDesconto(saidaPagtoTedDTO.getVlDesconto());
        setVlAbatimento(saidaPagtoTedDTO.getVlAbatimento());
        setVlMulta(saidaPagtoTedDTO.getVlMulta());
        setVlMora(saidaPagtoTedDTO.getVlMora());
        setVlDeducao(saidaPagtoTedDTO.getVlDeducao());
        setVlAcrescimo(saidaPagtoTedDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaPagtoTedDTO.getVlImpostoRenda());
        setVlIss(saidaPagtoTedDTO.getVlIss());
        setVlIof(saidaPagtoTedDTO.getVlIof());
        setVlInss(saidaPagtoTedDTO.getVlInss());
        setDtDevolucaoEstorno(saidaPagtoTedDTO.getDtDevolucaoEstorno());

        // trilhaAuditoria
        setDataHoraInclusao(saidaPagtoTedDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaPagtoTedDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaPagtoTedDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaPagtoTedDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaPagtoTedDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaPagtoTedDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaPagtoTedDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaPagtoTedDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - Ordem de Pagamento
    /**
     * Preenche dados man pagto ordem pagto.
     */
    public void preencheDadosManPagtoOrdemPagto() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharManPagtoOrdemPagtoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaOrdemPgtoDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoOrdemPagto(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaOrdemPgtoDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaOrdemPgtoDTO.getNomeCliente());
        setDsEmpresa(saidaOrdemPgtoDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaOrdemPgtoDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaOrdemPgtoDTO.getDsContrato());
        setCdSituacaoContrato(saidaOrdemPgtoDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaOrdemPgtoDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaOrdemPgtoDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaOrdemPgtoDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaOrdemPgtoDTO.getDsTipoContaDebito());

        // Manuten��o
        setDsTipoManutencao(saidaOrdemPgtoDTO.getDsTipoManutencao());

        limparFavorecidoBeneficiario();
        exibeBeneficiario = false;

        if (saidaOrdemPgtoDTO.getCdIndicadorModalidadePgit() == 1 || saidaOrdemPgtoDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(saidaOrdemPgtoDTO.getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(saidaOrdemPgtoDTO.getCdTipoInscricaoFavorecido());
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
            setDsFavorecido(saidaOrdemPgtoDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaOrdemPgtoDTO.getCdFavorecido() == null
                || saidaOrdemPgtoDTO.getCdFavorecido().equals("") ? null : Long.parseLong(saidaOrdemPgtoDTO
                    .getCdFavorecido()));
            setDsBancoFavorecido(saidaOrdemPgtoDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaOrdemPgtoDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaOrdemPgtoDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaOrdemPgtoDTO.getDsTipoContaFavorecido());
            setExibeDcom(saidaOrdemPgtoDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
        } else {
            setNumeroInscricaoBeneficiario(saidaOrdemPgtoDTO.getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(saidaOrdemPgtoDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaOrdemPgtoDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
            || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
            || !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(saidaOrdemPgtoDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaOrdemPgtoDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaOrdemPgtoDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaOrdemPgtoDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaOrdemPgtoDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaOrdemPgtoDTO.getCdListaDebito());
        setDtAgendamento(saidaOrdemPgtoDTO.getDtAgendamento());
        setDtPagamento(saidaOrdemPgtoDTO.getDtPagamento());
        setDtVencimento(saidaOrdemPgtoDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaOrdemPgtoDTO.getDsMoeda());
        setQtMoeda(saidaOrdemPgtoDTO.getQtMoeda());
        setVlrAgendamento(saidaOrdemPgtoDTO.getVlAgendado());
        setVlrEfetivacao(saidaOrdemPgtoDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaOrdemPgtoDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaOrdemPgtoDTO.getDsMotivoSituacao());
        setDsPagamento(saidaOrdemPgtoDTO.getDsPagamento());
        setNrDocumento(saidaOrdemPgtoDTO.getNrDocumento());
        setTipoDocumento(saidaOrdemPgtoDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaOrdemPgtoDTO.getCdSerieDocumento());
        setVlDocumento(saidaOrdemPgtoDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaOrdemPgtoDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaOrdemPgtoDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaOrdemPgtoDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaOrdemPgtoDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaOrdemPgtoDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaOrdemPgtoDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaOrdemPgtoDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaOrdemPgtoDTO.getDsTipoContaFavorecido());
        setNumeroOp(saidaOrdemPgtoDTO.getNrOperacao());
        setDataExpiracaoCredito(saidaOrdemPgtoDTO.getDtExpedicaoCredito());
        setInstrucaoPagamento(saidaOrdemPgtoDTO.getCdInsPagamento());
        setNumeroCheque(saidaOrdemPgtoDTO.getNrCheque());
        setSerieCheque(saidaOrdemPgtoDTO.getNrSerieCheque());
        setIndicadorAssociadoUnicaAgencia(saidaOrdemPgtoDTO.getDsUnidadeAgencia());
        setDtDevolucaoEstorno(saidaOrdemPgtoDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaOrdemPgtoDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaOrdemPgtoDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaOrdemPgtoDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaOrdemPgtoDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaOrdemPgtoDTO.getDsSituacaoDcom());

        setIdentificadorTipoRetirada(saidaOrdemPgtoDTO.getDsIdentificacaoTipoRetirada());
        setIdentificadorRetirada(saidaOrdemPgtoDTO.getDsIdentificacaoRetirada());

        setDataRetirada(saidaOrdemPgtoDTO.getDtRetirada());
        setVlImpostoMovFinanceira(saidaOrdemPgtoDTO.getVlImpostoMovimentacaoFinanceira());
        setVlDesconto(saidaOrdemPgtoDTO.getVlDescontoCredito());
        setVlAbatimento(saidaOrdemPgtoDTO.getVlAbatidoCredito());
        setVlMulta(saidaOrdemPgtoDTO.getVlMultaCredito());
        setVlMora(saidaOrdemPgtoDTO.getVlMoraJuros());
        setVlDeducao(saidaOrdemPgtoDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaOrdemPgtoDTO.getVlOutroAcrescimo());
        setVlImpostoRenda(saidaOrdemPgtoDTO.getVlIrCredito());
        setVlIss(saidaOrdemPgtoDTO.getVlIssCredito());
        setVlIof(saidaOrdemPgtoDTO.getVlIofCredito());
        setVlInss(saidaOrdemPgtoDTO.getVlInssCredito());
        setDtDevolucaoEstorno(saidaOrdemPgtoDTO.getDtDevolucaoEstorno());

        // trilhaAuditoria
        setDataHoraInclusao(saidaOrdemPgtoDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaOrdemPgtoDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaOrdemPgtoDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaOrdemPgtoDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaOrdemPgtoDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaOrdemPgtoDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaOrdemPgtoDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaOrdemPgtoDTO.getComplementoManutencaoFormatado());

    }

    /**
     * Limpar favorecido manutencao.
     */
    public void limparFavorecidoManutencao() {
        setNumeroInscricaoFavorecido("");
        setDsFavorecido("");
        setTipoFavorecido(0);
        setDescTipoFavorecido("");
        setCdFavorecido(0L);
        setSituacaoPagamento("");
        setMotivoPagamento("");
        setInscricaoFavorecido("");
        setCdTipoInscricaoFavorecido("");
        setTipoContaFavorecido("");
        setBancoFavorecido("");
        setAgenciaFavorecido("");
        setContaFavorecido("");
    }

    // Detalhar Manuten��o Pagamentos Individuais - Titulos Bradesco
    /**
     * Preenche dados man pagto titulo bradesco.
     */
    public void preencheDadosManPagtoTituloBradesco() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharManPagtoTituloBradescoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaPagtoTituloBardescoDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoTituloBradesco(
            entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaPagtoTituloBardescoDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaPagtoTituloBardescoDTO.getNomeCliente());
        setDsEmpresa(saidaPagtoTituloBardescoDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaPagtoTituloBardescoDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaPagtoTituloBardescoDTO.getDsContrato());
        setCdSituacaoContrato(saidaPagtoTituloBardescoDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaPagtoTituloBardescoDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaPagtoTituloBardescoDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaPagtoTituloBardescoDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaPagtoTituloBardescoDTO.getDsTipoContaDebito());

        // Manunte��o
        setDsTipoManutencao(saidaPagtoTituloBardescoDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaPagtoTituloBardescoDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaPagtoTituloBardescoDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaPagtoTituloBardescoDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaPagtoTituloBardescoDTO.getCdFavorecido() == null
            || saidaPagtoTituloBardescoDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaPagtoTituloBardescoDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaPagtoTituloBardescoDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaPagtoTituloBardescoDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaPagtoTituloBardescoDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaPagtoTituloBardescoDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDtLimiteDescontoPagamento(saidaPagtoTituloBardescoDTO.getDtLimiteDescontoPagamento());
        setCarteira(saidaPagtoTituloBardescoDTO.getCarteira());
        setDsTipoServico(saidaPagtoTituloBardescoDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaPagtoTituloBardescoDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaPagtoTituloBardescoDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaPagtoTituloBardescoDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaPagtoTituloBardescoDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaPagtoTituloBardescoDTO.getCdListaDebito());
        setDtAgendamento(saidaPagtoTituloBardescoDTO.getDtAgendamento());
        setDtPagamento(saidaPagtoTituloBardescoDTO.getDtPagamento());
        setDtVencimento(saidaPagtoTituloBardescoDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaPagtoTituloBardescoDTO.getDsMoeda());
        setQtMoeda(saidaPagtoTituloBardescoDTO.getQtMoeda());
        setVlrAgendamento(saidaPagtoTituloBardescoDTO.getVlAgendado());
        setVlrEfetivacao(saidaPagtoTituloBardescoDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaPagtoTituloBardescoDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaPagtoTituloBardescoDTO.getDsMotivoSituacao());
        setDsPagamento(saidaPagtoTituloBardescoDTO.getDsPagamento());
        setNrDocumento(saidaPagtoTituloBardescoDTO.getNrDocumento());
        setTipoDocumento(saidaPagtoTituloBardescoDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaPagtoTituloBardescoDTO.getCdSerieDocumento());
        setVlDocumento(saidaPagtoTituloBardescoDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaPagtoTituloBardescoDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaPagtoTituloBardescoDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaPagtoTituloBardescoDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaPagtoTituloBardescoDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaPagtoTituloBardescoDTO.getCdBarras());
        setVlDesconto(saidaPagtoTituloBardescoDTO.getVlDesconto());
        setVlAbatimento(saidaPagtoTituloBardescoDTO.getVlAbatimento());
        setVlMulta(saidaPagtoTituloBardescoDTO.getVlMulta());
        setVlMora(saidaPagtoTituloBardescoDTO.getVlMoraJuros());
        setVlDeducao(saidaPagtoTituloBardescoDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaPagtoTituloBardescoDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaPagtoTituloBardescoDTO.getNossoNumero());
        setDsOrigemPagamento(saidaPagtoTituloBardescoDTO.getDsOrigemPagamento());
        setDtDevolucaoEstorno(saidaPagtoTituloBardescoDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaPagtoTituloBardescoDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaPagtoTituloBardescoDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaPagtoTituloBardescoDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaPagtoTituloBardescoDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaPagtoTituloBardescoDTO.getDsSituacaoDcom());
        setVlBonfcao(saidaPagtoTituloBardescoDTO.getVlBonfcao());
        setDtLimitePagamento(saidaPagtoTituloBardescoDTO.getDtLimitePagamento());
        setVlBonificacao(saidaPagtoTituloBardescoDTO.getValorBonificacao());

        // trilhaAuditoria
        setDataHoraInclusao(saidaPagtoTituloBardescoDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaPagtoTituloBardescoDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaPagtoTituloBardescoDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaPagtoTituloBardescoDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaPagtoTituloBardescoDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaPagtoTituloBardescoDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaPagtoTituloBardescoDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaPagtoTituloBardescoDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - Titulos Outros Bancos
    /**
     * Preenche dados man pagto titulo outros bancos.
     */
    public void preencheDadosManPagtoTituloOutrosBancos() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharManPagtoTituloOutrosBancosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaTituloOutrosBancosDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoTituloOutrosBancos(
            entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaTituloOutrosBancosDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaTituloOutrosBancosDTO.getNomeCliente());
        setDsEmpresa(saidaTituloOutrosBancosDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaTituloOutrosBancosDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaTituloOutrosBancosDTO.getDsContrato());
        setCdSituacaoContrato(saidaTituloOutrosBancosDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaTituloOutrosBancosDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaTituloOutrosBancosDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaTituloOutrosBancosDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaTituloOutrosBancosDTO.getDsTipoContaDebito());

        // Maunten��o
        setDsTipoManutencao(saidaTituloOutrosBancosDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaTituloOutrosBancosDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaTituloOutrosBancosDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaTituloOutrosBancosDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaTituloOutrosBancosDTO.getCdFavorecido() == null
            || saidaTituloOutrosBancosDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaTituloOutrosBancosDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaTituloOutrosBancosDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaTituloOutrosBancosDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaTituloOutrosBancosDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaTituloOutrosBancosDTO.getDsTipoContaFavorecido());

        // Sacador/Avalista
        
        //setDsSacadorAvalista(dsSacadorAvalista);
        //setNumeroInscricaoSacadorAvalista(saidaTituloOutrosBancosDTO.getNumeroInscricaoSacadorAvalista);
        //setDsTipoSacadorAvalista(dsTipoSacadorAvalista);
        
        
        // Pagamento
        setCarteira(saidaTituloOutrosBancosDTO.getCarteira());
        setDtLimiteDescontoPagamento(saidaTituloOutrosBancosDTO.getDtLimiteDescontoPagamento());
        setDsTipoServico(saidaTituloOutrosBancosDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaTituloOutrosBancosDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaTituloOutrosBancosDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaTituloOutrosBancosDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaTituloOutrosBancosDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaTituloOutrosBancosDTO.getCdListaDebito());
        setDtAgendamento(saidaTituloOutrosBancosDTO.getDtAgendamento());
        setDtPagamento(saidaTituloOutrosBancosDTO.getDtPagamento());
        setDtVencimento(saidaTituloOutrosBancosDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaTituloOutrosBancosDTO.getDsMoeda());
        setQtMoeda(saidaTituloOutrosBancosDTO.getQtMoeda());
        setVlrAgendamento(saidaTituloOutrosBancosDTO.getVlAgendado());
        setVlrEfetivacao(saidaTituloOutrosBancosDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaTituloOutrosBancosDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaTituloOutrosBancosDTO.getDsMotivoSituacao());
        setDsPagamento(saidaTituloOutrosBancosDTO.getDsPagamento());
        setNrDocumento(saidaTituloOutrosBancosDTO.getNrDocumento());
        setTipoDocumento(saidaTituloOutrosBancosDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaTituloOutrosBancosDTO.getCdSerieDocumento());
        setVlDocumento(saidaTituloOutrosBancosDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaTituloOutrosBancosDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaTituloOutrosBancosDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaTituloOutrosBancosDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaTituloOutrosBancosDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaTituloOutrosBancosDTO.getCdBarras());
        setVlDesconto(saidaTituloOutrosBancosDTO.getVlDesconto());
        setVlAbatimento(saidaTituloOutrosBancosDTO.getVlAbatimento());
        setVlMulta(saidaTituloOutrosBancosDTO.getVlMulta());
        setVlMora(saidaTituloOutrosBancosDTO.getVlMoraJuros());
        setVlDeducao(saidaTituloOutrosBancosDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaTituloOutrosBancosDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaTituloOutrosBancosDTO.getNossoNumero());
        setDsOrigemPagamento(saidaTituloOutrosBancosDTO.getDsOrigemPagamento());
        setDtDevolucaoEstorno(saidaTituloOutrosBancosDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaTituloOutrosBancosDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaTituloOutrosBancosDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaTituloOutrosBancosDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaTituloOutrosBancosDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaTituloOutrosBancosDTO.getDsSituacaoDcom());
        setVlBonfcao(saidaTituloOutrosBancosDTO.getVlBonfcao());
        setDtLimitePagamento(saidaTituloOutrosBancosDTO.getDtLimitePagamento());
        setVlBonificacao(saidaTituloOutrosBancosDTO.getVlBonfcao());

        // trilhaAuditoria
        setDataHoraInclusao(saidaTituloOutrosBancosDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaTituloOutrosBancosDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaTituloOutrosBancosDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaTituloOutrosBancosDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaTituloOutrosBancosDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaTituloOutrosBancosDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaTituloOutrosBancosDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaTituloOutrosBancosDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - DARF
    /**
     * Preenche dados man pagto darf.
     */
    public void preencheDadosManPagtoDARF() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoDARFEntradaDTO entradaDTO = new DetalharManPagtoDARFEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaPagtoDarfDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoDARF(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaPagtoDarfDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaPagtoDarfDTO.getNomeCliente());
        setDsEmpresa(saidaPagtoDarfDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaPagtoDarfDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaPagtoDarfDTO.getDsContrato());
        setCdSituacaoContrato(saidaPagtoDarfDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaPagtoDarfDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaPagtoDarfDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaPagtoDarfDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaPagtoDarfDTO.getDsTipoContaDebito());

        // Maunten��o
        setDsTipoManutencao(saidaPagtoDarfDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaPagtoDarfDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaPagtoDarfDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaPagtoDarfDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaPagtoDarfDTO.getCdFavorecido() == null || saidaPagtoDarfDTO.getCdFavorecido().equals("") ? null
            : Long.parseLong(saidaPagtoDarfDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaPagtoDarfDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaPagtoDarfDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaPagtoDarfDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaPagtoDarfDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaPagtoDarfDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaPagtoDarfDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaPagtoDarfDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaPagtoDarfDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaPagtoDarfDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaPagtoDarfDTO.getCdListaDebito());
        setDtAgendamento(saidaPagtoDarfDTO.getDtAgendamento());
        setDtPagamento(saidaPagtoDarfDTO.getDtPagamento());
        setDtVencimento(saidaPagtoDarfDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaPagtoDarfDTO.getDsMoeda());
        setQtMoeda(saidaPagtoDarfDTO.getQtMoeda());
        setVlrAgendamento(saidaPagtoDarfDTO.getVlAgendado());
        setVlrEfetivacao(saidaPagtoDarfDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaPagtoDarfDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaPagtoDarfDTO.getDsMotivoSituacao());
        setDsPagamento(saidaPagtoDarfDTO.getDsPagamento());
        setNrDocumento(saidaPagtoDarfDTO.getNrDocumento());
        setTipoDocumento(saidaPagtoDarfDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaPagtoDarfDTO.getCdSerieDocumento());
        setVlDocumento(saidaPagtoDarfDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaPagtoDarfDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaPagtoDarfDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaPagtoDarfDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaPagtoDarfDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaPagtoDarfDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaPagtoDarfDTO.getCdTelefone());
        setInscricaoEstadualContribuinte(saidaPagtoDarfDTO.getCdInscricaoEstadual());
        setAgenteArrecadador(saidaPagtoDarfDTO.getCdAgenteArrecad());
        setCodigoReceita(saidaPagtoDarfDTO.getCdReceitaTributo());
        setNumeroReferencia(saidaPagtoDarfDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaPagtoDarfDTO.getNrCotaParcela());
        setPercentualReceita(saidaPagtoDarfDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaPagtoDarfDTO.getDsPeriodoApuracao());
        setAnoExercicio(saidaPagtoDarfDTO.getCdDanoExercicio());
        setDataReferencia(saidaPagtoDarfDTO.getDtReferenciaTributo());
        setVlReceita(saidaPagtoDarfDTO.getVlReceita());
        setVlPrincipal(saidaPagtoDarfDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaPagtoDarfDTO.getVlAtualMonet());
        setVlAbatimento(saidaPagtoDarfDTO.getVlAbatimentoCredito());
        setVlMulta(saidaPagtoDarfDTO.getVlMultaCredito());
        setVlMora(saidaPagtoDarfDTO.getVlMoraJuros());
        setVlTotal(saidaPagtoDarfDTO.getVlTotalTributo());
        setDtDevolucaoEstorno(saidaPagtoDarfDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaPagtoDarfDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaPagtoDarfDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaPagtoDarfDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaPagtoDarfDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaPagtoDarfDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaPagtoDarfDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaPagtoDarfDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaPagtoDarfDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaPagtoDarfDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaPagtoDarfDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaPagtoDarfDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - GARE
    /**
     * Preenche dados man pagto gare.
     */
    public void preencheDadosManPagtoGARE() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoGAREEntradaDTO entradaDTO = new DetalharManPagtoGAREEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaPagtoGareDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoGARE(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaPagtoGareDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaPagtoGareDTO.getNomeCliente());
        setDsEmpresa(saidaPagtoGareDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaPagtoGareDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaPagtoGareDTO.getDsContrato());
        setCdSituacaoContrato(saidaPagtoGareDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaPagtoGareDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaPagtoGareDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaPagtoGareDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaPagtoGareDTO.getDsTipoContaDebito());

        // Manuten��o
        setDsTipoManutencao(saidaPagtoGareDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaPagtoGareDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaPagtoGareDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaPagtoGareDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaPagtoGareDTO.getCdFavorecido() == null || saidaPagtoGareDTO.getCdFavorecido().equals("") ? null
            : Long.parseLong(saidaPagtoGareDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaPagtoGareDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaPagtoGareDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaPagtoGareDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaPagtoGareDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaPagtoGareDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaPagtoGareDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaPagtoGareDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaPagtoGareDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaPagtoGareDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaPagtoGareDTO.getCdListaDebito());
        setDtAgendamento(saidaPagtoGareDTO.getDtAgendamento());
        setDtPagamentoGare(saidaPagtoGareDTO.getDtPagamento());
        setDtVencimento(saidaPagtoGareDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaPagtoGareDTO.getDsMoeda());
        setQtMoeda(saidaPagtoGareDTO.getQtMoeda());
        setVlrAgendamento(saidaPagtoGareDTO.getVlAgendado());
        setVlrEfetivacao(saidaPagtoGareDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaPagtoGareDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaPagtoGareDTO.getDsMotivoSituacao());
        setDsPagamento(saidaPagtoGareDTO.getDsPagamento());
        setNrDocumento(saidaPagtoGareDTO.getNrDocumento());
        setTipoDocumento(saidaPagtoGareDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaPagtoGareDTO.getCdSerieDocumento());
        setVlDocumento(saidaPagtoGareDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaPagtoGareDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaPagtoGareDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaPagtoGareDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaPagtoGareDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaPagtoGareDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaPagtoGareDTO.getNrTelefoneContribuinte());
        setInscricaoEstadualContribuinte(saidaPagtoGareDTO.getCdInscricaoEstadualContribuinte());
        setCodigoReceita(saidaPagtoGareDTO.getCdReceita());
        setNumeroReferencia(saidaPagtoGareDTO.getNrReferencia());
        setNumeroCotaParcela(saidaPagtoGareDTO.getNrCota());
        setPercentualReceita(saidaPagtoGareDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaPagtoGareDTO.getDsApuracaoTributo());
        setDataReferencia(saidaPagtoGareDTO.getDtReferencia());
        setCodigoCnae(saidaPagtoGareDTO.getCdCnae());
        setPlacaVeiculo(saidaPagtoGareDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaPagtoGareDTO.getNrParcela());
        setCodigoOrgaoFavorecido(saidaPagtoGareDTO.getCdOrFavorecido());
        setNomeOrgaoFavorecido(saidaPagtoGareDTO.getDsOrFavorecido());
        setCodigoUfFavorecida(saidaPagtoGareDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaPagtoGareDTO.getDsUfFavorecido());
        setVlReceita(saidaPagtoGareDTO.getVlReceita());
        setVlPrincipal(saidaPagtoGareDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaPagtoGareDTO.getVlAtual());
        setVlAcrescimoFinanceiro(saidaPagtoGareDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaPagtoGareDTO.getVlHonorario());
        setVlAbatimento(saidaPagtoGareDTO.getVlAbatimento());
        setVlMulta(saidaPagtoGareDTO.getVlMulta());
        setVlMora(saidaPagtoGareDTO.getVlMora());
        setVlTotal(saidaPagtoGareDTO.getVlTotal());
        setObservacaoTributo(saidaPagtoGareDTO.getDsTributo());
        setDtDevolucaoEstorno(saidaPagtoGareDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaPagtoGareDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaPagtoGareDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaPagtoGareDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaPagtoGareDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaPagtoGareDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaPagtoGareDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaPagtoGareDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaPagtoGareDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaPagtoGareDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaPagtoGareDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaPagtoGareDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - GPS
    /**
     * Preenche dados man pagto gps.
     */
    public void preencheDadosManPagtoGPS() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoGPSEntradaDTO entradaDTO = new DetalharManPagtoGPSEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaPagtoGpsDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoGPS(entradaDTO);

        // Manuten��o
        setDsTipoManutencao(saidaPagtoGpsDTO.getDsTipoManutencao());

        // CLIENTE
        setNrCnpjCpf(saidaPagtoGpsDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaPagtoGpsDTO.getNomeCliente());
        setDsEmpresa(saidaPagtoGpsDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaPagtoGpsDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaPagtoGpsDTO.getDsContrato());
        setCdSituacaoContrato(saidaPagtoGpsDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaPagtoGpsDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaPagtoGpsDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaPagtoGpsDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaPagtoGpsDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaPagtoGpsDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaPagtoGpsDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaPagtoGpsDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaPagtoGpsDTO.getCdFavorecido() == null || saidaPagtoGpsDTO.getCdFavorecido().equals("") ? null
            : Long.parseLong(saidaPagtoGpsDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaPagtoGpsDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaPagtoGpsDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaPagtoGpsDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaPagtoGpsDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaPagtoGpsDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaPagtoGpsDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaPagtoGpsDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaPagtoGpsDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaPagtoGpsDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaPagtoGpsDTO.getCdListaDebito());
        setDtAgendamento(saidaPagtoGpsDTO.getDtAgendamento());
        setDtPagamento(saidaPagtoGpsDTO.getDtPagamento());
        setDtVencimento(saidaPagtoGpsDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaPagtoGpsDTO.getDsMoeda());
        setQtMoeda(saidaPagtoGpsDTO.getQtMoeda());
        setVlrAgendamento(saidaPagtoGpsDTO.getVlAgendado());
        setVlrEfetivacao(saidaPagtoGpsDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaPagtoGpsDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaPagtoGpsDTO.getDsMotivoSituacao());
        setDsPagamento(saidaPagtoGpsDTO.getDsPagamento());
        setNrDocumento(saidaPagtoGpsDTO.getNrDocumento());
        setTipoDocumento(saidaPagtoGpsDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaPagtoGpsDTO.getCdSerieDocumento());
        setVlDocumento(saidaPagtoGpsDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaPagtoGpsDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaPagtoGpsDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaPagtoGpsDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaPagtoGpsDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaPagtoGpsDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaPagtoGpsDTO.getCdTelContribuinte());
        setCodigoInss(saidaPagtoGpsDTO.getCdInss());
        setDataCompetencia(saidaPagtoGpsDTO.getDsMesAnoCompt());
        setVlPrincipal(saidaPagtoGpsDTO.getVlPrincipal());
        setVlOutrasEntidades(saidaPagtoGpsDTO.getVlOutraEntidade());
        setVlAbatimento(saidaPagtoGpsDTO.getVlAbatimento());
        setVlMulta(saidaPagtoGpsDTO.getVlMulta());
        setVlMora(saidaPagtoGpsDTO.getVlMora());
        setVlTotal(saidaPagtoGpsDTO.getVlTotal());
        setDtDevolucaoEstorno(saidaPagtoGpsDTO.getDtDevolucaoEstorno());

        // trilhaAuditoria
        setDataHoraInclusao(saidaPagtoGpsDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaPagtoGpsDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaPagtoGpsDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaPagtoGpsDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaPagtoGpsDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaPagtoGpsDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaPagtoGpsDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaPagtoGpsDTO.getComplementoManutencaoFormatado());

    }

    // Detalhar Manuten��o Pagamentos Individuais - C�digo de Barras
    /**
     * Preenche dados man pagto codigo barras.
     */
    public void preencheDadosManPagtoCodigoBarras() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharManPagtoCodigoBarrasEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaPagtoCodigoBarrasDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoCodigoBarras(
            entradaDTO);

        // Manuten��o
        setDsTipoManutencao(saidaPagtoCodigoBarrasDTO.getDsTipoManutencao());

        // CLIENTE
        setNrCnpjCpf(saidaPagtoCodigoBarrasDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaPagtoCodigoBarrasDTO.getNomeCliente());
        setDsEmpresa(saidaPagtoCodigoBarrasDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaPagtoCodigoBarrasDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaPagtoCodigoBarrasDTO.getDsContrato());
        setCdSituacaoContrato(saidaPagtoCodigoBarrasDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaPagtoCodigoBarrasDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaPagtoCodigoBarrasDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaPagtoCodigoBarrasDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaPagtoCodigoBarrasDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaPagtoCodigoBarrasDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaPagtoCodigoBarrasDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaPagtoCodigoBarrasDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaPagtoCodigoBarrasDTO.getCdFavorecido() == null
            || saidaPagtoCodigoBarrasDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaPagtoCodigoBarrasDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaPagtoCodigoBarrasDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaPagtoCodigoBarrasDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaPagtoCodigoBarrasDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaPagtoCodigoBarrasDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaPagtoCodigoBarrasDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaPagtoCodigoBarrasDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaPagtoCodigoBarrasDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaPagtoCodigoBarrasDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaPagtoCodigoBarrasDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaPagtoCodigoBarrasDTO.getCdListaDebito());
        setDtAgendamento(saidaPagtoCodigoBarrasDTO.getDtAgendamento());
        setDtPagamento(saidaPagtoCodigoBarrasDTO.getDtPagamento());
        setDtVencimento(saidaPagtoCodigoBarrasDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaPagtoCodigoBarrasDTO.getDsMoeda());
        setQtMoeda(saidaPagtoCodigoBarrasDTO.getQtMoeda());
        setVlrAgendamento(saidaPagtoCodigoBarrasDTO.getVlAgendado());
        setVlrEfetivacao(saidaPagtoCodigoBarrasDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaPagtoCodigoBarrasDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaPagtoCodigoBarrasDTO.getDsMotivoSituacao());
        setDsPagamento(saidaPagtoCodigoBarrasDTO.getDsPagamento());
        setNrDocumento(saidaPagtoCodigoBarrasDTO.getNrDocumento());
        setTipoDocumento(saidaPagtoCodigoBarrasDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaPagtoCodigoBarrasDTO.getCdSerieDocumento());
        setVlDocumento(saidaPagtoCodigoBarrasDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaPagtoCodigoBarrasDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaPagtoCodigoBarrasDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaPagtoCodigoBarrasDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaPagtoCodigoBarrasDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaPagtoCodigoBarrasDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaPagtoCodigoBarrasDTO.getNrTelefone());
        setInscricaoEstadualContribuinte(saidaPagtoCodigoBarrasDTO.getCdInscricaoEstadual());
        setCodigoBarras(saidaPagtoCodigoBarrasDTO.getDsCodigoBarraTributo());
        setCodigoReceita(saidaPagtoCodigoBarrasDTO.getCdReceitaTributo());
        setAgenteArrecadador(saidaPagtoCodigoBarrasDTO.getCdAgenteArrecadador());
        setCodigoTributo(saidaPagtoCodigoBarrasDTO.getCdTributoArrecadado());
        setNumeroReferencia(saidaPagtoCodigoBarrasDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaPagtoCodigoBarrasDTO.getNrCotaParcelaTributo());
        setPercentualReceita(saidaPagtoCodigoBarrasDTO.getCdPercentualTributo());
        setPeriodoApuracao(saidaPagtoCodigoBarrasDTO.getCdPeriodoApuracao());
        setAnoExercicio(saidaPagtoCodigoBarrasDTO.getCdAnoExercicioTributo());
        setDataReferencia(saidaPagtoCodigoBarrasDTO.getDtReferenciaTributo());
        setDataCompetencia(saidaPagtoCodigoBarrasDTO.getDtCompensacao());
        setCodigoCnae(saidaPagtoCodigoBarrasDTO.getCdCnae());
        setPlacaVeiculo(saidaPagtoCodigoBarrasDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaPagtoCodigoBarrasDTO.getNrParcelamentoTributo());
        setCodigoOrgaoFavorecido(saidaPagtoCodigoBarrasDTO.getCdOrgaoFavorecido());
        setNomeOrgaoFavorecido(saidaPagtoCodigoBarrasDTO.getDsOrgaoFavorecido());
        setCodigoUfFavorecida(saidaPagtoCodigoBarrasDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaPagtoCodigoBarrasDTO.getDsUfFavorecido());
        setVlReceita(saidaPagtoCodigoBarrasDTO.getVlReceita());
        setVlPrincipal(saidaPagtoCodigoBarrasDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaPagtoCodigoBarrasDTO.getVlAtualizado());
        setVlAcrescimoFinanceiro(saidaPagtoCodigoBarrasDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaPagtoCodigoBarrasDTO.getVlHonorario());
        setVlDesconto(saidaPagtoCodigoBarrasDTO.getVlDesconto());
        setVlAbatimento(saidaPagtoCodigoBarrasDTO.getVlAbatimento());
        setVlMulta(saidaPagtoCodigoBarrasDTO.getVlMulta());
        setVlMora(saidaPagtoCodigoBarrasDTO.getVlMora());
        setVlTotal(saidaPagtoCodigoBarrasDTO.getVlTotal());
        setObservacaoTributo(saidaPagtoCodigoBarrasDTO.getDsObservacao());
        setDtDevolucaoEstorno(saidaPagtoCodigoBarrasDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaPagtoCodigoBarrasDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaPagtoCodigoBarrasDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaPagtoCodigoBarrasDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaPagtoCodigoBarrasDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaPagtoCodigoBarrasDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaPagtoCodigoBarrasDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaPagtoCodigoBarrasDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaPagtoCodigoBarrasDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaPagtoCodigoBarrasDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaPagtoCodigoBarrasDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaPagtoCodigoBarrasDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - D�bito de Veiculos
    /**
     * Preenche dados man pagto debito veiculos.
     */
    public void preencheDadosManPagtoDebitoVeiculos() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharManPagtoDebitoVeiculosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaPagtoDebitoVeiculosDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoDebitoVeiculos(
            entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaPagtoDebitoVeiculosDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaPagtoDebitoVeiculosDTO.getNomeCliente());
        setDsEmpresa(saidaPagtoDebitoVeiculosDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaPagtoDebitoVeiculosDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaPagtoDebitoVeiculosDTO.getDsContrato());
        setCdSituacaoContrato(saidaPagtoDebitoVeiculosDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaPagtoDebitoVeiculosDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaPagtoDebitoVeiculosDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaPagtoDebitoVeiculosDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaPagtoDebitoVeiculosDTO.getDsTipoContaDebito());

        // Manunte��o
        setDsTipoManutencao(saidaPagtoDebitoVeiculosDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaPagtoDebitoVeiculosDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaPagtoDebitoVeiculosDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaPagtoDebitoVeiculosDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaPagtoDebitoVeiculosDTO.getCdFavorecido() == null
            || saidaPagtoDebitoVeiculosDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaPagtoDebitoVeiculosDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaPagtoDebitoVeiculosDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaPagtoDebitoVeiculosDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaPagtoDebitoVeiculosDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaPagtoDebitoVeiculosDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaPagtoDebitoVeiculosDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaPagtoDebitoVeiculosDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaPagtoDebitoVeiculosDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaPagtoDebitoVeiculosDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaPagtoDebitoVeiculosDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaPagtoDebitoVeiculosDTO.getCdListaDebito());
        setDtAgendamento(saidaPagtoDebitoVeiculosDTO.getDtAgendamento());
        setDtPagamento(saidaPagtoDebitoVeiculosDTO.getDtPagamento());
        setDtVencimento(saidaPagtoDebitoVeiculosDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaPagtoDebitoVeiculosDTO.getDsMoeda());
        setQtMoeda(saidaPagtoDebitoVeiculosDTO.getQtMoeda());
        setVlrAgendamento(saidaPagtoDebitoVeiculosDTO.getVlAgendado());
        setVlrEfetivacao(saidaPagtoDebitoVeiculosDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaPagtoDebitoVeiculosDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaPagtoDebitoVeiculosDTO.getDsMotivoSituacao());
        setDsPagamento(saidaPagtoDebitoVeiculosDTO.getDsPagamento());
        setNrDocumento(saidaPagtoDebitoVeiculosDTO.getNrDocumento());
        setTipoDocumento(saidaPagtoDebitoVeiculosDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaPagtoDebitoVeiculosDTO.getCdSerieDocumento());
        setVlDocumento(saidaPagtoDebitoVeiculosDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaPagtoDebitoVeiculosDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaPagtoDebitoVeiculosDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaPagtoDebitoVeiculosDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaPagtoDebitoVeiculosDTO.getDsMensagemSegundaLinha());
        setNumeroCotaParcela(saidaPagtoDebitoVeiculosDTO.getNrCotaParcelaTrib());
        setAnoExercicio(saidaPagtoDebitoVeiculosDTO.getDtAnoExercTributo());
        setPlacaVeiculo(saidaPagtoDebitoVeiculosDTO.getCdPlacaVeiculo());
        setCodigoRenavam(saidaPagtoDebitoVeiculosDTO.getCdRenavamVeicTributo());
        setMunicipioTributo(saidaPagtoDebitoVeiculosDTO.getCdMunicArrecadTrib());
        setUfTributo(saidaPagtoDebitoVeiculosDTO.getCdUfTributo());
        setNumeroNsu(saidaPagtoDebitoVeiculosDTO.getNrNsuTributo());
        setOpcaoTributo(saidaPagtoDebitoVeiculosDTO.getCdOpcaoTributo());
        setOpcaoRetiradaTributo(saidaPagtoDebitoVeiculosDTO.getCdOpcaoRetirada());
        setVlPrincipal(saidaPagtoDebitoVeiculosDTO.getVlPrincipalTributo());
        setVlAtualizacaoMonetaria(saidaPagtoDebitoVeiculosDTO.getVlAtualMonetar());
        setVlAcrescimoFinanceiro(saidaPagtoDebitoVeiculosDTO.getVlAcrescimoFinanc());
        setVlDesconto(saidaPagtoDebitoVeiculosDTO.getVlDesconto());
        setVlAbatimento(saidaPagtoDebitoVeiculosDTO.getVlAbatimento());
        setVlMulta(saidaPagtoDebitoVeiculosDTO.getVlMulta());
        setVlMora(saidaPagtoDebitoVeiculosDTO.getVlMoraJuros());
        setVlTotal(saidaPagtoDebitoVeiculosDTO.getVlTotalTributo());
        setObservacaoTributo(saidaPagtoDebitoVeiculosDTO.getDsObservacaoTributo());
        setDtFloatingPagamento(saidaPagtoDebitoVeiculosDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaPagtoDebitoVeiculosDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaPagtoDebitoVeiculosDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaPagtoDebitoVeiculosDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaPagtoDebitoVeiculosDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaPagtoDebitoVeiculosDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaPagtoDebitoVeiculosDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaPagtoDebitoVeiculosDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaPagtoDebitoVeiculosDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaPagtoDebitoVeiculosDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaPagtoDebitoVeiculosDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - Deposito Identificado
    /**
     * Preenche dados man pagto dep identificado.
     */
    public void preencheDadosManPagtoDepIdentificado() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharManPagtoDepIdentificadoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaPagtoDepIdentificadoDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoDepIdentificado(
            entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaPagtoDepIdentificadoDTO.getDsTipoManutencao());

        // CLIENTE
        setNrCnpjCpf(saidaPagtoDepIdentificadoDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaPagtoDepIdentificadoDTO.getNomeCliente());
        setDsEmpresa(saidaPagtoDepIdentificadoDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaPagtoDepIdentificadoDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaPagtoDepIdentificadoDTO.getDsContrato());
        setCdSituacaoContrato(saidaPagtoDepIdentificadoDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaPagtoDepIdentificadoDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaPagtoDepIdentificadoDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaPagtoDepIdentificadoDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaPagtoDepIdentificadoDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaPagtoDepIdentificadoDTO.getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaPagtoDepIdentificadoDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaPagtoDepIdentificadoDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaPagtoDepIdentificadoDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaPagtoDepIdentificadoDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaPagtoDepIdentificadoDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaPagtoDepIdentificadoDTO.getCdFavorecido() == null
            || saidaPagtoDepIdentificadoDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaPagtoDepIdentificadoDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaPagtoDepIdentificadoDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaPagtoDepIdentificadoDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaPagtoDepIdentificadoDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaPagtoDepIdentificadoDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaPagtoDepIdentificadoDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaPagtoDepIdentificadoDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaPagtoDepIdentificadoDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaPagtoDepIdentificadoDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaPagtoDepIdentificadoDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaPagtoDepIdentificadoDTO.getCdListaDebito());
        setDtAgendamento(saidaPagtoDepIdentificadoDTO.getDtAgendamento());
        setDtPagamento(saidaPagtoDepIdentificadoDTO.getDtPagamento());
        setDtVencimento(saidaPagtoDepIdentificadoDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaPagtoDepIdentificadoDTO.getDsMoeda());
        setQtMoeda(saidaPagtoDepIdentificadoDTO.getQtMoeda());
        setVlrAgendamento(saidaPagtoDepIdentificadoDTO.getVlAgendado());
        setVlrEfetivacao(saidaPagtoDepIdentificadoDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaPagtoDepIdentificadoDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaPagtoDepIdentificadoDTO.getDsMotivoSituacao());
        setDsPagamento(saidaPagtoDepIdentificadoDTO.getDsPagamento());
        setNrDocumento(saidaPagtoDepIdentificadoDTO.getNrDocumento());
        setTipoDocumento(saidaPagtoDepIdentificadoDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaPagtoDepIdentificadoDTO.getCdSerieDocumento());
        setVlDocumento(saidaPagtoDepIdentificadoDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaPagtoDepIdentificadoDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaPagtoDepIdentificadoDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaPagtoDepIdentificadoDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaPagtoDepIdentificadoDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaPagtoDepIdentificadoDTO.getCdBancoCredito(),
            saidaPagtoDepIdentificadoDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaPagtoDepIdentificadoDTO.getCdAgenciaCredito(),
            saidaPagtoDepIdentificadoDTO.getCdDigAgenciaCredito(), saidaPagtoDepIdentificadoDTO
            .getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(saidaPagtoDepIdentificadoDTO.getCdContaCredito(),
            saidaPagtoDepIdentificadoDTO.getCdDigContaCredito(), false));
        setTipoContaCredito(saidaPagtoDepIdentificadoDTO.getDsTipoContaFavorecido());
        setClienteDepositoIdentificado(saidaPagtoDepIdentificadoDTO.getCdClienteDepositoId());
        setTipoDespositoIdentificado(saidaPagtoDepIdentificadoDTO.getCdTipoDepositoId());
        setProdutoDepositoIdentificado(saidaPagtoDepIdentificadoDTO.getCdProdutoDepositoId());
        setSequenciaProdutoDepositoIdentificado(saidaPagtoDepIdentificadoDTO.getCdSequenciaProdutoDepositoId());
        setBancoCartaoDepositoIdentificado(saidaPagtoDepIdentificadoDTO.getCdBancoCartaoDepositanteId());
        setCartaoDepositoIdentificado(saidaPagtoDepIdentificadoDTO.getCdCartaoDepositante());
        setBimCartaoDepositoIdentificado(saidaPagtoDepIdentificadoDTO.getCdBinCartaoDepositanteId());
        setViaCartaoDepositoIdentificado(saidaPagtoDepIdentificadoDTO.getCdViaCartaoDepositanteId());
        setCodigoDepositante(saidaPagtoDepIdentificadoDTO.getCdDepositoAnteriorId());
        setNomeDepositante(saidaPagtoDepIdentificadoDTO.getDsDepositoAnteriorId());
        setVlDesconto(saidaPagtoDepIdentificadoDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaPagtoDepIdentificadoDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaPagtoDepIdentificadoDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaPagtoDepIdentificadoDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaPagtoDepIdentificadoDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaPagtoDepIdentificadoDTO.getVloutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaPagtoDepIdentificadoDTO.getVlIrCreditoPagamento());
        setVlIss(saidaPagtoDepIdentificadoDTO.getVlIssCreditoPagamento());
        setVlIof(saidaPagtoDepIdentificadoDTO.getVlIofCreditoPagamento());
        setVlInss(saidaPagtoDepIdentificadoDTO.getVlInssCreditoPagamento());
        setDtFloatingPagamento(saidaPagtoDepIdentificadoDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaPagtoDepIdentificadoDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaPagtoDepIdentificadoDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaPagtoDepIdentificadoDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaPagtoDepIdentificadoDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaPagtoDepIdentificadoDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaPagtoDepIdentificadoDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaPagtoDepIdentificadoDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaPagtoDepIdentificadoDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaPagtoDepIdentificadoDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaPagtoDepIdentificadoDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaPagtoDepIdentificadoDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaPagtoDepIdentificadoDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Individuais - Ordem de Cr�dito
    /**
     * Preenche dados man pagto ordem credito.
     */
    public void preencheDadosManPagtoOrdemCredito() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao().get(
            getItemSelecionadoListaManutencao());
        DetalharManPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharManPagtoOrdemCreditoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado.getDsEfetivacaoPagamento());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaDTO.setCdControlePagamento(retiraChaves);
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
        entradaDTO.setCdAgendadosPagoNaoPago(PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("") ? 0
            : getAgendadosPagosNaoPagos().equals("1") ? 3 : 4);
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao.getDtHoraManutencao());

        saidaPagtoOrdemCreditoDTO = getConsultarPagamentosIndividualServiceImpl().detalharManPagtoOrdemCredito(
            entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaPagtoOrdemCreditoDTO.getDsTipoManutencao());

        // CLIENTE
        setNrCnpjCpf(saidaPagtoOrdemCreditoDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaPagtoOrdemCreditoDTO.getNomeCliente());
        setDsEmpresa(saidaPagtoOrdemCreditoDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaPagtoOrdemCreditoDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaPagtoOrdemCreditoDTO.getDsContrato());
        setCdSituacaoContrato(saidaPagtoOrdemCreditoDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaPagtoOrdemCreditoDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaPagtoOrdemCreditoDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaPagtoOrdemCreditoDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaPagtoOrdemCreditoDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaPagtoOrdemCreditoDTO.getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaPagtoOrdemCreditoDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaPagtoOrdemCreditoDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaPagtoOrdemCreditoDTO.getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaPagtoOrdemCreditoDTO.getCdTipoInscricaoFavorecido());
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        setDsFavorecido(saidaPagtoOrdemCreditoDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaPagtoOrdemCreditoDTO.getCdFavorecido() == null
            || saidaPagtoOrdemCreditoDTO.getCdFavorecido().equals("") ? null : Long
                .parseLong(saidaPagtoOrdemCreditoDTO.getCdFavorecido()));
        setDsBancoFavorecido(saidaPagtoOrdemCreditoDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaPagtoOrdemCreditoDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaPagtoOrdemCreditoDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaPagtoOrdemCreditoDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaPagtoOrdemCreditoDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaPagtoOrdemCreditoDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaPagtoOrdemCreditoDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaPagtoOrdemCreditoDTO.getNrLoteArquivoRemessa());
        retiraChaves = PgitUtil.verificaStringNula(saidaPagtoOrdemCreditoDTO.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	setNumeroPagamento(retiraChaves);
        setCdListaDebito(saidaPagtoOrdemCreditoDTO.getCdListaDebito());
        setDtAgendamento(saidaPagtoOrdemCreditoDTO.getDtAgendamento());
        setDtPagamento(saidaPagtoOrdemCreditoDTO.getDtAgendamento());
        setDtVencimento(saidaPagtoOrdemCreditoDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaPagtoOrdemCreditoDTO.getDsMoeda());
        setQtMoeda(saidaPagtoOrdemCreditoDTO.getQtMoeda());
        setVlrAgendamento(saidaPagtoOrdemCreditoDTO.getVlAgendado());
        setVlrEfetivacao(saidaPagtoOrdemCreditoDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaPagtoOrdemCreditoDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaPagtoOrdemCreditoDTO.getDsMotivoSituacao());
        setDsPagamento(saidaPagtoOrdemCreditoDTO.getDsPagamento());
        setNrDocumento(saidaPagtoOrdemCreditoDTO.getNrDocumento());
        setTipoDocumento(saidaPagtoOrdemCreditoDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaPagtoOrdemCreditoDTO.getCdSerieDocumento());
        setVlDocumento(saidaPagtoOrdemCreditoDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaPagtoOrdemCreditoDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaPagtoOrdemCreditoDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaPagtoOrdemCreditoDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaPagtoOrdemCreditoDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaPagtoOrdemCreditoDTO.getCdBancoCredito(), saidaPagtoOrdemCreditoDTO
            .getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaPagtoOrdemCreditoDTO.getCdAgenciaCredito(),
            saidaPagtoOrdemCreditoDTO.getCdDigAgenciaCredito(), saidaPagtoOrdemCreditoDTO
            .getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(saidaPagtoOrdemCreditoDTO.getCdContaCredito(),
            saidaPagtoOrdemCreditoDTO.getCdDigContaCredito(), false));
        setTipoContaCredito(saidaPagtoOrdemCreditoDTO.getDsTipoContaFavorecido());
        setCodigoPagador(saidaPagtoOrdemCreditoDTO.getCdPagador());
        setCodigoOrdemCredito(saidaPagtoOrdemCreditoDTO.getCdOrdemCreditoPagamento());
        setVlDesconto(saidaPagtoOrdemCreditoDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaPagtoOrdemCreditoDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaPagtoOrdemCreditoDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaPagtoOrdemCreditoDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaPagtoOrdemCreditoDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaPagtoOrdemCreditoDTO.getVlOutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaPagtoOrdemCreditoDTO.getVlIrCreditoPagamento());
        setVlIss(saidaPagtoOrdemCreditoDTO.getVlIssCreditoPagamento());
        setVlIof(saidaPagtoOrdemCreditoDTO.getVlIofCreditoPagamento());
        setVlInss(saidaPagtoOrdemCreditoDTO.getVlInssCreditoPagamento());
        setDtFloatingPagamento(saidaPagtoOrdemCreditoDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaPagtoOrdemCreditoDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaPagtoOrdemCreditoDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaPagtoOrdemCreditoDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaPagtoOrdemCreditoDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaPagtoOrdemCreditoDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaPagtoOrdemCreditoDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaPagtoOrdemCreditoDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaPagtoOrdemCreditoDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaPagtoOrdemCreditoDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaPagtoOrdemCreditoDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaPagtoOrdemCreditoDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaPagtoOrdemCreditoDTO.getComplementoManutencaoFormatado());

    }

    /**
     * Iniciar tela.
     *
     * @param evt the evt
     */
    public void iniciarTela(ActionEvent evt) {
        // Tela de Filtro
        getFiltroAgendamentoEfetivacaoEstornoBean().setEntradaConsultarListaClientePessoas(
            new ConsultarListaClientePessoasEntradaDTO());
        getFiltroAgendamentoEfetivacaoEstornoBean().setSaidaConsultarListaClientePessoas(
            new ConsultarListaClientePessoasSaidaDTO());
        getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
        getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
        getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno("conPagamentoIndividual");

        limparCampos();
        setListaGridPagamentosIndividuais(null);
        setItemSelecionadoLista(null);
        setRegistroListaPagIndividuaisSelecionado(null);
        setDisableArgumentosConsulta(false);

        // Carregamento dos combos
        listarTipoServicoPagto();
        listarInscricaoFavorecido();
        listarConsultarSituacaoPagamento();

        setHabilitaArgumentosPesquisa(false);
        getFiltroAgendamentoEfetivacaoEstornoBean().setClienteContratoSelecionado(false);
        limparFavorecidoManutencao();
        getFiltroAgendamentoEfetivacaoEstornoBean().setVoltarParticipante("conPagamentoIndividual");
        getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);
    }

    /**
     * Limpar args lista apos pesquisa cliente contrato.
     *
     * @param evt the evt
     */
    public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
        limparCampos();
        setListaGridPagamentosIndividuais(null);
        setItemSelecionadoLista(null);
        setRegistroListaPagIndividuaisSelecionado(null);
    }

    /**
     * Limpar tela principal.
     *
     * @return the string
     */
    public String limparTelaPrincipal() {
        // Limpar Argumentos de Pesquisa e Lista
        limparFiltroPrincipal();
        setListaGridPagamentosIndividuais(null);
        setItemSelecionadoLista(null);
        setRegistroListaPagIndividuaisSelecionado(null);

        getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);
        getManterClientesOrgaoPublicoFederalBean().setListaCtasVincPartOrgao(null);
        getManterClientesOrgaoPublicoFederalBean().setFiltroSituacaoCliente(0);
        getManterClientesOrgaoPublicoFederalBean().setListaSaidaOrgaoPublico(null);
        getManterClientesOrgaoPublicoFederalBean().limparDadosConsultar();

        return "";
    }

    /**
     * Limpar apos alterar opcao cliente.
     *
     * @return the string
     */
    public String limparAposAlterarOpcaoCliente() {
        // Limpar Argumentos de Pesquisa e Lista
        getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
        setListaGridPagamentosIndividuais(null);
        setItemSelecionadoLista(null);
        setRegistroListaPagIndividuaisSelecionado(null);

        limparCampos();
        getManterClientesOrgaoPublicoFederalBean().setFiltroSituacaoCliente(0);
        getManterClientesOrgaoPublicoFederalBean().setListaSaidaOrgaoPublico(null);
        getManterClientesOrgaoPublicoFederalBean().limparFiltroPorCliente();

        return "";
    }

    /**
     * Limpar.
     *
     * @return the string
     */
    public String limpar() {
        limparFiltroPrincipal();
        setListaGridPagamentosIndividuais(null);
        setItemSelecionadoLista(null);
        setRegistroListaPagIndividuaisSelecionado(null);
        getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(null);
        setDisableArgumentosConsulta(false);
        limparFavorecido();
        return "";
    }

    /**
     * Limpar grid.
     *
     * @return the string
     */
    public String limparGrid() {
        setListaGridPagamentosIndividuais(null);
        setItemSelecionadoLista(null);
        setRegistroListaPagIndividuaisSelecionado(null);
        setDisableArgumentosConsulta(false);
        return "";
    }

    /**
     * Limpar campos compartilhados.
     */
    public void limparCamposCompartilhados() {
        setDtFloatingPagamento("");
        setDtEfetivFloatPgto("");
        setVlFloatingPagamento(null);
        setCdOperacaoDcom(null);
        setDsSituacaoDcom("");
    }

    /**
     * Consultar pagamentos individuais.
     *
     * @return the string
     */
    public String consultarPagamentosIndividuais() {
        try {
            setDisableArgumentosConsulta(true);

            entradaConsultaPgtos =  new ConsultarPagamentosIndividuaisEntradaDTO();
            
            /*
             * Se selecionado �Filtrar por Cliente� ou �Filtrar por Contrato� mover �1�. Se selecionado �Filtrar por
             * Conta D�bito� mover �2�;
             */
            if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")
                            || getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) {
                entradaConsultaPgtos.setCdTipoPesquisa(1);
            } else {
                entradaConsultaPgtos.setCdTipoPesquisa(2);
            }

            entradaConsultaPgtos.setCdAgendadosPagosNaoPagos(Integer.parseInt(getAgendadosPagosNaoPagos()));

            if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")
                            || getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) {
                /*
                 * >> Se Selecionado Filtrar por Cliente Mover Valores vindos da lista >> Se Selecionado Filtrar por
                 * Contrato Passar valor retornado da lista(Verificado por email), na especifica��o definia passar os
                 * combo e inputtext, o que poderia gerar problemas caso o usu�rio altere os valores do combo ap�s ter
                 * feito a pesquisa
                 */
                entradaConsultaPgtos.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getCdPessoaJuridicaContrato());
                entradaConsultaPgtos
                .setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getCdTipoContratoNegocio());
                entradaConsultaPgtos.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getNrSequenciaContratoNegocio());
            }

            if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
                /*
                 * Se selecionado �Filtrar por Conta D�bito� mover zeros para os cambos abaixo
                 */
                entradaConsultaPgtos.setCdPessoaJuridicaContrato(0L);
                entradaConsultaPgtos.setCdTipoContratoNegocio(0);
                entradaConsultaPgtos.setNrSequenciaContratoNegocio(0L);
            }

            /*
             * Se estiver selecionado "Filtrar por Conta D�bito", passar os campos deste filtro Se estiver selecionado
             * outro tipo de filtro, verificar se o "Conta D�bito" dos argumentos de pesquisa esta marcado Se estiver
             * marcado, passar os valores correspondentes da tela,caso contr�rio passar zeros
             */
            if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
                entradaConsultaPgtos.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean().getBancoContaDebitoFiltro());
                entradaConsultaPgtos.setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean().getAgenciaContaDebitoBancariaFiltro());
                entradaConsultaPgtos.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean().getContaBancariaContaDebitoFiltro());
                entradaConsultaPgtos.setCdDigitoConta(getFiltroAgendamentoEfetivacaoEstornoBean().getDigitoContaDebitoFiltro());
            } else {
                if (isChkContaDebito()) {
                    entradaConsultaPgtos.setCdBanco(getCdBancoContaDebito());
                    entradaConsultaPgtos.setCdAgencia(getCdAgenciaBancariaContaDebito());
                    entradaConsultaPgtos.setCdConta(getCdContaBancariaContaDebito());
                    entradaConsultaPgtos.setCdDigitoConta(getCdDigitoContaContaDebito());
                } else {
                    entradaConsultaPgtos.setCdBanco(0);
                    entradaConsultaPgtos.setCdAgencia(0);
                    entradaConsultaPgtos.setCdConta(0L);
                    entradaConsultaPgtos.setCdDigitoConta("00");
                }
            }

            entradaConsultaPgtos.setDtCreditoPagamentoInicio(FormatarData.formataDiaMesAno(getDataInicialPagamentoFiltro()));
            entradaConsultaPgtos.setDtCreditoPagamentoFim(FormatarData.formataDiaMesAno(getDataFinalPagamentoFiltro()));

            entradaConsultaPgtos.setNrArquivoRemssaPagamento(getNumeroRemessaFiltro().equals("") ? 0L : Long
                .parseLong(getNumeroRemessaFiltro()));

            if (isChkParticipanteContrato()) {
                entradaConsultaPgtos.setCdParticipante(getFiltroAgendamentoEfetivacaoEstornoBean().getCodigoParticipanteFiltro());
            } else {
                entradaConsultaPgtos.setCdParticipante(0L);
            }

            if (isChkRastreamentoTitulo()) {
                entradaConsultaPgtos.setCdTituloPgtoRastreado(Integer.parseInt(getRadioRastreados()));
            } else {
                entradaConsultaPgtos.setCdTituloPgtoRastreado(0);
            }

            setFlagExibeCampos(false);
            if(getTipoServicoFiltro() != null && "90200024".equals(getTipoServicoFiltro().toString())){
                setFlagExibeCampos(true);
            }
            entradaConsultaPgtos.setCdProdutoServicoOperacao(getTipoServicoFiltro());
            entradaConsultaPgtos.setCdProdutoServicoRelacionado(getModalidadeFiltro());
            String retiraChaves = PgitUtil.verificaStringNula(getNumeroPagamentoDeFiltro());
        	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
        	entradaConsultaPgtos.setCdControlePagamentoDe(retiraChaves);
        	
        	retiraChaves = PgitUtil.verificaStringNula(getNumeroPagamentoDeFiltro());
        	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
        	entradaConsultaPgtos.setCdControlePagamentoAte(retiraChaves);
//            entradaConsultaPgtos.setCdControlePagamentoDe(getNumeroPagamentoDeFiltro());
//            entradaConsultaPgtos.setCdControlePagamentoAte(getNumeroPagamentoDeFiltro());
            entradaConsultaPgtos.setVlPagamentoDe(getValorPagamentoDeFiltro());
            entradaConsultaPgtos.setVlPagamentoAte(getValorPagamentoAteFiltro());
            entradaConsultaPgtos.setCdSituacaoOperacaoPagamento(getCboSituacaoPagamentoFiltro());
            entradaConsultaPgtos.setCdMotivoSituacaoPagamento(getCboMotivoSituacaoFiltro());
            entradaConsultaPgtos.setCdBarraDocumento(PgitUtil.verificaStringNula(getLinhaDigitavel1())
                + PgitUtil.verificaStringNula(getLinhaDigitavel2())
                + PgitUtil.verificaStringNula(getLinhaDigitavel3())
                + PgitUtil.verificaStringNula(getLinhaDigitavel4())
                + PgitUtil.verificaStringNula(getLinhaDigitavel5())
                + PgitUtil.verificaStringNula(getLinhaDigitavel6())
                + PgitUtil.verificaStringNula(getLinhaDigitavel7())
                + PgitUtil.verificaStringNula(getLinhaDigitavel8()));
            entradaConsultaPgtos.setCdFavorecidoClientePagador(!getCodigoFavorecidoFiltro().equals("") ? Long
                .parseLong(getCodigoFavorecidoFiltro()) : 0L);
            entradaConsultaPgtos.setCdInscricaoFavorecido(!getInscricaoFiltro().equals("") ? Long.parseLong(getInscricaoFiltro())
                : 0L);
            entradaConsultaPgtos.setCdIdentificacaoInscricaoFavorecido(getTipoInscricaoFiltro());
            if (getRadioPesquisaFiltroPrincipal() != null && getRadioPesquisaFiltroPrincipal().equals("3")
                            && getRadioFavorecidoFiltro() != null && getRadioFavorecidoFiltro().equals("2")) {
                entradaConsultaPgtos.setCdBancoBeneficiario(getCdBancoContaCredito() != null ? getCdBancoContaCredito() : 0);
                entradaConsultaPgtos
                .setCdAgenciaBeneficiario(getCdAgenciaBancariaContaCredito() != null ? getCdAgenciaBancariaContaCredito()
                    : 0);
            } 

            if ("3".equals(getRadioPesquisaFiltroPrincipal()) && "3".equals(getRadioFavorecidoFiltro())) {
                entradaConsultaPgtos.setCdBancoSalarial(verificaIntegerNulo(getCdBancoContaSalario()));
                entradaConsultaPgtos.setCdAgencaSalarial(verificaIntegerNulo(getCdAgenciaBancariaContaSalario()));
                entradaConsultaPgtos.setCdContaSalarial(verificaLongNulo(getCdContaBancariaContaSalario()));
            }else{
            	entradaConsultaPgtos.setCdBancoSalarial(0);
                entradaConsultaPgtos.setCdAgencaSalarial(0);
                entradaConsultaPgtos.setCdContaSalarial(0L);
            }
            
            entradaConsultaPgtos.setCdContaBeneficiario(getCdContaBancariaContaCredito());
            entradaConsultaPgtos.setCdDigitoContaBeneficiario(getCdDigitoContaCredito());
            entradaConsultaPgtos.setNrUnidadeOrganizacional(0);
            entradaConsultaPgtos.setCdBeneficio(0L);
            entradaConsultaPgtos.setCdEspecieBeneficioInss(0);
            entradaConsultaPgtos.setCdClassificacao(getClassificacaoFiltro() != null ? getClassificacaoFiltro() : 000000000);
            entradaConsultaPgtos.setCdEmpresaContrato(0L);
            entradaConsultaPgtos.setCdTipoConta(0);
            entradaConsultaPgtos.setNrContrato(0L);
            
            if(getRadioPesquisaFiltroPrincipal() != null && getRadioFavorecidoFiltro() != null){
	            if(getRadioPesquisaFiltroPrincipal().equals("3") && getRadioFavorecidoFiltro().equals("4")){
	            	if(getCdBancoPagamento() == null || getCdBancoPagamento().equals("")){
	            		entradaConsultaPgtos.setCdBancoBeneficiario(0);           		
	            	}else {
	            		entradaConsultaPgtos.setCdBancoBeneficiario(Integer.valueOf(getCdBancoPagamento()));    
					}
	            	entradaConsultaPgtos.setCdIspbPagtoDestino(getIspb());
	            	entradaConsultaPgtos.setContaPagtoDestino(getCdContaPagamento());
	            }            
            }
            

            
            setListaGridPagamentosIndividuais(getConsultarPagamentosIndividualServiceImpl()
                .consultarPagamentosIndividuais(entradaConsultaPgtos));

            setItemSelecionadoLista(null);
            setRegistroListaPagIndividuaisSelecionado(null);
            this.listaControleRadio = new ArrayList<SelectItem>();

            for (int i = 0; i < getListaGridPagamentosIndividuais().size(); i++) {
                this.listaControleRadio.add(new SelectItem(i, " "));
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            setListaGridPagamentosIndividuais(null);
            setItemSelecionadoLista(null);
            setRegistroListaPagIndividuaisSelecionado(null);
            setDisableArgumentosConsulta(false);
        }

        return "";
    }

    /**
     * Detalhar.
     *
     * @return the string
     */
    public String detalhar() {

        try {
            setManutencao(false);
            carregaCabecalho();
            ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
                getItemSelecionadoLista());
            tipoTela = getListaGridPagamentosIndividuais().get(getItemSelecionadoLista()).getCdTipoTela();

            setRegistroListaPagIndividuaisSelecionado(registroSelecionado);
            limparCamposCompartilhados();

            if (tipoTela == 1) {
                preencheDadosPagtoCreditoConta();
                return "DETALHE_CREDITO_CONTA";
            }
            if (tipoTela == 2) {
                preencheDadosPagtoDebitoConta();
                return "DETALHE_DEBITO_CONTA";
            }
            if (tipoTela == 3) {
                preencheDadosPagtoDOC();
                return "DETALHE_DOC";
            }
            if (tipoTela == 4) {
                preencheDadosPagtoTED();
                return "DETALHE_TED";
            }
            if (tipoTela == 5) {
                preencheDadosPagtoOrdemPagto();
                return "DETALHE_ORDEM_PAGTO";
            }
            if (tipoTela == 6) {
                preencheDadosPagtoTituloBradesco();
                return "DETALHE_TITULOS_BRADESCO";
            }
            if (tipoTela == 7) {
                preencheDadosPagtoTituloOutrosBancos();
                return "DETALHE_TITULOS_OUTROS_BANCOS";
            }
            if (tipoTela == 8) {
                preencheDadosPagtoDARF();
                return "DETALHE_DARF";
            }
            if (tipoTela == 9) {
                preencheDadosPagtoGARE();
                return "DETALHE_GARE";
            }
            if (tipoTela == 10) {
                preencheDadosPagtoGPS();
                return "DETALHE_GPS";
            }
            if (tipoTela == 11) {
                preencheDadosPagtoCodigoBarras();
                return "DETALHE_CODIGO_BARRAS";
            }
            if (tipoTela == 12) {
                preencheDadosPagtoDebitoVeiculos();
                return "DETALHE_DEBITO_VEICULOS";
            }
            if (tipoTela == 13) {
                preencheDadosPagtoDepIdentificado();
                return "DETALHE_DEPOSITO_IDENTIFICADO";
            }
            if (tipoTela == 14) {
                preencheDadosPagtoOrdemCredito();
                return "DETALHE_ORDEM_CREDITO";
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
        }
        return "";
    }

    /**
     * Consultar manutencao.
     *
     * @return the string
     */
    public String consultarManutencao() {

        listaConsultarManutencao = new ArrayList<ConsultarManutencaoPagamentoSaidaDTO>();
        try {
            ConsultarManutencaoPagamentoEntradaDTO entradaDTO = new ConsultarManutencaoPagamentoEntradaDTO();
            ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
                getItemSelecionadoLista());

            entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
            entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
            entradaDTO.setCdTipoCanal(registroSelecionado.getCdTipoCanal());
            String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
        	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
        	entradaDTO.setCdControlePagamento(retiraChaves);
            entradaDTO.setCdModalidade(registroSelecionado.getCdTipoTela());
            entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
            entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
            entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
            entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
            entradaDTO.setCdDigitoAgenciaDebito(String.valueOf(registroSelecionado.getCdDigitoAgenciaDebito()));
            entradaDTO.setDtPagamento(registroSelecionado.getDtCreditoPagamento());

            carregaCabecalho();

            setListaConsultarManutencao(getConsultarPagamentosIndividualServiceImpl().consultarManutencaoPagamento(
                entradaDTO));

            // Comentado conforme solicitado - 10/03/2011 - Sempre Carregar
            // Conta de D�bito
            // if(getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")){
            setDsBancoDebito(PgitUtil.formatBanco(registroSelecionado.getCdBancoDebito(), getListaConsultarManutencao()
                .get(0).getDsBancoDebito(), false));
            setDsAgenciaDebito(PgitUtil.formatAgencia(registroSelecionado.getCdAgenciaDebito(), registroSelecionado
                .getCdDigitoAgenciaDebito(), getListaConsultarManutencao().get(0).getDsAgenciaDebito(),
                false));
            setContaDebito(PgitUtil.formatConta(registroSelecionado.getCdContaDebito(), registroSelecionado
                .getCdDigitoContaDebito(), false));
            setTipoContaDebito(getListaConsultarManutencao().get(0).getDsTipoContaDebito());
            // }

            // Se for uma Consulta Por conta D�bito, carregar Informa��es do
            // contrato conforme segue:
            if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
                setDsEmpresa(registroSelecionado.getDsEmpresa());
                setNroContrato(Long.toString(registroSelecionado.getNrContrato()));
                setDsContrato(getListaConsultarManutencao().get(0).getDsContrato());
                setCdSituacaoContrato(getListaConsultarManutencao().get(0).getDsSituacaoContrato());
            }

            limparFavorecidoManutencao();

            // Pagamento
            setDsTipoServico(registroSelecionado.getDsResumoProdutoServico());
            setDsIndicadorModalidade(registroSelecionado.getDsOperacaoProdutoServico());
            setNrSequenciaArquivoRemessa(getListaConsultarManutencao().get(0).getNrArquivoRemessaPagamento());
            setNrLote(getListaConsultarManutencao().get(0).getCdLote());
            setValorPagamentoFormatado(getListaConsultarManutencao().get(0).getVlPagamentoFormatado());
            retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
        	retiraChaves = retiraChaves.replace("[", "").replace("]", "");
        	setNumeroPagamento(retiraChaves);
            setCdListaDebito(getListaConsultarManutencao().get(0).getCdListaDebitoPagamento());
            setCanal(getListaConsultarManutencao().get(0).getDsTipoCanal());
            setDtPagamentoManutencao(getListaConsultarManutencao().get(0).getDtPagamento());
            // Na espec esta pedindo para pegar do
            // ConsultarPagamentosIndividuaisSaidaDTO mas n�o possui esse campo
            // na saida
            setDtVencimento(getListaConsultarManutencao().get(0).getDtVencimentoFormatada());
            setSituacaoPagamento(getListaConsultarManutencao().get(0).getDsSituacaoPagamentoCliente());
            setMotivoPagamento(getListaConsultarManutencao().get(0).getDsMotivoPagamentoCliente());

            // Favorecido
            setBancoFavorecido(getListaConsultarManutencao().get(0).getBancoFavorecidoFormatado());
            setAgenciaFavorecido(getListaConsultarManutencao().get(0).getAgenciaFavorecidoFormatada());
            setContaFavorecido(getListaConsultarManutencao().get(0).getContaFavorecidoFormatada());
            setTipoContaFavorecido(getListaConsultarManutencao().get(0).getCdTipoContaCreditoFavorecido());
            setDsFavorecido(registroSelecionado.getDsFavorecido());
            setTipoFavorecido(!PgitUtil.verificaStringNula(
                getListaConsultarManutencao().get(0).getCdTipoInscricaoFavorecido()).equals("") ? Integer
                    .parseInt(getListaConsultarManutencao().get(0).getCdTipoInscricaoFavorecido()) : null);
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(getTipoFavorecido(), getListaConsultarManutencao()
                .get(0).getInscricaoFavorecido().toString()));
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
            setCdFavorecido(!PgitUtil.verificaStringNula(registroSelecionado.getCdInscricaoFavorecido()).equals("") ? Long
                .parseLong(registroSelecionado.getCdInscricaoFavorecido())
                : null);

            listaManutencaoControle = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaConsultarManutencao().size(); i++) {
                listaManutencaoControle.add(new SelectItem(i, ""));
            }
            setItemSelecionadoListaManutencao(null);
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            setListaConsultarManutencao(null);
            setItemSelecionadoListaManutencao(null);
            return "";
        }

        return "CONSULTAR_MANUTENCAO";
    }

    /**
     * Historico consulta saldo.
     *
     * @return the string
     */
    public String historicoConsultaSaldo() {
        listaHistoricoConsulta = new ArrayList<ConsultarHistConsultaSaldoSaidaDTO>();
        try {
            ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
                getItemSelecionadoLista());
            ConsultarHistConsultaSaldoEntradaDTO entradaDTO = new ConsultarHistConsultaSaldoEntradaDTO();

            entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
            entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
            entradaDTO.setCdTipoCanal(registroSelecionado.getCdTipoCanal());
            String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
        	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
        	entradaDTO.setCdControlePagamento(retiraChaves);
            entradaDTO.setCdModalidade(registroSelecionado.getCdTipoTela());
            entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
            entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
            entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
            entradaDTO.setDtPagamento(registroSelecionado.getDtCreditoPagamento());

            setListaHistoricoConsulta(getConsultarPagamentosIndividualServiceImpl().consultarHistConsultaSaldo(
                entradaDTO));

            carregaCabecalho();

            // Comentado conforme solicitado - 10/03/2011 - Sempre Carregar
            // Conta de D�bito
            // if(getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")
            // &&
            // getListaHistoricoConsulta().size() > 0){
            setDsBancoDebito(PgitUtil.formatBanco(registroSelecionado.getCdBancoDebito(), getListaHistoricoConsulta()
                .get(0).getDsBancoDebito(), false));
            setDsAgenciaDebito(PgitUtil
                .formatAgencia(registroSelecionado.getCdAgenciaDebito(), registroSelecionado
                    .getCdDigitoAgenciaDebito(), getListaHistoricoConsulta().get(0)
                    .getDsAgenciaDebito(), false));
            setContaDebito(PgitUtil.formatConta(registroSelecionado.getCdContaDebito(), registroSelecionado
                .getCdDigitoContaDebito(), false));
            setTipoContaDebito(getListaHistoricoConsulta().get(0).getDsTipoConta());
            // }

            // Se for uma Consulta Por conta D�bito, carregar Informa��es do
            // contrato conforme segue:
            if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
                setDsEmpresa(registroSelecionado.getDsEmpresa());
                setNroContrato(Long.toString(registroSelecionado.getNrContrato()));
                setDsContrato(getListaHistoricoConsulta().get(0).getDsContrato());
                setCdSituacaoContrato(getListaHistoricoConsulta().get(0).getDsSituacaoContrato());
            }

            listaPesquisaHistoricoControle = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaHistoricoConsulta().size(); i++) {
                listaPesquisaHistoricoControle.add(new SelectItem(i, ""));
            }
            setItemSelecionadoListaHistorico(null);

            limparFavorecidoManutencao();

            setDsTipoServico(registroSelecionado.getDsResumoProdutoServico());
            setDsIndicadorModalidade(registroSelecionado.getDsOperacaoProdutoServico());
            setValorPagamentoFormatado(registroSelecionado.getVlEfetivoPagamentoFormatado());
            retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
        	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
        	setNumeroPagamento(retiraChaves);
            setSituacaoPagamento(getListaHistoricoConsulta().get(0).getDsSituacaoPagamentoCliente());
            setMotivoPagamento(getListaHistoricoConsulta().get(0).getDsMotivoPagamentoCliente());

            // Favorecido
            setBancoFavorecido(getListaHistoricoConsulta().get(0).getBancoFavorecidoFormatado());
            setAgenciaFavorecido(getListaHistoricoConsulta().get(0).getAgenciaFavorecidoFormatada());
            setContaFavorecido(getListaHistoricoConsulta().get(0).getContaFavorecidoFormatada());
            setTipoContaFavorecido(getListaHistoricoConsulta().get(0).getCdTipoContaCreditoFavorecido());
            setDsFavorecido(registroSelecionado.getDsFavorecido());
            setTipoFavorecido(!PgitUtil.verificaStringNula(
                getListaHistoricoConsulta().get(0).getCdTipoInscricaoFavorecido()).equals("") ? Integer
                    .parseInt(getListaHistoricoConsulta().get(0).getCdTipoInscricaoFavorecido()) : null);
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(getTipoFavorecido(), getListaHistoricoConsulta()
                .get(0).getInscricaoFavorecido().toString()));
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
            setCdFavorecido(!PgitUtil.verificaStringNula(registroSelecionado.getCdInscricaoFavorecido()).equals("") ? Long
                .parseLong(registroSelecionado.getCdInscricaoFavorecido())
                : null);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            setListaHistoricoConsulta(null);
            setItemSelecionadoListaHistorico(null);
            return "";
        }

        return "HISTORICO_CONSULTA";
    }

    /**
     * Pesquisar pagamento individual.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisarPagamentoIndividual(ActionEvent evt) {
        consultarPagamentosIndividuais();
        return "";
    }

    /**
     * Pesquisar.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisar(ActionEvent evt) {
        consultarManutencao();
        return "";
    }

    /**
     * Pesquisar historico.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisarHistorico(ActionEvent evt) {
        historicoConsultaSaldo();
        return "";
    }

    /**
     * Voltar imprimir.
     *
     * @return the string
     */
    public String voltarImprimir() {
        return "VOLTAR";
    }

    /**
     * Carrega cabecalho imprimir.
     */
    private void carregaCabecalhoImprimir() {
        limparVariaveis();
        if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")) { // Filtrar por
            // cliente
            setNrCnpjCpf(getFiltroAgendamentoEfetivacaoEstornoBean().getNrCpfCnpjCliente());
            setDsRazaoSocial(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoRazaoSocialCliente());
            setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescCliente());
            setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescCliente());
            setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescCliente());
            setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescCliente());
        } else {
            if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) { // Filtrar por
                // contrato
                setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescContrato());
                setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescContrato());
                setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescContrato());
                setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescContrato());
            }
        }
    }

    // In�cio Relat�rio imprimirPagamentoIndividual
    /**
     * Imprimir relatorio pag ind.
     *
     * @return the string
     */
    public String imprimirRelatorioPagInd() {
        
        /*
         * String caminho ="/images/Bradesco_logo.JPG"; FacesContext facesContext =
         * FacesContext.getCurrentInstance(); ServletContext servletContext =
         * (ServletContext)facesContext.getExternalContext().getContext(); String logoPath =
         * servletContext.getRealPath(caminho); //recupera os valores do cabe�alho carregaCabecalhoImprimir();
         * 
         * 
         * 
         * par.put("cpfCnpj", getNrCnpjCpf()); par.put("nomeRazaoSocial", getDsRazaoSocial()); par.put("empresa",
         * getDsEmpresa()); par.put("numero", getNroContrato()); par.put("descContrato", getDsContrato());
         * par.put("situacao", getCdSituacaoContrato());
         * 
         * if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado ().equals("2")){ try{
         * ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO = new
         * ConsultarDescBancoAgenciaContaEntradaDTO(); consultarDescBancoAgenciaContaEntradaDTO
         * .setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean ().getBancoContaDebitoFiltro());
         * consultarDescBancoAgenciaContaEntradaDTO .setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean
         * ().getAgenciaContaDebitoBancariaFiltro()); consultarDescBancoAgenciaContaEntradaDTO
         * .setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean ().getContaBancariaContaDebitoFiltro());
         * consultarDescBancoAgenciaContaEntradaDTO .setCdDigitoConta(getFiltroAgendamentoEfetivacaoEstornoBean
         * ().getDigitoContaDebitoFiltro()); ConsultarDescBancoAgenciaContaSaidaDTO
         * consultarDescBancoAgenciaContaSaidaDTO = getConsultasServiceImpl().consultarDescBancoAgenciaConta(
         * consultarDescBancoAgenciaContaEntradaDTO); par.put("bancoDebito",
         * consultarDescBancoAgenciaContaSaidaDTO.getCdBanco() + "-" +
         * consultarDescBancoAgenciaContaSaidaDTO.getDsBanco()); par.put("agenciaDebito",
         * consultarDescBancoAgenciaContaSaidaDTO.getCdAgencia() + "-" +
         * consultarDescBancoAgenciaContaSaidaDTO.getDsAgencia()); par.put("contaDebito",
         * consultarDescBancoAgenciaContaSaidaDTO.getCdConta() + "-" +
         * consultarDescBancoAgenciaContaSaidaDTO.getCdDigitoConta()); par.put("tipoContaDebito",
         * consultarDescBancoAgenciaContaSaidaDTO.getDsTipoConta()); }catch(PdcAdapterFunctionalException e){
         * par.put("bancoDebito", ""); par.put("agenciaDebito", ""); par.put("contaDebito", "");
         * par.put("tipoContaDebito", ""); } }else{ par.put("bancoDebito", ""); par.put("agenciaDebito", "");
         * par.put("contaDebito", ""); par.put("tipoContaDebito", ""); } par.put("logoBradesco", logoPath);
         */
        
        Map<String, Object> par = null;
        
        try {
            //Par�metros do Relat�rio
            par = carregaInicioRelatorio(false);
            par.put("titulo", "Consultar Pagamentos Individuais");
            
            getConsultarPagamentosIndividualServiceImpl().consultarImprimirPgtoIndividual(entradaConsultaPgtos);

            List<ConsultarPagamentosIndividuaisSaidaDTO> listaImpressao = getConsultarPagamentosIndividualServiceImpl().imprimirPagamentosIndividuais(entradaConsultaPgtos); 
            
            // M�todo que gera o relat�rio PDF.
            PgitUtil.geraRelatorioPdf("/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/individual",
                "imprimirPagamentosIndividuais", listaImpressao, par);
            
        } /* Exce��o do PDC */
        catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            
            par.put("bancoDebito", "");
            par.put("agenciaDebito", "");
            par.put("contaDebito", "");
            par.put("tipoContaDebito", "");
            
        } /* Exce��o do relat�rio */
        catch (Exception e) {
            throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
        }

        return "";
    }

    // Fim Relat�rio imprimirPagamentoIndividual

    /**
     * Imprimir comprovante conta.
     *
     * @return the string
     */
    public String imprimirComprovanteConta() {
        ImprimirComprovanteEntradaDTO entradaComprovante = obterImprimirComprovanteEntrada();

        if (entradaComprovante == null) {
            return null;
        }

        ImprimirComprovanteSaidaDTO saidaComprovante = getImprimirService().imprimirComprovanteCredConta(
            entradaComprovante);

        imprimirArquivo(saidaComprovante, "comprovanteCredConta");

        return null;
    }

    /**
     * Imprimir comprovante codigo barras.
     *
     * @return the string
     */
    public String imprimirComprovanteCodigoBarras() {
        ImprimirComprovanteEntradaDTO entradaComprovante = obterImprimirComprovanteEntrada();

        if (entradaComprovante == null) {
            return null;
        }

        ImprimirComprovanteSaidaDTO saidaComprovante = getImprimirService().imprimirComprovanteCodigoBarras(
            entradaComprovante);
        
        if (saidaComprovante.getCodMensagem().equals("PGIT0009")) {
            entradaComprovante.setCdTipoVia(2);
            ImprimirComprovanteSaidaDTO saidaSegundaViaComprovante =
                getImprimirService().imprimirComprovanteCodigoBarras(entradaComprovante);
            imprimirArquivoDuasVias(saidaComprovante, saidaSegundaViaComprovante, "comprovanteCodigoBarrasDuasVias");
        } else {
            imprimirArquivo(saidaComprovante, "comprovanteCodigoBarras");
        }
        
        return null;
    }

    /**
     * Imprimir arquivo duas vias.
     * 
     * @param comprovantePrimVia
     *            the comprovante prim via
     * @param comprovanteSegVia
     *            the comprovante seg via
     * @param nomeArquivo
     *            the nome arquivo
     */
    private void imprimirArquivoDuasVias(ImprimirComprovanteSaidaDTO comprovantePrimVia,
        ImprimirComprovanteSaidaDTO comprovanteSegVia, String nomeArquivo) {
        try {
            getImprimirService().imprimirArquivoDuasVias(FacesUtils.getServletResponse().getOutputStream(),
                comprovantePrimVia.getProtocolo(), comprovanteSegVia.getProtocolo());
        } catch (IOException e) {
            throw new PgitException(e.getMessage(), e, "");
        }
        PgitFacesUtils.generateReportResponse(nomeArquivo);
    }

    /**
     * Imprimir comprovante doc.
     *
     * @return the string
     */
    public String imprimirComprovanteDOC() {
        ImprimirComprovanteEntradaDTO entradaComprovante = obterImprimirComprovanteEntrada();

        if (entradaComprovante == null) {
            return null;
        }

        ImprimirComprovanteSaidaDTO saidaComprovante = getImprimirService().imprimirComprovanteDOC(entradaComprovante);

        imprimirArquivo(saidaComprovante, "comprovanteDOC");

        return null;
    }

    /**
     * Imprimir comprovante ted.
     *
     * @return the string
     */
    public String imprimirComprovanteTED() {
        ImprimirComprovanteEntradaDTO entradaComprovante = obterImprimirComprovanteEntrada();

        if (entradaComprovante == null) {
            return null;
        }

        ImprimirComprovanteSaidaDTO saidaComprovante = getImprimirService().imprimirComprovanteTED(entradaComprovante);

        imprimirArquivo(saidaComprovante, "comprovanteTED");

        return null;
    }

    /**
     * Imprimir comprovante op.
     *
     * @return the string
     */
    public String imprimirComprovanteOP() {
        ImprimirComprovanteEntradaDTO entradaComprovante = obterImprimirComprovanteEntrada();

        if (entradaComprovante == null) {
            return null;
        }

        ImprimirComprovanteSaidaDTO saidaComprovante = getImprimirService().imprimirComprovanteOP(entradaComprovante);

        imprimirArquivo(saidaComprovante, "comprovanteOP");

        return null;
    }

    /**
     * Imprimir comprovante titulo bradesco.
     *
     * @return the string
     */
    public String imprimirComprovanteTituloBradesco() {
        ImprimirComprovanteEntradaDTO entradaComprovante = obterImprimirComprovanteEntrada();

        if (entradaComprovante == null) {
            return null;
        }

        ImprimirComprovanteSaidaDTO saidaComprovante = getImprimirService().imprimirComprovanteTituloBradesco(
            entradaComprovante);

        imprimirArquivo(saidaComprovante, "comprovanteTituloBradesco");

        return null;
    }

    /**
     * Imprimir comprovante titulo outros.
     *
     * @return the string
     */
    public String imprimirComprovanteTituloOutros() {
        ImprimirComprovanteEntradaDTO entradaComprovante = obterImprimirComprovanteEntrada();

        if (entradaComprovante == null) {
            return null;
        }

        ImprimirComprovanteSaidaDTO saidaComprovante = getImprimirService().imprimirComprovanteTituloOutros(
            entradaComprovante);

        imprimirArquivo(saidaComprovante, "comprovanteTituloOutros");

        return null;
    }

    /**
     * Imprimir comprovante darf.
     *
     * @return the string
     */
    public String imprimirComprovanteDARF() {
        ImprimirComprovanteEntradaDTO entradaComprovante = obterImprimirComprovanteEntrada();

        if (entradaComprovante == null) {
            return null;
        }

        ImprimirComprovanteSaidaDTO saidaComprovante = getImprimirService().imprimirComprovanteDARF(entradaComprovante);

        imprimirArquivo(saidaComprovante, "comprovanteDARF");

        return null;
    }

    /**
     * Imprimir comprovante gare.
     *
     * @return the string
     */
    public String imprimirComprovanteGARE() {
        ImprimirComprovanteEntradaDTO entradaComprovante = obterImprimirComprovanteEntrada();

        if (entradaComprovante == null) {
            return null;
        }

        ImprimirComprovanteSaidaDTO saidaComprovante = getImprimirService().imprimirComprovanteGARE(entradaComprovante);

        imprimirArquivo(saidaComprovante, "comprovanteGARE");

        return null;
    }

    /**
     * Imprimir comprovante gps.
     *
     * @return the string
     */
    public String imprimirComprovanteGPS() {
        ImprimirComprovanteEntradaDTO entradaComprovante = obterImprimirComprovanteEntrada();

        if (entradaComprovante == null) {
            return null;
        }

        ImprimirComprovanteSaidaDTO saidaComprovante = getImprimirService().imprimirComprovanteGPS(entradaComprovante);

        imprimirArquivo(saidaComprovante, "comprovanteGPS");

        return null;
    }

    /**
     * Obter imprimir comprovante entrada.
     *
     * @return the imprimir comprovante entrada dto
     */
    private ImprimirComprovanteEntradaDTO obterImprimirComprovanteEntrada() {
        if (getRegistroListaPagIndividuaisSelecionado() == null) {
            return null;
        }

        ImprimirComprovanteEntradaDTO entradaComprovante = new ImprimirComprovanteEntradaDTO();
        entradaComprovante.setCdPessoaJuridicaContrato(getRegistroListaPagIndividuaisSelecionado().getCdEmpresa());
        entradaComprovante.setCdTipoContratoNegocio(getRegistroListaPagIndividuaisSelecionado()
            .getCdTipoContratoNegocio());
        entradaComprovante.setNrSequenciaContratoNegocio(getRegistroListaPagIndividuaisSelecionado().getNrContrato());
        //entradaComprovante.setCdControlePagamento(getRegistroListaPagIndividuaisSelecionado().getCdControlePagamento());
        String retiraChaves = PgitUtil.verificaStringNula(getRegistroListaPagIndividuaisSelecionado().getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entradaComprovante.setCdControlePagamento(retiraChaves);
        entradaComprovante.setCdTipoVia(1);
        String dtEfetivacaoPagamento = getRegistroListaPagIndividuaisSelecionado().getDsEfetivacaoPagamento();
        if (SiteUtil.isEmptyOrNull(dtEfetivacaoPagamento)) {
            dtEfetivacaoPagamento = getRegistroListaPagIndividuaisSelecionado().getDtCreditoPagamento();
        }
        entradaComprovante.setDtEfetivacaoPagamento(dtEfetivacaoPagamento);

        return entradaComprovante;
    }

    /**
     * Imprimir arquivo.
     *
     * @param saidaComprovante the saida comprovante
     * @param nomeArquivo the nome arquivo
     */
    private void imprimirArquivo(ImprimirComprovanteSaidaDTO saidaComprovante, String nomeArquivo) {
        try {
            getImprimirService().imprimirArquivo(FacesUtils.getServletResponse().getOutputStream(),
                saidaComprovante.getProtocolo());
        } catch (IOException e) {
            throw new PgitException(e.getMessage(), e, "");
        }
        PgitFacesUtils.generateReportResponse(nomeArquivo);
    }

    /**
     * Carrega inicio relatorio.
     *
     * @param detalhar the detalhar
     * @return the map< string, object>
     */
    private Map<String, Object> carregaInicioRelatorio(boolean detalhar) {
        if (getListaGridPagamentosIndividuais().size() > 0) {
            setDigAgenciaDebito(getListaGridPagamentosIndividuais().get(0).getCdDigitoAgenciaDebito());
        }

        String caminho = "/images/Bradesco_logo.JPG";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String logoPath = servletContext.getRealPath(caminho);
        // recupera os valores do cabe�alho

        if (detalhar) {
            carregaCabecalho();
        } else {
            carregaCabecalhoImprimir();
        }

        // Par�metros do Relat�rio
        Map<String, Object> par = new HashMap<String, Object>();
        par.put("titulo", "Consultar Pagamentos Individuais");

        par.put("cpfCnpj", getNrCnpjCpf());
        par.put("nomeRazaoSocial", getDsRazaoSocial());
        par.put("empresa", getDsEmpresa());
        par.put("numero", getNroContrato());
        par.put("descContrato", getDsContrato());
        par.put("situacao", getCdSituacaoContrato());

        if (!detalhar && getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
            ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO = new ConsultarDescBancoAgenciaContaEntradaDTO();
            consultarDescBancoAgenciaContaEntradaDTO.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getBancoContaDebitoFiltro());
            consultarDescBancoAgenciaContaEntradaDTO.setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getAgenciaContaDebitoBancariaFiltro());
            consultarDescBancoAgenciaContaEntradaDTO.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getContaBancariaContaDebitoFiltro());
            consultarDescBancoAgenciaContaEntradaDTO.setCdDigitoConta(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getDigitoContaDebitoFiltro());
            ConsultarDescBancoAgenciaContaSaidaDTO consultarDescBancoAgenciaContaSaidaDTO = getConsultasServiceImpl()
            .consultarDescBancoAgenciaConta(consultarDescBancoAgenciaContaEntradaDTO);
            par.put("bancoDebito", PgitUtil.formatBanco(consultarDescBancoAgenciaContaSaidaDTO.getCdBanco(),
                consultarDescBancoAgenciaContaSaidaDTO.getDsBanco(), false));
            par.put("agenciaDebito", PgitUtil.formatAgencia(consultarDescBancoAgenciaContaSaidaDTO.getCdAgencia(),
                getDigAgenciaDebito(), consultarDescBancoAgenciaContaSaidaDTO.getDsAgencia(), false));

            par.put("contaDebito", PgitUtil.formatConta(consultarDescBancoAgenciaContaSaidaDTO.getCdConta(),
                consultarDescBancoAgenciaContaSaidaDTO.getCdDigitoConta(), false));
            par.put("tipoContaDebito", consultarDescBancoAgenciaContaSaidaDTO.getDsTipoConta());
            
        } else {
            par.put("bancoDebito", "");
            par.put("agenciaDebito", "");
            par.put("contaDebito", "");
            par.put("tipoContaDebito", "");
        }
        
        par.put("logoBradesco", logoPath);

        return par;
    }

    /**
     * Imprimir relatorio manutencao.
     *
     * @return the string
     */
    public String imprimirRelatorioManutencao() {

        String caminho = "/images/Bradesco_logo.JPG";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String logoPath = servletContext.getRealPath(caminho);

        // Par�metros do Relat�rio
        Map<String, Object> par = new HashMap<String, Object>();

        par.put("cpfCnpj", getNrCnpjCpf());
        par.put("nomeRazaoSocial", getDsRazaoSocial());
        par.put("empresa", getDsEmpresa());
        par.put("numero", getNroContrato());
        par.put("descContrato", getDsContrato());
        par.put("situacao", getCdSituacaoContrato());
        par.put("logoBradesco", logoPath);

        par.put("titulo", "Consultar Manuten��o");

        par.put("bancoDebito", getDsBancoDebito());
        par.put("agenciaDebito", getDsAgenciaDebito());
        par.put("contaDebito", getContaDebito());
        par.put("tipoContaDebito", getTipoContaDebito());
        par.put("tipoServico", getDsTipoServico());
        par.put("modalidade", getDsIndicadorModalidade());
        par.put("remessa", getNrSequenciaArquivoRemessa());
        par.put("lote", getNrLote());
        par.put("valorPagamento", getValorPagamentoFormatado());
        par.put("numeroPagamento", getNumeroPagamento());
        par.put("listaDebito", getCdListaDebito());
        par.put("dataPagamento", getDtPagamentoManutencao());
        par.put("dataVencimento", getDtVencimento());
        par.put("numeroInscricaoFavorecido", getNumeroInscricaoFavorecido());
        par.put("tipoFavorecido", getTipoFavorecido());
        par.put("descTipoFavorecido", PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        par.put("cdFavorecido", getCdFavorecido());
        par.put("dsFavorecido", getDsFavorecido());
        par.put("bancoFavorecido", getBancoFavorecido());
        par.put("agenciaFavorecido", getAgenciaFavorecido());
        par.put("contaFavorecido", getContaFavorecido());
        par.put("tipoContaFavorecido", getTipoContaFavorecido());
        par.put("motivoPagamento", getMotivoPagamento());
        par.put("situacaoPagamento", getSituacaoPagamento());
        par.put("dsBancoDebito", getDsBancoDebito());
        par.put("dsAgenciaDebito", getDsAgenciaDebito());
        par.put("contaDebito", getContaDebito());

        try {

            // M�todo que gera o relat�rio PDF.
            PgitUtil.geraRelatorioPdf("/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/individual",
                "imprimirManPagamentosIndividuais", getListaConsultarManutencao(), par);

        } /* Exce��o do relat�rio */
        catch (Exception e) {
            throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
        }

        return "";
    }

    /**
     * Recupera detalhe imprimir.
     *
     * @return the string
     */
    private String recuperaDetalheImprimir() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
            getItemSelecionadoLista());
        if (registroSelecionado.getCdTipoTela() == 1) {
            return "imprimirPagamentoIndividualPagtoCreditoConta";
        }
        if (registroSelecionado.getCdTipoTela() == 2) {
            return "imprimirPagamentoIndividualPagtoDebitoConta";
        }
        if (registroSelecionado.getCdTipoTela() == 3) {
            return "imprimirPagamentoIndividualPagtoDOC";
        }
        if (registroSelecionado.getCdTipoTela() == 4) {
            return "imprimirPagamentoIndividualPagtoTED";
        }
        if (registroSelecionado.getCdTipoTela() == 5) {
            return "imprimirPagamentoIndividualPagtoOrdemPagto";
        }
        if (registroSelecionado.getCdTipoTela() == 6) {
            return "imprimirPagamentoIndividualPagtoTituloBradesco";
        }
        if (registroSelecionado.getCdTipoTela() == 7) {
            return "imprimirPagamentoIndividualPagtoTituloOutrosBancos";
        }
        if (registroSelecionado.getCdTipoTela() == 8) {
            return "imprimirPagamentoIndividualPagtoDARF";
        }
        if (registroSelecionado.getCdTipoTela() == 9) {
            return "imprimirPagamentoIndividualPagtoGARE";
        }
        if (registroSelecionado.getCdTipoTela() == 10) {
            return "imprimirPagamentoIndividualPagtoGPS";
        }
        if (registroSelecionado.getCdTipoTela() == 11) {
            return "imprimirPagamentoIndividualPagtoCodigoBarras";
        }
        if (registroSelecionado.getCdTipoTela() == 12) {
            return "imprimirPagamentoIndividualPagtoDebitoVeiculos";
        }
        if (registroSelecionado.getCdTipoTela() == 13) {
            return "imprimirPagamentoIndividualPagtoDepIdentificado";
        }
        if (registroSelecionado.getCdTipoTela() == 14) {
            return "imprimirPagamentoIndividualPagtoOrdemCredito";
        }
        return "";
    }

    /**
     * Imprimir detalhe.
     *
     * @return the string
     */
    public String imprimirDetalhe() {
        String relatorio = recuperaDetalheImprimir();

        if (relatorio != null && !"".equals(relatorio)) {
            Map<String, Object> par = new HashMap<String, Object>();
            String caminho = "/images/Bradesco_logo.JPG";
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
            String logoPath = servletContext.getRealPath(caminho);
            caminho = "/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/individual/imprimirPagamentoIndividualPagtoCabecalho.jasper";
            String pathCabecalho = servletContext.getRealPath(caminho);
            caminho = "/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/individual/imprimirPagamentoIndividualPagtoTrilhaAuditoria.jasper";
            String pathTrilhaAuditoria = servletContext.getRealPath(caminho);
            caminho = "/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/individual/imprimirPagamentoIndividualPagtoBeneficiario.jasper";
            String pathBeneficiario = servletContext.getRealPath(caminho);
            caminho = "/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/individual/imprimirPagamentoIndividualPagtoFavorecido.jasper";
            String pathFavorecido = servletContext.getRealPath(caminho);
            caminho = "/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/individual/imprimirPagamentoIndividualPagtoSacadorAvalista.jasper";
            String pathSacadorAvalista = servletContext.getRealPath(caminho);
            
            par.put("titulo", MessageHelperUtils.getI18nMessage("detPagamentoIndividual_title"));
            par.put("logoBradesco", logoPath);
            par.put("pathCabecalho", pathCabecalho);
            par.put("pathTrilhaAuditoria", pathTrilhaAuditoria);
            par.put("pathBeneficiario", pathBeneficiario);
            par.put("pathFavorecido", pathFavorecido);
            par.put("exibeBeneficiario", isExibeBeneficiario());
            par.put("pathSacadorAvalista", pathSacadorAvalista);

            par.put("agenciaDigitoCredito", getAgenciaDigitoCredito());
            par.put("agenciaDigitoDestino", getAgenciaDigitoDestino());
            par.put("agenteArrecadador", getAgenteArrecadador());
            par.put("anoExercicio", getAnoExercicio());
            par.put("bancoCartaoDepositoIdentificado", getBancoCartaoDepositoIdentificado());
            par.put("bimCartaoDepositoIdentificado", getBimCartaoDepositoIdentificado());
            par.put("camaraCentralizadora", getCamaraCentralizadora());
            par.put("cartaoDepositoIdentificado", getCartaoDepositoIdentificado());
            par.put("cdBancoCredito", getCdBancoCredito());
            par.put("dsBancoCredito", getDsBancoCredito());
            par.put("cdIspbPagamento", getCdIspbPagamento());
            par.put("dsNomeReclamante", getDsNomeReclamante() != null ? getDsNomeReclamante() : "");
            par.put("nrProcessoDepositoJudicial", getNrProcessoDepositoJudicial() != null ? getNrProcessoDepositoJudicial() : "");
            par.put("nrPisPasep", getNrPisPasep() != null ? getNrPisPasep() : 0L );
            par.put("cdBancoDestino", getCdBancoDestino());
            par.put("cdIndicadorEconomicoMoeda", getCdIndicadorEconomicoMoeda());
            par.put("cdListaDebito", getCdListaDebito());
            par.put("cdSerieDocumento", getCdSerieDocumento());
            par.put("cdSituacaoContrato", getCdSituacaoContrato());
            par.put("cdSituacaoOperacaoPagamento", getCdSituacaoOperacaoPagamento());
            par.put("clienteDepositoIdentificado", getClienteDepositoIdentificado());
            par.put("codigoBarras", getCodigoBarras());
            par.put("codigoCnae", getCodigoCnae());
            par.put("codigoDepositante", getCodigoDepositante());
            par.put("codigoInss", getCodigoInss());
            par.put("codigoOrdemCredito", getCodigoOrdemCredito());
            par.put("codigoOrgaoFavorecido", getCodigoOrgaoFavorecido());
            par.put("codigoPagador", getCodigoPagador());
            par.put("codigoReceita", getCodigoReceita());
            par.put("codigoRenavam", getCodigoRenavam());
            par.put("codigoTributo", getCodigoTributo());
            par.put("codigoUfFavorecida", getCodigoUfFavorecida());
            par.put("complementoInclusao", getComplementoInclusao());
            par.put("complementoManutencao", getComplementoManutencao());
            par.put("contaDebito", getContaDebito());
            par.put("contaDigitoCredito", getContaDigitoCredito());
            par.put("contaDigitoDestino", getContaDigitoDestino());
            par.put("dataCompetencia", getDataCompetencia());
            par.put("dataExpiracaoCredito", getDataExpiracaoCredito());
            par.put("dataHoraInclusao", getDataHoraInclusao());
            par.put("dataHoraManutencao", getDataHoraManutencao());
            par.put("dataReferencia", getDataReferencia());
            par.put("dataRetirada", getDataRetirada());
            par.put("dddContribuinte", getDddContribuinte());
            par.put("descricaoUfFavorecida", getDescricaoUfFavorecida());
            par.put("dsAgenciaDebito", getDsAgenciaDebito());
            par.put("dsBancoDebito", getDsBancoDebito());
            par.put("dsContrato", getDsContrato());
            par.put("dsEmpresa", getDsEmpresa());
            par.put("dsIndicadorModalidade", getDsIndicadorModalidade());
            par.put("dsMensagemPrimeiraLinha", getDsMensagemPrimeiraLinha());
            par.put("dsMensagemSegundaLinha", getDsMensagemSegundaLinha());
            par.put("dsMotivoSituacao", getDsMotivoSituacao());
            par.put("dsOrigemPagamento", getDsOrigemPagamento());
            par.put("dsPagamento", getDsPagamento());
            par.put("dsRazaoSocial", getDsRazaoSocial());
            par.put("dsTipoManutencao", getDsTipoManutencao());
            par.put("dsTipoServico", getDsTipoServico());
            par.put("dsUsoEmpresa", getDsUsoEmpresa());
            par.put("dtAgendamento", getDtAgendamento());
            par.put("dtEmissaoDocumento", getDtEmissaoDocumento());
            par.put("dtNascimentoBeneficiario", getDtNascimentoBeneficiario());
            par.put("dtPagamento", getDtPagamento().replaceAll("\\.", "/"));
            par.put("dtVencimento", getDtVencimento());
            par.put("finalidadeDoc", getFinalidadeDoc());
            par.put("cdFinalidadeTedDepositoJudicial", isCdFinalidadeTedDepositoJudicial());
            par.put("finalidadeTed", getFinalidadeTed());
            par.put("identificacaoTitularidade", getIdentificacaoTitularidade());
            par.put("identificacaoTransferenciaDoc", getIdentificacaoTransferenciaDoc());
            par.put("identificacaoTransferenciaTed", getIdentificacaoTransferenciaTed());
            par.put("identificadorRetirada", getIdentificadorRetirada());
            par.put("identificadorTipoRetirada", getIdentificadorTipoRetirada());
            par.put("cdFavorecido", getCdFavorecido());
            par.put("indicadorAssociadoUnicaAgencia", getIndicadorAssociadoUnicaAgencia());
            par.put("inscricaoEstadualContribuinte", getInscricaoEstadualContribuinte());
            par.put("instrucaoPagamento", getInstrucaoPagamento());
            par.put("municipioTributo", getMunicipioTributo());
            par.put("nomeBeneficiario", getNomeBeneficiario());
            par.put("nomeDepositante", getNomeDepositante());
            par.put("nomeOrgaoFavorecido", getNomeOrgaoFavorecido());
            par.put("nossoNumero", getNossoNumero());
            par.put("nrCnpjCpf", getNrCnpjCpf());
            par.put("nrDocumento", getNrDocumento());
            par.put("nrLoteArquivoRemessa", getNrLoteArquivoRemessa());
            par.put("nrSequenciaArquivoRemessa", getNrSequenciaArquivoRemessa());
            par.put("nroContrato", getNroContrato());
            par.put("numeroCheque", getNumeroCheque());
            par.put("numeroCotaParcela", getNumeroCotaParcela());
            par.put("numeroInscricaoBeneficiario", getNumeroInscricaoBeneficiario());
            par.put("numeroNsu", getNumeroNsu());
            par.put("numeroOp", getNumeroOp());
            par.put("numeroPagamento", getNumeroPagamento());
            par.put("numeroParcelamento", getNumeroParcelamento());
            par.put("numeroReferencia", getNumeroReferencia());
            par.put("observacaoTributo", getObservacaoTributo());
            par.put("opcaoRetiradaTributo", getOpcaoRetiradaTributo());
            par.put("opcaoTributo", getOpcaoTributo());
            par.put("percentualReceita", getPercentualReceita());
            par.put("periodoApuracao", getPeriodoApuracao());
            par.put("placaVeiculo", getPlacaVeiculo());
            par.put("produtoDepositoIdentificado", getProdutoDepositoIdentificado());
            par.put("qtMoeda", getQtMoeda());
            par.put("sequenciaProdutoDepositoIdentificado", getSequenciaProdutoDepositoIdentificado());
            par.put("serieCheque", getSerieCheque());
            par.put("telefoneContribuinte", getTelefoneContribuinte());
            par.put("tipoCanalInclusao", getTipoCanalInclusao());
            par.put("tipoCanalManutencao", getTipoCanalManutencao());
            par.put("tipoContaCredito", getTipoContaCredito());
            par.put("tipoContaDebito", getTipoContaDebito());
            par.put("tipoContaDestino", getTipoContaDestino());
            par.put("tipoDespositoIdentificado", getTipoDespositoIdentificado());
            par.put("tipoDocumento", getTipoDocumento());
            par.put("ufTributo", getUfTributo());
            par.put("usuarioInclusao", getUsuarioInclusao());
            par.put("usuarioManutencao", getUsuarioManutencao());
            par.put("viaCartaoDepositoIdentificado", getViaCartaoDepositoIdentificado());
            par.put("vlAbatimento", getVlAbatimento());
            par.put("vlAcrescimo", getVlAcrescimo());
            par.put("vlAcrescimoFinanceiro", getVlAcrescimoFinanceiro());
            par.put("vlAtualizacaoMonetaria", getVlAtualizacaoMonetaria());
            par.put("vlDeducao", getVlDeducao());
            par.put("vlDesconto", getVlDesconto());
            par.put("vlDocumento", getVlDocumento());
            par.put("vlBonificacao", getVlBonificacao());
            par.put("vlHonorarioAdvogado", getVlHonorarioAdvogado());
            par.put("vlImpostoMovFinanceira", getVlImpostoMovFinanceira());
            par.put("vlImpostoRenda", getVlImpostoRenda());
            par.put("vlInss", getVlInss());
            par.put("vlIof", getVlIof());
            par.put("vlIss", getVlIss());
            par.put("vlMora", getVlMora());
            par.put("vlMulta", getVlMulta());
            par.put("vlOutrasEntidades", getVlOutrasEntidades());
            par.put("vlPrincipal", getVlPrincipal());
            par.put("vlReceita", getVlReceita());
            par.put("vlTotal", getVlTotal());
            par.put("vlrAgendamento", getVlrAgendamento());
            par.put("vlrEfetivacao", getVlrEfetivacao());
            par.put("cdIndentificadorJudicial", getCdIndentificadorJudicial());
            par.put("dtLimiteDescontoPagamento", getDtLimiteDescontoPagamento());
            par.put("carteira", getCarteira());
            par.put("cdSituacaoTranferenciaAutomatica", getCdSituacaoTranferenciaAutomatica());
            par.put("tipoFavorecido", String.valueOf(getTipoFavorecido()));
            par.put("numeroInscricaoFavorecido", getNumeroInscricaoFavorecido());
            par.put("descTipoFavorecido", PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
            par.put("dsFavorecido", getDsFavorecido());
            par.put("cdFavorecido", getCdFavorecido());
            par.put("dsBancoFavorecido", getDsBancoOriginal());
            par.put("dsAgenciaFavorecido", getDsAgenciaOriginal());
            par.put("dsContaFavorecido", getCdDigitoContaOriginal());
            par.put("dsTipoContaFavorecido", getDsTipoContaOriginal());
            par.put("dtDevolucaoEstorno", getDtDevolucaoEstorno());
            par.put("dsEstornoPagamento", getDsEstornoPagamento());
            par.put("dtLimitePagamento", getDtLimitePagamento());
            par.put("dsRastreado", getDsRastreado());
            par.put("cdOperacaoDcom", getCdOperacaoDcom());
            par.put("dsSituacaoDcom", getDsSituacaoDcom());
            par.put("exibeDcom", getExibeDcom());
            par.put("dtEfetivFloatPgto", getDtEfetivFloatPgto());
            par.put("dtFloatingPagamento", getDtFloatingPagamento());
            par.put("numControleInternoLote", getNumControleInternoLote());
            par.put("cdFuncEmissaoOp", getCdFuncEmissaoOp());
            par.put("dtEmissaoOp", getDtEmissaoOp());
            par.put("cdTerminalCaixaEmissaoOp", getCdTerminalCaixaEmissaoOp());
            par.put("hrEmissaoOp", getHrEmissaoOp());
            par.put("vlEfetivacaoDebitoPagamento", getVlEfetivacaoDebitoPagamento());       
            par.put("vlEfetivacaoCreditoPagamento", getVlEfetivacaoCreditoPagamento());     
            par.put("vlDescontoPagamento", getVlDescontoPagamento());           
            par.put("hrEfetivacaoCreditoPagamento", getHrEfetivacaoCreditoPagamento());      
            par.put("hrEnvioCreditoPagamento", getHrEnvioCreditoPagamento());          
            par.put("cdIdentificadorTransferenciaPagto", getCdIdentificadorTransferenciaPagto());
            par.put("cdMensagemLinExtrato", getCdMensagemLinExtrato());             
            par.put("cdConveCtaSalarial", getCdConveCtaSalarial());               
            par.put("vlFloatingPagamento", getVlFloatingPagamento());               
            par.put("exibeCampos", new Boolean(isFlagExibeCampos()));     
            par.put("dsIdentificadorTransferenciaPagto", getDsIdentificadorTransferenciaPagto());
            par.put("cdBancoDestinoPagamento", getCdBancoDestinoPagamento());
            par.put("dsISPB", getDsISPB());
            par.put("cdContaPagamentoDet", getCdContaPagamentoDet());
            par.put("tipoContaDestinoPagamento", getTipoContaDestinoPagamento());
            par.put("exibeBancoCredito", new Boolean(isExibeBancoCredito())); 
            par.put("numeroInscricaoFavorecido", getNumeroInscricaoSacadorAvalista());
            par.put("descTipoFavorecido", getDsTipoSacadorAvalista());
            par.put("dsFavorecido", getDsSacadorAvalista());
            par.put("numeroInscricaoSacadorAvalista", getNumeroInscricaoSacadorAvalista());
            par.put("dsTipoSacadorAvalista", getDsTipoSacadorAvalista());
            par.put("dsSacadorAvalista", getDsSacadorAvalista());
            
            if(getNumControleInternoLote() != null && getNumControleInternoLote().compareTo(0L) != 0){
                par.put("exibeNumControleInternoLote", "S");
            }else{
                par.put("exibeNumControleInternoLote", "N");
            }

            ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
                getItemSelecionadoLista());
            if (registroSelecionado.getCdTipoTela() == 1 || registroSelecionado.getCdTipoTela() == 3
                            || registroSelecionado.getCdTipoTela() == 4 || registroSelecionado.getCdTipoTela() == 5) {
                par.put("cdTipoBeneficiario", getDsTipoBeneficiario());
            } else {
                if (getCdTipoBeneficiario() != null) {
                    if (getCdTipoBeneficiario().intValue() == 1) {
                        par.put("cdTipoBeneficiario", MessageHelperUtils.getI18nMessage("label_cpf"));
                    } else if (getCdTipoBeneficiario().intValue() == 2) {
                        par.put("cdTipoBeneficiario", MessageHelperUtils.getI18nMessage("label_cnpj"));
                    }
                }
            }

            try {
                ArrayList<String> lista = new ArrayList<String>();
                lista.add("");
                // M�todo que gera o relat�rio PDF.
                PgitUtil.geraRelatorioPdf("/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/individual",
                    relatorio, lista, par);

            } /* Exce��o do relat�rio */
            catch (Exception e) {
                throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
            }
        }

        return "";
    }
    
    /**
     * Consultar autorizantes.
     *
     * @return the string
     */
    public String consultarAutorizantes() {
        setItemSelecionadoListaAutorizante(null);
        setListaRadioAutorizante(new ArrayList<SelectItem>());
        setConsultarAutorizantesOcorrencia(new ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO>());
        
        
        try {
            ConsultarAutorizantesNetEmpresaEntradaDTO entradaDTO = new ConsultarAutorizantesNetEmpresaEntradaDTO();
            ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado = getListaGridPagamentosIndividuais().get(
                getItemSelecionadoLista());

            entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
            entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
            entradaDTO.setCdTipoCanal(registroSelecionado.getCdTipoCanal());
            String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
        	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
        	entradaDTO.setCdControlePagamento(retiraChaves);
            entradaDTO.setCdModalidade(registroSelecionado.getCdTipoTela());
            entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
            entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
            entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
            entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
            entradaDTO.setCdDigitoAgenciaDebito(String.valueOf(registroSelecionado.getCdDigitoAgenciaDebito()));

            ConsultarAutorizantesNetEmpresaSaidaDTO saida =
                consultaAutorizanteService.consultarAutorizantesNetEmpresa(entradaDTO);
            
            for (int i = 0; i < saida.getOcorrencias().size(); i++) {
                consultarAutorizantesOcorrencia.add(saida.getOcorrencias().get(i));
                listaRadioAutorizante.add(new SelectItem(i, ""));
            }

            consultarManutencao();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            return "";
        }

        return "CONSULTAR_AUTORIZANTE";
    }   
    
    /**
     * Consultar autorizantes paginacao.
     *
     * @param evt the evt
     * @return the string
     */
    public String consultarAutorizantesPaginacao(ActionEvent evt) {
        consultarAutorizantes();
        return "";
    }
    
    /**
     * Habilita botoes.
     */
    public void habilitaBotoes() {
        isHabilitaBotaoConsultarAutorizantes();
    }
    
        
    /**
     * Is habilita botao consultar autorizantes.
     *
     * @return true, if is habilita botao consultar autorizantes
     */
    public boolean isHabilitaBotaoConsultarAutorizantes(){
        if (getItemSelecionadoLista() != null){
            return false;
        }
        return true;
    }
    
    /**
     * Voltar autorizante.
     *
     * @return the string
     */
    public String voltarAutorizante() {
        setItemSelecionadoLista(null);
        
        return "VOLTAR_AUTORIZANTE";
    }   
    
    /**
     * Imprimir relatorio autorizante.
     */
    public void imprimirRelatorioAutorizante(){
        String caminho = "/images/Bradesco_logo.JPG";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String logoPath = servletContext.getRealPath(caminho);

        // Par�metros do Relat�rio
        Map<String, Object> par = new HashMap<String, Object>();

        par.put("cpfCnpj", getNrCnpjCpf());
        par.put("nomeRazaoSocial", getDsRazaoSocial());
        par.put("empresa", getDsEmpresa());
        par.put("numero", getNroContrato());
        par.put("descContrato", getDsContrato());
        par.put("situacao", getCdSituacaoContrato());
        par.put("logoBradesco", logoPath);

        par.put("titulo", "Consultar Autorizante do Pagamento Net Empresa - Individual");

        par.put("bancoDebito", getDsBancoDebito());
        par.put("agenciaDebito", getDsAgenciaDebito());
        par.put("contaDebito", getContaDebito());
        par.put("tipoContaDebito", getTipoContaDebito());
        par.put("tipoServico", getDsTipoServico());
        par.put("modalidade", getDsIndicadorModalidade());
        par.put("remessa", getNrSequenciaArquivoRemessa());
        par.put("lote", getNrLote());
        par.put("valorPagamento", getValorPagamentoFormatado());
        par.put("numeroPagamento", getNumeroPagamento());
        par.put("listaDebito", getCdListaDebito());
        par.put("dataPagamento", getDtPagamentoManutencao());
        par.put("dataVencimento", getDtVencimento());
        par.put("numeroInscricaoFavorecido", getNumeroInscricaoFavorecido());
        par.put("tipoFavorecido", getTipoFavorecido());
        par.put("descTipoFavorecido", PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
        par.put("cdFavorecido", getCdFavorecido());
        par.put("dsFavorecido", getDsFavorecido());
        par.put("bancoFavorecido", getBancoFavorecido());
        par.put("agenciaFavorecido", getAgenciaFavorecido());
        par.put("contaFavorecido", getContaFavorecido());
        par.put("tipoContaFavorecido", getTipoContaFavorecido());
        par.put("motivoPagamento", getMotivoPagamento());
        par.put("situacaoPagamento", getSituacaoPagamento());
        par.put("dsBancoDebito", getDsBancoDebito());
        par.put("dsAgenciaDebito", getDsAgenciaDebito());
        par.put("contaDebito", getContaDebito());

        try {

            PgitUtil.geraRelatorioPdf("/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/individual",
                "imprimirAutorizantesNetEmpresaIndividuais", getConsultarAutorizantesOcorrencia(), par);

        } /* Exce��o do relat�rio */
        catch (Exception e) {
            throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
        }
    }

    /**
     * Is botao detalhar retirada visivel.
     * 
     * @return true, if is botao detalhar retirada visivel
     */
    public boolean isBotaoDetalharRetiradaVisivel() {
        return detalheOrdemPagamento != null && detalheOrdemPagamento.getCdIdentificacaoTipoRetirada() != null
            && detalheOrdemPagamento.getCdIdentificacaoTipoRetirada().equals(CODIGO_IDENTIFICACAO_DIVERSOS); 
    }
    
    /**
     * Detalhar retirada.
     * 
     * @return the string
     */
    public String detalharRetirada() {
        carregarListaFormaPagamento();

        return "DETALHE_RETIRADA";
    }

    /**
     * Carregar lista forma pagamento.
     */
    private void carregarListaFormaPagamento() {
        ConsultarPagamentosIndividuaisSaidaDTO registroSelecionado =
            getListaGridPagamentosIndividuais().get(getItemSelecionadoLista());
        
        ListarTipoRetiradaEntradaDTO entrada = new ListarTipoRetiradaEntradaDTO();
        entrada.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
        String retiraChaves = PgitUtil.verificaStringNula(registroSelecionado.getCdControlePagamento());
    	retiraChaves = retiraChaves.replace("[", "").replace("]", "");		
    	entrada.setCdControlePagamento(retiraChaves);
        entrada.setCdProdutoServicoOperacao(registroSelecionado.getCdProdutoServicoOperacao());
        entrada.setCdProdutoOperacaoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
        entrada.setCdBancoPagador(detalheOrdemPagamento.getCdBancoDebito());
        entrada.setCdAgenciaBancariaPagador(detalheOrdemPagamento.getCdAgenciaDebito());
        entrada.setCdDigitoAgenciaPagador(detalheOrdemPagamento.getCdDigAgenciaDebto());
        entrada.setCdContaBancariaPagador(detalheOrdemPagamento.getCdContaDebito());
        entrada.setCdDigitoContaPagador(detalheOrdemPagamento.getDsDigAgenciaDebito());
        entrada.setCdBancoFavorecido(detalheOrdemPagamento.getCdBancoCredito());
        entrada.setCdAgenciaFavorecido(detalheOrdemPagamento.getCdAgenciaCredito());
        entrada.setCdDigitoAgenciaFavorecido(detalheOrdemPagamento.getCdDigAgenciaCredito());
        entrada.setCdContaFavorecido(detalheOrdemPagamento.getCdContaCredito());
        entrada.setCdDigitoContaFavorecido(detalheOrdemPagamento.getCdDigContaCredito());
        entrada.setDtPagamentoDe(detalheOrdemPagamento.getDtAgendamento());
        entrada.setDtPagamentoAte(detalheOrdemPagamento.getDtVencimento());

        ListarTipoRetiradaSaidaDTO saidaDetalheRetirada = consultasServiceImpl.listarTipoRetirada(entrada);
        listaFormaPagamento = saidaDetalheRetirada.getOcorrencias();
    }

    /**
     * Paginar lista forma pagamento.
     * 
     * @param actionEvent
     *            the action event
     */
    public void paginarListaFormaPagamento(ActionEvent actionEvent) {
        carregarListaFormaPagamento();
    }

    /**
     * Voltar detalhe retirada.
     * 
     * @return the string
     */
    public String voltarDetalheRetirada() {
        return "VOLTAR_DETALHE";
    }

    
    // Fim - M�todos

    /**
     * Get: listaPagamentosIndividuaisSaidaDTO.
     *
     * @return listaPagamentosIndividuaisSaidaDTO
     */
    public List<ConsultarPagamentosIndividuaisSaidaDTO> getListaPagamentosIndividuaisSaidaDTO() {
        return listaPagamentosIndividuaisSaidaDTO;
    }

    /**
     * Set: listaPagamentosIndividuaisSaidaDTO.
     *
     * @param listaPagamentosIndividuaisSaidaDTO the lista pagamentos individuais saida dto
     */
    public void setListaPagamentosIndividuaisSaidaDTO(
        List<ConsultarPagamentosIndividuaisSaidaDTO> listaPagamentosIndividuaisSaidaDTO) {
        this.listaPagamentosIndividuaisSaidaDTO = listaPagamentosIndividuaisSaidaDTO;
    }

    /**
     * Get: itemSelecionadoListaHistorico.
     *
     * @return itemSelecionadoListaHistorico
     */
    public Integer getItemSelecionadoListaHistorico() {
        return itemSelecionadoListaHistorico;
    }

    /**
     * Set: itemSelecionadoListaHistorico.
     *
     * @param itemSelecionadoListaHistorico the item selecionado lista historico
     */
    public void setItemSelecionadoListaHistorico(Integer itemSelecionadoListaHistorico) {
        this.itemSelecionadoListaHistorico = itemSelecionadoListaHistorico;
    }

    /**
     * Get: listaHistoricoConsulta.
     *
     * @return listaHistoricoConsulta
     */
    public List<ConsultarHistConsultaSaldoSaidaDTO> getListaHistoricoConsulta() {
        return listaHistoricoConsulta;
    }

    /**
     * Set: listaHistoricoConsulta.
     *
     * @param listaHistoricoConsulta the lista historico consulta
     */
    public void setListaHistoricoConsulta(List<ConsultarHistConsultaSaldoSaidaDTO> listaHistoricoConsulta) {
        this.listaHistoricoConsulta = listaHistoricoConsulta;
    }

    /**
     * Get: listaPesquisaHistoricoControle.
     *
     * @return listaPesquisaHistoricoControle
     */
    public List<SelectItem> getListaPesquisaHistoricoControle() {
        return listaPesquisaHistoricoControle;
    }

    /**
     * Set: listaPesquisaHistoricoControle.
     *
     * @param listaPesquisaHistoricoControle the lista pesquisa historico controle
     */
    public void setListaPesquisaHistoricoControle(List<SelectItem> listaPesquisaHistoricoControle) {
        this.listaPesquisaHistoricoControle = listaPesquisaHistoricoControle;
    }

    /**
     * Get: saldoAgregadoCDB.
     *
     * @return saldoAgregadoCDB
     */
    public BigDecimal getSaldoAgregadoCDB() {
        return saldoAgregadoCDB;
    }

    /**
     * Set: saldoAgregadoCDB.
     *
     * @param saldoAgregadoCDB the saldo agregado cdb
     */
    public void setSaldoAgregadoCDB(BigDecimal saldoAgregadoCDB) {
        this.saldoAgregadoCDB = saldoAgregadoCDB;
    }

    /**
     * Get: saldoAgregadoCredito.
     *
     * @return saldoAgregadoCredito
     */
    public BigDecimal getSaldoAgregadoCredito() {
        return saldoAgregadoCredito;
    }

    /**
     * Set: saldoAgregadoCredito.
     *
     * @param saldoAgregadoCredito the saldo agregado credito
     */
    public void setSaldoAgregadoCredito(BigDecimal saldoAgregadoCredito) {
        this.saldoAgregadoCredito = saldoAgregadoCredito;
    }

    /**
     * Get: saldoAgregadoFundo.
     *
     * @return saldoAgregadoFundo
     */
    public BigDecimal getSaldoAgregadoFundo() {
        return saldoAgregadoFundo;
    }

    /**
     * Set: saldoAgregadoFundo.
     *
     * @param saldoAgregadoFundo the saldo agregado fundo
     */
    public void setSaldoAgregadoFundo(BigDecimal saldoAgregadoFundo) {
        this.saldoAgregadoFundo = saldoAgregadoFundo;
    }

    /**
     * Get: saldoAgregadoPoupanca.
     *
     * @return saldoAgregadoPoupanca
     */
    public BigDecimal getSaldoAgregadoPoupanca() {
        return saldoAgregadoPoupanca;
    }

    /**
     * Set: saldoAgregadoPoupanca.
     *
     * @param saldoAgregadoPoupanca the saldo agregado poupanca
     */
    public void setSaldoAgregadoPoupanca(BigDecimal saldoAgregadoPoupanca) {
        this.saldoAgregadoPoupanca = saldoAgregadoPoupanca;
    }

    /**
     * Get: valorPagamento.
     *
     * @return valorPagamento
     */
    public BigDecimal getValorPagamento() {
        return valorPagamento;
    }

    /**
     * Set: valorPagamento.
     *
     * @param valorPagamento the valor pagamento
     */
    public void setValorPagamento(BigDecimal valorPagamento) {
        this.valorPagamento = valorPagamento;
    }

    /**
     * Get: saldoMomentoLancamento.
     *
     * @return saldoMomentoLancamento
     */
    public BigDecimal getSaldoMomentoLancamento() {
        return saldoMomentoLancamento;
    }

    /**
     * Set: saldoMomentoLancamento.
     *
     * @param saldoMomentoLancamento the saldo momento lancamento
     */
    public void setSaldoMomentoLancamento(BigDecimal saldoMomentoLancamento) {
        this.saldoMomentoLancamento = saldoMomentoLancamento;
    }

    /**
     * Get: saldoBonusCPMF.
     *
     * @return saldoBonusCPMF
     */
    public BigDecimal getSaldoBonusCPMF() {
        return saldoBonusCPMF;
    }

    /**
     * Set: saldoBonusCPMF.
     *
     * @param saldoBonusCPMF the saldo bonus cpmf
     */
    public void setSaldoBonusCPMF(BigDecimal saldoBonusCPMF) {
        this.saldoBonusCPMF = saldoBonusCPMF;
    }

    /**
     * Get: saldoOperacional.
     *
     * @return saldoOperacional
     */
    public BigDecimal getSaldoOperacional() {
        return saldoOperacional;
    }

    /**
     * Set: saldoOperacional.
     *
     * @param saldoOperacional the saldo operacional
     */
    public void setSaldoOperacional(BigDecimal saldoOperacional) {
        this.saldoOperacional = saldoOperacional;
    }

    /**
     * Get: saldoVincAdm.
     *
     * @return saldoVincAdm
     */
    public BigDecimal getSaldoVincAdm() {
        return saldoVincAdm;
    }

    /**
     * Set: saldoVincAdm.
     *
     * @param saldoVincAdm the saldo vinc adm
     */
    public void setSaldoVincAdm(BigDecimal saldoVincAdm) {
        this.saldoVincAdm = saldoVincAdm;
    }

    /**
     * Get: saldoVincAdmLp.
     *
     * @return saldoVincAdmLp
     */
    public BigDecimal getSaldoVincAdmLp() {
        return saldoVincAdmLp;
    }

    /**
     * Set: saldoVincAdmLp.
     *
     * @param saldoVincAdmLp the saldo vinc adm lp
     */
    public void setSaldoVincAdmLp(BigDecimal saldoVincAdmLp) {
        this.saldoVincAdmLp = saldoVincAdmLp;
    }

    /**
     * Get: saldoVincComReserva.
     *
     * @return saldoVincComReserva
     */
    public BigDecimal getSaldoVincComReserva() {
        return saldoVincComReserva;
    }

    /**
     * Set: saldoVincComReserva.
     *
     * @param saldoVincComReserva the saldo vinc com reserva
     */
    public void setSaldoVincComReserva(BigDecimal saldoVincComReserva) {
        this.saldoVincComReserva = saldoVincComReserva;
    }

    /**
     * Get: saldoVincJudicial.
     *
     * @return saldoVincJudicial
     */
    public BigDecimal getSaldoVincJudicial() {
        return saldoVincJudicial;
    }

    /**
     * Set: saldoVincJudicial.
     *
     * @param saldoVincJudicial the saldo vinc judicial
     */
    public void setSaldoVincJudicial(BigDecimal saldoVincJudicial) {
        this.saldoVincJudicial = saldoVincJudicial;
    }

    /**
     * Get: saldoVincSeguranca.
     *
     * @return saldoVincSeguranca
     */
    public BigDecimal getSaldoVincSeguranca() {
        return saldoVincSeguranca;
    }

    /**
     * Set: saldoVincSeguranca.
     *
     * @param saldoVincSeguranca the saldo vinc seguranca
     */
    public void setSaldoVincSeguranca(BigDecimal saldoVincSeguranca) {
        this.saldoVincSeguranca = saldoVincSeguranca;
    }

    /**
     * Get: saldoVincSemReserva.
     *
     * @return saldoVincSemReserva
     */
    public BigDecimal getSaldoVincSemReserva() {
        return saldoVincSemReserva;
    }

    /**
     * Set: saldoVincSemReserva.
     *
     * @param saldoVincSemReserva the saldo vinc sem reserva
     */
    public void setSaldoVincSemReserva(BigDecimal saldoVincSemReserva) {
        this.saldoVincSemReserva = saldoVincSemReserva;
    }

    /**
     * Get: complementoInclusao.
     *
     * @return complementoInclusao
     */
    public String getComplementoInclusao() {
        return complementoInclusao;
    }

    /**
     * Set: complementoInclusao.
     *
     * @param complementoInclusao the complemento inclusao
     */
    public void setComplementoInclusao(String complementoInclusao) {
        this.complementoInclusao = complementoInclusao;
    }

    /**
     * Get: complementoManutencao.
     *
     * @return complementoManutencao
     */
    public String getComplementoManutencao() {
        return complementoManutencao;
    }

    /**
     * Set: complementoManutencao.
     *
     * @param complementoManutencao the complemento manutencao
     */
    public void setComplementoManutencao(String complementoManutencao) {
        this.complementoManutencao = complementoManutencao;
    }

    /**
     * Get: dataHoraInclusao.
     *
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
        return dataHoraInclusao;
    }

    /**
     * Set: dataHoraInclusao.
     *
     * @param dataHoraInclusao the data hora inclusao
     */
    public void setDataHoraInclusao(String dataHoraInclusao) {
        this.dataHoraInclusao = dataHoraInclusao;
    }

    /**
     * Get: dataHoraManutencao.
     *
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
        return dataHoraManutencao;
    }

    /**
     * Set: dataHoraManutencao.
     *
     * @param dataHoraManutencao the data hora manutencao
     */
    public void setDataHoraManutencao(String dataHoraManutencao) {
        this.dataHoraManutencao = dataHoraManutencao;
    }

    /**
     * Get: tipoCanalInclusao.
     *
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
        return tipoCanalInclusao;
    }

    /**
     * Set: tipoCanalInclusao.
     *
     * @param tipoCanalInclusao the tipo canal inclusao
     */
    public void setTipoCanalInclusao(String tipoCanalInclusao) {
        this.tipoCanalInclusao = tipoCanalInclusao;
    }

    /**
     * Get: dsContaFavorecido.
     *
     * @return dsContaFavorecido
     */
    public String getDsContaFavorecido() {
        return dsContaFavorecido;
    }

    /**
     * Set: dsContaFavorecido.
     *
     * @param dsContaFavorecido the ds conta favorecido
     */
    public void setDsContaFavorecido(String dsContaFavorecido) {
        this.dsContaFavorecido = dsContaFavorecido;
    }

    /**
     * Get: tipoCanalManutencao.
     *
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
        return tipoCanalManutencao;
    }

    /**
     * Set: tipoCanalManutencao.
     *
     * @param tipoCanalManutencao the tipo canal manutencao
     */
    public void setTipoCanalManutencao(String tipoCanalManutencao) {
        this.tipoCanalManutencao = tipoCanalManutencao;
    }

    /**
     * Get: usuarioInclusao.
     *
     * @return usuarioInclusao
     */
    public String getUsuarioInclusao() {
        return usuarioInclusao;
    }

    /**
     * Set: usuarioInclusao.
     *
     * @param usuarioInclusao the usuario inclusao
     */
    public void setUsuarioInclusao(String usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    /**
     * Get: usuarioManutencao.
     *
     * @return usuarioManutencao
     */
    public String getUsuarioManutencao() {
        return usuarioManutencao;
    }

    /**
     * Set: usuarioManutencao.
     *
     * @param usuarioManutencao the usuario manutencao
     */
    public void setUsuarioManutencao(String usuarioManutencao) {
        this.usuarioManutencao = usuarioManutencao;
    }

    /**
     * Get: dataHoraConsultaSaldo.
     *
     * @return dataHoraConsultaSaldo
     */
    public String getDataHoraConsultaSaldo() {
        return dataHoraConsultaSaldo;
    }

    /**
     * Set: dataHoraConsultaSaldo.
     *
     * @param dataHoraConsultaSaldo the data hora consulta saldo
     */
    public void setDataHoraConsultaSaldo(String dataHoraConsultaSaldo) {
        this.dataHoraConsultaSaldo = dataHoraConsultaSaldo;
    }

    /**
     * Get: itemSelecionadoListaManutencao.
     *
     * @return itemSelecionadoListaManutencao
     */
    public Integer getItemSelecionadoListaManutencao() {
        return itemSelecionadoListaManutencao;
    }

    /**
     * Set: itemSelecionadoListaManutencao.
     *
     * @param itemSelecionadoListaManutencao the item selecionado lista manutencao
     */
    public void setItemSelecionadoListaManutencao(Integer itemSelecionadoListaManutencao) {
        this.itemSelecionadoListaManutencao = itemSelecionadoListaManutencao;
    }

    /**
     * Get: listaConsultarManutencao.
     *
     * @return listaConsultarManutencao
     */
    public List<ConsultarManutencaoPagamentoSaidaDTO> getListaConsultarManutencao() {
        return listaConsultarManutencao;
    }

    /**
     * Set: listaConsultarManutencao.
     *
     * @param listaConsultarManutencao the lista consultar manutencao
     */
    public void setListaConsultarManutencao(List<ConsultarManutencaoPagamentoSaidaDTO> listaConsultarManutencao) {
        this.listaConsultarManutencao = listaConsultarManutencao;
    }

    /**
     * Get: listaManutencaoControle.
     *
     * @return listaManutencaoControle
     */
    public List<SelectItem> getListaManutencaoControle() {
        return listaManutencaoControle;
    }

    /**
     * Set: listaManutencaoControle.
     *
     * @param listaManutencaoControle the lista manutencao controle
     */
    public void setListaManutencaoControle(List<SelectItem> listaManutencaoControle) {
        this.listaManutencaoControle = listaManutencaoControle;
    }

    /**
     * Get: agenciaDigitoCredito.
     *
     * @return agenciaDigitoCredito
     */
    public String getAgenciaDigitoCredito() {
        return agenciaDigitoCredito;
    }

    /**
     * Set: agenciaDigitoCredito.
     *
     * @param agenciaDigitoCredito the agencia digito credito
     */
    public void setAgenciaDigitoCredito(String agenciaDigitoCredito) {
        this.agenciaDigitoCredito = agenciaDigitoCredito;
    }

    /**
     * Get: dsEmpresa.
     *
     * @return dsEmpresa
     */
    public String getDsEmpresa() {
        return dsEmpresa;
    }

    /**
     * Set: dsEmpresa.
     *
     * @param dsEmpresa the ds empresa
     */
    public void setDsEmpresa(String dsEmpresa) {
        this.dsEmpresa = dsEmpresa;
    }

    /**
     * Get: cdTipoBeneficiario.
     *
     * @return cdTipoBeneficiario
     */
    public Integer getCdTipoBeneficiario() {
        return cdTipoBeneficiario;
    }

    /**
     * Set: cdTipoBeneficiario.
     *
     * @param cdTipoBeneficiario the cd tipo beneficiario
     */
    public void setCdTipoBeneficiario(Integer cdTipoBeneficiario) {
        this.cdTipoBeneficiario = cdTipoBeneficiario;
    }

    /**
     * Get: nroContrato.
     *
     * @return nroContrato
     */
    public String getNroContrato() {
        return nroContrato;
    }

    /**
     * Set: nroContrato.
     *
     * @param nroContrato the nro contrato
     */
    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    /**
     * Get: dsRazaoSocial.
     *
     * @return dsRazaoSocial
     */
    public String getDsRazaoSocial() {
        return dsRazaoSocial;
    }

    /**
     * Set: dsRazaoSocial.
     *
     * @param dsRazaoSocial the ds razao social
     */
    public void setDsRazaoSocial(String dsRazaoSocial) {
        this.dsRazaoSocial = dsRazaoSocial;
    }

    /**
     * Get: nrCnpjCpf.
     *
     * @return nrCnpjCpf
     */
    public String getNrCnpjCpf() {
        return nrCnpjCpf;
    }

    /**
     * Set: nrCnpjCpf.
     *
     * @param nrCnpjCpf the nr cnpj cpf
     */
    public void setNrCnpjCpf(String nrCnpjCpf) {
        this.nrCnpjCpf = nrCnpjCpf;
    }

    /**
     * Get: agenciaDigitoDestino.
     *
     * @return agenciaDigitoDestino
     */
    public String getAgenciaDigitoDestino() {
        return agenciaDigitoDestino;
    }

    /**
     * Set: agenciaDigitoDestino.
     *
     * @param agenciaDigitoDestino the agencia digito destino
     */
    public void setAgenciaDigitoDestino(String agenciaDigitoDestino) {
        this.agenciaDigitoDestino = agenciaDigitoDestino;
    }

    /**
     * Get: bancoCartaoDepositoIdentificado.
     *
     * @return bancoCartaoDepositoIdentificado
     */
    public String getBancoCartaoDepositoIdentificado() {
        return bancoCartaoDepositoIdentificado;
    }

    /**
     * Set: bancoCartaoDepositoIdentificado.
     *
     * @param bancoCartaoDepositoIdentificado the banco cartao deposito identificado
     */
    public void setBancoCartaoDepositoIdentificado(String bancoCartaoDepositoIdentificado) {
        this.bancoCartaoDepositoIdentificado = bancoCartaoDepositoIdentificado;
    }

    /**
     * Get: bimCartaoDepositoIdentificado.
     *
     * @return bimCartaoDepositoIdentificado
     */
    public String getBimCartaoDepositoIdentificado() {
        return bimCartaoDepositoIdentificado;
    }

    /**
     * Set: bimCartaoDepositoIdentificado.
     *
     * @param bimCartaoDepositoIdentificado the bim cartao deposito identificado
     */
    public void setBimCartaoDepositoIdentificado(String bimCartaoDepositoIdentificado) {
        this.bimCartaoDepositoIdentificado = bimCartaoDepositoIdentificado;
    }

    /**
     * Get: camaraCentralizadora.
     *
     * @return camaraCentralizadora
     */
    public String getCamaraCentralizadora() {
        return camaraCentralizadora;
    }

    /**
     * Set: camaraCentralizadora.
     *
     * @param camaraCentralizadora the camara centralizadora
     */
    public void setCamaraCentralizadora(String camaraCentralizadora) {
        this.camaraCentralizadora = camaraCentralizadora;
    }

    /**
     * Get: cartaoDepositoIdentificado.
     *
     * @return cartaoDepositoIdentificado
     */
    public String getCartaoDepositoIdentificado() {
        return cartaoDepositoIdentificado;
    }

    /**
     * Set: cartaoDepositoIdentificado.
     *
     * @param cartaoDepositoIdentificado the cartao deposito identificado
     */
    public void setCartaoDepositoIdentificado(String cartaoDepositoIdentificado) {
        this.cartaoDepositoIdentificado = cartaoDepositoIdentificado;
    }

    /**
     * Get: cdBancoCredito.
     *
     * @return cdBancoCredito
     */
    public String getCdBancoCredito() {
        return cdBancoCredito;
    }

    /**
     * Set: cdBancoCredito.
     *
     * @param cdBancoCredito the cd banco credito
     */
    public void setCdBancoCredito(String cdBancoCredito) {
        this.cdBancoCredito = cdBancoCredito;
    }

    /**
     * Get: cdBancoDestino.
     *
     * @return cdBancoDestino
     */
    public String getCdBancoDestino() {
        return cdBancoDestino;
    }

    /**
     * Set: cdBancoDestino.
     *
     * @param cdBancoDestino the cd banco destino
     */
    public void setCdBancoDestino(String cdBancoDestino) {
        this.cdBancoDestino = cdBancoDestino;
    }

    /**
     * Get: cdFavorecido.
     *
     * @return cdFavorecido
     */
    public Long getCdFavorecido() {
        return cdFavorecido;
    }

    /**
     * Set: cdFavorecido.
     *
     * @param cdFavorecido the cd favorecido
     */
    public void setCdFavorecido(Long cdFavorecido) {
        this.cdFavorecido = cdFavorecido;
    }

    /**
     * Get: cdIndicadorEconomicoMoeda.
     *
     * @return cdIndicadorEconomicoMoeda
     */
    public String getCdIndicadorEconomicoMoeda() {
        return cdIndicadorEconomicoMoeda;
    }

    /**
     * Set: cdIndicadorEconomicoMoeda.
     *
     * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
     */
    public void setCdIndicadorEconomicoMoeda(String cdIndicadorEconomicoMoeda) {
        this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
    }

    /**
     * Get: dsIndicadorModalidade.
     *
     * @return dsIndicadorModalidade
     */
    public String getDsIndicadorModalidade() {
        return dsIndicadorModalidade;
    }

    /**
     * Set: dsIndicadorModalidade.
     *
     * @param dsIndicadorModalidade the ds indicador modalidade
     */
    public void setDsIndicadorModalidade(String dsIndicadorModalidade) {
        this.dsIndicadorModalidade = dsIndicadorModalidade;
    }

    /**
     * Get: cdListaDebito.
     *
     * @return cdListaDebito
     */
    public String getCdListaDebito() {
        return cdListaDebito;
    }

    /**
     * Set: cdListaDebito.
     *
     * @param cdListaDebito the cd lista debito
     */
    public void setCdListaDebito(String cdListaDebito) {
        this.cdListaDebito = cdListaDebito;
    }

    /**
     * Get: cdMotivoSituacaoPagamento.
     *
     * @return cdMotivoSituacaoPagamento
     */
    public Integer getCdMotivoSituacaoPagamento() {
        return cdMotivoSituacaoPagamento;
    }

    /**
     * Set: cdMotivoSituacaoPagamento.
     *
     * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
     */
    public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
        this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
    }

    /**
     * Get: cdSituacaoContrato.
     *
     * @return cdSituacaoContrato
     */
    public String getCdSituacaoContrato() {
        return cdSituacaoContrato;
    }

    /**
     * Set: cdSituacaoContrato.
     *
     * @param cdSituacaoContrato the cd situacao contrato
     */
    public void setCdSituacaoContrato(String cdSituacaoContrato) {
        this.cdSituacaoContrato = cdSituacaoContrato;
    }

    /**
     * Get: cdSituacaoOperacaoPagamento.
     *
     * @return cdSituacaoOperacaoPagamento
     */
    public String getCdSituacaoOperacaoPagamento() {
        return cdSituacaoOperacaoPagamento;
    }

    /**
     * Set: cdSituacaoOperacaoPagamento.
     *
     * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
     */
    public void setCdSituacaoOperacaoPagamento(String cdSituacaoOperacaoPagamento) {
        this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
    }

    /**
     * Get: cdTipoContaFavorecido.
     *
     * @return cdTipoContaFavorecido
     */
    public Integer getCdTipoContaFavorecido() {
        return cdTipoContaFavorecido;
    }

    /**
     * Set: cdTipoContaFavorecido.
     *
     * @param cdTipoContaFavorecido the cd tipo conta favorecido
     */
    public void setCdTipoContaFavorecido(Integer cdTipoContaFavorecido) {
        this.cdTipoContaFavorecido = cdTipoContaFavorecido;
    }

    /**
     * Get: contaDebito.
     *
     * @return contaDebito
     */
    public String getContaDebito() {
        return contaDebito;
    }

    /**
     * Get: tipoFavorecido.
     *
     * @return tipoFavorecido
     */
    public Integer getTipoFavorecido() {
        return tipoFavorecido;
    }

    /**
     * Set: tipoFavorecido.
     *
     * @param tipoFavorecido the tipo favorecido
     */
    public void setTipoFavorecido(Integer tipoFavorecido) {
        this.tipoFavorecido = tipoFavorecido;
    }

    /**
     * Set: contaDebito.
     *
     * @param contaDebito the conta debito
     */
    public void setContaDebito(String contaDebito) {
        this.contaDebito = contaDebito;
    }

    /**
     * Get: clienteDepositoIdentificado.
     *
     * @return clienteDepositoIdentificado
     */
    public String getClienteDepositoIdentificado() {
        return clienteDepositoIdentificado;
    }

    /**
     * Set: clienteDepositoIdentificado.
     *
     * @param clienteDepositoIdentificado the cliente deposito identificado
     */
    public void setClienteDepositoIdentificado(String clienteDepositoIdentificado) {
        this.clienteDepositoIdentificado = clienteDepositoIdentificado;
    }

    /**
     * Get: codigoBarras.
     *
     * @return codigoBarras
     */
    public String getCodigoBarras() {
        return codigoBarras;
    }

    /**
     * Set: codigoBarras.
     *
     * @param codigoBarras the codigo barras
     */
    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    /**
     * Get: codigoCnae.
     *
     * @return codigoCnae
     */
    public String getCodigoCnae() {
        return codigoCnae;
    }

    /**
     * Set: codigoCnae.
     *
     * @param codigoCnae the codigo cnae
     */
    public void setCodigoCnae(String codigoCnae) {
        this.codigoCnae = codigoCnae;
    }

    /**
     * Get: codigoDepositante.
     *
     * @return codigoDepositante
     */
    public String getCodigoDepositante() {
        return codigoDepositante;
    }

    /**
     * Set: codigoDepositante.
     *
     * @param codigoDepositante the codigo depositante
     */
    public void setCodigoDepositante(String codigoDepositante) {
        this.codigoDepositante = codigoDepositante;
    }

    /**
     * Get: codigoInss.
     *
     * @return codigoInss
     */
    public String getCodigoInss() {
        return codigoInss;
    }

    /**
     * Set: codigoInss.
     *
     * @param codigoInss the codigo inss
     */
    public void setCodigoInss(String codigoInss) {
        this.codigoInss = codigoInss;
    }

    /**
     * Get: codigoOrdemCredito.
     *
     * @return codigoOrdemCredito
     */
    public String getCodigoOrdemCredito() {
        return codigoOrdemCredito;
    }

    /**
     * Set: codigoOrdemCredito.
     *
     * @param codigoOrdemCredito the codigo ordem credito
     */
    public void setCodigoOrdemCredito(String codigoOrdemCredito) {
        this.codigoOrdemCredito = codigoOrdemCredito;
    }

    /**
     * Get: codigoRenavam.
     *
     * @return codigoRenavam
     */
    public String getCodigoRenavam() {
        return codigoRenavam;
    }

    /**
     * Set: codigoRenavam.
     *
     * @param codigoRenavam the codigo renavam
     */
    public void setCodigoRenavam(String codigoRenavam) {
        this.codigoRenavam = codigoRenavam;
    }

    /**
     * Get: codigoTributo.
     *
     * @return codigoTributo
     */
    public String getCodigoTributo() {
        return codigoTributo;
    }

    /**
     * Set: codigoTributo.
     *
     * @param codigoTributo the codigo tributo
     */
    public void setCodigoTributo(String codigoTributo) {
        this.codigoTributo = codigoTributo;
    }

    /**
     * Get: codigoOrgaoFavorecido.
     *
     * @return codigoOrgaoFavorecido
     */
    public String getCodigoOrgaoFavorecido() {
        return codigoOrgaoFavorecido;
    }

    /**
     * Set: codigoOrgaoFavorecido.
     *
     * @param codigoOrgaoFavorecido the codigo orgao favorecido
     */
    public void setCodigoOrgaoFavorecido(String codigoOrgaoFavorecido) {
        this.codigoOrgaoFavorecido = codigoOrgaoFavorecido;
    }

    /**
     * Get: codigoPagador.
     *
     * @return codigoPagador
     */
    public String getCodigoPagador() {
        return codigoPagador;
    }

    /**
     * Set: codigoPagador.
     *
     * @param codigoPagador the codigo pagador
     */
    public void setCodigoPagador(String codigoPagador) {
        this.codigoPagador = codigoPagador;
    }

    /**
     * Get: codigoReceita.
     *
     * @return codigoReceita
     */
    public String getCodigoReceita() {
        return codigoReceita;
    }

    /**
     * Set: codigoReceita.
     *
     * @param codigoReceita the codigo receita
     */
    public void setCodigoReceita(String codigoReceita) {
        this.codigoReceita = codigoReceita;
    }

    /**
     * Get: codigoUfFavorecida.
     *
     * @return codigoUfFavorecida
     */
    public String getCodigoUfFavorecida() {
        return codigoUfFavorecida;
    }

    /**
     * Set: codigoUfFavorecida.
     *
     * @param codigoUfFavorecida the codigo uf favorecida
     */
    public void setCodigoUfFavorecida(String codigoUfFavorecida) {
        this.codigoUfFavorecida = codigoUfFavorecida;
    }

    /**
     * Get: dataReferencia.
     *
     * @return dataReferencia
     */
    public String getDataReferencia() {
        return dataReferencia;
    }

    /**
     * Set: dataReferencia.
     *
     * @param dataReferencia the data referencia
     */
    public void setDataReferencia(String dataReferencia) {
        this.dataReferencia = dataReferencia;
    }

    /**
     * Get: dddContribuinte.
     *
     * @return dddContribuinte
     */
    public String getDddContribuinte() {
        return dddContribuinte;
    }

    /**
     * Set: dddContribuinte.
     *
     * @param dddContribuinte the ddd contribuinte
     */
    public void setDddContribuinte(String dddContribuinte) {
        this.dddContribuinte = dddContribuinte;
    }

    /**
     * Get: inscricaoEstadualContribuinte.
     *
     * @return inscricaoEstadualContribuinte
     */
    public String getInscricaoEstadualContribuinte() {
        return inscricaoEstadualContribuinte;
    }

    /**
     * Set: inscricaoEstadualContribuinte.
     *
     * @param inscricaoEstadualContribuinte the inscricao estadual contribuinte
     */
    public void setInscricaoEstadualContribuinte(String inscricaoEstadualContribuinte) {
        this.inscricaoEstadualContribuinte = inscricaoEstadualContribuinte;
    }

    /**
     * Get: numeroParcelamento.
     *
     * @return numeroParcelamento
     */
    public String getNumeroParcelamento() {
        return numeroParcelamento;
    }

    /**
     * Get: dsFavorecido.
     *
     * @return dsFavorecido
     */
    public String getDsFavorecido() {
        return dsFavorecido;
    }

    /**
     * Set: dsFavorecido.
     *
     * @param dsFavorecido the ds favorecido
     */
    public void setDsFavorecido(String dsFavorecido) {
        this.dsFavorecido = dsFavorecido;
    }

    /**
     * Set: numeroParcelamento.
     *
     * @param numeroParcelamento the numero parcelamento
     */
    public void setNumeroParcelamento(String numeroParcelamento) {
        this.numeroParcelamento = numeroParcelamento;
    }

    /**
     * Get: numeroReferencia.
     *
     * @return numeroReferencia
     */
    public String getNumeroReferencia() {
        return numeroReferencia;
    }

    /**
     * Set: numeroReferencia.
     *
     * @param numeroReferencia the numero referencia
     */
    public void setNumeroReferencia(String numeroReferencia) {
        this.numeroReferencia = numeroReferencia;
    }

    /**
     * Get: contaDigitoCredito.
     *
     * @return contaDigitoCredito
     */
    public String getContaDigitoCredito() {
        return contaDigitoCredito;
    }

    /**
     * Set: contaDigitoCredito.
     *
     * @param contaDigitoCredito the conta digito credito
     */
    public void setContaDigitoCredito(String contaDigitoCredito) {
        this.contaDigitoCredito = contaDigitoCredito;
    }

    /**
     * Get: contaDigitoDestino.
     *
     * @return contaDigitoDestino
     */
    public String getContaDigitoDestino() {
        return contaDigitoDestino;
    }

    /**
     * Set: contaDigitoDestino.
     *
     * @param contaDigitoDestino the conta digito destino
     */
    public void setContaDigitoDestino(String contaDigitoDestino) {
        this.contaDigitoDestino = contaDigitoDestino;
    }

    /**
     * Get: dataExpiracaoCredito.
     *
     * @return dataExpiracaoCredito
     */
    public String getDataExpiracaoCredito() {
        return dataExpiracaoCredito;
    }

    /**
     * Set: dataExpiracaoCredito.
     *
     * @param dataExpiracaoCredito the data expiracao credito
     */
    public void setDataExpiracaoCredito(String dataExpiracaoCredito) {
        this.dataExpiracaoCredito = dataExpiracaoCredito;
    }

    /**
     * Get: dataRetirada.
     *
     * @return dataRetirada
     */
    public String getDataRetirada() {
        return dataRetirada;
    }

    /**
     * Set: dataRetirada.
     *
     * @param dataRetirada the data retirada
     */
    public void setDataRetirada(String dataRetirada) {
        this.dataRetirada = dataRetirada;
    }

    /**
     * Get: descricaoUfFavorecida.
     *
     * @return descricaoUfFavorecida
     */
    public String getDescricaoUfFavorecida() {
        return descricaoUfFavorecida;
    }

    /**
     * Set: descricaoUfFavorecida.
     *
     * @param descricaoUfFavorecida the descricao uf favorecida
     */
    public void setDescricaoUfFavorecida(String descricaoUfFavorecida) {
        this.descricaoUfFavorecida = descricaoUfFavorecida;
    }

    /**
     * Get: dsAgenciaDebito.
     *
     * @return dsAgenciaDebito
     */
    public String getDsAgenciaDebito() {
        return dsAgenciaDebito;
    }

    /**
     * Set: dsAgenciaDebito.
     *
     * @param dsAgenciaDebito the ds agencia debito
     */
    public void setDsAgenciaDebito(String dsAgenciaDebito) {
        this.dsAgenciaDebito = dsAgenciaDebito;
    }

    /**
     * Get: dsAgenciaFavorecido.
     *
     * @return dsAgenciaFavorecido
     */
    public String getDsAgenciaFavorecido() {
        return dsAgenciaFavorecido;
    }

    /**
     * Set: dsAgenciaFavorecido.
     *
     * @param dsAgenciaFavorecido the ds agencia favorecido
     */
    public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
        this.dsAgenciaFavorecido = dsAgenciaFavorecido;
    }

    /**
     * Get: dsBancoDebito.
     *
     * @return dsBancoDebito
     */
    public String getDsBancoDebito() {
        return dsBancoDebito;
    }

    /**
     * Set: dsBancoDebito.
     *
     * @param dsBancoDebito the ds banco debito
     */
    public void setDsBancoDebito(String dsBancoDebito) {
        this.dsBancoDebito = dsBancoDebito;
    }

    /**
     * Get: dsBancoFavorecido.
     *
     * @return dsBancoFavorecido
     */
    public String getDsBancoFavorecido() {
        return dsBancoFavorecido;
    }

    /**
     * Set: dsBancoFavorecido.
     *
     * @param dsBancoFavorecido the ds banco favorecido
     */
    public void setDsBancoFavorecido(String dsBancoFavorecido) {
        this.dsBancoFavorecido = dsBancoFavorecido;
    }

    /**
     * Get: dsContrato.
     *
     * @return dsContrato
     */
    public String getDsContrato() {
        return dsContrato;
    }

    /**
     * Set: dsContrato.
     *
     * @param dsContrato the ds contrato
     */
    public void setDsContrato(String dsContrato) {
        this.dsContrato = dsContrato;
    }

    /**
     * Get: dsMensagemPrimeiraLinha.
     *
     * @return dsMensagemPrimeiraLinha
     */
    public String getDsMensagemPrimeiraLinha() {
        return dsMensagemPrimeiraLinha;
    }

    /**
     * Set: dsMensagemPrimeiraLinha.
     *
     * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
     */
    public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
        this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
    }

    /**
     * Get: dsMensagemSegundaLinha.
     *
     * @return dsMensagemSegundaLinha
     */
    public String getDsMensagemSegundaLinha() {
        return dsMensagemSegundaLinha;
    }

    /**
     * Set: dsMensagemSegundaLinha.
     *
     * @param dsMensagemSegundaLinha the ds mensagem segunda linha
     */
    public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
        this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
    }

    /**
     * Get: agenteArrecadador.
     *
     * @return agenteArrecadador
     */
    public String getAgenteArrecadador() {
        return agenteArrecadador;
    }

    /**
     * Set: agenteArrecadador.
     *
     * @param agenteArrecadador the agente arrecadador
     */
    public void setAgenteArrecadador(String agenteArrecadador) {
        this.agenteArrecadador = agenteArrecadador;
    }

    /**
     * Get: anoExercicio.
     *
     * @return anoExercicio
     */
    public String getAnoExercicio() {
        return anoExercicio;
    }

    /**
     * Set: anoExercicio.
     *
     * @param anoExercicio the ano exercicio
     */
    public void setAnoExercicio(String anoExercicio) {
        this.anoExercicio = anoExercicio;
    }

    /**
     * Get: dataCompetencia.
     *
     * @return dataCompetencia
     */
    public String getDataCompetencia() {
        return dataCompetencia;
    }

    /**
     * Get: canal.
     *
     * @return canal
     */
    public String getCanal() {
        return canal;
    }

    /**
     * Set: canal.
     *
     * @param canal the canal
     */
    public void setCanal(String canal) {
        this.canal = canal;
    }

    /**
     * Get: orgaoPagador.
     *
     * @return orgaoPagador
     */
    public String getOrgaoPagador() {
        return orgaoPagador;
    }

    /**
     * Set: orgaoPagador.
     *
     * @param orgaoPagador the orgao pagador
     */
    public void setOrgaoPagador(String orgaoPagador) {
        this.orgaoPagador = orgaoPagador;
    }

    /**
     * Get: tipoCanal.
     *
     * @return tipoCanal
     */
    public String getTipoCanal() {
        return tipoCanal;
    }

    /**
     * Set: tipoCanal.
     *
     * @param tipoCanal the tipo canal
     */
    public void setTipoCanal(String tipoCanal) {
        this.tipoCanal = tipoCanal;
    }

    /**
     * Set: dataCompetencia.
     *
     * @param dataCompetencia the data competencia
     */
    public void setDataCompetencia(String dataCompetencia) {
        this.dataCompetencia = dataCompetencia;
    }

    /**
     * Get: telefoneContribuinte.
     *
     * @return telefoneContribuinte
     */
    public String getTelefoneContribuinte() {
        return telefoneContribuinte;
    }

    /**
     * Set: telefoneContribuinte.
     *
     * @param telefoneContribuinte the telefone contribuinte
     */
    public void setTelefoneContribuinte(String telefoneContribuinte) {
        this.telefoneContribuinte = telefoneContribuinte;
    }

    /**
     * Get: cdSerieDocumento.
     *
     * @return cdSerieDocumento
     */
    public String getCdSerieDocumento() {
        return cdSerieDocumento;
    }

    /**
     * Get: dsMotivoSituacao.
     *
     * @return dsMotivoSituacao
     */
    public String getDsMotivoSituacao() {
        return dsMotivoSituacao;
    }

    /**
     * Set: dsMotivoSituacao.
     *
     * @param dsMotivoSituacao the ds motivo situacao
     */
    public void setDsMotivoSituacao(String dsMotivoSituacao) {
        this.dsMotivoSituacao = dsMotivoSituacao;
    }

    /**
     * Set: cdSerieDocumento.
     *
     * @param cdSerieDocumento the cd serie documento
     */
    public void setCdSerieDocumento(String cdSerieDocumento) {
        this.cdSerieDocumento = cdSerieDocumento;
    }

    /**
     * Get: dsPagamento.
     *
     * @return dsPagamento
     */
    public String getDsPagamento() {
        return dsPagamento;
    }

    /**
     * Set: dsPagamento.
     *
     * @param dsPagamento the ds pagamento
     */
    public void setDsPagamento(String dsPagamento) {
        this.dsPagamento = dsPagamento;
    }

    /**
     * Get: dsTipoContaFavorecido.
     *
     * @return dsTipoContaFavorecido
     */
    public String getDsTipoContaFavorecido() {
        return dsTipoContaFavorecido;
    }

    /**
     * Set: dsTipoContaFavorecido.
     *
     * @param dsTipoContaFavorecido the ds tipo conta favorecido
     */
    public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
        this.dsTipoContaFavorecido = dsTipoContaFavorecido;
    }

    /**
     * Get: dsTipoServico.
     *
     * @return dsTipoServico
     */
    public String getDsTipoServico() {
        return dsTipoServico;
    }

    /**
     * Set: dsTipoServico.
     *
     * @param dsTipoServico the ds tipo servico
     */
    public void setDsTipoServico(String dsTipoServico) {
        this.dsTipoServico = dsTipoServico;
    }

    /**
     * Get: dsUsoEmpresa.
     *
     * @return dsUsoEmpresa
     */
    public String getDsUsoEmpresa() {
        return dsUsoEmpresa;
    }

    /**
     * Set: dsUsoEmpresa.
     *
     * @param dsUsoEmpresa the ds uso empresa
     */
    public void setDsUsoEmpresa(String dsUsoEmpresa) {
        this.dsUsoEmpresa = dsUsoEmpresa;
    }

    /**
     * Get: dtAgendamento.
     *
     * @return dtAgendamento
     */
    public String getDtAgendamento() {
        return dtAgendamento;
    }

    /**
     * Set: dtAgendamento.
     *
     * @param dtAgendamento the dt agendamento
     */
    public void setDtAgendamento(String dtAgendamento) {
        this.dtAgendamento = dtAgendamento;
    }

    /**
     * Get: dtEmissaoDocumento.
     *
     * @return dtEmissaoDocumento
     */
    public String getDtEmissaoDocumento() {
        return dtEmissaoDocumento;
    }

    /**
     * Set: dtEmissaoDocumento.
     *
     * @param dtEmissaoDocumento the dt emissao documento
     */
    public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
        this.dtEmissaoDocumento = dtEmissaoDocumento;
    }

    /**
     * Get: dtNascimentoBeneficiario.
     *
     * @return dtNascimentoBeneficiario
     */
    public String getDtNascimentoBeneficiario() {
        return dtNascimentoBeneficiario;
    }

    /**
     * Get: nomeBeneficiario.
     *
     * @return nomeBeneficiario
     */
    public String getNomeBeneficiario() {
        return nomeBeneficiario;
    }

    /**
     * Set: nomeBeneficiario.
     *
     * @param nomeBeneficiario the nome beneficiario
     */
    public void setNomeBeneficiario(String nomeBeneficiario) {
        this.nomeBeneficiario = nomeBeneficiario;
    }

    /**
     * Get: numeroInscricaoBeneficiario.
     *
     * @return numeroInscricaoBeneficiario
     */
    public String getNumeroInscricaoBeneficiario() {
        return numeroInscricaoBeneficiario;
    }

    /**
     * Set: numeroInscricaoBeneficiario.
     *
     * @param numeroInscricaoBeneficiario the numero inscricao beneficiario
     */
    public void setNumeroInscricaoBeneficiario(String numeroInscricaoBeneficiario) {
        this.numeroInscricaoBeneficiario = numeroInscricaoBeneficiario;
    }

    /**
     * Get: numeroInscricaoFavorecido.
     *
     * @return numeroInscricaoFavorecido
     */
    public String getNumeroInscricaoFavorecido() {
        return numeroInscricaoFavorecido;
    }

    /**
     * Set: numeroInscricaoFavorecido.
     *
     * @param numeroInscricaoFavorecido the numero inscricao favorecido
     */
    public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
        this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
    }

    /**
     * Get: tipoBeneficiario.
     *
     * @return tipoBeneficiario
     */
    public String getTipoBeneficiario() {
        return tipoBeneficiario;
    }

    /**
     * Set: tipoBeneficiario.
     *
     * @param tipoBeneficiario the tipo beneficiario
     */
    public void setTipoBeneficiario(String tipoBeneficiario) {
        this.tipoBeneficiario = tipoBeneficiario;
    }

    /**
     * Set: dtNascimentoBeneficiario.
     *
     * @param dtNascimentoBeneficiario the dt nascimento beneficiario
     */
    public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
        this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
    }

    /**
     * Get: dtPagamento.
     *
     * @return dtPagamento
     */
    public String getDtPagamento() {
        return dtPagamento;
    }

    /**
     * Set: dtPagamento.
     *
     * @param dtPagamento the dt pagamento
     */
    public void setDtPagamento(String dtPagamento) {
        this.dtPagamento = dtPagamento;
    }

    /**
     * Get: dtVencimento.
     *
     * @return dtVencimento
     */
    public String getDtVencimento() {
        return dtVencimento;
    }

    /**
     * Set: dtVencimento.
     *
     * @param dtVencimento the dt vencimento
     */
    public void setDtVencimento(String dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    /**
     * Get: finalidadeTed.
     *
     * @return finalidadeTed
     */
    public String getFinalidadeTed() {
        return finalidadeTed;
    }

    /**
     * Set: finalidadeTed.
     *
     * @param finalidadeTed the finalidade ted
     */
    public void setFinalidadeTed(String finalidadeTed) {
        this.finalidadeTed = finalidadeTed;
    }

    /**
     * Get: identificacaoTitularidade.
     *
     * @return identificacaoTitularidade
     */
    public String getIdentificacaoTitularidade() {
        return identificacaoTitularidade;
    }

    /**
     * Set: identificacaoTitularidade.
     *
     * @param identificacaoTitularidade the identificacao titularidade
     */
    public void setIdentificacaoTitularidade(String identificacaoTitularidade) {
        this.identificacaoTitularidade = identificacaoTitularidade;
    }

    /**
     * Get: identificacaoTransferenciaTed.
     *
     * @return identificacaoTransferenciaTed
     */
    public String getIdentificacaoTransferenciaTed() {
        return identificacaoTransferenciaTed;
    }

    /**
     * Set: identificacaoTransferenciaTed.
     *
     * @param identificacaoTransferenciaTed the identificacao transferencia ted
     */
    public void setIdentificacaoTransferenciaTed(String identificacaoTransferenciaTed) {
        this.identificacaoTransferenciaTed = identificacaoTransferenciaTed;
    }

    /**
     * Get: identificadorTipoRetirada.
     *
     * @return identificadorTipoRetirada
     */
    public String getIdentificadorTipoRetirada() {
        return identificadorTipoRetirada;
    }

    /**
     * Get: indicadorAssociadoUnicaAgencia.
     *
     * @return indicadorAssociadoUnicaAgencia
     */
    public String getIndicadorAssociadoUnicaAgencia() {
        return indicadorAssociadoUnicaAgencia;
    }

    /**
     * Set: indicadorAssociadoUnicaAgencia.
     *
     * @param indicadorAssociadoUnicaAgencia the indicador associado unica agencia
     */
    public void setIndicadorAssociadoUnicaAgencia(String indicadorAssociadoUnicaAgencia) {
        this.indicadorAssociadoUnicaAgencia = indicadorAssociadoUnicaAgencia;
    }

    /**
     * Set: identificadorRetirada.
     *
     * @param identificadorRetirada the identificador retirada
     */
    public void setIdentificadorRetirada(String identificadorRetirada) {
        this.identificadorRetirada = identificadorRetirada;
    }

    /**
     * Set: identificadorTipoRetirada.
     *
     * @param identificadorTipoRetirada the identificador tipo retirada
     */
    public void setIdentificadorTipoRetirada(String identificadorTipoRetirada) {
        this.identificadorTipoRetirada = identificadorTipoRetirada;
    }

    /**
     * Set: numeroCheque.
     *
     * @param numeroCheque the numero cheque
     */
    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    /**
     * Set: numeroOp.
     *
     * @param numeroOp the numero op
     */
    public void setNumeroOp(String numeroOp) {
        this.numeroOp = numeroOp;
    }

    /**
     * Get: instrucaoPagamento.
     *
     * @return instrucaoPagamento
     */
    public String getInstrucaoPagamento() {
        return instrucaoPagamento;
    }

    /**
     * Set: instrucaoPagamento.
     *
     * @param instrucaoPagamento the instrucao pagamento
     */
    public void setInstrucaoPagamento(String instrucaoPagamento) {
        this.instrucaoPagamento = instrucaoPagamento;
    }

    /**
     * Get: municipioTributo.
     *
     * @return municipioTributo
     */
    public String getMunicipioTributo() {
        return municipioTributo;
    }

    /**
     * Set: municipioTributo.
     *
     * @param municipioTributo the municipio tributo
     */
    public void setMunicipioTributo(String municipioTributo) {
        this.municipioTributo = municipioTributo;
    }

    /**
     * Get: nomeDepositante.
     *
     * @return nomeDepositante
     */
    public String getNomeDepositante() {
        return nomeDepositante;
    }

    /**
     * Set: nomeDepositante.
     *
     * @param nomeDepositante the nome depositante
     */
    public void setNomeDepositante(String nomeDepositante) {
        this.nomeDepositante = nomeDepositante;
    }

    /**
     * Get: nomeOrgaoFavorecido.
     *
     * @return nomeOrgaoFavorecido
     */
    public String getNomeOrgaoFavorecido() {
        return nomeOrgaoFavorecido;
    }

    /**
     * Set: nomeOrgaoFavorecido.
     *
     * @param nomeOrgaoFavorecido the nome orgao favorecido
     */
    public void setNomeOrgaoFavorecido(String nomeOrgaoFavorecido) {
        this.nomeOrgaoFavorecido = nomeOrgaoFavorecido;
    }

    /**
     * Get: nrDocumento.
     *
     * @return nrDocumento
     */
    public String getNrDocumento() {
        return nrDocumento;
    }

    /**
     * Set: nrDocumento.
     *
     * @param nrDocumento the nr documento
     */
    public void setNrDocumento(String nrDocumento) {
        this.nrDocumento = nrDocumento;
    }

    /**
     * Get: nrLoteArquivoRemessa.
     *
     * @return nrLoteArquivoRemessa
     */
    public String getNrLoteArquivoRemessa() {
        return nrLoteArquivoRemessa;
    }

    /**
     * Set: nrLoteArquivoRemessa.
     *
     * @param nrLoteArquivoRemessa the nr lote arquivo remessa
     */
    public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
        this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
    }

    /**
     * Get: nrSequenciaArquivoRemessa.
     *
     * @return nrSequenciaArquivoRemessa
     */
    public String getNrSequenciaArquivoRemessa() {
        return nrSequenciaArquivoRemessa;
    }

    /**
     * Set: nrSequenciaArquivoRemessa.
     *
     * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
     */
    public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
        this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
    }

    /**
     * Get: numeroCotaParcela.
     *
     * @return numeroCotaParcela
     */
    public String getNumeroCotaParcela() {
        return numeroCotaParcela;
    }

    /**
     * Set: numeroCotaParcela.
     *
     * @param numeroCotaParcela the numero cota parcela
     */
    public void setNumeroCotaParcela(String numeroCotaParcela) {
        this.numeroCotaParcela = numeroCotaParcela;
    }

    /**
     * Get: numeroNsu.
     *
     * @return numeroNsu
     */
    public String getNumeroNsu() {
        return numeroNsu;
    }

    /**
     * Set: numeroNsu.
     *
     * @param numeroNsu the numero nsu
     */
    public void setNumeroNsu(String numeroNsu) {
        this.numeroNsu = numeroNsu;
    }

    /**
     * Get: numeroPagamento.
     *
     * @return numeroPagamento
     */
    public String getNumeroPagamento() {
        return numeroPagamento;
    }

    /**
     * Set: numeroPagamento.
     *
     * @param numeroPagamento the numero pagamento
     */
    public void setNumeroPagamento(String numeroPagamento) {
        this.numeroPagamento = numeroPagamento;
    }

    /**
     * Get: observacaoTributo.
     *
     * @return observacaoTributo
     */
    public String getObservacaoTributo() {
        return observacaoTributo;
    }

    /**
     * Set: observacaoTributo.
     *
     * @param observacaoTributo the observacao tributo
     */
    public void setObservacaoTributo(String observacaoTributo) {
        this.observacaoTributo = observacaoTributo;
    }

    /**
     * Get: opcaoRetiradaTributo.
     *
     * @return opcaoRetiradaTributo
     */
    public String getOpcaoRetiradaTributo() {
        return opcaoRetiradaTributo;
    }

    /**
     * Set: opcaoRetiradaTributo.
     *
     * @param opcaoRetiradaTributo the opcao retirada tributo
     */
    public void setOpcaoRetiradaTributo(String opcaoRetiradaTributo) {
        this.opcaoRetiradaTributo = opcaoRetiradaTributo;
    }

    /**
     * Get: opcaoTributo.
     *
     * @return opcaoTributo
     */
    public String getOpcaoTributo() {
        return opcaoTributo;
    }

    /**
     * Set: opcaoTributo.
     *
     * @param opcaoTributo the opcao tributo
     */
    public void setOpcaoTributo(String opcaoTributo) {
        this.opcaoTributo = opcaoTributo;
    }

    /**
     * Get: percentualReceita.
     *
     * @return percentualReceita
     */
    public BigDecimal getPercentualReceita() {
        return percentualReceita;
    }

    /**
     * Set: percentualReceita.
     *
     * @param percentualReceita the percentual receita
     */
    public void setPercentualReceita(BigDecimal percentualReceita) {
        this.percentualReceita = percentualReceita;
    }

    /**
     * Get: periodoApuracao.
     *
     * @return periodoApuracao
     */
    public String getPeriodoApuracao() {
        return periodoApuracao;
    }

    /**
     * Set: periodoApuracao.
     *
     * @param periodoApuracao the periodo apuracao
     */
    public void setPeriodoApuracao(String periodoApuracao) {
        this.periodoApuracao = periodoApuracao;
    }

    /**
     * Get: placaVeiculo.
     *
     * @return placaVeiculo
     */
    public String getPlacaVeiculo() {
        return placaVeiculo;
    }

    /**
     * Set: placaVeiculo.
     *
     * @param placaVeiculo the placa veiculo
     */
    public void setPlacaVeiculo(String placaVeiculo) {
        this.placaVeiculo = placaVeiculo;
    }

    /**
     * Get: produtoDepositoIdentificado.
     *
     * @return produtoDepositoIdentificado
     */
    public String getProdutoDepositoIdentificado() {
        return produtoDepositoIdentificado;
    }

    /**
     * Set: produtoDepositoIdentificado.
     *
     * @param produtoDepositoIdentificado the produto deposito identificado
     */
    public void setProdutoDepositoIdentificado(String produtoDepositoIdentificado) {
        this.produtoDepositoIdentificado = produtoDepositoIdentificado;
    }

    /**
     * Get: qtMoeda.
     *
     * @return qtMoeda
     */
    public BigDecimal getQtMoeda() {
        return qtMoeda;
    }

    /**
     * Set: qtMoeda.
     *
     * @param qtMoeda the qt moeda
     */
    public void setQtMoeda(BigDecimal qtMoeda) {
        this.qtMoeda = qtMoeda;
    }

    /**
     * Get: sequenciaProdutoDepositoIdentificado.
     *
     * @return sequenciaProdutoDepositoIdentificado
     */
    public String getSequenciaProdutoDepositoIdentificado() {
        return sequenciaProdutoDepositoIdentificado;
    }

    /**
     * Set: sequenciaProdutoDepositoIdentificado.
     *
     * @param sequenciaProdutoDepositoIdentificado the sequencia produto deposito identificado
     */
    public void setSequenciaProdutoDepositoIdentificado(String sequenciaProdutoDepositoIdentificado) {
        this.sequenciaProdutoDepositoIdentificado = sequenciaProdutoDepositoIdentificado;
    }

    /**
     * Get: serieCheque.
     *
     * @return serieCheque
     */
    public String getSerieCheque() {
        return serieCheque;
    }

    /**
     * Set: serieCheque.
     *
     * @param serieCheque the serie cheque
     */
    public void setSerieCheque(String serieCheque) {
        this.serieCheque = serieCheque;
    }

    /**
     * Get: tipoContaCredito.
     *
     * @return tipoContaCredito
     */
    public String getTipoContaCredito() {
        return tipoContaCredito;
    }

    /**
     * Set: tipoContaCredito.
     *
     * @param tipoContaCredito the tipo conta credito
     */
    public void setTipoContaCredito(String tipoContaCredito) {
        this.tipoContaCredito = tipoContaCredito;
    }

    /**
     * Get: tipoContaDebito.
     *
     * @return tipoContaDebito
     */
    public String getTipoContaDebito() {
        return tipoContaDebito;
    }

    /**
     * Set: tipoContaDebito.
     *
     * @param tipoContaDebito the tipo conta debito
     */
    public void setTipoContaDebito(String tipoContaDebito) {
        this.tipoContaDebito = tipoContaDebito;
    }

    /**
     * Get: tipoContaDestino.
     *
     * @return tipoContaDestino
     */
    public String getTipoContaDestino() {
        return tipoContaDestino;
    }

    /**
     * Set: tipoContaDestino.
     *
     * @param tipoContaDestino the tipo conta destino
     */
    public void setTipoContaDestino(String tipoContaDestino) {
        this.tipoContaDestino = tipoContaDestino;
    }

    /**
     * Get: tipoDespositoIdentificado.
     *
     * @return tipoDespositoIdentificado
     */
    public String getTipoDespositoIdentificado() {
        return tipoDespositoIdentificado;
    }

    /**
     * Set: tipoDespositoIdentificado.
     *
     * @param tipoDespositoIdentificado the tipo desposito identificado
     */
    public void setTipoDespositoIdentificado(String tipoDespositoIdentificado) {
        this.tipoDespositoIdentificado = tipoDespositoIdentificado;
    }

    /**
     * Get: tipoDocumento.
     *
     * @return tipoDocumento
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Set: tipoDocumento.
     *
     * @param tipoDocumento the tipo documento
     */
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    /**
     * Get: ufTributo.
     *
     * @return ufTributo
     */
    public String getUfTributo() {
        return ufTributo;
    }

    /**
     * Set: ufTributo.
     *
     * @param ufTributo the uf tributo
     */
    public void setUfTributo(String ufTributo) {
        this.ufTributo = ufTributo;
    }

    /**
     * Get: viaCartaoDepositoIdentificado.
     *
     * @return viaCartaoDepositoIdentificado
     */
    public String getViaCartaoDepositoIdentificado() {
        return viaCartaoDepositoIdentificado;
    }

    /**
     * Set: viaCartaoDepositoIdentificado.
     *
     * @param viaCartaoDepositoIdentificado the via cartao deposito identificado
     */
    public void setViaCartaoDepositoIdentificado(String viaCartaoDepositoIdentificado) {
        this.viaCartaoDepositoIdentificado = viaCartaoDepositoIdentificado;
    }

    /**
     * Get: vlAbatimento.
     *
     * @return vlAbatimento
     */
    public BigDecimal getVlAbatimento() {
        return vlAbatimento;
    }

    /**
     * Set: vlAbatimento.
     *
     * @param vlAbatimento the vl abatimento
     */
    public void setVlAbatimento(BigDecimal vlAbatimento) {
        this.vlAbatimento = vlAbatimento;
    }

    /**
     * Get: vlAcrescimo.
     *
     * @return vlAcrescimo
     */
    public BigDecimal getVlAcrescimo() {
        return vlAcrescimo;
    }

    /**
     * Set: vlAcrescimo.
     *
     * @param vlAcrescimo the vl acrescimo
     */
    public void setVlAcrescimo(BigDecimal vlAcrescimo) {
        this.vlAcrescimo = vlAcrescimo;
    }

    /**
     * Get: vlAcrescimoFinanceiro.
     *
     * @return vlAcrescimoFinanceiro
     */
    public BigDecimal getVlAcrescimoFinanceiro() {
        return vlAcrescimoFinanceiro;
    }

    /**
     * Set: vlAcrescimoFinanceiro.
     *
     * @param vlAcrescimoFinanceiro the vl acrescimo financeiro
     */
    public void setVlAcrescimoFinanceiro(BigDecimal vlAcrescimoFinanceiro) {
        this.vlAcrescimoFinanceiro = vlAcrescimoFinanceiro;
    }

    /**
     * Get: vlAtualizacaoMonetaria.
     *
     * @return vlAtualizacaoMonetaria
     */
    public BigDecimal getVlAtualizacaoMonetaria() {
        return vlAtualizacaoMonetaria;
    }

    /**
     * Set: vlAtualizacaoMonetaria.
     *
     * @param vlAtualizacaoMonetaria the vl atualizacao monetaria
     */
    public void setVlAtualizacaoMonetaria(BigDecimal vlAtualizacaoMonetaria) {
        this.vlAtualizacaoMonetaria = vlAtualizacaoMonetaria;
    }

    /**
     * Get: vlDeducao.
     *
     * @return vlDeducao
     */
    public BigDecimal getVlDeducao() {
        return vlDeducao;
    }

    /**
     * Set: vlDeducao.
     *
     * @param vlDeducao the vl deducao
     */
    public void setVlDeducao(BigDecimal vlDeducao) {
        this.vlDeducao = vlDeducao;
    }

    /**
     * Get: vlDesconto.
     *
     * @return vlDesconto
     */
    public BigDecimal getVlDesconto() {
        return vlDesconto;
    }

    /**
     * Set: vlDesconto.
     *
     * @param vlDesconto the vl desconto
     */
    public void setVlDesconto(BigDecimal vlDesconto) {
        this.vlDesconto = vlDesconto;
    }

    /**
     * Get: vlDocumento.
     *
     * @return vlDocumento
     */
    public BigDecimal getVlDocumento() {
        return vlDocumento;
    }

    /**
     * Set: vlDocumento.
     *
     * @param vlDocumento the vl documento
     */
    public void setVlDocumento(BigDecimal vlDocumento) {
        this.vlDocumento = vlDocumento;
    }

    /**
     * Get: vlHonorarioAdvogado.
     *
     * @return vlHonorarioAdvogado
     */
    public BigDecimal getVlHonorarioAdvogado() {
        return vlHonorarioAdvogado;
    }

    /**
     * Set: vlHonorarioAdvogado.
     *
     * @param vlHonorarioAdvogado the vl honorario advogado
     */
    public void setVlHonorarioAdvogado(BigDecimal vlHonorarioAdvogado) {
        this.vlHonorarioAdvogado = vlHonorarioAdvogado;
    }

    /**
     * Get: vlImpostoMovFinanceira.
     *
     * @return vlImpostoMovFinanceira
     */
    public BigDecimal getVlImpostoMovFinanceira() {
        return vlImpostoMovFinanceira;
    }

    /**
     * Set: vlImpostoMovFinanceira.
     *
     * @param vlImpostoMovFinanceira the vl imposto mov financeira
     */
    public void setVlImpostoMovFinanceira(BigDecimal vlImpostoMovFinanceira) {
        this.vlImpostoMovFinanceira = vlImpostoMovFinanceira;
    }

    /**
     * Get: vlImpostoRenda.
     *
     * @return vlImpostoRenda
     */
    public BigDecimal getVlImpostoRenda() {
        return vlImpostoRenda;
    }

    /**
     * Set: vlImpostoRenda.
     *
     * @param vlImpostoRenda the vl imposto renda
     */
    public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
        this.vlImpostoRenda = vlImpostoRenda;
    }

    /**
     * Get: vlInss.
     *
     * @return vlInss
     */
    public BigDecimal getVlInss() {
        return vlInss;
    }

    /**
     * Set: vlInss.
     *
     * @param vlInss the vl inss
     */
    public void setVlInss(BigDecimal vlInss) {
        this.vlInss = vlInss;
    }

    /**
     * Get: vlIof.
     *
     * @return vlIof
     */
    public BigDecimal getVlIof() {
        return vlIof;
    }

    /**
     * Set: vlIof.
     *
     * @param vlIof the vl iof
     */
    public void setVlIof(BigDecimal vlIof) {
        this.vlIof = vlIof;
    }

    /**
     * Get: vlIss.
     *
     * @return vlIss
     */
    public BigDecimal getVlIss() {
        return vlIss;
    }

    /**
     * Set: vlIss.
     *
     * @param vlIss the vl iss
     */
    public void setVlIss(BigDecimal vlIss) {
        this.vlIss = vlIss;
    }

    /**
     * Get: vlMora.
     *
     * @return vlMora
     */
    public BigDecimal getVlMora() {
        return vlMora;
    }

    /**
     * Set: vlMora.
     *
     * @param vlMora the vl mora
     */
    public void setVlMora(BigDecimal vlMora) {
        this.vlMora = vlMora;
    }

    /**
     * Get: vlMulta.
     *
     * @return vlMulta
     */
    public BigDecimal getVlMulta() {
        return vlMulta;
    }

    /**
     * Set: vlMulta.
     *
     * @param vlMulta the vl multa
     */
    public void setVlMulta(BigDecimal vlMulta) {
        this.vlMulta = vlMulta;
    }

    /**
     * Get: vlOutrasEntidades.
     *
     * @return vlOutrasEntidades
     */
    public BigDecimal getVlOutrasEntidades() {
        return vlOutrasEntidades;
    }

    /**
     * Set: vlOutrasEntidades.
     *
     * @param vlOutrasEntidades the vl outras entidades
     */
    public void setVlOutrasEntidades(BigDecimal vlOutrasEntidades) {
        this.vlOutrasEntidades = vlOutrasEntidades;
    }

    /**
     * Get: vlPrincipal.
     *
     * @return vlPrincipal
     */
    public BigDecimal getVlPrincipal() {
        return vlPrincipal;
    }

    /**
     * Set: vlPrincipal.
     *
     * @param vlPrincipal the vl principal
     */
    public void setVlPrincipal(BigDecimal vlPrincipal) {
        this.vlPrincipal = vlPrincipal;
    }

    /**
     * Get: vlrAgendamento.
     *
     * @return vlrAgendamento
     */
    public BigDecimal getVlrAgendamento() {
        return vlrAgendamento;
    }

    /**
     * Set: vlrAgendamento.
     *
     * @param vlrAgendamento the vlr agendamento
     */
    public void setVlrAgendamento(BigDecimal vlrAgendamento) {
        this.vlrAgendamento = vlrAgendamento;
    }

    /**
     * Get: vlReceita.
     *
     * @return vlReceita
     */
    public BigDecimal getVlReceita() {
        return vlReceita;
    }

    /**
     * Set: vlReceita.
     *
     * @param vlReceita the vl receita
     */
    public void setVlReceita(BigDecimal vlReceita) {
        this.vlReceita = vlReceita;
    }

    /**
     * Get: vlrEfetivacao.
     *
     * @return vlrEfetivacao
     */
    public BigDecimal getVlrEfetivacao() {
        return vlrEfetivacao;
    }

    /**
     * Set: vlrEfetivacao.
     *
     * @param vlrEfetivacao the vlr efetivacao
     */
    public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
        this.vlrEfetivacao = vlrEfetivacao;
    }

    /**
     * Get: vlTotal.
     *
     * @return vlTotal
     */
    public BigDecimal getVlTotal() {
        return vlTotal;
    }

    /**
     * Set: vlTotal.
     *
     * @param vlTotal the vl total
     */
    public void setVlTotal(BigDecimal vlTotal) {
        this.vlTotal = vlTotal;
    }

    /**
     * Get: dsTipoManutencao.
     *
     * @return dsTipoManutencao
     */
    public String getDsTipoManutencao() {
        return dsTipoManutencao;
    }

    /**
     * Set: dsTipoManutencao.
     *
     * @param dsTipoManutencao the ds tipo manutencao
     */
    public void setDsTipoManutencao(String dsTipoManutencao) {
        this.dsTipoManutencao = dsTipoManutencao;
    }

    /**
     * Is manutencao.
     *
     * @return true, if is manutencao
     */
    public boolean isManutencao() {
        return manutencao;
    }

    /**
     * Set: manutencao.
     *
     * @param manutencao the manutencao
     */
    public void setManutencao(boolean manutencao) {
        this.manutencao = manutencao;
    }

    /**
     * Get: finalidadeDoc.
     *
     * @return finalidadeDoc
     */
    public String getFinalidadeDoc() {
        return finalidadeDoc;
    }

    /**
     * Set: finalidadeDoc.
     *
     * @param finalidadeDoc the finalidade doc
     */
    public void setFinalidadeDoc(String finalidadeDoc) {
        this.finalidadeDoc = finalidadeDoc;
    }

    /**
     * Get: identificacaoTransferenciaDoc.
     *
     * @return identificacaoTransferenciaDoc
     */
    public String getIdentificacaoTransferenciaDoc() {
        return identificacaoTransferenciaDoc;
    }

    /**
     * Set: identificacaoTransferenciaDoc.
     *
     * @param identificacaoTransferenciaDoc the identificacao transferencia doc
     */
    public void setIdentificacaoTransferenciaDoc(String identificacaoTransferenciaDoc) {
        this.identificacaoTransferenciaDoc = identificacaoTransferenciaDoc;
    }

    /**
     * Get: listaControleRadio.
     *
     * @return listaControleRadio
     */
    public List<SelectItem> getListaControleRadio() {
        return listaControleRadio;
    }

    /**
     * Set: listaControleRadio.
     *
     * @param listaControleRadio the lista controle radio
     */
    public void setListaControleRadio(List<SelectItem> listaControleRadio) {
        this.listaControleRadio = listaControleRadio;
    }

    /**
     * Get: itemSelecionadoLista.
     *
     * @return itemSelecionadoLista
     */
    public Integer getItemSelecionadoLista() {
        return itemSelecionadoLista;
    }

    /**
     * Set: itemSelecionadoLista.
     *
     * @param itemSelecionadoLista the item selecionado lista
     */
    public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
        this.itemSelecionadoLista = itemSelecionadoLista;
    }

    /**
     * Get: listaGridPagamentosIndividuais.
     *
     * @return listaGridPagamentosIndividuais
     */
    public List<ConsultarPagamentosIndividuaisSaidaDTO> getListaGridPagamentosIndividuais() {
        return listaGridPagamentosIndividuais;
    }

    /**
     * Set: listaGridPagamentosIndividuais.
     *
     * @param listaGridPagamentosIndividuais the lista grid pagamentos individuais
     */
    public void setListaGridPagamentosIndividuais(
        List<ConsultarPagamentosIndividuaisSaidaDTO> listaGridPagamentosIndividuais) {
        this.listaGridPagamentosIndividuais = listaGridPagamentosIndividuais;
    }

    /**
     * Get: listaGridPagamentosIndividuaisImprimir.
     *
     * @return listaGridPagamentosIndividuaisImprimir
     */
    public List<ConsultarPagamentosIndividuaisSaidaDTO> getListaGridPagamentosIndividuaisImprimir() {
        return listaGridPagamentosIndividuaisImprimir;
    }

    /**
     * Set: listaGridPagamentosIndividuaisImprimir.
     *
     * @param listaGridPagamentosIndividuaisImprimir the lista grid pagamentos individuais imprimir
     */
    public void setListaGridPagamentosIndividuaisImprimir(
        List<ConsultarPagamentosIndividuaisSaidaDTO> listaGridPagamentosIndividuaisImprimir) {
        this.listaGridPagamentosIndividuaisImprimir = listaGridPagamentosIndividuaisImprimir;
    }

    /**
     * Get: valorPagamentoFormatado.
     *
     * @return valorPagamentoFormatado
     */
    public String getValorPagamentoFormatado() {
        return valorPagamentoFormatado;
    }

    /**
     * Set: valorPagamentoFormatado.
     *
     * @param valorPagamentoFormatado the valor pagamento formatado
     */
    public void setValorPagamentoFormatado(String valorPagamentoFormatado) {
        this.valorPagamentoFormatado = valorPagamentoFormatado;
    }

    /**
     * Get: consultasServiceImpl.
     *
     * @return consultasServiceImpl
     */
    public IConsultasService getConsultasServiceImpl() {
        return consultasServiceImpl;
    }

    /**
     * Set: consultasServiceImpl.
     *
     * @param consultasServiceImpl the consultas service impl
     */
    public void setConsultasServiceImpl(IConsultasService consultasServiceImpl) {
        this.consultasServiceImpl = consultasServiceImpl;
    }

    /**
     * Get: identificadorRetirada.
     *
     * @return identificadorRetirada
     */
    public String getIdentificadorRetirada() {
        return identificadorRetirada;
    }

    /**
     * Get: numeroCheque.
     *
     * @return numeroCheque
     */
    public String getNumeroCheque() {
        return numeroCheque;
    }

    /**
     * Get: numeroOp.
     *
     * @return numeroOp
     */
    public String getNumeroOp() {
        return numeroOp;
    }

    /**
     * Get: contaDebitoGridHist.
     *
     * @return contaDebitoGridHist
     */
    public String getContaDebitoGridHist() {
        return contaDebitoGridHist;
    }

    /**
     * Set: contaDebitoGridHist.
     *
     * @param contaDebitoGridHist the conta debito grid hist
     */
    public void setContaDebitoGridHist(String contaDebitoGridHist) {
        this.contaDebitoGridHist = contaDebitoGridHist;
    }

    /**
     * Get: dsModalidadeGridHist.
     *
     * @return dsModalidadeGridHist
     */
    public String getDsModalidadeGridHist() {
        return dsModalidadeGridHist;
    }

    /**
     * Set: dsModalidadeGridHist.
     *
     * @param dsModalidadeGridHist the ds modalidade grid hist
     */
    public void setDsModalidadeGridHist(String dsModalidadeGridHist) {
        this.dsModalidadeGridHist = dsModalidadeGridHist;
    }

    /**
     * Get: numeroPagamentoGridHist.
     *
     * @return numeroPagamentoGridHist
     */
    public String getNumeroPagamentoGridHist() {
        return numeroPagamentoGridHist;
    }

    /**
     * Set: numeroPagamentoGridHist.
     *
     * @param numeroPagamentoGridHist the numero pagamento grid hist
     */
    public void setNumeroPagamentoGridHist(String numeroPagamentoGridHist) {
        this.numeroPagamentoGridHist = numeroPagamentoGridHist;
    }

    /**
     * Get: tpoServicoGridHist.
     *
     * @return tpoServicoGridHist
     */
    public String getTpoServicoGridHist() {
        return tpoServicoGridHist;
    }

    /**
     * Set: tpoServicoGridHist.
     *
     * @param tpoServicoGridHist the tpo servico grid hist
     */
    public void setTpoServicoGridHist(String tpoServicoGridHist) {
        this.tpoServicoGridHist = tpoServicoGridHist;
    }

    /**
     * Get: dsOrigemPagamento.
     *
     * @return dsOrigemPagamento
     */
    public String getDsOrigemPagamento() {
        return dsOrigemPagamento;
    }

    /**
     * Set: dsOrigemPagamento.
     *
     * @param dsOrigemPagamento the ds origem pagamento
     */
    public void setDsOrigemPagamento(String dsOrigemPagamento) {
        this.dsOrigemPagamento = dsOrigemPagamento;
    }

    /**
     * Get: nossoNumero.
     *
     * @return nossoNumero
     */
    public String getNossoNumero() {
        return nossoNumero;
    }

    /**
     * Set: nossoNumero.
     *
     * @param nossoNumero the nosso numero
     */
    public void setNossoNumero(String nossoNumero) {
        this.nossoNumero = nossoNumero;
    }

    /**
     * Is disable argumentos consulta.
     *
     * @return true, if is disable argumentos consulta
     */
    public boolean isDisableArgumentosConsulta() {
        return disableArgumentosConsulta;
    }

    /**
     * Set: disableArgumentosConsulta.
     *
     * @param disableArgumentosConsulta the disable argumentos consulta
     */
    public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
        this.disableArgumentosConsulta = disableArgumentosConsulta;
    }

    /**
     * Is exibe beneficiario.
     *
     * @return true, if is exibe beneficiario
     */
    public boolean isExibeBeneficiario() {
        return exibeBeneficiario;
    }

    /**
     * Set: exibeBeneficiario.
     *
     * @param exibeBeneficiario the exibe beneficiario
     */
    public void setExibeBeneficiario(boolean exibeBeneficiario) {
        this.exibeBeneficiario = exibeBeneficiario;
    }

    /**
     * Get: cdIndentificadorJudicial.
     *
     * @return cdIndentificadorJudicial
     */
    public String getCdIndentificadorJudicial() {
        return cdIndentificadorJudicial;
    }

    /**
     * Set: cdIndentificadorJudicial.
     *
     * @param cdIndentificadorJudicial the cd indentificador judicial
     */
    public void setCdIndentificadorJudicial(String cdIndentificadorJudicial) {
        this.cdIndentificadorJudicial = cdIndentificadorJudicial;
    }

    /**
     * Get: carteira.
     *
     * @return carteira
     */
    public String getCarteira() {
        return carteira;
    }

    /**
     * Set: carteira.
     *
     * @param carteira the carteira
     */
    public void setCarteira(String carteira) {
        this.carteira = carteira;
    }

    /**
     * Get: dtLimiteDescontoPagamento.
     *
     * @return dtLimiteDescontoPagamento
     */
    public String getDtLimiteDescontoPagamento() {
        return dtLimiteDescontoPagamento;
    }

    /**
     * Set: dtLimiteDescontoPagamento.
     *
     * @param dtLimiteDescontoPagamento the dt limite desconto pagamento
     */
    public void setDtLimiteDescontoPagamento(String dtLimiteDescontoPagamento) {
        this.dtLimiteDescontoPagamento = dtLimiteDescontoPagamento;
    }

    /**
     * Get: cdSituacaoTranferenciaAutomatica.
     *
     * @return cdSituacaoTranferenciaAutomatica
     */
    public String getCdSituacaoTranferenciaAutomatica() {
        return cdSituacaoTranferenciaAutomatica;
    }

    /**
     * Set: cdSituacaoTranferenciaAutomatica.
     *
     * @param cdSituacaoTranferenciaAutomatica the cd situacao tranferencia automatica
     */
    public void setCdSituacaoTranferenciaAutomatica(String cdSituacaoTranferenciaAutomatica) {
        this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
    }

    /**
     * Get: motivoPagamento.
     *
     * @return motivoPagamento
     */
    public String getMotivoPagamento() {
        return motivoPagamento;
    }

    /**
     * Set: motivoPagamento.
     *
     * @param motivoPagamento the motivo pagamento
     */
    public void setMotivoPagamento(String motivoPagamento) {
        this.motivoPagamento = motivoPagamento;
    }

    /**
     * Get: situacaoPagamento.
     *
     * @return situacaoPagamento
     */
    public String getSituacaoPagamento() {
        return situacaoPagamento;
    }

    /**
     * Set: situacaoPagamento.
     *
     * @param situacaoPagamento the situacao pagamento
     */
    public void setSituacaoPagamento(String situacaoPagamento) {
        this.situacaoPagamento = situacaoPagamento;
    }

    /**
     * Get: cdTipoInscricaoFavorecido.
     *
     * @return cdTipoInscricaoFavorecido
     */
    public String getCdTipoInscricaoFavorecido() {
        return cdTipoInscricaoFavorecido;
    }

    /**
     * Set: cdTipoInscricaoFavorecido.
     *
     * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
     */
    public void setCdTipoInscricaoFavorecido(String cdTipoInscricaoFavorecido) {
        this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
    }

    /**
     * Get: inscricaoFavorecido.
     *
     * @return inscricaoFavorecido
     */
    public String getInscricaoFavorecido() {
        return inscricaoFavorecido;
    }

    /**
     * Set: inscricaoFavorecido.
     *
     * @param inscricaoFavorecido the inscricao favorecido
     */
    public void setInscricaoFavorecido(String inscricaoFavorecido) {
        this.inscricaoFavorecido = inscricaoFavorecido;
    }

    /**
     * Get: agenciaFavorecido.
     *
     * @return agenciaFavorecido
     */
    public String getAgenciaFavorecido() {
        return agenciaFavorecido;
    }

    /**
     * Set: agenciaFavorecido.
     *
     * @param agenciaFavorecido the agencia favorecido
     */
    public void setAgenciaFavorecido(String agenciaFavorecido) {
        this.agenciaFavorecido = agenciaFavorecido;
    }

    /**
     * Get: bancoFavorecido.
     *
     * @return bancoFavorecido
     */
    public String getBancoFavorecido() {
        return bancoFavorecido;
    }

    /**
     * Set: bancoFavorecido.
     *
     * @param bancoFavorecido the banco favorecido
     */
    public void setBancoFavorecido(String bancoFavorecido) {
        this.bancoFavorecido = bancoFavorecido;
    }

    /**
     * Get: contaFavorecido.
     *
     * @return contaFavorecido
     */
    public String getContaFavorecido() {
        return contaFavorecido;
    }

    /**
     * Set: contaFavorecido.
     *
     * @param contaFavorecido the conta favorecido
     */
    public void setContaFavorecido(String contaFavorecido) {
        this.contaFavorecido = contaFavorecido;
    }

    /**
     * Get: tipoContaFavorecido.
     *
     * @return tipoContaFavorecido
     */
    public String getTipoContaFavorecido() {
        return tipoContaFavorecido;
    }

    /**
     * Set: tipoContaFavorecido.
     *
     * @param tipoContaFavorecido the tipo conta favorecido
     */
    public void setTipoContaFavorecido(String tipoContaFavorecido) {
        this.tipoContaFavorecido = tipoContaFavorecido;
    }

    /**
     * Get: descTipoFavorecido.
     *
     * @return descTipoFavorecido
     */
    public String getDescTipoFavorecido() {
        return descTipoFavorecido;
    }

    /**
     * Set: descTipoFavorecido.
     *
     * @param descTipoFavorecido the desc tipo favorecido
     */
    public void setDescTipoFavorecido(String descTipoFavorecido) {
        this.descTipoFavorecido = descTipoFavorecido;
    }

    /**
     * Get: dsTipoBeneficiario.
     *
     * @return dsTipoBeneficiario
     */
    public String getDsTipoBeneficiario() {
        return dsTipoBeneficiario;
    }

    /**
     * Set: dsTipoBeneficiario.
     *
     * @param dsTipoBeneficiario the ds tipo beneficiario
     */
    public void setDsTipoBeneficiario(String dsTipoBeneficiario) {
        this.dsTipoBeneficiario = dsTipoBeneficiario;
    }

    /**
     * Get: digAgenciaDebito.
     *
     * @return digAgenciaDebito
     */
    public Integer getDigAgenciaDebito() {
        return digAgenciaDebito;
    }

    /**
     * Set: digAgenciaDebito.
     *
     * @param digAgenciaDebito the dig agencia debito
     */
    public void setDigAgenciaDebito(Integer digAgenciaDebito) {
        this.digAgenciaDebito = digAgenciaDebito;
    }

    /**
     * Get: registroListaPagIndividuaisSelecionado.
     *
     * @return registroListaPagIndividuaisSelecionado
     */
    public ConsultarPagamentosIndividuaisSaidaDTO getRegistroListaPagIndividuaisSelecionado() {
        return registroListaPagIndividuaisSelecionado;
    }

    /**
     * Set: registroListaPagIndividuaisSelecionado.
     *
     * @param registroListaPagIndividuaisSelecionado the registro lista pag individuais selecionado
     */
    public void setRegistroListaPagIndividuaisSelecionado(
        ConsultarPagamentosIndividuaisSaidaDTO registroListaPagIndividuaisSelecionado) {
        this.registroListaPagIndividuaisSelecionado = registroListaPagIndividuaisSelecionado;
    }

    /**
     * Get: imprimirService.
     *
     * @return imprimirService
     */
    public IImprimirService getImprimirService() {
        return imprimirService;
    }

    /**
     * Set: imprimirService.
     *
     * @param imprimirService the imprimir service
     */
    public void setImprimirService(IImprimirService imprimirService) {
        this.imprimirService = imprimirService;
    }

    /**
     * Get: saidaIndividualDTO.
     *
     * @return saidaIndividualDTO
     */
    public DetalharPagtoCreditoContaSaidaDTO getSaidaIndividualDTO() {
        return saidaIndividualDTO;
    }

    /**
     * Set: saidaIndividualDTO.
     *
     * @param saidaIndividualDTO the saida individual dto
     */
    public void setSaidaIndividualDTO(DetalharPagtoCreditoContaSaidaDTO saidaIndividualDTO) {
        this.saidaIndividualDTO = saidaIndividualDTO;
    }

    /**
     * Get: saidaDebitoContaDTO.
     *
     * @return saidaDebitoContaDTO
     */
    public DetalharManPagtoDebitoContaSaidaDTO getSaidaDebitoContaDTO() {
        return saidaDebitoContaDTO;
    }

    /**
     * Set: saidaDebitoContaDTO.
     *
     * @param saidaDebitoContaDTO the saida debito conta dto
     */
    public void setSaidaDebitoContaDTO(DetalharManPagtoDebitoContaSaidaDTO saidaDebitoContaDTO) {
        this.saidaDebitoContaDTO = saidaDebitoContaDTO;
    }

    /**
     * Get: saidaPagtoTedDTO.
     *
     * @return saidaPagtoTedDTO
     */
    public DetalharManPagtoTEDSaidaDTO getSaidaPagtoTedDTO() {
        return saidaPagtoTedDTO;
    }

    /**
     * Set: saidaPagtoTedDTO.
     *
     * @param saidaPagtoTedDTO the saida pagto ted dto
     */
    public void setSaidaPagtoTedDTO(DetalharManPagtoTEDSaidaDTO saidaPagtoTedDTO) {
        this.saidaPagtoTedDTO = saidaPagtoTedDTO;
    }

    /**
     * Get: saidaPagtoDocDTO.
     *
     * @return saidaPagtoDocDTO
     */
    public DetalharManPagtoDOCSaidaDTO getSaidaPagtoDocDTO() {
        return saidaPagtoDocDTO;
    }

    /**
     * Set: saidaPagtoDocDTO.
     *
     * @param saidaPagtoDocDTO the saida pagto doc dto
     */
    public void setSaidaPagtoDocDTO(DetalharManPagtoDOCSaidaDTO saidaPagtoDocDTO) {
        this.saidaPagtoDocDTO = saidaPagtoDocDTO;
    }

    /**
     * Get: saidaOrdemPgtoDTO.
     *
     * @return saidaOrdemPgtoDTO
     */
    public DetalharManPagtoOrdemPagtoSaidaDTO getSaidaOrdemPgtoDTO() {
        return saidaOrdemPgtoDTO;
    }

    /**
     * Set: saidaOrdemPgtoDTO.
     *
     * @param saidaOrdemPgtoDTO the saida ordem pgto dto
     */
    public void setSaidaOrdemPgtoDTO(DetalharManPagtoOrdemPagtoSaidaDTO saidaOrdemPgtoDTO) {
        this.saidaOrdemPgtoDTO = saidaOrdemPgtoDTO;
    }

    /**
     * Get: saidaPagtoTituloBardescoDTO.
     *
     * @return saidaPagtoTituloBardescoDTO
     */
    public DetalharManPagtoTituloBradescoSaidaDTO getSaidaPagtoTituloBardescoDTO() {
        return saidaPagtoTituloBardescoDTO;
    }

    /**
     * Set: saidaPagtoTituloBardescoDTO.
     *
     * @param saidaPagtoTituloBardescoDTO the saida pagto titulo bardesco dto
     */
    public void setSaidaPagtoTituloBardescoDTO(DetalharManPagtoTituloBradescoSaidaDTO saidaPagtoTituloBardescoDTO) {
        this.saidaPagtoTituloBardescoDTO = saidaPagtoTituloBardescoDTO;
    }

    /**
     * Get: saidaTituloOutrosBancosDTO.
     *
     * @return saidaTituloOutrosBancosDTO
     */
    public DetalharManPagtoTituloOutrosBancosSaidaDTO getSaidaTituloOutrosBancosDTO() {
        return saidaTituloOutrosBancosDTO;
    }

    /**
     * Set: saidaTituloOutrosBancosDTO.
     *
     * @param saidaTituloOutrosBancosDTO the saida titulo outros bancos dto
     */
    public void setSaidaTituloOutrosBancosDTO(DetalharManPagtoTituloOutrosBancosSaidaDTO saidaTituloOutrosBancosDTO) {
        this.saidaTituloOutrosBancosDTO = saidaTituloOutrosBancosDTO;
    }

    /**
     * Get: saidaPagtoDarfDTO.
     *
     * @return saidaPagtoDarfDTO
     */
    public DetalharManPagtoDARFSaidaDTO getSaidaPagtoDarfDTO() {
        return saidaPagtoDarfDTO;
    }

    /**
     * Set: saidaPagtoDarfDTO.
     *
     * @param saidaPagtoDarfDTO the saida pagto darf dto
     */
    public void setSaidaPagtoDarfDTO(DetalharManPagtoDARFSaidaDTO saidaPagtoDarfDTO) {
        this.saidaPagtoDarfDTO = saidaPagtoDarfDTO;
    }

    /**
     * Get: saidaPagtoGareDTO.
     *
     * @return saidaPagtoGareDTO
     */
    public DetalharManPagtoGARESaidaDTO getSaidaPagtoGareDTO() {
        return saidaPagtoGareDTO;
    }

    /**
     * Set: saidaPagtoGareDTO.
     *
     * @param saidaPagtoGareDTO the saida pagto gare dto
     */
    public void setSaidaPagtoGareDTO(DetalharManPagtoGARESaidaDTO saidaPagtoGareDTO) {
        this.saidaPagtoGareDTO = saidaPagtoGareDTO;
    }

    /**
     * Get: saidaPagtoGpsDTO.
     *
     * @return saidaPagtoGpsDTO
     */
    public DetalharManPagtoGPSSaidaDTO getSaidaPagtoGpsDTO() {
        return saidaPagtoGpsDTO;
    }

    /**
     * Set: saidaPagtoGpsDTO.
     *
     * @param saidaPagtoGpsDTO the saida pagto gps dto
     */
    public void setSaidaPagtoGpsDTO(DetalharManPagtoGPSSaidaDTO saidaPagtoGpsDTO) {
        this.saidaPagtoGpsDTO = saidaPagtoGpsDTO;
    }

    /**
     * Get: saidaPagtoCodigoBarrasDTO.
     *
     * @return saidaPagtoCodigoBarrasDTO
     */
    public DetalharManPagtoCodigoBarrasSaidaDTO getSaidaPagtoCodigoBarrasDTO() {
        return saidaPagtoCodigoBarrasDTO;
    }

    /**
     * Set: saidaPagtoCodigoBarrasDTO.
     *
     * @param saidaPagtoCodigoBarrasDTO the saida pagto codigo barras dto
     */
    public void setSaidaPagtoCodigoBarrasDTO(DetalharManPagtoCodigoBarrasSaidaDTO saidaPagtoCodigoBarrasDTO) {
        this.saidaPagtoCodigoBarrasDTO = saidaPagtoCodigoBarrasDTO;
    }

    /**
     * Get: saidaPagtoDebitoVeiculosDTO.
     *
     * @return saidaPagtoDebitoVeiculosDTO
     */
    public DetalharManPagtoDebitoVeiculosSaidaDTO getSaidaPagtoDebitoVeiculosDTO() {
        return saidaPagtoDebitoVeiculosDTO;
    }

    /**
     * Set: saidaPagtoDebitoVeiculosDTO.
     *
     * @param saidaPagtoDebitoVeiculosDTO the saida pagto debito veiculos dto
     */
    public void setSaidaPagtoDebitoVeiculosDTO(DetalharManPagtoDebitoVeiculosSaidaDTO saidaPagtoDebitoVeiculosDTO) {
        this.saidaPagtoDebitoVeiculosDTO = saidaPagtoDebitoVeiculosDTO;
    }

    /**
     * Get: saidaPagtoDepIdentificadoDTO.
     *
     * @return saidaPagtoDepIdentificadoDTO
     */
    public DetalharManPagtoDepIdentificadoSaidaDTO getSaidaPagtoDepIdentificadoDTO() {
        return saidaPagtoDepIdentificadoDTO;
    }

    /**
     * Set: saidaPagtoDepIdentificadoDTO.
     *
     * @param saidaPagtoDepIdentificadoDTO the saida pagto dep identificado dto
     */
    public void setSaidaPagtoDepIdentificadoDTO(DetalharManPagtoDepIdentificadoSaidaDTO saidaPagtoDepIdentificadoDTO) {
        this.saidaPagtoDepIdentificadoDTO = saidaPagtoDepIdentificadoDTO;
    }

    /**
     * Get: saidaPagtoOrdemCreditoDTO.
     *
     * @return saidaPagtoOrdemCreditoDTO
     */
    public DetalharManPagtoOrdemCreditoSaidaDTO getSaidaPagtoOrdemCreditoDTO() {
        return saidaPagtoOrdemCreditoDTO;
    }

    /**
     * Set: saidaPagtoOrdemCreditoDTO.
     *
     * @param saidaPagtoOrdemCreditoDTO the saida pagto ordem credito dto
     */
    public void setSaidaPagtoOrdemCreditoDTO(DetalharManPagtoOrdemCreditoSaidaDTO saidaPagtoOrdemCreditoDTO) {
        this.saidaPagtoOrdemCreditoDTO = saidaPagtoOrdemCreditoDTO;
    }

    /**
     * Get: dtPagamentoGare.
     *
     * @return dtPagamentoGare
     */
    public Date getDtPagamentoGare() {
        return dtPagamentoGare;
    }

    /**
     * Set: dtPagamentoGare.
     *
     * @param dtPagamentoGare the dt pagamento gare
     */
    public void setDtPagamentoGare(Date dtPagamentoGare) {
        this.dtPagamentoGare = dtPagamentoGare;
    }

    /**
     * Get: dtPagamentoManutencao.
     *
     * @return dtPagamentoManutencao
     */
    public Date getDtPagamentoManutencao() {
        return dtPagamentoManutencao;
    }

    /**
     * Set: dtPagamentoManutencao.
     *
     * @param dtPagamentoManutencao the dt pagamento manutencao
     */
    public void setDtPagamentoManutencao(Date dtPagamentoManutencao) {
        this.dtPagamentoManutencao = dtPagamentoManutencao;
    }

    /**
     * Get: cdBancoOriginal.
     *
     * @return cdBancoOriginal
     */
    public Integer getCdBancoOriginal() {
        return cdBancoOriginal;
    }

    /**
     * Set: cdBancoOriginal.
     *
     * @param cdBancoOriginal the cd banco original
     */
    public void setCdBancoOriginal(Integer cdBancoOriginal) {
        this.cdBancoOriginal = cdBancoOriginal;
    }

    /**
     * Get: dsBancoOriginal.
     *
     * @return dsBancoOriginal
     */
    public String getDsBancoOriginal() {
        return dsBancoOriginal;
    }

    /**
     * Set: dsBancoOriginal.
     *
     * @param dsBancoOriginal the ds banco original
     */
    public void setDsBancoOriginal(String dsBancoOriginal) {
        this.dsBancoOriginal = dsBancoOriginal;
    }

    /**
     * Get: cdAgenciaBancariaOriginal.
     *
     * @return cdAgenciaBancariaOriginal
     */
    public Integer getCdAgenciaBancariaOriginal() {
        return cdAgenciaBancariaOriginal;
    }

    /**
     * Set: cdAgenciaBancariaOriginal.
     *
     * @param cdAgenciaBancariaOriginal the cd agencia bancaria original
     */
    public void setCdAgenciaBancariaOriginal(Integer cdAgenciaBancariaOriginal) {
        this.cdAgenciaBancariaOriginal = cdAgenciaBancariaOriginal;
    }

    /**
     * Get: cdDigitoAgenciaOriginal.
     *
     * @return cdDigitoAgenciaOriginal
     */
    public String getCdDigitoAgenciaOriginal() {
        return cdDigitoAgenciaOriginal;
    }

    /**
     * Set: cdDigitoAgenciaOriginal.
     *
     * @param cdDigitoAgenciaOriginal the cd digito agencia original
     */
    public void setCdDigitoAgenciaOriginal(String cdDigitoAgenciaOriginal) {
        this.cdDigitoAgenciaOriginal = cdDigitoAgenciaOriginal;
    }

    /**
     * Get: dsAgenciaOriginal.
     *
     * @return dsAgenciaOriginal
     */
    public String getDsAgenciaOriginal() {
        return dsAgenciaOriginal;
    }

    /**
     * Set: dsAgenciaOriginal.
     *
     * @param dsAgenciaOriginal the ds agencia original
     */
    public void setDsAgenciaOriginal(String dsAgenciaOriginal) {
        this.dsAgenciaOriginal = dsAgenciaOriginal;
    }

    /**
     * Get: cdContaBancariaOriginal.
     *
     * @return cdContaBancariaOriginal
     */
    public Long getCdContaBancariaOriginal() {
        return cdContaBancariaOriginal;
    }

    /**
     * Set: cdContaBancariaOriginal.
     *
     * @param cdContaBancariaOriginal the cd conta bancaria original
     */
    public void setCdContaBancariaOriginal(Long cdContaBancariaOriginal) {
        this.cdContaBancariaOriginal = cdContaBancariaOriginal;
    }

    /**
     * Get: cdDigitoContaOriginal.
     *
     * @return cdDigitoContaOriginal
     */
    public String getCdDigitoContaOriginal() {
        return cdDigitoContaOriginal;
    }

    /**
     * Set: cdDigitoContaOriginal.
     *
     * @param cdDigitoContaOriginal the cd digito conta original
     */
    public void setCdDigitoContaOriginal(String cdDigitoContaOriginal) {
        this.cdDigitoContaOriginal = cdDigitoContaOriginal;
    }

    /**
     * Get: dsTipoContaOriginal.
     *
     * @return dsTipoContaOriginal
     */
    public String getDsTipoContaOriginal() {
        return dsTipoContaOriginal;
    }

    /**
     * Set: dsTipoContaOriginal.
     *
     * @param dsTipoContaOriginal the ds tipo conta original
     */
    public void setDsTipoContaOriginal(String dsTipoContaOriginal) {
        this.dsTipoContaOriginal = dsTipoContaOriginal;
    }

    /**
     * Get: dtDevolucaoEstorno.
     *
     * @return dtDevolucaoEstorno
     */
    public String getDtDevolucaoEstorno() {
        return dtDevolucaoEstorno;
    }

    /**
     * Set: dtDevolucaoEstorno.
     *
     * @param dtDevolucaoEstorno the dt devolucao estorno
     */
    public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
        this.dtDevolucaoEstorno = dtDevolucaoEstorno;
    }

    /**
     * Get: sinalMomentoLancamento.
     *
     * @return sinalMomentoLancamento
     */
    public String getSinalMomentoLancamento() {
        return sinalMomentoLancamento;
    }

    /**
     * Set: sinalMomentoLancamento.
     *
     * @param sinalMomentoLancamento the sinal momento lancamento
     */
    public void setSinalMomentoLancamento(String sinalMomentoLancamento) {
        this.sinalMomentoLancamento = sinalMomentoLancamento;
    }

    /**
     * Get: dtLimitePagamento.
     *
     * @return dtLimitePagamento
     */
    public String getDtLimitePagamento() {
        return dtLimitePagamento;
    }

    /**
     * Set: dtLimitePagamento.
     *
     * @param dtLimitePagamento the dt limite pagamento
     */
    public void setDtLimitePagamento(String dtLimitePagamento) {
        this.dtLimitePagamento = dtLimitePagamento;
    }

    /**
     * Get: cdRastreado.
     *
     * @return cdRastreado
     */
    public Integer getCdRastreado() {
        return cdRastreado;
    }

    /**
     * Set: cdRastreado.
     *
     * @param cdRastreado the cd rastreado
     */
    public void setCdRastreado(Integer cdRastreado) {
        this.cdRastreado = cdRastreado;
    }

    /**
     * Get: dsRastreado.
     *
     * @return dsRastreado
     */
    public String getDsRastreado() {
        return dsRastreado;
    }

    /**
     * Set: dsRastreado.
     *
     * @param dsRastreado the ds rastreado
     */
    public void setDsRastreado(String dsRastreado) {
        this.dsRastreado = dsRastreado;
    }

    /**
     * Get: dtFloatingPagamento.
     *
     * @return dtFloatingPagamento
     */
    public String getDtFloatingPagamento() {
        return dtFloatingPagamento;
    }

    /**
     * Set: dtFloatingPagamento.
     *
     * @param dtFloatingPagamento the dt floating pagamento
     */
    public void setDtFloatingPagamento(String dtFloatingPagamento) {
        this.dtFloatingPagamento = dtFloatingPagamento;
    }

    /**
     * Get: dtEfetivFloatPgto.
     *
     * @return dtEfetivFloatPgto
     */
    public String getDtEfetivFloatPgto() {
        return dtEfetivFloatPgto;
    }

    /**
     * Set: dtEfetivFloatPgto.
     *
     * @param dtEfetivFloatPgto the dt efetiv float pgto
     */
    public void setDtEfetivFloatPgto(String dtEfetivFloatPgto) {
        this.dtEfetivFloatPgto = dtEfetivFloatPgto;
    }

    /**
     * Get: vlFloatingPagamento.
     *
     * @return vlFloatingPagamento
     */
    public BigDecimal getVlFloatingPagamento() {
        return vlFloatingPagamento;
    }

    /**
     * Set: vlFloatingPagamento.
     *
     * @param vlFloatingPagamento the vl floating pagamento
     */
    public void setVlFloatingPagamento(BigDecimal vlFloatingPagamento) {
        this.vlFloatingPagamento = vlFloatingPagamento;
    }

    /**
     * Get: cdOperacaoDcom.
     *
     * @return cdOperacaoDcom
     */
    public Long getCdOperacaoDcom() {
        return cdOperacaoDcom;
    }

    /**
     * Set: cdOperacaoDcom.
     *
     * @param cdOperacaoDcom the cd operacao dcom
     */
    public void setCdOperacaoDcom(Long cdOperacaoDcom) {
        this.cdOperacaoDcom = cdOperacaoDcom;
    }

    /**
     * Get: dsSituacaoDcom.
     *
     * @return dsSituacaoDcom
     */
    public String getDsSituacaoDcom() {
        return dsSituacaoDcom;
    }

    /**
     * Set: dsSituacaoDcom.
     *
     * @param dsSituacaoDcom the ds situacao dcom
     */
    public void setDsSituacaoDcom(String dsSituacaoDcom) {
        this.dsSituacaoDcom = dsSituacaoDcom;
    }

    /**
     * Get: exibeDcom.
     *
     * @return exibeDcom
     */
    public Boolean getExibeDcom() {
        return exibeDcom;
    }

    /**
     * Set: exibeDcom.
     *
     * @param exibeDcom the exibe dcom
     */
    public void setExibeDcom(Boolean exibeDcom) {
        this.exibeDcom = exibeDcom;
    }

    /**
     * Get: habilitaImprimirComprovante.
     *
     * @return habilitaImprimirComprovante
     */
    public Boolean getHabilitaImprimirComprovante() {
        return habilitaImprimirComprovante;
    }

    /**
     * Set: habilitaImprimirComprovante.
     *
     * @param habilitaImprimirComprovante the habilita imprimir comprovante
     */
    public void setHabilitaImprimirComprovante(Boolean habilitaImprimirComprovante) {
        this.habilitaImprimirComprovante = habilitaImprimirComprovante;
    }

    /**
     * Get: vlBonfcao.
     *
     * @return vlBonfcao
     */
    public BigDecimal getVlBonfcao() {
        return vlBonfcao;
    }

    /**
     * Set: vlBonfcao.
     *
     * @param vlBonfcao the vl bonfcao
     */
    public void setVlBonfcao(BigDecimal vlBonfcao) {
        this.vlBonfcao = vlBonfcao;
    }

    /**
     * Get: numControleInternoLote.
     *
     * @return numControleInternoLote
     */
    public Long getNumControleInternoLote() {
        return numControleInternoLote;
    }

    /**
     * Set: numControleInternoLote.
     *
     * @param numControleInternoLote the num controle interno lote
     */
    public void setNumControleInternoLote(Long numControleInternoLote) {
        this.numControleInternoLote = numControleInternoLote;
    }

    /**
     * Get: nrLote.
     *
     * @return nrLote
     */
    public String getNrLote() {
        return nrLote;
    }

    /**
     * Set: nrLote.
     *
     * @param nrLote the nr lote
     */
    public void setNrLote(String nrLote) {
        this.nrLote = nrLote;
    }

    /**
     * Get: vlBonificacao.
     *
     * @return vlBonificacao
     */
    public BigDecimal getVlBonificacao() {
        return vlBonificacao;
    }

    /**
     * Set: vlBonificacao.
     *
     * @param vlBonificacao the vl bonificacao
     */
    public void setVlBonificacao(BigDecimal vlBonificacao) {
        this.vlBonificacao = vlBonificacao;
    }

    /**
     * Get: cdFuncEmissaoOp.
     *
     * @return cdFuncEmissaoOp
     */
    public String getCdFuncEmissaoOp() {
        return cdFuncEmissaoOp;
    }

    /**
     * Set: cdFuncEmissaoOp.
     *
     * @param cdFuncEmissaoOp the cd func emissao op
     */
    public void setCdFuncEmissaoOp(String cdFuncEmissaoOp) {
        this.cdFuncEmissaoOp = cdFuncEmissaoOp;
    }

    /**
     * Get: cdTerminalCaixaEmissaoOp.
     *
     * @return cdTerminalCaixaEmissaoOp
     */
    public String getCdTerminalCaixaEmissaoOp() {
        return cdTerminalCaixaEmissaoOp;
    }

    /**
     * Set: cdTerminalCaixaEmissaoOp.
     *
     * @param cdTerminalCaixaEmissaoOp the cd terminal caixa emissao op
     */
    public void setCdTerminalCaixaEmissaoOp(String cdTerminalCaixaEmissaoOp) {
        this.cdTerminalCaixaEmissaoOp = cdTerminalCaixaEmissaoOp;
    }

    /**
     * Get: dtEmissaoOp.
     *
     * @return dtEmissaoOp
     */
    public String getDtEmissaoOp() {
        return dtEmissaoOp;
    }

    /**
     * Set: dtEmissaoOp.
     *
     * @param dtEmissaoOp the dt emissao op
     */
    public void setDtEmissaoOp(String dtEmissaoOp) {
        this.dtEmissaoOp = dtEmissaoOp;
    }

    /**
     * Get: hrEmissaoOp.
     *
     * @return hrEmissaoOp
     */
    public String getHrEmissaoOp() {
        return hrEmissaoOp;
    }

    /**
     * Set: hrEmissaoOp.
     *
     * @param hrEmissaoOp the hr emissao op
     */
    public void setHrEmissaoOp(String hrEmissaoOp) {
        this.hrEmissaoOp = hrEmissaoOp;
    }

    /**
     * Get: tipoTela.
     *
     * @return tipoTela
     */
    public Integer getTipoTela() {
        return tipoTela;
    }

    /**
     * Set: tipoTela.
     *
     * @param tipoTela the tipo tela
     */
    public void setTipoTela(Integer tipoTela) {
        this.tipoTela = tipoTela;
    }

    /**
     * Get: manterClientesOrgaoPublicoFederalBean.
     *
     * @return manterClientesOrgaoPublicoFederalBean
     */
    public ManterClientesOrgaoPublicoFederalBean getManterClientesOrgaoPublicoFederalBean() {
        return manterClientesOrgaoPublicoFederalBean;
    }

    /**
     * Set: manterClientesOrgaoPublicoFederalBean.
     *
     * @param manterClientesOrgaoPublicoFederalBean the manter clientes orgao publico federal bean
     */
    public void setManterClientesOrgaoPublicoFederalBean(
        ManterClientesOrgaoPublicoFederalBean manterClientesOrgaoPublicoFederalBean) {
        this.manterClientesOrgaoPublicoFederalBean = manterClientesOrgaoPublicoFederalBean;
    }

    /**
     * Get: dsEstornoPagamento.
     *
     * @return dsEstornoPagamento
     */
    public String getDsEstornoPagamento() {
        return dsEstornoPagamento;
    }

    /**
     * Set: dsEstornoPagamento.
     *
     * @param dsEstornoPagamento the ds estorno pagamento
     */
    public void setDsEstornoPagamento(String dsEstornoPagamento) {
        this.dsEstornoPagamento = dsEstornoPagamento;
    }

    /**
     * Get: vlEfetivacaoDebitoPagamento.
     *
     * @return the vlEfetivacaoDebitoPagamento
     */
    public BigDecimal getVlEfetivacaoDebitoPagamento() {
        return vlEfetivacaoDebitoPagamento;
    }

    /**
     * Set: vlEfetivacaoDebitoPagamento.
     *
     * @param vlEfetivacaoDebitoPagamento the vlEfetivacaoDebitoPagamento to set
     */
    public void setVlEfetivacaoDebitoPagamento(
        BigDecimal vlEfetivacaoDebitoPagamento) {
        this.vlEfetivacaoDebitoPagamento = vlEfetivacaoDebitoPagamento;
    }

    /**
     * Get: vlEfetivacaoCreditoPagamento.
     *
     * @return the vlEfetivacaoCreditoPagamento
     */
    public BigDecimal getVlEfetivacaoCreditoPagamento() {
        return vlEfetivacaoCreditoPagamento;
    }

    /**
     * Set: vlEfetivacaoCreditoPagamento.
     *
     * @param vlEfetivacaoCreditoPagamento the vlEfetivacaoCreditoPagamento to set
     */
    public void setVlEfetivacaoCreditoPagamento(
        BigDecimal vlEfetivacaoCreditoPagamento) {
        this.vlEfetivacaoCreditoPagamento = vlEfetivacaoCreditoPagamento;
    }

    /**
     * Get: vlDescontoPagamento.
     *
     * @return the vlDescontoPagamento
     */
    public BigDecimal getVlDescontoPagamento() {
        return vlDescontoPagamento;
    }

    /**
     * Set: vlDescontoPagamento.
     *
     * @param vlDescontoPagamento the vlDescontoPagamento to set
     */
    public void setVlDescontoPagamento(BigDecimal vlDescontoPagamento) {
        this.vlDescontoPagamento = vlDescontoPagamento;
    }

    /**
     * Get: hrEfetivacaoCreditoPagamento.
     *
     * @return the hrEfetivacaoCreditoPagamento
     */
    public String getHrEfetivacaoCreditoPagamento() {
        return hrEfetivacaoCreditoPagamento;
    }

    /**
     * Set: hrEfetivacaoCreditoPagamento.
     *
     * @param hrEfetivacaoCreditoPagamento the hrEfetivacaoCreditoPagamento to set
     */
    public void setHrEfetivacaoCreditoPagamento(String hrEfetivacaoCreditoPagamento) {
        this.hrEfetivacaoCreditoPagamento = hrEfetivacaoCreditoPagamento;
    }

    /**
     * Get: hrEnvioCreditoPagamento.
     *
     * @return the hrEnvioCreditoPagamento
     */
    public String getHrEnvioCreditoPagamento() {
        return hrEnvioCreditoPagamento;
    }

    /**
     * Set: hrEnvioCreditoPagamento.
     *
     * @param hrEnvioCreditoPagamento the hrEnvioCreditoPagamento to set
     */
    public void setHrEnvioCreditoPagamento(String hrEnvioCreditoPagamento) {
        this.hrEnvioCreditoPagamento = hrEnvioCreditoPagamento;
    }

    /**
     * Get: cdIdentificadorTransferenciaPagto.
     *
     * @return the cdIdentificadorTransferenciaPagto
     */
    public Integer getCdIdentificadorTransferenciaPagto() {
        return cdIdentificadorTransferenciaPagto;
    }

    /**
     * Set: cdIdentificadorTransferenciaPagto.
     *
     * @param cdIdentificadorTransferenciaPagto the cdIdentificadorTransferenciaPagto to set
     */
    public void setCdIdentificadorTransferenciaPagto(
        Integer cdIdentificadorTransferenciaPagto) {
        this.cdIdentificadorTransferenciaPagto = cdIdentificadorTransferenciaPagto;
    }

    /**
     * Get: cdMensagemLinExtrato.
     *
     * @return the cdMensagemLinExtrato
     */
    public Integer getCdMensagemLinExtrato() {
        return cdMensagemLinExtrato;
    }

    /**
     * Set: cdMensagemLinExtrato.
     *
     * @param cdMensagemLinExtrato the cdMensagemLinExtrato to set
     */
    public void setCdMensagemLinExtrato(Integer cdMensagemLinExtrato) {
        this.cdMensagemLinExtrato = cdMensagemLinExtrato;
    }

    /**
     * Get: cdConveCtaSalarial.
     *
     * @return the cdConveCtaSalarial
     */
    public Long getCdConveCtaSalarial() {
        return cdConveCtaSalarial;
    }

    /**
     * Set: cdConveCtaSalarial.
     *
     * @param cdConveCtaSalarial the cdConveCtaSalarial to set
     */
    public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
        this.cdConveCtaSalarial = cdConveCtaSalarial;
    }

    /**
     * Set: flagExibeCampos.
     *
     * @param flagExibeCampos the flag exibe campos
     */
    public void setFlagExibeCampos(boolean flagExibeCampos) {
        this.flagExibeCampos = flagExibeCampos;
    }

    /**
     * Is flag exibe campos.
     *
     * @return true, if is flag exibe campos
     */
    public boolean isFlagExibeCampos() {
        return flagExibeCampos;
    }

    /**
     * Set: dsIdentificadorTransferenciaPagto.
     *
     * @param dsIdentificadorTransferenciaPagto the ds identificador transferencia pagto
     */
    public void setDsIdentificadorTransferenciaPagto(
        String dsIdentificadorTransferenciaPagto) {
        this.dsIdentificadorTransferenciaPagto = dsIdentificadorTransferenciaPagto;
    }

    /**
     * Get: dsIdentificadorTransferenciaPagto.
     *
     * @return dsIdentificadorTransferenciaPagto
     */
    public String getDsIdentificadorTransferenciaPagto() {
        return dsIdentificadorTransferenciaPagto;
    }

    /**
     * Set: cdIspbPagamento.
     *
     * @param cdIspbPagamento the cdIspbPagamento to set
     */
    public void setCdIspbPagamento(String cdIspbPagamento) {
        this.cdIspbPagamento = cdIspbPagamento;
    }

    /**
     * Get: cdIspbPagamento.
     *
     * @return the cdIspbPagamento
     */
    public String getCdIspbPagamento() {
        return cdIspbPagamento;
    }

    /**
     * Set: dsBancoCredito.
     *
     * @param dsBancoCredito the dsBancoCredito to set
     */
    public void setDsBancoCredito(String dsBancoCredito) {
        this.dsBancoCredito = dsBancoCredito;
    }

    /**
     * Get: dsBancoCredito.
     *
     * @return the dsBancoCredito
     */
    public String getDsBancoCredito() {
        return dsBancoCredito;
    }

    /**
     * Get: consultaAutorizanteService.
     *
     * @return consultaAutorizanteService
     */
    public IConsultaAutorizanteService getConsultaAutorizanteService() {
        return consultaAutorizanteService;
    }

    /**
     * Set: consultaAutorizanteService.
     *
     * @param consultaAutorizanteService the consulta autorizante service
     */
    public void setConsultaAutorizanteService(IConsultaAutorizanteService consultaAutorizanteService) {
        this.consultaAutorizanteService = consultaAutorizanteService;
    }


    /**
     * Get: itemSelecionadoListaAutorizante.
     *
     * @return itemSelecionadoListaAutorizante
     */
    public Integer getItemSelecionadoListaAutorizante() {
        return itemSelecionadoListaAutorizante;
    }

    /**
     * Set: itemSelecionadoListaAutorizante.
     *
     * @param itemSelecionadoListaAutorizante the item selecionado lista autorizante
     */
    public void setItemSelecionadoListaAutorizante(Integer itemSelecionadoListaAutorizante) {
        this.itemSelecionadoListaAutorizante = itemSelecionadoListaAutorizante;
    }

    /**
     * Get: listaRadioAutorizante.
     *
     * @return listaRadioAutorizante
     */
    public ArrayList<SelectItem> getListaRadioAutorizante() {
        return listaRadioAutorizante;
    }

    /**
     * Set: listaRadioAutorizante.
     *
     * @param listaRadioAutorizante the lista radio autorizante
     */
    public void setListaRadioAutorizante(ArrayList<SelectItem> listaRadioAutorizante) {
        this.listaRadioAutorizante = listaRadioAutorizante;
    }

    /**
     * Get: consultarAutorizantesOcorrencia.
     *
     * @return consultarAutorizantesOcorrencia
     */
    public ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO> getConsultarAutorizantesOcorrencia() {
        return consultarAutorizantesOcorrencia;
    }

    /**
     * Set: consultarAutorizantesOcorrencia.
     *
     * @param consultarAutorizantesOcorrencia the consultar autorizantes ocorrencia
     */
    public void setConsultarAutorizantesOcorrencia(
        ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO> consultarAutorizantesOcorrencia) {
        this.consultarAutorizantesOcorrencia = consultarAutorizantesOcorrencia;
    }

    /**
     * Nome: setDetalharPagtoTed.
     *
     * @param detalharPagtoTed the detalhar pagto ted
     */
    public void setDetalharPagtoTed(DetalharPagtoTEDSaidaDTO detalharPagtoTed) {
        this.detalharPagtoTed = detalharPagtoTed;
    }

    /**
     * Nome: getDetalharPagtoTed.
     *
     * @return detalharPagtoTed
     */
    public DetalharPagtoTEDSaidaDTO getDetalharPagtoTed() {
        return detalharPagtoTed;
    }

    /**
     * Get: dsNomeReclamante.
     *
     * @return dsNomeReclamante
     */
    public String getDsNomeReclamante() {
        return dsNomeReclamante;
    }

    /**
     * Set: dsNomeReclamante.
     *
     * @param dsNomeReclamante the ds nome reclamante
     */
    public void setDsNomeReclamante(String dsNomeReclamante) {
        this.dsNomeReclamante = dsNomeReclamante;
    }

    /**
     * Get: nrProcessoDepositoJudicial.
     *
     * @return nrProcessoDepositoJudicial
     */
    public String getNrProcessoDepositoJudicial() {
        return nrProcessoDepositoJudicial;
    }

    /**
     * Set: nrProcessoDepositoJudicial.
     *
     * @param nrProcessoDepositoJudicial the nr processo deposito judicial
     */
    public void setNrProcessoDepositoJudicial(String nrProcessoDepositoJudicial) {
        this.nrProcessoDepositoJudicial = nrProcessoDepositoJudicial;
    }

    /**
     * Get: nrPisPasep.
     *
     * @return nrPisPasep
     */
    public String getNrPisPasep() {
        return nrPisPasep;
    }

    /**
     * Set: nrPisPasep.
     *
     * @param nrPisPasep the nr pis pasep
     */
    public void setNrPisPasep(String nrPisPasep) {
        this.nrPisPasep = nrPisPasep;
    }

    /**
     * Is cd finalidade ted deposito judicial.
     * 
     * @return true, if is cd finalidade ted deposito judicial
     */
    public boolean isCdFinalidadeTedDepositoJudicial() {
        return cdFinalidadeTedDepositoJudicial;
    }

    /**
     * Set: cdFinalidadeTedDepositoJudicial.
     * 
     * @param cdFinalidadeTedDepositoJudicial
     *            the cd finalidade ted deposito judicial
     */
    public void setCdFinalidadeTedDepositoJudicial(boolean cdFinalidadeTedDepositoJudicial) {
        this.cdFinalidadeTedDepositoJudicial = cdFinalidadeTedDepositoJudicial;
    }

    /**
     * Nome: getEntradaConsultaPgtos
     *
     * @return entradaConsultaPgtos
     */
    public ConsultarPagamentosIndividuaisEntradaDTO getEntradaConsultaPgtos() {
        return entradaConsultaPgtos;
    }

    /**
     * Nome: setEntradaConsultaPgtos
     *
     * @param entradaConsultaPgtos
     */
    public void setEntradaConsultaPgtos(ConsultarPagamentosIndividuaisEntradaDTO entradaConsultaPgtos) {
        this.entradaConsultaPgtos = entradaConsultaPgtos;
    }
  

    /**
     * Nome: getListaFormaPagamento
     *
     * @return listaFormaPagamento
     */
    public List<ListarTipoRetiradaOcorrenciasSaidaDTO> getListaFormaPagamento() {
        return listaFormaPagamento;
    }

    /**
     * Nome: setListaFormaPagamento
     *
     * @param listaFormaPagamento
     */
    public void setListaFormaPagamento(List<ListarTipoRetiradaOcorrenciasSaidaDTO> listaFormaPagamento) {
        this.listaFormaPagamento = listaFormaPagamento;
    }

	public boolean isExibeBancoCredito() {
		return exibeBancoCredito;
	}

	public void setExibeBancoCredito(boolean exibeBancoCredito) {
		this.exibeBancoCredito = exibeBancoCredito;
	}

	public String getCdBancoDestinoPagamento() {
		return cdBancoDestinoPagamento;
	}

	public void setCdBancoDestinoPagamento(String cdBancoDestinoPagamento) {
		this.cdBancoDestinoPagamento = cdBancoDestinoPagamento;
	}

	public String getContaDigitoDestinoPagamento() {
		return contaDigitoDestinoPagamento;
	}

	public void setContaDigitoDestinoPagamento(String contaDigitoDestinoPagamento) {
		this.contaDigitoDestinoPagamento = contaDigitoDestinoPagamento;
	}

	public String getTipoContaDestinoPagamento() {
		return tipoContaDestinoPagamento;
	}

	public void setTipoContaDestinoPagamento(String tipoContaDestinoPagamento) {
		this.tipoContaDestinoPagamento = tipoContaDestinoPagamento;
	}

	public String getDsISPB() {
		return dsISPB;
	}

	public void setDsISPB(String dsISPB) {
		this.dsISPB = dsISPB;
	}

	public String getCdContaPagamentoDet() {
		return cdContaPagamentoDet;
	}

	public void setCdContaPagamentoDet(String cdContaPagamentoDet) {
		this.cdContaPagamentoDet = cdContaPagamentoDet;
	}

	public Long getCdLoteInterno() {
		return cdLoteInterno;
	}

	public void setCdLoteInterno(Long cdLoteInterno) {
		this.cdLoteInterno = cdLoteInterno;
	}

	public String getNumeroInscricaoSacadorAvalista() {
		return numeroInscricaoSacadorAvalista;
	}

	public void setNumeroInscricaoSacadorAvalista(
			String numeroInscricaoSacadorAvalista) {
		this.numeroInscricaoSacadorAvalista = numeroInscricaoSacadorAvalista;
	}

	public String getDsSacadorAvalista() {
		return dsSacadorAvalista;
	}

	public void setDsSacadorAvalista(String dsSacadorAvalista) {
		this.dsSacadorAvalista = dsSacadorAvalista;
	}

	public String getDsTipoSacadorAvalista() {
		return dsTipoSacadorAvalista;
	}

	public void setDsTipoSacadorAvalista(String dsTipoSacadorAvalista) {
		this.dsTipoSacadorAvalista = dsTipoSacadorAvalista;
	}
	
	public String getAgenciaEstornoFormatado() {
		if(NUMERO_ZERO != detalheOrdemPagamento.getCdAgenciaEstorno() && !VAZIO.equals(detalheOrdemPagamento.getCdDigitoAgenciaEstorno())){			
			return detalheOrdemPagamento.getCdAgenciaEstorno() + "-" + detalheOrdemPagamento.getCdDigitoAgenciaEstorno();			
		}else{
			return VAZIO;
		} 
	}	
	
	public String getContaEstornoFormatado() {
		if(NUMERO_ZERO_LONG != detalheOrdemPagamento.getCdContaEstorno() && !VAZIO.equals(detalheOrdemPagamento.getCdDigitoContaEstorno())){
			return detalheOrdemPagamento.getCdContaEstorno() + "-" + detalheOrdemPagamento.getCdDigitoContaEstorno();
		}else{
			return VAZIO;
		}		
	}

	public DetalharPagtoOrdemPagtoSaidaDTO getDetalheOrdemPagamento() {
		return detalheOrdemPagamento;
	}

	public void setDetalheOrdemPagamento(
			DetalharPagtoOrdemPagtoSaidaDTO detalheOrdemPagamento) {
		this.detalheOrdemPagamento = detalheOrdemPagamento;
	}
  
	
	
}