package br.com.bradesco.web.pgit.view.bean.confcestastarifas;

import br.com.bradesco.web.pgit.service.business.confcestastarifas.IConfCestaTarifaService;
import br.com.bradesco.web.pgit.service.business.confcestastarifas.dto.ConfCestaTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.confcestastarifas.dto.ConfCestaTarifaSaidaDTO;

public class ConfCestasTarifasBean {
	
	private static final int CD_MAX_OCORRENCIAS = 50;
	
	private IConfCestaTarifaService iConfCestasTarifasService;
	
	private ConfCestaTarifaEntradaDTO entradaDTO;
	
	private ConfCestaTarifaSaidaDTO saida;
	
	public ConfCestaTarifaSaidaDTO consultarConfCestasTarifasSaidaDTO(ConfCestaTarifaEntradaDTO entrada){
		entradaDTO.setMaxOcorrencias(CD_MAX_OCORRENCIAS);
		entradaDTO.setCdPessoasJuridicaContrato(entrada.getCdPessoasJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(entrada.getCdTipoContratoNegocio());
		entradaDTO.setNumSequenciaNegocio(entrada.getNumSequenciaNegocio());
		
		saida = iConfCestasTarifasService.consultarConfCestaTarifa(entrada);
		
		return saida;
	}

	public IConfCestaTarifaService getiConfCestasTarifasService() {
		return iConfCestasTarifasService;
	}

	public void setiConfCestasTarifasService(
			IConfCestaTarifaService iConfCestasTarifasService) {
		this.iConfCestasTarifasService = iConfCestasTarifasService;
	}

	public ConfCestaTarifaEntradaDTO getEntradaDTO() {
		return entradaDTO;
	}

	public void setEntradaDTO(ConfCestaTarifaEntradaDTO entradaDTO) {
		this.entradaDTO = entradaDTO;
	}

	public ConfCestaTarifaSaidaDTO getSaida() {
		return saida;
	}

	public void setSaida(ConfCestaTarifaSaidaDTO saida) {
		this.saida = saida;
	}
}
