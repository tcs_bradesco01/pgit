/*
 * Nome: br.com.bradesco.web.pgit.view.bean.simulacaotarifasnegociacao
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.simulacaotarifasnegociacao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.ISimulacaoTarifasNegociacaoService;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.DetalharSimulacaoTarifaNegociacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.DetalharSimulacaoTarifaNegociacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.ListaOperacaoTarifaTipoServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.OcorrenciasDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: SimulacaoTarifasNegociacaoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SimulacaoTarifasNegociacaoBean {
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;
	
	/** Atributo grupoEconomico. */
	private String grupoEconomico;
	
	/** Atributo cpfCnpjCliente. */
	private String cpfCnpjCliente;
	
	/** Atributo atividadeEconomica. */
	private String atividadeEconomica;
	
	/** Atributo segmento. */
	private String segmento;
	
	/** Atributo subSegmento. */
	private String subSegmento;
	
	/** Atributo listaGrid. */
	private List<ListaOperacaoTarifaTipoServicoSaidaDTO> listaGrid;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio;
	
	/** Atributo painelBotoes. */
	private boolean painelBotoes;
	
	/** Atributo simulacaoTarifasNegociacaoService. */
	private ISimulacaoTarifasNegociacaoService simulacaoTarifasNegociacaoService;
	
	/** Atributo tarifaPadraoMes. */
	private BigDecimal tarifaPadraoMes;
	
	/** Atributo tarifaPropostaMes. */
	private BigDecimal tarifaPropostaMes;
	
	/** Atributo resultado. */
	private BigDecimal resultado;
	
	/** Atributo flexibilidade. */
	private BigDecimal flexibilidade;
	
	/** Atributo receitaFloatingCliente. */
	private BigDecimal receitaFloatingCliente;
	
	/** Atributo total. */
	private BigDecimal total;
	
	// campos de entrada no pdc principal
	/** Atributo cdFluxoNegocio. */
	private Integer cdFluxoNegocio;
    
    /** Atributo cdGrupoInfoFluxo. */
    private Integer cdGrupoInfoFluxo;
    
    /** Atributo cdProdutoOperacaoDeflt. */
    private Integer cdProdutoOperacaoDeflt;
    
    /** Atributo cdOperacaoProdutoServico. */
    private Integer cdOperacaoProdutoServico;
    
    /** Atributo cdTipoCanal. */
    private Integer cdTipoCanal;
    
    /** Atributo cdPessoaJuridicaContrato. */
    private Long cdPessoaJuridicaContrato;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaContratoNegocio. */
    private Long nrSequenciaContratoNegocio;
    
    /** Atributo nrOcorGrupoProposta. */
    private Integer nrOcorGrupoProposta;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    
    /** Atributo numeroOcorrencias. */
    private Integer numeroOcorrencias;
	    
	
	
	
	/**
	 * Iniciar pagina.
	 *
	 * @return the string
	 */
	public String iniciarPagina() {		
		
		setCpfCnpjCliente(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramCpfCnpjCliente")));
		setNomeRazaoSocial(PgitUtil.verificaStringNula((String)FacesUtils.getRequestParameter("paramNomeRazaoSocial")));
		setGrupoEconomico(PgitUtil.verificaStringNula((String)FacesUtils.getRequestParameter("paramGrupoEconomico")));
		setAtividadeEconomica(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramAtividadeEconomica")));
		setSegmento(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramSegmento")));
		setSubSegmento(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramSubSegmento")));
		
		Integer aux = 0;
		
		try {
			aux =Integer.parseInt(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramCdFluxoNegocio"))); 
		} catch (NumberFormatException e) {
			aux = 0;
		}
		
		setCdFluxoNegocio(aux);
		
		aux = 0;
		try {
			aux =Integer.parseInt(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramCdGrupoInfoFluxo"))); 
		} catch (NumberFormatException e) {
			aux = 0;
		}
		
		setCdGrupoInfoFluxo(aux);
		
		aux = 0;
		try {
			aux =Integer.parseInt(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramCdProdutoOperacaoDeflt"))); 
		} catch (NumberFormatException e) {
			aux = 0;
		}
		
		setCdProdutoOperacaoDeflt(aux);
		
		aux = 0;
		try {
			aux =Integer.parseInt(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramCdOperacaoProdutoServico"))); 
		} catch (NumberFormatException e) {
			aux = 0;
		}
		
		setCdOperacaoProdutoServico(aux);
		
		aux = 0;
		try {
			aux =Integer.parseInt(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramCdTipoCanal"))); 
		} catch (NumberFormatException e) {
			aux = 0;
		}
		
		setCdTipoCanal(aux);
		
		Long auxLong = 0l;
		try {
			auxLong =Long.parseLong(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramCdPessoaJuridicaContrato"))); 
		} catch (NumberFormatException e) {
			auxLong = 0l;
		}
		
		setCdPessoaJuridicaContrato(auxLong);
		
		
		aux = 0;
		try {
			aux =Integer.parseInt(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramCdTipoContratoNegocio"))); 
		} catch (NumberFormatException e) {
			aux = 0;
		}
		
		setCdTipoContratoNegocio(aux);
		
		
		 auxLong = 0l;
		try {
			auxLong =Long.parseLong(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramNrSequenciaContratoNegocio"))); 
		} catch (NumberFormatException e) {
			auxLong = 0l;
		}
		
		setNrSequenciaContratoNegocio(auxLong);
		
		
		aux = 0;
		
		try {
			aux =Integer.parseInt(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramNrOcorGrupoProposta"))); 
		} catch (NumberFormatException e) {
			aux = 0;
		}
		
		setNrOcorGrupoProposta(aux);

		
		
		try {
			aux =Integer.parseInt(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramCdProdutoServicoOperacao"))); 
		} catch (NumberFormatException e) {
			aux = 0;
		}
		
		setCdProdutoServicoOperacao(aux);
		
		
		try {
			aux =Integer.parseInt(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramCdProdutoOperacaoRelacionado"))); 
		} catch (NumberFormatException e) {
			aux = 0;
		}
		
		setCdProdutoOperacaoRelacionado(aux);
		
		
		try {
			aux =Integer.parseInt(PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("paramNumeroOcorrencias"))); 
		} catch (NumberFormatException e) {
			aux = 0;
		}
		
		setNumeroOcorrencias(aux);
		
		
		
		setTotal(BigDecimal.ZERO);
		listaGrid = new ArrayList<ListaOperacaoTarifaTipoServicoSaidaDTO>();
		try{
			
			ListaOperacaoTarifaTipoServicoEntradaDTO entradaDTO = new ListaOperacaoTarifaTipoServicoEntradaDTO();
			
			entradaDTO.setCdFluxoNegocio(getCdFluxoNegocio());
			entradaDTO.setCdGrupoInfoFluxo(getCdGrupoInfoFluxo());
			entradaDTO.setCdOperacaoProdutoServico(getCdOperacaoProdutoServico());
			//entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridicaContrato());
			entradaDTO.setCdProdutoOperacaoDeflt(getCdProdutoOperacaoDeflt());
			//entradaDTO.setCdProdutoOperacaoRelacionado(getCdProdutoOperacaoRelacionado());
			//entradaDTO.setCdProdutoServicoOperacao(getCdProdutoServicoOperacao());
			entradaDTO.setCdTipoCanal(getCdTipoCanal());
			//entradaDTO.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entradaDTO.setNrOcorGrupoProposta(getNrOcorGrupoProposta());
			entradaDTO.setNrSequenciaContratoNegocio(getNrSequenciaContratoNegocio());
			entradaDTO.setNumeroOcorrencias(getNumeroOcorrencias());
			
			//campos inseridos na vers�o 2.0 dia 2/05/2011 conforme solicitado via email por Andre Fernandes Vieira
			entradaDTO.setCdNegocioRenegocio(0);
			entradaDTO.setCdPessoaContrato(0L);
			entradaDTO.setCdPessoaJuridicaProposta(0L);
			entradaDTO.setCdTipoContrato(0);
			entradaDTO.setCdTipoContratoProposta(0);
			entradaDTO.setNrSequenciaContrato(0L);
			
			
		
		
		
			
			
			
						
		/*	ListaOperacaoTarifaTipoServicoSaidaDTO saidaDTO = new ListaOperacaoTarifaTipoServicoSaidaDTO();
			
			saidaDTO.setDsProdutoOperacaoRelacionado("Modalidade");
			saidaDTO.setDsProdutoServicoOperacao("Servi�o");
			saidaDTO.setCdRelacionamentoProdutoProduto(1);
			saidaDTO.setVlrTarifaContrato(BigDecimal.TEN);
			
			listaGrid.add(saidaDTO);
			
			 saidaDTO = new ListaOperacaoTarifaTipoServicoSaidaDTO();
				
			saidaDTO.setDsProdutoOperacaoRelacionado("Modalidade 2");
			saidaDTO.setDsProdutoServicoOperacao("Servi�o 2 ");
			saidaDTO.setCdRelacionamentoProdutoProduto(2);
			saidaDTO.setVlrTarifaContrato(BigDecimal.ONE);
			
			listaGrid.add(saidaDTO);
			
			 saidaDTO = new ListaOperacaoTarifaTipoServicoSaidaDTO();
				
				saidaDTO.setDsProdutoOperacaoRelacionado("Modalidade 3");
				saidaDTO.setDsProdutoServicoOperacao("Servi�o 3 ");
				saidaDTO.setCdRelacionamentoProdutoProduto(3);
				saidaDTO.setVlrTarifaContrato(BigDecimal.ONE);
				
				listaGrid.add(saidaDTO);
				
				 saidaDTO = new ListaOperacaoTarifaTipoServicoSaidaDTO();
					
					saidaDTO.setDsProdutoOperacaoRelacionado("Modalidade 4");
					saidaDTO.setDsProdutoServicoOperacao("Servi�o 4 ");
					saidaDTO.setCdRelacionamentoProdutoProduto(4);
					saidaDTO.setVlrTarifaContrato(BigDecimal.TEN);
					
					listaGrid.add(saidaDTO); */
			setListaGrid(getSimulacaoTarifasNegociacaoService().consultarListaOperacaoTarifaTipoServico(entradaDTO));
			
			listaControleRadio = new ArrayList<SelectItem>();
		    for(int i=0;i<getListaGrid().size();i++){
		    	listaControleRadio.add(new SelectItem(i,""));
		    }		    
				
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			
		}
		
		
		return "simulacaoTarifasNegociacao2";
	}
	
	/**
	 * Voltar.
	 */
	public void voltar(){
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
		
		String url = "http://" + request.getServerName() + ":" + request.getServerPort() + "/negociacao/xxxx.jsf";
		
		url = facesContext.getExternalContext().encodeResourceURL(url);
		
		try {   
		     facesContext.getExternalContext().redirect(url);
		} catch (Exception error) {   
			BradescoFacesUtils.addInfoModalMessage(MessageHelperUtils.getI18nMessage("label_redirecionamento_invalido"), false);   
		}
		
	}
	
	/**
	 * Voltar detalhe.
	 *
	 * @return the string
	 */
	public String voltarDetalhe(){
		return "VOLTAR";
	}
	
	/**
	 * Simular.
	 *
	 * @return the string
	 */
	public String simular(){
		StringBuffer mensagem = new StringBuffer("");
		
		for(int i=0;i<getListaGrid().size();i++){
			if(getListaGrid().get(i).isCheck()){
				ListaOperacaoTarifaTipoServicoSaidaDTO dto = getListaGrid().get(i);
				boolean percTarifa = PgitUtil.verificaBigDecimalNulo(dto.getPercentualTarifaIndividual()).equals(BigDecimal.ZERO) && PgitUtil.verificaBigDecimalNulo(dto.getVlrTarifaIndProposta()).equals(BigDecimal.ZERO);
				boolean qtdeDiasFloating = PgitUtil.verificaIntegerNulo(dto.getQtdeDiasFloating()) == 0 ;
				boolean qtdeEstimada = PgitUtil.verificaIntegerNulo(dto.getQtdeEstimada()) == 0 ;
				if ( percTarifa || qtdeDiasFloating ||  qtdeEstimada){
					mensagem.append(MessageHelperUtils.getI18nMessage("label_validar_simular_tarifas_negociacao1"));
					mensagem.append(" ");

					if (percTarifa){
						mensagem.append(MessageHelperUtils.getI18nMessage("label_tarifa_individual_proposta"));
						mensagem.append(" ");
						mensagem.append(MessageHelperUtils.getI18nMessage("label_ou"));
						mensagem.append(" ");
						mensagem.append(MessageHelperUtils.getI18nMessage("label_tarifa_individual_percent"));
					}

					if ( qtdeEstimada ){
						if (percTarifa && ! qtdeDiasFloating){
							mensagem.append(" ");
							mensagem.append(MessageHelperUtils.getI18nMessage("label_e"));
							mensagem.append(" ");
						}
						if(percTarifa && qtdeDiasFloating){
							mensagem.append(", ");
						}
						mensagem.append(MessageHelperUtils.getI18nMessage("label_qtde_estimada"));
					}
					
					if (PgitUtil.verificaIntegerNulo(dto.getQtdeDiasFloating()) == 0 ){
						if (percTarifa ||  qtdeEstimada){
							mensagem.append(" ");
							mensagem.append(MessageHelperUtils.getI18nMessage("label_e"));
							mensagem.append(" ");
						}						
						mensagem.append(MessageHelperUtils.getI18nMessage("label_qtde_dias_floating")).append('\n');
					}

					mensagem.append(" ");
					mensagem.append(MessageHelperUtils.getI18nMessage("label_da_linha"));
					mensagem.append(" ");
					mensagem.append((i+1));
					mensagem.append("!");

					BradescoFacesUtils.addInfoModalMessage(mensagem.toString(), false);
					
					return "";
				}
				
			}
		}
		
		try {
			
			BigDecimal valorTotal = BigDecimal.ZERO;
			BigDecimal valorTotalPadrao = BigDecimal.ZERO;
			DetalharSimulacaoTarifaNegociacaoEntradaDTO entradaDTO = new DetalharSimulacaoTarifaNegociacaoEntradaDTO();
			br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.OcorrenciasDTO ocorrenciaDTO;
			List<OcorrenciasDTO> ocorrencias = new ArrayList<OcorrenciasDTO>();
			ListaOperacaoTarifaTipoServicoSaidaDTO saidaDTO;
			int qtde = 0;
			
			for(int i=0;i<getListaGrid().size();i++){
				if(getListaGrid().get(i).isCheck()){
					ocorrenciaDTO = new br.com.bradesco.web.pgit.service.business.simulacaotarifasnegociacao.bean.OcorrenciasDTO ();
					
					saidaDTO= getListaGrid().get(i);
					
					ocorrenciaDTO.setCdOperacaoProdutoServico(saidaDTO.getCdOperacaoProdutoServico());
					ocorrenciaDTO.setCdProdutoOperacaoRelacionado(saidaDTO.getCdProdutoOperacaoRelacionado());
					ocorrenciaDTO.setCdProdutoServicoOperacao(saidaDTO.getCdProdutoServicoOperacao());
					ocorrenciaDTO.setCdRelacionamentoProduto(saidaDTO.getCdRelacionamentoProdutoProduto());
					ocorrenciaDTO.setQtDiasFloat(saidaDTO.getQtdeDiasFloating());
					ocorrenciaDTO.setQtEstimada(Long.parseLong(saidaDTO.getQtdeEstimada().toString()));
					ocorrenciaDTO.setVlTarifaPadrao(saidaDTO.getVlrTarifaContrato());
					ocorrenciaDTO.setVlTarifaProposta(saidaDTO.getVlrTarifaIndProposta());
					
					ocorrencias.add(ocorrenciaDTO);
					valorTotal = saidaDTO.getVlrTotal();
					valorTotalPadrao = saidaDTO.getVlrTotalPadrao();
					qtde = qtde + 1;
				
				}
			}
			
			entradaDTO.setQtOcorrencias(qtde);
			entradaDTO.setOcorrencias(ocorrencias);
			entradaDTO.setVlTarifaTotalPad(valorTotal);
			entradaDTO.setVlTarifaTotalPro(valorTotalPadrao);
			
			DetalharSimulacaoTarifaNegociacaoSaidaDTO detalharSaidaDTO = getSimulacaoTarifasNegociacaoService().detalharSimulacaoTarifaNegociacao(entradaDTO);
			
			setTarifaPadraoMes(detalharSaidaDTO.getVlTarifaTotalPad());
			setTarifaPropostaMes(detalharSaidaDTO.getVlTarifaTotalPro());
			setResultado(detalharSaidaDTO.getVlTarifaDesconto());
			setFlexibilidade(detalharSaidaDTO.getPercentualFlexibilidade());
			setReceitaFloatingCliente(detalharSaidaDTO.getVlFloat());
			
			
			
			
			
			
		} catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
		return "SIMULAR";
	}

	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes(){
		boolean aux = false;		
		for(int i=0;i<getListaGrid().size();i++){
			if(getListaGrid().get(i).isCheck()){
				aux = true;
			}else{
				ListaOperacaoTarifaTipoServicoSaidaDTO dto = getListaGrid().get(i);
				dto.setVlrTarifaIndProposta(null);
				dto.setPercentualTarifaIndividual(null);
				dto.setQtdeEstimada(null);
				dto.setQtdeDiasFloating(null);
				dto.setVlrMedio(BigDecimal.ZERO);
				dto.setVlrTotal(BigDecimal.ZERO);
				dto.setVlrMedioPadrao(BigDecimal.ZERO);
				dto.setVlrTotalPadrao(BigDecimal.ZERO);
				getListaGrid().set(i, dto);
			}
		}
		setPainelBotoes(aux);
		calcular();
	}

	
	/**
	 * Calcular.
	 */
	public void calcular(){
		BigDecimal soma = BigDecimal.ZERO;
		BigDecimal somaPadrao = BigDecimal.ZERO;
		for(int i=0;i<getListaGrid().size();i++){
			if(getListaGrid().get(i).isCheck()){
				ListaOperacaoTarifaTipoServicoSaidaDTO dto = getListaGrid().get(i);				
				
				if (!PgitUtil.verificaBigDecimalNulo(dto.getPercentualTarifaIndividual()).equals(BigDecimal.ZERO)){
					dto.setVlrTarifaIndProposta(dto.getPercentualTarifaIndividual().multiply(PgitUtil.verificaBigDecimalNulo(dto.getVlrTarifaContrato()).divide(new BigDecimal(100))));
				}else{
					dto.setVlrTarifaIndProposta(null);
				}
				if (!PgitUtil.verificaBigDecimalNulo(dto.getVlrTarifaIndProposta()).equals(BigDecimal.ZERO)){
					dto.setPercentualTarifaIndividual((dto.getVlrTarifaIndProposta().multiply(new BigDecimal(100)).divide(PgitUtil.verificaBigDecimalNulo(dto.getVlrTarifaContrato()))));
				}else{
					dto.setPercentualTarifaIndividual(null);
				}
				
				if (PgitUtil.verificaIntegerNulo(dto.getQtdeEstimada()) !=0  && !PgitUtil.verificaBigDecimalNulo(dto.getVlrTarifaIndProposta()).equals(BigDecimal.ZERO)){
					dto.setVlrMedio(dto.getVlrTarifaIndProposta().multiply(new BigDecimal(dto.getQtdeEstimada())));
					soma = soma.add(dto.getVlrMedio() == null? BigDecimal.ZERO : dto.getVlrMedio());
					dto.setVlrTotal(soma);
				}else{
					dto.setVlrMedio(null);
					dto.setVlrTotal(BigDecimal.ZERO);
					
				}
				if (PgitUtil.verificaIntegerNulo(dto.getQtdeEstimada()) !=0 ){					
					dto.setVlrMedioPadrao(dto.getVlrTarifaContrato().multiply(new BigDecimal(dto.getQtdeEstimada().toString())));
					somaPadrao = somaPadrao.add(dto.getVlrMedioPadrao() == null? BigDecimal.ZERO : dto.getVlrMedioPadrao());
					dto.setVlrTotalPadrao(somaPadrao);
					
				}else{
					dto.setVlrMedioPadrao(BigDecimal.ZERO);
					dto.setVlrTotalPadrao(BigDecimal.ZERO);
				}
				
				
				getListaGrid().set(i, dto);
			}
		}
		
	}
	
	/**
	 * Calcular percentual.
	 */
	public void calcularPercentual(){
		BigDecimal soma = BigDecimal.ZERO;
		//BigDecimal somaPadrao = BigDecimal.ZERO;
		for(int i=0;i<getListaGrid().size();i++){
			if(getListaGrid().get(i).isCheck()){
				ListaOperacaoTarifaTipoServicoSaidaDTO dto = getListaGrid().get(i);				
								
				if (!PgitUtil.verificaBigDecimalNulo(dto.getVlrTarifaIndProposta()).equals(BigDecimal.ZERO)){
					dto.setPercentualTarifaIndividual((dto.getVlrTarifaIndProposta().multiply(new BigDecimal(100)).divide(PgitUtil.verificaBigDecimalNulo(dto.getVlrTarifaContrato()))));
				}else{
					dto.setPercentualTarifaIndividual(null);
				}
				
				if (PgitUtil.verificaIntegerNulo(dto.getQtdeEstimada()) !=0l  && !PgitUtil.verificaBigDecimalNulo(dto.getVlrTarifaIndProposta()).equals(BigDecimal.ZERO)){
					dto.setVlrMedio(dto.getVlrTarifaIndProposta().multiply(new BigDecimal(dto.getQtdeEstimada())));
					soma = soma.add(dto.getVlrMedio() == null? BigDecimal.ZERO : dto.getVlrMedio());
					dto.setVlrTotal(soma);
				}else{
					dto.setVlrMedio(null);
					dto.setVlrTotal(BigDecimal.ZERO);
				}

				
				/*if (PgitUtil.verificaIntegerNulo(dto.getQtdeEstimada()) !=0l){
					dto.setVlrMedioPadrao(dto.getVlrTarifaContrato().multiply(new BigDecimal(dto.getQtdeEstimada())));
					somaPadrao = somaPadrao.add(dto.getVlrMedioPadrao() == null? BigDecimal.ZERO : dto.getVlrMedioPadrao());
					dto.setVlrTotalPadrao(somaPadrao);
				}else{
					dto.setVlrMedioPadrao(BigDecimal.ZERO);
					dto.setVlrTotalPadrao(BigDecimal.ZERO);
				}
				*/
				
				
				
				getListaGrid().set(i, dto);
			}
		}
		
	}
	
	/**
	 * Calcular valor proposta.
	 */
	public void calcularValorProposta(){
		BigDecimal soma = BigDecimal.ZERO;
		//BigDecimal somaPadrao = BigDecimal.ZERO;
		for(int i=0;i<getListaGrid().size();i++){
			if(getListaGrid().get(i).isCheck()){
				ListaOperacaoTarifaTipoServicoSaidaDTO dto = getListaGrid().get(i);				
				
				if (!PgitUtil.verificaBigDecimalNulo(dto.getPercentualTarifaIndividual()).equals(BigDecimal.ZERO)){
					dto.setVlrTarifaIndProposta(dto.getPercentualTarifaIndividual().multiply(PgitUtil.verificaBigDecimalNulo(dto.getVlrTarifaContrato()).divide(new BigDecimal(100))));
				}else{
					dto.setVlrTarifaIndProposta(null);
				}
				
				if (PgitUtil.verificaIntegerNulo(dto.getQtdeEstimada()) !=0  && !PgitUtil.verificaBigDecimalNulo(dto.getVlrTarifaIndProposta()).equals(BigDecimal.ZERO)){
					dto.setVlrMedio(dto.getVlrTarifaIndProposta().multiply(new BigDecimal(dto.getQtdeEstimada())));
					soma = soma.add(dto.getVlrMedio() == null? BigDecimal.ZERO : dto.getVlrMedio());
					dto.setVlrTotal(soma);
				}else{
					dto.setVlrMedio(null);
					dto.setVlrTotal(BigDecimal.ZERO);
				}
				

			/*	if (PgitUtil.verificaIntegerNulo(dto.getQtdeEstimada()) !=0 ){
					dto.setVlrMedioPadrao(dto.getVlrTarifaContrato().multiply(new BigDecimal(dto.getQtdeEstimada())));
					somaPadrao = somaPadrao.add(dto.getVlrMedioPadrao() == null? BigDecimal.ZERO : dto.getVlrMedioPadrao());
					dto.setVlrTotalPadrao(somaPadrao);
				}else{
					dto.setVlrMedioPadrao(BigDecimal.ZERO);
					dto.setVlrTotalPadrao(BigDecimal.ZERO);
				}*/
				
				
				
				getListaGrid().set(i, dto);
			}
		}
		
	}
	
	/**
	 * Get: atividadeEconomica.
	 *
	 * @return atividadeEconomica
	 */
	public String getAtividadeEconomica() {
		return atividadeEconomica;
	}


	/**
	 * Set: atividadeEconomica.
	 *
	 * @param atividadeEconomica the atividade economica
	 */
	public void setAtividadeEconomica(String atividadeEconomica) {
		this.atividadeEconomica = atividadeEconomica;
	}


	/**
	 * Get: cpfCnpjCliente.
	 *
	 * @return cpfCnpjCliente
	 */
	public String getCpfCnpjCliente() {
		return cpfCnpjCliente;
	}


	/**
	 * Set: cpfCnpjCliente.
	 *
	 * @param cpfCnpjCliente the cpf cnpj cliente
	 */
	public void setCpfCnpjCliente(String cpfCnpjCliente) {
		this.cpfCnpjCliente = cpfCnpjCliente;
	}


	/**
	 * Get: grupoEconomico.
	 *
	 * @return grupoEconomico
	 */
	public String getGrupoEconomico() {
		return grupoEconomico;
	}


	/**
	 * Set: grupoEconomico.
	 *
	 * @param grupoEconomico the grupo economico
	 */
	public void setGrupoEconomico(String grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}


	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}


	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}


	/**
	 * Get: segmento.
	 *
	 * @return segmento
	 */
	public String getSegmento() {
		return segmento;
	}


	/**
	 * Set: segmento.
	 *
	 * @param segmento the segmento
	 */
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}


	/**
	 * Get: subSegmento.
	 *
	 * @return subSegmento
	 */
	public String getSubSegmento() {
		return subSegmento;
	}


	/**
	 * Set: subSegmento.
	 *
	 * @param subSegmento the sub segmento
	 */
	public void setSubSegmento(String subSegmento) {
		this.subSegmento = subSegmento;
	}


	/**
	 * Get: simulacaoTarifasNegociacaoService.
	 *
	 * @return simulacaoTarifasNegociacaoService
	 */
	public ISimulacaoTarifasNegociacaoService getSimulacaoTarifasNegociacaoService() {
		return simulacaoTarifasNegociacaoService;
	}


	/**
	 * Set: simulacaoTarifasNegociacaoService.
	 *
	 * @param simulacaoTarifasNegociacaoService the simulacao tarifas negociacao service
	 */
	public void setSimulacaoTarifasNegociacaoService(
			ISimulacaoTarifasNegociacaoService simulacaoTarifasNegociacaoService) {
		this.simulacaoTarifasNegociacaoService = simulacaoTarifasNegociacaoService;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListaOperacaoTarifaTipoServicoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListaOperacaoTarifaTipoServicoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Is painel botoes.
	 *
	 * @return true, if is painel botoes
	 */
	public boolean isPainelBotoes() {
		return painelBotoes;
	}

	/**
	 * Set: painelBotoes.
	 *
	 * @param habilitarPanelBotoes the painel botoes
	 */
	public void setPainelBotoes(boolean habilitarPanelBotoes) {
		this.painelBotoes = habilitarPanelBotoes;
	}

	/**
	 * Get: flexibilidade.
	 *
	 * @return flexibilidade
	 */
	public BigDecimal getFlexibilidade() {
		return flexibilidade;
	}

	/**
	 * Set: flexibilidade.
	 *
	 * @param flexibilidade the flexibilidade
	 */
	public void setFlexibilidade(BigDecimal flexibilidade) {
		this.flexibilidade = flexibilidade;
	}

	/**
	 * Get: resultado.
	 *
	 * @return resultado
	 */
	public BigDecimal getResultado() {
		return resultado;
	}

	/**
	 * Set: resultado.
	 *
	 * @param resultado the resultado
	 */
	public void setResultado(BigDecimal resultado) {
		this.resultado = resultado;
	}

	/**
	 * Get: tarifaPadraoMes.
	 *
	 * @return tarifaPadraoMes
	 */
	public BigDecimal getTarifaPadraoMes() {
		return tarifaPadraoMes;
	}

	/**
	 * Set: tarifaPadraoMes.
	 *
	 * @param tarifaPadraoMes the tarifa padrao mes
	 */
	public void setTarifaPadraoMes(BigDecimal tarifaPadraoMes) {
		this.tarifaPadraoMes = tarifaPadraoMes;
	}

	/**
	 * Get: tarifaPropostaMes.
	 *
	 * @return tarifaPropostaMes
	 */
	public BigDecimal getTarifaPropostaMes() {
		return tarifaPropostaMes;
	}

	/**
	 * Set: tarifaPropostaMes.
	 *
	 * @param tarifaPropostaMes the tarifa proposta mes
	 */
	public void setTarifaPropostaMes(BigDecimal tarifaPropostaMes) {
		this.tarifaPropostaMes = tarifaPropostaMes;
	}

	/**
	 * Get: receitaFloatingCliente.
	 *
	 * @return receitaFloatingCliente
	 */
	public BigDecimal getReceitaFloatingCliente() {
		return receitaFloatingCliente;
	}

	/**
	 * Set: receitaFloatingCliente.
	 *
	 * @param receitaFloatingCliente the receita floating cliente
	 */
	public void setReceitaFloatingCliente(BigDecimal receitaFloatingCliente) {
		this.receitaFloatingCliente = receitaFloatingCliente;
	}

	/**
	 * Get: total.
	 *
	 * @return total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * Set: total.
	 *
	 * @param total the total
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	/**
	 * Get: cdFluxoNegocio.
	 *
	 * @return cdFluxoNegocio
	 */
	public Integer getCdFluxoNegocio() {
		return cdFluxoNegocio;
	}

	/**
	 * Set: cdFluxoNegocio.
	 *
	 * @param cdFluxoNegocio the cd fluxo negocio
	 */
	public void setCdFluxoNegocio(Integer cdFluxoNegocio) {
		this.cdFluxoNegocio = cdFluxoNegocio;
	}

	/**
	 * Get: cdGrupoInfoFluxo.
	 *
	 * @return cdGrupoInfoFluxo
	 */
	public Integer getCdGrupoInfoFluxo() {
		return cdGrupoInfoFluxo;
	}

	/**
	 * Set: cdGrupoInfoFluxo.
	 *
	 * @param cdGrupoInfoFluxo the cd grupo info fluxo
	 */
	public void setCdGrupoInfoFluxo(Integer cdGrupoInfoFluxo) {
		this.cdGrupoInfoFluxo = cdGrupoInfoFluxo;
	}

	/**
	 * Get: cdOperacaoProdutoServico.
	 *
	 * @return cdOperacaoProdutoServico
	 */
	public Integer getCdOperacaoProdutoServico() {
		return cdOperacaoProdutoServico;
	}

	/**
	 * Set: cdOperacaoProdutoServico.
	 *
	 * @param cdOperacaoProdutoServico the cd operacao produto servico
	 */
	public void setCdOperacaoProdutoServico(Integer cdOperacaoProdutoServico) {
		this.cdOperacaoProdutoServico = cdOperacaoProdutoServico;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdProdutoOperacaoDeflt.
	 *
	 * @return cdProdutoOperacaoDeflt
	 */
	public Integer getCdProdutoOperacaoDeflt() {
		return cdProdutoOperacaoDeflt;
	}

	/**
	 * Set: cdProdutoOperacaoDeflt.
	 *
	 * @param cdProdutoOperacaoDeflt the cd produto operacao deflt
	 */
	public void setCdProdutoOperacaoDeflt(Integer cdProdutoOperacaoDeflt) {
		this.cdProdutoOperacaoDeflt = cdProdutoOperacaoDeflt;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdTipoCanal.
	 *
	 * @return cdTipoCanal
	 */
	public Integer getCdTipoCanal() {
		return cdTipoCanal;
	}

	/**
	 * Set: cdTipoCanal.
	 *
	 * @param cdTipoCanal the cd tipo canal
	 */
	public void setCdTipoCanal(Integer cdTipoCanal) {
		this.cdTipoCanal = cdTipoCanal;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrOcorGrupoProposta.
	 *
	 * @return nrOcorGrupoProposta
	 */
	public Integer getNrOcorGrupoProposta() {
		return nrOcorGrupoProposta;
	}

	/**
	 * Set: nrOcorGrupoProposta.
	 *
	 * @param nrOcorGrupoProposta the nr ocor grupo proposta
	 */
	public void setNrOcorGrupoProposta(Integer nrOcorGrupoProposta) {
		this.nrOcorGrupoProposta = nrOcorGrupoProposta;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: numeroOcorrencias.
	 *
	 * @return numeroOcorrencias
	 */
	public Integer getNumeroOcorrencias() {
		return numeroOcorrencias;
	}

	/**
	 * Set: numeroOcorrencias.
	 *
	 * @param numeroOcorrencias the numero ocorrencias
	 */
	public void setNumeroOcorrencias(Integer numeroOcorrencias) {
		this.numeroOcorrencias = numeroOcorrencias;
	}
	
	
	

}
