/*
 * Nome: br.com.bradesco.web.pgit.view.validators
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.validators;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.validators.base.BradescoValidator;
import br.com.bradesco.web.pgit.utils.NumberUtils;

/**
 * Nome: HoraValidator
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class HoraValidator extends BradescoValidator {

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.aq.view.validators.base.BradescoValidator#validate(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if (value != null && (value instanceof String)) {
			if (!NumberUtils.validateHora((String) value)) {
				BradescoFacesUtils.addInfoModalMessage(MessageHelperUtils.getI18nMessage("error.hora.pattern"), false);
				addErrorMessage(new String[0], "error.hora.pattern", context, component);
			}
		}
	}
}
