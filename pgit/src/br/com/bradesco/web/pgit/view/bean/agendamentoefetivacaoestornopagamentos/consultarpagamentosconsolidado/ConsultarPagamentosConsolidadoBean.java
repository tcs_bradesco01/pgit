/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.consultarpagamentosconsolidado
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.consultarpagamentosconsolidado;

import static br.com.bradesco.web.pgit.utils.CpfCnpjUtils.formatCpfCnpjCompleto;
import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaStringToIntegerNula;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.IConsultaAutorizanteService;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesNetEmpresaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesNetEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultaautorizante.bean.ConsultarAutorizantesOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.IConsultarPagamentosConsolidadoService;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.ConsultarPagamentosConsolidadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.ConsultarPagamentosConsolidadosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.DetalharPagamentosConsolidadosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.DetalharPagamentosConsolidadosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosconsolidado.bean.OcorrenciasDetalharPagamentosConsolidados;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarHistConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarHistConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarManutencaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarManutencaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharHistConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharHistConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharManPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ListarTipoRetiradaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;

/**
 * Nome: ConsultarPagamentosConsolidadoBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ConsultarPagamentosConsolidadoBean extends PagamentosBean {
    
    /**
     * Atributo DetalharPagamentosConsolidadosEntradaDTO
     */
    private DetalharPagamentosConsolidadosEntradaDTO entradaDetalhe;

    /** Atributo CODIGO_IDENTIFICACAO_DIVERSOS. */
    private static final String CODIGO_IDENTIFICACAO_DIVERSOS = "4";
    
    private static final Integer NUMERO_ZERO = 0;
    
    private static final String VAZIO = "";
    
    private static final Long NUMERO_ZERO_LONG = 0l;

    /** Atributo consultaAutorizanteService. */
    private IConsultaAutorizanteService consultaAutorizanteService;
    // Gerais
    /** Atributo disableArgumentosConsulta. */
    private boolean disableArgumentosConsulta;

    /** Atributo consultarPagamentosConsolidadoServiceImpl. */
    private IConsultarPagamentosConsolidadoService consultarPagamentosConsolidadoServiceImpl;

    /** Atributo consultasService. */
    private IConsultasService consultasService;

    // conPagamentosConsolidado
    /** Atributo listaGridConPagamentosConsolidado. */
    private List<ConsultarPagamentosConsolidadosSaidaDTO> listaGridConPagamentosConsolidado;

    /** Atributo listaRadiosConPagamentosConsolidado. */
    private List<SelectItem> listaRadiosConPagamentosConsolidado;

    /** Atributo itemSelecionadoListaConPagamentosConsolidado. */
    private Integer itemSelecionadoListaConPagamentosConsolidado;

    // detPagamentosConsolidadoFiltro
    /** Atributo listaGridDetPagamentosConsolidado. */
    private List<OcorrenciasDetalharPagamentosConsolidados> listaGridDetPagamentosConsolidado;

    /** Atributo listaRadiosDetPagamentosConsolidado. */
    private List<SelectItem> listaRadiosDetPagamentosConsolidado;

    /** Atributo itemSelecionadoListaDetPagamentosConsolidado. */
    private Integer itemSelecionadoListaDetPagamentosConsolidado;

    /** Atributo tipoTela. */
    private Integer tipoTela;

    /** Atributo nrCnpjCpf. */
    private String nrCnpjCpf;

    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;

    /** The ds identificador transferencia pagto. */
    private String dsIdentificadorTransferenciaPagto;

    /** Atributo dsEmpresa. */
    private String dsEmpresa;

    /** Atributo nroContrato. */
    private String nroContrato;

    /** Atributo dsContrato. */
    private String dsContrato;

    /** Atributo cdSituacaoContrato. */
    private String cdSituacaoContrato;

    /** Atributo dsTipoServico. */
    private String dsTipoServico;

    /** Atributo dsEstornoPagamento. */
    private String dsEstornoPagamento;

    /** Atributo dsIndicadorModalidade. */
    private String dsIndicadorModalidade;

    /** Atributo dscRazaoSocial. */
    private String dscRazaoSocial;

    /** Atributo dsQtdePagamentos. */
    private String dsQtdePagamentos;

    /** Atributo dsVlrTotalPagamentos. */
    private BigDecimal dsVlrTotalPagamentos;

    /** Atributo dtPagamentoFiltro. */
    private String dtPagamentoFiltro;

    /** Atributo nrRemessaFiltro. */
    private Long nrRemessaFiltro;

    /** Atributo dsSituacaoPagamentoFiltro. */
    private String dsSituacaoPagamentoFiltro;

    // relatorio
    /** Atributo logoPath. */
    private String logoPath;

    // detPagamentosConsolidados
    // Conta de D�bito
    /** Atributo dsBancoDebito. */
    private String dsBancoDebito;

    /** Atributo dsBancoPagto. */
    private String dsBancoPagto;

    /** Atributo dsAgenciaDebito. */
    private String dsAgenciaDebito;

    /** Atributo dsAgenciaPagto. */
    private String dsAgenciaPagto;

    /** Atributo contaDebito. */
    private String contaDebito;

    /** Atributo contaPagto. */
    private String contaPagto;

    /** Atributo tipoContaDebito. */
    private String tipoContaDebito;

    /** Atributo descricaoTipoContaDebito. */
    private String descricaoTipoContaDebito;

    /** Atributo descricaoBancoContaDebito. */
    private String descricaoBancoContaDebito;

    /** Atributo descricaoAgenciaContaDebitoBancaria. */
    private String descricaoAgenciaContaDebitoBancaria;

    /** Atributo digAgenciaDebito. */
    private String digAgenciaDebito;

    /** Atributo digContaDebito. */
    private String digContaDebito;

    /** Atributo numControleInternoLote. */
    private Long numControleInternoLote;

    // Favorecido
    /** Atributo numeroInscricaoFavorecido. */
    private String numeroInscricaoFavorecido;

    /** Atributo tipoFavorecido. */
    private String tipoFavorecido;

    /** Atributo dsFavorecido. */
    private String dsFavorecido;

    /** Atributo cdFavorecido. */
    private String cdFavorecido;

    /** Atributo dsBancoFavorecido. */
    private String dsBancoFavorecido;

    /** Atributo dsAgenciaFavorecido. */
    private String dsAgenciaFavorecido;

    /** Atributo dsContaFavorecido. */
    private String dsContaFavorecido;

    /** Atributo dsTipoContaFavorecido. */
    private String dsTipoContaFavorecido;

    // Benefici�rio
    /** Atributo numeroInscricaoBeneficiario. */
    private String numeroInscricaoBeneficiario;

    /** Atributo tipoBeneficiario. */
    private String tipoBeneficiario;

    /** Atributo nomeBeneficiario. */
    private String nomeBeneficiario;

    /** Atributo dsTipoBeneficiario. */
    private String dsTipoBeneficiario;

    /** Atributo dtNascimentoBeneficiario. */
    private String dtNascimentoBeneficiario;

    // Pagamento
    /** Atributo nrSequenciaArquivoRemessa. */
    private String nrSequenciaArquivoRemessa;

    /** Atributo nrLoteArquivoRemessa. */
    private String nrLoteArquivoRemessa;

    /** Atributo dsTipoLayout. */
    private String dsTipoLayout;

    /** Atributo nrLote. */
    private String nrLote;

    /** Atributo numeroPagamento. */
    private String numeroPagamento;

    /** Atributo cdListaDebito. */
    private String cdListaDebito;

    /** Atributo dtAgendamento. */
    private String dtAgendamento;

    /** Atributo dtPagamento. */
    private String dtPagamento;

    /** Atributo dtDevolucaoEstorno. */
    private String dtDevolucaoEstorno;

    /** Atributo dtPagamentoGare. */
    private Date dtPagamentoGare;

    /** Atributo dtPagamentoManutencao. */
    private Date dtPagamentoManutencao;

    /** Atributo dtVencimento. */
    private String dtVencimento;

    /** Atributo cdIndicadorEconomicoMoeda. */
    private String cdIndicadorEconomicoMoeda;

    /** Atributo qtMoeda. */
    private BigDecimal qtMoeda;

    /** Atributo vlrAgendamento. */
    private BigDecimal vlrAgendamento;

    /** Atributo vlrEfetivacao. */
    private BigDecimal vlrEfetivacao;

    /** Atributo cdSituacaoOperacaoPagamento. */
    private String cdSituacaoOperacaoPagamento;

    /** Atributo dsMotivoSituacao. */
    private String dsMotivoSituacao;

    /** Atributo dsIndicadorAutorizacao. */
    private String dsIndicadorAutorizacao;

    /** Atributo dsPagamento. */
    private String dsPagamento;

    /** Atributo nrDocumento. */
    private String nrDocumento;

    /** Atributo tipoDocumento. */
    private String tipoDocumento;

    /** Atributo cdSerieDocumento. */
    private String cdSerieDocumento;

    /** Atributo vlDocumento. */
    private BigDecimal vlDocumento;

    /** Atributo dtEmissaoDocumento. */
    private String dtEmissaoDocumento;

    /** Atributo dsUsoEmpresa. */
    private String dsUsoEmpresa;

    /** Atributo dsMensagemPrimeiraLinha. */
    private String dsMensagemPrimeiraLinha;

    /** Atributo dsMensagemSegundaLinha. */
    private String dsMensagemSegundaLinha;

    /** Atributo cdBancoCredito. */
    private String cdBancoCredito;

    /** Atributo dsBancoCredito. */
    private String dsBancoCredito;

    /** Atributo cdIspbPagamento. */
    private String cdIspbPagamento;	

    /** Atributo dddContribuinte. */
    private String dddContribuinte;

    /** Atributo telefoneContribuinte. */
    private String telefoneContribuinte;

    /** Atributo inscricaoEstadualContribuinte. */
    private String inscricaoEstadualContribuinte;

    /** Atributo codigoBarras. */
    private String codigoBarras;

    /** Atributo codigoReceita. */
    private String codigoReceita;

    /** Atributo agenteArrecadador. */
    private String agenteArrecadador;

    /** Atributo codigoTributo. */
    private String codigoTributo;

    /** Atributo numeroReferencia. */
    private String numeroReferencia;

    /** Atributo numeroCotaParcela. */
    private String numeroCotaParcela;

    /** Atributo percentualReceita. */
    private BigDecimal percentualReceita;

    /** Atributo periodoApuracao. */
    private String periodoApuracao;

    /** Atributo anoExercicio. */
    private String anoExercicio;

    /** Atributo dataReferencia. */
    private String dataReferencia;

    /** Atributo dataCompetencia. */
    private String dataCompetencia;

    /** Atributo codigoCnae. */
    private String codigoCnae;

    /** Atributo placaVeiculo. */
    private String placaVeiculo;

    /** Atributo numeroParcelamento. */
    private String numeroParcelamento;

    /** Atributo codigoOrgaoFavorecido. */
    private String codigoOrgaoFavorecido;

    /** Atributo nomeOrgaoFavorecido. */
    private String nomeOrgaoFavorecido;

    /** Atributo codigoUfFavorecida. */
    private String codigoUfFavorecida;

    /** Atributo descricaoUfFavorecida. */
    private String descricaoUfFavorecida;

    /** Atributo vlReceita. */
    private BigDecimal vlReceita;

    /** Atributo vlPrincipal. */
    private BigDecimal vlPrincipal;

    /** Atributo vlAtualizacaoMonetaria. */
    private BigDecimal vlAtualizacaoMonetaria;

    /** Atributo vlAcrescimoFinanceiro. */
    private BigDecimal vlAcrescimoFinanceiro;

    /** Atributo vlHonorarioAdvogado. */
    private BigDecimal vlHonorarioAdvogado;

    /** Atributo vlDesconto. */
    private BigDecimal vlDesconto;

    /** Atributo vlAbatimento. */
    private BigDecimal vlAbatimento;

    /** Atributo vlMulta. */
    private BigDecimal vlMulta;

    /** Atributo vlMora. */
    private BigDecimal vlMora;

    /** Atributo vlTotal. */
    private BigDecimal vlTotal;

    /** Atributo observacaoTributo. */
    private String observacaoTributo;

    /** Atributo agenciaDigitoCredito. */
    private String agenciaDigitoCredito;

    /** Atributo contaDigitoCredito. */
    private String contaDigitoCredito;

    /** Atributo tipoContaCredito. */
    private String tipoContaCredito;

    /** Atributo cdBancoDestino. */
    private String cdBancoDestino;

    /** Atributo agenciaDigitoDestino. */
    private String agenciaDigitoDestino;

    /** Atributo contaDigitoDestino. */
    private String contaDigitoDestino;

    /** Atributo tipoContaDestino. */
    private String tipoContaDestino;

    /** Atributo vlDeducao. */
    private BigDecimal vlDeducao;

    /** Atributo vlAcrescimo. */
    private BigDecimal vlAcrescimo;

    /** Atributo vlImpostoRenda. */
    private BigDecimal vlImpostoRenda;

    /** Atributo vlIss. */
    private BigDecimal vlIss;

    /** Atributo vlInss. */
    private BigDecimal vlInss;

    /** Atributo vlIof. */
    private BigDecimal vlIof;

    /** Atributo codigoRenavam. */
    private String codigoRenavam;

    /** Atributo municipioTributo. */
    private String municipioTributo;

    /** Atributo ufTributo. */
    private String ufTributo;

    /** Atributo numeroNsu. */
    private String numeroNsu;

    /** Atributo opcaoTributo. */
    private String opcaoTributo;

    /** Atributo opcaoRetiradaTributo. */
    private String opcaoRetiradaTributo;

    /** Atributo clienteDepositoIdentificado. */
    private String clienteDepositoIdentificado;

    /** Atributo tipoDespositoIdentificado. */
    private String tipoDespositoIdentificado;

    /** Atributo produtoDepositoIdentificado. */
    private String produtoDepositoIdentificado;

    /** Atributo sequenciaProdutoDepositoIdentificado. */
    private String sequenciaProdutoDepositoIdentificado;

    /** Atributo bancoCartaoDepositoIdentificado. */
    private String bancoCartaoDepositoIdentificado;

    /** Atributo cartaoDepositoIdentificado. */
    private String cartaoDepositoIdentificado;

    /** Atributo bimCartaoDepositoIdentificado. */
    private String bimCartaoDepositoIdentificado;

    /** Atributo viaCartaoDepositoIdentificado. */
    private String viaCartaoDepositoIdentificado;

    /** Atributo codigoDepositante. */
    private String codigoDepositante;

    /** Atributo nomeDepositante. */
    private String nomeDepositante;

    /** Atributo camaraCentralizadora. */
    private String camaraCentralizadora;

    /** Atributo finalidadeDoc. */
    private String finalidadeDoc;

    /** Atributo identificacaoTransferenciaDoc. */
    private String identificacaoTransferenciaDoc;

    /** Atributo identificacaoTitularidade. */
    private String identificacaoTitularidade;

    /** Atributo codigoInss. */
    private String codigoInss;

    /** Atributo vlOutrasEntidades. */
    private BigDecimal vlOutrasEntidades;

    /** Atributo codigoPagador. */
    private String codigoPagador;

    /** Atributo codigoOrdemCredito. */
    private String codigoOrdemCredito;

    /** Atributo numeroOp. */
    private String numeroOp;

    /** Atributo dataExpiracaoCredito. */
    private String dataExpiracaoCredito;

    /** Atributo instrucaoPagamento. */
    private String instrucaoPagamento;

    /** Atributo numeroCheque. */
    private String numeroCheque;

    /** Atributo serieCheque. */
    private String serieCheque;

    /** Atributo indicadorAssociadoUnicaAgencia. */
    private String indicadorAssociadoUnicaAgencia;

    /** Atributo identificadorTipoRetirada. */
    private String identificadorTipoRetirada;

    /** Atributo identificadorRetirada. */
    private String identificadorRetirada;

    /** Atributo dataRetirada. */
    private String dataRetirada;

    /** Atributo vlImpostoMovFinanceira. */
    private BigDecimal vlImpostoMovFinanceira;

    /** Atributo finalidadeTed. */
    private String finalidadeTed;

    /** Atributo identificacaoTransferenciaTed. */
    private String identificacaoTransferenciaTed;

    /** Atributo nossoNumero. */
    private String nossoNumero;

    /** Atributo dsOrigemPagamento. */
    private String dsOrigemPagamento;

    /** Atributo cdIndentificadorJudicial. */
    private String cdIndentificadorJudicial;

    /** Atributo dtLimiteDescontoPagamento. */
    private String dtLimiteDescontoPagamento;

    /** Atributo dtLimitePagamento. */
    private String dtLimitePagamento;

    /** Atributo dsRastreado. */
    private String dsRastreado;

    /** Atributo carteira. */
    private String carteira;

    /** Atributo cdSituacaoTranferenciaAutomatica. */
    private String cdSituacaoTranferenciaAutomatica;

    /** Atributo descTipoFavorecido. */
    private String descTipoFavorecido;

    // Trilha
    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao;

    /** Atributo usuarioInclusao. */
    private String usuarioInclusao;

    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;

    /** Atributo complementoInclusao. */
    private String complementoInclusao;

    /** Atributo dataHoraManutencao. */
    private String dataHoraManutencao;

    /** Atributo usuarioManutencao. */
    private String usuarioManutencao;

    /** Atributo tipoCanalManutencao. */
    private String tipoCanalManutencao;

    /** Atributo complementoManutencao. */
    private String complementoManutencao;

    /** Atributo exibeBeneficiario. */
    private boolean exibeBeneficiario;

    /** Atributo detalharPagamentosConsolidadosSaidaDTO. */
    private DetalharPagamentosConsolidadosSaidaDTO detalharPagamentosConsolidadosSaidaDTO;

    // vari�veis detalhar manuten��o;

    /** Atributo listaConsultarManutencao. */
    private List<ConsultarManutencaoPagamentoSaidaDTO> listaConsultarManutencao;

    /** Atributo valorPagamentoFormatado. */
    private String valorPagamentoFormatado;

    /** Atributo canal. */
    private String canal;

    /** Atributo dsTipoManutencao. */
    private String dsTipoManutencao;

    /** Atributo manutencao. */
    private boolean manutencao;

    /** Atributo situacaoPagamento. */
    private String situacaoPagamento;

    /** Atributo motivoPagamento. */
    private String motivoPagamento;

    /** Atributo inscricaoFavorecido. */
    private String inscricaoFavorecido;

    /** Atributo cdTipoInscricaoFavorecido. */
    private String cdTipoInscricaoFavorecido;

    /** Atributo tipoContaFavorecido. */
    private String tipoContaFavorecido;

    /** Atributo bancoFavorecido. */
    private String bancoFavorecido;

    /** Atributo agenciaFavorecido. */
    private String agenciaFavorecido;

    /** Atributo contaFavorecido. */
    private String contaFavorecido;

    /** Atributo tipoFavorecidoManutencao. */
    private Integer tipoFavorecidoManutencao;

    /** Atributo listaManutencaoControle. */
    private List<SelectItem> listaManutencaoControle;

    /** Atributo itemSelecionadoListaManutencao. */
    private Integer itemSelecionadoListaManutencao;

    // hisConsultaSaldo
    /** Atributo listaHistoricoConsulta. */
    private List<ConsultarHistConsultaSaldoSaidaDTO> listaHistoricoConsulta;

    /** Atributo itemSelecionadoListaHistorico. */
    private Integer itemSelecionadoListaHistorico;

    /** Atributo listaPesquisaHistoricoControle. */
    private List<SelectItem> listaPesquisaHistoricoControle;

    // detConsultaSaldo
    /** Atributo dataHoraConsultaSaldo. */
    private String dataHoraConsultaSaldo;

    /** Atributo valorPagamento. */
    private BigDecimal valorPagamento;
    
    /** Atributo sinal. */
    private String sinal;

    /** Atributo saldoMomentoLancamento. */
    private BigDecimal saldoMomentoLancamento;

    /** Atributo saldoOperacional. */
    private BigDecimal saldoOperacional;

    /** Atributo saldoVincSemReserva. */
    private BigDecimal saldoVincSemReserva;

    /** Atributo saldoVincComReserva. */
    private BigDecimal saldoVincComReserva;

    /** Atributo saldoVincJudicial. */
    private BigDecimal saldoVincJudicial;

    /** Atributo saldoVincAdm. */
    private BigDecimal saldoVincAdm;

    /** Atributo saldoVincSeguranca. */
    private BigDecimal saldoVincSeguranca;

    /** Atributo saldoVincAdmLp. */
    private BigDecimal saldoVincAdmLp;

    /** Atributo saldoAgregadoFundo. */
    private BigDecimal saldoAgregadoFundo;

    /** Atributo saldoAgregadoCDB. */
    private BigDecimal saldoAgregadoCDB;

    /** Atributo saldoAgregadoPoupanca. */
    private BigDecimal saldoAgregadoPoupanca;

    /** Atributo saldoAgregadoCredito. */
    private BigDecimal saldoAgregadoCredito;

    /** Atributo saldoBonusCPMF. */
    private BigDecimal saldoBonusCPMF;

    /** Atributo cdBancoOriginal. */
    private Integer cdBancoOriginal;

    /** Atributo dsBancoOriginal. */
    private String dsBancoOriginal;

    /** Atributo cdAgenciaBancariaOriginal. */
    private Integer cdAgenciaBancariaOriginal;

    /** Atributo cdDigitoAgenciaOriginal. */
    private String cdDigitoAgenciaOriginal;

    /** Atributo dsAgenciaOriginal. */
    private String dsAgenciaOriginal;

    /** Atributo cdContaBancariaOriginal. */
    private Long cdContaBancariaOriginal;

    /** Atributo cdDigitoContaOriginal. */
    private String cdDigitoContaOriginal;

    /** Atributo dsTipoContaOriginal. */
    private String dsTipoContaOriginal;

    /** Atributo dtFloatingPagamento. */
    private String dtFloatingPagamento;

    /** Atributo dtEfetivFloatPgto. */
    private String dtEfetivFloatPgto;

    /** Atributo vlFloatingPagamento. */
    private BigDecimal vlFloatingPagamento;

    /** Atributo cdOperacaoDcom. */
    private Long cdOperacaoDcom;

    /** Atributo dsSituacaoDcom. */
    private String dsSituacaoDcom;

    /** Atributo vlBonfcao. */
    private BigDecimal vlBonfcao;

    /** Atributo vlBonificacao. */
    private BigDecimal vlBonificacao;

    /** Atributo cdFuncEmissaoOp. */
    private String cdFuncEmissaoOp;

    /** Atributo cdTerminalCaixaEmissaoOp. */
    private String cdTerminalCaixaEmissaoOp;

    /** Atributo dtEmissaoOp. */
    private String dtEmissaoOp;

    /** Atributo hrEmissaoOp. */
    private String hrEmissaoOp;

    /** Atributo exibeDcom. */
    private Boolean exibeDcom;

    /** The vl efetivacao debito pagamento. */
    private BigDecimal vlEfetivacaoDebitoPagamento;

    /** The vl efetivacao credito pagamento. */
    private BigDecimal vlEfetivacaoCreditoPagamento;

    /** The vl desconto pagamento. */
    private BigDecimal vlDescontoPagamento;

    /** The hr efetivacao credito pagamento. */
    private String hrEfetivacaoCreditoPagamento;

    /** The hr envio credito pagamento. */
    private String hrEnvioCreditoPagamento;

    /** The cd identificador transferencia pagto. */
    private Integer cdIdentificadorTransferenciaPagto;

    /** The cd mensagem lin extrato. */
    private Integer cdMensagemLinExtrato;

    /** The cd conve cta salarial. */
    private Long cdConveCtaSalarial;

    /** The flag exibe campos. */
    private boolean flagExibeCampos; 
    
    /** Atributo consultarAutorizantesOcorrencia. */
    private ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO> consultarAutorizantesOcorrencia =  
        new ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO>();
    
    /** Atributo itemSelecionadoListaAutorizante. */
    private Integer itemSelecionadoListaAutorizante;
    
    /** Atributo listaRadioAutorizante. */
    private ArrayList<SelectItem> listaRadioAutorizante;

    /** Atributo detalharPagtoTed. */
    private DetalharPagtoTEDSaidaDTO detalharPagtoTed = null;
    
    /** Atributo dsNomeReclamante. */
    private String dsNomeReclamante = null;
    
    /** Atributo nrProcessoDepositoJudicial. */
    private String nrProcessoDepositoJudicial = null;
    
    /** Atributo nrPisPasep. */
    private String nrPisPasep = null; 
    
    /** Atributo cdFinalidadeTedDepositoJudicial. */
    private boolean cdFinalidadeTedDepositoJudicial = false;

    /** Atributo detalheOrdemPagamento. */
    private DetalharPagtoOrdemPagtoSaidaDTO detalheOrdemPagamento = null; 

    /** Atributo listaFormaPagamento. */
    private List<ListarTipoRetiradaOcorrenciasSaidaDTO> listaFormaPagamento = null;
    
    private String cdBancoPagamento;
    private String ispb;
	private String cdContaPagamento;
	private String dsISPB;
	private String tipoContaDestinoPagamento;
    private String cdBancoDestinoPagamento;
	private String contaDigitoDestinoPagamento;
	private boolean exibeBancoCredito;
	private String cdContaPagamentoDet;
    
    /** Atributo numeroInscricaoSacadorAvalista. */
    private String numeroInscricaoSacadorAvalista;

    /** Atributo dsSacadorAvalista. */
    private String dsSacadorAvalista;

    /** Atributo dsTipoSacadorAvalista. */
    private String dsTipoSacadorAvalista;
	
	
	
    /**
     * Iniciar tela.
     *
     * @param e the e
     */
    public void iniciarTela(ActionEvent e) {
        limparTudo();

        // Tela de Filtro
        getFiltroAgendamentoEfetivacaoEstornoBean()
        .setEntradaConsultarListaClientePessoas(
            new ConsultarListaClientePessoasEntradaDTO());
        getFiltroAgendamentoEfetivacaoEstornoBean()
        .setSaidaConsultarListaClientePessoas(
            new ConsultarListaClientePessoasSaidaDTO());
        getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
        getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
        getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
        "conPagamentosConsolidado");

        // Carregamento dos Combos
        listarTipoServicoPagto();
        listarConsultarSituacaoPagamento();
        preencheTipoLayoutArquivo();

        setDisableArgumentosConsulta(false);
        setHabilitaArgumentosPesquisa(false);
        getFiltroAgendamentoEfetivacaoEstornoBean()
        .setClienteContratoSelecionado(false);
        getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(
            2269651L);
    }

    /**
     * Limpar args lista apos pesquisa cliente contrato.
     * 
     * @param evt
     *            the evt
     */
    public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
        limparCampos();
        setListaGridConPagamentosConsolidado(null);
        setItemSelecionadoListaConPagamentosConsolidado(null);
    }

    /**
     * Limpar argumentos pesquisa.
     */
    public void limparArgumentosPesquisa() {
        limparCampos();
        setHabilitaArgumentosPesquisa(false);
        setListaGridConPagamentosConsolidado(null);
        setItemSelecionadoListaConPagamentosConsolidado(null);
    }

    /**
     * Limpar apos alterar opcao cliente.
     * 
     * @return the string
     */
    public String limparAposAlterarOpcaoCliente() {
        // Limpar Argumentos de Pesquisa e Lista
        getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
        setListaGridConPagamentosConsolidado(null);
        setItemSelecionadoListaConPagamentosConsolidado(null);

        limparCampos();

        return "";

    }

    /**
     * Limpar grid.
     * 
     * @return the string
     */
    public String limparGrid() {
        setListaGridConPagamentosConsolidado(null);
        setItemSelecionadoListaConPagamentosConsolidado(null);
        setDisableArgumentosConsulta(false);
        return "";
    }

    /**
     * Limpar tela principal.
     */
    public void limparTelaPrincipal() {
        limparFiltroPrincipal();
        setListaGridConPagamentosConsolidado(null);
        setItemSelecionadoListaConPagamentosConsolidado(null);
    }

    /**
     * Limpar tudo.
     * 
     * @return the string
     */
    public String limparTudo() {
        limparCampos();
        setListaGridConPagamentosConsolidado(null);
        setListaGridDetPagamentosConsolidado(null);
        setItemSelecionadoListaConPagamentosConsolidado(null);
        limparVariaveisFiltroDetalhes();
        limparVariaveisDetalhes();
        getFiltroAgendamentoEfetivacaoEstornoBean().limparFiltroPrincipal();
        getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(
            null);
        setHabilitaArgumentosPesquisa(false);
        setDisableArgumentosConsulta(false);
        setChkRastreamentoTitulo(false);
        setRadioRastreados(null);
        return "";
    }

    // Navega��o
    /**
     * Detalhar.
     * 
     * @return the string
     */
    public String detalhar() {
        try {
            setManutencao(false);
            tipoTela = listaGridDetPagamentosConsolidado.get(
                getItemSelecionadoListaDetPagamentosConsolidado())
                .getCdTipoTela();

            limparVariaveisDetalhes();

            if (tipoTela == 1) {
                preencheDadosPagtoCreditoConta();
                return "DETALHE_CREDITO_CONTA";
            }
            if (tipoTela == 2) {
                preencheDadosPagtoDebitoConta();
                return "DETALHE_DEBITO_CONTA";
            }
            if (tipoTela == 3) {
                preencheDadosPagtoDOC();
                return "DETALHE_DOC";
            }
            if (tipoTela == 4) {
                preencheDadosPagtoTED();
                return "DETALHE_TED";
            }
            if (tipoTela == 5) {
                preencheDadosPagtoOrdemPagto();
                return "DETALHE_ORDEM_PAGTO";
            }
            if (tipoTela == 6) {
                preencheDadosPagtoTituloBradesco();
                return "DETALHE_TITULOS_BRADESCO";
            }
            if (tipoTela == 7) {
                preencheDadosPagtoTituloOutrosBancos();
                return "DETALHE_TITULOS_OUTROS_BANCOS";
            }
            if (tipoTela == 8) {
                preencheDadosPagtoDARF();
                return "DETALHE_DARF";
            }
            if (tipoTela == 9) {
                preencheDadosPagtoGARE();
                return "DETALHE_GARE";
            }
            if (tipoTela == 10) {
                preencheDadosPagtoGPS();
                return "DETALHE_GPS";
            }
            if (tipoTela == 11) {
                preencheDadosPagtoCodigoBarras();
                return "DETALHE_CODIGO_BARRAS";
            }
            if (tipoTela == 12) {
                preencheDadosPagtoDebitoVeiculos();
                return "DETALHE_DEBITO_VEICULOS";
            }
            if (tipoTela == 13) {
                preencheDadosPagtoDepIdentificado();
                return "DETALHE_DEPOSITO_IDENTIFICADO";
            }
            if (tipoTela == 14) {
                preencheDadosPagtoOrdemCredito();
                return "DETALHE_ORDEM_CREDITO";
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }

        return "";
    }

    /**
     * Detalhar filtro.
     * 
     * @return the string
     */
    public String detalharFiltro() {

        preencheDadosFiltroDetalharPagamentosConsolidados();
        try {
            carregaListaDetalharPagamentosConsolidado();

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridDetPagamentosConsolidado(null);
            setItemSelecionadoListaDetPagamentosConsolidado(null);
            return "";
        }
        return "DETALHARFILTRO";
    }

    /**
     * Imprimir.
     * 
     * @return the string
     */
    public String imprimir() {

        try {

            String caminho = "/images/Bradesco_logo.JPG";
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ServletContext servletContext = (ServletContext) facesContext
            .getExternalContext().getContext();
            logoPath = servletContext.getRealPath(caminho);

            carregaCabecalhoImprimir();

            Map<String, Object> par = new HashMap<String, Object>();
            par.put("titulo", "Consultar Pagamentos Consolidados");
            par.put("nrCnpjCpf", getNrCnpjCpf());
            par.put("dsEmpresa", getDsEmpresa());
            par.put("dsContrato", getDsContrato());
            par.put("dsRazaoSocial", getDsRazaoSocial());
            par.put("nroContrato", getNroContrato());
            par.put("cdSituacaoContrato", getCdSituacaoContrato());
            par.put("dsBancoDebito", getDsBancoDebito());
            par.put("dsAgenciaDebito", getDsAgenciaDebito());
            par.put("contaDebito", getContaDebito());
            par.put("tipoContaDebito", getTipoContaDebito());
            par.put("logoBradesco", getLogoPath());

            try {

                // M�todo que gera o relat�rio PDF.

                PgitUtil
                .geraRelatorioPdf(
                    "/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/consolidado",
                    "imprimirPagamentosConsolidados",
                    listaGridConPagamentosConsolidado, par);

            } /* Exce��o do relat�rio */
            catch (Exception e) {
                throw new BradescoViewException(e.getMessage(), e, "", "",
                    BradescoViewExceptionActionType.ACTION);
            }

        } /* Exce��o do PDC */
        catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }

        return "";
    }

    /**
     * Imprimir detalhe.
     * 
     * @return the string
     */
    public String imprimirDetalhe() {
        List<OcorrenciasDetalharPagamentosConsolidados> listaImpressaoDetalhe = null;
        try {

            String caminho = "/images/Bradesco_logo.JPG";
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ServletContext servletContext = (ServletContext) facesContext
            .getExternalContext().getContext();
            logoPath = servletContext.getRealPath(caminho);

            preencheDadosFiltroDetalharPagamentosConsolidados();

            Map<String, Object> par = new HashMap<String, Object>();
            par.put("titulo", "Detalhar Pagamentos Consolidados");
            par.put("cpfCnpj", getNrCnpjCpf());
            par.put("empresa", getDsEmpresa());
            par.put("descContrato", getDsContrato());
            par.put("nomeRazaoSocial", getDsRazaoSocial());
            par.put("numero", getNroContrato());
            par.put("situacao", getCdSituacaoContrato());
            par.put("bancoDebito", getDsBancoDebito());
            par.put("agenciaDebito", getDsAgenciaDebito());
            par.put("contaDebito", getContaDebito());
            par.put("tipoContaDebito", getTipoContaDebito());
            par.put("somatoriaQtPagamento", getDsQtdePagamentos());
            par.put("somatoriaVlEfetivo", getDsVlrTotalPagamentos());
            par.put("tipoServico", getDsTipoServico());
            par.put("modalidade", getDsIndicadorModalidade());
            par.put("logoBradesco", getLogoPath());
            par.put("dtPagamentoFiltro", getDtPagamentoFiltro());
            par.put("nrRemessaFiltro", getNrRemessaFiltro());
            par
            .put("dsSituacaoPagamentoFiltro",
                getDsSituacaoPagamentoFiltro());
            
            getConsultarPagamentosConsolidadoServiceImpl().consultarImprimirPgtoConsolidado(getEntradaDetalhe());
            
            listaImpressaoDetalhe = getConsultarPagamentosConsolidadoServiceImpl().imprimirPagamentosConsolidados(entradaDetalhe).getListaOcorrecnias();

            try {

                // M�todo que gera o relat�rio PDF.

                PgitUtil.geraRelatorioPdf("/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/consolidado",
                    "imprimirDetPagtoConsolidado", listaImpressaoDetalhe, par);

            } /* Exce��o do relat�rio */
            catch (Exception e) {
                throw new BradescoViewException(e.getMessage(), e, "", "",
                    BradescoViewExceptionActionType.ACTION);
            }

        } /* Exce��o do PDC */
        catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }

        return "";
    }

    /**
     * Voltar.
     * 
     * @return the string
     */
    public String voltar() {
        setItemSelecionadoListaConPagamentosConsolidado(null);

        return "VOLTAR";
    }

    /**
     * Voltar manutencao.
     * 
     * @return the string
     */
    public String voltarManutencao() {

        detalharFiltro();
        setItemSelecionadoListaDetPagamentosConsolidado(null);

        return "VOLTAR";
    }

    /**
     * Voltar detalhe.
     * 
     * @return the string
     */
    public String voltarDetalhe() {
        if (manutencao) {
            consultarManutencao();
            setItemSelecionadoListaManutencao(null);
        } else {
            try {
                carregaListaDetalharPagamentosConsolidado();
            } catch (PdcAdapterFunctionalException p) {
                BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                    false);
                setItemSelecionadoListaDetPagamentosConsolidado(null);
            }
        }
        return "VOLTAR";
    }    
    

    // Controle de exibi��o de dados
    /**
     * Limpar variaveis filtro detalhes.
     */
    public void limparVariaveisFiltroDetalhes() {
        setNrCnpjCpf("");
        setDsRazaoSocial("");
        setDsEmpresa("");
        setNroContrato("");
        setDsContrato("");
        setCdSituacaoContrato("");
        setDsBancoDebito("");
        setDsAgenciaDebito("");
        setContaDebito("");
        setTipoContaDebito("");
    }

    /**
     * Limpar variaveis detalhes.
     */
    public void limparVariaveisDetalhes() {
        setNumeroInscricaoFavorecido("");
        setTipoFavorecido("");
        setDescTipoFavorecido("");
        setDsFavorecido("");
        setCdFavorecido("");
        setNrLote("");
        setDsBancoFavorecido("");
        setDsAgenciaFavorecido("");
        setDsContaFavorecido("");
        setDsTipoContaFavorecido("");
        setNrSequenciaArquivoRemessa("");
        setNrLoteArquivoRemessa("");
        setNumeroPagamento("");
        setCdListaDebito("");
        setDtAgendamento("");
        setDtPagamento("");
        setDtVencimento("");
        setCdIndicadorEconomicoMoeda("");
        setQtMoeda(null);
        setVlrAgendamento(null);
        setVlrEfetivacao(null);
        setCdSituacaoOperacaoPagamento("");
        setDsMotivoSituacao("");
        setDsPagamento("");
        setNrDocumento("");
        setTipoDocumento("");
        setCdSerieDocumento("");
        setVlDocumento(null);
        setDtEmissaoDocumento("");
        setDsUsoEmpresa("");
        setDsMensagemPrimeiraLinha("");
        setDsMensagemSegundaLinha("");
        setDddContribuinte("");
        setTelefoneContribuinte("");
        setInscricaoEstadualContribuinte("");
        setCodigoBarras("");
        setCodigoReceita("");
        setAgenteArrecadador("");
        setCodigoTributo("");
        setNumeroReferencia("");
        setNumeroCotaParcela("");
        setPercentualReceita(null);
        setPeriodoApuracao("");
        setAnoExercicio("");
        setDataReferencia("");
        setDataCompetencia("");
        setCodigoCnae("");
        setPlacaVeiculo("");
        setNumeroParcelamento("");
        setCodigoOrgaoFavorecido("");
        setNomeOrgaoFavorecido("");
        setCodigoUfFavorecida("");
        setDescricaoUfFavorecida("");
        setVlReceita(null);
        setVlPrincipal(null);
        setVlAtualizacaoMonetaria(null);
        setVlAcrescimoFinanceiro(null);
        setVlHonorarioAdvogado(null);
        setVlDesconto(null);
        setVlAbatimento(null);
        setVlMulta(null);
        setVlMora(null);
        setVlTotal(null);
        setObservacaoTributo("");
        setCdBancoCredito("");
        setAgenciaDigitoCredito("");
        setContaDigitoCredito("");
        setTipoContaCredito("");
        setCdBancoDestino("");
        setAgenciaDigitoDestino("");
        setContaDigitoDestino("");
        setTipoContaDestino("");
        setVlDeducao(null);
        setVlAcrescimo(null);
        setVlImpostoRenda(null);
        setVlIss(null);
        setVlInss(null);
        setVlIof(null);
        setCodigoRenavam("");
        setMunicipioTributo("");
        setUfTributo("");
        setNumeroNsu("");
        setOpcaoTributo("");
        setOpcaoRetiradaTributo("");
        setClienteDepositoIdentificado("");
        setTipoDespositoIdentificado("");
        setProdutoDepositoIdentificado("");
        setSequenciaProdutoDepositoIdentificado("");
        setBancoCartaoDepositoIdentificado("");
        setCartaoDepositoIdentificado("");
        setBimCartaoDepositoIdentificado("");
        setViaCartaoDepositoIdentificado("");
        setCodigoDepositante("");
        setNomeDepositante("");
        setCamaraCentralizadora("");
        setFinalidadeDoc("");
        setIdentificacaoTransferenciaDoc("");
        setIdentificacaoTitularidade("");
        setCodigoInss("");
        setVlOutrasEntidades(null);
        setCodigoPagador("");
        setCodigoOrdemCredito("");
        setNumeroOp("");
        setDataExpiracaoCredito("");
        setInstrucaoPagamento("");
        setNumeroCheque("");
        setSerieCheque("");
        setIndicadorAssociadoUnicaAgencia("");
        setIdentificadorTipoRetirada("");
        setIdentificadorRetirada("");
        setDataRetirada("");
        setVlImpostoMovFinanceira(null);
        setFinalidadeTed("");
        setIdentificacaoTransferenciaTed("");
    }

    // PDC
    /**
     * Carrega lista consultar pagamentos consolidado.
     */
    public void carregaListaConsultarPagamentosConsolidado() {
        try {
            ConsultarPagamentosConsolidadosEntradaDTO entrada = new ConsultarPagamentosConsolidadosEntradaDTO();

            if (getFiltroAgendamentoEfetivacaoEstornoBean()
                            .getTipoFiltroSelecionado().equals("0")
                            || getFiltroAgendamentoEfetivacaoEstornoBean()
                            .getTipoFiltroSelecionado().equals("1")) {
                entrada.setCdTipoPesquisa(1);

                entrada.setCdPessoaJuridicaContrato(0L);
                entrada.setCdTipoContratoNegocio(0);
                entrada.setNrSequenciaContratoNegocio(0L);

                if (getFiltroAgendamentoEfetivacaoEstornoBean()
                                .getCdPessoaJuridicaContrato() != null) {
                    entrada
                    .setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getCdPessoaJuridicaContrato());
                    entrada
                    .setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getNrSequenciaContratoNegocio());
                }

                if (getFiltroAgendamentoEfetivacaoEstornoBean()
                                .getCdTipoContratoNegocio() != null) {
                    entrada
                    .setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getCdTipoContratoNegocio());
                }

                entrada.setCdBanco(0);
                entrada.setCdAgencia(0);
                entrada.setCdConta(0L);
                entrada.setCdDigitoConta("00");
            }
            if (getFiltroAgendamentoEfetivacaoEstornoBean()
                            .getTipoFiltroSelecionado().equals("2")) {
                entrada.setCdTipoPesquisa(2);
                entrada.setCdPessoaJuridicaContrato(0L);
                entrada.setCdTipoContratoNegocio(0);
                entrada.setNrSequenciaContratoNegocio(0L);
                entrada.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getBancoContaDebitoFiltro());
                entrada
                .setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getAgenciaContaDebitoBancariaFiltro());
                entrada.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getContaBancariaContaDebitoFiltro());
                entrada
                .setCdDigitoConta(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getDigitoContaDebitoFiltro());
            } else {
                if (isChkContaDebito()) {
                    entrada.setCdBanco(getCdBancoContaDebito());
                    entrada.setCdAgencia(getCdAgenciaBancariaContaDebito());
                    entrada.setCdConta(getCdContaBancariaContaDebito());

                    entrada.setCdDigitoConta("00");
                    if (getCdDigitoContaContaDebito() != null
                                    && !getCdDigitoContaContaDebito().equals("")) {
                        entrada.setCdDigitoConta(getCdDigitoContaContaDebito());
                    }

                } else {
                    entrada.setCdBanco(0);
                    entrada.setCdAgencia(0);
                    entrada.setCdConta(0l);
                    entrada.setCdDigitoConta("00");
                }

            }

            if (isChkRastreamentoTitulo()) {
                entrada.setCdTituloPgtoRastreado(Integer
                    .parseInt(getRadioRastreados()));
            } else {
                entrada.setCdTituloPgtoRastreado(0);
            }

            entrada.setCdAgendadosPagosNaoPagos(Integer
                .parseInt(getAgendadosPagosNaoPagos()));

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            entrada.setDtCreditoPagamentoInicio("");
            if (getDataInicialPagamentoFiltro() != null) {
                entrada.setDtCreditoPagamentoInicio(sdf
                    .format(getDataInicialPagamentoFiltro()));
            }

            entrada.setDtCreditoPagamentoFim("");
            if (getDataFinalPagamentoFiltro() != null) {
                entrada.setDtCreditoPagamentoFim(sdf
                    .format(getDataFinalPagamentoFiltro()));
            }

            entrada.setNrArquivoRemessaPagamento(0L);
            if (!getNumeroRemessaFiltro().equals("")) {
                entrada.setNrArquivoRemessaPagamento(Long
                    .parseLong(getNumeroRemessaFiltro()));
            }

            entrada.setCdProdutoServicoOperacao(0);
            if (getTipoServicoFiltro() != null) {
                entrada.setCdProdutoServicoOperacao(getTipoServicoFiltro());
            }

            entrada.setCdProdutoServicoRelacionado(0);
            if (getModalidadeFiltro() != null) {
                entrada.setCdProdutoServicoRelacionado(getModalidadeFiltro());
            }

            entrada.setCdSituacaoOperacaoPagamento(0);
            if (getCboSituacaoPagamentoFiltro() != null) {
                entrada
                .setCdSituacaoOperacaoPagamento(getCboSituacaoPagamentoFiltro());
            }

            entrada.setCdMotivoSituacaoPagamento(0);

            entrada.setCdIndicadorAutorizacao(0);
            if (isChkIndicadorAutorizacao()) {
                entrada.setCdIndicadorAutorizacao(Integer
                    .valueOf(getRadioIndicadorAutorizacao()));
            }

            entrada.setNrLoteInterno(0l);
            entrada.setCdTipoLayout(0);
            if (isChkLoteInterno()) {
                entrada.setNrLoteInterno(getLoteInterno());
                entrada.setCdTipoLayout(getFiltroTipoLayoutArquivo());
            }

            setListaGridConPagamentosConsolidado(getConsultarPagamentosConsolidadoServiceImpl()
                .consultarPagamentosConsolidados(entrada));

            listaRadiosConPagamentosConsolidado = new ArrayList<SelectItem>();

            for (int i = 0; i < getListaGridConPagamentosConsolidado().size(); i++) {
                listaRadiosConPagamentosConsolidado.add(new SelectItem(i, ""));
            }

            setItemSelecionadoListaConPagamentosConsolidado(null);
            setDisableArgumentosConsulta(true);
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridConPagamentosConsolidado(null);
            setItemSelecionadoListaConPagamentosConsolidado(null);
            setDisableArgumentosConsulta(false);
        }
    }

    /**
     * Carrega lista detalhar pagamentos consolidado.
     */
    public void carregaListaDetalharPagamentosConsolidado() {

        entradaDetalhe = new DetalharPagamentosConsolidadosEntradaDTO();
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        entradaDetalhe.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDetalhe.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDetalhe.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDetalhe.setNrCnpjCpf(selecaoConsulta.getNrCnpjCpf());
        entradaDetalhe.setNrFilialcnpjCpf(selecaoConsulta.getNrFilialCnpjCpf());
        entradaDetalhe.setNrControleCnpjCpf(selecaoConsulta.getNrDigitoCnpjCpf());
        entradaDetalhe.setNrArquivoRemessaPagamento(selecaoConsulta
            .getNrArquivoRemessaPagamento());
        entradaDetalhe.setDtCreditoPagamento(selecaoConsulta.getDtCreditoPagamento());
        entradaDetalhe.setCdBanco(selecaoConsulta.getCdBanco());
        entradaDetalhe.setCdAgencia(selecaoConsulta.getCdAgencia());
        entradaDetalhe.setCdConta(selecaoConsulta.getCdConta());
        entradaDetalhe.setCdDigitoConta(selecaoConsulta.getCdDigitoConta());
        entradaDetalhe.setCdProdutoServicoOperacao(selecaoConsulta
            .getCdProdutoServicoOperacao());
        entradaDetalhe.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDetalhe.setCdSituacaoOperacaoPagamento(selecaoConsulta
            .getCdSituacaoOperacaoPagamento());
        entradaDetalhe.setCdAgendadoPagaoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDetalhe.setCdPessoaContratoDebito(selecaoConsulta
            .getCdPessoaContratoDebito());
        entradaDetalhe.setCdTipoContratoDebito(selecaoConsulta
            .getCdTipoContratoDebito());
        entradaDetalhe.setNrSequenciaContratoDebito(selecaoConsulta
            .getNrSequenciaContratoDebito());
        entradaDetalhe.setCdIndicadorAutorizacaoPagador(PgitUtil.verificaStringToIntegerNula(getRadioIndicadorAutorizacao()));
        entradaDetalhe.setNrLoteInternoPagamento(getLoteInterno());

        if (isChkRastreamentoTitulo()) {
            entradaDetalhe.setCdTituloPgtoRastreado(Integer
                .parseInt(getRadioRastreados()));
        } else {
            entradaDetalhe.setCdTituloPgtoRastreado(0);
        }

        detalharPagamentosConsolidadosSaidaDTO = getConsultarPagamentosConsolidadoServiceImpl()
        .detalharPagamentosConsolidados(entradaDetalhe);

        setListaGridDetPagamentosConsolidado(detalharPagamentosConsolidadosSaidaDTO
            .getListaOcorrecnias());

        // in�cio do preenchimento do cabecalho
        setNrCnpjCpf(CpfCnpjUtils.formatCpfCnpjCompleto(selecaoConsulta
            .getNrCnpjCpf(), selecaoConsulta.getNrFilialCnpjCpf(),
            selecaoConsulta.getNrDigitoCnpjCpf()));
        setDsEmpresa(selecaoConsulta.getDsRazaoSocial());
        setNroContrato(Long.toString(selecaoConsulta.getNrContratoOrigem()));

        setDsRazaoSocial(detalharPagamentosConsolidadosSaidaDTO
            .getNomeCliente());
        setDsContrato(detalharPagamentosConsolidadosSaidaDTO.getDsContrato());
        setCdSituacaoContrato(detalharPagamentosConsolidadosSaidaDTO
            .getDsSituacaoContrato());

        setDsBancoDebito(PgitUtil.concatenarCampos(
            selecaoConsulta.getCdBanco(),
            detalharPagamentosConsolidadosSaidaDTO.getDsBancoDebito()));
        setDsAgenciaDebito(PgitUtil.formatAgencia(selecaoConsulta
            .getCdAgencia(), selecaoConsulta.getCdDigitoAgencia(),
            detalharPagamentosConsolidadosSaidaDTO.getDsAgenciaDebito(),
            false));
        setContaDebito(PgitUtil.concatenarCampos(selecaoConsulta.getCdConta(),
            selecaoConsulta.getCdDigitoConta()));
        setTipoContaDebito(detalharPagamentosConsolidadosSaidaDTO
            .getDsTipoContaDebito());
        // fim do preenchimento do cabecalho

        setDscRazaoSocial(detalharPagamentosConsolidadosSaidaDTO
            .getNomeCliente());
        listaRadiosDetPagamentosConsolidado = new ArrayList<SelectItem>();

        for (int i = 0; i < getListaGridDetPagamentosConsolidado().size(); i++) {
            listaRadiosDetPagamentosConsolidado.add(new SelectItem(i, ""));
        }

        setItemSelecionadoListaDetPagamentosConsolidado(null);

    }

    /**
     * Pesquisar historico.
     * 
     * @param evt
     *            the evt
     */
    public void pesquisarHistorico(ActionEvent evt) {
        historicoConsultaSaldo();
    }

    /**
     * Historico consulta saldo.
     * 
     * @return the string
     */
    public String historicoConsultaSaldo() {
        listaHistoricoConsulta = new ArrayList<ConsultarHistConsultaSaldoSaidaDTO>();
        try {
            ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
            .get(getItemSelecionadoListaConPagamentosConsolidado());

            OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
            .get(getItemSelecionadoListaDetPagamentosConsolidado());

            ConsultarHistConsultaSaldoEntradaDTO entradaDTO = new ConsultarHistConsultaSaldoEntradaDTO();

            entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
                .getCdPessoaJuridicaContrato());
            entradaDTO.setCdTipoContratoNegocio(registroSelecionado
                .getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
                .getNrContratoOrigem());
            entradaDTO.setCdTipoCanal(selecaoFiltroDetalhe.getCdTipoCanal());
            entradaDTO.setCdControlePagamento(selecaoFiltroDetalhe
                .getNrPagamento());
            entradaDTO.setCdModalidade(selecaoFiltroDetalhe.getCdTipoTela());
            entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
            entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
            entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
            entradaDTO.setDtPagamento(registroSelecionado.getDtCreditoPagamento());

            setListaHistoricoConsulta(getConsultarPagamentosIndividualServiceImpl()
                .consultarHistConsultaSaldo(entradaDTO));

            carregaCabecalho();

            // Comentado conforme solicitado - 10/03/2011 - Sempre Carregar
            // Conta de D�bito
            // if(getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")
            // &&
            // getListaHistoricoConsulta().size() > 0){
            setDsBancoDebito(PgitUtil.formatBanco(registroSelecionado
                .getCdBanco(), getListaHistoricoConsulta().get(0)
                .getDsBancoDebito(), false));
            setDsAgenciaDebito(PgitUtil.formatAgencia(registroSelecionado
                .getCdAgencia(), registroSelecionado.getCdDigitoAgencia(),
                getListaHistoricoConsulta().get(0).getDsAgenciaDebito(),
                false));
            setContaDebito(PgitUtil.formatConta(registroSelecionado
                .getCdConta(), registroSelecionado.getCdDigitoConta(),
                false));
            setTipoContaDebito(getListaHistoricoConsulta().get(0)
                .getDsTipoConta());
            // }

            // Se for uma Consulta Por conta D�bito, carregar Informa��es do
            // contrato conforme segue:
            if (getFiltroAgendamentoEfetivacaoEstornoBean()
                            .getTipoFiltroSelecionado().equals("2")) {
                setDsEmpresa(getDetalharPagamentosConsolidadosSaidaDTO()
                    .getNomeCliente());
                setNroContrato(Long.toString(registroSelecionado
                    .getNrContratoOrigem()));
                setDsContrato(getListaHistoricoConsulta().get(0)
                    .getDsContrato());
                setCdSituacaoContrato(getListaHistoricoConsulta().get(0)
                    .getDsSituacaoContrato());
            }

            listaPesquisaHistoricoControle = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaHistoricoConsulta().size(); i++) {
                listaPesquisaHistoricoControle.add(new SelectItem(i, ""));
            }
            setItemSelecionadoListaHistorico(null);

            limparFavorecidoManutencao();

            setDsTipoServico(registroSelecionado.getDsResumoProdutoServico());
            setDsIndicadorModalidade(registroSelecionado
                .getDsOperacaoProdutoServico());
            setValorPagamentoFormatado(selecaoFiltroDetalhe
                .getVlPagamentoFormatado());
            setNumeroPagamento(selecaoFiltroDetalhe.getNrPagamento());
            setSituacaoPagamento(getListaHistoricoConsulta().get(0)
                .getDsSituacaoPagamentoCliente());
            setMotivoPagamento(getListaHistoricoConsulta().get(0)
                .getDsMotivoPagamentoCliente());

            // Favorecido
            setBancoFavorecido(getListaHistoricoConsulta().get(0)
                .getBancoFavorecidoFormatado());
            setAgenciaFavorecido(getListaHistoricoConsulta().get(0)
                .getAgenciaFavorecidoFormatada());
            setContaFavorecido(getListaHistoricoConsulta().get(0)
                .getContaFavorecidoFormatada());
            setTipoContaFavorecido(getListaHistoricoConsulta().get(0)
                .getCdTipoContaCreditoFavorecido());
            setDsFavorecido(selecaoFiltroDetalhe.getDsBeneficio());

            setTipoFavorecido(null);
            if (!PgitUtil.verificaStringNula(
                getListaHistoricoConsulta().get(0)
                .getCdTipoInscricaoFavorecido()).equals("")) {

                setTipoFavorecido(getListaHistoricoConsulta().get(0)
                    .getCdTipoInscricaoFavorecido());
            }

            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(Integer
                .parseInt(getTipoFavorecido()), getListaHistoricoConsulta()
                .get(0).getInscricaoFavorecido().toString()));

            if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
            } else {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                    .parseInt(getTipoFavorecido())));
            }

            if (selecaoFiltroDetalhe.getCdFavorecido() == null
                            || selecaoFiltroDetalhe.getCdFavorecido().equals("")) {

                setCdFavorecido("");
            } else {
                setCdFavorecido(selecaoFiltroDetalhe.getCdFavorecido()
                    .toString());

            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaHistoricoConsulta(null);
            setItemSelecionadoListaHistorico(null);
            return "";
        }

        return "HISTORICO_CONSULTA";
    }

    /**
     * Carrega cabecalho.
     */
    private void carregaCabecalho() {
        limparVariaveis();
        if (getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getTipoFiltroSelecionado().equals("0")) { // Filtrar por
            // cliente
            setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getEmpresaGestoraDescCliente());
            setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getNumeroDescCliente());
            setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getDescricaoContratoDescCliente());
            setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getSituacaoDescCliente());
        } else {
            if (getFiltroAgendamentoEfetivacaoEstornoBean()
                            .getTipoFiltroSelecionado().equals("1")) { // Filtrar por
                // contrato
                setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getEmpresaGestoraDescContrato());
                setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getNumeroDescContrato());
                setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getDescricaoContratoDescContrato());
                setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getSituacaoDescContrato());
            }
        }

        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        setNrCnpjCpf(CpfCnpjUtils.formatCpfCnpjCompleto(registroSelecionado
            .getNrCnpjCpf(), registroSelecionado.getNrFilialCnpjCpf(),
            registroSelecionado.getNrDigitoCnpjCpf()));
        setDsRazaoSocial(getDscRazaoSocial());

    }

    /**
     * Limpar variaveis.
     */
    private void limparVariaveis() {
        setNrCnpjCpf(null);
        setDsRazaoSocial("");
        setDsEmpresa("");
        setNroContrato("");
        setDsContrato("");
        setCdSituacaoContrato("");
        setDsBancoDebito("");
        setDsAgenciaDebito("");
        setContaDebito(null);
        setTipoContaDebito("");
    }

    /**
     * Carrega cabecalho imprimir.
     */
    private void carregaCabecalhoImprimir() {
        limparVariaveisFiltroDetalhes();
        if (getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getTipoFiltroSelecionado().equals("0")) { // Filtrar por
            // cliente
            setNrCnpjCpf(String
                .valueOf(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getNrCpfCnpjCliente()));
            setDsRazaoSocial(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getDescricaoRazaoSocialCliente());
            setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getEmpresaGestoraDescCliente());
            setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getNumeroDescCliente());
            setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getDescricaoContratoDescCliente());
            setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                .getSituacaoDescCliente());
        } else {
            if (getFiltroAgendamentoEfetivacaoEstornoBean()
                            .getTipoFiltroSelecionado().equals("1")) { // Filtrar por
                // contrato
                setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getEmpresaGestoraDescContrato());
                setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getNumeroDescContrato());
                setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getDescricaoContratoDescContrato());
                setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
                    .getSituacaoDescContrato());
            } else {
                if (getFiltroAgendamentoEfetivacaoEstornoBean()
                                .getTipoFiltroSelecionado().equals("2")) { // Filtrar
                    // por

                    for (int i = 0; i < getListaGridConPagamentosConsolidado()
                    .size(); i++) {
                        setDigAgenciaDebito(getListaGridConPagamentosConsolidado()
                            .get(i).getCdDigitoAgencia().toString());
                        setDigContaDebito(getListaGridConPagamentosConsolidado()
                            .get(i).getCdDigitoConta());
                        break;
                    }
                    getFiltroAgendamentoEfetivacaoEstornoBean()
                    .carregaDescricaoConta();
                    setDsBancoDebito(PgitUtil.formatBanco(
                        getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getBancoContaDebitoFiltro(),
                        getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getDescricaoBancoContaDebito(), false));
                    setDsAgenciaDebito(PgitUtil.formatAgencia(
                        getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getAgenciaContaDebitoBancariaFiltro(),
                        getDigAgenciaDebito(),
                        getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getDescricaoAgenciaContaDebitoBancaria(),
                        false));
                    setContaDebito(PgitUtil.formatConta(
                        getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getContaBancariaContaDebitoFiltro(),
                        getDigContaDebito(), false));
                    setTipoContaDebito(getFiltroAgendamentoEfetivacaoEstornoBean()
                        .getDescricaoTipoContaDebito());
                }
            }
        }

    }

    // Preenche dados detalhe de pagamentos
    /**
     * Preenche dados filtro detalhar pagamentos consolidados.
     */
    public void preencheDadosFiltroDetalharPagamentosConsolidados() {
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = listaGridConPagamentosConsolidado
        .get(itemSelecionadoListaConPagamentosConsolidado);

        setDsTipoServico(selecaoConsulta.getDsResumoProdutoServico());
        setDsIndicadorModalidade(selecaoConsulta.getDsOperacaoProdutoServico());
        setDsQtdePagamentos(selecaoConsulta.getQtPagamento().toString());
        setDsVlrTotalPagamentos(selecaoConsulta.getVlEfetivoPagamentoCliente());
        setDtPagamentoFiltro(selecaoConsulta.getDtCreditoPagamento());
        setNrRemessaFiltro(selecaoConsulta.getNrArquivoRemessaPagamento());
        setDsSituacaoPagamentoFiltro(selecaoConsulta
            .getDsSituacaoOperacaoPagamento());

    }

    /**
     * Preenche dados pagto codigo barras.
     */
    public void preencheDadosPagtoCodigoBarras() {

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharPagtoCodigoBarrasEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoCodigoBarras(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(String.valueOf(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(String.valueOf(saidaDTO
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(saidaDTO
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(String.valueOf(saidaDTO.getCdListaDebito()));
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(String.valueOf(saidaDTO.getNrDocumento()));
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(String.valueOf(saidaDTO.getCdDddTelefone()));
        setTelefoneContribuinte(saidaDTO.getNrTelefone());
        setInscricaoEstadualContribuinte(String.valueOf(saidaDTO
            .getCdInscricaoEstadual()));
        setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setAgenteArrecadador(String.valueOf(saidaDTO.getCdAgenteArrecadador()));
        setCodigoTributo(String.valueOf(saidaDTO.getCdTributoArrecadado()));
        setNumeroReferencia(String.valueOf(saidaDTO.getNrReferenciaTributo()));
        setNumeroCotaParcela(String.valueOf(saidaDTO.getNrCotaParcelaTributo()));
        setPercentualReceita(saidaDTO.getVlReceitaTributo());
        setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
        setAnoExercicio(String.valueOf(saidaDTO.getCdAnoExercicioTributo()));
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setDataCompetencia(saidaDTO.getDtCompensacao());
        setCodigoCnae(String.valueOf(saidaDTO.getCdCnae()));
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(String.valueOf(saidaDTO
            .getNrParcelamentoTributo()));
        setCodigoOrgaoFavorecido(String
            .valueOf(saidaDTO.getCdOrgaoFavorecido()));
        setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsObservacao());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    /**
     * Preenche dados pagto credito conta.
     */
    public void preencheDadosPagtoCreditoConta() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoCreditoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoCreditoConta(entradaDTO);

        setFlagExibeCampos(false);
        if("90200024".equals(getTipoServicoFiltro().toString())){
            setFlagExibeCampos(true);
        }

        // Cabe�alho
        setNrCnpjCpf("");
        if (saidaDTO.getCdCpfCnpjCliente() != null
                        && saidaDTO.getCdCpfCnpjCliente().compareTo(0L) != 0) {
            setNrCnpjCpf(PgitUtil.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente()
                .toString()));
        }

        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
        setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
            saidaDTO.getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(
            saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
            saidaDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
            saidaDTO.getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        limparCamposFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidade() == 1
                        || saidaDTO.getCdIndicadorModalidade() == 3) {
            // Favorecido
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
                .getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido()
                .toString());

            setExibeDcom(false);
            if (saidaDTO.getCdIndicadorModalidade().compareTo(1) == 0) {
                setExibeDcom(true);
            }

            if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
            } else {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                    .parseInt(getTipoFavorecido())));
            }

            setDsFavorecido(saidaDTO.getDsNomeFavorecido());

            setCdFavorecido(null);
            if (saidaDTO.getCdFavorecido() != null
                            && !saidaDTO.getCdFavorecido().equals("")) {
                setCdFavorecido(saidaDTO.getCdFavorecido());
            }

            setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO
                .getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(),
                false));
            setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
                .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
                saidaDTO.getDsAgenciaFavorecido(), false));
            setDsContaFavorecido(PgitUtil.formatConta(saidaDTO
                .getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
                false));
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
        } else {
            // Benefici�rio
            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            setExibeDcom(false);

            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setCdSituacaoTranferenciaAutomatica(saidaDTO
            .getCdSituacaoTranferenciaAutomatica());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setVlrAgendamento(saidaDTO.getVlrAgendamento());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
        setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
        setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
        setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        // Favorecidos
        setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
        setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - "
            + saidaDTO.getDsBancoOriginal());
        setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
        setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
        setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-"
            + saidaDTO.getCdDigitoAgenciaOriginal() + " - "
            + saidaDTO.getDsAgenciaOriginal());
        setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
        setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-"
            + saidaDTO.getCdDigitoContaOriginal());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());
        setDsEstornoPagamento(saidaDTO.getDsEstornoPagamento());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());


        SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
        SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        setVlEfetivacaoDebitoPagamento(saidaDTO.getVlEfetivacaoDebitoPagamento());
        setVlEfetivacaoCreditoPagamento(saidaDTO.getVlEfetivacaoCreditoPagamento());
        setVlDescontoPagamento(saidaDTO.getVlDescontoPagamento());
        try {
            setHrEfetivacaoCreditoPagamento(formato2.format(formato1.parse(saidaDTO.getHrEfetivacaoCreditoPagamento())));
            setHrEnvioCreditoPagamento(formato2.format(formato1.parse(saidaDTO.getHrEnvioCreditoPagamento())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        setCdIdentificadorTransferenciaPagto(saidaDTO.getCdIdentificadorTransferenciaPagto());

        if ("".equals(saidaDTO.getDsIdentificadorTransferenciaPagto()) || 
                        "".equals(saidaDTO.getCdIdentificadorTransferenciaPagto())){
            setDsIdentificadorTransferenciaPagto(saidaDTO.getCdIdentificadorTransferenciaPagto() 
                + saidaDTO.getDsIdentificadorTransferenciaPagto());
        }else{
            setDsIdentificadorTransferenciaPagto(saidaDTO.getCdIdentificadorTransferenciaPagto() 
                +" - "+ saidaDTO.getDsIdentificadorTransferenciaPagto());
        }


        setCdMensagemLinExtrato(saidaDTO.getCdMensagemLinExtrato());
        setCdConveCtaSalarial(saidaDTO.getCdConveCtaSalarial());
        
        setTipoContaDestinoPagamento(saidaDTO.getDsTipoContaDestino());
        setCdBancoDestinoPagamento(saidaDTO.getCdBancoDestino().toString());
        setContaDigitoDestinoPagamento(saidaDTO.getContaDestinoFormatada());
        setDsISPB(saidaDTO.getCdIspbPagtoDestino() + " - " + saidaDTO.getDsBancoDestino());
        setCdContaPagamentoDet(saidaDTO.getContaPagtoDestino());
        
        if(saidaDTO.getCdIndicadorModalidade() == 3){
        	setExibeBancoCredito(true);
        	if(saidaDTO.getContaPagtoDestino().equals("0")){
        		setCdBancoDestinoPagamento(null);
        		setContaDigitoDestinoPagamento(null);
        		setTipoContaDestinoPagamento(null);
        		setDsISPB(null);
        		setCdContaPagamentoDet(null);
        	}else {
        		setCdBancoDestino(null);
        		setAgenciaDigitoDestino(null);
        		setContaDigitoDestino(null);
        		setTipoContaDestino(null);
        	}
        	
        }else{
        	setExibeBancoCredito(false);
        }
    }

    /**
     * Limpar campos favorecido beneficiario.
     */
    private void limparCamposFavorecidoBeneficiario() {
        // Favorecido
        setDsFavorecido("");
        setCdFavorecido("");
        setDsBancoFavorecido("");
        setDsAgenciaFavorecido("");
        setDsContaFavorecido("");
        setDsTipoContaFavorecido("");
        setNumeroInscricaoFavorecido("");
        setTipoFavorecido("");
        setDescTipoFavorecido("");

        // Benefici�rio
        setNumeroInscricaoBeneficiario("");
        setTipoBeneficiario("");
        setNomeBeneficiario("");
        setDtNascimentoBeneficiario("");
        setDsTipoBeneficiario("");
    }

    /**
     * Preenche dados pagto darf.
     */
    public void preencheDadosPagtoDARF() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoDARFEntradaDTO entradaDTO = new DetalharPagtoDARFEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDARF(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(String.valueOf(saidaDTO
            .getCdTipoInscricaoFavorecido()));

        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(String.valueOf(saidaDTO
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(saidaDTO
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(String.valueOf(saidaDTO.getCdListaDebito()));
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(String.valueOf(saidaDTO.getNrDocumento()));
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(String.valueOf(saidaDTO.getCdDddTelefone()));
        setTelefoneContribuinte(saidaDTO.getCdTelefone());
        setInscricaoEstadualContribuinte(String.valueOf(saidaDTO
            .getCdInscricaoEstadual()));
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setAgenteArrecadador(String.valueOf(saidaDTO.getCdAgenteArrecad()));
        setNumeroReferencia(String.valueOf(saidaDTO.getNrReferenciaTributo()));
        setNumeroCotaParcela(String.valueOf(saidaDTO.getNrCotaParcela()));
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
        setAnoExercicio(String.valueOf(saidaDTO.getCdDanoExercicio()));
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
        setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
        setVlMulta(saidaDTO.getVlMultaCredito());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    /**
     * Preenche dados pagto debito conta.
     */
    public void preencheDadosPagtoDebitoConta() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoDebitoContaEntradaDTO entradaDTO = new DetalharPagtoDebitoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDebitoConta(entradaDTO);

        // Cabe�alho - Cliente
        setNrCnpjCpf("");
        if (saidaDTO.getCdCpfCnpjCliente() != null
                        && saidaDTO.getCdCpfCnpjCliente().compareTo(0L) != 0) {
            setNrCnpjCpf(PgitUtil.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente()
                .toString()));
        }
        setDsRazaoSocial(saidaDTO.getNomeCliente());

        // Sempre Carregar Conta de D�bito
        setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
            saidaDTO.getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(
            saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
            saidaDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
            saidaDTO.getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // Favorecido
        setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido(), saidaDTO
            .getNmInscriFavorecido()));

        // setTipoFavorecido(String.valueOf(selecaoFiltroDetalhe.getCdCnpjCpfFavorecido()));
        setTipoFavorecido(String.valueOf(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(),
            saidaDTO.getCdDigContaCredito(), false));
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(String.valueOf(saidaDTO
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(saidaDTO
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(String.valueOf(saidaDTO.getCdListaDebito()));
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlrAgendamento());
        setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(String.valueOf(saidaDTO.getNrDocumento()));
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlInss(saidaDTO.getVlInss());
        setVlIof(saidaDTO.getVlIof());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto debito veiculos.
     */
    public void preencheDadosPagtoDebitoVeiculos() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharPagtoDebitoVeiculosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDebitoVeiculos(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(String.valueOf(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(String.valueOf(saidaDTO
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(saidaDTO
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(String.valueOf(saidaDTO.getNrDocumento()));
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setNumeroCotaParcela(String.valueOf(saidaDTO.getNrCotaParcelaTrib()));
        setAnoExercicio(String.valueOf(saidaDTO.getDtAnoExercTributo()));
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
        setMunicipioTributo(String.valueOf(saidaDTO.getCdMunicArrecadTrib()));
        setUfTributo(saidaDTO.getCdUfTributo());
        setNumeroNsu(String.valueOf(saidaDTO.getNrNsuTributo()));
        setOpcaoTributo(String.valueOf(saidaDTO.getCdOpcaoTributo()));
        setOpcaoRetiradaTributo(String.valueOf(saidaDTO.getCdOpcaoRetirada()));
        setVlPrincipal(saidaDTO.getVlPrincipalTributo());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setObservacaoTributo(saidaDTO.getDsObservacaoTributo());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto dep identificado.
     */
    public void preencheDadosPagtoDepIdentificado() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharPagtoDepIdentificadoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDepIdentificado(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO
            .getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(String.valueOf(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(String.valueOf(saidaDTO
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(saidaDTO
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(String.valueOf(saidaDTO.getNrDocumento()));
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setClienteDepositoIdentificado(String.valueOf(saidaDTO
            .getCdClienteDepositoId()));
        setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
        setProdutoDepositoIdentificado(String.valueOf(saidaDTO
            .getCdProdutoDepositoId()));
        setSequenciaProdutoDepositoIdentificado(String.valueOf(saidaDTO
            .getCdSequenciaProdutoDepositoId()));
        setBancoCartaoDepositoIdentificado(String.valueOf(saidaDTO
            .getCdBancoCartaoDepositanteId()));
        setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
        setBimCartaoDepositoIdentificado(String.valueOf(saidaDTO
            .getCdBinCartaoDepositanteId()));
        setViaCartaoDepositoIdentificado(String.valueOf(saidaDTO
            .getCdViaCartaoDepositanteId()));
        setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
        setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto doc.
     */
    public void preencheDadosPagtoDOC() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoDOCEntradaDTO entradaDTO = new DetalharPagtoDOCEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoDOC(entradaDTO);

        limparCamposFavorecidoBeneficiario();

        // Cabe�alho
        setNrCnpjCpf("");
        if (saidaDTO.getCdCpfCnpjCliente() != null
                        && saidaDTO.getCdCpfCnpjCliente().compareTo(0L) != 0) {
            setNrCnpjCpf(PgitUtil.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente()
                .toString()));
        }
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidadePgit() == 1
                        || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            // Favorecido
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
                .getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(String.valueOf(saidaDTO
                .getCdTipoInscricaoFavorecido()));

            setExibeDcom(false);
            if (saidaDTO.getCdIndicadorModalidadePgit().compareTo(1) == 0) {
                setExibeDcom(true);
            }

            if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
            } else {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                    .parseInt(getTipoFavorecido())));
            }

            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(saidaDTO.getCdFavorecido());
            setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
        } else {
            // Benefici�rio
            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            setExibeDcom(false);

            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || !PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                .equals("")
                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                .equals("");
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(String.valueOf(saidaDTO
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(saidaDTO
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(String.valueOf(saidaDTO.getCdListaDebito()));
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(String.valueOf(saidaDTO.getNrDocumento()));
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCamaraCentralizadora(String.valueOf(saidaDTO.getCdCamaraCentral()));
        setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
        setIdentificacaoTransferenciaDoc(saidaDTO
            .getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlInss(saidaDTO.getVlInss());
        setVlIof(saidaDTO.getVlIof());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    /**
     * Preenche dados pagto gare.
     */
    public void preencheDadosPagtoGARE() {

        OcorrenciasDetalharPagamentosConsolidados registroSelecionado = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO registroAnteriorSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoGAREEntradaDTO entradaDTO = new DetalharPagtoGAREEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroAnteriorSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroAnteriorSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroAnteriorSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(registroSelecionado
            .getDsEfetivacaoPagamento());
        entradaDTO.setCdControlePagamento(registroSelecionado.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroAnteriorSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroAnteriorSelecionado
            .getCdAgencia());
        entradaDTO.setCdContaDebito(registroAnteriorSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaCredito(registroSelecionado.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(registroAnteriorSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoGARE(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(String.valueOf(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(String.valueOf(saidaDTO
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(saidaDTO
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(String.valueOf(saidaDTO.getCdListaDebito()));
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(String.valueOf(saidaDTO.getNrDocumento()));
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(String.valueOf(saidaDTO.getCdDddContribuinte()));
        setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
        setInscricaoEstadualContribuinte(String.valueOf(saidaDTO
            .getCdInscricaoEstadualContribuinte()));
        setCodigoReceita(saidaDTO.getCdReceita());
        setNumeroReferencia(String.valueOf(saidaDTO.getNrReferencia()));
        setNumeroCotaParcela(String.valueOf(saidaDTO.getNrCota()));
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
        setDataReferencia(saidaDTO.getDtReferencia());
        setCodigoCnae(String.valueOf(saidaDTO.getCdCnae()));
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(String.valueOf(saidaDTO.getNrParcela()));
        setCodigoOrgaoFavorecido(String.valueOf(saidaDTO.getCdOrFavorecido()));
        setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsTributo());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    /**
     * Preenche dados pagto gps.
     */
    public void preencheDadosPagtoGPS() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoGPSEntradaDTO entradaDTO = new DetalharPagtoGPSEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoGPS(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(String.valueOf(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(String.valueOf(saidaDTO
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(saidaDTO
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(String.valueOf(saidaDTO.getCdListaDebito()));
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(String.valueOf(saidaDTO.getNrDocumento()));
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(String.valueOf(saidaDTO.getCdDddContribuinte()));
        setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
        setCodigoInss(String.valueOf(saidaDTO.getCdInss()));
        setDataCompetencia(String.valueOf(saidaDTO.getDsMesAnoCompt()));
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }


    /**
     * Preenche dados pagto ordem credito.
     */
    public void preencheDadosPagtoOrdemCredito() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharPagtoOrdemCreditoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoOrdemCredito(entradaDTO);

        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO
            .getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(String.valueOf(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(String.valueOf(saidaDTO
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(saidaDTO
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(String.valueOf(saidaDTO.getNrDocumento()));
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCodigoPagador(saidaDTO.getCdPagador());
        setCodigoOrdemCredito(String.valueOf(saidaDTO
            .getCdOrdemCreditoPagamento()));
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Preenche dados pagto ordem pagto.
     */
    public void preencheDadosPagtoOrdemPagto() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharPagtoOrdemPagtoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        detalheOrdemPagamento = getConsultarPagamentosIndividualServiceImpl().detalharPagtoOrdemPagto(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(detalheOrdemPagamento.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(detalheOrdemPagamento.getNomeCliente());
        setDsEmpresa(detalheOrdemPagamento.getDsPessoaJuridicaContrato());
        setNroContrato(detalheOrdemPagamento.getNrSequenciaContratoNegocio());
        setDsContrato(detalheOrdemPagamento.getDsContrato());
        setCdSituacaoContrato(detalheOrdemPagamento.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(detalheOrdemPagamento.getBancoDebitoFormatado());
        setDsAgenciaDebito(detalheOrdemPagamento.getAgenciaDebitoFormatada());
        setContaDebito(detalheOrdemPagamento.getContaDebitoFormatada());
        setTipoContaDebito(detalheOrdemPagamento.getDsTipoContaDebito());

        limparCamposFavorecidoBeneficiario();

        exibeBeneficiario = false;
        if (detalheOrdemPagamento.getCdIndicadorModalidadePgit() == 1
                        || detalheOrdemPagamento.getCdIndicadorModalidadePgit() == 3) {
            // Favorecido
            setNumeroInscricaoFavorecido(detalheOrdemPagamento
                .getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(String.valueOf(detalheOrdemPagamento
                .getCdTipoInscricaoFavorecido()));
            setExibeDcom(true);
            if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
            } else {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                    .parseInt(getTipoFavorecido())));
            }

            setDsFavorecido(detalheOrdemPagamento.getDsNomeFavorecido());
            setCdFavorecido(detalheOrdemPagamento.getCdFavorecido());
            setDsBancoFavorecido(detalheOrdemPagamento.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(detalheOrdemPagamento.getAgenciaCreditoFormatada());
            setDsContaFavorecido(detalheOrdemPagamento.getContaCreditoFormatada());
            setDsTipoContaFavorecido(detalheOrdemPagamento.getDsTipoContaFavorecido());
        } else {
            // Benefici�rio
            setNumeroInscricaoBeneficiario(detalheOrdemPagamento
                .getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(detalheOrdemPagamento.getCdTipoInscriBeneficio());
            setNomeBeneficiario(detalheOrdemPagamento.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(detalheOrdemPagamento.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(detalheOrdemPagamento.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(String.valueOf(detalheOrdemPagamento
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(detalheOrdemPagamento
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(detalheOrdemPagamento.getCdControlePagamento());
        setCdListaDebito(String.valueOf(detalheOrdemPagamento.getCdListaDebito()));
        setDtAgendamento(detalheOrdemPagamento.getDtAgendamento());
        setDtPagamento(detalheOrdemPagamento.getDtPagamento());
        setDtVencimento(detalheOrdemPagamento.getDtVencimento());
        setCdIndicadorEconomicoMoeda(detalheOrdemPagamento.getDsMoeda());
        setQtMoeda(detalheOrdemPagamento.getQtMoeda());
        setVlrAgendamento(detalheOrdemPagamento.getVlAgendado());
        setVlrEfetivacao(detalheOrdemPagamento.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(detalheOrdemPagamento.getCdSituacaoPagamento());
        setDsMotivoSituacao(detalheOrdemPagamento.getDsMotivoSituacao());
        setDsPagamento(detalheOrdemPagamento.getDsPagamento());
        setNrDocumento(String.valueOf(detalheOrdemPagamento.getNrDocumento()));
        setTipoDocumento(detalheOrdemPagamento.getDsTipoDocumento());
        setCdSerieDocumento(detalheOrdemPagamento.getCdSerieDocumento());
        setVlDocumento(detalheOrdemPagamento.getVlDocumento());
        setDtEmissaoDocumento(detalheOrdemPagamento.getDtEmissaoDocumento());
        setDsUsoEmpresa(detalheOrdemPagamento.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(detalheOrdemPagamento.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(detalheOrdemPagamento.getDsMensagemSegundaLinha());
        setCdBancoCredito(detalheOrdemPagamento.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(detalheOrdemPagamento.getAgenciaCreditoFormatada());
        setContaDigitoCredito(detalheOrdemPagamento.getContaCreditoFormatada());
        setTipoContaCredito(detalheOrdemPagamento.getDsTipoContaFavorecido());
        setNumeroOp(String.valueOf(detalheOrdemPagamento.getNrOperacao()));
        setDataExpiracaoCredito(detalheOrdemPagamento.getDtExpedicaoCredito());
        setInstrucaoPagamento(detalheOrdemPagamento.getCdInsPagamento());
        setNumeroCheque(String.valueOf(detalheOrdemPagamento.getNrCheque()));
        setSerieCheque(detalheOrdemPagamento.getNrSerieCheque());
        setIndicadorAssociadoUnicaAgencia(detalheOrdemPagamento.getDsUnidadeAgencia());
        setIdentificadorTipoRetirada(detalheOrdemPagamento.getDsIdentificacaoTipoRetirada());
        setIdentificadorRetirada(detalheOrdemPagamento.getDsIdentificacaoRetirada());
        setDataRetirada(detalheOrdemPagamento.getDtRetirada());
        setVlImpostoMovFinanceira(detalheOrdemPagamento.getVlImpostoMovimentacaoFinanceira());
        setVlDesconto(detalheOrdemPagamento.getVlDescontoCredito());
        setVlAbatimento(detalheOrdemPagamento.getVlAbatidoCredito());
        setVlMulta(detalheOrdemPagamento.getVlMultaCredito());
        setVlMora(detalheOrdemPagamento.getVlMoraJuros());
        setVlDeducao(detalheOrdemPagamento.getVlOutrasDeducoes());
        setVlAcrescimo(detalheOrdemPagamento.getVlOutroAcrescimo());
        setVlImpostoRenda(detalheOrdemPagamento.getVlIrCredito());
        setVlIss(detalheOrdemPagamento.getVlIssCredito());
        setVlInss(detalheOrdemPagamento.getVlInssCredito());
        setVlIof(detalheOrdemPagamento.getVlIofCredito());
        setDtDevolucaoEstorno(detalheOrdemPagamento.getDtDevolucaoEstorno());
        setDtFloatingPagamento(detalheOrdemPagamento.getDtFloatingPagamento());
        setDtEfetivFloatPgto(detalheOrdemPagamento.getDtEfetivFloatPgto());
        setVlFloatingPagamento(detalheOrdemPagamento.getVlFloatingPagamento());
        setCdOperacaoDcom(detalheOrdemPagamento.getCdOperacaoDcom());
        setDsSituacaoDcom(detalheOrdemPagamento.getDsSituacaoDcom());

        // Trilha
        setCdFuncEmissaoOp(detalheOrdemPagamento.getCdFuncEmissaoOp());
        setCdTerminalCaixaEmissaoOp(detalheOrdemPagamento.getCdTerminalCaixaEmissaoOp());
        setDtEmissaoOp(detalheOrdemPagamento.getDtEmissaoOp());
        setHrEmissaoOp(detalheOrdemPagamento.getHrEmissaoOp());
        setDataHoraInclusao(detalheOrdemPagamento.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(detalheOrdemPagamento.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(detalheOrdemPagamento.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(detalheOrdemPagamento.getComplementoInclusaoFormatado());
        setDataHoraManutencao(detalheOrdemPagamento.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(detalheOrdemPagamento.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(detalheOrdemPagamento.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(detalheOrdemPagamento.getComplementoManutencaoFormatado());

        setNumControleInternoLote(detalheOrdemPagamento.getCdLoteInterno());
        setDsIndicadorAutorizacao(detalheOrdemPagamento.getDsIndicadorAutorizacao());
        setDsTipoLayout(detalheOrdemPagamento.getDsTipoLayout());
    }

    /**
     * Preenche dados pagto ted.
     */
    public void preencheDadosPagtoTED() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoTEDEntradaDTO entradaDTO = new DetalharPagtoTEDEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        detalharPagtoTed = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoTED(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(detalharPagtoTed.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(detalharPagtoTed.getNomeCliente());
        setDsEmpresa(detalharPagtoTed.getDsPessoaJuridicaContrato());
        setNroContrato(detalharPagtoTed.getNrSequenciaContratoNegocio());
        setDsContrato(detalharPagtoTed.getDsContrato());
        setCdSituacaoContrato(detalharPagtoTed.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(detalharPagtoTed.getBancoDebitoFormatado());
        setDsAgenciaDebito(detalharPagtoTed.getAgenciaDebitoFormatada());
        setContaDebito(detalharPagtoTed.getContaDebitoFormatada());
        setTipoContaDebito(detalharPagtoTed.getDsTipoContaDebito());

        limparCamposFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (detalharPagtoTed.getCdIndicadorModalidadePgit() == 1
                        || detalharPagtoTed.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(detalharPagtoTed
                .getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(String.valueOf(detalharPagtoTed
                .getCdTipoInscricaoFavorecido()));
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(detalharPagtoTed
                .getCdTipoInscricaoFavorecido()));
            setDsFavorecido(detalharPagtoTed.getDsNomeFavorecido());
            setCdFavorecido(detalharPagtoTed.getCdFavorecido());
            setDsBancoFavorecido(detalharPagtoTed.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(detalharPagtoTed.getAgenciaCreditoFormatada());
            setDsContaFavorecido(detalharPagtoTed.getContaCreditoFormatada());
            setDsTipoContaFavorecido(detalharPagtoTed.getDsTipoContaFavorecido());

            setExibeDcom(false);
            if (detalharPagtoTed.getCdIndicadorModalidadePgit().compareTo(1) == 0) {
                setExibeDcom(true);
            }

        } else {
            setNumeroInscricaoBeneficiario(detalharPagtoTed
                .getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(detalharPagtoTed.getCdTipoInscriBeneficio());
            setNomeBeneficiario(detalharPagtoTed.getCdNomeBeneficio());
            setExibeDcom(false);
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
        }

        // Pagamento
        setDsTipoServico(detalharPagtoTed.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(detalharPagtoTed.getDsModalidadeRelacionado());
        setCdIndentificadorJudicial(detalharPagtoTed.getCdIndentificadorJudicial());
        setNrSequenciaArquivoRemessa(String.valueOf(detalharPagtoTed
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(detalharPagtoTed
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(detalharPagtoTed.getCdControlePagamento());
        setCdListaDebito(String.valueOf(detalharPagtoTed.getCdListaDebito()));
        setDtAgendamento(detalharPagtoTed.getDtAgendamento());
        setDtPagamento(detalharPagtoTed.getDtPagamento());
        setDtVencimento(detalharPagtoTed.getDtVencimento());
        setCdIndicadorEconomicoMoeda(detalharPagtoTed.getDsMoeda());
        setQtMoeda(detalharPagtoTed.getQtMoeda());
        setVlrAgendamento(detalharPagtoTed.getVlAgendado());
        setVlrEfetivacao(detalharPagtoTed.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(detalharPagtoTed.getCdSituacaoPagamento());
        setDsMotivoSituacao(detalharPagtoTed.getDsMotivoSituacao());
        setDsPagamento(detalharPagtoTed.getDsPagamento());
        setNrDocumento(String.valueOf(detalharPagtoTed.getNrDocumento()));
        setTipoDocumento(detalharPagtoTed.getDsTipoDocumento());
        setCdSerieDocumento(detalharPagtoTed.getCdSerieDocumento());
        setVlDocumento(detalharPagtoTed.getVlDocumento());
        setDtEmissaoDocumento(detalharPagtoTed.getDtEmissaoDocumento());
        setDsUsoEmpresa(detalharPagtoTed.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(detalharPagtoTed.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(detalharPagtoTed.getDsMensagemSegundaLinha());
        setCdBancoCredito(String.valueOf(detalharPagtoTed.getCdBancoCredito()));
        setDsBancoCredito(detalharPagtoTed.getDsBancoCredito());
        setCdIspbPagamento(detalharPagtoTed.getCdIspbPagamento());
        setDsNomeReclamante(detalharPagtoTed.getDsNomeReclamante());
        setNrProcessoDepositoJudicial(detalharPagtoTed.getNrProcessoDepositoJudicial());
        setNrPisPasep(NumberUtils.formatarNumeroPisPasep(detalharPagtoTed.getNrPisPasep()));
        setCdFinalidadeTedDepositoJudicial(detalharPagtoTed.isCdFinalidadeTedDepositoJudicial());
        setAgenciaDigitoCredito(detalharPagtoTed.getAgenciaCreditoFormatada());
        setContaDigitoCredito(detalharPagtoTed.getContaCreditoFormatada());
        setTipoContaCredito(detalharPagtoTed.getDsTipoContaFavorecido());
        setCamaraCentralizadora(String.valueOf(detalharPagtoTed.getCdCamaraCentral()));
        setFinalidadeTed(PgitUtil.concatenarCampos(detalharPagtoTed
            .getCdFinalidadeTed(), detalharPagtoTed.getDsFinalidadeDocTed(), " - "));
        setIdentificacaoTransferenciaTed(detalharPagtoTed
            .getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(detalharPagtoTed.getDsidentificacaoTitularPagto());
        setVlDesconto(detalharPagtoTed.getVlDesconto());
        setVlAbatimento(detalharPagtoTed.getVlAbatidoCredito());
        setVlMulta(detalharPagtoTed.getVlMultaCredito());
        setVlMora(detalharPagtoTed.getVlMoraJuros());
        setVlDeducao(detalharPagtoTed.getVlOutrasDeducoes());
        setVlAcrescimo(detalharPagtoTed.getVlOutroAcrescimo());
        setVlImpostoRenda(detalharPagtoTed.getVlIrCredito());
        setVlIss(detalharPagtoTed.getVlIssCredito());
        setVlInss(detalharPagtoTed.getVlInssCredito());
        setVlIof(detalharPagtoTed.getVlIofCredito());
        setDtDevolucaoEstorno(detalharPagtoTed.getDtDevolucaoEstorno());
        setDtFloatingPagamento(detalharPagtoTed.getDtFloatingPagamento());
        setDtEfetivFloatPgto(detalharPagtoTed.getDtEfetivFloatPgto());
        setVlFloatingPagamento(detalharPagtoTed.getVlFloatingPagamento());
        setCdOperacaoDcom(detalharPagtoTed.getCdOperacaoDcom());
        setDsSituacaoDcom(detalharPagtoTed.getDsSituacaoDcom());

        // Trilha
        setDataHoraInclusao(detalharPagtoTed.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(detalharPagtoTed.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(detalharPagtoTed.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(detalharPagtoTed.getComplementoInclusaoFormatado());
        setDataHoraManutencao(detalharPagtoTed.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(detalharPagtoTed.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(detalharPagtoTed.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(detalharPagtoTed.getComplementoManutencaoFormatado());

        setNumControleInternoLote(detalharPagtoTed.getCdLoteInterno());
        setDsIndicadorAutorizacao(detalharPagtoTed.getDsIndicadorAutorizacao());
        setDsTipoLayout(detalharPagtoTed.getDsTipoLayout());
        
    }

    /**
     * Preenche dados pagto titulo bradesco.
     */
    public void preencheDadosPagtoTituloBradesco() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharPagtoTituloBradescoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoTituloBradesco(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(String.valueOf(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // SACADOR/AVALISTA
        setNumeroInscricaoSacadorAvalista(saidaDTO.getNumeroInscricaoSacadorAvalista());
        setDsSacadorAvalista(saidaDTO.getDsSacadorAvalista());
        setDsTipoSacadorAvalista(saidaDTO.getDsTipoSacadorAvalista());    
        
        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setCarteira(saidaDTO.getCarteira());
        setNrSequenciaArquivoRemessa(String.valueOf(saidaDTO
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(saidaDTO
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(String.valueOf(saidaDTO.getCdListaDebito()));
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(String.valueOf(saidaDTO.getNrDocumento()));
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtLimitePagamento(saidaDTO.getDtLimitePagamento());

        setDsRastreado("N�o");
        if (saidaDTO.getCdRastreado() == 1) {
            setDsRastreado("Sim");
        }
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setVlBonfcao(saidaDTO.getVlBonfcao());
        setVlBonificacao(saidaDTO.getVlBonificacao());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    /**
     * Preenche dados pagto titulo outros bancos.
     */
    public void preencheDadosPagtoTituloOutrosBancos() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());
        ConsultarPagamentosConsolidadosSaidaDTO selecaoConsulta = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());
        DetalharPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharPagtoTituloOutrosBancosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
        entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
        entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO.setCdAgendadosPagoNaoPago(Integer
            .parseInt(getAgendadosPagosNaoPagos()));
        entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao("");

        DetalharPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoTituloOutrosBancos(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(String.valueOf(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido()));
        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(saidaDTO.getCdFavorecido());
        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // SACADOR/AVALISTA
        setNumeroInscricaoSacadorAvalista(saidaDTO.getNumeroInscricaoSacadorAvalista());
        setDsSacadorAvalista(saidaDTO.getDsSacadorAvalista());
        setDsTipoSacadorAvalista(saidaDTO.getDsTipoSacadorAvalista());    
        
        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setCarteira(saidaDTO.getCarteira());
        setNrSequenciaArquivoRemessa(String.valueOf(saidaDTO
            .getNrSequenciaArquivoRemessa()));
        setNrLoteArquivoRemessa(String.valueOf(saidaDTO
            .getNrLoteArquivoRemessa()));
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(String.valueOf(saidaDTO.getCdListaDebito()));
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(String.valueOf(saidaDTO.getNrDocumento()));
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
        setDsRastreado("N�o");
        if (saidaDTO.getCdRastreado() == 1) {
            setDsRastreado("Sim");
        }
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setVlBonfcao(saidaDTO.getVlBonfcao());
        setVlBonificacao(saidaDTO.getVlBonificacao());

        // Trilha
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    /**
     * Pesquisar.
     * 
     * @param e
     *            the e
     * @return the string
     */
    public String pesquisar(ActionEvent e) {
        carregaListaConsultarPagamentosConsolidado();
        return "";
    }

    /**
     * Pesquisar detalhar.
     * 
     * @param e
     *            the e
     * @return the string
     */
    public String pesquisarDetalhar(ActionEvent e) {
        preencheDadosFiltroDetalharPagamentosConsolidados();

        try {
            carregaListaDetalharPagamentosConsolidado();
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaGridDetPagamentosConsolidado(null);
            setItemSelecionadoListaDetPagamentosConsolidado(null);
            return "";
        }

        return "";
    }

    /**
     * Imprimir relatorio manutencao.
     * 
     * @return the string
     */
    public String imprimirRelatorioManutencao() {

        String caminho = "/images/Bradesco_logo.JPG";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext
        .getExternalContext().getContext();
        String logoRealPath = servletContext.getRealPath(caminho);

        // Par�metros do Relat�rio
        Map<String, Object> par = new HashMap<String, Object>();

        par.put("cpfCnpj", getNrCnpjCpf());
        par.put("nomeRazaoSocial", getDsRazaoSocial());
        par.put("empresa", getDsEmpresa());
        par.put("numero", getNroContrato());
        par.put("descContrato", getDsContrato());
        par.put("situacao", getCdSituacaoContrato());
        par.put("logoBradesco", logoRealPath);

        par.put("titulo", "Consultar Manuten��o");

        par.put("bancoDebito", getDsBancoDebito());
        par.put("agenciaDebito", getDsAgenciaDebito());
        par.put("contaDebito", getContaDebito());
        par.put("tipoContaDebito", getTipoContaDebito());
        par.put("tipoServico", getDsTipoServico());
        par.put("modalidade", getDsIndicadorModalidade());
        par.put("remessa", getNrSequenciaArquivoRemessa());
        par.put("lote", getNrLote());
        par.put("valorPagamento", getValorPagamentoFormatado());
        par.put("numeroPagamento", getNumeroPagamento());
        par.put("listaDebito", getCdListaDebito());
        par.put("dataPagamento", getDtPagamentoManutencao());
        par.put("dataVencimento", getDtVencimento());
        par.put("numeroInscricaoFavorecido", getNumeroInscricaoFavorecido());

        par.put("tipoFavorecido", 0);
        if (getTipoFavorecido() != null && !getTipoFavorecido().equals("")) {
            par.put("tipoFavorecido", Integer.parseInt(getTipoFavorecido()));
        }

        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        par.put("descTipoFavorecido", getDescTipoFavorecido());
        par.put("cdFavorecido", 0L);
        if (getCdFavorecido() != null && !getCdFavorecido().equals("")) {
            par.put("tipoFavorecido", Long.parseLong(getCdFavorecido()));
        }

        par.put("dsFavorecido", getDsFavorecido());
        par.put("bancoFavorecido", getBancoFavorecido());
        par.put("agenciaFavorecido", getAgenciaFavorecido());
        par.put("contaFavorecido", getContaFavorecido());
        par.put("tipoContaFavorecido", getTipoContaFavorecido());
        par.put("motivoPagamento", getMotivoPagamento());
        par.put("situacaoPagamento", getSituacaoPagamento());
        par.put("dsBancoDebito", getDsBancoDebito());
        par.put("dsAgenciaDebito", getDsAgenciaDebito());
        par.put("contaDebito", getContaDebito());

        try {

            // M�todo que gera o relat�rio PDF.
            PgitUtil
            .geraRelatorioPdf(
                "/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/consolidado",
                "imprimirManPagamentosConsolidados",
                getListaConsultarManutencao(), par);

        } /* Exce��o do relat�rio */
        catch (Exception e) {
            throw new BradescoViewException(e.getMessage(), e, "", "",
                BradescoViewExceptionActionType.ACTION);
        }

        return "";
    }

    /**
     * Pesquisar manutencao.
     * 
     * @param evt
     *            the evt
     */
    public void pesquisarManutencao(ActionEvent evt) {
        consultarManutencao();
    }

    /**
     * Consultar manutencao.
     * 
     * @return the string
     */
    public String consultarManutencao() {

        listaConsultarManutencao = new ArrayList<ConsultarManutencaoPagamentoSaidaDTO>();

        try {
            ConsultarManutencaoPagamentoEntradaDTO entradaDTO = new ConsultarManutencaoPagamentoEntradaDTO();
            OcorrenciasDetalharPagamentosConsolidados registroSelecionadoDetalhar = getListaGridDetPagamentosConsolidado()
            .get(getItemSelecionadoListaDetPagamentosConsolidado());

            ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
            .get(getItemSelecionadoListaConPagamentosConsolidado());

            entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
                .getCdPessoaJuridicaContrato());
            entradaDTO.setCdTipoContratoNegocio(registroSelecionado
                .getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
                .getNrContratoOrigem());
            entradaDTO.setCdTipoCanal(registroSelecionadoDetalhar
                .getCdTipoCanal());
            entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
                .getNrPagamento());
            entradaDTO.setCdModalidade(registroSelecionadoDetalhar
                .getCdTipoTela());
            entradaDTO.setCdAgendadosPagoNaoPago(Integer
                .parseInt(getAgendadosPagosNaoPagos()));
            entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
            entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
            entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
            entradaDTO.setCdDigitoAgenciaDebito(String
                .valueOf(registroSelecionado.getCdDigitoAgencia()));
            entradaDTO.setDtPagamento(registroSelecionado.getDtCreditoPagamento());

            setListaConsultarManutencao(getConsultarPagamentosIndividualServiceImpl()
                .consultarManutencaoPagamento(entradaDTO));

            // Comentado conforme solicitado - 10/03/2011 - Sempre Carregar
            // Conta de D�bito
            // if(getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")){
            setDsBancoPagto(PgitUtil.formatBanco(registroSelecionado
                .getCdBanco(), getDetalharPagamentosConsolidadosSaidaDTO()
                .getDsBancoDebito(), false));
            setDsAgenciaPagto(PgitUtil.formatAgencia(
                registroSelecionadoDetalhar.getCdAgencia(),
                registroSelecionadoDetalhar.getCdDigitoAgencia(),
                getDetalharPagamentosConsolidadosSaidaDTO()
                .getDsAgenciaDebito(), false));
            setContaPagto(PgitUtil.formatConta(registroSelecionadoDetalhar
                .getCdConta(), registroSelecionadoDetalhar
                .getCdDigitoConta(), false));
            setTipoContaDebito(getDetalharPagamentosConsolidadosSaidaDTO()
                .getDsTipoContaDebito());
            // }

            // Se for uma Consulta Por conta D�bito, carregar Informa��es do
            // contrato conforme segue:
            if (getFiltroAgendamentoEfetivacaoEstornoBean()
                            .getTipoFiltroSelecionado().equals("2")) {
                setDsEmpresa(getDetalharPagamentosConsolidadosSaidaDTO()
                    .getNomeCliente());
                setNroContrato(Long.toString(registroSelecionado
                    .getNrContratoOrigem()));
                setDsContrato(getListaConsultarManutencao().get(0)
                    .getDsContrato());
                setCdSituacaoContrato(getListaConsultarManutencao().get(0)
                    .getDsSituacaoContrato());
            }

            limparFavorecidoManutencao();

            // Pagamento
            setDsTipoServico(registroSelecionado.getDsResumoProdutoServico());
            setDsIndicadorModalidade(registroSelecionado
                .getDsOperacaoProdutoServico());
            setNrSequenciaArquivoRemessa(getListaConsultarManutencao().get(0)
                .getNrArquivoRemessaPagamento());
            setNrLote(getListaConsultarManutencao().get(0).getCdLote());
            setValorPagamentoFormatado(getListaConsultarManutencao().get(0)
                .getVlPagamentoFormatado());
            setNumeroPagamento(registroSelecionadoDetalhar.getNrPagamento());
            setCdListaDebito(getListaConsultarManutencao().get(0)
                .getCdListaDebitoPagamento());
            setCanal(getListaConsultarManutencao().get(0).getDsTipoCanal());
            setDtPagamentoManutencao(getListaConsultarManutencao().get(0)
                .getDtPagamento());
            // Na espec esta pedindo para pegar do
            // ConsultarPagamentosIndividuaisSaidaDTO mas n�o possui esse campo
            // na saida
            setDtVencimento(getListaConsultarManutencao().get(0)
                .getDtVencimentoFormatada());
            setSituacaoPagamento(getListaConsultarManutencao().get(0)
                .getDsSituacaoPagamentoCliente());
            setMotivoPagamento(getListaConsultarManutencao().get(0)
                .getDsMotivoPagamentoCliente());

            // Favorecido
            setBancoFavorecido(getListaConsultarManutencao().get(0)
                .getBancoFavorecidoFormatado());
            setAgenciaFavorecido(getListaConsultarManutencao().get(0)
                .getAgenciaFavorecidoFormatada());
            setContaFavorecido(getListaConsultarManutencao().get(0)
                .getContaFavorecidoFormatada());
            setTipoContaFavorecido(getListaConsultarManutencao().get(0)
                .getCdTipoContaCreditoFavorecido());
            setDsFavorecido(registroSelecionadoDetalhar.getDsBeneficio());

            setTipoFavorecidoManutencao(null);
            if (!PgitUtil.verificaStringNula(
                getListaConsultarManutencao().get(0)
                .getCdTipoInscricaoFavorecido()).equals("")) {
                setTipoFavorecidoManutencao(Integer
                    .parseInt(getListaConsultarManutencao().get(0)
                        .getCdTipoInscricaoFavorecido()));
            }

            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(
                getTipoFavorecidoManutencao(),
                getListaConsultarManutencao().get(0)
                .getInscricaoFavorecido().toString()));
            setDescTipoFavorecido(PgitUtil
                .validaTipoFavorecido(getTipoFavorecidoManutencao()));

            setCdFavorecido("");
            if (registroSelecionadoDetalhar.getCdFavorecido() != null) {
                setCdFavorecido(registroSelecionadoDetalhar.getCdFavorecido()
                    .toString());
            }

            listaManutencaoControle = new ArrayList<SelectItem>();
            for (int i = 0; i < getListaConsultarManutencao().size(); i++) {
                listaManutencaoControle.add(new SelectItem(i, ""));
            }
            setItemSelecionadoListaManutencao(null);
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
            setListaConsultarManutencao(null);
            setItemSelecionadoListaManutencao(null);
            return "";
        }

        return "CONSULTAR_MANUTENCAO";
    }

    /**
     * Voltar historico.
     * 
     * @return the string
     */
    public String voltarHistorico() {
        setItemSelecionadoListaHistorico(null);
        return "VOLTAR";
    }

    /**
     * Voltar consultar historico.
     * 
     * @return the string
     */
    public String voltarConsultarHistorico() {
        setItemSelecionadoListaDetPagamentosConsolidado(null);
        return "VOLTAR";
    }

    /**
     * Detalhar historico.
     * 
     * @return the string
     */
    public String detalharHistorico() {
        try {
            ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
            .get(getItemSelecionadoListaConPagamentosConsolidado());

            OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
            .get(getItemSelecionadoListaDetPagamentosConsolidado());

            ConsultarHistConsultaSaldoSaidaDTO registroSelecionadoHistorico = getListaHistoricoConsulta()
            .get(getItemSelecionadoListaHistorico());

            DetalharHistConsultaSaldoEntradaDTO entradaDTO = new DetalharHistConsultaSaldoEntradaDTO();

            /* Dados de Entrada */
            entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
                .getCdPessoaJuridicaContrato());
            entradaDTO.setCdTipoContratoNegocio(registroSelecionado
                .getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
                .getNrContratoOrigem());
            entradaDTO.setCdTipoCanal(selecaoFiltroDetalhe.getCdTipoCanal());
            entradaDTO.setCdControlePagamento(selecaoFiltroDetalhe
                .getNrPagamento());
            entradaDTO
            .setHrConsultasSaldoPagamento(registroSelecionadoHistorico
                .getHrConsultasSaldoPagamento());
            entradaDTO.setCdBanco(registroSelecionado.getCdBanco());
            entradaDTO.setCdAgencia(registroSelecionado.getCdAgencia());
            entradaDTO.setCdConta(registroSelecionado.getCdConta());
            entradaDTO.setCdModalidade(registroSelecionado
                .getCdProdutoServicoRelacionado());
            entradaDTO.setDtPagamento(registroSelecionado.getDtCreditoPagamento());

            DetalharHistConsultaSaldoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
            .detalharHistConsultaSaldo(entradaDTO);

            // Pagamento
            setDsTipoServico(registroSelecionado.getDsResumoProdutoServico()); // �
            // com
            // esse
            // campo
            // que
            // preenche!
            setDsIndicadorModalidade(registroSelecionado
                .getDsOperacaoProdutoServico()); // � com esse campo que
            // preenche!
            setNrSequenciaArquivoRemessa(saidaDTO
                .getNrArquivoRemessaPagamento());
            setNrLoteArquivoRemessa(saidaDTO.getCdPessoaLoteRemessa());
            setValorPagamento(saidaDTO.getVlPagamento());
            setNumeroPagamento(selecaoFiltroDetalhe.getNrPagamento());
            setCdListaDebito(saidaDTO.getCdListaDebitoPagamento());
            setCanal(saidaDTO.getDsCanal());
            setDtPagamento(saidaDTO.getDtCreditoPagamento());
            setDtVencimento(saidaDTO.getDtVencimento());
            setSinal(saidaDTO.getSinal());

            // Consulta de Saldo
            setDataHoraConsultaSaldo(registroSelecionadoHistorico
                .getDataHoraFormatada());
            setSaldoMomentoLancamento(saidaDTO.getVlSaldoAnteriorLancamento());
            setSaldoOperacional(saidaDTO.getVlSaldoOperacional());
            setSaldoVincSemReserva(saidaDTO.getVlSaldoSemReserva());
            setSaldoVincComReserva(saidaDTO.getVlSaldoComReserva());
            setSaldoVincJudicial(saidaDTO.getVlSaldoVinculadoJudicial());
            setSaldoVincAdm(saidaDTO.getVlSaldoVinculadoAdministrativo());
            setSaldoVincSeguranca(saidaDTO.getVlSaldoVinculadoSeguranca());
            setSaldoVincAdmLp(saidaDTO.getVlSaldoVinculadoAdministrativoLp());
            setSaldoAgregadoFundo(saidaDTO.getVlSaldoAgregadoFundo());
            setSaldoAgregadoCDB(saidaDTO.getVlSaldoAgregadoCdb());
            setSaldoAgregadoPoupanca(saidaDTO.getVlSaldoAgregadoPoupanca());
            setSaldoAgregadoCredito(saidaDTO.getVlSaldoCreditoRotativo());
            setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

            // Trilha de Auditoria
            setDataHoraInclusao(saidaDTO.getDtInclusao() + " "
                + saidaDTO.getHrInclusao());
            setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());

            setTipoCanalInclusao("");
            if (saidaDTO.getCdTipoCanalInclusao() != 0) {
                setTipoCanalInclusao(saidaDTO.getCdTipoCanalInclusao() + " - "
                    + saidaDTO.getDsTipoCanalInclusao());
            }

            setComplementoInclusao("");
            if (saidaDTO.getCdFluxoInclusao() != null
                            && !saidaDTO.getCdFluxoInclusao().equals("0")) {
                setComplementoInclusao(saidaDTO.getCdFluxoInclusao());
            }

            setDataHoraManutencao(saidaDTO.getDtManutencao() + " "
                + saidaDTO.getHrManutencao());
            setUsuarioManutencao(saidaDTO.getCdUsuarioManutencao());

            setTipoCanalManutencao("");
            if (saidaDTO.getCdTipoCanalManutencao() != 0) {
                setTipoCanalManutencao(saidaDTO.getCdTipoCanalManutencao()
                    + " - " + saidaDTO.getDsTipoCanalManutencao());
            }

            setComplementoManutencao("");
            if (saidaDTO.getCdFluxoManutencao() != null
                            && !saidaDTO.getCdFluxoManutencao().equals("0")) {
                setComplementoManutencao(saidaDTO.getCdFluxoManutencao());
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);

            return "";
        }

        return "DETALHAR";
    }

    /**
     * Detalhar manutencao.
     * 
     * @return the string
     */
    public String detalharManutencao() {
        try {
            setManutencao(true);
            OcorrenciasDetalharPagamentosConsolidados registroSelecionado = getListaGridDetPagamentosConsolidado()
            .get(getItemSelecionadoListaDetPagamentosConsolidado());

            if (registroSelecionado.getCdTipoTela() == 1) {
                preencheDadosManPagtoCreditoConta();
                return "DETALHE_MANUTENCAO_CREDITO_CONTA";
            }
            if (registroSelecionado.getCdTipoTela() == 2) {
                preencheDadosManPagtoDebitoConta();
                return "DETALHE_MANUTENCAO_DEBITO_CONTA";
            }
            if (registroSelecionado.getCdTipoTela() == 3) {
                preencheDadosManPagtoDOC();
                return "DETALHE_MANUTENCAO_DOC";
            }
            if (registroSelecionado.getCdTipoTela() == 4) {
                preencheDadosManPagtoTED();
                return "DETALHE_MANUTENCAO_TED";
            }
            if (registroSelecionado.getCdTipoTela() == 5) {
                preencheDadosManPagtoOrdemPagto();
                return "DETALHE_MANUTENCAO_ORDEM_PAGTO";
            }
            if (registroSelecionado.getCdTipoTela() == 6) {
                preencheDadosManPagtoTituloBradesco();
                return "DETALHE_MANUTENCAO_TITULOS_BRADESCO";
            }
            if (registroSelecionado.getCdTipoTela() == 7) {
                preencheDadosManPagtoTituloOutrosBancos();
                return "DETALHE_MANUTENCAO_TITULOS_OUTROS_BANCOS";
            }
            if (registroSelecionado.getCdTipoTela() == 8) {
                preencheDadosManPagtoDARF();
                return "DETALHE_MANUTENCAO_DARF";
            }
            if (registroSelecionado.getCdTipoTela() == 9) {
                preencheDadosManPagtoGARE();
                return "DETALHE_MANUTENCAO_GARE";
            }
            if (registroSelecionado.getCdTipoTela() == 10) {
                preencheDadosManPagtoGPS();
                return "DETALHE_MANUTENCAO_GPS";
            }
            if (registroSelecionado.getCdTipoTela() == 11) {
                preencheDadosManPagtoCodigoBarras();
                return "DETALHE_MANUTENCAO_CODIGO_BARRAS";
            }
            if (registroSelecionado.getCdTipoTela() == 12) {
                preencheDadosManPagtoDebitoVeiculos();
                return "DETALHE_MANUTENCAO_DEBITO_VEICULOS";
            }
            if (registroSelecionado.getCdTipoTela() == 13) {
                preencheDadosManPagtoDepIdentificado();
                return "DETALHE_MANUTENCAO_DEPOSITO_IDENTIFICADO";
            }
            if (registroSelecionado.getCdTipoTela() == 14) {
                preencheDadosManPagtoOrdemCredito();
                return "DETALHE_MANUTENCAO_ORDEM_CREDITO";
            }

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage(
                "(" + StringUtils.right(p.getCode(), 8) + ") "
                + p.getMessage(), false);
        }

        return "";
    }

    // Detalhar Manuten��o Pagamentos Consolidados - Cr�dito em Conta
    /**
     * Preenche dados man pagto credito conta.
     */
    public void preencheDadosManPagtoCreditoConta() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharPagtoCreditoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharPagtoCreditoConta(entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Cabe�alho
        setNrCnpjCpf("");
        if (saidaDTO.getCdCpfCnpjCliente() != null
                        && saidaDTO.getCdCpfCnpjCliente().compareTo(0L) != 0) {
            setNrCnpjCpf(PgitUtil.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente()
                .toString()));
        }
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
        setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
            saidaDTO.getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(
            saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
            saidaDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
            saidaDTO.getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        limparCamposFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidade() == 1
                        || saidaDTO.getCdIndicadorModalidade() == 3) {
            // Favorecido
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
                .getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido()
                .toString());

            setExibeDcom(false);
            if (saidaDTO.getCdIndicadorModalidade().compareTo(1) == 0) {
                setExibeDcom(true);
            }

            if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
            } else {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                    .parseInt(getTipoFavorecido())));
            }

            setDsFavorecido(saidaDTO.getDsNomeFavorecido());

            setCdFavorecido(null);
            if (saidaDTO.getCdFavorecido() != null
                            && !saidaDTO.getCdFavorecido().equals("")) {
                setCdFavorecido(saidaDTO.getCdFavorecido());
            }

            setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO
                .getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(),
                false));
            setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
                .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
                saidaDTO.getDsAgenciaFavorecido(), false));
            setDsContaFavorecido(PgitUtil.formatConta(saidaDTO
                .getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
                false));
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
        } else {
            // Benefici�rio
            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            setExibeDcom(false);

            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlrAgendamento());
        setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
        setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
        setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
        setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setCdSituacaoTranferenciaAutomatica(saidaDTO
            .getCdSituacaoTranferenciaAutomatica());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
        setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        // Favorecidos
        setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
        setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - "
            + saidaDTO.getDsBancoOriginal());
        setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
        setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
        setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-"
            + saidaDTO.getCdDigitoAgenciaOriginal() + " - "
            + saidaDTO.getDsAgenciaOriginal());
        setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
        setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-"
            + saidaDTO.getCdDigitoContaOriginal());
        setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
        
        setTipoContaDestinoPagamento(saidaDTO.getDsTipoContaDestino());
        setCdBancoDestinoPagamento(saidaDTO.getCdBancoDestino().toString());
        setContaDigitoDestinoPagamento(saidaDTO.getContaDestinoFormatada());
        setDsISPB(saidaDTO.getCdIspbPagtoDestino() + " - " + saidaDTO.getDsBancoDestino());
        setCdContaPagamentoDet(saidaDTO.getContaPagtoDestino());
        
        if(saidaDTO.getCdIndicadorModalidade() == 3){
        	setExibeBancoCredito(true);
        	if(saidaDTO.getContaPagtoDestino().equals("0")){
        		setCdBancoDestinoPagamento(null);
        		setContaDigitoDestinoPagamento(null);
        		setTipoContaDestinoPagamento(null);
        		setDsISPB(null);
        		setCdContaPagamentoDet(null);
        	}else {
        		setCdBancoDestino(null);
        		setAgenciaDigitoDestino(null);
        		setContaDigitoDestino(null);
        		setTipoContaDestino(null);
        	}
        	
        }else{
        	setExibeBancoCredito(false);
        }

    }

    // Detalhar Manuten��o Pagamentos Consolidados - D�bito em conta
    /**
     * Preenche dados man pagto debito conta.
     */
    public void preencheDadosManPagtoDebitoConta() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoDebitoContaEntradaDTO entradaDTO = new DetalharManPagtoDebitoContaEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoDebitoConta(entradaDTO);

        // Cabe�alho - Cliente
        setNrCnpjCpf("");
        if (saidaDTO.getCdCpfCnpjCliente() != null
                        && saidaDTO.getCdCpfCnpjCliente().compareTo(0L) != 0) {
            setNrCnpjCpf(PgitUtil.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente()
                .toString()));
        }
        setDsRazaoSocial(saidaDTO.getNomeCliente());

        // Sempre Carregar Conta de D�bito
        setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
            saidaDTO.getDsBancoDebito(), false));
        setDsAgenciaDebito(PgitUtil.formatAgencia(
            saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
            saidaDTO.getDsAgenciaDebito(), false));
        setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
            saidaDTO.getDsDigAgenciaDebito(), false));
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
            .getCdTipoInscricaoFavorecido(), saidaDTO
            .getNmInscriFavorecido()));
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(null);
        if (saidaDTO.getCdFavorecido() != null
                        && !saidaDTO.getCdFavorecido().equals("")) {
            setCdFavorecido(saidaDTO.getCdFavorecido());
        }

        setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(),
            saidaDTO.getCdDigContaCredito(), false));
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

    }

    // Detalhar Manuten��o Pagamentos Consolidados - DOC
    /**
     * Preenche dados man pagto doc.
     */
    public void preencheDadosManPagtoDOC() {
        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoDOCEntradaDTO entradaDTO = new DetalharManPagtoDOCEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoDOC(entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Cabe�alho
        setNrCnpjCpf("");
        if (saidaDTO.getCdCpfCnpjCliente() != null
                        && saidaDTO.getCdCpfCnpjCliente().compareTo(0L) != 0) {
            setNrCnpjCpf(PgitUtil.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente()
                .toString()));
        }
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        limparCamposFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidadePgit() == 1
                        || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
                .getCdTipoInscricaoFavorecido(), saidaDTO
                .getNmInscriFavorecido()));
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido()
                .toString());

            setExibeDcom(false);
            if (saidaDTO.getCdIndicadorModalidadePgit().compareTo(1) == 0) {
                setExibeDcom(true);
            }

            if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
            } else {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                    .parseInt(getTipoFavorecido())));
            }

            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(null);
            if (saidaDTO.getCdFavorecido() != null
                            && !saidaDTO.getCdFavorecido().equals("")) {
                setCdFavorecido(saidaDTO.getCdFavorecido());
            }

            setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || !PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                .equals("")
                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                .equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());

        setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
        setIdentificacaoTransferenciaDoc(saidaDTO
            .getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());

        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());

    }

    // Detalhar Manuten��o Pagamentos Consolidados - TED
    /**
     * Preenche dados man pagto ted.
     */
    public void preencheDadosManPagtoTED() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoTEDEntradaDTO entradaDTO = new DetalharManPagtoTEDEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgenPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgenPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgenPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoTEDSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoTED(entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        limparCamposFavorecidoBeneficiario();
        exibeBeneficiario = false;
        if (saidaDTO.getCdIndicadorModalidadePgit() == 1
                        || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(saidaDTO
                .getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido()
                .toString());

            setExibeDcom(false);
            if (saidaDTO.getCdIndicadorModalidadePgit().compareTo(1) == 0) {
                setExibeDcom(true);
            }
            if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
            } else {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                    .parseInt(getTipoFavorecido())));
            }

            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(null);
            if (saidaDTO.getCdFavorecido() != null
                            && !saidaDTO.getCdFavorecido().equals("")) {
                setCdFavorecido(saidaDTO.getCdFavorecido());
            }

            setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO
                .getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
            setExibeDcom(false);
        }

        // Pagamento
        setCdIndentificadorJudicial(saidaDTO.getCdIndentificadorJudicial());
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(String.valueOf(saidaDTO.getCdBancoCredito()));
        setDsBancoCredito(saidaDTO.getDsBancoCredito());
        setCdIspbPagamento(saidaDTO.getCdIspbPagamento());
        setDsNomeReclamante(saidaDTO.getDsNomeReclamante());
        setNrProcessoDepositoJudicial(saidaDTO.getNrProcessoDepositoJudicial());
        setNrPisPasep(NumberUtils.formatarNumeroPisPasep(saidaDTO.getNrPisPasep()));
        setCdFinalidadeTedDepositoJudicial(saidaDTO.isCdFinalidadeTedDepositoJudicial());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());

        setFinalidadeTed(saidaDTO.getFinalidadeTedFormatada());
        setIdentificacaoTransferenciaTed(saidaDTO
            .getDsIdentificacaoTransferenciaPagto());
        setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());

        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlDeducao(saidaDTO.getVlDeducao());
        setVlAcrescimo(saidaDTO.getVlAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
        setVlIss(saidaDTO.getVlIss());
        setVlIof(saidaDTO.getVlIof());
        setVlInss(saidaDTO.getVlInss());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());

    }

    // Detalhar Manuten��o Pagamentos Consolidado - Ordem de Pagamento
    /**
     * Preenche dados man pagto ordem pagto.
     */
    public void preencheDadosManPagtoOrdemPagto() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharManPagtoOrdemPagtoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoRelacionado());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoOrdemPagtoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoOrdemPagto(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Manuten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        limparCamposFavorecidoBeneficiario();
        exibeBeneficiario = false;

        if (saidaDTO.getCdIndicadorModalidadePgit() == 1
                        || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
            setNumeroInscricaoFavorecido(saidaDTO
                .getNumeroInscricaoFavorecidoFormatado());
            setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido()
                .toString());
            setExibeDcom(true);
            if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
            } else {
                setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                    .parseInt(getTipoFavorecido())));
            }

            setDsFavorecido(saidaDTO.getDsNomeFavorecido());
            setCdFavorecido(null);
            if (saidaDTO.getCdFavorecido() != null
                            && !saidaDTO.getCdFavorecido().equals("")) {
                setCdFavorecido(saidaDTO.getCdFavorecido());
            }

            setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
            setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
            setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
            setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
        } else {
            setNumeroInscricaoBeneficiario(saidaDTO
                .getNumeroInscricaoBeneficiarioFormatado());
            setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
            setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
            setExibeDcom(false);
            exibeBeneficiario = !PgitUtil.verificaStringNula(
                getNumeroInscricaoBeneficiario()).equals("")
                || (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
                                .equals(""))
                                || !PgitUtil.verificaStringNula(getNomeBeneficiario())
                                .equals("");
        }

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
        setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
        setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setNumeroOp(saidaDTO.getNrOperacao());
        setDataExpiracaoCredito(saidaDTO.getDtExpedicaoCredito());
        setInstrucaoPagamento(saidaDTO.getCdInsPagamento());
        setNumeroCheque(saidaDTO.getNrCheque());
        setSerieCheque(saidaDTO.getNrSerieCheque());
        setIndicadorAssociadoUnicaAgencia(saidaDTO.getDsUnidadeAgencia());

        setIdentificadorTipoRetirada(saidaDTO.getDsIdentificacaoTipoRetirada());
        setIdentificadorRetirada(saidaDTO.getDsIdentificacaoRetirada());

        setDataRetirada(saidaDTO.getDtRetirada());
        setVlImpostoMovFinanceira(saidaDTO.getVlImpostoMovimentacaoFinanceira());
        setVlDesconto(saidaDTO.getVlDescontoCredito());
        setVlAbatimento(saidaDTO.getVlAbatidoCredito());
        setVlMulta(saidaDTO.getVlMultaCredito());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
        setVlImpostoRenda(saidaDTO.getVlIrCredito());
        setVlIss(saidaDTO.getVlIssCredito());
        setVlIof(saidaDTO.getVlIofCredito());
        setVlInss(saidaDTO.getVlInssCredito());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    // Detalhar Manuten��o Pagamentos Consolidado - Titulos Bradesco
    /**
     * Preenche dados man pagto titulo bradesco.
     */
    public void preencheDadosManPagtoTituloBradesco() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharManPagtoTituloBradescoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoTituloBradesco(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Manunte��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(null);
        if (saidaDTO.getCdFavorecido() != null
                        && !saidaDTO.getCdFavorecido().equals("")) {
            setCdFavorecido(saidaDTO.getCdFavorecido());
        }

        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setCarteira(saidaDTO.getCarteira());
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setVlBonfcao(saidaDTO.getVlBonfcao());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
    }

    // Detalhar Manuten��o Pagamentos Consolidado - Titulos Outros Bancos
    /**
     * Preenche dados man pagto titulo outros bancos.
     */
    public void preencheDadosManPagtoTituloOutrosBancos() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharManPagtoTituloOutrosBancosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoTituloOutrosBancos(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Maunten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(null);
        if (saidaDTO.getCdFavorecido() != null
                        && !saidaDTO.getCdFavorecido().equals("")) {
            setCdFavorecido(saidaDTO.getCdFavorecido());
        }

        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setCarteira(saidaDTO.getCarteira());
        setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCodigoBarras(saidaDTO.getCdBarras());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlDeducao(saidaDTO.getVlOutrasDeducoes());
        setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
        setNossoNumero(saidaDTO.getNossoNumero());
        setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setVlBonfcao(saidaDTO.getVlBonfcao());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    // Detalhar Manuten��o Pagamentos Consolidado - DARF
    /**
     * Preenche dados man pagto darf.
     */
    public void preencheDadosManPagtoDARF() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoDARFEntradaDTO entradaDTO = new DetalharManPagtoDARFEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdAgendadosPagoNaoPago(4);

        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoDARF(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Maunten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(null);
        if (saidaDTO.getCdFavorecido() != null
                        && !saidaDTO.getCdFavorecido().equals("")) {
            setCdFavorecido(saidaDTO.getCdFavorecido());
        }

        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaDTO.getCdTelefone());
        setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
        setAgenteArrecadador(saidaDTO.getCdAgenteArrecad());
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcela());
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
        setAnoExercicio(saidaDTO.getCdDanoExercicio());
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
        setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
        setVlMulta(saidaDTO.getVlMultaCredito());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    // Detalhar Manuten��o Pagamentos Consolidado - GARE
    /**
     * Preenche dados man pagto gare.
     */
    public void preencheDadosManPagtoGARE() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoGAREEntradaDTO entradaDTO = new DetalharManPagtoGAREEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoGARE(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Manuten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(null);
        if (saidaDTO.getCdFavorecido() != null
                        && !saidaDTO.getCdFavorecido().equals("")) {
            setCdFavorecido(saidaDTO.getCdFavorecido());
        }

        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamentoGare(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
        setInscricaoEstadualContribuinte(saidaDTO
            .getCdInscricaoEstadualContribuinte());
        setCodigoReceita(saidaDTO.getCdReceita());
        setNumeroReferencia(saidaDTO.getNrReferencia());
        setNumeroCotaParcela(saidaDTO.getNrCota());
        setPercentualReceita(saidaDTO.getCdPercentualReceita());
        setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
        setDataReferencia(saidaDTO.getDtReferencia());
        setCodigoCnae(saidaDTO.getCdCnae());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaDTO.getNrParcela());
        setCodigoOrgaoFavorecido(saidaDTO.getCdOrFavorecido());
        setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsTributo());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    // Detalhar Manuten��o Pagamentos Consolidado - GPS
    /**
     * Preenche dados man pagto gps.
     */
    public void preencheDadosManPagtoGPS() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoGPSEntradaDTO entradaDTO = new DetalharManPagtoGPSEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoGPS(entradaDTO);

        // Manuten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(null);
        if (saidaDTO.getCdFavorecido() != null
                        && !saidaDTO.getCdFavorecido().equals("")) {
            setCdFavorecido(saidaDTO.getCdFavorecido());
        }

        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddContribuinte());
        setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
        setCodigoInss(saidaDTO.getCdInss());
        setDataCompetencia(saidaDTO.getDsMesAnoCompt());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    // Detalhar Manuten��o Pagamentos Individuais - C�digo de Barras
    /**
     * Preenche dados man pagto codigo barras.
     */
    public void preencheDadosManPagtoCodigoBarras() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());
        DetalharManPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharManPagtoCodigoBarrasEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoCodigoBarras(entradaDTO);

        // Manuten��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(null);
        if (saidaDTO.getCdFavorecido() != null
                        && !saidaDTO.getCdFavorecido().equals("")) {
            setCdFavorecido(saidaDTO.getCdFavorecido());
        }

        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setDddContribuinte(saidaDTO.getCdDddTelefone());
        setTelefoneContribuinte(saidaDTO.getNrTelefone());
        setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
        setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
        setCodigoReceita(saidaDTO.getCdReceitaTributo());
        setAgenteArrecadador(saidaDTO.getCdAgenteArrecadador());
        setCodigoTributo(saidaDTO.getCdTributoArrecadado());
        setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTributo());
        setPercentualReceita(saidaDTO.getCdPercentualTributo());
        setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
        setAnoExercicio(saidaDTO.getCdAnoExercicioTributo());
        setDataReferencia(saidaDTO.getDtReferenciaTributo());
        setDataCompetencia(saidaDTO.getDtCompensacao());
        setCodigoCnae(saidaDTO.getCdCnae());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setNumeroParcelamento(saidaDTO.getNrParcelamentoTributo());
        setCodigoOrgaoFavorecido(saidaDTO.getCdOrgaoFavorecido());
        setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
        setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
        setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
        setVlReceita(saidaDTO.getVlReceita());
        setVlPrincipal(saidaDTO.getVlPrincipal());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
        setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMora());
        setVlTotal(saidaDTO.getVlTotal());
        setObservacaoTributo(saidaDTO.getDsObservacao());
        setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

        setNumControleInternoLote(saidaDTO.getCdLoteInterno());
        setDsIndicadorAutorizacao(saidaDTO.getDsIndicadorAutorizacao());
        setDsTipoLayout(saidaDTO.getDsTipoLayout());
    }

    // Detalhar Manuten��o Pagamentos Consolidado - D�bito de Veiculos
    /**
     * Preenche dados man pagto debito veiculos.
     */
    public void preencheDadosManPagtoDebitoVeiculos() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharManPagtoDebitoVeiculosEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoDebitoVeiculos(entradaDTO);

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Manunte��o
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(null);
        if (saidaDTO.getCdFavorecido() != null
                        && !saidaDTO.getCdFavorecido().equals("")) {
            setCdFavorecido(saidaDTO.getCdFavorecido());
        }

        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTrib());
        setAnoExercicio(saidaDTO.getDtAnoExercTributo());
        setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
        setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
        setMunicipioTributo(saidaDTO.getCdMunicArrecadTrib());
        setUfTributo(saidaDTO.getCdUfTributo());
        setNumeroNsu(saidaDTO.getNrNsuTributo());
        setOpcaoTributo(saidaDTO.getCdOpcaoTributo());
        setOpcaoRetiradaTributo(saidaDTO.getCdOpcaoRetirada());
        setVlPrincipal(saidaDTO.getVlPrincipalTributo());
        setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
        setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
        setVlDesconto(saidaDTO.getVlDesconto());
        setVlAbatimento(saidaDTO.getVlAbatimento());
        setVlMulta(saidaDTO.getVlMulta());
        setVlMora(saidaDTO.getVlMoraJuros());
        setVlTotal(saidaDTO.getVlTotalTributo());
        setObservacaoTributo(saidaDTO.getDsObservacaoTributo());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    // Detalhar Manuten��o Pagamentos Consolidado - Deposito Identificado
    /**
     * Preenche dados man pagto dep identificado.
     */
    public void preencheDadosManPagtoDepIdentificado() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharManPagtoDepIdentificadoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoDepIdentificado(entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO
            .getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(null);
        if (saidaDTO.getCdFavorecido() != null
                        && !saidaDTO.getCdFavorecido().equals("")) {
            setCdFavorecido(saidaDTO.getCdFavorecido());
        }

        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtPagamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setClienteDepositoIdentificado(saidaDTO.getCdClienteDepositoId());
        setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
        setProdutoDepositoIdentificado(saidaDTO.getCdProdutoDepositoId());
        setSequenciaProdutoDepositoIdentificado(saidaDTO
            .getCdSequenciaProdutoDepositoId());
        setBancoCartaoDepositoIdentificado(saidaDTO
            .getCdBancoCartaoDepositanteId());
        setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
        setBimCartaoDepositoIdentificado(saidaDTO.getCdBinCartaoDepositanteId());
        setViaCartaoDepositoIdentificado(saidaDTO.getCdViaCartaoDepositanteId());
        setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
        setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

    }

    // Detalhar Manuten��o Pagamentos Consolidado - Ordem de Cr�dito
    /**
     * Preenche dados man pagto ordem credito.
     */
    public void preencheDadosManPagtoOrdemCredito() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
        .get(getItemSelecionadoListaConPagamentosConsolidado());

        OcorrenciasDetalharPagamentosConsolidados selecaoFiltroDetalhe = getListaGridDetPagamentosConsolidado()
        .get(getItemSelecionadoListaDetPagamentosConsolidado());

        ConsultarManutencaoPagamentoSaidaDTO registroSelecionadoManutencao = getListaConsultarManutencao()
        .get(getItemSelecionadoListaManutencao());

        DetalharManPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharManPagtoOrdemCreditoEntradaDTO();

        entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
            .getCdPessoaJuridicaContrato());
        entradaDTO.setCdTipoContratoNegocio(registroSelecionado
            .getCdTipoContratoNegocio());
        entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
            .getNrContratoOrigem());
        entradaDTO.setDsEfetivacaoPagamento(selecaoFiltroDetalhe
            .getDsEfetivacaoPagamento());
        entradaDTO
        .setCdControlePagamento(selecaoFiltroDetalhe.getNrPagamento());
        entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
        entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
        entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
        entradaDTO.setCdBancoCredito(selecaoFiltroDetalhe.getCdBanco());
        entradaDTO.setCdAgenciaCredito(selecaoFiltroDetalhe.getCdAgencia());
        entradaDTO.setCdContaCredito(selecaoFiltroDetalhe.getCdConta());

        entradaDTO.setCdAgendadosPagoNaoPago(4);
        if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos()).equals("")) {
            entradaDTO.setCdAgendadosPagoNaoPago(0);
        } else if (PgitUtil.verificaStringNula(getAgendadosPagosNaoPagos())
                        .equals("1")) {
            entradaDTO.setCdAgendadosPagoNaoPago(3);
        }

        entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
            .getCdProdutoServicoOperacao());
        entradaDTO.setDtHoraManutencao(registroSelecionadoManutencao
            .getDtHoraManutencao());

        DetalharManPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
        .detalharManPagtoOrdemCredito(entradaDTO);

        // Manutencao
        setDsTipoManutencao(saidaDTO.getDsTipoManutencao());

        // CLIENTE
        setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
        setDsRazaoSocial(saidaDTO.getNomeCliente());
        setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
        setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
        setDsContrato(saidaDTO.getDsContrato());
        setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

        // CONTA D�BITO
        setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
        setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
        setContaDebito(saidaDTO.getContaDebitoFormatada());
        setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

        // Beneficiario
        setNumeroInscricaoBeneficiario(saidaDTO
            .getNumeroInscricaoBeneficiarioFormatado());
        setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
        setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

        // Favorecido
        setNumeroInscricaoFavorecido(saidaDTO
            .getNumeroInscricaoFavorecidoFormatado());
        setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
        if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
        } else {
            setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
                .parseInt(getTipoFavorecido())));
        }

        setDsFavorecido(saidaDTO.getDsNomeFavorecido());
        setCdFavorecido(null);
        if (saidaDTO.getCdFavorecido() != null
                        && !saidaDTO.getCdFavorecido().equals("")) {
            setCdFavorecido(saidaDTO.getCdFavorecido());
        }

        setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
        setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
        setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
        setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

        // Pagamento
        setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
        setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
        setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
        setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
        setNumeroPagamento(saidaDTO.getCdControlePagamento());
        setCdListaDebito(saidaDTO.getCdListaDebito());
        setDtAgendamento(saidaDTO.getDtAgendamento());
        setDtPagamento(saidaDTO.getDtAgendamento());
        setDtVencimento(saidaDTO.getDtVencimento());
        setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
        setQtMoeda(saidaDTO.getQtMoeda());
        setVlrAgendamento(saidaDTO.getVlAgendado());
        setVlrEfetivacao(saidaDTO.getVlEfetivo());
        setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
        setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
        setDsPagamento(saidaDTO.getDsPagamento());
        setNrDocumento(saidaDTO.getNrDocumento());
        setTipoDocumento(saidaDTO.getDsTipoDocumento());
        setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
        setVlDocumento(saidaDTO.getVlDocumento());
        setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
        setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
        setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
        setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
        setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
            saidaDTO.getDsBancoFavorecido(), false));
        setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
            .getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
            saidaDTO.getDsAgenciaFavorecido(), false));
        setContaDigitoCredito(PgitUtil.formatConta(
            saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
            false));
        setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
        setCodigoPagador(saidaDTO.getCdPagador());
        setCodigoOrdemCredito(saidaDTO.getCdOrdemCreditoPagamento());
        setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
        setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
        setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
        setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
        setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
        setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
        setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
        setVlIss(saidaDTO.getVlIssCreditoPagamento());
        setVlIof(saidaDTO.getVlIofCreditoPagamento());
        setVlInss(saidaDTO.getVlInssCreditoPagamento());
        setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
        setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
        setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
        setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
        setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

        // trilhaAuditoria
        setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
        setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
        setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
        setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
        setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
        setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
        setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
        setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
    }

    /**
     * Limpar favorecido manutencao.
     */
    public void limparFavorecidoManutencao() {
        setNumeroInscricaoFavorecido("");
        setDsFavorecido("");
        setTipoFavorecidoManutencao(0);
        setDescTipoFavorecido("");
        setCdFavorecido("");
        setSituacaoPagamento("");
        setMotivoPagamento("");
        setInscricaoFavorecido("");
        setCdTipoInscricaoFavorecido("");
        setTipoContaFavorecido("");
        setBancoFavorecido("");
        setAgenciaFavorecido("");
        setContaFavorecido("");
    }

    /**
     * Consultar autorizantes.
     *
     * @return the string
     */
    public String consultarAutorizantes(){
        setItemSelecionadoListaAutorizante(null);
        setListaRadioAutorizante(new ArrayList<SelectItem>());
        
        try {
            ConsultarAutorizantesNetEmpresaEntradaDTO entradaDTO = new ConsultarAutorizantesNetEmpresaEntradaDTO();
            OcorrenciasDetalharPagamentosConsolidados registroSelecionadoDetalhar = getListaGridDetPagamentosConsolidado()
            .get(getItemSelecionadoListaDetPagamentosConsolidado());

            ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado = getListaGridConPagamentosConsolidado()
            .get(getItemSelecionadoListaConPagamentosConsolidado());

            entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
            entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
            entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
            entradaDTO.setCdTipoCanal(registroSelecionadoDetalhar.getCdTipoCanal());
            entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
            entradaDTO.setCdModalidade(registroSelecionadoDetalhar.getCdTipoTela());
            entradaDTO.setCdAgendadosPagoNaoPago(verificaStringToIntegerNula(getAgendadosPagosNaoPagos()));
            entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
            entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
            entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
            entradaDTO.setCdDigitoAgenciaDebito(String.valueOf(registroSelecionado.getCdDigitoAgencia()));

            ConsultarAutorizantesNetEmpresaSaidaDTO saida =
                consultaAutorizanteService.consultarAutorizantesNetEmpresa(entradaDTO);
            
            consultarAutorizantesOcorrencia = new ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO>();
            listaRadioAutorizante = new ArrayList<SelectItem>();
            for (int i = 0; i < saida.getOcorrencias().size(); i++) {
                consultarAutorizantesOcorrencia.add(saida.getOcorrencias().get(i));
                listaRadioAutorizante.add(new SelectItem(i, ""));
            }

            consultarManutencao();
        } catch (PdcAdapterFunctionalException p) {
            
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            return "";
        }
        
        return "CONSULTAR_AUTORIZANTE";
    }
    
    /**
     * Consultar autorizantes paginacao.
     *
     * @param evt the evt
     * @return the string
     */
    public String consultarAutorizantesPaginacao(ActionEvent evt) {
        consultarAutorizantes();
        return "";
    }

    /**
     * Voltar autorizante.
     *
     * @return the string
     */
    public String voltarAutorizante() {
        setItemSelecionadoListaDetPagamentosConsolidado(null);
        return "VOLTAR_AUTORIZANTE";
    }  

    /**
     * Imprimir relatorio autorizante.
     */
    public void imprimirRelatorioAutorizante(){
        String caminho = "/images/Bradesco_logo.JPG";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String logoPath = servletContext.getRealPath(caminho);

        // Par�metros do Relat�rio
        Map<String, Object> par = new HashMap<String, Object>();

        par.put("cpfCnpj", getNrCnpjCpf());
        par.put("nomeRazaoSocial", getDsRazaoSocial());
        par.put("empresa", getDsEmpresa());
        par.put("numero", getNroContrato());
        par.put("descContrato", getDsContrato());
        par.put("situacao", getCdSituacaoContrato());
        par.put("logoBradesco", logoPath);

        par.put("titulo", "Consultar Autorizante do Pagamento Net Empresa - Consolidado");

        par.put("bancoDebito", getDsBancoDebito());
        par.put("agenciaDebito", getDsAgenciaDebito());
        par.put("contaDebito", getContaDebito());
        par.put("tipoContaDebito", getTipoContaDebito());
        par.put("tipoServico", getDsTipoServico());
        par.put("modalidade", getDsIndicadorModalidade());
        par.put("remessa", getNrSequenciaArquivoRemessa());
        par.put("lote", getNrLote());
        par.put("valorPagamento", getValorPagamentoFormatado());
        par.put("numeroPagamento", getNumeroPagamento());
        par.put("listaDebito", getCdListaDebito());
        par.put("dataPagamento", getDtPagamentoManutencao());
        par.put("dataVencimento", getDtVencimento());
        par.put("numeroInscricaoFavorecido", getNumeroInscricaoFavorecido());
        par.put("tipoFavorecido", getTipoFavorecido());
        par.put("descTipoFavorecido", getTipoFavorecido());
        par.put("cdFavorecido", getCdFavorecido());
        par.put("dsFavorecido", getDsFavorecido());
        par.put("bancoFavorecido", getBancoFavorecido());
        par.put("agenciaFavorecido", getAgenciaFavorecido());
        par.put("contaFavorecido", getContaFavorecido());
        par.put("tipoContaFavorecido", getTipoContaFavorecido());
        par.put("motivoPagamento", getMotivoPagamento());
        par.put("situacaoPagamento", getSituacaoPagamento());
        par.put("dsBancoDebito", getDsBancoDebito());
        par.put("dsAgenciaDebito", getDsAgenciaDebito());
        par.put("contaDebito", getContaDebito());

        try {

            PgitUtil.geraRelatorioPdf("/relatorios/agendamentoEfetivacaoEstorno/consultarPagamentos/consolidado",
                "imprimirAutorizantesNetEmpresaConsolidado", getConsultarAutorizantesOcorrencia(), par);

        } /* Exce��o do relat�rio */
        catch (Exception e) {
            throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
        }
    }
    
    /**
     * Habilita botoes.
     */
    public void habilitaBotoes() {
        isHabilitaBotaoConsultarAutorizantes();
    }
    
    /**
     * Is habilita botao consultar autorizantes.
     *
     * @return true, if is habilita botao consultar autorizantes
     */
    public boolean isHabilitaBotaoConsultarAutorizantes(){
        if (getItemSelecionadoListaDetPagamentosConsolidado() != null){
            return false;
        }
        return true;
    }

    /**
     * Is botao detalhar retirada visivel.
     * 
     * @return true, if is botao detalhar retirada visivel
     */
    public boolean isBotaoDetalharRetiradaVisivel() {
        return detalheOrdemPagamento != null && detalheOrdemPagamento.getCdIdentificacaoTipoRetirada() != null
            && detalheOrdemPagamento.getCdIdentificacaoTipoRetirada().equals(CODIGO_IDENTIFICACAO_DIVERSOS); 
    }

    /**
     * Detalhar retirada.
     * 
     * @return the string
     */
    public String detalharRetirada() {
        carregarListaFormaPagamento();

        return "DETALHE_RETIRADA";
    }

    /**
     * Carregar lista forma pagamento.
     */
    private void carregarListaFormaPagamento() {
        ConsultarPagamentosConsolidadosSaidaDTO registroSelecionado =
            getListaGridConPagamentosConsolidado().get(getItemSelecionadoListaConPagamentosConsolidado());
        
        ListarTipoRetiradaEntradaDTO entrada = new ListarTipoRetiradaEntradaDTO();
        entrada.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
        entrada.setCdControlePagamento(detalheOrdemPagamento.getCdControlePagamento());
        entrada.setCdProdutoServicoOperacao(registroSelecionado.getCdProdutoServicoOperacao());
        entrada.setCdProdutoOperacaoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
        entrada.setCdBancoPagador(detalheOrdemPagamento.getCdBancoDebito());
        entrada.setCdAgenciaBancariaPagador(detalheOrdemPagamento.getCdAgenciaDebito());
        entrada.setCdDigitoAgenciaPagador(detalheOrdemPagamento.getCdDigAgenciaDebto());
        entrada.setCdContaBancariaPagador(detalheOrdemPagamento.getCdContaDebito());
        entrada.setCdDigitoContaPagador(detalheOrdemPagamento.getDsDigAgenciaDebito());
        entrada.setCdBancoFavorecido(detalheOrdemPagamento.getCdBancoCredito());
        entrada.setCdAgenciaFavorecido(detalheOrdemPagamento.getCdAgenciaCredito());
        entrada.setCdDigitoAgenciaFavorecido(detalheOrdemPagamento.getCdDigAgenciaCredito());
        entrada.setCdContaFavorecido(detalheOrdemPagamento.getCdContaCredito());
        entrada.setCdDigitoContaFavorecido(detalheOrdemPagamento.getCdDigContaCredito());
        entrada.setDtPagamentoDe(detalheOrdemPagamento.getDtAgendamento());
        entrada.setDtPagamentoAte(detalheOrdemPagamento.getDtVencimento());

        ListarTipoRetiradaSaidaDTO saidaDetalheRetirada = consultasService.listarTipoRetirada(entrada);
        listaFormaPagamento = saidaDetalheRetirada.getOcorrencias();
    }

    /**
     * Paginar lista forma pagamento.
     * 
     * @param actionEvent
     *            the action event
     */
    public void paginarListaFormaPagamento(ActionEvent actionEvent) {
        carregarListaFormaPagamento();
    }

    /**
     * Voltar detalhe retirada.
     * 
     * @return the string
     */
    public String voltarDetalheRetirada() {
        return "VOLTAR_DETALHE";
    }
    
    /**
     * (non-Javadoc).
     *
     * @return true, if is disable argumentos consulta
     * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#isDisableArgumentosConsulta()
     */
    @Override
    public boolean isDisableArgumentosConsulta() {
        return disableArgumentosConsulta;
    }

    /**
     * (non-Javadoc).
     *
     * @param disableArgumentosConsulta the disable argumentos consulta
     * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#setDisableArgumentosConsulta(boolean)
     */
    @Override
    public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
        this.disableArgumentosConsulta = disableArgumentosConsulta;
    }

    /**
     * Get: consultarPagamentosConsolidadoServiceImpl.
     *
     * @return consultarPagamentosConsolidadoServiceImpl
     */
    public IConsultarPagamentosConsolidadoService getConsultarPagamentosConsolidadoServiceImpl() {
        return consultarPagamentosConsolidadoServiceImpl;
    }

    /**
     * Set: consultarPagamentosConsolidadoServiceImpl.
     *
     * @param consultarPagamentosConsolidadoServiceImpl the consultar pagamentos consolidado service impl
     */
    public void setConsultarPagamentosConsolidadoServiceImpl(
        IConsultarPagamentosConsolidadoService consultarPagamentosConsolidadoServiceImpl) {
        this.consultarPagamentosConsolidadoServiceImpl = consultarPagamentosConsolidadoServiceImpl;
    }

    /**
     * Get: consultasService.
     *
     * @return consultasService
     */
    public IConsultasService getConsultasService() {
        return consultasService;
    }

    /**
     * Set: consultasService.
     *
     * @param consultasService the consultas service
     */
    public void setConsultasService(IConsultasService consultasService) {
        this.consultasService = consultasService;
    }

    /**
     * Get: listaGridConPagamentosConsolidado.
     *
     * @return listaGridConPagamentosConsolidado
     */
    public List<ConsultarPagamentosConsolidadosSaidaDTO> getListaGridConPagamentosConsolidado() {
        return listaGridConPagamentosConsolidado;
    }

    /**
     * Set: listaGridConPagamentosConsolidado.
     *
     * @param listaGridConPagamentosConsolidado the lista grid con pagamentos consolidado
     */
    public void setListaGridConPagamentosConsolidado(
        List<ConsultarPagamentosConsolidadosSaidaDTO> listaGridConPagamentosConsolidado) {
        this.listaGridConPagamentosConsolidado = listaGridConPagamentosConsolidado;
    }

    /**
     * Get: listaRadiosConPagamentosConsolidado.
     *
     * @return listaRadiosConPagamentosConsolidado
     */
    public List<SelectItem> getListaRadiosConPagamentosConsolidado() {
        return listaRadiosConPagamentosConsolidado;
    }

    /**
     * Set: listaRadiosConPagamentosConsolidado.
     *
     * @param listaRadiosConPagamentosConsolidado the lista radios con pagamentos consolidado
     */
    public void setListaRadiosConPagamentosConsolidado(
        List<SelectItem> listaRadiosConPagamentosConsolidado) {
        this.listaRadiosConPagamentosConsolidado = listaRadiosConPagamentosConsolidado;
    }

    /**
     * Get: itemSelecionadoListaConPagamentosConsolidado.
     *
     * @return itemSelecionadoListaConPagamentosConsolidado
     */
    public Integer getItemSelecionadoListaConPagamentosConsolidado() {
        return itemSelecionadoListaConPagamentosConsolidado;
    }

    /**
     * Set: itemSelecionadoListaConPagamentosConsolidado.
     *
     * @param itemSelecionadoListaConPagamentosConsolidado the item selecionado lista con pagamentos consolidado
     */
    public void setItemSelecionadoListaConPagamentosConsolidado(
        Integer itemSelecionadoListaConPagamentosConsolidado) {
        this.itemSelecionadoListaConPagamentosConsolidado = itemSelecionadoListaConPagamentosConsolidado;
    }

    /**
     * Get: listaGridDetPagamentosConsolidado.
     *
     * @return listaGridDetPagamentosConsolidado
     */
    public List<OcorrenciasDetalharPagamentosConsolidados> getListaGridDetPagamentosConsolidado() {
        return listaGridDetPagamentosConsolidado;
    }

    /**
     * Set: listaGridDetPagamentosConsolidado.
     *
     * @param listaGridDetPagamentosConsolidado the lista grid det pagamentos consolidado
     */
    public void setListaGridDetPagamentosConsolidado(
        List<OcorrenciasDetalharPagamentosConsolidados> listaGridDetPagamentosConsolidado) {
        this.listaGridDetPagamentosConsolidado = listaGridDetPagamentosConsolidado;
    }

    /**
     * Get: listaRadiosDetPagamentosConsolidado.
     *
     * @return listaRadiosDetPagamentosConsolidado
     */
    public List<SelectItem> getListaRadiosDetPagamentosConsolidado() {
        return listaRadiosDetPagamentosConsolidado;
    }

    /**
     * Set: listaRadiosDetPagamentosConsolidado.
     *
     * @param listaRadiosDetPagamentosConsolidado the lista radios det pagamentos consolidado
     */
    public void setListaRadiosDetPagamentosConsolidado(
        List<SelectItem> listaRadiosDetPagamentosConsolidado) {
        this.listaRadiosDetPagamentosConsolidado = listaRadiosDetPagamentosConsolidado;
    }

    /**
     * Get: itemSelecionadoListaDetPagamentosConsolidado.
     *
     * @return itemSelecionadoListaDetPagamentosConsolidado
     */
    public Integer getItemSelecionadoListaDetPagamentosConsolidado() {
        return itemSelecionadoListaDetPagamentosConsolidado;
    }

    /**
     * Set: itemSelecionadoListaDetPagamentosConsolidado.
     *
     * @param itemSelecionadoListaDetPagamentosConsolidado the item selecionado lista det pagamentos consolidado
     */
    public void setItemSelecionadoListaDetPagamentosConsolidado(
        Integer itemSelecionadoListaDetPagamentosConsolidado) {
        this.itemSelecionadoListaDetPagamentosConsolidado = itemSelecionadoListaDetPagamentosConsolidado;
    }

    /**
     * Get: tipoTela.
     *
     * @return tipoTela
     */
    public Integer getTipoTela() {
        return tipoTela;
    }

    /**
     * Set: tipoTela.
     *
     * @param tipoTela the tipo tela
     */
    public void setTipoTela(Integer tipoTela) {
        this.tipoTela = tipoTela;
    }

    /**
     * Get: nrCnpjCpf.
     *
     * @return nrCnpjCpf
     */
    public String getNrCnpjCpf() {
        return nrCnpjCpf;
    }

    /**
     * Set: nrCnpjCpf.
     *
     * @param nrCnpjCpf the nr cnpj cpf
     */
    public void setNrCnpjCpf(String nrCnpjCpf) {
        this.nrCnpjCpf = nrCnpjCpf;
    }

    /**
     * Get: dsRazaoSocial.
     *
     * @return dsRazaoSocial
     */
    public String getDsRazaoSocial() {
        return dsRazaoSocial;
    }

    /**
     * Set: dsRazaoSocial.
     *
     * @param dsRazaoSocial the ds razao social
     */
    public void setDsRazaoSocial(String dsRazaoSocial) {
        this.dsRazaoSocial = dsRazaoSocial;
    }

    /**
     * Get: dsEmpresa.
     *
     * @return dsEmpresa
     */
    public String getDsEmpresa() {
        return dsEmpresa;
    }

    /**
     * Set: dsEmpresa.
     *
     * @param dsEmpresa the ds empresa
     */
    public void setDsEmpresa(String dsEmpresa) {
        this.dsEmpresa = dsEmpresa;
    }

    /**
     * Get: nroContrato.
     *
     * @return nroContrato
     */
    public String getNroContrato() {
        return nroContrato;
    }

    /**
     * Set: nroContrato.
     *
     * @param nroContrato the nro contrato
     */
    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    /**
     * Get: dsContrato.
     *
     * @return dsContrato
     */
    public String getDsContrato() {
        return dsContrato;
    }

    /**
     * Set: dsContrato.
     *
     * @param dsContrato the ds contrato
     */
    public void setDsContrato(String dsContrato) {
        this.dsContrato = dsContrato;
    }

    /**
     * Get: cdSituacaoContrato.
     *
     * @return cdSituacaoContrato
     */
    public String getCdSituacaoContrato() {
        return cdSituacaoContrato;
    }

    /**
     * Set: cdSituacaoContrato.
     *
     * @param cdSituacaoContrato the cd situacao contrato
     */
    public void setCdSituacaoContrato(String cdSituacaoContrato) {
        this.cdSituacaoContrato = cdSituacaoContrato;
    }

    /**
     * Get: dsTipoServico.
     *
     * @return dsTipoServico
     */
    public String getDsTipoServico() {
        return dsTipoServico;
    }

    /**
     * Set: dsTipoServico.
     *
     * @param dsTipoServico the ds tipo servico
     */
    public void setDsTipoServico(String dsTipoServico) {
        this.dsTipoServico = dsTipoServico;
    }

    /**
     * Get: dsEstornoPagamento.
     *
     * @return dsEstornoPagamento
     */
    public String getDsEstornoPagamento() {
        return dsEstornoPagamento;
    }

    /**
     * Set: dsEstornoPagamento.
     *
     * @param dsEstornoPagamento the ds estorno pagamento
     */
    public void setDsEstornoPagamento(String dsEstornoPagamento) {
        this.dsEstornoPagamento = dsEstornoPagamento;
    }

    /**
     * Get: dsIndicadorModalidade.
     *
     * @return dsIndicadorModalidade
     */
    public String getDsIndicadorModalidade() {
        return dsIndicadorModalidade;
    }

    /**
     * Set: dsIndicadorModalidade.
     *
     * @param dsIndicadorModalidade the ds indicador modalidade
     */
    public void setDsIndicadorModalidade(String dsIndicadorModalidade) {
        this.dsIndicadorModalidade = dsIndicadorModalidade;
    }

    /**
     * Get: dsQtdePagamentos.
     *
     * @return dsQtdePagamentos
     */
    public String getDsQtdePagamentos() {
        return dsQtdePagamentos;
    }

    /**
     * Set: dsQtdePagamentos.
     *
     * @param dsQtdePagamentos the ds qtde pagamentos
     */
    public void setDsQtdePagamentos(String dsQtdePagamentos) {
        this.dsQtdePagamentos = dsQtdePagamentos;
    }

    /**
     * Get: dsVlrTotalPagamentos.
     *
     * @return dsVlrTotalPagamentos
     */
    public BigDecimal getDsVlrTotalPagamentos() {
        return dsVlrTotalPagamentos;
    }

    /**
     * Set: dsVlrTotalPagamentos.
     *
     * @param dsVlrTotalPagamentos the ds vlr total pagamentos
     */
    public void setDsVlrTotalPagamentos(BigDecimal dsVlrTotalPagamentos) {
        this.dsVlrTotalPagamentos = dsVlrTotalPagamentos;
    }

    /**
     * Get: dtPagamentoFiltro.
     *
     * @return dtPagamentoFiltro
     */
    public String getDtPagamentoFiltro() {
        return dtPagamentoFiltro;
    }

    /**
     * Set: dtPagamentoFiltro.
     *
     * @param dtPagamentoFiltro the dt pagamento filtro
     */
    public void setDtPagamentoFiltro(String dtPagamentoFiltro) {
        this.dtPagamentoFiltro = dtPagamentoFiltro;
    }

    /**
     * Get: nrRemessaFiltro.
     *
     * @return nrRemessaFiltro
     */
    public Long getNrRemessaFiltro() {
        return nrRemessaFiltro;
    }

    /**
     * Set: nrRemessaFiltro.
     *
     * @param nrRemessaFiltro the nr remessa filtro
     */
    public void setNrRemessaFiltro(Long nrRemessaFiltro) {
        this.nrRemessaFiltro = nrRemessaFiltro;
    }

    /**
     * Get: dsSituacaoPagamentoFiltro.
     *
     * @return dsSituacaoPagamentoFiltro
     */
    public String getDsSituacaoPagamentoFiltro() {
        return dsSituacaoPagamentoFiltro;
    }

    /**
     * Set: dsSituacaoPagamentoFiltro.
     *
     * @param dsSituacaoPagamentoFiltro the ds situacao pagamento filtro
     */
    public void setDsSituacaoPagamentoFiltro(String dsSituacaoPagamentoFiltro) {
        this.dsSituacaoPagamentoFiltro = dsSituacaoPagamentoFiltro;
    }

    /**
     * Get: logoPath.
     *
     * @return logoPath
     */
    public String getLogoPath() {
        return logoPath;
    }

    /**
     * Set: logoPath.
     *
     * @param logoPath the logo path
     */
    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    /**
     * Get: dsBancoDebito.
     *
     * @return dsBancoDebito
     */
    public String getDsBancoDebito() {
        return dsBancoDebito;
    }

    /**
     * Set: dsBancoDebito.
     *
     * @param dsBancoDebito the ds banco debito
     */
    public void setDsBancoDebito(String dsBancoDebito) {
        this.dsBancoDebito = dsBancoDebito;
    }

    /**
     * Get: dsBancoPagto.
     *
     * @return dsBancoPagto
     */
    public String getDsBancoPagto() {
        return dsBancoPagto;
    }

    /**
     * Set: dsBancoPagto.
     *
     * @param dsBancoPagto the ds banco pagto
     */
    public void setDsBancoPagto(String dsBancoPagto) {
        this.dsBancoPagto = dsBancoPagto;
    }

    /**
     * Get: dsAgenciaDebito.
     *
     * @return dsAgenciaDebito
     */
    public String getDsAgenciaDebito() {
        return dsAgenciaDebito;
    }

    /**
     * Set: dsAgenciaDebito.
     *
     * @param dsAgenciaDebito the ds agencia debito
     */
    public void setDsAgenciaDebito(String dsAgenciaDebito) {
        this.dsAgenciaDebito = dsAgenciaDebito;
    }

    /**
     * Get: dsAgenciaPagto.
     *
     * @return dsAgenciaPagto
     */
    public String getDsAgenciaPagto() {
        return dsAgenciaPagto;
    }

    /**
     * Set: dsAgenciaPagto.
     *
     * @param dsAgenciaPagto the ds agencia pagto
     */
    public void setDsAgenciaPagto(String dsAgenciaPagto) {
        this.dsAgenciaPagto = dsAgenciaPagto;
    }

    /**
     * Get: contaDebito.
     *
     * @return contaDebito
     */
    public String getContaDebito() {
        return contaDebito;
    }

    /**
     * Set: contaDebito.
     *
     * @param contaDebito the conta debito
     */
    public void setContaDebito(String contaDebito) {
        this.contaDebito = contaDebito;
    }

    /**
     * Get: contaPagto.
     *
     * @return contaPagto
     */
    public String getContaPagto() {
        return contaPagto;
    }

    /**
     * Set: contaPagto.
     *
     * @param contaPagto the conta pagto
     */
    public void setContaPagto(String contaPagto) {
        this.contaPagto = contaPagto;
    }

    /**
     * Get: tipoContaDebito.
     *
     * @return tipoContaDebito
     */
    public String getTipoContaDebito() {
        return tipoContaDebito;
    }

    /**
     * Set: tipoContaDebito.
     *
     * @param tipoContaDebito the tipo conta debito
     */
    public void setTipoContaDebito(String tipoContaDebito) {
        this.tipoContaDebito = tipoContaDebito;
    }

    /**
     * Get: descricaoTipoContaDebito.
     *
     * @return descricaoTipoContaDebito
     */
    public String getDescricaoTipoContaDebito() {
        return descricaoTipoContaDebito;
    }

    /**
     * Set: descricaoTipoContaDebito.
     *
     * @param descricaoTipoContaDebito the descricao tipo conta debito
     */
    public void setDescricaoTipoContaDebito(String descricaoTipoContaDebito) {
        this.descricaoTipoContaDebito = descricaoTipoContaDebito;
    }

    /**
     * Get: descricaoBancoContaDebito.
     *
     * @return descricaoBancoContaDebito
     */
    public String getDescricaoBancoContaDebito() {
        return descricaoBancoContaDebito;
    }

    /**
     * Set: descricaoBancoContaDebito.
     *
     * @param descricaoBancoContaDebito the descricao banco conta debito
     */
    public void setDescricaoBancoContaDebito(String descricaoBancoContaDebito) {
        this.descricaoBancoContaDebito = descricaoBancoContaDebito;
    }

    /**
     * Get: descricaoAgenciaContaDebitoBancaria.
     *
     * @return descricaoAgenciaContaDebitoBancaria
     */
    public String getDescricaoAgenciaContaDebitoBancaria() {
        return descricaoAgenciaContaDebitoBancaria;
    }

    /**
     * Set: descricaoAgenciaContaDebitoBancaria.
     *
     * @param descricaoAgenciaContaDebitoBancaria the descricao agencia conta debito bancaria
     */
    public void setDescricaoAgenciaContaDebitoBancaria(
        String descricaoAgenciaContaDebitoBancaria) {
        this.descricaoAgenciaContaDebitoBancaria = descricaoAgenciaContaDebitoBancaria;
    }

    /**
     * Get: digAgenciaDebito.
     *
     * @return digAgenciaDebito
     */
    public String getDigAgenciaDebito() {
        return digAgenciaDebito;
    }

    /**
     * Set: digAgenciaDebito.
     *
     * @param digAgenciaDebito the dig agencia debito
     */
    public void setDigAgenciaDebito(String digAgenciaDebito) {
        this.digAgenciaDebito = digAgenciaDebito;
    }

    /**
     * Get: digContaDebito.
     *
     * @return digContaDebito
     */
    public String getDigContaDebito() {
        return digContaDebito;
    }

    /**
     * Set: digContaDebito.
     *
     * @param digContaDebito the dig conta debito
     */
    public void setDigContaDebito(String digContaDebito) {
        this.digContaDebito = digContaDebito;
    }

    /**
     * Get: numControleInternoLote.
     *
     * @return numControleInternoLote
     */
    public Long getNumControleInternoLote() {
        return numControleInternoLote;
    }

    /**
     * Set: numControleInternoLote.
     *
     * @param numControleInternoLote the num controle interno lote
     */
    public void setNumControleInternoLote(Long numControleInternoLote) {
        this.numControleInternoLote = numControleInternoLote;
    }

    /**
     * Get: numeroInscricaoFavorecido.
     *
     * @return numeroInscricaoFavorecido
     */
    public String getNumeroInscricaoFavorecido() {
        return numeroInscricaoFavorecido;
    }

    /**
     * Set: numeroInscricaoFavorecido.
     *
     * @param numeroInscricaoFavorecido the numero inscricao favorecido
     */
    public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
        this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
    }

    /**
     * Get: tipoFavorecido.
     *
     * @return tipoFavorecido
     */
    public String getTipoFavorecido() {
        return tipoFavorecido;
    }

    /**
     * Set: tipoFavorecido.
     *
     * @param tipoFavorecido the tipo favorecido
     */
    public void setTipoFavorecido(String tipoFavorecido) {
        this.tipoFavorecido = tipoFavorecido;
    }

    /**
     * Get: dsFavorecido.
     *
     * @return dsFavorecido
     */
    public String getDsFavorecido() {
        return dsFavorecido;
    }

    /**
     * Set: dsFavorecido.
     *
     * @param dsFavorecido the ds favorecido
     */
    public void setDsFavorecido(String dsFavorecido) {
        this.dsFavorecido = dsFavorecido;
    }

    /**
     * Get: cdFavorecido.
     *
     * @return cdFavorecido
     */
    public String getCdFavorecido() {
        return cdFavorecido;
    }

    /**
     * Set: cdFavorecido.
     *
     * @param cdFavorecido the cd favorecido
     */
    public void setCdFavorecido(String cdFavorecido) {
        this.cdFavorecido = cdFavorecido;
    }

    /**
     * Get: dsBancoFavorecido.
     *
     * @return dsBancoFavorecido
     */
    public String getDsBancoFavorecido() {
        return dsBancoFavorecido;
    }

    /**
     * Set: dsBancoFavorecido.
     *
     * @param dsBancoFavorecido the ds banco favorecido
     */
    public void setDsBancoFavorecido(String dsBancoFavorecido) {
        this.dsBancoFavorecido = dsBancoFavorecido;
    }

    /**
     * Get: dsAgenciaFavorecido.
     *
     * @return dsAgenciaFavorecido
     */
    public String getDsAgenciaFavorecido() {
        return dsAgenciaFavorecido;
    }

    /**
     * Set: dsAgenciaFavorecido.
     *
     * @param dsAgenciaFavorecido the ds agencia favorecido
     */
    public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
        this.dsAgenciaFavorecido = dsAgenciaFavorecido;
    }

    /**
     * Get: dsContaFavorecido.
     *
     * @return dsContaFavorecido
     */
    public String getDsContaFavorecido() {
        return dsContaFavorecido;
    }

    /**
     * Set: dsContaFavorecido.
     *
     * @param dsContaFavorecido the ds conta favorecido
     */
    public void setDsContaFavorecido(String dsContaFavorecido) {
        this.dsContaFavorecido = dsContaFavorecido;
    }

    /**
     * Get: dsTipoContaFavorecido.
     *
     * @return dsTipoContaFavorecido
     */
    public String getDsTipoContaFavorecido() {
        return dsTipoContaFavorecido;
    }

    /**
     * Set: dsTipoContaFavorecido.
     *
     * @param dsTipoContaFavorecido the ds tipo conta favorecido
     */
    public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
        this.dsTipoContaFavorecido = dsTipoContaFavorecido;
    }

    /**
     * Get: numeroInscricaoBeneficiario.
     *
     * @return numeroInscricaoBeneficiario
     */
    public String getNumeroInscricaoBeneficiario() {
        return numeroInscricaoBeneficiario;
    }

    /**
     * Set: numeroInscricaoBeneficiario.
     *
     * @param numeroInscricaoBeneficiario the numero inscricao beneficiario
     */
    public void setNumeroInscricaoBeneficiario(
        String numeroInscricaoBeneficiario) {
        this.numeroInscricaoBeneficiario = numeroInscricaoBeneficiario;
    }

    /**
     * Get: tipoBeneficiario.
     *
     * @return tipoBeneficiario
     */
    public String getTipoBeneficiario() {
        return tipoBeneficiario;
    }

    /**
     * Set: tipoBeneficiario.
     *
     * @param tipoBeneficiario the tipo beneficiario
     */
    public void setTipoBeneficiario(String tipoBeneficiario) {
        this.tipoBeneficiario = tipoBeneficiario;
    }

    /**
     * Get: nomeBeneficiario.
     *
     * @return nomeBeneficiario
     */
    public String getNomeBeneficiario() {
        return nomeBeneficiario;
    }

    /**
     * Set: nomeBeneficiario.
     *
     * @param nomeBeneficiario the nome beneficiario
     */
    public void setNomeBeneficiario(String nomeBeneficiario) {
        this.nomeBeneficiario = nomeBeneficiario;
    }

    /**
     * Get: dsTipoBeneficiario.
     *
     * @return dsTipoBeneficiario
     */
    public String getDsTipoBeneficiario() {
        return dsTipoBeneficiario;
    }

    /**
     * Set: dsTipoBeneficiario.
     *
     * @param dsTipoBeneficiario the ds tipo beneficiario
     */
    public void setDsTipoBeneficiario(String dsTipoBeneficiario) {
        this.dsTipoBeneficiario = dsTipoBeneficiario;
    }

    /**
     * Get: dtNascimentoBeneficiario.
     *
     * @return dtNascimentoBeneficiario
     */
    public String getDtNascimentoBeneficiario() {
        return dtNascimentoBeneficiario;
    }

    /**
     * Set: dtNascimentoBeneficiario.
     *
     * @param dtNascimentoBeneficiario the dt nascimento beneficiario
     */
    public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
        this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
    }

    /**
     * Get: nrSequenciaArquivoRemessa.
     *
     * @return nrSequenciaArquivoRemessa
     */
    public String getNrSequenciaArquivoRemessa() {
        return nrSequenciaArquivoRemessa;
    }

    /**
     * Set: nrSequenciaArquivoRemessa.
     *
     * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
     */
    public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
        this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
    }

    /**
     * Get: nrLoteArquivoRemessa.
     *
     * @return nrLoteArquivoRemessa
     */
    public String getNrLoteArquivoRemessa() {
        return nrLoteArquivoRemessa;
    }

    /**
     * Set: nrLoteArquivoRemessa.
     *
     * @param nrLoteArquivoRemessa the nr lote arquivo remessa
     */
    public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
        this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
    }

    /**
     * Get: nrLote.
     *
     * @return nrLote
     */
    public String getNrLote() {
        return nrLote;
    }

    /**
     * Set: nrLote.
     *
     * @param nrLote the nr lote
     */
    public void setNrLote(String nrLote) {
        this.nrLote = nrLote;
    }

    /**
     * Get: numeroPagamento.
     *
     * @return numeroPagamento
     */
    public String getNumeroPagamento() {
        return numeroPagamento;
    }

    /**
     * Set: numeroPagamento.
     *
     * @param numeroPagamento the numero pagamento
     */
    public void setNumeroPagamento(String numeroPagamento) {
        this.numeroPagamento = numeroPagamento;
    }

    /**
     * Get: cdListaDebito.
     *
     * @return cdListaDebito
     */
    public String getCdListaDebito() {
        return cdListaDebito;
    }

    /**
     * Set: cdListaDebito.
     *
     * @param cdListaDebito the cd lista debito
     */
    public void setCdListaDebito(String cdListaDebito) {
        this.cdListaDebito = cdListaDebito;
    }

    /**
     * Get: dtAgendamento.
     *
     * @return dtAgendamento
     */
    public String getDtAgendamento() {
        return dtAgendamento;
    }

    /**
     * Set: dtAgendamento.
     *
     * @param dtAgendamento the dt agendamento
     */
    public void setDtAgendamento(String dtAgendamento) {
        this.dtAgendamento = dtAgendamento;
    }

    /**
     * Get: dtPagamento.
     *
     * @return dtPagamento
     */
    public String getDtPagamento() {
        return dtPagamento;
    }

    /**
     * Set: dtPagamento.
     *
     * @param dtPagamento the dt pagamento
     */
    public void setDtPagamento(String dtPagamento) {
        this.dtPagamento = dtPagamento;
    }

    /**
     * Get: dtDevolucaoEstorno.
     *
     * @return dtDevolucaoEstorno
     */
    public String getDtDevolucaoEstorno() {
        return dtDevolucaoEstorno;
    }

    /**
     * Set: dtDevolucaoEstorno.
     *
     * @param dtDevolucaoEstorno the dt devolucao estorno
     */
    public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
        this.dtDevolucaoEstorno = dtDevolucaoEstorno;
    }

    /**
     * Get: dtPagamentoGare.
     *
     * @return dtPagamentoGare
     */
    public Date getDtPagamentoGare() {
        return dtPagamentoGare;
    }

    /**
     * Set: dtPagamentoGare.
     *
     * @param dtPagamentoGare the dt pagamento gare
     */
    public void setDtPagamentoGare(Date dtPagamentoGare) {
        this.dtPagamentoGare = dtPagamentoGare;
    }

    /**
     * Get: dtPagamentoManutencao.
     *
     * @return dtPagamentoManutencao
     */
    public Date getDtPagamentoManutencao() {
        return dtPagamentoManutencao;
    }

    /**
     * Set: dtPagamentoManutencao.
     *
     * @param dtPagamentoManutencao the dt pagamento manutencao
     */
    public void setDtPagamentoManutencao(Date dtPagamentoManutencao) {
        this.dtPagamentoManutencao = dtPagamentoManutencao;
    }

    /**
     * Get: dtVencimento.
     *
     * @return dtVencimento
     */
    public String getDtVencimento() {
        return dtVencimento;
    }

    /**
     * Set: dtVencimento.
     *
     * @param dtVencimento the dt vencimento
     */
    public void setDtVencimento(String dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    /**
     * Get: cdIndicadorEconomicoMoeda.
     *
     * @return cdIndicadorEconomicoMoeda
     */
    public String getCdIndicadorEconomicoMoeda() {
        return cdIndicadorEconomicoMoeda;
    }

    /**
     * Set: cdIndicadorEconomicoMoeda.
     *
     * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
     */
    public void setCdIndicadorEconomicoMoeda(String cdIndicadorEconomicoMoeda) {
        this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
    }

    /**
     * Get: qtMoeda.
     *
     * @return qtMoeda
     */
    public BigDecimal getQtMoeda() {
        return qtMoeda;
    }

    /**
     * Set: qtMoeda.
     *
     * @param qtMoeda the qt moeda
     */
    public void setQtMoeda(BigDecimal qtMoeda) {
        this.qtMoeda = qtMoeda;
    }

    /**
     * Get: vlrAgendamento.
     *
     * @return vlrAgendamento
     */
    public BigDecimal getVlrAgendamento() {
        return vlrAgendamento;
    }

    /**
     * Set: vlrAgendamento.
     *
     * @param vlrAgendamento the vlr agendamento
     */
    public void setVlrAgendamento(BigDecimal vlrAgendamento) {
        this.vlrAgendamento = vlrAgendamento;
    }

    /**
     * Get: vlrEfetivacao.
     *
     * @return vlrEfetivacao
     */
    public BigDecimal getVlrEfetivacao() {
        return vlrEfetivacao;
    }

    /**
     * Set: vlrEfetivacao.
     *
     * @param vlrEfetivacao the vlr efetivacao
     */
    public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
        this.vlrEfetivacao = vlrEfetivacao;
    }

    /**
     * Get: cdSituacaoOperacaoPagamento.
     *
     * @return cdSituacaoOperacaoPagamento
     */
    public String getCdSituacaoOperacaoPagamento() {
        return cdSituacaoOperacaoPagamento;
    }

    /**
     * Set: cdSituacaoOperacaoPagamento.
     *
     * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
     */
    public void setCdSituacaoOperacaoPagamento(
        String cdSituacaoOperacaoPagamento) {
        this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
    }

    /**
     * Get: dsMotivoSituacao.
     *
     * @return dsMotivoSituacao
     */
    public String getDsMotivoSituacao() {
        return dsMotivoSituacao;
    }

    /**
     * Set: dsMotivoSituacao.
     *
     * @param dsMotivoSituacao the ds motivo situacao
     */
    public void setDsMotivoSituacao(String dsMotivoSituacao) {
        this.dsMotivoSituacao = dsMotivoSituacao;
    }

    /**
     * Get: dsPagamento.
     *
     * @return dsPagamento
     */
    public String getDsPagamento() {
        return dsPagamento;
    }

    /**
     * Set: dsPagamento.
     *
     * @param dsPagamento the ds pagamento
     */
    public void setDsPagamento(String dsPagamento) {
        this.dsPagamento = dsPagamento;
    }

    /**
     * Get: nrDocumento.
     *
     * @return nrDocumento
     */
    public String getNrDocumento() {
        return nrDocumento;
    }

    /**
     * Set: nrDocumento.
     *
     * @param nrDocumento the nr documento
     */
    public void setNrDocumento(String nrDocumento) {
        this.nrDocumento = nrDocumento;
    }

    /**
     * Get: tipoDocumento.
     *
     * @return tipoDocumento
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Set: tipoDocumento.
     *
     * @param tipoDocumento the tipo documento
     */
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    /**
     * Get: cdSerieDocumento.
     *
     * @return cdSerieDocumento
     */
    public String getCdSerieDocumento() {
        return cdSerieDocumento;
    }

    /**
     * Set: cdSerieDocumento.
     *
     * @param cdSerieDocumento the cd serie documento
     */
    public void setCdSerieDocumento(String cdSerieDocumento) {
        this.cdSerieDocumento = cdSerieDocumento;
    }

    /**
     * Get: vlDocumento.
     *
     * @return vlDocumento
     */
    public BigDecimal getVlDocumento() {
        return vlDocumento;
    }

    /**
     * Set: vlDocumento.
     *
     * @param vlDocumento the vl documento
     */
    public void setVlDocumento(BigDecimal vlDocumento) {
        this.vlDocumento = vlDocumento;
    }

    /**
     * Get: dtEmissaoDocumento.
     *
     * @return dtEmissaoDocumento
     */
    public String getDtEmissaoDocumento() {
        return dtEmissaoDocumento;
    }

    /**
     * Set: dtEmissaoDocumento.
     *
     * @param dtEmissaoDocumento the dt emissao documento
     */
    public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
        this.dtEmissaoDocumento = dtEmissaoDocumento;
    }

    /**
     * Get: dsUsoEmpresa.
     *
     * @return dsUsoEmpresa
     */
    public String getDsUsoEmpresa() {
        return dsUsoEmpresa;
    }

    /**
     * Set: dsUsoEmpresa.
     *
     * @param dsUsoEmpresa the ds uso empresa
     */
    public void setDsUsoEmpresa(String dsUsoEmpresa) {
        this.dsUsoEmpresa = dsUsoEmpresa;
    }

    /**
     * Get: dsMensagemPrimeiraLinha.
     *
     * @return dsMensagemPrimeiraLinha
     */
    public String getDsMensagemPrimeiraLinha() {
        return dsMensagemPrimeiraLinha;
    }

    /**
     * Set: dsMensagemPrimeiraLinha.
     *
     * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
     */
    public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
        this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
    }

    /**
     * Get: dsMensagemSegundaLinha.
     *
     * @return dsMensagemSegundaLinha
     */
    public String getDsMensagemSegundaLinha() {
        return dsMensagemSegundaLinha;
    }

    /**
     * Set: dsMensagemSegundaLinha.
     *
     * @param dsMensagemSegundaLinha the ds mensagem segunda linha
     */
    public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
        this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
    }

    /**
     * Get: dddContribuinte.
     *
     * @return dddContribuinte
     */
    public String getDddContribuinte() {
        return dddContribuinte;
    }

    /**
     * Set: dddContribuinte.
     *
     * @param dddContribuinte the ddd contribuinte
     */
    public void setDddContribuinte(String dddContribuinte) {
        this.dddContribuinte = dddContribuinte;
    }

    /**
     * Get: telefoneContribuinte.
     *
     * @return telefoneContribuinte
     */
    public String getTelefoneContribuinte() {
        return telefoneContribuinte;
    }

    /**
     * Set: telefoneContribuinte.
     *
     * @param telefoneContribuinte the telefone contribuinte
     */
    public void setTelefoneContribuinte(String telefoneContribuinte) {
        this.telefoneContribuinte = telefoneContribuinte;
    }

    /**
     * Get: inscricaoEstadualContribuinte.
     *
     * @return inscricaoEstadualContribuinte
     */
    public String getInscricaoEstadualContribuinte() {
        return inscricaoEstadualContribuinte;
    }

    /**
     * Set: inscricaoEstadualContribuinte.
     *
     * @param inscricaoEstadualContribuinte the inscricao estadual contribuinte
     */
    public void setInscricaoEstadualContribuinte(
        String inscricaoEstadualContribuinte) {
        this.inscricaoEstadualContribuinte = inscricaoEstadualContribuinte;
    }

    /**
     * Get: codigoBarras.
     *
     * @return codigoBarras
     */
    public String getCodigoBarras() {
        return codigoBarras;
    }

    /**
     * Set: codigoBarras.
     *
     * @param codigoBarras the codigo barras
     */
    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    /**
     * Get: codigoReceita.
     *
     * @return codigoReceita
     */
    public String getCodigoReceita() {
        return codigoReceita;
    }

    /**
     * Set: codigoReceita.
     *
     * @param codigoReceita the codigo receita
     */
    public void setCodigoReceita(String codigoReceita) {
        this.codigoReceita = codigoReceita;
    }

    /**
     * Get: agenteArrecadador.
     *
     * @return agenteArrecadador
     */
    public String getAgenteArrecadador() {
        return agenteArrecadador;
    }

    /**
     * Set: agenteArrecadador.
     *
     * @param agenteArrecadador the agente arrecadador
     */
    public void setAgenteArrecadador(String agenteArrecadador) {
        this.agenteArrecadador = agenteArrecadador;
    }

    /**
     * Get: codigoTributo.
     *
     * @return codigoTributo
     */
    public String getCodigoTributo() {
        return codigoTributo;
    }

    /**
     * Set: codigoTributo.
     *
     * @param codigoTributo the codigo tributo
     */
    public void setCodigoTributo(String codigoTributo) {
        this.codigoTributo = codigoTributo;
    }

    /**
     * Get: numeroReferencia.
     *
     * @return numeroReferencia
     */
    public String getNumeroReferencia() {
        return numeroReferencia;
    }

    /**
     * Set: numeroReferencia.
     *
     * @param numeroReferencia the numero referencia
     */
    public void setNumeroReferencia(String numeroReferencia) {
        this.numeroReferencia = numeroReferencia;
    }

    /**
     * Get: numeroCotaParcela.
     *
     * @return numeroCotaParcela
     */
    public String getNumeroCotaParcela() {
        return numeroCotaParcela;
    }

    /**
     * Set: numeroCotaParcela.
     *
     * @param numeroCotaParcela the numero cota parcela
     */
    public void setNumeroCotaParcela(String numeroCotaParcela) {
        this.numeroCotaParcela = numeroCotaParcela;
    }

    /**
     * Get: percentualReceita.
     *
     * @return percentualReceita
     */
    public BigDecimal getPercentualReceita() {
        return percentualReceita;
    }

    /**
     * Set: percentualReceita.
     *
     * @param percentualReceita the percentual receita
     */
    public void setPercentualReceita(BigDecimal percentualReceita) {
        this.percentualReceita = percentualReceita;
    }

    /**
     * Get: periodoApuracao.
     *
     * @return periodoApuracao
     */
    public String getPeriodoApuracao() {
        return periodoApuracao;
    }

    /**
     * Set: periodoApuracao.
     *
     * @param periodoApuracao the periodo apuracao
     */
    public void setPeriodoApuracao(String periodoApuracao) {
        this.periodoApuracao = periodoApuracao;
    }

    /**
     * Get: anoExercicio.
     *
     * @return anoExercicio
     */
    public String getAnoExercicio() {
        return anoExercicio;
    }

    /**
     * Set: anoExercicio.
     *
     * @param anoExercicio the ano exercicio
     */
    public void setAnoExercicio(String anoExercicio) {
        this.anoExercicio = anoExercicio;
    }

    /**
     * Get: dataReferencia.
     *
     * @return dataReferencia
     */
    public String getDataReferencia() {
        return dataReferencia;
    }

    /**
     * Set: dataReferencia.
     *
     * @param dataReferencia the data referencia
     */
    public void setDataReferencia(String dataReferencia) {
        this.dataReferencia = dataReferencia;
    }

    /**
     * Get: dataCompetencia.
     *
     * @return dataCompetencia
     */
    public String getDataCompetencia() {
        return dataCompetencia;
    }

    /**
     * Set: dataCompetencia.
     *
     * @param dataCompetencia the data competencia
     */
    public void setDataCompetencia(String dataCompetencia) {
        this.dataCompetencia = dataCompetencia;
    }

    /**
     * Get: codigoCnae.
     *
     * @return codigoCnae
     */
    public String getCodigoCnae() {
        return codigoCnae;
    }

    /**
     * Set: codigoCnae.
     *
     * @param codigoCnae the codigo cnae
     */
    public void setCodigoCnae(String codigoCnae) {
        this.codigoCnae = codigoCnae;
    }

    /**
     * Get: placaVeiculo.
     *
     * @return placaVeiculo
     */
    public String getPlacaVeiculo() {
        return placaVeiculo;
    }

    /**
     * Set: placaVeiculo.
     *
     * @param placaVeiculo the placa veiculo
     */
    public void setPlacaVeiculo(String placaVeiculo) {
        this.placaVeiculo = placaVeiculo;
    }

    /**
     * Get: numeroParcelamento.
     *
     * @return numeroParcelamento
     */
    public String getNumeroParcelamento() {
        return numeroParcelamento;
    }

    /**
     * Set: numeroParcelamento.
     *
     * @param numeroParcelamento the numero parcelamento
     */
    public void setNumeroParcelamento(String numeroParcelamento) {
        this.numeroParcelamento = numeroParcelamento;
    }

    /**
     * Get: codigoOrgaoFavorecido.
     *
     * @return codigoOrgaoFavorecido
     */
    public String getCodigoOrgaoFavorecido() {
        return codigoOrgaoFavorecido;
    }

    /**
     * Set: codigoOrgaoFavorecido.
     *
     * @param codigoOrgaoFavorecido the codigo orgao favorecido
     */
    public void setCodigoOrgaoFavorecido(String codigoOrgaoFavorecido) {
        this.codigoOrgaoFavorecido = codigoOrgaoFavorecido;
    }

    /**
     * Get: nomeOrgaoFavorecido.
     *
     * @return nomeOrgaoFavorecido
     */
    public String getNomeOrgaoFavorecido() {
        return nomeOrgaoFavorecido;
    }

    /**
     * Set: nomeOrgaoFavorecido.
     *
     * @param nomeOrgaoFavorecido the nome orgao favorecido
     */
    public void setNomeOrgaoFavorecido(String nomeOrgaoFavorecido) {
        this.nomeOrgaoFavorecido = nomeOrgaoFavorecido;
    }

    /**
     * Get: codigoUfFavorecida.
     *
     * @return codigoUfFavorecida
     */
    public String getCodigoUfFavorecida() {
        return codigoUfFavorecida;
    }

    /**
     * Set: codigoUfFavorecida.
     *
     * @param codigoUfFavorecida the codigo uf favorecida
     */
    public void setCodigoUfFavorecida(String codigoUfFavorecida) {
        this.codigoUfFavorecida = codigoUfFavorecida;
    }

    /**
     * Get: descricaoUfFavorecida.
     *
     * @return descricaoUfFavorecida
     */
    public String getDescricaoUfFavorecida() {
        return descricaoUfFavorecida;
    }

    /**
     * Set: descricaoUfFavorecida.
     *
     * @param descricaoUfFavorecida the descricao uf favorecida
     */
    public void setDescricaoUfFavorecida(String descricaoUfFavorecida) {
        this.descricaoUfFavorecida = descricaoUfFavorecida;
    }

    /**
     * Get: vlReceita.
     *
     * @return vlReceita
     */
    public BigDecimal getVlReceita() {
        return vlReceita;
    }

    /**
     * Set: vlReceita.
     *
     * @param vlReceita the vl receita
     */
    public void setVlReceita(BigDecimal vlReceita) {
        this.vlReceita = vlReceita;
    }

    /**
     * Get: vlPrincipal.
     *
     * @return vlPrincipal
     */
    public BigDecimal getVlPrincipal() {
        return vlPrincipal;
    }

    /**
     * Set: vlPrincipal.
     *
     * @param vlPrincipal the vl principal
     */
    public void setVlPrincipal(BigDecimal vlPrincipal) {
        this.vlPrincipal = vlPrincipal;
    }

    /**
     * Get: vlAtualizacaoMonetaria.
     *
     * @return vlAtualizacaoMonetaria
     */
    public BigDecimal getVlAtualizacaoMonetaria() {
        return vlAtualizacaoMonetaria;
    }

    /**
     * Set: vlAtualizacaoMonetaria.
     *
     * @param vlAtualizacaoMonetaria the vl atualizacao monetaria
     */
    public void setVlAtualizacaoMonetaria(BigDecimal vlAtualizacaoMonetaria) {
        this.vlAtualizacaoMonetaria = vlAtualizacaoMonetaria;
    }

    /**
     * Get: vlAcrescimoFinanceiro.
     *
     * @return vlAcrescimoFinanceiro
     */
    public BigDecimal getVlAcrescimoFinanceiro() {
        return vlAcrescimoFinanceiro;
    }

    /**
     * Set: vlAcrescimoFinanceiro.
     *
     * @param vlAcrescimoFinanceiro the vl acrescimo financeiro
     */
    public void setVlAcrescimoFinanceiro(BigDecimal vlAcrescimoFinanceiro) {
        this.vlAcrescimoFinanceiro = vlAcrescimoFinanceiro;
    }

    /**
     * Get: vlHonorarioAdvogado.
     *
     * @return vlHonorarioAdvogado
     */
    public BigDecimal getVlHonorarioAdvogado() {
        return vlHonorarioAdvogado;
    }

    /**
     * Set: vlHonorarioAdvogado.
     *
     * @param vlHonorarioAdvogado the vl honorario advogado
     */
    public void setVlHonorarioAdvogado(BigDecimal vlHonorarioAdvogado) {
        this.vlHonorarioAdvogado = vlHonorarioAdvogado;
    }

    /**
     * Get: vlDesconto.
     *
     * @return vlDesconto
     */
    public BigDecimal getVlDesconto() {
        return vlDesconto;
    }

    /**
     * Set: vlDesconto.
     *
     * @param vlDesconto the vl desconto
     */
    public void setVlDesconto(BigDecimal vlDesconto) {
        this.vlDesconto = vlDesconto;
    }

    /**
     * Get: vlAbatimento.
     *
     * @return vlAbatimento
     */
    public BigDecimal getVlAbatimento() {
        return vlAbatimento;
    }

    /**
     * Set: vlAbatimento.
     *
     * @param vlAbatimento the vl abatimento
     */
    public void setVlAbatimento(BigDecimal vlAbatimento) {
        this.vlAbatimento = vlAbatimento;
    }

    /**
     * Get: vlMulta.
     *
     * @return vlMulta
     */
    public BigDecimal getVlMulta() {
        return vlMulta;
    }

    /**
     * Set: vlMulta.
     *
     * @param vlMulta the vl multa
     */
    public void setVlMulta(BigDecimal vlMulta) {
        this.vlMulta = vlMulta;
    }

    /**
     * Get: vlMora.
     *
     * @return vlMora
     */
    public BigDecimal getVlMora() {
        return vlMora;
    }

    /**
     * Set: vlMora.
     *
     * @param vlMora the vl mora
     */
    public void setVlMora(BigDecimal vlMora) {
        this.vlMora = vlMora;
    }

    /**
     * Get: vlTotal.
     *
     * @return vlTotal
     */
    public BigDecimal getVlTotal() {
        return vlTotal;
    }

    /**
     * Set: vlTotal.
     *
     * @param vlTotal the vl total
     */
    public void setVlTotal(BigDecimal vlTotal) {
        this.vlTotal = vlTotal;
    }

    /**
     * Get: observacaoTributo.
     *
     * @return observacaoTributo
     */
    public String getObservacaoTributo() {
        return observacaoTributo;
    }

    /**
     * Set: observacaoTributo.
     *
     * @param observacaoTributo the observacao tributo
     */
    public void setObservacaoTributo(String observacaoTributo) {
        this.observacaoTributo = observacaoTributo;
    }

    /**
     * Get: cdBancoCredito.
     *
     * @return cdBancoCredito
     */
    public String getCdBancoCredito() {
        return cdBancoCredito;
    }

    /**
     * Set: cdBancoCredito.
     *
     * @param cdBancoCredito the cd banco credito
     */
    public void setCdBancoCredito(String cdBancoCredito) {
        this.cdBancoCredito = cdBancoCredito;
    }

    /**
     * Get: agenciaDigitoCredito.
     *
     * @return agenciaDigitoCredito
     */
    public String getAgenciaDigitoCredito() {
        return agenciaDigitoCredito;
    }

    /**
     * Set: agenciaDigitoCredito.
     *
     * @param agenciaDigitoCredito the agencia digito credito
     */
    public void setAgenciaDigitoCredito(String agenciaDigitoCredito) {
        this.agenciaDigitoCredito = agenciaDigitoCredito;
    }

    /**
     * Get: contaDigitoCredito.
     *
     * @return contaDigitoCredito
     */
    public String getContaDigitoCredito() {
        return contaDigitoCredito;
    }

    /**
     * Set: contaDigitoCredito.
     *
     * @param contaDigitoCredito the conta digito credito
     */
    public void setContaDigitoCredito(String contaDigitoCredito) {
        this.contaDigitoCredito = contaDigitoCredito;
    }

    /**
     * Get: tipoContaCredito.
     *
     * @return tipoContaCredito
     */
    public String getTipoContaCredito() {
        return tipoContaCredito;
    }

    /**
     * Set: tipoContaCredito.
     *
     * @param tipoContaCredito the tipo conta credito
     */
    public void setTipoContaCredito(String tipoContaCredito) {
        this.tipoContaCredito = tipoContaCredito;
    }

    /**
     * Get: cdBancoDestino.
     *
     * @return cdBancoDestino
     */
    public String getCdBancoDestino() {
        return cdBancoDestino;
    }

    /**
     * Set: cdBancoDestino.
     *
     * @param cdBancoDestino the cd banco destino
     */
    public void setCdBancoDestino(String cdBancoDestino) {
        this.cdBancoDestino = cdBancoDestino;
    }

    /**
     * Get: agenciaDigitoDestino.
     *
     * @return agenciaDigitoDestino
     */
    public String getAgenciaDigitoDestino() {
        return agenciaDigitoDestino;
    }

    /**
     * Set: agenciaDigitoDestino.
     *
     * @param agenciaDigitoDestino the agencia digito destino
     */
    public void setAgenciaDigitoDestino(String agenciaDigitoDestino) {
        this.agenciaDigitoDestino = agenciaDigitoDestino;
    }

    /**
     * Get: contaDigitoDestino.
     *
     * @return contaDigitoDestino
     */
    public String getContaDigitoDestino() {
        return contaDigitoDestino;
    }

    /**
     * Set: contaDigitoDestino.
     *
     * @param contaDigitoDestino the conta digito destino
     */
    public void setContaDigitoDestino(String contaDigitoDestino) {
        this.contaDigitoDestino = contaDigitoDestino;
    }

    /**
     * Get: tipoContaDestino.
     *
     * @return tipoContaDestino
     */
    public String getTipoContaDestino() {
        return tipoContaDestino;
    }

    /**
     * Set: tipoContaDestino.
     *
     * @param tipoContaDestino the tipo conta destino
     */
    public void setTipoContaDestino(String tipoContaDestino) {
        this.tipoContaDestino = tipoContaDestino;
    }

    /**
     * Get: vlDeducao.
     *
     * @return vlDeducao
     */
    public BigDecimal getVlDeducao() {
        return vlDeducao;
    }

    /**
     * Set: vlDeducao.
     *
     * @param vlDeducao the vl deducao
     */
    public void setVlDeducao(BigDecimal vlDeducao) {
        this.vlDeducao = vlDeducao;
    }

    /**
     * Get: vlAcrescimo.
     *
     * @return vlAcrescimo
     */
    public BigDecimal getVlAcrescimo() {
        return vlAcrescimo;
    }

    /**
     * Set: vlAcrescimo.
     *
     * @param vlAcrescimo the vl acrescimo
     */
    public void setVlAcrescimo(BigDecimal vlAcrescimo) {
        this.vlAcrescimo = vlAcrescimo;
    }

    /**
     * Get: vlImpostoRenda.
     *
     * @return vlImpostoRenda
     */
    public BigDecimal getVlImpostoRenda() {
        return vlImpostoRenda;
    }

    /**
     * Set: vlImpostoRenda.
     *
     * @param vlImpostoRenda the vl imposto renda
     */
    public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
        this.vlImpostoRenda = vlImpostoRenda;
    }

    /**
     * Get: vlIss.
     *
     * @return vlIss
     */
    public BigDecimal getVlIss() {
        return vlIss;
    }

    /**
     * Set: vlIss.
     *
     * @param vlIss the vl iss
     */
    public void setVlIss(BigDecimal vlIss) {
        this.vlIss = vlIss;
    }

    /**
     * Get: vlInss.
     *
     * @return vlInss
     */
    public BigDecimal getVlInss() {
        return vlInss;
    }

    /**
     * Set: vlInss.
     *
     * @param vlInss the vl inss
     */
    public void setVlInss(BigDecimal vlInss) {
        this.vlInss = vlInss;
    }

    /**
     * Get: vlIof.
     *
     * @return vlIof
     */
    public BigDecimal getVlIof() {
        return vlIof;
    }

    /**
     * Set: vlIof.
     *
     * @param vlIof the vl iof
     */
    public void setVlIof(BigDecimal vlIof) {
        this.vlIof = vlIof;
    }

    /**
     * Get: codigoRenavam.
     *
     * @return codigoRenavam
     */
    public String getCodigoRenavam() {
        return codigoRenavam;
    }

    /**
     * Set: codigoRenavam.
     *
     * @param codigoRenavam the codigo renavam
     */
    public void setCodigoRenavam(String codigoRenavam) {
        this.codigoRenavam = codigoRenavam;
    }

    /**
     * Get: municipioTributo.
     *
     * @return municipioTributo
     */
    public String getMunicipioTributo() {
        return municipioTributo;
    }

    /**
     * Set: municipioTributo.
     *
     * @param municipioTributo the municipio tributo
     */
    public void setMunicipioTributo(String municipioTributo) {
        this.municipioTributo = municipioTributo;
    }

    /**
     * Get: ufTributo.
     *
     * @return ufTributo
     */
    public String getUfTributo() {
        return ufTributo;
    }

    /**
     * Set: ufTributo.
     *
     * @param ufTributo the uf tributo
     */
    public void setUfTributo(String ufTributo) {
        this.ufTributo = ufTributo;
    }

    /**
     * Get: numeroNsu.
     *
     * @return numeroNsu
     */
    public String getNumeroNsu() {
        return numeroNsu;
    }

    /**
     * Set: numeroNsu.
     *
     * @param numeroNsu the numero nsu
     */
    public void setNumeroNsu(String numeroNsu) {
        this.numeroNsu = numeroNsu;
    }

    /**
     * Get: opcaoTributo.
     *
     * @return opcaoTributo
     */
    public String getOpcaoTributo() {
        return opcaoTributo;
    }

    /**
     * Set: opcaoTributo.
     *
     * @param opcaoTributo the opcao tributo
     */
    public void setOpcaoTributo(String opcaoTributo) {
        this.opcaoTributo = opcaoTributo;
    }

    /**
     * Get: opcaoRetiradaTributo.
     *
     * @return opcaoRetiradaTributo
     */
    public String getOpcaoRetiradaTributo() {
        return opcaoRetiradaTributo;
    }

    /**
     * Set: opcaoRetiradaTributo.
     *
     * @param opcaoRetiradaTributo the opcao retirada tributo
     */
    public void setOpcaoRetiradaTributo(String opcaoRetiradaTributo) {
        this.opcaoRetiradaTributo = opcaoRetiradaTributo;
    }

    /**
     * Get: clienteDepositoIdentificado.
     *
     * @return clienteDepositoIdentificado
     */
    public String getClienteDepositoIdentificado() {
        return clienteDepositoIdentificado;
    }

    /**
     * Set: clienteDepositoIdentificado.
     *
     * @param clienteDepositoIdentificado the cliente deposito identificado
     */
    public void setClienteDepositoIdentificado(
        String clienteDepositoIdentificado) {
        this.clienteDepositoIdentificado = clienteDepositoIdentificado;
    }

    /**
     * Get: tipoDespositoIdentificado.
     *
     * @return tipoDespositoIdentificado
     */
    public String getTipoDespositoIdentificado() {
        return tipoDespositoIdentificado;
    }

    /**
     * Set: tipoDespositoIdentificado.
     *
     * @param tipoDespositoIdentificado the tipo desposito identificado
     */
    public void setTipoDespositoIdentificado(String tipoDespositoIdentificado) {
        this.tipoDespositoIdentificado = tipoDespositoIdentificado;
    }

    /**
     * Get: produtoDepositoIdentificado.
     *
     * @return produtoDepositoIdentificado
     */
    public String getProdutoDepositoIdentificado() {
        return produtoDepositoIdentificado;
    }

    /**
     * Set: produtoDepositoIdentificado.
     *
     * @param produtoDepositoIdentificado the produto deposito identificado
     */
    public void setProdutoDepositoIdentificado(
        String produtoDepositoIdentificado) {
        this.produtoDepositoIdentificado = produtoDepositoIdentificado;
    }

    /**
     * Get: sequenciaProdutoDepositoIdentificado.
     *
     * @return sequenciaProdutoDepositoIdentificado
     */
    public String getSequenciaProdutoDepositoIdentificado() {
        return sequenciaProdutoDepositoIdentificado;
    }

    /**
     * Set: sequenciaProdutoDepositoIdentificado.
     *
     * @param sequenciaProdutoDepositoIdentificado the sequencia produto deposito identificado
     */
    public void setSequenciaProdutoDepositoIdentificado(
        String sequenciaProdutoDepositoIdentificado) {
        this.sequenciaProdutoDepositoIdentificado = sequenciaProdutoDepositoIdentificado;
    }

    /**
     * Get: bancoCartaoDepositoIdentificado.
     *
     * @return bancoCartaoDepositoIdentificado
     */
    public String getBancoCartaoDepositoIdentificado() {
        return bancoCartaoDepositoIdentificado;
    }

    /**
     * Set: bancoCartaoDepositoIdentificado.
     *
     * @param bancoCartaoDepositoIdentificado the banco cartao deposito identificado
     */
    public void setBancoCartaoDepositoIdentificado(
        String bancoCartaoDepositoIdentificado) {
        this.bancoCartaoDepositoIdentificado = bancoCartaoDepositoIdentificado;
    }

    /**
     * Get: cartaoDepositoIdentificado.
     *
     * @return cartaoDepositoIdentificado
     */
    public String getCartaoDepositoIdentificado() {
        return cartaoDepositoIdentificado;
    }

    /**
     * Set: cartaoDepositoIdentificado.
     *
     * @param cartaoDepositoIdentificado the cartao deposito identificado
     */
    public void setCartaoDepositoIdentificado(String cartaoDepositoIdentificado) {
        this.cartaoDepositoIdentificado = cartaoDepositoIdentificado;
    }

    /**
     * Get: bimCartaoDepositoIdentificado.
     *
     * @return bimCartaoDepositoIdentificado
     */
    public String getBimCartaoDepositoIdentificado() {
        return bimCartaoDepositoIdentificado;
    }

    /**
     * Set: bimCartaoDepositoIdentificado.
     *
     * @param bimCartaoDepositoIdentificado the bim cartao deposito identificado
     */
    public void setBimCartaoDepositoIdentificado(
        String bimCartaoDepositoIdentificado) {
        this.bimCartaoDepositoIdentificado = bimCartaoDepositoIdentificado;
    }

    /**
     * Get: viaCartaoDepositoIdentificado.
     *
     * @return viaCartaoDepositoIdentificado
     */
    public String getViaCartaoDepositoIdentificado() {
        return viaCartaoDepositoIdentificado;
    }

    /**
     * Set: viaCartaoDepositoIdentificado.
     *
     * @param viaCartaoDepositoIdentificado the via cartao deposito identificado
     */
    public void setViaCartaoDepositoIdentificado(
        String viaCartaoDepositoIdentificado) {
        this.viaCartaoDepositoIdentificado = viaCartaoDepositoIdentificado;
    }

    /**
     * Get: codigoDepositante.
     *
     * @return codigoDepositante
     */
    public String getCodigoDepositante() {
        return codigoDepositante;
    }

    /**
     * Set: codigoDepositante.
     *
     * @param codigoDepositante the codigo depositante
     */
    public void setCodigoDepositante(String codigoDepositante) {
        this.codigoDepositante = codigoDepositante;
    }

    /**
     * Get: nomeDepositante.
     *
     * @return nomeDepositante
     */
    public String getNomeDepositante() {
        return nomeDepositante;
    }

    /**
     * Set: nomeDepositante.
     *
     * @param nomeDepositante the nome depositante
     */
    public void setNomeDepositante(String nomeDepositante) {
        this.nomeDepositante = nomeDepositante;
    }

    /**
     * Get: camaraCentralizadora.
     *
     * @return camaraCentralizadora
     */
    public String getCamaraCentralizadora() {
        return camaraCentralizadora;
    }

    /**
     * Set: camaraCentralizadora.
     *
     * @param camaraCentralizadora the camara centralizadora
     */
    public void setCamaraCentralizadora(String camaraCentralizadora) {
        this.camaraCentralizadora = camaraCentralizadora;
    }

    /**
     * Get: finalidadeDoc.
     *
     * @return finalidadeDoc
     */
    public String getFinalidadeDoc() {
        return finalidadeDoc;
    }

    /**
     * Set: finalidadeDoc.
     *
     * @param finalidadeDoc the finalidade doc
     */
    public void setFinalidadeDoc(String finalidadeDoc) {
        this.finalidadeDoc = finalidadeDoc;
    }

    /**
     * Get: identificacaoTransferenciaDoc.
     *
     * @return identificacaoTransferenciaDoc
     */
    public String getIdentificacaoTransferenciaDoc() {
        return identificacaoTransferenciaDoc;
    }

    /**
     * Set: identificacaoTransferenciaDoc.
     *
     * @param identificacaoTransferenciaDoc the identificacao transferencia doc
     */
    public void setIdentificacaoTransferenciaDoc(
        String identificacaoTransferenciaDoc) {
        this.identificacaoTransferenciaDoc = identificacaoTransferenciaDoc;
    }

    /**
     * Get: identificacaoTitularidade.
     *
     * @return identificacaoTitularidade
     */
    public String getIdentificacaoTitularidade() {
        return identificacaoTitularidade;
    }

    /**
     * Set: identificacaoTitularidade.
     *
     * @param identificacaoTitularidade the identificacao titularidade
     */
    public void setIdentificacaoTitularidade(String identificacaoTitularidade) {
        this.identificacaoTitularidade = identificacaoTitularidade;
    }

    /**
     * Get: codigoInss.
     *
     * @return codigoInss
     */
    public String getCodigoInss() {
        return codigoInss;
    }

    /**
     * Set: codigoInss.
     *
     * @param codigoInss the codigo inss
     */
    public void setCodigoInss(String codigoInss) {
        this.codigoInss = codigoInss;
    }

    /**
     * Get: vlOutrasEntidades.
     *
     * @return vlOutrasEntidades
     */
    public BigDecimal getVlOutrasEntidades() {
        return vlOutrasEntidades;
    }

    /**
     * Set: vlOutrasEntidades.
     *
     * @param vlOutrasEntidades the vl outras entidades
     */
    public void setVlOutrasEntidades(BigDecimal vlOutrasEntidades) {
        this.vlOutrasEntidades = vlOutrasEntidades;
    }

    /**
     * Get: codigoPagador.
     *
     * @return codigoPagador
     */
    public String getCodigoPagador() {
        return codigoPagador;
    }

    /**
     * Set: codigoPagador.
     *
     * @param codigoPagador the codigo pagador
     */
    public void setCodigoPagador(String codigoPagador) {
        this.codigoPagador = codigoPagador;
    }

    /**
     * Get: codigoOrdemCredito.
     *
     * @return codigoOrdemCredito
     */
    public String getCodigoOrdemCredito() {
        return codigoOrdemCredito;
    }

    /**
     * Set: codigoOrdemCredito.
     *
     * @param codigoOrdemCredito the codigo ordem credito
     */
    public void setCodigoOrdemCredito(String codigoOrdemCredito) {
        this.codigoOrdemCredito = codigoOrdemCredito;
    }

    /**
     * Get: numeroOp.
     *
     * @return numeroOp
     */
    public String getNumeroOp() {
        return numeroOp;
    }

    /**
     * Set: numeroOp.
     *
     * @param numeroOp the numero op
     */
    public void setNumeroOp(String numeroOp) {
        this.numeroOp = numeroOp;
    }

    /**
     * Get: dataExpiracaoCredito.
     *
     * @return dataExpiracaoCredito
     */
    public String getDataExpiracaoCredito() {
        return dataExpiracaoCredito;
    }

    /**
     * Set: dataExpiracaoCredito.
     *
     * @param dataExpiracaoCredito the data expiracao credito
     */
    public void setDataExpiracaoCredito(String dataExpiracaoCredito) {
        this.dataExpiracaoCredito = dataExpiracaoCredito;
    }

    /**
     * Get: instrucaoPagamento.
     *
     * @return instrucaoPagamento
     */
    public String getInstrucaoPagamento() {
        return instrucaoPagamento;
    }

    /**
     * Set: instrucaoPagamento.
     *
     * @param instrucaoPagamento the instrucao pagamento
     */
    public void setInstrucaoPagamento(String instrucaoPagamento) {
        this.instrucaoPagamento = instrucaoPagamento;
    }

    /**
     * Get: numeroCheque.
     *
     * @return numeroCheque
     */
    public String getNumeroCheque() {
        return numeroCheque;
    }

    /**
     * Set: numeroCheque.
     *
     * @param numeroCheque the numero cheque
     */
    public void setNumeroCheque(String numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    /**
     * Get: serieCheque.
     *
     * @return serieCheque
     */
    public String getSerieCheque() {
        return serieCheque;
    }

    /**
     * Set: serieCheque.
     *
     * @param serieCheque the serie cheque
     */
    public void setSerieCheque(String serieCheque) {
        this.serieCheque = serieCheque;
    }

    /**
     * Get: indicadorAssociadoUnicaAgencia.
     *
     * @return indicadorAssociadoUnicaAgencia
     */
    public String getIndicadorAssociadoUnicaAgencia() {
        return indicadorAssociadoUnicaAgencia;
    }

    /**
     * Set: indicadorAssociadoUnicaAgencia.
     *
     * @param indicadorAssociadoUnicaAgencia the indicador associado unica agencia
     */
    public void setIndicadorAssociadoUnicaAgencia(
        String indicadorAssociadoUnicaAgencia) {
        this.indicadorAssociadoUnicaAgencia = indicadorAssociadoUnicaAgencia;
    }

    /**
     * Get: identificadorTipoRetirada.
     *
     * @return identificadorTipoRetirada
     */
    public String getIdentificadorTipoRetirada() {
        return identificadorTipoRetirada;
    }

    /**
     * Set: identificadorTipoRetirada.
     *
     * @param identificadorTipoRetirada the identificador tipo retirada
     */
    public void setIdentificadorTipoRetirada(String identificadorTipoRetirada) {
        this.identificadorTipoRetirada = identificadorTipoRetirada;
    }

    /**
     * Get: identificadorRetirada.
     *
     * @return identificadorRetirada
     */
    public String getIdentificadorRetirada() {
        return identificadorRetirada;
    }

    /**
     * Set: identificadorRetirada.
     *
     * @param identificadorRetirada the identificador retirada
     */
    public void setIdentificadorRetirada(String identificadorRetirada) {
        this.identificadorRetirada = identificadorRetirada;
    }

    /**
     * Get: dataRetirada.
     *
     * @return dataRetirada
     */
    public String getDataRetirada() {
        return dataRetirada;
    }

    /**
     * Set: dataRetirada.
     *
     * @param dataRetirada the data retirada
     */
    public void setDataRetirada(String dataRetirada) {
        this.dataRetirada = dataRetirada;
    }

    /**
     * Get: vlImpostoMovFinanceira.
     *
     * @return vlImpostoMovFinanceira
     */
    public BigDecimal getVlImpostoMovFinanceira() {
        return vlImpostoMovFinanceira;
    }

    /**
     * Set: vlImpostoMovFinanceira.
     *
     * @param vlImpostoMovFinanceira the vl imposto mov financeira
     */
    public void setVlImpostoMovFinanceira(BigDecimal vlImpostoMovFinanceira) {
        this.vlImpostoMovFinanceira = vlImpostoMovFinanceira;
    }

    /**
     * Get: finalidadeTed.
     *
     * @return finalidadeTed
     */
    public String getFinalidadeTed() {
        return finalidadeTed;
    }

    /**
     * Set: finalidadeTed.
     *
     * @param finalidadeTed the finalidade ted
     */
    public void setFinalidadeTed(String finalidadeTed) {
        this.finalidadeTed = finalidadeTed;
    }

    /**
     * Get: identificacaoTransferenciaTed.
     *
     * @return identificacaoTransferenciaTed
     */
    public String getIdentificacaoTransferenciaTed() {
        return identificacaoTransferenciaTed;
    }

    /**
     * Set: identificacaoTransferenciaTed.
     *
     * @param identificacaoTransferenciaTed the identificacao transferencia ted
     */
    public void setIdentificacaoTransferenciaTed(
        String identificacaoTransferenciaTed) {
        this.identificacaoTransferenciaTed = identificacaoTransferenciaTed;
    }

    /**
     * Get: nossoNumero.
     *
     * @return nossoNumero
     */
    public String getNossoNumero() {
        return nossoNumero;
    }

    /**
     * Set: nossoNumero.
     *
     * @param nossoNumero the nosso numero
     */
    public void setNossoNumero(String nossoNumero) {
        this.nossoNumero = nossoNumero;
    }

    /**
     * Get: dsOrigemPagamento.
     *
     * @return dsOrigemPagamento
     */
    public String getDsOrigemPagamento() {
        return dsOrigemPagamento;
    }

    /**
     * Set: dsOrigemPagamento.
     *
     * @param dsOrigemPagamento the ds origem pagamento
     */
    public void setDsOrigemPagamento(String dsOrigemPagamento) {
        this.dsOrigemPagamento = dsOrigemPagamento;
    }

    /**
     * Get: cdIndentificadorJudicial.
     *
     * @return cdIndentificadorJudicial
     */
    public String getCdIndentificadorJudicial() {
        return cdIndentificadorJudicial;
    }

    /**
     * Set: cdIndentificadorJudicial.
     *
     * @param cdIndentificadorJudicial the cd indentificador judicial
     */
    public void setCdIndentificadorJudicial(String cdIndentificadorJudicial) {
        this.cdIndentificadorJudicial = cdIndentificadorJudicial;
    }

    /**
     * Get: dtLimiteDescontoPagamento.
     *
     * @return dtLimiteDescontoPagamento
     */
    public String getDtLimiteDescontoPagamento() {
        return dtLimiteDescontoPagamento;
    }

    /**
     * Set: dtLimiteDescontoPagamento.
     *
     * @param dtLimiteDescontoPagamento the dt limite desconto pagamento
     */
    public void setDtLimiteDescontoPagamento(String dtLimiteDescontoPagamento) {
        this.dtLimiteDescontoPagamento = dtLimiteDescontoPagamento;
    }

    /**
     * Get: dtLimitePagamento.
     *
     * @return dtLimitePagamento
     */
    public String getDtLimitePagamento() {
        return dtLimitePagamento;
    }

    /**
     * Set: dtLimitePagamento.
     *
     * @param dtLimitePagamento the dt limite pagamento
     */
    public void setDtLimitePagamento(String dtLimitePagamento) {
        this.dtLimitePagamento = dtLimitePagamento;
    }

    /**
     * Get: dsRastreado.
     *
     * @return dsRastreado
     */
    public String getDsRastreado() {
        return dsRastreado;
    }

    /**
     * Set: dsRastreado.
     *
     * @param dsRastreado the ds rastreado
     */
    public void setDsRastreado(String dsRastreado) {
        this.dsRastreado = dsRastreado;
    }

    /**
     * Get: carteira.
     *
     * @return carteira
     */
    public String getCarteira() {
        return carteira;
    }

    /**
     * Set: carteira.
     *
     * @param carteira the carteira
     */
    public void setCarteira(String carteira) {
        this.carteira = carteira;
    }

    /**
     * Get: cdSituacaoTranferenciaAutomatica.
     *
     * @return cdSituacaoTranferenciaAutomatica
     */
    public String getCdSituacaoTranferenciaAutomatica() {
        return cdSituacaoTranferenciaAutomatica;
    }

    /**
     * Set: cdSituacaoTranferenciaAutomatica.
     *
     * @param cdSituacaoTranferenciaAutomatica the cd situacao tranferencia automatica
     */
    public void setCdSituacaoTranferenciaAutomatica(
        String cdSituacaoTranferenciaAutomatica) {
        this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
    }

    /**
     * Get: descTipoFavorecido.
     *
     * @return descTipoFavorecido
     */
    public String getDescTipoFavorecido() {
        return descTipoFavorecido;
    }

    /**
     * Set: descTipoFavorecido.
     *
     * @param descTipoFavorecido the desc tipo favorecido
     */
    public void setDescTipoFavorecido(String descTipoFavorecido) {
        this.descTipoFavorecido = descTipoFavorecido;
    }

    /**
     * Get: dataHoraInclusao.
     *
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
        return dataHoraInclusao;
    }

    /**
     * Set: dataHoraInclusao.
     *
     * @param dataHoraInclusao the data hora inclusao
     */
    public void setDataHoraInclusao(String dataHoraInclusao) {
        this.dataHoraInclusao = dataHoraInclusao;
    }

    /**
     * Get: usuarioInclusao.
     *
     * @return usuarioInclusao
     */
    public String getUsuarioInclusao() {
        return usuarioInclusao;
    }

    /**
     * Set: usuarioInclusao.
     *
     * @param usuarioInclusao the usuario inclusao
     */
    public void setUsuarioInclusao(String usuarioInclusao) {
        this.usuarioInclusao = usuarioInclusao;
    }

    /**
     * Get: tipoCanalInclusao.
     *
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
        return tipoCanalInclusao;
    }

    /**
     * Set: tipoCanalInclusao.
     *
     * @param tipoCanalInclusao the tipo canal inclusao
     */
    public void setTipoCanalInclusao(String tipoCanalInclusao) {
        this.tipoCanalInclusao = tipoCanalInclusao;
    }

    /**
     * Get: complementoInclusao.
     *
     * @return complementoInclusao
     */
    public String getComplementoInclusao() {
        return complementoInclusao;
    }

    /**
     * Set: complementoInclusao.
     *
     * @param complementoInclusao the complemento inclusao
     */
    public void setComplementoInclusao(String complementoInclusao) {
        this.complementoInclusao = complementoInclusao;
    }

    /**
     * Get: dataHoraManutencao.
     *
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
        return dataHoraManutencao;
    }

    /**
     * Set: dataHoraManutencao.
     *
     * @param dataHoraManutencao the data hora manutencao
     */
    public void setDataHoraManutencao(String dataHoraManutencao) {
        this.dataHoraManutencao = dataHoraManutencao;
    }

    /**
     * Get: usuarioManutencao.
     *
     * @return usuarioManutencao
     */
    public String getUsuarioManutencao() {
        return usuarioManutencao;
    }

    /**
     * Set: usuarioManutencao.
     *
     * @param usuarioManutencao the usuario manutencao
     */
    public void setUsuarioManutencao(String usuarioManutencao) {
        this.usuarioManutencao = usuarioManutencao;
    }

    /**
     * Get: tipoCanalManutencao.
     *
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
        return tipoCanalManutencao;
    }

    /**
     * Set: tipoCanalManutencao.
     *
     * @param tipoCanalManutencao the tipo canal manutencao
     */
    public void setTipoCanalManutencao(String tipoCanalManutencao) {
        this.tipoCanalManutencao = tipoCanalManutencao;
    }

    /**
     * Get: complementoManutencao.
     *
     * @return complementoManutencao
     */
    public String getComplementoManutencao() {
        return complementoManutencao;
    }

    /**
     * Set: complementoManutencao.
     *
     * @param complementoManutencao the complemento manutencao
     */
    public void setComplementoManutencao(String complementoManutencao) {
        this.complementoManutencao = complementoManutencao;
    }

    /**
     * Is exibe beneficiario.
     *
     * @return true, if is exibe beneficiario
     */
    public boolean isExibeBeneficiario() {
        return exibeBeneficiario;
    }

    /**
     * Set: exibeBeneficiario.
     *
     * @param exibeBeneficiario the exibe beneficiario
     */
    public void setExibeBeneficiario(boolean exibeBeneficiario) {
        this.exibeBeneficiario = exibeBeneficiario;
    }

    /**
     * Get: listaConsultarManutencao.
     *
     * @return listaConsultarManutencao
     */
    public List<ConsultarManutencaoPagamentoSaidaDTO> getListaConsultarManutencao() {
        return listaConsultarManutencao;
    }

    /**
     * Set: listaConsultarManutencao.
     *
     * @param listaConsultarManutencao the lista consultar manutencao
     */
    public void setListaConsultarManutencao(
        List<ConsultarManutencaoPagamentoSaidaDTO> listaConsultarManutencao) {
        this.listaConsultarManutencao = listaConsultarManutencao;
    }

    /**
     * Get: valorPagamentoFormatado.
     *
     * @return valorPagamentoFormatado
     */
    public String getValorPagamentoFormatado() {
        return valorPagamentoFormatado;
    }

    /**
     * Set: valorPagamentoFormatado.
     *
     * @param valorPagamentoFormatado the valor pagamento formatado
     */
    public void setValorPagamentoFormatado(String valorPagamentoFormatado) {
        this.valorPagamentoFormatado = valorPagamentoFormatado;
    }

    /**
     * Get: canal.
     *
     * @return canal
     */
    public String getCanal() {
        return canal;
    }

    /**
     * Set: canal.
     *
     * @param canal the canal
     */
    public void setCanal(String canal) {
        this.canal = canal;
    }

    /**
     * Get: dsTipoManutencao.
     *
     * @return dsTipoManutencao
     */
    public String getDsTipoManutencao() {
        return dsTipoManutencao;
    }

    /**
     * Set: dsTipoManutencao.
     *
     * @param dsTipoManutencao the ds tipo manutencao
     */
    public void setDsTipoManutencao(String dsTipoManutencao) {
        this.dsTipoManutencao = dsTipoManutencao;
    }

    /**
     * Is manutencao.
     *
     * @return true, if is manutencao
     */
    public boolean isManutencao() {
        return manutencao;
    }

    /**
     * Set: manutencao.
     *
     * @param manutencao the manutencao
     */
    public void setManutencao(boolean manutencao) {
        this.manutencao = manutencao;
    }

    /**
     * Get: situacaoPagamento.
     *
     * @return situacaoPagamento
     */
    public String getSituacaoPagamento() {
        return situacaoPagamento;
    }

    /**
     * Set: situacaoPagamento.
     *
     * @param situacaoPagamento the situacao pagamento
     */
    public void setSituacaoPagamento(String situacaoPagamento) {
        this.situacaoPagamento = situacaoPagamento;
    }

    /**
     * Get: motivoPagamento.
     *
     * @return motivoPagamento
     */
    public String getMotivoPagamento() {
        return motivoPagamento;
    }

    /**
     * Set: motivoPagamento.
     *
     * @param motivoPagamento the motivo pagamento
     */
    public void setMotivoPagamento(String motivoPagamento) {
        this.motivoPagamento = motivoPagamento;
    }

    /**
     * Get: inscricaoFavorecido.
     *
     * @return inscricaoFavorecido
     */
    public String getInscricaoFavorecido() {
        return inscricaoFavorecido;
    }

    /**
     * Set: inscricaoFavorecido.
     *
     * @param inscricaoFavorecido the inscricao favorecido
     */
    public void setInscricaoFavorecido(String inscricaoFavorecido) {
        this.inscricaoFavorecido = inscricaoFavorecido;
    }

    /**
     * Get: cdTipoInscricaoFavorecido.
     *
     * @return cdTipoInscricaoFavorecido
     */
    public String getCdTipoInscricaoFavorecido() {
        return cdTipoInscricaoFavorecido;
    }

    /**
     * Set: cdTipoInscricaoFavorecido.
     *
     * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
     */
    public void setCdTipoInscricaoFavorecido(String cdTipoInscricaoFavorecido) {
        this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
    }

    /**
     * Get: tipoContaFavorecido.
     *
     * @return tipoContaFavorecido
     */
    public String getTipoContaFavorecido() {
        return tipoContaFavorecido;
    }

    /**
     * Set: tipoContaFavorecido.
     *
     * @param tipoContaFavorecido the tipo conta favorecido
     */
    public void setTipoContaFavorecido(String tipoContaFavorecido) {
        this.tipoContaFavorecido = tipoContaFavorecido;
    }

    /**
     * Get: bancoFavorecido.
     *
     * @return bancoFavorecido
     */
    public String getBancoFavorecido() {
        return bancoFavorecido;
    }

    /**
     * Set: bancoFavorecido.
     *
     * @param bancoFavorecido the banco favorecido
     */
    public void setBancoFavorecido(String bancoFavorecido) {
        this.bancoFavorecido = bancoFavorecido;
    }

    /**
     * Get: agenciaFavorecido.
     *
     * @return agenciaFavorecido
     */
    public String getAgenciaFavorecido() {
        return agenciaFavorecido;
    }

    /**
     * Set: agenciaFavorecido.
     *
     * @param agenciaFavorecido the agencia favorecido
     */
    public void setAgenciaFavorecido(String agenciaFavorecido) {
        this.agenciaFavorecido = agenciaFavorecido;
    }

    /**
     * Get: contaFavorecido.
     *
     * @return contaFavorecido
     */
    public String getContaFavorecido() {
        return contaFavorecido;
    }

    /**
     * Set: contaFavorecido.
     *
     * @param contaFavorecido the conta favorecido
     */
    public void setContaFavorecido(String contaFavorecido) {
        this.contaFavorecido = contaFavorecido;
    }

    /**
     * Get: tipoFavorecidoManutencao.
     *
     * @return tipoFavorecidoManutencao
     */
    public Integer getTipoFavorecidoManutencao() {
        return tipoFavorecidoManutencao;
    }

    /**
     * Set: tipoFavorecidoManutencao.
     *
     * @param tipoFavorecidoManutencao the tipo favorecido manutencao
     */
    public void setTipoFavorecidoManutencao(Integer tipoFavorecidoManutencao) {
        this.tipoFavorecidoManutencao = tipoFavorecidoManutencao;
    }

    /**
     * Get: listaManutencaoControle.
     *
     * @return listaManutencaoControle
     */
    public List<SelectItem> getListaManutencaoControle() {
        return listaManutencaoControle;
    }

    /**
     * Set: listaManutencaoControle.
     *
     * @param listaManutencaoControle the lista manutencao controle
     */
    public void setListaManutencaoControle(
        List<SelectItem> listaManutencaoControle) {
        this.listaManutencaoControle = listaManutencaoControle;
    }

    /**
     * Get: itemSelecionadoListaManutencao.
     *
     * @return itemSelecionadoListaManutencao
     */
    public Integer getItemSelecionadoListaManutencao() {
        return itemSelecionadoListaManutencao;
    }

    /**
     * Set: itemSelecionadoListaManutencao.
     *
     * @param itemSelecionadoListaManutencao the item selecionado lista manutencao
     */
    public void setItemSelecionadoListaManutencao(
        Integer itemSelecionadoListaManutencao) {
        this.itemSelecionadoListaManutencao = itemSelecionadoListaManutencao;
    }

    /**
     * Get: listaHistoricoConsulta.
     *
     * @return listaHistoricoConsulta
     */
    public List<ConsultarHistConsultaSaldoSaidaDTO> getListaHistoricoConsulta() {
        return listaHistoricoConsulta;
    }

    /**
     * Set: listaHistoricoConsulta.
     *
     * @param listaHistoricoConsulta the lista historico consulta
     */
    public void setListaHistoricoConsulta(
        List<ConsultarHistConsultaSaldoSaidaDTO> listaHistoricoConsulta) {
        this.listaHistoricoConsulta = listaHistoricoConsulta;
    }

    /**
     * Get: itemSelecionadoListaHistorico.
     *
     * @return itemSelecionadoListaHistorico
     */
    public Integer getItemSelecionadoListaHistorico() {
        return itemSelecionadoListaHistorico;
    }

    /**
     * Set: itemSelecionadoListaHistorico.
     *
     * @param itemSelecionadoListaHistorico the item selecionado lista historico
     */
    public void setItemSelecionadoListaHistorico(
        Integer itemSelecionadoListaHistorico) {
        this.itemSelecionadoListaHistorico = itemSelecionadoListaHistorico;
    }

    /**
     * Get: listaPesquisaHistoricoControle.
     *
     * @return listaPesquisaHistoricoControle
     */
    public List<SelectItem> getListaPesquisaHistoricoControle() {
        return listaPesquisaHistoricoControle;
    }

    /**
     * Set: listaPesquisaHistoricoControle.
     *
     * @param listaPesquisaHistoricoControle the lista pesquisa historico controle
     */
    public void setListaPesquisaHistoricoControle(
        List<SelectItem> listaPesquisaHistoricoControle) {
        this.listaPesquisaHistoricoControle = listaPesquisaHistoricoControle;
    }

    /**
     * Get: dataHoraConsultaSaldo.
     *
     * @return dataHoraConsultaSaldo
     */
    public String getDataHoraConsultaSaldo() {
        return dataHoraConsultaSaldo;
    }

    /**
     * Set: dataHoraConsultaSaldo.
     *
     * @param dataHoraConsultaSaldo the data hora consulta saldo
     */
    public void setDataHoraConsultaSaldo(String dataHoraConsultaSaldo) {
        this.dataHoraConsultaSaldo = dataHoraConsultaSaldo;
    }

    /**
     * Get: valorPagamento.
     *
     * @return valorPagamento
     */
    public BigDecimal getValorPagamento() {
        return valorPagamento;
    }

    /**
     * Set: valorPagamento.
     *
     * @param valorPagamento the valor pagamento
     */
    public void setValorPagamento(BigDecimal valorPagamento) {
        this.valorPagamento = valorPagamento;
    }
    

	/**
     * Get: sinal.
     *
     * @return sinal
     */    
    public String getSinal() {
		return sinal;
	}
    
    /**
     * Set: sinal.
     *
     * @param sinal mais ou menos
     */
	public void setSinal(String sinal) {
		this.sinal = sinal;
	}

	/**
     * Get: saldoMomentoLancamento.
     *
     * @return saldoMomentoLancamento
     */
    public BigDecimal getSaldoMomentoLancamento() {
        return saldoMomentoLancamento;
    }

    /**
     * Set: saldoMomentoLancamento.
     *
     * @param saldoMomentoLancamento the saldo momento lancamento
     */
    public void setSaldoMomentoLancamento(BigDecimal saldoMomentoLancamento) {
        this.saldoMomentoLancamento = saldoMomentoLancamento;
    }

    /**
     * Get: saldoOperacional.
     *
     * @return saldoOperacional
     */
    public BigDecimal getSaldoOperacional() {
        return saldoOperacional;
    }

    /**
     * Set: saldoOperacional.
     *
     * @param saldoOperacional the saldo operacional
     */
    public void setSaldoOperacional(BigDecimal saldoOperacional) {
        this.saldoOperacional = saldoOperacional;
    }

    /**
     * Get: saldoVincSemReserva.
     *
     * @return saldoVincSemReserva
     */
    public BigDecimal getSaldoVincSemReserva() {
        return saldoVincSemReserva;
    }

    /**
     * Set: saldoVincSemReserva.
     *
     * @param saldoVincSemReserva the saldo vinc sem reserva
     */
    public void setSaldoVincSemReserva(BigDecimal saldoVincSemReserva) {
        this.saldoVincSemReserva = saldoVincSemReserva;
    }

    /**
     * Get: saldoVincComReserva.
     *
     * @return saldoVincComReserva
     */
    public BigDecimal getSaldoVincComReserva() {
        return saldoVincComReserva;
    }

    /**
     * Set: saldoVincComReserva.
     *
     * @param saldoVincComReserva the saldo vinc com reserva
     */
    public void setSaldoVincComReserva(BigDecimal saldoVincComReserva) {
        this.saldoVincComReserva = saldoVincComReserva;
    }

    /**
     * Get: saldoVincJudicial.
     *
     * @return saldoVincJudicial
     */
    public BigDecimal getSaldoVincJudicial() {
        return saldoVincJudicial;
    }

    /**
     * Set: saldoVincJudicial.
     *
     * @param saldoVincJudicial the saldo vinc judicial
     */
    public void setSaldoVincJudicial(BigDecimal saldoVincJudicial) {
        this.saldoVincJudicial = saldoVincJudicial;
    }

    /**
     * Get: saldoVincAdm.
     *
     * @return saldoVincAdm
     */
    public BigDecimal getSaldoVincAdm() {
        return saldoVincAdm;
    }

    /**
     * Set: saldoVincAdm.
     *
     * @param saldoVincAdm the saldo vinc adm
     */
    public void setSaldoVincAdm(BigDecimal saldoVincAdm) {
        this.saldoVincAdm = saldoVincAdm;
    }

    /**
     * Get: saldoVincSeguranca.
     *
     * @return saldoVincSeguranca
     */
    public BigDecimal getSaldoVincSeguranca() {
        return saldoVincSeguranca;
    }

    /**
     * Set: saldoVincSeguranca.
     *
     * @param saldoVincSeguranca the saldo vinc seguranca
     */
    public void setSaldoVincSeguranca(BigDecimal saldoVincSeguranca) {
        this.saldoVincSeguranca = saldoVincSeguranca;
    }

    /**
     * Get: saldoVincAdmLp.
     *
     * @return saldoVincAdmLp
     */
    public BigDecimal getSaldoVincAdmLp() {
        return saldoVincAdmLp;
    }

    /**
     * Set: saldoVincAdmLp.
     *
     * @param saldoVincAdmLp the saldo vinc adm lp
     */
    public void setSaldoVincAdmLp(BigDecimal saldoVincAdmLp) {
        this.saldoVincAdmLp = saldoVincAdmLp;
    }

    /**
     * Get: saldoAgregadoFundo.
     *
     * @return saldoAgregadoFundo
     */
    public BigDecimal getSaldoAgregadoFundo() {
        return saldoAgregadoFundo;
    }

    /**
     * Set: saldoAgregadoFundo.
     *
     * @param saldoAgregadoFundo the saldo agregado fundo
     */
    public void setSaldoAgregadoFundo(BigDecimal saldoAgregadoFundo) {
        this.saldoAgregadoFundo = saldoAgregadoFundo;
    }

    /**
     * Get: saldoAgregadoCDB.
     *
     * @return saldoAgregadoCDB
     */
    public BigDecimal getSaldoAgregadoCDB() {
        return saldoAgregadoCDB;
    }

    /**
     * Set: saldoAgregadoCDB.
     *
     * @param saldoAgregadoCDB the saldo agregado cdb
     */
    public void setSaldoAgregadoCDB(BigDecimal saldoAgregadoCDB) {
        this.saldoAgregadoCDB = saldoAgregadoCDB;
    }

    /**
     * Get: saldoAgregadoPoupanca.
     *
     * @return saldoAgregadoPoupanca
     */
    public BigDecimal getSaldoAgregadoPoupanca() {
        return saldoAgregadoPoupanca;
    }

    /**
     * Set: saldoAgregadoPoupanca.
     *
     * @param saldoAgregadoPoupanca the saldo agregado poupanca
     */
    public void setSaldoAgregadoPoupanca(BigDecimal saldoAgregadoPoupanca) {
        this.saldoAgregadoPoupanca = saldoAgregadoPoupanca;
    }

    /**
     * Get: saldoAgregadoCredito.
     *
     * @return saldoAgregadoCredito
     */
    public BigDecimal getSaldoAgregadoCredito() {
        return saldoAgregadoCredito;
    }

    /**
     * Set: saldoAgregadoCredito.
     *
     * @param saldoAgregadoCredito the saldo agregado credito
     */
    public void setSaldoAgregadoCredito(BigDecimal saldoAgregadoCredito) {
        this.saldoAgregadoCredito = saldoAgregadoCredito;
    }

    /**
     * Get: saldoBonusCPMF.
     *
     * @return saldoBonusCPMF
     */
    public BigDecimal getSaldoBonusCPMF() {
        return saldoBonusCPMF;
    }

    /**
     * Set: saldoBonusCPMF.
     *
     * @param saldoBonusCPMF the saldo bonus cpmf
     */
    public void setSaldoBonusCPMF(BigDecimal saldoBonusCPMF) {
        this.saldoBonusCPMF = saldoBonusCPMF;
    }

    /**
     * Get: cdBancoOriginal.
     *
     * @return cdBancoOriginal
     */
    public Integer getCdBancoOriginal() {
        return cdBancoOriginal;
    }

    /**
     * Set: cdBancoOriginal.
     *
     * @param cdBancoOriginal the cd banco original
     */
    public void setCdBancoOriginal(Integer cdBancoOriginal) {
        this.cdBancoOriginal = cdBancoOriginal;
    }

    /**
     * Get: dsBancoOriginal.
     *
     * @return dsBancoOriginal
     */
    public String getDsBancoOriginal() {
        return dsBancoOriginal;
    }

    /**
     * Set: dsBancoOriginal.
     *
     * @param dsBancoOriginal the ds banco original
     */
    public void setDsBancoOriginal(String dsBancoOriginal) {
        this.dsBancoOriginal = dsBancoOriginal;
    }

    /**
     * Get: cdAgenciaBancariaOriginal.
     *
     * @return cdAgenciaBancariaOriginal
     */
    public Integer getCdAgenciaBancariaOriginal() {
        return cdAgenciaBancariaOriginal;
    }

    /**
     * Set: cdAgenciaBancariaOriginal.
     *
     * @param cdAgenciaBancariaOriginal the cd agencia bancaria original
     */
    public void setCdAgenciaBancariaOriginal(Integer cdAgenciaBancariaOriginal) {
        this.cdAgenciaBancariaOriginal = cdAgenciaBancariaOriginal;
    }

    /**
     * Get: cdDigitoAgenciaOriginal.
     *
     * @return cdDigitoAgenciaOriginal
     */
    public String getCdDigitoAgenciaOriginal() {
        return cdDigitoAgenciaOriginal;
    }

    /**
     * Set: cdDigitoAgenciaOriginal.
     *
     * @param cdDigitoAgenciaOriginal the cd digito agencia original
     */
    public void setCdDigitoAgenciaOriginal(String cdDigitoAgenciaOriginal) {
        this.cdDigitoAgenciaOriginal = cdDigitoAgenciaOriginal;
    }

    /**
     * Get: dsAgenciaOriginal.
     *
     * @return dsAgenciaOriginal
     */
    public String getDsAgenciaOriginal() {
        return dsAgenciaOriginal;
    }

    /**
     * Set: dsAgenciaOriginal.
     *
     * @param dsAgenciaOriginal the ds agencia original
     */
    public void setDsAgenciaOriginal(String dsAgenciaOriginal) {
        this.dsAgenciaOriginal = dsAgenciaOriginal;
    }

    /**
     * Get: cdContaBancariaOriginal.
     *
     * @return cdContaBancariaOriginal
     */
    public Long getCdContaBancariaOriginal() {
        return cdContaBancariaOriginal;
    }

    /**
     * Set: cdContaBancariaOriginal.
     *
     * @param cdContaBancariaOriginal the cd conta bancaria original
     */
    public void setCdContaBancariaOriginal(Long cdContaBancariaOriginal) {
        this.cdContaBancariaOriginal = cdContaBancariaOriginal;
    }

    /**
     * Get: cdDigitoContaOriginal.
     *
     * @return cdDigitoContaOriginal
     */
    public String getCdDigitoContaOriginal() {
        return cdDigitoContaOriginal;
    }

    /**
     * Set: cdDigitoContaOriginal.
     *
     * @param cdDigitoContaOriginal the cd digito conta original
     */
    public void setCdDigitoContaOriginal(String cdDigitoContaOriginal) {
        this.cdDigitoContaOriginal = cdDigitoContaOriginal;
    }

    /**
     * Get: dsTipoContaOriginal.
     *
     * @return dsTipoContaOriginal
     */
    public String getDsTipoContaOriginal() {
        return dsTipoContaOriginal;
    }

    /**
     * Set: dsTipoContaOriginal.
     *
     * @param dsTipoContaOriginal the ds tipo conta original
     */
    public void setDsTipoContaOriginal(String dsTipoContaOriginal) {
        this.dsTipoContaOriginal = dsTipoContaOriginal;
    }

    /**
     * Get: dtFloatingPagamento.
     *
     * @return dtFloatingPagamento
     */
    public String getDtFloatingPagamento() {
        return dtFloatingPagamento;
    }

    /**
     * Set: dtFloatingPagamento.
     *
     * @param dtFloatingPagamento the dt floating pagamento
     */
    public void setDtFloatingPagamento(String dtFloatingPagamento) {
        this.dtFloatingPagamento = dtFloatingPagamento;
    }

    /**
     * Get: dtEfetivFloatPgto.
     *
     * @return dtEfetivFloatPgto
     */
    public String getDtEfetivFloatPgto() {
        return dtEfetivFloatPgto;
    }

    /**
     * Set: dtEfetivFloatPgto.
     *
     * @param dtEfetivFloatPgto the dt efetiv float pgto
     */
    public void setDtEfetivFloatPgto(String dtEfetivFloatPgto) {
        this.dtEfetivFloatPgto = dtEfetivFloatPgto;
    }

    /**
     * Get: vlFloatingPagamento.
     *
     * @return vlFloatingPagamento
     */
    public BigDecimal getVlFloatingPagamento() {
        return vlFloatingPagamento;
    }

    /**
     * Set: vlFloatingPagamento.
     *
     * @param vlFloatingPagamento the vl floating pagamento
     */
    public void setVlFloatingPagamento(BigDecimal vlFloatingPagamento) {
        this.vlFloatingPagamento = vlFloatingPagamento;
    }

    /**
     * Get: cdOperacaoDcom.
     *
     * @return cdOperacaoDcom
     */
    public Long getCdOperacaoDcom() {
        return cdOperacaoDcom;
    }

    /**
     * Set: cdOperacaoDcom.
     *
     * @param cdOperacaoDcom the cd operacao dcom
     */
    public void setCdOperacaoDcom(Long cdOperacaoDcom) {
        this.cdOperacaoDcom = cdOperacaoDcom;
    }

    /**
     * Get: dsSituacaoDcom.
     *
     * @return dsSituacaoDcom
     */
    public String getDsSituacaoDcom() {
        return dsSituacaoDcom;
    }

    /**
     * Set: dsSituacaoDcom.
     *
     * @param dsSituacaoDcom the ds situacao dcom
     */
    public void setDsSituacaoDcom(String dsSituacaoDcom) {
        this.dsSituacaoDcom = dsSituacaoDcom;
    }

    /**
     * Get: vlBonfcao.
     *
     * @return vlBonfcao
     */
    public BigDecimal getVlBonfcao() {
        return vlBonfcao;
    }

    /**
     * Set: vlBonfcao.
     *
     * @param vlBonfcao the vl bonfcao
     */
    public void setVlBonfcao(BigDecimal vlBonfcao) {
        this.vlBonfcao = vlBonfcao;
    }

    /**
     * Get: vlBonificacao.
     *
     * @return vlBonificacao
     */
    public BigDecimal getVlBonificacao() {
        return vlBonificacao;
    }

    /**
     * Set: vlBonificacao.
     *
     * @param vlBonificacao the vl bonificacao
     */
    public void setVlBonificacao(BigDecimal vlBonificacao) {
        this.vlBonificacao = vlBonificacao;
    }

    /**
     * Get: cdFuncEmissaoOp.
     *
     * @return cdFuncEmissaoOp
     */
    public String getCdFuncEmissaoOp() {
        return cdFuncEmissaoOp;
    }

    /**
     * Set: cdFuncEmissaoOp.
     *
     * @param cdFuncEmissaoOp the cd func emissao op
     */
    public void setCdFuncEmissaoOp(String cdFuncEmissaoOp) {
        this.cdFuncEmissaoOp = cdFuncEmissaoOp;
    }

    /**
     * Get: cdTerminalCaixaEmissaoOp.
     *
     * @return cdTerminalCaixaEmissaoOp
     */
    public String getCdTerminalCaixaEmissaoOp() {
        return cdTerminalCaixaEmissaoOp;
    }

    /**
     * Set: cdTerminalCaixaEmissaoOp.
     *
     * @param cdTerminalCaixaEmissaoOp the cd terminal caixa emissao op
     */
    public void setCdTerminalCaixaEmissaoOp(String cdTerminalCaixaEmissaoOp) {
        this.cdTerminalCaixaEmissaoOp = cdTerminalCaixaEmissaoOp;
    }

    /**
     * Get: dtEmissaoOp.
     *
     * @return dtEmissaoOp
     */
    public String getDtEmissaoOp() {
        return dtEmissaoOp;
    }

    /**
     * Set: dtEmissaoOp.
     *
     * @param dtEmissaoOp the dt emissao op
     */
    public void setDtEmissaoOp(String dtEmissaoOp) {
        this.dtEmissaoOp = dtEmissaoOp;
    }

    /**
     * Get: hrEmissaoOp.
     *
     * @return hrEmissaoOp
     */
    public String getHrEmissaoOp() {
        return hrEmissaoOp;
    }

    /**
     * Set: hrEmissaoOp.
     *
     * @param hrEmissaoOp the hr emissao op
     */
    public void setHrEmissaoOp(String hrEmissaoOp) {
        this.hrEmissaoOp = hrEmissaoOp;
    }

    /**
     * Get: exibeDcom.
     *
     * @return exibeDcom
     */
    public Boolean getExibeDcom() {
        return exibeDcom;
    }

    /**
     * Set: exibeDcom.
     *
     * @param exibeDcom the exibe dcom
     */
    public void setExibeDcom(Boolean exibeDcom) {
        this.exibeDcom = exibeDcom;
    }

    /**
     * Get: dsIndicadorAutorizacao.
     *
     * @return dsIndicadorAutorizacao
     */
    public String getDsIndicadorAutorizacao() {
        return dsIndicadorAutorizacao;
    }

    /**
     * Set: dsIndicadorAutorizacao.
     *
     * @param dsIndicadorAutorizacao the ds indicador autorizacao
     */
    public void setDsIndicadorAutorizacao(String dsIndicadorAutorizacao) {
        this.dsIndicadorAutorizacao = dsIndicadorAutorizacao;
    }

    /**
     * Get: dsTipoLayout.
     *
     * @return dsTipoLayout
     */
    public String getDsTipoLayout() {
        return dsTipoLayout;
    }

    /**
     * Set: dsTipoLayout.
     *
     * @param dsTipoLayout the ds tipo layout
     */
    public void setDsTipoLayout(String dsTipoLayout) {
        this.dsTipoLayout = dsTipoLayout;
    }

    /**
     * Nome: getDetalharPagamentosConsolidadosSaidaDTO.
     *
     * @return detalharPagamentosConsolidadosSaidaDTO
     */
    public DetalharPagamentosConsolidadosSaidaDTO getDetalharPagamentosConsolidadosSaidaDTO() {
        return detalharPagamentosConsolidadosSaidaDTO;
    }

    /**
     * Nome: setDetalharPagamentosConsolidadosSaidaDTO.
     *
     * @param detalharPagamentosConsolidadosSaidaDTO the detalhar pagamentos consolidados saida dto
     */
    public void setDetalharPagamentosConsolidadosSaidaDTO(
        DetalharPagamentosConsolidadosSaidaDTO detalharPagamentosConsolidadosSaidaDTO) {
        this.detalharPagamentosConsolidadosSaidaDTO = detalharPagamentosConsolidadosSaidaDTO;
    }

    /**
     * Get: dscRazaoSocial.
     *
     * @return the dscRazaoSocial
     */
    public String getDscRazaoSocial() {
        return dscRazaoSocial;
    }

    /**
     * Set: dscRazaoSocial.
     *
     * @param dscRazaoSocial the dscRazaoSocial to set
     */
    public void setDscRazaoSocial(String dscRazaoSocial) {
        this.dscRazaoSocial = dscRazaoSocial;
    }

    /**
     * Get: vlEfetivacaoDebitoPagamento.
     *
     * @return the vlEfetivacaoDebitoPagamento
     */
    public BigDecimal getVlEfetivacaoDebitoPagamento() {
        return vlEfetivacaoDebitoPagamento;
    }

    /**
     * Set: vlEfetivacaoDebitoPagamento.
     *
     * @param vlEfetivacaoDebitoPagamento the vlEfetivacaoDebitoPagamento to set
     */
    public void setVlEfetivacaoDebitoPagamento(
        BigDecimal vlEfetivacaoDebitoPagamento) {
        this.vlEfetivacaoDebitoPagamento = vlEfetivacaoDebitoPagamento;
    }

    /**
     * Get: vlEfetivacaoCreditoPagamento.
     *
     * @return the vlEfetivacaoCreditoPagamento
     */
    public BigDecimal getVlEfetivacaoCreditoPagamento() {
        return vlEfetivacaoCreditoPagamento;
    }

    /**
     * Set: vlEfetivacaoCreditoPagamento.
     *
     * @param vlEfetivacaoCreditoPagamento the vlEfetivacaoCreditoPagamento to set
     */
    public void setVlEfetivacaoCreditoPagamento(
        BigDecimal vlEfetivacaoCreditoPagamento) {
        this.vlEfetivacaoCreditoPagamento = vlEfetivacaoCreditoPagamento;
    }

    /**
     * Get: vlDescontoPagamento.
     *
     * @return the vlDescontoPagamento
     */
    public BigDecimal getVlDescontoPagamento() {
        return vlDescontoPagamento;
    }

    /**
     * Set: vlDescontoPagamento.
     *
     * @param vlDescontoPagamento the vlDescontoPagamento to set
     */
    public void setVlDescontoPagamento(BigDecimal vlDescontoPagamento) {
        this.vlDescontoPagamento = vlDescontoPagamento;
    }

    /**
     * Get: hrEfetivacaoCreditoPagamento.
     *
     * @return the hrEfetivacaoCreditoPagamento
     */
    public String getHrEfetivacaoCreditoPagamento() {
        return hrEfetivacaoCreditoPagamento;
    }

    /**
     * Set: hrEfetivacaoCreditoPagamento.
     *
     * @param hrEfetivacaoCreditoPagamento the hrEfetivacaoCreditoPagamento to set
     */
    public void setHrEfetivacaoCreditoPagamento(String hrEfetivacaoCreditoPagamento) {
        this.hrEfetivacaoCreditoPagamento = hrEfetivacaoCreditoPagamento;
    }

    /**
     * Get: hrEnvioCreditoPagamento.
     *
     * @return the hrEnvioCreditoPagamento
     */
    public String getHrEnvioCreditoPagamento() {
        return hrEnvioCreditoPagamento;
    }

    /**
     * Set: hrEnvioCreditoPagamento.
     *
     * @param hrEnvioCreditoPagamento the hrEnvioCreditoPagamento to set
     */
    public void setHrEnvioCreditoPagamento(String hrEnvioCreditoPagamento) {
        this.hrEnvioCreditoPagamento = hrEnvioCreditoPagamento;
    }

    /**
     * Get: cdIdentificadorTransferenciaPagto.
     *
     * @return the cdIdentificadorTransferenciaPagto
     */
    public Integer getCdIdentificadorTransferenciaPagto() {
        return cdIdentificadorTransferenciaPagto;
    }

    /**
     * Set: cdIdentificadorTransferenciaPagto.
     *
     * @param cdIdentificadorTransferenciaPagto the cdIdentificadorTransferenciaPagto to set
     */
    public void setCdIdentificadorTransferenciaPagto(
        Integer cdIdentificadorTransferenciaPagto) {
        this.cdIdentificadorTransferenciaPagto = cdIdentificadorTransferenciaPagto;
    }

    /**
     * Get: cdMensagemLinExtrato.
     *
     * @return the cdMensagemLinExtrato
     */
    public Integer getCdMensagemLinExtrato() {
        return cdMensagemLinExtrato;
    }

    /**
     * Set: cdMensagemLinExtrato.
     *
     * @param cdMensagemLinExtrato the cdMensagemLinExtrato to set
     */
    public void setCdMensagemLinExtrato(Integer cdMensagemLinExtrato) {
        this.cdMensagemLinExtrato = cdMensagemLinExtrato;
    }

    /**
     * Get: cdConveCtaSalarial.
     *
     * @return the cdConveCtaSalarial
     */
    public Long getCdConveCtaSalarial() {
        return cdConveCtaSalarial;
    }

    /**
     * Set: cdConveCtaSalarial.
     *
     * @param cdConveCtaSalarial the cdConveCtaSalarial to set
     */
    public void setCdConveCtaSalarial(Long cdConveCtaSalarial) {
        this.cdConveCtaSalarial = cdConveCtaSalarial;
    }

    /**
     * Is flag exibe campos.
     *
     * @return the flagExibeCampos
     */
    public boolean isFlagExibeCampos() {
        return flagExibeCampos;
    }

    /**
     * Set: flagExibeCampos.
     *
     * @param flagExibeCampos the flagExibeCampos to set
     */
    public void setFlagExibeCampos(boolean flagExibeCampos) {
        this.flagExibeCampos = flagExibeCampos;
    }

    /**
     * Get: dsIdentificadorTransferenciaPagto.
     *
     * @return the dsIdentificadorTransferenciaPagto
     */
    public String getDsIdentificadorTransferenciaPagto() {
        return dsIdentificadorTransferenciaPagto;
    }

    /**
     * Set: dsIdentificadorTransferenciaPagto.
     *
     * @param dsIdentificadorTransferenciaPagto the dsIdentificadorTransferenciaPagto to set
     */
    public void setDsIdentificadorTransferenciaPagto(
        String dsIdentificadorTransferenciaPagto) {
        this.dsIdentificadorTransferenciaPagto = dsIdentificadorTransferenciaPagto;
    }

    /**
     * Set: dsBancoCredito.
     *
     * @param dsBancoCredito the dsBancoCredito to set
     */
    public void setDsBancoCredito(String dsBancoCredito) {
        this.dsBancoCredito = dsBancoCredito;
    }

    /**
     * Get: dsBancoCredito.
     *
     * @return the dsBancoCredito
     */
    public String getDsBancoCredito() {
        return dsBancoCredito;
    }

    /**
     * Set: cdIspbPagamento.
     *
     * @param cdIspbPagamento the cdIspbPagamento to set
     */
    public void setCdIspbPagamento(String cdIspbPagamento) {
        this.cdIspbPagamento = cdIspbPagamento;
    }

    /**
     * Get: cdIspbPagamento.
     *
     * @return the cdIspbPagamento
     */
    public String getCdIspbPagamento() {
        return cdIspbPagamento;
    }

    /**
     * Get: consultaAutorizanteService.
     *
     * @return consultaAutorizanteService
     */
    public IConsultaAutorizanteService getConsultaAutorizanteService() {
        return consultaAutorizanteService;
    }

    /**
     * Set: consultaAutorizanteService.
     *
     * @param consultaAutorizanteService the consulta autorizante service
     */
    public void setConsultaAutorizanteService(IConsultaAutorizanteService consultaAutorizanteService) {
        this.consultaAutorizanteService = consultaAutorizanteService;
    }

    /**
     * Get: consultarAutorizantesOcorrencia.
     *
     * @return consultarAutorizantesOcorrencia
     */
    public ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO> getConsultarAutorizantesOcorrencia() {
        return consultarAutorizantesOcorrencia;
    }

    /**
     * Set: consultarAutorizantesOcorrencia.
     *
     * @param consultarAutorizantesOcorrencia the consultar autorizantes ocorrencia
     */
    public void setConsultarAutorizantesOcorrencia(
        ArrayList<ConsultarAutorizantesOcorrenciaSaidaDTO> consultarAutorizantesOcorrencia) {
        this.consultarAutorizantesOcorrencia = consultarAutorizantesOcorrencia;
    }

    /**
     * Get: itemSelecionadoListaAutorizante.
     *
     * @return itemSelecionadoListaAutorizante
     */
    public Integer getItemSelecionadoListaAutorizante() {
        return itemSelecionadoListaAutorizante;
    }

    /**
     * Set: itemSelecionadoListaAutorizante.
     *
     * @param itemSelecionadoListaAutorizante the item selecionado lista autorizante
     */
    public void setItemSelecionadoListaAutorizante(Integer itemSelecionadoListaAutorizante) {
        this.itemSelecionadoListaAutorizante = itemSelecionadoListaAutorizante;
    }

    /**
     * Get: listaRadioAutorizante.
     *
     * @return listaRadioAutorizante
     */
    public ArrayList<SelectItem> getListaRadioAutorizante() {
        return listaRadioAutorizante;
    }

    /**
     * Set: listaRadioAutorizante.
     *
     * @param listaRadioAutorizante the lista radio autorizante
     */
    public void setListaRadioAutorizante(ArrayList<SelectItem> listaRadioAutorizante) {
        this.listaRadioAutorizante = listaRadioAutorizante;
    }

    /**
     * Get: dsNomeReclamante.
     *
     * @return dsNomeReclamante
     */
    public String getDsNomeReclamante() {
        return dsNomeReclamante;
    }

    /**
     * Set: dsNomeReclamante.
     *
     * @param dsNomeReclamante the ds nome reclamante
     */
    public void setDsNomeReclamante(String dsNomeReclamante) {
        this.dsNomeReclamante = dsNomeReclamante;
    }

    /**
     * Get: nrProcessoDepositoJudicial.
     *
     * @return nrProcessoDepositoJudicial
     */
    public String getNrProcessoDepositoJudicial() {
        return nrProcessoDepositoJudicial;
    }

    /**
     * Set: nrProcessoDepositoJudicial.
     *
     * @param nrProcessoDepositoJudicial the nr processo deposito judicial
     */
    public void setNrProcessoDepositoJudicial(String nrProcessoDepositoJudicial) {
        this.nrProcessoDepositoJudicial = nrProcessoDepositoJudicial;
    }

    /**
     * Get: nrPisPasep.
     *
     * @return nrPisPasep
     */
    public String getNrPisPasep() {
        return nrPisPasep;
    }

    /**
     * Set: nrPisPasep.
     *
     * @param nrPisPasep the nr pis pasep
     */
    public void setNrPisPasep(String nrPisPasep) {
        this.nrPisPasep = nrPisPasep;
    }

    /**
     * Nome: setDetalharPagtoTed.
     *
     * @param detalharPagtoTed the detalhar pagto ted
     */
    public void setDetalharPagtoTed(DetalharPagtoTEDSaidaDTO detalharPagtoTed) {
        this.detalharPagtoTed = detalharPagtoTed;
    }

    /**
     * Nome: getDetalharPagtoTed.
     *
     * @return detalharPagtoTed
     */
    public DetalharPagtoTEDSaidaDTO getDetalharPagtoTed() {
        return detalharPagtoTed;
    }

    /**
     * Nome: setCdFinalidadeTedDepositoJudicial.
     *
     * @param cdFinalidadeTedDepositoJudicial the cd finalidade ted deposito judicial
     */
    public void setCdFinalidadeTedDepositoJudicial(boolean cdFinalidadeTedDepositoJudicial) {
        this.cdFinalidadeTedDepositoJudicial = cdFinalidadeTedDepositoJudicial;
    }

    /**
     * Nome: isCdFinalidadeTedDepositoJudicial.
     *
     * @return cdFinalidadeTedDepositoJudicial
     */
    public boolean isCdFinalidadeTedDepositoJudicial() {
        return cdFinalidadeTedDepositoJudicial;
    }

    /**
     * Nome: getEntradaDetalhe
     *
     * @return entradaDetalhe
     */
    public DetalharPagamentosConsolidadosEntradaDTO getEntradaDetalhe() {
        return entradaDetalhe;
    }

    /**
     * Nome: setEntradaDetalhe
     *
     * @param entradaDetalhe
     */
    public void setEntradaDetalhe(DetalharPagamentosConsolidadosEntradaDTO entradaDetalhe) {
        this.entradaDetalhe = entradaDetalhe;
    }

    /**
     * Nome: getListaFormaPagamento
     *
     * @return listaFormaPagamento
     */
    public List<ListarTipoRetiradaOcorrenciasSaidaDTO> getListaFormaPagamento() {
        return listaFormaPagamento;
    }

    /**
     * Nome: setListaFormaPagamento
     *
     * @param listaFormaPagamento
     */
    public void setListaFormaPagamento(List<ListarTipoRetiradaOcorrenciasSaidaDTO> listaFormaPagamento) {
        this.listaFormaPagamento = listaFormaPagamento;
    }

	public String getCdBancoPagamento() {
		return cdBancoPagamento;
	}

	public void setCdBancoPagamento(String cdBancoPagamento) {
		this.cdBancoPagamento = cdBancoPagamento;
	}

	public String getIspb() {
		return ispb;
	}

	public void setIspb(String ispb) {
		this.ispb = ispb;
	}

	public String getCdContaPagamento() {
		return cdContaPagamento;
	}

	public void setCdContaPagamento(String cdContaPagamento) {
		this.cdContaPagamento = cdContaPagamento;
	}

	public String getDsISPB() {
		return dsISPB;
	}

	public void setDsISPB(String dsISPB) {
		this.dsISPB = dsISPB;
	}

	public String getTipoContaDestinoPagamento() {
		return tipoContaDestinoPagamento;
	}

	public void setTipoContaDestinoPagamento(String tipoContaDestinoPagamento) {
		this.tipoContaDestinoPagamento = tipoContaDestinoPagamento;
	}

	public String getCdBancoDestinoPagamento() {
		return cdBancoDestinoPagamento;
	}

	public void setCdBancoDestinoPagamento(String cdBancoDestinoPagamento) {
		this.cdBancoDestinoPagamento = cdBancoDestinoPagamento;
	}

	public String getContaDigitoDestinoPagamento() {
		return contaDigitoDestinoPagamento;
	}

	public void setContaDigitoDestinoPagamento(String contaDigitoDestinoPagamento) {
		this.contaDigitoDestinoPagamento = contaDigitoDestinoPagamento;
	}

	public boolean isExibeBancoCredito() {
		return exibeBancoCredito;
	}

	public void setExibeBancoCredito(boolean exibeBancoCredito) {
		this.exibeBancoCredito = exibeBancoCredito;
	}

	public String getCdContaPagamentoDet() {
		return cdContaPagamentoDet;
	}

	public void setCdContaPagamentoDet(String cdContaPagamentoDet) {
		this.cdContaPagamentoDet = cdContaPagamentoDet;
	}

	public String getNumeroInscricaoSacadorAvalista() {
		return numeroInscricaoSacadorAvalista;
	}

	public void setNumeroInscricaoSacadorAvalista(
			String numeroInscricaoSacadorAvalista) {
		this.numeroInscricaoSacadorAvalista = numeroInscricaoSacadorAvalista;
	}

	public String getDsSacadorAvalista() {
		return dsSacadorAvalista;
	}

	public void setDsSacadorAvalista(String dsSacadorAvalista) {
		this.dsSacadorAvalista = dsSacadorAvalista;
	}

	public String getDsTipoSacadorAvalista() {
		return dsTipoSacadorAvalista;
	}

	public void setDsTipoSacadorAvalista(String dsTipoSacadorAvalista) {
		this.dsTipoSacadorAvalista = dsTipoSacadorAvalista;
	}
	
	public String getAgenciaEstornoFormatado() {
		if(NUMERO_ZERO != detalheOrdemPagamento.getCdAgenciaEstorno() && !VAZIO.equals(detalheOrdemPagamento.getCdDigitoAgenciaEstorno())){			
			return detalheOrdemPagamento.getCdAgenciaEstorno() + "-" + detalheOrdemPagamento.getCdDigitoAgenciaEstorno();			
		}else{
			return VAZIO;
		} 
	}	
	
	public String getContaEstornoFormatado() {
		if(NUMERO_ZERO_LONG != detalheOrdemPagamento.getCdContaEstorno() && !VAZIO.equals(detalheOrdemPagamento.getCdDigitoContaEstorno())){
			return detalheOrdemPagamento.getCdContaEstorno() + "-" + detalheOrdemPagamento.getCdDigitoContaEstorno();
		}else{
			return VAZIO;
		}		
	}

	public DetalharPagtoOrdemPagtoSaidaDTO getDetalheOrdemPagamento() {
		return detalheOrdemPagamento;
	}

	public void setDetalheOrdemPagamento(
			DetalharPagtoOrdemPagtoSaidaDTO detalheOrdemPagamento) {
		this.detalheOrdemPagamento = detalheOrdemPagamento;
	}

}
