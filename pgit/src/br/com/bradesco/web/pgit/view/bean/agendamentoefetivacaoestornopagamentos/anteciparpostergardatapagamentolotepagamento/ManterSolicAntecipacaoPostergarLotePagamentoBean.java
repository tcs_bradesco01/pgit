package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.anteciparpostergardatapagamentolotepagamento;

import static br.com.bradesco.web.pgit.view.converters.FormatarData.formataDiaMesAno;
import static br.com.bradesco.web.pgit.view.converters.FormatarData.formatarDataToPdc;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.event.ActionEvent;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ConsultarLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.DetalharLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.ExcluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.IncluirLotePagamentosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lotepagamentos.bean.OcorrenciasLotePagamentosDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.bean.ConsultarLotesIncSolicEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicantecipacaoproclotepagamento.bean.ConsultarLotesIncSolicSaidaDTO;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.lotepagamentos.LotePagamentosBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

public class ManterSolicAntecipacaoPostergarLotePagamentoBean extends
		LotePagamentosBean {

	private static final int COD_SOLICITACAO_PAGAMENTO_INTEGRADO_TRINTA = 30;
	private static final int COD_SOLICITACAO_PAGAMENTO_INTEGRADO_TRINTA_TRES = 33;
	private static final Integer DOIS = 2;
	private static final Integer ZERO = 0;
	private static final String MENSAGEM_LOTE = "ESTE LOTE CONT�M UM OU MAIS PAGAMENTOS QUE N�O ATENDEM AOS REQUISITOS DE MANUTEN��O POR LOTE. OPERA��O N�O PERMITIDA.";

	private static final String POSTERGAR = "Postergar";
	private static final String ANTECIPAR = "Antecipar";
	private static final String TELA_CONSULTA = "conSolicAntecipacaoPostergarLotePagto";
	private static final String TELA_DETALHE = "detSolicAntecipacaoPostergarLotePagto";
	private static final String TELA_EXCLUSAO = "excSolicAntecipacaoPostergarLotePagto";
	private static final String TELA_INCLUSAO = "incSolicAntecipacaoPostergarLotePagto";
	private static final String TELA_INCLUSAO_AVANCAR = "avancarIncSolicAntecipacaoPostergarLotePagto";
	private static final String TELA_INCLUSAO_CONFIRMACAO = "confIncSolicAntecipacaoPostergarLotePagto";
	private static final String TELA_DETALHE_OCORRENCIAS = "detSolicAntecipacaoPostergarLotePagtoOcorrencias";

	private Date novaDataAgendamento;
	private Date novaDataAgendamentoConfirma;
	private IncluirLotePagamentosEntradaDTO entradaInclusao;

	private ConsultarLotesIncSolicSaidaDTO loteSelecionado = new ConsultarLotesIncSolicSaidaDTO();

	@Override
	public String getTelaConsulta() {
		return TELA_CONSULTA;
	}

	@Override
	public String getTelaDetalhe() {
		return TELA_DETALHE;
	}

	@Override
	public String getTelaDetalheOcorrencias() {
		return TELA_DETALHE_OCORRENCIAS;
	}

	@Override
	public String getTelaExclusao() {
		return TELA_EXCLUSAO;
	}

	@Override
	public String getTelaInclusao() {
		return TELA_INCLUSAO;
	}

	public String avancar() {
		loteSelecionado = getListaLote().get(getItemSelecao());
		
		if (loteSelecionado.getCdIndicadorLoteApto().compareTo(DOIS) == ZERO) {

			BradescoFacesUtils.addInfoModalMessage(MENSAGEM_LOTE,
					getTelaInclusao(), getBean(), false);

			return null;

		}

		return TELA_INCLUSAO_AVANCAR;
	}

	@Override
	public String getTelaInclusaoConfirmacao() {
		return TELA_INCLUSAO_CONFIRMACAO;
	}

	@Override
	public String carregaListaLotePagamento() {

		ConsultarLotePagamentosEntradaDTO entrada = preparaBuscaListaLotePagamento();

		try {
			setListaGridLotePagto(getManterSolicAntecipacaoPostergarLotePagamentoService()
					.consultarSolicAntPosDtPgto(entrada).getOcorrencias());

			carregaListaControleRadios();

			getPagamentosBean().setDisableArgumentosConsulta(true);

		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);

			setListaGridLotePagto(new ArrayList<OcorrenciasLotePagamentosDTO>());
			setItemSelecionadoLista(null);
			getPagamentosBean().setDisableArgumentosConsulta(false);
		}

		return "";
	}

	@Override
	public boolean isDesabilitaConsultarLote() {
		if (getTipoAgendamento() != null
				&& getPagamentosBean().getLoteInterno() != null
				&& !getPagamentosBean().getLoteInterno().equals("")) {
			return false;
		} else {
			return true;
		}
	}

	public void consultaLotes() {
		ConsultarLotesIncSolicEntradaDTO entrada = new ConsultarLotesIncSolicEntradaDTO();
		entrada
				.setCdpessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getEmpresaGestoraFiltro());
		if (getTipoAgendamento() != null && getTipoAgendamento() == 1) {
			entrada.setCdSolicitacaoPagamentoIntegrado(30);
		} else if (getTipoAgendamento() != null && getTipoAgendamento() == 2) {
			entrada.setCdSolicitacaoPagamentoIntegrado(33);
		}
		entrada
				.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getCdTipoContratoNegocio());
		entrada.setNrLoteInterno(getPagamentosBean().getLoteInterno());
		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("0")) {
			entrada.setNrSequenciaContratoNegocio(NumberUtils
					.createLong(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNumeroDescCliente()));
		} else {
			entrada.setNrSequenciaContratoNegocio(NumberUtils
					.createLong(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNumeroDescContrato()));
		}

		setListaLote(getManterSolicAntecipacaoProcLotePagamentoService()
				.consultarLotes(entrada));
	}

	@Override
	public void buscaInformacoesDetalhe() {
		DetalharLotePagamentosEntradaDTO entrada = prepararBuscaInformacoesDetalhe();

		DetalharLotePagamentosSaidaDTO saidaDetlahe = getManterSolicAntecipacaoPostergarLotePagamentoService()
				.detalharSolicAntPosDtPgto(entrada);

		consolidaInformacoesDetalhe(saidaDetlahe);
	}
	
	/**
	 * carrega Lista Lote Pagamento
	 *
	 * @param evt the evt
	 */
	public void carregaListaLotePagamento(ActionEvent evt) {
		carregaListaLotePagamento();
	}

	@Override
	public String confirmarExcluir() {
		ExcluirLotePagamentosEntradaDTO entrada = prepararExclusaoLotePagamento();

		try {

			final ExcluirLotePagamentosSaidaDTO saida = getManterSolicAntecipacaoPostergarLotePagamentoService()
					.excluirSolicAntPosDtPgto(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),getTelaConsulta(),
					"#{manterSolicAntecipacaoPostergarLotePagamentoBean.carregaListaLotePagamento}", false);

		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}

		setItemSelecionadoLista(null);
		return "";
	}

	@Override
	public String getBean() {
		return "#{manterSolicAntecipacaoPostergarLotePagamentoBean.iniciarTela}";
	}

	public String avancarIncluirOutro() {
		if (novaDataAgendamentoConfirma == null) {
			BradescoFacesUtils.addInfoModalMessage("(O campo � obrigat�rio)",
					false);
			return "";
		}

		entradaInclusao = prepararInclusaoLotePagamento();

		entradaInclusao.setCdIndicadorFase(1);
		entradaInclusao
				.setDtNovaDataAgenda(formatarDataToPdc(formataDiaMesAno(getNovaDataAgendamentoConfirma())));
		entradaInclusao.setCdIndicadorAntecipPosterg(getTipoAgendamento());
		entradaInclusao
				.setCdpessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getCdPessoaJuridicaContrato());
		entradaInclusao
				.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getCdTipoContratoNegocio());
		entradaInclusao.setCdTipoLayoutArquivo(getLoteSelecionado()
				.getCdTipoLayoutArquivo());
		entradaInclusao.setCdIndicadorAntecipPosterg(getTipoAgendamento());
		entradaInclusao.setCdProdutoServicoOperacao(getLoteSelecionado()
				.getCdProdutoServicoOperacao());
		entradaInclusao.setQtdeTotalPagtoPrevistoSoltc(getLoteSelecionado()
				.getQtTotalPagamentoLote());
		entradaInclusao.setVlrTotPagtoPrevistoSolicitacao(getLoteSelecionado()
				.getVlTotalPagamentoLote());

		try {

			getManterSolicAntecipacaoPostergarLotePagamentoService()
					.incluirSolicAntPosDtPgto(entradaInclusao);

		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}

		return getTelaInclusaoConfirmacao();
	}

	public Date getNovaDataAgendamento() {
		return novaDataAgendamento;
	}

	public void setNovaDataAgendamento(Date novaDataAgendamento) {
		this.novaDataAgendamento = novaDataAgendamento;
	}

	public String getNovaDataAgendamentoFormatada() {
		return FormatarData.formataDiaMesAno(getNovaDataAgendamento());
	}

	public void habilitaCampoNovaDataAgendamento() {
		setNovaDataAgendamento(new Date());
	}


	public String voltarInclusaoPostergar() {
		setItemSelecao(null);
		return TELA_INCLUSAO_AVANCAR;
	} 
	
	public String getDescTipoAgendamento() {
		if(tipoAgendamento!=null){
			if (tipoAgendamento.compareTo(1) == 0) {
	
				return ANTECIPAR;
			}else{
				return POSTERGAR;
			}
		}
		return "";
	}

	public void setNovaDataAgendamentoConfirma(Date novaDataAgendamentoConfirma) {
		this.novaDataAgendamentoConfirma = novaDataAgendamentoConfirma;
	}

	public Date getNovaDataAgendamentoConfirma() {
		return novaDataAgendamentoConfirma;
	}

	@Override
	public Integer getCodigoSolicitacao() {

		if (getTipoAgendamento().compareTo(1) == 0) {
			return COD_SOLICITACAO_PAGAMENTO_INTEGRADO_TRINTA;
		} else {
			return COD_SOLICITACAO_PAGAMENTO_INTEGRADO_TRINTA_TRES;
		}

	}

	@Override
	public String confirmarIncluir() {

		try {
			entradaInclusao.setCdIndicadorFase(2);
			entradaInclusao.setCdTipoLayoutArquivo(getListaLote().get(getItemSelecao()).getCdTipoLayoutArquivo());

			IncluirLotePagamentosSaidaDTO saida = getManterSolicAntecipacaoPostergarLotePagamentoService()
					.incluirSolicAntPosDtPgto(entradaInclusao);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),getTelaInclusao(),
					"", false);
			
			setItemSelecao(null);
			
		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}

		return "";
	}

	@Override
	public ConsultarLotesIncSolicSaidaDTO getLoteSelecionado() {
		return loteSelecionado;
	}

	@Override
	public void setLoteSelecionado(
			ConsultarLotesIncSolicSaidaDTO loteSelecionado) {
		this.loteSelecionado = loteSelecionado;
	}
}
