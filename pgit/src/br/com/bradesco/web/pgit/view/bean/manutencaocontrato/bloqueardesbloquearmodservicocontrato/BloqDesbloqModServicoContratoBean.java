/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.bloqueardesbloquearmodservicocontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.bloqueardesbloquearmodservicocontrato;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.IBloquearDesbloquearServicoContratoService;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean.BloquearDesbloquearProdServConSaidaDTO;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean.BloquearDesbloquearProdServContEntradaDTO;
import br.com.bradesco.web.pgit.service.business.bloqueardesbloquearservicocontrato.bean.OcorrenciaBloquearDesbloquearProdServContDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoServContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoServContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarModalidadeTipoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarModalidadeTipoServicoSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: BloqDesbloqModServicoContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class BloqDesbloqModServicoContratoBean {
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo bloqDesbloqServicoContratoImpl. */
	private IBloquearDesbloquearServicoContratoService bloqDesbloqServicoContratoImpl;
	
	/** Atributo manterContratoService. */
	private IManterContratoService manterContratoService;
	
	/** Atributo bloquearDesbloquearFiltro. */
	private Integer bloquearDesbloquearFiltro;
	
	/** Atributo motivoBloqueioFiltro. */
	private Integer motivoBloqueioFiltro;

	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;
	
	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo nrSequenciaContrato. */
	private Long nrSequenciaContrato;
	
	/** Atributo cpfCnpjMaster. */
	private String cpfCnpjMaster;
	
	/** Atributo gerenteResponsavel. */
	private String gerenteResponsavel;
	
	/** Atributo clienteParticipante. */
	private String clienteParticipante;
	
	/** Atributo nomeRazaoMaster. */
	private String nomeRazaoMaster;
	
	/** Atributo grupoEconomicoMaster. */
	private String grupoEconomicoMaster;
	
	/** Atributo ativEconomicaMaster. */
	private String ativEconomicaMaster;
	
	/** Atributo segmentoMaster. */
	private String segmentoMaster;
	
	/** Atributo subSegmentoMaster. */
	private String subSegmentoMaster;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;
	
	/** Atributo empresaContrato. */
	private String empresaContrato;
	
	/** Atributo tipoContrato. */
	private String tipoContrato;
	
	/** Atributo numeroContrato. */
	private String numeroContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo situacaoContrato. */
	private String situacaoContrato;
	
	/** Atributo motivoContrato. */
	private String motivoContrato;
	
	/** Atributo participacaoContrato. */
	private String participacaoContrato;

	/** Atributo listaGridServico. */
	private List<ListarModalidadeTipoServicoSaidaDTO> listaGridServico;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionado. */
	private Integer itemSelecionado;
	
	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo listaGridModalidade. */
	private List<ListarModalidadeTipoServicoSaidaDTO> listaGridModalidade;
	
	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos;
	
	/** Atributo listaControleModalidade. */
	private List<SelectItem> listaControleModalidade;
	
	/** Atributo panelBotoes. */
	private boolean panelBotoes;
	
	/** Atributo hiddenProsseguir. */
	private String hiddenProsseguir;
	
	/** Atributo listaModalidadeSelecionado. */
	private List<ListarModalidadeTipoServicoSaidaDTO> listaModalidadeSelecionado;
	
	/** Atributo listaMotivoBloqueio. */
	private List<SelectItem> listaMotivoBloqueio = new ArrayList<SelectItem>();
	
	/** Atributo listaMotivoBloqueioHash. */
	private Map<Integer, String> listaMotivoBloqueioHash = new HashMap<Integer, String>();	
	
	/** Atributo dsMotivoBloqueio. */
	private String dsMotivoBloqueio;
	
	//M�todos
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt){
		setBloquearDesbloquearFiltro(null);
		setMotivoBloqueioFiltro(null);
		
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		//carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteBloqDesbloqModServicoContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conBloquearDesbloquearModServicoContrato");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		
		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		
	}
	
	/**
	 * Servicos pesquisar.
	 *
	 * @return the string
	 */
	public String servicosPesquisar(){
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtivEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdPessoaJuridica(saidaDTO.getCdPessoaJuridica());
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setCdTipoContrato(saidaDTO.getCdTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());
		setNrSequenciaContrato(saidaDTO.getNrSequenciaContrato());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
		
		setListaGridServico(null);
		setItemSelecionado(null);
		carregarDados();

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		
		} else {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		}

		return "SERVICOS";
	}
	
	/**
	 * Carregar dados.
	 *
	 * @return the string
	 */
	public String carregarDados(){
		
		try{
			ListarModalidadeTipoServicoEntradaDTO entradaDTO = new ListarModalidadeTipoServicoEntradaDTO();
	
			entradaDTO.setCdPessoaJuridica(getCdPessoaJuridica());
			entradaDTO.setCdTipoContrato(getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
			entradaDTO.setCdModalidade(0);
			entradaDTO.setCdTipoServico(0);
			
			setListaGridServico(manterContratoService.listarModalidadeTipoServico(entradaDTO));

			listaControleRadio = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridServico().size(); i++) {
				this.listaControleRadio.add(new SelectItem(i, " "));
			}
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "+ p.getMessage(), false);
			setListaGridServico(null);

		}

		return "OK";			

	}
	
	/**
	 * Pesquisar consulta.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarConsulta(ActionEvent evt){
		carregarDados();
		return "";
	}
	
	/**
	 * Pesquisar consulta modalidade.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarConsultaModalidade(ActionEvent evt){
		carregaModalidades();
		return "";
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Voltar servico.
	 *
	 * @return the string
	 */
	public String voltarServico(){
		setItemSelecionado(null);
		return "VOLTAR";
	}
	
	/**
	 * Modalidade.
	 *
	 * @return the string
	 */
	public String modalidade(){
		//Conforme solicitado pela Luciana, dia 22/julho, no pdc ListarModalidadeTipoServico, quando precisar aprensentar a dsServico pegar o campo
		//no pdc de dsModalidade, mesma coisa para o codigo, porem quando solicitar os dois campos, o cdServico sera ele mesmo e o cdModalidade tambem
		setCdTipoServico(getListaGridServico().get(getItemSelecionado()).getCdModalidade());
		setDsTipoServico(getListaGridServico().get(getItemSelecionado()).getDsModalidade());
		
		setOpcaoChecarTodos(false);
		setListaControleModalidade(null);
		setPanelBotoes(false);
		carregaModalidades();
		return "MODALIDADE";
	}
	
	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes(){
		for(int i=0;i<getListaGridModalidade().size();i++){
			if(getListaGridModalidade().get(i).isCheck()){
				setPanelBotoes(true);
				return;
			}
		}
		setPanelBotoes(false);	
	}
	
	/**
	 * Carrega modalidades.
	 */
	public void carregaModalidades(){
		try{
			ListarModalidadeTipoServicoEntradaDTO entradaDTO = new ListarModalidadeTipoServicoEntradaDTO();
			entradaDTO.setCdModalidade(0);
			entradaDTO.setCdPessoaJuridica(getCdPessoaJuridica());
			entradaDTO.setCdTipoContrato(getCdTipoContrato());
			entradaDTO.setCdTipoServico(getCdTipoServico());
			entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
	
			setListaGridModalidade(manterContratoService.listarModalidadeTipoServico(entradaDTO));

			listaControleModalidade = new ArrayList<SelectItem>();
			for(int i = 0;i<getListaGridModalidade().size();i++){
				listaControleModalidade.add(new SelectItem(i,""));
			}
			setPanelBotoes(false);
			setOpcaoChecarTodos(false);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "+ p.getMessage(), false);
			setListaGridModalidade(null);

		}
	}
	
	/**
	 * Bloquear desbloquear.
	 *
	 * @return the string
	 */
	public String bloquearDesbloquear(){
		listaModalidadeSelecionado = new ArrayList<ListarModalidadeTipoServicoSaidaDTO>();
		
		for(int i=0;i<getListaGridModalidade().size();i++){
			if(getListaGridModalidade().get(i).isCheck()){
				listaModalidadeSelecionado.add(getListaGridModalidade().get(i));
			}
		}
		
		setBloquearDesbloquearFiltro(0);
		setMotivoBloqueioFiltro(0);
		
		return "BLOQUEAR_DESBLOQUEAR";
	}
	
	/**
	 * Voltar modalidades.
	 *
	 * @return the string
	 */
	public String voltarModalidades(){
		for(int i=0;i<getListaGridModalidade().size();i++){
			getListaGridModalidade().get(i).setCheck(false);
		}
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
		return "VOLTAR";
	}
	
	/**
	 * Limpar filtros.
	 */
	public void limparFiltros(){
		setBloquearDesbloquearFiltro(null);
		setMotivoBloqueioFiltro(null);
		this.listaMotivoBloqueio.clear();
	}
	
	/**
	 * Listar motivo situacao.
	 */
	public void listarMotivoSituacao(){
		setMotivoBloqueioFiltro(0);
		this.listaMotivoBloqueio = new ArrayList<SelectItem>();
		
		List<ListarMotivoSituacaoServContratoSaidaDTO> saidaDTO = new ArrayList<ListarMotivoSituacaoServContratoSaidaDTO>();
		ListarMotivoSituacaoServContratoEntradaDTO entrada = new ListarMotivoSituacaoServContratoEntradaDTO();
		
		//Se for Bloqueio - Passar 3 - Email Enviado: quarta-feira, 7 de julho de 2010 16:05  
		if (getBloquearDesbloquearFiltro() == 1 ){
			entrada.setCdSituacaoVinculacaoConta(3);
		}
		else{
			entrada.setCdSituacaoVinculacaoConta(1);
		}
		
		saidaDTO = comboService.listarMotivoSituacaoServContrato(entrada);
		listaMotivoBloqueioHash.clear();
		
		for (ListarMotivoSituacaoServContratoSaidaDTO combo : saidaDTO) {
			listaMotivoBloqueioHash.put(combo.getCdMotivoSituacaoProduto(), combo.getDsMotivoSituacaoProduto());
			this.listaMotivoBloqueio.add(new SelectItem(combo.getCdMotivoSituacaoProduto(), combo.getDsMotivoSituacaoProduto()));
		}
	}
	
	/**
	 * Avancar bloquear desbloquear.
	 *
	 * @return the string
	 */
	public String avancarBloquearDesbloquear(){
		if(getHiddenProsseguir().equals("T")){
			setDsMotivoBloqueio((String)listaMotivoBloqueioHash.get(getMotivoBloqueioFiltro()));
			return "AVANCAR";
		}else{
			return "";
		}
	}
	
	/**
	 * Voltar confirmar.
	 *
	 * @return the string
	 */
	public String voltarConfirmar(){
		return "VOLTAR";
	}
	
	/**
	 * Confirmar.
	 *
	 * @return the string
	 */
	public String confirmar(){
		try{
			ListarContratosPgitSaidaDTO itemSelecionadoGrid = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
			BloquearDesbloquearProdServContEntradaDTO entradaDTO = new BloquearDesbloquearProdServContEntradaDTO();
			List<OcorrenciaBloquearDesbloquearProdServContDTO> listaEntrada = new ArrayList<OcorrenciaBloquearDesbloquearProdServContDTO>();
			
			entradaDTO.setCdPessoaJuridicaContrato(itemSelecionadoGrid.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(itemSelecionadoGrid.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(itemSelecionadoGrid.getNrSequenciaContrato());
			entradaDTO.setCdProdutoServicoOperacao(getCdTipoServico());
			//Se for Bloquear - Passar 3 | Se for Desbloquear passar 1 - Regra presente na especifica��o F�brica  - Lote 2
			entradaDTO.setCdSituacaoServicoRelacionado(getBloquearDesbloquearFiltro() == 1 ? 3 : 1);
			entradaDTO.setCdMotivoServicoRelacionado(getMotivoBloqueioFiltro());
			entradaDTO.setCdParametro(2);
			
			for(int i=0; i<getListaModalidadeSelecionado().size(); i++){
				OcorrenciaBloquearDesbloquearProdServContDTO ocorrencias = new OcorrenciaBloquearDesbloquearProdServContDTO();
				ocorrencias.setCdServicoRelacionado(getListaModalidadeSelecionado().get(i).getCdModalidade());
				listaEntrada.add(ocorrencias);
			}
			
			entradaDTO.setListaOcorrencias(listaEntrada);
			
			BloquearDesbloquearProdServConSaidaDTO saidaDTO = getBloqDesbloqServicoContratoImpl().bloquearDesbloquearProdServContratos(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "bloquearDesbloquearModServicoContrato2", BradescoViewExceptionActionType.ACTION, false);
		
			carregaModalidades();
			for(int i=0;i<getListaGridModalidade().size();i++){
				getListaGridModalidade().get(i).setCheck(false);
			}
			setOpcaoChecarTodos(false);
			setPanelBotoes(false);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		
		return "";
	}
	
	/**
	 * Limpar dados argumentos pesquisa.
	 */
	public void limparDadosArgumentosPesquisa(){
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(0l);
		identificacaoClienteContratoBean.setTipoContratoFiltro(0);
		identificacaoClienteContratoBean.setNumeroFiltro("");
		identificacaoClienteContratoBean.setAgenciaOperadoraFiltro("");
		identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		identificacaoClienteContratoBean.setCodigoFunc("");
		identificacaoClienteContratoBean.setSituacaoFiltro(0);		
		identificacaoClienteContratoBean.listarMotivoSituacao();
		identificacaoClienteContratoBean.setHabilitaCampos(false);
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setListaGridPesquisa(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		identificacaoClienteContratoBean.setEmpresaGestoraContrato(null);
		setNumeroContrato(null);
		setDescricaoContrato(null);
		setSituacaoContrato(null);
		identificacaoClienteContratoBean.limparRadioFiltro();
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(false);
	}
	
	/**
	 * Consultar contrato.
	 *
	 * @return the string
	 */
	public String consultarContrato(){
		if (identificacaoClienteContratoBean.getObrigatoriedade() != null && identificacaoClienteContratoBean.getObrigatoriedade().equals("T")){

			// desmarca o radio da Grid - vreghini - 01/7/2010.
			identificacaoClienteContratoBean.setItemSelecionadoLista(null);
			identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

			try{
				ListarContratosPgitEntradaDTO entradaDTO = new ListarContratosPgitEntradaDTO();

				entradaDTO.setCdAgencia(identificacaoClienteContratoBean.getAgenciaOperadoraFiltro()!=null&&!identificacaoClienteContratoBean.getAgenciaOperadoraFiltro().equals("")?Integer.parseInt(identificacaoClienteContratoBean.getAgenciaOperadoraFiltro()):0);
				entradaDTO.setCdClubPessoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdClub()!=null?identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdClub():0L);
				entradaDTO.setCdFuncionario(identificacaoClienteContratoBean.getCodigoFunc()!=null&&!identificacaoClienteContratoBean.getCodigoFunc().equals("")?Long.parseLong(identificacaoClienteContratoBean.getCodigoFunc()):0L);				
				entradaDTO.setCdPessoaJuridica(identificacaoClienteContratoBean.getEmpresaGestoraFiltro()!=null?identificacaoClienteContratoBean.getEmpresaGestoraFiltro():0);
				entradaDTO.setCdSituacaoContrato(identificacaoClienteContratoBean.getSituacaoFiltro() != null ? identificacaoClienteContratoBean.getSituacaoFiltro() : 0);
				entradaDTO.setCdTipoContratoNegocio(identificacaoClienteContratoBean.getTipoContratoFiltro()!=null?identificacaoClienteContratoBean.getTipoContratoFiltro():0);
				entradaDTO.setNrSequenciaContrato(identificacaoClienteContratoBean.getNumeroFiltro()!=null&&!identificacaoClienteContratoBean.getNumeroFiltro().equals("")?Long.valueOf(identificacaoClienteContratoBean.getNumeroFiltro()):Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdCpfCnpj());
				entradaDTO.setCdFilialCnpjPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdFilialCnpj());
				entradaDTO.setCdControleCpfPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdControleCnpj());
				
				if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conManterSolManutContrato")) {
					entradaDTO.setParametroPrograma(1);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conManterPendenciasContrato")) {
					entradaDTO.setParametroPrograma(2);
				} else if(identificacaoClienteContratoBean.getPaginaRetorno().equals("conRecuperarContratoEncerrado")) {
					entradaDTO.setParametroPrograma(3);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("pesqContratoPendenteAssinatura")) {
					entradaDTO.setParametroPrograma(4);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conEncerrarContrato")) {
					entradaDTO.setParametroPrograma(5);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("pesqManterSolicitacoesMudancaAmbienteOperacao")) {
					entradaDTO.setParametroPrograma(6);
				} else {
					entradaDTO.setParametroPrograma(0);
				}

				if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conEncerrarContrato")){
					List<ListarContratosPgitSaidaDTO> listaGridTemp = identificacaoClienteContratoBean.getFiltroIdentificaoService().pesquisarManutencaoContrato(entradaDTO);

					identificacaoClienteContratoBean.setListaGridPesquisa(new ArrayList<ListarContratosPgitSaidaDTO>());

					for (ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO : listaGridTemp) {
						if(listarContratosPgitSaidaDTO.getCdSituacaoContrato() == 1 || listarContratosPgitSaidaDTO.getCdSituacaoContrato() == 3){
							identificacaoClienteContratoBean.getListaGridPesquisa().add(listarContratosPgitSaidaDTO);
						}
					}
				}
				else{
					identificacaoClienteContratoBean.setSaidaContrato(new ListarContratosPgitSaidaDTO());
					identificacaoClienteContratoBean.setListaGridPesquisa(identificacaoClienteContratoBean.getFiltroIdentificaoService().pesquisarManutencaoContrato(entradaDTO));
					identificacaoClienteContratoBean.setSaidaContrato(identificacaoClienteContratoBean.getListaGridPesquisa().get(0));
					
				}

				this.identificacaoClienteContratoBean.setListaControleRadio(new ArrayList<SelectItem>());
				for (int i = 0; i < identificacaoClienteContratoBean.getListaGridPesquisa().size();i++) {
					this.identificacaoClienteContratoBean.getListaControleRadio().add(new SelectItem(i," "));
				}
				bloquearArgumentosPesquisaAposConsulta();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				identificacaoClienteContratoBean.setListaGridPesquisa(null);

			}

			return identificacaoClienteContratoBean.getPaginaRetorno();
		}
		
		return "";
	}
	
	/**
	 * Bloquear argumentos pesquisa apos consulta.
	 */
	public void bloquearArgumentosPesquisaAposConsulta(){
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
	}
	
	//Set's e Get's
	/**
	 * Get: bloqDesbloqServicoContratoImpl.
	 *
	 * @return bloqDesbloqServicoContratoImpl
	 */
	public IBloquearDesbloquearServicoContratoService getBloqDesbloqServicoContratoImpl() {
		return bloqDesbloqServicoContratoImpl;
	}
	
	/**
	 * Set: bloqDesbloqServicoContratoImpl.
	 *
	 * @param bloqDesbloqServicoContratoImpl the bloq desbloq servico contrato impl
	 */
	public void setBloqDesbloqServicoContratoImpl(
			IBloquearDesbloquearServicoContratoService bloqDesbloqServicoContratoImpl) {
		this.bloqDesbloqServicoContratoImpl = bloqDesbloqServicoContratoImpl;
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}
	
	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}
	
	/**
	 * Get: manterContratoService.
	 *
	 * @return manterContratoService
	 */
	public IManterContratoService getManterContratoService() {
		return manterContratoService;
	}
	
	/**
	 * Set: manterContratoService.
	 *
	 * @param manterContratoService the manter contrato service
	 */
	public void setManterContratoService(
			IManterContratoService manterContratoService) {
		this.manterContratoService = manterContratoService;
	}

	/**
	 * Get: bloquearDesbloquearFiltro.
	 *
	 * @return bloquearDesbloquearFiltro
	 */
	public Integer getBloquearDesbloquearFiltro() {
		return bloquearDesbloquearFiltro;
	}

	/**
	 * Set: bloquearDesbloquearFiltro.
	 *
	 * @param bloquearDesbloquearFiltro the bloquear desbloquear filtro
	 */
	public void setBloquearDesbloquearFiltro(Integer bloquearDesbloquearFiltro) {
		this.bloquearDesbloquearFiltro = bloquearDesbloquearFiltro;
	}

	/**
	 * Get: motivoBloqueioFiltro.
	 *
	 * @return motivoBloqueioFiltro
	 */
	public Integer getMotivoBloqueioFiltro() {
		return motivoBloqueioFiltro;
	}

	/**
	 * Set: motivoBloqueioFiltro.
	 *
	 * @param motivoBloqueioFiltro the motivo bloqueio filtro
	 */
	public void setMotivoBloqueioFiltro(Integer motivoBloqueioFiltro) {
		this.motivoBloqueioFiltro = motivoBloqueioFiltro;
	}

	/**
	 * Get: ativEconomicaMaster.
	 *
	 * @return ativEconomicaMaster
	 */
	public String getAtivEconomicaMaster() {
		return ativEconomicaMaster;
	}

	/**
	 * Set: ativEconomicaMaster.
	 *
	 * @param ativEconomicaMaster the ativ economica master
	 */
	public void setAtivEconomicaMaster(String ativEconomicaMaster) {
		this.ativEconomicaMaster = ativEconomicaMaster;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: cpfCnpjMaster.
	 *
	 * @return cpfCnpjMaster
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}

	/**
	 * Set: cpfCnpjMaster.
	 *
	 * @param cpfCnpjMaster the cpf cnpj master
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: grupoEconomicoMaster.
	 *
	 * @return grupoEconomicoMaster
	 */
	public String getGrupoEconomicoMaster() {
		return grupoEconomicoMaster;
	}

	/**
	 * Set: grupoEconomicoMaster.
	 *
	 * @param grupoEconomicoMaster the grupo economico master
	 */
	public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
		this.grupoEconomicoMaster = grupoEconomicoMaster;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaGridServico.
	 *
	 * @return listaGridServico
	 */
	public List<ListarModalidadeTipoServicoSaidaDTO> getListaGridServico() {
		return listaGridServico;
	}

	/**
	 * Set: listaGridServico.
	 *
	 * @param listaGridServico the lista grid servico
	 */
	public void setListaGridServico(
			List<ListarModalidadeTipoServicoSaidaDTO> listaGridServico) {
		this.listaGridServico = listaGridServico;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: nomeRazaoMaster.
	 *
	 * @return nomeRazaoMaster
	 */
	public String getNomeRazaoMaster() {
		return nomeRazaoMaster;
	}

	/**
	 * Set: nomeRazaoMaster.
	 *
	 * @param nomeRazaoMaster the nome razao master
	 */
	public void setNomeRazaoMaster(String nomeRazaoMaster) {
		this.nomeRazaoMaster = nomeRazaoMaster;
	}

	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: segmentoMaster.
	 *
	 * @return segmentoMaster
	 */
	public String getSegmentoMaster() {
		return segmentoMaster;
	}

	/**
	 * Set: segmentoMaster.
	 *
	 * @param segmentoMaster the segmento master
	 */
	public void setSegmentoMaster(String segmentoMaster) {
		this.segmentoMaster = segmentoMaster;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: subSegmentoMaster.
	 *
	 * @return subSegmentoMaster
	 */
	public String getSubSegmentoMaster() {
		return subSegmentoMaster;
	}

	/**
	 * Set: subSegmentoMaster.
	 *
	 * @param subSegmentoMaster the sub segmento master
	 */
	public void setSubSegmentoMaster(String subSegmentoMaster) {
		this.subSegmentoMaster = subSegmentoMaster;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public Integer getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(Integer itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}

	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}

	/**
	 * Get: listaGridModalidade.
	 *
	 * @return listaGridModalidade
	 */
	public List<ListarModalidadeTipoServicoSaidaDTO> getListaGridModalidade() {
		return listaGridModalidade;
	}

	/**
	 * Set: listaGridModalidade.
	 *
	 * @param listaGridModalidade the lista grid modalidade
	 */
	public void setListaGridModalidade(
			List<ListarModalidadeTipoServicoSaidaDTO> listaGridModalidade) {
		this.listaGridModalidade = listaGridModalidade;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Get: listaControleModalidade.
	 *
	 * @return listaControleModalidade
	 */
	public List<SelectItem> getListaControleModalidade() {
		return listaControleModalidade;
	}

	/**
	 * Set: listaControleModalidade.
	 *
	 * @param listaControleModalidade the lista controle modalidade
	 */
	public void setListaControleModalidade(List<SelectItem> listaControleModalidade) {
		this.listaControleModalidade = listaControleModalidade;
	}

	/**
	 * Is panel botoes.
	 *
	 * @return true, if is panel botoes
	 */
	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	/**
	 * Set: panelBotoes.
	 *
	 * @param panelBotoes the panel botoes
	 */
	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	/**
	 * Get: hiddenProsseguir.
	 *
	 * @return hiddenProsseguir
	 */
	public String getHiddenProsseguir() {
		return hiddenProsseguir;
	}

	/**
	 * Set: hiddenProsseguir.
	 *
	 * @param hiddenProsseguir the hidden prosseguir
	 */
	public void setHiddenProsseguir(String hiddenProsseguir) {
		this.hiddenProsseguir = hiddenProsseguir;
	}

	/**
	 * Get: listaModalidadeSelecionado.
	 *
	 * @return listaModalidadeSelecionado
	 */
	public List<ListarModalidadeTipoServicoSaidaDTO> getListaModalidadeSelecionado() {
		return listaModalidadeSelecionado;
	}

	/**
	 * Set: listaModalidadeSelecionado.
	 *
	 * @param listaModalidadeSelecionado the lista modalidade selecionado
	 */
	public void setListaModalidadeSelecionado(
			List<ListarModalidadeTipoServicoSaidaDTO> listaModalidadeSelecionado) {
		this.listaModalidadeSelecionado = listaModalidadeSelecionado;
	}

	/**
	 * Get: listaMotivoBloqueio.
	 *
	 * @return listaMotivoBloqueio
	 */
	public List<SelectItem> getListaMotivoBloqueio() {
		return listaMotivoBloqueio;
	}

	/**
	 * Set: listaMotivoBloqueio.
	 *
	 * @param listaMotivoBloqueio the lista motivo bloqueio
	 */
	public void setListaMotivoBloqueio(List<SelectItem> listaMotivoBloqueio) {
		this.listaMotivoBloqueio = listaMotivoBloqueio;
	}

	/**
	 * Get: listaMotivoBloqueioHash.
	 *
	 * @return listaMotivoBloqueioHash
	 */
	public Map<Integer, String> getListaMotivoBloqueioHash() {
		return listaMotivoBloqueioHash;
	}

	/**
	 * Set lista motivo bloqueio hash.
	 *
	 * @param listaMotivoBloqueioHash the lista motivo bloqueio hash
	 */
	public void setListaMotivoBloqueioHash(
			Map<Integer, String> listaMotivoBloqueioHash) {
		this.listaMotivoBloqueioHash = listaMotivoBloqueioHash;
	}

	/**
	 * Get: dsMotivoBloqueio.
	 *
	 * @return dsMotivoBloqueio
	 */
	public String getDsMotivoBloqueio() {
		return dsMotivoBloqueio;
	}

	/**
	 * Set: dsMotivoBloqueio.
	 *
	 * @param dsMotivoBloqueio the ds motivo bloqueio
	 */
	public void setDsMotivoBloqueio(String dsMotivoBloqueio) {
		this.dsMotivoBloqueio = dsMotivoBloqueio;
	}

	/**
	 * Get: gerenteResponsavel.
	 *
	 * @return gerenteResponsavel
	 */
	public String getGerenteResponsavel() {
		return gerenteResponsavel;
	}

	/**
	 * Set: gerenteResponsavel.
	 *
	 * @param gerenteResponsavel the gerente responsavel
	 */
	public void setGerenteResponsavel(String gerenteResponsavel) {
		this.gerenteResponsavel = gerenteResponsavel;
	}

	/**
	 * Get: clienteParticipante.
	 *
	 * @return clienteParticipante
	 */
	public String getClienteParticipante() {
		return clienteParticipante;
	}

	/**
	 * Set: clienteParticipante.
	 *
	 * @param clienteParticipante the cliente participante
	 */
	public void setClienteParticipante(String clienteParticipante) {
		this.clienteParticipante = clienteParticipante;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	
	

}
