/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManDadosBasicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManDadosBasicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManDadosBasicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManDadosBasicosSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: HistoricoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class HistoricoBean {

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;

	/** Atributo filtroIdentificaoService. */
	private IFiltroIdentificaoService filtroIdentificaoService;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

	/* Consultar */
	/** Atributo empresaGestoraContrato. */
	private Long empresaGestoraContrato;

	/** Atributo tipoContrato. */
	private Integer tipoContrato;

	/** Atributo numero. */
	private String numero;

	/** Atributo filtroDataDe. */
	private Date filtroDataDe;

	/** Atributo filtroDataAte. */
	private Date filtroDataAte;

	/* Detalhar */

	/** Atributo cpfCnpjCliente. */
	private String cpfCnpjCliente;

	/** Atributo nomeRazaoCliente. */
	private String nomeRazaoCliente;

	/** Atributo cdEmpresaGestoraContrato. */
	private Long cdEmpresaGestoraContrato;

	/** Atributo dsEempresaGestoraContrato. */
	private String dsEempresaGestoraContrato;

	/** Atributo cdTipoContrato. */
	private int cdTipoContrato;

	/** Atributo dsTipoContrato. */
	private String dsTipoContrato;

	/** Atributo numeroContrato. */
	private String numeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo cdIdiomaContrato. */
	private int cdIdiomaContrato;

	/** Atributo idiomaContrato. */
	private String idiomaContrato;

	/** Atributo cdMoedaContrato. */
	private int cdMoedaContrato;

	/** Atributo moedaContrato. */
	private String moedaContrato;

	/** Atributo dsOrigem. */
	private String dsOrigem;

	/** Atributo codOrigem. */
	private int codOrigem;

	/** Atributo dsNumProposta. */
	private String dsNumProposta;

	/** Atributo dsPossuiAditivos. */
	private String dsPossuiAditivos;

	/** Atributo dsSituacao. */
	private String dsSituacao;

	/** Atributo dsMotivo. */
	private String dsMotivo;

	/** Atributo inicioVigencia. */
	private String inicioVigencia;

	/** Atributo dsNumeroDocumentoFisicoContrato. */
	private String dsNumeroDocumentoFisicoContrato;

	/** Atributo dsParticipacao. */
	private String dsParticipacao;

	/** Atributo dataHoraAssinatura. */
	private String dataHoraAssinatura;

	/** Atributo permiteEncerramento. */
	private String permiteEncerramento;

	/** Atributo dsObsSituacao. */
	private String dsObsSituacao;

	/** Atributo dsObsAssinatura. */
	private String dsObsAssinatura;

	/** Atributo inicioVigenciaContrato. */
	private String inicioVigenciaContrato;

	/** Atributo fimVigenciaContrato. */
	private String fimVigenciaContrato;

	/** Atributo gestoraUnidOrg. */
	private String gestoraUnidOrg;

	/** Atributo gerenteResponsavelUnidOrg. */
	private String gerenteResponsavelUnidOrg;

	/** Atributo cdSetorContratoPagamento. */
	private Integer cdSetorContratoPagamento;

	/** Atributo dsSetorContratoPagamento. */
	private String dsSetorContratoPagamento;

	/** Atributo vlSaldoVirtualTeste. */
	private BigDecimal vlSaldoVirtualTeste;

	/** Atributo cdFuncionarioBradesco. */
	private Long cdFuncionarioBradesco;

	/** Atributo nmFuncionarioBradesco. */
	private String nmFuncionarioBradesco;
	
	/** Atributo tpManutencao. */
	private String tpManutencao;

	/** Atributo listaEmpresaGestora. */
	private List<SelectItem> listaEmpresaGestora = new ArrayList<SelectItem>();

	/** Atributo listaEmpresaGestoraHash. */
	private Map<Long, String> listaEmpresaGestoraHash = new HashMap<Long, String>();

	/** Atributo listaTipoContrato. */
	private List<SelectItem> listaTipoContrato = new ArrayList<SelectItem>();

	/** Atributo listaTipoContratoHash. */
	private Map<Integer, String> listaTipoContratoHash = new HashMap<Integer, String>();

	/** Atributo listaGridPesquisa. */
	private List<ListarConManDadosBasicosSaidaDTO> listaGridPesquisa;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo cdPessoaJuridicaProposta. */
	private Long cdPessoaJuridicaProposta;
	
    /** Atributo cdTipoContratoProposta. */
    private Integer cdTipoContratoProposta;
    
    /** Atributo nrContratoProposta. */
    private Long nrContratoProposta;
    
    /** Atributo dsPessoaJuridicaProposta. */
    private String dsPessoaJuridicaProposta;
    
    /** Atributo dsTipoContratoProposta. */
    private String dsTipoContratoProposta; 

	// Atributo utilizado para diferenciar se o Hist�rico � global ou para um contrato especifico selecionado na tela de
	// Consultar Contratos
	/** Atributo filtroPorContrato. */
	private boolean filtroPorContrato;

	// ATRIBUTOS TRILHA AUDITORIA
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo detalheHistoricoRepresentante. */
	private List<ListarContratosPgitSaidaDTO> detalheHistoricoRepresentante;

	/** Atributo cdEmpresaGestoraContratoFiltro. */
	private Long cdEmpresaGestoraContratoFiltro;

	/** Atributo cdTipoContratoFiltro. */
	private int cdTipoContratoFiltro;

	/** Atributo numeroContratoFiltro. */
	private String numeroContratoFiltro;
	
	/** Atributo cdDeptoGestor. */
	private Integer cdDeptoGestor;
	
	/** Atributo nmDepto. */
	private String nmDepto;
	
	/** Atributo nrSequenciaContratoOutros. */
	private Long nrSequenciaContratoOutros;
	
	/** Atributo nrSequenciaContratoPagamentoSalario. */
	private Long nrSequenciaContratoPagamentoSalario;

	/** Atributo dsFormaAutorizacaoPagamento. */
    private String dsFormaAutorizacaoPagamento = null;

	/* M�todos */
	/**
	 * Listar tipo contrato.
	 */
	public void listarTipoContrato() {

		this.listaTipoContrato = new ArrayList<SelectItem>();
		List<TipoContratoSaidaDTO> list = new ArrayList<TipoContratoSaidaDTO>();

		list = comboService.listarTipoContrato();

		listaTipoContrato.clear();
		this.listaTipoContratoHash.clear();

		for (TipoContratoSaidaDTO saida : list) {
			listaTipoContrato.add(new SelectItem(saida.getCdTipoContrato(), saida.getDsTipoContrato()));
			listaTipoContratoHash.put(saida.getCdTipoContrato(), saida.getDsTipoContrato());
		}
	}

	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora() {

		this.listaEmpresaGestora = new ArrayList<SelectItem>();
		List<EmpresaGestoraSaidaDTO> list = new ArrayList<EmpresaGestoraSaidaDTO>();

		list = comboService.listarEmpresasGestoras();

		listaEmpresaGestora.clear();
		listaEmpresaGestoraHash.clear();

		for (EmpresaGestoraSaidaDTO combo : list) {
			listaEmpresaGestora.add(new SelectItem(combo.getCdPessoaJuridicaContrato(), combo.getDsCnpj()));
			listaEmpresaGestoraHash.put(combo.getCdPessoaJuridicaContrato(), combo.getDsCnpj());
		}
	}

	/**
	 * Limpa dados.
	 *
	 * @return the string
	 */
	public String limpaDados() {
		setFiltroDataAte(new Date());
		setFiltroDataDe(new Date());
		setItemSelecionadoLista(null);
		setListaGridPesquisa(null);
		setCdEmpresaGestoraContratoFiltro(null);
		setCdTipoContratoFiltro(0);
		setNumeroContratoFiltro(null);

		return "";

	}

	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar() {
		setItemSelecionadoLista(null);
		setListaGridPesquisa(null);

		return "";
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		return "ok";
	}

	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista() {
		try {
			ListarConManDadosBasicosEntradaDTO entrada = new ListarConManDadosBasicosEntradaDTO();

			entrada.setCdPessoaJuridicaNegocio(getEmpresaGestoraContrato() == null ? new Long(0)
							: getEmpresaGestoraContrato());
			entrada.setCdTipoContratoNegocio(getTipoContrato());
			entrada.setNrSequenciaContratoNegocio(getNumero() == null || getNumero().equals("") ? Long.parseLong("0")
							: Long.parseLong(getNumero()));

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			entrada.setDtFim((getFiltroDataAte() == null) ? "" : sdf.format(getFiltroDataAte()));
			entrada.setDtInicio((getFiltroDataDe() == null) ? "" : sdf.format(getFiltroDataDe()));

			setListaGridPesquisa(getManterContratoImpl().listarConManDadosBasicos(entrada));

			this.listaControleRadio = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridPesquisa().size(); i++) {
				this.listaControleRadio.add(new SelectItem(i, " "));
			}

			setItemSelecionadoLista(null);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaGridPesquisa(null);
		}

		return "";

	}

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {
		return "VOLTAR";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		ConsultarConManDadosBasicosEntradaDTO consultarConManDadosBasicosEntradaDTO = new ConsultarConManDadosBasicosEntradaDTO();
		consultarConManDadosBasicosEntradaDTO
						.setCdPessoaJuridicaContrato(getEmpresaGestoraContrato() != null ? getEmpresaGestoraContrato()
										: 0);
		consultarConManDadosBasicosEntradaDTO.setCdTipoContratoNegocio(getTipoContrato() != null ? getTipoContrato()
						: 0);
		consultarConManDadosBasicosEntradaDTO.setNrSequenciaContratoNegocio(!getNumero().equals("") ? Long
						.parseLong(getNumero()) : 0);
		consultarConManDadosBasicosEntradaDTO.setHrInclusaoRegistro(getListaGridPesquisa().get(
						getItemSelecionadoLista()).getHrInclusaoRegistroHistorico());
		ConsultarConManDadosBasicosSaidaDTO consultarConManDadosBasicosSaidaDTO = getManterContratoImpl()
						.consultarConManDadosBasicos(consultarConManDadosBasicosEntradaDTO);

		SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
		SimpleDateFormat formato3 = new SimpleDateFormat("dd.MM.yyyy");
		SimpleDateFormat formato4 = new SimpleDateFormat("dd/MM/yyyy");

		// Contrato
		setDsEempresaGestoraContrato(consultarConManDadosBasicosSaidaDTO.getNmPessoaJuridica());
		setDsTipoContrato(consultarConManDadosBasicosSaidaDTO.getDsTipoContratoNegocio());
		setNumeroContrato(String.valueOf(consultarConManDadosBasicosSaidaDTO.getNrSequenciaContratoNegocio()));
		setDescricaoContrato(consultarConManDadosBasicosSaidaDTO.getDsContratoNegocio());
		setIdiomaContrato(consultarConManDadosBasicosSaidaDTO.getDsIdioma());
		setMoedaContrato(consultarConManDadosBasicosSaidaDTO.getDsMoeda());
		setDsOrigem(consultarConManDadosBasicosSaidaDTO.getDsOrigemContrato());
		setDsPossuiAditivos(consultarConManDadosBasicosSaidaDTO.getDsIndicadorAditivo());
		setCdPessoaJuridicaProposta(consultarConManDadosBasicosSaidaDTO.getCdPessoaJuridicaProposta());
		setCdTipoContratoProposta(consultarConManDadosBasicosSaidaDTO.getCdTipoContratoProposta());
		setNrContratoProposta(consultarConManDadosBasicosSaidaDTO.getNrContratoProposta());
	    setDsPessoaJuridicaProposta(consultarConManDadosBasicosSaidaDTO.getDsPessoaJuridicaProposta());
	    setDsTipoContratoProposta(consultarConManDadosBasicosSaidaDTO.getDsTipoContratoProposta()); 

		// Formatar Data/Hora Assinatura
		if (consultarConManDadosBasicosSaidaDTO.getHrAssinaturaContrato() != null) {

			if (consultarConManDadosBasicosSaidaDTO.getHrAssinaturaContrato() != null
							&& consultarConManDadosBasicosSaidaDTO.getHrAssinaturaContrato().length() >= 19) {

				String stringData = consultarConManDadosBasicosSaidaDTO.getHrAssinaturaContrato().substring(0, 19);
				try {
					setDataHoraAssinatura((formato4.format(formato1.parse(stringData))));
				} catch (ParseException e) {
					setDataHoraAssinatura("");
				}
			} else {
				setDataHoraAssinatura("");
			}
		}

		setDsSituacao(consultarConManDadosBasicosSaidaDTO.getDsSituacaoContrato());
		setDsMotivo(consultarConManDadosBasicosSaidaDTO.getDsMotivoSituacaoContrato());

		try {
			setInicioVigenciaContrato(String.valueOf(formato4.format(formato3.parse(consultarConManDadosBasicosSaidaDTO
							.getDtInicioVigencia()))));
		} catch (ParseException e) {
			setInicioVigenciaContrato("");
		}

		try {
			setFimVigenciaContrato(String.valueOf(formato4.format(formato3.parse(consultarConManDadosBasicosSaidaDTO
							.getDtFimVigencia()))));
		} catch (ParseException e) {
			setFimVigenciaContrato("");
		}

		setDsNumeroDocumentoFisicoContrato(consultarConManDadosBasicosSaidaDTO.getCdContratoNegocio());
		setDsParticipacao(consultarConManDadosBasicosSaidaDTO.getDsIndicadorParticipante());

		// Unidades Organizacionais Respons�veis:
		setGestoraUnidOrg(consultarConManDadosBasicosSaidaDTO.getCdDeptoGestor() + " - "
						+ consultarConManDadosBasicosSaidaDTO.getNmDepto());
		setGerenteResponsavelUnidOrg(consultarConManDadosBasicosSaidaDTO.getCdFuncionarioBradesco() + " - "
						+ consultarConManDadosBasicosSaidaDTO.getNmFuncionarioBradesco());

		// Trilha de Auditoria
		setDataHoraInclusao(consultarConManDadosBasicosSaidaDTO.getHrInclusaoRegistro());

		/*
		 * if (consultarConManDadosBasicosSaidaDTO.getHrInclusaoRegistro()!=null ){
		 * 
		 * String stringData = consultarConManDadosBasicosSaidaDTO.getHrInclusaoRegistro().substring(0, 19); try {
		 * setDataHoraInclusao((formato2.format(formato1.parse(stringData)))); } catch (ParseException e) {
		 * setDataHoraInclusao(""); } }
		 */

		setUsuarioInclusao(consultarConManDadosBasicosSaidaDTO.getCdUsuarioInclusao());
		setTipoCanalInclusao((consultarConManDadosBasicosSaidaDTO.getCdCanalInclusao() != 0) ? Integer
						.toString(consultarConManDadosBasicosSaidaDTO.getCdCanalInclusao())
						+ " - " + consultarConManDadosBasicosSaidaDTO.getDsCanalInclusao() : "");
		setComplementoInclusao(consultarConManDadosBasicosSaidaDTO.getNmOperacaoFluxoInclusao());

		setDataHoraManutencao(consultarConManDadosBasicosSaidaDTO.getHrManutencaoRegistro());

		/*
		 * if (consultarConManDadosBasicosSaidaDTO.getHrManutencaoRegistro()!=null ){
		 * 
		 * String stringData = consultarConManDadosBasicosSaidaDTO.getHrManutencaoRegistro().substring(0, 19); try {
		 * setDataHoraManutencao((formato2.format(formato1.parse(stringData)))); } catch (ParseException e) {
		 * setDataHoraManutencao(""); } }
		 */

		setUsuarioManutencao(consultarConManDadosBasicosSaidaDTO.getCdUsuarioManutencao());
		setTipoCanalManutencao((consultarConManDadosBasicosSaidaDTO.getCdCanalManutencao() != 0) ? Integer
						.toString(consultarConManDadosBasicosSaidaDTO.getCdCanalManutencao())
						+ " - " + consultarConManDadosBasicosSaidaDTO.getDsCanalManutencao() : "");
		setComplementoManutencao(consultarConManDadosBasicosSaidaDTO.getNmOperacaoFluxoManutencao());
		setDsSetorContratoPagamento(consultarConManDadosBasicosSaidaDTO.getDsSetorContratoPagamento());
		setVlSaldoVirtualTeste(consultarConManDadosBasicosSaidaDTO.getVlSaldoVirtualTeste());
		setTpManutencao(consultarConManDadosBasicosSaidaDTO.getDsIndicadorTipoManutencao());
		setCdFuncionarioBradesco(consultarConManDadosBasicosSaidaDTO.getCdFuncionarioBradesco());
		setNmFuncionarioBradesco(consultarConManDadosBasicosSaidaDTO.getNmFuncionarioBradesco());
		setCdDeptoGestor(consultarConManDadosBasicosSaidaDTO.getCdDeptoGestor());
		setNmDepto(consultarConManDadosBasicosSaidaDTO.getNmDepto());
		
		if (consultarConManDadosBasicosSaidaDTO.getNrSequenciaContratoOutros() != 0) {
			setNrSequenciaContratoOutros(consultarConManDadosBasicosSaidaDTO.getNrSequenciaContratoOutros());
		}
		
		if (consultarConManDadosBasicosSaidaDTO.getNrSequenciaContratoPagamentoSalario() != 0) {
			setNrSequenciaContratoPagamentoSalario(consultarConManDadosBasicosSaidaDTO.getNrSequenciaContratoPagamentoSalario());
		}

		setDsFormaAutorizacaoPagamento(consultarConManDadosBasicosSaidaDTO.getDsFormaAutorizacaoPagamento());

		return "DETALHAR";
	}
	
	/**
	 * Voltar historico.
	 *
	 * @return the string
	 */
	public String voltarHistorico() {
		setItemSelecionadoLista(null);
		return "VOLTAR_HISTORICO";
	}

	/* Getters e Setters */

	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: filtroDataAte.
	 *
	 * @return filtroDataAte
	 */
	public Date getFiltroDataAte() {
		return filtroDataAte;
	}

	/**
	 * Set: filtroDataAte.
	 *
	 * @param filtroDataAte the filtro data ate
	 */
	public void setFiltroDataAte(Date filtroDataAte) {
		this.filtroDataAte = filtroDataAte;
	}

	/**
	 * Get: filtroDataDe.
	 *
	 * @return filtroDataDe
	 */
	public Date getFiltroDataDe() {
		return filtroDataDe;
	}

	/**
	 * Set: filtroDataDe.
	 *
	 * @param filtroDataDe the filtro data de
	 */
	public void setFiltroDataDe(Date filtroDataDe) {
		this.filtroDataDe = filtroDataDe;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: cpfCnpjCliente.
	 *
	 * @return cpfCnpjCliente
	 */
	public String getCpfCnpjCliente() {
		return cpfCnpjCliente;
	}

	/**
	 * Set: cpfCnpjCliente.
	 *
	 * @param cpfCnpjCliente the cpf cnpj cliente
	 */
	public void setCpfCnpjCliente(String cpfCnpjCliente) {
		this.cpfCnpjCliente = cpfCnpjCliente;
	}

	/**
	 * Get: nomeRazaoCliente.
	 *
	 * @return nomeRazaoCliente
	 */
	public String getNomeRazaoCliente() {
		return nomeRazaoCliente;
	}

	/**
	 * Set: nomeRazaoCliente.
	 *
	 * @param nomeRazaoCliente the nome razao cliente
	 */
	public void setNomeRazaoCliente(String nomeRazaoCliente) {
		this.nomeRazaoCliente = nomeRazaoCliente;
	}

	/**
	 * Get: cdEmpresaGestoraContrato.
	 *
	 * @return cdEmpresaGestoraContrato
	 */
	public Long getCdEmpresaGestoraContrato() {
		return cdEmpresaGestoraContrato;
	}

	/**
	 * Set: cdEmpresaGestoraContrato.
	 *
	 * @param cdEmpresaGestoraContrato the cd empresa gestora contrato
	 */
	public void setCdEmpresaGestoraContrato(Long cdEmpresaGestoraContrato) {
		this.cdEmpresaGestoraContrato = cdEmpresaGestoraContrato;
	}

	/**
	 * Get: cdIdiomaContrato.
	 *
	 * @return cdIdiomaContrato
	 */
	public int getCdIdiomaContrato() {
		return cdIdiomaContrato;
	}

	/**
	 * Set: cdIdiomaContrato.
	 *
	 * @param cdIdiomaContrato the cd idioma contrato
	 */
	public void setCdIdiomaContrato(int cdIdiomaContrato) {
		this.cdIdiomaContrato = cdIdiomaContrato;
	}

	/**
	 * Get: cdMoedaContrato.
	 *
	 * @return cdMoedaContrato
	 */
	public int getCdMoedaContrato() {
		return cdMoedaContrato;
	}

	/**
	 * Set: cdMoedaContrato.
	 *
	 * @param cdMoedaContrato the cd moeda contrato
	 */
	public void setCdMoedaContrato(int cdMoedaContrato) {
		this.cdMoedaContrato = cdMoedaContrato;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public int getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(int cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: idiomaContrato.
	 *
	 * @return idiomaContrato
	 */
	public String getIdiomaContrato() {
		return idiomaContrato;
	}

	/**
	 * Set: idiomaContrato.
	 *
	 * @param idiomaContrato the idioma contrato
	 */
	public void setIdiomaContrato(String idiomaContrato) {
		this.idiomaContrato = idiomaContrato;
	}

	/**
	 * Get: moedaContrato.
	 *
	 * @return moedaContrato
	 */
	public String getMoedaContrato() {
		return moedaContrato;
	}

	/**
	 * Set: moedaContrato.
	 *
	 * @param moedaContrato the moeda contrato
	 */
	public void setMoedaContrato(String moedaContrato) {
		this.moedaContrato = moedaContrato;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: dsEempresaGestoraContrato.
	 *
	 * @return dsEempresaGestoraContrato
	 */
	public String getDsEempresaGestoraContrato() {
		return dsEempresaGestoraContrato;
	}

	/**
	 * Set: dsEempresaGestoraContrato.
	 *
	 * @param dsEempresaGestoraContrato the ds eempresa gestora contrato
	 */
	public void setDsEempresaGestoraContrato(String dsEempresaGestoraContrato) {
		this.dsEempresaGestoraContrato = dsEempresaGestoraContrato;
	}

	/**
	 * Get: dsTipoContrato.
	 *
	 * @return dsTipoContrato
	 */
	public String getDsTipoContrato() {
		return dsTipoContrato;
	}

	/**
	 * Set: dsTipoContrato.
	 *
	 * @param dsTipoContrato the ds tipo contrato
	 */
	public void setDsTipoContrato(String dsTipoContrato) {
		this.dsTipoContrato = dsTipoContrato;
	}

	/**
	 * Get: codOrigem.
	 *
	 * @return codOrigem
	 */
	public int getCodOrigem() {
		return codOrigem;
	}

	/**
	 * Set: codOrigem.
	 *
	 * @param codOrigem the cod origem
	 */
	public void setCodOrigem(int codOrigem) {
		this.codOrigem = codOrigem;
	}

	/**
	 * Get: dataHoraAssinatura.
	 *
	 * @return dataHoraAssinatura
	 */
	public String getDataHoraAssinatura() {
		return dataHoraAssinatura;
	}

	/**
	 * Set: dataHoraAssinatura.
	 *
	 * @param dataHoraAssinatura the data hora assinatura
	 */
	public void setDataHoraAssinatura(String dataHoraAssinatura) {
		this.dataHoraAssinatura = dataHoraAssinatura;
	}

	/**
	 * Get: dsMotivo.
	 *
	 * @return dsMotivo
	 */
	public String getDsMotivo() {
		return dsMotivo;
	}

	/**
	 * Set: dsMotivo.
	 *
	 * @param dsMotivo the ds motivo
	 */
	public void setDsMotivo(String dsMotivo) {
		this.dsMotivo = dsMotivo;
	}

	/**
	 * Get: dsNumProposta.
	 *
	 * @return dsNumProposta
	 */
	public String getDsNumProposta() {
		return dsNumProposta;
	}

	/**
	 * Set: dsNumProposta.
	 *
	 * @param dsNumProposta the ds num proposta
	 */
	public void setDsNumProposta(String dsNumProposta) {
		this.dsNumProposta = dsNumProposta;
	}

	/**
	 * Get: dsObsAssinatura.
	 *
	 * @return dsObsAssinatura
	 */
	public String getDsObsAssinatura() {
		return dsObsAssinatura;
	}

	/**
	 * Set: dsObsAssinatura.
	 *
	 * @param dsObsAssinatura the ds obs assinatura
	 */
	public void setDsObsAssinatura(String dsObsAssinatura) {
		this.dsObsAssinatura = dsObsAssinatura;
	}

	/**
	 * Get: dsObsSituacao.
	 *
	 * @return dsObsSituacao
	 */
	public String getDsObsSituacao() {
		return dsObsSituacao;
	}

	/**
	 * Set: dsObsSituacao.
	 *
	 * @param dsObsSituacao the ds obs situacao
	 */
	public void setDsObsSituacao(String dsObsSituacao) {
		this.dsObsSituacao = dsObsSituacao;
	}

	/**
	 * Get: dsOrigem.
	 *
	 * @return dsOrigem
	 */
	public String getDsOrigem() {
		return dsOrigem;
	}

	/**
	 * Set: dsOrigem.
	 *
	 * @param dsOrigem the ds origem
	 */
	public void setDsOrigem(String dsOrigem) {
		this.dsOrigem = dsOrigem;
	}

	/**
	 * Get: dsParticipacao.
	 *
	 * @return dsParticipacao
	 */
	public String getDsParticipacao() {
		return dsParticipacao;
	}

	/**
	 * Set: dsParticipacao.
	 *
	 * @param dsParticipacao the ds participacao
	 */
	public void setDsParticipacao(String dsParticipacao) {
		this.dsParticipacao = dsParticipacao;
	}

	/**
	 * Get: dsPossuiAditivos.
	 *
	 * @return dsPossuiAditivos
	 */
	public String getDsPossuiAditivos() {
		return dsPossuiAditivos;
	}

	/**
	 * Set: dsPossuiAditivos.
	 *
	 * @param dsPossuiAditivos the ds possui aditivos
	 */
	public void setDsPossuiAditivos(String dsPossuiAditivos) {
		this.dsPossuiAditivos = dsPossuiAditivos;
	}

	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}

	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}

	/**
	 * Get: inicioVigencia.
	 *
	 * @return inicioVigencia
	 */
	public String getInicioVigencia() {
		return inicioVigencia;
	}

	/**
	 * Set: inicioVigencia.
	 *
	 * @param inicioVigencia the inicio vigencia
	 */
	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	/**
	 * Get: permiteEncerramento.
	 *
	 * @return permiteEncerramento
	 */
	public String getPermiteEncerramento() {
		return permiteEncerramento;
	}

	/**
	 * Set: permiteEncerramento.
	 *
	 * @param permiteEncerramento the permite encerramento
	 */
	public void setPermiteEncerramento(String permiteEncerramento) {
		this.permiteEncerramento = permiteEncerramento;
	}

	/**
	 * Get: listaTipoContrato.
	 *
	 * @return listaTipoContrato
	 */
	public List<SelectItem> getListaTipoContrato() {
		return listaTipoContrato;
	}

	/**
	 * Set: listaTipoContrato.
	 *
	 * @param listaTipoContrato the lista tipo contrato
	 */
	public void setListaTipoContrato(List<SelectItem> listaTipoContrato) {
		this.listaTipoContrato = listaTipoContrato;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<ListarConManDadosBasicosSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(List<ListarConManDadosBasicosSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Is filtro por contrato.
	 *
	 * @return true, if is filtro por contrato
	 */
	public boolean isFiltroPorContrato() {
		return filtroPorContrato;
	}

	/**
	 * Set: filtroPorContrato.
	 *
	 * @param filtroPorContrato the filtro por contrato
	 */
	public void setFiltroPorContrato(boolean filtroPorContrato) {
		this.filtroPorContrato = filtroPorContrato;
	}

	/**
	 * Get: fimVigenciaContrato.
	 *
	 * @return fimVigenciaContrato
	 */
	public String getFimVigenciaContrato() {
		return fimVigenciaContrato;
	}

	/**
	 * Set: fimVigenciaContrato.
	 *
	 * @param fimVigenciaContrato the fim vigencia contrato
	 */
	public void setFimVigenciaContrato(String fimVigenciaContrato) {
		this.fimVigenciaContrato = fimVigenciaContrato;
	}

	/**
	 * Get: inicioVigenciaContrato.
	 *
	 * @return inicioVigenciaContrato
	 */
	public String getInicioVigenciaContrato() {
		return inicioVigenciaContrato;
	}

	/**
	 * Set: inicioVigenciaContrato.
	 *
	 * @param inicioVigenciaContrato the inicio vigencia contrato
	 */
	public void setInicioVigenciaContrato(String inicioVigenciaContrato) {
		this.inicioVigenciaContrato = inicioVigenciaContrato;
	}

	/**
	 * Get: dsNumeroDocumentoFisicoContrato.
	 *
	 * @return dsNumeroDocumentoFisicoContrato
	 */
	public String getDsNumeroDocumentoFisicoContrato() {
		return dsNumeroDocumentoFisicoContrato;
	}

	/**
	 * Set: dsNumeroDocumentoFisicoContrato.
	 *
	 * @param dsNumeroDocumentoFisicoContrato the ds numero documento fisico contrato
	 */
	public void setDsNumeroDocumentoFisicoContrato(String dsNumeroDocumentoFisicoContrato) {
		this.dsNumeroDocumentoFisicoContrato = dsNumeroDocumentoFisicoContrato;
	}

	/**
	 * Get: gerenteResponsavelUnidOrg.
	 *
	 * @return gerenteResponsavelUnidOrg
	 */
	public String getGerenteResponsavelUnidOrg() {
		return gerenteResponsavelUnidOrg;
	}

	/**
	 * Set: gerenteResponsavelUnidOrg.
	 *
	 * @param gerenteResponsavelUnidOrg the gerente responsavel unid org
	 */
	public void setGerenteResponsavelUnidOrg(String gerenteResponsavelUnidOrg) {
		this.gerenteResponsavelUnidOrg = gerenteResponsavelUnidOrg;
	}

	/**
	 * Get: gestoraUnidOrg.
	 *
	 * @return gestoraUnidOrg
	 */
	public String getGestoraUnidOrg() {
		return gestoraUnidOrg;
	}

	/**
	 * Set: gestoraUnidOrg.
	 *
	 * @param gestoraUnidOrg the gestora unid org
	 */
	public void setGestoraUnidOrg(String gestoraUnidOrg) {
		this.gestoraUnidOrg = gestoraUnidOrg;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: filtroIdentificaoService.
	 *
	 * @return filtroIdentificaoService
	 */
	public IFiltroIdentificaoService getFiltroIdentificaoService() {
		return filtroIdentificaoService;
	}

	/**
	 * Set: filtroIdentificaoService.
	 *
	 * @param filtroIdentificaoService the filtro identificao service
	 */
	public void setFiltroIdentificaoService(IFiltroIdentificaoService filtroIdentificaoService) {
		this.filtroIdentificaoService = filtroIdentificaoService;
	}

	/**
	 * Get: detalheHistoricoRepresentante.
	 *
	 * @return detalheHistoricoRepresentante
	 */
	public List<ListarContratosPgitSaidaDTO> getDetalheHistoricoRepresentante() {
		return detalheHistoricoRepresentante;
	}

	/**
	 * Set: detalheHistoricoRepresentante.
	 *
	 * @param detalheHistoricoRepresentante the detalhe historico representante
	 */
	public void setDetalheHistoricoRepresentante(List<ListarContratosPgitSaidaDTO> detalheHistoricoRepresentante) {
		this.detalheHistoricoRepresentante = detalheHistoricoRepresentante;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: empresaGestoraContrato.
	 *
	 * @return empresaGestoraContrato
	 */
	public Long getEmpresaGestoraContrato() {
		return empresaGestoraContrato;
	}

	/**
	 * Set: empresaGestoraContrato.
	 *
	 * @param empresaGestoraContrato the empresa gestora contrato
	 */
	public void setEmpresaGestoraContrato(Long empresaGestoraContrato) {
		this.empresaGestoraContrato = empresaGestoraContrato;
	}

	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public Integer getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(Integer tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: cdEmpresaGestoraContratoFiltro.
	 *
	 * @return cdEmpresaGestoraContratoFiltro
	 */
	public Long getCdEmpresaGestoraContratoFiltro() {
		return cdEmpresaGestoraContratoFiltro;
	}

	/**
	 * Set: cdEmpresaGestoraContratoFiltro.
	 *
	 * @param cdEmpresaGestoraContratoFiltro the cd empresa gestora contrato filtro
	 */
	public void setCdEmpresaGestoraContratoFiltro(Long cdEmpresaGestoraContratoFiltro) {
		this.cdEmpresaGestoraContratoFiltro = cdEmpresaGestoraContratoFiltro;
	}

	/**
	 * Get: cdTipoContratoFiltro.
	 *
	 * @return cdTipoContratoFiltro
	 */
	public int getCdTipoContratoFiltro() {
		return cdTipoContratoFiltro;
	}

	/**
	 * Set: cdTipoContratoFiltro.
	 *
	 * @param cdTipoContratoFiltro the cd tipo contrato filtro
	 */
	public void setCdTipoContratoFiltro(int cdTipoContratoFiltro) {
		this.cdTipoContratoFiltro = cdTipoContratoFiltro;
	}

	/**
	 * Get: numeroContratoFiltro.
	 *
	 * @return numeroContratoFiltro
	 */
	public String getNumeroContratoFiltro() {
		return numeroContratoFiltro;
	}

	/**
	 * Set: numeroContratoFiltro.
	 *
	 * @param numeroContratoFiltro the numero contrato filtro
	 */
	public void setNumeroContratoFiltro(String numeroContratoFiltro) {
		this.numeroContratoFiltro = numeroContratoFiltro;
	}

	/**
	 * Get: listaEmpresaGestoraHash.
	 *
	 * @return listaEmpresaGestoraHash
	 */
	public Map<Long, String> getListaEmpresaGestoraHash() {
		return listaEmpresaGestoraHash;
	}

	/**
	 * Set lista empresa gestora hash.
	 *
	 * @param listaEmpresaGestoraHash the lista empresa gestora hash
	 */
	public void setListaEmpresaGestoraHash(Map<Long, String> listaEmpresaGestoraHash) {
		this.listaEmpresaGestoraHash = listaEmpresaGestoraHash;
	}

	/**
	 * Get: listaTipoContratoHash.
	 *
	 * @return listaTipoContratoHash
	 */
	public Map<Integer, String> getListaTipoContratoHash() {
		return listaTipoContratoHash;
	}

	/**
	 * Set lista tipo contrato hash.
	 *
	 * @param listaTipoContratoHash the lista tipo contrato hash
	 */
	public void setListaTipoContratoHash(Map<Integer, String> listaTipoContratoHash) {
		this.listaTipoContratoHash = listaTipoContratoHash;
	}

	/**
	 * Get: cdSetorContratoPagamento.
	 *
	 * @return cdSetorContratoPagamento
	 */
	public Integer getCdSetorContratoPagamento() {
		return cdSetorContratoPagamento;
	}

	/**
	 * Set: cdSetorContratoPagamento.
	 *
	 * @param cdSetorContratoPagamento the cd setor contrato pagamento
	 */
	public void setCdSetorContratoPagamento(Integer cdSetorContratoPagamento) {
		this.cdSetorContratoPagamento = cdSetorContratoPagamento;
	}

	/**
	 * Get: dsSetorContratoPagamento.
	 *
	 * @return dsSetorContratoPagamento
	 */
	public String getDsSetorContratoPagamento() {
		return dsSetorContratoPagamento;
	}

	/**
	 * Set: dsSetorContratoPagamento.
	 *
	 * @param dsSetorContratoPagamento the ds setor contrato pagamento
	 */
	public void setDsSetorContratoPagamento(String dsSetorContratoPagamento) {
		this.dsSetorContratoPagamento = dsSetorContratoPagamento;
	}

	/**
	 * Get: vlSaldoVirtualTeste.
	 *
	 * @return vlSaldoVirtualTeste
	 */
	public BigDecimal getVlSaldoVirtualTeste() {
		return vlSaldoVirtualTeste;
	}

	/**
	 * Set: vlSaldoVirtualTeste.
	 *
	 * @param vlSaldoVirtualTeste the vl saldo virtual teste
	 */
	public void setVlSaldoVirtualTeste(BigDecimal vlSaldoVirtualTeste) {
		this.vlSaldoVirtualTeste = vlSaldoVirtualTeste;
	}
	
	/**
	 * Get: tpManutencao.
	 *
	 * @return tpManutencao
	 */
	public String getTpManutencao() {
		return tpManutencao;
	}

	/**
	 * Set: tpManutencao.
	 *
	 * @param tpManutencao the tp manutencao
	 */
	public void setTpManutencao(String tpManutencao) {
		this.tpManutencao = tpManutencao;
	}

	/**
	 * Get: cdFuncionarioBradesco.
	 *
	 * @return cdFuncionarioBradesco
	 */
	public Long getCdFuncionarioBradesco() {
		return cdFuncionarioBradesco;
	}

	/**
	 * Set: cdFuncionarioBradesco.
	 *
	 * @param cdFuncionarioBradesco the cd funcionario bradesco
	 */
	public void setCdFuncionarioBradesco(Long cdFuncionarioBradesco) {
		this.cdFuncionarioBradesco = cdFuncionarioBradesco;
	}

	/**
	 * Get: nmFuncionarioBradesco.
	 *
	 * @return nmFuncionarioBradesco
	 */
	public String getNmFuncionarioBradesco() {
		return nmFuncionarioBradesco;
	}

	/**
	 * Set: nmFuncionarioBradesco.
	 *
	 * @param nmFuncionarioBradesco the nm funcionario bradesco
	 */
	public void setNmFuncionarioBradesco(String nmFuncionarioBradesco) {
		this.nmFuncionarioBradesco = nmFuncionarioBradesco;
	}

	/**
	 * Get: funcionarioBradescoFormatado.
	 *
	 * @return funcionarioBradescoFormatado
	 */
	public String getFuncionarioBradescoFormatado() {
		return cdFuncionarioBradesco + " - " + nmFuncionarioBradesco;
	}

	/**
	 * Get: cdPessoaJuridicaProposta.
	 *
	 * @return cdPessoaJuridicaProposta
	 */
	public Long getCdPessoaJuridicaProposta() {
		return cdPessoaJuridicaProposta;
	}

	/**
	 * Set: cdPessoaJuridicaProposta.
	 *
	 * @param cdPessoaJuridicaProposta the cd pessoa juridica proposta
	 */
	public void setCdPessoaJuridicaProposta(Long cdPessoaJuridicaProposta) {
		this.cdPessoaJuridicaProposta = cdPessoaJuridicaProposta;
	}

	/**
	 * Get: cdTipoContratoProposta.
	 *
	 * @return cdTipoContratoProposta
	 */
	public Integer getCdTipoContratoProposta() {
		return cdTipoContratoProposta;
	}

	/**
	 * Set: cdTipoContratoProposta.
	 *
	 * @param cdTipoContratoProposta the cd tipo contrato proposta
	 */
	public void setCdTipoContratoProposta(Integer cdTipoContratoProposta) {
		this.cdTipoContratoProposta = cdTipoContratoProposta;
	}

	/**
	 * Get: nrContratoProposta.
	 *
	 * @return nrContratoProposta
	 */
	public Long getNrContratoProposta() {
		return nrContratoProposta;
	}

	/**
	 * Set: nrContratoProposta.
	 *
	 * @param nrContratoProposta the nr contrato proposta
	 */
	public void setNrContratoProposta(Long nrContratoProposta) {
		this.nrContratoProposta = nrContratoProposta;
	}

	/**
	 * Get: dsPessoaJuridicaProposta.
	 *
	 * @return dsPessoaJuridicaProposta
	 */
	public String getDsPessoaJuridicaProposta() {
		return dsPessoaJuridicaProposta;
	}

	/**
	 * Set: dsPessoaJuridicaProposta.
	 *
	 * @param dsPessoaJuridicaProposta the ds pessoa juridica proposta
	 */
	public void setDsPessoaJuridicaProposta(String dsPessoaJuridicaProposta) {
		this.dsPessoaJuridicaProposta = dsPessoaJuridicaProposta;
	}

	/**
	 * Get: dsTipoContratoProposta.
	 *
	 * @return dsTipoContratoProposta
	 */
	public String getDsTipoContratoProposta() {
		return dsTipoContratoProposta;
	}

	/**
	 * Set: dsTipoContratoProposta.
	 *
	 * @param dsTipoContratoProposta the ds tipo contrato proposta
	 */
	public void setDsTipoContratoProposta(String dsTipoContratoProposta) {
		this.dsTipoContratoProposta = dsTipoContratoProposta;
	}

	/**
	 * Set: cdDeptoGestor.
	 *
	 * @param cdDeptoGestor the cd depto gestor
	 */
	public void setCdDeptoGestor(Integer cdDeptoGestor) {
		this.cdDeptoGestor = cdDeptoGestor;
	}

	/**
	 * Get: cdDeptoGestor.
	 *
	 * @return cdDeptoGestor
	 */
	public Integer getCdDeptoGestor() {
		return cdDeptoGestor;
	}

	/**
	 * Set: nmDepto.
	 *
	 * @param nmDepto the nm depto
	 */
	public void setNmDepto(String nmDepto) {
		this.nmDepto = nmDepto;
	}

	/**
	 * Get: nmDepto.
	 *
	 * @return nmDepto
	 */
	public String getNmDepto() {
		return nmDepto;
	}

	/**
	 * @param nrSequenciaContratoOutros the nrSequenciaContratoOutros to set
	 */
	public void setNrSequenciaContratoOutros(Long nrSequenciaContratoOutros) {
		this.nrSequenciaContratoOutros = nrSequenciaContratoOutros;
	}

	/**
	 * @return the nrSequenciaContratoOutros
	 */
	public Long getNrSequenciaContratoOutros() {
		return nrSequenciaContratoOutros;
	}

	/**
	 * @param nrSequenciaContratoPagamentoSalario the nrSequenciaContratoPagamentoSalario to set
	 */
	public void setNrSequenciaContratoPagamentoSalario(
			Long nrSequenciaContratoPagamentoSalario) {
		this.nrSequenciaContratoPagamentoSalario = nrSequenciaContratoPagamentoSalario;
	}

	/**
	 * @return the nrSequenciaContratoPagamentoSalario
	 */
	public Long getNrSequenciaContratoPagamentoSalario() {
		return nrSequenciaContratoPagamentoSalario;
	}

    /**
     * Nome: getDsFormaAutorizacaoPagamento
     *
     * @return dsFormaAutorizacaoPagamento
     */
    public String getDsFormaAutorizacaoPagamento() {
        return dsFormaAutorizacaoPagamento;
    }

    /**
     * Nome: setDsFormaAutorizacaoPagamento
     *
     * @param dsFormaAutorizacaoPagamento
     */
    public void setDsFormaAutorizacaoPagamento(String dsFormaAutorizacaoPagamento) {
        this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    }
}