package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantermodalidadesnetempresa;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.IManterModalidadesNetEmpresaService;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.IManterModalidadesNetEmpresaServiceConstants;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.AlterarHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.AlterarHorarioLimiteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.DetalharHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.DetalharHorarioLimiteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ExcluirHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ExcluirHorarioLimiteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ListarHorarioLimiteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ListarHorarioLimiteOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermodalidadesnetempresa.bean.ListarHorarioLimiteSaidaDTO;

public class ManterModalidadesNetEmpresaBean {

	private IManterModalidadesNetEmpresaService manterModalidadesNetEmpresaService = null;

	/** Atributo listaGridPesquisa. */
	private List<ListarHorarioLimiteOcorrenciasSaidaDTO> listaGridPesquisa;

	/** Atributo entradaAlterarHistLiquidacaoPagto. */
	private AlterarHorarioLimiteEntradaDTO entradaHorarioLimiteAlterar = new AlterarHorarioLimiteEntradaDTO();

	private ListarHorarioLimiteSaidaDTO listarHorarioLimite;

	private ListarHorarioLimiteOcorrenciasSaidaDTO ocorrenciasListarHorarioLimite;

	private DetalharHorarioLimiteSaidaDTO saidaDetalharHorarioLimite;

	/** Atributo itemSelecionadoLista. */
	private String itemSelecionadoLista;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	private AlterarHorarioLimiteEntradaDTO entradaAlterarHorarioLimite = new AlterarHorarioLimiteEntradaDTO();

	/** Atributo funcoesAlcadaImpl. */

	/** Atributo CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO. */
	private static final String MANTER_MOD_NET_EMPRESA = "manterModalidadesNetEmpresa";
	
	/** Atributo CON_FORMA_LIQUIDACAO_PAGAMENTOS_INTEGRADO. */
	private static final String ALTERAR_MOD_NET_EMPRESA = "alterarManterModalidadesNetEmpresa";

	public ManterModalidadesNetEmpresaBean() {
		super();
	}

	public String iniciarTela() {
		carregarGrid();
		setItemSelecionadoLista(null);
		return "manterModalidadesNetEmpresa";
	}

	public String detalharModalidadesNetEmpresa() {

		this.setSaidaDetalharHorarioLimite(new DetalharHorarioLimiteSaidaDTO());
		DetalharHorarioLimiteEntradaDTO entrada = new DetalharHorarioLimiteEntradaDTO();

		 entrada.setCdModalidade(listaGridPesquisa.get(Integer.parseInt(itemSelecionadoLista)).getCdModalidade());

		try {
			saidaDetalharHorarioLimite = manterModalidadesNetEmpresaService.detalharHorarioLimite(entrada);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), MANTER_MOD_NET_EMPRESA,
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "detalharManterModalidadesNetEmpresa";
	}

	public String alterarModalidadesNetEmpresa() {
		setEntradaAlterarHorarioLimite(new AlterarHorarioLimiteEntradaDTO());
		ListarHorarioLimiteOcorrenciasSaidaDTO listarHorarioLimiteOcorrenciasSaidaDTO = listaGridPesquisa.get(Integer.valueOf(itemSelecionadoLista));

		entradaHorarioLimiteAlterar.setCdModalidade(listarHorarioLimiteOcorrenciasSaidaDTO.getCdModalidade());
		entradaHorarioLimiteAlterar.setDsModalidade(listarHorarioLimiteOcorrenciasSaidaDTO.getDsModalidade());
		entradaHorarioLimiteAlterar.setHrLimiteAutorizacao(listarHorarioLimiteOcorrenciasSaidaDTO.getHrLimiteAutorizacao());
		entradaHorarioLimiteAlterar.setHrLimiteTransmissao(listarHorarioLimiteOcorrenciasSaidaDTO.getHrLimiteTransmissao());
		entradaHorarioLimiteAlterar.setCdSequenciaTipoModalidade(listarHorarioLimiteOcorrenciasSaidaDTO.getCdSequenciaTipoModalidade());
		entradaHorarioLimiteAlterar.setCdSituacaoTipoModalidade(listarHorarioLimiteOcorrenciasSaidaDTO.getCdSituacaoTipoModalidade());
		entradaHorarioLimiteAlterar.setHrLimiteManutencao(listarHorarioLimiteOcorrenciasSaidaDTO.getHrLimiteManutencao());
		
		return "alterarManterModalidadesNetEmpresa";
	}

	public String avancarAlterarLiqPagtoIntegrado() {

		return "alterarManterModalidadesNetEmpresa2";
	}

	/**
	 * Confirmar alteracao liq pagto integrado. 
	 * 
	 * @return the string
	 */

	public String confirmarAlteracaoManterModalidadesNet() {
		
		entradaHorarioLimiteAlterar.setCdModalidade(entradaHorarioLimiteAlterar.getCdModalidade());
		entradaHorarioLimiteAlterar.setDsModalidade(entradaHorarioLimiteAlterar.getDsModalidade());
		entradaHorarioLimiteAlterar.setHrLimiteAutorizacao(entradaHorarioLimiteAlterar.getHrLimiteAutorizacao());
		entradaHorarioLimiteAlterar.setHrLimiteTransmissao(entradaHorarioLimiteAlterar.getHrLimiteTransmissao());
		entradaHorarioLimiteAlterar.setCdSequenciaTipoModalidade(entradaHorarioLimiteAlterar.getCdSequenciaTipoModalidade());
		entradaHorarioLimiteAlterar.setCdSituacaoTipoModalidade(entradaHorarioLimiteAlterar.getCdSituacaoTipoModalidade());
		entradaHorarioLimiteAlterar.setHrLimiteManutencao(entradaHorarioLimiteAlterar.getHrLimiteManutencao());
		

		try {
			AlterarHorarioLimiteSaidaDTO saida = manterModalidadesNetEmpresaService.alterarHorarioLimite(this.entradaHorarioLimiteAlterar);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(), MANTER_MOD_NET_EMPRESA,
					BradescoViewExceptionActionType.ACTION, false);
		} catch (PdcAdapterException p) {

			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					"altFormaLiquidacaoPagamentoIntegrado2",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		} finally {
			carregarGrid();
		}

		return "alterarManterModalidadesNetEmpresa2";
	}

	public String excluirModalidadeNetEmpresa() {

		ocorrenciasListarHorarioLimite = new ListarHorarioLimiteOcorrenciasSaidaDTO();
		this.setOcorrenciasListarHorarioLimite(listaGridPesquisa.get(Integer.parseInt(itemSelecionadoLista)));

		return "excluirManterModalidadesNetEmpresa";
	}
	


	/**
	 * Voltar inicio.
	 * 
	 * @return the string
	 */
	public String voltarInicio() {

		return MANTER_MOD_NET_EMPRESA;
	}
	
	/**
	 * Voltar inicio.
	 * 
	 * @return the string
	 */
	public String voltarAlterar() {

		return ALTERAR_MOD_NET_EMPRESA;
	}

	/**
	 * Confirmar exclusao liq pagto integrado.
	 * 
	 * @return the string
	 */
	public String confirmarExclusaoModalidadeNetEmpresa() {

		ExcluirHorarioLimiteEntradaDTO entradaExcluirHoraLimite = new ExcluirHorarioLimiteEntradaDTO();
		ExcluirHorarioLimiteSaidaDTO saida = new ExcluirHorarioLimiteSaidaDTO();

		entradaExcluirHoraLimite.setCdModalidade(ocorrenciasListarHorarioLimite.getCdModalidade());
		entradaExcluirHoraLimite.setCdSequenciaTipoModalidade(ocorrenciasListarHorarioLimite.getCdSequenciaTipoModalidade());
		entradaExcluirHoraLimite.setCdSituacaoTipoModalidade(ocorrenciasListarHorarioLimite.getCdSituacaoTipoModalidade());
		entradaExcluirHoraLimite.setDsModalidade(ocorrenciasListarHorarioLimite.getDsModalidade());
		entradaExcluirHoraLimite.setHrLimiteAutorizacao(ocorrenciasListarHorarioLimite.getHrLimiteAutorizacao());
		entradaExcluirHoraLimite.setHrLimiteTransmissao(ocorrenciasListarHorarioLimite.getHrLimiteTransmissao());
		entradaExcluirHoraLimite.setHrLimiteManutencao(ocorrenciasListarHorarioLimite.getHrLimiteManutencao());
		try {
			saida = manterModalidadesNetEmpresaService
					.excluirHorarioLimite(entradaExcluirHoraLimite);
			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
					"#{manterModalidadesNetEmpresaBean.consultarAposExclusao}",
					BradescoViewExceptionActionType.ACTION, false);

		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), MANTER_MOD_NET_EMPRESA,
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "excluirManterModalidadesNetEmpresa";
	}

	public String consultarAposExclusao() {
		try {
			listaGridPesquisa = new ArrayList<ListarHorarioLimiteOcorrenciasSaidaDTO>();
			ListarHorarioLimiteEntradaDTO entradaDTO = new ListarHorarioLimiteEntradaDTO();

			entradaDTO
					.setNumeroOcorrencias(IManterModalidadesNetEmpresaServiceConstants.MAX_OCORRENCIAS_100);
			listarHorarioLimite = manterModalidadesNetEmpresaService
					.listarHorarioLimite(entradaDTO);
			listaGridPesquisa = listarHorarioLimite.getOcorrencias();
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			setListaGridPesquisa(null);
		}
		return MANTER_MOD_NET_EMPRESA;
	}

	private void carregarGrid() {

		try {
			listaGridPesquisa = new ArrayList<ListarHorarioLimiteOcorrenciasSaidaDTO>();
			ListarHorarioLimiteEntradaDTO entradaDTO = new ListarHorarioLimiteEntradaDTO();

			entradaDTO.setNumeroOcorrencias(IManterModalidadesNetEmpresaServiceConstants.MAX_OCORRENCIAS_100);
			listarHorarioLimite = manterModalidadesNetEmpresaService.listarHorarioLimite(entradaDTO);
			listaGridPesquisa = listarHorarioLimite.getOcorrencias();
			setItemSelecionadoLista(null);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridPesquisa(null);

		}

	}

	public List<SelectItem> getSelecionaModalidadesNetEmpresa() {

		if (this.listaGridPesquisa == null || this.listaGridPesquisa.isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.listaGridPesquisa.size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/**
	 * Paginar orgao publico.
	 */
	public void paginarModalidadeNetEmpresa() {
		carregarGrid();
	}

	/**
	 * @param manterModalidadesNetEmpresaService
	 *            the manterModalidadesNetEmpresaService to set
	 */
	public void setManterModalidadesNetEmpresaService(
			IManterModalidadesNetEmpresaService manterModalidadesNetEmpresaService) {
		this.manterModalidadesNetEmpresaService = manterModalidadesNetEmpresaService;
	}

	/**
	 * @return the manterModalidadesNetEmpresaService
	 */
	public IManterModalidadesNetEmpresaService getManterModalidadesNetEmpresaService() {
		return manterModalidadesNetEmpresaService;
	}

	/**
	 * @return the listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * @param listaControleRadio
	 *            the listaControleRadio to set
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * @param itemSelecionadoLista
	 *            the itemSelecionadoLista to set
	 */
	public void setItemSelecionadoLista(String itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * @return the itemSelecionadoLista
	 */
	public String getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * @param ocorrenciasaidaListarHorarioLimite
	 *            the ocorrenciasaidaListarHorarioLimite to set
	 */
	public void setListarHorarioLimite(
			ListarHorarioLimiteSaidaDTO listarHorarioLimite) {
		this.listarHorarioLimite = listarHorarioLimite;
	}

	/**
	 * @return the ocorrenciasaidaListarHorarioLimite
	 */
	public ListarHorarioLimiteSaidaDTO getListarHorarioLimite() {
		return listarHorarioLimite;
	}

	/**
	 * @param saidaDetalharHorarioLimite
	 *            the saidaDetalharHorarioLimite to set
	 */
	public void setSaidaDetalharHorarioLimite(
			DetalharHorarioLimiteSaidaDTO saidaDetalharHorarioLimite) {
		this.saidaDetalharHorarioLimite = saidaDetalharHorarioLimite;
	}

	/**
	 * @return the saidaDetalharHorarioLimite
	 */
	public DetalharHorarioLimiteSaidaDTO getSaidaDetalharHorarioLimite() {
		return saidaDetalharHorarioLimite;
	}

	/**
	 * @param entradaAlterarHorarioLimite
	 *            the entradaAlterarHorarioLimite to set
	 */
	public void setEntradaAlterarHorarioLimite(
			AlterarHorarioLimiteEntradaDTO entradaAlterarHorarioLimite) {
		this.entradaAlterarHorarioLimite = entradaAlterarHorarioLimite;
	}

	/**
	 * @return the entradaAlterarHorarioLimite
	 */
	public AlterarHorarioLimiteEntradaDTO getEntradaAlterarHorarioLimite() {
		return entradaAlterarHorarioLimite;
	}

	/**
	 * @return the listaGridPesquisa
	 */
	public List<ListarHorarioLimiteOcorrenciasSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * @param listaGridPesquisa
	 *            the listaGridPesquisa to set
	 */
	public void setListaGridPesquisa(
			List<ListarHorarioLimiteOcorrenciasSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * @param ocorrenciasListarHorarioLimite
	 *            the ocorrenciasListarHorarioLimite to set
	 */
	public void setOcorrenciasListarHorarioLimite(
			ListarHorarioLimiteOcorrenciasSaidaDTO ocorrenciasListarHorarioLimite) {
		this.ocorrenciasListarHorarioLimite = ocorrenciasListarHorarioLimite;
	}

	/**
	 * @return the ocorrenciasListarHorarioLimite
	 */
	public ListarHorarioLimiteOcorrenciasSaidaDTO getOcorrenciasListarHorarioLimite() {
		return ocorrenciasListarHorarioLimite;
	}

	/**
	 * @param entradaHorarioLimiteAlterar
	 *            the entradaHorarioLimiteAlterar to set
	 */
	public void setEntradaHorarioLimiteAlterar(
			AlterarHorarioLimiteEntradaDTO entradaHorarioLimiteAlterar) {
		this.entradaHorarioLimiteAlterar = entradaHorarioLimiteAlterar;
	}

	/**
	 * @return the entradaHorarioLimiteAlterar
	 */
	public AlterarHorarioLimiteEntradaDTO getEntradaHorarioLimiteAlterar() {
		return entradaHorarioLimiteAlterar;
	}
}
