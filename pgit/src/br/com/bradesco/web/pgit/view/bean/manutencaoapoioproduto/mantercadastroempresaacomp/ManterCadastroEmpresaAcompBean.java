/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantercadastroempresaacomp
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantercadastroempresaacomp;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.IManterCadastroEmpresaAcompService;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ConsultarEmpresaAcompanhadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ConsultarEmpresaAcompanhadaListaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ConsultarEmpresaAcompanhadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastroacomp.bean.ManterEmpresaAcompanhadaEntradaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ManterCadastroEmpresaAcompBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterCadastroEmpresaAcompBean {
	
	/** Atributo itemFiltroSelecionado. */
	private String itemFiltroSelecionado;
	
	/** Atributo cdCnpj. */
	private Long cdCnpj;
	
	/** Atributo cdFilialCnpj. */
	private Integer cdFilialCnpj;
	
	/** Atributo cdControleCnpj. */
	private Integer cdControleCnpj;
	
	/** Atributo cdAgenciaBancaria. */
	private Integer cdAgenciaBancaria;
	
	/** Atributo cdContaBancaria. */
	private Integer cdContaBancaria;
	
	/** Atributo cdDigitoConta. */
	private Integer cdDigitoConta;
	
	/** Atributo razaoSocial. */
	private String razaoSocial;
	
	/** Atributo manterCadastroEmpresaAcompServiceImpl. */
	private IManterCadastroEmpresaAcompService manterCadastroEmpresaAcompServiceImpl;
	
	/** Atributo saidaConsulta. */
	private ConsultarEmpresaAcompanhadaSaidaDTO saidaConsulta;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo cdIndicadorSimNao. */
	private String cdIndicadorSimNao;
	
	/** Atributo manterCadastro. */
	private ManterEmpresaAcompanhadaEntradaDTO manterCadastro;
	
	/** Atributo manterDto. */
	private ConsultarEmpresaAcompanhadaListaDTO manterDto;
	
	/** Atributo ativarBotoes. */
	private boolean ativarBotoes;
	
	/** Atributo entrada. */
	private ConsultarEmpresaAcompanhadaEntradaDTO entrada;
	
	

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		limpar();
	}

	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar() {
		itemFiltroSelecionado = null;
		setSaidaConsulta(new ConsultarEmpresaAcompanhadaSaidaDTO());
		getSaidaConsulta().setOcorrencias(new ArrayList<ConsultarEmpresaAcompanhadaListaDTO>());
		setListaControleRadio(new ArrayList<SelectItem>());
		limparFiltro();
		setAtivarBotoes(false);
		cdIndicadorSimNao = null;
		return "conManterCadastroEmpresaAcomp";
	}

	/**
	 * Limpar filtro.
	 */
	public void limparFiltro() {
		setCdCnpj(null);
		setCdFilialCnpj(null);
		setCdControleCnpj(null);
		setCdAgenciaBancaria(null);
		setCdContaBancaria(null);
		setCdDigitoConta(null);
		setRazaoSocial(null);
	}

	/**
	 * Pesquisar.
	 *
	 * @return the string
	 */
	public String pesquisar() {
		entrada = new ConsultarEmpresaAcompanhadaEntradaDTO();
		entrada.setCdIndicadorPesquisa(itemFiltroSelecionado == null ? "4" : itemFiltroSelecionado);
		entrada.setCdCnpjCpf(PgitUtil.verificaLongNulo(cdCnpj));
		entrada.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(cdFilialCnpj));
		entrada.setCdCtrlCnpjCPf(PgitUtil.verificaIntegerNulo(cdControleCnpj));
		entrada.setDsRazaoSocial(PgitUtil.verificaStringNula(razaoSocial));
		entrada.setCdJuncaoAgencia(PgitUtil.verificaIntegerNulo(cdAgenciaBancaria));
		entrada.setCdContaCorrente(PgitUtil.verificaIntegerNulo(cdContaBancaria));
		entrada.setCdDigitoConta(cdDigitoConta == null ? "" : cdDigitoConta.toString());
		setAtivarBotoes(true);
		setSaidaConsulta(manterCadastroEmpresaAcompServiceImpl.consultarEmpresaAcompanhada(entrada));
		this.listaControleRadio = new ArrayList<SelectItem>();
		for (int i = 0; i <= getSaidaConsulta().getOcorrencias().size(); i++) {
			this.listaControleRadio.add(new SelectItem(i, " "));
		}
		return "";
	}
	
	/**
	 * Paginar pesquisar.
	 *
	 * @param e the e
	 */
	public void paginarPesquisar(ActionEvent e){
		pesquisar();
	}
	

	/**
	 * Preencher dados manutencao.
	 */
	private void preencherDadosManutencao() {
		manterCadastro = new ManterEmpresaAcompanhadaEntradaDTO();
		manterCadastro.setCdCnpjCpf(PgitUtil.verificaLongNulo(manterDto.getCdCnpjCpf()));
		manterCadastro.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(manterDto.getCdFilialCpfCnpj()));
		manterCadastro.setCdCtrlCnpjCPf(PgitUtil.verificaIntegerNulo(manterDto.getCdCtrlCpfCnpjSaida()));
		manterCadastro.setDsRazaoSocial(PgitUtil.verificaStringNula(manterDto.getDsRazaoSocial()));
		manterCadastro.setCdJuncaoAgencia(PgitUtil.verificaIntegerNulo(manterDto.getCdAgenciaSaida()));
		manterCadastro.setCdContaCorrente(PgitUtil.verificaIntegerNulo(manterDto.getCdContaCorrenteSaida()));
		manterCadastro.setCdDigitoConta(manterDto.getCdDigitoConta() == null ? "" : manterDto.getCdDigitoConta().toString());
		manterCadastro.setCdConveCtaSalarial(PgitUtil.verificaLongNulo(manterDto.getCdConveCtaSalarial()));
		manterCadastro.setCdIndicadorPrevidencia(PgitUtil.verificaStringNula(cdIndicadorSimNao));
	}

	/**
	 * Limpar manter.
	 *
	 * @return the string
	 */
	public String limparManter() {
		setManterDto(new ConsultarEmpresaAcompanhadaListaDTO());
		cdIndicadorSimNao = null;
		return "";
	}
	
	/**
	 * Limpar alterar.
	 *
	 * @return the string
	 */
	public String limparAlterar() {
		getManterDto().setDsRazaoSocial(null);
		getManterDto().setCdAgenciaSaida(null);
		getManterDto().setCdContaCorrenteSaida(null);
		getManterDto().setCdDigitoConta(null);
		getManterDto().setCdConveCtaSalarial(null);
		cdIndicadorSimNao = null;
		return "";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		manterDto = new ConsultarEmpresaAcompanhadaListaDTO();
		manterDto = getSaidaConsulta().getOcorrencias().get(itemSelecionadoLista);
		return "DETALHAR";
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		manterDto = new ConsultarEmpresaAcompanhadaListaDTO();
		cdIndicadorSimNao = null;
		return "INCLUIR";
	}

	/**
	 * Consistir incluir.
	 *
	 * @return the string
	 */
	public String consistirIncluir() {
		try {
			manterDto.setCdIndicadorPrevidencia(PgitUtil.verificaStringNula(cdIndicadorSimNao));
			preencherDadosManutencao();
			manterCadastroEmpresaAcompServiceImpl.consistirDadosLogicos(manterCadastro);

			return "CONFIRMAR_INCLUIR";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {
		try {
			String msg = manterCadastroEmpresaAcompServiceImpl.incluirEmpresaAcompanhada(manterCadastro);
			BradescoFacesUtils.addInfoModalMessage(msg, "#{manterCadastroEmpresaAcompBean.pesquisar}", BradescoViewExceptionActionType.ACTION, false);
			return "conManterCadastroEmpresaAcomp";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
	}

	/**
	 * Avancar excluir.
	 *
	 * @return the string
	 */
	public String avancarExcluir() {
		manterDto = new ConsultarEmpresaAcompanhadaListaDTO();
		manterDto = getSaidaConsulta().getOcorrencias().get(itemSelecionadoLista);
		return "EXCLUIR";
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {
		try {
			preencherDadosManutencao();
			String msg = manterCadastroEmpresaAcompServiceImpl.excluirEmpresaAcompanhada(manterCadastro);
			BradescoFacesUtils.addInfoModalMessage(msg, "#{manterCadastroEmpresaAcompBean.pesquisar}", BradescoViewExceptionActionType.ACTION, false);
			return "conManterCadastroEmpresaAcomp";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {
		manterDto = new ConsultarEmpresaAcompanhadaListaDTO();
		manterDto = getSaidaConsulta().getOcorrencias().get(itemSelecionadoLista);
		cdIndicadorSimNao = manterDto.getCdIndicadorPrevidencia();
		preencherDadosManutencao();
		return "ALTERAR";
	}

	/**
	 * Consistir alterar.
	 *
	 * @return the string
	 */
	public String consistirAlterar() {
		try {
			preencherDadosManutencao();
			manterCadastroEmpresaAcompServiceImpl.consistirDadosLogicos(manterCadastro);
			return "CONFIRMAR_ALTERAR";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {
		try {
			String msg = manterCadastroEmpresaAcompServiceImpl.alterarEmpresaAcompanhada(manterCadastro);
			BradescoFacesUtils.addInfoModalMessage(msg, "#{manterCadastroEmpresaAcompBean.pesquisar}", BradescoViewExceptionActionType.ACTION, false);
			return "conManterCadastroEmpresaAcomp";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
	}

	/**
	 * Get: itemFiltroSelecionado.
	 *
	 * @return itemFiltroSelecionado
	 */
	public String getItemFiltroSelecionado() {
		return itemFiltroSelecionado;
	}

	/**
	 * Set: itemFiltroSelecionado.
	 *
	 * @param itemFiltroSelecionado the item filtro selecionado
	 */
	public void setItemFiltroSelecionado(String itemFiltroSelecionado) {
		this.itemFiltroSelecionado = itemFiltroSelecionado;
	}

	/**
	 * Get: manterCadastroEmpresaAcompServiceImpl.
	 *
	 * @return manterCadastroEmpresaAcompServiceImpl
	 */
	public IManterCadastroEmpresaAcompService getManterCadastroEmpresaAcompServiceImpl() {
		return manterCadastroEmpresaAcompServiceImpl;
	}

	/**
	 * Set: manterCadastroEmpresaAcompServiceImpl.
	 *
	 * @param manterCadastroEmpresaAcompServiceImpl the manter cadastro empresa acomp service impl
	 */
	public void setManterCadastroEmpresaAcompServiceImpl(IManterCadastroEmpresaAcompService manterCadastroEmpresaAcompServiceImpl) {
		this.manterCadastroEmpresaAcompServiceImpl = manterCadastroEmpresaAcompServiceImpl;
	}

	/**
	 * Get: cdCnpj.
	 *
	 * @return cdCnpj
	 */
	public Long getCdCnpj() {
		return cdCnpj;
	}

	/**
	 * Set: cdCnpj.
	 *
	 * @param cdCnpj the cd cnpj
	 */
	public void setCdCnpj(Long cdCnpj) {
		this.cdCnpj = cdCnpj;
	}

	/**
	 * Get: cdFilialCnpj.
	 *
	 * @return cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}

	/**
	 * Set: cdFilialCnpj.
	 *
	 * @param cdFilialCnpj the cd filial cnpj
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}

	/**
	 * Get: cdControleCnpj.
	 *
	 * @return cdControleCnpj
	 */
	public Integer getCdControleCnpj() {
		return cdControleCnpj;
	}

	/**
	 * Set: cdControleCnpj.
	 *
	 * @param cdControleCnpj the cd controle cnpj
	 */
	public void setCdControleCnpj(Integer cdControleCnpj) {
		this.cdControleCnpj = cdControleCnpj;
	}

	/**
	 * Get: cdAgenciaBancaria.
	 *
	 * @return cdAgenciaBancaria
	 */
	public Integer getCdAgenciaBancaria() {
		return cdAgenciaBancaria;
	}

	/**
	 * Set: cdAgenciaBancaria.
	 *
	 * @param cdAgenciaBancaria the cd agencia bancaria
	 */
	public void setCdAgenciaBancaria(Integer cdAgenciaBancaria) {
		this.cdAgenciaBancaria = cdAgenciaBancaria;
	}

	/**
	 * Get: cdContaBancaria.
	 *
	 * @return cdContaBancaria
	 */
	public Integer getCdContaBancaria() {
		return cdContaBancaria;
	}

	/**
	 * Set: cdContaBancaria.
	 *
	 * @param cdContaBancaria the cd conta bancaria
	 */
	public void setCdContaBancaria(Integer cdContaBancaria) {
		this.cdContaBancaria = cdContaBancaria;
	}

	/**
	 * Get: cdDigitoConta.
	 *
	 * @return cdDigitoConta
	 */
	public Integer getCdDigitoConta() {
		return cdDigitoConta;
	}

	/**
	 * Set: cdDigitoConta.
	 *
	 * @param cdDigitoConta the cd digito conta
	 */
	public void setCdDigitoConta(Integer cdDigitoConta) {
		this.cdDigitoConta = cdDigitoConta;
	}

	/**
	 * Get: razaoSocial.
	 *
	 * @return razaoSocial
	 */
	public String getRazaoSocial() {
		return razaoSocial;
	}

	/**
	 * Set: razaoSocial.
	 *
	 * @param razaoSocial the razao social
	 */
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	/**
	 * Get: saidaConsulta.
	 *
	 * @return saidaConsulta
	 */
	public ConsultarEmpresaAcompanhadaSaidaDTO getSaidaConsulta() {
		return saidaConsulta;
	}

	/**
	 * Set: saidaConsulta.
	 *
	 * @param saidaConsulta the saida consulta
	 */
	public void setSaidaConsulta(ConsultarEmpresaAcompanhadaSaidaDTO saidaConsulta) {
		this.saidaConsulta = saidaConsulta;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: manterCadastro.
	 *
	 * @return manterCadastro
	 */
	public ManterEmpresaAcompanhadaEntradaDTO getManterCadastro() {
		return manterCadastro;
	}

	/**
	 * Set: manterCadastro.
	 *
	 * @param manterCadastro the manter cadastro
	 */
	public void setManterCadastro(ManterEmpresaAcompanhadaEntradaDTO manterCadastro) {
		this.manterCadastro = manterCadastro;
	}

	/**
	 * Get: cdIndicadorSimNao.
	 *
	 * @return cdIndicadorSimNao
	 */
	public String getCdIndicadorSimNao() {
		return cdIndicadorSimNao;
	}

	/**
	 * Set: cdIndicadorSimNao.
	 *
	 * @param cdIndicadorSimNao the cd indicador sim nao
	 */
	public void setCdIndicadorSimNao(String cdIndicadorSimNao) {
		this.cdIndicadorSimNao = cdIndicadorSimNao;
	}

	/**
	 * Get: manterDto.
	 *
	 * @return manterDto
	 */
	public ConsultarEmpresaAcompanhadaListaDTO getManterDto() {
		return manterDto;
	}

	/**
	 * Set: manterDto.
	 *
	 * @param manterDto the manter dto
	 */
	public void setManterDto(ConsultarEmpresaAcompanhadaListaDTO manterDto) {
		this.manterDto = manterDto;
	}

	/**
	 * Is ativar botoes.
	 *
	 * @return true, if is ativar botoes
	 */
	public boolean isAtivarBotoes() {
		return ativarBotoes;
	}

	/**
	 * Set: ativarBotoes.
	 *
	 * @param ativarBotoes the ativar botoes
	 */
	public void setAtivarBotoes(boolean ativarBotoes) {
		this.ativarBotoes = ativarBotoes;
	}

	/**
	 * Get: entrada.
	 *
	 * @return entrada
	 */
	public ConsultarEmpresaAcompanhadaEntradaDTO getEntrada() {
		return entrada;
	}

	/**
	 * Set: entrada.
	 *
	 * @param entrada the entrada
	 */
	public void setEntrada(ConsultarEmpresaAcompanhadaEntradaDTO entrada) {
		this.entrada = entrada;
	}

}