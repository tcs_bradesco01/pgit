/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterSolRelPagamentos
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterSolRelPagamentos;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarDependenciaDiretoriaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarDependenciaEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarParticipanteContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.ConsultarParticipanteContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.DetalharSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.DetalharSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncSoliRelatorioPagamentoValidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncSoliRelatorioPagamentoValidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncluirSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.IncluirSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.ISolRelContratosService;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ExcluirSoliRelatorioPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrelcontratos.bean.ExcluirSoliRelatorioPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: SolRelPagamentosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolRelPagamentosBean {
	
	/** Atributo manterSolicitacaoRelatorioContratoImpl. */
	private ISolRelContratosService manterSolicitacaoRelatorioContratoImpl;
	
	/** Atributo CON_MANTER_SOLICITACAO_REL_PGTO. */
	private static final String CON_MANTER_SOLICITACAO_REL_PGTO = "conManterSolicitacaoPagamentos";
	
	/** Atributo INC_MANTER_SOLICITACAO_REL_PGTO. */
	private static final String INC_MANTER_SOLICITACAO_REL_PGTO = "incManterSolicitacaoPagamentos";
	
	/** Atributo filtroTipoRelatorio. */
	private Integer filtroTipoRelatorio;
	
	/** Atributo filtroDataDe. */
	private Date filtroDataDe;
	
	/** Atributo filtroDataAte. */
	private Date filtroDataAte;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo listaGrid. */
	private List<ListarSoliRelatorioPagamentoSaidaDTO> listaGrid =  new ArrayList<ListarSoliRelatorioPagamentoSaidaDTO>();
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo listaDiretoriaRegional. */
	private List<SelectItem> listaDiretoriaRegional = new ArrayList<SelectItem>();
	
	/** Atributo listaDiretoriaRegionalHash. */
	private Map listaDiretoriaRegionalHash = new HashMap<Integer,String>();
	
	/** Atributo listaGerenciaRegional. */
	private List<SelectItem> listaGerenciaRegional = new ArrayList<SelectItem>();
	
	/** Atributo listaGerenciaRegionalHash. */
	private Map<Integer,String> listaGerenciaRegionalHash = new HashMap<Integer,String>();
	
	/** Atributo itemSelecionadoPgto. */
	private Integer itemSelecionadoPgto;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo filtroIdentificaoService. */
	private IFiltroIdentificaoService filtroIdentificaoService;
	
	/** Atributo consultarPagamentosIndividualServiceImpl. */
	private IConsultarPagamentosIndividualService consultarPagamentosIndividualServiceImpl;
	
	/** Atributo saidaDetalhePagamento. */
	private DetalharSoliRelatorioPagamentoSaidaDTO saidaDetalhePagamento;
	
	/** Atributo saidaExcluirPagamento. */
	private ExcluirSoliRelatorioPagamentoSaidaDTO saidaExcluirPagamento;
	
	/** Atributo entradaValidarIncluirPgto. */
	private IncSoliRelatorioPagamentoValidacaoEntradaDTO entradaValidarIncluirPgto;
	
	/** Atributo saidaValidarIncluirPgto. */
	private IncSoliRelatorioPagamentoValidacaoSaidaDTO saidaValidarIncluirPgto;
	
	/** Atributo entradaIncluirPgto. */
	private IncluirSoliRelatorioPagamentoEntradaDTO entradaIncluirPgto;
	
	/** Atributo saidaIncluirPgto. */
	private IncluirSoliRelatorioPagamentoSaidaDTO saidaIncluirPgto;
	
	/** Atributo criterioSelecaoContratos. */
	private Integer criterioSelecaoContratos ;
	
	/** Atributo nivelConsolidacao. */
	private Integer nivelConsolidacao;
	
	/** Atributo itemCheckEmpresaConglomerado. */
	private Boolean itemCheckEmpresaConglomerado;
	
	/** Atributo itemCheckDiretoriaRegional. */
	private Boolean itemCheckDiretoriaRegional;
	
	/** Atributo itemCheckGerenciaRegional. */
	private Boolean itemCheckGerenciaRegional;
	
	/** Atributo itemCheckAgenciaOperadora. */
	private Boolean itemCheckAgenciaOperadora;
	
	/** Atributo itemCheckSegmento. */
	private Boolean itemCheckSegmento;
	
	/** Atributo itemCheckParticipanteGrupoEconomico. */
	private Boolean itemCheckParticipanteGrupoEconomico;
	
	/** Atributo itemCheckTipoServico. */
	private Boolean itemCheckTipoServico;
	
	/** Atributo itemCheckModalidadeServico. */
	private Boolean itemCheckModalidadeServico;
	
	/** Atributo itemCheckClasse. */
	private Boolean itemCheckClasse;
	
	/** Atributo hiddenObrigatoriedade. */
	private String hiddenObrigatoriedade;	
	
	/** Atributo tipoServico. */
	private Integer tipoServico;
	
	/** Atributo tipoRelatorio. */
	private Integer tipoRelatorio = 1;
	
	/** Atributo tipoRelatorioDesc. */
	private String tipoRelatorioDesc;
	
	/** Atributo modalidadeServico. */
	private Integer modalidadeServico;	
	
	/** Atributo voltarPesquisar. */
	private String voltarPesquisar;	
	
	/** Atributo empresa. */
	private Long empresa;
	
	/** Atributo diretoriaRegional. */
	private Integer diretoriaRegional;
	
	/** Atributo gerenciaRegional. */
	private Integer gerenciaRegional;
	
	/** Atributo agenciaOperadora. */
	private String agenciaOperadora;
	
	/** Atributo agenciaDigitada. */
	private String agenciaDigitada;
	
	/** Atributo dsAgencia. */
	private String dsAgencia;
	
	/** Atributo dsAgenciaAux. */
	private String dsAgenciaAux;
	
	/** Atributo segmento. */
	private Integer segmento;	
	
	/** Atributo participante. */
	private Long participante;
	
	/** Atributo participanteDesc. */
	private String participanteDesc;
	
	/** Atributo tipoServicoDesc. */
	private String tipoServicoDesc;
	
	/** Atributo modalidadeServiceDesc. */
	private String modalidadeServiceDesc;
	
	/** Atributo empresaDesc. */
	private String empresaDesc;
	
	/** Atributo diretoriaRegionalDesc. */
	private String diretoriaRegionalDesc;
	
	/** Atributo gerenciaRegionalDesc. */
	private String gerenciaRegionalDesc;
	
	/** Atributo segmentoDesc. */
	private String segmentoDesc;
	
	/** Atributo dataHoraSolicitacao. */
	private String dataHoraSolicitacao;	
	
	/** Atributo codigoParticipanteFiltro. */
	private Long codigoParticipanteFiltro;
	
	/** Atributo nomeParticipanteFiltro. */
	private String nomeParticipanteFiltro;
	
	/** Atributo cpfCnpjParticipanteFiltro. */
	private String cpfCnpjParticipanteFiltro;
	
	/** Atributo corpoCpfCnpjParticipanteFiltro. */
	private Long corpoCpfCnpjParticipanteFiltro;
	
	/** Atributo filialCpfCnpjParticipanteFiltro. */
	private Integer filialCpfCnpjParticipanteFiltro;
	
	/** Atributo controleCpfCnpjParticipanteFiltro. */
	private Integer controleCpfCnpjParticipanteFiltro;
	
	/** Atributo listaParticipantes. */
	private List<ConsultarParticipanteContratoSaidaDTO>  listaParticipantes = new ArrayList<ConsultarParticipanteContratoSaidaDTO>();
	
	/** Atributo listaParticipantesControleRadio. */
	private List<SelectItem> listaParticipantesControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionadoListaParcipantes. */
	private Integer itemSelecionadoListaParcipantes;
	
	/** Atributo voltarParticipante. */
	private String voltarParticipante;
	
	/** Atributo cdBancoContaDebito. */
	private Integer cdBancoContaDebito;
	
	/** Atributo cdAgenciaBancariaContaDebito. */
	private Integer cdAgenciaBancariaContaDebito;
	
	/** Atributo cdContaBancariaContaDebito. */
	private Long cdContaBancariaContaDebito;
	
	/** Atributo cdDigitoContaContaDebito. */
	private String cdDigitoContaContaDebito;
	
	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;
	
	/** Atributo modalidadeFiltro. */
	private Integer modalidadeFiltro;
	
	/** Atributo listaModalidadeFiltro. */
	private List<SelectItem> listaModalidadeFiltro = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeHash. */
	private Map<Integer,String> listaModalidadeHash = new HashMap<Integer,String>();
	
	/** Atributo listaTipoServicoFiltro. */
	private List<SelectItem> listaTipoServicoFiltro = new ArrayList<SelectItem>();	
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer,String> listaTipoServicoHash = new HashMap<Integer,String>();
	
	/** Atributo dataInicioMov. */
	private String dataInicioMov;
	
	/** Atributo dataFimMov. */
	private String dataFimMov;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo empresaGestoraFiltro. */
	private Long empresaGestoraFiltro;
	
	/** Atributo tipoContratoFiltro. */
	private Integer tipoContratoFiltro;
	
	/** Atributo numeroFiltro. */
	private String numeroFiltro;
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta = false;
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsultaTipoServico = false;
	
	/** Atributo listaEmpresaGestora. */
	private List<SelectItem> listaEmpresaGestora= new ArrayList<SelectItem>();
	
	/** Atributo listaEmpresaGestoraHash. */
	private Map<Long,String> listaEmpresaGestoraHash = new HashMap<Long,String>();
	
	/** Atributo listaTipoContrato. */
	private List<SelectItem> listaTipoContrato = new ArrayList<SelectItem>();
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio; 
	
	/** Atributo clienteContratoSelecionado. */
	private boolean clienteContratoSelecionado;
	
	/** Atributo listaConsultarListaContratosPessoas. */
	private List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoas;
	
	/** Atributo itemClienteSelecionadoContrato. */
	private Integer itemClienteSelecionadoContrato;
	
	/** Atributo cpfCnpjFormatado. */
	private String cpfCnpjFormatado;
	
	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;
	
	/** Atributo empresaGestoraDescContrato. */
	private String empresaGestoraDescContrato;
	
	/** Atributo numeroDescContrato. */
	private String numeroDescContrato;
	
	/** Atributo descricaoTipoContratoDescContrato. */
	private String descricaoTipoContratoDescContrato;
	
	/** Atributo descricaoContratoDescContrato. */
	private String descricaoContratoDescContrato;
	
	/** Atributo situacaoDescContrato. */
	private String situacaoDescContrato;
	
	/** Atributo dataInicialPagamentoFiltro. */
	private Date dataInicialPagamentoFiltro;
	
	/** Atributo dataFinalPagamentoFiltro. */
	private Date dataFinalPagamentoFiltro;
	
	/** Atributo chkParticipanteContrato. */
	private boolean chkParticipanteContrato;
	
	private boolean chkContrato;	
	
	/** Atributo chkContaDebito. */
	private boolean chkContaDebito;
	
	/** Atributo chkServicoModalidade. */
	private boolean chkServicoModalidade;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo numContratoDetInc. */
	private String numContratoDetInc;
	
	/** Atributo descServicoDetInc. */
	private String descServicoDetInc;
	
	/** Atributo descModalidadeDetInc. */
	private String descModalidadeDetInc;
	
	/** Atributo agenciaDebitoDetInc. */
	private String agenciaDebitoDetInc;
	
	/** Atributo contaDebitoDetInc. */
	private String contaDebitoDetInc;
	
	/** Atributo periodoDeDetInc. */
	private String periodoDeDetInc;
	
	/** Atributo ateDetInc. */
	private String ateDetInc;
	
	/** Atributo cpfCpnjDetInc. */
	private String cpfCpnjDetInc;
	
	/** Atributo razaoSocialDetInc. */
	private String razaoSocialDetInc;
	
	
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		setFiltroDataAte(new Date());
		setFiltroDataDe(new Date());
		setFiltroTipoRelatorio(0);
		setListaGrid(null);
		setBtoAcionado(false);
	}
	
	/**
	 * Listar relatorio pagamento.
	 *
	 * @return the string
	 */
	public String listarRelatorioPagamento(){
			
		ListarSoliRelatorioPagamentoEntradaDTO entradaListarSolRelPagamento = new ListarSoliRelatorioPagamentoEntradaDTO();
			
		entradaListarSolRelPagamento.setDtInicioSolicitacao(FormatarData.formataDiaMesAno(getFiltroDataDe()));
		entradaListarSolRelPagamento.setDtFimSolicitacao(FormatarData.formataDiaMesAno(getFiltroDataAte()));
		entradaListarSolRelPagamento.setCdTipoSolicitacao(getFiltroTipoRelatorio()!=null ?getFiltroTipoRelatorio():0);
		

		setListaGrid(getManterSolicitacaoRelatorioContratoImpl().listarRelatorioPagamento(entradaListarSolRelPagamento));
		setBtoAcionado(true);
		
		this.listaControleRadio =  new ArrayList<SelectItem>();
		for (int i = 0; i <= getListaGrid().size();i++) {
			this.listaControleRadio.add(new SelectItem(i," "));
		}
		
		setItemSelecionadoPgto(null);

		return "";
	}
	
	/**
	 * Avancar detalhar.
	 *
	 * @return the string
	 */
	public String avancarDetalhar(){
		ListarSoliRelatorioPagamentoSaidaDTO listarPgtoSaida = getListaGrid().get(getItemSelecionadoPgto());

		DetalharSoliRelatorioPagamentoEntradaDTO entradaDetalhar = new DetalharSoliRelatorioPagamentoEntradaDTO();
		entradaDetalhar.setCdSolicitacaoPagamento(listarPgtoSaida.getCdSolicitacaoPagamento());
		entradaDetalhar.setNrSolicitacao(listarPgtoSaida.getNrSolicitacao());
		
		try {
			saidaDetalhePagamento = getManterSolicitacaoRelatorioContratoImpl().detalharRelatorioPagamento(entradaDetalhar);

			if (isCodigoValido(saidaDetalhePagamento.getCdTipoCanalInclusao())){
				setTipoCanalInclusao(PgitUtil.concatenarCampos(saidaDetalhePagamento.getCdTipoCanalInclusao(), saidaDetalhePagamento.getDsCanalInclusao()));	
			}

			if (isCodigoValido(saidaDetalhePagamento.getCdTipoCanalManutencao())){
				setTipoCanalManutencao(PgitUtil.concatenarCampos(saidaDetalhePagamento.getCdTipoCanalManutencao(), saidaDetalhePagamento.getDsCanalManutencao()));
			}
			
			saidaDetalhePagamento.setCodAgenciaDesc("0".equals(saidaDetalhePagamento.getCodAgenciaDesc()) ? null : saidaDetalhePagamento.getCodAgenciaDesc());
			saidaDetalhePagamento.setContaDigito("0".equals(saidaDetalhePagamento.getContaDigito()) ? null : saidaDetalhePagamento.getContaDigito());
			
			setDataInicioMov(FormatarData.formatarData(saidaDetalhePagamento.getDtInicioPerMovimentacao()));
			setDataFimMov(FormatarData.formatarData(saidaDetalhePagamento.getDsFimPerMovimentacao()));
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		} 
	
		return "AVANCAR_DETALHAR";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
			// Tela de Filtro
            listarEmpresaGestora();
			listarTipoContrato();

			limparCamposIncluir();
			setDisableArgumentosConsulta(false);
			
			// Carregamento dos combos
			listarTipoServico();

            setClienteContratoSelecionado(false);
			setEmpresaGestoraFiltro(2269651L);
			
			setTipoRelatorio(1);
			
		return "AVANCAR_INCLUIR";
	}
	
	/**
	 * Validar det incluir.
	 *
	 * @return the string
	 */
	public String validarDetIncluir(){
		entradaValidarIncluirPgto = new IncSoliRelatorioPagamentoValidacaoEntradaDTO();
		
		entradaValidarIncluirPgto.setTpRelatorio(PgitUtil.verificaIntegerNulo(getTipoRelatorio()));
	    entradaValidarIncluirPgto.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(getEmpresaGestoraFiltro()));
	    entradaValidarIncluirPgto.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(getTipoContratoFiltro()));
	    if(getNumeroFiltro() != null && !getNumeroFiltro().equals("")){
	    	entradaValidarIncluirPgto.setNrSequenciaContratoNegocio(Long.parseLong(getNumeroFiltro()));	    	
	    }else{
	    	entradaValidarIncluirPgto.setNrSequenciaContratoNegocio(0L);
	    }
	    entradaValidarIncluirPgto.setDtIniPagamento(FormatarData.formataDiaMesAno(getDataInicialPagamentoFiltro()));
	    entradaValidarIncluirPgto.setDtFimPagamento(FormatarData.formataDiaMesAno(getDataFinalPagamentoFiltro()));
	    entradaValidarIncluirPgto.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(getCorpoCpfCnpjParticipanteFiltro()));
	    entradaValidarIncluirPgto.setCdFilialCnpjParticipante(PgitUtil.verificaIntegerNulo(getFilialCpfCnpjParticipanteFiltro()));
	    entradaValidarIncluirPgto.setCdControleCpfParticipante(PgitUtil.verificaIntegerNulo(getControleCpfCnpjParticipanteFiltro()));
	    entradaValidarIncluirPgto.setCdPessoa(0l);
	    entradaValidarIncluirPgto.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(getCdAgenciaBancariaContaDebito()));
	    entradaValidarIncluirPgto.setCdContaDebito(PgitUtil.verificaLongNulo(getCdContaBancariaContaDebito()));
	    entradaValidarIncluirPgto.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(getTipoServicoFiltro()));
	    entradaValidarIncluirPgto.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(getModalidadeFiltro()));
	    entradaValidarIncluirPgto.setCdRelacionamentoProduto(1);
	    
	    try{
	    	saidaValidarIncluirPgto = getManterSolicitacaoRelatorioContratoImpl().validarIncluirPagamento(entradaValidarIncluirPgto);
	    	preencheValidacao();
	    	
	    	return "AVANCAR_DET_INCLUSAO";
	    	
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		} 
	    
	}
	
	/**
	 * Preenche validacao.
	 */
	public void preencheValidacao(){
		if(getTipoRelatorio() != null && getTipoRelatorio() == 1){
    		setTipoServicoDesc("Anal�tico");
    	}else{
    		setTipoServicoDesc("Estat�stico");
    	}
    	
    	StringBuilder contratoCompleto = new StringBuilder();
    	contratoCompleto.append(PgitUtil.verificaLongNulo(getEmpresaGestoraFiltro()));
    	contratoCompleto.append(PgitUtil.verificaIntegerNulo(getTipoContratoFiltro()));
    	contratoCompleto.append(PgitUtil.verificaStringNula(getNumeroFiltro()));
    	setNumContratoDetInc(contratoCompleto.toString());
    	
    	setDescServicoDetInc(PgitUtil.obterSelectItemSelecionado(listaTipoServicoFiltro, getTipoServicoFiltro()));
    	setDescModalidadeDetInc(PgitUtil.obterSelectItemSelecionado(listaModalidadeFiltro, getModalidadeFiltro()));
    	
    	setCpfCpnjDetInc(getCpfCnpjParticipanteFiltro());
    	setRazaoSocialDetInc(getNomeParticipanteFiltro());
    	
    	setAgenciaDebitoDetInc("0".equals(saidaValidarIncluirPgto.getCodAgenciaDesc()) ? null : saidaValidarIncluirPgto.getCodAgenciaDesc());
    	setContaDebitoDetInc("0".equals(saidaValidarIncluirPgto.getContaDigito()) ? null : saidaValidarIncluirPgto.getContaDigito());
    	
        setPeriodoDeDetInc(FormatarData.formataDiaMesAno(getDataInicialPagamentoFiltro()));
	    setAteDetInc(FormatarData.formataDiaMesAno(getDataFinalPagamentoFiltro()));
    	
	}
	
	/**
	 * Confirmar inclusao.
	 */
	public void confirmarInclusao(){
	entradaIncluirPgto = new IncluirSoliRelatorioPagamentoEntradaDTO();
		
	entradaIncluirPgto.setCdTipoRelatorio(PgitUtil.verificaIntegerNulo(getTipoRelatorio()));
	entradaIncluirPgto.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(getEmpresaGestoraFiltro()));
	entradaIncluirPgto.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(getTipoContratoFiltro()));
	if(getNumeroFiltro() != null && !getNumeroFiltro().equals("")){
		entradaIncluirPgto.setNrSequenciaContratoNegocio(Long.parseLong(getNumeroFiltro()));
	}
	
	entradaIncluirPgto.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(getTipoServicoFiltro()));
	entradaIncluirPgto.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(getModalidadeFiltro()));
	entradaIncluirPgto.setCdRelacionamentoProduto(1);
	entradaIncluirPgto.setCdCpfCnpjParticipante(PgitUtil.verificaLongNulo(getCorpoCpfCnpjParticipanteFiltro()));
	entradaIncluirPgto.setCdFilialCnpjParticipante(PgitUtil.verificaIntegerNulo(getFilialCpfCnpjParticipanteFiltro()));
	entradaIncluirPgto.setCdControleCpfParticipante(PgitUtil.verificaIntegerNulo(getControleCpfCnpjParticipanteFiltro()));
	entradaIncluirPgto.setCdPessoaJuridicaDebito(PgitUtil.verificaLongNulo(saidaValidarIncluirPgto.getCdPessoaJuridicaDebito()));
	entradaIncluirPgto.setCdTipoContratoDebito(PgitUtil.verificaIntegerNulo(saidaValidarIncluirPgto.getCdTipoContratoDebito()));
	entradaIncluirPgto.setNrSequenciaContratoDebito(PgitUtil.verificaLongNulo(saidaValidarIncluirPgto.getNrSequenciaContratoDebito()));
	entradaIncluirPgto.setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(getCdAgenciaBancariaContaDebito()));
	entradaIncluirPgto.setCdContaDebito(PgitUtil.verificaLongNulo(getCdContaBancariaContaDebito()));
	entradaIncluirPgto.setDtIniPagamento(FormatarData.formataDiaMesAno(getDataInicialPagamentoFiltro()));
	entradaIncluirPgto.setDtFimPagamento(FormatarData.formataDiaMesAno(getDataFinalPagamentoFiltro()));
	
	if(getCorpoCpfCnpjParticipanteFiltro()!= null && getCorpoCpfCnpjParticipanteFiltro()!= 0){
		entradaIncluirPgto.setCdPessoaParticipante(PgitUtil.verificaLongNulo(getCodigoParticipanteFiltro()));
	}else{
		entradaIncluirPgto.setCdPessoaParticipante(0l);
	}
	    
    try{
    	saidaIncluirPgto = getManterSolicitacaoRelatorioContratoImpl().incluirRelatorioPagamento(entradaIncluirPgto);
    	PgitFacesUtils.addInfoModalMessage(saidaIncluirPgto.getCodMensagem(), saidaIncluirPgto.getMensagem(),
		"#{manterSolicitacaoRelatorioPagamentosBean.consultarAposInclusao}");
    	
	} catch (PdcAdapterFunctionalException p) {
		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		return;
	} 
	
	}
	
	/**
	 * Confirmar excluir.
	 */
	public void confirmarExcluir(){
		ListarSoliRelatorioPagamentoSaidaDTO listarPgtoSaida = getListaGrid().get(getItemSelecionadoPgto());
		
		ExcluirSoliRelatorioPagamentoEntradaDTO entradaExcluir = new ExcluirSoliRelatorioPagamentoEntradaDTO();
		entradaExcluir.setCdSolicitacaoPagamento(listarPgtoSaida.getCdSolicitacaoPagamento());
		entradaExcluir.setNrSolicitacao(listarPgtoSaida.getNrSolicitacao());
		
		try {
			saidaExcluirPagamento = getManterSolicitacaoRelatorioContratoImpl().excluirRelatorioPagamento(entradaExcluir);
			PgitFacesUtils.addInfoModalMessage(saidaExcluirPagamento.getCodMensagem(), saidaExcluirPagamento.getMensagem(),
					"#{manterSolicitacaoRelatorioPagamentosBean.consultarAposExclusao}");
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return;
		} 
	}
	
	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora(){
		try {
			this.listaEmpresaGestora = new ArrayList<SelectItem>();
			List<EmpresaConglomeradoSaidaDTO> list = new ArrayList<EmpresaConglomeradoSaidaDTO>();
			
			EmpresaConglomeradoEntradaDTO entrada = new EmpresaConglomeradoEntradaDTO();
			entrada.setCdSituacao(0);
			entrada.setNrOcorrencias(180);
			
			list = comboService.listarEmpresaConglomerado(entrada);
			listaEmpresaGestora.clear();
			
			if (list.size() == 0 || list.size() > 1){
				listaEmpresaGestora.add(new SelectItem(new Long(0), MessageHelperUtils.getI18nMessage("label_combo_selecione")));
			}
			for(EmpresaConglomeradoSaidaDTO combo : list){
				listaEmpresaGestora.add(new SelectItem(combo.getCodClub(), combo.getCodClub() + " - " + combo.getDsRazaoSocial()));
				listaEmpresaGestoraHash.put(combo.getCodClub(), combo.getDsRazaoSocial());
			}			
		}catch (PdcAdapterFunctionalException p){
			listaEmpresaGestora = new ArrayList<SelectItem>();
			listaEmpresaGestoraHash = new HashMap<Long,String>();
		}
	}	
	
	
	/**
	 * Submit contrato.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String submitContrato(ActionEvent evt){
		return consultarContrato();
	}
	
	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		limparCampos();
		setItemSelecionadoLista(null);
	}

	/**
	 * Limpar criterio selecao contratos.
	 */
	public void limparCriterioSelecaoContratos(){
		
		if (getCriterioSelecaoContratos() != null){
			if (getCriterioSelecaoContratos() == 0){
				setModalidadeServico(0);				
			}else{
				setTipoServico(0);
			}
		}
		
	}
	
	/**
	 * Controle servico modalidade.
	 */
	public void controleServicoModalidade(){
		setModalidadeFiltro(null);
		setTipoServicoFiltro(null);
		setDisableArgumentosConsultaTipoServico(false);
		listaModalidadeFiltro = new ArrayList<SelectItem>();
	}
	
	/**
	 * Limpar participante contrato.
	 */
	public void limparParticipanteContrato(){
		setCpfCnpjParticipanteFiltro("");
		setNomeParticipanteFiltro("");
		setCorpoCpfCnpjParticipanteFiltro(null);
		setFilialCpfCnpjParticipanteFiltro(null);
		setControleCpfCnpjParticipanteFiltro(null);		
	}
	
	/**
	 * Limpar participante contrato.
	 */
	public void limparFiltroContrato(){
		setEmpresaGestoraFiltro(2269651L);
		setTipoContratoFiltro(0);
		setNumeroFiltro(null);
		setEmpresaGestoraDescContrato(null);
		setNumeroDescContrato(null);
		setDescricaoContratoDescContrato(null);
		setSituacaoDescContrato(null);
		setDisableArgumentosConsulta(false);
		
	}
	
	/**
	 * Limpar tipo servico.
	 */
	public void limparTipoServico(){
		setChkServicoModalidade(false);
		setTipoServicoFiltro(0);
		setModalidadeFiltro(0);
		setDisableArgumentosConsultaTipoServico(false);
	}


	/**
	 * Consultar apos exclusao.
	 *
	 * @return the string
	 */
	public String consultarAposExclusao() {
		try {
			listarRelatorioPagamento();
			limparCamposIncluir();
		} catch (PdcAdapterFunctionalException e) {
			setListaGrid(new ArrayList<ListarSoliRelatorioPagamentoSaidaDTO>());
		}
		
		return CON_MANTER_SOLICITACAO_REL_PGTO;
	}
	
	/**
	 * Consultar apos inclusao.
	 *
	 * @return the string
	 */
	public String consultarAposInclusao() {
		try {
			listarRelatorioPagamento();
		} catch (PdcAdapterFunctionalException e) {
			limparCamposIncluir();
		}
		
		return CON_MANTER_SOLICITACAO_REL_PGTO;
	}
	
	/**
	 * Get: selectItemContrato.
	 *
	 * @return selectItemContrato
	 */
	public List<SelectItem> getSelectItemContrato() {
		if (this.getListaConsultarListaContratosPessoas() == null || this.getListaConsultarListaContratosPessoas().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.getListaConsultarListaContratosPessoas().size(); index++){
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}		
	
	/**
	 * Limpar conta debito.
	 */
	public void limparContaDebito(){
		setCdBancoContaDebito(237);
		setCdContaBancariaContaDebito(null);
		setCdAgenciaBancariaContaDebito(null);
	}
	
	
	/**
	 * Buscar participante.
	 *
	 * @return the string
	 */
	public String buscarParticipante(){
		try{		
			ConsultarParticipanteContratoEntradaDTO entrada = new ConsultarParticipanteContratoEntradaDTO();		
			
			entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridicaContrato());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratonegocio(getNrSequenciaContratoNegocio());
			entrada.setCdControleCpfParticipante(getControleCpfCnpjParticipanteFiltro());
			entrada.setCdFilialCnpjParticipante(getFilialCpfCnpjParticipanteFiltro());
			entrada.setCdCpfCnpjParticipante(getCorpoCpfCnpjParticipanteFiltro());
			
			List<ConsultarParticipanteContratoSaidaDTO> listaParticipante = getConsultarPagamentosIndividualServiceImpl().consultarParticipanteContrato(entrada);
			
			if (listaParticipante.size() == 1){
				setNomeParticipanteFiltro(listaParticipante.get(0).getDsParticipante());
				setCpfCnpjParticipanteFiltro(CpfCnpjUtils.formatarCpfCnpj(listaParticipante.get(0).getCdCpfCnpjParticipante(), listaParticipante.get(0).getCdFilialCnpjParticipante(), listaParticipante.get(0).getCdControleCpfParticipante()));
				setCodigoParticipanteFiltro(listaParticipante.get(0).getCdParticipante());
				setCorpoCpfCnpjParticipanteFiltro(listaParticipante.get(0).getCdCpfCnpjParticipante());
				setFilialCpfCnpjParticipanteFiltro(listaParticipante.get(0).getCdFilialCnpjParticipante());
				setControleCpfCnpjParticipanteFiltro( listaParticipante.get(0).getCdControleCpfParticipante());
			}else{
				setListaParticipantes(listaParticipante);
				setItemSelecionadoListaParcipantes(null);
				listaParticipantesControleRadio = new ArrayList<SelectItem>();

				for (int i = 0; i < getListaParticipantes().size(); i++) {
					this.listaParticipantesControleRadio.add(new SelectItem(i, " "));
				}
			
				return "identificacaoParticipantePgtoAgendamento";
			}
			
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setCodigoParticipanteFiltro(null);			
			setNomeParticipanteFiltro("");
			setCpfCnpjParticipanteFiltro("");
			setItemSelecionadoListaParcipantes(null);
		}
						
		return "";
	}
	
	/**
	 * Pesquisar participantes.
	 *
	 * @param evt the evt
	 */
	public void pesquisarParticipantes(ActionEvent evt){
		
		try{		
			ConsultarParticipanteContratoEntradaDTO entrada = new ConsultarParticipanteContratoEntradaDTO();		
			
			entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridicaContrato());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratonegocio(getNrSequenciaContratoNegocio());
			entrada.setCdControleCpfParticipante(getControleCpfCnpjParticipanteFiltro());
			entrada.setCdFilialCnpjParticipante(getFilialCpfCnpjParticipanteFiltro());
			entrada.setCdCpfCnpjParticipante(getCorpoCpfCnpjParticipanteFiltro());
			
			List<ConsultarParticipanteContratoSaidaDTO> listaParticipante = getConsultarPagamentosIndividualServiceImpl().consultarParticipanteContrato(entrada);
						
			setListaParticipantes(listaParticipante);
			setItemSelecionadoListaParcipantes(null);
			listaParticipantesControleRadio = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaParticipantes().size(); i++) {
				this.listaParticipantesControleRadio.add(new SelectItem(i, " "));
			}		
			
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setCodigoParticipanteFiltro(null);
			setNomeParticipanteFiltro("");
			setCpfCnpjParticipanteFiltro("");
			setItemSelecionadoListaParcipantes(null);
		}
	}
	
	/**
	 * Selecionar participante.
	 *
	 * @return the string
	 */
	public String selecionarParticipante(){		
		setCodigoParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getCdParticipante());
		setCpfCnpjParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getCpfCnpj());
		setNomeParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getDsParticipante());
		setCorpoCpfCnpjParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getCdCpfCnpjParticipante());
		setFilialCpfCnpjParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getCdFilialCnpjParticipante());
		setControleCpfCnpjParticipanteFiltro(getListaParticipantes().get(getItemSelecionadoListaParcipantes()).getCdControleCpfParticipante());
		return INC_MANTER_SOLICITACAO_REL_PGTO;
	}
	
	/**
	 * Voltar participante.
	 *
	 * @return the string
	 */
	public String voltarParticipante(){
		return INC_MANTER_SOLICITACAO_REL_PGTO;
	}
	
	/**
	 * Selecionar contrato.
	 *
	 * @return the string
	 */
	public String selecionarContrato(){
		setCdPessoaJuridicaContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getCdPessoaJuridicaContrato());
		setCdTipoContratoNegocio(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getCdTipoContratoNegocio());
		setNrSequenciaContratoNegocio(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getNrSeqContratoNegocio());
		
		//Campos de descri��o,mostrados na tela
		//setEmpresaGestoraDescContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getDsPessoaJuridicaContrato());
		setEmpresaGestoraDescContrato(getListaEmpresaGestoraHash().get(getEmpresaGestoraFiltro()));
		setNumeroDescContrato(Long.toString(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getNrSeqContratoNegocio()));
		setDescricaoContratoDescContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getDsContrato());
		setSituacaoDescContrato(getListaConsultarListaContratosPessoas().get(getItemClienteSelecionadoContrato()).getDsSituacaoContratoNegocio());
		setClienteContratoSelecionado(true);		
		return INC_MANTER_SOLICITACAO_REL_PGTO;
	}
	
	
	/**
	 * Consultar contrato.
	 *
	 * @return the string
	 */
	public String consultarContrato(){
		try{				
			ConsultarListaContratosPessoasEntradaDTO entrada = new ConsultarListaContratosPessoasEntradaDTO();
			entrada.setCdClub(0L);
			entrada.setCdPessoaJuridicaContrato(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro() : 0L);
			entrada.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro() : 0);
			entrada.setNrSeqContratoNegocio(!getNumeroFiltro().equals("") ? Long.parseLong(getNumeroFiltro()) : 0);
			
			setListaConsultarListaContratosPessoas(getFiltroIdentificaoService().pesquisarContratos(entrada));
			
			if(getListaConsultarListaContratosPessoas().size() > 1){
				setItemClienteSelecionadoContrato(null);
				return "identificacaoContratoAgendamento";
			}
			
			setCdPessoaJuridicaContrato(getListaConsultarListaContratosPessoas().get(0).getCdPessoaJuridicaContrato());
			setCdTipoContratoNegocio(getListaConsultarListaContratosPessoas().get(0).getCdTipoContratoNegocio());
			setNrSequenciaContratoNegocio(getListaConsultarListaContratosPessoas().get(0).getNrSeqContratoNegocio());

			ConsultarListaContratosPessoasSaidaDTO saida = getListaConsultarListaContratosPessoas().get(0);

			setCpfCnpjFormatado(CpfCnpjUtils.formatarCpfCnpj(saida.getCdCpfCnpjParticipante(), saida.getCdFilialCnpjParticipante(), saida.getCdControleCpfCnpjParticipante()));
			setNomeRazaoParticipante(saida.getDsPessoaParticipante());
			
			//setEmpresaGestoraDescContrato(getListaConsultarListaContratosPessoas().get(0).getDsPessoaJuridicaContrato());
			setEmpresaGestoraDescContrato(getListaEmpresaGestoraHash().get(getEmpresaGestoraFiltro()));
			setNumeroDescContrato(Long.toString(getListaConsultarListaContratosPessoas().get(0).getNrSeqContratoNegocio()));
			setDescricaoContratoDescContrato(getListaConsultarListaContratosPessoas().get(0).getDsContrato());
			setSituacaoDescContrato(getListaConsultarListaContratosPessoas().get(0).getDsSituacaoContratoNegocio());
			setDescricaoTipoContratoDescContrato(getListaConsultarListaContratosPessoas().get(0).getDsTipoContratoNegocio());
			setClienteContratoSelecionado(true);			
			setDisableArgumentosConsulta(true);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setDisableArgumentosConsulta(false);
			setListaConsultarListaContratosPessoas(null);
			setItemClienteSelecionadoContrato(null);
			setClienteContratoSelecionado(false);
			setCdPessoaJuridicaContrato(null);
			setCdTipoContratoNegocio(null);
			setNrSequenciaContratoNegocio(null);			
			setEmpresaGestoraDescContrato("");
			setNumeroDescContrato("");
			setDescricaoContratoDescContrato("");
			setSituacaoDescContrato("");
		}
		
		return "";
	}
	
	/**
	 * Avancar excluir.
	 *
	 * @return the string
	 */
	public String avancarExcluir(){
		avancarDetalhar();
		return "AVANCAR_EXCLUIR";
	}
	
	/**
	 * Voltar contrato.
	 *
	 * @return the string
	 */
	public String voltarContrato(){
		return INC_MANTER_SOLICITACAO_REL_PGTO;
	}

	/**
	 * Is codigo valido.
	 *
	 * @param codigo the codigo
	 * @return true, if is codigo valido
	 */
	private boolean isCodigoValido(Integer codigo) {
		return codigo != null && codigo != 0;
	}
	
	/**
	 * Preenche lista modalidade.
	 */
	public void preencheListaModalidade(){
		listarModalidadesPagto();
	}
	
	/**
	 * Listar modalidades pagto.
	 */
	public void listarModalidadesPagto(){
		try{
			setModalidadeFiltro(0);
			if ( getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0){ 
					listaModalidadeFiltro = new ArrayList<SelectItem>();
					
					List<ConsultarModalidadePagtoSaidaDTO> list = comboService.consultarModalidadePagto(getTipoServicoFiltro());
			
					for (ConsultarModalidadePagtoSaidaDTO saida : list){
						listaModalidadeFiltro.add(new SelectItem(saida.getCdProdutoOperacaoRelacionado(), saida.getDsProdutoOperacaoRelacionado()));
						listaModalidadeHash.put(saida.getCdProdutoOperacaoRelacionado(),  saida.getDsProdutoOperacaoRelacionado());
					}
					setDisableArgumentosConsultaTipoServico(true);
			}else{
				listaModalidadeFiltro = new ArrayList<SelectItem>();
			}
		}catch(PdcAdapterFunctionalException p){
			listaModalidadeFiltro = new ArrayList<SelectItem>();
		}
	}	
	
	
	public void teste(){
		setDisableArgumentosConsultaTipoServico(true);
	}


	public boolean isDesabilitaBotaoAvancar(){
		if (isDisableArgumentosConsulta() || isDisableArgumentosConsultaTipoServico()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Preenche lista diretoria regional.
	 */
	public void preencheListaDiretoriaRegional(){
		getListaDiretoriaRegional();
	}
	
	/**
	 * Preenche lista gerencia regional.
	 */
	public void preencheListaGerenciaRegional(){
		getListaGerenciaRegional();
	}

	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		setItemSelecionadoPgto(null);
		return "VOLTAR_PESQUISAR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		setItemSelecionadoPgto(null);
		setDisableArgumentosConsulta(false);
		setFiltroDataDe(new Date());
		setFiltroDataAte(new Date());
		return "VOLTAR_PESQUISAR";
	}
	
	/**
	 * Voltar validacao.
	 *
	 * @return the string
	 */
	public String voltarValidacao(){
		   return "VOLTAR_INCLUIR";
	}
	
	/**
	 * Paginar rel pagamento.
	 */
	public void paginarRelPagamento(){
		listarRelatorioPagamento();
	}
	
	/**
	 * Get: listaDiretoriaRegional.
	 *
	 * @return listaDiretoriaRegional
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> getListaDiretoriaRegional() {
		try{
			if (getEmpresa() !=null && getEmpresa() != 0L  ){
				List<ListarDependenciaEmpresaSaidaDTO> list = comboService.pesquisaDiretoria(getEmpresa());
				
				listaDiretoriaRegionalHash.clear();
				listaDiretoriaRegional.clear();
				for(ListarDependenciaEmpresaSaidaDTO combo : list){
					listaDiretoriaRegionalHash.put(combo.getCdUnidadeOrganizacional(), PgitUtil.concatenarCampos(combo.getCdUnidadeOrganizacional(), combo.getDsUnidadeOrganizacional()));
					listaDiretoriaRegional.add(new SelectItem(combo.getCdUnidadeOrganizacional().toString(),PgitUtil.concatenarCampos(combo.getCdUnidadeOrganizacional(), combo.getDsUnidadeOrganizacional())));
				}
			}else{
				listaDiretoriaRegional = new ArrayList<SelectItem>();
			}
		}catch(PdcAdapterFunctionalException e){
			listaDiretoriaRegional = new ArrayList<SelectItem>();
		}

		return listaDiretoriaRegional;
	}
	
	/**
	 * Get: filtroTipoRelatorio.
	 *
	 * @return filtroTipoRelatorio
	 */
	public Integer getFiltroTipoRelatorio() {
		return filtroTipoRelatorio;
	}


    /**
     * Get: listaGerenciaRegional.
     *
     * @return listaGerenciaRegional
     */
    public List<SelectItem> getListaGerenciaRegional() {
		
		if ( getEmpresa() != null && getDiretoriaRegional() != null && getEmpresa() != 0L && getDiretoriaRegional() != 0L){ 
			try{
			
				List<ListarDependenciaDiretoriaSaidaDTO> list = comboService.pesquisaGerencia(getEmpresa(), Long.parseLong(getDiretoriaRegional().toString()),Integer.parseInt("0"));
				
				listaGerenciaRegional.clear();
				listaGerenciaRegionalHash.clear();
				for(ListarDependenciaDiretoriaSaidaDTO combo : list){
					listaGerenciaRegional.add(new SelectItem(combo.getCdDependencia(),PgitUtil.concatenarCampos(combo.getCdDependencia(), combo.getDsDependencia())));
					listaGerenciaRegionalHash.put(combo.getCdDependencia(), PgitUtil.concatenarCampos(combo.getCdDependencia(), combo.getDsDependencia()));
				
				}	
			}catch(PdcAdapterFunctionalException p){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				listaGerenciaRegional = new ArrayList<SelectItem>();
			}
			
		
		}else{
			listaGerenciaRegional = new ArrayList<SelectItem>();
		}
		
		return listaGerenciaRegional;
	}
    
	/**
	 * Listar tipo contrato.
	 */
	public void listarTipoContrato(){
		try{
			listaTipoContrato.clear();
			List<TipoContratoSaidaDTO> list = comboService.listarTipoContrato();
	
			if(list.size() == 0 || list.size() > 1  ){
				listaEmpresaGestora.add(new SelectItem(0, MessageHelperUtils.getI18nMessage("label_combo_selecione")));
			}
			for (TipoContratoSaidaDTO saida : list){
				listaTipoContrato.add(new SelectItem(saida.getCdTipoContrato(), saida.getDsTipoContrato()));
			}
		}catch (PdcAdapterFunctionalException p){
			listaTipoContrato = new ArrayList<SelectItem>();
		}		
	}	
	
	/**
	 * Listar tipo servico.
	 */
	public void listarTipoServico(){
		try{
			listaTipoServicoHash.clear();
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
			
			ListarServicosEntradaDTO entrada = new ListarServicosEntradaDTO();
			entrada.setCdNaturezaServico(0);
			entrada.setNumeroOcorrencias(30);
			
			List<ListarServicosSaidaDTO> list = comboService.listarTipoServico(entrada);
			
			for (ListarServicosSaidaDTO saida : list){
				listaTipoServicoFiltro.add(new SelectItem(saida.getCdServico(), saida.getDsServico()));
				listaTipoServicoHash.put(saida.getCdServico(), saida.getDsServico());
			}
			
		} catch (PdcAdapterFunctionalException p) {
			listaTipoServicoFiltro = new ArrayList<SelectItem>();			
		}			
	}	
    
	/**
	 * Limpar campos.
	 */
	public void limparCampos(){
		this.setFiltroTipoRelatorio(null);
		this.setFiltroDataAte(new Date());
		this.setFiltroDataDe(new Date());
		this.setItemSelecionadoPgto(null);
		setListaGrid(null);
		setBtoAcionado(false);
	}
	
	/**
	 * Limpar campos incluir.
	 */
	public void limparCamposIncluir(){
		setNumeroFiltro("");
		limparParticipanteContrato();
		limparContaDebito();
		limparTipoServico();
		setChkServicoModalidade(false);
		setChkParticipanteContrato(false);
		setChkContrato(false);
		setChkContaDebito(false);
		setDisableArgumentosConsulta(false);
		setDataInicialPagamentoFiltro(new Date());
		setDataFinalPagamentoFiltro(new Date());
		setFiltroDataDe(new Date());
		setFiltroDataAte(new Date());
		setItemSelecionadoPgto(null);
		setNumeroDescContrato("");
		setDescricaoContratoDescContrato("");
		setSituacaoDescContrato("");
		setCpfCnpjParticipanteFiltro("");
		setNomeParticipanteFiltro("");
		setCorpoCpfCnpjParticipanteFiltro(null);
		setFilialCpfCnpjParticipanteFiltro(null);
		setControleCpfCnpjParticipanteFiltro(null);
		setEmpresaGestoraDescContrato(null);
		setTipoRelatorio(1);
	}
	
	/**
	 * Limpar modalidade servico.
	 */
	public void limparModalidadeServico(){
		if (!getItemCheckModalidadeServico()){
			setModalidadeServico(0);
		}
	}
	
	/**
	 * Set: filtroTipoRelatorio.
	 *
	 * @param filtroTipoRelatorio the filtro tipo relatorio
	 */
	public void setFiltroTipoRelatorio(Integer filtroTipoRelatorio) {
		this.filtroTipoRelatorio = filtroTipoRelatorio;
	}


	/**
	 * Get: filtroDataDe.
	 *
	 * @return filtroDataDe
	 */
	public Date getFiltroDataDe() {
		return filtroDataDe;
	}


	/**
	 * Set: filtroDataDe.
	 *
	 * @param filtroDataDe the filtro data de
	 */
	public void setFiltroDataDe(Date filtroDataDe) {
		this.filtroDataDe = filtroDataDe;
	}


	/**
	 * Get: filtroDataAte.
	 *
	 * @return filtroDataAte
	 */
	public Date getFiltroDataAte() {
		return filtroDataAte;
	}


	/**
	 * Set: filtroDataAte.
	 *
	 * @param filtroDataAte the filtro data ate
	 */
	public void setFiltroDataAte(Date filtroDataAte) {
		this.filtroDataAte = filtroDataAte;
	}


	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}


	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: manterSolicitacaoRelatorioContratoImpl.
	 *
	 * @return manterSolicitacaoRelatorioContratoImpl
	 */
	public ISolRelContratosService getManterSolicitacaoRelatorioContratoImpl() {
		return manterSolicitacaoRelatorioContratoImpl;
	}

	/**
	 * Set: manterSolicitacaoRelatorioContratoImpl.
	 *
	 * @param manterSolicitacaoRelatorioContratoImpl the manter solicitacao relatorio contrato impl
	 */
	public void setManterSolicitacaoRelatorioContratoImpl(
			ISolRelContratosService manterSolicitacaoRelatorioContratoImpl) {
		this.manterSolicitacaoRelatorioContratoImpl = manterSolicitacaoRelatorioContratoImpl;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarSoliRelatorioPagamentoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarSoliRelatorioPagamentoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: itemSelecionadoPgto.
	 *
	 * @return itemSelecionadoPgto
	 */
	public Integer getItemSelecionadoPgto() {
		return itemSelecionadoPgto;
	}

	/**
	 * Set: itemSelecionadoPgto.
	 *
	 * @param itemSelecionadoPgto the item selecionado pgto
	 */
	public void setItemSelecionadoPgto(Integer itemSelecionadoPgto) {
		this.itemSelecionadoPgto = itemSelecionadoPgto;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: saidaDetalhePagamento.
	 *
	 * @return saidaDetalhePagamento
	 */
	public DetalharSoliRelatorioPagamentoSaidaDTO getSaidaDetalhePagamento() {
		return saidaDetalhePagamento;
	}

	/**
	 * Set: saidaDetalhePagamento.
	 *
	 * @param saidaDetalhePagamento the saida detalhe pagamento
	 */
	public void setSaidaDetalhePagamento(
			DetalharSoliRelatorioPagamentoSaidaDTO saidaDetalhePagamento) {
		this.saidaDetalhePagamento = saidaDetalhePagamento;
	}

	/**
	 * Get: dataInicioMov.
	 *
	 * @return dataInicioMov
	 */
	public String getDataInicioMov() {
		return dataInicioMov;
	}

	/**
	 * Set: dataInicioMov.
	 *
	 * @param dataInicioMov the data inicio mov
	 */
	public void setDataInicioMov(String dataInicioMov) {
		this.dataInicioMov = dataInicioMov;
	}

	/**
	 * Get: dataFimMov.
	 *
	 * @return dataFimMov
	 */
	public String getDataFimMov() {
		return dataFimMov;
	}

	/**
	 * Set: dataFimMov.
	 *
	 * @param dataFimMov the data fim mov
	 */
	public void setDataFimMov(String dataFimMov) {
		this.dataFimMov = dataFimMov;
	}

	/**
	 * Get: saidaExcluirPagamento.
	 *
	 * @return saidaExcluirPagamento
	 */
	public ExcluirSoliRelatorioPagamentoSaidaDTO getSaidaExcluirPagamento() {
		return saidaExcluirPagamento;
	}

	/**
	 * Set: saidaExcluirPagamento.
	 *
	 * @param saidaExcluirPagamento the saida excluir pagamento
	 */
	public void setSaidaExcluirPagamento(
			ExcluirSoliRelatorioPagamentoSaidaDTO saidaExcluirPagamento) {
		this.saidaExcluirPagamento = saidaExcluirPagamento;
	}

	/**
	 * Get: criterioSelecaoContratos.
	 *
	 * @return criterioSelecaoContratos
	 */
	public Integer getCriterioSelecaoContratos() {
		return criterioSelecaoContratos;
	}

	/**
	 * Set: criterioSelecaoContratos.
	 *
	 * @param criterioSelecaoContratos the criterio selecao contratos
	 */
	public void setCriterioSelecaoContratos(Integer criterioSelecaoContratos) {
		this.criterioSelecaoContratos = criterioSelecaoContratos;
	}

	/**
	 * Get: nivelConsolidacao.
	 *
	 * @return nivelConsolidacao
	 */
	public Integer getNivelConsolidacao() {
		return nivelConsolidacao;
	}

	/**
	 * Set: nivelConsolidacao.
	 *
	 * @param nivelConsolidacao the nivel consolidacao
	 */
	public void setNivelConsolidacao(Integer nivelConsolidacao) {
		this.nivelConsolidacao = nivelConsolidacao;
	}

	/**
	 * Get: itemCheckEmpresaConglomerado.
	 *
	 * @return itemCheckEmpresaConglomerado
	 */
	public Boolean getItemCheckEmpresaConglomerado() {
		return itemCheckEmpresaConglomerado;
	}

	/**
	 * Set: itemCheckEmpresaConglomerado.
	 *
	 * @param itemCheckEmpresaConglomerado the item check empresa conglomerado
	 */
	public void setItemCheckEmpresaConglomerado(Boolean itemCheckEmpresaConglomerado) {
		this.itemCheckEmpresaConglomerado = itemCheckEmpresaConglomerado;
	}

	/**
	 * Get: itemCheckDiretoriaRegional.
	 *
	 * @return itemCheckDiretoriaRegional
	 */
	public Boolean getItemCheckDiretoriaRegional() {
		return itemCheckDiretoriaRegional;
	}

	/**
	 * Set: itemCheckDiretoriaRegional.
	 *
	 * @param itemCheckDiretoriaRegional the item check diretoria regional
	 */
	public void setItemCheckDiretoriaRegional(Boolean itemCheckDiretoriaRegional) {
		this.itemCheckDiretoriaRegional = itemCheckDiretoriaRegional;
	}

	/**
	 * Get: itemCheckGerenciaRegional.
	 *
	 * @return itemCheckGerenciaRegional
	 */
	public Boolean getItemCheckGerenciaRegional() {
		return itemCheckGerenciaRegional;
	}

	/**
	 * Set: itemCheckGerenciaRegional.
	 *
	 * @param itemCheckGerenciaRegional the item check gerencia regional
	 */
	public void setItemCheckGerenciaRegional(Boolean itemCheckGerenciaRegional) {
		this.itemCheckGerenciaRegional = itemCheckGerenciaRegional;
	}

	/**
	 * Get: itemCheckAgenciaOperadora.
	 *
	 * @return itemCheckAgenciaOperadora
	 */
	public Boolean getItemCheckAgenciaOperadora() {
		return itemCheckAgenciaOperadora;
	}

	/**
	 * Set: itemCheckAgenciaOperadora.
	 *
	 * @param itemCheckAgenciaOperadora the item check agencia operadora
	 */
	public void setItemCheckAgenciaOperadora(Boolean itemCheckAgenciaOperadora) {
		this.itemCheckAgenciaOperadora = itemCheckAgenciaOperadora;
	}

	/**
	 * Get: itemCheckSegmento.
	 *
	 * @return itemCheckSegmento
	 */
	public Boolean getItemCheckSegmento() {
		return itemCheckSegmento;
	}

	/**
	 * Set: itemCheckSegmento.
	 *
	 * @param itemCheckSegmento the item check segmento
	 */
	public void setItemCheckSegmento(Boolean itemCheckSegmento) {
		this.itemCheckSegmento = itemCheckSegmento;
	}

	/**
	 * Get: itemCheckParticipanteGrupoEconomico.
	 *
	 * @return itemCheckParticipanteGrupoEconomico
	 */
	public Boolean getItemCheckParticipanteGrupoEconomico() {
		return itemCheckParticipanteGrupoEconomico;
	}

	/**
	 * Set: itemCheckParticipanteGrupoEconomico.
	 *
	 * @param itemCheckParticipanteGrupoEconomico the item check participante grupo economico
	 */
	public void setItemCheckParticipanteGrupoEconomico(
			Boolean itemCheckParticipanteGrupoEconomico) {
		this.itemCheckParticipanteGrupoEconomico = itemCheckParticipanteGrupoEconomico;
	}

	/**
	 * Get: itemCheckTipoServico.
	 *
	 * @return itemCheckTipoServico
	 */
	public Boolean getItemCheckTipoServico() {
		return itemCheckTipoServico;
	}

	/**
	 * Set: itemCheckTipoServico.
	 *
	 * @param itemCheckTipoServico the item check tipo servico
	 */
	public void setItemCheckTipoServico(Boolean itemCheckTipoServico) {
		this.itemCheckTipoServico = itemCheckTipoServico;
	}

	/**
	 * Get: itemCheckModalidadeServico.
	 *
	 * @return itemCheckModalidadeServico
	 */
	public Boolean getItemCheckModalidadeServico() {
		return itemCheckModalidadeServico;
	}

	/**
	 * Set: itemCheckModalidadeServico.
	 *
	 * @param itemCheckModalidadeServico the item check modalidade servico
	 */
	public void setItemCheckModalidadeServico(Boolean itemCheckModalidadeServico) {
		this.itemCheckModalidadeServico = itemCheckModalidadeServico;
	}

	/**
	 * Get: itemCheckClasse.
	 *
	 * @return itemCheckClasse
	 */
	public Boolean getItemCheckClasse() {
		return itemCheckClasse;
	}

	/**
	 * Set: itemCheckClasse.
	 *
	 * @param itemCheckClasse the item check classe
	 */
	public void setItemCheckClasse(Boolean itemCheckClasse) {
		this.itemCheckClasse = itemCheckClasse;
	}

	/**
	 * Get: conManterSolicitacaoRelPgto.
	 *
	 * @return conManterSolicitacaoRelPgto
	 */
	public static String getConManterSolicitacaoRelPgto() {
		return CON_MANTER_SOLICITACAO_REL_PGTO;
	}

	/**
	 * Get: hiddenObrigatoriedade.
	 *
	 * @return hiddenObrigatoriedade
	 */
	public String getHiddenObrigatoriedade() {
		return hiddenObrigatoriedade;
	}

	/**
	 * Set: hiddenObrigatoriedade.
	 *
	 * @param hiddenObrigatoriedade the hidden obrigatoriedade
	 */
	public void setHiddenObrigatoriedade(String hiddenObrigatoriedade) {
		this.hiddenObrigatoriedade = hiddenObrigatoriedade;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public Integer getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(Integer tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Get: tipoRelatorio.
	 *
	 * @return tipoRelatorio
	 */
	public Integer getTipoRelatorio() {
		return tipoRelatorio;
	}

	/**
	 * Set: tipoRelatorio.
	 *
	 * @param tipoRelatorio the tipo relatorio
	 */
	public void setTipoRelatorio(Integer tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}

	/**
	 * Get: tipoRelatorioDesc.
	 *
	 * @return tipoRelatorioDesc
	 */
	public String getTipoRelatorioDesc() {
		return tipoRelatorioDesc;
	}

	/**
	 * Set: tipoRelatorioDesc.
	 *
	 * @param tipoRelatorioDesc the tipo relatorio desc
	 */
	public void setTipoRelatorioDesc(String tipoRelatorioDesc) {
		this.tipoRelatorioDesc = tipoRelatorioDesc;
	}

	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public Integer getModalidadeServico() {
		return modalidadeServico;
	}

	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(Integer modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}

	/**
	 * Get: voltarPesquisar.
	 *
	 * @return voltarPesquisar
	 */
	public String getVoltarPesquisar() {
		return voltarPesquisar;
	}

	/**
	 * Set: voltarPesquisar.
	 *
	 * @param voltarPesquisar the voltar pesquisar
	 */
	public void setVoltarPesquisar(String voltarPesquisar) {
		this.voltarPesquisar = voltarPesquisar;
	}

	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public Long getEmpresa() {
		return empresa;
	}

	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(Long empresa) {
		this.empresa = empresa;
	}

	/**
	 * Get: diretoriaRegional.
	 *
	 * @return diretoriaRegional
	 */
	public Integer getDiretoriaRegional() {
		return diretoriaRegional;
	}

	/**
	 * Set: diretoriaRegional.
	 *
	 * @param diretoriaRegional the diretoria regional
	 */
	public void setDiretoriaRegional(Integer diretoriaRegional) {
		this.diretoriaRegional = diretoriaRegional;
	}

	/**
	 * Get: gerenciaRegional.
	 *
	 * @return gerenciaRegional
	 */
	public Integer getGerenciaRegional() {
		return gerenciaRegional;
	}

	/**
	 * Set: gerenciaRegional.
	 *
	 * @param gerenciaRegional the gerencia regional
	 */
	public void setGerenciaRegional(Integer gerenciaRegional) {
		this.gerenciaRegional = gerenciaRegional;
	}

	/**
	 * Get: agenciaOperadora.
	 *
	 * @return agenciaOperadora
	 */
	public String getAgenciaOperadora() {
		return agenciaOperadora;
	}

	/**
	 * Set: agenciaOperadora.
	 *
	 * @param agenciaOperadora the agencia operadora
	 */
	public void setAgenciaOperadora(String agenciaOperadora) {
		this.agenciaOperadora = agenciaOperadora;
	}

	/**
	 * Get: agenciaDigitada.
	 *
	 * @return agenciaDigitada
	 */
	public String getAgenciaDigitada() {
		return agenciaDigitada;
	}

	/**
	 * Set: agenciaDigitada.
	 *
	 * @param agenciaDigitada the agencia digitada
	 */
	public void setAgenciaDigitada(String agenciaDigitada) {
		this.agenciaDigitada = agenciaDigitada;
	}

	/**
	 * Get: dsAgencia.
	 *
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}

	/**
	 * Set: dsAgencia.
	 *
	 * @param dsAgencia the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}

	/**
	 * Get: dsAgenciaAux.
	 *
	 * @return dsAgenciaAux
	 */
	public String getDsAgenciaAux() {
		return dsAgenciaAux;
	}

	/**
	 * Set: dsAgenciaAux.
	 *
	 * @param dsAgenciaAux the ds agencia aux
	 */
	public void setDsAgenciaAux(String dsAgenciaAux) {
		this.dsAgenciaAux = dsAgenciaAux;
	}

	/**
	 * Get: segmento.
	 *
	 * @return segmento
	 */
	public Integer getSegmento() {
		return segmento;
	}

	/**
	 * Set: segmento.
	 *
	 * @param segmento the segmento
	 */
	public void setSegmento(Integer segmento) {
		this.segmento = segmento;
	}

	/**
	 * Get: participante.
	 *
	 * @return participante
	 */
	public Long getParticipante() {
		return participante;
	}

	/**
	 * Set: participante.
	 *
	 * @param participante the participante
	 */
	public void setParticipante(Long participante) {
		this.participante = participante;
	}

	/**
	 * Get: participanteDesc.
	 *
	 * @return participanteDesc
	 */
	public String getParticipanteDesc() {
		return participanteDesc;
	}

	/**
	 * Set: participanteDesc.
	 *
	 * @param participanteDesc the participante desc
	 */
	public void setParticipanteDesc(String participanteDesc) {
		this.participanteDesc = participanteDesc;
	}

	/**
	 * Get: tipoServicoDesc.
	 *
	 * @return tipoServicoDesc
	 */
	public String getTipoServicoDesc() {
		return tipoServicoDesc;
	}

	/**
	 * Set: tipoServicoDesc.
	 *
	 * @param tipoServicoDesc the tipo servico desc
	 */
	public void setTipoServicoDesc(String tipoServicoDesc) {
		this.tipoServicoDesc = tipoServicoDesc;
	}

	/**
	 * Get: modalidadeServiceDesc.
	 *
	 * @return modalidadeServiceDesc
	 */
	public String getModalidadeServiceDesc() {
		return modalidadeServiceDesc;
	}

	/**
	 * Set: modalidadeServiceDesc.
	 *
	 * @param modalidadeServiceDesc the modalidade service desc
	 */
	public void setModalidadeServiceDesc(String modalidadeServiceDesc) {
		this.modalidadeServiceDesc = modalidadeServiceDesc;
	}

	/**
	 * Get: empresaDesc.
	 *
	 * @return empresaDesc
	 */
	public String getEmpresaDesc() {
		return empresaDesc;
	}

	/**
	 * Set: empresaDesc.
	 *
	 * @param empresaDesc the empresa desc
	 */
	public void setEmpresaDesc(String empresaDesc) {
		this.empresaDesc = empresaDesc;
	}

	/**
	 * Get: diretoriaRegionalDesc.
	 *
	 * @return diretoriaRegionalDesc
	 */
	public String getDiretoriaRegionalDesc() {
		return diretoriaRegionalDesc;
	}

	/**
	 * Set: diretoriaRegionalDesc.
	 *
	 * @param diretoriaRegionalDesc the diretoria regional desc
	 */
	public void setDiretoriaRegionalDesc(String diretoriaRegionalDesc) {
		this.diretoriaRegionalDesc = diretoriaRegionalDesc;
	}

	/**
	 * Get: gerenciaRegionalDesc.
	 *
	 * @return gerenciaRegionalDesc
	 */
	public String getGerenciaRegionalDesc() {
		return gerenciaRegionalDesc;
	}

	/**
	 * Set: gerenciaRegionalDesc.
	 *
	 * @param gerenciaRegionalDesc the gerencia regional desc
	 */
	public void setGerenciaRegionalDesc(String gerenciaRegionalDesc) {
		this.gerenciaRegionalDesc = gerenciaRegionalDesc;
	}

	/**
	 * Get: segmentoDesc.
	 *
	 * @return segmentoDesc
	 */
	public String getSegmentoDesc() {
		return segmentoDesc;
	}

	/**
	 * Set: segmentoDesc.
	 *
	 * @param segmentoDesc the segmento desc
	 */
	public void setSegmentoDesc(String segmentoDesc) {
		this.segmentoDesc = segmentoDesc;
	}

	/**
	 * Get: dataHoraSolicitacao.
	 *
	 * @return dataHoraSolicitacao
	 */
	public String getDataHoraSolicitacao() {
		return dataHoraSolicitacao;
	}

	/**
	 * Set: dataHoraSolicitacao.
	 *
	 * @param dataHoraSolicitacao the data hora solicitacao
	 */
	public void setDataHoraSolicitacao(String dataHoraSolicitacao) {
		this.dataHoraSolicitacao = dataHoraSolicitacao;
	}

	/**
	 * Get: listaModalidadeServicoHash.
	 *
	 * @return listaModalidadeServicoHash
	 */
	public Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoHash() {
		return listaModalidadeServicoHash;
	}

	/**
	 * Set lista modalidade servico hash.
	 *
	 * @param listaModalidadeServicoHash the lista modalidade servico hash
	 */
	public void setListaModalidadeServicoHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash) {
		this.listaModalidadeServicoHash = listaModalidadeServicoHash;
	}

	/**
	 * Set: listaDiretoriaRegional.
	 *
	 * @param listaDiretoriaRegional the lista diretoria regional
	 */
	public void setListaDiretoriaRegional(List<SelectItem> listaDiretoriaRegional) {
		this.listaDiretoriaRegional = listaDiretoriaRegional;
	}

	/**
	 * Get: listaDiretoriaRegionalHash.
	 *
	 * @return listaDiretoriaRegionalHash
	 */
	public Map getListaDiretoriaRegionalHash() {
		return listaDiretoriaRegionalHash;
	}

	/**
	 * Set: listaDiretoriaRegionalHash.
	 *
	 * @param listaDiretoriaRegionalHash the lista diretoria regional hash
	 */
	public void setListaDiretoriaRegionalHash(Map listaDiretoriaRegionalHash) {
		this.listaDiretoriaRegionalHash = listaDiretoriaRegionalHash;
	}

	/**
	 * Set: listaGerenciaRegional.
	 *
	 * @param listaGerenciaRegional the lista gerencia regional
	 */
	public void setListaGerenciaRegional(List<SelectItem> listaGerenciaRegional) {
		this.listaGerenciaRegional = listaGerenciaRegional;
	}

	/**
	 * Get: listaGerenciaRegionalHash.
	 *
	 * @return listaGerenciaRegionalHash
	 */
	public Map<Integer, String> getListaGerenciaRegionalHash() {
		return listaGerenciaRegionalHash;
	}

	/**
	 * Set lista gerencia regional hash.
	 *
	 * @param listaGerenciaRegionalHash the lista gerencia regional hash
	 */
	public void setListaGerenciaRegionalHash(
			Map<Integer, String> listaGerenciaRegionalHash) {
		this.listaGerenciaRegionalHash = listaGerenciaRegionalHash;
	}

	/**
	 * Get: empresaGestoraFiltro.
	 *
	 * @return empresaGestoraFiltro
	 */
	public Long getEmpresaGestoraFiltro() {
		return empresaGestoraFiltro;
	}

	/**
	 * Set: empresaGestoraFiltro.
	 *
	 * @param empresaGestoraFiltro the empresa gestora filtro
	 */
	public void setEmpresaGestoraFiltro(Long empresaGestoraFiltro) {
		this.empresaGestoraFiltro = empresaGestoraFiltro;
	}

	/**
	 * Get: tipoContratoFiltro.
	 *
	 * @return tipoContratoFiltro
	 */
	public Integer getTipoContratoFiltro() {
		return tipoContratoFiltro;
	}

	/**
	 * Set: tipoContratoFiltro.
	 *
	 * @param tipoContratoFiltro the tipo contrato filtro
	 */
	public void setTipoContratoFiltro(Integer tipoContratoFiltro) {
		this.tipoContratoFiltro = tipoContratoFiltro;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: listaEmpresaGestoraHash.
	 *
	 * @return listaEmpresaGestoraHash
	 */
	public Map<Long, String> getListaEmpresaGestoraHash() {
		return listaEmpresaGestoraHash;
	}

	/**
	 * Set lista empresa gestora hash.
	 *
	 * @param listaEmpresaGestoraHash the lista empresa gestora hash
	 */
	public void setListaEmpresaGestoraHash(Map<Long, String> listaEmpresaGestoraHash) {
		this.listaEmpresaGestoraHash = listaEmpresaGestoraHash;
	}

	/**
	 * Get: listaTipoContrato.
	 *
	 * @return listaTipoContrato
	 */
	public List<SelectItem> getListaTipoContrato() {
		return listaTipoContrato;
	}

	/**
	 * Set: listaTipoContrato.
	 *
	 * @param listaTipoContrato the lista tipo contrato
	 */
	public void setListaTipoContrato(List<SelectItem> listaTipoContrato) {
		this.listaTipoContrato = listaTipoContrato;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Is cliente contrato selecionado.
	 *
	 * @return true, if is cliente contrato selecionado
	 */
	public boolean isClienteContratoSelecionado() {
		return clienteContratoSelecionado;
	}

	/**
	 * Set: clienteContratoSelecionado.
	 *
	 * @param clienteContratoSelecionado the cliente contrato selecionado
	 */
	public void setClienteContratoSelecionado(boolean clienteContratoSelecionado) {
		this.clienteContratoSelecionado = clienteContratoSelecionado;
	}

	/**
	 * Get: numeroFiltro.
	 *
	 * @return numeroFiltro
	 */
	public String getNumeroFiltro() {
		return numeroFiltro;
	}

	/**
	 * Set: numeroFiltro.
	 *
	 * @param numeroFiltro the numero filtro
	 */
	public void setNumeroFiltro(String numeroFiltro) {
		this.numeroFiltro = numeroFiltro;
	}

	/**
	 * Get: filtroIdentificaoService.
	 *
	 * @return filtroIdentificaoService
	 */
	public IFiltroIdentificaoService getFiltroIdentificaoService() {
		return filtroIdentificaoService;
	}

	/**
	 * Set: filtroIdentificaoService.
	 *
	 * @param filtroIdentificaoService the filtro identificao service
	 */
	public void setFiltroIdentificaoService(
			IFiltroIdentificaoService filtroIdentificaoService) {
		this.filtroIdentificaoService = filtroIdentificaoService;
	}

	/**
	 * Get: listaConsultarListaContratosPessoas.
	 *
	 * @return listaConsultarListaContratosPessoas
	 */
	public List<ConsultarListaContratosPessoasSaidaDTO> getListaConsultarListaContratosPessoas() {
		return listaConsultarListaContratosPessoas;
	}

	/**
	 * Set: listaConsultarListaContratosPessoas.
	 *
	 * @param listaConsultarListaContratosPessoas the lista consultar lista contratos pessoas
	 */
	public void setListaConsultarListaContratosPessoas(
			List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoas) {
		this.listaConsultarListaContratosPessoas = listaConsultarListaContratosPessoas;
	}

	/**
	 * Get: itemClienteSelecionadoContrato.
	 *
	 * @return itemClienteSelecionadoContrato
	 */
	public Integer getItemClienteSelecionadoContrato() {
		return itemClienteSelecionadoContrato;
	}

	/**
	 * Set: itemClienteSelecionadoContrato.
	 *
	 * @param itemClienteSelecionadoContrato the item cliente selecionado contrato
	 */
	public void setItemClienteSelecionadoContrato(
			Integer itemClienteSelecionadoContrato) {
		this.itemClienteSelecionadoContrato = itemClienteSelecionadoContrato;
	}

	/**
	 * Get: cpfCnpjFormatado.
	 *
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return cpfCnpjFormatado;
	}

	/**
	 * Set: cpfCnpjFormatado.
	 *
	 * @param cpfCnpjFormatado the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: empresaGestoraDescContrato.
	 *
	 * @return empresaGestoraDescContrato
	 */
	public String getEmpresaGestoraDescContrato() {
		return empresaGestoraDescContrato;
	}

	/**
	 * Set: empresaGestoraDescContrato.
	 *
	 * @param empresaGestoraDescContrato the empresa gestora desc contrato
	 */
	public void setEmpresaGestoraDescContrato(String empresaGestoraDescContrato) {
		this.empresaGestoraDescContrato = empresaGestoraDescContrato;
	}

	/**
	 * Get: numeroDescContrato.
	 *
	 * @return numeroDescContrato
	 */
	public String getNumeroDescContrato() {
		return numeroDescContrato;
	}

	/**
	 * Set: numeroDescContrato.
	 *
	 * @param numeroDescContrato the numero desc contrato
	 */
	public void setNumeroDescContrato(String numeroDescContrato) {
		this.numeroDescContrato = numeroDescContrato;
	}

	/**
	 * Get: descricaoTipoContratoDescContrato.
	 *
	 * @return descricaoTipoContratoDescContrato
	 */
	public String getDescricaoTipoContratoDescContrato() {
		return descricaoTipoContratoDescContrato;
	}

	/**
	 * Set: descricaoTipoContratoDescContrato.
	 *
	 * @param descricaoTipoContratoDescContrato the descricao tipo contrato desc contrato
	 */
	public void setDescricaoTipoContratoDescContrato(
			String descricaoTipoContratoDescContrato) {
		this.descricaoTipoContratoDescContrato = descricaoTipoContratoDescContrato;
	}

	/**
	 * Get: descricaoContratoDescContrato.
	 *
	 * @return descricaoContratoDescContrato
	 */
	public String getDescricaoContratoDescContrato() {
		return descricaoContratoDescContrato;
	}

	/**
	 * Set: descricaoContratoDescContrato.
	 *
	 * @param descricaoContratoDescContrato the descricao contrato desc contrato
	 */
	public void setDescricaoContratoDescContrato(
			String descricaoContratoDescContrato) {
		this.descricaoContratoDescContrato = descricaoContratoDescContrato;
	}

	/**
	 * Get: situacaoDescContrato.
	 *
	 * @return situacaoDescContrato
	 */
	public String getSituacaoDescContrato() {
		return situacaoDescContrato;
	}

	/**
	 * Set: situacaoDescContrato.
	 *
	 * @param situacaoDescContrato the situacao desc contrato
	 */
	public void setSituacaoDescContrato(String situacaoDescContrato) {
		this.situacaoDescContrato = situacaoDescContrato;
	}

	/**
	 * Get: dataInicialPagamentoFiltro.
	 *
	 * @return dataInicialPagamentoFiltro
	 */
	public Date getDataInicialPagamentoFiltro() {
		return dataInicialPagamentoFiltro;
	}

	/**
	 * Set: dataInicialPagamentoFiltro.
	 *
	 * @param dataInicialPagamentoFiltro the data inicial pagamento filtro
	 */
	public void setDataInicialPagamentoFiltro(Date dataInicialPagamentoFiltro) {
		this.dataInicialPagamentoFiltro = dataInicialPagamentoFiltro;
	}

	/**
	 * Get: dataFinalPagamentoFiltro.
	 *
	 * @return dataFinalPagamentoFiltro
	 */
	public Date getDataFinalPagamentoFiltro() {
		return dataFinalPagamentoFiltro;
	}

	/**
	 * Set: dataFinalPagamentoFiltro.
	 *
	 * @param dataFinalPagamentoFiltro the data final pagamento filtro
	 */
	public void setDataFinalPagamentoFiltro(Date dataFinalPagamentoFiltro) {
		this.dataFinalPagamentoFiltro = dataFinalPagamentoFiltro;
	}

	/**
	 * Get: codigoParticipanteFiltro.
	 *
	 * @return codigoParticipanteFiltro
	 */
	public Long getCodigoParticipanteFiltro() {
		return codigoParticipanteFiltro;
	}

	/**
	 * Set: codigoParticipanteFiltro.
	 *
	 * @param codigoParticipanteFiltro the codigo participante filtro
	 */
	public void setCodigoParticipanteFiltro(Long codigoParticipanteFiltro) {
		this.codigoParticipanteFiltro = codigoParticipanteFiltro;
	}

	/**
	 * Get: nomeParticipanteFiltro.
	 *
	 * @return nomeParticipanteFiltro
	 */
	public String getNomeParticipanteFiltro() {
		return nomeParticipanteFiltro;
	}

	/**
	 * Set: nomeParticipanteFiltro.
	 *
	 * @param nomeParticipanteFiltro the nome participante filtro
	 */
	public void setNomeParticipanteFiltro(String nomeParticipanteFiltro) {
		this.nomeParticipanteFiltro = nomeParticipanteFiltro;
	}

	/**
	 * Get: cpfCnpjParticipanteFiltro.
	 *
	 * @return cpfCnpjParticipanteFiltro
	 */
	public String getCpfCnpjParticipanteFiltro() {
		return cpfCnpjParticipanteFiltro;
	}

	/**
	 * Set: cpfCnpjParticipanteFiltro.
	 *
	 * @param cpfCnpjParticipanteFiltro the cpf cnpj participante filtro
	 */
	public void setCpfCnpjParticipanteFiltro(String cpfCnpjParticipanteFiltro) {
		this.cpfCnpjParticipanteFiltro = cpfCnpjParticipanteFiltro;
	}

	/**
	 * Get: corpoCpfCnpjParticipanteFiltro.
	 *
	 * @return corpoCpfCnpjParticipanteFiltro
	 */
	public Long getCorpoCpfCnpjParticipanteFiltro() {
		return corpoCpfCnpjParticipanteFiltro;
	}

	/**
	 * Set: corpoCpfCnpjParticipanteFiltro.
	 *
	 * @param corpoCpfCnpjParticipanteFiltro the corpo cpf cnpj participante filtro
	 */
	public void setCorpoCpfCnpjParticipanteFiltro(
			Long corpoCpfCnpjParticipanteFiltro) {
		this.corpoCpfCnpjParticipanteFiltro = corpoCpfCnpjParticipanteFiltro;
	}

	/**
	 * Get: filialCpfCnpjParticipanteFiltro.
	 *
	 * @return filialCpfCnpjParticipanteFiltro
	 */
	public Integer getFilialCpfCnpjParticipanteFiltro() {
		return filialCpfCnpjParticipanteFiltro;
	}

	/**
	 * Set: filialCpfCnpjParticipanteFiltro.
	 *
	 * @param filialCpfCnpjParticipanteFiltro the filial cpf cnpj participante filtro
	 */
	public void setFilialCpfCnpjParticipanteFiltro(
			Integer filialCpfCnpjParticipanteFiltro) {
		this.filialCpfCnpjParticipanteFiltro = filialCpfCnpjParticipanteFiltro;
	}

	/**
	 * Get: controleCpfCnpjParticipanteFiltro.
	 *
	 * @return controleCpfCnpjParticipanteFiltro
	 */
	public Integer getControleCpfCnpjParticipanteFiltro() {
		return controleCpfCnpjParticipanteFiltro;
	}

	/**
	 * Set: controleCpfCnpjParticipanteFiltro.
	 *
	 * @param controleCpfCnpjParticipanteFiltro the controle cpf cnpj participante filtro
	 */
	public void setControleCpfCnpjParticipanteFiltro(
			Integer controleCpfCnpjParticipanteFiltro) {
		this.controleCpfCnpjParticipanteFiltro = controleCpfCnpjParticipanteFiltro;
	}

	/**
	 * Is chk participante contrato.
	 *
	 * @return true, if is chk participante contrato
	 */
	public boolean isChkParticipanteContrato() {
		return chkParticipanteContrato;
	}

	/**
	 * Set: chkParticipanteContrato.
	 *
	 * @param chkParticipanteContrato the chk participante contrato
	 */
	public void setChkParticipanteContrato(boolean chkParticipanteContrato) {
		this.chkParticipanteContrato = chkParticipanteContrato;
	}

	/**
	 * Is chk conta debito.
	 *
	 * @return true, if is chk conta debito
	 */
	public boolean isChkContaDebito() {
		return chkContaDebito;
	}

	/**
	 * Set: chkContaDebito.
	 *
	 * @param chkContaDebito the chk conta debito
	 */
	public void setChkContaDebito(boolean chkContaDebito) {
		this.chkContaDebito = chkContaDebito;
	}

	/**
	 * Get: listaParticipantes.
	 *
	 * @return listaParticipantes
	 */
	public List<ConsultarParticipanteContratoSaidaDTO> getListaParticipantes() {
		return listaParticipantes;
	}

	/**
	 * Set: listaParticipantes.
	 *
	 * @param listaParticipantes the lista participantes
	 */
	public void setListaParticipantes(
			List<ConsultarParticipanteContratoSaidaDTO> listaParticipantes) {
		this.listaParticipantes = listaParticipantes;
	}

	/**
	 * Get: listaParticipantesControleRadio.
	 *
	 * @return listaParticipantesControleRadio
	 */
	public List<SelectItem> getListaParticipantesControleRadio() {
		return listaParticipantesControleRadio;
	}

	/**
	 * Set: listaParticipantesControleRadio.
	 *
	 * @param listaParticipantesControleRadio the lista participantes controle radio
	 */
	public void setListaParticipantesControleRadio(
			List<SelectItem> listaParticipantesControleRadio) {
		this.listaParticipantesControleRadio = listaParticipantesControleRadio;
	}

	/**
	 * Get: itemSelecionadoListaParcipantes.
	 *
	 * @return itemSelecionadoListaParcipantes
	 */
	public Integer getItemSelecionadoListaParcipantes() {
		return itemSelecionadoListaParcipantes;
	}

	/**
	 * Set: itemSelecionadoListaParcipantes.
	 *
	 * @param itemSelecionadoListaParcipantes the item selecionado lista parcipantes
	 */
	public void setItemSelecionadoListaParcipantes(
			Integer itemSelecionadoListaParcipantes) {
		this.itemSelecionadoListaParcipantes = itemSelecionadoListaParcipantes;
	}

	/**
	 * Get: voltarParticipante.
	 *
	 * @return voltarParticipante
	 */
	public String getVoltarParticipante() {
		return voltarParticipante;
	}

	/**
	 * Set: voltarParticipante.
	 *
	 * @param voltarParticipante the voltar participante
	 */
	public void setVoltarParticipante(String voltarParticipante) {
		this.voltarParticipante = voltarParticipante;
	}

	/**
	 * Get: consultarPagamentosIndividualServiceImpl.
	 *
	 * @return consultarPagamentosIndividualServiceImpl
	 */
	public IConsultarPagamentosIndividualService getConsultarPagamentosIndividualServiceImpl() {
		return consultarPagamentosIndividualServiceImpl;
	}

	/**
	 * Set: consultarPagamentosIndividualServiceImpl.
	 *
	 * @param consultarPagamentosIndividualServiceImpl the consultar pagamentos individual service impl
	 */
	public void setConsultarPagamentosIndividualServiceImpl(
			IConsultarPagamentosIndividualService consultarPagamentosIndividualServiceImpl) {
		this.consultarPagamentosIndividualServiceImpl = consultarPagamentosIndividualServiceImpl;
	}

	/**
	 * Get: cdBancoContaDebito.
	 *
	 * @return cdBancoContaDebito
	 */
	public Integer getCdBancoContaDebito() {
		return cdBancoContaDebito;
	}

	/**
	 * Set: cdBancoContaDebito.
	 *
	 * @param cdBancoContaDebito the cd banco conta debito
	 */
	public void setCdBancoContaDebito(Integer cdBancoContaDebito) {
		this.cdBancoContaDebito = cdBancoContaDebito;
	}

	/**
	 * Get: cdAgenciaBancariaContaDebito.
	 *
	 * @return cdAgenciaBancariaContaDebito
	 */
	public Integer getCdAgenciaBancariaContaDebito() {
		return cdAgenciaBancariaContaDebito;
	}

	/**
	 * Set: cdAgenciaBancariaContaDebito.
	 *
	 * @param cdAgenciaBancariaContaDebito the cd agencia bancaria conta debito
	 */
	public void setCdAgenciaBancariaContaDebito(Integer cdAgenciaBancariaContaDebito) {
		this.cdAgenciaBancariaContaDebito = cdAgenciaBancariaContaDebito;
	}

	/**
	 * Get: cdContaBancariaContaDebito.
	 *
	 * @return cdContaBancariaContaDebito
	 */
	public Long getCdContaBancariaContaDebito() {
		return cdContaBancariaContaDebito;
	}

	/**
	 * Set: cdContaBancariaContaDebito.
	 *
	 * @param cdContaBancariaContaDebito the cd conta bancaria conta debito
	 */
	public void setCdContaBancariaContaDebito(Long cdContaBancariaContaDebito) {
		this.cdContaBancariaContaDebito = cdContaBancariaContaDebito;
	}

	/**
	 * Get: cdDigitoContaContaDebito.
	 *
	 * @return cdDigitoContaContaDebito
	 */
	public String getCdDigitoContaContaDebito() {
		return cdDigitoContaContaDebito;
	}

	/**
	 * Set: cdDigitoContaContaDebito.
	 *
	 * @param cdDigitoContaContaDebito the cd digito conta conta debito
	 */
	public void setCdDigitoContaContaDebito(String cdDigitoContaContaDebito) {
		this.cdDigitoContaContaDebito = cdDigitoContaContaDebito;
	}

	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Get: modalidadeFiltro.
	 *
	 * @return modalidadeFiltro
	 */
	public Integer getModalidadeFiltro() {
		return modalidadeFiltro;
	}

	/**
	 * Set: modalidadeFiltro.
	 *
	 * @param modalidadeFiltro the modalidade filtro
	 */
	public void setModalidadeFiltro(Integer modalidadeFiltro) {
		this.modalidadeFiltro = modalidadeFiltro;
	}

	/**
	 * Get: listaModalidadeFiltro.
	 *
	 * @return listaModalidadeFiltro
	 */
	public List<SelectItem> getListaModalidadeFiltro() {
		return listaModalidadeFiltro;
	}

	/**
	 * Set: listaModalidadeFiltro.
	 *
	 * @param listaModalidadeFiltro the lista modalidade filtro
	 */
	public void setListaModalidadeFiltro(List<SelectItem> listaModalidadeFiltro) {
		this.listaModalidadeFiltro = listaModalidadeFiltro;
	}

	/**
	 * Get: listaModalidadeHash.
	 *
	 * @return listaModalidadeHash
	 */
	public Map<Integer, String> getListaModalidadeHash() {
		return listaModalidadeHash;
	}

	/**
	 * Set lista modalidade hash.
	 *
	 * @param listaModalidadeHash the lista modalidade hash
	 */
	public void setListaModalidadeHash(Map<Integer, String> listaModalidadeHash) {
		this.listaModalidadeHash = listaModalidadeHash;
	}

	/**
	 * Get: listaTipoServicoFiltro.
	 *
	 * @return listaTipoServicoFiltro
	 */
	public List<SelectItem> getListaTipoServicoFiltro() {
		return listaTipoServicoFiltro;
	}

	/**
	 * Set: listaTipoServicoFiltro.
	 *
	 * @param listaTipoServicoFiltro the lista tipo servico filtro
	 */
	public void setListaTipoServicoFiltro(List<SelectItem> listaTipoServicoFiltro) {
		this.listaTipoServicoFiltro = listaTipoServicoFiltro;
	}

	/**
	 * Is chk servico modalidade.
	 *
	 * @return true, if is chk servico modalidade
	 */
	public boolean isChkServicoModalidade() {
		return chkServicoModalidade;
	}

	/**
	 * Set: chkServicoModalidade.
	 *
	 * @param chkServicoModalidade the chk servico modalidade
	 */
	public void setChkServicoModalidade(boolean chkServicoModalidade) {
		this.chkServicoModalidade = chkServicoModalidade;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: entradaValidarIncluirPgto.
	 *
	 * @return entradaValidarIncluirPgto
	 */
	public IncSoliRelatorioPagamentoValidacaoEntradaDTO getEntradaValidarIncluirPgto() {
		return entradaValidarIncluirPgto;
	}

	/**
	 * Set: entradaValidarIncluirPgto.
	 *
	 * @param entradaValidarIncluirPgto the entrada validar incluir pgto
	 */
	public void setEntradaValidarIncluirPgto(
			IncSoliRelatorioPagamentoValidacaoEntradaDTO entradaValidarIncluirPgto) {
		this.entradaValidarIncluirPgto = entradaValidarIncluirPgto;
	}

	/**
	 * Get: saidaValidarIncluirPgto.
	 *
	 * @return saidaValidarIncluirPgto
	 */
	public IncSoliRelatorioPagamentoValidacaoSaidaDTO getSaidaValidarIncluirPgto() {
		return saidaValidarIncluirPgto;
	}

	/**
	 * Set: saidaValidarIncluirPgto.
	 *
	 * @param saidaValidarIncluirPgto the saida validar incluir pgto
	 */
	public void setSaidaValidarIncluirPgto(
			IncSoliRelatorioPagamentoValidacaoSaidaDTO saidaValidarIncluirPgto) {
		this.saidaValidarIncluirPgto = saidaValidarIncluirPgto;
	}

	/**
	 * Get: numContratoDetInc.
	 *
	 * @return numContratoDetInc
	 */
	public String getNumContratoDetInc() {
		return numContratoDetInc;
	}

	/**
	 * Set: numContratoDetInc.
	 *
	 * @param numContratoDetInc the num contrato det inc
	 */
	public void setNumContratoDetInc(String numContratoDetInc) {
		this.numContratoDetInc = numContratoDetInc;
	}

	/**
	 * Get: descServicoDetInc.
	 *
	 * @return descServicoDetInc
	 */
	public String getDescServicoDetInc() {
		return descServicoDetInc;
	}

	/**
	 * Set: descServicoDetInc.
	 *
	 * @param descServicoDetInc the desc servico det inc
	 */
	public void setDescServicoDetInc(String descServicoDetInc) {
		this.descServicoDetInc = descServicoDetInc;
	}

	/**
	 * Get: descModalidadeDetInc.
	 *
	 * @return descModalidadeDetInc
	 */
	public String getDescModalidadeDetInc() {
		return descModalidadeDetInc;
	}

	/**
	 * Set: descModalidadeDetInc.
	 *
	 * @param descModalidadeDetInc the desc modalidade det inc
	 */
	public void setDescModalidadeDetInc(String descModalidadeDetInc) {
		this.descModalidadeDetInc = descModalidadeDetInc;
	}

	/**
	 * Get: cpfCpnjDetInc.
	 *
	 * @return cpfCpnjDetInc
	 */
	public String getCpfCpnjDetInc() {
		return cpfCpnjDetInc;
	}

	/**
	 * Set: cpfCpnjDetInc.
	 *
	 * @param cpfCpnjDetInc the cpf cpnj det inc
	 */
	public void setCpfCpnjDetInc(String cpfCpnjDetInc) {
		this.cpfCpnjDetInc = cpfCpnjDetInc;
	}

	/**
	 * Get: razaoSocialDetInc.
	 *
	 * @return razaoSocialDetInc
	 */
	public String getRazaoSocialDetInc() {
		return razaoSocialDetInc;
	}

	/**
	 * Set: razaoSocialDetInc.
	 *
	 * @param razaoSocialDetInc the razao social det inc
	 */
	public void setRazaoSocialDetInc(String razaoSocialDetInc) {
		this.razaoSocialDetInc = razaoSocialDetInc;
	}

	/**
	 * Get: agenciaDebitoDetInc.
	 *
	 * @return agenciaDebitoDetInc
	 */
	public String getAgenciaDebitoDetInc() {
		return agenciaDebitoDetInc;
	}

	/**
	 * Set: agenciaDebitoDetInc.
	 *
	 * @param agenciaDebitoDetInc the agencia debito det inc
	 */
	public void setAgenciaDebitoDetInc(String agenciaDebitoDetInc) {
		this.agenciaDebitoDetInc = agenciaDebitoDetInc;
	}

	/**
	 * Get: contaDebitoDetInc.
	 *
	 * @return contaDebitoDetInc
	 */
	public String getContaDebitoDetInc() {
		return contaDebitoDetInc;
	}

	/**
	 * Set: contaDebitoDetInc.
	 *
	 * @param contaDebitoDetInc the conta debito det inc
	 */
	public void setContaDebitoDetInc(String contaDebitoDetInc) {
		this.contaDebitoDetInc = contaDebitoDetInc;
	}

	/**
	 * Get: periodoDeDetInc.
	 *
	 * @return periodoDeDetInc
	 */
	public String getPeriodoDeDetInc() {
		return periodoDeDetInc;
	}

	/**
	 * Set: periodoDeDetInc.
	 *
	 * @param periodoDeDetInc the periodo de det inc
	 */
	public void setPeriodoDeDetInc(String periodoDeDetInc) {
		this.periodoDeDetInc = periodoDeDetInc;
	}

	/**
	 * Get: ateDetInc.
	 *
	 * @return ateDetInc
	 */
	public String getAteDetInc() {
		return ateDetInc;
	}

	/**
	 * Set: ateDetInc.
	 *
	 * @param ateDetInc the ate det inc
	 */
	public void setAteDetInc(String ateDetInc) {
		this.ateDetInc = ateDetInc;
	}

	/**
	 * Get: entradaIncluirPgto.
	 *
	 * @return entradaIncluirPgto
	 */
	public IncluirSoliRelatorioPagamentoEntradaDTO getEntradaIncluirPgto() {
		return entradaIncluirPgto;
	}

	/**
	 * Set: entradaIncluirPgto.
	 *
	 * @param entradaIncluirPgto the entrada incluir pgto
	 */
	public void setEntradaIncluirPgto(
			IncluirSoliRelatorioPagamentoEntradaDTO entradaIncluirPgto) {
		this.entradaIncluirPgto = entradaIncluirPgto;
	}

	/**
	 * Get: saidaIncluirPgto.
	 *
	 * @return saidaIncluirPgto
	 */
	public IncluirSoliRelatorioPagamentoSaidaDTO getSaidaIncluirPgto() {
		return saidaIncluirPgto;
	}

	/**
	 * Set: saidaIncluirPgto.
	 *
	 * @param saidaIncluirPgto the saida incluir pgto
	 */
	public void setSaidaIncluirPgto(
			IncluirSoliRelatorioPagamentoSaidaDTO saidaIncluirPgto) {
		this.saidaIncluirPgto = saidaIncluirPgto;
	}

	public boolean isChkContrato() {
		return chkContrato;
	}

	public void setChkContrato(boolean chkContrato) {
		this.chkContrato = chkContrato;
	}

	public boolean isDisableArgumentosConsultaTipoServico() {
		return disableArgumentosConsultaTipoServico;
	}

	public void setDisableArgumentosConsultaTipoServico(
			boolean disableArgumentosConsultaTipoServico) {
		this.disableArgumentosConsultaTipoServico = disableArgumentosConsultaTipoServico;
	}

}
