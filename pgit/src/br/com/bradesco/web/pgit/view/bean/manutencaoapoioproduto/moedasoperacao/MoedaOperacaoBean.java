/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.moedasoperacao
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.moedasoperacao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.MoedaEconomicaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.MoedaEconomicaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.IMoedasOperacaoService;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.DetalharMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.DetalharMoedasOperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ExcluirMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ExcluirMoedasOperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.IncluirMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.IncluirMoedasOperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ListarMoedasOperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.moedasoperacao.bean.ListarMoedasOperacaoSaidaDTO;

/**
 * Nome: MoedaOperacaoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class MoedaOperacaoBean {
	
	/** Atributo listaGrid. */
	private List<ListarMoedasOperacaoSaidaDTO> listaGrid;
	
	/** Atributo manterMoedasOperacaoImpl. */
	private IMoedasOperacaoService manterMoedasOperacaoImpl;	
	
	/** Atributo filtroCodigoMoedaIndice. */
	private Integer filtroCodigoMoedaIndice;
	
	/** Atributo filtroTipoServico. */
	private Integer filtroTipoServico;
	
	/** Atributo filtroModalidadeServico. */
	private Integer filtroModalidadeServico;
	
	/** Atributo tipoServico. */
	private Integer tipoServico;
	
	/** Atributo modalidadeServico. */
	private Integer modalidadeServico;
	
	/** Atributo codigoMoedaIndice. */
	private Integer codigoMoedaIndice;
	
	/** Atributo voltarPesquisar. */
	private String voltarPesquisar;
	
	/** Atributo tipoServicoDesc. */
	private String tipoServicoDesc;
	
	/** Atributo tipoModalidadeDesc. */
	private String tipoModalidadeDesc;
	
	/** Atributo moedaIndiceDesc. */
	private String moedaIndiceDesc;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
		
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();	
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaCodigoMoedaIndice. */
	private List<SelectItem> listaCodigoMoedaIndice = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoIncluir. */
	private List<SelectItem> listaModalidadeServicoIncluir = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoIncluirHash. */
	private Map<Integer, ListarModalidadeSaidaDTO>  listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
	
	/** Atributo listaCodigoMoedaIndiceHash. */
	private Map<Integer, String> listaCodigoMoedaIndiceHash = new HashMap<Integer, String>();
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo filtroTipoRelacionamento. */
	private Integer filtroTipoRelacionamento;
	
	/** Atributo tipoRelacionamento. */
	private Integer tipoRelacionamento;
	
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {		
		
		
		setFiltroTipoServico(0);
		setFiltroModalidadeServico(0);
		setFiltroCodigoMoedaIndice(0);		
		setItemSelecionadoLista(null);	
		listaGrid = new ArrayList<ListarMoedasOperacaoSaidaDTO>();
		listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
		setListaGrid(null);
		limparCampos();
		preencheMoedaIndice();
		preencheListaModalidade();
		preencheListaTipoServico();
		setBtoAcionado(false);
		
	}
	
	
	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarMoedasOperacaoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarMoedasOperacaoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	

	/**
	 * Get: manterMoedasOperacaoImpl.
	 *
	 * @return manterMoedasOperacaoImpl
	 */
	public IMoedasOperacaoService getManterMoedasOperacaoImpl() {
		return manterMoedasOperacaoImpl;
	}

	/**
	 * Set: manterMoedasOperacaoImpl.
	 *
	 * @param manterMoedasOperacaoImpl the manter moedas operacao impl
	 */
	public void setManterMoedasOperacaoImpl(
			IMoedasOperacaoService manterMoedasOperacaoImpl) {
		this.manterMoedasOperacaoImpl = manterMoedasOperacaoImpl;
	}

	/**
	 * Get: codigoMoedaIndice.
	 *
	 * @return codigoMoedaIndice
	 */
	public Integer getCodigoMoedaIndice() {
		return codigoMoedaIndice;
	}

	/**
	 * Set: codigoMoedaIndice.
	 *
	 * @param codigoMoedaIndice the codigo moeda indice
	 */
	public void setCodigoMoedaIndice(Integer codigoMoedaIndice) {
		this.codigoMoedaIndice = codigoMoedaIndice;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: filtroCodigoMoedaIndice.
	 *
	 * @return filtroCodigoMoedaIndice
	 */
	public Integer getFiltroCodigoMoedaIndice() {
		return filtroCodigoMoedaIndice;
	}

	/**
	 * Set: filtroCodigoMoedaIndice.
	 *
	 * @param filtroCodigoMoedaIndice the filtro codigo moeda indice
	 */
	public void setFiltroCodigoMoedaIndice(Integer filtroCodigoMoedaIndice) {
		this.filtroCodigoMoedaIndice = filtroCodigoMoedaIndice;
	}

	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public Integer getModalidadeServico() {
		return modalidadeServico;
	}

	/**
	 * Set: modalidadeServico.
	 *
	 * @param operacao the modalidade servico
	 */
	public void setModalidadeServico(Integer operacao) {
		this.modalidadeServico = operacao;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public Integer getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param produto the tipo servico
	 */
	public void setTipoServico(Integer produto) {
		this.tipoServico = produto;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: voltarPesquisar.
	 *
	 * @return voltarPesquisar
	 */
	public String getVoltarPesquisar() {
		return voltarPesquisar;
	}

	/**
	 * Set: voltarPesquisar.
	 *
	 * @param voltarPesquisar the voltar pesquisar
	 */
	public void setVoltarPesquisar(String voltarPesquisar) {
		this.voltarPesquisar = voltarPesquisar;
	}

	
	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		
		ListarMoedasOperacaoSaidaDTO listarMoedasOperacaoSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
		
		DetalharMoedasOperacaoEntradaDTO detalharMoedasOperacaoEntradaDTO = new DetalharMoedasOperacaoEntradaDTO();
		
		detalharMoedasOperacaoEntradaDTO.setCodigoMoedaIndice(listarMoedasOperacaoSaidaDTO.getCodigoMoedaIndice());
		detalharMoedasOperacaoEntradaDTO.setModalidadeServico(listarMoedasOperacaoSaidaDTO.getCdModalidadeServico());
		detalharMoedasOperacaoEntradaDTO.setTipoRelacionamento(listarMoedasOperacaoSaidaDTO.getCdRelacionadoProduto());
		detalharMoedasOperacaoEntradaDTO.setTipoServico(listarMoedasOperacaoSaidaDTO.getCdTipoServico());
		
		DetalharMoedasOperacaoSaidaDTO detalharMoedasOperacaoSaidaDTO = getManterMoedasOperacaoImpl().detalharMoedasOperacao(detalharMoedasOperacaoEntradaDTO);
		
		setTipoServico(detalharMoedasOperacaoSaidaDTO.getCdTipoServico());
		setTipoServicoDesc(detalharMoedasOperacaoSaidaDTO.getDsTipoServico());
		setModalidadeServico(detalharMoedasOperacaoSaidaDTO.getCdModalidadeServico());
		setTipoModalidadeDesc(detalharMoedasOperacaoSaidaDTO.getDsModalidadeServico());
		setCodigoMoedaIndice(detalharMoedasOperacaoSaidaDTO.getCdMoedaIndice());
		setMoedaIndiceDesc(detalharMoedasOperacaoSaidaDTO.getCdMoedaIndice() + "-"  + detalharMoedasOperacaoSaidaDTO.getDsMoedaIndice());
		
		setUsuarioInclusao(detalharMoedasOperacaoSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharMoedasOperacaoSaidaDTO.getUsuarioManutencao());
		
		setComplementoInclusao(detalharMoedasOperacaoSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharMoedasOperacaoSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharMoedasOperacaoSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharMoedasOperacaoSaidaDTO.getComplementoManutencao());
		
		setTipoCanalInclusao(detalharMoedasOperacaoSaidaDTO.getCdCanalInclusao()==0? "" : detalharMoedasOperacaoSaidaDTO.getCdCanalInclusao() + " - " +  detalharMoedasOperacaoSaidaDTO.getDsCanalInclusao()); // TODO colocar a descri��o do canal de inclus�o
		setTipoCanalManutencao(detalharMoedasOperacaoSaidaDTO.getCdCanalManutencao()==0? "" : detalharMoedasOperacaoSaidaDTO.getCdCanalManutencao() + " - " + detalharMoedasOperacaoSaidaDTO.getDsCanalManutencao()); // TODO colocar a descri��o do canal de manuten��o
		
		setDataHoraInclusao(detalharMoedasOperacaoSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharMoedasOperacaoSaidaDTO.getDataHoraManutencao());
		
	}
	
	/**
	 * Avancar detalhar.
	 *
	 * @return the string
	 */
	public String avancarDetalhar(){
		
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "conManterMoedasSistema", BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "AVANCAR_DET";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		
		setTipoServico(0);
		setModalidadeServico(0);
		setCodigoMoedaIndice(0);
		
		
		return "AVANCAR_INC";
	}
	
	/**
	 * Avancar excluir.
	 *
	 * @return the string
	 */
	public String avancarExcluir(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "conManterMoedasSistema", BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
	
		return "AVANCAR_EXC";
	}
	
	/**
	 * Limpar campos.
	 */
	public void limparCampos(){
		
		setFiltroTipoServico(0);
		setFiltroModalidadeServico(0);
		setFiltroCodigoMoedaIndice(0);
		listaGrid = new ArrayList<ListarMoedasOperacaoSaidaDTO>();
		listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
		setItemSelecionadoLista(null);
		setBtoAcionado(false);
		
	}
	
	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		setItemSelecionadoLista(null);
		return "VOLTAR_CON";		
	}
	
	/**
	 * Avancar incluir confirmar.
	 *
	 * @return the string
	 */
	public String avancarIncluirConfirmar(){
		
		setMoedaIndiceDesc((String)listaCodigoMoedaIndiceHash.get(getCodigoMoedaIndice()));
		setTipoModalidadeDesc(listaModalidadeServicoIncluirHash.get(getModalidadeServico()).getDsModalidade());
		setTipoRelacionamento(listaModalidadeServicoIncluirHash.get(getModalidadeServico()).getCdRelacionamentoProduto());
		setTipoServicoDesc((String)listaTipoServicoHash.get(getTipoServico()));
		
		return "AVANCAR_INC2";
	}
	
		
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR_INC";
	}
	
	/**
	 * Pesquisar.
	 *
	 * @return the string
	 */
	public String pesquisar(){
		return "ok";
	}


	/**
	 * Get: moedaIndiceDesc.
	 *
	 * @return moedaIndiceDesc
	 */
	public String getMoedaIndiceDesc() {
		return moedaIndiceDesc;
	}


	/**
	 * Set: moedaIndiceDesc.
	 *
	 * @param codigoMoedaIndiceDesc the moeda indice desc
	 */
	public void setMoedaIndiceDesc(String codigoMoedaIndiceDesc) {
		this.moedaIndiceDesc = codigoMoedaIndiceDesc;
	}


	/**
	 * Get: tipoModalidadeDesc.
	 *
	 * @return tipoModalidadeDesc
	 */
	public String getTipoModalidadeDesc() {
		return tipoModalidadeDesc;
	}


	/**
	 * Set: tipoModalidadeDesc.
	 *
	 * @param operacaoDesc the tipo modalidade desc
	 */
	public void setTipoModalidadeDesc(String operacaoDesc) {
		this.tipoModalidadeDesc = operacaoDesc;
	}


	/**
	 * Get: tipoServicoDesc.
	 *
	 * @return tipoServicoDesc
	 */
	public String getTipoServicoDesc() {
		return tipoServicoDesc;
	}


	/**
	 * Set: tipoServicoDesc.
	 *
	 * @param produtoDesc the tipo servico desc
	 */
	public void setTipoServicoDesc(String produtoDesc) {
		this.tipoServicoDesc = produtoDesc;
	}

	//M�TODO CONSULTAR - CARREGA A GRID E REMOVE O RADIO SELECIONADO
	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar() {
		
		carregarGrid();
		
		return "ok";
	}
	
	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}


	/**
	 * Carregar grid.
	 */
	private void carregarGrid() {
		
		try{
			
			ListarMoedasOperacaoEntradaDTO entradaDTO = new ListarMoedasOperacaoEntradaDTO();
				
			entradaDTO.setCodigoMoedaIndice(getFiltroCodigoMoedaIndice()!=null?getFiltroCodigoMoedaIndice():0);
			entradaDTO.setModalidadeServico(getFiltroModalidadeServico()!=null?getFiltroModalidadeServico():0);
			entradaDTO.setTipoServico(getFiltroTipoServico()!=null?getFiltroTipoServico():0);
			if(getFiltroModalidadeServico() > 0){
				entradaDTO.setTipoRelacionamento(listaModalidadeServicoHash.isEmpty()? 0 :listaModalidadeServicoHash.get(getFiltroModalidadeServico()).getCdRelacionamentoProduto());
			}else{
				entradaDTO.setTipoRelacionamento(0);
			}

			setListaGrid(getManterMoedasOperacaoImpl().listarMoedasOperacao(entradaDTO));
			setBtoAcionado(true);
			this.listaControleRadio =  new ArrayList<SelectItem>();
			for (int i = 0; i <= getListaGrid().size();i++) {
				this.listaControleRadio.add(new SelectItem(i," "));
			}
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);

		}
		
	}

	/**
	 * Get: filtroModalidadeServico.
	 *
	 * @return filtroModalidadeServico
	 */
	public Integer getFiltroModalidadeServico() {
		return filtroModalidadeServico;
	}

	/**
	 * Set: filtroModalidadeServico.
	 *
	 * @param filtroModalidadeServico the filtro modalidade servico
	 */
	public void setFiltroModalidadeServico(Integer filtroModalidadeServico) {
		this.filtroModalidadeServico = filtroModalidadeServico;
	}

	/**
	 * Get: filtroTipoServico.
	 *
	 * @return filtroTipoServico
	 */
	public Integer getFiltroTipoServico() {
		return filtroTipoServico;
	}

	/**
	 * Set: filtroTipoServico.
	 *
	 * @param filtroTipoServico the filtro tipo servico
	 */
	public void setFiltroTipoServico(Integer filtroTipoServico) {
		this.filtroTipoServico = filtroTipoServico;
	}

	/**
	 * Get: listaModalidadeServico.
	 *
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {
		
		return listaModalidadeServico;
	}
	
	/**
	 * Get: listaModalidadeServicoIncluir.
	 *
	 * @return listaModalidadeServicoIncluir
	 */
	public List<SelectItem> getListaModalidadeServicoIncluir() {
		
		if (getTipoServico()!=null && getTipoServico()!=0){
			try{
				
				this.listaModalidadeServicoIncluir = new ArrayList<SelectItem>();
				
				List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
				ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
				
				listarModalidadeEntradaDTO.setCdServico(getTipoServico());
				
				listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
				listaModalidadeServicoIncluirHash.clear();
				for(ListarModalidadeSaidaDTO combo : listaModalidades){				
					this.listaModalidadeServicoIncluir.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
					listaModalidadeServicoIncluirHash.put(combo.getCdModalidade(), combo);						
				}
				
			} catch (PdcAdapterException p) {
				FacesContext.getCurrentInstance().addMessage("erro", new FacesMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage()));
				this.setListaModalidadeServico(new ArrayList<SelectItem>());
			} 
		}else{
			listaModalidadeServicoIncluir.clear();
			setFiltroModalidadeServico(0);
			setModalidadeServico(0);
		}
		
		return listaModalidadeServicoIncluir;
	}

	/**
	 * Set: listaModalidadeServico.
	 *
	 * @param listaModalidaeServico the lista modalidade servico
	 */
	public void setListaModalidadeServico(List<SelectItem> listaModalidaeServico) {
		this.listaModalidadeServico = listaModalidaeServico;
	}

	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
	
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: filtroTipoRelacionamento.
	 *
	 * @return filtroTipoRelacionamento
	 */
	public Integer getFiltroTipoRelacionamento() {
		return filtroTipoRelacionamento;
	}


	/**
	 * Set: filtroTipoRelacionamento.
	 *
	 * @param filtroTipoRelacionamento the filtro tipo relacionamento
	 */
	public void setFiltroTipoRelacionamento(Integer filtroTipoRelacionamento) {
		this.filtroTipoRelacionamento = filtroTipoRelacionamento;
	}
	
		
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {			
		return "ok";		
	}
	
	
	/**
	 * Carrega modalidade incluir.
	 */
	public void carregaModalidadeIncluir(){
		getListaModalidadeServicoIncluir();
	}
	


	/**
	 * Set: listaModalidadeServicoIncluir.
	 *
	 * @param listaModalidadeServicoIncluir the lista modalidade servico incluir
	 */
	public void setListaModalidadeServicoIncluir(
			List<SelectItem> listaModalidadeServicoIncluir) {
		this.listaModalidadeServicoIncluir = listaModalidadeServicoIncluir;
	}
	
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		
		try {
			IncluirMoedasOperacaoEntradaDTO incluirMoedasOperacaoEntradaDTO = new IncluirMoedasOperacaoEntradaDTO();
			
			incluirMoedasOperacaoEntradaDTO.setCodigoMoedaIndice(getCodigoMoedaIndice());
			incluirMoedasOperacaoEntradaDTO.setModalidadeServico(getModalidadeServico());
			incluirMoedasOperacaoEntradaDTO.setTipoRelacionamento(getTipoRelacionamento());
			incluirMoedasOperacaoEntradaDTO.setTipoServico(getTipoServico());
			
			IncluirMoedasOperacaoSaidaDTO incluirMoedasOperacaoSaidaDTO = getManterMoedasOperacaoImpl().incluirMoedasOperacao(incluirMoedasOperacaoEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +incluirMoedasOperacaoSaidaDTO.getCodMensagem() + ") " + incluirMoedasOperacaoSaidaDTO.getMensagem(), "conMoedasOperacao", BradescoViewExceptionActionType.ACTION, false);
			carregarGrid();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "incMoedasOperacao";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		
		try {
			ExcluirMoedasOperacaoEntradaDTO excluirMoedasOperacaoEntradaDTO = new ExcluirMoedasOperacaoEntradaDTO();
			
			ListarMoedasOperacaoSaidaDTO listarMoedasOperacaoSaidaDTO = getListaGrid().get(getItemSelecionadoLista());	
			
			
			excluirMoedasOperacaoEntradaDTO.setCodigoMoedaIndice(listarMoedasOperacaoSaidaDTO.getCodigoMoedaIndice());
			excluirMoedasOperacaoEntradaDTO.setModalidadeServico(listarMoedasOperacaoSaidaDTO.getCdModalidadeServico());
			excluirMoedasOperacaoEntradaDTO.setTipoRelacionamento(listarMoedasOperacaoSaidaDTO.getCdRelacionadoProduto());
			excluirMoedasOperacaoEntradaDTO.setTipoServico(listarMoedasOperacaoSaidaDTO.getCdTipoServico());
			
			ExcluirMoedasOperacaoSaidaDTO excluirMoedasOperacaoSaidaDTO = getManterMoedasOperacaoImpl().excluirMoedasOperacao(excluirMoedasOperacaoEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + excluirMoedasOperacaoSaidaDTO.getCodMensagem() + ") " + excluirMoedasOperacaoSaidaDTO.getMensagem(), "conMoedasOperacao", BradescoViewExceptionActionType.ACTION, false);

			carregarGrid();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "excMoedasOperacao";
	}


	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public Integer getTipoRelacionamento() {
		return tipoRelacionamento;
	}


	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(Integer tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Preenche lista modalidade.
	 */
	public void preencheListaModalidade(){
		try{
			listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
			listaModalidadeServico = new ArrayList<SelectItem>();
			
			if(getFiltroTipoServico() > 0){
				List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
				ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
				
				listarModalidadeEntradaDTO.setCdServico(getFiltroTipoServico());
			
				listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
				
				listaModalidadeServicoHash.clear();
				
				for(ListarModalidadeSaidaDTO combo : listaModalidades){				
					this.listaModalidadeServico.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
					listaModalidadeServicoHash.put(combo.getCdModalidade(),combo);
					
				}
			}else{
				listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
				setFiltroModalidadeServico(0);
				setModalidadeServico(0);
			}
		}catch(PdcAdapterFunctionalException e){
			listaModalidadeServico = new ArrayList<SelectItem>();
		}
	}
	
	/**
	 * Preenche lista tipo servico.
	 */
	public void preencheListaTipoServico(){
		try{
			
			this.listaTipoServico = new ArrayList<SelectItem>();
			
			List<ListarServicosSaidaDTO> listaServico = new ArrayList<ListarServicosSaidaDTO>();
			
			listaServico = comboService.listarTipoServicos(1);
			
			listaTipoServicoHash.clear();
			for(ListarServicosSaidaDTO combo : listaServico){
				listaTipoServicoHash.put(combo.getCdServico(), combo.getDsServico());
				listaTipoServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
			}
			
		} catch (PdcAdapterException p) {
			FacesContext.getCurrentInstance().addMessage("erro", new FacesMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage()));
			this.setListaTipoServico(new ArrayList<SelectItem>());
		} 
		
		
	}
	
	/**
	 * Preenche moeda indice.
	 *
	 * @return the list< select item>
	 */
	@SuppressWarnings("unchecked")
	public List<SelectItem> preencheMoedaIndice() {
		try{
			listaCodigoMoedaIndice = new ArrayList<SelectItem>();
			
			List<MoedaEconomicaSaidaDTO> listaCodigoMoeda = new ArrayList<MoedaEconomicaSaidaDTO>();
			MoedaEconomicaEntradaDTO moedaEconomicaEntradaDTO = new MoedaEconomicaEntradaDTO();
			moedaEconomicaEntradaDTO.setCdSituacao(0);
			
			listaCodigoMoeda = comboService.listarMoedaEconomica(moedaEconomicaEntradaDTO);
			listaCodigoMoedaIndiceHash.clear();
			for(MoedaEconomicaSaidaDTO combo : listaCodigoMoeda){
				listaCodigoMoedaIndiceHash.put(combo.getCdIndicadorEconomico(),combo.getDsIndicadorEconomico());
				listaCodigoMoedaIndice.add(new SelectItem(combo.getCdIndicadorEconomico(),combo.getDsIndicadorEconomico()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaCodigoMoedaIndice = new ArrayList<SelectItem>();
		}
				
		return listaCodigoMoedaIndice;
	}


	/**
	 * Get: listaCodigoMoedaIndice.
	 *
	 * @return listaCodigoMoedaIndice
	 */
	public List<SelectItem> getListaCodigoMoedaIndice() {
		return listaCodigoMoedaIndice;
	}


	/**
	 * Set: listaCodigoMoedaIndice.
	 *
	 * @param listaCodigoMoedaIndice the lista codigo moeda indice
	 */
	public void setListaCodigoMoedaIndice(List<SelectItem> listaCodigoMoedaIndice) {
		this.listaCodigoMoedaIndice = listaCodigoMoedaIndice;
	}


	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}


	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}
	
	
}
