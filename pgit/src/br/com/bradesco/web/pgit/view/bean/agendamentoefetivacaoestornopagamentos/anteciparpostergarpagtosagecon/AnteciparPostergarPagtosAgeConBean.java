/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.anteciparpostergarpagtosagecon
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.anteciparpostergarpagtosagecon;

import static br.com.bradesco.web.pgit.view.converters.FormatarData.formataDiaMesAnoToPdc;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.IAnteciparPostergarPagtosAgeConService;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.AntPostergarPagtosParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultaPostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultaPostergarConsolidadoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultarPagtosConsAntPostergacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ConsultarPagtosConsAntPostergacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalharAntPostergarPagtosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalharAntPostergarPagtosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalhePostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.DetalhePostergarConsolidadoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ExcluirPostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ExcluirPostergarConsolidadoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ListarPostergarConsolidadoSolicitacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ListarPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.ListarPostergarConsolidadoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.OcorrenciasAntPostergarPagtosParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.anteciparpostergarpagtosagecon.bean.OcorrenciasDetalharAntPostergarPagtosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

// TODO: Auto-generated Javadoc
/**
 * Nome: AnteciparPostergarPagtosAgeConBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AnteciparPostergarPagtosAgeConBean extends PagamentosBean {
	
	/** Atributo anteciparPostergarPagtosAgeConImpl. */
	private IAnteciparPostergarPagtosAgeConService anteciparPostergarPagtosAgeConImpl = null;

	// Lista Tela de Consultar
	/** Atributo listaGridConsultar. */
	private List<ConsultarPagtosConsAntPostergacaoSaidaDTO> listaGridConsultar = null;

	/** Atributo itemSelecionadoListaConsultar. */
	private Integer itemSelecionadoListaConsultar = null;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta = false;

	/** Atributo exibeBeneficiario. */
	private boolean exibeBeneficiario = false;

	// Lista Tela de Detalhar
	/** Atributo itemSelecionadoListaDetalhar. */
	private Integer itemSelecionadoListaDetalhar = null;

	/** Atributo listaGridDetalhar. */
	private List<OcorrenciasDetalharAntPostergarPagtosSaidaDTO> listaGridDetalhar = null;

	/** Atributo listaControleRadioDetalhe. */
	private List<SelectItem> listaControleRadioDetalhe = new ArrayList<SelectItem>();

	// Lista Alterar Parcial
	/** Atributo listaGridAlterarSelecionados. */
	private List<OcorrenciasDetalharAntPostergarPagtosSaidaDTO> listaGridAlterarSelecionados = null;

	/** Atributo itemSelecionadoAlterarParcial. */
	private Integer itemSelecionadoAlterarParcial = null;

	/** Atributo listaControleRadioAlterarParcial. */
	private List<SelectItem> listaControleRadioAlterarParcial = new ArrayList<SelectItem>();

	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos = false;

	/** Atributo panelBotoes. */
	private boolean panelBotoes = false;

	// Cabecalho
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf = null;

	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial = null;

	/** Atributo dsEmpresa. */
	private String dsEmpresa = null;

	/** Atributo nroContrato. */
	private String nroContrato = null;

	/** Atributo dsContrato. */
	private String dsContrato = null;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato = null;

	// Conta D�bito
	/** Atributo dsBancoDebito. */
	private String dsBancoDebito = null;

	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito = null;

	/** Atributo contaDebito. */
	private String contaDebito = null;

	/** Atributo tipoContaDebito. */
	private String tipoContaDebito = null;

	// Favorecido
	/** Atributo numeroInscricaoFavorecido. */
	private String numeroInscricaoFavorecido = null;

	/** Atributo tipoFavorecido. */
	private Integer tipoFavorecido = null;

	/** Atributo dsFavorecido. */
	private String dsFavorecido = null;

	/** Atributo cdFavorecido. */
	private String cdFavorecido = null;

	/** Atributo dsBancoFavorecido. */
	private String dsBancoFavorecido = null;

	/** Atributo dsAgenciaFavorecido. */
	private String dsAgenciaFavorecido = null;

	/** Atributo dsContaFavorecido. */
	private String dsContaFavorecido = null;

	/** Atributo dsTipoContaFavorecido. */
	private String dsTipoContaFavorecido = null;
	
	/** Atributo numControleInternoLote. */
	private Long numControleInternoLote = null;

	// Beneficiario
	/** Atributo numeroInscricaoBeneficiario. */
	private String numeroInscricaoBeneficiario = null;

	/** Atributo cdTipoBeneficiario. */
	private Integer cdTipoBeneficiario = null;

	/** Atributo tipoBeneficiario. */
	private String tipoBeneficiario = null;

	/** Atributo nomeBeneficiario. */
	private String nomeBeneficiario = null;

	/** Atributo dtNascimentoBeneficiario. */
	private String dtNascimentoBeneficiario = null;

	/** Atributo dsTipoBeneficiario. */
	private String dsTipoBeneficiario = null;

	// Pagamentos
	/** Atributo numeroPagamento. */
	private String numeroPagamento = null;

	/** Atributo qtdePagamentos. */
	private Long qtdePagamentos = null;

	/** Atributo qtdPagamentoAutorizados. */
	private Integer qtdPagamentoAutorizados = null;

	/** Atributo vlrPagamentosAutorizados. */
	private BigDecimal vlrPagamentosAutorizados = null;

	/** Atributo vlTotalPagamentos. */
	private BigDecimal vlTotalPagamentos = null;

	/** Atributo novaDataAgendamento. */
	private Date novaDataAgendamento = null;

	/** Atributo tipoAgendamento. */
	private String tipoAgendamento = null;

	/** Atributo dataAgendamento. */
	private String dataAgendamento = null;

	/** Atributo dsTipoServico. */
	private String dsTipoServico = null;

	/** Atributo dsIndicadorModalidade. */
	private String dsIndicadorModalidade = null;

	/** Atributo nrSequenciaArquivoRemessa. */
	private String nrSequenciaArquivoRemessa = null;

	/** Atributo nrLoteArquivoRemessa. */
	private String nrLoteArquivoRemessa = null;

	/** Atributo nroPagamento. */
	private String nroPagamento = null;
	
	/** Atributo cdListaDebito. */
	private String cdListaDebito = null;

	/** Atributo dtAgendamento. */
	private String dtAgendamento = null;

	/** Atributo dtPagamento. */
	private String dtPagamento = null;
	
	/** Atributo dtDevolucaoEstorno. */
	private String dtDevolucaoEstorno = null;

	/** Atributo dtVencimento. */
	private String dtVencimento = null;

	/** Atributo cdIndicadorEconomicoMoeda. */
	private String cdIndicadorEconomicoMoeda = null;

	/** Atributo qtMoeda. */
	private BigDecimal qtMoeda = null;

	/** Atributo vlrAgendamento. */
	private BigDecimal vlrAgendamento = null;

	/** Atributo vlrEfetivacao. */
	private BigDecimal vlrEfetivacao = null;

	/** Atributo cdSituacaoOperacaoPagamento. */
	private String cdSituacaoOperacaoPagamento = null;

	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao = null;

	/** Atributo dsPagamento. */
	private String dsPagamento = null;		

	/** Atributo nrDocumento. */
	private String nrDocumento = null;

	/** Atributo tipoDocumento. */
	private String tipoDocumento = null;

	/** Atributo cdSerieDocumento. */
	private String cdSerieDocumento = null;

	/** Atributo vlDocumento. */
	private BigDecimal vlDocumento = null;

	/** Atributo dtEmissaoDocumento. */
	private String dtEmissaoDocumento = null;

	/** Atributo dsUsoEmpresa. */
	private String dsUsoEmpresa = null;

	/** Atributo dsMensagemPrimeiraLinha. */
	private String dsMensagemPrimeiraLinha = null;

	/** Atributo dsMensagemSegundaLinha. */
	private String dsMensagemSegundaLinha = null;

	/** Atributo cdBancoCredito. */
	private String cdBancoCredito = null;

	/** Atributo agenciaDigitoCredito. */
	private String agenciaDigitoCredito = null;

	/** Atributo contaDigitoCredito. */
	private String contaDigitoCredito = null;

	/** Atributo tipoContaCredito. */
	private String tipoContaCredito = null;

	/** Atributo cdBancoDestino. */
	private String cdBancoDestino = null;

	/** Atributo agenciaDigitoDestino. */
	private String agenciaDigitoDestino = null;

	/** Atributo contaDigitoDestino. */
	private String contaDigitoDestino = null;

	/** Atributo tipoContaDestino. */
	private String tipoContaDestino = null;

	/** Atributo vlDesconto. */
	private BigDecimal vlDesconto = null;

	/** Atributo vlAbatimento. */
	private BigDecimal vlAbatimento = null;

	/** Atributo vlMulta. */
	private BigDecimal vlMulta = null;

	/** Atributo vlMora. */
	private BigDecimal vlMora = null;

	/** Atributo vlDeducao. */
	private BigDecimal vlDeducao = null;

	/** Atributo vlAcrescimo. */
	private BigDecimal vlAcrescimo = null;

	/** Atributo vlImpostoRenda. */
	private BigDecimal vlImpostoRenda = null;

	/** Atributo vlIss. */
	private BigDecimal vlIss = null;

	/** Atributo vlIof. */
	private BigDecimal vlIof = null;

	/** Atributo vlInss. */
	private BigDecimal vlInss = null;

	/** Atributo camaraCentralizadora. */
	private String camaraCentralizadora = null;

	/** Atributo finalidadeDoc. */
	private String finalidadeDoc = null;

	/** Atributo identificacaoTransferenciaDoc. */
	private String identificacaoTransferenciaDoc = null;

	/** Atributo identificacaoTitularidade. */
	private String identificacaoTitularidade = null;

	/** Atributo finalidadeTed. */
	private String finalidadeTed = null;

	/** Atributo identificacaoTransferenciaTed. */
	private String identificacaoTransferenciaTed = null;

	/** Atributo numeroOp. */
	private String numeroOp = null;

	/** Atributo dataExpiracaoCredito. */
	private String dataExpiracaoCredito = null;

	/** Atributo instrucaoPagamento. */
	private String instrucaoPagamento = null;

	/** Atributo numeroCheque. */
	private String numeroCheque = null;

	/** Atributo serieCheque. */
	private String serieCheque = null;

	/** Atributo indicadorAssociadoUnicaAgencia. */
	private String indicadorAssociadoUnicaAgencia = null;

	/** Atributo identificadorTipoRetirada. */
	private String identificadorTipoRetirada = null;

	/** Atributo identificadorRetirada. */
	private String identificadorRetirada = null;

	/** Atributo dataRetirada. */
	private String dataRetirada = null;

	/** Atributo vlImpostoMovFinanceira. */
	private BigDecimal vlImpostoMovFinanceira = null;

	/** Atributo codigoBarras. */
	private String codigoBarras = null;

	/** Atributo dddContribuinte. */
	private String dddContribuinte = null;

	/** Atributo telefoneContribuinte. */
	private String telefoneContribuinte = null;

	/** Atributo inscricaoEstadualContribuinte. */
	private String inscricaoEstadualContribuinte = null;

	/** Atributo agenteArrecadador. */
	private String agenteArrecadador = null;

	/** Atributo codigoReceita. */
	private String codigoReceita = null;

	/** Atributo numeroReferencia. */
	private String numeroReferencia = null;

	/** Atributo numeroCotaParcela. */
	private String numeroCotaParcela = null;

	/** Atributo percentualReceita. */
	private BigDecimal percentualReceita = null;

	/** Atributo periodoApuracao. */
	private String periodoApuracao = null;

	/** Atributo anoExercicio. */
	private String anoExercicio = null;

	/** Atributo dataReferencia. */
	private String dataReferencia = null;

	/** Atributo vlReceita. */
	private BigDecimal vlReceita = null;

	/** Atributo vlPrincipal. */
	private BigDecimal vlPrincipal = null;

	/** Atributo vlAtualizacaoMonetaria. */
	private BigDecimal vlAtualizacaoMonetaria = null;

	/** Atributo vlTotal. */
	private BigDecimal vlTotal = null;

	/** Atributo codigoCnae. */
	private String codigoCnae = null;

	/** Atributo placaVeiculo. */
	private String placaVeiculo = null;

	/** Atributo numeroParcelamento. */
	private String numeroParcelamento = null;

	/** Atributo codigoOrgaoFavorecido. */
	private String codigoOrgaoFavorecido = null;

	/** Atributo nomeOrgaoFavorecido. */
	private String nomeOrgaoFavorecido = null;

	/** Atributo codigoUfFavorecida. */
	private String codigoUfFavorecida = null;

	/** Atributo descricaoUfFavorecida. */
	private String descricaoUfFavorecida = null;

	/** Atributo vlAcrescimoFinanceiro. */
	private BigDecimal vlAcrescimoFinanceiro = null;

	/** Atributo vlHonorarioAdvogado. */
	private BigDecimal vlHonorarioAdvogado = null;

	/** Atributo observacaoTributo. */
	private String observacaoTributo = null;

	/** Atributo codigoInss. */
	private String codigoInss = null;

	/** Atributo dataCompetencia. */
	private String dataCompetencia = null;

	/** Atributo vlOutrasEntidades. */
	private BigDecimal vlOutrasEntidades = null;

	/** Atributo codigoTributo. */
	private String codigoTributo = null;

	/** Atributo codigoRenavam. */
	private String codigoRenavam = null;

	/** Atributo municipioTributo. */
	private String municipioTributo = null;

	/** Atributo ufTributo. */
	private String ufTributo = null;

	/** Atributo numeroNsu. */
	private String numeroNsu = null;

	/** Atributo opcaoTributo. */
	private String opcaoTributo = null;

	/** Atributo opcaoRetiradaTributo. */
	private String opcaoRetiradaTributo = null;

	/** Atributo clienteDepositoIdentificado. */
	private String clienteDepositoIdentificado = null;

	/** Atributo tipoDespositoIdentificado. */
	private String tipoDespositoIdentificado = null;

	/** Atributo produtoDepositoIdentificado. */
	private String produtoDepositoIdentificado = null;

	/** Atributo sequenciaProdutoDepositoIdentificado. */
	private String sequenciaProdutoDepositoIdentificado = null;

	/** Atributo bancoCartaoDepositoIdentificado. */
	private String bancoCartaoDepositoIdentificado = null;

	/** Atributo cartaoDepositoIdentificado. */
	private String cartaoDepositoIdentificado = null;

	/** Atributo bimCartaoDepositoIdentificado. */
	private String bimCartaoDepositoIdentificado = null;

	/** Atributo viaCartaoDepositoIdentificado. */
	private String viaCartaoDepositoIdentificado = null;

	/** Atributo codigoDepositante. */
	private String codigoDepositante = null;

	/** Atributo nomeDepositante. */
	private String nomeDepositante = null;

	/** Atributo codigoPagador. */
	private String codigoPagador = null;

	/** Atributo codigoOrdemCredito. */
	private String codigoOrdemCredito = null;

	/** Atributo nossoNumero. */
	private String nossoNumero = null;

	/** Atributo dsOrigemPagamento. */
	private String dsOrigemPagamento = null;

	/** Atributo cdIndentificadorJudicial. */
	private String cdIndentificadorJudicial = null;

	/** Atributo dtLimiteDescontoPagamento. */
	private String dtLimiteDescontoPagamento = null;
	
	/** Atributo dtLimitePagamento. */
	private String dtLimitePagamento = null;
	
	/** Atributo dsRastreado. */
	private String dsRastreado = null;

	/** Atributo carteira. */
	private String carteira = null;

	/** Atributo cdSituacaoTranferenciaAutomatica. */
	private String cdSituacaoTranferenciaAutomatica = null;

	/** Atributo nrRemessa. */
	private Long nrRemessa = null;

	/** Atributo descTipoFavorecido. */
	private String descTipoFavorecido = null;

	// Trilha de Auditoria
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao = null;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao = null;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao = null;

	/** Atributo complementoInclusao. */
	private String complementoInclusao = null;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao = null;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao = null;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao = null;

	/** Atributo complementoManutencao. */
	private String complementoManutencao = null;

	/** Atributo cdBancoOriginal. */
	private Integer cdBancoOriginal = null;

	/** Atributo dsBancoOriginal. */
	private String dsBancoOriginal = null;

	/** Atributo cdAgenciaBancariaOriginal. */
	private Integer cdAgenciaBancariaOriginal = null;

	/** Atributo cdDigitoAgenciaOriginal. */
	private String cdDigitoAgenciaOriginal = null;

	/** Atributo dsAgenciaOriginal. */
	private String dsAgenciaOriginal = null;

	/** Atributo cdContaBancariaOriginal. */
	private Long cdContaBancariaOriginal = null;

	/** Atributo cdDigitoContaOriginal. */
	private String cdDigitoContaOriginal = null;

	/** Atributo dsTipoContaOriginal. */
	private String dsTipoContaOriginal = null;
	
	/** Atributo dtFloatingPagamento. */
	private String dtFloatingPagamento = null;
	
	/** Atributo dtEfetivFloatPgto. */
	private String dtEfetivFloatPgto = null;
	
	/** Atributo vlFloatingPagamento. */
	private BigDecimal vlFloatingPagamento = null;
	
	/** Atributo cdOperacaoDcom. */
	private Long cdOperacaoDcom = null;

	/** Atributo dsSituacaoDcom. */
	private String dsSituacaoDcom = null;
	
	// Flag
	/** Atributo exibeDcom. */
	private Boolean exibeDcom = null;

	// relatorio
	/** Atributo logoPath. */
	private String logoPath = null;
	
	/** Atributo saidaDetalhePostergarConsolidadoSolicitacao. */
	private DetalhePostergarConsolidadoSolicitacaoSaidaDTO saidaDetalhePostergarConsolidadoSolicitacao = null;
	
	/** Atributo listaPostergarConsolidadoSolicitacao. */
	private List<ListarPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO> listaPostergarConsolidadoSolicitacao = null;
	
	/** Atributo listaConsultaPostergarConsolidadoSolicitacao. */
	private List<ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO> listaConsultaPostergarConsolidadoSolicitacao = null;
	
	/** Atributo listaConsultaPostergarConsolidadoSolicitacaoControle. */
	private List<SelectItem> listaConsultaPostergarConsolidadoSolicitacaoControle = new ArrayList<SelectItem>();
	
	/** Atributo listaConsultarSituacaoSolicitacaoEstorno. */
	private List<SelectItem> listaConsultarSituacaoSolicitacaoEstorno = new ArrayList<SelectItem>();
	
	/** Atributo listaConsultarMotivoSituacaoEstorno. */
	private List<SelectItem> listaConsultarMotivoSituacaoEstorno = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionadoConsultaPostergarConsolidado. */
	private Integer itemSelecionadoConsultaPostergarConsolidado = null;
	
	/** Atributo itemSelecionadoComboSolicitacao. */
	private Integer itemSelecionadoComboSolicitacao = null;
	
	/** Atributo itemSelecionadoComboMotivo. */
	private Integer itemSelecionadoComboMotivo = null;
	
	/** Atributo dataFinalSolicitacaoFiltro. */
	private Date dataFinalSolicitacaoFiltro = null; 
	
	/** Atributo dataInicialSolicitacaoFiltro. */
	private Date dataInicialSolicitacaoFiltro = null; 
	
	/** The cdpessoa juridica contrato. */
    private Long cdpessoaJuridicaContrato = null;

    /** The cd tipo contrato negocio. */
    private Integer cdTipoContratoNegocio = null;

    /** The nr sequencia contrato negocio. */
    private Long nrSequenciaContratoNegocio = null;
  
    /** Atributo desabilitaBotaoExcluir. */
    private boolean desabilitaBotaoExcluir = true;
    
    private boolean desabilitaArgumentoDePesquisa = false;

    private boolean desabilitaComboMotivo = false;
    
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		limparTudo();

		// Tela de Filtro
		getFiltroAgendamentoEfetivacaoEstornoBean().setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno("conAnteciparPostergarPagtosAgeCon");
		getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);

		// Carregamento dos combos
		listarTipoServicoPagto();
		setHabilitaArgumentosPesquisa(false);
		getFiltroAgendamentoEfetivacaoEstornoBean().setClienteContratoSelecionado(false);

		setDisableArgumentosConsulta(false);
		setAgendadosPagosNaoPagos("1");
	}

	// M�todo sobreposto para n�o limpar os agendados - pagos/nao pagos
	/**
	 * (non-Javadoc).
	 *
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#limparCampos()
	 */
	public void limparCampos() {
		getFiltroAgendamentoEfetivacaoEstornoBean().limparParticipanteContrato();
		setChkContaDebito(false);
		limparContaDebito();
		limparRemessaFavorecidoBeneficiarioLinha();
		limparSituacaoMotivo();
		limparTipoServicoModalidade();

		setAgendadosPagosNaoPagos("1");
		setDataInicialPagamentoFiltro(new Date());
		setDataFinalPagamentoFiltro(new Date());
		setChkParticipanteContrato(false);
		setChkRemessa(false);
		setChkServicoModalidade(false);
		setChkSituacaoMotivo(false);
		setRadioPesquisaFiltroPrincipal("");
		setRadioFavorecidoFiltro("");
		setRadioBeneficiarioFiltro("");

		setClassificacaoFiltro(null);

		setChkNumeroListaDebito(false);
		setNumListaDebitoDeFiltro("");
		setNumListaDebitoAteFiltro("");

		setCboSituacaoListaDebito(null);
		setChkSituacaoListaDebito(false);
		setValorPagamentoAteFiltro(null);
		setValorPagamentoDeFiltro(null);
	}

	/**
	 * Consultar.
	 *
	 * @param evt the evt
	 */
	public void consultar(ActionEvent evt) {
		consultar();
	}

	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar() {
		try {
			limparInformacaoConsulta();

			ConsultarPagtosConsAntPostergacaoEntradaDTO entrada = new ConsultarPagtosConsAntPostergacaoEntradaDTO();
			setItemSelecionadoListaConsultar(null);

			/*
			 * Se selecionado �Filtrar por Cliente� ou �Filtrar por Contrato� mover �1�. Se selecionado �Filtrar por
			 * Conta D�bito� mover �2�;
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")
							|| getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) {
				entrada.setCdTipoPesquisa(1);
			} else {
				entrada.setCdTipoPesquisa(2);
			}

			entrada.setCdAgendamentoPagosNaoPagos(Integer.parseInt(getAgendadosPagosNaoPagos()));

			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")
							|| getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) {
				/*
				 * >> Se Selecionado Filtrar por Cliente Mover Valores vindos da lista >> Se Selecionado Filtrar por
				 * Contrato Passar valor retornado da lista(Verificado por email), na especifica��o definia passar os
				 * combo e inputtext, o que poderia gerar problemas caso o usu�rio altere os valores do combo ap�s ter
				 * feito a pesquisa
				 */
				entrada.setCdPessoaJuridica(getFiltroAgendamentoEfetivacaoEstornoBean().getCdPessoaJuridicaContrato());
				entrada.setCdTipoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getCdTipoContratoNegocio());
				entrada.setNrSequencialContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio());
			}

			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
				/*
				 * Se selecionado �Filtrar por Conta D�bito� mover zeros para os cambos abaixo
				 */
				entrada.setCdPessoaJuridica(0L);
				entrada.setCdTipoContrato(0);
				entrada.setNrSequencialContrato(0L);
			}

			entrada.setDtInicialPagamento(FormatarData.formataDiaMesAno(getDataInicialPagamentoFiltro()));
			entrada.setDtFinalPagamento(FormatarData.formataDiaMesAno(getDataFinalPagamentoFiltro()));

			entrada.setNrRemessa(getNumeroRemessaFiltro().equals("") ? 0L : Long.parseLong(getNumeroRemessaFiltro()));

			/*
			 * Se estiver selecionado "Filtrar por Conta D�bito", passar os campos deste filtro Se estiver selecionado
			 * outro tipo de filtro, verificar se o "Conta D�bito" dos argumentos de pesquisa esta marcado Se estiver
			 * marcado, passar os valores correspondentes da tela,caso contr�rio passar zeros
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
				entrada.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean().getBancoContaDebitoFiltro());
				entrada.setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean().getAgenciaContaDebitoBancariaFiltro());
				entrada.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean().getContaBancariaContaDebitoFiltro());
				entrada.setCdDigitoConta(getFiltroAgendamentoEfetivacaoEstornoBean().getDigitoContaDebitoFiltro());
			} else {
				if (isChkContaDebito()) {
					entrada.setCdBanco(getCdBancoContaDebito());
					entrada.setCdAgencia(getCdAgenciaBancariaContaDebito());
					entrada.setCdConta(getCdContaBancariaContaDebito());
					entrada.setCdDigitoConta(getCdDigitoContaContaDebito());
				} else {
					entrada.setCdBanco(0);
					entrada.setCdAgencia(0);
					entrada.setCdConta(0L);
					entrada.setCdDigitoConta("00");
				}
			}

			entrada.setCdProdutoServicoOperacao(getTipoServicoFiltro());
			entrada.setCdProdutoServicoRelacionado(getModalidadeFiltro());

			// Mover zeros
			entrada.setCdSituacaoOperacaoPagamento(0);
			entrada.setCdMotivoSituacaoPagamento(0);
			entrada.setCdTipoAgendamento(Integer.parseInt(getTipoAgendamento()));

			setListaGridConsultar(getAnteciparPostergarPagtosAgeConImpl().consultarPagtosConsAntPostergacao(entrada));

			setDisableArgumentosConsulta(true);

			for (int i = 0; i < getListaGridConsultar().size(); i++) {
				getListaControleRadio().add(new SelectItem(i, " "));
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setDisableArgumentosConsulta(false);
			setListaGridConsultar(null);
		}

		return "";
	}

	/**
	 * Limpar variaveis.
	 */
	public void limparVariaveis() {
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroPagamento("");
		setDsContrato("");
		setCdSituacaoContrato("");
		setDsBancoDebito("");
		setDsAgenciaDebito("");
		setContaDebito(null);
		setTipoContaDebito("");
	}

	/**
	 * Verificar tipo agendamento.
	 */
	public void verificarTipoAgendamento() {
		// SE TIPO DE AGENDAMENTO FOR ANTECIPAR PROCESSAMENTO ENT�O A DATA DEVE
		// SER IGUAL A DATA ATUAL
		if (getTipoAgendamento() != null && getTipoAgendamento().equals("1")) {
			setNovaDataAgendamento(new Date());
		}
	}

	/**
	 * Carrega lista detalhar.
	 */
	public void carregaListaDetalhar() {
		setListaGridDetalhar(new ArrayList<OcorrenciasDetalharAntPostergarPagtosSaidaDTO>());

		DetalharAntPostergarPagtosEntradaDTO entradaDTO = new DetalharAntPostergarPagtosEntradaDTO();
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionado = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setNrCnpjCpf(registroSelecionado.getNrCnpjCpf());
		entradaDTO.setNrFilialcnpjCpf(registroSelecionado.getNrFilialCnpjCpf());
		entradaDTO.setNrControleCnpjCpf(registroSelecionado.getCdDigitoCnpjCpf());
		entradaDTO.setNrArquivoRemessaPagamento(registroSelecionado.getNrRemessa());
		entradaDTO.setDtCreditoPagamento(registroSelecionado.getDtPagamento());
		entradaDTO.setCdBanco(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgencia(registroSelecionado.getCdAgencia());
		entradaDTO.setCdConta(registroSelecionado.getCdConta());
		entradaDTO.setCdDigitoConta(SiteUtil.formatNumber(registroSelecionado.getCdDigitoConta(), 2));
		entradaDTO.setCdProdutoServicoOperacao(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
		entradaDTO.setCdSituacaoOperacaoPagamento(registroSelecionado.getCdSituacaoOperacaoPagamento());
		entradaDTO.setCdAgendadoPagaoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdPessoaContratoDebito(registroSelecionado.getCdPessoaContratoDebito());
		entradaDTO.setCdTipoContratoDebito(registroSelecionado.getCdTipoContratoDebito());
		entradaDTO.setNrSequenciaContratoDebito(registroSelecionado.getNrSequenciaContratoDebito());

		DetalharAntPostergarPagtosSaidaDTO detalharAntPostergarPagtosSaidaDTO = getAnteciparPostergarPagtosAgeConImpl()
						.detalharAntPostergarPagtos(entradaDTO);

		setListaGridDetalhar(getAnteciparPostergarPagtosAgeConImpl().detalharAntPostergarPagtos(entradaDTO)
						.getListaDetalharAntPostergarPagtos());

		setDsTipoServico(registroSelecionado.getDsResumoProdutoServico());
		setDsIndicadorModalidade(registroSelecionado.getDsOperacaoProdutoServico());
		setQtdePagamentos(registroSelecionado.getQtPagamento());
		setVlTotalPagamentos(registroSelecionado.getVlPagamento());
		setNovaDataAgendamento(new Date());

		// in�cio do preenchimento do cabecalho
		setNrCnpjCpf(CpfCnpjUtils.formatCpfCnpjCompleto(registroSelecionado.getNrCnpjCpf(), registroSelecionado
						.getNrFilialCnpjCpf(), registroSelecionado.getCdDigitoCnpjCpf()));
		setDsEmpresa(registroSelecionado.getDsRazaoSocial());
		setNroContrato(Long.toString(registroSelecionado.getNrContrato()));

		setDsRazaoSocial(detalharAntPostergarPagtosSaidaDTO.getNomeCliente());
		setDsContrato(detalharAntPostergarPagtosSaidaDTO.getDsContrato());
		setCdSituacaoContrato(detalharAntPostergarPagtosSaidaDTO.getDsSituacaoContrato());

		setDsBancoDebito(PgitUtil.concatenarCampos(registroSelecionado.getCdBanco(), detalharAntPostergarPagtosSaidaDTO
						.getDsBancoDebito()));
		setDsAgenciaDebito(PgitUtil.formatAgencia(registroSelecionado.getCdAgencia(), registroSelecionado
						.getCdDigitoAgencia(), detalharAntPostergarPagtosSaidaDTO.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(registroSelecionado.getCdConta(), registroSelecionado.getCdDigitoConta(),
						false));
		setTipoContaDebito(detalharAntPostergarPagtosSaidaDTO.getDsTipoContaDebito());

		setNrRemessa(registroSelecionado.getNrRemessa());
		setDtPagamento(registroSelecionado.getDtPagamento());
		// fim do preenchimento do cabecalho

		setItemSelecionadoListaDetalhar(null);
		setListaControleRadioDetalhe(new ArrayList<SelectItem>());
		for (int i = 0; i < getListaGridDetalhar().size(); i++) {
			getListaControleRadioDetalhe().add(new SelectItem(i, ""));
		}

	}

	/**
	 * Preenche dados pagto credito conta.
	 */
	public void preencheDadosPagtoCreditoConta() {

		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoCreditoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoCreditoConta(entradaDTO);

		// Cabe�alho sempre preenchido
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
						.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(), saidaDTO.getDsBancoDebito(), false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
						saidaDTO.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(), saidaDTO.getDsDigAgenciaDebito(), false));
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidade() == 1 || saidaDTO.getCdIndicadorModalidade() == 3) {
			// Favorecido
			setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
							.getNmInscriFavorecido()));
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null
							: saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(),
							false));
			setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
							.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
			setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
							false));
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidade() == 1 ? true : false);
		} else {
			// Benefici�rio
			setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlrAgendamento());
		setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
						.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil
						.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
		setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
		setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
		setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setCdSituacaoTranferenciaAutomatica(saidaDTO.getCdSituacaoTranferenciaAutomatica());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());	
		setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

		// Favorecidos
		setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
		setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - " + saidaDTO.getDsBancoOriginal());
		setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
		setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
		setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-" + saidaDTO.getCdDigitoAgenciaOriginal()
						+ " - " + saidaDTO.getDsAgenciaOriginal());
		setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
		setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-" + saidaDTO.getCdDigitoContaOriginal());
		setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());
	}

	/**
	 * Preenche dados pagto debito conta.
	 */
	public void preencheDadosPagtoDebitoConta() {

		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoDebitoContaEntradaDTO entradaDTO = new DetalharPagtoDebitoContaEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDebitoConta(entradaDTO);

		// Cabe�alho - Cliente
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
						.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());

		// Sempre Carregar Conta de D�bito
		setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(), saidaDTO.getDsBancoDebito(), false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
						saidaDTO.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(), saidaDTO.getDsDigAgenciaDebito(), false));
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// Favorecido
		setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
						.getNmInscriFavorecido()));
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(),
						saidaDTO.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlrAgendamento());
		setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());

		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
						.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil
						.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());

		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());	
		

		// trilhaAuditoria
		setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
		setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
		setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto doc.
	 */
	public void preencheDadosPagtoDOC() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoDOCEntradaDTO entradaDTO = new DetalharPagtoDOCEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoDOC(entradaDTO);

		limparCamposFavorecidoBeneficiario();

		// Cabecalho
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
						.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			// Favorecido
			setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
							.getNmInscriFavorecido()));
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			// Benefici�rio
			setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| !PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals("")
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
		setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
		setIdentificacaoTransferenciaDoc(saidaDTO.getDsIdentificacaoTransferenciaPagto());
		setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());	
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto ted.
	 */
	public void preencheDadosPagtoTED() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoTEDEntradaDTO entradaDTO = new DetalharPagtoTEDEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTEDSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoTED(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
		setFinalidadeTed(saidaDTO.getFinalidadeTedFormatada());
		setIdentificacaoTransferenciaTed(saidaDTO.getDsIdentificacaoTransferenciaPagto());
		setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatidoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlIrCredito());
		setVlIss(saidaDTO.getVlIssCredito());
		setVlIof(saidaDTO.getVlIofCredito());
		setVlInss(saidaDTO.getVlInssCredito());
		setCdIndentificadorJudicial(saidaDTO.getCdIndentificadorJudicial());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());	
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto ordem pagto.
	 */
	public void preencheDadosPagtoOrdemPagto() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharPagtoOrdemPagtoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoOrdemPagtoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoOrdemPagto(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			// Favorecido
			setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			// Benefici�rio
			setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setNumeroOp(saidaDTO.getNrOperacao());
		setDataExpiracaoCredito(saidaDTO.getDtExpedicaoCredito());
		setInstrucaoPagamento(saidaDTO.getCdInsPagamento());
		setNumeroCheque(saidaDTO.getNrCheque());
		setSerieCheque(saidaDTO.getNrSerieCheque());
		setIndicadorAssociadoUnicaAgencia(saidaDTO.getDsUnidadeAgencia());
		setIdentificadorTipoRetirada(saidaDTO.getCdIdentificacaoTipoRetirada());
		setIdentificadorRetirada(saidaDTO.getCdRetirada());
		setDataRetirada(saidaDTO.getDtRetirada());
		setVlImpostoMovFinanceira(saidaDTO.getVlImpostoMovimentacaoFinanceira());
		setVlDesconto(saidaDTO.getVlDescontoCredito());
		setVlAbatimento(saidaDTO.getVlAbatidoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlIrCredito());
		setVlIss(saidaDTO.getVlIssCredito());
		setVlIof(saidaDTO.getVlIofCredito());
		setVlInss(saidaDTO.getVlInssCredito());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());	
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto titulo bradesco.
	 */
	public void preencheDadosPagtoTituloBradesco() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharPagtoTituloBradescoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoTituloBradesco(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoContaFavorecido());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setCarteira(saidaDTO.getCarteira());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCodigoBarras(saidaDTO.getCdBarras());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
		setNossoNumero(saidaDTO.getNossoNumero());
		setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
		setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
		setDsRastreado(saidaDTO.getCdRastreado() == 1 ? "Sim":"N�o");
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto titulo outros bancos.
	 */
	public void preencheDadosPagtoTituloOutrosBancos() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharPagtoTituloOutrosBancosEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoTituloOutrosBancos(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoContaFavorecido());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setCarteira(saidaDTO.getCarteira());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCodigoBarras(saidaDTO.getCdBarras());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
		setNossoNumero(saidaDTO.getNossoNumero());
		setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
		setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
		setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
		setDsRastreado(saidaDTO.getCdRastreado() == 1 ? "Sim":"N�o");
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto darf.
	 */
	public void preencheDadosPagtoDARF() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoDARFEntradaDTO entradaDTO = new DetalharPagtoDARFEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDARF(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddTelefone());
		setTelefoneContribuinte(saidaDTO.getCdTelefone());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
		setAgenteArrecadador(saidaDTO.getCdAgenteArrecad());
		setCodigoReceita(saidaDTO.getCdReceitaTributo());
		setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcela());
		setPercentualReceita(saidaDTO.getCdPercentualReceita());
		setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
		setAnoExercicio(saidaDTO.getCdDanoExercicio());
		setDataReferencia(saidaDTO.getDtReferenciaTributo());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
		setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlTotal(saidaDTO.getVlTotalTributo());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());	
		
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto gare.
	 */
	public void preencheDadosPagtoGARE() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoGAREEntradaDTO entradaDTO = new DetalharPagtoGAREEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoGARE(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddContribuinte());
		setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadualContribuinte());
		setCodigoReceita(saidaDTO.getCdReceita());
		setNumeroReferencia(saidaDTO.getNrReferencia());
		setNumeroCotaParcela(saidaDTO.getNrCota());
		setPercentualReceita(saidaDTO.getCdPercentualReceita());
		setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
		setDataReferencia(saidaDTO.getDtReferencia());
		setCodigoCnae(saidaDTO.getCdCnae());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setNumeroParcelamento(saidaDTO.getNrParcela());
		setCodigoOrgaoFavorecido(saidaDTO.getCdOrFavorecido());
		setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
		setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
		setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
		setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setObservacaoTributo(saidaDTO.getDsTributo());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto gps.
	 */
	public void preencheDadosPagtoGPS() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoGPSEntradaDTO entradaDTO = new DetalharPagtoGPSEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoGPS(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddContribuinte());
		setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
		setCodigoInss(saidaDTO.getCdInss());
		setDataCompetencia(saidaDTO.getDsMesAnoCompt());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto codigo barras.
	 */
	public void preencheDadosPagtoCodigoBarras() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharPagtoCodigoBarrasEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoCodigoBarras(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddTelefone());
		setTelefoneContribuinte(saidaDTO.getNrTelefone());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
		setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
		setCodigoReceita(saidaDTO.getCdReceitaTributo());
		setAgenteArrecadador(saidaDTO.getCdAgenteArrecadador());
		setCodigoTributo(saidaDTO.getCdTributoArrecadado());
		setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTributo());
		setPercentualReceita(saidaDTO.getCdPercentualTributo());
		setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
		setAnoExercicio(saidaDTO.getCdAnoExercicioTributo());
		setDataReferencia(saidaDTO.getDtReferenciaTributo());
		setDataCompetencia(saidaDTO.getDtCompensacao());
		setCodigoCnae(saidaDTO.getCdCnae());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setNumeroParcelamento(saidaDTO.getNrParcelamentoTributo());
		setCodigoOrgaoFavorecido(saidaDTO.getCdOrgaoFavorecido());
		setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
		setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
		setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
		setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setObservacaoTributo(saidaDTO.getDsObservacao());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto debito veiculos.
	 */
	public void preencheDadosPagtoDebitoVeiculos() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharPagtoDebitoVeiculosEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDebitoVeiculos(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTrib());
		setAnoExercicio(saidaDTO.getDtAnoExercTributo());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
		setMunicipioTributo(saidaDTO.getCdMunicArrecadTrib());
		setUfTributo(saidaDTO.getCdUfTributo());
		setNumeroNsu(saidaDTO.getNrNsuTributo());
		setOpcaoTributo(saidaDTO.getCdOpcaoTributo());
		setOpcaoRetiradaTributo(saidaDTO.getCdOpcaoRetirada());
		setVlPrincipal(saidaDTO.getVlPrincipalTributo());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlTotal(saidaDTO.getVlTotalTributo());
		setObservacaoTributo(saidaDTO.getDsObservacaoTributo());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto dep identificado.
	 */
	public void preencheDadosPagtoDepIdentificado() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharPagtoDepIdentificadoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDepIdentificado(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Beneficiario
		setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
						.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil
						.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setClienteDepositoIdentificado(saidaDTO.getCdClienteDepositoId());
		setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
		setProdutoDepositoIdentificado(saidaDTO.getCdProdutoDepositoId());
		setSequenciaProdutoDepositoIdentificado(saidaDTO.getCdSequenciaProdutoDepositoId());
		setBancoCartaoDepositoIdentificado(saidaDTO.getCdBancoCartaoDepositanteId());
		setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
		setBimCartaoDepositoIdentificado(saidaDTO.getCdBinCartaoDepositanteId());
		setViaCartaoDepositoIdentificado(saidaDTO.getCdViaCartaoDepositanteId());
		setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
		setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
		setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
		setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
		setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
		setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
		setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
		setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
		setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
		setVlIss(saidaDTO.getVlIssCreditoPagamento());
		setVlIof(saidaDTO.getVlIofCreditoPagamento());
		setVlInss(saidaDTO.getVlInssCreditoPagamento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());	
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto ordem credito.
	 */
	public void preencheDadosPagtoOrdemCredito() {
		ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
						getItemSelecionadoListaConsultar());
		OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
						getItemSelecionadoListaDetalhar());

		DetalharPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharPagtoOrdemCreditoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsultar.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsultar.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsultar.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoOrdemCredito(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Beneficiario
		setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
						.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil
						.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCodigoPagador(saidaDTO.getCdPagador());
		setCodigoOrdemCredito(saidaDTO.getCdOrdemCreditoPagamento());
		setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
		setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
		setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
		setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
		setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
		setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
		setVlIss(saidaDTO.getVlIssCreditoPagamento());
		setVlIof(saidaDTO.getVlIofCreditoPagamento());
		setVlInss(saidaDTO.getVlInssCreditoPagamento());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());	
		

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Detalhar filtro.
	 *
	 * @return the string
	 */
	public String detalharFiltro() {

		try {
			carregaListaDetalhar();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaGridDetalhar(null);
			setItemSelecionadoListaDetalhar(null);
			return "";
		}

		return "DETALHAR";
	}

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {
		setItemSelecionadoListaConsultar(null);

		return "VOLTAR";
	}

	/**
	 * Limpar campos favorecido beneficiario.
	 */
	private void limparCamposFavorecidoBeneficiario() {
		// Favorecido
		setDsFavorecido("");
		setCdFavorecido("");
		setDsBancoFavorecido("");
		setDsAgenciaFavorecido("");
		setDsContaFavorecido("");
		setDsTipoContaFavorecido("");
		setNumeroInscricaoFavorecido("");
		setTipoFavorecido(0);

		// Benefici�rio
		setNumeroInscricaoBeneficiario("");
		setTipoBeneficiario("");
		setNomeBeneficiario("");
		setDtNascimentoBeneficiario("");
		setCdTipoBeneficiario(0);
		setDsTipoBeneficiario("");
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		try {

			OcorrenciasDetalharAntPostergarPagtosSaidaDTO registroSelecionadoDetalhar = getListaGridDetalhar().get(
							getItemSelecionadoListaDetalhar());
			if (registroSelecionadoDetalhar.getCdTipoTela() != null) {
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 1) {
					preencheDadosPagtoCreditoConta();
					return "CREDITO_CONTA";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 2) {
					preencheDadosPagtoDebitoConta();
					return "DEBITO_CONTA";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 3) {
					preencheDadosPagtoDOC();
					return "DOC";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 4) {
					preencheDadosPagtoTED();
					return "TED";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 5) {
					preencheDadosPagtoOrdemPagto();
					return "ORDEM_PAGAMENTO";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 6) {
					preencheDadosPagtoTituloBradesco();
					return "TITULO_BRADESCO";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 7) {
					preencheDadosPagtoTituloOutrosBancos();
					return "TITULO_OUTROS_BANCOS";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 8) {
					preencheDadosPagtoDARF();
					return "DARF";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 9) {
					preencheDadosPagtoGARE();
					return "GARE";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 10) {
					preencheDadosPagtoGPS();
					return "GPS";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 11) {
					preencheDadosPagtoCodigoBarras();
					return "CODIGO_BARRAS";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 12) {
					preencheDadosPagtoDebitoVeiculos();
					return "DEBITO_VEICULOS";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 13) {
					preencheDadosPagtoDepIdentificado();
					return "DEP_IDENTIFICADO";
				}
				if (registroSelecionadoDetalhar.getCdTipoTela().intValue() == 14) {
					preencheDadosPagtoOrdemCredito();
					return "ORDEM_CREDITO";
				}
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}

		return "";
	}

	/**
	 * Alterar parcial.
	 *
	 * @return the string
	 */
	public String alterarParcial() {
		try {
			setOpcaoChecarTodos(false);
			carregaListaDetalhar();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaGridDetalhar(null);
			setItemSelecionadoListaDetalhar(null);
			return "";
		}

		return "ALTERAR_PARCIAL";
	}

	/**
	 * Limpar selecionados.
	 */
	public void limparSelecionados() {
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
		for (OcorrenciasDetalharAntPostergarPagtosSaidaDTO o : getListaGridDetalhar()) {
			o.setCheck(false);
		}
	}

	/**
	 * Avancar.
	 *
	 * @return the string
	 */
	public String avancar() {
		setDataAgendamento(FormatarData.formataDiaMesAno(getNovaDataAgendamento()));

		return "AVANCAR";
	}

	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes() {
		setPanelBotoes(false);
		for (OcorrenciasDetalharAntPostergarPagtosSaidaDTO o : getListaGridDetalhar()) {
			if (o.isCheck()) {
				setPanelBotoes(true);
				break;
			}
		}
	}

	/**
	 * Confirmar alterar selecionados.
	 */
	public void confirmarAlterarSelecionados() {
		try {
			AntPostergarPagtosParcialEntradaDTO entradaDTO = new AntPostergarPagtosParcialEntradaDTO();
			ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionado = getListaGridConsultar().get(
							getItemSelecionadoListaConsultar());
			List<OcorrenciasAntPostergarPagtosParcialEntradaDTO> listaEntrada = new ArrayList<OcorrenciasAntPostergarPagtosParcialEntradaDTO>();

			for (OcorrenciasDetalharAntPostergarPagtosSaidaDTO o : getListaGridAlterarSelecionados()) {
				OcorrenciasAntPostergarPagtosParcialEntradaDTO ocorrenciasDTO = new OcorrenciasAntPostergarPagtosParcialEntradaDTO();
				ocorrenciasDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridica());
				ocorrenciasDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContrato());
				ocorrenciasDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
				ocorrenciasDTO.setCdProdutoServicoOperacao(registroSelecionado.getCdProdutoServicoOperacao());
				ocorrenciasDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
				ocorrenciasDTO.setCdTipoCanal(o.getCdTipoCanal());
				ocorrenciasDTO.setCdControlePagamento(o.getNrPagamento());
				ocorrenciasDTO.setCdIndicadorAntPosProPagamento(Integer.valueOf(getTipoAgendamento()));
				listaEntrada.add(ocorrenciasDTO);
			}

			entradaDTO.setListaOcorrencias(listaEntrada);
			entradaDTO.setDtQuitacao(FormatarData.formataDiaMesAno(getNovaDataAgendamento()));
			AntPostergarPagtosParcialSaidaDTO saidaDTO = getAnteciparPostergarPagtosAgeConImpl()
							.antPostergarPagtosParcial(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
							"conAnteciparPostergarPagtosAgeCon", "#{anteciparPostergarPagtosAgeConBean.consultar}",
							false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
	}

	/**
	 * Alterar integral.
	 *
	 * @return the string
	 */
	public String alterarIntegral() {
		try {
			carregaListaDetalhar();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaGridDetalhar(null);
			setItemSelecionadoListaDetalhar(null);
			return "";
		}

		if ("1".equals(getTipoAgendamento())) {
			setDataAgendamento(FormatarData.formataDiaMesAno(getNovaDataAgendamento()));

			return "altAnteciparPostergarConPagtosIntegralConf";
		} else {
			return "ALTERAR_INTEGRAL";
		}
	}

	/**
	 * Alterar selecionados.
	 *
	 * @return the string
	 */
	public String alterarSelecionados() {
		setDataAgendamento(FormatarData.formataDiaMesAno(getNovaDataAgendamento()));
		setListaGridAlterarSelecionados(new ArrayList<OcorrenciasDetalharAntPostergarPagtosSaidaDTO>());
		setVlrPagamentosAutorizados(BigDecimal.ZERO);
		for (OcorrenciasDetalharAntPostergarPagtosSaidaDTO o : getListaGridDetalhar()) {
			if (o.isCheck()) {
				getListaGridAlterarSelecionados().add(o);
				setVlrPagamentosAutorizados(getVlrPagamentosAutorizados().add(o.getVlPagamento()));
			}
		}

		setQtdPagamentoAutorizados(getListaGridAlterarSelecionados().size());

		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(new PDCDataBean());

		return "AVANCAR";
	}

	/**
	 * Confirma alterar integral.
	 */
	public void confirmaAlterarIntegral() {
		try {
			ConsultarPagtosConsAntPostergacaoSaidaDTO registroSelecionadoConsultar = getListaGridConsultar().get(
							getItemSelecionadoListaConsultar());
			AntPostergarPagtosIntegralEntradaDTO entradaDTO = new AntPostergarPagtosIntegralEntradaDTO();

			entradaDTO.setDtQuitacao(FormatarData.formataDiaMesAnoToPdc(getNovaDataAgendamento()));
			entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoConsultar.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(registroSelecionadoConsultar.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(registroSelecionadoConsultar.getNrContrato());
			entradaDTO.setCdCorpoCpfCnpj(registroSelecionadoConsultar.getNrCnpjCpf());
			entradaDTO.setCdCnpjFilial(registroSelecionadoConsultar.getNrFilialCnpjCpf());
			entradaDTO.setCdDigitoCpfCnpj(String.valueOf(registroSelecionadoConsultar.getCdDigitoCnpjCpf()));
			try {
				entradaDTO.setDtPagamento(SiteUtil.changeStringDateFormat(
								registroSelecionadoConsultar.getDtPagamento(), "dd/MM/yyyy", "dd.MM.yyyy"));
			} catch (ParseException e) {
				entradaDTO.setDtPagamento("");
			}
			entradaDTO.setNrArquivoRemessaPagamento(registroSelecionadoConsultar.getNrRemessa());
			entradaDTO.setCdBanco(registroSelecionadoConsultar.getCdBanco());
			entradaDTO.setCdAgenciaBancaria(registroSelecionadoConsultar.getCdAgencia());
			entradaDTO.setCdContaBancaria(registroSelecionadoConsultar.getCdConta());
			entradaDTO.setCdDigitoContaBancaria(SiteUtil.formatNumber(registroSelecionadoConsultar.getCdDigitoConta(),
							2));
			entradaDTO.setCdProdutoServicoOperacao(registroSelecionadoConsultar.getCdProdutoServicoOperacao());
			entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsultar.getCdProdutoServicoRelacionado());
			entradaDTO.setCdSituacaoOperacaoPagamento(registroSelecionadoConsultar.getCdSituacaoOperacaoPagamento());
			entradaDTO.setCdMotivoSituacaoPagamento(0);
			entradaDTO.setCdPessoaContratoDebito(registroSelecionadoConsultar.getCdPessoaContratoDebito());
			entradaDTO.setCdTipoContratoDebito(registroSelecionadoConsultar.getCdTipoContratoDebito());
			entradaDTO.setNrSequenciaContratoDebito(registroSelecionadoConsultar.getNrSequenciaContratoDebito());
			entradaDTO.setCdIndicadorAntPosProPagamento(Integer.valueOf(getTipoAgendamento()));

			AntPostergarPagtosIntegralSaidaDTO saida = getAnteciparPostergarPagtosAgeConImpl()
							.antPostergarPagtosIntegral(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
							"conAnteciparPostergarPagtosAgeCon", "#{anteciparPostergarPagtosAgeConBean.consultar}",
							false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
	}

	/**
	 * Imprimir parcial.
	 */
	public void imprimirParcial() {
		try {
			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par.put("titulo", "Antecipar-Postergar Pagamentos Agendados Consolidados");

			par.put("cpfCnpj", getNrCnpjCpf());
			par.put("nomeRazaoSocial", getDsRazaoSocial());
			par.put("empresaConglomerado", getDsEmpresa());
			par.put("nrContrato", getNroContrato());
			par.put("dsContratacao", getDsContrato());
			par.put("situacao", getCdSituacaoContrato());
			par.put("banco", getDsBancoDebito());
			par.put("agencia", getDsAgenciaDebito());
			par.put("conta", getContaDebito());
			par.put("tipo", getTipoContaDebito());
			par.put("logoBradesco", getLogoPath());

			par.put("dsTipoServico", getDsTipoServico());
			par.put("dsIndicadorModalidade", getDsIndicadorModalidade());
			par.put("qtdePagamentos", getQtdePagamentos());
			par.put("vlTotalPagamentos", getVlTotalPagamentos());
			par.put("qtdePagamentosAutorizados", getQtdPagamentoAutorizados());
			par.put("vlTotalPagamentosAutorizados", getVlrPagamentosAutorizados());
			par.put("dataAgendamento", getDataAgendamento());

			if (getTipoAgendamento() != null) {
				if (getTipoAgendamento().equals("1")) {
					par.put("tipoAgendamento", MessageHelperUtils.getI18nMessage("label_antecipar_processamento"));
				} else {
					if (getTipoAgendamento().equals("2")) {
						par.put("tipoAgendamento", MessageHelperUtils
										.getI18nMessage("label_antecipar_postergar_pagamento"));
					}
				}
			}
			try {

				// M�todo que gera o relat�rio PDF.
				PgitUtil
								.geraRelatorioPdf(
												"/relatorios/agendamentoEfetivacaoEstorno/anteciparPagamentosAgendados/postegarPagamentosAgendadosConsolidados/parcial",
												"imprimirAntPostPagtosAgeConsParcial",
												getListaGridAlterarSelecionados(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
	}

	/**
	 * Imprimir integral.
	 */
	public void imprimirIntegral() {
		try {
			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par.put("titulo", "Antecipar-Postergar Pagamentos Agendados Consolidados");

			par.put("cpfCnpj", getNrCnpjCpf());
			par.put("nomeRazaoSocial", getDsRazaoSocial());
			par.put("empresaConglomerado", getDsEmpresa());
			par.put("nrContrato", getNroContrato());
			par.put("dsContratacao", getDsContrato());
			par.put("situacao", getCdSituacaoContrato());
			par.put("banco", getDsBancoDebito());
			par.put("agencia", getDsAgenciaDebito());
			par.put("conta", getContaDebito());
			par.put("tipo", getTipoContaDebito());
			par.put("logoBradesco", getLogoPath());

			par.put("dsTipoServico", getDsTipoServico());
			par.put("dsIndicadorModalidade", getDsIndicadorModalidade());
			par.put("qtdePagamentos", getQtdePagamentos());
			par.put("vlTotalPagamentos", getVlTotalPagamentos());
			par.put("qtdePagamentosAutorizados", getQtdPagamentoAutorizados());
			par.put("vlTotalPagamentosAutorizados", getVlrPagamentosAutorizados());
			par.put("dataAgendamento", getDataAgendamento());

			if (getTipoAgendamento() != null) {
				if (getTipoAgendamento().equals("1")) {
					par.put("tipoAgendamento", MessageHelperUtils.getI18nMessage("label_antecipar_processamento"));
				} else {
					if (getTipoAgendamento().equals("2")) {
						par.put("tipoAgendamento", MessageHelperUtils
										.getI18nMessage("label_antecipar_postergar_pagamento"));
					}
				}
			}
			try {

				// M�todo que gera o relat�rio PDF.
				PgitUtil
								.geraRelatorioPdf(
												"/relatorios/agendamentoEfetivacaoEstorno/anteciparPagamentosAgendados/postegarPagamentosAgendadosConsolidados/integral",
												"imprimirAntPostPagtosAgeConsIntegral", getListaGridDetalhar(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoListaConsultar(null);

		return "VOLTAR";
	}

	/**
	 * Voltar tela consulta.
	 *
	 * @return the string
	 */
	public String voltarTelaConsulta() {
		setItemSelecionadoListaConsultar(null);

		return "conAnteciparPostergarPagtosAgeCon";
	}

	/**
	 * Voltar integral.
	 *
	 * @return the string
	 */
	public String voltarIntegral() {
		return "VOLTAR";
	}

	/**
	 * Voltar detalhe.
	 *
	 * @return the string
	 */
	public String voltarDetalhe() {
		setItemSelecionadoListaDetalhar(null);

		return "VOLTAR";
	}

	/**
	 * Voltar parcial.
	 *
	 * @return the string
	 */
	public String voltarParcial() {
		limparSelecionados();

		return "VOLTAR";
	}

	/**
	 * Limpar informacao consulta.
	 */
	public void limparInformacaoConsulta() {
		setListaGridConsultar(new ArrayList<ConsultarPagtosConsAntPostergacaoSaidaDTO>());
		setItemSelecionadoListaConsultar(null);
		setListaControleRadio(new ArrayList<SelectItem>());
		setDisableArgumentosConsulta(false);
	}

	/**
	 * Limpar tela principal.
	 *
	 * @return the string
	 */
	public String limparTelaPrincipal() {
		// Limpar Argumentos de Pesquisa e Lista
		limparFiltroPrincipal();
		limparInformacaoConsulta();

		return "";
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
		limparInformacaoConsulta();

		limparCampos();

		return "";

	}

	/**
	 * Limpar tudo.
	 */
	public void limparTudo() {
		limparFiltroPrincipal();
		limparInformacaoConsulta();
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(null);
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		limparCampos();
		limparInformacaoConsulta();
	}

	/**
	 * Pesquisar det antecipar postergar pagtos age con filtro.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarDetAnteciparPostergarPagtosAgeConFiltro(ActionEvent evt) {
		try {
			carregaListaDetalhar();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaGridDetalhar(null);
			setItemSelecionadoListaDetalhar(null);
			return "";
		}
		return "";
	}

	/**
	 * Pesquisar consultar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarConsultar(ActionEvent evt) {
		consultar();
		return "";
	}

	/**
	 * Consultar solicitacoes.
	 *
	 * @return the string
	 */
    public String consultarSolicitacoes() {

        limparCamposSolicitacoes();

        setCdpessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getCdPessoaJuridicaContrato());
        setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean().getCdTipoContratoNegocio());
        setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean().getNrSequenciaContratoNegocio());

        setNrCnpjCpf(getFiltroAgendamentoEfetivacaoEstornoBean().getSaidaConsultarListaClientePessoas()
            .getCnpjOuCpfFormatado());
        setDsRazaoSocial(getFiltroAgendamentoEfetivacaoEstornoBean().getSaidaConsultarListaClientePessoas()
            .getDsNomeRazao());
        setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescCliente());
        setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescCliente());
        setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescCliente());
        setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescCliente());

        return "CONSULTAR_SOLICITACOES";
    }
	
	/**
	 * Carregar combo solicitacao estorno.
	 */
	private void carregarComboSolicitacaoEstorno(){
	    try{
    	    ConsultarSituacaoSolicitacaoEstornoEntradaDTO entradaSituacao = new ConsultarSituacaoSolicitacaoEstornoEntradaDTO();
    	    entradaSituacao.setNumeroOcorrencias(50);
    	    entradaSituacao.setCdSituacao(0);
    	    entradaSituacao.setCdIndicador(2);
    	    
    	    setListaConsultarSituacaoSolicitacaoEstorno(new ArrayList<SelectItem>());
    	    
    	    List<ConsultarSituacaoSolicitacaoEstornoSaidaDTO> lista = 
    	        getComboService().consultarSituacaoSolicitacaoEstorno(entradaSituacao);
    
            for (ConsultarSituacaoSolicitacaoEstornoSaidaDTO saida : lista) {
                listaConsultarSituacaoSolicitacaoEstorno.add(new SelectItem(saida.getCodigo(), saida.getDescricao()));
            }
    	} catch (PdcAdapterFunctionalException p) {
    	    listaConsultarSituacaoSolicitacaoEstorno = new ArrayList<SelectItem>();
        }
	}
	
	/**
	 * Carregar combo motivo estorno.
	 */
	public void carregarComboMotivoEstorno(){
	    try{
    	    ConsultarMotivoSituacaoEstornoEntradaDTO entradaMotivo = new ConsultarMotivoSituacaoEstornoEntradaDTO();
            entradaMotivo.setCdSituacao(getItemSelecionadoComboSolicitacao());
            
            setListaConsultarMotivoSituacaoEstorno(new ArrayList<SelectItem>());
            
            List<ConsultarMotivoSituacaoEstornoSaidaDTO> lista = 
                getComboService().consultarMotivoSituacaoEstornoSaida(entradaMotivo);
            
            for (ConsultarMotivoSituacaoEstornoSaidaDTO saida : lista) {
                listaConsultarMotivoSituacaoEstorno.add(new SelectItem(saida.getCodigo(), saida.getDescricao()));
            }
            
            setDesabilitaComboMotivo(false);
	    } catch (PdcAdapterFunctionalException p) {
	        listaConsultarMotivoSituacaoEstorno = new ArrayList<SelectItem>();
	        setDesabilitaComboMotivo(true);
        }
	}
	
	/**
	 * Voltar solicitacoes.
	 *
	 * @return the string
	 */
	public String voltarSolicitacoes(){
	    setItemSelecionadoConsultaPostergarConsolidado(null);
	    setDesabilitaBotaoExcluir(true);
	    return "VOLTAR";
	}
	
	/**
	 * Limpar solicitacoes.
	 */
	public void limparSolicitacoes(){
	    setDesabilitaBotaoExcluir(true);
	    setListaConsultaPostergarConsolidadoSolicitacaoControle(new ArrayList<SelectItem>());
	    setListaConsultaPostergarConsolidadoSolicitacao(new ArrayList<ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO>());
	    setItemSelecionadoConsultaPostergarConsolidado(null);
	    setDesabilitaArgumentoDePesquisa(false);
	    if(getItemSelecionadoComboSolicitacao() != null){
	        setDesabilitaComboMotivo(false);
	    }
    }
	
	/**
	 * Limpar campos solicitacoes.
	 */
	public void limparCamposSolicitacoes(){
	    setListaConsultarMotivoSituacaoEstorno(new ArrayList<SelectItem>());
        setListaConsultarSituacaoSolicitacaoEstorno(new ArrayList<SelectItem>());
        setListaConsultaPostergarConsolidadoSolicitacaoControle(new ArrayList<SelectItem>());
        setListaConsultaPostergarConsolidadoSolicitacao(new ArrayList<ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO>());      
        
        setItemSelecionadoComboMotivo(null);
        setItemSelecionadoComboSolicitacao(null);
        setItemSelecionadoConsultaPostergarConsolidado(null);
        
        setDataFinalSolicitacaoFiltro(new Date());
        setDataInicialSolicitacaoFiltro(new Date());

        setDesabilitaComboMotivo(true);
        setDesabilitaBotaoExcluir(true);
        setDesabilitaArgumentoDePesquisa(false);
        
        carregarComboSolicitacaoEstorno();
	}
	
	/**
	 * Consulta postergar consolidado solicitacao.
	 */
	public void consultaPostergarConsolidadoSolicitacao(){
	    try {
    	    setListaConsultaPostergarConsolidadoSolicitacao(new ArrayList<ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO>());
    	    
    	    ConsultaPostergarConsolidadoSolicitacaoEntradaDTO entrada = new ConsultaPostergarConsolidadoSolicitacaoEntradaDTO();
    
            entrada.setCdpessoaJuridicaContrato(getCdpessoaJuridicaContrato());
            entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
            entrada.setNrSequenciaContratoNegocio(getNrSequenciaContratoNegocio());
            entrada.setDtPgtoInicial(formataDiaMesAnoToPdc(getDataInicialSolicitacaoFiltro()));
            entrada.setDtPgtoFinal(formataDiaMesAnoToPdc(getDataFinalSolicitacaoFiltro()));
            entrada.setCdSituacaoSolicitacaoPagamento(getItemSelecionadoComboSolicitacao());
            entrada.setCdMotivoSolicitacao(getItemSelecionadoComboMotivo());
            
        
            ConsultaPostergarConsolidadoSolicitacaoSaidaDTO saida =
                getAnteciparPostergarPagtosAgeConImpl().consultaPostergarConsolidadoSolicitacao(entrada);
            
            setListaConsultaPostergarConsolidadoSolicitacao(saida.getOcorrencias());
            
            setListaConsultaPostergarConsolidadoSolicitacaoControle(new ArrayList<SelectItem>(saida.getNrLinhas()));
            setItemSelecionadoConsultaPostergarConsolidado(null);
            
            for (int i = 0; i < saida.getNrLinhas(); i++) {
                listaConsultaPostergarConsolidadoSolicitacaoControle.add(new SelectItem(i, ""));
            }
            
            setDesabilitaArgumentoDePesquisa(true);
            setDesabilitaComboMotivo(true);
            
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
            setListaConsultaPostergarConsolidadoSolicitacao(new ArrayList<ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO>());
            setDesabilitaArgumentoDePesquisa(false);
        }	    
    }

    /**
     * Consulta postergar consolidado solicitacao listener.
     *
     * @param evt the evt
     * @return the string
     */
    public String consultaPostergarConsolidadoSolicitacaoListener(ActionEvent evt) {
        consultaPostergarConsolidadoSolicitacao();
        return "";
    }

    /**
     * Habilitar botao excluir.
     */
    public void habilitarBotaoExcluir() {
        setDesabilitaBotaoExcluir(true);

        ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO item =
            getListaConsultaPostergarConsolidadoSolicitacao().get(getItemSelecionadoConsultaPostergarConsolidado());

        if (item.getDsSituacaoSolicitacaoPagamento().equalsIgnoreCase("PENDENTE")) {
            setDesabilitaBotaoExcluir(false);
        }
    }

    /**
     * Detalhar solicitacoes.
     *
     * @return the string
     */
    public String detalharSolicitacoes() {
        detalhePostergarConsolidadoSolicitacao(); 
        listarPostergarConsolidadoSolicitacao();
        
        return "DETALHAR_SOLICITACOES";
    }

    /**
     * Excluir solicitacoes.
     *
     * @return the string
     */
    public String excluirSolicitacoes() {
        detalhePostergarConsolidadoSolicitacao(); 
        
        return "EXCLUIR_SOLICITACOES";
    }
    
    /**
     * Detalhe postergar consolidado solicitacao.
     */
    private void detalhePostergarConsolidadoSolicitacao(){
        try{
            setSaidaDetalhePostergarConsolidadoSolicitacao(new DetalhePostergarConsolidadoSolicitacaoSaidaDTO());
            DetalhePostergarConsolidadoSolicitacaoEntradaDTO entrada = new DetalhePostergarConsolidadoSolicitacaoEntradaDTO();
            
            ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO item =
                getListaConsultaPostergarConsolidadoSolicitacao().get(getItemSelecionadoConsultaPostergarConsolidado());
            
            entrada.setCdSolicitacaoPagamentoIntegrado(item.getCdSolicitacaoPagamentoIntegrado());
            entrada.setNrSolicitacaoPagamentoIntegrado(item.getNrSolicitacaoPagamentoIntegrado());
            
             
            saidaDetalhePostergarConsolidadoSolicitacao = 
                getAnteciparPostergarPagtosAgeConImpl().detalhePostergarConsolidadoSolicitacao(entrada);
        
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
        }
    }
    
    /**
     * Listar postergar consolidado solicitacao.
     */
    private void listarPostergarConsolidadoSolicitacao(){
        try {
            setListaPostergarConsolidadoSolicitacao(new ArrayList<ListarPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO>());
            
            ListarPostergarConsolidadoSolicitacaoEntradaDTO entrada = new ListarPostergarConsolidadoSolicitacaoEntradaDTO();
    
            entrada.setCdpessoaJuridicaContrato(getCdpessoaJuridicaContrato());
            entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
            entrada.setNrSequenciaContratoNegocio(getNrSequenciaContratoNegocio());
            
            ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO item =
                getListaConsultaPostergarConsolidadoSolicitacao().get(getItemSelecionadoConsultaPostergarConsolidado());
            
            entrada.setCdSolicitacaoPagamentoIntegrado(item.getCdSolicitacaoPagamentoIntegrado());
            entrada.setNrSolicitacaoPagamentoIntegrado(item.getNrSolicitacaoPagamentoIntegrado());
            
        
            ListarPostergarConsolidadoSolicitacaoSaidaDTO saida =
                getAnteciparPostergarPagtosAgeConImpl().listarPostergarConsolidadoSolicitacao(entrada);
            
            setListaPostergarConsolidadoSolicitacao(saida.getOcorrencias());
            
        } catch (PdcAdapterFunctionalException p) {
           
        }       
        
    }
    
    /**
     * Lista postergar consolidado solicitacao listener.
     */
    public void listaPostergarConsolidadoSolicitacaoListener(ActionEvent evt){
        listarPostergarConsolidadoSolicitacao();
    }
    
    /**
     * Confirmar exclusao solicitacao.
     */
    public void confirmarExclusaoSolicitacao(){
        try{
            ExcluirPostergarConsolidadoSolicitacaoEntradaDTO entrada = new ExcluirPostergarConsolidadoSolicitacaoEntradaDTO();
            
            ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO item =
                getListaConsultaPostergarConsolidadoSolicitacao().get(getItemSelecionadoConsultaPostergarConsolidado());
            
            entrada.setCdSolicitacaoPagamentoIntegrado(item.getCdSolicitacaoPagamentoIntegrado());
            entrada.setNrSolicitacaoPagamentoIntegrado(item.getNrSolicitacaoPagamentoIntegrado());
            
            ExcluirPostergarConsolidadoSolicitacaoSaidaDTO saida = 
                getAnteciparPostergarPagtosAgeConImpl().excluirPostergarConsolidadoSolicitacao(entrada);
        
                    
              
            BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
                voltarSolicitacoes(),"#{anteciparPostergarPagtosAgeConBean.consultaPostergarConsolidadoSolicitacaoListener}", false);
                       
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                false);
        }
    }
    
    public boolean isDesabilitaBotaoConsultarSolicitacoes(){
       if((!isHabilitaArgumentosPesquisa() && !getFiltroAgendamentoEfetivacaoEstornoBean().isClienteContratoSelecionado()) || 
        isDisableArgumentosConsulta()){
           if(!getListaGridConsultar().isEmpty()){
               return false;
           }
           return true;
       }
   
       return false;
    }
	
	// Set's e Get's
	/**
	 * Get: itemSelecionadoListaConsultar.
	 *
	 * @return itemSelecionadoListaConsultar
	 */
	public Integer getItemSelecionadoListaConsultar() {
		return itemSelecionadoListaConsultar;
	}

	/**
	 * Set: itemSelecionadoListaConsultar.
	 *
	 * @param itemSelecionadoListaConsultar the item selecionado lista consultar
	 */
	public void setItemSelecionadoListaConsultar(Integer itemSelecionadoListaConsultar) {
		this.itemSelecionadoListaConsultar = itemSelecionadoListaConsultar;
	}

	/**
	 * Get: listaGridConsultar.
	 *
	 * @return listaGridConsultar
	 */
	public List<ConsultarPagtosConsAntPostergacaoSaidaDTO> getListaGridConsultar() {
		return listaGridConsultar;
	}

	/**
	 * Set: listaGridConsultar.
	 *
	 * @param listaGridConsultar the lista grid consultar
	 */
	public void setListaGridConsultar(List<ConsultarPagtosConsAntPostergacaoSaidaDTO> listaGridConsultar) {
		this.listaGridConsultar = listaGridConsultar;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: anteciparPostergarPagtosAgeConImpl.
	 *
	 * @return anteciparPostergarPagtosAgeConImpl
	 */
	public IAnteciparPostergarPagtosAgeConService getAnteciparPostergarPagtosAgeConImpl() {
		return anteciparPostergarPagtosAgeConImpl;
	}

	/**
	 * Set: anteciparPostergarPagtosAgeConImpl.
	 *
	 * @param anteciparPostergarPagtosAgeConImpl the antecipar postergar pagtos age con impl
	 */
	public void setAnteciparPostergarPagtosAgeConImpl(
					IAnteciparPostergarPagtosAgeConService anteciparPostergarPagtosAgeConImpl) {
		this.anteciparPostergarPagtosAgeConImpl = anteciparPostergarPagtosAgeConImpl;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: itemSelecionadoListaDetalhar.
	 *
	 * @return itemSelecionadoListaDetalhar
	 */
	public Integer getItemSelecionadoListaDetalhar() {
		return itemSelecionadoListaDetalhar;
	}

	/**
	 * Set: itemSelecionadoListaDetalhar.
	 *
	 * @param itemSelecionadoListaDetalhar the item selecionado lista detalhar
	 */
	public void setItemSelecionadoListaDetalhar(Integer itemSelecionadoListaDetalhar) {
		this.itemSelecionadoListaDetalhar = itemSelecionadoListaDetalhar;
	}

	/**
	 * Get: qtdePagamentos.
	 *
	 * @return qtdePagamentos
	 */
	public Long getQtdePagamentos() {
		return qtdePagamentos;
	}

	/**
	 * Set: qtdePagamentos.
	 *
	 * @param qtdePagamentos the qtde pagamentos
	 */
	public void setQtdePagamentos(Long qtdePagamentos) {
		this.qtdePagamentos = qtdePagamentos;
	}

	/**
	 * Get: vlTotalPagamentos.
	 *
	 * @return vlTotalPagamentos
	 */
	public BigDecimal getVlTotalPagamentos() {
		return vlTotalPagamentos;
	}

	/**
	 * Set: vlTotalPagamentos.
	 *
	 * @param vlTotalPagamentos the vl total pagamentos
	 */
	public void setVlTotalPagamentos(BigDecimal vlTotalPagamentos) {
		this.vlTotalPagamentos = vlTotalPagamentos;
	}

	/**
	 * Get: listaControleRadioDetalhe.
	 *
	 * @return listaControleRadioDetalhe
	 */
	public List<SelectItem> getListaControleRadioDetalhe() {
		return listaControleRadioDetalhe;
	}

	/**
	 * Set: listaControleRadioDetalhe.
	 *
	 * @param listaControleRadioDetalhe the lista controle radio detalhe
	 */
	public void setListaControleRadioDetalhe(List<SelectItem> listaControleRadioDetalhe) {
		this.listaControleRadioDetalhe = listaControleRadioDetalhe;
	}

	/**
	 * Get: listaGridDetalhar.
	 *
	 * @return listaGridDetalhar
	 */
	public List<OcorrenciasDetalharAntPostergarPagtosSaidaDTO> getListaGridDetalhar() {
		return listaGridDetalhar;
	}

	/**
	 * Set: listaGridDetalhar.
	 *
	 * @param listaGridDetalhar the lista grid detalhar
	 */
	public void setListaGridDetalhar(List<OcorrenciasDetalharAntPostergarPagtosSaidaDTO> listaGridDetalhar) {
		this.listaGridDetalhar = listaGridDetalhar;
	}

	/**
	 * Get: agenciaDigitoCredito.
	 *
	 * @return agenciaDigitoCredito
	 */
	public String getAgenciaDigitoCredito() {
		return agenciaDigitoCredito;
	}

	/**
	 * Set: agenciaDigitoCredito.
	 *
	 * @param agenciaDigitoCredito the agencia digito credito
	 */
	public void setAgenciaDigitoCredito(String agenciaDigitoCredito) {
		this.agenciaDigitoCredito = agenciaDigitoCredito;
	}

	/**
	 * Get: agenciaDigitoDestino.
	 *
	 * @return agenciaDigitoDestino
	 */
	public String getAgenciaDigitoDestino() {
		return agenciaDigitoDestino;
	}

	/**
	 * Set: agenciaDigitoDestino.
	 *
	 * @param agenciaDigitoDestino the agencia digito destino
	 */
	public void setAgenciaDigitoDestino(String agenciaDigitoDestino) {
		this.agenciaDigitoDestino = agenciaDigitoDestino;
	}

	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public String getCdBancoCredito() {
		return cdBancoCredito;
	}

	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(String cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}

	/**
	 * Get: cdBancoDestino.
	 *
	 * @return cdBancoDestino
	 */
	public String getCdBancoDestino() {
		return cdBancoDestino;
	}

	/**
	 * Set: cdBancoDestino.
	 *
	 * @param cdBancoDestino the cd banco destino
	 */
	public void setCdBancoDestino(String cdBancoDestino) {
		this.cdBancoDestino = cdBancoDestino;
	}

	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public String getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}

	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(String cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}

	/**
	 * Get: cdListaDebito.
	 *
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}

	/**
	 * Set: cdListaDebito.
	 *
	 * @param cdListaDebito the cd lista debito
	 */
	public void setCdListaDebito(String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}

	/**
	 * Get: cdSerieDocumento.
	 *
	 * @return cdSerieDocumento
	 */
	public String getCdSerieDocumento() {
		return cdSerieDocumento;
	}

	/**
	 * Set: cdSerieDocumento.
	 *
	 * @param cdSerieDocumento the cd serie documento
	 */
	public void setCdSerieDocumento(String cdSerieDocumento) {
		this.cdSerieDocumento = cdSerieDocumento;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public String getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(String cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: cdTipoBeneficiario.
	 *
	 * @return cdTipoBeneficiario
	 */
	public Integer getCdTipoBeneficiario() {
		return cdTipoBeneficiario;
	}

	/**
	 * Set: cdTipoBeneficiario.
	 *
	 * @param cdTipoBeneficiario the cd tipo beneficiario
	 */
	public void setCdTipoBeneficiario(Integer cdTipoBeneficiario) {
		this.cdTipoBeneficiario = cdTipoBeneficiario;
	}

	/**
	 * Get: contaDigitoCredito.
	 *
	 * @return contaDigitoCredito
	 */
	public String getContaDigitoCredito() {
		return contaDigitoCredito;
	}

	/**
	 * Set: contaDigitoCredito.
	 *
	 * @param contaDigitoCredito the conta digito credito
	 */
	public void setContaDigitoCredito(String contaDigitoCredito) {
		this.contaDigitoCredito = contaDigitoCredito;
	}

	/**
	 * Get: contaDigitoDestino.
	 *
	 * @return contaDigitoDestino
	 */
	public String getContaDigitoDestino() {
		return contaDigitoDestino;
	}

	/**
	 * Set: contaDigitoDestino.
	 *
	 * @param contaDigitoDestino the conta digito destino
	 */
	public void setContaDigitoDestino(String contaDigitoDestino) {
		this.contaDigitoDestino = contaDigitoDestino;
	}

	/**
	 * Get: dsAgenciaFavorecido.
	 *
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}

	/**
	 * Set: dsAgenciaFavorecido.
	 *
	 * @param dsAgenciaFavorecido the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}

	/**
	 * Get: dsBancoFavorecido.
	 *
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}

	/**
	 * Set: dsBancoFavorecido.
	 *
	 * @param dsBancoFavorecido the ds banco favorecido
	 */
	public void setDsBancoFavorecido(String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}

	/**
	 * Get: dsContaFavorecido.
	 *
	 * @return dsContaFavorecido
	 */
	public String getDsContaFavorecido() {
		return dsContaFavorecido;
	}

	/**
	 * Set: dsContaFavorecido.
	 *
	 * @param dsContaFavorecido the ds conta favorecido
	 */
	public void setDsContaFavorecido(String dsContaFavorecido) {
		this.dsContaFavorecido = dsContaFavorecido;
	}

	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}

	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}

	/**
	 * Get: dsIndicadorModalidade.
	 *
	 * @return dsIndicadorModalidade
	 */
	public String getDsIndicadorModalidade() {
		return dsIndicadorModalidade;
	}

	/**
	 * Set: dsIndicadorModalidade.
	 *
	 * @param dsIndicadorModalidade the ds indicador modalidade
	 */
	public void setDsIndicadorModalidade(String dsIndicadorModalidade) {
		this.dsIndicadorModalidade = dsIndicadorModalidade;
	}

	/**
	 * Get: dsMensagemPrimeiraLinha.
	 *
	 * @return dsMensagemPrimeiraLinha
	 */
	public String getDsMensagemPrimeiraLinha() {
		return dsMensagemPrimeiraLinha;
	}

	/**
	 * Set: dsMensagemPrimeiraLinha.
	 *
	 * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
	 */
	public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
		this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
	}

	/**
	 * Get: dsMensagemSegundaLinha.
	 *
	 * @return dsMensagemSegundaLinha
	 */
	public String getDsMensagemSegundaLinha() {
		return dsMensagemSegundaLinha;
	}

	/**
	 * Set: dsMensagemSegundaLinha.
	 *
	 * @param dsMensagemSegundaLinha the ds mensagem segunda linha
	 */
	public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
		this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
	}

	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	/**
	 * Get: dsPagamento.
	 *
	 * @return dsPagamento
	 */
	public String getDsPagamento() {
		return dsPagamento;
	}

	/**
	 * Set: dsPagamento.
	 *
	 * @param dsPagamento the ds pagamento
	 */
	public void setDsPagamento(String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}

	/**
	 * Get: dsTipoContaFavorecido.
	 *
	 * @return dsTipoContaFavorecido
	 */
	public String getDsTipoContaFavorecido() {
		return dsTipoContaFavorecido;
	}

	/**
	 * Set: dsTipoContaFavorecido.
	 *
	 * @param dsTipoContaFavorecido the ds tipo conta favorecido
	 */
	public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
		this.dsTipoContaFavorecido = dsTipoContaFavorecido;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: dsUsoEmpresa.
	 *
	 * @return dsUsoEmpresa
	 */
	public String getDsUsoEmpresa() {
		return dsUsoEmpresa;
	}

	/**
	 * Set: dsUsoEmpresa.
	 *
	 * @param dsUsoEmpresa the ds uso empresa
	 */
	public void setDsUsoEmpresa(String dsUsoEmpresa) {
		this.dsUsoEmpresa = dsUsoEmpresa;
	}

	/**
	 * Get: dtAgendamento.
	 *
	 * @return dtAgendamento
	 */
	public String getDtAgendamento() {
		return dtAgendamento;
	}

	/**
	 * Set: dtAgendamento.
	 *
	 * @param dtAgendamento the dt agendamento
	 */
	public void setDtAgendamento(String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	/**
	 * Get: dtEmissaoDocumento.
	 *
	 * @return dtEmissaoDocumento
	 */
	public String getDtEmissaoDocumento() {
		return dtEmissaoDocumento;
	}

	/**
	 * Set: dtEmissaoDocumento.
	 *
	 * @param dtEmissaoDocumento the dt emissao documento
	 */
	public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
		this.dtEmissaoDocumento = dtEmissaoDocumento;
	}

	/**
	 * Get: dtNascimentoBeneficiario.
	 *
	 * @return dtNascimentoBeneficiario
	 */
	public String getDtNascimentoBeneficiario() {
		return dtNascimentoBeneficiario;
	}

	/**
	 * Set: dtNascimentoBeneficiario.
	 *
	 * @param dtNascimentoBeneficiario the dt nascimento beneficiario
	 */
	public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
		this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
	}

	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	/**
	 * Get: dtVencimento.
	 *
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 *
	 * @param dtVencimento the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: nomeBeneficiario.
	 *
	 * @return nomeBeneficiario
	 */
	public String getNomeBeneficiario() {
		return nomeBeneficiario;
	}

	/**
	 * Set: nomeBeneficiario.
	 *
	 * @param nomeBeneficiario the nome beneficiario
	 */
	public void setNomeBeneficiario(String nomeBeneficiario) {
		this.nomeBeneficiario = nomeBeneficiario;
	}

	/**
	 * Get: nrDocumento.
	 *
	 * @return nrDocumento
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}

	/**
	 * Set: nrDocumento.
	 *
	 * @param nrDocumento the nr documento
	 */
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	/**
	 * Get: nrLoteArquivoRemessa.
	 *
	 * @return nrLoteArquivoRemessa
	 */
	public String getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}

	/**
	 * Set: nrLoteArquivoRemessa.
	 *
	 * @param nrLoteArquivoRemessa the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}

	/**
	 * Get: nroPagamento.
	 *
	 * @return nroPagamento
	 */
	public String getNroPagamento() {
		return nroPagamento;
	}

	/**
	 * Set: nroPagamento.
	 *
	 * @param nroPagamento the nro pagamento
	 */
	public void setNroPagamento(String nroPagamento) {
		this.nroPagamento = nroPagamento;
	}

	/**
	 * Get: nrSequenciaArquivoRemessa.
	 *
	 * @return nrSequenciaArquivoRemessa
	 */
	public String getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}

	/**
	 * Set: nrSequenciaArquivoRemessa.
	 *
	 * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}

	/**
	 * Get: numeroInscricaoBeneficiario.
	 *
	 * @return numeroInscricaoBeneficiario
	 */
	public String getNumeroInscricaoBeneficiario() {
		return numeroInscricaoBeneficiario;
	}

	/**
	 * Set: numeroInscricaoBeneficiario.
	 *
	 * @param numeroInscricaoBeneficiario the numero inscricao beneficiario
	 */
	public void setNumeroInscricaoBeneficiario(String numeroInscricaoBeneficiario) {
		this.numeroInscricaoBeneficiario = numeroInscricaoBeneficiario;
	}

	/**
	 * Get: numeroInscricaoFavorecido.
	 *
	 * @return numeroInscricaoFavorecido
	 */
	public String getNumeroInscricaoFavorecido() {
		return numeroInscricaoFavorecido;
	}

	/**
	 * Set: numeroInscricaoFavorecido.
	 *
	 * @param numeroInscricaoFavorecido the numero inscricao favorecido
	 */
	public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
		this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
	}

	/**
	 * Get: qtMoeda.
	 *
	 * @return qtMoeda
	 */
	public BigDecimal getQtMoeda() {
		return qtMoeda;
	}

	/**
	 * Set: qtMoeda.
	 *
	 * @param qtMoeda the qt moeda
	 */
	public void setQtMoeda(BigDecimal qtMoeda) {
		this.qtMoeda = qtMoeda;
	}

	/**
	 * Get: tipoContaCredito.
	 *
	 * @return tipoContaCredito
	 */
	public String getTipoContaCredito() {
		return tipoContaCredito;
	}

	/**
	 * Set: tipoContaCredito.
	 *
	 * @param tipoContaCredito the tipo conta credito
	 */
	public void setTipoContaCredito(String tipoContaCredito) {
		this.tipoContaCredito = tipoContaCredito;
	}

	/**
	 * Get: tipoContaDestino.
	 *
	 * @return tipoContaDestino
	 */
	public String getTipoContaDestino() {
		return tipoContaDestino;
	}

	/**
	 * Set: tipoContaDestino.
	 *
	 * @param tipoContaDestino the tipo conta destino
	 */
	public void setTipoContaDestino(String tipoContaDestino) {
		this.tipoContaDestino = tipoContaDestino;
	}

	/**
	 * Get: tipoDocumento.
	 *
	 * @return tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Set: tipoDocumento.
	 *
	 * @param tipoDocumento the tipo documento
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Get: tipoFavorecido.
	 *
	 * @return tipoFavorecido
	 */
	public Integer getTipoFavorecido() {
		return tipoFavorecido;
	}

	/**
	 * Set: tipoFavorecido.
	 *
	 * @param tipoFavorecido the tipo favorecido
	 */
	public void setTipoFavorecido(Integer tipoFavorecido) {
		this.tipoFavorecido = tipoFavorecido;
	}

	/**
	 * Get: vlAbatimento.
	 *
	 * @return vlAbatimento
	 */
	public BigDecimal getVlAbatimento() {
		return vlAbatimento;
	}

	/**
	 * Set: vlAbatimento.
	 *
	 * @param vlAbatimento the vl abatimento
	 */
	public void setVlAbatimento(BigDecimal vlAbatimento) {
		this.vlAbatimento = vlAbatimento;
	}

	/**
	 * Get: vlAcrescimo.
	 *
	 * @return vlAcrescimo
	 */
	public BigDecimal getVlAcrescimo() {
		return vlAcrescimo;
	}

	/**
	 * Set: vlAcrescimo.
	 *
	 * @param vlAcrescimo the vl acrescimo
	 */
	public void setVlAcrescimo(BigDecimal vlAcrescimo) {
		this.vlAcrescimo = vlAcrescimo;
	}

	/**
	 * Get: vlDeducao.
	 *
	 * @return vlDeducao
	 */
	public BigDecimal getVlDeducao() {
		return vlDeducao;
	}

	/**
	 * Set: vlDeducao.
	 *
	 * @param vlDeducao the vl deducao
	 */
	public void setVlDeducao(BigDecimal vlDeducao) {
		this.vlDeducao = vlDeducao;
	}

	/**
	 * Get: vlDesconto.
	 *
	 * @return vlDesconto
	 */
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}

	/**
	 * Set: vlDesconto.
	 *
	 * @param vlDesconto the vl desconto
	 */
	public void setVlDesconto(BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	/**
	 * Get: vlDocumento.
	 *
	 * @return vlDocumento
	 */
	public BigDecimal getVlDocumento() {
		return vlDocumento;
	}

	/**
	 * Set: vlDocumento.
	 *
	 * @param vlDocumento the vl documento
	 */
	public void setVlDocumento(BigDecimal vlDocumento) {
		this.vlDocumento = vlDocumento;
	}

	/**
	 * Get: vlImpostoRenda.
	 *
	 * @return vlImpostoRenda
	 */
	public BigDecimal getVlImpostoRenda() {
		return vlImpostoRenda;
	}

	/**
	 * Set: vlImpostoRenda.
	 *
	 * @param vlImpostoRenda the vl imposto renda
	 */
	public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
		this.vlImpostoRenda = vlImpostoRenda;
	}

	/**
	 * Get: vlInss.
	 *
	 * @return vlInss
	 */
	public BigDecimal getVlInss() {
		return vlInss;
	}

	/**
	 * Set: vlInss.
	 *
	 * @param vlInss the vl inss
	 */
	public void setVlInss(BigDecimal vlInss) {
		this.vlInss = vlInss;
	}

	/**
	 * Get: vlIof.
	 *
	 * @return vlIof
	 */
	public BigDecimal getVlIof() {
		return vlIof;
	}

	/**
	 * Set: vlIof.
	 *
	 * @param vlIof the vl iof
	 */
	public void setVlIof(BigDecimal vlIof) {
		this.vlIof = vlIof;
	}

	/**
	 * Get: vlIss.
	 *
	 * @return vlIss
	 */
	public BigDecimal getVlIss() {
		return vlIss;
	}

	/**
	 * Set: vlIss.
	 *
	 * @param vlIss the vl iss
	 */
	public void setVlIss(BigDecimal vlIss) {
		this.vlIss = vlIss;
	}

	/**
	 * Get: vlMora.
	 *
	 * @return vlMora
	 */
	public BigDecimal getVlMora() {
		return vlMora;
	}

	/**
	 * Set: vlMora.
	 *
	 * @param vlMora the vl mora
	 */
	public void setVlMora(BigDecimal vlMora) {
		this.vlMora = vlMora;
	}

	/**
	 * Get: vlMulta.
	 *
	 * @return vlMulta
	 */
	public BigDecimal getVlMulta() {
		return vlMulta;
	}

	/**
	 * Set: vlMulta.
	 *
	 * @param vlMulta the vl multa
	 */
	public void setVlMulta(BigDecimal vlMulta) {
		this.vlMulta = vlMulta;
	}

	/**
	 * Get: vlrAgendamento.
	 *
	 * @return vlrAgendamento
	 */
	public BigDecimal getVlrAgendamento() {
		return vlrAgendamento;
	}

	/**
	 * Set: vlrAgendamento.
	 *
	 * @param vlrAgendamento the vlr agendamento
	 */
	public void setVlrAgendamento(BigDecimal vlrAgendamento) {
		this.vlrAgendamento = vlrAgendamento;
	}

	/**
	 * Get: vlrEfetivacao.
	 *
	 * @return vlrEfetivacao
	 */
	public BigDecimal getVlrEfetivacao() {
		return vlrEfetivacao;
	}

	/**
	 * Set: vlrEfetivacao.
	 *
	 * @param vlrEfetivacao the vlr efetivacao
	 */
	public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
		this.vlrEfetivacao = vlrEfetivacao;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: camaraCentralizadora.
	 *
	 * @return camaraCentralizadora
	 */
	public String getCamaraCentralizadora() {
		return camaraCentralizadora;
	}

	/**
	 * Set: camaraCentralizadora.
	 *
	 * @param camaraCentralizadora the camara centralizadora
	 */
	public void setCamaraCentralizadora(String camaraCentralizadora) {
		this.camaraCentralizadora = camaraCentralizadora;
	}

	/**
	 * Get: finalidadeDoc.
	 *
	 * @return finalidadeDoc
	 */
	public String getFinalidadeDoc() {
		return finalidadeDoc;
	}

	/**
	 * Set: finalidadeDoc.
	 *
	 * @param finalidadeDoc the finalidade doc
	 */
	public void setFinalidadeDoc(String finalidadeDoc) {
		this.finalidadeDoc = finalidadeDoc;
	}

	/**
	 * Get: identificacaoTitularidade.
	 *
	 * @return identificacaoTitularidade
	 */
	public String getIdentificacaoTitularidade() {
		return identificacaoTitularidade;
	}

	/**
	 * Set: identificacaoTitularidade.
	 *
	 * @param identificacaoTitularidade the identificacao titularidade
	 */
	public void setIdentificacaoTitularidade(String identificacaoTitularidade) {
		this.identificacaoTitularidade = identificacaoTitularidade;
	}

	/**
	 * Get: identificacaoTransferenciaDoc.
	 *
	 * @return identificacaoTransferenciaDoc
	 */
	public String getIdentificacaoTransferenciaDoc() {
		return identificacaoTransferenciaDoc;
	}

	/**
	 * Set: identificacaoTransferenciaDoc.
	 *
	 * @param identificacaoTransferenciaDoc the identificacao transferencia doc
	 */
	public void setIdentificacaoTransferenciaDoc(String identificacaoTransferenciaDoc) {
		this.identificacaoTransferenciaDoc = identificacaoTransferenciaDoc;
	}

	/**
	 * Get: finalidadeTed.
	 *
	 * @return finalidadeTed
	 */
	public String getFinalidadeTed() {
		return finalidadeTed;
	}

	/**
	 * Set: finalidadeTed.
	 *
	 * @param finalidadeTed the finalidade ted
	 */
	public void setFinalidadeTed(String finalidadeTed) {
		this.finalidadeTed = finalidadeTed;
	}

	/**
	 * Get: identificacaoTransferenciaTed.
	 *
	 * @return identificacaoTransferenciaTed
	 */
	public String getIdentificacaoTransferenciaTed() {
		return identificacaoTransferenciaTed;
	}

	/**
	 * Set: identificacaoTransferenciaTed.
	 *
	 * @param identificacaoTransferenciaTed the identificacao transferencia ted
	 */
	public void setIdentificacaoTransferenciaTed(String identificacaoTransferenciaTed) {
		this.identificacaoTransferenciaTed = identificacaoTransferenciaTed;
	}

	/**
	 * Get: dataExpiracaoCredito.
	 *
	 * @return dataExpiracaoCredito
	 */
	public String getDataExpiracaoCredito() {
		return dataExpiracaoCredito;
	}

	/**
	 * Set: dataExpiracaoCredito.
	 *
	 * @param dataExpiracaoCredito the data expiracao credito
	 */
	public void setDataExpiracaoCredito(String dataExpiracaoCredito) {
		this.dataExpiracaoCredito = dataExpiracaoCredito;
	}

	/**
	 * Get: dataRetirada.
	 *
	 * @return dataRetirada
	 */
	public String getDataRetirada() {
		return dataRetirada;
	}

	/**
	 * Set: dataRetirada.
	 *
	 * @param dataRetirada the data retirada
	 */
	public void setDataRetirada(String dataRetirada) {
		this.dataRetirada = dataRetirada;
	}

	/**
	 * Get: identificadorRetirada.
	 *
	 * @return identificadorRetirada
	 */
	public String getIdentificadorRetirada() {
		return identificadorRetirada;
	}

	/**
	 * Set: identificadorRetirada.
	 *
	 * @param identificadorRetirada the identificador retirada
	 */
	public void setIdentificadorRetirada(String identificadorRetirada) {
		this.identificadorRetirada = identificadorRetirada;
	}

	/**
	 * Get: identificadorTipoRetirada.
	 *
	 * @return identificadorTipoRetirada
	 */
	public String getIdentificadorTipoRetirada() {
		return identificadorTipoRetirada;
	}

	/**
	 * Set: identificadorTipoRetirada.
	 *
	 * @param identificadorTipoRetirada the identificador tipo retirada
	 */
	public void setIdentificadorTipoRetirada(String identificadorTipoRetirada) {
		this.identificadorTipoRetirada = identificadorTipoRetirada;
	}

	/**
	 * Get: indicadorAssociadoUnicaAgencia.
	 *
	 * @return indicadorAssociadoUnicaAgencia
	 */
	public String getIndicadorAssociadoUnicaAgencia() {
		return indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Set: indicadorAssociadoUnicaAgencia.
	 *
	 * @param indicadorAssociadoUnicaAgencia the indicador associado unica agencia
	 */
	public void setIndicadorAssociadoUnicaAgencia(String indicadorAssociadoUnicaAgencia) {
		this.indicadorAssociadoUnicaAgencia = indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Get: instrucaoPagamento.
	 *
	 * @return instrucaoPagamento
	 */
	public String getInstrucaoPagamento() {
		return instrucaoPagamento;
	}

	/**
	 * Set: instrucaoPagamento.
	 *
	 * @param instrucaoPagamento the instrucao pagamento
	 */
	public void setInstrucaoPagamento(String instrucaoPagamento) {
		this.instrucaoPagamento = instrucaoPagamento;
	}

	/**
	 * Get: numeroCheque.
	 *
	 * @return numeroCheque
	 */
	public String getNumeroCheque() {
		return numeroCheque;
	}

	/**
	 * Set: numeroCheque.
	 *
	 * @param numeroCheque the numero cheque
	 */
	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}

	/**
	 * Get: numeroOp.
	 *
	 * @return numeroOp
	 */
	public String getNumeroOp() {
		return numeroOp;
	}

	/**
	 * Set: numeroOp.
	 *
	 * @param numeroOp the numero op
	 */
	public void setNumeroOp(String numeroOp) {
		this.numeroOp = numeroOp;
	}

	/**
	 * Get: serieCheque.
	 *
	 * @return serieCheque
	 */
	public String getSerieCheque() {
		return serieCheque;
	}

	/**
	 * Set: serieCheque.
	 *
	 * @param serieCheque the serie cheque
	 */
	public void setSerieCheque(String serieCheque) {
		this.serieCheque = serieCheque;
	}

	/**
	 * Get: vlImpostoMovFinanceira.
	 *
	 * @return vlImpostoMovFinanceira
	 */
	public BigDecimal getVlImpostoMovFinanceira() {
		return vlImpostoMovFinanceira;
	}

	/**
	 * Set: vlImpostoMovFinanceira.
	 *
	 * @param vlImpostoMovFinanceira the vl imposto mov financeira
	 */
	public void setVlImpostoMovFinanceira(BigDecimal vlImpostoMovFinanceira) {
		this.vlImpostoMovFinanceira = vlImpostoMovFinanceira;
	}

	/**
	 * Get: codigoBarras.
	 *
	 * @return codigoBarras
	 */
	public String getCodigoBarras() {
		return codigoBarras;
	}

	/**
	 * Set: codigoBarras.
	 *
	 * @param codigoBarras the codigo barras
	 */
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	/**
	 * Get: agenteArrecadador.
	 *
	 * @return agenteArrecadador
	 */
	public String getAgenteArrecadador() {
		return agenteArrecadador;
	}

	/**
	 * Set: agenteArrecadador.
	 *
	 * @param agenteArrecadador the agente arrecadador
	 */
	public void setAgenteArrecadador(String agenteArrecadador) {
		this.agenteArrecadador = agenteArrecadador;
	}

	/**
	 * Get: anoExercicio.
	 *
	 * @return anoExercicio
	 */
	public String getAnoExercicio() {
		return anoExercicio;
	}

	/**
	 * Set: anoExercicio.
	 *
	 * @param anoExercicio the ano exercicio
	 */
	public void setAnoExercicio(String anoExercicio) {
		this.anoExercicio = anoExercicio;
	}

	/**
	 * Get: codigoReceita.
	 *
	 * @return codigoReceita
	 */
	public String getCodigoReceita() {
		return codigoReceita;
	}

	/**
	 * Set: codigoReceita.
	 *
	 * @param codigoReceita the codigo receita
	 */
	public void setCodigoReceita(String codigoReceita) {
		this.codigoReceita = codigoReceita;
	}

	/**
	 * Get: dataReferencia.
	 *
	 * @return dataReferencia
	 */
	public String getDataReferencia() {
		return dataReferencia;
	}

	/**
	 * Set: dataReferencia.
	 *
	 * @param dataReferencia the data referencia
	 */
	public void setDataReferencia(String dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	/**
	 * Get: dddContribuinte.
	 *
	 * @return dddContribuinte
	 */
	public String getDddContribuinte() {
		return dddContribuinte;
	}

	/**
	 * Set: dddContribuinte.
	 *
	 * @param dddContribuinte the ddd contribuinte
	 */
	public void setDddContribuinte(String dddContribuinte) {
		this.dddContribuinte = dddContribuinte;
	}

	/**
	 * Get: inscricaoEstadualContribuinte.
	 *
	 * @return inscricaoEstadualContribuinte
	 */
	public String getInscricaoEstadualContribuinte() {
		return inscricaoEstadualContribuinte;
	}

	/**
	 * Set: inscricaoEstadualContribuinte.
	 *
	 * @param inscricaoEstadualContribuinte the inscricao estadual contribuinte
	 */
	public void setInscricaoEstadualContribuinte(String inscricaoEstadualContribuinte) {
		this.inscricaoEstadualContribuinte = inscricaoEstadualContribuinte;
	}

	/**
	 * Get: numeroCotaParcela.
	 *
	 * @return numeroCotaParcela
	 */
	public String getNumeroCotaParcela() {
		return numeroCotaParcela;
	}

	/**
	 * Set: numeroCotaParcela.
	 *
	 * @param numeroCotaParcela the numero cota parcela
	 */
	public void setNumeroCotaParcela(String numeroCotaParcela) {
		this.numeroCotaParcela = numeroCotaParcela;
	}

	/**
	 * Get: numeroReferencia.
	 *
	 * @return numeroReferencia
	 */
	public String getNumeroReferencia() {
		return numeroReferencia;
	}

	/**
	 * Set: numeroReferencia.
	 *
	 * @param numeroReferencia the numero referencia
	 */
	public void setNumeroReferencia(String numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}

	/**
	 * Get: percentualReceita.
	 *
	 * @return percentualReceita
	 */
	public BigDecimal getPercentualReceita() {
		return percentualReceita;
	}

	/**
	 * Set: percentualReceita.
	 *
	 * @param percentualReceita the percentual receita
	 */
	public void setPercentualReceita(BigDecimal percentualReceita) {
		this.percentualReceita = percentualReceita;
	}

	/**
	 * Get: periodoApuracao.
	 *
	 * @return periodoApuracao
	 */
	public String getPeriodoApuracao() {
		return periodoApuracao;
	}

	/**
	 * Set: periodoApuracao.
	 *
	 * @param periodoApuracao the periodo apuracao
	 */
	public void setPeriodoApuracao(String periodoApuracao) {
		this.periodoApuracao = periodoApuracao;
	}

	/**
	 * Get: telefoneContribuinte.
	 *
	 * @return telefoneContribuinte
	 */
	public String getTelefoneContribuinte() {
		return telefoneContribuinte;
	}

	/**
	 * Set: telefoneContribuinte.
	 *
	 * @param telefoneContribuinte the telefone contribuinte
	 */
	public void setTelefoneContribuinte(String telefoneContribuinte) {
		this.telefoneContribuinte = telefoneContribuinte;
	}

	/**
	 * Get: vlAtualizacaoMonetaria.
	 *
	 * @return vlAtualizacaoMonetaria
	 */
	public BigDecimal getVlAtualizacaoMonetaria() {
		return vlAtualizacaoMonetaria;
	}

	/**
	 * Set: vlAtualizacaoMonetaria.
	 *
	 * @param vlAtualizacaoMonetaria the vl atualizacao monetaria
	 */
	public void setVlAtualizacaoMonetaria(BigDecimal vlAtualizacaoMonetaria) {
		this.vlAtualizacaoMonetaria = vlAtualizacaoMonetaria;
	}

	/**
	 * Get: vlPrincipal.
	 *
	 * @return vlPrincipal
	 */
	public BigDecimal getVlPrincipal() {
		return vlPrincipal;
	}

	/**
	 * Set: vlPrincipal.
	 *
	 * @param vlPrincipal the vl principal
	 */
	public void setVlPrincipal(BigDecimal vlPrincipal) {
		this.vlPrincipal = vlPrincipal;
	}

	/**
	 * Get: vlReceita.
	 *
	 * @return vlReceita
	 */
	public BigDecimal getVlReceita() {
		return vlReceita;
	}

	/**
	 * Set: vlReceita.
	 *
	 * @param vlReceita the vl receita
	 */
	public void setVlReceita(BigDecimal vlReceita) {
		this.vlReceita = vlReceita;
	}

	/**
	 * Get: vlTotal.
	 *
	 * @return vlTotal
	 */
	public BigDecimal getVlTotal() {
		return vlTotal;
	}

	/**
	 * Set: vlTotal.
	 *
	 * @param vlTotal the vl total
	 */
	public void setVlTotal(BigDecimal vlTotal) {
		this.vlTotal = vlTotal;
	}

	/**
	 * Get: codigoCnae.
	 *
	 * @return codigoCnae
	 */
	public String getCodigoCnae() {
		return codigoCnae;
	}

	/**
	 * Set: codigoCnae.
	 *
	 * @param codigoCnae the codigo cnae
	 */
	public void setCodigoCnae(String codigoCnae) {
		this.codigoCnae = codigoCnae;
	}

	/**
	 * Get: codigoOrgaoFavorecido.
	 *
	 * @return codigoOrgaoFavorecido
	 */
	public String getCodigoOrgaoFavorecido() {
		return codigoOrgaoFavorecido;
	}

	/**
	 * Set: codigoOrgaoFavorecido.
	 *
	 * @param codigoOrgaoFavorecido the codigo orgao favorecido
	 */
	public void setCodigoOrgaoFavorecido(String codigoOrgaoFavorecido) {
		this.codigoOrgaoFavorecido = codigoOrgaoFavorecido;
	}

	/**
	 * Get: codigoUfFavorecida.
	 *
	 * @return codigoUfFavorecida
	 */
	public String getCodigoUfFavorecida() {
		return codigoUfFavorecida;
	}

	/**
	 * Set: codigoUfFavorecida.
	 *
	 * @param codigoUfFavorecida the codigo uf favorecida
	 */
	public void setCodigoUfFavorecida(String codigoUfFavorecida) {
		this.codigoUfFavorecida = codigoUfFavorecida;
	}

	/**
	 * Get: descricaoUfFavorecida.
	 *
	 * @return descricaoUfFavorecida
	 */
	public String getDescricaoUfFavorecida() {
		return descricaoUfFavorecida;
	}

	/**
	 * Set: descricaoUfFavorecida.
	 *
	 * @param descricaoUfFavorecida the descricao uf favorecida
	 */
	public void setDescricaoUfFavorecida(String descricaoUfFavorecida) {
		this.descricaoUfFavorecida = descricaoUfFavorecida;
	}

	/**
	 * Get: nomeOrgaoFavorecido.
	 *
	 * @return nomeOrgaoFavorecido
	 */
	public String getNomeOrgaoFavorecido() {
		return nomeOrgaoFavorecido;
	}

	/**
	 * Set: nomeOrgaoFavorecido.
	 *
	 * @param nomeOrgaoFavorecido the nome orgao favorecido
	 */
	public void setNomeOrgaoFavorecido(String nomeOrgaoFavorecido) {
		this.nomeOrgaoFavorecido = nomeOrgaoFavorecido;
	}

	/**
	 * Get: numeroParcelamento.
	 *
	 * @return numeroParcelamento
	 */
	public String getNumeroParcelamento() {
		return numeroParcelamento;
	}

	/**
	 * Set: numeroParcelamento.
	 *
	 * @param numeroParcelamento the numero parcelamento
	 */
	public void setNumeroParcelamento(String numeroParcelamento) {
		this.numeroParcelamento = numeroParcelamento;
	}

	/**
	 * Get: observacaoTributo.
	 *
	 * @return observacaoTributo
	 */
	public String getObservacaoTributo() {
		return observacaoTributo;
	}

	/**
	 * Set: observacaoTributo.
	 *
	 * @param observacaoTributo the observacao tributo
	 */
	public void setObservacaoTributo(String observacaoTributo) {
		this.observacaoTributo = observacaoTributo;
	}

	/**
	 * Get: placaVeiculo.
	 *
	 * @return placaVeiculo
	 */
	public String getPlacaVeiculo() {
		return placaVeiculo;
	}

	/**
	 * Set: placaVeiculo.
	 *
	 * @param placaVeiculo the placa veiculo
	 */
	public void setPlacaVeiculo(String placaVeiculo) {
		this.placaVeiculo = placaVeiculo;
	}

	/**
	 * Get: vlAcrescimoFinanceiro.
	 *
	 * @return vlAcrescimoFinanceiro
	 */
	public BigDecimal getVlAcrescimoFinanceiro() {
		return vlAcrescimoFinanceiro;
	}

	/**
	 * Set: vlAcrescimoFinanceiro.
	 *
	 * @param vlAcrescimoFinanceiro the vl acrescimo financeiro
	 */
	public void setVlAcrescimoFinanceiro(BigDecimal vlAcrescimoFinanceiro) {
		this.vlAcrescimoFinanceiro = vlAcrescimoFinanceiro;
	}

	/**
	 * Get: vlHonorarioAdvogado.
	 *
	 * @return vlHonorarioAdvogado
	 */
	public BigDecimal getVlHonorarioAdvogado() {
		return vlHonorarioAdvogado;
	}

	/**
	 * Set: vlHonorarioAdvogado.
	 *
	 * @param vlHonorarioAdvogado the vl honorario advogado
	 */
	public void setVlHonorarioAdvogado(BigDecimal vlHonorarioAdvogado) {
		this.vlHonorarioAdvogado = vlHonorarioAdvogado;
	}

	/**
	 * Get: codigoInss.
	 *
	 * @return codigoInss
	 */
	public String getCodigoInss() {
		return codigoInss;
	}

	/**
	 * Set: codigoInss.
	 *
	 * @param codigoInss the codigo inss
	 */
	public void setCodigoInss(String codigoInss) {
		this.codigoInss = codigoInss;
	}

	/**
	 * Get: dataCompetencia.
	 *
	 * @return dataCompetencia
	 */
	public String getDataCompetencia() {
		return dataCompetencia;
	}

	/**
	 * Set: dataCompetencia.
	 *
	 * @param dataCompetencia the data competencia
	 */
	public void setDataCompetencia(String dataCompetencia) {
		this.dataCompetencia = dataCompetencia;
	}

	/**
	 * Get: vlOutrasEntidades.
	 *
	 * @return vlOutrasEntidades
	 */
	public BigDecimal getVlOutrasEntidades() {
		return vlOutrasEntidades;
	}

	/**
	 * Set: vlOutrasEntidades.
	 *
	 * @param vlOutrasEntidades the vl outras entidades
	 */
	public void setVlOutrasEntidades(BigDecimal vlOutrasEntidades) {
		this.vlOutrasEntidades = vlOutrasEntidades;
	}

	/**
	 * Get: codigoTributo.
	 *
	 * @return codigoTributo
	 */
	public String getCodigoTributo() {
		return codigoTributo;
	}

	/**
	 * Set: codigoTributo.
	 *
	 * @param codigoTributo the codigo tributo
	 */
	public void setCodigoTributo(String codigoTributo) {
		this.codigoTributo = codigoTributo;
	}

	/**
	 * Get: codigoRenavam.
	 *
	 * @return codigoRenavam
	 */
	public String getCodigoRenavam() {
		return codigoRenavam;
	}

	/**
	 * Set: codigoRenavam.
	 *
	 * @param codigoRenavam the codigo renavam
	 */
	public void setCodigoRenavam(String codigoRenavam) {
		this.codigoRenavam = codigoRenavam;
	}

	/**
	 * Get: municipioTributo.
	 *
	 * @return municipioTributo
	 */
	public String getMunicipioTributo() {
		return municipioTributo;
	}

	/**
	 * Set: municipioTributo.
	 *
	 * @param municipioTributo the municipio tributo
	 */
	public void setMunicipioTributo(String municipioTributo) {
		this.municipioTributo = municipioTributo;
	}

	/**
	 * Get: numeroNsu.
	 *
	 * @return numeroNsu
	 */
	public String getNumeroNsu() {
		return numeroNsu;
	}

	/**
	 * Set: numeroNsu.
	 *
	 * @param numeroNsu the numero nsu
	 */
	public void setNumeroNsu(String numeroNsu) {
		this.numeroNsu = numeroNsu;
	}

	/**
	 * Get: opcaoRetiradaTributo.
	 *
	 * @return opcaoRetiradaTributo
	 */
	public String getOpcaoRetiradaTributo() {
		return opcaoRetiradaTributo;
	}

	/**
	 * Set: opcaoRetiradaTributo.
	 *
	 * @param opcaoRetiradaTributo the opcao retirada tributo
	 */
	public void setOpcaoRetiradaTributo(String opcaoRetiradaTributo) {
		this.opcaoRetiradaTributo = opcaoRetiradaTributo;
	}

	/**
	 * Get: opcaoTributo.
	 *
	 * @return opcaoTributo
	 */
	public String getOpcaoTributo() {
		return opcaoTributo;
	}

	/**
	 * Set: opcaoTributo.
	 *
	 * @param opcaoTributo the opcao tributo
	 */
	public void setOpcaoTributo(String opcaoTributo) {
		this.opcaoTributo = opcaoTributo;
	}

	/**
	 * Get: ufTributo.
	 *
	 * @return ufTributo
	 */
	public String getUfTributo() {
		return ufTributo;
	}

	/**
	 * Set: ufTributo.
	 *
	 * @param ufTributo the uf tributo
	 */
	public void setUfTributo(String ufTributo) {
		this.ufTributo = ufTributo;
	}

	/**
	 * Get: bancoCartaoDepositoIdentificado.
	 *
	 * @return bancoCartaoDepositoIdentificado
	 */
	public String getBancoCartaoDepositoIdentificado() {
		return bancoCartaoDepositoIdentificado;
	}

	/**
	 * Set: bancoCartaoDepositoIdentificado.
	 *
	 * @param bancoCartaoDepositoIdentificado the banco cartao deposito identificado
	 */
	public void setBancoCartaoDepositoIdentificado(String bancoCartaoDepositoIdentificado) {
		this.bancoCartaoDepositoIdentificado = bancoCartaoDepositoIdentificado;
	}

	/**
	 * Get: bimCartaoDepositoIdentificado.
	 *
	 * @return bimCartaoDepositoIdentificado
	 */
	public String getBimCartaoDepositoIdentificado() {
		return bimCartaoDepositoIdentificado;
	}

	/**
	 * Set: bimCartaoDepositoIdentificado.
	 *
	 * @param bimCartaoDepositoIdentificado the bim cartao deposito identificado
	 */
	public void setBimCartaoDepositoIdentificado(String bimCartaoDepositoIdentificado) {
		this.bimCartaoDepositoIdentificado = bimCartaoDepositoIdentificado;
	}

	/**
	 * Get: cartaoDepositoIdentificado.
	 *
	 * @return cartaoDepositoIdentificado
	 */
	public String getCartaoDepositoIdentificado() {
		return cartaoDepositoIdentificado;
	}

	/**
	 * Set: cartaoDepositoIdentificado.
	 *
	 * @param cartaoDepositoIdentificado the cartao deposito identificado
	 */
	public void setCartaoDepositoIdentificado(String cartaoDepositoIdentificado) {
		this.cartaoDepositoIdentificado = cartaoDepositoIdentificado;
	}

	/**
	 * Get: clienteDepositoIdentificado.
	 *
	 * @return clienteDepositoIdentificado
	 */
	public String getClienteDepositoIdentificado() {
		return clienteDepositoIdentificado;
	}

	/**
	 * Set: clienteDepositoIdentificado.
	 *
	 * @param clienteDepositoIdentificado the cliente deposito identificado
	 */
	public void setClienteDepositoIdentificado(String clienteDepositoIdentificado) {
		this.clienteDepositoIdentificado = clienteDepositoIdentificado;
	}

	/**
	 * Get: codigoDepositante.
	 *
	 * @return codigoDepositante
	 */
	public String getCodigoDepositante() {
		return codigoDepositante;
	}

	/**
	 * Set: codigoDepositante.
	 *
	 * @param codigoDepositante the codigo depositante
	 */
	public void setCodigoDepositante(String codigoDepositante) {
		this.codigoDepositante = codigoDepositante;
	}

	/**
	 * Get: nomeDepositante.
	 *
	 * @return nomeDepositante
	 */
	public String getNomeDepositante() {
		return nomeDepositante;
	}

	/**
	 * Set: nomeDepositante.
	 *
	 * @param nomeDepositante the nome depositante
	 */
	public void setNomeDepositante(String nomeDepositante) {
		this.nomeDepositante = nomeDepositante;
	}

	/**
	 * Get: produtoDepositoIdentificado.
	 *
	 * @return produtoDepositoIdentificado
	 */
	public String getProdutoDepositoIdentificado() {
		return produtoDepositoIdentificado;
	}

	/**
	 * Set: produtoDepositoIdentificado.
	 *
	 * @param produtoDepositoIdentificado the produto deposito identificado
	 */
	public void setProdutoDepositoIdentificado(String produtoDepositoIdentificado) {
		this.produtoDepositoIdentificado = produtoDepositoIdentificado;
	}

	/**
	 * Get: sequenciaProdutoDepositoIdentificado.
	 *
	 * @return sequenciaProdutoDepositoIdentificado
	 */
	public String getSequenciaProdutoDepositoIdentificado() {
		return sequenciaProdutoDepositoIdentificado;
	}

	/**
	 * Set: sequenciaProdutoDepositoIdentificado.
	 *
	 * @param sequenciaProdutoDepositoIdentificado the sequencia produto deposito identificado
	 */
	public void setSequenciaProdutoDepositoIdentificado(String sequenciaProdutoDepositoIdentificado) {
		this.sequenciaProdutoDepositoIdentificado = sequenciaProdutoDepositoIdentificado;
	}

	/**
	 * Get: tipoDespositoIdentificado.
	 *
	 * @return tipoDespositoIdentificado
	 */
	public String getTipoDespositoIdentificado() {
		return tipoDespositoIdentificado;
	}

	/**
	 * Set: tipoDespositoIdentificado.
	 *
	 * @param tipoDespositoIdentificado the tipo desposito identificado
	 */
	public void setTipoDespositoIdentificado(String tipoDespositoIdentificado) {
		this.tipoDespositoIdentificado = tipoDespositoIdentificado;
	}

	/**
	 * Get: viaCartaoDepositoIdentificado.
	 *
	 * @return viaCartaoDepositoIdentificado
	 */
	public String getViaCartaoDepositoIdentificado() {
		return viaCartaoDepositoIdentificado;
	}

	/**
	 * Set: viaCartaoDepositoIdentificado.
	 *
	 * @param viaCartaoDepositoIdentificado the via cartao deposito identificado
	 */
	public void setViaCartaoDepositoIdentificado(String viaCartaoDepositoIdentificado) {
		this.viaCartaoDepositoIdentificado = viaCartaoDepositoIdentificado;
	}

	/**
	 * Get: codigoOrdemCredito.
	 *
	 * @return codigoOrdemCredito
	 */
	public String getCodigoOrdemCredito() {
		return codigoOrdemCredito;
	}

	/**
	 * Set: codigoOrdemCredito.
	 *
	 * @param codigoOrdemCredito the codigo ordem credito
	 */
	public void setCodigoOrdemCredito(String codigoOrdemCredito) {
		this.codigoOrdemCredito = codigoOrdemCredito;
	}

	/**
	 * Get: codigoPagador.
	 *
	 * @return codigoPagador
	 */
	public String getCodigoPagador() {
		return codigoPagador;
	}

	/**
	 * Set: codigoPagador.
	 *
	 * @param codigoPagador the codigo pagador
	 */
	public void setCodigoPagador(String codigoPagador) {
		this.codigoPagador = codigoPagador;
	}

	/**
	 * Get: tipoBeneficiario.
	 *
	 * @return tipoBeneficiario
	 */
	public String getTipoBeneficiario() {
		return tipoBeneficiario;
	}

	/**
	 * Set: tipoBeneficiario.
	 *
	 * @param tipoBeneficiario the tipo beneficiario
	 */
	public void setTipoBeneficiario(String tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}

	/**
	 * Get: novaDataAgendamento.
	 *
	 * @return novaDataAgendamento
	 */
	public Date getNovaDataAgendamento() {
		return novaDataAgendamento;
	}

	/**
	 * Set: novaDataAgendamento.
	 *
	 * @param novaDataAgendamento the nova data agendamento
	 */
	public void setNovaDataAgendamento(Date novaDataAgendamento) {
		this.novaDataAgendamento = novaDataAgendamento;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Is panel botoes.
	 *
	 * @return true, if is panel botoes
	 */
	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	/**
	 * Set: panelBotoes.
	 *
	 * @param panelBotoes the panel botoes
	 */
	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	/**
	 * Get: itemSelecionadoAlterarParcial.
	 *
	 * @return itemSelecionadoAlterarParcial
	 */
	public Integer getItemSelecionadoAlterarParcial() {
		return itemSelecionadoAlterarParcial;
	}

	/**
	 * Set: itemSelecionadoAlterarParcial.
	 *
	 * @param itemSelecionadoAlterarParcial the item selecionado alterar parcial
	 */
	public void setItemSelecionadoAlterarParcial(Integer itemSelecionadoAlterarParcial) {
		this.itemSelecionadoAlterarParcial = itemSelecionadoAlterarParcial;
	}

	/**
	 * Get: listaControleRadioAlterarParcial.
	 *
	 * @return listaControleRadioAlterarParcial
	 */
	public List<SelectItem> getListaControleRadioAlterarParcial() {
		return listaControleRadioAlterarParcial;
	}

	/**
	 * Set: listaControleRadioAlterarParcial.
	 *
	 * @param listaControleRadioAlterarParcial the lista controle radio alterar parcial
	 */
	public void setListaControleRadioAlterarParcial(List<SelectItem> listaControleRadioAlterarParcial) {
		this.listaControleRadioAlterarParcial = listaControleRadioAlterarParcial;
	}

	/**
	 * Get: vlrPagamentosAutorizados.
	 *
	 * @return vlrPagamentosAutorizados
	 */
	public BigDecimal getVlrPagamentosAutorizados() {
		return vlrPagamentosAutorizados;
	}

	/**
	 * Set: vlrPagamentosAutorizados.
	 *
	 * @param vlrPagamentosAutorizados the vlr pagamentos autorizados
	 */
	public void setVlrPagamentosAutorizados(BigDecimal vlrPagamentosAutorizados) {
		this.vlrPagamentosAutorizados = vlrPagamentosAutorizados;
	}

	/**
	 * Get: listaGridAlterarSelecionados.
	 *
	 * @return listaGridAlterarSelecionados
	 */
	public List<OcorrenciasDetalharAntPostergarPagtosSaidaDTO> getListaGridAlterarSelecionados() {
		return listaGridAlterarSelecionados;
	}

	/**
	 * Set: listaGridAlterarSelecionados.
	 *
	 * @param listaGridAlterarSelecionados the lista grid alterar selecionados
	 */
	public void setListaGridAlterarSelecionados(
					List<OcorrenciasDetalharAntPostergarPagtosSaidaDTO> listaGridAlterarSelecionados) {
		this.listaGridAlterarSelecionados = listaGridAlterarSelecionados;
	}

	/**
	 * Get: qtdPagamentoAutorizados.
	 *
	 * @return qtdPagamentoAutorizados
	 */
	public Integer getQtdPagamentoAutorizados() {
		return qtdPagamentoAutorizados;
	}

	/**
	 * Set: qtdPagamentoAutorizados.
	 *
	 * @param qtdPagamentoAutorizados the qtd pagamento autorizados
	 */
	public void setQtdPagamentoAutorizados(Integer qtdPagamentoAutorizados) {
		this.qtdPagamentoAutorizados = qtdPagamentoAutorizados;
	}

	/**
	 * Get: dataAgendamento.
	 *
	 * @return dataAgendamento
	 */
	public String getDataAgendamento() {
		return dataAgendamento;
	}

	/**
	 * Set: dataAgendamento.
	 *
	 * @param dataAgendamento the data agendamento
	 */
	public void setDataAgendamento(String dataAgendamento) {
		this.dataAgendamento = dataAgendamento;
	}

	/**
	 * Get: contaDebito.
	 *
	 * @return contaDebito
	 */
	public String getContaDebito() {
		return contaDebito;
	}

	/**
	 * Set: contaDebito.
	 *
	 * @param contaDebito the conta debito
	 */
	public void setContaDebito(String contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: tipoContaDebito.
	 *
	 * @return tipoContaDebito
	 */
	public String getTipoContaDebito() {
		return tipoContaDebito;
	}

	/**
	 * Set: tipoContaDebito.
	 *
	 * @param tipoContaDebito the tipo conta debito
	 */
	public void setTipoContaDebito(String tipoContaDebito) {
		this.tipoContaDebito = tipoContaDebito;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: numeroPagamento.
	 *
	 * @return numeroPagamento
	 */
	public String getNumeroPagamento() {
		return numeroPagamento;
	}

	/**
	 * Set: numeroPagamento.
	 *
	 * @param numeroPagamento the numero pagamento
	 */
	public void setNumeroPagamento(String numeroPagamento) {
		this.numeroPagamento = numeroPagamento;
	}

	/**
	 * Get: logoPath.
	 *
	 * @return logoPath
	 */
	public String getLogoPath() {
		return logoPath;
	}

	/**
	 * Set: logoPath.
	 *
	 * @param logoPath the logo path
	 */
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	/**
	 * Get: dsOrigemPagamento.
	 *
	 * @return dsOrigemPagamento
	 */
	public String getDsOrigemPagamento() {
		return dsOrigemPagamento;
	}

	/**
	 * Set: dsOrigemPagamento.
	 *
	 * @param dsOrigemPagamento the ds origem pagamento
	 */
	public void setDsOrigemPagamento(String dsOrigemPagamento) {
		this.dsOrigemPagamento = dsOrigemPagamento;
	}

	/**
	 * Get: nossoNumero.
	 *
	 * @return nossoNumero
	 */
	public String getNossoNumero() {
		return nossoNumero;
	}

	/**
	 * Set: nossoNumero.
	 *
	 * @param nossoNumero the nosso numero
	 */
	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Is exibe beneficiario.
	 *
	 * @return true, if is exibe beneficiario
	 */
	public boolean isExibeBeneficiario() {
		return exibeBeneficiario;
	}

	/**
	 * Set: exibeBeneficiario.
	 *
	 * @param exibeBeneficiario the exibe beneficiario
	 */
	public void setExibeBeneficiario(boolean exibeBeneficiario) {
		this.exibeBeneficiario = exibeBeneficiario;
	}

	/**
	 * Get: carteira.
	 *
	 * @return carteira
	 */
	public String getCarteira() {
		return carteira;
	}

	/**
	 * Set: carteira.
	 *
	 * @param carteira the carteira
	 */
	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}

	/**
	 * Get: cdIndentificadorJudicial.
	 *
	 * @return cdIndentificadorJudicial
	 */
	public String getCdIndentificadorJudicial() {
		return cdIndentificadorJudicial;
	}

	/**
	 * Set: cdIndentificadorJudicial.
	 *
	 * @param cdIndentificadorJudicial the cd indentificador judicial
	 */
	public void setCdIndentificadorJudicial(String cdIndentificadorJudicial) {
		this.cdIndentificadorJudicial = cdIndentificadorJudicial;
	}

	/**
	 * Get: cdSituacaoTranferenciaAutomatica.
	 *
	 * @return cdSituacaoTranferenciaAutomatica
	 */
	public String getCdSituacaoTranferenciaAutomatica() {
		return cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Set: cdSituacaoTranferenciaAutomatica.
	 *
	 * @param cdSituacaoTranferenciaAutomatica the cd situacao tranferencia automatica
	 */
	public void setCdSituacaoTranferenciaAutomatica(String cdSituacaoTranferenciaAutomatica) {
		this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Get: dtLimiteDescontoPagamento.
	 *
	 * @return dtLimiteDescontoPagamento
	 */
	public String getDtLimiteDescontoPagamento() {
		return dtLimiteDescontoPagamento;
	}

	/**
	 * Set: dtLimiteDescontoPagamento.
	 *
	 * @param dtLimiteDescontoPagamento the dt limite desconto pagamento
	 */
	public void setDtLimiteDescontoPagamento(String dtLimiteDescontoPagamento) {
		this.dtLimiteDescontoPagamento = dtLimiteDescontoPagamento;
	}

	/**
	 * Get: nrRemessa.
	 *
	 * @return nrRemessa
	 */
	public Long getNrRemessa() {
		return nrRemessa;
	}

	/**
	 * Set: nrRemessa.
	 *
	 * @param nrRemessa the nr remessa
	 */
	public void setNrRemessa(Long nrRemessa) {
		this.nrRemessa = nrRemessa;
	}

	/**
	 * Get: descTipoFavorecido.
	 *
	 * @return descTipoFavorecido
	 */
	public String getDescTipoFavorecido() {
		return descTipoFavorecido;
	}

	/**
	 * Set: descTipoFavorecido.
	 *
	 * @param descTipoFavorecido the desc tipo favorecido
	 */
	public void setDescTipoFavorecido(String descTipoFavorecido) {
		this.descTipoFavorecido = descTipoFavorecido;
	}

	/**
	 * Get: dsTipoBeneficiario.
	 *
	 * @return dsTipoBeneficiario
	 */
	public String getDsTipoBeneficiario() {
		return dsTipoBeneficiario;
	}

	/**
	 * Set: dsTipoBeneficiario.
	 *
	 * @param dsTipoBeneficiario the ds tipo beneficiario
	 */
	public void setDsTipoBeneficiario(String dsTipoBeneficiario) {
		this.dsTipoBeneficiario = dsTipoBeneficiario;
	}

	/**
	 * Get: tipoAgendamento.
	 *
	 * @return tipoAgendamento
	 */
	public String getTipoAgendamento() {
		return tipoAgendamento;
	}

	/**
	 * Set: tipoAgendamento.
	 *
	 * @param tipoAgendamento the tipo agendamento
	 */
	public void setTipoAgendamento(String tipoAgendamento) {
		this.tipoAgendamento = tipoAgendamento;
	}

	/**
	 * Get: cdBancoOriginal.
	 *
	 * @return cdBancoOriginal
	 */
	public Integer getCdBancoOriginal() {
		return cdBancoOriginal;
	}

	/**
	 * Set: cdBancoOriginal.
	 *
	 * @param cdBancoOriginal the cd banco original
	 */
	public void setCdBancoOriginal(Integer cdBancoOriginal) {
		this.cdBancoOriginal = cdBancoOriginal;
	}

	/**
	 * Get: dsBancoOriginal.
	 *
	 * @return dsBancoOriginal
	 */
	public String getDsBancoOriginal() {
		return dsBancoOriginal;
	}

	/**
	 * Set: dsBancoOriginal.
	 *
	 * @param dsBancoOriginal the ds banco original
	 */
	public void setDsBancoOriginal(String dsBancoOriginal) {
		this.dsBancoOriginal = dsBancoOriginal;
	}

	/**
	 * Get: cdAgenciaBancariaOriginal.
	 *
	 * @return cdAgenciaBancariaOriginal
	 */
	public Integer getCdAgenciaBancariaOriginal() {
		return cdAgenciaBancariaOriginal;
	}

	/**
	 * Set: cdAgenciaBancariaOriginal.
	 *
	 * @param cdAgenciaBancariaOriginal the cd agencia bancaria original
	 */
	public void setCdAgenciaBancariaOriginal(Integer cdAgenciaBancariaOriginal) {
		this.cdAgenciaBancariaOriginal = cdAgenciaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoAgenciaOriginal.
	 *
	 * @return cdDigitoAgenciaOriginal
	 */
	public String getCdDigitoAgenciaOriginal() {
		return cdDigitoAgenciaOriginal;
	}

	/**
	 * Set: cdDigitoAgenciaOriginal.
	 *
	 * @param cdDigitoAgenciaOriginal the cd digito agencia original
	 */
	public void setCdDigitoAgenciaOriginal(String cdDigitoAgenciaOriginal) {
		this.cdDigitoAgenciaOriginal = cdDigitoAgenciaOriginal;
	}

	/**
	 * Get: dsAgenciaOriginal.
	 *
	 * @return dsAgenciaOriginal
	 */
	public String getDsAgenciaOriginal() {
		return dsAgenciaOriginal;
	}

	/**
	 * Set: dsAgenciaOriginal.
	 *
	 * @param dsAgenciaOriginal the ds agencia original
	 */
	public void setDsAgenciaOriginal(String dsAgenciaOriginal) {
		this.dsAgenciaOriginal = dsAgenciaOriginal;
	}

	/**
	 * Get: cdContaBancariaOriginal.
	 *
	 * @return cdContaBancariaOriginal
	 */
	public Long getCdContaBancariaOriginal() {
		return cdContaBancariaOriginal;
	}

	/**
	 * Set: cdContaBancariaOriginal.
	 *
	 * @param cdContaBancariaOriginal the cd conta bancaria original
	 */
	public void setCdContaBancariaOriginal(Long cdContaBancariaOriginal) {
		this.cdContaBancariaOriginal = cdContaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoContaOriginal.
	 *
	 * @return cdDigitoContaOriginal
	 */
	public String getCdDigitoContaOriginal() {
		return cdDigitoContaOriginal;
	}

	/**
	 * Set: cdDigitoContaOriginal.
	 *
	 * @param cdDigitoContaOriginal the cd digito conta original
	 */
	public void setCdDigitoContaOriginal(String cdDigitoContaOriginal) {
		this.cdDigitoContaOriginal = cdDigitoContaOriginal;
	}

	/**
	 * Get: dsTipoContaOriginal.
	 *
	 * @return dsTipoContaOriginal
	 */
	public String getDsTipoContaOriginal() {
		return dsTipoContaOriginal;
	}

	/**
	 * Set: dsTipoContaOriginal.
	 *
	 * @param dsTipoContaOriginal the ds tipo conta original
	 */
	public void setDsTipoContaOriginal(String dsTipoContaOriginal) {
		this.dsTipoContaOriginal = dsTipoContaOriginal;
	}

	/**
	 * Get: dtDevolucaoEstorno.
	 *
	 * @return dtDevolucaoEstorno
	 */
	public String getDtDevolucaoEstorno() {
		return dtDevolucaoEstorno;
	}

	/**
	 * Set: dtDevolucaoEstorno.
	 *
	 * @param dtDevolucaoEstorno the dt devolucao estorno
	 */
	public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
		this.dtDevolucaoEstorno = dtDevolucaoEstorno;
	}

	/**
	 * Get: dtLimitePagamento.
	 *
	 * @return dtLimitePagamento
	 */
	public String getDtLimitePagamento() {
		return dtLimitePagamento;
	}

	/**
	 * Set: dtLimitePagamento.
	 *
	 * @param dtLimitePagamento the dt limite pagamento
	 */
	public void setDtLimitePagamento(String dtLimitePagamento) {
		this.dtLimitePagamento = dtLimitePagamento;
	}

	/**
	 * Get: dsRastreado.
	 *
	 * @return dsRastreado
	 */
	public String getDsRastreado() {
		return dsRastreado;
	}

	/**
	 * Set: dsRastreado.
	 *
	 * @param dsRastreado the ds rastreado
	 */
	public void setDsRastreado(String dsRastreado) {
		this.dsRastreado = dsRastreado;
	}

	/**
	 * Get: dtFloatingPagamento.
	 *
	 * @return dtFloatingPagamento
	 */
	public String getDtFloatingPagamento() {
		return dtFloatingPagamento;
	}

	/**
	 * Set: dtFloatingPagamento.
	 *
	 * @param dtFloatingPagamento the dt floating pagamento
	 */
	public void setDtFloatingPagamento(String dtFloatingPagamento) {
		this.dtFloatingPagamento = dtFloatingPagamento;
	}

	/**
	 * Get: dtEfetivFloatPgto.
	 *
	 * @return dtEfetivFloatPgto
	 */
	public String getDtEfetivFloatPgto() {
		return dtEfetivFloatPgto;
	}

	/**
	 * Set: dtEfetivFloatPgto.
	 *
	 * @param dtEfetivFloatPgto the dt efetiv float pgto
	 */
	public void setDtEfetivFloatPgto(String dtEfetivFloatPgto) {
		this.dtEfetivFloatPgto = dtEfetivFloatPgto;
	}

	/**
	 * Get: vlFloatingPagamento.
	 *
	 * @return vlFloatingPagamento
	 */
	public BigDecimal getVlFloatingPagamento() {
		return vlFloatingPagamento;
	}

	/**
	 * Set: vlFloatingPagamento.
	 *
	 * @param vlFloatingPagamento the vl floating pagamento
	 */
	public void setVlFloatingPagamento(BigDecimal vlFloatingPagamento) {
		this.vlFloatingPagamento = vlFloatingPagamento;
	}

	/**
	 * Get: cdOperacaoDcom.
	 *
	 * @return cdOperacaoDcom
	 */
	public Long getCdOperacaoDcom() {
		return cdOperacaoDcom;
	}

	/**
	 * Set: cdOperacaoDcom.
	 *
	 * @param cdOperacaoDcom the cd operacao dcom
	 */
	public void setCdOperacaoDcom(Long cdOperacaoDcom) {
		this.cdOperacaoDcom = cdOperacaoDcom;
	}

	/**
	 * Get: dsSituacaoDcom.
	 *
	 * @return dsSituacaoDcom
	 */
	public String getDsSituacaoDcom() {
		return dsSituacaoDcom;
	}

	/**
	 * Set: dsSituacaoDcom.
	 *
	 * @param dsSituacaoDcom the ds situacao dcom
	 */
	public void setDsSituacaoDcom(String dsSituacaoDcom) {
		this.dsSituacaoDcom = dsSituacaoDcom;
	}

	/**
	 * Get: exibeDcom.
	 *
	 * @return exibeDcom
	 */
	public Boolean getExibeDcom() {
		return exibeDcom;
	}

	/**
	 * Set: exibeDcom.
	 *
	 * @param exibeDcom the exibe dcom
	 */
	public void setExibeDcom(Boolean exibeDcom) {
		this.exibeDcom = exibeDcom;
	}

	/**
	 * Get: numControleInternoLote.
	 *
	 * @return numControleInternoLote
	 */
	public Long getNumControleInternoLote() {
		return numControleInternoLote;
	}

	/**
	 * Set: numControleInternoLote.
	 *
	 * @param numControleInternoLote the num controle interno lote
	 */
	public void setNumControleInternoLote(Long numControleInternoLote) {
		this.numControleInternoLote = numControleInternoLote;
	}

    /**
     * Nome: getSaidaDetalhePostergarConsolidadoSolicitacao.
     *
     * @return saidaDetalhePostergarConsolidadoSolicitacao
     */
    public DetalhePostergarConsolidadoSolicitacaoSaidaDTO getSaidaDetalhePostergarConsolidadoSolicitacao() {
        return saidaDetalhePostergarConsolidadoSolicitacao;
    }

    /**
     * Nome: setSaidaDetalhePostergarConsolidadoSolicitacao.
     *
     * @param saidaDetalhePostergarConsolidadoSolicitacao the saida detalhe postergar consolidado solicitacao
     */
    public void setSaidaDetalhePostergarConsolidadoSolicitacao(
        DetalhePostergarConsolidadoSolicitacaoSaidaDTO saidaDetalhePostergarConsolidadoSolicitacao) {
        this.saidaDetalhePostergarConsolidadoSolicitacao = saidaDetalhePostergarConsolidadoSolicitacao;
    }

    /**
     * Nome: getItemSelecionadoConsultaPostergarConsolidado.
     *
     * @return itemSelecionadoConsultaPostergarConsolidado
     */
    public Integer getItemSelecionadoConsultaPostergarConsolidado() {
        return itemSelecionadoConsultaPostergarConsolidado;
    }

    /**
     * Nome: setItemSelecionadoConsultaPostergarConsolidado.
     *
     * @param itemSelecionadoConsultaPostergarConsolidado the item selecionado consulta postergar consolidado
     */
    public void setItemSelecionadoConsultaPostergarConsolidado(Integer itemSelecionadoConsultaPostergarConsolidado) {
        this.itemSelecionadoConsultaPostergarConsolidado = itemSelecionadoConsultaPostergarConsolidado;
    }

    /**
     * Nome: getItemSelecionadoComboSolicitacao.
     *
     * @return itemSelecionadoComboSolicitacao
     */
    public Integer getItemSelecionadoComboSolicitacao() {
        return itemSelecionadoComboSolicitacao;
    }

    /**
     * Nome: setItemSelecionadoComboSolicitacao.
     *
     * @param itemSelecionadoComboSolicitacao the item selecionado combo solicitacao
     */
    public void setItemSelecionadoComboSolicitacao(Integer itemSelecionadoComboSolicitacao) {
        this.itemSelecionadoComboSolicitacao = itemSelecionadoComboSolicitacao;
    }

    /**
     * Nome: getItemSelecionadoComboMotivo.
     *
     * @return itemSelecionadoComboMotivo
     */
    public Integer getItemSelecionadoComboMotivo() {
        return itemSelecionadoComboMotivo;
    }

    /**
     * Nome: setItemSelecionadoComboMotivo.
     *
     * @param itemSelecionadoComboMotivo the item selecionado combo motivo
     */
    public void setItemSelecionadoComboMotivo(Integer itemSelecionadoComboMotivo) {
        this.itemSelecionadoComboMotivo = itemSelecionadoComboMotivo;
    }

    /**
     * Nome: getDataFinalSolicitacaoFiltro.
     *
     * @return dataFinalSolicitacaoFiltro
     */
    public Date getDataFinalSolicitacaoFiltro() {
        return dataFinalSolicitacaoFiltro;
    }

    /**
     * Nome: setDataFinalSolicitacaoFiltro.
     *
     * @param dataFinalSolicitacaoFiltro the data final solicitacao filtro
     */
    public void setDataFinalSolicitacaoFiltro(Date dataFinalSolicitacaoFiltro) {
        this.dataFinalSolicitacaoFiltro = dataFinalSolicitacaoFiltro;
    }

    /**
     * Nome: getDataInicialSolicitacaoFiltro.
     *
     * @return dataInicialSolicitacaoFiltro
     */
    public Date getDataInicialSolicitacaoFiltro() {
        return dataInicialSolicitacaoFiltro;
    }

    /**
     * Nome: setDataInicialSolicitacaoFiltro.
     *
     * @param dataInicialSolicitacaoFiltro the data inicial solicitacao filtro
     */
    public void setDataInicialSolicitacaoFiltro(Date dataInicialSolicitacaoFiltro) {
        this.dataInicialSolicitacaoFiltro = dataInicialSolicitacaoFiltro;
    }

    /**
     * Nome: getListaConsultarSituacaoSolicitacaoEstorno.
     *
     * @return listaConsultarSituacaoSolicitacaoEstorno
     */
    public List<SelectItem> getListaConsultarSituacaoSolicitacaoEstorno() {
        return listaConsultarSituacaoSolicitacaoEstorno;
    }

    /**
     * Nome: setListaConsultarSituacaoSolicitacaoEstorno.
     *
     * @param listaConsultarSituacaoSolicitacaoEstorno the lista consultar situacao solicitacao estorno
     */
    public void setListaConsultarSituacaoSolicitacaoEstorno(List<SelectItem> listaConsultarSituacaoSolicitacaoEstorno) {
        this.listaConsultarSituacaoSolicitacaoEstorno = listaConsultarSituacaoSolicitacaoEstorno;
    }

    /**
     * Nome: getListaConsultarMotivoSituacaoEstorno.
     *
     * @return listaConsultarMotivoSituacaoEstorno
     */
    public List<SelectItem> getListaConsultarMotivoSituacaoEstorno() {
        return listaConsultarMotivoSituacaoEstorno;
    }

    /**
     * Nome: setListaConsultarMotivoSituacaoEstorno.
     *
     * @param listaConsultarMotivoSituacaoEstorno the lista consultar motivo situacao estorno
     */
    public void setListaConsultarMotivoSituacaoEstorno(List<SelectItem> listaConsultarMotivoSituacaoEstorno) {
        this.listaConsultarMotivoSituacaoEstorno = listaConsultarMotivoSituacaoEstorno;
    }

    /**
     * Nome: getCdpessoaJuridicaContrato.
     *
     * @return cdpessoaJuridicaContrato
     */
    public Long getCdpessoaJuridicaContrato() {
        return cdpessoaJuridicaContrato;
    }

    /**
     * Nome: setCdpessoaJuridicaContrato.
     *
     * @param cdpessoaJuridicaContrato the cdpessoa juridica contrato
     */
    public void setCdpessoaJuridicaContrato(Long cdpessoaJuridicaContrato) {
        this.cdpessoaJuridicaContrato = cdpessoaJuridicaContrato;
    }

    /**
     * Nome: getCdTipoContratoNegocio.
     *
     * @return cdTipoContratoNegocio
     */
    public Integer getCdTipoContratoNegocio() {
        return cdTipoContratoNegocio;
    }

    /**
     * Nome: setCdTipoContratoNegocio.
     *
     * @param cdTipoContratoNegocio the cd tipo contrato negocio
     */
    public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
        this.cdTipoContratoNegocio = cdTipoContratoNegocio;
    }

    /**
     * Nome: getNrSequenciaContratoNegocio.
     *
     * @return nrSequenciaContratoNegocio
     */
    public Long getNrSequenciaContratoNegocio() {
        return nrSequenciaContratoNegocio;
    }

    /**
     * Nome: setNrSequenciaContratoNegocio.
     *
     * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
     */
    public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
        this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
    }

    /**
     * Nome: getListaPostergarConsolidadoSolicitacao.
     *
     * @return listaPostergarConsolidadoSolicitacao
     */
    public List<ListarPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO> getListaPostergarConsolidadoSolicitacao() {
        return listaPostergarConsolidadoSolicitacao;
    }

    /**
     * Nome: setListaPostergarConsolidadoSolicitacao.
     *
     * @param listaPostergarConsolidadoSolicitacao the lista postergar consolidado solicitacao
     */
    public void setListaPostergarConsolidadoSolicitacao(
        List<ListarPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO> listaPostergarConsolidadoSolicitacao) {
        this.listaPostergarConsolidadoSolicitacao = listaPostergarConsolidadoSolicitacao;
    }

    /**
     * Nome: getListaConsultaPostergarConsolidadoSolicitacao.
     *
     * @return listaConsultaPostergarConsolidadoSolicitacao
     */
    public List<ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO> getListaConsultaPostergarConsolidadoSolicitacao() {
        return listaConsultaPostergarConsolidadoSolicitacao;
    }

    /**
     * Nome: setListaConsultaPostergarConsolidadoSolicitacao.
     *
     * @param listaConsultaPostergarConsolidadoSolicitacao the lista consulta postergar consolidado solicitacao
     */
    public void setListaConsultaPostergarConsolidadoSolicitacao(
        List<ConsultaPostergarConsolidadoSolicitacaoOcorrenciasSaidaDTO> listaConsultaPostergarConsolidadoSolicitacao) {
        this.listaConsultaPostergarConsolidadoSolicitacao = listaConsultaPostergarConsolidadoSolicitacao;
    }

    /**
     * Nome: getListaConsultaPostergarConsolidadoSolicitacaoControle.
     *
     * @return listaConsultaPostergarConsolidadoSolicitacaoControle
     */
    public List<SelectItem> getListaConsultaPostergarConsolidadoSolicitacaoControle() {
        return listaConsultaPostergarConsolidadoSolicitacaoControle;
    }

    /**
     * Nome: setListaConsultaPostergarConsolidadoSolicitacaoControle.
     *
     * @param listaConsultaPostergarConsolidadoSolicitacaoControle the lista consulta postergar consolidado solicitacao controle
     */
    public void setListaConsultaPostergarConsolidadoSolicitacaoControle(
        List<SelectItem> listaConsultaPostergarConsolidadoSolicitacaoControle) {
        this.listaConsultaPostergarConsolidadoSolicitacaoControle = listaConsultaPostergarConsolidadoSolicitacaoControle;
    }

    /**
     * Nome: isDesabilitaBotaoExcluir.
     *
     * @return desabilitaBotaoExcluir
     */
    public boolean isDesabilitaBotaoExcluir() {
        return desabilitaBotaoExcluir;
    }

    /**
     * Nome: setDesabilitaBotaoExcluir.
     *
     * @param desabilitaBotaoExcluir the desabilita botao excluir
     */
    public void setDesabilitaBotaoExcluir(boolean desabilitaBotaoExcluir) {
        this.desabilitaBotaoExcluir = desabilitaBotaoExcluir;
    }

    /**
     * Nome: isDesabilitaArgumentoDePesquisa
     *
     * @return desabilitaArgumentoDePesquisa
     */
    public boolean isDesabilitaArgumentoDePesquisa() {
        return desabilitaArgumentoDePesquisa;
    }

    /**
     * Nome: setDesabilitaArgumentoDePesquisa
     *
     * @param desabilitaArgumentoDePesquisa
     */
    public void setDesabilitaArgumentoDePesquisa(boolean desabilitaArgumentoDePesquisa) {
        this.desabilitaArgumentoDePesquisa = desabilitaArgumentoDePesquisa;
    }

    /**
     * Nome: isDesabilitaComboMotivo
     *
     * @return desabilitaComboMotivo
     */
    public boolean isDesabilitaComboMotivo() {
        return desabilitaComboMotivo;
    }

    /**
     * Nome: setDesabilitaComboMotivo
     *
     * @param desabilitaComboMotivo
     */
    public void setDesabilitaComboMotivo(boolean desabilitaComboMotivo) {
        this.desabilitaComboMotivo = desabilitaComboMotivo;
    }

}