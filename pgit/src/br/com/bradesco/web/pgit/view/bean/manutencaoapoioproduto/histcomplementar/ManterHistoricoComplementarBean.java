/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.histcomplementar
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.histcomplementar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultaMensagemEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultaMensagemSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.IdiomaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarRecursoCanalMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.IHistComplementarService;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.AlterarMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.AlterarMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharHistMsgHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharHistMsgHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.DetalharMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ExcluirMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ExcluirMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.IncluirMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.IncluirMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarHistMsgHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarHistMsgHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarMantHistoricoComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.histcomplementar.bean.ListarMantHistoricoComplementarSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * Nome: ManterHistoricoComplementarBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterHistoricoComplementarBean {
	
	/** Atributo radioArgumentoPesquisa. */
	private String radioArgumentoPesquisa;
	
	//Consultar
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo histComplementarServiceImpl. */
	private IHistComplementarService histComplementarServiceImpl;
	
	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;
	
	/** Atributo codigoHistoricoComplementarFiltro. */
	private String codigoHistoricoComplementarFiltro;
	
	/** Atributo codTipoServicoFiltro. */
	private Integer codTipoServicoFiltro;
	
	/** Atributo codModalidadeServicoFiltro. */
	private Integer codModalidadeServicoFiltro;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer,String> listaTipoServicoHash = new HashMap<Integer,String>();
	
	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo listaModalidadeServicoFiltro. */
	private List<SelectItem> listaModalidadeServicoFiltro = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoFiltroHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoFiltroHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo listaGrid. */
	private List<ListarMantHistoricoComplementarSaidaDTO> listaGrid;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();
	
	//Detalhar
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;
	
	/** Atributo dsTipoLancamento. */
	private String dsTipoLancamento;
	
	/** Atributo prefixoCredito. */
	private String prefixoCredito;
	
	/** Atributo dsIdiomaCredito. */
	private String dsIdiomaCredito;
	
	/** Atributo dsRecursoCredito. */
	private String dsRecursoCredito;
	
	/** Atributo mensagemCredito. */
	private String mensagemCredito;
	
	/** Atributo prefixoDebito. */
	private String prefixoDebito;
	
	/** Atributo dsIdiomaDebito. */
	private String dsIdiomaDebito;
	
	/** Atributo dsRecursoDebito. */
	private String dsRecursoDebito;
	
	/** Atributo mensagemDebito. */
	private String mensagemDebito;
	
	/** Atributo dsRecursoDebFormatado. */
	private String dsRecursoDebFormatado;
	
	/** Atributo dsRecursoCredFormatado. */
	private String dsRecursoCredFormatado;
	
	//Trilha
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;	
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo btoAcionadoHistorico. */
	private Boolean btoAcionadoHistorico;
	
	//Incluir e Alterar
	/** Atributo codigoHistoricoComplementar. */
	private String codigoHistoricoComplementar;
	
	/** Atributo codTipoServico. */
	private Integer codTipoServico;
	
	/** Atributo codModalidadeServico. */
	private Integer codModalidadeServico;
	
	/** Atributo lancamentoCredito. */
	private boolean lancamentoCredito;
	
	/** Atributo lancamentoDebito. */
	private boolean lancamentoDebito;
	
	/** Atributo tipoLancamento. */
	private int tipoLancamento;
	
	/** Atributo eventoMensagemCredito. */
	private String eventoMensagemCredito;
	
	/** Atributo eventoMensagemDebito. */
	private String eventoMensagemDebito;
	
	/** Atributo codRestricao. */
	private Integer codRestricao;
	
	/** Atributo idiomaCredito. */
	private Integer idiomaCredito;
	
	/** Atributo recursoCredito. */
	private Integer recursoCredito;
	
	/** Atributo idiomaDebito. */
	private Integer idiomaDebito;
	
	/** Atributo recursoDebito. */
	private Integer recursoDebito;
	
	/** Atributo inicialCentroCustoCredito. */
	private String inicialCentroCustoCredito;
	
	/** Atributo centroCustoCredito. */
	private String centroCustoCredito;
	
	/** Atributo dsCentroCustoCredito. */
	private String dsCentroCustoCredito;
	
	/** Atributo inicialCentroCustoDebito. */
	private String inicialCentroCustoDebito;
	
	/** Atributo centroCustoDebito. */
	private String centroCustoDebito;
	
	/** Atributo dsCentroCustoDebito. */
	private String dsCentroCustoDebito;
	
	/** Atributo tipoRelacionamento. */
	private int tipoRelacionamento;
	
	/** Atributo mensagemCredFormatada. */
	private String mensagemCredFormatada;
	
	/** Atributo mensagemDebFormatada. */
	private String mensagemDebFormatada;
	
	/** Atributo listaIdioma. */
	private List<SelectItem> listaIdioma = new ArrayList<SelectItem>();
	
	/** Atributo listaIdiomaHash. */
	private Map<Integer,String> listaIdiomaHash = new HashMap<Integer,String>();
	
	/** Atributo listaRecurso. */
	private List<SelectItem> listaRecurso = new ArrayList<SelectItem>();
	
	/** Atributo listaRecursoHash. */
	private Map<Integer,String> listaRecursoHash = new HashMap<Integer,String>();
	
	/** Atributo listaCentroCustoCredito. */
	private List<SelectItem> listaCentroCustoCredito = new ArrayList<SelectItem>();
	
	/** Atributo listaCentroCustoDebito. */
	private List<SelectItem> listaCentroCustoDebito = new ArrayList<SelectItem>();
	
	/** Atributo listaCentroCustoCreditoHash. */
	private Map<String,String> listaCentroCustoCreditoHash = new HashMap<String,String>();
	
	/** Atributo listaCentroCustoDebitoHash. */
	private Map<String, String> listaCentroCustoDebitoHash = new HashMap<String, String>();
	
	
	//Historico
	/** Atributo codigoHistoricoComplementarFiltro2. */
	private String codigoHistoricoComplementarFiltro2;
	
	/** Atributo itemSelecionadoLista2. */
	private Integer itemSelecionadoLista2;
	
	/** Atributo filtroDataDe. */
	private Date filtroDataDe;
	
	/** Atributo filtroDataAte. */
	private Date filtroDataAte;
	
	/** Atributo detDataManutencao. */
	private String detDataManutencao;
	
	/** Atributo detResponsavel. */
	private String detResponsavel;
	
	/** Atributo detTipoManutencao. */
	private String detTipoManutencao;
	
	/** Atributo listaGrid2. */
	private List<ListarHistMsgHistoricoComplementarSaidaDTO> listaGrid2;
	
	/** Atributo listaControle2. */
	private List<SelectItem> listaControle2 = new ArrayList<SelectItem>();
	
	private String dsSegundaLinhaExtratoCredito;
	
	private String dsSegundaLinhaExtratoDebito;
	
	//Set's e Get's
	
	/**
	 * Get: codigoHistoricoComplementarFiltro.
	 *
	 * @return codigoHistoricoComplementarFiltro
	 */
	public String getCodigoHistoricoComplementarFiltro() {
		return codigoHistoricoComplementarFiltro;
	}

	/**
	 * Set: codigoHistoricoComplementarFiltro.
	 *
	 * @param codigoHistoricoComplementarFiltro the codigo historico complementar filtro
	 */
	public void setCodigoHistoricoComplementarFiltro(String codigoHistoricoComplementarFiltro) {
		this.codigoHistoricoComplementarFiltro = codigoHistoricoComplementarFiltro;
	}

	/**
	 * Get: codTipoServicoFiltro.
	 *
	 * @return codTipoServicoFiltro
	 */
	public Integer getCodTipoServicoFiltro() {
		return codTipoServicoFiltro;
	}

	/**
	 * Set: codTipoServicoFiltro.
	 *
	 * @param codTipoServicoFiltro the cod tipo servico filtro
	 */
	public void setCodTipoServicoFiltro(Integer codTipoServicoFiltro) {
		this.codTipoServicoFiltro = codTipoServicoFiltro;
	}
	
	/**
	 * Get: codModalidadeServicoFiltro.
	 *
	 * @return codModalidadeServicoFiltro
	 */
	public Integer getCodModalidadeServicoFiltro() {
		return codModalidadeServicoFiltro;
	}

	/**
	 * Set: codModalidadeServicoFiltro.
	 *
	 * @param codModalidadeServicoFiltro the cod modalidade servico filtro
	 */
	public void setCodModalidadeServicoFiltro(Integer codModalidadeServicoFiltro) {
		this.codModalidadeServicoFiltro = codModalidadeServicoFiltro;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarMantHistoricoComplementarSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarMantHistoricoComplementarSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}
	
	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}
	
	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}
	
	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}
	
	/**
	 * Get: dsModalidadeServico.
	 *
	 * @return dsModalidadeServico
	 */
	public String getDsModalidadeServico() {
		return dsModalidadeServico;
	}

	/**
	 * Set: dsModalidadeServico.
	 *
	 * @param dsModalidadeServico the ds modalidade servico
	 */
	public void setDsModalidadeServico(String dsModalidadeServico) {
		this.dsModalidadeServico = dsModalidadeServico;
	}
	
	/**
	 * Get: dsTipoLancamento.
	 *
	 * @return dsTipoLancamento
	 */
	public String getDsTipoLancamento() {
		return dsTipoLancamento;
	}

	/**
	 * Set: dsTipoLancamento.
	 *
	 * @param dsTipoLancamento the ds tipo lancamento
	 */
	public void setDsTipoLancamento(String dsTipoLancamento) {
		this.dsTipoLancamento = dsTipoLancamento;
	}
	
	/**
	 * Get: prefixoCredito.
	 *
	 * @return prefixoCredito
	 */
	public String getPrefixoCredito() {
		setPrefixoCredito("PGIT");
		return prefixoCredito;
	}

	/**
	 * Set: prefixoCredito.
	 *
	 * @param prefixoCredito the prefixo credito
	 */
	public void setPrefixoCredito(String prefixoCredito) {
		this.prefixoCredito = prefixoCredito;
	}
	
	/**
	 * Get: idiomaCredito.
	 *
	 * @return idiomaCredito
	 */
	public Integer getIdiomaCredito() {
		return idiomaCredito;
	}

	/**
	 * Set: idiomaCredito.
	 *
	 * @param idiomaCredito the idioma credito
	 */
	public void setIdiomaCredito(Integer idiomaCredito) {
		this.idiomaCredito = idiomaCredito;
	}
	
	/**
	 * Get: recursoCredito.
	 *
	 * @return recursoCredito
	 */
	public Integer getRecursoCredito() {
		return recursoCredito;
	}

	/**
	 * Set: recursoCredito.
	 *
	 * @param recursoCredito the recurso credito
	 */
	public void setRecursoCredito(Integer recursoCredito) {
		this.recursoCredito = recursoCredito;
	}
	
	/**
	 * Get: mensagemCredito.
	 *
	 * @return mensagemCredito
	 */
	public String getMensagemCredito() {
		return mensagemCredito;
	}

	/**
	 * Set: mensagemCredito.
	 *
	 * @param mensagemCredito the mensagem credito
	 */
	public void setMensagemCredito(String mensagemCredito) {
		this.mensagemCredito = mensagemCredito;
	}
	
	/**
	 * Get: prefixoDebito.
	 *
	 * @return prefixoDebito
	 */
	public String getPrefixoDebito() {
		setPrefixoDebito("PGIT");
		return prefixoDebito;
	}

	/**
	 * Set: prefixoDebito.
	 *
	 * @param prefixoDebito the prefixo debito
	 */
	public void setPrefixoDebito(String prefixoDebito) {
		this.prefixoDebito = prefixoDebito;
	}
	
	/**
	 * Get: idiomaDebito.
	 *
	 * @return idiomaDebito
	 */
	public Integer getIdiomaDebito() {
		return idiomaDebito;
	}

	/**
	 * Set: idiomaDebito.
	 *
	 * @param idiomaDebito the idioma debito
	 */
	public void setIdiomaDebito(Integer idiomaDebito) {
		this.idiomaDebito = idiomaDebito;
	}
	
	/**
	 * Get: recursoDebito.
	 *
	 * @return recursoDebito
	 */
	public Integer getRecursoDebito() {
		return recursoDebito;
	}

	/**
	 * Set: recursoDebito.
	 *
	 * @param recursoDebito the recurso debito
	 */
	public void setRecursoDebito(Integer recursoDebito) {
		this.recursoDebito = recursoDebito;
	}
	
	/**
	 * Get: mensagemDebito.
	 *
	 * @return mensagemDebito
	 */
	public String getMensagemDebito() {
		return mensagemDebito;
	}

	/**
	 * Set: mensagemDebito.
	 *
	 * @param mensagemDebito the mensagem debito
	 */
	public void setMensagemDebito(String mensagemDebito) {
		this.mensagemDebito = mensagemDebito;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}
	
	/**
	 * Get: codigoHistoricoComplementar.
	 *
	 * @return codigoHistoricoComplementar
	 */
	public String getCodigoHistoricoComplementar() {
		return codigoHistoricoComplementar;
	}

	/**
	 * Set: codigoHistoricoComplementar.
	 *
	 * @param codigoHistoricoComplementar the codigo historico complementar
	 */
	public void setCodigoHistoricoComplementar(
			String codigoHistoricoComplementar) {
		this.codigoHistoricoComplementar = codigoHistoricoComplementar;
	}
	
	/**
	 * Get: codTipoServico.
	 *
	 * @return codTipoServico
	 */
	public Integer getCodTipoServico() {
		return codTipoServico;
	}

	/**
	 * Set: codTipoServico.
	 *
	 * @param codTipoServico the cod tipo servico
	 */
	public void setCodTipoServico(Integer codTipoServico) {
		this.codTipoServico = codTipoServico;
	}

	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}
	
	/**
	 * Get: codModalidadeServico.
	 *
	 * @return codModalidadeServico
	 */
	public Integer getCodModalidadeServico() {
		return codModalidadeServico;
	}

	/**
	 * Set: codModalidadeServico.
	 *
	 * @param codModalidadeServico the cod modalidade servico
	 */
	public void setCodModalidadeServico(Integer codModalidadeServico) {
		this.codModalidadeServico = codModalidadeServico;
	}
	
	/**
	 * Get: listaModalidadeServico.
	 *
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {
		return listaModalidadeServico;
	}

	/**
	 * Set: listaModalidadeServico.
	 *
	 * @param listaModalidadeServico the lista modalidade servico
	 */
	public void setListaModalidadeServico(List<SelectItem> listaModalidadeServico) {
		this.listaModalidadeServico = listaModalidadeServico;
	}
	
	/**
	 * Is lancamento credito.
	 *
	 * @return true, if is lancamento credito
	 */
	public boolean isLancamentoCredito() {
		return lancamentoCredito;
	}

	/**
	 * Set: lancamentoCredito.
	 *
	 * @param lancamentoCredito the lancamento credito
	 */
	public void setLancamentoCredito(boolean lancamentoCredito) {
		this.lancamentoCredito = lancamentoCredito;
	}
	
	/**
	 * Is lancamento debito.
	 *
	 * @return true, if is lancamento debito
	 */
	public boolean isLancamentoDebito() {
		return lancamentoDebito;
	}

	/**
	 * Set: lancamentoDebito.
	 *
	 * @param lancamentoDebito the lancamento debito
	 */
	public void setLancamentoDebito(boolean lancamentoDebito) {
		this.lancamentoDebito = lancamentoDebito;
	}
	
	/**
	 * Get: eventoMensagemCredito.
	 *
	 * @return eventoMensagemCredito
	 */
	public String getEventoMensagemCredito() {
		return eventoMensagemCredito;
	}

	/**
	 * Set: eventoMensagemCredito.
	 *
	 * @param eventoMensagemCredito the evento mensagem credito
	 */
	public void setEventoMensagemCredito(String eventoMensagemCredito) {
		this.eventoMensagemCredito = eventoMensagemCredito;
	}
	
	/**
	 * Get: eventoMensagemDebito.
	 *
	 * @return eventoMensagemDebito
	 */
	public String getEventoMensagemDebito() {
		return eventoMensagemDebito;
	}

	/**
	 * Set: eventoMensagemDebito.
	 *
	 * @param eventoMensagemDebito the evento mensagem debito
	 */
	public void setEventoMensagemDebito(String eventoMensagemDebito) {
		this.eventoMensagemDebito = eventoMensagemDebito;
	}
	
	/**
	 * Get: codRestricao.
	 *
	 * @return codRestricao
	 */
	public Integer getCodRestricao() {
		return codRestricao;
	}

	/**
	 * Set: codRestricao.
	 *
	 * @param codRestricao the cod restricao
	 */
	public void setCodRestricao(Integer codRestricao) {
		this.codRestricao = codRestricao;
	}
	
	/**
	 * Get: dsIdiomaCredito.
	 *
	 * @return dsIdiomaCredito
	 */
	public String getDsIdiomaCredito() {
		return dsIdiomaCredito;
	}

	/**
	 * Set: dsIdiomaCredito.
	 *
	 * @param dsIdiomaCredito the ds idioma credito
	 */
	public void setDsIdiomaCredito(String dsIdiomaCredito) {
		this.dsIdiomaCredito = dsIdiomaCredito;
	}

	/**
	 * Get: dsIdiomaDebito.
	 *
	 * @return dsIdiomaDebito
	 */
	public String getDsIdiomaDebito() {
		return dsIdiomaDebito;
	}

	/**
	 * Set: dsIdiomaDebito.
	 *
	 * @param dsIdiomaDebito the ds idioma debito
	 */
	public void setDsIdiomaDebito(String dsIdiomaDebito) {
		this.dsIdiomaDebito = dsIdiomaDebito;
	}

	/**
	 * Get: dsRecursoCredito.
	 *
	 * @return dsRecursoCredito
	 */
	public String getDsRecursoCredito() {
		return dsRecursoCredito;
	}

	/**
	 * Set: dsRecursoCredito.
	 *
	 * @param dsRecursoCredito the ds recurso credito
	 */
	public void setDsRecursoCredito(String dsRecursoCredito) {
		this.dsRecursoCredito = dsRecursoCredito;
	}

	/**
	 * Get: dsRecursoDebito.
	 *
	 * @return dsRecursoDebito
	 */
	public String getDsRecursoDebito() {
		return dsRecursoDebito;
	}

	/**
	 * Set: dsRecursoDebito.
	 *
	 * @param dsRecursoDebito the ds recurso debito
	 */
	public void setDsRecursoDebito(String dsRecursoDebito) {
		this.dsRecursoDebito = dsRecursoDebito;
	}
	
	/**
	 * Get: codigoHistoricoComplementarFiltro2.
	 *
	 * @return codigoHistoricoComplementarFiltro2
	 */
	public String getCodigoHistoricoComplementarFiltro2() {
		return codigoHistoricoComplementarFiltro2;
	}

	/**
	 * Set: codigoHistoricoComplementarFiltro2.
	 *
	 * @param codigoHistoricoComplementarFiltro2 the codigo historico complementar filtro2
	 */
	public void setCodigoHistoricoComplementarFiltro2(
			String codigoHistoricoComplementarFiltro2) {
		this.codigoHistoricoComplementarFiltro2 = codigoHistoricoComplementarFiltro2;
	}

	/**
	 * Get: listaGrid2.
	 *
	 * @return listaGrid2
	 */
	public List<ListarHistMsgHistoricoComplementarSaidaDTO> getListaGrid2() {
		return listaGrid2;
	}

	/**
	 * Set: listaGrid2.
	 *
	 * @param listaGrid2 the lista grid2
	 */
	public void setListaGrid2(List<ListarHistMsgHistoricoComplementarSaidaDTO> listaGrid2) {
		this.listaGrid2 = listaGrid2;
	}
	
	/**
	 * Get: itemSelecionadoLista2.
	 *
	 * @return itemSelecionadoLista2
	 */
	public Integer getItemSelecionadoLista2() {
		return itemSelecionadoLista2;
	}

	/**
	 * Set: itemSelecionadoLista2.
	 *
	 * @param itemSelecionadoLista2 the item selecionado lista2
	 */
	public void setItemSelecionadoLista2(Integer itemSelecionadoLista2) {
		this.itemSelecionadoLista2 = itemSelecionadoLista2;
	}
	
	/**
	 * Get: listaControle2.
	 *
	 * @return listaControle2
	 */
	public List<SelectItem> getListaControle2() {
		return listaControle2;
	}

	/**
	 * Set: listaControle2.
	 *
	 * @param listaControle2 the lista controle2
	 */
	public void setListaControle2(List<SelectItem> listaControle2) {
		this.listaControle2 = listaControle2;
	}
	
	/**
	 * Get: filtroDataAte.
	 *
	 * @return filtroDataAte
	 */
	public Date getFiltroDataAte() {
		return filtroDataAte;
	}

	/**
	 * Set: filtroDataAte.
	 *
	 * @param filtroDataAte the filtro data ate
	 */
	public void setFiltroDataAte(Date filtroDataAte) {
		this.filtroDataAte = filtroDataAte;
	}

	/**
	 * Get: filtroDataDe.
	 *
	 * @return filtroDataDe
	 */
	public Date getFiltroDataDe() {
		return filtroDataDe;
	}

	/**
	 * Set: filtroDataDe.
	 *
	 * @param filtroDataDe the filtro data de
	 */
	public void setFiltroDataDe(Date filtroDataDe) {
		this.filtroDataDe = filtroDataDe;
	}
	
	/**
	 * Get: detDataManutencao.
	 *
	 * @return detDataManutencao
	 */
	public String getDetDataManutencao() {
		return detDataManutencao;
	}

	/**
	 * Set: detDataManutencao.
	 *
	 * @param detDataManutencao the det data manutencao
	 */
	public void setDetDataManutencao(String detDataManutencao) {
		this.detDataManutencao = detDataManutencao;
	}
	
	/**
	 * Get: detResponsavel.
	 *
	 * @return detResponsavel
	 */
	public String getDetResponsavel() {
		return detResponsavel;
	}

	/**
	 * Set: detResponsavel.
	 *
	 * @param detResponsavel the det responsavel
	 */
	public void setDetResponsavel(String detResponsavel) {
		this.detResponsavel = detResponsavel;
	}
	
	/**
	 * Get: detTipoManutencao.
	 *
	 * @return detTipoManutencao
	 */
	public String getDetTipoManutencao() {
		return detTipoManutencao;
	}

	/**
	 * Set: detTipoManutencao.
	 *
	 * @param detTipoManutencao the det tipo manutencao
	 */
	public void setDetTipoManutencao(String detTipoManutencao) {
		this.detTipoManutencao = detTipoManutencao;
	}
	
	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: listaIdioma.
	 *
	 * @return listaIdioma
	 */
	public List<SelectItem> getListaIdioma() {
		return listaIdioma;
	}

	/**
	 * Set: listaIdioma.
	 *
	 * @param listaIdioma the lista idioma
	 */
	public void setListaIdioma(List<SelectItem> listaIdioma) {
		this.listaIdioma = listaIdioma;
	}

	/**
	 * Get: listaIdiomaHash.
	 *
	 * @return listaIdiomaHash
	 */
	Map<Integer, String> getListaIdiomaHash() {
		return listaIdiomaHash;
	}

	/**
	 * Set lista idioma hash.
	 *
	 * @param listaIdiomaHash the lista idioma hash
	 */
	void setListaIdiomaHash(Map<Integer, String> listaIdiomaHash) {
		this.listaIdiomaHash = listaIdiomaHash;
	}

	/**
	 * Get: listaModalidadeServicoHash.
	 *
	 * @return listaModalidadeServicoHash
	 */
	Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoHash() {
		return listaModalidadeServicoHash;
	}

	/**
	 * Set lista modalidade servico hash.
	 *
	 * @param listaModalidadeServicoHash the lista modalidade servico hash
	 */
	void setListaModalidadeServicoHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash) {
		this.listaModalidadeServicoHash = listaModalidadeServicoHash;
	}

	/**
	 * Get: listaRecurso.
	 *
	 * @return listaRecurso
	 */
	public List<SelectItem> getListaRecurso() {
		return listaRecurso;
	}

	/**
	 * Set: listaRecurso.
	 *
	 * @param listaRecurso the lista recurso
	 */
	public void setListaRecurso(List<SelectItem> listaRecurso) {
		this.listaRecurso = listaRecurso;
	}
	
	/**
	 * Get: listaRecursoHash.
	 *
	 * @return listaRecursoHash
	 */
	Map<Integer, String> getListaRecursoHash() {
		return listaRecursoHash;
	}

	/**
	 * Set lista recurso hash.
	 *
	 * @param listaRecursoHash the lista recurso hash
	 */
	void setListaRecursoHash(Map<Integer, String> listaRecursoHash) {
		this.listaRecursoHash = listaRecursoHash;
	}

	/**
	 * Get: histComplementarServiceImpl.
	 *
	 * @return histComplementarServiceImpl
	 */
	public IHistComplementarService getHistComplementarServiceImpl() {
		return histComplementarServiceImpl;
	}

	/**
	 * Set: histComplementarServiceImpl.
	 *
	 * @param histComplementarServiceImpl the hist complementar service impl
	 */
	public void setHistComplementarServiceImpl(
			IHistComplementarService histComplementarServiceImpl) {
		this.histComplementarServiceImpl = histComplementarServiceImpl;
	}
	
	/**
	 * Get: tipoLancamento.
	 *
	 * @return tipoLancamento
	 */
	public int getTipoLancamento() {
		return tipoLancamento;
	}

	/**
	 * Set: tipoLancamento.
	 *
	 * @param tipoLancamento the tipo lancamento
	 */
	public void setTipoLancamento(int tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}

	/**
	 * Get: inicialCentroCustoCredito.
	 *
	 * @return inicialCentroCustoCredito
	 */
	public String getInicialCentroCustoCredito() {
		return inicialCentroCustoCredito;
	}

	/**
	 * Set: inicialCentroCustoCredito.
	 *
	 * @param inicialCentroCustoCredito the inicial centro custo credito
	 */
	public void setInicialCentroCustoCredito(String inicialCentroCustoCredito) {
		this.inicialCentroCustoCredito = inicialCentroCustoCredito;
	}
	
	/**
	 * Get: centroCustoCredito.
	 *
	 * @return centroCustoCredito
	 */
	public String getCentroCustoCredito() {
		return centroCustoCredito;
	}

	/**
	 * Set: centroCustoCredito.
	 *
	 * @param centroCustoCredito the centro custo credito
	 */
	public void setCentroCustoCredito(String centroCustoCredito) {
		this.centroCustoCredito = centroCustoCredito;
	}
	
	/**
	 * Get: listaCentroCustoCredito.
	 *
	 * @return listaCentroCustoCredito
	 */
	public List<SelectItem> getListaCentroCustoCredito() {
		return listaCentroCustoCredito;
	}

	/**
	 * Set: listaCentroCustoCredito.
	 *
	 * @param listaCentroCustoCredito the lista centro custo credito
	 */
	public void setListaCentroCustoCredito(List<SelectItem> listaCentroCustoCredito) {
		this.listaCentroCustoCredito = listaCentroCustoCredito;
	}
	
	/**
	 * Get: dsCentroCustoCredito.
	 *
	 * @return dsCentroCustoCredito
	 */
	public String getDsCentroCustoCredito() {
		return dsCentroCustoCredito;
	}

	/**
	 * Set: dsCentroCustoCredito.
	 *
	 * @param dsCentroCustoCredito the ds centro custo credito
	 */
	public void setDsCentroCustoCredito(String dsCentroCustoCredito) {
		this.dsCentroCustoCredito = dsCentroCustoCredito;
	}
	
	/**
	 * Get: centroCustoDebito.
	 *
	 * @return centroCustoDebito
	 */
	public String getCentroCustoDebito() {
		return centroCustoDebito;
	}

	/**
	 * Set: centroCustoDebito.
	 *
	 * @param centroCustoDebito the centro custo debito
	 */
	public void setCentroCustoDebito(String centroCustoDebito) {
		this.centroCustoDebito = centroCustoDebito;
	}

	/**
	 * Get: dsCentroCustoDebito.
	 *
	 * @return dsCentroCustoDebito
	 */
	public String getDsCentroCustoDebito() {
		return dsCentroCustoDebito;
	}

	/**
	 * Set: dsCentroCustoDebito.
	 *
	 * @param dsCentroCustoDebito the ds centro custo debito
	 */
	public void setDsCentroCustoDebito(String dsCentroCustoDebito) {
		this.dsCentroCustoDebito = dsCentroCustoDebito;
	}

	/**
	 * Get: inicialCentroCustoDebito.
	 *
	 * @return inicialCentroCustoDebito
	 */
	public String getInicialCentroCustoDebito() {
		return inicialCentroCustoDebito;
	}

	/**
	 * Set: inicialCentroCustoDebito.
	 *
	 * @param inicialCentroCustoDebito the inicial centro custo debito
	 */
	public void setInicialCentroCustoDebito(String inicialCentroCustoDebito) {
		this.inicialCentroCustoDebito = inicialCentroCustoDebito;
	}
	
	/**
	 * Get: listaCentroCustoDebito.
	 *
	 * @return listaCentroCustoDebito
	 */
	public List<SelectItem> getListaCentroCustoDebito() {
		return listaCentroCustoDebito;
	}

	/**
	 * Set: listaCentroCustoDebito.
	 *
	 * @param listaCentroCustoDebito the lista centro custo debito
	 */
	public void setListaCentroCustoDebito(List<SelectItem> listaCentroCustoDebito) {
		this.listaCentroCustoDebito = listaCentroCustoDebito;
	}
	
	
	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public int getTipoRelacionamento() {
		return tipoRelacionamento;
	}

	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(int tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	/**
	 * Get: listaModalidadeServicoFiltro.
	 *
	 * @return listaModalidadeServicoFiltro
	 */
	public List<SelectItem> getListaModalidadeServicoFiltro() {
		return listaModalidadeServicoFiltro;
	}

	/**
	 * Set: listaModalidadeServicoFiltro.
	 *
	 * @param listaModalidadeServicoFiltro the lista modalidade servico filtro
	 */
	public void setListaModalidadeServicoFiltro(
			List<SelectItem> listaModalidadeServicoFiltro) {
		this.listaModalidadeServicoFiltro = listaModalidadeServicoFiltro;
	}
	
	/**
	 * Get: listaModalidadeServicoFiltroHash.
	 *
	 * @return listaModalidadeServicoFiltroHash
	 */
	Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoFiltroHash() {
		return listaModalidadeServicoFiltroHash;
	}

	/**
	 * Set lista modalidade servico filtro hash.
	 *
	 * @param listaModalidadeServicoFiltroHash the lista modalidade servico filtro hash
	 */
	void setListaModalidadeServicoFiltroHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoFiltroHash) {
		this.listaModalidadeServicoFiltroHash = listaModalidadeServicoFiltroHash;
	}

	/**
	 * Get: listaCentroCustoCreditoHash.
	 *
	 * @return listaCentroCustoCreditoHash
	 */
	Map<String, String> getListaCentroCustoCreditoHash() {
		return listaCentroCustoCreditoHash;
	}

	/**
	 * Set lista centro custo credito hash.
	 *
	 * @param listaCentroCustoCreditoHash the lista centro custo credito hash
	 */
	void setListaCentroCustoCreditoHash(
			Map<String, String> listaCentroCustoCreditoHash) {
		this.listaCentroCustoCreditoHash = listaCentroCustoCreditoHash;
	}

	
	/**
	 * Get: listaCentroCustoDebitoHash.
	 *
	 * @return listaCentroCustoDebitoHash
	 */
	public Map<String, String> getListaCentroCustoDebitoHash() {
		return listaCentroCustoDebitoHash;
	}

	/**
	 * Set lista centro custo debito hash.
	 *
	 * @param listaCentroCustoDebitoHash the lista centro custo debito hash
	 */
	public void setListaCentroCustoDebitoHash(
			Map<String, String> listaCentroCustoDebitoHash) {
		this.listaCentroCustoDebitoHash = listaCentroCustoDebitoHash;
	}
	
	
	
	//M�todos
	
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {
		limparCampos();

		setRadioArgumentoPesquisa(null);		
		return "ok";
	}

	/**
	 * Limpar campos.
	 */
	public void limparCampos(){
		
		setCodigoHistoricoComplementarFiltro("");
		setCodTipoServicoFiltro(0);
		setCodModalidadeServicoFiltro(0);
		setListaModalidadeServico(new ArrayList<SelectItem>());
		setListaModalidadeServicoHash(new HashMap<Integer, ListarModalidadeSaidaDTO>());
		setListaModalidadeServicoFiltro(new ArrayList<SelectItem>());
		setListaGrid(null);
		setItemSelecionadoLista(null);
		setBtoAcionado(false);
		setDsSegundaLinhaExtratoCredito(null);
		setDsSegundaLinhaExtratoDebito(null);
	}
	
	/**
	 * Carrega lista.
	 */
	public void carregaLista() {
		
		try{
		
			ListarMantHistoricoComplementarEntradaDTO entradaDTO = new ListarMantHistoricoComplementarEntradaDTO();
			
			entradaDTO.setCodigoHistoricoComplementar(getCodigoHistoricoComplementarFiltro()!=null && !getCodigoHistoricoComplementarFiltro().equals("") ? Integer.parseInt(getCodigoHistoricoComplementarFiltro()):0);
			entradaDTO.setTipoServico(getCodTipoServicoFiltro()!=null ? getCodTipoServicoFiltro():0);
			entradaDTO.setModalidadeServico(getCodModalidadeServicoFiltro()!=null ? getCodModalidadeServicoFiltro():0);
			
			
			setListaGrid(getHistComplementarServiceImpl().listarMantHistoricoComplementar(entradaDTO));
			setBtoAcionado(true);
			
			for (int i = 0; i < this.getListaGrid().size(); i++) {
				listaControle.add(new SelectItem(i, " "));
			}

	   } catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);

		}
		setItemSelecionadoLista(null);
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		preencheDados();
		return "DETALHAR";
	}
	
	/**
	 * Detalhar voltar.
	 *
	 * @return the string
	 */
	public String detalharVoltar(){
		setItemSelecionadoLista(null);
		return "DETALHAR_VOLTAR";
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		setCodigoHistoricoComplementar("");
		setCodTipoServico(0) ;
		setCodModalidadeServico(0) ;
		setLancamentoCredito(false);
		setLancamentoDebito(false);
		setIdiomaCredito(0) ;
		setRecursoCredito(0) ;
		setIdiomaDebito(0) ;
		setRecursoDebito(0) ;
		setCodRestricao(0);
		carregaListaIdioma();
		carregaListaRecurso();
		setInicialCentroCustoCredito("");
		setInicialCentroCustoDebito("") ;
		setCentroCustoCredito("") ;
		setCentroCustoDebito("") ;
		listaCentroCustoCredito.clear();		
		listaCentroCustoDebito.clear();
		setEventoMensagemCredito("");
		setEventoMensagemDebito("") ;
		setMensagemCredito("");
		setMensagemDebito("");
		setMensagemDebFormatada("");
		setMensagemCredFormatada("");
		setDsSegundaLinhaExtratoCredito("");
		setDsSegundaLinhaExtratoDebito("");
		
		return "INCLUIR";
	}
	
	/**
	 * Incluir voltar.
	 *
	 * @return the string
	 */
	public String incluirVoltar(){
		setItemSelecionadoLista(null);
		return "INCLUIR_VOLTAR";
	}
	
	/**
	 * Incluir avancar.
	 *
	 * @return the string
	 */
	public String incluirAvancar(){
		if (this.getObrigatoriedade().equals("T")){
			ConsultaMensagemEntradaDTO mensagemEntradaDebito = new ConsultaMensagemEntradaDTO();
			ConsultaMensagemSaidaDTO mensagemSaidaDebito = new ConsultaMensagemSaidaDTO();
			ConsultaMensagemEntradaDTO mensagemEntradaCredito = new ConsultaMensagemEntradaDTO();
			ConsultaMensagemSaidaDTO mensagemSaidaCredito = new ConsultaMensagemSaidaDTO();
			setDsTipoServico((String)listaTipoServicoHash.get(getCodTipoServico()));
			setDsModalidadeServico(listaModalidadeServicoHash.get(getCodModalidadeServico()).getDsModalidade());
			setDsCentroCustoCredito(getCentroCustoCredito() != null && !getCentroCustoCredito().equals("")?(String)listaCentroCustoCreditoHash.get(getCentroCustoCredito()):"");
			setDsCentroCustoDebito( getCentroCustoDebito() != null && !getCentroCustoDebito().equals("")?(String)listaCentroCustoDebitoHash.get(getCentroCustoDebito()):"");
			setDsIdiomaCredito(getIdiomaCredito() != null && !getIdiomaCredito().equals("")?(String)listaIdiomaHash.get(getIdiomaCredito()):"");
			setDsRecursoCredito(getRecursoCredito() != null && !getRecursoCredito().equals("")?(String)listaRecursoHash.get(getRecursoCredito()):"");
			setDsIdiomaDebito(getIdiomaDebito() != null && !getIdiomaDebito().equals("")?(String)listaIdiomaHash.get(getIdiomaDebito()):"");
			setDsRecursoDebito(getRecursoDebito() != null && !getRecursoDebito().equals("")?(String)listaRecursoHash.get(getRecursoDebito()):"");
			
			if(!getCentroCustoDebito().equals("")){
				mensagemEntradaDebito.setCdSistema(getCentroCustoDebito());
				mensagemEntradaDebito.setCdIdiomaTextoMensagem(getIdiomaDebito());
				mensagemEntradaDebito.setCdRecursoGeradorMensagem(getRecursoDebito());
				mensagemEntradaDebito.setNrEventoMensagemNegocio(Integer.parseInt(getEventoMensagemDebito()));
				mensagemSaidaDebito = comboService.consultarMensagem(mensagemEntradaDebito);
				setMensagemDebito(mensagemSaidaDebito.getDsEventoMensagemNegocio());
				
				setMensagemDebFormatada(PgitUtil.concatenarCampos(getEventoMensagemDebito(), getMensagemDebito()));
			}
			
			if(!getCentroCustoCredito().equals("")){
				mensagemEntradaCredito.setCdSistema(getCentroCustoCredito());
				mensagemEntradaCredito.setCdIdiomaTextoMensagem(getIdiomaCredito());
				mensagemEntradaCredito.setCdRecursoGeradorMensagem(getRecursoCredito());
				mensagemEntradaCredito.setNrEventoMensagemNegocio(Integer.parseInt(getEventoMensagemCredito()));
				mensagemSaidaCredito = comboService.consultarMensagem(mensagemEntradaCredito);
				setMensagemCredito(mensagemSaidaCredito.getDsEventoMensagemNegocio());
				
				setMensagemCredFormatada(PgitUtil.concatenarCampos(getEventoMensagemCredito(), getMensagemCredito()));
			}
			
			return "INCLUIR_AVANCAR";
		}
		return "";
	}
	
	/**
	 * Incluir2 voltar.
	 *
	 * @return the string
	 */
	public String incluir2Voltar(){
		return "INCLUIR2_VOLTAR";
	}
	
	/**
	 * Incluir2 confirmar.
	 *
	 * @return the string
	 */
	public String incluir2Confirmar(){		
		try {
			IncluirMantHistoricoComplementarEntradaDTO incluirMantHistoricoComplementarEntradaDTO = new IncluirMantHistoricoComplementarEntradaDTO();
			
			incluirMantHistoricoComplementarEntradaDTO.setCodigoHistoricoComplementar(getCodigoHistoricoComplementar()!=null && !getCodigoHistoricoComplementar().equals("")?Integer.parseInt(getCodigoHistoricoComplementar()):0);
			incluirMantHistoricoComplementarEntradaDTO.setPrefixoCredito(getCentroCustoCredito());
			incluirMantHistoricoComplementarEntradaDTO.setEventoMensagemCredito(getEventoMensagemCredito()!=null && !getEventoMensagemCredito().equals("")?Integer.parseInt(getEventoMensagemCredito()):0);
			incluirMantHistoricoComplementarEntradaDTO.setRecursoCredito(getRecursoCredito());
			incluirMantHistoricoComplementarEntradaDTO.setIdiomaCredito(getIdiomaCredito());
			incluirMantHistoricoComplementarEntradaDTO.setPrefixoDebito(getCentroCustoDebito());
			incluirMantHistoricoComplementarEntradaDTO.setEventoMensagemDebito(getEventoMensagemDebito()!=null && !getEventoMensagemDebito().equals("")?Integer.parseInt(getEventoMensagemDebito()):0);
			incluirMantHistoricoComplementarEntradaDTO.setRecursoDebito(getRecursoDebito());
			incluirMantHistoricoComplementarEntradaDTO.setIdiomaDebito(getIdiomaDebito());
			incluirMantHistoricoComplementarEntradaDTO.setRestricao(getCodRestricao());
			
			if(isLancamentoCredito() && !isLancamentoDebito()){
				setTipoLancamento(2);
			}else if(isLancamentoDebito()&& !isLancamentoCredito() ){
				setTipoLancamento(1);
			}else if(isLancamentoCredito() && isLancamentoDebito() ){
				setTipoLancamento(3);
			}
			
			incluirMantHistoricoComplementarEntradaDTO.setTipoLancamento(getTipoLancamento());
			incluirMantHistoricoComplementarEntradaDTO.setTipoServico(getCodTipoServico());
			incluirMantHistoricoComplementarEntradaDTO.setModalidadeServico(getCodModalidadeServico());
			incluirMantHistoricoComplementarEntradaDTO.setTipoRelacionamento(listaModalidadeServicoHash.get(getCodModalidadeServico()).getCdRelacionamentoProduto());
			incluirMantHistoricoComplementarEntradaDTO.setDsSegundaLinhaExtratoCredito(getDsSegundaLinhaExtratoCredito());
			incluirMantHistoricoComplementarEntradaDTO.setDsSegundaLinhaExtratoCredito(getDsSegundaLinhaExtratoCredito());
			
			IncluirMantHistoricoComplementarSaidaDTO incluirMantHistoricoComplementarSaidaDTO = 
				getHistComplementarServiceImpl().incluirMantHistoricoComplementar(incluirMantHistoricoComplementarEntradaDTO);

			BradescoFacesUtils.addInfoModalMessage("(" +incluirMantHistoricoComplementarSaidaDTO.getCodMensagem() + ") " + incluirMantHistoricoComplementarSaidaDTO.getMensagem(), "conManterHistoricoComplementar", BradescoViewExceptionActionType.ACTION, false);
			
			carregaLista();			
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar(){
		preencheDados();
		carregaListaIdioma();
		carregaListaRecurso();
		return "ALTERAR";
	}
	
	/**
	 * Alterar voltar.
	 *
	 * @return the string
	 */
	public String alterarVoltar(){
		setItemSelecionadoLista(null);
		return "ALTERAR_VOLTAR";
	}
	
	/**
	 * Alterar avancar.
	 *
	 * @return the string
	 */
	public String alterarAvancar(){

		setMensagemDebFormatada("");
		setMensagemCredFormatada("");
		
		if (this.getObrigatoriedade().equals("T")){
			ConsultaMensagemEntradaDTO mensagemEntradaDebito = new ConsultaMensagemEntradaDTO();
			ConsultaMensagemSaidaDTO mensagemSaidaDebito = new ConsultaMensagemSaidaDTO();
			ConsultaMensagemEntradaDTO mensagemEntradaCredito = new ConsultaMensagemEntradaDTO();
			ConsultaMensagemSaidaDTO mensagemSaidaCredito = new ConsultaMensagemSaidaDTO();
			setDsCentroCustoCredito(getCentroCustoCredito() != null && !getCentroCustoCredito().equals("")?(String)listaCentroCustoCreditoHash.get(getCentroCustoCredito()):"");
			setDsCentroCustoDebito( getCentroCustoDebito() != null && !getCentroCustoDebito().equals("")?(String)listaCentroCustoDebitoHash.get(getCentroCustoDebito()):"");
			setDsIdiomaCredito(getIdiomaCredito() != null && !getIdiomaCredito().equals("")?(String)listaIdiomaHash.get(getIdiomaCredito()):"");
			setDsRecursoCredito(getRecursoCredito() != null && !getRecursoCredito().equals("")?(String)listaRecursoHash.get(getRecursoCredito()):"");
			setDsIdiomaDebito(getIdiomaDebito() != null && !getIdiomaDebito().equals("")?(String)listaIdiomaHash.get(getIdiomaDebito()):"");
			setDsRecursoDebito(getRecursoDebito() != null && !getRecursoDebito().equals("")?(String)listaRecursoHash.get(getRecursoDebito()):"");
			
			if(!getCentroCustoDebito().equals("")){
				mensagemEntradaDebito.setCdSistema(getCentroCustoDebito());
				mensagemEntradaDebito.setCdIdiomaTextoMensagem(getIdiomaDebito());
				mensagemEntradaDebito.setCdRecursoGeradorMensagem(getRecursoDebito());
				mensagemEntradaDebito.setNrEventoMensagemNegocio(Integer.parseInt(getEventoMensagemDebito()));
				mensagemSaidaDebito = comboService.consultarMensagem(mensagemEntradaDebito);
				setMensagemDebito(mensagemSaidaDebito.getDsEventoMensagemNegocio());
				
				setMensagemDebFormatada(PgitUtil.concatenarCampos(getEventoMensagemDebito(), getMensagemDebito()));
			}
			
			if(!getCentroCustoCredito().equals("")){
				mensagemEntradaCredito.setCdSistema(getCentroCustoCredito());
				mensagemEntradaCredito.setCdIdiomaTextoMensagem(getIdiomaCredito());
				mensagemEntradaCredito.setCdRecursoGeradorMensagem(getRecursoCredito());
				mensagemEntradaCredito.setNrEventoMensagemNegocio(Integer.parseInt(getEventoMensagemCredito()));
				mensagemSaidaCredito = comboService.consultarMensagem(mensagemEntradaCredito);
				setMensagemCredito(mensagemSaidaCredito.getDsEventoMensagemNegocio());
				
				setMensagemCredFormatada(PgitUtil.concatenarCampos(getEventoMensagemCredito(), getMensagemCredito()));
			}
			if(isLancamentoDebito()  && !isLancamentoCredito() ){
				dsIdiomaCredito = ("");
				dsRecursoCredito = ("");
				mensagemCredito = ("");
			}else if(!isLancamentoDebito()  && isLancamentoCredito() ){
				dsIdiomaDebito = ("");
				dsRecursoDebito = ("");
				mensagemDebito = ("");
			}
			
			setDsRecursoCredFormatado(PgitUtil.concatenarCampos(getRecursoCredito(), getDsRecursoCredito()));
			setDsRecursoDebFormatado(PgitUtil.concatenarCampos(getRecursoDebito(), getDsRecursoDebito()));
			
			return "ALTERAR_AVANCAR";
		}
		return "";
	}
	
	/**
	 * Alterar2 voltar.
	 *
	 * @return the string
	 */
	public String alterar2Voltar(){
		return "ALTERAR2_VOLTAR";
	}
	
	/**
	 * Alterar2 confirmar.
	 *
	 * @return the string
	 */
	public String alterar2Confirmar(){
		try {
			AlterarMantHistoricoComplementarEntradaDTO entradaDTO = new AlterarMantHistoricoComplementarEntradaDTO();
			
			entradaDTO.setCodigoHistoricoComplementar(getCodigoHistoricoComplementar()!=null && !getCodigoHistoricoComplementar().equals("")?Integer.parseInt(getCodigoHistoricoComplementar()):0);
			entradaDTO.setPrefixoCredito(getCentroCustoCredito());
			entradaDTO.setEventoMensagemCredito(getEventoMensagemCredito()!=null && !getEventoMensagemCredito().equals("")?Integer.parseInt(getEventoMensagemCredito()):0);
			entradaDTO.setRecursoCredito(getRecursoCredito());
			entradaDTO.setIdiomaCredito(getIdiomaCredito());
			entradaDTO.setPrefixoDebito(getCentroCustoDebito());
			entradaDTO.setEventoMensagemDebito(getEventoMensagemDebito()!=null && !getEventoMensagemDebito().equals("")?Integer.parseInt(getEventoMensagemDebito()):0);
			entradaDTO.setRecursoDebito(getRecursoDebito());
			entradaDTO.setIdiomaDebito(getIdiomaDebito());
			entradaDTO.setRestricao(getCodRestricao());
			
			if(isLancamentoCredito() && !isLancamentoDebito()){
				setTipoLancamento(2);
				
			}else if(isLancamentoDebito() && !isLancamentoCredito() ){
				setTipoLancamento(1);
				
			}else if(isLancamentoCredito() && isLancamentoDebito() ){
				setTipoLancamento(3);
				
			}
			entradaDTO.setTipoLancamento(getTipoLancamento());
			entradaDTO.setTipoServico(getCodTipoServico());
			entradaDTO.setModalidadeServico(getCodModalidadeServico());
			entradaDTO.setTipoRelacionamento(getTipoRelacionamento());
			entradaDTO.setDsSegundaLinhaExtratoCredito(getDsSegundaLinhaExtratoCredito());
			entradaDTO.setDsSegundaLinhaExtratoDebito(getDsSegundaLinhaExtratoDebito());
			
			AlterarMantHistoricoComplementarSaidaDTO alterarMantHistoricoComplementarSaidaDTO = getHistComplementarServiceImpl().alterarMantHistoricoComplementar(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage("(" +alterarMantHistoricoComplementarSaidaDTO.getCodMensagem() + ") " + alterarMantHistoricoComplementarSaidaDTO.getMensagem(), "conManterHistoricoComplementar", BradescoViewExceptionActionType.ACTION, false);
			
			carregaLista();			
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		preencheDados();
		return "EXCLUIR";
	}
	
	/**
	 * Excluir voltar.
	 *
	 * @return the string
	 */
	public String excluirVoltar(){
		setItemSelecionadoLista(null);
		return "EXCLUIR_VOLTAR";
	}
	
	/**
	 * Excluir confirmar.
	 *
	 * @return the string
	 */
	public String excluirConfirmar(){
		try {
			ExcluirMantHistoricoComplementarEntradaDTO entradaDTO = new ExcluirMantHistoricoComplementarEntradaDTO();
		
			entradaDTO.setCodigoHistoricoComplementar(getCodigoHistoricoComplementar()!=null && !getCodigoHistoricoComplementar().equals("")?Integer.parseInt(getCodigoHistoricoComplementar()):0);
			entradaDTO.setTipoServico(getCodTipoServico());
			entradaDTO.setModalidadeServico(getCodModalidadeServico());
			entradaDTO.setTipoRelacionamento(getTipoRelacionamento());
			
			ExcluirMantHistoricoComplementarSaidaDTO excluirMantHistoricoComplementarSaidaDTO = getHistComplementarServiceImpl().excluirMantHistoricoComplementar(entradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +excluirMantHistoricoComplementarSaidaDTO.getCodMensagem() + ") " + excluirMantHistoricoComplementarSaidaDTO.getMensagem(), "conManterHistoricoComplementar", BradescoViewExceptionActionType.ACTION, false);
			
			carregaLista();			
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Historico.
	 *
	 * @return the string
	 */
	public String historico(){
		this.codigoHistoricoComplementarFiltro2 = getItemSelecionadoLista()==null? "" : String.valueOf(getListaGrid().get(getItemSelecionadoLista()).getCodLancamentoPersonalizado());
		setItemSelecionadoLista2(null);
		setListaGrid2(null);
		this.setFiltroDataAte(new Date());
		this.setFiltroDataDe(new Date());
		setBtoAcionadoHistorico(false);
		return "HISTORICO";
	}
	
	/**
	 * Historico voltar.
	 *
	 * @return the string
	 */
	public String historicoVoltar(){
		setItemSelecionadoLista(null);
		return "HISTORICO_VOLTAR";
	}

	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
		
		limparDados();
		try{
			listarTipoServico();
		}catch(PdcAdapterFunctionalException e){
			listaTipoServico = new ArrayList<SelectItem>();
		}
		setBtoAcionado(false);
	}
	
	/**
	 * Limpar campos historico.
	 */
	public void limparCamposHistorico(){
		this.codigoHistoricoComplementarFiltro2 = "";
		this.setFiltroDataAte(new Date());
		this.setFiltroDataDe(new Date());
		setListaGrid2(null);
		setItemSelecionadoLista2(null);
		setBtoAcionadoHistorico(false);
	}
	
	/**
	 * Carrega lista historico.
	 */
	public void carregaListaHistorico(){
		
		try{
			
			ListarHistMsgHistoricoComplementarEntradaDTO entradaDTO = new ListarHistMsgHistoricoComplementarEntradaDTO();
			
			entradaDTO.setCodigoHistoricoComplementar(getCodigoHistoricoComplementarFiltro2() !=null   && !getCodigoHistoricoComplementarFiltro2().equals("")?Integer.valueOf(getCodigoHistoricoComplementarFiltro2()):0);
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			entradaDTO.setPeriodoFinal(getFiltroDataAte() != null && !getFiltroDataAte().equals("") ? df.format(getFiltroDataAte()): "");
			entradaDTO.setPeriodoInicial(getFiltroDataDe() != null && ! getFiltroDataDe().equals("") ? df.format(getFiltroDataDe()):"");
			
			setListaGrid2(getHistComplementarServiceImpl().listarHistMsgHistoricoComplementar(entradaDTO));
			setBtoAcionadoHistorico(true);
			this.listaControle2 = new ArrayList<SelectItem>();
			for(int i = 0; i < getListaGrid2().size(); i++){
				this.listaControle2.add(new SelectItem(i, ""));
			}
			
			setItemSelecionadoLista2(null);
			
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid2(null);

		}
		
	}
	
	/**
	 * Historico detalhar.
	 *
	 * @return the string
	 */
	public String historicoDetalhar(){
		preencheDadosHistorico();
		return "HISTORICO_DETALHAR";
	}
	
	/**
	 * Historico2 voltar.
	 *
	 * @return the string
	 */
	public String historico2Voltar(){
		setItemSelecionadoLista2(null);
		return "HISTORICO2_VOLTAR";
	}
	
	/**
	 * Listar tipo servico.
	 */
	private void listarTipoServico() {
		
		this.listaTipoServico = new ArrayList<SelectItem>();
		
		List<ListarServicosSaidaDTO> saidaDTO = new ArrayList<ListarServicosSaidaDTO>();
		
		saidaDTO = comboService.listarTipoServicos(1);
		
		listaTipoServicoHash.clear();
		for(ListarServicosSaidaDTO combo : saidaDTO){
			listaTipoServicoHash.put(combo.getCdServico(),combo.getDsServico());
			this.listaTipoServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
		}
	}
	
	/**
	 * Listar modalidade servico filtro.
	 */
	public void listarModalidadeServicoFiltro(){
		
		listaModalidadeServicoFiltroHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
		listaModalidadeServicoFiltro = new ArrayList<SelectItem>();
		
		if(getCodTipoServicoFiltro()!=null && getCodTipoServicoFiltro() !=0){
			List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
			ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
			
			listarModalidadeEntradaDTO.setCdServico(getCodTipoServicoFiltro());
		
			listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
			
			listaModalidadeServicoFiltroHash.clear();
			
			for(ListarModalidadeSaidaDTO combo : listaModalidades){				
				this.listaModalidadeServicoFiltro.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
				listaModalidadeServicoFiltroHash.put(combo.getCdModalidade(),combo);
				
			}
		}else{
			listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
			listaModalidadeServico.clear();
			setCodModalidadeServicoFiltro(0);
			setCodTipoServicoFiltro(0);
		}
	}
	
	/**
	 * Listar modalidade servico.
	 */
	public void listarModalidadeServico(){
		
		listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
		listaModalidadeServico = new ArrayList<SelectItem>();
		
		if(getCodTipoServico()!=null && getCodTipoServico() !=0){
			List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
			ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
			
			listarModalidadeEntradaDTO.setCdServico(getCodTipoServico());
		
			listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
			
			listaModalidadeServicoHash.clear();
			
			for(ListarModalidadeSaidaDTO combo : listaModalidades){				
				this.listaModalidadeServico.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
				listaModalidadeServicoHash.put(combo.getCdModalidade(),combo);
				
			}
		}else{
			listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
			listaModalidadeServico.clear();
			setCodModalidadeServico(0);
			setCodTipoServico(0);
		}
	}
	
	/**
	 * Carrega lista recurso.
	 */
	public void carregaListaRecurso(){
		
		this.listaRecurso = new ArrayList<SelectItem>();
		List<ListarRecursoCanalMsgSaidaDTO> listaRecursoCanalMsgSaidaDTO = new ArrayList<ListarRecursoCanalMsgSaidaDTO>();
		
		listaRecursoCanalMsgSaidaDTO = comboService.listarRecursoCanalMsg();
		
		listaRecursoHash.clear();
		
		for(ListarRecursoCanalMsgSaidaDTO combo : listaRecursoCanalMsgSaidaDTO){
			listaRecursoHash.put(combo.getCdTipoCanal(),combo.getDsTipoCanal());
			this.listaRecurso.add(new SelectItem(combo.getCdTipoCanal(),combo.getDsTipoCanal()));
		}
	}	
	
	/**
	 * Carrega lista idioma.
	 */
	public void carregaListaIdioma(){
		
		this.listaIdioma = new ArrayList<SelectItem>();
		List<IdiomaSaidaDTO> listaIdiomaSaida = new ArrayList<IdiomaSaidaDTO>();
		
		listaIdiomaSaida = comboService.listarIdioma();
			
		listaIdiomaHash.clear();
			
		for(IdiomaSaidaDTO combo : listaIdiomaSaida){
			listaIdiomaHash.put(combo.getCdIdioma(),combo.getDsIdioma());
			this.listaIdioma.add(new SelectItem(combo.getCdIdioma(),combo.getDsIdioma()));
		}
	}	
	
	/**
	 * Carrega combos.
	 */
	public void carregaCombos(){
		if(!isLancamentoCredito() ){
			setIdiomaCredito(0);
			setRecursoCredito(0);
			setEventoMensagemCredito("");
			setInicialCentroCustoCredito("");
			setCentroCustoCredito("");
			listaCentroCustoCredito.clear();
		}
		if(!isLancamentoDebito() ){
			setIdiomaDebito(0);
			setRecursoDebito(0);
			setEventoMensagemDebito("");
			setInicialCentroCustoDebito("");
			setCentroCustoDebito("");
			listaCentroCustoDebito.clear();
		}
	}
	
	
	
	/**
	 * Buscar centro custo credito.
	 */
	public void buscarCentroCustoCredito(){
		
		if (getInicialCentroCustoCredito()!=null && !getInicialCentroCustoCredito().equals("")){
			
			this.listaCentroCustoCredito = new ArrayList<SelectItem>();
			
			List<CentroCustoSaidaDTO> listaCentroCustoSaida = new ArrayList<CentroCustoSaidaDTO>();
			CentroCustoEntradaDTO centroCustoEntradaDTO = new CentroCustoEntradaDTO();
			
			centroCustoEntradaDTO.setCdSistema(getInicialCentroCustoCredito());
			
			listaCentroCustoSaida = comboService.listarCentroCusto(centroCustoEntradaDTO);
			listaCentroCustoCreditoHash.clear();
			
			for(CentroCustoSaidaDTO combo : listaCentroCustoSaida){	
				listaCentroCustoCreditoHash.put(combo.getCdCentroCusto(),combo.getCdCentroCusto() + "-" + combo.getDsCentroCusto());
				this.listaCentroCustoCredito.add(new SelectItem(combo.getCdCentroCusto(),combo.getCdCentroCusto() + "-" + combo.getDsCentroCusto()));
			}
			}else{			
			this.listaCentroCustoCredito = new ArrayList<SelectItem>();
			listaCentroCustoCreditoHash.clear();
		}		
			
	}
	
	/**
	 * Buscar centro custo debito.
	 */
	public void buscarCentroCustoDebito(){
		
		if (getInicialCentroCustoDebito()!=null && !getInicialCentroCustoDebito().equals("")){
			
			this.listaCentroCustoDebito = new ArrayList<SelectItem>();
			
			List<CentroCustoSaidaDTO> listaCentroCustoSaida = new ArrayList<CentroCustoSaidaDTO>();
			CentroCustoEntradaDTO centroCustoEntradaDTO = new CentroCustoEntradaDTO();
			
			centroCustoEntradaDTO.setCdSistema(getInicialCentroCustoDebito());
			
			listaCentroCustoSaida = comboService.listarCentroCusto(centroCustoEntradaDTO);
			listaCentroCustoDebitoHash.clear();
			
			for(CentroCustoSaidaDTO combo : listaCentroCustoSaida){	
				listaCentroCustoDebitoHash.put(combo.getCdCentroCusto(),combo.getCdCentroCusto() + "-" + combo.getDsCentroCusto());
				this.listaCentroCustoDebito.add(new SelectItem(combo.getCdCentroCusto(),combo.getCdCentroCusto() + "-" + combo.getDsCentroCusto()));
			}
			}else{			
			this.listaCentroCustoDebito = new ArrayList<SelectItem>();
		}		
			
	}
	
	/**
	 * Preenche dados.
	 */
	public void preencheDados(){
		
		ListarMantHistoricoComplementarSaidaDTO listarMantHistoricoComplementarSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
		DetalharMantHistoricoComplementarEntradaDTO entradaDTO = new DetalharMantHistoricoComplementarEntradaDTO();
		
		entradaDTO.setCodigoHistoricoComplementar(listarMantHistoricoComplementarSaidaDTO.getCodLancamentoPersonalizado());
		entradaDTO.setTipoServico(listarMantHistoricoComplementarSaidaDTO.getCdTipoServico());
		entradaDTO.setModalidadeServico(listarMantHistoricoComplementarSaidaDTO.getCdModalidadeServico());
		entradaDTO.setTipoRelacionamento(listarMantHistoricoComplementarSaidaDTO.getTipoRelacionamento());
		
		DetalharMantHistoricoComplementarSaidaDTO saidaDTO = getHistComplementarServiceImpl().detalharMantHistoricoComplementar(entradaDTO);
		setCodigoHistoricoComplementar(Integer.toString(saidaDTO.getHistoricoComplementar()));
		setCodRestricao(saidaDTO.getRestricao());
		setTipoLancamento(saidaDTO.getTipoLancamento());
		setCodTipoServico(saidaDTO.getCdTipoServico());
		setDsTipoServico(saidaDTO.getDsTipoServico());
		setCodModalidadeServico(saidaDTO.getCdModalidadeServico());
		setDsModalidadeServico(saidaDTO.getDsModalidadeServico());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencao());
		setComplementoInclusao(saidaDTO.getComplementoInclusao()==null || saidaDTO.getComplementoInclusao().equals("0")? "" : saidaDTO.getComplementoInclusao());
		setComplementoManutencao(saidaDTO.getComplementoManutencao()== null || saidaDTO.getComplementoManutencao().equals("0")? "" : saidaDTO.getComplementoManutencao());
		setTipoCanalInclusao(saidaDTO.getCdTipoCanalInclusao()==0? "" : saidaDTO.getCdTipoCanalInclusao() + " - " + saidaDTO.getDsTipoCanalInclusao());
		setTipoCanalManutencao(saidaDTO.getCdTipoCanalManutencao()==0? "" : saidaDTO.getCdTipoCanalManutencao() + " - " + saidaDTO.getDsTipoCanalManutencao());
		setDataHoraInclusao(saidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencao());
		setTipoRelacionamento(saidaDTO.getCdRelacionamento());
		
		
		setInicialCentroCustoDebito("");
		setCentroCustoDebito("");
		setEventoMensagemDebito("");
		setMensagemDebito("");
		setRecursoDebito(saidaDTO.getCdRecursoDebito());
		setDsRecursoDebito(saidaDTO.getDsRecursoDebito());
		setIdiomaDebito(0);
		setDsIdiomaDebito("");
		setCentroCustoCredito("");
		setInicialCentroCustoCredito("");
		setCentroCustoCredito("");
		setEventoMensagemCredito("");
		setMensagemCredito("");
		setRecursoCredito(saidaDTO.getCdRecursoCredito());
		setDsRecursoCredito(saidaDTO.getDsRecursoCredito());
		setIdiomaCredito(0);
		setDsIdiomaCredito("");
		setDsSegundaLinhaExtratoCredito(saidaDTO.getDsSegundaLinhaExtratoCredito());
		setDsSegundaLinhaExtratoDebito(saidaDTO.getDsSegundaLinhaExtratoDebito());
		
		if(getTipoLancamento() == 1){
			setLancamentoDebito(true);
			setLancamentoCredito(false);
			setInicialCentroCustoDebito(saidaDTO.getPrefixoDebito());
			buscarCentroCustoDebito();
			setCentroCustoDebito(saidaDTO.getPrefixoDebito());
			setEventoMensagemDebito(Integer.toString(saidaDTO.getCdMensagemDebito()));
			setMensagemDebito(saidaDTO.getCdMensagemDebito()==0? "" : saidaDTO.getCdMensagemDebito() + " - " + saidaDTO.getDsMensagemDebito());
			setRecursoDebito(saidaDTO.getCdRecursoDebito());
			setDsRecursoDebito(saidaDTO.getDsRecursoDebito());
			setIdiomaDebito(saidaDTO.getCdIdiomaDebito());
			setDsIdiomaDebito(saidaDTO.getDsIdiomaDebito());
		}else if(getTipoLancamento() == 2){
			setLancamentoDebito(false);
			setLancamentoCredito(true);
			setCentroCustoCredito(saidaDTO.getPrefixoCredito());
			setInicialCentroCustoCredito(saidaDTO.getPrefixoCredito());
			buscarCentroCustoCredito();
			setCentroCustoCredito(saidaDTO.getPrefixoCredito());
			setEventoMensagemCredito(Integer.toString(saidaDTO.getCdMensagemCredito()));
			setMensagemCredito(saidaDTO.getCdMensagemCredito()==0? "" : saidaDTO.getCdMensagemCredito() + " - " + saidaDTO.getDsMensagemCredito());
			setRecursoCredito(saidaDTO.getCdRecursoCredito());
			setDsRecursoCredito(saidaDTO.getDsRecursoCredito());
			setIdiomaCredito(saidaDTO.getCdIdiomaCredito());
			setDsIdiomaCredito(saidaDTO.getDsIdiomaCredito());
		}else if(getTipoLancamento() == 3){
			setLancamentoDebito(true);
			setLancamentoCredito(true);
			setInicialCentroCustoDebito(saidaDTO.getPrefixoDebito());
			buscarCentroCustoDebito();
			setCentroCustoDebito(saidaDTO.getPrefixoDebito());
			setEventoMensagemDebito(Integer.toString(saidaDTO.getCdMensagemDebito()));
			setMensagemDebito(saidaDTO.getCdMensagemDebito()==0? "" : saidaDTO.getCdMensagemDebito() + " - " + saidaDTO.getDsMensagemDebito());
			setRecursoDebito(saidaDTO.getCdRecursoDebito());
			setDsRecursoDebito(saidaDTO.getDsRecursoDebito());
			setIdiomaDebito(saidaDTO.getCdIdiomaDebito());
			setDsIdiomaDebito(saidaDTO.getDsIdiomaDebito());
			setCentroCustoCredito(saidaDTO.getPrefixoCredito());
			setInicialCentroCustoCredito(saidaDTO.getPrefixoCredito());
			buscarCentroCustoCredito();
			setCentroCustoCredito(saidaDTO.getPrefixoCredito());
			setEventoMensagemCredito(Integer.toString(saidaDTO.getCdMensagemCredito()));
			setMensagemCredito(saidaDTO.getCdMensagemCredito()==0? "" : saidaDTO.getCdMensagemCredito() + " - " + saidaDTO.getDsMensagemCredito());
			setRecursoCredito(saidaDTO.getCdRecursoCredito());
			setDsRecursoCredito(saidaDTO.getDsRecursoCredito());
			setIdiomaCredito(saidaDTO.getCdIdiomaCredito());
			setDsIdiomaCredito(saidaDTO.getDsIdiomaCredito());
		}
		
		setDsRecursoCredFormatado(PgitUtil.concatenarCampos(getRecursoCredito(), getDsRecursoCredito()));
		setDsRecursoDebFormatado(PgitUtil.concatenarCampos(getRecursoDebito(), getDsRecursoDebito()));
	}
	
	/**
	 * Preenche dados historico.
	 */
	private void preencheDadosHistorico(){
		
		ListarHistMsgHistoricoComplementarSaidaDTO listarHistMsgHistoricoComplementarSaidaDTO = getListaGrid2().get(getItemSelecionadoLista2());
		
		DetalharHistMsgHistoricoComplementarEntradaDTO entradaDTO = new DetalharHistMsgHistoricoComplementarEntradaDTO();
		
		entradaDTO.setCodigoHistoricoComplementar(listarHistMsgHistoricoComplementarSaidaDTO.getCodigoHistoricoComplementar());
		entradaDTO.setDataManutencao(listarHistMsgHistoricoComplementarSaidaDTO.getDataManutencao());
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		entradaDTO.setPeriodoFinal(df.format(getFiltroDataAte()));
		entradaDTO.setPeriodoInicial(df.format(getFiltroDataDe()));
		
		
		DetalharHistMsgHistoricoComplementarSaidaDTO saidaDTO = getHistComplementarServiceImpl().detalharHistMsgHistoricoComplementar(entradaDTO);
		setCodigoHistoricoComplementar(Integer.toString(saidaDTO.getCodigoHistoricoComplementar()));
		setDsTipoServico(saidaDTO.getDsTipoServico());
		setDsModalidadeServico(saidaDTO.getDsModalidadeServico());
		setTipoLancamento(saidaDTO.getTipoLancamento());
		
		setCentroCustoDebito("");
		setDsIdiomaDebito("");
		setRecursoDebito(saidaDTO.getCdRecursoDebito());
		setDsRecursoDebito(saidaDTO.getDsRecursoDebito());
		setMensagemDebito("");
		setCentroCustoCredito("");
		setDsIdiomaCredito("");
		setRecursoCredito(saidaDTO.getCdRecursoCredito());
		setDsRecursoCredito(saidaDTO.getDsRecursoCredito());
		setMensagemCredito("");
		setDsSegundaLinhaExtratoCredito(saidaDTO.getDsSegundaLinhaExtratoCredito());
		setDsSegundaLinhaExtratoDebito(saidaDTO.getDsSegundaLinhaExtratoDebito());
		
		if(getTipoLancamento() == 1){
			setCentroCustoDebito(saidaDTO.getPrefixoDebito());
			setDsIdiomaDebito(saidaDTO.getDsIdiomaDebito());
			setDsRecursoDebito(saidaDTO.getDsRecursoDebito());
			setMensagemDebito(saidaDTO.getCdMensagemDebito()==0 ? "" : saidaDTO.getCdMensagemDebito() + " - " + saidaDTO.getDsMensagemDebito());
		}else if(getTipoLancamento() == 2){
			setCentroCustoCredito(saidaDTO.getPrefixoCredito());
			setDsIdiomaCredito(saidaDTO.getDsIdiomaCredito());
			setDsRecursoCredito(saidaDTO.getDsRecursoCredito());
			setMensagemCredito(saidaDTO.getCdMensagemCredito()==0 ? "" : saidaDTO.getCdMensagemCredito() + " - " + saidaDTO.getDsMensagemCredito());
		}else if(getTipoLancamento() == 3){
			setCentroCustoDebito(saidaDTO.getPrefixoDebito());
			setDsIdiomaDebito(saidaDTO.getDsIdiomaDebito());
			setDsRecursoDebito(saidaDTO.getDsRecursoDebito());
			setMensagemDebito(saidaDTO.getCdMensagemDebito()==0 ? "" : saidaDTO.getCdMensagemDebito() + " - " + saidaDTO.getDsMensagemDebito());
			setCentroCustoCredito(saidaDTO.getPrefixoCredito());
			setDsIdiomaCredito(saidaDTO.getDsIdiomaCredito());
			setDsRecursoCredito(saidaDTO.getDsRecursoCredito());
			setMensagemCredito(saidaDTO.getCdMensagemCredito()==0 ? "" : saidaDTO.getCdMensagemCredito() + " - " + saidaDTO.getDsMensagemCredito());
		}
		setCodRestricao(saidaDTO.getRestricao());
		setDetDataManutencao(listarHistMsgHistoricoComplementarSaidaDTO.getDataManutencaoDesc());
		setDetResponsavel(listarHistMsgHistoricoComplementarSaidaDTO.getResponsavel());
		setDetTipoManutencao(Integer.toString(listarHistMsgHistoricoComplementarSaidaDTO.getTipoManutencao()));
		setUsuarioInclusao(saidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencao());
		setComplementoInclusao(saidaDTO.getComplementoInclusao().equals("0")? "" : saidaDTO.getComplementoInclusao());
		setComplementoManutencao(saidaDTO.getComplementoManutencao().equals("0")? "" : saidaDTO.getComplementoManutencao());
		setTipoCanalInclusao(saidaDTO.getCdTipoCanalInclusao()==0? "" : saidaDTO.getCdTipoCanalInclusao() + " - " +  saidaDTO.getDsTipoCanalInclusao()); 
		setTipoCanalManutencao(saidaDTO.getCdTipoCanalManutencao()==0? "" : saidaDTO.getCdTipoCanalManutencao() + " - " + saidaDTO.getDsTipoCanalManutencao()); 
		setDataHoraInclusao(saidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencao());
		
		setDsRecursoCredFormatado(PgitUtil.concatenarCampos(getRecursoCredito(), getDsRecursoCredito()));
		setDsRecursoDebFormatado(PgitUtil.concatenarCampos(getRecursoDebito(), getDsRecursoDebito()));
		
	}
	


	/**
	 * Pesquisar.
	 *
	 * @return the string
	 */
	public String pesquisar(){
		return "";
	}
	
	/**
	 * Pesquisar historico.
	 *
	 * @return the string
	 */
	public String pesquisarHistorico(){
		return "";
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: btoAcionadoHistorico.
	 *
	 * @return btoAcionadoHistorico
	 */
	public Boolean getBtoAcionadoHistorico() {
		return btoAcionadoHistorico;
	}

	/**
	 * Set: btoAcionadoHistorico.
	 *
	 * @param btoAcionadoHistorico the bto acionado historico
	 */
	public void setBtoAcionadoHistorico(Boolean btoAcionadoHistorico) {
		this.btoAcionadoHistorico = btoAcionadoHistorico;
	}

	/**
	 * Get: radioArgumentoPesquisa.
	 *
	 * @return radioArgumentoPesquisa
	 */
	public String getRadioArgumentoPesquisa() {
		return radioArgumentoPesquisa;
	}

	/**
	 * Set: radioArgumentoPesquisa.
	 *
	 * @param radioArgumentoPesquisa the radio argumento pesquisa
	 */
	public void setRadioArgumentoPesquisa(String radioArgumentoPesquisa) {
		this.radioArgumentoPesquisa = radioArgumentoPesquisa;
	}

	/**
	 * Get: dsRecursoCredFormatado.
	 *
	 * @return dsRecursoCredFormatado
	 */
	public String getDsRecursoCredFormatado() {
		return dsRecursoCredFormatado;
	}

	/**
	 * Set: dsRecursoCredFormatado.
	 *
	 * @param dsRecursoCredFormatado the ds recurso cred formatado
	 */
	public void setDsRecursoCredFormatado(String dsRecursoCredFormatado) {
		this.dsRecursoCredFormatado = dsRecursoCredFormatado;
	}

	/**
	 * Get: dsRecursoDebFormatado.
	 *
	 * @return dsRecursoDebFormatado
	 */
	public String getDsRecursoDebFormatado() {
		return dsRecursoDebFormatado;
	}

	/**
	 * Set: dsRecursoDebFormatado.
	 *
	 * @param dsRecursoDebFormatado the ds recurso deb formatado
	 */
	public void setDsRecursoDebFormatado(String dsRecursoDebFormatado) {
		this.dsRecursoDebFormatado = dsRecursoDebFormatado;
	}

	/**
	 * Get: mensagemCredFormatada.
	 *
	 * @return mensagemCredFormatada
	 */
	public String getMensagemCredFormatada() {
		return mensagemCredFormatada;
	}

	/**
	 * Set: mensagemCredFormatada.
	 *
	 * @param mensagemCredFormatada the mensagem cred formatada
	 */
	public void setMensagemCredFormatada(String mensagemCredFormatada) {
		this.mensagemCredFormatada = mensagemCredFormatada;
	}

	/**
	 * Get: mensagemDebFormatada.
	 *
	 * @return mensagemDebFormatada
	 */
	public String getMensagemDebFormatada() {
		return mensagemDebFormatada;
	}

	/**
	 * Set: mensagemDebFormatada.
	 *
	 * @param mensagemDebFormatada the mensagem deb formatada
	 */
	public void setMensagemDebFormatada(String mensagemDebFormatada) {
		this.mensagemDebFormatada = mensagemDebFormatada;
	}

	public String getDsSegundaLinhaExtratoCredito() {
		return dsSegundaLinhaExtratoCredito;
	}

	public void setDsSegundaLinhaExtratoCredito(String dsSegundaLinhaExtratoCredito) {
		this.dsSegundaLinhaExtratoCredito = dsSegundaLinhaExtratoCredito;
	}

	public String getDsSegundaLinhaExtratoDebito() {
		return dsSegundaLinhaExtratoDebito;
	}

	public void setDsSegundaLinhaExtratoDebito(String dsSegundaLinhaExtratoDebito) {
		this.dsSegundaLinhaExtratoDebito = dsSegundaLinhaExtratoDebito;
	}
}
