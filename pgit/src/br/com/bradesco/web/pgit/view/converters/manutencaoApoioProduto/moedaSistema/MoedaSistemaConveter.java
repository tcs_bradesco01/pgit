/*
 * Nome: br.com.bradesco.web.pgit.view.converters.manutencaoApoioProduto.moedaSistema
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.converters.manutencaoApoioProduto.moedaSistema;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import br.com.bradesco.web.pgit.service.business.moedassistema.bean.MoedaSistemaSaidaDTO;

/**
 * Nome: MoedaSistemaConveter
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class MoedaSistemaConveter implements Converter {
	
	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		if (value == null || value.trim().equals("")) {
			return null;
		}
		
		String[] arrAgenda = value.split(";");
		if (arrAgenda == null || arrAgenda.length != 3) {
			return null;
		}

		MoedaSistemaSaidaDTO consulta = new MoedaSistemaSaidaDTO();
		consulta.setCdTipoLayoutArquivo(Integer.parseInt(arrAgenda[0]));
		consulta.setCdIndicadorEconomicoLayout(arrAgenda[1]);
		consulta.setCdIndicadorEconomicoMoeda(Integer.parseInt(arrAgenda[2]));
		
		return consulta;
	}

	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		if (value == null) {
			return null;
		}

		MoedaSistemaSaidaDTO consulta = (MoedaSistemaSaidaDTO) value;
		StringBuilder consultaIncAltExcMoeda = new StringBuilder();
		consultaIncAltExcMoeda.append(consulta.getCdTipoLayoutArquivo());
		consultaIncAltExcMoeda.append(";");
		consultaIncAltExcMoeda.append(consulta.getCdIndicadorEconomicoLayout());
		consultaIncAltExcMoeda.append(";");
		consultaIncAltExcMoeda.append(consulta.getCdIndicadorEconomicoMoeda());
		
		return consultaIncAltExcMoeda.toString();
	}

}