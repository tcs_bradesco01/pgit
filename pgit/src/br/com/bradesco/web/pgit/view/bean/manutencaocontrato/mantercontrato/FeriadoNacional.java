package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

public enum FeriadoNacional {

	ANTECIPAR(1), POSTERGAR(2), REJEITAR(3), ACATAR(4);

	private Integer codigoFeriadoNacional;

	private FeriadoNacional(Integer codigoFeriadoNacional) {
		this.codigoFeriadoNacional = codigoFeriadoNacional;
	}

	public Integer getCodigoFeriadoNacional() {
		return codigoFeriadoNacional;
	}

	public void setCodigoFeriadoNacional(Integer codigoFeriadoNacional) {
		this.codigoFeriadoNacional = codigoFeriadoNacional;
	}

}
