/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.manterlibpreviasemconsaldos
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.manterlibpreviasemconsaldos;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarContasVinculadasContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarContasVinculadasContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasVincContEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasVincContSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.IManterLibPreviaSemConSaldosService;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.AlterarLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.AlterarLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarDetLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarDetLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ConsultarLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ExcluirLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.ExcluirLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.IncluirLiberacaoPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlibpreviasemconsaldos.bean.IncluirLiberacaoPreviaSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterLibPreviaSemConSaldosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterLibPreviaSemConSaldosBean extends PagamentosBean {

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo manterLibPreviaSemConSaldosServiceImpl. */
	private IManterLibPreviaSemConSaldosService manterLibPreviaSemConSaldosServiceImpl;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo consultasService. */
	private IConsultasService consultasService;
	
	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;

	/** Atributo listaGridManterLibPreviaSemConSaldos. */
	private List<ConsultarLiberacaoPreviaSaidaDTO> listaGridManterLibPreviaSemConSaldos;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo cdSituacaoFiltro. */
	private Integer cdSituacaoFiltro;

	/** Atributo cboContaDebito. */
	private String cboContaDebito;
	
	/** Atributo listaCboContaDebito. */
	private List<SelectItem> listaCboContaDebito = new ArrayList<SelectItem>();
	
	/** Atributo listaCboContaDebitoHash. */
	private Map<String, ListarContasVinculadasContratoSaidaDTO> listaCboContaDebitoHash = new HashMap<String, ListarContasVinculadasContratoSaidaDTO>();
	
	/** Atributo dataProgramada. */
	private Date dataProgramada;
	
	/** Atributo dataProgramadaAnterior. */
	private Date dataProgramadaAnterior;
	
	/** Atributo valorAutorizado. */
	private BigDecimal valorAutorizado;
	
	/** Atributo valorAutorizadoAnterior. */
	private BigDecimal valorAutorizadoAnterior;
	
	/** Atributo dtProgramada. */
	private String dtProgramada;

	/** Atributo dtLiberacao. */
	private String dtLiberacao;
	
	/** Atributo vlUtilizado. */
	private BigDecimal vlUtilizado;
	
	/** Atributo qtPagamentos. */
	private String qtPagamentos;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	// Cliente
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo nroContrato. */
	private String nroContrato;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	// Conta D�bito
	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;
	
	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;
	
	/** Atributo contaDebito. */
	private String contaDebito;
	
	/** Atributo tipoContaDebito. */
	private String tipoContaDebito;
	
	/** Atributo cpfContaDebito. */
	private String cpfContaDebito;
	
	/** Atributo dsRazaoSocialContaDebito. */
	private String dsRazaoSocialContaDebito;
	
	/** Atributo listaGridContaDebito. */
	private List<ListarContasVincContSaidaDTO> listaGridContaDebito;
	
	/** Atributo listaGridContaDebitoControle. */
	private List<SelectItem> listaGridContaDebitoControle = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionadoGridContaDebito. */
	private Integer itemSelecionadoGridContaDebito;
	
	/** Atributo habilitaContadebito. */
	private boolean habilitaContadebito = true;
	
	/** Atributo contaDebitoSelecionada. */
	private boolean contaDebitoSelecionada = true;
	
	/** Atributo itemContaDebitoSelecionado. */
	private Integer itemContaDebitoSelecionado;

	/** Atributo dsSituacaoSolicitacao. */
	private String dsSituacaoSolicitacao;
	
	/** Atributo dsMotivoSolicitacao. */
	private String dsMotivoSolicitacao;

	/** Atributo registroComContaSelecionada. */
	private ListarContasVincContSaidaDTO registroComContaSelecionada =  new ListarContasVincContSaidaDTO();

	// M�todos
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		// Tela de Filtro
		limparCampos();

		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
				"conManterLibPreviaSemConSaldos");
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setClienteContratoSelecionado(false);
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEmpresaGestoraFiltro(2269651L);

		limparLista();
		setDisableArgumentosConsulta(false);

	}

	/**
	 * Carrega cabecalho.
	 */
	private void carregaCabecalho() {
		limparVariaveis();
		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("0")) { // Filtrar por
															// cliente
			setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getEmpresaGestoraDescCliente());
			setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getNumeroDescCliente());
			setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getDescricaoContratoDescCliente());
			setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSituacaoDescCliente());
			setNrCnpjCpf(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getNrCpfCnpjCliente());
			setDsRazaoSocial(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getDescricaoRazaoSocialCliente());
		} else {
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("1")) { // Filtrar por
																// contrato
				setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getEmpresaGestoraDescContrato());
				setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getNumeroDescContrato());
				setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getDescricaoContratoDescContrato());
				setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getSituacaoDescContrato());

				try {
					DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
					entradaDTO
							.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getEmpresaGestoraFiltro());
					entradaDTO
							.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getCdTipoContratoNegocio());
					entradaDTO
							.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getNrSequenciaContratoNegocio());
					DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService()
							.detalharDadosContrato(entradaDTO);
					setNrCnpjCpf(saidaDTO.getCdCnpjCpfTitular());
					setDsRazaoSocial(saidaDTO.getDsParticipanteTitular());
				} catch (PdcAdapterFunctionalException p) {
					setNrCnpjCpf("");
					setDsRazaoSocial("");
				}
			}

		}

	}

	/**
	 * Imprimir.
	 */
	public void imprimir() {
		String caminho = "/images/Bradesco_logo.JPG";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext
				.getExternalContext().getContext();
		String logoPath = servletContext.getRealPath(caminho);

		carregaCabecalho();

		// Par�metros do Relat�rio
		Map<String, Object> par = new HashMap<String, Object>();
		par
				.put(
						"titulo",
						MessageHelperUtils
								.getI18nMessage("manterLiberacaoPreviaSemConsultaSaldo_path_title"));
		par.put("logoBradesco", logoPath);

		par.put("dsEmpresa", getDsEmpresa());
		par.put("nroContrato", getNroContrato());
		par.put("dsContrato", getDsContrato());
		par.put("cdSituacaoContrato", getCdSituacaoContrato());
		par.put("nrCnpjCpf", getNrCnpjCpf());
		par.put("dsRazaoSocial", getDsRazaoSocial());

		try {
			// M�todo que gera o relat�rio PDF.
			PgitUtil
					.geraRelatorioPdf(
							"/relatorios/agendamentoEfetivacaoEstorno/liberacaoPreviaPagamentos",
							"imprimirLiberacaoPreviaPagamentos",
							getListaGridManterLibPreviaSemConSaldos(), par);
		} catch (Exception e) {
			throw new BradescoViewException(e.getMessage(), e, "", "",
					BradescoViewExceptionActionType.ACTION);
		}
	}

	/**
	 * Limpar variaveis.
	 */
	public void limparVariaveis() {
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");

	}

	/**
	 * Listar combo conta debito.
	 *
	 * @return the string
	 */
	public String listarComboContaDebito() {
		try {
			setCboContaDebito("0");
			listaCboContaDebito = new ArrayList<SelectItem>();

			ListarContasVinculadasContratoEntradaDTO entradaDTO = new ListarContasVinculadasContratoEntradaDTO();

			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("0")
					|| getFiltroAgendamentoEfetivacaoEstornoBean()
							.getTipoFiltroSelecionado().equals("1")) {
				entradaDTO
						.setCdPessoaJuridicaNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdPessoaJuridicaContrato());
				entradaDTO
						.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdTipoContratoNegocio());
				entradaDTO
						.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio());
			}

			entradaDTO.setCdFinalidade(1);

			List<ListarContasVinculadasContratoSaidaDTO> list = comboService
					.listarContasVinculadasContrato(entradaDTO);

			listaCboContaDebitoHash.clear();
			for (ListarContasVinculadasContratoSaidaDTO saida : list) {
				listaCboContaDebito
						.add(new SelectItem(saida.getCdBanco() + " - " + "/"
								+ saida.getCdAgencia() + "/"
								+ saida.getCdConta() + " - "
								+ saida.getCdDigitoConta()));
				listaCboContaDebitoHash.put(saida.getCdBanco() + " - " + "/"
						+ saida.getCdAgencia() + "/" + saida.getCdConta()
						+ " - " + saida.getCdDigitoConta(), saida);
			}
		} catch (PdcAdapterFunctionalException p) {
			setCboContaDebito("0");
			listaCboContaDebito = new ArrayList<SelectItem>();
		}
		return "";
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		limparCampos();
		limparLista();
	}

	/**
	 * Limpar tela principal.
	 *
	 * @return the string
	 */
	public String limparTelaPrincipal() {
		limparContaDebito();
		setChkContaDebito(false);
		limparFiltroPrincipal();
		limparLista();

		return "";
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
		limparContaDebito();
		setChkContaDebito(false);
		limparLista();

		limparCampos();

		return "";

	}

	/**
	 * Consultar liberacao previa.
	 *
	 * @param evt the evt
	 */
	public void consultarLiberacaoPrevia(ActionEvent evt) {
		consultarLiberacaoPrevia();
	}

	// conManterLibPreviaSemConSaldos
	/**
	 * Consultar liberacao previa.
	 *
	 * @return the string
	 */
	public String consultarLiberacaoPrevia() {
		try {
			ConsultarLiberacaoPreviaEntradaDTO entrada = new ConsultarLiberacaoPreviaEntradaDTO();

			entrada
					.setNmPessoaJuridica(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdPessoaJuridicaContrato());
			entrada
					.setCdTipoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdTipoContratoNegocio());
			entrada
					.setNrSequencialContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrSequenciaContratoNegocio());

			if (isChkContaDebito()) {
				entrada.setCdBanco(getCdBancoContaDebito());
				entrada.setCdAgencia(getCdAgenciaBancariaContaDebito());
				entrada.setCdConta(getCdContaBancariaContaDebito());
				entrada
						.setCdDigitoConta(getCdDigitoContaContaDebito() != null
								&& !getCdDigitoContaContaDebito().equals("") ? getCdDigitoContaContaDebito()
								: "00");
			} else {
				entrada.setCdBanco(0);
				entrada.setCdAgencia(0);
				entrada.setCdConta(0L);
				entrada.setCdDigitoConta("00");
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			entrada
					.setDtInicioLiberacao((getDataInicialPagamentoFiltro() == null) ? ""
							: sdf.format(getDataInicialPagamentoFiltro()));
			entrada
					.setDtFinalLiberacao((getDataFinalPagamentoFiltro() == null) ? ""
							: sdf.format(getDataFinalPagamentoFiltro()));
			entrada.setCdPessoaParticipacao(0L);

			setListaGridManterLibPreviaSemConSaldos(getManterLibPreviaSemConSaldosServiceImpl()
					.consultarLiberacaoPrevia(entrada));

			setItemSelecionadoLista(null);
			this.listaControleRadio = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridManterLibPreviaSemConSaldos()
					.size(); i++) {
				this.listaControleRadio.add(new SelectItem(i, " "));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			limparLista();
			setDisableArgumentosConsulta(false);
		}

		return "";
	}

	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar() {
		limparTelaPrincipal();
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setTipoFiltroSelecionado("");
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Limpar lista.
	 *
	 * @return the string
	 */
	public String limparLista() {
		setListaGridManterLibPreviaSemConSaldos(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Preenche dados.
	 */
	public void preencheDados() {
		ConsultarLiberacaoPreviaSaidaDTO registroSelecionado = getListaGridManterLibPreviaSemConSaldos()
				.get(getItemSelecionadoLista());
		ConsultarDetLiberacaoPreviaEntradaDTO entradaDTO = new ConsultarDetLiberacaoPreviaEntradaDTO();

		entradaDTO.setCdPessoaJuridica(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContrato(registroSelecionado
				.getCdTipoPessoaJuridica());
		entradaDTO.setNrSequencialContrato(registroSelecionado
				.getNrSequencialContratoNegocio());
		entradaDTO.setCdTipoSolicitacao(registroSelecionado
				.getCdTipoSolicitacao());
		entradaDTO.setNrSolicitacao(registroSelecionado.getNrSolicitacao());

		ConsultarDetLiberacaoPreviaSaidaDTO saidaDTO = getManterLibPreviaSemConSaldosServiceImpl()
				.consultarDetLiberacaoPrevia(entradaDTO);

		carregaCabecalho();

		setDtLiberacao(registroSelecionado.getDtLiberacaoFormatada());

		setDsBancoDebito(PgitUtil.formatBanco(registroSelecionado
				.getCdBancoDebito(), registroSelecionado.getDsBancoDebito(),
				false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(registroSelecionado
				.getCdAgenciaDebito(), registroSelecionado
				.getCdDigitoAgenciaDebito(), registroSelecionado
				.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(registroSelecionado
				.getCdContaDebito(), registroSelecionado
				.getCdDigitoContaDebito(), false));
		setTipoContaDebito(getDescricaoConta(registroSelecionado
				.getCdBancoDebito(), registroSelecionado.getCdAgenciaDebito(),
				registroSelecionado.getCdContaDebito(), registroSelecionado
						.getCdDigitoContaDebito()));
		setValorAutorizado(registroSelecionado.getVlAutorizado());
		setVlUtilizado(registroSelecionado.getVlUtilizado());
		setQtPagamentos(registroSelecionado.getQtPagamento().toString());

		setDsSituacaoSolicitacao(registroSelecionado.getDsSituacaoSolicitacao());
		setDsMotivoSolicitacao(saidaDTO.getMotivoSolicitacao());

		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getCdUsuarioManutencao());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		consultarLiberacaoPrevia();
		return "";
	}

	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar() {
		try {

			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}
		return "CONSULTAR";
	}

	/**
	 * Selecionar conta debito.
	 *
	 * @return the string
	 */
	public String selecionarContaDebito() {
		setDsBancoDebito(getListaGridContaDebito().get(
				getItemSelecionadoGridContaDebito()).getDsBancoFormatado());
		setDsAgenciaDebito(getListaGridContaDebito().get(
				getItemSelecionadoGridContaDebito()).getDsAgenciaFormatada());
		setContaDebito(getListaGridContaDebito().get(
				getItemSelecionadoGridContaDebito()).getDsContaFormatada());
		setTipoContaDebito(getListaGridContaDebito().get(
				getItemSelecionadoGridContaDebito()).getDsTipoConta());
		setCpfContaDebito(getListaGridContaDebito().get(
				getItemSelecionadoGridContaDebito()).getCdCpfCnpj());
		setDsRazaoSocialContaDebito(getListaGridContaDebito().get(
				getItemSelecionadoGridContaDebito()).getNomeParticipante());

		registroComContaSelecionada = getListaGridContaDebito().get(
				getItemSelecionadoGridContaDebito());

		setContaDebitoSelecionada(true);

		return "SELECIONAR_CONTA_DEBITO";
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		carregaCabecalho();
		setDataProgramada(new Date());
		setValorAutorizado(null);
		// listarComboContaDebito();
		setItemSelecionadoGridContaDebito(null);

		setContaDebitoSelecionada(false);
		setDsBancoDebito("");
		setDsAgenciaDebito("");
		setContaDebito("");
		setTipoContaDebito("");
		setHabilitaContadebito(false);

		listaGridContaDebito = new ArrayList<ListarContasVincContSaidaDTO>();
		try {
			ListarContasVincContEntradaDTO entradaDTO = new ListarContasVincContEntradaDTO();

			entradaDTO.setCdAgencia(0);
			entradaDTO.setCdDigitoAgencia(0);
			entradaDTO.setCdBanco(0);
			entradaDTO.setCdConta(0L);
			entradaDTO.setCdDigitoConta("0");
			entradaDTO.setCdCorpoCfpCnpj(0L);
			entradaDTO.setCdControleCpfCnpj(0);
			entradaDTO.setCdDigitoCpfCnpj(0);
			entradaDTO.setCdFinalidade(0);
			entradaDTO.setCdTipoConta(0);

			entradaDTO
					.setCdPessoaJuridicaNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrSequenciaContratoNegocio());

			setListaGridContaDebito(getManterContratoImpl()
					.listarContasVinculadasContratoCinquenta(entradaDTO));

			listaGridContaDebitoControle = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridContaDebito().size(); i++) {
				listaGridContaDebitoControle.add(new SelectItem(i, ""));

			}

			// Mais que um registro, habilitar bot�o de consultar
			if (listaGridContaDebito != null && listaGridContaDebito.size() > 1) {
				setHabilitaContadebito(true);
				setContaDebitoSelecionada(false);
			} else {
				// Se for apenas 1 registro
				if (listaGridContaDebito != null
						&& listaGridContaDebito.size() == 1) {
					setContaDebitoSelecionada(true);
					setDsBancoDebito(getListaGridContaDebito().get(0)
							.getDsBancoFormatado());
					setDsAgenciaDebito(getListaGridContaDebito().get(0)
							.getDsAgenciaFormatada());
					setContaDebito(getListaGridContaDebito().get(0)
							.getDsContaFormatada());
					setTipoContaDebito(getListaGridContaDebito().get(0)
							.getDsTipoConta());

					// Ser� usado no confirmarIncluir
					registroComContaSelecionada = getListaGridContaDebito()
							.get(0);
					setHabilitaContadebito(false);
				} else {
					setContaDebitoSelecionada(false);
					setDsBancoDebito("");
					setDsAgenciaDebito("");
					setContaDebito("");
					setTipoContaDebito("");
					setHabilitaContadebito(false);
				}
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridContaDebito(null);
			setItemSelecionadoGridContaDebito(null);
			setContaDebitoSelecionada(false);
			setDsBancoDebito("");
			setDsAgenciaDebito("");
			setContaDebito("");
			setTipoContaDebito("");
			setHabilitaContadebito(false);
			return "";
		}

		return "INCLUIR";
	}

	/**
	 * Submit conta debito.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String submitContaDebito(ActionEvent evt) {
		return "";
	}

	/**
	 * Voltar conta debito.
	 *
	 * @return the string
	 */
	public String voltarContaDebito() {
		return "VOLTAR_DEBITO";
	}

	/**
	 * Consultar conta.
	 *
	 * @return the string
	 */
	public String consultarConta() {
		setItemSelecionadoGridContaDebito(null);

		return "CONTA_DEBITO";
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		ConsultarLiberacaoPreviaSaidaDTO registroSelecionado = getListaGridManterLibPreviaSemConSaldos()
				.get(getItemSelecionadoLista());
		carregaCabecalho();
		setDsBancoDebito(PgitUtil.formatBanco(registroSelecionado
				.getCdBancoDebito(), registroSelecionado.getDsBancoDebito(),
				false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(registroSelecionado
				.getCdAgenciaDebito(), registroSelecionado
				.getCdDigitoAgenciaDebito(), registroSelecionado
				.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(registroSelecionado
				.getCdContaDebito(), registroSelecionado
				.getCdDigitoContaDebito(), false));
		setTipoContaDebito(getDescricaoConta(registroSelecionado
				.getCdBancoDebito(), registroSelecionado.getCdAgenciaDebito(),
				registroSelecionado.getCdContaDebito(), registroSelecionado
						.getCdDigitoContaDebito()));

		setDataProgramada(FormatarData
				.formataDiaMesAnoFromPdc(registroSelecionado.getDtLiberacao()));
		setDataProgramadaAnterior(FormatarData
				.formataDiaMesAnoFromPdc(registroSelecionado.getDtLiberacao()));

		setValorAutorizadoAnterior(registroSelecionado.getVlAutorizado());
		setValorAutorizado(registroSelecionado.getVlAutorizado());

		return "ALTERAR";
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		ConsultarLiberacaoPreviaSaidaDTO registroSelecionado = getListaGridManterLibPreviaSemConSaldos()
				.get(getItemSelecionadoLista());
		carregaCabecalho();
		setDsBancoDebito(PgitUtil.formatBanco(registroSelecionado
				.getCdBancoDebito(), registroSelecionado.getDsBancoDebito(),
				false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(registroSelecionado
				.getCdAgenciaDebito(), registroSelecionado
				.getCdDigitoAgenciaDebito(), registroSelecionado
				.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(registroSelecionado
				.getCdContaDebito(), registroSelecionado
				.getCdDigitoContaDebito(), false));
		setTipoContaDebito(getDescricaoConta(registroSelecionado
				.getCdBancoDebito(), registroSelecionado.getCdAgenciaDebito(),
				registroSelecionado.getCdContaDebito(), registroSelecionado
						.getCdDigitoContaDebito()));
		setDtProgramada(registroSelecionado.getDtLiberacaoFormatada());
		setValorAutorizado(registroSelecionado.getVlAutorizado());
		return "EXCLUIR";
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoLista(null);

		return "VOLTAR";
	}

	/**
	 * Voltar alterar.
	 *
	 * @return the string
	 */
	public String voltarAlterar() {
		return "VOLTAR";
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		return "VOLTAR";
	}

	/**
	 * Get: descricaoConta.
	 *
	 * @param banco the banco
	 * @param agencia the agencia
	 * @param conta the conta
	 * @param digitoConta the digito conta
	 * @return descricaoConta
	 */
	private String getDescricaoConta(Integer banco, Integer agencia,
			Long conta, String digitoConta) {
		try {
			ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO = new ConsultarDescBancoAgenciaContaEntradaDTO();
			consultarDescBancoAgenciaContaEntradaDTO.setCdBanco(PgitUtil
					.verificaIntegerNulo(banco));
			consultarDescBancoAgenciaContaEntradaDTO.setCdAgencia(PgitUtil
					.verificaIntegerNulo(agencia));
			consultarDescBancoAgenciaContaEntradaDTO.setCdConta(PgitUtil
					.verificaLongNulo(conta));
			consultarDescBancoAgenciaContaEntradaDTO.setCdDigitoConta(PgitUtil
					.complementaDigitoConta(digitoConta));
			ConsultarDescBancoAgenciaContaSaidaDTO consultarDescBancoAgenciaContaSaidaDTO = getConsultasService()
					.consultarDescBancoAgenciaConta(
							consultarDescBancoAgenciaContaEntradaDTO);
			return consultarDescBancoAgenciaContaSaidaDTO.getDsTipoConta();
		} catch (PdcAdapterFunctionalException e) {
			return "";
		}
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		// setDsBancoDebito(PgitUtil.formatBanco(getListaCboContaDebitoHash().get(getCboContaDebito()).getCdBanco(),
		// getListaCboContaDebitoHash().get(getCboContaDebito()).getDsBanco()));
		// setDsAgenciaDebito(PgitUtil.concatenarCampos(getListaCboContaDebitoHash().get(getCboContaDebito()).getCdAgencia(),
		// getListaCboContaDebitoHash().get(getCboContaDebito()).getDsAgencia()));
		// setContaDebito(PgitUtil.formatConta(getListaCboContaDebitoHash().get(getCboContaDebito()).getCdConta(),getListaCboContaDebitoHash().get(getCboContaDebito()).getCdDigitoConta()));
		setDtProgramada(FormatarData.formataDiaMesAno(getDataProgramada()));
		// setTipoContaDebito(getListaCboContaDebitoHash().get(getCboContaDebito()).getCdTipoConta());
		return "AVANCAR_INCLUIR";
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {
		setDtProgramada(FormatarData.formataDiaMesAno(getDataProgramada()));
		return "AVANCAR_ALTERAR";
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {
		try {
			// ConsultarLiberacaoPreviaSaidaDTO registroSelecionado =
			// getListaGridManterLibPreviaSemConSaldos().get(getItemSelecionadoLista());
			IncluirLiberacaoPreviaEntradaDTO entradaDTO = new IncluirLiberacaoPreviaEntradaDTO();

			entradaDTO
					.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrSequenciaContratoNegocio());

			entradaDTO.setCdBancoDebito(registroComContaSelecionada
					.getCdBanco());
			entradaDTO.setCdAgenciaDebito(registroComContaSelecionada
					.getCdAgencia());
			entradaDTO.setCdContaDebito(registroComContaSelecionada
					.getCdConta());
			entradaDTO.setCdDigitoContaDebito(registroComContaSelecionada
					.getCdDigitoConta());

			entradaDTO.setDtProgramada(this.getDtProgramada());
			entradaDTO.setVlAutorizado(this.getValorAutorizado());

			IncluirLiberacaoPreviaSaidaDTO saidaDTO = getManterLibPreviaSemConSaldosServiceImpl()
					.incluirLiberacaoPrevia(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conManterLibPreviaSemConSaldos",
					BradescoViewExceptionActionType.ACTION, false);
			consultarLiberacaoPrevia();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}

		return "";
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {
		try {
			ConsultarLiberacaoPreviaSaidaDTO registroSelecionado = getListaGridManterLibPreviaSemConSaldos()
					.get(getItemSelecionadoLista());
			ExcluirLiberacaoPreviaEntradaDTO entradaDTO = new ExcluirLiberacaoPreviaEntradaDTO();

			entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
					.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(registroSelecionado
					.getCdTipoPessoaJuridica());
			entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
					.getNrSequencialContratoNegocio());
			entradaDTO.setCdTipoSolicitacao(registroSelecionado
					.getCdTipoSolicitacao());
			entradaDTO.setNrSolicitacao(Long.valueOf(registroSelecionado
					.getNrSolicitacao()));

			ExcluirLiberacaoPreviaSaidaDTO saidaDTO = getManterLibPreviaSemConSaldosServiceImpl()
					.excluirLiberacaoPrevia(entradaDTO);

			BradescoFacesUtils
					.addInfoModalMessage(
							"(" + saidaDTO.getCodMensagem() + ") "
									+ saidaDTO.getMensagem(),
							"conManterLibPreviaSemConSaldos",
							"#{manterLibPreviaSemConSaldosBean.consultarLiberacaoPrevia}",
							false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}
		return "";
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {
		try {
			ConsultarLiberacaoPreviaSaidaDTO registroSelecionado = getListaGridManterLibPreviaSemConSaldos()
					.get(getItemSelecionadoLista());
			AlterarLiberacaoPreviaEntradaDTO entradaDTO = new AlterarLiberacaoPreviaEntradaDTO();

			entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
					.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(registroSelecionado
					.getCdTipoPessoaJuridica());
			entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
					.getNrSequencialContratoNegocio());
			entradaDTO.setCdTipoSolicitacao(registroSelecionado
					.getCdTipoSolicitacao());
			entradaDTO.setNrSolicitacao(Long.valueOf(registroSelecionado
					.getNrSolicitacao()));
			entradaDTO.setDtProgramada(this.getDtProgramada());
			entradaDTO.setVlAutorizado(this.getValorAutorizado());

			AlterarLiberacaoPreviaSaidaDTO saidaDTO = getManterLibPreviaSemConSaldosServiceImpl()
					.alterarLiberacaoPrevia(entradaDTO);

			BradescoFacesUtils
					.addInfoModalMessage(
							"(" + saidaDTO.getCodMensagem() + ") "
									+ saidaDTO.getMensagem(),
							"conManterLibPreviaSemConSaldos",
							"#{manterLibPreviaSemConSaldosBean.consultarLiberacaoPrevia}",
							false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}
		return "";
	}

	// Set's e Get's
	/**
	 * Get: manterLibPreviaSemConSaldosServiceImpl.
	 *
	 * @return manterLibPreviaSemConSaldosServiceImpl
	 */
	public IManterLibPreviaSemConSaldosService getManterLibPreviaSemConSaldosServiceImpl() {
		return manterLibPreviaSemConSaldosServiceImpl;
	}

	/**
	 * Set: manterLibPreviaSemConSaldosServiceImpl.
	 *
	 * @param manterLibPreviaSemConSaldosServiceImpl the manter lib previa sem con saldos service impl
	 */
	public void setManterLibPreviaSemConSaldosServiceImpl(
			IManterLibPreviaSemConSaldosService manterLibPreviaSemConSaldosServiceImpl) {
		this.manterLibPreviaSemConSaldosServiceImpl = manterLibPreviaSemConSaldosServiceImpl;
	}

	/**
	 * Get: listaGridManterLibPreviaSemConSaldos.
	 *
	 * @return listaGridManterLibPreviaSemConSaldos
	 */
	public List<ConsultarLiberacaoPreviaSaidaDTO> getListaGridManterLibPreviaSemConSaldos() {
		return listaGridManterLibPreviaSemConSaldos;
	}

	/**
	 * Set: listaGridManterLibPreviaSemConSaldos.
	 *
	 * @param listaGridManterLibPreviaSemConSaldos the lista grid manter lib previa sem con saldos
	 */
	public void setListaGridManterLibPreviaSemConSaldos(
			List<ConsultarLiberacaoPreviaSaidaDTO> listaGridManterLibPreviaSemConSaldos) {
		this.listaGridManterLibPreviaSemConSaldos = listaGridManterLibPreviaSemConSaldos;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: cdSituacaoFiltro.
	 *
	 * @return cdSituacaoFiltro
	 */
	public Integer getCdSituacaoFiltro() {
		return cdSituacaoFiltro;
	}

	/**
	 * Set: cdSituacaoFiltro.
	 *
	 * @param cdSituacaoFiltro the cd situacao filtro
	 */
	public void setCdSituacaoFiltro(Integer cdSituacaoFiltro) {
		this.cdSituacaoFiltro = cdSituacaoFiltro;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: contaDebito.
	 *
	 * @return contaDebito
	 */
	public String getContaDebito() {
		return contaDebito;
	}

	/**
	 * Set: contaDebito.
	 *
	 * @param contaDebito the conta debito
	 */
	public void setContaDebito(String contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: tipoContaDebito.
	 *
	 * @return tipoContaDebito
	 */
	public String getTipoContaDebito() {
		return tipoContaDebito;
	}

	/**
	 * Set: tipoContaDebito.
	 *
	 * @param tipoContaDebito the tipo conta debito
	 */
	public void setTipoContaDebito(String tipoContaDebito) {
		this.tipoContaDebito = tipoContaDebito;
	}

	/**
	 * Get: listaCboContaDebito.
	 *
	 * @return listaCboContaDebito
	 */
	public List<SelectItem> getListaCboContaDebito() {
		return listaCboContaDebito;
	}

	/**
	 * Set: listaCboContaDebito.
	 *
	 * @param listaCboContaDebito the lista cbo conta debito
	 */
	public void setListaCboContaDebito(List<SelectItem> listaCboContaDebito) {
		this.listaCboContaDebito = listaCboContaDebito;
	}

	/**
	 * Get: dataProgramada.
	 *
	 * @return dataProgramada
	 */
	public Date getDataProgramada() {
		return dataProgramada;
	}

	/**
	 * Set: dataProgramada.
	 *
	 * @param dataProgramada the data programada
	 */
	public void setDataProgramada(Date dataProgramada) {
		this.dataProgramada = dataProgramada;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#getComboService()
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#setComboService(br.com.bradesco.web.pgit.service.business.combo.IComboService)
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: valorAutorizado.
	 *
	 * @return valorAutorizado
	 */
	public BigDecimal getValorAutorizado() {
		return valorAutorizado;
	}

	/**
	 * Set: valorAutorizado.
	 *
	 * @param valorAutorizado the valor autorizado
	 */
	public void setValorAutorizado(BigDecimal valorAutorizado) {
		this.valorAutorizado = valorAutorizado;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: dtLiberacao.
	 *
	 * @return dtLiberacao
	 */
	public String getDtLiberacao() {
		return dtLiberacao;
	}

	/**
	 * Set: dtLiberacao.
	 *
	 * @param dtLiberacao the dt liberacao
	 */
	public void setDtLiberacao(String dtLiberacao) {
		this.dtLiberacao = dtLiberacao;
	}

	/**
	 * Get: qtPagamentos.
	 *
	 * @return qtPagamentos
	 */
	public String getQtPagamentos() {
		return qtPagamentos;
	}

	/**
	 * Set: qtPagamentos.
	 *
	 * @param qtPagamentos the qt pagamentos
	 */
	public void setQtPagamentos(String qtPagamentos) {
		this.qtPagamentos = qtPagamentos;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: dtProgramada.
	 *
	 * @return dtProgramada
	 */
	public String getDtProgramada() {
		return dtProgramada;
	}

	/**
	 * Set: dtProgramada.
	 *
	 * @param dtProgramada the dt programada
	 */
	public void setDtProgramada(String dtProgramada) {
		this.dtProgramada = dtProgramada;
	}

	/**
	 * Get: vlUtilizado.
	 *
	 * @return vlUtilizado
	 */
	public BigDecimal getVlUtilizado() {
		return vlUtilizado;
	}

	/**
	 * Set: vlUtilizado.
	 *
	 * @param vlUtilizado the vl utilizado
	 */
	public void setVlUtilizado(BigDecimal vlUtilizado) {
		this.vlUtilizado = vlUtilizado;
	}

	/**
	 * Get: listaCboContaDebitoHash.
	 *
	 * @return listaCboContaDebitoHash
	 */
	public Map<String, ListarContasVinculadasContratoSaidaDTO> getListaCboContaDebitoHash() {
		return listaCboContaDebitoHash;
	}

	/**
	 * Set lista cbo conta debito hash.
	 *
	 * @param listaCboContaDebitoHash the lista cbo conta debito hash
	 */
	public void setListaCboContaDebitoHash(
			Map<String, ListarContasVinculadasContratoSaidaDTO> listaCboContaDebitoHash) {
		this.listaCboContaDebitoHash = listaCboContaDebitoHash;
	}

	/**
	 * Get: cboContaDebito.
	 *
	 * @return cboContaDebito
	 */
	public String getCboContaDebito() {
		return cboContaDebito;
	}

	/**
	 * Set: cboContaDebito.
	 *
	 * @param cboContaDebito the cbo conta debito
	 */
	public void setCboContaDebito(String cboContaDebito) {
		this.cboContaDebito = cboContaDebito;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasServiceImpl the consultas service
	 */
	public void setConsultasService(IConsultasService consultasServiceImpl) {
		this.consultasService = consultasServiceImpl;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: listaGridContaDebito.
	 *
	 * @return listaGridContaDebito
	 */
	public List<ListarContasVincContSaidaDTO> getListaGridContaDebito() {
		return listaGridContaDebito;
	}

	/**
	 * Set: listaGridContaDebito.
	 *
	 * @param listaGridContaDebito the lista grid conta debito
	 */
	public void setListaGridContaDebito(
			List<ListarContasVincContSaidaDTO> listaGridContaDebito) {
		this.listaGridContaDebito = listaGridContaDebito;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: itemSelecionadoGridContaDebito.
	 *
	 * @return itemSelecionadoGridContaDebito
	 */
	public Integer getItemSelecionadoGridContaDebito() {
		return itemSelecionadoGridContaDebito;
	}

	/**
	 * Set: itemSelecionadoGridContaDebito.
	 *
	 * @param itemSelecionadoGridContaDebito the item selecionado grid conta debito
	 */
	public void setItemSelecionadoGridContaDebito(
			Integer itemSelecionadoGridContaDebito) {
		this.itemSelecionadoGridContaDebito = itemSelecionadoGridContaDebito;
	}

	/**
	 * Get: listaGridContaDebitoControle.
	 *
	 * @return listaGridContaDebitoControle
	 */
	public List<SelectItem> getListaGridContaDebitoControle() {
		return listaGridContaDebitoControle;
	}

	/**
	 * Set: listaGridContaDebitoControle.
	 *
	 * @param listaGridContaDebitoControle the lista grid conta debito controle
	 */
	public void setListaGridContaDebitoControle(
			List<SelectItem> listaGridContaDebitoControle) {
		this.listaGridContaDebitoControle = listaGridContaDebitoControle;
	}

	/**
	 * Is habilita contadebito.
	 *
	 * @return true, if is habilita contadebito
	 */
	public boolean isHabilitaContadebito() {
		return habilitaContadebito;
	}

	/**
	 * Set: habilitaContadebito.
	 *
	 * @param habilitaContadebito the habilita contadebito
	 */
	public void setHabilitaContadebito(boolean habilitaContadebito) {
		this.habilitaContadebito = habilitaContadebito;
	}

	/**
	 * Is conta debito selecionada.
	 *
	 * @return true, if is conta debito selecionada
	 */
	public boolean isContaDebitoSelecionada() {
		return contaDebitoSelecionada;
	}

	/**
	 * Set: contaDebitoSelecionada.
	 *
	 * @param contaDebitoSelecionada the conta debito selecionada
	 */
	public void setContaDebitoSelecionada(boolean contaDebitoSelecionada) {
		this.contaDebitoSelecionada = contaDebitoSelecionada;
	}

	/**
	 * Get: itemContaDebitoSelecionado.
	 *
	 * @return itemContaDebitoSelecionado
	 */
	public Integer getItemContaDebitoSelecionado() {
		return itemContaDebitoSelecionado;
	}

	/**
	 * Set: itemContaDebitoSelecionado.
	 *
	 * @param itemContaDebitoSelecionado the item conta debito selecionado
	 */
	public void setItemContaDebitoSelecionado(Integer itemContaDebitoSelecionado) {
		this.itemContaDebitoSelecionado = itemContaDebitoSelecionado;
	}

	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}

	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}

	/**
	 * Get: dsMotivoSolicitacao.
	 *
	 * @return dsMotivoSolicitacao
	 */
	public String getDsMotivoSolicitacao() {
		return dsMotivoSolicitacao;
	}

	/**
	 * Set: dsMotivoSolicitacao.
	 *
	 * @param dsMotivoSolicitacao the ds motivo solicitacao
	 */
	public void setDsMotivoSolicitacao(String dsMotivoSolicitacao) {
		this.dsMotivoSolicitacao = dsMotivoSolicitacao;
	}

	/**
	 * Get: registroComContaSelecionada.
	 *
	 * @return registroComContaSelecionada
	 */
	public ListarContasVincContSaidaDTO getRegistroComContaSelecionada() {
		return registroComContaSelecionada;
	}

	/**
	 * Set: registroComContaSelecionada.
	 *
	 * @param registroComContaSelecionada the registro com conta selecionada
	 */
	public void setRegistroComContaSelecionada(
			ListarContasVincContSaidaDTO registroComContaSelecionada) {
		this.registroComContaSelecionada = registroComContaSelecionada;
	}

	/**
	 * Get: dataProgramadaAnterior.
	 *
	 * @return dataProgramadaAnterior
	 */
	public Date getDataProgramadaAnterior() {
		return dataProgramadaAnterior;
	}

	/**
	 * Set: dataProgramadaAnterior.
	 *
	 * @param dataProgramadaAnterior the data programada anterior
	 */
	public void setDataProgramadaAnterior(Date dataProgramadaAnterior) {
		this.dataProgramadaAnterior = dataProgramadaAnterior;
	}

	/**
	 * Get: valorAutorizadoAnterior.
	 *
	 * @return valorAutorizadoAnterior
	 */
	public BigDecimal getValorAutorizadoAnterior() {
		return valorAutorizadoAnterior;
	}

	/**
	 * Set: valorAutorizadoAnterior.
	 *
	 * @param valorAutorizadoAnterior the valor autorizado anterior
	 */
	public void setValorAutorizadoAnterior(BigDecimal valorAutorizadoAnterior) {
		this.valorAutorizadoAnterior = valorAutorizadoAnterior;
	}

	/**
	 * Get: cpfContaDebito.
	 *
	 * @return cpfContaDebito
	 */
	public String getCpfContaDebito() {
		return cpfContaDebito;
	}

	/**
	 * Set: cpfContaDebito.
	 *
	 * @param cpfContaDebito the cpf conta debito
	 */
	public void setCpfContaDebito(String cpfContaDebito) {
		this.cpfContaDebito = cpfContaDebito;
	}

	/**
	 * Get: dsRazaoSocialContaDebito.
	 *
	 * @return dsRazaoSocialContaDebito
	 */
	public String getDsRazaoSocialContaDebito() {
		return dsRazaoSocialContaDebito;
	}

	/**
	 * Set: dsRazaoSocialContaDebito.
	 *
	 * @param dsRazaoSocialContaDebito the ds razao social conta debito
	 */
	public void setDsRazaoSocialContaDebito(String dsRazaoSocialContaDebito) {
		this.dsRazaoSocialContaDebito = dsRazaoSocialContaDebito;
	}
}
