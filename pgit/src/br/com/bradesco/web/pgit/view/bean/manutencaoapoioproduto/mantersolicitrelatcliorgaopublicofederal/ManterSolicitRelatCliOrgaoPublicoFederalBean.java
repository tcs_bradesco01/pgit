/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantersolicitrelatcliorgaopublicofederal
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantersolicitrelatcliorgaopublicofederal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.IManterCompTipoServicoFormaLancService;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ConsultarUsuarioSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.IncluirSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterSolicitRelatCliOrgaoPublicoFederalBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterSolicitRelatCliOrgaoPublicoFederalBean {

	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo manterCompTipoServicoFormaLancServiceImpl. */
	private IManterCompTipoServicoFormaLancService manterCompTipoServicoFormaLancServiceImpl;
	
	/** Atributo manterClientesOrgaoPublicoService. */
	private IManterClientesOrgaoPublicoService manterClientesOrgaoPublicoService;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo listaGridPesquisa. */
	private List<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO> listaGridPesquisa;
	
	/** Atributo saidaListarSoliRelatorioOrgaosPublicos. */
	private ListarSoliRelatorioOrgaosPublicosSaidaDTO saidaListarSoliRelatorioOrgaosPublicos;
	
	/** Atributo saidaExcluirSoliRelatorioOrgaosPublicos. */
	private ExcluirSoliRelatorioOrgaosPublicosSaidaDTO saidaExcluirSoliRelatorioOrgaosPublicos;
	
	/** Atributo saidaConsultarUsuarioSoliRelatorioOrgaosPublicos. */
	private ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO saidaConsultarUsuarioSoliRelatorioOrgaosPublicos;
	
	/** Atributo saidaIncluirSoliRelatorioOrgaosPublicos. */
	private IncluirSoliRelatorioOrgaosPublicosSaidaDTO saidaIncluirSoliRelatorioOrgaosPublicos;
	
	/** Atributo dtSolicInicio. */
	private Date dtSolicInicio;
	
	/** Atributo dtSolicFinal. */
	private Date dtSolicFinal;
	
	/** Atributo usuarioSolicitante. */
	private String usuarioSolicitante;
	
	/** Atributo dataSolicitacao. */
	private String dataSolicitacao;
	
	
	
//M�TODOS	
	/**
 * Iniciar tela.
 *
 * @param evt the evt
 */
public void iniciarTela(ActionEvent evt){
		limparIniciar();
	}
	
	/**
	 * Limpar iniciar.
	 */
	public void limparIniciar(){
		setDtSolicFinal(new Date());
		setDtSolicInicio(new Date()); 
		setListaGridPesquisa(new ArrayList<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO>());
	}

	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados(){
		setDtSolicFinal(new Date());
		setDtSolicInicio(new Date());
		setListaGridPesquisa(new ArrayList<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO>());
		
		return"";
	}
	
	/**
	 * Carregar lista.
	 */
	public void carregarLista(){
		ListarSoliRelatorioOrgaosPublicosEntradaDTO entradaDto = new ListarSoliRelatorioOrgaosPublicosEntradaDTO();
		
		entradaDto.setDtFimSolicitacao(FormatarData.formataDiaMesAnoToPdc(getDtSolicFinal()));
		entradaDto.setDtInicioSolicitacao(FormatarData.formataDiaMesAnoToPdc(getDtSolicInicio()));
		
		try{
			saidaListarSoliRelatorioOrgaosPublicos = getManterClientesOrgaoPublicoService().listarSoliRelatorioOrgaosPublicos(entradaDto);
			setListaGridPesquisa(saidaListarSoliRelatorioOrgaosPublicos.getOcorrencias());
			
		}catch(PdcAdapterFunctionalException p){
			limparDados();
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
		}
		
		this.listaControleRadio =  new ArrayList<SelectItem>();
		for (int i = 0; i <= getListaGridPesquisa().size();i++) {
			this.listaControleRadio.add(new SelectItem(i," "));
		}
		
		setItemSelecionadoLista(null);
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		setUsuarioSolicitante(listaGridPesquisa.get(itemSelecionadoLista).getCdUsuario());
		setDataSolicitacao(listaGridPesquisa.get(itemSelecionadoLista).getDsDataSolicitacao().replace('.', '/'));
		
		return "DETALHAR";
	}
	
	/**
	 * Voltar detalhar.
	 *
	 * @return the string
	 */
	public String voltarDetalhar(){
		
		return "VOLTAR_DETALHAR";
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		setUsuarioSolicitante(listaGridPesquisa.get(itemSelecionadoLista).getCdUsuario());
		setDataSolicitacao(listaGridPesquisa.get(itemSelecionadoLista).getDsDataSolicitacao().replace('.', '/'));
		
		return "EXCLUIR";
	}
	
	/**
	 * Voltar excluir.
	 *
	 * @return the string
	 */
	public String voltarExcluir(){
		
		return "VOLTAR_EXCLUIR";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		ExcluirSoliRelatorioOrgaosPublicosEntradaDTO entrada = new ExcluirSoliRelatorioOrgaosPublicosEntradaDTO();
		entrada.setCdSolicitacaoPagamento(listaGridPesquisa.get(itemSelecionadoLista).getCdSolicitacaoPagamento());
		entrada.setNrSolicitacao(listaGridPesquisa.get(itemSelecionadoLista).getNrSolicitacao());
		
		try{
			saidaExcluirSoliRelatorioOrgaosPublicos = getManterClientesOrgaoPublicoService().excluirSoliRelatorioOrgaosPublicos(entrada);
			BradescoFacesUtils.addInfoModalMessage("(" + saidaExcluirSoliRelatorioOrgaosPublicos.getCodMensagem() + ") " +
					saidaExcluirSoliRelatorioOrgaosPublicos.getMensagem(), "#{manterSolicitRelatCliOrgaoPublicoFederalBean.aposExclusao}", BradescoViewExceptionActionType.ACTION, false);
			return "";

		} catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
	}
	
	/**
	 * Apos exclusao.
	 *
	 * @return the string
	 */
	public String aposExclusao(){
		carregarListaAposExclusao();
		return"CONFIRMAR_EXCLUIR";
	}
	
	
	/**
	 * Carregar lista apos exclusao.
	 */
	public void carregarListaAposExclusao(){
		ListarSoliRelatorioOrgaosPublicosEntradaDTO entradaDto = new ListarSoliRelatorioOrgaosPublicosEntradaDTO();
		
		entradaDto.setDtFimSolicitacao(FormatarData.formataDiaMesAnoToPdc(getDtSolicFinal()));
		entradaDto.setDtInicioSolicitacao(FormatarData.formataDiaMesAnoToPdc(getDtSolicInicio()));
		
		try{
			saidaListarSoliRelatorioOrgaosPublicos = getManterClientesOrgaoPublicoService().listarSoliRelatorioOrgaosPublicos(entradaDto);
			setListaGridPesquisa(saidaListarSoliRelatorioOrgaosPublicos.getOcorrencias());
			
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			limparDados();	
		}
		
		this.listaControleRadio =  new ArrayList<SelectItem>();
		for (int i = 0; i <= getListaGridPesquisa().size();i++) {
			this.listaControleRadio.add(new SelectItem(i," "));
		}
		
		setItemSelecionadoLista(null);
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		try{
			ConsultarUsuarioSoliRelatorioOrgaosPublicosEntradaDTO entrada = new ConsultarUsuarioSoliRelatorioOrgaosPublicosEntradaDTO();
			
			entrada.setCdSituacaoVinculacaoConta(null);
			entrada.setNrOcorrencias(null);
			
			saidaConsultarUsuarioSoliRelatorioOrgaosPublicos = getManterClientesOrgaoPublicoService().consultarUsuarioSoliRelatorioOrgaosPublicos(entrada);
			
			setUsuarioSolicitante(saidaConsultarUsuarioSoliRelatorioOrgaosPublicos.getCdUsuarioInclusao());
			setDataSolicitacao(saidaConsultarUsuarioSoliRelatorioOrgaosPublicos.getDtInclusao().replace('.', '/'));
			
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
		
		return"INCLUIR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		limparIniciar();
		return "VOLTAR_INCLUIR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try{
			saidaIncluirSoliRelatorioOrgaosPublicos = getManterClientesOrgaoPublicoService().incluirSoliRelatorioOrgaosPublicosSaidaDTO();
			BradescoFacesUtils.addInfoModalMessage("(" + saidaIncluirSoliRelatorioOrgaosPublicos.getCodMensagem() + ") " + saidaIncluirSoliRelatorioOrgaosPublicos.getMensagem(), "CONFIRMAR_INCLUIR", "", false);	
			carregarLista();
			
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
		
		return"";
	}
	
	

	
	
//GETTERS E SETTERS	
	/**
 * Set: comboService.
 *
 * @param comboService the combo service
 */
public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: manterCompTipoServicoFormaLancServiceImpl.
	 *
	 * @param manterCompTipoServicoFormaLancServiceImpl the manter comp tipo servico forma lanc service impl
	 */
	public void setManterCompTipoServicoFormaLancServiceImpl(
			IManterCompTipoServicoFormaLancService manterCompTipoServicoFormaLancServiceImpl) {
		this.manterCompTipoServicoFormaLancServiceImpl = manterCompTipoServicoFormaLancServiceImpl;
	}
	
	/**
	 * Get: manterCompTipoServicoFormaLancServiceImpl.
	 *
	 * @return manterCompTipoServicoFormaLancServiceImpl
	 */
	public IManterCompTipoServicoFormaLancService getManterCompTipoServicoFormaLancServiceImpl() {
		return manterCompTipoServicoFormaLancServiceImpl;
	}

	/**
	 * Set: dtSolicInicio.
	 *
	 * @param dtSolicInicio the dt solic inicio
	 */
	public void setDtSolicInicio(Date dtSolicInicio) {
		this.dtSolicInicio = dtSolicInicio;
	}

	/**
	 * Get: dtSolicInicio.
	 *
	 * @return dtSolicInicio
	 */
	public Date getDtSolicInicio() {
		return dtSolicInicio;
	}

	/**
	 * Set: dtSolicFinal.
	 *
	 * @param dtSolicFinal the dt solic final
	 */
	public void setDtSolicFinal(Date dtSolicFinal) {
		this.dtSolicFinal = dtSolicFinal;
	}

	/**
	 * Get: dtSolicFinal.
	 *
	 * @return dtSolicFinal
	 */
	public Date getDtSolicFinal() {
		return dtSolicFinal;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}


	/**
	 * Set: manterClientesOrgaoPublicoService.
	 *
	 * @param manterClientesOrgaoPublicoService the manter clientes orgao publico service
	 */
	public void setManterClientesOrgaoPublicoService(
			IManterClientesOrgaoPublicoService manterClientesOrgaoPublicoService) {
		this.manterClientesOrgaoPublicoService = manterClientesOrgaoPublicoService;
	}

	/**
	 * Get: manterClientesOrgaoPublicoService.
	 *
	 * @return manterClientesOrgaoPublicoService
	 */
	public IManterClientesOrgaoPublicoService getManterClientesOrgaoPublicoService() {
		return manterClientesOrgaoPublicoService;
	}

	/**
	 * Set: saidaListarSoliRelatorioOrgaosPublicos.
	 *
	 * @param saidaListarSoliRelatorioOrgaosPublicos the saida listar soli relatorio orgaos publicos
	 */
	public void setSaidaListarSoliRelatorioOrgaosPublicos(
			ListarSoliRelatorioOrgaosPublicosSaidaDTO saidaListarSoliRelatorioOrgaosPublicos) {
		this.saidaListarSoliRelatorioOrgaosPublicos = saidaListarSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Get: saidaListarSoliRelatorioOrgaosPublicos.
	 *
	 * @return saidaListarSoliRelatorioOrgaosPublicos
	 */
	public ListarSoliRelatorioOrgaosPublicosSaidaDTO getSaidaListarSoliRelatorioOrgaosPublicos() {
		return saidaListarSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(List<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: usuarioSolicitante.
	 *
	 * @param usuarioSolicitante the usuario solicitante
	 */
	public void setUsuarioSolicitante(String usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}

	/**
	 * Get: usuarioSolicitante.
	 *
	 * @return usuarioSolicitante
	 */
	public String getUsuarioSolicitante() {
		return usuarioSolicitante;
	}

	/**
	 * Set: dataSolicitacao.
	 *
	 * @param dataSolicitacao the data solicitacao
	 */
	public void setDataSolicitacao(String dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	/**
	 * Get: dataSolicitacao.
	 *
	 * @return dataSolicitacao
	 */
	public String getDataSolicitacao() {
		return dataSolicitacao;
	}

	/**
	 * Set: saidaExcluirSoliRelatorioOrgaosPublicos.
	 *
	 * @param saidaExcluirSoliRelatorioOrgaosPublicos the saida excluir soli relatorio orgaos publicos
	 */
	public void setSaidaExcluirSoliRelatorioOrgaosPublicos(
			ExcluirSoliRelatorioOrgaosPublicosSaidaDTO saidaExcluirSoliRelatorioOrgaosPublicos) {
		this.saidaExcluirSoliRelatorioOrgaosPublicos = saidaExcluirSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Get: saidaExcluirSoliRelatorioOrgaosPublicos.
	 *
	 * @return saidaExcluirSoliRelatorioOrgaosPublicos
	 */
	public ExcluirSoliRelatorioOrgaosPublicosSaidaDTO getSaidaExcluirSoliRelatorioOrgaosPublicos() {
		return saidaExcluirSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Set: saidaConsultarUsuarioSoliRelatorioOrgaosPublicos.
	 *
	 * @param saidaConsultarUsuarioSoliRelatorioOrgaosPublicos the saida consultar usuario soli relatorio orgaos publicos
	 */
	public void setSaidaConsultarUsuarioSoliRelatorioOrgaosPublicos(
			ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO saidaConsultarUsuarioSoliRelatorioOrgaosPublicos) {
		this.saidaConsultarUsuarioSoliRelatorioOrgaosPublicos = saidaConsultarUsuarioSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Get: saidaConsultarUsuarioSoliRelatorioOrgaosPublicos.
	 *
	 * @return saidaConsultarUsuarioSoliRelatorioOrgaosPublicos
	 */
	public ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO getSaidaConsultarUsuarioSoliRelatorioOrgaosPublicos() {
		return saidaConsultarUsuarioSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Set: saidaIncluirSoliRelatorioOrgaosPublicos.
	 *
	 * @param saidaIncluirSoliRelatorioOrgaosPublicos the saida incluir soli relatorio orgaos publicos
	 */
	public void setSaidaIncluirSoliRelatorioOrgaosPublicos(
			IncluirSoliRelatorioOrgaosPublicosSaidaDTO saidaIncluirSoliRelatorioOrgaosPublicos) {
		this.saidaIncluirSoliRelatorioOrgaosPublicos = saidaIncluirSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Get: saidaIncluirSoliRelatorioOrgaosPublicos.
	 *
	 * @return saidaIncluirSoliRelatorioOrgaosPublicos
	 */
	public IncluirSoliRelatorioOrgaosPublicosSaidaDTO getSaidaIncluirSoliRelatorioOrgaosPublicos() {
		return saidaIncluirSoliRelatorioOrgaosPublicos;
	}

}
