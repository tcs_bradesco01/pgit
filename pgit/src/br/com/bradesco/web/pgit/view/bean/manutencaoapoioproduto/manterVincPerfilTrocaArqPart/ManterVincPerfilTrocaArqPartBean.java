/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterVincPerfilTrocaArqPart
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterVincPerfilTrocaArqPart;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.IManterVincPerfilTrocaArqPartService;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharHistAssocTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharHistAssocTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharVincTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.DetalharVincTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ExcluirVincTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ExcluirVincTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincContMaeFilhoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincContMaeFilhoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.IncluirVincTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarAssocTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarAssocTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarContratoMaeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarContratoMaeOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarContratoMaeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarHistAssocTrocaArqParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarHistAssocTrocaArqParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervincperfiltrocaarqpart.bean.ListarParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.selectitem.SelectItemUtils;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterVincPerfilTrocaArqPartBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterVincPerfilTrocaArqPartBean {
	//964511061
	//Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_MANTER_VINC_PERFIL_TROCA_ARQ_PARTICIPANTE. */
	private static final String CON_MANTER_VINC_PERFIL_TROCA_ARQ_PARTICIPANTE = "conManterVincPerfilTrocaArqParticipante";
	
	/** Atributo CON_IDENTIFICACAO_PERFIL. */
	private static final String CON_IDENTIFICACAO_PERFIL = "identificacaoManterVincPerfilTrocaArqParticipantePerfil";
	
	/** Atributo INC_IDENTIFICACAO_PERFIL. */
	private static final String INC_IDENTIFICACAO_PERFIL = "identificacaoIncManterVincPerfilTrocaArqParticipante";
	
	/** Atributo VINCULACAO. */
	private static final String VINCULACAO = "vincManterVincPerfilTrocaArqParticipante";
	
	/** Atributo TELA_INCLUIR. */
	private static final String TELA_INCLUIR = "incManterVincPerfilTrocaArqParticipante";
	
	/** Atributo TELA_EXCLUIR. */
	private static final String TELA_EXCLUIR = "excManterVincPerfilTrocaArqParticipante";
	
	/** Atributo TELA_INCLUIR_AVANCAR. */
	private static final String TELA_INCLUIR_AVANCAR = "incManterVincPerfilTrocaArqParticipanteConf";
	
	/** Atributo TELA_DETALHAR. */
	private static final String TELA_DETALHAR = "detManterVincPerfilTrocaArqParticipante";
	
	/** Atributo TELA_ALTERAR_VINCULACAO. */
	private static final String TELA_ALTERAR_VINCULACAO = "altVincManterVincPerfilTrocaArqParticipante";
	
	/** Atributo TELA_CONFIRMAR_VINCULACAO. */
	private static final String TELA_CONFIRMAR_VINCULACAO = "confVincManterVincPerfilTrocaArqParticipante";
	
	//Pesquisar
	/** Atributo listaContrato. */
	private ListarContratosPgitSaidaDTO listaContrato = new ListarContratosPgitSaidaDTO();
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean = null;
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo habilitaBtoCont. */
	private Boolean habilitaBtoCont;
	
	/** Atributo foco. */
	private String foco;
	
	/** Atributo listaEmpresaGestora. */
	private List<SelectItem> listaEmpresaGestora = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLayout. */
	private List<SelectItem> listaTipoLayout = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLayoutHash. */
	private Map<Integer,String> listaTipoLayoutHash = new HashMap<Integer,String>();
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo codigoPerfil. */
	private Long codigoPerfil;
	
	/** Atributo tipoLayout. */
	private Integer tipoLayout;
	
	/** Atributo nomeRazaoSocialParticipante. */
	private String nomeRazaoSocialParticipante;
	
	/** Atributo itemFiltro. */
	private String itemFiltro;
	
	/** Atributo listaPesquisa. */
	private List<ListarAssocTrocaArqParticipanteSaidaDTO> listaPesquisa;
	
	/** Atributo listaPesquisaControle. */
	private List<SelectItem> listaPesquisaControle;
	
	/** Atributo listaGrid. */
	private List<ListarParticipantesSaidaDTO> listaGrid;
	
	/** Atributo saidaIncVincContratoMaeFilho. */
	private IncluirVincContMaeFilhoSaidaDTO saidaIncVincContratoMaeFilho;
	
	/** Atributo listaGridControle. */
	private List<SelectItem> listaGridControle;
	
	/** Atributo itemSelecionadoGrid. */
	private Integer itemSelecionadoGrid;
	
	/** Atributo cdClub. */
	private Long cdClub;
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	//bean cliente/contrato
	/** Atributo identificacaoClienteBean. */
	private IdentificacaoClienteBean identificacaoClienteBean;
	//impl	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo manterVincPerfilTrocaArqPartImpl. */
	private IManterVincPerfilTrocaArqPartService manterVincPerfilTrocaArqPartImpl;
	
	/** Atributo manterPerfilTrocaArqLayoutServiceImpl. */
	private IManterPerfilTrocaArqLayoutService manterPerfilTrocaArqLayoutServiceImpl;
	
	//tela incluir
	/** Atributo descTipoLayout. */
	private String descTipoLayout;
	
	/** Atributo nomeRazaoSocialParticipanteIncluir. */
	private String nomeRazaoSocialParticipanteIncluir;
	
	/** Atributo cpfCnpjParticipanteIncluir. */
	private String cpfCnpjParticipanteIncluir;
	
	/** Atributo tipoParticipacao. */
	private String tipoParticipacao;
	
	/** Atributo cdTipoParticipacao. */
	private Integer cdTipoParticipacao;
	
	/** Atributo situacaoParticipacao. */
	private String situacaoParticipacao;
	
	/** Atributo codigoPerfilIncluir. */
	private Long codigoPerfilIncluir;
	
	/** Atributo cdTipoLayout. */
	private Integer cdTipoLayout;
	
	/** Atributo cdParticipante. */
	private Long cdParticipante;
	//consultar participantes
	/** Atributo listaGridIncluir. */
	private List<ListarParticipantesSaidaDTO> listaGridIncluir;
	
	/** Atributo listaGridControleIncluir. */
	private List<SelectItem> listaGridControleIncluir;
	
	/** Atributo itemSelecionadoGridIncluir. */
	private Integer itemSelecionadoGridIncluir;
	//CONSULTAR Perfil
	/** Atributo listaGridPerfil. */
	private List<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO> listaGridPerfil;
	
	/** Atributo listaControlePerfil. */
	private List<SelectItem> listaControlePerfil;
	
	/** Atributo itemSelecionadoPerfil. */
	private Integer itemSelecionadoPerfil;
	
	/** Atributo codigoPerfilFiltro. */
	private Long codigoPerfilFiltro;
	
	/** Atributo tipoLayoutArquivoFiltro. */
	private Integer tipoLayoutArquivoFiltro;
	
	//Trilha Auditoria
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo tela. */
	private String tela;

	/** Atributo radioArgPesquisa. */
	private String radioArgPesquisa = null;

	// Cliente Representante
	/** Atributo cpfCnpjRepresentante. */
	private String cpfCnpjRepresentante;

	/** Atributo nomeRazaoRepresentante. */
	private String nomeRazaoRepresentante;

	/** Atributo grupEconRepresentante. */
	private String grupEconRepresentante;

	/** Atributo ativEconRepresentante. */
	private String ativEconRepresentante;

	/** Atributo segRepresentante. */
	private String segRepresentante;

	/** Atributo subSegRepresentante. */
	private String subSegRepresentante;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;
	
	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado;

	// Cliente Participante
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;
	
	/** Atributo cpfCnpjParticipanteTemp. */
	private String cpfCnpjParticipanteTemp;

	/** Atributo nomeRazaoParticipanteTemp. */
	private String nomeRazaoParticipanteTemp;

	/** Atributo cpfCnpjParticipanteDet. */
	private String cpfCnpjParticipanteDet;

	/** Atributo nomeRazaoParticipanteDet. */
	private String nomeRazaoParticipanteDet;

	/** Atributo grupoEconParticipante. */
	private String grupoEconParticipante;

	/** Atributo atividadeEconParticipante. */
	private String atividadeEconParticipante;

	/** Atributo segmentoParticipante. */
	private String segmentoParticipante;

	/** Atributo subSegmentoParticipante. */
	private String subSegmentoParticipante;

	// Contrato
	/** Atributo empresaContrato. */
	private String empresaContrato;

	/** Atributo tipoContrato. */
	private String tipoContrato;

	/** Atributo numeroContrato. */
	private String numeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo situacaoContrato. */
	private String situacaoContrato;

	/** Atributo motivoContrato. */
	private String motivoContrato;

	/** Atributo participacaoContrato. */
	private String participacaoContrato;
	
	/** Atributo consultarListaPerfilTrocaArquivoSaida. */
	private ConsultarListaPerfilTrocaArquivoSaidaDTO consultarListaPerfilTrocaArquivoSaida;

	/** Atributo saidaTipoLayoutArquivoSaidaDTO. */
	private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;

	/** Atributo cdContratoMae. */
	private Long cdContratoMae;

	/** Atributo dsContratoMae. */
	private String dsContratoMae;

	/** Atributo itemSelecionadoContratoMae. */
	private Integer itemSelecionadoContratoMae;

	/** Atributo listaControleContratoMae. */
	private List<SelectItem> listaControleContratoMae;

	/** Atributo cdCpfCnpjContratoMae. */
	private String cdCpfCnpjContratoMae;
	
	/** Atributo dsRazaoSocialContratoMae. */
	private String dsRazaoSocialContratoMae;
	
	/** Atributo dsTipoLayoutArquivoContratoMae. */
	private Long cdPerfilContratoMae;
	
	/** Atributo dsTipoLayoutArquivoContratoMae. */
	private String dsTipoLayoutArquivoContratoMae;
	
	/** Atributo cdTipoLayoutMae. */
	private Integer cdTipoLayoutMae;
	
	/** Atributo cdPerfilTrocaMae. */
	private Long cdPerfilTrocaMae;
	
	/** Atributo cdPessoaJuridicaMae. */
	private Long cdPessoaJuridicaMae;
	
	/** Atributo cdTipoContratoMae. */
	private Integer cdTipoContratoMae;
	
	/** Atributo nrSequencialContratoMae. */
	private Long nrSequencialContratoMae;
	
	/** Atributo cdTipoParticipanteMae. */
	private Integer cdTipoParticipanteMae;
	
	/** Atributo cdCpessoaMae. */
	private Long cdCpessoaMae;

	/** Atributo cdCpfCnpj. */
	private Long cdCpfCnpj;

	/** Atributo cdFilialCnpj. */
    private Integer cdFilialCnpj;

    /** Atributo cdControleCnpj. */
    private Integer cdControleCnpj;
    
    /** Atributo cdCpfCnpjMae. */
    private Long cdCpfCnpjMae;

    /** Atributo cdCnpjFilialMae. */
	private Integer cdCnpjFilialMae;

    /** Atributo cdDigitoCpfCnpjMae. */
	private String cdDigitoCpfCnpjMae;
	
	/** Atributo dsCodigoTipoLayout. */
	private String dsCodigoTipoLayout;
	
	/** Atributo cpfCnpjFormatadoContMae. */
	private String cpfCnpjFormatadoContMae;
	
	/** Atributo dsNomeRazaoContMae. */
	private String dsNomeRazaoContMae;
	
	/** Atributo cdPerfilTrocaArquivoContMae. */
	private Long cdPerfilTrocaArquivoContMae;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivoContMae;
	
	/** Atributo cdTipoParticipacaoPessoa. */
	private Integer cdTipoParticipacaoPessoa;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo cdPessoaContMae. */
	private Long cdPessoaContMae;
	
	/** Atributo entradaListaContratoMae. */
	private ListarContratoMaeEntradaDTO entradaListaContratoMae;
	
	/** Atributo entradaDetalharvincTrocaArq. */
	private DetalharVincTrocaArqParticipanteEntradaDTO entradaDetalharvincTrocaArq;
	
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		this.identificacaoClienteContratoBean.setPaginaRetorno(CON_MANTER_VINC_PERFIL_TROCA_ARQ_PARTICIPANTE);
		this.identificacaoClienteBean.setPaginaRetorno(CON_MANTER_VINC_PERFIL_TROCA_ARQ_PARTICIPANTE);

		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean.setPaginaRetorno(CON_MANTER_VINC_PERFIL_TROCA_ARQ_PARTICIPANTE);	
		

		// Limpar Variaveis
		setListaControleRadio(null);
		setItemSelecionadoLista(null);

		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
		setHabilitaBtoCont(true);

		this.identificacaoClienteContratoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		
		setEntradaListaContratoMae(new ListarContratoMaeEntradaDTO());
		setEntradaDetalharvincTrocaArq(new DetalharVincTrocaArqParticipanteEntradaDTO());
		
		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteManterContrato");
		identificacaoClienteContratoBean.setPaginaRetorno(CON_MANTER_VINC_PERFIL_TROCA_ARQ_PARTICIPANTE);
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
	}

	/**
	 * Consultar vinculacao.
	 */
	public void consultarVinculacao() {
		
	}

	/**
	 * Nome: carregaListaPaginacao
	 * 
	 * @param evt
	 */
	public void carregaListaPaginacao(ActionEvent evt){
	    carregaLista();
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		identificacaoClienteContratoBean.limparCampos();
		setItemSelecionadoLista(null);

		return "";
	}

	/**
	 * Limpar perfil.
	 *
	 * @return the string
	 */
	public String limparPerfil(){
		
		setCodigoPerfilFiltro(null);
		setTipoLayoutArquivoFiltro(null);
		setItemSelecionadoPerfil(null);
		setCdTipoLayout(null);
		setListaGridPerfil(null);
		
		return CON_IDENTIFICACAO_PERFIL;
	}
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados(){

		setItemSelecionadoLista(null);
		
		this.identificacaoClienteBean.setBloqueaRadio(false);
		this.identificacaoClienteBean.setItemFiltroSelecionado("");
		this.identificacaoClienteBean.setHabilitaFiltroDeContrato(true);
		this.identificacaoClienteBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteBean.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());										 
		this.identificacaoClienteBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteBean.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this.identificacaoClienteBean.setDesabilataFiltro(true);
		
		return CON_MANTER_VINC_PERFIL_TROCA_ARQ_PARTICIPANTE;
	}

	/**
	 * Limpar campos.
	 */
	public void limparCampos() {
		setCodigoPerfil(null);
		setCpfCnpjParticipante(null);
		setTipoLayout(null);
		setNomeRazaoSocialParticipante(null);
		setCdClub(null);
		setCpfCnpjParticipanteHistorico(null);
		setNomeRazaoSocialParticipanteHistorico(null);
	}

	/**
	 * Limpar pagina argumentos.
	 */
	public void limparPaginaArgumentos() {
		setListaPesquisa(new ArrayList<ListarAssocTrocaArqParticipanteSaidaDTO>());
		setListaPesquisaControle(new ArrayList<SelectItem>());
		setItemSelecionadoLista(null);
		limparArgumentosPesquisar();
	}

	/**
	 * Limpar argumentos pesquisar.
	 */
	public void limparArgumentosPesquisar() {
		limparCampos();
		setItemFiltro(null);
		limparInformacaoConsulta();
	}

	/**
	 * Listar tipo layout.
	 */
	public void listarTipoLayout(){
		try{
			this.listaTipoLayout = new ArrayList<SelectItem>();
			List<TipoLayoutArquivoSaidaDTO> list = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			
			TipoLayoutArquivoEntradaDTO tipoLayoutArquivoEntradaDTO = new TipoLayoutArquivoEntradaDTO();
			tipoLayoutArquivoEntradaDTO.setCdSituacaoVinculacaoConta(0);
			
			saidaTipoLayoutArquivoSaidaDTO = comboService.listarTipoLayoutArquivo(tipoLayoutArquivoEntradaDTO);
			list = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
			
			listaTipoLayout.clear();
				for(TipoLayoutArquivoSaidaDTO combo : list){
					listaTipoLayoutHash.put(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
					listaTipoLayout.add(new SelectItem(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo()));
				}
		}catch(PdcAdapterFunctionalException e){
			listaTipoLayout = new ArrayList<SelectItem>();
		}
	}
	
	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora(){
		try{
			this.listaEmpresaGestora = new ArrayList<SelectItem>();
			List<EmpresaGestoraSaidaDTO> list = new ArrayList<EmpresaGestoraSaidaDTO>();
			
			list = comboService.listarEmpresasGestoras();
			listaEmpresaGestora.clear();
			for(EmpresaGestoraSaidaDTO combo : list){
				listaEmpresaGestora.add(new SelectItem(combo.getCdPessoaJuridicaContrato(),combo.getDsCnpj()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaEmpresaGestora = new ArrayList<SelectItem>();
		}

	}

	/**
	 * Pesquisar participante.
	 *
	 * @return the string
	 */
	public String pesquisarParticipante() {
			listaGrid = new ArrayList<ListarParticipantesSaidaDTO>();
			try{
				
				ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();

				entradaDTO.setCdPessoaJuridica(listaContrato.getCdPessoaJuridica());
				entradaDTO.setCdTipoContrato(listaContrato.getCdTipoContrato());
				entradaDTO.setNrSequenciaContrato(listaContrato.getNrSequenciaContrato());

				setListaGridIncluir(getManterVincPerfilTrocaArqPartImpl().listarParticipantes(entradaDTO));

				this.listaGridControleIncluir = new ArrayList<SelectItem>();
				for(int i = 0;i<getListaGridIncluir().size();i++){
					listaGridControleIncluir.add(new SelectItem(i," "));
				}

				setItemSelecionadoGridIncluir(null);		
			}catch(PdcAdapterFunctionalException p){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				setListaGrid(null);
				setItemSelecionadoGrid(null);	
			}

			return INC_IDENTIFICACAO_PERFIL;
	}
	
	/**
	 * Nome: pesquisarParticipante
	 * 
	 * @param e
	 */
	public void pesquisarParticipante(ActionEvent e){
	    pesquisarParticipante();
	}

	/**
	 * Limpar informacao consulta.
	 *
	 * @return the string
	 */
	public String limparInformacaoConsulta(){
		setListaPesquisa(new ArrayList<ListarAssocTrocaArqParticipanteSaidaDTO>());
		setListaPesquisaControle(new ArrayList<SelectItem>());
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		
		return "";
	}

	/**
	 * Vinculacao.
	 *
	 * @return the string
	 */
	public String vinculacao() {
	    limparArgumentosPesquisar();
		listarTipoLayout();

		setRadioArgPesquisa(null);
		setListaPesquisa(new ArrayList<ListarAssocTrocaArqParticipanteSaidaDTO>());

		listaContrato = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
						.getItemSelecionadoLista());

		// Cliente Representante
		setCpfCnpjRepresentante(listaContrato.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(listaContrato.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(listaContrato.getDsGrupoEconomico());
		setAtivEconRepresentante(listaContrato.getDsAtividadeEconomica());
		setSegRepresentante(listaContrato.getDsSegmentoCliente());
		setSubSegRepresentante(listaContrato.getDsSubSegmentoCliente());
		setDsAgenciaGestora(listaContrato.getDsAgenciaOperadora());
		setDsGerenteResponsavel(listaContrato.getCdFuncionarioBradesco() + " - " + listaContrato.getDescFuncionarioBradesco());
		
		// Cliente Participante
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ||
				"".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
				setCpfCnpjParticipante("");
				setNomeRazaoParticipante("");
			} else {
				setCpfCnpjParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? ""
					: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
				setNomeRazaoParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
					: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
			}
		
		// Contrato
		setEmpresaContrato(String.valueOf(listaContrato.getDsPessoaJuridica()));
		setTipoContrato(listaContrato.getDsTipoContrato());
		setNumeroContrato(String.valueOf(listaContrato.getNrSequenciaContrato()));
		setSituacaoContrato(listaContrato.getDsSituacaoContrato());
		setMotivoContrato(listaContrato.getDsMotivoSituacao());
		setParticipacaoContrato(listaContrato.getCdTipoParticipacao());
		setDescricaoContrato(listaContrato.getDsContrato());

		return VINCULACAO;
	}
	
	public String alterarContratoMae(){		
		try {
			preencheDados();			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
		
		if (getNrSequencialContratoMae() != null && getNrSequencialContratoMae() == 0) {
			setNrSequencialContratoMae(null);
		}
		return TELA_ALTERAR_VINCULACAO;
	}
	
	public String detalharAlteracaoContratoMae(){
		
		entradaListaContratoMae = new ListarContratoMaeEntradaDTO();
		
		if (getNrSequencialContratoMae()!= null && getNrSequencialContratoMae() != 0) {
			entradaListaContratoMae.setCdPessoaJuridicaMae(getNrSequencialContratoMae());		
			
			try {			
				saidaContratoMae = getManterVincPerfilTrocaArqPartImpl().listarContratoMae(entradaListaContratoMae);			
				listaContratoMae = saidaContratoMae.getOcorrencias();
				
				setCpfCnpjFormatadoContMae(listaContratoMae.get(0).getCpfCnpjFormatado());
				setDsNomeRazaoContMae(listaContratoMae.get(0).getDsNomeRazao());
				setCdPerfilTrocaArquivoContMae(listaContratoMae.get(0).getCdPerfilTrocaArquivo());
				setDsTipoLayoutArquivoContMae(listaContratoMae.get(0).getDsTipoLayoutArquivo());
				setCdPessoaContMae(getNrSequencialContratoMae());
				
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			}			
		}else {
			setNrSequencialContratoMae(0l);
			limparCamposContratoMae();
		}		
		
		return TELA_CONFIRMAR_VINCULACAO;
	}
	
	private void limparCamposContratoMae(){
		setCpfCnpjFormatadoContMae("");
		setDsNomeRazaoContMae("");
		setCdPerfilTrocaArquivoContMae(null);
		setDsTipoLayoutArquivoContMae(null);
		setCdPessoaContMae(null);
	}
	
	public String incluirVincContMaeFilho(){
		
		IncluirVincContMaeFilhoEntradaDTO entrada = new IncluirVincContMaeFilhoEntradaDTO();
		
		entrada.setCdTipoLayoutArquivo(getCdTipoLayoutArquivo());
		entrada.setCdPerfilTrocaArquivo(getCodigoPerfilIncluir());
		entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNumeroContrato()));
		
		entrada.setCdTipoContratoNegocio(entradaDetalharvincTrocaArq.getCdTipoContratoNegocio());		
		entrada.setCdPessoa(entradaDetalharvincTrocaArq.getCdPessoa());		
		entrada.setCdpessoaJuridicaContrato(entradaDetalharvincTrocaArq.getCdPessoaJuridicaContrato());
		entrada.setCdTipoParticipacaoPessoa(entradaDetalharvincTrocaArq.getCdTipoParticipacaoPessoa());
		entrada.setCdPerfilTrocaArquivo(entradaDetalharvincTrocaArq.getCdPerfilTrocaArquivo());
		
		if (!listaContratoMae.isEmpty()) {
			entrada.setCdTipoLayoutMae(listaContratoMae.get(0).getCdTipoLayoutArquivo());
			entrada.setCdPerfilTrocaMae(listaContratoMae.get(0).getCdPerfilTrocaArquivo());
			entrada.setCdPessoaJuridicaMae(saidaContratoMae.getCdpessoaJuridicaContrato());
			entrada.setCdTipoContratoMae(saidaContratoMae.getCdTipoContratoNegocio());
			entrada.setNrSequencialContratoMae(entradaListaContratoMae.getCdPessoaJuridicaMae());
			entrada.setCdTipoParticipanteMae(listaContratoMae.get(0).getCdTipoParticipacaoPessoa());
			entrada.setCdCpessoaMae(listaContratoMae.get(0).getCdPessoa());			
		}
		
		try {
		
		saidaIncVincContratoMaeFilho = getManterVincPerfilTrocaArqPartImpl().incluirVincContratoMaeContratoFilho(entrada);
		
		setItemSelecionadoLista(null);
		
		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(saidaIncVincContratoMaeFilho.getCodMensagem(), 8) + ") " + saidaIncVincContratoMaeFilho.getMensagem(), 
				VINCULACAO, BradescoViewExceptionActionType.ACTION, false);				
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
		
		return null;
	}

	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista() {
		limparInformacaoConsulta();

		try {
			ListarAssocTrocaArqParticipanteEntradaDTO entradaDTO = new ListarAssocTrocaArqParticipanteEntradaDTO();
			entradaDTO.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(tipoLayout));
			entradaDTO.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(codigoPerfil));
			entradaDTO.setCdClub(getCdClub()!= null && getCdClub() != 0 ? getCdClub():0L);
			entradaDTO.setCdpessoaJuridicaContrato(listaContrato.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(listaContrato.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(listaContrato.getNrSequenciaContrato());
			setListaPesquisa(getManterVincPerfilTrocaArqPartImpl().listarAssocTrocaArqParticipante(entradaDTO));
			setListaPesquisaControle(SelectItemUtils.criarSelectItems(listaPesquisa));

			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaPesquisa(null);
			setItemSelecionadoLista(null);
		}

		return "";
	}

	/**
	 * Selecionar particpante.
	 *
	 * @return the string
	 */
	public String selecionarParticpante(){
		ListarParticipantesSaidaDTO itemSelecionado = getListaGridIncluir().get(getItemSelecionadoGridIncluir());
		setNomeRazaoSocialParticipante(itemSelecionado.getNomeRazao());
		setCpfCnpjParticipante(itemSelecionado.getCpf());
		setCdClub(itemSelecionado.getCdParticipante());
		
		return VINCULACAO;
	}

	/**
	 * Selecionar participante incluir.
	 *
	 * @return the string
	 */
	public String selecionarParticipanteIncluir(){
		if(getTela() != null){
			if("1".equals(getTela())) {
				return selecionarParticpante();
			} else {
				ListarParticipantesSaidaDTO itemSelecionado = getListaGridIncluir().get(getItemSelecionadoGridIncluir());
				setNomeRazaoSocialParticipanteIncluir(itemSelecionado.getNomeRazao());
				setCpfCnpjParticipanteIncluir(itemSelecionado.getCpf());
				setTipoParticipacao(itemSelecionado.getTipoParticipacaoFormatador());
				setSituacaoParticipacao(itemSelecionado.getDsSituacaoParticipacao());
				setCdTipoParticipacao(itemSelecionado.getTipoParticipacao());
				setCdParticipante(itemSelecionado.getCdParticipante());

				return TELA_INCLUIR;
			}
		}

		return "";
	}

	/**
	 * Flag tela consultar.
	 *
	 * @return the string
	 */
	public String flagTelaConsultar(){
		setTela("1");
		return pesquisarParticipante();
	}

	/**
	 * Flag tela incluir.
	 *
	 * @return the string
	 */
	public String flagTelaIncluir(){
		setTela("2");
		return pesquisarParticipante();
	}

	/**
	 * Selecionar perfil.
	 *
	 * @return the string
	 */
	public String selecionarPerfil(){
		
		ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO itemSelecionado = getListaGridPerfil().get(getItemSelecionadoPerfil());
		setCodigoPerfilIncluir(itemSelecionado.getCdPerfilTrocaArquivo());
		setCdTipoLayout(itemSelecionado.getCdTipoLayoutArquivo());
		setDescTipoLayout(itemSelecionado.getDsTipoLayoutArquivo());
		
		return "incManterVincPerfilTrocaArqParticipante";
	}
	
	/**
	 * Selecionar Contrato Mae.
	 *
	 * @return the string
	 */
	public String selecionarContratoMae(){
		ListarContratoMaeOcorrenciasSaidaDTO itemSelecionadoListaContratoMae = getSaidaContratoMae().getOcorrencias().get(getItemSelecionadoContratoMae());
		
		setCdCpfCnpj(itemSelecionadoListaContratoMae.getCdCpfCnpj());
	    setCdFilialCnpj(itemSelecionadoListaContratoMae.getCdFilialCnpj());
	    setCdControleCnpj(itemSelecionadoListaContratoMae.getCdControleCnpj());
		setCdCpfCnpjContratoMae(itemSelecionadoListaContratoMae.getCpfCnpjFormatado());
		setDsRazaoSocialContratoMae(itemSelecionadoListaContratoMae.getDsNomeRazao());
		setCdPerfilContratoMae(itemSelecionadoListaContratoMae.getCdPerfilTrocaArquivo());
		setDsTipoLayoutArquivoContratoMae(itemSelecionadoListaContratoMae.getDsTipoLayoutArquivo());
		
		return "incManterVincPerfilTrocaArqParticipante";
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		
		setDescTipoLayout("");
		setCodigoPerfilIncluir(null);
		setCdTipoLayout(null);
		setItemSelecionadoGridIncluir(null);
		setItemSelecionadoPerfil(null);
		setCpfCnpjParticipanteIncluir("");
		setNomeRazaoSocialParticipanteIncluir("");
		setTipoParticipacao("");
		setSituacaoParticipacao("");
		setCdContratoMae(null);
		setCdCpfCnpjContratoMae("");
		setDsRazaoSocialContratoMae("");
		setCdPerfilContratoMae(null);
		setDsTipoLayoutArquivoContratoMae("");
		
		return TELA_INCLUIR;
	}
	

	

	/**
	 * Pesquisar participante incluir.
	 *
	 * @return the string
	 */
	public String pesquisarParticipanteIncluir() {
		listaGridIncluir = new ArrayList<ListarParticipantesSaidaDTO>();

		try{
			ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();

			entradaDTO.setCdPessoaJuridica(listaContrato.getCdPessoaJuridica());
			entradaDTO.setCdTipoContrato(listaContrato.getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(listaContrato.getNrSequenciaContrato());

			setListaGridIncluir(getManterVincPerfilTrocaArqPartImpl().listarParticipantes(entradaDTO));
			
			this.listaGridControleIncluir = new ArrayList<SelectItem>();
			for(int i = 0;i<getListaGridIncluir().size();i++){
				listaGridControleIncluir.add(new SelectItem(i," "));
			}

			setItemSelecionadoGridIncluir(null);		
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridIncluir(null);
			setItemSelecionadoGridIncluir(null);	
		}

		return INC_IDENTIFICACAO_PERFIL;
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		return CON_MANTER_VINC_PERFIL_TROCA_ARQ_PARTICIPANTE;
	}
	
	public String voltarAlterar(){
		if (getNrSequencialContratoMae() == 0) {
			setNrSequencialContratoMae(null);
		}
		return TELA_ALTERAR_VINCULACAO;
	}	
	
	/**
	 * Voltar vinculacao.
	 *
	 * @return the string
	 */
	public String voltarVinculacao(){
		return VINCULACAO;
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		//setItemSelecionadoGridIncluir(null);
		return TELA_INCLUIR;
	}
	

	/**
	 * Consultar perfil.
	 *
	 * @return the string
	 */
	public String consultarPerfil(){
		
		try{
			ConsultarListaPerfilTrocaArquivoEntradaDTO  entrada = new ConsultarListaPerfilTrocaArquivoEntradaDTO();
			entrada.setCdPerfilTrocaArquivo(getCodigoPerfilFiltro());
			entrada.setCdTipoLayoutArquivo(getTipoLayoutArquivoFiltro());
			
			
			consultarListaPerfilTrocaArquivoSaida = getManterPerfilTrocaArqLayoutServiceImpl().consultarListaPerfilTrocaArquivo(entrada);
			setListaGridPerfil(consultarListaPerfilTrocaArquivoSaida.getOcorrencia());
			
			setListaControlePerfil(new ArrayList<SelectItem>());
			for(int i = 0; i < getListaGridPerfil().size();i++){
				getListaControlePerfil().add(new SelectItem(i,""));
			}
			setItemSelecionadoPerfil(null);
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
		
		return "identificacaoManterVincPerfilTrocaArqParticipantePerfil";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		
		return TELA_INCLUIR_AVANCAR;
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try{
		    ListarParticipantesSaidaDTO itemSelecionado = getListaGridIncluir().get(getItemSelecionadoGridIncluir());

		    IncluirVincTrocaArqParticipanteEntradaDTO entrada = new IncluirVincTrocaArqParticipanteEntradaDTO();
			entrada.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(getCdTipoLayout()));
			entrada.setCdPerfilTrocaArquivo(PgitUtil.verificaLongNulo(getCodigoPerfilIncluir()));
			entrada.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaContrato.getCdPessoaJuridica()));
			entrada.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listaContrato.getCdTipoContrato()));
			entrada.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaContrato.getNrSequenciaContrato()));
			entrada.setCdTipoParticipacaoPessoa(PgitUtil.verificaIntegerNulo(getCdTipoParticipacao()));
			entrada.setCdPessoa(PgitUtil.verificaLongNulo(getCdParticipante()));

			//campos adicionados dia 05/09
			entrada.setCdCorpoCpfCnpj(itemSelecionado.getCdCpfCnpj());
			entrada.setNrFilialCnpj(itemSelecionado.getCdFilialCnpj());
			entrada.setCdDigitoCpfCnpj(PgitUtil.preencherZerosAEsquerda(itemSelecionado.getCdControleCnpj().toString(), 2));
			
			if(getItemSelecionadoContratoMae()!=null){
				ListarContratoMaeOcorrenciasSaidaDTO itemSelecionadoListaContratoMae = saidaContratoMae.getOcorrencias().get(getItemSelecionadoContratoMae());
			
				entrada.setCdTipoLayoutMae(itemSelecionadoListaContratoMae.getCdTipoLayoutArquivo());
				entrada.setCdPerfilTrocaMae(itemSelecionadoListaContratoMae.getCdPerfilTrocaArquivo());
				entrada.setCdPessoaJuridicaMae(saidaContratoMae.getCdpessoaJuridicaContrato());
				entrada.setCdTipoContratoMae(saidaContratoMae.getCdTipoContratoNegocio());
				entrada.setNrSequencialContratoMae(getCdContratoMae());
				entrada.setCdTipoParticipanteMae(itemSelecionadoListaContratoMae.getCdTipoParticipacaoPessoa());
				entrada.setCdCpessoaMae(itemSelecionadoListaContratoMae.getCdPessoa());
				entrada.setCdCpfCnpjMae(itemSelecionadoListaContratoMae.getCdCpfCnpj());
				entrada.setCdCnpjFilialMae(itemSelecionadoListaContratoMae.getCdFilialCnpj());
				entrada.setCdDigitoCpfCnpjMae(itemSelecionadoListaContratoMae.getCdControleCnpj().toString());
			}
			
			IncluirVincTrocaArqParticipanteSaidaDTO saida = getManterVincPerfilTrocaArqPartImpl().incluirVincTrocaArqParticipante(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(), VINCULACAO, BradescoViewExceptionActionType.ACTION, false);
			
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
		}
		return "";
	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		limparDadosContratoMae();
		
		entradaDetalharvincTrocaArq = new DetalharVincTrocaArqParticipanteEntradaDTO();
		entradaDetalharvincTrocaArq.setCdPerfilTrocaArquivo(getListaPesquisa().get(getItemSelecionadoLista()).getCdPerfilTrocaArq());
		entradaDetalharvincTrocaArq.setCdTipoLayoutArquivo(getListaPesquisa().get(getItemSelecionadoLista()).getCdTipoLayoutArquivo());
		entradaDetalharvincTrocaArq.setCdPerfilTrocaArquivo(getListaPesquisa().get(getItemSelecionadoLista()).getCdPerfilTrocaArq());
		entradaDetalharvincTrocaArq.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaContrato.getCdPessoaJuridica()));
		entradaDetalharvincTrocaArq.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listaContrato.getCdTipoContrato()));
		entradaDetalharvincTrocaArq.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaContrato.getNrSequenciaContrato()));
		entradaDetalharvincTrocaArq.setCdTipoParticipacaoPessoa(getListaPesquisa().get(getItemSelecionadoLista()).getCdTipoParticipacaoPessoa());
		entradaDetalharvincTrocaArq.setCdPessoa(PgitUtil.verificaLongNulo(getListaPesquisa().get(getItemSelecionadoLista()).getCdClub()));
		
		DetalharVincTrocaArqParticipanteSaidaDTO saida = getManterVincPerfilTrocaArqPartImpl().detalharVincTrocaArqParticipante(entradaDetalharvincTrocaArq);
		
		setCpfCnpjParticipanteIncluir(saida.getCpfFormatado());
		setNomeRazaoSocialParticipanteIncluir(saida.getDsRazaoSocial()); 
		setTipoParticipacao(saida.getDsTipoParticipante());
		setCodigoPerfilIncluir(saida.getCdPerfilTrocaArquivo());
		setDescTipoLayout(getListaPesquisa().get(getItemSelecionadoLista()).getDsTipoLayoutArquivo());
		setSituacaoParticipacao(saida.getCdDescricaoSituacaoParticapante());
		setDsTipoLayoutArquivo(saida.getDsCodigoTipoLayout());
		setNrSequencialContratoMae(saida.getNrSequencialControtaoMae());
		setCdTipoParticipacaoPessoa(saida.getCdTipoParticipacaoPessoa());	
		setCdTipoLayoutArquivo(saida.getCdTipoLayoutArquivo());

		// Trilha Auditoria
		setDataHoraInclusao(saida.getHrInclusaoRegistro());
		setUsuarioInclusao((String)PgitUtil.verificaZero(saida.getCdUsuarioInclusao()));
		setTipoCanalInclusao(saida.getCanalInclusaoFormatado());
		setComplementoInclusao((String)PgitUtil.verificaZero(saida.getCdOperacaoCanalIncusao()));
		

		setDataHoraManutencao(saida.getHrManutencaoRegistro());
		setUsuarioManutencao(saida.getCdUsuarioManutencao()==null||saida.getCdUsuarioManutencao().equals("") ? "":String.valueOf(saida.getCdUsuarioManutencao()) );
		setTipoCanalManutencao(saida.getCanalManutencaoFormatado());
		setComplementoManutencao((String)PgitUtil.verificaZero(saida.getCdOperacaoCanalManutencao()));
		
		setCdContratoMae(validarCamposContratoMaeLong(saida.getNrSequencialControtaoMae()));
		setCdCpfCnpjContratoMae(validarCamposContratoMaeString(saida.getCpfCnpjFormatadoContratoMae()));
		setDsRazaoSocialContratoMae(validarCamposContratoMaeString(saida.getDsNomeRazao()));
		setCdPerfilContratoMae(validarCamposContratoMaeLong(saida.getCdPerfilTrocaMae()));
		setDsTipoLayoutArquivoContratoMae(validarCamposContratoMaeString(saida.getDsTipoLayoutArquivo()));
		setCdTipoLayoutMae(validarCamposContratoMaeInteger(saida.getCdTipoLayoutMae()));
		setCdPerfilTrocaMae(validarCamposContratoMaeLong(saida.getCdPerfilTrocaMae()));
		setCdPessoaJuridicaMae(validarCamposContratoMaeLong(saida.getCdPessoaJuridicaMae()));
		setCdTipoContratoMae(validarCamposContratoMaeInteger(saida.getCdTipoContratoMae()));
		setNrSequencialContratoMae(validarCamposContratoMaeLong(saida.getNrSequencialControtaoMae()));
		setCdTipoParticipanteMae(validarCamposContratoMaeInteger(saida.getCdTipoParticipanteMae()));
		setCdCpessoaMae(validarCamposContratoMaeLong(saida.getCdCpessoaMae()));
		setCdCpfCnpjMae(validarCamposContratoMaeLong(saida.getCdCpfCnpj()));
		setCdCnpjFilialMae(validarCamposContratoMaeInteger(Integer.parseInt(saida.getCdCnpjContrato())));
		setCdDigitoCpfCnpjMae(validarCamposContratoMaeString(saida.getCdControleCnpj().toString()));		
		
	}
	
	private Integer validarCamposContratoMaeInteger(int n){
		if (n == 0) {
			return null;
		}
		return n;
	}
	
	private Long validarCamposContratoMaeLong(Long l){
		if (l == 0) {
			return null;
		}
		return l;
	}
	
	private String validarCamposContratoMaeString(String s){
		if ("0".equals(s) || ("000.000.000-00".equals(s))) {
			return null;
		}
		return s;
	}
	
	/**
	 * Limpar dados cliente.
	 *
	 * @return the string
	 */
	public String limparDadosCliente(){
		identificacaoClienteBean.limparCliente();
		limparArgumentosPesquisar();
		return "";
	}
	
	/**
	 * Limpar dados contrato.
	 *
	 * @return the string
	 */
	public String limparDadosContrato(){
		identificacaoClienteBean.limparContrato();
		limparArgumentosPesquisar();
		return "";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try{
			preencheDados();
			return TELA_EXCLUIR;			
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
		}
		return "";
	}	

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public void limparDadosContratoMae(){
		setCdContratoMae(null);
		setCdCpfCnpjContratoMae(null);
		setDsRazaoSocialContratoMae("");
		setCdPerfilContratoMae(null);
		setDsTipoLayoutArquivoContratoMae(null);
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try{
			preencheDados();
			return TELA_DETALHAR;
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
		}
		return "";
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try{
			ExcluirVincTrocaArqParticipanteEntradaDTO entrada = new ExcluirVincTrocaArqParticipanteEntradaDTO();
			entrada.setCdTipoLayoutArquivo(getListaPesquisa().get(getItemSelecionadoLista()).getCdTipoLayoutArquivo());
			entrada.setCdPerfilTrocaArquivo(getListaPesquisa().get(getItemSelecionadoLista()).getCdPerfilTrocaArq());
			entrada.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(listaContrato.getCdPessoaJuridica()));
			entrada.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(listaContrato.getCdTipoContrato()));
			entrada.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(listaContrato.getNrSequenciaContrato()));
			entrada.setCdTipoParticipacaoPessoa(getListaPesquisa().get(getItemSelecionadoLista()).getCdTipoParticipacaoPessoa());
			entrada.setCdPessoa(PgitUtil.verificaLongNulo(getListaPesquisa().get(getItemSelecionadoLista()).getCdClub()));
			entrada.setCdTipoLayoutMae(getCdTipoLayoutMae());
			entrada.setCdPerfilTrocaMae(getCdPerfilTrocaMae());
			entrada.setCdPessoaJuridicaMae(getCdPessoaJuridicaMae());
			entrada.setCdTipoContratoMae(getCdTipoContratoMae());
			entrada.setNrSequencialContratoMae(getNrSequencialContratoMae());
			entrada.setCdTipoParticipanteMae(getCdTipoParticipanteMae());
			entrada.setCdCpessoaMae(getCdCpessoaMae());
																								  
			ExcluirVincTrocaArqParticipanteSaidaDTO saida = getManterVincPerfilTrocaArqPartImpl().excluirVincTrocaArqParticipante(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(), "#{manterVincPerfilTrocaArqPartBean.retornaVinculacaoPesquisa}", BradescoViewExceptionActionType.ACTION, false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
		}

		return "";
	}

	/**
	 * Retorna vinculacao pesquisa.
	 *
	 * @return the string
	 */
	public String retornaVinculacaoPesquisa() {
		carregaLista();

		return VINCULACAO;
	}

	/**
	 * Get: codigoPerfilFiltro.
	 *
	 * @return codigoPerfilFiltro
	 */
	public Long getCodigoPerfilFiltro() {
		return codigoPerfilFiltro;
	}
	
	/**
	 * Set: codigoPerfilFiltro.
	 *
	 * @param codigoPerfilFiltro the codigo perfil filtro
	 */
	public void setCodigoPerfilFiltro(Long codigoPerfilFiltro) {
		this.codigoPerfilFiltro = codigoPerfilFiltro;
	}
	
	/**
	 * Get: cpfCnpjParticipanteIncluir.
	 *
	 * @return cpfCnpjParticipanteIncluir
	 */
	public String getCpfCnpjParticipanteIncluir() {
		return cpfCnpjParticipanteIncluir;
	}
	
	/**
	 * Set: cpfCnpjParticipanteIncluir.
	 *
	 * @param cpfCnpjParticipanteIncluir the cpf cnpj participante incluir
	 */
	public void setCpfCnpjParticipanteIncluir(String cpfCnpjParticipanteIncluir) {
		this.cpfCnpjParticipanteIncluir = cpfCnpjParticipanteIncluir;
	}
	
	/**
	 * Get: nomeRazaoSocialParticipanteIncluir.
	 *
	 * @return nomeRazaoSocialParticipanteIncluir
	 */
	public String getNomeRazaoSocialParticipanteIncluir() {
		return nomeRazaoSocialParticipanteIncluir;
	}
	
	/**
	 * Set: nomeRazaoSocialParticipanteIncluir.
	 *
	 * @param nomeRazaoSocialParticipanteIncluir the nome razao social participante incluir
	 */
	public void setNomeRazaoSocialParticipanteIncluir(
			String nomeRazaoSocialParticipanteIncluir) {
		this.nomeRazaoSocialParticipanteIncluir = nomeRazaoSocialParticipanteIncluir;
	}
	
	/**
	 * Get: situacaoParticipacao.
	 *
	 * @return situacaoParticipacao
	 */
	public String getSituacaoParticipacao() {
		return situacaoParticipacao;
	}
	
	/**
	 * Set: situacaoParticipacao.
	 *
	 * @param situacaoParticipacao the situacao participacao
	 */
	public void setSituacaoParticipacao(String situacaoParticipacao) {
		this.situacaoParticipacao = situacaoParticipacao;
	}
	
	/**
	 * Get: tipoParticipacao.
	 *
	 * @return tipoParticipacao
	 */
	public String getTipoParticipacao() {
		return tipoParticipacao;
	}
	
	/**
	 * Set: tipoParticipacao.
	 *
	 * @param tipoParticipacao the tipo participacao
	 */
	public void setTipoParticipacao(String tipoParticipacao) {
		this.tipoParticipacao = tipoParticipacao;
	}
	
	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}
	
	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: identificacaoClienteBean.
	 *
	 * @return identificacaoClienteBean
	 */
	public IdentificacaoClienteBean getIdentificacaoClienteBean() {
		return identificacaoClienteBean;
	}
	
	/**
	 * Set: identificacaoClienteBean.
	 *
	 * @param identificacaoClienteBean the identificacao cliente bean
	 */
	public void setIdentificacaoClienteBean(
			IdentificacaoClienteBean identificacaoClienteBean) {
		this.identificacaoClienteBean = identificacaoClienteBean;
	}
	
	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}
	
	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}
	
	/**
	 * Get: itemSelecionadoGridIncluir.
	 *
	 * @return itemSelecionadoGridIncluir
	 */
	public Integer getItemSelecionadoGridIncluir() {
		return itemSelecionadoGridIncluir;
	}
	
	/**
	 * Set: itemSelecionadoGridIncluir.
	 *
	 * @param itemSelecionadoGridIncluir the item selecionado grid incluir
	 */
	public void setItemSelecionadoGridIncluir(Integer itemSelecionadoGridIncluir) {
		this.itemSelecionadoGridIncluir = itemSelecionadoGridIncluir;
	}
	
	/**
	 * Get: listaGridControleIncluir.
	 *
	 * @return listaGridControleIncluir
	 */
	public List<SelectItem> getListaGridControleIncluir() {
		return listaGridControleIncluir;
	}
	
	/**
	 * Set: listaGridControleIncluir.
	 *
	 * @param listaGridControleIncluir the lista grid controle incluir
	 */
	public void setListaGridControleIncluir(
			List<SelectItem> listaGridControleIncluir) {
		this.listaGridControleIncluir = listaGridControleIncluir;
	}
	
	/**
	 * Get: listaGridIncluir.
	 *
	 * @return listaGridIncluir
	 */
	public List<ListarParticipantesSaidaDTO> getListaGridIncluir() {
		return listaGridIncluir;
	}
	
	/**
	 * Set: listaGridIncluir.
	 *
	 * @param listaGridIncluir the lista grid incluir
	 */
	public void setListaGridIncluir(
			List<ListarParticipantesSaidaDTO> listaGridIncluir) {
		this.listaGridIncluir = listaGridIncluir;
	}
	
	/**
	 * Get: codigoPerfil.
	 *
	 * @return codigoPerfil
	 */
	public Long getCodigoPerfil() {
		return codigoPerfil;
	}
	
	/**
	 * Set: codigoPerfil.
	 *
	 * @param codigoPerfil the codigo perfil
	 */
	public void setCodigoPerfil(Long codigoPerfil) {
		this.codigoPerfil = codigoPerfil;
	}
	
	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}
	
	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}
	
	/**
	 * Get: nomeRazaoSocialParticipante.
	 *
	 * @return nomeRazaoSocialParticipante
	 */
	public String getNomeRazaoSocialParticipante() {
		return nomeRazaoSocialParticipante;
	}
	
	/**
	 * Set: nomeRazaoSocialParticipante.
	 *
	 * @param nomeRazaoSocialParticipante the nome razao social participante
	 */
	public void setNomeRazaoSocialParticipante(String nomeRazaoSocialParticipante) {
		this.nomeRazaoSocialParticipante = nomeRazaoSocialParticipante;
	}
	
	/**
	 * Get: tipoLayout.
	 *
	 * @return tipoLayout
	 */
	public Integer getTipoLayout() {
		return tipoLayout;
	}
	
	/**
	 * Set: tipoLayout.
	 *
	 * @param tipoLayout the tipo layout
	 */
	public void setTipoLayout(Integer tipoLayout) {
		this.tipoLayout = tipoLayout;
	}
	
	/**
	 * Get: itemFiltro.
	 *
	 * @return itemFiltro
	 */
	public String getItemFiltro() {
		return itemFiltro;
	}
	
	/**
	 * Set: itemFiltro.
	 *
	 * @param itemFiltro the item filtro
	 */
	public void setItemFiltro(String itemFiltro) {
		this.itemFiltro = itemFiltro;
	}
	
	/**
	 * Get: listaTipoLayout.
	 *
	 * @return listaTipoLayout
	 */
	public List<SelectItem> getListaTipoLayout() {
		return listaTipoLayout;
	}
	
	/**
	 * Set: listaTipoLayout.
	 *
	 * @param listaTipoLayout the lista tipo layout
	 */
	public void setListaTipoLayout(List<SelectItem> listaTipoLayout) {
		this.listaTipoLayout = listaTipoLayout;
	}
	
	/**
	 * Get: listaTipoLayoutHash.
	 *
	 * @return listaTipoLayoutHash
	 */
	public Map<Integer, String> getListaTipoLayoutHash() {
		return listaTipoLayoutHash;
	}
	
	/**
	 * Set lista tipo layout hash.
	 *
	 * @param listaTipoLayoutHash the lista tipo layout hash
	 */
	public void setListaTipoLayoutHash(Map<Integer, String> listaTipoLayoutHash) {
		this.listaTipoLayoutHash = listaTipoLayoutHash;
	}

	/**
	 * Get: descTipoLayout.
	 *
	 * @return descTipoLayout
	 */
	public String getDescTipoLayout() {
		return descTipoLayout;
	}
	
	/**
	 * Set: descTipoLayout.
	 *
	 * @param descTipoLayout the desc tipo layout
	 */
	public void setDescTipoLayout(String descTipoLayout) {
		this.descTipoLayout = descTipoLayout;
	}
	
	/**
	 * Get: itemSelecionadoGrid.
	 *
	 * @return itemSelecionadoGrid
	 */
	public Integer getItemSelecionadoGrid() {
		return itemSelecionadoGrid;
	}
	
	/**
	 * Set: itemSelecionadoGrid.
	 *
	 * @param itemSelecionadoGrid the item selecionado grid
	 */
	public void setItemSelecionadoGrid(Integer itemSelecionadoGrid) {
		this.itemSelecionadoGrid = itemSelecionadoGrid;
	}
	
	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarParticipantesSaidaDTO> getListaGrid() {
		return listaGrid;
	}
	
	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarParticipantesSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}
	
	/**
	 * Get: listaGridControle.
	 *
	 * @return listaGridControle
	 */
	public List<SelectItem> getListaGridControle() {
		return listaGridControle;
	}
	
	/**
	 * Set: listaGridControle.
	 *
	 * @param listaGridControle the lista grid controle
	 */
	public void setListaGridControle(List<SelectItem> listaGridControle) {
		this.listaGridControle = listaGridControle;
	}
	
	/**
	 * Get: manterVincPerfilTrocaArqPartImpl.
	 *
	 * @return manterVincPerfilTrocaArqPartImpl
	 */
	public IManterVincPerfilTrocaArqPartService getManterVincPerfilTrocaArqPartImpl() {
		return manterVincPerfilTrocaArqPartImpl;
	}
	
	/**
	 * Set: manterVincPerfilTrocaArqPartImpl.
	 *
	 * @param manterVincPerfilTrocaArqPartImpl the manter vinc perfil troca arq part impl
	 */
	public void setManterVincPerfilTrocaArqPartImpl(
			IManterVincPerfilTrocaArqPartService manterVincPerfilTrocaArqPartImpl) {
		this.manterVincPerfilTrocaArqPartImpl = manterVincPerfilTrocaArqPartImpl;
	}
	
	/**
	 * Get: listaPesquisa.
	 *
	 * @return listaPesquisa
	 */
	public List<ListarAssocTrocaArqParticipanteSaidaDTO> getListaPesquisa() {
		return listaPesquisa;
	}
	
	/**
	 * Set: listaPesquisa.
	 *
	 * @param listaPesquisa the lista pesquisa
	 */
	public void setListaPesquisa(
			List<ListarAssocTrocaArqParticipanteSaidaDTO> listaPesquisa) {
		this.listaPesquisa = listaPesquisa;
	}
	
	/**
	 * Get: listaPesquisaControle.
	 *
	 * @return listaPesquisaControle
	 */
	public List<SelectItem> getListaPesquisaControle() {
		return listaPesquisaControle;
	}
	
	/**
	 * Set: listaPesquisaControle.
	 *
	 * @param listaPesquisaControle the lista pesquisa controle
	 */
	public void setListaPesquisaControle(List<SelectItem> listaPesquisaControle) {
		this.listaPesquisaControle = listaPesquisaControle;
	}

	/**
	 * Get: tipoLayoutArquivoFiltro.
	 *
	 * @return tipoLayoutArquivoFiltro
	 */
	public Integer getTipoLayoutArquivoFiltro() {
		return tipoLayoutArquivoFiltro;
	}

	/**
	 * Set: tipoLayoutArquivoFiltro.
	 *
	 * @param tipoLayoutArquivoFiltro the tipo layout arquivo filtro
	 */
	public void setTipoLayoutArquivoFiltro(Integer tipoLayoutArquivoFiltro) {
		this.tipoLayoutArquivoFiltro = tipoLayoutArquivoFiltro;
	}

	/**
	 * Get: itemSelecionadoPerfil.
	 *
	 * @return itemSelecionadoPerfil
	 */
	public Integer getItemSelecionadoPerfil() {
		return itemSelecionadoPerfil;
	}

	/**
	 * Set: itemSelecionadoPerfil.
	 *
	 * @param itemSelecionadoPerfil the item selecionado perfil
	 */
	public void setItemSelecionadoPerfil(Integer itemSelecionadoPerfil) {
		this.itemSelecionadoPerfil = itemSelecionadoPerfil;
	}

	/**
	 * Get: listaControlePerfil.
	 *
	 * @return listaControlePerfil
	 */
	public List<SelectItem> getListaControlePerfil() {
		return listaControlePerfil;
	}

	/**
	 * Set: listaControlePerfil.
	 *
	 * @param listaControlePerfil the lista controle perfil
	 */
	public void setListaControlePerfil(List<SelectItem> listaControlePerfil) {
		this.listaControlePerfil = listaControlePerfil;
	}

	/**
	 * Set: codigoPerfilIncluir.
	 *
	 * @param codigoPerfilIncluir the codigo perfil incluir
	 */
	public void setCodigoPerfilIncluir(Long codigoPerfilIncluir) {
		this.codigoPerfilIncluir = codigoPerfilIncluir;
	}

	/**
	 * Get: manterPerfilTrocaArqLayoutServiceImpl.
	 *
	 * @return manterPerfilTrocaArqLayoutServiceImpl
	 */
	public IManterPerfilTrocaArqLayoutService getManterPerfilTrocaArqLayoutServiceImpl() {
		return manterPerfilTrocaArqLayoutServiceImpl;
	}

	/**
	 * Set: manterPerfilTrocaArqLayoutServiceImpl.
	 *
	 * @param manterPerfilTrocaArqLayoutServiceImpl the manter perfil troca arq layout service impl
	 */
	public void setManterPerfilTrocaArqLayoutServiceImpl(
			IManterPerfilTrocaArqLayoutService manterPerfilTrocaArqLayoutServiceImpl) {
		this.manterPerfilTrocaArqLayoutServiceImpl = manterPerfilTrocaArqLayoutServiceImpl;
	}

	/**
	 * Get: codigoPerfilIncluir.
	 *
	 * @return codigoPerfilIncluir
	 */
	public Long getCodigoPerfilIncluir() {
		return codigoPerfilIncluir;
	}

	
	/**
	 * Get: cdTipoLayout.
	 *
	 * @return cdTipoLayout
	 */
	public Integer getCdTipoLayout() {
		return cdTipoLayout;
	}

	/**
	 * Set: cdTipoLayout.
	 *
	 * @param cdTipoLayout the cd tipo layout
	 */
	public void setCdTipoLayout(Integer cdTipoLayout) {
		this.cdTipoLayout = cdTipoLayout;
	}

	/**
	 * Get: cdTipoParticipacao.
	 *
	 * @return cdTipoParticipacao
	 */
	public Integer getCdTipoParticipacao() {
		return cdTipoParticipacao;
	}

	/**
	 * Set: cdTipoParticipacao.
	 *
	 * @param cdTipoParticipacao the cd tipo participacao
	 */
	public void setCdTipoParticipacao(Integer cdTipoParticipacao) {
		this.cdTipoParticipacao = cdTipoParticipacao;
	}

	/**
	 * Get: cdParticipante.
	 *
	 * @return cdParticipante
	 */
	public Long getCdParticipante() {
		return cdParticipante;
	}

	/**
	 * Set: cdParticipante.
	 *
	 * @param cdParticipante the cd participante
	 */
	public void setCdParticipante(Long cdParticipante) {
		this.cdParticipante = cdParticipante;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public Long getCdClub() {
		return cdClub;
	}

	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

//	HISTORICO - THIAGO
	/** Atributo itemFiltroHistorico. */
private String itemFiltroHistorico;
	
	/** Atributo codigoPerfilHistorico. */
	private Long codigoPerfilHistorico;
	
	/** Atributo tipoLayoutHistorico. */
	private Integer tipoLayoutHistorico;
	
	/** Atributo cpfCnpjParticipanteHistorico. */
	private String cpfCnpjParticipanteHistorico;
	
	/** Atributo nomeRazaoSocialParticipanteHistorico. */
	private String nomeRazaoSocialParticipanteHistorico;
	
	/** Atributo dataIncialManutencao. */
	private Date dataIncialManutencao;
	
	/** Atributo dataFimManutencao. */
	private Date dataFimManutencao;
	
	/** Atributo listaHistoricoParticipante. */
	private List<ListarParticipantesSaidaDTO> listaHistoricoParticipante;
	
	/** Atributo listaControleHistoricoParticipante. */
	private List<SelectItem> listaControleHistoricoParticipante;
	
	/** Atributo itemSelecionadoHistoricoParticipante. */
	private Integer itemSelecionadoHistoricoParticipante;
	
	/** Atributo listaGridHistorico. */
	private List<ListarHistAssocTrocaArqParticipanteSaidaDTO> listaGridHistorico;
	
	/** Atributo itemSelecionadoHistorico. */
	private Integer itemSelecionadoHistorico;
	
	/** Atributo listaControleHistorico. */
	private List<SelectItem> listaControleHistorico;
	
	/** Atributo disableArgumentosConsultaHistorico. */
	private boolean disableArgumentosConsultaHistorico;
	
	/** Atributo dsCodigoPerfil. */
	private Long dsCodigoPerfil;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo dsCpfCnpj. */
	private String dsCpfCnpj;
	
	/** Atributo dsTipoNomeRazao. */
	private String dsTipoNomeRazao;
	
	/** Atributo dsTipoParticipacao. */
	private String dsTipoParticipacao;
	
	/** Atributo dsSituacaoParticipacao. */
	private String dsSituacaoParticipacao;
	
	/** Atributo tipoAcao. */
	private String tipoAcao;

	private ListarContratoMaeSaidaDTO saidaContratoMae;
	
	private List<ListarContratoMaeOcorrenciasSaidaDTO> listaContratoMae = new ArrayList<ListarContratoMaeOcorrenciasSaidaDTO>();

	// Trilha Auditoria
	
	/**
	 * Limpar argumentos historico.
	 */
	public void limparArgumentosHistorico(){
		setCodigoPerfilHistorico(null);
		setTipoLayoutHistorico(null);
		setCpfCnpjParticipanteHistorico(null);
		setNomeRazaoSocialParticipanteHistorico(null);
		setCpfCnpjParticipante(null);
		setNomeRazaoSocialParticipante(null);
		setCdClub(null);
	}
	
	/**
	 * Limpar dados historico.
	 */
	public void limparDadosHistorico(){
		setDisableArgumentosConsultaHistorico(false);
		setItemFiltro(null);
		setItemFiltroHistorico(null);
		setDataIncialManutencao(new Date());
		setDataFimManutencao(new Date());
		setListaGridHistorico(null);
		setItemSelecionadoHistorico(null);
		setListaHistoricoParticipante(null);
		setItemSelecionadoHistoricoParticipante(null);
		limparArgumentosHistorico();
	}
	
	/**
	 * Consultar historico.
	 */
	public void consultarHistorico(){
		try{
			listaGridHistorico = new ArrayList<ListarHistAssocTrocaArqParticipanteSaidaDTO>();
			ListarHistAssocTrocaArqParticipanteEntradaDTO entrada = new ListarHistAssocTrocaArqParticipanteEntradaDTO();

			entrada.setCdClub(getCdClub()!= null && getCdClub() != 0 ? getCdClub():0L);
			entrada.setCdPerfilTrocaArquivo(getCodigoPerfilHistorico());
			entrada.setCdTipoLayoutArquivo(getTipoLayoutHistorico());
			entrada.setDtManutencaoInicio(FormatarData.formataDiaMesAno(getDataIncialManutencao()));
			entrada.setDtManutencaoFim(FormatarData.formataDiaMesAno(getDataFimManutencao()));
			
			entrada.setCdPessoaJuridicaContrato(listaContrato.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(listaContrato.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(listaContrato.getNrSequenciaContrato());

			setListaGridHistorico(getManterVincPerfilTrocaArqPartImpl().listarHistAssocTrocaArqParticipante(entrada));
			
			listaControleHistorico = new ArrayList<SelectItem>();
			for(int i =0 ;i  < getListaGridHistorico().size();i++){
				listaControleHistorico.add(new SelectItem(i,""));
			}
			
			setItemSelecionadoHistorico(null);			
			setDisableArgumentosConsultaHistorico(true);
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridHistorico(null);
			setItemSelecionadoHistorico(null);
			setDisableArgumentosConsultaHistorico(false);
		}
	}
	
	/**
	 * Pesquisar historico.
	 *
	 * @param evt the evt
	 */
	public void pesquisarHistorico(ActionEvent evt){
		consultarHistorico();
	}
	
	/**
	 * Voltar participante historico.
	 *
	 * @return the string
	 */
	public  String voltarParticipanteHistorico(){
		setListaHistoricoParticipante(null);
		setItemSelecionadoHistoricoParticipante(null);
		return "TELA_HISTORICO"; 
	}

	/**
	 * Pesquisar participante historico.
	 *
	 * @return the string
	 */
	public String pesquisarParticipanteHistorico(){		
		listaHistoricoParticipante = new ArrayList<ListarParticipantesSaidaDTO>();
		try{
			ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();		

			entradaDTO.setCdPessoaJuridica(listaContrato.getCdPessoaJuridica());
			entradaDTO.setCdTipoContrato(listaContrato.getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(listaContrato.getNrSequenciaContrato());

			setListaHistoricoParticipante(getManterVincPerfilTrocaArqPartImpl().listarParticipantes(entradaDTO));

			this.listaControleHistoricoParticipante = new ArrayList<SelectItem>();
			for(int i = 0;i<getListaHistoricoParticipante().size();i++){
				listaControleHistoricoParticipante.add(new SelectItem(i," "));
			}
			setItemSelecionadoHistoricoParticipante(null);		

			return "TELA_PARTICIPANTE_HISTORICO";
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaHistoricoParticipante(null);
			setItemSelecionadoHistoricoParticipante(null);

			return "";
		}		
	}
	
	/**
	 * Pesquisar Contrato Mae.
	 *
	 * @return the string
	 */
	public String pesquisarContratoMae(){
		setItemSelecionadoContratoMae(null);
		
		ListarContratoMaeEntradaDTO entrada = new ListarContratoMaeEntradaDTO(); 
		entrada.setCdPessoaJuridicaMae(getCdContratoMae());
		
		try{
			saidaContratoMae = getManterVincPerfilTrocaArqPartImpl().listarContratoMae(entrada);
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
		setListaControleContratoMae(new ArrayList<SelectItem>());
		
		for(int i = 0; i < saidaContratoMae.getOcorrencias().size();i++){
			getListaControleContratoMae().add(new SelectItem(i,""));
		}
		
		return "identificarContratoMae";
	}

	/**
	 * Paginar Contrato Mae.
	 */
	public void paginarContratoMae(ActionEvent act){
		pesquisarContratoMae();
	}
	
	public String voltarIncluirContratoMae(){
		return "incManterVincPerfilTrocaArqParticipante";
	}
	
	/**
	 * Preenche dados participante selecionado.
	 */
	public void preencheDadosParticipanteSelecionado(){
		if(getCdClub() != null){
			setCpfCnpjParticipanteHistorico(getCpfCnpjParticipante());
			setNomeRazaoSocialParticipanteHistorico(getNomeRazaoSocialParticipante());
			setItemFiltroHistorico("1");
		}
	}

	/**
	 * Selecionar particpante historico.
	 *
	 * @return the string
	 */
	public String selecionarParticpanteHistorico(){
		ListarParticipantesSaidaDTO itemSelecionado = getListaHistoricoParticipante().get(getItemSelecionadoHistoricoParticipante());
		setCpfCnpjParticipanteHistorico(itemSelecionado.getCpf());
		setNomeRazaoSocialParticipanteHistorico(itemSelecionado.getNomeRazao());
		setNomeRazaoSocialParticipante(itemSelecionado.getNomeRazao());
		setCpfCnpjParticipante(itemSelecionado.getCpf());
		setCdClub(itemSelecionado.getCdParticipante());
		setItemFiltro(getItemFiltroHistorico());
		return "TELA_HISTORICO";
	}
	
	/**
	 * Historico.
	 *
	 * @return the string
	 */
	public String historico(){
		limparDadosHistorico();
		preencheDadosParticipanteSelecionado();
		return "hisManterVincPerfilTrocaArqParticipante";
	}

	/**
	 * Voltar consulta.
	 *
	 * @return the string
	 */
	public String voltarConsulta(){
		setItemSelecionadoLista(null);
		return "TELA_CONSULTA";
	}

	/**
	 * Detalhar historico.
	 *
	 * @return the string
	 */
	public String detalharHistorico(){
		limparDadosContratoMae();
		try{
			DetalharHistAssocTrocaArqParticipanteEntradaDTO entrada = new DetalharHistAssocTrocaArqParticipanteEntradaDTO();
			ListarHistAssocTrocaArqParticipanteSaidaDTO itemSelecionado = getListaGridHistorico().get(getItemSelecionadoHistorico());
			
			entrada.setCdTipoLayoutArquivo(itemSelecionado.getCdTipoLayoutArquivo());
			entrada.setCdPerfilTrocaArquivo(itemSelecionado.getCdPerfilTrocaArquivo());
			entrada.setCdPessoaJuridicaContrato(listaContrato.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(listaContrato.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(listaContrato.getNrSequenciaContrato());
			entrada.setCdTipoParticipacaoPessoa(itemSelecionado.getCdTipoParticipacaoPessoa());
			entrada.setCdPessoa(itemSelecionado.getCdClub());
			entrada.setHrInclusaoRegistroHist(itemSelecionado.getHrManutencaoRegistro());

			DetalharHistAssocTrocaArqParticipanteSaidaDTO saida = getManterVincPerfilTrocaArqPartImpl().detalharHistAssocTrocaArqParticipante(entrada);
			
			setDsCodigoPerfil(saida.getCdPerfilTrocaArquivo());
			setDsTipoLayoutArquivo(itemSelecionado.getDsLayout());
			setDsCpfCnpj(CpfCnpjUtils.formatarCpfCnpj(saida.getCdCorpoCpfCnpj(), saida.getCdCnpjContrato(), saida.getCdCpfCnpjDigito() ));
			setDsTipoNomeRazao(saida.getDsRazaoSocial());
			setDsTipoParticipacao(saida.getDsTipoParticipante());
			setDsSituacaoParticipacao(saida.getCdDescricaoSituacaoParticapante());
				
			setTipoAcao(saida.getDsIndicadorTipoManutencao());

			setDataHoraInclusao(saida.getHrInclusaoRegistro());
			setUsuarioInclusao(saida.getCdUsuarioInclusao());
			setTipoCanalInclusao(PgitUtil.concatenarCampos(saida.getCdTipoCanalInclusao(), saida.getDsCanalInclusao()));
			setComplementoInclusao((String)PgitUtil.verificaZero(saida.getCdOperacaoCanalInclusao()));

			setDataHoraManutencao(saida.getHrManutencaoRegistro());
			setUsuarioManutencao(String.valueOf(saida.getCdUsuarioManutencao()));
			setTipoCanalManutencao(PgitUtil.concatenarCampos(saida.getCdTipoCanalManutencao(), saida.getDsCanalManutencao()));
			setComplementoManutencao((String)PgitUtil.verificaZero(saida.getCdOperacaoCanalManutencao()));
			
			setCdContratoMae(saida.getCdContratoMae());
			setCdCpfCnpjContratoMae(saida.getCpfCnpjContratoMaeFormatado());
			setDsRazaoSocialContratoMae(saida.getDsRazaoSocialContratoMae());
			setCdPerfilContratoMae(saida.getCdPerfilContratoMae());
			setDsTipoLayoutArquivoContratoMae(saida.getDsTipoLayoutArquivoContratoMae());
			
			
			return "TELA_DETALHAR_HISTORICO";
		}catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
			
	}
	
	/**
	 * Voltar historico.
	 *
	 * @return the string
	 */
	public String voltarHistorico(){
		setItemSelecionadoHistorico(null);
		return "hisManterVincPerfilTrocaArqParticipante";
	}	
	
	/**
	 * Get: codigoPerfilHistorico.
	 *
	 * @return codigoPerfilHistorico
	 */
	public Long getCodigoPerfilHistorico() {
		return codigoPerfilHistorico;
	}

	/**
	 * Set: codigoPerfilHistorico.
	 *
	 * @param codigoPerfilHistorico the codigo perfil historico
	 */
	public void setCodigoPerfilHistorico(Long codigoPerfilHistorico) {
		this.codigoPerfilHistorico = codigoPerfilHistorico;
	}

	/**
	 * Get: itemFiltroHistorico.
	 *
	 * @return itemFiltroHistorico
	 */
	public String getItemFiltroHistorico() {
		return itemFiltroHistorico;
	}

	/**
	 * Set: itemFiltroHistorico.
	 *
	 * @param itemFiltroHistorico the item filtro historico
	 */
	public void setItemFiltroHistorico(String itemFiltroHistorico) {
		this.itemFiltroHistorico = itemFiltroHistorico;
	}

	/**
	 * Get: tipoLayoutHistorico.
	 *
	 * @return tipoLayoutHistorico
	 */
	public Integer getTipoLayoutHistorico() {
		return tipoLayoutHistorico;
	}

	/**
	 * Set: tipoLayoutHistorico.
	 *
	 * @param tipoLayoutHistorico the tipo layout historico
	 */
	public void setTipoLayoutHistorico(Integer tipoLayoutHistorico) {
		this.tipoLayoutHistorico = tipoLayoutHistorico;
	}	
	
	/**
	 * Is disable argumentos consulta historico.
	 *
	 * @return true, if is disable argumentos consulta historico
	 */
	public boolean isDisableArgumentosConsultaHistorico() {
		return disableArgumentosConsultaHistorico;
	}

	/**
	 * Set: disableArgumentosConsultaHistorico.
	 *
	 * @param disableArgumentosConsultaHistorico the disable argumentos consulta historico
	 */
	public void setDisableArgumentosConsultaHistorico(
			boolean disableArgumentosConsultaHistorico) {
		this.disableArgumentosConsultaHistorico = disableArgumentosConsultaHistorico;
	}
	
	/**
	 * Get: dataFimManutencao.
	 *
	 * @return dataFimManutencao
	 */
	public Date getDataFimManutencao() {
		return dataFimManutencao;
	}

	/**
	 * Set: dataFimManutencao.
	 *
	 * @param dataFimManutencao the data fim manutencao
	 */
	public void setDataFimManutencao(Date dataFimManutencao) {
		this.dataFimManutencao = dataFimManutencao;
	}

	/**
	 * Get: dataIncialManutencao.
	 *
	 * @return dataIncialManutencao
	 */
	public Date getDataIncialManutencao() {
		return dataIncialManutencao;
	}

	/**
	 * Set: dataIncialManutencao.
	 *
	 * @param dataIncialManutencao the data incial manutencao
	 */
	public void setDataIncialManutencao(Date dataIncialManutencao) {
		this.dataIncialManutencao = dataIncialManutencao;
	}

	/**
	 * Get: cpfCnpjParticipanteHistorico.
	 *
	 * @return cpfCnpjParticipanteHistorico
	 */
	public String getCpfCnpjParticipanteHistorico() {
		return cpfCnpjParticipanteHistorico;
	}

	/**
	 * Set: cpfCnpjParticipanteHistorico.
	 *
	 * @param cpfCnpjParticipanteHistorico the cpf cnpj participante historico
	 */
	public void setCpfCnpjParticipanteHistorico(String cpfCnpjParticipanteHistorico) {
		this.cpfCnpjParticipanteHistorico = cpfCnpjParticipanteHistorico;
	}

	/**
	 * Get: nomeRazaoSocialParticipanteHistorico.
	 *
	 * @return nomeRazaoSocialParticipanteHistorico
	 */
	public String getNomeRazaoSocialParticipanteHistorico() {
		return nomeRazaoSocialParticipanteHistorico;
	}

	/**
	 * Set: nomeRazaoSocialParticipanteHistorico.
	 *
	 * @param nomeRazaoSocialParticipanteHistorico the nome razao social participante historico
	 */
	public void setNomeRazaoSocialParticipanteHistorico(
			String nomeRazaoSocialParticipanteHistorico) {
		this.nomeRazaoSocialParticipanteHistorico = nomeRazaoSocialParticipanteHistorico;
	}

	/**
	 * Get: itemSelecionadoHistorico.
	 *
	 * @return itemSelecionadoHistorico
	 */
	public Integer getItemSelecionadoHistorico() {
		return itemSelecionadoHistorico;
	}

	/**
	 * Set: itemSelecionadoHistorico.
	 *
	 * @param itemSelecionadoHistorico the item selecionado historico
	 */
	public void setItemSelecionadoHistorico(Integer itemSelecionadoHistorico) {
		this.itemSelecionadoHistorico = itemSelecionadoHistorico;
	}

	/**
	 * Get: listaControleHistorico.
	 *
	 * @return listaControleHistorico
	 */
	public List<SelectItem> getListaControleHistorico() {
		return listaControleHistorico;
	}

	/**
	 * Set: listaControleHistorico.
	 *
	 * @param listaControleHistorico the lista controle historico
	 */
	public void setListaControleHistorico(List<SelectItem> listaControleHistorico) {
		this.listaControleHistorico = listaControleHistorico;
	}

	/**
	 * Get: listaGridHistorico.
	 *
	 * @return listaGridHistorico
	 */
	public List<ListarHistAssocTrocaArqParticipanteSaidaDTO> getListaGridHistorico() {
		return listaGridHistorico;
	}

	/**
	 * Set: listaGridHistorico.
	 *
	 * @param listaGridHistorico the lista grid historico
	 */
	public void setListaGridHistorico(
			List<ListarHistAssocTrocaArqParticipanteSaidaDTO> listaGridHistorico) {
		this.listaGridHistorico = listaGridHistorico;
	}

	/**
	 * Get: itemSelecionadoHistoricoParticipante.
	 *
	 * @return itemSelecionadoHistoricoParticipante
	 */
	public Integer getItemSelecionadoHistoricoParticipante() {
		return itemSelecionadoHistoricoParticipante;
	}

	/**
	 * Set: itemSelecionadoHistoricoParticipante.
	 *
	 * @param itemSelecionadoHistoricoParticipante the item selecionado historico participante
	 */
	public void setItemSelecionadoHistoricoParticipante(
			Integer itemSelecionadoHistoricoParticipante) {
		this.itemSelecionadoHistoricoParticipante = itemSelecionadoHistoricoParticipante;
	}

	/**
	 * Get: listaControleHistoricoParticipante.
	 *
	 * @return listaControleHistoricoParticipante
	 */
	public List<SelectItem> getListaControleHistoricoParticipante() {
		return listaControleHistoricoParticipante;
	}

	/**
	 * Set: listaControleHistoricoParticipante.
	 *
	 * @param listaControleHistoricoParticipante the lista controle historico participante
	 */
	public void setListaControleHistoricoParticipante(
			List<SelectItem> listaControleHistoricoParticipante) {
		this.listaControleHistoricoParticipante = listaControleHistoricoParticipante;
	}

	/**
	 * Get: listaHistoricoParticipante.
	 *
	 * @return listaHistoricoParticipante
	 */
	public List<ListarParticipantesSaidaDTO> getListaHistoricoParticipante() {
		return listaHistoricoParticipante;
	}

	/**
	 * Set: listaHistoricoParticipante.
	 *
	 * @param listaHistoricoParticipante the lista historico participante
	 */
	public void setListaHistoricoParticipante(
			List<ListarParticipantesSaidaDTO> listaHistoricoParticipante) {
		this.listaHistoricoParticipante = listaHistoricoParticipante;
	}

	/**
	 * Get: tipoAcao.
	 *
	 * @return tipoAcao
	 */
	public String getTipoAcao() {
		return tipoAcao;
	}

	/**
	 * Set: tipoAcao.
	 *
	 * @param tipoAcao the tipo acao
	 */
	public void setTipoAcao(String tipoAcao) {
		this.tipoAcao = tipoAcao;
	}

	/**
	 * Get: dsCodigoPerfil.
	 *
	 * @return dsCodigoPerfil
	 */
	public Long getDsCodigoPerfil() {
		return dsCodigoPerfil;
	}

	/**
	 * Set: dsCodigoPerfil.
	 *
	 * @param dsCodigoPerfil the ds codigo perfil
	 */
	public void setDsCodigoPerfil(Long dsCodigoPerfil) {
		this.dsCodigoPerfil = dsCodigoPerfil;
	}

	/**
	 * Get: dsCpfCnpj.
	 *
	 * @return dsCpfCnpj
	 */
	public String getDsCpfCnpj() {
		return dsCpfCnpj;
	}

	/**
	 * Set: dsCpfCnpj.
	 *
	 * @param dsCpfCnpj the ds cpf cnpj
	 */
	public void setDsCpfCnpj(String dsCpfCnpj) {
		this.dsCpfCnpj = dsCpfCnpj;
	}

	/**
	 * Get: dsSituacaoParticipacao.
	 *
	 * @return dsSituacaoParticipacao
	 */
	public String getDsSituacaoParticipacao() {
		return dsSituacaoParticipacao;
	}

	/**
	 * Set: dsSituacaoParticipacao.
	 *
	 * @param dsSituacaoParticipacao the ds situacao participacao
	 */
	public void setDsSituacaoParticipacao(String dsSituacaoParticipacao) {
		this.dsSituacaoParticipacao = dsSituacaoParticipacao;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Get: dsTipoNomeRazao.
	 *
	 * @return dsTipoNomeRazao
	 */
	public String getDsTipoNomeRazao() {
		return dsTipoNomeRazao;
	}

	/**
	 * Set: dsTipoNomeRazao.
	 *
	 * @param dsTipoNomeRazao the ds tipo nome razao
	 */
	public void setDsTipoNomeRazao(String dsTipoNomeRazao) {
		this.dsTipoNomeRazao = dsTipoNomeRazao;
	}

	/**
	 * Get: dsTipoParticipacao.
	 *
	 * @return dsTipoParticipacao
	 */
	public String getDsTipoParticipacao() {
		return dsTipoParticipacao;
	}

	/**
	 * Set: dsTipoParticipacao.
	 *
	 * @param dsTipoParticipacao the ds tipo participacao
	 */
	public void setDsTipoParticipacao(String dsTipoParticipacao) {
		this.dsTipoParticipacao = dsTipoParticipacao;
	}	
	//FIM HISTORICO THIAGO

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: tela.
	 *
	 * @return tela
	 */
	public String getTela() {
		return tela;
	}

	/**
	 * Set: tela.
	 *
	 * @param tela the tela
	 */
	public void setTela(String tela) {
		this.tela = tela;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: habilitaBtoCont.
	 *
	 * @return habilitaBtoCont
	 */
	public Boolean getHabilitaBtoCont() {
		return habilitaBtoCont;
	}

	/**
	 * Set: habilitaBtoCont.
	 *
	 * @param habilitaBtoCont the habilita bto cont
	 */
	public void setHabilitaBtoCont(Boolean habilitaBtoCont) {
		this.habilitaBtoCont = habilitaBtoCont;
	}

	/**
	 * Get: foco.
	 *
	 * @return foco
	 */
	public String getFoco() {
		return foco;
	}

	/**
	 * Set: foco.
	 *
	 * @param foco the foco
	 */
	public void setFoco(String foco) {
		this.foco = foco;
	}

	/**
	 * Get: radioArgPesquisa.
	 *
	 * @return radioArgPesquisa
	 */
	public String getRadioArgPesquisa() {
		return radioArgPesquisa;
	}

	/**
	 * Set: radioArgPesquisa.
	 *
	 * @param radioArgPesquisa the radio arg pesquisa
	 */
	public void setRadioArgPesquisa(String radioArgPesquisa) {
		this.radioArgPesquisa = radioArgPesquisa;
	}

	/**
	 * Get: cpfCnpjRepresentante.
	 *
	 * @return cpfCnpjRepresentante
	 */
	public String getCpfCnpjRepresentante() {
		return cpfCnpjRepresentante;
	}

	/**
	 * Set: cpfCnpjRepresentante.
	 *
	 * @param cpfCnpjRepresentante the cpf cnpj representante
	 */
	public void setCpfCnpjRepresentante(String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	/**
	 * Get: nomeRazaoRepresentante.
	 *
	 * @return nomeRazaoRepresentante
	 */
	public String getNomeRazaoRepresentante() {
		return nomeRazaoRepresentante;
	}

	/**
	 * Set: nomeRazaoRepresentante.
	 *
	 * @param nomeRazaoRepresentante the nome razao representante
	 */
	public void setNomeRazaoRepresentante(String nomeRazaoRepresentante) {
		this.nomeRazaoRepresentante = nomeRazaoRepresentante;
	}

	/**
	 * Get: grupEconRepresentante.
	 *
	 * @return grupEconRepresentante
	 */
	public String getGrupEconRepresentante() {
		return grupEconRepresentante;
	}

	/**
	 * Set: grupEconRepresentante.
	 *
	 * @param grupEconRepresentante the grup econ representante
	 */
	public void setGrupEconRepresentante(String grupEconRepresentante) {
		this.grupEconRepresentante = grupEconRepresentante;
	}

	/**
	 * Get: ativEconRepresentante.
	 *
	 * @return ativEconRepresentante
	 */
	public String getAtivEconRepresentante() {
		return ativEconRepresentante;
	}

	/**
	 * Set: ativEconRepresentante.
	 *
	 * @param ativEconRepresentante the ativ econ representante
	 */
	public void setAtivEconRepresentante(String ativEconRepresentante) {
		this.ativEconRepresentante = ativEconRepresentante;
	}

	/**
	 * Get: segRepresentante.
	 *
	 * @return segRepresentante
	 */
	public String getSegRepresentante() {
		return segRepresentante;
	}

	/**
	 * Set: segRepresentante.
	 *
	 * @param segRepresentante the seg representante
	 */
	public void setSegRepresentante(String segRepresentante) {
		this.segRepresentante = segRepresentante;
	}

	/**
	 * Get: subSegRepresentante.
	 *
	 * @return subSegRepresentante
	 */
	public String getSubSegRepresentante() {
		return subSegRepresentante;
	}

	/**
	 * Set: subSegRepresentante.
	 *
	 * @param subSegRepresentante the sub seg representante
	 */
	public void setSubSegRepresentante(String subSegRepresentante) {
		this.subSegRepresentante = subSegRepresentante;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: gerenteResponsavelFormatado.
	 *
	 * @return gerenteResponsavelFormatado
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}

	/**
	 * Set: gerenteResponsavelFormatado.
	 *
	 * @param gerenteResponsavelFormatado the gerente responsavel formatado
	 */
	public void setGerenteResponsavelFormatado(String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: cpfCnpjParticipanteTemp.
	 *
	 * @return cpfCnpjParticipanteTemp
	 */
	public String getCpfCnpjParticipanteTemp() {
		return cpfCnpjParticipanteTemp;
	}

	/**
	 * Set: cpfCnpjParticipanteTemp.
	 *
	 * @param cpfCnpjParticipanteTemp the cpf cnpj participante temp
	 */
	public void setCpfCnpjParticipanteTemp(String cpfCnpjParticipanteTemp) {
		this.cpfCnpjParticipanteTemp = cpfCnpjParticipanteTemp;
	}

	/**
	 * Get: nomeRazaoParticipanteTemp.
	 *
	 * @return nomeRazaoParticipanteTemp
	 */
	public String getNomeRazaoParticipanteTemp() {
		return nomeRazaoParticipanteTemp;
	}

	/**
	 * Set: nomeRazaoParticipanteTemp.
	 *
	 * @param nomeRazaoParticipanteTemp the nome razao participante temp
	 */
	public void setNomeRazaoParticipanteTemp(String nomeRazaoParticipanteTemp) {
		this.nomeRazaoParticipanteTemp = nomeRazaoParticipanteTemp;
	}

	/**
	 * Get: cpfCnpjParticipanteDet.
	 *
	 * @return cpfCnpjParticipanteDet
	 */
	public String getCpfCnpjParticipanteDet() {
		return cpfCnpjParticipanteDet;
	}

	/**
	 * Set: cpfCnpjParticipanteDet.
	 *
	 * @param cpfCnpjParticipanteDet the cpf cnpj participante det
	 */
	public void setCpfCnpjParticipanteDet(String cpfCnpjParticipanteDet) {
		this.cpfCnpjParticipanteDet = cpfCnpjParticipanteDet;
	}

	/**
	 * Get: nomeRazaoParticipanteDet.
	 *
	 * @return nomeRazaoParticipanteDet
	 */
	public String getNomeRazaoParticipanteDet() {
		return nomeRazaoParticipanteDet;
	}

	/**
	 * Set: nomeRazaoParticipanteDet.
	 *
	 * @param nomeRazaoParticipanteDet the nome razao participante det
	 */
	public void setNomeRazaoParticipanteDet(String nomeRazaoParticipanteDet) {
		this.nomeRazaoParticipanteDet = nomeRazaoParticipanteDet;
	}

	/**
	 * Get: grupoEconParticipante.
	 *
	 * @return grupoEconParticipante
	 */
	public String getGrupoEconParticipante() {
		return grupoEconParticipante;
	}

	/**
	 * Set: grupoEconParticipante.
	 *
	 * @param grupoEconParticipante the grupo econ participante
	 */
	public void setGrupoEconParticipante(String grupoEconParticipante) {
		this.grupoEconParticipante = grupoEconParticipante;
	}

	/**
	 * Get: atividadeEconParticipante.
	 *
	 * @return atividadeEconParticipante
	 */
	public String getAtividadeEconParticipante() {
		return atividadeEconParticipante;
	}

	/**
	 * Set: atividadeEconParticipante.
	 *
	 * @param atividadeEconParticipante the atividade econ participante
	 */
	public void setAtividadeEconParticipante(String atividadeEconParticipante) {
		this.atividadeEconParticipante = atividadeEconParticipante;
	}

	/**
	 * Get: segmentoParticipante.
	 *
	 * @return segmentoParticipante
	 */
	public String getSegmentoParticipante() {
		return segmentoParticipante;
	}

	/**
	 * Set: segmentoParticipante.
	 *
	 * @param segmentoParticipante the segmento participante
	 */
	public void setSegmentoParticipante(String segmentoParticipante) {
		this.segmentoParticipante = segmentoParticipante;
	}

	/**
	 * Get: subSegmentoParticipante.
	 *
	 * @return subSegmentoParticipante
	 */
	public String getSubSegmentoParticipante() {
		return subSegmentoParticipante;
	}

	/**
	 * Set: subSegmentoParticipante.
	 *
	 * @param subSegmentoParticipante the sub segmento participante
	 */
	public void setSubSegmentoParticipante(String subSegmentoParticipante) {
		this.subSegmentoParticipante = subSegmentoParticipante;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: consultarListaPerfilTrocaArquivoSaida.
	 *
	 * @return consultarListaPerfilTrocaArquivoSaida
	 */
	public ConsultarListaPerfilTrocaArquivoSaidaDTO getConsultarListaPerfilTrocaArquivoSaida() {
		return consultarListaPerfilTrocaArquivoSaida;
	}

	/**
	 * Set: consultarListaPerfilTrocaArquivoSaida.
	 *
	 * @param consultarListaPerfilTrocaArquivoSaida the consultar lista perfil troca arquivo saida
	 */
	public void setConsultarListaPerfilTrocaArquivoSaida(
			ConsultarListaPerfilTrocaArquivoSaidaDTO consultarListaPerfilTrocaArquivoSaida) {
		this.consultarListaPerfilTrocaArquivoSaida = consultarListaPerfilTrocaArquivoSaida;
	}

	/**
	 * Get: listaContrato.
	 *
	 * @return listaContrato
	 */
	public ListarContratosPgitSaidaDTO getListaContrato() {
		return listaContrato;
	}

	/**
	 * Set: listaContrato.
	 *
	 * @param listaContrato the lista contrato
	 */
	public void setListaContrato(ListarContratosPgitSaidaDTO listaContrato) {
		this.listaContrato = listaContrato;
	}

	/**
	 * Get: listaGridPerfil.
	 *
	 * @return listaGridPerfil
	 */
	public List<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO> getListaGridPerfil() {
		return listaGridPerfil;
	}

	/**
	 * Set: listaGridPerfil.
	 *
	 * @param listaGridPerfil the lista grid perfil
	 */
	public void setListaGridPerfil(
			List<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO> listaGridPerfil) {
		this.listaGridPerfil = listaGridPerfil;
	}

	/**
	 * Nome: setSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @param saidaTipoLayoutArquivoSaidaDTO
	 */
	public void setSaidaTipoLayoutArquivoSaidaDTO(
			TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
		this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: getSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @return saidaTipoLayoutArquivoSaidaDTO
	 */
	public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
		return saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * @return the cdContratoMae
	 */
	public Long getCdContratoMae() {
		return cdContratoMae;
	}

	/**
	 * @param cdContratoMae the cdContratoMae to set
	 */
	public void setCdContratoMae(Long cdContratoMae) {
		this.cdContratoMae = cdContratoMae;
	}

	/**
	 * @param itemSelecionadoContratoMae the itemSelecionadoContratoMae to set
	 */
	public void setItemSelecionadoContratoMae(Integer itemSelecionadoContratoMae) {
		this.itemSelecionadoContratoMae = itemSelecionadoContratoMae;
	}

	/**
	 * @return the listaControleContratoMae
	 */
	public List<SelectItem> getListaControleContratoMae() {
		return listaControleContratoMae;
	}

	/**
	 * @param listaControleContratoMae the listaControleContratoMae to set
	 */
	public void setListaControleContratoMae(
			List<SelectItem> listaControleContratoMae) {
		this.listaControleContratoMae = listaControleContratoMae;
	}

	/**
	 * @return the cdCpfCnpjContratoMae
	 */
	public String getCdCpfCnpjContratoMae() {
		return cdCpfCnpjContratoMae;
	}

	/**
	 * @param cdCpfCnpjContratoMae the cdCpfCnpjContratoMae to set
	 */
	public void setCdCpfCnpjContratoMae(String cdCpfCnpjContratoMae) {
		this.cdCpfCnpjContratoMae = cdCpfCnpjContratoMae;
	}

	/**
	 * @return the dsRazaoSocialContratoMae
	 */
	public String getDsRazaoSocialContratoMae() {
		return dsRazaoSocialContratoMae;
	}

	/**
	 * @param dsRazaoSocialContratoMae the dsRazaoSocialContratoMae to set
	 */
	public void setDsRazaoSocialContratoMae(String dsRazaoSocialContratoMae) {
		this.dsRazaoSocialContratoMae = dsRazaoSocialContratoMae;
	}

	/**
	 * @return the cdPerfilContratoMae
	 */
	public Long getCdPerfilContratoMae() {
		return cdPerfilContratoMae;
	}

	/**
	 * @param cdPerfilContratoMae the cdPerfilContratoMae to set
	 */
	public void setCdPerfilContratoMae(Long cdPerfilContratoMae) {
		this.cdPerfilContratoMae = cdPerfilContratoMae;
	}

	/**
	 * @return the dsTipoLayoutArquivoContratoMae
	 */
	public String getDsTipoLayoutArquivoContratoMae() {
		return dsTipoLayoutArquivoContratoMae;
	}

	/**
	 * @param dsTipoLayoutArquivoContratoMae the dsTipoLayoutArquivoContratoMae to set
	 */
	public void setDsTipoLayoutArquivoContratoMae(
			String dsTipoLayoutArquivoContratoMae) {
		this.dsTipoLayoutArquivoContratoMae = dsTipoLayoutArquivoContratoMae;
	}

	/**
	 * @return the saidaContratoMae
	 */
	public ListarContratoMaeSaidaDTO getSaidaContratoMae() {
		return saidaContratoMae;
	}

	/**
	 * @param saidaContratoMae the saidaContratoMae to set
	 */
	public void setSaidaContratoMae(ListarContratoMaeSaidaDTO saidaContratoMae) {
		this.saidaContratoMae = saidaContratoMae;
	}

	/**
	 * @return the itemSelecionadoContratoMae
	 */
	public Integer getItemSelecionadoContratoMae() {
		return itemSelecionadoContratoMae;
	}

	/**
	 * @return the dsContratoMae
	 */
	public String getDsContratoMae() {
		return dsContratoMae;
	}

	/**
	 * @param dsContratoMae the dsContratoMae to set
	 */
	public void setDsContratoMae(String dsContratoMae) {
		this.dsContratoMae = dsContratoMae;
	}

	/**
	 * @return the cdTipoLayoutMae
	 */
	public Integer getCdTipoLayoutMae() {
		return cdTipoLayoutMae;
	}

	/**
	 * @param cdTipoLayoutMae the cdTipoLayoutMae to set
	 */
	public void setCdTipoLayoutMae(Integer cdTipoLayoutMae) {
		this.cdTipoLayoutMae = cdTipoLayoutMae;
	}

	/**
	 * @return the cdPerfilTrocaMae
	 */
	public Long getCdPerfilTrocaMae() {
		return cdPerfilTrocaMae;
	}

	/**
	 * @param cdPerfilTrocaMae the cdPerfilTrocaMae to set
	 */
	public void setCdPerfilTrocaMae(Long cdPerfilTrocaMae) {
		this.cdPerfilTrocaMae = cdPerfilTrocaMae;
	}

	/**
	 * @return the cdPessoaJuridicaMae
	 */
	public Long getCdPessoaJuridicaMae() {
		return cdPessoaJuridicaMae;
	}

	/**
	 * @param cdPessoaJuridicaMae the cdPessoaJuridicaMae to set
	 */
	public void setCdPessoaJuridicaMae(Long cdPessoaJuridicaMae) {
		this.cdPessoaJuridicaMae = cdPessoaJuridicaMae;
	}

	/**
	 * @return the cdTipoContratoMae
	 */
	public Integer getCdTipoContratoMae() {
		return cdTipoContratoMae;
	}

	/**
	 * @param cdTipoContratoMae the cdTipoContratoMae to set
	 */
	public void setCdTipoContratoMae(Integer cdTipoContratoMae) {
		this.cdTipoContratoMae = cdTipoContratoMae;
	}

	/**
	 * @return the nrSequencialContratoMae
	 */
	public Long getNrSequencialContratoMae() {
		return nrSequencialContratoMae;
	}

	/**
	 * @param nrSequencialContratoMae the nrSequencialContratoMae to set
	 */
	public void setNrSequencialContratoMae(Long nrSequencialContratoMae) {
		this.nrSequencialContratoMae = nrSequencialContratoMae;
	}

	/**
	 * @return the cdTipoParticipanteMae
	 */
	public Integer getCdTipoParticipanteMae() {
		return cdTipoParticipanteMae;
	}

	/**
	 * @param cdTipoParticipanteMae the cdTipoParticipanteMae to set
	 */
	public void setCdTipoParticipanteMae(Integer cdTipoParticipanteMae) {
		this.cdTipoParticipanteMae = cdTipoParticipanteMae;
	}

	/**
	 * @return the cdCpessoaMae
	 */
	public Long getCdCpessoaMae() {
		return cdCpessoaMae;
	}

	/**
	 * @param cdCpessoaMae the cdCpessoaMae to set
	 */
	public void setCdCpessoaMae(Long cdCpessoaMae) {
		this.cdCpessoaMae = cdCpessoaMae;
	}

	/**
	 * @return the cdCpfCnpj
	 */
	public Long getCdCpfCnpj() {
		return cdCpfCnpj;
	}

	/**
	 * @param cdCpfCnpj the cdCpfCnpj to set
	 */
	public void setCdCpfCnpj(Long cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}

	/**
	 * @return the cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}

	/**
	 * @param cdFilialCnpj the cdFilialCnpj to set
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}

	/**
	 * @return the cdControleCnpj
	 */
	public Integer getCdControleCnpj() {
		return cdControleCnpj;
	}

	/**
	 * @param cdControleCnpj the cdControleCnpj to set
	 */
	public void setCdControleCnpj(Integer cdControleCnpj) {
		this.cdControleCnpj = cdControleCnpj;
	}

	/**
	 * @return the cdCpfCnpjMae
	 */
	public Long getCdCpfCnpjMae() {
		return cdCpfCnpjMae;
	}

	/**
	 * @param cdCpfCnpjMae the cdCpfCnpjMae to set
	 */
	public void setCdCpfCnpjMae(Long cdCpfCnpjMae) {
		this.cdCpfCnpjMae = cdCpfCnpjMae;
	}

	/**
	 * @return the cdCnpjFilialMae
	 */
	public Integer getCdCnpjFilialMae() {
		return cdCnpjFilialMae;
	}

	/**
	 * @param cdCnpjFilialMae the cdCnpjFilialMae to set
	 */
	public void setCdCnpjFilialMae(Integer cdCnpjFilialMae) {
		this.cdCnpjFilialMae = cdCnpjFilialMae;
	}

	/**
	 * @return the cdDigitoCpfCnpjMae
	 */
	public String getCdDigitoCpfCnpjMae() {
		return cdDigitoCpfCnpjMae;
	}

	/**
	 * @param cdDigitoCpfCnpjMae the cdDigitoCpfCnpjMae to set
	 */
	public void setCdDigitoCpfCnpjMae(String cdDigitoCpfCnpjMae) {
		this.cdDigitoCpfCnpjMae = cdDigitoCpfCnpjMae;
	}

	public String getDsCodigoTipoLayout() {
		return dsCodigoTipoLayout;
	}

	public void setDsCodigoTipoLayout(String dsCodigoTipoLayout) {
		this.dsCodigoTipoLayout = dsCodigoTipoLayout;
	}

	public List<ListarContratoMaeOcorrenciasSaidaDTO> getListaContratoMae() {
		return listaContratoMae;
	}

	public void setListaContratoMae(
			List<ListarContratoMaeOcorrenciasSaidaDTO> listaContratoMae) {
		this.listaContratoMae = listaContratoMae;
	}

	public String getCpfCnpjFormatadoContMae() {
		return cpfCnpjFormatadoContMae;
	}

	public void setCpfCnpjFormatadoContMae(String cpfCnpjFormatadoContMae) {
		this.cpfCnpjFormatadoContMae = cpfCnpjFormatadoContMae;
	}

	public String getDsNomeRazaoContMae() {
		return dsNomeRazaoContMae;
	}

	public void setDsNomeRazaoContMae(String dsNomeRazaoContMae) {
		this.dsNomeRazaoContMae = dsNomeRazaoContMae;
	}

	public Long getCdPerfilTrocaArquivoContMae() {
		return cdPerfilTrocaArquivoContMae;
	}

	public void setCdPerfilTrocaArquivoContMae(Long cdPerfilTrocaArquivoContMae) {
		this.cdPerfilTrocaArquivoContMae = cdPerfilTrocaArquivoContMae;
	}

	public String getDsTipoLayoutArquivoContMae() {
		return dsTipoLayoutArquivoContMae;
	}

	public void setDsTipoLayoutArquivoContMae(String dsTipoLayoutArquivoContMae) {
		this.dsTipoLayoutArquivoContMae = dsTipoLayoutArquivoContMae;
	}

	public Integer getCdTipoParticipacaoPessoa() {
		return cdTipoParticipacaoPessoa;
	}

	public void setCdTipoParticipacaoPessoa(Integer cdTipoParticipacaoPessoa) {
		this.cdTipoParticipacaoPessoa = cdTipoParticipacaoPessoa;
	}

	public IncluirVincContMaeFilhoSaidaDTO getSaidaIncVincContratoMaeFilho() {
		return saidaIncVincContratoMaeFilho;
	}

	public void setSaidaIncVincContratoMaeFilho(
			IncluirVincContMaeFilhoSaidaDTO saidaIncVincContratoMaeFilho) {
		this.saidaIncVincContratoMaeFilho = saidaIncVincContratoMaeFilho;
	}

	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	public Long getCdPessoaContMae() {
		return cdPessoaContMae;
	}

	public void setCdPessoaContMae(Long cdPessoaContMae) {
		this.cdPessoaContMae = cdPessoaContMae;
	}

	public ListarContratoMaeEntradaDTO getEntradaListaContratoMae() {
		return entradaListaContratoMae;
	}

	public void setEntradaListaContratoMae(
			ListarContratoMaeEntradaDTO entradaListaContratoMae) {
		this.entradaListaContratoMae = entradaListaContratoMae;
	}

	public DetalharVincTrocaArqParticipanteEntradaDTO getEntradaDetalharvincTrocaArq() {
		return entradaDetalharvincTrocaArq;
	}

	public void setEntradaDetalharvincTrocaArq(
			DetalharVincTrocaArqParticipanteEntradaDTO entradaDetalharvincTrocaArq) {
		this.entradaDetalharvincTrocaArq = entradaDetalharvincTrocaArq;
	}
}