/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.finalidadeendprodutoserv
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.finalidadeendprodutoserv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarFinalidadeEnderecoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOperacoesServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOperacoesServicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.IFinalidadeEndProdutoServService;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.DetalharFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.DetalharFinalidadeEndOpeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ExcluirFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ExcluirFinalidadeEndOpeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.IncluirFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.IncluirFinalidadeEndOpeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ListarFinalidadeEndOpeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.finalidadeendprodutoserv.bean.ListarFinalidadeEndOpeSaidaDTO;

/**
 * Nome: FinalidadeEndProdutoServBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FinalidadeEndProdutoServBean {
	
	//Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_FINALIDADE_ENDERECO_PRODUTO_OPERACAO. */
	private static final String CON_FINALIDADE_ENDERECO_PRODUTO_OPERACAO = "conFinalidadeEnderecoProdutoOperacao";
	
	/** Atributo finalidadeEnderecoFiltro. */
	private Integer finalidadeEnderecoFiltro;
	
	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;
	
	/** Atributo modalidadeServicoFiltro. */
	private Integer modalidadeServicoFiltro;
	
	/** Atributo operacaoFiltro. */
	private Integer operacaoFiltro;
	
	/** Atributo incFinalidadeEndereco. */
	private Integer incFinalidadeEndereco;
	
	/** Atributo incTipoServico. */
	private Integer incTipoServico;
	
	/** Atributo incModalidadeServico. */
	private Integer incModalidadeServico;
	
	/** Atributo incOperacao. */
	private Integer incOperacao;
	
	/** Atributo finalidadeEndereco. */
	private String finalidadeEndereco;
	
	/** Atributo tipoServico. */
	private String tipoServico;
	
	/** Atributo modalidadeServico. */
	private String modalidadeServico;
	
	/** Atributo operacao. */
	private String operacao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo listaFinalidadeEndereco. */
	private List<SelectItem> listaFinalidadeEndereco;
	
	/** Atributo listaFinalidadeEnderecoHash. */
	private Map<Integer, String> listaFinalidadeEnderecoHash = new HashMap<Integer, String>();
	
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico;
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();
	
	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico;
	
	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo listaOperacao. */
	private List<SelectItem> listaOperacao = new ArrayList<SelectItem>();
	
	/** Atributo listaOperacaoHash. */
	private Map<Integer, ListarOperacoesServicoSaidaDTO> listaOperacaoHash = new HashMap<Integer, ListarOperacoesServicoSaidaDTO>();
		
	/** Atributo listaModalidadeServicoIncluir. */
	private List<SelectItem> listaModalidadeServicoIncluir;
	
	/** Atributo listaModalidadeServicoIncluirHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo listaOperacaoIncluir. */
	private List<SelectItem> listaOperacaoIncluir;
	
	/** Atributo listaOperacaoIncluirHash. */
	private Map<Integer, ListarOperacoesServicoSaidaDTO> listaOperacaoIncluirHash = new HashMap<Integer, ListarOperacoesServicoSaidaDTO> ();
	
	/** Atributo listaGrid. */
	private List<ListarFinalidadeEndOpeSaidaDTO> listaGrid;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo finalidadeEndProdutoServService. */
	private IFinalidadeEndProdutoServService finalidadeEndProdutoServService;
	
	/** Atributo hiddenObrigatoriedade. */
	private String hiddenObrigatoriedade;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	//METODOS ********************************************************************************
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR";
	}
	
	/**
	 * Carrega lista tipo servico.
	 */
	public void carregaListaTipoServico(){		
		try{
			
			this.listaTipoServico = new ArrayList<SelectItem>();
			
			List<ListarServicosSaidaDTO> listaServico = new ArrayList<ListarServicosSaidaDTO>();
			
			listaServico = comboService.listarTipoServicos(0);
			
			listaTipoServicoHash.clear();
			for(ListarServicosSaidaDTO combo : listaServico){
				listaTipoServicoHash.put(combo.getCdServico(), combo.getDsServico());
				listaTipoServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
			}
			
		} catch (PdcAdapterException p) {
			FacesContext.getCurrentInstance().addMessage("erro", new FacesMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage()));
			this.setListaTipoServico(new ArrayList<SelectItem>());
		} 
	}
	
	/**
	 * Carrega lista finalidade end.
	 */
	public void carregaListaFinalidadeEnd(){
		
		try{
			
			this.listaFinalidadeEndereco = new ArrayList<SelectItem>();
			
			List<ListarFinalidadeEnderecoSaidaDTO> listaFinalidadeEnderecoAux = new ArrayList<ListarFinalidadeEnderecoSaidaDTO>();
			
			listaFinalidadeEnderecoAux = comboService.listarFinalidadeEndereco();
			
			listaFinalidadeEnderecoHash.clear();
			for(ListarFinalidadeEnderecoSaidaDTO combo : listaFinalidadeEnderecoAux){
				listaFinalidadeEnderecoHash.put(combo.getCdFinalidadeEndereco(),combo.getDsFinalidadeEndereco());
				this.listaFinalidadeEndereco.add(new SelectItem(combo.getCdFinalidadeEndereco(),combo.getDsFinalidadeEndereco()));
			}
			
		} catch (PdcAdapterException p) {
			FacesContext.getCurrentInstance().addMessage("erro", new FacesMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage()));
			this.setListaFinalidadeEndereco(new ArrayList<SelectItem>());
		} 	
	}
	
	/**
	 * Carrega combos.
	 *
	 * @return the string
	 */
	public String carregaCombos(){
		carregaListaModalidade();
		carregaListaOperacao();
		return "";
	}

	/**
	 * Carrega lista incluir.
	 */
	public void carregaListaIncluir(){
		carregaListaModalidadeIncluir();
		carregaListaOperacaoIncluir();
	}
	
	/**
	 * Carrega lista modalidade.
	 */
	public void carregaListaModalidade(){
		try{
			listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
			listaModalidadeServico = new ArrayList<SelectItem>();
			
			if( getTipoServicoFiltro()!=null && getTipoServicoFiltro() != 0){
	
				List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
				ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
				
				listarModalidadeEntradaDTO.setCdServico(getTipoServicoFiltro());
			
				listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
				
				listaModalidadeServicoHash.clear();
				
				for(ListarModalidadeSaidaDTO combo : listaModalidades){				
					this.listaModalidadeServico.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
					listaModalidadeServicoHash.put(combo.getCdModalidade(),combo);
					
				}
			}else{
				listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
				listaModalidadeServico.clear();
				setModalidadeServicoFiltro(0);
				setTipoServicoFiltro(0);
			}
		}catch(PdcAdapterFunctionalException e){
			listaModalidadeServico = new ArrayList<SelectItem>();
		}
		
	}
	
	/**
	 * Carrega lista modalidade incluir.
	 */
	public void carregaListaModalidadeIncluir(){
		listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
		listaModalidadeServicoIncluir = new ArrayList<SelectItem>();
		
		try{
			if( getIncTipoServico()!=null && getIncTipoServico() != 0){
				List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
				ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
				
				listarModalidadeEntradaDTO.setCdServico(getIncTipoServico());
			
				listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
				
				listaModalidadeServicoIncluirHash.clear();
				
				for(ListarModalidadeSaidaDTO combo : listaModalidades){				
					this.listaModalidadeServicoIncluir.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
					listaModalidadeServicoIncluirHash.put(combo.getCdModalidade(),combo);
					
				}
			}else{
				listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
				listaModalidadeServicoIncluir.clear();
				setIncModalidadeServico(0);
				setIncTipoServico(0);
			}
		}catch(PdcAdapterFunctionalException e){
			listaModalidadeServicoIncluir = new ArrayList<SelectItem>();
		}
	}
	
	/**
	 * Carrega lista operacao.
	 */
	public void carregaListaOperacao(){
		try{
			listaOperacaoHash = new HashMap<Integer, ListarOperacoesServicoSaidaDTO> (); 
			listaOperacao = new ArrayList<SelectItem>();
			
			if(getTipoServicoFiltro()!=null && getTipoServicoFiltro() != 0){
				
				List<ListarOperacoesServicoSaidaDTO> listaSaidaDTO = new ArrayList<ListarOperacoesServicoSaidaDTO>();
				ListarOperacoesServicosEntradaDTO entradaDTO = new ListarOperacoesServicosEntradaDTO();
				
				entradaDTO.setCdProduto(getTipoServicoFiltro());
			
				listaSaidaDTO = comboService.listarOperacoesServicos(entradaDTO);
				
				listaOperacaoHash.clear();
				
				for(ListarOperacoesServicoSaidaDTO combo : listaSaidaDTO){				
					listaOperacao.add(new SelectItem(combo.getCdOperacao(),combo.getDsOperacao()));
					listaOperacaoHash.put(combo.getCdOperacao(),combo);
					
				}
			}else{
				listaOperacaoHash = new HashMap<Integer, ListarOperacoesServicoSaidaDTO> ();
				listaOperacao.clear();
				setOperacaoFiltro(0);
				setTipoServicoFiltro(0);
			}
		}catch(PdcAdapterFunctionalException e){
			listaOperacao = new ArrayList<SelectItem>();
		}
	}
	
	/**
	 * Carrega lista operacao incluir.
	 */
	public void carregaListaOperacaoIncluir(){
		listaOperacaoIncluirHash = new HashMap<Integer, ListarOperacoesServicoSaidaDTO> (); 
		listaOperacaoIncluir = new ArrayList<SelectItem>();
		
		try{
			if( getIncTipoServico()!=null && getIncTipoServico() != 0){
				List<ListarOperacoesServicoSaidaDTO> listaOperacaoSaida = new ArrayList<ListarOperacoesServicoSaidaDTO>();
				ListarOperacoesServicosEntradaDTO listaOperacaoEntrada = new ListarOperacoesServicosEntradaDTO();
				
				listaOperacaoEntrada.setCdProduto(getIncTipoServico());
			
				listaOperacaoSaida = comboService.listarOperacoesServicos(listaOperacaoEntrada);
				
				listaOperacaoIncluirHash.clear();
				
				for(ListarOperacoesServicoSaidaDTO combo : listaOperacaoSaida){				
					this.listaOperacaoIncluir.add(new SelectItem(combo.getCdOperacao(),combo.getDsOperacao()));
					listaOperacaoIncluirHash.put(combo.getCdOperacao(),combo);
					
				}
			}else{
				listaOperacaoIncluirHash = new HashMap<Integer, ListarOperacoesServicoSaidaDTO> ();
				listaOperacaoIncluir.clear();
				setIncOperacao(0);
				setIncTipoServico(0);
			}
		}catch(PdcAdapterFunctionalException e){
			listaOperacaoIncluir = new ArrayList<SelectItem>();
		}		
	}
	
	/**
	 * Consultar.
	 */
	public void consultar(){
		try{
			
			ListarFinalidadeEndOpeEntradaDTO entradaDTO = new ListarFinalidadeEndOpeEntradaDTO();
			
			entradaDTO.setTipoServico(getTipoServicoFiltro() != null?getTipoServicoFiltro():0);
			entradaDTO.setModalidadeServico(getModalidadeServicoFiltro() == null ? 0 : getModalidadeServicoFiltro());
			entradaDTO.setFinalidadeEndereco(getFinalidadeEnderecoFiltro()!=null ? getFinalidadeEnderecoFiltro():0);
			entradaDTO.setTipoRelacionamento(getModalidadeServicoFiltro() == null || getModalidadeServicoFiltro() == 0 || listaModalidadeServicoHash.isEmpty()? 0 :listaModalidadeServicoHash.get(getModalidadeServicoFiltro()).getCdRelacionamentoProduto());
			entradaDTO.setCdOperacaoProdutoServico(getOperacaoFiltro());
			
			setListaGrid(getFinalidadeEndProdutoServService().listarFinalidadeEnderecoOperacao(entradaDTO));
			setBtoAcionado(true);
			listaControle = new ArrayList<SelectItem>();
			
			for(int i = 0; i < listaGrid.size(); i++){
				listaControle.add(new SelectItem(i,""));
			}
			
			setItemSelecionadoLista(null);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);

		}
		
		
	}
	
	/**
	 * Limpar dados.
	 */
	public void limparDados(){
		
		this.setFinalidadeEnderecoFiltro(0);
		this.setTipoServicoFiltro(0);
		this.setModalidadeServicoFiltro(0);
		setOperacaoFiltro(0);
		carregaListaModalidade();
		carregaListaOperacao();
		setListaGrid(null);
		setItemSelecionadoLista(null);
		setBtoAcionado(false);
		
	}
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt){
		
		carregaListaFinalidadeEnd();		
		carregaListaTipoServico();
		carregaListaModalidade();
		carregaListaOperacao();
		
		this.setFinalidadeEnderecoFiltro(0);
		this.setTipoServicoFiltro(0);
		this.setModalidadeServicoFiltro(0);
		setOperacaoFiltro(0);
		
		
		this.setListaControle(null);
		this.setListaGrid(null);
		this.setItemSelecionadoLista(null);		
		setBtoAcionado(false);
	}
	
	/**
	 * Limpar grid.
	 */
	public void limparGrid(){
		this.setListaControle(null);
		this.setListaGrid(null);
		this.setItemSelecionadoLista(null);
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){

		try {
			carregaVariaveis();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_FINALIDADE_ENDERECO_PRODUTO_OPERACAO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		
		return "DETALHAR";
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){	
		setIncFinalidadeEndereco(0);
		setIncModalidadeServico(0);
		setIncTipoServico(0);
		setIncOperacao(0);
		carregaListaModalidadeIncluir();
		carregaListaOperacaoIncluir();
		return "INCLUIR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try {
			carregaVariaveis();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_FINALIDADE_ENDERECO_PRODUTO_OPERACAO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "EXCLUIR";
	}
	
	/**
	 * Avancar.
	 *
	 * @return the string
	 */
	public String avancar(){
		if (getHiddenObrigatoriedade() != null && getHiddenObrigatoriedade().equals("T")){
			setFinalidadeEndereco((String)listaFinalidadeEnderecoHash.get(getIncFinalidadeEndereco()));
			setTipoServico((String) listaTipoServicoHash.get(getIncTipoServico()));
			setModalidadeServico(getIncModalidadeServico() == 0 ? "" : listaModalidadeServicoIncluirHash.get(getIncModalidadeServico()).getDsModalidade());
			setOperacao(getIncOperacao() == 0 ? "" : listaOperacaoIncluirHash.get(getIncOperacao()).getDsOperacao());
			return "AVANCAR";
		}
		return "";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		
		try {
			
			IncluirFinalidadeEndOpeEntradaDTO incluirFinalidadeEndOpeEntradaDTO = new IncluirFinalidadeEndOpeEntradaDTO();
			
			incluirFinalidadeEndOpeEntradaDTO.setTipoServico(getIncTipoServico());
			incluirFinalidadeEndOpeEntradaDTO.setModalidadeServico(getIncModalidadeServico());
			incluirFinalidadeEndOpeEntradaDTO.setTipoRelacionamento(getIncModalidadeServico() == 0 ? 0 : listaModalidadeServicoIncluirHash.get(getIncModalidadeServico()).getCdRelacionamentoProduto());
			incluirFinalidadeEndOpeEntradaDTO.setFinalidadeEndereco(getIncFinalidadeEndereco());
			incluirFinalidadeEndOpeEntradaDTO.setCdOperacaoProdutoServico(getIncOperacao());
			
			IncluirFinalidadeEndOpeSaidaDTO incluirFinalidadeEndOpeSaidaDTO = getFinalidadeEndProdutoServService().incluirFinalidadeEnderecoOperacao(incluirFinalidadeEndOpeEntradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +incluirFinalidadeEndOpeSaidaDTO.getCodMensagem() + ") " + incluirFinalidadeEndOpeSaidaDTO.getMensagem(), CON_FINALIDADE_ENDERECO_PRODUTO_OPERACAO, BradescoViewExceptionActionType.ACTION, false);			
			
			if(getListaGrid() != null){
				consultar();
			}
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
		
	}
	
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		
		try {
			
			ListarFinalidadeEndOpeSaidaDTO listarFinalidadeEndOpeSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
			
			ExcluirFinalidadeEndOpeEntradaDTO excluirFinalidadeEndOpeEntradaDTO = new ExcluirFinalidadeEndOpeEntradaDTO();
			
			excluirFinalidadeEndOpeEntradaDTO.setTipoServico(listarFinalidadeEndOpeSaidaDTO.getCdTipoServico());
			excluirFinalidadeEndOpeEntradaDTO.setModalidadeServico(listarFinalidadeEndOpeSaidaDTO.getCdModalidadeServico());
			excluirFinalidadeEndOpeEntradaDTO.setTipoRelacionamento(listarFinalidadeEndOpeSaidaDTO.getCdTipoRelacionamento());
			excluirFinalidadeEndOpeEntradaDTO.setFinalidadeEndereco(listarFinalidadeEndOpeSaidaDTO.getCdFinalidadeEndereco());
			excluirFinalidadeEndOpeEntradaDTO.setCdOperacaoProdutoServico(listarFinalidadeEndOpeSaidaDTO.getCdOperacaoProdutoServico());
			
			ExcluirFinalidadeEndOpeSaidaDTO excluirFinalidadeEndOpeSaidaDTO = getFinalidadeEndProdutoServService().excluirFinalidadeEnderecoOperacao(excluirFinalidadeEndOpeEntradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +excluirFinalidadeEndOpeSaidaDTO.getCodMensagem() + ") " + excluirFinalidadeEndOpeSaidaDTO.getMensagem(), CON_FINALIDADE_ENDERECO_PRODUTO_OPERACAO, BradescoViewExceptionActionType.ACTION, false);			
			consultar();
			
		} catch (PdcAdapterFunctionalException p) {
			
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
			
		}
		return "";
		
	}
	
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt){
		
	}
	
	/**
	 * Carrega variaveis.
	 */
	public void carregaVariaveis(){	
		
		ListarFinalidadeEndOpeSaidaDTO listarFinalidadeEndOpeSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
		
		DetalharFinalidadeEndOpeEntradaDTO detalharFinalidadeEndOpeEntradaDTO = new DetalharFinalidadeEndOpeEntradaDTO();
		
		detalharFinalidadeEndOpeEntradaDTO.setTipoServico(listarFinalidadeEndOpeSaidaDTO.getCdTipoServico());
		detalharFinalidadeEndOpeEntradaDTO.setModalidadeServico(listarFinalidadeEndOpeSaidaDTO.getCdModalidadeServico());
		detalharFinalidadeEndOpeEntradaDTO.setTipoRelacionamento(listarFinalidadeEndOpeSaidaDTO.getCdTipoRelacionamento());
		detalharFinalidadeEndOpeEntradaDTO.setFinalidadeEndereco(listarFinalidadeEndOpeSaidaDTO.getCdFinalidadeEndereco());
		detalharFinalidadeEndOpeEntradaDTO.setCdOperacaoProdutoServico(listarFinalidadeEndOpeSaidaDTO.getCdOperacaoProdutoServico());
		
		DetalharFinalidadeEndOpeSaidaDTO detalharFinalidadeEndOpeSaidaDTO = getFinalidadeEndProdutoServService().detalharFinalidadeEnderecoOperacao(detalharFinalidadeEndOpeEntradaDTO);
		
		setFinalidadeEndereco(detalharFinalidadeEndOpeSaidaDTO.getDsFinalidadeEndereco());
		setOperacao(detalharFinalidadeEndOpeSaidaDTO.getDsOperacaoProdutoServico());
		setTipoServico(detalharFinalidadeEndOpeSaidaDTO.getDsTipoServico());
		setModalidadeServico(detalharFinalidadeEndOpeSaidaDTO.getDsModalidadeServico());
		setUsuarioInclusao(detalharFinalidadeEndOpeSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharFinalidadeEndOpeSaidaDTO.getUsuarioManutencao());		
		setComplementoInclusao(detalharFinalidadeEndOpeSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharFinalidadeEndOpeSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharFinalidadeEndOpeSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharFinalidadeEndOpeSaidaDTO.getComplementoManutencao());		
		setTipoCanalInclusao(detalharFinalidadeEndOpeSaidaDTO.getCdCanalInclusao()==0? "" : detalharFinalidadeEndOpeSaidaDTO.getCdCanalInclusao() + " - " +  detalharFinalidadeEndOpeSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(detalharFinalidadeEndOpeSaidaDTO.getCdCanalManutencao()==0? "" : detalharFinalidadeEndOpeSaidaDTO.getCdCanalManutencao() + " - " + detalharFinalidadeEndOpeSaidaDTO.getDsCanalManutencao());
		setDataHoraInclusao(detalharFinalidadeEndOpeSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharFinalidadeEndOpeSaidaDTO.getDataHoraManutencao());
		
		/*SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss.SSSSSS");
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		try {
			setDataHoraInclusao(formato2.format(formato1.parse(detalharFinalidadeEndOpeSaidaDTO.getDataHoraInclusao())));
		} catch (ParseException e) {
			setDataHoraInclusao("");
		}
		try {
			setDataHoraManutencao(formato2.format(formato1.parse(detalharFinalidadeEndOpeSaidaDTO.getDataHoraManutencao())));
		} catch (ParseException e) {
			setDataHoraManutencao("");
		}*/
		
		
	}

	//GETS SETS -----------------------------------------------------------------
	
	
	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}
	
	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoServico the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoServico) {
		this.itemSelecionadoLista = itemSelecionadoServico;
	}
	
	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}
	
	/**
	 * Set: listaControle.
	 *
	 * @param listaControleServico the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControleServico) {
		this.listaControle = listaControleServico;
	}
	
	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarFinalidadeEndOpeSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarFinalidadeEndOpeSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: listaModalidadeServico.
	 *
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {		
		return listaModalidadeServico;
	}
	
	/**
	 * Set: listaModalidadeServico.
	 *
	 * @param listaModalidadeServico the lista modalidade servico
	 */
	public void setListaModalidadeServico(List<SelectItem> listaModalidadeServico) {
		this.listaModalidadeServico = listaModalidadeServico;
	}
	
	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}
	
	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}
	
	/**
	 * Get: finalidadeEnderecoFiltro.
	 *
	 * @return finalidadeEnderecoFiltro
	 */
	public Integer getFinalidadeEnderecoFiltro() {		
		return finalidadeEnderecoFiltro;
	}
	
	/**
	 * Set: finalidadeEnderecoFiltro.
	 *
	 * @param finalidadeEnderecoFiltro the finalidade endereco filtro
	 */
	public void setFinalidadeEnderecoFiltro(Integer finalidadeEnderecoFiltro) {
		this.finalidadeEnderecoFiltro = finalidadeEnderecoFiltro;
	}
	
	/**
	 * Get: listaFinalidadeEndereco.
	 *
	 * @return listaFinalidadeEndereco
	 */
	public List<SelectItem> getListaFinalidadeEndereco() {
		return listaFinalidadeEndereco;
	}
	
	/**
	 * Set: listaFinalidadeEndereco.
	 *
	 * @param listaFinalidadeEndereco the lista finalidade endereco
	 */
	public void setListaFinalidadeEndereco(List<SelectItem> listaFinalidadeEndereco) {
		this.listaFinalidadeEndereco = listaFinalidadeEndereco;
	}
	
	/**
	 * Get: modalidadeServicoFiltro.
	 *
	 * @return modalidadeServicoFiltro
	 */
	public Integer getModalidadeServicoFiltro() {
		return modalidadeServicoFiltro;
	}
	
	/**
	 * Set: modalidadeServicoFiltro.
	 *
	 * @param modalidadeServicoFiltro the modalidade servico filtro
	 */
	public void setModalidadeServicoFiltro(Integer modalidadeServicoFiltro) {
		this.modalidadeServicoFiltro = modalidadeServicoFiltro;
	}
	
	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}
	
	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}	
	
	/**
	 * Get: finalidadeEndereco.
	 *
	 * @return finalidadeEndereco
	 */
	public String getFinalidadeEndereco() {
		return finalidadeEndereco;
	}
	
	/**
	 * Set: finalidadeEndereco.
	 *
	 * @param finalidadeEndereco the finalidade endereco
	 */
	public void setFinalidadeEndereco(String finalidadeEndereco) {
		this.finalidadeEndereco = finalidadeEndereco;
	}
	
	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public String getModalidadeServico() {
		return modalidadeServico;
	}
	
	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(String modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public String getTipoServico() {
		return tipoServico;
	}
	
	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Get: incFinalidadeEndereco.
	 *
	 * @return incFinalidadeEndereco
	 */
	public Integer getIncFinalidadeEndereco() {
		return incFinalidadeEndereco;
	}

	/**
	 * Set: incFinalidadeEndereco.
	 *
	 * @param incFinalidadeEndereco the inc finalidade endereco
	 */
	public void setIncFinalidadeEndereco(Integer incFinalidadeEndereco) {
		this.incFinalidadeEndereco = incFinalidadeEndereco;
	}

	/**
	 * Get: incModalidadeServico.
	 *
	 * @return incModalidadeServico
	 */
	public Integer getIncModalidadeServico() {
		return incModalidadeServico;
	}

	/**
	 * Set: incModalidadeServico.
	 *
	 * @param incModalidadeServico the inc modalidade servico
	 */
	public void setIncModalidadeServico(Integer incModalidadeServico) {
		this.incModalidadeServico = incModalidadeServico;
	}

	/**
	 * Get: incTipoServico.
	 *
	 * @return incTipoServico
	 */
	public Integer getIncTipoServico() {
		return incTipoServico;
	}

	/**
	 * Set: incTipoServico.
	 *
	 * @param incTipoServico the inc tipo servico
	 */
	public void setIncTipoServico(Integer incTipoServico) {
		this.incTipoServico = incTipoServico;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: finalidadeEndProdutoServService.
	 *
	 * @return finalidadeEndProdutoServService
	 */
	public IFinalidadeEndProdutoServService getFinalidadeEndProdutoServService() {
		return finalidadeEndProdutoServService;
	}

	/**
	 * Set: finalidadeEndProdutoServService.
	 *
	 * @param finalidadeEndProdutoServService the finalidade end produto serv service
	 */
	public void setFinalidadeEndProdutoServService(
			IFinalidadeEndProdutoServService finalidadeEndProdutoServService) {
		this.finalidadeEndProdutoServService = finalidadeEndProdutoServService;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: listaModalidadeServicoIncluir.
	 *
	 * @return listaModalidadeServicoIncluir
	 */
	public List<SelectItem> getListaModalidadeServicoIncluir() {
		return listaModalidadeServicoIncluir;
	}
	
	/**
	 * Set: listaModalidadeServicoIncluir.
	 *
	 * @param listaModalidadeServicoIncluir the lista modalidade servico incluir
	 */
	public void setListaModalidadeServicoIncluir(
			List<SelectItem> listaModalidadeServicoIncluir) {
		this.listaModalidadeServicoIncluir = listaModalidadeServicoIncluir;
	}

	/**
	 * Get: hiddenObrigatoriedade.
	 *
	 * @return hiddenObrigatoriedade
	 */
	public String getHiddenObrigatoriedade() {
		return hiddenObrigatoriedade;
	}

	/**
	 * Set: hiddenObrigatoriedade.
	 *
	 * @param hiddenObrigatoriedade the hidden obrigatoriedade
	 */
	public void setHiddenObrigatoriedade(String hiddenObrigatoriedade) {
		this.hiddenObrigatoriedade = hiddenObrigatoriedade;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: operacaoFiltro.
	 *
	 * @return operacaoFiltro
	 */
	public Integer getOperacaoFiltro() {
		return operacaoFiltro;
	}

	/**
	 * Set: operacaoFiltro.
	 *
	 * @param operacaoFiltro the operacao filtro
	 */
	public void setOperacaoFiltro(Integer operacaoFiltro) {
		this.operacaoFiltro = operacaoFiltro;
	}

	/**
	 * Get: listaOperacao.
	 *
	 * @return listaOperacao
	 */
	public List<SelectItem> getListaOperacao() {
		return listaOperacao;
	}

	/**
	 * Set: listaOperacao.
	 *
	 * @param listaOperacao the lista operacao
	 */
	public void setListaOperacao(List<SelectItem> listaOperacao) {
		this.listaOperacao = listaOperacao;
	}

	/**
	 * Get: listaOperacaoHash.
	 *
	 * @return listaOperacaoHash
	 */
	public Map<Integer, ListarOperacoesServicoSaidaDTO> getListaOperacaoHash() {
		return listaOperacaoHash;
	}

	/**
	 * Set lista operacao hash.
	 *
	 * @param listaOperacaoHash the lista operacao hash
	 */
	public void setListaOperacaoHash(
			Map<Integer, ListarOperacoesServicoSaidaDTO> listaOperacaoHash) {
		this.listaOperacaoHash = listaOperacaoHash;
	}

	/**
	 * Get: operacao.
	 *
	 * @return operacao
	 */
	public String getOperacao() {
		return operacao;
	}

	/**
	 * Set: operacao.
	 *
	 * @param operacao the operacao
	 */
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	/**
	 * Get: listaOperacaoIncluirHash.
	 *
	 * @return listaOperacaoIncluirHash
	 */
	public Map<Integer, ListarOperacoesServicoSaidaDTO> getListaOperacaoIncluirHash() {
		return listaOperacaoIncluirHash;
	}

	/**
	 * Set lista operacao incluir hash.
	 *
	 * @param listaOperacaoIncluirHash the lista operacao incluir hash
	 */
	public void setListaOperacaoIncluirHash(
			Map<Integer, ListarOperacoesServicoSaidaDTO> listaOperacaoIncluirHash) {
		this.listaOperacaoIncluirHash = listaOperacaoIncluirHash;
	}

	/**
	 * Get: listaOperacaoIncluir.
	 *
	 * @return listaOperacaoIncluir
	 */
	public List<SelectItem> getListaOperacaoIncluir() {
		return listaOperacaoIncluir;
	}

	/**
	 * Set: listaOperacaoIncluir.
	 *
	 * @param listaOperacaoIncluir the lista operacao incluir
	 */
	public void setListaOperacaoIncluir(List<SelectItem> listaOperacaoIncluir) {
		this.listaOperacaoIncluir = listaOperacaoIncluir;
	}

	/**
	 * Get: incOperacao.
	 *
	 * @return incOperacao
	 */
	public Integer getIncOperacao() {
		return incOperacao;
	}

	/**
	 * Set: incOperacao.
	 *
	 * @param incOperacao the inc operacao
	 */
	public void setIncOperacao(Integer incOperacao) {
		this.incOperacao = incOperacao;
	}

	
	
	
}
