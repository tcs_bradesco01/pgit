/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.imprimirformalizacaoalteracaocontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.imprimirformalizacaoalteracaocontrato;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ListarImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.IImprimirFormalizacaoAlteracaoContratoService;
import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.ImprimirAditivoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimirformalizacaoalteracaocontrato.bean.ImprimirAditivoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.IListarFuncBradescoService;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: ImprimirFormalizacaoAlteracaoContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirFormalizacaoAlteracaoContratoBean {
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo listarFuncBradescoService. */
	private IListarFuncBradescoService listarFuncBradescoService;
	
	/** Atributo imprimirFormalizacaoAlteracaoContratoServiceImpl. */
	private IImprimirFormalizacaoAlteracaoContratoService imprimirFormalizacaoAlteracaoContratoServiceImpl;

	/** Atributo cpfCnpjRepresentante. */
	private String cpfCnpjRepresentante;
	
	/** Atributo nomeRazaoRepresentante. */
	private String nomeRazaoRepresentante;
	
	/** Atributo grupEconRepresentante. */
	private String grupEconRepresentante;
	
	/** Atributo ativEconRepresentante. */
	private String ativEconRepresentante;
	
	/** Atributo segRepresentante. */
	private String segRepresentante;
	
	/** Atributo subSegRepresentante. */
	private String subSegRepresentante;
	
	/** Atributo empresaContrato. */
	private String empresaContrato;
	
	/** Atributo cdEmpresaContrato. */
	private Long cdEmpresaContrato;
	
	/** Atributo tipoContrato. */
	private String tipoContrato;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo numeroContrato. */
	private String numeroContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo situacaoContrato. */
	private String situacaoContrato;
	
	/** Atributo motivoContrato. */
	private String motivoContrato;
	
	/** Atributo participacaoContrato. */
	private String participacaoContrato;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;
	
	
	//Atributos p�gina selecionarDocumentoImprimirContrato
	/** Atributo radioArgumentoPesquisaSelecionarDocumento. */
	private String radioArgumentoPesquisaSelecionarDocumento;
	
	/** Atributo numeroDocumentoFiltro. */
	private String numeroDocumentoFiltro;
	
	/** Atributo versaoFiltro. */
	private String versaoFiltro;
	
	/** Atributo listaDocumentos. */
	private List<ListarImprimirContratoSaidaDTO> listaDocumentos;
	
	/** Atributo listaControleDocumentos. */
	private List<SelectItem> listaControleDocumentos;
	
	/** Atributo itemSelecionadoListaDocumentos. */
	private Integer itemSelecionadoListaDocumentos;
	
	/** Atributo diaInicioFiltro. */
	private String diaInicioFiltro;
	
	/** Atributo mesInicioFiltro. */
	private String mesInicioFiltro;
	
	/** Atributo anoInicioFiltro. */
	private String anoInicioFiltro;
	
	/** Atributo diaFimFiltro. */
	private String diaFimFiltro;
	
	/** Atributo mesFimFiltro. */
	private String mesFimFiltro;
	
	/** Atributo anoFimFiltro. */
	private String anoFimFiltro;
	
	//Atributos p�gina selecionarDocumentoImprimirContrato
	/** Atributo listaGridFormalizacao. */
	private ImprimirAditivoContratoSaidaDTO listaGridFormalizacao;
	
	/** Atributo listaControleFormalizacao. */
	private List<SelectItem> listaControleFormalizacao;
	
	/** Atributo itemSelecionadoListaFormalizacao. */
	private Integer itemSelecionadoListaFormalizacao;
	
	/** Atributo cdPessoaJuridica. */
	private long cdPessoaJuridica;
	
	/** Atributo nrSequenciaContrato. */
	private long nrSequenciaContrato;
	
	//M�todos
	/**
	 * Limpar dados.
	 */
	public void limparDados(){
		setListaDocumentos(null);
		setListaControleDocumentos(null);
		setItemSelecionadoListaDocumentos(null);
		setListaGridFormalizacao(null);
		setListaControleFormalizacao(null);
		setItemSelecionadoListaFormalizacao(null);
		setRadioArgumentoPesquisaSelecionarDocumento(null);
		limparDadosFiltro();
	}
	
	/**
	 * Limpar dados filtro.
	 */
	public void limparDadosFiltro() {
		
		setNumeroDocumentoFiltro("");
		setVersaoFiltro("");
		setDiaInicioFiltro("");
		setMesInicioFiltro("");
		setAnoInicioFiltro("");
		setDiaFimFiltro("");
		setMesFimFiltro("");
		setAnoFimFiltro("");
	}
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt){
		setListaDocumentos(null);
		setListaControleDocumentos(null);
		setItemSelecionadoListaDocumentos(null);
		setListaGridFormalizacao(null);
		setListaControleFormalizacao(null);
		setItemSelecionadoListaFormalizacao(null);
		setRadioArgumentoPesquisaSelecionarDocumento(null);
		setNumeroDocumentoFiltro(null);
		setVersaoFiltro(null);
		
		identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		
		//carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteImprimirFormAltContrato.jsp");
		identificacaoClienteContratoBean.setPaginaRetorno("conImprimirFormalizacaoAlteracaoContrato");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		identificacaoClienteContratoBean.setBloqueiaRadio(false);
		identificacaoClienteContratoBean.limparDadosPesquisaContrato();		
		identificacaoClienteContratoBean.setObrigatoriedade("T");
	}
		
	/**
	 * Selecionar documento.
	 *
	 * @return the string
	 */
	public String selecionarDocumento(){
		
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresaContrato(saidaDTO.getCdPessoaJuridica());
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setCdTipoContrato(saidaDTO.getCdTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setDescricaoContrato(saidaDTO.getDsContrato());
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		
		return "SELECIONAR_DOCUMENTO";
	}
	
	/**
	 * Selecionar formalizacao.
	 *
	 * @return the string
	 */
	public String selecionarFormalizacao(){
		
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresaContrato(saidaDTO.getCdPessoaJuridica());
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setCdTipoContrato(saidaDTO.getCdTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setDescricaoContrato(saidaDTO.getDsContrato());
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setCdPessoaJuridica(saidaDTO.getCdPessoaJuridica());
		setNrSequenciaContrato(saidaDTO.getNrSequenciaContrato());
		
		carregaAditivos();
		
		return "SELECIONAR_FORMALIZACAO";
	}
	
	/**
	 * Carrega aditivos.
	 */
	public void carregaAditivos(){
		listaGridFormalizacao = new ImprimirAditivoContratoSaidaDTO();
		
		try{
			
			ImprimirAditivoContratoEntradaDTO entradaDTO = new ImprimirAditivoContratoEntradaDTO();
			entradaDTO.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
			/*entradaDTO.setCdTipoContrato(getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
			
			setListaGridFormalizacao(getImprimirFormalizacaoAlteracaoContratoServiceImpl().imprimirAditivoContrato(entradaDTO));
			
			for(int i = 0;i<getListaGridFormalizacao().size();i++){
				listaControleFormalizacao.add(new SelectItem(i," "));
			}*/
			
			this.listaControleFormalizacao = new ArrayList<SelectItem>();
			
			setItemSelecionadoListaFormalizacao(null);			
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridFormalizacao(null);
			setItemSelecionadoListaFormalizacao(null);			
		}
	}
	
	/**
	 * Voltar formalizacao.
	 *
	 * @return the string
	 */
	public String voltarFormalizacao(){
		return "VOLTAR_FORMALIZACAO";
	}
	
	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		return "VOLTAR_PESQUISAR";
	}
		
	//Get's e Set's
	/**
	 * Get: radioArgumentoPesquisaSelecionarDocumento.
	 *
	 * @return radioArgumentoPesquisaSelecionarDocumento
	 */
	public String getRadioArgumentoPesquisaSelecionarDocumento() {
		return radioArgumentoPesquisaSelecionarDocumento;
	}

	/**
	 * Set: radioArgumentoPesquisaSelecionarDocumento.
	 *
	 * @param radioArgumentoPesquisaSelecionarDocumento the radio argumento pesquisa selecionar documento
	 */
	public void setRadioArgumentoPesquisaSelecionarDocumento(
			String radioArgumentoPesquisaSelecionarDocumento) {
		this.radioArgumentoPesquisaSelecionarDocumento = radioArgumentoPesquisaSelecionarDocumento;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Voltar solicitacoes.
	 *
	 * @return the string
	 */
	public String voltarSolicitacoes(){
		return "VOLTAR_PESQUISAR";
		
	}
	
	/**
	 * Get: listarFuncBradescoService.
	 *
	 * @return listarFuncBradescoService
	 */
	public IListarFuncBradescoService getListarFuncBradescoService() {
		return listarFuncBradescoService;
	}

	/**
	 * Set: listarFuncBradescoService.
	 *
	 * @param listarFuncBradescoService the listar func bradesco service
	 */
	public void setListarFuncBradescoService(
			IListarFuncBradescoService listarFuncBradescoService) {
		this.listarFuncBradescoService = listarFuncBradescoService;
	}
	
	/**
	 * Get: numeroDocumentoFiltro.
	 *
	 * @return numeroDocumentoFiltro
	 */
	public String getNumeroDocumentoFiltro() {
		return numeroDocumentoFiltro;
	}

	/**
	 * Set: numeroDocumentoFiltro.
	 *
	 * @param numeroDocumentoFiltro the numero documento filtro
	 */
	public void setNumeroDocumentoFiltro(String numeroDocumentoFiltro) {
		this.numeroDocumentoFiltro = numeroDocumentoFiltro;
	}

	/**
	 * Get: versaoFiltro.
	 *
	 * @return versaoFiltro
	 */
	public String getVersaoFiltro() {
		return versaoFiltro;
	}

	/**
	 * Set: versaoFiltro.
	 *
	 * @param versaoFiltro the versao filtro
	 */
	public void setVersaoFiltro(String versaoFiltro) {
		this.versaoFiltro = versaoFiltro;
	}

	/**
	 * Get: itemSelecionadoListaDocumentos.
	 *
	 * @return itemSelecionadoListaDocumentos
	 */
	public Integer getItemSelecionadoListaDocumentos() {
		return itemSelecionadoListaDocumentos;
	}

	/**
	 * Set: itemSelecionadoListaDocumentos.
	 *
	 * @param itemSelecionadoListaDocumentos the item selecionado lista documentos
	 */
	public void setItemSelecionadoListaDocumentos(
			Integer itemSelecionadoListaDocumentos) {
		this.itemSelecionadoListaDocumentos = itemSelecionadoListaDocumentos;
	}

	/**
	 * Get: listaControleDocumentos.
	 *
	 * @return listaControleDocumentos
	 */
	public List<SelectItem> getListaControleDocumentos() {
		return listaControleDocumentos;
	}

	/**
	 * Set: listaControleDocumentos.
	 *
	 * @param listaControleDocumentos the lista controle documentos
	 */
	public void setListaControleDocumentos(List<SelectItem> listaControleDocumentos) {
		this.listaControleDocumentos = listaControleDocumentos;
	}

	/**
	 * Get: listaDocumentos.
	 *
	 * @return listaDocumentos
	 */
	public List<ListarImprimirContratoSaidaDTO> getListaDocumentos() {
		return listaDocumentos;
	}

	/**
	 * Set: listaDocumentos.
	 *
	 * @param listaDocumentos the lista documentos
	 */
	public void setListaDocumentos(
			List<ListarImprimirContratoSaidaDTO> listaDocumentos) {
		this.listaDocumentos = listaDocumentos;
	}

	/**
	 * Get: itemSelecionadoListaFormalizacao.
	 *
	 * @return itemSelecionadoListaFormalizacao
	 */
	public Integer getItemSelecionadoListaFormalizacao() {
		return itemSelecionadoListaFormalizacao;
	}

	/**
	 * Set: itemSelecionadoListaFormalizacao.
	 *
	 * @param itemSelecionadoListaFormalizacao the item selecionado lista formalizacao
	 */
	public void setItemSelecionadoListaFormalizacao(
			Integer itemSelecionadoListaFormalizacao) {
		this.itemSelecionadoListaFormalizacao = itemSelecionadoListaFormalizacao;
	}

	/**
	 * Get: listaControleFormalizacao.
	 *
	 * @return listaControleFormalizacao
	 */
	public List<SelectItem> getListaControleFormalizacao() {
		return listaControleFormalizacao;
	}

	/**
	 * Set: listaControleFormalizacao.
	 *
	 * @param listaControleFormalizacao the lista controle formalizacao
	 */
	public void setListaControleFormalizacao(
			List<SelectItem> listaControleFormalizacao) {
		this.listaControleFormalizacao = listaControleFormalizacao;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: anoFimFiltro.
	 *
	 * @return anoFimFiltro
	 */
	public String getAnoFimFiltro() {
		return anoFimFiltro;
	}

	/**
	 * Set: anoFimFiltro.
	 *
	 * @param anoFimFiltro the ano fim filtro
	 */
	public void setAnoFimFiltro(String anoFimFiltro) {
		this.anoFimFiltro = anoFimFiltro;
	}

	/**
	 * Get: anoInicioFiltro.
	 *
	 * @return anoInicioFiltro
	 */
	public String getAnoInicioFiltro() {
		return anoInicioFiltro;
	}

	/**
	 * Set: anoInicioFiltro.
	 *
	 * @param anoInicioFiltro the ano inicio filtro
	 */
	public void setAnoInicioFiltro(String anoInicioFiltro) {
		this.anoInicioFiltro = anoInicioFiltro;
	}

	/**
	 * Get: diaFimFiltro.
	 *
	 * @return diaFimFiltro
	 */
	public String getDiaFimFiltro() {
		return diaFimFiltro;
	}

	/**
	 * Set: diaFimFiltro.
	 *
	 * @param diaFimFiltro the dia fim filtro
	 */
	public void setDiaFimFiltro(String diaFimFiltro) {
		this.diaFimFiltro = diaFimFiltro;
	}

	/**
	 * Get: diaInicioFiltro.
	 *
	 * @return diaInicioFiltro
	 */
	public String getDiaInicioFiltro() {
		return diaInicioFiltro;
	}

	/**
	 * Set: diaInicioFiltro.
	 *
	 * @param diaInicioFiltro the dia inicio filtro
	 */
	public void setDiaInicioFiltro(String diaInicioFiltro) {
		this.diaInicioFiltro = diaInicioFiltro;
	}

	/**
	 * Get: mesFimFiltro.
	 *
	 * @return mesFimFiltro
	 */
	public String getMesFimFiltro() {
		return mesFimFiltro;
	}

	/**
	 * Set: mesFimFiltro.
	 *
	 * @param mesFimFiltro the mes fim filtro
	 */
	public void setMesFimFiltro(String mesFimFiltro) {
		this.mesFimFiltro = mesFimFiltro;
	}

	/**
	 * Get: mesInicioFiltro.
	 *
	 * @return mesInicioFiltro
	 */
	public String getMesInicioFiltro() {
		return mesInicioFiltro;
	}

	/**
	 * Set: mesInicioFiltro.
	 *
	 * @param mesInicioFiltro the mes inicio filtro
	 */
	public void setMesInicioFiltro(String mesInicioFiltro) {
		this.mesInicioFiltro = mesInicioFiltro;
	}

	/**
	 * Get: ativEconRepresentante.
	 *
	 * @return ativEconRepresentante
	 */
	public String getAtivEconRepresentante() {
		return ativEconRepresentante;
	}

	/**
	 * Set: ativEconRepresentante.
	 *
	 * @param ativEconRepresentante the ativ econ representante
	 */
	public void setAtivEconRepresentante(String ativEconRepresentante) {
		this.ativEconRepresentante = ativEconRepresentante;
	}

	/**
	 * Get: cdEmpresaContrato.
	 *
	 * @return cdEmpresaContrato
	 */
	public Long getCdEmpresaContrato() {
		return cdEmpresaContrato;
	}

	/**
	 * Set: cdEmpresaContrato.
	 *
	 * @param cdEmpresaContrato the cd empresa contrato
	 */
	public void setCdEmpresaContrato(Long cdEmpresaContrato) {
		this.cdEmpresaContrato = cdEmpresaContrato;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: cpfCnpjRepresentante.
	 *
	 * @return cpfCnpjRepresentante
	 */
	public String getCpfCnpjRepresentante() {
		return cpfCnpjRepresentante;
	}

	/**
	 * Set: cpfCnpjRepresentante.
	 *
	 * @param cpfCnpjRepresentante the cpf cnpj representante
	 */
	public void setCpfCnpjRepresentante(String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: grupEconRepresentante.
	 *
	 * @return grupEconRepresentante
	 */
	public String getGrupEconRepresentante() {
		return grupEconRepresentante;
	}

	/**
	 * Set: grupEconRepresentante.
	 *
	 * @param grupEconRepresentante the grup econ representante
	 */
	public void setGrupEconRepresentante(String grupEconRepresentante) {
		this.grupEconRepresentante = grupEconRepresentante;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: nomeRazaoRepresentante.
	 *
	 * @return nomeRazaoRepresentante
	 */
	public String getNomeRazaoRepresentante() {
		return nomeRazaoRepresentante;
	}

	/**
	 * Set: nomeRazaoRepresentante.
	 *
	 * @param nomeRazaoRepresentante the nome razao representante
	 */
	public void setNomeRazaoRepresentante(String nomeRazaoRepresentante) {
		this.nomeRazaoRepresentante = nomeRazaoRepresentante;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: segRepresentante.
	 *
	 * @return segRepresentante
	 */
	public String getSegRepresentante() {
		return segRepresentante;
	}

	/**
	 * Set: segRepresentante.
	 *
	 * @param segRepresentante the seg representante
	 */
	public void setSegRepresentante(String segRepresentante) {
		this.segRepresentante = segRepresentante;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: subSegRepresentante.
	 *
	 * @return subSegRepresentante
	 */
	public String getSubSegRepresentante() {
		return subSegRepresentante;
	}

	/**
	 * Set: subSegRepresentante.
	 *
	 * @param subSegRepresentante the sub seg representante
	 */
	public void setSubSegRepresentante(String subSegRepresentante) {
		this.subSegRepresentante = subSegRepresentante;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: listaGridFormalizacao.
	 *
	 * @return listaGridFormalizacao
	 */
	public ImprimirAditivoContratoSaidaDTO getListaGridFormalizacao() {
		return listaGridFormalizacao;
	}

	/**
	 * Set: listaGridFormalizacao.
	 *
	 * @param listaGridFormalizacao the lista grid formalizacao
	 */
	public void setListaGridFormalizacao(
			ImprimirAditivoContratoSaidaDTO listaGridFormalizacao) {
		this.listaGridFormalizacao = listaGridFormalizacao;
	}

	/**
	 * Get: imprimirFormalizacaoAlteracaoContratoServiceImpl.
	 *
	 * @return imprimirFormalizacaoAlteracaoContratoServiceImpl
	 */
	public IImprimirFormalizacaoAlteracaoContratoService getImprimirFormalizacaoAlteracaoContratoServiceImpl() {
		return imprimirFormalizacaoAlteracaoContratoServiceImpl;
	}

	/**
	 * Set: imprimirFormalizacaoAlteracaoContratoServiceImpl.
	 *
	 * @param imprimirFormalizacaoAlteracaoContratoServiceImpl the imprimir formalizacao alteracao contrato service impl
	 */
	public void setImprimirFormalizacaoAlteracaoContratoServiceImpl(
			IImprimirFormalizacaoAlteracaoContratoService imprimirFormalizacaoAlteracaoContratoServiceImpl) {
		this.imprimirFormalizacaoAlteracaoContratoServiceImpl = imprimirFormalizacaoAlteracaoContratoServiceImpl;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}
	
	
	
	
	
}
