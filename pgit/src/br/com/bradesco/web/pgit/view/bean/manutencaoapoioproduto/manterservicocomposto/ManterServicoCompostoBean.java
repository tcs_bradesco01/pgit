/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterservicocomposto
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterservicocomposto;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarFormaLancCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarFormaLancCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.IManterServicoCompostoService;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.AlterarServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.AlterarServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.DetalharServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.DetalharServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ExcluirServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ExcluirServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.IncluirServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.IncluirServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ListarServicoCompostoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ListarServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ManterServicoCompostoBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ManterServicoCompostoBean {

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo entradaListarServico. */
	private ListarServicoCompostoEntradaDTO entradaListarServico;

	/** Atributo saidaListarServico. */
	private ListarServicoCompostoSaidaDTO saidaListarServico;

	/** Atributo listaServico. */
	private List<ListarServicoCompostoSaidaDTO> listaServico;

	/** Atributo entradaIncluiServico. */
	private IncluirServicoCompostoEntradaDTO entradaIncluiServico;

	/** Atributo saidaDetalheServico. */
	private DetalharServicoCompostoSaidaDTO saidaDetalheServico;

	/** Atributo entradaExcluiServico. */
	private ExcluirServicoCompostoEntradaDTO entradaExcluiServico;

	/** Atributo entradaAlterarServico. */
	private AlterarServicoCompostoEntradaDTO entradaAlterarServico;

	/** Atributo manterServCompostoService. */
	private IManterServicoCompostoService manterServCompostoService;

	/** Atributo entradaLiquidacao. */
	private ListarFormaLiquidacaoEntradaDTO entradaLiquidacao;

	/** Atributo saidaLiquidacao. */
	private ListarFormaLiquidacaoSaidaDTO saidaLiquidacao;

	/** Atributo entradaLancCnab. */
	private ListarFormaLancCnabEntradaDTO entradaLancCnab;

	/** Atributo saidaLancCnab. */
	private ListarFormaLancCnabSaidaDTO saidaLancCnab;

	/** Atributo entradaServicoCnab. */
	private ListarServicoCnabEntradaDTO entradaServicoCnab;

	/** Atributo saidaServicoCnab. */
	private ListarServicoCnabSaidaDTO saidaServicoCnab;

	/** Atributo itemSelecionadoLista. */
	private ListarServicoCompostoSaidaDTO itemSelecionadoLista;

	/** Atributo cdServicoCompostoPagamento. */
	private Long cdServicoCompostoPagamento;

	/** Atributo cdServicoCompostoPagamentoFiltro. */
	private Long cdServicoCompostoPagamentoFiltro;

	/** Atributo dsServicoCompostoPagamento. */
	private String dsServicoCompostoPagamento;

	/** Atributo dsServicoCompostoPagamentoCompleto. */
	private String dsServicoCompostoPagamentoCompleto;

	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;

	/** Atributo cdFormaLiquidacaoFiltro. */
	private Integer cdFormaLiquidacaoFiltro;

	/** Atributo dsFormaLiquidacao. */
	private String dsFormaLiquidacao;

	/** Atributo cdTipoServicoCnab. */
	private Integer cdTipoServicoCnab;

	/** Atributo cdTipoServicoCnabFiltro. */
	private Integer cdTipoServicoCnabFiltro;

	/** Atributo dsTipoServicoCnab. */
	private String dsTipoServicoCnab;

	/** Atributo cdFormaLancamentoCnab. */
	private Integer cdFormaLancamentoCnab;

	/** Atributo dsFormaLancamentoCnab. */
	private String dsFormaLancamentoCnab;

	/** Atributo qtdeDiasLancFuturo. */
	private Integer qtdeDiasLancFuturo;

	/** Atributo permitirLancFuturo. */
	private Integer permitirLancFuturo;

	/** Atributo descPermitirLancFuturo. */
	private String descPermitirLancFuturo;

	/** Atributo btoAcionado. */
	private Boolean btoAcionado;

	/** Atributo preencherFormaLiquidacao. */
	private List<SelectItem> preencherFormaLiquidacao = new ArrayList<SelectItem>();

	/** Atributo preencherTipoServico. */
	private List<SelectItem> preencherTipoServico;

	/* M�todos */

	/**
	 * Inicia tela.
	 * 
	 * @return the string
	 */
	public String iniciaTela() {
		limparDados();
		this.setItemSelecionadoLista(null);
		this.setEntradaLancCnab(new ListarFormaLancCnabEntradaDTO());
		this.setEntradaLiquidacao(new ListarFormaLiquidacaoEntradaDTO());
		this.setEntradaServicoCnab(new ListarServicoCnabEntradaDTO());
		this.setListaServico(new ArrayList<ListarServicoCompostoSaidaDTO>());
		this.loadPreencherFormaLiquidacao();
		this.loadPreencherTipoServico();
		setBtoAcionado(false);

		return "conManterServicoComposto";
	}

	/**
	 * Voltar.
	 * 
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoLista(null);
		setBtoAcionado(false);
		return "conManterServicoComposto";
	}

	/**
	 * Voltar alt.
	 * 
	 * @return the string
	 */
	public String voltarAlt() {
		setItemSelecionadoLista(null);
		return "conManterServicoComposto";
	}

	/**
	 * Valida valor dias.
	 * 
	 * @return the string
	 */
	public String validaValorDias() {
		if (getQtdeDiasLancFuturo() != null
				&& (getQtdeDiasLancFuturo() < 0 || getQtdeDiasLancFuturo() > 365)) {
			setQtdeDiasLancFuturo(null);
			BradescoFacesUtils
					.addInfoModalMessage(
							"O valor do campo Quantidade de Dias �teis para Lan�amento Futuro deve ser menor que 365 dias!",
							false);
		}
		return "";
	}

	/**
	 * Get: selectItemServicoComposto.
	 * 
	 * @return selectItemServicoComposto
	 */
	public List<SelectItem> getSelectItemServicoComposto() {
		if (this.getListaServico() == null || this.getListaServico().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.getListaServico().size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/**
	 * Preencher forma liquidacao desc.
	 */
	private void preencherFormaLiquidacaoDesc() {
		setDsFormaLiquidacao("");
		SelectItem selectItemLiquidacao;

		for (int j = 0; j < getPreencherFormaLiquidacao().size(); j++) {

			selectItemLiquidacao = getPreencherFormaLiquidacao().get(j);

			if (getCdFormaLiquidacao().equals(selectItemLiquidacao.getValue())) {
				setDsFormaLiquidacao(selectItemLiquidacao.getLabel());
				break;

			}
		}
	}

	/**
	 * Load preencher forma liquidacao.
	 */
	private void loadPreencherFormaLiquidacao() {
		try {
			List<ListarFormaLiquidacaoSaidaDTO> list = this.getComboService()
					.listarComboFormaLiquidacao();

			preencherFormaLiquidacao = new ArrayList<SelectItem>();

			for (ListarFormaLiquidacaoSaidaDTO combo : list) {
				preencherFormaLiquidacao.add(new SelectItem(combo
						.getCdFormaLiquidacao(), combo.getDsFormaLiquidacao()));
			}
		} catch (PdcAdapterFunctionalException e) {
			preencherFormaLiquidacao = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Preencher tipo servico desc.
	 */
	private void preencherTipoServicoDesc() {
		setDsTipoServicoCnab("");
		SelectItem selectItemServicoCnab;

		if(getPreencherTipoServico() !=null){
			for (int j = 0; j < getPreencherTipoServico().size(); j++) {
	
				selectItemServicoCnab = getPreencherTipoServico().get(j);
	
				if (getCdTipoServicoCnab().equals(selectItemServicoCnab.getValue())) {
					setDsTipoServicoCnab(selectItemServicoCnab.getLabel());
					break;
				}
			}
		}
	}

	/**
	 * Load preencher tipo servico.
	 */
	private void loadPreencherTipoServico() {
		try {
			List<ListarServicoCnabSaidaDTO> list = comboService
					.listarServicoCnab();

			preencherTipoServico = new ArrayList<SelectItem>();

			for (ListarServicoCnabSaidaDTO combo : list) {
				preencherTipoServico.add(new SelectItem(combo
						.getCdTipoServicoCnab(), combo.getDsTipoServicoCnab()));
			}
		} catch (PdcAdapterFunctionalException e) {
			preencherTipoServico = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Carrega lista.
	 */
	public void carregaLista() {
		listaServico = new ArrayList<ListarServicoCompostoSaidaDTO>();

		ListarServicoCompostoEntradaDTO bean = new ListarServicoCompostoEntradaDTO();

		bean.setCdServicoCompostoPagamento(this
				.getCdServicoCompostoPagamentoFiltro());
		// bean.setCdFormaLancamentoCnab(this.cdFormaLancamentoCnab);
		bean.setCdFormaLancamentoCnab(0);
		bean.setDsServicoCompostoPagamento("");
		bean.setQtOcorrencias(50);

		listaServico = manterServCompostoService.consultarServicoComposto(bean);
		setBtoAcionado(true);
	}

	/**
	 * Limpar dados.
	 */
	public void limparDados() {
		this.setCdServicoCompostoPagamento(null);
		setCdServicoCompostoPagamentoFiltro(null);
		this.setCdFormaLancamentoCnab(0);
		this.setCdFormaLiquidacao(0);
		setCdFormaLiquidacaoFiltro(0);
		this.setCdTipoServicoCnab(0);
		setCdTipoServicoCnabFiltro(0);
		this.setDsServicoCompostoPagamento("");
		this.setListaServico(null);
		this.setItemSelecionadoLista(null);
		this.setQtdeDiasLancFuturo(null);
		this.setPermitirLancFuturo(null);

		setBtoAcionado(false);
	}

	/**
	 * Limpar dados incluir.
	 */
	public void limparDadosIncluir() {
		this.setCdServicoCompostoPagamento(null);
		this.setCdFormaLancamentoCnab(0);
		this.setCdFormaLiquidacao(0);
		this.setCdTipoServicoCnab(0);
		this.setDsServicoCompostoPagamento("");
		this.setQtdeDiasLancFuturo(null);
		this.setPermitirLancFuturo(null);
		setBtoAcionado(true);
	}

	/**
	 * Limpa dados tela.
	 */
	public void limpaDadosTela() {
		this.setCdServicoCompostoPagamento(null);
		this.setCdFormaLancamentoCnab(0);
		this.setCdFormaLiquidacao(0);
		this.setCdTipoServicoCnab(0);
		this.setDsServicoCompostoPagamento("");
		this.setListaServico(null);
		this.setItemSelecionadoLista(null);
		this.setQtdeDiasLancFuturo(null);
		this.setPermitirLancFuturo(null);
		setBtoAcionado(false);
	}

	/**
	 * Incluir.
	 * 
	 * @return the string
	 */
	public String incluir() {
		limparDados();
		limparDadosIncluir();
		setEntradaIncluiServico(new IncluirServicoCompostoEntradaDTO());
		return "INCLUIR";
	}

	/**
	 * Avancar incluir.
	 * 
	 * @return the string
	 */
	public String avancarIncluir() {
		saidaLiquidacao = new ListarFormaLiquidacaoSaidaDTO();
		entradaIncluiServico = new IncluirServicoCompostoEntradaDTO();
		entradaIncluiServico.setCdServicoCompostoPagamento(this
				.getCdServicoCompostoPagamento());
		entradaIncluiServico
				.setCdFormaLancamentoCnab(this.cdFormaLancamentoCnab == null ? 0
						: this.cdFormaLancamentoCnab);
		entradaIncluiServico
				.setCdFormaLiquidacao(this.cdFormaLiquidacao == null ? 0
						: this.cdFormaLiquidacao);
		entradaIncluiServico
				.setCdTipoServicoCnab(this.cdTipoServicoCnab == null ? 0
						: this.cdTipoServicoCnab);
		entradaIncluiServico
				.setDsServicoCompostoPagamento(this.dsServicoCompostoPagamento);
		entradaIncluiServico.setQtDiaLctoFuturo(this.qtdeDiasLancFuturo);
		entradaIncluiServico.setCdIndLctoFuturo(this.permitirLancFuturo);
		entradaIncluiServico.setQtOcorrencias(50);
		if (this.cdFormaLiquidacao != null) {
			preencherFormaLiquidacaoDesc();
		}
		if (this.cdTipoServicoCnab != null) {
			preencherTipoServicoDesc();
		}
		setDescPermitirLancFuturo(this.permitirLancFuturo != null
				&& this.permitirLancFuturo.equals(1) ? "SIM" : "N�O");
		return "AVANCAR_INCLUIR";
	}

	/**
	 * Confirma incluir.
	 * 
	 * @return the string
	 */
	public String confirmaIncluir() {
		IncluirServicoCompostoSaidaDTO saida = new IncluirServicoCompostoSaidaDTO();

		try {
			saida = manterServCompostoService
					.incluirServicoComposto(entradaIncluiServico);
			limparDados();
			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(), "conManterServicoComposto",
					BradescoViewExceptionActionType.ACTION, false);
		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}

		return "";
	}

	/**
	 * Voltar incluir.
	 * 
	 * @return the string
	 */
	public String voltarIncluir() {

		return "VOLTAR_INCLUIR";
	}

	/**
	 * Alterar.
	 * 
	 * @return the string
	 */
	public String alterar() {
		detalharExibicao();

		return "ALTERAR";
	}

	/**
	 * Avancar alterar.
	 * 
	 * @return the string
	 */
	public String avancarAlterar() {
		saidaLiquidacao = new ListarFormaLiquidacaoSaidaDTO();
		entradaAlterarServico = new AlterarServicoCompostoEntradaDTO();
		entradaAlterarServico
				.setCdServicoCompostoPagamento(this.cdServicoCompostoPagamento);
		entradaAlterarServico
				.setCdFormaLancamentoCnab(this.cdFormaLancamentoCnab == null ? 0
						: this.cdFormaLancamentoCnab);
		entradaAlterarServico
				.setCdFormaLiquidacao(this.cdFormaLiquidacao == null ? 0
						: this.cdFormaLiquidacao);
		entradaAlterarServico
				.setCdTipoServicoCnab(this.cdTipoServicoCnab == null ? 0
						: this.cdTipoServicoCnab);
		entradaAlterarServico
				.setDsServicoCompostoPagamento(this.dsServicoCompostoPagamento);
		entradaAlterarServico.setQtDiaLctoFuturo(this.qtdeDiasLancFuturo);
		entradaAlterarServico.setCdIndLctoFuturo(this.permitirLancFuturo);
		entradaAlterarServico.setQtOcorrencias(50);
		if (this.cdFormaLiquidacao != null) {
			preencherFormaLiquidacaoDesc();
		}
		if (this.cdTipoServicoCnab != null) {
			preencherTipoServicoDesc();
		}
		setDescPermitirLancFuturo(this.permitirLancFuturo != null
				&& this.permitirLancFuturo.equals(1) ? "SIM" : "N�O");
		return "AVANCAR_ALTERAR";
	}

	/**
	 * Confirma alterar.
	 * 
	 * @return the string
	 */
	public String confirmaAlterar() {
		AlterarServicoCompostoSaidaDTO saida = new AlterarServicoCompostoSaidaDTO();

		try {
			saida = manterServCompostoService
					.alterarServicoComposto(entradaAlterarServico);
			limparDados();
			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(), "conManterServicoComposto",
					BradescoViewExceptionActionType.ACTION, false);
		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}

		return "";
	}

	/**
	 * Voltar alterar.
	 * 
	 * @return the string
	 */
	public String voltarAlterar() {

		return "VOLTAR_ALTERAR";
	}

	/**
	 * Detalhar exibicao.
	 */
	private void detalharExibicao() {
		DetalharServicoCompostoEntradaDTO entrada = new DetalharServicoCompostoEntradaDTO();
		saidaListarServico = new ListarServicoCompostoSaidaDTO();

		// setSaidaListarServico(this.listaServico.get(this.itemSelecionadoLista));

		entrada.setCdFormaLancamentoCnab(this.itemSelecionadoLista
				.getCdFormaLancamentoCnab());
		entrada.setCdFormaLiquidacao(this.itemSelecionadoLista
				.getCdFormaLiquidacao());
		entrada.setCdServicoCompostoPagamento(this.itemSelecionadoLista
				.getCdServicoCompostoPagamento());
		entrada.setCdTipoServicoCnab(this.itemSelecionadoLista
				.getCdTipoServicoCnab());
		entrada.setDsServicoCompostoPagamento(this.itemSelecionadoLista
				.getDsServicoCompostoPagamento());
		entrada.setQtOcorrencias(50);

		saidaDetalheServico = manterServCompostoService
				.detalharServicoComposto(entrada);
		setDescPermitirLancFuturo(PgitUtil.verificaStringNula(
				saidaDetalheServico.getDescIndLctoFuturo()).toUpperCase());
		setDsServicoCompostoPagamentoCompleto(PgitUtil.concatenarCampos(
				saidaDetalheServico.getCdServicoCompostoPagamento(),
				saidaDetalheServico.getDsServicoCompostoPagamento()));

		setCdServicoCompostoPagamento(saidaDetalheServico
				.getCdServicoCompostoPagamento());
		setCdFormaLancamentoCnab(saidaDetalheServico.getCdFormaLancamentoCnab());
		setCdFormaLiquidacao(saidaDetalheServico.getCdFormaLiquidacao());
		setCdTipoServicoCnab(saidaDetalheServico.getCdTipoServicoCnab());
		setDsServicoCompostoPagamento(saidaDetalheServico
				.getDsServicoCompostoPagamento());
		setQtdeDiasLancFuturo(saidaDetalheServico.getQtDiaLctoFuturo());
		setPermitirLancFuturo(saidaDetalheServico.getCdIndLctoFuturo());
		setCdServicoCompostoPagamento(saidaDetalheServico
				.getCdServicoCompostoPagamento());
	}

	/**
	 * Detalhar.
	 * 
	 * @return the string
	 */
	public String detalhar() {
		detalharExibicao();
		return "DETALHAR";
	}

	/**
	 * Voltar detalhar.
	 * 
	 * @return the string
	 */
	public String voltarDetalhar() {
		setItemSelecionadoLista(null);
		setBtoAcionado(true);

		return "VOLTAR_DETALHAR";
	}

	/**
	 * Excluir.
	 * 
	 * @return the string
	 */
	public String excluir() {
		detalharExibicao();
		entradaExcluiServico = new ExcluirServicoCompostoEntradaDTO();

		entradaExcluiServico.setCdFormaLancamentoCnab(saidaDetalheServico
				.getCdFormaLancamentoCnab());
		entradaExcluiServico.setCdFormaLiquidacao(saidaDetalheServico
				.getCdFormaLiquidacao());
		entradaExcluiServico.setCdServicoCompostoPagamento(saidaDetalheServico
				.getCdServicoCompostoPagamento());
		entradaExcluiServico.setCdTipoServicoCnab(saidaDetalheServico
				.getCdTipoServicoCnab());
		entradaExcluiServico.setDsServicoCompostoPagamento(saidaDetalheServico
				.getDsServicoCompostoPagamento());
		entradaExcluiServico.setCdIndLctoFuturo(saidaDetalheServico
				.getCdIndLctoFuturo());
		entradaExcluiServico.setQtDiaLctoFuturo(saidaDetalheServico
				.getQtDiaLctoFuturo());
		entradaExcluiServico.setQtOcorrencias(50);
		return "EXCLUIR";
	}

	/**
	 * Confirma exclusao.
	 * 
	 * @return the string
	 */
	public String confirmaExclusao() {

		ExcluirServicoCompostoSaidaDTO saida = new ExcluirServicoCompostoSaidaDTO();
		try {
			saida = manterServCompostoService
					.excluirServicoComposto(entradaExcluiServico);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(), "CONFIRMA",
					BradescoViewExceptionActionType.ACTION, false);
			limparDados();
		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}
		return "";
	}

	/**
	 * Voltar excluir.
	 * 
	 * @return the string
	 */
	public String voltarExcluir() {
		setItemSelecionadoLista(null);
		setBtoAcionado(true);

		return "VOLTAR_EXCLUIR";
	}

	/**
	 * Submit pesquisar.
	 * 
	 * @param evt
	 *            the evt
	 * @return the string
	 */
	public String submitPesquisar(ActionEvent evt) {
		return "conManterServicoComposto";
	}

	/* Getters e Setters */

	/**
	 * Get: comboService.
	 * 
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 * 
	 * @param comboService
	 *            the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: entradaListarServico.
	 * 
	 * @return entradaListarServico
	 */
	public ListarServicoCompostoEntradaDTO getEntradaListarServico() {
		return entradaListarServico;
	}

	/**
	 * Get: listaPermiteLancFuturo.
	 * 
	 * @return listaPermiteLancFuturo
	 */
	public List<SelectItem> getListaPermiteLancFuturo() {
		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(1, ""));
		list.add(new SelectItem(2, ""));
		return list;
	}

	/**
	 * Set: entradaListarServico.
	 * 
	 * @param entradaListarServico
	 *            the entrada listar servico
	 */
	public void setEntradaListarServico(
			ListarServicoCompostoEntradaDTO entradaListarServico) {
		this.entradaListarServico = entradaListarServico;
	}

	/**
	 * Get: dsServicoCompostoPagamento.
	 * 
	 * @return dsServicoCompostoPagamento
	 */
	public String getDsServicoCompostoPagamento() {
		return dsServicoCompostoPagamento;
	}

	/**
	 * Set: dsServicoCompostoPagamento.
	 * 
	 * @param dsServicoCompostoPagamento
	 *            the ds servico composto pagamento
	 */
	public void setDsServicoCompostoPagamento(String dsServicoCompostoPagamento) {
		this.dsServicoCompostoPagamento = dsServicoCompostoPagamento;
	}

	/**
	 * Get: saidaListarServico.
	 * 
	 * @return saidaListarServico
	 */
	public ListarServicoCompostoSaidaDTO getSaidaListarServico() {
		return saidaListarServico;
	}

	/**
	 * Set: saidaListarServico.
	 * 
	 * @param saidaListarServico
	 *            the saida listar servico
	 */
	public void setSaidaListarServico(
			ListarServicoCompostoSaidaDTO saidaListarServico) {
		this.saidaListarServico = saidaListarServico;
	}

	/**
	 * Set: cdServicoCompostoPagamento.
	 * 
	 * @param cdServicoCompostoPagamento
	 *            the cd servico composto pagamento
	 */
	public void setCdServicoCompostoPagamento(Long cdServicoCompostoPagamento) {
		this.cdServicoCompostoPagamento = cdServicoCompostoPagamento;
	}

	/**
	 * Get: cdServicoCompostoPagamento.
	 * 
	 * @return cdServicoCompostoPagamento
	 */
	public Long getCdServicoCompostoPagamento() {
		return cdServicoCompostoPagamento;
	}

	/**
	 * Get: listaServico.
	 * 
	 * @return listaServico
	 */
	public List<ListarServicoCompostoSaidaDTO> getListaServico() {
		return listaServico;
	}

	/**
	 * Set: listaServico.
	 * 
	 * @param listaServico
	 *            the lista servico
	 */
	public void setListaServico(List<ListarServicoCompostoSaidaDTO> listaServico) {
		this.listaServico = listaServico;
	}

	/**
	 * Get: saidaLancCnab.
	 * 
	 * @return saidaLancCnab
	 */
	public ListarFormaLancCnabSaidaDTO getSaidaLancCnab() {
		return saidaLancCnab;
	}

	/**
	 * Set: saidaLancCnab.
	 * 
	 * @param saidaLancCnab
	 *            the saida lanc cnab
	 */
	public void setSaidaLancCnab(ListarFormaLancCnabSaidaDTO saidaLancCnab) {
		this.saidaLancCnab = saidaLancCnab;
	}

	/**
	 * Get: saidaLiquidacao.
	 * 
	 * @return saidaLiquidacao
	 */
	public ListarFormaLiquidacaoSaidaDTO getSaidaLiquidacao() {
		return saidaLiquidacao;
	}

	/**
	 * Set: saidaLiquidacao.
	 * 
	 * @param saidaLiquidacao
	 *            the saida liquidacao
	 */
	public void setSaidaLiquidacao(ListarFormaLiquidacaoSaidaDTO saidaLiquidacao) {
		this.saidaLiquidacao = saidaLiquidacao;
	}

	/**
	 * Get: saidaServicoCnab.
	 * 
	 * @return saidaServicoCnab
	 */
	public ListarServicoCnabSaidaDTO getSaidaServicoCnab() {
		return saidaServicoCnab;
	}

	/**
	 * Set: saidaServicoCnab.
	 * 
	 * @param saidaServicoCnab
	 *            the saida servico cnab
	 */
	public void setSaidaServicoCnab(ListarServicoCnabSaidaDTO saidaServicoCnab) {
		this.saidaServicoCnab = saidaServicoCnab;
	}

	/**
	 * Get: entradaLancCnab.
	 * 
	 * @return entradaLancCnab
	 */
	public ListarFormaLancCnabEntradaDTO getEntradaLancCnab() {
		return entradaLancCnab;
	}

	/**
	 * Set: entradaLancCnab.
	 * 
	 * @param entradaLancCnab
	 *            the entrada lanc cnab
	 */
	public void setEntradaLancCnab(ListarFormaLancCnabEntradaDTO entradaLancCnab) {
		this.entradaLancCnab = entradaLancCnab;
	}

	/**
	 * Get: entradaLiquidacao.
	 * 
	 * @return entradaLiquidacao
	 */
	public ListarFormaLiquidacaoEntradaDTO getEntradaLiquidacao() {
		return entradaLiquidacao;
	}

	/**
	 * Set: entradaLiquidacao.
	 * 
	 * @param entradaLiquidacao
	 *            the entrada liquidacao
	 */
	public void setEntradaLiquidacao(
			ListarFormaLiquidacaoEntradaDTO entradaLiquidacao) {
		this.entradaLiquidacao = entradaLiquidacao;
	}

	/**
	 * Get: entradaServicoCnab.
	 * 
	 * @return entradaServicoCnab
	 */
	public ListarServicoCnabEntradaDTO getEntradaServicoCnab() {
		return entradaServicoCnab;
	}

	/**
	 * Set: entradaServicoCnab.
	 * 
	 * @param entradaServicoCnab
	 *            the entrada servico cnab
	 */
	public void setEntradaServicoCnab(
			ListarServicoCnabEntradaDTO entradaServicoCnab) {
		this.entradaServicoCnab = entradaServicoCnab;
	}

	/**
	 * Get: entradaIncluiServico.
	 * 
	 * @return entradaIncluiServico
	 */
	public IncluirServicoCompostoEntradaDTO getEntradaIncluiServico() {
		return entradaIncluiServico;
	}

	/**
	 * Set: entradaIncluiServico.
	 * 
	 * @param entradaIncluiServico
	 *            the entrada inclui servico
	 */
	public void setEntradaIncluiServico(
			IncluirServicoCompostoEntradaDTO entradaIncluiServico) {
		this.entradaIncluiServico = entradaIncluiServico;
	}

	/**
	 * Get: saidaDetalheServico.
	 * 
	 * @return saidaDetalheServico
	 */
	public DetalharServicoCompostoSaidaDTO getSaidaDetalheServico() {
		return saidaDetalheServico;
	}

	/**
	 * Set: saidaDetalheServico.
	 * 
	 * @param saidaDetalheServico
	 *            the saida detalhe servico
	 */
	public void setSaidaDetalheServico(
			DetalharServicoCompostoSaidaDTO saidaDetalheServico) {
		this.saidaDetalheServico = saidaDetalheServico;
	}

	/**
	 * Get: cdFormaLancamentoCnab.
	 * 
	 * @return cdFormaLancamentoCnab
	 */
	public Integer getCdFormaLancamentoCnab() {
		return cdFormaLancamentoCnab;
	}

	/**
	 * Set: cdFormaLancamentoCnab.
	 * 
	 * @param cdFormaLancamentoCnab
	 *            the cd forma lancamento cnab
	 */
	public void setCdFormaLancamentoCnab(Integer cdFormaLancamentoCnab) {
		this.cdFormaLancamentoCnab = cdFormaLancamentoCnab;
	}

	/**
	 * Get: cdTipoServicoCnab.
	 * 
	 * @return cdTipoServicoCnab
	 */
	public Integer getCdTipoServicoCnab() {
		return cdTipoServicoCnab;
	}

	/**
	 * Set: cdTipoServicoCnab.
	 * 
	 * @param cdTipoServicoCnab
	 *            the cd tipo servico cnab
	 */
	public void setCdTipoServicoCnab(Integer cdTipoServicoCnab) {
		this.cdTipoServicoCnab = cdTipoServicoCnab;
	}

	/**
	 * Get: cdFormaLiquidacao.
	 * 
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 * 
	 * @param cdFormaLiquidacao
	 *            the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: dsTipoServicoCnab.
	 * 
	 * @return dsTipoServicoCnab
	 */
	public String getDsTipoServicoCnab() {
		return dsTipoServicoCnab;
	}

	/**
	 * Set: dsTipoServicoCnab.
	 * 
	 * @param dsTipoServicoCnab
	 *            the ds tipo servico cnab
	 */
	public void setDsTipoServicoCnab(String dsTipoServicoCnab) {
		this.dsTipoServicoCnab = dsTipoServicoCnab;
	}

	/**
	 * Get: manterServCompostoService.
	 * 
	 * @return manterServCompostoService
	 */
	public IManterServicoCompostoService getManterServCompostoService() {
		return manterServCompostoService;
	}

	/**
	 * Set: manterServCompostoService.
	 * 
	 * @param manterServCompostoService
	 *            the manter serv composto service
	 */
	public void setManterServCompostoService(
			IManterServicoCompostoService manterServCompostoService) {
		this.manterServCompostoService = manterServCompostoService;
	}

	/**
	 * Get: entradaExcluiServico.
	 * 
	 * @return entradaExcluiServico
	 */
	public ExcluirServicoCompostoEntradaDTO getEntradaExcluiServico() {
		return entradaExcluiServico;
	}

	/**
	 * Set: entradaExcluiServico.
	 * 
	 * @param entradaExcluiServico
	 *            the entrada exclui servico
	 */
	public void setEntradaExcluiServico(
			ExcluirServicoCompostoEntradaDTO entradaExcluiServico) {
		this.entradaExcluiServico = entradaExcluiServico;
	}

	/**
	 * Get: preencherTipoServico.
	 * 
	 * @return preencherTipoServico
	 */
	public List<SelectItem> getPreencherTipoServico() {
		return preencherTipoServico;
	}

	/**
	 * Set: preencherTipoServico.
	 * 
	 * @param preencherTipoServico
	 *            the preencher tipo servico
	 */
	public void setPreencherTipoServico(List<SelectItem> preencherTipoServico) {
		this.preencherTipoServico = preencherTipoServico;
	}

	/**
	 * Get: preencherFormaLiquidacao.
	 * 
	 * @return preencherFormaLiquidacao
	 */
	public List<SelectItem> getPreencherFormaLiquidacao() {
		return preencherFormaLiquidacao;
	}

	/**
	 * Set: preencherFormaLiquidacao.
	 * 
	 * @param preencherFormaLiquidacao
	 *            the preencher forma liquidacao
	 */
	public void setPreencherFormaLiquidacao(
			List<SelectItem> preencherFormaLiquidacao) {
		this.preencherFormaLiquidacao = preencherFormaLiquidacao;
	}

	/**
	 * Get: dsFormaLancamentoCnab.
	 * 
	 * @return dsFormaLancamentoCnab
	 */
	public String getDsFormaLancamentoCnab() {
		return dsFormaLancamentoCnab;
	}

	/**
	 * Set: dsFormaLancamentoCnab.
	 * 
	 * @param dsFormaLancamentoCnab
	 *            the ds forma lancamento cnab
	 */
	public void setDsFormaLancamentoCnab(String dsFormaLancamentoCnab) {
		this.dsFormaLancamentoCnab = dsFormaLancamentoCnab;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 * 
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
		return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 * 
	 * @param dsFormaLiquidacao
	 *            the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}

	/**
	 * Get: itemSelecionadoLista.
	 * 
	 * @return itemSelecionadoLista
	 */
	public ListarServicoCompostoSaidaDTO getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 * 
	 * @param itemSelecionadoLista
	 *            the item selecionado lista
	 */
	public void setItemSelecionadoLista(
			ListarServicoCompostoSaidaDTO itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: btoAcionado.
	 * 
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 * 
	 * @param btoAcionado
	 *            the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: cdFormaLiquidacaoFiltro.
	 * 
	 * @return cdFormaLiquidacaoFiltro
	 */
	public Integer getCdFormaLiquidacaoFiltro() {
		return cdFormaLiquidacaoFiltro;
	}

	/**
	 * Set: cdFormaLiquidacaoFiltro.
	 * 
	 * @param cdFormaLiquidacaoFiltro
	 *            the cd forma liquidacao filtro
	 */
	public void setCdFormaLiquidacaoFiltro(Integer cdFormaLiquidacaoFiltro) {
		this.cdFormaLiquidacaoFiltro = cdFormaLiquidacaoFiltro;
	}

	/**
	 * Get: cdServicoCompostoPagamentoFiltro.
	 * 
	 * @return cdServicoCompostoPagamentoFiltro
	 */
	public Long getCdServicoCompostoPagamentoFiltro() {
		return cdServicoCompostoPagamentoFiltro;
	}

	/**
	 * Set: cdServicoCompostoPagamentoFiltro.
	 * 
	 * @param cdServicoCompostoPagamentoFiltro
	 *            the cd servico composto pagamento filtro
	 */
	public void setCdServicoCompostoPagamentoFiltro(
			Long cdServicoCompostoPagamentoFiltro) {
		this.cdServicoCompostoPagamentoFiltro = cdServicoCompostoPagamentoFiltro;
	}

	/**
	 * Get: cdTipoServicoCnabFiltro.
	 * 
	 * @return cdTipoServicoCnabFiltro
	 */
	public Integer getCdTipoServicoCnabFiltro() {
		return cdTipoServicoCnabFiltro;
	}

	/**
	 * Set: cdTipoServicoCnabFiltro.
	 * 
	 * @param cdTipoServicoCnabFiltro
	 *            the cd tipo servico cnab filtro
	 */
	public void setCdTipoServicoCnabFiltro(Integer cdTipoServicoCnabFiltro) {
		this.cdTipoServicoCnabFiltro = cdTipoServicoCnabFiltro;
	}

	/**
	 * Get: entradaAlterarServico.
	 * 
	 * @return entradaAlterarServico
	 */
	public AlterarServicoCompostoEntradaDTO getEntradaAlterarServico() {
		return entradaAlterarServico;
	}

	/**
	 * Set: entradaAlterarServico.
	 * 
	 * @param entradaAlterarServico
	 *            the entrada alterar servico
	 */
	public void setEntradaAlterarServico(
			AlterarServicoCompostoEntradaDTO entradaAlterarServico) {
		this.entradaAlterarServico = entradaAlterarServico;
	}

	/**
	 * Get: qtdeDiasLancFuturo.
	 * 
	 * @return qtdeDiasLancFuturo
	 */
	public Integer getQtdeDiasLancFuturo() {
		return qtdeDiasLancFuturo;
	}

	/**
	 * Set: qtdeDiasLancFuturo.
	 * 
	 * @param qtdeDiasLancFuturo
	 *            the qtde dias lanc futuro
	 */
	public void setQtdeDiasLancFuturo(Integer qtdeDiasLancFuturo) {
		this.qtdeDiasLancFuturo = qtdeDiasLancFuturo;
	}

	/**
	 * Get: permitirLancFuturo.
	 * 
	 * @return permitirLancFuturo
	 */
	public Integer getPermitirLancFuturo() {
		return permitirLancFuturo;
	}

	/**
	 * Set: permitirLancFuturo.
	 * 
	 * @param permitirLancFuturo
	 *            the permitir lanc futuro
	 */
	public void setPermitirLancFuturo(Integer permitirLancFuturo) {
		this.permitirLancFuturo = permitirLancFuturo;
	}

	/**
	 * Get: descPermitirLancFuturo.
	 * 
	 * @return descPermitirLancFuturo
	 */
	public String getDescPermitirLancFuturo() {
		return descPermitirLancFuturo;
	}

	/**
	 * Set: descPermitirLancFuturo.
	 * 
	 * @param descPermitirLancFuturo
	 *            the desc permitir lanc futuro
	 */
	public void setDescPermitirLancFuturo(String descPermitirLancFuturo) {
		this.descPermitirLancFuturo = descPermitirLancFuturo;
	}

	/**
	 * Get: dsServicoCompostoPagamentoCompleto.
	 * 
	 * @return dsServicoCompostoPagamentoCompleto
	 */
	public String getDsServicoCompostoPagamentoCompleto() {
		return dsServicoCompostoPagamentoCompleto;
	}

	/**
	 * Set: dsServicoCompostoPagamentoCompleto.
	 * 
	 * @param dsServicoCompostoPagamentoCompleto
	 *            the ds servico composto pagamento completo
	 */
	public void setDsServicoCompostoPagamentoCompleto(
			String dsServicoCompostoPagamentoCompleto) {
		this.dsServicoCompostoPagamentoCompleto = dsServicoCompostoPagamentoCompleto;
	}

	public void limparCamposLancamentoFuturo() {
		this.setQtdeDiasLancFuturo(null);
		this.setPermitirLancFuturo(null);
	}

	public boolean isExibeCamposLancamentoFuturo() {
		Long codigoServicoComposto = null;

		if (getItemSelecionadoLista() != null) {
			codigoServicoComposto = getItemSelecionadoLista()
					.getCdServicoCompostoPagamento();
		} else {
			codigoServicoComposto = cdServicoCompostoPagamento;
		}

		if (codigoServicoComposto == null) {
			return false;
		}

		for (ServicoComposto sc : ServicoComposto.values()) {
			if (sc.getCodigoServicoComposto().compareTo(codigoServicoComposto) == 0) {
				return true;
			}
		}

		return false;

	}

	public boolean isHabilitaCamposLancamentoFuturo() {
		return !isExibeCamposLancamentoFuturo();
	}

}