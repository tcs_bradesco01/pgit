/*
 * Nome: br.com.bradesco.web.pgit.view.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean;

import java.io.IOException;

import javax.faces.event.ActionEvent;

import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.exception.PgitException;
import br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService;
import br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimir.bean.ImprimirComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: TesteBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TesteBean {

	/** Atributo imprimirService. */
	private IImprimirService imprimirService = null;

	/**
	 * Iniciar pagina.
	 *
	 * @param evt the evt
	 */
	public void iniciarPagina(ActionEvent evt) {

	}

	/**
	 * Imprimir.
	 *
	 * @return the string
	 */
	public String imprimir() {
		ImprimirComprovanteEntradaDTO imprimirComprovanteCredContaEntradaDTO = new ImprimirComprovanteEntradaDTO();

		imprimirComprovanteCredContaEntradaDTO
				.setCdPessoaJuridicaContrato(new Long(2269651));
		imprimirComprovanteCredContaEntradaDTO.setCdTipoContratoNegocio(21);
		imprimirComprovanteCredContaEntradaDTO
				.setNrSequenciaContratoNegocio(new Long(12));
		imprimirComprovanteCredContaEntradaDTO
				.setCdControlePagamento("IMP00000000001");
		imprimirComprovanteCredContaEntradaDTO
				.setDtEfetivacaoPagamento("01.06.2011");

		ImprimirComprovanteSaidaDTO imprimirComprovanteCredContaSaidaDTO = getImprimirService()
				.imprimirComprovanteCredConta(
						imprimirComprovanteCredContaEntradaDTO);

		try {
			getImprimirService().imprimirArquivo(FacesUtils.getServletResponse().getOutputStream(), imprimirComprovanteCredContaSaidaDTO.getProtocolo());
		} catch (IOException e) {
			throw new PgitException(e.getMessage(), e, "");
		}

		PgitFacesUtils.generateReportResponse("teste");

		return "";
	}

	/**
	 * Get: imprimirService.
	 *
	 * @return imprimirService
	 */
	public IImprimirService getImprimirService() {
		return imprimirService;
	}

	/**
	 * Set: imprimirService.
	 *
	 * @param imprimirService the imprimir service
	 */
	public void setImprimirService(IImprimirService imprimirService) {
		this.imprimirService = imprimirService;
	}

}
