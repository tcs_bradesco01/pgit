/*
 * Nome: br.com.bradesco.web.pgit.view.bean.tratarquivosremessa.consolicitacaorecuperacaoremessa
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.tratarquivosremessa.consolicitacaorecuperacaoremessa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.IArquivoRemessaService;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarArquivoRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarArquivoRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ConsultarSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.DetalharSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.DetalharSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ExcluirSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.ExcluirSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.IncluirSolicRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.IncluirSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.arquivoremessa.bean.OcorrenciasDetalharSolicRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarResultadoProcessamentoRemessaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoRemessaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcEntradaDTO;
import br.com.bradesco.web.pgit.utils.CamposUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: SolicitacaoRecuperacaoRemessaBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolicitacaoRecuperacaoRemessaBean {
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo arquivoRemessaServiceImpl. */
	private IArquivoRemessaService arquivoRemessaServiceImpl;
	
	/** Atributo filtroIncSolicitacaoRecuperacaoRemessaBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroIncSolicitacaoRecuperacaoRemessaBean;
	
	/** Atributo filtroConSolicitacaoRecuperacaoRemessaBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroConSolicitacaoRecuperacaoRemessaBean;
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo tipoContrato. */
	private String tipoContrato;
	
	/** Atributo bloqueiaRadio. */
	private boolean bloqueiaRadio; 
	
	/** Atributo itemSelecionadoIdentificacaoCliente. */
	private String itemSelecionadoIdentificacaoCliente; 
	
	/** Atributo empresaGestoraContrato. */
	private List<SelectItem> empresaGestoraContrato;
	
	/** Atributo itemSelecionadoEmpresaGestoraContrato. */
	private String itemSelecionadoEmpresaGestoraContrato;
	
	/** Atributo itemSelecionadoListaContrato. */
	private Integer itemSelecionadoListaContrato;
	
	/** Atributo tipoContratoLista. */
	private List<SelectItem>  tipoContratoLista;
	
	/** Atributo itemSelecionadoTipoContrato. */
	private String itemSelecionadoTipoContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo situacao. */
	private String situacao;
	
	/** Atributo dataSolicitacaoDe. */
	private Date dataSolicitacaoDe;
	
	/** Atributo dataSolicitacaoAte. */
	private Date dataSolicitacaoAte;
	
	/** Atributo usuario. */
	private String usuario;
	
	/** Atributo clienteContratoSelecionado. */
	private boolean clienteContratoSelecionado;
	
	/** Atributo cboSituacaoSolicitacao. */
	private Integer cboSituacaoSolicitacao; 
	
	/** Atributo listaSituacaoSolicitacao. */
	private List<SelectItem> listaSituacaoSolicitacao;
	
	/** Atributo listarEmpresaConglomerado. */
	private List<SelectItem> listarEmpresaConglomerado;
	
	/** Atributo itemSelecionadoSituacaoSolicitacao. */
	private String itemSelecionadoSituacaoSolicitacao;
	
	/** Atributo listaGridContrato. */
	private List<ConsultarSolicRecuperacaoSaidaDTO> listaGridContrato;
	
	/** Atributo listaAux. */
	private List<ConsultarSolicRecuperacaoSaidaDTO> listaAux; 
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo bloqueiaBotao. */
	private Integer bloqueiaBotao;
	
	/** Atributo listaControleRadioConsultar. */
	private List<SelectItem> listaControleRadioConsultar = new ArrayList<SelectItem>();	
	
	/** Atributo comboSituaProcessamento. */
	private List<SelectItem> comboSituaProcessamento;
	
	/** Atributo comboResultProcessamento. */
	private List<SelectItem> comboResultProcessamento;
	
	/** Atributo comboLayoutArquivo. */
	private List<SelectItem> comboLayoutArquivo;
	
	/** Atributo listaGridIncluir. */
	private List<ConsultarArquivoRecuperacaoSaidaDTO> listaGridIncluir;
	
	/** Atributo listaGridConfirmar. */
	private List<ConsultarArquivoRecuperacaoSaidaDTO> listaGridConfirmar;
	
	/** Atributo checkTudo. */
	private boolean checkTudo;
	
	/** Atributo relatorio. */
	private boolean relatorio;
	
	/** Atributo arquivoISD. */
	private boolean arquivoISD;
	
	/** Atributo arquivoRetorno. */
	private boolean arquivoRetorno;	
	
	/** Atributo sequenciaRemessa. */
	private Long sequenciaRemessa;
	
	/** Atributo sequenciaRemessaFiltro. */
	private Long sequenciaRemessaFiltro;
	
	/** Atributo itemSituaProcessamento. */
	private Integer itemSituaProcessamento;
	
	/** Atributo listaSituacaoProcessamento. */
	private List<SelectItem> listaSituacaoProcessamento = new ArrayList<SelectItem>();
	
	/** Atributo itemResultProcessamento. */
	private Integer itemResultProcessamento;
	
	/** Atributo listaResultadoProcessamento. */
	private List<SelectItem> listaResultadoProcessamento = new ArrayList<SelectItem>();
	
	/** Atributo itemLayoutArquivo. */
	private Integer itemLayoutArquivo;
	
	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	/** Atributo listarTipoLayoutArquivo. */
	private List<SelectItem> listarTipoLayoutArquivo;
	
	/** Atributo dataRecepcaoDe. */
	private Date dataRecepcaoDe;
	
	/** Atributo dataRecepcaoAte. */
	private Date dataRecepcaoAte;	
	
	/** Atributo relatorioCheck. */
	private String relatorioCheck; // valor Checkbox
	
	/** Atributo arquivoISDCheck. */
	private String arquivoISDCheck; // valor Checkbox
	
	/** Atributo arquivoRetornoCheck. */
	private String arquivoRetornoCheck; // valor Checkbox
	
	/** Atributo grupoEconomico. */
	private String grupoEconomico;
	
	/** Atributo atividadeEconomica. */
	private String atividadeEconomica;
	
	/** Atributo segmento. */
	private String segmento;
	
	/** Atributo subSegmento. */
	private String subSegmento;
	
	/** Atributo empresaConglomerado. */
	private String empresaConglomerado;
	
	/** Atributo dataHoraSolicitacao. */
	private String dataHoraSolicitacao;
	
	/** Atributo usuarioSolicitacao. */
	private String usuarioSolicitacao;
	
	/** Atributo situacaoSolic. */
	private String situacaoSolic;
	
	/** Atributo motivoSituacao. */
	private String motivoSituacao;
	
	/** Atributo destinoSolicitacao. */
	private String destinoSolicitacao;	
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo canalManutencao. */
	private String canalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;	
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo canalInclusao. */
	private String canalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;	
	
	/** Atributo listaGridDetSolicitacaoRecuperacaoRemessa. */
	private List<OcorrenciasDetalharSolicRecuperacaoSaidaDTO> listaGridDetSolicitacaoRecuperacaoRemessa = new ArrayList<OcorrenciasDetalharSolicRecuperacaoSaidaDTO>();
	
	/** Atributo qtdeRegistrosSelecionados. */
	private Integer qtdeRegistrosSelecionados;
	
	/** Atributo tipoFiltroPesquisa. */
	private Integer tipoFiltroPesquisa;
	
	/** Atributo nrContrato. */
	private Integer nrContrato;
	
	/** Atributo descContrato. */
	private String descContrato;
	
	/** Atributo descSituacao. */
	private String descSituacao;
	
	
	
	//Info cliente contrato
	
	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado;

	/** Atributo cpfCnpjMaster. */
	private String cpfCnpjMaster;

	/** Atributo nomeRazaoSocialMaster. */
	private String nomeRazaoSocialMaster;

	/** Atributo grupoEconomicoMaster. */
	private String grupoEconomicoMaster;

	/** Atributo atividadeEconomicaMaster. */
	private String atividadeEconomicaMaster;

	/** Atributo segmentoMaster. */
	private String segmentoMaster;

	/** Atributo subSegmentoMaster. */
	private String subSegmentoMaster;

	/** Atributo empresa. */
	private String empresa;

	/** Atributo cdEmpresa. */
	private Long cdEmpresa;

	/** Atributo tipo. */
	private String tipo;

	/** Atributo cdTipo. */
	private Integer cdTipo;

	/** Atributo numero. */
	private String numero;

	/** Atributo numeroContrato. */
	private Long numeroContrato;

	/** Atributo motivoDesc. */
	private String motivoDesc;

	/** Atributo situacaoDesc. */
	private String situacaoDesc;

	/** Atributo participacao. */
	private String participacao;

	/** Atributo possuiAditivos. */
	private String possuiAditivos;

	/** Atributo dataHoraCadastramento. */
	private String dataHoraCadastramento;

	/** Atributo inicioVigencia. */
	private String inicioVigencia;

	/** Atributo cdAgenciaGestora. */
	private int cdAgenciaGestora;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;

	/** Atributo cdGerenteResponsavel. */
	private Long cdGerenteResponsavel;
	
	/** Atributo dsNomeRazaoSocialParticipante. */
	private String dsNomeRazaoSocialParticipante;
	
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo cpfCnpj. */
	private String cpfCnpj;

	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;

	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos = false;
	
	/** Atributo renegociavel. */
	private String renegociavel;
	
	/** Atributo saidaTipoLayoutArquivoSaidaDTO. */
	private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;
	
//	M�todos - In�cio
	
	/**
 * Carrega lista situacao solicitacao.
 */
public void carregaListaSituacaoSolicitacao(){
		try{
			listaSituacaoSolicitacao = new ArrayList<SelectItem>();
				
			List<ListarSituacaoSolicitacaoSaidaDTO>  listaSaida = comboService.listarSituacaoSolicitacao();
			
			for (int i = 0; i < listaSaida.size(); i++) {
				
				listaSituacaoSolicitacao.add(new SelectItem(listaSaida.get(i).getCdSolicitacaoPagamentoIntegrado(),listaSaida.get(i).getDsTipoSolicitacaoPagamento()));
		
			}
		}catch (PdcAdapterFunctionalException p){
			listaSituacaoSolicitacao = new ArrayList<SelectItem>();
		}		
	}
	
  	/**
	   * Limpar args lista apos pesquisa cliente contrato.
	   *
	   * @param evt the evt
	   */
	  public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt){
  		limparCampos();
	}	
  	
  	/**
	   * Limpar args lista apos pesquisa cliente contrato consultar.
	   *
	   * @param evt the evt
	   */
	  public void limparArgsListaAposPesquisaClienteContratoConsultar(ActionEvent evt){
  		limparCamposConsultar();
	}	
  	
  	/**
	   * Consultar dados grid consultar.
	   *
	   * @param evt the evt
	   */
	  public void consultarDadosGridConsultar(ActionEvent evt){
  		consultarDadosGridConsultar();
  	}
  	
  	/**
	   * Consultar dados grid consultar.
	   *
	   * @return the string
	   */
	  public String consultarDadosGridConsultar(){
		if (getSequenciaRemessaFiltro() == null) {
			setSequenciaRemessa(0L);
		} else {
			setSequenciaRemessa(getSequenciaRemessaFiltro());
		}
		
		try {
			ConsultarSolicRecuperacaoEntradaDTO entradaDTO = new ConsultarSolicRecuperacaoEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteContratoBean.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(identificacaoClienteContratoBean.getCdTipoContratoNegocio());
			entradaDTO.setNrSequenciaContratoNegocio(identificacaoClienteContratoBean.getNrSequenciaContratoNegocio());
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			entradaDTO.setDtFinalSolicitacao((getDataSolicitacaoAte() == null) ? "" : sdf.format(getDataSolicitacaoAte()));
			entradaDTO.setDtInicioSolicitacao((getDataSolicitacaoDe() == null) ? "" : sdf.format(getDataSolicitacaoDe()));
			entradaDTO.setCdUsuarioSolicitacao(getUsuario());    
			entradaDTO.setCdSituacaoSolicitacaoRecuperacao(getCboSituacaoSolicitacao());

			setListaGridContrato(getArquivoRemessaServiceImpl().consultarSolicRecuperacao(entradaDTO));
			
			listaControleRadioConsultar = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridContrato().size(); i++) {
				this.listaControleRadioConsultar.add(new SelectItem(i, " "));
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
		
		return "";
  	}
  
	//habilita btn consultar
	/**
	 * Controlar argumentos pesquisa.
	 */
	public void controlarArgumentosPesquisa(){
		if (identificacaoClienteContratoBean.getHabilitaFiltroDeCliente()){
			setHabilitaArgumentosPesquisa(false);
			limparConSolicitacaoRecRemessa();
		}
		else{
			setHabilitaArgumentosPesquisa(true);
		}
	}
	
	/**
	 * Limpar campos.
	 */
	public void limparCampos() {
		setCheckTudo(false);
		setArquivoRetorno(false);
		setListaGridIncluir(null);
		setQtdeRegistrosSelecionados(0);
		setArquivoISD(false);
		setRelatorio(false);
		setItemLayoutArquivo(0);
		setItemResultProcessamento(0);
		setItemSituaProcessamento(0);
		setSequenciaRemessaFiltro(null);
		setDataRecepcaoDe(new Date());
		setDataRecepcaoAte(new Date());
	}
	
	/**
	 * Limpar campos consultar.
	 */
	public void limparCamposConsultar(){
		setUsuario(null);
		setItemSelecionadoListaContrato(null);
		setCboSituacaoSolicitacao(0);
		setListaGridContrato(null);
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
	}
	
	/**
	 * Limpar campos tipo filtro.
	 */
	public void limparCamposTipoFiltro(){
		identificacaoClienteContratoBean.limparCampos();
	}
	
	/**
	 * Limpar con solicitacao rec remessa.
	 */
	public void limparConSolicitacaoRecRemessa(){
		setUsuario(null);
		setItemSelecionadoListaContrato(null);
		setCboSituacaoSolicitacao(0);
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
		setListaGridContrato(null);
		setSequenciaRemessaFiltro(null);

		identificacaoClienteContratoBean.limparTela();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparGerenteResponsavel();
		
		
//		filtroConSolicitacaoRecuperacaoRemessaBean.setTipoFiltroSelecionado(null);
//		filtroConSolicitacaoRecuperacaoRemessaBean.setItemFiltroSelecionado(null);
//		filtroConSolicitacaoRecuperacaoRemessaBean.setSituacaoDescContrato(null);
//		filtroConSolicitacaoRecuperacaoRemessaBean.setDescricaoContratoDescContrato(null);
//		filtroConSolicitacaoRecuperacaoRemessaBean.setNumeroDescContrato(null);
//		filtroConSolicitacaoRecuperacaoRemessaBean.setEmpresaGestoraDescContrato(null);
//		filtroConSolicitacaoRecuperacaoRemessaBean.setNumeroFiltro(null);
//		
//		filtroConSolicitacaoRecuperacaoRemessaBean.setTipoFiltroSelecionado(null); 
//		filtroConSolicitacaoRecuperacaoRemessaBean.setTipoContratoFiltro(null);
//		filtroConSolicitacaoRecuperacaoRemessaBean.setEmpresaGestoraFiltro(null);
//	
//		filtroConSolicitacaoRecuperacaoRemessaBean.setSituacaoDescCliente(null);
//		filtroConSolicitacaoRecuperacaoRemessaBean.setDescricaoContratoDescCliente(null);
//		filtroConSolicitacaoRecuperacaoRemessaBean.setNumeroDescCliente(null);
//		filtroConSolicitacaoRecuperacaoRemessaBean.setEmpresaGestoraDescCliente(null);
//		
//		filtroConSolicitacaoRecuperacaoRemessaBean.limparFiltroPrincipal();
//		filtroConSolicitacaoRecuperacaoRemessaBean.setItemClienteSelecionadoContrato(null);
//		filtroConSolicitacaoRecuperacaoRemessaBean.setClienteContratoSelecionado(false);
	}
	 
	/**
	 * Limpar filtro principal.
	 */
	public void limparFiltroPrincipal(){
		identificacaoClienteContratoBean.limparCampos();
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		
		setUsuario(null);
		setItemSelecionadoListaContrato(null);
		setHabilitaArgumentosPesquisa(false);
		setListaGridContrato(null);
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
		setCboSituacaoSolicitacao(0);
	} 
	
	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente(){
		//Limpar Argumentos de Pesquisa e Lista
		filtroConSolicitacaoRecuperacaoRemessaBean.limpar();
		setListaGridContrato(null);
		setItemSelecionadoListaContrato(null);
		
		limparCampos();
		
		return "";
	}	
	
	/**
	 * Inicializa tela.
	 *
	 * @param evt the evt
	 */
	public void inicializaTela(ActionEvent evt){//metodo utilizado como action inicial
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
		setUsuario(null);
		setItemSelecionadoListaContrato(null);
		setHabilitaArgumentosPesquisa(false);
		filtroConSolicitacaoRecuperacaoRemessaBean.setClienteContratoSelecionado(false);
		filtroConSolicitacaoRecuperacaoRemessaBean.setClienteContratoSelecionado(false);
		filtroConSolicitacaoRecuperacaoRemessaBean.setTipoFiltroSelecionado(null);
		carregaListaSituacaoSolicitacao();
		// limpar todos os campos da tela
		
//		filtroConSolicitacaoRecuperacaoRemessaBean.listarEmpresaGestora();
//		filtroConSolicitacaoRecuperacaoRemessaBean.listarTipoContrato();
//		filtroConSolicitacaoRecuperacaoRemessaBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
//		
//		filtroConSolicitacaoRecuperacaoRemessaBean.setPaginaRetorno("conSolicitacaoRecuperacaoRemessa");
		setDisableArgumentosConsulta(false);
		setTipoFiltroPesquisa(null);
		
		
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		
		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteManterContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conSolicitacaoRecuperacaoRemessa");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		identificacaoClienteContratoBean.setTipoContratoFiltro(null);
		
		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");

		identificacaoClienteContratoBean.limparRadioFiltro();
		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		identificacaoClienteContratoBean.setBloqueiaRadio(true);
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
		
	}
  
	/**
	 * Limpar dados pesquisa contrato.
	 */
	public void limparDadosPesquisaContrato(){
		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		limparTudo();
	}
	
	/**
	 * Limpar pesquisa contrato.
	 */
	public void limparPesquisaContrato(){
		identificacaoClienteContratoBean.limparTela();
		limparTudo();
	}
	
	 /**
 	 * Limpar tela incluir.
 	 */
 	public void limparTelaIncluir(){
		setDataRecepcaoAte(new Date());
		setDataRecepcaoDe(new Date());
		
		setItemSituaProcessamento(0);
		setItemResultProcessamento(0);
		setItemLayoutArquivo(0);
		setListaGridIncluir(null);
		setQtdeRegistrosSelecionados(0);
		setCheckTudo(false);
		setRelatorio(false);
		setArquivoISD(false);
		setArquivoRetorno(false);
		setSequenciaRemessaFiltro(null);
				
		filtroIncSolicitacaoRecuperacaoRemessaBean.setClienteContratoSelecionado(false);
	 }
	 
     /**
      * Limpar filtro incluir.
      */
     public void limparFiltroIncluir(){
    	limparTelaIncluir();
    	filtroIncSolicitacaoRecuperacaoRemessaBean.limparFiltroPrincipal();
 		filtroIncSolicitacaoRecuperacaoRemessaBean.setItemFiltroSelecionado("");
	 }
     
 	 /**
 	  * Limpar apos alterar opcao cliente incluir.
 	  *
 	  * @return the string
 	  */
 	 public String limparAposAlterarOpcaoClienteIncluir(){
		//Limpar Argumentos de Pesquisa e Lista
 		filtroIncSolicitacaoRecuperacaoRemessaBean.limpar();
		setListaGridIncluir(null);
		
		limparTelaIncluir();
		
		return "";
	 }     
     
	/**
	 * Limpar filtro resultado.
	 */
	public void limparFiltroResultado(){
		setItemResultProcessamento(0);
	}
		
	/**
	 * Limpar incluir.
	 */
	public void limparIncluir(){
		setDataRecepcaoAte(new Date());
		setDataRecepcaoDe(new Date());
		setSequenciaRemessaFiltro(null);
		setItemSituaProcessamento(0);
		setItemResultProcessamento(0);
		setItemLayoutArquivo(0);
		setListaGridIncluir(null);
		setQtdeRegistrosSelecionados(0);
		setCheckTudo(false);
		setRelatorio(false);
		setArquivoISD(false);
		setArquivoRetorno(false);
		setSequenciaRemessa(null);
		setQtdeRegistrosSelecionados(0);
		
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setTipoFiltroSelecionado(null);
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setItemFiltroSelecionado(null);
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setSituacaoDescContrato(null);
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setDescricaoContratoDescContrato(null);
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setNumeroDescContrato(null);
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setEmpresaGestoraDescContrato(null);
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setNumeroFiltro(null);
//		
//
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setTipoContratoFiltro(null);
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setEmpresaGestoraFiltro(null);
//		
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setSituacaoDescCliente(null);
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setDescricaoContratoDescCliente(null);
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setNumeroDescCliente(null);
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setEmpresaGestoraDescCliente(null);
//			
//		filtroIncSolicitacaoRecuperacaoRemessaBean.limparFiltroPrincipal();
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setItemClienteSelecionadoContrato(null);
//		filtroIncSolicitacaoRecuperacaoRemessaBean.setClienteContratoSelecionado(false);
	}
	
	/**
	 * Carrega lista layout arquivo.
	 */
	private void carregaListaLayoutArquivo(){
		listarTipoLayoutArquivo = new ArrayList<SelectItem>();
		TipoLayoutArquivoEntradaDTO entradaDTO = new TipoLayoutArquivoEntradaDTO(); 
		try{
			saidaTipoLayoutArquivoSaidaDTO = comboService.listarTipoLayoutArquivo(entradaDTO);
			List<TipoLayoutArquivoSaidaDTO> listaSaida = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
		
		   for (TipoLayoutArquivoSaidaDTO saida : listaSaida ){			
			listarTipoLayoutArquivo.add(new SelectItem(saida.getCdTipoLayoutArquivo(),saida.getDsTipoLayoutArquivo()));
		   }
		   
		}catch(PdcAdapterFunctionalException e){	
			listarTipoLayoutArquivo = new ArrayList<SelectItem>();
		}
	}
		
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		
		limparIncluir();
	
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		
		identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		identificacaoClienteContratoBean.setPaginaRetorno("conSolicitacaoRecuperacaoRemessa");
		listarSituacaoProcessamento();
		listarResultadoProcessamento();
		carregaListaLayoutArquivo();
		identificacaoClienteContratoBean.consultarContrato();
		ListarContratosPgitSaidaDTO saidaDTO = obterContrato();

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDescricaoContrato(saidaDTO.getDsContrato());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getNmFuncionarioBradesco());
		
		
		setDataHoraCadastramento(null);
		
		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		
		if (getDataHoraCadastramento() != null) {
		
			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
			String stringData = getDataHoraCadastramento().substring(0, 19);
		
			try {
				setDataHoraCadastramento(formato2.format(formato1.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}
		}
		
		// setInicioVigencia(inicioVigencia)
		if (identificacaoClienteContratoBean.getHabilitaFiltroDeCliente() == true) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getListaConsultarListaClientePessoas().get(identificacaoClienteContratoBean.getItemClienteSelecionado()).getCnpjOuCpfFormatado());
			setDsNomeRazaoSocialParticipante(identificacaoClienteContratoBean.getListaConsultarListaClientePessoas().get(identificacaoClienteContratoBean.getItemClienteSelecionado()).getDsNomeRazao());
		
		
		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}
		
	    return "INCLUIR";
	}
	
	/**
	 * Listar situacao processamento.
	 */
	public void listarSituacaoProcessamento(){
		try{
			listaSituacaoProcessamento = new ArrayList<SelectItem>();
			List<ListarSituacaoRemessaSaidaDTO> listaSaida = new ArrayList<ListarSituacaoRemessaSaidaDTO>();
			listaSaida = this.getComboService().listarSituacaoRemessa();
			
			for(ListarSituacaoRemessaSaidaDTO combo : listaSaida){
				this.listaSituacaoProcessamento.add(new SelectItem(combo.getCodigo(),combo.getDescricao()));
			}
		}catch (PdcAdapterFunctionalException p){
			listaSituacaoProcessamento = new ArrayList<SelectItem>();
		}
	}	
	
	/**
	 * Listar resultado processamento.
	 */
	public void listarResultadoProcessamento(){
		try{
			listaResultadoProcessamento = new ArrayList<SelectItem>();
			List<ListarResultadoProcessamentoRemessaSaidaDTO> listaSaida = new ArrayList<ListarResultadoProcessamentoRemessaSaidaDTO>();
			listaSaida = getComboService().listarResultadoProcessamentoRemessa();
			
			for(ListarResultadoProcessamentoRemessaSaidaDTO combo:listaSaida){
				this.listaResultadoProcessamento.add(new SelectItem(combo.getCodigo(),combo.getDescricao()));
			}
		}catch (PdcAdapterFunctionalException p){
			listaResultadoProcessamento = new ArrayList<SelectItem>();
		}		
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		   limparIncluir();
		   
		   setItemSelecionadoListaContrato(null);
	       return "VOLTAR_INCLUIR";
	}
	
	/**
	 * Voltar confirmar.
	 *
	 * @return the string
	 */
	public String voltarConfirmar(){
		for(int i=0; i< getListaGridConfirmar().size(); i++ ){
			getListaGridConfirmar().get(i).setCheck(false);
		}
		
		setCheckTudo(false);
		setQtdeRegistrosSelecionados(0);
       return "VOLTAR_CONFIRMAR";
	}
	
	/**
	 * Confirmar.
	 *
	 * @return the string
	 */
	public String confirmar(){
	
		listaGridConfirmar = new ArrayList<ConsultarArquivoRecuperacaoSaidaDTO>();

		for (int i = 0; i < getListaGridIncluir().size(); i++) {
			if(getListaGridIncluir().get(i).isCheck()){
				listaGridConfirmar.add(getListaGridIncluir().get(i));			        				
			}
		}
	   
		if(isRelatorio()){
			setRelatorioCheck("X");
		}
		else{
			setRelatorioCheck("");
		}
		
		if(isArquivoISD()){
			setArquivoISDCheck("X");
		}else{
			setArquivoISDCheck("");
		}
		   
		if(isArquivoRetorno()){
			setArquivoRetornoCheck("X");
		}else{
			setArquivoRetornoCheck("");
		}
		   
		return "CONFIRMAR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		
		try{
			IncluirSolicRecuperacaoEntradaDTO entradaDTO = new IncluirSolicRecuperacaoEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteContratoBean.getEmpresaGestoraFiltro());
			entradaDTO.setCdTipoContratoNegocio(identificacaoClienteContratoBean.getTipoContratoFiltro());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(identificacaoClienteContratoBean.getNumeroFiltro()));
			
			entradaDTO.setCdDestinoRelat(getRelatorioCheck());
			entradaDTO.setCdDestinoIsd(getArquivoISDCheck());
			entradaDTO.setCdDestinoRet(getArquivoRetornoCheck());
			entradaDTO.setQtdeOcorrencias(getListaGridConfirmar().size());  
			
			List <ConsultarArquivoRecuperacaoSaidaDTO> listaEntrada = new ArrayList<ConsultarArquivoRecuperacaoSaidaDTO>();
			
			ConsultarArquivoRecuperacaoSaidaDTO listaEntradaDTO;
			
			for (int i=0; i < getListaGridConfirmar().size(); i++){   
				listaEntradaDTO = new ConsultarArquivoRecuperacaoSaidaDTO();
				listaEntradaDTO.setCdPessoa(listaGridConfirmar.get(i).getCdPessoa());
				listaEntradaDTO.setCdTipoLayoutArquivo(listaGridConfirmar.get(i).getCdTipoLayoutArquivo());
				listaEntradaDTO.setCdClienteTransfArquivo(listaGridConfirmar.get(i).getCdClienteTransfArquivo());
				listaEntradaDTO.setNrArquivoRemessa(listaGridConfirmar.get(i).getNrArquivoRemessa());
				listaEntradaDTO.setHrInclusaoRegistro(listaGridConfirmar.get(i).getHrInclusaoRegistro());
				listaEntrada.add(listaEntradaDTO);  	    	  
	     	} 	
		 
			entradaDTO.setListaIncluirSolicRecuperacao(listaEntrada);

			IncluirSolicRecuperacaoSaidaDTO saidaIncluirDTO =  (getArquivoRemessaServiceImpl().incluirSolicRecuperacao(entradaDTO));

			BradescoFacesUtils.addInfoModalMessage("(" +saidaIncluirDTO.getCodMensagem() + ") " + saidaIncluirDTO.getMensagem(), "conSolicitacaoRecuperacaoRemessa", BradescoViewExceptionActionType.ACTION,false);
			
			if (listaGridContrato != null && listaGridContrato.size() >0){
				consultarDadosGridConsultar();
			}
		
		}catch(PdcAdapterFunctionalException e){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
		    return"";
		}

		return "";
	}
		
	/**
	 * Popula grid incluir.
	 *
	 * @return the string
	 */
	public String populaGridIncluir(){

		listaGridIncluir = new ArrayList<ConsultarArquivoRecuperacaoSaidaDTO>();

		try {
			ConsultarArquivoRecuperacaoEntradaDTO entradaDTO = new ConsultarArquivoRecuperacaoEntradaDTO();
		
			entradaDTO.setCdPessoa(0L);
			entradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteContratoBean.getEmpresaGestoraFiltro());
			entradaDTO.setCdTipoContratoNegocio(identificacaoClienteContratoBean.getTipoContratoFiltro());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(identificacaoClienteContratoBean.getNumeroFiltro()));
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			entradaDTO.setDtFinalInclusaoRemessa((getDataRecepcaoAte() == null) ? "" : sdf.format(getDataRecepcaoAte()) );
			entradaDTO.setDtInicioInclusaoRemessa((getDataRecepcaoDe() == null) ? "" : sdf.format(getDataRecepcaoDe()));

			entradaDTO.setCdTipoLayoutArquivo(getItemLayoutArquivo());
			entradaDTO.setNrArquivoRemessa(getSequenciaRemessa());
			entradaDTO.setCdSituacaoProcessamentoRemessa(getItemSituaProcessamento());
			entradaDTO.setCdCondicaoProcessamentoRemessa(getItemResultProcessamento());

			setListaGridIncluir(getArquivoRemessaServiceImpl().consultarArquivoRecuperacao(entradaDTO));

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			
	    	setListaGridIncluir(null);
			setCheckTudo(false);
			setRelatorio(false);
			setArquivoISD(false);
			setArquivoRetorno(false);
		}
		
		return "";
	}

	/**
	 * Preenche dados det solicitacao recuperacao remessa.
	 *
	 * @return the string
	 */
	public String preencheDadosDetSolicitacaoRecuperacaoRemessa(){
		carregaListaGridDetSolicitacaoRecuperacaoRemessa();		
		return "DETALHAR_SOLICITACAO_REC_REMESSA";
	}
	
	/**
	 * Carrega lista grid det solicitacao recuperacao remessa.
	 */
	public void carregaListaGridDetSolicitacaoRecuperacaoRemessa() {
		listaGridDetSolicitacaoRecuperacaoRemessa = new ArrayList<OcorrenciasDetalharSolicRecuperacaoSaidaDTO>();

		//filtroConSolicitacaoRecuperacaoRemessaBean.setEmpresaGestoraDescContrato(filtroConSolicitacaoRecuperacaoRemessaBean.getListaEmpresaGestoraHash().get(getFiltroConSolicitacaoRecuperacaoRemessaBean().getCdPessoaJuridicaContrato()));
		
		// Registro do da Tela Anterior
		ConsultarSolicRecuperacaoSaidaDTO registroSelecionado = getListaGridContrato().get(getItemSelecionadoListaContrato());

		try {
			DetalharSolicRecuperacaoEntradaDTO entradaDTO = new DetalharSolicRecuperacaoEntradaDTO();
			DetalharSolicRecuperacaoSaidaDTO saidaDTO = new DetalharSolicRecuperacaoSaidaDTO();
			setDataHoraInclusao(saidaDTO.getDataFormatadaInclusaoRemessa());

			entradaDTO.setNrSolicitacaoRecuperacao(registroSelecionado.getNrSolicitacaoRecuperacao());

			saidaDTO = getArquivoRemessaServiceImpl().detalharSolicRecuperacao(entradaDTO);

			setDataHoraSolicitacao(registroSelecionado.getDataFormatada());

			setUsuarioSolicitacao(registroSelecionado.getCdUsuarioSolicitacao());
			setSituacaoSolic(registroSelecionado.getDsSituacaoSolicitacaoRecuperacao());
			setMotivoSituacao(saidaDTO.getDsMotvtoSituacaoRecuperacao());

			
			setDestinoSolicitacao(registroSelecionado.getDsDestinoRecuperacao());

			setListaGridDetSolicitacaoRecuperacaoRemessa(saidaDTO.getListaDetalharSolicRecuperacao());
			setDataHoraInclusao(saidaDTO.getDataFormatadaInclusaoRemessa());
			setUsuarioInclusao(saidaDTO.getCdAutenticacaoSegregacaoInclusao());
			setCanalInclusao(CamposUtils.formatadorCampos(saidaDTO.getCdCanalInclusao(), saidaDTO.getDsCanalInclusao()));			
			setComplementoInclusao(saidaDTO.getNmOperacaoFluxoInclusao());
			
			setDataHoraManutencao(saidaDTO.getDataFormatadaManutencaoRemessa());
			setUsuarioManutencao(saidaDTO.getCdAutenticacaoSegregacaoManutencao());			
			setCanalManutencao(CamposUtils.formatadorCampos(saidaDTO.getCdCanalManutencao(),saidaDTO.getDsCanalManutencao() ));
			setComplementoManutencao(saidaDTO.getNmOperacaoFluxoManutencao());

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridDetSolicitacaoRecuperacaoRemessa(null);
		}
	}
	
	/**
	 * Voltar consultar solic remessa.
	 *
	 * @return the string
	 */
	public String voltarConsultarSolicRemessa(){
		setItemSelecionadoListaContrato(null);
		
		return "CONSULTAR_SOLICITACAO_REC_REMESSA";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {
		try {
			
			ExcluirSolicRecuperacaoEntradaDTO entradaDTO = new ExcluirSolicRecuperacaoEntradaDTO();

			entradaDTO.setNrSolicitacaoRecuperacao(getListaGridContrato().get(getItemSelecionadoListaContrato()).getNrSolicitacaoRecuperacao());
			entradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteContratoBean.getEmpresaGestoraFiltro());
			entradaDTO.setCdTipoContratoNegocio(identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista() == null ? 0 : identificacaoClienteContratoBean.getItemSelecionadoLista()).getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista() == null ? 0 : identificacaoClienteContratoBean.getItemSelecionadoLista()).getNrSequenciaContrato());

			ExcluirSolicRecuperacaoSaidaDTO saidaDTO = getArquivoRemessaServiceImpl().excluirSolicRecuperacao(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conSolicitacaoRecuperacaoRemessa", "#{solicitacaoRecuperacaoRemessaBean.consultarDadosGridConsultar}", false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}

		return "";
	}
		
	
	/**
	 * Consultar cliente.
	 *
	 * @return the string
	 */
	public String consultarCliente(){
		
	    return "CONSULTARCLIENTE";
	}
	
	/**
	 * Consultar contrato.
	 *
	 * @return the string
	 */
	public String consultarContrato(){
	
	   return "CONSULTARCONTRATO";
	}

	/**
	 * Consultar grid.
	 */
	public void consultarGrid(){
	
	}
	
	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar(){
		setDataSolicitacaoAte(null);
		setDataSolicitacaoDe(null);
		setUsuario(null);
		this.setItemSelecionadoSituacaoSolicitacao(null);
		this.setItemSelecionadoListaContrato(null);
		
		return "conManterFavorecido";
	}
	
	/**
	 * Limpar grid.
	 *
	 * @return the string
	 */
	public String limparGrid(){
		setItemSelecionadoListaContrato(null);
		setListaGridContrato(null);
		setDisableArgumentosConsulta(false);
		return "";
		
	}
	
	/**
	 * Limpar tudo.
	 *
	 * @return the string
	 */
	public String limparTudo(){
		
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
		setUsuario(null);
		setItemSelecionadoListaContrato(null);
		setHabilitaArgumentosPesquisa(false);
		filtroConSolicitacaoRecuperacaoRemessaBean.setClienteContratoSelecionado(false);
		filtroConSolicitacaoRecuperacaoRemessaBean.setTipoFiltroSelecionado(null);
		carregaListaSituacaoSolicitacao();
		// limpar todos os campos da tela
		
//		filtroConSolicitacaoRecuperacaoRemessaBean.listarEmpresaGestora();
//		filtroConSolicitacaoRecuperacaoRemessaBean.listarTipoContrato();
//		filtroConSolicitacaoRecuperacaoRemessaBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
//		
//		filtroConSolicitacaoRecuperacaoRemessaBean.setPaginaRetorno("conSolicitacaoRecuperacaoRemessa");
		setDisableArgumentosConsulta(false);
		setTipoFiltroPesquisa(null);
		
		
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		
		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteManterContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conSolicitacaoRecuperacaoRemessa");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		identificacaoClienteContratoBean.setTipoContratoFiltro(null);
		
		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");

		identificacaoClienteContratoBean.limparRadioFiltro();
		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		identificacaoClienteContratoBean.setBloqueiaRadio(true);
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(false);
		
		limparGrid();
		setCboSituacaoSolicitacao(null);
		filtroConSolicitacaoRecuperacaoRemessaBean.limparFiltroPrincipal();
		setDataRecepcaoDe(new Date());
		setDataRecepcaoAte(new Date());
		return "";
	}
	
	/**
	 * Limpar grid incluir.
	 *
	 * @return the string
	 */
	public String limparGridIncluir(){
		setListaGridIncluir(null);
		setCheckTudo(false);
		setArquivoISD(false);
		setArquivoRetorno(false);
		setRelatorio(false);
		return "";
		
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		carregaListaGridDetSolicitacaoRecuperacaoRemessa();
		identificacaoClienteContratoBean.consultarContrato();
		
		ListarContratosPgitSaidaDTO saidaDTO = obterContrato();

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDescricaoContrato(saidaDTO.getDsContrato());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getNmFuncionarioBradesco());
		setDataHoraCadastramento(null);
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		
		if (getDataHoraCadastramento() != null) {
		
			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
			String stringData = getDataHoraCadastramento().substring(0, 19);
		
			try {
				setDataHoraCadastramento(formato2.format(formato1.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}
		}
		
		// setInicioVigencia(inicioVigencia)
		if (identificacaoClienteContratoBean.getHabilitaFiltroDeCliente() == true) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getListaConsultarListaClientePessoas().get(identificacaoClienteContratoBean.getItemClienteSelecionado()).getCnpjOuCpfFormatado());
			setDsNomeRazaoSocialParticipante(identificacaoClienteContratoBean.getListaConsultarListaClientePessoas().get(identificacaoClienteContratoBean.getItemClienteSelecionado()).getDsNomeRazao());
		
		
		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}
		
		return "EXCLUIR";
	}

	/**
	 * Obter contrato.
	 *
	 * @return the listar contratos pgit saida dto
	 */
	private ListarContratosPgitSaidaDTO obterContrato() {
		ListarContratosPgitSaidaDTO saidaDTO = new ListarContratosPgitSaidaDTO();

		if (identificacaoClienteContratoBean.getSaidaContrato() != null) {
			saidaDTO = identificacaoClienteContratoBean.getSaidaContrato();
		}

		return saidaDTO;
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		carregaListaGridDetSolicitacaoRecuperacaoRemessa();
		identificacaoClienteContratoBean.consultarContrato();
		
		ListarContratosPgitSaidaDTO saidaDTO = obterContrato();

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDescricaoContrato(saidaDTO.getDsContrato());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getNmFuncionarioBradesco());
		
		setDataHoraCadastramento(null);
		
		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		
		if (getDataHoraCadastramento() != null) {
		
			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
			String stringData = getDataHoraCadastramento().substring(0, 19);
		
			try {
				setDataHoraCadastramento(formato2.format(formato1.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}
		}
		
		// setInicioVigencia(inicioVigencia)
		if (identificacaoClienteContratoBean.getHabilitaFiltroDeCliente() == true) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getListaConsultarListaClientePessoas().get(identificacaoClienteContratoBean.getItemClienteSelecionado()).getCnpjOuCpfFormatado());
			setDsNomeRazaoSocialParticipante(identificacaoClienteContratoBean.getListaConsultarListaClientePessoas().get(identificacaoClienteContratoBean.getItemClienteSelecionado()).getDsNomeRazao());
		
		
		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}
		
		return "DETALHAR";
	}
	
	/**
	 * Checa todos.
	 */
	public void checaTodos() {		
		for (int i = 0; i < (getListaGridIncluir().size()); i++) {
			getListaGridIncluir().get(i).setCheck(isCheckTudo());
		}
		setQtdeRegistrosSelecionados(isCheckTudo()?getListaGridIncluir().size():0);		
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param e the e
	 * @return the string
	 */
	public String pesquisar(ActionEvent e){		
		consultarDadosGridConsultar();
		return "";
	}
	
	/**
	 * Pesquisar incluir.
	 *
	 * @param e the e
	 * @return the string
	 */
	public String pesquisarIncluir(ActionEvent e){		
		populaGridIncluir();
		return "";
	}
	
	/**
	 * Pesquisar detalhar excluir.
	 *
	 * @param e the e
	 * @return the string
	 */
	public String pesquisarDetalharExcluir(ActionEvent e){		
		carregaListaGridDetSolicitacaoRecuperacaoRemessa();
		return "";
	}
	
	/**
	 * Limpar radio filtro.
	 */
	public void limparRadioFiltro(){
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
		setUsuario(null);
		setCboSituacaoSolicitacao(null);
		setListaGridContrato(null);
		identificacaoClienteContratoBean.limparRadioFiltro();
	}

	/**
	 * Is desabilita limpar e incluir.
	 *
	 * @return true, if is desabilita limpar e incluir
	 */
	public boolean isDesabilitaLimparEIncluir() {
		ListarContratosPgitSaidaDTO contrato = obterContrato();

		return contrato.getCdPessoaJuridica() == null
			   || contrato.getCdPessoaJuridica() == 0L
			   || contrato.getDsPessoaJuridica() == null
			   || contrato.getDsPessoaJuridica().trim().equals("");
	}
	
	/**
	 * Consultar contrato solicitacao recuperacao remessa.
	 *
	 * @return the string
	 */
	public String consultarContratoSolicitacaoRecuperacaoRemessa(){
		if (identificacaoClienteContratoBean.getObrigatoriedade() != null && identificacaoClienteContratoBean.getObrigatoriedade().equals("T")){

			// desmarca o radio da Grid - vreghini - 01/7/2010.
			identificacaoClienteContratoBean.setItemSelecionadoLista(null);
			identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

			try{
				ConsultarListaContratosPessoasEntradaDTO entradaDTO = new ConsultarListaContratosPessoasEntradaDTO();
				
				entradaDTO.setCdClub(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdClub()!=null?identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdClub():0L);
				entradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteContratoBean.getEmpresaGestoraFiltro()!=null?identificacaoClienteContratoBean.getEmpresaGestoraFiltro():0);
				entradaDTO.setCdTipoContratoNegocio(identificacaoClienteContratoBean.getTipoContratoFiltro()!=null?identificacaoClienteContratoBean.getTipoContratoFiltro():0);
				entradaDTO.setNrSeqContratoNegocio(identificacaoClienteContratoBean.getNumeroFiltro()!=null&&!identificacaoClienteContratoBean.getNumeroFiltro().equals("")?Long.valueOf(identificacaoClienteContratoBean.getNumeroFiltro()):Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdCpfCnpj());
				entradaDTO.setCdFilialCnpjPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdFilialCnpj());
				entradaDTO.setCdControleCpfPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdControleCnpj());
				
				identificacaoClienteContratoBean.setListaSaidaConsultarListaContratosPessoas(new ArrayList<ConsultarListaContratosPessoasSaidaDTO>());
				identificacaoClienteContratoBean.setListaSaidaConsultarListaContratosPessoas(identificacaoClienteContratoBean.getFiltroIdentificaoService().pesquisarContratos(entradaDTO));
				
				identificacaoClienteContratoBean.setSaidaConsultarListaContratosPessoas(identificacaoClienteContratoBean.getListaSaidaConsultarListaContratosPessoas().get(0));
				
				identificacaoClienteContratoBean.setEmpresaGestoraContrato(identificacaoClienteContratoBean.getSaidaConsultarListaContratosPessoas().getDescricaoPessoaJuridicaFormatada());
				identificacaoClienteContratoBean.setNumeroContrato(identificacaoClienteContratoBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio().toString());
				identificacaoClienteContratoBean.setDescricaoContrato(identificacaoClienteContratoBean.getSaidaConsultarListaContratosPessoas().getDsContrato());
				identificacaoClienteContratoBean.setSituacaoContrato(identificacaoClienteContratoBean.getSaidaConsultarListaContratosPessoas().getDsSituacaoContratoNegocio());
				identificacaoClienteContratoBean.setCdPessoaJuridicaContrato(identificacaoClienteContratoBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
				identificacaoClienteContratoBean.setNrSequenciaContratoNegocio(identificacaoClienteContratoBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
				identificacaoClienteContratoBean.setCdTipoContratoNegocio(identificacaoClienteContratoBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
				
				identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
			
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			}
		}
		
		return "";
	}	

	// INICIO GETS AND SETS
	/**
	 * Get: atividadeEconomica.
	 *
	 * @return atividadeEconomica
	 */
	public String getAtividadeEconomica() {
		return atividadeEconomica;
	}
	
	/**
	 * Set: atividadeEconomica.
	 *
	 * @param atividadeEconomica the atividade economica
	 */
	public void setAtividadeEconomica(String atividadeEconomica) {
		this.atividadeEconomica = atividadeEconomica;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: dataHoraSolicitacao.
	 *
	 * @return dataHoraSolicitacao
	 */
	public String getDataHoraSolicitacao() {
		return dataHoraSolicitacao;
	}
	
	/**
	 * Set: dataHoraSolicitacao.
	 *
	 * @param dataHoraSolicitacao the data hora solicitacao
	 */
	public void setDataHoraSolicitacao(String dataHoraSolicitacao) {
		this.dataHoraSolicitacao = dataHoraSolicitacao;
	}
	
	/**
	 * Get: destinoSolicitacao.
	 *
	 * @return destinoSolicitacao
	 */
	public String getDestinoSolicitacao() {
		return destinoSolicitacao;
	}
	
	/**
	 * Set: destinoSolicitacao.
	 *
	 * @param destinoSolicitacao the destino solicitacao
	 */
	public void setDestinoSolicitacao(String destinoSolicitacao) {
		this.destinoSolicitacao = destinoSolicitacao;
	}
	
	/**
	 * Get: empresaConglomerado.
	 *
	 * @return empresaConglomerado
	 */
	public String getEmpresaConglomerado() {
		return empresaConglomerado;
	}
	
	/**
	 * Set: empresaConglomerado.
	 *
	 * @param empresaConglomerado the empresa conglomerado
	 */
	public void setEmpresaConglomerado(String empresaConglomerado) {
		this.empresaConglomerado = empresaConglomerado;
	}
	
	/**
	 * Get: grupoEconomico.
	 *
	 * @return grupoEconomico
	 */
	public String getGrupoEconomico() {
		return grupoEconomico;
	}
	
	/**
	 * Set: grupoEconomico.
	 *
	 * @param grupoEconomico the grupo economico
	 */
	public void setGrupoEconomico(String grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}
	
	/**
	 * Get: segmento.
	 *
	 * @return segmento
	 */
	public String getSegmento() {
		return segmento;
	}
	
	/**
	 * Set: segmento.
	 *
	 * @param segmento the segmento
	 */
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}
	
	/**
	 * Get: subSegmento.
	 *
	 * @return subSegmento
	 */
	public String getSubSegmento() {
		return subSegmento;
	}
	
	/**
	 * Set: subSegmento.
	 *
	 * @param subSegmento the sub segmento
	 */
	public void setSubSegmento(String subSegmento) {
		this.subSegmento = subSegmento;
	}
	
	/**
	 * Get: canalInclusao.
	 *
	 * @return canalInclusao
	 */
	public String getCanalInclusao() {
		return canalInclusao;
	}
	
	/**
	 * Set: canalInclusao.
	 *
	 * @param tipoCanalInclusao the canal inclusao
	 */
	public void setCanalInclusao(String tipoCanalInclusao) {
		this.canalInclusao = tipoCanalInclusao;
	}
	
	/**
	 * Get: canalManutencao.
	 *
	 * @return canalManutencao
	 */
	public String getCanalManutencao() {
		return canalManutencao;
	}
	
	/**
	 * Set: canalManutencao.
	 *
	 * @param tipoCanalManutencao the canal manutencao
	 */
	public void setCanalManutencao(String tipoCanalManutencao) {
		this.canalManutencao = tipoCanalManutencao;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}	
	
	/**
	 * Get: itemSelecionadoListaContrato.
	 *
	 * @return itemSelecionadoListaContrato
	 */
	public Integer getItemSelecionadoListaContrato() {
		return itemSelecionadoListaContrato;
	}
	
	/**
	 * Set: itemSelecionadoListaContrato.
	 *
	 * @param itemSelecionadoListaContrato the item selecionado lista contrato
	 */
	public void setItemSelecionadoListaContrato(Integer itemSelecionadoListaContrato) {
		this.itemSelecionadoListaContrato = itemSelecionadoListaContrato;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: usuarioSolicitacao.
	 *
	 * @return usuarioSolicitacao
	 */
	public String getUsuarioSolicitacao() {
		return usuarioSolicitacao;
	}
	
	/**
	 * Set: usuarioSolicitacao.
	 *
	 * @param usuarioSolicitacao the usuario solicitacao
	 */
	public void setUsuarioSolicitacao(String usuarioSolicitacao) {
		this.usuarioSolicitacao = usuarioSolicitacao;
	}
	
	/**
	 * Get: situacaoSolic.
	 *
	 * @return situacaoSolic
	 */
	public String getSituacaoSolic() {
		return situacaoSolic;
	}
	
	/**
	 * Set: situacaoSolic.
	 *
	 * @param situacaoSolic the situacao solic
	 */
	public void setSituacaoSolic(String situacaoSolic) {
		this.situacaoSolic = situacaoSolic;
	}
	
	/**
	 * Get: listaGridDetSolicitacaoRecuperacaoRemessa.
	 *
	 * @return listaGridDetSolicitacaoRecuperacaoRemessa
	 */
	public List<OcorrenciasDetalharSolicRecuperacaoSaidaDTO> getListaGridDetSolicitacaoRecuperacaoRemessa() {
		return listaGridDetSolicitacaoRecuperacaoRemessa;
	}
	
	/**
	 * Set: listaGridDetSolicitacaoRecuperacaoRemessa.
	 *
	 * @param listaGridDetSolicitacaoRecuperacaoRemessa the lista grid det solicitacao recuperacao remessa
	 */
	public void setListaGridDetSolicitacaoRecuperacaoRemessa(
			List<OcorrenciasDetalharSolicRecuperacaoSaidaDTO> listaGridDetSolicitacaoRecuperacaoRemessa) {
		this.listaGridDetSolicitacaoRecuperacaoRemessa = listaGridDetSolicitacaoRecuperacaoRemessa;
	}
	
	/**
	 * Get: arquivoRemessaServiceImpl.
	 *
	 * @return arquivoRemessaServiceImpl
	 */
	public IArquivoRemessaService getArquivoRemessaServiceImpl() {
		return arquivoRemessaServiceImpl;
	}
	
	/**
	 * Set: arquivoRemessaServiceImpl.
	 *
	 * @param arquivoRemessaServiceImpl the arquivo remessa service impl
	 */
	public void setArquivoRemessaServiceImpl(
			IArquivoRemessaService arquivoRemessaServiceImpl) {
		this.arquivoRemessaServiceImpl = arquivoRemessaServiceImpl;
	}
	
	/**
	 * Get: motivoSituacao.
	 *
	 * @return motivoSituacao
	 */
	public String getMotivoSituacao() {
		return motivoSituacao;
	}
	
	/**
	 * Set: motivoSituacao.
	 *
	 * @param motivoSituacao the motivo situacao
	 */
	public void setMotivoSituacao(String motivoSituacao) {
		this.motivoSituacao = motivoSituacao;
	}
	
	/**
	 * Get: comboSituaProcessamento.
	 *
	 * @return comboSituaProcessamento
	 */
	public List<SelectItem> getComboSituaProcessamento() {
		return comboSituaProcessamento;
	}
	
	/**
	 * Set: comboSituaProcessamento.
	 *
	 * @param comboSituaProcessamento the combo situa processamento
	 */
	public void setComboSituaProcessamento(List<SelectItem> comboSituaProcessamento) {
		this.comboSituaProcessamento = comboSituaProcessamento;
	}
	
	/**
	 * Get: comboResultProcessamento.
	 *
	 * @return comboResultProcessamento
	 */
	public List<SelectItem> getComboResultProcessamento() {
		return comboResultProcessamento;
	}
	
	/**
	 * Set: comboResultProcessamento.
	 *
	 * @param resultadoProcessamento the combo result processamento
	 */
	public void setComboResultProcessamento(List<SelectItem> resultadoProcessamento) {
		this.comboResultProcessamento = resultadoProcessamento;
	}
	
	/**
	 * Get: listaGridIncluir.
	 *
	 * @return listaGridIncluir
	 */
	public List<ConsultarArquivoRecuperacaoSaidaDTO> getListaGridIncluir() {
		return listaGridIncluir;
	}
	
	/**
	 * Set: listaGridIncluir.
	 *
	 * @param listaGridIncluir the lista grid incluir
	 */
	public void setListaGridIncluir(
			List<ConsultarArquivoRecuperacaoSaidaDTO> listaGridIncluir) {
		this.listaGridIncluir = listaGridIncluir;
	}
	
	/**
	 * Is check tudo.
	 *
	 * @return true, if is check tudo
	 */
	public boolean isCheckTudo() {
		return checkTudo;
	}
	
	/**
	 * Set: checkTudo.
	 *
	 * @param checkTudo the check tudo
	 */
	public void setCheckTudo(boolean checkTudo) {
		this.checkTudo = checkTudo;
	}
	
	/**
	 * Is arquivo isd.
	 *
	 * @return true, if is arquivo isd
	 */
	public boolean isArquivoISD() {
		return arquivoISD;
	}
	
	/**
	 * Set: arquivoISD.
	 *
	 * @param arquivoISD the arquivo isd
	 */
	public void setArquivoISD(boolean arquivoISD) {
		this.arquivoISD = arquivoISD;
	}
	
	/**
	 * Is arquivo retorno.
	 *
	 * @return true, if is arquivo retorno
	 */
	public boolean isArquivoRetorno() {
		return arquivoRetorno;
	}
	
	/**
	 * Set: arquivoRetorno.
	 *
	 * @param arquivoRetorno the arquivo retorno
	 */
	public void setArquivoRetorno(boolean arquivoRetorno) {
		this.arquivoRetorno = arquivoRetorno;
	}
	
	/**
	 * Is relatorio.
	 *
	 * @return true, if is relatorio
	 */
	public boolean isRelatorio() {
		return relatorio;
	}
	
	/**
	 * Set: relatorio.
	 *
	 * @param relatorio the relatorio
	 */
	public void setRelatorio(boolean relatorio) {
		this.relatorio = relatorio;
	}
	
	/**
	 * Get: sequenciaRemessa.
	 *
	 * @return sequenciaRemessa
	 */
	public Long getSequenciaRemessa() {
		return sequenciaRemessa;
	}
	
	/**
	 * Set: sequenciaRemessa.
	 *
	 * @param sequenciaRemessa the sequencia remessa
	 */
	public void setSequenciaRemessa(Long sequenciaRemessa) {
		this.sequenciaRemessa = sequenciaRemessa;
	}
	
	/**
	 * Get: itemResultProcessamento.
	 *
	 * @return itemResultProcessamento
	 */
	public Integer getItemResultProcessamento() {
		return itemResultProcessamento;
	}
	
	/**
	 * Set: itemResultProcessamento.
	 *
	 * @param itemResultProcessamento the item result processamento
	 */
	public void setItemResultProcessamento(Integer itemResultProcessamento) {
		this.itemResultProcessamento = itemResultProcessamento;
	}
	
	/**
	 * Get: itemSituaProcessamento.
	 *
	 * @return itemSituaProcessamento
	 */
	public Integer getItemSituaProcessamento() {
		return itemSituaProcessamento;
	}
	
	/**
	 * Set: itemSituaProcessamento.
	 *
	 * @param itemSituaProcessamento the item situa processamento
	 */
	public void setItemSituaProcessamento(Integer itemSituaProcessamento) {
		this.itemSituaProcessamento = itemSituaProcessamento;
	}
	
	/**
	 * Get: itemLayoutArquivo.
	 *
	 * @return itemLayoutArquivo
	 */
	public Integer getItemLayoutArquivo() {
		return itemLayoutArquivo;
	}
	
	/**
	 * Set: itemLayoutArquivo.
	 *
	 * @param itemLayoutArquivo the item layout arquivo
	 */
	public void setItemLayoutArquivo(Integer itemLayoutArquivo) {
		this.itemLayoutArquivo = itemLayoutArquivo;
	}
	
	/**
	 * Get: listarTipoLayoutArquivo.
	 *
	 * @return listarTipoLayoutArquivo
	 */
	public List<SelectItem> getListarTipoLayoutArquivo() {
		return listarTipoLayoutArquivo;
	}
	
	/**
	 * Set: listarTipoLayoutArquivo.
	 *
	 * @param listarTipoLayoutArquivo the listar tipo layout arquivo
	 */
	public void setListarTipoLayoutArquivo(List<SelectItem> listarTipoLayoutArquivo) {
		this.listarTipoLayoutArquivo = listarTipoLayoutArquivo;
	}
	
	/**
	 * Get: comboLayoutArquivo.
	 *
	 * @return comboLayoutArquivo
	 */
	public List<SelectItem> getComboLayoutArquivo() {
		return comboLayoutArquivo;
	}
	
	/**
	 * Set: comboLayoutArquivo.
	 *
	 * @param comboLayoutArquivo the combo layout arquivo
	 */
	public void setComboLayoutArquivo(List<SelectItem> comboLayoutArquivo) {
		this.comboLayoutArquivo = comboLayoutArquivo;
	}
	
	/**
	 * Get: listaGridConfirmar.
	 *
	 * @return listaGridConfirmar
	 */
	public List<ConsultarArquivoRecuperacaoSaidaDTO> getListaGridConfirmar() {
		return listaGridConfirmar;
	}
	
	/**
	 * Set: listaGridConfirmar.
	 *
	 * @param listaGridConfirmar the lista grid confirmar
	 */
	public void setListaGridConfirmar(
			List<ConsultarArquivoRecuperacaoSaidaDTO> listaGridConfirmar) {
		this.listaGridConfirmar = listaGridConfirmar;
	}
	
	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}
	
	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}
	
	/**
	 * Get: filtroConSolicitacaoRecuperacaoRemessaBean.
	 *
	 * @return filtroConSolicitacaoRecuperacaoRemessaBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroConSolicitacaoRecuperacaoRemessaBean() {
		return filtroConSolicitacaoRecuperacaoRemessaBean;
	}
	
	/**
	 * Set: filtroConSolicitacaoRecuperacaoRemessaBean.
	 *
	 * @param filtroConSolicitacaoRecuperacaoRemessaBean the filtro con solicitacao recuperacao remessa bean
	 */
	public void setFiltroConSolicitacaoRecuperacaoRemessaBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroConSolicitacaoRecuperacaoRemessaBean) {
		this.filtroConSolicitacaoRecuperacaoRemessaBean = filtroConSolicitacaoRecuperacaoRemessaBean;
	}
	
	/**
	 * Get: filtroIncSolicitacaoRecuperacaoRemessaBean.
	 *
	 * @return filtroIncSolicitacaoRecuperacaoRemessaBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroIncSolicitacaoRecuperacaoRemessaBean() {
		return filtroIncSolicitacaoRecuperacaoRemessaBean;
	}
	
	/**
	 * Set: filtroIncSolicitacaoRecuperacaoRemessaBean.
	 *
	 * @param filtroIncSolicitacaoRecuperacaoRemessaBean the filtro inc solicitacao recuperacao remessa bean
	 */
	public void setFiltroIncSolicitacaoRecuperacaoRemessaBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroIncSolicitacaoRecuperacaoRemessaBean) {
		this.filtroIncSolicitacaoRecuperacaoRemessaBean = filtroIncSolicitacaoRecuperacaoRemessaBean;
	}
	
	/**
	 * Get: dataRecepcaoAte.
	 *
	 * @return dataRecepcaoAte
	 */
	public Date getDataRecepcaoAte() {
		return dataRecepcaoAte;
	}
	
	/**
	 * Is cliente contrato selecionado.
	 *
	 * @return true, if is cliente contrato selecionado
	 */
	public boolean isClienteContratoSelecionado() {
		return clienteContratoSelecionado;
	}
	
	/**
	 * Set: clienteContratoSelecionado.
	 *
	 * @param clienteContratoSelecionado the cliente contrato selecionado
	 */
	public void setClienteContratoSelecionado(boolean clienteContratoSelecionado) {
		this.clienteContratoSelecionado = clienteContratoSelecionado;
	}
	
	/**
	 * Set: dataRecepcaoAte.
	 *
	 * @param dataRecepcaoAte the data recepcao ate
	 */
	public void setDataRecepcaoAte(Date dataRecepcaoAte) {
		this.dataRecepcaoAte = dataRecepcaoAte;
	}
	
	/**
	 * Get: dataRecepcaoDe.
	 *
	 * @return dataRecepcaoDe
	 */
	public Date getDataRecepcaoDe() {
		return dataRecepcaoDe;
	}
	
	/**
	 * Set: dataRecepcaoDe.
	 *
	 * @param dataRecepcaoDe the data recepcao de
	 */
	public void setDataRecepcaoDe(Date dataRecepcaoDe) {
		this.dataRecepcaoDe = dataRecepcaoDe;
	}
	
	/**
	 * Get: arquivoISDCheck.
	 *
	 * @return arquivoISDCheck
	 */
	public String getArquivoISDCheck() {
		return arquivoISDCheck;
	}
	
	/**
	 * Set: arquivoISDCheck.
	 *
	 * @param arquivoISDCheck the arquivo isd check
	 */
	public void setArquivoISDCheck(String arquivoISDCheck) {
		this.arquivoISDCheck = arquivoISDCheck;
	}
	
	/**
	 * Get: arquivoRetornoCheck.
	 *
	 * @return arquivoRetornoCheck
	 */
	public String getArquivoRetornoCheck() {
		return arquivoRetornoCheck;
	}
	
	/**
	 * Set: arquivoRetornoCheck.
	 *
	 * @param arquivoRetornoCheck the arquivo retorno check
	 */
	public void setArquivoRetornoCheck(String arquivoRetornoCheck) {
		this.arquivoRetornoCheck = arquivoRetornoCheck;
	}
	
	/**
	 * Get: relatorioCheck.
	 *
	 * @return relatorioCheck
	 */
	public String getRelatorioCheck() {
		return relatorioCheck;
	}
	
	/**
	 * Set: relatorioCheck.
	 *
	 * @param relatorioCheck the relatorio check
	 */
	public void setRelatorioCheck(String relatorioCheck) {
		this.relatorioCheck = relatorioCheck;
	}
	
	/**
	 * Get: listaGridContrato.
	 *
	 * @return listaGridContrato
	 */
	public List<ConsultarSolicRecuperacaoSaidaDTO> getListaGridContrato() {
		return listaGridContrato;
	}
	
	/**
	 * Set: listaGridContrato.
	 *
	 * @param listaGridContrato the lista grid contrato
	 */
	public void setListaGridContrato(List<ConsultarSolicRecuperacaoSaidaDTO> listaGridContrato) {
		this.listaGridContrato = listaGridContrato;
	}
	
	/**
	 * Get: cboSituacaoSolicitacao.
	 *
	 * @return cboSituacaoSolicitacao
	 */
	public Integer getCboSituacaoSolicitacao() {
		return cboSituacaoSolicitacao;
	}
	
	/**
	 * Set: cboSituacaoSolicitacao.
	 *
	 * @param cboSituacaoSolicitacao the cbo situacao solicitacao
	 */
	public void setCboSituacaoSolicitacao(Integer cboSituacaoSolicitacao) {
		this.cboSituacaoSolicitacao = cboSituacaoSolicitacao;
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: listaSituacaoSolicitacao.
	 *
	 * @return listaSituacaoSolicitacao
	 */
	public List<SelectItem> getListaSituacaoSolicitacao() {
		return listaSituacaoSolicitacao;
	}
	
	/**
	 * Get: listarEmpresaConglomerado.
	 *
	 * @return listarEmpresaConglomerado
	 */
	public List<SelectItem> getListarEmpresaConglomerado() {
		return listarEmpresaConglomerado;
	}
	
	/**
	 * Set: listarEmpresaConglomerado.
	 *
	 * @param listarEmpresaConglomerado the listar empresa conglomerado
	 */
	public void setListarEmpresaConglomerado(List<SelectItem> listarEmpresaConglomerado) {
		this.listarEmpresaConglomerado = listarEmpresaConglomerado;
	}
	
	/**
	 * Get: bloqueiaBotao.
	 *
	 * @return bloqueiaBotao
	 */
	public Integer getBloqueiaBotao() {
		return bloqueiaBotao;
	}

	/**
	 * Set: bloqueiaBotao.
	 *
	 * @param bloqueiaBotao the bloqueia botao
	 */
	public void setBloqueiaBotao(Integer bloqueiaBotao) {
		this.bloqueiaBotao = bloqueiaBotao;
	}
	
	/**
	 * Get: dataSolicitacaoAte.
	 *
	 * @return dataSolicitacaoAte
	 */
	public Date getDataSolicitacaoAte() {
		return dataSolicitacaoAte;
	}
	
	/**
	 * Set: dataSolicitacaoAte.
	 *
	 * @param dataSolicitacaoAte the data solicitacao ate
	 */
	public void setDataSolicitacaoAte(Date dataSolicitacaoAte) {
		this.dataSolicitacaoAte = dataSolicitacaoAte;
	}
	
	/**
	 * Get: dataSolicitacaoDe.
	 *
	 * @return dataSolicitacaoDe
	 */
	public Date getDataSolicitacaoDe() {
		return dataSolicitacaoDe;
	}
	
	/**
	 * Set: dataSolicitacaoDe.
	 *
	 * @param dataSolicitacaoDe the data solicitacao de
	 */
	public void setDataSolicitacaoDe(Date dataSolicitacaoDe) {
		this.dataSolicitacaoDe = dataSolicitacaoDe;
	}
	
	/**
	 * Get: listaControleRadioConsultar.
	 *
	 * @return listaControleRadioConsultar
	 */
	public List<SelectItem> getListaControleRadioConsultar() {
		return listaControleRadioConsultar;
	}
	
	/**
	 * Set: listaControleRadioConsultar.
	 *
	 * @param listaControleRadioConsultar the lista controle radio consultar
	 */
	public void setListaControleRadioConsultar(List<SelectItem> listaControleRadioConsultar) {
		this.listaControleRadioConsultar = listaControleRadioConsultar;
	}
	
	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}
	
	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}
	
	/**
	 * Get: empresaGestoraContrato.
	 *
	 * @return empresaGestoraContrato
	 */
	public List<SelectItem> getEmpresaGestoraContrato() {
		return empresaGestoraContrato;
	}
	
	/**
	 * Set: empresaGestoraContrato.
	 *
	 * @param empresaGestoraContrato the empresa gestora contrato
	 */
	public void setEmpresaGestoraContrato(List<SelectItem> empresaGestoraContrato) {
		this.empresaGestoraContrato = empresaGestoraContrato;
	}
	
	/**
	 * Get: itemSelecionadoEmpresaGestoraContrato.
	 *
	 * @return itemSelecionadoEmpresaGestoraContrato
	 */
	public String getItemSelecionadoEmpresaGestoraContrato() {
		return itemSelecionadoEmpresaGestoraContrato;
	}
	
	/**
	 * Set: itemSelecionadoEmpresaGestoraContrato.
	 *
	 * @param itemSelecionadoEmpresaGestoraContrato the item selecionado empresa gestora contrato
	 */
	public void setItemSelecionadoEmpresaGestoraContrato(String itemSelecionadoEmpresaGestoraContrato) {
		this.itemSelecionadoEmpresaGestoraContrato = itemSelecionadoEmpresaGestoraContrato;
	}
	
	/**
	 * Get: listaAux.
	 *
	 * @return listaAux
	 */
	public List<ConsultarSolicRecuperacaoSaidaDTO> getListaAux() {
		return listaAux;
	}
	
	/**
	 * Set: listaAux.
	 *
	 * @param listaAux the lista aux
	 */
	public void setListaAux(List<ConsultarSolicRecuperacaoSaidaDTO> listaAux) {
		this.listaAux = listaAux;
	}
	
	/**
	 * Get: itemSelecionadoIdentificacaoCliente.
	 *
	 * @return itemSelecionadoIdentificacaoCliente
	 */
	public String getItemSelecionadoIdentificacaoCliente() {
		return itemSelecionadoIdentificacaoCliente;
	}
	
	/**
	 * Set: itemSelecionadoIdentificacaoCliente.
	 *
	 * @param itemSelecionadoIdentificacaoCliente the item selecionado identificacao cliente
	 */
	public void setItemSelecionadoIdentificacaoCliente(String itemSelecionadoIdentificacaoCliente) {
		this.itemSelecionadoIdentificacaoCliente = itemSelecionadoIdentificacaoCliente;
	}
	
	/**
	 * Get: itemSelecionadoSituacaoSolicitacao.
	 *
	 * @return itemSelecionadoSituacaoSolicitacao
	 */
	public String getItemSelecionadoSituacaoSolicitacao() {
		return itemSelecionadoSituacaoSolicitacao;
	}
	
	/**
	 * Set: itemSelecionadoSituacaoSolicitacao.
	 *
	 * @param itemSelecionadoSituacaoSolicitacao the item selecionado situacao solicitacao
	 */
	public void setItemSelecionadoSituacaoSolicitacao(String itemSelecionadoSituacaoSolicitacao) {
		this.itemSelecionadoSituacaoSolicitacao = itemSelecionadoSituacaoSolicitacao;
	}
	
	/**
	 * Get: itemSelecionadoTipoContrato.
	 *
	 * @return itemSelecionadoTipoContrato
	 */
	public String getItemSelecionadoTipoContrato() {
		return itemSelecionadoTipoContrato;
	}
	
	/**
	 * Set: itemSelecionadoTipoContrato.
	 *
	 * @param itemSelecionadoTipoContrato the item selecionado tipo contrato
	 */
	public void setItemSelecionadoTipoContrato(String itemSelecionadoTipoContrato) {
		this.itemSelecionadoTipoContrato = itemSelecionadoTipoContrato;
	}
	
	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}
	
	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	/**
	 * Set: listaSituacaoSolicitacao.
	 *
	 * @param cboSituacaoSolicitacao the lista situacao solicitacao
	 */
	public void setListaSituacaoSolicitacao(List<SelectItem> cboSituacaoSolicitacao) {
		this.listaSituacaoSolicitacao = cboSituacaoSolicitacao;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}
	
	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}
	
	/**
	 * Get: tipoContratoLista.
	 *
	 * @return tipoContratoLista
	 */
	public List<SelectItem> getTipoContratoLista() {
		return tipoContratoLista;
	}
	
	/**
	 * Set: tipoContratoLista.
	 *
	 * @param tipoContratoLista the tipo contrato lista
	 */
	public void setTipoContratoLista(List<SelectItem> tipoContratoLista) {
		this.tipoContratoLista = tipoContratoLista;
	}
	
	/**
	 * Get: usuario.
	 *
	 * @return usuario
	 */
	public String getUsuario() {
		return usuario;
	}
	
	/**
	 * Is bloqueia radio.
	 *
	 * @return true, if is bloqueia radio
	 */
	public boolean isBloqueiaRadio() {
		return bloqueiaRadio;
	}
	
	/**
	 * Set: bloqueiaRadio.
	 *
	 * @param bloqueiaRadio the bloqueia radio
	 */
	public void setBloqueiaRadio(boolean bloqueiaRadio) {
		this.bloqueiaRadio = bloqueiaRadio;
	}
	
	/**
	 * Set: usuario.
	 *
	 * @param usuario the usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	/**
	 * Get: sequenciaRemessaFiltro.
	 *
	 * @return sequenciaRemessaFiltro
	 */
	public Long getSequenciaRemessaFiltro() {
		return sequenciaRemessaFiltro;
	}
	
	/**
	 * Set: sequenciaRemessaFiltro.
	 *
	 * @param sequenciaRemessaFiltro the sequencia remessa filtro
	 */
	public void setSequenciaRemessaFiltro(Long sequenciaRemessaFiltro) {
		this.sequenciaRemessaFiltro = sequenciaRemessaFiltro;
	}
	
	/**
	 * Get: qtdeRegistrosSelecionados.
	 *
	 * @return qtdeRegistrosSelecionados
	 */
	public Integer getQtdeRegistrosSelecionados() {
		return qtdeRegistrosSelecionados;
	}
	
	/**
	 * Set: qtdeRegistrosSelecionados.
	 *
	 * @param qtdeRegistrosSelecionados the qtde registros selecionados
	 */
	public void setQtdeRegistrosSelecionados(Integer qtdeRegistrosSelecionados) {
		this.qtdeRegistrosSelecionados = qtdeRegistrosSelecionados;
	}
	
	/**
	 * Get: listaResultadoProcessamento.
	 *
	 * @return listaResultadoProcessamento
	 */
	public List<SelectItem> getListaResultadoProcessamento() {
		return listaResultadoProcessamento;
	}
	
	/**
	 * Set: listaResultadoProcessamento.
	 *
	 * @param listaResultadoProcessamento the lista resultado processamento
	 */
	public void setListaResultadoProcessamento(
			List<SelectItem> listaResultadoProcessamento) {
		this.listaResultadoProcessamento = listaResultadoProcessamento;
	}
	
	/**
	 * Get: listaSituacaoProcessamento.
	 *
	 * @return listaSituacaoProcessamento
	 */
	public List<SelectItem> getListaSituacaoProcessamento() {
		return listaSituacaoProcessamento;
	}
	
	/**
	 * Set: listaSituacaoProcessamento.
	 *
	 * @param listaSituacaoProcessamento the lista situacao processamento
	 */
	public void setListaSituacaoProcessamento(
			List<SelectItem> listaSituacaoProcessamento) {
		this.listaSituacaoProcessamento = listaSituacaoProcessamento;
	}
	
	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}
	
	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}
	
	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}
	
	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}
	
	/**
	 * Get: tipoFiltroPesquisa.
	 *
	 * @return tipoFiltroPesquisa
	 */
	public Integer getTipoFiltroPesquisa() {
		return tipoFiltroPesquisa;
	}
	
	/**
	 * Set: tipoFiltroPesquisa.
	 *
	 * @param tipoFiltroPesquisa the tipo filtro pesquisa
	 */
	public void setTipoFiltroPesquisa(Integer tipoFiltroPesquisa) {
		this.tipoFiltroPesquisa = tipoFiltroPesquisa;
	}
	
	/**
	 * Get: nrContrato.
	 *
	 * @return nrContrato
	 */
	public Integer getNrContrato() {
		return nrContrato;
	}
	
	/**
	 * Set: nrContrato.
	 *
	 * @param nrContrato the nr contrato
	 */
	public void setNrContrato(Integer nrContrato) {
		this.nrContrato = nrContrato;
	}
	
	/**
	 * Get: descContrato.
	 *
	 * @return descContrato
	 */
	public String getDescContrato() {
		return descContrato;
	}
	
	/**
	 * Set: descContrato.
	 *
	 * @param descContrato the desc contrato
	 */
	public void setDescContrato(String descContrato) {
		this.descContrato = descContrato;
	}
	
	/**
	 * Get: descSituacao.
	 *
	 * @return descSituacao
	 */
	public String getDescSituacao() {
		return descSituacao;
	}
	
	/**
	 * Set: descSituacao.
	 *
	 * @param descSituacao the desc situacao
	 */
	public void setDescSituacao(String descSituacao) {
		this.descSituacao = descSituacao;
	}
	
	/**
	 * Get: gerenteResponsavelFormatado.
	 *
	 * @return gerenteResponsavelFormatado
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}
	
	/**
	 * Set: gerenteResponsavelFormatado.
	 *
	 * @param gerenteResponsavelFormatado the gerente responsavel formatado
	 */
	public void setGerenteResponsavelFormatado(String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}
	
	/**
	 * Get: cpfCnpjMaster.
	 *
	 * @return cpfCnpjMaster
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}
	
	/**
	 * Set: cpfCnpjMaster.
	 *
	 * @param cpfCnpjMaster the cpf cnpj master
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}
	
	/**
	 * Get: nomeRazaoSocialMaster.
	 *
	 * @return nomeRazaoSocialMaster
	 */
	public String getNomeRazaoSocialMaster() {
		return nomeRazaoSocialMaster;
	}
	
	/**
	 * Set: nomeRazaoSocialMaster.
	 *
	 * @param nomeRazaoSocialMaster the nome razao social master
	 */
	public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
		this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
	}
	
	/**
	 * Get: grupoEconomicoMaster.
	 *
	 * @return grupoEconomicoMaster
	 */
	public String getGrupoEconomicoMaster() {
		return grupoEconomicoMaster;
	}
	
	/**
	 * Set: grupoEconomicoMaster.
	 *
	 * @param grupoEconomicoMaster the grupo economico master
	 */
	public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
		this.grupoEconomicoMaster = grupoEconomicoMaster;
	}
	
	/**
	 * Get: atividadeEconomicaMaster.
	 *
	 * @return atividadeEconomicaMaster
	 */
	public String getAtividadeEconomicaMaster() {
		return atividadeEconomicaMaster;
	}
	
	/**
	 * Set: atividadeEconomicaMaster.
	 *
	 * @param atividadeEconomicaMaster the atividade economica master
	 */
	public void setAtividadeEconomicaMaster(String atividadeEconomicaMaster) {
		this.atividadeEconomicaMaster = atividadeEconomicaMaster;
	}
	
	/**
	 * Get: segmentoMaster.
	 *
	 * @return segmentoMaster
	 */
	public String getSegmentoMaster() {
		return segmentoMaster;
	}
	
	/**
	 * Set: segmentoMaster.
	 *
	 * @param segmentoMaster the segmento master
	 */
	public void setSegmentoMaster(String segmentoMaster) {
		this.segmentoMaster = segmentoMaster;
	}
	
	/**
	 * Get: subSegmentoMaster.
	 *
	 * @return subSegmentoMaster
	 */
	public String getSubSegmentoMaster() {
		return subSegmentoMaster;
	}
	
	/**
	 * Set: subSegmentoMaster.
	 *
	 * @param subSegmentoMaster the sub segmento master
	 */
	public void setSubSegmentoMaster(String subSegmentoMaster) {
		this.subSegmentoMaster = subSegmentoMaster;
	}
	
	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}
	
	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	
	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}
	
	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}
	
	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}
	
	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	/**
	 * Get: cdTipo.
	 *
	 * @return cdTipo
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}
	
	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}
	
	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}
	
	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public Long getNumeroContrato() {
		return numeroContrato;
	}
	
	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(Long numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	
	/**
	 * Get: motivoDesc.
	 *
	 * @return motivoDesc
	 */
	public String getMotivoDesc() {
		return motivoDesc;
	}
	
	/**
	 * Set: motivoDesc.
	 *
	 * @param motivoDesc the motivo desc
	 */
	public void setMotivoDesc(String motivoDesc) {
		this.motivoDesc = motivoDesc;
	}
	
	/**
	 * Get: situacaoDesc.
	 *
	 * @return situacaoDesc
	 */
	public String getSituacaoDesc() {
		return situacaoDesc;
	}
	
	/**
	 * Set: situacaoDesc.
	 *
	 * @param situacaoDesc the situacao desc
	 */
	public void setSituacaoDesc(String situacaoDesc) {
		this.situacaoDesc = situacaoDesc;
	}
	
	/**
	 * Get: participacao.
	 *
	 * @return participacao
	 */
	public String getParticipacao() {
		return participacao;
	}
	
	/**
	 * Set: participacao.
	 *
	 * @param participacao the participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}
	
	/**
	 * Get: possuiAditivos.
	 *
	 * @return possuiAditivos
	 */
	public String getPossuiAditivos() {
		return possuiAditivos;
	}
	
	/**
	 * Set: possuiAditivos.
	 *
	 * @param possuiAditivos the possui aditivos
	 */
	public void setPossuiAditivos(String possuiAditivos) {
		this.possuiAditivos = possuiAditivos;
	}
	
	/**
	 * Get: dataHoraCadastramento.
	 *
	 * @return dataHoraCadastramento
	 */
	public String getDataHoraCadastramento() {
		return dataHoraCadastramento;
	}
	
	/**
	 * Set: dataHoraCadastramento.
	 *
	 * @param dataHoraCadastramento the data hora cadastramento
	 */
	public void setDataHoraCadastramento(String dataHoraCadastramento) {
		this.dataHoraCadastramento = dataHoraCadastramento;
	}
	
	/**
	 * Get: inicioVigencia.
	 *
	 * @return inicioVigencia
	 */
	public String getInicioVigencia() {
		return inicioVigencia;
	}
	
	/**
	 * Set: inicioVigencia.
	 *
	 * @param inicioVigencia the inicio vigencia
	 */
	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}
	
	/**
	 * Get: cdAgenciaGestora.
	 *
	 * @return cdAgenciaGestora
	 */
	public int getCdAgenciaGestora() {
		return cdAgenciaGestora;
	}
	
	/**
	 * Set: cdAgenciaGestora.
	 *
	 * @param cdAgenciaGestora the cd agencia gestora
	 */
	public void setCdAgenciaGestora(int cdAgenciaGestora) {
		this.cdAgenciaGestora = cdAgenciaGestora;
	}
	
	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}
	
	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}
	
	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}
	
	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}
	
	/**
	 * Get: cdGerenteResponsavel.
	 *
	 * @return cdGerenteResponsavel
	 */
	public Long getCdGerenteResponsavel() {
		return cdGerenteResponsavel;
	}
	
	/**
	 * Set: cdGerenteResponsavel.
	 *
	 * @param cdGerenteResponsavel the cd gerente responsavel
	 */
	public void setCdGerenteResponsavel(Long cdGerenteResponsavel) {
		this.cdGerenteResponsavel = cdGerenteResponsavel;
	}
	
	/**
	 * Get: dsNomeRazaoSocialParticipante.
	 *
	 * @return dsNomeRazaoSocialParticipante
	 */
	public String getDsNomeRazaoSocialParticipante() {
		return dsNomeRazaoSocialParticipante;
	}
	
	/**
	 * Set: dsNomeRazaoSocialParticipante.
	 *
	 * @param dsNomeRazaoSocialParticipante the ds nome razao social participante
	 */
	public void setDsNomeRazaoSocialParticipante(
			String dsNomeRazaoSocialParticipante) {
		this.dsNomeRazaoSocialParticipante = dsNomeRazaoSocialParticipante;
	}
	
	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}
	
	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	
	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}
	
	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}
	
	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}
	
	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}
	
	/**
	 * Get: renegociavel.
	 *
	 * @return renegociavel
	 */
	public String getRenegociavel() {
		return renegociavel;
	}
	
	/**
	 * Set: renegociavel.
	 *
	 * @param renegociavel the renegociavel
	 */
	public void setRenegociavel(String renegociavel) {
		this.renegociavel = renegociavel;
	}

	/**
	 * Nome: getSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @return saidaTipoLayoutArquivoSaidaDTO
	 */
	public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
		return saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: setSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @param saidaTipoLayoutArquivoSaidaDTO
	 */
	public void setSaidaTipoLayoutArquivoSaidaDTO(
			TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
		this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
	}
	
}
