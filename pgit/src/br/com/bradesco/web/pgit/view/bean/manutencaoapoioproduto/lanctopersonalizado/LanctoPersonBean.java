/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.lanctopersonalizado
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.lanctopersonalizado;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.ILanctoPersonService;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.AlterarLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.AlterarLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharHistoricoLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharHistoricoLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.DetalharLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ExcluirLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ExcluirLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.HistoricoLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.HistoricoLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.IncluirLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.IncluirLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ListarLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ListarLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: LanctoPersonBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class LanctoPersonBean {
	
	// Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_LACTO_PERSONALIZADO. */
	private static final String CON_LACTO_PERSONALIZADO = "conLanctoPersonalizado";
	
	/** Atributo codLancamentoPersonalizadoFiltro. */
	private String codLancamentoPersonalizadoFiltro;
	
	/** Atributo listaCodigoLancamentoDebito. */
	private List<SelectItem> listaCodigoLancamentoDebito = new ArrayList<SelectItem>();
	
	/** Atributo listaCodigoLancamentoCredito. */
	private List<SelectItem> listaCodigoLancamentoCredito = new ArrayList<SelectItem>();
	
	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;
	
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer,String> listaTipoServicoHash = new HashMap<Integer,String>();
	
	/** Atributo modalidadeServicoFiltro. */
	private Integer modalidadeServicoFiltro;
	
	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo listaGridLancamento. */
	private List<ListarLanctoPersonalizadoSaidaDTO> listaGridLancamento;
	
	/** Atributo listaGridHistorico. */
	private List<HistoricoLanctoPersonalizadoSaidaDTO> listaGridHistorico;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo listaControleRadioHistorico. */
	private List<SelectItem> listaControleRadioHistorico = new ArrayList<SelectItem>();
	
	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;
	
	/** Atributo listaModalidadeServicoIncluir. */
	private List<SelectItem> listaModalidadeServicoIncluir = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoIncluirHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo extratoCredito. */
	private boolean extratoCredito;
	
	/** Atributo extratoDebito. */
	private boolean extratoDebito;
	
	/** Atributo tipoLancamento. */
	private String tipoLancamento;
	
	/** Atributo restricao. */
	private Integer restricao;
	
	/** Atributo tipoServico. */
	private Integer tipoServico;
	
	/** Atributo tipoServicoDesc. */
	private String tipoServicoDesc;
	
	/** Atributo modalidadeServico. */
	private Integer modalidadeServico;
	
	/** Atributo tipoRelacionamento. */
	private Integer tipoRelacionamento;
	
	/** Atributo modalidadeServicoDesc. */
	private String modalidadeServicoDesc;
	
	/** Atributo codigoLancamentoPersonalizado. */
	private String codigoLancamentoPersonalizado;
	
	/** Atributo codigoLancamentoCredito. */
	private String codigoLancamentoCredito;
	
	/** Atributo codigoLancamentoDebito. */
	private String codigoLancamentoDebito;
	
	/** Atributo lancamentoCreditoDesc. */
	private String lancamentoCreditoDesc;
	
	/** Atributo lancamentoDebitoDesc. */
	private String lancamentoDebitoDesc;
	
	/** Atributo codLancamentoPersonalizadoHistorico. */
	private String codLancamentoPersonalizadoHistorico;
	
	/** Atributo filtroDataDe. */
	private Date filtroDataDe;
	
	/** Atributo filtroDataAte. */
	private Date filtroDataAte;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo radioArgumentoPesquisa. */
	private String radioArgumentoPesquisa;
	
	/** Atributo itemSelecionadoLancamento. */
	private Integer itemSelecionadoLancamento;
	
	/** Atributo itemSelecionadoHistorico. */
	private Integer itemSelecionadoHistorico;
	
	/** Atributo habilitaCredito. */
	private boolean habilitaCredito;
	
	/** Atributo habilitaDebito. */
	private boolean habilitaDebito;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo btoAcionadoHistorico. */
	private Boolean btoAcionadoHistorico;
	
	/** Atributo dataManutencao. */
	private String dataManutencao;
	
	/** Atributo responsavel. */
	private String responsavel;
	
	/** Atributo tipoManutencao. */
	private String tipoManutencao;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo manterLancamentoPersonalizadoServiceImpl. */
	private ILanctoPersonService manterLancamentoPersonalizadoServiceImpl;
		
	
	/**
	 * Get: manterLancamentoPersonalizadoServiceImpl.
	 *
	 * @return manterLancamentoPersonalizadoServiceImpl
	 */
	public ILanctoPersonService getManterLancamentoPersonalizadoServiceImpl() {
		return manterLancamentoPersonalizadoServiceImpl;
	}

	/**
	 * Set: manterLancamentoPersonalizadoServiceImpl.
	 *
	 * @param manterLancamentoPersonalizadoServiceImpl the manter lancamento personalizado service impl
	 */
	public void setManterLancamentoPersonalizadoServiceImpl(
			ILanctoPersonService manterLancamentoPersonalizadoServiceImpl) {
		this.manterLancamentoPersonalizadoServiceImpl = manterLancamentoPersonalizadoServiceImpl;
	}

	/**
	 * Get: itemSelecionadoLancamento.
	 *
	 * @return itemSelecionadoLancamento
	 */
	public Integer getItemSelecionadoLancamento() {
		return itemSelecionadoLancamento;
	}
	
	/**
	 * Set: itemSelecionadoLancamento.
	 *
	 * @param itemSelecionadoLancamento the item selecionado lancamento
	 */
	public void setItemSelecionadoLancamento(Integer itemSelecionadoLancamento) {
		this.itemSelecionadoLancamento = itemSelecionadoLancamento;
	}
	
	/**
	 * Get: listaCodigoLancamentoCredito.
	 *
	 * @return listaCodigoLancamentoCredito
	 */
	public List<SelectItem> getListaCodigoLancamentoCredito() {
		return listaCodigoLancamentoCredito;
	}
	
	/**
	 * Set: listaCodigoLancamentoCredito.
	 *
	 * @param listaCodigoLancamentoCredito the lista codigo lancamento credito
	 */
	public void setListaCodigoLancamentoCredito(
			List<SelectItem> listaCodigoLancamentoCredito) {
		this.listaCodigoLancamentoCredito = listaCodigoLancamentoCredito;
	}
	
	/**
	 * Get: listaCodigoLancamentoDebito.
	 *
	 * @return listaCodigoLancamentoDebito
	 */
	public List<SelectItem> getListaCodigoLancamentoDebito() {
		return listaCodigoLancamentoDebito;
	}
	
	/**
	 * Set: listaCodigoLancamentoDebito.
	 *
	 * @param listaCodigoLancamentoDebito the lista codigo lancamento debito
	 */
	public void setListaCodigoLancamentoDebito(
			List<SelectItem> listaCodigoLancamentoDebito) {
		this.listaCodigoLancamentoDebito = listaCodigoLancamentoDebito;
	}
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
		this.limparDados();
		this.limparCamposIncluir();

		this.itemSelecionadoLancamento = null;
	
		this.listaGridLancamento = null;
		setBtoAcionado(false);
		//carrega os combos da tela de pesquisar
		try{
			listarTipoServico();
		}catch(PdcAdapterFunctionalException e){
			listaTipoServico = new ArrayList<SelectItem>();
		}
		
	}
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {
		
		this.limparCampos();

		this.radioArgumentoPesquisa = null;		
		
		return "ok";
		
	}
	
	/**
	 * Limpar campos.
	 */
	public void limparCampos() {
		
		this.setCodLancamentoPersonalizadoFiltro("");
		this.setTipoServicoFiltro(0);
		this.setModalidadeServicoFiltro(0);
		this.setListaGridLancamento(null);
		this.setItemSelecionadoLancamento(null);
		this.setListaModalidadeServico(new ArrayList<SelectItem>());
		listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
		setBtoAcionado(false);
	}
	
	/**
	 * Limpar campos incluir.
	 */
	public void limparCamposIncluir() {
		
		this.setCodigoLancamentoPersonalizado("");
		this.setTipoServico(0);
		this.setModalidadeServico(0);
		this.setTipoServicoDesc("");
		this.setModalidadeServicoDesc("");
		this.setExtratoCredito(false);
		this.setExtratoDebito(false);
		this.setCodigoLancamentoCredito("");
		this.setCodigoLancamentoDebito("");
		this.setRestricao(0);
		this.setHabilitaCredito(true);
		this.setHabilitaDebito(true);
		this.setTipoLancamento("");		
		
	}

	/**
	 * Limpar dados historico.
	 */
	public void limparDadosHistorico() {
		
		this.setCodLancamentoPersonalizadoHistorico("");
		this.setFiltroDataAte(new Date());
		this.setFiltroDataDe(new Date());
		this.setDataManutencao("");
		this.setResponsavel("");
		this.setTipoManutencao("");
		setListaGridHistorico(null);
		setItemSelecionadoHistorico(null);
		setBtoAcionadoHistorico(false);
		
	}
	
	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String  consultar() {
		carregarGrid();
		return "ok";		
	}	
	
	/**
	 * Carregar grid.
	 */
	private void carregarGrid() {
		
		try{
			
			ListarLanctoPersonalizadoEntradaDTO entradaDTO = new ListarLanctoPersonalizadoEntradaDTO();
			
			entradaDTO.setCodLancamento(!getCodLancamentoPersonalizadoFiltro().equals("")?Integer.parseInt(getCodLancamentoPersonalizadoFiltro()):0);
			entradaDTO.setTipoServico(getTipoServicoFiltro()!=null?getTipoServicoFiltro():0);
			entradaDTO.setModalidadeServico(getModalidadeServicoFiltro()!=null?getModalidadeServicoFiltro():0);
			entradaDTO.setTipoRelacionamento(getModalidadeServicoFiltro() == null || getModalidadeServicoFiltro() == 0 || listaModalidadeServicoHash.isEmpty()? 0 :listaModalidadeServicoHash.get(getModalidadeServicoFiltro()).getCdRelacionamentoProduto());

			setListaGridLancamento(getManterLancamentoPersonalizadoServiceImpl().listarLanctoPersonalizado(entradaDTO));
			setBtoAcionado(true);
			this.listaControleRadio =  new ArrayList<SelectItem>();
			for (int i = 0; i <= getListaGridLancamento().size();i++) {
				this.listaControleRadio.add(new SelectItem(i," "));
			}
			
			setItemSelecionadoLancamento(null);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridLancamento(null);

		}
		
	}

	/**
	 * Consultar historico.
	 *
	 * @return the string
	 */
	public String consultarHistorico() {
		
		try{
			
			HistoricoLanctoPersonalizadoEntradaDTO entradaDTO = new HistoricoLanctoPersonalizadoEntradaDTO();
			
			entradaDTO.setCodLancamentoPersonalizado(getCodLancamentoPersonalizadoHistorico().equals("")? 0 : Integer.valueOf(getCodLancamentoPersonalizadoHistorico()));
			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			entradaDTO.setDataFinal(df.format(getFiltroDataAte()));
			entradaDTO.setDataInicial(df.format(getFiltroDataDe()));

			setListaGridHistorico(getManterLancamentoPersonalizadoServiceImpl().historicoLanctoPersonalizado(entradaDTO));
			setBtoAcionadoHistorico(true);
			this.listaControleRadioHistorico =  new ArrayList<SelectItem>();
			for (int i = 0; i <= getListaGridHistorico().size();i++) {
				this.listaControleRadioHistorico.add(new SelectItem(i," "));
			}
			
			setItemSelecionadoHistorico(null);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridHistorico(null);

		}
		
		return "ok";
	}	
	
	
	/**
	 * Voltar historico.
	 *
	 * @return the string
	 */
	public String voltarHistorico(){
		setItemSelecionadoLancamento(null);
		return "VOLTAR";
	}
	
	/**
	 * Voltar det hist.
	 *
	 * @return the string
	 */
	public String voltarDetHist(){
		setItemSelecionadoHistorico(null);
		return "VOLTAR_HISTORICO";
	}
	

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}
	
	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt) {
		carregarGrid();
	}

	/**
	 * Pesquisar historico.
	 *
	 * @param evt the evt
	 */
	public void pesquisarHistorico(ActionEvent evt) {
		consultarHistorico();
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		this.limparCamposIncluir();
		
		return "INCLUIR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_LACTO_PERSONALIZADO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 

		return "EXCLUIR";
	}
	
	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar(){
		
		try {
			preencheDados();
			
			if (getTipoLancamento().equals("1")){
				this.setExtratoDebito(true); 
				this.setExtratoCredito(false);
				this.setHabilitaCredito(true);
				this.setHabilitaDebito(false);
			}else if (getTipoLancamento().equals("2")){
				this.setExtratoCredito(true);
				this.setExtratoDebito(false);
				this.setHabilitaCredito(false);
				this.setHabilitaDebito(true);
			}else if (getTipoLancamento().equals("3")){
				this.setExtratoDebito(true);
				this.setExtratoCredito(true);
				this.setHabilitaCredito(false);
				this.setHabilitaDebito(false);
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_LACTO_PERSONALIZADO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 

		return "ALTERAR";
	}	
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_LACTO_PERSONALIZADO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "DETALHAR";

	}
	
	/**
	 * Historico.
	 *
	 * @return the string
	 */
	public String historico(){
		if(getItemSelecionadoLancamento() != null){
			ListarLanctoPersonalizadoSaidaDTO listarLanctoPersonalizadoSaidaDTO = getListaGridLancamento().get(getItemSelecionadoLancamento());
			this.limparDadosHistorico();
			this.setCodLancamentoPersonalizadoHistorico(listarLanctoPersonalizadoSaidaDTO.getCodLancamento().toString());
			this.listaGridHistorico = null;
			this.itemSelecionadoHistorico = null;
			setBtoAcionadoHistorico(false);
		}else{
			this.limparDadosHistorico();
			this.listaGridHistorico = null;
			this.itemSelecionadoHistorico = null;
			setBtoAcionadoHistorico(false);
		}
		
		
		return "HISTORICO";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		if (getObrigatoriedade().equals("T")){
			
			setTipoServicoDesc((String)listaTipoServicoHash.get(getTipoServico()));
			setModalidadeServicoDesc(listaModalidadeServicoIncluirHash.get(getModalidadeServico()).getDsModalidade());
			
			if (extratoCredito && extratoDebito){
				tipoLancamento = "3";
			}
			else if (extratoCredito){
				tipoLancamento = "2";
			}
			else if (extratoDebito){
				tipoLancamento = "1";
			}
			
			return "AVANCAR_INCLUIR";
		}
		
		return "";
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar(){
		if (getObrigatoriedade().equals("T")){
			if (extratoCredito && extratoDebito){
				tipoLancamento = "3";
			}
			else if (extratoCredito){
				tipoLancamento = "2";
			}
			else if (extratoDebito){
				tipoLancamento = "1";
			}
			return "AVANCAR_ALTERAR";
		}
		
		return "";
	}
	
	/**
	 * Detalhar historico.
	 *
	 * @return the string
	 */
	public String detalharHistorico(){
		
		try {
			preencheDadosHistorico();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_LACTO_PERSONALIZADO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 

		return "DETALHAR_HISTORICO";
	}

	/**
	 * Confirmar.
	 *
	 * @return the string
	 */
	public String confirmar(){
		try {
			
			IncluirLanctoPersonalizadoEntradaDTO incluirLanctoPersonalizadoEntradaDTO = new IncluirLanctoPersonalizadoEntradaDTO();
			
			incluirLanctoPersonalizadoEntradaDTO.setCodLancamentoCredito(getCodigoLancamentoCredito() !=null && !getCodigoLancamentoCredito().equals("")?Integer.parseInt(getCodigoLancamentoCredito()):0);
			incluirLanctoPersonalizadoEntradaDTO.setCodLancamentoDebito(getCodigoLancamentoDebito() !=null && !getCodigoLancamentoDebito().equals("")?Integer.parseInt(getCodigoLancamentoDebito()):0);
			incluirLanctoPersonalizadoEntradaDTO.setRestricao(getRestricao());
			
			incluirLanctoPersonalizadoEntradaDTO.setCodLancamento(0);
			incluirLanctoPersonalizadoEntradaDTO.setModalidadeServico(getModalidadeServico());
			incluirLanctoPersonalizadoEntradaDTO.setTipoServico(getTipoServico());
			incluirLanctoPersonalizadoEntradaDTO.setTipoRelacionamento(listaModalidadeServicoIncluirHash.get(getModalidadeServico()).getCdRelacionamentoProduto());
			
			if (extratoCredito && extratoDebito){
				tipoLancamento = "3";
			}
			else if (extratoCredito){
				tipoLancamento = "2";
			}
			else if (extratoDebito){
				tipoLancamento = "1";
			}
			
			incluirLanctoPersonalizadoEntradaDTO.setTipoLancamento(Integer.parseInt(getTipoLancamento()));
			
			IncluirLanctoPersonalizadoSaidaDTO incluirLanctoPersonalizadoSaidaDTO = getManterLancamentoPersonalizadoServiceImpl().incluirLanctoPersonalizado(incluirLanctoPersonalizadoEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +incluirLanctoPersonalizadoSaidaDTO.getCodMensagem() + ") " + incluirLanctoPersonalizadoSaidaDTO.getMensagem(), CON_LACTO_PERSONALIZADO, BradescoViewExceptionActionType.ACTION, false);
			
			carregarGrid();			
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		
		return "";

	}
	
	private Integer verificaStringToIntegerNula(String str) {
        if (str == null ||  "".equals(str)) {
            return 0;
        } else {
            return Integer.valueOf(str);
        }
        
    }
	
	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar(){
		try {
			
			AlterarLanctoPersonalizadoEntradaDTO alterarLanctoPersonalizadoEntradaDTO = new AlterarLanctoPersonalizadoEntradaDTO();
			
			ListarLanctoPersonalizadoSaidaDTO listarLanctoPersonalizadoSaidaDTO = getListaGridLancamento().get(getItemSelecionadoLancamento());	
			
			alterarLanctoPersonalizadoEntradaDTO.setCodLancamentoCredito(getCodigoLancamentoCredito() != null && !getCodigoLancamentoCredito().equals("")?Integer.parseInt(getCodigoLancamentoCredito()):0);
			alterarLanctoPersonalizadoEntradaDTO.setCodLancamentoDebito(getCodigoLancamentoDebito() !=null && !getCodigoLancamentoDebito().equals("")?Integer.parseInt(getCodigoLancamentoDebito()):0);
			alterarLanctoPersonalizadoEntradaDTO.setRestricao(getRestricao());
			alterarLanctoPersonalizadoEntradaDTO.setCodLancamento(Integer.parseInt(getCodigoLancamentoPersonalizado()));
			alterarLanctoPersonalizadoEntradaDTO.setModalidadeServico(listarLanctoPersonalizadoSaidaDTO.getCdTipoModalidade());
			alterarLanctoPersonalizadoEntradaDTO.setTipoServico(listarLanctoPersonalizadoSaidaDTO.getCdTipoServico());
			alterarLanctoPersonalizadoEntradaDTO.setTipoRelacionamento(listarLanctoPersonalizadoSaidaDTO.getCdTipoRelacionamento());
			
			if (extratoCredito && extratoDebito){
				tipoLancamento = "3";
			}
			else if (extratoCredito){
				tipoLancamento = "2";
			}
			else if (extratoDebito){
				tipoLancamento = "1";
			}
			
			alterarLanctoPersonalizadoEntradaDTO.setTipoLancamento(Integer.parseInt(getTipoLancamento()));
			
			
			AlterarLanctoPersonalizadoSaidaDTO alterarLanctoPersonalizadoSaidaDTO = getManterLancamentoPersonalizadoServiceImpl().alterarLanctoPersonalizado(alterarLanctoPersonalizadoEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + alterarLanctoPersonalizadoSaidaDTO.getCodMensagem() + ") " + alterarLanctoPersonalizadoSaidaDTO.getMensagem(), CON_LACTO_PERSONALIZADO, BradescoViewExceptionActionType.ACTION, false);

			carregarGrid();
	
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try {
			
			ExcluirLanctoPersonalizadoEntradaDTO excluirLanctoPersonalizadoEntradaDTO = new ExcluirLanctoPersonalizadoEntradaDTO();
			
			ListarLanctoPersonalizadoSaidaDTO listarLanctoPersonalizadoSaidaDTO = getListaGridLancamento().get(getItemSelecionadoLancamento());	
			
			excluirLanctoPersonalizadoEntradaDTO.setCodLancamento(listarLanctoPersonalizadoSaidaDTO.getCodLancamento());
			excluirLanctoPersonalizadoEntradaDTO.setTipoServico(listarLanctoPersonalizadoSaidaDTO.getCdTipoServico());
			excluirLanctoPersonalizadoEntradaDTO.setModalidadeServico(listarLanctoPersonalizadoSaidaDTO.getCdTipoModalidade());
			excluirLanctoPersonalizadoEntradaDTO.setTipoRelacionamento(listarLanctoPersonalizadoSaidaDTO.getCdTipoRelacionamento());
						
			ExcluirLanctoPersonalizadoSaidaDTO excluirLanctoPersonalizadoSaidaDTO = getManterLancamentoPersonalizadoServiceImpl().excluirLanctoPersonalizado(excluirLanctoPersonalizadoEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + excluirLanctoPersonalizadoSaidaDTO.getCodMensagem() + ") " + excluirLanctoPersonalizadoSaidaDTO.getMensagem(), CON_LACTO_PERSONALIZADO, BradescoViewExceptionActionType.ACTION, false);

			carregarGrid();
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";

	}
	
	/**
	 * Voltar consulta.
	 *
	 * @return the string
	 */
	public String voltarConsulta(){
		
		setItemSelecionadoLancamento(null);
		
		return "conLanctoPersonalizado";
	}
	
	/**
	 * Get: restricao.
	 *
	 * @return restricao
	 */
	public Integer getRestricao() {
		return restricao;
	}
	
	/**
	 * Set: restricao.
	 *
	 * @param restricao the restricao
	 */
	public void setRestricao(Integer restricao) {
		this.restricao = restricao;
	}
	
	/**
	 * Get: tipoLancamento.
	 *
	 * @return tipoLancamento
	 */
	public String getTipoLancamento() {
		return tipoLancamento;
	}
	
	/**
	 * Set: tipoLancamento.
	 *
	 * @param tipoLancamento the tipo lancamento
	 */
	public void setTipoLancamento(String tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}
	
	/**
	 * Get: codigoLancamentoCredito.
	 *
	 * @return codigoLancamentoCredito
	 */
	public String getCodigoLancamentoCredito() {
		return codigoLancamentoCredito;
	}
	
	/**
	 * Set: codigoLancamentoCredito.
	 *
	 * @param codigoLancamentoCredito the codigo lancamento credito
	 */
	public void setCodigoLancamentoCredito(String codigoLancamentoCredito) {
		this.codigoLancamentoCredito = codigoLancamentoCredito;
	}
	
	/**
	 * Get: codigoLancamentoDebito.
	 *
	 * @return codigoLancamentoDebito
	 */
	public String getCodigoLancamentoDebito() {
		return codigoLancamentoDebito;
	}
	
	/**
	 * Set: codigoLancamentoDebito.
	 *
	 * @param codigoLancamentoDebito the codigo lancamento debito
	 */
	public void setCodigoLancamentoDebito(String codigoLancamentoDebito) {
		this.codigoLancamentoDebito = codigoLancamentoDebito;
	}
	
	/**
	 * Get: codigoLancamentoPersonalizado.
	 *
	 * @return codigoLancamentoPersonalizado
	 */
	public String getCodigoLancamentoPersonalizado() {
		return codigoLancamentoPersonalizado;
	}
	
	/**
	 * Set: codigoLancamentoPersonalizado.
	 *
	 * @param codigoLancamentoPersonalizado the codigo lancamento personalizado
	 */
	public void setCodigoLancamentoPersonalizado(
			String codigoLancamentoPersonalizado) {
		this.codigoLancamentoPersonalizado = codigoLancamentoPersonalizado;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: itemSelecionadoHistorico.
	 *
	 * @return itemSelecionadoHistorico
	 */
	public Integer getItemSelecionadoHistorico() {
		return itemSelecionadoHistorico;
	}

	/**
	 * Set: itemSelecionadoHistorico.
	 *
	 * @param itemSelecionadoHistorico the item selecionado historico
	 */
	public void setItemSelecionadoHistorico(Integer itemSelecionadoHistorico) {
		this.itemSelecionadoHistorico = itemSelecionadoHistorico;
	}

	/**
	 * Get: radioArgumentoPesquisa.
	 *
	 * @return radioArgumentoPesquisa
	 */
	public String getRadioArgumentoPesquisa() {
		return radioArgumentoPesquisa;
	}

	/**
	 * Set: radioArgumentoPesquisa.
	 *
	 * @param radioArgumentoPesquisa the radio argumento pesquisa
	 */
	public void setRadioArgumentoPesquisa(String radioArgumentoPesquisa) {
		this.radioArgumentoPesquisa = radioArgumentoPesquisa;
	}


	/**
	 * Get: listaGridLancamento.
	 *
	 * @return listaGridLancamento
	 */
	public List<ListarLanctoPersonalizadoSaidaDTO> getListaGridLancamento() {
		return listaGridLancamento;
	}

	/**
	 * Set: listaGridLancamento.
	 *
	 * @param listaGridLancamento the lista grid lancamento
	 */
	public void setListaGridLancamento(
			List<ListarLanctoPersonalizadoSaidaDTO> listaGridLancamento) {
		this.listaGridLancamento = listaGridLancamento;
	}

	/**
	 * Get: codLancamentoPersonalizadoFiltro.
	 *
	 * @return codLancamentoPersonalizadoFiltro
	 */
	public String getCodLancamentoPersonalizadoFiltro() {
		return codLancamentoPersonalizadoFiltro;
	}

	/**
	 * Set: codLancamentoPersonalizadoFiltro.
	 *
	 * @param codLancamentoPersonalizadoFiltro the cod lancamento personalizado filtro
	 */
	public void setCodLancamentoPersonalizadoFiltro(
			String codLancamentoPersonalizadoFiltro) {
		this.codLancamentoPersonalizadoFiltro = codLancamentoPersonalizadoFiltro;
	}

	/**
	 * Get: modalidadeServicoFiltro.
	 *
	 * @return modalidadeServicoFiltro
	 */
	public Integer getModalidadeServicoFiltro() {
		return modalidadeServicoFiltro;
	}

	/**
	 * Set: modalidadeServicoFiltro.
	 *
	 * @param modalidadeServicoFiltro the modalidade servico filtro
	 */
	public void setModalidadeServicoFiltro(Integer modalidadeServicoFiltro) {
		this.modalidadeServicoFiltro = modalidadeServicoFiltro;
	}

	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Get: listaModalidadeServico.
	 *
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {
		return listaModalidadeServico;
	}

	/**
	 * Set: listaModalidadeServico.
	 *
	 * @param listaModalidadeServico the lista modalidade servico
	 */
	public void setListaModalidadeServico(List<SelectItem> listaModalidadeServico) {
		this.listaModalidadeServico = listaModalidadeServico;
	}

	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Listar tipo servico.
	 */
	private void listarTipoServico() {
		
		this.listaTipoServico = new ArrayList<SelectItem>();
		
		List<ListarServicosSaidaDTO> saidaDTO = new ArrayList<ListarServicosSaidaDTO>();
		
		saidaDTO = comboService.listarTipoServicos(1);
		
		listaTipoServicoHash.clear();
		for(ListarServicosSaidaDTO combo : saidaDTO){
			listaTipoServicoHash.put(combo.getCdServico(),combo.getDsServico());
			this.listaTipoServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
		}
			
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Listar modalidade servico.
	 */
	public void listarModalidadeServico(){
		
		listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
		listaModalidadeServico = new ArrayList<SelectItem>();
		
		if( getTipoServicoFiltro()!=null && getTipoServicoFiltro() !=0){
			List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
			ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
			
			listarModalidadeEntradaDTO.setCdServico(getTipoServicoFiltro());
		
			listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
			
			listaModalidadeServicoHash.clear();
			
			for(ListarModalidadeSaidaDTO combo : listaModalidades){				
				this.listaModalidadeServico.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
				listaModalidadeServicoHash.put(combo.getCdModalidade(),combo);
				
			}
		}else{
			listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
			listaModalidadeServico.clear();
			setModalidadeServicoFiltro(0);
			setTipoServicoFiltro(0);
		}
	}

	/**
	 * Get: listaModalidadeServicoHash.
	 *
	 * @return listaModalidadeServicoHash
	 */
	Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoHash() {
		return listaModalidadeServicoHash;
	}

	/**
	 * Set lista modalidade servico hash.
	 *
	 * @param listaModalidadeServicoHash the lista modalidade servico hash
	 */
	void setListaModalidadeServicoHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash) {
		this.listaModalidadeServicoHash = listaModalidadeServicoHash;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public Integer getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(Integer tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: listaModalidadeServicoIncluir.
	 *
	 * @return listaModalidadeServicoIncluir
	 */
	public List<SelectItem> getListaModalidadeServicoIncluir() {
		return listaModalidadeServicoIncluir;
	}

	/**
	 * Set: listaModalidadeServicoIncluir.
	 *
	 * @param listaModalidadeServicoIncluir the lista modalidade servico incluir
	 */
	public void setListaModalidadeServicoIncluir(
			List<SelectItem> listaModalidadeServicoIncluir) {
		this.listaModalidadeServicoIncluir = listaModalidadeServicoIncluir;
	}

	/**
	 * Get: listaModalidadeServicoIncluirHash.
	 *
	 * @return listaModalidadeServicoIncluirHash
	 */
	Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoIncluirHash() {
		return listaModalidadeServicoIncluirHash;
	}

	/**
	 * Set lista modalidade servico incluir hash.
	 *
	 * @param listaModalidadeServicoIncluirHash the lista modalidade servico incluir hash
	 */
	void setListaModalidadeServicoIncluirHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoIncluirHash) {
		this.listaModalidadeServicoIncluirHash = listaModalidadeServicoIncluirHash;
	}

	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public Integer getModalidadeServico() {
		return modalidadeServico;
	}

	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(Integer modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}
	
	/**
	 * Listar modalidade servico incluir.
	 */
	public void listarModalidadeServicoIncluir(){
		
		listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
		listaModalidadeServicoIncluir = new ArrayList<SelectItem>();
		
		if( getTipoServico()!=null && getTipoServico()!=0){
			List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
			ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
			
			listarModalidadeEntradaDTO.setCdServico(getTipoServico());
		
			listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
			
			listaModalidadeServicoIncluirHash.clear();
			
			for(ListarModalidadeSaidaDTO combo : listaModalidades){				
				this.listaModalidadeServicoIncluir.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
				listaModalidadeServicoIncluirHash.put(combo.getCdModalidade(),combo);
				
			}
		}else{
			listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
			listaModalidadeServicoIncluir.clear();
			setModalidadeServico(0);
			setTipoServico(0);
		}
	}

	/**
	 * Habilita credito.
	 */
	public void habilitaCredito(){
		setCodigoLancamentoCredito("");
		
		// quando seleciona tipo de lancamento credito	
		if (isExtratoCredito()){
			setHabilitaCredito(false);
		}else{
			setHabilitaCredito(true);
		}
	}
	
	/**
	 * Habilita debito.
	 */
	public void habilitaDebito(){
		setCodigoLancamentoDebito("");
		
		// quando seleciona tipo de lancamento credito	
		if (isExtratoDebito()){
			setHabilitaDebito(false);
		}else{
			setHabilitaDebito(true);
		}
			
	}

	/**
	 * Is extrato credito.
	 *
	 * @return true, if is extrato credito
	 */
	public boolean isExtratoCredito() {
		return extratoCredito;
	}

	/**
	 * Set: extratoCredito.
	 *
	 * @param extratoCredito the extrato credito
	 */
	public void setExtratoCredito(boolean extratoCredito) {
		this.extratoCredito = extratoCredito;
	}

	/**
	 * Is extrato debito.
	 *
	 * @return true, if is extrato debito
	 */
	public boolean isExtratoDebito() {
		return extratoDebito;
	}

	/**
	 * Set: extratoDebito.
	 *
	 * @param extratoDebito the extrato debito
	 */
	public void setExtratoDebito(boolean extratoDebito) {
		this.extratoDebito = extratoDebito;
	}

	/**
	 * Is habilita credito.
	 *
	 * @return true, if is habilita credito
	 */
	public boolean isHabilitaCredito() {
		return habilitaCredito;
	}

	/**
	 * Set: habilitaCredito.
	 *
	 * @param habilitaCredito the habilita credito
	 */
	public void setHabilitaCredito(boolean habilitaCredito) {
		this.habilitaCredito = habilitaCredito;
	}

	/**
	 * Is habilita debito.
	 *
	 * @return true, if is habilita debito
	 */
	public boolean isHabilitaDebito() {
		return habilitaDebito;
	}

	/**
	 * Set: habilitaDebito.
	 *
	 * @param habilitaDebito the habilita debito
	 */
	public void setHabilitaDebito(boolean habilitaDebito) {
		this.habilitaDebito = habilitaDebito;
	}

	/**
	 * Get: modalidadeServicoDesc.
	 *
	 * @return modalidadeServicoDesc
	 */
	public String getModalidadeServicoDesc() {
		return modalidadeServicoDesc;
	}

	/**
	 * Set: modalidadeServicoDesc.
	 *
	 * @param modalidadeServicoDesc the modalidade servico desc
	 */
	public void setModalidadeServicoDesc(String modalidadeServicoDesc) {
		this.modalidadeServicoDesc = modalidadeServicoDesc;
	}

	/**
	 * Get: tipoServicoDesc.
	 *
	 * @return tipoServicoDesc
	 */
	public String getTipoServicoDesc() {
		return tipoServicoDesc;
	}

	/**
	 * Set: tipoServicoDesc.
	 *
	 * @param tipoServicoDesc the tipo servico desc
	 */
	public void setTipoServicoDesc(String tipoServicoDesc) {
		this.tipoServicoDesc = tipoServicoDesc;
	}
	
	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		
		ListarLanctoPersonalizadoSaidaDTO listarLanctoPersonalizadoSaidaDTO = getListaGridLancamento().get(getItemSelecionadoLancamento());
		
		DetalharLanctoPersonalizadoEntradaDTO entradaDTO = new DetalharLanctoPersonalizadoEntradaDTO();

		entradaDTO.setCodLancamento(listarLanctoPersonalizadoSaidaDTO.getCodLancamento());
		entradaDTO.setTipoServico(listarLanctoPersonalizadoSaidaDTO.getCdTipoServico());
		entradaDTO.setModalidadeServico(listarLanctoPersonalizadoSaidaDTO.getCdTipoModalidade());
		entradaDTO.setTipoRelacionamento(listarLanctoPersonalizadoSaidaDTO.getCdTipoRelacionamento());
		
		DetalharLanctoPersonalizadoSaidaDTO detalharLanctoPersonalizadoSaidaDTO = getManterLancamentoPersonalizadoServiceImpl().detalharLanctoPersonalizado(entradaDTO);

		setCodigoLancamentoPersonalizado(String.valueOf(detalharLanctoPersonalizadoSaidaDTO.getCodLancamento()));
		setCodigoLancamentoDebito(detalharLanctoPersonalizadoSaidaDTO.getCdLancDebito()==0? "" : String.valueOf(detalharLanctoPersonalizadoSaidaDTO.getCdLancDebito()));
		setLancamentoDebitoDesc(detalharLanctoPersonalizadoSaidaDTO.getDsLancDebito());
		setCodigoLancamentoCredito(detalharLanctoPersonalizadoSaidaDTO.getCdLancCredito()==0? "" : String.valueOf(detalharLanctoPersonalizadoSaidaDTO.getCdLancCredito()));
		setLancamentoCreditoDesc(detalharLanctoPersonalizadoSaidaDTO.getDsLancCredito());
		setRestricao(detalharLanctoPersonalizadoSaidaDTO.getRestricao());
		setTipoLancamento(String.valueOf(detalharLanctoPersonalizadoSaidaDTO.getTipoLancamento()));
		setTipoServico(detalharLanctoPersonalizadoSaidaDTO.getCdTipoServico());
		setTipoServicoDesc(detalharLanctoPersonalizadoSaidaDTO.getDsTipoServico());
		setModalidadeServico(detalharLanctoPersonalizadoSaidaDTO.getCdTipoModalidade());
		setModalidadeServicoDesc(detalharLanctoPersonalizadoSaidaDTO.getDsTipoModalidade());
		setTipoRelacionamento(detalharLanctoPersonalizadoSaidaDTO.getCdTipoRelacionamento());
		
		setUsuarioInclusao(detalharLanctoPersonalizadoSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharLanctoPersonalizadoSaidaDTO.getUsuarioManutencao());
		
		setComplementoInclusao(detalharLanctoPersonalizadoSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharLanctoPersonalizadoSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharLanctoPersonalizadoSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharLanctoPersonalizadoSaidaDTO.getComplementoManutencao());
		
		setTipoCanalInclusao(detalharLanctoPersonalizadoSaidaDTO.getCdCanalInclusao()==0? "" : detalharLanctoPersonalizadoSaidaDTO.getCdCanalInclusao() + " - " +  detalharLanctoPersonalizadoSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(detalharLanctoPersonalizadoSaidaDTO.getCdCanalManutencao()==0? "" : detalharLanctoPersonalizadoSaidaDTO.getCdCanalManutencao() + " - " + detalharLanctoPersonalizadoSaidaDTO.getDsCanalManutencao()); 
		
		setDataHoraInclusao(detalharLanctoPersonalizadoSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharLanctoPersonalizadoSaidaDTO.getDataHoraManutencao());
		
	}

	/**
	 * Preenche dados historico.
	 */
	private void preencheDadosHistorico(){
		
		HistoricoLanctoPersonalizadoSaidaDTO historicoLanctoPersonalizadoSaidaDTO = getListaGridHistorico().get(getItemSelecionadoHistorico());
		
		DetalharHistoricoLanctoPersonalizadoEntradaDTO entradaDTO = new DetalharHistoricoLanctoPersonalizadoEntradaDTO();

		entradaDTO.setCodLancamentoPersonalizado(historicoLanctoPersonalizadoSaidaDTO.getCodigoLancamentoPersonalizado());
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		entradaDTO.setDataFinal(df.format(getFiltroDataAte()));
		entradaDTO.setDataInicial(df.format(getFiltroDataDe()));
		entradaDTO.setDataManutencao(historicoLanctoPersonalizadoSaidaDTO.getDataManutencao());
		
		DetalharHistoricoLanctoPersonalizadoSaidaDTO detalharHistoricoLanctoPersonalizadoSaidaDTO = getManterLancamentoPersonalizadoServiceImpl().detalharHistoricoLanctoPersonalizado(entradaDTO);

		setCodigoLancamentoPersonalizado(String.valueOf(detalharHistoricoLanctoPersonalizadoSaidaDTO.getCodLancamento()));
		setCodigoLancamentoDebito(detalharHistoricoLanctoPersonalizadoSaidaDTO.getCdLancDebito()==0? "" : String.valueOf(detalharHistoricoLanctoPersonalizadoSaidaDTO.getCdLancDebito()));
		setLancamentoDebitoDesc(detalharHistoricoLanctoPersonalizadoSaidaDTO.getDsLancDebito());
		setCodigoLancamentoCredito(detalharHistoricoLanctoPersonalizadoSaidaDTO.getCdLancCredito()==0? "" : String.valueOf(detalharHistoricoLanctoPersonalizadoSaidaDTO.getCdLancCredito()));
		setLancamentoCreditoDesc(detalharHistoricoLanctoPersonalizadoSaidaDTO.getDsLancCredito());
		setRestricao(detalharHistoricoLanctoPersonalizadoSaidaDTO.getRestricao());
		setTipoLancamento(String.valueOf(detalharHistoricoLanctoPersonalizadoSaidaDTO.getTipoLancamento()));
		setTipoServico(detalharHistoricoLanctoPersonalizadoSaidaDTO.getCdTipoServico());
		setTipoServicoDesc(detalharHistoricoLanctoPersonalizadoSaidaDTO.getDsTipoServico());
		setModalidadeServico(detalharHistoricoLanctoPersonalizadoSaidaDTO.getCdTipoModalidade());
		setModalidadeServicoDesc(detalharHistoricoLanctoPersonalizadoSaidaDTO.getDsTipoModalidade());
		setDataManutencao(historicoLanctoPersonalizadoSaidaDTO.getDataManutencaoDesc());
		setResponsavel(historicoLanctoPersonalizadoSaidaDTO.getResponsavel());
		setTipoManutencao(String.valueOf(historicoLanctoPersonalizadoSaidaDTO.getTipoManutencao()));
		
		
		setUsuarioInclusao(detalharHistoricoLanctoPersonalizadoSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharHistoricoLanctoPersonalizadoSaidaDTO.getUsuarioManutencao());
		
		setComplementoInclusao(detalharHistoricoLanctoPersonalizadoSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharHistoricoLanctoPersonalizadoSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharHistoricoLanctoPersonalizadoSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharHistoricoLanctoPersonalizadoSaidaDTO.getComplementoManutencao());
		
		setTipoCanalInclusao(detalharHistoricoLanctoPersonalizadoSaidaDTO.getCdCanalInclusao()==0? "" : detalharHistoricoLanctoPersonalizadoSaidaDTO.getCdCanalInclusao() + " - " +  detalharHistoricoLanctoPersonalizadoSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(detalharHistoricoLanctoPersonalizadoSaidaDTO.getCdCanalManutencao()==0? "" : detalharHistoricoLanctoPersonalizadoSaidaDTO.getCdCanalManutencao() + " - " + detalharHistoricoLanctoPersonalizadoSaidaDTO.getDsCanalManutencao()); 
		
		setDataHoraInclusao(detalharHistoricoLanctoPersonalizadoSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharHistoricoLanctoPersonalizadoSaidaDTO.getDataHoraManutencao());
		
	}

	/**
	 * Get: lancamentoCreditoDesc.
	 *
	 * @return lancamentoCreditoDesc
	 */
	public String getLancamentoCreditoDesc() {
		return lancamentoCreditoDesc;
	}

	/**
	 * Set: lancamentoCreditoDesc.
	 *
	 * @param lancamentoCreditoDesc the lancamento credito desc
	 */
	public void setLancamentoCreditoDesc(String lancamentoCreditoDesc) {
		this.lancamentoCreditoDesc = lancamentoCreditoDesc;
	}

	/**
	 * Get: lancamentoDebitoDesc.
	 *
	 * @return lancamentoDebitoDesc
	 */
	public String getLancamentoDebitoDesc() {
		return lancamentoDebitoDesc;
	}

	/**
	 * Set: lancamentoDebitoDesc.
	 *
	 * @param lancamentoDebitoDesc the lancamento debito desc
	 */
	public void setLancamentoDebitoDesc(String lancamentoDebitoDesc) {
		this.lancamentoDebitoDesc = lancamentoDebitoDesc;
	}

	/**
	 * Get: tipoRelacionamento.
	 *
	 * @return tipoRelacionamento
	 */
	public Integer getTipoRelacionamento() {
		return tipoRelacionamento;
	}

	/**
	 * Set: tipoRelacionamento.
	 *
	 * @param tipoRelacionamento the tipo relacionamento
	 */
	public void setTipoRelacionamento(Integer tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}

	/**
	 * Get: codLancamentoPersonalizadoHistorico.
	 *
	 * @return codLancamentoPersonalizadoHistorico
	 */
	public String getCodLancamentoPersonalizadoHistorico() {
		return codLancamentoPersonalizadoHistorico;
	}

	/**
	 * Set: codLancamentoPersonalizadoHistorico.
	 *
	 * @param codLancamentoPersonalizadoHistorico the cod lancamento personalizado historico
	 */
	public void setCodLancamentoPersonalizadoHistorico(
			String codLancamentoPersonalizadoHistorico) {
		this.codLancamentoPersonalizadoHistorico = codLancamentoPersonalizadoHistorico;
	}

	/**
	 * Get: listaGridHistorico.
	 *
	 * @return listaGridHistorico
	 */
	public List<HistoricoLanctoPersonalizadoSaidaDTO> getListaGridHistorico() {
		return listaGridHistorico;
	}

	/**
	 * Set: listaGridHistorico.
	 *
	 * @param listaGridHistorico the lista grid historico
	 */
	public void setListaGridHistorico(
			List<HistoricoLanctoPersonalizadoSaidaDTO> listaGridHistorico) {
		this.listaGridHistorico = listaGridHistorico;
	}

	/**
	 * Get: filtroDataAte.
	 *
	 * @return filtroDataAte
	 */
	public Date getFiltroDataAte() {
		return filtroDataAte;
	}

	/**
	 * Set: filtroDataAte.
	 *
	 * @param filtroDataAte the filtro data ate
	 */
	public void setFiltroDataAte(Date filtroDataAte) {
		this.filtroDataAte = filtroDataAte;
	}

	/**
	 * Get: filtroDataDe.
	 *
	 * @return filtroDataDe
	 */
	public Date getFiltroDataDe() {
		return filtroDataDe;
	}

	/**
	 * Set: filtroDataDe.
	 *
	 * @param filtroDataDe the filtro data de
	 */
	public void setFiltroDataDe(Date filtroDataDe) {
		this.filtroDataDe = filtroDataDe;
	}

	/**
	 * Get: dataManutencao.
	 *
	 * @return dataManutencao
	 */
	public String getDataManutencao() {
		return dataManutencao;
	}

	/**
	 * Set: dataManutencao.
	 *
	 * @param dataManutencao the data manutencao
	 */
	public void setDataManutencao(String dataManutencao) {
		this.dataManutencao = dataManutencao;
	}

	/**
	 * Get: responsavel.
	 *
	 * @return responsavel
	 */
	public String getResponsavel() {
		return responsavel;
	}

	/**
	 * Set: responsavel.
	 *
	 * @param responsavel the responsavel
	 */
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	/**
	 * Get: tipoManutencao.
	 *
	 * @return tipoManutencao
	 */
	public String getTipoManutencao() {
		return tipoManutencao;
	}

	/**
	 * Set: tipoManutencao.
	 *
	 * @param tipoManutencao the tipo manutencao
	 */
	public void setTipoManutencao(String tipoManutencao) {
		this.tipoManutencao = tipoManutencao;
	}
	
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		
		return "VOLTAR_INCLUIR";
		
	}

	/**
	 * Get: listaControleRadioHistorico.
	 *
	 * @return listaControleRadioHistorico
	 */
	public List<SelectItem> getListaControleRadioHistorico() {
		return listaControleRadioHistorico;
	}

	/**
	 * Set: listaControleRadioHistorico.
	 *
	 * @param listaControleRadioHistorico the lista controle radio historico
	 */
	public void setListaControleRadioHistorico(
			List<SelectItem> listaControleRadioHistorico) {
		this.listaControleRadioHistorico = listaControleRadioHistorico;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: btoAcionadoHistorico.
	 *
	 * @return btoAcionadoHistorico
	 */
	public Boolean getBtoAcionadoHistorico() {
		return btoAcionadoHistorico;
	}

	/**
	 * Set: btoAcionadoHistorico.
	 *
	 * @param btoAcionadoHistorico the bto acionado historico
	 */
	public void setBtoAcionadoHistorico(Boolean btoAcionadoHistorico) {
		this.btoAcionadoHistorico = btoAcionadoHistorico;
	}
	
	

}
