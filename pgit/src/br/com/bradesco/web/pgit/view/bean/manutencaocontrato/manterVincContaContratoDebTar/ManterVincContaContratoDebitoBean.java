/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterVincContaContratoDebTar;
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 28/10/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterVincContaContratoDebTar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.geral.MensagemSaida;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarTarifaAcimaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarContaDebTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarContaDebTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarTarifaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.DetalharHistoricoDebTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.DetalharHistoricoDebTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirContaDebTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirContaDebTarifaOcorrencias;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContaDebTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContaDebTarifaOcorrencias;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContaDebTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarHistoricoDebTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarHistoricoDebTarifaOcorrencias;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarHistoricoDebTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarOperacaoContaDebTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarOperacaoContaDebTarifaOcorrencias;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarOperacaoContaDebTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.IManterVinculacaoConvenioContaSalarioService;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnListaOcorrenciasDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterVincContaContratoDebitoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterVincContaContratoDebitoBean {

	/** Atributo manterVinculacaoConvenioContaSalarioService. */
	private IManterVinculacaoConvenioContaSalarioService manterVinculacaoConvenioContaSalarioService;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean = null;

	/** Atributo comboService. */
	private IComboService comboService = null;

	/** Atributo identificacaoClienteBean. */
	private IdentificacaoClienteBean identificacaoClienteBean = null;
	
	
	/** Atributo CONF_PAGAMENTO. */
	private static final String MANTER_VINC = "conManterVincContOperacoes";

	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo dtoConvenio. */
	private DetalharVincConvnCtaSalarioSaidaDTO dtoConvenio = new DetalharVincConvnCtaSalarioSaidaDTO();

	/** Atributo listaSelConvenio. */
	private List<ListarDadosCtaConvnListaOcorrenciasDTO> listaSelConvenio = new ArrayList<ListarDadosCtaConvnListaOcorrenciasDTO>();

	/** Atributo listaControleSelConvenio. */
	private List<SelectItem> listaControleSelConvenio = new ArrayList<SelectItem>();

	/** Atributo itemSelecionadoListaConvenio. */
	private Integer itemSelecionadoListaConvenio;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo foco. */
	private String foco;

	/** Atributo habilitaBtoCont. */
	private Boolean habilitaBtoCont;

	/** Atributo btoAcao. */
	private String btoAcao;

	/** Atributo codigoConvenio. */
	private Long codigoConvenio;

	/** Atributo decricaoConvenio. */
	private String decricaoConvenio;

	/** Atributo situacaoConvenio. */
	private String situacaoConvenio;

	/** Atributo bancoAgenciaContaConvenio. */
	private String bancoAgenciaContaConvenio;

	/** Atributo dtHoraInclusao. */
	private String dtHoraInclusao;

	/** Atributo cdUsuario. */
	private String cdUsuario;

	// Cliente Representante
	/** Atributo cpfCnpjRepresentante. */
	private String cpfCnpjRepresentante;

	/** Atributo nomeRazaoRepresentante. */
	private String nomeRazaoRepresentante;

	/** Atributo grupEconRepresentante. */
	private String grupEconRepresentante;

	/** Atributo ativEconRepresentante. */
	private String ativEconRepresentante;

	/** Atributo segRepresentante. */
	private String segRepresentante;

	/** Atributo subSegRepresentante. */
	private String subSegRepresentante;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;

	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado;

	// Cliente Participante
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;

	/** Atributo cpfCnpjParticipanteTemp. */
	private String cpfCnpjParticipanteTemp;

	/** Atributo nomeRazaoParticipanteTemp. */
	private String nomeRazaoParticipanteTemp;

	/** Atributo cpfCnpjParticipanteDet. */
	private String cpfCnpjParticipanteDet;

	/** Atributo nomeRazaoParticipanteDet. */
	private String nomeRazaoParticipanteDet;

	/** Atributo grupoEconParticipante. */
	private String grupoEconParticipante;

	/** Atributo atividadeEconParticipante. */
	private String atividadeEconParticipante;

	/** Atributo segmentoParticipante. */
	private String segmentoParticipante;

	/** Atributo subSegmentoParticipante. */
	private String subSegmentoParticipante;

	/** Atributo dsTipoParticipacao. */
	private String dsTipoParticipacao;

	/** Atributo situacaoParticipacao. */
	private String situacaoParticipacao;

	/** Atributo dsTipoServico. */
	private String dsTipoServico;

	// Contrato
	/** Atributo empresaContrato. */
	private String empresaContrato;

	/** Atributo tipoContrato. */
	private String tipoContrato;

	/** Atributo numeroContrato. */
	private String numeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo situacaoContrato. */
	private String situacaoContrato;

	/** Atributo motivoContrato. */
	private String motivoContrato;

	/** Atributo participacaoContrato. */
	private String participacaoContrato;

	/** Atributo dtManutencaoInicio. */
	private Date dtManutencaoInicio;

	/** Atributo dtManutencaoFim. */
	private Date dtManutencaoFim;

	/* Trilha de Auditoria */
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;


    /** Atributo checkTudo. */
    private boolean checkTudo;
    
    
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo paginaRetorno. */
	private String paginaRetorno;

	/** Atributo saidaAlterarTarifaAcimaPadrao. */
	private AlterarTarifaAcimaPadraoSaidaDTO saidaAlterarTarifaAcimaPadrao = new AlterarTarifaAcimaPadraoSaidaDTO();
	
	/** Atributo listaGridPesquisaAterarValorTarifa. */
	private List<ConsultarTarifaContratoSaidaDTO> listaGridPesquisaAterarValorTarifa;
	
	/** Atributo listaControleRadioGridPesquisaAltValTarifa. */
	private List<SelectItem> listaControleRadioGridPesquisaAltValTarifa = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecListaGridPesquisaAltValTarifa. */
	private Integer itemSelecListaGridPesquisaAltValTarifa;
	
	/** Atributo itemSelecListaGridOperacao. */
	private Integer itemSelecListaGridOperacao;
	
	/** Atributo itemSelecListaGridOperacao. */
	private Integer itemSelecListaContaDebTarifa;
	
	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;
	
	/** Atributo tipoServico. */
	private String tipoServico;
	
	/** Atributo modalidade. */
	private String modalidade;
	
	/** Atributo operacao. */
	private String operacao;
	
	/** Atributo tarifaContratada. */
	private String tarifaContratada;
	
	/** Atributo tarifaMaximaPadrao. */
	private String tarifaMaximaPadrao;
	
	/** Atributo valorRefernciaMinima. */
	private String valorRefernciaMinima;
	
	/** Atributo dtInicioVigencia. */
	private String dtInicioVigencia;
	
	/** Atributo dtFimVigencia. */
	private String dtFimVigencia;
	
	/** Atributo valorTarifa. */
	private BigDecimal valorTarifa;
	
	/** Atributo saidaListarOperacao. */
	private DetalharHistoricoDebTarifaSaidaDTO saidaListarDetHistorico = new DetalharHistoricoDebTarifaSaidaDTO();
	
	/** Atributo saidaListarOperacao. */
	private ConsultarContaDebTarifaSaidaDTO saidaConsultarContaDebTarifa = new ConsultarContaDebTarifaSaidaDTO();
	
	/** Atributo saidaListarOperacao. */
	private ListarOperacaoContaDebTarifaSaidaDTO saidaListarOperacao = new ListarOperacaoContaDebTarifaSaidaDTO();
	
	/** Atributo listaGridListarOperacao. */
	private List<ListarOperacaoContaDebTarifaOcorrencias> listaGridListarOperacao ;
	
	/** Atributo listaGridExcluir. */
	private List<ListarOperacaoContaDebTarifaOcorrencias> listaGridExcluir;
	
	/** Atributo listaGridIncluir. */
	private List<ListarOperacaoContaDebTarifaOcorrencias> listaGridIncluir;
	
	/** Atributo listaControleRadioGridListarOperacao. */
	private List<SelectItem> listaControleRadioGridListarOperacao = new ArrayList<SelectItem>();
	
	/** Atributo saidaListarOperacao. */
	private ListarContaDebTarifaSaidaDTO saidaListarContaDebTarifa = new ListarContaDebTarifaSaidaDTO();
	
	/** Atributo listaGridListarOperacao. */
	private List<ListarContaDebTarifaOcorrencias> listaContaDebTarifa ;
	
	/** Atributo listaControleRadioGridListarOperacao. */
	private List<SelectItem> listaControleRadioGridListaContaDebTarifa = new ArrayList<SelectItem>();
	
	/** Atributo saidaListarHistoricoDeb. */
	private ListarHistoricoDebTarifaSaidaDTO saidaListarHistoricoDeb = new ListarHistoricoDebTarifaSaidaDTO();
	
	/** Atributo listaGridHistoricoDebTarifa. */
	private List<ListarHistoricoDebTarifaOcorrencias> listaGridHistoricoDebTarifa ;
	
	/** Atributo listaControleRadioGridHistoricoDebTarifa. */
	private List<SelectItem> listaControleRadioGridHistoricoDebTarifa = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecListaGridHistoricoDeb. */
	private Integer itemSelecListaGridHistoricoDeb;
	
	/** Atributo dsModModalidade. */
	private String dsModModalidade;
	
	/** Atributo dsTipoServicoModalidade. */
	private String dsTipoServicoModalidade;
	
	/** Atributo dsOperacao. */
	private String dsOperacao;

	/** Atributo banco. */
	private String banco;
	
	/** Atributo agencia. */
	private String agencia;
	
	/** Atributo conta. */
	private String conta;
	
	/** Atributo tipo. */
	private String tipo;
	
	/** Atributo tipoManutencao. */
	private String tipoManutencao;

	/** Atributo dataHoraManut. */
	private String dataHoraManut;
	
	/** Atributo dataIniHist. */
	private Date dataIniHist;
	
	/** Atributo dataFimHist. */
	private Date dataFimHist;
	
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo nomeRazao. */
	private String nomeRazao;
	
	/** Atributo radioArgumentoVincular. */
	private String radioArgumentoVincular;
	
    /** Atributo cdCpfCnpjFiltro. */
    private Long cdCpfCnpjFiltro;

    /** Atributo cdFilialCnpjFiltro. */
    private Integer cdFilialCnpjFiltro;

    /** Atributo cdControleCnpjFiltro. */
    private Integer cdControleCnpjFiltro;
    
    /** Atributo cdCpfFiltro. */
    private String cdCpfFiltro;

    /** Atributo cdControleCpfFiltro. */
    private String cdControleCpfFiltro;

    /** Atributo cpf. */
    private String cpf;

    /** Atributo bancoFiltro. */
    private Integer bancoFiltro;

    /** Atributo agenciaFiltro. */
    private Integer agenciaFiltro;

    /** Atributo agenciaDigitoFiltro. */
    private Integer agenciaDigitoFiltro;

    /** Atributo contaFiltro. */
    private Long contaFiltro;

    /** Atributo digitoFiltro. */
    private String digitoFiltro;

    /** Atributo tipoContaFiltro. */
    private Integer tipoContaFiltro;

    /** Atributo finalidadeFiltro. */
    private Integer finalidadeFiltro;
    
    /** Atributo listaTipoConta. */
    private List<SelectItem> listaTipoConta = new ArrayList<SelectItem>();

    /** Atributo listaTipoContaHash. */
    private Map<Integer, String> listaTipoContaHash = new HashMap<Integer, String>();
    
   private Integer tipoOperacao;
   
   /** Atributo bancoFiltro. */
   private String bancoFiltroInc;

   /** Atributo agenciaFiltro. */
   private String agenciaFiltroInc;

   /** Atributo contaFiltro. */
   private String contaFiltroInc;

   /** Atributo tipoContaFiltro. */
   private String tipoContaFiltroInc;
   
   /** Atributo cdPessoaJuridicaVinculo. */
   private Long cdPessoaJuridicaVinculo;
   
   /** Atributo cdTipoContratoVinculo. */
   private Integer cdTipoContratoVinculo;
   
   /** Atributo nrSequenciaContratoVinculo. */
   private Long nrSequenciaContratoVinculo;
   
   /** Atributo cdTipoVinculoContrato. */
   private Integer cdTipoVinculoContrato;
	
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(final ActionEvent evt) {
		this.identificacaoClienteContratoBean.setPaginaRetorno("conManterVincContaContratoDebitoTr");
		this.identificacaoClienteBean.setPaginaRetorno("conconManterVincContaContratoDebitoTrAlterarValorTarifa");

		filtroAgendamentoEfetivacaoEstornoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean.setPaginaRetorno("conManterVincContaContratoDebitoTr");

		// Limpar Variaveis
		setListaControleRadio(null);
		setItemSelecionadoLista(null);

		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
		setHabilitaBtoCont(true);

		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteManterContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conManterVincContaContratoDebitoTr");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
	}

	/**
	 * Selecionar.
	 *
	 * @return the string
	 */
	public String selecionar() {
		setBtoAcao("");
		setPaginaRetorno("conManterVincContOperacoes");

		final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

		// detalharVinculacao();

		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());

		// Cliente Participante
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null || "".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {

			setCpfCnpjParticipante("");
			setNomeRazaoParticipante("");

		} else {
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}

		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());

		carregaListaOperacao();
		setItemSelecListaGridOperacao(null);
		return getPaginaRetorno();
	}

	/**
	 * Carrega carregaListaOperacao .
	 */
	public void carregaListaOperacao() {
		
		setListaGridListarOperacao(new ArrayList<ListarOperacaoContaDebTarifaOcorrencias>());
		
		final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		try {
			final ListarOperacaoContaDebTarifaEntradaDTO entradaDTO = new ListarOperacaoContaDebTarifaEntradaDTO();
			
			/* Dados do Contrato */
			entradaDTO.setCdPessoaJuridicaNegocio(saidaDTO.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
			entradaDTO.setNrOcorrencias(1);
			
			setSaidaListarOperacao(getManterContratoImpl().listarOperacaoContaDebTarifa(entradaDTO));
			listaGridListarOperacao = getSaidaListarOperacao().getOcorrencias();
			
			this.listaControleRadioGridListarOperacao = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridListarOperacao().size(); i++) {
				listaControleRadioGridListarOperacao.add(new SelectItem(i, " "));
			}
			
			setItemSelecListaGridOperacao(null);
		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridListarOperacao(null);
			setItemSelecListaGridOperacao(null);
		}
	}
	
	/**
	 * Carrega consultarContaDebTarifa .
	 */
	public void consultarContaDebTarifa() {
		
		
		final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		final ListarOperacaoContaDebTarifaOcorrencias itemSelecionado = getListaGridListarOperacao().get(getItemSelecListaGridOperacao());
		
		try {
			final ConsultarContaDebTarifaEntradaDTO entradaDTO = new ConsultarContaDebTarifaEntradaDTO();
			
			/* Dados do Contrato */
			entradaDTO.setCdPessoaJuridicaNegocio(saidaDTO.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
		    entradaDTO.setCdProdutoServicoOperacao(itemSelecionado.getCdProdutoServicoOperacao());
		    entradaDTO.setCdProdutoOperacaoRelacionado(itemSelecionado.getCdProdutoOperacaoRelacionado());
		    entradaDTO.setCdOperacaoProdutoServico(itemSelecionado.getCdOperacaoProdutoServico());
		    entradaDTO.setCdOperacaoServicoIntegrado(itemSelecionado.getCdOperacaoServicoIntegrado());
			
			setSaidaConsultarContaDebTarifa(getManterContratoImpl().consultarContaDebTarifa(entradaDTO));
			
		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
	}
	
    /**
     * Limpar campos filtro conta.
     */
    public void limparCamposFiltroConta() {
	setAgenciaFiltro(null);
	setBancoFiltro(null);
	setAgenciaFiltro(null);
	setContaFiltro(null);
	setDigitoFiltro(null);
	setFinalidadeFiltro(null);
	setTipoContaFiltro(null);
	setAgenciaDigitoFiltro(null);

	setCdCpfCnpjFiltro(null);
	setCdFilialCnpjFiltro(null);
	setCdControleCnpjFiltro(null);
	setCdCpfFiltro("");
	setCdControleCpfFiltro("");

    }
	
    /**
     * Checa todos.
     */
    public void checaTodos() {
        final List<ListarOperacaoContaDebTarifaOcorrencias> lista = getListaGridListarOperacao();

        if (lista != null) {
            for (final ListarOperacaoContaDebTarifaOcorrencias element : lista) {
                element.setCheck(isCheckTudo());
            }
        }

        verificaBemSelecionado();
    }
    
    /**
     * Verifica bem selecionado.
     */
    public void verificaBemSelecionado() {
        if (getListaGridListarOperacao() != null) {
            final List<ListarOperacaoContaDebTarifaOcorrencias> lista = getListaGridListarOperacao();

                int qtdRegistrosSelecionados = 0;

                for (final ListarOperacaoContaDebTarifaOcorrencias element : lista) {
                    if (element.isCheck()) {
                        qtdRegistrosSelecionados++;
                    }
                }

        }
    }
	
    /**
     * Limpa checados.
     */
    public void limpaChecados() {
        setCheckTudo(false);
        if(getListaGridListarOperacao() != null){
            for (int i = 0; i < getListaGridListarOperacao().size(); i++) {
            	getListaGridListarOperacao().get(i).setCheck(isCheckTudo());
            }
        }
    }

    
    /**
     * Limpar Tela Vinculacao.
     */
    public void LimparTelaVincular() {
       limpaChecados();
       setListaGridListarOperacao(new ArrayList<ListarOperacaoContaDebTarifaOcorrencias>());
    }
    
    
  
    /**
    * Preenche detalhar Vinc Cont Operacoes
    *
     * @return the string
     */
	public String detalharVincContOperacoes() {

		setPaginaRetorno("conManterVincContOperacoes");

		final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		final ListarOperacaoContaDebTarifaOcorrencias  itemSelecionado = getListaGridListarOperacao().get(itemSelecListaGridOperacao);

		// detalharVinculacao();

		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());

		// Cliente Participante
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null || "".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {

			setCpfCnpjParticipante("");
			setNomeRazaoParticipante("");

		} else {
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}

		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());

		/* Dados da Tarifa */
		setDsModModalidade(itemSelecionado.getDsProdutoOperacaoRelacionada());
		setDsTipoServicoModalidade(itemSelecionado.getDsProdutoServicoOperacao());
		setDsOperacao(itemSelecionado.getDsOperacaoProdutoServico());
		
       //	dados conta
		setBanco(PgitUtil.concatenarCampos(itemSelecionado.getCdBanco(), itemSelecionado.getDsBanco(), "-"));
		setAgencia(PgitUtil.concatenarCampos(itemSelecionado.getCdAgencia(), itemSelecionado.getDsAgencia(), "-"));
		setConta(PgitUtil.concatenarCampos(itemSelecionado.getCdConta(), itemSelecionado.getCdDigitoConta(), "-"));
		setTipo(itemSelecionado.getDsTipoConta());
		setCpfCnpj(itemSelecionado.getCdCorpoCpfCnpj());
		setNomeRazao(itemSelecionado.getNmParticipante());

		consultarContaDebTarifa();

		return "DETALHAR";
	}
	

    /**
     * Carrega lista tipo conta.
     */
    public void carregaListaTipoConta() {
	listaTipoConta = new ArrayList<SelectItem>();
	List<ListarTipoContaSaidaDTO> listaSaida = new ArrayList<ListarTipoContaSaidaDTO>();
	listaSaida = this.getComboService().listarTipoConta();

	listaTipoContaHash.clear();

	for (final ListarTipoContaSaidaDTO combo : listaSaida) {
	    listaTipoContaHash.put(combo.getCdTipoConta(), combo.getDsTipoConta());
	    this.listaTipoConta.add(new SelectItem(combo.getCdTipoConta(), combo.getDsTipoConta()));
	}

    }
  
   /**
    * Preenche historico Vinc Cont Operacoes
    *
    * @return the string
    */
	public String historicoVincContOperacoes() {
		
		setPaginaRetorno("detManterVincContOperacoes");
		
		final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
		
		// Cliente Participante
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null || "".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
			
			setCpfCnpjParticipante("");
			setNomeRazaoParticipante("");
			
		} else {
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}
		
		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());
		
		setDataIniHist(new Date());
		setDataFimHist(new Date());
		
		setItemSelecListaContaDebTarifa(null);
		setItemSelecListaGridHistoricoDeb(null);
		
		return "HISTORICO";
	}
	
	
	/**
	 * carregar Lista Historico.
	 */
	public void carregarListaHistorico(){ 
			
			setListaGridHistoricoDebTarifa(new ArrayList<ListarHistoricoDebTarifaOcorrencias>());
			
			final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
			
			try {
				final ListarHistoricoDebTarifaEntradaDTO entradaDTO = new ListarHistoricoDebTarifaEntradaDTO();
				
				/* Dados do Contrato */
				entradaDTO.setCdPessoaJuridicaNegocio(saidaDTO.getCdPessoaJuridica());
				entradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
				entradaDTO.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
				entradaDTO.setDataInicio(FormatarData.formataDiaMesAnoToPdc(getDataIniHist()));
				entradaDTO.setDataFim(FormatarData.formataDiaMesAnoToPdc(getDataFimHist()));
				entradaDTO.setNrOcorrencias(10);
				
				setSaidaListarHistoricoDeb(getManterContratoImpl().listarHistoricoDebTarifa(entradaDTO));
				listaGridHistoricoDebTarifa = getSaidaListarHistoricoDeb().getOcorrencias();
				
				this.listaControleRadioGridHistoricoDebTarifa = new ArrayList<SelectItem>();
				for (int i = 0; i < getListaGridHistoricoDebTarifa().size(); i++) {
					listaControleRadioGridHistoricoDebTarifa.add(new SelectItem(i, " "));
				}
				
				setItemSelecListaGridHistoricoDeb(null);
			} catch (final PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				setListaGridHistoricoDebTarifa(null);
				setItemSelecListaGridHistoricoDeb(null);
			}
	}
 
 
   /**
    * detalhar Historico Vinc Cont Operacoes
    *
    * @return the string
    */
   public String detalharHistoricoVincContOperacoes() {
		
		setPaginaRetorno("histManterVincContOperacoes");
		
		final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		final ListarHistoricoDebTarifaOcorrencias  itemSelecionado = getListaGridHistoricoDebTarifa().get(itemSelecListaGridHistoricoDeb);
		
		
		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
		
		// Cliente Participante
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null || "".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
			
			setCpfCnpjParticipante("");
			setNomeRazaoParticipante("");
			
		} else {
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}
		
		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
     	setDescricaoContrato(saidaDTO.getDsContrato());
     
     	
     	/* Dados da Tarifa */
		setDsModModalidade(itemSelecionado.getDsProdutoOperacaoRelacionada());
		setDsTipoServicoModalidade(itemSelecionado.getDsProdutoServicoOperacao());
		setDsOperacao(itemSelecionado.getDsOperacaoProdutoServico());
     	
     	//dados conta
     	setBanco(PgitUtil.concatenarCampos(itemSelecionado.getCdBanco(), itemSelecionado.getDsBanco(), "-"));
		setAgencia(PgitUtil.concatenarCampos(itemSelecionado.getCdAgencia(), itemSelecionado.getDsAgencia(), "-"));
		setConta(PgitUtil.concatenarCampos(itemSelecionado.getCdConta(), itemSelecionado.getCdDigitoConta(), "-"));
		setTipo(itemSelecionado.getDsTipoConta());
		setTipoManutencao(itemSelecionado.getDsTipoManutencao());
		setDataHoraManutencao(itemSelecionado.getDataHoraManuFormatado());
		
		setCpfCnpj(itemSelecionado.getCdCorpoCpfCnpj());
		setNomeRazao(itemSelecionado.getNmParticipante());
		
		detalharHistoricoDebTarifa();
		
		return "DETALHAR_HISTORICO";
	}
   

   /**
    * detalhar Historico Deb Tarifa
    *
    * @return the string
    */
   public void detalharHistoricoDebTarifa(){
   	try {
			final DetalharHistoricoDebTarifaEntradaDTO entradaDTO = new DetalharHistoricoDebTarifaEntradaDTO();
			final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
			final ListarHistoricoDebTarifaOcorrencias itemSelecionado = listaGridHistoricoDebTarifa.get(itemSelecListaGridHistoricoDeb);
			
		    entradaDTO.setNrOcorrencias(1);
		    entradaDTO.setCdPessoaJuridicaNegocio(PgitUtil.verificaLongNulo(saidaDTO.getCdPessoaJuridica()));
		    entradaDTO.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(saidaDTO.getCdTipoContrato()));
		    entradaDTO.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(saidaDTO.getNrSequenciaContrato()));
		    entradaDTO.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(itemSelecionado.getCdProdutoServicoOperacao()));
		    entradaDTO.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(itemSelecionado.getCdProdutoOperacaoRelacionado()));
		    entradaDTO.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(itemSelecionado.getCdOperacaoProdutoServico()));
		    entradaDTO.setHrInclusaoRegistroHist(PgitUtil.verificaStringNula(itemSelecionado.getHrInclusaoRegistroHist()));
			
			setSaidaListarDetHistorico(getManterContratoImpl().detalharHistoricoDebTarifa(entradaDTO));
			
		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		
		}
		
	}


   /**
    * vincular Cont Operacoes
    *
    * @return the string
    */
   public String vincularContOperacoes() {
		
		setPaginaRetorno("conManterVincContOperacoes");
		
		final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
		
		// Cliente Participante
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null || "".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
			
			setCpfCnpjParticipante("");
			setNomeRazaoParticipante("");
			
		} else {
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}
		
		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());
		
		carregaListaOperacao();
		
		limpaChecados();
		
		return "VINCULAR";
	}
   
   /**
    * vincular Cont Operacoes
    *
    * @return the string
    */
   public String incluirVincularContOperacoes() {
	   
	   setPaginaRetorno("vincManterVincContOperacoes");
	   
	   final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
	   
	   // Cliente Representante
	   setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
	   setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
	   setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
	   setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
	   setSegRepresentante(saidaDTO.getDsSegmentoCliente());
	   setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
	   setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
	   setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
	   
	   // Cliente Participante
	   if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null || "".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
		   
		   setCpfCnpjParticipante("");
		   setNomeRazaoParticipante("");
		   
	   } else {
		   setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
		   setNomeRazaoParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? "" : identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
	   }
	   
	   // Contrato
	   setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
	   setTipoContrato(saidaDTO.getDsTipoContrato());
	   setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
	   setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
	   setMotivoContrato(saidaDTO.getDsMotivoSituacao());
	   setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
	   setDescricaoContrato(saidaDTO.getDsContrato());
	   
	   carregaListaTipoConta();
	   setListaContaDebTarifa(new ArrayList<ListarContaDebTarifaOcorrencias>());
	   
	   return "INCLUIR_VINCULAR";
   }
   
   
   /**
    *  listarContaDebTarifa .
    */
   public void listarContaDebTarifa(){
	   
		setListaContaDebTarifa(new ArrayList<ListarContaDebTarifaOcorrencias>());
		
		
		try {

		   final ListarContaDebTarifaEntradaDTO entradaDTO = new ListarContaDebTarifaEntradaDTO();
		   final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
			
		   entradaDTO.setNrOcorrencias(1);
		   entradaDTO.setCdPessoaJuridicaNegocio(saidaDTO.getCdPessoaJuridica());
		   entradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
		   entradaDTO.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
		   
		   entradaDTO.setCdCorpoCpfCnpj(PgitUtil.verificaLongNulo(getCdCpfCnpjFiltro()));
		   entradaDTO.setCdControleCpfCnpj(PgitUtil.verificaIntegerNulo(getCdFilialCnpjFiltro()));
		   entradaDTO.setCdDigitoCpfCnpj(PgitUtil.verificaIntegerNulo(getCdControleCnpjFiltro()));
		   
		   entradaDTO.setCdBanco(PgitUtil.verificaIntegerNulo(getBancoFiltro()));
		   entradaDTO.setCdAgencia(PgitUtil.verificaIntegerNulo(getAgenciaFiltro()));
		   entradaDTO.setCdDigitoAgencia(PgitUtil.verificaIntegerNulo(getAgenciaDigitoFiltro()));
		   entradaDTO.setCdConta(PgitUtil.verificaLongNulo(getContaFiltro()));
		   entradaDTO.setCdDigitoConta(PgitUtil.verificaBytesDigitoConta(getDigitoFiltro()));
		   entradaDTO.setCdTipoConta(PgitUtil.verificaIntegerNulo(getTipoContaFiltro()));
		   
			setSaidaListarContaDebTarifa(getManterContratoImpl().listarContaDebTarifa(entradaDTO));
			listaContaDebTarifa = getSaidaListarContaDebTarifa().getOcorrencias();
			
			this.listaControleRadioGridListaContaDebTarifa = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaContaDebTarifa().size(); i++) {
				listaControleRadioGridListaContaDebTarifa.add(new SelectItem(i, " "));
			}
			
			setItemSelecListaContaDebTarifa(null);
		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaContaDebTarifa(null);
			setItemSelecListaContaDebTarifa(null);
		}
	   
   }
   
   /**
    *  preencheListaConfIncluirVinc .
    */
   public void preencheListaConfIncluirVinc(){
	  setListaGridIncluir(new ArrayList<ListarOperacaoContaDebTarifaOcorrencias>());
	   
	   for (int i = 0; i < getListaGridListarOperacao().size(); i++) {
			   if(getListaGridListarOperacao().get(i).isCheck()){
				   final ListarOperacaoContaDebTarifaOcorrencias obj = new ListarOperacaoContaDebTarifaOcorrencias();
					obj.setCdProdutoServicoOperacaoString(PgitUtil.verificaStringNula(getListaGridListarOperacao().get(i).getCdProdutoServicoOperacaoFormatado()));
					obj.setCdProdutoOperacaoRelacionadoString(PgitUtil.verificaStringNula(getListaGridListarOperacao().get(i).getCdProdutoOperacaoRelacionadoFormatado()));
					obj.setCdOperacaoProdutoServicoString(PgitUtil.verificaStringNula(getListaGridListarOperacao().get(i).getCdOperacaoProdutoServicoFormatado()));
					obj.setCdDigitoAgenciaString(PgitUtil.verificaStringNula(getListaGridListarOperacao().get(i).getCdDigitoAgenciaFormatada()));
					obj.setCdTipoContaString(PgitUtil.verificaStringNula(getListaGridListarOperacao().get(i).getCdTipoContaFormatada()));
                    //					inclusao/exclusao
				     obj.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(getListaGridListarOperacao().get(i).getCdProdutoServicoOperacao()));
				     obj.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(getListaGridListarOperacao().get(i).getCdProdutoOperacaoRelacionado()));
				     obj.setCdServicoCompostoPagamento(PgitUtil.verificaLongNulo(getListaGridListarOperacao().get(i).getCdServicoCompostoPagamento()));
				     obj.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(getListaGridListarOperacao().get(i).getCdOperacaoProdutoServico()));
				     obj.setCdOperacaoServicoIntegrado(PgitUtil.verificaIntegerNulo(getListaGridListarOperacao().get(i).getCdOperacaoServicoIntegrado()));
				     obj.setCdNaturezaOperacaoPagamento(PgitUtil.verificaIntegerNulo(getListaGridListarOperacao().get(i).getCdNaturezaOperacaoPagamento()));
				     
					listaGridIncluir.add(obj);
			   }
	  }
   }
   
   /**
    *  excluirVinculacao .
    */
   public String excluirVinculacao(){
	   historicoVincContOperacoes();
	   
	  setListaGridExcluir(new ArrayList<ListarOperacaoContaDebTarifaOcorrencias>());
	   
	   for (int i = 0; i < getListaGridListarOperacao().size(); i++) {
			   if(getListaGridListarOperacao().get(i).isCheck() && getListaGridListarOperacao().get(i).getCdDigitoAgenciaFormatada()!= null ){
				   final ListarOperacaoContaDebTarifaOcorrencias obj = new ListarOperacaoContaDebTarifaOcorrencias();
					obj.setCdProdutoServicoOperacaoString(PgitUtil.verificaStringNula(getListaGridListarOperacao().get(i).getCdProdutoServicoOperacaoFormatado()));
					obj.setCdProdutoOperacaoRelacionadoString(PgitUtil.verificaStringNula(getListaGridListarOperacao().get(i).getCdProdutoOperacaoRelacionadoFormatado()));
					obj.setCdOperacaoProdutoServicoString(PgitUtil.verificaStringNula(getListaGridListarOperacao().get(i).getCdOperacaoProdutoServicoFormatado()));
					obj.setCdDigitoAgenciaString(PgitUtil.verificaStringNula(getListaGridListarOperacao().get(i).getCdDigitoAgenciaFormatada()));
					obj.setCdTipoContaString(PgitUtil.verificaStringNula(getListaGridListarOperacao().get(i).getCdTipoContaFormatada()));
                    //					inclusao/exclusao
				     obj.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(getListaGridListarOperacao().get(i).getCdProdutoServicoOperacao()));
				     obj.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(getListaGridListarOperacao().get(i).getCdProdutoOperacaoRelacionado()));
				     obj.setCdServicoCompostoPagamento(PgitUtil.verificaLongNulo(getListaGridListarOperacao().get(i).getCdServicoCompostoPagamento()));
				     obj.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(getListaGridListarOperacao().get(i).getCdOperacaoProdutoServico()));
				     obj.setCdOperacaoServicoIntegrado(PgitUtil.verificaIntegerNulo(getListaGridListarOperacao().get(i).getCdOperacaoServicoIntegrado()));
				     obj.setCdNaturezaOperacaoPagamento(PgitUtil.verificaIntegerNulo(getListaGridListarOperacao().get(i).getCdNaturezaOperacaoPagamento()));
				     
					listaGridExcluir.add(obj);
			   }
	  }
	   
	   return "EXCLUIR";
	   
   }
   
   /**
    *  confirmarExclusao .
    */
   public String confirmarExclusao(){
	   setTipoOperacao(2);
	   confirmarExclusaoInclusaoVinc();
	   return MANTER_VINC;
   }
   
   
   /**
    *  confirmarInclusao .
    */
   public String confirmarInclusao(){
	   setTipoOperacao(1);
	   confirmarExclusaoInclusaoVinc();
	   return MANTER_VINC;
   }
   
   
   /**
    *  confirmarExclusaoInclusao .
    */
   public void confirmarExclusaoInclusaoVinc(){
		
		try {

		   final IncluirContaDebTarifaEntradaDTO entradaDTO = new IncluirContaDebTarifaEntradaDTO();
		   final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		   final List<IncluirContaDebTarifaOcorrencias> listaIncluirEntrada = new ArrayList<IncluirContaDebTarifaOcorrencias>();
		   
		    entradaDTO.setCdPessoaJuridicaNegocio(PgitUtil.verificaLongNulo(saidaDTO.getCdPessoaJuridica()));
		    entradaDTO.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(saidaDTO.getCdTipoContrato()));
		    entradaDTO.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(saidaDTO.getNrSequenciaContrato()));
		    entradaDTO.setCdPessoaJuridicaVinculo(PgitUtil.verificaLongNulo(getCdPessoaJuridicaVinculo()));
		    entradaDTO.setCdTipoContratoVinculo(PgitUtil.verificaIntegerNulo(getCdTipoContratoVinculo()));
		    entradaDTO.setNrSequenciaContratoVinculo(PgitUtil.verificaLongNulo(getNrSequenciaContratoVinculo()));
		    entradaDTO.setCdTipoVincContrato(PgitUtil.verificaIntegerNulo(getCdTipoVinculoContrato()));
		    entradaDTO.setCdTipoOperacao(getTipoOperacao());
		    entradaDTO.setOcorrencias(new ArrayList<IncluirContaDebTarifaOcorrencias>());
		  
		    if(getTipoOperacao() != null && getTipoOperacao() != 1){
		    	entradaDTO.setNrOcorrencias(getListaGridExcluir().size());
			    if( getListaGridExcluir()!= null){
				    for(int i = 0; i < getListaGridExcluir().size();i++){
				     final IncluirContaDebTarifaOcorrencias obj = new IncluirContaDebTarifaOcorrencias();
				     
				     obj.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(getListaGridExcluir().get(i).getCdProdutoServicoOperacao()));
				     obj.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(getListaGridExcluir().get(i).getCdProdutoOperacaoRelacionado()));
				     obj.setCdServicoCompostoPagamento(PgitUtil.verificaLongNulo(getListaGridExcluir().get(i).getCdServicoCompostoPagamento()));
				     obj.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(getListaGridExcluir().get(i).getCdOperacaoProdutoServico()));
				     obj.setCdOperacaoServicoIntegrado(PgitUtil.verificaIntegerNulo(getListaGridExcluir().get(i).getCdOperacaoServicoIntegrado()));
				     obj.setCdNaturezaOperacaoPagamento(PgitUtil.verificaIntegerNulo(getListaGridExcluir().get(i).getCdNaturezaOperacaoPagamento()));
			        
				     listaIncluirEntrada.add(obj);
				     entradaDTO.setOcorrencias(listaIncluirEntrada);
				    }
			    }
		    }else{
		    	 if( getListaGridIncluir()!= null){
		    		 entradaDTO.setNrOcorrencias(getListaGridIncluir().size());
					    for(int i = 0; i < getListaGridIncluir().size();i++){
					     final IncluirContaDebTarifaOcorrencias obj = new IncluirContaDebTarifaOcorrencias();
					     
					     obj.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(getListaGridIncluir().get(i).getCdProdutoServicoOperacao()));
					     obj.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(getListaGridIncluir().get(i).getCdProdutoOperacaoRelacionado()));
					     obj.setCdServicoCompostoPagamento(PgitUtil.verificaLongNulo(getListaGridIncluir().get(i).getCdServicoCompostoPagamento()));
					     obj.setCdOperacaoProdutoServico(PgitUtil.verificaIntegerNulo(getListaGridIncluir().get(i).getCdOperacaoProdutoServico()));
					     obj.setCdOperacaoServicoIntegrado(PgitUtil.verificaIntegerNulo(getListaGridIncluir().get(i).getCdOperacaoServicoIntegrado()));
					     obj.setCdNaturezaOperacaoPagamento(PgitUtil.verificaIntegerNulo(getListaGridIncluir().get(i).getCdNaturezaOperacaoPagamento()));
				        
					     listaIncluirEntrada.add(obj);
					     entradaDTO.setOcorrencias(listaIncluirEntrada);
					    }
				    }
		    }
		   
		   final MensagemSaida  mensagem  = getManterContratoImpl().incluirContaDebTarifa(entradaDTO);
		   BradescoFacesUtils.addInfoModalMessage(	"(" + mensagem.getCodMensagem() + ") "+ mensagem.getMensagem(), false);
			
		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
		
		carregaListaOperacao();
	   
  }
   
   /**
    *  confirmarIncluirVinculacao .
    */
   public String confirmarIncluirVinculacao(){
	   final ListarContaDebTarifaOcorrencias itemSelecionado = listaContaDebTarifa.get(itemSelecListaContaDebTarifa);
		
	   historicoVincContOperacoes();
	   
	   setBancoFiltroInc(PgitUtil.concatenarCampos(itemSelecionado.getCdBanco(), itemSelecionado.getDsBanco(), " - "));
	   setAgenciaFiltroInc(PgitUtil.concatenarCampos(itemSelecionado.getCdAgencia(),itemSelecionado.getDsAgencia(), " - "));
	   setContaFiltroInc(PgitUtil.concatenarCampos(itemSelecionado.getCdConta(), itemSelecionado.getCdDigitoConta(), " - "));
	   setTipoContaFiltroInc(itemSelecionado.getDsTipoConta());
	   setCpfCnpj(itemSelecionado.getCdCpfCnpj());
	   setNomeRazao(itemSelecionado.getNmParticipante());
	   
	    setCdPessoaJuridicaVinculo(PgitUtil.verificaLongNulo(itemSelecionado.getCdPessoaJuridicaVinculo()));
	    setCdTipoContratoVinculo(PgitUtil.verificaIntegerNulo(itemSelecionado.getCdTipoContratoVinculo()));
	    setNrSequenciaContratoVinculo(PgitUtil.verificaLongNulo(itemSelecionado.getNrSequenciaContratoVinculo()));
	    setCdTipoVinculoContrato(PgitUtil.verificaIntegerNulo(itemSelecionado.getCdTipoVinculoContrato()));
	   
	   
	   preencheListaConfIncluirVinc();
	   limparDadosIncVinc();
	   
	   return "CONFIRMAR_VINCULAR";
	   
   }
   
   
   /**
    *  isHabilitaExcluir .
    */
   public boolean isHabilitaExcluir(){
	   for(int i = 0 ; i<getListaGridListarOperacao().size();i++){
			if(getListaGridListarOperacao().get(i).isCheck() && !"".equals(getListaGridListarOperacao().get(i).getCdDigitoAgenciaFormatada())){
				return false;
			}
		}
	   
	   return true;
   }
   
   /**
    *  isHabilitaIncluir .
    */
   public boolean isHabilitaIncluir(){
	   for(int i = 0 ; i<getListaGridListarOperacao().size();i++){
		   if(getListaGridListarOperacao().get(i).isCheck()){
			   return false;
		   }
	   }
	   
	   return true;
   }

   
   /**
    *  limparDadosIncVinc .
    */
  public void limparDadosIncVinc(){
	  limparCamposFiltroConta();
	  setListaContaDebTarifa(new ArrayList<ListarContaDebTarifaOcorrencias>());
	  
  }
	/**
	 * Carrega carregaListaVinculacao .
	 */
	public void carregaListaVinculacao() {
		
		setListaGridListarOperacao(new ArrayList<ListarOperacaoContaDebTarifaOcorrencias>());
		
		final ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		try {
			final ListarOperacaoContaDebTarifaEntradaDTO entradaDTO = new ListarOperacaoContaDebTarifaEntradaDTO();
			
			/* Dados do Contrato */
			entradaDTO.setCdPessoaJuridicaNegocio(saidaDTO.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
			entradaDTO.setNrOcorrencias(1);
			
			setSaidaListarOperacao(getManterContratoImpl().listarOperacaoContaDebTarifa(entradaDTO));
			listaGridListarOperacao = getSaidaListarOperacao().getOcorrencias();
			
			this.listaControleRadioGridListarOperacao = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridListarOperacao().size(); i++) {
				listaControleRadioGridListarOperacao.add(new SelectItem(i, " "));
			}
			
			setItemSelecListaGridOperacao(null);
		} catch (final PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridListarOperacao(null);
			setItemSelecListaGridOperacao(null);
		
		}
		
	}
	
	/**
	 * limpar Campos Historico
	 */
	public void limparCamposHistorico() {
		setDataIniHist(null);
		setDataFimHist(null);
	}
	
	
	/**
	 * limpar Tudo Historico.
	 */
	public void limparTudoHistorico() {
		setDataIniHist(null);
		setDataFimHist(null);
		setListaGridHistoricoDebTarifa(new ArrayList<ListarHistoricoDebTarifaOcorrencias>());
	}
	
	

	/**
	 * Voltar consulta contrato.
	 *
	 * @return the string
	 */
	public String voltarConsultaOperacao() {
		limpaChecados();
		setItemSelecListaGridHistoricoDeb(null);
		carregaListaOperacao();
		return "VOLTAR";
	}

	/**
	 * paginar Contas.
	 *
	 * 
	 */
	public void paginarContas(final ActionEvent evt) {
		carregaListaOperacao();
	}

	/**
	 * Get: manterVinculacaoConvenioContaSalarioService.
	 *
	 * @return manterVinculacaoConvenioContaSalarioService
	 */
	public IManterVinculacaoConvenioContaSalarioService getManterVinculacaoConvenioContaSalarioService() {
		return manterVinculacaoConvenioContaSalarioService;
	}

	/**
	 * Set: manterVinculacaoConvenioContaSalarioService.
	 *
	 * @param manterVinculacaoConvenioContaSalarioService the manter vinculacao convenio conta salario service
	 */
	public void setManterVinculacaoConvenioContaSalarioService(final IManterVinculacaoConvenioContaSalarioService manterVinculacaoConvenioContaSalarioService) {
		this.manterVinculacaoConvenioContaSalarioService = manterVinculacaoConvenioContaSalarioService;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(final IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: identificacaoClienteBean.
	 *
	 * @return identificacaoClienteBean
	 */
	public IdentificacaoClienteBean getIdentificacaoClienteBean() {
		return identificacaoClienteBean;
	}

	/**
	 * Set: identificacaoClienteBean.
	 *
	 * @param identificacaoClienteBean the identificacao cliente bean
	 */
	public void setIdentificacaoClienteBean(final IdentificacaoClienteBean identificacaoClienteBean) {
		this.identificacaoClienteBean = identificacaoClienteBean;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(final FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(final List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(final Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: habilitaBtoCont.
	 *
	 * @return habilitaBtoCont
	 */
	public Boolean getHabilitaBtoCont() {
		return habilitaBtoCont;
	}

	/**
	 * Set: habilitaBtoCont.
	 *
	 * @param habilitaBtoCont the habilita bto cont
	 */
	public void setHabilitaBtoCont(final Boolean habilitaBtoCont) {
		this.habilitaBtoCont = habilitaBtoCont;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(final IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: foco.
	 *
	 * @return foco
	 */
	public String getFoco() {
		return foco;
	}

	/**
	 * Set: foco.
	 *
	 * @param foco the foco
	 */
	public void setFoco(final String foco) {
		this.foco = foco;
	}

	/**
	 * Get: codigoConvenio.
	 *
	 * @return codigoConvenio
	 */
	public Long getCodigoConvenio() {
		return codigoConvenio;
	}

	/**
	 * Set: codigoConvenio.
	 *
	 * @param codigoConvenio the codigo convenio
	 */
	public void setCodigoConvenio(final Long codigoConvenio) {
		this.codigoConvenio = codigoConvenio;
	}

	/**
	 * Get: cpfCnpjRepresentante.
	 *
	 * @return cpfCnpjRepresentante
	 */
	public String getCpfCnpjRepresentante() {
		return cpfCnpjRepresentante;
	}

	/**
	 * Set: cpfCnpjRepresentante.
	 *
	 * @param cpfCnpjRepresentante the cpf cnpj representante
	 */
	public void setCpfCnpjRepresentante(final String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	/**
	 * Get: nomeRazaoRepresentante.
	 *
	 * @return nomeRazaoRepresentante
	 */
	public String getNomeRazaoRepresentante() {
		return nomeRazaoRepresentante;
	}

	/**
	 * Set: nomeRazaoRepresentante.
	 *
	 * @param nomeRazaoRepresentante the nome razao representante
	 */
	public void setNomeRazaoRepresentante(final String nomeRazaoRepresentante) {
		this.nomeRazaoRepresentante = nomeRazaoRepresentante;
	}

	/**
	 * Get: grupEconRepresentante.
	 *
	 * @return grupEconRepresentante
	 */
	public String getGrupEconRepresentante() {
		return grupEconRepresentante;
	}

	/**
	 * Set: grupEconRepresentante.
	 *
	 * @param grupEconRepresentante the grup econ representante
	 */
	public void setGrupEconRepresentante(final String grupEconRepresentante) {
		this.grupEconRepresentante = grupEconRepresentante;
	}

	/**
	 * Get: ativEconRepresentante.
	 *
	 * @return ativEconRepresentante
	 */
	public String getAtivEconRepresentante() {
		return ativEconRepresentante;
	}

	/**
	 * Set: ativEconRepresentante.
	 *
	 * @param ativEconRepresentante the ativ econ representante
	 */
	public void setAtivEconRepresentante(final String ativEconRepresentante) {
		this.ativEconRepresentante = ativEconRepresentante;
	}

	/**
	 * Get: segRepresentante.
	 *
	 * @return segRepresentante
	 */
	public String getSegRepresentante() {
		return segRepresentante;
	}

	/**
	 * Set: segRepresentante.
	 *
	 * @param segRepresentante the seg representante
	 */
	public void setSegRepresentante(final String segRepresentante) {
		this.segRepresentante = segRepresentante;
	}

	/**
	 * Get: subSegRepresentante.
	 *
	 * @return subSegRepresentante
	 */
	public String getSubSegRepresentante() {
		return subSegRepresentante;
	}

	/**
	 * Set: subSegRepresentante.
	 *
	 * @param subSegRepresentante the sub seg representante
	 */
	public void setSubSegRepresentante(final String subSegRepresentante) {
		this.subSegRepresentante = subSegRepresentante;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(final String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(final String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: gerenteResponsavelFormatado.
	 *
	 * @return gerenteResponsavelFormatado
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}

	/**
	 * Set: gerenteResponsavelFormatado.
	 *
	 * @param gerenteResponsavelFormatado the gerente responsavel formatado
	 */
	public void setGerenteResponsavelFormatado(final String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(final String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(final String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: cpfCnpjParticipanteTemp.
	 *
	 * @return cpfCnpjParticipanteTemp
	 */
	public String getCpfCnpjParticipanteTemp() {
		return cpfCnpjParticipanteTemp;
	}

	/**
	 * Set: cpfCnpjParticipanteTemp.
	 *
	 * @param cpfCnpjParticipanteTemp the cpf cnpj participante temp
	 */
	public void setCpfCnpjParticipanteTemp(final String cpfCnpjParticipanteTemp) {
		this.cpfCnpjParticipanteTemp = cpfCnpjParticipanteTemp;
	}

	/**
	 * Get: nomeRazaoParticipanteTemp.
	 *
	 * @return nomeRazaoParticipanteTemp
	 */
	public String getNomeRazaoParticipanteTemp() {
		return nomeRazaoParticipanteTemp;
	}

	/**
	 * Set: nomeRazaoParticipanteTemp.
	 *
	 * @param nomeRazaoParticipanteTemp the nome razao participante temp
	 */
	public void setNomeRazaoParticipanteTemp(final String nomeRazaoParticipanteTemp) {
		this.nomeRazaoParticipanteTemp = nomeRazaoParticipanteTemp;
	}

	/**
	 * Get: cpfCnpjParticipanteDet.
	 *
	 * @return cpfCnpjParticipanteDet
	 */
	public String getCpfCnpjParticipanteDet() {
		return cpfCnpjParticipanteDet;
	}

	/**
	 * Set: cpfCnpjParticipanteDet.
	 *
	 * @param cpfCnpjParticipanteDet the cpf cnpj participante det
	 */
	public void setCpfCnpjParticipanteDet(final String cpfCnpjParticipanteDet) {
		this.cpfCnpjParticipanteDet = cpfCnpjParticipanteDet;
	}

	/**
	 * Get: nomeRazaoParticipanteDet.
	 *
	 * @return nomeRazaoParticipanteDet
	 */
	public String getNomeRazaoParticipanteDet() {
		return nomeRazaoParticipanteDet;
	}

	/**
	 * Set: nomeRazaoParticipanteDet.
	 *
	 * @param nomeRazaoParticipanteDet the nome razao participante det
	 */
	public void setNomeRazaoParticipanteDet(final String nomeRazaoParticipanteDet) {
		this.nomeRazaoParticipanteDet = nomeRazaoParticipanteDet;
	}

	/**
	 * Get: grupoEconParticipante.
	 *
	 * @return grupoEconParticipante
	 */
	public String getGrupoEconParticipante() {
		return grupoEconParticipante;
	}

	/**
	 * Set: grupoEconParticipante.
	 *
	 * @param grupoEconParticipante the grupo econ participante
	 */
	public void setGrupoEconParticipante(final String grupoEconParticipante) {
		this.grupoEconParticipante = grupoEconParticipante;
	}

	/**
	 * Get: atividadeEconParticipante.
	 *
	 * @return atividadeEconParticipante
	 */
	public String getAtividadeEconParticipante() {
		return atividadeEconParticipante;
	}

	/**
	 * Set: atividadeEconParticipante.
	 *
	 * @param atividadeEconParticipante the atividade econ participante
	 */
	public void setAtividadeEconParticipante(final String atividadeEconParticipante) {
		this.atividadeEconParticipante = atividadeEconParticipante;
	}

	/**
	 * Get: segmentoParticipante.
	 *
	 * @return segmentoParticipante
	 */
	public String getSegmentoParticipante() {
		return segmentoParticipante;
	}

	/**
	 * Set: segmentoParticipante.
	 *
	 * @param segmentoParticipante the segmento participante
	 */
	public void setSegmentoParticipante(final String segmentoParticipante) {
		this.segmentoParticipante = segmentoParticipante;
	}

	/**
	 * Get: subSegmentoParticipante.
	 *
	 * @return subSegmentoParticipante
	 */
	public String getSubSegmentoParticipante() {
		return subSegmentoParticipante;
	}

	/**
	 * Set: subSegmentoParticipante.
	 *
	 * @param subSegmentoParticipante the sub segmento participante
	 */
	public void setSubSegmentoParticipante(final String subSegmentoParticipante) {
		this.subSegmentoParticipante = subSegmentoParticipante;
	}

	/**
	 * Get: dsTipoParticipacao.
	 *
	 * @return dsTipoParticipacao
	 */
	public String getDsTipoParticipacao() {
		return dsTipoParticipacao;
	}

	/**
	 * Set: dsTipoParticipacao.
	 *
	 * @param dsTipoParticipacao the ds tipo participacao
	 */
	public void setDsTipoParticipacao(final String dsTipoParticipacao) {
		this.dsTipoParticipacao = dsTipoParticipacao;
	}

	/**
	 * Get: situacaoParticipacao.
	 *
	 * @return situacaoParticipacao
	 */
	public String getSituacaoParticipacao() {
		return situacaoParticipacao;
	}

	/**
	 * Set: situacaoParticipacao.
	 *
	 * @param situacaoParticipacao the situacao participacao
	 */
	public void setSituacaoParticipacao(final String situacaoParticipacao) {
		this.situacaoParticipacao = situacaoParticipacao;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(final String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(final String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(final String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(final String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(final String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(final String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(final String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(final String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: dtManutencaoInicio.
	 *
	 * @return dtManutencaoInicio
	 */
	public Date getDtManutencaoInicio() {
		return dtManutencaoInicio;
	}

	/**
	 * Set: dtManutencaoInicio.
	 *
	 * @param dtManutencaoInicio the dt manutencao inicio
	 */
	public void setDtManutencaoInicio(final Date dtManutencaoInicio) {
		this.dtManutencaoInicio = dtManutencaoInicio;
	}

	/**
	 * Get: dtManutencaoFim.
	 *
	 * @return dtManutencaoFim
	 */
	public Date getDtManutencaoFim() {
		return dtManutencaoFim;
	}

	/**
	 * Set: dtManutencaoFim.
	 *
	 * @param dtManutencaoFim the dt manutencao fim
	 */
	public void setDtManutencaoFim(final Date dtManutencaoFim) {
		this.dtManutencaoFim = dtManutencaoFim;
	}

	/**
	 * Get: decricaoConvenio.
	 *
	 * @return decricaoConvenio
	 */
	public String getDecricaoConvenio() {
		return decricaoConvenio;
	}

	/**
	 * Set: decricaoConvenio.
	 *
	 * @param decricaoConvenio the decricao convenio
	 */
	public void setDecricaoConvenio(final String decricaoConvenio) {
		this.decricaoConvenio = decricaoConvenio;
	}

	/**
	 * Get: situacaoConvenio.
	 *
	 * @return situacaoConvenio
	 */
	public String getSituacaoConvenio() {
		return situacaoConvenio;
	}

	/**
	 * Set: situacaoConvenio.
	 *
	 * @param situacaoConvenio the situacao convenio
	 */
	public void setSituacaoConvenio(final String situacaoConvenio) {
		this.situacaoConvenio = situacaoConvenio;
	}

	/**
	 * Get: bancoAgenciaContaConvenio.
	 *
	 * @return bancoAgenciaContaConvenio
	 */
	public String getBancoAgenciaContaConvenio() {
		return bancoAgenciaContaConvenio;
	}

	/**
	 * Set: bancoAgenciaContaConvenio.
	 *
	 * @param bancoAgenciaContaConvenio the banco agencia conta convenio
	 */
	public void setBancoAgenciaContaConvenio(final String bancoAgenciaContaConvenio) {
		this.bancoAgenciaContaConvenio = bancoAgenciaContaConvenio;
	}

	/**
	 * Get: dtoConvenio.
	 *
	 * @return dtoConvenio
	 */
	public DetalharVincConvnCtaSalarioSaidaDTO getDtoConvenio() {
		return dtoConvenio;
	}

	/**
	 * Set: dtoConvenio.
	 *
	 * @param dtoConvenio the dto convenio
	 */
	public void setDtoConvenio(final DetalharVincConvnCtaSalarioSaidaDTO dtoConvenio) {
		this.dtoConvenio = dtoConvenio;
	}

	/**
	 * Get: btoAcao.
	 *
	 * @return btoAcao
	 */
	public String getBtoAcao() {
		return btoAcao;
	}

	/**
	 * Set: btoAcao.
	 *
	 * @param btoAcao the bto acao
	 */
	public void setBtoAcao(final String btoAcao) {
		this.btoAcao = btoAcao;
	}

	/**
	 * Get: listaSelConvenio.
	 *
	 * @return listaSelConvenio
	 */
	public List<ListarDadosCtaConvnListaOcorrenciasDTO> getListaSelConvenio() {
		return listaSelConvenio;
	}

	/**
	 * Set: listaSelConvenio.
	 *
	 * @param listaSelConvenio the lista sel convenio
	 */
	public void setListaSelConvenio(final List<ListarDadosCtaConvnListaOcorrenciasDTO> listaSelConvenio) {
		this.listaSelConvenio = listaSelConvenio;
	}

	/**
	 * Get: listaControleSelConvenio.
	 *
	 * @return listaControleSelConvenio
	 */
	public List<SelectItem> getListaControleSelConvenio() {
		return listaControleSelConvenio;
	}

	/**
	 * Set: listaControleSelConvenio.
	 *
	 * @param listaControleSelConvenio the lista controle sel convenio
	 */
	public void setListaControleSelConvenio(final List<SelectItem> listaControleSelConvenio) {
		this.listaControleSelConvenio = listaControleSelConvenio;
	}

	/**
	 * Get: itemSelecionadoListaConvenio.
	 *
	 * @return itemSelecionadoListaConvenio
	 */
	public Integer getItemSelecionadoListaConvenio() {
		return itemSelecionadoListaConvenio;
	}

	/**
	 * Set: itemSelecionadoListaConvenio.
	 *
	 * @param itemSelecionadoListaConvenio the item selecionado lista convenio
	 */
	public void setItemSelecionadoListaConvenio(final Integer itemSelecionadoListaConvenio) {
		this.itemSelecionadoListaConvenio = itemSelecionadoListaConvenio;
	}

	/**
	 * Get: dtHoraInclusao.
	 *
	 * @return dtHoraInclusao
	 */
	public String getDtHoraInclusao() {
		return dtHoraInclusao;
	}

	/**
	 * Set: dtHoraInclusao.
	 *
	 * @param dtHoraInclusao the dt hora inclusao
	 */
	public void setDtHoraInclusao(final String dtHoraInclusao) {
		this.dtHoraInclusao = dtHoraInclusao;
	}

	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}

	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(final String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(final String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(final String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(final String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(final String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(final String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(final String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(final String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(final String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: paginaRetorno.
	 *
	 * @return paginaRetorno
	 */
	public String getPaginaRetorno() {
		return paginaRetorno;
	}

	/**
	 * Set: paginaRetorno.
	 *
	 * @param paginaRetorno the pagina retorno
	 */
	public void setPaginaRetorno(final String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(final IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: itemSelecListaGridPesquisaAltValTarifa.
	 *
	 * @param itemSelecListaGridPesquisaAltValTarifa the item selec lista grid pesquisa alt val tarifa
	 */
	public void setItemSelecListaGridPesquisaAltValTarifa(final Integer itemSelecListaGridPesquisaAltValTarifa) {
		this.itemSelecListaGridPesquisaAltValTarifa = itemSelecListaGridPesquisaAltValTarifa;
	}

	/**
	 * Get: itemSelecListaGridPesquisaAltValTarifa.
	 *
	 * @return itemSelecListaGridPesquisaAltValTarifa
	 */
	public Integer getItemSelecListaGridPesquisaAltValTarifa() {
		return itemSelecListaGridPesquisaAltValTarifa;
	}

	/**
	 * Get: listaControleRadioGridPesquisaAltValTarifa.
	 *
	 * @return listaControleRadioGridPesquisaAltValTarifa
	 */
	public List<SelectItem> getListaControleRadioGridPesquisaAltValTarifa() {
		return listaControleRadioGridPesquisaAltValTarifa;
	}

	/**
	 * Set: listaControleRadioGridPesquisaAltValTarifa.
	 *
	 * @param listaControleRadioGridPesquisaAltValTarifa the lista controle radio grid pesquisa alt val tarifa
	 */
	public void setListaControleRadioGridPesquisaAltValTarifa(final List<SelectItem> listaControleRadioGridPesquisaAltValTarifa) {
		this.listaControleRadioGridPesquisaAltValTarifa = listaControleRadioGridPesquisaAltValTarifa;
	}

	/**
	 * Set: listaGridPesquisaAterarValorTarifa.
	 *
	 * @param listaGridPesquisaAterarValorTarifa the lista grid pesquisa aterar valor tarifa
	 */
	public void setListaGridPesquisaAterarValorTarifa(final List<ConsultarTarifaContratoSaidaDTO> listaGridPesquisaAterarValorTarifa) {
		this.listaGridPesquisaAterarValorTarifa = listaGridPesquisaAterarValorTarifa;
	}

	/**
	 * Get: listaGridPesquisaAterarValorTarifa.
	 *
	 * @return listaGridPesquisaAterarValorTarifa
	 */
	public List<ConsultarTarifaContratoSaidaDTO> getListaGridPesquisaAterarValorTarifa() {
		return listaGridPesquisaAterarValorTarifa;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public String getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(final String tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Get: modalidade.
	 *
	 * @return modalidade
	 */
	public String getModalidade() {
		return modalidade;
	}

	/**
	 * Set: modalidade.
	 *
	 * @param modalidade the modalidade
	 */
	public void setModalidade(final String modalidade) {
		this.modalidade = modalidade;
	}

	/**
	 * Get: operacao.
	 *
	 * @return operacao
	 */
	public String getOperacao() {
		return operacao;
	}

	/**
	 * Set: operacao.
	 *
	 * @param operacao the operacao
	 */
	public void setOperacao(final String operacao) {
		this.operacao = operacao;
	}

	/**
	 * Set: tarifaContratada.
	 *
	 * @param tarifaContratada the tarifa contratada
	 */
	public void setTarifaContratada(final String tarifaContratada) {
		this.tarifaContratada = tarifaContratada;
	}

	/**
	 * Get: tarifaContratada.
	 *
	 * @return tarifaContratada
	 */
	public String getTarifaContratada() {
		return tarifaContratada;
	}

	/**
	 * Set: tarifaMaximaPadrao.
	 *
	 * @param tarifaMaximaPadrao the tarifa maxima padrao
	 */
	public void setTarifaMaximaPadrao(final String tarifaMaximaPadrao) {
		this.tarifaMaximaPadrao = tarifaMaximaPadrao;
	}

	/**
	 * Get: tarifaMaximaPadrao.
	 *
	 * @return tarifaMaximaPadrao
	 */
	public String getTarifaMaximaPadrao() {
		return tarifaMaximaPadrao;
	}

	/**
	 * Set: dtInicioVigencia.
	 *
	 * @param dtInicioVigencia the dt inicio vigencia
	 */
	public void setDtInicioVigencia(final String dtInicioVigencia) {
		this.dtInicioVigencia = dtInicioVigencia;
	}

	/**
	 * Get: dtInicioVigencia.
	 *
	 * @return dtInicioVigencia
	 */
	public String getDtInicioVigencia() {
		return dtInicioVigencia;
	}

	/**
	 * Set: dtFimVigencia.
	 *
	 * @param dtFimVigencia the dt fim vigencia
	 */
	public void setDtFimVigencia(final String dtFimVigencia) {
		this.dtFimVigencia = dtFimVigencia;
	}

	/**
	 * Get: dtFimVigencia.
	 *
	 * @return dtFimVigencia
	 */
	public String getDtFimVigencia() {
		return dtFimVigencia;
	}

	/**
	 * Set: valorTarifa.
	 *
	 * @param valorTarifa the valor tarifa
	 */
	public void setValorTarifa(final BigDecimal valorTarifa) {
		this.valorTarifa = valorTarifa;
	}

	/**
	 * Get: valorTarifa.
	 *
	 * @return valorTarifa
	 */
	public BigDecimal getValorTarifa() {
		return valorTarifa;
	}

	/**
	 * Set: valorRefernciaMinima.
	 *
	 * @param valorRefernciaMinima the valor referncia minima
	 */
	public void setValorRefernciaMinima(final String valorRefernciaMinima) {
		this.valorRefernciaMinima = valorRefernciaMinima;
	}

	/**
	 * Get: valorRefernciaMinima.
	 *
	 * @return valorRefernciaMinima
	 */
	public String getValorRefernciaMinima() {
		return valorRefernciaMinima;
	}

	/**
	 * Get: saidaAlterarTarifaAcimaPadrao.
	 *
	 * @return saidaAlterarTarifaAcimaPadrao
	 */
	public AlterarTarifaAcimaPadraoSaidaDTO getSaidaAlterarTarifaAcimaPadrao() {
		return saidaAlterarTarifaAcimaPadrao;
	}

	/**
	 * Set: saidaAlterarTarifaAcimaPadrao.
	 *
	 * @param saidaAlterarTarifaAcimaPadrao the saida alterar tarifa acima padrao
	 */
	public void setSaidaAlterarTarifaAcimaPadrao(final AlterarTarifaAcimaPadraoSaidaDTO saidaAlterarTarifaAcimaPadrao) {
		this.saidaAlterarTarifaAcimaPadrao = saidaAlterarTarifaAcimaPadrao;
	}

	public ListarOperacaoContaDebTarifaSaidaDTO getSaidaListarOperacao() {
		return saidaListarOperacao;
	}

	public void setSaidaListarOperacao(
			final ListarOperacaoContaDebTarifaSaidaDTO saidaListarOperacao) {
		this.saidaListarOperacao = saidaListarOperacao;
	}

	public List<ListarOperacaoContaDebTarifaOcorrencias> getListaGridListarOperacao() {
		return listaGridListarOperacao;
	}

	public void setListaGridListarOperacao(
			final List<ListarOperacaoContaDebTarifaOcorrencias> listaGridListarOperacao) {
		this.listaGridListarOperacao = listaGridListarOperacao;
	}

	public List<SelectItem> getListaControleRadioGridListarOperacao() {
		return listaControleRadioGridListarOperacao;
	}

	public void setListaControleRadioGridListarOperacao(
			final List<SelectItem> listaControleRadioGridListarOperacao) {
		this.listaControleRadioGridListarOperacao = listaControleRadioGridListarOperacao;
	}

	public Integer getItemSelecListaGridOperacao() {
		return itemSelecListaGridOperacao;
	}

	public void setItemSelecListaGridOperacao(final Integer itemSelecListaGridOperacao) {
		this.itemSelecListaGridOperacao = itemSelecListaGridOperacao;
	}

	public String getDsModModalidade() {
		return dsModModalidade;
	}

	public void setDsModModalidade(final String dsModModalidade) {
		this.dsModModalidade = dsModModalidade;
	}

	public String getDsTipoServicoModalidade() {
		return dsTipoServicoModalidade;
	}

	public void setDsTipoServicoModalidade(final String dsTipoServicoModalidade) {
		this.dsTipoServicoModalidade = dsTipoServicoModalidade;
	}

	public String getDsOperacao() {
		return dsOperacao;
	}

	public void setDsOperacao(final String dsOperacao) {
		this.dsOperacao = dsOperacao;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(final String banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(final String agencia) {
		this.agencia = agencia;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(final String conta) {
		this.conta = conta;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(final String tipo) {
		this.tipo = tipo;
	}

	public Date getDataIniHist() {
		return dataIniHist;
	}

	public void setDataIniHist(final Date dataIniHist) {
		this.dataIniHist = dataIniHist;
	}

	public Date getDataFimHist() {
		return dataFimHist;
	}

	public void setDataFimHist(final Date dataFimHist) {
		this.dataFimHist = dataFimHist;
	}

	public ListarHistoricoDebTarifaSaidaDTO getSaidaListarHistoricoDeb() {
		return saidaListarHistoricoDeb;
	}

	public void setSaidaListarHistoricoDeb(
			final ListarHistoricoDebTarifaSaidaDTO saidaListarHistoricoDeb) {
		this.saidaListarHistoricoDeb = saidaListarHistoricoDeb;
	}

	public List<ListarHistoricoDebTarifaOcorrencias> getListaGridHistoricoDebTarifa() {
		return listaGridHistoricoDebTarifa;
	}

	public void setListaGridHistoricoDebTarifa(
			final List<ListarHistoricoDebTarifaOcorrencias> listaGridHistoricoDebTarifa) {
		this.listaGridHistoricoDebTarifa = listaGridHistoricoDebTarifa;
	}

	public List<SelectItem> getListaControleRadioGridHistoricoDebTarifa() {
		return listaControleRadioGridHistoricoDebTarifa;
	}

	public void setListaControleRadioGridHistoricoDebTarifa(
			final List<SelectItem> listaControleRadioGridHistoricoDebTarifa) {
		this.listaControleRadioGridHistoricoDebTarifa = listaControleRadioGridHistoricoDebTarifa;
	}

	public Integer getItemSelecListaGridHistoricoDeb() {
		return itemSelecListaGridHistoricoDeb;
	}

	public void setItemSelecListaGridHistoricoDeb(
			final Integer itemSelecListaGridHistoricoDeb) {
		this.itemSelecListaGridHistoricoDeb = itemSelecListaGridHistoricoDeb;
	}

	public String getTipoManutencao() {
		return tipoManutencao;
	}

	public void setTipoManutencao(final String tipoManutencao) {
		this.tipoManutencao = tipoManutencao;
	}

	public String getDataHoraManut() {
		return dataHoraManut;
	}

	public void setDataHoraManut(final String dataHoraManut) {
		this.dataHoraManut = dataHoraManut;
	}

	public DetalharHistoricoDebTarifaSaidaDTO getSaidaListarDetHistorico() {
		return saidaListarDetHistorico;
	}

	public void setSaidaListarDetHistorico(
			final DetalharHistoricoDebTarifaSaidaDTO saidaListarDetHistorico) {
		this.saidaListarDetHistorico = saidaListarDetHistorico;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(final String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNomeRazao() {
		return nomeRazao;
	}

	public void setNomeRazao(final String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}

	public boolean isCheckTudo() {
		return checkTudo;
	}

	public void setCheckTudo(final boolean checkTudo) {
		this.checkTudo = checkTudo;
	}

	public String getCdCpfFiltro() {
		return cdCpfFiltro;
	}

	public void setCdCpfFiltro(final String cdCpfFiltro) {
		this.cdCpfFiltro = cdCpfFiltro;
	}

	public String getCdControleCpfFiltro() {
		return cdControleCpfFiltro;
	}

	public void setCdControleCpfFiltro(final String cdControleCpfFiltro) {
		this.cdControleCpfFiltro = cdControleCpfFiltro;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(final String cpf) {
		this.cpf = cpf;
	}

	public String getDigitoFiltro() {
		return digitoFiltro;
	}

	public void setDigitoFiltro(final String digitoFiltro) {
		this.digitoFiltro = digitoFiltro;
	}

	public Integer getTipoContaFiltro() {
		return tipoContaFiltro;
	}

	public void setTipoContaFiltro(final Integer tipoContaFiltro) {
		this.tipoContaFiltro = tipoContaFiltro;
	}

	public Integer getFinalidadeFiltro() {
		return finalidadeFiltro;
	}

	public void setFinalidadeFiltro(final Integer finalidadeFiltro) {
		this.finalidadeFiltro = finalidadeFiltro;
	}

	public List<SelectItem> getListaTipoConta() {
		return listaTipoConta;
	}

	public void setListaTipoConta(final List<SelectItem> listaTipoConta) {
		this.listaTipoConta = listaTipoConta;
	}

	public Map<Integer, String> getListaTipoContaHash() {
		return listaTipoContaHash;
	}

	public void setListaTipoContaHash(final Map<Integer, String> listaTipoContaHash) {
		this.listaTipoContaHash = listaTipoContaHash;
	}

	public ListarContaDebTarifaSaidaDTO getSaidaListarContaDebTarifa() {
		return saidaListarContaDebTarifa;
	}

	public void setSaidaListarContaDebTarifa(
			final ListarContaDebTarifaSaidaDTO saidaListarContaDebTarifa) {
		this.saidaListarContaDebTarifa = saidaListarContaDebTarifa;
	}

	public List<SelectItem> getListaControleRadioGridListaContaDebTarifa() {
		return listaControleRadioGridListaContaDebTarifa;
	}

	public void setListaControleRadioGridListaContaDebTarifa(
			final List<SelectItem> listaControleRadioGridListaContaDebTarifa) {
		this.listaControleRadioGridListaContaDebTarifa = listaControleRadioGridListaContaDebTarifa;
	}

	public List<ListarContaDebTarifaOcorrencias> getListaContaDebTarifa() {
		return listaContaDebTarifa;
	}

	public void setListaContaDebTarifa(
			final List<ListarContaDebTarifaOcorrencias> listaContaDebTarifa) {
		this.listaContaDebTarifa = listaContaDebTarifa;
	}

	public Integer getItemSelecListaContaDebTarifa() {
		return itemSelecListaContaDebTarifa;
	}

	public void setItemSelecListaContaDebTarifa(final Integer itemSelecListaContaDebTarifa) {
		this.itemSelecListaContaDebTarifa = itemSelecListaContaDebTarifa;
	}

	public Long getCdCpfCnpjFiltro() {
		return cdCpfCnpjFiltro;
	}

	public void setCdCpfCnpjFiltro(final Long cdCpfCnpjFiltro) {
		this.cdCpfCnpjFiltro = cdCpfCnpjFiltro;
	}

	public Integer getCdFilialCnpjFiltro() {
		return cdFilialCnpjFiltro;
	}

	public void setCdFilialCnpjFiltro(final Integer cdFilialCnpjFiltro) {
		this.cdFilialCnpjFiltro = cdFilialCnpjFiltro;
	}

	public Integer getCdControleCnpjFiltro() {
		return cdControleCnpjFiltro;
	}

	public void setCdControleCnpjFiltro(final Integer cdControleCnpjFiltro) {
		this.cdControleCnpjFiltro = cdControleCnpjFiltro;
	}

	public Integer getBancoFiltro() {
		return bancoFiltro;
	}

	public void setBancoFiltro(final Integer bancoFiltro) {
		this.bancoFiltro = bancoFiltro;
	}

	public Integer getAgenciaFiltro() {
		return agenciaFiltro;
	}

	public void setAgenciaFiltro(final Integer agenciaFiltro) {
		this.agenciaFiltro = agenciaFiltro;
	}

	public Integer getAgenciaDigitoFiltro() {
		return agenciaDigitoFiltro;
	}

	public void setAgenciaDigitoFiltro(final Integer agenciaDigitoFiltro) {
		this.agenciaDigitoFiltro = agenciaDigitoFiltro;
	}

	public Long getContaFiltro() {
		return contaFiltro;
	}

	public void setContaFiltro(final Long contaFiltro) {
		this.contaFiltro = contaFiltro;
	}

	public List<ListarOperacaoContaDebTarifaOcorrencias> getListaGridExcluir() {
		return listaGridExcluir;
	}

	public void setListaGridExcluir(
			final List<ListarOperacaoContaDebTarifaOcorrencias> listaGridExcluir) {
		this.listaGridExcluir = listaGridExcluir;
	}

	public Integer getTipoOperacao() {
		return tipoOperacao;
	}

	public void setTipoOperacao(final Integer tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}

	public List<ListarOperacaoContaDebTarifaOcorrencias> getListaGridIncluir() {
		return listaGridIncluir;
	}

	public void setListaGridIncluir(
			final List<ListarOperacaoContaDebTarifaOcorrencias> listaGridIncluir) {
		this.listaGridIncluir = listaGridIncluir;
	}

	public ConsultarContaDebTarifaSaidaDTO getSaidaConsultarContaDebTarifa() {
		return saidaConsultarContaDebTarifa;
	}

	public void setSaidaConsultarContaDebTarifa(
			final ConsultarContaDebTarifaSaidaDTO saidaConsultarContaDebTarifa) {
		this.saidaConsultarContaDebTarifa = saidaConsultarContaDebTarifa;
	}

	public String getBancoFiltroInc() {
		return bancoFiltroInc;
	}

	public void setBancoFiltroInc(final String bancoFiltroInc) {
		this.bancoFiltroInc = bancoFiltroInc;
	}

	public String getAgenciaFiltroInc() {
		return agenciaFiltroInc;
	}

	public void setAgenciaFiltroInc(final String agenciaFiltroInc) {
		this.agenciaFiltroInc = agenciaFiltroInc;
	}

	public String getContaFiltroInc() {
		return contaFiltroInc;
	}

	public void setContaFiltroInc(final String contaFiltroInc) {
		this.contaFiltroInc = contaFiltroInc;
	}

	public String getTipoContaFiltroInc() {
		return tipoContaFiltroInc;
	}

	public void setTipoContaFiltroInc(final String tipoContaFiltroInc) {
		this.tipoContaFiltroInc = tipoContaFiltroInc;
	}

	public Long getCdPessoaJuridicaVinculo() {
		return cdPessoaJuridicaVinculo;
	}

	public void setCdPessoaJuridicaVinculo(final Long cdPessoaJuridicaVinculo) {
		this.cdPessoaJuridicaVinculo = cdPessoaJuridicaVinculo;
	}

	public Integer getCdTipoContratoVinculo() {
		return cdTipoContratoVinculo;
	}

	public void setCdTipoContratoVinculo(final Integer cdTipoContratoVinculo) {
		this.cdTipoContratoVinculo = cdTipoContratoVinculo;
	}

	public Long getNrSequenciaContratoVinculo() {
		return nrSequenciaContratoVinculo;
	}

	public void setNrSequenciaContratoVinculo(final Long nrSequenciaContratoVinculo) {
		this.nrSequenciaContratoVinculo = nrSequenciaContratoVinculo;
	}

	public Integer getCdTipoVinculoContrato() {
		return cdTipoVinculoContrato;
	}

	public void setCdTipoVinculoContrato(final Integer cdTipoVinculoContrato) {
		this.cdTipoVinculoContrato = cdTipoVinculoContrato;
	}

	public String getRadioArgumentoVincular() {
		return radioArgumentoVincular;
	}

	public void setRadioArgumentoVincular(final String radioArgumentoVincular) {
		this.radioArgumentoVincular = radioArgumentoVincular;
	}
}