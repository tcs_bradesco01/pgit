/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantermensagemcomprovantesalarial
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantermensagemcomprovantesalarial;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.IMsgCompSalarialService;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.AlterarMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.AlterarMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.DetalharMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.DetalharMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ExcluirMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ExcluirMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.IncluirMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.IncluirMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.ITipoComprovanteService;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ListarTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ListarTipoComprovanteSaidaDTO;

/**
 * Nome: MsgCompSalarialBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class MsgCompSalarialBean {

	//Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_MANTER_MENSAGEM_COMPROVANTE_SALARIAL. */
	private static final String CON_MANTER_MENSAGEM_COMPROVANTE_SALARIAL = "conManterMensagemComprovanteSalarial";
	
	/*Consulta e Detalhar*/
	/** Atributo codListaRadio. */
	private int codListaRadio;

	/** Atributo codigoTipoMensagemFiltro. */
	private String codigoTipoMensagemFiltro;

	/** Atributo tipoComprovanteFiltro. */
	private Integer tipoComprovanteFiltro;

	/** Atributo tipoMensagemFiltro. */
	private Integer tipoMensagemFiltro;

	/** Atributo indicadorRestricaoFiltro. */
	private String indicadorRestricaoFiltro;
	
	/** Atributo tipoComprovanteDesc. */
	private String tipoComprovanteDesc;
	
	/** Atributo descricao. */
	private String descricao;
	
	/** Atributo codigoTipoMensagem. */
	private String codigoTipoMensagem;
	
	/** Atributo tipoMensagemRadio. */
	private int tipoMensagemRadio;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo rdoNivelMensagemFiltro. */
	private Integer rdoNivelMensagemFiltro;
	
	/** Atributo rdoRestricaoFiltro. */
	private Integer rdoRestricaoFiltro;
	
	/*Incluir*/
	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;
	
	/** Atributo tipoMensagem. */
	private String tipoMensagem;
	
	/** Atributo tipoComprovante. */
	private Integer tipoComprovante;
	
	/** Atributo indicadorRestricao. */
	private Integer indicadorRestricao;
	
	/** Atributo retricaoDesc. */
	private String retricaoDesc;
	
	/*Listas*/
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo listaTipoComprovante. */
	private List<SelectItem> listaTipoComprovante = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoComprovanteHash. */
	private Map<Integer, String> listaTipoComprovanteHash = new HashMap<Integer, String>();

	/** Atributo listaIndicadorRestricao. */
	private List<SelectItem> listaIndicadorRestricao = new ArrayList<SelectItem>();
	
	/** Atributo listaGridPesquisa. */
	private List<ListarMsgComprovanteSalrlSaidaDTO> listaGridPesquisa;

	/*Trilha de Auditoria*/
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo msgCompSalarialImpl. */
	private IMsgCompSalarialService msgCompSalarialImpl;
	
	/** Atributo tipoComprovanteImpl. */
	private ITipoComprovanteService tipoComprovanteImpl;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	
	
	
	//controle de campos
	/** Atributo habilitaCliente. */
	private boolean habilitaCliente;

	/*Getters e Setters*/

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}



	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}



	/**
	 * Get: tipoComprovante.
	 *
	 * @return tipoComprovante
	 */
	public Integer getTipoComprovante() {
		return tipoComprovante;
	}

	/**
	 * Set: tipoComprovante.
	 *
	 * @param tipoComprovante the tipo comprovante
	 */
	public void setTipoComprovante(Integer tipoComprovante) {
		this.tipoComprovante = tipoComprovante;
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public String getTipoMensagem() {
		return tipoMensagem;
	}

	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(String tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: codigoTipoMensagem.
	 *
	 * @return codigoTipoMensagem
	 */
	public String getCodigoTipoMensagem() {
		return codigoTipoMensagem;
	}

	/**
	 * Set: codigoTipoMensagem.
	 *
	 * @param codigoTipoMensagem the codigo tipo mensagem
	 */
	public void setCodigoTipoMensagem(String codigoTipoMensagem) {
		this.codigoTipoMensagem = codigoTipoMensagem;
	}

	/**
	 * Get: descricao.
	 *
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Set: descricao.
	 *
	 * @param descricao the descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}



	/**
	 * Get: indicadorRestricao.
	 *
	 * @return indicadorRestricao
	 */
	public Integer getIndicadorRestricao() {
		return indicadorRestricao;
	}

	/**
	 * Set: indicadorRestricao.
	 *
	 * @param indicadorRestricao the indicador restricao
	 */
	public void setIndicadorRestricao(Integer indicadorRestricao) {
		this.indicadorRestricao = indicadorRestricao;
	}

	/**
	 * Get: tipoMensagemRadio.
	 *
	 * @return tipoMensagemRadio
	 */
	public int getTipoMensagemRadio() {
		return tipoMensagemRadio;
	}

	/**
	 * Set: tipoMensagemRadio.
	 *
	 * @param tipoMensagemRadio the tipo mensagem radio
	 */
	public void setTipoMensagemRadio(int tipoMensagemRadio) {
		this.tipoMensagemRadio = tipoMensagemRadio;
	}




	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: tipoComprovanteDesc.
	 *
	 * @return tipoComprovanteDesc
	 */
	public String getTipoComprovanteDesc() {
		return tipoComprovanteDesc;
	}

	/**
	 * Set: tipoComprovanteDesc.
	 *
	 * @param tipoComprovanteFiltroDesc the tipo comprovante desc
	 */
	public void setTipoComprovanteDesc(String tipoComprovanteFiltroDesc) {
		this.tipoComprovanteDesc = tipoComprovanteFiltroDesc;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: codigoTipoMensagemFiltro.
	 *
	 * @return codigoTipoMensagemFiltro
	 */
	public String getCodigoTipoMensagemFiltro() {
		return codigoTipoMensagemFiltro;
	}

	/**
	 * Set: codigoTipoMensagemFiltro.
	 *
	 * @param codigoTipoMensagemFiltro the codigo tipo mensagem filtro
	 */
	public void setCodigoTipoMensagemFiltro(String codigoTipoMensagemFiltro) {
		this.codigoTipoMensagemFiltro = codigoTipoMensagemFiltro;
	}

	/**
	 * Get: codListaRadio.
	 *
	 * @return codListaRadio
	 */
	public int getCodListaRadio() {
		return codListaRadio;
	}

	/**
	 * Set: codListaRadio.
	 *
	 * @param codListaRadio the cod lista radio
	 */
	public void setCodListaRadio(int codListaRadio) {
		this.codListaRadio = codListaRadio;
	}



	/**
	 * Get: indicadorRestricaoFiltro.
	 *
	 * @return indicadorRestricaoFiltro
	 */
	public String getIndicadorRestricaoFiltro() {
		return indicadorRestricaoFiltro;
	}

	/**
	 * Set: indicadorRestricaoFiltro.
	 *
	 * @param indicadorRestricaoFiltro the indicador restricao filtro
	 */
	public void setIndicadorRestricaoFiltro(String indicadorRestricaoFiltro) {
		this.indicadorRestricaoFiltro = indicadorRestricaoFiltro;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<ListarMsgComprovanteSalrlSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(List<ListarMsgComprovanteSalrlSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: listaIndicadorRestricao.
	 *
	 * @return listaIndicadorRestricao
	 */
	public List<SelectItem> getListaIndicadorRestricao() {
		return listaIndicadorRestricao;
	}

	/**
	 * Set: listaIndicadorRestricao.
	 *
	 * @param listaIndicadorRestricao the lista indicador restricao
	 */
	public void setListaIndicadorRestricao(
			List<SelectItem> listaIndicadorRestricao) {
		this.listaIndicadorRestricao = listaIndicadorRestricao;
	}

	/**
	 * Get: listaTipoComprovante.
	 *
	 * @return listaTipoComprovante
	 */
	public List<SelectItem> getListaTipoComprovante() {
		return listaTipoComprovante;
	}

	/**
	 * Set: listaTipoComprovante.
	 *
	 * @param listaTipoComprovante the lista tipo comprovante
	 */
	public void setListaTipoComprovante(List<SelectItem> listaTipoComprovante) {
		this.listaTipoComprovante = listaTipoComprovante;
	}

	/**
	 * Get: tipoComprovanteFiltro.
	 *
	 * @return tipoComprovanteFiltro
	 */
	public Integer getTipoComprovanteFiltro() {
		return tipoComprovanteFiltro;
	}

	/**
	 * Set: tipoComprovanteFiltro.
	 *
	 * @param tipoComprovanteFiltro the tipo comprovante filtro
	 */
	public void setTipoComprovanteFiltro(Integer tipoComprovanteFiltro) {
		this.tipoComprovanteFiltro = tipoComprovanteFiltro;
	}

	
	/*M�todos*/
	
	/**
	 * Inicia tela.
	 *
	 * @param evt the evt
	 */
	public void iniciaTela(ActionEvent evt) {
		limparDados();
		limparDadosIncluir();
		listarTipoComprovante();
		this.setIndicadorRestricao(0);
		setBtoAcionado(false);

	}

	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {
		this.setCodigoTipoMensagemFiltro("");
		this.setTipoComprovanteFiltro(0);
		this.setTipoMensagemFiltro(0);
		this.setIndicadorRestricaoFiltro("");
		setListaGridPesquisa(null);		
		setBtoAcionado(false);
		this.setRdoNivelMensagemFiltro(null);
		this.setRdoRestricaoFiltro(null);
		return "";
	}
	
	/**
	 * Limpar dados incluir.
	 *
	 * @return the string
	 */
	public String limparDadosIncluir(){
		this.setTipoMensagem(null);
		this.setIndicadorRestricao(null);
		this.setDescricao("");
		this.setTipoComprovante(null);
		this.setCodigoTipoMensagem(null);
		this.setHabilitaCliente(true);
		
		return "";
		
	}
	
	/**
	 * Get: retricaoDesc.
	 *
	 * @return retricaoDesc
	 */
	public String getRetricaoDesc() {
		return retricaoDesc;
	}

	/**
	 * Set: retricaoDesc.
	 *
	 * @param retricaoDesc the retricao desc
	 */
	public void setRetricaoDesc(String retricaoDesc) {
		this.retricaoDesc = retricaoDesc;
	}
	
    	/**
	     * Limpa radio restricao.
	     */
	    public void limpaRadioRestricao(){
    	    setRdoRestricaoFiltro(null);
    	    limparCampo();
    	}

	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista() {
		
		
			
			try{
				
				ListarMsgComprovanteSalrlEntradaDTO entradaDTO = new ListarMsgComprovanteSalrlEntradaDTO();
			
				entradaDTO.setCdControleMensagemSalarial(getCodigoTipoMensagemFiltro()!=null && !getCodigoTipoMensagemFiltro().equals("")?Integer.parseInt(getCodigoTipoMensagemFiltro()):0);				
				entradaDTO.setCdTipoComprovanteSalarial(getTipoComprovanteFiltro() != null?getTipoComprovanteFiltro():0);
				entradaDTO.setCdTipoMensagemSalarial(getRdoNivelMensagemFiltro());
				entradaDTO.setCdRestMensagemSalarial(getRdoRestricaoFiltro());
				
				setListaGridPesquisa(getMsgCompSalarialImpl().listarMsgComprovanteSalrl(entradaDTO));
				setBtoAcionado(true);
				this.listaControleRadio =  new ArrayList<SelectItem>();
				for (int i = 0; i <= getListaGridPesquisa().size();i++) {
					this.listaControleRadio.add(new SelectItem(i," "));
				}
				
				setItemSelecionadoLista(null);
				
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_MENSAGEM_COMPROVANTE_SALARIAL, BradescoViewExceptionActionType.ACTION, false);
				setListaGridPesquisa(null);

			}
			
			return CON_MANTER_MENSAGEM_COMPROVANTE_SALARIAL;
				
			
		} 
		
	/**
	 * Carrega lista retorno.
	 *
	 * @return the string
	 */
	public String carregaListaRetorno() {
		
		if(getObrigatoriedade().equals("T")){
			
			try{
				
				ListarMsgComprovanteSalrlEntradaDTO entradaDTO = new ListarMsgComprovanteSalrlEntradaDTO();
			
				entradaDTO.setCdControleMensagemSalarial(getCodigoTipoMensagemFiltro()!=null && !getCodigoTipoMensagemFiltro().equals("")?Integer.parseInt(getCodigoTipoMensagemFiltro()):0);
				//entradaDTO.setCdRestMensagemComprovante(getIndicadorRestricaoFiltro()!=null && !getIndicadorRestricaoFiltro().equals("")?Integer.parseInt(getIndicadorRestricaoFiltro()):0);
				//entradaDTO.setCdTipoMensagemSalarial(getTipoMensagemFiltro()!=null?getTipoMensagemFiltro():0);
				entradaDTO.setCdTipoComprovanteSalarial(getTipoComprovanteFiltro() != null?getTipoComprovanteFiltro():0);

				setListaGridPesquisa(getMsgCompSalarialImpl().listarMsgComprovanteSalrl(entradaDTO));
				setBtoAcionado(true);
				this.listaControleRadio =  new ArrayList<SelectItem>();
				for (int i = 0; i <= getListaGridPesquisa().size();i++) {
					this.listaControleRadio.add(new SelectItem(i," "));
				}
				
				setItemSelecionadoLista(null);
				
			} catch (PdcAdapterFunctionalException p) {
			
			}
			
			return CON_MANTER_MENSAGEM_COMPROVANTE_SALARIAL;
			}
		else{
			return"";
		}
	} 
		
	
		
	

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		return "ok";
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {

		limparDadosIncluir();
		
		listarTipoComprovante();
		
		return "INCLUIR";
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_MENSAGEM_COMPROVANTE_SALARIAL, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 

		
		return "EXCLUIR";
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {

		try {
			preencheDados();
				
			setHabilitaCliente(!getTipoMensagem().equals("2"));
			
			listarTipoComprovante();
				
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_MENSAGEM_COMPROVANTE_SALARIAL, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 

		return "ALTERAR";
	}

	/**
	 * Get: tipoMensagemFiltro.
	 *
	 * @return tipoMensagemFiltro
	 */
	public Integer getTipoMensagemFiltro() {
		return tipoMensagemFiltro;
	}

	/**
	 * Set: tipoMensagemFiltro.
	 *
	 * @param tipoMensagemFiltro the tipo mensagem filtro
	 */
	public void setTipoMensagemFiltro(Integer tipoMensagemFiltro) {
		this.tipoMensagemFiltro = tipoMensagemFiltro;
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		
		if(getObrigatoriedade()!= null && getObrigatoriedade().equals("T") ){
			
			setTipoComprovanteDesc(getTipoComprovante()!=null && getTipoComprovante() != 0 ?(String)listaTipoComprovanteHash.get(getTipoComprovante()):"");
			setIndicadorRestricao(getTipoComprovante()!=null && getTipoComprovante() != 0? getIndicadorRestricao():0);
			
			return "AVANCAR_INCLUIR";
		}
		return "";
	
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {
		
	if(getObrigatoriedade()!= null && getObrigatoriedade().equals("T") ){
			
			setTipoComprovanteDesc(getTipoComprovante()!=null && getTipoComprovante() != 0 ?(String)listaTipoComprovanteHash.get(getTipoComprovante()):"");
			setIndicadorRestricao(getTipoComprovante()!=null && getTipoComprovante() != 0? getIndicadorRestricao():0);
			
			return "AVANCAR_ALTERAR";
		}
		return "";
		
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_MENSAGEM_COMPROVANTE_SALARIAL, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		
		return "DETALHAR";
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {
		
		try {
			IncluirMsgComprovanteSalrlEntradaDTO incluirMsgComprovanteSalrlEntradaDTO = new IncluirMsgComprovanteSalrlEntradaDTO();
			
			incluirMsgComprovanteSalrlEntradaDTO.setCodigoTipoMensagem(Integer.parseInt(getCodigoTipoMensagem()));
			incluirMsgComprovanteSalrlEntradaDTO.setTipoComprovante(getTipoComprovante()!=null?getTipoComprovante():0);
			incluirMsgComprovanteSalrlEntradaDTO.setNivelMensagem(Integer.parseInt(getTipoMensagem()));
			incluirMsgComprovanteSalrlEntradaDTO.setDescricao(getDescricao());
			incluirMsgComprovanteSalrlEntradaDTO.setRestricao(getIndicadorRestricao()==null?0:getIndicadorRestricao());
			
			IncluirMsgComprovanteSalrlSaidaDTO incluirMsgComprovanteSalrlSaidaDTO = getMsgCompSalarialImpl().incluirMsgComprovanteSalrl(incluirMsgComprovanteSalrlEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +incluirMsgComprovanteSalrlSaidaDTO.getCodMensagem() + ") " + incluirMsgComprovanteSalrlSaidaDTO.getMensagem(), CON_MANTER_MENSAGEM_COMPROVANTE_SALARIAL, BradescoViewExceptionActionType.ACTION, false);
			carregaListaRetorno();
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		
		
		return "ok";
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {
		
		try {
			
			ExcluirMsgComprovanteSalrlEntradaDTO excluirMsgComprovanteSalrlEntradaDTO = new ExcluirMsgComprovanteSalrlEntradaDTO();
			
			ListarMsgComprovanteSalrlSaidaDTO listarMsgComprovanteSalrlSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());	
			
			excluirMsgComprovanteSalrlEntradaDTO.setCodigoTipoMensagem(listarMsgComprovanteSalrlSaidaDTO.getCodigo());
			
			ExcluirMsgComprovanteSalrlSaidaDTO excluirMsgComprovanteSalrlSaidaDTO = getMsgCompSalarialImpl().excluirFuncoesAlcada(excluirMsgComprovanteSalrlEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + excluirMsgComprovanteSalrlSaidaDTO.getCodMensagem() + ") " + excluirMsgComprovanteSalrlSaidaDTO.getMensagem(), CON_MANTER_MENSAGEM_COMPROVANTE_SALARIAL, BradescoViewExceptionActionType.ACTION, false);

			carregaLista();
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";

	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {
		
		
		try {
			
			
			
			AlterarMsgComprovanteSalrlEntradaDTO alterarMsgComprovanteSalrlEntradaDTO = new AlterarMsgComprovanteSalrlEntradaDTO();
			
			
			ListarMsgComprovanteSalrlSaidaDTO listarMsgComprovanteSalrlSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());
			
			alterarMsgComprovanteSalrlEntradaDTO.setCodigoTipoMensagem(listarMsgComprovanteSalrlSaidaDTO.getCodigo());
			alterarMsgComprovanteSalrlEntradaDTO.setTipoComprovante(getTipoComprovante()!=null?getTipoComprovante():0);
			alterarMsgComprovanteSalrlEntradaDTO.setNivelMensagem(Integer.parseInt(getTipoMensagem()));
			alterarMsgComprovanteSalrlEntradaDTO.setDescricao(getDescricao());
			alterarMsgComprovanteSalrlEntradaDTO.setRestricao(getIndicadorRestricao()!=null?getIndicadorRestricao():0);
			
			AlterarMsgComprovanteSalrlSaidaDTO alterarMsgComprovanteSalrlSaidaDTO = getMsgCompSalarialImpl().alterarMsgComprovanteSalrl(alterarMsgComprovanteSalrlEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + alterarMsgComprovanteSalrlSaidaDTO.getCodMensagem() + ") " + alterarMsgComprovanteSalrlSaidaDTO.getMensagem(), CON_MANTER_MENSAGEM_COMPROVANTE_SALARIAL, BradescoViewExceptionActionType.ACTION, false);

			carregaLista();
			
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";

	}
	
	/**
	 * Voltar detalhar.
	 *
	 * @return the string
	 */
	public String voltarDetalhar() {
		setItemSelecionadoLista(null);
		return "VOLTAR_DETALHAR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		setItemSelecionadoLista(null);
		return "VOLTAR_INCLUIR";
	}
	
	/**
	 * Voltar incluir2.
	 *
	 * @return the string
	 */
	public String voltarIncluir2() {
		return "VOLTAR_INCLUIR2";
	}
	
	/**
	 * Voltar alterar.
	 *
	 * @return the string
	 */
	public String voltarAlterar() {
		setItemSelecionadoLista(null);
		return "VOLTAR_ALTERAR";
	}
	
	/**
	 * Voltar alterar2.
	 *
	 * @return the string
	 */
	public String voltarAlterar2() {
		return "VOLTAR_ALTERAR2";
	}
	
	/**
	 * Voltar excluir.
	 *
	 * @return the string
	 */
	public String voltarExcluir() {
		setItemSelecionadoLista(null);
		return "VOLTAR_EXCLUIR";
	}
	

	/**
	 * Get: msgCompSalarialImpl.
	 *
	 * @return msgCompSalarialImpl
	 */
	public IMsgCompSalarialService getMsgCompSalarialImpl() {
		return msgCompSalarialImpl;
	}

	/**
	 * Set: msgCompSalarialImpl.
	 *
	 * @param msgCompSalarialImpl the msg comp salarial impl
	 */
	public void setMsgCompSalarialImpl(IMsgCompSalarialService msgCompSalarialImpl) {
		this.msgCompSalarialImpl = msgCompSalarialImpl;
	}
	
	/**
	 * Listar tipo comprovante.
	 */
	private void listarTipoComprovante() {
		try{
			this.listaTipoComprovante = new ArrayList<SelectItem>();
			
			List<ListarTipoComprovanteSaidaDTO> listarTipoComprovanteSaidaDTO = new ArrayList<ListarTipoComprovanteSaidaDTO>();
			ListarTipoComprovanteEntradaDTO listarTipoComprovanteEntradaDTO = new ListarTipoComprovanteEntradaDTO();
			listarTipoComprovanteEntradaDTO.setCdTipoComprovante(0);
			
			listarTipoComprovanteSaidaDTO = tipoComprovanteImpl.listarTipoComprovante(listarTipoComprovanteEntradaDTO);
			
			listaTipoComprovanteHash.clear();
			for(ListarTipoComprovanteSaidaDTO combo : listarTipoComprovanteSaidaDTO){
				listaTipoComprovanteHash.put(combo.getCdComprovanteSalarial(),combo.getDsComprovanteSalarial());
				this.listaTipoComprovante.add(new SelectItem(combo.getCdComprovanteSalarial(),combo.getDsComprovanteSalarial()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoComprovante = new ArrayList<SelectItem>();
		}
	}

	
	
	/**
	 * Get: listaTipoComprovanteHash.
	 *
	 * @return listaTipoComprovanteHash
	 */
	public Map<Integer, String> getListaTipoComprovanteHash() {
		return listaTipoComprovanteHash;
	}

	/**
	 * Set lista tipo comprovante hash.
	 *
	 * @param listaTipoComprovanteHash the lista tipo comprovante hash
	 */
	public void setListaTipoComprovanteHash(
			Map<Integer, String> listaTipoComprovanteHash) {
		this.listaTipoComprovanteHash = listaTipoComprovanteHash;
	}
	
	/**
	 * Limpar campo.
	 *
	 * @return the string
	 */
	public String limparCampo(){
        	setTipoComprovanteFiltro(0);
        	return "";
	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		
		ListarMsgComprovanteSalrlSaidaDTO listarMsgComprovanteSalrlSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());
		
		DetalharMsgComprovanteSalrlEntradaDTO detalharMsgComprovanteSalrlEntradaDTO = new DetalharMsgComprovanteSalrlEntradaDTO();
		detalharMsgComprovanteSalrlEntradaDTO.setCodigoTipoMensagem(listarMsgComprovanteSalrlSaidaDTO.getCodigo());
		
		DetalharMsgComprovanteSalrlSaidaDTO detalharMsgComprovanteSalrlSaidaDTO = getMsgCompSalarialImpl().detalharMsgComprovanteSalrl(detalharMsgComprovanteSalrlEntradaDTO);

		
		
		
		
		setDescricao(detalharMsgComprovanteSalrlSaidaDTO.getDescricao() );
		setCodigoTipoMensagem(String.valueOf(detalharMsgComprovanteSalrlSaidaDTO.getCodigoTipoMensagem()));
		setTipoMensagem(String.valueOf(detalharMsgComprovanteSalrlSaidaDTO.getCdNivelMensagem()));
		
		if (detalharMsgComprovanteSalrlSaidaDTO.getCdNivelMensagem() == 2){
			setTipoComprovante(detalharMsgComprovanteSalrlSaidaDTO.getCdTipoComprovante());
			setTipoComprovanteDesc(detalharMsgComprovanteSalrlSaidaDTO.getCdTipoComprovante() + "-" + detalharMsgComprovanteSalrlSaidaDTO.getDsTipoComprovante());
			setIndicadorRestricao(detalharMsgComprovanteSalrlSaidaDTO.getCdRestricao());
		}else{
			setTipoComprovante(0);
			setTipoComprovanteDesc("");
			setIndicadorRestricao(0);
		}
		
		
		setUsuarioInclusao(detalharMsgComprovanteSalrlSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharMsgComprovanteSalrlSaidaDTO.getUsuarioManutencao());
		
		setComplementoInclusao(detalharMsgComprovanteSalrlSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharMsgComprovanteSalrlSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharMsgComprovanteSalrlSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharMsgComprovanteSalrlSaidaDTO.getComplementoManutencao());
		
		setTipoCanalInclusao(detalharMsgComprovanteSalrlSaidaDTO.getCdCanalInclusao()==0? "" : detalharMsgComprovanteSalrlSaidaDTO.getCdCanalInclusao() + " - " +  detalharMsgComprovanteSalrlSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(detalharMsgComprovanteSalrlSaidaDTO.getCdCanalManutencao()==0? "" : detalharMsgComprovanteSalrlSaidaDTO.getCdCanalManutencao() + " - " + detalharMsgComprovanteSalrlSaidaDTO.getDsCanalManutencao()); 
		
		setDataHoraInclusao(detalharMsgComprovanteSalrlSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharMsgComprovanteSalrlSaidaDTO.getDataHoraManutencao());
		
	}
	
	/**
	 * Habilita campos.
	 */
	public void habilitaCampos(){
		
		setTipoComprovante(0);
		setIndicadorRestricao(null);
		
		// Organiza��o selecionado.	
		if (getTipoMensagem() != null &&  getTipoMensagem().equals("1")){
			setHabilitaCliente(true);
			
		}
		//Cliente selecionado.
		if (getTipoMensagem() != null &&  getTipoMensagem().equals("2")){
			setHabilitaCliente(false);
			
		}	
	}

	/**
	 * Is desabilita campos.
	 *
	 * @return true, if is desabilita campos
	 */
	public boolean isDesabilitaCampos() {
		if ("".equals(codigoTipoMensagemFiltro) || codigoTipoMensagemFiltro == null) {
			return false;
		} else {
			setRdoNivelMensagemFiltro(null);
			setTipoComprovanteFiltro(null);
			setRdoRestricaoFiltro(null);
			return true;
		}
	}

	/**
	 * Is habilita cliente.
	 *
	 * @return true, if is habilita cliente
	 */
	public boolean isHabilitaCliente() {
		return habilitaCliente;
	}

	/**
	 * Set: habilitaCliente.
	 *
	 * @param habilitaCliente the habilita cliente
	 */
	public void setHabilitaCliente(boolean habilitaCliente) {
		this.habilitaCliente = habilitaCliente;
	}

	/**
	 * Get: tipoComprovanteImpl.
	 *
	 * @return tipoComprovanteImpl
	 */
	public ITipoComprovanteService getTipoComprovanteImpl() {
		return tipoComprovanteImpl;
	}

	/**
	 * Set: tipoComprovanteImpl.
	 *
	 * @param tipoComprovanteImpl the tipo comprovante impl
	 */
	public void setTipoComprovanteImpl(ITipoComprovanteService tipoComprovanteImpl) {
		this.tipoComprovanteImpl = tipoComprovanteImpl;
	}

	/**
	 * Limpar tipo comprovante restricao.
	 */
	public void limparTipoComprovanteRestricao(){
		
			setTipoComprovanteFiltro(0);
			setIndicadorRestricaoFiltro(null);
		
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: rdoNivelMensagemFiltro.
	 *
	 * @return rdoNivelMensagemFiltro
	 */
	public Integer getRdoNivelMensagemFiltro() {
	    return rdoNivelMensagemFiltro;
	}

	/**
	 * Set: rdoNivelMensagemFiltro.
	 *
	 * @param rdoNivelMensagemFiltro the rdo nivel mensagem filtro
	 */
	public void setRdoNivelMensagemFiltro(Integer rdoNivelMensagemFiltro) {
	    this.rdoNivelMensagemFiltro = rdoNivelMensagemFiltro;
	}

	/**
	 * Get: rdoRestricaoFiltro.
	 *
	 * @return rdoRestricaoFiltro
	 */
	public Integer getRdoRestricaoFiltro() {
	    return rdoRestricaoFiltro;
	}

	/**
	 * Set: rdoRestricaoFiltro.
	 *
	 * @param rdoRestricaoFiltro the rdo restricao filtro
	 */
	public void setRdoRestricaoFiltro(Integer rdoRestricaoFiltro) {
	    this.rdoRestricaoFiltro = rdoRestricaoFiltro;
	}
	
}
