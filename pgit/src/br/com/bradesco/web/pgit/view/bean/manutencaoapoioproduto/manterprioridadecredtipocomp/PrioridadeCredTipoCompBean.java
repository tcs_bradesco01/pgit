/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterprioridadecredtipocomp
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterprioridadecredtipocomp;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.IPrioridadeCredTipoCompService;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.AlterarPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.AlterarPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.DetalharPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.DetalharPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ExcluirPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ExcluirPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.IncluirPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.IncluirPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ListarPrioridadeCredTipoCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.prioridadecredtipocomp.bean.ListarPrioridadeCredTipoCompSaidaDTO;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: PrioridadeCredTipoCompBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class PrioridadeCredTipoCompBean {
	
	//Constante utilizada  para evitar redund�ncia apontada pelo Sonar 	
	/** Atributo CON_MANTER_PRIORIDADE_CRED_TIPO_COMP. */
	private static final String CON_MANTER_PRIORIDADE_CRED_TIPO_COMP = "conManterPrioridadeCredTipoComp";
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/*'*/
	/** Atributo tipoCompromissoFiltro. */
	private String tipoCompromissoFiltro;
	
	/** Atributo prioridadeFiltro. */
	private String prioridadeFiltro;
	
	/** Atributo dtInicioVig. */
	private Date dtInicioVig;
	//private Date dataVigenciaFiltro;
	/** Atributo diaDataVigencia. */
	private String diaDataVigencia;
	
	/** Atributo mesDataVigencia. */
	private String mesDataVigencia;
	
	/** Atributo anoDataVigencia. */
	private String anoDataVigencia;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo listaGridPesquisa. */
	private List<ListarPrioridadeCredTipoCompSaidaDTO> listaGridPesquisa;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo bloqueaRadio. */
	private boolean bloqueaRadio;
	
	/*Detalhar, Incluir, Excluir, Alterar*/
	/** Atributo tipoCompromisso. */
	private String tipoCompromisso;
	
	/** Atributo prioridade. */
	private String prioridade;
	
	/** Atributo dataInicioVigencia. */
	private Date dataInicioVigencia;
	
	/** Atributo dataFimVigencia. */
	private Date dataFimVigencia;	
	
	/** Atributo dataInicioVigenciaDesc. */
	private String  dataInicioVigenciaDesc;
	
	/** Atributo dataFinalVigenciaDesc. */
	private String  dataFinalVigenciaDesc;
	
	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo centroCustoLupaFiltro. */
	private String centroCustoLupaFiltro;
	
	/** Atributo listaCentroCustoLupa. */
	private List<CentroCustoSaidaDTO> listaCentroCustoLupa = new ArrayList<CentroCustoSaidaDTO>();
	
	/** Atributo listaCentroCustoLupaHash. */
	private Map<String,String> listaCentroCustoLupaHash = new HashMap<String, String>();
	
	/** Atributo listaCentroCustoLupaSelectItem. */
	private List<SelectItem> listaCentroCustoLupaSelectItem =  new ArrayList<SelectItem>();
	
	
	/** Atributo cdSistema. */
	private String cdSistema;

	/*Trilha de Auditoria*/	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;

	/** Atributo manterPrioridadeCredTipoComp. */
	private IPrioridadeCredTipoCompService manterPrioridadeCredTipoComp;
	
	/** Atributo inicialCentroCustoFiltro. */
	private String inicialCentroCustoFiltro;
	
	/** Atributo centroCustoFiltro. */
	private String centroCustoFiltro;
	
	/** Atributo listaCentroCustoFiltro. */
	private List<SelectItem> listaCentroCustoFiltro = new ArrayList<SelectItem>();
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
	
		setListaGridPesquisa(null);
		setTipoCompromissoFiltro("");
		setPrioridadeFiltro("");
		setDiaDataVigencia("");
		setMesDataVigencia("");
		setAnoDataVigencia("");
		setItemSelecionadoLista(null);
		setObrigatoriedade("");
		setBtoAcionado(false);
		
	}	
	
	/**
	 * Obter centro custo.
	 */
	public void obterCentroCusto() {
		try{
			setListaCentroCustoLupa(new ArrayList<CentroCustoSaidaDTO>());
			setListaCentroCustoLupaSelectItem(new ArrayList<SelectItem>());
			listaCentroCustoLupaHash.clear();
			if (getCentroCustoLupaFiltro() != null && !getCentroCustoLupaFiltro().trim().equals("")) {
				setListaCentroCustoLupa(getComboService().listarCentroCusto(new CentroCustoEntradaDTO(getCentroCustoLupaFiltro().toUpperCase())));
			}
			
			
			for (int i = 0; i < getListaCentroCustoLupa().size(); i++) {
				listaCentroCustoLupaSelectItem.add(new SelectItem(listaCentroCustoLupa.get(i).getCdCentroCusto().toUpperCase(), listaCentroCustoLupa.get(i).getCdCentroCusto().toUpperCase() + " - " + listaCentroCustoLupa.get(i).getDsCentroCusto().toUpperCase()));
				listaCentroCustoLupaHash.put(listaCentroCustoLupa.get(i).getCdCentroCusto().toUpperCase(), listaCentroCustoLupa.get(i).getCdCentroCusto().toUpperCase() + " - " + listaCentroCustoLupa.get(i).getDsCentroCusto().toUpperCase());
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);			
		}
			
		
	}
	
	/**
	 * Navegar tela inicial.
	 *
	 * @return the string
	 */
	public String navegarTelaInicial(){
		//this.setDataInicioVigencia(new Date());
		setBtoAcionado(false);

		limpar();

		return CON_MANTER_PRIORIDADE_CRED_TIPO_COMP;
	}
	
	/**
	 * Limpar.
	 */
	public void limpar(){
		
		this.setTipoCompromisso("");
		this.setPrioridade("");
		setDtInicioVig(null);
		//this.setDataInicioVigencia(new Date());
		setBtoAcionado(false);
	}
	
	/**
	 * Buscar centro custo.
	 */
	public void buscarCentroCusto(){
		
		if (getInicialCentroCustoFiltro()!=null && !getInicialCentroCustoFiltro().equals("")){
			
			this.listaCentroCustoFiltro = new ArrayList<SelectItem>();
			
			List<CentroCustoSaidaDTO> listaCentroCustoSaida = new ArrayList<CentroCustoSaidaDTO>();
			CentroCustoEntradaDTO centroCustoEntradaDTO = new CentroCustoEntradaDTO();
			
			centroCustoEntradaDTO.setCdSistema(getInicialCentroCustoFiltro());
			
			listaCentroCustoSaida = comboService.listarCentroCusto(centroCustoEntradaDTO);
			
			
			for(CentroCustoSaidaDTO combo : listaCentroCustoSaida){			
				this.listaCentroCustoFiltro.add(new SelectItem(combo.getCdCentroCusto(),combo.getCdCentroCusto() + "-" + combo.getDsCentroCusto()));
			}
		}else{			
			this.listaCentroCustoFiltro = new ArrayList<SelectItem>();
		}		
			
	}
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados(){
		setDiaDataVigencia("");
		setMesDataVigencia("");
		setAnoDataVigencia("");
		this.setPrioridadeFiltro("");
		this.setTipoCompromissoFiltro("");
		setListaGridPesquisa(null);
		setDtInicioVig(null);
		setItemSelecionadoLista(null);
		//this.setDataInicioVigencia(new Date());
		setDataInicioVigencia(null);
		setBtoAcionado(false);
		setCentroCusto("");
		setCentroCustoFiltro("");
		setInicialCentroCustoFiltro("");
		setListaCentroCustoFiltro(new ArrayList<SelectItem>());
		
		return "ok";
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		return "ok";
	}
	
	/**
	 * Carrega lista.
	 */
	public void carregaLista() {		
		try{
			listaGridPesquisa = new ArrayList<ListarPrioridadeCredTipoCompSaidaDTO>();

			ListarPrioridadeCredTipoCompEntradaDTO entradaDTO = new ListarPrioridadeCredTipoCompEntradaDTO();

			entradaDTO.setTipoCompromisso(getTipoCompromissoFiltro()!=null && !getTipoCompromissoFiltro().equals("")? Integer.parseInt(getTipoCompromissoFiltro()):0);	
			entradaDTO.setPrioridade(getPrioridadeFiltro()!=null && !getPrioridadeFiltro().equals("")? Integer.parseInt(getPrioridadeFiltro()):0);
			entradaDTO.setCdCustoPrioridadeProduto(getCentroCustoFiltro());
			entradaDTO.setDtInicioVig(getDtInicioVig());

			setListaGridPesquisa(getManterPrioridadeCredTipoComp().listarPrioridadeCredTipoComp(entradaDTO));
			setBtoAcionado(true);
			listaControleRadio= new ArrayList<SelectItem>();

			for(int i = 0; i < listaGridPesquisa.size(); i++){
				listaControleRadio.add(new SelectItem(i,""));
			}

			setItemSelecionadoLista(null);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setItemSelecionadoLista(null);
			setListaGridPesquisa(null);
		}
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		setPrioridade("");
		setTipoCompromisso("");
		setDataInicioVigencia(new Date());
		setDataFimVigencia(null);
		setListaCentroCustoLupa(new ArrayList<CentroCustoSaidaDTO>());
		setCentroCustoLupaFiltro("");
		setCdSistema("");
		setListaCentroCustoLupaSelectItem(new ArrayList<SelectItem>());
		return "INCLUIR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_PRIORIDADE_CRED_TIPO_COMP, BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "EXCLUIR";
	}
	
	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_PRIORIDADE_CRED_TIPO_COMP, BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		
		return "ALTERAR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR_INCLUIR";
	}
	
	/**
	 * Voltar alterar.
	 *
	 * @return the string
	 */
	public String voltarAlterar(){
		return "VOLTAR_ALTERAR";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		if (getObrigatoriedade().equals("T")){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");				
			setDataInicioVigenciaDesc(df.format(getDataInicioVigencia()));
			setDataFinalVigenciaDesc(getDataFimVigencia() != null ? df.format(getDataFimVigencia()) : "");
			setCentroCusto(listaCentroCustoLupaHash.get(getCdSistema()));
			return "AVANCAR_INCLUIR";
		}	
		
		return "";
	}
	
	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar(){
		if(getObrigatoriedade() != null && getObrigatoriedade().equals("T")){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");				
			setDataInicioVigenciaDesc(df.format(getDataInicioVigencia()));
			setDataFinalVigenciaDesc(getDataFimVigencia() != null ? df.format(getDataFimVigencia()) : "");
			return "AVANCAR_ALTERAR";
		}

		return "";
	}
	
	/**
	 * Preenche dados.
	 */
	public void preencheDados(){
		
			
		
		ListarPrioridadeCredTipoCompSaidaDTO listarPrioridadeCredTipoCompSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());
		
		DetalharPrioridadeCredTipoCompEntradaDTO  detalharPrioridadeCredTipoCompEntradaDTO = new DetalharPrioridadeCredTipoCompEntradaDTO();

		detalharPrioridadeCredTipoCompEntradaDTO.setTipoCompromisso(listarPrioridadeCredTipoCompSaidaDTO.getTipoCompromisso());
		
		DetalharPrioridadeCredTipoCompSaidaDTO detalharPrioridadeCredTipoCompSaidaDTO = getManterPrioridadeCredTipoComp().detalharPrioridadeCredTipoComp(detalharPrioridadeCredTipoCompEntradaDTO);
		
		setTipoCompromisso(String.valueOf(detalharPrioridadeCredTipoCompSaidaDTO.getTipoComprimisso()));
		setPrioridade(String.valueOf(detalharPrioridadeCredTipoCompSaidaDTO.getPrioridade()));
		setCentroCusto(listarPrioridadeCredTipoCompSaidaDTO.getCentroCusto());
			

		
		if  (detalharPrioridadeCredTipoCompSaidaDTO.getDataFinalVigencia()!=null  &&  !detalharPrioridadeCredTipoCompSaidaDTO.getDataFinalVigencia().equals("")){
			
			Date dataFinal = FormatarData.formataDiaMesAnoFromPdc(detalharPrioridadeCredTipoCompSaidaDTO.getDataFinalVigencia());
			setDataFimVigencia(dataFinal);					
			setDataFinalVigenciaDesc(FormatarData.formataDiaMesAno(dataFinal));	
			
		}else{
			
			setDataFimVigencia(null);
			setDataFinalVigenciaDesc("");
			
		}
		
		if  (detalharPrioridadeCredTipoCompSaidaDTO.getDataInicioVigencia()!=null  &&  !detalharPrioridadeCredTipoCompSaidaDTO.getDataInicioVigencia().equals("")){
			Date dataInicio = FormatarData.formataDiaMesAnoFromPdc(detalharPrioridadeCredTipoCompSaidaDTO.getDataInicioVigencia());			 
 			setDataInicioVigencia(dataInicio);								
			setDataInicioVigenciaDesc(FormatarData.formataDiaMesAno(dataInicio));				
			
		}else{
			setDataInicioVigencia(null);								
			setDataInicioVigenciaDesc("");	
		}
		
		
		
		/*Trilha de Auditoria*/
		setDataHoraManutencao(detalharPrioridadeCredTipoCompSaidaDTO.getDataHoraManutencao());
		setDataHoraInclusao(detalharPrioridadeCredTipoCompSaidaDTO.getDataHoraInclusao());
		
		setTipoCanalInclusao(detalharPrioridadeCredTipoCompSaidaDTO.getCdCanalInclusao() + " - " + detalharPrioridadeCredTipoCompSaidaDTO.getDsCanalInclusao());
		setTipoCanalManutencao(detalharPrioridadeCredTipoCompSaidaDTO.getCdCanalManutencao()==0? "" : detalharPrioridadeCredTipoCompSaidaDTO.getCdCanalManutencao() + " - " + detalharPrioridadeCredTipoCompSaidaDTO.getDsCanalManutencao()); 
		
		
		setUsuarioInclusao(detalharPrioridadeCredTipoCompSaidaDTO.getAutenticacaoSegurancaInclusao());
		setUsuarioManutencao(detalharPrioridadeCredTipoCompSaidaDTO.getAutenticacaoSegurancaManutencao());
		
		setComplementoInclusao(detalharPrioridadeCredTipoCompSaidaDTO.getNumOperacaoFluxoInclusao().equals("0")? "" : detalharPrioridadeCredTipoCompSaidaDTO.getNumOperacaoFluxoInclusao());
		setComplementoManutencao(detalharPrioridadeCredTipoCompSaidaDTO.getNumOperacaoFluxoManutencao().equals("0")? "" : detalharPrioridadeCredTipoCompSaidaDTO.getNumOperacaoFluxoManutencao());
		
		
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_PRIORIDADE_CRED_TIPO_COMP, BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		
		return "DETALHAR";
	}
	
	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		
		setItemSelecionadoLista(null);
		limpar();
		return "VOLTAR";
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar(){
		
		try {
			AlterarPrioridadeCredTipoCompEntradaDTO alterarPrioridadeCredTipoCompEntradaDTO = new AlterarPrioridadeCredTipoCompEntradaDTO();
			ListarPrioridadeCredTipoCompSaidaDTO listarSaidaDTO= getListaGridPesquisa().get(getItemSelecionadoLista());
			
			alterarPrioridadeCredTipoCompEntradaDTO.setTipoCompromisso(listarSaidaDTO.getTipoCompromisso());
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			alterarPrioridadeCredTipoCompEntradaDTO.setDataInicioVigencia(df.format(getDataInicioVigencia()));
			alterarPrioridadeCredTipoCompEntradaDTO.setDataFimVigencia(df.format(getDataFimVigencia()));
			alterarPrioridadeCredTipoCompEntradaDTO.setPrioridade(listarSaidaDTO.getPrioridade());
			alterarPrioridadeCredTipoCompEntradaDTO.setCentroCustoPrioridadeProduto(listarSaidaDTO.getCdCustoPrioridadeProduto());
			
			AlterarPrioridadeCredTipoCompSaidaDTO alterarPrioridadeCredTipoCompSaidaDTO = getManterPrioridadeCredTipoComp().alterarPrioridadeCredTipoCompSaidaDTO(alterarPrioridadeCredTipoCompEntradaDTO);
			
						
			BradescoFacesUtils.addInfoModalMessage("(" +alterarPrioridadeCredTipoCompSaidaDTO.getCodMensagem() + ") " +alterarPrioridadeCredTipoCompSaidaDTO.getMensagem(), CON_MANTER_PRIORIDADE_CRED_TIPO_COMP, BradescoViewExceptionActionType.ACTION, false);
			carregaLista();
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		
		
		try {
			IncluirPrioridadeCredTipoCompEntradaDTO incluirPrioridadeCredTipoCompEntradaDTO = new IncluirPrioridadeCredTipoCompEntradaDTO();
			
			incluirPrioridadeCredTipoCompEntradaDTO.setTipoCompromisso(Integer.parseInt(getTipoCompromisso()));
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			incluirPrioridadeCredTipoCompEntradaDTO.setDataInicioVigencia(getDataInicioVigencia() == null ? "" : df.format(getDataInicioVigencia()));
			incluirPrioridadeCredTipoCompEntradaDTO.setDataFimVigencia(getDataFimVigencia() == null ? "" : df.format(getDataFimVigencia()));
			incluirPrioridadeCredTipoCompEntradaDTO.setPrioridade(Integer.parseInt(getPrioridade()));
			incluirPrioridadeCredTipoCompEntradaDTO.setCentroCusto(getCdSistema());
					
			IncluirPrioridadeCredTipoCompSaidaDTO incluirPrioridadeCredTipoCompSaidaDTO = getManterPrioridadeCredTipoComp().incluirPrioridadeCredTipoComp(incluirPrioridadeCredTipoCompEntradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +incluirPrioridadeCredTipoCompSaidaDTO.getCodMensagem() + ") " +incluirPrioridadeCredTipoCompSaidaDTO.getMensagem(), CON_MANTER_PRIORIDADE_CRED_TIPO_COMP, BradescoViewExceptionActionType.ACTION, false);
			carregaLista();
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		
		return "";
	}
	
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		
		
		try {
			ExcluirPrioridadeCredTipoCompEntradaDTO excluirPrioridadeCredTipoCompEntradaDTO = new ExcluirPrioridadeCredTipoCompEntradaDTO();
			
			ListarPrioridadeCredTipoCompSaidaDTO listarSaidaDTO= getListaGridPesquisa().get(getItemSelecionadoLista());
			
			excluirPrioridadeCredTipoCompEntradaDTO.setTipoCompromisso(listarSaidaDTO.getTipoCompromisso());
					
			ExcluirPrioridadeCredTipoCompSaidaDTO excluirPrioridadeCredTipoCompSaidaDTO = getManterPrioridadeCredTipoComp().excluirPrioridadeCredTipoComp(excluirPrioridadeCredTipoCompEntradaDTO);
						
			BradescoFacesUtils.addInfoModalMessage("(" +excluirPrioridadeCredTipoCompSaidaDTO.getCodMensagem() + ") " +excluirPrioridadeCredTipoCompSaidaDTO.getMensagem(), CON_MANTER_PRIORIDADE_CRED_TIPO_COMP, BradescoViewExceptionActionType.ACTION, false);
			carregaLista();
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		
		return "";
	}
	
	/*Getters e Setters*/
	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<ListarPrioridadeCredTipoCompSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(
			List<ListarPrioridadeCredTipoCompSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: tipoCompromisso.
	 *
	 * @return tipoCompromisso
	 */
	public String getTipoCompromisso() {
		return tipoCompromisso;
	}

	/**
	 * Set: tipoCompromisso.
	 *
	 * @param tipoCompromisso the tipo compromisso
	 */
	public void setTipoCompromisso(String tipoCompromisso) {
		this.tipoCompromisso = tipoCompromisso;
	}


	/**
	 * Get: anoDataVigencia.
	 *
	 * @return anoDataVigencia
	 */
	public String getAnoDataVigencia() {
		return anoDataVigencia;
	}

	/**
	 * Set: anoDataVigencia.
	 *
	 * @param anoDataVigencia the ano data vigencia
	 */
	public void setAnoDataVigencia(String anoDataVigencia) {
		this.anoDataVigencia = anoDataVigencia;
	}

	/**
	 * Get: diaDataVigencia.
	 *
	 * @return diaDataVigencia
	 */
	public String getDiaDataVigencia() {
		return diaDataVigencia;
	}

	/**
	 * Set: diaDataVigencia.
	 *
	 * @param diaDataVigencia the dia data vigencia
	 */
	public void setDiaDataVigencia(String diaDataVigencia) {
		this.diaDataVigencia = diaDataVigencia;
	}

	/**
	 * Get: mesDataVigencia.
	 *
	 * @return mesDataVigencia
	 */
	public String getMesDataVigencia() {
		return mesDataVigencia;
	}

	/**
	 * Set: mesDataVigencia.
	 *
	 * @param mesDataVigencia the mes data vigencia
	 */
	public void setMesDataVigencia(String mesDataVigencia) {
		this.mesDataVigencia = mesDataVigencia;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: prioridadeFiltro.
	 *
	 * @return prioridadeFiltro
	 */
	public String getPrioridadeFiltro() {
		return prioridadeFiltro;
	}

	/**
	 * Set: prioridadeFiltro.
	 *
	 * @param prioridadeFiltro the prioridade filtro
	 */
	public void setPrioridadeFiltro(String prioridadeFiltro) {
		this.prioridadeFiltro = prioridadeFiltro;
	}

	/**
	 * Get: tipoCompromissoFiltro.
	 *
	 * @return tipoCompromissoFiltro
	 */
	public String getTipoCompromissoFiltro() {
		return tipoCompromissoFiltro;
	}

	/**
	 * Set: tipoCompromissoFiltro.
	 *
	 * @param tipoCompromissoFiltro the tipo compromisso filtro
	 */
	public void setTipoCompromissoFiltro(String tipoCompromissoFiltro) {
		this.tipoCompromissoFiltro = tipoCompromissoFiltro;
	}

	/**
	 * Get: prioridade.
	 *
	 * @return prioridade
	 */
	public String getPrioridade() {
		return prioridade;
	}

	/**
	 * Get: dataFinalVigenciaDesc.
	 *
	 * @return dataFinalVigenciaDesc
	 */
	public String getDataFinalVigenciaDesc() {
		return dataFinalVigenciaDesc;
	}

	/**
	 * Set: dataFinalVigenciaDesc.
	 *
	 * @param dataFinalVigenciaDesc the data final vigencia desc
	 */
	public void setDataFinalVigenciaDesc(String dataFinalVigenciaDesc) {
		this.dataFinalVigenciaDesc = dataFinalVigenciaDesc;
	}

	/**
	 * Get: dataInicioVigenciaDesc.
	 *
	 * @return dataInicioVigenciaDesc
	 */
	public String getDataInicioVigenciaDesc() {
		return dataInicioVigenciaDesc;
	}

	/**
	 * Set: dataInicioVigenciaDesc.
	 *
	 * @param dataInicioVigenciaDesc the data inicio vigencia desc
	 */
	public void setDataInicioVigenciaDesc(String dataInicioVigenciaDesc) {
		this.dataInicioVigenciaDesc = dataInicioVigenciaDesc;
	}

	/**
	 * Set: prioridade.
	 *
	 * @param prioridade the prioridade
	 */
	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}

	/**
	 * Get: dataFimVigencia.
	 *
	 * @return dataFimVigencia
	 */
	public Date getDataFimVigencia() {		 
		return dataFimVigencia;
	}

	/**
	 * Set: dataFimVigencia.
	 *
	 * @param dataFimVigencia the data fim vigencia
	 */
	public void setDataFimVigencia(Date dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	/**
	 * Get: dataInicioVigencia.
	 *
	 * @return dataInicioVigencia
	 */
	public Date getDataInicioVigencia() {		 
		return dataInicioVigencia;
	}

	/**
	 * Set: dataInicioVigencia.
	 *
	 * @param dataInicioVigencia the data inicio vigencia
	 */
	public void setDataInicioVigencia(Date dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}
	
	/**
	 * Get: manterPrioridadeCredTipoComp.
	 *
	 * @return manterPrioridadeCredTipoComp
	 */
	public IPrioridadeCredTipoCompService getManterPrioridadeCredTipoComp() {
		return manterPrioridadeCredTipoComp;
	}

	/**
	 * Set: manterPrioridadeCredTipoComp.
	 *
	 * @param manterPrioridadeCredTipoComp the manter prioridade cred tipo comp
	 */
	public void setManterPrioridadeCredTipoComp(
			IPrioridadeCredTipoCompService manterPrioridadeCredTipoComp) {
		this.manterPrioridadeCredTipoComp = manterPrioridadeCredTipoComp;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Limpar prioridade vigencia.
	 */
	public void limparPrioridadeVigencia(){
		if (getTipoCompromissoFiltro() != null && !getTipoCompromisso().equals("")){
			setDiaDataVigencia("");
			setMesDataVigencia("");
			setAnoDataVigencia("");
			setPrioridadeFiltro("");
		}
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Is bloquea radio.
	 *
	 * @return true, if is bloquea radio
	 */
	public boolean isBloqueaRadio() {
		return bloqueaRadio;
	}

	/**
	 * Set: bloqueaRadio.
	 *
	 * @param bloqueaRadio the bloquea radio
	 */
	public void setBloqueaRadio(boolean bloqueaRadio) {
		this.bloqueaRadio = bloqueaRadio;
	}

	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}

	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}

	/**
	 * Get: centroCustoLupaFiltro.
	 *
	 * @return centroCustoLupaFiltro
	 */
	public String getCentroCustoLupaFiltro() {
		return centroCustoLupaFiltro;
	}

	/**
	 * Set: centroCustoLupaFiltro.
	 *
	 * @param centroCustoLupaFiltro the centro custo lupa filtro
	 */
	public void setCentroCustoLupaFiltro(String centroCustoLupaFiltro) {
		this.centroCustoLupaFiltro = centroCustoLupaFiltro;
	}

	/**
	 * Get: listaCentroCustoLupa.
	 *
	 * @return listaCentroCustoLupa
	 */
	public List<CentroCustoSaidaDTO> getListaCentroCustoLupa() {
		return listaCentroCustoLupa;
	}

	/**
	 * Set: listaCentroCustoLupa.
	 *
	 * @param listaCentroCustoLupa the lista centro custo lupa
	 */
	public void setListaCentroCustoLupa(
			List<CentroCustoSaidaDTO> listaCentroCustoLupa) {
		this.listaCentroCustoLupa = listaCentroCustoLupa;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: listaCentroCustoLupaSelectItem.
	 *
	 * @return listaCentroCustoLupaSelectItem
	 */
	public List<SelectItem> getListaCentroCustoLupaSelectItem() {
		return listaCentroCustoLupaSelectItem;
	}

	/**
	 * Set: listaCentroCustoLupaSelectItem.
	 *
	 * @param listaCentroCustoLupaSelectItem the lista centro custo lupa select item
	 */
	public void setListaCentroCustoLupaSelectItem(
			List<SelectItem> listaCentroCustoLupaSelectItem) {
		this.listaCentroCustoLupaSelectItem = listaCentroCustoLupaSelectItem;
	}

	/**
	 * Get: cdSistema.
	 *
	 * @return cdSistema
	 */
	public String getCdSistema() {
		return cdSistema;
	}

	/**
	 * Set: cdSistema.
	 *
	 * @param cdSistema the cd sistema
	 */
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	/**
	 * Get: centroCustoFiltro.
	 *
	 * @return centroCustoFiltro
	 */
	public String getCentroCustoFiltro() {
		return centroCustoFiltro;
	}

	/**
	 * Set: centroCustoFiltro.
	 *
	 * @param centroCustoFiltro the centro custo filtro
	 */
	public void setCentroCustoFiltro(String centroCustoFiltro) {
		this.centroCustoFiltro = centroCustoFiltro;
	}

	/**
	 * Get: inicialCentroCustoFiltro.
	 *
	 * @return inicialCentroCustoFiltro
	 */
	public String getInicialCentroCustoFiltro() {
		return inicialCentroCustoFiltro;
	}

	/**
	 * Set: inicialCentroCustoFiltro.
	 *
	 * @param inicialCentroCustoFiltro the inicial centro custo filtro
	 */
	public void setInicialCentroCustoFiltro(String inicialCentroCustoFiltro) {
		this.inicialCentroCustoFiltro = inicialCentroCustoFiltro;
	}

	/**
	 * Get: listaCentroCustoFiltro.
	 *
	 * @return listaCentroCustoFiltro
	 */
	public List<SelectItem> getListaCentroCustoFiltro() {
		return listaCentroCustoFiltro;
	}

	/**
	 * Set: listaCentroCustoFiltro.
	 *
	 * @param listaCentroCustoFiltro the lista centro custo filtro
	 */
	public void setListaCentroCustoFiltro(List<SelectItem> listaCentroCustoFiltro) {
		this.listaCentroCustoFiltro = listaCentroCustoFiltro;
	}

	/**
	 * Get: listaCentroCustoLupaHash.
	 *
	 * @return listaCentroCustoLupaHash
	 */
	public Map<String, String> getListaCentroCustoLupaHash() {
		return listaCentroCustoLupaHash;
	}

	/**
	 * Set lista centro custo lupa hash.
	 *
	 * @param listaCentroCustoLupaHash the lista centro custo lupa hash
	 */
	public void setListaCentroCustoLupaHash(
			Map<String, String> listaCentroCustoLupaHash) {
		this.listaCentroCustoLupaHash = listaCentroCustoLupaHash;
	}

	/**
	 * Get: dtInicioVig.
	 *
	 * @return dtInicioVig
	 */
	public Date getDtInicioVig() {
		return dtInicioVig;
	}

	/**
	 * Set: dtInicioVig.
	 *
	 * @param dtInicioVig the dt inicio vig
	 */
	public void setDtInicioVig(Date dtInicioVig) {
		this.dtInicioVig = dtInicioVig;
	}
	
	

}
