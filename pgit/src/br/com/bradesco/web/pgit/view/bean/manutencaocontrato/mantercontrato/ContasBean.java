/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarContasRelacionadasParaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarContasRelacionadasParaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarFinalidadeContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMunicipioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMunicipioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoRelacContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoRelacContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarUnidadeFederativaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarConfContaContEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarConfContaContSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarContaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarContaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.DetalharContaRelacionadaHistEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.DetalharContaRelacionadaHistSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirContaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirContaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirRelaciAssocEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirRelaciAssocSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.FinalidadeAlterarContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.FinalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirContaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirContaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirRelacContaContEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirRelacContaContSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirRelacContaOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManContaVinculadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManContaVinculadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasPossiveisInclusaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasPossiveisInclusaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasRelContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasRelContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasRelacionadasHistEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasRelacionadasHistSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasVincContEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasVincContSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.SubsRelacContaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.SubsRelacContaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.SubsRelacContaOcorrenciasDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ContasBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ContasBean {

	/** Atributo SIM_CONSTANT. */
	private static final String SIM_CONSTANT = "SIM";

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;

	/** Atributo manterContratoBean. */
	private ManterContratoBean manterContratoBean;

	// Atributos Tela Consultar
	/** Atributo radioPesquisaContas. */
	private String radioPesquisaContas;

	/** Atributo participanteFiltro. */
	private Long participanteFiltro;

	/** Atributo bancoFiltro. */
	private String bancoFiltro;

	/** Atributo agenciaFiltro. */
	private String agenciaFiltro;

	/** Atributo agenciaDigitoFiltro. */
	private String agenciaDigitoFiltro;

	/** Atributo contaFiltro. */
	private String contaFiltro;

	/** Atributo digitoFiltro. */
	private String digitoFiltro;

	/** Atributo tipoContaFiltro. */
	private Integer tipoContaFiltro;

	/** Atributo finalidadeFiltro. */
	private Integer finalidadeFiltro;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo cdCpfCnpjFiltro. */
	private String cdCpfCnpjFiltro;

	/** Atributo cdFilialCnpjFiltro. */
	private String cdFilialCnpjFiltro;

	/** Atributo cdControleCnpjFiltro. */
	private String cdControleCnpjFiltro;

	/** Atributo cdCpfFiltro. */
	private String cdCpfFiltro;

	/** Atributo cdControleCpfFiltro. */
	private String cdControleCpfFiltro;

	/** Atributo cpf. */
	private String cpf;

	/** Atributo razao. */
	private String razao;

	/** Atributo codBanco. */
	private Integer codBanco;

	/** Atributo cdAgencia. */
	private Integer cdAgencia;

	/** Atributo cdTipoRelacionamentoHistorico. */
	private Integer cdTipoRelacionamentoHistorico;

	/** Atributo listaParticipanteFiltro. */
	private List<SelectItem> listaParticipanteFiltro = new ArrayList<SelectItem>();

	/** Atributo listaParticipanteHash. */
	private Map<Integer, String> listaParticipanteHash = new HashMap<Integer, String>();

	/** Atributo listaTipoConta. */
	private List<SelectItem> listaTipoConta = new ArrayList<SelectItem>();

	/** Atributo listaTipoContaHash. */
	private Map<Integer, String> listaTipoContaHash = new HashMap<Integer, String>();

	/** Atributo listaFinalidadeConta. */
	private List<SelectItem> listaFinalidadeConta = new ArrayList<SelectItem>();

	/** Atributo listaFinalidadeContaHash. */
	private Map<Integer, String> listaFinalidadeContaHash = new HashMap<Integer, String>();

	/** Atributo listaFinalidadeContaSelecionada. */
	private List<SelectItem> listaFinalidadeContaSelecionada = new ArrayList<SelectItem>();

	/** Atributo listaFinalidadeContaSelecionadaHash. */
	private Map<Integer, String> listaFinalidadeContaSelecionadaHash = new HashMap<Integer, String>();

	/** Atributo listaUnidadeFederativa. */
	private List<SelectItem> listaUnidadeFederativa = new ArrayList<SelectItem>();

	/** Atributo listaUnidadeFederativaHash. */
	private Map<Integer, String> listaUnidadeFederativaHash = new HashMap<Integer, String>();

	/** Atributo listaMunicipio. */
	private List<SelectItem> listaMunicipio = new ArrayList<SelectItem>();

	/** Atributo listaMunicipioHash. */
	private Map<Long, String> listaMunicipioHash = new HashMap<Long, String>();

	/** Atributo obrigatoriedadeConsultar. */
	private String obrigatoriedadeConsultar;

	/** Atributo hiddenConfirma. */
	private boolean hiddenConfirma;

	// Grid Consultar Contas
	/** Atributo listaGridPesquisaContas. */
	private List<ListarContasVincContSaidaDTO> listaGridPesquisaContas;

	/** Atributo itemSelecionadoListaContas. */
	private Integer itemSelecionadoListaContas;

	/** Atributo listaControleRadioContas. */
	private List<SelectItem> listaControleRadioContas = new ArrayList<SelectItem>();

	// Grid Consultar Substituir
	/** Atributo listaGridPesquisaSubstituir. */
	private List<ListarContasVincContSaidaDTO> listaGridPesquisaSubstituir;

	// Grid Consultar Substituir
	/** Atributo listaGridPesquisaSubst. */
	private List<ListarContasRelacionadasParaContratoSaidaDTO> listaGridPesquisaSubst;

	/** Atributo itemSelecionadoListaSubstituir. */
	private Integer itemSelecionadoListaSubstituir;

	/** Atributo listaControleRadioSubstituir. */
	private List<SelectItem> listaControleRadioSubstituir = new ArrayList<SelectItem>();

	// Grid Consultar Historico
	/** Atributo listaGridPesquisaHistorico. */
	private List<ListarConManContaVinculadaSaidaDTO> listaGridPesquisaHistorico;

	/** Atributo itemSelecionadoListaHistorico. */
	private Integer itemSelecionadoListaHistorico;

	/** Atributo listaControleRadioHistorico. */
	private List<SelectItem> listaControleRadioHistorico = new ArrayList<SelectItem>();

	// ATRIBUTOS TRILHA AUDITORIA
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo finalidade. */
	private Integer finalidade;

	/** Atributo radioPesquisa. */
	private String radioPesquisa;

	/** Atributo participante. */
	private Long participante;

	/** Atributo banco. */
	private String banco;

	/** Atributo agencia. */
	private String agencia;

	/** Atributo agenciaDigito. */
	private String agenciaDigito;

	/** Atributo conta. */
	private String conta;

	/** Atributo digito. */
	private String digito;

	/** Atributo tipoConta. */
	private Integer tipoConta;

	/** Atributo cboTipoRelacionamento. */
	private Integer cboTipoRelacionamento;

	/** Atributo listaCboTipoRelacionamento. */
	private List<SelectItem> listaCboTipoRelacionamento = new ArrayList<SelectItem>();

	/** Atributo cdCpfCnpj. */
	private String cdCpfCnpj;

	/** Atributo cdFilialCnpj. */
	private String cdFilialCnpj;

	/** Atributo cdControleCnpj. */
	private String cdControleCnpj;

	/** Atributo cdCpf. */
	private String cdCpf;

	/** Atributo cdControleCpf. */
	private String cdControleCpf;
	
	private String descApelido;

	// Tela Relacionamentos
	/** Atributo listaGridPesquisaContasRelacionamentos. */
	private List<ListarContasRelContaSaidaDTO> listaGridPesquisaContasRelacionamentos;

	/** Atributo itemSelecionadoListaContasRelacionamentos. */
	private Integer itemSelecionadoListaContasRelacionamentos;

	/** Atributo listaControleRadioContasRelacionamentos. */
	private List<SelectItem> listaControleRadioContasRelacionamentos = new ArrayList<SelectItem>();

	// Tela Incluir
	/** Atributo listaGridPesquisaContasIncluir. */
	private List<ListarContasPossiveisInclusaoSaidaDTO> listaGridPesquisaContasIncluir;

	/** Atributo itemSelecionadoListaContasIncluir. */
	private Integer itemSelecionadoListaContasIncluir;

	/** Atributo listaControleRadioContasIncluir. */
	private List<SelectItem> listaControleRadioContasIncluir = new ArrayList<SelectItem>();

	// Tela Relacionamento Hist�rico
	/** Atributo listaGridPesquisaContasRelHistorico. */
	private List<ListarContasRelacionadasHistSaidaDTO> listaGridPesquisaContasRelHistorico;

	/** Atributo itemSelecionadoListaContasRelHistorico. */
	private Integer itemSelecionadoListaContasRelHistorico;

	/** Atributo listaControleRadioContasRelHistorico. */
	private List<SelectItem> listaControleRadioContasRelHistorico = new ArrayList<SelectItem>();

	// Tela Relacionamento Incluir
	/** Atributo habilitaSelecionarIncluirRelacionamento. */
	private boolean habilitaSelecionarIncluirRelacionamento;

	/** Atributo listaGridPesquisaContasRelIncluir. */
	private List<ListarContasVincContSaidaDTO> listaGridPesquisaContasRelIncluir;

	/** Atributo listaGridPesquisaContasRelIncluir. */
	private List<ListarContasRelacionadasParaContratoSaidaDTO> listaGridPesquisaContasRelInc;

	/** Atributo itemSelecionadoListaContasRelIncluir. */
	private Integer itemSelecionadoListaContasRelIncluir;

	/** Atributo listaControleRadioContasRelIncluir. */
	private List<SelectItem> listaControleRadioContasRelIncluir = new ArrayList<SelectItem>();

	/** Atributo listaFinalidades. */
	private List<FinalidadeSaidaDTO> listaFinalidades;

	/** Atributo listaControleCheckFinalidades. */
	private List<SelectItem> listaControleCheckFinalidades;

	/** Atributo listaFinalidadesSelecionadas. */
	private List<FinalidadeSaidaDTO> listaFinalidadesSelecionadas;

	/** Atributo listaOcorrenciasIncluirRelAux. */
	private List<ListarContasVincContSaidaDTO> listaOcorrenciasIncluirRelAux;

	/** Atributo listaOcorrenciasIncluirRelAux. */
	private List<ListarContasRelacionadasParaContratoSaidaDTO> listaOcorrenciasIncluirRelAux2;

	/** Atributo listaOcorrenciasIncluirRel. */
	private List<IncluirRelacContaOcorrenciasDTO> listaOcorrenciasIncluirRel;

	/** Atributo listaControleCheckRelInc. */
	private List<SelectItem> listaControleCheckRelInc;

	/** Atributo radioMunicipioFeriadoLocal. */
	private String radioMunicipioFeriadoLocal;

	/** Atributo unidadeFederativa. */
	private Integer unidadeFederativa;

	/** Atributo municipio. */
	private Long municipio;

	/** Atributo finalidadeRelacionamento. */
	private Integer finalidadeRelacionamento;

	/** Atributo tipoRelacionamento. */
	private Integer tipoRelacionamento;

	/** Atributo tipoRelacionamentoHistorico. */
	private Integer tipoRelacionamentoHistorico;

	/** Atributo cdPessoaJuridica. */
	private Long cdPessoaJuridica;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;

	/** Atributo nrSequenciaContratoNegocio. */
	private String nrSequenciaContratoNegocio;

	// Conta do Contrato
	/** Atributo bancoDesc. */
	private String bancoDesc;

	/** Atributo agenciaDesc. */
	private String agenciaDesc;

	/** Atributo contaDesc. */
	private String contaDesc;

	/** Atributo tipoDesc. */
	private String tipoDesc;

	/** Atributo situacaoVinculoDesc. */
	private String situacaoVinculoDesc;

	/** Atributo dataVinculacaoDesc. */
	private String dataVinculacaoDesc;

	/** Atributo municipioFeriadoLocalDesc. */
	private int municipioFeriadoLocalDesc;

	/** Atributo finalidadeDesc. */
	private String finalidadeDesc;

	/** Atributo finalidadeDescSub. */
	private String finalidadeDescSub;

	// Conta a ser Relacionada
	/** Atributo clubRelacionadoDesc. */
	private String clubRelacionadoDesc;

	/** Atributo cpfCnpjDesc. */
	private String cpfCnpjDesc;

	/** Atributo nomeRazaoDesc. */
	private String nomeRazaoDesc;

	/** Atributo bancoRelacionadaDesc. */
	private String bancoRelacionadaDesc;

	/** Atributo agenciaRelacionadaDesc. */
	private String agenciaRelacionadaDesc;

	/** Atributo contaRelacionadaDesc. */
	private String contaRelacionadaDesc;

	/** Atributo tipoRelacionadaDesc. */
	private String tipoRelacionadaDesc;

	/** Atributo finalidadeContaRelacionadaDesc. */
	private String finalidadeContaRelacionadaDesc;

	/** Atributo tipoManutencao. */
	private String tipoManutencao;

	/** Atributo cdMotivoSituacaoConta. */
	private int cdMotivoSituacaoConta;

	/** Atributo cdSituacaoVinculacaoConta. */
	private int cdSituacaoVinculacaoConta;
	
	private String cdTipoInscricao;

	/** Atributo municipioDesc. */
	private String municipioDesc;

	/** Atributo unidadeFederativaDesc. */
	private String unidadeFederativaDesc;

	// Tela Hist�rico
	/** Atributo dataManutencaoInicio. */
	private Date dataManutencaoInicio;

	/** Atributo dataManutencaoFim. */
	private Date dataManutencaoFim;

	/** Atributo habilitarHistorico. */
	private boolean habilitarHistorico;

	/** Atributo habilitarConta. */
	private boolean habilitarConta;

	/** Atributo habilitaMunicipioAlterar. */
	private boolean habilitaMunicipioAlterar;

	/** Atributo habilitaRadioMunicipioFeriadoLocal. */
	private boolean habilitaRadioMunicipioFeriadoLocal;

	/** Atributo tipoRelacionamentoDesc. */
	private String tipoRelacionamentoDesc;

	/** Atributo tipoContaDesc. */
	private String tipoContaDesc;

	/** Atributo bancoDescFormatado. */
	private String bancoDescFormatado;

	/** Atributo agenciaDescFormatado. */
	private String agenciaDescFormatado;

	/** Atributo dsMunicipio. */
	private String dsMunicipio;

	/** Atributo dsUnidFede. */
	private String dsUnidFede;

	/** Atributo cdMunicipioFeri. */
	private Integer cdMunicipioFeri;

	/** Atributo valorLimite. */
	private BigDecimal valorLimite;

	/** Atributo dataPrazoInicio. */
	private Date dataPrazoInicio;

	/** Atributo dataPrazoFim. */
	private Date dataPrazoFim;

	/** Atributo valorLimiteTED. */
	private BigDecimal valorLimiteTED;

	/** Atributo dataPrazoInicioTED. */
	private Date dataPrazoInicioTED;

	/** Atributo dataPrazoFimTED. */
	private Date dataPrazoFimTED;

	/** Atributo prazoValidadeAltDesc. */
	private String prazoValidadeAltDesc;

	/** Atributo prazoValidadeTEDAltDesc. */
	private String prazoValidadeTEDAltDesc;

	/** Atributo tpManutencao. */
	private String tpManutencao;

	/** Atributo cdCpfCnpjRepresentante. */
	private Long cdCpfCnpjRepresentante;

	/** Atributo cdFilialCpfCnpjRepresentante. */
	private Integer cdFilialCpfCnpjRepresentante;

	/** Atributo cdControleCpfCnpjRepresentante. */
	private Integer cdControleCpfCnpjRepresentante;

	private Integer codBancoPrincipal;

	private Integer codAgenciaPrincipal;

	private Integer codDigAgenciaPrincipal;

	private Long codContaPrincipal;

	private String codDigitoContaPrincipal;

	/** Atributo filtroContasPaginacao. */
	private ListarContasVincContEntradaDTO filtroContasPaginacao = null;
	
	/* M�TODOS */

	/**
	 * Carrega combo tipo relacionamento.
	 */
	public void carregaComboTipoRelacionamento() {
		try {
			setTipoRelacionamento(null);
			setCboTipoRelacionamento(null);
			setListaCboTipoRelacionamento(new ArrayList<SelectItem>());
			ListarTipoRelacContaEntradaDTO entrada = new ListarTipoRelacContaEntradaDTO();

			entrada.setCdSituacaoVinculacaoConta(0);

			List<ListarTipoRelacContaSaidaDTO> list = getComboService().listarTipoRelacConta(entrada);

			for (ListarTipoRelacContaSaidaDTO combo : list) {
				listaCboTipoRelacionamento.add(new SelectItem(combo.getTpRelacionamentoContrato(), combo
						.getRsTipoRelacionamentoContrato()));
			}
		} catch (PdcAdapterFunctionalException e) {
			setCboTipoRelacionamento(null);
			setListaCboTipoRelacionamento(new ArrayList<SelectItem>());
		}
	}

	/**
	 * Iniciar tela.
	 */
	public void iniciarTela() {
		setListaGridPesquisaContas(null);
		setItemSelecionadoListaContas(null);
		limparDados();
		carregaListaTipoConta();
		carregaListaFinalidadeConta();
		carregaListaUnidadeFederativa();
		carregaComboTipoRelacionamento();
	}

	/**
	 * Limpar dados.
	 */
	public void limparDados() {
		setRadioPesquisaContas(null);
		limparCamposFiltroConta();
		limparGrid();
	}

	/**
	 * Consultar.
	 * 
	 * @return the string
	 */
	public String consultar() {
		carregaListaSemLoop();
		if (getListaGridPesquisaContas() != null && getListaGridPesquisaContas().size() >= 1) {
			if (getListaGridPesquisaContas().get(0).getCodMensagem().equals("PGIT1133")
					|| getListaGridPesquisaContas().get(0).getCodMensagem().equals("PGIT1134")) {
				BradescoFacesUtils.addInfoModalMessage("(" + getListaGridPesquisaContas().get(0).getCodMensagem()
						+ ") " + getListaGridPesquisaContas().get(0).getMensagem(), false);
			}
		}
		return "ok";
	}

	/**
	 * Habilita botao selecionar incluir relacionamento.
	 */
	public void habilitaBotaoSelecionarIncluirRelacionamento() {
		setHabilitaSelecionarIncluirRelacionamento(false);

		for (int i = 0; i < getListaGridPesquisaContasRelInc().size(); i++) {
			if (getListaGridPesquisaContasRelInc().get(i).isCheck()) {
				setHabilitaSelecionarIncluirRelacionamento(true);
			}
		}
	}

	/**
	 * Carrega lista.
	 * 
	 * @return the string
	 */
	public String carregaLista() {
		ListarContasVincContEntradaDTO entrada = new ListarContasVincContEntradaDTO();

		entrada.setCdBanco((!getBancoFiltro().equals("")) ? Integer.parseInt(getBancoFiltro()) : 0);
		entrada.setCdAgencia((!getAgenciaFiltro().equals("")) ? Integer.parseInt(getAgenciaFiltro()) : 0);
		entrada.setCdConta((!getContaFiltro().equals("")) ? Long.parseLong(getContaFiltro()) : 0);
		entrada.setCdDigitoConta((!getDigitoFiltro().equals("")) ? getDigitoFiltro() : "0");
		entrada.setCdDigitoAgencia((!getAgenciaDigitoFiltro().equals("")) ? Integer
				.parseInt(getAgenciaDigitoFiltro()) : 0);
		entrada.setCdFinalidade((getFinalidadeFiltro() != null) ? getFinalidadeFiltro() : 0);
		entrada.setCdTipoConta((getTipoContaFiltro() != null) ? getTipoContaFiltro() : 0);

		entrada.setCdPessoaJuridicaNegocio(getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
		entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

		if (getRadioPesquisaContas() != null) {

			// Pesquisar por CNPJ
			if ("0".equals(getRadioPesquisaContas())) {
				if (!"".equals(getCdCpfCnpjFiltro())) {
					entrada.setCdCorpoCfpCnpj(Long.parseLong(getCdCpfCnpjFiltro()));
					entrada.setCdControleCpfCnpj(Integer.parseInt(getCdFilialCnpjFiltro()));
					entrada.setCdDigitoCpfCnpj(Integer.parseInt(getCdControleCnpjFiltro()));
				} else {
					entrada.setCdCorpoCfpCnpj(getCdCpfCnpjRepresentante());
					entrada.setCdControleCpfCnpj(getCdFilialCpfCnpjRepresentante());
					entrada.setCdDigitoCpfCnpj(getCdControleCpfCnpjRepresentante());
				}
			}

			// Pesquisar por CPF
			if ("1".equals(getRadioPesquisaContas())) {
				if (!"".equals(getCdCpfFiltro())) {
					entrada.setCdCorpoCfpCnpj(Long.parseLong(getCdCpfFiltro()));
					entrada.setCdControleCpfCnpj(0);
					entrada.setCdDigitoCpfCnpj(Integer.parseInt(getCdControleCpfFiltro()));
				} else {
					entrada.setCdCorpoCfpCnpj(getCdCpfCnpjRepresentante());
					entrada.setCdControleCpfCnpj(getCdFilialCpfCnpjRepresentante());
					entrada.setCdDigitoCpfCnpj(getCdControleCpfCnpjRepresentante());
				}
			}

		} else {
			entrada.setCdCorpoCfpCnpj(0l);
			entrada.setCdControleCpfCnpj(0);
			entrada.setCdDigitoCpfCnpj(0);
		}

		filtroContasPaginacao = entrada.clone();

		listarContasVinculadasContrato();

		return "";

	}
	
	/**
	 * Carrega lista.
	 * 
	 * @return the string
	 */
	public String carregaListaSemLoop() {
		ListarContasVincContEntradaDTO entrada = new ListarContasVincContEntradaDTO();

		entrada.setCdBanco((!getBancoFiltro().equals("")) ? Integer.parseInt(getBancoFiltro()) : 0);
		entrada.setCdAgencia((!getAgenciaFiltro().equals("")) ? Integer.parseInt(getAgenciaFiltro()) : 0);
		entrada.setCdConta((!getContaFiltro().equals("")) ? Long.parseLong(getContaFiltro()) : 0);
		entrada.setCdDigitoConta((!getDigitoFiltro().equals("")) ? getDigitoFiltro() : "0");
		entrada.setCdDigitoAgencia((!getAgenciaDigitoFiltro().equals("")) ? Integer
				.parseInt(getAgenciaDigitoFiltro()) : 0);
		entrada.setCdFinalidade((getFinalidadeFiltro() != null) ? getFinalidadeFiltro() : 0);
		entrada.setCdTipoConta((getTipoContaFiltro() != null) ? getTipoContaFiltro() : 0);

		entrada.setCdPessoaJuridicaNegocio(getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
		entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

		if (getRadioPesquisaContas() != null) {

			// Pesquisar por CNPJ
			if ("0".equals(getRadioPesquisaContas())) {
				if (!"".equals(getCdCpfCnpjFiltro())) {
					entrada.setCdCorpoCfpCnpj(Long.parseLong(getCdCpfCnpjFiltro()));
					entrada.setCdControleCpfCnpj(Integer.parseInt(getCdFilialCnpjFiltro()));
					entrada.setCdDigitoCpfCnpj(Integer.parseInt(getCdControleCnpjFiltro()));
				} else {
					entrada.setCdCorpoCfpCnpj(getCdCpfCnpjRepresentante());
					entrada.setCdControleCpfCnpj(getCdFilialCpfCnpjRepresentante());
					entrada.setCdDigitoCpfCnpj(getCdControleCpfCnpjRepresentante());
				}
			}

			// Pesquisar por CPF
			if ("1".equals(getRadioPesquisaContas())) {
				if (!"".equals(getCdCpfFiltro())) {
					entrada.setCdCorpoCfpCnpj(Long.parseLong(getCdCpfFiltro()));
					entrada.setCdControleCpfCnpj(0);
					entrada.setCdDigitoCpfCnpj(Integer.parseInt(getCdControleCpfFiltro()));
				} else {
					entrada.setCdCorpoCfpCnpj(getCdCpfCnpjRepresentante());
					entrada.setCdControleCpfCnpj(getCdFilialCpfCnpjRepresentante());
					entrada.setCdDigitoCpfCnpj(getCdControleCpfCnpjRepresentante());
				}
			}

		} else {
			entrada.setCdCorpoCfpCnpj(0l);
			entrada.setCdControleCpfCnpj(0);
			entrada.setCdDigitoCpfCnpj(0);
		}

		filtroContasPaginacao = entrada.clone();

		listarContasVinculadasContratoSemLoop();

		return "";

	}

	/**
     * Listar contas vinculadas contrato.
     */
	private void listarContasVinculadasContrato() {
	    listaControleRadioContas = new ArrayList<SelectItem>();
	    try {
            setListaGridPesquisaContas(getManterContratoImpl().listarContasVinculadasContrato(filtroContasPaginacao));

            for (int i = 0; i < getListaGridPesquisaContas().size(); i++) {
                this.listaControleRadioContas.add(new SelectItem(i, " "));
            }

            setItemSelecionadoListaContas(null);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                    false);
            setListaGridPesquisaContas(null);
        }
	}
	
	/**
     * Listar contas vinculadas contrato.
     */
	private void listarContasVinculadasContratoSemLoop() {
	    listaControleRadioContas = new ArrayList<SelectItem>();
	    try {
            setListaGridPesquisaContas(getManterContratoImpl().listarContasVinculadasContratoCinquenta(filtroContasPaginacao));

            for (int i = 0; i < getListaGridPesquisaContas().size(); i++) {
                this.listaControleRadioContas.add(new SelectItem(i, " "));
            }

            setItemSelecionadoListaContas(null);

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                    false);
            setListaGridPesquisaContas(null);
        }
	}
	
	/**
	 * Limpar campos filtro conta.
	 */
	public void limparCamposFiltroConta() {
		setAgenciaFiltro("");
		setBancoFiltro("");
		setAgenciaFiltro("");
		setContaFiltro("");
		setDigitoFiltro("");
		setFinalidadeFiltro(null);
		setParticipanteFiltro(null);
		setTipoContaFiltro(null);
		setAgenciaDigitoFiltro("");

		setCdCpfCnpjFiltro("");
		setCdFilialCnpjFiltro("");
		setCdControleCnpjFiltro("");
		setCdCpfFiltro("");
		setCdControleCpfFiltro("");
		valorBanco();

	}

	/**
	 * Valor banco.
	 */
	public void valorBanco() {
		if ("2".equals(getRadioPesquisaContas())) {
			setBancoFiltro("237");
		} else {
			setBancoFiltro("");
		}
	}

	/**
	 * Carrega lista tipo conta.
	 */
	public void carregaListaTipoConta() {
		listaTipoConta = new ArrayList<SelectItem>();
		List<ListarTipoContaSaidaDTO> listaSaida = new ArrayList<ListarTipoContaSaidaDTO>();
		listaSaida = this.getComboService().listarTipoConta();

		listaTipoContaHash.clear();

		for (ListarTipoContaSaidaDTO combo : listaSaida) {
			listaTipoContaHash.put(combo.getCdTipoConta(), combo.getDsTipoConta());
			this.listaTipoConta.add(new SelectItem(combo.getCdTipoConta(), combo.getDsTipoConta()));
		}

	}

	/**
	 * Carrega lista unidade federativa.
	 */
	public void carregaListaUnidadeFederativa() {
		listaUnidadeFederativa = new ArrayList<SelectItem>();
		List<ListarUnidadeFederativaSaidaDTO> listaSaida = new ArrayList<ListarUnidadeFederativaSaidaDTO>();
		listaSaida = this.getComboService().listarUnidadeFederativa();

		listaUnidadeFederativaHash.clear();

		Collections.sort((List<ListarUnidadeFederativaSaidaDTO>) listaSaida, new Comparator<Object>() {
			public int compare(Object arg0, Object arg1) {
				return ((ListarUnidadeFederativaSaidaDTO) arg0).getDsUnidadeFederativa().toUpperCase().compareTo(
						((ListarUnidadeFederativaSaidaDTO) arg1).getDsUnidadeFederativa().toUpperCase());
			}
		});

		for (ListarUnidadeFederativaSaidaDTO combo : listaSaida) {
			listaUnidadeFederativaHash.put(combo.getCdUnidadeFederativa(), combo.getDsUnidadeFederativa());
			this.listaUnidadeFederativa.add(new SelectItem(combo.getCdUnidadeFederativa(), combo
					.getDsUnidadeFederativa()));
		}

	}

	/**
	 * Carrega lista finalidade conta.
	 */
	public void carregaListaFinalidadeConta() {
		listaFinalidadeConta = new ArrayList<SelectItem>();
		List<ListarFinalidadeContaSaidaDTO> listaSaida = new ArrayList<ListarFinalidadeContaSaidaDTO>();
		listaSaida = this.getComboService().listarFinalidadeConta();

		listaFinalidadeContaHash.clear();

		for (int i = 0; i < listaSaida.size(); i++) {
			listaFinalidadeContaHash.put(listaSaida.get(i).getCdFinalidadeContaPagamento(), listaSaida.get(i)
					.getRsFinalidadeContaPagamento());
			this.listaFinalidadeConta.add(new SelectItem(listaSaida.get(i).getCdFinalidadeContaPagamento(), listaSaida
					.get(i).getRsFinalidadeContaPagamento()));
		}
	}

	/**
	 * Limpar grid.
	 * 
	 * @return the string
	 */
	public String limparGrid() {
		setListaGridPesquisaContas(null);
		setItemSelecionadoListaContas(null);

		return "";
	}

	/**
	 * Confirmar excluir.
	 * 
	 * @return the string
	 */
	public String confirmarExcluir() {
		if (isHiddenConfirma()) {
			try {
				ExcluirContaContratoEntradaDTO entrada = new ExcluirContaContratoEntradaDTO();
				entrada.setCdPessoaJuridicaVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
						.getCdPessoaJuridicaVinculo());
				entrada.setCdTipoContratoVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
						.getCdTipoContratoVinculo());
				entrada.setCdTipoVinculoContrato(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
						.getCdTipoVinculoContrato());
				entrada.setNrSequenciaContratoVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
						.getNrSequenciaContratoVinculo());

				entrada.setCdPessoaJuridica(getCdPessoaJuridica());
				entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
				entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));
				entrada.setCdCpfCnpj(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
						.getCdCpfCnpj());

				ExcluirContaContratoSaidaDTO saida = getManterContratoImpl().excluirContaContrato(entrada);

				BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
						"conContasManterContrato", BradescoViewExceptionActionType.ACTION, false);
				listarContasVinculadasContrato();

			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
						false);
				return null;
			}
		}
		return "";
	}

	/**
	 * Limpar dados relacionamento.
	 * 
	 * @return the string
	 */
	public String limparDadosRelacionamento() {
		this.setFinalidadeRelacionamento(null);
		this.setListaGridPesquisaContasRelacionamentos(null);
		this.setItemSelecionadoListaContasRelacionamentos(null);

		return "";
	}

	/**
     * Pesquisar contas.
     * 
     * @param evt
     *            the evt
     */
	public void pesquisarContas(ActionEvent evt) {
		listarContasVinculadasContrato();
	}

	/**
	 * Pesquisar historico.
	 * 
	 * @param evt
	 *            the evt
	 * @return the string
	 */
	public String pesquisarHistorico(ActionEvent evt) {
		return "ok";
	}

	/**
	 * Pesquisar incluir.
	 * 
	 * @param evt
	 *            the evt
	 * @return the string
	 */
	public String pesquisarIncluir(ActionEvent evt) {
		return "ok";
	}

	/**
	 * Pesquisar relacionamento.
	 * 
	 * @param evt
	 *            the evt
	 * @return the string
	 */
	public String pesquisarRelacionamento(ActionEvent evt) {
		return "ok";
	}

	/**
	 * Pesquisar relacionamento historico.
	 * 
	 * @param evt
	 *            the evt
	 * @return the string
	 */
	public String pesquisarRelacionamentoHistorico(ActionEvent evt) {
		return "ok";
	}

	/**
	 * Pesquisar relacionamento incluir.
	 * 
	 * @param evt
	 *            the evt
	 * @return the string
	 */
	public String pesquisarRelacionamentoIncluir(ActionEvent evt) {
		return "ok";
	}

	/**
	 * Pesquisar relacionamento substituir.
	 * 
	 * @param evt
	 *            the evt
	 * @return the string
	 */
	public String pesquisarRelacionamentoSubstituir(ActionEvent evt) {
		return "ok";
	}

	/**
	 * Consultar relacionamento.
	 * 
	 * @return the string
	 */
	public String consultarRelacionamento() {
		try {
			ListarContasRelContaEntradaDTO entrada = new ListarContasRelContaEntradaDTO();

			entrada.setCdFinalidadeContaPagamento(PgitUtil.verificaIntegerNulo(getFinalidadeRelacionamento()));
			entrada.setCdPessoaJuridicaVinculo(PgitUtil.verificaLongNulo(getListaGridPesquisaContas().get(
					getItemSelecionadoListaContas()).getCdPessoaJuridicaVinculo()));
			entrada.setCdTipoContratoVinculo(PgitUtil.verificaIntegerNulo(getListaGridPesquisaContas().get(
					getItemSelecionadoListaContas()).getCdTipoContratoVinculo()));
			entrada.setCdTipoVinculoContrato(PgitUtil.verificaIntegerNulo(getListaGridPesquisaContas().get(
					getItemSelecionadoListaContas()).getCdTipoVinculoContrato()));
			entrada.setNrSequenciaContratoVinculo(PgitUtil.verificaLongNulo(getListaGridPesquisaContas().get(
					getItemSelecionadoListaContas()).getNrSequenciaContratoVinculo()));
			entrada.setCdFinalidadeContaPagamento(PgitUtil.verificaIntegerNulo(getTipoRelacionamento()));
			entrada.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(getCdPessoaJuridica()));
			entrada.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(getCdTipoContratoNegocio()));
			entrada.setNrSequenciaContratoNegocio(PgitUtil.isStringNullLong(getNrSequenciaContratoNegocio()));

			setListaGridPesquisaContasRelacionamentos(getManterContratoImpl().listarContasRelacionadas(entrada));

			this.listaControleRadioContasRelacionamentos = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridPesquisaContasRelacionamentos().size(); i++) {
				this.listaControleRadioContasRelacionamentos.add(new SelectItem(i, " "));
			}

			setItemSelecionadoListaContasRelacionamentos(null);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			setListaGridPesquisaContasRelacionamentos(null);
		}

		return "ok";
	}

	/**
	 * Limpar variaveis relacionado.
	 */
	public void limparVariaveisRelacionado() {
		setClubRelacionadoDesc("");
		setCpfCnpjDesc("");
		setNomeRazaoDesc("");
		setBancoRelacionadaDesc("");
		setAgenciaRelacionadaDesc("");
		setContaRelacionadaDesc("");
		setTipoRelacionadaDesc("");
		setFinalidadeContaRelacionadaDesc("");
		setTipoManutencao("");
	}

	/**
	 * Historico relacionamento.
	 * 
	 * @return the string
	 */
	public String historicoRelacionamento() {
		limparVariaveisRelacionado();
		setListaGridPesquisaContasRelHistorico(null);
		setItemSelecionadoListaContasRelHistorico(null);
		limparDadosRelHistorico();

		return "AVANCAR_HISTORICO_RELACIONAMENTO";
	}

	/**
	 * Incluir relacionamento.
	 * 
	 * @return the string
	 */
	public String incluirRelacionamento() {

		if (!getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getCdTipoDebito()
				.equalsIgnoreCase("SIM")) {
			BradescoFacesUtils.addInfoModalMessage(MessageHelperUtils
					.getI18nMessage("label_conta_contrato_nao_associada"), false);
			return "";
		}

		limparVariaveisRelacionado();
		setListaGridPesquisaContasRelIncluir(null);
		setItemSelecionadoListaContasRelIncluir(null);
		limparDadosRelIncluir();

		return "AVANCAR_INCLUIR_RELACIONAMENTO";
	}

	/**
	 * Excluir relacionamento.
	 * 
	 * @return the string
	 */
	public String excluirRelacionamento() {

		ListarContasRelContaSaidaDTO itemSelecionado =
			getListaGridPesquisaContasRelacionamentos().get(getItemSelecionadoListaContasRelacionamentos());

		setClubRelacionadoDesc(itemSelecionado.getCdPessoa() != null ? itemSelecionado.getCdPessoa().toString() : "");
		setCpfCnpjDesc(itemSelecionado.getCdCpfCnpj());
		setNomeRazaoDesc(itemSelecionado.getDsPessoa());
		setBancoRelacionadaDesc(PgitUtil.formatBanco(itemSelecionado.getCdBanco(), itemSelecionado.getDsBanco(), false));
		setAgenciaRelacionadaDesc(PgitUtil.formatAgencia(itemSelecionado.getCdAgenciaContabil(), itemSelecionado
				.getDsAgenciaContabil(), false));
		setContaRelacionadaDesc(PgitUtil.formatConta(itemSelecionado.getCdConta(), itemSelecionado.getCdDigitoConta(),
				false));
		setTipoContaDesc(itemSelecionado.getDsTipoConta());
		setTipoRelacionamentoDesc(itemSelecionado.getDsTipoRelacionamento());
		setBancoDescFormatado(PgitUtil.concatenarCampos(getListaGridPesquisaContas().get(
				getItemSelecionadoListaContas()).getCdBanco(), getListaGridPesquisaContas().get(
						getItemSelecionadoListaContas()).getDsBanco()));
		setAgenciaDescFormatado(PgitUtil.concatenarCampos(getListaGridPesquisaContas().get(
				getItemSelecionadoListaContas()).getCdAgencia(), getListaGridPesquisaContas().get(
						getItemSelecionadoListaContas()).getDsAgencia()));

		return "AVANCAR_EXCLUIR_RELACIONAMENTO";
	}

	/**
	 * Confirmar excluir relacionamento.
	 * 
	 * @return the string
	 */
	public String confirmarExcluirRelacionamento() {
		try {
			ListarContasRelContaSaidaDTO itemSelecionado =
				getListaGridPesquisaContasRelacionamentos().get(getItemSelecionadoListaContasRelacionamentos());

			ExcluirRelaciAssocEntradaDTO entrada = new ExcluirRelaciAssocEntradaDTO();

			entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

			entrada.setCdPessoaJuridicaVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
					.getCdPessoaJuridicaVinculo());
			entrada.setCdTipoContratoVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
					.getCdTipoContratoVinculo());
			entrada.setNrSequenciaContratoVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
					.getNrSequenciaContratoVinculo());

			entrada.setCdTipoVincContrato(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
					.getCdTipoVinculoContrato());
			entrada.setCdTipoRelacionamentoConta(itemSelecionado.getCdTipoRelacionamento());
			entrada.setCdTipoTesteAuxiliar("");
			entrada.setCdPssoa(itemSelecionado.getCdPessoa());

			ExcluirRelaciAssocSaidaDTO saida = getManterContratoImpl().excluirRelaciAssoc(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
					"relContasManterContrato", BradescoViewExceptionActionType.ACTION, false);

			consultarRelacionamento();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
		}
		return "";
	}

	/**
	 * Avancar rel historico detalhar.
	 * 
	 * @return the string
	 */
	public String avancarRelHistoricoDetalhar() {
		try {
			ListarContasRelContaSaidaDTO registroSelecionadoRelConta = new ListarContasRelContaSaidaDTO();
			ListarContasRelacionadasHistSaidaDTO registroSelecionado =
				getListaGridPesquisaContasRelHistorico().get(getItemSelecionadoListaContasRelHistorico());
			ListarContasVincContSaidaDTO registroSelecionadoConta =
				getListaGridPesquisaContas().get(getItemSelecionadoListaContas());
			if (getItemSelecionadoListaContasRelacionamentos() != null) {
				registroSelecionadoRelConta =
					getListaGridPesquisaContasRelacionamentos().get(getItemSelecionadoListaContasRelacionamentos());
			}

			DetalharContaRelacionadaHistEntradaDTO entrada = new DetalharContaRelacionadaHistEntradaDTO();
			entrada.setHrInclusaoRegistro(registroSelecionado.getHrInclusaoManutencao());

			entrada.setCdPessoaJuridicaNegocio(getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

			entrada.setCdPessoVinc(registroSelecionadoConta.getCdPessoaJuridicaVinculo());
			entrada.setCdTipoContratoVinc(registroSelecionadoConta.getCdTipoContratoVinculo());
			entrada.setNrSeqContratoVinc(registroSelecionadoConta.getNrSequenciaContratoVinculo());

			entrada.setCdTipoVincContrato(registroSelecionadoConta.getCdTipoVinculoContrato());
			entrada.setCdTipoRelacionamentoConta(registroSelecionado.getCdTipoRelacionamento() == null ? 0
					: registroSelecionado.getCdTipoRelacionamento());

			entrada.setCdBancoRelacionamento(registroSelecionado.getCdBancoRelacionamento());
			entrada.setCdAgenciaRelacionamento(registroSelecionado.getCdAgenciaRelacionamento());
			entrada.setCdContaRelacionamento(registroSelecionado.getCdContaRelacionamento());
			entrada.setDigitoContaRelacionamento(registroSelecionado.getCdDigitoContaRelacionamento());
			entrada.setCdTipoConta(registroSelecionado.getCdTipoConta());
			entrada.setCdCorpoCpfCnpjParticipante(registroSelecionado.getCdCorpoCpfCnpjParticipante());
			entrada.setCdFilialCpfCnpjParticipante(registroSelecionado.getCdFilialCpfCnpjParticipante());
			entrada.setCdDigitoCpfCnpjParticipante(registroSelecionado.getCdDigitoCpfCnpjParticipante());
			entrada.setCdPssoa(registroSelecionado.getcPssoa());

			DetalharContaRelacionadaHistSaidaDTO saidaDTO =
				getManterContratoImpl().detalharContaRelacionadaHist(entrada);

			setCpfCnpjDesc(saidaDTO.getCpfCnpjParticipanteFormatado());
			setNomeRazaoDesc(registroSelecionado.getNmParticipante());
			setBancoRelacionadaDesc(registroSelecionado.getBancoFormatado());
			setAgenciaRelacionadaDesc(registroSelecionado.getAgenciaFormatada());
			setContaRelacionadaDesc(registroSelecionado.getContaFormatada());
			setTipoRelacionadaDesc(saidaDTO.getDsTipoContaRelacionamento());
			setTipoManutencao(saidaDTO.getDsTipoManutencao());

			setDataHoraInclusao(FormatarData.formatarDataTrilha(saidaDTO.getHrInclusaoRegistro()));
			setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
			setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
			setComplementoInclusao("0".equals(saidaDTO.getCdOperacaoCanalInclusao()) ? "" : saidaDTO
					.getCdOperacaoCanalInclusao());

			setDataHoraManutencao(FormatarData.formatarDataTrilha(saidaDTO.getHrManutencaoRegistro()));
			setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
			setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
			setComplementoManutencao("0".equals(saidaDTO.getCdOperacaoCanalManutencao()) ? "" : saidaDTO
					.getCdOperacaoCanalManutencao());

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			return "";
		}
		return "AVANCAR_HISTORICO_DETALHAR";
	}

	/**
	 * Substituir relacionamento.
	 * 
	 * @return the string
	 */

	public String substituirRelacionamento() {
		limparVariaveisRelacionado();
		setListaGridPesquisaSubst(null);
		setItemSelecionadoListaSubstituir(null);
		limparDadosSubstituir();

		// Finalidade para "CONTA DO CONTRATO
		setFinalidadeDesc((String) listaFinalidadeContaSelecionadaHash.get(getFinalidadeRelacionamento()));

		// Finalidade para "CONTA A SER RELACIONADA" , (Tipo de Relacionamento ) - Email 01/07/2010
		setFinalidadeContaRelacionadaDesc(getListaGridPesquisaContasRelacionamentos().get(
				getItemSelecionadoListaContasRelacionamentos()).getDsTipoRelacionamento());
		return "AVANCAR_SUBSTITUIR_RELACIONAMENTO";
	}

	/**
	 * Limpar dados substituir.
	 */
	public void limparDadosSubstituir() {
		limparCamposFiltroRadio();
		this.setRadioPesquisa("");
	}

	/**
	 * Consultar substituir.
	 * 
	 * @return the string
	 */
	public String consultarSubstituir() {
		try {
			ListarContasRelacionadasParaContratoEntradaDTO entrada =
				new ListarContasRelacionadasParaContratoEntradaDTO();

			// Dados do Contrato
			entrada.setCdPessoaJuridicaNegocio(getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

			entrada.setCdBanco((!getBanco().equals("")) ? Integer.parseInt(getBanco()) : 0);
			entrada.setCdAgencia((!getAgencia().equals("")) ? Integer.parseInt(getAgencia()) : 0);
			entrada.setCdDigitoAgencia((!getAgenciaDigito().equals("")) ? Integer.parseInt(getAgenciaDigito()) : 0);
			entrada.setCdConta((!getConta().equals("")) ? Long.parseLong(getConta()) : 0);
			entrada.setCdDigitoConta((!getDigito().equals("")) ? getDigito() : "0");
			entrada.setCdTipoConta((getTipoConta() != null) ? getTipoConta() : 0);

			entrada.setCodBancoPrincipal(PgitUtil.verificaIntegerNulo(getCodBancoPrincipal()));
			entrada.setCodAgenciaPrincipal(PgitUtil.verificaIntegerNulo(getCodAgenciaPrincipal()));
			entrada.setCodDigAgenciaPrincipal(PgitUtil.verificaIntegerNulo(getCodDigAgenciaPrincipal()));
			entrada.setCodContaPrincipal(PgitUtil.verificaLongNulo(getCodContaPrincipal()));
			entrada.setCodDigitoContaPrincipal(PgitUtil.verificaStringNula(getCodDigitoContaPrincipal()));

			// Pesquisar por CNPJ
			if (getRadioPesquisa() != null) {
				if (getRadioPesquisa().equals("0")) {
					entrada.setCdCorpoCfpCnpj(Long.parseLong(getCdCpfCnpj()));
					entrada.setCdControleCpfCnpj(Integer.parseInt(getCdFilialCnpj()));
					entrada.setCdDigitoCpfCnpj(Integer.parseInt(getCdControleCnpj()));
				} else { // Pesquisar po CPF
					if (getRadioPesquisa().equals("1")) {
						entrada.setCdCorpoCfpCnpj(Long.parseLong(getCdCpf()));
						entrada.setCdControleCpfCnpj(0);
						entrada.setCdDigitoCpfCnpj(Integer.parseInt(getCdControleCpf()));
					} else {
						entrada.setCdCorpoCfpCnpj(0L);
						entrada.setCdControleCpfCnpj(0);
						entrada.setCdDigitoCpfCnpj(0);
					}
				}
			} else {
				entrada.setCdCorpoCfpCnpj(0L);
				entrada.setCdControleCpfCnpj(0);
				entrada.setCdDigitoCpfCnpj(0);
			}

			entrada.setCdFinalidade(PgitUtil.verificaIntegerNulo(getFinalidade()));

			// setListaGridPesquisaSubstituir(getManterContratoImpl().listarContasVinculadasContrato(entrada));
			setListaGridPesquisaSubst(getManterContratoImpl().listarContasRelacionadasContrato(entrada));

			this.listaControleRadioSubstituir = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridPesquisaSubst().size(); i++) {
				this.listaControleRadioSubstituir.add(new SelectItem(i, " "));
			}

			setItemSelecionadoListaSubstituir(null);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			setListaGridPesquisaSubst(null);

		}
		return "";
	}

	/**
	 * Limpar grid substituir.
	 */
	public void limparGridSubstituir() {
		this.setListaGridPesquisaSubst(null);
		this.setItemSelecionadoListaSubstituir(itemSelecionadoListaSubstituir);
	}

	/**
	 * Selecionar substituir.
	 * 
	 * @return the string
	 */
	public String selecionarSubstituir() {
		ListarContasRelacionadasParaContratoSaidaDTO registroSelecionado =
			getListaGridPesquisaSubst().get(getItemSelecionadoListaSubstituir());

		setCpfCnpjDesc(registroSelecionado.getCdCpfCnpj());
		setNomeRazaoDesc(registroSelecionado.getNomeParticipante());
		setBancoRelacionadaDesc(registroSelecionado.getDsBancoFormatado());
		setAgenciaRelacionadaDesc(registroSelecionado.getDsAgenciaFormatada());

		StringBuffer contaAux = new StringBuffer();
		contaAux.append(registroSelecionado.getCdConta() != null ? registroSelecionado.getCdConta() : "");
		contaAux.append(!registroSelecionado.getCdDigitoConta().equals("") ? "-"
				+ registroSelecionado.getCdDigitoConta() : "");
		setContaRelacionadaDesc(contaAux.toString());

		setTipoRelacionadaDesc(getListaGridPesquisaSubst().get(getItemSelecionadoListaSubstituir()).getDsTipoConta());
		setFinalidadeContaRelacionadaDesc(getListaGridPesquisaSubst().get(getItemSelecionadoListaSubstituir())
				.getDsFinalidade());

		return "AVANCAR_SUBSTITUIR";
	}

	/**
	 * Voltar substituicao.
	 * 
	 * @return the string
	 */
	public String voltarSubstituicao() {
		setItemSelecionadoListaSubstituir(null);
		return "VOLTAR_SUBSTITUIR";
	}

	/**
	 * Limpar campos filtro radio.
	 */
	public void limparCamposFiltroRadio() {
		this.setBanco("");
		this.setBancoFiltro("");
		this.setAgencia("");
		this.setAgenciaDigito("");
		this.setConta("");
		this.setDigito("");
		this.setTipoConta(null);
		this.setFinalidade(null);
		this.setParticipante(null);
		this.setCdCpfCnpj("");
		this.setCdFilialCnpj("");
		this.setCdControleCnpj("");
		this.setCdCpf("");
		this.setCdControleCpf("");
		if ("2".equals(radioPesquisa)) {
			setBanco("237");
		} else {
			setBanco("");
		}
	}

	/**
	 * Confirmar incluir relacionamento.
	 * 
	 * @return the string
	 */
	public String confirmarIncluirRelacionamento() {
		if (isHiddenConfirma()) {
			try {
				// Regras DE-PARA presentes no email : segunda-feira, 9 de
				// agosto de 2010 18:18 - C�pias : Jos� - Silvia
				IncluirRelacContaContEntradaDTO entrada = new IncluirRelacContaContEntradaDTO();

				// Tela de Contrato
				entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
				entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
				entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

				// Passa fixo 21
				entrada.setCdTipoVinculoContrato(21);

				// Adicionado Combo Tipo Relacinamento na tela de inclus�o
				// conforme email 16/09/2011
				entrada.setCdTipoRelacionamentoConta(getCboTipoRelacionamento());

				/* Enviar Zeros */
				entrada.setCdFinalidadeContaPagamento(0);

				/*
				 * S�o os campos do listarContasVinculadasContrato (Primeira
				 * sele��o da conta)
				 */
				entrada.setCdPessoaJuridicaRelacionada(getListaGridPesquisaContas()
						.get(getItemSelecionadoListaContas()).getCdPessoaJuridicaVinculo());
				entrada.setCdTipoContratoRelacionado(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
						.getCdTipoContratoVinculo());
				entrada.setNrSequenciaContratoRelacionado(getListaGridPesquisaContas().get(
						getItemSelecionadoListaContas()).getNrSequenciaContratoVinculo());

				// Lista de Ocorr�ncias para Conta - V�rios Registros podem ser
				// inclu�dos
				IncluirRelacContaOcorrenciasDTO ocorrencia;
				listaOcorrenciasIncluirRel = new ArrayList<IncluirRelacContaOcorrenciasDTO>();

				ListarContasRelacionadasParaContratoSaidaDTO itemSelecionado = getListaGridPesquisaContasRelInc().get(
						itemSelecionadoListaContasRelIncluir);
				
				entrada.setCdTipoInscricao(itemSelecionado.getCdTipoIncricao());
				entrada.setCpfCNPJ(itemSelecionado.getCdCpfCnpj());
				entrada.setCdPssoa(itemSelecionado.getCdParticipante());
				
				ocorrencia = new IncluirRelacContaOcorrenciasDTO();
				ocorrencia.setCdPessoaJuridicaVinculo(PgitUtil.verificaLongNulo(itemSelecionado
						.getCdPessoaJuridicaVinculo()));
				ocorrencia.setCdTipoContratoVinculo(PgitUtil.verificaIntegerNulo(itemSelecionado
						.getCdTipoContratoVinculo()));
				ocorrencia.setNrSequenciaContratoVinculo(PgitUtil.verificaLongNulo(itemSelecionado
						.getNrSequenciaContratoVinculo()));

				listaOcorrenciasIncluirRel.add(ocorrencia);

				entrada.setOcorrenciasContas(getListaOcorrenciasIncluirRel());

				IncluirRelacContaContSaidaDTO saida = getManterContratoImpl().incluirRelacionamentoContaContrato(
						entrada);
				BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
						"relContasManterContrato", BradescoViewExceptionActionType.ACTION, false);
				consultarRelacionamento();

			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
						false);
				return null;
			}
		}
		return "";
	}

	/**
	 * Selecionar rel incluir.
	 * 
	 * @return the string
	 */
	public String selecionarRelIncluir() {
		// Limpa Lista
		ListarContasRelacionadasParaContratoSaidaDTO itemSelecionado =
			getListaGridPesquisaContasRelInc().get(getItemSelecionadoListaContasRelIncluir());

		listaOcorrenciasIncluirRelAux2 = new ArrayList<ListarContasRelacionadasParaContratoSaidaDTO>();
		getListaOcorrenciasIncluirRelAux2().add(itemSelecionado);

		setClubRelacionadoDesc(itemSelecionado.getCdParticipante() != null ? itemSelecionado.getCdParticipante().toString() : "");
		setCpfCnpjDesc(itemSelecionado.getCdCpfCnpj());
		setCdTipoInscricao(itemSelecionado.getCdTipoIncricao() != null ? itemSelecionado.getCdTipoIncricao() : "");
		setNomeRazaoDesc(itemSelecionado.getNomeParticipante());
		setBancoRelacionadaDesc(itemSelecionado.getDsBanco());
		setAgenciaRelacionadaDesc(itemSelecionado.getDsAgencia());
		setContaRelacionadaDesc(PgitUtil.concatenarCampos(itemSelecionado.getCdConta(), itemSelecionado
				.getCdDigitoConta()));
		setTipoRelacionadaDesc(itemSelecionado.getDsTipoConta());
		setFinalidadeContaRelacionadaDesc(itemSelecionado.getDsFinalidade());

		carregaComboTipoRelacionamento();
		return "AVANCAR_INC2";
	}

	/**
	 * Confirmar substituir.
	 * 
	 * @return the string
	 */
	public String confirmarSubstituir() {
		if (isHiddenConfirma()) {
			try {
				// Regras DE-PARA presentes no email : segunda-feira, 9 de agosto de 2010 18:18 - C�pias : Jos� - Silvia

				SubsRelacContaContratoEntradaDTO entrada = new SubsRelacContaContratoEntradaDTO();

				// Tela de Contrato
				entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
				entrada.setNrSequencialContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));
				entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());

				// Valor Fixo 021
				entrada.setCdTipoVinculoContrato(getListaGridPesquisaSubst().get(getItemSelecionadoListaSubstituir())
						.getCdTipoVinculoContrato());

				entrada.setCdTipoRelacionamentoConta(getListaGridPesquisaContasRelacionamentos().get(
						getItemSelecionadoListaContasRelacionamentos()).getCdTipoRelacionamento());
				entrada.setCdFinalidadeContaPagamento(getListaGridPesquisaContasRelacionamentos().get(
						getItemSelecionadoListaContasRelacionamentos()).getCdTipoRelacionamento());

				/*
				 * Os campos s�o a chave da conta do registro selecionado na lista gerada ap�s pressionado o bot�o de
				 * substituir.
				 */
				entrada.setCdPessoaJuridicaRelacionada(getListaGridPesquisaSubst().get(
						getItemSelecionadoListaSubstituir()).getCdPessoaJuridicaVinculo());
				entrada.setCdTipoContratoRelacionado(getListaGridPesquisaSubst().get(
						getItemSelecionadoListaSubstituir()).getCdTipoContratoVinculo());
				entrada.setNrSequenciaContratoRelacionado(getListaGridPesquisaSubst().get(
						getItemSelecionadoListaSubstituir()).getNrSequenciaContratoVinculo());
				entrada.setCdTipoInscricao(getListaGridPesquisaSubst().get(getItemSelecionadoListaSubstituir()).getCdTipoIncricao());
				entrada.setCpfCNPJ(getListaGridPesquisaSubst().get(getItemSelecionadoListaSubstituir()).getCdCpfCnpj());
				entrada.setCdPssoa(getListaGridPesquisaSubst().get(getItemSelecionadoListaSubstituir()).getCdParticipante());
				
				

				// Apenas 1 Ocorr�ncia de Substitui��o
				SubsRelacContaOcorrenciasDTO ocorrencia = new SubsRelacContaOcorrenciasDTO();

				/*
				 * Quando � selecionado um valor diferente de "d�bito" no combo, o valor � dos campos da conta
				 * selecionada na ListaRelacionamentoContas Quando o valor � igual a "Debito", s�o os campos do
				 * listarContasVinculadasContrato (Primeira sele��o da conta)
				 */
				if (getFinalidadeRelacionamento() != null && getFinalidadeRelacionamento() != 1) {
					ocorrencia.setCdPessoaJuridicaVinculo(getListaGridPesquisaContasRelacionamentos().get(
							getItemSelecionadoListaContasRelacionamentos()).getCdPessoaJuridicaRelacionada());
					ocorrencia.setNrSequenciaContratoVinculo(getListaGridPesquisaContasRelacionamentos().get(
							getItemSelecionadoListaContasRelacionamentos()).getNrSequenciaContratoRelacionada());
					ocorrencia.setCdTipoContratoVinculo(getListaGridPesquisaContasRelacionamentos().get(
							getItemSelecionadoListaContasRelacionamentos()).getCdTipoContratoRelacionada());
				} else {
					ocorrencia.setCdPessoaJuridicaVinculo(getListaGridPesquisaContas().get(
							getItemSelecionadoListaContas()).getCdPessoaJuridicaVinculo());
					ocorrencia.setNrSequenciaContratoVinculo(getListaGridPesquisaContas().get(
							getItemSelecionadoListaContas()).getNrSequenciaContratoVinculo());
					ocorrencia.setCdTipoContratoVinculo(getListaGridPesquisaContas().get(
							getItemSelecionadoListaContas()).getCdTipoContratoVinculo());
				}

				entrada.setOcorrencia(ocorrencia);

				SubsRelacContaContratoSaidaDTO saida =
					getManterContratoImpl().substituirRelacionamentoContaContrato(entrada);
				BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
						"relContasManterContrato", BradescoViewExceptionActionType.ACTION, false);
				consultarRelacionamento();

			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
						false);
				return null;
			}
		}
		return "";
	}

	/**
	 * Limpar grid rel historico.
	 */
	public void limparGridRelHistorico() {
		this.setListaGridPesquisaContasRelHistorico(null);
		this.setItemSelecionadoListaContasRelHistorico(null);
	}

	/**
	 * Limpar grid rel incluir.
	 */
	public void limparGridRelIncluir() {
		this.setListaGridPesquisaContasRelIncluir(null);
		this.setListaGridPesquisaContasRelInc(null);
		this.setItemSelecionadoListaContasRelIncluir(null);
	}

	/**
	 * Limpar grid incluir.
	 */
	public void limparGridIncluir() {
		this.setListaGridPesquisaContasIncluir(null);
		this.setItemSelecionadoListaContasIncluir(null);
	}

	/**
	 * limparArgumentosPesquisa
	 */
	public void limparArgumentosPesquisa() {
		setCdCpfCnpj(null);
		setCdFilialCnpj(null);
		setCdControleCnpj(null);

		setCdCpf(null);
		setCdControleCpf(null);

		setBanco(null);
		setAgencia(null);
		setConta(null);
	}

	/**
	 * Voltar pesquisa relacionamento.
	 * 
	 * @return the string
	 */
	public String voltarPesquisaRelacionamento() {
		setItemSelecionadoListaContasRelacionamentos(null);
		return "VOLTAR_RELACIONAMENTO";
	}

	/**
	 * Limpar dados rel historico.
	 */
	public void limparDadosRelHistorico() {
		limparCamposFiltroRadio();
		setRadioPesquisa("");
		limparGridRelHistorico();
		setDataManutencaoInicio(new Date());
		setDataManutencaoFim(new Date());
		setCdTipoRelacionamentoHistorico(null);
	}

	/**
	 * Limpar dados rel incluir.
	 */
	public void limparDadosRelIncluir() {
		setRadioPesquisa(null);
		limparCamposFiltroRadio();
		limparGridRelIncluir();
		setHabilitaSelecionarIncluirRelacionamento(false);
	}

	/**
	 * Limpar dados incluir.
	 */
	public void limparDadosIncluir() {
		setRadioPesquisa(null);
		limparCamposFiltroRadio();
		limparGridIncluir();
		setRadioPesquisa("");
	}

	/**
	 * Voltar relacionamento historico.
	 * 
	 * @return the string
	 */
	public String voltarRelacionamentoHistorico() {
		setItemSelecionadoListaContasRelHistorico(null);
		return "VOLTAR_HISTORICO";
	}

	/**
	 * Voltar relacionamento incluir.
	 * 
	 * @return the string
	 */
	public String voltarRelacionamentoIncluir() {
		setItemSelecionadoListaContasRelIncluir(null);
		return "VOLTAR_INCLUIR";
	}

	/**
	 * Consultar rel historico.
	 * 
	 * @return the string
	 */
	public String consultarRelHistorico() {
		try {
			ListarContasVincContSaidaDTO registroSelecionado =
				getListaGridPesquisaContas().get(getItemSelecionadoListaContas());
			ListarContasRelacionadasHistEntradaDTO entrada = new ListarContasRelacionadasHistEntradaDTO();

			// Dados do Contrato
			entrada.setCdPessoaJuridicaNegocio(getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

			entrada.setCdPessoaJuridicaVinculo(registroSelecionado.getCdPessoaJuridicaVinculo());
			entrada.setCdTipoContratoVinculo(registroSelecionado.getCdTipoContratoVinculo());
			entrada.setNrSequenciaContratoVinculo(registroSelecionado.getNrSequenciaContratoVinculo());

			entrada.setCdTipoVinculoContrato(registroSelecionado.getCdTipoVinculoContrato());
			entrada.setCdTipoRelacionamentoConta(getCdTipoRelacionamentoHistorico());

			entrada.setCdBancoRelacionamento((getBanco() != null && !getBanco().equals("")) ? Integer
					.parseInt(getBanco()) : 0);
			entrada.setCdAgenciaRelacionamento((getAgencia() != null && !getAgencia().equals("")) ? Integer
					.parseInt(getAgencia()) : 0);
			entrada.setCdContaRelacionamento((getConta() != null && !getConta().equals("")) ? Long
					.parseLong(getConta()) : 0);
			entrada
			.setCdDigitoContaRelacionamento((getDigito() != null && !getDigito().equals("")) ? getDigito() : "0");

			entrada.setDtInicioManutencao(PgitUtil.converterData(getDataManutencaoInicio()));
			entrada.setDtFimManutencao(PgitUtil.converterData(getDataManutencaoFim()));

			setListaGridPesquisaContasRelHistorico(getManterContratoImpl().listarContasRelacionadasHist((entrada)));

			setListaControleRadioContasRelHistorico(new ArrayList<SelectItem>());

			for (int i = 0; i < getListaGridPesquisaContasRelHistorico().size(); i++) {
				getListaControleRadioContasRelHistorico().add(new SelectItem(i, " "));
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			setListaGridPesquisaContasRelHistorico(null);
		}

		return "";
	}

	/**
	 * Consultar rel incluir.
	 * 
	 * @return the string
	 */
	public String consultarRelIncluir() {
		try {
			ListarContasRelacionadasParaContratoEntradaDTO entrada =
				new ListarContasRelacionadasParaContratoEntradaDTO();

			// Dados do Contrato
			entrada.setCdPessoaJuridicaNegocio(getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

			entrada.setCdBanco((!getBanco().equals("")) ? Integer.parseInt(getBanco()) : 0);
			entrada.setCdAgencia((!getAgencia().equals("")) ? Integer.parseInt(getAgencia()) : 0);
			entrada.setCdConta((!getConta().equals("")) ? Long.parseLong(getConta()) : 0);
			entrada.setCdDigitoConta((!getDigito().equals("")) ? getDigito() : "0");
			entrada.setCdDigitoAgencia((!getAgenciaDigito().equals("")) ? Integer.parseInt(getAgenciaDigito()) : 0);

			entrada.setCodBancoPrincipal(PgitUtil.verificaIntegerNulo(getCodBancoPrincipal()));
			entrada.setCodAgenciaPrincipal(PgitUtil.verificaIntegerNulo(getCodAgenciaPrincipal()));
			entrada.setCodDigAgenciaPrincipal(PgitUtil.verificaIntegerNulo(getCodDigAgenciaPrincipal()));
			entrada.setCodContaPrincipal(PgitUtil.verificaLongNulo(getCodContaPrincipal()));
			entrada.setCodDigitoContaPrincipal(PgitUtil.verificaStringNula(getCodDigitoContaPrincipal()));

			// Informado por Email, combo finalidade foi adicionado na tela 14/09/2011
			entrada.setCdFinalidade(getFinalidade() != null ? getFinalidade() : 0);

			entrada.setCdTipoConta((getTipoConta() != null) ? getTipoConta() : 0);

			// Pesquisar por CNPJ
			if (getRadioPesquisa() != null) {
				if (getRadioPesquisa().equals("0")) {
					entrada.setCdCorpoCfpCnpj(Long.parseLong(getCdCpfCnpj()));
					entrada.setCdControleCpfCnpj(Integer.parseInt(getCdFilialCnpj()));
					entrada.setCdDigitoCpfCnpj(Integer.parseInt(getCdControleCnpj()));
				} else { // Pesquisar po CPF
					if (getRadioPesquisa().equals("1")) {
						entrada.setCdCorpoCfpCnpj(Long.parseLong(getCdCpf()));
						entrada.setCdControleCpfCnpj(0);
						entrada.setCdDigitoCpfCnpj(Integer.parseInt(getCdControleCpf()));
					} else {
						entrada.setCdCorpoCfpCnpj(0L);
						entrada.setCdControleCpfCnpj(0);
						entrada.setCdDigitoCpfCnpj(0);
					}
				}
			} else {
				entrada.setCdCorpoCfpCnpj(0L);
				entrada.setCdControleCpfCnpj(0);
				entrada.setCdDigitoCpfCnpj(0);
			}

			// setListaGridPesquisaContasRelIncluir(getManterContratoImpl().listarContasVinculadasContrato(entrada));

			setListaGridPesquisaContasRelInc(getManterContratoImpl().listarContasRelacionadasContrato(entrada));

			this.listaControleRadioContasRelIncluir = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridPesquisaContasRelInc().size(); i++) {
				this.listaControleRadioContasRelIncluir.add(new SelectItem(i, " "));
			}
			setItemSelecionadoListaContasRelIncluir(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			setListaGridPesquisaContasRelInc(null);
			setItemSelecionadoListaContasRelIncluir(null);
		}

		return "";
	}

	/**
	 * Consultar incluir.
	 */
	public void consultarIncluir() {
		try {
			ListarContasPossiveisInclusaoEntradaDTO entrada = new ListarContasPossiveisInclusaoEntradaDTO();

			entrada.setCdBanco(PgitUtil.verificaStringToIntegerNula(getBanco()));
			entrada.setCdAgencia(PgitUtil.verificaStringToIntegerNula(getAgencia()));
			entrada.setCdConta(PgitUtil.isStringNullLong(getConta()));
			entrada.setCdDigitoAgencia(getAgenciaDigito() == null || "".equals(getAgenciaDigito()) ? 0 :PgitUtil.verificaStringToIntegerNula(getAgenciaDigito()));
			entrada.setCdTipoConta(PgitUtil.verificaIntegerNulo(getTipoConta()));

			// Pesquisar por CNPJ
			if (getRadioPesquisa().equals("0")) {
				entrada.setCdCorpoCpfCnpj(Long.parseLong(getCdCpfCnpj()));
				entrada.setCdFilialCpfCnpj(Integer.parseInt(getCdFilialCnpj()));
				entrada.setCdDigitoCpfCnpj(Integer.parseInt(getCdControleCnpj()));
			} else { // Pesquisar po CPF
				if (getRadioPesquisa().equals("1")) {
					entrada.setCdCorpoCpfCnpj(Long.parseLong(getCdCpf()));
					entrada.setCdFilialCpfCnpj(0);
					entrada.setCdDigitoCpfCnpj(Integer.parseInt(getCdControleCpf()));
				} else {
					entrada.setCdCorpoCpfCnpj(0L);
					entrada.setCdFilialCpfCnpj(0);
					entrada.setCdDigitoCpfCnpj(0);
				}
			}

			// Atributos vindos da tela de Contrato
			entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica() != null ? getCdPessoaJuridica() : 0);
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio() != null ? getCdTipoContratoNegocio() : 0);
			entrada.setNrSequenciaContratoNegocio(getNrSequenciaContratoNegocio() != null ? Long
					.parseLong(getNrSequenciaContratoNegocio()) : 0);

			setListaGridPesquisaContasIncluir(getManterContratoImpl().listarContasPossiveisInclusao(entrada));

			this.listaControleRadioContasIncluir = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridPesquisaContasIncluir().size(); i++) {
				this.listaControleRadioContasIncluir.add(new SelectItem(i, " "));
			}

			setItemSelecionadoListaContasIncluir(null);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			setListaGridPesquisaSubst(null);

		}

	}

	/**
	 * Confirmar incluir.
	 * 
	 * @return the string
	 */
	public String confirmarIncluir() {
		if (isHiddenConfirma()) {
			try {
				IncluirContaContratoEntradaDTO entrada = new IncluirContaContratoEntradaDTO();

				// Regras de passagem de campos - Email segunda-feira, 12 de julho de 2010 19:21

				// Atributos vindos da tela de Contrato
				entrada.setCdPessoaJuridica(getCdPessoaJuridica() != null ? getCdPessoaJuridica() : 0);
				entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio() != null ? getCdTipoContratoNegocio() : 0);
				entrada.setNrSequenciaContratoNegocio(getNrSequenciaContratoNegocio() != null ? Long
						.parseLong(getNrSequenciaContratoNegocio()) : 0);

				// Atributos vindos da Lista de Incluir - ListarContasPossiveisInclus�o
				ListarContasPossiveisInclusaoSaidaDTO contaAux =
					getListaGridPesquisaContasIncluir().get(getItemSelecionadoListaContasIncluir());

				entrada.setCdPessoaJuridicaVinculo(contaAux.getCdPessoaJuridicaVinculo() != null ? contaAux
						.getCdPessoaJuridicaVinculo() : 0);
				entrada.setCdTipoContratoNegocioVinculo(contaAux.getCdTipoContratoVinculo() != null ? contaAux
						.getCdTipoContratoVinculo() : 0);
				entrada.setNrSequenciaContratoNegocioVinculo(contaAux.getNrSequenciaContratoVinculo() != null
						? contaAux.getNrSequenciaContratoVinculo() : 0);

				entrada.setCdTipoVinculoContrato(21);

				IncluirContaContratoSaidaDTO saida = getManterContratoImpl().incluirContaContrato(entrada);

				BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
						"incContasManterContrato", BradescoViewExceptionActionType.ACTION, false);

				consultarIncluir();

			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
						false);
				return null;
			}
		}
		return "";
	}

	/**
	 * Avancar alterar.
	 * 
	 * @return the string
	 */
	public String avancarAlterar() {
		setUnidadeFederativaDesc((String) listaUnidadeFederativaHash.get(getUnidadeFederativa()));
		setMunicipioDesc((String) listaMunicipioHash.get(getMunicipio()));

		// Preenche Grid com as finalidades selecionadas
		listaFinalidadesSelecionadas = new ArrayList<FinalidadeSaidaDTO>();
		for (int i = 0; i < listaFinalidades.size(); i++) {
			if (listaFinalidades.get(i).isCheck()) {
				listaFinalidadesSelecionadas.add(listaFinalidades.get(i));
			}
		}

		return "AVANCAR_ALTERAR";
	}

	/**
	 * Preencher dados.
	 */
	public void preencherDados() {

		ConsultarContaContratoEntradaDTO entrada = new ConsultarContaContratoEntradaDTO();

		entrada.setCdPessoaJuridicaVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
				.getCdPessoaJuridicaVinculo());
		entrada.setCdTipoContratoVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
				.getCdTipoContratoVinculo());
		entrada.setCdTipoVinculoContrato(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
				.getCdTipoVinculoContrato());
		entrada.setNrSequenciaContratoVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
				.getNrSequenciaContratoVinculo());

		entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
		entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

		ConsultarContaContratoSaidaDTO saidaDTO = getManterContratoImpl().consultarContaContrato(entrada);

		// Grid
		setListaFinalidades(saidaDTO.getListaFinalidade());

		this.listaControleCheckFinalidades = new ArrayList<SelectItem>();

		for (int i = 0; i < getListaFinalidades().size(); i++) {
			this.listaControleCheckFinalidades.add(new SelectItem(i, " "));
		}

		this.listaFinalidadeContaSelecionadaHash.clear();
		listaFinalidadeContaSelecionada.clear();

		// Preenche Combo com Finalidades da Conta
		// for (int i = 0; i < saidaDTO.getListaFinalidade().size(); i++) {
		// listaFinalidadeContaSelecionadaHash.put(saidaDTO.getListaFinalidade().get(i).getCdFinalidade(),
		// saidaDTO.getListaFinalidade().get(i)
		// .getDsFinalidade());
		// listaFinalidadeContaSelecionada.add(new SelectItem(saidaDTO.getListaFinalidade().get(i).getCdFinalidade(),
		// saidaDTO.getListaFinalidade()
		// .get(i).getDsFinalidade()));
		// }

		setCodBancoPrincipal(PgitUtil.verificaIntegerNulo(getListaGridPesquisaContas().get(
				getItemSelecionadoListaContas()).getCdBanco()));
		setCodAgenciaPrincipal(PgitUtil.verificaIntegerNulo(getListaGridPesquisaContas().get(
				getItemSelecionadoListaContas()).getCdAgencia()));
		setCodDigAgenciaPrincipal(PgitUtil.verificaIntegerNulo(getListaGridPesquisaContas().get(
				getItemSelecionadoListaContas()).getCdDigitoAgencia()));
		setCodContaPrincipal(PgitUtil.verificaLongNulo(getListaGridPesquisaContas()
				.get(getItemSelecionadoListaContas()).getCdConta()));
		setCodDigitoContaPrincipal(PgitUtil.verificaStringNula(getListaGridPesquisaContas().get(
				getItemSelecionadoListaContas()).getCdDigitoConta()));

		setBancoDesc(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getDsBanco());
		setCodBanco(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getCdBanco());
		setCdAgencia(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getCdAgencia());
		setAgenciaDesc(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getDsAgencia());
		setFinalidadeDescSub(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getDsFinalidade());
		setContaDesc(Long.toString(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getCdConta())
				+ "-" + getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getCdDigitoConta());
		setTipoDesc(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getDsTipoConta());
		setCpf(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getCdCpfCnpj());
		setRazao(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getNomeParticipante());
		setSituacaoVinculoDesc(saidaDTO.getDsSituacaoVinculacaoConta());
		setDataVinculacaoDesc(saidaDTO.getHrInclusaoRegistro());
		setDataHoraInclusao(saidaDTO.getHrInclusaoRegistro());
		setTipoCanalInclusao((saidaDTO.getCdTipoCanalInclusao() != 0) ? Integer.toString(saidaDTO
				.getCdTipoCanalInclusao())
				+ " - " + saidaDTO.getDsTipoCanalInclusao() : "");
		setComplementoInclusao("0".equals(saidaDTO.getCdOperacaoCanalInclusao()) ? "" : saidaDTO
				.getCdOperacaoCanalInclusao());

		setDataHoraManutencao(saidaDTO.getHrManutencaoRegistro());

		if (!"".equals(saidaDTO.getCdUsuarioInclusao())) {
			setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
		} else {
			setUsuarioInclusao(saidaDTO.getCdUsuarioInclusaoExterno());
		}

		if (!"".equals(saidaDTO.getCdUsuarioManutencao())) {
			setUsuarioManutencao(saidaDTO.getCdUsuarioManutencao());
		} else {
			setUsuarioManutencao(saidaDTO.getCdUsuarioManutencaoExterno());
		}

		setTipoCanalManutencao(saidaDTO.getCdTipoCanalManutencao() != 0 ? Integer.toString(saidaDTO
				.getCdTipoCanalManutencao())
				+ " - " + saidaDTO.getDsTipoCanalManutencao() : "");
		setComplementoManutencao("0".equals(saidaDTO.getCdOperacaoCanalManutencao()) ? "" : saidaDTO
				.getCdOperacaoCanalManutencao());

		setMunicipioDesc(saidaDTO.getDsMunicipio());
		setUnidadeFederativaDesc(saidaDTO.getCdSiglaUf());
		setMunicipioFeriadoLocalDesc(saidaDTO.getCdMunicipioFeriadoLocal());
		setCdMotivoSituacaoConta(saidaDTO.getCdMotivoSituacaoConta());
		setCdSituacaoVinculacaoConta(saidaDTO.getCdSituacaoVinculacaoConta());

		setUnidadeFederativa(saidaDTO.getCdUf());

		setMunicipio(saidaDTO.getCdMunicipio());
		setRadioMunicipioFeriadoLocal(Integer.toString(saidaDTO.getCdMunicipioFeriadoLocal()));
		setDescApelido(saidaDTO.getDescApelido());

		setValorLimite(saidaDTO.getVlAdicionalContratoConta());
		setDataPrazoInicio(FormatarData.formataDiaMesAnoFromPdc(saidaDTO.getDtInicioValorAdicional()));
		setDataPrazoFim(FormatarData.formataDiaMesAnoFromPdc(saidaDTO.getDtFimValorAdicional()));
		setValorLimiteTED(saidaDTO.getVlAdicContrCtaTed());
		setDataPrazoInicioTED(FormatarData.formataDiaMesAnoFromPdc(saidaDTO.getDtIniVlrAdicionalTed()));
		setDataPrazoFimTED(FormatarData.formataDiaMesAnoFromPdc(saidaDTO.getDtFimVlrAdicionalTed()));

		setPrazoValidadeTEDAltDesc(PgitUtil.concatenarCampos(FormatarData.formatarData(saidaDTO
				.getDtIniVlrAdicionalTed()), FormatarData.formatarData(saidaDTO.getDtFimVlrAdicionalTed())));
		setPrazoValidadeAltDesc(PgitUtil.concatenarCampos(FormatarData.formatarData(saidaDTO
				.getDtInicioValorAdicional()), FormatarData.formatarData(saidaDTO.getDtFimValorAdicional())));

	}

	/**
	 * Voltar alterar.
	 * 
	 * @return the string
	 */
	public String voltarAlterar() {
		return "VOLTAR_ALTERAR";
	}

	/**
	 * Confirmar alterar.
	 * 
	 * @return the string
	 */
	public String confirmarAlterar() {
		if (isHiddenConfirma()) {
			try {
				AlterarConfContaContEntradaDTO entrada = new AlterarConfContaContEntradaDTO();

				entrada.setCdMunicipioFeriadoLocal(PgitUtil.verificaStringNula(getRadioMunicipioFeriadoLocal()).equals(
						"") ? 0 : Integer.parseInt(getRadioMunicipioFeriadoLocal()));
				if (isHabilitaMunicipioAlterar()) {
					entrada.setCdMunicipio(getMunicipio());
				} else {

					/*
					 * - Quando a op��o �Municipio de Agencia da Conta D�bito� estiver selecionado, os combos de UF e
					 * Municipio devem estar desabilitados e a tela deve passar zeros para esses campos. - Se a
					 * finalidade D�bito n�o foi selecionada tamb�m passamos 0 (zero)
					 */
					entrada.setCdMunicipio(0L);
				}

				entrada.setCdMotivoSituacaoConta(getCdMotivoSituacaoConta());
				entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
				entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
				entrada.setNrSequenciaContratoNegocio(PgitUtil.verificaStringNula(getNrSequenciaContratoNegocio())
						.equals("") ? 0l : Long.parseLong(getNrSequenciaContratoNegocio()));

				entrada.setCdPessoaJuridicaVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
						.getCdPessoaJuridicaVinculo());
				entrada.setCdSituacaoVinculacaoConta(getCdSituacaoVinculacaoConta());
				entrada.setCdTipoContratoNegocioVinculo(getListaGridPesquisaContas().get(
						getItemSelecionadoListaContas()).getCdTipoContratoVinculo());
				entrada.setCdTipoVinculoContrato(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
						.getCdTipoVinculoContrato());
				entrada.setNrSequenciaContratoVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
						.getNrSequenciaContratoVinculo());
				entrada.setDescApelido(getDescApelido());

				List<FinalidadeAlterarContaEntradaDTO> lista = new ArrayList<FinalidadeAlterarContaEntradaDTO>();
				FinalidadeAlterarContaEntradaDTO finalidadeAlterarContaEntradaDTO;

				for (int i = 0; i < getListaFinalidades().size(); i++) {
					finalidadeAlterarContaEntradaDTO = new FinalidadeAlterarContaEntradaDTO();
					finalidadeAlterarContaEntradaDTO.setCdFinalidadeContaPagamento(getListaFinalidades().get(i)
							.getCdFinalidade());
					finalidadeAlterarContaEntradaDTO.setCdTipoAcaoFinalidade(getListaFinalidades().get(i).isCheck()
							? "S" : "N");
					lista.add(finalidadeAlterarContaEntradaDTO);
				}

				entrada.setListaFinalidade(lista);

				AlterarConfContaContSaidaDTO saidaDTO =
					getManterContratoImpl().alterarConfiguracaoContraContrato(entrada);

				BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
						"conContasManterContrato", BradescoViewExceptionActionType.ACTION, false);

				listarContasVinculadasContrato();

			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
						false);
				return null;
			}
		}
		return "";
	}

	/**
	 * Limpar dados historico.
	 * 
	 * @return the string
	 */
	public String limparDadosHistorico() {
		setAgencia("");
		setBanco("");
		setAgencia("");
		setConta("");
		setAgenciaDigito("");
		setDigito("");
		setTipoConta(null);
		setDataManutencaoFim(new Date());
		setDataManutencaoInicio(new Date());
		setHabilitarHistorico(true);
		setHabilitarConta(true);
		limparListaHist();
		return "";
	}

	/**
	 * Limpar lista hist.
	 * 
	 * @return the string
	 */
	public String limparListaHist() {

		this.setListaGridPesquisaHistorico(null);
		this.setItemSelecionadoListaHistorico(null);

		return "";
	}

	/**
	 * Consultar historico.
	 * 
	 * @return the string
	 */
	public String consultarHistorico() {
		try {
			ListarConManContaVinculadaEntradaDTO entrada = new ListarConManContaVinculadaEntradaDTO();

			entrada.setCdAgencia((!getAgencia().equals("")) ? Integer.parseInt(getAgencia()) : 0);
			entrada.setCdBanco((!getBanco().equals("")) ? Integer.parseInt(getBanco()) : 0);
			entrada.setCdConta((!getConta().equals("")) ? Long.parseLong(getConta()) : 0);
			entrada.setCdDigitoAgencia((!getAgenciaDigito().equals("")) ? Integer.parseInt(getAgenciaDigito()) : 0);
			entrada.setCdDigitoConta((!getDigito().equals("")) ? getDigito() : "0");

			entrada.setCdPessoaJuridicaNegocio(getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			entrada.setDtFim((getDataManutencaoFim() == null) ? "" : sdf.format(getDataManutencaoFim()));
			entrada.setDtInicio((getDataManutencaoInicio() == null) ? "" : sdf.format(getDataManutencaoInicio()));

			setListaGridPesquisaHistorico(getManterContratoImpl().listarConManContaVinculadaHistorico(entrada));

			this.listaControleRadioHistorico = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridPesquisaHistorico().size(); i++) {
				this.listaControleRadioHistorico.add(new SelectItem(i, " "));
			}

			setItemSelecionadoListaHistorico(null);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			setListaGridPesquisaHistorico(null);
		}

		return "";
	}

	/**
	 * Avancar historico detalhar.
	 * 
	 * @return the string
	 */
	public String avancarHistoricoDetalhar() {
		try {
			ConsultarConManContaEntradaDTO entrada = new ConsultarConManContaEntradaDTO();
			entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

			ListarConManContaVinculadaSaidaDTO registroSelecionado =
				getListaGridPesquisaHistorico().get(getItemSelecionadoListaHistorico());

			entrada.setCdPessoaJuridicaConta(registroSelecionado.getCdPessoaJuridicaVinculo() != null
					? registroSelecionado.getCdPessoaJuridicaVinculo() : 0);
			entrada.setCdTipoContratoConta(registroSelecionado.getCdTipoContratoVinculo() != null ? registroSelecionado
					.getCdTipoContratoVinculo() : 0);
			entrada.setHrInclusaoRegistro(registroSelecionado.getHrInclusaoRegistroHistorico());
			entrada.setNrSequenciaContratoConta(registroSelecionado.getNrSequenciaContratoVinculo() != null
					? registroSelecionado.getNrSequenciaContratoVinculo() : 0);

			entrada.setCdTipoVinculoContrato(21);
			setCpf(registroSelecionado.getCdCpfCnpj());
			setRazao(registroSelecionado.getDsNomeParticipante());

			ConsultarConManContaSaidaDTO saidaDTO = getManterContratoImpl().consultarConManContaHistorico(entrada);

			setBancoDesc(saidaDTO.getDsBanco());
			setAgenciaDesc(saidaDTO.getDsComercialAgenciaContabil());
			setContaDesc(saidaDTO.getCdNumeroContaBancaria() + "-" + saidaDTO.getCdDigitoContaBancaria());
			setTipoDesc(saidaDTO.getDsTipoConta());
			setSituacaoVinculoDesc(saidaDTO.getDsSituacaoVinculo());
			setDataVinculacaoDesc(saidaDTO.getHrInclusaoRegistro());
			setTpManutencao(saidaDTO.getDsIndicadorTipoManutencao());

			setDsMunicipio(saidaDTO.getDsMunicipio());
			setDsUnidFede(saidaDTO.getDsUnidadeFederativa());
			setCdMunicipioFeri(saidaDTO.getCdMunicipioFeri());

			setValorLimite(saidaDTO.getValorAdicionadoContratoConta());
			setDataPrazoInicio(FormatarData.formataDiaMesAnoFromPdc(saidaDTO.getDtInicialValorAdicionado()));
			setDataPrazoFim(FormatarData.formataDiaMesAnoFromPdc(saidaDTO.getDtFinalValorAdicionado()));
			setValorLimiteTED(saidaDTO.getVlAdicionalTed());
			setDataPrazoInicioTED(FormatarData.formataDiaMesAnoFromPdc(saidaDTO.getDtValidadeInicialTed()));
			setDataPrazoFimTED(FormatarData.formataDiaMesAnoFromPdc(saidaDTO.getDtValidadeFinalTed()));

			setPrazoValidadeTEDAltDesc(PgitUtil.concatenarCampos(FormatarData.formatarData(saidaDTO
					.getDtValidadeInicialTed()), FormatarData.formatarData(saidaDTO.getDtValidadeFinalTed())));

			setPrazoValidadeAltDesc(PgitUtil.concatenarCampos(FormatarData.formatarData(saidaDTO
					.getDtInicialValorAdicionado()), FormatarData.formatarData(saidaDTO.getDtFinalValorAdicionado())));

			// Valorizar ap�s PDC estar atualizado (Estes campos ainda n�o constam no PDC)
			
			  setValorLimiteTED(saidaDTO.getValorAdicionadoContratoConta()); setDataPrazoInicioTED(FormatarData
			  .formataDiaMesAnoFromPdc(saidaDTO.getDtInicialValorAdicionado())); setDataPrazoFimTED(FormatarData
			  .formataDiaMesAnoFromPdc(saidaDTO.getDtFinalValorAdicionado()));
			  setPrazoValidadeTEDAltDesc(PgitUtil.concatenarCampos(getDataPrazoInicioTED(), getDataPrazoFimTED()));
			 

			setDataHoraInclusao(saidaDTO.getHrInclusaoRegistro());
			setTipoCanalInclusao(saidaDTO.getCdTipoCanalInclusao() + " - " + saidaDTO.getDsCanalInclusao());
			setComplementoInclusao("0".equals(saidaDTO.getCdOperacaoCanalInclusao()) ? "" : saidaDTO
					.getCdOperacaoCanalInclusao());

			setDataHoraManutencao(saidaDTO.getHrManutencaoRegistro());

			if (!"".equals(saidaDTO.getCdUsuarioInclusao())) {
				setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
			} else {
				setUsuarioInclusao(saidaDTO.getCdUsuarioInclusaoExterno());

			}

			if (!"".equals(saidaDTO.getCdUsuarioManutencao())) {
				setUsuarioManutencao(saidaDTO.getCdUsuarioManutencao());
			} else {
				setUsuarioManutencao(saidaDTO.getCdUsuarioManutencaoExterno());
			}

			setTipoCanalManutencao(saidaDTO.getCdTipoCanalManutencao() + " - " + saidaDTO.getDsCanalManutencao());
			setComplementoManutencao("0".equals(saidaDTO.getCdOperacaoCanalManutencao()) ? "" : saidaDTO
					.getCdOperacaoCanalManutencao());
			
			setDescApelido(saidaDTO.getDsEmpresaSegLinha());

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			return "";
		}
		return "AVANCAR_HISTORICO_DETALHAR";
	}

	/**
	 * Voltar historico pesquisar.
	 * 
	 * @return the string
	 */
	public String voltarHistoricoPesquisar() {
		setItemSelecionadoListaHistorico(null);
		return "VOLTAR_HISTORICO_PESQUISAR";
	}

	/**
	 * Voltar historico.
	 * 
	 * @return the string
	 */
	public String voltarHistorico() {
		return "VOLTAR_HISTORICO";
	}

	/**
	 * Avancar historico configuracao.
	 * 
	 * @return the string
	 */
	public String avancarHistoricoConfiguracao() {
		return "AVANCAR_HISTORICO_CONFIGURACAO";
	}

	/**
	 * Relacionamento.
	 * 
	 * @return the string
	 */
	public String relacionamento() {
		try {
			preencherDados();
			carregaComboTipoRelacionamento();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			return "";
		}

		setListaGridPesquisaContasRelacionamentos(null);
		setItemSelecionadoListaSubstituir(null);
		limparDadosRelacionamento();

		return "RELACIONAMENTO";
	}

	/**
	 * Detalhar.
	 * 
	 * @return the string
	 */
	public String detalhar() {
		try {
			preencherDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			return "";
		}

		if (getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getCdTipoDebito().equals(SIM_CONSTANT)) {
			setHabilitaMunicipioAlterar(true);
		} else {
			setHabilitaMunicipioAlterar(false);
		}
		return "DETALHAR";
	}

	/**
	 * Incluir.
	 * 
	 * @return the string
	 */
	public String incluir() {
		setListaGridPesquisaContasIncluir(null);
		setItemSelecionadoListaContasIncluir(null);
		limparDadosRelIncluir();

		return "INCLUIR";
	}

	/**
	 * Alterar.
	 * 
	 * @return the string
	 */
	public String alterar() {
		try {
			// carregaListaMunicipio();

			setRadioMunicipioFeriadoLocal("");
			setUnidadeFederativa(null);
			setMunicipio(null);

			try {
				preencherDados();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
						false);
				return "";
			}

			/*
			 * Rotina que cria uma grid fixa de Finalidades para a tela de alterar,contendo todas as finalidades
			 * existentes(4 no total),e "checando-marcando" as finalidades que est�o vinculadas a conta
			 */

			// Lista que ter� as 4 finalidades
			List<ListarFinalidadeContaSaidaDTO> listaTodasFinalidades = this.getComboService().listarFinalidadeConta();

			// Registro selecionado na Grid, do qual cont�m as informa��es de finalidades registradas para a conta
			ListarContasVincContSaidaDTO registroSelecionado =
				getListaGridPesquisaContas().get(getItemSelecionadoListaContas());

			listaFinalidades = new ArrayList<FinalidadeSaidaDTO>();

			FinalidadeSaidaDTO finalidadeSaidaDTO;
			// Quantidade Finalidade fixada pelo analista DS Kelli Caldato
			int qtdeFinalidadeFixo = 6;
			int qtdOcorrencias = 0;

			if (listaTodasFinalidades.size() > qtdeFinalidadeFixo){
				qtdOcorrencias = qtdeFinalidadeFixo;
			}else{
				qtdOcorrencias = listaTodasFinalidades.size();
			}

			for (int i = 0; i < qtdOcorrencias; i++) {
				finalidadeSaidaDTO = new FinalidadeSaidaDTO();
				finalidadeSaidaDTO.setCdFinalidade(listaTodasFinalidades.get(i).getCdFinalidadeContaPagamento());
				finalidadeSaidaDTO.setDsFinalidade(listaTodasFinalidades.get(i).getRsFinalidadeContaPagamento());

				// Se finalidade for D�bito,testa se a mesma esta vinculada na conta,se estiver checamos o checkbox da
				// Grid
				if (finalidadeSaidaDTO.getCdFinalidade() == 1) {
					finalidadeSaidaDTO.setCheck(registroSelecionado.getCdTipoDebito().equals(SIM_CONSTANT) ? true : false);
				} else
					// Se finalidade for Tarifa,testa se a mesma esta vinculada na conta,se estiver checamos o checkbox da
					// Grid
					if (finalidadeSaidaDTO.getCdFinalidade() == 2) {
						finalidadeSaidaDTO.setCheck(registroSelecionado.getCdTipoTarifa().equals(SIM_CONSTANT) ? true : false);
					} else
						// Se finalidade for Estorno,testa se a mesma esta vinculada na conta,se estiver checamos o checkbox da
						// Grid
						if (finalidadeSaidaDTO.getCdFinalidade() == 3) {
							finalidadeSaidaDTO.setCheck(registroSelecionado.getCdTipoEstorno().equals(SIM_CONSTANT) ? true : false);
						} else
							// Se finalidade for Alternativa D�bito,testa se a mesma esta vinculada na conta,se estiver checamos o
							// checkbox da Grid
							if (finalidadeSaidaDTO.getCdFinalidade() == 4) {
								finalidadeSaidaDTO.setCheck(registroSelecionado.getDsAlternativa().equals(SIM_CONSTANT) ? true : false);
							} else{
								// Se finalidade for devolucao,testa se a mesma esta vinculada na conta,se estiver checamos o
								// checkbox da Grid
								if (finalidadeSaidaDTO.getCdFinalidade() == 5) {
									finalidadeSaidaDTO.setCheck(SIM_CONSTANT.equals(registroSelecionado.getDsDevolucao()) ? true : false);
								} else{
									// Se finalidade for estorno/devolu��o op,testa se a mesma esta vinculada na conta,se estiver checamos o
									// checkbox da Grid
									if (finalidadeSaidaDTO.getCdFinalidade() == 6) {
										finalidadeSaidaDTO.setCheck(SIM_CONSTANT.equals(registroSelecionado.getDsEstornoDevolucao()) ? true : false);
									}
									
								}
							}
				listaFinalidades.add(finalidadeSaidaDTO);
			}

			habilitarMunicipiosAlterar();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					"conContasManterContrato", BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "ALTERAR";
	}

	/**
	 * Habilitar municipios alterar.
	 */
	public void habilitarMunicipiosAlterar() {
		/*
		 * ### REGRA ### Quando a Finalidade de Debito estiver selecionada, os campos referentes � escolha do municipio
		 * deve estar habilitado,seguindo a regra: - Op��o �Municipio definido pelo cliente� estiver selecionado, os
		 * combos de UF e Municipio devem estar habilitados. - Quando a op��o �Municipio de Agencia da Conta D�bito�
		 * estiver selecionado, os combos de UF e Municipio devem estar desabilitados e a tela deve passar zeros para
		 * esses campos.
		 */

		// Se a finalidade D�bito n�o foi selecionada, limpo os campos e desativo os campos de munic�pios
		if (!listaFinalidades.get(0).isCheck()) {
			setMunicipio(null);
			setUnidadeFederativa(null);
			listaMunicipio.clear();
			setRadioMunicipioFeriadoLocal("");
			setHabilitaMunicipioAlterar(false);
			setHabilitaRadioMunicipioFeriadoLocal(false);
		} else {
			// Finalidade D�bito foi selecionada
			if (listaFinalidades.get(0).isCheck()) {

				// Habilita radio para selecionar "Municipio de Agencia da Conta D�bito" ou "Municipio definido pelo
				// cliente"
				setHabilitaRadioMunicipioFeriadoLocal(true);

				if (getRadioMunicipioFeriadoLocal() == null) {
					setRadioMunicipioFeriadoLocal("");
				}

				if (!getRadioMunicipioFeriadoLocal().equals("2")) {
					// Limpo campos de Municipios
					setMunicipio(null);
					setUnidadeFederativa(null);
					listaMunicipio.clear();
				}

				// Se for selecionada "Municipio definido pelo cliente" , ativo campos de mun�cipio
				if (getRadioMunicipioFeriadoLocal().equals("2")) {
					setHabilitaMunicipioAlterar(true);
				}

				// Se for selecionada "Municipio de Agencia da Conta D�bito" , desativo campos de mun�cipio
				else if (getRadioMunicipioFeriadoLocal().equals("1")) {
					setHabilitaMunicipioAlterar(false);
				}

			}
		}
	}

	/**
	 * Carrega municipio conta.
	 */
	public void carregaMunicipioConta() {
		ConsultarContaContratoEntradaDTO entrada = new ConsultarContaContratoEntradaDTO();

		entrada.setCdPessoaJuridicaVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
				.getCdPessoaJuridicaVinculo());
		entrada.setCdTipoContratoVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
				.getCdTipoContratoVinculo());
		entrada.setCdTipoVinculoContrato(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
				.getCdTipoVinculoContrato());
		entrada.setNrSequenciaContratoVinculo(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
				.getNrSequenciaContratoVinculo());

		entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
		entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContratoNegocio()));

		ConsultarContaContratoSaidaDTO saidaDTO = getManterContratoImpl().consultarContaContrato(entrada);

		setUnidadeFederativa(saidaDTO.getCdUf());
		carregaListaMunicipio();
		setMunicipio(saidaDTO.getCdMunicipio());
		setRadioMunicipioFeriadoLocal(Integer.toString(saidaDTO.getCdMunicipioFeriadoLocal()));
	}

	/**
	 * Excluir.
	 * 
	 * @return the string
	 */
	public String excluir() {
		try {
			preencherDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			return "";
		}

		return "EXCLUIR";
	}

	/**
	 * Historico.
	 * 
	 * @return the string
	 */
	public String historico() {
		setListaGridPesquisaHistorico(null);
		setItemSelecionadoListaHistorico(null);

		if (getItemSelecionadoListaContas() != null) {
			setBanco(Integer.toString(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getCdBanco()));
			setAgencia(Integer.toString(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
					.getCdAgencia()));
			setAgenciaDigito(Integer.toString(getListaGridPesquisaContas().get(getItemSelecionadoListaContas())
					.getCdDigitoAgencia()));
			setConta(Long.toString(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getCdConta()));
			setDigito(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getCdDigitoConta());

			setCpf(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getCdCpfCnpj());
			setRazao(getListaGridPesquisaContas().get(getItemSelecionadoListaContas()).getNomeParticipante());

			setDataManutencaoFim(new Date());
			setDataManutencaoInicio(new Date());

			setHabilitarHistorico(true);
			setHabilitarConta(false);
			// consultarHistorico();
		} else {
			limparDadosHistorico();
			setHabilitarHistorico(true);
			setHabilitarConta(true);
		}

		return "HISTORICO";
	}

	/**
	 * Voltar pesquisar contas.
	 * 
	 * @return the string
	 */
	public String voltarPesquisarContas() {
		setItemSelecionadoListaContas(null);
		return "VOLTAR_PESQUISAR_CONTAS";
	}

	/**
	 * Selecionar incluir.
	 * 
	 * @return the string
	 */
	public String selecionarIncluir() {
		ListarContasPossiveisInclusaoSaidaDTO det =
			getListaGridPesquisaContasIncluir().get(getItemSelecionadoListaContasIncluir());

		setBancoDesc(det.getDsBanco());
		setCodBanco(det.getCdBanco());
		setAgenciaDesc(det.getDsAgencia());
		setCdAgencia(det.getCdAgencia());
		setCpf(getListaGridPesquisaContasIncluir().get(getItemSelecionadoListaContasIncluir()).getCdCpfCnpj());
		setRazao(getListaGridPesquisaContasIncluir().get(getItemSelecionadoListaContasIncluir()).getNmParticipante());

		StringBuffer descricaoConta = new StringBuffer();
		if (det.getCdConta() != null && det.getCdConta() != 0) {
			descricaoConta.append(Long.toString(getListaGridPesquisaContasIncluir().get(
					getItemSelecionadoListaContasIncluir()).getCdConta()));
			descricaoConta.append("-");
			descricaoConta.append(getListaGridPesquisaContasIncluir().get(getItemSelecionadoListaContasIncluir())
					.getCdDigitoConta());
			setContaDesc(descricaoConta.toString());
		} else {
			setContaDesc("");
		}

		setTipoDesc(det.getDsTipoConta());

		return "AVANCAR_INCLUIR";
	}

	/**
	 * Voltar incluir.
	 * 
	 * @return the string
	 */
	public String voltarIncluir() {
		setItemSelecionadoListaContasIncluir(null);
		return "VOLTAR_INCLUIR";
	}

	/**
	 * Carrega lista municipio.
	 */
	public void carregaListaMunicipio() {
		setMunicipio(null);
		listaMunicipio = new ArrayList<SelectItem>();

		ListarMunicipioEntradaDTO entrada = new ListarMunicipioEntradaDTO();
		List<ListarMunicipioSaidaDTO> saida = new ArrayList<ListarMunicipioSaidaDTO>();

		entrada.setCdUf(getUnidadeFederativa());

		saida = comboService.listarMunicipio(entrada);

		listaMunicipioHash.clear();

		Collections.sort((List<ListarMunicipioSaidaDTO>) saida, new Comparator<Object>() {
			public int compare(Object arg0, Object arg1) {
				return ((ListarMunicipioSaidaDTO) arg0).getDsMunicipio().toUpperCase().compareTo(
						((ListarMunicipioSaidaDTO) arg1).getDsMunicipio().toUpperCase());
			}
		});

		for (ListarMunicipioSaidaDTO combo : saida) {
			listaMunicipioHash.put(combo.getCdMunicipio(), combo.getDsMunicipio());
			listaMunicipio.add(new SelectItem(combo.getCdMunicipio(), combo.getDsMunicipio()));
		}

	}

	/**
	 * Voltar manter contrato.
	 * 
	 * @return the string
	 */
	public String voltarManterContrato() {
		return "VOLTAR_MANTER_CONTRATO";
	}

	/* SETs e GETs */
	/**
	 * Get: obrigatoriedadeConsultar.
	 * 
	 * @return obrigatoriedadeConsultar
	 */
	public String getObrigatoriedadeConsultar() {
		return obrigatoriedadeConsultar;
	}

	/**
	 * Set: obrigatoriedadeConsultar.
	 * 
	 * @param obrigatoriedadeConsultar
	 *            the obrigatoriedade consultar
	 */
	public void setObrigatoriedadeConsultar(String obrigatoriedadeConsultar) {
		this.obrigatoriedadeConsultar = obrigatoriedadeConsultar;
	}

	/**
	 * Get: listaParticipanteFiltro.
	 * 
	 * @return listaParticipanteFiltro
	 */
	public List<SelectItem> getListaParticipanteFiltro() {
		return listaParticipanteFiltro;
	}

	/**
	 * Set: listaParticipanteFiltro.
	 * 
	 * @param listaParticipanteFiltro
	 *            the lista participante filtro
	 */
	public void setListaParticipanteFiltro(List<SelectItem> listaParticipanteFiltro) {
		this.listaParticipanteFiltro = listaParticipanteFiltro;
	}

	/**
	 * Get: listaParticipanteHash.
	 * 
	 * @return listaParticipanteHash
	 */
	Map<Integer, String> getListaParticipanteHash() {
		return listaParticipanteHash;
	}

	/**
	 * Set lista participante hash.
	 * 
	 * @param listaParticipanteHash
	 *            the lista participante hash
	 */
	void setListaParticipanteHash(Map<Integer, String> listaParticipanteHash) {
		this.listaParticipanteHash = listaParticipanteHash;
	}

	/**
	 * Get: comboService.
	 * 
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 * 
	 * @param comboService
	 *            the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: manterContratoImpl.
	 * 
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 * 
	 * @param manterContratoImpl
	 *            the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: radioPesquisaContas.
	 * 
	 * @return radioPesquisaContas
	 */
	public String getRadioPesquisaContas() {
		return radioPesquisaContas;
	}

	/**
	 * Set: radioPesquisaContas.
	 * 
	 * @param radioPesquisaContas
	 *            the radio pesquisa contas
	 */
	public void setRadioPesquisaContas(String radioPesquisaContas) {
		this.radioPesquisaContas = radioPesquisaContas;
	}

	/**
	 * Get: agenciaFiltro.
	 * 
	 * @return agenciaFiltro
	 */
	public String getAgenciaFiltro() {
		return agenciaFiltro;
	}

	/**
	 * Set: agenciaFiltro.
	 * 
	 * @param agenciaFiltro
	 *            the agencia filtro
	 */
	public void setAgenciaFiltro(String agenciaFiltro) {
		this.agenciaFiltro = agenciaFiltro;
	}

	/**
	 * Get: bancoFiltro.
	 * 
	 * @return bancoFiltro
	 */
	public String getBancoFiltro() {
		return bancoFiltro;
	}

	/**
	 * Set: bancoFiltro.
	 * 
	 * @param bancoFiltro
	 *            the banco filtro
	 */
	public void setBancoFiltro(String bancoFiltro) {
		this.bancoFiltro = bancoFiltro;
	}

	/**
	 * Get: contaFiltro.
	 * 
	 * @return contaFiltro
	 */
	public String getContaFiltro() {
		return contaFiltro;
	}

	/**
	 * Set: contaFiltro.
	 * 
	 * @param contaFiltro
	 *            the conta filtro
	 */
	public void setContaFiltro(String contaFiltro) {
		this.contaFiltro = contaFiltro;
	}

	/**
	 * Get: digitoFiltro.
	 * 
	 * @return digitoFiltro
	 */
	public String getDigitoFiltro() {
		return digitoFiltro;
	}

	/**
	 * Set: digitoFiltro.
	 * 
	 * @param digitoFiltro
	 *            the digito filtro
	 */
	public void setDigitoFiltro(String digitoFiltro) {
		this.digitoFiltro = digitoFiltro;
	}

	/**
	 * Get: finalidadeFiltro.
	 * 
	 * @return finalidadeFiltro
	 */
	public Integer getFinalidadeFiltro() {
		return finalidadeFiltro;
	}

	/**
	 * Set: finalidadeFiltro.
	 * 
	 * @param finalidadeFiltro
	 *            the finalidade filtro
	 */
	public void setFinalidadeFiltro(Integer finalidadeFiltro) {
		this.finalidadeFiltro = finalidadeFiltro;
	}

	/**
	 * Get: participanteFiltro.
	 * 
	 * @return participanteFiltro
	 */
	public Long getParticipanteFiltro() {
		return participanteFiltro;
	}

	/**
	 * Set: participanteFiltro.
	 * 
	 * @param participanteFiltro
	 *            the participante filtro
	 */
	public void setParticipanteFiltro(Long participanteFiltro) {
		this.participanteFiltro = participanteFiltro;
	}

	/**
	 * Get: tipoContaFiltro.
	 * 
	 * @return tipoContaFiltro
	 */
	public Integer getTipoContaFiltro() {
		return tipoContaFiltro;
	}

	/**
	 * Set: tipoContaFiltro.
	 * 
	 * @param tipoContaFiltro
	 *            the tipo conta filtro
	 */
	public void setTipoContaFiltro(Integer tipoContaFiltro) {
		this.tipoContaFiltro = tipoContaFiltro;
	}

	/**
	 * Get: listaGridPesquisaContas.
	 * 
	 * @return listaGridPesquisaContas
	 */
	public List<ListarContasVincContSaidaDTO> getListaGridPesquisaContas() {
		return listaGridPesquisaContas;
	}

	/**
	 * Set: listaGridPesquisaContas.
	 * 
	 * @param listaGridPesquisaContas
	 *            the lista grid pesquisa contas
	 */
	public void setListaGridPesquisaContas(List<ListarContasVincContSaidaDTO> listaGridPesquisaContas) {
		this.listaGridPesquisaContas = listaGridPesquisaContas;
	}

	/**
	 * Get: itemSelecionadoListaContas.
	 * 
	 * @return itemSelecionadoListaContas
	 */
	public Integer getItemSelecionadoListaContas() {
		return itemSelecionadoListaContas;
	}

	/**
	 * Set: itemSelecionadoListaContas.
	 * 
	 * @param itemSelecionadoListaContas
	 *            the item selecionado lista contas
	 */
	public void setItemSelecionadoListaContas(Integer itemSelecionadoListaContas) {
		this.itemSelecionadoListaContas = itemSelecionadoListaContas;
	}

	/**
	 * Get: listaControleRadioContas.
	 * 
	 * @return listaControleRadioContas
	 */
	public List<SelectItem> getListaControleRadioContas() {
		return listaControleRadioContas;
	}

	/**
	 * Set: listaControleRadioContas.
	 * 
	 * @param listaControleRadioContas
	 *            the lista controle radio contas
	 */
	public void setListaControleRadioContas(List<SelectItem> listaControleRadioContas) {
		this.listaControleRadioContas = listaControleRadioContas;
	}

	/**
	 * Get: complementoInclusao.
	 * 
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 * 
	 * @param complementoInclusao
	 *            the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = "0".equals(complementoInclusao) ? "" : complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 * 
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 * 
	 * @param complementoManutencao
	 *            the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = "0".equals(complementoManutencao) ? "" : complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 * 
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 * 
	 * @param dataHoraInclusao
	 *            the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 * 
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 * 
	 * @param dataHoraManutencao
	 *            the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 * 
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 * 
	 * @param tipoCanalInclusao
	 *            the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 * 
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 * 
	 * @param tipoCanalManutencao
	 *            the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 * 
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 * 
	 * @param usuarioInclusao
	 *            the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 * 
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 * 
	 * @param usuarioManutencao
	 *            the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Is hidden confirma.
	 * 
	 * @return true, if is hidden confirma
	 */
	public boolean isHiddenConfirma() {
		return hiddenConfirma;
	}

	/**
	 * Set: hiddenConfirma.
	 * 
	 * @param hiddenConfirma
	 *            the hidden confirma
	 */
	public void setHiddenConfirma(boolean hiddenConfirma) {
		this.hiddenConfirma = hiddenConfirma;
	}

	/**
	 * Get: finalidade.
	 * 
	 * @return finalidade
	 */
	public Integer getFinalidade() {
		return finalidade;
	}

	/**
	 * Set: finalidade.
	 * 
	 * @param finalidade
	 *            the finalidade
	 */
	public void setFinalidade(Integer finalidade) {
		this.finalidade = finalidade;
	}

	/**
	 * Get: itemSelecionadoListaContasRelacionamentos.
	 * 
	 * @return itemSelecionadoListaContasRelacionamentos
	 */
	public Integer getItemSelecionadoListaContasRelacionamentos() {
		return itemSelecionadoListaContasRelacionamentos;
	}

	/**
	 * Set: itemSelecionadoListaContasRelacionamentos.
	 * 
	 * @param itemSelecionadoListaContasRelacionamentos
	 *            the item selecionado lista contas relacionamentos
	 */
	public void setItemSelecionadoListaContasRelacionamentos(Integer itemSelecionadoListaContasRelacionamentos) {
		this.itemSelecionadoListaContasRelacionamentos = itemSelecionadoListaContasRelacionamentos;
	}

	/**
	 * Get: listaControleRadioContasRelacionamentos.
	 * 
	 * @return listaControleRadioContasRelacionamentos
	 */
	public List<SelectItem> getListaControleRadioContasRelacionamentos() {
		return listaControleRadioContasRelacionamentos;
	}

	/**
	 * Set: listaControleRadioContasRelacionamentos.
	 * 
	 * @param listaControleRadioContasRelacionamentos
	 *            the lista controle radio contas relacionamentos
	 */
	public void setListaControleRadioContasRelacionamentos(List<SelectItem> listaControleRadioContasRelacionamentos) {
		this.listaControleRadioContasRelacionamentos = listaControleRadioContasRelacionamentos;
	}

	/**
	 * Get: listaGridPesquisaContasRelacionamentos.
	 * 
	 * @return listaGridPesquisaContasRelacionamentos
	 */
	public List<ListarContasRelContaSaidaDTO> getListaGridPesquisaContasRelacionamentos() {
		return listaGridPesquisaContasRelacionamentos;
	}

	/**
	 * Set: listaGridPesquisaContasRelacionamentos.
	 * 
	 * @param listaGridPesquisaContasRelacionamentos
	 *            the lista grid pesquisa contas relacionamentos
	 */
	public void setListaGridPesquisaContasRelacionamentos(
			List<ListarContasRelContaSaidaDTO> listaGridPesquisaContasRelacionamentos) {
		this.listaGridPesquisaContasRelacionamentos = listaGridPesquisaContasRelacionamentos;
	}

	/**
	 * Get: agencia.
	 * 
	 * @return agencia
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * Set: agencia.
	 * 
	 * @param agencia
	 *            the agencia
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * Get: banco.
	 * 
	 * @return banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * Set: banco.
	 * 
	 * @param banco
	 *            the banco
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}

	/**
	 * Get: conta.
	 * 
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 * 
	 * @param conta
	 *            the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * Get: digito.
	 * 
	 * @return digito
	 */
	public String getDigito() {
		return digito;
	}

	/**
	 * Set: digito.
	 * 
	 * @param digito
	 *            the digito
	 */
	public void setDigito(String digito) {
		this.digito = digito;
	}

	/**
	 * Get: itemSelecionadoListaSubstituir.
	 * 
	 * @return itemSelecionadoListaSubstituir
	 */
	public Integer getItemSelecionadoListaSubstituir() {
		return itemSelecionadoListaSubstituir;
	}

	/**
	 * Set: itemSelecionadoListaSubstituir.
	 * 
	 * @param itemSelecionadoListaSubstituir
	 *            the item selecionado lista substituir
	 */
	public void setItemSelecionadoListaSubstituir(Integer itemSelecionadoListaSubstituir) {
		this.itemSelecionadoListaSubstituir = itemSelecionadoListaSubstituir;
	}

	/**
	 * Get: listaControleRadioSubstituir.
	 * 
	 * @return listaControleRadioSubstituir
	 */
	public List<SelectItem> getListaControleRadioSubstituir() {
		return listaControleRadioSubstituir;
	}

	/**
	 * Set: listaControleRadioSubstituir.
	 * 
	 * @param listaControleRadioSubstituir
	 *            the lista controle radio substituir
	 */
	public void setListaControleRadioSubstituir(List<SelectItem> listaControleRadioSubstituir) {
		this.listaControleRadioSubstituir = listaControleRadioSubstituir;
	}

	/**
	 * Get: tipoConta.
	 * 
	 * @return tipoConta
	 */
	public Integer getTipoConta() {
		return tipoConta;
	}

	/**
	 * Set: tipoConta.
	 * 
	 * @param tipoConta
	 *            the tipo conta
	 */
	public void setTipoConta(Integer tipoConta) {
		this.tipoConta = tipoConta;
	}

	/**
	 * Get: itemSelecionadoListaContasIncluir.
	 * 
	 * @return itemSelecionadoListaContasIncluir
	 */
	public Integer getItemSelecionadoListaContasIncluir() {
		return itemSelecionadoListaContasIncluir;
	}

	/**
	 * Set: itemSelecionadoListaContasIncluir.
	 * 
	 * @param itemSelecionadoListaContasIncluir
	 *            the item selecionado lista contas incluir
	 */
	public void setItemSelecionadoListaContasIncluir(Integer itemSelecionadoListaContasIncluir) {
		this.itemSelecionadoListaContasIncluir = itemSelecionadoListaContasIncluir;
	}

	/**
	 * Get: itemSelecionadoListaContasRelIncluir.
	 * 
	 * @return itemSelecionadoListaContasRelIncluir
	 */
	public Integer getItemSelecionadoListaContasRelIncluir() {
		return itemSelecionadoListaContasRelIncluir;
	}

	/**
	 * Set: itemSelecionadoListaContasRelIncluir.
	 * 
	 * @param itemSelecionadoListaContasRelIncluir
	 *            the item selecionado lista contas rel incluir
	 */
	public void setItemSelecionadoListaContasRelIncluir(Integer itemSelecionadoListaContasRelIncluir) {
		this.itemSelecionadoListaContasRelIncluir = itemSelecionadoListaContasRelIncluir;
	}

	/**
	 * Get: listaControleRadioContasIncluir.
	 * 
	 * @return listaControleRadioContasIncluir
	 */
	public List<SelectItem> getListaControleRadioContasIncluir() {
		return listaControleRadioContasIncluir;
	}

	/**
	 * Set: listaControleRadioContasIncluir.
	 * 
	 * @param listaControleRadioContasIncluir
	 *            the lista controle radio contas incluir
	 */
	public void setListaControleRadioContasIncluir(List<SelectItem> listaControleRadioContasIncluir) {
		this.listaControleRadioContasIncluir = listaControleRadioContasIncluir;
	}

	/**
	 * Get: listaControleRadioContasRelIncluir.
	 * 
	 * @return listaControleRadioContasRelIncluir
	 */
	public List<SelectItem> getListaControleRadioContasRelIncluir() {
		return listaControleRadioContasRelIncluir;
	}

	/**
	 * Set: listaControleRadioContasRelIncluir.
	 * 
	 * @param listaControleRadioContasRelIncluir
	 *            the lista controle radio contas rel incluir
	 */
	public void setListaControleRadioContasRelIncluir(List<SelectItem> listaControleRadioContasRelIncluir) {
		this.listaControleRadioContasRelIncluir = listaControleRadioContasRelIncluir;
	}

	/**
	 * Get: listaGridPesquisaContasRelIncluir.
	 * 
	 * @return listaGridPesquisaContasRelIncluir
	 */
	public List<ListarContasVincContSaidaDTO> getListaGridPesquisaContasRelIncluir() {
		return listaGridPesquisaContasRelIncluir;
	}

	/**
	 * Set: listaGridPesquisaContasRelIncluir.
	 * 
	 * @param listaGridPesquisaContasRelIncluir
	 *            the lista grid pesquisa contas rel incluir
	 */
	public void setListaGridPesquisaContasRelIncluir(
			List<ListarContasVincContSaidaDTO> listaGridPesquisaContasRelIncluir) {
		this.listaGridPesquisaContasRelIncluir = listaGridPesquisaContasRelIncluir;
	}

	/**
	 * Get: listaFinalidades.
	 * 
	 * @return listaFinalidades
	 */
	public List<FinalidadeSaidaDTO> getListaFinalidades() {
		return listaFinalidades;
	}

	/**
	 * Set: listaFinalidades.
	 * 
	 * @param listaFinalidades
	 *            the lista finalidades
	 */
	public void setListaFinalidades(List<FinalidadeSaidaDTO> listaFinalidades) {
		this.listaFinalidades = listaFinalidades;
	}

	/**
	 * Get: listaControleCheckFinalidades.
	 * 
	 * @return listaControleCheckFinalidades
	 */
	public List<SelectItem> getListaControleCheckFinalidades() {
		return listaControleCheckFinalidades;
	}

	/**
	 * Set: listaControleCheckFinalidades.
	 * 
	 * @param listaControleCheckFinalidades
	 *            the lista controle check finalidades
	 */
	public void setListaControleCheckFinalidades(List<SelectItem> listaControleCheckFinalidades) {
		this.listaControleCheckFinalidades = listaControleCheckFinalidades;
	}

	/**
	 * Get: radioMunicipioFeriadoLocal.
	 * 
	 * @return radioMunicipioFeriadoLocal
	 */
	public String getRadioMunicipioFeriadoLocal() {
		return radioMunicipioFeriadoLocal;
	}

	/**
	 * Set: radioMunicipioFeriadoLocal.
	 * 
	 * @param radioMunicipioFeriadoLocal
	 *            the radio municipio feriado local
	 */
	public void setRadioMunicipioFeriadoLocal(String radioMunicipioFeriadoLocal) {
		this.radioMunicipioFeriadoLocal = radioMunicipioFeriadoLocal;
	}

	/**
	 * Get: unidadeFederativa.
	 * 
	 * @return unidadeFederativa
	 */
	public Integer getUnidadeFederativa() {
		return unidadeFederativa;
	}

	/**
	 * Set: unidadeFederativa.
	 * 
	 * @param unidadeFederativa
	 *            the unidade federativa
	 */
	public void setUnidadeFederativa(Integer unidadeFederativa) {
		this.unidadeFederativa = unidadeFederativa;
	}

	/**
	 * Get: itemSelecionadoListaHistorico.
	 * 
	 * @return itemSelecionadoListaHistorico
	 */
	public Integer getItemSelecionadoListaHistorico() {
		return itemSelecionadoListaHistorico;
	}

	/**
	 * Set: itemSelecionadoListaHistorico.
	 * 
	 * @param itemSelecionadoListaHistorico
	 *            the item selecionado lista historico
	 */
	public void setItemSelecionadoListaHistorico(Integer itemSelecionadoListaHistorico) {
		this.itemSelecionadoListaHistorico = itemSelecionadoListaHistorico;
	}

	/**
	 * Get: listaControleRadioHistorico.
	 * 
	 * @return listaControleRadioHistorico
	 */
	public List<SelectItem> getListaControleRadioHistorico() {
		return listaControleRadioHistorico;
	}

	/**
	 * Set: listaControleRadioHistorico.
	 * 
	 * @param listaControleRadioHistorico
	 *            the lista controle radio historico
	 */
	public void setListaControleRadioHistorico(List<SelectItem> listaControleRadioHistorico) {
		this.listaControleRadioHistorico = listaControleRadioHistorico;
	}

	/**
	 * Get: agenciaDigitoFiltro.
	 * 
	 * @return agenciaDigitoFiltro
	 */
	public String getAgenciaDigitoFiltro() {
		return agenciaDigitoFiltro;
	}

	/**
	 * Set: agenciaDigitoFiltro.
	 * 
	 * @param agenciaDigitoFiltro
	 *            the agencia digito filtro
	 */
	public void setAgenciaDigitoFiltro(String agenciaDigitoFiltro) {
		this.agenciaDigitoFiltro = agenciaDigitoFiltro;
	}

	/**
	 * Get: agenciaDigito.
	 * 
	 * @return agenciaDigito
	 */
	public String getAgenciaDigito() {
		return agenciaDigito;
	}

	/**
	 * Set: agenciaDigito.
	 * 
	 * @param agenciaDigito
	 *            the agencia digito
	 */
	public void setAgenciaDigito(String agenciaDigito) {
		this.agenciaDigito = agenciaDigito;
	}

	/**
	 * Get: radioPesquisa.
	 * 
	 * @return radioPesquisa
	 */
	public String getRadioPesquisa() {
		return radioPesquisa;
	}

	/**
	 * Set: radioPesquisa.
	 * 
	 * @param radioPesquisa
	 *            the radio pesquisa
	 */
	public void setRadioPesquisa(String radioPesquisa) {
		this.radioPesquisa = radioPesquisa;
	}

	/**
	 * Get: listaTipoConta.
	 * 
	 * @return listaTipoConta
	 */
	public List<SelectItem> getListaTipoConta() {
		return listaTipoConta;
	}

	/**
	 * Set: listaTipoConta.
	 * 
	 * @param listaTipoConta
	 *            the lista tipo conta
	 */
	public void setListaTipoConta(List<SelectItem> listaTipoConta) {
		this.listaTipoConta = listaTipoConta;
	}

	/**
	 * Get: listaTipoContaHash.
	 * 
	 * @return listaTipoContaHash
	 */
	Map<Integer, String> getListaTipoContaHash() {
		return listaTipoContaHash;
	}

	/**
	 * Set lista tipo conta hash.
	 * 
	 * @param listaTipoContaHash
	 *            the lista tipo conta hash
	 */
	void setListaTipoContaHash(Map<Integer, String> listaTipoContaHash) {
		this.listaTipoContaHash = listaTipoContaHash;
	}

	/**
	 * Get: listaFinalidadeConta.
	 * 
	 * @return listaFinalidadeConta
	 */
	public List<SelectItem> getListaFinalidadeConta() {
		return listaFinalidadeConta;
	}

	/**
	 * Set: listaFinalidadeConta.
	 * 
	 * @param listaFinalidadeConta
	 *            the lista finalidade conta
	 */
	public void setListaFinalidadeConta(List<SelectItem> listaFinalidadeConta) {
		this.listaFinalidadeConta = listaFinalidadeConta;
	}

	/**
	 * Get: listaFinalidadeContaHash.
	 * 
	 * @return listaFinalidadeContaHash
	 */
	Map<Integer, String> getListaFinalidadeContaHash() {
		return listaFinalidadeContaHash;
	}

	/**
	 * Set lista finalidade conta hash.
	 * 
	 * @param listaFinalidadeContaHash
	 *            the lista finalidade conta hash
	 */
	void setListaFinalidadeContaHash(Map<Integer, String> listaFinalidadeContaHash) {
		this.listaFinalidadeContaHash = listaFinalidadeContaHash;
	}

	/**
	 * Get: finalidadeRelacionamento.
	 * 
	 * @return finalidadeRelacionamento
	 */
	public Integer getFinalidadeRelacionamento() {
		return finalidadeRelacionamento;
	}

	/**
	 * Set: finalidadeRelacionamento.
	 * 
	 * @param finalidadeRelacionamento
	 *            the finalidade relacionamento
	 */
	public void setFinalidadeRelacionamento(Integer finalidadeRelacionamento) {
		this.finalidadeRelacionamento = finalidadeRelacionamento;
	}

	/**
	 * Get: listaUnidadeFederativa.
	 * 
	 * @return listaUnidadeFederativa
	 */
	public List<SelectItem> getListaUnidadeFederativa() {
		return listaUnidadeFederativa;
	}

	/**
	 * Set: listaUnidadeFederativa.
	 * 
	 * @param listaUnidadeFederativa
	 *            the lista unidade federativa
	 */
	public void setListaUnidadeFederativa(List<SelectItem> listaUnidadeFederativa) {
		this.listaUnidadeFederativa = listaUnidadeFederativa;
	}

	/**
	 * Get: listaUnidadeFederativaHash.
	 * 
	 * @return listaUnidadeFederativaHash
	 */
	Map<Integer, String> getListaUnidadeFederativaHash() {
		return listaUnidadeFederativaHash;
	}

	/**
	 * Set lista unidade federativa hash.
	 * 
	 * @param listaUnidadeFederativaHash
	 *            the lista unidade federativa hash
	 */
	void setListaUnidadeFederativaHash(Map<Integer, String> listaUnidadeFederativaHash) {
		this.listaUnidadeFederativaHash = listaUnidadeFederativaHash;
	}

	/**
	 * Get: listaGridPesquisaSubstituir.
	 * 
	 * @return listaGridPesquisaSubstituir
	 */
	public List<ListarContasVincContSaidaDTO> getListaGridPesquisaSubstituir() {
		return listaGridPesquisaSubstituir;
	}

	/**
	 * Set: listaGridPesquisaSubstituir.
	 * 
	 * @param listaGridPesquisaSubstituir
	 *            the lista grid pesquisa substituir
	 */
	public void setListaGridPesquisaSubstituir(List<ListarContasVincContSaidaDTO> listaGridPesquisaSubstituir) {
		this.listaGridPesquisaSubstituir = listaGridPesquisaSubstituir;
	}

	/**
	 * Get: cdControleCnpj.
	 * 
	 * @return cdControleCnpj
	 */
	public String getCdControleCnpj() {
		return cdControleCnpj;
	}

	/**
	 * Set: cdControleCnpj.
	 * 
	 * @param cdControleCnpj
	 *            the cd controle cnpj
	 */
	public void setCdControleCnpj(String cdControleCnpj) {
		this.cdControleCnpj = cdControleCnpj;
	}

	/**
	 * Get: cdControleCpf.
	 * 
	 * @return cdControleCpf
	 */
	public String getCdControleCpf() {
		return cdControleCpf;
	}

	/**
	 * Set: cdControleCpf.
	 * 
	 * @param cdControleCpf
	 *            the cd controle cpf
	 */
	public void setCdControleCpf(String cdControleCpf) {
		this.cdControleCpf = cdControleCpf;
	}

	/**
	 * Get: cdCpf.
	 * 
	 * @return cdCpf
	 */
	public String getCdCpf() {
		return cdCpf;
	}

	/**
	 * Set: cdCpf.
	 * 
	 * @param cdCpf
	 *            the cd cpf
	 */
	public void setCdCpf(String cdCpf) {
		this.cdCpf = cdCpf;
	}

	/**
	 * Get: cdCpfCnpj.
	 * 
	 * @return cdCpfCnpj
	 */
	public String getCdCpfCnpj() {
		return cdCpfCnpj;
	}

	/**
	 * Set: cdCpfCnpj.
	 * 
	 * @param cdCpfCnpj
	 *            the cd cpf cnpj
	 */
	public void setCdCpfCnpj(String cdCpfCnpj) {
		this.cdCpfCnpj = cdCpfCnpj;
	}

	/**
	 * Get: cdFilialCnpj.
	 * 
	 * @return cdFilialCnpj
	 */
	public String getCdFilialCnpj() {
		return cdFilialCnpj;
	}

	/**
	 * Set: cdFilialCnpj.
	 * 
	 * @param cdFilialCnpj
	 *            the cd filial cnpj
	 */
	public void setCdFilialCnpj(String cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}

	/**
	 * Get: listaControleCheckRelInc.
	 * 
	 * @return listaControleCheckRelInc
	 */
	public List<SelectItem> getListaControleCheckRelInc() {
		return listaControleCheckRelInc;
	}

	/**
	 * Set: listaControleCheckRelInc.
	 * 
	 * @param listaControleCheckRelInc
	 *            the lista controle check rel inc
	 */
	public void setListaControleCheckRelInc(List<SelectItem> listaControleCheckRelInc) {
		this.listaControleCheckRelInc = listaControleCheckRelInc;
	}

	/**
	 * Get: cdPessoaJuridica.
	 * 
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 * 
	 * @param cdPessoaJuridica
	 *            the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 * 
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 * 
	 * @param cdTipoContratoNegocio
	 *            the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 * 
	 * @return nrSequenciaContratoNegocio
	 */
	public String getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 * 
	 * @param nrSequenciaContratoNegocio
	 *            the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(String nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: listaOcorrenciasIncluirRelAux.
	 * 
	 * @return listaOcorrenciasIncluirRelAux
	 */
	public List<ListarContasVincContSaidaDTO> getListaOcorrenciasIncluirRelAux() {
		return listaOcorrenciasIncluirRelAux;
	}

	/**
	 * Set: listaOcorrenciasIncluirRelAux.
	 * 
	 * @param listaOcorrenciasIncluirRelAux
	 *            the lista ocorrencias incluir rel aux
	 */
	public void setListaOcorrenciasIncluirRelAux(List<ListarContasVincContSaidaDTO> listaOcorrenciasIncluirRelAux) {
		this.listaOcorrenciasIncluirRelAux = listaOcorrenciasIncluirRelAux;
	}

	/**
	 * Get: listaOcorrenciasIncluirRel.
	 * 
	 * @return listaOcorrenciasIncluirRel
	 */
	public List<IncluirRelacContaOcorrenciasDTO> getListaOcorrenciasIncluirRel() {
		return listaOcorrenciasIncluirRel;
	}

	/**
	 * Set: listaOcorrenciasIncluirRel.
	 * 
	 * @param listaOcorrenciasIncluirRel
	 *            the lista ocorrencias incluir rel
	 */
	public void setListaOcorrenciasIncluirRel(List<IncluirRelacContaOcorrenciasDTO> listaOcorrenciasIncluirRel) {
		this.listaOcorrenciasIncluirRel = listaOcorrenciasIncluirRel;
	}

	/**
	 * Get: agenciaDesc.
	 * 
	 * @return agenciaDesc
	 */
	public String getAgenciaDesc() {
		return agenciaDesc;
	}

	/**
	 * Set: agenciaDesc.
	 * 
	 * @param agenciaDesc
	 *            the agencia desc
	 */
	public void setAgenciaDesc(String agenciaDesc) {
		this.agenciaDesc = agenciaDesc;
	}

	/**
	 * Get: bancoDesc.
	 * 
	 * @return bancoDesc
	 */
	public String getBancoDesc() {
		return bancoDesc;
	}

	/**
	 * Set: bancoDesc.
	 * 
	 * @param bancoDesc
	 *            the banco desc
	 */
	public void setBancoDesc(String bancoDesc) {
		this.bancoDesc = bancoDesc;
	}

	/**
	 * Get: contaDesc.
	 * 
	 * @return contaDesc
	 */
	public String getContaDesc() {
		return contaDesc;
	}

	/**
	 * Set: contaDesc.
	 * 
	 * @param contaDesc
	 *            the conta desc
	 */
	public void setContaDesc(String contaDesc) {
		this.contaDesc = contaDesc;
	}

	/**
	 * Get: dataVinculacaoDesc.
	 * 
	 * @return dataVinculacaoDesc
	 */
	public String getDataVinculacaoDesc() {
		return dataVinculacaoDesc;
	}

	/**
	 * Set: dataVinculacaoDesc.
	 * 
	 * @param dataVinculacaoDesc
	 *            the data vinculacao desc
	 */
	public void setDataVinculacaoDesc(String dataVinculacaoDesc) {
		this.dataVinculacaoDesc = dataVinculacaoDesc;
	}

	/**
	 * Get: situacaoVinculoDesc.
	 * 
	 * @return situacaoVinculoDesc
	 */
	public String getSituacaoVinculoDesc() {
		return situacaoVinculoDesc;
	}

	/**
	 * Set: situacaoVinculoDesc.
	 * 
	 * @param situacaoVinculoDesc
	 *            the situacao vinculo desc
	 */
	public void setSituacaoVinculoDesc(String situacaoVinculoDesc) {
		this.situacaoVinculoDesc = situacaoVinculoDesc;
	}

	/**
	 * Get: tipoDesc.
	 * 
	 * @return tipoDesc
	 */
	public String getTipoDesc() {
		return tipoDesc;
	}

	/**
	 * Set: tipoDesc.
	 * 
	 * @param tipoDesc
	 *            the tipo desc
	 */
	public void setTipoDesc(String tipoDesc) {
		this.tipoDesc = tipoDesc;
	}

	/**
	 * Get: municipioDesc.
	 * 
	 * @return municipioDesc
	 */
	public String getMunicipioDesc() {
		return municipioDesc;
	}

	/**
	 * Set: municipioDesc.
	 * 
	 * @param municipioDesc
	 *            the municipio desc
	 */
	public void setMunicipioDesc(String municipioDesc) {
		this.municipioDesc = municipioDesc;
	}

	/**
	 * Get: municipioFeriadoLocalDesc.
	 * 
	 * @return municipioFeriadoLocalDesc
	 */
	public int getMunicipioFeriadoLocalDesc() {
		return municipioFeriadoLocalDesc;
	}

	/**
	 * Set: municipioFeriadoLocalDesc.
	 * 
	 * @param municipioFeriadoLocalDesc
	 *            the municipio feriado local desc
	 */
	public void setMunicipioFeriadoLocalDesc(int municipioFeriadoLocalDesc) {
		this.municipioFeriadoLocalDesc = municipioFeriadoLocalDesc;
	}

	/**
	 * Get: cpfCnpjDesc.
	 * 
	 * @return cpfCnpjDesc
	 */
	public String getCpfCnpjDesc() {
		return cpfCnpjDesc;
	}

	/**
	 * Set: cpfCnpjDesc.
	 * 
	 * @param cpfCnpjDesc
	 *            the cpf cnpj desc
	 */
	public void setCpfCnpjDesc(String cpfCnpjDesc) {
		this.cpfCnpjDesc = cpfCnpjDesc;
	}

	/**
	 * Get: nomeRazaoDesc.
	 * 
	 * @return nomeRazaoDesc
	 */
	public String getNomeRazaoDesc() {
		return nomeRazaoDesc;
	}

	/**
	 * Set: nomeRazaoDesc.
	 * 
	 * @param nomeRazaoDesc
	 *            the nome razao desc
	 */
	public void setNomeRazaoDesc(String nomeRazaoDesc) {
		this.nomeRazaoDesc = nomeRazaoDesc;
	}

	/**
	 * Get: finalidadeContaRelacionadaDesc.
	 * 
	 * @return finalidadeContaRelacionadaDesc
	 */
	public String getFinalidadeContaRelacionadaDesc() {
		return finalidadeContaRelacionadaDesc;
	}

	/**
	 * Set: finalidadeContaRelacionadaDesc.
	 * 
	 * @param finalidadeContaRelacionadaDesc
	 *            the finalidade conta relacionada desc
	 */
	public void setFinalidadeContaRelacionadaDesc(String finalidadeContaRelacionadaDesc) {
		this.finalidadeContaRelacionadaDesc = finalidadeContaRelacionadaDesc;
	}

	/**
	 * Get: finalidadeDesc.
	 * 
	 * @return finalidadeDesc
	 */
	public String getFinalidadeDesc() {
		return finalidadeDesc;
	}

	/**
	 * Set: finalidadeDesc.
	 * 
	 * @param finalidadeDesc
	 *            the finalidade desc
	 */
	public void setFinalidadeDesc(String finalidadeDesc) {
		this.finalidadeDesc = finalidadeDesc;
	}

	/**
	 * Get: agenciaRelacionadaDesc.
	 * 
	 * @return agenciaRelacionadaDesc
	 */
	public String getAgenciaRelacionadaDesc() {
		return agenciaRelacionadaDesc;
	}

	/**
	 * Set: agenciaRelacionadaDesc.
	 * 
	 * @param agenciaRelacionadaDesc
	 *            the agencia relacionada desc
	 */
	public void setAgenciaRelacionadaDesc(String agenciaRelacionadaDesc) {
		this.agenciaRelacionadaDesc = agenciaRelacionadaDesc;
	}

	/**
	 * Get: bancoRelacionadaDesc.
	 * 
	 * @return bancoRelacionadaDesc
	 */
	public String getBancoRelacionadaDesc() {
		return bancoRelacionadaDesc;
	}

	/**
	 * Set: bancoRelacionadaDesc.
	 * 
	 * @param bancoRelacionadaDesc
	 *            the banco relacionada desc
	 */
	public void setBancoRelacionadaDesc(String bancoRelacionadaDesc) {
		this.bancoRelacionadaDesc = bancoRelacionadaDesc;
	}

	/**
	 * Get: contaRelacionadaDesc.
	 * 
	 * @return contaRelacionadaDesc
	 */
	public String getContaRelacionadaDesc() {
		return contaRelacionadaDesc;
	}

	/**
	 * Set: contaRelacionadaDesc.
	 * 
	 * @param contaRelacionadaDesc
	 *            the conta relacionada desc
	 */
	public void setContaRelacionadaDesc(String contaRelacionadaDesc) {
		this.contaRelacionadaDesc = contaRelacionadaDesc;
	}

	/**
	 * Get: tipoRelacionadaDesc.
	 * 
	 * @return tipoRelacionadaDesc
	 */
	public String getTipoRelacionadaDesc() {
		return tipoRelacionadaDesc;
	}

	/**
	 * Set: tipoRelacionadaDesc.
	 * 
	 * @param tipoRelacionadaDesc
	 *            the tipo relacionada desc
	 */
	public void setTipoRelacionadaDesc(String tipoRelacionadaDesc) {
		this.tipoRelacionadaDesc = tipoRelacionadaDesc;
	}

	/**
	 * Get: listaFinalidadeContaSelecionada.
	 * 
	 * @return listaFinalidadeContaSelecionada
	 */
	public List<SelectItem> getListaFinalidadeContaSelecionada() {
		return listaFinalidadeContaSelecionada;
	}

	/**
	 * Set: listaFinalidadeContaSelecionada.
	 * 
	 * @param listaFinalidadeContaSelecionada
	 *            the lista finalidade conta selecionada
	 */
	public void setListaFinalidadeContaSelecionada(List<SelectItem> listaFinalidadeContaSelecionada) {
		this.listaFinalidadeContaSelecionada = listaFinalidadeContaSelecionada;
	}

	/**
	 * Get: listaFinalidadeContaSelecionadaHash.
	 * 
	 * @return listaFinalidadeContaSelecionadaHash
	 */
	Map<Integer, String> getListaFinalidadeContaSelecionadaHash() {
		return listaFinalidadeContaSelecionadaHash;
	}

	/**
	 * Set lista finalidade conta selecionada hash.
	 * 
	 * @param listaFinalidadeContaSelecionadaHash
	 *            the lista finalidade conta selecionada hash
	 */
	void setListaFinalidadeContaSelecionadaHash(Map<Integer, String> listaFinalidadeContaSelecionadaHash) {
		this.listaFinalidadeContaSelecionadaHash = listaFinalidadeContaSelecionadaHash;
	}

	/**
	 * Get: cdMotivoSituacaoConta.
	 * 
	 * @return cdMotivoSituacaoConta
	 */
	public int getCdMotivoSituacaoConta() {
		return cdMotivoSituacaoConta;
	}

	/**
	 * Set: cdMotivoSituacaoConta.
	 * 
	 * @param cdMotivoSituacaoConta
	 *            the cd motivo situacao conta
	 */
	public void setCdMotivoSituacaoConta(int cdMotivoSituacaoConta) {
		this.cdMotivoSituacaoConta = cdMotivoSituacaoConta;
	}

	/**
	 * Get: cdSituacaoVinculacaoConta.
	 * 
	 * @return cdSituacaoVinculacaoConta
	 */
	public int getCdSituacaoVinculacaoConta() {
		return cdSituacaoVinculacaoConta;
	}

	/**
	 * Set: cdSituacaoVinculacaoConta.
	 * 
	 * @param cdSituacaoVinculacaoConta
	 *            the cd situacao vinculacao conta
	 */
	public void setCdSituacaoVinculacaoConta(int cdSituacaoVinculacaoConta) {
		this.cdSituacaoVinculacaoConta = cdSituacaoVinculacaoConta;
	}

	/**
	 * Get: dataManutencaoFim.
	 * 
	 * @return dataManutencaoFim
	 */
	public Date getDataManutencaoFim() {
		return dataManutencaoFim;
	}

	/**
	 * Set: dataManutencaoFim.
	 * 
	 * @param dataManutencaoFim
	 *            the data manutencao fim
	 */
	public void setDataManutencaoFim(Date dataManutencaoFim) {
		this.dataManutencaoFim = dataManutencaoFim;
	}

	/**
	 * Get: dataManutencaoInicio.
	 * 
	 * @return dataManutencaoInicio
	 */
	public Date getDataManutencaoInicio() {
		return dataManutencaoInicio;
	}

	/**
	 * Set: dataManutencaoInicio.
	 * 
	 * @param dataManutencaoInicio
	 *            the data manutencao inicio
	 */
	public void setDataManutencaoInicio(Date dataManutencaoInicio) {
		this.dataManutencaoInicio = dataManutencaoInicio;
	}

	/**
	 * Get: listaGridPesquisaHistorico.
	 * 
	 * @return listaGridPesquisaHistorico
	 */
	public List<ListarConManContaVinculadaSaidaDTO> getListaGridPesquisaHistorico() {
		return listaGridPesquisaHistorico;
	}

	/**
	 * Set: listaGridPesquisaHistorico.
	 * 
	 * @param listaGridPesquisaHistorico
	 *            the lista grid pesquisa historico
	 */
	public void setListaGridPesquisaHistorico(List<ListarConManContaVinculadaSaidaDTO> listaGridPesquisaHistorico) {
		this.listaGridPesquisaHistorico = listaGridPesquisaHistorico;
	}

	/**
	 * Get: unidadeFederativaDesc.
	 * 
	 * @return unidadeFederativaDesc
	 */
	public String getUnidadeFederativaDesc() {
		return unidadeFederativaDesc;
	}

	/**
	 * Set: unidadeFederativaDesc.
	 * 
	 * @param unidadeFederativaDesc
	 *            the unidade federativa desc
	 */
	public void setUnidadeFederativaDesc(String unidadeFederativaDesc) {
		this.unidadeFederativaDesc = unidadeFederativaDesc;
	}

	/**
	 * Get: participante.
	 * 
	 * @return participante
	 */
	public Long getParticipante() {
		return participante;
	}

	/**
	 * Set: participante.
	 * 
	 * @param participante
	 *            the participante
	 */
	public void setParticipante(Long participante) {
		this.participante = participante;
	}

	/**
	 * Get: listaMunicipio.
	 * 
	 * @return listaMunicipio
	 */
	public List<SelectItem> getListaMunicipio() {
		return listaMunicipio;
	}

	/**
	 * Set: listaMunicipio.
	 * 
	 * @param listaMunicipio
	 *            the lista municipio
	 */
	public void setListaMunicipio(List<SelectItem> listaMunicipio) {
		this.listaMunicipio = listaMunicipio;
	}

	/**
	 * Get: listaMunicipioHash.
	 * 
	 * @return listaMunicipioHash
	 */
	Map<Long, String> getListaMunicipioHash() {
		return listaMunicipioHash;
	}

	/**
	 * Set lista municipio hash.
	 * 
	 * @param listaMunicipioHash
	 *            the lista municipio hash
	 */
	void setListaMunicipioHash(Map<Long, String> listaMunicipioHash) {
		this.listaMunicipioHash = listaMunicipioHash;
	}

	/**
	 * Get: municipio.
	 * 
	 * @return municipio
	 */
	public Long getMunicipio() {
		return municipio;
	}

	/**
	 * Set: municipio.
	 * 
	 * @param municipio
	 *            the municipio
	 */
	public void setMunicipio(Long municipio) {
		this.municipio = municipio;
	}

	/**
	 * Get: descricaoContrato.
	 * 
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 * 
	 * @param descricaoContrato
	 *            the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: listaGridPesquisaContasIncluir.
	 * 
	 * @return listaGridPesquisaContasIncluir
	 */
	public List<ListarContasPossiveisInclusaoSaidaDTO> getListaGridPesquisaContasIncluir() {
		return listaGridPesquisaContasIncluir;
	}

	/**
	 * Set: listaGridPesquisaContasIncluir.
	 * 
	 * @param listaGridPesquisaContasIncluir
	 *            the lista grid pesquisa contas incluir
	 */
	public void setListaGridPesquisaContasIncluir(
			List<ListarContasPossiveisInclusaoSaidaDTO> listaGridPesquisaContasIncluir) {
		this.listaGridPesquisaContasIncluir = listaGridPesquisaContasIncluir;
	}

	/**
	 * Get: cdControleCnpjFiltro.
	 * 
	 * @return cdControleCnpjFiltro
	 */
	public String getCdControleCnpjFiltro() {
		return cdControleCnpjFiltro;
	}

	/**
	 * Set: cdControleCnpjFiltro.
	 * 
	 * @param cdControleCnpjFiltro
	 *            the cd controle cnpj filtro
	 */
	public void setCdControleCnpjFiltro(String cdControleCnpjFiltro) {
		this.cdControleCnpjFiltro = cdControleCnpjFiltro;
	}

	/**
	 * Get: cdControleCpfFiltro.
	 * 
	 * @return cdControleCpfFiltro
	 */
	public String getCdControleCpfFiltro() {
		return cdControleCpfFiltro;
	}

	/**
	 * Set: cdControleCpfFiltro.
	 * 
	 * @param cdControleCpfFiltro
	 *            the cd controle cpf filtro
	 */
	public void setCdControleCpfFiltro(String cdControleCpfFiltro) {
		this.cdControleCpfFiltro = cdControleCpfFiltro;
	}

	/**
	 * Get: cdCpfCnpjFiltro.
	 * 
	 * @return cdCpfCnpjFiltro
	 */
	public String getCdCpfCnpjFiltro() {
		return cdCpfCnpjFiltro;
	}

	/**
	 * Set: cdCpfCnpjFiltro.
	 * 
	 * @param cdCpfCnpjFiltro
	 *            the cd cpf cnpj filtro
	 */
	public void setCdCpfCnpjFiltro(String cdCpfCnpjFiltro) {
		this.cdCpfCnpjFiltro = cdCpfCnpjFiltro;
	}

	/**
	 * Get: cdCpfFiltro.
	 * 
	 * @return cdCpfFiltro
	 */
	public String getCdCpfFiltro() {
		return cdCpfFiltro;
	}

	/**
	 * Set: cdCpfFiltro.
	 * 
	 * @param cdCpfFiltro
	 *            the cd cpf filtro
	 */
	public void setCdCpfFiltro(String cdCpfFiltro) {
		this.cdCpfFiltro = cdCpfFiltro;
	}

	/**
	 * Get: cdFilialCnpjFiltro.
	 * 
	 * @return cdFilialCnpjFiltro
	 */
	public String getCdFilialCnpjFiltro() {
		return cdFilialCnpjFiltro;
	}

	/**
	 * Set: cdFilialCnpjFiltro.
	 * 
	 * @param cdFilialCnpjFiltro
	 *            the cd filial cnpj filtro
	 */
	public void setCdFilialCnpjFiltro(String cdFilialCnpjFiltro) {
		this.cdFilialCnpjFiltro = cdFilialCnpjFiltro;
	}

	/**
	 * Is habilita selecionar incluir relacionamento.
	 * 
	 * @return true, if is habilita selecionar incluir relacionamento
	 */
	public boolean isHabilitaSelecionarIncluirRelacionamento() {
		return habilitaSelecionarIncluirRelacionamento;
	}

	/**
	 * Set: habilitaSelecionarIncluirRelacionamento.
	 * 
	 * @param habilitaSelecionarIncluirRelacionamento
	 *            the habilita selecionar incluir relacionamento
	 */
	public void setHabilitaSelecionarIncluirRelacionamento(boolean habilitaSelecionarIncluirRelacionamento) {
		this.habilitaSelecionarIncluirRelacionamento = habilitaSelecionarIncluirRelacionamento;
	}

	/**
	 * Get: listaFinalidadesSelecionadas.
	 * 
	 * @return listaFinalidadesSelecionadas
	 */
	public List<FinalidadeSaidaDTO> getListaFinalidadesSelecionadas() {
		return listaFinalidadesSelecionadas;
	}

	/**
	 * Set: listaFinalidadesSelecionadas.
	 * 
	 * @param listaFinalidadesSelecionadas
	 *            the lista finalidades selecionadas
	 */
	public void setListaFinalidadesSelecionadas(List<FinalidadeSaidaDTO> listaFinalidadesSelecionadas) {
		this.listaFinalidadesSelecionadas = listaFinalidadesSelecionadas;
	}

	/**
	 * Is habilitar historico.
	 * 
	 * @return true, if is habilitar historico
	 */
	public boolean isHabilitarHistorico() {
		return habilitarHistorico;
	}

	/**
	 * Set: habilitarHistorico.
	 * 
	 * @param habilitarHistorico
	 *            the habilitar historico
	 */
	public void setHabilitarHistorico(boolean habilitarHistorico) {
		this.habilitarHistorico = habilitarHistorico;
	}

	/**
	 * Is habilita municipio alterar.
	 * 
	 * @return true, if is habilita municipio alterar
	 */
	public boolean isHabilitaMunicipioAlterar() {
		return habilitaMunicipioAlterar;
	}

	/**
	 * Set: habilitaMunicipioAlterar.
	 * 
	 * @param habilitaMunicipioAlterar
	 *            the habilita municipio alterar
	 */
	public void setHabilitaMunicipioAlterar(boolean habilitaMunicipioAlterar) {
		this.habilitaMunicipioAlterar = habilitaMunicipioAlterar;
	}

	/**
	 * Is habilita radio municipio feriado local.
	 * 
	 * @return true, if is habilita radio municipio feriado local
	 */
	public boolean isHabilitaRadioMunicipioFeriadoLocal() {
		return habilitaRadioMunicipioFeriadoLocal;
	}

	/**
	 * Set: habilitaRadioMunicipioFeriadoLocal.
	 * 
	 * @param habilitaRadioMunicipioFeriadoLocal
	 *            the habilita radio municipio feriado local
	 */
	public void setHabilitaRadioMunicipioFeriadoLocal(boolean habilitaRadioMunicipioFeriadoLocal) {
		this.habilitaRadioMunicipioFeriadoLocal = habilitaRadioMunicipioFeriadoLocal;
	}

	/**
	 * Get: cpf.
	 * 
	 * @return cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Set: cpf.
	 * 
	 * @param cpf
	 *            the cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Get: razao.
	 * 
	 * @return razao
	 */
	public String getRazao() {
		return razao;
	}

	/**
	 * Set: razao.
	 * 
	 * @param razao
	 *            the razao
	 */
	public void setRazao(String razao) {
		this.razao = razao;
	}

	/**
	 * Get: cdAgencia.
	 * 
	 * @return cdAgencia
	 */
	public Integer getCdAgencia() {
		return cdAgencia;
	}

	/**
	 * Set: cdAgencia.
	 * 
	 * @param cdAgencia
	 *            the cd agencia
	 */
	public void setCdAgencia(Integer cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	/**
	 * Get: codBanco.
	 * 
	 * @return codBanco
	 */
	public Integer getCodBanco() {
		return codBanco;
	}

	/**
	 * Set: codBanco.
	 * 
	 * @param codBanco
	 *            the cod banco
	 */
	public void setCodBanco(Integer codBanco) {
		this.codBanco = codBanco;
	}

	/**
	 * Get: itemSelecionadoListaContasRelHistorico.
	 * 
	 * @return itemSelecionadoListaContasRelHistorico
	 */
	public Integer getItemSelecionadoListaContasRelHistorico() {
		return itemSelecionadoListaContasRelHistorico;
	}

	/**
	 * Set: itemSelecionadoListaContasRelHistorico.
	 * 
	 * @param itemSelecionadoListaContasRelHistorico
	 *            the item selecionado lista contas rel historico
	 */
	public void setItemSelecionadoListaContasRelHistorico(Integer itemSelecionadoListaContasRelHistorico) {
		this.itemSelecionadoListaContasRelHistorico = itemSelecionadoListaContasRelHistorico;
	}

	/**
	 * Get: listaControleRadioContasRelHistorico.
	 * 
	 * @return listaControleRadioContasRelHistorico
	 */
	public List<SelectItem> getListaControleRadioContasRelHistorico() {
		return listaControleRadioContasRelHistorico;
	}

	/**
	 * Set: listaControleRadioContasRelHistorico.
	 * 
	 * @param listaControleRadioContasRelHistorico
	 *            the lista controle radio contas rel historico
	 */
	public void setListaControleRadioContasRelHistorico(List<SelectItem> listaControleRadioContasRelHistorico) {
		this.listaControleRadioContasRelHistorico = listaControleRadioContasRelHistorico;
	}

	/**
	 * Get: listaGridPesquisaContasRelHistorico.
	 * 
	 * @return listaGridPesquisaContasRelHistorico
	 */
	public List<ListarContasRelacionadasHistSaidaDTO> getListaGridPesquisaContasRelHistorico() {
		return listaGridPesquisaContasRelHistorico;
	}

	/**
	 * Set: listaGridPesquisaContasRelHistorico.
	 * 
	 * @param listaGridPesquisaContasRelHistorico
	 *            the lista grid pesquisa contas rel historico
	 */
	public void setListaGridPesquisaContasRelHistorico(
			List<ListarContasRelacionadasHistSaidaDTO> listaGridPesquisaContasRelHistorico) {
		this.listaGridPesquisaContasRelHistorico = listaGridPesquisaContasRelHistorico;
	}

	/**
	 * Get: tipoManutencao.
	 * 
	 * @return tipoManutencao
	 */
	public String getTipoManutencao() {
		return tipoManutencao;
	}

	/**
	 * Set: tipoManutencao.
	 * 
	 * @param tipoManutencao
	 *            the tipo manutencao
	 */
	public void setTipoManutencao(String tipoManutencao) {
		this.tipoManutencao = tipoManutencao;
	}

	/**
	 * Is habilitar conta.
	 * 
	 * @return true, if is habilitar conta
	 */
	public boolean isHabilitarConta() {
		return habilitarConta;
	}

	/**
	 * Set: habilitarConta.
	 * 
	 * @param habilitarConta
	 *            the habilitar conta
	 */
	public void setHabilitarConta(boolean habilitarConta) {
		this.habilitarConta = habilitarConta;
	}

	/**
	 * Get: cboTipoRelacionamento.
	 * 
	 * @return cboTipoRelacionamento
	 */
	public Integer getCboTipoRelacionamento() {
		return cboTipoRelacionamento;
	}

	/**
	 * Set: cboTipoRelacionamento.
	 * 
	 * @param cboTipoRelacionamento
	 *            the cbo tipo relacionamento
	 */
	public void setCboTipoRelacionamento(Integer cboTipoRelacionamento) {
		this.cboTipoRelacionamento = cboTipoRelacionamento;
	}

	/**
	 * Get: listaCboTipoRelacionamento.
	 * 
	 * @return listaCboTipoRelacionamento
	 */
	public List<SelectItem> getListaCboTipoRelacionamento() {
		return listaCboTipoRelacionamento;
	}

	/**
	 * Set: listaCboTipoRelacionamento.
	 * 
	 * @param listaCboTipoRelacionamento
	 *            the lista cbo tipo relacionamento
	 */
	public void setListaCboTipoRelacionamento(List<SelectItem> listaCboTipoRelacionamento) {
		this.listaCboTipoRelacionamento = listaCboTipoRelacionamento;
	}

	/**
	 * Get: clubRelacionadoDesc.
	 * 
	 * @return clubRelacionadoDesc
	 */
	public String getClubRelacionadoDesc() {
		return clubRelacionadoDesc;
	}

	/**
	 * Set: clubRelacionadoDesc.
	 * 
	 * @param clubRelacionadoDesc
	 *            the club relacionado desc
	 */
	public void setClubRelacionadoDesc(String clubRelacionadoDesc) {
		this.clubRelacionadoDesc = clubRelacionadoDesc;
	}

	/**
	 * Get: tipoRelacionamentoDesc.
	 * 
	 * @return tipoRelacionamentoDesc
	 */
	public String getTipoRelacionamentoDesc() {
		return tipoRelacionamentoDesc;
	}

	/**
	 * Set: tipoRelacionamentoDesc.
	 * 
	 * @param tipoRelacionamentoDesc
	 *            the tipo relacionamento desc
	 */
	public void setTipoRelacionamentoDesc(String tipoRelacionamentoDesc) {
		this.tipoRelacionamentoDesc = tipoRelacionamentoDesc;
	}

	/**
	 * Get: tipoContaDesc.
	 * 
	 * @return tipoContaDesc
	 */
	public String getTipoContaDesc() {
		return tipoContaDesc;
	}

	/**
	 * Set: tipoContaDesc.
	 * 
	 * @param tipoContaDesc
	 *            the tipo conta desc
	 */
	public void setTipoContaDesc(String tipoContaDesc) {
		this.tipoContaDesc = tipoContaDesc;
	}

	/**
	 * Get: agenciaDescFormatado.
	 * 
	 * @return agenciaDescFormatado
	 */
	public String getAgenciaDescFormatado() {
		return cdAgencia + " - " + agenciaDesc;
	}

	/**
	 * Set: agenciaDescFormatado.
	 * 
	 * @param agenciaDescFormatado
	 *            the agencia desc formatado
	 */
	public void setAgenciaDescFormatado(String agenciaDescFormatado) {
		this.agenciaDescFormatado = agenciaDescFormatado;
	}

	/**
	 * Get: bancoDescFormatado.
	 * 
	 * @return bancoDescFormatado
	 */
	public String getBancoDescFormatado() {
		return codBanco + " - " + bancoDesc;
	}

	/**
	 * Set: bancoDescFormatado.
	 * 
	 * @param bancoDescFormatado
	 *            the banco desc formatado
	 */
	public void setBancoDescFormatado(String bancoDescFormatado) {
		this.bancoDescFormatado = bancoDescFormatado;
	}

	/**
	 * Get: tipoRelacionamento.
	 * 
	 * @return tipoRelacionamento
	 */
	public Integer getTipoRelacionamento() {
		return tipoRelacionamento;
	}

	/**
	 * Set: tipoRelacionamento.
	 * 
	 * @param tipoRelacionamento
	 *            the tipo relacionamento
	 */
	public void setTipoRelacionamento(Integer tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}

	/**
	 * Get: tipoRelacionamentoHistorico.
	 * 
	 * @return tipoRelacionamentoHistorico
	 */
	public Integer getTipoRelacionamentoHistorico() {
		return tipoRelacionamentoHistorico;
	}

	/**
	 * Set: tipoRelacionamentoHistorico.
	 * 
	 * @param tipoRelacionamentoHistorico
	 *            the tipo relacionamento historico
	 */
	public void setTipoRelacionamentoHistorico(Integer tipoRelacionamentoHistorico) {
		this.tipoRelacionamentoHistorico = tipoRelacionamentoHistorico;
	}

	/**
	 * Get: cdTipoRelacionamentoHistorico.
	 * 
	 * @return cdTipoRelacionamentoHistorico
	 */
	public Integer getCdTipoRelacionamentoHistorico() {
		return cdTipoRelacionamentoHistorico;
	}

	/**
	 * Set: cdTipoRelacionamentoHistorico.
	 * 
	 * @param cdTipoRelacionamentoHistorico
	 *            the cd tipo relacionamento historico
	 */
	public void setCdTipoRelacionamentoHistorico(Integer cdTipoRelacionamentoHistorico) {
		this.cdTipoRelacionamentoHistorico = cdTipoRelacionamentoHistorico;
	}

	/**
	 * Get: dsMunicipio.
	 * 
	 * @return dsMunicipio
	 */
	public String getDsMunicipio() {
		return dsMunicipio;
	}

	/**
	 * Set: dsMunicipio.
	 * 
	 * @param dsMunicipio
	 *            the ds municipio
	 */
	public void setDsMunicipio(String dsMunicipio) {
		this.dsMunicipio = dsMunicipio;
	}

	/**
	 * Get: dsUnidFede.
	 * 
	 * @return dsUnidFede
	 */
	public String getDsUnidFede() {
		return dsUnidFede;
	}

	/**
	 * Set: dsUnidFede.
	 * 
	 * @param dsUnidFede
	 *            the ds unid fede
	 */
	public void setDsUnidFede(String dsUnidFede) {
		this.dsUnidFede = dsUnidFede;
	}

	/**
	 * Get: cdMunicipioFeri.
	 * 
	 * @return cdMunicipioFeri
	 */
	public Integer getCdMunicipioFeri() {
		return cdMunicipioFeri;
	}

	/**
	 * Set: cdMunicipioFeri.
	 * 
	 * @param cdMunicipioFeri
	 *            the cd municipio feri
	 */
	public void setCdMunicipioFeri(Integer cdMunicipioFeri) {
		this.cdMunicipioFeri = cdMunicipioFeri;
	}

	/**
	 * Get: tpManutencao.
	 * 
	 * @return tpManutencao
	 */
	public String getTpManutencao() {
		return tpManutencao;
	}

	/**
	 * Set: tpManutencao.
	 * 
	 * @param tpManutencao
	 *            the tp manutencao
	 */
	public void setTpManutencao(String tpManutencao) {
		this.tpManutencao = tpManutencao;
	}

	/**
	 * Get: valorLimite.
	 * 
	 * @return valorLimite
	 */
	public BigDecimal getValorLimite() {
		return valorLimite;
	}

	/**
	 * Set: valorLimite.
	 * 
	 * @param valorLimite
	 *            the valor limite
	 */
	public void setValorLimite(BigDecimal valorLimite) {
		this.valorLimite = valorLimite;
	}

	/**
	 * Get: dataPrazoInicio.
	 * 
	 * @return dataPrazoInicio
	 */
	public Date getDataPrazoInicio() {
		return dataPrazoInicio;
	}

	/**
	 * Set: dataPrazoInicio.
	 * 
	 * @param dataPrazoInicio
	 *            the data prazo inicio
	 */
	public void setDataPrazoInicio(Date dataPrazoInicio) {
		this.dataPrazoInicio = dataPrazoInicio;
	}

	/**
	 * Get: dataPrazoFim.
	 * 
	 * @return dataPrazoFim
	 */
	public Date getDataPrazoFim() {
		return dataPrazoFim;
	}

	/**
	 * Set: dataPrazoFim.
	 * 
	 * @param dataPrazoFim
	 *            the data prazo fim
	 */
	public void setDataPrazoFim(Date dataPrazoFim) {
		this.dataPrazoFim = dataPrazoFim;
	}

	/**
	 * Get: valorLimiteTED.
	 * 
	 * @return valorLimiteTED
	 */
	public BigDecimal getValorLimiteTED() {
		return valorLimiteTED;
	}

	/**
	 * Set: valorLimiteTED.
	 * 
	 * @param valorLimiteTED
	 *            the valor limite ted
	 */
	public void setValorLimiteTED(BigDecimal valorLimiteTED) {
		this.valorLimiteTED = valorLimiteTED;
	}

	/**
	 * Get: dataPrazoInicioTED.
	 * 
	 * @return dataPrazoInicioTED
	 */
	public Date getDataPrazoInicioTED() {
		return dataPrazoInicioTED;
	}

	/**
	 * Set: dataPrazoInicioTED.
	 * 
	 * @param dataPrazoInicioTED
	 *            the data prazo inicio ted
	 */
	public void setDataPrazoInicioTED(Date dataPrazoInicioTED) {
		this.dataPrazoInicioTED = dataPrazoInicioTED;
	}

	/**
	 * Get: dataPrazoFimTED.
	 * 
	 * @return dataPrazoFimTED
	 */
	public Date getDataPrazoFimTED() {
		return dataPrazoFimTED;
	}

	/**
	 * Set: dataPrazoFimTED.
	 * 
	 * @param dataPrazoFimTED
	 *            the data prazo fim ted
	 */
	public void setDataPrazoFimTED(Date dataPrazoFimTED) {
		this.dataPrazoFimTED = dataPrazoFimTED;
	}

	/**
	 * Get: prazoValidadeAltDesc.
	 * 
	 * @return prazoValidadeAltDesc
	 */
	public String getPrazoValidadeAltDesc() {
		return prazoValidadeAltDesc;
	}

	/**
	 * Set: prazoValidadeAltDesc.
	 * 
	 * @param prazoValidadeAltDesc
	 *            the prazo validade alt desc
	 */
	public void setPrazoValidadeAltDesc(String prazoValidadeAltDesc) {
		this.prazoValidadeAltDesc = prazoValidadeAltDesc;
	}

	/**
	 * Get: prazoValidadeTEDAltDesc.
	 * 
	 * @return prazoValidadeTEDAltDesc
	 */
	public String getPrazoValidadeTEDAltDesc() {
		return prazoValidadeTEDAltDesc;
	}

	/**
	 * Set: prazoValidadeTEDAltDesc.
	 * 
	 * @param prazoValidadeTEDAltDesc
	 *            the prazo validade ted alt desc
	 */
	public void setPrazoValidadeTEDAltDesc(String prazoValidadeTEDAltDesc) {
		this.prazoValidadeTEDAltDesc = prazoValidadeTEDAltDesc;
	}

	/**
	 * Get: manterContratoBean.
	 * 
	 * @return manterContratoBean
	 */
	public ManterContratoBean getManterContratoBean() {
		return manterContratoBean;
	}

	/**
	 * Set: manterContratoBean.
	 * 
	 * @param manterContratoBean
	 *            the manter contrato bean
	 */
	public void setManterContratoBean(ManterContratoBean manterContratoBean) {
		this.manterContratoBean = manterContratoBean;
	}

	/**
	 * Get: cdCpfCnpjRepresentante.
	 * 
	 * @return cdCpfCnpjRepresentante
	 */
	public Long getCdCpfCnpjRepresentante() {
		return cdCpfCnpjRepresentante;
	}

	/**
	 * Set: cdCpfCnpjRepresentante.
	 * 
	 * @param cdCpfCnpjRepresentante
	 *            the cd cpf cnpj representante
	 */
	public void setCdCpfCnpjRepresentante(Long cdCpfCnpjRepresentante) {
		this.cdCpfCnpjRepresentante = cdCpfCnpjRepresentante;
	}

	/**
	 * Get: cdFilialCpfCnpjRepresentante.
	 * 
	 * @return cdFilialCpfCnpjRepresentante
	 */
	public Integer getCdFilialCpfCnpjRepresentante() {
		return cdFilialCpfCnpjRepresentante;
	}

	/**
	 * Set: cdFilialCpfCnpjRepresentante.
	 * 
	 * @param cdFilialCpfCnpjRepresentante
	 *            the cd filial cpf cnpj representante
	 */
	public void setCdFilialCpfCnpjRepresentante(Integer cdFilialCpfCnpjRepresentante) {
		this.cdFilialCpfCnpjRepresentante = cdFilialCpfCnpjRepresentante;
	}

	/**
	 * Get: cdControleCpfCnpjRepresentante.
	 * 
	 * @return cdControleCpfCnpjRepresentante
	 */
	public Integer getCdControleCpfCnpjRepresentante() {
		return cdControleCpfCnpjRepresentante;
	}

	/**
	 * Set: cdControleCpfCnpjRepresentante.
	 * 
	 * @param cdControleCpfCnpjRepresentante
	 *            the cd controle cpf cnpj representante
	 */
	public void setCdControleCpfCnpjRepresentante(Integer cdControleCpfCnpjRepresentante) {
		this.cdControleCpfCnpjRepresentante = cdControleCpfCnpjRepresentante;
	}

	public List<ListarContasRelacionadasParaContratoSaidaDTO> getListaGridPesquisaSubst() {
		return listaGridPesquisaSubst;
	}

	public void setListaGridPesquisaSubst(List<ListarContasRelacionadasParaContratoSaidaDTO> listaGridPesquisaSubst) {
		this.listaGridPesquisaSubst = listaGridPesquisaSubst;
	}

	public List<ListarContasRelacionadasParaContratoSaidaDTO> getListaGridPesquisaContasRelInc() {
		return listaGridPesquisaContasRelInc;
	}

	public void setListaGridPesquisaContasRelInc(
			List<ListarContasRelacionadasParaContratoSaidaDTO> listaGridPesquisaContasRelInc) {
		this.listaGridPesquisaContasRelInc = listaGridPesquisaContasRelInc;
	}

	public List<ListarContasRelacionadasParaContratoSaidaDTO> getListaOcorrenciasIncluirRelAux2() {
		return listaOcorrenciasIncluirRelAux2;
	}

	public void setListaOcorrenciasIncluirRelAux2(
			List<ListarContasRelacionadasParaContratoSaidaDTO> listaOcorrenciasIncluirRelAux2) {
		this.listaOcorrenciasIncluirRelAux2 = listaOcorrenciasIncluirRelAux2;
	}

	public Integer getCodBancoPrincipal() {
		return codBancoPrincipal;
	}

	public void setCodBancoPrincipal(Integer codBancoPrincipal) {
		this.codBancoPrincipal = codBancoPrincipal;
	}

	public Integer getCodAgenciaPrincipal() {
		return codAgenciaPrincipal;
	}

	public void setCodAgenciaPrincipal(Integer codAgenciaPrincipal) {
		this.codAgenciaPrincipal = codAgenciaPrincipal;
	}

	public Integer getCodDigAgenciaPrincipal() {
		return codDigAgenciaPrincipal;
	}

	public void setCodDigAgenciaPrincipal(Integer codDigAgenciaPrincipal) {
		this.codDigAgenciaPrincipal = codDigAgenciaPrincipal;
	}

	public Long getCodContaPrincipal() {
		return codContaPrincipal;
	}

	public void setCodContaPrincipal(Long codContaPrincipal) {
		this.codContaPrincipal = codContaPrincipal;
	}

	public String getCodDigitoContaPrincipal() {
		return codDigitoContaPrincipal;
	}

	public void setCodDigitoContaPrincipal(String codDigitoContaPrincipal) {
		this.codDigitoContaPrincipal = codDigitoContaPrincipal;
	}

	public String getFinalidadeDescSub() {
		return finalidadeDescSub;
	}

	public void setFinalidadeDescSub(String finalidadeDescSub) {
		this.finalidadeDescSub = finalidadeDescSub;
	}

	/**
	 * Get: descApelido
	 * @return the descApelido
	 */
	public String getDescApelido() {
		return descApelido;
	}

	/**
	 * Set: descApelido
	 * @param descApelido the descApelido to set
	 */
	public void setDescApelido(String descApelido) {
		this.descApelido = descApelido;
	}

	public String getCdTipoInscricao() {
		return cdTipoInscricao;
	}

	public void setCdTipoInscricao(String cdTipoInscricao) {
		this.cdTipoInscricao = cdTipoInscricao;
	}

}
