/*
 * Nome: br.com.bradesco.web.pgit.view.converters
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.converters;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.bradesco.web.pgit.exception.PgitException;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: FormatarData
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public final class FormatarData {

	/** Atributo LOCALE_BR. */
	private static final Locale LOCALE_BR = new Locale("pt","BR");

	/** Atributo DATE_FORMAT_PDC. */
	private static final String DATE_FORMAT_PDC = "dd.MM.yyyy";

	/** Atributo DATE_FORMAT. */
	private static final String DATE_FORMAT = "dd/MM/yyyy";

	/** Atributo TIMESTAMP_FORMAT_PDC. */
	private static final String TIMESTAMP_FORMAT_PDC = "yyyy-MM-dd-HH.mm.ss";

	/** Atributo TIMESTAMP_FORMAT. */
	private static final String TIMESTAMP_FORMAT = "dd/MM/yyyy - HH:mm:ss";
	
	/** Atributo FORMATO1. */
	private static final SimpleDateFormat FORMATO1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");		
	
	/** Atributo FORMATO2. */
	private static final SimpleDateFormat FORMATO2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	/** Atributo FORMATO_DIA_MES_ANO_PDC. */
	private static final SimpleDateFormat FORMATO_DIA_MES_ANO_PDC = new SimpleDateFormat(DATE_FORMAT_PDC);		
	
	/** Atributo FORMATO_DIA_MES_ANO. */
	private static final SimpleDateFormat FORMATO_DIA_MES_ANO = new SimpleDateFormat(DATE_FORMAT);
	
	/**
	 * Formatar data.
	 */
	private FormatarData() {
		super();
	}

	/**
	 * Formata ano mes dia.
	 *
	 * @param dataInteira the data inteira
	 * @return the string
	 */
	public static String formataAnoMesDia(Date dataInteira){
       	return formataData(dataInteira, "yyyy/MM/dd");
    }

	/**
	 * Formata dia mes ano.
	 *
	 * @param dataInteira the data inteira
	 * @return the string
	 */
	public static String formataDiaMesAno(Date dataInteira){
        return formataData(dataInteira, DATE_FORMAT);
    }
	
	/**
	 * Formata dia mes ano to pdc.
	 *
	 * @param dataInteira the data inteira
	 * @return the string
	 */
	public static String formataDiaMesAnoToPdc(Date dataInteira){
        return formataData(dataInteira, DATE_FORMAT_PDC);
    }

	/**
	 * Formata dia mes ano from pdc.
	 *
	 * @param data the data
	 * @return the date
	 */
	public static Date formataDiaMesAnoFromPdc(String data) {   
        return formataData(data, DATE_FORMAT_PDC);   
    }

	/**
	 * Formata string data.
	 *
	 * @param data the data
	 * @return the date
	 */
	public static Date formataStringData(String data) {   
        return formataData(data, DATE_FORMAT);
    }

	/**
	 * Formata timestamp from pdc.
	 *
	 * @param data the data
	 * @return the date
	 */
	public static Date formataTimestampFromPdc(String data) {   
        return formataData(data, TIMESTAMP_FORMAT_PDC);
    }

	/**
	 * Formata timestamp from pdc.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String formataTimestampFromPdc(Date data) {   
        return formataData(data, TIMESTAMP_FORMAT);
    }

	/**
	 * Formata timestamp to pdc.
	 *
	 * @param data the data
	 * @return the date
	 */
	public static Date formataTimestampToPdc(String data) {   
        return formataData(data, TIMESTAMP_FORMAT);
    }

	/**
	 * Formata timestamp to pdc.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String formataTimestampToPdc(Date data) {   
        return formataData(data, TIMESTAMP_FORMAT_PDC);
    }

	/**
	 * Formata data.
	 *
	 * @param data the data
	 * @param mask the mask
	 * @return the date
	 */
	public static Date formataData(String data, String mask) {
		if (data == null || data.equals("")){   
            return null;
		}

			
		Date date = null;   
        try {   
            DateFormat formatter = new SimpleDateFormat(mask, LOCALE_BR);   
            date = (java.util.Date)formatter.parse(data);   
        } catch (ParseException e) {               
            throw new PgitException(e.getMessage(), e, "");   
        }
        return date;
	}

	/**
	 * Formata data.
	 *
	 * @param data the data
	 * @param mask the mask
	 * @return the string
	 */
	public static String formataData(Date data, String mask){
       	if(data != null){
        	SimpleDateFormat formatoSaida = new SimpleDateFormat(mask, LOCALE_BR);
	        return formatoSaida.format(data);
       	}
        return "";
    }
	
	/**
	 * Formatar data trilha.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String formatarDataTrilha(String data){
		
		if (data != null && data.length() >=19 ){
			
			String stringData = data.substring(0, 19);
			
			try {
				return FORMATO2.format(FORMATO1.parse(stringData));
			}  catch (ParseException e) {
				return "";
			}
		}else{
			return "";
		}
    }
	
	/**
	 * Formatar data.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String formatarData(String data){
		
		
		
		try {
			return FORMATO_DIA_MES_ANO.format(FORMATO_DIA_MES_ANO_PDC.parse(data));
		}  catch (ParseException e) {
			return "";
		}
		
        
    }
	
	/**
	 * Formatar data to pdc.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String formatarDataToPdc(String data){		
		
			try {
				return FORMATO_DIA_MES_ANO_PDC.format(FORMATO_DIA_MES_ANO.parse(data));
			} catch (ParseException e) {
				return "";
			}
		
        
    }
	
	/**
	 * Formatar data from pdc.
	 *
	 * @param data the data
	 * @return the string
	 */
	public static String formatarDataFromPdc(String data){		
		
		try {
			return FORMATO_DIA_MES_ANO.format(FORMATO_DIA_MES_ANO_PDC.parse(data));
		} catch (ParseException e) {
			return "";
		}
	
    
	}
	
	/***
	 * M�todo para formatar um inteiro em data mm/yyyy
	 * Ex.: 
	 * 		 12010 -> 01/2010
	 * 		112010 -> 11/2010
	 * @param data
	 * @return
	 */
	public static String formatarDataInteira(Integer data){
		if (data !=null && data.intValue() == 0){
			return "";
		}
		
		if(data != null && (String.valueOf(data).length() == 6 || String.valueOf(data).length() == 5)){
			String dataConvertida = PgitUtil.complementaDigito(String.valueOf(data), 6);
			StringBuffer dataFormatada = new StringBuffer(dataConvertida);
			dataFormatada.insert(2, '/');
			return dataFormatada.toString();
		}
		else{
			return "";
		}
	}
	
}
