/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assocmsgcompsal
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assocmsgcompsal;


import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.IAssocMsgCompSalService;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.DetalharAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.DetalharAssociacaoContratoMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ExcluirAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ExcluirAssociacaoContratoMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.IncluirAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.IncluirAssociacaoContratoMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ListarAssociacaoContratoMsgEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assocmsgcompsal.bean.ListarAssociacaoContratoMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoMensagemEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoMensagemSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;

/**
 * Nome: AssocMsgCompSalBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AssocMsgCompSalBean extends IdentificacaoClienteBean {
	
	//Variaveis Pesquisar		
	/** Atributo codigoTipoMensagemFiltro. */
	private Integer codigoTipoMensagemFiltro;
	
	/** Atributo descTipoMensagemFiltro. */
	private String descTipoMensagemFiltro;
	
	/** Atributo listaGrid. */
	private List<ListarAssociacaoContratoMsgSaidaDTO> listaGrid;
	
	/** Atributo itemSelecionadoListaGrid. */
	private Integer itemSelecionadoListaGrid;
	
	/** Atributo listaGridControle. */
	private List<SelectItem> listaGridControle;
	//Fim Variaveis Pesquisar	

	//Variaveis Incluir - Detalhar - Excluir
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo nomeRazao. */
	private String nomeRazao;
	
	/** Atributo contratoEmpresa. */
	private String contratoEmpresa;
	
	/** Atributo contratoTipo. */
	private String contratoTipo;
	
	/** Atributo contratoNumero. */
	private String contratoNumero;
	
	/** Atributo contratoDescricaoContrato. */
	private String contratoDescricaoContrato;
	
	/** Atributo contratoSituacao. */
	private String contratoSituacao;
	
	/** Atributo tipoMensagem. */
	private Integer tipoMensagem;	 
	
	/** Atributo tipoMensagemDesc. */
	private String tipoMensagemDesc;
	
	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;
	//Fim Variaveis Incluir - Detalhar - Excluir 
	
	//Variaveis Trilha Auditoria
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;	
	//Fim variaveis trilha auditoria
	
	/** Atributo assocMsgCompSalServiceImpl. */
	private IAssocMsgCompSalService assocMsgCompSalServiceImpl;

	//Combos
	/** Atributo preencherTipoContrato. */
	private List<SelectItem> preencherTipoContrato= new ArrayList<SelectItem>();
	
	/** Atributo preencherTipoMensagem. */
	private List<SelectItem> preencherTipoMensagem= new ArrayList<SelectItem>();
	
	/** Atributo habilitaFiltroDeCliente. */
	private boolean habilitaFiltroDeCliente;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	
	/**
	 * Assoc msg comp sal bean.
	 */
	public AssocMsgCompSalBean() {
		super("conManterAssociacaoMsgComprovanteSalarial", "identificacaoAssocMsgCompSalarialCliente", "identificacaoAssocMsgCompSalarialContrato");
	}
	
	//Metodos
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt){
		this.setItemFiltroSelecionado(null);
		this.setDesabilatalimpar(true);
		this.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this.setPreencherListaEmpresaGestora(new ArrayList<SelectItem>());
		this.setPreencherTipoContrato(new ArrayList<SelectItem>());
		this.setHabilitaFiltroDeContrato(true);
		this.setBloqueaTipoContrato(true);
		this.setDesabilataFiltro(true);
		this.setBloqueaRadio(false);
		loadEmpresaGestora();
		loadTipoContrato();
		listarTipoMensagem();
		setBtoAcionado(false);
		getEntradaConsultarListaContratosPessoas().setCdPessoaJuridicaContrato(2269651L);
	}
	
	/**
	 * Limpar dados cliente.
	 *
	 * @return the string
	 */
	public String limparDadosCliente(){
		limparCliente();
		limparArgumentosPesquisa();
		return "";
	}
	
	/**
	 * Limpar argumentos pesquisa.
	 *
	 * @return the string
	 */
	public String limparArgumentosPesquisa(){
		setCodigoTipoMensagemFiltro(0);
		limparGrid();
		setBtoAcionado(false);
		return "";
	}
	
	/**
	 * Limpar dados contrato.
	 *
	 * @return the string
	 */
	public String limparDadosContrato(){
		limparContrato();
		limparArgumentosPesquisa();
		return "";
	}
	
	/**
	 * Limpar grid.
	 */
	public void limparGrid(){
		setListaGrid(null);
		setItemSelecionadoListaGrid(null);
	}
	
	/**
	 * Limpar pagina total.
	 *
	 * @return the string
	 */
	public String limparPaginaTotal(){
		this.setItemFiltroSelecionado("0");
		this.setDesabilatalimpar(true);
		this.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.setPreencherListaEmpresaGestora(new ArrayList<SelectItem>());
		this.setPreencherTipoContrato(new ArrayList<SelectItem>());
		this.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this.setHabilitaFiltroDeContrato(true);
		this.setHabilitaFiltroDeCliente(false);
		this.setBloqueaTipoContrato(true);
		this.setDesabilataFiltro(true);
		this.setBloqueaRadio(false);
		this.loadEmpresaGestora();
		this.loadTipoContrato();
		this.listarTipoMensagem();
		setBtoAcionado(false);
		setListaGrid(null);
		setCodigoTipoMensagemFiltro(null);
		return "conManterAssociacaoMsgComprovanteSalarial";
	}
	
	/**
	 * Limpa campos.
	 *
	 * @return the string
	 */
	public String limpaCampos(){
		limparCliente();
		limparContrato();
		limparArgumentosPesquisa();
		return "conManterAssociacaoMsgComprovanteSalarial";
	}

	/**
	 * Limpar variaveis.
	 */
	public void limparVariaveis(){
		setCpfCnpj("");
		setNomeRazao("");
		setContratoEmpresa("");
		setContratoTipo("");
		setContratoNumero("");
		setContratoDescricaoContrato("");
		setContratoSituacao("");
		setTipoMensagem(null);		
		setTipoMensagemDesc("");
		setBtoAcionado(false);
	}
	
	/**
	 * Consultar cliente.
	 *
	 * @return the string
	 */
	public String consultarCliente(){
		return"";
	}
	
	/**
	 * Consultar contrato.
	 *
	 * @return the string
	 */
	public String consultarContrato(){
		return "";
	}
	
	
	/**
	 * Limpar campos.
	 */
	public void limparCampos(){
		this.setItemFiltroSelecionado("");
		this.setHabilitaFiltroDeContrato(true);
		this.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this.setDesabilataFiltro(true);
		this.setHabilitaFiltroDeCliente(false);
		setCodigoTipoMensagemFiltro(null);
		setListaGrid(null);
		setItemSelecionadoListaGrid(null);
		setBtoAcionado(false);
	}
	
	/**
	 * Carrega lista.
	 */
	public void carregaLista(){
		
		try{
			ListarAssociacaoContratoMsgEntradaDTO entradaDTO = new ListarAssociacaoContratoMsgEntradaDTO();
			
			entradaDTO.setTipoMensagem(getCodigoTipoMensagemFiltro()!=null && !getCodigoTipoMensagemFiltro().equals(0)? (getCodigoTipoMensagemFiltro()):0);							
			entradaDTO.setContratoPssoaJurdContr(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			entradaDTO.setContratoTpoContrNegoc(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			entradaDTO.setContratoSeqContrNegoc(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			
			setListaGrid(getAssocMsgCompSalServiceImpl().listarAssociacaoContratoMsg(entradaDTO));
			setBtoAcionado(true);
			this.listaGridControle =  new ArrayList<SelectItem>();
			for (int i = 0; i <= getListaGrid().size();i++) {
				this.listaGridControle.add(new SelectItem(i," "));
			}
			
			this.removeRadioSelecionado();
			this.setHabilitaFiltroDeContrato(true);			
			this.setHabilitaFiltroDeCliente(true);
			this.setItemSelecionadoListaGrid(null);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);
			setItemSelecionadoListaGrid(null);
			setHabilitaFiltroDeCliente(false);
		}
	}
	
	/**
	 * Remove radio selecionado.
	 */
	public void removeRadioSelecionado() {
		setItemSelecionadoListaGrid(null);
	}	
	
	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora(){
		List<SelectItem> preencherListaEmpresaGestora = new ArrayList<SelectItem>();
		List<EmpresaGestoraSaidaDTO> list = new ArrayList<EmpresaGestoraSaidaDTO>();
		
		list = super.getComboService().listarEmpresasGestoras();
		for(EmpresaGestoraSaidaDTO combo : list){
			preencherListaEmpresaGestora.add(new SelectItem(combo.getCdPessoaJuridicaContrato(),combo.getDsCnpj()));
		}
	}
	
	
	/**
	 * Listar tipo mensagem.
	 */
	private void listarTipoMensagem(){
		try{
			ListarTipoMensagemEntradaDTO entrada = new ListarTipoMensagemEntradaDTO();
			
			entrada.setCdControleMensagemSalarial(0);			
			entrada.setCdTipoComprovanteSalarial(0);	
			entrada.setCdRestMensagemSalarial(0);
			entrada.setCdTipoMensagemSalarial(2);
			
			entrada.setNumeroOcorrencias(50);
			List<ListarTipoMensagemSaidaDTO> list = super.getComboService().listarTipoMensagem(entrada);
			
			preencherTipoMensagem.clear();
			
			for (ListarTipoMensagemSaidaDTO saida : list){
				preencherTipoMensagem.add(new SelectItem(saida.getCdControleMensagemSalarial(), PgitUtil.concatenarCampos(saida.getCdControleMensagemSalarial(), saida.getDsControleMensagemSalarial())));
			}
		}catch(PdcAdapterFunctionalException e){
			preencherTipoMensagem = new ArrayList<SelectItem>();
		}
	}
	
	/**
	 * Descricao mensagem.
	 */
	@SuppressWarnings("unused")
	private void descricaoMensagem(){
		
		for (int i = 0; i < preencherTipoMensagem.size(); i++){
			if (this.getCodigoTipoMensagemFiltro().equals(Integer.valueOf(preencherTipoMensagem.get(i).getValue().toString()))){
				this.setDescTipoMensagemFiltro(preencherTipoMensagem.get(i).getLabel());
				break;
			}
		}
	}
	
	/**
	 * Descricao mensagem incluir.
	 */
	private void descricaoMensagemIncluir(){
		
		for (int i = 0; i < preencherTipoMensagem.size(); i++){
			if (this.getTipoMensagem().equals(Integer.valueOf(preencherTipoMensagem.get(i).getValue().toString()))){
				this.setDescTipoMensagemFiltro(preencherTipoMensagem.get(i).getLabel());
				break;
			}
		}
	}	
	
	/**
	 * Listar tipo contrato.
	 */
	public void listarTipoContrato(){
		List<TipoContratoSaidaDTO> list = super.getComboService().listarTipoContrato();

		for (TipoContratoSaidaDTO saida : list){
			preencherTipoContrato.add(new SelectItem(saida.getCdTipoContrato(), saida.getDsTipoContrato()));
		}
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt){
		return;
	}
	
	/**
	 * Voltar cliente.
	 *
	 * @return the string
	 */
	public String voltarCliente(){
		setItemClienteSelecionado(null);
		
		return "VOLTAR";
	}
	
	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		setItemSelecionadoListaGrid(null);
		
		return "VOLTAR";
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "conManterAssociacaoLinhasTipoMensagem", BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "DETALHAR";
	}
	
	/**
	 * Preenche dados.
	 */
	private void preencheDados() {
		ListarAssociacaoContratoMsgSaidaDTO  listarAssociacaoContratoMsgSaidaDTO =  getListaGrid().get(getItemSelecionadoListaGrid());
		
		DetalharAssociacaoContratoMsgEntradaDTO detalharAssociacaoContratoMsgEntradaDTO =  new DetalharAssociacaoContratoMsgEntradaDTO();
		
		detalharAssociacaoContratoMsgEntradaDTO.setContratoPssoaJuridContr(listarAssociacaoContratoMsgSaidaDTO.getContratoPssoaJurdContr());
		detalharAssociacaoContratoMsgEntradaDTO.setContratoSeqContrNegoc(listarAssociacaoContratoMsgSaidaDTO.getContratoSeqContrNegoc());
		detalharAssociacaoContratoMsgEntradaDTO.setContratoTpoContrNegoc(listarAssociacaoContratoMsgSaidaDTO.getContratoTpoContrNegoc());
		detalharAssociacaoContratoMsgEntradaDTO.setTipoMensagem(listarAssociacaoContratoMsgSaidaDTO.getCodMensagemSalarial());
		
		DetalharAssociacaoContratoMsgSaidaDTO detalharAssociacaoContratoMsgSaidaDTO =  getAssocMsgCompSalServiceImpl().detalharAssociacaoContratoMsg(detalharAssociacaoContratoMsgEntradaDTO);
		
		setTipoMensagem(detalharAssociacaoContratoMsgSaidaDTO.getCodTipoMensagem());
		setTipoMensagemDesc(PgitUtil.concatenarCampos(detalharAssociacaoContratoMsgSaidaDTO.getCodTipoMensagem(), detalharAssociacaoContratoMsgSaidaDTO.getDsTipoMensagem()));
		setUsuarioInclusao(detalharAssociacaoContratoMsgSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharAssociacaoContratoMsgSaidaDTO.getUsuarioManutencao());		
		setComplementoInclusao(detalharAssociacaoContratoMsgSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharAssociacaoContratoMsgSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharAssociacaoContratoMsgSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharAssociacaoContratoMsgSaidaDTO.getComplementoManutencao());		
		setTipoCanalInclusao(detalharAssociacaoContratoMsgSaidaDTO.getCdCanalInclusao()==0? "" : detalharAssociacaoContratoMsgSaidaDTO.getCdCanalInclusao() + " - " +  detalharAssociacaoContratoMsgSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(detalharAssociacaoContratoMsgSaidaDTO.getCdCanalManutencao()==0? "" : detalharAssociacaoContratoMsgSaidaDTO.getCdCanalManutencao() + " - " + detalharAssociacaoContratoMsgSaidaDTO.getDsCanalManutencao());
		setDataHoraInclusao(detalharAssociacaoContratoMsgSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharAssociacaoContratoMsgSaidaDTO.getDataHoraManutencao());
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){		
		setTipoMensagem(null);
		return "INCLUIR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "conManterAssociacaoLinhasTipoMensagem", BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "EXCLUIR";
	}
	
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		if(getObrigatoriedade().equals("T")) {
			descricaoMensagemIncluir();
			return "AVANCAR_INCLUIR";
		} 
		
		return"";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR_INCLUIR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try {
			IncluirAssociacaoContratoMsgEntradaDTO incluirAssociacaoContratoMsgEntradaDTO = new IncluirAssociacaoContratoMsgEntradaDTO();

			incluirAssociacaoContratoMsgEntradaDTO.setTipoMensagem(getTipoMensagem());
			incluirAssociacaoContratoMsgEntradaDTO.setContratoPssoaJurdContr(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			incluirAssociacaoContratoMsgEntradaDTO.setContratoSeqContrNegoc(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			incluirAssociacaoContratoMsgEntradaDTO.setContratoTpoContrNegoc(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			
			IncluirAssociacaoContratoMsgSaidaDTO incluirAssociacaoContratoMsgSaidaDTO = getAssocMsgCompSalServiceImpl().incluirAssociacaoContratoMsg(incluirAssociacaoContratoMsgEntradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +incluirAssociacaoContratoMsgSaidaDTO.getCodMensagem() + ") " + incluirAssociacaoContratoMsgSaidaDTO.getMensagem(), "conManterAssociacaoMsgComprovanteSalarial", BradescoViewExceptionActionType.ACTION, false);
			
			carregaLista();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try {
			ExcluirAssociacaoContratoMsgEntradaDTO excluirAssociacaoContratoMsgEntradaDTO = new ExcluirAssociacaoContratoMsgEntradaDTO();
			
			excluirAssociacaoContratoMsgEntradaDTO.setTipoMensagem(getTipoMensagem());
			excluirAssociacaoContratoMsgEntradaDTO.setContratoPssoaJurdContr(this.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			excluirAssociacaoContratoMsgEntradaDTO.setContratoSeqContrNegoc(this.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			excluirAssociacaoContratoMsgEntradaDTO.setContratoTpoContrNegoc(this.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			
			ExcluirAssociacaoContratoMsgSaidaDTO excluirAssociacaoContratoMsgSaidaDTO = getAssocMsgCompSalServiceImpl().excluirAssociacaoContratoMsg(excluirAssociacaoContratoMsgEntradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +excluirAssociacaoContratoMsgSaidaDTO.getCodMensagem() + ") " + excluirAssociacaoContratoMsgSaidaDTO.getMensagem(), "conManterAssociacaoMsgComprovanteSalarial", BradescoViewExceptionActionType.ACTION, false);
			
			carregaLista();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		
		return "";
	}
	
	/**
	 * Limpar campos de associcao msg comp.
	 *
	 * @return the string
	 */
	public String limparCamposDeAssocicaoMsgComp() {
		this.codigoTipoMensagemFiltro = null;
		this.listaGrid = new ArrayList<ListarAssociacaoContratoMsgSaidaDTO>();
		
		return "conManterAssociacaoMsgComprovanteSalarial";
	}
	
	//Set's & Get's --------------------------------------------------
	/**
	 * Get: codigoTipoMensagemFiltro.
	 *
	 * @return codigoTipoMensagemFiltro
	 */
	public Integer getCodigoTipoMensagemFiltro() {
		return codigoTipoMensagemFiltro;
	}
	
	/**
	 * Set: codigoTipoMensagemFiltro.
	 *
	 * @param codigoTipoMensagem the codigo tipo mensagem filtro
	 */
	public void setCodigoTipoMensagemFiltro(Integer codigoTipoMensagem) {
		this.codigoTipoMensagemFiltro = codigoTipoMensagem;
	}
	
	
	/**
	 * Get: itemSelecionadoListaGrid.
	 *
	 * @return itemSelecionadoListaGrid
	 */
	public Integer getItemSelecionadoListaGrid() {
		return itemSelecionadoListaGrid;
	}
	
	/**
	 * Set: itemSelecionadoListaGrid.
	 *
	 * @param itemSelecionadoListaGrid the item selecionado lista grid
	 */
	public void setItemSelecionadoListaGrid(Integer itemSelecionadoListaGrid) {
		this.itemSelecionadoListaGrid = itemSelecionadoListaGrid;
	}
	
	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarAssociacaoContratoMsgSaidaDTO> getListaGrid() {
		return listaGrid;
	}
	
	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarAssociacaoContratoMsgSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}
	
	/**
	 * Get: listaGridControle.
	 *
	 * @return listaGridControle
	 */
	public List<SelectItem> getListaGridControle() {
		return listaGridControle;
	}
	
	/**
	 * Set: listaGridControle.
	 *
	 * @param listaGridControle the lista grid controle
	 */
	public void setListaGridControle(List<SelectItem> listaGridControle) {
		this.listaGridControle = listaGridControle;
	}

	/**
	 * Get: contratoDescricaoContrato.
	 *
	 * @return contratoDescricaoContrato
	 */
	public String getContratoDescricaoContrato() {
		return contratoDescricaoContrato;
	}

	/**
	 * Set: contratoDescricaoContrato.
	 *
	 * @param contratoDescricaoContrato the contrato descricao contrato
	 */
	public void setContratoDescricaoContrato(String contratoDescricaoContrato) {
		this.contratoDescricaoContrato = contratoDescricaoContrato;
	}

	/**
	 * Get: contratoEmpresa.
	 *
	 * @return contratoEmpresa
	 */
	public String getContratoEmpresa() {
		return contratoEmpresa;
	}

	/**
	 * Set: contratoEmpresa.
	 *
	 * @param contratoEmpresa the contrato empresa
	 */
	public void setContratoEmpresa(String contratoEmpresa) {
		this.contratoEmpresa = contratoEmpresa;
	}

	/**
	 * Get: contratoNumero.
	 *
	 * @return contratoNumero
	 */
	public String getContratoNumero() {
		return contratoNumero;
	}

	/**
	 * Set: contratoNumero.
	 *
	 * @param contratoNumero the contrato numero
	 */
	public void setContratoNumero(String contratoNumero) {
		this.contratoNumero = contratoNumero;
	}

	/**
	 * Get: contratoSituacao.
	 *
	 * @return contratoSituacao
	 */
	public String getContratoSituacao() {
		return contratoSituacao;
	}

	/**
	 * Set: contratoSituacao.
	 *
	 * @param contratoSituacao the contrato situacao
	 */
	public void setContratoSituacao(String contratoSituacao) {
		this.contratoSituacao = contratoSituacao;
	}

	/**
	 * Get: contratoTipo.
	 *
	 * @return contratoTipo
	 */
	public String getContratoTipo() {
		return contratoTipo;
	}

	/**
	 * Set: contratoTipo.
	 *
	 * @param contratoTipo the contrato tipo
	 */
	public void setContratoTipo(String contratoTipo) {
		this.contratoTipo = contratoTipo;
	}

	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	

	/**
	 * Get: nomeRazao.
	 *
	 * @return nomeRazao
	 */
	public String getNomeRazao() {
		return nomeRazao;
	}

	/**
	 * Set: nomeRazao.
	 *
	 * @param nomeRazao the nome razao
	 */
	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}

	/**
	 * Get: tipoMensagem.
	 *
	 * @return tipoMensagem
	 */
	public Integer getTipoMensagem() {
		return tipoMensagem;
	}

	/**
	 * Set: tipoMensagem.
	 *
	 * @param tipoMensagem the tipo mensagem
	 */
	public void setTipoMensagem(Integer tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

	/**
	 * Get: tipoMensagemDesc.
	 *
	 * @return tipoMensagemDesc
	 */
	public String getTipoMensagemDesc() {
		return tipoMensagemDesc;
	}

	/**
	 * Set: tipoMensagemDesc.
	 *
	 * @param tipoMensagemDesc the tipo mensagem desc
	 */
	public void setTipoMensagemDesc(String tipoMensagemDesc) {
		this.tipoMensagemDesc = tipoMensagemDesc;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: assocMsgCompSalServiceImpl.
	 *
	 * @return assocMsgCompSalServiceImpl
	 */
	public IAssocMsgCompSalService getAssocMsgCompSalServiceImpl() {
		return assocMsgCompSalServiceImpl;
	}

	/**
	 * Set: assocMsgCompSalServiceImpl.
	 *
	 * @param assocMsgCompSalServiceImpl the assoc msg comp sal service impl
	 */
	public void setAssocMsgCompSalServiceImpl(
			IAssocMsgCompSalService assocMsgCompSalServiceImpl) {
		this.assocMsgCompSalServiceImpl = assocMsgCompSalServiceImpl;
	}
	
	/**
	 * Limpar campos pesquisa cliente consultar.
	 */
	public void limparCamposPesquisaClienteConsultar(){
		this.getEntradaConsultarListaClientePessoas().setCdCpfCnpj(null);
		this.getEntradaConsultarListaClientePessoas().setCdFilialCnpj(null);
		this.getEntradaConsultarListaClientePessoas().setCdControleCnpj(null);
		this.getEntradaConsultarListaClientePessoas().setCdCpf(null);
		this.getEntradaConsultarListaClientePessoas().setCdControleCpf(null);
		this.getEntradaConsultarListaClientePessoas().setDsNomeRazaoSocial("");
		this.getEntradaConsultarListaClientePessoas().setCdBanco(null);
		this.getEntradaConsultarListaClientePessoas().setCdAgenciaBancaria(null);
		this.getEntradaConsultarListaClientePessoas().setCdContaBancaria(null);
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean#isHabilitaFiltroDeCliente()
	 */
	public boolean isHabilitaFiltroDeCliente() {
		return habilitaFiltroDeCliente;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean#setHabilitaFiltroDeCliente(boolean)
	 */
	public void setHabilitaFiltroDeCliente(boolean habilitaFiltroDeCliente) {
		this.habilitaFiltroDeCliente = habilitaFiltroDeCliente;
	}

	/**
	 * Get: preencherTipoMensagem.
	 *
	 * @return preencherTipoMensagem
	 */
	public List<SelectItem> getPreencherTipoMensagem() {
		return preencherTipoMensagem;
	}

	/**
	 * Set: preencherTipoMensagem.
	 *
	 * @param preencherTipoMensagem the preencher tipo mensagem
	 */
	public void setPreencherTipoMensagem(List<SelectItem> preencherTipoMensagem) {
		this.preencherTipoMensagem = preencherTipoMensagem;
	}

	/**
	 * Get: descTipoMensagemFiltro.
	 *
	 * @return descTipoMensagemFiltro
	 */
	public String getDescTipoMensagemFiltro() {
		return descTipoMensagemFiltro;
	}

	/**
	 * Set: descTipoMensagemFiltro.
	 *
	 * @param descTipoMensagemFiltro the desc tipo mensagem filtro
	 */
	public void setDescTipoMensagemFiltro(String descTipoMensagemFiltro) {
		this.descTipoMensagemFiltro = descTipoMensagemFiltro;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}
	
}
