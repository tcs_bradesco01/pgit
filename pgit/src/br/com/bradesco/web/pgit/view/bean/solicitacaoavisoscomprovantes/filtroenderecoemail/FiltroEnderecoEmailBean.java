/*
 * Nome: br.com.bradesco.web.pgit.view.bean.solicitacaoavisoscomprovantes.filtroenderecoemail
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.solicitacaoavisoscomprovantes.filtroenderecoemail;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEmailEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEmailSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEnderecoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEnderecoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;

/**
 * Nome: FiltroEnderecoEmailBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class FiltroEnderecoEmailBean {

	/** Atributo TELA_CONSULTAR_CLIENTE. */
	private static final String TELA_CONSULTAR_CLIENTE = "TELA_CONSULTAR_CLIENTE";

	/** Atributo TELA_CONSULTAR_ENDERECO. */
	private static final String TELA_CONSULTAR_ENDERECO = "TELA_CONSULTAR_ENDERECO";

	/** Atributo TELA_CONSULTAR_EMAIL. */
	private static final String TELA_CONSULTAR_EMAIL = "TELA_CONSULTAR_EMAIL";

	/** Atributo PESQUISA_POR_EMAIL. */
	private static final int PESQUISA_POR_EMAIL = 1;

	/** Atributo PESQUISA_POR_ENDERECO. */
	private static final int PESQUISA_POR_ENDERECO = 2;

	/** Atributo consultasServiceImpl. */
	private IConsultasService consultasServiceImpl;

	/** Atributo filtroIdentificaoServiceImpl. */
	private IFiltroIdentificaoService filtroIdentificaoServiceImpl;

	/** Atributo entradaCliente. */
	private ConsultarListaClientePessoasEntradaDTO entradaCliente;

	/** Atributos **/
	private List<ConsultarListaClientePessoasSaidaDTO> listaGridCliente;

	/** Atributo listaGridEndereco. */
	private List<ConsultarEnderecoSaidaDTO> listaGridEndereco;

	/** Atributo listaGridEmail. */
	private List<ConsultarEmailSaidaDTO> listaGridEmail;

	/** Atributo listaControleCliente. */
	private List<SelectItem> listaControleCliente = new ArrayList<SelectItem>();

	/** Atributo listaControleEnderecoEmail. */
	private List<SelectItem> listaControleEnderecoEmail = new ArrayList<SelectItem>();

	/** Atributo itemSelecionadoCliente. */
	private Integer itemSelecionadoCliente;

	/** Atributo itemSelecionadoEnderecoEmail. */
	private Integer itemSelecionadoEnderecoEmail;

	/** Atributo paginaRetorno. */
	private String paginaRetorno;

	/** Atributo radioFiltroConsultaEnderecoEmail. */
	private String radioFiltroConsultaEnderecoEmail;

	/** Atributo enderecoSelecionado. */
	private ConsultarEnderecoSaidaDTO enderecoSelecionado;

	/** Atributo emailSelecionado. */
	private ConsultarEmailSaidaDTO emailSelecionado;

	/** Atributo empresaGestoraContrato. */
	private Long empresaGestoraContrato;

	/** Atributo flagEnderecoEmail. */
	private Integer flagEnderecoEmail = 2; // 1 = PESQUISA_POR_EMAIL,2 =

	// PESQUISA_POR_ENDERECO

	/**
	 * Limpar campos.
	 */
	public void limparCampos() {
		limparArgumentosCliente();
		limparInformacaoCliente();
		limparInformacaoEnderecoEmail();

		setRadioFiltroConsultaEnderecoEmail("");
	}

	/**
	 * Limpar endereco email.
	 */
	public void limparEnderecoEmail() {
		setEnderecoSelecionado(new ConsultarEnderecoSaidaDTO());
		setEmailSelecionado(new ConsultarEmailSaidaDTO());
		limparCampos();
	}

	/**
	 * Limpar informacao cliente.
	 */
	private void limparInformacaoCliente() {
		setListaGridCliente(new ArrayList<ConsultarListaClientePessoasSaidaDTO>());
		setListaControleCliente(new ArrayList<SelectItem>());
		setItemSelecionadoCliente(null);
	}

	/**
	 * Limpar informacao endereco email.
	 */
	private void limparInformacaoEnderecoEmail() {
		setListaGridEndereco(new ArrayList<ConsultarEnderecoSaidaDTO>());
		setListaGridEmail(new ArrayList<ConsultarEmailSaidaDTO>());
		setListaControleEnderecoEmail(new ArrayList<SelectItem>());
		setItemSelecionadoEnderecoEmail(null);
	}

	/**
	 * Limpar argumentos cliente.
	 */
	public void limparArgumentosCliente() {
		setEntradaCliente(new ConsultarListaClientePessoasEntradaDTO());
	}

	/**
	 * Pesquisar endereco.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void pesquisarEndereco(ActionEvent evt) {
		pesquisarEndereco();
	}

	/**
	 * Pesquisar email.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void pesquisarEmail(ActionEvent evt) {
		pesquisarEmail();
	}

	/**
	 * Pesquisar cliente.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void pesquisarCliente(ActionEvent evt) {
		consultarClientes();
	}

	/**
	 * Consultar clientes.
	 * 
	 * @return the string
	 */
	public String consultarClientes() {
		limparInformacaoEnderecoEmail();
		limparInformacaoCliente();
		try {
			// Efetua busca do cliente
			// (pgic.favorecidos.manterFavorecidos.consultarListaClientePessoas)
			pesquisarClientes(getEntradaCliente());
			setListaControleCliente(new ArrayList<SelectItem>());
			for (int i = 0; i < getListaGridCliente().size(); i++) {
				getListaControleCliente().add(new SelectItem(i, " "));
			}

			// Se a pesquisa retornar mais de um resultado
			if (getListaGridCliente().size() > 1) {
				// Abrir tela de sele��o de cliente
				return TELA_CONSULTAR_CLIENTE;
			} else {
				// Se a pesquisa retornar apenas um resultado realizar consulta
				// de endere�o/email
				if (getListaGridCliente().size() == 1) {
					// Primeiro e �nico registro
					setItemSelecionadoCliente(0);
					return pesquisarEnderecoEmail();
				}
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
		return "";
	}

	/**
	 * Pesquisar clientes.
	 * 
	 * @param entrada
	 *            the entrada
	 */
	private void pesquisarClientes(
			ConsultarListaClientePessoasEntradaDTO entrada) {
		setListaGridCliente(getFiltroIdentificaoServiceImpl()
				.pesquisarClientes(entrada));
	}

	/**
	 * Consultar endereco email.
	 * 
	 * @return the string
	 */
	public String consultarEnderecoEmail() {
		limparCampos();

		return TELA_CONSULTAR_ENDERECO;
	}

	/**
	 * Pesquisar endereco email.
	 * 
	 * @return the string
	 */
	public String pesquisarEnderecoEmail() {
		if (getFlagEnderecoEmail() != null) {
			if (getFlagEnderecoEmail() == PESQUISA_POR_EMAIL) {
				return pesquisarEmail();
			} else if (getFlagEnderecoEmail() == PESQUISA_POR_ENDERECO) {
				return pesquisarEndereco();
			}
		}

		return "";
	}

	// M�todo chamado pelo bot�o "Selecionar" da tela "conEnderecoEmail" e
	// dentro do m�todo consultarClientes
	/**
	 * Pesquisar email.
	 * 
	 * @return the string
	 */
	private String pesquisarEmail() {
		try {
			limparInformacaoEnderecoEmail();

			ConsultarEmailEntradaDTO entrada = new ConsultarEmailEntradaDTO();

			ConsultarListaClientePessoasSaidaDTO registroSelecionado = getListaGridCliente()
					.get(getItemSelecionadoCliente());
			if (registroSelecionado.getCdFilialCnpj() == 0) {
				entrada.setCdTipoPessoa("F");
			} else {
				entrada.setCdTipoPessoa("J");
			}
			entrada.setCdClub(registroSelecionado.getCdClub());
			entrada.setCdEmpresaContrato(getEmpresaGestoraContrato());

			// PDC pgit.consultas.consultarEmail
			setListaGridEmail(getConsultasServiceImpl().consultarEmail(entrada));
			setListaControleEnderecoEmail(new ArrayList<SelectItem>());
			for (int i = 0; i < getListaGridEmail().size(); i++) {
				getListaControleEnderecoEmail().add(new SelectItem(i, ""));
			}

			return verificaNavegacaoEnderecoEmail();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}
	}

	/**
	 * Pesquisar endereco.
	 * 
	 * @return the string
	 */
	private String pesquisarEndereco() {
		try {
			limparInformacaoEnderecoEmail();

			ConsultarEnderecoEntradaDTO entrada = new ConsultarEnderecoEntradaDTO();

			ConsultarListaClientePessoasSaidaDTO registroSelecionado = getListaGridCliente()
					.get(getItemSelecionadoCliente());
			if (registroSelecionado.getCdFilialCnpj() == 0) {
				entrada.setCdTipoPessoa("F");
			} else {
				entrada.setCdTipoPessoa("J");
			}
			entrada.setCdClub(registroSelecionado.getCdClub());
			entrada.setCdEmpresaContrato(getEmpresaGestoraContrato());

			// PDC pgit.consultas.consultarEndereco
			setListaGridEndereco(getConsultasServiceImpl().consultarEndereco(
					entrada));
			setListaControleEnderecoEmail(new ArrayList<SelectItem>());
			for (int i = 0; i < getListaGridEndereco().size(); i++) {
				getListaControleEnderecoEmail().add(new SelectItem(i, ""));
			}

			return verificaNavegacaoEnderecoEmail();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}
	}

	/**
	 * Selecionar endereco.
	 * 
	 * @return the string
	 */
	public String selecionarEndereco() {
		setEnderecoSelecionado(getListaGridEndereco().get(
				getItemSelecionadoEnderecoEmail()));
		setEmailSelecionado(new ConsultarEmailSaidaDTO());
		return getPaginaRetorno();
	}

	/**
	 * Selecionar email.
	 * 
	 * @return the string
	 */
	public String selecionarEmail() {
		setEnderecoSelecionado(new ConsultarEnderecoSaidaDTO());
		setEmailSelecionado(getListaGridEmail().get(
				getItemSelecionadoEnderecoEmail()));
		return getPaginaRetorno();
	}

	/**
	 * Voltar cliente.
	 * 
	 * @return the string
	 */
	public String voltarCliente() {
		return verificaNavegacaoEnderecoEmail();
	}

	/**
	 * Voltar.
	 * 
	 * @return the string
	 */
	public String voltar() {
		return getPaginaRetorno();
	}

	/**
	 * Verifica navegacao endereco email.
	 * 
	 * @return the string
	 */
	private String verificaNavegacaoEnderecoEmail() {
		if (getFlagEnderecoEmail() == PESQUISA_POR_EMAIL) {
			return TELA_CONSULTAR_EMAIL;
		} else if (getFlagEnderecoEmail() == PESQUISA_POR_ENDERECO) {
			return TELA_CONSULTAR_ENDERECO;
		}
		return "";
	}

	/* GETTERS E SETTERS */

	/**
	 * Get: itemSelecionadoEnderecoEmail.
	 * 
	 * @return itemSelecionadoEnderecoEmail
	 */
	public Integer getItemSelecionadoEnderecoEmail() {
		return itemSelecionadoEnderecoEmail;
	}

	/**
	 * Set: itemSelecionadoEnderecoEmail.
	 * 
	 * @param itemSelecionadoEnderecoEmail
	 *            the item selecionado endereco email
	 */
	public void setItemSelecionadoEnderecoEmail(
			Integer itemSelecionadoEnderecoEmail) {
		this.itemSelecionadoEnderecoEmail = itemSelecionadoEnderecoEmail;
	}

	/**
	 * Get: radioFiltroConsultaEnderecoEmail.
	 * 
	 * @return radioFiltroConsultaEnderecoEmail
	 */
	public String getRadioFiltroConsultaEnderecoEmail() {
		return radioFiltroConsultaEnderecoEmail;
	}

	/**
	 * Set: radioFiltroConsultaEnderecoEmail.
	 * 
	 * @param radioFiltroConsultaEnderecoEmail
	 *            the radio filtro consulta endereco email
	 */
	public void setRadioFiltroConsultaEnderecoEmail(
			String radioFiltroConsultaEnderecoEmail) {
		this.radioFiltroConsultaEnderecoEmail = radioFiltroConsultaEnderecoEmail;
	}

	/**
	 * Get: listaGridEmail.
	 * 
	 * @return listaGridEmail
	 */
	public List<ConsultarEmailSaidaDTO> getListaGridEmail() {
		return listaGridEmail;
	}

	/**
	 * Set: listaGridEmail.
	 * 
	 * @param listaGridEmail
	 *            the lista grid email
	 */
	public void setListaGridEmail(List<ConsultarEmailSaidaDTO> listaGridEmail) {
		this.listaGridEmail = listaGridEmail;
	}

	/**
	 * Get: listaGridEndereco.
	 * 
	 * @return listaGridEndereco
	 */
	public List<ConsultarEnderecoSaidaDTO> getListaGridEndereco() {
		return listaGridEndereco;
	}

	/**
	 * Set: listaGridEndereco.
	 * 
	 * @param listaGridEndereco
	 *            the lista grid endereco
	 */
	public void setListaGridEndereco(
			List<ConsultarEnderecoSaidaDTO> listaGridEndereco) {
		this.listaGridEndereco = listaGridEndereco;
	}

	/**
	 * Get: paginaRetorno.
	 * 
	 * @return paginaRetorno
	 */
	public String getPaginaRetorno() {
		return paginaRetorno;
	}

	/**
	 * Set: paginaRetorno.
	 * 
	 * @param paginaRetorno
	 *            the pagina retorno
	 */
	public void setPaginaRetorno(String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}

	/**
	 * Get: consultasServiceImpl.
	 * 
	 * @return consultasServiceImpl
	 */
	public IConsultasService getConsultasServiceImpl() {
		return consultasServiceImpl;
	}

	/**
	 * Set: consultasServiceImpl.
	 * 
	 * @param consultasServiceImpl
	 *            the consultas service impl
	 */
	public void setConsultasServiceImpl(IConsultasService consultasServiceImpl) {
		this.consultasServiceImpl = consultasServiceImpl;
	}

	/**
	 * Get: filtroIdentificaoServiceImpl.
	 * 
	 * @return filtroIdentificaoServiceImpl
	 */
	public IFiltroIdentificaoService getFiltroIdentificaoServiceImpl() {
		return filtroIdentificaoServiceImpl;
	}

	/**
	 * Set: filtroIdentificaoServiceImpl.
	 * 
	 * @param filtroIdentificaoServiceImpl
	 *            the filtro identificao service impl
	 */
	public void setFiltroIdentificaoServiceImpl(
			IFiltroIdentificaoService filtroIdentificaoServiceImpl) {
		this.filtroIdentificaoServiceImpl = filtroIdentificaoServiceImpl;
	}

	/**
	 * Get: entradaCliente.
	 * 
	 * @return entradaCliente
	 */
	public ConsultarListaClientePessoasEntradaDTO getEntradaCliente() {
		return entradaCliente;
	}

	/**
	 * Set: entradaCliente.
	 * 
	 * @param entradaCliente
	 *            the entrada cliente
	 */
	public void setEntradaCliente(
			ConsultarListaClientePessoasEntradaDTO entradaCliente) {
		this.entradaCliente = entradaCliente;
	}

	/**
	 * Get: itemSelecionadoCliente.
	 * 
	 * @return itemSelecionadoCliente
	 */
	public Integer getItemSelecionadoCliente() {
		return itemSelecionadoCliente;
	}

	/**
	 * Set: itemSelecionadoCliente.
	 * 
	 * @param itemSelecionadoCliente
	 *            the item selecionado cliente
	 */
	public void setItemSelecionadoCliente(Integer itemSelecionadoCliente) {
		this.itemSelecionadoCliente = itemSelecionadoCliente;
	}

	/**
	 * Get: listaControleCliente.
	 * 
	 * @return listaControleCliente
	 */
	public List<SelectItem> getListaControleCliente() {
		return listaControleCliente;
	}

	/**
	 * Set: listaControleCliente.
	 * 
	 * @param listaControleCliente
	 *            the lista controle cliente
	 */
	public void setListaControleCliente(List<SelectItem> listaControleCliente) {
		this.listaControleCliente = listaControleCliente;
	}

	/**
	 * Get: listaControleEnderecoEmail.
	 * 
	 * @return listaControleEnderecoEmail
	 */
	public List<SelectItem> getListaControleEnderecoEmail() {
		return listaControleEnderecoEmail;
	}

	/**
	 * Set: listaControleEnderecoEmail.
	 * 
	 * @param listaControleEnderecoEmail
	 *            the lista controle endereco email
	 */
	public void setListaControleEnderecoEmail(
			List<SelectItem> listaControleEnderecoEmail) {
		this.listaControleEnderecoEmail = listaControleEnderecoEmail;
	}

	/**
	 * Get: listaGridCliente.
	 * 
	 * @return listaGridCliente
	 */
	public List<ConsultarListaClientePessoasSaidaDTO> getListaGridCliente() {
		return listaGridCliente;
	}

	/**
	 * Set: listaGridCliente.
	 * 
	 * @param listaGridCliente
	 *            the lista grid cliente
	 */
	public void setListaGridCliente(
			List<ConsultarListaClientePessoasSaidaDTO> listaGridCliente) {
		this.listaGridCliente = listaGridCliente;
	}

	/**
	 * Get: flagEnderecoEmail.
	 * 
	 * @return flagEnderecoEmail
	 */
	public Integer getFlagEnderecoEmail() {
		return flagEnderecoEmail;
	}

	/**
	 * Set: flagEnderecoEmail.
	 * 
	 * @param flagEnderecoEmail
	 *            the flag endereco email
	 */
	public void setFlagEnderecoEmail(String flagEnderecoEmail) {
		this.flagEnderecoEmail = 0;
		if (flagEnderecoEmail != null && !"".equals(flagEnderecoEmail)) {
			if (flagEnderecoEmail.equals("0")) {
				this.flagEnderecoEmail = PESQUISA_POR_ENDERECO;
			} else if (flagEnderecoEmail.equals("1")) {
				this.flagEnderecoEmail = PESQUISA_POR_EMAIL;
			}
		}
	}

	/**
	 * Get: empresaGestoraContrato.
	 * 
	 * @return empresaGestoraContrato
	 */
	public Long getEmpresaGestoraContrato() {
		return empresaGestoraContrato;
	}

	/**
	 * Set: empresaGestoraContrato.
	 * 
	 * @param empresaGestoraContrato
	 *            the empresa gestora contrato
	 */
	public void setEmpresaGestoraContrato(Long empresaGestoraContrato) {
		this.empresaGestoraContrato = empresaGestoraContrato;
	}

	/**
	 * Get: emailSelecionado.
	 * 
	 * @return emailSelecionado
	 */
	public ConsultarEmailSaidaDTO getEmailSelecionado() {
		return emailSelecionado;
	}

	/**
	 * Set: emailSelecionado.
	 * 
	 * @param emailSelecionado
	 *            the email selecionado
	 */
	public void setEmailSelecionado(ConsultarEmailSaidaDTO emailSelecionado) {
		this.emailSelecionado = emailSelecionado;
	}

	/**
	 * Get: enderecoSelecionado.
	 * 
	 * @return enderecoSelecionado
	 */
	public ConsultarEnderecoSaidaDTO getEnderecoSelecionado() {
		return enderecoSelecionado;
	}

	/**
	 * Set: enderecoSelecionado.
	 * 
	 * @param enderecoSelecionado
	 *            the endereco selecionado
	 */
	public void setEnderecoSelecionado(
			ConsultarEnderecoSaidaDTO enderecoSelecionado) {
		this.enderecoSelecionado = enderecoSelecionado;
	}
}