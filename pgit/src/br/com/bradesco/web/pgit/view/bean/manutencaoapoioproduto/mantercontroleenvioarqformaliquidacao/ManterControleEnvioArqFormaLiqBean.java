/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantercontroleenvioarqformaliquidacao
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantercontroleenvioarqformaliquidacao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarLiquidacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarLiquidacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.IManterConEnvioArqFormaLiquidacaoService;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ConsutarContrEnvioFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ConsutarContrEnvioFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.DetalharContrEnvioFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.DetalharContrEnvioFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ExcluirContrEnvioArqFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.ExcluirContrEnvioArqFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.IncluirContrEnvioArqFormaLiquidacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterconenvioarqformaliquidacao.bean.IncluirContrEnvioArqFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;



/**
 * Nome: ManterControleEnvioArqFormaLiqBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterControleEnvioArqFormaLiqBean {
	
	
	
	/** Atributo listaGrid. */
	private List<ConsutarContrEnvioFormaLiquidacaoSaidaDTO> listaGrid;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle;
	
	/** Atributo manterConEnvioArqFormaLiquidacaoImpl. */
	private IManterConEnvioArqFormaLiquidacaoService manterConEnvioArqFormaLiquidacaoImpl;	
	
	//Filtros
	/** Atributo cdFormaLiquidacaoFiltro. */
	private Integer cdFormaLiquidacaoFiltro;
	
	/** Atributo sequenciaEnvioFiltro. */
	private String sequenciaEnvioFiltro;
	
	/** Atributo itemSelecionado. */
	private Integer itemSelecionado;
	
	//Incluir, Exluir, Detalhar
	/** Atributo dsFormaLiquidacao. */
	private String dsFormaLiquidacao;
	
	/** Atributo sequenciaEnvio. */
	private String sequenciaEnvio;
	
	/** Atributo horario. */
	private String horario;
	
	/** Atributo cdFormaLiquidacao. */
	private Integer cdFormaLiquidacao;
	
	
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
		
	
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	//Combos
	/** Atributo listaFormaLiquidacao. */
	private List<SelectItem> listaFormaLiquidacao = new ArrayList<SelectItem>();
	
	/** Atributo listaFormaLiquidacaoHash. */
	private Map<Integer, String> listaFormaLiquidacaoHash = new HashMap<Integer, String>();
	
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {		
		limparCampos();
		listarFormaLiquidacao();
		limparVariaveis();
		setListaGrid(null);
		setItemSelecionado(null);
		setBtoAcionado(false);
	}
	
	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos(){
		setCdFormaLiquidacaoFiltro(0);
		setSequenciaEnvioFiltro("");
		setBtoAcionado(false);
		setListaGrid(null);
		setItemSelecionado(null);
		
		return "";
	}
	
	/**
	 * Limpar variaveis.
	 *
	 * @return the string
	 */
	private String limparVariaveis(){
		setCdFormaLiquidacao(0);
		setDsFormaLiquidacao("");
		setHorario("");
		setSequenciaEnvio("");
		
		return "";
	}
	
	/**
	 * Listar forma liquidacao.
	 *
	 * @return the string
	 */
	public String listarFormaLiquidacao(){
		try{
			ListarLiquidacaoPagamentoEntradaDTO entrada = new ListarLiquidacaoPagamentoEntradaDTO();
			
			listaFormaLiquidacao = new ArrayList<SelectItem>();
			listaFormaLiquidacaoHash.clear();
			
			entrada.setCdFormaLiquidacao(0);
			entrada.setDtFinalVigencia("");
			entrada.setHrInclusaoRegistroHist("");
			entrada.setDtInicioVigencia("");
			
			
			List<ListarLiquidacaoPagamentoSaidaDTO> listaLiquidacao = getComboService().listarLiquidacaoPagamento(entrada);
			
			
			
			for(ListarLiquidacaoPagamentoSaidaDTO saida : listaLiquidacao){
				getListaFormaLiquidacao().add(new SelectItem(saida.getCdFormaLiquidacao(), saida.getDsFormaLiquidacao()));
				getListaFormaLiquidacaoHash().put(saida.getCdFormaLiquidacao(), saida.getDsFormaLiquidacao());
			}
		}catch(PdcAdapterFunctionalException p){
			listaFormaLiquidacao = new ArrayList<SelectItem>();
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);			
		}
		
		return "";
	}
	
	/**
	 * Consultar.
	 */
	public void consultar(){
		try {
	    ConsutarContrEnvioFormaLiquidacaoEntradaDTO entrada = new ConsutarContrEnvioFormaLiquidacaoEntradaDTO();

	    entrada.setCdFormaLiquidacao(getCdFormaLiquidacaoFiltro() != null ? getCdFormaLiquidacaoFiltro() : 0);
	    entrada.setNrEnvioFormaLiquidacaoDoc((getSequenciaEnvioFiltro() != null && !(getSequenciaEnvioFiltro().equals(""))) ? Integer
		    .parseInt(getSequenciaEnvioFiltro()) : 0);

	    setListaGrid(getManterConEnvioArqFormaLiquidacaoImpl().consutarContrEnvioFormaLiquidacao(entrada));

	    listaControle = new ArrayList<SelectItem>();

	    for (int i = 0; i < getListaGrid().size(); i++) {
		getListaControle().add(new SelectItem(i, ""));
	    }

	    setItemSelecionado(null);
	    setBtoAcionado(true);
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
	    setListaGrid(null);
	    setItemSelecionado(null);
	    setBtoAcionado(false);
	}
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt) {
		consultar();
	}
	
	/**
	 * Voltar inicio.
	 *
	 * @return the string
	 */
	public String voltarInicio(){
		setItemSelecionado(null);
		return "VOLTAR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try{
			preencheDados();
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
		return "EXCLUIR";
	}
	
	
	
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		limparVariaveis();
		listarFormaLiquidacao();
		return "INCLUIR";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		
		setDsFormaLiquidacao((String)listaFormaLiquidacaoHash.get(getCdFormaLiquidacao()));
		
		return "AVANCAR";
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try{
			preencheDados();
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
		return "DETALHAR";
	}
	
	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		ConsutarContrEnvioFormaLiquidacaoSaidaDTO registroSelecionado = getListaGrid().get(getItemSelecionado());
		DetalharContrEnvioFormaLiquidacaoEntradaDTO entradaDTO = new DetalharContrEnvioFormaLiquidacaoEntradaDTO();
		
		entradaDTO.setCdFormaLiquidacao(registroSelecionado.getCdFormaLiquidacao());
		entradaDTO.setNrEnvioFormaLiquidacaoDoc(registroSelecionado.getNrEnvioFormaLiquidacaoDoc());
		
		DetalharContrEnvioFormaLiquidacaoSaidaDTO saidaDTO = getManterConEnvioArqFormaLiquidacaoImpl().detalharContrEnvioFormaLiquidacao(entradaDTO);
		
		setDsFormaLiquidacao(saidaDTO.getDsFormaLiquidacao());
		setHorario(saidaDTO.getHrEnvioFormaLiquidacaoDoc() != null ? saidaDTO.getHrEnvioFormaLiquidacaoDoc().replace('.',':') : "");
		setSequenciaEnvio(saidaDTO.getNrEnvioFormaLiquidacaoDoc().toString());
		setDataHoraInclusao(saidaDTO.getHrInclusaoRegistro());
		setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
		setTipoCanalInclusao(PgitUtil.concatenarCampos(saidaDTO.getCdTipoCanalInclusao(), saidaDTO.getDsTipoCanalInclusao())); 
		setComplementoInclusao(saidaDTO.getCdOperacaoCanalInclusao() == null || saidaDTO.getCdOperacaoCanalInclusao().equals("0") ? "" : saidaDTO.getCdOperacaoCanalInclusao());
		setDataHoraManutencao(saidaDTO.getHrManutencaoRegistro());
		setUsuarioManutencao(saidaDTO.getCdUsuarioManutencao());
		setTipoCanalManutencao(PgitUtil.concatenarCampos(saidaDTO.getCdTipoCanalManutencao(), saidaDTO.getDsCanalManutencao())); 
		setComplementoManutencao(saidaDTO.getCdOperacaoCanalManutencao() == null || saidaDTO.getCdOperacaoCanalManutencao().equals("0") ? "" : saidaDTO.getCdOperacaoCanalManutencao());
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try {
			ConsutarContrEnvioFormaLiquidacaoSaidaDTO registroSelecionado = getListaGrid().get(getItemSelecionado());
			
			ExcluirContrEnvioArqFormaLiquidacaoEntradaDTO entradaDTO = new ExcluirContrEnvioArqFormaLiquidacaoEntradaDTO();
			
			entradaDTO.setCdFormaLiquidacao(registroSelecionado.getCdFormaLiquidacao());
			entradaDTO.setNrEnvioFormaLiquidacaoDoc(registroSelecionado.getNrEnvioFormaLiquidacaoDoc());
			
			ExcluirContrEnvioArqFormaLiquidacaoSaidaDTO saidaDTO = getManterConEnvioArqFormaLiquidacaoImpl().excluirContrEnvioArqFormaLiquidacao(entradaDTO);
	
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conManterConEnvioArqFormaLiq", BradescoViewExceptionActionType.ACTION, false);
			
			consultar();
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try {
			IncluirContrEnvioArqFormaLiquidacaoEntradaDTO entradaDTO = new IncluirContrEnvioArqFormaLiquidacaoEntradaDTO();
			
			entradaDTO.setCdFormaLiquidacao(getCdFormaLiquidacao());
			entradaDTO.setHrEnvioFormaLiquidacaoDoc(getHorario());
			entradaDTO.setNrEnvioFormaLiquidacaoDoc((getSequenciaEnvio() != null && !(getSequenciaEnvio().equals("")))  ? Integer.parseInt(getSequenciaEnvio()) : 0);
			
			IncluirContrEnvioArqFormaLiquidacaoSaidaDTO saidaDTO = getManterConEnvioArqFormaLiquidacaoImpl().incluirContrEnvioArqFormaLiquidacao(entradaDTO);
	
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conManterConEnvioArqFormaLiq", BradescoViewExceptionActionType.ACTION, false);
			
			if(getListaGrid() != null){
				consultar();
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}


	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}


	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}


	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}


	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}


	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}


	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}


	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}


	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}


	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}


	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}


	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}


	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}


	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ConsutarContrEnvioFormaLiquidacaoSaidaDTO> getListaGrid() {
		return listaGrid;
	}


	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(
			List<ConsutarContrEnvioFormaLiquidacaoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}


	/**
	 * Get: manterConEnvioArqFormaLiquidacaoImpl.
	 *
	 * @return manterConEnvioArqFormaLiquidacaoImpl
	 */
	public IManterConEnvioArqFormaLiquidacaoService getManterConEnvioArqFormaLiquidacaoImpl() {
		return manterConEnvioArqFormaLiquidacaoImpl;
	}


	/**
	 * Set: manterConEnvioArqFormaLiquidacaoImpl.
	 *
	 * @param manterConEnvioArqFormaLiquidacaoImpl the manter con envio arq forma liquidacao impl
	 */
	public void setManterConEnvioArqFormaLiquidacaoImpl(
			IManterConEnvioArqFormaLiquidacaoService manterConEnvioArqFormaLiquidacaoImpl) {
		this.manterConEnvioArqFormaLiquidacaoImpl = manterConEnvioArqFormaLiquidacaoImpl;
	}


	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}


	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}


	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}


	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}


	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}


	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}


	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}


	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}


	


	/**
	 * Get: cdFormaLiquidacaoFiltro.
	 *
	 * @return cdFormaLiquidacaoFiltro
	 */
	public Integer getCdFormaLiquidacaoFiltro() {
		return cdFormaLiquidacaoFiltro;
	}

	/**
	 * Set: cdFormaLiquidacaoFiltro.
	 *
	 * @param cdFormaLiquidacaoFiltro the cd forma liquidacao filtro
	 */
	public void setCdFormaLiquidacaoFiltro(Integer cdFormaLiquidacaoFiltro) {
		this.cdFormaLiquidacaoFiltro = cdFormaLiquidacaoFiltro;
	}

	/**
	 * Get: sequenciaEnvioFiltro.
	 *
	 * @return sequenciaEnvioFiltro
	 */
	public String getSequenciaEnvioFiltro() {
		return sequenciaEnvioFiltro;
	}

	/**
	 * Set: sequenciaEnvioFiltro.
	 *
	 * @param sequenciaEnvioFiltro the sequencia envio filtro
	 */
	public void setSequenciaEnvioFiltro(String sequenciaEnvioFiltro) {
		this.sequenciaEnvioFiltro = sequenciaEnvioFiltro;
	}

	/**
	 * Get: listaFormaLiquidacao.
	 *
	 * @return listaFormaLiquidacao
	 */
	public List<SelectItem> getListaFormaLiquidacao() {
		return listaFormaLiquidacao;
	}


	/**
	 * Set: listaFormaLiquidacao.
	 *
	 * @param listaFormaLiquidacao the lista forma liquidacao
	 */
	public void setListaFormaLiquidacao(List<SelectItem> listaFormaLiquidacao) {
		this.listaFormaLiquidacao = listaFormaLiquidacao;
	}


	/**
	 * Get: listaFormaLiquidacaoHash.
	 *
	 * @return listaFormaLiquidacaoHash
	 */
	public Map<Integer, String> getListaFormaLiquidacaoHash() {
		return listaFormaLiquidacaoHash;
	}


	/**
	 * Set lista forma liquidacao hash.
	 *
	 * @param listaFormaLiquidacaoHash the lista forma liquidacao hash
	 */
	public void setListaFormaLiquidacaoHash(
			Map<Integer, String> listaFormaLiquidacaoHash) {
		this.listaFormaLiquidacaoHash = listaFormaLiquidacaoHash;
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public Integer getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(Integer itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
		return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}


	/**
	 * Get: cdFormaLiquidacao.
	 *
	 * @return cdFormaLiquidacao
	 */
	public Integer getCdFormaLiquidacao() {
		return cdFormaLiquidacao;
	}

	/**
	 * Set: cdFormaLiquidacao.
	 *
	 * @param cdFormaLiquidacao the cd forma liquidacao
	 */
	public void setCdFormaLiquidacao(Integer cdFormaLiquidacao) {
		this.cdFormaLiquidacao = cdFormaLiquidacao;
	}

	/**
	 * Get: sequenciaEnvio.
	 *
	 * @return sequenciaEnvio
	 */
	public String getSequenciaEnvio() {
		return sequenciaEnvio;
	}

	/**
	 * Set: sequenciaEnvio.
	 *
	 * @param sequenciaEnvio the sequencia envio
	 */
	public void setSequenciaEnvio(String sequenciaEnvio) {
		this.sequenciaEnvio = sequenciaEnvio;
	}

	/**
	 * Get: horario.
	 *
	 * @return horario
	 */
	public String getHorario() {
		return horario;
	}

	/**
	 * Set: horario.
	 *
	 * @param horario the horario
	 */
	public void setHorario(String horario) {
		this.horario = horario;
	}

	

	
	
	
	
	
	
	
	
}
