/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarTipoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarTipoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeListaDebitoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOrgaoPagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOrgaoPagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesListaDebitoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.IConsultarPagamentosIndividualService;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaoexclusaolotepagamentosservice.bean.OcorrenciasSolicLoteDTO;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.exclusaolotepagamentos.ConfimarInclusaoLotePagamentosBean;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: PagamentosBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class PagamentosBean {

	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean = new FiltroAgendamentoEfetivacaoEstornoBean();

	/** Atributo consultarPagamentosIndividualServiceImpl. */
	private IConsultarPagamentosIndividualService consultarPagamentosIndividualServiceImpl;

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo agendadosPagosNaoPagos. */
	private String agendadosPagosNaoPagos;

	/** Atributo dataInicialPagamentoFiltro. */
	private Date dataInicialPagamentoFiltro;

	/** Atributo dataFinalPagamentoFiltro. */
	private Date dataFinalPagamentoFiltro;

	/** Atributo chkParticipanteContrato. */
	private boolean chkParticipanteContrato;

	/** Atributo chkContaDebito. */
	private boolean chkContaDebito;

	/** Atributo cdBancoContaDebito. */
	private Integer cdBancoContaDebito;

	/** Atributo cdAgenciaBancariaContaDebito. */
	private Integer cdAgenciaBancariaContaDebito;

	/** Atributo cdContaBancariaContaDebito. */
	private Long cdContaBancariaContaDebito;

	/** Atributo cdDigitoContaContaDebito. */
	private String cdDigitoContaContaDebito;

	/** Atributo cdGrupoContabil. */
	private Integer cdGrupoContabil;

	/** Atributo cdSubGrupoContabil. */
	private Integer cdSubGrupoContabil;

	/** Atributo chkServicoModalidade. */
	private boolean chkServicoModalidade;

	/** Atributo chkRastreamentoTitulo. */
	private boolean chkRastreamentoTitulo;

	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;

	/** Atributo modalidadeFiltro. */
	private Integer modalidadeFiltro;

	/** Atributo chkSituacaoMotivo. */
	private boolean chkSituacaoMotivo;

	/** Atributo cboSituacaoPagamentoFiltro. */
	private Integer cboSituacaoPagamentoFiltro;

	/** Atributo cboMotivoSituacaoFiltro. */
	private Integer cboMotivoSituacaoFiltro;

	/** Atributo numeroPagamentoDeFiltro. */
	private String numeroPagamentoDeFiltro;

	/** Atributo numeroPagamentoAteFiltro. */
	private String numeroPagamentoAteFiltro;

	/** Atributo valorPagamentoDeFiltro. */
	private BigDecimal valorPagamentoDeFiltro;

	/** Atributo valorPagamentoAteFiltro. */
	private BigDecimal valorPagamentoAteFiltro;

	/** Atributo chkRemessa. */
	private boolean chkRemessa;

	/** Atributo numeroRemessaFiltro. */
	private String numeroRemessaFiltro;

	/** Atributo numeroListaDebitoFiltro. */
	private String numeroListaDebitoFiltro;

	/** Atributo linhaDigitavel1. */
	private String linhaDigitavel1;

	/** Atributo linhaDigitavel2. */
	private String linhaDigitavel2;

	/** Atributo linhaDigitavel3. */
	private String linhaDigitavel3;

	/** Atributo linhaDigitavel4. */
	private String linhaDigitavel4;

	/** Atributo linhaDigitavel5. */
	private String linhaDigitavel5;

	/** Atributo linhaDigitavel6. */
	private String linhaDigitavel6;

	/** Atributo linhaDigitavel7. */
	private String linhaDigitavel7;

	/** Atributo linhaDigitavel8. */
	private String linhaDigitavel8;

	/** Atributo radioPesquisaFiltroPrincipal. */
	private String radioPesquisaFiltroPrincipal;

	/** Atributo radioFavorecidoFiltro. */
	private String radioFavorecidoFiltro;

	/** Atributo radioRastreados. */
	private String radioRastreados;

	/** Atributo codigoFavorecidoFiltro. */
	private String codigoFavorecidoFiltro;

	/** Atributo inscricaoFiltro. */
	private String inscricaoFiltro;

	/** Atributo inscricaoFiltroBeneficiario. */
	private String inscricaoFiltroBeneficiario;

	/** Atributo tipoInscricaoFiltro. */
	private Integer tipoInscricaoFiltro;

	/** Atributo cdBancoContaCredito. */
	private Integer cdBancoContaCredito;
	
	/** Atributo cdBancoContaSalario. */
	private Integer cdBancoContaSalario;
	
	private String cdBancoPagamento;

	/** Atributo cdAgenciaBancariaContaCredito. */
	private Integer cdAgenciaBancariaContaCredito;
	
	/** Atributo cdAgenciaBancariaContaSalario. */
	private Integer cdAgenciaBancariaContaSalario;

	/** Atributo cdContaBancariaContaCredito. */
	private Long cdContaBancariaContaCredito;
	
	/** Atributo cdContaBancariaContaSalario. */
	private Long cdContaBancariaContaSalario;
	
	private String cdContaPagamento;

	/** Atributo cdDigitoContaCredito. */
	private String cdDigitoContaCredito;

	/** Atributo radioBeneficiarioFiltro. */
	private String radioBeneficiarioFiltro;

	/** Atributo codigoBeneficiarioFiltro. */
	private String codigoBeneficiarioFiltro;

	/** Atributo tipoBeneficiarioFiltro. */
	private Integer tipoBeneficiarioFiltro;

	/** Atributo orgaoPagadorFiltro. */
	private Integer orgaoPagadorFiltro;

	/** Atributo classificacaoFiltro. */
	private Integer classificacaoFiltro;

	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	private String ispb;

	/** Atributo listaTipoServicoFiltro. */
	private List<SelectItem> listaTipoServicoFiltro = new ArrayList<SelectItem>();

	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();

	/** Atributo listaModalidadeFiltro. */
	private List<SelectItem> listaModalidadeFiltro = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeHash. */
	private Map<Integer, String> listaModalidadeHash = new HashMap<Integer, String>();

	/** Atributo listaModalidadeListaDebitoFiltro. */
	private List<SelectItem> listaModalidadeListaDebitoFiltro = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeListaDebitoHash. */
	private Map<Integer, String> listaModalidadeListaDebitoHash = new HashMap<Integer, String>();

	/** Atributo listaInscricaoFavorecidoFiltro. */
	private List<SelectItem> listaInscricaoFavorecidoFiltro = new ArrayList<SelectItem>();

	/** Atributo listaInscricaoFavorecidoHash. */
	private Map<Integer, String> listaInscricaoFavorecidoHash = new HashMap<Integer, String>();

	/** Atributo listaConsultarSituacaoPagamentoFiltro. */
	private List<SelectItem> listaConsultarSituacaoPagamentoFiltro = new ArrayList<SelectItem>();

	/** Atributo listaConsultarSituacaoPagamentoHash. */
	private Map<Integer, String> listaConsultarSituacaoPagamentoHash = new HashMap<Integer, String>();

	/** Atributo listaConsultarMotivoSituacaoPagamentoFiltro. */
	private List<SelectItem> listaConsultarMotivoSituacaoPagamentoFiltro = new ArrayList<SelectItem>();

	/** Atributo listaConsultarMotivoSituacaoPagamentoHash. */
	private Map<Integer, String> listaConsultarMotivoSituacaoPagamentoHash = new HashMap<Integer, String>();

	/** Atributo listaOrgaoPagadorFiltro. */
	private List<SelectItem> listaOrgaoPagadorFiltro = new ArrayList<SelectItem>();

	/** Atributo chkNumeroListaDebito. */
	private boolean chkNumeroListaDebito;
	
	/** Atributo desabilitaBtnLimparCampos. */
	private Boolean desabilitaBtnLimparCampos = false;

	/** Atributo numListaDebitoDeFiltro. */
	private String numListaDebitoDeFiltro;

	/** Atributo numListaDebitoAteFiltro. */
	private String numListaDebitoAteFiltro;

	/** Atributo chkSituacaoListaDebito. */
	private boolean chkSituacaoListaDebito;

	/** Atributo cboSituacaoListaDebito. */
	private Integer cboSituacaoListaDebito;

	/** Atributo listaSituacaoListaDebito. */
	private List<SelectItem> listaSituacaoListaDebito = new ArrayList<SelectItem>();

	/** Atributo listaCmbTipoConta. */
	private List<SelectItem> listaCmbTipoConta = new ArrayList<SelectItem>();

	/** Atributo listaCmbTipoContaDebito. */
	private List<SelectItem> listaCmbTipoContaDebito = new ArrayList<SelectItem>();

	/** Atributo listaSituacaoListaDebitoHash. */
	private Map<Integer, ConsultarTipoContaSaidaDTO> listaSituacaoListaDebitoHash = new HashMap<Integer, ConsultarTipoContaSaidaDTO>();

	/** Atributo listaCmbTipoContaDebitoHash. */
	private Map<Integer, ConsultarTipoContaSaidaDTO> listaCmbTipoContaDebitoHash = new HashMap<Integer, ConsultarTipoContaSaidaDTO>();

	/** Atributo cmbTipoConta. */
	private Integer cmbTipoConta;

	/** Atributo cmbTipoContaDebito. */
	private Integer cmbTipoContaDebito;

	/** Atributo foco. */
	private String foco;

	/** Atributo listaModalidadeListaDebito. */
	private List<ListarModalidadeListaDebitoSaidaDTO> listaModalidadeListaDebito = new ArrayList<ListarModalidadeListaDebitoSaidaDTO>();
	
	/** Atributo saidaTipoLayoutArquivoSaidaDTO. */
	private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;

	/**
	 * Atributos referente a Lote de Pagamentos
	 */
	private boolean disableArgumentosConsulta;
	private boolean panelBotoes;
	private boolean chkLoteInterno;
	private boolean chkSituacaoSolicitacao;
	private Long loteInterno;
	private Integer filtroTipoLayoutArquivo;
	private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();
	private Map<Integer, String> listaTipoLayoutArquivoHash = new HashMap<Integer, String>();
	private Integer cboSituacaoSolicitacao;
	private Integer cboMotivoSituacao;
	private List<SelectItem> listaSituacaoSolicitacaoFiltro = new ArrayList<SelectItem>();
	private List<SelectItem> listaMotivoSituacaoFiltro = new ArrayList<SelectItem>();
	private boolean chkDataPagamento;
	private boolean chkTipoServico;

	private ConfimarInclusaoLotePagamentosBean confirmaInclusaoBean;

	private List<OcorrenciasSolicLoteDTO> listaOcorrenciasDetalhe;

	private boolean chkIndicadorAutorizacao;
	private String radioIndicadorAutorizacao;

	private boolean chkHabilitaCredito = false;
	private boolean chkHabilitaSalario = false;
	private boolean chkHabilitaPagamento = false;
	
	public void limparSituacaoListaDebito() {
		setCboSituacaoListaDebito(null);
	}

	/**
	 * Limpar numero lista debito.
	 */
	public void limparNumeroListaDebito() {
		setNumListaDebitoDeFiltro("");
		setNumListaDebitoAteFiltro("");
	}

	/**
	 * Controlar argumentos pesquisa.
	 */
	public void controlarArgumentosPesquisa() {
		FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean = (FiltroAgendamentoEfetivacaoEstornoBean) FacesUtils
				.getManagedBean("filtroAgendamentoEfetivacaoEstornoBean");

		if (filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro() == null
				|| filtroAgendamentoEfetivacaoEstornoBean
						.getAgenciaContaDebitoBancariaFiltro() == null
				|| filtroAgendamentoEfetivacaoEstornoBean
						.getContaBancariaContaDebitoFiltro() == null) {
			setHabilitaArgumentosPesquisa(false);
			limparCampos();
		} else {
			setHabilitaArgumentosPesquisa(true);
		}
	}

	/**
	 * Listar orgao pagador.
	 */
	public void listarOrgaoPagador() {
		try {
			listaOrgaoPagadorFiltro = new ArrayList<SelectItem>();
			ListarOrgaoPagadorEntradaDTO entrada = new ListarOrgaoPagadorEntradaDTO();

			entrada.setMaximoOcorrencias(50);
			entrada.setCdOrgaoPagadorOrganizacao(0);
			entrada.setCdPessoaJuridica(0L);
			entrada.setCdSituacaoOrgaoPagador(0);
			entrada.setCdTarifaOrgaoPagador(0);
			entrada.setCdTipoUnidadeOrganizacao(0);
			entrada.setNrSequenciaUnidadeOrganizacao(0);

			List<ListarOrgaoPagadorSaidaDTO> list = comboService
					.listarOrgaoPagadorDescOrgao(entrada);

			for (ListarOrgaoPagadorSaidaDTO saida : list) {
				listaOrgaoPagadorFiltro.add(new SelectItem(saida
						.getCdOrgaoPagadorOrganizacao(), Integer.toString(saida
						.getCdOrgaoPagadorOrganizacao())));
			}
		} catch (PdcAdapterFunctionalException p) {
			listaOrgaoPagadorFiltro = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar consultar motivo situacao pagamento solicitacao.
	 */
	public void listarConsultarMotivoSituacaoPagamentoSolicitacao() {
		try {
			listaConsultarMotivoSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
			listaConsultarMotivoSituacaoPagamentoHash.clear();
			setCboMotivoSituacaoFiltro(null);

			ConsultarMotivoSituacaoPagamentoEntradaDTO entrada = new ConsultarMotivoSituacaoPagamentoEntradaDTO();
			entrada.setCdSituacao(7);

			List<ConsultarMotivoSituacaoPagamentoSaidaDTO> list = comboService
					.consultarMotivoSituacaoPagamento(entrada);

			for (ConsultarMotivoSituacaoPagamentoSaidaDTO saida : list) {
				listaConsultarMotivoSituacaoPagamentoFiltro.add(new SelectItem(
						saida.getCodigo(), saida.getDescricao()));
				listaConsultarMotivoSituacaoPagamentoHash.put(
						saida.getCodigo(), saida.getDescricao());
			}
		} catch (PdcAdapterFunctionalException p) {
			listaConsultarMotivoSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
			setCboMotivoSituacaoFiltro(null);
		}
	}

	/**
	 * Listar situacao lista debito.
	 */
	public void listarSituacaoListaDebito() {
		listaSituacaoListaDebito = new ArrayList<SelectItem>();
		listaSituacaoListaDebito.add(new SelectItem("1", MessageHelperUtils
				.getI18nMessage("conConsultarListasDebitos_combo_autorizada")));
		listaSituacaoListaDebito
				.add(new SelectItem(
						"2",
						MessageHelperUtils
								.getI18nMessage("conConsultarListasDebitos_combo_nao_autorizada")));
		listaSituacaoListaDebito
				.add(new SelectItem(
						"3",
						MessageHelperUtils
								.getI18nMessage("conConsultarListasDebitos_combo_autorizada_parcialmente")));
		listaSituacaoListaDebito.add(new SelectItem("4", MessageHelperUtils
				.getI18nMessage("conConsultarListasDebitos_combo_processada")));
	}

	/**
	 * Listar consultar motivo situacao pagamento.
	 */
	public void listarConsultarMotivoSituacaoPagamento() {
		try {
			listaConsultarMotivoSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
			listaConsultarMotivoSituacaoPagamentoHash.clear();
			setCboMotivoSituacaoFiltro(null);

			if (getCboSituacaoPagamentoFiltro() == null
					|| getCboSituacaoPagamentoFiltro() == 0) {
				return;
			}

			ConsultarMotivoSituacaoPagamentoEntradaDTO entrada = new ConsultarMotivoSituacaoPagamentoEntradaDTO();
			entrada.setCdSituacao(getCboSituacaoPagamentoFiltro());

			List<ConsultarMotivoSituacaoPagamentoSaidaDTO> list = comboService
					.consultarMotivoSituacaoPagamento(entrada);

			for (ConsultarMotivoSituacaoPagamentoSaidaDTO saida : list) {
				listaConsultarMotivoSituacaoPagamentoFiltro.add(new SelectItem(
						saida.getCodigo(), saida.getDescricao()));
				listaConsultarMotivoSituacaoPagamentoHash.put(
						saida.getCodigo(), saida.getDescricao());
			}
		} catch (PdcAdapterFunctionalException p) {
			listaConsultarMotivoSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
			setCboMotivoSituacaoFiltro(null);
		}
	}

	/**
	 * Listar consultar situacao pagamento.
	 */
	public void listarConsultarSituacaoPagamento() {
		try {
			listaConsultarSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
			listaConsultarSituacaoPagamentoHash.clear();

			ConsultarSituacaoPagamentoEntradaDTO entrada = new ConsultarSituacaoPagamentoEntradaDTO();

			entrada.setCdSituacao(0);

			List<ConsultarSituacaoPagamentoSaidaDTO> list = comboService
					.consultarSituacaoPagamento(entrada);

			for (ConsultarSituacaoPagamentoSaidaDTO saida : list) {
				listaConsultarSituacaoPagamentoFiltro.add(new SelectItem(saida
						.getCodigo(), saida.getDescricao()));
				listaConsultarSituacaoPagamentoHash.put(saida.getCodigo(),
						saida.getDescricao());

			}
		} catch (PdcAdapterFunctionalException p) {
			listaConsultarSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar inscricao favorecido.
	 */
	public void listarInscricaoFavorecido() {
		try {
			listaInscricaoFavorecidoFiltro = new ArrayList<SelectItem>();
			listaInscricaoFavorecidoHash.clear();

			/*
			 * InscricaoFavorecidosEntradaDTO entrada = new
			 * InscricaoFavorecidosEntradaDTO();
			 * entrada.setCdSituacaoVinculacaoConta(0);
			 * entrada.setNumeroOcorrencias(100);
			 * 
			 * List<InscricaoFavorecidosSaidaDTO> list =
			 * comboService.listarInscricaoFavorecido(entrada);
			 * 
			 * for (InscricaoFavorecidosSaidaDTO saida : list){
			 * listaInscricaoFavorecidoFiltro.add(new
			 * SelectItem(saida.getCdTipoInscricaoFavorecidos(),
			 * saida.getDsTipoInscricaoFavorecidos()));
			 * listaInscricaoFavorecidoHash
			 * .put(saida.getCdTipoInscricaoFavorecidos(),
			 * saida.getDsTipoInscricaoFavorecidos()); }
			 */

			listaInscricaoFavorecidoFiltro.add(new SelectItem(1,
					MessageHelperUtils.getI18nMessage("label_cpf")));
			listaInscricaoFavorecidoFiltro.add(new SelectItem(2,
					MessageHelperUtils.getI18nMessage("label_cnpj")));
			listaInscricaoFavorecidoFiltro.add(new SelectItem(9,
					MessageHelperUtils.getI18nMessage("label_outros")));

			listaInscricaoFavorecidoHash.put(1, MessageHelperUtils
					.getI18nMessage("label_cpf"));
			listaInscricaoFavorecidoHash.put(2, MessageHelperUtils
					.getI18nMessage("label_cnpj"));
			listaInscricaoFavorecidoHash.put(9, MessageHelperUtils
					.getI18nMessage("label_outros"));

		} catch (PdcAdapterFunctionalException p) {
			listaInscricaoFavorecidoFiltro = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar tipo servico.
	 */
	public void listarTipoServico() {
		try {
			listaTipoServicoHash.clear();
			listaTipoServicoFiltro = new ArrayList<SelectItem>();

			ListarServicosEntradaDTO entrada = new ListarServicosEntradaDTO();
			entrada.setCdNaturezaServico(0);
			entrada.setNumeroOcorrencias(30);

			List<ListarServicosSaidaDTO> list = comboService
					.listarTipoServico(entrada);

			for (ListarServicosSaidaDTO saida : list) {
				listaTipoServicoFiltro.add(new SelectItem(saida.getCdServico(),
						saida.getDsServico()));
				listaTipoServicoHash.put(saida.getCdServico(), saida
						.getDsServico());
			}

		} catch (PdcAdapterFunctionalException p) {
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar modalidades.
	 */
	public void listarModalidades() {
		try {
			setModalidadeFiltro(0);
			listaModalidadeHash.clear();
			if (getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0) {
				listaModalidadeFiltro = new ArrayList<SelectItem>();

				ListarModalidadesEntradaDTO entrada = new ListarModalidadesEntradaDTO();
				entrada.setCdServico(getTipoServicoFiltro());
				entrada.setNumeroOcorrencias(100);

				List<ListarModalidadeSaidaDTO> list = comboService
						.listarModalidadesEntrada(entrada);

				for (ListarModalidadeSaidaDTO saida : list) {
					listaModalidadeFiltro.add(new SelectItem(saida
							.getCdModalidade(), saida.getDsModalidade()));
					listaModalidadeHash.put(saida.getCdModalidade(), saida
							.getDsModalidade());

				}
			} else {
				listaModalidadeFiltro = new ArrayList<SelectItem>();
			}
		} catch (PdcAdapterFunctionalException p) {
			listaModalidadeFiltro = new ArrayList<SelectItem>();
			setModalidadeFiltro(0);
		}
	}

	/**
	 * Listar modalidades lista debito.
	 */
	public void listarModalidadesListaDebito() {
		try {
			setModalidadeFiltro(0);
			listaModalidadeListaDebitoHash.clear();
			listaModalidadeListaDebito.clear();
			if (getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0) {
				listaModalidadeListaDebitoFiltro = new ArrayList<SelectItem>();

				ListarModalidadesListaDebitoEntradaDTO entrada = new ListarModalidadesListaDebitoEntradaDTO();
				entrada.setCdProdutoServicoOperacao(getTipoServicoFiltro());
				entrada.setNrOcorrencias(100);

				List<ListarModalidadeListaDebitoSaidaDTO> list = comboService
						.listarModalidadesListaDebitoEntrada(entrada);

				listaModalidadeListaDebito.addAll(list);

				for (ListarModalidadeListaDebitoSaidaDTO saida : list) {
					listaModalidadeListaDebitoFiltro.add(new SelectItem(saida
							.getCdProdutoOperacaoRelacionado(), saida
							.getDsProdutoOperacaoRelacionada()));

				}
			} else {
				listaModalidadeListaDebitoFiltro = new ArrayList<SelectItem>();
			}
		} catch (PdcAdapterFunctionalException p) {
			listaModalidadeListaDebitoFiltro = new ArrayList<SelectItem>();
			setModalidadeFiltro(0);
		}
	}

	// Tipo de Servi�o utilizado nas telas do Menu de Agendamento Efetiva��o
	/**
	 * Listar tipo servico pagto.
	 */
	public void listarTipoServicoPagto() {
		try {
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
			listaTipoServicoHash.clear();
			List<ConsultarServicoPagtoSaidaDTO> list = comboService
					.consultarServicoPagto();

			for (ConsultarServicoPagtoSaidaDTO saida : list) {
				listaTipoServicoFiltro.add(new SelectItem(saida.getCdServico(),
						saida.getDsServico()));
				listaTipoServicoHash.put(saida.getCdServico(), saida
						.getDsServico());
			}
		} catch (PdcAdapterFunctionalException p) {
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
		}
	}

	// Modalidade utilizada nas telas do Menu de Agendamento Efetiva��o
	/**
	 * Listar modalidades pagto.
	 */
	public void listarModalidadesPagto() {
		try {
			setModalidadeFiltro(0);
			if (getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0) {
				listaModalidadeFiltro = new ArrayList<SelectItem>();

				List<ConsultarModalidadePagtoSaidaDTO> list = comboService
						.consultarModalidadePagto(getTipoServicoFiltro());

				for (ConsultarModalidadePagtoSaidaDTO saida : list) {
					listaModalidadeFiltro.add(new SelectItem(saida
							.getCdProdutoOperacaoRelacionado(), saida
							.getDsProdutoOperacaoRelacionado()));
					listaModalidadeHash.put(saida
							.getCdProdutoOperacaoRelacionado(), saida
							.getDsProdutoOperacaoRelacionado());
				}
			} else {
				listaModalidadeFiltro = new ArrayList<SelectItem>();
			}
		} catch (PdcAdapterFunctionalException p) {
			listaModalidadeFiltro = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Controle servico modalidade.
	 */
	public void controleServicoModalidade() {
		setModalidadeFiltro(null);
		setTipoServicoFiltro(null);
		listaModalidadeFiltro = new ArrayList<SelectItem>();
	}

	/**
	 * Controle rastreados nao rastreados.
	 */
	public void controleRastreadosNaoRastreados() {
		setRadioRastreados(null);
	}

	public void controleIndicadorAutorizacao() {
		setRadioIndicadorAutorizacao(null);
	}

	/**
	 * Limpar filtro principal.
	 */
	public void limparFiltroPrincipal() {
		FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean = (FiltroAgendamentoEfetivacaoEstornoBean) FacesUtils
				.getManagedBean("filtroAgendamentoEfetivacaoEstornoBean");

		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();

		if (filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado() != null
				&& filtroAgendamentoEfetivacaoEstornoBean
						.getTipoFiltroSelecionado().equals("2")) {
			filtroAgendamentoEfetivacaoEstornoBean.limparParticipanteContrato();
			setChkContaDebito(false);
			setChkHabilitaCredito(false);
			setChkHabilitaSalario(false);
			setChkHabilitaPagamento(false);
			limparContaDebito();
			setChkParticipanteContrato(false);
			filtroAgendamentoEfetivacaoEstornoBean
					.setBancoContaDebitoFiltro(237);
		}

		setChkRastreamentoTitulo(false);
		setRadioRastreados(null);

		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean
				.setClienteContratoSelecionado(false);

		limparCampos();
	}

	public void limpaTelaPrincipal() {
		limparFiltroPrincipal();
		setDisableArgumentosConsulta(false);
		setChkLoteInterno(false);
		setChkSituacaoSolicitacao(false);
		limparCamposLoteInterno();
		limparSituacaoSolicitacao();
		setDesabilitaBtnLimparCampos(true);
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);
	}
	
	public void limpaTelaPrincipalLote() {
		limparFiltroPrincipal();
		setDisableArgumentosConsulta(false);
		setChkLoteInterno(false);
		setChkSituacaoSolicitacao(false);
		limparCamposLoteInterno();
		limparSituacaoSolicitacao();
	}
	
	public void	 chamarAposDesabilitar(ActionEvent evt) { 
		setDesabilitaBtnLimparCampos(false);
	}	

	public void limparSituacaoSolicitacao() {
		setCboMotivoSituacao(null);
		setCboSituacaoSolicitacao(null);
	}

	@SuppressWarnings("deprecation")
	public void limparArgumentossListaAposPesquisaClienteContrato(
			final ActionEvent evt) {
		limparCampos();
		setPanelBotoes(false);

		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());
	}
	
	public void limpaTelaInicialLote() {
		FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean = (FiltroAgendamentoEfetivacaoEstornoBean) FacesUtils
		.getManagedBean("filtroAgendamentoEfetivacaoEstornoBean");

        filtroAgendamentoEfetivacaoEstornoBean.limpar();

		limparFiltroPrincipal();
		setDisableArgumentosConsulta(false);
		setChkLoteInterno(false);
		setChkSituacaoSolicitacao(false);
		limparCamposLoteInterno();
		limparSituacaoSolicitacao();
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);
}


	/**
	 * Limpar conta debito.
	 */
	public void limparContaDebito() {
		setCdBancoContaDebito(null);
		setCdAgenciaBancariaContaDebito(null);
		setCdGrupoContabil(null);
		setCdSubGrupoContabil(null);
		setCdContaBancariaContaDebito(null);
		setCdDigitoContaContaDebito("");

		if (isChkContaDebito()) {
			setCdBancoContaDebito(237);
		}

		limparTipoContaDebito();
	}

	/**
	 * Limpar tipo servico modalidade.
	 */
	public void limparTipoServicoModalidade() {
		setTipoServicoFiltro(0);
		listaModalidadeFiltro = new ArrayList<SelectItem>();
	}

	/**
	 * Limpar situacao motivo.
	 */
	public void limparSituacaoMotivo() {
		setCboSituacaoPagamentoFiltro(null);
		setCboMotivoSituacaoFiltro(null);
		listaConsultarMotivoSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
	}

	/**
	 * Limpar motivo.
	 */
	public void limparMotivo() {
		setCboMotivoSituacaoFiltro(null);
	}

	/**
	 * Limpar remessa favorecido beneficiario linha.
	 */
	public void limparRemessaFavorecidoBeneficiarioLinha() {
		setNumeroPagamentoDeFiltro("");
		setNumeroPagamentoAteFiltro("");
		setValorPagamentoDeFiltro(null);
		setValorPagamentoAteFiltro(null);
		setLinhaDigitavel1("");
		setLinhaDigitavel2("");
		setLinhaDigitavel3("");
		setLinhaDigitavel4("");
		setLinhaDigitavel5("");
		setLinhaDigitavel6("");
		setLinhaDigitavel7("");
		setLinhaDigitavel8("");
		limparRemessa();
		limparListaDebito();
		limparFavorecido();
		limparBeneficiario();
		setRadioFavorecidoFiltro("");
		setRadioBeneficiarioFiltro("");
		limparTipoConta();
	}

	/**
	 * Limpar tipo conta.
	 */
	public void limparTipoConta() {
		setCmbTipoConta(null);
		this.listaCmbTipoConta = new ArrayList<SelectItem>();
	}

	/**
	 * Limpar tipo conta debito.
	 */
	public void limparTipoContaDebito() {
		setCmbTipoContaDebito(null);
		this.listaCmbTipoContaDebito = new ArrayList<SelectItem>();
	}

	/**
	 * Limpar remessa / lote interno
	 */
	public void limparRemessa() {
		setNumeroRemessaFiltro("");
		setLoteInterno(null);
	}

	/**
	 * Limpar lista debito.
	 */
	public void limparListaDebito() {
		setNumeroListaDebitoFiltro("");
	}

	/**
	 * Limpar favorecido.
	 */
	public void limparFavorecido() {
		setCodigoFavorecidoFiltro("");
		setInscricaoFiltro("");
		setTipoInscricaoFiltro(null);
		setCdBancoContaCredito(null);
		setCdAgenciaBancariaContaCredito(null);
		setCdContaBancariaContaCredito(null);
		setCdDigitoContaCredito("");
		limparTipoConta();
		setCdAgenciaBancariaContaSalario(null);
		setCdContaBancariaContaSalario(null);
		setIspb(null);
		setCdContaPagamento(null);
		setCdBancoPagamento(null);
		if("3".equals(getRadioPesquisaFiltroPrincipal()) && "3".equals(getRadioFavorecidoFiltro())){
			setCdBancoContaSalario(237);
		}else{
			setCdBancoContaSalario(null);
		}
	}

	/**
	 * Limpar beneficiario.
	 */
	public void limparBeneficiario() {
		setCodigoBeneficiarioFiltro("");
		setTipoBeneficiarioFiltro(null);
		setOrgaoPagadorFiltro(null);
		setInscricaoFiltroBeneficiario("");
		setRadioFavorecidoFiltro(null);
	}

	/**
	 * Limpar campos.
	 */
	public void limparCampos() {
		FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean = (FiltroAgendamentoEfetivacaoEstornoBean) FacesUtils
				.getManagedBean("filtroAgendamentoEfetivacaoEstornoBean");

		filtroAgendamentoEfetivacaoEstornoBean.limparParticipanteContrato();
		setChkContaDebito(false);
		setChkHabilitaCredito(false);
		setChkHabilitaSalario(false);
		setChkHabilitaPagamento(false);
		limparContaDebito();
		limparRemessaFavorecidoBeneficiarioLinha();
		if (getFiltroAgendamentoEfetivacaoEstornoBean() != null
				&& getFiltroAgendamentoEfetivacaoEstornoBean()
						.getPaginaRetorno() != null
				&& getFiltroAgendamentoEfetivacaoEstornoBean()
						.getPaginaRetorno().equals("conSolRecPagVenNaoPagos")) {
			limparMotivo();
		} else {
			limparSituacaoMotivo();
		}
		limparTipoServicoModalidade();

		setAgendadosPagosNaoPagos("");
		setDataInicialPagamentoFiltro(new Date());
		setDataFinalPagamentoFiltro(new Date());
		setChkParticipanteContrato(false);
		setChkRemessa(false);
		setChkServicoModalidade(false);
		setChkSituacaoMotivo(false);
		setRadioPesquisaFiltroPrincipal("");
		setRadioFavorecidoFiltro("");
		setRadioBeneficiarioFiltro("");

		setClassificacaoFiltro(null);

		setChkNumeroListaDebito(false);
		setNumListaDebitoDeFiltro("");
		setNumListaDebitoAteFiltro("");

		setCboSituacaoListaDebito(null);
		setChkSituacaoListaDebito(false);
		setValorPagamentoAteFiltro(null);
		setValorPagamentoDeFiltro(null);
	}

	public void limparCamposLoteInterno() {
		setLoteInterno(null);
		setFiltroTipoLayoutArquivo(0);
	}

	/**
	 * Consultar tipo conta.
	 * 
	 * @return the string
	 */
	public String consultarTipoConta() {
		try {
			ConsultarTipoContaEntradaDTO entrada = new ConsultarTipoContaEntradaDTO();
			entrada.setCdAgencia(getCdAgenciaBancariaContaCredito());
			entrada.setCdBanco(getCdBancoContaCredito());
			entrada.setCdConta(getCdContaBancariaContaCredito());
			entrada.setCdDigitoConta(getCdDigitoContaCredito());
			entrada.setCdDigitoAgencia(0);

			List<ConsultarTipoContaSaidaDTO> listaTipoConta = getComboService()
					.consultarTipoConta(entrada);

			listaSituacaoListaDebitoHash.clear();
			listaCmbTipoConta = new ArrayList<SelectItem>();

			for (int i = 0; i < listaTipoConta.size(); i++) {
				listaCmbTipoConta.add(new SelectItem(listaTipoConta.get(i)
						.getCdTipo(), listaTipoConta.get(i).getDsTipo()));
				listaSituacaoListaDebitoHash.put(listaTipoConta.get(i)
						.getCdTipo(), listaTipoConta.get(i));
			}
		} catch (PdcAdapterFunctionalException p) {
			listaCmbTipoConta = new ArrayList<SelectItem>();
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "";
	}

	/**
	 * Consultar tipo conta debito.
	 * 
	 * @return the string
	 */
	public String consultarTipoContaDebito() {
		try {
			ConsultarTipoContaEntradaDTO entrada = new ConsultarTipoContaEntradaDTO();
			entrada.setCdAgencia(getCdAgenciaBancariaContaDebito());
			entrada.setCdBanco(getCdBancoContaDebito());
			entrada.setCdConta(getCdContaBancariaContaDebito());
			entrada.setCdDigitoConta(getCdDigitoContaContaDebito());
			entrada.setCdDigitoAgencia(0);

			List<ConsultarTipoContaSaidaDTO> listaTipoConta = getComboService()
					.consultarTipoConta(entrada);

			getListaCmbTipoContaDebitoHash().clear();
			setListaCmbTipoContaDebito(new ArrayList<SelectItem>());

			for (int i = 0; i < listaTipoConta.size(); i++) {
				getListaCmbTipoContaDebito().add(
						new SelectItem(listaTipoConta.get(i).getCdTipo(),
								listaTipoConta.get(i).getDsTipo()));
				getListaCmbTipoContaDebitoHash().put(
						listaTipoConta.get(i).getCdTipo(),
						listaTipoConta.get(i));
			}
		} catch (PdcAdapterFunctionalException p) {
			listaCmbTipoContaDebito = new ArrayList<SelectItem>();
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "";
	}

	/**
	 * Get: agendadosPagosNaoPagos.
	 * 
	 * @return agendadosPagosNaoPagos
	 */
	public String getAgendadosPagosNaoPagos() {
		return agendadosPagosNaoPagos;
	}

	/**
	 * Set: agendadosPagosNaoPagos.
	 * 
	 * @param agendadosPagosNaoPagos
	 *            the agendados pagos nao pagos
	 */
	public void setAgendadosPagosNaoPagos(String agendadosPagosNaoPagos) {
		this.agendadosPagosNaoPagos = agendadosPagosNaoPagos;
	}

	/**
	 * Get: cboMotivoSituacaoFiltro.
	 * 
	 * @return cboMotivoSituacaoFiltro
	 */
	public Integer getCboMotivoSituacaoFiltro() {
		return cboMotivoSituacaoFiltro;
	}

	/**
	 * Set: cboMotivoSituacaoFiltro.
	 * 
	 * @param cboMotivoSituacaoFiltro
	 *            the cbo motivo situacao filtro
	 */
	public void setCboMotivoSituacaoFiltro(Integer cboMotivoSituacaoFiltro) {
		this.cboMotivoSituacaoFiltro = cboMotivoSituacaoFiltro;
	}

	/**
	 * Get: cboSituacaoPagamentoFiltro.
	 * 
	 * @return cboSituacaoPagamentoFiltro
	 */
	public Integer getCboSituacaoPagamentoFiltro() {
		return cboSituacaoPagamentoFiltro;
	}

	/**
	 * Set: cboSituacaoPagamentoFiltro.
	 * 
	 * @param cboSituacaoPagamentoFiltro
	 *            the cbo situacao pagamento filtro
	 */
	public void setCboSituacaoPagamentoFiltro(Integer cboSituacaoPagamentoFiltro) {
		this.cboSituacaoPagamentoFiltro = cboSituacaoPagamentoFiltro;
	}

	/**
	 * Get: cdAgenciaBancariaContaCredito.
	 * 
	 * @return cdAgenciaBancariaContaCredito
	 */
	public Integer getCdAgenciaBancariaContaCredito() {
		return cdAgenciaBancariaContaCredito;
	}

	/**
	 * Set: cdAgenciaBancariaContaCredito.
	 * 
	 * @param cdAgenciaBancariaContaCredito
	 *            the cd agencia bancaria conta credito
	 */
	public void setCdAgenciaBancariaContaCredito(
			Integer cdAgenciaBancariaContaCredito) {
		this.cdAgenciaBancariaContaCredito = cdAgenciaBancariaContaCredito;
	}

	/**
	 * Get: cdAgenciaBancariaContaDebito.
	 * 
	 * @return cdAgenciaBancariaContaDebito
	 */
	public Integer getCdAgenciaBancariaContaDebito() {
		return cdAgenciaBancariaContaDebito;
	}

	/**
	 * Set: cdAgenciaBancariaContaDebito.
	 * 
	 * @param cdAgenciaBancariaContaDebito
	 *            the cd agencia bancaria conta debito
	 */
	public void setCdAgenciaBancariaContaDebito(
			Integer cdAgenciaBancariaContaDebito) {
		this.cdAgenciaBancariaContaDebito = cdAgenciaBancariaContaDebito;
	}

	/**
	 * Get: cdBancoContaCredito.
	 * 
	 * @return cdBancoContaCredito
	 */
	public Integer getCdBancoContaCredito() {
		return cdBancoContaCredito;
	}

	/**
	 * Set: cdBancoContaCredito.
	 * 
	 * @param cdBancoContaCredito
	 *            the cd banco conta credito
	 */
	public void setCdBancoContaCredito(Integer cdBancoContaCredito) {
		this.cdBancoContaCredito = cdBancoContaCredito;
	}

	/**
	 * Get: cdBancoContaDebito.
	 * 
	 * @return cdBancoContaDebito
	 */
	public Integer getCdBancoContaDebito() {
		return cdBancoContaDebito;
	}

	/**
	 * Set: cdBancoContaDebito.
	 * 
	 * @param cdBancoContaDebito
	 *            the cd banco conta debito
	 */
	public void setCdBancoContaDebito(Integer cdBancoContaDebito) {
		this.cdBancoContaDebito = cdBancoContaDebito;
	}

	/**
	 * Get: cdContaBancariaContaCredito.
	 * 
	 * @return cdContaBancariaContaCredito
	 */
	public Long getCdContaBancariaContaCredito() {
		return cdContaBancariaContaCredito;
	}

	/**
	 * Set: cdContaBancariaContaCredito.
	 * 
	 * @param cdContaBancariaContaCredito
	 *            the cd conta bancaria conta credito
	 */
	public void setCdContaBancariaContaCredito(Long cdContaBancariaContaCredito) {
		this.cdContaBancariaContaCredito = cdContaBancariaContaCredito;
	}

	/**
	 * Get: cdContaBancariaContaDebito.
	 * 
	 * @return cdContaBancariaContaDebito
	 */
	public Long getCdContaBancariaContaDebito() {
		return cdContaBancariaContaDebito;
	}

	/**
	 * Set: cdContaBancariaContaDebito.
	 * 
	 * @param cdContaBancariaContaDebito
	 *            the cd conta bancaria conta debito
	 */
	public void setCdContaBancariaContaDebito(Long cdContaBancariaContaDebito) {
		this.cdContaBancariaContaDebito = cdContaBancariaContaDebito;
	}

	/**
	 * Get: cdDigitoContaContaDebito.
	 * 
	 * @return cdDigitoContaContaDebito
	 */
	public String getCdDigitoContaContaDebito() {
		return cdDigitoContaContaDebito;
	}

	/**
	 * Set: cdDigitoContaContaDebito.
	 * 
	 * @param cdDigitoContaContaDebito
	 *            the cd digito conta conta debito
	 */
	public void setCdDigitoContaContaDebito(String cdDigitoContaContaDebito) {
		this.cdDigitoContaContaDebito = cdDigitoContaContaDebito;
	}

	/**
	 * Get: cdDigitoContaCredito.
	 * 
	 * @return cdDigitoContaCredito
	 */
	public String getCdDigitoContaCredito() {
		return cdDigitoContaCredito;
	}

	/**
	 * Set: cdDigitoContaCredito.
	 * 
	 * @param cdDigitoContaCredito
	 *            the cd digito conta credito
	 */
	public void setCdDigitoContaCredito(String cdDigitoContaCredito) {
		this.cdDigitoContaCredito = cdDigitoContaCredito;
	}

	/**
	 * Is chk conta debito.
	 * 
	 * @return true, if is chk conta debito
	 */
	public boolean isChkContaDebito() {
		return chkContaDebito;
	}

	/**
	 * Set: chkContaDebito.
	 * 
	 * @param chkContaDebito
	 *            the chk conta debito
	 */
	public void setChkContaDebito(boolean chkContaDebito) {
		this.chkContaDebito = chkContaDebito;
	}

	/**
	 * Is chk participante contrato.
	 * 
	 * @return true, if is chk participante contrato
	 */
	public boolean isChkParticipanteContrato() {
		return chkParticipanteContrato;
	}

	/**
	 * Set: chkParticipanteContrato.
	 * 
	 * @param chkParticipanteContrato
	 *            the chk participante contrato
	 */
	public void setChkParticipanteContrato(boolean chkParticipanteContrato) {
		this.chkParticipanteContrato = chkParticipanteContrato;
	}

	/**
	 * Is chk servico modalidade.
	 * 
	 * @return true, if is chk servico modalidade
	 */
	public boolean isChkServicoModalidade() {
		return chkServicoModalidade;
	}

	/**
	 * Set: chkServicoModalidade.
	 * 
	 * @param chkServicoModalidade
	 *            the chk servico modalidade
	 */
	public void setChkServicoModalidade(boolean chkServicoModalidade) {
		this.chkServicoModalidade = chkServicoModalidade;
	}

	/**
	 * Is chk situacao motivo.
	 * 
	 * @return true, if is chk situacao motivo
	 */
	public boolean isChkSituacaoMotivo() {
		return chkSituacaoMotivo;
	}

	/**
	 * Set: chkSituacaoMotivo.
	 * 
	 * @param chkSituacaoMotivo
	 *            the chk situacao motivo
	 */
	public void setChkSituacaoMotivo(boolean chkSituacaoMotivo) {
		this.chkSituacaoMotivo = chkSituacaoMotivo;
	}

	/**
	 * Get: classificacaoFiltro.
	 * 
	 * @return classificacaoFiltro
	 */
	public Integer getClassificacaoFiltro() {
		return classificacaoFiltro;
	}

	/**
	 * Set: classificacaoFiltro.
	 * 
	 * @param classificacaoFiltro
	 *            the classificacao filtro
	 */
	public void setClassificacaoFiltro(Integer classificacaoFiltro) {
		this.classificacaoFiltro = classificacaoFiltro;
	}

	/**
	 * Get: codigoBeneficiarioFiltro.
	 * 
	 * @return codigoBeneficiarioFiltro
	 */
	public String getCodigoBeneficiarioFiltro() {
		return codigoBeneficiarioFiltro;
	}

	/**
	 * Set: codigoBeneficiarioFiltro.
	 * 
	 * @param codigoBeneficiarioFiltro
	 *            the codigo beneficiario filtro
	 */
	public void setCodigoBeneficiarioFiltro(String codigoBeneficiarioFiltro) {
		this.codigoBeneficiarioFiltro = codigoBeneficiarioFiltro;
	}

	/**
	 * Get: codigoFavorecidoFiltro.
	 * 
	 * @return codigoFavorecidoFiltro
	 */
	public String getCodigoFavorecidoFiltro() {
		return codigoFavorecidoFiltro;
	}

	/**
	 * Set: codigoFavorecidoFiltro.
	 * 
	 * @param codigoFavorecidoFiltro
	 *            the codigo favorecido filtro
	 */
	public void setCodigoFavorecidoFiltro(String codigoFavorecidoFiltro) {
		this.codigoFavorecidoFiltro = codigoFavorecidoFiltro;
	}

	/**
	 * Get: dataFinalPagamentoFiltro.
	 * 
	 * @return dataFinalPagamentoFiltro
	 */
	public Date getDataFinalPagamentoFiltro() {
		return dataFinalPagamentoFiltro;
	}

	/**
	 * Set: dataFinalPagamentoFiltro.
	 * 
	 * @param dataFinalPagamentoFiltro
	 *            the data final pagamento filtro
	 */
	public void setDataFinalPagamentoFiltro(Date dataFinalPagamentoFiltro) {
		this.dataFinalPagamentoFiltro = dataFinalPagamentoFiltro;
	}

	/**
	 * Get: dataInicialPagamentoFiltro.
	 * 
	 * @return dataInicialPagamentoFiltro
	 */
	public Date getDataInicialPagamentoFiltro() {
		return dataInicialPagamentoFiltro;
	}

	/**
	 * Set: dataInicialPagamentoFiltro.
	 * 
	 * @param dataInicialPagamentoFiltro
	 *            the data inicial pagamento filtro
	 */
	public void setDataInicialPagamentoFiltro(Date dataInicialPagamentoFiltro) {
		this.dataInicialPagamentoFiltro = dataInicialPagamentoFiltro;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 * 
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 * 
	 * @param filtroAgendamentoEfetivacaoEstornoBean
	 *            the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 * 
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 * 
	 * @param habilitaArgumentosPesquisa
	 *            the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}

	/**
	 * Get: inscricaoFiltro.
	 * 
	 * @return inscricaoFiltro
	 */
	public String getInscricaoFiltro() {
		return inscricaoFiltro;
	}

	/**
	 * Set: inscricaoFiltro.
	 * 
	 * @param inscricaoFiltro
	 *            the inscricao filtro
	 */
	public void setInscricaoFiltro(String inscricaoFiltro) {
		this.inscricaoFiltro = inscricaoFiltro;
	}

	/**
	 * Get: linhaDigitavel1.
	 * 
	 * @return linhaDigitavel1
	 */
	public String getLinhaDigitavel1() {
		return linhaDigitavel1;
	}

	/**
	 * Set: linhaDigitavel1.
	 * 
	 * @param linhaDigitavel1
	 *            the linha digitavel1
	 */
	public void setLinhaDigitavel1(String linhaDigitavel1) {
		this.linhaDigitavel1 = linhaDigitavel1;
	}

	/**
	 * Get: linhaDigitavel2.
	 * 
	 * @return linhaDigitavel2
	 */
	public String getLinhaDigitavel2() {
		return linhaDigitavel2;
	}

	/**
	 * Set: linhaDigitavel2.
	 * 
	 * @param linhaDigitavel2
	 *            the linha digitavel2
	 */
	public void setLinhaDigitavel2(String linhaDigitavel2) {
		this.linhaDigitavel2 = linhaDigitavel2;
	}

	/**
	 * Get: linhaDigitavel3.
	 * 
	 * @return linhaDigitavel3
	 */
	public String getLinhaDigitavel3() {
		return linhaDigitavel3;
	}

	/**
	 * Set: linhaDigitavel3.
	 * 
	 * @param linhaDigitavel3
	 *            the linha digitavel3
	 */
	public void setLinhaDigitavel3(String linhaDigitavel3) {
		this.linhaDigitavel3 = linhaDigitavel3;
	}

	/**
	 * Get: linhaDigitavel4.
	 * 
	 * @return linhaDigitavel4
	 */
	public String getLinhaDigitavel4() {
		return linhaDigitavel4;
	}

	/**
	 * Set: linhaDigitavel4.
	 * 
	 * @param linhaDigitavel4
	 *            the linha digitavel4
	 */
	public void setLinhaDigitavel4(String linhaDigitavel4) {
		this.linhaDigitavel4 = linhaDigitavel4;
	}

	/**
	 * Get: linhaDigitavel5.
	 * 
	 * @return linhaDigitavel5
	 */
	public String getLinhaDigitavel5() {
		return linhaDigitavel5;
	}

	/**
	 * Set: linhaDigitavel5.
	 * 
	 * @param linhaDigitavel5
	 *            the linha digitavel5
	 */
	public void setLinhaDigitavel5(String linhaDigitavel5) {
		this.linhaDigitavel5 = linhaDigitavel5;
	}

	/**
	 * Get: linhaDigitavel6.
	 * 
	 * @return linhaDigitavel6
	 */
	public String getLinhaDigitavel6() {
		return linhaDigitavel6;
	}

	/**
	 * Set: linhaDigitavel6.
	 * 
	 * @param linhaDigitavel6
	 *            the linha digitavel6
	 */
	public void setLinhaDigitavel6(String linhaDigitavel6) {
		this.linhaDigitavel6 = linhaDigitavel6;
	}

	/**
	 * Get: linhaDigitavel7.
	 * 
	 * @return linhaDigitavel7
	 */
	public String getLinhaDigitavel7() {
		return linhaDigitavel7;
	}

	/**
	 * Set: linhaDigitavel7.
	 * 
	 * @param linhaDigitavel7
	 *            the linha digitavel7
	 */
	public void setLinhaDigitavel7(String linhaDigitavel7) {
		this.linhaDigitavel7 = linhaDigitavel7;
	}

	/**
	 * Get: linhaDigitavel8.
	 * 
	 * @return linhaDigitavel8
	 */
	public String getLinhaDigitavel8() {
		return linhaDigitavel8;
	}

	/**
	 * Set: linhaDigitavel8.
	 * 
	 * @param linhaDigitavel8
	 *            the linha digitavel8
	 */
	public void setLinhaDigitavel8(String linhaDigitavel8) {
		this.linhaDigitavel8 = linhaDigitavel8;
	}

	/**
	 * Get: listaConsultarMotivoSituacaoPagamentoFiltro.
	 * 
	 * @return listaConsultarMotivoSituacaoPagamentoFiltro
	 */
	public List<SelectItem> getListaConsultarMotivoSituacaoPagamentoFiltro() {
		return listaConsultarMotivoSituacaoPagamentoFiltro;
	}

	/**
	 * Set: listaConsultarMotivoSituacaoPagamentoFiltro.
	 * 
	 * @param listaConsultarMotivoSituacaoPagamentoFiltro
	 *            the lista consultar motivo situacao pagamento filtro
	 */
	public void setListaConsultarMotivoSituacaoPagamentoFiltro(
			List<SelectItem> listaConsultarMotivoSituacaoPagamentoFiltro) {
		this.listaConsultarMotivoSituacaoPagamentoFiltro = listaConsultarMotivoSituacaoPagamentoFiltro;
	}

	/**
	 * Get: listaConsultarSituacaoPagamentoFiltro.
	 * 
	 * @return listaConsultarSituacaoPagamentoFiltro
	 */
	public List<SelectItem> getListaConsultarSituacaoPagamentoFiltro() {
		return listaConsultarSituacaoPagamentoFiltro;
	}

	/**
	 * Set: listaConsultarSituacaoPagamentoFiltro.
	 * 
	 * @param listaConsultarSituacaoPagamentoFiltro
	 *            the lista consultar situacao pagamento filtro
	 */
	public void setListaConsultarSituacaoPagamentoFiltro(
			List<SelectItem> listaConsultarSituacaoPagamentoFiltro) {
		this.listaConsultarSituacaoPagamentoFiltro = listaConsultarSituacaoPagamentoFiltro;
	}

	/**
	 * Get: listaInscricaoFavorecidoFiltro.
	 * 
	 * @return listaInscricaoFavorecidoFiltro
	 */
	public List<SelectItem> getListaInscricaoFavorecidoFiltro() {
		return listaInscricaoFavorecidoFiltro;
	}

	/**
	 * Set: listaInscricaoFavorecidoFiltro.
	 * 
	 * @param listaInscricaoFavorecidoFiltro
	 *            the lista inscricao favorecido filtro
	 */
	public void setListaInscricaoFavorecidoFiltro(
			List<SelectItem> listaInscricaoFavorecidoFiltro) {
		this.listaInscricaoFavorecidoFiltro = listaInscricaoFavorecidoFiltro;
	}

	/**
	 * Get: listaModalidadeFiltro.
	 * 
	 * @return listaModalidadeFiltro
	 */
	public List<SelectItem> getListaModalidadeFiltro() {
		return listaModalidadeFiltro;
	}

	/**
	 * Set: listaModalidadeFiltro.
	 * 
	 * @param listaModalidadeFiltro
	 *            the lista modalidade filtro
	 */
	public void setListaModalidadeFiltro(List<SelectItem> listaModalidadeFiltro) {
		this.listaModalidadeFiltro = listaModalidadeFiltro;
	}

	/**
	 * Get: listaOrgaoPagadorFiltro.
	 * 
	 * @return listaOrgaoPagadorFiltro
	 */
	public List<SelectItem> getListaOrgaoPagadorFiltro() {
		return listaOrgaoPagadorFiltro;
	}

	/**
	 * Set: listaOrgaoPagadorFiltro.
	 * 
	 * @param listaOrgaoPagadorFiltro
	 *            the lista orgao pagador filtro
	 */
	public void setListaOrgaoPagadorFiltro(
			List<SelectItem> listaOrgaoPagadorFiltro) {
		this.listaOrgaoPagadorFiltro = listaOrgaoPagadorFiltro;
	}

	/**
	 * Get: listaTipoServicoFiltro.
	 * 
	 * @return listaTipoServicoFiltro
	 */
	public List<SelectItem> getListaTipoServicoFiltro() {
		return listaTipoServicoFiltro;
	}

	/**
	 * Set: listaTipoServicoFiltro.
	 * 
	 * @param listaTipoServicoFiltro
	 *            the lista tipo servico filtro
	 */
	public void setListaTipoServicoFiltro(
			List<SelectItem> listaTipoServicoFiltro) {
		this.listaTipoServicoFiltro = listaTipoServicoFiltro;
	}

	/**
	 * Get: modalidadeFiltro.
	 * 
	 * @return modalidadeFiltro
	 */
	public Integer getModalidadeFiltro() {
		return modalidadeFiltro;
	}

	/**
	 * Set: modalidadeFiltro.
	 * 
	 * @param modalidadeFiltro
	 *            the modalidade filtro
	 */
	public void setModalidadeFiltro(Integer modalidadeFiltro) {
		this.modalidadeFiltro = modalidadeFiltro;
	}

	/**
	 * Get: numeroPagamentoAteFiltro.
	 * 
	 * @return numeroPagamentoAteFiltro
	 */
	public String getNumeroPagamentoAteFiltro() {
		return numeroPagamentoAteFiltro;
	}

	/**
	 * Set: numeroPagamentoAteFiltro.
	 * 
	 * @param numeroPagamentoAteFiltro
	 *            the numero pagamento ate filtro
	 */
	public void setNumeroPagamentoAteFiltro(String numeroPagamentoAteFiltro) {
		this.numeroPagamentoAteFiltro = numeroPagamentoAteFiltro;
	}

	/**
	 * Get: numeroPagamentoDeFiltro.
	 * 
	 * @return numeroPagamentoDeFiltro
	 */
	public String getNumeroPagamentoDeFiltro() {
		return numeroPagamentoDeFiltro;
	}

	/**
	 * Set: numeroPagamentoDeFiltro.
	 * 
	 * @param numeroPagamentoDeFiltro
	 *            the numero pagamento de filtro
	 */
	public void setNumeroPagamentoDeFiltro(String numeroPagamentoDeFiltro) {
		this.numeroPagamentoDeFiltro = numeroPagamentoDeFiltro;
	}

	/**
	 * Get: numeroRemessaFiltro.
	 * 
	 * @return numeroRemessaFiltro
	 */
	public String getNumeroRemessaFiltro() {
		return numeroRemessaFiltro;
	}

	/**
	 * Set: numeroRemessaFiltro.
	 * 
	 * @param numeroRemessaFiltro
	 *            the numero remessa filtro
	 */
	public void setNumeroRemessaFiltro(String numeroRemessaFiltro) {
		this.numeroRemessaFiltro = numeroRemessaFiltro;
	}

	/**
	 * Get: orgaoPagadorFiltro.
	 * 
	 * @return orgaoPagadorFiltro
	 */
	public Integer getOrgaoPagadorFiltro() {
		return orgaoPagadorFiltro;
	}

	/**
	 * Set: orgaoPagadorFiltro.
	 * 
	 * @param orgaoPagadorFiltro
	 *            the orgao pagador filtro
	 */
	public void setOrgaoPagadorFiltro(Integer orgaoPagadorFiltro) {
		this.orgaoPagadorFiltro = orgaoPagadorFiltro;
	}

	/**
	 * Get: radioBeneficiarioFiltro.
	 * 
	 * @return radioBeneficiarioFiltro
	 */
	public String getRadioBeneficiarioFiltro() {
		return radioBeneficiarioFiltro;
	}

	/**
	 * Set: radioBeneficiarioFiltro.
	 * 
	 * @param radioBeneficiarioFiltro
	 *            the radio beneficiario filtro
	 */
	public void setRadioBeneficiarioFiltro(String radioBeneficiarioFiltro) {
		this.radioBeneficiarioFiltro = radioBeneficiarioFiltro;
	}

	/**
	 * Get: radioFavorecidoFiltro.
	 * 
	 * @return radioFavorecidoFiltro
	 */
	public String getRadioFavorecidoFiltro() {
		return radioFavorecidoFiltro;
	}

	/**
	 * Set: radioFavorecidoFiltro.
	 * 
	 * @param radioFavorecidoFiltro
	 *            the radio favorecido filtro
	 */
	public void setRadioFavorecidoFiltro(String radioFavorecidoFiltro) {
		this.radioFavorecidoFiltro = radioFavorecidoFiltro;
	}

	/**
	 * Get: radioPesquisaFiltroPrincipal.
	 * 
	 * @return radioPesquisaFiltroPrincipal
	 */
	public String getRadioPesquisaFiltroPrincipal() {
		return radioPesquisaFiltroPrincipal;
	}

	/**
	 * Set: radioPesquisaFiltroPrincipal.
	 * 
	 * @param radioPesquisaFiltroPrincipal
	 *            the radio pesquisa filtro principal
	 */
	public void setRadioPesquisaFiltroPrincipal(
			String radioPesquisaFiltroPrincipal) {
		this.radioPesquisaFiltroPrincipal = radioPesquisaFiltroPrincipal;
	}

	/**
	 * Get: tipoBeneficiarioFiltro.
	 * 
	 * @return tipoBeneficiarioFiltro
	 */
	public Integer getTipoBeneficiarioFiltro() {
		return tipoBeneficiarioFiltro;
	}

	/**
	 * Set: tipoBeneficiarioFiltro.
	 * 
	 * @param tipoBeneficiarioFiltro
	 *            the tipo beneficiario filtro
	 */
	public void setTipoBeneficiarioFiltro(Integer tipoBeneficiarioFiltro) {
		this.tipoBeneficiarioFiltro = tipoBeneficiarioFiltro;
	}

	/**
	 * Get: tipoInscricaoFiltro.
	 * 
	 * @return tipoInscricaoFiltro
	 */
	public Integer getTipoInscricaoFiltro() {
		return tipoInscricaoFiltro;
	}

	/**
	 * Set: tipoInscricaoFiltro.
	 * 
	 * @param tipoInscricaoFiltro
	 *            the tipo inscricao filtro
	 */
	public void setTipoInscricaoFiltro(Integer tipoInscricaoFiltro) {
		this.tipoInscricaoFiltro = tipoInscricaoFiltro;
	}

	/**
	 * Get: tipoServicoFiltro.
	 * 
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 * 
	 * @param tipoServicoFiltro
	 *            the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Get: valorPagamentoAteFiltro.
	 * 
	 * @return valorPagamentoAteFiltro
	 */
	public BigDecimal getValorPagamentoAteFiltro() {
		return valorPagamentoAteFiltro;
	}

	/**
	 * Set: valorPagamentoAteFiltro.
	 * 
	 * @param valorPagamentoAteFiltro
	 *            the valor pagamento ate filtro
	 */
	public void setValorPagamentoAteFiltro(BigDecimal valorPagamentoAteFiltro) {
		this.valorPagamentoAteFiltro = valorPagamentoAteFiltro;
	}

	/**
	 * Get: valorPagamentoDeFiltro.
	 * 
	 * @return valorPagamentoDeFiltro
	 */
	public BigDecimal getValorPagamentoDeFiltro() {
		return valorPagamentoDeFiltro;
	}

	/**
	 * Set: valorPagamentoDeFiltro.
	 * 
	 * @param valorPagamentoDeFiltro
	 *            the valor pagamento de filtro
	 */
	public void setValorPagamentoDeFiltro(BigDecimal valorPagamentoDeFiltro) {
		this.valorPagamentoDeFiltro = valorPagamentoDeFiltro;
	}

	/**
	 * Get: consultarPagamentosIndividualServiceImpl.
	 * 
	 * @return consultarPagamentosIndividualServiceImpl
	 */
	public IConsultarPagamentosIndividualService getConsultarPagamentosIndividualServiceImpl() {
		return consultarPagamentosIndividualServiceImpl;
	}

	/**
	 * Set: consultarPagamentosIndividualServiceImpl.
	 * 
	 * @param consultarPagamentosIndividualServiceImpl
	 *            the consultar pagamentos individual service impl
	 */
	public void setConsultarPagamentosIndividualServiceImpl(
			IConsultarPagamentosIndividualService consultarPagamentosIndividualServiceImpl) {
		this.consultarPagamentosIndividualServiceImpl = consultarPagamentosIndividualServiceImpl;
	}

	/**
	 * Get: comboService.
	 * 
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 * 
	 * @param comboService
	 *            the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Is chk remessa.
	 * 
	 * @return true, if is chk remessa
	 */
	public boolean isChkRemessa() {
		return chkRemessa;
	}

	/**
	 * Set: chkRemessa.
	 * 
	 * @param chkRemessa
	 *            the chk remessa
	 */
	public void setChkRemessa(boolean chkRemessa) {
		this.chkRemessa = chkRemessa;
	}

	/**
	 * Is chk numero lista debito.
	 * 
	 * @return true, if is chk numero lista debito
	 */
	public boolean isChkNumeroListaDebito() {
		return chkNumeroListaDebito;
	}

	/**
	 * Set: chkNumeroListaDebito.
	 * 
	 * @param chkNumeroListaDebito
	 *            the chk numero lista debito
	 */
	public void setChkNumeroListaDebito(boolean chkNumeroListaDebito) {
		this.chkNumeroListaDebito = chkNumeroListaDebito;
	}

	/**
	 * Get: numListaDebitoAteFiltro.
	 * 
	 * @return numListaDebitoAteFiltro
	 */
	public String getNumListaDebitoAteFiltro() {
		return numListaDebitoAteFiltro;
	}

	/**
	 * Set: numListaDebitoAteFiltro.
	 * 
	 * @param numListaDebitoAteFiltro
	 *            the num lista debito ate filtro
	 */
	public void setNumListaDebitoAteFiltro(String numListaDebitoAteFiltro) {
		this.numListaDebitoAteFiltro = numListaDebitoAteFiltro;
	}

	/**
	 * Get: numListaDebitoDeFiltro.
	 * 
	 * @return numListaDebitoDeFiltro
	 */
	public String getNumListaDebitoDeFiltro() {
		return numListaDebitoDeFiltro;
	}

	/**
	 * Set: numListaDebitoDeFiltro.
	 * 
	 * @param numListaDebitoDeFiltro
	 *            the num lista debito de filtro
	 */
	public void setNumListaDebitoDeFiltro(String numListaDebitoDeFiltro) {
		this.numListaDebitoDeFiltro = numListaDebitoDeFiltro;
	}

	/**
	 * Get: cboSituacaoListaDebito.
	 * 
	 * @return cboSituacaoListaDebito
	 */
	public Integer getCboSituacaoListaDebito() {
		return cboSituacaoListaDebito;
	}

	/**
	 * Set: cboSituacaoListaDebito.
	 * 
	 * @param cboSituacaoListaDebito
	 *            the cbo situacao lista debito
	 */
	public void setCboSituacaoListaDebito(Integer cboSituacaoListaDebito) {
		this.cboSituacaoListaDebito = cboSituacaoListaDebito;
	}

	/**
	 * Is chk situacao lista debito.
	 * 
	 * @return true, if is chk situacao lista debito
	 */
	public boolean isChkSituacaoListaDebito() {
		return chkSituacaoListaDebito;
	}

	/**
	 * Set: chkSituacaoListaDebito.
	 * 
	 * @param chkSituacaoListaDebito
	 *            the chk situacao lista debito
	 */
	public void setChkSituacaoListaDebito(boolean chkSituacaoListaDebito) {
		this.chkSituacaoListaDebito = chkSituacaoListaDebito;
	}

	/**
	 * Get: listaSituacaoListaDebito.
	 * 
	 * @return listaSituacaoListaDebito
	 */
	public List<SelectItem> getListaSituacaoListaDebito() {
		return listaSituacaoListaDebito;
	}

	/**
	 * Set: listaSituacaoListaDebito.
	 * 
	 * @param listaSituacaoListaDebito
	 *            the lista situacao lista debito
	 */
	public void setListaSituacaoListaDebito(
			List<SelectItem> listaSituacaoListaDebito) {
		this.listaSituacaoListaDebito = listaSituacaoListaDebito;
	}

	/**
	 * Get: listaCmbTipoConta.
	 * 
	 * @return listaCmbTipoConta
	 */
	public List<SelectItem> getListaCmbTipoConta() {
		return listaCmbTipoConta;
	}

	/**
	 * Set: listaCmbTipoConta.
	 * 
	 * @param listaCmbTipoConta
	 *            the lista cmb tipo conta
	 */
	public void setListaCmbTipoConta(List<SelectItem> listaCmbTipoConta) {
		this.listaCmbTipoConta = listaCmbTipoConta;
	}

	/**
	 * Get: cmbTipoConta.
	 * 
	 * @return cmbTipoConta
	 */
	public Integer getCmbTipoConta() {
		return cmbTipoConta;
	}

	/**
	 * Set: cmbTipoConta.
	 * 
	 * @param cmbTipoConta
	 *            the cmb tipo conta
	 */
	public void setCmbTipoConta(Integer cmbTipoConta) {
		this.cmbTipoConta = cmbTipoConta;
	}

	/**
	 * Get: listaSituacaoListaDebitoHash.
	 * 
	 * @return listaSituacaoListaDebitoHash
	 */
	public Map<Integer, ConsultarTipoContaSaidaDTO> getListaSituacaoListaDebitoHash() {
		return listaSituacaoListaDebitoHash;
	}

	/**
	 * Set lista situacao lista debito hash.
	 * 
	 * @param listaSituacaoListaDebitoHash
	 *            the lista situacao lista debito hash
	 */
	public void setListaSituacaoListaDebitoHash(
			Map<Integer, ConsultarTipoContaSaidaDTO> listaSituacaoListaDebitoHash) {
		this.listaSituacaoListaDebitoHash = listaSituacaoListaDebitoHash;
	}

	/**
	 * Get: numeroListaDebitoFiltro.
	 * 
	 * @return numeroListaDebitoFiltro
	 */
	public String getNumeroListaDebitoFiltro() {
		return numeroListaDebitoFiltro;
	}

	/**
	 * Set: numeroListaDebitoFiltro.
	 * 
	 * @param numeroListaDebitoFiltro
	 *            the numero lista debito filtro
	 */
	public void setNumeroListaDebitoFiltro(String numeroListaDebitoFiltro) {
		this.numeroListaDebitoFiltro = numeroListaDebitoFiltro;
	}

	/**
	 * Get: foco.
	 * 
	 * @return foco
	 */
	public String getFoco() {
		return foco;
	}

	/**
	 * Set: foco.
	 * 
	 * @param foco
	 *            the foco
	 */
	public void setFoco(String foco) {
		this.foco = foco;
	}

	/**
	 * Get: listaInscricaoFavorecidoHash.
	 * 
	 * @return listaInscricaoFavorecidoHash
	 */
	public Map<Integer, String> getListaInscricaoFavorecidoHash() {
		return listaInscricaoFavorecidoHash;
	}

	/**
	 * Set lista inscricao favorecido hash.
	 * 
	 * @param listaInscricaoFavorecidoHash
	 *            the lista inscricao favorecido hash
	 */
	public void setListaInscricaoFavorecidoHash(
			Map<Integer, String> listaInscricaoFavorecidoHash) {
		this.listaInscricaoFavorecidoHash = listaInscricaoFavorecidoHash;
	}

	/**
	 * Get: listaModalidadeHash.
	 * 
	 * @return listaModalidadeHash
	 */
	public Map<Integer, String> getListaModalidadeHash() {
		return listaModalidadeHash;
	}

	/**
	 * Set lista modalidade hash.
	 * 
	 * @param listaModalidadeHash
	 *            the lista modalidade hash
	 */
	public void setListaModalidadeHash(Map<Integer, String> listaModalidadeHash) {
		this.listaModalidadeHash = listaModalidadeHash;
	}

	/**
	 * Get: listaTipoServicoHash.
	 * 
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 * 
	 * @param listaTipoServicoHash
	 *            the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(
			Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: listaConsultarMotivoSituacaoPagamentoHash.
	 * 
	 * @return listaConsultarMotivoSituacaoPagamentoHash
	 */
	public Map<Integer, String> getListaConsultarMotivoSituacaoPagamentoHash() {
		return listaConsultarMotivoSituacaoPagamentoHash;
	}

	/**
	 * Set lista consultar motivo situacao pagamento hash.
	 * 
	 * @param listaConsultarMotivoSituacaoPagamentoHash
	 *            the lista consultar motivo situacao pagamento hash
	 */
	public void setListaConsultarMotivoSituacaoPagamentoHash(
			Map<Integer, String> listaConsultarMotivoSituacaoPagamentoHash) {
		this.listaConsultarMotivoSituacaoPagamentoHash = listaConsultarMotivoSituacaoPagamentoHash;
	}

	/**
	 * Get: listaConsultarSituacaoPagamentoHash.
	 * 
	 * @return listaConsultarSituacaoPagamentoHash
	 */
	public Map<Integer, String> getListaConsultarSituacaoPagamentoHash() {
		return listaConsultarSituacaoPagamentoHash;
	}

	/**
	 * Set lista consultar situacao pagamento hash.
	 * 
	 * @param listaConsultarSituacaoPagamentoHash
	 *            the lista consultar situacao pagamento hash
	 */
	public void setListaConsultarSituacaoPagamentoHash(
			Map<Integer, String> listaConsultarSituacaoPagamentoHash) {
		this.listaConsultarSituacaoPagamentoHash = listaConsultarSituacaoPagamentoHash;
	}

	/**
	 * Get: cmbTipoContaDebito.
	 * 
	 * @return cmbTipoContaDebito
	 */
	public Integer getCmbTipoContaDebito() {
		return cmbTipoContaDebito;
	}

	/**
	 * Set: cmbTipoContaDebito.
	 * 
	 * @param cmbTipoContaDebito
	 *            the cmb tipo conta debito
	 */
	public void setCmbTipoContaDebito(Integer cmbTipoContaDebito) {
		this.cmbTipoContaDebito = cmbTipoContaDebito;
	}

	/**
	 * Get: inscricaoFiltroBeneficiario.
	 * 
	 * @return inscricaoFiltroBeneficiario
	 */
	public String getInscricaoFiltroBeneficiario() {
		return inscricaoFiltroBeneficiario;
	}

	/**
	 * Set: inscricaoFiltroBeneficiario.
	 * 
	 * @param inscricaoFiltroBeneficiario
	 *            the inscricao filtro beneficiario
	 */
	public void setInscricaoFiltroBeneficiario(
			String inscricaoFiltroBeneficiario) {
		this.inscricaoFiltroBeneficiario = inscricaoFiltroBeneficiario;
	}

	/**
	 * Get: listaCmbTipoContaDebito.
	 * 
	 * @return listaCmbTipoContaDebito
	 */
	public List<SelectItem> getListaCmbTipoContaDebito() {
		return listaCmbTipoContaDebito;
	}

	/**
	 * Set: listaCmbTipoContaDebito.
	 * 
	 * @param listaCmbTipoContaDebito
	 *            the lista cmb tipo conta debito
	 */
	public void setListaCmbTipoContaDebito(
			List<SelectItem> listaCmbTipoContaDebito) {
		this.listaCmbTipoContaDebito = listaCmbTipoContaDebito;
	}

	/**
	 * Get: listaCmbTipoContaDebitoHash.
	 * 
	 * @return listaCmbTipoContaDebitoHash
	 */
	public Map<Integer, ConsultarTipoContaSaidaDTO> getListaCmbTipoContaDebitoHash() {
		return listaCmbTipoContaDebitoHash;
	}

	/**
	 * Set lista cmb tipo conta debito hash.
	 * 
	 * @param listaCmbTipoContaDebitoHash
	 *            the lista cmb tipo conta debito hash
	 */
	public void setListaCmbTipoContaDebitoHash(
			Map<Integer, ConsultarTipoContaSaidaDTO> listaCmbTipoContaDebitoHash) {
		this.listaCmbTipoContaDebitoHash = listaCmbTipoContaDebitoHash;
	}

	/**
	 * Get: cdGrupoContabil.
	 * 
	 * @return cdGrupoContabil
	 */
	public Integer getCdGrupoContabil() {
		return cdGrupoContabil;
	}

	/**
	 * Set: cdGrupoContabil.
	 * 
	 * @param cdGrupoContabil
	 *            the cd grupo contabil
	 */
	public void setCdGrupoContabil(Integer cdGrupoContabil) {
		this.cdGrupoContabil = cdGrupoContabil;
	}

	/**
	 * Get: cdSubGrupoContabil.
	 * 
	 * @return cdSubGrupoContabil
	 */
	public Integer getCdSubGrupoContabil() {
		return cdSubGrupoContabil;
	}

	/**
	 * Set: cdSubGrupoContabil.
	 * 
	 * @param cdSubGrupoContabil
	 *            the cd sub grupo contabil
	 */
	public void setCdSubGrupoContabil(Integer cdSubGrupoContabil) {
		this.cdSubGrupoContabil = cdSubGrupoContabil;
	}

	/**
	 * Get: listaModalidadeListaDebitoFiltro.
	 * 
	 * @return listaModalidadeListaDebitoFiltro
	 */
	public List<SelectItem> getListaModalidadeListaDebitoFiltro() {
		return listaModalidadeListaDebitoFiltro;
	}

	/**
	 * Set: listaModalidadeListaDebitoFiltro.
	 * 
	 * @param listaModalidadeListaDebitoFiltro
	 *            the lista modalidade lista debito filtro
	 */
	public void setListaModalidadeListaDebitoFiltro(
			List<SelectItem> listaModalidadeListaDebitoFiltro) {
		this.listaModalidadeListaDebitoFiltro = listaModalidadeListaDebitoFiltro;
	}

	/**
	 * Get: listaModalidadeListaDebitoHash.
	 * 
	 * @return listaModalidadeListaDebitoHash
	 */
	public Map<Integer, String> getListaModalidadeListaDebitoHash() {
		return listaModalidadeListaDebitoHash;
	}

	/**
	 * Set lista modalidade lista debito hash.
	 * 
	 * @param listaModalidadeListaDebitoHash
	 *            the lista modalidade lista debito hash
	 */
	public void setListaModalidadeListaDebitoHash(
			Map<Integer, String> listaModalidadeListaDebitoHash) {
		this.listaModalidadeListaDebitoHash = listaModalidadeListaDebitoHash;
	}

	/**
	 * Get: listaModalidadeListaDebito.
	 * 
	 * @return listaModalidadeListaDebito
	 */
	public List<ListarModalidadeListaDebitoSaidaDTO> getListaModalidadeListaDebito() {
		return listaModalidadeListaDebito;
	}

	/**
	 * Set: listaModalidadeListaDebito.
	 * 
	 * @param listaModalidadeListaDebito
	 *            the lista modalidade lista debito
	 */
	public void setListaModalidadeListaDebito(
			List<ListarModalidadeListaDebitoSaidaDTO> listaModalidadeListaDebito) {
		this.listaModalidadeListaDebito = listaModalidadeListaDebito;
	}

	/**
	 * Is chk rastreamento titulo.
	 * 
	 * @return true, if is chk rastreamento titulo
	 */
	public boolean isChkRastreamentoTitulo() {
		return chkRastreamentoTitulo;
	}

	/**
	 * Set: chkRastreamentoTitulo.
	 * 
	 * @param chkRastreamentoTitulo
	 *            the chk rastreamento titulo
	 */
	public void setChkRastreamentoTitulo(boolean chkRastreamentoTitulo) {
		this.chkRastreamentoTitulo = chkRastreamentoTitulo;
	}

	/**
	 * Get: radioRastreados.
	 * 
	 * @return radioRastreados
	 */
	public String getRadioRastreados() {
		return radioRastreados;
	}

	/**
	 * Set: radioRastreados.
	 * 
	 * @param radioRastreados
	 *            the radio rastreados
	 */
	public void setRadioRastreados(String radioRastreados) {
		this.radioRastreados = radioRastreados;
	}

	public String getModalidade() {
		return getListaModalidadeHash().get(getModalidadeFiltro());
	}

	public String getTipoServico() {
		return getListaTipoServicoHash().get(getTipoServicoFiltro());
	}

	public String getTipoLayoutArquivo() {
		return getListaTipoLayoutArquivoHash()
				.get(getFiltroTipoLayoutArquivo());
	}

	public String getDataInicialPagamentoFormatada() {
		return FormatarData.formataDiaMesAno(getDataInicialPagamentoFiltro());
	}

	public String getDataFinalPagamentoFormatada() {
		return FormatarData.formataDiaMesAno(getDataFinalPagamentoFiltro());
	}

	/**
	 * M�todos referente a Lote de Pagamentos
	 */
	public void preencheTipoLayoutArquivo() {
		try {
			setListaTipoLayoutArquivo(new ArrayList<SelectItem>());
			List<TipoLayoutArquivoSaidaDTO> listaTipoLayout = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			TipoLayoutArquivoEntradaDTO tipoLoteEntradaDTO = new TipoLayoutArquivoEntradaDTO();
			tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);

			saidaTipoLayoutArquivoSaidaDTO = getComboService().listarTipoLayoutArquivo(tipoLoteEntradaDTO);
			
			listaTipoLayout = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
			getListaTipoLayoutArquivoHash().clear();
			for (TipoLayoutArquivoSaidaDTO combo : listaTipoLayout) {
				getListaTipoLayoutArquivoHash().put(
						combo.getCdTipoLayoutArquivo(),
						combo.getDsTipoLayoutArquivo());
				listaTipoLayoutArquivo.add(new SelectItem(combo
						.getCdTipoLayoutArquivo(), combo
						.getDsTipoLayoutArquivo()));
			}

		} catch (PdcAdapterFunctionalException e) {
			setListaTipoLayoutArquivo(new ArrayList<SelectItem>());
		}
	}

	public void listarConsultarSituacaoSolicitacao() {
		try {
			setListaSituacaoSolicitacaoFiltro(new ArrayList<SelectItem>());

			ConsultarSituacaoSolicitacaoEstornoEntradaDTO entrada = new ConsultarSituacaoSolicitacaoEstornoEntradaDTO();

			entrada.setCdSituacao(0);
			entrada.setNumeroOcorrencias(30);
			entrada.setCdIndicador(2);

			List<ConsultarSituacaoSolicitacaoEstornoSaidaDTO> list = getComboService()
					.consultarSituacaoSolicitacaoEstorno(entrada);

			for (ConsultarSituacaoSolicitacaoEstornoSaidaDTO saida : list) {
				getListaSituacaoSolicitacaoFiltro()
						.add(
								new SelectItem(saida.getCodigo(), saida
										.getDescricao()));
			}
		} catch (PdcAdapterFunctionalException p) {
			setListaSituacaoSolicitacaoFiltro(new ArrayList<SelectItem>());
		}
	}

	/**
	 * Getters / Setters referente a Lote de Pagamentos
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	public boolean isChkLoteInterno() {
		return chkLoteInterno;
	}

	public void setChkLoteInterno(boolean chkLoteInterno) {
		this.chkLoteInterno = chkLoteInterno;
	}

	public boolean isChkSituacaoSolicitacao() {
		return chkSituacaoSolicitacao;
	}

	public void setChkSituacaoSolicitacao(boolean chkSituacaoSolicitacao) {
		this.chkSituacaoSolicitacao = chkSituacaoSolicitacao;
	}

	public Long getLoteInterno() {
		return loteInterno;
	}

	public void setLoteInterno(Long loteInterno) {
		this.loteInterno = loteInterno;
	}

	public Integer getFiltroTipoLayoutArquivo() {
		return filtroTipoLayoutArquivo;
	}

	public void setFiltroTipoLayoutArquivo(Integer filtroTipoLayoutArquivo) {
		this.filtroTipoLayoutArquivo = filtroTipoLayoutArquivo;
	}

	public List<SelectItem> getListaTipoLayoutArquivo() {
		return listaTipoLayoutArquivo;
	}

	public void setListaTipoLayoutArquivo(
			List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	public Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}

	public void setListaTipoLayoutArquivoHash(
			Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}

	public Integer getCboSituacaoSolicitacao() {
		return cboSituacaoSolicitacao;
	}

	public void setCboSituacaoSolicitacao(Integer cboSituacaoSolicitacao) {
		this.cboSituacaoSolicitacao = cboSituacaoSolicitacao;
	}

	public Integer getCboMotivoSituacao() {
		return cboMotivoSituacao;
	}

	public void setCboMotivoSituacao(Integer cboMotivoSituacao) {
		this.cboMotivoSituacao = cboMotivoSituacao;
	}

	public List<SelectItem> getListaSituacaoSolicitacaoFiltro() {
		return listaSituacaoSolicitacaoFiltro;
	}

	public void setListaSituacaoSolicitacaoFiltro(
			List<SelectItem> listaSituacaoSolicitacaoFiltro) {
		this.listaSituacaoSolicitacaoFiltro = listaSituacaoSolicitacaoFiltro;
	}

	public List<SelectItem> getListaMotivoSituacaoFiltro() {
		return listaMotivoSituacaoFiltro;
	}

	public void setListaMotivoSituacaoFiltro(
			List<SelectItem> listaMotivoSituacaoFiltro) {
		this.listaMotivoSituacaoFiltro = listaMotivoSituacaoFiltro;
	}

	public boolean isChkDataPagamento() {
		return chkDataPagamento;
	}

	public void setChkDataPagamento(boolean chkDataPagamento) {
		this.chkDataPagamento = chkDataPagamento;
	}

	public boolean isChkTipoServico() {
		return chkTipoServico;
	}

	public void setChkTipoServico(boolean chkTipoServico) {
		this.chkTipoServico = chkTipoServico;
	}

	public void listarConsultarMotivoSituacaoFiltro() {
		setCboMotivoSituacao(null);

		setListaMotivoSituacaoFiltro(new ArrayList<SelectItem>());

		if (getCboSituacaoSolicitacao() == null
				|| getCboSituacaoSolicitacao().compareTo(0) == 0) {
			return;
		}

		try {
			ConsultarMotivoSituacaoEstornoEntradaDTO entrada = new ConsultarMotivoSituacaoEstornoEntradaDTO();

			entrada.setCdSituacao(getCboSituacaoSolicitacao());

			List<ConsultarMotivoSituacaoEstornoSaidaDTO> list = getComboService()
					.consultarMotivoSituacaoEstornoSaida(entrada);

			for (ConsultarMotivoSituacaoEstornoSaidaDTO saida : list) {
				getListaMotivoSituacaoFiltro()
						.add(
								new SelectItem(saida.getCodigo(), saida
										.getDescricao()));
			}
		} catch (PdcAdapterFunctionalException p) {
			setListaMotivoSituacaoFiltro(new ArrayList<SelectItem>());
		}
	}

	protected void consolidarCamposInclusaoLotePagamentos() {
		confirmaInclusaoBean = new ConfimarInclusaoLotePagamentosBean();

		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("0")) {
			confirmaInclusaoBean.setNrCnpjCpf(String
					.valueOf(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrCpfCnpjCliente()));
			confirmaInclusaoBean
					.setDsRazaoSocial(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getDescricaoRazaoSocialCliente());
			confirmaInclusaoBean
					.setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getEmpresaGestoraDescCliente());
			confirmaInclusaoBean
					.setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNumeroDescCliente());
			confirmaInclusaoBean
					.setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getDescricaoContratoDescCliente());
			confirmaInclusaoBean
					.setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getSituacaoDescCliente());

		} else if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("1")) {
			confirmaInclusaoBean
					.setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getEmpresaGestoraDescContrato());
			confirmaInclusaoBean
					.setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNumeroDescContrato());
			confirmaInclusaoBean
					.setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getDescricaoContratoDescContrato());
			confirmaInclusaoBean
					.setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getSituacaoDescContrato());
		}
	}

	public ConfimarInclusaoLotePagamentosBean getConfirmaInclusaoBean() {
		return confirmaInclusaoBean;
	}

	public void setConfirmaInclusaoBean(
			ConfimarInclusaoLotePagamentosBean confirmaInclusaoBean) {
		this.confirmaInclusaoBean = confirmaInclusaoBean;
	}

	public List<OcorrenciasSolicLoteDTO> getListaOcorrenciasDetalhe() {
		return listaOcorrenciasDetalhe;
	}

	public void setListaOcorrenciasDetalhe(
			List<OcorrenciasSolicLoteDTO> listaOcorrenciasDetalhe) {
		this.listaOcorrenciasDetalhe = listaOcorrenciasDetalhe;
	}

	public boolean isChkIndicadorAutorizacao() {
		return chkIndicadorAutorizacao;
	}

	public void setChkIndicadorAutorizacao(boolean chkIndicadorAutorizacao) {
		this.chkIndicadorAutorizacao = chkIndicadorAutorizacao;
	}

	public String getRadioIndicadorAutorizacao() {
		return radioIndicadorAutorizacao;
	}

	public void setRadioIndicadorAutorizacao(String radioIndicadorAutorizacao) {
		this.radioIndicadorAutorizacao = radioIndicadorAutorizacao;
	}

	public Boolean getDesabilitaBtnLimparCampos() {
		return desabilitaBtnLimparCampos;
	}

	public void setDesabilitaBtnLimparCampos(Boolean desabilitaBtnLimparCampos) {
		this.desabilitaBtnLimparCampos = desabilitaBtnLimparCampos;
	}

	/**
	 * Nome: setSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @param saidaTipoLayoutArquivoSaidaDTO
	 */
	public void setSaidaTipoLayoutArquivoSaidaDTO(
			TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
		this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: getSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @return saidaTipoLayoutArquivoSaidaDTO
	 */
	public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
		return saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * @return the cdBancoContaSalario
	 */
	public Integer getCdBancoContaSalario() {
		return cdBancoContaSalario;
	}

	/**
	 * @param cdBancoContaSalario the cdBancoContaSalario to set
	 */
	public void setCdBancoContaSalario(Integer cdBancoContaSalario) {
		this.cdBancoContaSalario = cdBancoContaSalario;
	}

	/**
	 * @return the cdAgenciaBancariaContaSalario
	 */
	public Integer getCdAgenciaBancariaContaSalario() {
		return cdAgenciaBancariaContaSalario;
	}

	/**
	 * @param cdAgenciaBancariaContaSalario the cdAgenciaBancariaContaSalario to set
	 */
	public void setCdAgenciaBancariaContaSalario(
			Integer cdAgenciaBancariaContaSalario) {
		this.cdAgenciaBancariaContaSalario = cdAgenciaBancariaContaSalario;
	}

	/**
	 * @return the cdContaBancariaContaSalario
	 */
	public Long getCdContaBancariaContaSalario() {
		return cdContaBancariaContaSalario;
	}

	/**
	 * @param cdContaBancariaContaSalario the cdContaBancariaContaSalario to set
	 */
	public void setCdContaBancariaContaSalario(Long cdContaBancariaContaSalario) {
		this.cdContaBancariaContaSalario = cdContaBancariaContaSalario;
	}

	public String getCdBancoPagamento() {
		return cdBancoPagamento;
	}

	public void setCdBancoPagamento(String cdBancoPagamento) {
		this.cdBancoPagamento = cdBancoPagamento;
	}

	public String getCdContaPagamento() {
		return cdContaPagamento;
	}

	public void setCdContaPagamento(String cdContaPagamento) {
		this.cdContaPagamento = cdContaPagamento;
	}

	public String getIspb() {
		return ispb;
	}

	public void setIspb(String ispb) {
		this.ispb = ispb;
	}

	public boolean isChkHabilitaCredito() {
		return chkHabilitaCredito;
	}

	public void setChkHabilitaCredito(boolean chkHabilitaCredito) {
		this.chkHabilitaCredito = chkHabilitaCredito;
	}

	public boolean isChkHabilitaSalario() {
		return chkHabilitaSalario;
	}

	public void setChkHabilitaSalario(boolean chkHabilitaSalario) {
		this.chkHabilitaSalario = chkHabilitaSalario;
	}

	public boolean isChkHabilitaPagamento() {
		return chkHabilitaPagamento;
	}

	public void setChkHabilitaPagamento(boolean chkHabilitaPagamento) {
		this.chkHabilitaPagamento = chkHabilitaPagamento;
	}

}
