/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaofavorecido.mantercadcontadestino
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaofavorecido.mantercadcontadestino;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.AlterarCadContasDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.AlterarCadContasDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConVinculoContaSalarioDestEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConVinculoContaSalarioDestSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarDetHistContaSalarioDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarDetHistContaSalarioDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ExcluirVinculoContaSalarioDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ExcluirVinculoContaSalarioDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.IncluirCadContaDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.IncluirCadContaDestinoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListaVinculoContaSalarioDestEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListaVinculoContaSalarioDestSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListarHistVinculoContaSalarioDestinoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ListarHistVinculoContaSalarioDestinoSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * @author Rodrigo T
 * 
 */
public class ManterCadContaDestinoBean extends IdentificacaoClienteBean {

	/** Atributo CON_MANTER_CADASTRO_SALARIO_CONTA_DESTINO. */
	private static final String CON_MANTER_CADASTRO_SALARIO_CONTA_DESTINO = "conManterCadastroContaSalarioContaDestino";

	/** Atributo INC_MANTER_CADASTRO_SALARIO_CONTA_DESTINO. */
	private static final String INC_MANTER_CADASTRO_SALARIO_CONTA_DESTINO = "incManterCadastroContaSalarioContaDestino";

	/** Atributo manterCadContaDestinoService. */
	private IManterCadContaDestinoService manterCadContaDestinoService;

	// Entrada
	/** Atributo entradaListaContaSalDestino. */
	private ListaVinculoContaSalarioDestEntradaDTO entradaListaContaSalDestino;

	/** Atributo entradaListaHistContaSalDestino. */
	private ListarHistVinculoContaSalarioDestinoEntradaDTO entradaListaHistContaSalDestino;

	/** Atributo entradaIncluirContaDestino. */
	private IncluirCadContaDestinoEntradaDTO entradaIncluirContaDestino;

	/** Atributo entradaAlterarContaDestino. */
	private AlterarCadContasDestinoEntradaDTO entradaAlterarContaDestino;

	/** Atributo entradaExcluirContaDestino. */
	private ExcluirVinculoContaSalarioDestinoEntradaDTO entradaExcluirContaDestino;

	// Saida
	/** Atributo saidaListaContaSalDestino. */
	private ListaVinculoContaSalarioDestSaidaDTO saidaListaContaSalDestino;

	/** Atributo saidaConVincContaSalarioDest. */
	private ConVinculoContaSalarioDestSaidaDTO saidaConVincContaSalarioDest;

	/** Atributo saidaListaHistContaSalarioDest. */
	private ListarHistVinculoContaSalarioDestinoSaidaDTO saidaListaHistContaSalarioDest;

	/** Atributo saidaConHistVincContaSalarioDest. */
	private ConsultarDetHistContaSalarioDestinoSaidaDTO saidaConHistVincContaSalarioDest;

	/** Atributo listaContaSalDestino. */
	private List<ListaVinculoContaSalarioDestSaidaDTO> listaContaSalDestino;

	/** Atributo listaHistoricoContaSalarioDest. */
	private List<ListarHistVinculoContaSalarioDestinoSaidaDTO> listaHistoricoContaSalarioDest;

	/** Atributo listaTipoConta. */
	private List<ListarTipoContaSaidaDTO> listaTipoConta;

	/** Atributo bancoSalarioHistorico. */
	private Integer bancoSalarioHistorico;

	/** Atributo agenciaSalarioHistorico. */
	private Integer agenciaSalarioHistorico;

	/** Atributo contaSalarioHistorico. */
	private Long contaSalarioHistorico;

	/** Atributo digitoContaSalarioHistorico. */
	private String digitoContaSalarioHistorico;

	/** Atributo disableArgumentosConsultaHistorico. */
	private boolean disableArgumentosConsultaHistorico;

	/** Atributo periodoInicialHistorico. */
	private Date periodoInicialHistorico;

	/** Atributo periodoFinalHistorico. */
	private Date periodoFinalHistorico;

	/** Atributo cdPesquisaLista. */
	private Integer cdPesquisaLista;

	/** Atributo itemSelecionadoLista. */
	private String itemSelecionadoLista;

	/** Atributo itemSelecionadoListaHist. */
	private String itemSelecionadoListaHist;

	/** Atributo itemSelecionadoFiltro. */
	private String itemSelecionadoFiltro;

	/** Atributo bloqueaRadio. */
	private boolean bloqueaRadio;

	/** Atributo bloqueaTipoContrato. */
	private boolean bloqueaTipoContrato;

	/** Atributo habilitaFiltro. */
	private boolean habilitaFiltro;

	/**
	 * Manter cad conta destino bean.
	 */
	public ManterCadContaDestinoBean() {
		super(CON_MANTER_CADASTRO_SALARIO_CONTA_DESTINO,
				"identificaoCadastroContaSalarioDestCliente",
				"identificaoCadastroContaSalarioDestContrato");
	}

	/**
	 * Inicializar campos cad conta salario.
	 *
	 * @return the string
	 */
	public String inicializarCamposCadContaSalario() {

		this
				.setEntradaListaContaSalDestino(new ListaVinculoContaSalarioDestEntradaDTO());
		this.setItemFiltroSelecionado("");
		this
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this
				.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.setPreencherListaEmpresaGestora(new ArrayList<SelectItem>());
		this.setPreencherTipoContrato(new ArrayList<SelectItem>());
		this.setHabilitaFiltroDeContrato(true);
		this.setDesabilataFiltro(true);
		setContratoSelecionado(false);
		this.loadEmpresaGestora();
		this.loadTipoContrato();
		this.setItemSelecionadoLista("");
		this.setItemSelecionadoFiltro("");
		this.setDesabilataFiltro(true);
		this.setBloqueaRadio(false);
		this.setBloqueaTipoContrato(true);
		this.setBloqueaFiltro(true);
		this.setHabilitaFiltro(true);
		return CON_MANTER_CADASTRO_SALARIO_CONTA_DESTINO;
	}

	/**
	 * Is lista conta sal destino empty.
	 *
	 * @return true, if is lista conta sal destino empty
	 */
	public boolean isListaContaSalDestinoEmpty() {
		return this.listaContaSalDestino == null
				|| this.listaContaSalDestino.isEmpty();
	}

	/**
	 * Is lista hist conta sal destino empty.
	 *
	 * @return true, if is lista hist conta sal destino empty
	 */
	public boolean isListaHistContaSalDestinoEmpty() {
		return this.listaHistoricoContaSalarioDest == null
				|| this.listaHistoricoContaSalarioDest.isEmpty();
	}

	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar() {
		setItemSelecionadoLista("");
		setItemSelecionadoFiltro("");
		setBloqueaFiltro(false);
		setEntradaListaContaSalDestino(new ListaVinculoContaSalarioDestEntradaDTO());
		setHabilitaFiltro(true);
		listaContaSalDestino = new ArrayList<ListaVinculoContaSalarioDestSaidaDTO>();
		setHabilitaFiltroDeCadContaSalarioDet(false);
		return CON_MANTER_CADASTRO_SALARIO_CONTA_DESTINO;
	}

	/**
	 * Limpa campo radio.
	 *
	 * @return the string
	 */
	public String limpaCampoRadio() {
		this
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		return CON_MANTER_CADASTRO_SALARIO_CONTA_DESTINO;
	}

	/**
	 * Limpar campo filtro.
	 */
	public void limparCampoFiltro() {
		setEntradaListaContaSalDestino(new ListaVinculoContaSalarioDestEntradaDTO());
	}

	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos() {
		inicializarCamposCadContaSalario();
		setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		setListaContaSalDestino(new ArrayList<ListaVinculoContaSalarioDestSaidaDTO>());
		return CON_MANTER_CADASTRO_SALARIO_CONTA_DESTINO;
	}

	/**
	 * Consultar cad conta salario.
	 */
	public void consultarCadContaSalario() {
		setItemSelecionadoLista(null);
		this.entradaListaContaSalDestino.setCdPessoaJuridicaNegocio(this
				.getSaidaConsultarListaContratosPessoas()
				.getCdPessoaJuridicaContrato());
		this.entradaListaContaSalDestino.setCdTipoContratoNegocio(this
				.getSaidaConsultarListaContratosPessoas()
				.getCdTipoContratoNegocio());
		this.entradaListaContaSalDestino.setNrSequenciaContratoNegocio(this
				.getSaidaConsultarListaContratosPessoas()
				.getNrSeqContratoNegocio());
		this.entradaListaContaSalDestino.setNumeroOcorrencias(50);

		listaContaSalDestino = manterCadContaDestinoService
				.pesquisarCadContaSalarioDestino(this.entradaListaContaSalDestino);

		if (listaContaSalDestino != null) {
			setBloqueaFiltro(true);
			setHabilitaFiltro(false);
			setHabilitaFiltroDeCadContaSalarioDet(true);
		}
	}

	/**
	 * Listar conta sal destino.
	 *
	 * @param e the e
	 */
	public void listarContaSalDestino(ActionEvent e) {
		consultarCadContaSalario();
	}

	/**
	 * Detalhar cad conta salario.
	 *
	 * @return the string
	 */
	public String detalharCadContaSalario() {

		this
				.setSaidaConVincContaSalarioDest(new ConVinculoContaSalarioDestSaidaDTO());
		ConVinculoContaSalarioDestEntradaDTO entrada = new ConVinculoContaSalarioDestEntradaDTO();

		saidaListaContaSalDestino = listaContaSalDestino.get(Integer
				.parseInt(itemSelecionadoLista));

		entrada.setCdPessoaJuridNegocio(entradaListaContaSalDestino
				.getCdPessoaJuridicaNegocio());
		entrada.setCdPessoaJuridVinc(saidaListaContaSalDestino
				.getCdPessoaJuridVinc());
		entrada.setCdTipoContratoNegocio(entradaListaContaSalDestino
				.getCdTipoContratoNegocio());
		entrada.setCdTipoContratoVinc(saidaListaContaSalDestino
				.getCdTipoContratoVinc());
		entrada.setCdTipoVinculoContrato(saidaListaContaSalDestino
				.getCdTipoVinculoContrato());
		entrada.setNrSeqContratoNegocio(entradaListaContaSalDestino
				.getNrSequenciaContratoNegocio());
		entrada.setNrSeqContratoVinc(saidaListaContaSalDestino
				.getNrSeqContratoVinc());

		saidaConVincContaSalarioDest = manterCadContaDestinoService
				.detalharVinculoContaSalarioDest(entrada);

		return "detManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Incluir cad conta salario.
	 *
	 * @return the string
	 */
	public String incluirCadContaSalario() {
		setEntradaIncluirContaDestino(new IncluirCadContaDestinoEntradaDTO());
		listaTipoConta = this.getComboService().listarTipoConta();
		return INC_MANTER_CADASTRO_SALARIO_CONTA_DESTINO;
	}

	/**
	 * Avancar incluir cad conta salario.
	 *
	 * @return the string
	 */
	public String avancarIncluirCadContaSalario() {
		ConsultarBancoAgenciaEntradaDTO entradaConsultarBancoAgencia = new ConsultarBancoAgenciaEntradaDTO();

		for (ListarTipoContaSaidaDTO tipoConta : listaTipoConta) {
			if (tipoConta.getCdTipoConta().equals(
					entradaIncluirContaDestino.getCdTipoConta())) {
				entradaIncluirContaDestino.setDsTipoConta(tipoConta
						.getDsTipoConta());
			}
		}

		entradaConsultarBancoAgencia.setCdAgencia(entradaIncluirContaDestino
				.getCdAgencia());
		entradaConsultarBancoAgencia.setCdBanco(entradaIncluirContaDestino
				.getCdBanco());
		entradaConsultarBancoAgencia
				.setCdDigitoAgencia(entradaIncluirContaDestino
						.getCdDigitoAgencia());

		ConsultarBancoAgenciaSaidaDTO saidaConsultarBancoAgencia = manterCadContaDestinoService
				.consultarDescricaoBancoAgencia(entradaConsultarBancoAgencia);

		entradaIncluirContaDestino.setDsBancoDestino(saidaConsultarBancoAgencia
				.getDsBanco());
		entradaIncluirContaDestino
				.setDsAgenciaDestino(saidaConsultarBancoAgencia.getDsAgencia());

		entradaConsultarBancoAgencia.setCdAgencia(entradaIncluirContaDestino
				.getCdAgenciaSalario());
		entradaConsultarBancoAgencia.setCdBanco(entradaIncluirContaDestino
				.getCdBancoSalario());
		entradaConsultarBancoAgencia
				.setCdDigitoAgencia(entradaIncluirContaDestino
						.getCdDigitoAgenciaSalario());

		saidaConsultarBancoAgencia = manterCadContaDestinoService
				.consultarDescricaoBancoAgencia(entradaConsultarBancoAgencia);

		entradaIncluirContaDestino.setDsBancoSalario(saidaConsultarBancoAgencia
				.getDsBanco());
		entradaIncluirContaDestino
				.setDsAgenciaSalario(saidaConsultarBancoAgencia.getDsAgencia());

		entradaIncluirContaDestino
				.setDsContingenciaTed(entradaIncluirContaDestino
						.getCdContingenciaTed() == 2 ? "N�O" : "SIM");

		return "incConfManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Confirmar inclusao cad conta salario.
	 *
	 * @return the string
	 */
	public String confirmarInclusaoCadContaSalario() {

		IncluirCadContaDestinoSaidaDTO saidaIncluirCadContaDestino = new IncluirCadContaDestinoSaidaDTO();

		entradaIncluirContaDestino
				.setCdPessoaJuridNegocio(getSaidaConsultarListaContratosPessoas()
						.getCdPessoaJuridicaContrato());
		entradaIncluirContaDestino
				.setCdTipoContratoNegocio(getSaidaConsultarListaContratosPessoas()
						.getCdTipoContratoNegocio());
		entradaIncluirContaDestino.setCdTipoVinculoContrato(0);
		entradaIncluirContaDestino
				.setNrSeqContratoNegocio(getSaidaConsultarListaContratosPessoas()
						.getNrSeqContratoNegocio());

		try {
			saidaIncluirCadContaDestino = manterCadContaDestinoService
					.incluirVinculoContaSalarioDest(entradaIncluirContaDestino);
			BradescoFacesUtils.addInfoModalMessage("("
					+ saidaIncluirCadContaDestino.getCodMensagem() + ") "
					+ saidaIncluirCadContaDestino.getMensagem(),
					CON_MANTER_CADASTRO_SALARIO_CONTA_DESTINO,
					BradescoViewExceptionActionType.ACTION, false);
			consultarCadContaSalario();
		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					INC_MANTER_CADASTRO_SALARIO_CONTA_DESTINO,
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return INC_MANTER_CADASTRO_SALARIO_CONTA_DESTINO;
	}

	/**
	 * Alterar cad conta salario.
	 *
	 * @return the string
	 */
	public String alterarCadContaSalario() {
		entradaAlterarContaDestino = new AlterarCadContasDestinoEntradaDTO();
		listaTipoConta = this.getComboService().listarTipoConta();
		saidaListaContaSalDestino = listaContaSalDestino.get(Integer
				.parseInt(itemSelecionadoLista));

		entradaAlterarContaDestino
				.setCdPessoaJuridicaNegocio(entradaListaContaSalDestino
						.getCdPessoaJuridicaNegocio());
		entradaAlterarContaDestino
				.setCdPessoaJuridicaVinculo(saidaListaContaSalDestino
						.getCdPessoaJuridVinc());
		entradaAlterarContaDestino
				.setCdTipoContratoNegocio(entradaListaContaSalDestino
						.getCdTipoContratoNegocio());
		entradaAlterarContaDestino
				.setCdTipoContratoVinculo(saidaListaContaSalDestino
						.getCdTipoContratoVinc());
		entradaAlterarContaDestino
				.setCdTipoVinculoContrato(saidaListaContaSalDestino
						.getCdTipoVinculoContrato());
		entradaAlterarContaDestino
				.setNrSequenciaContratoNegocio(entradaListaContaSalDestino
						.getNrSequenciaContratoNegocio());
		entradaAlterarContaDestino
				.setNrSequenciaContratoVinculo(saidaListaContaSalDestino
						.getNrSeqContratoVinc());

		return "altManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {
		ConsultarBancoAgenciaEntradaDTO entradaConsultarBancoAgencia = new ConsultarBancoAgenciaEntradaDTO();

		for (ListarTipoContaSaidaDTO tipoConta : listaTipoConta) {
			if (tipoConta.getCdTipoConta().equals(
					entradaAlterarContaDestino.getCdTipoConta())) {
				entradaAlterarContaDestino.setDsTipoConta(tipoConta
						.getDsTipoConta());
			}
		}

		entradaConsultarBancoAgencia.setCdAgencia(entradaAlterarContaDestino
				.getCdAgencia());
		entradaConsultarBancoAgencia.setCdBanco(entradaAlterarContaDestino
				.getCdBanco());
		entradaConsultarBancoAgencia
				.setCdDigitoAgencia(entradaAlterarContaDestino
						.getCdDigitoAgencia());

		try {
			ConsultarBancoAgenciaSaidaDTO saidaConsultarBancoAgencia = manterCadContaDestinoService
					.consultarDescricaoBancoAgencia(entradaConsultarBancoAgencia);
			entradaAlterarContaDestino.setDsBanco(saidaConsultarBancoAgencia
					.getDsBanco());
			entradaAlterarContaDestino.setDsAgencia(saidaConsultarBancoAgencia
					.getDsAgencia());
		} catch (PdcAdapterException p) {
			entradaAlterarContaDestino.setDsBanco("");
			entradaAlterarContaDestino.setDsAgencia("");
		}

		entradaAlterarContaDestino
				.setDsContingenciaTed(entradaAlterarContaDestino
						.getCdContingenciaTed() == 2 ? "N�O" : "SIM");

		return "altConfManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Confirmar alteracao.
	 *
	 * @return the string
	 */
	public String confirmarAlteracao() {

		AlterarCadContasDestinoSaidaDTO saidaAlterarContaDestino = new AlterarCadContasDestinoSaidaDTO();

		try {
			saidaAlterarContaDestino = manterCadContaDestinoService
					.alterarCadastroContaDestino(entradaAlterarContaDestino);
			BradescoFacesUtils.addInfoModalMessage("("
					+ saidaAlterarContaDestino.getCodMensagem() + ") "
					+ saidaAlterarContaDestino.getMensagem(),
					CON_MANTER_CADASTRO_SALARIO_CONTA_DESTINO,
					BradescoViewExceptionActionType.ACTION, false);
			consultarCadContaSalario();
		} catch (PdcAdapterException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					"altManterCadastroContaSalarioContaDestino",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "altConfManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Excluir cad conta salario.
	 *
	 * @return the string
	 */
	public String excluirCadContaSalario() {

		detalharCadContaSalario();

		saidaListaContaSalDestino = listaContaSalDestino.get(Integer
				.parseInt(itemSelecionadoLista));
		entradaExcluirContaDestino = new ExcluirVinculoContaSalarioDestinoEntradaDTO();

		entradaExcluirContaDestino
				.setCdPessoaJuridicaNegocio(entradaListaContaSalDestino
						.getCdPessoaJuridicaNegocio());
		entradaExcluirContaDestino
				.setCdPessoaJuridicaVinculo(saidaListaContaSalDestino
						.getCdPessoaJuridVinc());
		entradaExcluirContaDestino
				.setCdTipoContratoNegocio(entradaListaContaSalDestino
						.getCdTipoContratoNegocio());
		entradaExcluirContaDestino
				.setCdTipoContratoVinculo(saidaListaContaSalDestino
						.getCdTipoContratoVinc());
		entradaExcluirContaDestino
				.setCdTipoVinculoContrato(saidaListaContaSalDestino
						.getCdTipoVinculoContrato());
		entradaExcluirContaDestino
				.setNrSequenciaContratoNegocio(entradaListaContaSalDestino
						.getNrSequenciaContratoNegocio());
		entradaExcluirContaDestino
				.setNrSequenciaContratoVinculo(saidaListaContaSalDestino
						.getNrSeqContratoVinc());

		return "excManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Confirmar exclusao cad conta salario.
	 *
	 * @return the string
	 */
	public String confirmarExclusaoCadContaSalario() {

		ExcluirVinculoContaSalarioDestinoSaidaDTO saidaExcluirVincContaSalarioDest = new ExcluirVinculoContaSalarioDestinoSaidaDTO();

		try {
			saidaExcluirVincContaSalarioDest = manterCadContaDestinoService
					.excluirVincContaSalarioDestino(entradaExcluirContaDestino);
			BradescoFacesUtils.addInfoModalMessage("("
					+ saidaExcluirVincContaSalarioDest.getCodMensagem() + ") "
					+ saidaExcluirVincContaSalarioDest.getMensagem(),
					CON_MANTER_CADASTRO_SALARIO_CONTA_DESTINO,
					BradescoViewExceptionActionType.ACTION, false);
			consultarCadContaSalario();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					"excManterCadastroContaSalarioContaDestino",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "excManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Limpar pagina historico.
	 */
	public void limparPaginaHistorico() {
		setDisableArgumentosConsultaHistorico(false);

		setCdPesquisaLista(2);

		setListaHistoricoContaSalarioDest(null);
		setItemSelecionadoListaHist(null);

		setBancoSalarioHistorico(null);
		setAgenciaSalarioHistorico(null);
		setContaSalarioHistorico(null);
		setDigitoContaSalarioHistorico(null);
		setPeriodoInicialHistorico(new Date());
		setPeriodoFinalHistorico(new Date());
	}

	/**
	 * Abrir consultar historico.
	 *
	 * @return the string
	 */
	public String abrirConsultarHistorico() {
		try {
			setSaidaListaContaSalDestino(new ListaVinculoContaSalarioDestSaidaDTO());
			limparPaginaHistorico();

			// SE O USU�RIO TIVER SELECIONADO UM ITEM ENT�O CONSULTA � REALIZADA
			// COM FILTROS SELECIONADOS
			if (getItemSelecionadoLista() != null) {
				setSaidaListaContaSalDestino(getListaContaSalDestino().get(
						Integer.parseInt(getItemSelecionadoLista())));

				setCdPesquisaLista(1);
				setBancoSalarioHistorico(getSaidaListaContaSalDestino()
						.getCdBancoSalario());
				setAgenciaSalarioHistorico(getSaidaListaContaSalDestino()
						.getCdAgenciaSalario());
				setContaSalarioHistorico(getSaidaListaContaSalDestino()
						.getCdContaSalario());
				setDigitoContaSalarioHistorico(getSaidaListaContaSalDestino()
						.getCdDigitoContaSalario());
				setPeriodoInicialHistorico(new Date());
				setPeriodoFinalHistorico(new Date());
				pesquisarHistorico();
			}
		} catch (PdcAdapterFunctionalException p) {
			setCdPesquisaLista(2);
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}

		return "hisManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Pesquisar historico.
	 */
	private void pesquisarHistorico() {
		entradaListaHistContaSalDestino = new ListarHistVinculoContaSalarioDestinoEntradaDTO();

		entradaListaHistContaSalDestino
				.setCdPessoaJuridNegocio(getSaidaConsultarListaContratosPessoas()
						.getCdPessoaJuridicaContrato());
		entradaListaHistContaSalDestino
				.setCdTipoContratoNegocio(getSaidaConsultarListaContratosPessoas()
						.getCdTipoContratoNegocio());
		entradaListaHistContaSalDestino
				.setNrSeqContratoNegocio(getSaidaConsultarListaContratosPessoas()
						.getNrSeqContratoNegocio());

		if (getCdPesquisaLista() == 1) {
			entradaListaHistContaSalDestino
					.setCdPessoaJuridVinc(getSaidaListaContaSalDestino()
							.getCdPessoaJuridVinc());
			entradaListaHistContaSalDestino
					.setCdTipoContratoVinc(getSaidaListaContaSalDestino()
							.getCdTipoContratoVinc());
			entradaListaHistContaSalDestino
					.setCdTipoVinculoContrato(getSaidaListaContaSalDestino()
							.getCdTipoVinculoContrato());
			entradaListaHistContaSalDestino
					.setNrSeqContratoVinc(getSaidaListaContaSalDestino()
							.getNrSeqContratoVinc());
		} else {
			if (getCdPesquisaLista() == 2) {
				entradaListaHistContaSalDestino.setCdPessoaJuridVinc(0l);
				entradaListaHistContaSalDestino.setCdTipoContratoVinc(0);
				entradaListaHistContaSalDestino.setCdTipoVinculoContrato(0);
				entradaListaHistContaSalDestino.setNrSeqContratoVinc(0l);
			}
		}

		entradaListaHistContaSalDestino.setCdBanco(getBancoSalarioHistorico());
		entradaListaHistContaSalDestino
				.setCdAgencia(getAgenciaSalarioHistorico());
		entradaListaHistContaSalDestino.setCdConta(getContaSalarioHistorico());
		entradaListaHistContaSalDestino
				.setCdDigitoConta(getDigitoContaSalarioHistorico());
		entradaListaHistContaSalDestino.setDataInicio(FormatarData
				.formataDiaMesAno(getPeriodoInicialHistorico()));
		entradaListaHistContaSalDestino.setDataFim(FormatarData
				.formataDiaMesAno(getPeriodoFinalHistorico()));
		entradaListaHistContaSalDestino
				.setCdPesquisaLista(getCdPesquisaLista());

		setListaHistoricoContaSalarioDest(manterCadContaDestinoService
				.consultarHistoricoContaSalarioDest(entradaListaHistContaSalDestino));
		setDisableArgumentosConsultaHistorico(true);
	}

	/**
	 * Consultar historico.
	 *
	 * @return the string
	 */
	public String consultarHistorico() {
		try {
			pesquisarHistorico();
		} catch (PdcAdapterFunctionalException p) {
			setCdPesquisaLista(2);
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "";
	}

	/**
	 * Detalhe historico.
	 *
	 * @return the string
	 */
	public String detalheHistorico() {

		saidaListaHistContaSalarioDest = listaHistoricoContaSalarioDest
				.get(Integer.parseInt(itemSelecionadoListaHist));
		ConsultarDetHistContaSalarioDestinoEntradaDTO entradaDetHistorico = new ConsultarDetHistContaSalarioDestinoEntradaDTO();

		entradaDetHistorico
				.setCdPessoaJuridNegocio(entradaListaHistContaSalDestino
						.getCdPessoaJuridNegocio());
		entradaDetHistorico.setCdPessoaJuridVinc(saidaListaHistContaSalarioDest
				.getCdPessoaContratoVinculado());
		entradaDetHistorico
				.setCdTipoContratoNegocio(entradaListaHistContaSalDestino
						.getCdTipoContratoNegocio());
		entradaDetHistorico
				.setCdTipoContratoVinc(saidaListaHistContaSalarioDest
						.getCdTipoVincContrato());
		entradaDetHistorico.setCdTipoVincContrato(6);
		entradaDetHistorico
				.setHrInclusaoRegistro(saidaListaHistContaSalarioDest
						.getHrInclusaoManutencao());
		entradaDetHistorico
				.setNrSeqContratoNegocio(entradaListaHistContaSalDestino
						.getNrSeqContratoNegocio());
		entradaDetHistorico.setNrSeqContratoVinc(saidaListaHistContaSalarioDest
				.getNrSequenciaVinculacaoContrato());

		saidaConHistVincContaSalarioDest = manterCadContaDestinoService
				.detalharHistVinculoContaSalarioDest(entradaDetHistorico);

		return "hisConfManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Voltar pagina cad conta salario.
	 *
	 * @return the string
	 */
	public String voltarPaginaCadContaSalario() {
		setItemSelecionadoLista(null);
		return CON_MANTER_CADASTRO_SALARIO_CONTA_DESTINO;
	}

	/**
	 * Voltar alteracao.
	 *
	 * @return the string
	 */
	public String voltarAlteracao() {
		return "altManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Voltar pagina incluir cad conta salario.
	 *
	 * @return the string
	 */
	public String voltarPaginaIncluirCadContaSalario() {
		return INC_MANTER_CADASTRO_SALARIO_CONTA_DESTINO;
	}

	/**
	 * Voltar historico.
	 *
	 * @return the string
	 */
	public String voltarHistorico() {
		setItemSelecionadoListaHist(null);
		return "hisManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Confirmar incluir cad conta salario.
	 *
	 * @return the string
	 */
	public String confirmarIncluirCadContaSalario() {
		return "incConfManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Confirmar alterar cad conta salario.
	 *
	 * @return the string
	 */
	public String confirmarAlterarCadContaSalario() {
		return "altConfManterCadastroContaSalarioContaDestino";
	}

	/**
	 * Get: selectItemManterCadContaDestino.
	 *
	 * @return selectItemManterCadContaDestino
	 */
	public List<SelectItem> getSelectItemManterCadContaDestino() {

		if (this.listaContaSalDestino == null
				|| this.listaContaSalDestino.isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.listaContaSalDestino.size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/**
	 * Get: selectItemHistContaSalarioDest.
	 *
	 * @return selectItemHistContaSalarioDest
	 */
	public List<SelectItem> getSelectItemHistContaSalarioDest() {
		if (this.listaHistoricoContaSalarioDest == null
				|| this.listaHistoricoContaSalarioDest.isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.listaHistoricoContaSalarioDest
					.size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}
			return list;
		}

	}

	/**
	 * Get: entradaListaContaSalDestino.
	 *
	 * @return entradaListaContaSalDestino
	 */
	public ListaVinculoContaSalarioDestEntradaDTO getEntradaListaContaSalDestino() {
		return entradaListaContaSalDestino;
	}

	/**
	 * Set: entradaListaContaSalDestino.
	 *
	 * @param entradaListaContaSalDestino the entrada lista conta sal destino
	 */
	public void setEntradaListaContaSalDestino(
			ListaVinculoContaSalarioDestEntradaDTO entradaListaContaSalDestino) {
		this.entradaListaContaSalDestino = entradaListaContaSalDestino;
	}

	/**
	 * Get: listaContaSalDestino.
	 *
	 * @return listaContaSalDestino
	 */
	public List<ListaVinculoContaSalarioDestSaidaDTO> getListaContaSalDestino() {
		return listaContaSalDestino;
	}

	/**
	 * Set: listaContaSalDestino.
	 *
	 * @param listaContaSalDestino the lista conta sal destino
	 */
	public void setListaContaSalDestino(
			List<ListaVinculoContaSalarioDestSaidaDTO> listaContaSalDestino) {
		this.listaContaSalDestino = listaContaSalDestino;
	}

	/**
	 * Get: manterCadContaDestinoService.
	 *
	 * @return manterCadContaDestinoService
	 */
	public IManterCadContaDestinoService getManterCadContaDestinoService() {
		return manterCadContaDestinoService;
	}

	/**
	 * Set: manterCadContaDestinoService.
	 *
	 * @param manterCadContaDestinoService the manter cad conta destino service
	 */
	public void setManterCadContaDestinoService(
			IManterCadContaDestinoService manterCadContaDestinoService) {
		this.manterCadContaDestinoService = manterCadContaDestinoService;
	}

	/**
	 * Get: saidaListaContaSalDestino.
	 *
	 * @return saidaListaContaSalDestino
	 */
	public ListaVinculoContaSalarioDestSaidaDTO getSaidaListaContaSalDestino() {
		return saidaListaContaSalDestino;
	}

	/**
	 * Set: saidaListaContaSalDestino.
	 *
	 * @param saidaListaContaSalDestino the saida lista conta sal destino
	 */
	public void setSaidaListaContaSalDestino(
			ListaVinculoContaSalarioDestSaidaDTO saidaListaContaSalDestino) {
		this.saidaListaContaSalDestino = saidaListaContaSalDestino;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public String getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(String itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: saidaConVincContaSalarioDest.
	 *
	 * @return saidaConVincContaSalarioDest
	 */
	public ConVinculoContaSalarioDestSaidaDTO getSaidaConVincContaSalarioDest() {
		return saidaConVincContaSalarioDest;
	}

	/**
	 * Get: itemSelecionadoFiltro.
	 *
	 * @return itemSelecionadoFiltro
	 */
	public String getItemSelecionadoFiltro() {
		return itemSelecionadoFiltro;
	}

	/**
	 * Set: itemSelecionadoFiltro.
	 *
	 * @param itemSelecionadoFiltro the item selecionado filtro
	 */
	public void setItemSelecionadoFiltro(String itemSelecionadoFiltro) {
		this.itemSelecionadoFiltro = itemSelecionadoFiltro == null ? ""
				: itemSelecionadoFiltro;
		if (!this.itemSelecionadoFiltro.trim().equals("")
				&& this.itemSelecionadoFiltro != null) {
			setHabilitaFiltro(false);
		}
	}

	/**
	 * Set: saidaConVincContaSalarioDest.
	 *
	 * @param saidaConVincContaSalarioDest the saida con vinc conta salario dest
	 */
	public void setSaidaConVincContaSalarioDest(
			ConVinculoContaSalarioDestSaidaDTO saidaConVincContaSalarioDest) {
		this.saidaConVincContaSalarioDest = saidaConVincContaSalarioDest;
	}

	/**
	 * Get: saidaListaHistContaSalarioDest.
	 *
	 * @return saidaListaHistContaSalarioDest
	 */
	public ListarHistVinculoContaSalarioDestinoSaidaDTO getSaidaListaHistContaSalarioDest() {
		return saidaListaHistContaSalarioDest;
	}

	/**
	 * Set: saidaListaHistContaSalarioDest.
	 *
	 * @param saidaListaHistContaSalarioDest the saida lista hist conta salario dest
	 */
	public void setSaidaListaHistContaSalarioDest(
			ListarHistVinculoContaSalarioDestinoSaidaDTO saidaListaHistContaSalarioDest) {
		this.saidaListaHistContaSalarioDest = saidaListaHistContaSalarioDest;
	}

	/**
	 * Get: listaHistoricoContaSalarioDest.
	 *
	 * @return listaHistoricoContaSalarioDest
	 */
	public List<ListarHistVinculoContaSalarioDestinoSaidaDTO> getListaHistoricoContaSalarioDest() {
		return listaHistoricoContaSalarioDest;
	}

	/**
	 * Set: listaHistoricoContaSalarioDest.
	 *
	 * @param listaHistoricoContaSalarioDest the lista historico conta salario dest
	 */
	public void setListaHistoricoContaSalarioDest(
			List<ListarHistVinculoContaSalarioDestinoSaidaDTO> listaHistoricoContaSalarioDest) {
		this.listaHistoricoContaSalarioDest = listaHistoricoContaSalarioDest;
	}

	/**
	 * Get: entradaIncluirContaDestino.
	 *
	 * @return entradaIncluirContaDestino
	 */
	public IncluirCadContaDestinoEntradaDTO getEntradaIncluirContaDestino() {
		return entradaIncluirContaDestino;
	}

	/**
	 * Set: entradaIncluirContaDestino.
	 *
	 * @param entradaIncluirContaDestino the entrada incluir conta destino
	 */
	public void setEntradaIncluirContaDestino(
			IncluirCadContaDestinoEntradaDTO entradaIncluirContaDestino) {
		this.entradaIncluirContaDestino = entradaIncluirContaDestino;
	}

	/**
	 * Get: entradaAlterarContaDestino.
	 *
	 * @return entradaAlterarContaDestino
	 */
	public AlterarCadContasDestinoEntradaDTO getEntradaAlterarContaDestino() {
		return entradaAlterarContaDestino;
	}

	/**
	 * Set: entradaAlterarContaDestino.
	 *
	 * @param entradaAlterarContaDestino the entrada alterar conta destino
	 */
	public void setEntradaAlterarContaDestino(
			AlterarCadContasDestinoEntradaDTO entradaAlterarContaDestino) {
		this.entradaAlterarContaDestino = entradaAlterarContaDestino;
	}

	/**
	 * Get: entradaExcluirContaDestino.
	 *
	 * @return entradaExcluirContaDestino
	 */
	public ExcluirVinculoContaSalarioDestinoEntradaDTO getEntradaExcluirContaDestino() {
		return entradaExcluirContaDestino;
	}

	/**
	 * Set: entradaExcluirContaDestino.
	 *
	 * @param entradaExcluirContaDestino the entrada excluir conta destino
	 */
	public void setEntradaExcluirContaDestino(
			ExcluirVinculoContaSalarioDestinoEntradaDTO entradaExcluirContaDestino) {
		this.entradaExcluirContaDestino = entradaExcluirContaDestino;
	}

	/**
	 * Get: saidaConHistVincContaSalarioDest.
	 *
	 * @return saidaConHistVincContaSalarioDest
	 */
	public ConsultarDetHistContaSalarioDestinoSaidaDTO getSaidaConHistVincContaSalarioDest() {
		return saidaConHistVincContaSalarioDest;
	}

	/**
	 * Set: saidaConHistVincContaSalarioDest.
	 *
	 * @param saidaConHistVincContaSalarioDest the saida con hist vinc conta salario dest
	 */
	public void setSaidaConHistVincContaSalarioDest(
			ConsultarDetHistContaSalarioDestinoSaidaDTO saidaConHistVincContaSalarioDest) {
		this.saidaConHistVincContaSalarioDest = saidaConHistVincContaSalarioDest;
	}

	/**
	 * Get: itemSelecionadoListaHist.
	 *
	 * @return itemSelecionadoListaHist
	 */
	public String getItemSelecionadoListaHist() {
		return itemSelecionadoListaHist;
	}

	/**
	 * Set: itemSelecionadoListaHist.
	 *
	 * @param itemSelecionadoListaHist the item selecionado lista hist
	 */
	public void setItemSelecionadoListaHist(String itemSelecionadoListaHist) {
		this.itemSelecionadoListaHist = itemSelecionadoListaHist;
	}

	/**
	 * Get: entradaListaHistContaSalDestino.
	 *
	 * @return entradaListaHistContaSalDestino
	 */
	public ListarHistVinculoContaSalarioDestinoEntradaDTO getEntradaListaHistContaSalDestino() {
		return entradaListaHistContaSalDestino;
	}

	/**
	 * Set: entradaListaHistContaSalDestino.
	 *
	 * @param entradaListaHistContaSalDestino the entrada lista hist conta sal destino
	 */
	public void setEntradaListaHistContaSalDestino(
			ListarHistVinculoContaSalarioDestinoEntradaDTO entradaListaHistContaSalDestino) {
		this.entradaListaHistContaSalDestino = entradaListaHistContaSalDestino;
	}

	/**
	 * Get: listaTipoConta.
	 *
	 * @return listaTipoConta
	 */
	public List<ListarTipoContaSaidaDTO> getListaTipoConta() {
		return listaTipoConta;
	}

	/**
	 * Set: listaTipoConta.
	 *
	 * @param listaTipoConta the lista tipo conta
	 */
	public void setListaTipoConta(List<ListarTipoContaSaidaDTO> listaTipoConta) {
		this.listaTipoConta = listaTipoConta;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean#isBloqueaRadio()
	 */
	public boolean isBloqueaRadio() {
		return bloqueaRadio;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean#setBloqueaRadio(boolean)
	 */
	public void setBloqueaRadio(boolean bloqueaRadio) {
		this.bloqueaRadio = bloqueaRadio;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean#isBloqueaTipoContrato()
	 */
	public boolean isBloqueaTipoContrato() {
		return bloqueaTipoContrato;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean#setBloqueaTipoContrato(boolean)
	 */
	public void setBloqueaTipoContrato(boolean bloqueaTipoContrato) {
		this.bloqueaTipoContrato = bloqueaTipoContrato;
	}

	/**
	 * Is habilita filtro.
	 *
	 * @return true, if is habilita filtro
	 */
	public boolean isHabilitaFiltro() {
		return habilitaFiltro;
	}

	/**
	 * Set: habilitaFiltro.
	 *
	 * @param habilitaFiltro the habilita filtro
	 */
	public void setHabilitaFiltro(boolean habilitaFiltro) {
		this.habilitaFiltro = habilitaFiltro;
	}

	/**
	 * Get: agenciaSalarioHistorico.
	 *
	 * @return agenciaSalarioHistorico
	 */
	public Integer getAgenciaSalarioHistorico() {
		return agenciaSalarioHistorico;
	}

	/**
	 * Set: agenciaSalarioHistorico.
	 *
	 * @param agenciaSalarioHistorico the agencia salario historico
	 */
	public void setAgenciaSalarioHistorico(Integer agenciaSalarioHistorico) {
		this.agenciaSalarioHistorico = agenciaSalarioHistorico;
	}

	/**
	 * Get: bancoSalarioHistorico.
	 *
	 * @return bancoSalarioHistorico
	 */
	public Integer getBancoSalarioHistorico() {
		return bancoSalarioHistorico;
	}

	/**
	 * Set: bancoSalarioHistorico.
	 *
	 * @param bancoSalarioHistorico the banco salario historico
	 */
	public void setBancoSalarioHistorico(Integer bancoSalarioHistorico) {
		this.bancoSalarioHistorico = bancoSalarioHistorico;
	}

	/**
	 * Get: cdPesquisaLista.
	 *
	 * @return cdPesquisaLista
	 */
	public Integer getCdPesquisaLista() {
		return cdPesquisaLista;
	}

	/**
	 * Set: cdPesquisaLista.
	 *
	 * @param cdPesquisaLista the cd pesquisa lista
	 */
	public void setCdPesquisaLista(Integer cdPesquisaLista) {
		this.cdPesquisaLista = cdPesquisaLista;
	}

	/**
	 * Get: contaSalarioHistorico.
	 *
	 * @return contaSalarioHistorico
	 */
	public Long getContaSalarioHistorico() {
		return contaSalarioHistorico;
	}

	/**
	 * Set: contaSalarioHistorico.
	 *
	 * @param contaSalarioHistorico the conta salario historico
	 */
	public void setContaSalarioHistorico(Long contaSalarioHistorico) {
		this.contaSalarioHistorico = contaSalarioHistorico;
	}

	/**
	 * Get: digitoContaSalarioHistorico.
	 *
	 * @return digitoContaSalarioHistorico
	 */
	public String getDigitoContaSalarioHistorico() {
		return digitoContaSalarioHistorico;
	}

	/**
	 * Set: digitoContaSalarioHistorico.
	 *
	 * @param digitoContaSalarioHistorico the digito conta salario historico
	 */
	public void setDigitoContaSalarioHistorico(
			String digitoContaSalarioHistorico) {
		this.digitoContaSalarioHistorico = digitoContaSalarioHistorico;
	}

	/**
	 * Is disable argumentos consulta historico.
	 *
	 * @return true, if is disable argumentos consulta historico
	 */
	public boolean isDisableArgumentosConsultaHistorico() {
		return disableArgumentosConsultaHistorico;
	}

	/**
	 * Set: disableArgumentosConsultaHistorico.
	 *
	 * @param disableArgumentosConsultaHistorico the disable argumentos consulta historico
	 */
	public void setDisableArgumentosConsultaHistorico(
			boolean disableArgumentosConsultaHistorico) {
		this.disableArgumentosConsultaHistorico = disableArgumentosConsultaHistorico;
	}

	/**
	 * Get: periodoFinalHistorico.
	 *
	 * @return periodoFinalHistorico
	 */
	public Date getPeriodoFinalHistorico() {
		return periodoFinalHistorico;
	}

	/**
	 * Set: periodoFinalHistorico.
	 *
	 * @param periodoFinalHistorico the periodo final historico
	 */
	public void setPeriodoFinalHistorico(Date periodoFinalHistorico) {
		this.periodoFinalHistorico = periodoFinalHistorico;
	}

	/**
	 * Get: periodoInicialHistorico.
	 *
	 * @return periodoInicialHistorico
	 */
	public Date getPeriodoInicialHistorico() {
		return periodoInicialHistorico;
	}

	/**
	 * Set: periodoInicialHistorico.
	 *
	 * @param periodoInicialHistorico the periodo inicial historico
	 */
	public void setPeriodoInicialHistorico(Date periodoInicialHistorico) {
		this.periodoInicialHistorico = periodoInicialHistorico;
	}
}
