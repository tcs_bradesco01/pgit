/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantercomptiposervicoformalanc
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantercomptiposervicoformalanc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarFormaLancCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarFormaLiquidacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.IManterCompTipoServicoFormaLancService;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ConsultarServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ConsultarServicoLancamentoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.DetalharServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.DetalharServicoLancamentoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ExcluirServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.ExcluirServicoLancamentoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.IncluirServicoLancamentoCnabEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.bean.IncluirServicoLancamentoCnabSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ManterCompTipoServicoFormaLancBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterCompTipoServicoFormaLancBean {
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo manterCompTipoServicoFormaLancServiceImpl. */
	private IManterCompTipoServicoFormaLancService manterCompTipoServicoFormaLancServiceImpl;
	
	/** Atributo rdoPesquisa. */
	private String rdoPesquisa;
	
	/** Atributo tipoServicoCnabFiltro. */
	private Integer tipoServicoCnabFiltro;
	
	/** Atributo listaTipoServicoCnab. */
	private List<SelectItem> listaTipoServicoCnab = new ArrayList<SelectItem>();
	
	/** Atributo formaLancCnabFiltro. */
	private Integer formaLancCnabFiltro;
	
	/** Atributo listaFormaLancCnab. */
	private List<SelectItem> listaFormaLancCnab = new ArrayList<SelectItem>();
	
	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;
	
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();
	
	/** Atributo modalidadeServicoFiltro. */
	private Integer modalidadeServicoFiltro;
	
	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico = new ArrayList<SelectItem>();
	
	/** Atributo formaLiquidacaoFiltro. */
	private Integer formaLiquidacaoFiltro;
	
	/** Atributo listaFormaLiquidacao. */
	private List<SelectItem> listaFormaLiquidacao = new ArrayList<SelectItem>();
	
	/** Atributo listaGridConsultar. */
	private List<ConsultarServicoLancamentoCnabSaidaDTO> listaGridConsultar;
	
	/** Atributo listaGridControle. */
	private List<SelectItem> listaGridControle;
	
	/** Atributo itemGridSelecionado. */
	private Integer itemGridSelecionado;
	
	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	/** Atributo tipoServicoCnab. */
	private Integer tipoServicoCnab;
	
	/** Atributo listaTipoServicoCnabHash. */
	private Map<Integer, String> listaTipoServicoCnabHash = new HashMap<Integer,String>();
	
	/** Atributo formaLancCnab. */
	private Integer formaLancCnab;
	
	/** Atributo listaFormaLancCnabHash. */
	private Map<Integer, String> listaFormaLancCnabHash = new HashMap<Integer, String>();
	
	/** Atributo tipoServico. */
	private Integer tipoServico;
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();
	
	/** Atributo modalidadeServico. */
	private Integer modalidadeServico;
	
	/** Atributo listaModalidadeServicoInc. */
	private List<SelectItem> listaModalidadeServicoInc = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, String> listaModalidadeServicoHash = new HashMap<Integer, String>();
	
	/** Atributo formaLiquidacao. */
	private Integer formaLiquidacao;
	
	/** Atributo listaFormaLiquidacaoHash. */
	private Map<Integer, String> listaFormaLiquidacaoHash = new HashMap<Integer, String>();
	
	/** Atributo dsTipoServicoCnab. */
	private String dsTipoServicoCnab;
	
	/** Atributo dsFormaLancCnab. */
	private String dsFormaLancCnab;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;
	
	/** Atributo dsFormaLiquidacao. */
	private String dsFormaLiquidacao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		limparCampos();
		listarComboServicoCnab();
		listarComboFormaLancCnab();
		listarComboListarServicos();
		listarComboListarFormaLiquidacao();		
	}
	
	/**
	 * Limpar radio pesquisa.
	 */
	public void limparRadioPesquisa(){
        	setTipoServicoCnabFiltro(null);
        	setFormaLancCnabFiltro(null);
        	setTipoServicoFiltro(null);
        	setModalidadeServicoFiltro(null);
        	setListaModalidadeServico(new ArrayList<SelectItem>());
        	setFormaLiquidacaoFiltro(null);
	}
	
	/**
	 * Listar combo servico cnab.
	 */
	public void listarComboServicoCnab(){
		try{
			listaTipoServicoCnab = new ArrayList<SelectItem>();
			
			List<ListarServicoCnabSaidaDTO> list = getComboService().listarServicoCnab();
			
			listaTipoServicoCnabHash.clear();
			for(ListarServicoCnabSaidaDTO combo : list){
				listaTipoServicoCnab.add(new SelectItem(combo.getCdTipoServicoCnab(),combo.getDsTipoServicoCnab()));
				listaTipoServicoCnabHash.put(combo.getCdTipoServicoCnab(), combo.getDsTipoServicoCnab());
			}
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			listaTipoServicoCnab = new ArrayList<SelectItem>();
		}
		
	}
	
	/**
	 * Listar combo forma lanc cnab.
	 */
	public void listarComboFormaLancCnab(){
		try{
			listaFormaLancCnab = new ArrayList<SelectItem>();
			
			List<ListarFormaLancCnabSaidaDTO> list = getComboService().listarFormaLancCnab();
			
			listaFormaLancCnabHash.clear();
			for(ListarFormaLancCnabSaidaDTO combo : list){
				listaFormaLancCnab.add(new SelectItem(combo.getCdFormaLancamentoCnab(),combo.getDsFormaLancamentoCnab()));
				listaFormaLancCnabHash.put(combo.getCdFormaLancamentoCnab(), combo.getDsFormaLancamentoCnab());
			}
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			listaFormaLancCnab = new ArrayList<SelectItem>();
		}
		
	}
	
	/**
	 * Listar combo listar servicos.
	 */
	public void listarComboListarServicos(){
		try{
			listaTipoServico = new ArrayList<SelectItem>();
			
			List<ListarServicosSaidaDTO> list = getComboService().listarTipoServicos(1);
			
			listaTipoServicoHash.clear();
			for(ListarServicosSaidaDTO combo : list){
				listaTipoServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
				listaTipoServicoHash.put(combo.getCdServico(), combo.getDsServico());
			}
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			listaTipoServico = new ArrayList<SelectItem>();
		}
		
	}
	
	
	
	/**
	 * Listar combo listar modalidades inc.
	 */
	public void listarComboListarModalidadesInc(){
		try{
			if(getTipoServico() != null && getTipoServico() != 0){
				listaModalidadeServicoInc = new ArrayList<SelectItem>();
				setModalidadeServicoFiltro(0);
				ListarModalidadesEntradaDTO entrada = new ListarModalidadesEntradaDTO();
				
				entrada.setCdServico(getTipoServico());			
				
				List<ListarModalidadeSaidaDTO> list = getComboService().listarModalidades(entrada);
				
				listaModalidadeServicoHash.clear();
				for(ListarModalidadeSaidaDTO combo : list){
					listaModalidadeServicoInc.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
					listaModalidadeServicoHash.put(combo.getCdModalidade(), combo.getDsModalidade());
				}
			}else{				
				listaModalidadeServicoInc = new ArrayList<SelectItem>();
				setModalidadeServico(0);
			}
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			listaModalidadeServicoInc = new ArrayList<SelectItem>();
		}
		
	}
	
	/**
	 * Listar combo listar modalidades.
	 */
	public void listarComboListarModalidades(){
		try{
			if(getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0){
				listaModalidadeServico = new ArrayList<SelectItem>();
				setModalidadeServicoFiltro(0);
				ListarModalidadesEntradaDTO entrada = new ListarModalidadesEntradaDTO();
				
				entrada.setCdServico(getTipoServicoFiltro());			
				
				List<ListarModalidadeSaidaDTO> list = getComboService().listarModalidades(entrada);
				
				
				for(ListarModalidadeSaidaDTO combo : list){
					listaModalidadeServico.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));					
				}
			}else{				
				listaModalidadeServico = new ArrayList<SelectItem>();
				setModalidadeServicoFiltro(0);
			}
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			listaModalidadeServico = new ArrayList<SelectItem>();
		}
		
	}
	
	/**
	 * Listar combo listar forma liquidacao.
	 */
	public void listarComboListarFormaLiquidacao(){
		try{
			listaFormaLiquidacao = new ArrayList<SelectItem>();
			
			List<ListarFormaLiquidacaoSaidaDTO> list = getComboService().listarFormaLiquidacao();
			
			listaFormaLiquidacaoHash.clear();
			for(ListarFormaLiquidacaoSaidaDTO combo : list){
				listaFormaLiquidacao.add(new SelectItem(combo.getCdFormaLiquidacao(),combo.getDsFormaLiquidacao()));
				listaFormaLiquidacaoHash.put(combo.getCdFormaLiquidacao(), combo.getDsFormaLiquidacao());
			}
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			listaFormaLiquidacao = new ArrayList<SelectItem>();
		}
		
	}
	
	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos(){
		setTipoServicoCnabFiltro(null);
		setFormaLancCnabFiltro(null);
		setTipoServicoFiltro(null);
		setModalidadeServicoFiltro(null);
		setListaModalidadeServico(new ArrayList<SelectItem>());
		setFormaLiquidacaoFiltro(null);
		setRdoPesquisa(null);
		
		setListaGridConsultar(null);
		setItemGridSelecionado(null);
		setHabilitaArgumentosPesquisa(true);
		
		return "";
	}
	
	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar(){
		try{
			listaGridConsultar = new ArrayList<ConsultarServicoLancamentoCnabSaidaDTO>();
			ConsultarServicoLancamentoCnabEntradaDTO entrada = new ConsultarServicoLancamentoCnabEntradaDTO();
			
			entrada.setCdFormaLancamentoCnab(getFormaLancCnabFiltro());
			entrada.setCdFormaLiquidacao(getFormaLiquidacaoFiltro());
			entrada.setCdProdutoOperacaoRelacionado(getModalidadeServicoFiltro());
			entrada.setCdProdutoServicoOperacao(getTipoServicoFiltro());
			entrada.setCdTipoServicoCnab(getTipoServicoCnabFiltro());
			
			setListaGridConsultar(getManterCompTipoServicoFormaLancServiceImpl().consultarServicoLancamentoCnab(entrada));
			
			listaGridControle = new ArrayList<SelectItem>();
			for(int i=0;i<getListaGridConsultar().size();i++){
				listaGridControle.add(new SelectItem(i,""));
			}
			
			setItemGridSelecionado(null);
			setHabilitaArgumentosPesquisa(false);
		}catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridConsultar(null);
			setItemGridSelecionado(null);
			setHabilitaArgumentosPesquisa(true);
		}
		return "";
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt){
		consultar();
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try{	
			limparVariaveis();
			ConsultarServicoLancamentoCnabSaidaDTO itemSelecionado = getListaGridConsultar().get(getItemGridSelecionado());	
			DetalharServicoLancamentoCnabEntradaDTO entrada = new DetalharServicoLancamentoCnabEntradaDTO();
			
			entrada.setCdFormaLancamentoCnab(itemSelecionado.getCdFormaLancamentoCnab());
			entrada.setCdTipoServicoCnab(itemSelecionado.getCdTipoServicoCnab());
			
			DetalharServicoLancamentoCnabSaidaDTO saida = getManterCompTipoServicoFormaLancServiceImpl().detalharServicoLancamentoCnab(entrada);
			
			
			setDsTipoServicoCnab(itemSelecionado.getDsTipoServicoCnab());
			setDsFormaLancCnab(itemSelecionado.getDsFormaLancamentoCnab());
			setDsTipoServico(itemSelecionado.getDsProdutoServicoOperacao());
			setDsModalidadeServico(itemSelecionado.getDsProdutoServicoRelacionado());
			setDsFormaLiquidacao(itemSelecionado.getDsFormaLiquidacao());
			
			setDataHoraManutencao(saida.getHrManutencaoRegistro());
			setUsuarioManutencao(saida.getCdUsuarioManutencao());
			setTipoCanalManutencao(PgitUtil.concatenarCampos(saida.getCdTipoCanalManutencao(), saida.getDsCanalManutencao()));
			setComplementoManutencao(saida.getCdOperacaoCanalManutencao() == null || saida.getCdOperacaoCanalManutencao().equals("0") ? "" : saida.getCdOperacaoCanalManutencao());
				
			setDataHoraInclusao(saida.getHrInclusaoRegistro());
			setUsuarioInclusao(saida.getCdUsuarioInclusao());
			setTipoCanalInclusao(PgitUtil.concatenarCampos(saida.getCdTipoCanalInclusao(), saida.getDsCanalInclusao()));
			setComplementoInclusao(saida.getCdOperacaoCanalInclusao() == null || saida.getCdOperacaoCanalInclusao().equals("0") ? "" : saida.getCdOperacaoCanalInclusao());
			
			return "DETALHAR";
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
		
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		setTipoServicoCnab(null);
		setFormaLancCnab(null);
		setTipoServico(null);
		setModalidadeServico(null);
		setFormaLiquidacao(null);
		setListaModalidadeServicoInc(new ArrayList<SelectItem>());
		return "INCLUIR";
	}
	
	/**
	 * Limpar variaveis.
	 */
	public void limparVariaveis(){
		setDsTipoServicoCnab(null);
		setDsFormaLancCnab(null);
		setDsTipoServico(null);
		setDsModalidadeServico(null);
		setDsFormaLiquidacao(null);
		
		setDataHoraManutencao(null);
		setUsuarioManutencao(null);
		setTipoCanalManutencao(null);
		setComplementoManutencao(null);
			
		setDataHoraInclusao(null);
		setUsuarioInclusao(null);
		setTipoCanalInclusao(null);
		setComplementoInclusao(null);
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		limparVariaveis();
		setDsTipoServicoCnab(getListaTipoServicoCnabHash().get(getTipoServicoCnab()));
		setDsFormaLancCnab(getListaFormaLancCnabHash().get(getFormaLancCnab()));
		setDsTipoServico(getListaTipoServicoHash().get(getTipoServico()));
		setDsModalidadeServico(getListaModalidadeServicoHash().get(getModalidadeServico()));
		setDsFormaLiquidacao(getListaFormaLiquidacaoHash().get(getFormaLiquidacao()));
		
		return "AVANCAR_INCLUIR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try{
			IncluirServicoLancamentoCnabEntradaDTO entrada = new IncluirServicoLancamentoCnabEntradaDTO();
			
			entrada.setCdFormaLancamentoCnab(getFormaLancCnab());
			entrada.setCdFormaLiquidacao(getFormaLiquidacao());
			entrada.setCdProdutoOperacaoRelacionado(getModalidadeServico());
			entrada.setCdProdutoServicoOperacao(getTipoServico());
			entrada.setCdTipoServicoCnab(getTipoServicoCnab());
			
			IncluirServicoLancamentoCnabSaidaDTO saida = getManterCompTipoServicoFormaLancServiceImpl().incluirServicoLancamentoCnab(entrada);
			
			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(), "conManterCompTipoServicoFormaLanc", BradescoViewExceptionActionType.ACTION, false);
			
			if(getListaGridConsultar() != null){
				consultar();
			}
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
		return "";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		limparVariaveis();
		
		ConsultarServicoLancamentoCnabSaidaDTO itemSelecionado = getListaGridConsultar().get(getItemGridSelecionado());	
		
		setDsTipoServicoCnab(itemSelecionado.getDsTipoServicoCnab());
		setDsFormaLancCnab(itemSelecionado.getDsFormaLancamentoCnab());
		setDsTipoServico(itemSelecionado.getDsProdutoServicoOperacao());
		setDsModalidadeServico(itemSelecionado.getDsProdutoServicoRelacionado());
		setDsFormaLiquidacao(itemSelecionado.getDsFormaLiquidacao());
		
		return "EXCLUIR";
	}
	
	/**
	 * Confirmar excluir.
	 */
	public void confirmarExcluir(){
		try{
			ConsultarServicoLancamentoCnabSaidaDTO itemSelecionado = getListaGridConsultar().get(getItemGridSelecionado());		
			ExcluirServicoLancamentoCnabEntradaDTO entrada = new ExcluirServicoLancamentoCnabEntradaDTO();
			
			entrada.setCdFormaLancamentoCnab(itemSelecionado.getCdFormaLancamentoCnab());
			entrada.setCdTipoServicoCnab(itemSelecionado.getCdTipoServicoCnab());
			
			ExcluirServicoLancamentoCnabSaidaDTO saida = getManterCompTipoServicoFormaLancServiceImpl().excluirServicoLancamentoCnab(entrada);
						
			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(), "conManterCompTipoServicoFormaLanc", BradescoViewExceptionActionType.ACTION, false);
			
			consultar();
		}catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		setItemGridSelecionado(null);
		return "VOLTAR";
	}
	
	//GET�s SET�s	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: manterCompTipoServicoFormaLancServiceImpl.
	 *
	 * @return manterCompTipoServicoFormaLancServiceImpl
	 */
	public IManterCompTipoServicoFormaLancService getManterCompTipoServicoFormaLancServiceImpl() {
		return manterCompTipoServicoFormaLancServiceImpl;
	}
	
	/**
	 * Set: manterCompTipoServicoFormaLancServiceImpl.
	 *
	 * @param manterCompTipoServicoFormaLancServiceImpl the manter comp tipo servico forma lanc service impl
	 */
	public void setManterCompTipoServicoFormaLancServiceImpl(
			IManterCompTipoServicoFormaLancService manterCompTipoServicoFormaLancServiceImpl) {
		this.manterCompTipoServicoFormaLancServiceImpl = manterCompTipoServicoFormaLancServiceImpl;
	}

	/**
	 * Get: formaLancCnabFiltro.
	 *
	 * @return formaLancCnabFiltro
	 */
	public Integer getFormaLancCnabFiltro() {
		return formaLancCnabFiltro;
	}

	/**
	 * Set: formaLancCnabFiltro.
	 *
	 * @param formaLancCnabFiltro the forma lanc cnab filtro
	 */
	public void setFormaLancCnabFiltro(Integer formaLancCnabFiltro) {
		this.formaLancCnabFiltro = formaLancCnabFiltro;
	}

	/**
	 * Get: listaFormaLancCnab.
	 *
	 * @return listaFormaLancCnab
	 */
	public List<SelectItem> getListaFormaLancCnab() {
		return listaFormaLancCnab;
	}

	/**
	 * Set: listaFormaLancCnab.
	 *
	 * @param listaFormaLancCnab the lista forma lanc cnab
	 */
	public void setListaFormaLancCnab(List<SelectItem> listaFormaLancCnab) {
		this.listaFormaLancCnab = listaFormaLancCnab;
	}

	/**
	 * Get: listaTipoServicoCnab.
	 *
	 * @return listaTipoServicoCnab
	 */
	public List<SelectItem> getListaTipoServicoCnab() {
		return listaTipoServicoCnab;
	}

	/**
	 * Set: listaTipoServicoCnab.
	 *
	 * @param listaTipoServicoCnab the lista tipo servico cnab
	 */
	public void setListaTipoServicoCnab(List<SelectItem> listaTipoServicoCnab) {
		this.listaTipoServicoCnab = listaTipoServicoCnab;
	}

	/**
	 * Get: tipoServicoCnabFiltro.
	 *
	 * @return tipoServicoCnabFiltro
	 */
	public Integer getTipoServicoCnabFiltro() {
		return tipoServicoCnabFiltro;
	}

	/**
	 * Set: tipoServicoCnabFiltro.
	 *
	 * @param tipoServicoCnabFiltro the tipo servico cnab filtro
	 */
	public void setTipoServicoCnabFiltro(Integer tipoServicoCnabFiltro) {
		this.tipoServicoCnabFiltro = tipoServicoCnabFiltro;
	}

	/**
	 * Get: listaModalidadeServico.
	 *
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {
		return listaModalidadeServico;
	}

	/**
	 * Set: listaModalidadeServico.
	 *
	 * @param listaModalidadeServico the lista modalidade servico
	 */
	public void setListaModalidadeServico(List<SelectItem> listaModalidadeServico) {
		this.listaModalidadeServico = listaModalidadeServico;
	}

	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: modalidadeServicoFiltro.
	 *
	 * @return modalidadeServicoFiltro
	 */
	public Integer getModalidadeServicoFiltro() {
		return modalidadeServicoFiltro;
	}

	/**
	 * Set: modalidadeServicoFiltro.
	 *
	 * @param modalidadeServicoFiltro the modalidade servico filtro
	 */
	public void setModalidadeServicoFiltro(Integer modalidadeServicoFiltro) {
		this.modalidadeServicoFiltro = modalidadeServicoFiltro;
	}

	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Get: formaLiquidacaoFiltro.
	 *
	 * @return formaLiquidacaoFiltro
	 */
	public Integer getFormaLiquidacaoFiltro() {
		return formaLiquidacaoFiltro;
	}

	/**
	 * Set: formaLiquidacaoFiltro.
	 *
	 * @param formaLiquidacaoFiltro the forma liquidacao filtro
	 */
	public void setFormaLiquidacaoFiltro(Integer formaLiquidacaoFiltro) {
		this.formaLiquidacaoFiltro = formaLiquidacaoFiltro;
	}

	/**
	 * Get: listaFormaLiquidacao.
	 *
	 * @return listaFormaLiquidacao
	 */
	public List<SelectItem> getListaFormaLiquidacao() {
		return listaFormaLiquidacao;
	}

	/**
	 * Set: listaFormaLiquidacao.
	 *
	 * @param listaFormaLiquidacao the lista forma liquidacao
	 */
	public void setListaFormaLiquidacao(List<SelectItem> listaFormaLiquidacao) {
		this.listaFormaLiquidacao = listaFormaLiquidacao;
	}

	/**
	 * Get: itemGridSelecionado.
	 *
	 * @return itemGridSelecionado
	 */
	public Integer getItemGridSelecionado() {
		return itemGridSelecionado;
	}

	/**
	 * Set: itemGridSelecionado.
	 *
	 * @param itemGridSelecionado the item grid selecionado
	 */
	public void setItemGridSelecionado(Integer itemGridSelecionado) {
		this.itemGridSelecionado = itemGridSelecionado;
	}

	/**
	 * Get: listaGridConsultar.
	 *
	 * @return listaGridConsultar
	 */
	public List<ConsultarServicoLancamentoCnabSaidaDTO> getListaGridConsultar() {
		return listaGridConsultar;
	}

	/**
	 * Set: listaGridConsultar.
	 *
	 * @param listaGridConsultar the lista grid consultar
	 */
	public void setListaGridConsultar(
			List<ConsultarServicoLancamentoCnabSaidaDTO> listaGridConsultar) {
		this.listaGridConsultar = listaGridConsultar;
	}

	/**
	 * Get: listaGridControle.
	 *
	 * @return listaGridControle
	 */
	public List<SelectItem> getListaGridControle() {
		return listaGridControle;
	}

	/**
	 * Set: listaGridControle.
	 *
	 * @param listaGridControle the lista grid controle
	 */
	public void setListaGridControle(List<SelectItem> listaGridControle) {
		this.listaGridControle = listaGridControle;
	}

	/**
	 * Get: formaLancCnab.
	 *
	 * @return formaLancCnab
	 */
	public Integer getFormaLancCnab() {
		return formaLancCnab;
	}

	/**
	 * Set: formaLancCnab.
	 *
	 * @param formaLancCnab the forma lanc cnab
	 */
	public void setFormaLancCnab(Integer formaLancCnab) {
		this.formaLancCnab = formaLancCnab;
	}

	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public Integer getModalidadeServico() {
		return modalidadeServico;
	}

	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(Integer modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public Integer getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(Integer tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Get: tipoServicoCnab.
	 *
	 * @return tipoServicoCnab
	 */
	public Integer getTipoServicoCnab() {
		return tipoServicoCnab;
	}

	/**
	 * Set: tipoServicoCnab.
	 *
	 * @param tipoServicoCnab the tipo servico cnab
	 */
	public void setTipoServicoCnab(Integer tipoServicoCnab) {
		this.tipoServicoCnab = tipoServicoCnab;
	}

	/**
	 * Get: formaLiquidacao.
	 *
	 * @return formaLiquidacao
	 */
	public Integer getFormaLiquidacao() {
		return formaLiquidacao;
	}

	/**
	 * Set: formaLiquidacao.
	 *
	 * @param formaLiquidacao the forma liquidacao
	 */
	public void setFormaLiquidacao(Integer formaLiquidacao) {
		this.formaLiquidacao = formaLiquidacao;
	}

	/**
	 * Get: listaFormaLancCnabHash.
	 *
	 * @return listaFormaLancCnabHash
	 */
	public Map<Integer, String> getListaFormaLancCnabHash() {
		return listaFormaLancCnabHash;
	}

	/**
	 * Set lista forma lanc cnab hash.
	 *
	 * @param listaFormaLancCnabHash the lista forma lanc cnab hash
	 */
	public void setListaFormaLancCnabHash(
			Map<Integer, String> listaFormaLancCnabHash) {
		this.listaFormaLancCnabHash = listaFormaLancCnabHash;
	}

	/**
	 * Get: listaFormaLiquidacaoHash.
	 *
	 * @return listaFormaLiquidacaoHash
	 */
	public Map<Integer, String> getListaFormaLiquidacaoHash() {
		return listaFormaLiquidacaoHash;
	}

	/**
	 * Set lista forma liquidacao hash.
	 *
	 * @param listaFormaLiquidacaoHash the lista forma liquidacao hash
	 */
	public void setListaFormaLiquidacaoHash(
			Map<Integer, String> listaFormaLiquidacaoHash) {
		this.listaFormaLiquidacaoHash = listaFormaLiquidacaoHash;
	}

	/**
	 * Get: listaModalidadeServicoHash.
	 *
	 * @return listaModalidadeServicoHash
	 */
	public Map<Integer, String> getListaModalidadeServicoHash() {
		return listaModalidadeServicoHash;
	}

	/**
	 * Set lista modalidade servico hash.
	 *
	 * @param listaModalidadeServicoHash the lista modalidade servico hash
	 */
	public void setListaModalidadeServicoHash(
			Map<Integer, String> listaModalidadeServicoHash) {
		this.listaModalidadeServicoHash = listaModalidadeServicoHash;
	}

	/**
	 * Get: listaTipoServicoCnabHash.
	 *
	 * @return listaTipoServicoCnabHash
	 */
	public Map<Integer, String> getListaTipoServicoCnabHash() {
		return listaTipoServicoCnabHash;
	}

	/**
	 * Set lista tipo servico cnab hash.
	 *
	 * @param listaTipoServicoCnabHash the lista tipo servico cnab hash
	 */
	public void setListaTipoServicoCnabHash(
			Map<Integer, String> listaTipoServicoCnabHash) {
		this.listaTipoServicoCnabHash = listaTipoServicoCnabHash;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: dsFormaLancCnab.
	 *
	 * @return dsFormaLancCnab
	 */
	public String getDsFormaLancCnab() {
		return dsFormaLancCnab;
	}

	/**
	 * Set: dsFormaLancCnab.
	 *
	 * @param dsFormaLancCnab the ds forma lanc cnab
	 */
	public void setDsFormaLancCnab(String dsFormaLancCnab) {
		this.dsFormaLancCnab = dsFormaLancCnab;
	}

	/**
	 * Get: dsFormaLiquidacao.
	 *
	 * @return dsFormaLiquidacao
	 */
	public String getDsFormaLiquidacao() {
		return dsFormaLiquidacao;
	}

	/**
	 * Set: dsFormaLiquidacao.
	 *
	 * @param dsFormaLiquidacao the ds forma liquidacao
	 */
	public void setDsFormaLiquidacao(String dsFormaLiquidacao) {
		this.dsFormaLiquidacao = dsFormaLiquidacao;
	}

	/**
	 * Get: dsModalidadeServico.
	 *
	 * @return dsModalidadeServico
	 */
	public String getDsModalidadeServico() {
		return dsModalidadeServico;
	}

	/**
	 * Set: dsModalidadeServico.
	 *
	 * @param dsModalidadeServico the ds modalidade servico
	 */
	public void setDsModalidadeServico(String dsModalidadeServico) {
		this.dsModalidadeServico = dsModalidadeServico;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: dsTipoServicoCnab.
	 *
	 * @return dsTipoServicoCnab
	 */
	public String getDsTipoServicoCnab() {
		return dsTipoServicoCnab;
	}

	/**
	 * Set: dsTipoServicoCnab.
	 *
	 * @param dsTipoServicoCnab the ds tipo servico cnab
	 */
	public void setDsTipoServicoCnab(String dsTipoServicoCnab) {
		this.dsTipoServicoCnab = dsTipoServicoCnab;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: listaModalidadeServicoInc.
	 *
	 * @return listaModalidadeServicoInc
	 */
	public List<SelectItem> getListaModalidadeServicoInc() {
		return listaModalidadeServicoInc;
	}

	/**
	 * Set: listaModalidadeServicoInc.
	 *
	 * @param listaModalidadeServicoInc the lista modalidade servico inc
	 */
	public void setListaModalidadeServicoInc(
			List<SelectItem> listaModalidadeServicoInc) {
		this.listaModalidadeServicoInc = listaModalidadeServicoInc;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
	    return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
	    this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}

	/**
	 * Get: rdoPesquisa.
	 *
	 * @return rdoPesquisa
	 */
	public String getRdoPesquisa() {
	    return rdoPesquisa;
	}

	/**
	 * Set: rdoPesquisa.
	 *
	 * @param rdoPesquisa the rdo pesquisa
	 */
	public void setRdoPesquisa(String rdoPesquisa) {
	    this.rdoPesquisa = rdoPesquisa;
	}	
	
	
}
