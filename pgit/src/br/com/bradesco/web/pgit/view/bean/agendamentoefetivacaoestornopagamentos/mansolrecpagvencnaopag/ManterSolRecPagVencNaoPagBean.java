/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.mansolrecpagvencnaopag
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.mansolrecpagvencnaopag;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.IManSolRecPagVencNaoPagService;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarSolRecPagtosVencNaoPagoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ConsultarSolRecPagtosVencNaoPagoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ExcluirSolRecuperacaoPagtosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecpagvencnaopag.bean.ExcluirSolRecuperacaoPagtosSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterSolRecPagVencNaoPagBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterSolRecPagVencNaoPagBean {
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo manSolRecPagVencNaoPagServiceImpl. */
	private IManSolRecPagVencNaoPagService manSolRecPagVencNaoPagServiceImpl;
	
	/** Atributo consultasServiceImpl. */
	private IConsultasService consultasServiceImpl;
	
	/** Atributo listaDetalhe. */
	private List<ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO> listaDetalhe;
	
	/** Atributo listaConsulta. */
	private List<ConsultarSolRecPagtosVencNaoPagoSaidaDTO> listaConsulta;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle;
	
	/** Atributo itemSelecionado. */
	private Integer itemSelecionado;
	
	/** Atributo foco. */
	private String foco;
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	/** Atributo chkServicoModalidade. */
	private boolean chkServicoModalidade;
	
	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;
	
	/** Atributo modalidadeFiltro. */
	private Integer modalidadeFiltro;	
	
	/** Atributo listaTipoServicoFiltro. */
	private List<SelectItem> listaTipoServicoFiltro = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeFiltro. */
	private List<SelectItem> listaModalidadeFiltro = new ArrayList<SelectItem>();	
	
	/** Atributo dataInicialSolicitacaoFiltro. */
	private Date dataInicialSolicitacaoFiltro;
	
	/** Atributo dataFinalSolicitacaoFiltro. */
	private Date dataFinalSolicitacaoFiltro;
	
	/** Atributo listaConsultarSituacaoPagamentoFiltro. */
	private List<SelectItem> listaConsultarSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
	
	/** Atributo chkSituacaoSolicitacao. */
	private boolean chkSituacaoSolicitacao;
	
	/** Atributo listaConsultarMotivoSituacaoPagamentoFiltro. */
	private List<SelectItem> listaConsultarMotivoSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
	
	/** Atributo cboMotivoSituacaoFiltro. */
	private Integer cboMotivoSituacaoFiltro;
	
	/** Atributo cboSituacaoPagamentoFiltro. */
	private Integer cboSituacaoPagamentoFiltro;
	
	/* Detalhar */
	//Cabe�alho
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo nroContrato. */
	private String nroContrato;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;
	
	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;
	
	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;
	
	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;
	
	/** Atributo contaDebito. */
	private String contaDebito;
	
	/** Atributo tipoContaDebito. */
	private String tipoContaDebito;
	
	/** Atributo dsNumero. */
	private String dsNumero;
	
	/** Atributo dsSituacaoSolicitacao. */
	private String dsSituacaoSolicitacao;
	
	/** Atributo dsMotivoSolicitacao. */
	private String dsMotivoSolicitacao;
	
	/** Atributo dsDataSolicitacao. */
	private String dsDataSolicitacao;
	
	/** Atributo dsDataProgramadaRecuperacao. */
	private String dsDataProgramadaRecuperacao;
	
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		//Tela de Filtro
		filtroAgendamentoEfetivacaoEstornoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean.setPaginaRetorno("conManSolRecPagVencNaoPag");
		
		setListaConsulta(null);
		setItemSelecionado(null);
		
		limparCampos();
		
		//Carregamento dos combos
		listarTipoServicoPagto();
		listarConsultarSituacaoPagamento();
		
		setHabilitaArgumentosPesquisa(false);
		
		setDisableArgumentosConsulta(false);
		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);		
		filtroAgendamentoEfetivacaoEstornoBean.setEmpresaGestoraFiltro(2269651L);
	}	
	
	/**
	 * Listar consultar situacao pagamento.
	 */
	public void listarConsultarSituacaoPagamento(){
		try{
			listaConsultarSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
			
			ConsultarSituacaoSolicitacaoEstornoEntradaDTO entrada = new ConsultarSituacaoSolicitacaoEstornoEntradaDTO();
			
			entrada.setCdSituacao(0);
			entrada.setNumeroOcorrencias(30);
			entrada.setCdIndicador(2);
			
			List<ConsultarSituacaoSolicitacaoEstornoSaidaDTO> list = comboService.consultarSituacaoSolicitacaoEstorno(entrada);
	
			for (ConsultarSituacaoSolicitacaoEstornoSaidaDTO saida : list){
				listaConsultarSituacaoPagamentoFiltro.add(new SelectItem(saida.getCodigo(), saida.getDescricao()));
			}
		}catch(PdcAdapterFunctionalException p){
			listaConsultarSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
		}
	}
	
	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos(){
		setListaConsulta(null);
		setItemSelecionado(null);
		setDisableArgumentosConsulta(false);
		
		setDataInicialSolicitacaoFiltro(new Date());
		setDataFinalSolicitacaoFiltro(new Date());
		setChkServicoModalidade(false);
		setChkSituacaoSolicitacao(false);
		setModalidadeFiltro(null);
		setListaModalidadeFiltro(new ArrayList<SelectItem>());
		setTipoServicoFiltro(null);
		setCboMotivoSituacaoFiltro(null);
		setListaConsultarMotivoSituacaoPagamentoFiltro(new ArrayList<SelectItem>());
		setCboSituacaoPagamentoFiltro(null);
		
		return "";
	}	
	
	/**
	 * Listar consultar motivo pagamento.
	 */
	public void listarConsultarMotivoPagamento(){
		try{
			setCboMotivoSituacaoFiltro(null);
			
			setListaConsultarMotivoSituacaoPagamentoFiltro(new ArrayList<SelectItem>());
			setCboMotivoSituacaoFiltro(null);
			
			ConsultarMotivoSituacaoEstornoEntradaDTO entrada = new ConsultarMotivoSituacaoEstornoEntradaDTO();
			entrada.setCdSituacao(getCboSituacaoPagamentoFiltro() != null ? getCboSituacaoPagamentoFiltro() : 0);
			
			List<ConsultarMotivoSituacaoEstornoSaidaDTO> list = getComboService().consultarMotivoSituacaoEstornoSaida(entrada);
	
			for (ConsultarMotivoSituacaoEstornoSaidaDTO saida : list){
				getListaConsultarMotivoSituacaoPagamentoFiltro().add(new SelectItem(saida.getCodigo(), saida.getDescricao()));
			}
		}catch (PdcAdapterFunctionalException p){
			setListaConsultarMotivoSituacaoPagamentoFiltro(new ArrayList<SelectItem>());			
		}			
	}	
	
	/**
	 * Listar modalidades pagto.
	 */
	public void listarModalidadesPagto(){
		try{
			listaModalidadeFiltro = new ArrayList<SelectItem>();
			
			if (getTipoServicoFiltro() != null &&  getTipoServicoFiltro()!=0) {
				List<ConsultarModalidadePagtoSaidaDTO> listaModalidades = new ArrayList<ConsultarModalidadePagtoSaidaDTO>();
			
				listaModalidades = comboService.consultarModalidadePagto(getTipoServicoFiltro());
	
				for(ConsultarModalidadePagtoSaidaDTO combo : listaModalidades){
					this.listaModalidadeFiltro.add(new SelectItem(combo.getCdProdutoOperacaoRelacionado(),combo.getDsProdutoOperacaoRelacionado()));
				}
			}else{
				listaModalidadeFiltro = new ArrayList<SelectItem>();
				setModalidadeFiltro(null);
				
			}
		}catch(PdcAdapterFunctionalException e){
			listaModalidadeFiltro = new ArrayList<SelectItem>();
		}
	}	
	
	/**
	 * Listar tipo servico pagto.
	 */
	public void listarTipoServicoPagto(){
		try{
			this.listaTipoServicoFiltro = new ArrayList<SelectItem>();
			
			List<ConsultarServicoPagtoSaidaDTO> listaServico = new ArrayList<ConsultarServicoPagtoSaidaDTO>();
			
			listaServico = comboService.consultarServicoPagto();
			
			for(ConsultarServicoPagtoSaidaDTO combo : listaServico){
				listaTipoServicoFiltro.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
		}
	}	
	
	/**
	 * Limpar tela.
	 *
	 * @return the string
	 */
	public String limparTela(){
		limparCampos();
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado("");
		filtroAgendamentoEfetivacaoEstornoBean.setItemFiltroSelecionado("");

		return "";
	}
	
	/**
	 * Consultar.
	 *
	 * @param evt the evt
	 */
	public void consultar(ActionEvent evt){
		consultar();
	}
	
	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar(){
		try{
			limparInformacaoConsulta();
			
			ConsultarSolRecPagtosVencNaoPagoEntradaDTO entrada = new ConsultarSolRecPagtosVencNaoPagoEntradaDTO();
			
			entrada.setCdFinalidadeRecuperacao(1);
			entrada.setCdSelecaoRecuperacao(0);
			entrada.setCdSolicitacaoPagamento(10);
			
			entrada.setCdTipoContaDebito(0);
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			entrada.setDtFimPesquisa((getDataFinalSolicitacaoFiltro() == null) ? "" : sdf.format(getDataFinalSolicitacaoFiltro()) );
			entrada.setDtInicioPesquisa((getDataInicialSolicitacaoFiltro() == null) ? "" : sdf.format(getDataInicialSolicitacaoFiltro()));	
			
			entrada.setCdProduto(getTipoServicoFiltro());
			entrada.setCdProdutoRelacionado(getModalidadeFiltro());
			entrada.setCdSituacaoSolicitacaoPagamento(getCboSituacaoPagamentoFiltro());
			entrada.setCdMotivoSolicitacao(getCboMotivoSituacaoFiltro());
			
			if ( filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("0") ||  filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("1")){
				entrada.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato() != null ? filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato()	: 0L);
				entrada.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio() != null ? filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio() : 0);
				entrada.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio() != null ? filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio() : 0L);
				entrada.setCdBancoDebito(0);
				entrada.setCdAgenciaDebito(0);
				entrada.setCdContaDebito(0L);
				entrada.setCdDigitoAgenciaDebito(0);
				entrada.setCdDigitoContaDebito("");					
			}
		
			if ( filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("2") ){
				/* Se selecionado �Filtrar por Conta D�bito� mover zeros para os cambos abaixo */
				entrada.setCdPessoaJuridicaContrato(0L);
				entrada.setCdTipoContratoNegocio(0);
				entrada.setNrSequenciaContratoNegocio(0L);
				entrada.setCdBancoDebito(filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro());
				entrada.setCdAgenciaDebito(filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro());
				entrada.setCdContaDebito(filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro());
				entrada.setCdDigitoContaDebito(filtroAgendamentoEfetivacaoEstornoBean.getDigitoContaDebitoFiltro());
				entrada.setCdDigitoAgenciaDebito(0);
			}	
			
			setListaConsulta(getManSolRecPagVencNaoPagServiceImpl().consultarSolRecPagtosVencNaoPago(entrada));
			
			setListaControle(new ArrayList<SelectItem>());
			
			for (int i = 0; i < getListaConsulta().size();i++) {
				getListaControle().add(new SelectItem(i," "));
			}
			
			setDisableArgumentosConsulta(true);			
			
			
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setDisableArgumentosConsulta(false);
		}
		
		return "";
	}
	
	/**
	 * Limpar grid.
	 *
	 * @return the string
	 */
	public String limparGrid(){
		setListaConsulta(null);
		setItemSelecionado(null);
		setDisableArgumentosConsulta(false);
		return "";
	}		
	
	/**
	 * Carrega cabecalho.
	 */
	private void carregaCabecalho(){
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");	
		if(filtroAgendamentoEfetivacaoEstornoBean.isClienteContratoSelecionado()){
			if(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("0")){ //Filtrar por cliente
				setNrCnpjCpf(String.valueOf(filtroAgendamentoEfetivacaoEstornoBean.getNrCpfCnpjCliente()));
				setDsRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoRazaoSocialCliente());
				setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean.getEmpresaGestoraDescCliente());
				setNroContrato(filtroAgendamentoEfetivacaoEstornoBean.getNumeroDescCliente());
				setDsContrato(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoContratoDescCliente());
				setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getSituacaoDescCliente());
			}else{
				if(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("1")){ //Filtrar por contrato
					setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean.getEmpresaGestoraDescContrato());
					setNroContrato(filtroAgendamentoEfetivacaoEstornoBean.getNumeroDescContrato());
					setDsContrato(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoContratoDescContrato());
					setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getSituacaoDescContrato());
				}
			}
		}
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try{
			preencheDados();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
		return "detManSolRecPagVencNaoPag";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try{
			preencheDados();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
		
		return "excManSolRecPagVencNaoPag";
	}	
	
	/**
	 * Preenche dados.
	 */
	public void preencheDados(){
		
		carregaCabecalho();
		ConsultarSolRecPagtosVencNaoPagoSaidaDTO registroSelecionado = getListaConsulta().get(getItemSelecionado());
		
		ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO entrada = new ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO();
		entrada.setCdSolicitacaoPagamento(getListaConsulta().get(getItemSelecionado()).getCdSolicitacaoPagamento());
		entrada.setNrSolicitacaoPagamento(getListaConsulta().get(getItemSelecionado()).getNrSolicitacaoPagamento());
		/*entrada.setCdControlePagamento("");
		entrada.setCdModalidadePagamento(0);
		entrada.setCdPessoaJuridicaContrato(getListaConsulta().get(getItemSelecionado()).getCdPessoaJuridicaContrato());
		entrada.setCdTipoContratoNegocio(getListaConsulta().get(getItemSelecionado()).getCdTipoContratoNegocio());
		entrada.setNrSequenciaContratoNegocio(getListaConsulta().get(getItemSelecionado()).getNrSequenciaContratoNegocio());*/
		
		setListaDetalhe(getManSolRecPagVencNaoPagServiceImpl().conPagtosSolRecVenNaoPago(entrada));

		setDsSituacaoSolicitacao(registroSelecionado.getDsSituacaoSolicitacao());
		setDsMotivoSolicitacao(registroSelecionado.getDsMotivoSolicitacao());
		setDsNumero(Integer.toString(registroSelecionado.getNrSolicitacaoPagamento()));
		setDsDataSolicitacao(FormatarData.formatarData(registroSelecionado.getDsDataSolicitacao()));
		
		if (filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado() != null && filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("2")){
	    	 try{
     	    	ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO = new ConsultarDescBancoAgenciaContaEntradaDTO();
     	    	consultarDescBancoAgenciaContaEntradaDTO.setCdBanco(PgitUtil.verificaIntegerNulo(filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro()));
     	    	consultarDescBancoAgenciaContaEntradaDTO.setCdAgencia(PgitUtil.verificaIntegerNulo(filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro()));
     	    	consultarDescBancoAgenciaContaEntradaDTO.setCdConta(PgitUtil.verificaLongNulo(filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro()));
     	    	consultarDescBancoAgenciaContaEntradaDTO.setCdDigitoConta(PgitUtil.complementaDigito(PgitUtil.verificaStringNula(filtroAgendamentoEfetivacaoEstornoBean.getDigitoContaDebitoFiltro()), 2));
     	    	
     	    	ConsultarDescBancoAgenciaContaSaidaDTO consultarDescBancoAgenciaContaSaidaDTO = getConsultasServiceImpl().consultarDescBancoAgenciaConta(consultarDescBancoAgenciaContaEntradaDTO);
     	    	
    			setDsBancoDebito(PgitUtil.concatenarCampos(consultarDescBancoAgenciaContaSaidaDTO.getCdBanco(),consultarDescBancoAgenciaContaSaidaDTO.getDsBanco()));
    			setDsAgenciaDebito(PgitUtil.concatenarCampos(consultarDescBancoAgenciaContaSaidaDTO.getCdAgencia(),consultarDescBancoAgenciaContaSaidaDTO.getDsAgencia()));
    			setContaDebito(PgitUtil.concatenarCampos(consultarDescBancoAgenciaContaSaidaDTO.getCdConta(), consultarDescBancoAgenciaContaSaidaDTO.getCdDigitoConta()));
    			setTipoContaDebito(consultarDescBancoAgenciaContaSaidaDTO.getDsTipoConta());
    			
 	    	 }catch(PdcAdapterFunctionalException e){
 				setDsBancoDebito(PgitUtil.verificaStringNula(filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro().toString()));
 				setDsAgenciaDebito(PgitUtil.verificaStringNula(filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro().toString()));
 				setContaDebito(PgitUtil.concatenarCampos(PgitUtil.verificaLongNulo(filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro()), PgitUtil.complementaDigito(PgitUtil.verificaStringNula(filtroAgendamentoEfetivacaoEstornoBean.getDigitoContaDebitoFiltro()), 2)));
 				setTipoContaDebito("");
 	    	 }
			

		}else{
			setDsBancoDebito("");
			setDsAgenciaDebito("");
			setContaDebito("");
			setTipoContaDebito("");
		}
		
		setDsDataProgramadaRecuperacao(FormatarData.formatarData(registroSelecionado.getDtRecuperacao()));
				
	}	
	
	/**
	 * Limpar informacao consulta.
	 */
	public void limparInformacaoConsulta(){
		setListaConsulta(null);
		setItemSelecionado(null);
		setDisableArgumentosConsulta(false);
	}	
	
	/**
	 * Limpar tela principal.
	 */
	public void limparTelaPrincipal(){
		limparFiltroPrincipal();

		limparCampos();
	}
	
	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente(){
		//Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		
		setListaConsulta(null);
		setItemSelecionado(null);
		
		limparCampos();
		
		return "";
	}
	
	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt){
		limparCampos();
		setListaConsulta(null);
		setItemSelecionado(null);		
	}
	
	/**
	 * Controlar argumentos pesquisa.
	 */
	public void controlarArgumentosPesquisa(){
		if (filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro() == null ||
		filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro() == null || 
		filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro() == null){
			setHabilitaArgumentosPesquisa(false);
			limparCampos();
		}
		else{
			setHabilitaArgumentosPesquisa(true);
		}
	}	

	/**
	 * Voltar detalhe.
	 *
	 * @return the string
	 */
	public String voltarDetalhe(){
		setItemSelecionado(null);
		
		return "conManSolRecPagVencNaoPag";
	}
	
	/**
	 * Controle situacao motivo solicitacao.
	 */
	public void controleSituacaoMotivoSolicitacao(){
		setCboSituacaoPagamentoFiltro(null);
		setCboMotivoSituacaoFiltro(null);
		setListaConsultarMotivoSituacaoPagamentoFiltro(new ArrayList<SelectItem>());

		setListaConsultarMotivoSituacaoPagamentoFiltro(new ArrayList<SelectItem>());
	}	
	
	
	/**
	 * Controle servico modalidade.
	 */
	public void controleServicoModalidade(){
		setModalidadeFiltro(null);
		setTipoServicoFiltro(null);
		listaModalidadeFiltro = new ArrayList<SelectItem>();
	}	
	
	/**
	 * Voltar excluir.
	 *
	 * @return the string
	 */
	public String voltarExcluir(){
		setItemSelecionado(null);
		
		return "conManSolRecPagVencNaoPag";
	}
	
	/**
	 * Limpar filtro principal.
	 */
	public void limparFiltroPrincipal(){
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		
		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try{
			ExcluirSolRecuperacaoPagtosEntradaDTO entradaDTO = new ExcluirSolRecuperacaoPagtosEntradaDTO();
			entradaDTO.setCdSolicitacaoPagamento(getListaConsulta().get(getItemSelecionado()).getCdSolicitacaoPagamento());
			entradaDTO.setNrSolicitacaoPagamento(getListaConsulta().get(getItemSelecionado()).getNrSolicitacaoPagamento()); 
		
			ExcluirSolRecuperacaoPagtosSaidaDTO saida = getManSolRecPagVencNaoPagServiceImpl().excluirSolRecuperacaoPagtos(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +saida.getCodMensagem() + ") " + saida.getMensagem(), "conManSolRecPagVencNaoPag", "#{manterSolRecPagVencNaoPagBean.consultar}", false);
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
		
		return "";
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt){
		consultar();
		return "";
	}
	
	/**
	 * Pesquisar det exc.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarDetExc(ActionEvent evt){
		try{
			preencheDados();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
		return "";
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}
	
	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}
	
	/**
	 * Get: manSolRecPagVencNaoPagServiceImpl.
	 *
	 * @return manSolRecPagVencNaoPagServiceImpl
	 */
	public IManSolRecPagVencNaoPagService getManSolRecPagVencNaoPagServiceImpl() {
		return manSolRecPagVencNaoPagServiceImpl;
	}
	
	/**
	 * Set: manSolRecPagVencNaoPagServiceImpl.
	 *
	 * @param manSolRecPagVencNaoPagServiceImpl the man sol rec pag venc nao pag service impl
	 */
	public void setManSolRecPagVencNaoPagServiceImpl(
			IManSolRecPagVencNaoPagService manSolRecPagVencNaoPagServiceImpl) {
		this.manSolRecPagVencNaoPagServiceImpl = manSolRecPagVencNaoPagServiceImpl;
	}

	/**
	 * Get: cboMotivoSituacaoFiltro.
	 *
	 * @return cboMotivoSituacaoFiltro
	 */
	public Integer getCboMotivoSituacaoFiltro() {
		return cboMotivoSituacaoFiltro;
	}

	/**
	 * Set: cboMotivoSituacaoFiltro.
	 *
	 * @param cboMotivoSituacaoFiltro the cbo motivo situacao filtro
	 */
	public void setCboMotivoSituacaoFiltro(Integer cboMotivoSituacaoFiltro) {
		this.cboMotivoSituacaoFiltro = cboMotivoSituacaoFiltro;
	}

	/**
	 * Get: cboSituacaoPagamentoFiltro.
	 *
	 * @return cboSituacaoPagamentoFiltro
	 */
	public Integer getCboSituacaoPagamentoFiltro() {
		return cboSituacaoPagamentoFiltro;
	}

	/**
	 * Set: cboSituacaoPagamentoFiltro.
	 *
	 * @param cboSituacaoPagamentoFiltro the cbo situacao pagamento filtro
	 */
	public void setCboSituacaoPagamentoFiltro(Integer cboSituacaoPagamentoFiltro) {
		this.cboSituacaoPagamentoFiltro = cboSituacaoPagamentoFiltro;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Is chk servico modalidade.
	 *
	 * @return true, if is chk servico modalidade
	 */
	public boolean isChkServicoModalidade() {
		return chkServicoModalidade;
	}

	/**
	 * Set: chkServicoModalidade.
	 *
	 * @param chkServicoModalidade the chk servico modalidade
	 */
	public void setChkServicoModalidade(boolean chkServicoModalidade) {
		this.chkServicoModalidade = chkServicoModalidade;
	}

	/**
	 * Is chk situacao solicitacao.
	 *
	 * @return true, if is chk situacao solicitacao
	 */
	public boolean isChkSituacaoSolicitacao() {
		return chkSituacaoSolicitacao;
	}

	/**
	 * Set: chkSituacaoSolicitacao.
	 *
	 * @param chkSituacaoSolicitacao the chk situacao solicitacao
	 */
	public void setChkSituacaoSolicitacao(boolean chkSituacaoSolicitacao) {
		this.chkSituacaoSolicitacao = chkSituacaoSolicitacao;
	}

	/**
	 * Get: dataFinalSolicitacaoFiltro.
	 *
	 * @return dataFinalSolicitacaoFiltro
	 */
	public Date getDataFinalSolicitacaoFiltro() {
		return dataFinalSolicitacaoFiltro;
	}

	/**
	 * Set: dataFinalSolicitacaoFiltro.
	 *
	 * @param dataFinalSolicitacaoFiltro the data final solicitacao filtro
	 */
	public void setDataFinalSolicitacaoFiltro(Date dataFinalSolicitacaoFiltro) {
		this.dataFinalSolicitacaoFiltro = dataFinalSolicitacaoFiltro;
	}

	/**
	 * Get: dataInicialSolicitacaoFiltro.
	 *
	 * @return dataInicialSolicitacaoFiltro
	 */
	public Date getDataInicialSolicitacaoFiltro() {
		return dataInicialSolicitacaoFiltro;
	}

	/**
	 * Set: dataInicialSolicitacaoFiltro.
	 *
	 * @param dataInicialSolicitacaoFiltro the data inicial solicitacao filtro
	 */
	public void setDataInicialSolicitacaoFiltro(Date dataInicialSolicitacaoFiltro) {
		this.dataInicialSolicitacaoFiltro = dataInicialSolicitacaoFiltro;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	/**
	 * Get: foco.
	 *
	 * @return foco
	 */
	public String getFoco() {
		return foco;
	}

	/**
	 * Set: foco.
	 *
	 * @param foco the foco
	 */
	public void setFoco(String foco) {
		this.foco = foco;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public Integer getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(Integer itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: listaConsultarMotivoSituacaoPagamentoFiltro.
	 *
	 * @return listaConsultarMotivoSituacaoPagamentoFiltro
	 */
	public List<SelectItem> getListaConsultarMotivoSituacaoPagamentoFiltro() {
		return listaConsultarMotivoSituacaoPagamentoFiltro;
	}

	/**
	 * Set: listaConsultarMotivoSituacaoPagamentoFiltro.
	 *
	 * @param listaConsultarMotivoSituacaoPagamentoFiltro the lista consultar motivo situacao pagamento filtro
	 */
	public void setListaConsultarMotivoSituacaoPagamentoFiltro(
			List<SelectItem> listaConsultarMotivoSituacaoPagamentoFiltro) {
		this.listaConsultarMotivoSituacaoPagamentoFiltro = listaConsultarMotivoSituacaoPagamentoFiltro;
	}

	/**
	 * Get: listaConsultarSituacaoPagamentoFiltro.
	 *
	 * @return listaConsultarSituacaoPagamentoFiltro
	 */
	public List<SelectItem> getListaConsultarSituacaoPagamentoFiltro() {
		return listaConsultarSituacaoPagamentoFiltro;
	}

	/**
	 * Set: listaConsultarSituacaoPagamentoFiltro.
	 *
	 * @param listaConsultarSituacaoPagamentoFiltro the lista consultar situacao pagamento filtro
	 */
	public void setListaConsultarSituacaoPagamentoFiltro(
			List<SelectItem> listaConsultarSituacaoPagamentoFiltro) {
		this.listaConsultarSituacaoPagamentoFiltro = listaConsultarSituacaoPagamentoFiltro;
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: listaModalidadeFiltro.
	 *
	 * @return listaModalidadeFiltro
	 */
	public List<SelectItem> getListaModalidadeFiltro() {
		return listaModalidadeFiltro;
	}

	/**
	 * Set: listaModalidadeFiltro.
	 *
	 * @param listaModalidadeFiltro the lista modalidade filtro
	 */
	public void setListaModalidadeFiltro(List<SelectItem> listaModalidadeFiltro) {
		this.listaModalidadeFiltro = listaModalidadeFiltro;
	}

	/**
	 * Get: listaTipoServicoFiltro.
	 *
	 * @return listaTipoServicoFiltro
	 */
	public List<SelectItem> getListaTipoServicoFiltro() {
		return listaTipoServicoFiltro;
	}

	/**
	 * Set: listaTipoServicoFiltro.
	 *
	 * @param listaTipoServicoFiltro the lista tipo servico filtro
	 */
	public void setListaTipoServicoFiltro(List<SelectItem> listaTipoServicoFiltro) {
		this.listaTipoServicoFiltro = listaTipoServicoFiltro;
	}

	/**
	 * Get: modalidadeFiltro.
	 *
	 * @return modalidadeFiltro
	 */
	public Integer getModalidadeFiltro() {
		return modalidadeFiltro;
	}

	/**
	 * Set: modalidadeFiltro.
	 *
	 * @param modalidadeFiltro the modalidade filtro
	 */
	public void setModalidadeFiltro(Integer modalidadeFiltro) {
		this.modalidadeFiltro = modalidadeFiltro;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Get: dsMotivoSolicitacao.
	 *
	 * @return dsMotivoSolicitacao
	 */
	public String getDsMotivoSolicitacao() {
		return dsMotivoSolicitacao;
	}

	/**
	 * Set: dsMotivoSolicitacao.
	 *
	 * @param dsMotivoSolicitacao the ds motivo solicitacao
	 */
	public void setDsMotivoSolicitacao(String dsMotivoSolicitacao) {
		this.dsMotivoSolicitacao = dsMotivoSolicitacao;
	}

	/**
	 * Get: dsNumero.
	 *
	 * @return dsNumero
	 */
	public String getDsNumero() {
		return dsNumero;
	}

	/**
	 * Set: dsNumero.
	 *
	 * @param dsNumero the ds numero
	 */
	public void setDsNumero(String dsNumero) {
		this.dsNumero = dsNumero;
	}

	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}

	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}

	/**
	 * Get: dsDataProgramadaRecuperacao.
	 *
	 * @return dsDataProgramadaRecuperacao
	 */
	public String getDsDataProgramadaRecuperacao() {
		return dsDataProgramadaRecuperacao;
	}

	/**
	 * Set: dsDataProgramadaRecuperacao.
	 *
	 * @param dsDataProgramadaRecuperacao the ds data programada recuperacao
	 */
	public void setDsDataProgramadaRecuperacao(String dsDataProgramadaRecuperacao) {
		this.dsDataProgramadaRecuperacao = dsDataProgramadaRecuperacao;
	}

	/**
	 * Get: dsDataSolicitacao.
	 *
	 * @return dsDataSolicitacao
	 */
	public String getDsDataSolicitacao() {
		return dsDataSolicitacao;
	}

	/**
	 * Set: dsDataSolicitacao.
	 *
	 * @param dsDataSolicitacao the ds data solicitacao
	 */
	public void setDsDataSolicitacao(String dsDataSolicitacao) {
		this.dsDataSolicitacao = dsDataSolicitacao;
	}

	/**
	 * Get: listaConsulta.
	 *
	 * @return listaConsulta
	 */
	public List<ConsultarSolRecPagtosVencNaoPagoSaidaDTO> getListaConsulta() {
		return listaConsulta;
	}

	/**
	 * Set: listaConsulta.
	 *
	 * @param listaConsulta the lista consulta
	 */
	public void setListaConsulta(
			List<ConsultarSolRecPagtosVencNaoPagoSaidaDTO> listaConsulta) {
		this.listaConsulta = listaConsulta;
	}

	/**
	 * Get: listaDetalhe.
	 *
	 * @return listaDetalhe
	 */
	public List<ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO> getListaDetalhe() {
		return listaDetalhe;
	}

	/**
	 * Set: listaDetalhe.
	 *
	 * @param listaDetalhe the lista detalhe
	 */
	public void setListaDetalhe(
			List<ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO> listaDetalhe) {
		this.listaDetalhe = listaDetalhe;
	}

	/**
	 * Get: contaDebito.
	 *
	 * @return contaDebito
	 */
	public String getContaDebito() {
		return contaDebito;
	}

	/**
	 * Set: contaDebito.
	 *
	 * @param contaDebito the conta debito
	 */
	public void setContaDebito(String contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: tipoContaDebito.
	 *
	 * @return tipoContaDebito
	 */
	public String getTipoContaDebito() {
		return tipoContaDebito;
	}

	/**
	 * Set: tipoContaDebito.
	 *
	 * @param tipoContaDebito the tipo conta debito
	 */
	public void setTipoContaDebito(String tipoContaDebito) {
		this.tipoContaDebito = tipoContaDebito;
	}

	/**
	 * Get: consultasServiceImpl.
	 *
	 * @return consultasServiceImpl
	 */
	public IConsultasService getConsultasServiceImpl() {
		return consultasServiceImpl;
	}

	/**
	 * Set: consultasServiceImpl.
	 *
	 * @param consultasServiceImpl the consultas service impl
	 */
	public void setConsultasServiceImpl(IConsultasService consultasServiceImpl) {
		this.consultasServiceImpl = consultasServiceImpl;
	}
	
}
