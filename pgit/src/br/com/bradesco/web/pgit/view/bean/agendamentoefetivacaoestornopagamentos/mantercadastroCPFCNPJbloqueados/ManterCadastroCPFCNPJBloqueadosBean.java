/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.mantercadastroCPFCNPJbloqueados
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.mantercadastroCPFCNPJbloqueados;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ConsultarBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ConsultarBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ExcluirBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ExcluirBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.IncluirBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.IncluirBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarIncluirBloqueioRastreamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarIncluirBloqueioRastreamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: ManterCadastroCPFCNPJBloqueadosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterCadastroCPFCNPJBloqueadosBean {
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;

	/** Atributo PARTICIPANTES. */
	private String PARTICIPANTES = "conManCadCpfCnpjParticipantes";

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo excCnpjCpfFormatado. */
	private String excCnpjCpfFormatado;

	/** Atributo excDsPessoa. */
	private String excDsPessoa;

	/** Atributo itemFiltroInclusaoSelecionado. */
	private String itemFiltroInclusaoSelecionado;

	/** Atributo listaGridParticipantes. */
	private List<ListarParticipantesSaidaDTO> listaGridParticipantes = new ArrayList<ListarParticipantesSaidaDTO>();
	
	/** Atributo listaGridControleParticipantes. */
	private List<SelectItem> listaGridControleParticipantes = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionadoParticipantes. */
	private Integer itemSelecionadoParticipantes;

	/** Atributo listaGridCpfCnpjBloqueados. */
	private List<ConsultarBloqueioRastreamentoSaidaDTO> listaGridCpfCnpjBloqueados = new ArrayList<ConsultarBloqueioRastreamentoSaidaDTO>();

	/** Atributo listaGridControleCpfCnpjBloqueados. */
	private List<SelectItem> listaGridControleCpfCnpjBloqueados = new ArrayList<SelectItem>();

	/** Atributo itemSelecionadoCpfCnpjBloqueados. */
	private Integer itemSelecionadoCpfCnpjBloqueados;

	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;

	/** Atributo listaGrid. */
	private List<ConsultarBloqueioRastreamentoSaidaDTO> listaGrid = new ArrayList<ConsultarBloqueioRastreamentoSaidaDTO>();

	/** Atributo listaExclusao. */
	private List<ConsultarBloqueioRastreamentoSaidaDTO> listaExclusao;

	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo foco. */
	private String foco;

	/** Atributo manterCadastroCPFCNPJbloqueadosServiceImpl. */
	private IMantercadastroCPFCNPJbloqueadosService manterCadastroCPFCNPJbloqueadosServiceImpl;

	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos;

	/** Atributo listaControleCheck. */
	private List<SelectItem> listaControleCheck;

	/** Atributo habilitaExcluir. */
	private boolean habilitaExcluir = true;

	/** Atributo habilitaIncluir. */
	private boolean habilitaIncluir = true;

	/** Atributo habilitaDetalhar. */
	private boolean habilitaDetalhar = true;

	/** Atributo cdCpf. */
	private Long cdCpf;

	/** Atributo cdCnpj. */
	private Long cdCnpj;

	/** Atributo cdFilialCnpj. */
	private Integer cdFilialCnpj;

	/** Atributo cdControleCpf. */
	private Integer cdControleCpf;

	/** Atributo cdControleCnpj. */
	private Integer cdControleCnpj;

	/** Atributo cdCpfInclusao. */
	private Long cdCpfInclusao;

	/** Atributo cdCnpjInclusao. */
	private Long cdCnpjInclusao;

	/** Atributo cdFilialCnpjInclusao. */
	private Integer cdFilialCnpjInclusao;

	/** Atributo cdControleCpfInclusao. */
	private Integer cdControleCpfInclusao;

	/** Atributo cdControleCnpjInclusao. */
	private Integer cdControleCnpjInclusao;

	/** Atributo dsPessoa. */
	private String dsPessoa;

	/** Atributo descEmpresaGestora. */
	private String descEmpresaGestora;

	/** Atributo descContrato. */
	private String descContrato;

	/** Atributo descNumeroContrato. */
	private String descNumeroContrato;

	/** Atributo descSituacao. */
	private String descSituacao;

	/** Atributo cpfCNPJFormatado. */
	private String cpfCNPJFormatado;

	/** Atributo cdCanalInclusao. */
	private String cdCanalInclusao;

	/** Atributo nmOperacaoFluxoInclusao. */
	private String nmOperacaoFluxoInclusao;

	/** Atributo cdCanalManutencao. */
	private String cdCanalManutencao;

	/** Atributo nmOperacaoFluxoManutencao. */
	private String nmOperacaoFluxoManutencao;
	
	// Cliente Representante
	/** Atributo cpfCnpjRepresentante. */
	private String cpfCnpjRepresentante;

	/** Atributo nomeRazaoRepresentante. */
	private String nomeRazaoRepresentante;

	/** Atributo grupEconRepresentante. */
	private String grupEconRepresentante;

	/** Atributo ativEconRepresentante. */
	private String ativEconRepresentante;

	/** Atributo segRepresentante. */
	private String segRepresentante;

	/** Atributo subSegRepresentante. */
	private String subSegRepresentante;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;
	
	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado;

	// Cliente Participante
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;
	
	/** Atributo cpfCnpjParticipanteTemp. */
	private String cpfCnpjParticipanteTemp;

	/** Atributo nomeRazaoParticipanteTemp. */
	private String nomeRazaoParticipanteTemp;

	/** Atributo cpfCnpjParticipanteDet. */
	private String cpfCnpjParticipanteDet;

	/** Atributo nomeRazaoParticipanteDet. */
	private String nomeRazaoParticipanteDet;

	/** Atributo grupoEconParticipante. */
	private String grupoEconParticipante;

	/** Atributo atividadeEconParticipante. */
	private String atividadeEconParticipante;

	/** Atributo segmentoParticipante. */
	private String segmentoParticipante;

	/** Atributo subSegmentoParticipante. */
	private String subSegmentoParticipante;

	// Contrato
	/** Atributo empresaContrato. */
	private String empresaContrato;

	/** Atributo tipoContrato. */
	private String tipoContrato;

	/** Atributo numeroContrato. */
	private String numeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo situacaoContrato. */
	private String situacaoContrato;

	/** Atributo motivoContrato. */
	private String motivoContrato;

	/** Atributo participacaoContrato. */
	private String participacaoContrato;

	/** Atributo detalharBloqueioRastreamentoSaidaDTO. */
	private DetalharBloqueioRastreamentoSaidaDTO detalharBloqueioRastreamentoSaidaDTO;

	/** Atributo CON_CADASTRO_CPF_CNPJ_BLOQUEADOS. */
	private static final String CON_CADASTRO_CPF_CNPJ_BLOQUEADOS = "conCadastroCPFCNPJBloqueados";

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		// Tela de Filtro
		filtroAgendamentoEfetivacaoEstornoBean
						.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
						.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean.setPaginaRetorno("conCadastroCPFCNPJBloqueados");

		// Limpar Variaveis
		setListaControleRadio(null);
		setListaGrid(null);
		setItemSelecionadoLista(null);

		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
		setDisableArgumentosConsulta(false);
		setHabilitaDetalhar(true);
		setHabilitaExcluir(true);
		setHabilitaIncluir(true);

		this.identificacaoClienteContratoBean
						.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
						.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteManterContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conCadastroCPFCNPJBloqueados");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
	}

	/**
	 * Limpar.
	 */
	public void limpar() {
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);

		filtroAgendamentoEfetivacaoEstornoBean.setEmpresaGestoraDescCliente("");
		filtroAgendamentoEfetivacaoEstornoBean.setNumeroDescCliente("");
		filtroAgendamentoEfetivacaoEstornoBean.setDescricaoContratoDescCliente("");
		filtroAgendamentoEfetivacaoEstornoBean.setSituacaoDescCliente("");

		filtroAgendamentoEfetivacaoEstornoBean.setEmpresaGestoraDescContrato("");
		filtroAgendamentoEfetivacaoEstornoBean.setNumeroDescContrato("");
		filtroAgendamentoEfetivacaoEstornoBean.setDescricaoContratoDescContrato("");
		filtroAgendamentoEfetivacaoEstornoBean.setSituacaoDescContrato("");

		setListaGrid(null);
		setDisableArgumentosConsulta(false);
	}

	/**
	 * Limpar campos.
	 */
	public void limparCampos() {
		setCdCpf(null);
		setCdControleCpf(null);
		setCdCnpj(null);
		setCdFilialCnpj(null);
		setCdControleCnpj(null);
	}

	/**
	 * Limpar grid consultar.
	 */
	public void limparGridConsultar() {
		setListaControleRadio(null);
		setListaGrid(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		setHabilitaDetalhar(true);
		setHabilitaExcluir(true);
		setHabilitaIncluir(true);
		setOpcaoChecarTodos(false);
	}

	/**
	 * Limpar radios principais.
	 */
	public void limparRadiosPrincipais() {
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		setListaGrid(null);
		setItemSelecionadoLista(null);

	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		identificacaoClienteContratoBean.limparCampos();
		setListaGrid(null);
		setItemSelecionadoLista(null);

		return "";

	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		setListaGrid(null);
		setItemSelecionadoLista(null);
	}

	/**
	 * Limpar tela principal.
	 *
	 * @return the string
	 */
	public String limparTelaPrincipal() {
		// Limpar Argumentos de Pesquisa e Lista
		setListaGrid(null);
		setItemSelecionadoLista(null);
		setOpcaoChecarTodos(false);

		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
		if (filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado() != null
						&& filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("2")) {
			filtroAgendamentoEfetivacaoEstornoBean.limparParticipanteContrato();
			filtroAgendamentoEfetivacaoEstornoBean.setBancoContaDebitoFiltro(237);
		}

		return "";
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		consultar();
		return "";
	}

	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar() {

		try {
			identificacaoClienteContratoBean.consultarContrato();

			setDisableArgumentosConsulta(true);
			setHabilitaDetalhar(true);
			setHabilitaExcluir(true);
			setHabilitaIncluir(true);

			ConsultarBloqueioRastreamentoEntradaDTO entrada = new ConsultarBloqueioRastreamentoEntradaDTO();
			listaGrid = new ArrayList<ConsultarBloqueioRastreamentoSaidaDTO>();

			entrada
							.setCdpessoaJuridicaContrato(identificacaoClienteContratoBean.getEmpresaGestoraFiltro() != null ? identificacaoClienteContratoBean
											.getEmpresaGestoraFiltro()
											: 0);
			entrada
							.setCdTipoContratoNegocio(identificacaoClienteContratoBean.getTipoContratoFiltro() != null ? identificacaoClienteContratoBean
											.getTipoContratoFiltro()
											: 0);
			entrada.setNrSequenciaContratoNegocio(identificacaoClienteContratoBean.getNumeroFiltro() != null
							&& !identificacaoClienteContratoBean.getNumeroFiltro().equals("") ? Long
							.valueOf(identificacaoClienteContratoBean.getNumeroFiltro()) : Long.valueOf(0));

			List<ConsultarBloqueioRastreamentoSaidaDTO> lista = null;
			lista = getManterCadastroCPFCNPJbloqueadosServiceImpl().pesquisarCPFCNPJBloqueados(entrada);
			for (Iterator<ConsultarBloqueioRastreamentoSaidaDTO> iterator = lista.iterator(); iterator.hasNext();) {
				ConsultarBloqueioRastreamentoSaidaDTO saida = iterator.next();

				String cpfCnpjFormatado = "";
				String corpoCpfCnpj = saida.getCdCpfCnpjBloqueio().toString();
				String filialCpfCnpj = saida.getCdFilialCnpjBloqueio().toString();
				String controleCpfCnpj = saida.getCdControleCnpjCpf().toString();

				if (!filialCpfCnpj.equals("0")) {

					while (corpoCpfCnpj.length() < 8) {
						corpoCpfCnpj = "0" + corpoCpfCnpj;
					}

					while (filialCpfCnpj.length() < 4) {
						filialCpfCnpj = "0" + filialCpfCnpj;
					}
				} else {
					while (corpoCpfCnpj.length() < 9) {
						corpoCpfCnpj = "0" + corpoCpfCnpj;
					}
				}

				if (controleCpfCnpj.length() == 1) {
					controleCpfCnpj = "0" + controleCpfCnpj;
				}

				if (saida.getCdFilialCnpjBloqueio() == 0) {// formata cpf
					cpfCnpjFormatado = corpoCpfCnpj.substring(0, 3) + ".";
					cpfCnpjFormatado += corpoCpfCnpj.substring(3, 6) + ".";
					cpfCnpjFormatado += corpoCpfCnpj.substring(6) + "-";
					cpfCnpjFormatado += controleCpfCnpj;
					saida.setCnpjCpfFormatado(cpfCnpjFormatado);
				} else {// formata cnpj
					cpfCnpjFormatado = corpoCpfCnpj.substring(0, 2) + ".";
					cpfCnpjFormatado += corpoCpfCnpj.substring(2, 5) + ".";
					cpfCnpjFormatado += corpoCpfCnpj.substring(5) + "/";
					cpfCnpjFormatado += filialCpfCnpj + "-";
					cpfCnpjFormatado += controleCpfCnpj;
					saida.setCnpjCpfFormatado(cpfCnpjFormatado);
				}

				listaGrid.add(saida);
			}

			listaControleCheck = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGrid().size(); i++) {
				this.listaControleCheck.add(new SelectItem(i, " "));
			}

		} catch (PdcAdapterFunctionalException p) {
//			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
//							false);
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setDisableArgumentosConsulta(false);
		}

		return "";
	}

	/**
	 * Habilita botoes.
	 */
	public void habilitaBotoes() {

		int cont = 0;
		boolean habilita = true;
		for (int i = 0; i < getListaGrid().size(); i++) {
			if (getListaGrid().get(i).isCheck()) {
				habilita = false;
				cont++;
			}
		}

		setHabilitaExcluir(habilita);

		if (cont == 1) {
			setHabilitaDetalhar(false);
		} else {
			setHabilitaDetalhar(true);
		}

	}

	/**
	 * Habilita botoes2.
	 */
	public void habilitaBotoes2() {
		setHabilitaExcluir(false);// Desabilita excluir
		setHabilitaDetalhar(false);
		setHabilitaIncluir(false);

	}

	/**
	 * Iniciar participantes.
	 *
	 * @return the string
	 */
	public String iniciarParticipantes() {
		try {
			carregaListaParticipantes();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridParticipantes(null);
			setItemSelecionadoParticipantes(null);
			return "";
		}

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(
						identificacaoClienteContratoBean
								.getItemSelecionadoLista());

		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());

		// Cliente Participante
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		
		} else {
			setCpfCnpjParticipante("");
			setNomeRazaoParticipante("");
		}

		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());

		return PARTICIPANTES;
	}

	/**
	 * Cpf cnpj bloqueados.
	 *
	 * @return the string
	 */
	public String cpfCnpjBloqueados() {
		try {
			carregaListaCpfCnpjBloqueados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridCpfCnpjBloqueados(null);
			setItemSelecionadoCpfCnpjBloqueados(null);
		}
		return "conManCadCpfCnpj";
	}

	/**
	 * Carrega lista cpf cnpj bloqueados.
	 */
	public void carregaListaCpfCnpjBloqueados() {
		try {
			ConsultarBloqueioRastreamentoEntradaDTO entrada = new ConsultarBloqueioRastreamentoEntradaDTO();

			ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().
			get(identificacaoClienteContratoBean.getItemSelecionadoLista());
	
			entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
	
			setListaGridCpfCnpjBloqueados(manterCadastroCPFCNPJbloqueadosServiceImpl.pesquisarCPFCNPJBloqueados(entrada));
	
			listaGridControleCpfCnpjBloqueados = new ArrayList<SelectItem>();
	
			for (int i = 0; i < listaGridCpfCnpjBloqueados.size(); i++) {
				listaGridControleCpfCnpjBloqueados.add(new SelectItem(i, " "));
			}
	
			setItemSelecionadoCpfCnpjBloqueados(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridCpfCnpjBloqueados(new ArrayList<ConsultarBloqueioRastreamentoSaidaDTO>());
			setItemSelecionadoCpfCnpjBloqueados(null);
		}
	}

	/**
	 * Voltar participantes.
	 *
	 * @return the string
	 */
	public String voltarParticipantes() {
		return "conCadastroCPFCNPJBloqueados";
	}

	/**
	 * Carrega lista participantes.
	 *
	 * @return the string
	 */
	public String carregaListaParticipantes() {
		listaGridParticipantes = new ArrayList<ListarParticipantesSaidaDTO>();

		ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().
				get(identificacaoClienteContratoBean.getItemSelecionadoLista());

		entradaDTO.setMaxOcorrencias(20);
		entradaDTO.setCodPessoaJuridica(saidaDTO.getCdPessoaJuridica());
		entradaDTO.setCodTipoContrato(saidaDTO.getCdTipoContrato());
		entradaDTO.setNrSequenciaContrato(saidaDTO.getNrSequenciaContrato());

		setListaGridParticipantes(getManterContratoImpl().listarParticipantes(entradaDTO));

		this.listaGridControleParticipantes = new ArrayList<SelectItem>();
		for (int i = 0; i < getListaGridParticipantes().size(); i++) {
			listaGridControleParticipantes.add(new SelectItem(i, " "));
		}

		setItemSelecionadoParticipantes(null);

		return "";
	}

	/**
	 * Iniciar incluir.
	 *
	 * @return the string
	 */
	public String iniciarIncluir() {
		ListarContratosPgitSaidaDTO dto = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean
						.getItemSelecionadoLista());

		// Cliente Representante
		setCpfCnpjRepresentante(dto.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(dto.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(dto.getDsGrupoEconomico());
		setAtivEconRepresentante(dto.getDsAtividadeEconomica());
		setSegRepresentante(dto.getDsSegmentoCliente());
		setSubSegRepresentante(dto.getDsSubSegmentoCliente());
		setDsAgenciaGestora(dto.getDsAgenciaOperadora());
		setDsGerenteResponsavel(dto.getDescFuncionarioBradesco());
		setGerenteResponsavelFormatado(dto.getCdFuncionarioBradesco() + " - " + dto.getNmFuncionarioBradesco());

		// Cliente Participante
		setNomeRazaoParticipante(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? getNomeRazaoRepresentante()
				: identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getDsNomeRazao());
		setCpfCnpjParticipante(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? getCpfCnpjRepresentante()
				: identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado());

		// Contrato
		setEmpresaContrato(String.valueOf(dto.getDsPessoaJuridica()));
		setTipoContrato(dto.getDsTipoContrato());
		setNumeroContrato(String.valueOf(dto.getNrSequenciaContrato()));
		setSituacaoContrato(dto.getDsSituacaoContrato());
		setMotivoContrato(dto.getDsMotivoSituacao());
		setParticipacaoContrato(dto.getCdTipoParticipacao());
		setDescricaoContrato(dto.getDsContrato());

		setItemFiltroInclusaoSelecionado(null);
		limparTelaInclusao();

		return "INICIAR_INCLUIR";
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		try {
			ValidarIncluirBloqueioRastreamentoEntradaDTO entrada = new ValidarIncluirBloqueioRastreamentoEntradaDTO();
			ValidarIncluirBloqueioRastreamentoSaidaDTO saida = null;

			entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());

			entrada.setCdCpfCnpjBloqueio(getCdCpfInclusao() == null ? getCdCnpjInclusao() : getCdCpfInclusao());
			entrada.setCdFilialCnpjBloqueio(getCdFilialCnpjInclusao() == null ? 0 : getCdFilialCnpjInclusao());
			entrada.setCdControleCnpjCpf(getCdControleCpfInclusao() == null ? getCdControleCnpjInclusao()
							: getCdControleCpfInclusao());

			saida = getManterCadastroCPFCNPJbloqueadosServiceImpl().validarBloqueioRastreamento(entrada);
			setDsPessoa(saida.getDsPessoa());

			String cpfCNPJ = "";
			String corpoCpfCNPJ = "";
			String filialCNPJ = "";
			String controleCpfCNPJ = "";
			if (getCdCpfInclusao() != null) {
				corpoCpfCNPJ = getCdCpfInclusao().toString();
				while (corpoCpfCNPJ.length() < 9) {
					corpoCpfCNPJ = "0" + corpoCpfCNPJ;
				}

				controleCpfCNPJ = getCdControleCpfInclusao().toString();
				while (controleCpfCNPJ.length() < 2) {
					controleCpfCNPJ = "0" + controleCpfCNPJ;
				}

				cpfCNPJ = corpoCpfCNPJ.substring(0, 3) + ".";
				cpfCNPJ += corpoCpfCNPJ.substring(3, 6) + ".";
				cpfCNPJ += corpoCpfCNPJ.substring(6) + "-";
				cpfCNPJ += controleCpfCNPJ;
			} else {
				corpoCpfCNPJ = getCdCnpjInclusao().toString();
				while (corpoCpfCNPJ.length() < 8) {
					corpoCpfCNPJ = "0" + corpoCpfCNPJ;
				}

				filialCNPJ = getCdFilialCnpjInclusao().toString();
				while (filialCNPJ.length() < 4) {
					filialCNPJ = "0" + filialCNPJ;
				}

				controleCpfCNPJ = getCdControleCnpjInclusao().toString();
				while (controleCpfCNPJ.length() < 2) {
					controleCpfCNPJ = "0" + controleCpfCNPJ;
				}

				cpfCNPJ = corpoCpfCNPJ.substring(0, 2) + ".";
				cpfCNPJ += corpoCpfCNPJ.substring(2, 5) + ".";
				cpfCNPJ += corpoCpfCNPJ.substring(5) + "/";
				cpfCNPJ += filialCNPJ + "-";
				cpfCNPJ += controleCpfCNPJ;
			}

			setCpfCNPJFormatado(cpfCNPJ);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setDisableArgumentosConsulta(false);
			return "INICIAR_INCLUIR";
		}

		return "AVANCAR_INCLUIR";
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		try {
			IncluirBloqueioRastreamentoEntradaDTO entrada = new IncluirBloqueioRastreamentoEntradaDTO();
			IncluirBloqueioRastreamentoSaidaDTO saida = null;

			entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());

			entrada.setCdCpfCnpjBloqueio(getCdCpfInclusao() == null ? getCdCnpjInclusao() : getCdCpfInclusao());
			entrada.setCdFilialCnpjBloqueio(getCdFilialCnpjInclusao() == null ? 0 : getCdFilialCnpjInclusao());
			entrada.setCdControleCnpjCpf(getCdControleCpfInclusao() == null ? getCdControleCnpjInclusao()
							: getCdControleCpfInclusao());

			saida = getManterCadastroCPFCNPJbloqueadosServiceImpl().incluirBloqueioRastreamento(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(), false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setDisableArgumentosConsulta(false);
			return null;
		}

		limparTelaInclusao();
		carregaListaCpfCnpjBloqueados();
		return "conManCadCpfCnpj";
	}

	/**
	 * Limpar tela inclusao.
	 */
	public void limparTelaInclusao() {
		setCdCpfInclusao(null);
		setCdControleCpfInclusao(null);
		setCdCnpjInclusao(null);
		setCdFilialCnpjInclusao(null);
		setCdControleCnpjInclusao(null);
	}

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {

		return "conManCadCpfCnpj";
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		return "INICIAR_INCLUIR";
	}

	/**
	 * Voltar cpf cnpj bloqueados.
	 *
	 * @return the string
	 */
	public String voltarCpfCnpjBloqueados() {
		return PARTICIPANTES;
	}

	/**
	 * Iniciar excluir.
	 *
	 * @return the string
	 */
	public String iniciarExcluir() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getDescFuncionarioBradesco());
		setGerenteResponsavelFormatado(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getNmFuncionarioBradesco());

		// Cliente Participante
		setNomeRazaoParticipante(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? getNomeRazaoRepresentante()
				: identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getDsNomeRazao());
		setCpfCnpjParticipante(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? getCpfCnpjRepresentante()
				: identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado());

		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());
		setExcCnpjCpfFormatado(listaGridCpfCnpjBloqueados.get(itemSelecionadoCpfCnpjBloqueados).getCpfCnpjFormatado());
		setExcDsPessoa(listaGridCpfCnpjBloqueados.get(itemSelecionadoCpfCnpjBloqueados).getDsPessoa());

		listaExclusao = new ArrayList<ConsultarBloqueioRastreamentoSaidaDTO>();

		listaExclusao.add(listaGridCpfCnpjBloqueados.get(itemSelecionadoCpfCnpjBloqueados));

		return "INICIAR_EXCLUIR";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getDescFuncionarioBradesco());
		setGerenteResponsavelFormatado(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getNmFuncionarioBradesco());

		// Cliente Participante
		setNomeRazaoParticipante(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? getNomeRazaoRepresentante()
				: identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getDsNomeRazao());
		setCpfCnpjParticipante(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? getCpfCnpjRepresentante()
				: identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado());

		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());

		try {
			DetalharBloqueioRastreamentoEntradaDTO entrada = new DetalharBloqueioRastreamentoEntradaDTO();

			entrada.setCdpessoaJuridicaContrato(PgitUtil.verificaLongNulo(saidaDTO.getCdPessoaJuridica()));
			entrada.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(saidaDTO.getCdTipoContrato()));
			entrada.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(saidaDTO.getNrSequenciaContrato()));

			entrada.setNmControleRastreabilidadeTitulo(listaGridCpfCnpjBloqueados.get(itemSelecionadoCpfCnpjBloqueados).getNmControleRastreabilidadeTitulo());

			detalharBloqueioRastreamentoSaidaDTO = getManterCadastroCPFCNPJbloqueadosServiceImpl()
							.detalharBloqueioRastreamento(entrada);

			String cpfCnpjFormatado = "";
			String corpoCpfCnpj = detalharBloqueioRastreamentoSaidaDTO.getCdCpfCnpjBloqueio() == null ? "0" : detalharBloqueioRastreamentoSaidaDTO.getCdCpfCnpjBloqueio().toString();
			String filialCpfCnpj = detalharBloqueioRastreamentoSaidaDTO.getCdFilialCnpjBloqueio() == null ? "0" : detalharBloqueioRastreamentoSaidaDTO.getCdFilialCnpjBloqueio().toString();
			String controleCpfCnpj = detalharBloqueioRastreamentoSaidaDTO.getCdControleCnpjCpf() == null ? "0" : detalharBloqueioRastreamentoSaidaDTO.getCdControleCnpjCpf().toString();

			if (!filialCpfCnpj.equals("0")) {

				while (corpoCpfCnpj.length() < 8) {
					corpoCpfCnpj = "0" + corpoCpfCnpj;
				}

				while (filialCpfCnpj.length() < 4) {
					filialCpfCnpj = "0" + filialCpfCnpj;
				}
			} else {
				while (corpoCpfCnpj.length() < 9) {
					corpoCpfCnpj = "0" + corpoCpfCnpj;
				}
			}

			if (controleCpfCnpj.length() == 1) {
				controleCpfCnpj = "0" + controleCpfCnpj;
			}

			if (detalharBloqueioRastreamentoSaidaDTO.getCdFilialCnpjBloqueio() == 0) {// formata
				// cpf
				cpfCnpjFormatado = corpoCpfCnpj.substring(0, 3) + ".";
				cpfCnpjFormatado += corpoCpfCnpj.substring(3, 6) + ".";
				cpfCnpjFormatado += corpoCpfCnpj.substring(6) + "-";
				cpfCnpjFormatado += controleCpfCnpj;
				detalharBloqueioRastreamentoSaidaDTO.setCnpjCpfFormatado(cpfCnpjFormatado);
			} else {// formata cnpj
				cpfCnpjFormatado = corpoCpfCnpj.substring(0, 2) + ".";
				cpfCnpjFormatado += corpoCpfCnpj.substring(2, 5) + ".";
				cpfCnpjFormatado += corpoCpfCnpj.substring(5) + "/";
				cpfCnpjFormatado += filialCpfCnpj + "-";
				cpfCnpjFormatado += controleCpfCnpj;
				detalharBloqueioRastreamentoSaidaDTO.setCnpjCpfFormatado(cpfCnpjFormatado);
			}

			setCdCanalInclusao(detalharBloqueioRastreamentoSaidaDTO.getCdCanalInclusao() == 0 ? " "
							: detalharBloqueioRastreamentoSaidaDTO.getCdCanalInclusao().toString() + "  -   ");
			setNmOperacaoFluxoInclusao(detalharBloqueioRastreamentoSaidaDTO.getNmOperacaoFluxoInclusao().equals("0") ? " "
							: detalharBloqueioRastreamentoSaidaDTO.getNmOperacaoFluxoInclusao());
			setCdCanalManutencao(detalharBloqueioRastreamentoSaidaDTO.getCdCanalManutencao() == 0 ? " "
							: detalharBloqueioRastreamentoSaidaDTO.getCdCanalManutencao().toString() + "  -   ");
			setNmOperacaoFluxoManutencao(detalharBloqueioRastreamentoSaidaDTO.getNmOperacaoFluxoManutencao()
							.equals("0") ? " " : detalharBloqueioRastreamentoSaidaDTO.getNmOperacaoFluxoManutencao());

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setDisableArgumentosConsulta(false);
			return null;
		}

		return "DETALHAR";
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		try {
			ExcluirBloqueioRastreamentoEntradaDTO entrada = new ExcluirBloqueioRastreamentoEntradaDTO();
			ExcluirBloqueioRastreamentoSaidaDTO saida = null;

			entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
			entrada.setQtdeRegistrosOcorrencias(getListaExclusao().size());

			entrada.setOcorrencias(new ArrayList<Integer>());
			for (int i = 0; i < getListaExclusao().size(); i++) {
				entrada.getOcorrencias().add(getListaExclusao().get(i).getNmControleRastreabilidadeTitulo());
			}

			saida = getManterCadastroCPFCNPJbloqueadosServiceImpl().excluirBloqueioRastreamento(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(), "#{manterCadastroCPFCNPJBloqueadosBean.carregaListaCpfCnpjBloqueados}", BradescoViewExceptionActionType.ACTION, false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}

		return "conManCadCpfCnpj";
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ConsultarBloqueioRastreamentoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ConsultarBloqueioRastreamentoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
					FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: foco.
	 *
	 * @return foco
	 */
	public String getFoco() {
		return foco;
	}

	/**
	 * Set: foco.
	 *
	 * @param foco the foco
	 */
	public void setFoco(String foco) {
		this.foco = foco;
	}

	/**
	 * Get: manterCadastroCPFCNPJbloqueadosServiceImpl.
	 *
	 * @return manterCadastroCPFCNPJbloqueadosServiceImpl
	 */
	public IMantercadastroCPFCNPJbloqueadosService getManterCadastroCPFCNPJbloqueadosServiceImpl() {
		return manterCadastroCPFCNPJbloqueadosServiceImpl;
	}

	/**
	 * Set: manterCadastroCPFCNPJbloqueadosServiceImpl.
	 *
	 * @param manterCadastroCPFCNPJbloqueadosServiceImpl the manter cadastro cpfcnp jbloqueados service impl
	 */
	public void setManterCadastroCPFCNPJbloqueadosServiceImpl(
					IMantercadastroCPFCNPJbloqueadosService manterCadastroCPFCNPJbloqueadosServiceImpl) {
		this.manterCadastroCPFCNPJbloqueadosServiceImpl = manterCadastroCPFCNPJbloqueadosServiceImpl;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Get: listaControleCheck.
	 *
	 * @return listaControleCheck
	 */
	public List<SelectItem> getListaControleCheck() {
		return listaControleCheck;
	}

	/**
	 * Set: listaControleCheck.
	 *
	 * @param listaControleCheck the lista controle check
	 */
	public void setListaControleCheck(List<SelectItem> listaControleCheck) {
		this.listaControleCheck = listaControleCheck;
	}

	/**
	 * Is habilita excluir.
	 *
	 * @return true, if is habilita excluir
	 */
	public boolean isHabilitaExcluir() {
		return habilitaExcluir;
	}

	/**
	 * Set: habilitaExcluir.
	 *
	 * @param habilitaExcluir the habilita excluir
	 */
	public void setHabilitaExcluir(boolean habilitaExcluir) {
		this.habilitaExcluir = habilitaExcluir;
	}

	/**
	 * Is habilita detalhar.
	 *
	 * @return true, if is habilita detalhar
	 */
	public boolean isHabilitaDetalhar() {
		return habilitaDetalhar;
	}

	/**
	 * Set: habilitaDetalhar.
	 *
	 * @param habilitaDetalhar the habilita detalhar
	 */
	public void setHabilitaDetalhar(boolean habilitaDetalhar) {
		this.habilitaDetalhar = habilitaDetalhar;
	}

	/**
	 * Get: dsPessoa.
	 *
	 * @return dsPessoa
	 */
	public String getDsPessoa() {
		return dsPessoa;
	}

	/**
	 * Set: dsPessoa.
	 *
	 * @param dsPessoa the ds pessoa
	 */
	public void setDsPessoa(String dsPessoa) {
		this.dsPessoa = dsPessoa;
	}

	/**
	 * Get: cdCpf.
	 *
	 * @return cdCpf
	 */
	public Long getCdCpf() {
		return cdCpf;
	}

	/**
	 * Set: cdCpf.
	 *
	 * @param cdCpf the cd cpf
	 */
	public void setCdCpf(Long cdCpf) {
		this.cdCpf = cdCpf;
	}

	/**
	 * Get: cdCnpj.
	 *
	 * @return cdCnpj
	 */
	public Long getCdCnpj() {
		return cdCnpj;
	}

	/**
	 * Set: cdCnpj.
	 *
	 * @param cdCnpj the cd cnpj
	 */
	public void setCdCnpj(Long cdCnpj) {
		this.cdCnpj = cdCnpj;
	}

	/**
	 * Get: cdFilialCnpj.
	 *
	 * @return cdFilialCnpj
	 */
	public Integer getCdFilialCnpj() {
		return cdFilialCnpj;
	}

	/**
	 * Set: cdFilialCnpj.
	 *
	 * @param cdFilialCnpj the cd filial cnpj
	 */
	public void setCdFilialCnpj(Integer cdFilialCnpj) {
		this.cdFilialCnpj = cdFilialCnpj;
	}

	/**
	 * Get: cdControleCpf.
	 *
	 * @return cdControleCpf
	 */
	public Integer getCdControleCpf() {
		return cdControleCpf;
	}

	/**
	 * Set: cdControleCpf.
	 *
	 * @param cdControleCpf the cd controle cpf
	 */
	public void setCdControleCpf(Integer cdControleCpf) {
		this.cdControleCpf = cdControleCpf;
	}

	/**
	 * Get: cdControleCnpj.
	 *
	 * @return cdControleCnpj
	 */
	public Integer getCdControleCnpj() {
		return cdControleCnpj;
	}

	/**
	 * Set: cdControleCnpj.
	 *
	 * @param cdControleCnpj the cd controle cnpj
	 */
	public void setCdControleCnpj(Integer cdControleCnpj) {
		this.cdControleCnpj = cdControleCnpj;
	}

	/**
	 * Get: cpfCNPJFormatado.
	 *
	 * @return cpfCNPJFormatado
	 */
	public String getCpfCNPJFormatado() {
		return cpfCNPJFormatado;
	}

	/**
	 * Set: cpfCNPJFormatado.
	 *
	 * @param cpfCNPJFormatado the cpf cnpj formatado
	 */
	public void setCpfCNPJFormatado(String cpfCNPJFormatado) {
		this.cpfCNPJFormatado = cpfCNPJFormatado;
	}

	/**
	 * Get: listaExclusao.
	 *
	 * @return listaExclusao
	 */
	public List<ConsultarBloqueioRastreamentoSaidaDTO> getListaExclusao() {
		return listaExclusao;
	}

	/**
	 * Set: listaExclusao.
	 *
	 * @param listaExclusao the lista exclusao
	 */
	public void setListaExclusao(List<ConsultarBloqueioRastreamentoSaidaDTO> listaExclusao) {
		this.listaExclusao = listaExclusao;
	}

	/**
	 * Get: detalharBloqueioRastreamentoSaidaDTO.
	 *
	 * @return detalharBloqueioRastreamentoSaidaDTO
	 */
	public DetalharBloqueioRastreamentoSaidaDTO getDetalharBloqueioRastreamentoSaidaDTO() {
		return detalharBloqueioRastreamentoSaidaDTO;
	}

	/**
	 * Set: detalharBloqueioRastreamentoSaidaDTO.
	 *
	 * @param detalharBloqueioRastreamentoSaidaDTO the detalhar bloqueio rastreamento saida dto
	 */
	public void setDetalharBloqueioRastreamentoSaidaDTO(
					DetalharBloqueioRastreamentoSaidaDTO detalharBloqueioRastreamentoSaidaDTO) {
		this.detalharBloqueioRastreamentoSaidaDTO = detalharBloqueioRastreamentoSaidaDTO;
	}

	/**
	 * Get: descEmpresaGestora.
	 *
	 * @return descEmpresaGestora
	 */
	public String getDescEmpresaGestora() {
		return descEmpresaGestora;
	}

	/**
	 * Set: descEmpresaGestora.
	 *
	 * @param descEmpresaGestora the desc empresa gestora
	 */
	public void setDescEmpresaGestora(String descEmpresaGestora) {
		this.descEmpresaGestora = descEmpresaGestora;
	}

	/**
	 * Get: descContrato.
	 *
	 * @return descContrato
	 */
	public String getDescContrato() {
		return descContrato;
	}

	/**
	 * Set: descContrato.
	 *
	 * @param descContrato the desc contrato
	 */
	public void setDescContrato(String descContrato) {
		this.descContrato = descContrato;
	}

	/**
	 * Get: descNumeroContrato.
	 *
	 * @return descNumeroContrato
	 */
	public String getDescNumeroContrato() {
		return descNumeroContrato;
	}

	/**
	 * Set: descNumeroContrato.
	 *
	 * @param descNumeroContrato the desc numero contrato
	 */
	public void setDescNumeroContrato(String descNumeroContrato) {
		this.descNumeroContrato = descNumeroContrato;
	}

	/**
	 * Get: descSituacao.
	 *
	 * @return descSituacao
	 */
	public String getDescSituacao() {
		return descSituacao;
	}

	/**
	 * Set: descSituacao.
	 *
	 * @param descSituacao the desc situacao
	 */
	public void setDescSituacao(String descSituacao) {
		this.descSituacao = descSituacao;
	}

	/**
	 * Get: itemFiltroInclusaoSelecionado.
	 *
	 * @return itemFiltroInclusaoSelecionado
	 */
	public String getItemFiltroInclusaoSelecionado() {
		return itemFiltroInclusaoSelecionado;
	}

	/**
	 * Set: itemFiltroInclusaoSelecionado.
	 *
	 * @param itemFiltroInclusaoSelecionado the item filtro inclusao selecionado
	 */
	public void setItemFiltroInclusaoSelecionado(String itemFiltroInclusaoSelecionado) {
		this.itemFiltroInclusaoSelecionado = itemFiltroInclusaoSelecionado;
	}

	/**
	 * Get: cdCpfInclusao.
	 *
	 * @return cdCpfInclusao
	 */
	public Long getCdCpfInclusao() {
		return cdCpfInclusao;
	}

	/**
	 * Set: cdCpfInclusao.
	 *
	 * @param cdCpfInclusao the cd cpf inclusao
	 */
	public void setCdCpfInclusao(Long cdCpfInclusao) {
		this.cdCpfInclusao = cdCpfInclusao;
	}

	/**
	 * Get: cdCnpjInclusao.
	 *
	 * @return cdCnpjInclusao
	 */
	public Long getCdCnpjInclusao() {
		return cdCnpjInclusao;
	}

	/**
	 * Set: cdCnpjInclusao.
	 *
	 * @param cdCnpjInclusao the cd cnpj inclusao
	 */
	public void setCdCnpjInclusao(Long cdCnpjInclusao) {
		this.cdCnpjInclusao = cdCnpjInclusao;
	}

	/**
	 * Get: cdFilialCnpjInclusao.
	 *
	 * @return cdFilialCnpjInclusao
	 */
	public Integer getCdFilialCnpjInclusao() {
		return cdFilialCnpjInclusao;
	}

	/**
	 * Set: cdFilialCnpjInclusao.
	 *
	 * @param cdFilialCnpjInclusao the cd filial cnpj inclusao
	 */
	public void setCdFilialCnpjInclusao(Integer cdFilialCnpjInclusao) {
		this.cdFilialCnpjInclusao = cdFilialCnpjInclusao;
	}

	/**
	 * Get: cdControleCpfInclusao.
	 *
	 * @return cdControleCpfInclusao
	 */
	public Integer getCdControleCpfInclusao() {
		return cdControleCpfInclusao;
	}

	/**
	 * Set: cdControleCpfInclusao.
	 *
	 * @param cdControleCpfInclusao the cd controle cpf inclusao
	 */
	public void setCdControleCpfInclusao(Integer cdControleCpfInclusao) {
		this.cdControleCpfInclusao = cdControleCpfInclusao;
	}

	/**
	 * Get: cdControleCnpjInclusao.
	 *
	 * @return cdControleCnpjInclusao
	 */
	public Integer getCdControleCnpjInclusao() {
		return cdControleCnpjInclusao;
	}

	/**
	 * Set: cdControleCnpjInclusao.
	 *
	 * @param cdControleCnpjInclusao the cd controle cnpj inclusao
	 */
	public void setCdControleCnpjInclusao(Integer cdControleCnpjInclusao) {
		this.cdControleCnpjInclusao = cdControleCnpjInclusao;
	}

	/**
	 * Get: cdCanalInclusao.
	 *
	 * @return cdCanalInclusao
	 */
	public String getCdCanalInclusao() {
		return cdCanalInclusao;
	}

	/**
	 * Set: cdCanalInclusao.
	 *
	 * @param cdCanalInclusao the cd canal inclusao
	 */
	public void setCdCanalInclusao(String cdCanalInclusao) {
		this.cdCanalInclusao = cdCanalInclusao;
	}

	/**
	 * Get: nmOperacaoFluxoInclusao.
	 *
	 * @return nmOperacaoFluxoInclusao
	 */
	public String getNmOperacaoFluxoInclusao() {
		return nmOperacaoFluxoInclusao;
	}

	/**
	 * Set: nmOperacaoFluxoInclusao.
	 *
	 * @param nmOperacaoFluxoInclusao the nm operacao fluxo inclusao
	 */
	public void setNmOperacaoFluxoInclusao(String nmOperacaoFluxoInclusao) {
		this.nmOperacaoFluxoInclusao = nmOperacaoFluxoInclusao;
	}

	/**
	 * Get: cdCanalManutencao.
	 *
	 * @return cdCanalManutencao
	 */
	public String getCdCanalManutencao() {
		return cdCanalManutencao;
	}

	/**
	 * Set: cdCanalManutencao.
	 *
	 * @param cdCanalManutencao the cd canal manutencao
	 */
	public void setCdCanalManutencao(String cdCanalManutencao) {
		this.cdCanalManutencao = cdCanalManutencao;
	}

	/**
	 * Get: nmOperacaoFluxoManutencao.
	 *
	 * @return nmOperacaoFluxoManutencao
	 */
	public String getNmOperacaoFluxoManutencao() {
		return nmOperacaoFluxoManutencao;
	}

	/**
	 * Set: nmOperacaoFluxoManutencao.
	 *
	 * @param nmOperacaoFluxoManutencao the nm operacao fluxo manutencao
	 */
	public void setNmOperacaoFluxoManutencao(String nmOperacaoFluxoManutencao) {
		this.nmOperacaoFluxoManutencao = nmOperacaoFluxoManutencao;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Is habilita incluir.
	 *
	 * @return true, if is habilita incluir
	 */
	public boolean isHabilitaIncluir() {
		return habilitaIncluir;
	}

	/**
	 * Set: habilitaIncluir.
	 *
	 * @param habilitaIncluir the habilita incluir
	 */
	public void setHabilitaIncluir(boolean habilitaIncluir) {
		this.habilitaIncluir = habilitaIncluir;
	}

	/**
	 * Get: cpfCnpjRepresentante.
	 *
	 * @return cpfCnpjRepresentante
	 */
	public String getCpfCnpjRepresentante() {
		return cpfCnpjRepresentante;
	}

	/**
	 * Set: cpfCnpjRepresentante.
	 *
	 * @param cpfCnpjRepresentante the cpf cnpj representante
	 */
	public void setCpfCnpjRepresentante(String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	/**
	 * Get: nomeRazaoRepresentante.
	 *
	 * @return nomeRazaoRepresentante
	 */
	public String getNomeRazaoRepresentante() {
		return nomeRazaoRepresentante;
	}

	/**
	 * Set: nomeRazaoRepresentante.
	 *
	 * @param nomeRazaoRepresentante the nome razao representante
	 */
	public void setNomeRazaoRepresentante(String nomeRazaoRepresentante) {
		this.nomeRazaoRepresentante = nomeRazaoRepresentante;
	}

	/**
	 * Get: grupEconRepresentante.
	 *
	 * @return grupEconRepresentante
	 */
	public String getGrupEconRepresentante() {
		return grupEconRepresentante;
	}

	/**
	 * Set: grupEconRepresentante.
	 *
	 * @param grupEconRepresentante the grup econ representante
	 */
	public void setGrupEconRepresentante(String grupEconRepresentante) {
		this.grupEconRepresentante = grupEconRepresentante;
	}

	/**
	 * Get: ativEconRepresentante.
	 *
	 * @return ativEconRepresentante
	 */
	public String getAtivEconRepresentante() {
		return ativEconRepresentante;
	}

	/**
	 * Set: ativEconRepresentante.
	 *
	 * @param ativEconRepresentante the ativ econ representante
	 */
	public void setAtivEconRepresentante(String ativEconRepresentante) {
		this.ativEconRepresentante = ativEconRepresentante;
	}

	/**
	 * Get: segRepresentante.
	 *
	 * @return segRepresentante
	 */
	public String getSegRepresentante() {
		return segRepresentante;
	}

	/**
	 * Set: segRepresentante.
	 *
	 * @param segRepresentante the seg representante
	 */
	public void setSegRepresentante(String segRepresentante) {
		this.segRepresentante = segRepresentante;
	}

	/**
	 * Get: subSegRepresentante.
	 *
	 * @return subSegRepresentante
	 */
	public String getSubSegRepresentante() {
		return subSegRepresentante;
	}

	/**
	 * Set: subSegRepresentante.
	 *
	 * @param subSegRepresentante the sub seg representante
	 */
	public void setSubSegRepresentante(String subSegRepresentante) {
		this.subSegRepresentante = subSegRepresentante;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: cpfCnpjParticipanteTemp.
	 *
	 * @return cpfCnpjParticipanteTemp
	 */
	public String getCpfCnpjParticipanteTemp() {
		return cpfCnpjParticipanteTemp;
	}

	/**
	 * Set: cpfCnpjParticipanteTemp.
	 *
	 * @param cpfCnpjParticipanteTemp the cpf cnpj participante temp
	 */
	public void setCpfCnpjParticipanteTemp(String cpfCnpjParticipanteTemp) {
		this.cpfCnpjParticipanteTemp = cpfCnpjParticipanteTemp;
	}

	/**
	 * Get: nomeRazaoParticipanteTemp.
	 *
	 * @return nomeRazaoParticipanteTemp
	 */
	public String getNomeRazaoParticipanteTemp() {
		return nomeRazaoParticipanteTemp;
	}

	/**
	 * Set: nomeRazaoParticipanteTemp.
	 *
	 * @param nomeRazaoParticipanteTemp the nome razao participante temp
	 */
	public void setNomeRazaoParticipanteTemp(String nomeRazaoParticipanteTemp) {
		this.nomeRazaoParticipanteTemp = nomeRazaoParticipanteTemp;
	}

	/**
	 * Get: cpfCnpjParticipanteDet.
	 *
	 * @return cpfCnpjParticipanteDet
	 */
	public String getCpfCnpjParticipanteDet() {
		return cpfCnpjParticipanteDet;
	}

	/**
	 * Set: cpfCnpjParticipanteDet.
	 *
	 * @param cpfCnpjParticipanteDet the cpf cnpj participante det
	 */
	public void setCpfCnpjParticipanteDet(String cpfCnpjParticipanteDet) {
		this.cpfCnpjParticipanteDet = cpfCnpjParticipanteDet;
	}

	/**
	 * Get: nomeRazaoParticipanteDet.
	 *
	 * @return nomeRazaoParticipanteDet
	 */
	public String getNomeRazaoParticipanteDet() {
		return nomeRazaoParticipanteDet;
	}

	/**
	 * Set: nomeRazaoParticipanteDet.
	 *
	 * @param nomeRazaoParticipanteDet the nome razao participante det
	 */
	public void setNomeRazaoParticipanteDet(String nomeRazaoParticipanteDet) {
		this.nomeRazaoParticipanteDet = nomeRazaoParticipanteDet;
	}

	/**
	 * Get: grupoEconParticipante.
	 *
	 * @return grupoEconParticipante
	 */
	public String getGrupoEconParticipante() {
		return grupoEconParticipante;
	}

	/**
	 * Set: grupoEconParticipante.
	 *
	 * @param grupoEconParticipante the grupo econ participante
	 */
	public void setGrupoEconParticipante(String grupoEconParticipante) {
		this.grupoEconParticipante = grupoEconParticipante;
	}

	/**
	 * Get: atividadeEconParticipante.
	 *
	 * @return atividadeEconParticipante
	 */
	public String getAtividadeEconParticipante() {
		return atividadeEconParticipante;
	}

	/**
	 * Set: atividadeEconParticipante.
	 *
	 * @param atividadeEconParticipante the atividade econ participante
	 */
	public void setAtividadeEconParticipante(String atividadeEconParticipante) {
		this.atividadeEconParticipante = atividadeEconParticipante;
	}

	/**
	 * Get: segmentoParticipante.
	 *
	 * @return segmentoParticipante
	 */
	public String getSegmentoParticipante() {
		return segmentoParticipante;
	}

	/**
	 * Set: segmentoParticipante.
	 *
	 * @param segmentoParticipante the segmento participante
	 */
	public void setSegmentoParticipante(String segmentoParticipante) {
		this.segmentoParticipante = segmentoParticipante;
	}

	/**
	 * Get: subSegmentoParticipante.
	 *
	 * @return subSegmentoParticipante
	 */
	public String getSubSegmentoParticipante() {
		return subSegmentoParticipante;
	}

	/**
	 * Set: subSegmentoParticipante.
	 *
	 * @param subSegmentoParticipante the sub segmento participante
	 */
	public void setSubSegmentoParticipante(String subSegmentoParticipante) {
		this.subSegmentoParticipante = subSegmentoParticipante;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: gerenteResponsavelFormatado.
	 *
	 * @return gerenteResponsavelFormatado
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}

	/**
	 * Set: gerenteResponsavelFormatado.
	 *
	 * @param gerenteResponsavelFormatado the gerente responsavel formatado
	 */
	public void setGerenteResponsavelFormatado(String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: listaGridControleCpfCnpjBloqueados.
	 *
	 * @return listaGridControleCpfCnpjBloqueados
	 */
	public List<SelectItem> getListaGridControleCpfCnpjBloqueados() {
		return listaGridControleCpfCnpjBloqueados;
	}

	/**
	 * Set: listaGridControleCpfCnpjBloqueados.
	 *
	 * @param listaGridControleCpfCnpjBloqueados the lista grid controle cpf cnpj bloqueados
	 */
	public void setListaGridControleCpfCnpjBloqueados(
			List<SelectItem> listaGridControleCpfCnpjBloqueados) {
		this.listaGridControleCpfCnpjBloqueados = listaGridControleCpfCnpjBloqueados;
	}

	/**
	 * Get: itemSelecionadoCpfCnpjBloqueados.
	 *
	 * @return itemSelecionadoCpfCnpjBloqueados
	 */
	public Integer getItemSelecionadoCpfCnpjBloqueados() {
		return itemSelecionadoCpfCnpjBloqueados;
	}

	/**
	 * Set: itemSelecionadoCpfCnpjBloqueados.
	 *
	 * @param itemSelecionadoCpfCnpjBloqueados the item selecionado cpf cnpj bloqueados
	 */
	public void setItemSelecionadoCpfCnpjBloqueados(
			Integer itemSelecionadoCpfCnpjBloqueados) {
		this.itemSelecionadoCpfCnpjBloqueados = itemSelecionadoCpfCnpjBloqueados;
	}

	/**
	 * Get: listaGridCpfCnpjBloqueados.
	 *
	 * @return listaGridCpfCnpjBloqueados
	 */
	public List<ConsultarBloqueioRastreamentoSaidaDTO> getListaGridCpfCnpjBloqueados() {
		return listaGridCpfCnpjBloqueados;
	}

	/**
	 * Set: listaGridCpfCnpjBloqueados.
	 *
	 * @param listaGridCpfCnpjBloqueados the lista grid cpf cnpj bloqueados
	 */
	public void setListaGridCpfCnpjBloqueados(
			List<ConsultarBloqueioRastreamentoSaidaDTO> listaGridCpfCnpjBloqueados) {
		this.listaGridCpfCnpjBloqueados = listaGridCpfCnpjBloqueados;
	}

	/**
	 * Get: listaGridParticipantes.
	 *
	 * @return listaGridParticipantes
	 */
	public List<ListarParticipantesSaidaDTO> getListaGridParticipantes() {
		return listaGridParticipantes;
	}

	/**
	 * Set: listaGridParticipantes.
	 *
	 * @param listaGridParticipantes the lista grid participantes
	 */
	public void setListaGridParticipantes(
			List<ListarParticipantesSaidaDTO> listaGridParticipantes) {
		this.listaGridParticipantes = listaGridParticipantes;
	}

	/**
	 * Get: listaGridControleParticipantes.
	 *
	 * @return listaGridControleParticipantes
	 */
	public List<SelectItem> getListaGridControleParticipantes() {
		return listaGridControleParticipantes;
	}

	/**
	 * Set: listaGridControleParticipantes.
	 *
	 * @param listaGridControleParticipantes the lista grid controle participantes
	 */
	public void setListaGridControleParticipantes(
			List<SelectItem> listaGridControleParticipantes) {
		this.listaGridControleParticipantes = listaGridControleParticipantes;
	}

	/**
	 * Get: itemSelecionadoParticipantes.
	 *
	 * @return itemSelecionadoParticipantes
	 */
	public Integer getItemSelecionadoParticipantes() {
		return itemSelecionadoParticipantes;
	}

	/**
	 * Set: itemSelecionadoParticipantes.
	 *
	 * @param itemSelecionadoParticipantes the item selecionado participantes
	 */
	public void setItemSelecionadoParticipantes(Integer itemSelecionadoParticipantes) {
		this.itemSelecionadoParticipantes = itemSelecionadoParticipantes;
	}

	/**
	 * Get: excCnpjCpfFormatado.
	 *
	 * @return excCnpjCpfFormatado
	 */
	public String getExcCnpjCpfFormatado() {
		return excCnpjCpfFormatado;
	}

	/**
	 * Set: excCnpjCpfFormatado.
	 *
	 * @param excCnpjCpfFormatado the exc cnpj cpf formatado
	 */
	public void setExcCnpjCpfFormatado(String excCnpjCpfFormatado) {
		this.excCnpjCpfFormatado = excCnpjCpfFormatado;
	}

	/**
	 * Get: excDsPessoa.
	 *
	 * @return excDsPessoa
	 */
	public String getExcDsPessoa() {
		return excDsPessoa;
	}

	/**
	 * Set: excDsPessoa.
	 *
	 * @param excDsPessoa the exc ds pessoa
	 */
	public void setExcDsPessoa(String excDsPessoa) {
		this.excDsPessoa = excDsPessoa;
	}
}