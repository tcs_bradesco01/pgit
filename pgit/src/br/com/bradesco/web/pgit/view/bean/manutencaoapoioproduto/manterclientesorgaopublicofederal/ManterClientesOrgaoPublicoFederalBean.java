/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterclientesorgaopublicofederal
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterclientesorgaopublicofederal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercomptiposervicoformalanc.IManterCompTipoServicoFormaLancService;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.IManterClientesOrgaoPublicoService;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.AlterarParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.AlterarParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ConsultarUsuarioSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.DetalharParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.DetalharParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ExcluirSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.IncluirParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.IncluirParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarCtasVincPartOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarCtasVincPartOrgaoPublicoOcorrencias;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarCtasVincPartOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarParticipantesOrgaoOcorrencias;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarParticipantesOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarParticipantesOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ListarSoliRelatorioOrgaosPublicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ValidarContratoPagSalPartOrgaoPublicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.materclientesorgaopublico.bean.ValidarContratoPagSalPartOrgaoPublicoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: ManterClientesOrgaoPublicoFederalBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ManterClientesOrgaoPublicoFederalBean extends PagamentosBean {

	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean = new FiltroAgendamentoEfetivacaoEstornoBean();

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo manterCompTipoServicoFormaLancServiceImpl. */
	private IManterCompTipoServicoFormaLancService manterCompTipoServicoFormaLancServiceImpl;

	/** Atributo manterClientesOrgaoPublicoService. */
	private IManterClientesOrgaoPublicoService manterClientesOrgaoPublicoService;

	/** Atributo CON_MANTER_CLIENTES_ORGAO. */
	private static final String CON_MANTER_CLIENTES_ORGAO = "conManterClientesOrgaoPublicoFederal";

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo listaGridPesquisa. */
	private List<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO> listaGridPesquisa;

	/** Atributo saidaListarSoliRelatorioOrgaosPublicos. */
	private ListarSoliRelatorioOrgaosPublicosSaidaDTO saidaListarSoliRelatorioOrgaosPublicos;

	/** Atributo saidaExcluirSoliRelatorioOrgaosPublicos. */
	private ExcluirSoliRelatorioOrgaosPublicosSaidaDTO saidaExcluirSoliRelatorioOrgaosPublicos;

	/** Atributo saidaConsultarUsuarioSoliRelatorioOrgaosPublicos. */
	private ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO saidaConsultarUsuarioSoliRelatorioOrgaosPublicos;

	/** Atributo saidaListarPartOrgaoPublico. */
	private ListarParticipantesOrgaoPublicoSaidaDTO saidaListarPartOrgaoPublico;

	/** Atributo saidaDetalharPartOrgaoPublico. */
	private DetalharParticipantesOrgaoPublicoSaidaDTO saidaDetalharPartOrgaoPublico;

	/** Atributo saidaExcluirPartOrgaoPublico. */
	private ExcluirParticipantesOrgaoPublicoSaidaDTO saidaExcluirPartOrgaoPublico;

	/** Atributo saidaAlterarPartOrgaoPublico. */
	private AlterarParticipantesOrgaoPublicoSaidaDTO saidaAlterarPartOrgaoPublico;

	/** Atributo saidaValidarContratoPagSalPartOrgaoPublico. */
	private ValidarContratoPagSalPartOrgaoPublicoSaidaDTO saidaValidarContratoPagSalPartOrgaoPublico;

	/** Atributo saidaListarCtasVincPartOrgaoPublico. */
	private ListarCtasVincPartOrgaoPublicoSaidaDTO saidaListarCtasVincPartOrgaoPublico;

	/** Atributo saidaIncluirPartOrgaoPublico. */
	private IncluirParticipantesOrgaoPublicoSaidaDTO saidaIncluirPartOrgaoPublico;

	/** Atributo listaCtasVincPartOrgao. */
	private List<ListarCtasVincPartOrgaoPublicoOcorrencias> listaCtasVincPartOrgao;

	/** Atributo listaSaidaOrgaoPublico. */
	private List<ListarParticipantesOrgaoOcorrencias> listaSaidaOrgaoPublico = new ArrayList<ListarParticipantesOrgaoOcorrencias>();

	/** Atributo itemSelecionadoListaCtas. */
	private Integer itemSelecionadoListaCtas;

	/** Atributo itemSelecionadoPartOrgao. */
	private Integer itemSelecionadoPartOrgao;

	/** Atributo dtSolicInicio. */
	private Date dtSolicInicio;

	/** Atributo dtSolicFinal. */
	private Date dtSolicFinal;

	/** Atributo usuarioSolicitante. */
	private String usuarioSolicitante;

	/** Atributo dataSolicitacao. */
	private String dataSolicitacao;

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;

	/** Atributo filtroSituacaoCliente. */
	private Integer filtroSituacaoCliente;

	/** Atributo filtroSituacaoClienteAlterar. */
	private Integer filtroSituacaoClienteAlterar;

	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;

	/** Atributo dsNomeRazaoSocial. */
	private String dsNomeRazaoSocial;

	/** Atributo cpfCnpjFormatado. */
	private String cpfCnpjFormatado;

	/** Atributo cdAgenciaDebito. */
	private Integer cdAgenciaDebito;

	/** Atributo contaDigito. */
	private String contaDigito;

	/** Atributo dsSituacaoRegistro. */
	private String dsSituacaoRegistro;

	/** Atributo codAgenciaDesc. */
	private String codAgenciaDesc;

	/** Atributo flagIncluir. */
	private boolean flagIncluir = false;

	// M�TODOS
	/**
	 * Iniciar tela.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
				"conManterClientesOrgaoPublicoFederal");
		limparIniciar();
	}

	/**
	 * Limpar iniciar.
	 */
	public void limparIniciar() {
		setDtSolicFinal(new Date());
		setDtSolicInicio(new Date());
		limparSituacaoCliente();
		getFiltroAgendamentoEfetivacaoEstornoBean().setItemFiltroSelecionado(
				null);
		setDisableArgumentosConsulta(false);
	}

	/**
	 * Limpar dados.
	 */
	public void limparDados() {
		setDtSolicFinal(new Date());
		setDtSolicInicio(new Date());
	}

	/**
	 * Limpar situacao cliente.
	 */
	public void limparSituacaoCliente() {
		setFiltroSituacaoCliente(0);
		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getSaidaConsultarListaClientePessoas() != null) {
			getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSaidaConsultarListaClientePessoas()
					.setCnpjOuCpfFormatado("");
			getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSaidaConsultarListaClientePessoas().setDsNomeRazao("");
		}
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(
				null);
		limparFiltroPrincipal();
		setItemSelecionadoPartOrgao(null);
		listaSaidaOrgaoPublico = new ArrayList<ListarParticipantesOrgaoOcorrencias>();
	}

	/**
	 * Limpar filtro por cliente.
	 */
	public void limparFiltroPorCliente() {
		setFiltroSituacaoCliente(0);
		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getSaidaConsultarListaClientePessoas() != null) {
			getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSaidaConsultarListaClientePessoas()
					.setCnpjOuCpfFormatado("");
			getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSaidaConsultarListaClientePessoas().setDsNomeRazao("");
		}
		listaSaidaOrgaoPublico = new ArrayList<ListarParticipantesOrgaoOcorrencias>();
	}

	/**
	 * Limpar dados incluir.
	 */
	public void limparDadosIncluir() {
		getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(
				2269651l);
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoContratoFiltro(null);
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEmpresaGestoraDescContratoOrgao("");
		getFiltroAgendamentoEfetivacaoEstornoBean().setNumeroDescContratoOrgao(
				"");
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setDescricaoContratoDescContratoOrgao("");
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.getSaidaConsultarListaClientePessoas().setCnpjOuCpfFormatado(
						"");
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.getSaidaConsultarListaClientePessoas().setDsNomeRazao("");
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(
				null);
		getFiltroAgendamentoEfetivacaoEstornoBean().setItemFiltroSelecionado(
				null);

		setDisableArgumentosConsulta(false);
		limparFiltroPrincipal();

		listaCtasVincPartOrgao = new ArrayList<ListarCtasVincPartOrgaoPublicoOcorrencias>();
	}

	/**
	 * Limpar dados consultar.
	 */
	public void limparDadosConsultar() {
		setFiltroSituacaoCliente(0);
		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getSaidaConsultarListaClientePessoas() != null) {
			getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSaidaConsultarListaClientePessoas()
					.setCnpjOuCpfFormatado("");
			getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSaidaConsultarListaClientePessoas().setDsNomeRazao("");
		}
		limparFiltroPrincipal();
		setItemSelecionadoPartOrgao(null);
		listaSaidaOrgaoPublico = new ArrayList<ListarParticipantesOrgaoOcorrencias>();
	}

	/**
	 * Carregar lista.
	 */
	public void carregarLista() {
		ListarSoliRelatorioOrgaosPublicosEntradaDTO entradaDto = new ListarSoliRelatorioOrgaosPublicosEntradaDTO();

		entradaDto.setDtFimSolicitacao(FormatarData
				.formataDiaMesAnoToPdc(getDtSolicFinal()));
		entradaDto.setDtInicioSolicitacao(FormatarData
				.formataDiaMesAnoToPdc(getDtSolicInicio()));

		try {
			saidaListarSoliRelatorioOrgaosPublicos = getManterClientesOrgaoPublicoService()
					.listarSoliRelatorioOrgaosPublicos(entradaDto);
			setListaGridPesquisa(saidaListarSoliRelatorioOrgaosPublicos
					.getOcorrencias());

			this.listaControleRadio = new ArrayList<SelectItem>();
			for (int i = 0; i <= getListaGridPesquisa().size(); i++) {
				this.listaControleRadio.add(new SelectItem(i, " "));
			}

			setItemSelecionadoLista(null);

		} catch (PdcAdapterFunctionalException p) {
			if (!isFlagIncluir()) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
			}
		}

		setFlagIncluir(false);
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		limparCampos();
		getFiltroAgendamentoEfetivacaoEstornoBean().setSinalizaOrgaoPublico(
				true);
		setListaSaidaOrgaoPublico(new ArrayList<ListarParticipantesOrgaoOcorrencias>());
		setListaCtasVincPartOrgao(new ArrayList<ListarCtasVincPartOrgaoPublicoOcorrencias>());
	}

	/**
	 * Limpar args lista apos pesquisa cliente cliente.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void limparArgsListaAposPesquisaClienteCliente(ActionEvent evt) {
		limparCampos();
		getFiltroAgendamentoEfetivacaoEstornoBean().setSinalizaOrgaoPublico(
				true);
		setListaSaidaOrgaoPublico(new ArrayList<ListarParticipantesOrgaoOcorrencias>());
		setListaCtasVincPartOrgao(new ArrayList<ListarCtasVincPartOrgaoPublicoOcorrencias>());
	}

	/**
	 * Listar participantes orgao publico.
	 */
	public void listarParticipantesOrgaoPublico() {
		listaSaidaOrgaoPublico = new ArrayList<ListarParticipantesOrgaoOcorrencias>();
		ListarParticipantesOrgaoPublicoEntradaDTO entrada = new ListarParticipantesOrgaoPublicoEntradaDTO();

		// 0 - por cliente
		if (!"".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado())
				&& !"1".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getTipoFiltroSelecionado())) {
			entrada
					.setCdpessoaJuridicaContrato(PgitUtil
							.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getCdPessoaJuridicaContrato()));
			entrada
					.setCdTipoContratoNegocio(PgitUtil
							.verificaIntegerNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getCdTipoContratoNegocio()));
			entrada
					.setNrSequenciaContratoNegocio(PgitUtil
							.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getNrSequenciaContratoNegocio()));
		} else {
			entrada.setCdpessoaJuridicaContrato(0l);
			entrada.setNrSequenciaContratoNegocio(0l);
			entrada.setCdTipoContratoNegocio(0);
		}

		// 1 - por situacao
		if (!"".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado())
				&& !"0".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getTipoFiltroSelecionado())) {
			entrada.setCdSituacaoRegistro(PgitUtil.verificaIntegerNulo(
					getFiltroSituacaoCliente()).toString());
			entrada.setCdpessoaJuridicaContrato(0l);
			entrada.setNrSequenciaContratoNegocio(0l);
			entrada.setCdTipoContratoNegocio(0);

		} else {
			entrada.setCdSituacaoRegistro("0");
		}

		entrada.setNumeroOcorrencias(100);
		entrada.setCdPessoa(0l);

		try {
			saidaListarPartOrgaoPublico = getManterClientesOrgaoPublicoService()
					.listarParticipantesOrgaoPublico(entrada);

			if (Integer.valueOf(0).equals(
					saidaListarPartOrgaoPublico.getNumeroLinhas())) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ saidaListarPartOrgaoPublico.getCodMensagem() + ") "
						+ saidaListarPartOrgaoPublico.getMensagem(), false);
			} else {
				setListaSaidaOrgaoPublico(saidaListarPartOrgaoPublico
						.getOcorrencias());

				this.listaControleRadio = new ArrayList<SelectItem>();
				for (int i = 0; i <= listaSaidaOrgaoPublico.size(); i++) {
					this.listaControleRadio.add(new SelectItem(i, " "));
				}

				setItemSelecionadoPartOrgao(null);

			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Paginar orgao publico.
	 */
	public String paginarOrgaoPublico(ActionEvent evt) {
		listarParticipantesOrgaoPublico();
		return "ok";
	}

	/**
	 * Paginar ctas vinc part orgao.
	 */
	public void paginarCtasVincPartOrgao() {
		carregaListaCtasVincPartOrgaoPublico();
	}

	/**
	 * Avancar incluir.
	 * 
	 * @return the string
	 */
	public String avancarIncluir() {
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
				"incManterClientesOrgaoPublicoFederal");

		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		limparDadosIncluir();
		return "AVANCAR_INCLUIR";
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 */
	public void limparAposAlterarOpcaoCliente() {
		getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
		setListaCtasVincPartOrgao(new ArrayList<ListarCtasVincPartOrgaoPublicoOcorrencias>());
		limparCampos();
	}

	/**
	 * Detalhar participante orgao publico.
	 * 
	 * @return the string
	 */
	public String detalharParticipanteOrgaoPublico() {
		DetalharParticipantesOrgaoPublicoEntradaDTO entrada = new DetalharParticipantesOrgaoPublicoEntradaDTO();

		ListarParticipantesOrgaoOcorrencias registroSelecionado = getListaSaidaOrgaoPublico()
				.get(getItemSelecionadoPartOrgao());

		entrada.setCdPessoa(PgitUtil.verificaLongNulo(registroSelecionado
				.getCdPessoa()));
		entrada.setCdpessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getCdpessoaJuridicaContrato()));
		entrada.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(registroSelecionado
						.getCdTipoContratoNegocio()));
		entrada.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getNrSequenciaContratoNegocio()));
		entrada.setCdPessoaJuridicaVinculo(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getCdPessoaJuridicaVinculo()));
		entrada.setCdTipoContratoVinculo(PgitUtil
				.verificaIntegerNulo(registroSelecionado
						.getCdTipoContratoVinculo()));
		entrada.setNrSequenciaContratoVinculo(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getNrSequenciaContratoVinculo()));
		entrada.setCdTipoVincContrato(PgitUtil
				.verificaIntegerNulo(registroSelecionado
						.getCdTipoVincContrato()));

		try {
			saidaDetalharPartOrgaoPublico = getManterClientesOrgaoPublicoService()
					.detalharParticipanteOrgaoPublico(entrada);

			setNrSequenciaContratoNegocio(PgitUtil
					.verificaLongNulo(registroSelecionado
							.getNrSequenciaContratoNegocio()));
			setCpfCnpjFormatado(PgitUtil.verificaStringNula(registroSelecionado
					.getCpfCnpjFormatado()));
			setDsNomeRazaoSocial(PgitUtil
					.verificaStringNula(registroSelecionado
							.getDsNomeRazaoSocial()));
			setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(registroSelecionado
					.getCdAgenciaDebito()));
			setContaDigito(PgitUtil.verificaStringNula(registroSelecionado
					.getContaDigito()));
			setDsSituacaoRegistro(PgitUtil
					.verificaStringNula(registroSelecionado
							.getDsSituacaoRegistro()));
			setCodAgenciaDesc(PgitUtil.concatenarCampos(registroSelecionado
					.getCdAgenciaDebito(), registroSelecionado
					.getDsNomeAgenciaDebito(), "-"));

			saidaDetalharPartOrgaoPublico
					.setDataHoraInclusao("0"
							.equals(saidaDetalharPartOrgaoPublico
									.getDataHoraInclusao()) ? null
							: saidaDetalharPartOrgaoPublico
									.getDataHoraInclusao());
			saidaDetalharPartOrgaoPublico
					.setDataHoraManut("0".equals(saidaDetalharPartOrgaoPublico
							.getDataHoraManut()) ? null
							: saidaDetalharPartOrgaoPublico.getDataHoraManut());

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "AVANCAR_DETALHAR";
	}

	/**
	 * Confirmar excluir part orgao publico.
	 */
	public void confirmarExcluirPartOrgaoPublico() {
		ExcluirParticipantesOrgaoPublicoEntradaDTO entrada = new ExcluirParticipantesOrgaoPublicoEntradaDTO();
		ListarParticipantesOrgaoOcorrencias registroSelecionado = getListaSaidaOrgaoPublico()
				.get(getItemSelecionadoPartOrgao());

		entrada.setCdPessoa(PgitUtil.verificaLongNulo(registroSelecionado
				.getCdPessoa()));
		entrada.setCdpessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getCdpessoaJuridicaContrato()));
		entrada.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(registroSelecionado
						.getCdTipoContratoNegocio()));
		entrada.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getNrSequenciaContratoNegocio()));
		entrada.setCdPessoaJuridicaVinculo(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getCdPessoaJuridicaVinculo()));
		entrada.setCdTipoContratoVinculo(PgitUtil
				.verificaIntegerNulo(registroSelecionado
						.getCdTipoContratoVinculo()));
		entrada.setNrSequenciaContratoVinculo(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getNrSequenciaContratoVinculo()));
		entrada.setCdTipoVincContrato(PgitUtil
				.verificaIntegerNulo(registroSelecionado
						.getCdTipoVincContrato()));

		try {
			saidaExcluirPartOrgaoPublico = getManterClientesOrgaoPublicoService()
					.excluirParticipanteOrgaoPublico(entrada);
			PgitFacesUtils
					.addInfoModalMessage(saidaExcluirPartOrgaoPublico
							.getCodMensagem(), saidaExcluirPartOrgaoPublico
							.getMensagem(),
							"#{manterClientesOrgaoPublicoFederalBean.consultarAposExclusao}");

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return;
		}
	}

	/**
	 * Is desabilita consulta.
	 * 
	 * @return true, if is desabilita consulta
	 */
	public boolean isDesabilitaConsulta() {
		if ("0".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado())) {
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado() != null
					&& !"".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getSaidaConsultarListaClientePessoas()
							.getCnpjOuCpfFormatado())) {
				return false;
			}
		} else if ("1".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado())) {
			return false;
		}

		return true;
	}

	/**
	 * Is desabilita consulta incluir.
	 * 
	 * @return true, if is desabilita consulta incluir
	 */
	public boolean isDesabilitaConsultaIncluir() {
		if ("0".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado())) {
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado() != null
					&& !"".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getSaidaConsultarListaClientePessoas()
							.getCnpjOuCpfFormatado())) {
				return false;
			}
		} else if ("1".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado())) {
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getEmpresaGestoraDescContratoOrgao() != null
					&& !"".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getEmpresaGestoraDescContratoOrgao())) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Consultar apos exclusao.
	 * 
	 * @return the string
	 */
	public String consultarAposExclusao() {
		try {
			listarParticipantesOrgaoPublico();
			setItemSelecionadoPartOrgao(null);
		} catch (PdcAdapterFunctionalException e) {
		}

		return CON_MANTER_CLIENTES_ORGAO;
	}

	/**
	 * Confirmar alterar part orgao publico.
	 */
	public void confirmarAlterarPartOrgaoPublico() {
		AlterarParticipantesOrgaoPublicoEntradaDTO entrada = new AlterarParticipantesOrgaoPublicoEntradaDTO();

		ListarParticipantesOrgaoOcorrencias registroSelecionado = getListaSaidaOrgaoPublico()
				.get(getItemSelecionadoPartOrgao());

		entrada.setCdPessoa(PgitUtil.verificaLongNulo(registroSelecionado
				.getCdPessoa()));
		entrada.setCdpessoaJuridicaContrato(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getCdpessoaJuridicaContrato()));
		entrada.setCdTipoContratoNegocio(PgitUtil
				.verificaIntegerNulo(registroSelecionado
						.getCdTipoContratoNegocio()));
		entrada.setNrSequenciaContratoNegocio(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getNrSequenciaContratoNegocio()));
		entrada.setCdPessoaJuridicaVinculo(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getCdPessoaJuridicaVinculo()));
		entrada.setCdTipoContratoVinculo(PgitUtil
				.verificaIntegerNulo(registroSelecionado
						.getCdTipoContratoVinculo()));
		entrada.setNrSequenciaContratoVinculo(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getNrSequenciaContratoVinculo()));
		entrada.setCdTipoVincContrato(PgitUtil
				.verificaIntegerNulo(registroSelecionado
						.getCdTipoVincContrato()));
		entrada.setCdSituacaoRegistro(PgitUtil.verificaIntegerNulo(
				getFiltroSituacaoClienteAlterar()).toString());

		try {
			saidaAlterarPartOrgaoPublico = getManterClientesOrgaoPublicoService()
					.alterarParticipanteOrgaoPublico(entrada);
			PgitFacesUtils
					.addInfoModalMessage(saidaAlterarPartOrgaoPublico
							.getCodMensagem(), saidaAlterarPartOrgaoPublico
							.getMensagem(),
							"#{manterClientesOrgaoPublicoFederalBean.consultarAposExclusao}");

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return;
		}
	}

	/**
	 * Validar participante orgao publico.
	 */
	public boolean validarParticipanteOrgaoPublico() {
		ValidarContratoPagSalPartOrgaoPublicoEntradaDTO entrada = new ValidarContratoPagSalPartOrgaoPublicoEntradaDTO();

		// contrato
		if (!"".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado())
				&& !"0".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getTipoFiltroSelecionado())) {
			entrada
					.setCdpessoaJuridicaContrato(PgitUtil
							.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getEmpresaGestoraFiltro()));
			entrada
					.setCdTipoContratoNegocio(PgitUtil
							.verificaIntegerNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getTipoContratoFiltro()));
			entrada
					.setNrSequenciaContratoNegocio(Long
							.parseLong(PgitUtil
									.verificaStringNula(getFiltroAgendamentoEfetivacaoEstornoBean()
											.getNumeroFiltro())));
		} else {
			entrada
					.setCdpessoaJuridicaContrato(PgitUtil
							.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getCdPessoaJuridicaContrato()));
			entrada
					.setCdTipoContratoNegocio(PgitUtil
							.verificaIntegerNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getCdTipoContratoNegocio()));
			entrada
					.setNrSequenciaContratoNegocio(PgitUtil
							.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getNrSequenciaContratoNegocio()));
		}

		try {
			saidaValidarContratoPagSalPartOrgaoPublico = getManterClientesOrgaoPublicoService()
					.validarParticipanteOrgaoPublico(entrada);

			if (!"".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado())
					&& !"0".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getTipoFiltroSelecionado())) {
				if (!"PGIT0040"
						.equals(saidaValidarContratoPagSalPartOrgaoPublico
								.getCodMensagem())) {
					BradescoFacesUtils.addInfoModalMessage("("
							+ saidaValidarContratoPagSalPartOrgaoPublico
									.getCodMensagem()
							+ ") "
							+ saidaValidarContratoPagSalPartOrgaoPublico
									.getMensagem(), false);

					return false;
				}
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);

			return false;
		}

		return true;

	}

	/**
	 * Carrega lista ctas vinc part orgao publico.
	 */
	public void carregaListaCtasVincPartOrgaoPublico() {
		ListarCtasVincPartOrgaoPublicoEntradaDTO entrada = new ListarCtasVincPartOrgaoPublicoEntradaDTO();

		// contrato
		if (!"".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado())
				&& !"0".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getTipoFiltroSelecionado())) {
			entrada
					.setCdpessoaJuridicaContrato(PgitUtil
							.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getEmpresaGestoraFiltro()));
			entrada
					.setCdTipoContratoNegocio(PgitUtil
							.verificaIntegerNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getTipoContratoFiltro()));
			entrada
					.setNrSequenciaContratoNegocio(Long
							.parseLong(PgitUtil
									.verificaStringNula(getFiltroAgendamentoEfetivacaoEstornoBean()
											.getNumeroFiltro())));
			entrada.setNumeroOcorrencias(100);
		} else {
			entrada
					.setCdpessoaJuridicaContrato(PgitUtil
							.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getCdPessoaJuridicaContrato()));
			entrada
					.setCdTipoContratoNegocio(PgitUtil
							.verificaIntegerNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getCdTipoContratoNegocio()));
			entrada
					.setNrSequenciaContratoNegocio(PgitUtil
							.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getNrSequenciaContratoNegocio()));
			entrada.setNumeroOcorrencias(100);
		}

		try {
			saidaListarCtasVincPartOrgaoPublico = getManterClientesOrgaoPublicoService()
					.listarCtasVincPartOrgaoPublico(entrada);

			if (saidaListarCtasVincPartOrgaoPublico.getNumeroLinhas() != null
					&& saidaListarCtasVincPartOrgaoPublico.getNumeroLinhas() != 0) {
				setListaCtasVincPartOrgao(saidaListarCtasVincPartOrgaoPublico
						.getOcorrencias());
			} else {
				BradescoFacesUtils.addInfoModalMessage("("
						+ saidaListarCtasVincPartOrgaoPublico.getCodMensagem()
						+ ") "
						+ saidaListarCtasVincPartOrgaoPublico.getMensagem(),
						false);
			}

			this.listaControleRadio = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaCtasVincPartOrgao().size(); i++) {
				this.listaControleRadio.add(new SelectItem(i, " "));

				getListaCtasVincPartOrgao().get(i).setCdPessoaJuridicaVinculo(
						saidaListarCtasVincPartOrgaoPublico.getOcorrencias().get(i).getCdPessoaJuridicaVinculo());
				getListaCtasVincPartOrgao().get(i).setCdTipoContratoVinculo(
						saidaListarCtasVincPartOrgaoPublico.getOcorrencias().get(i).getCdTipoContratoVinculo());
				getListaCtasVincPartOrgao().get(i)
						.setNrSequenciaContratoVinculo(
								saidaListarCtasVincPartOrgaoPublico.getOcorrencias().get(i).getNrSequenciaContratoVinculo());
			}

			setItemSelecionadoListaCtas(null);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Confirmar incluir part orgao publico.
	 */
	public String confirmarIncluirPartOrgaoPublico() {
		IncluirParticipantesOrgaoPublicoEntradaDTO entrada = new IncluirParticipantesOrgaoPublicoEntradaDTO();
		ListarCtasVincPartOrgaoPublicoOcorrencias registroSelecionado = getListaCtasVincPartOrgao()
				.get(getItemSelecionadoListaCtas());

		if (!"".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado())
				&& !"0".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getTipoFiltroSelecionado())) {
			entrada
					.setCdpessoaJuridicaContrato(PgitUtil
							.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getEmpresaGestoraFiltro()));
			entrada
					.setCdTipoContratoNegocio(PgitUtil
							.verificaIntegerNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getTipoContratoFiltro()));
			entrada
					.setNrSequenciaContratoNegocio(Long
							.parseLong(PgitUtil
									.verificaStringNula(getFiltroAgendamentoEfetivacaoEstornoBean()
											.getNumeroFiltro())));
		} else {
			entrada
					.setCdpessoaJuridicaContrato(PgitUtil
							.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getCdPessoaJuridicaContrato()));
			entrada
					.setCdTipoContratoNegocio(PgitUtil
							.verificaIntegerNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getCdTipoContratoNegocio()));
			entrada
					.setNrSequenciaContratoNegocio(PgitUtil
							.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getNrSequenciaContratoNegocio()));
		}

		entrada.setCdPessoaJuridicaVinculo(PgitUtil
				.verificaLongNulo(registroSelecionado
						.getCdPessoaJuridicaVinculo()));
		entrada.setCdTipoContratoVinculo(PgitUtil
				.verificaIntegerNulo(registroSelecionado
						.getCdTipoContratoVinculo()));
		entrada.setNrSequenciaContratoVinculo(PgitUtil
				.verificaLongNulo(registroSelecionado.getNrSequenciaContratoVinculo()));
		entrada.setCdPessoa(PgitUtil.verificaLongNulo(registroSelecionado
				.getCdPessoa()));

		try {
			saidaIncluirPartOrgaoPublico = getManterClientesOrgaoPublicoService()
					.incluirParticipanteOrgaoPublico(entrada);
			BradescoFacesUtils.addInfoModalMessage("("
					+ saidaIncluirPartOrgaoPublico.getCodMensagem() + ") "
					+ saidaIncluirPartOrgaoPublico.getMensagem(),
					"VOLTAR_CONFIRMAR_INCLUIR",
					BradescoViewExceptionActionType.ACTION, false);
			setFlagIncluir(true);
			carregarLista();

			listaCtasVincPartOrgao.remove(listaCtasVincPartOrgao
					.get(itemSelecionadoListaCtas));
			
			setItemSelecionadoListaCtas(null);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return voltarConfirmarIncluir();
		}
		return null;
	}

	/**
	 * Confirmar excluir.
	 * 
	 * @return the string
	 */
	public String confirmarExcluir() {
		ExcluirSoliRelatorioOrgaosPublicosEntradaDTO entrada = new ExcluirSoliRelatorioOrgaosPublicosEntradaDTO();
		entrada.setCdSolicitacaoPagamento(listaGridPesquisa.get(
				itemSelecionadoLista).getCdSolicitacaoPagamento());
		entrada.setNrSolicitacao(listaGridPesquisa.get(itemSelecionadoLista)
				.getNrSolicitacao());

		try {
			saidaExcluirSoliRelatorioOrgaosPublicos = getManterClientesOrgaoPublicoService()
					.excluirSoliRelatorioOrgaosPublicos(entrada);
			BradescoFacesUtils.addInfoModalMessage("("
					+ saidaExcluirSoliRelatorioOrgaosPublicos.getCodMensagem()
					+ ") "
					+ saidaExcluirSoliRelatorioOrgaosPublicos.getMensagem(),
					false);
			carregarLista();

			return "CONFIRMAR_EXCLUIR";

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}
	}

	/**
	 * Incluir.
	 * 
	 * @return the string
	 */
	public String incluir() {
		try {
			ConsultarUsuarioSoliRelatorioOrgaosPublicosEntradaDTO entrada = new ConsultarUsuarioSoliRelatorioOrgaosPublicosEntradaDTO();
			entrada.setCdSituacaoVinculacaoConta(null);
			entrada.setNrOcorrencias(null);

			saidaConsultarUsuarioSoliRelatorioOrgaosPublicos = getManterClientesOrgaoPublicoService()
					.consultarUsuarioSoliRelatorioOrgaosPublicos(entrada);

			setUsuarioSolicitante(saidaConsultarUsuarioSoliRelatorioOrgaosPublicos
					.getCdUsuarioInclusao());
			setDataSolicitacao(saidaConsultarUsuarioSoliRelatorioOrgaosPublicos
					.getDtInclusao().replace('.', '/'));

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}

		return "INCLUIR";
	}

	/**
	 * Voltar confirmar alterar.
	 * 
	 * @return the string
	 */
	public String voltarConfirmarAlterar() {
		return "VOLTAR_CONFIRMAR_ALTERAR";
	}

	/**
	 * Voltar confirmar incluir.
	 * 
	 * @return the string
	 */
	public String voltarConfirmarIncluir() {
		return "VOLTAR_CONFIRMAR_INCLUIR";
	}

	/**
	 * Voltar incluir.
	 * 
	 * @return the string
	 */
	public String voltarIncluir() {
		limparIniciar();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
				"conManterClientesOrgaoPublicoFederal");
		return "VOLTAR_INCLUIR";
	}

	/**
	 * Voltar alterar.
	 * 
	 * @return the string
	 */
	public String voltarAlterar() {
		return "VOLTAR_ALTERAR";
	}

	/**
	 * Avancar tela confirmar inclusao.
	 * 
	 * @return the string
	 */
	public String avancarTelaConfirmarInclusao() {
		ListarCtasVincPartOrgaoPublicoOcorrencias registroSelecionado = getListaCtasVincPartOrgao()
				.get(getItemSelecionadoListaCtas());

		// Valida clientes que n�o possuem a modalidade de Cr�dito em Conta do
		// Servi�o Pagamento de Sal�rio
		if (!validarParticipanteOrgaoPublico()) {
			return "";
		}

		// contrato
		if (!"".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado())
				&& !"0".equals(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getTipoFiltroSelecionado())) {
			setNrSequenciaContratoNegocio(Long
					.parseLong(PgitUtil
							.verificaStringNula(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getNumeroFiltro())));
		} else {
			setNrSequenciaContratoNegocio(PgitUtil
					.verificaLongNulo(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrSequenciaContratoNegocio()));
		}

		setCpfCnpjFormatado(PgitUtil.verificaStringNula(registroSelecionado
				.getCpfCnpjFormatado()));
		setDsNomeRazaoSocial(PgitUtil.verificaStringNula(registroSelecionado
				.getDsNomeRazaoSocial()));
		setCdAgenciaDebito(PgitUtil.verificaIntegerNulo(registroSelecionado
				.getCdAgenciaDebito()));
		setContaDigito(PgitUtil.verificaStringNula(registroSelecionado
				.getContaDigito()));

		setCodAgenciaDesc(PgitUtil.concatenarCampos(registroSelecionado
				.getCdAgenciaDebito(), registroSelecionado
				.getDsNomeAgenciaDebito(), "-"));

		return "AVANCAR_CONFIRMAR_INCLUIR";
	}

	/**
	 * Avancar excluir.
	 * 
	 * @return the string
	 */
	public String avancarExcluir() {
		detalharParticipanteOrgaoPublico();
		return "AVANCAR_EXCLUIR";
	}

	/**
	 * Avancar alterar.
	 * 
	 * @return the string
	 */
	public String avancarAlterar() {
		detalharParticipanteOrgaoPublico();
		setFiltroSituacaoClienteAlterar(0);
		return "AVANCAR_ALTERAR";
	}

	/**
	 * Voltar detalhar.
	 * 
	 * @return the string
	 */
	public String voltarDetalhar() {
		return "VOLTAR_DETALHAR";
	}

	/**
	 * Voltar detalhar orgao.
	 * 
	 * @return the string
	 */
	public String voltarDetalharOrgao() {
		return "VOLTAR_DETALHAR";
	}

	/**
	 * Detalhar.
	 * 
	 * @return the string
	 */
	public String detalhar() {
		setUsuarioSolicitante(listaGridPesquisa.get(itemSelecionadoLista)
				.getCdUsuario());
		setDataSolicitacao(listaGridPesquisa.get(itemSelecionadoLista)
				.getDsDataSolicitacao().replace('.', '/'));
		return "DETALHAR";
	}

	/**
	 * Excluir.
	 * 
	 * @return the string
	 */
	public String excluir() {
		setUsuarioSolicitante(listaGridPesquisa.get(itemSelecionadoLista)
				.getCdUsuario());
		setDataSolicitacao(listaGridPesquisa.get(itemSelecionadoLista)
				.getDsDataSolicitacao().replace('.', '/'));
		return "EXCLUIR";
	}

	/**
	 * Voltar excluir.
	 * 
	 * @return the string
	 */
	public String voltarExcluir() {
		return "VOLTAR_EXCLUIR";
	}

	// GETTERS E SETTERS
	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#setComboService(br.com.bradesco.web.pgit.service.business.combo.IComboService)
	 */
	@Override
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#getComboService()
	 */
	@Override
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: manterCompTipoServicoFormaLancServiceImpl.
	 * 
	 * @param manterCompTipoServicoFormaLancServiceImpl
	 *            the manter comp tipo servico forma lanc service impl
	 */
	public void setManterCompTipoServicoFormaLancServiceImpl(
			IManterCompTipoServicoFormaLancService manterCompTipoServicoFormaLancServiceImpl) {
		this.manterCompTipoServicoFormaLancServiceImpl = manterCompTipoServicoFormaLancServiceImpl;
	}

	/**
	 * Get: manterCompTipoServicoFormaLancServiceImpl.
	 * 
	 * @return manterCompTipoServicoFormaLancServiceImpl
	 */
	public IManterCompTipoServicoFormaLancService getManterCompTipoServicoFormaLancServiceImpl() {
		return manterCompTipoServicoFormaLancServiceImpl;
	}

	/**
	 * Set: dtSolicInicio.
	 * 
	 * @param dtSolicInicio
	 *            the dt solic inicio
	 */
	public void setDtSolicInicio(Date dtSolicInicio) {
		this.dtSolicInicio = dtSolicInicio;
	}

	/**
	 * Get: dtSolicInicio.
	 * 
	 * @return dtSolicInicio
	 */
	public Date getDtSolicInicio() {
		return dtSolicInicio;
	}

	/**
	 * Set: dtSolicFinal.
	 * 
	 * @param dtSolicFinal
	 *            the dt solic final
	 */
	public void setDtSolicFinal(Date dtSolicFinal) {
		this.dtSolicFinal = dtSolicFinal;
	}

	/**
	 * Get: dtSolicFinal.
	 * 
	 * @return dtSolicFinal
	 */
	public Date getDtSolicFinal() {
		return dtSolicFinal;
	}

	/**
	 * Set: itemSelecionadoLista.
	 * 
	 * @param itemSelecionadoLista
	 *            the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: itemSelecionadoLista.
	 * 
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: listaControleRadio.
	 * 
	 * @param listaControleRadio
	 *            the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaControleRadio.
	 * 
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: manterClientesOrgaoPublicoService.
	 * 
	 * @param manterClientesOrgaoPublicoService
	 *            the manter clientes orgao publico service
	 */
	public void setManterClientesOrgaoPublicoService(
			IManterClientesOrgaoPublicoService manterClientesOrgaoPublicoService) {
		this.manterClientesOrgaoPublicoService = manterClientesOrgaoPublicoService;
	}

	/**
	 * Get: manterClientesOrgaoPublicoService.
	 * 
	 * @return manterClientesOrgaoPublicoService
	 */
	public IManterClientesOrgaoPublicoService getManterClientesOrgaoPublicoService() {
		return manterClientesOrgaoPublicoService;
	}

	/**
	 * Set: saidaListarSoliRelatorioOrgaosPublicos.
	 * 
	 * @param saidaListarSoliRelatorioOrgaosPublicos
	 *            the saida listar soli relatorio orgaos publicos
	 */
	public void setSaidaListarSoliRelatorioOrgaosPublicos(
			ListarSoliRelatorioOrgaosPublicosSaidaDTO saidaListarSoliRelatorioOrgaosPublicos) {
		this.saidaListarSoliRelatorioOrgaosPublicos = saidaListarSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Get: saidaListarSoliRelatorioOrgaosPublicos.
	 * 
	 * @return saidaListarSoliRelatorioOrgaosPublicos
	 */
	public ListarSoliRelatorioOrgaosPublicosSaidaDTO getSaidaListarSoliRelatorioOrgaosPublicos() {
		return saidaListarSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Set: listaGridPesquisa.
	 * 
	 * @param listaGridPesquisa
	 *            the lista grid pesquisa
	 */
	public void setListaGridPesquisa(
			List<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: listaGridPesquisa.
	 * 
	 * @return listaGridPesquisa
	 */
	public List<ListarSoliRelatorioOrgaosPublicosOcorrSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: usuarioSolicitante.
	 * 
	 * @param usuarioSolicitante
	 *            the usuario solicitante
	 */
	public void setUsuarioSolicitante(String usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}

	/**
	 * Get: usuarioSolicitante.
	 * 
	 * @return usuarioSolicitante
	 */
	public String getUsuarioSolicitante() {
		return usuarioSolicitante;
	}

	/**
	 * Set: dataSolicitacao.
	 * 
	 * @param dataSolicitacao
	 *            the data solicitacao
	 */
	public void setDataSolicitacao(String dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	/**
	 * Get: dataSolicitacao.
	 * 
	 * @return dataSolicitacao
	 */
	public String getDataSolicitacao() {
		return dataSolicitacao;
	}

	/**
	 * Set: saidaExcluirSoliRelatorioOrgaosPublicos.
	 * 
	 * @param saidaExcluirSoliRelatorioOrgaosPublicos
	 *            the saida excluir soli relatorio orgaos publicos
	 */
	public void setSaidaExcluirSoliRelatorioOrgaosPublicos(
			ExcluirSoliRelatorioOrgaosPublicosSaidaDTO saidaExcluirSoliRelatorioOrgaosPublicos) {
		this.saidaExcluirSoliRelatorioOrgaosPublicos = saidaExcluirSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Get: saidaExcluirSoliRelatorioOrgaosPublicos.
	 * 
	 * @return saidaExcluirSoliRelatorioOrgaosPublicos
	 */
	public ExcluirSoliRelatorioOrgaosPublicosSaidaDTO getSaidaExcluirSoliRelatorioOrgaosPublicos() {
		return saidaExcluirSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Set: saidaConsultarUsuarioSoliRelatorioOrgaosPublicos.
	 * 
	 * @param saidaConsultarUsuarioSoliRelatorioOrgaosPublicos
	 *            the saida consultar usuario soli relatorio orgaos publicos
	 */
	public void setSaidaConsultarUsuarioSoliRelatorioOrgaosPublicos(
			ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO saidaConsultarUsuarioSoliRelatorioOrgaosPublicos) {
		this.saidaConsultarUsuarioSoliRelatorioOrgaosPublicos = saidaConsultarUsuarioSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Get: saidaConsultarUsuarioSoliRelatorioOrgaosPublicos.
	 * 
	 * @return saidaConsultarUsuarioSoliRelatorioOrgaosPublicos
	 */
	public ConsultarUsuarioSoliRelatorioOrgaosPublicosSaidaDTO getSaidaConsultarUsuarioSoliRelatorioOrgaosPublicos() {
		return saidaConsultarUsuarioSoliRelatorioOrgaosPublicos;
	}

	/**
	 * Get: saidaListarPartOrgaoPublico.
	 * 
	 * @return saidaListarPartOrgaoPublico
	 */
	public ListarParticipantesOrgaoPublicoSaidaDTO getSaidaListarPartOrgaoPublico() {
		return saidaListarPartOrgaoPublico;
	}

	/**
	 * Set: saidaListarPartOrgaoPublico.
	 * 
	 * @param saidaListarPartOrgaoPublico
	 *            the saida listar part orgao publico
	 */
	public void setSaidaListarPartOrgaoPublico(
			ListarParticipantesOrgaoPublicoSaidaDTO saidaListarPartOrgaoPublico) {
		this.saidaListarPartOrgaoPublico = saidaListarPartOrgaoPublico;
	}

	/**
	 * Get: saidaDetalharPartOrgaoPublico.
	 * 
	 * @return saidaDetalharPartOrgaoPublico
	 */
	public DetalharParticipantesOrgaoPublicoSaidaDTO getSaidaDetalharPartOrgaoPublico() {
		return saidaDetalharPartOrgaoPublico;
	}

	/**
	 * Set: saidaDetalharPartOrgaoPublico.
	 * 
	 * @param saidaDetalharPartOrgaoPublico
	 *            the saida detalhar part orgao publico
	 */
	public void setSaidaDetalharPartOrgaoPublico(
			DetalharParticipantesOrgaoPublicoSaidaDTO saidaDetalharPartOrgaoPublico) {
		this.saidaDetalharPartOrgaoPublico = saidaDetalharPartOrgaoPublico;
	}

	/**
	 * Get: saidaExcluirPartOrgaoPublico.
	 * 
	 * @return saidaExcluirPartOrgaoPublico
	 */
	public ExcluirParticipantesOrgaoPublicoSaidaDTO getSaidaExcluirPartOrgaoPublico() {
		return saidaExcluirPartOrgaoPublico;
	}

	/**
	 * Set: saidaExcluirPartOrgaoPublico.
	 * 
	 * @param saidaExcluirPartOrgaoPublico
	 *            the saida excluir part orgao publico
	 */
	public void setSaidaExcluirPartOrgaoPublico(
			ExcluirParticipantesOrgaoPublicoSaidaDTO saidaExcluirPartOrgaoPublico) {
		this.saidaExcluirPartOrgaoPublico = saidaExcluirPartOrgaoPublico;
	}

	/**
	 * Get: saidaAlterarPartOrgaoPublico.
	 * 
	 * @return saidaAlterarPartOrgaoPublico
	 */
	public AlterarParticipantesOrgaoPublicoSaidaDTO getSaidaAlterarPartOrgaoPublico() {
		return saidaAlterarPartOrgaoPublico;
	}

	/**
	 * Set: saidaAlterarPartOrgaoPublico.
	 * 
	 * @param saidaAlterarPartOrgaoPublico
	 *            the saida alterar part orgao publico
	 */
	public void setSaidaAlterarPartOrgaoPublico(
			AlterarParticipantesOrgaoPublicoSaidaDTO saidaAlterarPartOrgaoPublico) {
		this.saidaAlterarPartOrgaoPublico = saidaAlterarPartOrgaoPublico;
	}

	/**
	 * Get: saidaValidarContratoPagSalPartOrgaoPublico.
	 * 
	 * @return saidaValidarContratoPagSalPartOrgaoPublico
	 */
	public ValidarContratoPagSalPartOrgaoPublicoSaidaDTO getSaidaValidarContratoPagSalPartOrgaoPublico() {
		return saidaValidarContratoPagSalPartOrgaoPublico;
	}

	/**
	 * Set: saidaValidarContratoPagSalPartOrgaoPublico.
	 * 
	 * @param saidaValidarContratoPagSalPartOrgaoPublico
	 *            the saida validar contrato pag sal part orgao publico
	 */
	public void setSaidaValidarContratoPagSalPartOrgaoPublico(
			ValidarContratoPagSalPartOrgaoPublicoSaidaDTO saidaValidarContratoPagSalPartOrgaoPublico) {
		this.saidaValidarContratoPagSalPartOrgaoPublico = saidaValidarContratoPagSalPartOrgaoPublico;
	}

	/**
	 * Get: saidaListarCtasVincPartOrgaoPublico.
	 * 
	 * @return saidaListarCtasVincPartOrgaoPublico
	 */
	public ListarCtasVincPartOrgaoPublicoSaidaDTO getSaidaListarCtasVincPartOrgaoPublico() {
		return saidaListarCtasVincPartOrgaoPublico;
	}

	/**
	 * Set: saidaListarCtasVincPartOrgaoPublico.
	 * 
	 * @param saidaListarCtasVincPartOrgaoPublico
	 *            the saida listar ctas vinc part orgao publico
	 */
	public void setSaidaListarCtasVincPartOrgaoPublico(
			ListarCtasVincPartOrgaoPublicoSaidaDTO saidaListarCtasVincPartOrgaoPublico) {
		this.saidaListarCtasVincPartOrgaoPublico = saidaListarCtasVincPartOrgaoPublico;
	}

	/**
	 * Get: itemSelecionadoListaCtas.
	 * 
	 * @return itemSelecionadoListaCtas
	 */
	public Integer getItemSelecionadoListaCtas() {
		return itemSelecionadoListaCtas;
	}

	/**
	 * Set: itemSelecionadoListaCtas.
	 * 
	 * @param itemSelecionadoListaCtas
	 *            the item selecionado lista ctas
	 */
	public void setItemSelecionadoListaCtas(Integer itemSelecionadoListaCtas) {
		this.itemSelecionadoListaCtas = itemSelecionadoListaCtas;
	}

	/**
	 * Get: listaCtasVincPartOrgao.
	 * 
	 * @return listaCtasVincPartOrgao
	 */
	public List<ListarCtasVincPartOrgaoPublicoOcorrencias> getListaCtasVincPartOrgao() {
		return listaCtasVincPartOrgao;
	}

	/**
	 * Set: listaCtasVincPartOrgao.
	 * 
	 * @param listaCtasVincPartOrgao
	 *            the lista ctas vinc part orgao
	 */
	public void setListaCtasVincPartOrgao(
			List<ListarCtasVincPartOrgaoPublicoOcorrencias> listaCtasVincPartOrgao) {
		this.listaCtasVincPartOrgao = listaCtasVincPartOrgao;
	}

	/**
	 * Get: saidaIncluirPartOrgaoPublico.
	 * 
	 * @return saidaIncluirPartOrgaoPublico
	 */
	public IncluirParticipantesOrgaoPublicoSaidaDTO getSaidaIncluirPartOrgaoPublico() {
		return saidaIncluirPartOrgaoPublico;
	}

	/**
	 * Set: saidaIncluirPartOrgaoPublico.
	 * 
	 * @param saidaIncluirPartOrgaoPublico
	 *            the saida incluir part orgao publico
	 */
	public void setSaidaIncluirPartOrgaoPublico(
			IncluirParticipantesOrgaoPublicoSaidaDTO saidaIncluirPartOrgaoPublico) {
		this.saidaIncluirPartOrgaoPublico = saidaIncluirPartOrgaoPublico;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#getFiltroAgendamentoEfetivacaoEstornoBean()
	 */
	@Override
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#setFiltroAgendamentoEfetivacaoEstornoBean(br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean)
	 */
	@Override
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Is disable argumentos consulta.
	 * 
	 * @return true, if is disable argumentos consulta
	 */
	@Override
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 * 
	 * @param disableArgumentosConsulta
	 *            the disable argumentos consulta
	 */
	@Override
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: filtroSituacaoCliente.
	 * 
	 * @return filtroSituacaoCliente
	 */
	public Integer getFiltroSituacaoCliente() {
		return filtroSituacaoCliente;
	}

	/**
	 * Set: filtroSituacaoCliente.
	 * 
	 * @param filtroSituacaoCliente
	 *            the filtro situacao cliente
	 */
	public void setFiltroSituacaoCliente(Integer filtroSituacaoCliente) {
		this.filtroSituacaoCliente = filtroSituacaoCliente;
	}

	/**
	 * Get: itemSelecionadoPartOrgao.
	 * 
	 * @return itemSelecionadoPartOrgao
	 */
	public Integer getItemSelecionadoPartOrgao() {
		return itemSelecionadoPartOrgao;
	}

	/**
	 * Set: itemSelecionadoPartOrgao.
	 * 
	 * @param itemSelecionadoPartOrgao
	 *            the item selecionado part orgao
	 */
	public void setItemSelecionadoPartOrgao(Integer itemSelecionadoPartOrgao) {
		this.itemSelecionadoPartOrgao = itemSelecionadoPartOrgao;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 * 
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 * 
	 * @param nrSequenciaContratoNegocio
	 *            the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: dsNomeRazaoSocial.
	 * 
	 * @return dsNomeRazaoSocial
	 */
	public String getDsNomeRazaoSocial() {
		return dsNomeRazaoSocial;
	}

	/**
	 * Set: dsNomeRazaoSocial.
	 * 
	 * @param dsNomeRazaoSocial
	 *            the ds nome razao social
	 */
	public void setDsNomeRazaoSocial(String dsNomeRazaoSocial) {
		this.dsNomeRazaoSocial = dsNomeRazaoSocial;
	}

	/**
	 * Get: cpfCnpjFormatado.
	 * 
	 * @return cpfCnpjFormatado
	 */
	public String getCpfCnpjFormatado() {
		return cpfCnpjFormatado;
	}

	/**
	 * Set: cpfCnpjFormatado.
	 * 
	 * @param cpfCnpjFormatado
	 *            the cpf cnpj formatado
	 */
	public void setCpfCnpjFormatado(String cpfCnpjFormatado) {
		this.cpfCnpjFormatado = cpfCnpjFormatado;
	}

	/**
	 * Get: cdAgenciaDebito.
	 * 
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	/**
	 * Set: cdAgenciaDebito.
	 * 
	 * @param cdAgenciaDebito
	 *            the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	/**
	 * Get: contaDigito.
	 * 
	 * @return contaDigito
	 */
	public String getContaDigito() {
		return contaDigito;
	}

	/**
	 * Set: contaDigito.
	 * 
	 * @param contaDigito
	 *            the conta digito
	 */
	public void setContaDigito(String contaDigito) {
		this.contaDigito = contaDigito;
	}

	/**
	 * Get: dsSituacaoRegistro.
	 * 
	 * @return dsSituacaoRegistro
	 */
	public String getDsSituacaoRegistro() {
		return dsSituacaoRegistro;
	}

	/**
	 * Set: dsSituacaoRegistro.
	 * 
	 * @param dsSituacaoRegistro
	 *            the ds situacao registro
	 */
	public void setDsSituacaoRegistro(String dsSituacaoRegistro) {
		this.dsSituacaoRegistro = dsSituacaoRegistro;
	}

	/**
	 * Get: listaSaidaOrgaoPublico.
	 * 
	 * @return listaSaidaOrgaoPublico
	 */
	public List<ListarParticipantesOrgaoOcorrencias> getListaSaidaOrgaoPublico() {
		return listaSaidaOrgaoPublico;
	}

	/**
	 * Set: listaSaidaOrgaoPublico.
	 * 
	 * @param listaSaidaOrgaoPublico
	 *            the lista saida orgao publico
	 */
	public void setListaSaidaOrgaoPublico(
			List<ListarParticipantesOrgaoOcorrencias> listaSaidaOrgaoPublico) {
		this.listaSaidaOrgaoPublico = listaSaidaOrgaoPublico;
	}

	/**
	 * Get: codAgenciaDesc.
	 * 
	 * @return codAgenciaDesc
	 */
	public String getCodAgenciaDesc() {
		return codAgenciaDesc;
	}

	/**
	 * Set: codAgenciaDesc.
	 * 
	 * @param codAgenciaDesc
	 *            the cod agencia desc
	 */
	public void setCodAgenciaDesc(String codAgenciaDesc) {
		this.codAgenciaDesc = codAgenciaDesc;
	}

	/**
	 * Get: filtroSituacaoClienteAlterar.
	 * 
	 * @return filtroSituacaoClienteAlterar
	 */
	public Integer getFiltroSituacaoClienteAlterar() {
		return filtroSituacaoClienteAlterar;
	}

	/**
	 * Set: filtroSituacaoClienteAlterar.
	 * 
	 * @param filtroSituacaoClienteAlterar
	 *            the filtro situacao cliente alterar
	 */
	public void setFiltroSituacaoClienteAlterar(
			Integer filtroSituacaoClienteAlterar) {
		this.filtroSituacaoClienteAlterar = filtroSituacaoClienteAlterar;
	}

	/**
	 * Is flag incluir.
	 * 
	 * @return true, if is flag incluir
	 */
	public boolean isFlagIncluir() {
		return flagIncluir;
	}

	/**
	 * Set: flagIncluir.
	 * 
	 * @param flagIncluir
	 *            the flag incluir
	 */
	public void setFlagIncluir(boolean flagIncluir) {
		this.flagIncluir = flagIncluir;
	}

}
