/*
 * Nome: br.com.bradesco.web.pgit.view.bean.geracaoarquivosretorno.mantercontroleultimoretornogerado
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.geracaoarquivosretorno.mantercontroleultimoretornogerado;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarAssLoteLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarAssLoteLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.IManterControleUltimoRetornoGeradoService;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.AlterarUltimoNumeroRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.AlterarUltimoNumeroRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ConsultarUltimoNumeroRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ConsultarUltimoNumeroRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ListarUltimoNumeroRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontroleultimoretornogerado.bean.ListarUltimoNumeroRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;

/**
 * Nome: ManterControleUltimoRetornoGeradoBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ManterControleUltimoRetornoGeradoBean {

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;

	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo manterSolicitacaoRastFavService. */
	private IManterSolicitacaoRastFavService manterSolicitacaoRastFavService;

	/** Atributo manterControleUltimoRetornoGeradoServiceImpl. */
	private IManterControleUltimoRetornoGeradoService manterControleUltimoRetornoGeradoServiceImpl;

	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();

	/** Atributo listaTipoLote. */
	private List<SelectItem> listaTipoLote = new ArrayList<SelectItem>();

	/** Atributo layoutFiltro. */
	private Integer layoutFiltro;

	/** Atributo tipoLoteFiltro. */
	private Integer tipoLoteFiltro;

	/** Atributo dadosRemessa. */
	private ConsultarUltimoNumeroRetornoSaidaDTO dadosRemessa;

	/** Atributo listaGrid. */
	private List<ListarUltimoNumeroRetornoSaidaDTO> listaGrid;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;

	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;

	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;

	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;

	/** Atributo dsEmpresa. */
	private String dsEmpresa;

	/** Atributo nroContrato. */
	private String nroContrato;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo cdPerfilTrocaArquivoPerfil. */
	private Long cdPerfilTrocaArquivoPerfil;

	/** Atributo dsSituacaoPerfil. */
	private String dsSituacaoPerfil;

	// Variaveis Detalhar
	/** Atributo dsNivelControle. */
	private String dsNivelControle;

	/** Atributo dsTipoControle. */
	private String dsTipoControle;

	/** Atributo cdTipoLayoutArquivo. */
	private String cdTipoLayoutArquivo;

	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;

	/** Atributo cdTipoLoteLayout. */
	private String cdTipoLoteLayout;

	/** Atributo dsTipoLoteLayout. */
	private String dsTipoLoteLayout;

	/** Atributo nrUltimoNumeroArquivoRemessa. */
	private String nrUltimoNumeroArquivoRemessa;

	/** Atributo nrMaximoArquivoRemessa. */
	private String nrMaximoArquivoRemessa;

	private String cdCorpoCpfCnpj;

	private Long cdClub;

	/** Atributo consultasService. */
	private IConsultasService consultasService;

	/**
	 * Consultar.
	 */
	public void consultar() {
		setItemSelecionadoLista(null);

		listaGrid = new ArrayList<ListarUltimoNumeroRetornoSaidaDTO>();

		try {
			ListarUltimoNumeroRetornoEntradaDTO entrada = new ListarUltimoNumeroRetornoEntradaDTO();

			entrada
					.setCdPerfilTrocaArquivo(filtroAgendamentoEfetivacaoEstornoBean
							.getTipoFiltroSelecionado().equals("0") ? filtroAgendamentoEfetivacaoEstornoBean
							.getCdPerfilTrocaArquivoPerfilCliente()
							: filtroAgendamentoEfetivacaoEstornoBean
									.getCdPerfilTrocaArquivoPerfilContrato());
			entrada.setCdTipoLayoutArquivo(getLayoutFiltro());
			entrada.setCdTipoLoteLayout(getTipoLoteFiltro());

			setListaGrid(getManterControleUltimoRetornoGeradoServiceImpl()
					.listarUltimoNumeroRetorno(entrada));

			listaControleRadio = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGrid().size(); i++) {
				this.listaControleRadio.add(new SelectItem(i, " "));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setDisableArgumentosConsulta(false);
		}
	}

	/**
	 * Detalhar.
	 * 
	 * @return the string
	 */
	public String detalhar() {
		carregarCabecalho();
		try {
			ConsultarUltimoNumeroRetornoEntradaDTO entrada = new ConsultarUltimoNumeroRetornoEntradaDTO();

			entrada.setCdPerfilTrocaArquivo(getListaGrid().get(
					getItemSelecionadoLista()).getCdPerfilTrocaArquivo());
			entrada.setCdTipoLayoutArquivo(getListaGrid().get(
					getItemSelecionadoLista()).getCdTipoLayoutArquivo());
			entrada.setCdTipoLoteLayout(getListaGrid().get(
					getItemSelecionadoLista()).getCdTipoLoteLayout());
			entrada.setCdClub(getListaGrid().get(
					getItemSelecionadoLista()).getCdClub());

			dadosRemessa = getManterControleUltimoRetornoGeradoServiceImpl()
					.consultarUltimoNumeroRetorno(entrada);

			setDsNivelControle(dadosRemessa.getDsNivelControle());
			setDsTipoControle(dadosRemessa.getDsTipoControle());
			setCdTipoLayoutArquivo(getCdTipoLayoutArquivo());
			setDsTipoLayoutArquivo(dadosRemessa.getDsTipoLayoutArquivo());
			setCdTipoLoteLayout(String.valueOf(dadosRemessa
					.getCdTipoLoteLayout()));
			setDsTipoLoteLayout(dadosRemessa.getDsTipoLoteLayout());
			setNrUltimoNumeroArquivoRemessa(String.valueOf(dadosRemessa
					.getNrUltimoArquivoRemessa()));
			setNrMaximoArquivoRemessa(String.valueOf(dadosRemessa
					.getNrMaximoArquivoRemessa()));
			setCdCorpoCpfCnpj(dadosRemessa.getCdCorpoCpfCnpj());
			setCdClub(getListaGrid().get(getItemSelecionadoLista()).getCdClub());

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
		return "DETALHAR";
	}

	/**
	 * Voltar alterar.
	 * 
	 * @return the string
	 */
	public String voltarAlterar() {
		return "VOLTAR_DETALHE";
	}

	/**
	 * Confirma paginacao.
	 * 
	 * @return the string
	 */
	public String confirmaPaginacao() {
		return "CONFIRMAR";
	}

	/**
	 * Voltar consultar.
	 * 
	 * @return the string
	 */
	public String voltarConsultar() {
		setItemSelecionadoLista(null);

		return "VOLTAR_CONSULTA";
	}

	/**
	 * Confirmar.
	 * 
	 * @return the string
	 */
	public String confirmar() {
		try {
			AlterarUltimoNumeroRetornoEntradaDTO entrada = new AlterarUltimoNumeroRetornoEntradaDTO();
			entrada
					.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdPessoaJuridicaContrato());
			entrada
					.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdTipoContratoNegocio());
			entrada
					.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrSequenciaContratoNegocio());
			entrada.setCdTipoLayoutArquivo(getDadosRemessa()
					.getCdTipoLayoutArquivo());
			entrada.setCdPerfilTrocaArquivo(getCdPerfilTrocaArquivoPerfil());
			entrada
					.setCdTipoLoteLayout(getDadosRemessa()
							.getCdTipoLoteLayout());
			entrada.setNrUltimoArquivo(new Long(
					getNrUltimoNumeroArquivoRemessa()));
			entrada.setNrMaximoArquivo(getDadosRemessa()
					.getNrMaximoArquivoRemessa());
			entrada.setCdClub(getCdClub());

			AlterarUltimoNumeroRetornoSaidaDTO saida = getManterControleUltimoRetornoGeradoServiceImpl()
					.alterarUltimoNumeroRetorno(entrada);
			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(),
					"conManterControleUltimoRetornoGerado",
					BradescoViewExceptionActionType.ACTION, false);
			consultar();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "";
	}

	/**
	 * Limpar variaveis.
	 */
	public void limparVariaveis() {
		setCdPessoaJuridicaContrato(null);
		setCdTipoContratoNegocio(null);
		setNrSequenciaContratoNegocio(null);
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");
		setCdPerfilTrocaArquivoPerfil(null);
		setDsSituacaoPerfil("");
		setCdClub(null);
	}

	/**
	 * Iniciar tela.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		limpar();
		// Tela de Filtro
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean
				.setPaginaRetorno("conManterControleUltimoRetornoGerado");
		filtroAgendamentoEfetivacaoEstornoBean
				.setClienteContratoSelecionado(false);
		filtroAgendamentoEfetivacaoEstornoBean
				.setEmpresaGestoraFiltro(2269651L);

		listarTipoLayout();
		setDisableArgumentosConsulta(false);
	}

	/**
	 * Limpar.
	 * 
	 * @return the string
	 */
	public String limpar() {
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();

		setListaGrid(null);
		setItemSelecionadoLista(null);
		setLayoutFiltro(null);
		setTipoLoteFiltro(null);

		setListaTipoLote(new ArrayList<SelectItem>());

		return "";
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 * 
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		setListaGrid(null);
		setItemSelecionadoLista(null);
		setLayoutFiltro(null);
		setTipoLoteFiltro(null);
		setListaTipoLote(new ArrayList<SelectItem>());

		return "";
	}

	/**
	 * Limpar botao.
	 * 
	 * @return the string
	 */
	public String limparBotao() {
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);
		setListaGrid(null);
		setItemSelecionadoLista(null);
		setLayoutFiltro(null);
		setTipoLoteFiltro(null);
		setDisableArgumentosConsulta(false);
		setListaTipoLote(new ArrayList<SelectItem>());
		return "";
	}

	/**
	 * Limpar grid consultar.
	 * 
	 * @return the string
	 */
	public String limparGridConsultar() {
		setListaGrid(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Listar tipo layout.
	 */
	public void listarTipoLayout() {
		try {
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
			List<TipoLayoutArquivoSaidaDTO> listaTipoLayout = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			TipoLayoutArquivoEntradaDTO tipoLoteEntradaDTO = new TipoLayoutArquivoEntradaDTO();
			tipoLoteEntradaDTO.setCdSituacaoVinculacaoConta(0);

			listaTipoLayout = getComboService().listarTipoLayoutArquivoCem(
					tipoLoteEntradaDTO);

			for (TipoLayoutArquivoSaidaDTO combo : listaTipoLayout) {
				listaTipoLayoutArquivo.add(new SelectItem(combo
						.getCdTipoLayoutArquivo(), combo
						.getDsTipoLayoutArquivo()));
			}
		} catch (PdcAdapterFunctionalException p) {
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar tipo lote.
	 */
	public void listarTipoLote() {
		try {
			listaTipoLote = new ArrayList<SelectItem>();
			if (getLayoutFiltro() != null && getLayoutFiltro() != 0) {
				List<ListarAssLoteLayoutArquivoSaidaDTO> listaTipoLoteAux = new ArrayList<ListarAssLoteLayoutArquivoSaidaDTO>();
				ListarAssLoteLayoutArquivoEntradaDTO entrada = new ListarAssLoteLayoutArquivoEntradaDTO();
				entrada.setCdTipoLayoutArquivo(getLayoutFiltro());
				entrada.setCdTipoLoteLayout(0);

				listaTipoLoteAux = getComboService()
						.listarAssLoteLayoutArquivo(entrada);

				for (ListarAssLoteLayoutArquivoSaidaDTO combo : listaTipoLoteAux) {
					listaTipoLote
							.add(new SelectItem(combo.getCdTipoLoteLayout(),
									combo.getDsTipoLoteLayout()));
				}
			}
		} catch (PdcAdapterFunctionalException p) {
			listaTipoLote = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Limpar cliente.
	 */
	public void limparCliente() {
		filtroAgendamentoEfetivacaoEstornoBean.limparCamposCliente();
		setListaGrid(null);
		setLayoutFiltro(null);
		setTipoLoteFiltro(null);
		setItemSelecionadoLista(null);
		setListaTipoLote(new ArrayList<SelectItem>());
	}

	/**
	 * Limpar evento.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void limparEvento(ActionEvent evt) {
		setListaGrid(null);
		setLayoutFiltro(null);
		setTipoLoteFiltro(null);
		setItemSelecionadoLista(null);

		setListaTipoLote(new ArrayList<SelectItem>());
	}

	/**
	 * Carregar cabecalho.
	 */
	public void carregarCabecalho() {
		limparVariaveis();
		setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getCdPessoaJuridicaContrato());
		setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getCdTipoContratoNegocio());
		setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
				.getNrSequenciaContratoNegocio());

		if (filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado()
				.equals("0")) {
			setNrCnpjCpf(filtroAgendamentoEfetivacaoEstornoBean
					.getNrCpfCnpjCliente());
			setDsRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoRazaoSocialCliente());

			setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
					.getEmpresaGestoraDescCliente());
			setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getNumeroDescCliente());
			setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoContratoDescCliente());
			setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getSituacaoDescCliente());

			setCdPerfilTrocaArquivoPerfil(filtroAgendamentoEfetivacaoEstornoBean
					.getCdPerfilTrocaArquivoPerfilCliente());
			setDsSituacaoPerfil(filtroAgendamentoEfetivacaoEstornoBean
					.getDsSituacaoPerfilCliente());

		} else {
			try {
				DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
				entradaDTO
						.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
								.getEmpresaGestoraFiltro());
				entradaDTO
						.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
								.getCdTipoContratoNegocio());
				entradaDTO
						.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
								.getNrSequenciaContratoNegocio());
				DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService()
						.detalharDadosContrato(entradaDTO);
				setNrCnpjCpf(saidaDTO.getCdCnpjCpfTitular());
				setDsRazaoSocial(saidaDTO.getDsParticipanteTitular());
			} catch (PdcAdapterFunctionalException p) {
				setNrCnpjCpf("");
				setDsRazaoSocial("");
			}
			setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
					.getEmpresaGestoraDescContrato());
			setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getNumeroDescContrato());
			setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoContratoDescContrato());
			setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getSituacaoDescContrato());

			setCdPerfilTrocaArquivoPerfil(filtroAgendamentoEfetivacaoEstornoBean
					.getCdPerfilTrocaArquivoPerfilContrato());
			setDsSituacaoPerfil(filtroAgendamentoEfetivacaoEstornoBean
					.getDsSituacaoPerfilContrato());
		}

	}

	/**
	 * Submit pesquisar.
	 * 
	 * @param evt
	 *            the evt
	 * @return the string
	 */
	public String submitPesquisar(ActionEvent evt) {
		consultar();
		return "";
	}

	/**
	 * Get: comboService.
	 * 
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 * 
	 * @param comboService
	 *            the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 * 
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 * 
	 * @param filtroAgendamentoEfetivacaoEstornoBean
	 *            the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Get: layoutFiltro.
	 * 
	 * @return layoutFiltro
	 */
	public Integer getLayoutFiltro() {
		return layoutFiltro;
	}

	/**
	 * Set: layoutFiltro.
	 * 
	 * @param layoutFiltro
	 *            the layout filtro
	 */
	public void setLayoutFiltro(Integer layoutFiltro) {
		this.layoutFiltro = layoutFiltro;
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 * 
	 * @return listaTipoLayoutArquivo
	 */
	public List<SelectItem> getListaTipoLayoutArquivo() {
		return listaTipoLayoutArquivo;
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 * 
	 * @param listaTipoLayoutArquivo
	 *            the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(
			List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	/**
	 * Get: listaTipoLote.
	 * 
	 * @return listaTipoLote
	 */
	public List<SelectItem> getListaTipoLote() {
		return listaTipoLote;
	}

	/**
	 * Set: listaTipoLote.
	 * 
	 * @param listaTipoLote
	 *            the lista tipo lote
	 */
	public void setListaTipoLote(List<SelectItem> listaTipoLote) {
		this.listaTipoLote = listaTipoLote;
	}

	/**
	 * Get: tipoLoteFiltro.
	 * 
	 * @return tipoLoteFiltro
	 */
	public Integer getTipoLoteFiltro() {
		return tipoLoteFiltro;
	}

	/**
	 * Set: tipoLoteFiltro.
	 * 
	 * @param tipoLoteFiltro
	 *            the tipo lote filtro
	 */
	public void setTipoLoteFiltro(Integer tipoLoteFiltro) {
		this.tipoLoteFiltro = tipoLoteFiltro;
	}

	/**
	 * Get: itemSelecionadoLista.
	 * 
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 * 
	 * @param itemSelecionadoLista
	 *            the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 * 
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 * 
	 * @param listaControleRadio
	 *            the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: manterControleUltimoRetornoGeradoServiceImpl.
	 * 
	 * @return manterControleUltimoRetornoGeradoServiceImpl
	 */
	public IManterControleUltimoRetornoGeradoService getManterControleUltimoRetornoGeradoServiceImpl() {
		return manterControleUltimoRetornoGeradoServiceImpl;
	}

	/**
	 * Set: manterControleUltimoRetornoGeradoServiceImpl.
	 * 
	 * @param manterControleUltimoRetornoGeradoServiceImpl
	 *            the manter controle ultimo retorno gerado service impl
	 */
	public void setManterControleUltimoRetornoGeradoServiceImpl(
			IManterControleUltimoRetornoGeradoService manterControleUltimoRetornoGeradoServiceImpl) {
		this.manterControleUltimoRetornoGeradoServiceImpl = manterControleUltimoRetornoGeradoServiceImpl;
	}

	/**
	 * Get: listaGrid.
	 * 
	 * @return listaGrid
	 */
	public List<ListarUltimoNumeroRetornoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 * 
	 * @param listaGrid
	 *            the lista grid
	 */
	public void setListaGrid(List<ListarUltimoNumeroRetornoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 * 
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 * 
	 * @param cdPessoaJuridicaContrato
	 *            the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdSituacaoContrato.
	 * 
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 * 
	 * @param cdSituacaoContrato
	 *            the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 * 
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 * 
	 * @param cdTipoContratoNegocio
	 *            the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: dsContrato.
	 * 
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 * 
	 * @param dsContrato
	 *            the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 * 
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 * 
	 * @param dsEmpresa
	 *            the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 * 
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 * 
	 * @param dsRazaoSocial
	 *            the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: nrCnpjCpf.
	 * 
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 * 
	 * @param nrCnpjCpf
	 *            the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 * 
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 * 
	 * @param nroContrato
	 *            the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 * 
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 * 
	 * @param nrSequenciaContratoNegocio
	 *            the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Get: cdPerfilTrocaArquivoPerfil.
	 * 
	 * @return cdPerfilTrocaArquivoPerfil
	 */
	public Long getCdPerfilTrocaArquivoPerfil() {
		return cdPerfilTrocaArquivoPerfil;
	}

	/**
	 * Set: cdPerfilTrocaArquivoPerfil.
	 * 
	 * @param cdPerfilTrocaArquivoPerfil
	 *            the cd perfil troca arquivo perfil
	 */
	public void setCdPerfilTrocaArquivoPerfil(Long cdPerfilTrocaArquivoPerfil) {
		this.cdPerfilTrocaArquivoPerfil = cdPerfilTrocaArquivoPerfil;
	}

	/**
	 * Get: dsSituacaoPerfil.
	 * 
	 * @return dsSituacaoPerfil
	 */
	public String getDsSituacaoPerfil() {
		return dsSituacaoPerfil;
	}

	/**
	 * Set: dsSituacaoPerfil.
	 * 
	 * @param dsSituacaoPerfil
	 *            the ds situacao perfil
	 */
	public void setDsSituacaoPerfil(String dsSituacaoPerfil) {
		this.dsSituacaoPerfil = dsSituacaoPerfil;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 * 
	 * @return cdTipoLayoutArquivo
	 */
	public String getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 * 
	 * @param cdTipoLayoutArquivo
	 *            the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(String cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdTipoLoteLayout.
	 * 
	 * @return cdTipoLoteLayout
	 */
	public String getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}

	/**
	 * Set: cdTipoLoteLayout.
	 * 
	 * @param cdTipoLoteLayout
	 *            the cd tipo lote layout
	 */
	public void setCdTipoLoteLayout(String cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}

	/**
	 * Get: dsNivelControle.
	 * 
	 * @return dsNivelControle
	 */
	public String getDsNivelControle() {
		return dsNivelControle;
	}

	/**
	 * Set: dsNivelControle.
	 * 
	 * @param dsNivelControle
	 *            the ds nivel controle
	 */
	public void setDsNivelControle(String dsNivelControle) {
		this.dsNivelControle = dsNivelControle;
	}

	/**
	 * Get: dsTipoControle.
	 * 
	 * @return dsTipoControle
	 */
	public String getDsTipoControle() {
		return dsTipoControle;
	}

	/**
	 * Set: dsTipoControle.
	 * 
	 * @param dsTipoControle
	 *            the ds tipo controle
	 */
	public void setDsTipoControle(String dsTipoControle) {
		this.dsTipoControle = dsTipoControle;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 * 
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Set: dsTipoLayoutArquivo.
	 * 
	 * @param dsTipoLayoutArquivo
	 *            the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Get: dsTipoLoteLayout.
	 * 
	 * @return dsTipoLoteLayout
	 */
	public String getDsTipoLoteLayout() {
		return dsTipoLoteLayout;
	}

	/**
	 * Set: dsTipoLoteLayout.
	 * 
	 * @param dsTipoLoteLayout
	 *            the ds tipo lote layout
	 */
	public void setDsTipoLoteLayout(String dsTipoLoteLayout) {
		this.dsTipoLoteLayout = dsTipoLoteLayout;
	}

	/**
	 * Get: nrMaximoArquivoRemessa.
	 * 
	 * @return nrMaximoArquivoRemessa
	 */
	public String getNrMaximoArquivoRemessa() {
		return nrMaximoArquivoRemessa;
	}

	/**
	 * Set: nrMaximoArquivoRemessa.
	 * 
	 * @param nrMaximoArquivoRemessa
	 *            the nr maximo arquivo remessa
	 */
	public void setNrMaximoArquivoRemessa(String nrMaximoArquivoRemessa) {
		this.nrMaximoArquivoRemessa = nrMaximoArquivoRemessa;
	}

	/**
	 * Get: nrUltimoNumeroArquivoRemessa.
	 * 
	 * @return nrUltimoNumeroArquivoRemessa
	 */
	public String getNrUltimoNumeroArquivoRemessa() {
		return nrUltimoNumeroArquivoRemessa;
	}

	/**
	 * Set: nrUltimoNumeroArquivoRemessa.
	 * 
	 * @param nrUltimoNumeroArquivoRemessa
	 *            the nr ultimo numero arquivo remessa
	 */
	public void setNrUltimoNumeroArquivoRemessa(
			String nrUltimoNumeroArquivoRemessa) {
		this.nrUltimoNumeroArquivoRemessa = nrUltimoNumeroArquivoRemessa;
	}

	/**
	 * Get: dadosRemessa.
	 * 
	 * @return dadosRemessa
	 */
	public ConsultarUltimoNumeroRetornoSaidaDTO getDadosRemessa() {
		return dadosRemessa;
	}

	/**
	 * Set: dadosRemessa.
	 * 
	 * @param dadosRemessa
	 *            the dados remessa
	 */
	public void setDadosRemessa(
			ConsultarUltimoNumeroRetornoSaidaDTO dadosRemessa) {
		this.dadosRemessa = dadosRemessa;
	}

	/**
	 * Is disable argumentos consulta.
	 * 
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 * 
	 * @param disableArgumentosConsulta
	 *            the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: consultasService.
	 * 
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 * 
	 * @param consultasService
	 *            the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
		this.consultasService = consultasService;
	}

	/**
	 * Get: manterSolicitacaoRastFavService.
	 * 
	 * @return manterSolicitacaoRastFavService
	 */
	public IManterSolicitacaoRastFavService getManterSolicitacaoRastFavService() {
		return manterSolicitacaoRastFavService;
	}

	/**
	 * Set: manterSolicitacaoRastFavService.
	 * 
	 * @param manterSolicitacaoRastFavService
	 *            the manter solicitacao rast fav service
	 */
	public void setManterSolicitacaoRastFavService(
			IManterSolicitacaoRastFavService manterSolicitacaoRastFavService) {
		this.manterSolicitacaoRastFavService = manterSolicitacaoRastFavService;
	}

	public Long getCdClub() {
		return cdClub;
	}

	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

	public String getCdCorpoCpfCnpj() {
		return cdCorpoCpfCnpj;
	}

	public void setCdCorpoCpfCnpj(String cdCorpoCpfCnpj) {
		this.cdCorpoCpfCnpj = cdCorpoCpfCnpj;
	}

}
