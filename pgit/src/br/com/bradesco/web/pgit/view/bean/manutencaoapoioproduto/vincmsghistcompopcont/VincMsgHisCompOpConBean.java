/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.vincmsghistcompopcont
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.vincmsghistcompopcont;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.arquivorecebido.bean.ComboProdutoDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.IVincMsgHistCompOpeContService;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.DetalheVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ExcluirVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ExcluirVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.IncluirVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.IncluirVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarMsgHistCompEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarMsgHistCompSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarVinMsgHistComOpEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsghistcompopecont.bean.ListarVinMsgHistComOpSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;

/**
 * Nome: VincMsgHisCompOpConBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class VincMsgHisCompOpConBean {
	
	//Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_MANTER_VIC_MSG_HIST_COMP_OPE_CONTRATADA. */
	private static final String CON_MANTER_VIC_MSG_HIST_COMP_OPE_CONTRATADA = "conManterVincMsgHistCompOpeContratada";
	
	/** Atributo listaGridPesquisa. */
	private List<ListarVinMsgHistComOpSaidaDTO> listaGridPesquisa;
	
	/** Atributo listaGridIncluir. */
	private List<ListarMsgHistCompSaidaDTO> listaGridIncluir;	
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo identificacaoClienteBean. */
	private IdentificacaoClienteBean identificacaoClienteBean;
	
	/** Atributo habilitaFiltroDeCliente. */
	private boolean habilitaFiltroDeCliente;
	
	/** Atributo listaEmpresaGestora. */
	private List<SelectItem> listaEmpresaGestora= new ArrayList<SelectItem>();
	
	/** Atributo listaTipoContrato. */
	private List<SelectItem> listaTipoContrato= new ArrayList<SelectItem>();	
	
	/** Atributo codListaRadio. */
	private int codListaRadio;
	
	/** Atributo filtro. */
	private String filtro;
	
	/** Atributo validaCpf. */
	private String validaCpf;
	
	/** Atributo validaCpfCliente. */
	private String validaCpfCliente;
	
	/** Atributo cpfFiltro. */
	private String cpfFiltro;
	
	/** Atributo cpf. */
	private String cpf;
	
	/** Atributo nomeRazao. */
	private String nomeRazao;
	
	/** Atributo empresaCliente. */
	private String empresaCliente;
	
	/** Atributo situacaoCliente. */
	private String situacaoCliente;
	
	/** Atributo tipoCliente. */
	private String tipoCliente;
	
	/** Atributo numeroCliente. */
	private String numeroCliente;
	
	/** Atributo descricaoContratoCliente. */
	private String descricaoContratoCliente;
	
	/** Atributo codigoHistoricoComplementarFiltro. */
	private String codigoHistoricoComplementarFiltro;
	
	/** Atributo listaCodigoMensagem. */
	private List<SelectItem> listaCodigoMensagem = new ArrayList<SelectItem>();
	
	/** Atributo servicoFiltro. */
	private String servicoFiltro;
	
	/** Atributo cdRecursoDebito. */
	private int cdRecursoDebito;
	
	/** Atributo cdRecursoCredito. */
	private int cdRecursoCredito;
	
	/** Atributo listaTipoServicoFiltro. */
	private List<SelectItem> listaTipoServicoFiltro;
	
	/** Atributo listaTipoServicoFiltroHash. */
	private Map<Integer,String> listaTipoServicoFiltroHash = new HashMap<Integer,String>();
	
	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;
	
	/** Atributo modalidadeServicoFiltro. */
	private Integer modalidadeServicoFiltro;
	
	/** Atributo tipoServico. */
	private Integer tipoServico;
	
	/** Atributo modalidadeServico. */
	private Integer modalidadeServico;
	
	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico;
	
	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo codigoHistoricoComplementar. */
	private String codigoHistoricoComplementar;
	
	/** Atributo itemSelecionadoListaInc. */
	private Integer itemSelecionadoListaInc;
	
	
	/** Atributo listaModalidadeServicoFiltro. */
	private List<SelectItem> listaModalidadeServicoFiltro;
	
	/** Atributo listaModalidadeServicoFiltroHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoFiltroHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();	
	
	
	/** Atributo listaProdutoAux. */
	private List<ComboProdutoDTO> listaProdutoAux;
	
	/** Atributo listaControleRadioMensagem. */
	private List<SelectItem> listaControleRadioMensagem;
	
	/** Atributo codigoMensagem. */
	private String codigoMensagem;
	
	/** Atributo lancamentoDebito. */
	private String lancamentoDebito;
	
	/** Atributo lancamentoCredito. */
	private String lancamentoCredito;
	
	/** Atributo produto. */
	private String produto;
	
	/** Atributo operacao. */
	private String operacao;
	
	/** Atributo listaPesquisaControle. */
	private List<SelectItem> listaPesquisaControle = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo mensagemGrid. */
	private String mensagemGrid;
	
	/** Atributo lancamentoCreditoGrid. */
	private String lancamentoCreditoGrid;
	
	/** Atributo lancamentoDebitoGrid. */
	private String lancamentoDebitoGrid;
	
	/** Atributo produtoGrid. */
	private String produtoGrid;
	
	/** Atributo operacaoGrid. */
	private String operacaoGrid;
	
	/** Atributo listaPesquisaControleIncluir. */
	private List<SelectItem> listaPesquisaControleIncluir = new ArrayList<SelectItem>();
	
	/** Atributo mostraBotoesIncManterVincMsgHistCompOpeContratada. */
	private boolean mostraBotoesIncManterVincMsgHistCompOpeContratada;
	
	/** Atributo servicoGrid. */
	private String servicoGrid;
	
	/** Atributo vincMsgHisCompOpeConService. */
	private IVincMsgHistCompOpeContService vincMsgHisCompOpeConService;
	
	/*Trilha de Auditoria*/
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;	
	
	/** Atributo detalheVinMsgHistComOpSaidaDTO. */
	private DetalheVinMsgHistComOpSaidaDTO detalheVinMsgHistComOpSaidaDTO = new DetalheVinMsgHistComOpSaidaDTO();	
	
	/** Atributo hiddenConfirma. */
	private boolean hiddenConfirma;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
		
	/**
	 * Get: servicoGrid.
	 *
	 * @return servicoGrid
	 */
	public String getServicoGrid() {
		return servicoGrid;
	}

	/**
	 * Set: servicoGrid.
	 *
	 * @param servicoGrid the servico grid
	 */
	public void setServicoGrid(String servicoGrid) {
		this.servicoGrid = servicoGrid;
	}
	
	/**
	 * Is mostra botoes inc manter vinc msg hist comp ope contratada.
	 *
	 * @return true, if is mostra botoes inc manter vinc msg hist comp ope contratada
	 */
	public boolean isMostraBotoesIncManterVincMsgHistCompOpeContratada() {
		return mostraBotoesIncManterVincMsgHistCompOpeContratada;
	}

	/**
	 * Set: mostraBotoesIncManterVincMsgHistCompOpeContratada.
	 *
	 * @param mostraBotoesIncManterVincMsgHistCompOpeContratada the mostra botoes inc manter vinc msg hist comp ope contratada
	 */
	public void setMostraBotoesIncManterVincMsgHistCompOpeContratada(
			boolean mostraBotoesIncManterVincMsgHistCompOpeContratada) {
		this.mostraBotoesIncManterVincMsgHistCompOpeContratada = mostraBotoesIncManterVincMsgHistCompOpeContratada;
	}

	/**
	 * Get: listaPesquisaControleIncluir.
	 *
	 * @return listaPesquisaControleIncluir
	 */
	public List<SelectItem> getListaPesquisaControleIncluir() {
		return listaPesquisaControleIncluir;
	}

	/**
	 * Set: listaPesquisaControleIncluir.
	 *
	 * @param listaPesquisaControleIncluir the lista pesquisa controle incluir
	 */
	public void setListaPesquisaControleIncluir(
			List<SelectItem> listaPesquisaControleIncluir) {
		this.listaPesquisaControleIncluir = listaPesquisaControleIncluir;
	}


	/**
	 * Get: lancamentoCreditoGrid.
	 *
	 * @return lancamentoCreditoGrid
	 */
	public String getLancamentoCreditoGrid() {
		return lancamentoCreditoGrid;
	}

	/**
	 * Set: lancamentoCreditoGrid.
	 *
	 * @param lancamentoCreditoGrid the lancamento credito grid
	 */
	public void setLancamentoCreditoGrid(String lancamentoCreditoGrid) {
		this.lancamentoCreditoGrid = lancamentoCreditoGrid;
	}

	/**
	 * Get: lancamentoDebitoGrid.
	 *
	 * @return lancamentoDebitoGrid
	 */
	public String getLancamentoDebitoGrid() {
		return lancamentoDebitoGrid;
	}

	/**
	 * Set: lancamentoDebitoGrid.
	 *
	 * @param lancamentoDebitoGrid the lancamento debito grid
	 */
	public void setLancamentoDebitoGrid(String lancamentoDebitoGrid) {
		this.lancamentoDebitoGrid = lancamentoDebitoGrid;
	}

	/**
	 * Get: mensagemGrid.
	 *
	 * @return mensagemGrid
	 */
	public String getMensagemGrid() {
		return mensagemGrid;
	}

	/**
	 * Set: mensagemGrid.
	 *
	 * @param mensagemGrid the mensagem grid
	 */
	public void setMensagemGrid(String mensagemGrid) {
		this.mensagemGrid = mensagemGrid;
	}

	/**
	 * Get: operacaoGrid.
	 *
	 * @return operacaoGrid
	 */
	public String getOperacaoGrid() {
		return operacaoGrid;
	}

	/**
	 * Set: operacaoGrid.
	 *
	 * @param operacaoGrid the operacao grid
	 */
	public void setOperacaoGrid(String operacaoGrid) {
		this.operacaoGrid = operacaoGrid;
	}

	/**
	 * Get: produtoGrid.
	 *
	 * @return produtoGrid
	 */
	public String getProdutoGrid() {
		return produtoGrid;
	}

	/**
	 * Set: produtoGrid.
	 *
	 * @param produtoGrid the produto grid
	 */
	public void setProdutoGrid(String produtoGrid) {
		this.produtoGrid = produtoGrid;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaPesquisaControle.
	 *
	 * @return listaPesquisaControle
	 */
	public List<SelectItem> getListaPesquisaControle() {
		return listaPesquisaControle;
	}

	/**
	 * Set: listaPesquisaControle.
	 *
	 * @param listaPesquisaControle the lista pesquisa controle
	 */
	public void setListaPesquisaControle(List<SelectItem> listaPesquisaControle) {
		this.listaPesquisaControle = listaPesquisaControle;
	}

	/**
	 * Get: codListaRadio.
	 *
	 * @return codListaRadio
	 */
	public int getCodListaRadio() {
		return codListaRadio;
	}

	/**
	 * Set: codListaRadio.
	 *
	 * @param codListaRadio the cod lista radio
	 */
	public void setCodListaRadio(int codListaRadio) {
		this.codListaRadio = codListaRadio;
	}

	/**
	 * Get: cpf.
	 *
	 * @return cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Set: cpf.
	 *
	 * @param cpf the cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Get: cpfFiltro.
	 *
	 * @return cpfFiltro
	 */
	public String getCpfFiltro() {
		return cpfFiltro;
	}

	/**
	 * Set: cpfFiltro.
	 *
	 * @param cpfFiltro the cpf filtro
	 */
	public void setCpfFiltro(String cpfFiltro) {
		this.cpfFiltro = cpfFiltro;
	}

	/**
	 * Get: descricaoContratoCliente.
	 *
	 * @return descricaoContratoCliente
	 */
	public String getDescricaoContratoCliente() {
		return descricaoContratoCliente;
	}

	/**
	 * Set: descricaoContratoCliente.
	 *
	 * @param descricaoContratoCliente the descricao contrato cliente
	 */
	public void setDescricaoContratoCliente(String descricaoContratoCliente) {
		this.descricaoContratoCliente = descricaoContratoCliente;
	}

	/**
	 * Get: empresaCliente.
	 *
	 * @return empresaCliente
	 */
	public String getEmpresaCliente() {
		return empresaCliente;
	}

	/**
	 * Set: empresaCliente.
	 *
	 * @param empresaCliente the empresa cliente
	 */
	public void setEmpresaCliente(String empresaCliente) {
		this.empresaCliente = empresaCliente;
	}



	/**
	 * Get: nomeRazao.
	 *
	 * @return nomeRazao
	 */
	public String getNomeRazao() {
		return nomeRazao;
	}

	/**
	 * Set: nomeRazao.
	 *
	 * @param nomeRazao the nome razao
	 */
	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}

	/**
	 * Get: numeroCliente.
	 *
	 * @return numeroCliente
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * Set: numeroCliente.
	 *
	 * @param numeroCliente the numero cliente
	 */
	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	/**
	 * Get: situacaoCliente.
	 *
	 * @return situacaoCliente
	 */
	public String getSituacaoCliente() {
		return situacaoCliente;
	}

	/**
	 * Set: situacaoCliente.
	 *
	 * @param situacaoCliente the situacao cliente
	 */
	public void setSituacaoCliente(String situacaoCliente) {
		this.situacaoCliente = situacaoCliente;
	}

	/**
	 * Get: tipoCliente.
	 *
	 * @return tipoCliente
	 */
	public String getTipoCliente() {
		return tipoCliente;
	}

	/**
	 * Set: tipoCliente.
	 *
	 * @param tipoCliente the tipo cliente
	 */
	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	/**
	 * Get: validaCpf.
	 *
	 * @return validaCpf
	 */
	public String getValidaCpf() {
		return validaCpf;
	}

	/**
	 * Set: validaCpf.
	 *
	 * @param validaCpf the valida cpf
	 */
	public void setValidaCpf(String validaCpf) {
		this.validaCpf = validaCpf;
	}

	/**
	 * Get: validaCpfCliente.
	 *
	 * @return validaCpfCliente
	 */
	public String getValidaCpfCliente() {
		return validaCpfCliente;
	}

	/**
	 * Set: validaCpfCliente.
	 *
	 * @param validaCpfCliente the valida cpf cliente
	 */
	public void setValidaCpfCliente(String validaCpfCliente) {
		this.validaCpfCliente = validaCpfCliente;
	}

	/**
	 * Get: codigoMensagem.
	 *
	 * @return codigoMensagem
	 */
	public String getCodigoMensagem() {
		return codigoMensagem;
	}

	/**
	 * Set: codigoMensagem.
	 *
	 * @param codigoMensagem the codigo mensagem
	 */
	public void setCodigoMensagem(String codigoMensagem) {
		this.codigoMensagem = codigoMensagem;
	}

	/**
	 * Get: lancamentoCredito.
	 *
	 * @return lancamentoCredito
	 */
	public String getLancamentoCredito() {
		return lancamentoCredito;
	}

	/**
	 * Set: lancamentoCredito.
	 *
	 * @param lancamentoCredito the lancamento credito
	 */
	public void setLancamentoCredito(String lancamentoCredito) {
		this.lancamentoCredito = lancamentoCredito;
	}

	/**
	 * Get: lancamentoDebito.
	 *
	 * @return lancamentoDebito
	 */
	public String getLancamentoDebito() {
		return lancamentoDebito;
	}

	/**
	 * Set: lancamentoDebito.
	 *
	 * @param lancamentoDebito the lancamento debito
	 */
	public void setLancamentoDebito(String lancamentoDebito) {
		this.lancamentoDebito = lancamentoDebito;
	}

	/**
	 * Set: listaCodigoMensagem.
	 *
	 * @param listaCodigoMensagem the lista codigo mensagem
	 */
	public void setListaCodigoMensagem(List<SelectItem> listaCodigoMensagem) {
		this.listaCodigoMensagem = listaCodigoMensagem;
	}

	/**
	 * Get: listaControleRadioMensagem.
	 *
	 * @return listaControleRadioMensagem
	 */
	public List<SelectItem> getListaControleRadioMensagem() {
		return listaControleRadioMensagem;
	}

	/**
	 * Set: listaControleRadioMensagem.
	 *
	 * @param listaControleRadioMensagem the lista controle radio mensagem
	 */
	public void setListaControleRadioMensagem(
			List<SelectItem> listaControleRadioMensagem) {
		this.listaControleRadioMensagem = listaControleRadioMensagem;
	}

	/**
	 * Get: listaProdutoAux.
	 *
	 * @return listaProdutoAux
	 */
	public List<ComboProdutoDTO> getListaProdutoAux() {
		return listaProdutoAux;
	}

	/**
	 * Set: listaProdutoAux.
	 *
	 * @param listaProdutoAux the lista produto aux
	 */
	public void setListaProdutoAux(List<ComboProdutoDTO> listaProdutoAux) {
		this.listaProdutoAux = listaProdutoAux;
	}

	/**
	 * Get: operacao.
	 *
	 * @return operacao
	 */
	public String getOperacao() {
		return operacao;
	}

	/**
	 * Set: operacao.
	 *
	 * @param operacao the operacao
	 */
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	/**
	 * Get: produto.
	 *
	 * @return produto
	 */
	public String getProduto() {
		return produto;
	}

	/**
	 * Set: produto.
	 *
	 * @param produto the produto
	 */
	public void setProduto(String produto) {
		this.produto = produto;
	}

	/**
	 * Get: servicoFiltro.
	 *
	 * @return servicoFiltro
	 */
	public String getServicoFiltro() {
		return servicoFiltro;
	}

	/**
	 * Set: servicoFiltro.
	 *
	 * @param servicoFiltro the servico filtro
	 */
	public void setServicoFiltro(String servicoFiltro) {
		this.servicoFiltro = servicoFiltro;
	}

	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {
		setCodigoHistoricoComplementarFiltro("");		
		
		
		identificacaoClienteBean.setPaginaCliente("identificacaoVincMsgHistCompOpCliente");
		identificacaoClienteBean.setPaginaContrato("identificacaoVincMsgHistCompOpContrato");
		identificacaoClienteBean.setPaginaRetorno(CON_MANTER_VIC_MSG_HIST_COMP_OPE_CONTRATADA);
		identificacaoClienteBean.setItemClienteSelecionado(null);
		identificacaoClienteBean.setItemContratoSelecionado(null);
		identificacaoClienteBean.setItemFiltroSelecionado("0");
		
		this.identificacaoClienteBean.setHabilitaFiltroDeContrato(true);
		this.identificacaoClienteBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteBean.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.identificacaoClienteBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteBean.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this.identificacaoClienteBean.setDesabilataFiltro(true);
		this.setHabilitaFiltroDeCliente(false);
		this.identificacaoClienteBean.setBloqueaRadio(false);
		
		
		this.setTipoServicoFiltro(0);
		this.setModalidadeServicoFiltro(0);
		this.setListaModalidadeServico(new ArrayList<SelectItem>());
		listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
		this.setListaGridPesquisa(null);
		this.setItemSelecionadoLista(null);
		this.identificacaoClienteBean.setBloqueaRadio(false);
		try{
			listarEmpresaGestora();
		}catch(PdcAdapterFunctionalException e){
			listaEmpresaGestora = new ArrayList<SelectItem>();
		}
		
		try{
			listarTipoContrato();
		}catch(PdcAdapterFunctionalException e){
			listaTipoContrato = new ArrayList<SelectItem>();
		}
		
		try{
			listarTipoServicoFiltro();
		}catch(PdcAdapterFunctionalException e){
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
		}
		
		setBtoAcionado(false);
		return "OK";
	}
	
	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar(){
		identificacaoClienteBean.limparCliente();
		identificacaoClienteBean.limparContrato();
		limparArgumentosPesquisa();
		limparGrid();
		return "";
		
	}
	
	/**
	 * Limpar dados incluir.
	 *
	 * @return the string
	 */
	public String limparDadosIncluir(){
		setCodigoHistoricoComplementar("");
		setTipoServico(null);
		setModalidadeServico(null);
		setListaGridIncluir(null);
		setItemSelecionadoListaInc(null);
		return "OK";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_VIC_MSG_HIST_COMP_OPE_CONTRATADA, BradescoViewExceptionActionType.ACTION, false);
			return "";
		} 
		
		return "AVANCAR_DETALHAR";
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		setListaGridIncluir(null);
		setItemSelecionadoListaInc(null);
		setCodigoHistoricoComplementar("");
		setTipoServico(null);
		setModalidadeServico(null);
		return "AVANCAR_INCLUIR";
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_VIC_MSG_HIST_COMP_OPE_CONTRATADA, BradescoViewExceptionActionType.ACTION, false);
			return "";
		} 		
		
		return "AVANCAR_EXCLUIR";
	}

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {
		setItemSelecionadoLista(null);
		return "VOLTAR_PESQUISA";
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		preencheDadosAvancarIncluir();
		return "CONFIRMAR_INCLUIR";
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		setItemSelecionadoListaInc(null);
		return "VOLTAR_INCLUIR";
	}

	/**
	 * Confirmar inclusao.
	 *
	 * @return the string
	 */
	public String confirmarInclusao() {
		if (isHiddenConfirma()){
			try{	
				IncluirVinMsgHistComOpEntradaDTO msgHistCompOp = new IncluirVinMsgHistComOpEntradaDTO();
				msgHistCompOp.setCdMensagemLinhaExtrato(listaGridIncluir.get(itemSelecionadoListaInc).getCdMensagemLinhaExtrato());
				msgHistCompOp.setCdPessoaJuridicaContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
				msgHistCompOp.setCdProdutoOperacaoRelacionado(listaGridIncluir.get(itemSelecionadoListaInc).getCdProdutoOperacaoRelacionado());
				msgHistCompOp.setCdProdutoServicoOperacao(listaGridIncluir.get(itemSelecionadoListaInc).getCdProdutoServicoOperacao());
				msgHistCompOp.setCdRelacionamentoProduto(listaGridIncluir.get(itemSelecionadoListaInc).getCdRelacionadoProduto());
				msgHistCompOp.setCdTipoContratoNegocio(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
				msgHistCompOp.setNrSequenciaContratoNegocio(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());

				IncluirVinMsgHistComOpSaidaDTO incluirVinMsgHistComOpSaidaDTO = getVincMsgHisCompOpeConService().incluirVinMsgHistoricoComplementar(msgHistCompOp);
				carregarGrid();
				BradescoFacesUtils.addInfoModalMessage("(" +incluirVinMsgHistComOpSaidaDTO.getCodMensagem() + ") " + incluirVinMsgHistComOpSaidaDTO.getMensagem(), CON_MANTER_VIC_MSG_HIST_COMP_OPE_CONTRATADA, BradescoViewExceptionActionType.ACTION, false);
				
			}catch (PdcAdapterFunctionalException p) {
					BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
					return null;
			}
			
		}		
		
		return "ok";
	}

	/**
	 * Tela.
	 *
	 * @return the string
	 */
	public String tela() {
		return "ok";
	}

	/**
	 * Confirmar exclusao.
	 *
	 * @return the string
	 */
	public String confirmarExclusao() {
		if (isHiddenConfirma()){
			try{	
				ExcluirVinMsgHistComOpEntradaDTO msgHistCompOp = new ExcluirVinMsgHistComOpEntradaDTO();
				
				msgHistCompOp.setCdMensagemLinhaExtrato(detalheVinMsgHistComOpSaidaDTO.getCdMensagemLinhaExtrato());
				msgHistCompOp.setCdProdutoOperacaoRelacionado(detalheVinMsgHistComOpSaidaDTO.getCdProdutoOperacaoRelacionado());
				msgHistCompOp.setCdProdutoServicoOperacao(detalheVinMsgHistComOpSaidaDTO.getCdProdutoServicoOperacao());
				msgHistCompOp.setCdRelacionamentoProduto(detalheVinMsgHistComOpSaidaDTO.getCdRelacionadoProduto());
				msgHistCompOp.setCdTipoContratoNegocio(detalheVinMsgHistComOpSaidaDTO.getCdTipoContratoNegocio());
				msgHistCompOp.setCdPessoaJuridicaContrato(detalheVinMsgHistComOpSaidaDTO.getCdPessoaJuridicaContrato());
				msgHistCompOp.setNrSequenciaContratoNegocio(detalheVinMsgHistComOpSaidaDTO.getNrSequenciaContratoNegocio());
				
				ExcluirVinMsgHistComOpSaidaDTO excluirVinMsgHistComOpSaidaDTO = getVincMsgHisCompOpeConService().excluirVinMsgHistoricoComplementar(msgHistCompOp);
				
				carregarGrid();
				BradescoFacesUtils.addInfoModalMessage("(" +excluirVinMsgHistComOpSaidaDTO.getCodMensagem() + ") " + excluirVinMsgHistComOpSaidaDTO.getMensagem(), CON_MANTER_VIC_MSG_HIST_COMP_OPE_CONTRATADA, BradescoViewExceptionActionType.ACTION, false);
				
			}catch (PdcAdapterFunctionalException p) {
					BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
					return null;
			}
			
		}		
		
		return "ok";
	}

	/**
	 * Pesquisar.
	 *
	 * @param e the e
	 * @return the string
	 */
	public String pesquisar(ActionEvent e) {
		return "ok";
	}
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt){
		if (identificacaoClienteBean== null){
			identificacaoClienteBean=  new IdentificacaoClienteBean();
		}
		
		this.setListaTipoServicoFiltro(new ArrayList<SelectItem>());
		this.setListaModalidadeServicoFiltro(new ArrayList<SelectItem>());	
		this.setListaModalidadeServico(new ArrayList<SelectItem>());
		
		//carrega os combos do contrato
		try{
			listarEmpresaGestora();
		}catch(PdcAdapterFunctionalException e){
			listaEmpresaGestora = new ArrayList<SelectItem>();
		}
		
		try{
			listarTipoContrato();
		}catch(PdcAdapterFunctionalException e){
			listaTipoContrato = new ArrayList<SelectItem>();
		}
		
		try{
			listarTipoServicoFiltro();
		}catch(PdcAdapterFunctionalException e){
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
		}
		
		identificacaoClienteBean.setPaginaCliente("identificacaoVincMsgHistCompOpCliente");
		identificacaoClienteBean.setPaginaContrato("identificacaoVincMsgHistCompOpContrato");
		identificacaoClienteBean.setPaginaRetorno(CON_MANTER_VIC_MSG_HIST_COMP_OPE_CONTRATADA);
		identificacaoClienteBean.setItemClienteSelecionado(null);
		identificacaoClienteBean.setItemContratoSelecionado(null);	
		this.identificacaoClienteBean.setItemFiltroSelecionado("0");
		this.identificacaoClienteBean.setHabilitaFiltroDeContrato(true);
		this.identificacaoClienteBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteBean.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.identificacaoClienteBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteBean.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this.identificacaoClienteBean.setDesabilataFiltro(false);
		this.identificacaoClienteBean.setItemFiltroSelecionado("");
		identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().setCdPessoaJuridicaContrato(2269651L);
		this.setHabilitaFiltroDeCliente(false);	
		
		this.setListaEmpresaGestora(new ArrayList<SelectItem>());
		this.setListaTipoContrato(new ArrayList<SelectItem>());		
		setCodListaRadio(-1);
		setFiltro("");
		setValidaCpf("");
		setValidaCpfCliente("");
		setCpfFiltro("");
		setCpf("");
		setNomeRazao("");
		setEmpresaCliente("");
		setSituacaoCliente("");
		setTipoCliente("");
		setNumeroCliente("");
		setDescricaoContratoCliente("");
		setCodigoHistoricoComplementarFiltro("");
		setServicoFiltro("");
		setCodigoMensagem("");
		setLancamentoDebito("");
		setLancamentoCredito("");
		setProduto("");
		setOperacao("");
		setTipoServicoFiltro(null);
		setModalidadeServicoFiltro(null);
		
		//setListaGrid(null);
		setItemSelecionadoLista(null);
		//setListaGridIncluir(null);
		setItemSelecionadoListaInc(null);
		setCodigoHistoricoComplementar("");
		
		//carrega os combos do contrato
		listarEmpresaGestora();
		listarTipoContrato();		
		setBtoAcionado(false);
		
	}

	/**
	 * isHabilitaConsu.
	 *
	 * @return boolean
	 */
	public boolean isHabilitaConsu(){
		if (!"".equals(identificacaoClienteBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())
				&& identificacaoClienteBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				|| !"".equals(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getDsPessoaJuridicaContrato())
				&& identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getDsPessoaJuridicaContrato() != null) {
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * paginarLista.
	 *
	 * @void
	*/
	public void paginarLista(ActionEvent evt){
		carregarGrid();
	}
	
	/**
	 * Carregar grid.
	 *
	 * @return the string
	 */
	public String carregarGrid(){
		try{
			setItemSelecionadoLista(null);
			ListarVinMsgHistComOpEntradaDTO entradaDTO = new ListarVinMsgHistComOpEntradaDTO();
			
			//C�digo Hist�rico Complementar
			entradaDTO.setCdMensagemLinhaExtrato( (!getCodigoHistoricoComplementarFiltro().equals("")) ? Integer.parseInt(getCodigoHistoricoComplementarFiltro()) : 0 );
			
			//Empresa do Contrato 
			entradaDTO.setCdPessoaJuridicaContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			
			//Modalidade Servi�o
			entradaDTO.setCdProdutoOperacaoRelacionado( (getModalidadeServicoFiltro() != null) ? getModalidadeServicoFiltro(): 0);//Modal Servico
			
			//Tipo Servi�o
			entradaDTO.setCdProdutoServicoOperacao((getTipoServicoFiltro()!= null)? getTipoServicoFiltro(): 0);//TipoServico

			//Tipo Relacionamento (campo oculto no combo Modalidade Servi�o)
			if (getModalidadeServicoFiltro() != null && getModalidadeServicoFiltro() != 0){ 
				entradaDTO.setCdRelacionamentoProduto( (int) listaModalidadeServicoFiltroHash.get(getModalidadeServicoFiltro()).getCdRelacionamentoProduto());
			}
			else{
				entradaDTO.setCdRelacionamentoProduto(0);
			}
			
			//Tipo do Contrato 
			entradaDTO.setCdTipoContratoNegocio(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			
			//N�mero do Contrato
			entradaDTO.setNrSequenciaContratoNegocio(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			
			
			setListaGridPesquisa(vincMsgHisCompOpeConService.listarVinculoMsgHistCompOperacao(entradaDTO));
			setBtoAcionado(true);
			this.listaPesquisaControle =  new ArrayList<SelectItem>();

			for (int i = 0; i <= getListaGridPesquisa().size();i++) {
				this.listaPesquisaControle.add(new SelectItem(i," "));
			}
			
			identificacaoClienteBean.setHabilitaFiltroDeContrato(true);
			setHabilitaFiltroDeCliente(true);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);			
			setHabilitaFiltroDeCliente(false);
			identificacaoClienteBean.setHabilitaFiltroDeContrato(false);
			setListaGridPesquisa(null);			
			return "";
		}
		
		return "";
	}
	
	/**
	 * Carregar grid incluir.
	 */
	public void carregarGridIncluir(){		
		try{
			setItemSelecionadoListaInc(null);
			
			ListarMsgHistCompEntradaDTO entradaDTO = new ListarMsgHistCompEntradaDTO();
			
			entradaDTO.setCdMensagemLinhaExtrato( (!getCodigoHistoricoComplementar().equals("")) ? Integer.parseInt(getCodigoHistoricoComplementar()) : 0 );

			entradaDTO.setCdIndicadorRestricaoContrato(0);
			entradaDTO.setCdTipoMensagemExtrato(0);			
			entradaDTO.setCdProdutoOperacaoRelacionado( (getModalidadeServico() != null) ? getModalidadeServico(): 0);
			entradaDTO.setCdProdutoServicoOperacao((getTipoServico()!= null)? getTipoServico(): 0);
			
			//Tipo Relacionamento (campo oculto no combo Modalidade Servi�o)
			if (getModalidadeServico() != null && getModalidadeServico() != 0){
				entradaDTO.setCdRelacionamentoProduto( (int) listaModalidadeServicoHash.get(getModalidadeServico()).getCdRelacionamentoProduto());
			}
			else{
				entradaDTO.setCdRelacionamentoProduto(0);
			}
			
			setListaGridIncluir(vincMsgHisCompOpeConService.listarMsgHistComplementar(entradaDTO));
			
			this.listaPesquisaControleIncluir =  new ArrayList<SelectItem>();
			
			for (int i = 0; i <= getListaGridIncluir().size();i++) {
				this.listaPesquisaControleIncluir.add(new SelectItem(i," "));
			}
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setItemSelecionadoListaInc(null);
		}		
	}

	/**
	 * Get: filtro.
	 *
	 * @return filtro
	 */
	public String getFiltro() {
		return filtro;
	}

	/**
	 * Set: filtro.
	 *
	 * @param filtro the filtro
	 */
	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}

	/**
	 * Get: vincMsgHisCompOpeConService.
	 *
	 * @return vincMsgHisCompOpeConService
	 */
	public IVincMsgHistCompOpeContService getVincMsgHisCompOpeConService() {
		return vincMsgHisCompOpeConService;
	}

	/**
	 * Set: vincMsgHisCompOpeConService.
	 *
	 * @param vincMsgHisCompOpeConService the vinc msg his comp ope con service
	 */
	public void setVincMsgHisCompOpeConService(
			IVincMsgHistCompOpeContService vincMsgHisCompOpeConService) {
		this.vincMsgHisCompOpeConService = vincMsgHisCompOpeConService;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: identificacaoClienteBean.
	 *
	 * @return identificacaoClienteBean
	 */
	public IdentificacaoClienteBean getIdentificacaoClienteBean() {
		return identificacaoClienteBean;
	}

	/**
	 * Set: identificacaoClienteBean.
	 *
	 * @param identificacaoClienteBean the identificacao cliente bean
	 */
	public void setIdentificacaoClienteBean(
			IdentificacaoClienteBean identificacaoClienteBean) {
		this.identificacaoClienteBean = identificacaoClienteBean;
	}
	
	/**
	 * Limpar dados cliente.
	 *
	 * @return the string
	 */
	public String limparDadosCliente(){
		identificacaoClienteBean.limparCliente();
		limparArgumentosPesquisa();
		return "";
	}
	
	/**
	 * Limpar dados contrato.
	 *
	 * @return the string
	 */
	public String limparDadosContrato(){
		identificacaoClienteBean.limparContrato();
		limparArgumentosPesquisa();
		return "";
	}
	
	/**
	 * Limpar argumentos pesquisa.
	 *
	 * @return the string
	 */
	public String limparArgumentosPesquisa(){
		setCodigoHistoricoComplementarFiltro("");
		setTipoServicoFiltro(0);
		setModalidadeServicoFiltro(0);
		limparGrid();
		setBtoAcionado(false);
		return "";
	}
	
	/**
	 * Limpar grid.
	 */
	public void limparGrid(){
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
	}

	/**
	 * Limpar campos pesquisa cliente consultar.
	 */
	public void limparCamposPesquisaClienteConsultar(){
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdCpfCnpj(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdFilialCnpj(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdControleCnpj(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdCpf(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdControleCpf(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setDsNomeRazaoSocial("");
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdBanco(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdAgenciaBancaria(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdContaBancaria(null);
		
	}
	
	/**
	 * Listar tipo servico filtro.
	 */
	public void listarTipoServicoFiltro(){
		
		this.listaTipoServicoFiltro = new ArrayList<SelectItem>();
		List<ListarServicosSaidaDTO> list = new ArrayList<ListarServicosSaidaDTO>();
		
		list = comboService.listarTipoServicos(1);
		listaTipoServicoFiltro.clear();
			for(ListarServicosSaidaDTO combo : list){
				listaTipoServicoFiltroHash.put(combo.getCdServico(), combo.getDsServico());
				listaTipoServicoFiltro.add(new SelectItem(combo.getCdServico(), combo.getDsServico()));
			}
	}
	
	/**
	 * Listar modalidade servico filtro.
	 */
	public void listarModalidadeServicoFiltro(){
		
		listaModalidadeServicoFiltro = new ArrayList<SelectItem>();
		
		if(getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0){
			
			List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
			ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
			
			listarModalidadeEntradaDTO.setCdServico(getTipoServicoFiltro());
			
			listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
			
			listaModalidadeServicoFiltroHash.clear();
			for(ListarModalidadeSaidaDTO combo : listaModalidades){
				listaModalidadeServicoFiltroHash.put(combo.getCdModalidade(),combo);
				this.listaModalidadeServicoFiltro.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
			}
			}else{
				
				listaModalidadeServicoFiltroHash.clear();
				listaModalidadeServicoFiltro.clear();
				setTipoServicoFiltro(0);
			}
	}	
	
	/**
	 * Listar modalidade servico.
	 */
	public void listarModalidadeServico(){
		
		listaModalidadeServico = new ArrayList<SelectItem>();
		
		if(getTipoServico() != null && getTipoServico() != 0){
			
			List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
			ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
			
			listarModalidadeEntradaDTO.setCdServico(getTipoServico());
			
			listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
			
			listaModalidadeServicoHash.clear();
			for(ListarModalidadeSaidaDTO combo : listaModalidades){
				listaModalidadeServicoHash.put(combo.getCdModalidade(),combo);
				this.listaModalidadeServico.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
			}
			}else{
				
				listaModalidadeServicoHash.clear();
				listaModalidadeServico.clear();
				setTipoServico(0);
			}
	}		
	
	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora(){
		
		this.listaEmpresaGestora = new ArrayList<SelectItem>();
		List<EmpresaGestoraSaidaDTO> list = new ArrayList<EmpresaGestoraSaidaDTO>();
		
		list = comboService.listarEmpresasGestoras();
		listaEmpresaGestora.clear();
		for(EmpresaGestoraSaidaDTO combo : list){
			listaEmpresaGestora.add(new SelectItem(combo.getCdPessoaJuridicaContrato(),combo.getDsCnpj()));
		}
		

	}
	
	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		
		ListarVinMsgHistComOpSaidaDTO listarVinMsgHistComOpSaidaDTO= getListaGridPesquisa().get(getItemSelecionadoLista());
		DetalheVinMsgHistComOpEntradaDTO detalheVinMsgHistComOpEntradaDTO = new DetalheVinMsgHistComOpEntradaDTO();
		
		detalheVinMsgHistComOpEntradaDTO.setCdMensagemLinhaExtrato(listarVinMsgHistComOpSaidaDTO.getCdMensagemLinhaExtrato());
		detalheVinMsgHistComOpEntradaDTO.setCdPessoaJuridicaContrato(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		detalheVinMsgHistComOpEntradaDTO.setCdProdutoOperacaoRelacionado(listarVinMsgHistComOpSaidaDTO.getCdProdutoOperacaoRelacionado());
		detalheVinMsgHistComOpEntradaDTO.setCdProdutoServicoOperacao(listarVinMsgHistComOpSaidaDTO.getCdProdutoServicoOperacao());
		detalheVinMsgHistComOpEntradaDTO.setCdRelacionamentoProduto(listarVinMsgHistComOpSaidaDTO.getCdRelacionamentoProduto());
		detalheVinMsgHistComOpEntradaDTO.setCdTipoContratoNegocio(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		detalheVinMsgHistComOpEntradaDTO.setNrSequenciaContratoNegocio(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		
		
		DetalheVinMsgHistComOpSaidaDTO saida = getVincMsgHisCompOpeConService().detalharVinMsgHistoricoComplementar(detalheVinMsgHistComOpEntradaDTO);
		
		this.detalheVinMsgHistComOpSaidaDTO = new DetalheVinMsgHistComOpSaidaDTO();
		
		this.detalheVinMsgHistComOpSaidaDTO.setCdAutenticacaoSegurancaInclusao(saida.getCdAutenticacaoSegurancaInclusao());
		this.detalheVinMsgHistComOpSaidaDTO.setCdAutenticacaoSegurancaManutencao(saida.getCdAutenticacaoSegurancaManutencao());
		this.detalheVinMsgHistComOpSaidaDTO.setCdCanalInclusao(saida.getCdCanalInclusao());
		this.detalheVinMsgHistComOpSaidaDTO.setCdCanalManutencao(saida.getCdCanalManutencao());
		this.detalheVinMsgHistComOpSaidaDTO.setCdIdiomaLancamentoCredito(saida.getCdIdiomaLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdIdiomaLancamentoDebito(saida.getCdIdiomaLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdIndicadorRestricaoContrato(saida.getCdIndicadorRestricaoContrato());
		this.detalheVinMsgHistComOpSaidaDTO.setCdMensagemLinhaExtrato(saida.getCdMensagemLinhaExtrato());
		this.detalheVinMsgHistComOpSaidaDTO.setCdPessoaJuridicaContrato(saida.getCdPessoaJuridicaContrato());
		this.detalheVinMsgHistComOpSaidaDTO.setCdProdutoOperacaoRelacionado(saida.getCdProdutoOperacaoRelacionado());
		this.detalheVinMsgHistComOpSaidaDTO.setCdProdutoServicoOperacao(saida.getCdProdutoServicoOperacao());
		this.detalheVinMsgHistComOpSaidaDTO.setCdRecursoLancamentoCredito(saida.getCdRecursoLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdRecursoLancamentoDebito(saida.getCdRecursoLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdRelacionadoProduto(saida.getCdRelacionadoProduto());
		this.detalheVinMsgHistComOpSaidaDTO.setCdSistemaLancamentoCredito(saida.getCdSistemaLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdSistemaLancamentoDebito(saida.getCdSistemaLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdTipoContratoNegocio(saida.getCdTipoContratoNegocio());
		this.detalheVinMsgHistComOpSaidaDTO.setCdTipoMensagemExtrato(saida.getCdTipoMensagemExtrato());
		this.detalheVinMsgHistComOpSaidaDTO.setCodMensagem(saida.getCodMensagem());
		this.detalheVinMsgHistComOpSaidaDTO.setDsCanalInclusao(saida.getDsCanalInclusao());
		this.detalheVinMsgHistComOpSaidaDTO.setDsCanalManutencao(saida.getDsCanalManutencao());
		this.detalheVinMsgHistComOpSaidaDTO.setDsEventoLancamentoCredito(saida.getDsEventoLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsEventoLancamentoDebito(saida.getDsEventoLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsIdiomaLancamentoCredito(saida.getDsIdiomaLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsIdiomaLancamentoDebito(saida.getDsIdiomaLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsProdutoOperacaoRelacionado(saida.getDsProdutoOperacaoRelacionado());
		this.detalheVinMsgHistComOpSaidaDTO.setDsProdutoServicoOperacao(saida.getDsProdutoServicoOperacao());
		this.detalheVinMsgHistComOpSaidaDTO.setDsRecursoLancamentoCredito(saida.getDsRecursoLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsRecursoLancamentoDebito(saida.getDsRecursoLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsSistemaLancamentoCredito(saida.getDsSistemaLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsSistemaLancamentoDebito(saida.getDsSistemaLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setHrInclusaoRegistro(saida.getHrInclusaoRegistro());
		this.detalheVinMsgHistComOpSaidaDTO.setHrManutencaoRegistro(saida.getHrManutencaoRegistro());
		this.detalheVinMsgHistComOpSaidaDTO.setMensagem(saida.getMensagem());
		this.detalheVinMsgHistComOpSaidaDTO.setNmOperacaoFluxoInclusao(saida.getNmOperacaoFluxoInclusao());
		this.detalheVinMsgHistComOpSaidaDTO.setNmOperacaoFluxoManutencao(saida.getNmOperacaoFluxoManutencao());
		this.detalheVinMsgHistComOpSaidaDTO.setNrEventoLancamentoCredito(saida.getNrEventoLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setNrEventoLancamentoDebito(saida.getNrEventoLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setNrSequenciaContratoNegocio(saida.getNrSequenciaContratoNegocio());	
		this.detalheVinMsgHistComOpSaidaDTO.setDsMensagemLinhaCredito(saida.getDsMensagemLinhaCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsMensagemLinhaDebito(saida.getDsMensagemLinhaDebito());
		
		setDataHoraInclusao( this.detalheVinMsgHistComOpSaidaDTO.getHrInclusaoRegistro());
		setUsuarioInclusao( this.detalheVinMsgHistComOpSaidaDTO.getCdAutenticacaoSegurancaInclusao());
		setTipoCanalInclusao( (this.detalheVinMsgHistComOpSaidaDTO.getCdCanalInclusao() != 0 ? this.detalheVinMsgHistComOpSaidaDTO.getCdCanalInclusao() + " - " + this.detalheVinMsgHistComOpSaidaDTO.getDsCanalInclusao() : "" ) );
		setComplementoInclusao( (this.detalheVinMsgHistComOpSaidaDTO.getNmOperacaoFluxoInclusao()) != null && !(this.detalheVinMsgHistComOpSaidaDTO.getNmOperacaoFluxoInclusao().equals("")) ? this.detalheVinMsgHistComOpSaidaDTO.getNmOperacaoFluxoInclusao() : "");
		
		setDataHoraManutencao( this.detalheVinMsgHistComOpSaidaDTO.getHrManutencaoRegistro());
		setUsuarioManutencao( this.detalheVinMsgHistComOpSaidaDTO.getCdAutenticacaoSegurancaManutencao());
		setTipoCanalManutencao( (this.detalheVinMsgHistComOpSaidaDTO.getCdCanalManutencao()) != 0 ? this.detalheVinMsgHistComOpSaidaDTO.getCdCanalManutencao() + " - " + this.detalheVinMsgHistComOpSaidaDTO.getDsCanalManutencao() : "");
		setComplementoManutencao( (this.detalheVinMsgHistComOpSaidaDTO.getNmOperacaoFluxoManutencao()) != null && !(this.detalheVinMsgHistComOpSaidaDTO.getNmOperacaoFluxoManutencao().equals("")) ? this.detalheVinMsgHistComOpSaidaDTO.getNmOperacaoFluxoManutencao() : "");
	}	
	
	/**
	 * Preenche dados avancar incluir.
	 */
	private void preencheDadosAvancarIncluir(){
		
		ListarMsgHistCompSaidaDTO listarMsgHistCompSaidaDTO = getListaGridIncluir().get(getItemSelecionadoListaInc());
		
		DetalheMsgHistComOpEntradaDTO detalheMsgHistComOpEntradaDTO = new DetalheMsgHistComOpEntradaDTO();
		
		detalheMsgHistComOpEntradaDTO.setCdMensagemLinhaExtrato(listarMsgHistCompSaidaDTO.getCdMensagemLinhaExtrato());
		detalheMsgHistComOpEntradaDTO.setCdProdutoOperacaoRelacionado(listarMsgHistCompSaidaDTO.getCdProdutoOperacaoRelacionado());
		detalheMsgHistComOpEntradaDTO.setCdProdutoServicoOperacao(listarMsgHistCompSaidaDTO.getCdProdutoServicoOperacao());
		detalheMsgHistComOpEntradaDTO.setCdRelacionamentoProduto(listarMsgHistCompSaidaDTO.getCdRelacionadoProduto());

		DetalheMsgHistComOpSaidaDTO saida = getVincMsgHisCompOpeConService().detalharMsgHistoricoComplementar(detalheMsgHistComOpEntradaDTO);
		
		this.detalheVinMsgHistComOpSaidaDTO = new DetalheVinMsgHistComOpSaidaDTO();
		this.detalheVinMsgHistComOpSaidaDTO.setCdAutenticacaoSegurancaInclusao(saida.getCdAutenticacaoSegurancaInclusao());
		this.detalheVinMsgHistComOpSaidaDTO.setCdAutenticacaoSegurancaManutencao(saida.getCdAutenticacaoSegurancaManutencao());
		this.detalheVinMsgHistComOpSaidaDTO.setCdCanalInclusao(saida.getCdCanalInclusao());
		this.detalheVinMsgHistComOpSaidaDTO.setCdCanalManutencao(saida.getCdCanalManutencao());
		this.detalheVinMsgHistComOpSaidaDTO.setCdIdiomaLancamentoCredito(saida.getCdIdiomaLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdIdiomaLancamentoDebito(saida.getCdIdiomaLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdIndicadorRestricaoContrato(saida.getCdIndicadorRestricaoContrato());
		this.detalheVinMsgHistComOpSaidaDTO.setCdMensagemLinhaExtrato(saida.getCdMensagemLinhaExtrato());
		this.detalheVinMsgHistComOpSaidaDTO.setCdPessoaJuridicaContrato(saida.getCdPessoaJuridicaContrato());
		this.detalheVinMsgHistComOpSaidaDTO.setCdProdutoOperacaoRelacionado(saida.getCdProdutoOperacaoRelacionado());
		this.detalheVinMsgHistComOpSaidaDTO.setCdProdutoServicoOperacao(saida.getCdProdutoServicoOperacao());
		this.detalheVinMsgHistComOpSaidaDTO.setCdRecursoLancamentoCredito(saida.getCdRecursoLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdRecursoLancamentoDebito(saida.getCdRecursoLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdRelacionadoProduto(saida.getCdRelacionadoProduto());
		this.detalheVinMsgHistComOpSaidaDTO.setCdSistemaLancamentoCredito(saida.getCdSistemaLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdSistemaLancamentoDebito(saida.getCdSistemaLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setCdTipoContratoNegocio(saida.getCdTipoContratoNegocio());
		this.detalheVinMsgHistComOpSaidaDTO.setCdTipoMensagemExtrato(saida.getCdTipoMensagemExtrato());
		this.detalheVinMsgHistComOpSaidaDTO.setCodMensagem(saida.getCodMensagem());
		this.detalheVinMsgHistComOpSaidaDTO.setDsCanalInclusao(saida.getDsCanalInclusao());
		this.detalheVinMsgHistComOpSaidaDTO.setDsCanalManutencao(saida.getDsCanalManutencao());
		this.detalheVinMsgHistComOpSaidaDTO.setDsEventoLancamentoCredito(saida.getDsEventoLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsEventoLancamentoDebito(saida.getDsEventoLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsIdiomaLancamentoCredito(saida.getDsIdiomaLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsIdiomaLancamentoDebito(saida.getDsIdiomaLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsProdutoOperacaoRelacionado(saida.getDsProdutoOperacaoRelacionado());
		this.detalheVinMsgHistComOpSaidaDTO.setDsProdutoServicoOperacao(saida.getDsProdutoServicoOperacao());
		this.detalheVinMsgHistComOpSaidaDTO.setDsRecursoLancamentoCredito(saida.getDsRecursoLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsRecursoLancamentoDebito(saida.getDsRecursoLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsSistemaLancamentoCredito(saida.getDsSistemaLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsSistemaLancamentoDebito(saida.getDsSistemaLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setHrInclusaoRegistro(saida.getHrInclusaoRegistro());
		this.detalheVinMsgHistComOpSaidaDTO.setHrManutencaoRegistro(saida.getHrManutencaoRegistro());
		this.detalheVinMsgHistComOpSaidaDTO.setMensagem(saida.getMensagem());
		this.detalheVinMsgHistComOpSaidaDTO.setNmOperacaoFluxoInclusao(saida.getNmOperacaoFluxoInclusao());
		this.detalheVinMsgHistComOpSaidaDTO.setNmOperacaoFluxoManutencao(saida.getNmOperacaoFluxoManutencao());
		this.detalheVinMsgHistComOpSaidaDTO.setNrEventoLancamentoCredito(saida.getNrEventoLancamentoCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setNrEventoLancamentoDebito(saida.getNrEventoLancamentoDebito());
		this.detalheVinMsgHistComOpSaidaDTO.setNrSequenciaContratoNegocio(saida.getNrSequenciaContratoNegocio());			
		this.detalheVinMsgHistComOpSaidaDTO.setDsMensagemLinhaCredito(saida.getDsMensagemLinhaCredito());
		this.detalheVinMsgHistComOpSaidaDTO.setDsMensagemLinhaDebito(saida.getDsMensagemLinhaDebito());
	}	
		
	/**
	 * Listar tipo contrato.
	 */
	public void listarTipoContrato(){
		
		List<TipoContratoSaidaDTO> list = comboService.listarTipoContrato();

		for (TipoContratoSaidaDTO saida : list){
			listaTipoContrato.add(new SelectItem(saida.getCdTipoContrato(), saida.getDsTipoContrato()));
		}
	}

	/**
	 * Is habilita filtro de cliente.
	 *
	 * @return true, if is habilita filtro de cliente
	 */
	public boolean isHabilitaFiltroDeCliente() {
		return habilitaFiltroDeCliente;
	}

	/**
	 * Set: habilitaFiltroDeCliente.
	 *
	 * @param habilitaFiltroDeCliente the habilita filtro de cliente
	 */
	public void setHabilitaFiltroDeCliente(boolean habilitaFiltroDeCliente) {
		this.habilitaFiltroDeCliente = habilitaFiltroDeCliente;
	}

	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: listaTipoContrato.
	 *
	 * @return listaTipoContrato
	 */
	public List<SelectItem> getListaTipoContrato() {
		return listaTipoContrato;
	}

	/**
	 * Set: listaTipoContrato.
	 *
	 * @param listaTipoContrato the lista tipo contrato
	 */
	public void setListaTipoContrato(List<SelectItem> listaTipoContrato) {
		this.listaTipoContrato = listaTipoContrato;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: listaTipoServicoFiltro.
	 *
	 * @return listaTipoServicoFiltro
	 */
	public List<SelectItem> getListaTipoServicoFiltro() {
		return listaTipoServicoFiltro;
	}

	/**
	 * Set: listaTipoServicoFiltro.
	 *
	 * @param listaTipoServicoFiltro the lista tipo servico filtro
	 */
	public void setListaTipoServicoFiltro(List<SelectItem> listaTipoServicoFiltro) {
		this.listaTipoServicoFiltro = listaTipoServicoFiltro;
	}

	/**
	 * Get: listaTipoServicoFiltroHash.
	 *
	 * @return listaTipoServicoFiltroHash
	 */
	Map<Integer, String> getListaTipoServicoFiltroHash() {
		return listaTipoServicoFiltroHash;
	}

	/**
	 * Set lista tipo servico filtro hash.
	 *
	 * @param listaTipoServicoFiltroHash the lista tipo servico filtro hash
	 */
	void setListaTipoServicoFiltroHash(
			Map<Integer, String> listaTipoServicoFiltroHash) {
		this.listaTipoServicoFiltroHash = listaTipoServicoFiltroHash;
	}

	/**
	 * Get: listaModalidadeServicoFiltro.
	 *
	 * @return listaModalidadeServicoFiltro
	 */
	public List<SelectItem> getListaModalidadeServicoFiltro() {
		return listaModalidadeServicoFiltro;
	}

	/**
	 * Set: listaModalidadeServicoFiltro.
	 *
	 * @param listaModalidadeServicoFiltro the lista modalidade servico filtro
	 */
	public void setListaModalidadeServicoFiltro(
			List<SelectItem> listaModalidadeServicoFiltro) {
		this.listaModalidadeServicoFiltro = listaModalidadeServicoFiltro;
	}

	/**
	 * Get: listaModalidadeServicoFiltroHash.
	 *
	 * @return listaModalidadeServicoFiltroHash
	 */
	Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoFiltroHash() {
		return listaModalidadeServicoFiltroHash;
	}

	/**
	 * Set lista modalidade servico filtro hash.
	 *
	 * @param listaModalidadeServicoFiltroHash the lista modalidade servico filtro hash
	 */
	void setListaModalidadeServicoFiltroHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoFiltroHash) {
		this.listaModalidadeServicoFiltroHash = listaModalidadeServicoFiltroHash;
	}

	/**
	 * Get: modalidadeServicoFiltro.
	 *
	 * @return modalidadeServicoFiltro
	 */
	public Integer getModalidadeServicoFiltro() {
		return modalidadeServicoFiltro;
	}

	/**
	 * Set: modalidadeServicoFiltro.
	 *
	 * @param modalidadeServicoFiltro the modalidade servico filtro
	 */
	public void setModalidadeServicoFiltro(Integer modalidadeServicoFiltro) {
		this.modalidadeServicoFiltro = modalidadeServicoFiltro;
	}

	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Get: codigoHistoricoComplementarFiltro.
	 *
	 * @return codigoHistoricoComplementarFiltro
	 */
	public String getCodigoHistoricoComplementarFiltro() {
		return codigoHistoricoComplementarFiltro;
	}

	/**
	 * Set: codigoHistoricoComplementarFiltro.
	 *
	 * @param codigoHistoricoComplementarFiltro the codigo historico complementar filtro
	 */
	public void setCodigoHistoricoComplementarFiltro(
			String codigoHistoricoComplementarFiltro) {
		this.codigoHistoricoComplementarFiltro = codigoHistoricoComplementarFiltro;
	}
	
	/**
	 * Get: listaCodigoMensagem.
	 *
	 * @return listaCodigoMensagem
	 */
	public List<SelectItem> getListaCodigoMensagem() {
		return listaCodigoMensagem;
	}

	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<ListarVinMsgHistComOpSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(
			List<ListarVinMsgHistComOpSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: codigoHistoricoComplementar.
	 *
	 * @return codigoHistoricoComplementar
	 */
	public String getCodigoHistoricoComplementar() {
		return codigoHistoricoComplementar;
	}

	/**
	 * Set: codigoHistoricoComplementar.
	 *
	 * @param codigoHistoricoComplementar the codigo historico complementar
	 */
	public void setCodigoHistoricoComplementar(String codigoHistoricoComplementar) {
		this.codigoHistoricoComplementar = codigoHistoricoComplementar;
	}

	/**
	 * Get: itemSelecionadoListaInc.
	 *
	 * @return itemSelecionadoListaInc
	 */
	public Integer getItemSelecionadoListaInc() {
		return itemSelecionadoListaInc;
	}

	/**
	 * Set: itemSelecionadoListaInc.
	 *
	 * @param itemSelecionadoListaInc the item selecionado lista inc
	 */
	public void setItemSelecionadoListaInc(Integer itemSelecionadoListaInc) {
		this.itemSelecionadoListaInc = itemSelecionadoListaInc;
	}

	/**
	 * Get: listaModalidadeServico.
	 *
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {
		return listaModalidadeServico;
	}

	/**
	 * Set: listaModalidadeServico.
	 *
	 * @param listaModalidadeServico the lista modalidade servico
	 */
	public void setListaModalidadeServico(List<SelectItem> listaModalidadeServico) {
		this.listaModalidadeServico = listaModalidadeServico;
	}

	/**
	 * Get: listaModalidadeServicoHash.
	 *
	 * @return listaModalidadeServicoHash
	 */
	Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoHash() {
		return listaModalidadeServicoHash;
	}

	/**
	 * Set lista modalidade servico hash.
	 *
	 * @param listaModalidadeServicoHash the lista modalidade servico hash
	 */
	void setListaModalidadeServicoHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash) {
		this.listaModalidadeServicoHash = listaModalidadeServicoHash;
	}

	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public Integer getModalidadeServico() {
		return modalidadeServico;
	}

	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(Integer modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public Integer getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(Integer tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Get: listaGridIncluir.
	 *
	 * @return listaGridIncluir
	 */
	public List<ListarMsgHistCompSaidaDTO> getListaGridIncluir() {
		return listaGridIncluir;
	}

	/**
	 * Set: listaGridIncluir.
	 *
	 * @param listaGridIncluir the lista grid incluir
	 */
	public void setListaGridIncluir(List<ListarMsgHistCompSaidaDTO> listaGridIncluir) {
		this.listaGridIncluir = listaGridIncluir;
	}

	/**
	 * Get: detalheVinMsgHistComOpSaidaDTO.
	 *
	 * @return detalheVinMsgHistComOpSaidaDTO
	 */
	public DetalheVinMsgHistComOpSaidaDTO getDetalheVinMsgHistComOpSaidaDTO() {
		return detalheVinMsgHistComOpSaidaDTO;
	}

	/**
	 * Set: detalheVinMsgHistComOpSaidaDTO.
	 *
	 * @param detalheVinMsgHistComOpSaidaDTO the detalhe vin msg hist com op saida dto
	 */
	public void setDetalheVinMsgHistComOpSaidaDTO(
			DetalheVinMsgHistComOpSaidaDTO detalheVinMsgHistComOpSaidaDTO) {
		this.detalheVinMsgHistComOpSaidaDTO = detalheVinMsgHistComOpSaidaDTO;
	}

	/**
	 * Is hidden confirma.
	 *
	 * @return true, if is hidden confirma
	 */
	public boolean isHiddenConfirma() {
		return hiddenConfirma;
	}

	/**
	 * Set: hiddenConfirma.
	 *
	 * @param hiddenConfirma the hidden confirma
	 */
	public void setHiddenConfirma(boolean hiddenConfirma) {
		this.hiddenConfirma = hiddenConfirma;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	
	/**
	 * Get: cdRecursoDebito.
	 *
	 * @return cdRecursoDebito
	 */
	public int getCdRecursoDebito() {
		return cdRecursoDebito;
	}

	/**
	 * Set: cdRecursoDebito.
	 *
	 * @param cdRecursoDebito the cd recurso debito
	 */
	public void setCdRecursoDebito(int cdRecursoDebito) {
		this.cdRecursoDebito = cdRecursoDebito;
	}

	/**
	 * Get: cdRecursoCredito.
	 *
	 * @return cdRecursoCredito
	 */
	public int getCdRecursoCredito() {
		return cdRecursoCredito;
	}

	/**
	 * Set: cdRecursoCredito.
	 *
	 * @param cdRecursoCredito the cd recurso credito
	 */
	public void setCdRecursoCredito(int cdRecursoCredito) {
		this.cdRecursoCredito = cdRecursoCredito;
	}

	
	
	
}
