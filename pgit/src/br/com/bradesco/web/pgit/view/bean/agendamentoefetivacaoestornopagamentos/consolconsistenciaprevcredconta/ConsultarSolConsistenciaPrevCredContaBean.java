/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.consolconsistenciaprevcredconta
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.consolconsistenciaprevcredconta;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoSolConsistenciaPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoSolConsistenciaPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.IConSolConsistenciaPrevCredContaService;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.AprovarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.AprovarSolicitacaoConsistPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ConsultarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ConsultarSolicitacaoConsistPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.RejeitarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.RejeitarSolicitacaoConsistPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ReprocessarSolicitacaoConsistPreviaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consolconsistenciaprevcredconta.bean.ReprocessarSolicitacaoConsistPreviaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;



/**
 * Nome: ConsultarSolConsistenciaPrevCredContaBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConsultarSolConsistenciaPrevCredContaBean {
	
	//Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_SOLICITACAO_CONSISTENCIA_PREVIA. */
	private static final String CON_SOLICITACAO_CONSISTENCIA_PREVIA = "conSolicitacaoConsistenciaPrevia";
	
	//ConsultarSolicitacaoConsistenciaPrevia
	/** Atributo listaGridPesquisa. */
	private List<ConsultarSolicitacaoConsistPreviaSaidaDTO> listaGridPesquisa;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio;
	
	/** Atributo periodoSolInicioFiltro. */
	private Date periodoSolInicioFiltro;
	
	/** Atributo periodoSolFimFiltro. */
	private Date periodoSolFimFiltro;
	
	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	
	/** Atributo cdSituacaoFiltro. */
	private Integer cdSituacaoFiltro;
	
	/** Atributo listaSituacaoSolConsistenciaPrevia. */
	private List<SelectItem> listaSituacaoSolConsistenciaPrevia;
	
	/** Atributo listaSituacaoSolConsistenciaPreviaHash. */
	private Map<Integer,String> listaSituacaoSolConsistenciaPreviaHash = new HashMap<Integer,String>();
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean; 
	
	//reprocessarSolicitacaoConsistenciaPrevia
	//aprovarSolicitacaoConsistenciaPrevia
	//reprocessarSolicitacaoConsistenciaPrevia
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;
	
	/** Atributo empresaConglomerado. */
	private String empresaConglomerado;
	
	/** Atributo numeroContrato. */
	private String numeroContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo situacaoContrato. */
	private String situacaoContrato;
	
	/** Atributo dataInclusao. */
	private String dataInclusao;
	
	/** Atributo situacaoSolicitacao. */
	private String situacaoSolicitacao;
	
	/** Atributo dataGravacaoLote. */
	private String dataGravacaoLote;
	
	/** Atributo tipoLote. */
	private String tipoLote;
	
	/** Atributo nrLote. */
	private String nrLote;
	
	/** Atributo controleTransmissao. */
	private String controleTransmissao;
	
	/** Atributo dataHraInicioProc. */
	private String dataHraInicioProc;
	
	/** Atributo volTotalLote. */
	private String volTotalLote;
	
	/** Atributo valorTotalLote. */
	private String valorTotalLote;
	
	/** Atributo situacaoLote. */
	private String situacaoLote;
		
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo conSolConsistenciaPrevCredContaImpl. */
	private IConSolConsistenciaPrevCredContaService conSolConsistenciaPrevCredContaImpl;
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	//M�todos
	/**
	 * Iniciar tela.
	 *
	 * @param e the e
	 */
	public void iniciarTela(ActionEvent e){
		carregaListaSituacao();
		limparDados();
		limpar();
		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
		
		//Tela de Filtro
		filtroAgendamentoEfetivacaoEstornoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		
		filtroAgendamentoEfetivacaoEstornoBean.setPaginaRetorno(CON_SOLICITACAO_CONSISTENCIA_PREVIA);
		setDisableArgumentosConsulta(false);
	}
	

	//conSolicitacaoConsistenciaPrevia
	/**
	 * Carrega lista consultar.
	 */
	public void carregaListaConsultar(){
		
		listaGridPesquisa = new ArrayList<ConsultarSolicitacaoConsistPreviaSaidaDTO>();
		setDisableArgumentosConsulta(true);
		try{
			ConsultarSolicitacaoConsistPreviaEntradaDTO entradaDTO = new ConsultarSolicitacaoConsistPreviaEntradaDTO();
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			entradaDTO.setDtCreditoPagamentoInicial((getPeriodoSolInicioFiltro() == null) ? "" : sdf.format(getPeriodoSolInicioFiltro()) );
			entradaDTO.setDtCreditoPagamentoFinal((getPeriodoSolFimFiltro() == null) ? "" : sdf.format(getPeriodoSolFimFiltro()));
			
			entradaDTO.setCdSituacaoLotePagamento(getCdSituacaoFiltro());
			
			/* Se Selecionado Filtrar por Contrato*/
			if(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("1") ){
				//Mover valor do combo Empresa 
				entradaDTO.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getEmpresaGestoraFiltro());
				//mover valor do combo Tipo Contrato
				entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getTipoContratoFiltro());
				//mover valor do campo Numero Contrato
				entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(filtroAgendamentoEfetivacaoEstornoBean.getNumeroFiltro()));				
			}else{
				entradaDTO.setCdPessoaJuridicaContrato(0L);
				entradaDTO.setCdTipoContratoNegocio(0);
				entradaDTO.setNrSequenciaContratoNegocio(0L);				
			}

			/* Se Selecionado Filtrar  por Conta D�bito */
			if ( filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("2") ){
				//mover valor do campo Banco
				entradaDTO.setCdContaDebito(Long.parseLong(String.valueOf(filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro())));
				//mover valor do campo Ag�ncia
				entradaDTO.setCdAgenciaDebito(filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro());
				//mover valor do campo Conta
				entradaDTO.setCdContaDebito(filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro()); 
				//mover valor do campo Digito Conta
				entradaDTO.setDigitoContaDebito(filtroAgendamentoEfetivacaoEstornoBean.getDigitoContaDebitoFiltro());
			}else{
				entradaDTO.setCdContaDebito(0L);
				entradaDTO.setCdAgenciaDebito(0);
				entradaDTO.setCdContaDebito(0L); 
				entradaDTO.setDigitoContaDebito("");
			}	
		
		
			
			
			setListaGridPesquisa(getConSolConsistenciaPrevCredContaImpl().consultarSolicitacaoConsistPrevia(entradaDTO));
			
			this.listaControleRadio = new ArrayList<SelectItem>();
			
			for(int i = 0;i<getListaGridPesquisa().size();i++){
				listaControleRadio.add(new SelectItem(i," "));
			}
			
			setItemSelecionadoLista(null);
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridPesquisa(null);
			setItemSelecionadoLista(null);
			setDisableArgumentosConsulta(false);
		}
	}
	
	/**
	 * Carrega lista situacao.
	 */
	public void carregaListaSituacao(){	
		try{
			this.listaSituacaoSolConsistenciaPrevia = new ArrayList<SelectItem>();
	
			List<ListarSituacaoSolConsistenciaPreviaSaidaDTO> saidaDTO = new ArrayList<ListarSituacaoSolConsistenciaPreviaSaidaDTO>();
			
			ListarSituacaoSolConsistenciaPreviaEntradaDTO entrada = new ListarSituacaoSolConsistenciaPreviaEntradaDTO();
			
			saidaDTO = comboService.listarSituacaoSolConsistenciaPrevia();
			entrada.setCdSituacao(getCdSituacaoFiltro());
			
			listaSituacaoSolConsistenciaPreviaHash.clear();
	
			for(ListarSituacaoSolConsistenciaPreviaSaidaDTO combo : saidaDTO){
				listaSituacaoSolConsistenciaPreviaHash.put(combo.getCodigo(), combo.getDescricao());
				this.listaSituacaoSolConsistenciaPrevia.add(new SelectItem(combo.getCodigo(), combo.getDescricao()));
			}
		}catch(PdcAdapterFunctionalException p){
			this.listaSituacaoSolConsistenciaPrevia = new ArrayList<SelectItem>();
		}
	}
	
	/**
	 * Confirmar reprocessar.
	 *
	 * @return the string
	 */
	public String confirmarReprocessar(){
				
 	try{
			ReprocessarSolicitacaoConsistPreviaEntradaDTO entradaDTO = new ReprocessarSolicitacaoConsistPreviaEntradaDTO();
			ConsultarSolicitacaoConsistPreviaSaidaDTO saidaLista = listaGridPesquisa.get(itemSelecionadoLista);
						
			entradaDTO.setCdConsistenciaPreviaPagamento(saidaLista.getCdConsistenciaPreviaPagamento());
			entradaDTO.setCdPessoaJuridicaContrato(saidaLista.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(saidaLista.getCdTipoContratoNegocio());
			entradaDTO.setNrSequenciaContratoNegocio(saidaLista.getNrSequenciaContratoNegocio());
			
			
			ReprocessarSolicitacaoConsistPreviaSaidaDTO reprocessarSolicitacaoConsistPreviaSaidaDTO = getConSolConsistenciaPrevCredContaImpl().reprocessarSolicitacaoConsistPrevia(entradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" + reprocessarSolicitacaoConsistPreviaSaidaDTO.getCdMensagem() + ") " + reprocessarSolicitacaoConsistPreviaSaidaDTO.getMensagem(), CON_SOLICITACAO_CONSISTENCIA_PREVIA, BradescoViewExceptionActionType.ACTION, false);
			
			if (getListaGridPesquisa() != null && getListaGridPesquisa().size() > 0){
				carregaListaConsultar();
			}		
					
			return "OK";
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "+ p.getMessage(), false);
			return "";
		} 
		
	}
	
	/**
	 * Confirmar aprovar.
	 *
	 * @return the string
	 */
	public String confirmarAprovar(){
		
		try{
			AprovarSolicitacaoConsistPreviaEntradaDTO entradaDTO = new AprovarSolicitacaoConsistPreviaEntradaDTO();
			ConsultarSolicitacaoConsistPreviaSaidaDTO saidaLista = listaGridPesquisa.get(itemSelecionadoLista);
			
			entradaDTO.setCdConsistenciaPreviaPagamento(saidaLista.getCdConsistenciaPreviaPagamento());
			entradaDTO.setCdPessoaJuridicaContrato(saidaLista.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(saidaLista.getCdTipoContratoNegocio());
			entradaDTO.setNrSequenciaContratoNegocio(saidaLista.getNrSequenciaContratoNegocio());
			
			AprovarSolicitacaoConsistPreviaSaidaDTO aprovarSolicitacaoConsistPreviaSaidaDTO = getConSolConsistenciaPrevCredContaImpl().aprovarSolicitacaoConsistPrevia(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + aprovarSolicitacaoConsistPreviaSaidaDTO.getCdMensagem() + ") " + aprovarSolicitacaoConsistPreviaSaidaDTO.getMensagem(), CON_SOLICITACAO_CONSISTENCIA_PREVIA, BradescoViewExceptionActionType.ACTION, false);
			if (getListaGridPesquisa() != null && getListaGridPesquisa().size() > 0){
				carregaListaConsultar();
			}
			return "OK";
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "+ p.getMessage(), false);
			return "";
		}	
	}
	
	/**
	 * Confirmar rejeitar.
	 *
	 * @return the string
	 */
	public String confirmarRejeitar(){
		try{
			RejeitarSolicitacaoConsistPreviaEntradaDTO entradaDTO = new RejeitarSolicitacaoConsistPreviaEntradaDTO();
			ConsultarSolicitacaoConsistPreviaSaidaDTO saidaLista = listaGridPesquisa.get(itemSelecionadoLista);
			
			entradaDTO.setCdConsistenciaPreviaPagamento(saidaLista.getCdConsistenciaPreviaPagamento());
			entradaDTO.setCdPessoaJuridicaContrato(saidaLista.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(saidaLista.getCdTipoContratoNegocio());
			entradaDTO.setNrSequenciaContratoNegocio(saidaLista.getNrSequenciaContratoNegocio());
			
			RejeitarSolicitacaoConsistPreviaSaidaDTO rejeitarSolicitacaoConsistPreviaSaidaDTO = getConSolConsistenciaPrevCredContaImpl().rejeitarSolicitacaoConsistPrevia(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + rejeitarSolicitacaoConsistPreviaSaidaDTO.getCdMensagem() + ") " + rejeitarSolicitacaoConsistPreviaSaidaDTO.getMensagem(), CON_SOLICITACAO_CONSISTENCIA_PREVIA, BradescoViewExceptionActionType.ACTION, false);
			if (getListaGridPesquisa() != null && getListaGridPesquisa().size() > 0){
				carregaListaConsultar();
			}
			return "OK";
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "+ p.getMessage(), false);
			return "";
		} 
		
	}
	
	/**
	 * Aprovar.
	 *
	 * @return the string
	 */
	public String aprovar(){
		limpar();
		preencheDados();
		return "APROVAR";
	}
	
	/**
	 * Rejeitar.
	 *
	 * @return the string
	 */
	public String rejeitar(){
		limpar();
		preencheDados();
		return "REJEITAR";
	}
	
	/**
	 * Reprocessar.
	 *
	 * @return the string
	 */
	public String reprocessar(){
		limpar();
		preencheDados();
		return "REPROCESSAR";
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		setItemSelecionadoLista(null);
		
		return "VOLTAR";
	}
	
//	conSolicitacaoConsistenciaPrevia	
	/**
 * Limpar dados.
 */
public void limparDados(){
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
	}
	
//	conSolicitacaoConsistenciaPrevia	
	/**
 * Limpar variaveis.
 */
public void limparVariaveis(){
		setCpfCnpj(null);
		setNomeRazaoSocial("");
		setEmpresaConglomerado("");
		setNumeroContrato("");
		setDescricaoContrato("");
		setSituacaoContrato("");
	}
	
	/**
	 * Carrega cabecalho.
	 */
	private void carregaCabecalho(){
		limparVariaveis();
		if(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("0")){ //Filtrar por Cliente
			setCpfCnpj(filtroAgendamentoEfetivacaoEstornoBean.getNrCpfCnpjCliente());
			setNomeRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoRazaoSocialCliente());
			setEmpresaConglomerado(filtroAgendamentoEfetivacaoEstornoBean.getEmpresaGestoraDescCliente());
			setNumeroContrato(filtroAgendamentoEfetivacaoEstornoBean.getNumeroDescCliente());
			setDescricaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoContratoDescCliente());
			setSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getSituacaoDescCliente());
		}else{
			if(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("1")){ //Filtrar por Contrato
				setEmpresaConglomerado(filtroAgendamentoEfetivacaoEstornoBean.getEmpresaGestoraDescContrato());
				setNumeroContrato(filtroAgendamentoEfetivacaoEstornoBean.getNumeroDescContrato());
				setDescricaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoContratoDescContrato());
				setSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getSituacaoDescContrato());
			}
		}
	}
	
	//Aprovar, Rejeitar, Reprocessar
	/**
	 * Preenche dados.
	 */
	private void preencheDados() {
		ConsultarSolicitacaoConsistPreviaSaidaDTO consultarSolicitacaoConsistPreviaSaidaDTO =   getListaGridPesquisa().get(getItemSelecionadoLista());
		
		carregaCabecalho();
		
		setDataInclusao(consultarSolicitacaoConsistPreviaSaidaDTO.getHrInclusaoPagamento());
		setDataGravacaoLote(consultarSolicitacaoConsistPreviaSaidaDTO.getDtGravacaoLote());
		setTipoLote(String.valueOf(consultarSolicitacaoConsistPreviaSaidaDTO.getCdTipoLote()));
		setNrLote(String.valueOf(consultarSolicitacaoConsistPreviaSaidaDTO.getNrLoteArquivoRemessa()));
		setDataHraInicioProc(consultarSolicitacaoConsistPreviaSaidaDTO.getDtProcessoLote());
		setVolTotalLote(NumberUtils.format(consultarSolicitacaoConsistPreviaSaidaDTO.getVlTotal(),"#,##0.00"));
		setValorTotalLote(NumberUtils.format(consultarSolicitacaoConsistPreviaSaidaDTO.getVlTotal(),"#,##0.00"));
		setSituacaoLote(consultarSolicitacaoConsistPreviaSaidaDTO.getDsSituacaoLotePagamento());
		setControleTransmissao(String.valueOf(consultarSolicitacaoConsistPreviaSaidaDTO.getCdControleTransacao()));
		setSituacaoSolicitacao(consultarSolicitacaoConsistPreviaSaidaDTO.getDsSituacaoSolicitacao()); 
		
				
	}
	
//Aprovar, Rejeitar, Reprocessar
	/**
 * Limpar.
 */
public void limpar(){
		setCpfCnpj(null);
		setNomeRazaoSocial("");
		setEmpresaConglomerado("");
		setSituacaoContrato("");
		setNumeroContrato("");
		setDataInclusao("");
		setDataGravacaoLote("");
		setTipoLote("");
		setNrLote("");
		setDataHraInicioProc("");
		setVolTotalLote("");
		setValorTotalLote("");
		setSituacaoLote("");
		setControleTransmissao("");
		setSituacaoSolicitacao(""); 
		
	}
	
//	conSolicitacaoConsistenciaPrevia
	/**
 * Limpar campos.
 */
public void limparCampos(){
			
		setPeriodoSolFimFiltro(new Date());
		setPeriodoSolInicioFiltro(new Date());
		setCdSituacaoFiltro(0);
			
		
	}
	

		
//	conSolicitacaoConsistenciaPrevia
	/**
 * Controlar argumentos pesquisa.
 */
public void controlarArgumentosPesquisa(){
		if (filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro() == null || filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro().equals("") ||
		filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro() == null || filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro().equals("") ||
		filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro() == null || filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro().equals("") ||
		filtroAgendamentoEfetivacaoEstornoBean.getDigitoContaDebitoFiltro() == null || filtroAgendamentoEfetivacaoEstornoBean.getDigitoContaDebitoFiltro().equals("")){
			setHabilitaArgumentosPesquisa(false);
			limparCampos();
		}
		else{
			setHabilitaArgumentosPesquisa(true);
		}
	}

//	conSolicitacaoConsistenciaPrevia
	/**
 * Limpar filtro principal.
 */
public void limparFiltroPrincipal(){
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		setHabilitaArgumentosPesquisa(false);
		limparCampos();
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
		
	}	
	
	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente(){
		//Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
		
		limparCampos();
		
		return "";
	}	
	
//	conSolicitacaoConsistenciaPrevia
	/**
 * Limpar args lista apos pesquisa cliente contrato.
 *
 * @param evt the evt
 */
public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt){
		limparCampos();
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
	}
	
//	conSolicitacaoConsistenciaPrevia
	/**
 * Limpar consulta.
 */
public void limparConsulta(){
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado("");
		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
		limparCampos();
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt){
		carregaListaConsultar();
		return "";
	}
	
	//Getters e Setters
	
	
	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}
	
	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}
	
	/**
	 * Get: empresaConglomerado.
	 *
	 * @return empresaConglomerado
	 */
	public String getEmpresaConglomerado() {
		return empresaConglomerado;
	}
	
	/**
	 * Set: empresaConglomerado.
	 *
	 * @param empresaConglomerado the empresa conglomerado
	 */
	public void setEmpresaConglomerado(String empresaConglomerado) {
		this.empresaConglomerado = empresaConglomerado;
	}
	
	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}
	
	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}
	
	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}
	
	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}
	
	/**
	 * Get: dataGravacaoLote.
	 *
	 * @return dataGravacaoLote
	 */
	public String getDataGravacaoLote() {
		return dataGravacaoLote;
	}
	
	/**
	 * Set: dataGravacaoLote.
	 *
	 * @param dataGravacaoLote the data gravacao lote
	 */
	public void setDataGravacaoLote(String dataGravacaoLote) {
		this.dataGravacaoLote = dataGravacaoLote;
	}
	
	/**
	 * Get: dataInclusao.
	 *
	 * @return dataInclusao
	 */
	public String getDataInclusao() {
		return dataInclusao;
	}
	
	/**
	 * Set: dataInclusao.
	 *
	 * @param dataInclusao the data inclusao
	 */
	public void setDataInclusao(String dataInclusao) {
		this.dataInclusao = dataInclusao;
	}
	
	/**
	 * Get: situacaoSolicitacao.
	 *
	 * @return situacaoSolicitacao
	 */
	public String getSituacaoSolicitacao() {
		return situacaoSolicitacao;
	}
	
	/**
	 * Set: situacaoSolicitacao.
	 *
	 * @param situacaoSolicitacao the situacao solicitacao
	 */
	public void setSituacaoSolicitacao(String situacaoSolicitacao) {
		this.situacaoSolicitacao = situacaoSolicitacao;
	}
	
	/**
	 * Get: controleTransmissao.
	 *
	 * @return controleTransmissao
	 */
	public String getControleTransmissao() {
		return controleTransmissao;
	}
	
	/**
	 * Set: controleTransmissao.
	 *
	 * @param controleTransmissao the controle transmissao
	 */
	public void setControleTransmissao(String controleTransmissao) {
		this.controleTransmissao = controleTransmissao;
	}
	
	/**
	 * Get: tipoLote.
	 *
	 * @return tipoLote
	 */
	public String getTipoLote() {
		return tipoLote;
	}
	
	/**
	 * Set: tipoLote.
	 *
	 * @param tipoLote the tipo lote
	 */
	public void setTipoLote(String tipoLote) {
		this.tipoLote = tipoLote;
	}
	
	/**
	 * Get: dataHraInicioProc.
	 *
	 * @return dataHraInicioProc
	 */
	public String getDataHraInicioProc() {
		return dataHraInicioProc;
	}
	
	/**
	 * Set: dataHraInicioProc.
	 *
	 * @param dataHraInicioProc the data hra inicio proc
	 */
	public void setDataHraInicioProc(String dataHraInicioProc) {
		this.dataHraInicioProc = dataHraInicioProc;
	}
	
	/**
	 * Get: valorTotalLote.
	 *
	 * @return valorTotalLote
	 */
	public String getValorTotalLote() {
		return valorTotalLote;
	}
	
	/**
	 * Set: valorTotalLote.
	 *
	 * @param valorTotalLote the valor total lote
	 */
	public void setValorTotalLote(String valorTotalLote) {
		this.valorTotalLote = valorTotalLote;
	}
	
	
	/**
	 * Get: nrLote.
	 *
	 * @return nrLote
	 */
	public String getNrLote() {
		return nrLote;
	}

	/**
	 * Set: nrLote.
	 *
	 * @param nrLote the nr lote
	 */
	public void setNrLote(String nrLote) {
		this.nrLote = nrLote;
	}

	/**
	 * Get: volTotalLote.
	 *
	 * @return volTotalLote
	 */
	public String getVolTotalLote() {
		return volTotalLote;
	}

	/**
	 * Set: volTotalLote.
	 *
	 * @param volTotalLote the vol total lote
	 */
	public void setVolTotalLote(String volTotalLote) {
		this.volTotalLote = volTotalLote;
	}

	/**
	 * Get: situacaoLote.
	 *
	 * @return situacaoLote
	 */
	public String getSituacaoLote() {
		return situacaoLote;
	}
	
	/**
	 * Set: situacaoLote.
	 *
	 * @param situacaoLote the situacao lote
	 */
	public void setSituacaoLote(String situacaoLote) {
		this.situacaoLote = situacaoLote;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<ConsultarSolicitacaoConsistPreviaSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(
			List<ConsultarSolicitacaoConsistPreviaSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: conSolConsistenciaPrevCredContaImpl.
	 *
	 * @return conSolConsistenciaPrevCredContaImpl
	 */
	public IConSolConsistenciaPrevCredContaService getConSolConsistenciaPrevCredContaImpl() {
		return conSolConsistenciaPrevCredContaImpl;
	}

	/**
	 * Set: conSolConsistenciaPrevCredContaImpl.
	 *
	 * @param conSolConsistenciaPrevCredContaImpl the con sol consistencia prev cred conta impl
	 */
	public void setConSolConsistenciaPrevCredContaImpl(
			IConSolConsistenciaPrevCredContaService conSolConsistenciaPrevCredContaImpl) {
		this.conSolConsistenciaPrevCredContaImpl = conSolConsistenciaPrevCredContaImpl;
	}

	/**
	 * Get: cdSituacaoFiltro.
	 *
	 * @return cdSituacaoFiltro
	 */
	public Integer getCdSituacaoFiltro() {
		return cdSituacaoFiltro;
	}

	/**
	 * Set: cdSituacaoFiltro.
	 *
	 * @param cdSituacaoFiltro the cd situacao filtro
	 */
	public void setCdSituacaoFiltro(Integer cdSituacaoFiltro) {
		this.cdSituacaoFiltro = cdSituacaoFiltro;
	}

	/**
	 * Get: listaSituacaoSolConsistenciaPrevia.
	 *
	 * @return listaSituacaoSolConsistenciaPrevia
	 */
	public List<SelectItem> getListaSituacaoSolConsistenciaPrevia() {
		return listaSituacaoSolConsistenciaPrevia;
	}

	/**
	 * Set: listaSituacaoSolConsistenciaPrevia.
	 *
	 * @param listaSituacaoSolConsistenciaPrevia the lista situacao sol consistencia previa
	 */
	public void setListaSituacaoSolConsistenciaPrevia(
			List<SelectItem> listaSituacaoSolConsistenciaPrevia) {
		this.listaSituacaoSolConsistenciaPrevia = listaSituacaoSolConsistenciaPrevia;
	}

	/**
	 * Get: periodoSolFimFiltro.
	 *
	 * @return periodoSolFimFiltro
	 */
	public Date getPeriodoSolFimFiltro() {
		return periodoSolFimFiltro;
	}

	/**
	 * Set: periodoSolFimFiltro.
	 *
	 * @param periodoSolFimFiltro the periodo sol fim filtro
	 */
	public void setPeriodoSolFimFiltro(Date periodoSolFimFiltro) {
		this.periodoSolFimFiltro = periodoSolFimFiltro;
	}

	/**
	 * Get: periodoSolInicioFiltro.
	 *
	 * @return periodoSolInicioFiltro
	 */
	public Date getPeriodoSolInicioFiltro() {
		return periodoSolInicioFiltro;
	}

	/**
	 * Set: periodoSolInicioFiltro.
	 *
	 * @param periodoSolInicioFiltro the periodo sol inicio filtro
	 */
	public void setPeriodoSolInicioFiltro(Date periodoSolInicioFiltro) {
		this.periodoSolInicioFiltro = periodoSolInicioFiltro;
	}
	
	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}


	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}


	/**
	 * Get: listaSituacaoSolConsistenciaPreviaHash.
	 *
	 * @return listaSituacaoSolConsistenciaPreviaHash
	 */
	public Map<Integer, String> getListaSituacaoSolConsistenciaPreviaHash() {
		return listaSituacaoSolConsistenciaPreviaHash;
	}


	/**
	 * Set lista situacao sol consistencia previa hash.
	 *
	 * @param listaSituacaoSolConsistenciaPreviaHash the lista situacao sol consistencia previa hash
	 */
	public void setListaSituacaoSolConsistenciaPreviaHash(
			Map<Integer, String> listaSituacaoSolConsistenciaPreviaHash) {
		this.listaSituacaoSolConsistenciaPreviaHash = listaSituacaoSolConsistenciaPreviaHash;
	}


	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}


	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}
	
	
	
}
