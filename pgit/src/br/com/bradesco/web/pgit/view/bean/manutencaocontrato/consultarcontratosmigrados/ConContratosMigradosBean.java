/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.consultarcontratosmigrados
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.consultarcontratosmigrados;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.IConContratosMigradosService;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.ConContratoMigradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.ConContratoMigradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.DetalharContratoMigradoHsbcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.DetalharContratoMigradoHsbcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.RevertContratoMigradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.concontratosmigrados.bean.RevertContratoMigradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarUsuarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarUsuarioSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

// TODO: Auto-generated Javadoc
/**
 * Nome: ConContratosMigradosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ConContratosMigradosBean {
	
	/** The combo service. */
	private IComboService comboService;
	
	/** The con contratos migrados service impl. */
	private IConContratosMigradosService conContratosMigradosServiceImpl;
	
	private IManterContratoService manterContratoImpl;
	
	/** The saida detalhar contrato migrado hsbc. */
	private DetalharContratoMigradoHsbcSaidaDTO saidaDetalharContratoMigradoHsbc = null;
	
	/** The lista contratos migrados. */
	private List<ConContratoMigradoSaidaDTO> listaContratosMigrados;
	
	/** The lista cbo custo origem. */
	private List<SelectItem> listaCboCustoOrigem;
	
	/** The lista controle. */
	private List<SelectItem> listaControle;
	
	/** The lista empresa gestora. */
	private List<SelectItem> listaEmpresaGestora = new ArrayList<SelectItem>();
	
	/** The lista situacao. */
	private List<SelectItem> listaSituacao = new ArrayList<SelectItem>();
	
	/** The lista tipo contrato. */
	private List<SelectItem> listaTipoContrato = new ArrayList<SelectItem>();

	/** The lista situacao hash. */
	private Map<Integer,String> listaSituacaoHash = new HashMap<Integer,String>();
	
	/** The lista empresa gestora hash. */
	private Map<Long, String> listaEmpresaGestoraHash = new HashMap<Long, String>();

	/** The habilita desfazer reversao. */
	private boolean habilitaDesfazerReversao;
	
	/** The habilita reverter. */
	private boolean habilitaReverter;
	
	/** The data migracao a. */
	private Date dataMigracaoA;
	
	/** The data migracao de. */
	private Date dataMigracaoDe;
	
	/** The agencia. */
	private Integer agencia;
	
	/** The cbo custo origem. */
	private Integer cboCustoOrigem;
	
	/** The cbo situacao. */
	private Integer cboSituacao;
	
	/** The cbo tp contrato. */
	private Integer cboTpContrato;
	
	/** The conta. */
	private Integer conta;
	
	/** The digito cnpj. */
	private Integer digitoCnpj;
	
	/** The filia cnpj. */
	private Integer filiaCnpj;
	
	/** The item selecionado. */
	private Integer itemSelecionado;
	
	/** The cbo empresa gestora cont. */
	private Long cboEmpresaGestoraCont;
	
	/** The cod empresa perfil. */
	private Long codEmpresaPerfil;
	
	/** The corpo cpf cnpj. */
	private Long corpoCpfCnpj;
	
	/** The det numero. */
	private Long detNumero;
	
	/** The numero. */
	private Long numero;
	
	/** The atividade economica. */
	private String atividadeEconomica;
	
	/** The complemento inclusao. */
	private String complementoInclusao;
	
	/** The complemento manutencao. */
	private String complementoManutencao;
	
	/** The cpf cnpj. */
	private String cpfCnpj;
	
	/** The cpf cnpj master. */
	private String cpfCnpjMaster;
	
	/** The data inclusao. */
	private String dataInclusao;
	
	/** The data manutencao. */
	private String dataManutencao;
	
	/** The descricao. */
	private String descricao;
	
	/** The ds agencia gestora. */
	private String dsAgenciaGestora;
	
	/** The ds setor contrato. */
	private String dsSetorContrato;
	
	/** The ds situacao. */
	private String dsSituacao;
	
	/** The dt hora assinatura. */
	private String dtHoraAssinatura;
	
	/** The empresa gestora. */
	private String empresaGestora;
	
	/** The fim vigencia contrato. */
	private String fimVigenciaContrato;
	
	/** The gerente responsavel. */
	private String gerenteResponsavel;
	
	/** The grupo economico. */
	private String grupoEconomico;
	
	/** The hora inclusao. */
	private String horaInclusao;
	
	/** The hora manutencao. */
	private String horaManutencao;
	
	/** The idioma. */
	private String idioma;
	
	/** The inicio vigencia. */
	private String inicioVigencia;
	
	/** The moeda. */
	private String moeda;
	
	/** The motivo. */
	private String motivo;
	
	/** The nome razao master. */
	private String nomeRazaoMaster;
	
	/** The nome razao social. */
	private String nomeRazaoSocial;
	
	/** The num comercial. */
	private String numComercial;
	
	/** The origem. */
	private String origem;
	
	/** The participacao. */
	private String participacao;
	
	/** The possui aditivo. */
	private String possuiAditivo;
	
	/** The radio pesquisa. */
	private String radioPesquisa;
	
	/** The segmento. */
	private String segmento;
	
	/** The situacao. */
	private String situacao;
	
	/** The sub segmento. */
	private String subSegmento;
	
	/** The tipo canal inclusao. */
	private String tipoCanalInclusao;
	
	/** The tipo canal manutencao. */
	private String tipoCanalManutencao;
	
	/** The tp contrato. */
	private String tpContrato;
	
	/** The usuario inclusao. */
	private String usuarioInclusao;
	
	/** The usuario manutencao. */
	private String usuarioManutencao;
	
	private ValidarUsuarioSaidaDTO saidaValidarUsuario;


	/**
	 * Iniciar tela.
	 *
	 * @param e the e
	 */
	public void iniciarTela(ActionEvent e) {
		limparPaginaConsulta();
		carregaCombos();
		carregaComboSituacao();
		setHabilitaDesfazerReversao(false);
		setHabilitaReverter(false);
	}

	/**
	 * Limpar.
	 */
	public void limpar() {
		setCboCustoOrigem(null);
		setCboSituacao(null);
		setDataMigracaoDe(new Date());
		setDataMigracaoA(new Date());
		setCorpoCpfCnpj(null);
		setFiliaCnpj(null);
		setDigitoCnpj(null);
		setCodEmpresaPerfil(null);
		setAgencia(null);
		setConta(null);
		setCboEmpresaGestoraCont(null);
		setCboTpContrato(null);
		setNumero(null);
		setHabilitaDesfazerReversao(false);
		setHabilitaReverter(false);
		setCboEmpresaGestoraCont(2269651L);
	}

	/**
	 * Limpar pagina consulta.
	 *
	 * @return the string
	 */
	public String limparPaginaConsulta() {
		setRadioPesquisa("");
		
		limpar();
		limparGrid();
		
		return "";
	}
	
	/**
	 * Limpar agencia conta.
	 */
	public void limparAgenciaConta(){
		setAgencia(null);
		setConta(null);
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try {
			ConContratoMigradoSaidaDTO registroSelecionadoDTO = getListaContratosMigrados().get(getItemSelecionado());
			
			ConsultarDadosBasicoContrato(registroSelecionadoDTO);
			
			if(isContratoMigradoHsbc()){
				consultarContratoMigradoHsbc(registroSelecionadoDTO);
			}
						
			return "DETALHAR";
		
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
		return "";
	}
	
	public boolean isContratoMigradoHsbc(){
		return getDescricao() != null && ("HCPG".equals(getDescricao()) || "HCOB".equals(getDescricao()));
	}

	/**
	 * Consultar contrato migrado hsbc.
	 *
	 * @param registroSelecionadoDTO the registro selecionado dto
	 */
	private void consultarContratoMigradoHsbc (ConContratoMigradoSaidaDTO registroSelecionadoDTO){
		DetalharContratoMigradoHsbcEntradaDTO entrada = new DetalharContratoMigradoHsbcEntradaDTO();
		entrada.setCdpessoaJuridicaContrato(registroSelecionadoDTO.getCdPessoaJuridicaContrato());
		entrada.setCdTipoContratoNegocio(registroSelecionadoDTO.getCdTipoContratoNegocio());
		entrada.setNrSequenciaContratoNegocio(registroSelecionadoDTO.getNrSequenciaContratoNegocio());
		
		setSaidaDetalharContratoMigradoHsbc(getConContratosMigradosServiceImpl().detalharContratoMigradoHsbc(entrada));
	
	}
	
	/**
	 * Consultar dados basico contrato.
	 *
	 * @param registroSelecionadoDTO the registro selecionado dto
	 */
	private void ConsultarDadosBasicoContrato (ConContratoMigradoSaidaDTO registroSelecionadoDTO){
		ConsultarDadosBasicoContratoEntradaDTO entradaDTO = new ConsultarDadosBasicoContratoEntradaDTO();
		
		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoDTO.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContrato(registroSelecionadoDTO.getCdTipoContratoNegocio());
		entradaDTO.setNumeroSequenciaContrato(registroSelecionadoDTO.getNrSequenciaContratoNegocio());
		entradaDTO.setCdClub(registroSelecionadoDTO.getCdClub());
		entradaDTO.setCdCgcCpf(registroSelecionadoDTO.getCdCorpoCpfCnpj());
		entradaDTO.setCdFilialCgcCpf(registroSelecionadoDTO.getFiliaCnpj());
		entradaDTO.setCdControleCgcCpf(registroSelecionadoDTO.getDigitoCnpj());
		entradaDTO.setCdAgencia(registroSelecionadoDTO.getCdAgencia());
		
		ConsultarDadosBasicoContratoSaidaDTO saidaDTO = 
			getConContratosMigradosServiceImpl().consultarDadosBasicoContrato(entradaDTO);
		
		setCpfCnpjMaster(registroSelecionadoDTO.getCpfCnpjRepresentanteFormatado());
		setNomeRazaoMaster(registroSelecionadoDTO.getDsRezaoSocialRepresentente());
		setGrupoEconomico(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomica(saidaDTO.getDsAtividadeEconomica());
		setSegmento(saidaDTO.getDsSegmentoEconomico());
		setSubSegmento(saidaDTO.getDsSubSegmentoEconomico());
		setDsAgenciaGestora(saidaDTO.getCdDeptoGestor() + " - " + saidaDTO.getDsDeptoGestor());
		setGerenteResponsavel(saidaDTO.getDsFuncionarioBradesco());
		setCpfCnpj(registroSelecionadoDTO.getCpfCnpjFormatado());
		setNomeRazaoSocial(registroSelecionadoDTO.getDsNome());
	    setEmpresaGestora(registroSelecionadoDTO.getDsPessoaJuridicaContrato());
		setTpContrato(registroSelecionadoDTO.getDsTipoContratoNegocio());
		setDetNumero(registroSelecionadoDTO.getNrSequenciaContratoNegocio());
		setNumComercial(saidaDTO.getNumeroComercialContrato());
		setDescricao(saidaDTO.getDsContratoNegocio());
		setIdioma(saidaDTO.getDsIdioma());
		setMoeda(saidaDTO.getDsMoeda());
		setOrigem(saidaDTO.getDsOrigem());
		setPossuiAditivo(saidaDTO.getDsPossuiAditivo());
		setParticipacao(saidaDTO.getDsTipoParticipacaoContrato());
		setDtHoraAssinatura(saidaDTO.getDataAssinaturaContrato());
		setSituacao(saidaDTO.getDsSituacaoContrato());
		setMotivo(saidaDTO.getDsSituacaoMotivoContrato());
		setInicioVigencia(saidaDTO.getDataInclusaoContrato());
		setFimVigenciaContrato(saidaDTO.getDataCadastroContrato());
		setDsSetorContrato(saidaDTO.getDsSetorContrato());
		setDataInclusao(PgitUtil.concatenarCampos(
				saidaDTO.getDataInclusaoContrato(),saidaDTO.getHoraInclusaoContrato()));			
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoContrato());
		setTipoCanalInclusao(saidaDTO.getCdTipoCanalInclusao() != 0 ? PgitUtil.concatenarCampos(
				saidaDTO.getCdTipoCanalInclusao(),saidaDTO.getDsTipoCanalInclusao()):"");
		setComplementoInclusao(saidaDTO.getOperacaoFluxoInclusao()==null || 
				saidaDTO.getOperacaoFluxoInclusao().equals("0")? "" : saidaDTO.getOperacaoFluxoInclusao());
		setDataManutencao(PgitUtil.concatenarCampos(saidaDTO.getDataManutencaoContrato(),
				saidaDTO.getHoraManutencaoContrato()));			
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoContrato());
		setTipoCanalManutencao(saidaDTO.getCdTipoCanalManutencao() ==0? "" : 
			PgitUtil.concatenarCampos(saidaDTO.getCdTipoCanalManutencao(), saidaDTO.getDsTipoCanalManutencao()));
		setComplementoManutencao(saidaDTO.getOperacaoFluxoManutencao()==null || 
				saidaDTO.getOperacaoFluxoManutencao().equals("0")? "" : saidaDTO.getOperacaoFluxoManutencao());
	}
	

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		setItemSelecionado(null);
		setHabilitaDesfazerReversao(false);
		setHabilitaReverter(false);
		
		return "VOLTAR";
	}
	
	/**
	 * Carrega combos.
	 */
	public void carregaCombos() {
		listaCboCustoOrigem = new ArrayList<SelectItem>();

		listaCboCustoOrigem.add(new SelectItem(1, "1 � CREC"));
		listaCboCustoOrigem.add(new SelectItem(2, "2 � PFOR"));
		listaCboCustoOrigem.add(new SelectItem(3, "3 � PTRB"));
		listaCboCustoOrigem.add(new SelectItem(4, "4 � HCPG"));
		listaCboCustoOrigem.add(new SelectItem(5, "5 � HCOB"));

		listarTipoContrato();
		listarEmpresaGestora();
	}

	
	/**
	 * Carrega combo situacao.
	 */
	public void carregaComboSituacao() {
		listaSituacao = new ArrayList<SelectItem>();
		listaSituacao.add(new SelectItem(1,MessageHelperUtils.getI18nMessage("label_migrado") ));
		listaSituacao.add(new SelectItem(2,MessageHelperUtils.getI18nMessage("label_simulacao") ));
		listaSituacao.add(new SelectItem(3,MessageHelperUtils.getI18nMessage( "label_migrado_reversao") ));
		listaSituacao.add(new SelectItem(4,MessageHelperUtils.getI18nMessage("label_simulacao_reversao") ));
		listaSituacao.add(new SelectItem(5,MessageHelperUtils.getI18nMessage("label_migrado_prod") ));
		listaSituacao.add(new SelectItem(6,MessageHelperUtils.getI18nMessage("label_migracao_teste") ));
		
		listaSituacaoHash = new HashMap<Integer, String>();
		listaSituacaoHash.put(1,MessageHelperUtils.getI18nMessage("label_migrado"));
		listaSituacaoHash.put(2,MessageHelperUtils.getI18nMessage("label_migrado"));
		listaSituacaoHash.put(3,MessageHelperUtils.getI18nMessage("label_migrado_reversao"));
		listaSituacaoHash.put(4,MessageHelperUtils.getI18nMessage("label_simulacao_reversao"));
		listaSituacaoHash.put(5,MessageHelperUtils.getI18nMessage("label_migrado_prod"));
		listaSituacaoHash.put(6,MessageHelperUtils.getI18nMessage("label_migracao_teste"));		
		
	}
	
	/**
	 * Listar tipo contrato.
	 */
	public void listarTipoContrato() {
		try {
			listaTipoContrato.clear();
			List<TipoContratoSaidaDTO> list = comboService.listarTipoContrato();

			for (TipoContratoSaidaDTO saida : list) {
				listaTipoContrato.add(new SelectItem(saida.getCdTipoContrato(),
						saida.getDsTipoContrato()));
			}
		} catch (PdcAdapterFunctionalException p) {
			listaTipoContrato = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora() {
		try {
			this.listaEmpresaGestora = new ArrayList<SelectItem>();
			List<EmpresaConglomeradoSaidaDTO> list = new ArrayList<EmpresaConglomeradoSaidaDTO>();

			EmpresaConglomeradoEntradaDTO entrada = new EmpresaConglomeradoEntradaDTO();
			entrada.setCdSituacao(0);
			entrada.setNrOcorrencias(180);

			list = comboService.listarEmpresaConglomerado(entrada);
			listaEmpresaGestora.clear();

			for (EmpresaConglomeradoSaidaDTO combo : list) {
				listaEmpresaGestora.add(new SelectItem(combo.getCodClub(), combo.getCodClub() + " - " + combo.getDsRazaoSocial()));
				listaEmpresaGestoraHash.put(combo.getCodClub(), combo.getDsRazaoSocial());
			}
		} catch (PdcAdapterFunctionalException p) {
			listaEmpresaGestora = new ArrayList<SelectItem>();
			listaEmpresaGestoraHash = new HashMap<Long, String>();
		}
	}

	/**
	 * Limpar grid.
	 *
	 * @return the string
	 */
	public String limparGrid() {
		setListaControle(new ArrayList<SelectItem>());
		setListaContratosMigrados(null);
		setItemSelecionado(null);
		setHabilitaDesfazerReversao(false);
		setHabilitaReverter(false);
		
		return "";
	}

	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar() {
		setItemSelecionado(null);
		setHabilitaDesfazerReversao(false);
		setHabilitaReverter(false);

		try {
		    ValidarUsuarioEntradaDTO entradaValidarUsuario = new ValidarUsuarioEntradaDTO();
            entradaValidarUsuario.setMaxOcorrencias(0);
            
            saidaValidarUsuario = getManterContratoImpl().validarUsuario(entradaValidarUsuario);
            
		    ConContratoMigradoEntradaDTO entrada = new ConContratoMigradoEntradaDTO();
			
			entrada.setCentroCustoOrigem(getCboCustoOrigem());
			
	    	SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			entrada.setDtInicio((getDataMigracaoDe() == null) ? "" : sdf.format(getDataMigracaoDe()) );
			entrada.setDtFim((getDataMigracaoA() == null) ? "" : sdf.format(getDataMigracaoA()));
			
			entrada.setCdTipoContratoNegocio(getCboTpContrato());
			entrada.setNrSequenciaContratoNegocio(getNumero());
			entrada.setCdCorpoCpfCnpj(getCorpoCpfCnpj());
			entrada.setFiliaCnpj(getFiliaCnpj());
			entrada.setDigitoCnpj(getDigitoCnpj());
			entrada.setCdPerfil(getCodEmpresaPerfil());
			entrada.setCdAgencia(getAgencia());
			entrada.setCdConta(getConta());
			entrada.setCdPessoaJuridicaContrato(getCboEmpresaGestoraCont());
			entrada.setCdSituacaoContrato(getCboSituacao());

			setListaContratosMigrados(getConContratosMigradosServiceImpl().conContratoMigrado(entrada));

			listaControle = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaContratosMigrados().size(); i++) {
				listaControle.add(new SelectItem(i, ""));
			}

			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaContratosMigrados(null);
			listaControle = new ArrayList<SelectItem>();
			setItemSelecionado(null);
			setHabilitaDesfazerReversao(false);
			setHabilitaReverter(false);

		}
		
		return "";
	}
	
	/**
	 * Habilita reverter desfazer.
	 */
	public void habilitaReverterDesfazer(){
		
		if (getSaidaValidarUsuario().getCdAcessoDepartamento() != null && 
				getSaidaValidarUsuario().getCdAcessoDepartamento() ==  1) {
			
		
			ConContratoMigradoSaidaDTO registroSelecionado = getListaContratosMigrados().get(getItemSelecionado());
			
			if(registroSelecionado.getCdSituacaoContrato().intValue() == 1 ||registroSelecionado.getCdSituacaoContrato().intValue() == 2 ){			
				setHabilitaDesfazerReversao(false);
			}else{
				if(registroSelecionado.getCdSituacaoContrato().intValue() == 3 ||registroSelecionado.getCdSituacaoContrato().intValue() == 4){
					setHabilitaDesfazerReversao(true);				
				}else{
					setHabilitaDesfazerReversao(false);				
				}
			}
			
			if(registroSelecionado.getCdSituacaoContrato().intValue() == 1 || registroSelecionado.getCdSituacaoContrato().intValue() == 2 || registroSelecionado.getCdSituacaoContrato().intValue() == 5 || registroSelecionado.getCdSituacaoContrato().intValue() == 6 ){
				setHabilitaReverter(true);
			}
			else{
				setHabilitaReverter(false);			
			}
			
			if("HCOB".equals(registroSelecionado.getCentroCustoOrigem()) || "HCPG".equals(registroSelecionado.getCentroCustoOrigem())){
			    setHabilitaDesfazerReversao(false);
			    setHabilitaReverter(false);
			}
		}else{
			setHabilitaDesfazerReversao(false);
		    setHabilitaReverter(false);
		}
		
	}
	
	/**
	 * Reverter.
	 *
	 * @return the string
	 */
	public String reverter(){
	
		try{
			preencheDados();
			return "REVERTER";
		}catch (PdcAdapterFunctionalException p) {
			if(StringUtils.right(p.getCode(), 8).equalsIgnoreCase("PGIT0039")){
				preencheDadosTelaReverter();
				
				return "REVERTER";
			}
			
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param e the e
	 * @return the string
	 */
	public String pesquisar(ActionEvent e){
		setItemSelecionado(null);

		setHabilitaDesfazerReversao(false);

		setHabilitaReverter(false);

		try {

			ConContratoMigradoEntradaDTO entrada = new ConContratoMigradoEntradaDTO();

			entrada.setCentroCustoOrigem(getCboCustoOrigem());

			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			entrada.setDtInicio((getDataMigracaoDe() == null) ? "" : sdf.format(getDataMigracaoDe()) );
			entrada.setDtFim((getDataMigracaoA() == null) ? "" : sdf.format(getDataMigracaoA()));

			entrada.setCdTipoContratoNegocio(getCboTpContrato());
			entrada.setNrSequenciaContratoNegocio(getNumero());
			entrada.setCdCorpoCpfCnpj(getCorpoCpfCnpj());
			entrada.setFiliaCnpj(getFiliaCnpj());
			entrada.setDigitoCnpj(getDigitoCnpj());
			entrada.setCdPerfil(getCodEmpresaPerfil());
			entrada.setCdAgencia(getAgencia());
			entrada.setCdConta(getConta());
			entrada.setCdPessoaJuridicaContrato(getCboEmpresaGestoraCont());
			entrada.setCdSituacaoContrato(getCboSituacao());

			setListaContratosMigrados(getConContratosMigradosServiceImpl().conContratoMigrado(entrada));

			listaControle = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaContratosMigrados().size(); i++) {
				listaControle.add(new SelectItem(i, ""));
			}
		} catch (PdcAdapterFunctionalException p) {

			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);

			setListaContratosMigrados(null);

			listaControle = new ArrayList<SelectItem>();

			setItemSelecionado(null);

			setHabilitaDesfazerReversao(false);

			setHabilitaReverter(false);
		}

		return "";
	}
	
	/**
	 * Desfazer reversao.
	 *
	 * @return the string
	 */
	public String desfazerReversao(){
		
		try{
			preencheDados();
			return "DESFAZER_REVERSAO";
		}catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
	}
	
	/**
	 * Preenche dados.
	 */
	public void preencheDados(){
		
			ConContratoMigradoSaidaDTO registroSelecionadoDTO = getListaContratosMigrados().get(getItemSelecionado());
			
			ConsultarDadosBasicoContratoEntradaDTO entradaDTO = new ConsultarDadosBasicoContratoEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaContrato(registroSelecionadoDTO.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContrato(registroSelecionadoDTO.getCdTipoContratoNegocio());
			entradaDTO.setNumeroSequenciaContrato(registroSelecionadoDTO.getNrSequenciaContratoNegocio());
			entradaDTO.setCdClub(registroSelecionadoDTO.getCdClub());
			entradaDTO.setCdCgcCpf(registroSelecionadoDTO.getCdCorpoCpfCnpj());
			entradaDTO.setCdFilialCgcCpf(registroSelecionadoDTO.getFiliaCnpj());
			entradaDTO.setCdControleCgcCpf(registroSelecionadoDTO.getDigitoCnpj());
			entradaDTO.setCdAgencia(registroSelecionadoDTO.getCdAgencia());
			
			ConsultarDadosBasicoContratoSaidaDTO saidaDTO = getConContratosMigradosServiceImpl().consultarDadosBasicoContrato(entradaDTO);
			
			setCpfCnpjMaster(registroSelecionadoDTO.getCpfCnpjRepresentanteFormatado());
			setNomeRazaoMaster(registroSelecionadoDTO.getDsRezaoSocialRepresentente());
			setGrupoEconomico(saidaDTO.getDsGrupoEconomico());
			setAtividadeEconomica(saidaDTO.getDsAtividadeEconomica());
			setSegmento(saidaDTO.getDsSegmentoEconomico());
			setSubSegmento(saidaDTO.getDsSubSegmentoEconomico());
			setDsAgenciaGestora(saidaDTO.getCdDeptoGestor() + " - " + saidaDTO.getDsDeptoGestor());
			setGerenteResponsavel(saidaDTO.getDsFuncionarioBradesco());
			
			setCpfCnpj(registroSelecionadoDTO.getCpfCnpjFormatado());
			setNomeRazaoSocial(registroSelecionadoDTO.getDsNome());
			
		    setEmpresaGestora(registroSelecionadoDTO.getDsPessoaJuridicaContrato());
			setTpContrato(registroSelecionadoDTO.getDsTipoContratoNegocio());
			setDetNumero(registroSelecionadoDTO.getNrSequenciaContratoNegocio());
			
			//N�mero Documento F�sico do Contrato
			setNumComercial(saidaDTO.getNumeroComercialContrato());
			setDescricao(saidaDTO.getDsContratoNegocio());
			setIdioma(saidaDTO.getDsIdioma());
			setMoeda(saidaDTO.getDsMoeda());
			setOrigem(saidaDTO.getDsOrigem());
			setPossuiAditivo(saidaDTO.getDsPossuiAditivo());
			setParticipacao(saidaDTO.getDsTipoParticipacaoContrato());
			setDtHoraAssinatura(saidaDTO.getDataAssinaturaContrato());
			setSituacao(saidaDTO.getDsSituacaoContrato());
			setMotivo(saidaDTO.getDsSituacaoMotivoContrato());

			setInicioVigencia(saidaDTO.getDataInclusaoContrato());
			setFimVigenciaContrato(saidaDTO.getDataCadastroContrato());
			setDsSetorContrato(saidaDTO.getDsSetorContrato());
			
			setDataInclusao(PgitUtil.concatenarCampos(saidaDTO.getDataInclusaoContrato(),saidaDTO.getHoraInclusaoContrato()));			
			setUsuarioInclusao(saidaDTO.getUsuarioInclusaoContrato());
			setTipoCanalInclusao(saidaDTO.getCdTipoCanalInclusao() != 0 ? PgitUtil.concatenarCampos(saidaDTO.getCdTipoCanalInclusao(),saidaDTO.getDsTipoCanalInclusao()):"");
			setComplementoInclusao(saidaDTO.getOperacaoFluxoInclusao()==null || saidaDTO.getOperacaoFluxoInclusao().equals("0")? "" : saidaDTO.getOperacaoFluxoInclusao());
			setDataManutencao(PgitUtil.concatenarCampos(saidaDTO.getDataManutencaoContrato(),saidaDTO.getHoraManutencaoContrato()));			
			setUsuarioManutencao(saidaDTO.getUsuarioManutencaoContrato());
			setTipoCanalManutencao(saidaDTO.getCdTipoCanalManutencao() ==0? "" : PgitUtil.concatenarCampos(saidaDTO.getCdTipoCanalManutencao(), saidaDTO.getDsTipoCanalManutencao()));
			setComplementoManutencao(saidaDTO.getOperacaoFluxoManutencao()==null || saidaDTO.getOperacaoFluxoManutencao().equals("0")? "" : saidaDTO.getOperacaoFluxoManutencao());
			
		
		
	}
	
	/**
	 * Preenche dados tela reverter.
	 */
	public void preencheDadosTelaReverter(){
		
		ConContratoMigradoSaidaDTO saidaDTO = getListaContratosMigrados().get(getItemSelecionado());
		
		String cpfCnpjFormatado = CpfCnpjUtils.formatarCpfCnpj(saidaDTO.getCdCpfCnpjRepresentante(), saidaDTO.getCdFilialRepresentante(), saidaDTO.getCdCpfDigitoRepresentante());
		
		setCpfCnpjMaster(cpfCnpjFormatado);
		setNomeRazaoMaster(saidaDTO.getDsRezaoSocialRepresentente());
//		setGrupoEconomico(saidaDTO.getDsGrupoEconomico());
//		setAtividadeEconomica(saidaDTO.getDsAtividadeEconomica());
//		setSegmento(saidaDTO.getDsSegmentoEconomico());
//		setSubSegmento(saidaDTO.getDsSubSegmentoEconomico());
//		setDsAgenciaGestora(saidaDTO.getCdDeptoGestor() + " - " + saidaDTO.getDsDeptoGestor());
//		setGerenteResponsavel(saidaDTO.getDsFuncionarioBradesco());
		
		setCpfCnpj(saidaDTO.getCpfCnpjFormatado());
		setNomeRazaoSocial(saidaDTO.getDsNome());
		
	    setEmpresaGestora(saidaDTO.getDsPessoaJuridicaContrato());
		setTpContrato(saidaDTO.getDsTipoContratoNegocio());
		setDetNumero(saidaDTO.getNrSequenciaContratoNegocio());
		
		//N�mero Documento F�sico do Contrato
//		setNumComercial(saidaDTO.getn NumeroComercialContrato());
//		setDescricao(saidaDTO.getDsContratoNegocio());
//		setIdioma(saidaDTO.getDsIdioma());
//		setMoeda(saidaDTO.getDsMoeda());
//		setOrigem(saidaDTO.getDsOrigem());
//		setPossuiAditivo(saidaDTO.getDsPossuiAditivo());
//		setParticipacao(saidaDTO.getDsTipoParticipacaoContrato());
//		setDtHoraAssinatura(saidaDTO.getDataAssinaturaContrato());
//		setSituacao(saidaDTO.getDsSituacaoContrato());
//		setMotivo(saidaDTO.getDsSituacaoMotivoContrato());

		setInicioVigencia(saidaDTO.getDtMigracao());
//		setFimVigenciaContrato(saidaDTO.getDataCadastroContrato());
//		setDsSetorContrato(saidaDTO.getDsSetorContrato());
		
//		setDataInclusao(PgitUtil.concatenarCampos(saidaDTO.getDataInclusaoContrato(),saidaDTO.getHoraInclusaoContrato()));			
//		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoContrato());
//		setTipoCanalInclusao(saidaDTO.getCdTipoCanalInclusao() != 0 ? PgitUtil.concatenarCampos(saidaDTO.getCdTipoCanalInclusao(),saidaDTO.getDsTipoCanalInclusao()):"");
//		setComplementoInclusao(saidaDTO.getOperacaoFluxoInclusao()==null || saidaDTO.getOperacaoFluxoInclusao().equals("0")? "" : saidaDTO.getOperacaoFluxoInclusao());
//		setDataManutencao(PgitUtil.concatenarCampos(saidaDTO.getDataManutencaoContrato(),saidaDTO.getHoraManutencaoContrato()));			
//		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoContrato());
//		setTipoCanalManutencao(saidaDTO.getCdTipoCanalManutencao() ==0? "" : PgitUtil.concatenarCampos(saidaDTO.getCdTipoCanalManutencao(), saidaDTO.getDsTipoCanalManutencao()));
//		setComplementoManutencao(saidaDTO.getOperacaoFluxoManutencao()==null || saidaDTO.getOperacaoFluxoManutencao().equals("0")? "" : saidaDTO.getOperacaoFluxoManutencao());
		
	
	
	}
	
	/**
	 * Confirmar reverter.
	 */
	public void confirmarReverter(){
		try{
			
			RevertContratoMigradoEntradaDTO entradaDTO = new RevertContratoMigradoEntradaDTO();
			ConContratoMigradoSaidaDTO registroSelecionado = getListaContratosMigrados().get(getItemSelecionado());
			
		
			entradaDTO.setCdAcao(1); 
			entradaDTO.setCdpessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
			
			if(registroSelecionado.getCentroCustoOrigem().equals("CREC")){
				entradaDTO.setCentroCustoOrigem(1);
			}else{ 
				if(registroSelecionado.getCentroCustoOrigem().equals("PFOR")) {
					entradaDTO.setCentroCustoOrigem(2);
				}else{
					if(registroSelecionado.getCentroCustoOrigem().equals("PTRB")){
						entradaDTO.setCentroCustoOrigem(3);
					}else{
							entradaDTO.setCentroCustoOrigem(0);
						}	
					}
				}
			
			entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrSequenciaContratoNegocio());
			
			RevertContratoMigradoSaidaDTO saidaDTO = getConContratosMigradosServiceImpl().revertContratoMigrado(entradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conConsultarContratosMigrados", BradescoViewExceptionActionType.ACTION, false);

			setItemSelecionado(null);
			setHabilitaDesfazerReversao(false);
			setHabilitaReverter(false);
			
			consultar();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}	
	}
	
	/**
	 * Confirmar desfazer reversao.
	 */
	public void confirmarDesfazerReversao(){
		try{
			
			RevertContratoMigradoEntradaDTO entradaDTO = new RevertContratoMigradoEntradaDTO();
			ConContratoMigradoSaidaDTO registroSelecionado = getListaContratosMigrados().get(getItemSelecionado());
			
		
			entradaDTO.setCdAcao(2); 
			entradaDTO.setCdpessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
			if(registroSelecionado.getCentroCustoOrigem().equals("CREC")){
				entradaDTO.setCentroCustoOrigem(1);
			}else{ 
				if(registroSelecionado.getCentroCustoOrigem().equals("PFOR")) {
					entradaDTO.setCentroCustoOrigem(2);
				}else{
					if(registroSelecionado.getCentroCustoOrigem().equals("PTRB")){
						entradaDTO.setCentroCustoOrigem(3);
					}else{
							entradaDTO.setCentroCustoOrigem(0);
						}	
					}
				}
			entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrSequenciaContratoNegocio());
			
			RevertContratoMigradoSaidaDTO saidaDTO = getConContratosMigradosServiceImpl().revertContratoMigrado(entradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conConsultarContratosMigrados", BradescoViewExceptionActionType.ACTION, false);
			

			setItemSelecionado(null);
			setHabilitaDesfazerReversao(false);
			setHabilitaReverter(false);
			
			consultar();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}	
	}
	
	/**
	 * Get: listaCboCustoOrigem.
	 *
	 * @return listaCboCustoOrigem
	 */
	public List<SelectItem> getListaCboCustoOrigem() {
		return listaCboCustoOrigem;
	}

	/**
	 * Set: listaCboCustoOrigem.
	 *
	 * @param listaCboCustoOrigem the lista cbo custo origem
	 */
	public void setListaCboCustoOrigem(List<SelectItem> listaCboCustoOrigem) {
		this.listaCboCustoOrigem = listaCboCustoOrigem;
	}


	/**
	 * Get: radioPesquisa.
	 *
	 * @return radioPesquisa
	 */
	public String getRadioPesquisa() {
		return radioPesquisa;
	}

	/**
	 * Set: radioPesquisa.
	 *
	 * @param radioPesquisa the radio pesquisa
	 */
	public void setRadioPesquisa(String radioPesquisa) {
		this.radioPesquisa = radioPesquisa;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public Integer getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(Integer itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: listaEmpresaGestoraHash.
	 *
	 * @return listaEmpresaGestoraHash
	 */
	public Map<Long, String> getListaEmpresaGestoraHash() {
		return listaEmpresaGestoraHash;
	}

	/**
	 * Set lista empresa gestora hash.
	 *
	 * @param listaEmpresaGestoraHash the lista empresa gestora hash
	 */
	public void setListaEmpresaGestoraHash(
			Map<Long, String> listaEmpresaGestoraHash) {
		this.listaEmpresaGestoraHash = listaEmpresaGestoraHash;
	}

	/**
	 * Get: listaTipoContrato.
	 *
	 * @return listaTipoContrato
	 */
	public List<SelectItem> getListaTipoContrato() {
		return listaTipoContrato;
	}

	/**
	 * Set: listaTipoContrato.
	 *
	 * @param listaTipoContrato the lista tipo contrato
	 */
	public void setListaTipoContrato(List<SelectItem> listaTipoContrato) {
		this.listaTipoContrato = listaTipoContrato;
	}

	/**
	 * Get: cboEmpresaGestoraCont.
	 *
	 * @return cboEmpresaGestoraCont
	 */
	public Long getCboEmpresaGestoraCont() {
		return cboEmpresaGestoraCont;
	}

	/**
	 * Set: cboEmpresaGestoraCont.
	 *
	 * @param cboEmpresaGestoraCont the cbo empresa gestora cont
	 */
	public void setCboEmpresaGestoraCont(Long cboEmpresaGestoraCont) {
		this.cboEmpresaGestoraCont = cboEmpresaGestoraCont;
	}

	/**
	 * Get: cboTpContrato.
	 *
	 * @return cboTpContrato
	 */
	public Integer getCboTpContrato() {
		return cboTpContrato;
	}

	/**
	 * Set: cboTpContrato.
	 *
	 * @param cboTpContrato the cbo tp contrato
	 */
	public void setCboTpContrato(Integer cboTpContrato) {
		this.cboTpContrato = cboTpContrato;
	}

	/**
	 * Get: listaContratosMigrados.
	 *
	 * @return listaContratosMigrados
	 */
	public List<ConContratoMigradoSaidaDTO> getListaContratosMigrados() {
		return listaContratosMigrados;
	}

	/**
	 * Set: listaContratosMigrados.
	 *
	 * @param listaContratosMigrados the lista contratos migrados
	 */
	public void setListaContratosMigrados(
			List<ConContratoMigradoSaidaDTO> listaContratosMigrados) {
		this.listaContratosMigrados = listaContratosMigrados;
	}

	/**
	 * Get: conContratosMigradosServiceImpl.
	 *
	 * @return conContratosMigradosServiceImpl
	 */
	public IConContratosMigradosService getConContratosMigradosServiceImpl() {
		return conContratosMigradosServiceImpl;
	}

	/**
	 * Set: conContratosMigradosServiceImpl.
	 *
	 * @param conContratosMigradosServiceImpl the con contratos migrados service impl
	 */
	public void setConContratosMigradosServiceImpl(
			IConContratosMigradosService conContratosMigradosServiceImpl) {
		this.conContratosMigradosServiceImpl = conContratosMigradosServiceImpl;
	}

	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public Integer getAgencia() {
		return agencia;
	}

	/**
	 * Set: agencia.
	 *
	 * @param agencia the agencia
	 */
	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}

	/**
	 * Get: codEmpresaPerfil.
	 *
	 * @return codEmpresaPerfil
	 */
	public Long getCodEmpresaPerfil() {
		return codEmpresaPerfil;
	}

	/**
	 * Set: codEmpresaPerfil.
	 *
	 * @param codEmpresaPerfil the cod empresa perfil
	 */
	public void setCodEmpresaPerfil(Long codEmpresaPerfil) {
		this.codEmpresaPerfil = codEmpresaPerfil;
	}

	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public Integer getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(Integer conta) {
		this.conta = conta;
	}

	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public Long getNumero() {
		return numero;
	}

	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(Long numero) {
		this.numero = numero;
	}

	/**
	 * Get: corpoCpfCnpj.
	 *
	 * @return corpoCpfCnpj
	 */
	public Long getCorpoCpfCnpj() {
		return corpoCpfCnpj;
	}

	/**
	 * Set: corpoCpfCnpj.
	 *
	 * @param corpoCpfCnpj the corpo cpf cnpj
	 */
	public void setCorpoCpfCnpj(Long corpoCpfCnpj) {
		this.corpoCpfCnpj = corpoCpfCnpj;
	}

	/**
	 * Get: digitoCnpj.
	 *
	 * @return digitoCnpj
	 */
	public Integer getDigitoCnpj() {
		return digitoCnpj;
	}

	/**
	 * Set: digitoCnpj.
	 *
	 * @param digitoCnpj the digito cnpj
	 */
	public void setDigitoCnpj(Integer digitoCnpj) {
		this.digitoCnpj = digitoCnpj;
	}

	/**
	 * Get: filiaCnpj.
	 *
	 * @return filiaCnpj
	 */
	public Integer getFiliaCnpj() {
		return filiaCnpj;
	}

	/**
	 * Set: filiaCnpj.
	 *
	 * @param filiaCnpj the filia cnpj
	 */
	public void setFiliaCnpj(Integer filiaCnpj) {
		this.filiaCnpj = filiaCnpj;
	}
	
	/**
	 * Get: atividadeEconomica.
	 *
	 * @return atividadeEconomica
	 */
	public String getAtividadeEconomica() {
		return atividadeEconomica;
	}
	
	/**
	 * Set: atividadeEconomica.
	 *
	 * @param atividadeEconomica the atividade economica
	 */
	public void setAtividadeEconomica(String atividadeEconomica) {
		this.atividadeEconomica = atividadeEconomica;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	
	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	/**
	 * Get: cpfCnpjMaster.
	 *
	 * @return cpfCnpjMaster
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}
	
	/**
	 * Set: cpfCnpjMaster.
	 *
	 * @param cpfCnpjMaster the cpf cnpj master
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}
	
	/**
	 * Get: descricao.
	 *
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	
	/**
	 * Set: descricao.
	 *
	 * @param descricao the descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Get: dtHoraAssinatura.
	 *
	 * @return dtHoraAssinatura
	 */
	public String getDtHoraAssinatura() {
		return dtHoraAssinatura;
	}
	
	/**
	 * Set: dtHoraAssinatura.
	 *
	 * @param dtHoraAssinatura the dt hora assinatura
	 */
	public void setDtHoraAssinatura(String dtHoraAssinatura) {
		this.dtHoraAssinatura = dtHoraAssinatura;
	}
	
	/**
	 * Get: gerenteResponsavel.
	 *
	 * @return gerenteResponsavel
	 */
	public String getGerenteResponsavel() {
		return gerenteResponsavel;
	}
	
	/**
	 * Set: gerenteResponsavel.
	 *
	 * @param gerenteResponsavel the gerente responsavel
	 */
	public void setGerenteResponsavel(String gerenteResponsavel) {
		this.gerenteResponsavel = gerenteResponsavel;
	}
	
	/**
	 * Get: grupoEconomico.
	 *
	 * @return grupoEconomico
	 */
	public String getGrupoEconomico() {
		return grupoEconomico;
	}
	
	/**
	 * Set: grupoEconomico.
	 *
	 * @param grupoEconomico the grupo economico
	 */
	public void setGrupoEconomico(String grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}
	
	/**
	 * Get: idioma.
	 *
	 * @return idioma
	 */
	public String getIdioma() {
		return idioma;
	}
	
	/**
	 * Set: idioma.
	 *
	 * @param idioma the idioma
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	
	/**
	 * Get: inicioVigencia.
	 *
	 * @return inicioVigencia
	 */
	public String getInicioVigencia() {
		return inicioVigencia;
	}
	
	/**
	 * Set: inicioVigencia.
	 *
	 * @param inicioVigencia the inicio vigencia
	 */
	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}
	
	/**
	 * Get: moeda.
	 *
	 * @return moeda
	 */
	public String getMoeda() {
		return moeda;
	}
	
	/**
	 * Set: moeda.
	 *
	 * @param moeda the moeda
	 */
	public void setMoeda(String moeda) {
		this.moeda = moeda;
	}
	
	/**
	 * Get: motivo.
	 *
	 * @return motivo
	 */
	public String getMotivo() {
		return motivo;
	}
	
	/**
	 * Set: motivo.
	 *
	 * @param motivo the motivo
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	/**
	 * Get: nomeRazaoMaster.
	 *
	 * @return nomeRazaoMaster
	 */
	public String getNomeRazaoMaster() {
		return nomeRazaoMaster;
	}
	
	/**
	 * Set: nomeRazaoMaster.
	 *
	 * @param nomeRazaoMaster the nome razao master
	 */
	public void setNomeRazaoMaster(String nomeRazaoMaster) {
		this.nomeRazaoMaster = nomeRazaoMaster;
	}
	
	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}
	
	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}
	
	/**
	 * Get: numComercial.
	 *
	 * @return numComercial
	 */
	public String getNumComercial() {
		return numComercial;
	}
	
	/**
	 * Set: numComercial.
	 *
	 * @param numComercial the num comercial
	 */
	public void setNumComercial(String numComercial) {
		this.numComercial = numComercial;
	}
	
	/**
	 * Get: origem.
	 *
	 * @return origem
	 */
	public String getOrigem() {
		return origem;
	}
	
	/**
	 * Set: origem.
	 *
	 * @param origem the origem
	 */
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	
	/**
	 * Get: participacao.
	 *
	 * @return participacao
	 */
	public String getParticipacao() {
		return participacao;
	}
	
	/**
	 * Set: participacao.
	 *
	 * @param participacao the participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}
	
	/**
	 * Get: possuiAditivo.
	 *
	 * @return possuiAditivo
	 */
	public String getPossuiAditivo() {
		return possuiAditivo;
	}
	
	/**
	 * Set: possuiAditivo.
	 *
	 * @param possuiAditivo the possui aditivo
	 */
	public void setPossuiAditivo(String possuiAditivo) {
		this.possuiAditivo = possuiAditivo;
	}
	
	/**
	 * Get: segmento.
	 *
	 * @return segmento
	 */
	public String getSegmento() {
		return segmento;
	}
	
	/**
	 * Set: segmento.
	 *
	 * @param segmento the segmento
	 */
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}
	
	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}
	
	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	/**
	 * Get: subSegmento.
	 *
	 * @return subSegmento
	 */
	public String getSubSegmento() {
		return subSegmento;
	}
	
	/**
	 * Set: subSegmento.
	 *
	 * @param subSegmento the sub segmento
	 */
	public void setSubSegmento(String subSegmento) {
		this.subSegmento = subSegmento;
	}
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}
	
	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}
	
	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}
	
	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: dataInclusao.
	 *
	 * @return dataInclusao
	 */
	public String getDataInclusao() {
		return dataInclusao;
	}
	
	/**
	 * Set: dataInclusao.
	 *
	 * @param dataInclusao the data inclusao
	 */
	public void setDataInclusao(String dataInclusao) {
		this.dataInclusao = dataInclusao;
	}
	
	/**
	 * Get: dataManutencao.
	 *
	 * @return dataManutencao
	 */
	public String getDataManutencao() {
		return dataManutencao;
	}
	
	/**
	 * Set: dataManutencao.
	 *
	 * @param dataManutencao the data manutencao
	 */
	public void setDataManutencao(String dataManutencao) {
		this.dataManutencao = dataManutencao;
	}
	
	/**
	 * Get: horaInclusao.
	 *
	 * @return horaInclusao
	 */
	public String getHoraInclusao() {
		return horaInclusao;
	}
	
	/**
	 * Set: horaInclusao.
	 *
	 * @param horaInclusao the hora inclusao
	 */
	public void setHoraInclusao(String horaInclusao) {
		this.horaInclusao = horaInclusao;
	}
	
	/**
	 * Get: horaManutencao.
	 *
	 * @return horaManutencao
	 */
	public String getHoraManutencao() {
		return horaManutencao;
	}
	
	/**
	 * Set: horaManutencao.
	 *
	 * @param horaManutencao the hora manutencao
	 */
	public void setHoraManutencao(String horaManutencao) {
		this.horaManutencao = horaManutencao;
	}
	
	/**
	 * Get: detNumero.
	 *
	 * @return detNumero
	 */
	public Long getDetNumero() {
		return detNumero;
	}
	
	/**
	 * Set: detNumero.
	 *
	 * @param detNumero the det numero
	 */
	public void setDetNumero(Long detNumero) {
		this.detNumero = detNumero;
	}
	
	/**
	 * Get: empresaGestora.
	 *
	 * @return empresaGestora
	 */
	public String getEmpresaGestora() {
		return empresaGestora;
	}
	
	/**
	 * Set: empresaGestora.
	 *
	 * @param empresaGestora the empresa gestora
	 */
	public void setEmpresaGestora(String empresaGestora) {
		this.empresaGestora = empresaGestora;
	}
	
	/**
	 * Get: tpContrato.
	 *
	 * @return tpContrato
	 */
	public String getTpContrato() {
		return tpContrato;
	}
	
	/**
	 * Set: tpContrato.
	 *
	 * @param tpContrato the tp contrato
	 */
	public void setTpContrato(String tpContrato) {
		this.tpContrato = tpContrato;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: dsSetorContrato.
	 *
	 * @return dsSetorContrato
	 */
	public String getDsSetorContrato() {
		return dsSetorContrato;
	}

	/**
	 * Set: dsSetorContrato.
	 *
	 * @param dsSetorContrato the ds setor contrato
	 */
	public void setDsSetorContrato(String dsSetorContrato) {
		this.dsSetorContrato = dsSetorContrato;
	}

	/**
	 * Get: fimVigenciaContrato.
	 *
	 * @return fimVigenciaContrato
	 */
	public String getFimVigenciaContrato() {
		return fimVigenciaContrato;
	}

	/**
	 * Set: fimVigenciaContrato.
	 *
	 * @param fimVigenciaContrato the fim vigencia contrato
	 */
	public void setFimVigenciaContrato(String fimVigenciaContrato) {
		this.fimVigenciaContrato = fimVigenciaContrato;
	}

	/**
	 * Get: cboCustoOrigem.
	 *
	 * @return cboCustoOrigem
	 */
	public Integer getCboCustoOrigem() {
		return cboCustoOrigem;
	}

	/**
	 * Set: cboCustoOrigem.
	 *
	 * @param cboCustoOrigem the cbo custo origem
	 */
	public void setCboCustoOrigem(Integer cboCustoOrigem) {
		this.cboCustoOrigem = cboCustoOrigem;
	}

	/**
	 * Get: dataMigracaoA.
	 *
	 * @return dataMigracaoA
	 */
	public Date getDataMigracaoA() {
		return dataMigracaoA;
	}

	/**
	 * Set: dataMigracaoA.
	 *
	 * @param dataMigracaoA the data migracao a
	 */
	public void setDataMigracaoA(Date dataMigracaoA) {
		this.dataMigracaoA = dataMigracaoA;
	}

	/**
	 * Get: listaSituacao.
	 *
	 * @return listaSituacao
	 */
	public List<SelectItem> getListaSituacao() {
		return listaSituacao;
	}

	/**
	 * Set: listaSituacao.
	 *
	 * @param listaSituacao the lista situacao
	 */
	public void setListaSituacao(List<SelectItem> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}

	/**
	 * Get: dataMigracaoDe.
	 *
	 * @return dataMigracaoDe
	 */
	public Date getDataMigracaoDe() {
		return dataMigracaoDe;
	}

	/**
	 * Get: listaSituacaoHash.
	 *
	 * @return listaSituacaoHash
	 */
	public Map<Integer, String> getListaSituacaoHash() {
		return listaSituacaoHash;
	}

	/**
	 * Set lista situacao hash.
	 *
	 * @param listaSituacaoHash the lista situacao hash
	 */
	public void setListaSituacaoHash(Map<Integer, String> listaSituacaoHash) {
		this.listaSituacaoHash = listaSituacaoHash;
	}

	/**
	 * Get: cboSituacao.
	 *
	 * @return cboSituacao
	 */
	public Integer getCboSituacao() {
		return cboSituacao;
	}

	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}

	
	/**
	 * Is habilita desfazer reversao.
	 *
	 * @return true, if is habilita desfazer reversao
	 */
	public boolean isHabilitaDesfazerReversao() {
		return habilitaDesfazerReversao;
	}

	/**
	 * Set: habilitaDesfazerReversao.
	 *
	 * @param habilitaDesfazerReversao the habilita desfazer reversao
	 */
	public void setHabilitaDesfazerReversao(boolean habilitaDesfazerReversao) {
		this.habilitaDesfazerReversao = habilitaDesfazerReversao;
	}

	/**
	 * Is habilita reverter.
	 *
	 * @return true, if is habilita reverter
	 */
	public boolean isHabilitaReverter() {
		return habilitaReverter;
	}

	/**
	 * Set: habilitaReverter.
	 *
	 * @param habilitaReverter the habilita reverter
	 */
	public void setHabilitaReverter(boolean habilitaReverter) {
		this.habilitaReverter = habilitaReverter;
	}

	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}

	/**
	 * Set: cboSituacao.
	 *
	 * @param cboSituacao the cbo situacao
	 */
	public void setCboSituacao(Integer cboSituacao) {
		this.cboSituacao = cboSituacao;
	}

	/**
	 * Set: dataMigracaoDe.
	 *
	 * @param dataMigracaoDe the data migracao de
	 */
	public void setDataMigracaoDe(Date dataMigracaoDe) {
		this.dataMigracaoDe = dataMigracaoDe;
	}

	/**
	 * Gets the saida detalhar contrato migrado hsbc.
	 *
	 * @return the saida detalhar contrato migrado hsbc
	 */
	public DetalharContratoMigradoHsbcSaidaDTO getSaidaDetalharContratoMigradoHsbc() {
		return saidaDetalharContratoMigradoHsbc;
	}

	/**
	 * Sets the saida detalhar contrato migrado hsbc.
	 *
	 * @param saidaDetalharContratoMigradoHsbc the saida detalhar contrato migrado hsbc
	 */
	public void setSaidaDetalharContratoMigradoHsbc(
			DetalharContratoMigradoHsbcSaidaDTO saidaDetalharContratoMigradoHsbc) {
		this.saidaDetalharContratoMigradoHsbc = saidaDetalharContratoMigradoHsbc;
	}

	public ValidarUsuarioSaidaDTO getSaidaValidarUsuario() {
		return saidaValidarUsuario;
	}

	public void setSaidaValidarUsuario(ValidarUsuarioSaidaDTO saidaValidarUsuario) {
		this.saidaValidarUsuario = saidaValidarUsuario;
	}

	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}
	
}
