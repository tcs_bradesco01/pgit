/**
 * Nome: br.com.bradesco.web.pgit.view.bean.geracaoarquivosretorno.mantersolicgeracaoarqretbasepagtos
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.view.bean.geracaoarquivosretorno.mantersolicgeracaoarqretbasepagtos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarTipoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarTipoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoSolicitacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarInformacoesFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarInformacoesFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.IManterSolicGeracaoArqRetBasePagtosService;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.DetalharSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.DetalharSolBasePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ExcluirSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ExcluirSolBasePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.IncluirSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.IncluirSolBasePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ListarSolBasePagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicgeracaoarqretbasepagtos.bean.ListarSolBasePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.IManterSolicitacaoRastFavService;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantersolicitacaorastfav.bean.VerificaraAtributoSoltcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterSolicGeracaoArqRetBasePagtosBean
 * <p>
 * Prop�sito: Implementa��o do Bean ManterSolicGeracaoArqRetBasePagtosBean
 * </p>
 * 
 * @author todo! - Solu��es em Tecnologia / TI Melhorias - Arquitetura
 * @version 1.0
 */
public class ManterSolicGeracaoArqRetBasePagtosBean {

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo solEmissaoComprovantePagtoClientePagServiceImpl. */
	private ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl;
	
	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	/** Atributo manterSolicGeracaoArqRetBasePagtosServiceImpl. */
	private IManterSolicGeracaoArqRetBasePagtosService manterSolicGeracaoArqRetBasePagtosServiceImpl;
	
	/** Atributo manterSolicitacaoRastFavService. */
	private IManterSolicitacaoRastFavService manterSolicitacaoRastFavService;
	
	/** Atributo atributoSoltc. */
	private VerificaraAtributoSoltcSaidaDTO atributoSoltc;
	
	/** Atributo consultasService. */
	private IConsultasService consultasService;
	
	/** Atributo itemSelecionadoListaConsultar. */
	private Integer itemSelecionadoListaConsultar;
	
	/** Atributo cboSituacaoSolicitacao. */
	private Integer cboSituacaoSolicitacao;
	
	/** Atributo cboOrigemSolicitacao. */
	private Integer cboOrigemSolicitacao;
	
	/** Atributo dataSolicitacaoDe. */
	private Date dataSolicitacaoDe;
	
	/** Atributo dataSolicitacaoAte. */
	private Date dataSolicitacaoAte;
	
	/** Atributo numeroSolicitacao. */
	private Integer numeroSolicitacao;
	
	/** Atributo listaSituacaoSolicitacao. */
	private List<SelectItem> listaSituacaoSolicitacao;
	
	/** Atributo radioGridConsultar. */
	private List<SelectItem> radioGridConsultar;
	
	/** Atributo listaGridConsultar. */
	private List<ListarSolBasePagtoSaidaDTO> listaGridConsultar;

	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo nroContrato. */
	private String nroContrato;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;
	// detalhar
	/** Atributo nrSolicitacaoPagamento. */
	private Integer nrSolicitacaoPagamento;
	
	/** Atributo hrSolicitacaoPagamento. */
	private String hrSolicitacaoPagamento;
	
	/** Atributo dsSituacaoSolicitacao. */
	private String dsSituacaoSolicitacao;
	
	/** Atributo resultadoProcessamento. */
	private String resultadoProcessamento;
	
	/** Atributo hrAtendimentoSolicitacao. */
	private String hrAtendimentoSolicitacao;
	
	/** Atributo qtdeRegistrosSolicitacao. */
	private Long qtdeRegistrosSolicitacao;
	
	/** Atributo dtInicioPagamento. */
	private String dtInicioPagamento;
	
	/** Atributo dtFimPagamento. */
	private String dtFimPagamento;
	
	/** Atributo cdServico. */
	private String cdServico;
	
	/** Atributo cdServicoRelacionado. */
	private String cdServicoRelacionado;
	
	/** Atributo cdBancoDebito. */
	private Integer cdBancoDebito;
	
	/** Atributo cdAgenciaDebito. */
	private Integer cdAgenciaDebito;
	
	/** Atributo digitoAgenciaDebito. */
	private Integer digitoAgenciaDebito;
	
	/** Atributo cdContaDebito. */
	private Long cdContaDebito;
	
	/** Atributo digitoContaDebito. */
	private String digitoContaDebito;
	
	/** Atributo indicadorPagos. */
	private String indicadorPagos;
	
	/** Atributo indicadorNaoPagos. */
	private String indicadorNaoPagos;
	
	/** Atributo indicadorAgendados. */
	private String indicadorAgendados;
	
	/** Atributo cdOrigem. */
	private Integer cdOrigem;
	
	/** Atributo cdDestino. */
	private Integer cdDestino;
	
	/** Atributo dsFavorecido. */
	private String dsFavorecido;
	
	/** Atributo cdFavorecido. */
	private String cdFavorecido;
	
	/** Atributo cdInscricaoFavorecido. */
	private String cdInscricaoFavorecido;
	
	/** Atributo cdTipoInscricaoFavorecido. */
	private String cdTipoInscricaoFavorecido;
	
	/** Atributo nmMinemonicoFavorecido. */
	private String nmMinemonicoFavorecido;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	/** Atributo hrSolicitacaoPagamentoFormatada. */
	private String hrSolicitacaoPagamentoFormatada;
	
	/** Atributo hrAtendimentoSolicitacaoFormatada. */
	private String hrAtendimentoSolicitacaoFormatada;
	
	/** Atributo origemDesc. */
	private String origemDesc;
	
	/** Atributo destinoDesc. */
	private String destinoDesc;
	
	/** Atributo contaDebitoFormatado. */
	private String contaDebitoFormatado;
	
	/** Atributo tipoContaDebitoDesc. */
	private String tipoContaDebitoDesc;
	// incluir
	/** Atributo habilitaArgumentosPesquisaIncluir. */
	private boolean habilitaArgumentosPesquisaIncluir;
	
	/** Atributo dataPagamentoDe. */
	private Date dataPagamentoDe;
	
	/** Atributo dataPagamentoAte. */
	private Date dataPagamentoAte;
	
	/** Atributo itemSelecionadoListaIncluir. */
	private Integer itemSelecionadoListaIncluir;
	
	/** Atributo cboTipoServico. */
	private Integer cboTipoServico;
	
	/** Atributo cboModalidade. */
	private Integer cboModalidade;
	
	/** Atributo chkTipoServico. */
	private boolean chkTipoServico;
	
	/** Atributo chkModalidade. */
	private boolean chkModalidade;
	
	/** Atributo listaServicos. */
	private List<SelectItem> listaServicos;
	
	/** Atributo listaModalidades. */
	private List<SelectItem> listaModalidades;
	
	/** Atributo chkContaDebito. */
	private boolean chkContaDebito;
	
	/** Atributo chkFavorecido. */
	private boolean chkFavorecido;
	
	/** Atributo bancoContaDeb. */
	private Integer bancoContaDeb;
	
	/** Atributo agenciaContaDeb. */
	private Integer agenciaContaDeb;
	
	/** Atributo contaContaDeb. */
	private Long contaContaDeb;
	
	/** Atributo digitoContaDeb. */
	private String digitoContaDeb;
	
	/** Atributo tipoContaDeb. */
	private Integer tipoContaDeb;
	
	/** Atributo chkCompromissosPagos. */
	private boolean chkCompromissosPagos;
	
	/** Atributo chkCompromissosNaoPagos. */
	private boolean chkCompromissosNaoPagos;
	
	/** Atributo chkCompromissosAgendados. */
	private boolean chkCompromissosAgendados;
	
	/** Atributo itemFiltroFavSelecionado. */
	private String itemFiltroFavSelecionado;
	
	/** Atributo codigoFav. */
	private Long codigoFav;
	
	/** Atributo cboTipo. */
	private Integer cboTipo;
	
	/** Atributo listaTipoInscricao. */
	private List<SelectItem> listaTipoInscricao;
	
	/** Atributo cboOrigem. */
	private Integer cboOrigem;
	
	/** Atributo radioDestino. */
	private Integer radioDestino;
	
	/** Atributo inscricaoFav. */
	private Long inscricaoFav;
	
	/** Atributo cdIndicadorPagos. */
	private String cdIndicadorPagos;
	
	/** Atributo cdIndicadorNaoPagos. */
	private String cdIndicadorNaoPagos;
	
	/** Atributo cdIndicadorAgendados. */
	private String cdIndicadorAgendados;
	
	/** Atributo indicadorPagosFormatado. */
	private String indicadorPagosFormatado;
	
	/** Atributo indicadorNaoPagosFormatado. */
	private String indicadorNaoPagosFormatado;
	
	/** Atributo indicadorAgendadosFormatado. */
	private String indicadorAgendadosFormatado;
	
	/** Atributo mnemonico. */
	private String mnemonico;
	
	/** Atributo dataFormatadaDe. */
	private String dataFormatadaDe;
	
	/** Atributo dataFormatadaAte. */
	private String dataFormatadaAte;
	
	/** Atributo tipoServico. */
	private String tipoServico;
	
	/** Atributo modalidade. */
	private String modalidade;
	
	/** Atributo tipoInscricao. */
	private String tipoInscricao;
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();
	
	/** Atributo listaModalidadeHash. */
	private Map<Integer, String> listaModalidadeHash = new HashMap<Integer, String>();
	
	/** Atributo listaTipoInscricaoHash. */
	private Map<Integer, String> listaTipoInscricaoHash = new HashMap<Integer, String>();
	
	/** Atributo listaTipoContaDeb. */
	private List<SelectItem> listaTipoContaDeb = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoContaDebHash. */
	private Map<Integer, ConsultarTipoContaSaidaDTO> listaTipoContaDebHash = new HashMap<Integer, ConsultarTipoContaSaidaDTO>();
	
	/** Atributo nmFavorecidoInc. */
	private String nmFavorecidoInc;
	
	/** Atributo cdFavorecidoInc. */
	private Long cdFavorecidoInc;
	
	/** Atributo inscricaoFavorecidoInc. */
	private String inscricaoFavorecidoInc;
	
	/** Atributo dsTipoFavorecidoInc. */
	private String dsTipoFavorecidoInc;

	/** Atributo foco. */
	private String foco;

	/** Atributo vlTarifaPadrao. */
	private BigDecimal vlTarifaPadrao;
	
	/** Atributo vlDescontoTarifa. */
	private BigDecimal vlDescontoTarifa;
	
	/** Atributo vlTarifaAtualizada. */
	private BigDecimal vlTarifaAtualizada;
	
	/** Atributo indicadorDescontoBloqueio. */
	private boolean indicadorDescontoBloqueio;
	
	/** Atributo consultarAgenciaOpeTarifaPadraoEntradaDTO. */
	private ConsultarAgenciaOpeTarifaPadraoEntradaDTO consultarAgenciaOpeTarifaPadraoEntradaDTO = null;

	// Metodos

	/**
	 * Inicializa tela.
	 *
	 * @param evt the evt
	 */
	public void inicializaTela(ActionEvent evt) {// metodo utilizado como action
		// inicial
		filtroAgendamentoEfetivacaoEstornoBean
				.setClienteContratoSelecionado(false);
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());

		setItemSelecionadoListaConsultar(null);
		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean
				.setClienteContratoSelecionado(false);
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);

		// limpar todos os campos da tela

		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setPaginaRetorno("conManterSolicGeracaoArqRetBasePagtos");
		filtroAgendamentoEfetivacaoEstornoBean
				.setEmpresaGestoraFiltro(2269651L);

		carregaListaSituacaoSolicitacao();
		setDisableArgumentosConsulta(false);

	}

	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar() {

		filtroAgendamentoEfetivacaoEstornoBean
				.setClienteContratoSelecionado(false);
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado(null);
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());

		carregaListaSituacaoSolicitacao();
		limparCamposConsultar();
		limparRadiosPrincipais();
		setDisableArgumentosConsulta(false);
		return "";

	}

	/**
	 * Limpar grid consultar.
	 *
	 * @return the string
	 */
	public String limparGridConsultar() {
		setListaGridConsultar(null);
		setItemSelecionadoListaConsultar(null);
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Limpar radios principais.
	 */
	public void limparRadiosPrincipais() {
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		limparCamposConsultar();
		setHabilitaArgumentosPesquisa(false);
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		limparCamposConsultar();

		return "";
	}

	/**
	 * Limpar radios principais incluir.
	 */
	public void limparRadiosPrincipaisIncluir() {
		vlDescontoTarifa = BigDecimal.ZERO;

		if (indicadorDescontoBloqueio) {
			vlTarifaAtualizada = vlTarifaPadrao;
		} else {
			vlTarifaAtualizada = BigDecimal.ZERO;
		}

		limparIncluir();
	}

	/**
	 * Limpar apos alterar opcao cliente incluir.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoClienteIncluir() {
		// Limpar Argumentos de Pesquisa e Lista
		setHabilitaArgumentosPesquisaIncluir(false);
		limparIncluir();
		return "";

	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato consultar.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContratoConsultar(
			ActionEvent evt) {
		limparCamposConsultar();
		setListaGridConsultar(null);
		setItemSelecionadoListaConsultar(null);
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato incluir.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContratoIncluir(
			ActionEvent evt) {
		limparIncluir();
	}

	/**
	 * Limpar campos consultar.
	 */
	public void limparCamposConsultar() {

		setItemSelecionadoListaConsultar(null);
		setCboSituacaoSolicitacao(0);
		setCboOrigemSolicitacao(0);
		setListaGridConsultar(null);
		setDataSolicitacaoDe(new Date());
		setDataSolicitacaoAte(new Date());
		setHabilitaArgumentosPesquisa(false);
		setNumeroSolicitacao(null);
	}

	/**
	 * Consultar tipo conta.
	 *
	 * @return the string
	 */
	public String consultarTipoConta() {
		try {
			ConsultarTipoContaEntradaDTO entrada = new ConsultarTipoContaEntradaDTO();
			entrada.setCdAgencia(getAgenciaContaDeb());
			entrada.setCdBanco(getBancoContaDeb());
			entrada.setCdConta(getContaContaDeb());
			entrada.setCdDigitoConta(getDigitoContaDeb());
			entrada.setCdDigitoAgencia(0);

			List<ConsultarTipoContaSaidaDTO> listaTipoConta = getComboService()
					.consultarTipoConta(entrada);
			getListaTipoContaDebHash().clear();
			setListaTipoContaDeb(new ArrayList<SelectItem>());

			for (int i = 0; i < listaTipoConta.size(); i++) {
				getListaTipoContaDeb().add(
						new SelectItem(listaTipoConta.get(i).getCdTipo(),
								listaTipoConta.get(i).getDsTipo()));
				getListaTipoContaDebHash().put(
						listaTipoConta.get(i).getCdTipo(),
						listaTipoConta.get(i));
			}
		} catch (PdcAdapterFunctionalException p) {
			setListaTipoContaDeb(new ArrayList<SelectItem>());
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "";
	}

	/**
	 * Nome: carregaListaSituacaoSolicitacao
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void carregaListaSituacaoSolicitacao() {

		try {
			listaSituacaoSolicitacao = new ArrayList<SelectItem>();

			List<ListarSituacaoSolicitacaoSaidaDTO> listaSaida = comboService
					.listarSituacaoSolicitacao();

			for (int i = 0; i < listaSaida.size(); i++) {
				listaSituacaoSolicitacao.add(new SelectItem(listaSaida.get(i)
						.getCdSolicitacaoPagamentoIntegrado(), listaSaida
						.get(i).getDsTipoSolicitacaoPagamento()));
			}

		} catch (PdcAdapterFunctionalException p) {
			listaSituacaoSolicitacao = new ArrayList<SelectItem>();
		}

	}

	/**
	 * Nome: populaGridConsultar
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void populaGridConsultar(ActionEvent evt) {
		populaGridConsultar();
	}

	/**
	 * Nome: populaGridConsultar
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void populaGridConsultar() {
		setItemSelecionadoListaConsultar(null);
		setRadioGridConsultar(null);
		listaGridConsultar = new ArrayList<ListarSolBasePagtoSaidaDTO>();
		try {
			ListarSolBasePagtoEntradaDTO entradaDTO = new ListarSolBasePagtoEntradaDTO();

			entradaDTO
					.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getNrSequenciaContratoNegocio());
			entradaDTO
					.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getCdTipoContratoNegocio());
			entradaDTO
					.setDtInicioInclusaoRemessa((getDataSolicitacaoDe() == null) ? ""
							: (FormatarData
									.formataDiaMesAnoToPdc(getDataSolicitacaoDe())));
			entradaDTO
					.setDtFinalInclusaoRemessa((getDataSolicitacaoAte() == null) ? ""
							: (FormatarData
									.formataDiaMesAnoToPdc(getDataSolicitacaoAte())));
			entradaDTO.setNrSolicitacaoPagamento(getNumeroSolicitacao());
			entradaDTO.setCdSituacaoSolicitacao(getCboSituacaoSolicitacao());
			entradaDTO.setCdOrigemSolicitacao(getCboOrigemSolicitacao());

			setListaGridConsultar(getManterSolicGeracaoArqRetBasePagtosServiceImpl()
					.listarSolBasePagto(entradaDTO));
			radioGridConsultar = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridConsultar().size(); i++) {
				this.radioGridConsultar.add(new SelectItem(i, " "));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridConsultar(null);
			setItemSelecionadoListaConsultar(null);
			setDisableArgumentosConsulta(false);
		}

	}

	/**
	 * Nome: carregaCabecalho
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void carregaCabecalho() {
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");
		if (filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado()
				.equals("0")) { // Filtrar por cliente
			setNrCnpjCpf(String.valueOf(filtroAgendamentoEfetivacaoEstornoBean
					.getNrCpfCnpjCliente()));
			setDsRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoRazaoSocialCliente());
			setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
					.getEmpresaGestoraDescCliente());
			setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getNumeroDescCliente());
			setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoContratoDescCliente());
			setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getSituacaoDescCliente());
		} else {
			if (filtroAgendamentoEfetivacaoEstornoBean
					.getTipoFiltroSelecionado().equals("1")) { // Filtrar por
				// contrato

				try {
					DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
					entradaDTO
							.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
									.getEmpresaGestoraFiltro());
					entradaDTO
							.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
									.getCdTipoContratoNegocio());
					entradaDTO
							.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
									.getNrSequenciaContratoNegocio());
					DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService()
							.detalharDadosContrato(entradaDTO);
					setNrCnpjCpf(saidaDTO.getCdCnpjCpfTitular());
					setDsRazaoSocial(saidaDTO.getDsParticipanteTitular());
				} catch (PdcAdapterFunctionalException p) {
					setNrCnpjCpf("");
					setDsRazaoSocial("");
				}
				setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
						.getEmpresaGestoraDescContrato());
				setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getNumeroDescContrato());
				setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getDescricaoContratoDescContrato());
				setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getSituacaoDescContrato());

			}
		}
	}

	/**
	 * Nome: carregarCamposDetalhar
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String carregarCamposDetalhar() {

		DetalharSolBasePagtoEntradaDTO entradaDTO = new DetalharSolBasePagtoEntradaDTO();
		ListarSolBasePagtoSaidaDTO selecaoConsulta = listaGridConsultar
				.get(itemSelecionadoListaConsultar);

		entradaDTO.setNrSolicitacaoPagamento(selecaoConsulta
				.getNrSolicitacaoPagamento());

		DetalharSolBasePagtoSaidaDTO saidaDTO = getManterSolicGeracaoArqRetBasePagtosServiceImpl()
				.detalharSolBasePagto(entradaDTO);

		setNrSolicitacaoPagamento(saidaDTO.getNrSolicitacaoPagamento());
		setHrSolicitacaoPagamento(saidaDTO.getHrSolicitacaoPagamento());
		setHrSolicitacaoPagamentoFormatada(saidaDTO
				.getHrSolicitacaoPagamentoFormatada());
		setDsSituacaoSolicitacao(saidaDTO.getDsSituacaoSolicitacao());
		setResultadoProcessamento(saidaDTO.getResultadoProcessamento());
		setHrAtendimentoSolicitacao(saidaDTO.getHrAtendimentoSolicitacao());
		setHrAtendimentoSolicitacaoFormatada(saidaDTO
				.getHrAtendimentoSolicitacaoFormatada());
		setQtdeRegistrosSolicitacao(saidaDTO.getQtdeRegistrosSolicitacao());
		setDtInicioPagamento(saidaDTO.getDtInicioPagamento());
		setDtFimPagamento(saidaDTO.getDtFimPagamento());
		setCdServico(saidaDTO.getCdServico());
		setCdServicoRelacionado(saidaDTO.getCdServicoRelacionado());
		setCdBancoDebito(saidaDTO.getCdBancoDebito());
		setCdAgenciaDebito(saidaDTO.getCdAgenciaDebito());
		setDigitoAgenciaDebito(saidaDTO.getDigitoAgenciaDebito());
		setCdContaDebito(saidaDTO.getCdContaDebito());
		setDigitoContaDebito(saidaDTO.getDigitoContaDebito());
		setContaDebitoFormatado(saidaDTO.getContaDebitoFormatado());

		setIndicadorPagos(saidaDTO.getIndicadorPagos());
		if (getIndicadorPagos().equals("S")) {
			setIndicadorPagosFormatado(MessageHelperUtils
					.getI18nMessage("label_sim"));
		} else {
			setIndicadorPagosFormatado(MessageHelperUtils
					.getI18nMessage("label_nao"));
		}

		setIndicadorNaoPagos(saidaDTO.getIndicadorNaoPagos());
		if (getIndicadorNaoPagos().equals("S")) {
			setIndicadorNaoPagosFormatado(MessageHelperUtils
					.getI18nMessage("label_sim"));
		} else {
			setIndicadorNaoPagosFormatado(MessageHelperUtils
					.getI18nMessage("label_nao"));
		}

		setIndicadorAgendados(saidaDTO.getIndicadorAgendados());
		if (getIndicadorAgendados().equals("S")) {
			setIndicadorAgendadosFormatado(MessageHelperUtils
					.getI18nMessage("label_sim"));
		} else {
			setIndicadorAgendadosFormatado(MessageHelperUtils
					.getI18nMessage("label_nao"));
		}

		setCdOrigem(saidaDTO.getCdOrigem());
		if (cdOrigem == 1) {
			setOrigemDesc(MessageHelperUtils.getI18nMessage("label_proprio"));
		}
		if (cdOrigem == 2) {
			setOrigemDesc(MessageHelperUtils.getI18nMessage("label_compror"));
		}
		if (cdOrigem == 0) {
			setOrigemDesc("");
		}

		setCdDestino(saidaDTO.getCdDestino());
		if (cdDestino == 1) {
			setDestinoDesc(MessageHelperUtils.getI18nMessage("label_cliente"));
		} else {
			setDestinoDesc(MessageHelperUtils
					.getI18nMessage("label_departamento_gestor"));
		}

		setCdFavorecido(PgitUtil.verificaLongNulo(saidaDTO.getCdFavorecido()) == 0L ? ""
				: saidaDTO.getCdFavorecido().toString());
		setDsFavorecido(saidaDTO.getDsFavorecido());
		setCdInscricaoFavorecido(PgitUtil.verificaLongNulo(saidaDTO
				.getCdInscricaoFavorecido()) == 0L ? "" : saidaDTO
				.getCdInscricaoFavorecido().toString());
		setCdTipoInscricaoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setNmMinemonicoFavorecido(saidaDTO.getNmMinemonicoFavorecido());
		setVlTarifaPadrao(saidaDTO.getVlTarifaPadrao());
		setVlDescontoTarifa(saidaDTO.getNumPercentualDescTarifa());
		setVlTarifaAtualizada(saidaDTO.getVlrTarifaAtual());

		setHrInclusaoRegistro(saidaDTO.getHrInclusaoRegistroFormatada());
		setUsuarioInclusao(saidaDTO.getCdAutenticacaoSegurancaInclusao());
		setTipoCanalInclusao((saidaDTO.getCdCananInclusao() == 0 ? ""
				: saidaDTO.getCdCananInclusao() + " - "
						+ saidaDTO.getDsCanalInclusao()));
		setComplementoInclusao(saidaDTO.getNmOperacaoFluxoInclusao() == null
				|| saidaDTO.getNmOperacaoFluxoInclusao().equals("0") ? ""
				: saidaDTO.getNmOperacaoFluxoInclusao());
		setHrManutencaoRegistro(saidaDTO.getHrManutencaoRegistroFormatada());
		setUsuarioManutencao(saidaDTO.getCdAutenticacaoSegurancaManutencao());
		setTipoCanalManutencao(saidaDTO.getTipoCanalFormatado());
		setComplementoManutencao(saidaDTO.getNmOperacaoFluxoManutencao() == null
				|| saidaDTO.getNmOperacaoFluxoManutencao().equals("0") ? ""
				: saidaDTO.getNmOperacaoFluxoManutencao());

		return "";

	}

	/**
	 * Nome: detalhar
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String detalhar() {
		carregaCabecalho();
		try {
			carregarCamposDetalhar();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}

		return "DETALHAR";
	}

	/**
	 * Nome: voltar
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String voltar() {
		setItemSelecionadoListaConsultar(null);

		return "VOLTAR";
	}

	/**
	 * Nome: voltarPrincipal
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String voltarPrincipal() {
		setItemSelecionadoListaConsultar(null);

		return "VOLTARPRINCIPAL";
	}

	/**
	 * Nome: voltarIncluir
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String voltarIncluir() {
		if (getContaDebitoFormatado() == null
				|| getContaDebitoFormatado().equals("")) {
			setBancoContaDeb(null);
			setAgenciaContaDeb(null);
			setContaContaDeb(null);
			setDigitoContaDeb(null);
		}

		return "VOLTARINCLUIR";
	}

	/**
	 * Nome: excluir
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String excluir() {
		carregaCabecalho();
		try {
			carregarCamposDetalhar();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}

		return "EXCLUIR";
	}

	/**
	 * Nome: confirmarExcluir
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void confirmarExcluir() {
		try {

			ExcluirSolBasePagtoEntradaDTO entradaDTO = new ExcluirSolBasePagtoEntradaDTO();

			entradaDTO.setNrSolicitacaoGeracaoRetorno(getListaGridConsultar()
					.get(getItemSelecionadoListaConsultar())
					.getNrSolicitacaoPagamento());
			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());

			ExcluirSolBasePagtoSaidaDTO saidaDTO = getManterSolicGeracaoArqRetBasePagtosServiceImpl()
					.excluirSolBasePagto(entradaDTO);

			BradescoFacesUtils
					.addInfoModalMessage(
							"(" + saidaDTO.getCodMensagem() + ") "
									+ saidaDTO.getMensagem(),
							"conManterSolicGeracaoArqRetBasePagtos",
							"#{manterSolicGeracaoArqRetBasePagtosBean.populaGridConsultar}",
							false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

	}

	/**
	 * Nome: incluir
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String incluir() {
		vlTarifaPadrao = BigDecimal.ZERO;
		atributoSoltc = getManterSolicitacaoRastFavService()
				.verificaraAtributoSoltc(
						new VerificaraAtributoSoltcEntradaDTO(2));

		try {
			carregaCabecalho();
			carregaTipoServico();
			carregaTipoInscricao();
			controleServicoModalidade();
			consultarAgenciaOpeTarifaPadrao();
			limparRadiosPrincipaisIncluir();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}

		return "INCLUIR";
	}

	/**
	 * Nome: calcularTarifaAtualizada
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void calcularTarifaAtualizada() {
		vlTarifaAtualizada = vlTarifaPadrao.subtract(vlTarifaPadrao.multiply(
				vlDescontoTarifa.divide(new BigDecimal(100))).setScale(2,
				BigDecimal.ROUND_HALF_UP));
	}

	/**
	 * Nome: limparIncluir
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void limparIncluir() {
		controleServicoModalidade();
		setChkContaDebito(false);
		limparCheckContaDebito();
		limparCheckFavorecido();
		setChkCompromissosPagos(false);
		setChkCompromissosNaoPagos(false);
		setChkCompromissosAgendados(false);
		setChkTipoServico(false);
		setChkModalidade(false);
		setChkContaDebito(false);
		setChkFavorecido(false);
		setCboOrigem(0);
		setRadioDestino(null);
		setDataPagamentoDe(new Date());
		setDataPagamentoAte(new Date());
		setItemSelecionadoListaIncluir(null);

	}

	/**
	 * Nome: limparCheckContaDebito
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void limparCheckContaDebito() {
		setBancoContaDeb(null);
		setAgenciaContaDeb(null);
		setContaContaDeb(null);
		setDigitoContaDeb(null);

		if (getChkContaDebito()) {
			setBancoContaDeb(237);
		}

		limparTipoContaDebito();
	}

	/**
	 * Nome: limparTipoContaDebito
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void limparTipoContaDebito() {
		setTipoContaDeb(null);
		setListaTipoContaDeb(new ArrayList<SelectItem>());
	}

	/**
	 * Nome: limparCheckFavorecido
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void limparCheckFavorecido() {
		limparCamposFavorecido();
		setItemFiltroFavSelecionado("");
	}

	/**
	 * Nome: limparCamposFavorecido
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void limparCamposFavorecido() {
		setCodigoFav(null);
		setInscricaoFav(null);
		setCboTipo(0);
		setMnemonico(null);
	}

	/**
	 * Nome: carregaTipoServico
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void carregaTipoServico() {
		try {
			listaServicos = new ArrayList<SelectItem>();

			ListarServicosEntradaDTO entrada = new ListarServicosEntradaDTO();
			entrada.setCdNaturezaServico(1);
			entrada.setNumeroOcorrencias(30);

			List<ListarServicosSaidaDTO> list = comboService
					.listarTipoServico(entrada);
			listaTipoServicoHash.clear();

			for (int i = 0; i < list.size(); i++) {
				listaTipoServicoHash.put(list.get(i).getCdServico(), list
						.get(i).getDsServico());
				listaServicos.add(new SelectItem(list.get(i).getCdServico(),
						list.get(i).getDsServico()));
			}

		} catch (PdcAdapterFunctionalException p) {
			listaServicos = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Nome: listarModalidades
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void listarModalidades() {
		try {
			setCboModalidade(0);
			if (getCboTipoServico() != null && getCboTipoServico() != 0) {
				listaModalidades = new ArrayList<SelectItem>();

				List<ConsultarModalidadePagtoSaidaDTO> list = comboService
						.consultarModalidadePagto(getCboTipoServico());

				for (ConsultarModalidadePagtoSaidaDTO saida : list) {
					listaModalidades.add(new SelectItem(saida
							.getCdProdutoOperacaoRelacionado(), saida
							.getDsProdutoOperacaoRelacionado()));
					listaModalidadeHash.put(saida
							.getCdProdutoOperacaoRelacionado(), saida
							.getDsProdutoOperacaoRelacionado());
				}
			} else {
				listaModalidades = new ArrayList<SelectItem>();
			}
		} catch (PdcAdapterFunctionalException p) {
			listaModalidades = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Nome: controleServicoModalidade
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void controleServicoModalidade() {
		setCboModalidade(null);
		setCboTipoServico(null);
		listaModalidades = new ArrayList<SelectItem>();
	}

	/**
	 * Nome: carregaTipoInscricao
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void carregaTipoInscricao() {
		listaTipoInscricao = new ArrayList<SelectItem>();

		try {
			listaTipoInscricao.add(new SelectItem(1, MessageHelperUtils
					.getI18nMessage("label_cpf")));
			listaTipoInscricao.add(new SelectItem(2, MessageHelperUtils
					.getI18nMessage("label_cnpj")));

			listaTipoInscricaoHash.put(1, MessageHelperUtils
					.getI18nMessage("label_cpf"));
			listaTipoInscricaoHash.put(2, MessageHelperUtils
					.getI18nMessage("label_cnpj"));

		} catch (PdcAdapterFunctionalException p) {
			listaTipoInscricao = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Nome: carregaFavorecido
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void carregaFavorecido() {
		try {

			ConsultarInformacoesFavorecidoEntradaDTO entradaDTO = new ConsultarInformacoesFavorecidoEntradaDTO();

			entradaDTO.setCdFavorecido(getCodigoFav());
			entradaDTO.setCdTipoIsncricaoFavorecido(getCboTipo());
			entradaDTO.setNrInscricaoFavorecido(getInscricaoFav());
			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());

			ConsultarInformacoesFavorecidoSaidaDTO saidaDTO = consultasService
					.consultarInformacoesFavorecido(entradaDTO);

			setNmFavorecidoInc(saidaDTO.getNmFavorecido());
			setCdFavorecidoInc((Long) PgitUtil.verificaZero(saidaDTO
					.getCdFavorecido()));
			setInscricaoFavorecidoInc(saidaDTO.getInscricaoFavorecido());
			setDsTipoFavorecidoInc(saidaDTO.getDsTipoFavorecido());

		} catch (PdcAdapterFunctionalException p) {
			setNmFavorecidoInc(null);
			setCdFavorecidoInc(null);
			setInscricaoFavorecidoInc(null);
			setDsTipoFavorecidoInc(null);
			setMnemonico(null);
		}
	}

	/**
	 * Nome: avancarIncluir
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String avancarIncluir() {
		calcularTarifaAtualizada();

		if (getChkFavorecido().equals(true)) {
			if (getItemFiltroFavSelecionado().equals("0")
					&& (getCodigoFav() != 0)) {
				carregaFavorecido();
			} else if (getItemFiltroFavSelecionado().equals("1")
					&& (getInscricaoFav() != 0 && getCboTipo() != 0)) {
				carregaFavorecido();
			} else if (getItemFiltroFavSelecionado().equals("2")) {
				setNmFavorecidoInc(null);
				setCdFavorecidoInc(null);
				setInscricaoFavorecidoInc(null);
				setDsTipoFavorecidoInc(null);
			}
		} else {
			setNmFavorecidoInc("");
			setCdFavorecidoInc(null);
			setInscricaoFavorecidoInc("");
			setDsTipoFavorecidoInc("");
		}

		setTipoServico((String) listaTipoServicoHash.get(getCboTipoServico()));
		setModalidade((String) listaModalidadeHash.get(getCboModalidade()));
		setTipoInscricao((String) listaTipoInscricaoHash.get(getCboTipo()));

		if (isChkCompromissosPagos()) {
			setCdIndicadorPagos("S");
			setIndicadorPagosFormatado(MessageHelperUtils
					.getI18nMessage("label_sim"));
		} else {
			setCdIndicadorPagos("N");
			setIndicadorPagosFormatado(MessageHelperUtils
					.getI18nMessage("label_nao"));
		}

		if (isChkCompromissosNaoPagos()) {
			setCdIndicadorNaoPagos("S");
			setIndicadorNaoPagosFormatado(MessageHelperUtils
					.getI18nMessage("label_sim"));
		} else {
			setCdIndicadorNaoPagos("N");
			setIndicadorNaoPagosFormatado(MessageHelperUtils
					.getI18nMessage("label_nao"));
		}

		if (isChkCompromissosAgendados()) {
			setCdIndicadorAgendados("S");
			setIndicadorAgendadosFormatado(MessageHelperUtils
					.getI18nMessage("label_sim"));
		} else {
			setCdIndicadorAgendados("N");
			setIndicadorAgendadosFormatado(MessageHelperUtils
					.getI18nMessage("label_nao"));
		}

		if (cboOrigem == 1) {
			setOrigemDesc(MessageHelperUtils.getI18nMessage("label_proprio"));
		} else if (cboOrigem == 2) {
			setOrigemDesc(MessageHelperUtils.getI18nMessage("label_compror"));
		} else if (cboOrigem == 0) {
			setOrigemDesc("");
		}

		if (radioDestino == 1) {
			setDestinoDesc(MessageHelperUtils.getI18nMessage("label_cliente"));
		} else {
			setDestinoDesc(MessageHelperUtils
					.getI18nMessage("label_departamento_gestor"));
		}

		setDataFormatadaDe(FormatarData.formataDiaMesAno(getDataPagamentoDe()));
		setDataFormatadaAte(FormatarData
				.formataDiaMesAno(getDataPagamentoAte()));
		setBancoContaDeb(PgitUtil.verificaIntegerNulo(getBancoContaDeb()));
		setAgenciaContaDeb(PgitUtil.verificaIntegerNulo(getAgenciaContaDeb()));
		setContaContaDeb(PgitUtil.verificaLongNulo(getContaContaDeb()));
		setDigitoContaDeb(PgitUtil.verificaStringNula(getDigitoContaDeb()));

		if (getBancoContaDeb() != 0 && getBancoContaDeb() != null) {
			setContaDebitoFormatado(PgitUtil.formatBancoAgenciaConta(
					getBancoContaDeb(), getAgenciaContaDeb(), null,
					getContaContaDeb(), getDigitoContaDeb(), false));
			if (getTipoContaDeb() != null && getTipoContaDeb().intValue() != 0
					&& getListaTipoContaDebHash() != null
					&& getListaTipoContaDebHash().size() > 0) {
				setTipoContaDebitoDesc(getListaTipoContaDebHash().get(
						getTipoContaDeb()).getDsTipo());
			} else {
				setTipoContaDebitoDesc("");
			}
		} else {
			setContaDebitoFormatado("");
			setTipoContaDebitoDesc("");
		}

		return "AVANCAR";
	}

	/**
	 * Nome: confirmarIncluir
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void confirmarIncluir() {
		IncluirSolBasePagtoEntradaDTO entradaDTO = new IncluirSolBasePagtoEntradaDTO();

		try {
			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entradaDTO.setDtInicioPagamento(FormatarData
					.formataDiaMesAnoToPdc(getDataPagamentoDe()));
			entradaDTO.setDtFimPagamento(FormatarData
					.formataDiaMesAnoToPdc(getDataPagamentoAte()));
			entradaDTO.setCdServico(getCboTipoServico());
			entradaDTO.setCdServicoRelacionado(getCboModalidade());
			entradaDTO.setCdBancoDebito(getBancoContaDeb());
			entradaDTO.setCdAgenciaDebito(getAgenciaContaDeb());
			entradaDTO.setCdContaDebito(getContaContaDeb());
			entradaDTO.setDigitoContaDebito(getDigitoContaDeb());
			if (getTipoContaDeb() != null && getTipoContaDeb().intValue() != 0
					&& getListaTipoContaDebHash() != null
					&& getListaTipoContaDebHash().size() > 0) {
				entradaDTO.setCdPessoaJuridicaConta(getListaTipoContaDebHash()
						.get(getTipoContaDeb()).getCdEmpresa());
				entradaDTO.setCdTipoContratoConta(getListaTipoContaDebHash()
						.get(getTipoContaDeb()).getCdTipo());
				entradaDTO
						.setNrSequenciaContratoConta(getListaTipoContaDebHash()
								.get(getTipoContaDeb()).getNrContrato());
			} else {
				entradaDTO.setCdPessoaJuridicaConta(0L);
				entradaDTO.setCdTipoContratoConta(0);
				entradaDTO.setNrSequenciaContratoConta(0L);
			}

			entradaDTO.setCdIndicadorPagos(getCdIndicadorPagos());
			entradaDTO.setCdIndicadorNaoPagos(getCdIndicadorNaoPagos());
			entradaDTO.setCdIndicadorAgendados(getCdIndicadorAgendados());
			entradaDTO.setCdFavorecido(getCdFavorecidoInc());
			entradaDTO.setCdInscricaoFavorecido(getInscricaoFav());
			entradaDTO.setCdTipoInscricaoFavorecido(getCboTipo());
			entradaDTO.setNmMinemonicoFavorecido(getMnemonico());
			entradaDTO.setCdOrigem(getCboOrigem());
			entradaDTO.setCdDestino(getRadioDestino());
			entradaDTO.setVlTarifaPadrao(vlTarifaPadrao);
			entradaDTO.setNumPercentualDescTarifa(vlDescontoTarifa);
			entradaDTO.setVlrTarifaAtual(vlTarifaAtualizada);

			IncluirSolBasePagtoSaidaDTO saidaDTO = getManterSolicGeracaoArqRetBasePagtosServiceImpl()
					.incluirSolBasePagto(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conManterSolicGeracaoArqRetBasePagtos",
					BradescoViewExceptionActionType.ACTION, false);

			if (getListaGridConsultar() != null
					&& getListaGridConsultar().size() > 0) {
				populaGridConsultar();
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Nome: pesquisar
	 * 
	 * @exception
	 * @param
	 * @return
	 * @throws
	 * @see
	 */
	public String pesquisar(ActionEvent evt) {
		populaGridConsultar();
		return "";
	}

	/**
	 * Nome: consultarAgenciaOpeTarifaPadrao
	 * 
	 * @exception
	 * @param
	 * @throws
	 * @see
	 */
	public void consultarAgenciaOpeTarifaPadrao() {
		try {		
		
			consultarAgenciaOpeTarifaPadraoEntradaDTO = new ConsultarAgenciaOpeTarifaPadraoEntradaDTO();

			consultarAgenciaOpeTarifaPadraoEntradaDTO
					.setCdPessoaJuridicaEmpresa(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			consultarAgenciaOpeTarifaPadraoEntradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			consultarAgenciaOpeTarifaPadraoEntradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			consultarAgenciaOpeTarifaPadraoEntradaDTO.setCdProdutoServicoOperacao(0);
			consultarAgenciaOpeTarifaPadraoEntradaDTO.setCdProdutoServicoRelacionado(0);
			consultarAgenciaOpeTarifaPadraoEntradaDTO.setCdTipoTarifa(8);

			ConsultarAgenciaOpeTarifaPadraoSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl()
					.consultarAgenciaOpeTarifaPadrao(consultarAgenciaOpeTarifaPadraoEntradaDTO);

			indicadorDescontoBloqueio = "N".equals(saidaDTO
					.getCdIndicadorDescontoBloqueio());

			setVlTarifaPadrao(saidaDTO.getVlTarifaPadrao());
		} catch (PdcAdapterFunctionalException p) {
			throw p;
		}
	}
	
	public boolean isDesabilitaCampoDescontoTarifa(){		
		
		if (indicadorDescontoBloqueio) {
			setVlDescontoTarifa(new BigDecimal(0));
			return true;
		}		
		
		return false;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}

	/**
	 * Get: manterSolicGeracaoArqRetBasePagtosServiceImpl.
	 *
	 * @return manterSolicGeracaoArqRetBasePagtosServiceImpl
	 */
	public IManterSolicGeracaoArqRetBasePagtosService getManterSolicGeracaoArqRetBasePagtosServiceImpl() {
		return manterSolicGeracaoArqRetBasePagtosServiceImpl;
	}

	/**
	 * Set: manterSolicGeracaoArqRetBasePagtosServiceImpl.
	 *
	 * @param manterSolicGeracaoArqRetBasePagtosServiceImpl the manter solic geracao arq ret base pagtos service impl
	 */
	public void setManterSolicGeracaoArqRetBasePagtosServiceImpl(
			IManterSolicGeracaoArqRetBasePagtosService manterSolicGeracaoArqRetBasePagtosServiceImpl) {
		this.manterSolicGeracaoArqRetBasePagtosServiceImpl = manterSolicGeracaoArqRetBasePagtosServiceImpl;
	}

	/**
	 * Get: cboSituacaoSolicitacao.
	 *
	 * @return cboSituacaoSolicitacao
	 */
	public Integer getCboSituacaoSolicitacao() {
		return cboSituacaoSolicitacao;
	}

	/**
	 * Set: cboSituacaoSolicitacao.
	 *
	 * @param cboSituacaoSolicitacao the cbo situacao solicitacao
	 */
	public void setCboSituacaoSolicitacao(Integer cboSituacaoSolicitacao) {
		this.cboSituacaoSolicitacao = cboSituacaoSolicitacao;
	}

	/**
	 * Get: dataSolicitacaoAte.
	 *
	 * @return dataSolicitacaoAte
	 */
	public Date getDataSolicitacaoAte() {
		return dataSolicitacaoAte;
	}

	/**
	 * Set: dataSolicitacaoAte.
	 *
	 * @param dataSolicitacaoAte the data solicitacao ate
	 */
	public void setDataSolicitacaoAte(Date dataSolicitacaoAte) {
		this.dataSolicitacaoAte = dataSolicitacaoAte;
	}

	/**
	 * Get: dataSolicitacaoDe.
	 *
	 * @return dataSolicitacaoDe
	 */
	public Date getDataSolicitacaoDe() {
		return dataSolicitacaoDe;
	}

	/**
	 * Set: dataSolicitacaoDe.
	 *
	 * @param dataSolicitacaoDe the data solicitacao de
	 */
	public void setDataSolicitacaoDe(Date dataSolicitacaoDe) {
		this.dataSolicitacaoDe = dataSolicitacaoDe;
	}

	/**
	 * Get: itemSelecionadoListaConsultar.
	 *
	 * @return itemSelecionadoListaConsultar
	 */
	public Integer getItemSelecionadoListaConsultar() {
		return itemSelecionadoListaConsultar;
	}

	/**
	 * Set: itemSelecionadoListaConsultar.
	 *
	 * @param itemSelecionadoListaConsultar the item selecionado lista consultar
	 */
	public void setItemSelecionadoListaConsultar(
			Integer itemSelecionadoListaConsultar) {
		this.itemSelecionadoListaConsultar = itemSelecionadoListaConsultar;
	}

	/**
	 * Get: numeroSolicitacao.
	 *
	 * @return numeroSolicitacao
	 */
	public Integer getNumeroSolicitacao() {
		return numeroSolicitacao;
	}

	/**
	 * Set: numeroSolicitacao.
	 *
	 * @param numeroSolicitacao the numero solicitacao
	 */
	public void setNumeroSolicitacao(Integer numeroSolicitacao) {
		this.numeroSolicitacao = numeroSolicitacao;
	}

	/**
	 * Get: cboOrigemSolicitacao.
	 *
	 * @return cboOrigemSolicitacao
	 */
	public Integer getCboOrigemSolicitacao() {
		return cboOrigemSolicitacao;
	}

	/**
	 * Set: cboOrigemSolicitacao.
	 *
	 * @param cboOrigemSolicitacao the cbo origem solicitacao
	 */
	public void setCboOrigemSolicitacao(Integer cboOrigemSolicitacao) {
		this.cboOrigemSolicitacao = cboOrigemSolicitacao;
	}

	/**
	 * Get: listaSituacaoSolicitacao.
	 *
	 * @return listaSituacaoSolicitacao
	 */
	public List<SelectItem> getListaSituacaoSolicitacao() {
		return listaSituacaoSolicitacao;
	}

	/**
	 * Set: listaSituacaoSolicitacao.
	 *
	 * @param listaSituacaoSolicitacao the lista situacao solicitacao
	 */
	public void setListaSituacaoSolicitacao(
			List<SelectItem> listaSituacaoSolicitacao) {
		this.listaSituacaoSolicitacao = listaSituacaoSolicitacao;
	}

	/**
	 * Get: listaGridConsultar.
	 *
	 * @return listaGridConsultar
	 */
	public List<ListarSolBasePagtoSaidaDTO> getListaGridConsultar() {
		return listaGridConsultar;
	}

	/**
	 * Set: listaGridConsultar.
	 *
	 * @param listaGridConsultar the lista grid consultar
	 */
	public void setListaGridConsultar(
			List<ListarSolBasePagtoSaidaDTO> listaGridConsultar) {
		this.listaGridConsultar = listaGridConsultar;
	}

	/**
	 * Get: radioGridConsultar.
	 *
	 * @return radioGridConsultar
	 */
	public List<SelectItem> getRadioGridConsultar() {
		return radioGridConsultar;
	}

	/**
	 * Set: radioGridConsultar.
	 *
	 * @param radioGridConsultar the radio grid consultar
	 */
	public void setRadioGridConsultar(List<SelectItem> radioGridConsultar) {
		this.radioGridConsultar = radioGridConsultar;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: cdAgenciaDebito.
	 *
	 * @return cdAgenciaDebito
	 */
	public Integer getCdAgenciaDebito() {
		return cdAgenciaDebito;
	}

	/**
	 * Set: cdAgenciaDebito.
	 *
	 * @param cdAgenciaDebito the cd agencia debito
	 */
	public void setCdAgenciaDebito(Integer cdAgenciaDebito) {
		this.cdAgenciaDebito = cdAgenciaDebito;
	}

	/**
	 * Get: cdBancoDebito.
	 *
	 * @return cdBancoDebito
	 */
	public Integer getCdBancoDebito() {
		return cdBancoDebito;
	}

	/**
	 * Set: cdBancoDebito.
	 *
	 * @param cdBancoDebito the cd banco debito
	 */
	public void setCdBancoDebito(Integer cdBancoDebito) {
		this.cdBancoDebito = cdBancoDebito;
	}

	/**
	 * Get: cdContaDebito.
	 *
	 * @return cdContaDebito
	 */
	public Long getCdContaDebito() {
		return cdContaDebito;
	}

	/**
	 * Set: cdContaDebito.
	 *
	 * @param cdContaDebito the cd conta debito
	 */
	public void setCdContaDebito(Long cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	/**
	 * Get: cdDestino.
	 *
	 * @return cdDestino
	 */
	public Integer getCdDestino() {
		return cdDestino;
	}

	/**
	 * Set: cdDestino.
	 *
	 * @param cdDestino the cd destino
	 */
	public void setCdDestino(Integer cdDestino) {
		this.cdDestino = cdDestino;
	}

	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdInscricaoFavorecido.
	 *
	 * @return cdInscricaoFavorecido
	 */
	public String getCdInscricaoFavorecido() {
		return cdInscricaoFavorecido;
	}

	/**
	 * Set: cdInscricaoFavorecido.
	 *
	 * @param cdInscricaoFavorecido the cd inscricao favorecido
	 */
	public void setCdInscricaoFavorecido(String cdInscricaoFavorecido) {
		this.cdInscricaoFavorecido = cdInscricaoFavorecido;
	}

	/**
	 * Get: cdOrigem.
	 *
	 * @return cdOrigem
	 */
	public Integer getCdOrigem() {
		return cdOrigem;
	}

	/**
	 * Set: cdOrigem.
	 *
	 * @param cdOrigem the cd origem
	 */
	public void setCdOrigem(Integer cdOrigem) {
		this.cdOrigem = cdOrigem;
	}

	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public String getCdServico() {
		return cdServico;
	}

	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(String cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Get: cdServicoRelacionado.
	 *
	 * @return cdServicoRelacionado
	 */
	public String getCdServicoRelacionado() {
		return cdServicoRelacionado;
	}

	/**
	 * Set: cdServicoRelacionado.
	 *
	 * @param cdServicoRelacionado the cd servico relacionado
	 */
	public void setCdServicoRelacionado(String cdServicoRelacionado) {
		this.cdServicoRelacionado = cdServicoRelacionado;
	}

	/**
	 * Get: cdTipoInscricaoFavorecido.
	 *
	 * @return cdTipoInscricaoFavorecido
	 */
	public String getCdTipoInscricaoFavorecido() {
		return cdTipoInscricaoFavorecido;
	}

	/**
	 * Set: cdTipoInscricaoFavorecido.
	 *
	 * @param cdTipoInscricaoFavorecido the cd tipo inscricao favorecido
	 */
	public void setCdTipoInscricaoFavorecido(String cdTipoInscricaoFavorecido) {
		this.cdTipoInscricaoFavorecido = cdTipoInscricaoFavorecido;
	}

	/**
	 * Get: digitoAgenciaDebito.
	 *
	 * @return digitoAgenciaDebito
	 */
	public Integer getDigitoAgenciaDebito() {
		return digitoAgenciaDebito;
	}

	/**
	 * Set: digitoAgenciaDebito.
	 *
	 * @param digitoAgenciaDebito the digito agencia debito
	 */
	public void setDigitoAgenciaDebito(Integer digitoAgenciaDebito) {
		this.digitoAgenciaDebito = digitoAgenciaDebito;
	}

	/**
	 * Get: digitoContaDebito.
	 *
	 * @return digitoContaDebito
	 */
	public String getDigitoContaDebito() {
		return digitoContaDebito;
	}

	/**
	 * Set: digitoContaDebito.
	 *
	 * @param digitoContaDebito the digito conta debito
	 */
	public void setDigitoContaDebito(String digitoContaDebito) {
		this.digitoContaDebito = digitoContaDebito;
	}

	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}

	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}

	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}

	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}

	/**
	 * Get: dtFimPagamento.
	 *
	 * @return dtFimPagamento
	 */
	public String getDtFimPagamento() {
		return dtFimPagamento;
	}

	/**
	 * Set: dtFimPagamento.
	 *
	 * @param dtFimPagamento the dt fim pagamento
	 */
	public void setDtFimPagamento(String dtFimPagamento) {
		this.dtFimPagamento = dtFimPagamento;
	}

	/**
	 * Get: dtInicioPagamento.
	 *
	 * @return dtInicioPagamento
	 */
	public String getDtInicioPagamento() {
		return dtInicioPagamento;
	}

	/**
	 * Set: dtInicioPagamento.
	 *
	 * @param dtInicioPagamento the dt inicio pagamento
	 */
	public void setDtInicioPagamento(String dtInicioPagamento) {
		this.dtInicioPagamento = dtInicioPagamento;
	}

	/**
	 * Get: hrAtendimentoSolicitacao.
	 *
	 * @return hrAtendimentoSolicitacao
	 */
	public String getHrAtendimentoSolicitacao() {
		return hrAtendimentoSolicitacao;
	}

	/**
	 * Set: hrAtendimentoSolicitacao.
	 *
	 * @param hrAtendimentoSolicitacao the hr atendimento solicitacao
	 */
	public void setHrAtendimentoSolicitacao(String hrAtendimentoSolicitacao) {
		this.hrAtendimentoSolicitacao = hrAtendimentoSolicitacao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	/**
	 * Get: hrSolicitacaoPagamento.
	 *
	 * @return hrSolicitacaoPagamento
	 */
	public String getHrSolicitacaoPagamento() {
		return hrSolicitacaoPagamento;
	}

	/**
	 * Set: hrSolicitacaoPagamento.
	 *
	 * @param hrSolicitacaoPagamento the hr solicitacao pagamento
	 */
	public void setHrSolicitacaoPagamento(String hrSolicitacaoPagamento) {
		this.hrSolicitacaoPagamento = hrSolicitacaoPagamento;
	}

	/**
	 * Get: indicadorAgendados.
	 *
	 * @return indicadorAgendados
	 */
	public String getIndicadorAgendados() {
		return indicadorAgendados;
	}

	/**
	 * Set: indicadorAgendados.
	 *
	 * @param indicadorAgendados the indicador agendados
	 */
	public void setIndicadorAgendados(String indicadorAgendados) {
		this.indicadorAgendados = indicadorAgendados;
	}

	/**
	 * Get: indicadorNaoPagos.
	 *
	 * @return indicadorNaoPagos
	 */
	public String getIndicadorNaoPagos() {
		return indicadorNaoPagos;
	}

	/**
	 * Set: indicadorNaoPagos.
	 *
	 * @param indicadorNaoPagos the indicador nao pagos
	 */
	public void setIndicadorNaoPagos(String indicadorNaoPagos) {
		this.indicadorNaoPagos = indicadorNaoPagos;
	}

	/**
	 * Get: indicadorPagos.
	 *
	 * @return indicadorPagos
	 */
	public String getIndicadorPagos() {
		return indicadorPagos;
	}

	/**
	 * Set: indicadorPagos.
	 *
	 * @param indicadorPagos the indicador pagos
	 */
	public void setIndicadorPagos(String indicadorPagos) {
		this.indicadorPagos = indicadorPagos;
	}

	/**
	 * Get: nmMinemonicoFavorecido.
	 *
	 * @return nmMinemonicoFavorecido
	 */
	public String getNmMinemonicoFavorecido() {
		return nmMinemonicoFavorecido;
	}

	/**
	 * Set: nmMinemonicoFavorecido.
	 *
	 * @param nmMinemonicoFavorecido the nm minemonico favorecido
	 */
	public void setNmMinemonicoFavorecido(String nmMinemonicoFavorecido) {
		this.nmMinemonicoFavorecido = nmMinemonicoFavorecido;
	}

	/**
	 * Get: nrSolicitacaoPagamento.
	 *
	 * @return nrSolicitacaoPagamento
	 */
	public Integer getNrSolicitacaoPagamento() {
		return nrSolicitacaoPagamento;
	}

	/**
	 * Set: nrSolicitacaoPagamento.
	 *
	 * @param nrSolicitacaoPagamento the nr solicitacao pagamento
	 */
	public void setNrSolicitacaoPagamento(Integer nrSolicitacaoPagamento) {
		this.nrSolicitacaoPagamento = nrSolicitacaoPagamento;
	}

	/**
	 * Get: qtdeRegistrosSolicitacao.
	 *
	 * @return qtdeRegistrosSolicitacao
	 */
	public Long getQtdeRegistrosSolicitacao() {
		return qtdeRegistrosSolicitacao;
	}

	/**
	 * Set: qtdeRegistrosSolicitacao.
	 *
	 * @param qtdeRegistrosSolicitacao the qtde registros solicitacao
	 */
	public void setQtdeRegistrosSolicitacao(Long qtdeRegistrosSolicitacao) {
		this.qtdeRegistrosSolicitacao = qtdeRegistrosSolicitacao;
	}

	/**
	 * Get: resultadoProcessamento.
	 *
	 * @return resultadoProcessamento
	 */
	public String getResultadoProcessamento() {
		return resultadoProcessamento;
	}

	/**
	 * Set: resultadoProcessamento.
	 *
	 * @param resultadoProcessamento the resultado processamento
	 */
	public void setResultadoProcessamento(String resultadoProcessamento) {
		this.resultadoProcessamento = resultadoProcessamento;
	}

	/**
	 * Get: hrAtendimentoSolicitacaoFormatada.
	 *
	 * @return hrAtendimentoSolicitacaoFormatada
	 */
	public String getHrAtendimentoSolicitacaoFormatada() {
		return hrAtendimentoSolicitacaoFormatada;
	}

	/**
	 * Set: hrAtendimentoSolicitacaoFormatada.
	 *
	 * @param hrAtendimentoSolicitacaoFormatada the hr atendimento solicitacao formatada
	 */
	public void setHrAtendimentoSolicitacaoFormatada(
			String hrAtendimentoSolicitacaoFormatada) {
		this.hrAtendimentoSolicitacaoFormatada = hrAtendimentoSolicitacaoFormatada;
	}

	/**
	 * Get: hrSolicitacaoPagamentoFormatada.
	 *
	 * @return hrSolicitacaoPagamentoFormatada
	 */
	public String getHrSolicitacaoPagamentoFormatada() {
		return hrSolicitacaoPagamentoFormatada;
	}

	/**
	 * Set: hrSolicitacaoPagamentoFormatada.
	 *
	 * @param hrSolicitacaoPagamentoFormatada the hr solicitacao pagamento formatada
	 */
	public void setHrSolicitacaoPagamentoFormatada(
			String hrSolicitacaoPagamentoFormatada) {
		this.hrSolicitacaoPagamentoFormatada = hrSolicitacaoPagamentoFormatada;
	}

	/**
	 * Get: destinoDesc.
	 *
	 * @return destinoDesc
	 */
	public String getDestinoDesc() {
		return destinoDesc;
	}

	/**
	 * Set: destinoDesc.
	 *
	 * @param destinoDesc the destino desc
	 */
	public void setDestinoDesc(String destinoDesc) {
		this.destinoDesc = destinoDesc;
	}

	/**
	 * Get: origemDesc.
	 *
	 * @return origemDesc
	 */
	public String getOrigemDesc() {
		return origemDesc;
	}

	/**
	 * Set: origemDesc.
	 *
	 * @param origemDesc the origem desc
	 */
	public void setOrigemDesc(String origemDesc) {
		this.origemDesc = origemDesc;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: contaDebitoFormatado.
	 *
	 * @return contaDebitoFormatado
	 */
	public String getContaDebitoFormatado() {
		return contaDebitoFormatado;
	}

	/**
	 * Set: contaDebitoFormatado.
	 *
	 * @param contaDebitoFormatado the conta debito formatado
	 */
	public void setContaDebitoFormatado(String contaDebitoFormatado) {
		this.contaDebitoFormatado = contaDebitoFormatado;
	}

	/**
	 * Get: dataPagamentoAte.
	 *
	 * @return dataPagamentoAte
	 */
	public Date getDataPagamentoAte() {
		return dataPagamentoAte;
	}

	/**
	 * Set: dataPagamentoAte.
	 *
	 * @param dataPagamentoAte the data pagamento ate
	 */
	public void setDataPagamentoAte(Date dataPagamentoAte) {
		this.dataPagamentoAte = dataPagamentoAte;
	}

	/**
	 * Get: dataPagamentoDe.
	 *
	 * @return dataPagamentoDe
	 */
	public Date getDataPagamentoDe() {
		return dataPagamentoDe;
	}

	/**
	 * Set: dataPagamentoDe.
	 *
	 * @param dataPagamentoDe the data pagamento de
	 */
	public void setDataPagamentoDe(Date dataPagamentoDe) {
		this.dataPagamentoDe = dataPagamentoDe;
	}

	/**
	 * Get: itemSelecionadoListaIncluir.
	 *
	 * @return itemSelecionadoListaIncluir
	 */
	public Integer getItemSelecionadoListaIncluir() {
		return itemSelecionadoListaIncluir;
	}

	/**
	 * Set: itemSelecionadoListaIncluir.
	 *
	 * @param itemSelecionadoListaIncluir the item selecionado lista incluir
	 */
	public void setItemSelecionadoListaIncluir(
			Integer itemSelecionadoListaIncluir) {
		this.itemSelecionadoListaIncluir = itemSelecionadoListaIncluir;
	}

	/**
	 * Get: cboModalidade.
	 *
	 * @return cboModalidade
	 */
	public Integer getCboModalidade() {
		return cboModalidade;
	}

	/**
	 * Set: cboModalidade.
	 *
	 * @param cboModalidade the cbo modalidade
	 */
	public void setCboModalidade(Integer cboModalidade) {
		this.cboModalidade = cboModalidade;
	}

	/**
	 * Get: cboTipoServico.
	 *
	 * @return cboTipoServico
	 */
	public Integer getCboTipoServico() {
		return cboTipoServico;
	}

	/**
	 * Set: cboTipoServico.
	 *
	 * @param cboTipoServico the cbo tipo servico
	 */
	public void setCboTipoServico(Integer cboTipoServico) {
		this.cboTipoServico = cboTipoServico;
	}

	/**
	 * Get: chkModalidade.
	 *
	 * @return chkModalidade
	 */
	public Boolean getChkModalidade() {
		return chkModalidade;
	}

	/**
	 * Set: chkModalidade.
	 *
	 * @param chkModalidade the chk modalidade
	 */
	public void setChkModalidade(Boolean chkModalidade) {
		this.chkModalidade = chkModalidade;
	}

	/**
	 * Get: chkTipoServico.
	 *
	 * @return chkTipoServico
	 */
	public Boolean getChkTipoServico() {
		return chkTipoServico;
	}

	/**
	 * Set: chkTipoServico.
	 *
	 * @param chkTipoServico the chk tipo servico
	 */
	public void setChkTipoServico(Boolean chkTipoServico) {
		this.chkTipoServico = chkTipoServico;
	}

	/**
	 * Get: chkContaDebito.
	 *
	 * @return chkContaDebito
	 */
	public Boolean getChkContaDebito() {
		return chkContaDebito;
	}

	/**
	 * Set: chkContaDebito.
	 *
	 * @param chkContaDebito the chk conta debito
	 */
	public void setChkContaDebito(Boolean chkContaDebito) {
		this.chkContaDebito = chkContaDebito;
	}

	/**
	 * Get: chkFavorecido.
	 *
	 * @return chkFavorecido
	 */
	public Boolean getChkFavorecido() {
		return chkFavorecido;
	}

	/**
	 * Set: chkFavorecido.
	 *
	 * @param chkFavorecido the chk favorecido
	 */
	public void setChkFavorecido(Boolean chkFavorecido) {
		this.chkFavorecido = chkFavorecido;
	}

	/**
	 * Get: listaModalidades.
	 *
	 * @return listaModalidades
	 */
	public List<SelectItem> getListaModalidades() {
		return listaModalidades;
	}

	/**
	 * Set: listaModalidades.
	 *
	 * @param listaModalidades the lista modalidades
	 */
	public void setListaModalidades(List<SelectItem> listaModalidades) {
		this.listaModalidades = listaModalidades;
	}

	/**
	 * Get: listaServicos.
	 *
	 * @return listaServicos
	 */
	public List<SelectItem> getListaServicos() {
		return listaServicos;
	}

	/**
	 * Set: listaServicos.
	 *
	 * @param listaServicos the lista servicos
	 */
	public void setListaServicos(List<SelectItem> listaServicos) {
		this.listaServicos = listaServicos;
	}

	/**
	 * Get: agenciaContaDeb.
	 *
	 * @return agenciaContaDeb
	 */
	public Integer getAgenciaContaDeb() {
		return agenciaContaDeb;
	}

	/**
	 * Set: agenciaContaDeb.
	 *
	 * @param agenciaContaDeb the agencia conta deb
	 */
	public void setAgenciaContaDeb(Integer agenciaContaDeb) {
		this.agenciaContaDeb = agenciaContaDeb;
	}

	/**
	 * Get: bancoContaDeb.
	 *
	 * @return bancoContaDeb
	 */
	public Integer getBancoContaDeb() {
		return bancoContaDeb;
	}

	/**
	 * Set: bancoContaDeb.
	 *
	 * @param bancoContaDeb the banco conta deb
	 */
	public void setBancoContaDeb(Integer bancoContaDeb) {
		this.bancoContaDeb = bancoContaDeb;
	}

	/**
	 * Get: contaContaDeb.
	 *
	 * @return contaContaDeb
	 */
	public Long getContaContaDeb() {
		return contaContaDeb;
	}

	/**
	 * Set: contaContaDeb.
	 *
	 * @param contaContaDeb the conta conta deb
	 */
	public void setContaContaDeb(Long contaContaDeb) {
		this.contaContaDeb = contaContaDeb;
	}

	/**
	 * Get: digitoContaDeb.
	 *
	 * @return digitoContaDeb
	 */
	public String getDigitoContaDeb() {
		return digitoContaDeb;
	}

	/**
	 * Set: digitoContaDeb.
	 *
	 * @param digitoContaDeb the digito conta deb
	 */
	public void setDigitoContaDeb(String digitoContaDeb) {
		this.digitoContaDeb = digitoContaDeb;
	}

	/**
	 * Is chk compromissos agendados.
	 *
	 * @return true, if is chk compromissos agendados
	 */
	public boolean isChkCompromissosAgendados() {
		return chkCompromissosAgendados;
	}

	/**
	 * Set: chkCompromissosAgendados.
	 *
	 * @param chkCompromissosAgendados the chk compromissos agendados
	 */
	public void setChkCompromissosAgendados(boolean chkCompromissosAgendados) {
		this.chkCompromissosAgendados = chkCompromissosAgendados;
	}

	/**
	 * Is chk compromissos nao pagos.
	 *
	 * @return true, if is chk compromissos nao pagos
	 */
	public boolean isChkCompromissosNaoPagos() {
		return chkCompromissosNaoPagos;
	}

	/**
	 * Set: chkCompromissosNaoPagos.
	 *
	 * @param chkCompromissosNaoPagos the chk compromissos nao pagos
	 */
	public void setChkCompromissosNaoPagos(boolean chkCompromissosNaoPagos) {
		this.chkCompromissosNaoPagos = chkCompromissosNaoPagos;
	}

	/**
	 * Is chk compromissos pagos.
	 *
	 * @return true, if is chk compromissos pagos
	 */
	public boolean isChkCompromissosPagos() {
		return chkCompromissosPagos;
	}

	/**
	 * Set: chkCompromissosPagos.
	 *
	 * @param chkCompromissosPagos the chk compromissos pagos
	 */
	public void setChkCompromissosPagos(boolean chkCompromissosPagos) {
		this.chkCompromissosPagos = chkCompromissosPagos;
	}

	/**
	 * Set: chkContaDebito.
	 *
	 * @param chkContaDebito the chk conta debito
	 */
	public void setChkContaDebito(boolean chkContaDebito) {
		this.chkContaDebito = chkContaDebito;
	}

	/**
	 * Set: chkFavorecido.
	 *
	 * @param chkFavorecido the chk favorecido
	 */
	public void setChkFavorecido(boolean chkFavorecido) {
		this.chkFavorecido = chkFavorecido;
	}

	/**
	 * Set: chkModalidade.
	 *
	 * @param chkModalidade the chk modalidade
	 */
	public void setChkModalidade(boolean chkModalidade) {
		this.chkModalidade = chkModalidade;
	}

	/**
	 * Set: chkTipoServico.
	 *
	 * @param chkTipoServico the chk tipo servico
	 */
	public void setChkTipoServico(boolean chkTipoServico) {
		this.chkTipoServico = chkTipoServico;
	}

	/**
	 * Get: itemFiltroFavSelecionado.
	 *
	 * @return itemFiltroFavSelecionado
	 */
	public String getItemFiltroFavSelecionado() {
		return itemFiltroFavSelecionado;
	}

	/**
	 * Set: itemFiltroFavSelecionado.
	 *
	 * @param itemFiltroFavSelecionado the item filtro fav selecionado
	 */
	public void setItemFiltroFavSelecionado(String itemFiltroFavSelecionado) {
		this.itemFiltroFavSelecionado = itemFiltroFavSelecionado;
	}

	/**
	 * Get: codigoFav.
	 *
	 * @return codigoFav
	 */
	public Long getCodigoFav() {
		return codigoFav;
	}

	/**
	 * Set: codigoFav.
	 *
	 * @param codigoFav the codigo fav
	 */
	public void setCodigoFav(Long codigoFav) {
		this.codigoFav = codigoFav;
	}

	/**
	 * Get: cboTipo.
	 *
	 * @return cboTipo
	 */
	public Integer getCboTipo() {
		return cboTipo;
	}

	/**
	 * Set: cboTipo.
	 *
	 * @param cboTipo the cbo tipo
	 */
	public void setCboTipo(Integer cboTipo) {
		this.cboTipo = cboTipo;
	}

	/**
	 * Get: cboOrigem.
	 *
	 * @return cboOrigem
	 */
	public Integer getCboOrigem() {
		return cboOrigem;
	}

	/**
	 * Set: cboOrigem.
	 *
	 * @param cboOrigem the cbo origem
	 */
	public void setCboOrigem(Integer cboOrigem) {
		this.cboOrigem = cboOrigem;
	}

	/**
	 * Get: radioDestino.
	 *
	 * @return radioDestino
	 */
	public Integer getRadioDestino() {
		return radioDestino;
	}

	/**
	 * Set: radioDestino.
	 *
	 * @param radioDestino the radio destino
	 */
	public void setRadioDestino(Integer radioDestino) {
		this.radioDestino = radioDestino;
	}

	/**
	 * Get: vlDescontoTarifa.
	 *
	 * @return vlDescontoTarifa
	 */
	public BigDecimal getVlDescontoTarifa() {
		return vlDescontoTarifa;
	}

	/**
	 * Set: vlDescontoTarifa.
	 *
	 * @param vlDescontoTarifa the vl desconto tarifa
	 */
	public void setVlDescontoTarifa(BigDecimal vlDescontoTarifa) {
		this.vlDescontoTarifa = vlDescontoTarifa;
	}

	/**
	 * Get: inscricaoFav.
	 *
	 * @return inscricaoFav
	 */
	public Long getInscricaoFav() {
		return inscricaoFav;
	}

	/**
	 * Set: inscricaoFav.
	 *
	 * @param inscricaoFav the inscricao fav
	 */
	public void setInscricaoFav(Long inscricaoFav) {
		this.inscricaoFav = inscricaoFav;
	}

	/**
	 * Get: listaTipoInscricao.
	 *
	 * @return listaTipoInscricao
	 */
	public List<SelectItem> getListaTipoInscricao() {
		return listaTipoInscricao;
	}

	/**
	 * Set: listaTipoInscricao.
	 *
	 * @param listaTipoInscricao the lista tipo inscricao
	 */
	public void setListaTipoInscricao(List<SelectItem> listaTipoInscricao) {
		this.listaTipoInscricao = listaTipoInscricao;
	}

	/**
	 * Get: cdIndicadorPagos.
	 *
	 * @return cdIndicadorPagos
	 */
	public String getCdIndicadorPagos() {
		return cdIndicadorPagos;
	}

	/**
	 * Set: cdIndicadorPagos.
	 *
	 * @param cdIndicadorPagos the cd indicador pagos
	 */
	public void setCdIndicadorPagos(String cdIndicadorPagos) {
		this.cdIndicadorPagos = cdIndicadorPagos;
	}

	/**
	 * Get: cdIndicadorAgendados.
	 *
	 * @return cdIndicadorAgendados
	 */
	public String getCdIndicadorAgendados() {
		return cdIndicadorAgendados;
	}

	/**
	 * Set: cdIndicadorAgendados.
	 *
	 * @param cdIndicadorAgendados the cd indicador agendados
	 */
	public void setCdIndicadorAgendados(String cdIndicadorAgendados) {
		this.cdIndicadorAgendados = cdIndicadorAgendados;
	}

	/**
	 * Get: cdIndicadorNaoPagos.
	 *
	 * @return cdIndicadorNaoPagos
	 */
	public String getCdIndicadorNaoPagos() {
		return cdIndicadorNaoPagos;
	}

	/**
	 * Set: cdIndicadorNaoPagos.
	 *
	 * @param cdIndicadorNaoPagos the cd indicador nao pagos
	 */
	public void setCdIndicadorNaoPagos(String cdIndicadorNaoPagos) {
		this.cdIndicadorNaoPagos = cdIndicadorNaoPagos;
	}

	/**
	 * Get: indicadorAgendadosFormatado.
	 *
	 * @return indicadorAgendadosFormatado
	 */
	public String getIndicadorAgendadosFormatado() {
		return indicadorAgendadosFormatado;
	}

	/**
	 * Set: indicadorAgendadosFormatado.
	 *
	 * @param indicadorAgendadosFormatado the indicador agendados formatado
	 */
	public void setIndicadorAgendadosFormatado(
			String indicadorAgendadosFormatado) {
		this.indicadorAgendadosFormatado = indicadorAgendadosFormatado;
	}

	/**
	 * Get: indicadorNaoPagosFormatado.
	 *
	 * @return indicadorNaoPagosFormatado
	 */
	public String getIndicadorNaoPagosFormatado() {
		return indicadorNaoPagosFormatado;
	}

	/**
	 * Set: indicadorNaoPagosFormatado.
	 *
	 * @param indicadorNaoPagosFormatado the indicador nao pagos formatado
	 */
	public void setIndicadorNaoPagosFormatado(String indicadorNaoPagosFormatado) {
		this.indicadorNaoPagosFormatado = indicadorNaoPagosFormatado;
	}

	/**
	 * Get: indicadorPagosFormatado.
	 *
	 * @return indicadorPagosFormatado
	 */
	public String getIndicadorPagosFormatado() {
		return indicadorPagosFormatado;
	}

	/**
	 * Set: indicadorPagosFormatado.
	 *
	 * @param indicadorPagosFormatado the indicador pagos formatado
	 */
	public void setIndicadorPagosFormatado(String indicadorPagosFormatado) {
		this.indicadorPagosFormatado = indicadorPagosFormatado;
	}

	/**
	 * Get: mnemonico.
	 *
	 * @return mnemonico
	 */
	public String getMnemonico() {
		return mnemonico;
	}

	/**
	 * Set: mnemonico.
	 *
	 * @param mnemonico the mnemonico
	 */
	public void setMnemonico(String mnemonico) {
		this.mnemonico = mnemonico;
	}

	/**
	 * Get: dataFormatadaAte.
	 *
	 * @return dataFormatadaAte
	 */
	public String getDataFormatadaAte() {
		return dataFormatadaAte;
	}

	/**
	 * Set: dataFormatadaAte.
	 *
	 * @param dataFormatadaAte the data formatada ate
	 */
	public void setDataFormatadaAte(String dataFormatadaAte) {
		this.dataFormatadaAte = dataFormatadaAte;
	}

	/**
	 * Get: dataFormatadaDe.
	 *
	 * @return dataFormatadaDe
	 */
	public String getDataFormatadaDe() {
		return dataFormatadaDe;
	}

	/**
	 * Set: dataFormatadaDe.
	 *
	 * @param dataFormatadaDe the data formatada de
	 */
	public void setDataFormatadaDe(String dataFormatadaDe) {
		this.dataFormatadaDe = dataFormatadaDe;
	}

	/**
	 * Get: listaModalidadeHash.
	 *
	 * @return listaModalidadeHash
	 */
	public Map<Integer, String> getListaModalidadeHash() {
		return listaModalidadeHash;
	}

	/**
	 * Set lista modalidade hash.
	 *
	 * @param listaModalidadeHash the lista modalidade hash
	 */
	public void setListaModalidadeHash(Map<Integer, String> listaModalidadeHash) {
		this.listaModalidadeHash = listaModalidadeHash;
	}

	/**
	 * Get: listaTipoInscricaoHash.
	 *
	 * @return listaTipoInscricaoHash
	 */
	public Map<Integer, String> getListaTipoInscricaoHash() {
		return listaTipoInscricaoHash;
	}

	/**
	 * Set lista tipo inscricao hash.
	 *
	 * @param listaTipoInscricaoHash the lista tipo inscricao hash
	 */
	public void setListaTipoInscricaoHash(
			Map<Integer, String> listaTipoInscricaoHash) {
		this.listaTipoInscricaoHash = listaTipoInscricaoHash;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(
			Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public String getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Get: modalidade.
	 *
	 * @return modalidade
	 */
	public String getModalidade() {
		return modalidade;
	}

	/**
	 * Set: modalidade.
	 *
	 * @param modalidade the modalidade
	 */
	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}

	/**
	 * Get: tipoInscricao.
	 *
	 * @return tipoInscricao
	 */
	public String getTipoInscricao() {
		return tipoInscricao;
	}

	/**
	 * Set: tipoInscricao.
	 *
	 * @param tipoInscricao the tipo inscricao
	 */
	public void setTipoInscricao(String tipoInscricao) {
		this.tipoInscricao = tipoInscricao;
	}

	/**
	 * Is habilita argumentos pesquisa incluir.
	 *
	 * @return true, if is habilita argumentos pesquisa incluir
	 */
	public boolean isHabilitaArgumentosPesquisaIncluir() {
		return habilitaArgumentosPesquisaIncluir;
	}

	/**
	 * Set: habilitaArgumentosPesquisaIncluir.
	 *
	 * @param habilitaArgumentosPesquisaIncluir the habilita argumentos pesquisa incluir
	 */
	public void setHabilitaArgumentosPesquisaIncluir(
			boolean habilitaArgumentosPesquisaIncluir) {
		this.habilitaArgumentosPesquisaIncluir = habilitaArgumentosPesquisaIncluir;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasService the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
		this.consultasService = consultasService;
	}

	/**
	 * Get: cdFavorecidoInc.
	 *
	 * @return cdFavorecidoInc
	 */
	public Long getCdFavorecidoInc() {
		return cdFavorecidoInc;
	}

	/**
	 * Set: cdFavorecidoInc.
	 *
	 * @param cdFavorecidoInc the cd favorecido inc
	 */
	public void setCdFavorecidoInc(Long cdFavorecidoInc) {
		this.cdFavorecidoInc = cdFavorecidoInc;
	}

	/**
	 * Get: dsTipoFavorecidoInc.
	 *
	 * @return dsTipoFavorecidoInc
	 */
	public String getDsTipoFavorecidoInc() {
		return dsTipoFavorecidoInc;
	}

	/**
	 * Set: dsTipoFavorecidoInc.
	 *
	 * @param dsTipoFavorecidoInc the ds tipo favorecido inc
	 */
	public void setDsTipoFavorecidoInc(String dsTipoFavorecidoInc) {
		this.dsTipoFavorecidoInc = dsTipoFavorecidoInc;
	}

	/**
	 * Get: inscricaoFavorecidoInc.
	 *
	 * @return inscricaoFavorecidoInc
	 */
	public String getInscricaoFavorecidoInc() {
		return inscricaoFavorecidoInc;
	}

	/**
	 * Set: inscricaoFavorecidoInc.
	 *
	 * @param inscricaoFavorecidoInc the inscricao favorecido inc
	 */
	public void setInscricaoFavorecidoInc(String inscricaoFavorecidoInc) {
		this.inscricaoFavorecidoInc = inscricaoFavorecidoInc;
	}

	/**
	 * Get: nmFavorecidoInc.
	 *
	 * @return nmFavorecidoInc
	 */
	public String getNmFavorecidoInc() {
		return nmFavorecidoInc;
	}

	/**
	 * Set: nmFavorecidoInc.
	 *
	 * @param nmFavorecidoInc the nm favorecido inc
	 */
	public void setNmFavorecidoInc(String nmFavorecidoInc) {
		this.nmFavorecidoInc = nmFavorecidoInc;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: listaTipoContaDeb.
	 *
	 * @return listaTipoContaDeb
	 */
	public List<SelectItem> getListaTipoContaDeb() {
		return listaTipoContaDeb;
	}

	/**
	 * Set: listaTipoContaDeb.
	 *
	 * @param listaTipoContaDeb the lista tipo conta deb
	 */
	public void setListaTipoContaDeb(List<SelectItem> listaTipoContaDeb) {
		this.listaTipoContaDeb = listaTipoContaDeb;
	}

	/**
	 * Get: listaTipoContaDebHash.
	 *
	 * @return listaTipoContaDebHash
	 */
	public Map<Integer, ConsultarTipoContaSaidaDTO> getListaTipoContaDebHash() {
		return listaTipoContaDebHash;
	}

	/**
	 * Set lista tipo conta deb hash.
	 *
	 * @param listaTipoContaDebHash the lista tipo conta deb hash
	 */
	public void setListaTipoContaDebHash(
			Map<Integer, ConsultarTipoContaSaidaDTO> listaTipoContaDebHash) {
		this.listaTipoContaDebHash = listaTipoContaDebHash;
	}

	/**
	 * Get: tipoContaDeb.
	 *
	 * @return tipoContaDeb
	 */
	public Integer getTipoContaDeb() {
		return tipoContaDeb;
	}

	/**
	 * Set: tipoContaDeb.
	 *
	 * @param tipoContaDeb the tipo conta deb
	 */
	public void setTipoContaDeb(Integer tipoContaDeb) {
		this.tipoContaDeb = tipoContaDeb;
	}

	/**
	 * Get: tipoContaDebitoDesc.
	 *
	 * @return tipoContaDebitoDesc
	 */
	public String getTipoContaDebitoDesc() {
		return tipoContaDebitoDesc;
	}

	/**
	 * Set: tipoContaDebitoDesc.
	 *
	 * @param tipoContaDebitoDesc the tipo conta debito desc
	 */
	public void setTipoContaDebitoDesc(String tipoContaDebitoDesc) {
		this.tipoContaDebitoDesc = tipoContaDebitoDesc;
	}

	/**
	 * Get: foco.
	 *
	 * @return foco
	 */
	public String getFoco() {
		return foco;
	}

	/**
	 * Set: foco.
	 *
	 * @param foco the foco
	 */
	public void setFoco(String foco) {
		this.foco = foco;
	}

	/**
	 * Is desabilita desconto tarifa.
	 *
	 * @return true, if is desabilita desconto tarifa
	 */
	public boolean isDesabilitaDescontoTarifa() {
		if (atributoSoltc != null
				&& "S".equalsIgnoreCase(atributoSoltc.getDsTarifaBloqueio())) {
			return true;
		}
		return false;
	}

	/**
	 * Get: manterSolicitacaoRastFavService.
	 *
	 * @return manterSolicitacaoRastFavService
	 */
	public IManterSolicitacaoRastFavService getManterSolicitacaoRastFavService() {
		return manterSolicitacaoRastFavService;
	}

	/**
	 * Set: manterSolicitacaoRastFavService.
	 *
	 * @param manterSolicitacaoRastFavService the manter solicitacao rast fav service
	 */
	public void setManterSolicitacaoRastFavService(
			IManterSolicitacaoRastFavService manterSolicitacaoRastFavService) {
		this.manterSolicitacaoRastFavService = manterSolicitacaoRastFavService;
	}

	/**
	 * Get: atributoSoltc.
	 *
	 * @return atributoSoltc
	 */
	public VerificaraAtributoSoltcSaidaDTO getAtributoSoltc() {
		return atributoSoltc;
	}

	/**
	 * Set: atributoSoltc.
	 *
	 * @param atributoSoltc the atributo soltc
	 */
	public void setAtributoSoltc(VerificaraAtributoSoltcSaidaDTO atributoSoltc) {
		this.atributoSoltc = atributoSoltc;
	}

	/**
	 * Get: vlTarifaPadrao.
	 *
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}

	/**
	 * Set: vlTarifaPadrao.
	 *
	 * @param vlTarifaPadrao the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}

	/**
	 * Get: vlTarifaAtualizada.
	 *
	 * @return vlTarifaAtualizada
	 */
	public BigDecimal getVlTarifaAtualizada() {
		return vlTarifaAtualizada;
	}

	/**
	 * Set: vlTarifaAtualizada.
	 *
	 * @param vlTarifaAtualizada the vl tarifa atualizada
	 */
	public void setVlTarifaAtualizada(BigDecimal vlTarifaAtualizada) {
		this.vlTarifaAtualizada = vlTarifaAtualizada;
	}

	/**
	 * Is indicador desconto bloqueio.
	 *
	 * @return true, if is indicador desconto bloqueio
	 */
	public boolean isIndicadorDescontoBloqueio() {
		return indicadorDescontoBloqueio;
	}

	/**
	 * Get: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @return solEmissaoComprovantePagtoClientePagServiceImpl
	 */
	public ISolEmissaoComprovantePagtoClientePagService getSolEmissaoComprovantePagtoClientePagServiceImpl() {
		return solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	/**
	 * Set: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @param solEmissaoComprovantePagtoClientePagServiceImpl the sol emissao comprovante pagto cliente pag service impl
	 */
	public void setSolEmissaoComprovantePagtoClientePagServiceImpl(
			ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl) {
		this.solEmissaoComprovantePagtoClientePagServiceImpl = solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	public ConsultarAgenciaOpeTarifaPadraoEntradaDTO getConsultarAgenciaOpeTarifaPadraoEntradaDTO() {
		return consultarAgenciaOpeTarifaPadraoEntradaDTO;
	}

	public void setConsultarAgenciaOpeTarifaPadraoEntradaDTO(
			ConsultarAgenciaOpeTarifaPadraoEntradaDTO consultarAgenciaOpeTarifaPadraoEntradaDTO) {
		this.consultarAgenciaOpeTarifaPadraoEntradaDTO = consultarAgenciaOpeTarifaPadraoEntradaDTO;
	}
}