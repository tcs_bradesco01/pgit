/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.liberarpagtosconsolidadospend
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.liberarpagtosconsolidadospend;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoPagPendenteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoPagPendenteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.ILiberarPagtosConsolidadosPendService;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarListaPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarListaPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarPagtoPendConsolidadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.ConsultarPagtoPendConsolidadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPagtoPendConParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosconsolidadospend.bean.LiberarPendenciaPagtoConParcialSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;

/**
 * Nome: LiberarPagtosConsolidadosPendBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class LiberarPagtosConsolidadosPendBean extends PagamentosBean {

	/** Atributo liberarPagtosConsolidadosPendImpl. */
	private ILiberarPagtosConsolidadosPendService liberarPagtosConsolidadosPendImpl;

	/** Atributo listaConsultarMotivoSituacaoPagamentoFiltro. */
	private List<SelectItem> listaConsultarMotivoSituacaoPagamentoFiltro = new ArrayList<SelectItem>();

	/** Atributo listaGridLiberarPagtosConsolidadosPend. */
	private List<ConsultarPagtoPendConsolidadoSaidaDTO> listaGridLiberarPagtosConsolidadosPend;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo consultasService. */
	private IConsultasService consultasService;

	/** Atributo listaGridListaPagamentos. */
	private List<ConsultarListaPagamentoSaidaDTO> listaGridListaPagamentos;

	/** Atributo opcaoChecarTodosLiberacao. */
	private boolean opcaoChecarTodosLiberacao;

	/** Atributo listaControleLiberacao. */
	private List<SelectItem> listaControleLiberacao = new ArrayList<SelectItem>();

	/** Atributo panelBotoesLiberacao. */
	private boolean panelBotoesLiberacao;

	/** Atributo itemSelecionadoListaPagamentos. */
	private Integer itemSelecionadoListaPagamentos;

	/** Atributo listaGridSelecionados. */
	private List<ConsultarListaPagamentoSaidaDTO> listaGridSelecionados;

	// Cliente
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;

	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;

	/** Atributo dsEmpresa. */
	private String dsEmpresa;

	/** Atributo nroContrato. */
	private String nroContrato;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo descricaoTipoContaDebito. */
	private String descricaoTipoContaDebito;

	/** Atributo descricaoBancoContaDebito. */
	private String descricaoBancoContaDebito;

	/** Atributo descricaoAgenciaContaDebitoBancaria. */
	private String descricaoAgenciaContaDebitoBancaria;

	// ContaDebito
	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;

	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;

	/** Atributo contaDebito. */
	private String contaDebito;

	/** Atributo tipoContaDebito. */
	private String tipoContaDebito;

	/** Atributo descricaoEmpresa. */
	private String descricaoEmpresa;

	/** Atributo descricaoNumeroContrato. */
	private String descricaoNumeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo descricaoSituacaoContrato. */
	private String descricaoSituacaoContrato;

	// Pagamentos
	/** Atributo tipoServico. */
	private String tipoServico;

	/** Atributo modalidade. */
	private String modalidade;

	/** Atributo qtdePagamentos. */
	private String qtdePagamentos;

	/** Atributo qtdePagamentosLiberados. */
	private Long qtdePagamentosLiberados;

	/** Atributo vlTotalPagamentos. */
	private BigDecimal vlTotalPagamentos;

	/** Atributo vlTotalPagamentosLiberados. */
	private BigDecimal vlTotalPagamentosLiberados;

	/** Atributo numeroInscricaoFavorecido. */
	private String numeroInscricaoFavorecido;

	/** Atributo tipoFavorecido. */
	private String tipoFavorecido;

	/** Atributo descTipoFavorecido. */
	private String descTipoFavorecido;

	/** Atributo dsFavorecido. */
	private String dsFavorecido;

	/** Atributo cdFavorecido. */
	private String cdFavorecido;

	/** Atributo dsBancoFavorecido. */
	private String dsBancoFavorecido;

	/** Atributo dsAgenciaFavorecido. */
	private String dsAgenciaFavorecido;

	/** Atributo dsContaFavorecido. */
	private String dsContaFavorecido;

	/** Atributo dsTipoContaFavorecido. */
	private String dsTipoContaFavorecido;

	/** Atributo numeroInscricaoBeneficiario. */
	private String numeroInscricaoBeneficiario;

	/** Atributo tipoBeneficiario. */
	private String tipoBeneficiario;

	/** Atributo nomeBeneficiario. */
	private String nomeBeneficiario;

	/** Atributo dsTipoBeneficiario. */
	private String dsTipoBeneficiario;

	/** Atributo dtNascimentoBeneficiario. */
	private String dtNascimentoBeneficiario;

	/** Atributo dsTipoServico. */
	private String dsTipoServico;

	/** Atributo dsIndicadorModalidade. */
	private String dsIndicadorModalidade;

	/** Atributo nrSequenciaArquivoRemessa. */
	private String nrSequenciaArquivoRemessa;

	/** Atributo nrLoteArquivoRemessa. */
	private String nrLoteArquivoRemessa;

	/** Atributo numeroPagamento. */
	private String numeroPagamento;

	/** Atributo cdListaDebito. */
	private String cdListaDebito;

	/** Atributo dtAgendamento. */
	private String dtAgendamento;

	/** Atributo dtPagamento. */
	private String dtPagamento;

	/** Atributo dtPagamentoDet. */
	private String dtPagamentoDet;
	
	/** Atributo dtDevolucaoEstorno. */
	private String dtDevolucaoEstorno;

	/** Atributo dtVencimento. */
	private String dtVencimento;

	/** Atributo cdIndicadorEconomicoMoeda. */
	private String cdIndicadorEconomicoMoeda;

	/** Atributo qtMoeda. */
	private BigDecimal qtMoeda;

	/** Atributo vlrAgendamento. */
	private BigDecimal vlrAgendamento;

	/** Atributo vlrEfetivacao. */
	private BigDecimal vlrEfetivacao;

	/** Atributo cdSituacaoOperacaoPagamento. */
	private String cdSituacaoOperacaoPagamento;

	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;

	/** Atributo dsPagamento. */
	private String dsPagamento;

	/** Atributo nrDocumento. */
	private String nrDocumento;

	/** Atributo tipoDocumento. */
	private String tipoDocumento;

	/** Atributo cdSerieDocumento. */
	private String cdSerieDocumento;

	/** Atributo vlDocumento. */
	private BigDecimal vlDocumento;

	/** Atributo dtEmissaoDocumento. */
	private String dtEmissaoDocumento;

	/** Atributo dsUsoEmpresa. */
	private String dsUsoEmpresa;

	/** Atributo dsMensagemPrimeiraLinha. */
	private String dsMensagemPrimeiraLinha;

	/** Atributo dsMensagemSegundaLinha. */
	private String dsMensagemSegundaLinha;

	/** Atributo cdBancoCredito. */
	private String cdBancoCredito;

	/** Atributo agenciaDigitoCredito. */
	private String agenciaDigitoCredito;

	/** Atributo contaDigitoCredito. */
	private String contaDigitoCredito;

	/** Atributo tipoContaCredito. */
	private String tipoContaCredito;

	/** Atributo cdBancoDestino. */
	private String cdBancoDestino;

	/** Atributo agenciaDigitoDestino. */
	private String agenciaDigitoDestino;

	/** Atributo contaDigitoDestino. */
	private String contaDigitoDestino;

	/** Atributo tipoContaDestino. */
	private String tipoContaDestino;

	/** Atributo vlDesconto. */
	private BigDecimal vlDesconto;

	/** Atributo vlAbatimento. */
	private BigDecimal vlAbatimento;

	/** Atributo vlMulta. */
	private BigDecimal vlMulta;

	/** Atributo vlMora. */
	private BigDecimal vlMora;

	/** Atributo vlDeducao. */
	private BigDecimal vlDeducao;

	/** Atributo vlAcrescimo. */
	private BigDecimal vlAcrescimo;

	/** Atributo vlImpostoRenda. */
	private BigDecimal vlImpostoRenda;

	/** Atributo vlIss. */
	private BigDecimal vlIss;

	/** Atributo vlIof. */
	private BigDecimal vlIof;

	/** Atributo vlInss. */
	private BigDecimal vlInss;

	/** Atributo camaraCentralizadora. */
	private String camaraCentralizadora;

	/** Atributo finalidadeDoc. */
	private String finalidadeDoc;

	/** Atributo identificacaoTransferenciaDoc. */
	private String identificacaoTransferenciaDoc;

	/** Atributo identificacaoTitularidade. */
	private String identificacaoTitularidade;

	/** Atributo finalidadeTed. */
	private String finalidadeTed;

	/** Atributo identificacaoTransferenciaTed. */
	private String identificacaoTransferenciaTed;

	/** Atributo numeroOp. */
	private String numeroOp;

	/** Atributo dataExpiracaoCredito. */
	private String dataExpiracaoCredito;

	/** Atributo instrucaoPagamento. */
	private String instrucaoPagamento;

	/** Atributo numeroCheque. */
	private String numeroCheque;

	/** Atributo serieCheque. */
	private String serieCheque;

	/** Atributo indicadorAssociadoUnicaAgencia. */
	private String indicadorAssociadoUnicaAgencia;

	/** Atributo identificadorTipoRetirada. */
	private String identificadorTipoRetirada;

	/** Atributo identificadorRetirada. */
	private String identificadorRetirada;

	/** Atributo dataRetirada. */
	private String dataRetirada;

	/** Atributo vlImpostoMovFinanceira. */
	private BigDecimal vlImpostoMovFinanceira;

	/** Atributo codigoBarras. */
	private String codigoBarras;

	/** Atributo nossoNumero. */
	private String nossoNumero;

	/** Atributo dsOrigemPagamento. */
	private String dsOrigemPagamento;

	/** Atributo dddContribuinte. */
	private String dddContribuinte;

	/** Atributo telefoneContribuinte. */
	private String telefoneContribuinte;

	/** Atributo inscricaoEstadualContribuinte. */
	private String inscricaoEstadualContribuinte;

	/** Atributo agenteArrecadador. */
	private String agenteArrecadador;

	/** Atributo codigoReceita. */
	private String codigoReceita;

	/** Atributo numeroReferencia. */
	private String numeroReferencia;

	/** Atributo numeroCotaParcela. */
	private String numeroCotaParcela;

	/** Atributo percentualReceita. */
	private BigDecimal percentualReceita;

	/** Atributo periodoApuracao. */
	private String periodoApuracao;

	/** Atributo anoExercicio. */
	private String anoExercicio;

	/** Atributo dataReferencia. */
	private String dataReferencia;

	/** Atributo vlReceita. */
	private BigDecimal vlReceita;

	/** Atributo vlPrincipal. */
	private BigDecimal vlPrincipal;

	/** Atributo vlAtualizacaoMonetaria. */
	private BigDecimal vlAtualizacaoMonetaria;

	/** Atributo vlTotal. */
	private BigDecimal vlTotal;

	/** Atributo codigoCnae. */
	private String codigoCnae;

	/** Atributo placaVeiculo. */
	private String placaVeiculo;

	/** Atributo numeroParcelamento. */
	private String numeroParcelamento;

	/** Atributo codigoOrgaoFavorecido. */
	private String codigoOrgaoFavorecido;

	/** Atributo nomeOrgaoFavorecido. */
	private String nomeOrgaoFavorecido;

	/** Atributo codigoUfFavorecida. */
	private String codigoUfFavorecida;

	/** Atributo descricaoUfFavorecida. */
	private String descricaoUfFavorecida;

	/** Atributo vlAcrescimoFinanceiro. */
	private BigDecimal vlAcrescimoFinanceiro;

	/** Atributo vlHonorarioAdvogado. */
	private BigDecimal vlHonorarioAdvogado;

	/** Atributo observacaoTributo. */
	private String observacaoTributo;

	/** Atributo codigoInss. */
	private String codigoInss;

	/** Atributo dataCompetencia. */
	private String dataCompetencia;

	/** Atributo vlOutrasEntidades. */
	private BigDecimal vlOutrasEntidades;

	/** Atributo codigoTributo. */
	private String codigoTributo;

	/** Atributo codigoRenavam. */
	private String codigoRenavam;

	/** Atributo municipioTributo. */
	private String municipioTributo;

	/** Atributo ufTributo. */
	private String ufTributo;

	/** Atributo numeroNsu. */
	private String numeroNsu;

	/** Atributo opcaoTributo. */
	private String opcaoTributo;

	/** Atributo opcaoRetiradaTributo. */
	private String opcaoRetiradaTributo;

	/** Atributo clienteDepositoIdentificado. */
	private String clienteDepositoIdentificado;

	/** Atributo tipoDespositoIdentificado. */
	private String tipoDespositoIdentificado;

	/** Atributo produtoDepositoIdentificado. */
	private String produtoDepositoIdentificado;

	/** Atributo sequenciaProdutoDepositoIdentificado. */
	private String sequenciaProdutoDepositoIdentificado;

	/** Atributo bancoCartaoDepositoIdentificado. */
	private String bancoCartaoDepositoIdentificado;

	/** Atributo cartaoDepositoIdentificado. */
	private String cartaoDepositoIdentificado;

	/** Atributo bimCartaoDepositoIdentificado. */
	private String bimCartaoDepositoIdentificado;

	/** Atributo viaCartaoDepositoIdentificado. */
	private String viaCartaoDepositoIdentificado;

	/** Atributo codigoDepositante. */
	private String codigoDepositante;

	/** Atributo nomeDepositante. */
	private String nomeDepositante;

	/** Atributo codigoPagador. */
	private String codigoPagador;

	/** Atributo codigoOrdemCredito. */
	private String codigoOrdemCredito;

	/** Atributo cdIndentificadorJudicial. */
	private String cdIndentificadorJudicial;

	/** Atributo dtLimiteDescontoPagamento. */
	private String dtLimiteDescontoPagamento;
	
	/** Atributo dtLimitePagamento. */
	private String dtLimitePagamento;
	
	/** Atributo dsRastreado. */
	private String dsRastreado;
	
	/** Atributo carteira. */
	private String carteira;

	/** Atributo cdSituacaoTranferenciaAutomatica. */
	private String cdSituacaoTranferenciaAutomatica;

	/** Atributo nrRemessa. */
	private Long nrRemessa;
	
	/** Atributo cdOperacaoDcom. */
	private Long cdOperacaoDcom;
	
	/** Atributo dsSituacaoDcom. */
	private String dsSituacaoDcom;
	
	/** Atributo numControleInternoLote. */
	private Long numControleInternoLote;
	
	// Flag
	/** Atributo exibeDcom. */
	private Boolean exibeDcom;

	// TrilhaAuditoria
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	// relatorio
	/** Atributo logoPath. */
	private String logoPath;

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;

	/** Atributo exibeBeneficiario. */
	private boolean exibeBeneficiario;
	
	/** Atributo cdBancoOriginal. */
	private Integer cdBancoOriginal;

	/** Atributo dsBancoOriginal. */
	private String dsBancoOriginal;

	/** Atributo cdAgenciaBancariaOriginal. */
	private Integer cdAgenciaBancariaOriginal;

	/** Atributo cdDigitoAgenciaOriginal. */
	private String cdDigitoAgenciaOriginal;

	/** Atributo dsAgenciaOriginal. */
	private String dsAgenciaOriginal;

	/** Atributo cdContaBancariaOriginal. */
	private Long cdContaBancariaOriginal;

	/** Atributo cdDigitoContaOriginal. */
	private String cdDigitoContaOriginal;

	/** Atributo dsTipoContaOriginal. */
	private String dsTipoContaOriginal;

	/**
	 * Pesquisar.
	 */
	public void pesquisar() {
		setOpcaoChecarTodosLiberacao(false);
		setPanelBotoesLiberacao(false);
		for (int i = 0; i < getListaGridListaPagamentos().size(); i++) {
			getListaGridListaPagamentos().get(i).setCheck(false);
		}
	}

	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes() {
		setPanelBotoesLiberacao(false);
		for (ConsultarListaPagamentoSaidaDTO o : getListaGridListaPagamentos()) {
			if (o.isCheck()) {
				setPanelBotoesLiberacao(true);
				return;
			}
		}
	}

	/**
	 * Confirmar parcial pendencia.
	 *
	 * @return the string
	 */
	public String confirmarParcialPendencia() {
		try {
			List<LiberarPendenciaPagtoConParcialEntradaDTO> listaOcorrenciasLiberarParcialPendencia = new ArrayList<LiberarPendenciaPagtoConParcialEntradaDTO>();
			ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionado = getListaGridLiberarPagtosConsolidadosPend()
					.get(getItemSelecionadoLista());

			for (int i = 0; i < (getListaGridSelecionados().size()); i++) {
				LiberarPendenciaPagtoConParcialEntradaDTO ocorrencia = new LiberarPendenciaPagtoConParcialEntradaDTO();
				ocorrencia
						.setCdPessoaJuridicaContrato(getListaGridSelecionados()
								.get(i).getCdPessoaJuridicaContrato());
				ocorrencia.setCdTipoContratoNegocio(getListaGridSelecionados()
						.get(i).getCdTipoContratoNegocio());
				ocorrencia
						.setNrSequenciaContratoNegocio(getListaGridSelecionados()
								.get(i).getNrSequenciaContratoNegocio());
				ocorrencia.setCdTipoCanal(getListaGridSelecionados().get(i)
						.getCdTipoCanal());
				ocorrencia.setCdControlePagamento(getListaGridSelecionados()
						.get(i).getNrPagamento());
				ocorrencia.setCdModalidade(getListaGridSelecionados().get(i)
						.getCdTipoTela());
				ocorrencia.setCdMotivoPendencia(registroSelecionado
						.getCdMotivoOperacaoPagamento());

				listaOcorrenciasLiberarParcialPendencia.add(ocorrencia);
			}

			LiberarPendenciaPagtoConParcialSaidaDTO saidaDTO = getLiberarPagtosConsolidadosPendImpl()
					.liberarPendenciaPagtoConParcial(
							listaOcorrenciasLiberarParcialPendencia);
			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conLiberarPagtosConsolidadosPend",
					"#{liberarPagtosConsolidadosPendBean.consultar}", false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "";
	}

	/**
	 * Confirmar parcial processamento.
	 *
	 * @return the string
	 */
	public String confirmarParcialProcessamento() {
		try {
			List<LiberarPagtoPendConParcialEntradaDTO> listaOcorrenciasLiberarParcialProcessamento = new ArrayList<LiberarPagtoPendConParcialEntradaDTO>();

			for (int i = 0; i < (getListaGridSelecionados().size()); i++) {
				LiberarPagtoPendConParcialEntradaDTO ocorrencia = new LiberarPagtoPendConParcialEntradaDTO();
				ocorrencia.setCdPessoaJuridica(getListaGridSelecionados()
						.get(i).getCdPessoaJuridicaContrato());
				ocorrencia.setCdTipoContrato(getListaGridSelecionados().get(i)
						.getCdTipoContratoNegocio());
				ocorrencia.setNrSequencialContrato(getListaGridSelecionados()
						.get(i).getNrSequenciaContratoNegocio());
				ocorrencia.setCdTipoCanal(getListaGridSelecionados().get(i)
						.getCdTipoCanal());
				ocorrencia.setCdControlePagamento(getListaGridSelecionados()
						.get(i).getNrPagamento());
				ocorrencia.setCdModalidade(getListaGridSelecionados().get(i)
						.getCdTipoTela());

				listaOcorrenciasLiberarParcialProcessamento.add(ocorrencia);
			}

			LiberarPagtoPendConParcialSaidaDTO saidaDTO = getLiberarPagtosConsolidadosPendImpl()
					.liberarPagtoPendConParcial(
							listaOcorrenciasLiberarParcialProcessamento);
			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conLiberarPagtosConsolidadosPend",
					"#{liberarPagtosConsolidadosPendBean.consultar}", false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
		return "";
	}

	/**
	 * Confirmar integral pendencia.
	 *
	 * @return the string
	 */
	public String confirmarIntegralPendencia() {
		try {
			LiberarPendenciaPagtoConIntegralEntradaDTO entradaDTO = new LiberarPendenciaPagtoConIntegralEntradaDTO();
			ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionado = getListaGridLiberarPagtosConsolidadosPend()
					.get(getItemSelecionadoLista());

			/*
			 * entradaDTO.setCdPessoaJuridica(getListaGridListaPagamentos().get(0
			 * ).getCdPessoaJuridicaContrato());
			 * entradaDTO.setCdTipoContrato(getListaGridListaPagamentos
			 * ().get(0).getCdTipoContratoNegocio());
			 * entradaDTO.setNrSequenciaContrato
			 * (getListaGridListaPagamentos().get
			 * (0).getNrSequenciaContratoNegocio());
			 * entradaDTO.setCdCnpjCliente(
			 * getListaGridListaPagamentos().get(0).getCdCorpoCpfCnpjFavorecido
			 * ());
			 * entradaDTO.setCdCnpjFilial(getListaGridListaPagamentos().get(
			 * 0).getCdFilialCpfCnpjFavorecido());
			 * entradaDTO.setCdCnpjDigito(getListaGridListaPagamentos
			 * ().get(0).getCdControleCpfCnpjFavorecido());
			 */
			entradaDTO.setCdPessoaJuridica(registroSelecionado
					.getCdPessoaJuridica());
			entradaDTO.setCdTipoContrato(registroSelecionado
					.getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(registroSelecionado
					.getNrContrato());
			entradaDTO.setCdCnpjCliente(registroSelecionado.getNrCnpjCpf());
			entradaDTO
					.setCdCnpjFilial(registroSelecionado.getNrFilialCnpjCpf());
			entradaDTO
					.setCdCnpjDigito(registroSelecionado.getCdDigitoCnpjCpf());
			entradaDTO.setNrRemessa(registroSelecionado.getNrRemessa());
			entradaDTO.setDtCredito(registroSelecionado.getDtPagamento());
			entradaDTO.setCdPessoaJuridicaDebito(registroSelecionado
					.getCdPessoaJuridicaDebito());
			entradaDTO.setCdTipoContratoDebito(registroSelecionado
					.getCdTipoContratoDebito());
			entradaDTO.setNrSequenciaContratoDebito(registroSelecionado
					.getNrSequenciaContratoDebito());
			entradaDTO.setCdProdutoServicoOperacao(registroSelecionado
					.getCdProdutoServicoOperacao());
			entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
					.getCdProdutoServicoRelacionado());
			entradaDTO.setCdSituacaoOperacaoPagamento(registroSelecionado
					.getCdSituacaoOperacaoPagamento());
			entradaDTO.setCdMotivoPendencia(registroSelecionado
					.getCdMotivoOperacaoPagamento());

			LiberarPendenciaPagtoConIntegralSaidaDTO saidaDTO = getLiberarPagtosConsolidadosPendImpl()
					.liberarPendenciaPagtoConIntegral(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conLiberarPagtosConsolidadosPend",
					"#{liberarPagtosConsolidadosPendBean.consultar}", false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
		return "";
	}

	/**
	 * Confirmar integral processamento.
	 *
	 * @return the string
	 */
	public String confirmarIntegralProcessamento() {
		try {
			LiberarPagtoPendConIntegralEntradaDTO entradaDTO = new LiberarPagtoPendConIntegralEntradaDTO();
			ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionado = getListaGridLiberarPagtosConsolidadosPend()
					.get(getItemSelecionadoLista());

			/*
			 * entradaDTO.setCdPessoaJuridica(getListaGridListaPagamentos().get(0
			 * ).getCdPessoaJuridicaContrato());
			 * entradaDTO.setCdTipoContrato(getListaGridListaPagamentos
			 * ().get(0).getCdTipoContratoNegocio());
			 * entradaDTO.setNrSequenciaContrato
			 * (getListaGridListaPagamentos().get
			 * (0).getNrSequenciaContratoNegocio());
			 * entradaDTO.setCdCnpjCliente(
			 * getListaGridListaPagamentos().get(0).getCdCorpoCpfCnpjFavorecido
			 * ());
			 * entradaDTO.setCdCnpjFilial(getListaGridListaPagamentos().get(
			 * 0).getCdFilialCpfCnpjFavorecido());
			 * entradaDTO.setCdCnpjDigito(getListaGridListaPagamentos
			 * ().get(0).getCdControleCpfCnpjFavorecido());
			 */
			entradaDTO.setCdPessoaJuridica(registroSelecionado
					.getCdPessoaJuridica());
			entradaDTO.setCdTipoContrato(registroSelecionado
					.getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(registroSelecionado
					.getNrContrato());
			entradaDTO.setCdCnpjCliente(registroSelecionado.getNrCnpjCpf());
			entradaDTO
					.setCdCnpjFilial(registroSelecionado.getNrFilialCnpjCpf());
			entradaDTO
					.setCdCnpjDigito(registroSelecionado.getCdDigitoCnpjCpf());
			entradaDTO.setNrRemessa(registroSelecionado.getNrRemessa());
			entradaDTO.setDtCredito(registroSelecionado.getDtPagamento());
			entradaDTO.setCdPessoaJuridicaDebito(registroSelecionado
					.getCdPessoaJuridicaDebito());
			entradaDTO.setCdTipoContratoDebito(registroSelecionado
					.getCdTipoContratoDebito());
			entradaDTO.setNrSequenciaContratoDebito(registroSelecionado
					.getNrSequenciaContratoDebito());
			entradaDTO.setCdProdutoServicoOperacao(registroSelecionado
					.getCdProdutoServicoOperacao());
			entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
					.getCdProdutoServicoRelacionado());
			entradaDTO.setCdSituacaoOperacaoPagamento(registroSelecionado
					.getCdSituacaoOperacaoPagamento());
			entradaDTO.setCdMotivoPendencia(registroSelecionado
					.getCdMotivoOperacaoPagamento());

			LiberarPagtoPendConIntegralSaidaDTO saidaDTO = getLiberarPagtosConsolidadosPendImpl()
					.liberarPagtoPendConIntegral(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"conLiberarPagtosConsolidadosPend",
					"#{liberarPagtosConsolidadosPendBean.consultar}", false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
		return "";
	}

	/**
	 * Limpar pagamentos.
	 */
	private void limparPagamentos() {
		setQtdePagamentosLiberados(null);
	}

	/**
	 * Liberar selecionados.
	 *
	 * @return the string
	 */
	public String liberarSelecionados() {

		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());

		limparPagamentos();
		setVlTotalPagamentosLiberados(BigDecimal.ZERO);
		setListaGridSelecionados(new ArrayList<ConsultarListaPagamentoSaidaDTO>());
		for (ConsultarListaPagamentoSaidaDTO saidaDTO : getListaGridListaPagamentos()) {
			if (saidaDTO.isCheck()) {
				getListaGridSelecionados().add(saidaDTO);
				setVlTotalPagamentosLiberados(new BigDecimal(
						getVlTotalPagamentosLiberados().doubleValue()
								+ saidaDTO.getVlrPagamento().doubleValue()));
			}
		}
		setQtdePagamentosLiberados(new Long(getListaGridSelecionados().size()));

		return "LIBERAR_SELECIONADOS";
	}

	/**
	 * Pesquisar parcial.
	 *
	 * @param evt the evt
	 */
	public void pesquisarParcial(ActionEvent evt) {
		return;
	}

	/**
	 * Pesquisar consultar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarConsultar(ActionEvent evt) {
		consultar();
		return "";
	}

	/**
	 * Pesquisar lista pagamentos.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarListaPagamentos(ActionEvent evt) {
		try {
			consultarListaPagamentoDataScroller();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridListaPagamentos(null);
			setItemSelecionadoListaPagamentos(null);
			return "";
		}
		return "";
	}

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {
		setItemSelecionadoLista(null);

		return "VOLTAR";
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoListaPagamentos(null);

		return "VOLTAR";
	}

	/**
	 * Imprimir integral processamento.
	 */
	public void imprimirIntegralProcessamento() {
		try {

			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
					.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			carregarPagamentos();
			carregaCabecalhoIntermediario();

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par
					.put("titulo",
							"Liberar Pagamentos Consolidados Pendentes - Liberar Processamento");

			par.put("cpfCnpj", getNrCnpjCpf());
			par.put("nomeRazaoSocial", getDsRazaoSocial());
			par.put("empresaConglomerado", getDsEmpresa());
			par.put("nrContrato", getNroContrato());
			par.put("dsContratacao", getDsContrato());
			par.put("situacao", getCdSituacaoContrato());

			par.put("dsBancoDebito", getDsBancoDebito());
			par.put("dsAgenciaDebito", getDsAgenciaDebito());
			par.put("contaDebito", getContaDebito());
			par.put("tipoContaDebito", getTipoContaDebito());

			par.put("qtdePagamentosLiberados", getQtdePagamentos());
			par.put("vlTotalPagamentosLiberados", getVlTotalPagamentos());

			par.put("logoBradesco", getLogoPath());
			try {

				// M�todo que gera o relat�rio PDF.
				PgitUtil
						.geraRelatorioPdf(
								"/relatorios/agendamentoEfetivacaoEstorno/liberarPagtosConPendentes/libPagtoConIntLibProc",
								"imprimirLibPagtosConPenIntLibProc",
								getListaGridListaPagamentos(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Imprimir integral pendencia.
	 */
	public void imprimirIntegralPendencia() {
		try {

			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
					.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			carregarPagamentos();
			carregaCabecalhoIntermediario();

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par
					.put("titulo",
							"Liberar Pagamentos Consolidados Pendentes - Liberar Pend�ncia");

			par.put("cpfCnpj", getNrCnpjCpf());
			par.put("nomeRazaoSocial", getDsRazaoSocial());
			par.put("empresaConglomerado", getDsEmpresa());
			par.put("nrContrato", getNroContrato());
			par.put("dsContratacao", getDsContrato());
			par.put("situacao", getCdSituacaoContrato());

			par.put("dsBancoDebito", getDsBancoDebito());
			par.put("dsAgenciaDebito", getDsAgenciaDebito());
			par.put("contaDebito", getContaDebito());
			par.put("tipoContaDebito", getTipoContaDebito());

			par.put("qtdePagamentosLiberados", getQtdePagamentos());
			par.put("vlTotalPagamentosLiberados", getVlTotalPagamentos());

			par.put("logoBradesco", getLogoPath());
			try {

				// M�todo que gera o relat�rio PDF.
				PgitUtil
						.geraRelatorioPdf(
								"/relatorios/agendamentoEfetivacaoEstorno/liberarPagtosConPendentes/libPagtoConIntLibPen",
								"imprimirLibPagtosConPenIntLibPen",
								getListaGridListaPagamentos(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Imprimir parcial pendencia.
	 */
	public void imprimirParcialPendencia() {
		try {

			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
					.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			carregarPagamentos();
			carregaCabecalhoIntermediario();

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par
					.put("titulo",
							"Liberar Pagamentos Consolidados Pendentes - Liberar Pend�ncia");

			par.put("cpfCnpj", getNrCnpjCpf());
			par.put("nomeRazaoSocial", getDsRazaoSocial());
			par.put("empresaConglomerado", getDsEmpresa());
			par.put("nrContrato", getNroContrato());
			par.put("dsContratacao", getDsContrato());
			par.put("situacao", getCdSituacaoContrato());

			par.put("dsBancoDebito", getDsBancoDebito());
			par.put("dsAgenciaDebito", getDsAgenciaDebito());
			par.put("contaDebito", getContaDebito());
			par.put("tipoContaDebito", getTipoContaDebito());

			par.put("qtdePagamentosLiberados", getQtdePagamentosLiberados());
			par.put("vlTotalPagamentosLiberados",
					getVlTotalPagamentosLiberados());
			par.put("qtdePagamentos", getQtdePagamentos());
			par.put("vlTotalPagamentos", getVlTotalPagamentos());

			par.put("logoBradesco", getLogoPath());
			try {

				// M�todo que gera o relat�rio PDF.
				PgitUtil
						.geraRelatorioPdf(
								"/relatorios/agendamentoEfetivacaoEstorno/liberarPagtosConPendentes/confLibPagtoConParcLibPen",
								"imprimirConfLibPagtosConPenParcLibPen",
								getListaGridSelecionados(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Imprimir parcial processamento.
	 */
	public void imprimirParcialProcessamento() {
		try {

			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
					.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			carregarPagamentos();
			carregaCabecalhoIntermediario();

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par
					.put("titulo",
							"Liberar Pagamentos Consolidados Pendentes - Liberar Processamento");

			par.put("cpfCnpj", getNrCnpjCpf());
			par.put("nomeRazaoSocial", getDsRazaoSocial());
			par.put("empresaConglomerado", getDsEmpresa());
			par.put("nrContrato", getNroContrato());
			par.put("dsContratacao", getDsContrato());
			par.put("situacao", getCdSituacaoContrato());

			par.put("dsBancoDebito", getDsBancoDebito());
			par.put("dsAgenciaDebito", getDsAgenciaDebito());
			par.put("contaDebito", getContaDebito());
			par.put("tipoContaDebito", getTipoContaDebito());

			par.put("qtdePagamentosLiberados", getQtdePagamentosLiberados());
			par.put("vlTotalPagamentosLiberados",
					getVlTotalPagamentosLiberados());
			par.put("qtdePagamentos", getQtdePagamentos());
			par.put("vlTotalPagamentos", getVlTotalPagamentos());

			par.put("logoBradesco", getLogoPath());
			try {

				// M�todo que gera o relat�rio PDF.
				PgitUtil
						.geraRelatorioPdf(
								"/relatorios/agendamentoEfetivacaoEstorno/liberarPagtosConPendentes/confLibPagtoConParcLibProc",
								"imprimirConfLibPagtosConPenParcLibProc",
								getListaGridSelecionados(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		// Tela de Filtro
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
				"conLiberarPagtosConsolidadosPend");

		listarTipoServicoPagto();
		listarConsultarMotivoPagamento();
		limparTelaPrincipal();
		setHabilitaArgumentosPesquisa(false);
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setClienteContratoSelecionado(false);
		setDisableArgumentosConsulta(false);
		getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);

		// Radio de Agendados � Pagos/N�o Pagos, ficara sempre protegido e com a
		// op��o �Agendados� selecionada;
		setAgendadosPagosNaoPagos("1");
	}

	// M�todo sobreposto para n�o limpar os agendados - pagos/nao pagos
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#limparCampos()
	 */
	public void limparCampos() {
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.limparParticipanteContrato();
		setChkContaDebito(false);
		limparContaDebito();
		limparRemessaFavorecidoBeneficiarioLinha();
		if (getFiltroAgendamentoEfetivacaoEstornoBean() != null
				&& getFiltroAgendamentoEfetivacaoEstornoBean()
						.getPaginaRetorno() != null
				&& getFiltroAgendamentoEfetivacaoEstornoBean()
						.getPaginaRetorno().equals("conSolRecPagVenNaoPagos")) {
			limparMotivo();
		} else {
			limparSituacaoMotivo();
		}
		limparTipoServicoModalidade();

		setDataInicialPagamentoFiltro(new Date());
		setDataFinalPagamentoFiltro(new Date());
		setChkParticipanteContrato(false);
		setChkRemessa(false);
		setChkServicoModalidade(false);
		setChkSituacaoMotivo(false);
		setRadioPesquisaFiltroPrincipal("");
		setRadioFavorecidoFiltro("");
		setRadioBeneficiarioFiltro("");

		setClassificacaoFiltro(null);

		setChkNumeroListaDebito(false);
		setNumListaDebitoDeFiltro("");
		setNumListaDebitoAteFiltro("");

		setCboSituacaoListaDebito(null);
		setChkSituacaoListaDebito(false);
		setValorPagamentoAteFiltro(null);
		setValorPagamentoDeFiltro(null);
	}

	/**
	 * Limpar tela principal.
	 *
	 * @return the string
	 */
	public String limparTelaPrincipal() {
		limparArgumentosPesquisa();
		limparFiltroPrincipal();
		setListaGridLiberarPagtosConsolidadosPend(null);
		setItemSelecionadoLista(null);
		return "";
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
		limparArgumentosPesquisa();
		setListaGridLiberarPagtosConsolidadosPend(null);
		setItemSelecionadoLista(null);

		return "";

	}

	/**
	 * Limpar grid consultar.
	 *
	 * @return the string
	 */
	public String limparGridConsultar() {
		setListaGridLiberarPagtosConsolidadosPend(null);
		setItemSelecionadoLista(null);
		setListaControleRadio(null);
		setDisableArgumentosConsulta(false);

		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());
		return "";
	}

	/**
	 * Limpar tela estado inicial.
	 *
	 * @return the string
	 */
	public String limparTelaEstadoInicial() {
		limparTelaPrincipal();
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setTipoFiltroSelecionado("");
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Listar consultar motivo pagamento.
	 */
	public void listarConsultarMotivoPagamento() {
		try {
			setListaConsultarMotivoSituacaoPagamentoFiltro(new ArrayList<SelectItem>());
			setCboMotivoSituacaoFiltro(null);

			ConsultarMotivoSituacaoPagPendenteEntradaDTO entrada = new ConsultarMotivoSituacaoPagPendenteEntradaDTO();
			// SITUA��O DO PAGAMENTO SER� SEMPRE 2 NESTA TELA
			entrada.setCdSituacaoPagamento(2);

			List<ConsultarMotivoSituacaoPagPendenteSaidaDTO> list = getComboService()
					.consultarMotivoSituacaoPagPendente(entrada);

			for (ConsultarMotivoSituacaoPagPendenteSaidaDTO saida : list) {
				getListaConsultarMotivoSituacaoPagamentoFiltro().add(
						new SelectItem(saida.getCdMotivoSituacaoPagamento(),
								saida.getDsMotivoSituacaoPag()));
			}
		} catch (PdcAdapterFunctionalException p) {
			setListaConsultarMotivoSituacaoPagamentoFiltro(new ArrayList<SelectItem>());
		}
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		limparArgumentosPesquisa();
		setListaGridLiberarPagtosConsolidadosPend(null);
		setItemSelecionadoLista(null);
		return "";
	}

	/**
	 * Limpar argumentos pesquisa.
	 *
	 * @return the string
	 */
	public String limparArgumentosPesquisa() {
		limparRemessa();
		limparContaDebito();
		limparSituacaoMotivo();
		controleServicoModalidade();
		limparCampos();
		setListaGridLiberarPagtosConsolidadosPend(null);
		setItemSelecionadoLista(null);
		return "";
	}

	/**
	 * Consultar.
	 *
	 * @param evt the evt
	 */
	public void consultar(ActionEvent evt) {
		consultar();
	}

	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar() {
		try {
			setDisableArgumentosConsulta(true);
			ConsultarPagtoPendConsolidadoEntradaDTO entrada = new ConsultarPagtoPendConsolidadoEntradaDTO();

			/*
			 * Se selecionado �Filtrar por Cliente� ou �Filtrar por Contrato�
			 * mover �1�. Se selecionado �Filtrar por Conta D�bito� mover �2�;
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("0")
					|| getFiltroAgendamentoEfetivacaoEstornoBean()
							.getTipoFiltroSelecionado().equals("1")) {
				entrada.setCdTipoPesquisa(1);
			} else {
				entrada.setCdTipoPesquisa(2);
			}

			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("0")
					|| getFiltroAgendamentoEfetivacaoEstornoBean()
							.getTipoFiltroSelecionado().equals("1")) {
				/*
				 * >> Se Selecionado Filtrar por Cliente Mover Valores vindos da
				 * lista >> Se Selecionado Filtrar por Contrato Passar valor
				 * retornado da lista(Verificado por email), na especifica��o
				 * definia passar os combo e inputtext, o que poderia gerar
				 * problemas caso o usu�rio altere os valores do combo ap�s ter
				 * feito a pesquisa
				 */
				entrada
						.setCdPessoaJuridica(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdPessoaJuridicaContrato());
				entrada
						.setCdTipoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdTipoContratoNegocio());
				entrada
						.setNrSequencialContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio());
			}

			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("2")) {
				/*
				 * Se selecionado �Filtrar por Conta D�bito� mover zeros para os
				 * cambos abaixo
				 */
				entrada.setCdPessoaJuridica(0L);
				entrada.setCdTipoContrato(0);
				entrada.setNrSequencialContrato(0L);
			}

			/*
			 * Se estiver selecionado "Filtrar por Conta D�bito", passar os
			 * campos deste filtro Se estiver selecionado outro tipo de filtro,
			 * verificar se o "Conta D�bito" dos argumentos de pesquisa esta
			 * marcado Se estiver marcado, passar os valores correspondentes da
			 * tela,caso contr�rio passar zeros
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("2")) {
				entrada.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getBancoContaDebitoFiltro());
				entrada
						.setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getAgenciaContaDebitoBancariaFiltro());
				entrada.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getContaBancariaContaDebitoFiltro());
				entrada
						.setCdDigitoConta(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getDigitoContaDebitoFiltro());
			} else {
				if (isChkContaDebito()) {
					entrada.setCdBanco(getCdBancoContaDebito());
					entrada.setCdAgencia(getCdAgenciaBancariaContaDebito());
					entrada.setCdConta(getCdContaBancariaContaDebito());
					entrada
							.setCdDigitoConta(getCdDigitoContaContaDebito() != null
									&& !getCdDigitoContaContaDebito()
											.equals("") ? getCdDigitoContaContaDebito()
									: "0");
				} else {
					entrada.setCdBanco(0);
					entrada.setCdAgencia(0);
					entrada.setCdConta(0L);
					entrada.setCdDigitoConta("00");
				}
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			entrada.setDtInicialPagamento(sdf
					.format(getDataInicialPagamentoFiltro()));
			entrada.setDtFinalPagamento(sdf
					.format(getDataFinalPagamentoFiltro()));
			entrada.setNrRemessa(getNumeroRemessaFiltro().equals("") ? 0L
					: Long.parseLong(getNumeroRemessaFiltro()));
			entrada.setCdProdutoServicoOperacao(getTipoServicoFiltro());
			entrada.setCdProdutoServicoRelacionado(getModalidadeFiltro());
			entrada.setCdMotivoSituacaoPagamento(getCboMotivoSituacaoFiltro());

			setListaGridLiberarPagtosConsolidadosPend(getLiberarPagtosConsolidadosPendImpl()
					.consultarPagtoPendConsolidado(entrada));

			setItemSelecionadoLista(null);
			this.listaControleRadio = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridLiberarPagtosConsolidadosPend()
					.size(); i++) {
				this.listaControleRadio.add(new SelectItem(i, " "));
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridLiberarPagtosConsolidadosPend(null);
			setItemSelecionadoLista(null);
			setDisableArgumentosConsulta(false);
		}
		return "";
	}

	/**
	 * Consultar lista pagamento.
	 *
	 * @return the string
	 */
	public String consultarListaPagamento() {
		carregaCabecalhoIntermediario();
		carregarPagamentos();
		ConsultarListaPagamentoEntradaDTO entradaDTO = new ConsultarListaPagamentoEntradaDTO();
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionado = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContrato());
		entradaDTO.setCdCorpoCpfCnpj(registroSelecionado.getNrCnpjCpf());
		entradaDTO.setCdFilialCpfCnpj(registroSelecionado.getNrFilialCnpjCpf());
		entradaDTO.setCdControleCpfCnpj(registroSelecionado
				.getCdDigitoCnpjCpf());
		entradaDTO.setNrArquivoRemessa(registroSelecionado.getNrRemessa());
		entradaDTO.setDtCreditoPagamento(registroSelecionado.getDtPagamento());
		entradaDTO.setCdBanco(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgencia(registroSelecionado.getCdAgencia());
		entradaDTO.setCdConta(registroSelecionado.getCdConta());
		entradaDTO.setCdDigitoConta(registroSelecionado.getCdDigitoConta());
		entradaDTO.setCdProdutoServicoOperacao(registroSelecionado
				.getCdProdutoServicoOperacao());
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setCdSituacaoOperacaoPagamento(registroSelecionado
				.getCdSituacaoOperacaoPagamento());
		entradaDTO.setCdPessoaJuridicaDebito(registroSelecionado
				.getCdPessoaJuridicaDebito());
		entradaDTO.setCdTipoContratoDebito(registroSelecionado
				.getCdTipoContratoDebito());
		entradaDTO.setNrSequenciaContratoDebito(registroSelecionado
				.getNrSequenciaContratoDebito());
		entradaDTO.setCdMotivoPendencia(registroSelecionado
				.getCdMotivoOperacaoPagamento());

		List<ConsultarListaPagamentoSaidaDTO> listaGrid = getLiberarPagtosConsolidadosPendImpl()
				.consultarListaPagamento(entradaDTO);

		for (int i = 0; i < listaGrid.size(); i++) {
			ConsultarListaPagamentoSaidaDTO dto = listaGrid.get(i);
			dto.setTipoServico(registroSelecionado.getTipoServicoFormatado());
			dto.setModalidade(registroSelecionado.getModalidadeFormatada());
			listaGrid.set(i, dto);
		}

		setListaGridListaPagamentos(listaGrid);
		setItemSelecionadoListaPagamentos(null);

		for (int i = 0; i < getListaGridListaPagamentos().size(); i++) {
			listaControleLiberacao.add(new SelectItem(i, " "));
		}

		return "";
	}

	/**
	 * Consultar lista pagamento data scroller.
	 *
	 * @return the string
	 */
	public String consultarListaPagamentoDataScroller() {
		ConsultarListaPagamentoEntradaDTO entradaDTO = new ConsultarListaPagamentoEntradaDTO();
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionado = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContrato());
		entradaDTO.setCdCorpoCpfCnpj(registroSelecionado.getNrCnpjCpf());
		entradaDTO.setCdFilialCpfCnpj(registroSelecionado.getNrFilialCnpjCpf());
		entradaDTO.setCdControleCpfCnpj(registroSelecionado
				.getCdDigitoCnpjCpf());
		entradaDTO.setNrArquivoRemessa(registroSelecionado.getNrRemessa());
		entradaDTO.setDtCreditoPagamento(registroSelecionado.getDtPagamento());
		entradaDTO.setCdBanco(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgencia(registroSelecionado.getCdAgencia());
		entradaDTO.setCdConta(registroSelecionado.getCdConta());
		entradaDTO.setCdDigitoConta(registroSelecionado.getCdDigitoConta());
		entradaDTO.setCdProdutoServicoOperacao(registroSelecionado
				.getCdProdutoServicoOperacao());
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setCdSituacaoOperacaoPagamento(registroSelecionado
				.getCdSituacaoOperacaoPagamento());
		entradaDTO.setCdPessoaJuridicaDebito(registroSelecionado
				.getCdPessoaJuridicaDebito());
		entradaDTO.setCdTipoContratoDebito(registroSelecionado
				.getCdTipoContratoDebito());
		entradaDTO.setNrSequenciaContratoDebito(registroSelecionado
				.getNrSequenciaContratoDebito());
		entradaDTO.setCdMotivoPendencia(registroSelecionado
				.getCdMotivoOperacaoPagamento());

		List<ConsultarListaPagamentoSaidaDTO> listaGrid = getLiberarPagtosConsolidadosPendImpl()
				.consultarListaPagamento(entradaDTO);

		for (int i = 0; i < listaGrid.size(); i++) {
			ConsultarListaPagamentoSaidaDTO dto = listaGrid.get(i);
			dto.setTipoServico(registroSelecionado.getTipoServicoFormatado());
			dto.setModalidade(registroSelecionado.getModalidadeFormatada());
			listaGrid.set(i, dto);
		}

		setListaGridListaPagamentos(listaGrid);
		setItemSelecionadoListaPagamentos(null);

		for (int i = 0; i < getListaGridListaPagamentos().size(); i++) {
			listaControleLiberacao.add(new SelectItem(i, " "));
		}

		return "";
	}

	/**
	 * Limpar variaveis carrega cabecalho.
	 */
	private void limparVariaveisCarregaCabecalho() {
		setNrCnpjCpf(null);
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");
		setDsBancoDebito("");
		setDsAgenciaDebito("");
		setContaDebito(null);
		setTipoContaDebito("");
		setDescricaoContrato("");
		setDescricaoNumeroContrato("");
		setDescricaoEmpresa("");
		setDescricaoSituacaoContrato("");
		setDescricaoTipoContaDebito("");
		setDescricaoBancoContaDebito("");
		setDescricaoAgenciaContaDebitoBancaria("");
	}

	/**
	 * Carrega cabecalho intermediario.
	 */
	private void carregaCabecalhoIntermediario() {

		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionado = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());

		limparVariaveisCarregaCabecalho();
		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("0")) { // Filtrar por
															// cliente
			setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getEmpresaGestoraDescCliente());
			setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getNumeroDescCliente());
			setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getDescricaoContratoDescCliente());
			setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSituacaoDescCliente());
			carregaContaBancoAgenciaDebito();
		} else {
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("1")) { // Filtrar por
				// contrato
				setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getEmpresaGestoraDescContrato());
				setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getNumeroDescContrato());
				setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getDescricaoContratoDescContrato());
				setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getSituacaoDescContrato());
				carregaContaBancoAgenciaDebito();
			} else {
				if (getFiltroAgendamentoEfetivacaoEstornoBean()
						.getTipoFiltroSelecionado().equals("2")) { // Filtrar
					// contaDebito
					carregaDadosContrato();
					getFiltroAgendamentoEfetivacaoEstornoBean()
							.carregaDescricaoConta();
					setContaDebito(PgitUtil.formatConta(registroSelecionado
							.getCdConta(), registroSelecionado
							.getCdDigitoConta(), false));
					setTipoContaDebito(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getDescricaoTipoContaDebito());
					setDsBancoDebito(PgitUtil.formatBanco(registroSelecionado
							.getCdBanco(),
							getFiltroAgendamentoEfetivacaoEstornoBean()
									.getDescricaoBancoContaDebito(), false));
					setDsAgenciaDebito(PgitUtil.formatAgencia(
							registroSelecionado.getCdAgencia(),
							registroSelecionado.getCdDigitoAgencia(),
							getFiltroAgendamentoEfetivacaoEstornoBean()
									.getDescricaoAgenciaContaDebitoBancaria(),
							false));
				}
			}

		}

		setNrCnpjCpf(CpfCnpjUtils.formatCpfCnpjCompleto(registroSelecionado
				.getNrCnpjCpf(), registroSelecionado.getNrFilialCnpjCpf(),
				registroSelecionado.getCdDigitoCnpjCpf()));
		setDsRazaoSocial(registroSelecionado.getDsRazaoSocial());

	}

	/**
	 * Carrega conta banco agencia debito.
	 */
	public void carregaContaBancoAgenciaDebito() {

		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionado = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());

		try {
			ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO = new ConsultarDescBancoAgenciaContaEntradaDTO();
			consultarDescBancoAgenciaContaEntradaDTO
					.setCdBanco(registroSelecionado.getCdBanco());
			consultarDescBancoAgenciaContaEntradaDTO
					.setCdAgencia(registroSelecionado.getCdAgencia());
			consultarDescBancoAgenciaContaEntradaDTO
					.setCdConta(registroSelecionado.getCdConta());
			consultarDescBancoAgenciaContaEntradaDTO.setCdDigitoConta(PgitUtil
					.complementaDigitoConta(registroSelecionado
							.getCdDigitoConta()));

			ConsultarDescBancoAgenciaContaSaidaDTO consultarDescBancoAgenciaContaSaidaDTO = getConsultasService()
					.consultarDescBancoAgenciaConta(
							consultarDescBancoAgenciaContaEntradaDTO);

			setDescricaoTipoContaDebito(consultarDescBancoAgenciaContaSaidaDTO
					.getDsTipoConta());
			setDescricaoBancoContaDebito(consultarDescBancoAgenciaContaSaidaDTO
					.getDsBanco());
			setDescricaoAgenciaContaDebitoBancaria(consultarDescBancoAgenciaContaSaidaDTO
					.getDsAgencia());
		} catch (PdcAdapterFunctionalException e) {
			setDescricaoTipoContaDebito("");
			setDescricaoBancoContaDebito("");
			setDescricaoAgenciaContaDebitoBancaria("");
		}

		setDsBancoDebito(PgitUtil.concatenarCampos(registroSelecionado
				.getCdBanco(), getDescricaoBancoContaDebito(), "-"));
		setDsAgenciaDebito(PgitUtil.formatAgencia(registroSelecionado
				.getCdAgencia(), registroSelecionado.getCdDigitoAgencia(),
				getDescricaoAgenciaContaDebitoBancaria(), false));
		setContaDebito(PgitUtil.concatenarCampos(registroSelecionado
				.getCdConta(), registroSelecionado.getCdDigitoConta(), "-"));
		setTipoContaDebito(getDescricaoTipoContaDebito());
	}

	/**
	 * Carrega dados contrato.
	 */
	public void carregaDadosContrato() {

		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionado = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());

		try {
			DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
			entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
					.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(registroSelecionado
					.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
					.getNrContrato());
			DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService()
					.detalharDadosContrato(entradaDTO);
			setDescricaoContrato(saidaDTO.getDsContrato());
			setDescricaoNumeroContrato(String.valueOf(saidaDTO
					.getNrSequenciaContratoNegocio()));
			setDescricaoEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
			setDescricaoSituacaoContrato(saidaDTO
					.getDsSituacaoContratoNegocio());
		} catch (PdcAdapterFunctionalException p) {
			setDescricaoContrato("");
			setDescricaoNumeroContrato("");
			setDescricaoEmpresa("");
			setDescricaoSituacaoContrato("");
		}

		setDsEmpresa(getDescricaoEmpresa());
		setNroContrato(getDescricaoNumeroContrato());
		setDsContrato(getDescricaoContrato());
		setCdSituacaoContrato(getDescricaoSituacaoContrato());

	}

	/**
	 * Lib parc processamento.
	 *
	 * @return the string
	 */
	public String libParcProcessamento() {
		try {
			consultarListaPagamento();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridListaPagamentos(null);
			setItemSelecionadoListaPagamentos(null);
			return "";
		}
		setOpcaoChecarTodosLiberacao(false);
		setPanelBotoesLiberacao(false);
		return "LIB_PARC_PROCESSAMENTO";
	}

	/**
	 * Voltar parcial processamento pendencia.
	 *
	 * @return the string
	 */
	public String voltarParcialProcessamentoPendencia() {
		setOpcaoChecarTodosLiberacao(false);

		for (int i = 0; i < getListaGridListaPagamentos().size(); i++) {
			getListaGridListaPagamentos().get(i).setCheck(false);
		}

		return "VOLTAR";
	}

	/**
	 * Lib parc pendencia.
	 *
	 * @return the string
	 */
	public String libParcPendencia() {
		try {
			consultarListaPagamento();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridListaPagamentos(null);
			setItemSelecionadoListaPagamentos(null);
			return "";
		}
		setOpcaoChecarTodosLiberacao(false);
		setPanelBotoesLiberacao(false);
		return "LIB_PARC_PENDENCIA";
	}

	/**
	 * Lib int processamento.
	 *
	 * @return the string
	 */
	public String libIntProcessamento() {
		try {
			consultarListaPagamento();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridListaPagamentos(null);
			setItemSelecionadoListaPagamentos(null);
			return "";
		}
		return "LIB_INT_PROCESSAMENTO";
	}

	/**
	 * Lib int pendencia.
	 *
	 * @return the string
	 */
	public String libIntPendencia() {
		try {
			consultarListaPagamento();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridListaPagamentos(null);
			setItemSelecionadoListaPagamentos(null);
			return "";
		}
		return "LIB_INT_PENDENCIA";
	}

	/**
	 * Detalhar filtro.
	 *
	 * @return the string
	 */
	public String detalharFiltro() {
		try {
			consultarListaPagamento();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridListaPagamentos(null);
			setItemSelecionadoListaPagamentos(null);
			return "";
		}
		return "DETALHAR_FILTRO";
	}

	/**
	 * Carregar pagamentos.
	 */
	private void carregarPagamentos() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionado = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		setTipoServico(registroSelecionado.getDsResumoProduto());
		setModalidade(registroSelecionado.getDsOperacao());
		setQtdePagamentos(registroSelecionado.getQtddPagamentoFormatado());
		setVlTotalPagamentos(registroSelecionado.getVlPagamento());
		setNrRemessa(registroSelecionado.getNrRemessa());
		setDtPagamento(registroSelecionado.getDtPagamento());
	}

	/**
	 * Limpar campos favorecido beneficiario.
	 */
	private void limparCamposFavorecidoBeneficiario() {
		// Favorecido
		setDsFavorecido("");
		setCdFavorecido("");
		setDsBancoFavorecido("");
		setDsAgenciaFavorecido("");
		setDsContaFavorecido("");
		setDsTipoContaFavorecido("");
		setNumeroInscricaoFavorecido("");
		setTipoFavorecido("");

		// Benefici�rio
		setNumeroInscricaoBeneficiario("");
		setTipoBeneficiario("");
		setNomeBeneficiario("");
		setDtNascimentoBeneficiario("");
		setTipoBeneficiario("");
		setDsTipoBeneficiario("");
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		try {
			ConsultarListaPagamentoSaidaDTO registroSelecionado = getListaGridListaPagamentos()
					.get(getItemSelecionadoListaPagamentos());

			switch (registroSelecionado.getCdTipoTela()) {
			case 1:
				preencheDadosPagtoCreditoConta();
				return "DETALHE_CREDITO_CONTA";
			case 2:
				preencheDadosPagtoDebitoConta();
				return "DETALHE_DEBITO_CONTA";
			case 3:
				preencheDadosPagtoDOC();
				return "DETALHE_DOC";
			case 4:
				preencheDadosPagtoTED();
				return "DETALHE_TED";
			case 5:
				preencheDadosPagtoOrdemPagto();
				return "DETALHE_ORDEM_PAGTO";
			case 6:
				preencheDadosPagtoTituloBradesco();
				return "DETALHE_TITULOS_BRADESCO";
			case 7:
				preencheDadosPagtoTituloOutrosBancos();
				return "DETALHE_TITULOS_OUTROS_BANCOS";
			case 8:
				preencheDadosPagtoDARF();
				return "DETALHE_DARF";
			case 9:
				preencheDadosPagtoGARE();
				return "DETALHE_GARE";
			case 10:
				preencheDadosPagtoGPS();
				return "DETALHE_GPS";
			case 11:
				preencheDadosPagtoCodigoBarras();
				return "DETALHE_CODIGO_BARRAS";
			case 12:
				preencheDadosPagtoDebitoVeiculos();
				return "DETALHE_DEBITO_VEICULOS";
			case 13:
				preencheDadosPagtoDepIdentificado();
				return "DETALHE_DEPOSITO_IDENTIFICADO";
			case 14:
				preencheDadosPagtoOrdemCredito();
				return "DETALHE_ORDEM_CREDITO";
			default:
				return "";
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return "";
		}
	}

	/**
	 * Preenche dados pagto credito conta.
	 */
	public void preencheDadosPagtoCreditoConta() {

		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoCreditoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoCreditoConta(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null
				|| saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
				.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
				saidaDTO.getDsBancoDebito(), false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(
				saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
				saidaDTO.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
				saidaDTO.getDsDigAgenciaDebito(), false));
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidade() == 1 || saidaDTO.getCdIndicadorModalidade() == 3) {
			// Favorecido
			setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
					.getCdTipoInscricaoFavorecido(), saidaDTO
					.getNmInscriFavorecido()));
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido()
					.toString());
			if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
				setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
			} else {
				setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
						.parseInt(getTipoFavorecido())));
			}
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido() == null
					|| saidaDTO.getCdFavorecido().equals("") ? null : saidaDTO
					.getCdFavorecido());
			setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO
					.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(),
					false));
			setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
					.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
					saidaDTO.getDsAgenciaFavorecido(), false));
			setDsContaFavorecido(PgitUtil.formatConta(saidaDTO
					.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
					false));
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidade() == 1 ? true : false);
		} else {
			// Benefici�rio
			setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

			exibeBeneficiario = !PgitUtil.verificaStringNula(
					getNumeroInscricaoBeneficiario()).equals("")
					|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
							.equals(""))
					|| !PgitUtil.verificaStringNula(getNomeBeneficiario())
							.equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
				saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
				saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil.formatConta(
				saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
				false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setCdSituacaoTranferenciaAutomatica(saidaDTO
				.getCdSituacaoTranferenciaAutomatica());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setVlrAgendamento(saidaDTO.getVlrAgendamento());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
		setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
		setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
		setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
		
		//Favorecidos
		setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
		setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - " + saidaDTO.getDsBancoOriginal());
		setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
		setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
		setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-" + saidaDTO.getCdDigitoAgenciaOriginal() + " - " + saidaDTO.getDsAgenciaOriginal());
		setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
		setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-" + saidaDTO.getCdDigitoContaOriginal());
		setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());

	}

	/**
	 * Preenche dados pagto debito conta.
	 */
	public void preencheDadosPagtoDebitoConta() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoDebitoContaEntradaDTO entradaDTO = new DetalharPagtoDebitoContaEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoDebitoConta(entradaDTO);

		// Cabecalho
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null
				|| saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
				.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// Conta D�bito
		setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
				saidaDTO.getDsBancoDebito(), false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(
				saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
				saidaDTO.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
				saidaDTO.getCdDigitoContaDestino(), false));
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
				.getCdTipoInscricaoFavorecido(), saidaDTO
				.getNmInscriFavorecido()));
		setTipoFavorecido(String.valueOf(PgitUtil.verificaIntegerNulo(saidaDTO
				.getCdTipoInscricaoFavorecido())));
		if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
		} else {
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
					.parseInt(getTipoFavorecido())));
		}
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : saidaDTO
				.getCdFavorecido().toString());
		setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
				saidaDTO.getDsBancoFavorecido(), false));
		setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
				saidaDTO.getDsAgenciaFavorecido(), false));
		setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(),
				saidaDTO.getCdDigContaCredito(), false));
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlrAgendamento());
		setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
				saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
				saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil.formatConta(
				saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
				false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto doc.
	 */
	public void preencheDadosPagtoDOC() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoDOCEntradaDTO entradaDTO = new DetalharPagtoDOCEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoDOC(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null
				|| saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
				.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			// Favorecido
			setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
					.getCdTipoInscricaoFavorecido(), saidaDTO
					.getNmInscriFavorecido()));
			setTipoFavorecido(String.valueOf(saidaDTO
					.getCdTipoInscricaoFavorecido()));
			if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
				setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
			} else {
				setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
						.parseInt(getTipoFavorecido())));
			}
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			// Benefici�rio
			setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

			exibeBeneficiario = !PgitUtil.verificaStringNula(
					getNumeroInscricaoBeneficiario()).equals("")
					|| !PgitUtil.verificaStringNula(getDsTipoBeneficiario())
							.equals("")
					|| !PgitUtil.verificaStringNula(getNomeBeneficiario())
							.equals("");
			setExibeDcom(false);

		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
		setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
		setIdentificacaoTransferenciaDoc(saidaDTO
				.getDsIdentificacaoTransferenciaPagto());
		setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto ted.
	 */
	public void preencheDadosPagtoTED() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoTEDEntradaDTO entradaDTO = new DetalharPagtoTEDEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTEDSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoTED(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;

		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			// Favorecido
			setNumeroInscricaoFavorecido(saidaDTO
					.getNumeroInscricaoFavorecidoFormatado());
			setTipoFavorecido(String.valueOf(saidaDTO
					.getCdTipoInscricaoFavorecido()));
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
					.getCdTipoInscricaoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido() == null ? "" : saidaDTO
					.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			// Benefici�rio
			setNumeroInscricaoBeneficiario(saidaDTO.getCdFavorecido());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(registroSelecionadoDetalharFiltro
					.getDsBeneficiario());

			exibeBeneficiario = !PgitUtil.verificaStringNula(
					getNumeroInscricaoBeneficiario()).equals("")
					|| !PgitUtil.verificaStringNula(getDsTipoBeneficiario())
							.equals("")
					|| !PgitUtil.verificaStringNula(getNomeBeneficiario())
							.equals("");
			setExibeDcom(false);

		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(registroSelecionadoDetalharFiltro.getNrPagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(registroSelecionadoConsulta.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(registroSelecionadoConsulta
				.getDsSituacaoOperacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());

		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());

		setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
		setFinalidadeTed(saidaDTO.getDsFinalidadeDocTed());
		setIdentificacaoTransferenciaTed(saidaDTO
				.getDsIdentificacaoTransferenciaPagto());
		setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
		setCdIndentificadorJudicial(saidaDTO.getCdIndentificadorJudicial());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatidoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlIrCredito());
		setVlIss(saidaDTO.getVlIssCredito());
		setVlIof(saidaDTO.getVlIofCredito());
		setVlInss(saidaDTO.getVlInssCredito());
		setCdIndentificadorJudicial(saidaDTO.getCdIndentificadorJudicial());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto ordem pagto.
	 */
	public void preencheDadosPagtoOrdemPagto() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharPagtoOrdemPagtoEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoOrdemPagtoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoOrdemPagto(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			setNumeroInscricaoFavorecido(saidaDTO
					.getNumeroInscricaoFavorecidoFormatado());
			setTipoFavorecido(String.valueOf(saidaDTO
					.getCdTipoInscricaoFavorecido()));
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
					.getCdTipoInscricaoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			setNumeroInscricaoBeneficiario(saidaDTO
					.getNumeroInscricaoBeneficiarioFormatado());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(
					getNumeroInscricaoBeneficiario()).equals("")
					|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
							.equals(""))
					|| !PgitUtil.verificaStringNula(getNomeBeneficiario())
							.equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setNumeroOp(saidaDTO.getNrOperacao());
		setDataExpiracaoCredito(saidaDTO.getDtExpedicaoCredito());
		setInstrucaoPagamento(saidaDTO.getCdInsPagamento());
		setNumeroCheque(saidaDTO.getNrCheque());
		setSerieCheque(saidaDTO.getNrSerieCheque());
		setIndicadorAssociadoUnicaAgencia(saidaDTO.getDsUnidadeAgencia());
		setIdentificadorTipoRetirada(saidaDTO.getDsIdentificacaoTipoRetirada());
		setIdentificadorRetirada(saidaDTO.getDsIdentificacaoRetirada());
		setDataRetirada(saidaDTO.getDtRetirada());
		setVlImpostoMovFinanceira(saidaDTO.getVlImpostoMovimentacaoFinanceira());
		setVlDesconto(saidaDTO.getVlDescontoCredito());
		setVlAbatimento(saidaDTO.getVlAbatidoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlIrCredito());
		setVlIss(saidaDTO.getVlIssCredito());
		setVlIof(saidaDTO.getVlIofCredito());
		setVlInss(saidaDTO.getVlInssCredito());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

	}

	/**
	 * Preenche dados pagto titulo bradesco.
	 */
	public void preencheDadosPagtoTituloBradesco() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharPagtoTituloBradescoEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoTituloBradesco(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(String.valueOf(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCodigoBarras(saidaDTO.getCdBarras());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
		setNossoNumero(saidaDTO.getNossoNumero());
		setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
		setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
		setCarteira(saidaDTO.getCarteira());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
		setDsRastreado(saidaDTO.getCdRastreado()==1?"Sim":"N�o");

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto titulo outros bancos.
	 */
	public void preencheDadosPagtoTituloOutrosBancos() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharPagtoTituloOutrosBancosEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoTituloOutrosBancos(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(String.valueOf(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCodigoBarras(saidaDTO.getCdBarras());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
		setNossoNumero(saidaDTO.getNossoNumero());
		setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
		setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
		setCarteira(saidaDTO.getCarteira());
		setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
		setDsRastreado(saidaDTO.getCdRastreado()==1?"Sim":"N�o");

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto darf.
	 */
	public void preencheDadosPagtoDARF() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoDARFEntradaDTO entradaDTO = new DetalharPagtoDARFEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoDARF(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(String.valueOf(saidaDTO
				.getCdTipoInscricaoFavorecido()));

		if (PgitUtil.verificaStringNula(getTipoFavorecido()).equals("")) {
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(0));
		} else {
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(Integer
					.parseInt(getTipoFavorecido())));
		}

		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddTelefone());
		setTelefoneContribuinte(saidaDTO.getCdTelefone());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
		setAgenteArrecadador(saidaDTO.getCdAgenteArrecad());
		setCodigoReceita(saidaDTO.getCdReceitaTributo());
		setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcela());
		setPercentualReceita(saidaDTO.getCdPercentualReceita());
		setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
		setAnoExercicio(saidaDTO.getCdDanoExercicio());
		setDataReferencia(saidaDTO.getDtReferenciaTributo());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
		setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlTotal(saidaDTO.getVlTotalTributo());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto gare.
	 */
	public void preencheDadosPagtoGARE() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoGAREEntradaDTO entradaDTO = new DetalharPagtoGAREEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoGARE(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(String.valueOf(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
				.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddContribuinte());
		setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
		setInscricaoEstadualContribuinte(saidaDTO
				.getCdInscricaoEstadualContribuinte());
		setCodigoReceita(saidaDTO.getCdReceita());
		setNumeroReferencia(saidaDTO.getNrReferencia());
		setNumeroCotaParcela(saidaDTO.getNrCota());
		setPercentualReceita(saidaDTO.getCdPercentualReceita());
		setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
		setDataReferencia(saidaDTO.getDtReferencia());
		setCodigoCnae(saidaDTO.getCdCnae());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setNumeroParcelamento(saidaDTO.getNrParcela());
		setCodigoOrgaoFavorecido(saidaDTO.getCdOrFavorecido());
		setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
		setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
		setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
		setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setObservacaoTributo(saidaDTO.getDsTributo());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto gps.
	 */
	public void preencheDadosPagtoGPS() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoGPSEntradaDTO entradaDTO = new DetalharPagtoGPSEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoGPS(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(String.valueOf(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : saidaDTO
				.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddContribuinte());
		setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
		setCodigoInss(saidaDTO.getCdInss());
		setDataCompetencia(saidaDTO.getDsMesAnoCompt());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto codigo barras.
	 */
	public void preencheDadosPagtoCodigoBarras() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharPagtoCodigoBarrasEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoCodigoBarras(entradaDTO);

		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(String.valueOf(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddTelefone());
		setTelefoneContribuinte(saidaDTO.getNrTelefone());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
		setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
		setCodigoReceita(saidaDTO.getCdReceitaTributo());
		setAgenteArrecadador(saidaDTO.getCdAgenteArrecadador());
		setCodigoTributo(saidaDTO.getCdTributoArrecadado());
		setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTributo());
		setPercentualReceita(saidaDTO.getCdPercentualTributo());
		setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
		setAnoExercicio(saidaDTO.getCdAnoExercicioTributo());
		setDataReferencia(saidaDTO.getDtReferenciaTributo());
		setDataCompetencia(saidaDTO.getDtCompensacao());
		setCodigoCnae(saidaDTO.getCdCnae());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setNumeroParcelamento(saidaDTO.getNrParcelamentoTributo());
		setCodigoOrgaoFavorecido(saidaDTO.getCdOrgaoFavorecido());
		setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
		setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
		setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
		setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setObservacaoTributo(saidaDTO.getDsObservacao());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto debito veiculos.
	 */
	public void preencheDadosPagtoDebitoVeiculos() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharPagtoDebitoVeiculosEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoDebitoVeiculos(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
				.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTrib());
		setAnoExercicio(saidaDTO.getDtAnoExercTributo());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
		setMunicipioTributo(saidaDTO.getCdMunicArrecadTrib());
		setUfTributo(saidaDTO.getCdUfTributo());
		setNumeroNsu(saidaDTO.getNrNsuTributo());
		setOpcaoTributo(saidaDTO.getCdOpcaoTributo());
		setOpcaoRetiradaTributo(saidaDTO.getCdOpcaoRetirada());
		setVlPrincipal(saidaDTO.getVlPrincipalTributo());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlTotal(saidaDTO.getVlTotalTributo());
		setObservacaoTributo(saidaDTO.getDsObservacaoTributo());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

	}

	/**
	 * Preenche dados pagto dep identificado.
	 */
	public void preencheDadosPagtoDepIdentificado() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharPagtoDepIdentificadoEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoDepIdentificado(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Beneficiario
		setNumeroInscricaoBeneficiario(saidaDTO
				.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido().toString());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
				.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
				saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
				saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil.formatConta(
				saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
				false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setClienteDepositoIdentificado(saidaDTO.getCdClienteDepositoId());
		setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
		setProdutoDepositoIdentificado(saidaDTO.getCdProdutoDepositoId());
		setSequenciaProdutoDepositoIdentificado(saidaDTO
				.getCdSequenciaProdutoDepositoId());
		setBancoCartaoDepositoIdentificado(saidaDTO
				.getCdBancoCartaoDepositanteId());
		setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
		setBimCartaoDepositoIdentificado(saidaDTO.getCdBinCartaoDepositanteId());
		setViaCartaoDepositoIdentificado(saidaDTO.getCdViaCartaoDepositanteId());
		setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
		setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
		setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
		setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
		setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
		setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
		setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
		setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
		setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
		setVlIss(saidaDTO.getVlIssCreditoPagamento());
		setVlIof(saidaDTO.getVlIofCreditoPagamento());
		setVlInss(saidaDTO.getVlInssCreditoPagamento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto ordem credito.
	 */
	public void preencheDadosPagtoOrdemCredito() {
		ConsultarPagtoPendConsolidadoSaidaDTO registroSelecionadoConsulta = getListaGridLiberarPagtosConsolidadosPend()
				.get(getItemSelecionadoLista());
		ConsultarListaPagamentoSaidaDTO registroSelecionadoDetalharFiltro = getListaGridListaPagamentos()
				.get(getItemSelecionadoListaPagamentos());
		DetalharPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharPagtoOrdemCreditoEntradaDTO();

		entradaDTO
				.setCdPessoaJuridicaContrato(registroSelecionadoDetalharFiltro
						.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionadoDetalharFiltro
				.getCdTipoContratoNegocio());
		entradaDTO
				.setNrSequenciaContratoNegocio(registroSelecionadoDetalharFiltro
						.getNrSequenciaContratoNegocio());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalharFiltro
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionadoConsulta.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionadoConsulta
				.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionadoConsulta.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalharFiltro
				.getCdBancoFavorecido());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalharFiltro
				.getCdAgenciaFavorecido());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalharFiltro
				.getCdContaFavorecido());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionadoConsulta
				.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoOrdemCredito(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Beneficiario
		setNumeroInscricaoBeneficiario(saidaDTO
				.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(String.valueOf(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(saidaDTO
				.getCdTipoInscricaoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
				.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamentoDet(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
				saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
				saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil.formatConta(
				saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
				false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCodigoPagador(saidaDTO.getCdPagador());
		setCodigoOrdemCredito(saidaDTO.getCdOrdemCreditoPagamento());
		setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
		setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
		setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
		setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
		setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
		setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
		setVlIss(saidaDTO.getVlIssCreditoPagamento());
		setVlIof(saidaDTO.getVlIofCreditoPagamento());
		setVlInss(saidaDTO.getVlInssCreditoPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#getListaConsultarMotivoSituacaoPagamentoFiltro()
	 */
	public List<SelectItem> getListaConsultarMotivoSituacaoPagamentoFiltro() {
		return listaConsultarMotivoSituacaoPagamentoFiltro;
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#setListaConsultarMotivoSituacaoPagamentoFiltro(java.util.List)
	 */
	public void setListaConsultarMotivoSituacaoPagamentoFiltro(
			List<SelectItem> listaConsultarMotivoSituacaoPagamentoFiltro) {
		this.listaConsultarMotivoSituacaoPagamentoFiltro = listaConsultarMotivoSituacaoPagamentoFiltro;
	}

	/**
	 * Get: liberarPagtosConsolidadosPendImpl.
	 *
	 * @return liberarPagtosConsolidadosPendImpl
	 */
	public ILiberarPagtosConsolidadosPendService getLiberarPagtosConsolidadosPendImpl() {
		return liberarPagtosConsolidadosPendImpl;
	}

	/**
	 * Set: liberarPagtosConsolidadosPendImpl.
	 *
	 * @param liberarPagtosConsolidadosPendImpl the liberar pagtos consolidados pend impl
	 */
	public void setLiberarPagtosConsolidadosPendImpl(
			ILiberarPagtosConsolidadosPendService liberarPagtosConsolidadosPendImpl) {
		this.liberarPagtosConsolidadosPendImpl = liberarPagtosConsolidadosPendImpl;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaGridLiberarPagtosConsolidadosPend.
	 *
	 * @return listaGridLiberarPagtosConsolidadosPend
	 */
	public List<ConsultarPagtoPendConsolidadoSaidaDTO> getListaGridLiberarPagtosConsolidadosPend() {
		return listaGridLiberarPagtosConsolidadosPend;
	}

	/**
	 * Set: listaGridLiberarPagtosConsolidadosPend.
	 *
	 * @param listaGridLiberarPagtosConsolidadosPend the lista grid liberar pagtos consolidados pend
	 */
	public void setListaGridLiberarPagtosConsolidadosPend(
			List<ConsultarPagtoPendConsolidadoSaidaDTO> listaGridLiberarPagtosConsolidadosPend) {
		this.listaGridLiberarPagtosConsolidadosPend = listaGridLiberarPagtosConsolidadosPend;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: qtdePagamentos.
	 *
	 * @return qtdePagamentos
	 */
	public String getQtdePagamentos() {
		return qtdePagamentos;
	}

	/**
	 * Set: qtdePagamentos.
	 *
	 * @param qtdePagamentos the qtde pagamentos
	 */
	public void setQtdePagamentos(String qtdePagamentos) {
		this.qtdePagamentos = qtdePagamentos;
	}

	/**
	 * Get: vlTotalPagamentos.
	 *
	 * @return vlTotalPagamentos
	 */
	public BigDecimal getVlTotalPagamentos() {
		return vlTotalPagamentos;
	}

	/**
	 * Set: vlTotalPagamentos.
	 *
	 * @param vlTotalPagamentos the vl total pagamentos
	 */
	public void setVlTotalPagamentos(BigDecimal vlTotalPagamentos) {
		this.vlTotalPagamentos = vlTotalPagamentos;
	}

	/**
	 * Get: listaGridSelecionados.
	 *
	 * @return listaGridSelecionados
	 */
	public List<ConsultarListaPagamentoSaidaDTO> getListaGridSelecionados() {
		return listaGridSelecionados;
	}

	/**
	 * Set: listaGridSelecionados.
	 *
	 * @param listaGridSelecionados the lista grid selecionados
	 */
	public void setListaGridSelecionados(
			List<ConsultarListaPagamentoSaidaDTO> listaGridSelecionados) {
		this.listaGridSelecionados = listaGridSelecionados;
	}

	/**
	 * Get: listaControleLiberacao.
	 *
	 * @return listaControleLiberacao
	 */
	public List<SelectItem> getListaControleLiberacao() {
		return listaControleLiberacao;
	}

	/**
	 * Set: listaControleLiberacao.
	 *
	 * @param listaControleLiberacao the lista controle liberacao
	 */
	public void setListaControleLiberacao(
			List<SelectItem> listaControleLiberacao) {
		this.listaControleLiberacao = listaControleLiberacao;
	}

	/**
	 * Is opcao checar todos liberacao.
	 *
	 * @return true, if is opcao checar todos liberacao
	 */
	public boolean isOpcaoChecarTodosLiberacao() {
		return opcaoChecarTodosLiberacao;
	}

	/**
	 * Set: opcaoChecarTodosLiberacao.
	 *
	 * @param opcaoChecarTodosLiberacao the opcao checar todos liberacao
	 */
	public void setOpcaoChecarTodosLiberacao(boolean opcaoChecarTodosLiberacao) {
		this.opcaoChecarTodosLiberacao = opcaoChecarTodosLiberacao;
	}

	/**
	 * Is panel botoes liberacao.
	 *
	 * @return true, if is panel botoes liberacao
	 */
	public boolean isPanelBotoesLiberacao() {
		return panelBotoesLiberacao;
	}

	/**
	 * Set: panelBotoesLiberacao.
	 *
	 * @param panelBotoesLiberacao the panel botoes liberacao
	 */
	public void setPanelBotoesLiberacao(boolean panelBotoesLiberacao) {
		this.panelBotoesLiberacao = panelBotoesLiberacao;
	}

	/**
	 * Get: contaDebito.
	 *
	 * @return contaDebito
	 */
	public String getContaDebito() {
		return contaDebito;
	}

	/**
	 * Set: contaDebito.
	 *
	 * @param contaDebito the conta debito
	 */
	public void setContaDebito(String contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: tipoContaDebito.
	 *
	 * @return tipoContaDebito
	 */
	public String getTipoContaDebito() {
		return tipoContaDebito;
	}

	/**
	 * Set: tipoContaDebito.
	 *
	 * @param tipoContaDebito the tipo conta debito
	 */
	public void setTipoContaDebito(String tipoContaDebito) {
		this.tipoContaDebito = tipoContaDebito;
	}

	/**
	 * Get: modalidade.
	 *
	 * @return modalidade
	 */
	public String getModalidade() {
		return modalidade;
	}

	/**
	 * Set: modalidade.
	 *
	 * @param modalidade the modalidade
	 */
	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}

	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public String getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Get: itemSelecionadoListaPagamentos.
	 *
	 * @return itemSelecionadoListaPagamentos
	 */
	public Integer getItemSelecionadoListaPagamentos() {
		return itemSelecionadoListaPagamentos;
	}

	/**
	 * Set: itemSelecionadoListaPagamentos.
	 *
	 * @param itemSelecionadoListaPagamentos the item selecionado lista pagamentos
	 */
	public void setItemSelecionadoListaPagamentos(
			Integer itemSelecionadoListaPagamentos) {
		this.itemSelecionadoListaPagamentos = itemSelecionadoListaPagamentos;
	}

	/**
	 * Get: listaGridListaPagamentos.
	 *
	 * @return listaGridListaPagamentos
	 */
	public List<ConsultarListaPagamentoSaidaDTO> getListaGridListaPagamentos() {
		return listaGridListaPagamentos;
	}

	/**
	 * Set: listaGridListaPagamentos.
	 *
	 * @param listaGridListaPagamentos the lista grid lista pagamentos
	 */
	public void setListaGridListaPagamentos(
			List<ConsultarListaPagamentoSaidaDTO> listaGridListaPagamentos) {
		this.listaGridListaPagamentos = listaGridListaPagamentos;
	}

	/**
	 * Get: agenciaDigitoCredito.
	 *
	 * @return agenciaDigitoCredito
	 */
	public String getAgenciaDigitoCredito() {
		return agenciaDigitoCredito;
	}

	/**
	 * Set: agenciaDigitoCredito.
	 *
	 * @param agenciaDigitoCredito the agencia digito credito
	 */
	public void setAgenciaDigitoCredito(String agenciaDigitoCredito) {
		this.agenciaDigitoCredito = agenciaDigitoCredito;
	}

	/**
	 * Get: agenciaDigitoDestino.
	 *
	 * @return agenciaDigitoDestino
	 */
	public String getAgenciaDigitoDestino() {
		return agenciaDigitoDestino;
	}

	/**
	 * Set: agenciaDigitoDestino.
	 *
	 * @param agenciaDigitoDestino the agencia digito destino
	 */
	public void setAgenciaDigitoDestino(String agenciaDigitoDestino) {
		this.agenciaDigitoDestino = agenciaDigitoDestino;
	}

	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public String getCdBancoCredito() {
		return cdBancoCredito;
	}

	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(String cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}

	/**
	 * Get: cdBancoDestino.
	 *
	 * @return cdBancoDestino
	 */
	public String getCdBancoDestino() {
		return cdBancoDestino;
	}

	/**
	 * Set: cdBancoDestino.
	 *
	 * @param cdBancoDestino the cd banco destino
	 */
	public void setCdBancoDestino(String cdBancoDestino) {
		this.cdBancoDestino = cdBancoDestino;
	}

	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public String getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}

	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(String cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}

	/**
	 * Get: cdListaDebito.
	 *
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}

	/**
	 * Set: cdListaDebito.
	 *
	 * @param cdListaDebito the cd lista debito
	 */
	public void setCdListaDebito(String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}

	/**
	 * Get: cdSerieDocumento.
	 *
	 * @return cdSerieDocumento
	 */
	public String getCdSerieDocumento() {
		return cdSerieDocumento;
	}

	/**
	 * Set: cdSerieDocumento.
	 *
	 * @param cdSerieDocumento the cd serie documento
	 */
	public void setCdSerieDocumento(String cdSerieDocumento) {
		this.cdSerieDocumento = cdSerieDocumento;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public String getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(
			String cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: contaDigitoCredito.
	 *
	 * @return contaDigitoCredito
	 */
	public String getContaDigitoCredito() {
		return contaDigitoCredito;
	}

	/**
	 * Set: contaDigitoCredito.
	 *
	 * @param contaDigitoCredito the conta digito credito
	 */
	public void setContaDigitoCredito(String contaDigitoCredito) {
		this.contaDigitoCredito = contaDigitoCredito;
	}

	/**
	 * Get: contaDigitoDestino.
	 *
	 * @return contaDigitoDestino
	 */
	public String getContaDigitoDestino() {
		return contaDigitoDestino;
	}

	/**
	 * Set: contaDigitoDestino.
	 *
	 * @param contaDigitoDestino the conta digito destino
	 */
	public void setContaDigitoDestino(String contaDigitoDestino) {
		this.contaDigitoDestino = contaDigitoDestino;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: dsAgenciaFavorecido.
	 *
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}

	/**
	 * Set: dsAgenciaFavorecido.
	 *
	 * @param dsAgenciaFavorecido the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}

	/**
	 * Get: dsBancoFavorecido.
	 *
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}

	/**
	 * Set: dsBancoFavorecido.
	 *
	 * @param dsBancoFavorecido the ds banco favorecido
	 */
	public void setDsBancoFavorecido(String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}

	/**
	 * Get: dsContaFavorecido.
	 *
	 * @return dsContaFavorecido
	 */
	public String getDsContaFavorecido() {
		return dsContaFavorecido;
	}

	/**
	 * Set: dsContaFavorecido.
	 *
	 * @param dsContaFavorecido the ds conta favorecido
	 */
	public void setDsContaFavorecido(String dsContaFavorecido) {
		this.dsContaFavorecido = dsContaFavorecido;
	}

	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}

	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}

	/**
	 * Get: dsIndicadorModalidade.
	 *
	 * @return dsIndicadorModalidade
	 */
	public String getDsIndicadorModalidade() {
		return dsIndicadorModalidade;
	}

	/**
	 * Set: dsIndicadorModalidade.
	 *
	 * @param dsIndicadorModalidade the ds indicador modalidade
	 */
	public void setDsIndicadorModalidade(String dsIndicadorModalidade) {
		this.dsIndicadorModalidade = dsIndicadorModalidade;
	}

	/**
	 * Get: dsMensagemPrimeiraLinha.
	 *
	 * @return dsMensagemPrimeiraLinha
	 */
	public String getDsMensagemPrimeiraLinha() {
		return dsMensagemPrimeiraLinha;
	}

	/**
	 * Set: dsMensagemPrimeiraLinha.
	 *
	 * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
	 */
	public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
		this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
	}

	/**
	 * Get: dsMensagemSegundaLinha.
	 *
	 * @return dsMensagemSegundaLinha
	 */
	public String getDsMensagemSegundaLinha() {
		return dsMensagemSegundaLinha;
	}

	/**
	 * Set: dsMensagemSegundaLinha.
	 *
	 * @param dsMensagemSegundaLinha the ds mensagem segunda linha
	 */
	public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
		this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
	}

	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	/**
	 * Get: dsPagamento.
	 *
	 * @return dsPagamento
	 */
	public String getDsPagamento() {
		return dsPagamento;
	}

	/**
	 * Set: dsPagamento.
	 *
	 * @param dsPagamento the ds pagamento
	 */
	public void setDsPagamento(String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}

	/**
	 * Get: dsTipoContaFavorecido.
	 *
	 * @return dsTipoContaFavorecido
	 */
	public String getDsTipoContaFavorecido() {
		return dsTipoContaFavorecido;
	}

	/**
	 * Set: dsTipoContaFavorecido.
	 *
	 * @param dsTipoContaFavorecido the ds tipo conta favorecido
	 */
	public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
		this.dsTipoContaFavorecido = dsTipoContaFavorecido;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: dsUsoEmpresa.
	 *
	 * @return dsUsoEmpresa
	 */
	public String getDsUsoEmpresa() {
		return dsUsoEmpresa;
	}

	/**
	 * Set: dsUsoEmpresa.
	 *
	 * @param dsUsoEmpresa the ds uso empresa
	 */
	public void setDsUsoEmpresa(String dsUsoEmpresa) {
		this.dsUsoEmpresa = dsUsoEmpresa;
	}

	/**
	 * Get: dtAgendamento.
	 *
	 * @return dtAgendamento
	 */
	public String getDtAgendamento() {
		return dtAgendamento;
	}

	/**
	 * Set: dtAgendamento.
	 *
	 * @param dtAgendamento the dt agendamento
	 */
	public void setDtAgendamento(String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	/**
	 * Get: dtEmissaoDocumento.
	 *
	 * @return dtEmissaoDocumento
	 */
	public String getDtEmissaoDocumento() {
		return dtEmissaoDocumento;
	}

	/**
	 * Set: dtEmissaoDocumento.
	 *
	 * @param dtEmissaoDocumento the dt emissao documento
	 */
	public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
		this.dtEmissaoDocumento = dtEmissaoDocumento;
	}

	/**
	 * Get: dtNascimentoBeneficiario.
	 *
	 * @return dtNascimentoBeneficiario
	 */
	public String getDtNascimentoBeneficiario() {
		return dtNascimentoBeneficiario;
	}

	/**
	 * Set: dtNascimentoBeneficiario.
	 *
	 * @param dtNascimentoBeneficiario the dt nascimento beneficiario
	 */
	public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
		this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
	}

	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	/**
	 * Get: dtVencimento.
	 *
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 *
	 * @param dtVencimento the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: nomeBeneficiario.
	 *
	 * @return nomeBeneficiario
	 */
	public String getNomeBeneficiario() {
		return nomeBeneficiario;
	}

	/**
	 * Set: nomeBeneficiario.
	 *
	 * @param nomeBeneficiario the nome beneficiario
	 */
	public void setNomeBeneficiario(String nomeBeneficiario) {
		this.nomeBeneficiario = nomeBeneficiario;
	}

	/**
	 * Get: nrDocumento.
	 *
	 * @return nrDocumento
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}

	/**
	 * Set: nrDocumento.
	 *
	 * @param nrDocumento the nr documento
	 */
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	/**
	 * Get: nrLoteArquivoRemessa.
	 *
	 * @return nrLoteArquivoRemessa
	 */
	public String getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}

	/**
	 * Set: nrLoteArquivoRemessa.
	 *
	 * @param nrLoteArquivoRemessa the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}

	/**
	 * Get: nrSequenciaArquivoRemessa.
	 *
	 * @return nrSequenciaArquivoRemessa
	 */
	public String getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}

	/**
	 * Set: nrSequenciaArquivoRemessa.
	 *
	 * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}

	/**
	 * Get: numeroInscricaoBeneficiario.
	 *
	 * @return numeroInscricaoBeneficiario
	 */
	public String getNumeroInscricaoBeneficiario() {
		return numeroInscricaoBeneficiario;
	}

	/**
	 * Set: numeroInscricaoBeneficiario.
	 *
	 * @param numeroInscricaoBeneficiario the numero inscricao beneficiario
	 */
	public void setNumeroInscricaoBeneficiario(
			String numeroInscricaoBeneficiario) {
		this.numeroInscricaoBeneficiario = numeroInscricaoBeneficiario;
	}

	/**
	 * Get: numeroInscricaoFavorecido.
	 *
	 * @return numeroInscricaoFavorecido
	 */
	public String getNumeroInscricaoFavorecido() {
		return numeroInscricaoFavorecido;
	}

	/**
	 * Set: numeroInscricaoFavorecido.
	 *
	 * @param numeroInscricaoFavorecido the numero inscricao favorecido
	 */
	public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
		this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
	}

	/**
	 * Get: numeroPagamento.
	 *
	 * @return numeroPagamento
	 */
	public String getNumeroPagamento() {
		return numeroPagamento;
	}

	/**
	 * Set: numeroPagamento.
	 *
	 * @param numeroPagamento the numero pagamento
	 */
	public void setNumeroPagamento(String numeroPagamento) {
		this.numeroPagamento = numeroPagamento;
	}

	/**
	 * Get: qtMoeda.
	 *
	 * @return qtMoeda
	 */
	public BigDecimal getQtMoeda() {
		return qtMoeda;
	}

	/**
	 * Set: qtMoeda.
	 *
	 * @param qtMoeda the qt moeda
	 */
	public void setQtMoeda(BigDecimal qtMoeda) {
		this.qtMoeda = qtMoeda;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: tipoContaCredito.
	 *
	 * @return tipoContaCredito
	 */
	public String getTipoContaCredito() {
		return tipoContaCredito;
	}

	/**
	 * Set: tipoContaCredito.
	 *
	 * @param tipoContaCredito the tipo conta credito
	 */
	public void setTipoContaCredito(String tipoContaCredito) {
		this.tipoContaCredito = tipoContaCredito;
	}

	/**
	 * Get: tipoContaDestino.
	 *
	 * @return tipoContaDestino
	 */
	public String getTipoContaDestino() {
		return tipoContaDestino;
	}

	/**
	 * Set: tipoContaDestino.
	 *
	 * @param tipoContaDestino the tipo conta destino
	 */
	public void setTipoContaDestino(String tipoContaDestino) {
		this.tipoContaDestino = tipoContaDestino;
	}

	/**
	 * Get: tipoDocumento.
	 *
	 * @return tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Set: tipoDocumento.
	 *
	 * @param tipoDocumento the tipo documento
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Get: tipoFavorecido.
	 *
	 * @return tipoFavorecido
	 */
	public String getTipoFavorecido() {
		return tipoFavorecido;
	}

	/**
	 * Set: tipoFavorecido.
	 *
	 * @param tipoFavorecido the tipo favorecido
	 */
	public void setTipoFavorecido(String tipoFavorecido) {
		this.tipoFavorecido = tipoFavorecido;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: vlAbatimento.
	 *
	 * @return vlAbatimento
	 */
	public BigDecimal getVlAbatimento() {
		return vlAbatimento;
	}

	/**
	 * Set: vlAbatimento.
	 *
	 * @param vlAbatimento the vl abatimento
	 */
	public void setVlAbatimento(BigDecimal vlAbatimento) {
		this.vlAbatimento = vlAbatimento;
	}

	/**
	 * Get: vlAcrescimo.
	 *
	 * @return vlAcrescimo
	 */
	public BigDecimal getVlAcrescimo() {
		return vlAcrescimo;
	}

	/**
	 * Set: vlAcrescimo.
	 *
	 * @param vlAcrescimo the vl acrescimo
	 */
	public void setVlAcrescimo(BigDecimal vlAcrescimo) {
		this.vlAcrescimo = vlAcrescimo;
	}

	/**
	 * Get: vlDeducao.
	 *
	 * @return vlDeducao
	 */
	public BigDecimal getVlDeducao() {
		return vlDeducao;
	}

	/**
	 * Set: vlDeducao.
	 *
	 * @param vlDeducao the vl deducao
	 */
	public void setVlDeducao(BigDecimal vlDeducao) {
		this.vlDeducao = vlDeducao;
	}

	/**
	 * Get: vlDesconto.
	 *
	 * @return vlDesconto
	 */
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}

	/**
	 * Set: vlDesconto.
	 *
	 * @param vlDesconto the vl desconto
	 */
	public void setVlDesconto(BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	/**
	 * Get: vlDocumento.
	 *
	 * @return vlDocumento
	 */
	public BigDecimal getVlDocumento() {
		return vlDocumento;
	}

	/**
	 * Set: vlDocumento.
	 *
	 * @param vlDocumento the vl documento
	 */
	public void setVlDocumento(BigDecimal vlDocumento) {
		this.vlDocumento = vlDocumento;
	}

	/**
	 * Get: vlImpostoRenda.
	 *
	 * @return vlImpostoRenda
	 */
	public BigDecimal getVlImpostoRenda() {
		return vlImpostoRenda;
	}

	/**
	 * Set: vlImpostoRenda.
	 *
	 * @param vlImpostoRenda the vl imposto renda
	 */
	public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
		this.vlImpostoRenda = vlImpostoRenda;
	}

	/**
	 * Get: vlInss.
	 *
	 * @return vlInss
	 */
	public BigDecimal getVlInss() {
		return vlInss;
	}

	/**
	 * Set: vlInss.
	 *
	 * @param vlInss the vl inss
	 */
	public void setVlInss(BigDecimal vlInss) {
		this.vlInss = vlInss;
	}

	/**
	 * Get: vlIof.
	 *
	 * @return vlIof
	 */
	public BigDecimal getVlIof() {
		return vlIof;
	}

	/**
	 * Set: vlIof.
	 *
	 * @param vlIof the vl iof
	 */
	public void setVlIof(BigDecimal vlIof) {
		this.vlIof = vlIof;
	}

	/**
	 * Get: vlIss.
	 *
	 * @return vlIss
	 */
	public BigDecimal getVlIss() {
		return vlIss;
	}

	/**
	 * Set: vlIss.
	 *
	 * @param vlIss the vl iss
	 */
	public void setVlIss(BigDecimal vlIss) {
		this.vlIss = vlIss;
	}

	/**
	 * Get: vlMora.
	 *
	 * @return vlMora
	 */
	public BigDecimal getVlMora() {
		return vlMora;
	}

	/**
	 * Set: vlMora.
	 *
	 * @param vlMora the vl mora
	 */
	public void setVlMora(BigDecimal vlMora) {
		this.vlMora = vlMora;
	}

	/**
	 * Get: vlMulta.
	 *
	 * @return vlMulta
	 */
	public BigDecimal getVlMulta() {
		return vlMulta;
	}

	/**
	 * Set: vlMulta.
	 *
	 * @param vlMulta the vl multa
	 */
	public void setVlMulta(BigDecimal vlMulta) {
		this.vlMulta = vlMulta;
	}

	/**
	 * Get: vlrAgendamento.
	 *
	 * @return vlrAgendamento
	 */
	public BigDecimal getVlrAgendamento() {
		return vlrAgendamento;
	}

	/**
	 * Set: vlrAgendamento.
	 *
	 * @param vlrAgendamento the vlr agendamento
	 */
	public void setVlrAgendamento(BigDecimal vlrAgendamento) {
		this.vlrAgendamento = vlrAgendamento;
	}

	/**
	 * Get: vlrEfetivacao.
	 *
	 * @return vlrEfetivacao
	 */
	public BigDecimal getVlrEfetivacao() {
		return vlrEfetivacao;
	}

	/**
	 * Set: vlrEfetivacao.
	 *
	 * @param vlrEfetivacao the vlr efetivacao
	 */
	public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
		this.vlrEfetivacao = vlrEfetivacao;
	}

	/**
	 * Get: tipoBeneficiario.
	 *
	 * @return tipoBeneficiario
	 */
	public String getTipoBeneficiario() {
		return tipoBeneficiario;
	}

	/**
	 * Set: tipoBeneficiario.
	 *
	 * @param tipoBeneficiario the tipo beneficiario
	 */
	public void setTipoBeneficiario(String tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}

	/**
	 * Get: camaraCentralizadora.
	 *
	 * @return camaraCentralizadora
	 */
	public String getCamaraCentralizadora() {
		return camaraCentralizadora;
	}

	/**
	 * Set: camaraCentralizadora.
	 *
	 * @param camaraCentralizadora the camara centralizadora
	 */
	public void setCamaraCentralizadora(String camaraCentralizadora) {
		this.camaraCentralizadora = camaraCentralizadora;
	}

	/**
	 * Get: finalidadeDoc.
	 *
	 * @return finalidadeDoc
	 */
	public String getFinalidadeDoc() {
		return finalidadeDoc;
	}

	/**
	 * Set: finalidadeDoc.
	 *
	 * @param finalidadeDoc the finalidade doc
	 */
	public void setFinalidadeDoc(String finalidadeDoc) {
		this.finalidadeDoc = finalidadeDoc;
	}

	/**
	 * Get: identificacaoTitularidade.
	 *
	 * @return identificacaoTitularidade
	 */
	public String getIdentificacaoTitularidade() {
		return identificacaoTitularidade;
	}

	/**
	 * Set: identificacaoTitularidade.
	 *
	 * @param identificacaoTitularidade the identificacao titularidade
	 */
	public void setIdentificacaoTitularidade(String identificacaoTitularidade) {
		this.identificacaoTitularidade = identificacaoTitularidade;
	}

	/**
	 * Get: identificacaoTransferenciaDoc.
	 *
	 * @return identificacaoTransferenciaDoc
	 */
	public String getIdentificacaoTransferenciaDoc() {
		return identificacaoTransferenciaDoc;
	}

	/**
	 * Set: identificacaoTransferenciaDoc.
	 *
	 * @param identificacaoTransferenciaDoc the identificacao transferencia doc
	 */
	public void setIdentificacaoTransferenciaDoc(
			String identificacaoTransferenciaDoc) {
		this.identificacaoTransferenciaDoc = identificacaoTransferenciaDoc;
	}

	/**
	 * Get: finalidadeTed.
	 *
	 * @return finalidadeTed
	 */
	public String getFinalidadeTed() {
		return finalidadeTed;
	}

	/**
	 * Set: finalidadeTed.
	 *
	 * @param finalidadeTed the finalidade ted
	 */
	public void setFinalidadeTed(String finalidadeTed) {
		this.finalidadeTed = finalidadeTed;
	}

	/**
	 * Get: identificacaoTransferenciaTed.
	 *
	 * @return identificacaoTransferenciaTed
	 */
	public String getIdentificacaoTransferenciaTed() {
		return identificacaoTransferenciaTed;
	}

	/**
	 * Set: identificacaoTransferenciaTed.
	 *
	 * @param identificacaoTransferenciaTed the identificacao transferencia ted
	 */
	public void setIdentificacaoTransferenciaTed(
			String identificacaoTransferenciaTed) {
		this.identificacaoTransferenciaTed = identificacaoTransferenciaTed;
	}

	/**
	 * Get: dataExpiracaoCredito.
	 *
	 * @return dataExpiracaoCredito
	 */
	public String getDataExpiracaoCredito() {
		return dataExpiracaoCredito;
	}

	/**
	 * Set: dataExpiracaoCredito.
	 *
	 * @param dataExpiracaoCredito the data expiracao credito
	 */
	public void setDataExpiracaoCredito(String dataExpiracaoCredito) {
		this.dataExpiracaoCredito = dataExpiracaoCredito;
	}

	/**
	 * Get: dataRetirada.
	 *
	 * @return dataRetirada
	 */
	public String getDataRetirada() {
		return dataRetirada;
	}

	/**
	 * Set: dataRetirada.
	 *
	 * @param dataRetirada the data retirada
	 */
	public void setDataRetirada(String dataRetirada) {
		this.dataRetirada = dataRetirada;
	}

	/**
	 * Get: identificadorRetirada.
	 *
	 * @return identificadorRetirada
	 */
	public String getIdentificadorRetirada() {
		return identificadorRetirada;
	}

	/**
	 * Set: identificadorRetirada.
	 *
	 * @param identificadorRetirada the identificador retirada
	 */
	public void setIdentificadorRetirada(String identificadorRetirada) {
		this.identificadorRetirada = identificadorRetirada;
	}

	/**
	 * Get: identificadorTipoRetirada.
	 *
	 * @return identificadorTipoRetirada
	 */
	public String getIdentificadorTipoRetirada() {
		return identificadorTipoRetirada;
	}

	/**
	 * Set: identificadorTipoRetirada.
	 *
	 * @param identificadorTipoRetirada the identificador tipo retirada
	 */
	public void setIdentificadorTipoRetirada(String identificadorTipoRetirada) {
		this.identificadorTipoRetirada = identificadorTipoRetirada;
	}

	/**
	 * Get: indicadorAssociadoUnicaAgencia.
	 *
	 * @return indicadorAssociadoUnicaAgencia
	 */
	public String getIndicadorAssociadoUnicaAgencia() {
		return indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Set: indicadorAssociadoUnicaAgencia.
	 *
	 * @param indicadorAssociadoUnicaAgencia the indicador associado unica agencia
	 */
	public void setIndicadorAssociadoUnicaAgencia(
			String indicadorAssociadoUnicaAgencia) {
		this.indicadorAssociadoUnicaAgencia = indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Get: instrucaoPagamento.
	 *
	 * @return instrucaoPagamento
	 */
	public String getInstrucaoPagamento() {
		return instrucaoPagamento;
	}

	/**
	 * Set: instrucaoPagamento.
	 *
	 * @param instrucaoPagamento the instrucao pagamento
	 */
	public void setInstrucaoPagamento(String instrucaoPagamento) {
		this.instrucaoPagamento = instrucaoPagamento;
	}

	/**
	 * Get: numeroCheque.
	 *
	 * @return numeroCheque
	 */
	public String getNumeroCheque() {
		return numeroCheque;
	}

	/**
	 * Set: numeroCheque.
	 *
	 * @param numeroCheque the numero cheque
	 */
	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}

	/**
	 * Get: numeroOp.
	 *
	 * @return numeroOp
	 */
	public String getNumeroOp() {
		return numeroOp;
	}

	/**
	 * Set: numeroOp.
	 *
	 * @param numeroOp the numero op
	 */
	public void setNumeroOp(String numeroOp) {
		this.numeroOp = numeroOp;
	}

	/**
	 * Get: serieCheque.
	 *
	 * @return serieCheque
	 */
	public String getSerieCheque() {
		return serieCheque;
	}

	/**
	 * Set: serieCheque.
	 *
	 * @param serieCheque the serie cheque
	 */
	public void setSerieCheque(String serieCheque) {
		this.serieCheque = serieCheque;
	}

	/**
	 * Get: vlImpostoMovFinanceira.
	 *
	 * @return vlImpostoMovFinanceira
	 */
	public BigDecimal getVlImpostoMovFinanceira() {
		return vlImpostoMovFinanceira;
	}

	/**
	 * Set: vlImpostoMovFinanceira.
	 *
	 * @param vlImpostoMovFinanceira the vl imposto mov financeira
	 */
	public void setVlImpostoMovFinanceira(BigDecimal vlImpostoMovFinanceira) {
		this.vlImpostoMovFinanceira = vlImpostoMovFinanceira;
	}

	/**
	 * Get: codigoBarras.
	 *
	 * @return codigoBarras
	 */
	public String getCodigoBarras() {
		return codigoBarras;
	}

	/**
	 * Set: codigoBarras.
	 *
	 * @param codigoBarras the codigo barras
	 */
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	/**
	 * Get: dsOrigemPagamento.
	 *
	 * @return dsOrigemPagamento
	 */
	public String getDsOrigemPagamento() {
		return dsOrigemPagamento;
	}

	/**
	 * Set: dsOrigemPagamento.
	 *
	 * @param dsOrigemPagamento the ds origem pagamento
	 */
	public void setDsOrigemPagamento(String dsOrigemPagamento) {
		this.dsOrigemPagamento = dsOrigemPagamento;
	}

	/**
	 * Get: nossoNumero.
	 *
	 * @return nossoNumero
	 */
	public String getNossoNumero() {
		return nossoNumero;
	}

	/**
	 * Set: nossoNumero.
	 *
	 * @param nossoNumero the nosso numero
	 */
	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	/**
	 * Get: agenteArrecadador.
	 *
	 * @return agenteArrecadador
	 */
	public String getAgenteArrecadador() {
		return agenteArrecadador;
	}

	/**
	 * Set: agenteArrecadador.
	 *
	 * @param agenteArrecadador the agente arrecadador
	 */
	public void setAgenteArrecadador(String agenteArrecadador) {
		this.agenteArrecadador = agenteArrecadador;
	}

	/**
	 * Get: anoExercicio.
	 *
	 * @return anoExercicio
	 */
	public String getAnoExercicio() {
		return anoExercicio;
	}

	/**
	 * Set: anoExercicio.
	 *
	 * @param anoExercicio the ano exercicio
	 */
	public void setAnoExercicio(String anoExercicio) {
		this.anoExercicio = anoExercicio;
	}

	/**
	 * Get: codigoReceita.
	 *
	 * @return codigoReceita
	 */
	public String getCodigoReceita() {
		return codigoReceita;
	}

	/**
	 * Set: codigoReceita.
	 *
	 * @param codigoReceita the codigo receita
	 */
	public void setCodigoReceita(String codigoReceita) {
		this.codigoReceita = codigoReceita;
	}

	/**
	 * Get: dataReferencia.
	 *
	 * @return dataReferencia
	 */
	public String getDataReferencia() {
		return dataReferencia;
	}

	/**
	 * Set: dataReferencia.
	 *
	 * @param dataReferencia the data referencia
	 */
	public void setDataReferencia(String dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	/**
	 * Get: dddContribuinte.
	 *
	 * @return dddContribuinte
	 */
	public String getDddContribuinte() {
		return dddContribuinte;
	}

	/**
	 * Set: dddContribuinte.
	 *
	 * @param dddContribuinte the ddd contribuinte
	 */
	public void setDddContribuinte(String dddContribuinte) {
		this.dddContribuinte = dddContribuinte;
	}

	/**
	 * Get: inscricaoEstadualContribuinte.
	 *
	 * @return inscricaoEstadualContribuinte
	 */
	public String getInscricaoEstadualContribuinte() {
		return inscricaoEstadualContribuinte;
	}

	/**
	 * Set: inscricaoEstadualContribuinte.
	 *
	 * @param inscricaoEstadualContribuinte the inscricao estadual contribuinte
	 */
	public void setInscricaoEstadualContribuinte(
			String inscricaoEstadualContribuinte) {
		this.inscricaoEstadualContribuinte = inscricaoEstadualContribuinte;
	}

	/**
	 * Get: numeroCotaParcela.
	 *
	 * @return numeroCotaParcela
	 */
	public String getNumeroCotaParcela() {
		return numeroCotaParcela;
	}

	/**
	 * Set: numeroCotaParcela.
	 *
	 * @param numeroCotaParcela the numero cota parcela
	 */
	public void setNumeroCotaParcela(String numeroCotaParcela) {
		this.numeroCotaParcela = numeroCotaParcela;
	}

	/**
	 * Get: numeroReferencia.
	 *
	 * @return numeroReferencia
	 */
	public String getNumeroReferencia() {
		return numeroReferencia;
	}

	/**
	 * Set: numeroReferencia.
	 *
	 * @param numeroReferencia the numero referencia
	 */
	public void setNumeroReferencia(String numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}

	/**
	 * Get: percentualReceita.
	 *
	 * @return percentualReceita
	 */
	public BigDecimal getPercentualReceita() {
		return percentualReceita;
	}

	/**
	 * Set: percentualReceita.
	 *
	 * @param percentualReceita the percentual receita
	 */
	public void setPercentualReceita(BigDecimal percentualReceita) {
		this.percentualReceita = percentualReceita;
	}

	/**
	 * Get: periodoApuracao.
	 *
	 * @return periodoApuracao
	 */
	public String getPeriodoApuracao() {
		return periodoApuracao;
	}

	/**
	 * Set: periodoApuracao.
	 *
	 * @param periodoApuracao the periodo apuracao
	 */
	public void setPeriodoApuracao(String periodoApuracao) {
		this.periodoApuracao = periodoApuracao;
	}

	/**
	 * Get: telefoneContribuinte.
	 *
	 * @return telefoneContribuinte
	 */
	public String getTelefoneContribuinte() {
		return telefoneContribuinte;
	}

	/**
	 * Set: telefoneContribuinte.
	 *
	 * @param telefoneContribuinte the telefone contribuinte
	 */
	public void setTelefoneContribuinte(String telefoneContribuinte) {
		this.telefoneContribuinte = telefoneContribuinte;
	}

	/**
	 * Get: vlAtualizacaoMonetaria.
	 *
	 * @return vlAtualizacaoMonetaria
	 */
	public BigDecimal getVlAtualizacaoMonetaria() {
		return vlAtualizacaoMonetaria;
	}

	/**
	 * Set: vlAtualizacaoMonetaria.
	 *
	 * @param vlAtualizacaoMonetaria the vl atualizacao monetaria
	 */
	public void setVlAtualizacaoMonetaria(BigDecimal vlAtualizacaoMonetaria) {
		this.vlAtualizacaoMonetaria = vlAtualizacaoMonetaria;
	}

	/**
	 * Get: vlPrincipal.
	 *
	 * @return vlPrincipal
	 */
	public BigDecimal getVlPrincipal() {
		return vlPrincipal;
	}

	/**
	 * Set: vlPrincipal.
	 *
	 * @param vlPrincipal the vl principal
	 */
	public void setVlPrincipal(BigDecimal vlPrincipal) {
		this.vlPrincipal = vlPrincipal;
	}

	/**
	 * Get: vlReceita.
	 *
	 * @return vlReceita
	 */
	public BigDecimal getVlReceita() {
		return vlReceita;
	}

	/**
	 * Set: vlReceita.
	 *
	 * @param vlReceita the vl receita
	 */
	public void setVlReceita(BigDecimal vlReceita) {
		this.vlReceita = vlReceita;
	}

	/**
	 * Get: vlTotal.
	 *
	 * @return vlTotal
	 */
	public BigDecimal getVlTotal() {
		return vlTotal;
	}

	/**
	 * Set: vlTotal.
	 *
	 * @param vlTotal the vl total
	 */
	public void setVlTotal(BigDecimal vlTotal) {
		this.vlTotal = vlTotal;
	}

	/**
	 * Get: codigoCnae.
	 *
	 * @return codigoCnae
	 */
	public String getCodigoCnae() {
		return codigoCnae;
	}

	/**
	 * Set: codigoCnae.
	 *
	 * @param codigoCnae the codigo cnae
	 */
	public void setCodigoCnae(String codigoCnae) {
		this.codigoCnae = codigoCnae;
	}

	/**
	 * Get: codigoOrgaoFavorecido.
	 *
	 * @return codigoOrgaoFavorecido
	 */
	public String getCodigoOrgaoFavorecido() {
		return codigoOrgaoFavorecido;
	}

	/**
	 * Set: codigoOrgaoFavorecido.
	 *
	 * @param codigoOrgaoFavorecido the codigo orgao favorecido
	 */
	public void setCodigoOrgaoFavorecido(String codigoOrgaoFavorecido) {
		this.codigoOrgaoFavorecido = codigoOrgaoFavorecido;
	}

	/**
	 * Get: codigoUfFavorecida.
	 *
	 * @return codigoUfFavorecida
	 */
	public String getCodigoUfFavorecida() {
		return codigoUfFavorecida;
	}

	/**
	 * Set: codigoUfFavorecida.
	 *
	 * @param codigoUfFavorecida the codigo uf favorecida
	 */
	public void setCodigoUfFavorecida(String codigoUfFavorecida) {
		this.codigoUfFavorecida = codigoUfFavorecida;
	}

	/**
	 * Get: descricaoUfFavorecida.
	 *
	 * @return descricaoUfFavorecida
	 */
	public String getDescricaoUfFavorecida() {
		return descricaoUfFavorecida;
	}

	/**
	 * Set: descricaoUfFavorecida.
	 *
	 * @param descricaoUfFavorecida the descricao uf favorecida
	 */
	public void setDescricaoUfFavorecida(String descricaoUfFavorecida) {
		this.descricaoUfFavorecida = descricaoUfFavorecida;
	}

	/**
	 * Get: nomeOrgaoFavorecido.
	 *
	 * @return nomeOrgaoFavorecido
	 */
	public String getNomeOrgaoFavorecido() {
		return nomeOrgaoFavorecido;
	}

	/**
	 * Set: nomeOrgaoFavorecido.
	 *
	 * @param nomeOrgaoFavorecido the nome orgao favorecido
	 */
	public void setNomeOrgaoFavorecido(String nomeOrgaoFavorecido) {
		this.nomeOrgaoFavorecido = nomeOrgaoFavorecido;
	}

	/**
	 * Get: numeroParcelamento.
	 *
	 * @return numeroParcelamento
	 */
	public String getNumeroParcelamento() {
		return numeroParcelamento;
	}

	/**
	 * Set: numeroParcelamento.
	 *
	 * @param numeroParcelamento the numero parcelamento
	 */
	public void setNumeroParcelamento(String numeroParcelamento) {
		this.numeroParcelamento = numeroParcelamento;
	}

	/**
	 * Get: observacaoTributo.
	 *
	 * @return observacaoTributo
	 */
	public String getObservacaoTributo() {
		return observacaoTributo;
	}

	/**
	 * Set: observacaoTributo.
	 *
	 * @param observacaoTributo the observacao tributo
	 */
	public void setObservacaoTributo(String observacaoTributo) {
		this.observacaoTributo = observacaoTributo;
	}

	/**
	 * Get: placaVeiculo.
	 *
	 * @return placaVeiculo
	 */
	public String getPlacaVeiculo() {
		return placaVeiculo;
	}

	/**
	 * Set: placaVeiculo.
	 *
	 * @param placaVeiculo the placa veiculo
	 */
	public void setPlacaVeiculo(String placaVeiculo) {
		this.placaVeiculo = placaVeiculo;
	}

	/**
	 * Get: vlAcrescimoFinanceiro.
	 *
	 * @return vlAcrescimoFinanceiro
	 */
	public BigDecimal getVlAcrescimoFinanceiro() {
		return vlAcrescimoFinanceiro;
	}

	/**
	 * Set: vlAcrescimoFinanceiro.
	 *
	 * @param vlAcrescimoFinanceiro the vl acrescimo financeiro
	 */
	public void setVlAcrescimoFinanceiro(BigDecimal vlAcrescimoFinanceiro) {
		this.vlAcrescimoFinanceiro = vlAcrescimoFinanceiro;
	}

	/**
	 * Get: vlHonorarioAdvogado.
	 *
	 * @return vlHonorarioAdvogado
	 */
	public BigDecimal getVlHonorarioAdvogado() {
		return vlHonorarioAdvogado;
	}

	/**
	 * Set: vlHonorarioAdvogado.
	 *
	 * @param vlHonorarioAdvogado the vl honorario advogado
	 */
	public void setVlHonorarioAdvogado(BigDecimal vlHonorarioAdvogado) {
		this.vlHonorarioAdvogado = vlHonorarioAdvogado;
	}

	/**
	 * Get: codigoInss.
	 *
	 * @return codigoInss
	 */
	public String getCodigoInss() {
		return codigoInss;
	}

	/**
	 * Set: codigoInss.
	 *
	 * @param codigoInss the codigo inss
	 */
	public void setCodigoInss(String codigoInss) {
		this.codigoInss = codigoInss;
	}

	/**
	 * Get: dataCompetencia.
	 *
	 * @return dataCompetencia
	 */
	public String getDataCompetencia() {
		return dataCompetencia;
	}

	/**
	 * Set: dataCompetencia.
	 *
	 * @param dataCompetencia the data competencia
	 */
	public void setDataCompetencia(String dataCompetencia) {
		this.dataCompetencia = dataCompetencia;
	}

	/**
	 * Get: vlOutrasEntidades.
	 *
	 * @return vlOutrasEntidades
	 */
	public BigDecimal getVlOutrasEntidades() {
		return vlOutrasEntidades;
	}

	/**
	 * Set: vlOutrasEntidades.
	 *
	 * @param vlOutrasEntidades the vl outras entidades
	 */
	public void setVlOutrasEntidades(BigDecimal vlOutrasEntidades) {
		this.vlOutrasEntidades = vlOutrasEntidades;
	}

	/**
	 * Get: codigoTributo.
	 *
	 * @return codigoTributo
	 */
	public String getCodigoTributo() {
		return codigoTributo;
	}

	/**
	 * Set: codigoTributo.
	 *
	 * @param codigoTributo the codigo tributo
	 */
	public void setCodigoTributo(String codigoTributo) {
		this.codigoTributo = codigoTributo;
	}

	/**
	 * Get: codigoRenavam.
	 *
	 * @return codigoRenavam
	 */
	public String getCodigoRenavam() {
		return codigoRenavam;
	}

	/**
	 * Set: codigoRenavam.
	 *
	 * @param codigoRenavam the codigo renavam
	 */
	public void setCodigoRenavam(String codigoRenavam) {
		this.codigoRenavam = codigoRenavam;
	}

	/**
	 * Get: municipioTributo.
	 *
	 * @return municipioTributo
	 */
	public String getMunicipioTributo() {
		return municipioTributo;
	}

	/**
	 * Set: municipioTributo.
	 *
	 * @param municipioTributo the municipio tributo
	 */
	public void setMunicipioTributo(String municipioTributo) {
		this.municipioTributo = municipioTributo;
	}

	/**
	 * Get: numeroNsu.
	 *
	 * @return numeroNsu
	 */
	public String getNumeroNsu() {
		return numeroNsu;
	}

	/**
	 * Set: numeroNsu.
	 *
	 * @param numeroNsu the numero nsu
	 */
	public void setNumeroNsu(String numeroNsu) {
		this.numeroNsu = numeroNsu;
	}

	/**
	 * Get: opcaoRetiradaTributo.
	 *
	 * @return opcaoRetiradaTributo
	 */
	public String getOpcaoRetiradaTributo() {
		return opcaoRetiradaTributo;
	}

	/**
	 * Set: opcaoRetiradaTributo.
	 *
	 * @param opcaoRetiradaTributo the opcao retirada tributo
	 */
	public void setOpcaoRetiradaTributo(String opcaoRetiradaTributo) {
		this.opcaoRetiradaTributo = opcaoRetiradaTributo;
	}

	/**
	 * Get: opcaoTributo.
	 *
	 * @return opcaoTributo
	 */
	public String getOpcaoTributo() {
		return opcaoTributo;
	}

	/**
	 * Set: opcaoTributo.
	 *
	 * @param opcaoTributo the opcao tributo
	 */
	public void setOpcaoTributo(String opcaoTributo) {
		this.opcaoTributo = opcaoTributo;
	}

	/**
	 * Get: ufTributo.
	 *
	 * @return ufTributo
	 */
	public String getUfTributo() {
		return ufTributo;
	}

	/**
	 * Set: ufTributo.
	 *
	 * @param ufTributo the uf tributo
	 */
	public void setUfTributo(String ufTributo) {
		this.ufTributo = ufTributo;
	}

	/**
	 * Get: bancoCartaoDepositoIdentificado.
	 *
	 * @return bancoCartaoDepositoIdentificado
	 */
	public String getBancoCartaoDepositoIdentificado() {
		return bancoCartaoDepositoIdentificado;
	}

	/**
	 * Set: bancoCartaoDepositoIdentificado.
	 *
	 * @param bancoCartaoDepositoIdentificado the banco cartao deposito identificado
	 */
	public void setBancoCartaoDepositoIdentificado(
			String bancoCartaoDepositoIdentificado) {
		this.bancoCartaoDepositoIdentificado = bancoCartaoDepositoIdentificado;
	}

	/**
	 * Get: bimCartaoDepositoIdentificado.
	 *
	 * @return bimCartaoDepositoIdentificado
	 */
	public String getBimCartaoDepositoIdentificado() {
		return bimCartaoDepositoIdentificado;
	}

	/**
	 * Set: bimCartaoDepositoIdentificado.
	 *
	 * @param bimCartaoDepositoIdentificado the bim cartao deposito identificado
	 */
	public void setBimCartaoDepositoIdentificado(
			String bimCartaoDepositoIdentificado) {
		this.bimCartaoDepositoIdentificado = bimCartaoDepositoIdentificado;
	}

	/**
	 * Get: cartaoDepositoIdentificado.
	 *
	 * @return cartaoDepositoIdentificado
	 */
	public String getCartaoDepositoIdentificado() {
		return cartaoDepositoIdentificado;
	}

	/**
	 * Set: cartaoDepositoIdentificado.
	 *
	 * @param cartaoDepositoIdentificado the cartao deposito identificado
	 */
	public void setCartaoDepositoIdentificado(String cartaoDepositoIdentificado) {
		this.cartaoDepositoIdentificado = cartaoDepositoIdentificado;
	}

	/**
	 * Get: clienteDepositoIdentificado.
	 *
	 * @return clienteDepositoIdentificado
	 */
	public String getClienteDepositoIdentificado() {
		return clienteDepositoIdentificado;
	}

	/**
	 * Set: clienteDepositoIdentificado.
	 *
	 * @param clienteDepositoIdentificado the cliente deposito identificado
	 */
	public void setClienteDepositoIdentificado(
			String clienteDepositoIdentificado) {
		this.clienteDepositoIdentificado = clienteDepositoIdentificado;
	}

	/**
	 * Get: codigoDepositante.
	 *
	 * @return codigoDepositante
	 */
	public String getCodigoDepositante() {
		return codigoDepositante;
	}

	/**
	 * Set: codigoDepositante.
	 *
	 * @param codigoDepositante the codigo depositante
	 */
	public void setCodigoDepositante(String codigoDepositante) {
		this.codigoDepositante = codigoDepositante;
	}

	/**
	 * Get: nomeDepositante.
	 *
	 * @return nomeDepositante
	 */
	public String getNomeDepositante() {
		return nomeDepositante;
	}

	/**
	 * Set: nomeDepositante.
	 *
	 * @param nomeDepositante the nome depositante
	 */
	public void setNomeDepositante(String nomeDepositante) {
		this.nomeDepositante = nomeDepositante;
	}

	/**
	 * Get: produtoDepositoIdentificado.
	 *
	 * @return produtoDepositoIdentificado
	 */
	public String getProdutoDepositoIdentificado() {
		return produtoDepositoIdentificado;
	}

	/**
	 * Set: produtoDepositoIdentificado.
	 *
	 * @param produtoDepositoIdentificado the produto deposito identificado
	 */
	public void setProdutoDepositoIdentificado(
			String produtoDepositoIdentificado) {
		this.produtoDepositoIdentificado = produtoDepositoIdentificado;
	}

	/**
	 * Get: sequenciaProdutoDepositoIdentificado.
	 *
	 * @return sequenciaProdutoDepositoIdentificado
	 */
	public String getSequenciaProdutoDepositoIdentificado() {
		return sequenciaProdutoDepositoIdentificado;
	}

	/**
	 * Set: sequenciaProdutoDepositoIdentificado.
	 *
	 * @param sequenciaProdutoDepositoIdentificado the sequencia produto deposito identificado
	 */
	public void setSequenciaProdutoDepositoIdentificado(
			String sequenciaProdutoDepositoIdentificado) {
		this.sequenciaProdutoDepositoIdentificado = sequenciaProdutoDepositoIdentificado;
	}

	/**
	 * Get: tipoDespositoIdentificado.
	 *
	 * @return tipoDespositoIdentificado
	 */
	public String getTipoDespositoIdentificado() {
		return tipoDespositoIdentificado;
	}

	/**
	 * Set: tipoDespositoIdentificado.
	 *
	 * @param tipoDespositoIdentificado the tipo desposito identificado
	 */
	public void setTipoDespositoIdentificado(String tipoDespositoIdentificado) {
		this.tipoDespositoIdentificado = tipoDespositoIdentificado;
	}

	/**
	 * Get: viaCartaoDepositoIdentificado.
	 *
	 * @return viaCartaoDepositoIdentificado
	 */
	public String getViaCartaoDepositoIdentificado() {
		return viaCartaoDepositoIdentificado;
	}

	/**
	 * Set: viaCartaoDepositoIdentificado.
	 *
	 * @param viaCartaoDepositoIdentificado the via cartao deposito identificado
	 */
	public void setViaCartaoDepositoIdentificado(
			String viaCartaoDepositoIdentificado) {
		this.viaCartaoDepositoIdentificado = viaCartaoDepositoIdentificado;
	}

	/**
	 * Get: codigoOrdemCredito.
	 *
	 * @return codigoOrdemCredito
	 */
	public String getCodigoOrdemCredito() {
		return codigoOrdemCredito;
	}

	/**
	 * Set: codigoOrdemCredito.
	 *
	 * @param codigoOrdemCredito the codigo ordem credito
	 */
	public void setCodigoOrdemCredito(String codigoOrdemCredito) {
		this.codigoOrdemCredito = codigoOrdemCredito;
	}

	/**
	 * Get: codigoPagador.
	 *
	 * @return codigoPagador
	 */
	public String getCodigoPagador() {
		return codigoPagador;
	}

	/**
	 * Set: codigoPagador.
	 *
	 * @param codigoPagador the codigo pagador
	 */
	public void setCodigoPagador(String codigoPagador) {
		this.codigoPagador = codigoPagador;
	}

	/**
	 * Get: qtdePagamentosLiberados.
	 *
	 * @return qtdePagamentosLiberados
	 */
	public Long getQtdePagamentosLiberados() {
		return qtdePagamentosLiberados;
	}

	/**
	 * Set: qtdePagamentosLiberados.
	 *
	 * @param qtdePagamentosLiberados the qtde pagamentos liberados
	 */
	public void setQtdePagamentosLiberados(Long qtdePagamentosLiberados) {
		this.qtdePagamentosLiberados = qtdePagamentosLiberados;
	}

	/**
	 * Get: vlTotalPagamentosLiberados.
	 *
	 * @return vlTotalPagamentosLiberados
	 */
	public BigDecimal getVlTotalPagamentosLiberados() {
		return vlTotalPagamentosLiberados;
	}

	/**
	 * Set: vlTotalPagamentosLiberados.
	 *
	 * @param vlTotalPagamentosLiberados the vl total pagamentos liberados
	 */
	public void setVlTotalPagamentosLiberados(
			BigDecimal vlTotalPagamentosLiberados) {
		this.vlTotalPagamentosLiberados = vlTotalPagamentosLiberados;
	}

	/**
	 * Get: logoPath.
	 *
	 * @return logoPath
	 */
	public String getLogoPath() {
		return logoPath;
	}

	/**
	 * Set: logoPath.
	 *
	 * @param logoPath the logo path
	 */
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Is exibe beneficiario.
	 *
	 * @return true, if is exibe beneficiario
	 */
	public boolean isExibeBeneficiario() {
		return exibeBeneficiario;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: descricaoEmpresa.
	 *
	 * @return descricaoEmpresa
	 */
	public String getDescricaoEmpresa() {
		return descricaoEmpresa;
	}

	/**
	 * Set: descricaoEmpresa.
	 *
	 * @param descricaoEmpresa the descricao empresa
	 */
	public void setDescricaoEmpresa(String descricaoEmpresa) {
		this.descricaoEmpresa = descricaoEmpresa;
	}

	/**
	 * Get: descricaoNumeroContrato.
	 *
	 * @return descricaoNumeroContrato
	 */
	public String getDescricaoNumeroContrato() {
		return descricaoNumeroContrato;
	}

	/**
	 * Set: descricaoNumeroContrato.
	 *
	 * @param descricaoNumeroContrato the descricao numero contrato
	 */
	public void setDescricaoNumeroContrato(String descricaoNumeroContrato) {
		this.descricaoNumeroContrato = descricaoNumeroContrato;
	}

	/**
	 * Get: descricaoSituacaoContrato.
	 *
	 * @return descricaoSituacaoContrato
	 */
	public String getDescricaoSituacaoContrato() {
		return descricaoSituacaoContrato;
	}

	/**
	 * Set: descricaoSituacaoContrato.
	 *
	 * @param descricaoSituacaoContrato the descricao situacao contrato
	 */
	public void setDescricaoSituacaoContrato(String descricaoSituacaoContrato) {
		this.descricaoSituacaoContrato = descricaoSituacaoContrato;
	}

	/**
	 * Set: exibeBeneficiario.
	 *
	 * @param exibeBeneficiario the exibe beneficiario
	 */
	public void setExibeBeneficiario(boolean exibeBeneficiario) {
		this.exibeBeneficiario = exibeBeneficiario;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasService the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
		this.consultasService = consultasService;
	}

	/**
	 * Get: descricaoAgenciaContaDebitoBancaria.
	 *
	 * @return descricaoAgenciaContaDebitoBancaria
	 */
	public String getDescricaoAgenciaContaDebitoBancaria() {
		return descricaoAgenciaContaDebitoBancaria;
	}

	/**
	 * Set: descricaoAgenciaContaDebitoBancaria.
	 *
	 * @param descricaoAgenciaContaDebitoBancaria the descricao agencia conta debito bancaria
	 */
	public void setDescricaoAgenciaContaDebitoBancaria(
			String descricaoAgenciaContaDebitoBancaria) {
		this.descricaoAgenciaContaDebitoBancaria = descricaoAgenciaContaDebitoBancaria;
	}

	/**
	 * Get: descricaoBancoContaDebito.
	 *
	 * @return descricaoBancoContaDebito
	 */
	public String getDescricaoBancoContaDebito() {
		return descricaoBancoContaDebito;
	}

	/**
	 * Set: descricaoBancoContaDebito.
	 *
	 * @param descricaoBancoContaDebito the descricao banco conta debito
	 */
	public void setDescricaoBancoContaDebito(String descricaoBancoContaDebito) {
		this.descricaoBancoContaDebito = descricaoBancoContaDebito;
	}

	/**
	 * Get: descricaoTipoContaDebito.
	 *
	 * @return descricaoTipoContaDebito
	 */
	public String getDescricaoTipoContaDebito() {
		return descricaoTipoContaDebito;
	}

	/**
	 * Set: descricaoTipoContaDebito.
	 *
	 * @param descricaoTipoContaDebito the descricao tipo conta debito
	 */
	public void setDescricaoTipoContaDebito(String descricaoTipoContaDebito) {
		this.descricaoTipoContaDebito = descricaoTipoContaDebito;
	}

	/**
	 * Get: carteira.
	 *
	 * @return carteira
	 */
	public String getCarteira() {
		return carteira;
	}

	/**
	 * Set: carteira.
	 *
	 * @param carteira the carteira
	 */
	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}

	/**
	 * Get: cdIndentificadorJudicial.
	 *
	 * @return cdIndentificadorJudicial
	 */
	public String getCdIndentificadorJudicial() {
		return cdIndentificadorJudicial;
	}

	/**
	 * Set: cdIndentificadorJudicial.
	 *
	 * @param cdIndentificadorJudicial the cd indentificador judicial
	 */
	public void setCdIndentificadorJudicial(String cdIndentificadorJudicial) {
		this.cdIndentificadorJudicial = cdIndentificadorJudicial;
	}

	/**
	 * Get: cdSituacaoTranferenciaAutomatica.
	 *
	 * @return cdSituacaoTranferenciaAutomatica
	 */
	public String getCdSituacaoTranferenciaAutomatica() {
		return cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Set: cdSituacaoTranferenciaAutomatica.
	 *
	 * @param cdSituacaoTranferenciaAutomatica the cd situacao tranferencia automatica
	 */
	public void setCdSituacaoTranferenciaAutomatica(
			String cdSituacaoTranferenciaAutomatica) {
		this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Get: dtLimiteDescontoPagamento.
	 *
	 * @return dtLimiteDescontoPagamento
	 */
	public String getDtLimiteDescontoPagamento() {
		return dtLimiteDescontoPagamento;
	}

	/**
	 * Set: dtLimiteDescontoPagamento.
	 *
	 * @param dtLimiteDescontoPagamento the dt limite desconto pagamento
	 */
	public void setDtLimiteDescontoPagamento(String dtLimiteDescontoPagamento) {
		this.dtLimiteDescontoPagamento = dtLimiteDescontoPagamento;
	}

	/**
	 * Get: nrRemessa.
	 *
	 * @return nrRemessa
	 */
	public Long getNrRemessa() {
		return nrRemessa;
	}

	/**
	 * Set: nrRemessa.
	 *
	 * @param nrRemessa the nr remessa
	 */
	public void setNrRemessa(Long nrRemessa) {
		this.nrRemessa = nrRemessa;
	}

	/**
	 * Get: descTipoFavorecido.
	 *
	 * @return descTipoFavorecido
	 */
	public String getDescTipoFavorecido() {
		return descTipoFavorecido;
	}

	/**
	 * Set: descTipoFavorecido.
	 *
	 * @param descTipoFavorecido the desc tipo favorecido
	 */
	public void setDescTipoFavorecido(String descTipoFavorecido) {
		this.descTipoFavorecido = descTipoFavorecido;
	}

	/**
	 * Get: dsTipoBeneficiario.
	 *
	 * @return dsTipoBeneficiario
	 */
	public String getDsTipoBeneficiario() {
		return dsTipoBeneficiario;
	}

	/**
	 * Set: dsTipoBeneficiario.
	 *
	 * @param dsTipoBeneficiario the ds tipo beneficiario
	 */
	public void setDsTipoBeneficiario(String dsTipoBeneficiario) {
		this.dsTipoBeneficiario = dsTipoBeneficiario;
	}

	/**
	 * Get: cdBancoOriginal.
	 *
	 * @return cdBancoOriginal
	 */
	public Integer getCdBancoOriginal() {
		return cdBancoOriginal;
	}

	/**
	 * Set: cdBancoOriginal.
	 *
	 * @param cdBancoOriginal the cd banco original
	 */
	public void setCdBancoOriginal(Integer cdBancoOriginal) {
		this.cdBancoOriginal = cdBancoOriginal;
	}

	/**
	 * Get: dsBancoOriginal.
	 *
	 * @return dsBancoOriginal
	 */
	public String getDsBancoOriginal() {
		return dsBancoOriginal;
	}

	/**
	 * Set: dsBancoOriginal.
	 *
	 * @param dsBancoOriginal the ds banco original
	 */
	public void setDsBancoOriginal(String dsBancoOriginal) {
		this.dsBancoOriginal = dsBancoOriginal;
	}

	/**
	 * Get: cdAgenciaBancariaOriginal.
	 *
	 * @return cdAgenciaBancariaOriginal
	 */
	public Integer getCdAgenciaBancariaOriginal() {
		return cdAgenciaBancariaOriginal;
	}

	/**
	 * Set: cdAgenciaBancariaOriginal.
	 *
	 * @param cdAgenciaBancariaOriginal the cd agencia bancaria original
	 */
	public void setCdAgenciaBancariaOriginal(Integer cdAgenciaBancariaOriginal) {
		this.cdAgenciaBancariaOriginal = cdAgenciaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoAgenciaOriginal.
	 *
	 * @return cdDigitoAgenciaOriginal
	 */
	public String getCdDigitoAgenciaOriginal() {
		return cdDigitoAgenciaOriginal;
	}

	/**
	 * Set: cdDigitoAgenciaOriginal.
	 *
	 * @param cdDigitoAgenciaOriginal the cd digito agencia original
	 */
	public void setCdDigitoAgenciaOriginal(String cdDigitoAgenciaOriginal) {
		this.cdDigitoAgenciaOriginal = cdDigitoAgenciaOriginal;
	}

	/**
	 * Get: dsAgenciaOriginal.
	 *
	 * @return dsAgenciaOriginal
	 */
	public String getDsAgenciaOriginal() {
		return dsAgenciaOriginal;
	}

	/**
	 * Set: dsAgenciaOriginal.
	 *
	 * @param dsAgenciaOriginal the ds agencia original
	 */
	public void setDsAgenciaOriginal(String dsAgenciaOriginal) {
		this.dsAgenciaOriginal = dsAgenciaOriginal;
	}

	/**
	 * Get: cdContaBancariaOriginal.
	 *
	 * @return cdContaBancariaOriginal
	 */
	public Long getCdContaBancariaOriginal() {
		return cdContaBancariaOriginal;
	}

	/**
	 * Set: cdContaBancariaOriginal.
	 *
	 * @param cdContaBancariaOriginal the cd conta bancaria original
	 */
	public void setCdContaBancariaOriginal(Long cdContaBancariaOriginal) {
		this.cdContaBancariaOriginal = cdContaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoContaOriginal.
	 *
	 * @return cdDigitoContaOriginal
	 */
	public String getCdDigitoContaOriginal() {
		return cdDigitoContaOriginal;
	}

	/**
	 * Set: cdDigitoContaOriginal.
	 *
	 * @param cdDigitoContaOriginal the cd digito conta original
	 */
	public void setCdDigitoContaOriginal(String cdDigitoContaOriginal) {
		this.cdDigitoContaOriginal = cdDigitoContaOriginal;
	}

	/**
	 * Get: dsTipoContaOriginal.
	 *
	 * @return dsTipoContaOriginal
	 */
	public String getDsTipoContaOriginal() {
		return dsTipoContaOriginal;
	}

	/**
	 * Set: dsTipoContaOriginal.
	 *
	 * @param dsTipoContaOriginal the ds tipo conta original
	 */
	public void setDsTipoContaOriginal(String dsTipoContaOriginal) {
		this.dsTipoContaOriginal = dsTipoContaOriginal;
	}

	/**
	 * Get: dtDevolucaoEstorno.
	 *
	 * @return dtDevolucaoEstorno
	 */
	public String getDtDevolucaoEstorno() {
		return dtDevolucaoEstorno;
	}

	/**
	 * Set: dtDevolucaoEstorno.
	 *
	 * @param dtDevolucaoEstorno the dt devolucao estorno
	 */
	public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
		this.dtDevolucaoEstorno = dtDevolucaoEstorno;
	}

	/**
	 * Get: dtPagamentoDet.
	 *
	 * @return dtPagamentoDet
	 */
	public String getDtPagamentoDet() {
		return dtPagamentoDet;
	}

	/**
	 * Set: dtPagamentoDet.
	 *
	 * @param dtPagamentoDet the dt pagamento det
	 */
	public void setDtPagamentoDet(String dtPagamentoDet) {
		this.dtPagamentoDet = dtPagamentoDet;
	}

	/**
	 * Get: dsRastreado.
	 *
	 * @return dsRastreado
	 */
	public String getDsRastreado() {
		return dsRastreado;
	}

	/**
	 * Set: dsRastreado.
	 *
	 * @param dsRastreado the ds rastreado
	 */
	public void setDsRastreado(String dsRastreado) {
		this.dsRastreado = dsRastreado;
	}

	/**
	 * Get: dtLimitePagamento.
	 *
	 * @return dtLimitePagamento
	 */
	public String getDtLimitePagamento() {
		return dtLimitePagamento;
	}

	/**
	 * Set: dtLimitePagamento.
	 *
	 * @param dtLimitePagamento the dt limite pagamento
	 */
	public void setDtLimitePagamento(String dtLimitePagamento) {
		this.dtLimitePagamento = dtLimitePagamento;
	}

	/**
	 * Get: cdOperacaoDcom.
	 *
	 * @return cdOperacaoDcom
	 */
	public Long getCdOperacaoDcom() {
		return cdOperacaoDcom;
	}

	/**
	 * Set: cdOperacaoDcom.
	 *
	 * @param cdOperacaoDcom the cd operacao dcom
	 */
	public void setCdOperacaoDcom(Long cdOperacaoDcom) {
		this.cdOperacaoDcom = cdOperacaoDcom;
	}

	/**
	 * Get: dsSituacaoDcom.
	 *
	 * @return dsSituacaoDcom
	 */
	public String getDsSituacaoDcom() {
		return dsSituacaoDcom;
	}

	/**
	 * Set: dsSituacaoDcom.
	 *
	 * @param dsSituacaoDcom the ds situacao dcom
	 */
	public void setDsSituacaoDcom(String dsSituacaoDcom) {
		this.dsSituacaoDcom = dsSituacaoDcom;
	}

	/**
	 * Get: exibeDcom.
	 *
	 * @return exibeDcom
	 */
	public Boolean getExibeDcom() {
		return exibeDcom;
	}

	/**
	 * Set: exibeDcom.
	 *
	 * @param exibeDcom the exibe dcom
	 */
	public void setExibeDcom(Boolean exibeDcom) {
		this.exibeDcom = exibeDcom;
	}

	/**
	 * Get: numControleInternoLote.
	 *
	 * @return numControleInternoLote
	 */
	public Long getNumControleInternoLote() {
		return numControleInternoLote;
	}

	/**
	 * Set: numControleInternoLote.
	 *
	 * @param numControleInternoLote the num controle interno lote
	 */
	public void setNumControleInternoLote(Long numControleInternoLote) {
		this.numControleInternoLote = numControleInternoLote;
	}
}