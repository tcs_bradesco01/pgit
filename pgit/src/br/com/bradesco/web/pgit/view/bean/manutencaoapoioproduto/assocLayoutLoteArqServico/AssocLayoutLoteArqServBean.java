/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assocLayoutLoteArqServico
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 09/02/2015
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assocLayoutLoteArqServico;

import static br.com.bradesco.web.pgit.utils.SiteUtil.isEquals;
import static br.com.bradesco.web.pgit.utils.SiteUtil.isNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerService;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ConsultarAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ConsultarAssVersaoLayoutLoteServicoSaidaDto;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssVersaoLayoutLoteServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssVersaoLayoutLoteServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssVersaoLayoutLoteServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssVersaoLayoutLoteServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoCnabSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoArquivoRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLoteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLoteSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.selectitem.SelectItemUtils;

/**
 * Nome: AssocLayoutLoteArqServBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class AssocLayoutLoteArqServBean {

	private static final String CNAB0240_BRADESCO = "CNAB0240-BRADESCO";

	/** Atributo CON_MANTER_ASSOC_RETORNO_LAY_ARQ_PROD_SERV. */
	private static final String CON_MANTER_VERSAO = "conManterAssocVersaoLayLoteAqrServ";

	/** Atributo COD_LAYOUT_CNAB0240_BRADESCO. */
	private static final Integer COD_LAYOUT_CNAB0240_BRADESCO = 14;

	/** Atributo assRetLayArqProSerServiceImpl. */
	private IAssRetLayArqProSerService assRetLayArqProSerServiceImpl;

	/** Atributo filtroTipoServico. */
	private Integer filtroTipoServico;

	/** Atributo filtroTipoArquivoRetorno. */
	private Integer filtroTipoArquivoRetorno;

	/** Atributo filtroTipoLayoutArquivo. */
	private Integer filtroTipoLayoutArquivo;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;

	/** Atributo cdTipoRetorno. */
	private Integer cdTipoRetorno;

	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;

	/** Atributo dsTipoServico. */
	private String dsTipoServico;

	/** Atributo dsTipoRetorno. */
	private String dsTipoRetorno;

	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo listaTipoLote. */
	private List<SelectItem> listaTipoLote = new ArrayList<SelectItem>();

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo btoAcionado. */
	private Boolean btoAcionado;

	/** Atributo listaGrid. */
	private List<ListarAssRetLayArqProSerSaidaDTO> listaGrid;

	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();

	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();

	/** Atributo listaTipoArquivoRetorno. */
	private List<SelectItem> listaTipoArquivoRetorno = new ArrayList<SelectItem>();

	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();

	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();

	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer, String> listaTipoLayoutArquivoHash = new HashMap<Integer, String>();

	/** Atributo listaTipoArquivoRetornoHash. */
	private Map<Integer, String> listaTipoArquivoRetornoHash = new HashMap<Integer, String>();

	/** Atributo listaTipoServicoCnab. */
	private List<SelectItem> listaTipoServicoCnab = new ArrayList<SelectItem>();

	/** Atributo listaTipoServicoCnabHash. */
	private Map<Integer, String> listaTipoServicoCnabHash = new HashMap<Integer, String>();

	/** Atributo nrVersaoLayoutArquivo. */
	private Integer nrVersaoLayoutArquivo;

	/** Atributo cdTipoLoteLayout. */
	private Integer cdTipoLoteLayout;

	/** Atributo dsTipoLoteLayout. */
	private String dsTipoLoteLayout;

	/** Atributo nrVersaoLoteLayout. */
	private Integer nrVersaoLoteLayout;

	/** Atributo cdTipoServicoCnab. */
	private Integer cdTipoServicoCnab;

	/** Atributo dsTipoServicoCnab. */
	private String dsTipoServicoCnab;

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo descTipoLayoutArqCompl. */
	private String descTipoLayoutArqCompl;

	/** Atributo descTipoLoteLayoutCompl. */
	private String descTipoLoteLayoutCompl;

	/** Atributo descTipoServicoCnabCompl. */
	private String descTipoServicoCnabCompl;

	/** Atributo descLayoutArquivo. */
	private String descLayoutArquivo;

	private IncluirAssVersaoLayoutLoteServicoSaidaDTO saidaLayoutLoteServ;

	private ExcluirAssVersaoLayoutLoteServicoSaidaDTO saidaExclusaoLayoutLoteServ;

	private ListarAssVersaoLayoutLoteServicoSaidaDTO saidaListarLayoutLoteServ;

	private List<ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO> listaLayoutLoteServ;

	private ConsultarAssVersaoLayoutLoteServicoSaidaDto saidaConsultarLayoutLoteServ;

	ConsultarAssVersaoLayoutLoteServicoEntradaDTO entradaConsultarVersao = new ConsultarAssVersaoLayoutLoteServicoEntradaDTO();

	ListarAssVersaoLayoutLoteServicoEntradaDTO entradaListarLote = new ListarAssVersaoLayoutLoteServicoEntradaDTO();

	IncluirAssVersaoLayoutLoteServicoEntradaDTO entradaIncluirLoteServ = new IncluirAssVersaoLayoutLoteServicoEntradaDTO();

	/** Atributo saidaTipoLayoutArquivoSaidaDTO. */
	private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;

	/**
	 * Nome: limparCampos
	 * 
	 */
	public void limparCampos() {
		limpar();
		setBtoAcionado(false);
		entradaListarLote.setCdTipoLayoutArquivo(null);
		entradaListarLote.setNrVersaoLayoutArquivo(null);
		entradaListarLote.setCdTipoLoteLayout(null);
		entradaListarLote.setNrVersaoLoteLayout(null);
		entradaListarLote.setCdTipoServicoCnab(null);
	}
	
	/**
	 * Nome: limpar
	 * 
	 */
	public void limpar() {
		setItemSelecionadoLista(null);
		setListaLayoutLoteServ(null);
	}

	/**
	 * Nome: limparCamposTelaInclusao
	 * 
	 */
	private void limparCamposTelaInclusao() {
		entradaIncluirLoteServ.setNrVersaoLayoutArquivo(null);
		entradaIncluirLoteServ.setCdTipoLoteLayout(null);
		entradaIncluirLoteServ.setNrVersaoLoteLayout(null);
		entradaIncluirLoteServ.setCdTipoServicoCnab(null);
	}

	/**
	 * Iniciar tela.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		setFiltroTipoServico(0);
		this.setFiltroTipoArquivoRetorno(0);
		this.setFiltroTipoLayoutArquivo(0);
		setListaGrid(null);
		setItemSelecionadoLista(null);
		preencherListaTipoServico();
		preencherListaTipoLayoutArquivo();
		preencherListaTipoRetorno();
		setBtoAcionado(false);
		setEntradaListarLote(new ListarAssVersaoLayoutLoteServicoEntradaDTO());
		listarTipoLote();
		listarComboServicoCnab();
		setListaLayoutLoteServ(new ArrayList<ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO>());
		limparCampos();

		setDescLayoutArquivo(CNAB0240_BRADESCO);
	}

	/**
	 * Nome: preencherListaTipoServico
	 * 
	 * @exception PdcAdapterFunctionalException
	 */
	public void preencherListaTipoServico() {
		try {
			this.listaTipoServico = new ArrayList<SelectItem>();

			List<ListarServicosSaidaDTO> listaServico = new ArrayList<ListarServicosSaidaDTO>();

			listaServico = comboService.listarTipoServicos(0);

			listaTipoServicoHash.clear();
			for (ListarServicosSaidaDTO combo : listaServico) {
				listaTipoServicoHash.put(combo.getCdServico(), combo
						.getDsServico());
				listaTipoServico.add(new SelectItem(combo.getCdServico(), combo
						.getDsServico()));
			}
		} catch (PdcAdapterFunctionalException e) {
			listaTipoServico = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Nome: listarComboServicoCnab
	 * 
	 * @exception PdcAdapterFunctionalException
	 */
	public void listarComboServicoCnab() {
		try {
			listaTipoServicoCnab = new ArrayList<SelectItem>();

			List<ListarServicoCnabSaidaDTO> list = getComboService()
					.listarServicoCnab();

			listaTipoServicoCnabHash.clear();
			for (ListarServicoCnabSaidaDTO combo : list) {
				listaTipoServicoCnab.add(new SelectItem(combo
						.getCdTipoServicoCnab(), combo.getDsTipoServicoCnab()));
				listaTipoServicoCnabHash.put(combo.getCdTipoServicoCnab(),
						combo.getDsTipoServicoCnab());
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			listaTipoServicoCnab = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Nome: incluirAssVersaoLayoutLoteServico
	 * 
	 * @exception PdcAdapterFunctionalException
	 */
	public void incluirAssVersaoLayoutLoteServico() {
		try {
			entradaIncluirLoteServ
					.setCdTipoLayoutArquivo(COD_LAYOUT_CNAB0240_BRADESCO);
			// entrada.setNrVersaoLayoutArquivo(PgitUtil
			// .verificaIntegerNulo(entradaIncluirLoteServ
			// .getNrVersaoLayoutArquivo()));
			// entrada.setCdTipoLoteLayout(PgitUtil
			// .verificaIntegerNulo(entradaIncluirLoteServ
			// .getCdTipoLoteLayout()));
			// entrada.setNrVersaoLoteLayout(PgitUtil
			// .verificaIntegerNulo(entradaIncluirLoteServ
			// .getNrVersaoLoteLayout()));
			// entrada.setCdTipoServicoCnab(PgitUtil
			// .verificaIntegerNulo(entradaIncluirLoteServ
			// .getCdTipoServicoCnab()));

			saidaLayoutLoteServ = getAssRetLayArqProSerServiceImpl()
					.incluirAssVersaoLayoutLoteServico(entradaIncluirLoteServ);
			BradescoFacesUtils.addInfoModalMessage("("
					+ saidaLayoutLoteServ.getCodMensagem() + ") "
					+ saidaLayoutLoteServ.getMensagem(), CON_MANTER_VERSAO,
					BradescoViewExceptionActionType.ACTION, false);
			listarAssVersaoLayoutLoteServicoAposInclusao();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Nome: listarAssVersaoLayoutLoteServicoAposInclusao
	 * 
	 * @exception PdcAdapterFunctionalException
	 */
	public void listarAssVersaoLayoutLoteServicoAposInclusao() {
		try {
			entradaListarLote = new ListarAssVersaoLayoutLoteServicoEntradaDTO();
			entradaListarLote.setCdTipoLayoutArquivo(entradaIncluirLoteServ
					.getCdTipoLayoutArquivo());
			entradaListarLote.setCdTipoLoteLayout(entradaIncluirLoteServ
					.getCdTipoLoteLayout());
			entradaListarLote.setCdTipoServicoCnab(entradaIncluirLoteServ
					.getCdTipoServicoCnab());
			entradaListarLote.setNrVersaoLayoutArquivo(entradaIncluirLoteServ
					.getNrVersaoLayoutArquivo());
			entradaListarLote.setNrVersaoLoteLayout(entradaIncluirLoteServ
					.getNrVersaoLoteLayout());

			saidaListarLayoutLoteServ = getAssRetLayArqProSerServiceImpl()
					.listarAssVersaoLayoutLoteServico(entradaListarLote);
			listaLayoutLoteServ = saidaListarLayoutLoteServ.getOcorrencias();

			for (int i = 0; i <= this.getListaLayoutLoteServ().size(); i++) {
				listaControle.add(new SelectItem(i, " "));
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		setItemSelecionadoLista(null);
	}

	/**
	 * Nome: excluirAssVersaoLayoutLoteServico
	 * 
	 * @exception PdcAdapterFunctionalException
	 */
	public void excluirAssVersaoLayoutLoteServico() {
		ExcluirAssVersaoLayoutLoteServicoEntradaDTO entrada = new ExcluirAssVersaoLayoutLoteServicoEntradaDTO();
		try {
			entrada.setCdTipoLayoutArquivo(PgitUtil
					.verificaIntegerNulo(saidaConsultarLayoutLoteServ
							.getCdTipoLayoutArquivo()));
			entrada.setNrVersaoLayoutArquivo(PgitUtil
					.verificaIntegerNulo(saidaConsultarLayoutLoteServ
							.getNrVersaoLayoutArquivo()));
			entrada.setCdTipoLoteLayout(PgitUtil
					.verificaIntegerNulo(saidaConsultarLayoutLoteServ
							.getCdTipoLoteLayout()));
			entrada.setNrVersaoLoteLayout(PgitUtil
					.verificaIntegerNulo(saidaConsultarLayoutLoteServ
							.getNrVersaoLoteLayout()));
			entrada.setCdTipoServicoCnab(PgitUtil
					.verificaIntegerNulo(saidaConsultarLayoutLoteServ
							.getCdTipoServicoCnab()));

			saidaExclusaoLayoutLoteServ = getAssRetLayArqProSerServiceImpl()
					.excluirAssVersaoLayoutLoteServico(entrada);
			BradescoFacesUtils.addInfoModalMessage("("
					+ saidaExclusaoLayoutLoteServ.getCodMensagem() + ") "
					+ saidaExclusaoLayoutLoteServ.getMensagem(),
					CON_MANTER_VERSAO, BradescoViewExceptionActionType.ACTION,
					false);
			listaLayoutLoteServ = new ArrayList<ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO>();
			listarAssVersaoLayoutLoteServico();
			limparCampos();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Nome: avancarConfirmarIncluir
	 * 
	 * @return
	 * 
	 */
	public String avancarConfirmarIncluir() {
		obterDescConfIncluir();

		return "AVANCAR";
	}

	/**
	 * Nome: obterDescConfIncluir
	 * 
	 */
	public void obterDescConfIncluir() {
		StringBuilder descTipoLayoutArq = new StringBuilder();
		descTipoLayoutArq.append(SelectItemUtils.obterLabelSelecItem(
				listaTipoLayoutArquivo, COD_LAYOUT_CNAB0240_BRADESCO));
		setDescTipoLayoutArqCompl(descTipoLayoutArq.toString());

		StringBuilder descTipoLoteLayout = new StringBuilder();
		descTipoLoteLayout.append(SelectItemUtils.obterLabelSelecItem(
				listaTipoLote, entradaIncluirLoteServ.getCdTipoLoteLayout()));
		setDescTipoLoteLayoutCompl(descTipoLoteLayout.toString());

		StringBuilder descTipoServicoCnab = new StringBuilder();
		descTipoServicoCnab.append(SelectItemUtils.obterLabelSelecItem(
				listaTipoServicoCnab, entradaIncluirLoteServ
						.getCdTipoServicoCnab()));
		setDescTipoServicoCnabCompl(descTipoServicoCnab.toString());
	}

	/**
	 * Nome: listarTipoLote
	 * 
	 * @exception PdcAdapterFunctionalException
	 */
	public void listarTipoLote() {
		try {
			listaTipoLote = new ArrayList<SelectItem>();
			List<TipoLoteSaidaDTO> listaTipoLoteAux = new ArrayList<TipoLoteSaidaDTO>();
			TipoLoteEntradaDTO entrada = new TipoLoteEntradaDTO();
			entrada.setCdSituacaoVinculacaoConta(0);

			listaTipoLoteAux = getComboService().listarTipoLote(entrada);

			for (TipoLoteSaidaDTO combo : listaTipoLoteAux) {
				listaTipoLote.add(new SelectItem(combo.getCdTipoLoteLayout(),
						combo.getDsTipoLoteLayout()));
			}
		} catch (PdcAdapterFunctionalException p) {
			listaTipoLote = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Nome: avancarExcluir
	 * 
	 * @return
	 */
	public String avancarExcluir() {
		consultarAssVersaoLayoutLoteServico();
		return "EXCLUIR";
	}

	/**
	 * Nome: avancarDetalhar
	 * 
	 * @return
	 */
	public String avancarDetalhar() {
		consultarAssVersaoLayoutLoteServico();
		return "DETALHAR";
	}

	/**
	 * Nome: consultarAssVersaoLayoutLoteServico
	 * 
	 * @exception PdcAdapterFunctionalException
	 */
	public void consultarAssVersaoLayoutLoteServico() {

		ConsultarAssVersaoLayoutLoteServicoEntradaDTO entradaConsultarVersao = new ConsultarAssVersaoLayoutLoteServicoEntradaDTO();
		ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO itemLista = getListaLayoutLoteServ()
				.get(getItemSelecionadoLista());

		try {
			entradaConsultarVersao.setCdTipoLayoutArquivo(PgitUtil
					.verificaIntegerNulo(itemLista.getCdTipoLayoutArquivo()));
			entradaConsultarVersao.setNrVersaoLayoutArquivo(PgitUtil
					.verificaIntegerNulo(itemLista.getNrVersaoLayoutArquivo()));
			entradaConsultarVersao.setCdTipoLoteLayout(PgitUtil
					.verificaIntegerNulo(itemLista.getCdTipoLoteLayout()));
			entradaConsultarVersao.setNrVersaoLoteLayout(PgitUtil
					.verificaIntegerNulo(itemLista.getNrVersaoLoteLayout()));
			entradaConsultarVersao.setCdTipoServicoCnab(PgitUtil
					.verificaIntegerNulo(itemLista.getCdTipoServicoCnab()));

			saidaConsultarLayoutLoteServ = getAssRetLayArqProSerServiceImpl()
					.ConsultarAssVersaoLayoutLoteServico(entradaConsultarVersao);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

	}

	/**
	 * Nome: listarAssVersaoLayoutLoteServico
	 * 
	 * @exception PdcAdapterFunctionalException
	 */
	public void listarAssVersaoLayoutLoteServico() {
		try {
			entradaListarLote
					.setCdTipoLayoutArquivo(COD_LAYOUT_CNAB0240_BRADESCO);

			saidaListarLayoutLoteServ = getAssRetLayArqProSerServiceImpl()
					.listarAssVersaoLayoutLoteServico(entradaListarLote);
			listaLayoutLoteServ = saidaListarLayoutLoteServ.getOcorrencias();

			for (int i = 0; i <= this.getListaLayoutLoteServ().size(); i++) {
				listaControle.add(new SelectItem(i, " "));
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		setItemSelecionadoLista(null);
	}

	/**
	 * Nome: isDesabilitaBotaoConsultar
	 * 
	 * @return
	 */
	public boolean isDesabilitaBotaoConsultar() {
		if (isNull(entradaListarLote.getCdTipoLoteLayout())
				&& isNull(entradaListarLote.getCdTipoServicoCnab())) {
			return true;
		}

		return isEquals(entradaListarLote.getCdTipoLoteLayout(), 0)
				&& isEquals(entradaListarLote.getCdTipoServicoCnab(), 0);

	}

	/**
	 * Nome: preencherListaTipoLayoutArquivo
	 * 
	 * @exception PdcAdapterFunctionalException
	 */
	public void preencherListaTipoLayoutArquivo() {
		try {
			this.listaTipoLayoutArquivo = new ArrayList<SelectItem>();

			List<TipoLayoutArquivoSaidaDTO> listaTipoLayoutArquivoSaida = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			TipoLayoutArquivoEntradaDTO tipoLayoutArquivoEntrada = new TipoLayoutArquivoEntradaDTO();
			tipoLayoutArquivoEntrada.setCdSituacaoVinculacaoConta(0);

			saidaTipoLayoutArquivoSaidaDTO = comboService
					.listarTipoLayoutArquivo(tipoLayoutArquivoEntrada);
			listaTipoLayoutArquivoSaida = saidaTipoLayoutArquivoSaidaDTO
					.getOcorrencias();

			listaTipoLayoutArquivoHash.clear();
			for (TipoLayoutArquivoSaidaDTO combo : listaTipoLayoutArquivoSaida) {
				listaTipoLayoutArquivoHash.put(combo.getCdTipoLayoutArquivo(),
						combo.getDsTipoLayoutArquivo());
				listaTipoLayoutArquivo.add(new SelectItem(combo
						.getCdTipoLayoutArquivo(), combo
						.getDsTipoLayoutArquivo()));

			}
		} catch (PdcAdapterFunctionalException e) {
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Nome: isDesabilitaBtnAvancarInc
	 * 
	 * @return
	 */
	public boolean isDesabilitaBtnAvancarInc() {
		if (isNull(entradaIncluirLoteServ.getNrVersaoLayoutArquivo())
				|| isNull(entradaIncluirLoteServ.getCdTipoLoteLayout())
				|| isNull(entradaIncluirLoteServ.getNrVersaoLoteLayout())
				|| isNull(entradaIncluirLoteServ.getCdTipoServicoCnab())) {

			return true;
		}

		return isEquals(entradaIncluirLoteServ.getNrVersaoLayoutArquivo(), 0)
				|| isEquals(entradaIncluirLoteServ.getCdTipoLoteLayout(), 0)
				|| isEquals(entradaIncluirLoteServ.getNrVersaoLoteLayout(), 0)
				|| isEquals(entradaIncluirLoteServ.getCdTipoServicoCnab(), 0);

	}

	/**
	 * Nome: preencherListaTipoRetorno
	 * 
	 * @exception PdcAdapterFunctionalException
	 */
	public void preencherListaTipoRetorno() {
		try {
			this.listaTipoArquivoRetorno = new ArrayList<SelectItem>();

			List<ListarTipoArquivoRetornoSaidaDTO> listaTipoArquivoRetornoSaida = new ArrayList<ListarTipoArquivoRetornoSaidaDTO>();

			listaTipoArquivoRetornoSaida = comboService
					.listarTipoArquivoRetorno();

			listaTipoArquivoRetornoHash.clear();
			for (ListarTipoArquivoRetornoSaidaDTO combo : listaTipoArquivoRetornoSaida) {
				listaTipoArquivoRetornoHash.put(
						combo.getCdTipoArquivoRetorno(), combo
								.getDsTipoArquivoRetorno());
				listaTipoArquivoRetorno.add(new SelectItem(combo
						.getCdTipoArquivoRetorno(), combo
						.getDsTipoArquivoRetorno()));
			}
		} catch (PdcAdapterFunctionalException e) {
			listaTipoArquivoRetorno = new ArrayList<SelectItem>();
		}

	}

	/**
	 * Nome: carregaLista
	 * 
	 * @exception PdcAdapterFunctionalException
	 */
	public void carregaLista() {

		try {

			ListarAssRetLayArqProSerEntradaDTO entradaDTO = new ListarAssRetLayArqProSerEntradaDTO();
			entradaDTO
					.setTipoServico(getFiltroTipoServico() != null ? getFiltroTipoServico()
							: 0);
			entradaDTO
					.setTipoArquivoRetorno(getFiltroTipoArquivoRetorno() != null ? getFiltroTipoArquivoRetorno()
							: 0);
			entradaDTO
					.setTipoLayoutArquivo(getFiltroTipoLayoutArquivo() != null ? getFiltroTipoLayoutArquivo()
							: 0);

			setListaGrid(getAssRetLayArqProSerServiceImpl()
					.listarAssRetLayArqProSer(entradaDTO));
			setBtoAcionado(true);
			for (int i = 0; i <= this.getListaGrid().size(); i++) {
				listaControle.add(new SelectItem(i, " "));
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(p.getMessage(), false);
			setListaGrid(null);

		}
		setItemSelecionadoLista(null);
	}

	/**
	 * Incluir.
	 * 
	 * @return the string
	 */
	public String incluir() {
		setCdTipoServico(0);
		setCdTipoRetorno(0);
		setCdTipoLayoutArquivo(0);
		preencherListaTipoRetorno();
		entradaIncluirLoteServ = new IncluirAssVersaoLayoutLoteServicoEntradaDTO();
		setBtoAcionado(false);

		return "INCLUIR";
	}

	/**
	 * Avancar incluir.
	 * 
	 * @return the string
	 */
	public String avancarIncluir() {
		setDsTipoServico((String) listaTipoServicoHash.get(getCdTipoServico()));
		setDsTipoRetorno((String) listaTipoArquivoRetornoHash
				.get(getCdTipoRetorno()));
		setDsTipoLayoutArquivo((String) listaTipoLayoutArquivoHash
				.get(getCdTipoLayoutArquivo()));
		return "AVANCARINCLUIR";
	}

	/**
	 * Voltar.
	 * 
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoLista(null);
		return "VOLTAR";
	}

	/**
	 * Voltar incluir.
	 * 
	 * @return the string
	 */
	public String voltarIncluir() {
		limparCampos();
		limparCamposTelaInclusao();

		return "VOLTAR";
	}

	/**
	 * Get: btoAcionado.
	 * 
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 * 
	 * @param btoAcionado
	 *            the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	public ExcluirAssVersaoLayoutLoteServicoSaidaDTO getSaidaExclusaoLayoutLoteServ() {
		return saidaExclusaoLayoutLoteServ;
	}

	public void setSaidaExclusaoLayoutLoteServ(
			ExcluirAssVersaoLayoutLoteServicoSaidaDTO saidaExclusaoLayoutLoteServ) {
		this.saidaExclusaoLayoutLoteServ = saidaExclusaoLayoutLoteServ;
	}

	public IncluirAssVersaoLayoutLoteServicoSaidaDTO getSaidaLayoutLoteServ() {
		return saidaLayoutLoteServ;
	}

	public void setSaidaLayoutLoteServ(
			IncluirAssVersaoLayoutLoteServicoSaidaDTO saidaLayoutLoteServ) {
		this.saidaLayoutLoteServ = saidaLayoutLoteServ;
	}

	public ListarAssVersaoLayoutLoteServicoSaidaDTO getSaidaListarLayoutLoteServ() {
		return saidaListarLayoutLoteServ;
	}

	public void setSaidaListarLayoutLoteServ(
			ListarAssVersaoLayoutLoteServicoSaidaDTO saidaListarLayoutLoteServ) {
		this.saidaListarLayoutLoteServ = saidaListarLayoutLoteServ;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 * 
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 * 
	 * @param cdTipoLayoutArquivo
	 *            the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdTipoRetorno.
	 * 
	 * @return cdTipoRetorno
	 */
	public Integer getCdTipoRetorno() {
		return cdTipoRetorno;
	}

	/**
	 * Set: cdTipoRetorno.
	 * 
	 * @param cdTipoRetorno
	 *            the cd tipo retorno
	 */
	public void setCdTipoRetorno(Integer cdTipoRetorno) {
		this.cdTipoRetorno = cdTipoRetorno;
	}

	/**
	 * Get: cdTipoServico.
	 * 
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}

	/**
	 * Set: cdTipoServico.
	 * 
	 * @param cdTipoServico
	 *            the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}

	/**
	 * Get: complementoInclusao.
	 * 
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 * 
	 * @param complementoInclusao
	 *            the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 * 
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 * 
	 * @param complementoManutencao
	 *            the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 * 
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 * 
	 * @param dataHoraInclusao
	 *            the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 * 
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 * 
	 * @param dataHoraManutencao
	 *            the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 * 
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Set: dsTipoLayoutArquivo.
	 * 
	 * @param dsTipoLayoutArquivo
	 *            the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Get: dsTipoRetorno.
	 * 
	 * @return dsTipoRetorno
	 */
	public String getDsTipoRetorno() {
		return dsTipoRetorno;
	}

	/**
	 * Set: dsTipoRetorno.
	 * 
	 * @param dsTipoRetorno
	 *            the ds tipo retorno
	 */
	public void setDsTipoRetorno(String dsTipoRetorno) {
		this.dsTipoRetorno = dsTipoRetorno;
	}

	/**
	 * Get: dsTipoServico.
	 * 
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 * 
	 * @param dsTipoServico
	 *            the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: tipoCanalInclusao.
	 * 
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 * 
	 * @param tipoCanalInclusao
	 *            the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 * 
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 * 
	 * @param tipoCanalManutencao
	 *            the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 * 
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 * 
	 * @param usuarioInclusao
	 *            the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 * 
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 * 
	 * @param usuarioManutencao
	 *            the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: listaControle.
	 * 
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 * 
	 * @param listaControle
	 *            the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: listaGrid.
	 * 
	 * @return listaGrid
	 */
	public List<ListarAssRetLayArqProSerSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 * 
	 * @param listaGrid
	 *            the lista grid
	 */
	public void setListaGrid(List<ListarAssRetLayArqProSerSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: assRetLayArqProSerServiceImpl.
	 * 
	 * @return assRetLayArqProSerServiceImpl
	 */
	public IAssRetLayArqProSerService getAssRetLayArqProSerServiceImpl() {
		return assRetLayArqProSerServiceImpl;
	}

	/**
	 * Set: assRetLayArqProSerServiceImpl.
	 * 
	 * @param assRetLayArqProSerServiceImpl
	 *            the ass ret lay arq pro ser service impl
	 */
	public void setAssRetLayArqProSerServiceImpl(
			IAssRetLayArqProSerService assRetLayArqProSerServiceImpl) {
		this.assRetLayArqProSerServiceImpl = assRetLayArqProSerServiceImpl;
	}

	/**
	 * Get: listaTipoArquivoRetornoHash.
	 * 
	 * @return listaTipoArquivoRetornoHash
	 */
	public Map<Integer, String> getListaTipoArquivoRetornoHash() {
		return listaTipoArquivoRetornoHash;
	}

	/**
	 * Set lista tipo arquivo retorno hash.
	 * 
	 * @param listaTipoArquivoRetornoHash
	 *            the lista tipo arquivo retorno hash
	 */
	public void setListaTipoArquivoRetornoHash(
			Map<Integer, String> listaTipoArquivoRetornoHash) {
		this.listaTipoArquivoRetornoHash = listaTipoArquivoRetornoHash;
	}

	/**
	 * Get: listaTipoLayoutArquivoHash.
	 * 
	 * @return listaTipoLayoutArquivoHash
	 */
	public Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}

	/**
	 * Set lista tipo layout arquivo hash.
	 * 
	 * @param listaTipoLayoutArquivoHash
	 *            the lista tipo layout arquivo hash
	 */
	public void setListaTipoLayoutArquivoHash(
			Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}

	/**
	 * Get: listaTipoServicoHash.
	 * 
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 * 
	 * @param listaTipoServicoHash
	 *            the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(
			Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: comboService.
	 * 
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 * 
	 * @param comboService
	 *            the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: filtroTipoServico.
	 * 
	 * @return filtroTipoServico
	 */
	public Integer getFiltroTipoServico() {
		return filtroTipoServico;
	}

	/**
	 * Set: filtroTipoServico.
	 * 
	 * @param filtroTipoServico
	 *            the filtro tipo servico
	 */
	public void setFiltroTipoServico(Integer filtroTipoServico) {
		this.filtroTipoServico = filtroTipoServico;
	}

	/**
	 * Get: listaTipoServico.
	 * 
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 * 
	 * @param listaTipoServico
	 *            the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: filtroTipoArquivoRetorno.
	 * 
	 * @return filtroTipoArquivoRetorno
	 */
	public Integer getFiltroTipoArquivoRetorno() {
		return filtroTipoArquivoRetorno;
	}

	/**
	 * Set: filtroTipoArquivoRetorno.
	 * 
	 * @param filtroTipoArquivoRetorno
	 *            the filtro tipo arquivo retorno
	 */
	public void setFiltroTipoArquivoRetorno(Integer filtroTipoArquivoRetorno) {
		this.filtroTipoArquivoRetorno = filtroTipoArquivoRetorno;
	}

	/**
	 * Get: listaTipoArquivoRetorno.
	 * 
	 * @return listaTipoArquivoRetorno
	 */
	public List<SelectItem> getListaTipoArquivoRetorno() {
		return listaTipoArquivoRetorno;
	}

	/**
	 * Set: listaTipoArquivoRetorno.
	 * 
	 * @param listaTipoArquivoRetorno
	 *            the lista tipo arquivo retorno
	 */
	public void setListaTipoArquivoRetorno(
			List<SelectItem> listaTipoArquivoRetorno) {
		this.listaTipoArquivoRetorno = listaTipoArquivoRetorno;
	}

	/**
	 * Get: filtroTipoLayoutArquivo.
	 * 
	 * @return filtroTipoLayoutArquivo
	 */
	public Integer getFiltroTipoLayoutArquivo() {
		return filtroTipoLayoutArquivo;
	}

	/**
	 * Set: filtroTipoLayoutArquivo.
	 * 
	 * @param filtroTipoLayoutArquivo
	 *            the filtro tipo layout arquivo
	 */
	public void setFiltroTipoLayoutArquivo(Integer filtroTipoLayoutArquivo) {
		this.filtroTipoLayoutArquivo = filtroTipoLayoutArquivo;
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 * 
	 * @return listaTipoLayoutArquivo
	 */
	public List<SelectItem> getListaTipoLayoutArquivo() {
		return listaTipoLayoutArquivo;
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 * 
	 * @param listaTipoLayoutArquivo
	 *            the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(
			List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	/**
	 * Get: itemSelecionadoLista.
	 * 
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 * 
	 * @param itemSelecionadoLista
	 *            the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	public List<ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO> getListaLayoutLoteServ() {
		return listaLayoutLoteServ;
	}

	public void setListaLayoutLoteServ(
			List<ListarAssVersaoLayoutLoteServicoOcorrenciasSaidaDTO> listaLayoutLoteServ) {
		this.listaLayoutLoteServ = listaLayoutLoteServ;
	}

	public ConsultarAssVersaoLayoutLoteServicoSaidaDto getSaidaConsultarLayoutLoteServ() {
		return saidaConsultarLayoutLoteServ;
	}

	public void setSaidaConsultarLayoutLoteServ(
			ConsultarAssVersaoLayoutLoteServicoSaidaDto saidaConsultarLayoutLoteServ) {
		this.saidaConsultarLayoutLoteServ = saidaConsultarLayoutLoteServ;
	}

	public ConsultarAssVersaoLayoutLoteServicoEntradaDTO getEntradaConsultarVersao() {
		return entradaConsultarVersao;
	}

	public void setEntradaConsultarVersao(
			ConsultarAssVersaoLayoutLoteServicoEntradaDTO entradaConsultarVersao) {
		this.entradaConsultarVersao = entradaConsultarVersao;
	}

	public ListarAssVersaoLayoutLoteServicoEntradaDTO getEntradaListarLote() {
		return entradaListarLote;
	}

	public void setEntradaListarLote(
			ListarAssVersaoLayoutLoteServicoEntradaDTO entradaListarLote) {
		this.entradaListarLote = entradaListarLote;
	}

	public Integer getNrVersaoLayoutArquivo() {
		return nrVersaoLayoutArquivo;
	}

	public void setNrVersaoLayoutArquivo(Integer nrVersaoLayoutArquivo) {
		this.nrVersaoLayoutArquivo = nrVersaoLayoutArquivo;
	}

	public Integer getCdTipoLoteLayout() {
		return cdTipoLoteLayout;
	}

	public void setCdTipoLoteLayout(Integer cdTipoLoteLayout) {
		this.cdTipoLoteLayout = cdTipoLoteLayout;
	}

	public String getDsTipoLoteLayout() {
		return dsTipoLoteLayout;
	}

	public void setDsTipoLoteLayout(String dsTipoLoteLayout) {
		this.dsTipoLoteLayout = dsTipoLoteLayout;
	}

	public Integer getNrVersaoLoteLayout() {
		return nrVersaoLoteLayout;
	}

	public void setNrVersaoLoteLayout(Integer nrVersaoLoteLayout) {
		this.nrVersaoLoteLayout = nrVersaoLoteLayout;
	}

	public Integer getCdTipoServicoCnab() {
		return cdTipoServicoCnab;
	}

	public void setCdTipoServicoCnab(Integer cdTipoServicoCnab) {
		this.cdTipoServicoCnab = cdTipoServicoCnab;
	}

	public String getDsTipoServicoCnab() {
		return dsTipoServicoCnab;
	}

	public void setDsTipoServicoCnab(String dsTipoServicoCnab) {
		this.dsTipoServicoCnab = dsTipoServicoCnab;
	}

	public IncluirAssVersaoLayoutLoteServicoEntradaDTO getEntradaIncluirLoteServ() {
		return entradaIncluirLoteServ;
	}

	public void setEntradaIncluirLoteServ(
			IncluirAssVersaoLayoutLoteServicoEntradaDTO entradaIncluirLoteServ) {
		this.entradaIncluirLoteServ = entradaIncluirLoteServ;
	}

	public List<SelectItem> getListaTipoLote() {
		return listaTipoLote;
	}

	public void setListaTipoLote(List<SelectItem> listaTipoLote) {
		this.listaTipoLote = listaTipoLote;
	}

	public List<SelectItem> getListaTipoServicoCnab() {
		return listaTipoServicoCnab;
	}

	public void setListaTipoServicoCnab(List<SelectItem> listaTipoServicoCnab) {
		this.listaTipoServicoCnab = listaTipoServicoCnab;
	}

	/**
	 * Nome: setSaidaTipoLayoutArquivoSaidaDTO
	 * 
	 * @exception
	 * @throws
	 * @param saidaTipoLayoutArquivoSaidaDTO
	 */
	public void setSaidaTipoLayoutArquivoSaidaDTO(
			TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
		this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: getSaidaTipoLayoutArquivoSaidaDTO
	 * 
	 * @exception
	 * @throws
	 * @return saidaTipoLayoutArquivoSaidaDTO
	 */
	public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
		return saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: setDescTipoLayoutArqCompl
	 * 
	 * @exception
	 * @throws
	 * @param descTipoLayoutArqCompl
	 */
	public void setDescTipoLayoutArqCompl(String descTipoLayoutArqCompl) {
		this.descTipoLayoutArqCompl = descTipoLayoutArqCompl;
	}

	/**
	 * Nome: getDescTipoLayoutArqCompl
	 * 
	 * @exception
	 * @throws
	 * @return descTipoLayoutArqCompl
	 */
	public String getDescTipoLayoutArqCompl() {
		return descTipoLayoutArqCompl;
	}

	/**
	 * Nome: setDescTipoLoteLayoutCompl
	 * 
	 * @exception
	 * @throws
	 * @param descTipoLoteLayoutCompl
	 */
	public void setDescTipoLoteLayoutCompl(String descTipoLoteLayoutCompl) {
		this.descTipoLoteLayoutCompl = descTipoLoteLayoutCompl;
	}

	/**
	 * Nome: getDescTipoLoteLayoutCompl
	 * 
	 * @exception
	 * @throws
	 * @return descTipoLoteLayoutCompl
	 */
	public String getDescTipoLoteLayoutCompl() {
		return descTipoLoteLayoutCompl;
	}

	/**
	 * Nome: setDescTipoServicoCnabCompl
	 * 
	 * @exception
	 * @throws
	 * @param descTipoServicoCnabCompl
	 */
	public void setDescTipoServicoCnabCompl(String descTipoServicoCnabCompl) {
		this.descTipoServicoCnabCompl = descTipoServicoCnabCompl;
	}

	/**
	 * Nome: getDescTipoServicoCnabCompl
	 * 
	 * @exception
	 * @throws
	 * @return descTipoServicoCnabCompl
	 */
	public String getDescTipoServicoCnabCompl() {
		return descTipoServicoCnabCompl;
	}

	/**
	 * @return the descLayoutArquivo
	 */
	public String getDescLayoutArquivo() {
		return descLayoutArquivo;
	}

	/**
	 * @param descLayoutArquivo
	 *            the descLayoutArquivo to set
	 */
	public void setDescLayoutArquivo(String descLayoutArquivo) {
		this.descLayoutArquivo = descLayoutArquivo;
	}

}
