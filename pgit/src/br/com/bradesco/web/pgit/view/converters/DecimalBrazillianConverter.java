/*
 * Nome: br.com.bradesco.web.pgit.view.converters
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.converters;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 * Nome: DecimalBrazillianConverter
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DecimalBrazillianConverter implements Converter {

	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2)
			throws ConverterException {
		if(arg2 == null || arg2.trim().equals("")){
			return null;
		}
		
		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(new Locale ("pt", "BR"));
		df.applyPattern("#,##0.00");
		df.setParseBigDecimal(true);

		try {
			return df.parse(arg2);
		} catch (ParseException e) {
			throw new ConverterException(e);
		}  
	}

	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2)	throws ConverterException {

		if(arg2 == null){
			return null;
		}
		
		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(new Locale ("pt", "BR"));
		df.applyPattern("#,##0.00");

		return df.format(arg2);
	}
}