/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.imprimircontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.imprimircontrato;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ListarImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.IListarFuncBradescoService;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: ImprimirContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ImprimirContratoBean {
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo listarFuncBradescoService. */
	private IListarFuncBradescoService listarFuncBradescoService;

	/** Atributo cpfCnpjMaster. */
	private String cpfCnpjMaster;
	
	/** Atributo nomeRazaoSocialMaster. */
	private String nomeRazaoSocialMaster;
	
	/** Atributo grupoEconomicoMaster. */
	private String grupoEconomicoMaster;
	
	/** Atributo atividadeEconomicaMaster. */
	private String atividadeEconomicaMaster;
	
	/** Atributo segmentoMaster. */
	private String segmentoMaster;
	
	/** Atributo subSegmentoMaster. */
	private String subSegmentoMaster;
	
	/** Atributo empresa. */
	private String empresa;
	
	/** Atributo cdEmpresa. */
	private Long cdEmpresa;
	
	/** Atributo tipo. */
	private String tipo;
	
	/** Atributo cdTipo. */
	private Integer cdTipo;
	
	/** Atributo numero. */
	private String numero;
	
	/** Atributo motivoDesc. */
	private String motivoDesc;
	
	/** Atributo situacaoDesc. */
	private String situacaoDesc;
	
	/** Atributo participacao. */
	private String participacao;
	
	/** Atributo dsDescricaoContrato. */
	private String dsDescricaoContrato;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;
	
	

	
	//Atributos p�gina selecionarDocumentoImprimirContrato
	/** Atributo radioArgumentoPesquisaSelecionarDocumento. */
	private String radioArgumentoPesquisaSelecionarDocumento;
	
	/** Atributo numeroDocumentoFiltro. */
	private String numeroDocumentoFiltro;
	
	/** Atributo versaoFiltro. */
	private String versaoFiltro;
	
	/** Atributo listaDocumentos. */
	private List<ListarImprimirContratoSaidaDTO> listaDocumentos;
	
	/** Atributo listaControleDocumentos. */
	private List<SelectItem> listaControleDocumentos;
	
	/** Atributo itemSelecionadoListaDocumentos. */
	private Integer itemSelecionadoListaDocumentos;
	
	/** Atributo diaFiltro. */
	private String diaFiltro;
	
	/** Atributo mesFiltro. */
	private String mesFiltro;
	
	/** Atributo anoFiltro. */
	private String anoFiltro;
	
	//Atributos p�gina selecionarDocumentoImprimirContrato
	/** Atributo listaFormalizacao. */
	private List<ListarImprimirContratoSaidaDTO> listaFormalizacao;
	
	/** Atributo listaControleFormalizacao. */
	private List<SelectItem> listaControleFormalizacao;
	
	/** Atributo itemSelecionadoListaFormalizacao. */
	private Integer itemSelecionadoListaFormalizacao;
	
	//M�TODOS
	/**
	 * Carrega lista documentos.
	 */
	public void carregaListaDocumentos(){
		listaDocumentos = new ArrayList<ListarImprimirContratoSaidaDTO>();
		
		ListarImprimirContratoSaidaDTO contrato1 = new ListarImprimirContratoSaidaDTO();
		contrato1.setCdDocumento(1);
		contrato1.setCdVersao(1);
		contrato1.setDsDataGeracao("data 1");
		contrato1.setDsFormulario("formulario 1");
		contrato1.setDsIdioma("idioma 1");
		
		ListarImprimirContratoSaidaDTO contrato2 = new ListarImprimirContratoSaidaDTO();
		contrato2.setCdDocumento(2);
		contrato2.setCdVersao(2);
		contrato2.setDsDataGeracao("data 2");
		contrato2.setDsFormulario("formulario 2");
		contrato2.setDsIdioma("idioma 2");
		
		listaDocumentos.add(contrato1);
		listaDocumentos.add(contrato2);
		
		listaControleDocumentos = new ArrayList<SelectItem>();
		
		for (int i = 0; i <= listaDocumentos.size(); i++) {
			listaControleDocumentos.add(new SelectItem(i, " "));
		}
		
		
		
		
		/*
			try{
			
				ListarImprimirContratoEntradaDTO entradaDTO = new ListarImprimirContratoEntradaDTO();
				
				entradaDTO.setCdTipoComprovante(getCodigoTipoComprovanteFiltro()!=null ? getCodigoTipoComprovanteFiltro():0);
				
				
				setListaDocumentos(getTipoComprovanteServiceImpl().listarTipoComprovante(entradaDTO));
				
				for (int i = 0; i <= listaDocumentos.size(); i++) {
					listaControleDocumentos.add(new SelectItem(i, " "));
				}
	
				
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				setListaDocumentos(null);	
			}
			setItemSelecionadoListaDocumentos(null);
		*/
	}
	
	/**
	 * Limpar dados.
	 */
	public void limparDados(){
		setListaDocumentos(null);
		setListaControleDocumentos(null);
		setItemSelecionadoListaDocumentos(null);
		setListaFormalizacao(null);
		setListaControleFormalizacao(null);
		setItemSelecionadoListaFormalizacao(null);
		setRadioArgumentoPesquisaSelecionarDocumento(null);
		setNumeroDocumentoFiltro(null);
		setVersaoFiltro(null);
	}
	
	/**
	 * Limpar dados filtro.
	 */
	public void limparDadosFiltro() {
		
		setNumeroDocumentoFiltro("");
		setDiaFiltro("");
		setMesFiltro("");
		setAnoFiltro("");
		setVersaoFiltro("");
	}
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt){
		setListaDocumentos(null);
		setListaControleDocumentos(null);
		setItemSelecionadoListaDocumentos(null);
		setListaFormalizacao(null);
		setListaControleFormalizacao(null);
		setItemSelecionadoListaFormalizacao(null);
		setRadioArgumentoPesquisaSelecionarDocumento(null);
		setNumeroDocumentoFiltro(null);
		setVersaoFiltro(null);
		
		identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		
		//carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteImprimirContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conImprimirContrato");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		identificacaoClienteContratoBean.setBloqueiaRadio(false);
		identificacaoClienteContratoBean.limparDadosPesquisaContrato();		
		identificacaoClienteContratoBean.setObrigatoriedade("T");
	}
		
	/**
	 * Selecionar documento.
	 *
	 * @return the string
	 */
	public String selecionarDocumento(){
		
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setDsDescricaoContrato(saidaDTO.getDsContrato());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
	
		
		return "SELECIONAR_DOCUMENTO";
	}
	
	/**
	 * Selecionar formalizacao.
	 *
	 * @return the string
	 */
	public String selecionarFormalizacao(){
		return "SELECIONAR_FORMALIZACAO";
	}
	
	/**
	 * Voltar formalizacao.
	 *
	 * @return the string
	 */
	public String voltarFormalizacao(){
		return "VOLTAR_FORMALIZACAO";
	}
	
	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		return "VOLTAR_PESQUISAR";
	}
		
	//GETTERS E SETTERS
	/**
	 * Get: radioArgumentoPesquisaSelecionarDocumento.
	 *
	 * @return radioArgumentoPesquisaSelecionarDocumento
	 */
	public String getRadioArgumentoPesquisaSelecionarDocumento() {
		return radioArgumentoPesquisaSelecionarDocumento;
	}

	/**
	 * Set: radioArgumentoPesquisaSelecionarDocumento.
	 *
	 * @param radioArgumentoPesquisaSelecionarDocumento the radio argumento pesquisa selecionar documento
	 */
	public void setRadioArgumentoPesquisaSelecionarDocumento(
			String radioArgumentoPesquisaSelecionarDocumento) {
		this.radioArgumentoPesquisaSelecionarDocumento = radioArgumentoPesquisaSelecionarDocumento;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: atividadeEconomicaMaster.
	 *
	 * @return atividadeEconomicaMaster
	 */
	public String getAtividadeEconomicaMaster() {
		return atividadeEconomicaMaster;
	}

	/**
	 * Set: atividadeEconomicaMaster.
	 *
	 * @param atividadeEconomicaMaster the atividade economica master
	 */
	public void setAtividadeEconomicaMaster(String atividadeEconomicaMaster) {
		this.atividadeEconomicaMaster = atividadeEconomicaMaster;
	}

	/**
	 * Get: cpfCnpjMaster.
	 *
	 * @return cpfCnpjMaster
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}

	/**
	 * Set: cpfCnpjMaster.
	 *
	 * @param cpfCnpjMaster the cpf cnpj master
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}

	/**
	 * Get: grupoEconomicoMaster.
	 *
	 * @return grupoEconomicoMaster
	 */
	public String getGrupoEconomicoMaster() {
		return grupoEconomicoMaster;
	}

	/**
	 * Set: grupoEconomicoMaster.
	 *
	 * @param grupoEconomicoMaster the grupo economico master
	 */
	public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
		this.grupoEconomicoMaster = grupoEconomicoMaster;
	}

	/**
	 * Get: nomeRazaoSocialMaster.
	 *
	 * @return nomeRazaoSocialMaster
	 */
	public String getNomeRazaoSocialMaster() {
		return nomeRazaoSocialMaster;
	}

	/**
	 * Set: nomeRazaoSocialMaster.
	 *
	 * @param nomeRazaoSocialMaster the nome razao social master
	 */
	public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
		this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
	}

	/**
	 * Get: segmentoMaster.
	 *
	 * @return segmentoMaster
	 */
	public String getSegmentoMaster() {
		return segmentoMaster;
	}

	/**
	 * Set: segmentoMaster.
	 *
	 * @param segmentoMaster the segmento master
	 */
	public void setSegmentoMaster(String segmentoMaster) {
		this.segmentoMaster = segmentoMaster;
	}

	/**
	 * Get: subSegmentoMaster.
	 *
	 * @return subSegmentoMaster
	 */
	public String getSubSegmentoMaster() {
		return subSegmentoMaster;
	}

	/**
	 * Set: subSegmentoMaster.
	 *
	 * @param subSegmentoMaster the sub segmento master
	 */
	public void setSubSegmentoMaster(String subSegmentoMaster) {
		this.subSegmentoMaster = subSegmentoMaster;
	}
	
	/**
	 * Voltar solicitacoes.
	 *
	 * @return the string
	 */
	public String voltarSolicitacoes(){
		return "VOLTAR_PESQUISAR";
		
	}
	
	/**
	 * Get: listarFuncBradescoService.
	 *
	 * @return listarFuncBradescoService
	 */
	public IListarFuncBradescoService getListarFuncBradescoService() {
		return listarFuncBradescoService;
	}

	/**
	 * Set: listarFuncBradescoService.
	 *
	 * @param listarFuncBradescoService the listar func bradesco service
	 */
	public void setListarFuncBradescoService(
			IListarFuncBradescoService listarFuncBradescoService) {
		this.listarFuncBradescoService = listarFuncBradescoService;
	}
	
	/**
	 * Get: numeroDocumentoFiltro.
	 *
	 * @return numeroDocumentoFiltro
	 */
	public String getNumeroDocumentoFiltro() {
		return numeroDocumentoFiltro;
	}

	/**
	 * Set: numeroDocumentoFiltro.
	 *
	 * @param numeroDocumentoFiltro the numero documento filtro
	 */
	public void setNumeroDocumentoFiltro(String numeroDocumentoFiltro) {
		this.numeroDocumentoFiltro = numeroDocumentoFiltro;
	}

	/**
	 * Get: versaoFiltro.
	 *
	 * @return versaoFiltro
	 */
	public String getVersaoFiltro() {
		return versaoFiltro;
	}

	/**
	 * Set: versaoFiltro.
	 *
	 * @param versaoFiltro the versao filtro
	 */
	public void setVersaoFiltro(String versaoFiltro) {
		this.versaoFiltro = versaoFiltro;
	}

	/**
	 * Get: itemSelecionadoListaDocumentos.
	 *
	 * @return itemSelecionadoListaDocumentos
	 */
	public Integer getItemSelecionadoListaDocumentos() {
		return itemSelecionadoListaDocumentos;
	}

	/**
	 * Set: itemSelecionadoListaDocumentos.
	 *
	 * @param itemSelecionadoListaDocumentos the item selecionado lista documentos
	 */
	public void setItemSelecionadoListaDocumentos(
			Integer itemSelecionadoListaDocumentos) {
		this.itemSelecionadoListaDocumentos = itemSelecionadoListaDocumentos;
	}

	/**
	 * Get: listaControleDocumentos.
	 *
	 * @return listaControleDocumentos
	 */
	public List<SelectItem> getListaControleDocumentos() {
		return listaControleDocumentos;
	}

	/**
	 * Set: listaControleDocumentos.
	 *
	 * @param listaControleDocumentos the lista controle documentos
	 */
	public void setListaControleDocumentos(List<SelectItem> listaControleDocumentos) {
		this.listaControleDocumentos = listaControleDocumentos;
	}

	/**
	 * Get: listaDocumentos.
	 *
	 * @return listaDocumentos
	 */
	public List<ListarImprimirContratoSaidaDTO> getListaDocumentos() {
		return listaDocumentos;
	}

	/**
	 * Set: listaDocumentos.
	 *
	 * @param listaDocumentos the lista documentos
	 */
	public void setListaDocumentos(
			List<ListarImprimirContratoSaidaDTO> listaDocumentos) {
		this.listaDocumentos = listaDocumentos;
	}

	/**
	 * Get: itemSelecionadoListaFormalizacao.
	 *
	 * @return itemSelecionadoListaFormalizacao
	 */
	public Integer getItemSelecionadoListaFormalizacao() {
		return itemSelecionadoListaFormalizacao;
	}

	/**
	 * Set: itemSelecionadoListaFormalizacao.
	 *
	 * @param itemSelecionadoListaFormalizacao the item selecionado lista formalizacao
	 */
	public void setItemSelecionadoListaFormalizacao(
			Integer itemSelecionadoListaFormalizacao) {
		this.itemSelecionadoListaFormalizacao = itemSelecionadoListaFormalizacao;
	}

	/**
	 * Get: listaControleFormalizacao.
	 *
	 * @return listaControleFormalizacao
	 */
	public List<SelectItem> getListaControleFormalizacao() {
		return listaControleFormalizacao;
	}

	/**
	 * Set: listaControleFormalizacao.
	 *
	 * @param listaControleFormalizacao the lista controle formalizacao
	 */
	public void setListaControleFormalizacao(
			List<SelectItem> listaControleFormalizacao) {
		this.listaControleFormalizacao = listaControleFormalizacao;
	}

	/**
	 * Get: listaFormalizacao.
	 *
	 * @return listaFormalizacao
	 */
	public List<ListarImprimirContratoSaidaDTO> getListaFormalizacao() {
		listaFormalizacao = new ArrayList<ListarImprimirContratoSaidaDTO>();
		
		ListarImprimirContratoSaidaDTO contrato1 = new ListarImprimirContratoSaidaDTO();
		contrato1.setCdAditivo(1);
		contrato1.setDsDataInclusao("data 1");
		contrato1.setDsSituacao("situacao 1");
		contrato1.setDsSubstituiContratoComercial("sim");
		
		ListarImprimirContratoSaidaDTO contrato2 = new ListarImprimirContratoSaidaDTO();
		contrato2.setCdAditivo(1);
		contrato2.setDsDataInclusao("data 1");
		contrato2.setDsSituacao("situacao 1");
		contrato2.setDsSubstituiContratoComercial("sim");
		
		listaFormalizacao.add(contrato1);
		listaFormalizacao.add(contrato2);
		
		listaControleFormalizacao = new ArrayList<SelectItem>();
		
		for (int i = 0; i <= listaFormalizacao.size(); i++) {
			listaControleFormalizacao.add(new SelectItem(i, " "));
		}
		
		/*
			try{
			
				ListarImprimirContratoEntradaDTO entradaDTO = new ListarImprimirContratoEntradaDTO();
				
				entradaDTO.setCdTipoComprovante(getCodigoTipoComprovanteFiltro()!=null ? getCodigoTipoComprovanteFiltro():0);
				
				
				setListaDocumentos(getTipoComprovanteServiceImpl().listarTipoComprovante(entradaDTO));
				
				for (int i = 0; i <= listaDocumentos.size(); i++) {
					listaControleDocumentos.add(new SelectItem(i, " "));
				}
	
				
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				setListaDocumentos(null);	
			}
			setItemSelecionadoListaDocumentos(null);
		*/
		
		return listaFormalizacao;
	}

	/**
	 * Set: listaFormalizacao.
	 *
	 * @param listaFormalizacao the lista formalizacao
	 */
	public void setListaFormalizacao(
			List<ListarImprimirContratoSaidaDTO> listaFormalizacao) {
		this.listaFormalizacao = listaFormalizacao;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Get: cdTipo.
	 *
	 * @return cdTipo
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}

	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}



	/**
	 * Get: motivoDesc.
	 *
	 * @return motivoDesc
	 */
	public String getMotivoDesc() {
		return motivoDesc;
	}

	/**
	 * Set: motivoDesc.
	 *
	 * @param motivoDesc the motivo desc
	 */
	public void setMotivoDesc(String motivoDesc) {
		this.motivoDesc = motivoDesc;
	}

	/**
	 * Get: participacao.
	 *
	 * @return participacao
	 */
	public String getParticipacao() {
		return participacao;
	}

	/**
	 * Set: participacao.
	 *
	 * @param participacao the participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	/**
	 * Get: situacaoDesc.
	 *
	 * @return situacaoDesc
	 */
	public String getSituacaoDesc() {
		return situacaoDesc;
	}

	/**
	 * Set: situacaoDesc.
	 *
	 * @param situacaoDesc the situacao desc
	 */
	public void setSituacaoDesc(String situacaoDesc) {
		this.situacaoDesc = situacaoDesc;
	}

	

	/**
	 * Get: dsDescricaoContrato.
	 *
	 * @return dsDescricaoContrato
	 */
	public String getDsDescricaoContrato() {
		return dsDescricaoContrato;
	}

	/**
	 * Set: dsDescricaoContrato.
	 *
	 * @param dsDescricaoContrato the ds descricao contrato
	 */
	public void setDsDescricaoContrato(String dsDescricaoContrato) {
		this.dsDescricaoContrato = dsDescricaoContrato;
	}

	/**
	 * Get: anoFiltro.
	 *
	 * @return anoFiltro
	 */
	public String getAnoFiltro() {
		return anoFiltro;
	}

	/**
	 * Set: anoFiltro.
	 *
	 * @param anoFiltro the ano filtro
	 */
	public void setAnoFiltro(String anoFiltro) {
		this.anoFiltro = anoFiltro;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: diaFiltro.
	 *
	 * @return diaFiltro
	 */
	public String getDiaFiltro() {
		return diaFiltro;
	}

	/**
	 * Set: diaFiltro.
	 *
	 * @param diaFiltro the dia filtro
	 */
	public void setDiaFiltro(String diaFiltro) {
		this.diaFiltro = diaFiltro;
	}

	/**
	 * Get: mesFiltro.
	 *
	 * @return mesFiltro
	 */
	public String getMesFiltro() {
		return mesFiltro;
	}

	/**
	 * Set: mesFiltro.
	 *
	 * @param mesFiltro the mes filtro
	 */
	public void setMesFiltro(String mesFiltro) {
		this.mesFiltro = mesFiltro;
	}
}
