/**
 * 
 */
package br.com.bradesco.web.pgit.view.bean;

import javax.faces.event.ActionEvent;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.security.intranet.service.IAuthenticationService;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.provider.intranet.IAlterarSenhaService;

/**
 * @author marcio.alves
 *
 */
public class ChangePasswordBean {

	/** Atributo alterarSenhaService. */
	private IAlterarSenhaService alterarSenhaService;
	
	/** Atributo autenticationService. */
	private IAuthenticationService autenticationService; 
	
	/** Atributo usuario. */
	private String usuario;
	
	/** Atributo senha. */
	private String senha;
	
	/** Atributo novaSenha. */
	private String novaSenha;
	
	/** Atributo novaSenhaConf. */
	private String novaSenhaConf;	
	
	/**
	 * Change pass.
	 *
	 * @param evt the evt
	 */
	public void changePass(ActionEvent evt) {
		try {
			this.alterarSenhaService.trocarSenha(this.usuario, this.senha, this.novaSenha, this.novaSenhaConf);	
		} catch (Exception ex) {
			throw new BradescoViewException(ex.getMessage(), ex, "", "", BradescoViewExceptionActionType.ACTION);
		}
		BradescoFacesUtils.addInfoModalMessage("Senha alterada com sucesso.", "/index.jsp", BradescoViewExceptionActionType.PATH, false);
	}	
	
	/**
	 * Get: usuario.
	 *
	 * @return usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * Set: usuario.
	 *
	 * @param usuario the usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * Get: senha.
	 *
	 * @return senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * Set: senha.
	 *
	 * @param senha the senha
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * Get: novaSenha.
	 *
	 * @return novaSenha
	 */
	public String getNovaSenha() {
		return novaSenha;
	}

	/**
	 * Set: novaSenha.
	 *
	 * @param novaSenha the nova senha
	 */
	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	/**
	 * Get: novaSenhaConf.
	 *
	 * @return novaSenhaConf
	 */
	public String getNovaSenhaConf() {
		return novaSenhaConf;
	}

	/**
	 * Set: novaSenhaConf.
	 *
	 * @param novaSenhaConf the nova senha conf
	 */
	public void setNovaSenhaConf(String novaSenhaConf) {
		this.novaSenhaConf = novaSenhaConf;
	}

	/**
	 * Nome: getAlterarSenhaService
	 * <p>Prop�sito: M�todo get para o atributo alterarSenhaService. </p>
	 * @return IAlterarSenhaService alterarSenhaService
	 */
	public IAlterarSenhaService getAlterarSenhaService() {
		return alterarSenhaService;
	}

	/**
	 * Nome: setAlterarSenhaService
	 * <p>Prop�sito: M�todo set para o atributo alterarSenhaService. </p>
	 * @param IAlterarSenhaService alterarSenhaService
	 */
	public void setAlterarSenhaService(IAlterarSenhaService alterarSenhaService) {
		this.alterarSenhaService = alterarSenhaService;
	}

	/**
	 * Nome: getAutenticationService
	 * <p>Prop�sito: M�todo get para o atributo autenticationService. </p>
	 * @return IAuthenticationService autenticationService
	 */
	public IAuthenticationService getAutenticationService() {
		return autenticationService;
	}

	/**
	 * Nome: setAutenticationService
	 * <p>Prop�sito: M�todo set para o atributo autenticationService. </p>
	 * @param IAuthenticationService autenticationService
	 */
	public void setAutenticationService(IAuthenticationService autenticationService) {
		this.autenticationService = autenticationService;
	}
}