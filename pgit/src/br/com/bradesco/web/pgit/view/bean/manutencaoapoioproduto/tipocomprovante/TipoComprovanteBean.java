/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.tipocomprovante
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.tipocomprovante;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlEntradaDTO;
import br.com.bradesco.web.pgit.service.business.msgcompsalarial.bean.ListarMsgComprovanteSalrlSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.ITipoComprovanteService;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.AlterarTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.AlterarTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.DetalharTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.DetalharTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ExcluirTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ExcluirTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.IncluirTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.IncluirTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ListarTipoComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.tipocomprovante.bean.ListarTipoComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: TipoComprovanteBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TipoComprovanteBean {
	
	/** Atributo CON_TIPO_COMPROVANTE. */
	private static final String CON_TIPO_COMPROVANTE = "conTipoComprovante";
	
	/** Atributo tipoComprovanteServiceImpl. */
	private ITipoComprovanteService tipoComprovanteServiceImpl;
	
	/** Atributo codigoTipoComprovanteFiltro. */
	private Integer codigoTipoComprovanteFiltro;
	
	/** Atributo codigoTipoComprovante. */
	private String codigoTipoComprovante;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo cdMsgPadraoOrganizacao. */
	private Integer cdMsgPadraoOrganizacao;
	
	/** Atributo dsMsgPadraoOrganizacao. */
	private String dsMsgPadraoOrganizacao;
	
	/** Atributo descricaoTipoComprovante. */
	private String descricaoTipoComprovante;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo listaGrid. */
	private List<ListarTipoComprovanteSaidaDTO> listaGrid;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();	
	
	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo listaMsgComprovanteSalarial. */
	private List<SelectItem> listaMsgComprovanteSalarial = new ArrayList<SelectItem>();
	
	/** Atributo listaMsgComprovanteSalarialHash. */
	private Map<Integer, String> listaMsgComprovanteSalarialHash = new HashMap<Integer, String>();	
	
	/**
	 * Get: dsMsgPadraoOrganizacao.
	 *
	 * @return dsMsgPadraoOrganizacao
	 */
	public String getDsMsgPadraoOrganizacao() {
		return dsMsgPadraoOrganizacao;
	}

	/**
	 * Set: dsMsgPadraoOrganizacao.
	 *
	 * @param dsMsgPadraoOrganizacao the ds msg padrao organizacao
	 */
	public void setDsMsgPadraoOrganizacao(String dsMsgPadraoOrganizacao) {
		this.dsMsgPadraoOrganizacao = dsMsgPadraoOrganizacao;
	}

	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: descricaoTipoComprovante.
	 *
	 * @return descricaoTipoComprovante
	 */
	public String getDescricaoTipoComprovante() {
		return descricaoTipoComprovante;
	}

	/**
	 * Set: descricaoTipoComprovante.
	 *
	 * @param descricaoTipoComprovante the descricao tipo comprovante
	 */
	public void setDescricaoTipoComprovante(String descricaoTipoComprovante) {
		this.descricaoTipoComprovante = descricaoTipoComprovante;
	}

	/**
	 * Get: codigoTipoComprovante.
	 *
	 * @return codigoTipoComprovante
	 */
	public String getCodigoTipoComprovante() {
		return codigoTipoComprovante;
	}

	/**
	 * Set: codigoTipoComprovante.
	 *
	 * @param codigoTipoComprovante the codigo tipo comprovante
	 */
	public void setCodigoTipoComprovante(String codigoTipoComprovante) {
		this.codigoTipoComprovante = codigoTipoComprovante;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarTipoComprovanteSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarTipoComprovanteSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: tipoComprovanteServiceImpl.
	 *
	 * @return tipoComprovanteServiceImpl
	 */
	public ITipoComprovanteService getTipoComprovanteServiceImpl() {
		return tipoComprovanteServiceImpl;
	}

	/**
	 * Set: tipoComprovanteServiceImpl.
	 *
	 * @param tipoComprovanteServiceImpl the tipo comprovante service impl
	 */
	public void setTipoComprovanteServiceImpl(
			ITipoComprovanteService tipoComprovanteServiceImpl) {
		this.tipoComprovanteServiceImpl = tipoComprovanteServiceImpl;
	}

	/**
	 * Get: codigoTipoComprovanteFiltro.
	 *
	 * @return codigoTipoComprovanteFiltro
	 */
	public Integer getCodigoTipoComprovanteFiltro() {
		return codigoTipoComprovanteFiltro;
	}

	/**
	 * Set: codigoTipoComprovanteFiltro.
	 *
	 * @param codigoTipoComprovanteFiltro the codigo tipo comprovante filtro
	 */
	public void setCodigoTipoComprovanteFiltro(Integer codigoTipoComprovanteFiltro) {
		this.codigoTipoComprovanteFiltro = codigoTipoComprovanteFiltro;
	}
	
	/**
	 * Get: listaMsgComprovanteSalarial.
	 *
	 * @return listaMsgComprovanteSalarial
	 */
	public List<SelectItem> getListaMsgComprovanteSalarial() {
		return listaMsgComprovanteSalarial;
	}

	/**
	 * Set: listaMsgComprovanteSalarial.
	 *
	 * @param listaMsgComprovanteSalarial the lista msg comprovante salarial
	 */
	public void setListaMsgComprovanteSalarial(
			List<SelectItem> listaMsgComprovanteSalarial) {
		this.listaMsgComprovanteSalarial = listaMsgComprovanteSalarial;
	}
	
	/**
	 * Get: cdMsgPadraoOrganizacao.
	 *
	 * @return cdMsgPadraoOrganizacao
	 */
	public Integer getCdMsgPadraoOrganizacao() {
		return cdMsgPadraoOrganizacao;
	}

	/**
	 * Set: cdMsgPadraoOrganizacao.
	 *
	 * @param cdMsgPadraoOrganizacao the cd msg padrao organizacao
	 */
	public void setCdMsgPadraoOrganizacao(Integer cdMsgPadraoOrganizacao) {
		this.cdMsgPadraoOrganizacao = cdMsgPadraoOrganizacao;
	}


	/**
	 * Get: listaMsgComprovanteSalarialHash.
	 *
	 * @return listaMsgComprovanteSalarialHash
	 */
	public Map<Integer, String> getListaMsgComprovanteSalarialHash() {
		return listaMsgComprovanteSalarialHash;
	}

	/**
	 * Set lista msg comprovante salarial hash.
	 *
	 * @param listaMsgComprovanteSalarialHash the lista msg comprovante salarial hash
	 */
	public void setListaMsgComprovanteSalarialHash(
			Map<Integer, String> listaMsgComprovanteSalarialHash) {
		this.listaMsgComprovanteSalarialHash = listaMsgComprovanteSalarialHash;
	}

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		this.setCodigoTipoComprovanteFiltro(null);
		this.setCodigoTipoComprovante(null);
		this.setDescricaoTipoComprovante("");
		setItemSelecionadoLista(null);
		setListaGrid(null);
		listarMsgComprovanteSalarial();
		setBtoAcionado(false);
	}
	
	/**
	 * Limpar campos.
	 */
	public void limparCampos(){
		this.setCodigoTipoComprovanteFiltro(null);
		setItemSelecionadoLista(null);
		setListaGrid(null);
		setBtoAcionado(false);
	}
	
	/**
	 * Carrega lista.
	 */
	public void carregaLista(){
		try{
			
			ListarTipoComprovanteEntradaDTO entradaDTO = new ListarTipoComprovanteEntradaDTO();
			
			entradaDTO.setCdTipoComprovante(getCodigoTipoComprovanteFiltro()!=null ? getCodigoTipoComprovanteFiltro():0);
			
			
			setListaGrid(getTipoComprovanteServiceImpl().listarTipoComprovante(entradaDTO));
			setBtoAcionado(true);
			for (int i = 0; i <= this.getListaGrid().size(); i++) {
				listaControle.add(new SelectItem(i, " "));
			}

			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);

		}
		setItemSelecionadoLista(null);
	}	
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try {			
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_TIPO_COMPROVANTE, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "DETALHAR";
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		this.setCodigoTipoComprovante("");
		this.setDescricaoTipoComprovante("");
		this.setCdMsgPadraoOrganizacao(null);
		return "INCLUIR";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		if (getObrigatoriedade() != null && getObrigatoriedade().equals("T")){
			setDsMsgPadraoOrganizacao( getCdMsgPadraoOrganizacao() + " - " + (String) listaMsgComprovanteSalarialHash.get(getCdMsgPadraoOrganizacao()));
			return "AVANCARINCLUIR";
		}
		return "";
		
	}
	
	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_TIPO_COMPROVANTE, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "ALTERAR";
	}
	
	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar(){
		if (getObrigatoriedade() != null && getObrigatoriedade().equals("T")){
			setDsMsgPadraoOrganizacao(getCdMsgPadraoOrganizacao() + " - " + (String) listaMsgComprovanteSalarialHash.get(getCdMsgPadraoOrganizacao()));
			return "AVANCARALTERAR";
		}
		return "";
		
	}
	
	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar(){
		try {
			
			AlterarTipoComprovanteEntradaDTO alterarTipoComprovanteEntradaDTO = new AlterarTipoComprovanteEntradaDTO();
			
			alterarTipoComprovanteEntradaDTO.setCdTipoComprovante(Integer.parseInt(getCodigoTipoComprovante()));
			alterarTipoComprovanteEntradaDTO.setDsTipoComprovante(getDescricaoTipoComprovante());
			alterarTipoComprovanteEntradaDTO.setMsgPadraoOrganizacional(getCdMsgPadraoOrganizacao());
		
			AlterarTipoComprovanteSaidaDTO alterarTipoComprovanteSaidaDTO = getTipoComprovanteServiceImpl().alterarTipoComprovante(alterarTipoComprovanteEntradaDTO);			
			BradescoFacesUtils.addInfoModalMessage("(" +alterarTipoComprovanteSaidaDTO.getCodMensagem() + ") " + alterarTipoComprovanteSaidaDTO.getMensagem(), CON_TIPO_COMPROVANTE, BradescoViewExceptionActionType.ACTION, false);
			carregaLista();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				return null;
			}
		return "";
		
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try {			
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_TIPO_COMPROVANTE, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "EXCLUIR";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try {
			
			ExcluirTipoComprovanteEntradaDTO excluirTipoComprovanteEntradaDTO = new ExcluirTipoComprovanteEntradaDTO();
			ListarTipoComprovanteSaidaDTO listarTipoComprovanteSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
			
			excluirTipoComprovanteEntradaDTO.setCdTipoComprovanteSalarial(listarTipoComprovanteSaidaDTO.getCdComprovanteSalarial());
			
			ExcluirTipoComprovanteSaidaDTO excluitTipoComprovanteSaidaDTO = getTipoComprovanteServiceImpl().excluirTipoComprovante(excluirTipoComprovanteEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + excluitTipoComprovanteSaidaDTO.getCodMensagem() + ") " + excluitTipoComprovanteSaidaDTO.getMensagem(), CON_TIPO_COMPROVANTE, BradescoViewExceptionActionType.ACTION, false);

			carregaLista();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Voltar principal.
	 *
	 * @return the string
	 */
	public String voltarPrincipal(){
		setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){		
		return "VOLTAR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try {
			
			IncluirTipoComprovanteEntradaDTO incluirTipoComprovanteEntradaDTO = new IncluirTipoComprovanteEntradaDTO();
			
			incluirTipoComprovanteEntradaDTO.setCdTipoComprovante(Integer.parseInt(getCodigoTipoComprovante()));
			incluirTipoComprovanteEntradaDTO.setDsTipoComprovante(getDescricaoTipoComprovante());
			incluirTipoComprovanteEntradaDTO.setMsgPadraoOrganizacional(getCdMsgPadraoOrganizacao());
			
			IncluirTipoComprovanteSaidaDTO incluirTipoComprovanteSaidaDTO = getTipoComprovanteServiceImpl().incluirTipoComprovante(incluirTipoComprovanteEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +incluirTipoComprovanteSaidaDTO.getCodMensagem() + ") " + incluirTipoComprovanteSaidaDTO.getMensagem(), CON_TIPO_COMPROVANTE, BradescoViewExceptionActionType.ACTION, false);
			carregaLista();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}

	/**
	 * Preenche dados.
	 */
	public void preencheDados(){
		
		
		ListarTipoComprovanteSaidaDTO listaTipoComprovanteSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
		DetalharTipoComprovanteEntradaDTO detalharTipoComprovanteEntradaDTO = new DetalharTipoComprovanteEntradaDTO();
		
		detalharTipoComprovanteEntradaDTO.setCdComprovanteSalarial(listaTipoComprovanteSaidaDTO.getCdComprovanteSalarial());
		
		DetalharTipoComprovanteSaidaDTO detalharTipoComprovanteSaidaDTO = getTipoComprovanteServiceImpl().detalharTipoComprovante(detalharTipoComprovanteEntradaDTO);
		
		setCodigoTipoComprovante(Integer.toString(detalharTipoComprovanteSaidaDTO.getCdTipoComprovante()));
		setDescricaoTipoComprovante(detalharTipoComprovanteSaidaDTO.getDsTipoComprovante());
		setCdMsgPadraoOrganizacao(detalharTipoComprovanteSaidaDTO.getCdMsgPadraoOrganizacional());
		setDsMsgPadraoOrganizacao(detalharTipoComprovanteSaidaDTO.getCdMsgPadraoOrganizacional() != 0 ? detalharTipoComprovanteSaidaDTO.getCdMsgPadraoOrganizacional() + " - " +  (detalharTipoComprovanteSaidaDTO.getDsMsgPadraoOrganizacional() == null ? "" : detalharTipoComprovanteSaidaDTO.getDsMsgPadraoOrganizacional()) :"" );		
		setUsuarioInclusao(detalharTipoComprovanteSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharTipoComprovanteSaidaDTO.getUsuarioManutencao());
		setComplementoInclusao(detalharTipoComprovanteSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharTipoComprovanteSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharTipoComprovanteSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharTipoComprovanteSaidaDTO.getComplementoManutencao());
		setTipoCanalInclusao(detalharTipoComprovanteSaidaDTO.getCdCanalInclusao()==0? "" : detalharTipoComprovanteSaidaDTO.getCdCanalInclusao() + " - " + detalharTipoComprovanteSaidaDTO.getDsCanalInclusao());
		setTipoCanalManutencao(detalharTipoComprovanteSaidaDTO.getCdCanalManutencao()==0? "" : detalharTipoComprovanteSaidaDTO.getCdCanalManutencao() + " - " + detalharTipoComprovanteSaidaDTO.getDsCanalManutencao());
		setDataHoraInclusao(detalharTipoComprovanteSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharTipoComprovanteSaidaDTO.getDataHoraManutencao());
		
	}
	
	/**
	 * Listar msg comprovante salarial.
	 */
	public void listarMsgComprovanteSalarial(){
		try{
			this.listaMsgComprovanteSalarial = new ArrayList<SelectItem>();
			
			ListarMsgComprovanteSalrlEntradaDTO entradaDTO = new ListarMsgComprovanteSalrlEntradaDTO();
			entradaDTO.setCdControleMensagemSalarial(0);
			entradaDTO.setCdTipoComprovanteSalarial(0);
			entradaDTO.setCdTipoMensagemSalarial(1);
			entradaDTO.setCdRestMensagemSalarial(0);
			
			List<ListarMsgComprovanteSalrlSaidaDTO> list = new ArrayList<ListarMsgComprovanteSalrlSaidaDTO>();
			
			list = getTipoComprovanteServiceImpl().listarMsgComprovanteSalrl(entradaDTO);
			
			listaMsgComprovanteSalarialHash.clear();
			for(ListarMsgComprovanteSalrlSaidaDTO combo : list){			
				listaMsgComprovanteSalarialHash.put(combo.getCodigo(), combo.getDescricao());
				listaMsgComprovanteSalarial.add(new SelectItem(combo.getCodigo(), PgitUtil.concatenarCampos(combo.getCodigo(), combo.getDescricao())));
			}
		}catch(PdcAdapterFunctionalException e){
			listaMsgComprovanteSalarial = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}	
	
	
	
}
