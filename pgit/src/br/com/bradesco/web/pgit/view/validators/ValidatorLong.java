/*
 * Nome: br.com.bradesco.web.pgit.view.validators
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.validators;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import br.com.bradesco.web.aq.view.validators.base.BradescoValidator;

/**
 * Nome: ValidatorLong
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ValidatorLong extends BradescoValidator {
	/**
	 * Cadeia de identifica��o do validador.
	 */
	private static String validatorLongId = "ValidatorLong";

	/** Atributo id. */
	private String id = null;

	/**
	 * <p>
	 * M�todo validador de Campos Long
	 * </p>
	 * 
	 * @param context
	 *            context.
	 * @param component
	 *            component.
	 * @param value
	 *            value.
	 * 
	 * @throws ValidatorException
	 *             Excep��o se h� algum erro de valida��o.
	 * 
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String[] params;

		StringBuffer fieldBuffer = new StringBuffer(component.getId());
		fieldBuffer.append("_long");

		if (!validaLong(String.valueOf(value))) {
			params = new String[0];
			addErrorMessage(params, fieldBuffer.toString(), context, component);
		}

	}

	/**
	 * Valida long.
	 *
	 * @param value the value
	 * @return true, if valida long
	 */
	private boolean validaLong(String value) {
		try {
			Long.parseLong(value);
		} catch (NumberFormatException e) {
			return false;
		}

		return true;
	}

	/**
	 * Get: validatorLongId.
	 *
	 * @return validatorLongId
	 */
	public static String getValidatorLongId() {
		return validatorLongId;
	}

	/**
	 * Set: validatorLongId.
	 *
	 * @param validatorLongId the validator long id
	 */
	public static void setValidatorLongId(String validatorLongId) {
		ValidatorLong.validatorLongId = validatorLongId;
	}

	/**
	 * Get: id.
	 *
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set: id.
	 *
	 * @param id the id
	 */
	public void setId(String id) {
		this.id = id;
	}

}
