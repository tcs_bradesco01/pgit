package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.confirmarassinaturacontratocontingencia;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.bean.RegistrarAssinaturaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.bean.RegistrarAssinaturaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.registrarassinaturacontrato.impl.RegistrarAssinaturaContratoServiceImpl;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;


public class ConfirmarAssinaturaContratoContingenciaBean {

    /** Atributo identificacaoClienteContratoBean. */
    private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

    /** Atributo comboService. */
    private IComboService comboService;

    /** Atributo registrarAssinaturaContratoServiceImpl. */
    private RegistrarAssinaturaContratoServiceImpl registrarAssinaturaContratoServiceImpl;

    /** Atributo cnpjCpfMaster. */
    private String cnpjCpfMaster;

    /** Atributo nomeRazaoSocialMaster. */
    private String nomeRazaoSocialMaster;

    /** Atributo grupoEconomico. */
    private String grupoEconomico;

    /** Atributo segmento. */
    private String segmento;

    /** Atributo subSegmento. */
    private String subSegmento;

    /** Atributo atividadeEconomico. */
    private String atividadeEconomico;

    /** Atributo dsAgenciaGestora. */
    private String dsAgenciaGestora;

    /** Atributo gerenteResponsavel. */
    private String gerenteResponsavel;

    /** Atributo empresaGestora. */
    private String empresaGestora;

    /** Atributo cdEmpresa. */
    private Long cdEmpresa;

    /** Atributo situacao. */
    private String situacao;

    /** Atributo tipo. */
    private String tipo;

    /** Atributo cdTipo. */
    private Integer cdTipo;

    /** Atributo numero. */
    private String numero;

    /** Atributo motivo. */
    private String motivo;

    /** Atributo participacao. */
    private String participacao;

    /** Atributo dsContrato. */
    private String dsContrato;

    /** Atributo numContratoFisico. */
    private String numContratoFisico;

    /** Atributo dataAssinatura. */
    private String dataAssinatura;

    /** Atributo obrigatoriedade. */
    private String obrigatoriedade;

    /** Atributo itemSelecionadoSolicitacao. */
    private Integer itemSelecionadoSolicitacao;

    /** Atributo dataDeFiltro. */
    private Date dataDeFiltro;

    /** Atributo motivoAtivacaoDesc. */
    private String motivoAtivacaoDesc;

    /** Atributo listaMotivoAtual. */
    private List<SelectItem> listaMotivoAtual = new ArrayList<SelectItem>();    

    /**
     * Limpar pagina.
     *
     * @param evt the evt
     */
    public void limparPagina(ActionEvent evt) {

        this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
        this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
        this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

        //carrega os combos da tela de pesquisa
        identificacaoClienteContratoBean.listarEmpresaGestora();
        identificacaoClienteContratoBean.listarTipoContrato();
        identificacaoClienteContratoBean.listarSituacaoContrato();

        this.setItemSelecionadoSolicitacao(null);

        identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
        identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteRegAssinaturaContrato");
        identificacaoClienteContratoBean.setPaginaRetorno("confirmarAssinaturaContratoContingencia");
        identificacaoClienteContratoBean.setItemClienteSelecionado(null);
        identificacaoClienteContratoBean.setItemSelecionadoLista(null);

        this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
        this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

        identificacaoClienteContratoBean.limparDadosPesquisaContrato();     
        identificacaoClienteContratoBean.limparDadosCliente();
        identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

        identificacaoClienteContratoBean.setObrigatoriedade("T");
        identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);


        limpaDados();
    }

    /**
     * Avancar registrar contrato confirmar.
     *
     * @return the string
     */
    public String avancarRegistrarContratoConfirmar(){

        if(getObrigatoriedade() != null && getObrigatoriedade().equals("T")){
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");         
            setDataAssinatura(df.format(getDataDeFiltro()));


            return "AVANCAR_CONFIRMAR";
        }else if(getObrigatoriedade().equals("F")){
            return "";
        }

        return "";
    }

    /**
     * Confirma registro.
     *
     * @return the string
     */
    public String confirmaRegistro(){
        try {

            RegistrarAssinaturaContratoEntradaDTO registrarAssinaturaContratoEntradaDTO = new RegistrarAssinaturaContratoEntradaDTO();

            /*Dados Contratos*/
            ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

            registrarAssinaturaContratoEntradaDTO.setCdPessoaJuridicaContrato(Integer.parseInt(String.valueOf(saidaDTO.getCdPessoaJuridica())));
            registrarAssinaturaContratoEntradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
            registrarAssinaturaContratoEntradaDTO.setNrSequenciaContratoNegocio(String.valueOf(saidaDTO.getNrSequenciaContrato()));
            registrarAssinaturaContratoEntradaDTO.setNumContratoFisico(getNumContratoFisico());


            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

            registrarAssinaturaContratoEntradaDTO.setDtAssinaturaAditivo(df.format(getDataDeFiltro()));

            RegistrarAssinaturaContratoSaidaDTO registrarAssinaturaContratoSaidaDTO = getRegistrarAssinaturaContratoServiceImpl().registrarAssinaturaContratoSaidaDTO(registrarAssinaturaContratoEntradaDTO);



            BradescoFacesUtils.addInfoModalMessage("(" + registrarAssinaturaContratoSaidaDTO.getCodMensagem() + ") " +registrarAssinaturaContratoSaidaDTO.getMensagem(), "#{registrarAssinaturaContratoBean.limparRetorno}", BradescoViewExceptionActionType.ACTION, false);

            //identificacaoClienteContratoBean.consultarContrato();

        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false); 
            return null;
        } 

        return "" ;
    }

    public String limparRetorno() {

        this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
        this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
        this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

        //carrega os combos da tela de pesquisa
        identificacaoClienteContratoBean.listarEmpresaGestora();
        identificacaoClienteContratoBean.listarTipoContrato();
        identificacaoClienteContratoBean.listarSituacaoContrato();

        this.setItemSelecionadoSolicitacao(null);

        identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
        identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteRegAssinaturaContrato");
        identificacaoClienteContratoBean.setPaginaRetorno("confirmarAssinaturaContratoContingencia");
        identificacaoClienteContratoBean.setItemClienteSelecionado(null);
        identificacaoClienteContratoBean.setItemSelecionadoLista(null);

        this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
        this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

        identificacaoClienteContratoBean.limparDadosPesquisaContrato();     
        identificacaoClienteContratoBean.limparDadosCliente();
        identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

        identificacaoClienteContratoBean.setObrigatoriedade("T");
        identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);


        limpaDados();

        return "confirmarAssinaturaContratoContingencia";
    }

    /**
     * Avancar ativar registrar alt contrato.
     *
     * @return the string
     */
    public String avancarAtivarRegistrarAltContrato(){
        return "AVANCAR_ALTERAR_CONFIRMAR";
    }

    /**
     * Voltar ativar registrar alt contrato.
     *
     * @return the string
     */
    public String voltarAtivarRegistrarAltContrato(){
        return "VOLTAR_ALTERAR_CONFIRMAR";
    }

    /**
     * Avancar ativar registrar formalizacao alt contrato.
     *
     * @return the string
     */
    public String avancarAtivarRegistrarFormalizacaoAltContrato(){
        return "AVANCAR";
    }

    /**
     * Voltar.
     *
     * @return the string
     */
    public String voltar(){
        return "VOLTAR_REGISTRAR";
    }

    /**
     * Voltar con registrar assinatura contrato.
     *
     * @return the string
     */
    public String voltarConRegistrarAssinaturaContrato(){
        identificacaoClienteContratoBean.setItemSelecionadoLista(null);
        return "VOLTAR";
    }

    /**
     * Voltar registrar assinatura contrato.
     *
     * @return the string
     */
    public String voltarRegistrarAssinaturaContrato(){
        return "VOLTAR";
    }


    /**
     * Registrar.
     *
     * @return the string
     */
    public String registrar(){

        ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

        setCnpjCpfMaster(saidaDTO.getCnpjOuCpfFormatado());
        setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
        setGrupoEconomico(saidaDTO.getDsGrupoEconomico());
        setAtividadeEconomico(saidaDTO.getDsAtividadeEconomica());
        setSegmento(saidaDTO.getDsSegmentoCliente());
        setSubSegmento(saidaDTO.getDsSubSegmentoCliente());
        setEmpresaGestora(String.valueOf(saidaDTO.getDsPessoaJuridica()));
        setCdEmpresa(saidaDTO.getCdPessoaJuridica());
        setTipo(saidaDTO.getDsTipoContrato());
        setCdTipo(saidaDTO.getCdTipoContrato());
        setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
        setSituacao(saidaDTO.getDsSituacaoContrato());
        setMotivo(saidaDTO.getDsMotivoSituacao());
        setParticipacao(saidaDTO.getCdTipoParticipacao());
        setDsContrato(saidaDTO.getDsContrato());
        setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
        setGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
        limpaDados();

        return "REGISTRAR";
    }



    /**
     * Limpa dados.
     */
    public void limpaDados(){

        this.setDataDeFiltro((new Date()));
        setNumContratoFisico("");

    }

    /**
     * Voltar ativar assinatura formalizacao alt contrato.
     *
     * @return the string
     */
    public String voltarAtivarAssinaturaFormalizacaoAltContrato(){
        return "VOLTAR";
    }

    /**
     * Get: itemSelecionadoSolicitacao.
     *
     * @return itemSelecionadoSolicitacao
     */
    public Integer getItemSelecionadoSolicitacao() {
        return itemSelecionadoSolicitacao;
    }

    /**
     * Set: itemSelecionadoSolicitacao.
     *
     * @param itemSelecionadoSolicitacao the item selecionado solicitacao
     */
    public void setItemSelecionadoSolicitacao(Integer itemSelecionadoSolicitacao) {
        this.itemSelecionadoSolicitacao = itemSelecionadoSolicitacao;
    }   

    /**
     * Get: listaMotivoAtual.
     *
     * @return listaMotivoAtual
     */
    public List<SelectItem> getListaMotivoAtual() {
        return listaMotivoAtual;
    }

    /**
     * Set: listaMotivoAtual.
     *
     * @param listaMotivoAtual the lista motivo atual
     */
    public void setListaMotivoAtual(List<SelectItem> listaMotivoAtual) {
        this.listaMotivoAtual = listaMotivoAtual;
    }

    /**
     * Get: dataDeFiltro.
     *
     * @return dataDeFiltro
     */
    public Date getDataDeFiltro() {
        return dataDeFiltro;
    }

    /**
     * Set: dataDeFiltro.
     *
     * @param dataDeFiltro the data de filtro
     */
    public void setDataDeFiltro(Date dataDeFiltro) {
        this.dataDeFiltro = dataDeFiltro;
    }

    /**
     * Get: atividadeEconomico.
     *
     * @return atividadeEconomico
     */
    public String getAtividadeEconomico() {
        return atividadeEconomico;
    }

    /**
     * Set: atividadeEconomico.
     *
     * @param atividadeEconomico the atividade economico
     */
    public void setAtividadeEconomico(String atividadeEconomico) {
        this.atividadeEconomico = atividadeEconomico;
    }

    /**
     * Get: cnpjCpfMaster.
     *
     * @return cnpjCpfMaster
     */
    public String getCnpjCpfMaster() {
        return cnpjCpfMaster;
    }

    /**
     * Set: cnpjCpfMaster.
     *
     * @param cnpjCpfMaster the cnpj cpf master
     */
    public void setCnpjCpfMaster(String cnpjCpfMaster) {
        this.cnpjCpfMaster = cnpjCpfMaster;
    }

    /**
     * Get: empresaGestora.
     *
     * @return empresaGestora
     */
    public String getEmpresaGestora() {
        return empresaGestora;
    }

    /**
     * Set: empresaGestora.
     *
     * @param empresaGestora the empresa gestora
     */
    public void setEmpresaGestora(String empresaGestora) {
        this.empresaGestora = empresaGestora;
    }

    /**
     * Get: grupoEconomico.
     *
     * @return grupoEconomico
     */
    public String getGrupoEconomico() {
        return grupoEconomico;
    }

    /**
     * Set: grupoEconomico.
     *
     * @param grupoEconomico the grupo economico
     */
    public void setGrupoEconomico(String grupoEconomico) {
        this.grupoEconomico = grupoEconomico;
    }

    /**
     * Get: motivo.
     *
     * @return motivo
     */
    public String getMotivo() {
        return motivo;
    }

    /**
     * Set: motivo.
     *
     * @param motivo the motivo
     */
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    /**
     * Get: nomeRazaoSocialMaster.
     *
     * @return nomeRazaoSocialMaster
     */
    public String getNomeRazaoSocialMaster() {
        return nomeRazaoSocialMaster;
    }

    /**
     * Set: nomeRazaoSocialMaster.
     *
     * @param nomeRazaoSocialMaster the nome razao social master
     */
    public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
        this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
    }

    /**
     * Get: numero.
     *
     * @return numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Set: numero.
     *
     * @param numero the numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Get: participacao.
     *
     * @return participacao
     */
    public String getParticipacao() {
        return participacao;
    }

    /**
     * Set: participacao.
     *
     * @param participacao the participacao
     */
    public void setParticipacao(String participacao) {
        this.participacao = participacao;
    }

    /**
     * Get: segmento.
     *
     * @return segmento
     */
    public String getSegmento() {
        return segmento;
    }

    /**
     * Set: segmento.
     *
     * @param segmento the segmento
     */
    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    /**
     * Get: situacao.
     *
     * @return situacao
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * Set: situacao.
     *
     * @param situacao the situacao
     */
    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    /**
     * Get: subSegmento.
     *
     * @return subSegmento
     */
    public String getSubSegmento() {
        return subSegmento;
    }

    /**
     * Set: subSegmento.
     *
     * @param subSegmento the sub segmento
     */
    public void setSubSegmento(String subSegmento) {
        this.subSegmento = subSegmento;
    }

    /**
     * Get: tipo.
     *
     * @return tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Set: tipo.
     *
     * @param tipo the tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }





    /**
     * Get: comboService.
     *
     * @return comboService
     */
    public IComboService getComboService() {
        return comboService;
    }

    /**
     * Set: comboService.
     *
     * @param comboService the combo service
     */
    public void setComboService(IComboService comboService) {
        this.comboService = comboService;
    }



    /**
     * Get: identificacaoClienteContratoBean.
     *
     * @return identificacaoClienteContratoBean
     */
    public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
        return identificacaoClienteContratoBean;
    }

    /**
     * Set: identificacaoClienteContratoBean.
     *
     * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
     */
    public void setIdentificacaoClienteContratoBean(
        IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
        this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
    }

    /**
     * Get: cdEmpresa.
     *
     * @return cdEmpresa
     */
    public Long getCdEmpresa() {
        return cdEmpresa;
    }

    /**
     * Set: cdEmpresa.
     *
     * @param cdEmpresa the cd empresa
     */
    public void setCdEmpresa(Long cdEmpresa) {
        this.cdEmpresa = cdEmpresa;
    }

    /**
     * Get: cdTipo.
     *
     * @return cdTipo
     */
    public Integer getCdTipo() {
        return cdTipo;
    }

    /**
     * Set: cdTipo.
     *
     * @param cdTipo the cd tipo
     */
    public void setCdTipo(Integer cdTipo) {
        this.cdTipo = cdTipo;
    }

    /**
     * Get: registrarAssinaturaContratoServiceImpl.
     *
     * @return registrarAssinaturaContratoServiceImpl
     */
    public RegistrarAssinaturaContratoServiceImpl getRegistrarAssinaturaContratoServiceImpl() {
        return registrarAssinaturaContratoServiceImpl;
    }

    /**
     * Set: registrarAssinaturaContratoServiceImpl.
     *
     * @param registrarAssinaturaContratoServiceImpl the registrar assinatura contrato service impl
     */
    public void setRegistrarAssinaturaContratoServiceImpl(
        RegistrarAssinaturaContratoServiceImpl registrarAssinaturaContratoServiceImpl) {
        this.registrarAssinaturaContratoServiceImpl = registrarAssinaturaContratoServiceImpl;
    }

    /**
     * Get: dsAgenciaGestora.
     *
     * @return dsAgenciaGestora
     */
    public String getDsAgenciaGestora() {
        return dsAgenciaGestora;
    }


    /**
     * Set: dsAgenciaGestora.
     *
     * @param dsAgenciaGestora the ds agencia gestora
     */
    public void setDsAgenciaGestora(String dsAgenciaGestora) {
        this.dsAgenciaGestora = dsAgenciaGestora;
    }


    /**
     * Get: dataAssinatura.
     *
     * @return dataAssinatura
     */
    public String getDataAssinatura() {
        return dataAssinatura;
    }

    /**
     * Set: dataAssinatura.
     *
     * @param dataAssinatura the data assinatura
     */
    public void setDataAssinatura(String dataAssinatura) {
        this.dataAssinatura = dataAssinatura;
    }

    /**
     * Get: dsContrato.
     *
     * @return dsContrato
     */
    public String getDsContrato() {
        return dsContrato;
    }

    /**
     * Set: dsContrato.
     *
     * @param dsContrato the ds contrato
     */
    public void setDsContrato(String dsContrato) {
        this.dsContrato = dsContrato;
    }

    /**
     * Get: obrigatoriedade.
     *
     * @return obrigatoriedade
     */
    public String getObrigatoriedade() {
        return obrigatoriedade;
    }

    /**
     * Set: obrigatoriedade.
     *
     * @param obrigatoriedade the obrigatoriedade
     */
    public void setObrigatoriedade(String obrigatoriedade) {
        this.obrigatoriedade = obrigatoriedade;
    }

    /**
     * Get: motivoAtivacaoDesc.
     *
     * @return motivoAtivacaoDesc
     */
    public String getMotivoAtivacaoDesc() {
        return motivoAtivacaoDesc;
    }

    /**
     * Set: motivoAtivacaoDesc.
     *
     * @param motivoAtivacaoDesc the motivo ativacao desc
     */
    public void setMotivoAtivacaoDesc(String motivoAtivacaoDesc) {
        this.motivoAtivacaoDesc = motivoAtivacaoDesc;
    }


    /**
     * Get: numContratoFisico.
     *
     * @return numContratoFisico
     */
    public String getNumContratoFisico() {
        return numContratoFisico;
    }


    /**
     * Set: numContratoFisico.
     *
     * @param numContratoFisico the num contrato fisico
     */
    public void setNumContratoFisico(String numContratoFisico) {
        this.numContratoFisico = numContratoFisico;
    }


    /**
     * Get: gerenteResponsavel.
     *
     * @return gerenteResponsavel
     */
    public String getGerenteResponsavel() {
        return gerenteResponsavel;
    }


    /**
     * Set: gerenteResponsavel.
     *
     * @param gerenteResponsavel the gerente responsavel
     */
    public void setGerenteResponsavel(String gerenteResponsavel) {
        this.gerenteResponsavel = gerenteResponsavel;
    }

}
