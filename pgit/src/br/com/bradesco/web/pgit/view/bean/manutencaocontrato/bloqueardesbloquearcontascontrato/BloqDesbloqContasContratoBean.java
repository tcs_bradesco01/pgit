/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.bloqueardesbloquearcontascontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.bloqueardesbloquearcontascontrato;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.IBloqdesbloqcontacontratoService;
import br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.bean.BloquearDesbloquearContaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.bloqdesbloqcontacontrato.bean.BloquearDesbloquearContaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarFinalidadeContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoVincContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoVincContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarContaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarContaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasVincContEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasVincContSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;


/**
 * Nome: BloqDesbloqContasContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class BloqDesbloqContasContratoBean {
	
	/** Atributo bloqDesbloqContaContratoServiceImpl. */
	private IBloqdesbloqcontacontratoService bloqDesbloqContaContratoServiceImpl;
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo hiddenProsseguir. */
	private String hiddenProsseguir;
	
	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;
	
//ATRIBUTOS Pesquisa
	/** Atributo cnpjFiltro. */
private String cnpjFiltro;
	
	/** Atributo cnpjFiltro2. */
	private String cnpjFiltro2;
	
	/** Atributo cnpjFiltro3. */
	private String cnpjFiltro3;
	
	/** Atributo cpfFiltro. */
	private String cpfFiltro;
	
	/** Atributo cpfFiltro2. */
	private String cpfFiltro2;
	
	/** Atributo radioPesquisaContas. */
	private String radioPesquisaContas;
	
	
	/** Atributo bancoFiltro. */
	private String bancoFiltro;
	
	/** Atributo agenciaFiltro. */
	private String agenciaFiltro;
	
	/** Atributo agenciaDigitoFiltro. */
	private String agenciaDigitoFiltro;
	
	/** Atributo contaFiltro. */
	private String contaFiltro;
	
	/** Atributo digitoFiltro. */
	private String digitoFiltro;
	
	/** Atributo tipoContaFiltro. */
	private Integer tipoContaFiltro;
	
	/** Atributo finalidadeFiltro. */
	private Integer finalidadeFiltro;
	
	/** Atributo listaTipoConta. */
	private List<SelectItem> listaTipoConta =  new ArrayList<SelectItem>();
	
	/** Atributo listaFinalidadeConta. */
	private List<SelectItem> listaFinalidadeConta =  new ArrayList<SelectItem>();
	
	
		
	/** Atributo bloqDesbloqFiltro. */
	private Integer bloqDesbloqFiltro;
	
	/** Atributo motivoBloqueio. */
	private Integer motivoBloqueio;
	
	/** Atributo listaMotivoBloqueio. */
	private List<SelectItem> listaMotivoBloqueio = new ArrayList<SelectItem>();
	
	/** Atributo listaMotivoBloqueioHash. */
	private Map<Integer, String> listaMotivoBloqueioHash = new HashMap<Integer, String>();
	
	/** Atributo motivoBloqueioDesc. */
	private String motivoBloqueioDesc;
	
	/** Atributo btnAvancar. */
	private boolean btnAvancar;
//Fim ATRIBUTOS Pesquisa	
	
// atributo listaGrid deve ser trocado por uma lista do DTO
	/** Atributo listaGrid. */
private List<ListarContasVincContSaidaDTO> listaGrid;
	
	/** Atributo listaGridControle. */
	private List<SelectItem> listaGridControle;
	
	/** Atributo itemSelecionadoGrid. */
	private Integer itemSelecionadoGrid;
	
//Variaveis Cliente Master - Cliente - Contrato - Conta
	/** Atributo cpfCnpj. */
private String cpfCnpj;
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;
	
	/** Atributo cpfCnpjMaster. */
	private String cpfCnpjMaster;
	
	/** Atributo gerenteResponsavel. */
	private String gerenteResponsavel;
	
	/** Atributo clienteParticipante. */
	private String clienteParticipante;
	
	/** Atributo nomeRazaoMaster. */
	private String nomeRazaoMaster;
	
	/** Atributo grupoEconomicoMaster. */
	private String grupoEconomicoMaster;
	
	/** Atributo ativEconomicaMaster. */
	private String ativEconomicaMaster;
	
	/** Atributo segmentoMaster. */
	private String segmentoMaster;
	
	/** Atributo subSegmentoMaster. */
	private String subSegmentoMaster;
	
	/** Atributo cpfCnpjCliente. */
	private String cpfCnpjCliente;
	
	/** Atributo nomeRazaoCliente. */
	private String nomeRazaoCliente;
	
	/** Atributo empresaContrato. */
	private String empresaContrato;
	
	/** Atributo tipoContrato. */
	private String tipoContrato;
	
	/** Atributo numeroContrato. */
	private String numeroContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo situacaoContrato. */
	private String situacaoContrato;
	
	/** Atributo motivoContrato. */
	private String motivoContrato;
	
	/** Atributo participacaoContrato. */
	private String participacaoContrato;	
	
	/** Atributo cdPessoaJuridica. */
	private Long   cdPessoaJuridica;
	
	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;
	
	/** Atributo bancoConta. */
	private String bancoConta;
	
	/** Atributo agenciaConta. */
	private String agenciaConta;
	
	/** Atributo conta. */
	private String conta;
	
	/** Atributo tipoConta. */
	private String tipoConta;
	
	/** Atributo tipoVinculoConta. */
	private String tipoVinculoConta;
	
	/** Atributo situacaoConta. */
	private String situacaoConta;
	
	/** Atributo cdSituacaoConta. */
	private Integer cdSituacaoConta;	 
	
	/** Atributo dataVinculacaoContaDesc. */
	private String dataVinculacaoContaDesc;
	
	/** Atributo dataVinculacaoConta. */
	private String dataVinculacaoConta;
	
	/** Atributo consultarContaContratoSaidaDTO. */
	private ConsultarContaContratoSaidaDTO consultarContaContratoSaidaDTO;
	
//Fim Variaveis Cliente Master - Cliente - Contrato - Conta

//M�TODOS
	/**
 * Habilita avancar.
 */
public void habilitaAvancar(){
		if(this.getMotivoBloqueio() != 0){
			setBtnAvancar(true);
		}else{
			setBtnAvancar(false);
		}
	}
	
	/**
	 * Carrega lista motivo.
	 */
	public void carregaListaMotivo(){
		this.listaMotivoBloqueio.clear();
		if(this.getBloqDesbloqFiltro() == 1){
			listarMotivoSituacaoVinculo(2);
		}else
			if(this.getBloqDesbloqFiltro() == 2){
				listarMotivoSituacaoVinculo(1);
			}
		setMotivoBloqueio(0);
		setBtnAvancar(false);
	}
	
	/**
	 * Listar motivo situacao vinculo.
	 *
	 * @param a the a
	 */
	public void listarMotivoSituacaoVinculo(int a){
		listaMotivoBloqueio = new ArrayList<SelectItem>();
		List<ListarMotivoSituacaoVincContratoSaidaDTO> list = new ArrayList<ListarMotivoSituacaoVincContratoSaidaDTO>();
		ListarMotivoSituacaoVincContratoEntradaDTO entradaDTO = new ListarMotivoSituacaoVincContratoEntradaDTO();
		       
		entradaDTO.setCdSituacaoVincConta(a);
		
		list = comboService.listarMotivoSituacaoVincContrato(entradaDTO);
							  
		listaMotivoBloqueio.clear();
		for(ListarMotivoSituacaoVincContratoSaidaDTO combo : list){
			listaMotivoBloqueioHash.put(combo.getCdMotivoSituacaoContrato(), combo.getDsMotivoSituacaoContrato());
			listaMotivoBloqueio.add(new SelectItem(combo.getCdMotivoSituacaoContrato(), combo.getDsMotivoSituacaoContrato()));
		}
	}
	
	/**
	 * Limpar.
	 */
	public void limpar(){
		setListaGrid(null);
		setItemSelecionadoGrid(null);
		setListaGridControle(null);
	}
	
	/**
	 * Limpar2.
	 */
	public void limpar2(){
		setBloqDesbloqFiltro(null);
		setMotivoBloqueio(0);
		setBtnAvancar(false);
	}
	
	/**
	 * Limpar dados.
	 */
	public void limparDados(){
		setCnpjFiltro("");	
		setCnpjFiltro2("");	
		setCnpjFiltro3("");	
		setCpfFiltro("");
		setCpfFiltro2("");
		setAgenciaFiltro("");
		setBancoFiltro("");
		setAgenciaFiltro("");
		setContaFiltro("");
		setDigitoFiltro("");
		setFinalidadeFiltro(null);		
		setTipoContaFiltro(null);
		setAgenciaDigitoFiltro("");
		
		setRadioPesquisaContas(null);
		
		setListaGrid(null);
		setItemSelecionadoGrid(null);
		setListaGridControle(null);
	}
	
	/**
	 * Carrega lista contas.
	 *
	 * @return the string
	 */
	public String carregaListaContas(){
		listaGrid = new ArrayList<ListarContasVincContSaidaDTO>();
		try{
			ListarContasVincContEntradaDTO entradaDTO = new ListarContasVincContEntradaDTO();
			
			entradaDTO.setCdAgencia((getAgenciaFiltro()==null || getAgenciaFiltro().equals("")) ? 0: Integer.parseInt(getAgenciaFiltro()));					
			entradaDTO.setCdDigitoAgencia((getAgenciaDigitoFiltro()==null || getAgenciaDigitoFiltro().equals("")) ? 0: Integer.parseInt(getAgenciaDigitoFiltro()));
			
			entradaDTO.setCdBanco((getBancoFiltro()==null || getBancoFiltro().equals("")) ?  0: Integer.parseInt(getBancoFiltro()));
			
			entradaDTO.setCdConta((getContaFiltro()==null || getContaFiltro().equals("")) ? 0: Long.parseLong((getContaFiltro())));
			entradaDTO.setCdDigitoConta((getDigitoFiltro()==null || getDigitoFiltro().equals("")) ? "": getDigitoFiltro());

			entradaDTO.setCdCorpoCfpCnpj(( getCnpjFiltro() == null || getCnpjFiltro().equals("")) ? 0: Long.parseLong(getCnpjFiltro()));
			entradaDTO.setCdControleCpfCnpj(( getCnpjFiltro2()== null || getCnpjFiltro2().equals("")) ? 0: Integer.parseInt(getCnpjFiltro2()));
			entradaDTO.setCdDigitoCpfCnpj(( getCnpjFiltro3() == null || getCnpjFiltro3().equals("")) ? 0: Integer.parseInt(getCnpjFiltro3()));

			entradaDTO.setCdFinalidade(getFinalidadeFiltro() != null ? getFinalidadeFiltro():0);
			entradaDTO.setCdTipoConta(getTipoContaFiltro()!=null ? getTipoContaFiltro():0);
			
			entradaDTO.setCdPessoaJuridicaNegocio(getCdPessoaJuridica() !=null ? getCdPessoaJuridica():0);
			entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato() != null ? getCdTipoContrato():0);
			entradaDTO.setNrSequenciaContratoNegocio(getNumeroContrato()!= null ? (Long.parseLong(getNumeroContrato())):0);
					
			setListaGrid(getManterContratoImpl().listarContasVinculadasContrato(entradaDTO));
			
			listaGridControle = new ArrayList<SelectItem>();
			for(int i=0;i<getListaGrid().size();i++){
				listaGridControle.add(new SelectItem(i,""));
			}
			setItemSelecionadoGrid(null);
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);
			setItemSelecionadoGrid(null);
		}
		
		return "";
		
	}
	
	/**
	 * Iniciar pagina.
	 *
	 * @param evt the evt
	 */
	public void iniciarPagina(ActionEvent evt){
		setBloqDesbloqFiltro(null);
		
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		//carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteBloqDesbloqContasContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conBloquearDesbloquearContasContrato");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		
		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();		
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
		
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		
		setListaMotivoBloqueio(new ArrayList<SelectItem>());		
	}
	
	/**
	 * Carrega lista tipo conta.
	 */
	public void carregaListaTipoConta(){
		listaTipoConta = new ArrayList<SelectItem>();
		List<ListarTipoContaSaidaDTO> listaSaida = new ArrayList<ListarTipoContaSaidaDTO>();
		listaSaida = this.getComboService().listarTipoConta();
		
		
		
		for (ListarTipoContaSaidaDTO combo : listaSaida){
			
			this.listaTipoConta.add(new SelectItem(combo.getCdTipoConta(),combo.getDsTipoConta()));
		}
		
	}
	
	/**
	 * Carrega lista finalidade conta.
	 */
	public void carregaListaFinalidadeConta(){
		listaFinalidadeConta = new ArrayList<SelectItem>();
		List<ListarFinalidadeContaSaidaDTO> listaSaida = new ArrayList<ListarFinalidadeContaSaidaDTO>();
		listaSaida = this.getComboService().listarFinalidadeConta();
		
		
		
		for (int i = 0; i< listaSaida.size(); i++){			
			this.listaFinalidadeConta.add(new SelectItem(listaSaida.get(i).getCdFinalidadeContaPagamento(),listaSaida.get(i).getRsFinalidadeContaPagamento()));
		}
		
	}		
	
	/**
	 * Pesquisar.
	 *
	 * @param ev the ev
	 */
	public void pesquisar(ActionEvent ev){
		return;
	}

	

	/**
	 * Voltar contas.
	 *
	 * @return the string
	 */
	public String voltarContas(){
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Bloquear desbloquear.
	 *
	 * @return the string
	 */
	public String bloquearDesbloquear(){
		ListarContasVincContSaidaDTO dto = listaGrid.get(getItemSelecionadoGrid());
		
		setBancoConta(String.valueOf(dto.getCdBanco()));
		setAgenciaConta(String.valueOf(dto.getCdAgencia()));
		setConta(String.valueOf(dto.getCdConta()));
		setTipoConta(dto.getDsTipoConta());
		
		setTipoVinculoConta(String.valueOf(dto.getCdTipoVinculoContrato()));
		
		ConsultarContaContratoEntradaDTO entrada = new ConsultarContaContratoEntradaDTO();

		entrada.setCdPessoaJuridicaVinculo(	dto.getCdPessoaJuridicaVinculo());
		entrada.setCdTipoContratoVinculo(dto.getCdTipoContratoVinculo());
		entrada.setCdTipoVinculoContrato(dto.getCdTipoVinculoContrato());
		entrada.setNrSequenciaContratoVinculo(dto.getNrSequenciaContratoVinculo());			

		entrada.setCdPessoaJuridicaContrato(getCdPessoaJuridica());
		entrada.setCdTipoContratoNegocio(getCdTipoContrato());
		entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNumeroContrato()));

		ConsultarContaContratoSaidaDTO saidaDTO = getManterContratoImpl().consultarContaContrato(entrada);	
		
		
		setSituacaoConta(saidaDTO.getDsSituacaoVinculacaoConta());
		setDataVinculacaoConta(saidaDTO.getHrInclusaoRegistro());
		
		if (saidaDTO.getHrInclusaoRegistro() !=null ){
			
					
			SimpleDateFormat formato1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
			
			try {
				
				
				setDataVinculacaoContaDesc(formato2.format(formato1.parse(saidaDTO.getHrInclusaoRegistro())));
			}  catch (ParseException e) {
				setDataVinculacaoContaDesc("");
			}
			
		}
		
		
		
		setConsultarContaContratoSaidaDTO(saidaDTO);
		
		
		setBloqDesbloqFiltro(0);
		setMotivoBloqueio(0);
		
		return "AVANCAR";
	}
	
	/**
	 * Voltar bloquear desbloquear.
	 *
	 * @return the string
	 */
	public String voltarBloquearDesbloquear(){
		setItemSelecionadoGrid(null);
		return "VOLTAR";
	}
	
	/**
	 * Avancar.
	 *
	 * @return the string
	 */
	public String avancar(){
		setMotivoBloqueioDesc((String)listaMotivoBloqueioHash.get(getMotivoBloqueio()));		
		return "AVANCAR";	
	}
	
	/**
	 * Voltar confirmar.
	 *
	 * @return the string
	 */
	public String voltarConfirmar(){
		return "VOLTAR";
	}
	
	/**
	 * Limpar combo.
	 */
	public void limparCombo(){
		setMotivoBloqueio(0);
	}
	
	/**
	 * Confirmar.
	 *
	 * @return the string
	 */
	public String confirmar(){  
		                             
		try{
			BloquearDesbloquearContaContratoEntradaDTO entradaDTO = new BloquearDesbloquearContaContratoEntradaDTO();
			ListarContasVincContSaidaDTO dto = listaGrid.get(getItemSelecionadoGrid());
			
			entradaDTO.setCdBanco(dto.getCdBanco());
			entradaDTO.setCdComercialAgenciaContabil(dto.getCdAgencia());
			entradaDTO.setCdConta(dto.getCdConta());
			entradaDTO.setCdDigitoConta(Integer.parseInt(dto.getCdDigitoConta()));
			entradaDTO.setCdMotivoBloqueioConta(this.getMotivoBloqueio());
			entradaDTO.setCdPessoaJuridicaContrato(this.getCdPessoaJuridica());
			entradaDTO.setCdPessoaJuridicaVinculo(dto.getCdPessoaJuridicaVinculo());
			entradaDTO.setCdSituacaoContrato(this.getBloqDesbloqFiltro());
			entradaDTO.setCdTipoAcao(getBloqDesbloqFiltro() == 1 ? "B":"D");
			/*Silvia, 

 

			Favor passar o mesmo valor que � passado para o cdTipoContratoVinculo.

			De qualquer maneira tirei a obrigatoriedade do campo.
			
			Fabio de Almeida


			 */
			entradaDTO.setCdTipoContratoConta(dto.getCdTipoContratoVinculo());
			entradaDTO.setCdTipoContratoNegocio(this.getCdTipoContrato());
			entradaDTO.setCdTipoContratoVinculo(dto.getCdTipoContratoVinculo());
			entradaDTO.setCdTipoVinculoContrato(dto.getCdTipoVinculoContrato());
			entradaDTO.setDtInicioVinculacaoContrato(getDataVinculacaoContaDesc());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(this.getNumeroContrato()));
			entradaDTO.setNrSequenciaContratoVinculo(dto.getNrSequenciaContratoVinculo());
			
			BloquearDesbloquearContaContratoSaidaDTO saidaDTO = getBloqDesbloqContaContratoServiceImpl().bloquearDesbloquearContaContrato(entradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "bloquearDesbloquearContasContrato", BradescoViewExceptionActionType.ACTION,false);
			
			carregaListaContas();
			
			}catch(PdcAdapterFunctionalException p){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				return null;
			}
			
		return "";		
	}
	
	/**
	 * Consultar contas.
	 *
	 * @return the string
	 */
	public String consultarContas(){
		
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtivEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdPessoaJuridica(saidaDTO.getCdPessoaJuridica());
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setCdTipoContrato(saidaDTO.getCdTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
		
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		
		} else {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		}
		
		setDescricaoContrato(saidaDTO.getDsContrato());
		
		carregaListaTipoConta();
		carregaListaFinalidadeConta();		
		//listarParticipante();
		
		
		// altera��o conf. doc.27, 28 e 29.07.10 - Corre��es Telas da homologa��o do dia 14 e 15.07.10.ppt
		// slide 108
		
		// Limpa os dados do filtro
		limparDados();
		// Traz a lista carregadas com todos os registros;
		carregaListaContas();
		
		return "CONSULTAR_CONTAS";
	}
	
	
	/**
	 * Limpar campos filtro conta.
	 */
	public void limparCamposFiltroConta() {
		setCnpjFiltro("");	
		setCnpjFiltro2("");	
		setCnpjFiltro3("");	
		setCpfFiltro("");
		setCpfFiltro2("");
		setAgenciaFiltro("");
		setBancoFiltro("");
		setAgenciaFiltro("");
		setContaFiltro("");
		setDigitoFiltro("");
		setFinalidadeFiltro(null);		
		setTipoContaFiltro(null);
		setAgenciaDigitoFiltro("");
		
		// se selecionar filtro por banco, seta 237 para o banco quando clicar 
		if (this.radioPesquisaContas.equals("2")) {
			setBancoFiltro("237");
		}
	
	}	
	
	/**
	 * Limpar dados argumentos pesquisa.
	 */
	public void limparDadosArgumentosPesquisa(){
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(0l);
		identificacaoClienteContratoBean.setTipoContratoFiltro(0);
		identificacaoClienteContratoBean.setNumeroFiltro("");
		identificacaoClienteContratoBean.setAgenciaOperadoraFiltro("");
		identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		identificacaoClienteContratoBean.setCodigoFunc("");
		identificacaoClienteContratoBean.setSituacaoFiltro(0);		
		identificacaoClienteContratoBean.listarMotivoSituacao();
		identificacaoClienteContratoBean.setHabilitaCampos(false);
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setListaGridPesquisa(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		identificacaoClienteContratoBean.setEmpresaGestoraContrato(null);
		setNumeroContrato(null);
		setDescricaoContrato(null);
		setSituacaoContrato(null);
		identificacaoClienteContratoBean.limparRadioFiltro();
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(false);
	}
	
	/**
	 * Consultar contrato.
	 *
	 * @return the string
	 */
	public String consultarContrato(){
		if (identificacaoClienteContratoBean.getObrigatoriedade() != null && identificacaoClienteContratoBean.getObrigatoriedade().equals("T")){

			// desmarca o radio da Grid - vreghini - 01/7/2010.
			identificacaoClienteContratoBean.setItemSelecionadoLista(null);
			identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

			try{
				ListarContratosPgitEntradaDTO entradaDTO = new ListarContratosPgitEntradaDTO();

				entradaDTO.setCdAgencia(identificacaoClienteContratoBean.getAgenciaOperadoraFiltro()!=null&&!identificacaoClienteContratoBean.getAgenciaOperadoraFiltro().equals("")?Integer.parseInt(identificacaoClienteContratoBean.getAgenciaOperadoraFiltro()):0);
				entradaDTO.setCdClubPessoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdClub()!=null?identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdClub():0L);
				entradaDTO.setCdFuncionario(identificacaoClienteContratoBean.getCodigoFunc()!=null&&!identificacaoClienteContratoBean.getCodigoFunc().equals("")?Long.parseLong(identificacaoClienteContratoBean.getCodigoFunc()):0L);				
				entradaDTO.setCdPessoaJuridica(identificacaoClienteContratoBean.getEmpresaGestoraFiltro()!=null?identificacaoClienteContratoBean.getEmpresaGestoraFiltro():0);
				entradaDTO.setCdSituacaoContrato(identificacaoClienteContratoBean.getSituacaoFiltro() != null ? identificacaoClienteContratoBean.getSituacaoFiltro() : 0);
				entradaDTO.setCdTipoContratoNegocio(identificacaoClienteContratoBean.getTipoContratoFiltro()!=null?identificacaoClienteContratoBean.getTipoContratoFiltro():0);
				entradaDTO.setNrSequenciaContrato(identificacaoClienteContratoBean.getNumeroFiltro()!=null&&!identificacaoClienteContratoBean.getNumeroFiltro().equals("")?Long.valueOf(identificacaoClienteContratoBean.getNumeroFiltro()):Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdCpfCnpj());
				entradaDTO.setCdFilialCnpjPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdFilialCnpj());
				entradaDTO.setCdControleCpfPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdControleCnpj());
				
				if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conManterSolManutContrato")) {
					entradaDTO.setParametroPrograma(1);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conManterPendenciasContrato")) {
					entradaDTO.setParametroPrograma(2);
				} else if(identificacaoClienteContratoBean.getPaginaRetorno().equals("conRecuperarContratoEncerrado")) {
					entradaDTO.setParametroPrograma(3);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("pesqContratoPendenteAssinatura")) {
					entradaDTO.setParametroPrograma(4);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conEncerrarContrato")) {
					entradaDTO.setParametroPrograma(5);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("pesqManterSolicitacoesMudancaAmbienteOperacao")) {
					entradaDTO.setParametroPrograma(6);
				} else {
					entradaDTO.setParametroPrograma(0);
				}

				if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conEncerrarContrato")){
					List<ListarContratosPgitSaidaDTO> listaGridTemp = identificacaoClienteContratoBean.getFiltroIdentificaoService().pesquisarManutencaoContrato(entradaDTO);

					identificacaoClienteContratoBean.setListaGridPesquisa(new ArrayList<ListarContratosPgitSaidaDTO>());

					for (ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO : listaGridTemp) {
						if(listarContratosPgitSaidaDTO.getCdSituacaoContrato() == 1 || listarContratosPgitSaidaDTO.getCdSituacaoContrato() == 3){
							identificacaoClienteContratoBean.getListaGridPesquisa().add(listarContratosPgitSaidaDTO);
						}
					}
				}
				else{
					identificacaoClienteContratoBean.setSaidaContrato(new ListarContratosPgitSaidaDTO());
					identificacaoClienteContratoBean.setListaGridPesquisa(identificacaoClienteContratoBean.getFiltroIdentificaoService().pesquisarManutencaoContrato(entradaDTO));
					identificacaoClienteContratoBean.setSaidaContrato(identificacaoClienteContratoBean.getListaGridPesquisa().get(0));
					
				}

				this.identificacaoClienteContratoBean.setListaControleRadio(new ArrayList<SelectItem>());
				for (int i = 0; i < identificacaoClienteContratoBean.getListaGridPesquisa().size();i++) {
					this.identificacaoClienteContratoBean.getListaControleRadio().add(new SelectItem(i," "));
				}
				bloquearArgumentosPesquisaAposConsulta();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				identificacaoClienteContratoBean.setListaGridPesquisa(null);

			}

			return identificacaoClienteContratoBean.getPaginaRetorno();
		}
		
		return "";
	}
	
	/**
	 * Bloquear argumentos pesquisa apos consulta.
	 */
	public void bloquearArgumentosPesquisaAposConsulta(){
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
	}
	
//	GET/SET	
	
	


	/**
 * Get: listaGrid.
 *
 * @return listaGrid
 */
public List<ListarContasVincContSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarContasVincContSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: bloqDesbloqContaContratoServiceImpl.
	 *
	 * @return bloqDesbloqContaContratoServiceImpl
	 */
	public IBloqdesbloqcontacontratoService getBloqDesbloqContaContratoServiceImpl() {
		return bloqDesbloqContaContratoServiceImpl;
	}

	/**
	 * Set: bloqDesbloqContaContratoServiceImpl.
	 *
	 * @param bloqdesbloqcontacontratoServiceImpl the bloq desbloq conta contrato service impl
	 */
	public void setBloqDesbloqContaContratoServiceImpl(
			IBloqdesbloqcontacontratoService bloqdesbloqcontacontratoServiceImpl) {
		this.bloqDesbloqContaContratoServiceImpl = bloqdesbloqcontacontratoServiceImpl;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: ativEconomicaMaster.
	 *
	 * @return ativEconomicaMaster
	 */
	public String getAtivEconomicaMaster() {
		return ativEconomicaMaster;
	}

	/**
	 * Set: ativEconomicaMaster.
	 *
	 * @param ativEconomicaMaster the ativ economica master
	 */
	public void setAtivEconomicaMaster(String ativEconomicaMaster) {
		this.ativEconomicaMaster = ativEconomicaMaster;
	}

	/**
	 * Get: cpfCnpjCliente.
	 *
	 * @return cpfCnpjCliente
	 */
	public String getCpfCnpjCliente() {
		return cpfCnpjCliente;
	}

	/**
	 * Set: cpfCnpjCliente.
	 *
	 * @param cpfCnpjCliente the cpf cnpj cliente
	 */
	public void setCpfCnpjCliente(String cpfCnpjCliente) {
		this.cpfCnpjCliente = cpfCnpjCliente;
	}

	/**
	 * Get: cpfCnpjMaster.
	 *
	 * @return cpfCnpjMaster
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}

	/**
	 * Set: cpfCnpjMaster.
	 *
	 * @param cpfCnpjMaster the cpf cnpj master
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}



	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: grupoEconomicoMaster.
	 *
	 * @return grupoEconomicoMaster
	 */
	public String getGrupoEconomicoMaster() {
		return grupoEconomicoMaster;
	}

	/**
	 * Set: grupoEconomicoMaster.
	 *
	 * @param grupoEconomicoMaster the grupo economico master
	 */
	public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
		this.grupoEconomicoMaster = grupoEconomicoMaster;
	}



	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: nomeRazaoCliente.
	 *
	 * @return nomeRazaoCliente
	 */
	public String getNomeRazaoCliente() {
		return nomeRazaoCliente;
	}

	/**
	 * Set: nomeRazaoCliente.
	 *
	 * @param nomeRazaoCliente the nome razao cliente
	 */
	public void setNomeRazaoCliente(String nomeRazaoCliente) {
		this.nomeRazaoCliente = nomeRazaoCliente;
	}

	/**
	 * Get: nomeRazaoMaster.
	 *
	 * @return nomeRazaoMaster
	 */
	public String getNomeRazaoMaster() {
		return nomeRazaoMaster;
	}

	/**
	 * Set: nomeRazaoMaster.
	 *
	 * @param nomeRazaoMaster the nome razao master
	 */
	public void setNomeRazaoMaster(String nomeRazaoMaster) {
		this.nomeRazaoMaster = nomeRazaoMaster;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}



	/**
	 * Get: segmentoMaster.
	 *
	 * @return segmentoMaster
	 */
	public String getSegmentoMaster() {
		return segmentoMaster;
	}

	/**
	 * Set: segmentoMaster.
	 *
	 * @param segmentoMaster the segmento master
	 */
	public void setSegmentoMaster(String segmentoMaster) {
		this.segmentoMaster = segmentoMaster;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: subSegmentoMaster.
	 *
	 * @return subSegmentoMaster
	 */
	public String getSubSegmentoMaster() {
		return subSegmentoMaster;
	}

	/**
	 * Set: subSegmentoMaster.
	 *
	 * @param subSegmentoMaster the sub segmento master
	 */
	public void setSubSegmentoMaster(String subSegmentoMaster) {
		this.subSegmentoMaster = subSegmentoMaster;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: agenciaConta.
	 *
	 * @return agenciaConta
	 */
	public String getAgenciaConta() {
		return agenciaConta;
	}

	/**
	 * Set: agenciaConta.
	 *
	 * @param agenciaConta the agencia conta
	 */
	public void setAgenciaConta(String agenciaConta) {
		this.agenciaConta = agenciaConta;
	}

	/**
	 * Get: bancoConta.
	 *
	 * @return bancoConta
	 */
	public String getBancoConta() {
		return bancoConta;
	}

	/**
	 * Set: bancoConta.
	 *
	 * @param bancoConta the banco conta
	 */
	public void setBancoConta(String bancoConta) {
		this.bancoConta = bancoConta;
	}

	/**
	 * Get: bloqDesbloqFiltro.
	 *
	 * @return bloqDesbloqFiltro
	 */
	public Integer getBloqDesbloqFiltro() {
		return bloqDesbloqFiltro;
	}

	/**
	 * Set: bloqDesbloqFiltro.
	 *
	 * @param bloqDesbloqFiltro the bloq desbloq filtro
	 */
	public void setBloqDesbloqFiltro(Integer bloqDesbloqFiltro) {
		this.bloqDesbloqFiltro = bloqDesbloqFiltro;
	}

	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * Get: dataVinculacaoConta.
	 *
	 * @return dataVinculacaoConta
	 */
	public String getDataVinculacaoConta() {
		return dataVinculacaoConta;
	}

	/**
	 * Set: dataVinculacaoConta.
	 *
	 * @param dataVinculacaoConta the data vinculacao conta
	 */
	public void setDataVinculacaoConta(String dataVinculacaoConta) {
		this.dataVinculacaoConta = dataVinculacaoConta;
	}

	/**
	 * Get: situacaoConta.
	 *
	 * @return situacaoConta
	 */
	public String getSituacaoConta() {
		return situacaoConta;
	}

	/**
	 * Set: situacaoConta.
	 *
	 * @param situacaoConta the situacao conta
	 */
	public void setSituacaoConta(String situacaoConta) {
		this.situacaoConta = situacaoConta;
	}

	/**
	 * Get: tipoConta.
	 *
	 * @return tipoConta
	 */
	public String getTipoConta() {
		return tipoConta;
	}

	/**
	 * Set: tipoConta.
	 *
	 * @param tipoConta the tipo conta
	 */
	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}

	/**
	 * Get: tipoVinculoConta.
	 *
	 * @return tipoVinculoConta
	 */
	public String getTipoVinculoConta() {
		return tipoVinculoConta;
	}

	/**
	 * Set: tipoVinculoConta.
	 *
	 * @param tipoVinculoConta the tipo vinculo conta
	 */
	public void setTipoVinculoConta(String tipoVinculoConta) {
		this.tipoVinculoConta = tipoVinculoConta;
	}

	/**
	 * Get: listaMotivoBloqueio.
	 *
	 * @return listaMotivoBloqueio
	 */
	public List<SelectItem> getListaMotivoBloqueio() {
		return listaMotivoBloqueio;
	}

	/**
	 * Set: listaMotivoBloqueio.
	 *
	 * @param listaMotivoBloqueio the lista motivo bloqueio
	 */
	public void setListaMotivoBloqueio(List<SelectItem> listaMotivoBloqueio) {
		this.listaMotivoBloqueio = listaMotivoBloqueio;
	}

	/**
	 * Get: motivoBloqueio.
	 *
	 * @return motivoBloqueio
	 */
	public Integer getMotivoBloqueio() {
		return motivoBloqueio;
	}

	/**
	 * Set: motivoBloqueio.
	 *
	 * @param motivoBloqueio the motivo bloqueio
	 */
	public void setMotivoBloqueio(Integer motivoBloqueio) {
		this.motivoBloqueio = motivoBloqueio;
	}


	/**
	 * Get: motivoBloqueioDesc.
	 *
	 * @return motivoBloqueioDesc
	 */
	public String getMotivoBloqueioDesc() {
		return motivoBloqueioDesc;
	}

	/**
	 * Set: motivoBloqueioDesc.
	 *
	 * @param motivoBloqueioDesc the motivo bloqueio desc
	 */
	public void setMotivoBloqueioDesc(String motivoBloqueioDesc) {
		this.motivoBloqueioDesc = motivoBloqueioDesc;
	}

	/**
	 * Get: hiddenProsseguir.
	 *
	 * @return hiddenProsseguir
	 */
	public String getHiddenProsseguir() {
		return hiddenProsseguir;
	}

	/**
	 * Set: hiddenProsseguir.
	 *
	 * @param hiddenProsseguir the hidden prosseguir
	 */
	public void setHiddenProsseguir(String hiddenProsseguir) {
		this.hiddenProsseguir = hiddenProsseguir;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: cdPessoaJuridica.
	 *
	 * @return cdPessoaJuridica
	 */
	public Long getCdPessoaJuridica() {
		return cdPessoaJuridica;
	}

	/**
	 * Set: cdPessoaJuridica.
	 *
	 * @param cdPessoaJuridica the cd pessoa juridica
	 */
	public void setCdPessoaJuridica(Long cdPessoaJuridica) {
		this.cdPessoaJuridica = cdPessoaJuridica;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: itemSelecionadoGrid.
	 *
	 * @return itemSelecionadoGrid
	 */
	public Integer getItemSelecionadoGrid() {
		return itemSelecionadoGrid;
	}

	/**
	 * Set: itemSelecionadoGrid.
	 *
	 * @param itemSelecionadoGrid the item selecionado grid
	 */
	public void setItemSelecionadoGrid(Integer itemSelecionadoGrid) {
		this.itemSelecionadoGrid = itemSelecionadoGrid;
	}

	/**
	 * Get: listaGridControle.
	 *
	 * @return listaGridControle
	 */
	public List<SelectItem> getListaGridControle() {
		return listaGridControle;
	}

	/**
	 * Set: listaGridControle.
	 *
	 * @param listaGridControle the lista grid controle
	 */
	public void setListaGridControle(List<SelectItem> listaGridControle) {
		this.listaGridControle = listaGridControle;
	}
	

	/**
	 * Get: listaMotivoBloqueioHash.
	 *
	 * @return listaMotivoBloqueioHash
	 */
	public Map<Integer, String> getListaMotivoBloqueioHash() {
		return listaMotivoBloqueioHash;
	}

	/**
	 * Set lista motivo bloqueio hash.
	 *
	 * @param listaMotivoBloqueioHash the lista motivo bloqueio hash
	 */
	public void setListaMotivoBloqueioHash(
			Map<Integer, String> listaMotivoBloqueioHash) {
		this.listaMotivoBloqueioHash = listaMotivoBloqueioHash;
	}

	/**
	 * Is btn avancar.
	 *
	 * @return true, if is btn avancar
	 */
	public boolean isBtnAvancar() {
		return btnAvancar;
	}

	/**
	 * Set: btnAvancar.
	 *
	 * @param btnAvancar the btn avancar
	 */
	public void setBtnAvancar(boolean btnAvancar) {
		this.btnAvancar = btnAvancar;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: agenciaDigitoFiltro.
	 *
	 * @return agenciaDigitoFiltro
	 */
	public String getAgenciaDigitoFiltro() {
		return agenciaDigitoFiltro;
	}

	/**
	 * Set: agenciaDigitoFiltro.
	 *
	 * @param agenciaDigitoFiltro the agencia digito filtro
	 */
	public void setAgenciaDigitoFiltro(String agenciaDigitoFiltro) {
		this.agenciaDigitoFiltro = agenciaDigitoFiltro;
	}

	/**
	 * Get: agenciaFiltro.
	 *
	 * @return agenciaFiltro
	 */
	public String getAgenciaFiltro() {
		return agenciaFiltro;
	}

	/**
	 * Set: agenciaFiltro.
	 *
	 * @param agenciaFiltro the agencia filtro
	 */
	public void setAgenciaFiltro(String agenciaFiltro) {
		this.agenciaFiltro = agenciaFiltro;
	}

	/**
	 * Get: bancoFiltro.
	 *
	 * @return bancoFiltro
	 */
	public String getBancoFiltro() {
		return bancoFiltro;
	}

	/**
	 * Set: bancoFiltro.
	 *
	 * @param bancoFiltro the banco filtro
	 */
	public void setBancoFiltro(String bancoFiltro) {
		this.bancoFiltro = bancoFiltro;
	}

	/**
	 * Get: contaFiltro.
	 *
	 * @return contaFiltro
	 */
	public String getContaFiltro() {
		return contaFiltro;
	}

	/**
	 * Set: contaFiltro.
	 *
	 * @param contaFiltro the conta filtro
	 */
	public void setContaFiltro(String contaFiltro) {
		this.contaFiltro = contaFiltro;
	}

	/**
	 * Get: digitoFiltro.
	 *
	 * @return digitoFiltro
	 */
	public String getDigitoFiltro() {
		return digitoFiltro;
	}

	/**
	 * Set: digitoFiltro.
	 *
	 * @param digitoFiltro the digito filtro
	 */
	public void setDigitoFiltro(String digitoFiltro) {
		this.digitoFiltro = digitoFiltro;
	}

	/**
	 * Get: finalidadeFiltro.
	 *
	 * @return finalidadeFiltro
	 */
	public Integer getFinalidadeFiltro() {
		return finalidadeFiltro;
	}

	/**
	 * Set: finalidadeFiltro.
	 *
	 * @param finalidadeFiltro the finalidade filtro
	 */
	public void setFinalidadeFiltro(Integer finalidadeFiltro) {
		this.finalidadeFiltro = finalidadeFiltro;
	}

	/**
	 * Get: radioPesquisaContas.
	 *
	 * @return radioPesquisaContas
	 */
	public String getRadioPesquisaContas() {
		return radioPesquisaContas;
	}

	/**
	 * Set: radioPesquisaContas.
	 *
	 * @param radioPesquisaContas the radio pesquisa contas
	 */
	public void setRadioPesquisaContas(String radioPesquisaContas) {
		this.radioPesquisaContas = radioPesquisaContas;
	}

	/**
	 * Get: tipoContaFiltro.
	 *
	 * @return tipoContaFiltro
	 */
	public Integer getTipoContaFiltro() {
		return tipoContaFiltro;
	}

	/**
	 * Set: tipoContaFiltro.
	 *
	 * @param tipoContaFiltro the tipo conta filtro
	 */
	public void setTipoContaFiltro(Integer tipoContaFiltro) {
		this.tipoContaFiltro = tipoContaFiltro;
	}

	/**
	 * Get: listaTipoConta.
	 *
	 * @return listaTipoConta
	 */
	public List<SelectItem> getListaTipoConta() {
		return listaTipoConta;
	}

	/**
	 * Set: listaTipoConta.
	 *
	 * @param listaTipoConta the lista tipo conta
	 */
	public void setListaTipoConta(List<SelectItem> listaTipoConta) {
		this.listaTipoConta = listaTipoConta;
	}

	/**
	 * Get: listaFinalidadeConta.
	 *
	 * @return listaFinalidadeConta
	 */
	public List<SelectItem> getListaFinalidadeConta() {
		return listaFinalidadeConta;
	}

	/**
	 * Set: listaFinalidadeConta.
	 *
	 * @param listaFinalidadeConta the lista finalidade conta
	 */
	public void setListaFinalidadeConta(List<SelectItem> listaFinalidadeConta) {
		this.listaFinalidadeConta = listaFinalidadeConta;
	}

	/**
	 * Get: cnpjFiltro.
	 *
	 * @return cnpjFiltro
	 */
	public String getCnpjFiltro() {
		return cnpjFiltro;
	}

	/**
	 * Set: cnpjFiltro.
	 *
	 * @param cnpjFiltro the cnpj filtro
	 */
	public void setCnpjFiltro(String cnpjFiltro) {
		this.cnpjFiltro = cnpjFiltro;
	}

	/**
	 * Get: cnpjFiltro2.
	 *
	 * @return cnpjFiltro2
	 */
	public String getCnpjFiltro2() {
		return cnpjFiltro2;
	}

	/**
	 * Set: cnpjFiltro2.
	 *
	 * @param cnpjFiltro2 the cnpj filtro2
	 */
	public void setCnpjFiltro2(String cnpjFiltro2) {
		this.cnpjFiltro2 = cnpjFiltro2;
	}

	/**
	 * Get: cnpjFiltro3.
	 *
	 * @return cnpjFiltro3
	 */
	public String getCnpjFiltro3() {
		return cnpjFiltro3;
	}

	/**
	 * Set: cnpjFiltro3.
	 *
	 * @param cnpjFiltro3 the cnpj filtro3
	 */
	public void setCnpjFiltro3(String cnpjFiltro3) {
		this.cnpjFiltro3 = cnpjFiltro3;
	}

	/**
	 * Get: cpfFiltro.
	 *
	 * @return cpfFiltro
	 */
	public String getCpfFiltro() {
		return cpfFiltro;
	}

	/**
	 * Set: cpfFiltro.
	 *
	 * @param cpfFiltro the cpf filtro
	 */
	public void setCpfFiltro(String cpfFiltro) {
		this.cpfFiltro = cpfFiltro;
	}

	/**
	 * Get: cpfFiltro2.
	 *
	 * @return cpfFiltro2
	 */
	public String getCpfFiltro2() {
		return cpfFiltro2;
	}

	/**
	 * Set: cpfFiltro2.
	 *
	 * @param cpfFiltro2 the cpf filtro2
	 */
	public void setCpfFiltro2(String cpfFiltro2) {
		this.cpfFiltro2 = cpfFiltro2;
	}

	/**
	 * Get: cdSituacaoConta.
	 *
	 * @return cdSituacaoConta
	 */
	public Integer getCdSituacaoConta() {
		return cdSituacaoConta;
	}

	/**
	 * Set: cdSituacaoConta.
	 *
	 * @param cdSituacaoConta the cd situacao conta
	 */
	public void setCdSituacaoConta(Integer cdSituacaoConta) {
		this.cdSituacaoConta = cdSituacaoConta;
	}

	/**
	 * Get: dataVinculacaoContaDesc.
	 *
	 * @return dataVinculacaoContaDesc
	 */
	public String getDataVinculacaoContaDesc() {
		return dataVinculacaoContaDesc;
	}

	/**
	 * Set: dataVinculacaoContaDesc.
	 *
	 * @param dataVinculacaoContaDesc the data vinculacao conta desc
	 */
	public void setDataVinculacaoContaDesc(String dataVinculacaoContaDesc) {
		this.dataVinculacaoContaDesc = dataVinculacaoContaDesc;
	}

	/**
	 * Get: consultarContaContratoSaidaDTO.
	 *
	 * @return consultarContaContratoSaidaDTO
	 */
	public ConsultarContaContratoSaidaDTO getConsultarContaContratoSaidaDTO() {
		return consultarContaContratoSaidaDTO;
	}

	/**
	 * Set: consultarContaContratoSaidaDTO.
	 *
	 * @param consultarContaContratoSaidaDTO the consultar conta contrato saida dto
	 */
	public void setConsultarContaContratoSaidaDTO(
			ConsultarContaContratoSaidaDTO consultarContaContratoSaidaDTO) {
		this.consultarContaContratoSaidaDTO = consultarContaContratoSaidaDTO;
	}
	
	/**
	 * Get: gerenteResponsavel.
	 *
	 * @return gerenteResponsavel
	 */
	public String getGerenteResponsavel() {
		return gerenteResponsavel;
	}

	/**
	 * Set: gerenteResponsavel.
	 *
	 * @param gerenteResponsavel the gerente responsavel
	 */
	public void setGerenteResponsavel(String gerenteResponsavel) {
		this.gerenteResponsavel = gerenteResponsavel;
	}

	/**
	 * Get: clienteParticipante.
	 *
	 * @return clienteParticipante
	 */
	public String getClienteParticipante() {
		return clienteParticipante;
	}

	/**
	 * Set: clienteParticipante.
	 *
	 * @param clienteParticipante the cliente participante
	 */
	public void setClienteParticipante(String clienteParticipante) {
		this.clienteParticipante = clienteParticipante;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
}
