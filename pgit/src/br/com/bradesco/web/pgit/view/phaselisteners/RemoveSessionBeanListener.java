/*
 * Nome: br.com.bradesco.web.pgit.view.phaselisteners
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.phaselisteners;

import java.util.Map;
import java.util.Set;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import br.com.bradesco.web.aq.view.util.FacesUtils;

/**
 * Nome: RemoveSessionBeanListener
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
@SuppressWarnings("serial")
public class RemoveSessionBeanListener implements PhaseListener {

	/** Atributo SAFE_TAB_MENU. */
	private static final String SAFE_TAB_MENU = "stateSafeTabMenu";

	/** Atributo SAFE_ROLLER_MENU. */
	private static final String SAFE_ROLLER_MENU = "stateSafeRollerMenu";

	/**
	 * (non-Javadoc)
	 * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
	 */
	@SuppressWarnings("unchecked")
	public void afterPhase(PhaseEvent event) {
		Map requestMap = event.getFacesContext().getExternalContext().getRequestParameterMap();
		if (requestMap.get(SAFE_TAB_MENU) != null && requestMap.get(SAFE_ROLLER_MENU) != null) {
			Set<String> keys = event.getFacesContext().getExternalContext().getSessionMap().keySet();
			for (String k : keys) {
				Object o = FacesUtils.getSessionAttribute(k);
				if (isPgicManagedBean(o.getClass())) {
					FacesUtils.resetManagedBean(k);
				}
			}
		}
	}

	/**
	 * (non-Javadoc)
	 * @see javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent)
	 */
	public void beforePhase(PhaseEvent arg0) {
	}

	/**
	 * (non-Javadoc)
	 * @see javax.faces.event.PhaseListener#getPhaseId()
	 */
	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}

	/**
	 * Is pgic managed bean.
	 *
	 * @param value the value
	 * @return true, if is pgic managed bean
	 */
	private boolean isPgicManagedBean(Class value) {
		if (value == null) {
			return false;
		}

		String className = value.getName();
		if (className == null || className.trim().equals("")) {
			return false;
		}
		return (className.endsWith("Bean") && className.contains(".pgit."));
	}
}
