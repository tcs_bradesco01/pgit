/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.solmanutcontratos
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.solmanutcontratos;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.ISolManutContratosService;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.CancelarManutencaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.CancelarManutencaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoServicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ConsultarManutencaoServicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarManutencaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solmanutcontratos.bean.ListarManutencaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;


/**
 * Nome: SolicitacoesManutencaoContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolicitacoesManutencaoContratoBean {

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo solManutContratosServiceImpl. */
	private ISolManutContratosService solManutContratosServiceImpl;
	
	/** Atributo listaGridSolicitacao. */
	private List<ListarManutencaoContratoSaidaDTO> listaGridSolicitacao;
	
	/** Atributo itemSelecionadoSolicitacao. */
	private Integer itemSelecionadoSolicitacao;
	
	/** Atributo indicadorTipoManutencao. */
	private Integer indicadorTipoManutencao;
	
	/** Atributo listaControleSolicitacao. */
	private List<SelectItem> listaControleSolicitacao = new ArrayList<SelectItem>();
	
	
	/** Atributo radioFiltroSolicitacoes. */
	private String radioFiltroSolicitacoes;	
	
	/** Atributo tipoManutencaoFiltro. */
	private Integer tipoManutencaoFiltro;
	
	/** Atributo acaoFiltro. */
	private Integer acaoFiltro;
	
	//dados cliente
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;
	
	/** Atributo cpfCnpjMaster. */
	private String cpfCnpjMaster;
	
	/** Atributo nomeRazaoSocialMaster. */
	private String nomeRazaoSocialMaster;
	
	/** Atributo grupoEconomicoMaster. */
	private String grupoEconomicoMaster;
	
	/** Atributo atividadeEconomicaMaster. */
	private String atividadeEconomicaMaster;
	
	/** Atributo segmentoMaster. */
	private String segmentoMaster;
	
	/** Atributo subSegmentoMaster. */
	private String subSegmentoMaster;
	
	/** Atributo cpfParticipante. */
	private String cpfParticipante;
	
	/** Atributo nomeParticipante. */
	private String nomeParticipante;
	
	/** Atributo dataNascimentoConstituicao. */
	private String dataNascimentoConstituicao;
	
	/** Atributo grupoEconomicoParticipante. */
	private String grupoEconomicoParticipante;
	
	/** Atributo tipoParticipacaoContrato. */
	private String tipoParticipacaoContrato;	
	
	/** Atributo banco. */
	private String banco;
	
	/** Atributo agencia. */
	private String agencia;
	
	/** Atributo conta. */
	private String conta;
	
	/** Atributo tipoConta. */
	private String tipoConta;
	
	/** Atributo cpfCnpjTitular. */
	private String cpfCnpjTitular;
	
	/** Atributo nomeRazaoSocialConta. */
	private String nomeRazaoSocialConta;
	
	/** Atributo servico. */
	private String servico;
	
	/** Atributo modalidade. */
	private String modalidade;
	
	/** Atributo situacao. */
	private String situacao;
	
	/** Atributo motivo. */
	private String motivo;
	
	/** Atributo acao. */
	private String acao;
	
	/** Atributo empresa. */
	private String empresa;
	
	/** Atributo cdEmpresa. */
	private Long cdEmpresa;
	
	/** Atributo tipo. */
	private String tipo;
	
	/** Atributo cdTipo. */
	private Integer cdTipo;
	
	/** Atributo numero. */
	private String numero;
	
	/** Atributo motivoDesc. */
	private String motivoDesc;
	
	/** Atributo situacaoDesc. */
	private String situacaoDesc;
	
	/** Atributo participacao. */
	private String participacao;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo desabilitaCamposPesquisa. */
	private boolean desabilitaCamposPesquisa;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;
	
	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;
	
	/** Atributo hrManutencaoRegistro. */
	private String hrManutencaoRegistro;
	
	//trilha
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
		
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		//carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		
		this.setItemSelecionadoSolicitacao(null);
		this.setListaGridSolicitacao(null);
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteSolManutContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conManterSolManutContrato");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		
		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();		
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		
		setDesabilitaCamposPesquisa(false);
		

	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: atividadeEconomicaMaster.
	 *
	 * @return atividadeEconomicaMaster
	 */
	public String getAtividadeEconomicaMaster() {
		return atividadeEconomicaMaster;
	}

	/**
	 * Set: atividadeEconomicaMaster.
	 *
	 * @param atividadeEconomicaMaster the atividade economica master
	 */
	public void setAtividadeEconomicaMaster(String atividadeEconomicaMaster) {
		this.atividadeEconomicaMaster = atividadeEconomicaMaster;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: cpfCnpjMaster.
	 *
	 * @return cpfCnpjMaster
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}

	/**
	 * Set: cpfCnpjMaster.
	 *
	 * @param cpfCnpjMaster the cpf cnpj master
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}

	/**
	 * Get: grupoEconomicoMaster.
	 *
	 * @return grupoEconomicoMaster
	 */
	public String getGrupoEconomicoMaster() {
		return grupoEconomicoMaster;
	}

	/**
	 * Set: grupoEconomicoMaster.
	 *
	 * @param grupoEconomicoMaster the grupo economico master
	 */
	public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
		this.grupoEconomicoMaster = grupoEconomicoMaster;
	}

	/**
	 * Get: itemSelecionadoSolicitacao.
	 *
	 * @return itemSelecionadoSolicitacao
	 */
	public Integer getItemSelecionadoSolicitacao() {
		return itemSelecionadoSolicitacao;
	}

	/**
	 * Set: itemSelecionadoSolicitacao.
	 *
	 * @param itemSelecionadoSolicitacao the item selecionado solicitacao
	 */
	public void setItemSelecionadoSolicitacao(Integer itemSelecionadoSolicitacao) {
		this.itemSelecionadoSolicitacao = itemSelecionadoSolicitacao;
	}

	/**
	 * Get: nomeRazaoSocialMaster.
	 *
	 * @return nomeRazaoSocialMaster
	 */
	public String getNomeRazaoSocialMaster() {
		return nomeRazaoSocialMaster;
	}

	/**
	 * Set: nomeRazaoSocialMaster.
	 *
	 * @param nomeRazaoSocialMaster the nome razao social master
	 */
	public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
		this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
	}

	/**
	 * Get: segmentoMaster.
	 *
	 * @return segmentoMaster
	 */
	public String getSegmentoMaster() {
		return segmentoMaster;
	}

	/**
	 * Set: segmentoMaster.
	 *
	 * @param segmentoMaster the segmento master
	 */
	public void setSegmentoMaster(String segmentoMaster) {
		this.segmentoMaster = segmentoMaster;
	}

	/**
	 * Get: subSegmentoMaster.
	 *
	 * @return subSegmentoMaster
	 */
	public String getSubSegmentoMaster() {
		return subSegmentoMaster;
	}

	/**
	 * Set: subSegmentoMaster.
	 *
	 * @param subSegmentoMaster the sub segmento master
	 */
	public void setSubSegmentoMaster(String subSegmentoMaster) {
		this.subSegmentoMaster = subSegmentoMaster;
	}

	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * Set: agencia.
	 *
	 * @param agencia the agencia
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * Set: banco.
	 *
	 * @param banco the banco
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}

	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * Get: cpfParticipante.
	 *
	 * @return cpfParticipante
	 */
	public String getCpfParticipante() {
		return cpfParticipante;
	}

	/**
	 * Set: cpfParticipante.
	 *
	 * @param cpfParticipante the cpf participante
	 */
	public void setCpfParticipante(String cpfParticipante) {
		this.cpfParticipante = cpfParticipante;
	}

	/**
	 * Get: dataNascimentoConstituicao.
	 *
	 * @return dataNascimentoConstituicao
	 */
	public String getDataNascimentoConstituicao() {
		return dataNascimentoConstituicao;
	}

	/**
	 * Set: dataNascimentoConstituicao.
	 *
	 * @param dataNascimentoConstituicao the data nascimento constituicao
	 */
	public void setDataNascimentoConstituicao(String dataNascimentoConstituicao) {
		this.dataNascimentoConstituicao = dataNascimentoConstituicao;
	}

	/**
	 * Get: grupoEconomicoParticipante.
	 *
	 * @return grupoEconomicoParticipante
	 */
	public String getGrupoEconomicoParticipante() {
		return grupoEconomicoParticipante;
	}

	/**
	 * Set: grupoEconomicoParticipante.
	 *
	 * @param grupoEconomicoParticipante the grupo economico participante
	 */
	public void setGrupoEconomicoParticipante(String grupoEconomicoParticipante) {
		this.grupoEconomicoParticipante = grupoEconomicoParticipante;
	}

	/**
	 * Get: nomeParticipante.
	 *
	 * @return nomeParticipante
	 */
	public String getNomeParticipante() {
		return nomeParticipante;
	}

	/**
	 * Set: nomeParticipante.
	 *
	 * @param nomeParticipante the nome participante
	 */
	public void setNomeParticipante(String nomeParticipante) {
		this.nomeParticipante = nomeParticipante;
	}

	/**
	 * Get: tipoConta.
	 *
	 * @return tipoConta
	 */
	public String getTipoConta() {
		return tipoConta;
	}

	/**
	 * Set: tipoConta.
	 *
	 * @param tipoConta the tipo conta
	 */
	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}

	/**
	 * Get: tipoParticipacaoContrato.
	 *
	 * @return tipoParticipacaoContrato
	 */
	public String getTipoParticipacaoContrato() {
		return tipoParticipacaoContrato;
	}

	/**
	 * Set: tipoParticipacaoContrato.
	 *
	 * @param tipoParticipacaoContrato the tipo participacao contrato
	 */
	public void setTipoParticipacaoContrato(String tipoParticipacaoContrato) {
		this.tipoParticipacaoContrato = tipoParticipacaoContrato;
	}

	/**
	 * Get: cpfCnpjTitular.
	 *
	 * @return cpfCnpjTitular
	 */
	public String getCpfCnpjTitular() {
		return cpfCnpjTitular;
	}

	/**
	 * Set: cpfCnpjTitular.
	 *
	 * @param cpfCnpjTitular the cpf cnpj titular
	 */
	public void setCpfCnpjTitular(String cpfCnpjTitular) {
		this.cpfCnpjTitular = cpfCnpjTitular;
	}

	/**
	 * Get: nomeRazaoSocialConta.
	 *
	 * @return nomeRazaoSocialConta
	 */
	public String getNomeRazaoSocialConta() {
		return nomeRazaoSocialConta;
	}

	/**
	 * Set: nomeRazaoSocialConta.
	 *
	 * @param nomeRazaoSocialConta the nome razao social conta
	 */
	public void setNomeRazaoSocialConta(String nomeRazaoSocialConta) {
		this.nomeRazaoSocialConta = nomeRazaoSocialConta;
	}

	/**
	 * Get: servico.
	 *
	 * @return servico
	 */
	public String getServico() {
		return servico;
	}

	/**
	 * Set: servico.
	 *
	 * @param servico the servico
	 */
	public void setServico(String servico) {
		this.servico = servico;
	}

	/**
	 * Get: acao.
	 *
	 * @return acao
	 */
	public String getAcao() {
		return acao;
	}

	/**
	 * Set: acao.
	 *
	 * @param acao the acao
	 */
	public void setAcao(String acao) {
		this.acao = acao;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: motivo.
	 *
	 * @return motivo
	 */
	public String getMotivo() {
		return motivo;
	}

	/**
	 * Set: motivo.
	 *
	 * @param motivo the motivo
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}

	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Voltar solicitacoes.
	 *
	 * @return the string
	 */
	public String voltarSolicitacoes(){
		setItemSelecionadoSolicitacao(null);
		
		return "VOLTAR_PESQUISAR";
		
	}
	
	/**
	 * Solicitacoes.
	 *
	 * @return the string
	 */
	public String solicitacoes(){
		
		limparGridSolicitacoes();
		
		
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());		
		setDescricaoContrato(saidaDTO.getDsContrato());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
	
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null && !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado().equals("")){
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
			
		}else{
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}
		
		consultarSolicitacoes();
		
		
		return "SOLICITACOES";
		
	}

	/**
	 * Get: radioFiltroSolicitacoes.
	 *
	 * @return radioFiltroSolicitacoes
	 */
	public String getRadioFiltroSolicitacoes() {
		return radioFiltroSolicitacoes;
	}

	/**
	 * Set: radioFiltroSolicitacoes.
	 *
	 * @param radioFiltroSolicitacoes the radio filtro solicitacoes
	 */
	public void setRadioFiltroSolicitacoes(String radioFiltroSolicitacoes) {
		this.radioFiltroSolicitacoes = radioFiltroSolicitacoes;
	}
	
	/**
	 * Limpar campos solicitacoes.
	 */
	public void limparCamposSolicitacoes(){
		
		radioFiltroSolicitacoes = "";		
		tipoManutencaoFiltro = 0;
		acaoFiltro = 0;
		
	}

	/**
	 * Get: acaoFiltro.
	 *
	 * @return acaoFiltro
	 */
	public Integer getAcaoFiltro() {
		return acaoFiltro;
	}

	/**
	 * Set: acaoFiltro.
	 *
	 * @param acaoFiltro the acao filtro
	 */
	public void setAcaoFiltro(Integer acaoFiltro) {
		this.acaoFiltro = acaoFiltro;
	}


	/**
	 * Get: tipoManutencaoFiltro.
	 *
	 * @return tipoManutencaoFiltro
	 */
	public Integer getTipoManutencaoFiltro() {
		return tipoManutencaoFiltro;
	}

	/**
	 * Set: tipoManutencaoFiltro.
	 *
	 * @param tipoManutencaoFiltro the tipo manutencao filtro
	 */
	public void setTipoManutencaoFiltro(Integer tipoManutencaoFiltro) {
		this.tipoManutencaoFiltro = tipoManutencaoFiltro;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}


	/**
	 * Get: listaControleSolicitacao.
	 *
	 * @return listaControleSolicitacao
	 */
	public List<SelectItem> getListaControleSolicitacao() {
		return listaControleSolicitacao;
	}

	/**
	 * Set: listaControleSolicitacao.
	 *
	 * @param listaControleSolicitacao the lista controle solicitacao
	 */
	public void setListaControleSolicitacao(
			List<SelectItem> listaControleSolicitacao) {
		this.listaControleSolicitacao = listaControleSolicitacao;
	}

	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Get: motivoDesc.
	 *
	 * @return motivoDesc
	 */
	public String getMotivoDesc() {
		return motivoDesc;
	}

	/**
	 * Set: motivoDesc.
	 *
	 * @param motivoDesc the motivo desc
	 */
	public void setMotivoDesc(String motivoDesc) {
		this.motivoDesc = motivoDesc;
	}

	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Get: situacaoDesc.
	 *
	 * @return situacaoDesc
	 */
	public String getSituacaoDesc() {
		return situacaoDesc;
	}

	/**
	 * Set: situacaoDesc.
	 *
	 * @param situacaoDesc the situacao desc
	 */
	public void setSituacaoDesc(String situacaoDesc) {
		this.situacaoDesc = situacaoDesc;
	}

	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	/**
	 * Voltar consulta.
	 *
	 * @return the string
	 */
	public String voltarConsulta(){
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		
		return "VOLTAR_CONTRATO";
		
	}
	
	/**
	 * Limpar dados solicitacoes.
	 *
	 * @return the string
	 */
	public String limparDadosSolicitacoes(){
		
		
		setTipoManutencaoFiltro(0);
		setAcaoFiltro(0);
		setListaGridSolicitacao(null);
		setItemSelecionadoSolicitacao(null);
		setDesabilitaCamposPesquisa(false);
		
		return "ok";
		
	}
	
	/**
	 * Limpar grid solicitacoes.
	 *
	 * @return the string
	 */
	public String limparGridSolicitacoes(){
		
		limparDadosSolicitacoes();
		setListaGridSolicitacao(null);
		setItemSelecionadoSolicitacao(null);
		
		
		return "ok";
		
	}
	
	
/*	private void listarAcao() {
		
		this.listaTipoAcao = new ArrayList<SelectItem>();
		
		List<TipoAcaoSaidaDTO> listaTipoAcaoSaida = new ArrayList<TipoAcaoSaidaDTO>();
		
		listaTipoAcaoSaida = comboService.listarTipoAcao();
		
		listaTipoAcaoHash.clear();
		for(TipoAcaoSaidaDTO combo : listaTipoAcaoSaida){
			listaTipoAcaoHash.put(combo.getCdAcao(),combo.getDsAcao());
			this.listaTipoAcao.add(new SelectItem(combo.getCdAcao(),combo.getDsAcao()));
		}
			
		
	}

	public List<SelectItem> getListaTipoAcao() {
		return listaTipoAcao;
	}

	public void setListaTipoAcao(List<SelectItem> listaTipoAcao) {
		this.listaTipoAcao = listaTipoAcao;
	}

	public HashMap getListaTipoAcaoHash() {
		return listaTipoAcaoHash;
	}

	public void setListaTipoAcaoHash(HashMap listaTipoAcaoHash) {
		this.listaTipoAcaoHash = listaTipoAcaoHash;
	}
*/
	

	/**
 * Get: participacao.
 *
 * @return participacao
 */
public String getParticipacao() {
		return participacao;
	}

	/**
	 * Set: participacao.
	 *
	 * @param participacao the participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	
	
	/**
	 * Preenche dados servico.
	 */
	private void preencheDadosServico(){

		ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = getListaGridSolicitacao().get(getItemSelecionadoSolicitacao());
		
		ListarContratosPgitSaidaDTO  listarContratosPgitSaidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		ConsultarManutencaoServicoContratoEntradaDTO consultarManutencaoServicoContratoEntradaDTO = new ConsultarManutencaoServicoContratoEntradaDTO();
		
		consultarManutencaoServicoContratoEntradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
		consultarManutencaoServicoContratoEntradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
		consultarManutencaoServicoContratoEntradaDTO.setCdTipoManutencaoContrato(listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato());
		consultarManutencaoServicoContratoEntradaDTO.setNrManutencaoContratoNegocio(listarManutencaoContratoSaidaDTO.getCdManutencao());
		consultarManutencaoServicoContratoEntradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO.getNrSequenciaContrato());
		
		ConsultarManutencaoServicoContratoSaidaDTO consultarManutencaoServicoContratoSaidaDTO = solManutContratosServiceImpl.detalharServicoContrato(consultarManutencaoServicoContratoEntradaDTO);
	
		setServico(consultarManutencaoServicoContratoSaidaDTO.getDsServico());
		setModalidade(consultarManutencaoServicoContratoSaidaDTO.getDsModalidade());
		
		
		setSituacao(consultarManutencaoServicoContratoSaidaDTO.getDsSituacao());
		setMotivo(consultarManutencaoServicoContratoSaidaDTO.getDsMotivo());
		setAcao(listarManutencaoContratoSaidaDTO.getDsIndicadorTipoManutencao()); 
		
		setUsuarioInclusao(consultarManutencaoServicoContratoSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(consultarManutencaoServicoContratoSaidaDTO.getUsuarioManutencao());
		
		setComplementoInclusao(consultarManutencaoServicoContratoSaidaDTO.getComplementoInclusao().equals("0")? "" : consultarManutencaoServicoContratoSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(consultarManutencaoServicoContratoSaidaDTO.getComplementoManutencao().equals("0")? "" : consultarManutencaoServicoContratoSaidaDTO.getComplementoManutencao());
		setTipoCanalInclusao(consultarManutencaoServicoContratoSaidaDTO.getCdCanalInclusao()==0? "" : consultarManutencaoServicoContratoSaidaDTO.getCdCanalInclusao() + " - " +  consultarManutencaoServicoContratoSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(consultarManutencaoServicoContratoSaidaDTO.getCdCanalManutencao()==0? "" : consultarManutencaoServicoContratoSaidaDTO.getCdCanalManutencao() + " - " + consultarManutencaoServicoContratoSaidaDTO.getDsCanalManutencao()); 
		
		setDataHoraInclusao(consultarManutencaoServicoContratoSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(consultarManutencaoServicoContratoSaidaDTO.getDataHoraManutencao());
	
	
	}

	/**
	 * Preenche dados conta.
	 */
	private void preencheDadosConta(){
		
		
		ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = getListaGridSolicitacao().get(getItemSelecionadoSolicitacao());
		
		ListarContratosPgitSaidaDTO  listarContratosPgitSaidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		ConsultarManutencaoContaEntradaDTO consultarManutencaoContaEntradaDTO = new ConsultarManutencaoContaEntradaDTO();
		
		consultarManutencaoContaEntradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
		consultarManutencaoContaEntradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
		consultarManutencaoContaEntradaDTO.setCdTipoManutencaoContrato(listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato());
		consultarManutencaoContaEntradaDTO.setNrManutencaoContratoNegocio(listarManutencaoContratoSaidaDTO.getCdManutencao());
		consultarManutencaoContaEntradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO.getNrSequenciaContrato());
		
		ConsultarManutencaoContaSaidaDTO consultarManutencaoContaSaidaDTO = solManutContratosServiceImpl.detalharContaContrato(consultarManutencaoContaEntradaDTO);
		
		
		setConta(consultarManutencaoContaSaidaDTO.getNrConta() + "-" + consultarManutencaoContaSaidaDTO.getCdDigitoConta());
		setBanco(consultarManutencaoContaSaidaDTO.getDsBanco());
		setAgencia(consultarManutencaoContaSaidaDTO.getDsComercialAgenciaContabil());
		setTipoConta(consultarManutencaoContaSaidaDTO.getDsTipoContaContrato());
		setCpfCnpjTitular(consultarManutencaoContaSaidaDTO.getCdCpfCnpj());
		setNomeRazaoSocialConta(consultarManutencaoContaSaidaDTO.getNmPessoa());
		
		setSituacao(consultarManutencaoContaSaidaDTO.getDsSituacaoManutencaoContrato());
		setMotivo(consultarManutencaoContaSaidaDTO.getDsMotivoManutencaoContrato());
		setAcao(listarManutencaoContratoSaidaDTO.getDsIndicadorTipoManutencao()); 
				
		setUsuarioInclusao(consultarManutencaoContaSaidaDTO.getCdUsuarioInclusao());
		setUsuarioManutencao(consultarManutencaoContaSaidaDTO.getCdusuarioManutencao());
		
		setComplementoInclusao(consultarManutencaoContaSaidaDTO.getCdOperacaoCanalInclusao().equals("0")? "" : consultarManutencaoContaSaidaDTO.getCdOperacaoCanalInclusao());
		setComplementoManutencao(consultarManutencaoContaSaidaDTO.getCdOperacaoCanalManutencao().equals("0")? "" : consultarManutencaoContaSaidaDTO.getCdOperacaoCanalManutencao());
		setTipoCanalInclusao(consultarManutencaoContaSaidaDTO.getCdTipoCanalInclusao()==0? "" : consultarManutencaoContaSaidaDTO.getCdTipoCanalInclusao() + " - " +  consultarManutencaoContaSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(consultarManutencaoContaSaidaDTO.getCdTipoCanalManutencao()==0? "" : consultarManutencaoContaSaidaDTO.getCdTipoCanalManutencao() + " - " + consultarManutencaoContaSaidaDTO.getDsCanalManutencao()); 
		
		setDataHoraInclusao(consultarManutencaoContaSaidaDTO.getHrInclusaoRegistro());
		setDataHoraManutencao(consultarManutencaoContaSaidaDTO.getHrManutencaoRegistro());
		
	}
	
	/**
	 * Get: listaGridSolicitacao.
	 *
	 * @return listaGridSolicitacao
	 */
	public List<ListarManutencaoContratoSaidaDTO> getListaGridSolicitacao() {
		return listaGridSolicitacao;
	}

	/**
	 * Set: listaGridSolicitacao.
	 *
	 * @param listaGridSolicitacao the lista grid solicitacao
	 */
	public void setListaGridSolicitacao(
			List<ListarManutencaoContratoSaidaDTO> listaGridSolicitacao) {
		this.listaGridSolicitacao = listaGridSolicitacao;
	}

	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Get: cdTipo.
	 *
	 * @return cdTipo
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}

	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Get: solManutContratosServiceImpl.
	 *
	 * @return solManutContratosServiceImpl
	 */
	public ISolManutContratosService getSolManutContratosServiceImpl() {
		return solManutContratosServiceImpl;
	}

	/**
	 * Set: solManutContratosServiceImpl.
	 *
	 * @param solManutContratosServiceImpl the sol manut contratos service impl
	 */
	public void setSolManutContratosServiceImpl(
			ISolManutContratosService solManutContratosServiceImpl) {
		this.solManutContratosServiceImpl = solManutContratosServiceImpl;
	}

	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Get: indicadorTipoManutencao.
	 *
	 * @return indicadorTipoManutencao
	 */
	public Integer getIndicadorTipoManutencao() {
		return indicadorTipoManutencao;
	}

	/**
	 * Set: indicadorTipoManutencao.
	 *
	 * @param indicadorTipoManutencao the indicador tipo manutencao
	 */
	public void setIndicadorTipoManutencao(Integer indicadorTipoManutencao) {
		this.indicadorTipoManutencao = indicadorTipoManutencao;
	}

	/**
	 * Get: modalidade.
	 *
	 * @return modalidade
	 */
	public String getModalidade() {
		return modalidade;
	}

	/**
	 * Set: modalidade.
	 *
	 * @param modalidade the modalidade
	 */
	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		
		ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO =  getListaGridSolicitacao().get(getItemSelecionadoSolicitacao());
		
		try {
			
			// seta label�s com campos do Grid para ser exibido no detalhar.
			setDsTipoManutencao(listarManutencaoContratoSaidaDTO.getDsTipoManutencaoContrato());
			setHrManutencaoRegistro(listarManutencaoContratoSaidaDTO.getHrManutencaoRegistro());
			
			
			if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 1){
				setIndicadorTipoManutencao(1);
				preencheDadosParticipantes();
			}else {
				if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 2){
					setIndicadorTipoManutencao(2);
					preencheDadosConta();
					
				}else {
					
					if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 3){
						setIndicadorTipoManutencao(3);
						preencheDadosServico();
					}else{
						return "";
					}
				}
			}
				
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "conManterSolManutContrato2", BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "DETALHAR";
	}
	
	/**
	 * Preenche dados participantes.
	 */
	private void preencheDadosParticipantes() {
		
		ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = getListaGridSolicitacao().get(getItemSelecionadoSolicitacao());
		
		ListarContratosPgitSaidaDTO  listarContratosPgitSaidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		ConsultarManutencaoParticipanteEntradaDTO  consultarManutencaoParticipanteEntradaDTO = new ConsultarManutencaoParticipanteEntradaDTO();
		
		consultarManutencaoParticipanteEntradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
		consultarManutencaoParticipanteEntradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
		consultarManutencaoParticipanteEntradaDTO.setCdTipoManutencaoContrato(listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato());
		consultarManutencaoParticipanteEntradaDTO.setNrManutencaoContratoNegocio(listarManutencaoContratoSaidaDTO.getCdManutencao());
		consultarManutencaoParticipanteEntradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO.getNrSequenciaContrato());
		
		ConsultarManutencaoParticipanteSaidaDTO consultarManutencaoParticipanteSaidaDTO = solManutContratosServiceImpl.detalharParticipanteContrato(consultarManutencaoParticipanteEntradaDTO);
		
		
		setCpfParticipante(consultarManutencaoParticipanteSaidaDTO.getCpfParticipante());
		setNomeParticipante(consultarManutencaoParticipanteSaidaDTO.getNomeParticipane());
		setDataNascimentoConstituicao(consultarManutencaoParticipanteSaidaDTO.getDataNascimento());
		setGrupoEconomicoParticipante(consultarManutencaoParticipanteSaidaDTO.getDsGrupoEconomicoParticipante());
		setTipoParticipacaoContrato(consultarManutencaoParticipanteSaidaDTO.getDsTipoParticipacaoPessoa());
		
		setSituacao(consultarManutencaoParticipanteSaidaDTO.getDsSituacaoManutencaoContrato());
		setMotivo(consultarManutencaoParticipanteSaidaDTO.getDsMotivoManutencaoContrato());
		setAcao(listarManutencaoContratoSaidaDTO.getDsIndicadorTipoManutencao());
		
		
		setUsuarioInclusao(consultarManutencaoParticipanteSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(consultarManutencaoParticipanteSaidaDTO.getUsuarioManutencao());
		
		setComplementoInclusao(consultarManutencaoParticipanteSaidaDTO.getComplementoInclusao().equals("0")? "" : consultarManutencaoParticipanteSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(consultarManutencaoParticipanteSaidaDTO.getComplementoManutencao().equals("0")? "" : consultarManutencaoParticipanteSaidaDTO.getComplementoManutencao());
		setTipoCanalInclusao(consultarManutencaoParticipanteSaidaDTO.getCdCanalInclusao()==0? "" : consultarManutencaoParticipanteSaidaDTO.getCdCanalInclusao() + " - " +  consultarManutencaoParticipanteSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(consultarManutencaoParticipanteSaidaDTO.getCdCanalManutencao()==0? "" : consultarManutencaoParticipanteSaidaDTO.getCdCanalManutencao() + " - " + consultarManutencaoParticipanteSaidaDTO.getDsCanalManutencao()); 
		
		setDataHoraInclusao(consultarManutencaoParticipanteSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(consultarManutencaoParticipanteSaidaDTO.getDataHoraManutencao()); 
		
		
		
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
			
		ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO =  getListaGridSolicitacao().get(getItemSelecionadoSolicitacao());
		
		try {
			
			// seta label�s com campos do Grid para ser exibido no Excluir
			setDsTipoManutencao(listarManutencaoContratoSaidaDTO.getDsTipoManutencaoContrato());
			setHrManutencaoRegistro(listarManutencaoContratoSaidaDTO.getHrManutencaoRegistro());
			
			if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 1){
				setIndicadorTipoManutencao(1);
				preencheDadosParticipantes();
			}else {
				if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 2){
					setIndicadorTipoManutencao(2);
					preencheDadosConta();
				}else {
					
					if (listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato() == 3){
						setIndicadorTipoManutencao(3);
						preencheDadosServico();
					}else{
						return "";
					}
				}
			}
				
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "conManterSolManutContrato2", BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "EXCLUIR";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		
		try {
			
			CancelarManutencaoEntradaDTO cancelarManutencaoEntradaDTO = new CancelarManutencaoEntradaDTO();
			
			ListarManutencaoContratoSaidaDTO listarManutencaoContratoSaidaDTO = getListaGridSolicitacao().get(getItemSelecionadoSolicitacao());
			
			ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
			
			cancelarManutencaoEntradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
			cancelarManutencaoEntradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());			
			cancelarManutencaoEntradaDTO.setCdTipoManutencaoContrato(listarManutencaoContratoSaidaDTO.getCdTipoManutencaoContrato());			
			cancelarManutencaoEntradaDTO.setNrManutencaoContratoNegocio(listarManutencaoContratoSaidaDTO.getCdManutencao());			
			cancelarManutencaoEntradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO.getNrSequenciaContrato());
			
			
			CancelarManutencaoSaidaDTO cancelarManutencaoSaidaDTO = getSolManutContratosServiceImpl().cancelarManutencao(cancelarManutencaoEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + cancelarManutencaoSaidaDTO.getCodMensagem() + ") " + cancelarManutencaoSaidaDTO.getMensagem(), "conManterSolManutContrato2", BradescoViewExceptionActionType.ACTION, false);
			
			consultarSolicitacoes();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setItemSelecionadoSolicitacao(null);
			setListaGridSolicitacao(null);
			return null;
		}
		return "";
	}
	
	/**
	 * Consultar solicitacoes.
	 *
	 * @return the string
	 */
	public String consultarSolicitacoes(){
		setDesabilitaCamposPesquisa(false);
		try{
			
			ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
			
			ListarManutencaoContratoEntradaDTO entradaDTO = new ListarManutencaoContratoEntradaDTO();
		
			entradaDTO.setCdTipoManutencaoContrato(getTipoManutencaoFiltro() != null ? getTipoManutencaoFiltro():0);
			entradaDTO.setCdAcao(getAcaoFiltro() != null ? getAcaoFiltro() : 0);
			entradaDTO.setCdPessoaJuridicaContrato(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO.getNrSequenciaContrato());
			
			setListaGridSolicitacao(getSolManutContratosServiceImpl().listarManutencaoContrato(entradaDTO));
			
			listaControleSolicitacao = new ArrayList<SelectItem>();
			
			for(int i = 0; i < listaGridSolicitacao.size(); i++){
				listaControleSolicitacao.add(new SelectItem(i,""));
			}
			
			setItemSelecionadoSolicitacao(null);
			setDesabilitaCamposPesquisa(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridSolicitacao(null);
			setItemSelecionadoSolicitacao(null);

		}
		
		return "";
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: desabilitaCamposPesquisa.
	 *
	 * @return desabilitaCamposPesquisa
	 */
	public boolean getDesabilitaCamposPesquisa() {
		return desabilitaCamposPesquisa;
	}

	/**
	 * Set: desabilitaCamposPesquisa.
	 *
	 * @param desabilitaCamposPesquisa the desabilita campos pesquisa
	 */
	public void setDesabilitaCamposPesquisa(boolean desabilitaCamposPesquisa) {
		this.desabilitaCamposPesquisa = desabilitaCamposPesquisa;
	}
	
	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	/**
	 * Get: hrManutencaoRegistro.
	 *
	 * @return hrManutencaoRegistro
	 */
	public String getHrManutencaoRegistro() {
		return hrManutencaoRegistro;
	}

	/**
	 * Set: hrManutencaoRegistro.
	 *
	 * @param hrManutencaoRegistro the hr manutencao registro
	 */
	public void setHrManutencaoRegistro(String hrManutencaoRegistro) {
		this.hrManutencaoRegistro = hrManutencaoRegistro;
	}

	
	
	
}
