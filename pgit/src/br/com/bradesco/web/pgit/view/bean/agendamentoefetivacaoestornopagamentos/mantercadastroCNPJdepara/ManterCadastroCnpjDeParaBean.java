/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.mantercadastroCNPJdepara
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.mantercadastroCNPJdepara;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.geral.MensagemSaida;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.IMantercadastroCPFCNPJbloqueadosService;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.DetalharCnpjFicticioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ExcluirCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.IncluirCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ListarCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ListarCnpjFicticioOcorrencias;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ListarCnpjFicticioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarCnpjFicticioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadastrocpfcnpjbloqueados.bean.ValidarCnpjFicticioSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: ManterCadastroCnpjDeParaBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterCadastroCnpjDeParaBean {
	
	/** Atributo CONSULTA. */
	private static final String CONSULTA = "conManterCadastroCNPJDePara";
	
	/** Atributo manterCadastroCPFCNPJbloqueadosServiceImpl. */
	private IMantercadastroCPFCNPJbloqueadosService manterCadastroCPFCNPJbloqueadosServiceImpl;
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean = new FiltroAgendamentoEfetivacaoEstornoBean();	
	
	/** Atributo entradaListarCnpjFicticio. */
	private ListarCnpjFicticioEntradaDTO entradaListarCnpjFicticio;
	
	/** Atributo saidaListarCnpjFicticio. */
	private ListarCnpjFicticioSaidaDTO saidaListarCnpjFicticio;
	
	/** Atributo saidaDetalharCnpjFicticio. */
	private DetalharCnpjFicticioSaidaDTO saidaDetalharCnpjFicticio;
	
	/** Atributo listaCnpjFicticio. */
	private List<ListarCnpjFicticioOcorrencias> listaCnpjFicticio;
	
	/** Atributo entradaValidarCNPJFicticio. */
	private ValidarCnpjFicticioEntradaDTO entradaValidarCNPJFicticio;  
	
	/** Atributo saidaValidarCNPJFicticio. */
	private ValidarCnpjFicticioSaidaDTO saidaValidarCNPJFicticio;  
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	 
	/** Atributo itemFiltroSelecionado. */
	private Integer itemFiltroSelecionado;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo numeroFiltro. */
	private Long numeroFiltro;
	
	/** Atributo cpfCnpjOriginalLista. */
	private String cpfCnpjOriginalLista;
	
	/** Atributo contratoPGITLista. */
	private Long contratoPGITLista;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo cdCtrlCpfCnpjString. */
	private String cdCtrlCpfCnpjString;
	
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
	    getFiltroAgendamentoEfetivacaoEstornoBean().setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
        getFiltroAgendamentoEfetivacaoEstornoBean().setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
        getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno("manterCadastroCNPJDePara");
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		limparIniciar();
	}

	/**
	 * Listar cnpj ficticio.
	 */
	public void listarCNPJFicticio(){
		setListaCnpjFicticio(new ArrayList<ListarCnpjFicticioOcorrencias>());
		
		try {
			saidaListarCnpjFicticio = getManterCadastroCPFCNPJbloqueadosServiceImpl().listarCnpjFicticio(entradaListarCnpjFicticio);
			setListaCnpjFicticio(saidaListarCnpjFicticio.getOcorrencias());
			
			this.listaControleRadio =  new ArrayList<SelectItem>();
			for (int i = 0; i <= listaCnpjFicticio.size();i++) {
				this.listaControleRadio.add(new SelectItem(i," "));
			}
			
			setItemSelecionadoLista(null);
			setDisableArgumentosConsulta(true);
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
		}
	}
	
	/**
	 * Confirmar exclusao.
	 */
	public void confirmarExclusao(){
		ExcluirCnpjFicticioEntradaDTO  entradaExcluir = new ExcluirCnpjFicticioEntradaDTO();
		ListarCnpjFicticioOcorrencias registroSelecionado = getListaCnpjFicticio().get(getItemSelecionadoLista());
		
		entradaExcluir.setCdCnpjCpf(PgitUtil.verificaLongNulo(registroSelecionado.getCdCpfCnpjSaida()));
		entradaExcluir.setCdCtrlCpfCnpj(PgitUtil.verificaIntegerNulo(registroSelecionado.getCdCtrlCpfCnpjSaida()));
		entradaExcluir.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(registroSelecionado.getCdFilialCpfCnpjSaida()));
		entradaExcluir.setCdContrato(PgitUtil.verificaLongNulo(registroSelecionado.getCdContratoSaida()));
		
		try {
			MensagemSaida saida = getManterCadastroCPFCNPJbloqueadosServiceImpl().excluirCnpjFicticio(entradaExcluir);
			PgitFacesUtils.addInfoModalMessage(saida.getCodMensagem(), saida.getMensagem(),"#{manterCadastroCnpjDeParaBean.consultarAposAcao}");
		} catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
		}
	}
	
	/**
	 * Confirmar inclusao.
	 */
	public void confirmarInclusao(){
		IncluirCnpjFicticioEntradaDTO entradaIncluir = new IncluirCnpjFicticioEntradaDTO();

		entradaIncluir.setCdCnpjCpf(PgitUtil.verificaLongNulo(saidaValidarCNPJFicticio.getCdCnpjCpf()));
		entradaIncluir.setCdCtrlCpfCnpj(PgitUtil.verificaIntegerNulo(saidaValidarCNPJFicticio.getCdCtrlCpfCnpj()));
		entradaIncluir.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(saidaValidarCNPJFicticio.getCdFilialCpfCnpj()));
		
		try {
			MensagemSaida saida = getManterCadastroCPFCNPJbloqueadosServiceImpl().IncluirCnpjFicticio(entradaIncluir);
			PgitFacesUtils.addInfoModalMessage(saida.getCodMensagem(), saida.getMensagem(),"#{manterCadastroCnpjDeParaBean.consultarAposAcao}");
		} catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			limparDadosValidacao();
		}
	}

	/**
	 * Avancar validar inclusao.
	 *
	 * @return the string
	 */
	public String avancarValidarInclusao(){
		try {
			entradaValidarCNPJFicticio.setCdCtrlCpfCnpj(Integer.parseInt(getCdCtrlCpfCnpjString()));
			saidaValidarCNPJFicticio = getManterCadastroCPFCNPJbloqueadosServiceImpl().validarCnpjFicticio(entradaValidarCNPJFicticio);
		} catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
		
		return "AVANCAR_CONFIRMAR_INCLUIR";
	}
	
	
	/**
	 * Consultar apos acao.
	 *
	 * @return the string
	 */
	public String consultarAposAcao() {
		limparTudo();
		return CONSULTA;
	}
	
	/**
	 * Limpar dados validacao.
	 */
	public void limparDadosValidacao(){
		setCdCtrlCpfCnpjString(null);
		setEntradaValidarCNPJFicticio(new ValidarCnpjFicticioEntradaDTO());
	}
	
	/**
	 * Detalhar cnpj ficticio.
	 *
	 * @return the string
	 */
	public String detalharCNPJFicticio(){
		DetalharCnpjFicticioEntradaDTO entradaDetalhar = new DetalharCnpjFicticioEntradaDTO();
		ListarCnpjFicticioOcorrencias registroSelecionado = getListaCnpjFicticio().get(getItemSelecionadoLista());
		
		entradaDetalhar.setCdCnpjCpf(PgitUtil.verificaLongNulo(registroSelecionado.getCdCpfCnpjSaida()));
		entradaDetalhar.setCdCtrlCpfCnpj(PgitUtil.verificaIntegerNulo(registroSelecionado.getCdCtrlCpfCnpjSaida()));
		entradaDetalhar.setCdFilialCnpjCpf(PgitUtil.verificaIntegerNulo(registroSelecionado.getCdFilialCpfCnpjSaida()));
		entradaDetalhar.setCdContrato(PgitUtil.verificaLongNulo(registroSelecionado.getCdContratoSaida()));
		
		try {
			saidaDetalharCnpjFicticio = getManterCadastroCPFCNPJbloqueadosServiceImpl().detalharCnpjFicticio(entradaDetalhar);
			
			setCpfCnpjOriginalLista(PgitUtil.verificaStringNula(registroSelecionado.getCpfCnpjOriginalFormatado()));
			setContratoPGITLista(PgitUtil.verificaLongNulo(registroSelecionado.getCdNumeroContrato()) == 0l ? null : PgitUtil.verificaLongNulo(registroSelecionado.getCdNumeroContrato()));
			setDataHoraManutencao(PgitUtil.concatenarCampos(saidaDetalharCnpjFicticio.getDtManutencaoRegistro(),saidaDetalharCnpjFicticio.getHrManutencaoRegistro()," - "));
			setDataHoraInclusao(PgitUtil.concatenarCampos(saidaDetalharCnpjFicticio.getDtInclusaoRegistro(),saidaDetalharCnpjFicticio.getHrInclusaoRegistro()," - "));
			
		} catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			setItemSelecionadoLista(null);
			return"";
		}
		
		return "DETALHAR";
	}

//	private String obterDataDetalhe(String valorData) {
//		Date data = FormatarData.formataTimestampFromPdc(valorData);
//		return FormatarData.formataDiaMesAno(data);
//	}
	
	/**
 * Voltar.
 *
 * @return the string
 */
public String voltar(){
		return "VOLTAR";
	}
	
	/**
	 * Avancar excluir.
	 *
	 * @return the string
	 */
	public String avancarExcluir(){
		detalharCNPJFicticio();
		return"EXCLUIR";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		entradaValidarCNPJFicticio = new ValidarCnpjFicticioEntradaDTO();
		return "INCLUIR";
	}
	
	/**
	 * Paginar cnpj ficticio.
	 */
	public void paginarCNPJFicticio(){
		listarCNPJFicticio();
	}
	
	/**
	 * Limpar iniciar.
	 */
	public void limparIniciar(){
		setDisableArgumentosConsulta(false);
		setEntradaListarCnpjFicticio(new ListarCnpjFicticioEntradaDTO());
		getEntradaListarCnpjFicticio().setCdpessoaJuridicaContrato(2269651l);
		getEntradaListarCnpjFicticio().setCdTipoContratoNegocio(null);
	}
	
	/**
	 * Limpar lista.
	 */
	public void limparLista(){
		setListaCnpjFicticio(new ArrayList<ListarCnpjFicticioOcorrencias>());
		setItemFiltroSelecionado(null);
		setDisableArgumentosConsulta(false);
	}

	/**
	 * Limpar tudo.
	 */
	public void limparTudo(){
		setDisableArgumentosConsulta(false);
		setEntradaListarCnpjFicticio(new ListarCnpjFicticioEntradaDTO());
		getEntradaListarCnpjFicticio().setCdpessoaJuridicaContrato(2269651l);
		getEntradaListarCnpjFicticio().setCdTipoContratoNegocio(null);
		setItemFiltroSelecionado(null);
		setListaCnpjFicticio(new ArrayList<ListarCnpjFicticioOcorrencias>());
	}
	
	/**
	 * Is desabilita botao consultar.
	 *
	 * @return true, if is desabilita botao consultar
	 */
	public boolean isDesabilitaBotaoConsultar(){
		if(getItemFiltroSelecionado() != null ){
			if(Integer.valueOf(0).equals(getItemFiltroSelecionado())){
//				 if(getEntradaListarCnpjFicticio().getCdContrato() != null && getEntradaListarCnpjFicticio().getCdContrato() != 0l){
					    return false;
//				 }
			}else if(Integer.valueOf(1).equals(getItemFiltroSelecionado())){
//					if(getEntradaListarCnpjFicticio().getCdCnpjCpf() != null && !"".equals(getEntradaListarCnpjFicticio().getCdCnpjCpf()) &&
//					   getEntradaListarCnpjFicticio().getCdCtrlCpfCnpj() != null  &&  !"".equals(getEntradaListarCnpjFicticio().getCdCtrlCpfCnpj())){
						return false;
//					}
				} else if(Integer.valueOf(2).equals(getItemFiltroSelecionado())){
//					if(getEntradaListarCnpjFicticio().getNrSequenciaContratoNegocio() != null && getEntradaListarCnpjFicticio().getNrSequenciaContratoNegocio() != 0l){
						return false;
//					}
			    }
		}	
		return true;
	}
	
	/**
	 * Is desabilita botao avancar.
	 *
	 * @return true, if is desabilita botao avancar
	 */
	public boolean isDesabilitaBotaoAvancar(){
			if(getEntradaValidarCNPJFicticio().getCdCnpjCpf() != null && !"".equals(getEntradaValidarCNPJFicticio().getCdCnpjCpf()) &&
					getCdCtrlCpfCnpjString() != null  &&  !"".equals(getCdCtrlCpfCnpjString())){
						if(getCdCtrlCpfCnpjString().length() > 1){
							return false;
					    }else{
					    	return true;
					    }
			}
		return true;
	}
	
	/**
	 * Limpar apos alterar opcao cnpj.
	 */
	public void limparAposAlterarOpcaoCNPJ(){
		setDisableArgumentosConsulta(false);
		setEntradaListarCnpjFicticio(new ListarCnpjFicticioEntradaDTO());
		getEntradaListarCnpjFicticio().setCdpessoaJuridicaContrato(2269651l);
	}
	
	/**
	 * Get: radiosFiltro.
	 *
	 * @return radiosFiltro
	 */
	public List<SelectItem> getRadiosFiltro() {
		List<SelectItem> radios = new ArrayList<SelectItem>();
		radios.add(new SelectItem(0, ""));
		radios.add(new SelectItem(1, ""));
		radios.add(new SelectItem(2, ""));
		return radios;
	}
	
	/**
	 * Get: manterCadastroCPFCNPJbloqueadosServiceImpl.
	 *
	 * @return manterCadastroCPFCNPJbloqueadosServiceImpl
	 */
	public IMantercadastroCPFCNPJbloqueadosService getManterCadastroCPFCNPJbloqueadosServiceImpl() {
		return manterCadastroCPFCNPJbloqueadosServiceImpl;
	}
	
	/**
	 * Set: manterCadastroCPFCNPJbloqueadosServiceImpl.
	 *
	 * @param manterCadastroCPFCNPJbloqueadosServiceImpl the manter cadastro cpfcnp jbloqueados service impl
	 */
	public void setManterCadastroCPFCNPJbloqueadosServiceImpl(
			IMantercadastroCPFCNPJbloqueadosService manterCadastroCPFCNPJbloqueadosServiceImpl) {
		this.manterCadastroCPFCNPJbloqueadosServiceImpl = manterCadastroCPFCNPJbloqueadosServiceImpl;
	}

	/**
	 * Get: itemFiltroSelecionado.
	 *
	 * @return itemFiltroSelecionado
	 */
	public Integer getItemFiltroSelecionado() {
		return itemFiltroSelecionado;
	}

	/**
	 * Set: itemFiltroSelecionado.
	 *
	 * @param itemFiltroSelecionado the item filtro selecionado
	 */
	public void setItemFiltroSelecionado(Integer itemFiltroSelecionado) {
		this.itemFiltroSelecionado = itemFiltroSelecionado;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: entradaListarCnpjFicticio.
	 *
	 * @return entradaListarCnpjFicticio
	 */
	public ListarCnpjFicticioEntradaDTO getEntradaListarCnpjFicticio() {
		return entradaListarCnpjFicticio;
	}

	/**
	 * Set: entradaListarCnpjFicticio.
	 *
	 * @param entradaListarCnpjFicticio the entrada listar cnpj ficticio
	 */
	public void setEntradaListarCnpjFicticio(
			ListarCnpjFicticioEntradaDTO entradaListarCnpjFicticio) {
		this.entradaListarCnpjFicticio = entradaListarCnpjFicticio;
	}

	/**
	 * Get: numeroFiltro.
	 *
	 * @return numeroFiltro
	 */
	public Long getNumeroFiltro() {
		return numeroFiltro;
	}

	/**
	 * Set: numeroFiltro.
	 *
	 * @param numeroFiltro the numero filtro
	 */
	public void setNumeroFiltro(Long numeroFiltro) {
		this.numeroFiltro = numeroFiltro;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Get: saidaListarCnpjFicticio.
	 *
	 * @return saidaListarCnpjFicticio
	 */
	public ListarCnpjFicticioSaidaDTO getSaidaListarCnpjFicticio() {
		return saidaListarCnpjFicticio;
	}

	/**
	 * Set: saidaListarCnpjFicticio.
	 *
	 * @param saidaListarCnpjFicticio the saida listar cnpj ficticio
	 */
	public void setSaidaListarCnpjFicticio(
			ListarCnpjFicticioSaidaDTO saidaListarCnpjFicticio) {
		this.saidaListarCnpjFicticio = saidaListarCnpjFicticio;
	}

	/**
	 * Get: listaCnpjFicticio.
	 *
	 * @return listaCnpjFicticio
	 */
	public List<ListarCnpjFicticioOcorrencias> getListaCnpjFicticio() {
		return listaCnpjFicticio;
	}

	/**
	 * Set: listaCnpjFicticio.
	 *
	 * @param listaCnpjFicticio the lista cnpj ficticio
	 */
	public void setListaCnpjFicticio(
			List<ListarCnpjFicticioOcorrencias> listaCnpjFicticio) {
		this.listaCnpjFicticio = listaCnpjFicticio;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: saidaDetalharCnpjFicticio.
	 *
	 * @return saidaDetalharCnpjFicticio
	 */
	public DetalharCnpjFicticioSaidaDTO getSaidaDetalharCnpjFicticio() {
		return saidaDetalharCnpjFicticio;
	}

	/**
	 * Set: saidaDetalharCnpjFicticio.
	 *
	 * @param saidaDetalharCnpjFicticio the saida detalhar cnpj ficticio
	 */
	public void setSaidaDetalharCnpjFicticio(
			DetalharCnpjFicticioSaidaDTO saidaDetalharCnpjFicticio) {
		this.saidaDetalharCnpjFicticio = saidaDetalharCnpjFicticio;
	}

	/**
	 * Get: cpfCnpjOriginalLista.
	 *
	 * @return cpfCnpjOriginalLista
	 */
	public String getCpfCnpjOriginalLista() {
		return cpfCnpjOriginalLista;
	}

	/**
	 * Set: cpfCnpjOriginalLista.
	 *
	 * @param cpfCnpjOriginalLista the cpf cnpj original lista
	 */
	public void setCpfCnpjOriginalLista(String cpfCnpjOriginalLista) {
		this.cpfCnpjOriginalLista = cpfCnpjOriginalLista;
	}

	/**
	 * Get: contratoPGITLista.
	 *
	 * @return contratoPGITLista
	 */
	public Long getContratoPGITLista() {
		return contratoPGITLista;
	}

	/**
	 * Set: contratoPGITLista.
	 *
	 * @param contratoPGITLista the contrato pgit lista
	 */
	public void setContratoPGITLista(Long contratoPGITLista) {
		this.contratoPGITLista = contratoPGITLista;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: entradaValidarCNPJFicticio.
	 *
	 * @return entradaValidarCNPJFicticio
	 */
	public ValidarCnpjFicticioEntradaDTO getEntradaValidarCNPJFicticio() {
		return entradaValidarCNPJFicticio;
	}

	/**
	 * Set: entradaValidarCNPJFicticio.
	 *
	 * @param entradaValidarCNPJFicticio the entrada validar cnpj ficticio
	 */
	public void setEntradaValidarCNPJFicticio(
			ValidarCnpjFicticioEntradaDTO entradaValidarCNPJFicticio) {
		this.entradaValidarCNPJFicticio = entradaValidarCNPJFicticio;
	}

	/**
	 * Get: saidaValidarCNPJFicticio.
	 *
	 * @return saidaValidarCNPJFicticio
	 */
	public ValidarCnpjFicticioSaidaDTO getSaidaValidarCNPJFicticio() {
		return saidaValidarCNPJFicticio;
	}

	/**
	 * Set: saidaValidarCNPJFicticio.
	 *
	 * @param saidaValidarCNPJFicticio the saida validar cnpj ficticio
	 */
	public void setSaidaValidarCNPJFicticio(
			ValidarCnpjFicticioSaidaDTO saidaValidarCNPJFicticio) {
		this.saidaValidarCNPJFicticio = saidaValidarCNPJFicticio;
	}

	/**
	 * Get: cdCtrlCpfCnpjString.
	 *
	 * @return cdCtrlCpfCnpjString
	 */
	public String getCdCtrlCpfCnpjString() {
		return cdCtrlCpfCnpjString;
	}

	/**
	 * Set: cdCtrlCpfCnpjString.
	 *
	 * @param cdCtrlCpfCnpjString the cd ctrl cpf cnpj string
	 */
	public void setCdCtrlCpfCnpjString(String cdCtrlCpfCnpjString) {
		this.cdCtrlCpfCnpjString = cdCtrlCpfCnpjString;
	}

}
