/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaofavorecido.relatoriofavorecido
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaofavorecido.relatoriofavorecido;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaSegmentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarClasseRamoAtvddSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarDependenciaDiretoriaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarDependenciaEmpresaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSramoAtvddEconcEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSramoAtvddEconcSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescGrupoEconomicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescGrupoEconomicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.IRelatorioFavorecidoService;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.ConRelFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.ConRelFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.DetRelFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.DetRelFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.ExcRelFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.ExcRelFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.IncRelFavorecidoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.relatoriofavorecido.bean.IncRelFavorecidoSaidaDTO;
import br.com.bradesco.web.pgit.utils.MessageUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * @author gguimaraes
 * 
 */
public class RelatorioFavorecidoBean {

	// Constante Utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo SOLICITACAO_RELATORIOS_FAVORECIDOS. */
	private static final String SOLICITACAO_RELATORIOS_FAVORECIDOS = "conSolicitacaoRelatoriosFavorecidos";

	/** Atributo entradaConRelFavorecido. */
	private ConRelFavorecidoEntradaDTO entradaConRelFavorecido;
	
	/** Atributo manterCadContaDestinoService. */
	private IManterCadContaDestinoService manterCadContaDestinoService;

	/** Atributo listaConRelFavorecido. */
	private List<ConRelFavorecidoSaidaDTO> listaConRelFavorecido;

	/** Atributo relatorioFavorecidoService. */
	private IRelatorioFavorecidoService relatorioFavorecidoService;

	/** Atributo entradaDetRelFavorecido. */
	private DetRelFavorecidoEntradaDTO entradaDetRelFavorecido;

	/** Atributo saidaDetRelFavorecido. */
	private DetRelFavorecidoSaidaDTO saidaDetRelFavorecido;

	/** Atributo entradaIncRelFavorecido. */
	private IncRelFavorecidoEntradaDTO entradaIncRelFavorecido;

	/** Atributo saidaIncRelFavorecido. */
	private IncRelFavorecidoSaidaDTO saidaIncRelFavorecido;

	/** Atributo entradaConsultarDescricao. */
	private ConsultarDescricaoUnidOrganizacionalSaidaDTO entradaConsultarDescricao;

	/** Atributo saidaTipoSolicitacao. */
	private ConRelFavorecidoSaidaDTO saidaTipoSolicitacao;

	/** Atributo entradaTipoSolicitacao. */
	private ConRelFavorecidoEntradaDTO entradaTipoSolicitacao;

	/** Atributo entradaExcRelFavorecido. */
	private ExcRelFavorecidoSaidaDTO entradaExcRelFavorecido;

	/** Atributo saidaExcRelFavorecido. */
	private ExcRelFavorecidoEntradaDTO saidaExcRelFavorecido;

	/** Atributo saidaConfirmaExclusao. */
	private ExcRelFavorecidoSaidaDTO saidaConfirmaExclusao;

	/** Atributo saidaEmpresaGestora. */
	private EmpresaGestoraSaidaDTO saidaEmpresaGestora;

	/** Atributo listaEmpresaGestora. */
	private List<EmpresaGestoraSaidaDTO> listaEmpresaGestora;

	/** Atributo preencherListaEmpresaGestora. */
	private List<SelectItem> preencherListaEmpresaGestora;

	/** Atributo itemSelecionadoListaRelatorio. */
	private String itemSelecionadoListaRelatorio;

	/** Atributo itemServicoSelecionado. */
	private String itemServicoSelecionado;

	/** Atributo itemCheckDirReg. */
	private boolean itemCheckDirReg;

	/** Atributo itemCheckGerReg. */
	private boolean itemCheckGerReg;

	/** Atributo itemCheckAgeOpe. */
	private boolean itemCheckAgeOpe;

	/** Atributo itemCheckSeg. */
	private boolean itemCheckSeg;

	/** Atributo itemCheckParGrpEcp. */
	private boolean itemCheckParGrpEcp;

	/** Atributo itemCheckAtiEco. */
	private boolean itemCheckAtiEco;

	/** Atributo codigoIdentificacaoEmpresa. */
	private Integer codigoIdentificacaoEmpresa;

	/** Atributo filtroEmpresaConglomerado. */
	private String filtroEmpresaConglomerado;

	/** Atributo empresaConglomerado. */
	private String empresaConglomerado;

	/** Atributo empresaConglomeradoDesc. */
	private String empresaConglomeradoDesc;

	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo consultasService. */
	private IConsultasService consultasService;

	/** Atributo itemFiltroSelecionado. */
	private String itemFiltroSelecionado;

	/** Atributo empresaConglomeradoIncluir. */
	private Long empresaConglomeradoIncluir;

	/** Atributo listaDiretoria. */
	private List<SelectItem> listaDiretoria = new ArrayList<SelectItem>();

	/** Atributo listaGerencia. */
	private List<SelectItem> listaGerencia = new ArrayList<SelectItem>();

	/** Atributo itemSelecionadoLista. */
	private String itemSelecionadoLista;

	/** Atributo cboClasseRamo. */
	private String cboClasseRamo;

	/** Atributo listaCboClasseRamo. */
	private List<SelectItem> listaCboClasseRamo = new ArrayList<SelectItem>();

	/** Atributo listaCboClasseRamoHash. */
	private Map<String, ListarClasseRamoAtvddSaidaDTO> listaCboClasseRamoHash = new HashMap<String, ListarClasseRamoAtvddSaidaDTO>();

	/** Atributo cboSubRamoAtividade. */
	private String cboSubRamoAtividade;

	/** Atributo listaCboSubRamoAtividade. */
	private List<SelectItem> listaCboSubRamoAtividade = new ArrayList<SelectItem>();

	/** Atributo listaCboSubRamoAtividadeHash. */
	private Map<String, ListarSramoAtvddEconcSaidaDTO> listaCboSubRamoAtividadeHash = new HashMap<String, ListarSramoAtvddEconcSaidaDTO>();

	/** Atributo dsClasseRamo. */
	private String dsClasseRamo;

	/** Atributo dsSubRamoAtividade. */
	private String dsSubRamoAtividade;

	/** Atributo valida. */
	private boolean valida;
	
	/** Atributo agenciaFiltro. */
	private Integer agenciaFiltro;
	
	/** Atributo saidaGrupoEconomico. */
	private ConsultarDescGrupoEconomicoSaidaDTO saidaGrupoEconomico = new ConsultarDescGrupoEconomicoSaidaDTO();

	/**
	 * Carrega combo classe ramo.
	 */
	public void carregaComboClasseRamo() {
		try {
			listaCboClasseRamo = new ArrayList<SelectItem>();
			setCboClasseRamo(null);

			List<ListarClasseRamoAtvddSaidaDTO> lista = getComboService()
					.listarClasseRamoAtvdd();
			listaCboClasseRamoHash.clear();
			for (ListarClasseRamoAtvddSaidaDTO combo : lista) {
				listaCboClasseRamo.add(new SelectItem(combo
						.getCdRamoAtividade()
						+ "/" + combo.getCdClassAtividade(), combo
						.getClasseRamoFormatado()));
				listaCboClasseRamoHash.put(combo.getCdRamoAtividade() + "/"
						+ combo.getCdClassAtividade(), combo);
			}
		} catch (PdcAdapterFunctionalException p) {
			listaCboClasseRamo = new ArrayList<SelectItem>();
			setCboClasseRamo(null);
		}
	}

	/**
	 * Carrega combo sub ramo atividade.
	 */
	public void carregaComboSubRamoAtividade() {
		try {
			listaCboSubRamoAtividade = new ArrayList<SelectItem>();
			setCboSubRamoAtividade(null);

			if (getCboClasseRamo() != null && !getCboClasseRamo().equals("")) {
				ListarSramoAtvddEconcEntradaDTO entrada = new ListarSramoAtvddEconcEntradaDTO();
				entrada.setCdClassAtividade(getListaCboClasseRamoHash().get(
						getCboClasseRamo()).getCdClassAtividade());
				entrada.setCdRamoAtividade(getListaCboClasseRamoHash().get(
						getCboClasseRamo()).getCdRamoAtividade());

				List<ListarSramoAtvddEconcSaidaDTO> lista = getComboService()
						.listarSramoAtvddEconc(entrada);

				for (ListarSramoAtvddEconcSaidaDTO combo : lista) {
					listaCboSubRamoAtividade.add(new SelectItem(combo
							.getCdAtividadeEconomica() + "/" + combo.getCdSubRamoAtividade(), combo
							.getSubRamoAtividadeFormatado()));
					listaCboSubRamoAtividadeHash.put(combo
							.getCdAtividadeEconomica() + "/" + combo.getCdSubRamoAtividade(), combo);
				}
			}
		} catch (PdcAdapterFunctionalException p) {
			listaCboSubRamoAtividade = new ArrayList<SelectItem>();
			setCboSubRamoAtividade(null);
		}
	}

	/**
	 * Is valida.
	 *
	 * @return true, if is valida
	 */
	public boolean isValida() {
		return valida;
	}

	/**
	 * Set: valida.
	 *
	 * @param valida the valida
	 */
	public void setValida(boolean valida) {
		this.valida = valida;
	}

	/**
	 * Relatorio favorecido bean.
	 */
	public RelatorioFavorecidoBean() {
		super();
	}

	/**
	 * Inicializar campos relatorios solic.
	 *
	 * @return the string
	 */
	public String inicializarCamposRelatoriosSolic() {
		this.setEntradaConRelFavorecido(new ConRelFavorecidoEntradaDTO());
		this.setEntradaDetRelFavorecido(new DetRelFavorecidoEntradaDTO());
		this.setEntradaIncRelFavorecido(new IncRelFavorecidoEntradaDTO());
		this.setItemSelecionadoListaRelatorio("");
		
		return SOLICITACAO_RELATORIOS_FAVORECIDOS;
	}

	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos() {
		this.entradaConRelFavorecido.setTpSolicitacao(null);
		this.entradaConRelFavorecido.setDtInicioSolicitacao(new Date());
		this.entradaConRelFavorecido.setDtFimSolicitacao(new Date());
		this
				.setListaConRelFavorecido(new ArrayList<ConRelFavorecidoSaidaDTO>());
		setItemSelecionadoListaRelatorio("");
		return SOLICITACAO_RELATORIOS_FAVORECIDOS;
	}

	/**
	 * Consultar relatorios.
	 *
	 * @return the string
	 */
	public String consultarRelatorios() {

		this.buscarRelatorios();
		return SOLICITACAO_RELATORIOS_FAVORECIDOS;
	}

	/**
	 * Submit.
	 *
	 * @param event the event
	 */
	public void submit(ActionEvent event) {
		this.buscarRelatorios();
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoListaRelatorio(null);
		return "conSolicitacaoRelatoriosFavorecidos";
	}

	/**
	 * Buscar relatorios.
	 */
	private void buscarRelatorios() {
		this.setListaConRelFavorecido(this.relatorioFavorecidoService
				.pesquisaRelFavorecido(this.entradaConRelFavorecido));
	}

	/**
	 * Voltar solicitacao relatorio favorecido.
	 *
	 * @return the string
	 */
	public String voltarSolicitacaoRelatorioFavorecido() {

		setItemSelecionadoListaRelatorio(null);
		return SOLICITACAO_RELATORIOS_FAVORECIDOS;
	}

	/**
	 * Load empresa gestora.
	 */
	private void loadEmpresaGestora() {
		try {
			List<EmpresaGestoraSaidaDTO> list = comboService
					.listarEmpresasGestoras();

			preencherListaEmpresaGestora = new ArrayList<SelectItem>();

			for (EmpresaGestoraSaidaDTO saida : list) {
				preencherListaEmpresaGestora.add(new SelectItem(saida
						.getCdPessoaJuridicaContrato(), saida.getDsCnpj()));
			}
		} catch (PdcAdapterFunctionalException e) {
			preencherListaEmpresaGestora = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Get: preencherEmpresaGestora.
	 *
	 * @return preencherEmpresaGestora
	 */
	public List<SelectItem> getPreencherEmpresaGestora() {
		return preencherListaEmpresaGestora;
	}

	/**
	 * Get: preencheComboSegmento.
	 *
	 * @return preencheComboSegmento
	 */
	public List<SelectItem> getPreencheComboSegmento() {

		List<EmpresaSegmentoSaidaDTO> list = comboService.pesquisaSegmento();

		List<SelectItem> listaEmpresaSegmento = new ArrayList<SelectItem>();

		for (EmpresaSegmentoSaidaDTO combo : list) {
			listaEmpresaSegmento.add(new SelectItem(combo.getCdSegmento(),
					combo.getDsSegmento()));
		}

		return listaEmpresaSegmento;
	}
     
	
	/**
	 * Define cod pessoa jurid diretoria.
	 */
	public void defineCodPessoaJuridDiretoria(){
		if(entradaIncRelFavorecido.getTpEmpresaConglomerada() != null && entradaIncRelFavorecido.getTpEmpresaConglomerada() == 0){
			setItemCheckDirReg(false);
			setItemCheckGerReg(false);
			setItemCheckAgeOpe(false);
			setAgenciaFiltro(null);
			entradaIncRelFavorecido.setTpDiretoriaRegional(0);
			entradaIncRelFavorecido.setTpGerenciaRegional(0);
		}else{
			entradaIncRelFavorecido.setTpEmpresaConglomerada(2269651l);
		}
	}
	
	/**
	 * Preenche combo diretoria.
	 */
	public void preencheComboDiretoria() {
		defineCodPessoaJuridDiretoria();
		
		listaDiretoria = new ArrayList<SelectItem>();
		listaGerencia = new ArrayList<SelectItem>();
		
		
		List<ListarDependenciaEmpresaSaidaDTO> list = comboService.pesquisaDiretoria(entradaIncRelFavorecido.getTpEmpresaConglomerada());

		for (ListarDependenciaEmpresaSaidaDTO combo : list) {
			listaDiretoria.add(new SelectItem(combo
					.getCdUnidadeOrganizacional().toString(), PgitUtil.concatenarCampos(combo.getCdUnidadeOrganizacional(), combo.getDsUnidadeOrganizacional())));
		}

	}

	/**
	 * Get: litarComboDiretoria.
	 *
	 * @return litarComboDiretoria
	 */
	public List<SelectItem> getLitarComboDiretoria() {
		return listaDiretoria;
	}

	/**
	 * Preenche combo gerencia.
	 */
	public void preencheComboGerencia() {
		if(entradaIncRelFavorecido.getTpDiretoriaRegional()  != null && entradaIncRelFavorecido.getTpDiretoriaRegional() == 0){
			setItemCheckGerReg(false);
			setItemCheckAgeOpe(false);
			setAgenciaFiltro(null);
			entradaIncRelFavorecido.setTpGerenciaRegional(0);
		}
		
		listaGerencia = new ArrayList<SelectItem>();
		List<ListarDependenciaDiretoriaSaidaDTO> list = comboService
				.pesquisaGerencia(entradaIncRelFavorecido
						.getTpEmpresaConglomerada(), new Long(
						entradaIncRelFavorecido.getTpDiretoriaRegional()), 0);

		for (ListarDependenciaDiretoriaSaidaDTO combo : list) {
			listaGerencia.add(new SelectItem(combo.getCdDependencia(), PgitUtil.concatenarCampos(combo.getCdDependencia(), combo.getDsDependencia())));

		}

	}
	
	/**
	 * Limpar agencia combo.
	 */
	public void limparAgenciaCombo(){
		if(entradaIncRelFavorecido.getTpGerenciaRegional() == 0){
			setItemCheckAgeOpe(false);
			setAgenciaFiltro(null);
		}
	}

	/**
	 * Get: litarComboGerencia.
	 *
	 * @return litarComboGerencia
	 */
	public List<SelectItem> getLitarComboGerencia() {
		return listaGerencia;
	}

	/**
	 * Detalhar relatorio.
	 *
	 * @return the string
	 */
	public String detalharRelatorio() {

		ConRelFavorecidoSaidaDTO saida = listaConRelFavorecido.get(Integer
				.parseInt(itemSelecionadoListaRelatorio));

		entradaDetRelFavorecido.setNrSolicitacao(saida.getNrSolicitacao());
		entradaDetRelFavorecido.setTpSolicitacao(saida
				.getCdSolicitacaoPagamento());

		try {
			saidaDetRelFavorecido = relatorioFavorecidoService
					.pesquisarDetalheRelFavorecido(entradaDetRelFavorecido);
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage(e.getMessage(), false);
			return null;
		}

		return "detSolicitacaoRelatoriosFavorecidos";
	}

	/**
	 * Incluir solitacao relatorio.
	 *
	 * @return the string
	 */
	public String incluirSolitacaoRelatorio() {
	    	setAgenciaFiltro(null);
		this.entradaIncRelFavorecido.setCdAgenciaOperadora(null);
		this.entradaIncRelFavorecido.setDsAgencia(null);
		this.entradaIncRelFavorecido.setCdParticipanteGrupoEconomico(null);
		this.entradaIncRelFavorecido.setCdClasse(null);
		this.entradaIncRelFavorecido.setCdRamo(null);
		this.entradaIncRelFavorecido.setCdSubramo(null);
		this.entradaIncRelFavorecido.setCdAtividadeEconomica(null);
		this.entradaIncRelFavorecido.setTpGerenciaRegional(null);
		this.entradaIncRelFavorecido.setTpDiretoriaRegional(null);
		this.entradaIncRelFavorecido.setCdSegmento(null);
		this.entradaIncRelFavorecido.setDsAgencia(null);
		this.entradaIncRelFavorecido.setTpEmpresaConglomerada(null);
		this.setItemServicoSelecionado("");
		this.setItemCheckDirReg(false);
		this.setItemCheckGerReg(false);
		this.setItemCheckAgeOpe(false);
		this.setItemCheckSeg(false);
		this.setItemCheckParGrpEcp(false);
		this.setItemCheckAtiEco(false);
		this.entradaIncRelFavorecido.setTpRelatorio(1);
		this.entradaIncRelFavorecido.setCdSegmento(null);
		loadEmpresaGestora();
		listaDiretoria = new ArrayList<SelectItem>();
		listaGerencia = new ArrayList<SelectItem>();
		
		preencheComboDiretoria();

		carregaComboClasseRamo();
		setListaCboSubRamoAtividade(new ArrayList<SelectItem>());
		saidaGrupoEconomico.setCdGrupoEconomico(null);
		saidaGrupoEconomico.setDsGrupoEconomico(null);
		getEntradaIncRelFavorecido().setTpEmpresaConglomerada(2269651L);
		return "incSolicitacaoRelatoriosFavorecidos";
	}
	
	/**
	 * Busca ag operadora.
	 */
	public void buscaAgOperadora(){
	    if(getAgenciaFiltro() != null){
        	    try{
        		entradaIncRelFavorecido.setDsAgencia("");
        		ConsultarBancoAgenciaEntradaDTO entrada = new ConsultarBancoAgenciaEntradaDTO();
        		
        		entrada.setCdAgencia(getAgenciaFiltro());
        		entrada.setCdBanco(237);
        		entrada.setCdDigitoAgencia(0);
        		entrada.setCdPessoaJuridicaContrato(entradaIncRelFavorecido.getTpEmpresaConglomerada());
        		
        		ConsultarBancoAgenciaSaidaDTO saida = getManterCadContaDestinoService().consultarDescricaoBancoAgencia(entrada);
        		
        		entradaIncRelFavorecido.setCdAgenciaOperadora(getAgenciaFiltro());
        		entradaIncRelFavorecido.setDsAgencia(saida.getDsAgencia());
        		
        	    }catch(PdcAdapterFunctionalException p){        		
        		entradaIncRelFavorecido.setDsAgencia("");
        		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);   
        	    }
	    }
	}
	
	/**
	 * Busca desc grupo economico.
	 */
	public void buscaDescGrupoEconomico(){
		saidaGrupoEconomico.setDsGrupoEconomico(null);
	    if(entradaIncRelFavorecido.getCdParticipanteGrupoEconomico() != null && !entradaIncRelFavorecido.getCdParticipanteGrupoEconomico().equals("")){
        	    try{
        		
        		ConsultarDescGrupoEconomicoEntradaDTO entrada = new ConsultarDescGrupoEconomicoEntradaDTO();
        		
        		entrada.setCdGrupoEconomico(entradaIncRelFavorecido.getCdParticipanteGrupoEconomico());
        		        		
        		saidaGrupoEconomico = getConsultasService().consultarDescGrupoEconomico(entrada);
        		        		
        	    }catch(PdcAdapterFunctionalException p){
	        		saidaGrupoEconomico = new ConsultarDescGrupoEconomicoSaidaDTO();
	        		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);   
        	    }
	    }
	}

	/**
	 * Avancar solicitacao relatorio.
	 *
	 * @return the string
	 */
	public String avancarSolicitacaoRelatorio() {

		for (int i = 0; i < preencherListaEmpresaGestora.size(); i++) {
			if (entradaIncRelFavorecido.getTpEmpresaConglomerada().equals(
					Long.valueOf(preencherListaEmpresaGestora.get(i).getValue()
							.toString()))) {
				entradaIncRelFavorecido
						.setDsEmpresaConglomerada(preencherListaEmpresaGestora
								.get(i).getLabel());
				break;
			}
		}

		if (itemCheckDirReg) {
			for (int i = 0; i < listaDiretoria.size(); i++) {
				if (entradaIncRelFavorecido.getTpDiretoriaRegional().equals(
						Integer.valueOf(listaDiretoria.get(i).getValue()
								.toString()))) {
					entradaIncRelFavorecido
							.setDsUnidadeOrganizacional(listaDiretoria.get(i)
									.getLabel());
					break;
				}
			}
		} else {
			entradaIncRelFavorecido.setDsUnidadeOrganizacional("");
		}

		if (itemCheckGerReg) {
			for (int i = 0; i < listaGerencia.size(); i++) {
				if (entradaIncRelFavorecido.getTpGerenciaRegional().equals(
						Integer.valueOf(listaGerencia.get(i).getValue()
								.toString()))) {
					entradaIncRelFavorecido.setDsDependencia(listaGerencia.get(
							i).getLabel());
					break;
				}
			}
		} else {
			entradaIncRelFavorecido.setDsDependencia("");
		}

		if (itemCheckSeg) {
			for (int i = 0; i < getPreencheComboSegmento().size(); i++) {
				if (entradaIncRelFavorecido.getCdSegmento().equals(
						Integer.valueOf(getPreencheComboSegmento().get(i)
								.getValue().toString()))) {
					entradaIncRelFavorecido
							.setDsSegmento(getPreencheComboSegmento().get(i)
									.getLabel());
					break;
				}
			}
		} else {
			entradaIncRelFavorecido.setDsSegmento("");
		}

		if (entradaIncRelFavorecido.getTpRelatorio() != null) {
			if (entradaIncRelFavorecido.getTpRelatorio().equals(1)) {
				entradaIncRelFavorecido.setDsRelatorio("ANAL�TICO");
			} else {
				entradaIncRelFavorecido.setDsRelatorio("ESTAT�STICO");
			}
		}
		/*
		if (entradaIncRelFavorecido.getCdAgenciaOperadora() != null) {
			loadAgencia();

		}*/
		entradaIncRelFavorecido.setDtSolicitacao(FormatarData
				.formataTimestampFromPdc(new Date()));

		if (isItemCheckAtiEco()) {
			setDsClasseRamo(getListaCboClasseRamoHash().get(getCboClasseRamo())
					.getClasseRamoFormatado());
			setDsSubRamoAtividade(getListaCboSubRamoAtividadeHash().get(
					getCboSubRamoAtividade()).getSubRamoAtividadeFormatado());
		} else {
			setDsClasseRamo(null);
			setDsSubRamoAtividade(null);
		}
		return "incSolicitacaoRelatoriosFavorecidosConf";
	}

	/**
	 * Confirmar inclusao solic relatorio.
	 *
	 * @return the string
	 */
	public String confirmarInclusaoSolicRelatorio() {

		try {
			entradaIncRelFavorecido
					.setCdClasse(getListaCboClasseRamoHash() != null
							&& getCboClasseRamo() != null ? getListaCboClasseRamoHash()
							.get(getCboClasseRamo()).getCdClassAtividade()
							: "");

			entradaIncRelFavorecido
					.setCdRamo(getListaCboClasseRamoHash() != null
							&& getCboClasseRamo() != null ? getListaCboClasseRamoHash()
									.get(getCboClasseRamo()).getCdRamoAtividade()
									: 0);
		
			entradaIncRelFavorecido
					.setCdSubramo(getListaCboSubRamoAtividadeHash() != null
							&& getCboSubRamoAtividade() != null ? getListaCboSubRamoAtividadeHash()
							.get(getCboSubRamoAtividade())
							.getCdSubRamoAtividade()
							: 0);
			entradaIncRelFavorecido
					.setCdAtividadeEconomica(getListaCboSubRamoAtividadeHash() != null
							&& getCboSubRamoAtividade() != null ? getListaCboSubRamoAtividadeHash()
									.get(getCboSubRamoAtividade())
									.getCdAtividadeEconomica()
									: 0);

			saidaIncRelFavorecido = relatorioFavorecidoService
					.incluirMotivoBloqueioFavorecido(entradaIncRelFavorecido);
			BradescoFacesUtils.addInfoModalMessage("("
					+ saidaIncRelFavorecido.getCodMensagem() + ") "
					+ saidaIncRelFavorecido.getMensagem(),
					"/conSolicitacaoRelatoriosFavorecidos.jsf",
					BradescoViewExceptionActionType.PATH, false);
			incluirSolitacaoRelatorio();
			consultarRelatorios();
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(e.getCode(), 8) + ") "
							+ e.getMessage(),
					"incSolicitacaoRelatoriosFavorecidos",
					BradescoViewExceptionActionType.ACTION, false);
		}

		return "incSolicitacaoRelatoriosFavorecidosConf";
	}

	/**
	 * Voltar incluir solic relatorio favorecido.
	 *
	 * @return the string
	 */
	public String voltarIncluirSolicRelatorioFavorecido() {	    	
	    	setAgenciaFiltro(entradaIncRelFavorecido.getCdAgenciaOperadora());
		return "incSolicitacaoRelatoriosFavorecidos";
	}

	/**
	 * Voltar excluir solic relatorio favorecido.
	 *
	 * @return the string
	 */
	public String voltarExcluirSolicRelatorioFavorecido() {
		setItemSelecionadoListaRelatorio(null);
		return SOLICITACAO_RELATORIOS_FAVORECIDOS;
	}

	/**
	 * Detalhe solic relatorio favorecido.
	 *
	 * @return the string
	 */
	public String detalheSolicRelatorioFavorecido() {

		return "detSolicitacaoRelatoriosFavorecidos";
	}

	/**
	 * Excluir solic relatorio favorecido.
	 *
	 * @return the string
	 */
	public String excluirSolicRelatorioFavorecido() {

		ConRelFavorecidoSaidaDTO saida = listaConRelFavorecido.get(Integer
				.parseInt(itemSelecionadoListaRelatorio));

		entradaDetRelFavorecido.setNrSolicitacao(saida.getNrSolicitacao());
		entradaDetRelFavorecido.setTpSolicitacao(saida
				.getCdSolicitacaoPagamento());

		try {
			saidaDetRelFavorecido = relatorioFavorecidoService
					.pesquisarDetalheRelFavorecido(entradaDetRelFavorecido);
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage(e.getMessage(), false);
			return null;
		}

		return "excSolicitacaoRelatoriosFavorecidos";
	}

	/**
	 * Confirmar exclusao solic relatorio.
	 *
	 * @return the string
	 */
	public String confirmarExclusaoSolicRelatorio() {
		Integer numSolicitacao = entradaDetRelFavorecido.getNrSolicitacao();

		try {
			saidaConfirmaExclusao = relatorioFavorecidoService.excluirMotivoBloqueioFavorecido(numSolicitacao);

			BradescoFacesUtils.addInfoModalMessage("("+ saidaConfirmaExclusao.getCodMensagem() + ") " + saidaConfirmaExclusao.getMensagem(),
					"/conSolicitacaoRelatoriosFavorecidos.jsf", BradescoViewExceptionActionType.PATH, false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + MessageUtils.getMessageCode(p) + ") " + p.getMessage(), false);
			return "";
		}

		incluirSolitacaoRelatorio();
		consultarRelatorios();
		return null;
	}

	/**
	 * Is lista con rel favorecido empty.
	 *
	 * @return true, if is lista con rel favorecido empty
	 */
	public boolean isListaConRelFavorecidoEmpty() {
		return this.listaConRelFavorecido == null
				|| this.listaConRelFavorecido.isEmpty();

	}

	/**
	 * Get: selectItemConRelFavorecido.
	 *
	 * @return selectItemConRelFavorecido
	 */
	public List<SelectItem> getSelectItemConRelFavorecido() {

		if (this.listaConRelFavorecido == null
				|| this.listaConRelFavorecido.isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.listaConRelFavorecido.size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/**
	 * Limpar agencia.
	 */
	public void limparAgencia() {

		if (!itemCheckAgeOpe) {
		    	setAgenciaFiltro(null);
			this.entradaIncRelFavorecido.setCdAgenciaOperadora(null);
			this.entradaIncRelFavorecido.setDsAgencia(null);
		}
		if (!itemCheckParGrpEcp) {
			this.entradaIncRelFavorecido.setCdParticipanteGrupoEconomico(null);
		}
		if (!itemCheckAtiEco) {
			/*
			 * this.entradaIncRelFavorecido.setCdClasse(null);
			 * this.entradaIncRelFavorecido.setCdRamo(null);
			 * this.entradaIncRelFavorecido.setCdSubramo(null);
			 * this.entradaIncRelFavorecido.setCdAtividadeEconomica(null);
			 */
			setCboClasseRamo(null);
			setCboSubRamoAtividade(null);
			setListaCboSubRamoAtividade(new ArrayList<SelectItem>());
		}
		if (!itemCheckSeg) {
			this.entradaIncRelFavorecido.setCdSegmento(null);
		}
		if (!itemCheckDirReg) {
			this.entradaIncRelFavorecido.setTpDiretoriaRegional(null);
		}
		if (!itemCheckGerReg) {
			this.entradaIncRelFavorecido.setTpGerenciaRegional(null);
		}

	}

	/**
	 * Get: listaGerencia.
	 *
	 * @return listaGerencia
	 */
	public List<SelectItem> getListaGerencia() {
		return listaGerencia;
	}

	/**
	 * Set: listaGerencia.
	 *
	 * @param listaGerencia the lista gerencia
	 */
	public void setListaGerencia(List<SelectItem> listaGerencia) {
		this.listaGerencia = listaGerencia;
	}

	/**
	 * Get: entradaConRelFavorecido.
	 *
	 * @return entradaConRelFavorecido
	 */
	public ConRelFavorecidoEntradaDTO getEntradaConRelFavorecido() {
		return entradaConRelFavorecido;
	}

	/**
	 * Set: entradaConRelFavorecido.
	 *
	 * @param entradaConRelFavorecido the entrada con rel favorecido
	 */
	public void setEntradaConRelFavorecido(
			ConRelFavorecidoEntradaDTO entradaConRelFavorecido) {
		this.entradaConRelFavorecido = entradaConRelFavorecido;
	}

	/**
	 * Get: relatorioFavorecidoService.
	 *
	 * @return relatorioFavorecidoService
	 */
	public IRelatorioFavorecidoService getRelatorioFavorecidoService() {
		return relatorioFavorecidoService;
	}

	/**
	 * Set: relatorioFavorecidoService.
	 *
	 * @param relatorioFavorecidoService the relatorio favorecido service
	 */
	public void setRelatorioFavorecidoService(
			IRelatorioFavorecidoService relatorioFavorecidoService) {
		this.relatorioFavorecidoService = relatorioFavorecidoService;
	}

	/**
	 * Get: listaConRelFavorecido.
	 *
	 * @return listaConRelFavorecido
	 */
	public List<ConRelFavorecidoSaidaDTO> getListaConRelFavorecido() {
		return listaConRelFavorecido;
	}

	/**
	 * Set: listaConRelFavorecido.
	 *
	 * @param listaConRelFavorecido the lista con rel favorecido
	 */
	public void setListaConRelFavorecido(
			List<ConRelFavorecidoSaidaDTO> listaConRelFavorecido) {
		this.listaConRelFavorecido = listaConRelFavorecido;
	}

	/**
	 * Get: itemSelecionadoListaRelatorio.
	 *
	 * @return itemSelecionadoListaRelatorio
	 */
	public String getItemSelecionadoListaRelatorio() {
		return itemSelecionadoListaRelatorio;
	}

	/**
	 * Set: itemSelecionadoListaRelatorio.
	 *
	 * @param itemSelecionadoListaRelatorio the item selecionado lista relatorio
	 */
	public void setItemSelecionadoListaRelatorio(
			String itemSelecionadoListaRelatorio) {
		this.itemSelecionadoListaRelatorio = itemSelecionadoListaRelatorio;
	}

	/**
	 * Get: entradaDetRelFavorecido.
	 *
	 * @return entradaDetRelFavorecido
	 */
	public DetRelFavorecidoEntradaDTO getEntradaDetRelFavorecido() {
		return entradaDetRelFavorecido;
	}

	/**
	 * Set: entradaDetRelFavorecido.
	 *
	 * @param entradaDetRelFavorecido the entrada det rel favorecido
	 */
	public void setEntradaDetRelFavorecido(
			DetRelFavorecidoEntradaDTO entradaDetRelFavorecido) {
		this.entradaDetRelFavorecido = entradaDetRelFavorecido;
	}

	/**
	 * Get: saidaDetRelFavorecido.
	 *
	 * @return saidaDetRelFavorecido
	 */
	public DetRelFavorecidoSaidaDTO getSaidaDetRelFavorecido() {
		return saidaDetRelFavorecido;
	}

	/**
	 * Set: saidaDetRelFavorecido.
	 *
	 * @param saidaDetRelFavorecido the saida det rel favorecido
	 */
	public void setSaidaDetRelFavorecido(
			DetRelFavorecidoSaidaDTO saidaDetRelFavorecido) {
		this.saidaDetRelFavorecido = saidaDetRelFavorecido;
	}

	/**
	 * Get: entradaIncRelFavorecido.
	 *
	 * @return entradaIncRelFavorecido
	 */
	public IncRelFavorecidoEntradaDTO getEntradaIncRelFavorecido() {
		return entradaIncRelFavorecido;
	}

	/**
	 * Set: entradaIncRelFavorecido.
	 *
	 * @param entradaIncRelFavorecido the entrada inc rel favorecido
	 */
	public void setEntradaIncRelFavorecido(
			IncRelFavorecidoEntradaDTO entradaIncRelFavorecido) {
		this.entradaIncRelFavorecido = entradaIncRelFavorecido;
	}

	/**
	 * Get: saidaIncRelFavorecido.
	 *
	 * @return saidaIncRelFavorecido
	 */
	public IncRelFavorecidoSaidaDTO getSaidaIncRelFavorecido() {
		return saidaIncRelFavorecido;
	}

	/**
	 * Set: saidaIncRelFavorecido.
	 *
	 * @param saidaIncRelFavorecido the saida inc rel favorecido
	 */
	public void setSaidaIncRelFavorecido(
			IncRelFavorecidoSaidaDTO saidaIncRelFavorecido) {
		this.saidaIncRelFavorecido = saidaIncRelFavorecido;
	}

	/**
	 * Is item check age ope.
	 *
	 * @return true, if is item check age ope
	 */
	public boolean isItemCheckAgeOpe() {
		return itemCheckAgeOpe;
	}

	/**
	 * Set: itemCheckAgeOpe.
	 *
	 * @param itemCheckAgeOpe the item check age ope
	 */
	public void setItemCheckAgeOpe(boolean itemCheckAgeOpe) {
		this.itemCheckAgeOpe = itemCheckAgeOpe;
	}

	/**
	 * Is item check ati eco.
	 *
	 * @return true, if is item check ati eco
	 */
	public boolean isItemCheckAtiEco() {
		return itemCheckAtiEco;
	}

	/**
	 * Set: itemCheckAtiEco.
	 *
	 * @param itemCheckAtiEco the item check ati eco
	 */
	public void setItemCheckAtiEco(boolean itemCheckAtiEco) {
		this.itemCheckAtiEco = itemCheckAtiEco;
	}

	/**
	 * Is item check dir reg.
	 *
	 * @return true, if is item check dir reg
	 */
	public boolean isItemCheckDirReg() {
		return itemCheckDirReg;
	}

	/**
	 * Set: itemCheckDirReg.
	 *
	 * @param itemCheckDirReg the item check dir reg
	 */
	public void setItemCheckDirReg(boolean itemCheckDirReg) {
		this.itemCheckDirReg = itemCheckDirReg;
	}

	/**
	 * Is item check ger reg.
	 *
	 * @return true, if is item check ger reg
	 */
	public boolean isItemCheckGerReg() {
		return itemCheckGerReg;
	}

	/**
	 * Set: itemCheckGerReg.
	 *
	 * @param itemCheckGerReg the item check ger reg
	 */
	public void setItemCheckGerReg(boolean itemCheckGerReg) {
		this.itemCheckGerReg = itemCheckGerReg;
	}

	/**
	 * Is item check par grp ecp.
	 *
	 * @return true, if is item check par grp ecp
	 */
	public boolean isItemCheckParGrpEcp() {
		return itemCheckParGrpEcp;
	}

	/**
	 * Set: itemCheckParGrpEcp.
	 *
	 * @param itemCheckParGrpEcp the item check par grp ecp
	 */
	public void setItemCheckParGrpEcp(boolean itemCheckParGrpEcp) {
		this.itemCheckParGrpEcp = itemCheckParGrpEcp;
	}

	/**
	 * Is item check seg.
	 *
	 * @return true, if is item check seg
	 */
	public boolean isItemCheckSeg() {
		return itemCheckSeg;
	}

	/**
	 * Set: itemCheckSeg.
	 *
	 * @param itemCheckSeg the item check seg
	 */
	public void setItemCheckSeg(boolean itemCheckSeg) {
		this.itemCheckSeg = itemCheckSeg;
	}

	/**
	 * Get: itemServicoSelecionado.
	 *
	 * @return itemServicoSelecionado
	 */
	public String getItemServicoSelecionado() {
		return itemServicoSelecionado;
	}

	/**
	 * Set: itemServicoSelecionado.
	 *
	 * @param itemServicoSelecionado the item servico selecionado
	 */
	public void setItemServicoSelecionado(String itemServicoSelecionado) {
		this.itemServicoSelecionado = itemServicoSelecionado;
	}

	/**
	 * Get: saidaTipoSolicitacao.
	 *
	 * @return saidaTipoSolicitacao
	 */
	public ConRelFavorecidoSaidaDTO getSaidaTipoSolicitacao() {
		return saidaTipoSolicitacao;
	}

	/**
	 * Set: saidaTipoSolicitacao.
	 *
	 * @param saidaTipoSolicitacao the saida tipo solicitacao
	 */
	public void setSaidaTipoSolicitacao(
			ConRelFavorecidoSaidaDTO saidaTipoSolicitacao) {
		this.saidaTipoSolicitacao = saidaTipoSolicitacao;
	}

	/**
	 * Get: entradaTipoSolicitacao.
	 *
	 * @return entradaTipoSolicitacao
	 */
	public ConRelFavorecidoEntradaDTO getEntradaTipoSolicitacao() {
		return entradaTipoSolicitacao;
	}

	/**
	 * Set: entradaTipoSolicitacao.
	 *
	 * @param entradaTipoSolicitacao the entrada tipo solicitacao
	 */
	public void setEntradaTipoSolicitacao(
			ConRelFavorecidoEntradaDTO entradaTipoSolicitacao) {
		this.entradaTipoSolicitacao = entradaTipoSolicitacao;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public String getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(String itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: codigoIdentificacaoEmpresa.
	 *
	 * @return codigoIdentificacaoEmpresa
	 */
	public Integer getCodigoIdentificacaoEmpresa() {
		return codigoIdentificacaoEmpresa;
	}

	/**
	 * Set: codigoIdentificacaoEmpresa.
	 *
	 * @param codigoIdentificacaoEmpresa the codigo identificacao empresa
	 */
	public void setCodigoIdentificacaoEmpresa(Integer codigoIdentificacaoEmpresa) {
		this.codigoIdentificacaoEmpresa = codigoIdentificacaoEmpresa;
	}

	/**
	 * Get: entradaExcRelFavorecido.
	 *
	 * @return entradaExcRelFavorecido
	 */
	public ExcRelFavorecidoSaidaDTO getEntradaExcRelFavorecido() {
		return entradaExcRelFavorecido;
	}

	/**
	 * Set: entradaExcRelFavorecido.
	 *
	 * @param entradaExcRelFavorecido the entrada exc rel favorecido
	 */
	public void setEntradaExcRelFavorecido(
			ExcRelFavorecidoSaidaDTO entradaExcRelFavorecido) {
		this.entradaExcRelFavorecido = entradaExcRelFavorecido;
	}

	/**
	 * Get: saidaExcRelFavorecido.
	 *
	 * @return saidaExcRelFavorecido
	 */
	public ExcRelFavorecidoEntradaDTO getSaidaExcRelFavorecido() {
		return saidaExcRelFavorecido;
	}

	/**
	 * Set: saidaExcRelFavorecido.
	 *
	 * @param saidaExcRelFavorecido the saida exc rel favorecido
	 */
	public void setSaidaExcRelFavorecido(
			ExcRelFavorecidoEntradaDTO saidaExcRelFavorecido) {
		this.saidaExcRelFavorecido = saidaExcRelFavorecido;
	}

	/**
	 * Get: saidaConfirmaExclusao.
	 *
	 * @return saidaConfirmaExclusao
	 */
	public ExcRelFavorecidoSaidaDTO getSaidaConfirmaExclusao() {
		return saidaConfirmaExclusao;
	}

	/**
	 * Set: saidaConfirmaExclusao.
	 *
	 * @param saidaConfirmaExclusao the saida confirma exclusao
	 */
	public void setSaidaConfirmaExclusao(
			ExcRelFavorecidoSaidaDTO saidaConfirmaExclusao) {
		this.saidaConfirmaExclusao = saidaConfirmaExclusao;
	}

	/**
	 * Get: empresaConglomerado.
	 *
	 * @return empresaConglomerado
	 */
	public String getEmpresaConglomerado() {
		return empresaConglomerado;
	}

	/**
	 * Set: empresaConglomerado.
	 *
	 * @param empresaConglomerado the empresa conglomerado
	 */
	public void setEmpresaConglomerado(String empresaConglomerado) {
		this.empresaConglomerado = empresaConglomerado;
	}

	/**
	 * Get: empresaConglomeradoDesc.
	 *
	 * @return empresaConglomeradoDesc
	 */
	public String getEmpresaConglomeradoDesc() {
		return empresaConglomeradoDesc;
	}

	/**
	 * Set: empresaConglomeradoDesc.
	 *
	 * @param empresaConglomeradoDesc the empresa conglomerado desc
	 */
	public void setEmpresaConglomeradoDesc(String empresaConglomeradoDesc) {
		this.empresaConglomeradoDesc = empresaConglomeradoDesc;
	}

	/**
	 * Get: filtroEmpresaConglomerado.
	 *
	 * @return filtroEmpresaConglomerado
	 */
	public String getFiltroEmpresaConglomerado() {
		return filtroEmpresaConglomerado;
	}

	/**
	 * Set: filtroEmpresaConglomerado.
	 *
	 * @param filtroEmpresaConglomerado the filtro empresa conglomerado
	 */
	public void setFiltroEmpresaConglomerado(String filtroEmpresaConglomerado) {
		this.filtroEmpresaConglomerado = filtroEmpresaConglomerado;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: itemFiltroSelecionado.
	 *
	 * @return itemFiltroSelecionado
	 */
	public String getItemFiltroSelecionado() {
		return itemFiltroSelecionado;
	}

	/**
	 * Set: itemFiltroSelecionado.
	 *
	 * @param itemFiltroSelecionado the item filtro selecionado
	 */
	public void setItemFiltroSelecionado(String itemFiltroSelecionado) {
		this.itemFiltroSelecionado = itemFiltroSelecionado;
	}

	/**
	 * Get: empresaConglomeradoIncluir.
	 *
	 * @return empresaConglomeradoIncluir
	 */
	public Long getEmpresaConglomeradoIncluir() {
		return empresaConglomeradoIncluir;
	}

	/**
	 * Set: empresaConglomeradoIncluir.
	 *
	 * @param empresaConglomeradoIncluir the empresa conglomerado incluir
	 */
	public void setEmpresaConglomeradoIncluir(Long empresaConglomeradoIncluir) {
		this.empresaConglomeradoIncluir = empresaConglomeradoIncluir;
	}

	/**
	 * Get: listaDiretoria.
	 *
	 * @return listaDiretoria
	 */
	public List<SelectItem> getListaDiretoria() {
		return listaDiretoria;
	}

	/**
	 * Set: listaDiretoria.
	 *
	 * @param listaDiretoria the lista diretoria
	 */
	public void setListaDiretoria(List<SelectItem> listaDiretoria) {
		this.listaDiretoria = listaDiretoria;
	}

	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<EmpresaGestoraSaidaDTO> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(
			List<EmpresaGestoraSaidaDTO> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: saidaEmpresaGestora.
	 *
	 * @return saidaEmpresaGestora
	 */
	public EmpresaGestoraSaidaDTO getSaidaEmpresaGestora() {
		return saidaEmpresaGestora;
	}

	/**
	 * Set: saidaEmpresaGestora.
	 *
	 * @param saidaEmpresaGestora the saida empresa gestora
	 */
	public void setSaidaEmpresaGestora(
			EmpresaGestoraSaidaDTO saidaEmpresaGestora) {
		this.saidaEmpresaGestora = saidaEmpresaGestora;
	}

	/**
	 * Get: preencherListaEmpresaGestora.
	 *
	 * @return preencherListaEmpresaGestora
	 */
	public List<SelectItem> getPreencherListaEmpresaGestora() {
		return preencherListaEmpresaGestora;
	}

	/**
	 * Set: preencherListaEmpresaGestora.
	 *
	 * @param preencherListaEmpresaGestora the preencher lista empresa gestora
	 */
	public void setPreencherListaEmpresaGestora(
			List<SelectItem> preencherListaEmpresaGestora) {
		this.preencherListaEmpresaGestora = preencherListaEmpresaGestora;
	}

	/**
	 * Get: entradaConsultarDescricao.
	 *
	 * @return entradaConsultarDescricao
	 */
	public ConsultarDescricaoUnidOrganizacionalSaidaDTO getEntradaConsultarDescricao() {
		return entradaConsultarDescricao;
	}

	/**
	 * Set: entradaConsultarDescricao.
	 *
	 * @param entradaConsultarDescricao the entrada consultar descricao
	 */
	public void setEntradaConsultarDescricao(
			ConsultarDescricaoUnidOrganizacionalSaidaDTO entradaConsultarDescricao) {
		this.entradaConsultarDescricao = entradaConsultarDescricao;
	}

	
	/**
	 * Get: listaCboClasseRamo.
	 *
	 * @return listaCboClasseRamo
	 */
	public List<SelectItem> getListaCboClasseRamo() {
		return listaCboClasseRamo;
	}

	/**
	 * Set: listaCboClasseRamo.
	 *
	 * @param listaCboClasseRamo the lista cbo classe ramo
	 */
	public void setListaCboClasseRamo(List<SelectItem> listaCboClasseRamo) {
		this.listaCboClasseRamo = listaCboClasseRamo;
	}

	/**
	 * Get: listaCboSubRamoAtividade.
	 *
	 * @return listaCboSubRamoAtividade
	 */
	public List<SelectItem> getListaCboSubRamoAtividade() {
		return listaCboSubRamoAtividade;
	}

	/**
	 * Set: listaCboSubRamoAtividade.
	 *
	 * @param listaCboSubRamoAtividade the lista cbo sub ramo atividade
	 */
	public void setListaCboSubRamoAtividade(
			List<SelectItem> listaCboSubRamoAtividade) {
		this.listaCboSubRamoAtividade = listaCboSubRamoAtividade;
	}

	/**
	 * Get: listaCboClasseRamoHash.
	 *
	 * @return listaCboClasseRamoHash
	 */
	public Map<String, ListarClasseRamoAtvddSaidaDTO> getListaCboClasseRamoHash() {
		return listaCboClasseRamoHash;
	}

	/**
	 * Set lista cbo classe ramo hash.
	 *
	 * @param listaCboClasseRamoHash the lista cbo classe ramo hash
	 */
	public void setListaCboClasseRamoHash(
			Map<String, ListarClasseRamoAtvddSaidaDTO> listaCboClasseRamoHash) {
		this.listaCboClasseRamoHash = listaCboClasseRamoHash;
	}

	/**
	 * Get: listaCboSubRamoAtividadeHash.
	 *
	 * @return listaCboSubRamoAtividadeHash
	 */
	public Map<String, ListarSramoAtvddEconcSaidaDTO> getListaCboSubRamoAtividadeHash() {
		return listaCboSubRamoAtividadeHash;
	}

	/**
	 * Set lista cbo sub ramo atividade hash.
	 *
	 * @param listaCboSubRamoAtividadeHash the lista cbo sub ramo atividade hash
	 */
	public void setListaCboSubRamoAtividadeHash(
			Map<String, ListarSramoAtvddEconcSaidaDTO> listaCboSubRamoAtividadeHash) {
		this.listaCboSubRamoAtividadeHash = listaCboSubRamoAtividadeHash;
	}

	/**
	 * Get: dsClasseRamo.
	 *
	 * @return dsClasseRamo
	 */
	public String getDsClasseRamo() {
		return dsClasseRamo;
	}

	/**
	 * Set: dsClasseRamo.
	 *
	 * @param dsClasseRamo the ds classe ramo
	 */
	public void setDsClasseRamo(String dsClasseRamo) {
		this.dsClasseRamo = dsClasseRamo;
	}

	/**
	 * Get: dsSubRamoAtividade.
	 *
	 * @return dsSubRamoAtividade
	 */
	public String getDsSubRamoAtividade() {
		return dsSubRamoAtividade;
	}

	/**
	 * Set: dsSubRamoAtividade.
	 *
	 * @param dsSubRamoAtividade the ds sub ramo atividade
	 */
	public void setDsSubRamoAtividade(String dsSubRamoAtividade) {
		this.dsSubRamoAtividade = dsSubRamoAtividade;
	}

	/**
	 * Get: cboClasseRamo.
	 *
	 * @return cboClasseRamo
	 */
	public String getCboClasseRamo() {
		return cboClasseRamo;
	}

	/**
	 * Set: cboClasseRamo.
	 *
	 * @param cboClasseRamo the cbo classe ramo
	 */
	public void setCboClasseRamo(String cboClasseRamo) {
		this.cboClasseRamo = cboClasseRamo;
	}

	/**
	 * Get: cboSubRamoAtividade.
	 *
	 * @return cboSubRamoAtividade
	 */
	public String getCboSubRamoAtividade() {
		return cboSubRamoAtividade;
	}

	/**
	 * Set: cboSubRamoAtividade.
	 *
	 * @param cboSubRamoAtividade the cbo sub ramo atividade
	 */
	public void setCboSubRamoAtividade(String cboSubRamoAtividade) {
		this.cboSubRamoAtividade = cboSubRamoAtividade;
	}

	/**
	 * Get: manterCadContaDestinoService.
	 *
	 * @return manterCadContaDestinoService
	 */
	public IManterCadContaDestinoService getManterCadContaDestinoService() {
	    return manterCadContaDestinoService;
	}

	/**
	 * Set: manterCadContaDestinoService.
	 *
	 * @param manterCadContaDestinoService the manter cad conta destino service
	 */
	public void setManterCadContaDestinoService(IManterCadContaDestinoService manterCadContaDestinoService) {
	    this.manterCadContaDestinoService = manterCadContaDestinoService;
	}

	/**
	 * Get: agenciaFiltro.
	 *
	 * @return agenciaFiltro
	 */
	public Integer getAgenciaFiltro() {
	    return agenciaFiltro;
	}

	/**
	 * Set: agenciaFiltro.
	 *
	 * @param agenciaFiltro the agencia filtro
	 */
	public void setAgenciaFiltro(Integer agenciaFiltro) {
	    this.agenciaFiltro = agenciaFiltro;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
	    return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasService the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
	    this.consultasService = consultasService;
	}

	/**
	 * Get: saidaGrupoEconomico.
	 *
	 * @return saidaGrupoEconomico
	 */
	public ConsultarDescGrupoEconomicoSaidaDTO getSaidaGrupoEconomico() {
	    return saidaGrupoEconomico;
	}

	/**
	 * Set: saidaGrupoEconomico.
	 *
	 * @param saidaGrupoEconomico the saida grupo economico
	 */
	public void setSaidaGrupoEconomico(ConsultarDescGrupoEconomicoSaidaDTO saidaGrupoEconomico) {
	    this.saidaGrupoEconomico = saidaGrupoEconomico;
	}
	
}