package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.exclusaolotepagamentos;

import java.math.BigDecimal;

public class ConfimarInclusaoLotePagamentosBean {

	private String nrCnpjCpf;
	private String dsRazaoSocial;
	private String dsEmpresa;
	private String nroContrato;
	private String dsContrato;
	private String cdSituacaoContrato;

	private Long qtdePagamentosPrevistos;
	private BigDecimal valorPagamentosPrevistos;

	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	public String getDsEmpresa() {
		return dsEmpresa;
	}

	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	public String getNroContrato() {
		return nroContrato;
	}

	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	public String getDsContrato() {
		return dsContrato;
	}

	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	public Long getQtdePagamentosPrevistos() {
		return qtdePagamentosPrevistos;
	}

	public void setQtdePagamentosPrevistos(Long qtdePagamentosPrevistos) {
		this.qtdePagamentosPrevistos = qtdePagamentosPrevistos;
	}

	public BigDecimal getValorPagamentosPrevistos() {
		return valorPagamentosPrevistos;
	}

	public void setValorPagamentosPrevistos(BigDecimal valorPagamentosPrevistos) {
		this.valorPagamentosPrevistos = valorPagamentosPrevistos;
	}

}
