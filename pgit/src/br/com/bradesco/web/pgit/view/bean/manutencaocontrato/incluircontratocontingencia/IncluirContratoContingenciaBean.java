/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.incluircontratocontingencia
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.incluircontratocontingencia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarListaValoresDiscretosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarListaValoresDiscretosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.IdiomaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.MoedaEconomicaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.MoedaEconomicaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarClubesClientesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.IIncluirContratoContingenciaService;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarListaContasInclusaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ConsultarListaContasInclusaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.IncluirContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.IncluirContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ListarDadosCtaConvnContingenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ListarDadosCtaConvnContingenciaOcorrenciasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.OcorrenciasContaDTO;
import br.com.bradesco.web.pgit.service.business.incluircontratocontingencia.bean.OcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.IListarFuncBradescoService;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ValidarServicoContratadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.validarparametrotela.IValidarParametroTelaService;
import br.com.bradesco.web.pgit.service.business.validarparametrotela.bean.ValidarParametroTelaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.validarparametrotela.bean.ValidarParametroTelaOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: IncluirContratoContingenciaBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class IncluirContratoContingenciaBean {

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

	/** Atributo manterCadContaDestinoService. */
	private IManterCadContaDestinoService manterCadContaDestinoService;

	/** Atributo bancoAgenciaDTO. */
	private ConsultarBancoAgenciaSaidaDTO bancoAgenciaDTO = new ConsultarBancoAgenciaSaidaDTO();

	/** Atributo funcionarioDTO. */
	private ListarFuncionarioSaidaDTO funcionarioDTO = new ListarFuncionarioSaidaDTO();

	/** Atributo listarFuncBradescoService. */
	private IListarFuncBradescoService listarFuncBradescoService;

	/** Atributo tipoConta. */
	private Integer tipoConta;

	/** Atributo dsEmpresaGestoraContrato. */
	private String dsEmpresaGestoraContrato;

	/** Atributo dsTipoContrato. */
	private String dsTipoContrato;

	/** Atributo cdEmpresaGestoraContrato. */
	private Long cdEmpresaGestoraContrato;

	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;

	/** Atributo cdAgenciaGestora. */
	private String cdAgenciaGestora;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo cdIdioma. */
	private Integer cdIdioma;

	/** Atributo cdMoeda. */
	private Integer cdMoeda;

	/** Atributo cdSetor. */
	private Long cdSetor;

	/** Atributo dsIdentificacaoContrato. */
	private String dsIdentificacaoContrato;

	/** Atributo codigoFunc. */
	private String codigoFunc;

	/** Atributo bloqueiaConta. */
	private boolean bloqueiaConta;

	/** Atributo listaGridListarContas. */
	private List<ConsultarListaContasInclusaoSaidaDTO> listaGridListarContas;

	/** Atributo listaGridListarContasSelecionadas. */
	private List<ConsultarListaContasInclusaoSaidaDTO> listaGridListarContasSelecionadas;

	/** Atributo listaGridConvenio. */
	private List<ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO> listaGridConvenio;

	/** Atributo listaGridConvenioRadio. */
	private List<SelectItem> listaGridConvenioRadio;

	/** Atributo listaControleGridListarContas. */
	private List<SelectItem> listaControleGridListarContas;

	/** Atributo itemSelecionadoListaGridConvenio. */
	private Integer itemSelecionadoListaGridConvenio;
	
	/** Atributo contaConvenioDTO. */
	private ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO contaConvenioDTO;

	/** Atributo itemSelecionadoListarContas. */
	private Integer itemSelecionadoListarContas;

	/** Atributo incluirContratoContingenciaServiceImpl. */
	private IIncluirContratoContingenciaService incluirContratoContingenciaServiceImpl;

	/** Atributo validarParametroTelaService. */
	private IValidarParametroTelaService validarParametroTelaService;

	/** Atributo panelBotoes. */
	private boolean panelBotoes;

	/** Atributo panelBotoesContas. */
	private boolean panelBotoesContas;

	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos;

	/** Atributo opcaoChecarTodosContas. */
	private boolean opcaoChecarTodosContas;

	/** Atributo panelConvenio. */
	private boolean panelConvenio;

	/** Atributo listaGridTipoServico. */
	private List<ValidarParametroTelaOcorrenciaSaidaDTO> listaGridTipoServico;

	/** Atributo listaGridTipoServicoSelecionados. */
	private List<ValidarParametroTelaOcorrenciaSaidaDTO> listaGridTipoServicoSelecionados;

	/** Atributo listaControleGridTipoServico. */
	private List<SelectItem> listaControleGridTipoServico;
	
	/** Atributo habilitaConfirmar. */
	private boolean habilitaConfirmar;

	/** Atributo listaTipoConta. */
	private List<SelectItem> listaTipoConta = new ArrayList<SelectItem>();

	/** Atributo listaTipoContaHash. */
	private Map<Integer, String> listaTipoContaHash = new HashMap<Integer, String>();

	/** Atributo listaEmpresaGestora. */
	private List<SelectItem> listaEmpresaGestora = new ArrayList<SelectItem>();

	/** Atributo listaEmpresaGestoraHash. */
	private Map<Long, String> listaEmpresaGestoraHash = new HashMap<Long, String>();

	/** Atributo listaTipoContrato. */
	private List<SelectItem> listaTipoContrato = new ArrayList<SelectItem>();

	/** Atributo listaTipoContratoHash. */
	private Map<Integer, String> listaTipoContratoHash = new HashMap<Integer, String>();

	/** Atributo listaIdioma. */
	private List<SelectItem> listaIdioma = new ArrayList<SelectItem>();

	/** Atributo listaIdiomaHash. */
	private Map<Integer, String> listaIdiomaHash = new HashMap<Integer, String>();

	/** Atributo listaMoeda. */
	private List<SelectItem> listaMoeda = new ArrayList<SelectItem>();

	/** Atributo listaMoedaHash. */
	private Map<Integer, String> listaMoedaHash = new HashMap<Integer, String>();

	/** Atributo listaSetor. */
	private List<SelectItem> listaSetor = new ArrayList<SelectItem>();

	/** Atributo listaSetorHash. */
	private Map<Long, String> listaSetorHash = new HashMap<Long, String>();
	
	/** Atributo entradaContaDTO. */
	private ConsultarListaContasInclusaoEntradaDTO entradaContaDTO = new ConsultarListaContasInclusaoEntradaDTO();

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
	    entradaContaDTO = new ConsultarListaContasInclusaoEntradaDTO();
	    
		this.identificacaoClienteContratoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
				.setSaidaListarClubesClientes(new ListarClubesClientesSaidaDTO());
		setBancoAgenciaDTO(new ConsultarBancoAgenciaSaidaDTO());
		setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean
				.setPaginaCliente("identificacaoClienteIncluirContratoContingencia");
		identificacaoClienteContratoBean
				.setPaginaRetorno("incContratoPagamentosIntegrados");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");

		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean
				.setHabilitaEmpresaGestoraTipoContrato(true);

		limparDadosConta();
		limparDadosContrato();
		listarTipoConta();
		listarEmpresaGestora();
		listarTipoContrato();
		listarIdioma();
		listarMoeda();
		listarSetor();
		habilitaConfirmar = false;
		panelConvenio = false;
		setCdEmpresaGestoraContrato(2269651L);

		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);
	}

	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {
		identificacaoClienteContratoBean.limparDadosCliente();
		limparDadosConta();
		identificacaoClienteContratoBean.limparCampos();
		identificacaoClienteContratoBean.setSaidaListarClubesClientes(new ListarClubesClientesSaidaDTO()); 
		return "";
	}

	/**
	 * Limpar dados conta.
	 *
	 * @return the string
	 */
	public String limparDadosConta() {
		setBloqueiaConta(true);
		setListaGridListarContasSelecionadas(new ArrayList<ConsultarListaContasInclusaoSaidaDTO>());
		return "";
	}
	
	/**
	 * Nome: limparCamposConta
	 * 
	 * @return
	 */
	public String limparCamposConta() {
	    this.entradaContaDTO.setCdAgencia(null);
	    this.entradaContaDTO.setCdConta(null);
	    
	    return "";
	}

	/**
	 * Limpar gerente responsavel incluir.
	 */
	public void limparGerenteResponsavelIncluir() {
		setCodigoFunc("");
		funcionarioDTO = new ListarFuncionarioSaidaDTO();
		bancoAgenciaDTO.setDsAgencia("");
	}

	/**
	 * Limpar gerente nome.
	 */
	public void limparGerenteNome() {
		funcionarioDTO.setDsFuncionario("");
	}

	/**
	 * Limpar dados contrato.
	 *
	 * @return the string
	 */
	public String limparDadosContrato() {
		setCdEmpresaGestoraContrato(2269651L);
		setCdTipoContrato(0);
		setCdAgenciaGestora("");
		setCodigoFunc("");
		bancoAgenciaDTO = new ConsultarBancoAgenciaSaidaDTO();
		funcionarioDTO = new ListarFuncionarioSaidaDTO();
		setCdIdioma(0);
		setCdMoeda(0);
		setCdSetor(0L);
		setDsIdentificacaoContrato("");
		return "";
	}

	/**
	 * Consultar cliente.
	 *
	 * @return the string
	 */
	public String consultarCliente() {

		setBloqueiaConta(true);

		String retorno = identificacaoClienteContratoBean.listarClientes();

		if (retorno.equals("incContratoPagamentosIntegrados")) {
			setBloqueiaConta(false);
		}

		return retorno;

	}

	/**
	 * Selecionar cliente.
	 *
	 * @return the string
	 */
	public String selecionarCliente() {

		setBloqueiaConta(false);

		return identificacaoClienteContratoBean.selecionarListarCliente();

	}

	/**
	 * Consultar conta.
	 *
	 * @return the string
	 */
	public String consultarConta() {
	    String retorno = "";
	    
	    setListaGridListarContas(null);
	    
		try {
			setPanelBotoesContas(false);
			setOpcaoChecarTodosContas(false);

			entradaContaDTO
					.setCdPessoaJuridicaContrato(getCdEmpresaGestoraContrato());

			entradaContaDTO.setCdCpfCnpj(getIdentificacaoClienteContratoBean()
					.getSaidaListarClubesClientes().getCdCpfCnpj());
			entradaContaDTO.setCdFilialCpfCnpj(getIdentificacaoClienteContratoBean()
					.getSaidaListarClubesClientes().getCdFilialCnpj());
			entradaContaDTO
					.setCdControleCpfCnpj(getIdentificacaoClienteContratoBean()
							.getSaidaListarClubesClientes()
							.getCdControleCnpj());

			setListaGridListarContas(getIncluirContratoContingenciaServiceImpl()
					.consultarListaContasInclusao(entradaContaDTO));

			listaControleGridListarContas = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridListarContas().size(); i++) {
				listaControleGridListarContas.add(new SelectItem(i, ""));
			}
			setItemSelecionadoListarContas(null);

			retorno = "CONTAS";

		} catch (PdcAdapterFunctionalException p) {
		    setBloqueiaConta(false);
		    
			String codMensagem = StringUtils.right(p.getCode(), 8);
			if(codMensagem.equalsIgnoreCase("PGIT1446")){
			    retorno = "CONTAS";
			    
			    setBloqueiaConta(true);
			}
			
            BradescoFacesUtils.addInfoModalMessage("(" + codMensagem + ") " + p.getMessage(), false);
		}
		
		entradaContaDTO.setCdBanco(237);
		
		return retorno;
	}
	
    /**
     * Paginar tela listar contas.
     *
     * @param evt the evt
     */
    public void paginarTelaListarContas(ActionEvent evt) {
        consultarConta();
	}	

	/**
	 * Selecionar conta.
	 *
	 * @return the string
	 */
	public String selecionarConta() {
		carregaListaContasSelecionadas();
		return "SELECIONAR";
	}

	/**
	 * Buscar agencia gestora.
	 *
	 * @return the string
	 */
	public String buscarAgenciaGestora() {
		if (!getCdAgenciaGestora().equals("") && getCdAgenciaGestora() != null) {
			try {
				bancoAgenciaDTO = new ConsultarBancoAgenciaSaidaDTO();
				ConsultarBancoAgenciaEntradaDTO entradaDTO = new ConsultarBancoAgenciaEntradaDTO();
				entradaDTO.setCdBanco(237);
				entradaDTO
						.setCdAgencia(Integer.parseInt(getCdAgenciaGestora()));
				entradaDTO.setCdDigitoAgencia(00);
				setBancoAgenciaDTO(getManterCadContaDestinoService()
						.consultarDescricaoBancoAgencia(entradaDTO));

			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				setBancoAgenciaDTO(new ConsultarBancoAgenciaSaidaDTO());
			}
		} else {
			setBancoAgenciaDTO(new ConsultarBancoAgenciaSaidaDTO());
		}
		return "";
	}

	/**
	 * Busca funcionarios.
	 */
	public void buscaFuncionarios() {

		if (!getCodigoFunc().equals("") && getCodigoFunc() != null) {
			try {
				funcionarioDTO = new ListarFuncionarioSaidaDTO();
				setFuncionarioDTO(getListarFuncBradescoService()
						.listarFuncionario(Long.parseLong(getCodigoFunc())));
				setCodigoFunc(Long.toString(funcionarioDTO.getCdFuncionario()));
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
			}
		} else {
			funcionarioDTO = new ListarFuncionarioSaidaDTO();
		}

	}

	/**
	 * Listar tipo conta.
	 */
	public void listarTipoConta() {
		listaTipoConta = new ArrayList<SelectItem>();
		List<ListarTipoContaSaidaDTO> listaSaida = new ArrayList<ListarTipoContaSaidaDTO>();
		listaSaida = this.getComboService().listarTipoConta();

		listaTipoConta.clear();
		listaTipoContaHash.clear();

		for (ListarTipoContaSaidaDTO combo : listaSaida) {
			listaTipoContaHash.put(combo.getCdTipoConta(), combo
					.getDsTipoConta());
			listaTipoConta.add(new SelectItem(combo.getCdTipoConta(), combo
					.getDsTipoConta()));
		}

	}

	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora() {
		try {
			listaEmpresaGestora = new ArrayList<SelectItem>();
			List<EmpresaGestoraSaidaDTO> listaSaida = new ArrayList<EmpresaGestoraSaidaDTO>();
			listaSaida = comboService.listarEmpresasGestoras();

			listaEmpresaGestora.clear();
			listaEmpresaGestoraHash.clear();

			for (EmpresaGestoraSaidaDTO combo : listaSaida) {
				listaEmpresaGestora.add(new SelectItem(combo
						.getCdPessoaJuridicaContrato(), combo.getDsCnpj()));
				listaEmpresaGestoraHash.put(
						combo.getCdPessoaJuridicaContrato(), combo.getDsCnpj());
			}
		} catch (PdcAdapterFunctionalException p) {
			listaEmpresaGestora = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar tipo contrato.
	 */
	public void listarTipoContrato() {
		try {
			listaTipoContrato = new ArrayList<SelectItem>();
			List<TipoContratoSaidaDTO> listaSaida = new ArrayList<TipoContratoSaidaDTO>();
			listaSaida = comboService.listarTipoContrato();

			listaTipoContrato.clear();
			listaTipoContratoHash.clear();

			for (TipoContratoSaidaDTO saida : listaSaida) {
				listaTipoContrato.add(new SelectItem(saida.getCdTipoContrato(),
						saida.getDsTipoContrato()));
				listaTipoContratoHash.put(saida.getCdTipoContrato(), saida
						.getDsTipoContrato());
			}
		} catch (PdcAdapterFunctionalException p) {
			listaTipoContrato = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar idioma.
	 */
	public void listarIdioma() {
		try {
			listaIdioma = new ArrayList<SelectItem>();
			List<IdiomaSaidaDTO> listaSaida = new ArrayList<IdiomaSaidaDTO>();
			listaSaida = comboService.listarIdioma();

			listaIdioma.clear();
			listaIdiomaHash.clear();

			for (IdiomaSaidaDTO combo : listaSaida) {
				listaIdiomaHash.put(combo.getCdIdioma(), combo.getDsIdioma());
				this.listaIdioma.add(new SelectItem(combo.getCdIdioma(), combo
						.getDsIdioma()));
			}
		} catch (PdcAdapterFunctionalException p) {
			listaIdioma = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar moeda.
	 */
	public void listarMoeda() {
		try {
			listaMoeda = new ArrayList<SelectItem>();
			List<MoedaEconomicaSaidaDTO> listaMoedaSaida = new ArrayList<MoedaEconomicaSaidaDTO>();
			MoedaEconomicaEntradaDTO moedaEconomicaEntradaDTO = new MoedaEconomicaEntradaDTO();
			moedaEconomicaEntradaDTO.setCdSituacao(0);

			listaMoedaSaida = comboService
					.listarMoedaEconomica(moedaEconomicaEntradaDTO);
			listaMoeda.clear();
			listaMoedaHash.clear();

			for (MoedaEconomicaSaidaDTO combo : listaMoedaSaida) {
				listaMoedaHash.put(combo.getCdIndicadorEconomico(), combo
						.getDsIndicadorEconomico());
				listaMoeda.add(new SelectItem(combo.getCdIndicadorEconomico(),
						combo.getDsIndicadorEconomico()));
			}
		} catch (PdcAdapterFunctionalException p) {
			listaMoeda = new ArrayList<SelectItem>();
		}

	}

	/**
	 * Listar setor.
	 */
	public void listarSetor() {
		try {
			listaSetor = new ArrayList<SelectItem>();
			List<ConsultarListaValoresDiscretosSaidaDTO> listaSetorSaida = new ArrayList<ConsultarListaValoresDiscretosSaidaDTO>();
			ConsultarListaValoresDiscretosEntradaDTO setorEntradaDTO = new ConsultarListaValoresDiscretosEntradaDTO();
			setorEntradaDTO.setNumeroCombo(1);

			listaSetorSaida = comboService
					.consultarListaValoresDiscretos(setorEntradaDTO);
			listaSetor.clear();
			listaSetorHash.clear();

			for (ConsultarListaValoresDiscretosSaidaDTO combo : listaSetorSaida) {
				listaSetorHash.put(combo.getCdCombo(), combo.getDsCombo());
				listaSetor.add(new SelectItem(combo.getCdCombo(), combo
						.getDsCombo()));
			}
		} catch (PdcAdapterFunctionalException p) {
			listaSetor = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
	    entradaContaDTO = new ConsultarListaContasInclusaoEntradaDTO();
		return "VOLTAR";
	}

	/**
	 * Voltar confirmacao.
	 *
	 * @return the string
	 */
	public String voltarConfirmacao() {
		for (ValidarParametroTelaOcorrenciaSaidaDTO dto : getListaGridTipoServico()) {
				dto.setCheck(false);
		}
		if(panelConvenio){
			setContaConvenioDTO(null);
		}
		for (ValidarParametroTelaOcorrenciaSaidaDTO dto :getListaGridTipoServicoSelecionados()) {
			if(dto.getCdParametroTela().compareTo(2) == 0){
					setPanelBotoes(true);
					setOpcaoChecarTodos(false);
					setPanelConvenio(true);
					setHabilitaConfirmar(true);
					return "VOLTAR";
			}
		}
			setPanelBotoes(false);
			setOpcaoChecarTodos(false);
			setPanelConvenio(false);
			
			setHabilitaConfirmar(false);
		
		return "VOLTAR";
	}

	/**
	 * Confirmar incluir contrato.
	 *
	 * @return the string
	 */
	public String confirmarIncluirContrato() {
		try {
			IncluirContratoPgitEntradaDTO entrada = new IncluirContratoPgitEntradaDTO();
			entrada.setFaseContrato(1);
			entrada
					.setCdPessoaJuridicaNegocio(getCdEmpresaGestoraContrato() != null ? Long
							.valueOf(getCdEmpresaGestoraContrato())
							: 0L);
			entrada.setCdTipoContratoNegocio(getCdTipoContrato());
			entrada.setCdAcaoRelacionamento(0L);
			entrada.setCdContratoNegocioPagamento("0");
			entrada.setCdIndicadorEconomicoMoeda(getCdMoeda());
			entrada.setCdIdioma(getCdIdioma());
			entrada.setDsContratoNegocio(getDsIdentificacaoContrato());
			entrada.setCdTipoResponsavelOrganizacao(0);
			entrada.setCdFuncionalidadeGerContrato(getCodigoFunc() == null
					|| getCodigoFunc().equals("") ? 0L : Long
					.valueOf(getCodigoFunc()));
			entrada.setCdAgenciaContrato(getCdAgenciaGestora() == null
					|| getCdAgenciaGestora().equals("") ? 0 : Integer
					.valueOf(getCdAgenciaGestora()));
			if (getIdentificacaoClienteContratoBean() != null
					&& getIdentificacaoClienteContratoBean()
							.getSaidaListarClubesClientes() != null) {
				entrada.setCdPessoa(getIdentificacaoClienteContratoBean()
						.getSaidaListarClubesClientes().getCdClub());
			}
			entrada
					.setCdPessoaJuridica(getCdEmpresaGestoraContrato() != null ? Long
							.valueOf(getCdEmpresaGestoraContrato())
							: 0L);
			entrada.setCdTipoParticipacaoPessoa(7);

			entrada.setOcorrenciasConta(new ArrayList<OcorrenciasContaDTO>());
			for (ConsultarListaContasInclusaoSaidaDTO o : getListaGridListarContasSelecionadas()) {
				OcorrenciasContaDTO ocorrenciasConta = new OcorrenciasContaDTO();
				ocorrenciasConta.setCdBanco(o.getCdBanco());
				ocorrenciasConta.setCdAgencia(o.getCdAgencia());
				ocorrenciasConta.setCdDigitoAgencia(0);
				ocorrenciasConta.setCdConta(o.getCdConta());
				ocorrenciasConta.setCdDigitoConta(PgitUtil.complementaDigito(o
						.getCdDigitoConta(), 2));
				ocorrenciasConta.setCdTipoConta(o.getCdTipoConta());
				entrada.getOcorrenciasConta().add(ocorrenciasConta);
			}
			entrada.setQtContas(entrada.getOcorrenciasConta().size());

			entrada.setQtServico(getListaGridTipoServicoSelecionados().size());
			entrada.setNrSequenciaContratoNegocio(0L);
			entrada.setCdSetor(Integer.parseInt(String.valueOf(getCdSetor())));
			// Ocorr�ncias
			entrada.setOcorrencias(new ArrayList<OcorrenciasDTO>());
			for (ValidarParametroTelaOcorrenciaSaidaDTO o : getListaGridTipoServicoSelecionados()) {
				entrada.getOcorrencias().add(new OcorrenciasDTO(o.getCdProdutoOperacaoRelacionado()));
			}
			
			//convenio
			if(contaConvenioDTO != null){
				entrada.setCdCpfCnpjConve(contaConvenioDTO.getCdCpfCnpjConve());

				entrada.setCdFilialCnpjConve(contaConvenioDTO.getCdFilialCnpjConve());

				entrada.setCdControleCnpjConve(contaConvenioDTO.getCdCtrlCnpjEmp());

				entrada.setDsConveCtaSalrl(contaConvenioDTO.getDsConveCtaSalrl());

				entrada.setCdBcoEmprConvn(contaConvenioDTO.getCdBancoEmpresaConvn());

				entrada.setCdAgEmprConvn(contaConvenioDTO.getCdAgeEmpresaConvn());

				entrada.setCdCtaEmpresaConvn(contaConvenioDTO.getCdCtaEmpresaConvn());

				entrada.setCdDigEmpresaConvn(contaConvenioDTO.getCdDigEmpresaConvn());

				entrada.setCdConveCtaSalarial(contaConvenioDTO.getCdConveCtaSalarial());
				
				entrada.setCdIndicadorConvnNovo(contaConvenioDTO.getCdConveNovo());
				
			}

			IncluirContratoPgitSaidaDTO saidaDTO = getIncluirContratoContingenciaServiceImpl()
					.incluirContratoPgit(entrada);

			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"#{incluirContratoContingenciaBean.voltarConsultar}",
					BradescoViewExceptionActionType.ACTION, false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "";
	}

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {
		
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setSaidaListarClubesClientes(new ListarClubesClientesSaidaDTO());

		limparDados();
		limparDadosConta();
		limparDadosContrato();

		return "incContratoPagamentosIntegrados";
	}

	/**
	 * Incluir contrato.
	 *
	 * @return the string
	 */
	public String incluirContrato() {
		try {
			setDsEmpresaGestoraContrato("");
			setDsTipoContrato("");
			if (getCdEmpresaGestoraContrato() != null
					&& getListaEmpresaGestoraHash().containsKey(
							getCdEmpresaGestoraContrato())) {
				setDsEmpresaGestoraContrato(getListaEmpresaGestoraHash().get(
						getCdEmpresaGestoraContrato()));
			}
			if (getCdTipoContrato() != null
					&& getListaTipoContratoHash().containsKey(
							getCdTipoContrato())) {
				setDsTipoContrato(getListaTipoContratoHash().get(
						getCdTipoContrato()));
			}

			setOpcaoChecarTodos(false);
			setPanelBotoes(false);

			ValidarParametroTelaEntradaDTO entrada = new ValidarParametroTelaEntradaDTO();
			
			entrada.setCdProdutoOperacaoRelacionado(0);
			entrada.setCdProdutoServicoOperacao(0);
			entrada.setCdRelacionamentoProduto(0);
			entrada.setNrOcorrencias(100);

			setListaGridTipoServico(getValidarParametroTelaService().listarValidarParametroTela(entrada).getOcorrencias());
			BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
					new PDCDataBean());

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "INCLUIR";
	}

	/**
	 * Incluir contrato avancar.
	 *
	 * @return the string
	 */
	public String incluirContratoAvancar() {
		ValidarServicoContratadoEntradaDTO entrada = new ValidarServicoContratadoEntradaDTO();
		List<Integer> lista = new ArrayList<Integer>();
		
		for (int i = 0; i < listaGridTipoServico.size(); i++) {
			if (listaGridTipoServico.get(i).isCheck()) {
				lista.add(listaGridTipoServico.get(i).getCdProdutoOperacaoRelacionado());
			}
		}

		entrada.setOcorrencias(lista);
		entrada.setCdProdutoServico(0);

		try {
			manterCadContaDestinoService.validarServicoContratado(entrada);

			BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(new PDCDataBean());
			carregaListaTipoServicoSelecionados();
			
			for (ValidarParametroTelaOcorrenciaSaidaDTO servico : listaGridTipoServicoSelecionados) {
				if (servico.getCdParametroTela().equals(2)){
					habilitaConfirmar = true;
					panelConvenio = true;
					setContaConvenioDTO(null);
				}
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "+ p.getMessage(), false);
			return "";
		}

		return "AVANCAR";
	}

	/**
	 * Carrega lista tipo servico selecionados.
	 */
	private void carregaListaTipoServicoSelecionados() {
		setListaGridTipoServicoSelecionados(new ArrayList<ValidarParametroTelaOcorrenciaSaidaDTO>());
		for (ValidarParametroTelaOcorrenciaSaidaDTO o : getListaGridTipoServico()) {
			if (o.isCheck()) {
				getListaGridTipoServicoSelecionados().add(o);
			}
		}
	}

	/*private boolean isVerificaServicoSelecionado() {
		for (ConsultarServRelacServperacionalSaidaDTO o : getListaGridTipoServicoSelecionados()) {
			if (o.getCdParametroTela() == 2) {
				return true;
			}
		}

		return false;
	}*/

	/**
	 * Carrega lista contas selecionadas.
	 */
	private void carregaListaContasSelecionadas() {
		setListaGridListarContasSelecionadas(new ArrayList<ConsultarListaContasInclusaoSaidaDTO>());
		for (ConsultarListaContasInclusaoSaidaDTO o : getListaGridListarContas()) {
			if (o.isCheck()) {
				getListaGridListarContasSelecionadas().add(o);
			}
		}
	}

	/**
	 * Checar todos.
	 */
	public void checarTodos() {
		for (int i = 0; i < getListaGridTipoServico().size(); i++) {
			getListaGridTipoServico().get(i).setCheck(opcaoChecarTodos);
		}

		setPanelBotoes(opcaoChecarTodos);
	}

	/**
	 * Pesquisar.
	 */
	public void pesquisar() {
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
		for (ValidarParametroTelaOcorrenciaSaidaDTO o : getListaGridTipoServico()) {
			o.setCheck(false);
		}
	}

	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes() {
		for (ValidarParametroTelaOcorrenciaSaidaDTO o : getListaGridTipoServico()) {
			if (o.isCheck()) {
				setPanelBotoes(true);
				return;
			}
		}
		setPanelBotoes(false);
	}

	/**
	 * Habilitar panel botoes contas.
	 */
	public void habilitarPanelBotoesContas() {
		for (ConsultarListaContasInclusaoSaidaDTO o : getListaGridListarContas()) {
			if (o.isCheck()) {
				setPanelBotoesContas(true);
				return;
			}
		}
		setPanelBotoesContas(false);
	}

	/**
	 * Pesquisar tela listar contas.
	 *
	 * @param evt the evt
	 */
	public void pesquisarTelaListarContas(ActionEvent evt) {

	}

	/**
	 * Pesquisar tela tipo servico.
	 *
	 * @param evt the evt
	 */
	public void pesquisarTelaTipoServico(ActionEvent evt) {

	}
	
	/**
	 * Carrega lista convenios.
	 */
//	public void carregaListaConvenios(){
//		ListarDadosCtaConvnContingenciaEntradaDTO entrada = new ListarDadosCtaConvnContingenciaEntradaDTO();
//		ListarDadosCtaConvnContingenciaOcorrenciasEntradaDTO ocurrEntrada;
//		List<ListarDadosCtaConvnContingenciaOcorrenciasEntradaDTO> listaOcorrencias =  new ArrayList<ListarDadosCtaConvnContingenciaOcorrenciasEntradaDTO>();
//		
//		entrada.setCdCpfCnpjRepresentante(getIdentificacaoClienteContratoBean().getSaidaListarClubesClientes().getCdCpfCnpj());
//		entrada.setCdFilialCnpjRepresentante(getIdentificacaoClienteContratoBean().getSaidaListarClubesClientes().getCdFilialCnpj());
//		entrada.setCdControleCnpjRepresentante(getIdentificacaoClienteContratoBean().getSaidaListarClubesClientes().getCdControleCnpj()); 
//		entrada.setNrOcorrencias(listaGridListarContasSelecionadas.size());
//		
//		for(int i = 0; i< listaGridListarContasSelecionadas.size();i++){
//			ocurrEntrada = new ListarDadosCtaConvnContingenciaOcorrenciasEntradaDTO();
//			ocurrEntrada.setCdAgencia(listaGridListarContasSelecionadas.get(i).getCdAgencia());	
//			ocurrEntrada.setCdBanco(listaGridListarContasSelecionadas.get(i).getCdBanco());	
//			ocurrEntrada.setCdDigitoAgencia(listaGridListarContasSelecionadas.get(i).getCdDigitoAgencia());	
//			ocurrEntrada.setCdConta(listaGridListarContasSelecionadas.get(i).getCdConta());
//			ocurrEntrada.setCdDigitoConta(listaGridListarContasSelecionadas.get(i).getCdDigitoConta());
//			listaOcorrencias.add(ocurrEntrada);
//		}
//		
//		entrada.setOcorrencias(listaOcorrencias);
//		
//		setListaGridConvenio(getIncluirContratoContingenciaServiceImpl().listarDadosCtaConvnContingencia(entrada).getOcorrencias());
//		listaGridConvenioRadio = new ArrayList<SelectItem>();
//		for (int i = 0; i < getListaGridConvenio().size(); i++) {
//			listaGridConvenioRadio.add(new SelectItem(i, ""));
//		}
//		setItemSelecionadoListaGridConvenio(null);
//	}
	
	/**
	 * Vincular contrato.
	 *
	 * @return the string
	 */
//	public String vincularContrato(){
//		carregaListaConvenios();
//		return "SELECIONAR";
//	}
	
	/**
	 * Vincular contrato selecionado.
	 *
	 * @return the string
	 */
//	public String vincularContratoSelecionado(){
//		contaConvenioDTO = new ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO();
//		setContaConvenioDTO(listaGridConvenio.get(itemSelecionadoListaGridConvenio));
//		if(contaConvenioDTO.getCdConveNovo() == 1 ){
//			contaConvenioDTO.setDsConveCtaSalrl(identificacaoClienteContratoBean.getSaidaListarClubesClientes().getDsNomeRazao());
//		}
//		
//		habilitaConfirmar = false;
//		return "SELECIONAR";
//	}
	

	// Sets e Gets
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: listaTipoConta.
	 *
	 * @return listaTipoConta
	 */
	public List<SelectItem> getListaTipoConta() {
		return listaTipoConta;
	}

	/**
	 * Set: listaTipoConta.
	 *
	 * @param listaTipoConta the lista tipo conta
	 */
	public void setListaTipoConta(List<SelectItem> listaTipoConta) {
		this.listaTipoConta = listaTipoConta;
	}

	/**
	 * Get: tipoConta.
	 *
	 * @return tipoConta
	 */
	public Integer getTipoConta() {
		return tipoConta;
	}

	/**
	 * Set: tipoConta.
	 *
	 * @param tipoConta the tipo conta
	 */
	public void setTipoConta(Integer tipoConta) {
		this.tipoConta = tipoConta;
	}

	/**
	 * Get: listaTipoContaHash.
	 *
	 * @return listaTipoContaHash
	 */
	public Map<Integer, String> getListaTipoContaHash() {
		return listaTipoContaHash;
	}

	/**
	 * Set lista tipo conta hash.
	 *
	 * @param listaTipoContaHash the lista tipo conta hash
	 */
	public void setListaTipoContaHash(Map<Integer, String> listaTipoContaHash) {
		this.listaTipoContaHash = listaTipoContaHash;
	}

	/**
	 * Get: cdEmpresaGestoraContrato.
	 *
	 * @return cdEmpresaGestoraContrato
	 */
	public Long getCdEmpresaGestoraContrato() {
		return cdEmpresaGestoraContrato;
	}

	/**
	 * Set: cdEmpresaGestoraContrato.
	 *
	 * @param cdEmpresaGestoraContrato the cd empresa gestora contrato
	 */
	public void setCdEmpresaGestoraContrato(Long cdEmpresaGestoraContrato) {
		this.cdEmpresaGestoraContrato = cdEmpresaGestoraContrato;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: listaEmpresaGestoraHash.
	 *
	 * @return listaEmpresaGestoraHash
	 */
	public Map<Long, String> getListaEmpresaGestoraHash() {
		return listaEmpresaGestoraHash;
	}

	/**
	 * Set lista empresa gestora hash.
	 *
	 * @param listaEmpresaGestoraHash the lista empresa gestora hash
	 */
	public void setListaEmpresaGestoraHash(
			Map<Long, String> listaEmpresaGestoraHash) {
		this.listaEmpresaGestoraHash = listaEmpresaGestoraHash;
	}

	/**
	 * Get: listaTipoContrato.
	 *
	 * @return listaTipoContrato
	 */
	public List<SelectItem> getListaTipoContrato() {
		return listaTipoContrato;
	}

	/**
	 * Set: listaTipoContrato.
	 *
	 * @param listaTipoContrato the lista tipo contrato
	 */
	public void setListaTipoContrato(List<SelectItem> listaTipoContrato) {
		this.listaTipoContrato = listaTipoContrato;
	}

	/**
	 * Get: listaTipoContratoHash.
	 *
	 * @return listaTipoContratoHash
	 */
	public Map<Integer, String> getListaTipoContratoHash() {
		return listaTipoContratoHash;
	}

	/**
	 * Set lista tipo contrato hash.
	 *
	 * @param listaTipoContratoHash the lista tipo contrato hash
	 */
	public void setListaTipoContratoHash(
			Map<Integer, String> listaTipoContratoHash) {
		this.listaTipoContratoHash = listaTipoContratoHash;
	}

	/**
	 * Get: cdAgenciaGestora.
	 *
	 * @return cdAgenciaGestora
	 */
	public String getCdAgenciaGestora() {
		return cdAgenciaGestora;
	}

	/**
	 * Set: cdAgenciaGestora.
	 *
	 * @param cdAgenciaGestora the cd agencia gestora
	 */
	public void setCdAgenciaGestora(String cdAgenciaGestora) {
		this.cdAgenciaGestora = cdAgenciaGestora;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: cdIdioma.
	 *
	 * @return cdIdioma
	 */
	public Integer getCdIdioma() {
		return cdIdioma;
	}

	/**
	 * Set: cdIdioma.
	 *
	 * @param cdIdioma the cd idioma
	 */
	public void setCdIdioma(Integer cdIdioma) {
		this.cdIdioma = cdIdioma;
	}

	/**
	 * Get: cdMoeda.
	 *
	 * @return cdMoeda
	 */
	public Integer getCdMoeda() {
		return cdMoeda;
	}

	/**
	 * Set: cdMoeda.
	 *
	 * @param cdMoeda the cd moeda
	 */
	public void setCdMoeda(Integer cdMoeda) {
		this.cdMoeda = cdMoeda;
	}

	/**
	 * Get: cdSetor.
	 *
	 * @return cdSetor
	 */
	public Long getCdSetor() {
		return cdSetor;
	}

	/**
	 * Set: cdSetor.
	 *
	 * @param cdSetor the cd setor
	 */
	public void setCdSetor(Long cdSetor) {
		this.cdSetor = cdSetor;
	}

	/**
	 * Get: listaIdioma.
	 *
	 * @return listaIdioma
	 */
	public List<SelectItem> getListaIdioma() {
		return listaIdioma;
	}

	/**
	 * Set: listaIdioma.
	 *
	 * @param listaIdioma the lista idioma
	 */
	public void setListaIdioma(List<SelectItem> listaIdioma) {
		this.listaIdioma = listaIdioma;
	}

	/**
	 * Get: listaIdiomaHash.
	 *
	 * @return listaIdiomaHash
	 */
	public Map<Integer, String> getListaIdiomaHash() {
		return listaIdiomaHash;
	}

	/**
	 * Set lista idioma hash.
	 *
	 * @param listaIdiomaHash the lista idioma hash
	 */
	public void setListaIdiomaHash(Map<Integer, String> listaIdiomaHash) {
		this.listaIdiomaHash = listaIdiomaHash;
	}

	/**
	 * Get: listaMoeda.
	 *
	 * @return listaMoeda
	 */
	public List<SelectItem> getListaMoeda() {
		return listaMoeda;
	}

	/**
	 * Set: listaMoeda.
	 *
	 * @param listaMoeda the lista moeda
	 */
	public void setListaMoeda(List<SelectItem> listaMoeda) {
		this.listaMoeda = listaMoeda;
	}

	/**
	 * Get: listaMoedaHash.
	 *
	 * @return listaMoedaHash
	 */
	public Map<Integer, String> getListaMoedaHash() {
		return listaMoedaHash;
	}

	/**
	 * Set lista moeda hash.
	 *
	 * @param listaMoedaHash the lista moeda hash
	 */
	public void setListaMoedaHash(Map<Integer, String> listaMoedaHash) {
		this.listaMoedaHash = listaMoedaHash;
	}

	/**
	 * Get: listaSetor.
	 *
	 * @return listaSetor
	 */
	public List<SelectItem> getListaSetor() {
		return listaSetor;
	}

	/**
	 * Set: listaSetor.
	 *
	 * @param listaSetor the lista setor
	 */
	public void setListaSetor(List<SelectItem> listaSetor) {
		this.listaSetor = listaSetor;
	}

	/**
	 * Get: listaSetorHash.
	 *
	 * @return listaSetorHash
	 */
	public Map<Long, String> getListaSetorHash() {
		return listaSetorHash;
	}

	/**
	 * Set lista setor hash.
	 *
	 * @param listaSetorHash the lista setor hash
	 */
	public void setListaSetorHash(Map<Long, String> listaSetorHash) {
		this.listaSetorHash = listaSetorHash;
	}

	/**
	 * Get: dsIdentificacaoContrato.
	 *
	 * @return dsIdentificacaoContrato
	 */
	public String getDsIdentificacaoContrato() {
		if (dsIdentificacaoContrato != null) {
			return dsIdentificacaoContrato.toUpperCase();
		}
		return dsIdentificacaoContrato;
	}

	/**
	 * Set: dsIdentificacaoContrato.
	 *
	 * @param dsIdentificacaoContrato the ds identificacao contrato
	 */
	public void setDsIdentificacaoContrato(String dsIdentificacaoContrato) {
		this.dsIdentificacaoContrato = dsIdentificacaoContrato;
	}

	/**
	 * Get: bancoAgenciaDTO.
	 *
	 * @return bancoAgenciaDTO
	 */
	public ConsultarBancoAgenciaSaidaDTO getBancoAgenciaDTO() {
		return bancoAgenciaDTO;
	}

	/**
	 * Set: bancoAgenciaDTO.
	 *
	 * @param bancoAgenciaDTO the banco agencia dto
	 */
	public void setBancoAgenciaDTO(ConsultarBancoAgenciaSaidaDTO bancoAgenciaDTO) {
		this.bancoAgenciaDTO = bancoAgenciaDTO;
	}

	/**
	 * Get: manterCadContaDestinoService.
	 *
	 * @return manterCadContaDestinoService
	 */
	public IManterCadContaDestinoService getManterCadContaDestinoService() {
		return manterCadContaDestinoService;
	}

	/**
	 * Set: manterCadContaDestinoService.
	 *
	 * @param manterCadContaDestinoService the manter cad conta destino service
	 */
	public void setManterCadContaDestinoService(
			IManterCadContaDestinoService manterCadContaDestinoService) {
		this.manterCadContaDestinoService = manterCadContaDestinoService;
	}

	/**
	 * Is panel botoes.
	 *
	 * @return true, if is panel botoes
	 */
	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	/**
	 * Set: panelBotoes.
	 *
	 * @param panelBotoes the panel botoes
	 */
	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Get: incluirContratoContingenciaServiceImpl.
	 *
	 * @return incluirContratoContingenciaServiceImpl
	 */
	public IIncluirContratoContingenciaService getIncluirContratoContingenciaServiceImpl() {
		return incluirContratoContingenciaServiceImpl;
	}

	/**
	 * Set: incluirContratoContingenciaServiceImpl.
	 *
	 * @param incluirContratoContingenciaServiceImpl the incluir contrato contingencia service impl
	 */
	public void setIncluirContratoContingenciaServiceImpl(
			IIncluirContratoContingenciaService incluirContratoContingenciaServiceImpl) {
		this.incluirContratoContingenciaServiceImpl = incluirContratoContingenciaServiceImpl;
	}

	/**
	 * Get: itemSelecionadoListarContas.
	 *
	 * @return itemSelecionadoListarContas
	 */
	public Integer getItemSelecionadoListarContas() {
		return itemSelecionadoListarContas;
	}

	/**
	 * Set: itemSelecionadoListarContas.
	 *
	 * @param itemSelecionadoListarContas the item selecionado listar contas
	 */
	public void setItemSelecionadoListarContas(
			Integer itemSelecionadoListarContas) {
		this.itemSelecionadoListarContas = itemSelecionadoListarContas;
	}

	/**
	 * Get: listaControleGridListarContas.
	 *
	 * @return listaControleGridListarContas
	 */
	public List<SelectItem> getListaControleGridListarContas() {
		return listaControleGridListarContas;
	}

	/**
	 * Set: listaControleGridListarContas.
	 *
	 * @param listaControleGridListarContas the lista controle grid listar contas
	 */
	public void setListaControleGridListarContas(
			List<SelectItem> listaControleGridListarContas) {
		this.listaControleGridListarContas = listaControleGridListarContas;
	}

	/**
	 * Get: listaGridListarContas.
	 *
	 * @return listaGridListarContas
	 */
	public List<ConsultarListaContasInclusaoSaidaDTO> getListaGridListarContas() {
		return listaGridListarContas;
	}

	/**
	 * Set: listaGridListarContas.
	 *
	 * @param listaGridListarContas the lista grid listar contas
	 */
	public void setListaGridListarContas(
			List<ConsultarListaContasInclusaoSaidaDTO> listaGridListarContas) {
		this.listaGridListarContas = listaGridListarContas;
	}

	/**
	 * Get: dsEmpresaGestoraContrato.
	 *
	 * @return dsEmpresaGestoraContrato
	 */
	public String getDsEmpresaGestoraContrato() {
		return dsEmpresaGestoraContrato;
	}

	/**
	 * Set: dsEmpresaGestoraContrato.
	 *
	 * @param dsEmpresaGestoraContrato the ds empresa gestora contrato
	 */
	public void setDsEmpresaGestoraContrato(String dsEmpresaGestoraContrato) {
		this.dsEmpresaGestoraContrato = dsEmpresaGestoraContrato;
	}

	/**
	 * Get: dsTipoContrato.
	 *
	 * @return dsTipoContrato
	 */
	public String getDsTipoContrato() {
		return dsTipoContrato;
	}

	/**
	 * Set: dsTipoContrato.
	 *
	 * @param dsTipoContrato the ds tipo contrato
	 */
	public void setDsTipoContrato(String dsTipoContrato) {
		this.dsTipoContrato = dsTipoContrato;
	}

	/**
	 * Get: codigoFunc.
	 *
	 * @return codigoFunc
	 */
	public String getCodigoFunc() {
		return codigoFunc;
	}

	/**
	 * Set: codigoFunc.
	 *
	 * @param codigoFunc the codigo func
	 */
	public void setCodigoFunc(String codigoFunc) {
		this.codigoFunc = codigoFunc;
	}

	/**
	 * Get: funcionarioDTO.
	 *
	 * @return funcionarioDTO
	 */
	public ListarFuncionarioSaidaDTO getFuncionarioDTO() {
		return funcionarioDTO;
	}

	/**
	 * Set: funcionarioDTO.
	 *
	 * @param funcionarioDTO the funcionario dto
	 */
	public void setFuncionarioDTO(ListarFuncionarioSaidaDTO funcionarioDTO) {
		this.funcionarioDTO = funcionarioDTO;
	}

	/**
	 * Get: listarFuncBradescoService.
	 *
	 * @return listarFuncBradescoService
	 */
	public IListarFuncBradescoService getListarFuncBradescoService() {
		return listarFuncBradescoService;
	}

	/**
	 * Set: listarFuncBradescoService.
	 *
	 * @param listarFuncBradescoService the listar func bradesco service
	 */
	public void setListarFuncBradescoService(
			IListarFuncBradescoService listarFuncBradescoService) {
		this.listarFuncBradescoService = listarFuncBradescoService;
	}

	/**
	 * Is bloqueia conta.
	 *
	 * @return true, if is bloqueia conta
	 */
	public boolean isBloqueiaConta() {
		return bloqueiaConta;
	}

	/**
	 * Set: bloqueiaConta.
	 *
	 * @param bloqueiaConta the bloqueia conta
	 */
	public void setBloqueiaConta(boolean bloqueiaConta) {
		this.bloqueiaConta = bloqueiaConta;
	}

	/**
	 * Get: listaGridListarContasSelecionadas.
	 *
	 * @return listaGridListarContasSelecionadas
	 */
	public List<ConsultarListaContasInclusaoSaidaDTO> getListaGridListarContasSelecionadas() {
		return listaGridListarContasSelecionadas;
	}

	/**
	 * Set: listaGridListarContasSelecionadas.
	 *
	 * @param listaGridListarContasSelecionadas the lista grid listar contas selecionadas
	 */
	public void setListaGridListarContasSelecionadas(
			List<ConsultarListaContasInclusaoSaidaDTO> listaGridListarContasSelecionadas) {
		this.listaGridListarContasSelecionadas = listaGridListarContasSelecionadas;
	}

	/**
	 * Is opcao checar todos contas.
	 *
	 * @return true, if is opcao checar todos contas
	 */
	public boolean isOpcaoChecarTodosContas() {
		return opcaoChecarTodosContas;
	}

	/**
	 * Set: opcaoChecarTodosContas.
	 *
	 * @param opcaoChecarTodosContas the opcao checar todos contas
	 */
	public void setOpcaoChecarTodosContas(boolean opcaoChecarTodosContas) {
		this.opcaoChecarTodosContas = opcaoChecarTodosContas;
	}

	/**
	 * Is panel botoes contas.
	 *
	 * @return true, if is panel botoes contas
	 */
	public boolean isPanelBotoesContas() {
		return panelBotoesContas;
	}

	/**
	 * Set: panelBotoesContas.
	 *
	 * @param panelBotoesContas the panel botoes contas
	 */
	public void setPanelBotoesContas(boolean panelBotoesContas) {
		this.panelBotoesContas = panelBotoesContas;
	}

	/**
	 * Get: listaGridConvenioRadio.
	 *
	 * @return listaGridConvenioRadio
	 */
	public List<SelectItem> getListaGridConvenioRadio() {
		return listaGridConvenioRadio;
	}

	/**
	 * Set: listaGridConvenioRadio.
	 *
	 * @param listaGridConvenioRadio the lista grid convenio radio
	 */
	public void setListaGridConvenioRadio(List<SelectItem> listaGridConvenioRadio) {
		this.listaGridConvenioRadio = listaGridConvenioRadio;
	}

	/**
	 * Get: listaGridConvenio.
	 *
	 * @return listaGridConvenio
	 */
	public List<ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO> getListaGridConvenio() {
		return listaGridConvenio;
	}

	/**
	 * Set: listaGridConvenio.
	 *
	 * @param listaGridConvenio the lista grid convenio
	 */
	public void setListaGridConvenio(
			List<ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO> listaGridConvenio) {
		this.listaGridConvenio = listaGridConvenio;
	}

	/**
	 * Get: itemSelecionadoListaGridConvenio.
	 *
	 * @return itemSelecionadoListaGridConvenio
	 */
	public Integer getItemSelecionadoListaGridConvenio() {
		return itemSelecionadoListaGridConvenio;
	}

	/**
	 * Set: itemSelecionadoListaGridConvenio.
	 *
	 * @param itemSelecionadoListaGridConvenio the item selecionado lista grid convenio
	 */
	public void setItemSelecionadoListaGridConvenio(
			Integer itemSelecionadoListaGridConvenio) {
		this.itemSelecionadoListaGridConvenio = itemSelecionadoListaGridConvenio;
	}

	/**
	 * Get: contaConvenioDTO.
	 *
	 * @return contaConvenioDTO
	 */
	public ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO getContaConvenioDTO() {
		return contaConvenioDTO;
	}

	/**
	 * Set: contaConvenioDTO.
	 *
	 * @param contaConvenioDTO the conta convenio dto
	 */
	public void setContaConvenioDTO(
			ListarDadosCtaConvnContingenciaOcorrenciasSaidaDTO contaConvenioDTO) {
		this.contaConvenioDTO = contaConvenioDTO;
	}

	/**
	 * Is habilita confirmar.
	 *
	 * @return true, if is habilita confirmar
	 */
	public boolean isHabilitaConfirmar() {
		return habilitaConfirmar;
	}

	/**
	 * Set: habilitaConfirmar.
	 *
	 * @param habilitaConfirmar the habilita confirmar
	 */
	public void setHabilitaConfirmar(boolean habilitaConfirmar) {
		this.habilitaConfirmar = habilitaConfirmar;
	}

	/**
	 * Get: listaGridTipoServico.
	 *
	 * @return listaGridTipoServico
	 */
	public List<ValidarParametroTelaOcorrenciaSaidaDTO> getListaGridTipoServico() {
		return listaGridTipoServico;
	}

	/**
	 * Set: listaGridTipoServico.
	 *
	 * @param listaGridTipoServico the lista grid tipo servico
	 */
	public void setListaGridTipoServico(
			List<ValidarParametroTelaOcorrenciaSaidaDTO> listaGridTipoServico) {
		this.listaGridTipoServico = listaGridTipoServico;
	}

	/**
	 * Get: listaGridTipoServicoSelecionados.
	 *
	 * @return listaGridTipoServicoSelecionados
	 */
	public List<ValidarParametroTelaOcorrenciaSaidaDTO> getListaGridTipoServicoSelecionados() {
		return listaGridTipoServicoSelecionados;
	}

	/**
	 * Set: listaGridTipoServicoSelecionados.
	 *
	 * @param listaGridTipoServicoSelecionados the lista grid tipo servico selecionados
	 */
	public void setListaGridTipoServicoSelecionados(
			List<ValidarParametroTelaOcorrenciaSaidaDTO> listaGridTipoServicoSelecionados) {
		this.listaGridTipoServicoSelecionados = listaGridTipoServicoSelecionados;
	}

	/**
	 * Get: listaControleGridTipoServico.
	 *
	 * @return listaControleGridTipoServico
	 */
	public List<SelectItem> getListaControleGridTipoServico() {
		return listaControleGridTipoServico;
	}

	/**
	 * Set: listaControleGridTipoServico.
	 *
	 * @param listaControleGridTipoServico the lista controle grid tipo servico
	 */
	public void setListaControleGridTipoServico(
			List<SelectItem> listaControleGridTipoServico) {
		this.listaControleGridTipoServico = listaControleGridTipoServico;
	}

	/**
	 * Get: validarParametroTelaService.
	 *
	 * @return validarParametroTelaService
	 */
	public IValidarParametroTelaService getValidarParametroTelaService() {
		return validarParametroTelaService;
	}

	/**
	 * Set: validarParametroTelaService.
	 *
	 * @param validarParametroTelaService the validar parametro tela service
	 */
	public void setValidarParametroTelaService(
			IValidarParametroTelaService validarParametroTelaService) {
		this.validarParametroTelaService = validarParametroTelaService;
	}

	/**
	 * Is panel convenio.
	 *
	 * @return true, if is panel convenio
	 */
	public boolean isPanelConvenio() {
		return panelConvenio;
	}

	/**
	 * Set: panelConvenio.
	 *
	 * @param panelConvenio the panel convenio
	 */
	public void setPanelConvenio(boolean panelConvenio) {
		this.panelConvenio = panelConvenio;
	}

    /**
     * Nome: getEntradaContaDTO
     *
     * @return entradaContaDTO
     */
    public ConsultarListaContasInclusaoEntradaDTO getEntradaContaDTO() {
        return entradaContaDTO;
    }

    /**
     * Nome: setEntradaContaDTO
     *
     * @param entradaContaDTO
     */
    public void setEntradaContaDTO(ConsultarListaContasInclusaoEntradaDTO entradaContaDTO) {
        this.entradaContaDTO = entradaContaDTO;
    }


}