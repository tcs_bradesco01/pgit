/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterperfiltrocaarqlayout
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterperfiltrocaarqlayout;

import static br.com.bradesco.web.pgit.utils.PgitUtil.verificaIntegerNulo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.IAssocLayoutArqProdServService;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ListarAssLayoutArqProdServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclayoutarqprodserv.bean.ListarAssLayoutArqProdServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarAplicativoFormatacaoArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarAplicativoFormatacaoArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarEmpresaTransmissaoArquivoVanEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarEmpresaTransmissaoArquivoVanSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMeioTransmissaoPagamentoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMeioTransmissaoPagamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.PeriodicidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.PeriodicidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.IFiltroAgendamentoEfetivacaoEstornoService;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ConsultarContratoClienteFiltroSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ListarPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.AlterarPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.AlterarPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarHistoricoPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarHistoricoPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharHistoricoPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharHistoricoPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.DetalharPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ExcluirPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ExcluirPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.IncluirPerfilTrocaArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.IncluirPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ListaContratosVinculadosPerfilEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ListaContratosVinculadosPerfilOcorrencias;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.ManterContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterPerfilTrocaArqLayoutBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterPerfilTrocaArqLayoutBean {

	/** Atributo MEIO_TRANSMISSAO_WEB_TA. */
	private static final Integer MEIO_TRANSMISSAO_WEB_TA = 2;

	// NAVEGA��O
	/** Atributo TELA_CONSULTAR. */
	private static final String TELA_CONSULTAR = "TELA_CONSULTAR";

	/** Atributo TELA_INCLUIR. */
	private static final String TELA_INCLUIR = "TELA_INCLUIR";

	/** Atributo TELA_INCLUIR_AVANCAR. */
	private static final String TELA_INCLUIR_AVANCAR = "TELA_INCLUIR_AVANCAR";

	/** Atributo TELA_ALTERAR. */
	private static final String TELA_ALTERAR = "TELA_ALTERAR";

	/** Atributo TELA_ALTERAR_AVANCAR. */
	private static final String TELA_ALTERAR_AVANCAR = "TELA_ALTERAR_AVANCAR";

	/** Atributo TELA_EXCLUIR. */
	private static final String TELA_EXCLUIR = "TELA_EXCLUIR";

	/** Atributo TELA_DETALHAR. */
	private static final String TELA_DETALHAR = "TELA_DETALHAR";

	/** Atributo TELA_HISTORICO. */
	private static final String TELA_HISTORICO = "TELA_HISTORICO";

	/** Atributo TELA_DETALHAR_HISTORICO. */
	private static final String TELA_DETALHAR_HISTORICO = "TELA_DETALHAR_HISTORICO";

	/** Atributo TELA_CONTRATO. */
	private static final String TELA_CONTRATO = "TELA_CONTRATO";
	
	//bean
	
	/** Atributo manterContratoBean. */
	private ManterContratoBean manterContratoBean = null;

	// IMPL
	/** Atributo manterPerfilTrocaArqLayoutServiceImpl. */
	private IManterPerfilTrocaArqLayoutService manterPerfilTrocaArqLayoutServiceImpl = null;
	
	/** Atributo manterPerfilTrocaArqLayoutServiceImpl. */
	private IAssocLayoutArqProdServService assocLayoutArqProdServImpl = null;

	/** Atributo comboServiceImpl. */
	private IComboService comboServiceImpl = null;
	
	/** Atributo filtroIdentificaoService. */
	private IFiltroIdentificaoService filtroIdentificaoService = null;
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoImpl. */
	private IFiltroAgendamentoEfetivacaoEstornoService filtroAgendamentoEfetivacaoEstornoImpl = null;	

	// COMBOS
	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();

	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer, String> listaTipoLayoutArquivoHash = new HashMap<Integer, String>();
	
	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaAplicativoFormatArquivo = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer, String> listaAplicativoFormatArquivoHash = new HashMap<Integer, String>();

	/** Atributo listaAplicativoFormatacao. */
	private List<SelectItem> listaAplicativoFormatacao = new ArrayList<SelectItem>();

	/** Atributo listaAplicativoFormatacaoHash. */
	private Map<Integer, String> listaAplicativoFormatacaoHash = new HashMap<Integer, String>();

	/** Atributo listaSistemaOrigemArquivo. */
	private List<SelectItem> listaSistemaOrigemArquivo = new ArrayList<SelectItem>();

	/** Atributo listaSistemaOrigemArquivoHash. */
	private Map<String, String> listaSistemaOrigemArquivoHash = new HashMap<String, String>();

	/** Atributo listaEmpresaResponsavelTransmissao. */
	private List<SelectItem> listaEmpresaResponsavelTransmissao = new ArrayList<SelectItem>();

	/** Atributo listaEmpresaResponsavelTransmissaoHash. */
	private Map<Long, String> listaEmpresaResponsavelTransmissaoHash = new HashMap<Long, String>();

	/** Atributo listaTipoRejeicaoAcolhimento. */
	private List<SelectItem> listaTipoRejeicaoAcolhimento = new ArrayList<SelectItem>();

	/** Atributo listaTipoRejeicaoAcolhimentoHash. */
	private Map<Integer, String> listaTipoRejeicaoAcolhimentoHash = new HashMap<Integer, String>();

	/** Atributo listaNivelControle. */
	private List<SelectItem> listaNivelControle = new ArrayList<SelectItem>();

	/** Atributo listaNivelControleHash. */
	private Map<Integer, String> listaNivelControleHash = new HashMap<Integer, String>();

	/** Atributo listaNivelControleRetorno. */
	private List<SelectItem> listaNivelControleRetorno = new ArrayList<SelectItem>();

	/** Atributo listaNivelControleRetornoHash. */
	private Map<Integer, String> listaNivelControleRetornoHash = new HashMap<Integer, String>();

	/** Atributo listaTipoControle. */
	private List<SelectItem> listaTipoControle = new ArrayList<SelectItem>();

	/** Atributo listaTipoControleHash. */
	private Map<Integer, String> listaTipoControleHash = new HashMap<Integer, String>();

	/** Atributo listaTipoControleRetorno. */
	private List<SelectItem> listaTipoControleRetorno = new ArrayList<SelectItem>();

	/** Atributo listaTipoControleRetornoHash. */
	private Map<Integer, String> listaTipoControleRetornoHash = new HashMap<Integer, String>();

	/** Atributo listaTipoControleRemessa. */
	private List<SelectItem> listaTipoControleRemessa = new ArrayList<SelectItem>();

	/** Atributo listaTipoControleRemessaHash. */
	private Map<Integer, String> listaTipoControleRemessaHash = new HashMap<Integer, String>();
	
	/** Atributo listaPeriodicidadeInicializacaoContagem. */
	private List<SelectItem> listaPeriodicidadeInicializacaoContagem = new ArrayList<SelectItem>();

	/** Atributo listaPeriodicidadeInicializacaoContagemHash. */
	private Map<Integer, String> listaPeriodicidadeInicializacaoContagemHash = new HashMap<Integer, String>();

	/** Atributo listaMeioTransmissao. */
	private List<SelectItem> listaMeioTransmissao = new ArrayList<SelectItem>();

	/** Atributo listaMeioTransmissaoHash. */
	private Map<Integer, String> listaMeioTransmissaoHash = new HashMap<Integer, String>();

	// CONSULTAR
	/** Atributo listaGridConsultar. */
	private List<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO> listaGridConsultar = null;

	/** Atributo listaControleConsultar. */
	private List<SelectItem> listaControleConsultar = null;

	/** Atributo itemSelecionadoConsultar. */
	private Integer itemSelecionadoConsultar = null;		
	
	private String tipoServico;

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta = false;

	/** Atributo codigoPerfilFiltro. */
	private Long codigoPerfilFiltro = null;

	/** Atributo tipoLayoutArquivoFiltro. */
	private Integer tipoLayoutArquivoFiltro = null;
	
	/** Atributo consultarListaPerfilTrocaArquivoSaida. */
	private ConsultarListaPerfilTrocaArquivoSaidaDTO consultarListaPerfilTrocaArquivoSaida = null;
	
	/** Atributo contratoSelecionado. */
	private ListarContratosPgitSaidaDTO contratoSelecionado = null;
	
	/** Atributo dsLayoutProprio. */
	private String dsLayoutProprio = null;

	// DETALHAR / EXCLUIR / ALTERAR / INCLUIR
	// Perfil de Troca de Arquivos
	/** Atributo codigo. */
	private Long codigo = null;

	/** Atributo tipoLayoutArquivo. */
	private String tipoLayoutArquivo = null;

	/** Atributo cboTipoLayoutArquivo. */
	private Integer cboTipoLayoutArquivo = null;

	/** Atributo aplicativoFormatacao. */
	private String aplicativoFormatacao = null;
	
	/** Atributo utilizaLayoutProprio. */
	private String utilizaLayoutProprio = null;
	
	/** Atributo volumeMensalTraf. */
	private String volumeMensalTraf = null;
	
	/** Atributo juncaoRespCustoTrans. */
	private Integer juncaoRespCustoTrans = null;
	
	/** Atributo nomeContatoCliente. */
	private String nomeContatoCliente = null;
	
	/** Atributo dddContatoCliente. */
	private Integer dddContatoCliente = null;
	
	/** Atributo telefoneContatoCliente. */
	private String telefoneContatoCliente = null;
	
	/** Atributo ramalContatoCliente. */
	private String ramalContatoCliente = null;
	
	/** Atributo emailContatoCliente. */
	private String emailContatoCliente = null;
		
	/** Atributo nomeContatoSolicitante. */
	private String nomeContatoSolicitante = null;
	
	/** Atributo dddContatoSolicitante. */
	private Integer dddContatoSolicitante = null;
	
	/** Atributo telefoneContatoSolicitante. */
	private String telefoneContatoSolicitante = null;
	
	/** Atributo ramalContatoSolicitante. */
	private String ramalContatoSolicitante = null;
	
	/** Atributo emailContatoSolicitante. */
	private String emailContatoSolicitante = null;
	
	/** Atributo justificativaSolicitante. */
	private String justificativaSolicitante = null;
	
	/** Atributo listaContratosVinculados. */
	private List<ListaContratosVinculadosPerfilOcorrencias> listaContratosVinculados = null;
	

	/** Atributo cboAplicativoFormatacao. */
	private Integer cboAplicativoFormatacao = null;

	/** Atributo sistemaOrigemArquivoFiltro. */
	private String sistemaOrigemArquivoFiltro = null;

	/** Atributo sistemaOrigemArquivo. */
	private String sistemaOrigemArquivo = null;

	/** Atributo cboSistemaOrigemArquivo. */
	private String cboSistemaOrigemArquivo = null;

	/** Atributo empresaResponsavelTransmissao. */
	private String empresaResponsavelTransmissao = null;

	/** Atributo cboEmpresaResponsavelTransmissao. */
	private Long cboEmpresaResponsavelTransmissao = null;

	/** Atributo username. */
	private String username = null;

	/** Atributo nomeArquivoRemessa. */
	private String nomeArquivoRemessa = null;

	/** Atributo nomeArquivoRetorno. */
	private String nomeArquivoRetorno = null;

	/** Atributo tipoFiltroSelecionado. */
	private String tipoFiltroSelecionado = null;

	/** Atributo desabilitaRadio. */
	private boolean desabilitaRadio = false;

	/** Atributo rdoAplicFormProprio. */
	private String rdoAplicFormProprio = null;

	// Remessa
	/** Atributo tipoRejeicaoAcolhimento. */
	private String tipoRejeicaoAcolhimento = null;

	/** Atributo cboTipoRejeicaoAcolhimento. */
	private Integer cboTipoRejeicaoAcolhimento = null;

	/** Atributo quantidadeRegistrosInconsistentes. */
	private Integer quantidadeRegistrosInconsistentes = null;

	/** Atributo quantidadeRegistrosInconsistentesFormatada. */
	private String quantidadeRegistrosInconsistentesFormatada = null;

	/** Atributo percentualRegistrosInconsistentes. */
	private BigDecimal percentualRegistrosInconsistentes = null;

	/** Atributo tipoControleRemessa. */
	private String tipoControleRemessa = null;

	/** Atributo cboTipoControleRemessa. */
	private Integer cboTipoControleRemessa = null;

	/** Atributo nivelControleRemessa. */
	private String nivelControleRemessa = null;

	/** Atributo cboNivelControleRemessa. */
	private Integer cboNivelControleRemessa = null;

	/** Atributo periodicidadeInicializacaoContagemRemessa. */
	private String periodicidadeInicializacaoContagemRemessa = null;

	/** Atributo cboPeriodicidadeInicializacaoContagemRemessa. */
	private Integer cboPeriodicidadeInicializacaoContagemRemessa = null;

	/** Atributo numeroMaximoRemessa. */
	private Long numeroMaximoRemessa = null;
	
	/** Atributo strNumeroMaximoRemessa. */
	private String strNumeroMaximoRemessa = null;

	/** Atributo meioTransmissaoPrincipalRemessa. */
	private String meioTransmissaoPrincipalRemessa = null;

	/** Atributo cboMeioTransmissaoPrincipalRemessa. */
	private Integer cboMeioTransmissaoPrincipalRemessa = null;

	/** Atributo meioTransmissaoAlternativoRemessa. */
	private String meioTransmissaoAlternativoRemessa = null;

	/** Atributo cboMeioTransmissaoAlternativoRemessa. */
	private Integer cboMeioTransmissaoAlternativoRemessa = null;

	// Retorno
	/** Atributo nivelControleRetorno. */
	private String nivelControleRetorno = null;

	/** Atributo cboNivelControleRetorno. */
	private Integer cboNivelControleRetorno = null;

	/** Atributo tipoControleRetorno. */
	private String tipoControleRetorno = null;

	/** Atributo cboTipoControleRetorno. */
	private Integer cboTipoControleRetorno = null;

	/** Atributo periodicidadeInicializacaoContagemRetorno. */
	private String periodicidadeInicializacaoContagemRetorno = null;

	/** Atributo cboPeriodicidadeInicializacaoContagemRetorno. */
	private Integer cboPeriodicidadeInicializacaoContagemRetorno = null;

	/** Atributo numeroMaximoRetorno. */
	private Long numeroMaximoRetorno = null;
	
	/** Atributo strNumeroMaximoRetorno. */
	private String strNumeroMaximoRetorno = null;

	/** Atributo meioTransmissaoPrincipalRetorno. */
	private String meioTransmissaoPrincipalRetorno = null;

	/** Atributo cboMeioTransmissaoPrincipalRetorno. */
	private Integer cboMeioTransmissaoPrincipalRetorno = null;

	/** Atributo meioTransmissaoAlternativoRetorno. */
	private String meioTransmissaoAlternativoRetorno = null;

	/** Atributo cboMeioTransmissaoAlternativoRetorno. */
	private Integer cboMeioTransmissaoAlternativoRetorno = null;

	/** Atributo tipoAcao. */
	private String tipoAcao = null;

	//Dados de Controle
	/** Atributo rdoVan. */
	private String rdoVan = null;

	/** Atributo cboEmpresaRespTransDadosContr. */
	private Long cboEmpresaRespTransDadosContr = null;

	/** Atributo cboRespCustoTransArqDadosContr. */
	private Integer cboRespCustoTransArqDadosContr = null;

	/** Atributo txtPercentualCustoTransArq. */
	private BigDecimal txtPercentualCustoTransArq = null;

	/** Atributo listaResponsavelCustoTransArq. */
	private List<SelectItem> listaResponsavelCustoTransArq = new ArrayList<SelectItem>();

	/** Atributo listaResponsavelCustoTransArqHash. */
	private Map<Integer, String> listaResponsavelCustoTransArqHash = new HashMap<Integer, String>();

	/** Atributo responsavelCustoTransmissaoArquivo. */
	private String responsavelCustoTransmissaoArquivo = null;

	// Trilha Auditoria
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao = null;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao = null;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao = null;

	/** Atributo complementoInclusao. */
	private String complementoInclusao = null;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao = null;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao = null;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao = null;

	/** Atributo complementoManutencao. */
	private String complementoManutencao = null;

	/** Atributo cdSerieAplicTranmicao. */
	private String cdSerieAplicTranmicao = null;

	// HISTORICO
	/** Atributo codigoPerfilHist. */
	private Long codigoPerfilHist = null;

	/** Atributo tipoLayoutArquivoHist. */
	private Integer tipoLayoutArquivoHist = null;

	/** Atributo dataIncialManutencao. */
	private Date dataIncialManutencao = null;

	/** Atributo dataFimManutencao. */
	private Date dataFimManutencao = null;

	/** Atributo listaGridHistorico. */
	private List<ConsultarHistoricoPerfilTrocaArquivoSaidaDTO> listaGridHistorico = null;

	/** Atributo itemSelecionadoHistorico. */
	private Integer itemSelecionadoHistorico = null;

	/** Atributo listaControleHistorico. */
	private List<SelectItem> listaControleHistorico = null;

	/** Atributo disableArgumentosConsultaHistorico. */
	private boolean disableArgumentosConsultaHistorico = false;

	/** Atributo disableAplicativoFormatacao. */
	private boolean disableAplicativoFormatacao = false;
	
	/** Atributo disableTipoLayoutArquivo. */
	private boolean disableTipoLayoutArquivo = false;
	
	//Contrato filtro
	
	/** Atributo empresaGestoraFiltro. */
	private Long empresaGestoraFiltro = null;
	
	/** Atributo tipoContratoFiltro. */
	private Integer tipoContratoFiltro = null;
	
	/** Atributo numeroFiltro. */
	private String numeroFiltro = null;
	
	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato = null;
	
	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio = null;
	
	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio = null; 
	
	/** Atributo clienteContratoSelecionado. */
	private boolean clienteContratoSelecionado = false;
	
	/** Atributo listaEmpresaGestora. */
	private List<SelectItem> listaEmpresaGestora= new ArrayList<SelectItem>();
	
	/** Atributo listaEmpresaGestoraHash. */
	private Map<Long,String> listaEmpresaGestoraHash = new HashMap<Long,String>();
	
	/** Atributo listaTipoContrato. */
	private List<SelectItem> listaTipoContrato = new ArrayList<SelectItem>();
	
	//Desc Contrato
	
	/** Atributo empresaGestoraDescContrato. */
	private String empresaGestoraDescContrato = null;
	
	/** Atributo numeroDescContrato. */
	private String numeroDescContrato = null;
	
	/** Atributo descricaoTipoContratoDescContrato. */
	private String descricaoTipoContratoDescContrato = null;
	
	/** Atributo descricaoContratoDescContrato. */
	private String descricaoContratoDescContrato = null;
	
	/** Atributo situacaoDescContrato. */
	private String situacaoDescContrato = null;
	
	//pdc -> pgit.consultas.consultarContratoClienteFiltro
	/** Atributo listaContratoClientePerfil. */
	private List<ConsultarContratoClienteFiltroSaidaDTO> listaContratoClientePerfil = null;	
	
	/** Atributo itemSelecionadoListaContratoClientePerfil. */
	private Integer itemSelecionadoListaContratoClientePerfil = null;
	
	/** Atributo listaControleRadioContratoClientePerfil. */
	private List<SelectItem> listaControleRadioContratoClientePerfil = new ArrayList<SelectItem>();	
	
	/** Atributo listarPerfilTrocaArquivo. */
	private List<ListarPerfilTrocaArquivoSaidaDTO> listarPerfilTrocaArquivo = null;	
	
	/** Atributo itemSelecionadoListaPerfilTroca. */
	private Integer itemSelecionadoListaPerfilTroca = null;
	
	/** Atributo listaControleRadioPerfilTroca. */
	private List<SelectItem> listaControleRadioPerfilTroca = new ArrayList<SelectItem>();
	
	//Perfil  - Filtrar por contrato
	/** Atributo listaConsultarListaContratosPessoasPerfil. */
	private List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoasPerfil = null;
	
	/** Atributo itemClienteSelecionadoContratoPerfil. */
	private Integer itemClienteSelecionadoContratoPerfil = null;		
	
	/** Atributo listaControleRadioContratosPessoasPerfil. */
	private List<SelectItem> listaControleRadioContratosPessoasPerfil = new ArrayList<SelectItem>();	
	
	/* Perfil Cliente Pessoa */
	/** Atributo listaConsultarListaClientePessoasPerfil. */
	private List<ConsultarListaClientePessoasSaidaDTO> listaConsultarListaClientePessoasPerfil = null;
	
	/** Atributo itemSelecionadoListaClientePerfil. */
	private Integer itemSelecionadoListaClientePerfil = null;
	
	/** Atributo listaControleRadioClientePerfil. */
	private List<SelectItem> listaControleRadioClientePerfil = new ArrayList<SelectItem>();
	
	/* Atributo que controla se a p�gina "identificacaoClienteContratoAgendamento vai voltar para ela Principal ou
	 * Para tela de consulta de cliente (identificacaoClienteAgendamento)
	 */
	/** Atributo telaVoltarIdentificacaoClienteContrato. */
	private String telaVoltarIdentificacaoClienteContrato = null;
	
	/** Atributo telaVoltarIdentificacaoPerfil. */
	private String telaVoltarIdentificacaoPerfil = null;	

	//Atributos perfil
    /** Atributo cdPessoaPerfil. */
	private Long cdPessoaPerfil = null;
    
    /** Atributo cdCpfCnpjPessoaPerfil. */
    private String cdCpfCnpjPessoaPerfil = null;
    
    /** Atributo dsPessoaPerfil. */
    private String dsPessoaPerfil = null;
    
    /** Atributo cdPerfilTrocaArquivoPerfilCliente. */
    private Long cdPerfilTrocaArquivoPerfilCliente = null;    
    
    /** Atributo dsSituacaoPerfilCliente. */
    private String dsSituacaoPerfilCliente = null;
    
    /** Atributo cdPerfilTrocaArquivoPerfilContrato. */
    private Long cdPerfilTrocaArquivoPerfilContrato = null;
    
    /** Atributo dsSituacaoPerfilContrato. */
    private String dsSituacaoPerfilContrato = null;
	
	// Pagina retorno
    
	/** Atributo paginaRetorno. */
	private String paginaRetorno = null;

	//flag	
	/** Atributo flagMeioTransmissao. */
	private boolean flagMeioTransmissao = false;
	
	/** Atributo flagCustoTransmissao. */
	private boolean  flagCustoTransmissao = false;
	
	/** Atributo flagDadosControle. */
	private boolean  flagDadosControle = false;
	
	/** Atributo flagClienteSolicitante. */
	private boolean  flagClienteSolicitante = false;

	/** Atributo flagContratoVinc. */
	private boolean  flagContratoVinc = false;
	
	/** Atributo percentualCliente. */
	private BigDecimal percentualCliente = null;

	/** Atributo clienteFormatado. */
	private String clienteFormatado = null;

	/** Atributo bancoFormatado. */
	private String bancoFormatado = null;
	
	private List<ListarAssLayoutArqProdServicoSaidaDTO> listaGrid = new ArrayList<ListarAssLayoutArqProdServicoSaidaDTO>();
	
	/** Atributo listaGridPesquisa. */
	private List<IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO> listaGridTipoLayoutArquivo = null;
	
	private boolean checkAll;
	
	private boolean exibePanelGroupRepresentantes;
	
	private List<IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO> listaItensSelecionados = new ArrayList<IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO>(); 
	
	/** Atributo rdoCdIndicadorGeracaoSegmentoB. */
	private Integer rdoCdIndicadorGeracaoSegmentoB = null;
	
    /** Atributo dsSegmentoB. */
    private String dsSegmentoB = null;	
	
	/** Atributo cboCdIndicadorGeracaoSegmentoZ. */
	private Integer cboCdIndicadorGeracaoSegmentoZ = null;
	
    /** Atributo dsSegmentoZ. */
    private String dsSegmentoZ = null;	
	
	/** Atributo renderizaCamposGeracaoSegmentosBeZ */
	private boolean renderizaCamposGeracaoSegmentosBeZ = false;
	
	/** Atributo exibirCamposCNAB */
	private boolean exibirCamposCNAB = false;
	
	/** Atributo cdIndicadorCpfLayout */
	private Integer cdIndicadorCpfLayout = null;
	
	/** Atributo cdIndicadorAssociacaoLayout */
	private Integer cdIndicadorAssociacaoLayout = null;
	
	/** Atributo cdIndicadorContaComplementar */
	private Integer cdIndicadorContaComplementar = null;
	
	/** Atributo cdContaDebito */
	private Integer cdContaDebito = null;

	/** Atributo cdIndicadorGeracaoSegmentoB */
	private Integer cdIndicadorGeracaoSegmentoB = null;
	
	/** Atributo cdIndicadorGeracaoSegmentoZ */
	private Integer cdIndicadorGeracaoSegmentoZ = null;

	/** Atributo listaSegmentoZ */
	private List<SelectItem> listaSegmentoZ = null; 

	/** Atributo dsIndicadorCpfLayout */
	private String dsIndicadorCpfLayout = null;

	/** Atributo dsIndicadorContaComplementar */
	private String dsIndicadorContaComplementar = null;

	/** Atributo dsContaDebito */
	private String dsContaDebito = null;

	/** Atributo dsIndicadorAssociacaoLayout */
	private String dsIndicadorAssociacaoLayout = null;

	/** Atributo dsIndicadorGeracaoSegmentoB */
	private String dsIndicadorGeracaoSegmentoB = null;

	/** Atributo dsIndicadorGeracaoSegmentoZ */
	private String dsIndicadorGeracaoSegmentoZ = null;

	/** Atributo dsIndicadorConsisteContaDebito */
	private String dsIndicadorConsisteContaDebito =  null; 

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {

		// limpa tudo
		limpar();

		// carrega combo
		listarTipoLayoutArquivo();
		setExibePanelGroupRepresentantes(false);
		listarEmpresaGestora();
		listarTipoContrato();
		setClienteContratoSelecionado(false);
		setRdoCdIndicadorGeracaoSegmentoB(null);
		setCboCdIndicadorGeracaoSegmentoZ(null);
	}
	
	/**
	 * Listar tipo contrato.
	 */
	public void listarTipoContrato(){
		try{
			listaTipoContrato.clear();
			List<TipoContratoSaidaDTO> list = comboServiceImpl.listarTipoContrato();
	
			if(list.size() == 0 || list.size() > 1  ){
				listaEmpresaGestora.add(new SelectItem(0, MessageHelperUtils.getI18nMessage(
						"label_combo_selecione")));
			}
			for (TipoContratoSaidaDTO saida : list){
				listaTipoContrato.add(new SelectItem(saida.getCdTipoContrato(), saida.getDsTipoContrato()));
			}
		}catch (PdcAdapterFunctionalException p){
			listaTipoContrato = new ArrayList<SelectItem>();
		}		
	}	
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt) {
		consultar();
	}
	
	/**
	 * Pesquisar contratos vinculados.
	 *
	 * @param evt the evt
	 */
	public void pesquisarContratosVinculados(ActionEvent evt) {
		buscaContratosVinculados();
	}
	
	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora(){
		try {
			this.listaEmpresaGestora = new ArrayList<SelectItem>();
			List<EmpresaConglomeradoSaidaDTO> list = new ArrayList<EmpresaConglomeradoSaidaDTO>();
			
			EmpresaConglomeradoEntradaDTO entrada = new EmpresaConglomeradoEntradaDTO();
			entrada.setCdSituacao(0);
			entrada.setNrOcorrencias(180);
			
			list = comboServiceImpl.listarEmpresaConglomerado(entrada);
			listaEmpresaGestora.clear();
			
			if (list.size() == 0 || list.size() > 1){
				listaEmpresaGestora.add(new SelectItem(new Long(0), MessageHelperUtils.getI18nMessage(
						"label_combo_selecione")));
			}
			for(EmpresaConglomeradoSaidaDTO combo : list){
				listaEmpresaGestora.add(new SelectItem(combo.getCodClub(),combo.getDsRazaoSocial()));
				listaEmpresaGestoraHash.put(combo.getCodClub(), combo.getDsRazaoSocial());
			}			
		}catch (PdcAdapterFunctionalException p){
			listaEmpresaGestora = new ArrayList<SelectItem>();
			listaEmpresaGestoraHash = new HashMap<Long,String>();
		}
	}	
	
	/**
     * Consultar contrato.
     *
     * @return the string
     */
    public String consultarContrato(){
		manterContratoBean.iniciarPagina(null);
		manterContratoBean.getIdentificacaoClienteContratoBean().setPaginaRetorno("conContrato");
		return TELA_CONTRATO;
	} 
	

	/**
	 * Selecionar contrato.
	 *
	 * @return the string
	 */
	public String selecionarContrato(){
		Integer itemSelecionadoContrato = manterContratoBean.getIdentificacaoClienteContratoBean().
			getItemSelecionadoLista();
		contratoSelecionado = manterContratoBean.getIdentificacaoClienteContratoBean().getListaGridPesquisa().
			get(itemSelecionadoContrato);
		
		setCdPessoaJuridicaContrato(contratoSelecionado.getCdPessoaJuridica());
		setCdTipoContratoNegocio(contratoSelecionado.getCdTipoContrato());
		setNrSequenciaContratoNegocio(contratoSelecionado.getNrSequenciaContrato());
	    setEmpresaGestoraDescContrato(contratoSelecionado.getDsPessoaJuridica());
		setNumeroDescContrato(Long.toString(contratoSelecionado.getNrSequenciaContrato()));
		setDescricaoContratoDescContrato(contratoSelecionado.getDsContrato());
		setSituacaoDescContrato(contratoSelecionado.getDsSituacaoContrato());
		setClienteContratoSelecionado(true);

	    setCdPessoaPerfil(contratoSelecionado.getCdPessoaJuridica());
	    setCdCpfCnpjPessoaPerfil("");
	    setDsPessoaPerfil("");
		
 		return TELA_CONSULTAR;
	} 
	
	
	
	/**
	 * Setar atributos contrato perfil.
	 *
	 * @return the string
	 */
	public String setarAtributosContratoPerfil(){
		setCdPessoaJuridicaContrato(getListaConsultarListaContratosPessoasPerfil().get(
				getItemClienteSelecionadoContratoPerfil()).getCdPessoaJuridicaContrato());
		setCdTipoContratoNegocio(getListaConsultarListaContratosPessoasPerfil().get(
				getItemClienteSelecionadoContratoPerfil()).getCdTipoContratoNegocio());
		setNrSequenciaContratoNegocio(getListaConsultarListaContratosPessoasPerfil().get(
				getItemClienteSelecionadoContratoPerfil()).getNrSeqContratoNegocio());

	    setCdPerfilTrocaArquivoPerfilContrato(getListarPerfilTrocaArquivo().get(
	    		getItemSelecionadoListaPerfilTroca()).getCdPerfilTrocaArquivo());
	    setDsSituacaoPerfilContrato(getListarPerfilTrocaArquivo().get(
	    		getItemSelecionadoListaPerfilTroca()).getDsSituacaoPerfil());
	    setEmpresaGestoraDescContrato(getListaEmpresaGestoraHash().get(getEmpresaGestoraFiltro()));
		setNumeroDescContrato(Long.toString(getListaConsultarListaContratosPessoasPerfil().get(
				getItemClienteSelecionadoContratoPerfil()).getNrSeqContratoNegocio()));
		setDescricaoContratoDescContrato(getListaConsultarListaContratosPessoasPerfil().get(
				getItemClienteSelecionadoContratoPerfil()).getDsContrato());
		setSituacaoDescContrato(getListaConsultarListaContratosPessoasPerfil().get(
				getItemClienteSelecionadoContratoPerfil()).getDsSituacaoContratoNegocio());
		setClienteContratoSelecionado(true);

	    setCdPessoaPerfil(getListarPerfilTrocaArquivo().get(getItemSelecionadoListaPerfilTroca()).getCdPessoa());
	    setCdCpfCnpjPessoaPerfil("");
	    setDsPessoaPerfil("");
		
		return TELA_CONSULTAR;
	}
	
	/**
	 * Consultar.
	 */
	public void consultar() {
		limparInformacaoConsulta();
		try {
			ConsultarListaPerfilTrocaArquivoEntradaDTO entrada = new ConsultarListaPerfilTrocaArquivoEntradaDTO();
			entrada.setCdPerfilTrocaArquivo(getCodigoPerfilFiltro());
			entrada.setCdTipoLayoutArquivo(getTipoLayoutArquivoFiltro());
			entrada.setCdpessoaJuridicaContrato(getCdPessoaJuridicaContrato());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(getNrSequenciaContratoNegocio());

			consultarListaPerfilTrocaArquivoSaida = getManterPerfilTrocaArqLayoutServiceImpl().consultarListaPerfilTrocaArquivo(entrada);
			
			setListaGridConsultar(consultarListaPerfilTrocaArquivoSaida.getOcorrencia());
			setListaControleConsultar(new ArrayList<SelectItem>());
			for (int i = 0; i < getListaGridConsultar().size(); i++) {
				getListaControleConsultar().add(new SelectItem(i, ""));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		
	}
	
	/**
	 * Consultar exclusao.
	 *
	 * @return the string
	 */
	public String consultarExclusao() {
		limparInformacaoConsulta();
		try {
			ConsultarListaPerfilTrocaArquivoEntradaDTO entrada = new ConsultarListaPerfilTrocaArquivoEntradaDTO();
			entrada.setCdPerfilTrocaArquivo(getCodigoPerfilFiltro());
			entrada.setCdTipoLayoutArquivo(getTipoLayoutArquivoFiltro());
			entrada.setCdpessoaJuridicaContrato(getCdPessoaJuridicaContrato());
			entrada.setCdTipoContratoNegocio(getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(getNrSequenciaContratoNegocio());
			
			consultarListaPerfilTrocaArquivoSaida = 
				getManterPerfilTrocaArqLayoutServiceImpl().consultarListaPerfilTrocaArquivo(entrada);
			
			setListaGridConsultar(consultarListaPerfilTrocaArquivoSaida.getOcorrencia());
			setListaControleConsultar(new ArrayList<SelectItem>());
			for (int i = 0; i < getListaGridConsultar().size(); i++) {
				getListaControleConsultar().add(new SelectItem(i, ""));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
		}
		return "conManterPerfilTrocaArqLayout";
		
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		try {
			preencheDados();

			setBancoFormatado(formatarValorBanco());

			BigDecimal valorCliente = null;
			if ("CLIENTE".equalsIgnoreCase(getResponsavelCustoTransmissaoArquivo())) {
				valorCliente = new BigDecimal(100);
			} else {
				if (txtPercentualCustoTransArq != null) {
					valorCliente = new BigDecimal("100").subtract(txtPercentualCustoTransArq);
				}
			}
			
			setClienteFormatado(PgitUtil.formatNumberPercent((BigDecimal) PgitUtil.verificaZero(valorCliente)));

			return TELA_DETALHAR;
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		return "";
	}

	/**
	 * Formatar valor banco.
	 *
	 * @return the string
	 */
	private String formatarValorBanco() {
		BigDecimal valorBanco = null;
		if ("ORGANIZACAO BRADESCO".equalsIgnoreCase(getResponsavelCustoTransmissaoArquivo())) {
			valorBanco = new BigDecimal(100);
		} else {
			valorBanco = (BigDecimal) PgitUtil.verificaZero(txtPercentualCustoTransArq);
		}

		return PgitUtil.formatNumberPercent(valorBanco);
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		limparCampos();
		listarEmpresaResponsavelTransmissao();
		listarTipoRejeicaoAcolhimento();
		listarNivelControle();
		listarNivelControleRetorno();
		listarPeriodicidadeInicializacaoContagem();
		listarMeioTransmissao();
		setDesabilitaRadio(true);
		listarResponsavelCustoTransmissaorquivo();
		setTipoFiltroSelecionado(null);
		setFlagMeioTransmissao(true);
	    setFlagDadosControle(true);
	    setFlagClienteSolicitante(true);
	    setExibePanelGroupRepresentantes(false);
	    popularListaSegmentoZ();
		return TELA_INCLUIR;
	}

	/**
	 * Limpar percentual dados controle.
	 */
	public void limparPercentualDadosControle() {
		if(cboRespCustoTransArqDadosContr != 1){
			setJuncaoRespCustoTrans(null);
		}
		if(cboRespCustoTransArqDadosContr!= 3 ){
			setTxtPercentualCustoTransArq(null);
			setJuncaoRespCustoTrans(null);
		}
	}

	/**
	 * Preenche campo.
	 */
	public void carregaCampo(){
		for (int i = 0; i < getListaGridTipoLayoutArquivo().size(); i++) {
			IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO itemSelecionado = getListaGridTipoLayoutArquivo().get(i);
			if (itemSelecionado.getCdIndicadorLayoutProprio() != null && 
					itemSelecionado.getCdIndicadorLayoutProprio() == 1 && itemSelecionado.isSelecionado()) {
				itemSelecionado.setCdApliFormat(0);
			}
		}
	}
	
	
	/**
	 * Calcular percentual cliente.
	 */
	public void calcularPercentualCliente() {
		if (txtPercentualCustoTransArq != null) {
			BigDecimal percentualDiferenca = new BigDecimal("100").subtract(txtPercentualCustoTransArq);

			setPercentualCliente(percentualDiferenca);
		} else {
			setPercentualCliente(null);
		}
	}

	/**
	 * Atribuir percentual banco cliente.
	 */
	public void atribuirPercentualBancoCliente() {
		if (!Integer.valueOf(0).equals(cboRespCustoTransArqDadosContr) && 
				!Integer.valueOf(3).equals(cboRespCustoTransArqDadosContr)) {
			BigDecimal percentualCem = new BigDecimal("100.00"), percentualZero = new BigDecimal("0.00");

			if (Integer.valueOf(1).equals(cboRespCustoTransArqDadosContr)) {
				setTxtPercentualCustoTransArq(percentualCem);
				setPercentualCliente(percentualZero);
			} else if (Integer.valueOf(2).equals(cboRespCustoTransArqDadosContr)) {
				setTxtPercentualCustoTransArq(percentualZero);
				setPercentualCliente(percentualCem);
			}
		} else {
			setTxtPercentualCustoTransArq(null);
			setPercentualCliente(null);
		}
	}

	/**
	 * Habilitar radios.
	 *
	 * @return the string
	 */
	public String habilitarRadios() {
		setTipoFiltroSelecionado(null);
		setQuantidadeRegistrosInconsistentes(null);
		setPercentualRegistrosInconsistentes(null);

		if (getCboTipoRejeicaoAcolhimento() == 3) {
			setDesabilitaRadio(false);
		} else {
			setDesabilitaRadio(true);
		}
		return "";
	}

	/**
	 * Limpar radios.
	 *
	 * @return the string
	 */
	public String limparRadios() {
		setQuantidadeRegistrosInconsistentes(null);
		setPercentualRegistrosInconsistentes(null);
		return "";
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		// Perfil de Troca de Arquivos
		setTipoLayoutArquivo(getListaTipoLayoutArquivoHash().get(getCboTipoLayoutArquivo()));
		setAplicativoFormatacao(getListaAplicativoFormatacaoHash().get(getCboAplicativoFormatacao()));

		// Remessa
		if (getQuantidadeRegistrosInconsistentes() != null) {
			setQuantidadeRegistrosInconsistentesFormatada(PgitUtil.formatarNumero(
							getQuantidadeRegistrosInconsistentes(), true));
		} else {
			setQuantidadeRegistrosInconsistentesFormatada("");
		}
		setTipoRejeicaoAcolhimento(getListaTipoRejeicaoAcolhimentoHash().get(
				getCboTipoRejeicaoAcolhimento()));
		setNivelControleRemessa(getListaNivelControleHash().get(getCboNivelControleRemessa()));
		setTipoControleRemessa(getListaTipoControleHash().get(getCboTipoControleRemessa()));
		setPeriodicidadeInicializacaoContagemRemessa(getListaPeriodicidadeInicializacaoContagemHash().get(
						getCboPeriodicidadeInicializacaoContagemRemessa()));
		setMeioTransmissaoPrincipalRemessa(getListaMeioTransmissaoHash().get(
				getCboMeioTransmissaoPrincipalRemessa()));
		setMeioTransmissaoAlternativoRemessa(getListaMeioTransmissaoHash().get(
						getCboMeioTransmissaoAlternativoRemessa()));

		// Retorno
		setNivelControleRetorno(getListaNivelControleHash().get(getCboNivelControleRetorno()));
		setTipoControleRetorno(getListaTipoControleHash().get(getCboTipoControleRetorno()));
		setPeriodicidadeInicializacaoContagemRetorno(getListaPeriodicidadeInicializacaoContagemHash().get(
						getCboPeriodicidadeInicializacaoContagemRetorno()));
		setMeioTransmissaoPrincipalRetorno(getListaMeioTransmissaoHash().get(
				getCboMeioTransmissaoPrincipalRetorno()));
		setMeioTransmissaoAlternativoRetorno(getListaMeioTransmissaoHash().get(
						getCboMeioTransmissaoAlternativoRetorno()));

		//Dados de Controle
		setSistemaOrigemArquivo(getListaSistemaOrigemArquivoHash().get(getCboSistemaOrigemArquivo()));
		setEmpresaResponsavelTransmissao(getListaEmpresaResponsavelTransmissaoHash().get(
						getCboEmpresaRespTransDadosContr()));
		setResponsavelCustoTransmissaoArquivo(getListaResponsavelCustoTransArqHash().get(
				getCboRespCustoTransArqDadosContr()));

		formatarDadoCliente();
		setTxtPercentualCustoTransArq((BigDecimal) PgitUtil.verificaZero(txtPercentualCustoTransArq));
		
		listaItensSelecionados = new ArrayList<IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO>();
				
		for (IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO item : getListaGridTipoLayoutArquivo()) {
			if(item.isSelecionado()){
				
				String indicadorLayoutProprio = ""; 
				
						
				for (int i = 0; i < getListaTipoLayoutArquivo().size(); i++) {
					if(getListaAplicativoFormatArquivo() != null && i < getListaAplicativoFormatArquivo().size() &&
							getListaAplicativoFormatArquivo().get(i).getValue()
							.equals(item.getCdApliFormat())){
						indicadorLayoutProprio = getListaAplicativoFormatArquivo().get(i).getLabel();
					}
				}
				
				IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO entrada = 
					new IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO(
						item.getCdProdutoOperacaoRelacionado(), 
						item.getCdIndicadorLayoutProprio(), 
						item.getCdApliFormat(), item.isSelecionado(),
						item.getDsProdutoOperacaoRelacionado(), indicadorLayoutProprio);
				listaItensSelecionados.add(entrada);
			}
		}

		if(getCboTipoLayoutArquivo() != null){
			if(getCboTipoLayoutArquivo().equals(14) || getCboTipoLayoutArquivo().equals(15) || getCboTipoLayoutArquivo().equals(16)){
				setExibirCamposCNAB(true);
			}else{
				setExibirCamposCNAB(false);
			}
		}
		
		return TELA_INCLUIR_AVANCAR;
	}

	/**
	 * Formatar dado cliente.
	 */
	private void formatarDadoCliente() {
		String clienteFormatado = "";

		if ("CLIENTE".equalsIgnoreCase(getResponsavelCustoTransmissaoArquivo())) {
			clienteFormatado = PgitUtil.formatNumberPercent(new BigDecimal(100));
		} else if (txtPercentualCustoTransArq != null) {
			BigDecimal valorCliente = new BigDecimal("100").subtract(txtPercentualCustoTransArq);
			clienteFormatado = PgitUtil.formatNumberPercent((BigDecimal) PgitUtil.verificaZero(valorCliente));
		}

		setClienteFormatado(clienteFormatado);
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {
		try {
			IncluirPerfilTrocaArquivoEntradaDTO entrada = new IncluirPerfilTrocaArquivoEntradaDTO();
			entrada.setCdPerfilTrocaArquivo(0L);
			entrada.setCdSerieAplicacaoTransmissao("1");
			entrada.setCdTipoLayoutArquivo(getCboTipoLayoutArquivo());
			entrada.setCdSistemaOrigemArquivo(getCboSistemaOrigemArquivo());
			entrada.setDsUserNameCliente(getUsername());
			entrada.setDsNomeArquivoRemessa(getNomeArquivoRemessa());
			entrada.setDsNameArquivoRetorno(getNomeArquivoRetorno());
			entrada.setCdRejeicaoAcltoRemessa(getCboTipoRejeicaoAcolhimento());
			entrada.setQtMaxIncotRemessa(getQuantidadeRegistrosInconsistentes());
			entrada.setPercentualInconsistenciaRejeicaoRemessa(getPercentualRegistrosInconsistentes());
			entrada.setCdNivelControleRemessa(getCboNivelControleRemessa());
			entrada.setCdControleNumeroRemessa(getCboTipoControleRemessa());
			entrada.setCdPeriodicidadeContagemRemessa(getCboPeriodicidadeInicializacaoContagemRemessa());
			entrada.setNrMaximoContagemRemessa(getNumeroMaximoRemessa());
			entrada.setCdMeioPrincipalRemessa(getCboMeioTransmissaoPrincipalRemessa());
			entrada.setCdMeioAlternativoRemessa(getCboMeioTransmissaoAlternativoRemessa());
			entrada.setCdNivelControleRetorno(getCboNivelControleRetorno());
			entrada.setCdControleNumeroRetorno(getCboTipoControleRetorno());
			entrada.setCdPerdcContagemRetorno(getCboPeriodicidadeInicializacaoContagemRetorno());
			entrada.setNrMaximoContagemRetorno(getNumeroMaximoRetorno());
			entrada.setCdMeioPrincipalRetorno(getCboMeioTransmissaoPrincipalRetorno());
			entrada.setCdMeioAlternativoRetorno(getCboMeioTransmissaoAlternativoRetorno());
			
			entrada.setCdSistemaOrigemArquivo(getCboSistemaOrigemArquivo());
			entrada.setPercentualInconsistenciaRejeicaoRemessa(getPercentualRegistrosInconsistentes());
		

			//Dados controle
			entrada.setCdPessoaJuridicaParceiro(getCboEmpresaRespTransDadosContr());
			entrada.setCdPessoaJuridica(getCdPessoaJuridicaContrato());
			entrada.setCdEmpresaResponsavel(getCboRespCustoTransArqDadosContr());
			entrada.setCdCustoTransmicao(getTxtPercentualCustoTransArq());
			
			entrada.setCdUnidadeOrganizacional(getJuncaoRespCustoTrans());
			entrada.setQtMesRegistroTrafg(NumberUtils.convertVolume(getVolumeMensalTraf()));

			//Dados de Contato do Cliente
			entrada.setDsNomeContatoCliente(getNomeContatoCliente());
			entrada.setCdAreaFone(getDddContatoCliente());
			entrada.setCdFoneContatoCliente(NumberUtils.convertTelefoneSemDdd(getTelefoneContatoCliente()));
			entrada.setCdRamalContatoCliente(getRamalContatoCliente());
			entrada.setDsEmailContatoCliente(getEmailContatoCliente());
			
			//Dados de Contato do solicitante
			entrada.setDsSolicitacaoPendente(getNomeContatoSolicitante());
			entrada.setCdAreaFonePend(getDddContatoSolicitante());
			entrada.setCdFoneSolicitacaoPend(NumberUtils.convertTelefoneSemDdd(getTelefoneContatoSolicitante()));
			entrada.setCdRamalSolctPend(getRamalContatoSolicitante());
			entrada.setDsEmailSolctPend(getEmailContatoSolicitante());
			entrada.setDsObsGeralPerfil(getJustificativaSolicitante());
			
			entrada.setOcorrencias(getListaItensSelecionados());
			
			entrada.setMaximaOcorrencias(getListaItensSelecionados().size());
			
			entrada.setCdIndicadorCpfLayout(getCdIndicadorCpfLayout());
			entrada.setCdIndicadorContaComplementar(getCdIndicadorContaComplementar());
			entrada.setCdIndicadorConsisteContaDebito(getCdContaDebito());
			entrada.setCdIndicadorAssociacaoLayout(getCdIndicadorAssociacaoLayout());
			entrada.setCdIndicadorGeracaoSegmentoB(getCdIndicadorGeracaoSegmentoB());
			entrada.setCdIndicadorGeracaoSegmentoZ(getCdIndicadorGeracaoSegmentoZ());
			
			//entrada.setCdApliFormat(new Long(getCboAplicativoFormatacao()));


			IncluirPerfilTrocaArquivoSaidaDTO saida = getManterPerfilTrocaArqLayoutServiceImpl()
							.incluirPerfilTrocaArquivo(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
							"conManterPerfilTrocaArqLayout", BradescoViewExceptionActionType.ACTION, false);
			
			
			limparCampos();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		return "";
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		try {
			limparCampos();
			listarAplicativoFormatacao();
			listarTipoLayoutArquivo();
			listarEmpresaResponsavelTransmissao();
			listarTipoRejeicaoAcolhimento();
			listarNivelControle();
			listarTipoControle();
			listarResponsavelCustoTransmissaorquivo();
			listarPeriodicidadeInicializacaoContagem();
			listarMeioTransmissao();
			listarTipoControleRetorno();
			listarTipoControleIncluir();
			
			preencheDados();
			habilitarFiltroMeioTransmissaoAlterar();

			if (getSistemaOrigemArquivoFiltro() != null && !"".equals(getSistemaOrigemArquivoFiltro())) {
				obterSistemaOrigemArquivo();
				setCboSistemaOrigemArquivo(getSistemaOrigemArquivoFiltro());
			}

			BigDecimal percentDiferenca = null;
			if ("CLIENTE".equalsIgnoreCase(getResponsavelCustoTransmissaoArquivo())) {
				percentDiferenca = new BigDecimal("100");
			} else if(txtPercentualCustoTransArq != null) {
				percentDiferenca = new BigDecimal("100").subtract(txtPercentualCustoTransArq);
			}

			setPercentualCliente(percentDiferenca);
			if("sim".equalsIgnoreCase(listaGridConsultar.get(itemSelecionadoConsultar).getDsLayoutProprio())){
				setRdoAplicFormProprio("1");
			}
			else{
				setRdoAplicFormProprio("2");
			}
			
			Integer tipoLayoutArquivoSelecionado = listaGridConsultar.get(itemSelecionadoConsultar).getCdTipoLayoutArquivo();
			if(tipoLayoutArquivoSelecionado != null){
				if(tipoLayoutArquivoSelecionado.equals(14) || tipoLayoutArquivoSelecionado.equals(15) || tipoLayoutArquivoSelecionado.equals(16)){
					setExibirCamposCNAB(true);
				}else{
					setExibirCamposCNAB(false);
				}
			}
			
			popularListaSegmentoZ();
			
			
			return TELA_ALTERAR;
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		return "";
	}

	/**
	 * 
	 */
	private void popularListaSegmentoZ() {
		setListaSegmentoZ(new ArrayList<SelectItem>());

		getListaSegmentoZ().add(new SelectItem(1, "TODOS OS SERVI�OS"));
		getListaSegmentoZ().add(new SelectItem(2, "TRIBUTO"));
		getListaSegmentoZ().add(new SelectItem(3, "SAL�RIO"));
		getListaSegmentoZ().add(new SelectItem(4, "FORNECEDOR"));
		getListaSegmentoZ().add(new SelectItem(5, "N�O"));
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {
		// Perfil de Troca de Arquivos
		setAplicativoFormatacao(getListaAplicativoFormatacaoHash().get(getCboAplicativoFormatacao()));

		// Remessa
		if (getQuantidadeRegistrosInconsistentes() != null) {
			setQuantidadeRegistrosInconsistentesFormatada(PgitUtil.formatarNumero(
							getQuantidadeRegistrosInconsistentes(), true));
		} else {
			setQuantidadeRegistrosInconsistentesFormatada("");
		}

		setTipoRejeicaoAcolhimento(getListaTipoRejeicaoAcolhimentoHash().get(getCboTipoRejeicaoAcolhimento()));
		setNivelControleRemessa(getListaNivelControleHash().get(getCboNivelControleRemessa()));
		setTipoControleRemessa(getListaTipoControleHash().get(getCboTipoControleRemessa()));
		setPeriodicidadeInicializacaoContagemRemessa(getListaPeriodicidadeInicializacaoContagemHash().get(
						getCboPeriodicidadeInicializacaoContagemRemessa()));
		setMeioTransmissaoPrincipalRemessa(getListaMeioTransmissaoHash().get(
				getCboMeioTransmissaoPrincipalRemessa()));
		setMeioTransmissaoAlternativoRemessa(getListaMeioTransmissaoHash().get(
						getCboMeioTransmissaoAlternativoRemessa()));

		// Retorno
		setNivelControleRetorno(getListaNivelControleHash().get(getCboNivelControleRetorno()));
		setTipoControleRetorno(getListaTipoControleRetornoHash().get(getCboTipoControleRetorno()));
		setPeriodicidadeInicializacaoContagemRetorno(getListaPeriodicidadeInicializacaoContagemHash().get(
						getCboPeriodicidadeInicializacaoContagemRetorno()));
		setMeioTransmissaoPrincipalRetorno(getListaMeioTransmissaoHash().get(
				getCboMeioTransmissaoPrincipalRetorno()));
		setMeioTransmissaoAlternativoRetorno(getListaMeioTransmissaoHash().get(
						getCboMeioTransmissaoAlternativoRetorno()));

		//Dados de Controle
		setSistemaOrigemArquivo(getListaSistemaOrigemArquivoHash().get(getCboSistemaOrigemArquivo()));
		setEmpresaResponsavelTransmissao(getListaEmpresaResponsavelTransmissaoHash().get(
						getCboEmpresaRespTransDadosContr()));
		setResponsavelCustoTransmissaoArquivo(getListaResponsavelCustoTransArqHash().get(
						getCboRespCustoTransArqDadosContr()));
		
		buscaContratosVinculados();

		setBancoFormatado(PgitUtil.formatNumberPercent(txtPercentualCustoTransArq));
		formatarDadoCliente();

		return TELA_ALTERAR_AVANCAR;
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {
		try {
			ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO registroSelecionado = 
				getListaGridConsultar().get(getItemSelecionadoConsultar());

			AlterarPerfilTrocaArquivoEntradaDTO entrada = new AlterarPerfilTrocaArquivoEntradaDTO();
			entrada.setCdPerfilTrocaArquivo(registroSelecionado.getCdPerfilTrocaArquivo());
			entrada.setCdSerieAplicacaoTransmissao(getCdSerieAplicTranmicao());
			entrada.setCdTipoLayoutArquivo(registroSelecionado.getCdTipoLayoutArquivo());
			entrada.setCdSistemaOrigemArquivo(getCboSistemaOrigemArquivo());
			entrada.setDsUserNameCliente(getUsername());
			entrada.setDsNomeArquivoRemessa(getNomeArquivoRemessa());
			entrada.setDsNameArquivoRetorno(getNomeArquivoRetorno());
			entrada.setCdRejeicaoAcltoRemessa(getCboTipoRejeicaoAcolhimento());
			entrada.setQtMaxIncotRemessa(getQuantidadeRegistrosInconsistentes());
			entrada.setPercentualInconsistenciaRejeicaoRemessa(getPercentualRegistrosInconsistentes());
			entrada.setCdNivelControleRemessa(getCboNivelControleRemessa());
			entrada.setCdControleNumeroRemessa(getCboTipoControleRemessa());
			entrada.setCdPeriodicidadeContagemRemessa(getCboPeriodicidadeInicializacaoContagemRemessa());
			entrada.setNrMaximoContagemRemessa(getNumeroMaximoRemessa());
			entrada.setCdMeioPrincipalRemessa(getCboMeioTransmissaoPrincipalRemessa());
			entrada.setCdMeioAlternativoRemessa(getCboMeioTransmissaoAlternativoRemessa());
			entrada.setCdNivelControleRetorno(getCboNivelControleRetorno());
			entrada.setCdControleNumeroRetorno(getCboTipoControleRetorno());
			entrada.setCdPerdcContagemRetorno(getCboPeriodicidadeInicializacaoContagemRetorno());
			entrada.setNrMaximoContagemRetorno(getNumeroMaximoRetorno());
			entrada.setCdMeioPrincipalRetorno(getCboMeioTransmissaoPrincipalRetorno());
			entrada.setCdMeioAlternativoRetorno(getCboMeioTransmissaoAlternativoRetorno());
			entrada.setCdEmpresaResponsavelTransporte(getCboEmpresaResponsavelTransmissao());
			entrada.setCdApliFormat(new Long(PgitUtil.verificaIntegerNulo(getCboAplicativoFormatacao())));

			//Dados Controle
			entrada.setCdPessoaJuridicaParceiro(getCboEmpresaRespTransDadosContr());
			entrada.setCdEmpresaResponsavel(PgitUtil.verificaIntegerNulo(getCboRespCustoTransArqDadosContr()));
			entrada.setCdCustoTrasmicao(PgitUtil.verificaBigDecimalNulo(getTxtPercentualCustoTransArq()));
			
			//Novos Campos - Solicita��o 900007480
			entrada.setQtMesRegistroTrafg(NumberUtils.convertVolume(getVolumeMensalTraf()));
			entrada.setCdUnidadeOrganizacional(getJuncaoRespCustoTrans());			
			entrada.setDsNomeContatoCliente(getNomeContatoCliente());
			entrada.setCdAreaFone(getDddContatoCliente());
			entrada.setCdFoneContatoCliente(NumberUtils.convertTelefoneSemDdd(getTelefoneContatoCliente()));
			entrada.setCdRamalContatoCliente(getRamalContatoCliente());
			entrada.setDsEmailContatoCliente(getEmailContatoCliente());			
			entrada.setDsSolicitacaoPendente(getNomeContatoSolicitante());
			entrada.setCdAreaFonePend(getDddContatoSolicitante());
			entrada.setCdFoneSolicitacaoPend(NumberUtils.convertTelefoneSemDdd(getTelefoneContatoSolicitante()));
			entrada.setCdRamalSolctPend(getRamalContatoSolicitante());
			entrada.setDsEmailSolctPend(getEmailContatoSolicitante());
			entrada.setDsObsGeralPerfil(getJustificativaSolicitante());
			
			entrada.setCdIndicadorCpfLayout(getCdIndicadorCpfLayout());
			entrada.setCdIndicadorContaComplementar(getCdIndicadorContaComplementar());
			entrada.setCdContaDebito(getCdContaDebito());
			entrada.setCdIndicadorAssociacaoLayout(getCdIndicadorAssociacaoLayout());
			entrada.setCdIndicadorGeracaoSegmentoB(getCdIndicadorGeracaoSegmentoB());
			entrada.setCdIndicadorGeracaoSegmentoZ(getCdIndicadorGeracaoSegmentoZ());

			AlterarPerfilTrocaArquivoSaidaDTO saida = getManterPerfilTrocaArqLayoutServiceImpl()
							.alterarPerfilTrocaArquivo(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
					 "#{manterPerfilTrocaArqLayoutBean.consultarExclusao}", "conManterPerfilTrocaArqLayout", false);

			
			limparCampos();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		return "";
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		try {
			preencheDados();
			return TELA_EXCLUIR;
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		return "";
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {
		try {
			ExcluirPerfilTrocaArquivoEntradaDTO entrada = new ExcluirPerfilTrocaArquivoEntradaDTO();
			
			ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO item = getListaGridConsultar().get(getItemSelecionadoConsultar());
			
			entrada.setCdPerfilTrocaArquivo(item.getCdPerfilTrocaArquivo());
			entrada.setCdTipoLayoutArquivo(item.getCdTipoLayoutArquivo());
			entrada.setCdProdutoServicoOperacao(item.getCdProdutoServicoOperacao());
			entrada.setCdProdutoOperacaoRelacionado(item.getCdProdutoOperacaoRelacionado());
			entrada.setCdRelacionamentoProduto(item.getCdRelacionamentoProduto());

			ExcluirPerfilTrocaArquivoSaidaDTO saida = getManterPerfilTrocaArqLayoutServiceImpl()
							.excluirPerfilTrocaArquivo(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
							 "#{manterPerfilTrocaArqLayoutBean.consultarExclusao}", "conManterPerfilTrocaArqLayout", false);
			limparCampos();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		return "";
	}
	
	// LIMPAR
	/**
	 * Limpar.
	 */
	public void limpar() {
		setCodigoPerfilFiltro(null);
		setTipoLayoutArquivoFiltro(null);
		setCdPessoaJuridicaContrato(null);
		setCdTipoContratoNegocio(null);
		setNrSequenciaContratoNegocio(null);
		consultarListaPerfilTrocaArquivoSaida = new ConsultarListaPerfilTrocaArquivoSaidaDTO();
		setListaGridConsultar(null);
		setListaControleConsultar(null);
		limparInformacaoConsulta();
		limparContrato();
		
		
	}

	/**
	 * Limpar informacao consulta.
	 *
	 * @return the string
	 */
	public String limparInformacaoConsulta() {
		setListaGridConsultar(new ArrayList<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO>());
		setListaControleConsultar(new ArrayList<SelectItem>());
		setItemSelecionadoConsultar(null);
		setDisableArgumentosConsulta(false);
		
		return "";
	}

	/**
	 * Limpar campos.
	 */
	private void limparCampos() {
		// Perfil de Troca de Arquivos
		setCodigo(null);
		setDisableAplicativoFormatacao(true);
		setDisableTipoLayoutArquivo(true);
		setTipoLayoutArquivo(null);
		setCboTipoLayoutArquivo(null);
		setAplicativoFormatacao(null);
		setCboAplicativoFormatacao(null);
		setSistemaOrigemArquivoFiltro(null);
		setSistemaOrigemArquivo(null);
		setCboSistemaOrigemArquivo(null);
		setListaSistemaOrigemArquivo(new ArrayList<SelectItem>());
		setEmpresaResponsavelTransmissao(null);
		setCboEmpresaResponsavelTransmissao(null);
		setUsername(null);
		setNomeArquivoRemessa(null);
		setNomeArquivoRetorno(null);
		setRdoCdIndicadorGeracaoSegmentoB(null);
		setCboCdIndicadorGeracaoSegmentoZ(null);
		setRenderizaCamposGeracaoSegmentosBeZ(false);
		
		// Remessa
		setTipoFiltroSelecionado("");
		setTipoRejeicaoAcolhimento(null);
		setCboTipoRejeicaoAcolhimento(null);
		setQuantidadeRegistrosInconsistentes(null);
		setQuantidadeRegistrosInconsistentesFormatada(null);
		setPercentualRegistrosInconsistentes(null);
		setTipoControleRemessa(null);
		setCboTipoControleRemessa(null);
		setNivelControleRemessa(null);
		setCboNivelControleRemessa(null);
		setPeriodicidadeInicializacaoContagemRemessa(null);
		setCboPeriodicidadeInicializacaoContagemRemessa(null);
		setNumeroMaximoRemessa(null);
		setMeioTransmissaoPrincipalRemessa(null);
		setCboMeioTransmissaoPrincipalRemessa(null);
		setMeioTransmissaoAlternativoRemessa(null);
		setCboMeioTransmissaoAlternativoRemessa(null);
		setCdIndicadorCpfLayout(null);
		setDsIndicadorCpfLayout(null);
		setCdIndicadorContaComplementar(null);
		setDsIndicadorContaComplementar(null);
		setDsIndicadorConsisteContaDebito(null);
		setCdContaDebito(null);
		setDsContaDebito(null);

		// Retorno
		setNivelControleRetorno(null);
		setCboNivelControleRetorno(null);
		setTipoControleRetorno(null);
		setCboTipoControleRetorno(null);
		setPeriodicidadeInicializacaoContagemRetorno(null);
		setCboPeriodicidadeInicializacaoContagemRetorno(null);
		setNumeroMaximoRetorno(null);
		setMeioTransmissaoPrincipalRetorno(null);
		setCboMeioTransmissaoPrincipalRetorno(null);
		setMeioTransmissaoAlternativoRetorno(null);
		setCboMeioTransmissaoAlternativoRetorno(null);
		setCdIndicadorAssociacaoLayout(null);
		setDsIndicadorAssociacaoLayout(null);
		setCdIndicadorGeracaoSegmentoB(null);
		setDsIndicadorGeracaoSegmentoB(null);
		setCdIndicadorGeracaoSegmentoZ(null);
		setDsIndicadorGeracaoSegmentoZ(null);

		setCdSerieAplicTranmicao(null);

		//Dados de Controle
		setCboEmpresaRespTransDadosContr(null);
		setCboRespCustoTransArqDadosContr(null);
		setRdoAplicFormProprio(null);
		setRdoVan(null);
		setTxtPercentualCustoTransArq(null);
		setPercentualCliente(null);
		
		setRdoVan("");
		setEmpresaResponsavelTransmissao("");
		setResponsavelCustoTransmissaoArquivo("");
		setTxtPercentualCustoTransArq(null);
		setVolumeMensalTraf(null);
		setJuncaoRespCustoTrans(null);
		setNomeArquivoRemessa("");
		setNomeArquivoRetorno("");
		setUsername("");
		
		setNomeContatoCliente("");
		setDddContatoCliente(null);
		setTelefoneContatoCliente(null);
		setRamalContatoCliente("");
		setEmailContatoCliente("");
		
		setNomeContatoSolicitante("");
		setDddContatoSolicitante(null);
		setTelefoneContatoSolicitante(null);
		setRamalContatoSolicitante("");
		setEmailContatoSolicitante("");
		setJustificativaSolicitante("");
		
		setListaItensSelecionados(new ArrayList<IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO>());
		setListaGridTipoLayoutArquivo(new ArrayList<IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO>());

		setExibirCamposCNAB(false);
	}
	
	/**
	 * Limpar contrato.
	 */
	public void limparContrato(){
		setEmpresaGestoraFiltro(new Long(0));
		setTipoContratoFiltro(null);
		setNumeroFiltro("");
		setEmpresaGestoraDescContrato("");
		setNumeroDescContrato("");
		setDescricaoContratoDescContrato("");
		setSituacaoDescContrato("");
		
		getManterContratoBean().getIdentificacaoClienteContratoBean().limparDadosCliente();
		getManterContratoBean().getIdentificacaoClienteContratoBean().limparDadosPesquisaContrato();
		contratoSelecionado =  new ListarContratosPgitSaidaDTO();

	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados() {
		DetalharPerfilTrocaArquivoEntradaDTO entrada = new DetalharPerfilTrocaArquivoEntradaDTO();
		entrada.setCdPerfilTrocaArquivo(getListaGridConsultar().get(getItemSelecionadoConsultar())
						.getCdPerfilTrocaArquivo());
		entrada.setCdTipoLayoutArquivo(getListaGridConsultar().get(getItemSelecionadoConsultar())
						.getCdTipoLayoutArquivo());
		entrada.setCdProdutoServicoOperacao(getListaGridConsultar().get(getItemSelecionadoConsultar())
				.getCdProdutoServicoOperacao());
		entrada.setCdProdutoOperacaoRelacionado(getListaGridConsultar().get(getItemSelecionadoConsultar())
				.getCdProdutoOperacaoRelacionado());
		entrada.setCdRelacionamentoProduto(getListaGridConsultar().get(getItemSelecionadoConsultar())
				.getCdRelacionamentoProduto());

		DetalharPerfilTrocaArquivoSaidaDTO saida = getManterPerfilTrocaArqLayoutServiceImpl()
						.detalharPerfilTrocaArquivo(entrada);

		Integer tipoLayoutArquivoSelecionado = getListaGridConsultar().get(getItemSelecionadoConsultar()).getCdTipoLayoutArquivo();
		if(tipoLayoutArquivoSelecionado != null){
			if(tipoLayoutArquivoSelecionado.equals(14) || tipoLayoutArquivoSelecionado.equals(15) || tipoLayoutArquivoSelecionado.equals(16)){
				setExibirCamposCNAB(true);
			}else{
				setExibirCamposCNAB(false);
			}
		}
		
		setDsIndicadorCpfLayout(saida.getDsIndicadorCpfLayout());
		setDsIndicadorContaComplementar(saida.getDsIndicadorContaComplementar());
		setDsIndicadorConsisteContaDebito(saida.getDsIndicadorConsisteContaDebito());
		setCdContaDebito(saida.getCdIndicadorConsisteContaDebito());
		setCdIndicadorContaComplementar(saida.getCdIndicadorContaComplementar());
		setDsIndicadorAssociacaoLayout(saida.getDsIndicadorAssociacaoLayout());
		setDsIndicadorGeracaoSegmentoB(saida.getDsSegmentoB());
		setDsIndicadorGeracaoSegmentoZ(saida.getDsSegmentoZ());
		
		setCdIndicadorCpfLayout(saida.getCdIndicadorCpfLayout());
		setCdIndicadorAssociacaoLayout(saida.getCdIndicadorAssociacaoLayout());
		setCdIndicadorGeracaoSegmentoB(saida.getCdIndicadorGeracaoSegmentoB());
		setCdIndicadorGeracaoSegmentoZ(saida.getCdIndicadorGeracaoSegmentoZ());
		
		setCodigo(saida.getCdPerfilTrocaArq());
		setTipoLayoutArquivo(saida.getDsCodTipoLayout());
		setCboTipoLayoutArquivo(saida.getCdTipoLayoutArquivo());
		setAplicativoFormatacao(saida.getDsAplicFormat());
		setUtilizaLayoutProprio(saida.getDsUtilizacaoEmpresaVan());
		setCboEmpresaRespTransDadosContr(saida.getCdPessoaJuridicaParceiro());
		setCboRespCustoTransArqDadosContr(saida.getCdResponsavelCustoEmpresa());
		setDsLayoutProprio(getListaGridConsultar().get(getItemSelecionadoConsultar())
				.getDsLayoutProprio());
		

		setCboAplicativoFormatacao(saida.getCdAplicacaoTransmPagamento().intValue());
		  

		// Remessa
		setTipoRejeicaoAcolhimento(saida.getDsRejeicaoAcltoRemessa());
		setCboTipoRejeicaoAcolhimento(saida.getCdRejeicaoAcltoRemessa());
		setTipoFiltroSelecionado("");
		setQuantidadeRegistrosInconsistentes(null);
		setQuantidadeRegistrosInconsistentesFormatada("");
		setPercentualRegistrosInconsistentes(null);
		setDesabilitaRadio(true);
		
	

		if (getCboTipoRejeicaoAcolhimento() != null && getCboTipoRejeicaoAcolhimento() == 3) {
			setDesabilitaRadio(false);
			if(saida.getPercentualIncotRejeiRemessa() != null){
				if (saida.getQtMaxIncotRemessa() != null && saida.getQtMaxIncotRemessa() != 0 &&
						saida.getPercentualIncotRejeiRemessa().compareTo(BigDecimal.ZERO) == 0) {
					setTipoFiltroSelecionado("0");
					setQuantidadeRegistrosInconsistentes(saida.getQtMaxIncotRemessa());
					setQuantidadeRegistrosInconsistentesFormatada(saida.getQtMaxIncotRemessaFormatada());
				} else {
					if (saida.getQtMaxIncotRemessa() != null && saida.getQtMaxIncotRemessa() == 0
									&& PgitUtil.verificaBigDecimalNulo(
											saida.getPercentualIncotRejeiRemessa()).compareTo(BigDecimal.ZERO) != 0) {
						setTipoFiltroSelecionado("1");
						setPercentualRegistrosInconsistentes(saida.getPercentualIncotRejeiRemessa());
					}
				}
			}
		}
		setTipoControleRemessa(saida.getDsControleNumeroRemessa());
		setCboTipoControleRemessa(saida.getCdControleNumeroRemessa());
		setNivelControleRemessa(saida.getDsNivelControleRemessa());
		setCboNivelControleRemessa(saida.getCdNivelControleRemessa());
		setPeriodicidadeInicializacaoContagemRemessa(saida.getDsPeriodicidadeContagemRemessa());
		setCboPeriodicidadeInicializacaoContagemRemessa(saida.getCdPeriodicidadeContagemRemessa());
		setNumeroMaximoRemessa(Long.valueOf(0L).equals(saida.getNrMaxContagemRemessa()) ? null : 
			saida.getNrMaxContagemRemessa());
		setMeioTransmissaoPrincipalRemessa(saida.getDsCodMeioPrincipalRemessa());
		setCboMeioTransmissaoPrincipalRemessa(saida.getCdMeioPrincipalRemessa());
		setMeioTransmissaoAlternativoRemessa(saida.getDsCodMeioAlternRemessa());
		setCboMeioTransmissaoAlternativoRemessa(saida.getCdMeioAlternRemessa());

		// Retorno
		setNivelControleRetorno(saida.getDsNivelControleRetorno());
		setCboNivelControleRetorno(saida.getCdNivelControleRetorno());
		setTipoControleRetorno(saida.getDsControleNumeroRetorno());
		setCboTipoControleRetorno(saida.getCdControleNumeroRetorno());
		setPeriodicidadeInicializacaoContagemRetorno(saida.getDsPeriodicodadeContagemRetorno());
		setCboPeriodicidadeInicializacaoContagemRetorno(saida.getCdPeriodicodadeContagemRetorno());
		setNumeroMaximoRetorno(saida.getNrMaxContagemRetorno());
		setMeioTransmissaoPrincipalRetorno(saida.getCdCodMeioPrincipalRetorno());
		setCboMeioTransmissaoPrincipalRetorno(saida.getCdMeioPrincipalRetorno());
		setMeioTransmissaoAlternativoRetorno(saida.getDsMeioAltrnRetorno());
		setCboMeioTransmissaoAlternativoRetorno(saida.getCdMeioAltrnRetorno());

		//Dados de Controle
		setCboEmpresaResponsavelTransmissao(saida.getCdPessoaJuridicaParceiro());
		setSistemaOrigemArquivoFiltro(saida.getCdSistemaOrigemArquivo());
		setSistemaOrigemArquivo(PgitUtil.concatenarCampos(saida.getCdSistemaOrigemArquivo(), saida
						.getDsSistemaOrigemArquivo(), "-"));
		setCboSistemaOrigemArquivo(saida.getCdSistemaOrigemArquivo());
		setEmpresaResponsavelTransmissao(saida.getDsEmpresa());
		setRdoVan(saida.getDsUtilizacaoEmpresaVan().equalsIgnoreCase("sim") ? "1" : "2");
		setUsername(saida.getDsNomeCliente());
		setNomeArquivoRemessa(saida.getDsNomeArquivoRemessa());
		setNomeArquivoRetorno(saida.getDsNomeArquivoRetorno());
		setCboRespCustoTransArqDadosContr(saida.getCdResponsavelCustoEmpresa());
		setResponsavelCustoTransmissaoArquivo(saida.getDsResponsavelCustoEmpresa());

		if (saida.getPcCustoOrganizacaoTransmissao() != null && saida.getPcCustoOrganizacaoTransmissao().compareTo(
				BigDecimal.ZERO) == 0) {
			setTxtPercentualCustoTransArq(null);
		} else {
			setTxtPercentualCustoTransArq(saida.getPcCustoOrganizacaoTransmissao());
		}

		setVolumeMensalTraf(saida.getQtMesRegistroTrafg());
		setJuncaoRespCustoTrans(Integer.valueOf(0).equals(saida.getCdUnidadeOrganizacional()) ? null : 
			saida.getCdUnidadeOrganizacional());

		//Dados de Contato do Cliente
		setNomeContatoCliente(saida.getDsNomeContatoCliente());
		setDddContatoCliente(saida.getCdAreaFoneCliente() == 0 ? null : saida.getCdAreaFoneCliente());
		setTelefoneContatoCliente(NumberUtils.formatTelefoneSemDdd(saida.getCdFoneContatoCliente()
				.replace("-", "")));
		setRamalContatoCliente(saida.getCdRamalContatoCliente());
		setEmailContatoCliente(saida.getDsEmailContatoCliente());
		
		//Dados de Contato do Cliente
		setNomeContatoSolicitante(saida.getDsSolicitacaoPendente());
		setDddContatoSolicitante(saida.getCdAreaFonePend() == 0 ? null : saida.getCdAreaFonePend());
		setTelefoneContatoSolicitante(NumberUtils.formatTelefoneSemDdd(saida.getCdFoneSolicitacaoPend()
				.replace("-", "")));
		setRamalContatoSolicitante(saida.getCdRamalSolctPend());
		setEmailContatoSolicitante(saida.getDsEmailSolctPend());
		setJustificativaSolicitante(saida.getDsObsGeralPerfil());

		// Trilha Auditoria
		setDataHoraInclusao(saida.getHrInclusaoRegistro());
		setUsuarioInclusao(saida.getCdUsuarioInclusao());
		setTipoCanalInclusao(saida.getCanalInclusaoFormatado());
		setComplementoInclusao((String) PgitUtil.verificaZero(saida.getCdOperacaoCanalInclusao()));

		setDataHoraManutencao(saida.getHrManutencaoRegistro());
		setUsuarioManutencao(saida.getCdUsuarioManutencao());
		setTipoCanalManutencao(saida.getCanalManutencaoFormatado());
		setComplementoManutencao((String) PgitUtil.verificaZero(saida.getCdOperacaoCanalManutencao()));

		setCdSerieAplicTranmicao(saida.getCdSerieAplicTranmicao());
		
		//Aciona processo pgic.apoioProduto.perfilListaContratosVinculadosPerfil		
		buscaContratosVinculados();
		formatarDadoCliente();
		
		if(saida.getCdIndicadorLayoutProprio() == 1 ){
			setAplicativoFormatacao("");
			setDsLayoutProprio("Sim");
		}else if(saida.getCdIndicadorLayoutProprio() == 2){
			setDsLayoutProprio("N�o");
			setDsLayoutProprio(saida.getDsAplicFormat());
		}
		
		if (saida.getCdTipoLayoutArquivo() == 14 || saida.getCdTipoLayoutArquivo() == 15 || saida.getCdTipoLayoutArquivo() == 16) {
			setRenderizaCamposGeracaoSegmentosBeZ(true);
			setRdoCdIndicadorGeracaoSegmentoB(saida.getCdIndicadorGeracaoSegmentoB());
			setDsSegmentoB(saida.getDsSegmentoB());
			setCboCdIndicadorGeracaoSegmentoZ(saida.getCdIndicadorGeracaoSegmentoZ());
			setDsSegmentoZ(saida.getDsSegmentoZ());
		 }
		else{
			setRenderizaCamposGeracaoSegmentosBeZ(false);
		}
  		
		setTipoServico(listaGridConsultar.get(itemSelecionadoConsultar).getDsProdutoServico());
	}
	
	/**
	 * Busca contratos vinculados.
	 */
	private void buscaContratosVinculados(){
		ListaContratosVinculadosPerfilEntradaDTO entradaListaContratosVinc =
			new ListaContratosVinculadosPerfilEntradaDTO();
		
		entradaListaContratosVinc.setCdTipoLayoutArquivo(getListaGridConsultar().get(itemSelecionadoConsultar)
				.getCdTipoLayoutArquivo());
		entradaListaContratosVinc.setCdPerfilTrocaArquivo(getListaGridConsultar().get(itemSelecionadoConsultar)
				.getCdPerfilTrocaArquivo());
		entradaListaContratosVinc.setCdPessoaJuridica(getEmpresaGestoraFiltro());
		entradaListaContratosVinc.setCdTipoContrato(getTipoContratoFiltro());
		if(getNumeroFiltro() == null) {
			entradaListaContratosVinc.setNrSequenciaContrato(0L);
		}else{
			entradaListaContratosVinc.setNrSequenciaContrato("".equals(getNumeroFiltro()) ? 0L : 
				new Long(getNumeroFiltro()));
		}
		
		listaContratosVinculados = getManterPerfilTrocaArqLayoutServiceImpl().listarContratosVinculadosPerfil(
				entradaListaContratosVinc);
		flagContratoVinc = listaContratosVinculados.size() > 0 ? true :false;
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		return TELA_INCLUIR;
	}

	/**
	 * Voltar alterar.
	 *
	 * @return the string
	 */
	public String voltarAlterar() {
		return TELA_ALTERAR;
	}

	/*
	 * HIST�RICO
	 */
	/**
	 * Voltar consulta.
	 *
	 * @return the string
	 */
	public String voltarConsulta() {
		return TELA_CONSULTAR;
	}

	/**
	 * Historico.
	 *
	 * @return the string
	 */
	public String historico() {
		// limpa tudo
		limparHistorico();

		return TELA_HISTORICO;
	}

	/**
	 * Detalhar historico.
	 *
	 * @return the string
	 */
	public String detalharHistorico() {
		try {
			ConsultarHistoricoPerfilTrocaArquivoSaidaDTO registroSelecionado = getListaGridHistorico().get(
							getItemSelecionadoHistorico());

			DetalharHistoricoPerfilTrocaArquivoEntradaDTO entrada = 
				new DetalharHistoricoPerfilTrocaArquivoEntradaDTO();
			entrada.setCdPerfilTrocaArquivo(registroSelecionado.getCdPerfilTrocaArquivo());
			entrada.setCdTipoLayoutArquivo(registroSelecionado.getCdTipoLayoutArquivo());
			entrada.setHrInclusaoRegistroHist(registroSelecionado.getHrManutencao());

			DetalharHistoricoPerfilTrocaArquivoSaidaDTO saida = getManterPerfilTrocaArqLayoutServiceImpl()
							.detalharHistoricoPerfilTrocaArquivo(entrada);

			Integer tipoLayoutArquivoSelecionado = registroSelecionado.getCdTipoLayoutArquivo();
			if(tipoLayoutArquivoSelecionado != null){
				if(tipoLayoutArquivoSelecionado.equals(14) || tipoLayoutArquivoSelecionado.equals(15) || tipoLayoutArquivoSelecionado.equals(16)){
					setExibirCamposCNAB(true);
				}else{
					setExibirCamposCNAB(false);
				}
			}
			
			setDsIndicadorCpfLayout(saida.getDsIndicadorCpfLayout());
			setDsIndicadorContaComplementar(saida.getDsIndicadorContaComplementar());
			setDsIndicadorConsisteContaDebito(saida.getDsIndicadorConsisteContaDebito());
			setDsIndicadorAssociacaoLayout(saida.getDsIndicadorAssociacaoLayout());
			setDsIndicadorGeracaoSegmentoB(saida.getDsSegmentoB());
			setDsIndicadorGeracaoSegmentoZ(saida.getDsSegmentoZ());
			
			setCodigo(saida.getCdPerfilTrocaArq());
			setTipoLayoutArquivo(saida.getDsCodTipoLayout());
			setAplicativoFormatacao(saida.getDsAplicFormat());
			setSistemaOrigemArquivo(PgitUtil.concatenarCampos(saida.getCdSistemaOrigemArquivo(), saida
							.getDsSistemaOrigemArquivo(), "-"));
			setEmpresaResponsavelTransmissao(saida.getDsEmpresa());
			setRdoVan(saida.getDsUtilizacaoEmpresaVan());

			if (saida.getPcCustoOrganizacaoTransmissao() != null && saida.getPcCustoOrganizacaoTransmissao()
					.compareTo(BigDecimal.ZERO) == 0) {
				setTxtPercentualCustoTransArq(null);
			} else {
				setTxtPercentualCustoTransArq(saida.getPcCustoOrganizacaoTransmissao());
			}

			setResponsavelCustoTransmissaoArquivo(saida.getDsResponsavelCustoEmpresa());
			setUsername(saida.getDsNomeCliente());
			setNomeArquivoRemessa(saida.getDsNomeArquivoRemessa());
			setNomeArquivoRetorno(saida.getDsNomeArquivoRetorno());

			// Remessa
			setTipoRejeicaoAcolhimento(saida.getDsRejeicaoAcltoRemessa());

			setQuantidadeRegistrosInconsistentes(null);
			setQuantidadeRegistrosInconsistentesFormatada("");
			setPercentualRegistrosInconsistentes(null);
			if (saida.getCdRejeicaoAcltoRemessa() != null && saida.getCdRejeicaoAcltoRemessa() == 3) {
				if (saida.getQtMaxIncotRemessa() != null && saida.getQtMaxIncotRemessa() != 0
								&& saida.getQtMaxIncotRemessa().compareTo(0) == 0) {
					setQuantidadeRegistrosInconsistentes(saida.getQtMaxIncotRemessa());
					setQuantidadeRegistrosInconsistentesFormatada(saida.getQtMaxIncotRemessaFormatada());
				} else {
					if (saida.getQtMaxIncotRemessa() != null
									&& saida.getQtMaxIncotRemessa() == 0
									&& saida.getQtMaxIncotRemessa().compareTo(0) != 0) {
						setPercentualRegistrosInconsistentes(saida.getPercentualIncotRejeiRemessa());
					}
				}
			}

			setTipoControleRemessa(saida.getDsControleNumeroRemessa());
			setNivelControleRemessa(saida.getDsNivelControleRemessa());
			setPeriodicidadeInicializacaoContagemRemessa(saida.getDsPeriodicidadeContagemRemessa());
			setStrNumeroMaximoRemessa(saida.getNrMaxContagemRemessa().toString().replace("0", "").equals("") ? "" : 
				saida.getNrMaxContagemRemessa().toString());
			setMeioTransmissaoPrincipalRemessa(saida.getDsCodMeioPrincipalRemessa());
			setMeioTransmissaoAlternativoRemessa(saida.getDsCodMeioAlternRemessa());

			// Retorno
			setNivelControleRetorno(saida.getDsNivelControleRetorno());
			setTipoControleRetorno(saida.getDsControleNumeroRetorno());
			setPeriodicidadeInicializacaoContagemRetorno(saida.getDsPeriodicodadeContagemRetorno());
			setNumeroMaximoRetorno(saida.getNrMaxContagemRetorno());
			setStrNumeroMaximoRetorno(saida.getNrMaxContagemRetorno().toString().replace("0", "").equals("") ? "" : 
				saida.getNrMaxContagemRetorno().toString());
			setMeioTransmissaoPrincipalRetorno(saida.getCdCodMeioPrincipalRetorno());
			setMeioTransmissaoAlternativoRetorno(saida.getDsMeioAltrnRetorno());

			setTipoAcao(saida.getDsIndicadorTipoManutencao());

			// Trilha Auditoria
			setDataHoraInclusao(FormatarData.formatarDataTrilha(saida.getHrInclusaoRegistro()));
			setUsuarioInclusao(saida.getCdUsuarioInclusao());
			setTipoCanalInclusao(saida.getCanalInclusaoFormatado());
			setComplementoInclusao((String) PgitUtil.verificaZero(saida.getCdOperacaoCanalInclusao()));

			setDataHoraManutencao(FormatarData.formatarDataTrilha(saida.getHrManutencaoRegistro()));
			setUsuarioManutencao(saida.getCdUsuarioManutencao());
			setTipoCanalManutencao(saida.getCanalManutencaoFormatado());
			
			//Campos novos - solicita��o 900007480
			setVolumeMensalTraf(NumberUtils.formatVolume(saida.getQtMesRegistroTrafg()).replace(",", "."));
			setJuncaoRespCustoTrans(Integer.valueOf(0).equals(saida.getCdUnidadeOrganizacional()) ? null : 
				saida.getCdUnidadeOrganizacional());
			setNomeContatoCliente(saida.getDsNomeContatoCliente());
			setDddContatoCliente(Integer.valueOf(0).equals(saida.getCdAreaFoneCliente()) ? null : 
				saida.getCdAreaFoneCliente());
			setTelefoneContatoCliente(NumberUtils.formatTelefoneSemDdd(saida.getCdFoneContatoCliente()));
			setRamalContatoCliente(saida.getCdRamalContatoCliente());
			setEmailContatoCliente(saida.getDsEmailContatoCliente());
			setNomeContatoSolicitante(saida.getDsSolicitacaoPendente());
			setDddContatoSolicitante(Integer.valueOf(0).equals(saida.getCdAreaFonePend()) ? null : 
				saida.getCdAreaFonePend());
			setTelefoneContatoSolicitante(NumberUtils.formatTelefoneSemDdd(saida.getCdFoneSolicitacaoPend()));
			setRamalContatoSolicitante(saida.getCdRamalSolctPend());
			setEmailContatoSolicitante(saida.getDsEmailSolctPend());
			setJustificativaSolicitante(saida.getDsObsGeralPerfil());

			setComplementoManutencao((String) PgitUtil.verificaZero(saida.getCdOperacaoCanalManutencao()));
			formatarDadoCliente();
			
	  		if (saida.getCdTipoLayoutArquivo() == 14 || saida.getCdTipoLayoutArquivo() == 15 || saida.getCdTipoLayoutArquivo() == 16) {
				setRenderizaCamposGeracaoSegmentosBeZ(true);
				setRdoCdIndicadorGeracaoSegmentoB(saida.getCdIndicadorGeracaoSegmentoB());
				setDsSegmentoB(saida.getDsSegmentoB());
				setCboCdIndicadorGeracaoSegmentoZ(saida.getCdIndicadorGeracaoSegmentoZ());
				setDsSegmentoZ(saida.getDsSegmentoZ());
			 }
			else{
				setRenderizaCamposGeracaoSegmentosBeZ(false);
			}
	  		
			return TELA_DETALHAR_HISTORICO;
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			return "";
		}
	}

	/**
	 * Voltar historico.
	 *
	 * @return the string
	 */
	public String voltarHistorico() {
		return TELA_HISTORICO;
	}
	
	/**
	 * Voltar contrato.
	 *
	 * @return the string
	 */
	public String voltarContrato(){
		return TELA_CONSULTAR;
	}

	/**
	 * Pesquisar historico.
	 *
	 * @param evt the evt
	 */
	public void pesquisarHistorico(ActionEvent evt) {
		consultarHistorico();
	}

	/**
	 * Consultar historico.
	 *
	 * @return the string
	 */
	public String consultarHistorico() {
		limparInformacaoConsultaHistorico();
		try {
			setListaGridHistorico(new ArrayList<ConsultarHistoricoPerfilTrocaArquivoSaidaDTO>());
			ConsultarHistoricoPerfilTrocaArquivoEntradaDTO entrada = 
				new ConsultarHistoricoPerfilTrocaArquivoEntradaDTO();
			entrada.setCdPerfilTrocaArquivo(getCodigoPerfilHist());
			entrada.setCdTipoLayoutArquivo(getTipoLayoutArquivoHist());
			entrada.setDtManutencaoInicio(FormatarData.formataDiaMesAno(getDataIncialManutencao()));
			entrada.setDtManutencaoFim(FormatarData.formataDiaMesAno(getDataFimManutencao()));

			setListaGridHistorico(getManterPerfilTrocaArqLayoutServiceImpl().consultarHistoricoPerfilTrocaArquivo(
							entrada));

			setListaControleHistorico(new ArrayList<SelectItem>());
			for (int i = 0; i < getListaGridHistorico().size(); i++) {
				getListaControleHistorico().add(new SelectItem(i, ""));
			}

			setItemSelecionadoHistorico(null);
			setDisableArgumentosConsultaHistorico(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setItemSelecionadoHistorico(null);
			setListaGridHistorico(null);
		}

		return "";
	}

	/**
	 * Limpar historico.
	 *
	 * @return the string
	 */
	public String limparHistorico() {
		setCodigoPerfilHist(null);
		setTipoLayoutArquivoHist(null);
		setDataIncialManutencao(new Date());
		setDataFimManutencao(new Date());
		limparInformacaoConsultaHistorico();
		setDisableArgumentosConsultaHistorico(false);
		return "";
	}

	/**
	 * Limpar informacao consulta historico.
	 *
	 * @return the string
	 */
	public String limparInformacaoConsultaHistorico() {
		setListaGridHistorico(null);
		setItemSelecionadoHistorico(null);
		return "";
	}

	/*
	 * FIM HIST�RICO
	 */

	// COMBOS
	/**
	 * Listar tipo layout arquivo.
	 */
	private void listarTipoLayoutArquivo() {
		try {
			setListaTipoLayoutArquivo(new ArrayList<SelectItem>());
			setListaTipoLayoutArquivoHash(new HashMap<Integer, String>());

			TipoLayoutArquivoEntradaDTO entrada = new TipoLayoutArquivoEntradaDTO();
			entrada.setCdSituacaoVinculacaoConta(0);

			List<TipoLayoutArquivoSaidaDTO> lista = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			lista = getComboServiceImpl().listarTipoLayoutArquivoCem(entrada);

			for (TipoLayoutArquivoSaidaDTO combo : lista) {
				getListaTipoLayoutArquivo().add(
								new SelectItem(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo()));
				getListaTipoLayoutArquivoHash().put(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaTipoLayoutArquivo(new ArrayList<SelectItem>());
			setListaTipoLayoutArquivoHash(new HashMap<Integer, String>());
		}
	}

	/**
	 * Listar combo tipo layout arquivo com entrada.
	 */
	public void listarComboTipoLayoutArquivoComEntrada() {
		try {
			setListaTipoLayoutArquivo(new ArrayList<SelectItem>());
			setListaTipoLayoutArquivoHash(new HashMap<Integer, String>());

			TipoLayoutArquivoEntradaDTO entrada = new TipoLayoutArquivoEntradaDTO();
			entrada.setCdSerie(getCboAplicativoFormatacao().intValue());
			entrada.setCdSituacaoVinculacaoConta(0);

			List<TipoLayoutArquivoSaidaDTO> lista = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			lista = getComboServiceImpl().listarAplictvFormtTipoLayoutArqv(entrada);

			for (TipoLayoutArquivoSaidaDTO combo : lista) {
				getListaTipoLayoutArquivo().add(
								new SelectItem(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo()));
				getListaTipoLayoutArquivoHash().put(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
			}

			habilitaCombos();
		} catch (PdcAdapterFunctionalException e) {
			setListaTipoLayoutArquivo(new ArrayList<SelectItem>());
			setListaTipoLayoutArquivoHash(new HashMap<Integer, String>());
		}
	}

	/**
	 * Listar aplicativo formatacao arquivo.
	 *
	 * @param entrada the entrada
	 * @return the list< listar aplicativo formatacao arquivo saida dt o>
	 */
	private List<ListarAplicativoFormatacaoArquivoSaidaDTO> listarAplicativoFormatacaoArquivo(
			ListarAplicativoFormatacaoArquivoEntradaDTO entrada) {
		try {

			entrada.setCdSituacao(PgitUtil.verificaIntegerNulo(entrada.getCdSituacao()));
			entrada.setCdAplictvTransmPgto(0);
			entrada.setCdTipoLayoutArquivo(PgitUtil.verificaIntegerNulo(entrada.getCdTipoLayoutArquivo()));
			entrada.setNrOcorrencias(50);

			return getComboServiceImpl().listarAplicativoFormatacaoArquivo(entrada);
		} catch (PdcAdapterFunctionalException p) {
			return new ArrayList<ListarAplicativoFormatacaoArquivoSaidaDTO>();
		}
	}
	
	/**
	 * Consultar empresa transmissao arquivo van.
	 *
	 * @param numeroCombo the numero combo
	 * @return the list< listar empresa transmissao arquivo van saida dt o>
	 */
	private List<ListarEmpresaTransmissaoArquivoVanSaidaDTO> consultarEmpresaTransmissaoArquivoVan(
			Integer numeroCombo) {
		try {
			ListarEmpresaTransmissaoArquivoVanEntradaDTO entrada = 
				new ListarEmpresaTransmissaoArquivoVanEntradaDTO();
			entrada.setNrOcorrencias(numeroCombo);
			entrada.setCdSituacaoVinculacaoConta(0);

			return getComboServiceImpl().consultarEmpresaTransmissaoArquivoVan(entrada);
		} catch (PdcAdapterFunctionalException p) {
			return new ArrayList<ListarEmpresaTransmissaoArquivoVanSaidaDTO>();
		}
	}

	/**
	 * Listar aplicativo formatacao.
	 */
	public void listarAplicativoFormatacao() {
		setListaAplicativoFormatacao(new ArrayList<SelectItem>());
		setListaAplicativoFormatacaoHash(new HashMap<Integer, String>());
		
		ListarAplicativoFormatacaoArquivoEntradaDTO entradaDTO = 
			new ListarAplicativoFormatacaoArquivoEntradaDTO();
		entradaDTO.setCdSituacao(48);
		entradaDTO.setCdTipoLayoutArquivo(cboTipoLayoutArquivo);
		entradaDTO.setNrOcorrencias(50);

		List<ListarAplicativoFormatacaoArquivoSaidaDTO> lista = listarAplicativoFormatacaoArquivo(entradaDTO);

		for (ListarAplicativoFormatacaoArquivoSaidaDTO combo : lista) {
			getListaAplicativoFormatacao().add(new SelectItem(combo.getCodigo(), combo.getDescricao()));
			getListaAplicativoFormatacaoHash().put(combo.getCodigo(), combo.getDescricao());
		}
	}

	/**
	 * Obter sistema origem arquivo.
	 */
	public void obterSistemaOrigemArquivo() {
		try {
			setCboSistemaOrigemArquivo(null);

			setListaSistemaOrigemArquivo(new ArrayList<SelectItem>());
			setListaSistemaOrigemArquivoHash(new HashMap<String, String>());

			CentroCustoEntradaDTO entrada = new CentroCustoEntradaDTO();
			entrada.setCdSistema(PgitUtil.verificaStringNula(getSistemaOrigemArquivoFiltro()).toUpperCase());

			List<CentroCustoSaidaDTO> lista = new ArrayList<CentroCustoSaidaDTO>();
			lista = getComboServiceImpl().listarCentroCusto(entrada);

			for (CentroCustoSaidaDTO combo : lista) {
				getListaSistemaOrigemArquivo().add(
								new SelectItem(combo.getCdCentroCusto(), PgitUtil.concatenarCampos(combo
												.getCdCentroCusto(), combo.getDsCentroCusto(), "-")));
				getListaSistemaOrigemArquivoHash().put(combo.getCdCentroCusto(),
								PgitUtil.concatenarCampos(combo.getCdCentroCusto(), combo.getDsCentroCusto(), "-"));
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaSistemaOrigemArquivo(new ArrayList<SelectItem>());
			setListaSistemaOrigemArquivoHash(new HashMap<String, String>());
		}
	}

	/**
	 * Listar empresa responsavel transmissao.
	 */
	private void listarEmpresaResponsavelTransmissao() {
		setListaEmpresaResponsavelTransmissao(new ArrayList<SelectItem>());
		setListaEmpresaResponsavelTransmissaoHash(new HashMap<Long, String>());

		List<ListarEmpresaTransmissaoArquivoVanSaidaDTO> lista = consultarEmpresaTransmissaoArquivoVan(50);

		for (ListarEmpresaTransmissaoArquivoVanSaidaDTO combo : lista) {
			getListaEmpresaResponsavelTransmissao().add(new SelectItem(combo.getCdTipoParticipacao(), 
					combo.getDsTipoParticipacao()));
			getListaEmpresaResponsavelTransmissaoHash().put(combo.getCdTipoParticipacao(), 
					combo.getDsTipoParticipacao());
		}
	}
	
	/**
	 * Listar tipo rejeicao acolhimento.
	 */
	private void listarTipoRejeicaoAcolhimento() {
		setListaTipoRejeicaoAcolhimento(new ArrayList<SelectItem>());
		getListaTipoRejeicaoAcolhimento().add(new SelectItem(1, MessageHelperUtils.getI18nMessage(
				"combo_rejeicaoParcial")));
		getListaTipoRejeicaoAcolhimento().add(new SelectItem(2, MessageHelperUtils.getI18nMessage(
				"combo_rejeicaoToltal")));
		getListaTipoRejeicaoAcolhimento().add(new SelectItem(3, MessageHelperUtils.getI18nMessage(
				"combo_rejeicaoQtdePerc")));

		setListaTipoRejeicaoAcolhimentoHash(new HashMap<Integer, String>());
		getListaTipoRejeicaoAcolhimentoHash().put(1, MessageHelperUtils.getI18nMessage("combo_rejeicaoParcial"));
		getListaTipoRejeicaoAcolhimentoHash().put(2, MessageHelperUtils.getI18nMessage("combo_rejeicaoToltal"));
		getListaTipoRejeicaoAcolhimentoHash().put(3, MessageHelperUtils.getI18nMessage(
				"combo_rejeicaoQtdePerc"));
	}

	/**
	 * Listar responsavel custo transmissaorquivo.
	 */
	public void listarResponsavelCustoTransmissaorquivo() {
		setListaResponsavelCustoTransArq(new ArrayList<SelectItem>());
		getListaResponsavelCustoTransArq().add(new SelectItem(1, MessageHelperUtils.getI18nMessage(
				"label_organizacao_bradesco")));
		getListaResponsavelCustoTransArq().add(new SelectItem(2, MessageHelperUtils.getI18nMessage(
				"label_cliente")));
		getListaResponsavelCustoTransArq().add(new SelectItem(3, MessageHelperUtils.getI18nMessage(
				"label_rateio")));

		setListaResponsavelCustoTransArqHash(new HashMap<Integer, String>());
		getListaResponsavelCustoTransArqHash().put(1, MessageHelperUtils.getI18nMessage(
				"label_organizacao_bradesco"));
		getListaResponsavelCustoTransArqHash().put(2, MessageHelperUtils.getI18nMessage("label_cliente"));
		getListaResponsavelCustoTransArqHash().put(3, MessageHelperUtils.getI18nMessage("label_rateio"));
	}

	/**
	 * Listar nivel controle.
	 */
	public void listarNivelControle() {
		setListaNivelControle(new ArrayList<SelectItem>());
		getListaNivelControle().add(new SelectItem(1, MessageHelperUtils.getI18nMessage("label_arquivo")));
		getListaNivelControle().add(new SelectItem(2, MessageHelperUtils.getI18nMessage("label_lote")));
		getListaNivelControle().add(new SelectItem(3, MessageHelperUtils.getI18nMessage("label_sem_controle")));

		setListaNivelControleHash(new HashMap<Integer, String>());
		getListaNivelControleHash().put(1, MessageHelperUtils.getI18nMessage("label_arquivo"));
		getListaNivelControleHash().put(2, MessageHelperUtils.getI18nMessage("label_lote"));
		getListaNivelControleHash().put(3, MessageHelperUtils.getI18nMessage("label_sem_controle"));
	}

	/**
	 * Listar tipo controle.
	 */
	private void listarTipoControle() {
		setListaTipoControle(new ArrayList<SelectItem>());
		getListaTipoControle().add(new SelectItem(1, MessageHelperUtils.getI18nMessage("label_sequencial")));
		getListaTipoControle().add(new SelectItem(2, MessageHelperUtils.getI18nMessage("label_crescente")));
		getListaTipoControle().add(new SelectItem(3, MessageHelperUtils.getI18nMessage("label_duplicidade")));

		setListaTipoControleHash(new HashMap<Integer, String>());
		getListaTipoControleHash().put(1, MessageHelperUtils.getI18nMessage("label_sequencial"));
		getListaTipoControleHash().put(2, MessageHelperUtils.getI18nMessage("label_crescente"));
		getListaTipoControleHash().put(3, MessageHelperUtils.getI18nMessage("label_duplicidade"));
	}

	/**
	 * Limpar combos dados controle.
	 */
	public void limparCombosDadosControle() {
		setCboEmpresaRespTransDadosContr(0L);
		setCboRespCustoTransArqDadosContr(0);
		setVolumeMensalTraf("");
		setJuncaoRespCustoTrans(null);
		setNomeArquivoRemessa("");
		setNomeArquivoRetorno("");
		setUsername("");
		setTxtPercentualCustoTransArq(null);
		setPercentualCliente(null);
	}

	/**
	 * Limpar cbo aplic formatacao.
	 */
	public void limparCboAplicFormatacao() {
		setCboAplicativoFormatacao(0);
		setCboTipoLayoutArquivo(0);
	}

	/**
	 * Limpar cbo aplic formatacao alterar.
	 */
	public void limparCboAplicFormatacaoAlterar() {
		if("2".equals(rdoAplicFormProprio)) {
			setCboAplicativoFormatacao(2);
		} else {
			setCboAplicativoFormatacao(0);
		}
	}

	/**
	 * Valida combos.
	 */
	public void validaCombos() {
		limparCboAplicFormatacao();
		listarAplicativoFormatacao();
		atribuirComboAplicativoFormatacao();
		habilitaCombos();
		
		if(rdoAplicFormProprio.equals("1")){
			listarComboTipoLayoutArquivoComEntrada();
		}
	}

	/**
	 * Atribuir combo aplicativo formatacao.
	 */
	private void atribuirComboAplicativoFormatacao() {
		if ("1".equals(rdoAplicFormProprio)) {
			setCboAplicativoFormatacao(2);
		} else {
			setCboAplicativoFormatacao(0);	
		}
	}

	/**
	 * Habilita combos.
	 */
	public void habilitaCombos() {
		if (getRdoAplicFormProprio().equals("1")) {
			setDisableAplicativoFormatacao(false);
			if (getCboAplicativoFormatacao() != 0) {
				setDisableTipoLayoutArquivo(false);
			} else {
				setDisableTipoLayoutArquivo(true);				
			}
		} else if (getRdoAplicFormProprio().equals("2")) {
			listarTipoLayoutArquivo();
			setDisableAplicativoFormatacao(true);
			setDisableTipoLayoutArquivo(false);
		}
	}
	
	/**
	 * Habilitar filtro meio transmissao.
	 */
	public void habilitarFiltroMeioTransmissao(){
		boolean flagMeioTransmissaoPrincipalRemessa = true;
		boolean flagMeioTransmissaoAlternativoRemessa = true;
		boolean flagMeioTransmissaoAlternativoRetorno = true;
		boolean flagMeioTransmissaoPrincipalRetorno= true;
		boolean flagMeioTransmissao14 = true ;
		flagCustoTransmissao = true;
		setFlagDadosControle(true);
		setFlagClienteSolicitante(true);
				
		if(cboMeioTransmissaoPrincipalRemessa != null && cboMeioTransmissaoPrincipalRemessa != 0){
			switch(cboMeioTransmissaoPrincipalRemessa){
				case 7: flagMeioTransmissaoPrincipalRemessa = false;break;
				case 8:flagMeioTransmissaoPrincipalRemessa = false;break;
				case 9:flagMeioTransmissaoPrincipalRemessa = false;break;
				default:flagMeioTransmissaoPrincipalRemessa =true;
			}
		}
		if(cboMeioTransmissaoAlternativoRemessa != null && cboMeioTransmissaoAlternativoRemessa != 0){
			switch(cboMeioTransmissaoAlternativoRemessa){
				case 7: flagMeioTransmissaoAlternativoRemessa = false;break;
				case 8: flagMeioTransmissaoAlternativoRemessa = false;break;
				case 9: flagMeioTransmissaoAlternativoRemessa = false;break;
				default:flagMeioTransmissaoAlternativoRemessa =true;
			}
		}
		if(cboMeioTransmissaoAlternativoRetorno != null && cboMeioTransmissaoAlternativoRetorno != 0){
			switch(cboMeioTransmissaoAlternativoRetorno){
				case 7: flagMeioTransmissaoAlternativoRetorno = false;break;
				case 8:flagMeioTransmissaoAlternativoRetorno = false;break;
				case 9:flagMeioTransmissaoAlternativoRetorno = false;break;
				default:flagMeioTransmissaoAlternativoRetorno =true;
			}
		}
		if(cboMeioTransmissaoPrincipalRetorno != null && cboMeioTransmissaoPrincipalRetorno != 0){
			switch(cboMeioTransmissaoPrincipalRetorno){
				case 7: flagMeioTransmissaoPrincipalRetorno = false;break;
				case 8: flagMeioTransmissaoPrincipalRetorno = false;break;
				case 9: flagMeioTransmissaoPrincipalRetorno = false;break;
				default:flagMeioTransmissaoPrincipalRetorno =true;
			}
		}
		if(cboMeioTransmissaoPrincipalRemessa != null && (cboMeioTransmissaoPrincipalRemessa == 14 
			|| cboMeioTransmissaoAlternativoRemessa == 14 
			|| cboMeioTransmissaoAlternativoRetorno == 14 
			|| cboMeioTransmissaoPrincipalRetorno == 14))
		{
			 flagMeioTransmissao14 = false;
			 setFlagCustoTransmissao(false);
		 }
		if(!flagMeioTransmissao14){
			setFlagMeioTransmissao(false);
			setFlagDadosControle(false);
			setFlagClienteSolicitante(false);
		}else if(!flagMeioTransmissaoPrincipalRemessa || !flagMeioTransmissaoAlternativoRemessa ||
				!flagMeioTransmissaoAlternativoRetorno || !flagMeioTransmissaoPrincipalRetorno)
		{
			setFlagMeioTransmissao(false);
			setFlagDadosControle(true);
			setFlagClienteSolicitante(false);
			limparCombosDadosControle();
		}else{
			  setFlagMeioTransmissao(true);
			  setFlagDadosControle(true);
			  setFlagClienteSolicitante(true);
			  limparCombosDadosControle();
			  limparClienteSolicitante();
		}

		if (MEIO_TRANSMISSAO_WEB_TA.equals(cboMeioTransmissaoAlternativoRetorno)
				&& MEIO_TRANSMISSAO_WEB_TA.equals(cboMeioTransmissaoAlternativoRemessa)
				&& MEIO_TRANSMISSAO_WEB_TA.equals(cboMeioTransmissaoPrincipalRetorno)
				&& MEIO_TRANSMISSAO_WEB_TA.equals(cboMeioTransmissaoPrincipalRemessa)) {

			setTxtPercentualCustoTransArq(null);
			setPercentualCliente(null);
		}
	}

	/**
	 * Habilitar filtro meio transmissao alterar.
	 */
	public void habilitarFiltroMeioTransmissaoAlterar(){
		boolean flagMeioTransmissaoPrincipalRemessa = true;
		boolean flagMeioTransmissaoAlternativoRemessa = true;
		boolean flagMeioTransmissaoAlternativoRetorno = true;
		boolean flagMeioTransmissaoPrincipalRetorno= true;
		boolean flagMeioTransmissao14 = true ;
		flagCustoTransmissao = true;
		setFlagDadosControle(true);
		setFlagClienteSolicitante(true);
				
		if(cboMeioTransmissaoPrincipalRemessa != null && cboMeioTransmissaoPrincipalRemessa != 0){
			switch(cboMeioTransmissaoPrincipalRemessa){
				case 7: flagMeioTransmissaoPrincipalRemessa = false;break;
				case 8:flagMeioTransmissaoPrincipalRemessa = false;break;
				case 9:flagMeioTransmissaoPrincipalRemessa = false;break;
				default:flagMeioTransmissaoPrincipalRemessa =true;
			}
		}
		if(cboMeioTransmissaoAlternativoRemessa != null && cboMeioTransmissaoAlternativoRemessa != 0){
			switch(cboMeioTransmissaoAlternativoRemessa){
				case 7: flagMeioTransmissaoAlternativoRemessa = false;break;
				case 8: flagMeioTransmissaoAlternativoRemessa = false;break;
				case 9: flagMeioTransmissaoAlternativoRemessa = false;break;
				default:flagMeioTransmissaoAlternativoRemessa =true;
			}
		}
		if(cboMeioTransmissaoAlternativoRetorno != null && cboMeioTransmissaoAlternativoRetorno != 0){
			switch(cboMeioTransmissaoAlternativoRetorno){
				case 7: flagMeioTransmissaoAlternativoRetorno = false;break;
				case 8:flagMeioTransmissaoAlternativoRetorno = false;break;
				case 9:flagMeioTransmissaoAlternativoRetorno = false;break;
				default:flagMeioTransmissaoAlternativoRetorno =true;
			}
		}
		if(cboMeioTransmissaoPrincipalRetorno != null && cboMeioTransmissaoPrincipalRetorno != 0){
			switch(cboMeioTransmissaoPrincipalRetorno){
				case 7: flagMeioTransmissaoPrincipalRetorno = false;break;
				case 8: flagMeioTransmissaoPrincipalRetorno = false;break;
				case 9: flagMeioTransmissaoPrincipalRetorno = false;break;
				default:flagMeioTransmissaoPrincipalRetorno =true;
			}
		}
		if(cboMeioTransmissaoPrincipalRemessa != null && (cboMeioTransmissaoPrincipalRemessa == 14 
			|| cboMeioTransmissaoAlternativoRemessa == 14 
			|| cboMeioTransmissaoAlternativoRetorno == 14 
			|| cboMeioTransmissaoPrincipalRetorno == 14))
		{
			 flagMeioTransmissao14 = false;
			 setFlagCustoTransmissao(false);
		 }
		if(flagMeioTransmissao14 == false){
			setFlagMeioTransmissao(false);
			setFlagDadosControle(false);
			setFlagClienteSolicitante(false);
		}else if(flagMeioTransmissaoPrincipalRemessa == false 
				 ||flagMeioTransmissaoAlternativoRemessa == false 
				 || flagMeioTransmissaoAlternativoRetorno == false 
				 || flagMeioTransmissaoPrincipalRetorno == false )
		{
			setFlagMeioTransmissao(false);
			setFlagDadosControle(true);
			setFlagClienteSolicitante(false);
		}else{
			  setFlagMeioTransmissao(true);
			  setFlagDadosControle(true);
			  setFlagClienteSolicitante(true);
		}
	}

	/**
	 * Limpar cliente solicitante.
	 */
	public void limparClienteSolicitante(){
		setNomeContatoCliente(null);
		setDddContatoCliente(null);
		setTelefoneContatoCliente(null);
		setRamalContatoCliente(null);
		setEmailContatoCliente(null);
		setNomeContatoSolicitante(null);
		setDddContatoSolicitante(null);
		setTelefoneContatoSolicitante(null);
		setRamalContatoSolicitante(null);
		setEmailContatoSolicitante(null);
		setJustificativaSolicitante(null);
	}
	
	/**
	 * Listar tipo controle incluir.
	 */
	public void listarTipoControleIncluir() {

		try {
		
				setListaTipoControle(new ArrayList<SelectItem>());
				getListaTipoControle().add(new SelectItem(1, MessageHelperUtils.getI18nMessage("label_sequencial")));
				getListaTipoControle().add(new SelectItem(2, MessageHelperUtils.getI18nMessage("label_crescente")));
				getListaTipoControle().add(new SelectItem(3, MessageHelperUtils.getI18nMessage("label_duplicidade")));

				setListaTipoControleHash(new HashMap<Integer, String>());
				getListaTipoControleHash().put(1, MessageHelperUtils.getI18nMessage("label_sequencial"));
				getListaTipoControleHash().put(2, MessageHelperUtils.getI18nMessage("label_crescente"));
				getListaTipoControleHash().put(3, MessageHelperUtils.getI18nMessage("label_duplicidade"));
			
			if(getCboNivelControleRemessa() != null && getCboNivelControleRemessa().equals(3)){
				setCboTipoControleRemessa(0);
			}
		} catch (Exception e) {
			setListaTipoControle(new ArrayList<SelectItem>());
		}
	}

	/**
	 * Listar tipo controle retorno.
	 */
	public void listarTipoControleRetorno() {
		try {
			
				setListaTipoControleRetorno(new ArrayList<SelectItem>());
				getListaTipoControleRetorno().add(new SelectItem(1, MessageHelperUtils.getI18nMessage(
						"label_sequencial")));
	
				setListaTipoControleRetornoHash(new HashMap<Integer, String>());
				getListaTipoControleRetornoHash().put(1, MessageHelperUtils.getI18nMessage("label_sequencial"));
			if (getCboNivelControleRetorno() != null && getCboNivelControleRetorno().equals(3)) {
					setCboTipoControleRetorno(0);
				}
		} catch (Exception e) {
			setListaTipoControleRetorno(new ArrayList<SelectItem>());
		}
	}

	public void carregaListaGridTipoLayout(){
		if(getCboTipoLayoutArquivo() != null && getCboTipoLayoutArquivo() == 0){
			setExibePanelGroupRepresentantes(false);
		}else{
			setExibePanelGroupRepresentantes(true);
			popularTipoLayoutArquivo();
			comboAplicativoFormatacaoArquivo();
		}
		
		if (getCboTipoLayoutArquivo() == 14 || getCboTipoLayoutArquivo() == 15 || getCboTipoLayoutArquivo() == 16) {
			setRenderizaCamposGeracaoSegmentosBeZ(true);
		 }
		else{
			setRenderizaCamposGeracaoSegmentosBeZ(false);
		}
		
		
		
		setRdoCdIndicadorGeracaoSegmentoB(0);
		setCboCdIndicadorGeracaoSegmentoZ(0);

	}
	
	public void popularTipoLayoutArquivo(){
		try {
			ListarAssLayoutArqProdServicoEntradaDTO entradaDTO = new ListarAssLayoutArqProdServicoEntradaDTO();
			listaGridTipoLayoutArquivo = new ArrayList<IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO>();
			
			entradaDTO.setTipoLayoutArquivo(verificaIntegerNulo(getCboTipoLayoutArquivo()));
	
			setListaGrid(getAssocLayoutArqProdServImpl().listarAssocicaoLayoutArqProdutoServico(entradaDTO));
			
			IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO ocorrencia;
			for (int i = 0; i < getListaGrid().size(); i++) {
				ocorrencia = new IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO(
						 getListaGrid().get(i).getCdTipoServico(), 0, 0, false,
						 getListaGrid().get(i).getDsTipoServico() );
				listaGridTipoLayoutArquivo.add(ocorrencia);
			}
			
					
		} catch (PdcAdapterFunctionalException e) {
			setListaGrid(new ArrayList<ListarAssLayoutArqProdServicoSaidaDTO>());
			System.out.println(e);
		}
	}
	
	public void comboAplicativoFormatacaoArquivo(){
		try{
			setListaAplicativoFormatArquivo(new ArrayList<SelectItem>());
			setListaAplicativoFormatArquivoHash(new HashMap<Integer, String>());

			List<ListarAplicativoFormatacaoArquivoSaidaDTO> lista = new ArrayList<ListarAplicativoFormatacaoArquivoSaidaDTO>();
			lista = getComboServiceImpl().listarAplicativoFormatacaoArquivo(new ListarAplicativoFormatacaoArquivoEntradaDTO());

			for (int i = 0; i < lista.size(); i++) {
				getListaAplicativoFormatArquivo().add(
						new SelectItem(lista.get(i).getCodigo(), lista.get(i).getDescricao()));
				getListaAplicativoFormatArquivoHash().put(lista.get(i).getCodigo(), lista.get(i).getDescricao());
				
			}	
			

		} catch (PdcAdapterFunctionalException e) {
			setListaAplicativoFormatArquivo(new ArrayList<SelectItem>());
			setListaAplicativoFormatArquivoHash(new HashMap<Integer, String>());
			System.out.println(e);
		}
	}
	
	
	/**
	 * Listar nivel controle retorno.
	 */
	public void listarNivelControleRetorno() {
		setListaNivelControleRetorno(new ArrayList<SelectItem>());
		getListaNivelControleRetorno().add(new SelectItem(1, MessageHelperUtils.getI18nMessage("label_arquivo")));
		getListaNivelControleRetorno().add(new SelectItem(2, MessageHelperUtils.getI18nMessage("label_lote")));
		getListaNivelControleRetorno().add(new SelectItem(3, MessageHelperUtils.getI18nMessage(
				"label_sem_controle")));

		setListaNivelControleRetornoHash(new HashMap<Integer, String>());
		getListaNivelControleRetornoHash().put(1, MessageHelperUtils.getI18nMessage("label_arquivo"));
		getListaNivelControleRetornoHash().put(2, MessageHelperUtils.getI18nMessage("label_lote"));
		getListaNivelControleRetornoHash().put(3, MessageHelperUtils.getI18nMessage("label_sem_controle"));
	}

	/**
	 * Listar periodicidade inicializacao contagem.
	 */
	private void listarPeriodicidadeInicializacaoContagem() {
		try {
			setListaPeriodicidadeInicializacaoContagem(new ArrayList<SelectItem>());
			setListaPeriodicidadeInicializacaoContagemHash(new HashMap<Integer, String>());

			PeriodicidadeEntradaDTO entrada = new PeriodicidadeEntradaDTO();
			entrada.setCdSituacaoVinculacaoConta(0);

			List<PeriodicidadeSaidaDTO> lista = new ArrayList<PeriodicidadeSaidaDTO>();
			lista = getComboServiceImpl().listarPeriodicidade(entrada);

			for (PeriodicidadeSaidaDTO combo : lista) {
				if (combo.getCdPeriodicidade() != 8) {
					getListaPeriodicidadeInicializacaoContagem().add(
									new SelectItem(combo.getCdPeriodicidade(), combo.getDsPeriodicidade()));
					getListaPeriodicidadeInicializacaoContagemHash().put(combo.getCdPeriodicidade(),
									combo.getDsPeriodicidade());
				}
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaPeriodicidadeInicializacaoContagem(new ArrayList<SelectItem>());
			setListaPeriodicidadeInicializacaoContagemHash(new HashMap<Integer, String>());
		}
	}

	/**
	 * Listar meio transmissao.
	 */
	private void listarMeioTransmissao() {
		try {
			setListaMeioTransmissao(new ArrayList<SelectItem>());
			setListaMeioTransmissaoHash(new HashMap<Integer, String>());

			ListarMeioTransmissaoPagamentoEntradaDTO entrada = new ListarMeioTransmissaoPagamentoEntradaDTO();
			entrada.setCdSituacaoVinculacaoConta(0);

			ListarMeioTransmissaoPagamentoSaidaDTO saida =  new ListarMeioTransmissaoPagamentoSaidaDTO();
 			List<ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO> lista = 
 				new ArrayList<ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO>();
			
 			saida = getComboServiceImpl().listarMeioTransmissaoPagamento(entrada);

 			lista =  saida.getOcorrencias();
 			
			for (ListarMeioTransmissaoPagamentoOcorrenciaSaidaDTO combo : lista) {
				getListaMeioTransmissao().add(
								new SelectItem(combo.getCdTipoMeioTransmissao(), combo.getDsTipoMeioTransmissao()));
				getListaMeioTransmissaoHash().put(combo.getCdTipoMeioTransmissao(), 
						combo.getDsTipoMeioTransmissao());
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaMeioTransmissao(new ArrayList<SelectItem>());
			setListaMeioTransmissaoHash(new HashMap<Integer, String>());
		}
	}

	/**
	 * Is desabilita custo transmissao.
	 *
	 * @return true, if is desabilita custo transmissao
	 */
	public boolean isDesabilitaCustoTransmissao() {
		if (!Integer.valueOf(0).equals(cboRespCustoTransArqDadosContr) && cboRespCustoTransArqDadosContr != null){
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Is desabilita percentual banco.
	 *
	 * @return true, if is desabilita percentual banco
	 */
	public boolean isDesabilitaPercentualBanco() {
		Integer van = Integer.valueOf(14);
		Integer rvs = Integer.valueOf(7);
		Integer iterpel = Integer.valueOf(8);
		Integer conect = Integer.valueOf(9);
		
		if (van.equals(cboMeioTransmissaoPrincipalRemessa)
				|| van.equals(cboMeioTransmissaoAlternativoRemessa)
				|| van.equals(cboMeioTransmissaoAlternativoRetorno)
				|| van.equals(cboMeioTransmissaoPrincipalRetorno)
				|| rvs.equals(cboMeioTransmissaoPrincipalRemessa)
				|| rvs.equals(cboMeioTransmissaoAlternativoRemessa)
				|| rvs.equals(cboMeioTransmissaoAlternativoRetorno)
				|| rvs.equals(cboMeioTransmissaoPrincipalRetorno)
				|| iterpel.equals(cboMeioTransmissaoPrincipalRemessa)
				|| iterpel.equals(cboMeioTransmissaoAlternativoRemessa)
				|| iterpel.equals(cboMeioTransmissaoAlternativoRetorno)
				|| iterpel.equals(cboMeioTransmissaoPrincipalRetorno)
				|| conect.equals(cboMeioTransmissaoPrincipalRemessa)
				|| conect.equals(cboMeioTransmissaoAlternativoRemessa)
				|| conect.equals(cboMeioTransmissaoAlternativoRetorno)
				|| conect.equals(cboMeioTransmissaoPrincipalRetorno)) {
			return !Integer.valueOf(3).equals(cboRespCustoTransArqDadosContr);
		}
		
		return true;
	}

	// GETTERS E SETTERS

	/**
	 * Get: comboServiceImpl.
	 *
	 * @return comboServiceImpl
	 */
	public IComboService getComboServiceImpl() {
		return comboServiceImpl;
	}

	/**
	 * Set: comboServiceImpl.
	 *
	 * @param comboServiceImpl the combo service impl
	 */
	public void setComboServiceImpl(IComboService comboServiceImpl) {
		this.comboServiceImpl = comboServiceImpl;
	}

	/**
	 * Get: itemSelecionadoConsultar.
	 *
	 * @return itemSelecionadoConsultar
	 */
	public Integer getItemSelecionadoConsultar() {
		return itemSelecionadoConsultar;
	}

	/**
	 * Set: itemSelecionadoConsultar.
	 *
	 * @param itemSelecionadoConsultar the item selecionado consultar
	 */
	public void setItemSelecionadoConsultar(Integer itemSelecionadoConsultar) {
		this.itemSelecionadoConsultar = itemSelecionadoConsultar;
	}

	/**
	 * Get: listaControleConsultar.
	 *
	 * @return listaControleConsultar
	 */
	public List<SelectItem> getListaControleConsultar() {
		return listaControleConsultar;
	}

	/**
	 * Set: listaControleConsultar.
	 *
	 * @param listaControleConsultar the lista controle consultar
	 */
	public void setListaControleConsultar(List<SelectItem> listaControleConsultar) {
		this.listaControleConsultar = listaControleConsultar;
	}

	/**
	 * Get: manterPerfilTrocaArqLayoutServiceImpl.
	 *
	 * @return manterPerfilTrocaArqLayoutServiceImpl
	 */
	public IManterPerfilTrocaArqLayoutService getManterPerfilTrocaArqLayoutServiceImpl() {
		return manterPerfilTrocaArqLayoutServiceImpl;
	}

	/**
	 * Set: manterPerfilTrocaArqLayoutServiceImpl.
	 *
	 * @param manterPerfilTrocaArqLayoutServiceImpl the manter perfil troca arq layout service impl
	 */
	public void setManterPerfilTrocaArqLayoutServiceImpl(
					IManterPerfilTrocaArqLayoutService manterPerfilTrocaArqLayoutServiceImpl) {
		this.manterPerfilTrocaArqLayoutServiceImpl = manterPerfilTrocaArqLayoutServiceImpl;
	}

	/**
	 * Get: codigoPerfilFiltro.
	 *
	 * @return codigoPerfilFiltro
	 */
	public Long getCodigoPerfilFiltro() {
		return codigoPerfilFiltro;
	}

	/**
	 * Set: codigoPerfilFiltro.
	 *
	 * @param codigoPerfilFiltro the codigo perfil filtro
	 */
	public void setCodigoPerfilFiltro(Long codigoPerfilFiltro) {
		this.codigoPerfilFiltro = codigoPerfilFiltro;
	}

	/**
	 * Get: tipoLayoutArquivoFiltro.
	 *
	 * @return tipoLayoutArquivoFiltro
	 */
	public Integer getTipoLayoutArquivoFiltro() {
		return tipoLayoutArquivoFiltro;
	}

	/**
	 * Set: tipoLayoutArquivoFiltro.
	 *
	 * @param tipoLayoutArquivoFiltro the tipo layout arquivo filtro
	 */
	public void setTipoLayoutArquivoFiltro(Integer tipoLayoutArquivoFiltro) {
		this.tipoLayoutArquivoFiltro = tipoLayoutArquivoFiltro;
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 *
	 * @return listaTipoLayoutArquivo
	 */
	public List<SelectItem> getListaTipoLayoutArquivo() {
		return listaTipoLayoutArquivo;
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 *
	 * @param listaTipoLayoutArquivo the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: aplicativoFormatacao.
	 *
	 * @return aplicativoFormatacao
	 */
	public String getAplicativoFormatacao() {
		return aplicativoFormatacao;
	}

	/**
	 * Set: aplicativoFormatacao.
	 *
	 * @param aplicativoFormatacao the aplicativo formatacao
	 */
	public void setAplicativoFormatacao(String aplicativoFormatacao) {
		this.aplicativoFormatacao = aplicativoFormatacao;
	}

	/**
	 * Get: codigo.
	 *
	 * @return codigo
	 */
	public Long getCodigo() {
		return codigo;
	}

	/**
	 * Set: codigo.
	 *
	 * @param codigo the codigo
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: empresaResponsavelTransmissao.
	 *
	 * @return empresaResponsavelTransmissao
	 */
	public String getEmpresaResponsavelTransmissao() {
		return empresaResponsavelTransmissao;
	}

	/**
	 * Set: empresaResponsavelTransmissao.
	 *
	 * @param empresaResponsavelTransmissao the empresa responsavel transmissao
	 */
	public void setEmpresaResponsavelTransmissao(String empresaResponsavelTransmissao) {
		this.empresaResponsavelTransmissao = empresaResponsavelTransmissao;
	}

	/**
	 * Get: meioTransmissaoAlternativoRemessa.
	 *
	 * @return meioTransmissaoAlternativoRemessa
	 */
	public String getMeioTransmissaoAlternativoRemessa() {
		return meioTransmissaoAlternativoRemessa;
	}

	/**
	 * Set: meioTransmissaoAlternativoRemessa.
	 *
	 * @param meioTransmissaoAlternativoRemessa the meio transmissao alternativo remessa
	 */
	public void setMeioTransmissaoAlternativoRemessa(String meioTransmissaoAlternativoRemessa) {
		this.meioTransmissaoAlternativoRemessa = meioTransmissaoAlternativoRemessa;
	}

	/**
	 * Get: meioTransmissaoAlternativoRetorno.
	 *
	 * @return meioTransmissaoAlternativoRetorno
	 */
	public String getMeioTransmissaoAlternativoRetorno() {
		return meioTransmissaoAlternativoRetorno;
	}

	/**
	 * Set: meioTransmissaoAlternativoRetorno.
	 *
	 * @param meioTransmissaoAlternativoRetorno the meio transmissao alternativo retorno
	 */
	public void setMeioTransmissaoAlternativoRetorno(String meioTransmissaoAlternativoRetorno) {
		this.meioTransmissaoAlternativoRetorno = meioTransmissaoAlternativoRetorno;
	}

	/**
	 * Get: meioTransmissaoPrincipalRemessa.
	 *
	 * @return meioTransmissaoPrincipalRemessa
	 */
	public String getMeioTransmissaoPrincipalRemessa() {
		return meioTransmissaoPrincipalRemessa;
	}

	/**
	 * Set: meioTransmissaoPrincipalRemessa.
	 *
	 * @param meioTransmissaoPrincipalRemessa the meio transmissao principal remessa
	 */
	public void setMeioTransmissaoPrincipalRemessa(String meioTransmissaoPrincipalRemessa) {
		this.meioTransmissaoPrincipalRemessa = meioTransmissaoPrincipalRemessa;
	}

	/**
	 * Get: meioTransmissaoPrincipalRetorno.
	 *
	 * @return meioTransmissaoPrincipalRetorno
	 */
	public String getMeioTransmissaoPrincipalRetorno() {
		return meioTransmissaoPrincipalRetorno;
	}

	/**
	 * Set: meioTransmissaoPrincipalRetorno.
	 *
	 * @param meioTransmissaoPrincipalRetorno the meio transmissao principal retorno
	 */
	public void setMeioTransmissaoPrincipalRetorno(String meioTransmissaoPrincipalRetorno) {
		this.meioTransmissaoPrincipalRetorno = meioTransmissaoPrincipalRetorno;
	}

	/**
	 * Get: nivelControleRemessa.
	 *
	 * @return nivelControleRemessa
	 */
	public String getNivelControleRemessa() {
		return nivelControleRemessa;
	}

	/**
	 * Set: nivelControleRemessa.
	 *
	 * @param nivelControleRemessa the nivel controle remessa
	 */
	public void setNivelControleRemessa(String nivelControleRemessa) {
		this.nivelControleRemessa = nivelControleRemessa;
	}

	/**
	 * Get: nivelControleRetorno.
	 *
	 * @return nivelControleRetorno
	 */
	public String getNivelControleRetorno() {
		return nivelControleRetorno;
	}

	/**
	 * Set: nivelControleRetorno.
	 *
	 * @param nivelControleRetorno the nivel controle retorno
	 */
	public void setNivelControleRetorno(String nivelControleRetorno) {
		this.nivelControleRetorno = nivelControleRetorno;
	}

	/**
	 * Get: nomeArquivoRemessa.
	 *
	 * @return nomeArquivoRemessa
	 */
	public String getNomeArquivoRemessa() {
		return nomeArquivoRemessa;
	}

	/**
	 * Set: nomeArquivoRemessa.
	 *
	 * @param nomeArquivoRemessa the nome arquivo remessa
	 */
	public void setNomeArquivoRemessa(String nomeArquivoRemessa) {
		this.nomeArquivoRemessa = nomeArquivoRemessa;
	}

	/**
	 * Get: nomeArquivoRetorno.
	 *
	 * @return nomeArquivoRetorno
	 */
	public String getNomeArquivoRetorno() {
		return nomeArquivoRetorno;
	}

	/**
	 * Set: nomeArquivoRetorno.
	 *
	 * @param nomeArquivoRetorno the nome arquivo retorno
	 */
	public void setNomeArquivoRetorno(String nomeArquivoRetorno) {
		this.nomeArquivoRetorno = nomeArquivoRetorno;
	}

	/**
	 * Get: numeroMaximoRemessa.
	 *
	 * @return numeroMaximoRemessa
	 */
	public Long getNumeroMaximoRemessa() {
		return numeroMaximoRemessa;
	}

	/**
	 * Set: numeroMaximoRemessa.
	 *
	 * @param numeroMaximoRemessa the numero maximo remessa
	 */
	public void setNumeroMaximoRemessa(Long numeroMaximoRemessa) {
		this.numeroMaximoRemessa = numeroMaximoRemessa;
	}

	/**
	 * Get: numeroMaximoRetorno.
	 *
	 * @return numeroMaximoRetorno
	 */
	public Long getNumeroMaximoRetorno() {
		return numeroMaximoRetorno;
	}

	/**
	 * Set: numeroMaximoRetorno.
	 *
	 * @param numeroMaximoRetorno the numero maximo retorno
	 */
	public void setNumeroMaximoRetorno(Long numeroMaximoRetorno) {
		this.numeroMaximoRetorno = numeroMaximoRetorno;
	}

	/**
	 * Get: percentualRegistrosInconsistentes.
	 *
	 * @return percentualRegistrosInconsistentes
	 */
	public BigDecimal getPercentualRegistrosInconsistentes() {
		return percentualRegistrosInconsistentes;
	}

	/**
	 * Set: percentualRegistrosInconsistentes.
	 *
	 * @param percentualRegistrosInconsistentes the percentual registros inconsistentes
	 */
	public void setPercentualRegistrosInconsistentes(BigDecimal percentualRegistrosInconsistentes) {
		this.percentualRegistrosInconsistentes = percentualRegistrosInconsistentes;
	}

	/**
	 * Get: quantidadeRegistrosInconsistentes.
	 *
	 * @return quantidadeRegistrosInconsistentes
	 */
	public Integer getQuantidadeRegistrosInconsistentes() {
		return quantidadeRegistrosInconsistentes;
	}

	/**
	 * Set: quantidadeRegistrosInconsistentes.
	 *
	 * @param quantidadeRegistrosInconsistentes the quantidade registros inconsistentes
	 */
	public void setQuantidadeRegistrosInconsistentes(Integer quantidadeRegistrosInconsistentes) {
		this.quantidadeRegistrosInconsistentes = quantidadeRegistrosInconsistentes;
	}

	/**
	 * Get: sistemaOrigemArquivo.
	 *
	 * @return sistemaOrigemArquivo
	 */
	public String getSistemaOrigemArquivo() {
		return sistemaOrigemArquivo;
	}

	/**
	 * Set: sistemaOrigemArquivo.
	 *
	 * @param sistemaOrigemArquivo the sistema origem arquivo
	 */
	public void setSistemaOrigemArquivo(String sistemaOrigemArquivo) {
		this.sistemaOrigemArquivo = sistemaOrigemArquivo;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: tipoControleRemessa.
	 *
	 * @return tipoControleRemessa
	 */
	public String getTipoControleRemessa() {
		return tipoControleRemessa;
	}

	/**
	 * Set: tipoControleRemessa.
	 *
	 * @param tipoControleRemessa the tipo controle remessa
	 */
	public void setTipoControleRemessa(String tipoControleRemessa) {
		this.tipoControleRemessa = tipoControleRemessa;
	}

	/**
	 * Get: tipoControleRetorno.
	 *
	 * @return tipoControleRetorno
	 */
	public String getTipoControleRetorno() {
		return tipoControleRetorno;
	}

	/**
	 * Set: tipoControleRetorno.
	 *
	 * @param tipoControleRetorno the tipo controle retorno
	 */
	public void setTipoControleRetorno(String tipoControleRetorno) {
		this.tipoControleRetorno = tipoControleRetorno;
	}

	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public String getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}

	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(String tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}

	/**
	 * Get: tipoRejeicaoAcolhimento.
	 *
	 * @return tipoRejeicaoAcolhimento
	 */
	public String getTipoRejeicaoAcolhimento() {
		return tipoRejeicaoAcolhimento;
	}

	/**
	 * Set: tipoRejeicaoAcolhimento.
	 *
	 * @param tipoRejeicaoAcolhimento the tipo rejeicao acolhimento
	 */
	public void setTipoRejeicaoAcolhimento(String tipoRejeicaoAcolhimento) {
		this.tipoRejeicaoAcolhimento = tipoRejeicaoAcolhimento;
	}

	/**
	 * Get: username.
	 *
	 * @return username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Set: username.
	 *
	 * @param username the username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: periodicidadeInicializacaoContagemRemessa.
	 *
	 * @return periodicidadeInicializacaoContagemRemessa
	 */
	public String getPeriodicidadeInicializacaoContagemRemessa() {
		return periodicidadeInicializacaoContagemRemessa;
	}

	/**
	 * Set: periodicidadeInicializacaoContagemRemessa.
	 *
	 * @param periodicidadeInicializacaoContagemRemessa the periodicidade inicializacao contagem remessa
	 */
	public void setPeriodicidadeInicializacaoContagemRemessa(String periodicidadeInicializacaoContagemRemessa) {
		this.periodicidadeInicializacaoContagemRemessa = periodicidadeInicializacaoContagemRemessa;
	}

	/**
	 * Get: periodicidadeInicializacaoContagemRetorno.
	 *
	 * @return periodicidadeInicializacaoContagemRetorno
	 */
	public String getPeriodicidadeInicializacaoContagemRetorno() {
		return periodicidadeInicializacaoContagemRetorno;
	}

	/**
	 * Set: periodicidadeInicializacaoContagemRetorno.
	 *
	 * @param periodicidadeInicializacaoContagemRetorno the periodicidade inicializacao contagem retorno
	 */
	public void setPeriodicidadeInicializacaoContagemRetorno(String periodicidadeInicializacaoContagemRetorno) {
		this.periodicidadeInicializacaoContagemRetorno = periodicidadeInicializacaoContagemRetorno;
	}

	/**
	 * Get: codigoPerfilHist.
	 *
	 * @return codigoPerfilHist
	 */
	public Long getCodigoPerfilHist() {
		return codigoPerfilHist;
	}

	/**
	 * Set: codigoPerfilHist.
	 *
	 * @param codigoPerfilHist the codigo perfil hist
	 */
	public void setCodigoPerfilHist(Long codigoPerfilHist) {
		this.codigoPerfilHist = codigoPerfilHist;
	}

	/**
	 * Get: dataFimManutencao.
	 *
	 * @return dataFimManutencao
	 */
	public Date getDataFimManutencao() {
		return dataFimManutencao;
	}

	/**
	 * Set: dataFimManutencao.
	 *
	 * @param dataFimManutencao the data fim manutencao
	 */
	public void setDataFimManutencao(Date dataFimManutencao) {
		this.dataFimManutencao = dataFimManutencao;
	}

	/**
	 * Get: dataIncialManutencao.
	 *
	 * @return dataIncialManutencao
	 */
	public Date getDataIncialManutencao() {
		return dataIncialManutencao;
	}

	/**
	 * Set: dataIncialManutencao.
	 *
	 * @param dataIncialManutencao the data incial manutencao
	 */
	public void setDataIncialManutencao(Date dataIncialManutencao) {
		this.dataIncialManutencao = dataIncialManutencao;
	}

	/**
	 * Get: tipoLayoutArquivoHist.
	 *
	 * @return tipoLayoutArquivoHist
	 */
	public Integer getTipoLayoutArquivoHist() {
		return tipoLayoutArquivoHist;
	}

	/**
	 * Set: tipoLayoutArquivoHist.
	 *
	 * @param tipoLayoutArquivoHist the tipo layout arquivo hist
	 */
	public void setTipoLayoutArquivoHist(Integer tipoLayoutArquivoHist) {
		this.tipoLayoutArquivoHist = tipoLayoutArquivoHist;
	}

	/**
	 * Get: listaAplicativoFormatacao.
	 *
	 * @return listaAplicativoFormatacao
	 */
	public List<SelectItem> getListaAplicativoFormatacao() {
		return listaAplicativoFormatacao;
	}

	/**
	 * Set: listaAplicativoFormatacao.
	 *
	 * @param listaAplicativoFormatacao the lista aplicativo formatacao
	 */
	public void setListaAplicativoFormatacao(List<SelectItem> listaAplicativoFormatacao) {
		this.listaAplicativoFormatacao = listaAplicativoFormatacao;
	}

	/**
	 * Get: listaTipoLayoutArquivoHash.
	 *
	 * @return listaTipoLayoutArquivoHash
	 */
	public Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}

	/**
	 * Set lista tipo layout arquivo hash.
	 *
	 * @param listaTipoLayoutArquivoHash the lista tipo layout arquivo hash
	 */
	public void setListaTipoLayoutArquivoHash(Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}

	/**
	 * Get: listaAplicativoFormatacaoHash.
	 *
	 * @return listaAplicativoFormatacaoHash
	 */
	public Map<Integer, String> getListaAplicativoFormatacaoHash() {
		return listaAplicativoFormatacaoHash;
	}

	/**
	 * Set lista aplicativo formatacao hash.
	 *
	 * @param listaAplicativoFormatacaoHash the lista aplicativo formatacao hash
	 */
	public void setListaAplicativoFormatacaoHash(Map<Integer, String> listaAplicativoFormatacaoHash) {
		this.listaAplicativoFormatacaoHash = listaAplicativoFormatacaoHash;
	}

	/**
	 * Get: listaSistemaOrigemArquivo.
	 *
	 * @return listaSistemaOrigemArquivo
	 */
	public List<SelectItem> getListaSistemaOrigemArquivo() {
		return listaSistemaOrigemArquivo;
	}

	/**
	 * Set: listaSistemaOrigemArquivo.
	 *
	 * @param listaSistemaOrigemArquivo the lista sistema origem arquivo
	 */
	public void setListaSistemaOrigemArquivo(List<SelectItem> listaSistemaOrigemArquivo) {
		this.listaSistemaOrigemArquivo = listaSistemaOrigemArquivo;
	}

	/**
	 * Get: listaSistemaOrigemArquivoHash.
	 *
	 * @return listaSistemaOrigemArquivoHash
	 */
	public Map<String, String> getListaSistemaOrigemArquivoHash() {
		return listaSistemaOrigemArquivoHash;
	}

	/**
	 * Set lista sistema origem arquivo hash.
	 *
	 * @param listaSistemaOrigemArquivoHash the lista sistema origem arquivo hash
	 */
	public void setListaSistemaOrigemArquivoHash(Map<String, String> listaSistemaOrigemArquivoHash) {
		this.listaSistemaOrigemArquivoHash = listaSistemaOrigemArquivoHash;
	}

	/**
	 * Get: sistemaOrigemArquivoFiltro.
	 *
	 * @return sistemaOrigemArquivoFiltro
	 */
	public String getSistemaOrigemArquivoFiltro() {
		return sistemaOrigemArquivoFiltro;
	}

	/**
	 * Set: sistemaOrigemArquivoFiltro.
	 *
	 * @param sistemaOrigemArquivoFiltro the sistema origem arquivo filtro
	 */
	public void setSistemaOrigemArquivoFiltro(String sistemaOrigemArquivoFiltro) {
		this.sistemaOrigemArquivoFiltro = sistemaOrigemArquivoFiltro;
	}

	/**
	 * Get: cboEmpresaResponsavelTransmissao.
	 *
	 * @return cboEmpresaResponsavelTransmissao
	 */
	public Long getCboEmpresaResponsavelTransmissao() {
		return cboEmpresaResponsavelTransmissao;
	}

	/**
	 * Set: cboEmpresaResponsavelTransmissao.
	 *
	 * @param cboEmpresaResponsavelTransmissao the cbo empresa responsavel transmissao
	 */
	public void setCboEmpresaResponsavelTransmissao(Long cboEmpresaResponsavelTransmissao) {
		this.cboEmpresaResponsavelTransmissao = cboEmpresaResponsavelTransmissao;
	}

	/**
	 * Get: listaTipoRejeicaoAcolhimento.
	 *
	 * @return listaTipoRejeicaoAcolhimento
	 */
	public List<SelectItem> getListaTipoRejeicaoAcolhimento() {
		return listaTipoRejeicaoAcolhimento;
	}

	/**
	 * Set: listaTipoRejeicaoAcolhimento.
	 *
	 * @param listaTipoRejeicaoAcolhimento the lista tipo rejeicao acolhimento
	 */
	public void setListaTipoRejeicaoAcolhimento(List<SelectItem> listaTipoRejeicaoAcolhimento) {
		this.listaTipoRejeicaoAcolhimento = listaTipoRejeicaoAcolhimento;
	}

	/**
	 * Get: listaTipoRejeicaoAcolhimentoHash.
	 *
	 * @return listaTipoRejeicaoAcolhimentoHash
	 */
	public Map<Integer, String> getListaTipoRejeicaoAcolhimentoHash() {
		return listaTipoRejeicaoAcolhimentoHash;
	}

	/**
	 * Set lista tipo rejeicao acolhimento hash.
	 *
	 * @param listaTipoRejeicaoAcolhimentoHash the lista tipo rejeicao acolhimento hash
	 */
	public void setListaTipoRejeicaoAcolhimentoHash(Map<Integer, String> listaTipoRejeicaoAcolhimentoHash) {
		this.listaTipoRejeicaoAcolhimentoHash = listaTipoRejeicaoAcolhimentoHash;
	}

	/**
	 * Get: cboAplicativoFormatacao.
	 *
	 * @return cboAplicativoFormatacao
	 */
	public Integer getCboAplicativoFormatacao() {
		return cboAplicativoFormatacao;
	}

	/**
	 * Set: cboAplicativoFormatacao.
	 *
	 * @param cboAplicativoFormatacao the cbo aplicativo formatacao
	 */
	public void setCboAplicativoFormatacao(Integer cboAplicativoFormatacao) {
		this.cboAplicativoFormatacao = cboAplicativoFormatacao;
	}

	/**
	 * Get: cboSistemaOrigemArquivo.
	 *
	 * @return cboSistemaOrigemArquivo
	 */
	public String getCboSistemaOrigemArquivo() {
		return cboSistemaOrigemArquivo;
	}

	/**
	 * Set: cboSistemaOrigemArquivo.
	 *
	 * @param cboSistemaOrigemArquivo the cbo sistema origem arquivo
	 */
	public void setCboSistemaOrigemArquivo(String cboSistemaOrigemArquivo) {
		this.cboSistemaOrigemArquivo = cboSistemaOrigemArquivo;
	}

	/**
	 * Get: cboTipoLayoutArquivo.
	 *
	 * @return cboTipoLayoutArquivo
	 */
	public Integer getCboTipoLayoutArquivo() {
		return cboTipoLayoutArquivo;
	}

	/**
	 * Set: cboTipoLayoutArquivo.
	 *
	 * @param cboTipoLayoutArquivo the cbo tipo layout arquivo
	 */
	public void setCboTipoLayoutArquivo(Integer cboTipoLayoutArquivo) {
		this.cboTipoLayoutArquivo = cboTipoLayoutArquivo;
	}

	/**
	 * Get: listaNivelControle.
	 *
	 * @return listaNivelControle
	 */
	public List<SelectItem> getListaNivelControle() {
		return listaNivelControle;
	}

	/**
	 * Set: listaNivelControle.
	 *
	 * @param listaNivelControle the lista nivel controle
	 */
	public void setListaNivelControle(List<SelectItem> listaNivelControle) {
		this.listaNivelControle = listaNivelControle;
	}

	/**
	 * Get: listaNivelControleHash.
	 *
	 * @return listaNivelControleHash
	 */
	public Map<Integer, String> getListaNivelControleHash() {
		return listaNivelControleHash;
	}

	/**
	 * Set lista nivel controle hash.
	 *
	 * @param listaNivelControleHash the lista nivel controle hash
	 */
	public void setListaNivelControleHash(Map<Integer, String> listaNivelControleHash) {
		this.listaNivelControleHash = listaNivelControleHash;
	}

	/**
	 * Get: cboNivelControleRemessa.
	 *
	 * @return cboNivelControleRemessa
	 */
	public Integer getCboNivelControleRemessa() {
		return cboNivelControleRemessa;
	}

	/**
	 * Set: cboNivelControleRemessa.
	 *
	 * @param cboNivelControleRemessa the cbo nivel controle remessa
	 */
	public void setCboNivelControleRemessa(Integer cboNivelControleRemessa) {
		this.cboNivelControleRemessa = cboNivelControleRemessa;
	}

	/**
	 * Get: cboNivelControleRetorno.
	 *
	 * @return cboNivelControleRetorno
	 */
	public Integer getCboNivelControleRetorno() {
		return cboNivelControleRetorno;
	}

	/**
	 * Set: cboNivelControleRetorno.
	 *
	 * @param cboNivelControleRetorno the cbo nivel controle retorno
	 */
	public void setCboNivelControleRetorno(Integer cboNivelControleRetorno) {
		this.cboNivelControleRetorno = cboNivelControleRetorno;
	}

	/**
	 * Get: cboTipoControleRemessa.
	 *
	 * @return cboTipoControleRemessa
	 */
	public Integer getCboTipoControleRemessa() {
		return cboTipoControleRemessa;
	}

	/**
	 * Set: cboTipoControleRemessa.
	 *
	 * @param cboTipoControleRemessa the cbo tipo controle remessa
	 */
	public void setCboTipoControleRemessa(Integer cboTipoControleRemessa) {
		this.cboTipoControleRemessa = cboTipoControleRemessa;
	}

	/**
	 * Get: cboTipoControleRetorno.
	 *
	 * @return cboTipoControleRetorno
	 */
	public Integer getCboTipoControleRetorno() {
		return cboTipoControleRetorno;
	}

	/**
	 * Set: cboTipoControleRetorno.
	 *
	 * @param cboTipoControleRetorno the cbo tipo controle retorno
	 */
	public void setCboTipoControleRetorno(Integer cboTipoControleRetorno) {
		this.cboTipoControleRetorno = cboTipoControleRetorno;
	}

	/**
	 * Get: cboTipoRejeicaoAcolhimento.
	 *
	 * @return cboTipoRejeicaoAcolhimento
	 */
	public Integer getCboTipoRejeicaoAcolhimento() {
		return cboTipoRejeicaoAcolhimento;
	}

	/**
	 * Set: cboTipoRejeicaoAcolhimento.
	 *
	 * @param cboTipoRejeicaoAcolhimento the cbo tipo rejeicao acolhimento
	 */
	public void setCboTipoRejeicaoAcolhimento(Integer cboTipoRejeicaoAcolhimento) {
		this.cboTipoRejeicaoAcolhimento = cboTipoRejeicaoAcolhimento;
	}

	/**
	 * Get: listaTipoControle.
	 *
	 * @return listaTipoControle
	 */
	public List<SelectItem> getListaTipoControle() {
		return listaTipoControle;
	}

	/**
	 * Set: listaTipoControle.
	 *
	 * @param listaTipoControle the lista tipo controle
	 */
	public void setListaTipoControle(List<SelectItem> listaTipoControle) {
		this.listaTipoControle = listaTipoControle;
	}

	/**
	 * Get: listaTipoControleHash.
	 *
	 * @return listaTipoControleHash
	 */
	public Map<Integer, String> getListaTipoControleHash() {
		return listaTipoControleHash;
	}

	/**
	 * Set lista tipo controle hash.
	 *
	 * @param listaTipoControleHash the lista tipo controle hash
	 */
	public void setListaTipoControleHash(Map<Integer, String> listaTipoControleHash) {
		this.listaTipoControleHash = listaTipoControleHash;
	}

	/**
	 * Get: cboPeriodicidadeInicializacaoContagemRemessa.
	 *
	 * @return cboPeriodicidadeInicializacaoContagemRemessa
	 */
	public Integer getCboPeriodicidadeInicializacaoContagemRemessa() {
		return cboPeriodicidadeInicializacaoContagemRemessa;
	}

	/**
	 * Set: cboPeriodicidadeInicializacaoContagemRemessa.
	 *
	 * @param cboPeriodicidadeInicializacaoContagemRemessa the cbo periodicidade inicializacao contagem remessa
	 */
	public void setCboPeriodicidadeInicializacaoContagemRemessa(
			Integer cboPeriodicidadeInicializacaoContagemRemessa) {
		this.cboPeriodicidadeInicializacaoContagemRemessa = cboPeriodicidadeInicializacaoContagemRemessa;
	}

	/**
	 * Get: cboPeriodicidadeInicializacaoContagemRetorno.
	 *
	 * @return cboPeriodicidadeInicializacaoContagemRetorno
	 */
	public Integer getCboPeriodicidadeInicializacaoContagemRetorno() {
		return cboPeriodicidadeInicializacaoContagemRetorno;
	}

	/**
	 * Set: cboPeriodicidadeInicializacaoContagemRetorno.
	 *
	 * @param cboPeriodicidadeInicializacaoContagemRetorno the cbo periodicidade inicializacao contagem retorno
	 */
	public void setCboPeriodicidadeInicializacaoContagemRetorno(
			Integer cboPeriodicidadeInicializacaoContagemRetorno) {
		this.cboPeriodicidadeInicializacaoContagemRetorno = cboPeriodicidadeInicializacaoContagemRetorno;
	}

	/**
	 * Get: listaPeriodicidadeInicializacaoContagem.
	 *
	 * @return listaPeriodicidadeInicializacaoContagem
	 */
	public List<SelectItem> getListaPeriodicidadeInicializacaoContagem() {
		return listaPeriodicidadeInicializacaoContagem;
	}

	/**
	 * Set: listaPeriodicidadeInicializacaoContagem.
	 *
	 * @param listaPeriodicidadeInicializacaoContagem the lista periodicidade inicializacao contagem
	 */
	public void setListaPeriodicidadeInicializacaoContagem(List<SelectItem> listaPeriodicidadeInicializacaoContagem){
		this.listaPeriodicidadeInicializacaoContagem = listaPeriodicidadeInicializacaoContagem;
	}

	/**
	 * Get: listaPeriodicidadeInicializacaoContagemHash.
	 *
	 * @return listaPeriodicidadeInicializacaoContagemHash
	 */
	public Map<Integer, String> getListaPeriodicidadeInicializacaoContagemHash() {
		return listaPeriodicidadeInicializacaoContagemHash;
	}

	/**
	 * Set lista periodicidade inicializacao contagem hash.
	 *
	 * @param listaPeriodicidadeInicializacaoContagemHash the lista periodicidade inicializacao contagem hash
	 */
	public void setListaPeriodicidadeInicializacaoContagemHash(
					Map<Integer, String> listaPeriodicidadeInicializacaoContagemHash) {
		this.listaPeriodicidadeInicializacaoContagemHash = listaPeriodicidadeInicializacaoContagemHash;
	}

	/**
	 * Get: cboMeioTransmissaoAlternativoRemessa.
	 *
	 * @return cboMeioTransmissaoAlternativoRemessa
	 */
	public Integer getCboMeioTransmissaoAlternativoRemessa() {
		return cboMeioTransmissaoAlternativoRemessa;
	}

	/**
	 * Set: cboMeioTransmissaoAlternativoRemessa.
	 *
	 * @param cboMeioTransmissaoAlternativoRemessa the cbo meio transmissao alternativo remessa
	 */
	public void setCboMeioTransmissaoAlternativoRemessa(Integer cboMeioTransmissaoAlternativoRemessa) {
		this.cboMeioTransmissaoAlternativoRemessa = cboMeioTransmissaoAlternativoRemessa;
	}

	/**
	 * Get: cboMeioTransmissaoAlternativoRetorno.
	 *
	 * @return cboMeioTransmissaoAlternativoRetorno
	 */
	public Integer getCboMeioTransmissaoAlternativoRetorno() {
		return cboMeioTransmissaoAlternativoRetorno;
	}

	/**
	 * Set: cboMeioTransmissaoAlternativoRetorno.
	 *
	 * @param cboMeioTransmissaoAlternativoRetorno the cbo meio transmissao alternativo retorno
	 */
	public void setCboMeioTransmissaoAlternativoRetorno(Integer cboMeioTransmissaoAlternativoRetorno) {
		this.cboMeioTransmissaoAlternativoRetorno = cboMeioTransmissaoAlternativoRetorno;
	}

	/**
	 * Get: cboMeioTransmissaoPrincipalRemessa.
	 *
	 * @return cboMeioTransmissaoPrincipalRemessa
	 */
	public Integer getCboMeioTransmissaoPrincipalRemessa() {
		return cboMeioTransmissaoPrincipalRemessa;
	}

	/**
	 * Set: cboMeioTransmissaoPrincipalRemessa.
	 *
	 * @param cboMeioTransmissaoPrincipalRemessa the cbo meio transmissao principal remessa
	 */
	public void setCboMeioTransmissaoPrincipalRemessa(Integer cboMeioTransmissaoPrincipalRemessa) {
		this.cboMeioTransmissaoPrincipalRemessa = cboMeioTransmissaoPrincipalRemessa;
	}

	/**
	 * Get: cboMeioTransmissaoPrincipalRetorno.
	 *
	 * @return cboMeioTransmissaoPrincipalRetorno
	 */
	public Integer getCboMeioTransmissaoPrincipalRetorno() {
		return cboMeioTransmissaoPrincipalRetorno;
	}

	/**
	 * Set: cboMeioTransmissaoPrincipalRetorno.
	 *
	 * @param cboMeioTransmissaoPrincipalRetorno the cbo meio transmissao principal retorno
	 */
	public void setCboMeioTransmissaoPrincipalRetorno(Integer cboMeioTransmissaoPrincipalRetorno) {
		this.cboMeioTransmissaoPrincipalRetorno = cboMeioTransmissaoPrincipalRetorno;
	}

	/**
	 * Get: listaMeioTransmissao.
	 *
	 * @return listaMeioTransmissao
	 */
	public List<SelectItem> getListaMeioTransmissao() {
		return listaMeioTransmissao;
	}

	/**
	 * Set: listaMeioTransmissao.
	 *
	 * @param listaMeioTransmissao the lista meio transmissao
	 */
	public void setListaMeioTransmissao(List<SelectItem> listaMeioTransmissao) {
		this.listaMeioTransmissao = listaMeioTransmissao;
	}

	/**
	 * Get: listaMeioTransmissaoHash.
	 *
	 * @return listaMeioTransmissaoHash
	 */
	public Map<Integer, String> getListaMeioTransmissaoHash() {
		return listaMeioTransmissaoHash;
	}

	/**
	 * Set lista meio transmissao hash.
	 *
	 * @param listaMeioTransmissaoHash the lista meio transmissao hash
	 */
	public void setListaMeioTransmissaoHash(Map<Integer, String> listaMeioTransmissaoHash) {
		this.listaMeioTransmissaoHash = listaMeioTransmissaoHash;
	}

	/**
	 * Get: listaEmpresaResponsavelTransmissao.
	 *
	 * @return listaEmpresaResponsavelTransmissao
	 */
	public List<SelectItem> getListaEmpresaResponsavelTransmissao() {
		return listaEmpresaResponsavelTransmissao;
	}

	/**
	 * Set: listaEmpresaResponsavelTransmissao.
	 *
	 * @param listaEmpresaResponsavelTransmissao the lista empresa responsavel transmissao
	 */
	public void setListaEmpresaResponsavelTransmissao(List<SelectItem> listaEmpresaResponsavelTransmissao) {
		this.listaEmpresaResponsavelTransmissao = listaEmpresaResponsavelTransmissao;
	}

	/**
	 * Get: listaEmpresaResponsavelTransmissaoHash.
	 *
	 * @return listaEmpresaResponsavelTransmissaoHash
	 */
	public Map<Long, String> getListaEmpresaResponsavelTransmissaoHash() {
		return listaEmpresaResponsavelTransmissaoHash;
	}

	/**
	 * Set lista empresa responsavel transmissao hash.
	 *
	 * @param listaEmpresaResponsavelTransmissaoHash the lista empresa responsavel transmissao hash
	 */
	public void setListaEmpresaResponsavelTransmissaoHash(Map<Long, String> 
	listaEmpresaResponsavelTransmissaoHash) {
		this.listaEmpresaResponsavelTransmissaoHash = listaEmpresaResponsavelTransmissaoHash;
	}

	/**
	 * Get: itemSelecionadoHistorico.
	 *
	 * @return itemSelecionadoHistorico
	 */
	public Integer getItemSelecionadoHistorico() {
		return itemSelecionadoHistorico;
	}

	/**
	 * Set: itemSelecionadoHistorico.
	 *
	 * @param itemSelecionadoHistorico the item selecionado historico
	 */
	public void setItemSelecionadoHistorico(Integer itemSelecionadoHistorico) {
		this.itemSelecionadoHistorico = itemSelecionadoHistorico;
	}

	/**
	 * Get: listaControleHistorico.
	 *
	 * @return listaControleHistorico
	 */
	public List<SelectItem> getListaControleHistorico() {
		return listaControleHistorico;
	}

	/**
	 * Set: listaControleHistorico.
	 *
	 * @param listaControleHistorico the lista controle historico
	 */
	public void setListaControleHistorico(List<SelectItem> listaControleHistorico) {
		this.listaControleHistorico = listaControleHistorico;
	}

	/**
	 * Get: listaGridHistorico.
	 *
	 * @return listaGridHistorico
	 */
	public List<ConsultarHistoricoPerfilTrocaArquivoSaidaDTO> getListaGridHistorico() {
		return listaGridHistorico;
	}

	/**
	 * Set: listaGridHistorico.
	 *
	 * @param listaGridHistorico the lista grid historico
	 */
	public void setListaGridHistorico(List<ConsultarHistoricoPerfilTrocaArquivoSaidaDTO> listaGridHistorico) {
		this.listaGridHistorico = listaGridHistorico;
	}

	/**
	 * Get: tipoAcao.
	 *
	 * @return tipoAcao
	 */
	public String getTipoAcao() {
		return tipoAcao;
	}

	/**
	 * Set: tipoAcao.
	 *
	 * @param tipoAcao the tipo acao
	 */
	public void setTipoAcao(String tipoAcao) {
		this.tipoAcao = tipoAcao;
	}

	/**
	 * Get: quantidadeRegistrosInconsistentesFormatada.
	 *
	 * @return quantidadeRegistrosInconsistentesFormatada
	 */
	public String getQuantidadeRegistrosInconsistentesFormatada() {
		return quantidadeRegistrosInconsistentesFormatada;
	}

	/**
	 * Set: quantidadeRegistrosInconsistentesFormatada.
	 *
	 * @param quantidadeRegistrosInconsistentesFormatada the quantidade registros inconsistentes formatada
	 */
	public void setQuantidadeRegistrosInconsistentesFormatada(String quantidadeRegistrosInconsistentesFormatada){
		this.quantidadeRegistrosInconsistentesFormatada = quantidadeRegistrosInconsistentesFormatada;
	}

	/**
	 * Is disable argumentos consulta historico.
	 *
	 * @return true, if is disable argumentos consulta historico
	 */
	public boolean isDisableArgumentosConsultaHistorico() {
		return disableArgumentosConsultaHistorico;
	}

	/**
	 * Set: disableArgumentosConsultaHistorico.
	 *
	 * @param disableArgumentosConsultaHistorico the disable argumentos consulta historico
	 */
	public void setDisableArgumentosConsultaHistorico(boolean disableArgumentosConsultaHistorico) {
		this.disableArgumentosConsultaHistorico = disableArgumentosConsultaHistorico;
	}

	/**
	 * Get: cdSerieAplicTranmicao.
	 *
	 * @return cdSerieAplicTranmicao
	 */
	public String getCdSerieAplicTranmicao() {
		return cdSerieAplicTranmicao;
	}

	/**
	 * Set: cdSerieAplicTranmicao.
	 *
	 * @param cdSerieAplicTranmicao the cd serie aplic tranmicao
	 */
	public void setCdSerieAplicTranmicao(String cdSerieAplicTranmicao) {
		this.cdSerieAplicTranmicao = cdSerieAplicTranmicao;
	}

	/**
	 * Get: tipoFiltroSelecionado.
	 *
	 * @return tipoFiltroSelecionado
	 */
	public String getTipoFiltroSelecionado() {
		return tipoFiltroSelecionado;
	}

	/**
	 * Set: tipoFiltroSelecionado.
	 *
	 * @param tipoFiltroSelecionado the tipo filtro selecionado
	 */
	public void setTipoFiltroSelecionado(String tipoFiltroSelecionado) {
		this.tipoFiltroSelecionado = tipoFiltroSelecionado;
	}

	/**
	 * Is desabilita radio.
	 *
	 * @return true, if is desabilita radio
	 */
	public boolean isDesabilitaRadio() {
		return desabilitaRadio;
	}

	/**
	 * Set: desabilitaRadio.
	 *
	 * @param desabilitaRadio the desabilita radio
	 */
	public void setDesabilitaRadio(boolean desabilitaRadio) {
		this.desabilitaRadio = desabilitaRadio;
	}

	/**
	 * Get: listaTipoControleRetorno.
	 *
	 * @return listaTipoControleRetorno
	 */
	public List<SelectItem> getListaTipoControleRetorno() {
		return listaTipoControleRetorno;
	}

	/**
	 * Set: listaTipoControleRetorno.
	 *
	 * @param listaTipoControleRetorno the lista tipo controle retorno
	 */
	public void setListaTipoControleRetorno(List<SelectItem> listaTipoControleRetorno) {
		this.listaTipoControleRetorno = listaTipoControleRetorno;
	}

	/**
	 * Get: listaTipoControleRetornoHash.
	 *
	 * @return listaTipoControleRetornoHash
	 */
	public Map<Integer, String> getListaTipoControleRetornoHash() {
		return listaTipoControleRetornoHash;
	}

	/**
	 * Set lista tipo controle retorno hash.
	 *
	 * @param listaTipoControleRetornoHash the lista tipo controle retorno hash
	 */
	public void setListaTipoControleRetornoHash(Map<Integer, String> listaTipoControleRetornoHash) {
		this.listaTipoControleRetornoHash = listaTipoControleRetornoHash;
	}

	/**
	 * Get: listaNivelControleRetorno.
	 *
	 * @return listaNivelControleRetorno
	 */
	public List<SelectItem> getListaNivelControleRetorno() {
		return listaNivelControleRetorno;
	}

	/**
	 * Set: listaNivelControleRetorno.
	 *
	 * @param listaNivelControleRetorno the lista nivel controle retorno
	 */
	public void setListaNivelControleRetorno(List<SelectItem> listaNivelControleRetorno) {
		this.listaNivelControleRetorno = listaNivelControleRetorno;
	}

	/**
	 * Get: listaNivelControleRetornoHash.
	 *
	 * @return listaNivelControleRetornoHash
	 */
	public Map<Integer, String> getListaNivelControleRetornoHash() {
		return listaNivelControleRetornoHash;
	}

	/**
	 * Set lista nivel controle retorno hash.
	 *
	 * @param listaNivelControleRetornoHash the lista nivel controle retorno hash
	 */
	public void setListaNivelControleRetornoHash(Map<Integer, String> listaNivelControleRetornoHash) {
		this.listaNivelControleRetornoHash = listaNivelControleRetornoHash;
	}

	/**
	 * Get: rdoVan.
	 *
	 * @return rdoVan
	 */
	public String getRdoVan() {
		return rdoVan;
	}

	/**
	 * Set: rdoVan.
	 *
	 * @param rdoVan the rdo van
	 */
	public void setRdoVan(String rdoVan) {
		this.rdoVan = rdoVan;
	}

	/**
	 * Get: cboEmpresaRespTransDadosContr.
	 *
	 * @return cboEmpresaRespTransDadosContr
	 */
	public Long getCboEmpresaRespTransDadosContr() {
		return cboEmpresaRespTransDadosContr;
	}

	/**
	 * Set: cboEmpresaRespTransDadosContr.
	 *
	 * @param cboEmpresaRespTransDadosContr the cbo empresa resp trans dados contr
	 */
	public void setCboEmpresaRespTransDadosContr(Long cboEmpresaRespTransDadosContr) {
		this.cboEmpresaRespTransDadosContr = cboEmpresaRespTransDadosContr;
	}

	/**
	 * Get: cboRespCustoTransArqDadosContr.
	 *
	 * @return cboRespCustoTransArqDadosContr
	 */
	public Integer getCboRespCustoTransArqDadosContr() {
		return cboRespCustoTransArqDadosContr;
	}

	/**
	 * Set: cboRespCustoTransArqDadosContr.
	 *
	 * @param cboRespCustoTransArqDadosContr the cbo resp custo trans arq dados contr
	 */
	public void setCboRespCustoTransArqDadosContr(Integer cboRespCustoTransArqDadosContr) {
		this.cboRespCustoTransArqDadosContr = cboRespCustoTransArqDadosContr;
	}

	/**
	 * Get: txtPercentualCustoTransArq.
	 *
	 * @return txtPercentualCustoTransArq
	 */
	public BigDecimal getTxtPercentualCustoTransArq() {
		return txtPercentualCustoTransArq;
	}

	/**
	 * Set: txtPercentualCustoTransArq.
	 *
	 * @param txtPercentualCustoTransArq the txt percentual custo trans arq
	 */
	public void setTxtPercentualCustoTransArq(BigDecimal txtPercentualCustoTransArq) {
		this.txtPercentualCustoTransArq = txtPercentualCustoTransArq;
	}

	/**
	 * Get: listaResponsavelCustoTransArq.
	 *
	 * @return listaResponsavelCustoTransArq
	 */
	public List<SelectItem> getListaResponsavelCustoTransArq() {
		return listaResponsavelCustoTransArq;
	}

	/**
	 * Set: listaResponsavelCustoTransArq.
	 *
	 * @param listaResponsavelCustoTransArq the lista responsavel custo trans arq
	 */
	public void setListaResponsavelCustoTransArq(List<SelectItem> listaResponsavelCustoTransArq) {
		this.listaResponsavelCustoTransArq = listaResponsavelCustoTransArq;
	}

	/**
	 * Get: listaResponsavelCustoTransArqHash.
	 *
	 * @return listaResponsavelCustoTransArqHash
	 */
	public Map<Integer, String> getListaResponsavelCustoTransArqHash() {
		return listaResponsavelCustoTransArqHash;
	}

	/**
	 * Set lista responsavel custo trans arq hash.
	 *
	 * @param listaResponsavelCustoTransArqHash the lista responsavel custo trans arq hash
	 */
	public void setListaResponsavelCustoTransArqHash(Map<Integer, String> listaResponsavelCustoTransArqHash) {
		this.listaResponsavelCustoTransArqHash = listaResponsavelCustoTransArqHash;
	}

	/**
	 * Get: rdoAplicFormProprio.
	 *
	 * @return rdoAplicFormProprio
	 */
	public String getRdoAplicFormProprio() {
		return rdoAplicFormProprio;
	}

	/**
	 * Set: rdoAplicFormProprio.
	 *
	 * @param rdoAplicFormProprio the rdo aplic form proprio
	 */
	public void setRdoAplicFormProprio(String rdoAplicFormProprio) {
		this.rdoAplicFormProprio = rdoAplicFormProprio;
	}

	/**
	 * Get: responsavelCustoTransmissaoArquivo.
	 *
	 * @return responsavelCustoTransmissaoArquivo
	 */
	public String getResponsavelCustoTransmissaoArquivo() {
		return responsavelCustoTransmissaoArquivo;
	}

	/**
	 * Set: responsavelCustoTransmissaoArquivo.
	 *
	 * @param responsavelCustoTransmissaoArquivo the responsavel custo transmissao arquivo
	 */
	public void setResponsavelCustoTransmissaoArquivo(String responsavelCustoTransmissaoArquivo) {
		this.responsavelCustoTransmissaoArquivo = responsavelCustoTransmissaoArquivo;
	}

	/**
	 * Is disable aplicativo formatacao.
	 *
	 * @return true, if is disable aplicativo formatacao
	 */
	public boolean isDisableAplicativoFormatacao() {
		return disableAplicativoFormatacao;
	}

	/**
	 * Set: disableAplicativoFormatacao.
	 *
	 * @param disableAplicativoFormatacao the disable aplicativo formatacao
	 */
	public void setDisableAplicativoFormatacao(boolean disableAplicativoFormatacao) {
		this.disableAplicativoFormatacao = disableAplicativoFormatacao;
	}

	/**
	 * Is disable tipo layout arquivo.
	 *
	 * @return true, if is disable tipo layout arquivo
	 */
	public boolean isDisableTipoLayoutArquivo() {
		return disableTipoLayoutArquivo;
	}

	/**
	 * Set: disableTipoLayoutArquivo.
	 *
	 * @param disableTipoLayoutArquivo the disable tipo layout arquivo
	 */
	public void setDisableTipoLayoutArquivo(boolean disableTipoLayoutArquivo) {
		this.disableTipoLayoutArquivo = disableTipoLayoutArquivo;
	}

	/**
	 * Get: listaTipoControleRemessa.
	 *
	 * @return listaTipoControleRemessa
	 */
	public List<SelectItem> getListaTipoControleRemessa() {
		return listaTipoControleRemessa;
	}

	/**
	 * Set: listaTipoControleRemessa.
	 *
	 * @param listaTipoControleRemessa the lista tipo controle remessa
	 */
	public void setListaTipoControleRemessa(
			List<SelectItem> listaTipoControleRemessa) {
		this.listaTipoControleRemessa = listaTipoControleRemessa;
	}

	/**
	 * Get: listaTipoControleRemessaHash.
	 *
	 * @return listaTipoControleRemessaHash
	 */
	public Map<Integer, String> getListaTipoControleRemessaHash() {
		return listaTipoControleRemessaHash;
	}

	/**
	 * Set lista tipo controle remessa hash.
	 *
	 * @param listaTipoControleRemessaHash the lista tipo controle remessa hash
	 */
	public void setListaTipoControleRemessaHash(
			Map<Integer, String> listaTipoControleRemessaHash) {
		this.listaTipoControleRemessaHash = listaTipoControleRemessaHash;
	}

	/**
	 * Get: empresaGestoraFiltro.
	 *
	 * @return empresaGestoraFiltro
	 */
	public Long getEmpresaGestoraFiltro() {
		return empresaGestoraFiltro;
	}

	/**
	 * Set: empresaGestoraFiltro.
	 *
	 * @param empresaGestoraFiltro the empresa gestora filtro
	 */
	public void setEmpresaGestoraFiltro(Long empresaGestoraFiltro) {
		this.empresaGestoraFiltro = empresaGestoraFiltro;
	}

	/**
	 * Get: tipoContratoFiltro.
	 *
	 * @return tipoContratoFiltro
	 */
	public Integer getTipoContratoFiltro() {
		return tipoContratoFiltro;
	}

	/**
	 * Set: tipoContratoFiltro.
	 *
	 * @param tipoContratoFiltro the tipo contrato filtro
	 */
	public void setTipoContratoFiltro(Integer tipoContratoFiltro) {
		this.tipoContratoFiltro = tipoContratoFiltro;
	}

	/**
	 * Get: numeroFiltro.
	 *
	 * @return numeroFiltro
	 */
	public String getNumeroFiltro() {
		return numeroFiltro;
	}

	/**
	 * Set: numeroFiltro.
	 *
	 * @param numeroFiltro the numero filtro
	 */
	public void setNumeroFiltro(String numeroFiltro) {
		this.numeroFiltro = numeroFiltro;
	}

	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: listaEmpresaGestoraHash.
	 *
	 * @return listaEmpresaGestoraHash
	 */
	public Map<Long, String> getListaEmpresaGestoraHash() {
		return listaEmpresaGestoraHash;
	}

	/**
	 * Set lista empresa gestora hash.
	 *
	 * @param listaEmpresaGestoraHash the lista empresa gestora hash
	 */
	public void setListaEmpresaGestoraHash(Map<Long, String> listaEmpresaGestoraHash) {
		this.listaEmpresaGestoraHash = listaEmpresaGestoraHash;
	}

	/**
	 * Get: listaTipoContrato.
	 *
	 * @return listaTipoContrato
	 */
	public List<SelectItem> getListaTipoContrato() {
		return listaTipoContrato;
	}

	/**
	 * Set: listaTipoContrato.
	 *
	 * @param listaTipoContrato the lista tipo contrato
	 */
	public void setListaTipoContrato(List<SelectItem> listaTipoContrato) {
		this.listaTipoContrato = listaTipoContrato;
	}

	/**
	 * Get: filtroIdentificaoService.
	 *
	 * @return filtroIdentificaoService
	 */
	public IFiltroIdentificaoService getFiltroIdentificaoService() {
		return filtroIdentificaoService;
	}

	/**
	 * Set: filtroIdentificaoService.
	 *
	 * @param filtroIdentificaoService the filtro identificao service
	 */
	public void setFiltroIdentificaoService(
			IFiltroIdentificaoService filtroIdentificaoService) {
		this.filtroIdentificaoService = filtroIdentificaoService;
	}

	/**
	 * Get: listaConsultarListaContratosPessoasPerfil.
	 *
	 * @return listaConsultarListaContratosPessoasPerfil
	 */
	public List<ConsultarListaContratosPessoasSaidaDTO> getListaConsultarListaContratosPessoasPerfil() {
		return listaConsultarListaContratosPessoasPerfil;
	}

	/**
	 * Set: listaConsultarListaContratosPessoasPerfil.
	 *
	 * @param listaConsultarListaContratosPessoasPerfil the lista consultar lista contratos pessoas perfil
	 */
	public void setListaConsultarListaContratosPessoasPerfil(
			List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoasPerfil) {
		this.listaConsultarListaContratosPessoasPerfil = listaConsultarListaContratosPessoasPerfil;
	}

	/**
	 * Get: itemClienteSelecionadoContratoPerfil.
	 *
	 * @return itemClienteSelecionadoContratoPerfil
	 */
	public Integer getItemClienteSelecionadoContratoPerfil() {
		return itemClienteSelecionadoContratoPerfil;
	}

	/**
	 * Set: itemClienteSelecionadoContratoPerfil.
	 *
	 * @param itemClienteSelecionadoContratoPerfil the item cliente selecionado contrato perfil
	 */
	public void setItemClienteSelecionadoContratoPerfil(
			Integer itemClienteSelecionadoContratoPerfil) {
		this.itemClienteSelecionadoContratoPerfil = itemClienteSelecionadoContratoPerfil;
	}

	/**
	 * Get: listaControleRadioContratosPessoasPerfil.
	 *
	 * @return listaControleRadioContratosPessoasPerfil
	 */
	public List<SelectItem> getListaControleRadioContratosPessoasPerfil() {
		return listaControleRadioContratosPessoasPerfil;
	}

	/**
	 * Set: listaControleRadioContratosPessoasPerfil.
	 *
	 * @param listaControleRadioContratosPessoasPerfil the lista controle radio contratos pessoas perfil
	 */
	public void setListaControleRadioContratosPessoasPerfil(
			List<SelectItem> listaControleRadioContratosPessoasPerfil) {
		this.listaControleRadioContratosPessoasPerfil = listaControleRadioContratosPessoasPerfil;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 *
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 *
	 * @param cdPessoaJuridicaContrato the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 *
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 *
	 * @param nrSequenciaContratoNegocio the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Is cliente contrato selecionado.
	 *
	 * @return true, if is cliente contrato selecionado
	 */
	public boolean isClienteContratoSelecionado() {
		return clienteContratoSelecionado;
	}

	/**
	 * Set: clienteContratoSelecionado.
	 *
	 * @param clienteContratoSelecionado the cliente contrato selecionado
	 */
	public void setClienteContratoSelecionado(boolean clienteContratoSelecionado) {
		this.clienteContratoSelecionado = clienteContratoSelecionado;
	}

	/**
	 * Get: empresaGestoraDescContrato.
	 *
	 * @return empresaGestoraDescContrato
	 */
	public String getEmpresaGestoraDescContrato() {
		return empresaGestoraDescContrato;
	}

	/**
	 * Set: empresaGestoraDescContrato.
	 *
	 * @param empresaGestoraDescContrato the empresa gestora desc contrato
	 */
	public void setEmpresaGestoraDescContrato(String empresaGestoraDescContrato) {
		this.empresaGestoraDescContrato = empresaGestoraDescContrato;
	}

	/**
	 * Get: numeroDescContrato.
	 *
	 * @return numeroDescContrato
	 */
	public String getNumeroDescContrato() {
		return numeroDescContrato;
	}

	/**
	 * Set: numeroDescContrato.
	 *
	 * @param numeroDescContrato the numero desc contrato
	 */
	public void setNumeroDescContrato(String numeroDescContrato) {
		this.numeroDescContrato = numeroDescContrato;
	}

	/**
	 * Get: descricaoTipoContratoDescContrato.
	 *
	 * @return descricaoTipoContratoDescContrato
	 */
	public String getDescricaoTipoContratoDescContrato() {
		return descricaoTipoContratoDescContrato;
	}

	/**
	 * Set: descricaoTipoContratoDescContrato.
	 *
	 * @param descricaoTipoContratoDescContrato the descricao tipo contrato desc contrato
	 */
	public void setDescricaoTipoContratoDescContrato(
			String descricaoTipoContratoDescContrato) {
		this.descricaoTipoContratoDescContrato = descricaoTipoContratoDescContrato;
	}

	/**
	 * Get: descricaoContratoDescContrato.
	 *
	 * @return descricaoContratoDescContrato
	 */
	public String getDescricaoContratoDescContrato() {
		return descricaoContratoDescContrato;
	}

	/**
	 * Set: descricaoContratoDescContrato.
	 *
	 * @param descricaoContratoDescContrato the descricao contrato desc contrato
	 */
	public void setDescricaoContratoDescContrato(
			String descricaoContratoDescContrato) {
		this.descricaoContratoDescContrato = descricaoContratoDescContrato;
	}

	/**
	 * Get: situacaoDescContrato.
	 *
	 * @return situacaoDescContrato
	 */
	public String getSituacaoDescContrato() {
		return situacaoDescContrato;
	}

	/**
	 * Set: situacaoDescContrato.
	 *
	 * @param situacaoDescContrato the situacao desc contrato
	 */
	public void setSituacaoDescContrato(String situacaoDescContrato) {
		this.situacaoDescContrato = situacaoDescContrato;
	}

	/**
	 * Get: telaVoltarIdentificacaoClienteContrato.
	 *
	 * @return telaVoltarIdentificacaoClienteContrato
	 */
	public String getTelaVoltarIdentificacaoClienteContrato() {
		return telaVoltarIdentificacaoClienteContrato;
	}

	/**
	 * Set: telaVoltarIdentificacaoClienteContrato.
	 *
	 * @param telaVoltarIdentificacaoClienteContrato the tela voltar identificacao cliente contrato
	 */
	public void setTelaVoltarIdentificacaoClienteContrato(
			String telaVoltarIdentificacaoClienteContrato) {
		this.telaVoltarIdentificacaoClienteContrato = telaVoltarIdentificacaoClienteContrato;
	}

	/**
	 * Get: telaVoltarIdentificacaoPerfil.
	 *
	 * @return telaVoltarIdentificacaoPerfil
	 */
	public String getTelaVoltarIdentificacaoPerfil() {
		return telaVoltarIdentificacaoPerfil;
	}

	/**
	 * Set: telaVoltarIdentificacaoPerfil.
	 *
	 * @param telaVoltarIdentificacaoPerfil the tela voltar identificacao perfil
	 */
	public void setTelaVoltarIdentificacaoPerfil(
			String telaVoltarIdentificacaoPerfil) {
		this.telaVoltarIdentificacaoPerfil = telaVoltarIdentificacaoPerfil;
	}

	/**
	 * Get: cdPessoaPerfil.
	 *
	 * @return cdPessoaPerfil
	 */
	public Long getCdPessoaPerfil() {
		return cdPessoaPerfil;
	}

	/**
	 * Set: cdPessoaPerfil.
	 *
	 * @param cdPessoaPerfil the cd pessoa perfil
	 */
	public void setCdPessoaPerfil(Long cdPessoaPerfil) {
		this.cdPessoaPerfil = cdPessoaPerfil;
	}

	/**
	 * Get: cdCpfCnpjPessoaPerfil.
	 *
	 * @return cdCpfCnpjPessoaPerfil
	 */
	public String getCdCpfCnpjPessoaPerfil() {
		return cdCpfCnpjPessoaPerfil;
	}

	/**
	 * Set: cdCpfCnpjPessoaPerfil.
	 *
	 * @param cdCpfCnpjPessoaPerfil the cd cpf cnpj pessoa perfil
	 */
	public void setCdCpfCnpjPessoaPerfil(String cdCpfCnpjPessoaPerfil) {
		this.cdCpfCnpjPessoaPerfil = cdCpfCnpjPessoaPerfil;
	}

	/**
	 * Get: dsPessoaPerfil.
	 *
	 * @return dsPessoaPerfil
	 */
	public String getDsPessoaPerfil() {
		return dsPessoaPerfil;
	}

	/**
	 * Set: dsPessoaPerfil.
	 *
	 * @param dsPessoaPerfil the ds pessoa perfil
	 */
	public void setDsPessoaPerfil(String dsPessoaPerfil) {
		this.dsPessoaPerfil = dsPessoaPerfil;
	}

	/**
	 * Get: cdPerfilTrocaArquivoPerfilCliente.
	 *
	 * @return cdPerfilTrocaArquivoPerfilCliente
	 */
	public Long getCdPerfilTrocaArquivoPerfilCliente() {
		return cdPerfilTrocaArquivoPerfilCliente;
	}

	/**
	 * Set: cdPerfilTrocaArquivoPerfilCliente.
	 *
	 * @param cdPerfilTrocaArquivoPerfilCliente the cd perfil troca arquivo perfil cliente
	 */
	public void setCdPerfilTrocaArquivoPerfilCliente(
			Long cdPerfilTrocaArquivoPerfilCliente) {
		this.cdPerfilTrocaArquivoPerfilCliente = cdPerfilTrocaArquivoPerfilCliente;
	}

	/**
	 * Get: dsSituacaoPerfilCliente.
	 *
	 * @return dsSituacaoPerfilCliente
	 */
	public String getDsSituacaoPerfilCliente() {
		return dsSituacaoPerfilCliente;
	}

	/**
	 * Set: dsSituacaoPerfilCliente.
	 *
	 * @param dsSituacaoPerfilCliente the ds situacao perfil cliente
	 */
	public void setDsSituacaoPerfilCliente(String dsSituacaoPerfilCliente) {
		this.dsSituacaoPerfilCliente = dsSituacaoPerfilCliente;
	}

	/**
	 * Get: cdPerfilTrocaArquivoPerfilContrato.
	 *
	 * @return cdPerfilTrocaArquivoPerfilContrato
	 */
	public Long getCdPerfilTrocaArquivoPerfilContrato() {
		return cdPerfilTrocaArquivoPerfilContrato;
	}

	/**
	 * Set: cdPerfilTrocaArquivoPerfilContrato.
	 *
	 * @param cdPerfilTrocaArquivoPerfilContrato the cd perfil troca arquivo perfil contrato
	 */
	public void setCdPerfilTrocaArquivoPerfilContrato(
			Long cdPerfilTrocaArquivoPerfilContrato) {
		this.cdPerfilTrocaArquivoPerfilContrato = cdPerfilTrocaArquivoPerfilContrato;
	}

	/**
	 * Get: dsSituacaoPerfilContrato.
	 *
	 * @return dsSituacaoPerfilContrato
	 */
	public String getDsSituacaoPerfilContrato() {
		return dsSituacaoPerfilContrato;
	}

	/**
	 * Set: dsSituacaoPerfilContrato.
	 *
	 * @param dsSituacaoPerfilContrato the ds situacao perfil contrato
	 */
	public void setDsSituacaoPerfilContrato(String dsSituacaoPerfilContrato) {
		this.dsSituacaoPerfilContrato = dsSituacaoPerfilContrato;
	}

	/**
	 * Get: listaContratoClientePerfil.
	 *
	 * @return listaContratoClientePerfil
	 */
	public List<ConsultarContratoClienteFiltroSaidaDTO> getListaContratoClientePerfil() {
		return listaContratoClientePerfil;
	}

	/**
	 * Set: listaContratoClientePerfil.
	 *
	 * @param listaContratoClientePerfil the lista contrato cliente perfil
	 */
	public void setListaContratoClientePerfil(
			List<ConsultarContratoClienteFiltroSaidaDTO> listaContratoClientePerfil) {
		this.listaContratoClientePerfil = listaContratoClientePerfil;
	}

	/**
	 * Get: itemSelecionadoListaContratoClientePerfil.
	 *
	 * @return itemSelecionadoListaContratoClientePerfil
	 */
	public Integer getItemSelecionadoListaContratoClientePerfil() {
		return itemSelecionadoListaContratoClientePerfil;
	}

	/**
	 * Set: itemSelecionadoListaContratoClientePerfil.
	 *
	 * @param itemSelecionadoListaContratoClientePerfil the item selecionado lista contrato cliente perfil
	 */
	public void setItemSelecionadoListaContratoClientePerfil(
			Integer itemSelecionadoListaContratoClientePerfil) {
		this.itemSelecionadoListaContratoClientePerfil = itemSelecionadoListaContratoClientePerfil;
	}

	/**
	 * Get: listaControleRadioContratoClientePerfil.
	 *
	 * @return listaControleRadioContratoClientePerfil
	 */
	public List<SelectItem> getListaControleRadioContratoClientePerfil() {
		return listaControleRadioContratoClientePerfil;
	}

	/**
	 * Set: listaControleRadioContratoClientePerfil.
	 *
	 * @param listaControleRadioContratoClientePerfil the lista controle radio contrato cliente perfil
	 */
	public void setListaControleRadioContratoClientePerfil(
			List<SelectItem> listaControleRadioContratoClientePerfil) {
		this.listaControleRadioContratoClientePerfil = listaControleRadioContratoClientePerfil;
	}

	/**
	 * Get: listarPerfilTrocaArquivo.
	 *
	 * @return listarPerfilTrocaArquivo
	 */
	public List<ListarPerfilTrocaArquivoSaidaDTO> getListarPerfilTrocaArquivo() {
		return listarPerfilTrocaArquivo;
	}

	/**
	 * Set: listarPerfilTrocaArquivo.
	 *
	 * @param listarPerfilTrocaArquivo the listar perfil troca arquivo
	 */
	public void setListarPerfilTrocaArquivo(
			List<ListarPerfilTrocaArquivoSaidaDTO> listarPerfilTrocaArquivo) {
		this.listarPerfilTrocaArquivo = listarPerfilTrocaArquivo;
	}

	/**
	 * Get: itemSelecionadoListaPerfilTroca.
	 *
	 * @return itemSelecionadoListaPerfilTroca
	 */
	public Integer getItemSelecionadoListaPerfilTroca() {
		return itemSelecionadoListaPerfilTroca;
	}

	/**
	 * Set: itemSelecionadoListaPerfilTroca.
	 *
	 * @param itemSelecionadoListaPerfilTroca the item selecionado lista perfil troca
	 */
	public void setItemSelecionadoListaPerfilTroca(
			Integer itemSelecionadoListaPerfilTroca) {
		this.itemSelecionadoListaPerfilTroca = itemSelecionadoListaPerfilTroca;
	}

	/**
	 * Get: listaControleRadioPerfilTroca.
	 *
	 * @return listaControleRadioPerfilTroca
	 */
	public List<SelectItem> getListaControleRadioPerfilTroca() {
		return listaControleRadioPerfilTroca;
	}

	/**
	 * Set: listaControleRadioPerfilTroca.
	 *
	 * @param listaControleRadioPerfilTroca the lista controle radio perfil troca
	 */
	public void setListaControleRadioPerfilTroca(
			List<SelectItem> listaControleRadioPerfilTroca) {
		this.listaControleRadioPerfilTroca = listaControleRadioPerfilTroca;
	}

	/**
	 * Get: paginaRetorno.
	 *
	 * @return paginaRetorno
	 */
	public String getPaginaRetorno() {
		return paginaRetorno;
	}

	/**
	 * Set: paginaRetorno.
	 *
	 * @param paginaRetorno the pagina retorno
	 */
	public void setPaginaRetorno(String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}

	/**
	 * Get: listaConsultarListaClientePessoasPerfil.
	 *
	 * @return listaConsultarListaClientePessoasPerfil
	 */
	public List<ConsultarListaClientePessoasSaidaDTO> getListaConsultarListaClientePessoasPerfil() {
		return listaConsultarListaClientePessoasPerfil;
	}

	/**
	 * Set: listaConsultarListaClientePessoasPerfil.
	 *
	 * @param listaConsultarListaClientePessoasPerfil the lista consultar lista cliente pessoas perfil
	 */
	public void setListaConsultarListaClientePessoasPerfil(
			List<ConsultarListaClientePessoasSaidaDTO> listaConsultarListaClientePessoasPerfil) {
		this.listaConsultarListaClientePessoasPerfil = listaConsultarListaClientePessoasPerfil;
	}

	/**
	 * Get: itemSelecionadoListaClientePerfil.
	 *
	 * @return itemSelecionadoListaClientePerfil
	 */
	public Integer getItemSelecionadoListaClientePerfil() {
		return itemSelecionadoListaClientePerfil;
	}

	/**
	 * Set: itemSelecionadoListaClientePerfil.
	 *
	 * @param itemSelecionadoListaClientePerfil the item selecionado lista cliente perfil
	 */
	public void setItemSelecionadoListaClientePerfil(
			Integer itemSelecionadoListaClientePerfil) {
		this.itemSelecionadoListaClientePerfil = itemSelecionadoListaClientePerfil;
	}

	/**
	 * Get: listaControleRadioClientePerfil.
	 *
	 * @return listaControleRadioClientePerfil
	 */
	public List<SelectItem> getListaControleRadioClientePerfil() {
		return listaControleRadioClientePerfil;
	}

	/**
	 * Set: listaControleRadioClientePerfil.
	 *
	 * @param listaControleRadioClientePerfil the lista controle radio cliente perfil
	 */
	public void setListaControleRadioClientePerfil(
			List<SelectItem> listaControleRadioClientePerfil) {
		this.listaControleRadioClientePerfil = listaControleRadioClientePerfil;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoImpl.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoImpl
	 */
	public IFiltroAgendamentoEfetivacaoEstornoService getFiltroAgendamentoEfetivacaoEstornoImpl() {
		return filtroAgendamentoEfetivacaoEstornoImpl;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoImpl.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoImpl the filtro agendamento efetivacao estorno impl
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoImpl(
			IFiltroAgendamentoEfetivacaoEstornoService filtroAgendamentoEfetivacaoEstornoImpl) {
		this.filtroAgendamentoEfetivacaoEstornoImpl = filtroAgendamentoEfetivacaoEstornoImpl;
	}

	
	/**
	 * Get: consultarListaPerfilTrocaArquivoSaida.
	 *
	 * @return consultarListaPerfilTrocaArquivoSaida
	 */
	public ConsultarListaPerfilTrocaArquivoSaidaDTO getConsultarListaPerfilTrocaArquivoSaida() {
		return consultarListaPerfilTrocaArquivoSaida;
	}

	/**
	 * Set: consultarListaPerfilTrocaArquivoSaida.
	 *
	 * @param consultarListaPerfilTrocaArquivoSaida the consultar lista perfil troca arquivo saida
	 */
	public void setConsultarListaPerfilTrocaArquivoSaida(
			ConsultarListaPerfilTrocaArquivoSaidaDTO consultarListaPerfilTrocaArquivoSaida) {
		this.consultarListaPerfilTrocaArquivoSaida = consultarListaPerfilTrocaArquivoSaida;
	}

	/**
	 * Get: listaGridConsultar.
	 *
	 * @return listaGridConsultar
	 */
	public List<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO> getListaGridConsultar() {
		return listaGridConsultar;
	}

	/**
	 * Set: listaGridConsultar.
	 *
	 * @param listaGridConsultar the lista grid consultar
	 */
	public void setListaGridConsultar(
			List<ConsultarListaPerfilTrocaArquivoOcorrenciaSaidaDTO> listaGridConsultar) {
		this.listaGridConsultar = listaGridConsultar;
	}

	/**
	 * Get: utilizaLayoutProprio.
	 *
	 * @return utilizaLayoutProprio
	 */
	public String getUtilizaLayoutProprio() {
		return utilizaLayoutProprio;
	}

	/**
	 * Set: utilizaLayoutProprio.
	 *
	 * @param utilizaLayoutProprio the utiliza layout proprio
	 */
	public void setUtilizaLayoutProprio(String utilizaLayoutProprio) {
		this.utilizaLayoutProprio = utilizaLayoutProprio;
	}

	/**
	 * Get: volumeMensalTraf.
	 *
	 * @return volumeMensalTraf
	 */
	public String getVolumeMensalTraf() {
		return volumeMensalTraf;
	}

	/**
	 * Set: volumeMensalTraf.
	 *
	 * @param volumeMensalTraf the volume mensal traf
	 */
	public void setVolumeMensalTraf(String volumeMensalTraf) {
		this.volumeMensalTraf = volumeMensalTraf;
	}

	/**
	 * Get: juncaoRespCustoTrans.
	 *
	 * @return juncaoRespCustoTrans
	 */
	public Integer getJuncaoRespCustoTrans() {
		return juncaoRespCustoTrans;
	}

	/**
	 * Set: juncaoRespCustoTrans.
	 *
	 * @param juncaoRespCustoTrans the juncao resp custo trans
	 */
	public void setJuncaoRespCustoTrans(Integer juncaoRespCustoTrans) {
		this.juncaoRespCustoTrans = juncaoRespCustoTrans;
	}

	/**
	 * Get: nomeContatoCliente.
	 *
	 * @return nomeContatoCliente
	 */
	public String getNomeContatoCliente() {
		return nomeContatoCliente;
	}

	/**
	 * Set: nomeContatoCliente.
	 *
	 * @param nomeContatoCliente the nome contato cliente
	 */
	public void setNomeContatoCliente(String nomeContatoCliente) {
		this.nomeContatoCliente = nomeContatoCliente;
	}

	/**
	 * Get: dddContatoCliente.
	 *
	 * @return dddContatoCliente
	 */
	public Integer getDddContatoCliente() {
		return dddContatoCliente;
	}

	/**
	 * Set: dddContatoCliente.
	 *
	 * @param dddContatoCliente the ddd contato cliente
	 */
	public void setDddContatoCliente(Integer dddContatoCliente) {
		this.dddContatoCliente = dddContatoCliente;
	}

	/**
	 * Get: telefoneContatoCliente.
	 *
	 * @return telefoneContatoCliente
	 */
	public String getTelefoneContatoCliente() {
		return telefoneContatoCliente;
	}

	/**
	 * Set: telefoneContatoCliente.
	 *
	 * @param telefoneContatoCliente the telefone contato cliente
	 */
	public void setTelefoneContatoCliente(String telefoneContatoCliente) {
		this.telefoneContatoCliente = telefoneContatoCliente;
	}

	/**
	 * Get: ramalContatoCliente.
	 *
	 * @return ramalContatoCliente
	 */
	public String getRamalContatoCliente() {
		return ramalContatoCliente;
	}

	/**
	 * Set: ramalContatoCliente.
	 *
	 * @param ramalContatoCliente the ramal contato cliente
	 */
	public void setRamalContatoCliente(String ramalContatoCliente) {
		this.ramalContatoCliente = ramalContatoCliente;
	}

	/**
	 * Get: emailContatoCliente.
	 *
	 * @return emailContatoCliente
	 */
	public String getEmailContatoCliente() {
		return emailContatoCliente;
	}

	/**
	 * Set: emailContatoCliente.
	 *
	 * @param emailContatoCliente the email contato cliente
	 */
	public void setEmailContatoCliente(String emailContatoCliente) {
		this.emailContatoCliente = emailContatoCliente;
	}

	/**
	 * Get: nomeContatoSolicitante.
	 *
	 * @return nomeContatoSolicitante
	 */
	public String getNomeContatoSolicitante() {
		return nomeContatoSolicitante;
	}

	/**
	 * Set: nomeContatoSolicitante.
	 *
	 * @param nomeContatoSolicitante the nome contato solicitante
	 */
	public void setNomeContatoSolicitante(String nomeContatoSolicitante) {
		this.nomeContatoSolicitante = nomeContatoSolicitante;
	}

	/**
	 * Get: dddContatoSolicitante.
	 *
	 * @return dddContatoSolicitante
	 */
	public Integer getDddContatoSolicitante() {
		return dddContatoSolicitante;
	}

	/**
	 * Set: dddContatoSolicitante.
	 *
	 * @param dddContatoSolicitante the ddd contato solicitante
	 */
	public void setDddContatoSolicitante(Integer dddContatoSolicitante) {
		this.dddContatoSolicitante = dddContatoSolicitante;
	}

	/**
	 * Get: telefoneContatoSolicitante.
	 *
	 * @return telefoneContatoSolicitante
	 */
	public String getTelefoneContatoSolicitante() {
		return telefoneContatoSolicitante;
	}

	/**
	 * Set: telefoneContatoSolicitante.
	 *
	 * @param telefoneContatoSolicitante the telefone contato solicitante
	 */
	public void setTelefoneContatoSolicitante(String telefoneContatoSolicitante) {
		this.telefoneContatoSolicitante = telefoneContatoSolicitante;
	}

	/**
	 * Get: ramalContatoSolicitante.
	 *
	 * @return ramalContatoSolicitante
	 */
	public String getRamalContatoSolicitante() {
		return ramalContatoSolicitante;
	}

	/**
	 * Set: ramalContatoSolicitante.
	 *
	 * @param ramalContatoSolicitante the ramal contato solicitante
	 */
	public void setRamalContatoSolicitante(String ramalContatoSolicitante) {
		this.ramalContatoSolicitante = ramalContatoSolicitante;
	}

	/**
	 * Get: emailContatoSolicitante.
	 *
	 * @return emailContatoSolicitante
	 */
	public String getEmailContatoSolicitante() {
		return emailContatoSolicitante;
	}

	/**
	 * Set: emailContatoSolicitante.
	 *
	 * @param emailContatoSolicitante the email contato solicitante
	 */
	public void setEmailContatoSolicitante(String emailContatoSolicitante) {
		this.emailContatoSolicitante = emailContatoSolicitante;
	}

	/**
	 * Get: justificativaSolicitante.
	 *
	 * @return justificativaSolicitante
	 */
	public String getJustificativaSolicitante() {
		return justificativaSolicitante;
	}

	/**
	 * Set: justificativaSolicitante.
	 *
	 * @param justificativaSolicitante the justificativa solicitante
	 */
	public void setJustificativaSolicitante(String justificativaSolicitante) {
		this.justificativaSolicitante = justificativaSolicitante;
	}

	/**
	 * Get: listaContratosVinculados.
	 *
	 * @return listaContratosVinculados
	 */
	public List<ListaContratosVinculadosPerfilOcorrencias> getListaContratosVinculados() {
		return listaContratosVinculados;
	}

	/**
	 * Set: listaContratosVinculados.
	 *
	 * @param listaContratosVinculados the lista contratos vinculados
	 */
	public void setListaContratosVinculados(
			List<ListaContratosVinculadosPerfilOcorrencias> listaContratosVinculados) {
		this.listaContratosVinculados = listaContratosVinculados;
	}

	/**
	 * Is flag meio transmissao.
	 *
	 * @return true, if is flag meio transmissao
	 */
	public boolean isFlagMeioTransmissao() {
		return flagMeioTransmissao;
	}

	/**
	 * Set: flagMeioTransmissao.
	 *
	 * @param flagMeioTransmissao the flag meio transmissao
	 */
	public void setFlagMeioTransmissao(boolean flagMeioTransmissao) {
		this.flagMeioTransmissao = flagMeioTransmissao;
	}

	/**
	 * Is flag custo transmissao.
	 *
	 * @return true, if is flag custo transmissao
	 */
	public boolean isFlagCustoTransmissao() {
		return flagCustoTransmissao;
	}

	/**
	 * Set: flagCustoTransmissao.
	 *
	 * @param flagCustoTransmissao the flag custo transmissao
	 */
	public void setFlagCustoTransmissao(boolean flagCustoTransmissao) {
		this.flagCustoTransmissao = flagCustoTransmissao;
	}

	/**
	 * Get: manterContratoBean.
	 *
	 * @return manterContratoBean
	 */
	public ManterContratoBean getManterContratoBean() {
		return manterContratoBean;
	}

	/**
	 * Set: manterContratoBean.
	 *
	 * @param manterContratoBean the manter contrato bean
	 */
	public void setManterContratoBean(ManterContratoBean manterContratoBean) {
		this.manterContratoBean = manterContratoBean;
	}

	/**
	 * Get: contratoSelecionado.
	 *
	 * @return contratoSelecionado
	 */
	public ListarContratosPgitSaidaDTO getContratoSelecionado() {
		return contratoSelecionado;
	}

	/**
	 * Set: contratoSelecionado.
	 *
	 * @param contratoSelecionado the contrato selecionado
	 */
	public void setContratoSelecionado(
			ListarContratosPgitSaidaDTO contratoSelecionado) {
		this.contratoSelecionado = contratoSelecionado;
	}

	/**
	 * Is flag contrato vinc.
	 *
	 * @return true, if is flag contrato vinc
	 */
	public boolean isFlagContratoVinc() {
		return flagContratoVinc;
	}

	/**
	 * Set: flagContratoVinc.
	 *
	 * @param flagContratoVinc the flag contrato vinc
	 */
	public void setFlagContratoVinc(boolean flagContratoVinc) {
		this.flagContratoVinc = flagContratoVinc;
	}

	/**
	 * Get: dsLayoutProprio.
	 *
	 * @return dsLayoutProprio
	 */
	public String getDsLayoutProprio() {
		return dsLayoutProprio;
	}

	/**
	 * Set: dsLayoutProprio.
	 *
	 * @param dsLayoutProprio the ds layout proprio
	 */
	public void setDsLayoutProprio(String dsLayoutProprio) {
		this.dsLayoutProprio = dsLayoutProprio;
	}

	/**
	 * Is flag dados controle.
	 *
	 * @return true, if is flag dados controle
	 */
	public boolean isFlagDadosControle() {
		return flagDadosControle;
	}

	/**
	 * Set: flagDadosControle.
	 *
	 * @param flagDadosControle the flag dados controle
	 */
	public void setFlagDadosControle(boolean flagDadosControle) {
		this.flagDadosControle = flagDadosControle;
	}

	/**
	 * Is flag cliente solicitante.
	 *
	 * @return true, if is flag cliente solicitante
	 */
	public boolean isFlagClienteSolicitante() {
		return flagClienteSolicitante;
	}

	/**
	 * Set: flagClienteSolicitante.
	 *
	 * @param flagClienteSolicitante the flag cliente solicitante
	 */
	public void setFlagClienteSolicitante(boolean flagClienteSolicitante) {
		this.flagClienteSolicitante = flagClienteSolicitante;
	}

	/**
	 * Get: percentualCliente.
	 *
	 * @return percentualCliente
	 */
	public BigDecimal getPercentualCliente() {
		return percentualCliente;
	}

	/**
	 * Set: percentualCliente.
	 *
	 * @param percentualCliente the percentual cliente
	 */
	public void setPercentualCliente(BigDecimal percentualCliente) {
		this.percentualCliente = percentualCliente;
	}

	/**
	 * Get: strNumeroMaximoRemessa.
	 *
	 * @return strNumeroMaximoRemessa
	 */
	public String getStrNumeroMaximoRemessa() {
		return strNumeroMaximoRemessa;
	}

	/**
	 * Set: strNumeroMaximoRemessa.
	 *
	 * @param strNumeroMaximoRemessa the str numero maximo remessa
	 */
	public void setStrNumeroMaximoRemessa(String strNumeroMaximoRemessa) {
		this.strNumeroMaximoRemessa = strNumeroMaximoRemessa;
	}

	/**
	 * Get: strNumeroMaximoRetorno.
	 *
	 * @return strNumeroMaximoRetorno
	 */
	public String getStrNumeroMaximoRetorno() {
		return strNumeroMaximoRetorno;
	}

	/**
	 * Set: strNumeroMaximoRetorno.
	 *
	 * @param strNumeroMaximoRetorno the str numero maximo retorno
	 */
	public void setStrNumeroMaximoRetorno(String strNumeroMaximoRetorno) {
		this.strNumeroMaximoRetorno = strNumeroMaximoRetorno;
	}

	/**
	 * Get: clienteFormatado.
	 *
	 * @return clienteFormatado
	 */
	public String getClienteFormatado() {
		return clienteFormatado;
	}

	/**
	 * Set: clienteFormatado.
	 *
	 * @param clienteFormatado the cliente formatado
	 */
	public void setClienteFormatado(String clienteFormatado) {
		this.clienteFormatado = clienteFormatado;
	}

	/**
	 * Get: bancoFormatado.
	 *
	 * @return bancoFormatado
	 */
	public String getBancoFormatado() {
		return bancoFormatado;
	}

	/**
	 * Set: bancoFormatado.
	 *
	 * @param bancoFormatado the banco formatado
	 */
	public void setBancoFormatado(String bancoFormatado) {
		this.bancoFormatado = bancoFormatado;
	}

	public IAssocLayoutArqProdServService getAssocLayoutArqProdServImpl() {
		return assocLayoutArqProdServImpl;
	}

	public void setAssocLayoutArqProdServImpl(
			IAssocLayoutArqProdServService assocLayoutArqProdServImpl) {
		this.assocLayoutArqProdServImpl = assocLayoutArqProdServImpl;
	}

	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	public String getTipoServico() {
		return tipoServico;
	}

	public void setListaGridTipoLayoutArquivo(
			List<IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO> listaGridTipoLayoutArquivo) {
		this.listaGridTipoLayoutArquivo = listaGridTipoLayoutArquivo;
	}

	public List<IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO> getListaGridTipoLayoutArquivo() {
		return listaGridTipoLayoutArquivo;
	}

	public List<ListarAssLayoutArqProdServicoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	public void setListaGrid(List<ListarAssLayoutArqProdServicoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	public void setCheckAll(boolean checkAll) {
		this.checkAll = checkAll;
	}

	public boolean isCheckAll() {
		return checkAll;
	}

	public void setExibePanelGroupRepresentantes(
			boolean exibePanelGroupRepresentantes) {
		this.exibePanelGroupRepresentantes = exibePanelGroupRepresentantes;
	}

	public boolean isExibePanelGroupRepresentantes() {
		return exibePanelGroupRepresentantes;
	}

	public List<IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO> getListaItensSelecionados() {
		return listaItensSelecionados;
	}

	public void setListaItensSelecionados(
			List<IncluirPerfilTrocaArquivoOcorrenciasEntradaDTO> listaItensSelecionados) {
		this.listaItensSelecionados = listaItensSelecionados;
	}

	public List<SelectItem> getListaAplicativoFormatArquivo() {
		return listaAplicativoFormatArquivo;
	}

	public void setListaAplicativoFormatArquivo(
			List<SelectItem> listaAplicativoFormatArquivo) {
		this.listaAplicativoFormatArquivo = listaAplicativoFormatArquivo;
	}

	public Map<Integer, String> getListaAplicativoFormatArquivoHash() {
		return listaAplicativoFormatArquivoHash;
	}

	public void setListaAplicativoFormatArquivoHash(
			Map<Integer, String> listaAplicativoFormatArquivoHash) {
		this.listaAplicativoFormatArquivoHash = listaAplicativoFormatArquivoHash;
	}

	/**
	 * @param rdoCdIndicadorGeracaoSegmentoB the rdoCdIndicadorGeracaoSegmentoB to set
	 */
	public void setRdoCdIndicadorGeracaoSegmentoB(
			Integer rdoCdIndicadorGeracaoSegmentoB) {
		this.rdoCdIndicadorGeracaoSegmentoB = rdoCdIndicadorGeracaoSegmentoB;
	}

	/**
	 * @return the rdoCdIndicadorGeracaoSegmentoB
	 */
	public Integer getRdoCdIndicadorGeracaoSegmentoB() {
		return rdoCdIndicadorGeracaoSegmentoB;
	}

	/**
	 * @param cboCdIndicadorGeracaoSegmentoZ the cboCdIndicadorGeracaoSegmentoZ to set
	 */
	public void setCboCdIndicadorGeracaoSegmentoZ(
			Integer cboCdIndicadorGeracaoSegmentoZ) {
		this.cboCdIndicadorGeracaoSegmentoZ = cboCdIndicadorGeracaoSegmentoZ;
	}

	/**
	 * @return the cboCdIndicadorGeracaoSegmentoZ
	 */
	public Integer getCboCdIndicadorGeracaoSegmentoZ() {
		return cboCdIndicadorGeracaoSegmentoZ;
	}

	/**
	 * @param renderizaCamposGeracaoSegmentosBeZ the renderizaCamposGeracaoSegmentosBeZ to set
	 */
	public void setRenderizaCamposGeracaoSegmentosBeZ(
			boolean renderizaCamposGeracaoSegmentosBeZ) {
		this.renderizaCamposGeracaoSegmentosBeZ = renderizaCamposGeracaoSegmentosBeZ;
	}

	/**
	 * @return the renderizaCamposGeracaoSegmentosBeZ
	 */
	public boolean isRenderizaCamposGeracaoSegmentosBeZ() {
		return renderizaCamposGeracaoSegmentosBeZ;
	}

	/**
	 * @param dsSegmentoB the dsSegmentoB to set
	 */
	public void setDsSegmentoB(String dsSegmentoB) {
		this.dsSegmentoB = dsSegmentoB;
	}

	/**
	 * @return the dsSegmentoB
	 */
	public String getDsSegmentoB() {
		return dsSegmentoB;
	}

	/**
	 * @param dsSegmentoZ the dsSegmentoZ to set
	 */
	public void setDsSegmentoZ(String dsSegmentoZ) {
		this.dsSegmentoZ = dsSegmentoZ;
	}

	/**
	 * @return the dsSegmentoZ
	 */
	public String getDsSegmentoZ() {
		return dsSegmentoZ;
	}

	/**
	 * @return the exibirCamposCNAB
	 */
	public boolean isExibirCamposCNAB() {
		return exibirCamposCNAB;
	}

	/**
	 * @param exibirCamposCNAB the exibirCamposCNAB to set
	 */
	public void setExibirCamposCNAB(boolean exibirCamposCNAB) {
		this.exibirCamposCNAB = exibirCamposCNAB;
	}

	/**
	 * @return the cdIndicadorCpfLayout
	 */
	public Integer getCdIndicadorCpfLayout() {
		return cdIndicadorCpfLayout;
	}

	/**
	 * @param cdIndicadorCpfLayout the cdIndicadorCpfLayout to set
	 */
	public void setCdIndicadorCpfLayout(Integer cdIndicadorCpfLayout) {
		this.cdIndicadorCpfLayout = cdIndicadorCpfLayout;
	}

	/**
	 * @return the cdIndicadorAssociacaoLayout
	 */
	public Integer getCdIndicadorAssociacaoLayout() {
		return cdIndicadorAssociacaoLayout;
	}

	/**
	 * @param cdIndicadorAssociacaoLayout the cdIndicadorAssociacaoLayout to set
	 */
	public void setCdIndicadorAssociacaoLayout(Integer cdIndicadorAssociacaoLayout) {
		this.cdIndicadorAssociacaoLayout = cdIndicadorAssociacaoLayout;
	}

	/**
	 * @return the cdIndicadorContaComplementar
	 */
	public Integer getCdIndicadorContaComplementar() {
		return cdIndicadorContaComplementar;
	}

	/**
	 * @param cdIndicadorContaComplementar the cdIndicadorContaComplementar to set
	 */
	public void setCdIndicadorContaComplementar(Integer cdIndicadorContaComplementar) {
		this.cdIndicadorContaComplementar = cdIndicadorContaComplementar;
	}

	/**
	 * @return the cdContaDebito
	 */
	public Integer getCdContaDebito() {
		return cdContaDebito;
	}

	/**
	 * @param cdContaDebito the cdContaDebito to set
	 */
	public void setCdContaDebito(Integer cdContaDebito) {
		this.cdContaDebito = cdContaDebito;
	}

	/**
	 * @return the cdIndicadorGeracaoSegmentoB
	 */
	public Integer getCdIndicadorGeracaoSegmentoB() {
		return cdIndicadorGeracaoSegmentoB;
	}

	/**
	 * @param cdIndicadorGeracaoSegmentoB the cdIndicadorGeracaoSegmentoB to set
	 */
	public void setCdIndicadorGeracaoSegmentoB(Integer cdIndicadorGeracaoSegmentoB) {
		this.cdIndicadorGeracaoSegmentoB = cdIndicadorGeracaoSegmentoB;
	}

	/**
	 * @return the cdIndicadorGeracaoSegmentoZ
	 */
	public Integer getCdIndicadorGeracaoSegmentoZ() {
		return cdIndicadorGeracaoSegmentoZ;
	}

	/**
	 * @param cdIndicadorGeracaoSegmentoZ the cdIndicadorGeracaoSegmentoZ to set
	 */
	public void setCdIndicadorGeracaoSegmentoZ(Integer cdIndicadorGeracaoSegmentoZ) {
		this.cdIndicadorGeracaoSegmentoZ = cdIndicadorGeracaoSegmentoZ;
	}

	/**
	 * @return the listaSegmentoZ
	 */
	public List<SelectItem> getListaSegmentoZ() {
		return listaSegmentoZ;
	}

	/**
	 * @param listaSegmentoZ the listaSegmentoZ to set
	 */
	public void setListaSegmentoZ(List<SelectItem> listaSegmentoZ) {
		this.listaSegmentoZ = listaSegmentoZ;
	}

	/**
	 * @return the dsIndicadorCpfLayout
	 */
	public String getDsIndicadorCpfLayout() {
		return dsIndicadorCpfLayout;
	}

	/**
	 * @param dsIndicadorCpfLayout the dsIndicadorCpfLayout to set
	 */
	public void setDsIndicadorCpfLayout(String dsIndicadorCpfLayout) {
		this.dsIndicadorCpfLayout = dsIndicadorCpfLayout;
	}

	/**
	 * @return the dsIndicadorContaComplementar
	 */
	public String getDsIndicadorContaComplementar() {
		return dsIndicadorContaComplementar;
	}

	/**
	 * @param dsIndicadorContaComplementar the dsIndicadorContaComplementar to set
	 */
	public void setDsIndicadorContaComplementar(String dsIndicadorContaComplementar) {
		this.dsIndicadorContaComplementar = dsIndicadorContaComplementar;
	}

	/**
	 * @return the dsContaDebito
	 */
	public String getDsContaDebito() {
		return dsContaDebito;
	}

	/**
	 * @param dsContaDebito the dsContaDebito to set
	 */
	public void setDsContaDebito(String dsContaDebito) {
		this.dsContaDebito = dsContaDebito;
	}

	/**
	 * @return the dsIndicadorAssociacaoLayout
	 */
	public String getDsIndicadorAssociacaoLayout() {
		return dsIndicadorAssociacaoLayout;
	}

	/**
	 * @param dsIndicadorAssociacaoLayout the dsIndicadorAssociacaoLayout to set
	 */
	public void setDsIndicadorAssociacaoLayout(String dsIndicadorAssociacaoLayout) {
		this.dsIndicadorAssociacaoLayout = dsIndicadorAssociacaoLayout;
	}

	/**
	 * @return the dsIndicadorGeracaoSegmentoB
	 */
	public String getDsIndicadorGeracaoSegmentoB() {
		return dsIndicadorGeracaoSegmentoB;
	}

	/**
	 * @param dsIndicadorGeracaoSegmentoB the dsIndicadorGeracaoSegmentoB to set
	 */
	public void setDsIndicadorGeracaoSegmentoB(String dsIndicadorGeracaoSegmentoB) {
		this.dsIndicadorGeracaoSegmentoB = dsIndicadorGeracaoSegmentoB;
	}

	/**
	 * @return the dsIndicadorGeracaoSegmentoZ
	 */
	public String getDsIndicadorGeracaoSegmentoZ() {
		return dsIndicadorGeracaoSegmentoZ;
	}

	/**
	 * @param dsIndicadorGeracaoSegmentoZ the dsIndicadorGeracaoSegmentoZ to set
	 */
	public void setDsIndicadorGeracaoSegmentoZ(String dsIndicadorGeracaoSegmentoZ) {
		this.dsIndicadorGeracaoSegmentoZ = dsIndicadorGeracaoSegmentoZ;
	}

	/**
	 * @param dsIndicadorConsisteContaDebito the dsIndicadorConsisteContaDebito to set
	 */
	public void setDsIndicadorConsisteContaDebito(
			String dsIndicadorConsisteContaDebito) {
		this.dsIndicadorConsisteContaDebito = dsIndicadorConsisteContaDebito;
	}

	/**
	 * @return the dsIndicadorConsisteContaDebito
	 */
	public String getDsIndicadorConsisteContaDebito() {
		return dsIndicadorConsisteContaDebito;
	}


}