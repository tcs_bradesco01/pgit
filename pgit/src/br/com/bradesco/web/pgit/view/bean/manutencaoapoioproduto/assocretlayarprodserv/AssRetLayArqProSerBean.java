/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assocretlayarprodserv
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assocretlayarprodserv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.IAssRetLayArqProSerService;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.DetalharAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.DetalharAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ExcluirAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.IncluirAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssRetLayArqProSerEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assretlayarqproser.bean.ListarAssRetLayArqProSerSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoArquivoRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;



/**
 * Nome: AssRetLayArqProSerBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AssRetLayArqProSerBean {
	
	/** Atributo CON_MANTER_ASSOC_RETORNO_LAY_ARQ_PROD_SERV. */
	private static final String CON_MANTER_ASSOC_RETORNO_LAY_ARQ_PROD_SERV = "conManterAssocRetornoLayArqProdServ";
	
	/** Atributo assRetLayArqProSerServiceImpl. */
	private IAssRetLayArqProSerService assRetLayArqProSerServiceImpl;
	
	/** Atributo filtroTipoServico. */
	private Integer filtroTipoServico;
	
	/** Atributo filtroTipoArquivoRetorno. */
	private Integer filtroTipoArquivoRetorno;
	
	/** Atributo filtroTipoLayoutArquivo. */
	private Integer filtroTipoLayoutArquivo;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;
	
	/** Atributo cdTipoRetorno. */
	private Integer cdTipoRetorno;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dsTipoRetorno. */
	private String dsTipoRetorno;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo listaGrid. */
	private List<ListarAssRetLayArqProSerSaidaDTO> listaGrid;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoArquivoRetorno. */
	private List<SelectItem> listaTipoArquivoRetorno = new ArrayList<SelectItem>();
	//private List<SelectItem> listaTipoArquivoRetornoIncluir = new ArrayList<SelectItem>();
	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer,String> listaTipoServicoHash = new HashMap<Integer,String>();
	
	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer,String> listaTipoLayoutArquivoHash = new HashMap<Integer,String>();
	
	/** Atributo listaTipoArquivoRetornoHash. */
	private Map<Integer,String> listaTipoArquivoRetornoHash = new HashMap<Integer,String>();
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo saidaTipoLayoutArquivoSaidaDTO. */
	private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;

	
	
	
	/*public List<SelectItem> getListaTipoArquivoRetornoIncluir() {
		return listaTipoArquivoRetornoIncluir;
	}

	public void setListaTipoArquivoRetornoIncluir(
			List<SelectItem> listaTipoArquivoRetornoIncluir) {
		this.listaTipoArquivoRetornoIncluir = listaTipoArquivoRetornoIncluir;
	}
*/
	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdTipoRetorno.
	 *
	 * @return cdTipoRetorno
	 */
	public Integer getCdTipoRetorno() {
		return cdTipoRetorno;
	}

	/**
	 * Set: cdTipoRetorno.
	 *
	 * @param cdTipoRetorno the cd tipo retorno
	 */
	public void setCdTipoRetorno(Integer cdTipoRetorno) {
		this.cdTipoRetorno = cdTipoRetorno;
	}

	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}

	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Get: dsTipoRetorno.
	 *
	 * @return dsTipoRetorno
	 */
	public String getDsTipoRetorno() {
		return dsTipoRetorno;
	}

	/**
	 * Set: dsTipoRetorno.
	 *
	 * @param dsTipoRetorno the ds tipo retorno
	 */
	public void setDsTipoRetorno(String dsTipoRetorno) {
		this.dsTipoRetorno = dsTipoRetorno;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarAssRetLayArqProSerSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarAssRetLayArqProSerSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: assRetLayArqProSerServiceImpl.
	 *
	 * @return assRetLayArqProSerServiceImpl
	 */
	public IAssRetLayArqProSerService getAssRetLayArqProSerServiceImpl() {
		return assRetLayArqProSerServiceImpl;
	}

	/**
	 * Set: assRetLayArqProSerServiceImpl.
	 *
	 * @param assRetLayArqProSerServiceImpl the ass ret lay arq pro ser service impl
	 */
	public void setAssRetLayArqProSerServiceImpl(
			IAssRetLayArqProSerService assRetLayArqProSerServiceImpl) {
		this.assRetLayArqProSerServiceImpl = assRetLayArqProSerServiceImpl;
	}

	/**
	 * Get: listaTipoArquivoRetornoHash.
	 *
	 * @return listaTipoArquivoRetornoHash
	 */
	public Map<Integer, String> getListaTipoArquivoRetornoHash() {
		return listaTipoArquivoRetornoHash;
	}

	/**
	 * Set lista tipo arquivo retorno hash.
	 *
	 * @param listaTipoArquivoRetornoHash the lista tipo arquivo retorno hash
	 */
	public void setListaTipoArquivoRetornoHash(
			Map<Integer, String> listaTipoArquivoRetornoHash) {
		this.listaTipoArquivoRetornoHash = listaTipoArquivoRetornoHash;
	}

	/**
	 * Get: listaTipoLayoutArquivoHash.
	 *
	 * @return listaTipoLayoutArquivoHash
	 */
	public Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}

	/**
	 * Set lista tipo layout arquivo hash.
	 *
	 * @param listaTipoLayoutArquivoHash the lista tipo layout arquivo hash
	 */
	public void setListaTipoLayoutArquivoHash(
			Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: filtroTipoServico.
	 *
	 * @return filtroTipoServico
	 */
	public Integer getFiltroTipoServico() {
		return filtroTipoServico;
	}
	
	/**
	 * Set: filtroTipoServico.
	 *
	 * @param filtroTipoServico the filtro tipo servico
	 */
	public void setFiltroTipoServico(Integer filtroTipoServico) {
		this.filtroTipoServico = filtroTipoServico;
	}
	
	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}
	
	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: filtroTipoArquivoRetorno.
	 *
	 * @return filtroTipoArquivoRetorno
	 */
	public Integer getFiltroTipoArquivoRetorno() {
		return filtroTipoArquivoRetorno;
	}

	/**
	 * Set: filtroTipoArquivoRetorno.
	 *
	 * @param filtroTipoArquivoRetorno the filtro tipo arquivo retorno
	 */
	public void setFiltroTipoArquivoRetorno(Integer filtroTipoArquivoRetorno) {
		this.filtroTipoArquivoRetorno = filtroTipoArquivoRetorno;
	}

	/**
	 * Get: listaTipoArquivoRetorno.
	 *
	 * @return listaTipoArquivoRetorno
	 */
	public List<SelectItem> getListaTipoArquivoRetorno() {
		return listaTipoArquivoRetorno;
	}

	/**
	 * Set: listaTipoArquivoRetorno.
	 *
	 * @param listaTipoArquivoRetorno the lista tipo arquivo retorno
	 */
	public void setListaTipoArquivoRetorno(List<SelectItem> listaTipoArquivoRetorno) {
		this.listaTipoArquivoRetorno = listaTipoArquivoRetorno;
	}

	/**
	 * Get: filtroTipoLayoutArquivo.
	 *
	 * @return filtroTipoLayoutArquivo
	 */
	public Integer getFiltroTipoLayoutArquivo() {
		return filtroTipoLayoutArquivo;
	}

	/**
	 * Set: filtroTipoLayoutArquivo.
	 *
	 * @param filtroTipoLayoutArquivo the filtro tipo layout arquivo
	 */
	public void setFiltroTipoLayoutArquivo(Integer filtroTipoLayoutArquivo) {
		this.filtroTipoLayoutArquivo = filtroTipoLayoutArquivo;
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 *
	 * @return listaTipoLayoutArquivo
	 */
	public List<SelectItem> getListaTipoLayoutArquivo() {
		return listaTipoLayoutArquivo;
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 *
	 * @param listaTipoLayoutArquivo the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}
	
	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	

	/**
	 * Limpar campos.
	 */
	public void limparCampos(){
		this.setFiltroTipoServico(0);
		this.setFiltroTipoArquivoRetorno(0);
		this.setFiltroTipoLayoutArquivo(0);
		setItemSelecionadoLista(null);
		setListaGrid(null);		
		setBtoAcionado(false);
	}
	
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		setFiltroTipoServico(0);
		this.setFiltroTipoArquivoRetorno(0);
		this.setFiltroTipoLayoutArquivo(0);
		setListaGrid(null);
		setItemSelecionadoLista(null);
		preencherListaTipoServico();
		preencherListaTipoLayoutArquivo();
		preencherListaTipoRetorno();
		setBtoAcionado(false);
	}
	
	/**
	 * Preencher lista tipo servico.
	 */
	public void preencherListaTipoServico(){
		try{
			this.listaTipoServico = new ArrayList<SelectItem>();
			
			List<ListarServicosSaidaDTO> listaServico = new ArrayList<ListarServicosSaidaDTO>();
			
			listaServico = comboService.listarTipoServicos(0);
			
			listaTipoServicoHash.clear();
			for(ListarServicosSaidaDTO combo : listaServico){
				listaTipoServicoHash.put(combo.getCdServico(), combo.getDsServico());
				listaTipoServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoServico = new ArrayList<SelectItem>();
		}
			
	}
	
	/*public void preencherListaTipoRetornoIncluir(){
		
		if (getCdTipoServico() != null && getCdTipoServico() != 0){
			setCdTipoServico(1);
			this.listaTipoArquivoRetornoIncluir = new ArrayList<SelectItem>();
			
			List<ListarTipoArquivoRetornoSaidaDTO> listaTipoArquivoRetornoSaida = new ArrayList<ListarTipoArquivoRetornoSaidaDTO>();
			
			listaTipoArquivoRetornoSaida = comboService.listarTipoArquivoRetorno();
			
			listaTipoArquivoRetornoHash.clear();		
			for(ListarTipoArquivoRetornoSaidaDTO combo: listaTipoArquivoRetornoSaida){
				listaTipoArquivoRetornoHash.put(combo.getCdTipoArquivoRetorno(), combo.getDsTipoArquivoRetorno());
				listaTipoArquivoRetornoIncluir.add(new SelectItem(combo.getCdTipoArquivoRetorno(), combo.getDsTipoArquivoRetorno()));
			}
		}else{
			listaTipoArquivoRetornoHash.clear();
			listaTipoArquivoRetornoIncluir.clear();
			setCdTipoServico(0);
			
			
		}	
	}*/
	
	
	
	/**
	 * Preencher lista tipo layout arquivo.
	 */
	public void preencherListaTipoLayoutArquivo(){
		try{	
			this.listaTipoLayoutArquivo = new ArrayList<SelectItem>();
			
			List<TipoLayoutArquivoSaidaDTO> listaTipoLayoutArquivoSaida = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			TipoLayoutArquivoEntradaDTO tipoLayoutArquivoEntrada = new TipoLayoutArquivoEntradaDTO();
			tipoLayoutArquivoEntrada.setCdSituacaoVinculacaoConta(0);
			
			saidaTipoLayoutArquivoSaidaDTO = comboService.listarTipoLayoutArquivo(tipoLayoutArquivoEntrada);
			listaTipoLayoutArquivoSaida = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
			
			listaTipoLayoutArquivoHash.clear();
			for(TipoLayoutArquivoSaidaDTO combo : listaTipoLayoutArquivoSaida){
				listaTipoLayoutArquivoHash.put(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
				listaTipoLayoutArquivo.add(new SelectItem(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		}
	}
	
	/**
	 * Preencher lista tipo retorno.
	 */
	public void preencherListaTipoRetorno(){
		try{
			this.listaTipoArquivoRetorno = new ArrayList<SelectItem>();
			
			List<ListarTipoArquivoRetornoSaidaDTO> listaTipoArquivoRetornoSaida = new ArrayList<ListarTipoArquivoRetornoSaidaDTO>();
			
			listaTipoArquivoRetornoSaida = comboService.listarTipoArquivoRetorno();
			
			listaTipoArquivoRetornoHash.clear();	
			for(ListarTipoArquivoRetornoSaidaDTO combo: listaTipoArquivoRetornoSaida){
				listaTipoArquivoRetornoHash.put(combo.getCdTipoArquivoRetorno(), combo.getDsTipoArquivoRetorno());
				listaTipoArquivoRetorno.add(new SelectItem(combo.getCdTipoArquivoRetorno(), combo.getDsTipoArquivoRetorno()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoArquivoRetorno = new ArrayList<SelectItem>();
		}
		
		
	}
	
	/**
	 * Carrega lista.
	 */
	public void carregaLista() {
		
		try{
			
			ListarAssRetLayArqProSerEntradaDTO entradaDTO = new ListarAssRetLayArqProSerEntradaDTO();
			entradaDTO.setTipoServico(getFiltroTipoServico()!=null?getFiltroTipoServico():0);
			entradaDTO.setTipoArquivoRetorno(getFiltroTipoArquivoRetorno()!=null?getFiltroTipoArquivoRetorno():0);
			entradaDTO.setTipoLayoutArquivo(getFiltroTipoLayoutArquivo()!=null?getFiltroTipoLayoutArquivo():0);
			
			setListaGrid(getAssRetLayArqProSerServiceImpl().listarAssRetLayArqProSer(entradaDTO));
			setBtoAcionado(true);
			for (int i = 0; i <= this.getListaGrid().size(); i++) {
				listaControle.add(new SelectItem(i, " "));
			}

		   
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(p.getMessage(), false);
			setListaGrid(null);

		}
		setItemSelecionadoLista(null);
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_ASSOC_RETORNO_LAY_ARQ_PROD_SERV, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "DETALHAR";
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		setCdTipoServico(0);
		setCdTipoRetorno(0);
		setCdTipoLayoutArquivo(0);
		preencherListaTipoRetorno();
		//this.listaTipoArquivoRetornoIncluir.clear();
		return "INCLUIR";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		setDsTipoServico((String)listaTipoServicoHash.get(getCdTipoServico()));
		setDsTipoRetorno((String)listaTipoArquivoRetornoHash.get(getCdTipoRetorno()));
		setDsTipoLayoutArquivo((String)listaTipoLayoutArquivoHash.get(getCdTipoLayoutArquivo()));
		return "AVANCARINCLUIR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_MANTER_ASSOC_RETORNO_LAY_ARQ_PROD_SERV, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "EXCLUIR";
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		
		return "VOLTAR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try {
			
			IncluirAssRetLayArqProSerEntradaDTO incluirAssRetLayArqProSerEntradaDTO = new IncluirAssRetLayArqProSerEntradaDTO();
			
			incluirAssRetLayArqProSerEntradaDTO.setTipoServico(getCdTipoServico());
			incluirAssRetLayArqProSerEntradaDTO.setTipoArquivoRetorno(getCdTipoRetorno());
			incluirAssRetLayArqProSerEntradaDTO.setTipoLayoutArquivo(getCdTipoLayoutArquivo());
			
			IncluirAssRetLayArqProSerSaidaDTO incluirAssRetLayArqProSerSaidaDTO = getAssRetLayArqProSerServiceImpl().incluirAssRetLayArqProSer(incluirAssRetLayArqProSerEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +incluirAssRetLayArqProSerSaidaDTO.getCodMensagem() + ") " + incluirAssRetLayArqProSerSaidaDTO.getMensagem(), CON_MANTER_ASSOC_RETORNO_LAY_ARQ_PROD_SERV, BradescoViewExceptionActionType.ACTION, false);
			carregaLista();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				return null;
			}
			return "";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try {
			
			ExcluirAssRetLayArqProSerEntradaDTO excluirAssRetLayArqProSerEntradaDTO = new ExcluirAssRetLayArqProSerEntradaDTO();
			ListarAssRetLayArqProSerSaidaDTO listarAssRetLayArqProSerSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
			
			excluirAssRetLayArqProSerEntradaDTO.setTipoServico(listarAssRetLayArqProSerSaidaDTO.getCdTipoServico());
			excluirAssRetLayArqProSerEntradaDTO.setTipoArquivoRetorno(listarAssRetLayArqProSerSaidaDTO.getCdTipoArquivoRetorno());
			excluirAssRetLayArqProSerEntradaDTO.setTipoLayoutArquivo(listarAssRetLayArqProSerSaidaDTO.getCdTipoLayoutArquivo());
			
			ExcluirAssRetLayArqProSerSaidaDTO excluirAssRetLayArqProSerSaidaDTO = getAssRetLayArqProSerServiceImpl().excluirAssRetLayArqProSer(excluirAssRetLayArqProSerEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + excluirAssRetLayArqProSerSaidaDTO.getCodMensagem() + ") " + excluirAssRetLayArqProSerSaidaDTO.getMensagem(), CON_MANTER_ASSOC_RETORNO_LAY_ARQ_PROD_SERV, BradescoViewExceptionActionType.ACTION, false);

			carregaLista();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Preenche dados.
	 */
	public void preencheDados(){
		ListarAssRetLayArqProSerSaidaDTO listarAssRetLayArqProSerSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
		DetalharAssRetLayArqProSerEntradaDTO detalharAssRetLayArqProSerEntradaDTO = new DetalharAssRetLayArqProSerEntradaDTO();
		
		detalharAssRetLayArqProSerEntradaDTO.setTipoServico(listarAssRetLayArqProSerSaidaDTO.getCdTipoServico());
		detalharAssRetLayArqProSerEntradaDTO.setTipoArquivoRetorno(listarAssRetLayArqProSerSaidaDTO.getCdTipoArquivoRetorno());
		detalharAssRetLayArqProSerEntradaDTO.setTipoLayoutArquivo(listarAssRetLayArqProSerSaidaDTO.getCdTipoLayoutArquivo());
		
		DetalharAssRetLayArqProSerSaidaDTO detalharAssRetLayArqProSerSaidaDTO = getAssRetLayArqProSerServiceImpl().detalharAssRetLayArqProSer(detalharAssRetLayArqProSerEntradaDTO);
		setCdTipoServico(detalharAssRetLayArqProSerSaidaDTO.getCdTipoServico());
		setDsTipoServico(detalharAssRetLayArqProSerSaidaDTO.getDsTipoServico());
		setCdTipoRetorno(detalharAssRetLayArqProSerSaidaDTO.getCdTipoArquivoRetorno());
		setDsTipoRetorno(detalharAssRetLayArqProSerSaidaDTO.getDsTipoArquivoRetorno());
		setCdTipoLayoutArquivo(detalharAssRetLayArqProSerSaidaDTO.getCdLayoutArquivo());
		setDsTipoLayoutArquivo(detalharAssRetLayArqProSerSaidaDTO.getDsLayoutArquivo());
		setUsuarioInclusao(detalharAssRetLayArqProSerSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharAssRetLayArqProSerSaidaDTO.getUsuarioManutencao());
		setComplementoInclusao(detalharAssRetLayArqProSerSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharAssRetLayArqProSerSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharAssRetLayArqProSerSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharAssRetLayArqProSerSaidaDTO.getComplementoManutencao());
		setTipoCanalInclusao(detalharAssRetLayArqProSerSaidaDTO.getCdCanalInclusao()==0? "" : detalharAssRetLayArqProSerSaidaDTO.getCdCanalInclusao() + " - " + detalharAssRetLayArqProSerSaidaDTO.getDsCanalInclusao());
		setTipoCanalManutencao(detalharAssRetLayArqProSerSaidaDTO.getCdCanalManutencao()==0? "" : detalharAssRetLayArqProSerSaidaDTO.getCdCanalManutencao() + " - " + detalharAssRetLayArqProSerSaidaDTO.getDsCanalManutencao());
		setDataHoraInclusao(detalharAssRetLayArqProSerSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharAssRetLayArqProSerSaidaDTO.getDataHoraManutencao());
		
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Nome: setSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @param saidaTipoLayoutArquivoSaidaDTO
	 */
	public void setSaidaTipoLayoutArquivoSaidaDTO(
			TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
		this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: getSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @return saidaTipoLayoutArquivoSaidaDTO
	 */
	public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
		return saidaTipoLayoutArquivoSaidaDTO;
	}

}
