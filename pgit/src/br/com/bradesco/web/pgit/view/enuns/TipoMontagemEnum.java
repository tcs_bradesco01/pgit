package br.com.bradesco.web.pgit.view.enuns;


public enum TipoMontagemEnum {

	SELECIONE(0, "::SELECIONE::"), REMESSA(1, "Remessa"), AGENCIA_CONTA(2,
			"Ag�ncia/Conta de D�bito"), TIPO_SERVICO_MODALIDADE(3,
			"Tipo de Servi�o + Modalidade"), REMESSA_TIPO_SERVICO_MODALIDADE(0,
			"Remessa + Tipo de Servi�o + Modalidade"), SEM_SEPARACAO(9,
			"Sem Separa��o"), TIPO_SERVICO(5, "Tipo de Servi�o");

	/** Atributo cdTipoMontagem. */
	private final Integer cdTipoMontagem;

	/** Atributo dsTipoMontagem. */
	private final String dsTipoMontagem;

	private TipoMontagemEnum(Integer cdTipoMontagem, String dsTipoMontagem) {
		this.cdTipoMontagem = cdTipoMontagem;
		this.dsTipoMontagem = dsTipoMontagem;
	}

	public Integer getCdTipoMontagem() {
		return cdTipoMontagem;
	}

	public String getDsTipoMontagem() {
		return dsTipoMontagem;
	}

}
