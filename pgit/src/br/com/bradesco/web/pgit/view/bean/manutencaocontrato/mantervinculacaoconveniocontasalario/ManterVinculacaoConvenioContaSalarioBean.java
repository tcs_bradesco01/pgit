/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantervinculacaoconveniocontasalario
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantervinculacaoconveniocontasalario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.ExcluirVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.ExcluirVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.IManterVinculacaoConvenioContaSalarioService;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.AlterarVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.AlterarVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharPiramideSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.DetalharVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.FolhaPagamentoEntradaDto;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.FolhaPagamentoSaidaDto;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.IncluirVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.IncluirVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosConvenioContaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnListaOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarDadosCtaConvnSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarVincConvnCtaSalarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarVincConvnCtaSalarioOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.mantervinculacaoconveniocontasalario.bean.ListarVincConvnCtaSalarioSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterVinculacaoConvenioContaSalarioBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterVinculacaoConvenioContaSalarioBean {

	/**
	 * Atributo int
	 */
	private static final int CD_PARM_CONVENIO = 1;

    /** Atributo manterVinculacaoConvenioContaSalarioService. */ 
	private IManterVinculacaoConvenioContaSalarioService manterVinculacaoConvenioContaSalarioService;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean = null;

	/** Atributo comboService. */
	private IComboService comboService = null;

	/** Atributo identificacaoClienteBean. */
	private IdentificacaoClienteBean identificacaoClienteBean = null;

	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo dtoConvenio. */
	private DetalharVincConvnCtaSalarioSaidaDTO dtoConvenio = new DetalharVincConvnCtaSalarioSaidaDTO();

	/** Atributo listaSelConvenio. */
	private List<ListarDadosCtaConvnListaOcorrenciasDTO> listaSelConvenio = new ArrayList<ListarDadosCtaConvnListaOcorrenciasDTO>();
	
	private ListarDadosCtaConvnSaidaDTO saidaListarDadosCtaConvn = null;

	/** Atributo listaControleSelConvenio. */
	private List<SelectItem> listaControleSelConvenio = new ArrayList<SelectItem>();

	/** Atributo itemSelecionadoListaConvenio. */
	private Integer itemSelecionadoListaConvenio;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo foco. */
	private String foco;

	/** Atributo habilitaBtoCont. */
	private Boolean habilitaBtoCont;

	/** Atributo btoAcao. */
	private String btoAcao;

	/** Atributo codigoConvenio. */
	private Long codigoConvenio;
	
	/** Atributo decricaoConvenio. */
	private String decricaoConvenio;
	
	/** Atributo situacaoConvenio. */
	private String situacaoConvenio;
	
	/** Atributo bancoAgenciaContaConvenio. */
	private String bancoAgenciaContaConvenio;
	
	/** Atributo dtHoraInclusao. */
	private String dtHoraInclusao;

	/** Atributo cdUsuario. */
	private String cdUsuario;
	
	// Cliente Representante
	/** Atributo cpfCnpjRepresentante. */
	private String cpfCnpjRepresentante;

	/** Atributo nomeRazaoRepresentante. */
	private String nomeRazaoRepresentante;

	/** Atributo grupEconRepresentante. */
	private String grupEconRepresentante;

	/** Atributo ativEconRepresentante. */
	private String ativEconRepresentante;

	/** Atributo segRepresentante. */
	private String segRepresentante;

	/** Atributo subSegRepresentante. */
	private String subSegRepresentante;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;
	
	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado;

	// Cliente Participante
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;
	
	/** Atributo cpfCnpjParticipanteTemp. */
	private String cpfCnpjParticipanteTemp;

	/** Atributo nomeRazaoParticipanteTemp. */
	private String nomeRazaoParticipanteTemp;

	/** Atributo cpfCnpjParticipanteDet. */
	private String cpfCnpjParticipanteDet;

	/** Atributo nomeRazaoParticipanteDet. */
	private String nomeRazaoParticipanteDet;

	/** Atributo grupoEconParticipante. */
	private String grupoEconParticipante;

	/** Atributo atividadeEconParticipante. */
	private String atividadeEconParticipante;

	/** Atributo segmentoParticipante. */
	private String segmentoParticipante;

	/** Atributo subSegmentoParticipante. */
	private String subSegmentoParticipante;
	
	/** Atributo dsTipoParticipacao. */
	private String dsTipoParticipacao;
	
	/** Atributo situacaoParticipacao. */
	private String situacaoParticipacao;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;

	// Contrato
	/** Atributo empresaContrato. */
	private String empresaContrato;

	/** Atributo tipoContrato. */
	private String tipoContrato;

	/** Atributo numeroContrato. */
	private String numeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo situacaoContrato. */
	private String situacaoContrato;

	/** Atributo motivoContrato. */
	private String motivoContrato;

	/** Atributo participacaoContrato. */
	private String participacaoContrato;

	/** Atributo dtManutencaoInicio. */
	private Date dtManutencaoInicio;
	
	/** Atributo dtManutencaoFim. */
	private Date dtManutencaoFim;
	
	/* Trilha de Auditoria */
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo paginaRetorno. */
	private String paginaRetorno;
	
	/** Atributo valorFolhaPagamentoEmpresa */
	private BigDecimal valorFolhaPagamentoEmpresa;
	
	/** Atributo qtFuncionarioEmpresaPagadora */
	private String qtFuncionarioEmpresaPagadora;
	
	/** Atributo vlMediaSalarialEmpresa */
	private BigDecimal vlMediaSalarialEmpresa;
	
	/** Atributo pagamentoMes */
	private String pagamentoMes;
	
	/** Atributo pagamentoQuinzena */
	private String pagamentoQuinzena;
	
	/** Atributo aberturaContaSalario */
	private String aberturaContaSalario;
	
	private boolean verificaMensagem; 

	/** The dados piramide. */
	private DetalharPiramideSaidaDTO dadosPiramide;
	
	private Integer desabilitaBotao = null;

	private String verificaBotao = null;
	
	private List<ListarDadosConvenioContaSalarioSaidaDTO> listaSaidaDTO = null;
	
	private List<ListarVincConvnCtaSalarioOcorrenciasDTO> listaDadosConvenio = null;
	
	private static final int NUMERO_CINQUENTA = 50;
	
	ListarVincConvnCtaSalarioSaidaDTO retornoSaidaDTO = new ListarVincConvnCtaSalarioSaidaDTO();
	
	/** Atributo lista. */
	private List<Object> lista = new ArrayList<Object>();
	
	/**
     * Instancia um novo ManterVinculacaoConvenioContaSalarioBean
     *
     */
	
	
	public ListarDadosConvenioContaSalarioSaidaDTO listaDadosSaida;
	
	public List<ListarDadosConvenioContaSalarioSaidaDTO> listaOcorrenciasSaidaDTO;
	public List<ListarDadosConvenioContaSalarioSaidaDTO> listarDadosConvenioContaSalario;
	
    public ManterVinculacaoConvenioContaSalarioBean() {
        super();
        lista.add(new Object());
        lista.add(new Object());
        lista.add(new Object());
        lista.add(new Object());
        lista.add(new Object());
    }

    /**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		this.identificacaoClienteContratoBean.setPaginaRetorno("conManterVinculacaoConvenioContaSalario");
		this.identificacaoClienteBean.setPaginaRetorno("conManterVinculacaoConvenioContaSalario");

		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean.setPaginaRetorno("conManterVinculacaoConvenioContaSalario");

		// Limpar Variaveis
		setListaControleRadio(null);
		setItemSelecionadoLista(null);

		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
		setHabilitaBtoCont(true);

		this.identificacaoClienteContratoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteManterContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conManterVinculacaoConvenioContaSalario");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
		
	}

	/**
	 * Vinculacao.
	 *
	 * @return the string
	 */
	public String vinculacao() {
		setBtoAcao("");
		setPaginaRetorno("vincManterVinculacaoConvenioContaSalario");

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa()
		.get(identificacaoClienteContratoBean.getItemSelecionadoLista());

		//detalharVinculacao();

		// Cliente Representante
		setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupEconRepresentante(saidaDTO.getDsGrupoEconomico());
		setAtivEconRepresentante(saidaDTO.getDsAtividadeEconomica());
		setSegRepresentante(saidaDTO.getDsSegmentoCliente());
		setSubSegRepresentante(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
		
		// Cliente Participante
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ||
				"".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
				setCpfCnpjParticipante("");
				setNomeRazaoParticipante("");
			} else {
				setCpfCnpjParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? ""
					: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
				setNomeRazaoParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
					: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
			}
		
		// Contrato
		setEmpresaContrato(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());
		setMotivoContrato(saidaDTO.getDsMotivoSituacao());
		setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());

		listarDadosConvenioContaSalario(saidaDTO);		
		return getPaginaRetorno();
	}
	
	public void carregarPaginacao(ActionEvent e,ListarContratosPgitSaidaDTO saidaDTO){
		listarDadosConvenioContaSalario(saidaDTO);
	}
	
	public ListarDadosConvenioContaSalarioSaidaDTO getListaDadosSaida() {
		return listaDadosSaida;
	}

	public void setListaDadosSaida(ListarDadosConvenioContaSalarioSaidaDTO listaDadosSaida) {
		this.listaDadosSaida = listaDadosSaida;
	}

	public List<ListarVincConvnCtaSalarioOcorrenciasDTO> listarDadosConvenioContaSalario(ListarContratosPgitSaidaDTO saida){		
		
		
		ListarVincConvnCtaSalarioEntradaDTO entrada = new ListarVincConvnCtaSalarioEntradaDTO();
		
		entrada.setCodPessoaJuridica(saida.getCdPessoaJuridica());
		entrada.setCodTipoContratoNegocio(saida.getCdTipoContrato());
		entrada.setMaxOcorrencias(NUMERO_CINQUENTA);
		entrada.setNumSeqContratoNegocio(saida.getNrSequenciaContrato());
		
		ListarVincConvnCtaSalarioSaidaDTO saidaDTO = manterVinculacaoConvenioContaSalarioService.listarConvenioContaSalarioNovo(entrada);
		retornoSaidaDTO.setIndicadorConvenioNovo(saidaDTO.getIndicadorConvenioNovo());
		retornoSaidaDTO.setUsuarioInclusao(saidaDTO.getUsuarioInclusao());
		retornoSaidaDTO.setHorarioInclusao(saidaDTO.getHorarioInclusao());
		retornoSaidaDTO.setDescCanalInclusao(saidaDTO.getDescCanalInclusao());
		retornoSaidaDTO.setDescCanalManutencao(saidaDTO.getDescCanalManutencao());
		retornoSaidaDTO.setTipoCanalInclusao(saidaDTO.getTipoCanalInclusao());
		retornoSaidaDTO.setTipoCanalManutencao(saidaDTO.getTipoCanalManutencao());
		retornoSaidaDTO.setUsuarioManutencao(saidaDTO.getUsuarioManutencao());
		retornoSaidaDTO.setHorarioManutencao(saidaDTO.getHorarioManutencao());
		
		listaDadosConvenio = new ArrayList<ListarVincConvnCtaSalarioOcorrenciasDTO>();

		for (ListarVincConvnCtaSalarioOcorrenciasDTO o : saidaDTO.getListaOcorrencias()) {
			ListarVincConvnCtaSalarioOcorrenciasDTO dto = new ListarVincConvnCtaSalarioOcorrenciasDTO();
			
			dto.setCodConvenioContaSalario(o.getCodConvenioContaSalario());
			dto.setCpfCnpj(o.getCpfCnpj());			
			dto.setFilialCpfCnpj(o.getFilialCpfCnpj());
			dto.setControleCpfCnpj(o.getControleCpfCnpj());
			dto.setBanco(o.getBanco());
			dto.setAgencia(o.getAgencia());			
			dto.setDigAgencia(o.getDigAgencia());
			dto.setConta(o.getConta());
			dto.setDigConta(o.getDigConta());
			dto.setCodSituacaoConvenio(o.getCodSituacaoConvenio());
			dto.setSituacaoConvenio(o.getSituacaoConvenio());
			listaDadosConvenio.add(dto);
		}

		return listaDadosConvenio;

	}
	
	

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		setBtoAcao("I");
		setItemSelecionadoListaConvenio(null);

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		try {
			ListarDadosCtaConvnEntradaDTO entrada = new ListarDadosCtaConvnEntradaDTO();
			listaSelConvenio = new ArrayList<ListarDadosCtaConvnListaOcorrenciasDTO>();
			listaControleSelConvenio = new ArrayList<SelectItem>();

			entrada.setCdClubPessoaRepresentante(saidaDTO.getCdClubRepresentante());
			entrada.setCdCpfCnpjRepresentante(saidaDTO.getCdCpfCnpjRepresentante());
			entrada.setCdControleCnpjRepresentante(saidaDTO.getCdControleCpfCnpjRepresentante());
			entrada.setCdFilialCnpjRepresentante(saidaDTO.getCdFilialCpfCnpjRepresentante());
			entrada.setCdPessoa(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
			entrada.setCdParmConvenio(CD_PARM_CONVENIO);

			saidaListarDadosCtaConvn = getManterVinculacaoConvenioContaSalarioService().listarDadosContaConvenio(entrada);
			setListaSelConvenio(saidaListarDadosCtaConvn.getOcorrencias());

			for (int i = 0; i < listaSelConvenio.size(); i++) {
				listaControleSelConvenio.add(new SelectItem(i, " "));
			}
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
			return "";
		}

		return "incManterVinculacaoConvenioContaSalario";
	}

	/**
	 * Detalhar vinculacao.
	 */
	public void detalharVinculacao() {
		setVerificaMensagem(false);
		
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		try {
			DetalharVincConvnCtaSalarioEntradaDTO entrada = new DetalharVincConvnCtaSalarioEntradaDTO();

			entrada.setCdPessoaJuridica(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContrato(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContrato(saidaDTO.getNrSequenciaContrato());

  			dtoConvenio = getManterVinculacaoConvenioContaSalarioService().detalharVincConvnCtaSalario(entrada);
			
			dataHoraManutencao = FormatarData.formatarDataTrilha(dtoConvenio.getDtManutencao()+"-"+dtoConvenio.getHrManutencao());

			usuarioManutencao =  dtoConvenio.getCdUsuarioManutencao();

			tipoCanalManutencao = dtoConvenio.getCdCanalManutencao().compareTo(0) == 0 ? "" : dtoConvenio.getCdCanalManutencao().toString() +"-"+ (dtoConvenio.getDsTipoCanalManutencao());

			complementoManutencao =  "0".equals(dtoConvenio.getCdOperacaoFluxoManutencao()) ? "" : dtoConvenio.getCdOperacaoFluxoManutencao();

			dataHoraInclusao = FormatarData.formatarDataTrilha(dtoConvenio.getDtInclusao()+"-"+dtoConvenio.getHrInclusao());

			usuarioInclusao =  dtoConvenio.getCdUsuarioInclusao();

			tipoCanalInclusao =  dtoConvenio.getCdTipoCanalInclusao().compareTo(0) == 0 ? "" : dtoConvenio.getCdTipoCanalInclusao().toString() +"-"+ (dtoConvenio.getDsTipoCanalInclusao());

			complementoInclusao =  "0".equals(dtoConvenio.getCdOperacaoFluxoInclusao()) ? "" : dtoConvenio.getCdOperacaoFluxoInclusao(); 
			
			// Dados Convenio
			setCodigoConvenio(dtoConvenio.getCdConveCtaSalarial()==0?null:dtoConvenio.getCdConveCtaSalarial());
			setDecricaoConvenio(dtoConvenio.getDsConvCtaSalarial());
			setSituacaoConvenio(dtoConvenio.getCdSitConvCta()==1?"ATIVO":dtoConvenio.getCdSitConvCta()==2?"INATIVO":"");
			if (dtoConvenio.getCdBcoEmprConvn() != 0 || dtoConvenio.getCdAgenciaEmprConvn() != 0 || dtoConvenio.getCdCtaEmprConvn() != 0) {
				setBancoAgenciaContaConvenio(dtoConvenio.getCdBcoEmprConvn() + "/" + dtoConvenio.getCdAgenciaEmprConvn() + "/" + dtoConvenio.getCdCtaEmprConvn() + "-" + dtoConvenio.getCdDigitoEmprConvn());
			}else{
				setBancoAgenciaContaConvenio(null);
			}
			setDtHoraInclusao(dtoConvenio.getDtHoraInclusao());
			setCdUsuario(dtoConvenio.getCdUsuarioInclusao());
			
			if ("PGIT1092".equalsIgnoreCase(dtoConvenio.getCodMensagem())) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(dtoConvenio.getCodMensagem(), 8) + ") " + dtoConvenio.getMensagem(), false);
			}
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
			limparDadosConvenio();
			setPaginaRetorno("");
			setVerificaMensagem(true);			
		}
	}
	
	/**
	 * Limpar dados convenio.
	 */
	public void limparDadosConvenio(){
		dtoConvenio = new DetalharVincConvnCtaSalarioSaidaDTO();
		setDataHoraManutencao(null);
		setUsuarioManutencao(null);
		setTipoCanalManutencao(null);
		setComplementoManutencao(null);
		setDataHoraInclusao(null);
		setUsuarioInclusao(null);
		setTipoCanalInclusao(null);
		setComplementoInclusao(null); 
		setCodigoConvenio(null);
		setDecricaoConvenio(null);
		setSituacaoConvenio(null);
		setBancoAgenciaContaConvenio(null);
		setDtHoraInclusao(null);
		setCdUsuario(null);
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		try {
			ExcluirVincConvnCtaSalarioEntradaDTO entrada = new ExcluirVincConvnCtaSalarioEntradaDTO();

			entrada.setCdPessoaJuridica(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContrato(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContrato(saidaDTO.getNrSequenciaContrato());

			ExcluirVincConvnCtaSalarioSaidaDTO saida = getManterVinculacaoConvenioContaSalarioService().excluirVincConvnCtaSalario(entrada);

			setCodigoConvenio(null);
			setDecricaoConvenio("");
			setSituacaoConvenio("");
			setBancoAgenciaContaConvenio("");
			setDtHoraInclusao("");
			setCdUsuario("");

			dtoConvenio = new DetalharVincConvnCtaSalarioSaidaDTO();

			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(saida.getCodMensagem(), 8) + ") " + saida.getMensagem(), false);
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
		}

		return "vincManterVinculacaoConvenioContaSalario";
	}

	/**
	 * Voltar consulta contrato.
	 *
	 * @return the string
	 */
	public String voltarConsultaContrato() {
		return "conManterVinculacaoConvenioContaSalario";
	}

	/**
	 * Voltar vinculacao.
	 *
	 * @return the string
	 */
	public String voltarVinculacao() {
		return "vincManterVinculacaoConvenioContaSalario";
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		setBtoAcao("A");
		setItemSelecionadoListaConvenio(null);

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		try {
			ListarDadosCtaConvnEntradaDTO entrada = new ListarDadosCtaConvnEntradaDTO();
			listaSelConvenio = new ArrayList<ListarDadosCtaConvnListaOcorrenciasDTO>();
			listaControleSelConvenio = new ArrayList<SelectItem>();

			entrada.setCdClubPessoaRepresentante(saidaDTO.getCdClubRepresentante());
			entrada.setCdCpfCnpjRepresentante(saidaDTO.getCdCpfCnpjRepresentante());
			entrada.setCdControleCnpjRepresentante(saidaDTO.getCdControleCpfCnpjRepresentante());
			entrada.setCdFilialCnpjRepresentante(saidaDTO.getCdFilialCpfCnpjRepresentante());
			entrada.setCdPessoa(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
			entrada.setCdParmConvenio(CD_PARM_CONVENIO);

			saidaListarDadosCtaConvn = getManterVinculacaoConvenioContaSalarioService().listarDadosContaConvenio(entrada);
			setListaSelConvenio(saidaListarDadosCtaConvn.getOcorrencias());

			for (int i = 0; i < listaSelConvenio.size(); i++) {
				listaControleSelConvenio.add(new SelectItem(i, " "));
			}
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
			return "";
		}

		return "altManterVinculacaoConvenioContaSalario";
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {
		IncluirVincConvnCtaSalarioEntradaDTO entrada = new IncluirVincConvnCtaSalarioEntradaDTO();

		try {
			ListarContratosPgitSaidaDTO listaContrato = identificacaoClienteContratoBean.getListaGridPesquisa().get(
					identificacaoClienteContratoBean.getItemSelecionadoLista());
			ListarDadosCtaConvnListaOcorrenciasDTO lista = listaSelConvenio.get(itemSelecionadoListaConvenio);

			entrada.setCdAgencCtaEmp(lista.getCdAgencia());
			entrada.setCdBcoCtaEmp(lista.getCdBanco());
			entrada.setCdCnpjEmp(lista.getCdCpfCnpjEmp());
			entrada.setCdConveCtaSalarial(lista.getCdConveCtaSalarial());
			entrada.setCdConvenNovo(lista.getCdConvnNovo());
			entrada.setCdCtrlCnpjEmp(lista.getCdControleCnpjEmp());
			entrada.setCdDigitoCtaEmp(lista.getCdDigitoConta());
			entrada.setCdFilialEmp(lista.getCdFilialCnpjEmp());
			entrada.setCdNumeroCtaDest(lista.getCdCta());
			if (lista.getCdConvnNovo() == 1) {
				entrada.setDsConvCtaEmp(getNomeRazaoRepresentante());				
			} else {
				entrada.setDsConvCtaEmp(lista.getDsConvn());				
			}
			entrada.setCdPessoaJuridica(listaContrato.getCdPessoaJuridica());
			entrada.setCdTipoContrato(listaContrato.getCdTipoContrato());
			entrada.setNrSequenciaContrato(listaContrato.getNrSequenciaContrato());

			IncluirVincConvnCtaSalarioSaidaDTO saida = getManterVinculacaoConvenioContaSalarioService().incluirVincConvnCtaSalario(entrada);

			detalharVinculacao();
			
			setCodigoConvenio(dtoConvenio.getCdConveCtaSalarial()==0?null:dtoConvenio.getCdConveCtaSalarial());
			setDecricaoConvenio(dtoConvenio.getDsConvCtaSalarial());
			setSituacaoConvenio(dtoConvenio.getCdSitConvCta()==1?"ATIVO":dtoConvenio.getCdSitConvCta()==2?"INATIVO":"");
			if (dtoConvenio.getCdBcoEmprConvn() != 0 || dtoConvenio.getCdAgenciaEmprConvn() != 0 || dtoConvenio.getCdCtaEmprConvn() != 0) {
				setBancoAgenciaContaConvenio(dtoConvenio.getCdBcoEmprConvn() + "/" + 
					dtoConvenio.getCdAgenciaEmprConvn() + "/" + 
						dtoConvenio.getCdCtaEmprConvn() + "-" + 
							dtoConvenio.getCdDigitoEmprConvn());
			}
			setDtHoraInclusao(dtoConvenio.getDtHoraInclusao());
			setCdUsuario(dtoConvenio.getCdUsuarioInclusao());

			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(saida.getCodMensagem(), 8) + ") " + saida.getMensagem(), "vincManterVinculacaoConvenioContaSalario", BradescoViewExceptionActionType.ACTION, false);
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
		}

		return "";
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {
		AlterarVincConvnCtaSalarioEntradaDTO entrada = new AlterarVincConvnCtaSalarioEntradaDTO();

		try {
			ListarContratosPgitSaidaDTO listaContrato = identificacaoClienteContratoBean.getListaGridPesquisa().get(
					identificacaoClienteContratoBean.getItemSelecionadoLista());
			ListarDadosCtaConvnListaOcorrenciasDTO lista = listaSelConvenio.get(itemSelecionadoListaConvenio);

			entrada.setCdAgencCtaEmp(lista.getCdAgencia());
			entrada.setCdBcoCtaEmp(lista.getCdBanco());
			entrada.setCdCnpjEmp(lista.getCdCpfCnpjEmp());
			entrada.setCdConveCtaSalarial(lista.getCdConveCtaSalarial());
			entrada.setCdConvenNovo(lista.getCdConvnNovo());
			entrada.setCdCtrlCnpjEmp(lista.getCdControleCnpjEmp());
			entrada.setCdDigitoCtaEmp(lista.getCdDigitoConta());
			entrada.setCdFilialEmp(lista.getCdFilialCnpjEmp());
			entrada.setCdNumeroCtaDest(lista.getCdCta());
			if (lista.getCdConvnNovo() == 1) {
				entrada.setDsConvCtaEmp(getNomeRazaoRepresentante());				
			} else {
				entrada.setDsConvCtaEmp(lista.getDsConvn());				
			}
			entrada.setCdPessoaJuridica(listaContrato.getCdPessoaJuridica());
			entrada.setCdTipoContrato(listaContrato.getCdTipoContrato());
			entrada.setNrSequenciaContrato(listaContrato.getNrSequenciaContrato());

			AlterarVincConvnCtaSalarioSaidaDTO saida = getManterVinculacaoConvenioContaSalarioService().alterarVincConvnCtaSalario(entrada);

			detalharVinculacao();

			setCodigoConvenio(dtoConvenio.getCdConveCtaSalarial()==0?null:dtoConvenio.getCdConveCtaSalarial());
			setDecricaoConvenio(dtoConvenio.getDsConvCtaSalarial());
			setSituacaoConvenio(dtoConvenio.getCdSitConvCta()==1?"ATIVO":dtoConvenio.getCdSitConvCta()==2?"INATIVO":"");
			if (dtoConvenio.getCdBcoEmprConvn() != 0 || dtoConvenio.getCdAgenciaEmprConvn() != 0 || dtoConvenio.getCdCtaEmprConvn() != 0) {
				setBancoAgenciaContaConvenio(dtoConvenio.getCdBcoEmprConvn() + "/" + 
					dtoConvenio.getCdAgenciaEmprConvn() + "/" + 
						dtoConvenio.getCdCtaEmprConvn() + "-" + 
							dtoConvenio.getCdDigitoEmprConvn());
			}
			setDtHoraInclusao(dtoConvenio.getDtHoraInclusao());
			setCdUsuario(dtoConvenio.getCdUsuarioInclusao());

			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(saida.getCodMensagem(), 8) + ") " + saida.getMensagem(), "vincManterVinculacaoConvenioContaSalario", BradescoViewExceptionActionType.ACTION, false);
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
		}

		return "";
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt) {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		try {
			ListarDadosCtaConvnEntradaDTO entrada = new ListarDadosCtaConvnEntradaDTO();
			listaSelConvenio = new ArrayList<ListarDadosCtaConvnListaOcorrenciasDTO>();
			listaControleSelConvenio = new ArrayList<SelectItem>();

			entrada.setCdClubPessoaRepresentante(saidaDTO.getCdClubRepresentante());
			entrada.setCdCpfCnpjRepresentante(saidaDTO.getCdCpfCnpjRepresentante());
			entrada.setCdControleCnpjRepresentante(saidaDTO.getCdControleCpfCnpjRepresentante());
			entrada.setCdFilialCnpjRepresentante(saidaDTO.getCdFilialCpfCnpjRepresentante());
			entrada.setCdPessoa(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
			entrada.setCdParmConvenio(CD_PARM_CONVENIO);
			
			saidaListarDadosCtaConvn = getManterVinculacaoConvenioContaSalarioService().listarDadosContaConvenio(entrada);
			setListaSelConvenio(saidaListarDadosCtaConvn.getOcorrencias());

			for (int i = 0; i < listaSelConvenio.size(); i++) {
				listaControleSelConvenio.add(new SelectItem(i, " "));
			}
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
		}
	}

	/**
	 * Is valida dados convenio.
	 *
	 * @return true, if is valida dados convenio
	 */
	public boolean isValidaDadosConvenio() {
		if (dtoConvenio.getCdBcoEmprConvn() != null && dtoConvenio.getCdBcoEmprConvn() != 0) {
			return false;
		}

		return true;
	}
	
	public boolean isDesabilitaBtnConvenio() {
	    if (getCodigoConvenio() != null && getCodigoConvenio().equals("S") && !isDesabilitaBotoesIndicadorConvenioNovo()) {
	        return false;
	    }
	    
	    return true;
	}
	
	public boolean isDesabilitaBtnConvenioTodos() {
	    if (getCodigoConvenio() != null && getCodigoConvenio().equals("N") && !isDesabilitaBotoesIndicadorConvenioNovo()) {
	        return false;
	    }
	    
	    return true;
	}
	
	public boolean isDesabilitaBotoesIndicadorConvenioNovo(){
		if(null == retornoSaidaDTO){
			return false;
		}else if(retornoSaidaDTO.getIndicadorConvenioNovo().equalsIgnoreCase("N")){
			return false;
		}else{
			return true;
		}
	}
	
	
	

	/**
	 * Detalhar folha pagamento.
	 *
	 * @return the string
	 */
	public String detalharFolhaPagamento() {
		FolhaPagamentoEntradaDto entrada = new FolhaPagamentoEntradaDto();
		
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
	
		vinculacao();
		
		FolhaPagamentoSaidaDto dtoSaida = new FolhaPagamentoSaidaDto();
		
		try	{
			entrada.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
			
			dtoSaida = getManterVinculacaoConvenioContaSalarioService().detalharFolhaPagamento(entrada);
			
			if (dtoSaida != null) {
				if (dtoSaida.getCdAberturaContaSalario() == 1) {
					setAberturaContaSalario("Ag�ncia");
				} else {
					setAberturaContaSalario("Bradesco Expresso");
				}
				if (dtoSaida.getCdIndicadorPagamentoMes() == 1 && dtoSaida.getCdIndicadorPagamentoQuinzena() == 1) {
					setPagamentoMes("MENSAL");
					setPagamentoQuinzena("QUINZENAL");
				}else if(dtoSaida.getCdIndicadorPagamentoMes() == 1){
					setPagamentoMes("MENSAL");
				}else if(dtoSaida.getCdIndicadorPagamentoQuinzena() == 1){
					setPagamentoMes("QUINZENAL");
				}
				setQtFuncionarioEmpresaPagadora(dtoSaida.getQtFuncionarioEmpresaPagadora().toString());
				setValorFolhaPagamentoEmpresa(dtoSaida.getVlFolhaPagamentoEmpresa());
				setVlMediaSalarialEmpresa(dtoSaida.getVlMediaSalarialEmpresa());
			} 
			
		} catch (PdcAdapterFunctionalException e) {
			if(!isVerificaMensagem()){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
			}
			limparDadosConvenio();
			setPaginaRetorno("");
			return "";
		}
		
		return "detManterVinculacaoConvenioContaSalario";
	}
	
	public String imprimirFolhaPagamentoPDF() {
		try {
			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
					.getExternalContext().getContext();
			String logoPath = servletContext.getRealPath(caminho);

			Map<String, Object> par = new HashMap<String, Object>();

			par.put("titulo", "Criar / Associar Conv�nio de Conta Sal�rio - Dados Folha de Pagamento");
			par.put("cpfCnpjRepresentante", getCpfCnpjRepresentante());
			par.put("nomeRazaoSocialRepresentante", getNomeRazaoRepresentante());
			par.put("grupoEconomico", getGrupEconRepresentante());
			par.put("atividadeEconomica", getAtivEconRepresentante());
			par.put("segmento", getSegRepresentante());
			par.put("subSegmento", getSubSegRepresentante());
			par.put("agenciaGestoraContrato", getDsAgenciaGestora());
			par.put("codigoFuncionalResponsavel", getDsGerenteResponsavel());
			par.put("cpfCnpjParticipante", getCpfCnpjParticipante());
			par.put("nomeRazaoSocialParticipante", getNomeRazaoParticipante());
			par.put("empresaGestora", getEmpresaContrato());
			par.put("tipo", getTipoContrato());
			par.put("numeroContrato", getNumeroContrato());
			par.put("descricaoContrato", getDescricaoContrato());
			par.put("situacaoContrato", getSituacaoContrato());
			par.put("motivoContrato", getMotivoContrato());
			par.put("nivelParticipacao", getParticipacaoContrato());
			par.put("valorFolhaPagamento", getValorFolhaPagamentoEmpresa());
			par.put("quantidadeFuncionarios", getQtFuncionarioEmpresaPagadora());
			par.put("mediaSalarial", getVlMediaSalarialEmpresa());
			par.put("pagamentoMensal", getPagamentoMes());
			par.put("pagamentoQuinzenal", getPagamentoQuinzena());
			par.put("aberturaContaSalario", getAberturaContaSalario());
			par.put("logoBradesco", logoPath);
			
			List<String> listaRelatorio = new ArrayList<String>();
			listaRelatorio.add("Criar / Associar Conv�nio de Conta Sal�rio - Dados Folha de Pagamento");

			try {
				// M�todo que gera o relat�rio PDF.
				PgitUtil.geraRelatorioPdf(
						"relatorios/manutencaoContrato/manterContrato/vinculacaoConvenioContaSalario",
						"detManterVinculacaoConvenioContaSalario",
						listaRelatorio,
						par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "";
	}

	/**
     * Detalhar piramide salarial.
     * 
     * @return the string
     */
	public String detalharPiramideSalarial() {
	    Integer selecaoLista = identificacaoClienteContratoBean.getItemSelecionadoLista();
	    if (selecaoLista != null) {
	        vinculacao();

	        ListarContratosPgitSaidaDTO saidaDTO =
                identificacaoClienteContratoBean.getListaGridPesquisa().get(selecaoLista);

	        DetalharPiramideEntradaDTO filtroDetalhePiramide = new DetalharPiramideEntradaDTO();
	       
	        filtroDetalhePiramide.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
	        filtroDetalhePiramide.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
	        filtroDetalhePiramide.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());

	        try{
	            
	            dadosPiramide = manterVinculacaoConvenioContaSalarioService.detalharPiramideSalarial(filtroDetalhePiramide);
	            
	        }catch (PdcAdapterFunctionalException e) {
	            if(!verificaMensagem){
	                BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") " + e.getMessage(), false);
	            }
	            
	            return null;
	        }

	        return "detPiramideSalarial";
	    }

	    return null;
	}

	/**
     * Imprimir piramide salarial pdf.
     * 
     * @return the string
     */
	public String imprimirPiramideSalarialPdf() {
        String caminhoLogo = "/images/Bradesco_logo.JPG";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext
                .getExternalContext().getContext();

        String caminhoRel = "relatorios/manutencaoContrato/manterContrato/vinculacaoConvenioContaSalario";

        Map<String, Object> par = new HashMap<String, Object>();

        par.put("titulo", "Criar / Associar Conv�nio de Conta Sal�rio - Pir�mide Salarial");
        par.put("cpfCnpjRepresentante", getCpfCnpjRepresentante());
        par.put("nomeRazaoSocialRepresentante", getNomeRazaoRepresentante());
        par.put("grupoEconomico", getGrupEconRepresentante());
        par.put("atividadeEconomica", getAtivEconRepresentante());
        par.put("segmento", getSegRepresentante());
        par.put("subSegmento", getSubSegRepresentante());
        par.put("agenciaGestoraContrato", getDsAgenciaGestora());
        par.put("codigoFuncionalResponsavel", getDsGerenteResponsavel());
        par.put("cpfCnpjParticipante", getCpfCnpjParticipante());
        par.put("nomeRazaoSocialParticipante", getNomeRazaoParticipante());
        par.put("empresaGestora", getEmpresaContrato());
        par.put("tipo", getTipoContrato());
        par.put("numeroContrato", getNumeroContrato());
        par.put("descricaoContrato", getDescricaoContrato());
        par.put("situacaoContrato", getSituacaoContrato());
        par.put("motivoContrato", getMotivoContrato());
        par.put("nivelParticipacao", getParticipacaoContrato());
        par.put("valorFolhaPagamento", getValorFolhaPagamentoEmpresa());
        par.put("quantidadeFuncionarios", getQtFuncionarioEmpresaPagadora());
        par.put("mediaSalarial", getVlMediaSalarialEmpresa());
        par.put("pagamentoMensal", getPagamentoMes());
        par.put("pagamentoQuinzenal", getPagamentoQuinzena());
        par.put("aberturaContaSalario", getAberturaContaSalario());
        par.put("logoBradesco", servletContext.getRealPath(caminhoLogo));
        par.put("caminhoJasper", servletContext.getRealPath(caminhoRel));

        par.put("bancos", new JRBeanCollectionDataSource(dadosPiramide.getOcorrencias5()));
        par.put("faixas", new JRBeanCollectionDataSource(dadosPiramide.getOcorrencias1()));
        par.put("rotatividades", new JRBeanCollectionDataSource(dadosPiramide.getOcorrencias2()));
        par.put("filiais", new JRBeanCollectionDataSource(dadosPiramide.getOcorrencias3()));
        par.put("creditos", new JRBeanCollectionDataSource(dadosPiramide.getOcorrencias4()));
        par.put("atendimentos", new JRBeanCollectionDataSource(dadosPiramide.getOcorrencias6()));
        
        par.put("totalQtdFuncionarioFilial01", dadosPiramide.getTotalQtdFuncionarioFilial01());
        par.put("totalQtdFuncionarioFilial02", dadosPiramide.getTotalQtdFuncionarioFilial02());
        par.put("totalQtdFuncionarioFilial03", dadosPiramide.getTotalQtdFuncionarioFilial03());
        par.put("totalQtdFuncionarioFilial04", dadosPiramide.getTotalQtdFuncionarioFilial04());
        par.put("totalQtdFuncionarioFilial05", dadosPiramide.getTotalQtdFuncionarioFilial05());
        par.put("totalQtdFuncionarioFilial06", dadosPiramide.getTotalQtdFuncionarioFilial06());
        par.put("totalQtdFuncionarioFilial07", dadosPiramide.getTotalQtdFuncionarioFilial07());
        par.put("totalQtdFuncionarioFilial08", dadosPiramide.getTotalQtdFuncionarioFilial08());
        par.put("totalQtdFuncionarioFilial09", dadosPiramide.getTotalQtdFuncionarioFilial09());
        par.put("totalQtdFuncionarioFilial10", dadosPiramide.getTotalQtdFuncionarioFilial10());
        par.put("totalQtdFuncCasaQuest", dadosPiramide.getTotalQtdFuncCasaQuest());
        par.put("totalQtdFuncDeslgQuest", dadosPiramide.getTotalQtdFuncDeslgQuest());
        par.put("totalPtcRenovFaixaQuest", dadosPiramide.getTotalPtcRenovFaixaQuest());
        
        par.put("vlPagamentoMes", dadosPiramide.getVlPagamentoMes());
        par.put("qtdFuncionarioFlPgto", dadosPiramide.getQtdFuncionarioFlPgto());
        par.put("vlMediaSalarialMes", dadosPiramide.getVlMediaSalarialMes());
        par.put("mesAnoCorrespondente", dadosPiramide.getMesAnoCorrespondente());
        
        String pagamento = "";
        if(dadosPiramide.getCdPagamentoSalarialQzena() == 1 && dadosPiramide.getCdPagamentoSalarialMes() != 1){
            pagamento = "Quinzenal";
        }
        
        if(dadosPiramide.getCdPagamentoSalarialMes() == 1 && dadosPiramide.getCdPagamentoSalarialQzena() != 1){
            pagamento = "Mensal";
        }
        
        if(dadosPiramide.getCdPagamentoSalarialMes() == 1 && dadosPiramide.getCdPagamentoSalarialQzena() == 1){
            pagamento = "Quinzenal e Mensal";
        }
        
        par.put("pagamento", pagamento);
        par.put("dsLocCtaSalarial", dadosPiramide.getDsLocCtaSalarial());
        par.put("indicadorGrupoEconomico", dadosPiramide.getIndicadorGrupoEconomico());
        par.put("codigoGrupoEconomico", dadosPiramide.getCodigoGrupoEconomico());
        par.put("distanciaAgEmpresa", dadosPiramide.getDistanciaAgEmpresa());
        par.put("dsLogradouroEmprQuest", dadosPiramide.getDsLogradouroEmprQuest());
        par.put("dsBairroEmprQuest", dadosPiramide.getDsBairroEmprQuest());
        par.put("cepEmpresa", dadosPiramide.getCepEmpresa());
        par.put("dsUfEmprQuest", dadosPiramide.getDsUfEmprQuest());
        par.put("dsMuncEmpreQuest", dadosPiramide.getDsMuncEmpreQuest());
        par.put("dsRamoAtividadeQuest", dadosPiramide.getDsRamoAtividadeQuest());
        par.put("telefoneEmpresa", dadosPiramide.getTelefoneEmpresa());
        par.put("dsSiteEmprQuest", dadosPiramide.getDsSiteEmprQuest());
        par.put("dsGerRelacionamentoQuest", dadosPiramide.getDsGerRelacionamentoQuest());
        par.put("telefoneContato", dadosPiramide.getTelefoneContato());
        par.put("dsAbrevSegmentoEmpr", dadosPiramide.getDsAbrevSegmentoEmpr());
        par.put("indicadorNaoPossuiBanco", dadosPiramide.getOcorrencias5().isEmpty());
        
        String formaPagamento = "";
        
        if(dadosPiramide.getCdPagamentoSalCredt() == 1){
            
            formaPagamento += "Cr�dito em conta ";
        }
        
        if(dadosPiramide.getCdPagamentoSalCreque() == 1){
            
            formaPagamento += "Cheque           ";
        }
        
        if(dadosPiramide.getCdPagamentoSalDinheiro() == 1){
            
            formaPagamento += "Dinheiro         ";
        }
        
        par.put("dsPagamentoSalDivers", formaPagamento + dadosPiramide.getDsPagamentoSalDivers());
        par.put("porcentagemAdmissaoDemissao", dadosPiramide.getPorcentagemAdmissaoDemissao());
        par.put("dsTextoJustfConc", dadosPiramide.getDsTextoJustfConc());
        par.put("indicadorNaoPossuiCredito", dadosPiramide.getOcorrencias4().isEmpty());
        par.put("indicadorPreferenciaCreditoConsignado", dadosPiramide.getIndicadorPreferenciaCreditoConsignado());
        par.put("atendimentoConcorrente", dadosPiramide.getAtendimentoConcorrente());
        par.put("dsTextoJustfRecip", dadosPiramide.getDsTextoJustfRecip());
        par.put("indicadorInstalacaoEstrutura", dadosPiramide.getIndicadorInstalacaoEstrutura());
        par.put("dsEstruturaInstaEmpr", dadosPiramide.getDsEstruturaInstaEmpr());
        par.put("dsAgenciaVincInsta", dadosPiramide.getDsAgenciaVincInsta());
        par.put("distanciaAgInstalacao", dadosPiramide.getDistanciaAgInstalacao());
        par.put("dsIndicadorVisitaInsta", dadosPiramide.getDsIndicadorVisitaInsta());
        par.put("dsGerVisitanteInsta", dadosPiramide.getDsGerVisitanteInsta());
        par.put("areaInstalacao", dadosPiramide.getAreaInstalacao());
        par.put("dsTipoNecessInsta", dadosPiramide.getDsTipoNecessInsta());
        par.put("dsIndicadorPerdarelacionamento", dadosPiramide.getDsIndicadorPerdarelacionamento());
        par.put("dsTextoJustfCompl", dadosPiramide.getDsTextoJustfCompl());

        try {
            PgitUtil.geraRelatorioPdf(caminhoRel, "detPiramideSalarial", new ArrayList<Object>(), par);
        } catch (Exception e) {
            throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
        }

        return "";
    }

	/**
	 * Get: manterVinculacaoConvenioContaSalarioService.
	 *
	 * @return manterVinculacaoConvenioContaSalarioService
	 */
	public IManterVinculacaoConvenioContaSalarioService getManterVinculacaoConvenioContaSalarioService() {
		return manterVinculacaoConvenioContaSalarioService;
	}

	/**
	 * Set: manterVinculacaoConvenioContaSalarioService.
	 *
	 * @param manterVinculacaoConvenioContaSalarioService the manter vinculacao convenio conta salario service
	 */
	public void setManterVinculacaoConvenioContaSalarioService(
			IManterVinculacaoConvenioContaSalarioService manterVinculacaoConvenioContaSalarioService) {
		this.manterVinculacaoConvenioContaSalarioService = manterVinculacaoConvenioContaSalarioService;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: identificacaoClienteBean.
	 *
	 * @return identificacaoClienteBean
	 */
	public IdentificacaoClienteBean getIdentificacaoClienteBean() {
		return identificacaoClienteBean;
	}

	/**
	 * Set: identificacaoClienteBean.
	 *
	 * @param identificacaoClienteBean the identificacao cliente bean
	 */
	public void setIdentificacaoClienteBean(
			IdentificacaoClienteBean identificacaoClienteBean) {
		this.identificacaoClienteBean = identificacaoClienteBean;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: habilitaBtoCont.
	 *
	 * @return habilitaBtoCont
	 */
	public Boolean getHabilitaBtoCont() {
		return habilitaBtoCont;
	}

	/**
	 * Set: habilitaBtoCont.
	 *
	 * @param habilitaBtoCont the habilita bto cont
	 */
	public void setHabilitaBtoCont(Boolean habilitaBtoCont) {
		this.habilitaBtoCont = habilitaBtoCont;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: foco.
	 *
	 * @return foco
	 */
	public String getFoco() {
		return foco;
	}

	/**
	 * Set: foco.
	 *
	 * @param foco the foco
	 */
	public void setFoco(String foco) {
		this.foco = foco;
	}

	/**
	 * Get: codigoConvenio.
	 *
	 * @return codigoConvenio
	 */
	public Long getCodigoConvenio() {
		return codigoConvenio;
	}

	/**
	 * Set: codigoConvenio.
	 *
	 * @param codigoConvenio the codigo convenio
	 */
	public void setCodigoConvenio(Long codigoConvenio) {
		this.codigoConvenio = codigoConvenio;
	}

	/**
	 * Get: cpfCnpjRepresentante.
	 *
	 * @return cpfCnpjRepresentante
	 */
	public String getCpfCnpjRepresentante() {
		return cpfCnpjRepresentante;
	}

	/**
	 * Set: cpfCnpjRepresentante.
	 *
	 * @param cpfCnpjRepresentante the cpf cnpj representante
	 */
	public void setCpfCnpjRepresentante(String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	/**
	 * Get: nomeRazaoRepresentante.
	 *
	 * @return nomeRazaoRepresentante
	 */
	public String getNomeRazaoRepresentante() {
		return nomeRazaoRepresentante;
	}

	/**
	 * Set: nomeRazaoRepresentante.
	 *
	 * @param nomeRazaoRepresentante the nome razao representante
	 */
	public void setNomeRazaoRepresentante(String nomeRazaoRepresentante) {
		this.nomeRazaoRepresentante = nomeRazaoRepresentante;
	}

	/**
	 * Get: grupEconRepresentante.
	 *
	 * @return grupEconRepresentante
	 */
	public String getGrupEconRepresentante() {
		return grupEconRepresentante;
	}

	/**
	 * Set: grupEconRepresentante.
	 *
	 * @param grupEconRepresentante the grup econ representante
	 */
	public void setGrupEconRepresentante(String grupEconRepresentante) {
		this.grupEconRepresentante = grupEconRepresentante;
	}

	/**
	 * Get: ativEconRepresentante.
	 *
	 * @return ativEconRepresentante
	 */
	public String getAtivEconRepresentante() {
		return ativEconRepresentante;
	}

	/**
	 * Set: ativEconRepresentante.
	 *
	 * @param ativEconRepresentante the ativ econ representante
	 */
	public void setAtivEconRepresentante(String ativEconRepresentante) {
		this.ativEconRepresentante = ativEconRepresentante;
	}

	/**
	 * Get: segRepresentante.
	 *
	 * @return segRepresentante
	 */
	public String getSegRepresentante() {
		return segRepresentante;
	}

	/**
	 * Set: segRepresentante.
	 *
	 * @param segRepresentante the seg representante
	 */
	public void setSegRepresentante(String segRepresentante) {
		this.segRepresentante = segRepresentante;
	}

	/**
	 * Get: subSegRepresentante.
	 *
	 * @return subSegRepresentante
	 */
	public String getSubSegRepresentante() {
		return subSegRepresentante;
	}

	/**
	 * Set: subSegRepresentante.
	 *
	 * @param subSegRepresentante the sub seg representante
	 */
	public void setSubSegRepresentante(String subSegRepresentante) {
		this.subSegRepresentante = subSegRepresentante;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: gerenteResponsavelFormatado.
	 *
	 * @return gerenteResponsavelFormatado
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}

	/**
	 * Set: gerenteResponsavelFormatado.
	 *
	 * @param gerenteResponsavelFormatado the gerente responsavel formatado
	 */
	public void setGerenteResponsavelFormatado(String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: cpfCnpjParticipanteTemp.
	 *
	 * @return cpfCnpjParticipanteTemp
	 */
	public String getCpfCnpjParticipanteTemp() {
		return cpfCnpjParticipanteTemp;
	}

	/**
	 * Set: cpfCnpjParticipanteTemp.
	 *
	 * @param cpfCnpjParticipanteTemp the cpf cnpj participante temp
	 */
	public void setCpfCnpjParticipanteTemp(String cpfCnpjParticipanteTemp) {
		this.cpfCnpjParticipanteTemp = cpfCnpjParticipanteTemp;
	}

	/**
	 * Get: nomeRazaoParticipanteTemp.
	 *
	 * @return nomeRazaoParticipanteTemp
	 */
	public String getNomeRazaoParticipanteTemp() {
		return nomeRazaoParticipanteTemp;
	}

	/**
	 * Set: nomeRazaoParticipanteTemp.
	 *
	 * @param nomeRazaoParticipanteTemp the nome razao participante temp
	 */
	public void setNomeRazaoParticipanteTemp(String nomeRazaoParticipanteTemp) {
		this.nomeRazaoParticipanteTemp = nomeRazaoParticipanteTemp;
	}

	/**
	 * Get: cpfCnpjParticipanteDet.
	 *
	 * @return cpfCnpjParticipanteDet
	 */
	public String getCpfCnpjParticipanteDet() {
		return cpfCnpjParticipanteDet;
	}

	/**
	 * Set: cpfCnpjParticipanteDet.
	 *
	 * @param cpfCnpjParticipanteDet the cpf cnpj participante det
	 */
	public void setCpfCnpjParticipanteDet(String cpfCnpjParticipanteDet) {
		this.cpfCnpjParticipanteDet = cpfCnpjParticipanteDet;
	}

	/**
	 * Get: nomeRazaoParticipanteDet.
	 *
	 * @return nomeRazaoParticipanteDet
	 */
	public String getNomeRazaoParticipanteDet() {
		return nomeRazaoParticipanteDet;
	}

	/**
	 * Set: nomeRazaoParticipanteDet.
	 *
	 * @param nomeRazaoParticipanteDet the nome razao participante det
	 */
	public void setNomeRazaoParticipanteDet(String nomeRazaoParticipanteDet) {
		this.nomeRazaoParticipanteDet = nomeRazaoParticipanteDet;
	}

	/**
	 * Get: grupoEconParticipante.
	 *
	 * @return grupoEconParticipante
	 */
	public String getGrupoEconParticipante() {
		return grupoEconParticipante;
	}

	/**
	 * Set: grupoEconParticipante.
	 *
	 * @param grupoEconParticipante the grupo econ participante
	 */
	public void setGrupoEconParticipante(String grupoEconParticipante) {
		this.grupoEconParticipante = grupoEconParticipante;
	}

	/**
	 * Get: atividadeEconParticipante.
	 *
	 * @return atividadeEconParticipante
	 */
	public String getAtividadeEconParticipante() {
		return atividadeEconParticipante;
	}

	/**
	 * Set: atividadeEconParticipante.
	 *
	 * @param atividadeEconParticipante the atividade econ participante
	 */
	public void setAtividadeEconParticipante(String atividadeEconParticipante) {
		this.atividadeEconParticipante = atividadeEconParticipante;
	}

	/**
	 * Get: segmentoParticipante.
	 *
	 * @return segmentoParticipante
	 */
	public String getSegmentoParticipante() {
		return segmentoParticipante;
	}

	/**
	 * Set: segmentoParticipante.
	 *
	 * @param segmentoParticipante the segmento participante
	 */
	public void setSegmentoParticipante(String segmentoParticipante) {
		this.segmentoParticipante = segmentoParticipante;
	}

	/**
	 * Get: subSegmentoParticipante.
	 *
	 * @return subSegmentoParticipante
	 */
	public String getSubSegmentoParticipante() {
		return subSegmentoParticipante;
	}

	/**
	 * Set: subSegmentoParticipante.
	 *
	 * @param subSegmentoParticipante the sub segmento participante
	 */
	public void setSubSegmentoParticipante(String subSegmentoParticipante) {
		this.subSegmentoParticipante = subSegmentoParticipante;
	}

	/**
	 * Get: dsTipoParticipacao.
	 *
	 * @return dsTipoParticipacao
	 */
	public String getDsTipoParticipacao() {
		return dsTipoParticipacao;
	}

	/**
	 * Set: dsTipoParticipacao.
	 *
	 * @param dsTipoParticipacao the ds tipo participacao
	 */
	public void setDsTipoParticipacao(String dsTipoParticipacao) {
		this.dsTipoParticipacao = dsTipoParticipacao;
	}

	/**
	 * Get: situacaoParticipacao.
	 *
	 * @return situacaoParticipacao
	 */
	public String getSituacaoParticipacao() {
		return situacaoParticipacao;
	}

	/**
	 * Set: situacaoParticipacao.
	 *
	 * @param situacaoParticipacao the situacao participacao
	 */
	public void setSituacaoParticipacao(String situacaoParticipacao) {
		this.situacaoParticipacao = situacaoParticipacao;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: dtManutencaoInicio.
	 *
	 * @return dtManutencaoInicio
	 */
	public Date getDtManutencaoInicio() {
		return dtManutencaoInicio;
	}

	/**
	 * Set: dtManutencaoInicio.
	 *
	 * @param dtManutencaoInicio the dt manutencao inicio
	 */
	public void setDtManutencaoInicio(Date dtManutencaoInicio) {
		this.dtManutencaoInicio = dtManutencaoInicio;
	}

	/**
	 * Get: dtManutencaoFim.
	 *
	 * @return dtManutencaoFim
	 */
	public Date getDtManutencaoFim() {
		return dtManutencaoFim;
	}

	/**
	 * Set: dtManutencaoFim.
	 *
	 * @param dtManutencaoFim the dt manutencao fim
	 */
	public void setDtManutencaoFim(Date dtManutencaoFim) {
		this.dtManutencaoFim = dtManutencaoFim;
	}

	/**
	 * Get: decricaoConvenio.
	 *
	 * @return decricaoConvenio
	 */
	public String getDecricaoConvenio() {
		return decricaoConvenio;
	}

	/**
	 * Set: decricaoConvenio.
	 *
	 * @param decricaoConvenio the decricao convenio
	 */
	public void setDecricaoConvenio(String decricaoConvenio) {
		this.decricaoConvenio = decricaoConvenio;
	}

	/**
	 * Get: situacaoConvenio.
	 *
	 * @return situacaoConvenio
	 */
	public String getSituacaoConvenio() {
		return situacaoConvenio;
	}

	/**
	 * Set: situacaoConvenio.
	 *
	 * @param situacaoConvenio the situacao convenio
	 */
	public void setSituacaoConvenio(String situacaoConvenio) {
		this.situacaoConvenio = situacaoConvenio;
	}

	/**
	 * Get: bancoAgenciaContaConvenio.
	 *
	 * @return bancoAgenciaContaConvenio
	 */
	public String getBancoAgenciaContaConvenio() {
		return bancoAgenciaContaConvenio;
	}

	/**
	 * Set: bancoAgenciaContaConvenio.
	 *
	 * @param bancoAgenciaContaConvenio the banco agencia conta convenio
	 */
	public void setBancoAgenciaContaConvenio(String bancoAgenciaContaConvenio) {
		this.bancoAgenciaContaConvenio = bancoAgenciaContaConvenio;
	}

	/**
	 * Get: dtoConvenio.
	 *
	 * @return dtoConvenio
	 */
	public DetalharVincConvnCtaSalarioSaidaDTO getDtoConvenio() {
		return dtoConvenio;
	}

	/**
	 * Set: dtoConvenio.
	 *
	 * @param dtoConvenio the dto convenio
	 */
	public void setDtoConvenio(DetalharVincConvnCtaSalarioSaidaDTO dtoConvenio) {
		this.dtoConvenio = dtoConvenio;
	}

	/**
	 * Get: btoAcao.
	 *
	 * @return btoAcao
	 */
	public String getBtoAcao() {
		return btoAcao;
	}

	/**
	 * Set: btoAcao.
	 *
	 * @param btoAcao the bto acao
	 */
	public void setBtoAcao(String btoAcao) {
		this.btoAcao = btoAcao;
	}

	/**
	 * Get: listaSelConvenio.
	 *
	 * @return listaSelConvenio
	 */
	public List<ListarDadosCtaConvnListaOcorrenciasDTO> getListaSelConvenio() {
		return listaSelConvenio;
	}

	/**
	 * Set: listaSelConvenio.
	 *
	 * @param listaSelConvenio the lista sel convenio
	 */
	public void setListaSelConvenio(
			List<ListarDadosCtaConvnListaOcorrenciasDTO> listaSelConvenio) {
		this.listaSelConvenio = listaSelConvenio;
	}

	/**
	 * Get: listaControleSelConvenio.
	 *
	 * @return listaControleSelConvenio
	 */
	public List<SelectItem> getListaControleSelConvenio() {
		return listaControleSelConvenio;
	}

	/**
	 * Set: listaControleSelConvenio.
	 *
	 * @param listaControleSelConvenio the lista controle sel convenio
	 */
	public void setListaControleSelConvenio(
			List<SelectItem> listaControleSelConvenio) {
		this.listaControleSelConvenio = listaControleSelConvenio;
	}

	/**
	 * Get: itemSelecionadoListaConvenio.
	 *
	 * @return itemSelecionadoListaConvenio
	 */
	public Integer getItemSelecionadoListaConvenio() {
		return itemSelecionadoListaConvenio;
	}

	/**
	 * Set: itemSelecionadoListaConvenio.
	 *
	 * @param itemSelecionadoListaConvenio the item selecionado lista convenio
	 */
	public void setItemSelecionadoListaConvenio(Integer itemSelecionadoListaConvenio) {
		this.itemSelecionadoListaConvenio = itemSelecionadoListaConvenio;
	}

	/**
	 * Get: dtHoraInclusao.
	 *
	 * @return dtHoraInclusao
	 */
	public String getDtHoraInclusao() {
		return dtHoraInclusao;
	}

	/**
	 * Set: dtHoraInclusao.
	 *
	 * @param dtHoraInclusao the dt hora inclusao
	 */
	public void setDtHoraInclusao(String dtHoraInclusao) {
		this.dtHoraInclusao = dtHoraInclusao;
	}

	/**
	 * Get: cdUsuario.
	 *
	 * @return cdUsuario
	 */
	public String getCdUsuario() {
		return cdUsuario;
	}

	/**
	 * Set: cdUsuario.
	 *
	 * @param cdUsuario the cd usuario
	 */
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: paginaRetorno.
	 *
	 * @return paginaRetorno
	 */
	public String getPaginaRetorno() {
		return paginaRetorno;
	}

	/**
	 * Set: paginaRetorno.
	 *
	 * @param paginaRetorno the pagina retorno
	 */
	public void setPaginaRetorno(String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}

	/**
	 * @return the valorFolhaPagamentoEmpresa
	 */
	public BigDecimal getValorFolhaPagamentoEmpresa() {
		return valorFolhaPagamentoEmpresa;
	}

	/**
	 * @param valorFolhaPagamentoEmpresa the valorFolhaPagamentoEmpresa to set
	 */
	public void setValorFolhaPagamentoEmpresa(BigDecimal valorFolhaPagamentoEmpresa) {
		this.valorFolhaPagamentoEmpresa = valorFolhaPagamentoEmpresa;
	}

	/**
	 * @return the qtFuncionarioEmpresaPagadora
	 */
	public String getQtFuncionarioEmpresaPagadora() {
		return qtFuncionarioEmpresaPagadora;
	}

	/**
	 * @param qtFuncionarioEmpresaPagadora the qtFuncionarioEmpresaPagadora to set
	 */
	public void setQtFuncionarioEmpresaPagadora(String qtFuncionarioEmpresaPagadora) {
		this.qtFuncionarioEmpresaPagadora = qtFuncionarioEmpresaPagadora;
	}

	/**
	 * @return the vlMediaSalarialEmpresa
	 */
	public BigDecimal getVlMediaSalarialEmpresa() {
		return vlMediaSalarialEmpresa;
	}

	/**
	 * @param vlMediaSalarialEmpresa the vlMediaSalarialEmpresa to set
	 */
	public void setVlMediaSalarialEmpresa(BigDecimal vlMediaSalarialEmpresa) {
		this.vlMediaSalarialEmpresa = vlMediaSalarialEmpresa;
	}

	/**
	 * @return the pagamentoMes
	 */
	public String getPagamentoMes() {
		return pagamentoMes;
	}

	/**
	 * @param pagamentoMes the pagamentoMes to set
	 */
	public void setPagamentoMes(String pagamentoMes) {
		this.pagamentoMes = pagamentoMes;
	}

	/**
	 * @return the pagamentoQuinzena
	 */
	public String getPagamentoQuinzena() {
		return pagamentoQuinzena;
	}

	/**
	 * @param pagamentoQuinzena the pagamentoQuinzena to set
	 */
	public void setPagamentoQuinzena(String pagamentoQuinzena) {
		this.pagamentoQuinzena = pagamentoQuinzena;
	}

	/**
	 * @return the aberturaContaSalario
	 */
	public String getAberturaContaSalario() {
		return aberturaContaSalario;
	}

	/**
	 * @param aberturaContaSalario the aberturaContaSalario to set
	 */
	public void setAberturaContaSalario(String aberturaContaSalario) {
		this.aberturaContaSalario = aberturaContaSalario;
	}

	/**
	 * @param verificaMensagem the verificaMensagem to set
	 */
	public void setVerificaMensagem(boolean verificaMensagem) {
		this.verificaMensagem = verificaMensagem;
	}

	/**
	 * @return the verificaMensagem
	 */
	public boolean isVerificaMensagem() {
		return verificaMensagem;
	}

	/**
	 * @return the saidaListarDadosCtaConvn
	 */
	public ListarDadosCtaConvnSaidaDTO getSaidaListarDadosCtaConvn() {
		return saidaListarDadosCtaConvn;
	}

	/**
	 * @param saidaListarDadosCtaConvn the saidaListarDadosCtaConvn to set
	 */
	public void setSaidaListarDadosCtaConvn(
			ListarDadosCtaConvnSaidaDTO saidaListarDadosCtaConvn) {
		this.saidaListarDadosCtaConvn = saidaListarDadosCtaConvn;
	}

    /**
     * Nome: getDadosPiramide
     *
     * @return dadosPiramide
     */
    public DetalharPiramideSaidaDTO getDadosPiramide() {
        return dadosPiramide;
    }

    /**
     * Nome: getLista
     *
     * @return lista
     */
    public List<Object> getLista() {
        return lista;
    }

    /**
     * Nome: setLista
     *
     * @param lista
     */
    public void setLista(List<Object> lista) {
        this.lista = lista;
    }

	public List<ListarDadosConvenioContaSalarioSaidaDTO> getListaOcorrenciasSaidaDTO() {
		return listaOcorrenciasSaidaDTO;
	}

	public void setListaOcorrenciasSaidaDTO(
			List<ListarDadosConvenioContaSalarioSaidaDTO> listaOcorrenciasSaidaDTO) {
		this.listaOcorrenciasSaidaDTO = listaOcorrenciasSaidaDTO;
	}

	public List<ListarDadosConvenioContaSalarioSaidaDTO> getListarDadosConvenioContaSalario() {
		return listarDadosConvenioContaSalario;
	}

	public void setListarDadosConvenioContaSalario(
			List<ListarDadosConvenioContaSalarioSaidaDTO> listarDadosConvenioContaSalario) {
		this.listarDadosConvenioContaSalario = listarDadosConvenioContaSalario;
	}

	public Integer getDesabilitaBotao() {
		return desabilitaBotao;
	}

	public void setDesabilitaBotao(Integer desabilitaBotao) {
		this.desabilitaBotao = desabilitaBotao;
	}

	public List<ListarDadosConvenioContaSalarioSaidaDTO> getListaSaidaDTO() {
		return listaSaidaDTO;
	}

	public void setListaSaidaDTO(
			List<ListarDadosConvenioContaSalarioSaidaDTO> listaSaidaDTO) {
		this.listaSaidaDTO = listaSaidaDTO;
	}

	public List<ListarVincConvnCtaSalarioOcorrenciasDTO> getListaDadosConvenio() {
		return listaDadosConvenio;
	}

	public void setListaDadosConvenio(
			List<ListarVincConvnCtaSalarioOcorrenciasDTO> listaDadosConvenio) {
		this.listaDadosConvenio = listaDadosConvenio;
	}

	public ListarVincConvnCtaSalarioSaidaDTO getRetornoSaidaDTO() {
		return retornoSaidaDTO;
	}

	public void setRetornoSaidaDTO(ListarVincConvnCtaSalarioSaidaDTO retornoSaidaDTO) {
		this.retornoSaidaDTO = retornoSaidaDTO;
	}
    
}