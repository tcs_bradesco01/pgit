/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.vincmsglanctopecontratada
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.vincmsglanctopecontratada;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.ILanctoPersonService;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ListarLanctoPersonalizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.lanctopersonalizado.bean.ListarLanctoPersonalizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.IVincMsgLanctOpeContratadaService;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.DetalharVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.DetalharVincMsgLancOperOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.DetalharVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ExcluirVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ExcluirVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.IncluirVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.IncluirVincMsgLancOperEntradaOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.IncluirVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarTiposPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarTiposPagtoSaidaOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarVincMsgLancOperEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteBean;


/**
 * Nome: VincMsgLanctoPersOpConBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class VincMsgLanctoPersOpConBean {
	
	//Constante Utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo MANTER_VINC_MSG_LANCTO_PERS_OPE_CONTRATADO. */
	private static final String MANTER_VINC_MSG_LANCTO_PERS_OPE_CONTRATADO = "conManterVincMsgLanctoPersOpeContratado";
	
	/** Atributo identificacaoClienteBean. */
	private IdentificacaoClienteBean identificacaoClienteBean;
	
	/** Atributo vincMsgLanctoPersOpConImpl. */
	private IVincMsgLanctOpeContratadaService vincMsgLanctoPersOpConImpl;
	
	/** Atributo manterLancamentoPersonalizadoServiceImpl. */
	private ILanctoPersonService manterLancamentoPersonalizadoServiceImpl;
	
	/** Atributo codigoLancamentoPersonalizadoFiltro. */
	private String codigoLancamentoPersonalizadoFiltro;
	
	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;
	
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer,String> listaTipoServicoHash = new HashMap<Integer,String>();
	
	/** Atributo modalidadeServicoFiltro. */
	private Integer modalidadeServicoFiltro;
	
	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo listaGridPesquisa. */
	private List<ListarVincMsgLancOperSaidaDTO> listaGridPesquisa;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoIncluir. */
	private List<SelectItem> listaModalidadeServicoIncluir = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeServicoIncluirHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
	
	/** Atributo listaGridPesquisaIncluir. */
	private List<ListarLanctoPersonalizadoSaidaDTO> listaGridPesquisaIncluir;
	
	/** Atributo itemSelecionadoListaIncluir. */
	private Integer itemSelecionadoListaIncluir;
	
	/** Atributo listaControleRadioIncluir. */
	private List<SelectItem> listaControleRadioIncluir = new ArrayList<SelectItem>();
	
	
	/** Atributo listaEmpresaGestora. */
	private List<SelectItem> listaEmpresaGestora= new ArrayList<SelectItem>();
	
	/** Atributo listaTipoContrato. */
	private List<SelectItem> listaTipoContrato = new ArrayList<SelectItem>();	
	
	/** Atributo saidaListarTiposPagto. */
	private ListarTiposPagtoSaidaDTO saidaListarTiposPagto = null;
	
	/** Atributo listaTiposPagto. */
	private List<ListarTiposPagtoSaidaOcorrenciasDTO> listaTiposPagto = new ArrayList<ListarTiposPagtoSaidaOcorrenciasDTO>();
	
	/** Atributo listaTiposPagto. */
	private List<ListarTiposPagtoSaidaOcorrenciasDTO> listaTiposPagtoSelecionados = new ArrayList<ListarTiposPagtoSaidaOcorrenciasDTO>();
	
	private List<DetalharVincMsgLancOperOcorrenciasSaidaDTO> listaDetalharVincMsgLancOper = new ArrayList<DetalharVincMsgLancOperOcorrenciasSaidaDTO>();

	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo codigoLancamentoPersonalizado. */
	private String codigoLancamentoPersonalizado;
	
	/** Atributo tipoServico. */
	private Integer tipoServico;
	
	/** Atributo modalidadeServico. */
	private Integer modalidadeServico;
	
	/** Atributo tipoServicoDesc. */
	private String tipoServicoDesc;
	
	/** Atributo modalidadeServicoDesc. */
	private String modalidadeServicoDesc;
	
	/** Atributo codigoLancamentoPersonalizadoDesc. */
	private String codigoLancamentoPersonalizadoDesc;
	
	/** Atributo lancDebitoDesc. */
	private String lancDebitoDesc;
	
	/** Atributo lancCreditoDesc. */
	private String lancCreditoDesc;
	
	/** Atributo tipoServicoIncluir. */
	private Integer tipoServicoIncluir;
	
	/** Atributo modalidadeServicoIncluir. */
	private Integer modalidadeServicoIncluir;
	
	/** Atributo tipoRelacionamentoIncluir. */
	private Integer tipoRelacionamentoIncluir;
	
	/** Atributo lancDebito. */
	private String lancDebito;
	
	/** Atributo lancCredito. */
	private String lancCredito;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo btoAcionadoIncluir. */
	private Boolean btoAcionadoIncluir;
	
	/** Atributo checkTodos. */
	private Boolean checkTodos;
	
	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: codigoLancamentoPersonalizado.
	 *
	 * @return codigoLancamentoPersonalizado
	 */
	public String getCodigoLancamentoPersonalizado() {
		return codigoLancamentoPersonalizado;
	}

	/**
	 * Set: codigoLancamentoPersonalizado.
	 *
	 * @param codigoLancamentoPersonalizado the codigo lancamento personalizado
	 */
	public void setCodigoLancamentoPersonalizado(
			String codigoLancamentoPersonalizado) {
		this.codigoLancamentoPersonalizado = codigoLancamentoPersonalizado;
	}



	/**
	 * Get: modalidadeServico.
	 *
	 * @return modalidadeServico
	 */
	public Integer getModalidadeServico() {
		return modalidadeServico;
	}



	/**
	 * Set: modalidadeServico.
	 *
	 * @param modalidadeServico the modalidade servico
	 */
	public void setModalidadeServico(Integer modalidadeServico) {
		this.modalidadeServico = modalidadeServico;
	}



	/**
	 * Get: tipoServico.
	 *
	 * @return tipoServico
	 */
	public Integer getTipoServico() {
		return tipoServico;
	}



	/**
	 * Set: tipoServico.
	 *
	 * @param tipoServico the tipo servico
	 */
	public void setTipoServico(Integer tipoServico) {
		this.tipoServico = tipoServico;
	}



	/**
	 * Limpar campos pesquisa cliente consultar.
	 */
	public void limparCamposPesquisaClienteConsultar(){
		
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdCpfCnpj(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdFilialCnpj(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdControleCnpj(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdCpf(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdControleCpf(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setDsNomeRazaoSocial("");
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdBanco(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdAgenciaBancaria(null);
		identificacaoClienteBean.getEntradaConsultarListaClientePessoas().setCdContaBancaria(null);
		
		
	}
	
	/**
	 * Limpar dados cliente.
	 *
	 * @return the string
	 */
	public String limparDadosCliente(){
		identificacaoClienteBean.limparCliente();
		limparArgumentosPesquisa();
		return "";
	}
	
	/**
	 * Limpar dados contrato.
	 *
	 * @return the string
	 */
	public String limparDadosContrato(){
		identificacaoClienteBean.limparContrato();
		limparArgumentosPesquisa();
		return "";
	}
	
	/**
	 * Limpar argumentos pesquisa.
	 *
	 * @return the string
	 */
	public String limparArgumentosPesquisa(){
		setCodigoLancamentoPersonalizadoFiltro("");
		setTipoServicoFiltro(0);
		setModalidadeServicoFiltro(0);
		limparGrid();
		setBtoAcionado(false);
		return "";
	}
	
	/**
	 * Limpar grid.
	 */
	public void limparGrid(){
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Vinc msg lancto pers op con bean.
	 */
	public VincMsgLanctoPersOpConBean(){
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		
		limparDadosIncluir();
		listarTipoServico();
		
		return "INCLUIR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		setItemSelecionadoListaIncluir(null);
		setCheckTodos(false);
		desmarcarCheckBoxListaTiposPagto();
		
		return "VOLTAR_INCLUIR";
	}
	
	private void desmarcarCheckBoxListaTiposPagto(){
		if (!saidaListarTiposPagto.getOcorrencias().isEmpty()) {
			for (int i = 0; i < saidaListarTiposPagto.getOcorrencias().size(); i++) {
				saidaListarTiposPagto.getOcorrencias().get(i).setCheckEvento(false);
			}			
		}
	}
	
	/**
	 * Voltar pesquisa.
	 *
	 * @return the string
	 */
	public String voltarPesquisa(){
		setItemSelecionadoLista(null);
		return "VOLTAR_PESQUISA";
	}
	
	/**
	 * Get: identificacaoClienteBean.
	 *
	 * @return identificacaoClienteBean
	 */
	public IdentificacaoClienteBean getIdentificacaoClienteBean() {
		return identificacaoClienteBean;
	}


	/**
	 * Set: identificacaoClienteBean.
	 *
	 * @param identificacaoClienteBean the identificacao cliente bean
	 */
	public void setIdentificacaoClienteBean(IdentificacaoClienteBean identificacaoClienteBean) {
		this.identificacaoClienteBean = identificacaoClienteBean;
	}


	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		
		limparDados();
		
		//carrega os combos do contrato
		try{
			listarEmpresaGestora();
		}catch(PdcAdapterFunctionalException e){
			listaEmpresaGestora = new ArrayList<SelectItem>();
		}
		
		try{
			listarTipoContrato();
		}catch(PdcAdapterFunctionalException e){
			listaTipoContrato = new ArrayList<SelectItem>();
		}
		
		try{
			listarTipoServico();
		}catch(PdcAdapterFunctionalException e){
			listaTipoServico = new ArrayList<SelectItem>();
		}
		
	}
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {
		
		identificacaoClienteBean.setPaginaCliente("identificaoVincMsgLanctoPersOpeContCliente");
		identificacaoClienteBean.setPaginaContrato("identificaoVincMsgLanctoPersOpeContContrato");
		identificacaoClienteBean.setPaginaRetorno(MANTER_VINC_MSG_LANCTO_PERS_OPE_CONTRATADO);
		identificacaoClienteBean.setItemClienteSelecionado(null);
		identificacaoClienteBean.setItemContratoSelecionado(null);
		
		
		this.identificacaoClienteBean.setItemFiltroSelecionado("");
		this.identificacaoClienteBean.setHabilitaFiltroDeContrato(true);
		this.identificacaoClienteBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteBean.setEntradaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasEntradaDTO());
		this.identificacaoClienteBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteBean.setSaidaConsultarListaContratosPessoas(new ConsultarListaContratosPessoasSaidaDTO());
		this.identificacaoClienteBean.setDesabilataFiltro(false);
		this.identificacaoClienteBean.setHabilitaFiltroDeCliente(false);
		
		this.codigoLancamentoPersonalizadoFiltro = "";
		this.setTipoServicoFiltro(0);
		this.setModalidadeServicoFiltro(0);
		this.setListaModalidadeServico(new ArrayList<SelectItem>());
		listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
		this.setListaGridPesquisa(null);
		this.setItemSelecionadoLista(null);
		this.identificacaoClienteBean.setBloqueaRadio(false);
		setBtoAcionado(false);
		identificacaoClienteBean.getEntradaConsultarListaContratosPessoas().setCdPessoaJuridicaContrato(2269651L);
		
		return "ok";
		
	}
	
	/**
	 * Limpar dados incluir.
	 *
	 * @return the string
	 */
	public String limparDadosIncluir() {
		
		
		this.codigoLancamentoPersonalizado = "";
		this.setTipoServico(0);
		this.setModalidadeServico(0);
		this.setListaModalidadeServicoIncluir(new ArrayList<SelectItem>());
		listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
		this.setListaGridPesquisaIncluir(null);
		this.setItemSelecionadoListaIncluir(null);
		this.setTipoServicoIncluir(0);
		this.setModalidadeServicoIncluir(0);
		this.setTipoRelacionamentoIncluir(0);
		this.setLancCreditoDesc("");
		this.setLancDebitoDesc("");
		setBtoAcionadoIncluir(false);
		
		return "ok";
		
	}

	

	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora(){
		
		this.listaEmpresaGestora = new ArrayList<SelectItem>();
		List<EmpresaGestoraSaidaDTO> list = new ArrayList<EmpresaGestoraSaidaDTO>();
		
		list = comboService.listarEmpresasGestoras();
		listaEmpresaGestora.clear();
		for(EmpresaGestoraSaidaDTO combo : list){
			
			
				listaEmpresaGestora.add(new SelectItem(combo.getCdPessoaJuridicaContrato(),combo.getDsCnpj()));
			
		}
		

	}
	
	/**
	 * Listar tipo contrato.
	 */
	public void listarTipoContrato(){
		
		List<TipoContratoSaidaDTO> list = comboService.listarTipoContrato();

		for (TipoContratoSaidaDTO saida : list){
			listaTipoContrato.add(new SelectItem(saida.getCdTipoContrato(), saida.getDsTipoContrato()));
		}
	}

	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: listaTipoContrato.
	 *
	 * @return listaTipoContrato
	 */
	public List<SelectItem> getListaTipoContrato() {
		return listaTipoContrato;
	}

	/**
	 * Set: listaTipoContrato.
	 *
	 * @param listaTipoContrato the lista tipo contrato
	 */
	public void setListaTipoContrato(List<SelectItem> listaTipoContrato) {
		this.listaTipoContrato = listaTipoContrato;
	}

	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}
	
	/**
	 * Listar tipo servico.
	 */
	private void listarTipoServico() {
		
		this.listaTipoServico = new ArrayList<SelectItem>();
		
		List<ListarServicosSaidaDTO> saidaDTO = new ArrayList<ListarServicosSaidaDTO>();
		
		saidaDTO = comboService.listarTipoServicos(1);
		
		listaTipoServicoHash.clear();
		for(ListarServicosSaidaDTO combo : saidaDTO){
			listaTipoServicoHash.put(combo.getCdServico(),combo.getDsServico());
			this.listaTipoServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
		}
			
	}

	/**
	 * Get: codigoLancamentoPersonalizadoFiltro.
	 *
	 * @return codigoLancamentoPersonalizadoFiltro
	 */
	public String getCodigoLancamentoPersonalizadoFiltro() {
		return codigoLancamentoPersonalizadoFiltro;
	}

	/**
	 * Set: codigoLancamentoPersonalizadoFiltro.
	 *
	 * @param codigoLancamentoPersonalizadoFiltro the codigo lancamento personalizado filtro
	 */
	public void setCodigoLancamentoPersonalizadoFiltro(
			String codigoLancamentoPersonalizadoFiltro) {
		this.codigoLancamentoPersonalizadoFiltro = codigoLancamentoPersonalizadoFiltro;
	}

	/**
	 * Get: listaModalidadeServico.
	 *
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {
		return listaModalidadeServico;
	}

	/**
	 * Set: listaModalidadeServico.
	 *
	 * @param listaModalidadeServico the lista modalidade servico
	 */
	public void setListaModalidadeServico(List<SelectItem> listaModalidadeServico) {
		this.listaModalidadeServico = listaModalidadeServico;
	}

	/**
	 * Get: listaModalidadeServicoHash.
	 *
	 * @return listaModalidadeServicoHash
	 */
	Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoHash() {
		return listaModalidadeServicoHash;
	}

	/**
	 * Set lista modalidade servico hash.
	 *
	 * @param listaModalidadeServicoHash the lista modalidade servico hash
	 */
	void setListaModalidadeServicoHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash) {
		this.listaModalidadeServicoHash = listaModalidadeServicoHash;
	}

	/**
	 * Get: modalidadeServicoFiltro.
	 *
	 * @return modalidadeServicoFiltro
	 */
	public Integer getModalidadeServicoFiltro() {
		return modalidadeServicoFiltro;
	}

	/**
	 * Set: modalidadeServicoFiltro.
	 *
	 * @param modalidadeServicoFiltro the modalidade servico filtro
	 */
	public void setModalidadeServicoFiltro(Integer modalidadeServicoFiltro) {
		this.modalidadeServicoFiltro = modalidadeServicoFiltro;
	}
	
	/**
	 * Listar modalidade servico.
	 */
	public void listarModalidadeServico(){
		
		listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
		listaModalidadeServico = new ArrayList<SelectItem>();
		
		if( getTipoServicoFiltro()!=null && getTipoServicoFiltro() !=0){
			List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
			ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
			
			listarModalidadeEntradaDTO.setCdServico(getTipoServicoFiltro());
		
			listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
			
			listaModalidadeServicoHash.clear();
			
			for(ListarModalidadeSaidaDTO combo : listaModalidades){				
				this.listaModalidadeServico.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
				listaModalidadeServicoHash.put(combo.getCdModalidade(),combo);
				
			}
		}else{
			listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
			listaModalidadeServico.clear();
			setModalidadeServicoFiltro(0);
			setTipoServicoFiltro(0);
		}
	}

	/**
	 * Get: vincMsgLanctoPersOpConImpl.
	 *
	 * @return vincMsgLanctoPersOpConImpl
	 */
	public IVincMsgLanctOpeContratadaService getVincMsgLanctoPersOpConImpl() {
		return vincMsgLanctoPersOpConImpl;
	}

	/**
	 * Set: vincMsgLanctoPersOpConImpl.
	 *
	 * @param vincMsgLanctoPersOpConImpl the vinc msg lancto pers op con impl
	 */
	public void setVincMsgLanctoPersOpConImpl(
			IVincMsgLanctOpeContratadaService vincMsgLanctoPersOpConImpl) {
		this.vincMsgLanctoPersOpConImpl = vincMsgLanctoPersOpConImpl;
	}
	
	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista() {
		
		try{
			
			ListarVincMsgLancOperEntradaDTO entradaDTO = new ListarVincMsgLancOperEntradaDTO();
			
			entradaDTO.setCodigoHistorico(!getCodigoLancamentoPersonalizadoFiltro().equals("")?Integer.parseInt(getCodigoLancamentoPersonalizadoFiltro()):0);
			entradaDTO.setTipoServico(getTipoServicoFiltro()!=null?getTipoServicoFiltro():0);
			entradaDTO.setModalidadeServico(getModalidadeServicoFiltro()!=null?getModalidadeServicoFiltro():0);
			entradaDTO.setTipoRelacionamento(getModalidadeServicoFiltro() == null || getModalidadeServicoFiltro() == 0 || listaModalidadeServicoHash.isEmpty()? 0 :listaModalidadeServicoHash.get(getModalidadeServicoFiltro()).getCdRelacionamentoProduto());
			entradaDTO.setNumeroContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio() == null ? 0L : this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			entradaDTO.setPessoaJurContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			entradaDTO.setTipoContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());

			setListaGridPesquisa(getVincMsgLanctoPersOpConImpl().listarVincMsgLancOper(entradaDTO));
			setBtoAcionado(true);
			this.listaControleRadio =  new ArrayList<SelectItem>();
			for (int i = 0; i <= getListaGridPesquisa().size();i++) {
				this.listaControleRadio.add(new SelectItem(i," "));
			}
			
			setItemSelecionadoLista(null);
			
			identificacaoClienteBean.setHabilitaFiltroDeContrato(true);
			identificacaoClienteBean.setHabilitaFiltroDeCliente(true);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridPesquisa(null);
			
			return "";

		}
		
		return "";
		
	}
	
	/**
	 * paginarLista.
	 *
	 * @void
	*/
	public void paginarLista(ActionEvent evt){
		carregaLista();
	}
	
	public void paginarListaIncluir(ActionEvent event){
	    consultarIncluir();
	}

	/**
	 * isHabilitaConsu.
	 *
	 * @return boolean
	 */
	public boolean isHabilitaConsu(){
		if (!"".equals(identificacaoClienteBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())
				&& identificacaoClienteBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				|| !"".equals(identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getDsPessoaJuridicaContrato())
				&& identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getDsPessoaJuridicaContrato() != null) {
			return false;
		}
		
		return true;
	}
	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<ListarVincMsgLancOperSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(
			List<ListarVincMsgLancOperSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}



	/**
	 * Get: listaModalidadeServicoIncluir.
	 *
	 * @return listaModalidadeServicoIncluir
	 */
	public List<SelectItem> getListaModalidadeServicoIncluir() {
		return listaModalidadeServicoIncluir;
	}



	/**
	 * Set: listaModalidadeServicoIncluir.
	 *
	 * @param listaModalidadeServicoIncluir the lista modalidade servico incluir
	 */
	public void setListaModalidadeServicoIncluir(
			List<SelectItem> listaModalidadeServicoIncluir) {
		this.listaModalidadeServicoIncluir = listaModalidadeServicoIncluir;
	}

	/**
	 * Get: listaModalidadeServicoIncluirHash.
	 *
	 * @return listaModalidadeServicoIncluirHash
	 */
	Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoIncluirHash() {
		return listaModalidadeServicoIncluirHash;
	}

	/**
	 * Set lista modalidade servico incluir hash.
	 *
	 * @param listaModalidadeServicoIncluirHash the lista modalidade servico incluir hash
	 */
	void setListaModalidadeServicoIncluirHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoIncluirHash) {
		this.listaModalidadeServicoIncluirHash = listaModalidadeServicoIncluirHash;
	}

	/**
	 * Get: listaGridPesquisaIncluir.
	 *
	 * @return listaGridPesquisaIncluir
	 */
	public List<ListarLanctoPersonalizadoSaidaDTO> getListaGridPesquisaIncluir() {
		return listaGridPesquisaIncluir;
	}



	/**
	 * Set: listaGridPesquisaIncluir.
	 *
	 * @param listaGridPesquisaIncluir the lista grid pesquisa incluir
	 */
	public void setListaGridPesquisaIncluir(
			List<ListarLanctoPersonalizadoSaidaDTO> listaGridPesquisaIncluir) {
		this.listaGridPesquisaIncluir = listaGridPesquisaIncluir;
	}



	/**
	 * Get: itemSelecionadoListaIncluir.
	 *
	 * @return itemSelecionadoListaIncluir
	 */
	public Integer getItemSelecionadoListaIncluir() {
		return itemSelecionadoListaIncluir;
	}



	/**
	 * Set: itemSelecionadoListaIncluir.
	 *
	 * @param itemSelecionadoListaIncluir the item selecionado lista incluir
	 */
	public void setItemSelecionadoListaIncluir(Integer itemSelecionadoListaIncluir) {
		this.itemSelecionadoListaIncluir = itemSelecionadoListaIncluir;
	}



	/**
	 * Get: listaControleRadioIncluir.
	 *
	 * @return listaControleRadioIncluir
	 */
	public List<SelectItem> getListaControleRadioIncluir() {
		return listaControleRadioIncluir;
	}

	/**
	 * Set: listaControleRadioIncluir.
	 *
	 * @param listaControleRadioIncluir the lista controle radio incluir
	 */
	public void setListaControleRadioIncluir(
			List<SelectItem> listaControleRadioIncluir) {
		this.listaControleRadioIncluir = listaControleRadioIncluir;
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
			
		ListarLanctoPersonalizadoSaidaDTO listarLanctoPersonalizadoSaidaDTO = getListaGridPesquisaIncluir().get(getItemSelecionadoListaIncluir());
		
		this.setCodigoLancamentoPersonalizadoDesc(String.valueOf(listarLanctoPersonalizadoSaidaDTO.getCodLancamento()));
		this.setLancCreditoDesc(listarLanctoPersonalizadoSaidaDTO.getDsLancCredito());
		this.setLancDebitoDesc(listarLanctoPersonalizadoSaidaDTO.getDsLancDebito());
		this.setTipoServicoDesc(listarLanctoPersonalizadoSaidaDTO.getDsTipoServico());
		this.setModalidadeServicoDesc(listarLanctoPersonalizadoSaidaDTO.getDsTipoModalidade());
		this.setTipoServicoIncluir(listarLanctoPersonalizadoSaidaDTO.getCdTipoServico());
		this.setModalidadeServicoIncluir(listarLanctoPersonalizadoSaidaDTO.getCdTipoModalidade());
		this.setTipoRelacionamentoIncluir(listarLanctoPersonalizadoSaidaDTO.getCdTipoRelacionamento());
		
		try {
			setSaidaListarTiposPagto(vincMsgLanctoPersOpConImpl.listarTiposPagto());
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
		
		return "CONFIRMAR_INCLUIR";
	
	}


	/**
	 * Get: modalidadeServicoDesc.
	 *
	 * @return modalidadeServicoDesc
	 */
	public String getModalidadeServicoDesc() {
		return modalidadeServicoDesc;
	}



	/**
	 * Set: modalidadeServicoDesc.
	 *
	 * @param modalidadeServicoDesc the modalidade servico desc
	 */
	public void setModalidadeServicoDesc(String modalidadeServicoDesc) {
		this.modalidadeServicoDesc = modalidadeServicoDesc;
	}



	/**
	 * Get: tipoServicoDesc.
	 *
	 * @return tipoServicoDesc
	 */
	public String getTipoServicoDesc() {
		return tipoServicoDesc;
	}



	/**
	 * Set: tipoServicoDesc.
	 *
	 * @param tipoServicoDesc the tipo servico desc
	 */
	public void setTipoServicoDesc(String tipoServicoDesc) {
		this.tipoServicoDesc = tipoServicoDesc;
	}
	
	/**
	 * Consultar incluir.
	 *
	 * @return the string
	 */
	public String consultarIncluir() {
		
		try{
			
			ListarLanctoPersonalizadoEntradaDTO entradaDTO = new ListarLanctoPersonalizadoEntradaDTO();
			
			entradaDTO.setCodLancamento(!getCodigoLancamentoPersonalizado().equals("")?Integer.parseInt(getCodigoLancamentoPersonalizado()):0);
			entradaDTO.setTipoServico(getTipoServico()!=null?getTipoServico():0);
			entradaDTO.setModalidadeServico(getModalidadeServico()!=null?getModalidadeServico():0);
			entradaDTO.setTipoRelacionamento(getModalidadeServico() == null || getModalidadeServico() == 0 || listaModalidadeServicoIncluirHash.isEmpty()? 0 :listaModalidadeServicoIncluirHash.get(getModalidadeServico()).getCdRelacionamentoProduto());

			setListaGridPesquisaIncluir(getManterLancamentoPersonalizadoServiceImpl().listarLanctoPersonalizado(entradaDTO));
			setBtoAcionadoIncluir(true);
			this.listaControleRadioIncluir =  new ArrayList<SelectItem>();
			for (int i = 0; i <= getListaGridPesquisaIncluir().size();i++) {
				this.listaControleRadioIncluir.add(new SelectItem(i," "));
			}
			
			setItemSelecionadoListaIncluir(null);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridPesquisaIncluir(null);

			return "";

		}
		
		return "";
		
	}



	/**
	 * Get: manterLancamentoPersonalizadoServiceImpl.
	 *
	 * @return manterLancamentoPersonalizadoServiceImpl
	 */
	public ILanctoPersonService getManterLancamentoPersonalizadoServiceImpl() {
		return manterLancamentoPersonalizadoServiceImpl;
	}



	/**
	 * Set: manterLancamentoPersonalizadoServiceImpl.
	 *
	 * @param manterLancamentoPersonalizadoServiceImpl the manter lancamento personalizado service impl
	 */
	public void setManterLancamentoPersonalizadoServiceImpl(
			ILanctoPersonService manterLancamentoPersonalizadoServiceImpl) {
		this.manterLancamentoPersonalizadoServiceImpl = manterLancamentoPersonalizadoServiceImpl;
	}
	
	/**
	 * Listar modalidade servico incluir.
	 */
	public void listarModalidadeServicoIncluir(){
		
		listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> (); 
		listaModalidadeServicoIncluir = new ArrayList<SelectItem>();
		
		if( getTipoServico()!=null && getTipoServico()!=0){
			List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
			ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();
			
			listarModalidadeEntradaDTO.setCdServico(getTipoServico());
		
			listaModalidades = comboService.listarModalidades(listarModalidadeEntradaDTO);
			
			listaModalidadeServicoIncluirHash.clear();
			
			for(ListarModalidadeSaidaDTO combo : listaModalidades){				
				this.listaModalidadeServicoIncluir.add(new SelectItem(combo.getCdModalidade(),combo.getDsModalidade()));
				listaModalidadeServicoIncluirHash.put(combo.getCdModalidade(),combo);
				
			}
		}else{
			listaModalidadeServicoIncluirHash = new HashMap<Integer, ListarModalidadeSaidaDTO> ();
			listaModalidadeServicoIncluir.clear();
			setModalidadeServico(0);
			setTipoServico(0);
		}
	}



	/**
	 * Get: codigoLancamentoPersonalizadoDesc.
	 *
	 * @return codigoLancamentoPersonalizadoDesc
	 */
	public String getCodigoLancamentoPersonalizadoDesc() {
		return codigoLancamentoPersonalizadoDesc;
	}



	/**
	 * Set: codigoLancamentoPersonalizadoDesc.
	 *
	 * @param codigoLancamentoPersonalizadoDesc the codigo lancamento personalizado desc
	 */
	public void setCodigoLancamentoPersonalizadoDesc(
			String codigoLancamentoPersonalizadoDesc) {
		this.codigoLancamentoPersonalizadoDesc = codigoLancamentoPersonalizadoDesc;
	}



	/**
	 * Get: lancCreditoDesc.
	 *
	 * @return lancCreditoDesc
	 */
	public String getLancCreditoDesc() {
		return lancCreditoDesc;
	}



	/**
	 * Set: lancCreditoDesc.
	 *
	 * @param lancCreditoDesc the lanc credito desc
	 */
	public void setLancCreditoDesc(String lancCreditoDesc) {
		this.lancCreditoDesc = lancCreditoDesc;
	}



	/**
	 * Get: lancDebitoDesc.
	 *
	 * @return lancDebitoDesc
	 */
	public String getLancDebitoDesc() {
		return lancDebitoDesc;
	}



	/**
	 * Set: lancDebitoDesc.
	 *
	 * @param lancDebitoDesc the lanc debito desc
	 */
	public void setLancDebitoDesc(String lancDebitoDesc) {
		this.lancDebitoDesc = lancDebitoDesc;
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try {
			
			IncluirVincMsgLancOperEntradaDTO incluirVincMsgLancOperEntradaDTO = new IncluirVincMsgLancOperEntradaDTO();
			
			incluirVincMsgLancOperEntradaDTO.setCodigoHistorico(Integer.parseInt(getCodigoLancamentoPersonalizadoDesc()));
			incluirVincMsgLancOperEntradaDTO.setTipoServico(getTipoServicoIncluir());
			incluirVincMsgLancOperEntradaDTO.setModalidadeServico(getModalidadeServicoIncluir());
			incluirVincMsgLancOperEntradaDTO.setTipoRelacionamento(getTipoRelacionamentoIncluir());
			incluirVincMsgLancOperEntradaDTO.setNumeroContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			incluirVincMsgLancOperEntradaDTO.setPessoaJurContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			incluirVincMsgLancOperEntradaDTO.setTipoContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
			incluirVincMsgLancOperEntradaDTO.setOcorrencias(adicionarItemSelecionadoIncluir());
			

			IncluirVincMsgLancOperSaidaDTO incluirVincMsgLancOperSaidaDTO = getVincMsgLanctoPersOpConImpl().incluirVincMsgLancOper(incluirVincMsgLancOperEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +incluirVincMsgLancOperSaidaDTO.getCodMensagem() + ") " + incluirVincMsgLancOperSaidaDTO.getMensagem(), MANTER_VINC_MSG_LANCTO_PERS_OPE_CONTRATADO, BradescoViewExceptionActionType.ACTION, false);
			
			carregaLista();	
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		
		return "";

	}	
	
	private List<IncluirVincMsgLancOperEntradaOcorrenciasDTO> adicionarItemSelecionadoIncluir(){
		
		List<IncluirVincMsgLancOperEntradaOcorrenciasDTO> listaIncluir = new ArrayList<IncluirVincMsgLancOperEntradaOcorrenciasDTO>();
		
		for (int i = 0; i < listaTiposPagtoSelecionados.size(); i++) {
			IncluirVincMsgLancOperEntradaOcorrenciasDTO ocorrencias = new IncluirVincMsgLancOperEntradaOcorrenciasDTO();
			
			ocorrencias.setCdTipoPagamentoCliente(listaTiposPagtoSelecionados.get(i).getCdTipoPagamentoCliente());
			
			listaIncluir.add(ocorrencias);
		}
		
		return listaIncluir;
	}
	
	public String avancarTelaConfirmacaoIncluir(){
		
		List<ListarTiposPagtoSaidaOcorrenciasDTO> ocorrencias = saidaListarTiposPagto.getOcorrencias();
		
		listaTiposPagtoSelecionados.clear();
		
		if (!ocorrencias.isEmpty()) {
			for (ListarTiposPagtoSaidaOcorrenciasDTO element : ocorrencias) {
				if (element.getCheckEvento()) {
					listaTiposPagtoSelecionados.add(element);
				}
			}			
		}		
		
		return "AVANCAR"; 
	}

	public void marcarTodos() {
		List<ListarTiposPagtoSaidaOcorrenciasDTO> ocorrencias = saidaListarTiposPagto.getOcorrencias();

		if (!ocorrencias.isEmpty()) {
			for (ListarTiposPagtoSaidaOcorrenciasDTO element : ocorrencias) {
				element.setCheckEvento(getCheckTodos());
			}
		}
	}
	

	/**
	 * Get: modalidadeServicoIncluir.
	 *
	 * @return modalidadeServicoIncluir
	 */
	public Integer getModalidadeServicoIncluir() {
		return modalidadeServicoIncluir;
	}



	/**
	 * Set: modalidadeServicoIncluir.
	 *
	 * @param modalidadeServicoIncluir the modalidade servico incluir
	 */
	public void setModalidadeServicoIncluir(Integer modalidadeServicoIncluir) {
		this.modalidadeServicoIncluir = modalidadeServicoIncluir;
	}



	/**
	 * Get: tipoServicoIncluir.
	 *
	 * @return tipoServicoIncluir
	 */
	public Integer getTipoServicoIncluir() {
		return tipoServicoIncluir;
	}



	/**
	 * Set: tipoServicoIncluir.
	 *
	 * @param tipoServicoIncluir the tipo servico incluir
	 */
	public void setTipoServicoIncluir(Integer tipoServicoIncluir) {
		this.tipoServicoIncluir = tipoServicoIncluir;
	}



	/**
	 * Get: tipoRelacionamentoIncluir.
	 *
	 * @return tipoRelacionamentoIncluir
	 */
	public Integer getTipoRelacionamentoIncluir() {
		return tipoRelacionamentoIncluir;
	}



	/**
	 * Set: tipoRelacionamentoIncluir.
	 *
	 * @param tipoRelacionamentoIncluir the tipo relacionamento incluir
	 */
	public void setTipoRelacionamentoIncluir(Integer tipoRelacionamentoIncluir) {
		this.tipoRelacionamentoIncluir = tipoRelacionamentoIncluir;
	}
	
	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		
//		ListarVincMsgLancOperSaidaDTO listarVincMsgLancOperSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());		
		
		DetalharVincMsgLancOperEntradaDTO entradaDTO = new DetalharVincMsgLancOperEntradaDTO();
		
		entradaDTO.setCdPessoaJuridica(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
		entradaDTO.setCdProdutoServicoOperacao(getListaGridPesquisa().get(getItemSelecionadoLista()).getTipoServico());
		entradaDTO.setCdprodutoOperacaoRelacionado(getListaGridPesquisa().get(getItemSelecionadoLista()).getModalidadeServico());
		entradaDTO.setCdRelacionamentoProdutoProduto(getListaGridPesquisa().get(getItemSelecionadoLista()).getTipoRelacionamento());
		entradaDTO.setCdTipoContratoNegocio(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
		entradaDTO.setCdMensagemLinhaExtrato(getListaGridPesquisa().get(getItemSelecionadoLista()).getCodigoHistorico());

		DetalharVincMsgLancOperSaidaDTO detalharVincMsgLancOperSaidaDTO = getVincMsgLanctoPersOpConImpl().detalharVincMsgLancOper(entradaDTO);
		
		listaDetalharVincMsgLancOper = detalharVincMsgLancOperSaidaDTO.getOcorrencias();

		setCodigoLancamentoPersonalizado(String.valueOf(detalharVincMsgLancOperSaidaDTO.getCdMensagemLinhaExtrato()));
		setLancCreditoDesc(detalharVincMsgLancOperSaidaDTO.getCdIdentificadorLancamentoCredito()==0? "" :detalharVincMsgLancOperSaidaDTO.getCdIdentificadorLancamentoCredito() + " - "+ detalharVincMsgLancOperSaidaDTO.getDsIdentificadorLancamentoCredito());
		setLancDebitoDesc(detalharVincMsgLancOperSaidaDTO.getCdIdentificadorLancamentoDebito()==0? "" :detalharVincMsgLancOperSaidaDTO.getCdIdentificadorLancamentoDebito() +  " - " + detalharVincMsgLancOperSaidaDTO.getDsIdentificadorLancamentoDebito());
		setLancCredito(detalharVincMsgLancOperSaidaDTO.getCdIdentificadorLancamentoCredito()==0? "" : String.valueOf(detalharVincMsgLancOperSaidaDTO.getDsIdentificadorLancamentoCredito()));
		setLancDebito(detalharVincMsgLancOperSaidaDTO.getCdIdentificadorLancamentoDebito()==0? "" : String.valueOf(detalharVincMsgLancOperSaidaDTO.getDsIdentificadorLancamentoDebito()));
		setTipoServico(detalharVincMsgLancOperSaidaDTO.getCdProdutoServicoOperacao());
		setTipoServicoDesc(detalharVincMsgLancOperSaidaDTO.getDsProdutoServicoOperacao());
		setModalidadeServico(detalharVincMsgLancOperSaidaDTO.getCdProdutoOperacaoRelacionado());
		setModalidadeServicoDesc(detalharVincMsgLancOperSaidaDTO.getDsProdutoOperacaoRelacionado());
		setTipoRelacionamentoIncluir(detalharVincMsgLancOperSaidaDTO.getCdRelacionamentoProdutoProduto());
		
		setUsuarioInclusao(detalharVincMsgLancOperSaidaDTO.getCdAutenticacaoSegurancaInclusao());
		setUsuarioManutencao(detalharVincMsgLancOperSaidaDTO.getCdAutenticacaoSegurancaManutencao());
		
		setComplementoInclusao(detalharVincMsgLancOperSaidaDTO.getDsCanalInclusao().equals("0")? "" : detalharVincMsgLancOperSaidaDTO.getDsCanalInclusao());
		setComplementoManutencao(detalharVincMsgLancOperSaidaDTO.getDsCanalManutencao().equals("0")? "" : detalharVincMsgLancOperSaidaDTO.getDsCanalManutencao());
		
		setTipoCanalInclusao(detalharVincMsgLancOperSaidaDTO.getCdCanalInclusao()==0? "" : detalharVincMsgLancOperSaidaDTO.getCdCanalInclusao() + " - " +  detalharVincMsgLancOperSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(detalharVincMsgLancOperSaidaDTO.getCdCanalManutencao()==0? "" : detalharVincMsgLancOperSaidaDTO.getCdCanalManutencao() + " - " + detalharVincMsgLancOperSaidaDTO.getDsCanalManutencao()); 
		
		setDataHoraInclusao(detalharVincMsgLancOperSaidaDTO.getHrInclusaoRegistro());
		setDataHoraManutencao(detalharVincMsgLancOperSaidaDTO.getHrManutencaoRegistro());
		
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
	
	try {
		preencheDados();
	} catch (PdcAdapterFunctionalException p) {
		BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), MANTER_VINC_MSG_LANCTO_PERS_OPE_CONTRATADO, BradescoViewExceptionActionType.ACTION, false);
		return null;
	} 
	return "DETALHAR";

}

	/**
	 * Get: lancCredito.
	 *
	 * @return lancCredito
	 */
	public String getLancCredito() {
		return lancCredito;
	}

	/**
	 * Set: lancCredito.
	 *
	 * @param lancCredito the lanc credito
	 */
	public void setLancCredito(String lancCredito) {
		this.lancCredito = lancCredito;
	}

	/**
	 * Get: lancDebito.
	 *
	 * @return lancDebito
	 */
	public String getLancDebito() {
		return lancDebito;
	}

	/**
	 * Set: lancDebito.
	 *
	 * @param lancDebito the lanc debito
	 */
	public void setLancDebito(String lancDebito) {
		this.lancDebito = lancDebito;
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), MANTER_VINC_MSG_LANCTO_PERS_OPE_CONTRATADO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 

		return "EXCLUIR";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try {
			
			ListarVincMsgLancOperSaidaDTO listarVincMsgLancOperSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());
			
			ExcluirVincMsgLancOperEntradaDTO entradaDTO = new ExcluirVincMsgLancOperEntradaDTO();

			entradaDTO.setCodigoHistorico(listarVincMsgLancOperSaidaDTO.getCodigoHistorico());
			entradaDTO.setTipoServico(listarVincMsgLancOperSaidaDTO.getTipoServico());
			entradaDTO.setModalidadeServico(listarVincMsgLancOperSaidaDTO.getModalidadeServico());
			entradaDTO.setTipoRelacionamento(listarVincMsgLancOperSaidaDTO.getTipoRelacionamento());
			entradaDTO.setNumeroContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getNrSeqContratoNegocio());
			entradaDTO.setPessoaJurContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdPessoaJuridicaContrato());
			entradaDTO.setTipoContrato(this.identificacaoClienteBean.getSaidaConsultarListaContratosPessoas().getCdTipoContratoNegocio());
						
			ExcluirVincMsgLancOperSaidaDTO excluirVincMsgLancOperSaidaDTO = getVincMsgLanctoPersOpConImpl().excluirVincMsgLancOper(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + excluirVincMsgLancOperSaidaDTO.getCodMensagem() + ") " + excluirVincMsgLancOperSaidaDTO.getMensagem(), MANTER_VINC_MSG_LANCTO_PERS_OPE_CONTRATADO, BradescoViewExceptionActionType.ACTION, false);

			carregaLista();
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";

	}


	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar(){
		identificacaoClienteBean.limparCliente();
		identificacaoClienteBean.limparContrato();
		limparArgumentosPesquisa();
		return "";
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: btoAcionadoIncluir.
	 *
	 * @return btoAcionadoIncluir
	 */
	public Boolean getBtoAcionadoIncluir() {
		return btoAcionadoIncluir;
	}

	/**
	 * Set: btoAcionadoIncluir.
	 *
	 * @param btoAcionadoIncluir the bto acionado incluir
	 */
	public void setBtoAcionadoIncluir(Boolean btoAcionadoIncluir) {
		this.btoAcionadoIncluir = btoAcionadoIncluir;
	}

	public ListarTiposPagtoSaidaDTO getSaidaListarTiposPagto() {
		return saidaListarTiposPagto;
	}

	public void setSaidaListarTiposPagto(
			ListarTiposPagtoSaidaDTO saidaListarTiposPagto) {
		this.saidaListarTiposPagto = saidaListarTiposPagto;
	}

	public List<ListarTiposPagtoSaidaOcorrenciasDTO> getListaTiposPagto() {
		return listaTiposPagto;
	}

	public void setListaTiposPagto(
			List<ListarTiposPagtoSaidaOcorrenciasDTO> listaTiposPagto) {
		this.listaTiposPagto = listaTiposPagto;
	}

	public Boolean getCheckTodos() {
		return checkTodos;
	}

	public void setCheckTodos(Boolean checkTodos) {
		this.checkTodos = checkTodos;
	}

	public List<ListarTiposPagtoSaidaOcorrenciasDTO> getListaTiposPagtoSelecionados() {
		return listaTiposPagtoSelecionados;
	}

	public void setListaTiposPagtoSelecionados(
			List<ListarTiposPagtoSaidaOcorrenciasDTO> listaTiposPagtoSelecionados) {
		this.listaTiposPagtoSelecionados = listaTiposPagtoSelecionados;
	}

	public List<DetalharVincMsgLancOperOcorrenciasSaidaDTO> getListaDetalharVincMsgLancOper() {
		return listaDetalharVincMsgLancOper;
	}

	public void setListaDetalharVincMsgLancOper(
			List<DetalharVincMsgLancOperOcorrenciasSaidaDTO> listaDetalharVincMsgLancOper) {
		this.listaDetalharVincMsgLancOper = listaDetalharVincMsgLancOper;
	}
}
