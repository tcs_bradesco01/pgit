/*
 * Nome: br.com.bradesco.web.pgit.view.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean;

import java.util.List;

import javax.faces.event.ActionEvent;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.pgit.service.business.confrontodep.IConfrontoDepService;
import br.com.bradesco.web.pgit.service.business.confrontodep.bean.ConfrontoDepVO;

/**
 * Nome: ScrollableDataTablePagBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ScrollableDataTablePagBean {

	/** Atributo codEmpresa. */
	private String codEmpresa;
	
	/** Atributo codDependencia. */
	private String codDependencia;
	
	/** Atributo dtReferenciaDe. */
	private String dtReferenciaDe;
	
	/** Atributo dtReferenciaAte. */
	private String dtReferenciaAte;
	
	/** Atributo maxOcorrencias. */
	private String maxOcorrencias;
	
	/** Atributo tipoConta. */
	private String tipoConta;
	
    /** Atributo dependencias. */
    private List<ConfrontoDepVO> dependencias;
    
    /** Atributo confrontoDependenciaService. */
    private IConfrontoDepService confrontoDependenciaService;
    
	/**
	 * Nome: setDependencias
	 * <p>Prop�sito: M�todo set para o atributo dependencias. </p>
	 * @param List<ListaRegraVO> dependencias
	 */
	public void setDependencias(List<ConfrontoDepVO> dependencias) {
		this.dependencias = dependencias;
	}

	/**
	 * Carrega lista.
	 *
	 * @param evt the evt
	 */
	public void carregaLista(ActionEvent evt) {
		ConfrontoDepVO vo = new ConfrontoDepVO();
		
		vo.setCdConta("0");
		vo.setCodDependencia(this.getCodDependencia());
		vo.setCodEmpresa(this.getCodEmpresa());
		vo.setCodTipoSaldo("0");
		vo.setDescConta("0");
		vo.setDescEmpresa("0");
		vo.setDtContabil("01/01/2008");
		vo.setDtReferenciaAte(this.getDtReferenciaAte());
		vo.setDtReferenciaDe(this.getDtReferenciaDe());
		vo.setMaxOcorrencia(this.getMaxOcorrencias());
		vo.setTipoConta(this.getTipoConta());
		
		try {
			this.setDependencias(this.confrontoDependenciaService.listarConfrontoDependencia(vo));
		} catch(Exception e) {
			throw new BradescoViewException(e.getMessage(), e, null);
		}
	}
	
	/**
	 * Get: dependencias.
	 *
	 * @return dependencias
	 */
	public List<ConfrontoDepVO> getDependencias() {
		ConfrontoDepVO vo = new ConfrontoDepVO();
		
		vo.setCdConta("0");
		vo.setCodDependencia(this.getCodDependencia());
		vo.setCodEmpresa(this.getCodEmpresa());
		vo.setCodTipoSaldo("0");
		vo.setDescConta("0");
		vo.setDescEmpresa("0");
		vo.setDtContabil("01/01/2008");
		vo.setDtReferenciaAte(this.getDtReferenciaAte());
		vo.setDtReferenciaDe(this.getDtReferenciaDe());
		vo.setMaxOcorrencia(this.getMaxOcorrencias());
		vo.setTipoConta(this.getTipoConta());
		
		try {
			this.setDependencias(this.confrontoDependenciaService.listarConfrontoDependencia(vo));
		} catch(Exception e) {
			throw new BradescoViewException(e.getMessage(), e, null);
		}
		return this.dependencias;
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param event the event
	 */
	public void pesquisar(ActionEvent event) { 
		ConfrontoDepVO vo = new ConfrontoDepVO();
		
		vo.setCdConta("0");
		vo.setCodDependencia(this.getCodDependencia());
		vo.setCodEmpresa(this.getCodEmpresa());
		vo.setCodTipoSaldo("0");
		vo.setDescConta("0");
		vo.setDescEmpresa("0");
		vo.setDtContabil("01/01/2008");
		vo.setDtReferenciaAte(this.getDtReferenciaAte());
		vo.setDtReferenciaDe(this.getDtReferenciaDe());
		vo.setMaxOcorrencia(this.getMaxOcorrencias());
		vo.setTipoConta(this.getTipoConta());
		
		try {
			this.setDependencias(this.confrontoDependenciaService.listarConfrontoDependencia(vo));
		} catch(Exception e) {
			throw new BradescoViewException(e.getMessage(), e, null);
		}
	}

	/**
	 * Nome: getCodEmpresa
	 * <p>Prop�sito: M�todo get para o atributo codEmpresa. </p>
	 * @return String codEmpresa
	 */
	public String getCodEmpresa() {
		return codEmpresa;
	}

	/**
	 * Nome: setCodEmpresa
	 * <p>Prop�sito: M�todo set para o atributo codEmpresa. </p>
	 * @param String codEmpresa
	 */
	public void setCodEmpresa(String codEmpresa) {
		this.codEmpresa = codEmpresa;
	}

	/**
	 * Nome: getCodDependencia
	 * <p>Prop�sito: M�todo get para o atributo codDependencia. </p>
	 * @return String codDependencia
	 */
	public String getCodDependencia() {
		return codDependencia;
	}

	/**
	 * Nome: setCodDependencia
	 * <p>Prop�sito: M�todo set para o atributo codDependencia. </p>
	 * @param String codDependencia
	 */
	public void setCodDependencia(String codDependencia) {
		this.codDependencia = codDependencia;
	}

	/**
	 * Nome: getDtReferenciaDe
	 * <p>Prop�sito: M�todo get para o atributo dtReferenciaDe. </p>
	 * @return String dtReferenciaDe
	 */
	public String getDtReferenciaDe() {
		return dtReferenciaDe;
	}

	/**
	 * Nome: setDtReferenciaDe
	 * <p>Prop�sito: M�todo set para o atributo dtReferenciaDe. </p>
	 * @param String dtReferenciaDe
	 */
	public void setDtReferenciaDe(String dtReferenciaDe) {
		this.dtReferenciaDe = dtReferenciaDe;
	}

	/**
	 * Nome: getDtReferenciaAte
	 * <p>Prop�sito: M�todo get para o atributo dtReferenciaAte. </p>
	 * @return String dtReferenciaAte
	 */
	public String getDtReferenciaAte() {
		return dtReferenciaAte;
	}

	/**
	 * Nome: setDtReferenciaAte
	 * <p>Prop�sito: M�todo set para o atributo dtReferenciaAte. </p>
	 * @param String dtReferenciaAte
	 */
	public void setDtReferenciaAte(String dtReferenciaAte) {
		this.dtReferenciaAte = dtReferenciaAte;
	}

	/**
	 * Nome: getMaxOcorrencias
	 * <p>Prop�sito: M�todo get para o atributo maxOcorrencias. </p>
	 * @return String maxOcorrencias
	 */
	public String getMaxOcorrencias() {
		return maxOcorrencias;
	}

	/**
	 * Nome: setMaxOcorrencias
	 * <p>Prop�sito: M�todo set para o atributo maxOcorrencias. </p>
	 * @param String maxOcorrencias
	 */
	public void setMaxOcorrencias(String maxOcorrencias) {
		this.maxOcorrencias = maxOcorrencias;
	}

	/**
	 * Nome: getTipoConta
	 * <p>Prop�sito: M�todo get para o atributo tipoConta. </p>
	 * @return String tipoConta
	 */
	public String getTipoConta() {
		return tipoConta;
	}

	/**
	 * Nome: setTipoConta
	 * <p>Prop�sito: M�todo set para o atributo tipoConta. </p>
	 * @param String tipoConta
	 */
	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}

	/**
	 * Nome: getConfrontoDependenciaService
	 * <p>Prop�sito: M�todo get para o atributo confrontoDependenciaService. </p>
	 * @return IConfrontoDepService confrontoDependenciaService
	 */
	public IConfrontoDepService getConfrontoDependenciaService() {
		return confrontoDependenciaService;
	}

	/**
	 * Nome: setConfrontoDependenciaService
	 * <p>Prop�sito: M�todo set para o atributo confrontoDependenciaService. </p>
	 * @param IConfrontoDepService confrontoDependenciaService
	 */
	public void setConfrontoDependenciaService(
			IConfrontoDepService confrontoDependenciaService) {
		this.confrontoDependenciaService = confrontoDependenciaService;
	}	
}