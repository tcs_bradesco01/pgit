/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantermensagemlayout
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.mantermensagemlayout;
		

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.IManterMensagemLayoutService;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.AlterarMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.AlterarMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ConsultarMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ConsultarMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.DetalharMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.DetalharMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ExcluirMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.ExcluirMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.IncluirMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.IncluirMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ManterMensagemLayoutBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterMensagemLayoutBean {
	
	/** Atributo manterMensagemLayoutImpl. */
	private IManterMensagemLayoutService manterMensagemLayoutImpl;
	
	/** Atributo comboServiceImpl. */
	private IComboService comboServiceImpl;
	
	/** Atributo cdTipoLayoutArquivoFiltro. */
	private Integer cdTipoLayoutArquivoFiltro;
	
	/** Atributo listaTipoLayoutArquivoFiltro. */
	private List<SelectItem> listaTipoLayoutArquivoFiltro = new ArrayList<SelectItem>();
	
	/** Atributo codigoMensagemFiltro. */
	private String codigoMensagemFiltro;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo dsTipoLayoutArquivo. */
	private String dsTipoLayoutArquivo;
	
	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer,String> listaTipoLayoutArquivoHash = new HashMap<Integer,String>();
	
	/** Atributo codigoMensagem. */
	private String codigoMensagem;
	
	/** Atributo cdTipoMensagem. */
	private Integer cdTipoMensagem;
	
	/** Atributo cdNivelMensagem. */
	private Integer cdNivelMensagem;
	
	/** Atributo btoAcionado. */
	private boolean btoAcionado;
	
	/** Atributo posicaoInicialCampo. */
	private String posicaoInicialCampo;
	
	/** Atributo posicaoFinalCampo. */
	private String posicaoFinalCampo;
	
	/** Atributo descricaoMensagemCliente. */
	private String descricaoMensagemCliente;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaGrid. */
	private List<ConsultarMsgLayoutArqRetornoSaidaDTO> listaGrid;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();	
	
	/** Atributo saidaTipoLayoutArquivoSaidaDTO. */
	private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;
	
	//M�todos
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		listarTipoLayoutArquivoFiltro();
		setCdTipoLayoutArquivoFiltro(0);
		setCodigoMensagemFiltro("");
		setItemSelecionadoLista(null);
		setListaGrid(null);
		setBtoAcionado(false);
	}
	
	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos(){
		setCdTipoLayoutArquivoFiltro(0);
		setCodigoMensagemFiltro("");
		setBtoAcionado(false);
		setListaGrid(null);
		setItemSelecionadoLista(null);
		return "";
	}
	
	/**
	 * Listar tipo layout arquivo filtro.
	 */
	public void listarTipoLayoutArquivoFiltro(){
		try{
			listaTipoLayoutArquivoFiltro = new ArrayList<SelectItem>();
			
			TipoLayoutArquivoEntradaDTO entrada = new TipoLayoutArquivoEntradaDTO();
			entrada.setCdSituacaoVinculacaoConta(0);
			
			saidaTipoLayoutArquivoSaidaDTO = comboServiceImpl.listarTipoLayoutArquivo(entrada);
			List<TipoLayoutArquivoSaidaDTO> list = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
			
			for (TipoLayoutArquivoSaidaDTO saida : list){
				listaTipoLayoutArquivoFiltro.add(new SelectItem(saida.getCdTipoLayoutArquivo(), PgitUtil.concatenarCampos(saida.getCdTipoLayoutArquivo(), saida.getDsTipoLayoutArquivo())));
			}
			
		} catch (PdcAdapterFunctionalException p) {
			listaTipoLayoutArquivoFiltro = new ArrayList<SelectItem>();			
		}			
	}	
	
	/**
	 * Listar tipo layout arquivo.
	 */
	public void listarTipoLayoutArquivo(){
		try{
			listaTipoLayoutArquivoHash.clear();
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
			
			TipoLayoutArquivoEntradaDTO entrada = new TipoLayoutArquivoEntradaDTO();
			entrada.setCdSituacaoVinculacaoConta(0);
			
			saidaTipoLayoutArquivoSaidaDTO = comboServiceImpl.listarTipoLayoutArquivo(entrada);
			List<TipoLayoutArquivoSaidaDTO> list = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
			
			for (TipoLayoutArquivoSaidaDTO saida : list){
				listaTipoLayoutArquivo.add(new SelectItem(saida.getCdTipoLayoutArquivo(), PgitUtil.concatenarCampos(saida.getCdTipoLayoutArquivo(), saida.getDsTipoLayoutArquivo())));
				listaTipoLayoutArquivoHash.put(saida.getCdTipoLayoutArquivo(), PgitUtil.concatenarCampos(saida.getCdTipoLayoutArquivo(), saida.getDsTipoLayoutArquivo()));
			}
			
		} catch (PdcAdapterFunctionalException p) {
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();			
		}		
	}	
	
	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar(){
		try{
			setBtoAcionado(true);
			
			ConsultarMsgLayoutArqRetornoEntradaDTO entrada = new ConsultarMsgLayoutArqRetornoEntradaDTO();
			entrada.setCdTipoLayoutArquivo(getCdTipoLayoutArquivoFiltro());
			entrada.setCdMensagemArquivoRetorno(getCodigoMensagemFiltro());
			
			setListaGrid(getManterMensagemLayoutImpl().consultarMsgLayoutArqRetorno(entrada));
			
			setItemSelecionadoLista(null);			
			this.listaControleRadio =  new ArrayList<SelectItem>();
			
			for (int i = 0; i < getListaGrid().size();i++) {
				this.listaControleRadio.add(new SelectItem(i," "));
			}
		
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setBtoAcionado(false);
		}		
		
		return "";
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt){
		consultar();
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try{
			preencheDados();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
		return "DETALHAR";
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		listarTipoLayoutArquivo();
		setCdTipoLayoutArquivo(0);
		setCodigoMensagem("");
		setCdTipoMensagem(0);
		setCdNivelMensagem(0);
		setPosicaoInicialCampo("");
		setPosicaoFinalCampo("");
		setDescricaoMensagemCliente("");
		return "INCLUIR";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		setDsTipoLayoutArquivo((String)listaTipoLayoutArquivoHash.get(getCdTipoLayoutArquivo()));
		return "AVANCAR";
	}
	
	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar(){
		return "AVANCAR";
	}
	
	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar(){
		try{
			preencheDados();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
		return "ALTERAR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try{
			preencheDados();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
		return "EXCLUIR";
	}
	
	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		return "VOLTAR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try {
			IncluirMsgLayoutArqRetornoEntradaDTO entradaDTO = new IncluirMsgLayoutArqRetornoEntradaDTO();
			entradaDTO.setCdTipoLayoutArquivo(getCdTipoLayoutArquivo());
			entradaDTO.setCdMensagemArquivoRetorno(getCodigoMensagem());
			entradaDTO.setCdTipoMensagemRetorno(getCdTipoMensagem());
			entradaDTO.setCdNivelMensagemLayout(getCdNivelMensagem());
			entradaDTO.setNrPosicaoinicialmensagem(getPosicaoInicialCampo() != null && !getPosicaoInicialCampo().equals("") ? Integer.parseInt(getPosicaoInicialCampo()) : 0);
			entradaDTO.setNrPosicaoFinalMensagem(getPosicaoFinalCampo() != null && !getPosicaoFinalCampo().equals("") ? Integer.parseInt(getPosicaoFinalCampo()) : 0);
			entradaDTO.setDsMensagemLayoutRetorno(getDescricaoMensagemCliente());
			
			IncluirMsgLayoutArqRetornoSaidaDTO saidaDTO = getManterMensagemLayoutImpl().incluirMsgLayoutArqRetorno(entradaDTO);
	
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conManterMensagemLayout", BradescoViewExceptionActionType.ACTION, false);
			
			if(getListaGrid() != null){
				consultar();
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar(){
		try {
			ConsultarMsgLayoutArqRetornoSaidaDTO registroSelecionado = getListaGrid().get(getItemSelecionadoLista());
			AlterarMsgLayoutArqRetornoEntradaDTO entradaDTO = new AlterarMsgLayoutArqRetornoEntradaDTO();
			entradaDTO.setCdTipoLayoutArquivo(getCdTipoLayoutArquivo());
			entradaDTO.setNrMensagemArquivoRetorno(registroSelecionado.getNrMensagemArquivoRetorno());
			entradaDTO.setCdMensagemArquivoRetorno(getCodigoMensagem());
			entradaDTO.setCdTipoMensagemRetorno(getCdTipoMensagem());
			entradaDTO.setNrPosicaoInicialMensagem(getPosicaoInicialCampo() != null && !getPosicaoInicialCampo().equals("") ? Integer.parseInt(getPosicaoInicialCampo()) : 0);
			entradaDTO.setNrPosicaoFinalMensagem(getPosicaoFinalCampo() != null && !getPosicaoFinalCampo().equals("") ? Integer.parseInt(getPosicaoFinalCampo()) : 0);
			entradaDTO.setCdNivelMensagemLayout(getCdNivelMensagem());
			entradaDTO.setDsMensagemLayoutRetorno(getDescricaoMensagemCliente());
			
			AlterarMsgLayoutArqRetornoSaidaDTO saidaDTO = getManterMensagemLayoutImpl().alterarMsgLayoutArqRetorno(entradaDTO);
	
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conManterMensagemLayout", BradescoViewExceptionActionType.ACTION, false);
			
			if(getListaGrid() != null){
				consultar();
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try {
			ConsultarMsgLayoutArqRetornoSaidaDTO registroSelecionado = getListaGrid().get(getItemSelecionadoLista());
			ExcluirMsgLayoutArqRetornoEntradaDTO entradaDTO = new ExcluirMsgLayoutArqRetornoEntradaDTO();
			entradaDTO.setCdTipoLayoutArquivo(registroSelecionado.getCdTipoLayoutArquivo());
			entradaDTO.setNrMensagemArquivoRetorno(registroSelecionado.getNrMensagemArquivoRetorno());
			//entradaDTO.setCdMensagemArquivoRetorno(registroSelecionado.getCdMensagemArquivoRetorno());
			
			ExcluirMsgLayoutArqRetornoSaidaDTO saidaDTO = getManterMensagemLayoutImpl().excluirMsgLayoutArqRetorno(entradaDTO);
	
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conManterMensagemLayout", BradescoViewExceptionActionType.ACTION, false);
			
			consultar();
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		ConsultarMsgLayoutArqRetornoSaidaDTO registroSelecionado = getListaGrid().get(getItemSelecionadoLista());
		DetalharMsgLayoutArqRetornoEntradaDTO entradaDTO = new DetalharMsgLayoutArqRetornoEntradaDTO();
		
		entradaDTO.setCdTipoLayoutArquivo(registroSelecionado.getCdTipoLayoutArquivo());
		entradaDTO.setNrMensagemArquivoRetorno(registroSelecionado.getNrMensagemArquivoRetorno());
		entradaDTO.setCdMensagemArquivoRetorno(registroSelecionado.getCdMensagemArquivoRetorno());
		
		DetalharMsgLayoutArqRetornoSaidaDTO saidaDTO = getManterMensagemLayoutImpl().detalharMsgLayoutArqRetorno(entradaDTO);
		
		setCdTipoLayoutArquivo(saidaDTO.getCdTipoLayoutArquivo());
		setDsTipoLayoutArquivo(PgitUtil.concatenarCampos(saidaDTO.getCdTipoLayoutArquivo(), saidaDTO.getDsTipoLayoutArquivo()));
		setCodigoMensagem(saidaDTO.getCdMensagemArquivoRetorno());
		setCdTipoMensagem(saidaDTO.getCdTipoMensagemRetorno());
		setCdNivelMensagem(saidaDTO.getCdNivelMensagemLayout());
		setPosicaoInicialCampo(saidaDTO.getNrPosicaoInicialMensagem() == 0 ? "" : saidaDTO.getNrPosicaoInicialMensagem().toString());
		setPosicaoFinalCampo(saidaDTO.getNrPosicaoFinalMensagem() == 0 ? "" : saidaDTO.getNrPosicaoFinalMensagem().toString());
		setDescricaoMensagemCliente(saidaDTO.getDsMensagemLayoutRetorno());
		setDataHoraInclusao(saidaDTO.getHrInclusaoRegistro());
		setUsuarioInclusao(saidaDTO.getCdAutenticacaoSegurancaInclusao());
		setTipoCanalInclusao(PgitUtil.concatenarCampos(saidaDTO.getCdCanalInclusao(), saidaDTO.getDsCanalInclusao()));
		setComplementoInclusao(saidaDTO.getNmOperacaoFluxoInclusao() == null || saidaDTO.getNmOperacaoFluxoInclusao().equals("0") ? "" : saidaDTO.getNmOperacaoFluxoInclusao());
		setDataHoraManutencao(saidaDTO.getHrManutencaoRegistro());
		setUsuarioManutencao(saidaDTO.getCdAutenticacaoSegurancaManutencao());
		setTipoCanalManutencao(PgitUtil.concatenarCampos(saidaDTO.getCdCanalManutencao(), saidaDTO.getDsCanalManutencao()));
		setComplementoManutencao(saidaDTO.getNmOperacaoFluxoManutencao() == null || saidaDTO.getNmOperacaoFluxoManutencao().equals("0") ? "" : saidaDTO.getNmOperacaoFluxoManutencao());
	}
	
	//Set's e Get's
	/**
	 * Get: manterMensagemLayoutImpl.
	 *
	 * @return manterMensagemLayoutImpl
	 */
	public IManterMensagemLayoutService getManterMensagemLayoutImpl() {
		return manterMensagemLayoutImpl;
	}
	
	/**
	 * Set: manterMensagemLayoutImpl.
	 *
	 * @param manterMensagemLayoutImpl the manter mensagem layout impl
	 */
	public void setManterMensagemLayoutImpl(
			IManterMensagemLayoutService manterMensagemLayoutImpl) {
		this.manterMensagemLayoutImpl = manterMensagemLayoutImpl;
	}

	/**
	 * Is bto acionado.
	 *
	 * @return true, if is bto acionado
	 */
	public boolean isBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ConsultarMsgLayoutArqRetornoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ConsultarMsgLayoutArqRetornoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: comboServiceImpl.
	 *
	 * @return comboServiceImpl
	 */
	public IComboService getComboServiceImpl() {
		return comboServiceImpl;
	}

	/**
	 * Set: comboServiceImpl.
	 *
	 * @param comboServiceImpl the combo service impl
	 */
	public void setComboServiceImpl(IComboService comboServiceImpl) {
		this.comboServiceImpl = comboServiceImpl;
	}

	/**
	 * Get: codigoMensagemFiltro.
	 *
	 * @return codigoMensagemFiltro
	 */
	public String getCodigoMensagemFiltro() {
		return codigoMensagemFiltro;
	}

	/**
	 * Set: codigoMensagemFiltro.
	 *
	 * @param codigoMensagemFiltro the codigo mensagem filtro
	 */
	public void setCodigoMensagemFiltro(String codigoMensagemFiltro) {
		this.codigoMensagemFiltro = codigoMensagemFiltro;
	}

	/**
	 * Get: codigoMensagem.
	 *
	 * @return codigoMensagem
	 */
	public String getCodigoMensagem() {
		return codigoMensagem;
	}

	/**
	 * Set: codigoMensagem.
	 *
	 * @param codigoMensagem the codigo mensagem
	 */
	public void setCodigoMensagem(String codigoMensagem) {
		this.codigoMensagem = codigoMensagem;
	}

	/**
	 * Get: listaTipoLayoutArquivoFiltro.
	 *
	 * @return listaTipoLayoutArquivoFiltro
	 */
	public List<SelectItem> getListaTipoLayoutArquivoFiltro() {
		return listaTipoLayoutArquivoFiltro;
	}

	/**
	 * Set: listaTipoLayoutArquivoFiltro.
	 *
	 * @param listaTipoLayoutArquivoFiltro the lista tipo layout arquivo filtro
	 */
	public void setListaTipoLayoutArquivoFiltro(
			List<SelectItem> listaTipoLayoutArquivoFiltro) {
		this.listaTipoLayoutArquivoFiltro = listaTipoLayoutArquivoFiltro;
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 *
	 * @return listaTipoLayoutArquivo
	 */
	public List<SelectItem> getListaTipoLayoutArquivo() {
		return listaTipoLayoutArquivo;
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 *
	 * @param listaTipoLayoutArquivo the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	/**
	 * Get: listaTipoLayoutArquivoHash.
	 *
	 * @return listaTipoLayoutArquivoHash
	 */
	public Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}

	/**
	 * Set lista tipo layout arquivo hash.
	 *
	 * @param listaTipoLayoutArquivoHash the lista tipo layout arquivo hash
	 */
	public void setListaTipoLayoutArquivoHash(
			Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdTipoLayoutArquivoFiltro.
	 *
	 * @return cdTipoLayoutArquivoFiltro
	 */
	public Integer getCdTipoLayoutArquivoFiltro() {
		return cdTipoLayoutArquivoFiltro;
	}

	/**
	 * Set: cdTipoLayoutArquivoFiltro.
	 *
	 * @param cdTipoLayoutArquivoFiltro the cd tipo layout arquivo filtro
	 */
	public void setCdTipoLayoutArquivoFiltro(Integer cdTipoLayoutArquivoFiltro) {
		this.cdTipoLayoutArquivoFiltro = cdTipoLayoutArquivoFiltro;
	}

	/**
	 * Get: cdTipoMensagem.
	 *
	 * @return cdTipoMensagem
	 */
	public Integer getCdTipoMensagem() {
		return cdTipoMensagem;
	}

	/**
	 * Set: cdTipoMensagem.
	 *
	 * @param cdTipoMensagem the cd tipo mensagem
	 */
	public void setCdTipoMensagem(Integer cdTipoMensagem) {
		this.cdTipoMensagem = cdTipoMensagem;
	}

	/**
	 * Get: cdNivelMensagem.
	 *
	 * @return cdNivelMensagem
	 */
	public Integer getCdNivelMensagem() {
		return cdNivelMensagem;
	}

	/**
	 * Set: cdNivelMensagem.
	 *
	 * @param cdNivelMensagem the cd nivel mensagem
	 */
	public void setCdNivelMensagem(Integer cdNivelMensagem) {
		this.cdNivelMensagem = cdNivelMensagem;
	}

	/**
	 * Get: descricaoMensagemCliente.
	 *
	 * @return descricaoMensagemCliente
	 */
	public String getDescricaoMensagemCliente() {
		return descricaoMensagemCliente;
	}

	/**
	 * Set: descricaoMensagemCliente.
	 *
	 * @param descricaoMensagemCliente the descricao mensagem cliente
	 */
	public void setDescricaoMensagemCliente(String descricaoMensagemCliente) {
		this.descricaoMensagemCliente = descricaoMensagemCliente;
	}

	/**
	 * Get: posicaoFinalCampo.
	 *
	 * @return posicaoFinalCampo
	 */
	public String getPosicaoFinalCampo() {
		return posicaoFinalCampo;
	}

	/**
	 * Set: posicaoFinalCampo.
	 *
	 * @param posicaoFinalCampo the posicao final campo
	 */
	public void setPosicaoFinalCampo(String posicaoFinalCampo) {
		this.posicaoFinalCampo = posicaoFinalCampo;
	}

	/**
	 * Get: posicaoInicialCampo.
	 *
	 * @return posicaoInicialCampo
	 */
	public String getPosicaoInicialCampo() {
		return posicaoInicialCampo;
	}

	/**
	 * Set: posicaoInicialCampo.
	 *
	 * @param posicaoInicialCampo the posicao inicial campo
	 */
	public void setPosicaoInicialCampo(String posicaoInicialCampo) {
		this.posicaoInicialCampo = posicaoInicialCampo;
	}

	/**
	 * Get: dsTipoLayoutArquivo.
	 *
	 * @return dsTipoLayoutArquivo
	 */
	public String getDsTipoLayoutArquivo() {
		return dsTipoLayoutArquivo;
	}

	/**
	 * Set: dsTipoLayoutArquivo.
	 *
	 * @param dsTipoLayoutArquivo the ds tipo layout arquivo
	 */
	public void setDsTipoLayoutArquivo(String dsTipoLayoutArquivo) {
		this.dsTipoLayoutArquivo = dsTipoLayoutArquivo;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Nome: setSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @param saidaTipoLayoutArquivoSaidaDTO
	 */
	public void setSaidaTipoLayoutArquivoSaidaDTO(
			TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
		this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: getSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @return saidaTipoLayoutArquivoSaidaDTO
	 */
	public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
		return saidaTipoLayoutArquivoSaidaDTO;
	}

	

}
