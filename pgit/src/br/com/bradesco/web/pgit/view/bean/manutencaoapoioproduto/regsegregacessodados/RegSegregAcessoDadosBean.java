/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.regsegregacessodados
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.regsegregacessodados;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.ICadTipPendenciaService;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoUnidadeOrganizacionalDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.IRegSegregAcessoDadosService;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.AlterarRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.DetalharRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ExcluirRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.IncluirRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarExcRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarExcRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarRegraSegregacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.regsegregacessodados.bean.ListarRegraSegregacaoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;


/**
 * Nome: RegSegregAcessoDadosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class RegSegregAcessoDadosBean {
	
	/** Atributo regSegregAcessoDadosServiceImpl. */
	private IRegSegregAcessoDadosService regSegregAcessoDadosServiceImpl;
	
	/** Atributo cadTipPendenciaService. */
	private ICadTipPendenciaService cadTipPendenciaService;
	
	// Constantes utilizadas no m�todo addInfoModalMessage do  BradescoFacesUtils para evitar redund�ncia apontada pelo Sonar
	/** Atributo EX_REGRA_SEGREGACAO. */
	private static final String EX_REGRA_SEGREGACAO = "conExRegraSegregacaoAcessoDados";
	
	/** Atributo REGRA_SEGREGACAO. */
	private static final String REGRA_SEGREGACAO = "conRegraSegregacaoAcessoDados";
	
	// Variaveis consultar
	/** Atributo codigoRegraFiltro. */
	private String codigoRegraFiltro;
	
	/** Atributo tipoManutencaoFiltro. */
	private Integer tipoManutencaoFiltro;
	
	/** Atributo tipoUnidadeOrgFiltro. */
	private Integer tipoUnidadeOrgFiltro;
	
	/** Atributo nivelPermissaoFiltro. */
	private Integer nivelPermissaoFiltro;	
	
	/** Atributo listaTipoUnidadeOrganizacional. */
	private List<SelectItem> listaTipoUnidadeOrganizacional =  new ArrayList<SelectItem>();
	
	/** Atributo listaGridRegrasFiltro. */
	private List<ListarRegraSegregacaoSaidaDTO> listaGridRegrasFiltro;
	
	/** Atributo itemSelecionadoListaRegrasFiltro. */
	private Integer itemSelecionadoListaRegrasFiltro;
	
	/** Atributo listaControleRadioRegrasFiltro. */
	private List<SelectItem> listaControleRadioRegrasFiltro;
	
	/** Atributo listaTipoUnidadeOrganizacionalHash. */
	private Map<Integer, String> listaTipoUnidadeOrganizacionalHash = new HashMap<Integer, String>();
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo rdoSelArgPesq. */
	private String rdoSelArgPesq;
	//Fim Variaveis Consultar
	
	//Variaveis Incluir e Incluir2
	/** Atributo cdRegraAcessoDado. */
	private Integer cdRegraAcessoDado;
	
	/** Atributo tipoUnidadeOrg. */
	private Integer tipoUnidadeOrg;
	
	/** Atributo tipoUnidadeOrganizacionalUsuarioDesc. */
	private String tipoUnidadeOrganizacionalUsuarioDesc;
	
	/** Atributo tipoUnidadeOrgProprietario. */
	private Integer tipoUnidadeOrgProprietario;
	
	/** Atributo tipoUnidadeOrganizacionalProprietarioDadosDesc. */
	private String tipoUnidadeOrganizacionalProprietarioDadosDesc;
	
	/** Atributo tipoManutencao. */
	private Integer tipoManutencao;	
	
	/** Atributo nivelPermissao. */
	private Integer nivelPermissao;	
	
	/** Atributo perfilGerenciadorSegDesc. */
	private Integer perfilGerenciadorSegDesc;
	
	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;
	
	/** Atributo perfilGerenciadorSegurancaDesc. */
	private String perfilGerenciadorSegurancaDesc;
	// FIm	Variaveis Incluir e Incluir2
	
	//Variaveis Trilha Auditoria
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	
	
	//Fim variaveis trilha auditoria
	
	//Variaveis Excec��o
	/** Atributo codigoRegraEx. */
	private String codigoRegraEx;		
	
	/** Atributo unidadeOrgUsuarioExDesc. */
	private String unidadeOrgUsuarioExDesc;
	
	/** Atributo unidadeOrgProprietarioUsuarioExDesc. */
	private String unidadeOrgProprietarioUsuarioExDesc;
	
	/** Atributo tipoExcecao. */
	private String tipoExcecao;
	
	/** Atributo tipoAbrangencia. */
	private String tipoAbrangencia;
	
	/** Atributo tipoExececaoAcessoFiltro. */
	private Integer tipoExececaoAcessoFiltro;
	
	/** Atributo tipoUnidadeOrgFiltroEx. */
	private Integer tipoUnidadeOrgFiltroEx;
	
	/** Atributo tipoAbrangenciaAcessoFiltro. */
	private Integer tipoAbrangenciaAcessoFiltro;
	
	/** Atributo listaGridExcecoesFiltro. */
	private List<ListarExcRegraSegregacaoSaidaDTO> listaGridExcecoesFiltro;
	
	/** Atributo itemSelecionadoListaExcecaoFiltro. */
	private Integer itemSelecionadoListaExcecaoFiltro;
	
	/** Atributo listaControleRadioExcecoesFiltro. */
	private List<SelectItem> listaControleRadioExcecoesFiltro;
		
	//	Variaveis Excec��o Incluir;	
	/** Atributo codigoEx. */
	private String codigoEx;
	
	/** Atributo tipoUnidadeOrgUsuarioEx. */
	private Integer tipoUnidadeOrgUsuarioEx;
	
	/** Atributo unidadeOrgUsuarioEx. */
	private String unidadeOrgUsuarioEx;
	
	/** Atributo tipoUnidadeOrgProprietarioUsuarioEx. */
	private Integer tipoUnidadeOrgProprietarioUsuarioEx;
	
	/** Atributo unidadeOrgProprietarioUsuarioEx. */
	private String unidadeOrgProprietarioUsuarioEx;
	
	/** Atributo tipoExcecaoRadio. */
	private Integer tipoExcecaoRadio;
	
	/** Atributo tipoAbrangenciaExcecaoRadio. */
	private Integer tipoAbrangenciaExcecaoRadio;
	
	/** Atributo tipoUnidadeOrgUsuarioExDesc. */
	private String tipoUnidadeOrgUsuarioExDesc;
	
	/** Atributo tipoUnidadeOrgProprietarioUsuarioDescEx. */
	private String tipoUnidadeOrgProprietarioUsuarioDescEx;
	
	/** Atributo empresaConglomeradoUsuario. */
	private Long empresaConglomeradoUsuario;
	
	/** Atributo empresaConglomeradoProprietario. */
	private Long empresaConglomeradoProprietario;
	
	/** Atributo empresaConglomeradoUsuarioDesc. */
	private String empresaConglomeradoUsuarioDesc;
	
	/** Atributo empresaConglomeradoProprietarioDesc. */
	private String empresaConglomeradoProprietarioDesc;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo btoAcionadoExcecao. */
	private Boolean btoAcionadoExcecao;
		
	
	/** Atributo listaEmpresaConglomerado. */
	private List<SelectItem> listaEmpresaConglomerado = new ArrayList<SelectItem>();
	
	/** Atributo listaEmpresaConglomeradoProprietario. */
	private List<SelectItem> listaEmpresaConglomeradoProprietario = new ArrayList<SelectItem>();
	
	/** Atributo listaEmpresaConglomeradoHash. */
	private Map<Long, String> listaEmpresaConglomeradoHash = new HashMap<Long, String>();
	
	/** Atributo listaEmpresaConglomeradoProprietarioHash. */
	private Map<Long, String> listaEmpresaConglomeradoProprietarioHash = new HashMap<Long, String>();
	//	Fim Variaveis Excec��o	

	

	//	METODOS EXECE��O -----------------------------------------------------
	/**
	 * Preenche empresa conglomerado.
	 */
	@SuppressWarnings("unchecked")
	public void preencheEmpresaConglomerado(){
				
		if (getTipoUnidadeOrgUsuarioEx()!=null && getTipoUnidadeOrgUsuarioEx()!=0){
		
			listaEmpresaConglomerado = new ArrayList<SelectItem>();
			List<EmpresaConglomeradoSaidaDTO> listaConglomerado;
			
			EmpresaConglomeradoEntradaDTO tipoUnidadeOrganizacionalDTO = new EmpresaConglomeradoEntradaDTO();
			tipoUnidadeOrganizacionalDTO.setNrOcorrencias(50);
			tipoUnidadeOrganizacionalDTO.setCdSituacao(0);
		
			listaConglomerado = comboService.listarEmpresaConglomerado(tipoUnidadeOrganizacionalDTO);
			
			listaEmpresaConglomeradoHash.clear();
			for (int i = 0; i < listaConglomerado.size(); i++) {
				listaEmpresaConglomeradoHash.put(listaConglomerado.get(i).getCodClub(), listaConglomerado.get(i).getDsRazaoSocial());
				this.listaEmpresaConglomerado.add(new SelectItem(String.valueOf(listaConglomerado.get(i).getCodClub()),PgitUtil.concatenarCampos(listaConglomerado.get(i).getCodClub(), listaConglomerado.get(i).getDsRazaoSocial())));
			}
		}else{
			listaEmpresaConglomerado.clear();
			setEmpresaConglomeradoUsuario(0l);
			
		}
	}
	
	/**
	 * Limpar tela principal.
	 */
	public void limparTelaPrincipal(){
		
		setCodigoRegraFiltro("");
		setTipoManutencaoFiltro(0);
		setTipoUnidadeOrgFiltro(0);
		setNivelPermissaoFiltro(0);
		setListaGridRegrasFiltro(null);
		setItemSelecionadoListaRegrasFiltro(0);
	}

	
	/**
	 * Preenche empresa conglomerado proprietario.
	 */
	@SuppressWarnings("unchecked")
	public void preencheEmpresaConglomeradoProprietario(){
		
		if (getTipoUnidadeOrgProprietarioUsuarioEx()!=null && getTipoUnidadeOrgProprietarioUsuarioEx()!=0){
		
			listaEmpresaConglomeradoProprietario = new ArrayList<SelectItem>();
			List<EmpresaConglomeradoSaidaDTO> listaConglomerado;
			
			EmpresaConglomeradoEntradaDTO tipoUnidadeOrganizacionalDTO = new EmpresaConglomeradoEntradaDTO();
			tipoUnidadeOrganizacionalDTO.setNrOcorrencias(50);
			tipoUnidadeOrganizacionalDTO.setCdSituacao(0);
		
			listaConglomerado = comboService.listarEmpresaConglomerado(tipoUnidadeOrganizacionalDTO);
			
			listaEmpresaConglomeradoProprietarioHash.clear();
			for (int i = 0; i < listaConglomerado.size(); i++) {
				listaEmpresaConglomeradoProprietarioHash.put(listaConglomerado.get(i).getCodClub(), listaConglomerado.get(i).getDsRazaoSocial());
				this.listaEmpresaConglomeradoProprietario.add(new SelectItem(String.valueOf(listaConglomerado.get(i).getCodClub()), PgitUtil.concatenarCampos(listaConglomerado.get(i).getCodClub(), listaConglomerado.get(i).getDsRazaoSocial())));
			}
		}else{
			listaEmpresaConglomeradoProprietario.clear();
			setEmpresaConglomeradoProprietario(0l);
			
		}
	}
	
	/**
	 * Limpar variaveis incluir.
	 */
	public void limparVariaveisIncluir(){
		
		this.setUnidadeOrgUsuarioEx("");
		this.setUnidadeOrgProprietarioUsuarioEx("");
		setUnidadeOrgProprietarioUsuarioEx("");		
		this.setTipoExcecaoRadio(null);
		this.setTipoAbrangenciaExcecaoRadio(null);
		this.setEmpresaConglomeradoUsuario(0l);
		this.setEmpresaConglomeradoProprietario(0l);
	}
	
	
	/**
	 * Limpar dados excecao.
	 */
	public void limparDadosExcecao(){
		this.setTipoExececaoAcessoFiltro(0);
		this.setTipoUnidadeOrgFiltroEx(0);
		this.setTipoAbrangenciaAcessoFiltro(0);
		setListaGridExcecoesFiltro(null);
		setItemSelecionadoListaExcecaoFiltro(null);
		setBtoAcionadoExcecao(false);
	}
	
		
	
	/**
	 * Carrega lista excecao.
	 */
	public void carregaListaExcecao(){
		
		
		try{
			
			listaGridExcecoesFiltro = new ArrayList<ListarExcRegraSegregacaoSaidaDTO>();
			
			ListarExcRegraSegregacaoEntradaDTO listarExcSegregacaoAcessoEntradaDTO = new ListarExcRegraSegregacaoEntradaDTO();
						
			listarExcSegregacaoAcessoEntradaDTO.setCdRegraAcessoDado(getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro()).getCodigoRegra());
			listarExcSegregacaoAcessoEntradaDTO.setCdExcecaoAcessoDado(0);
			listarExcSegregacaoAcessoEntradaDTO.setTipoUnidadeOrganizacionalUsuario(getTipoUnidadeOrgFiltroEx() != null ? getTipoUnidadeOrgFiltroEx():0);
			listarExcSegregacaoAcessoEntradaDTO.setEmpresa(0);
			listarExcSegregacaoAcessoEntradaDTO.setUnidadeOrganizacional(0);
			listarExcSegregacaoAcessoEntradaDTO.setTipoExcecao(getTipoExececaoAcessoFiltro() != null ? getTipoExececaoAcessoFiltro() :0);
			listarExcSegregacaoAcessoEntradaDTO.setTipoAbrangenciaAcesso(getTipoAbrangenciaAcessoFiltro() != null ? getTipoAbrangenciaAcessoFiltro() :0);
					
			setListaGridExcecoesFiltro(getRegSegregAcessoDadosServiceImpl().listarExcRegraSegregacao(listarExcSegregacaoAcessoEntradaDTO));
			setBtoAcionadoExcecao(true);
			listaControleRadioExcecoesFiltro = new ArrayList<SelectItem>();
			
			for(int i = 0; i < listaGridExcecoesFiltro.size();i++){
				listaControleRadioExcecoesFiltro.add(new SelectItem(i,""));
			}
			
			setItemSelecionadoListaExcecaoFiltro(null);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridExcecoesFiltro(null);
			setItemSelecionadoListaExcecaoFiltro(null);

		}
		
			
	}	
	
	
	/**
	 * Voltar exececao.
	 *
	 * @return the string
	 */
	public String voltarExececao(){
		setItemSelecionadoListaRegrasFiltro(null);
		
		return "VOLTAR_EXCECAO";
	}
	
	/**
	 * Detalhar excecao.
	 *
	 * @return the string
	 */
	public String detalharExcecao(){
		try {
			preencheDadosExcecao();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), EX_REGRA_SEGREGACAO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		
		return "DETALHAR_EXCECAO";
	}	
	
	/**
	 * Incluir excecao.
	 *
	 * @return the string
	 */
	public String incluirExcecao(){		
			limparVariaveisIncluir();
			ListarRegraSegregacaoSaidaDTO listarRegraSegregacaoSaidaDTO = getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro());
			setTipoUnidadeOrgUsuarioEx(listarRegraSegregacaoSaidaDTO.getCdTipoUnidadeOrganizacionalUsuario());
			setTipoUnidadeOrgProprietarioUsuarioEx(listarRegraSegregacaoSaidaDTO.getCdTipoUnidadeOrganizacionalProprietario());
			preencheEmpresaConglomerado();
			preencheEmpresaConglomeradoProprietario();
			return "INCLUIR_EXCECAO";
	}		
	
	/**
	 * Alterar excecao.
	 *
	 * @return the string
	 */
	public String alterarExcecao(){
		try {
			preencheDadosExcecao();
			
			preencheListaTipoUnidadeOrganizacionalFiltro();
			
			if (getTipoUnidadeOrgUsuarioEx() != null && getTipoUnidadeOrgUsuarioEx() !=0){
				preencheEmpresaConglomerado();
			}
			
			if (getTipoUnidadeOrgProprietarioUsuarioEx() != null && getTipoUnidadeOrgProprietarioUsuarioEx() !=0){
				preencheEmpresaConglomeradoProprietario();
			}
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), EX_REGRA_SEGREGACAO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "ALTERAR_EXCECAO";
	}
	
	/**
	 * Excluir excecao.
	 *
	 * @return the string
	 */
	public String excluirExcecao(){
		try {
			preencheDadosExcecao();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), EX_REGRA_SEGREGACAO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "EXCLUIR_EXCECAO";
	}
	
	/**
	 * Voltar exececao detalhar.
	 *
	 * @return the string
	 */
	public String voltarExececaoDetalhar(){	
		setItemSelecionadoListaExcecaoFiltro(null);
		
		return "VOLTAR_PESQUISA_EXCECAO";
	}
	
	/**
	 * Voltar incluir excecao.
	 *
	 * @return the string
	 */
	public String voltarIncluirExcecao(){		
		return "VOLTAR_INCLUIR_EXCECAO";
	}
	
	/**
	 * Avancar incluir excecao.
	 *
	 * @return the string
	 */
	public String avancarIncluirExcecao(){
		
		

		if(this.getUnidadeOrgUsuarioEx() != null && !this.getUnidadeOrgUsuarioEx().equals("")){
			ConsultarDescricaoUnidOrganizacionalSaidaDTO descUnidOrgSaidaDTO = new ConsultarDescricaoUnidOrganizacionalSaidaDTO();
			ConsultarDescricaoUnidOrganizacionalEntradaDTO descUnidOrgEntradaDTO =  new ConsultarDescricaoUnidOrganizacionalEntradaDTO();
			descUnidOrgEntradaDTO.setCdPessoaJuridica(Long.valueOf(this.getEmpresaConglomeradoUsuario()));
			descUnidOrgEntradaDTO.setCdTipoUnidadeOrganizacional(this.getTipoUnidadeOrgUsuarioEx());
			descUnidOrgEntradaDTO.setNrSequenciaUnidadeOrganizacional(Integer.parseInt(this.getUnidadeOrgUsuarioEx()));
			descUnidOrgSaidaDTO = getCadTipPendenciaService().descricaoUnidadeOrganizacional(descUnidOrgEntradaDTO);
			setUnidadeOrgUsuarioExDesc(PgitUtil.concatenarCampos(getUnidadeOrgUsuarioEx(), descUnidOrgSaidaDTO.getDsNumeroSeqUnidadeOrganizacional()));
		}else{
			setUnidadeOrgUsuarioEx("");
			setUnidadeOrgUsuarioExDesc("");
		}
		
		if(this.getUnidadeOrgProprietarioUsuarioEx() != null && !this.getUnidadeOrgProprietarioUsuarioEx().equals("")){
			ConsultarDescricaoUnidOrganizacionalSaidaDTO descUnidOrgSaidaDTO = new ConsultarDescricaoUnidOrganizacionalSaidaDTO();
			ConsultarDescricaoUnidOrganizacionalEntradaDTO descUnidOrgEntradaDTO =  new ConsultarDescricaoUnidOrganizacionalEntradaDTO();
			descUnidOrgEntradaDTO.setCdPessoaJuridica(Long.valueOf(this.getEmpresaConglomeradoProprietario()));
			descUnidOrgEntradaDTO.setCdTipoUnidadeOrganizacional(this.getTipoUnidadeOrgProprietarioUsuarioEx());
			descUnidOrgEntradaDTO.setNrSequenciaUnidadeOrganizacional(Integer.parseInt(this.getUnidadeOrgProprietarioUsuarioEx()));
			descUnidOrgSaidaDTO = getCadTipPendenciaService().descricaoUnidadeOrganizacional(descUnidOrgEntradaDTO);
			setUnidadeOrgProprietarioUsuarioExDesc(PgitUtil.concatenarCampos(getUnidadeOrgProprietarioUsuarioEx(), descUnidOrgSaidaDTO.getDsNumeroSeqUnidadeOrganizacional()));
		}
		
		if(getObrigatoriedade()!= null && getObrigatoriedade().equals("V")){
			//setTipoUnidadeOrgUsuarioExDesc(getTipoUnidadeOrgUsuarioEx() != null && getTipoUnidadeOrgUsuarioEx() != 0? (String)listaTipoUnidadeOrganizacionalHash.get(getTipoUnidadeOrgUsuarioEx()):"");
			setEmpresaConglomeradoUsuarioDesc(getEmpresaConglomeradoUsuario() != null && getEmpresaConglomeradoUsuario() != 0l? (String)listaEmpresaConglomeradoHash.get(getEmpresaConglomeradoUsuario()):"");
			//setTipoUnidadeOrgProprietarioUsuarioDescEx(getTipoUnidadeOrgProprietarioUsuarioEx() != null && getTipoUnidadeOrgProprietarioUsuarioEx() != 0? (String)listaTipoUnidadeOrganizacionalHash.get(getTipoUnidadeOrgProprietarioUsuarioEx()):"");
			setEmpresaConglomeradoProprietarioDesc(getEmpresaConglomeradoProprietario() != null && getEmpresaConglomeradoProprietario() != 0l? (String)listaEmpresaConglomeradoProprietarioHash.get(getEmpresaConglomeradoProprietario()):"");
			return "AVANCAR_INCLUIR_EXCECAO";
			
		}else{
			return"";
		}
	} 
	
	/**
	 * Confirmar incluir excecao.
	 *
	 * @return the string
	 */
	public String confirmarIncluirExcecao(){
		
		try {
			
			IncluirExcRegraSegregacaoEntradaDTO incluirExcRegraSegregacaoEntradaDTO = new IncluirExcRegraSegregacaoEntradaDTO();
			
			incluirExcRegraSegregacaoEntradaDTO.setCodigoRegra(Integer.parseInt(getCodigoRegraEx()));			
			incluirExcRegraSegregacaoEntradaDTO.setUnidadeOrganizacionalUsuario(getUnidadeOrgUsuarioEx() != null && !getUnidadeOrgUsuarioEx().equals("")?Integer.parseInt(getUnidadeOrgUsuarioEx()):0);
			incluirExcRegraSegregacaoEntradaDTO.setUnidadeOrganizacionalProprietario(Integer.parseInt(getUnidadeOrgProprietarioUsuarioEx()));
			incluirExcRegraSegregacaoEntradaDTO.setEmpresaConglomeradoUsuario(getEmpresaConglomeradoUsuario() != null  && getEmpresaConglomeradoUsuario() !=0l?getEmpresaConglomeradoUsuario():0);
			incluirExcRegraSegregacaoEntradaDTO.setEmpresaConglomeradoProprietario(getEmpresaConglomeradoProprietario());
			incluirExcRegraSegregacaoEntradaDTO.setTipoUnidadeOrganizacionalUsuario(getTipoUnidadeOrgUsuarioEx());
			incluirExcRegraSegregacaoEntradaDTO.setTipoUnidadeOrganizacionalProprietario(getTipoUnidadeOrgProprietarioUsuarioEx()!=null && getTipoUnidadeOrgProprietarioUsuarioEx()!=0?getTipoUnidadeOrgProprietarioUsuarioEx():0);
			incluirExcRegraSegregacaoEntradaDTO.setTipoExcecao(getTipoExcecaoRadio());
			incluirExcRegraSegregacaoEntradaDTO.setTipoAbrangenciaExcecao(getTipoAbrangenciaExcecaoRadio());
			
			IncluirExcRegraSegregacaoSaidaDTO incluirExcRegraSegregacaoSaidaDTO = getRegSegregAcessoDadosServiceImpl().incluirExcRegraSegregacao(incluirExcRegraSegregacaoEntradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +incluirExcRegraSegregacaoSaidaDTO.getCodMensagem() + ") " + incluirExcRegraSegregacaoSaidaDTO.getMensagem(), EX_REGRA_SEGREGACAO, BradescoViewExceptionActionType.ACTION, false);			
			carregaListaExcecao();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
		
	}
	
	/**
	 * Voltar pesquisa excecao.
	 *
	 * @return the string
	 */
	public String voltarPesquisaExcecao(){
		setItemSelecionadoListaExcecaoFiltro(null);
		
		return "VOLTAR_PESQUISA_EXCECAO";
	}
	
	/**
	 * Avancar alterar excecao.
	 *
	 * @return the string
	 */
	public String avancarAlterarExcecao(){
		if(this.getUnidadeOrgUsuarioEx() != null && !this.getUnidadeOrgUsuarioEx().equals("")){
			ConsultarDescricaoUnidOrganizacionalSaidaDTO descUnidOrgSaidaDTO = new ConsultarDescricaoUnidOrganizacionalSaidaDTO();
			ConsultarDescricaoUnidOrganizacionalEntradaDTO descUnidOrgEntradaDTO =  new ConsultarDescricaoUnidOrganizacionalEntradaDTO();
			descUnidOrgEntradaDTO.setCdPessoaJuridica(Long.valueOf(this.getEmpresaConglomeradoUsuario()));
			descUnidOrgEntradaDTO.setCdTipoUnidadeOrganizacional(this.getTipoUnidadeOrgUsuarioEx());
			descUnidOrgEntradaDTO.setNrSequenciaUnidadeOrganizacional(Integer.parseInt(this.getUnidadeOrgUsuarioEx()));
			descUnidOrgSaidaDTO = getCadTipPendenciaService().descricaoUnidadeOrganizacional(descUnidOrgEntradaDTO);
			setUnidadeOrgUsuarioExDesc(PgitUtil.concatenarCampos(getUnidadeOrgUsuarioEx(), descUnidOrgSaidaDTO.getDsNumeroSeqUnidadeOrganizacional()));
		}else{
			setUnidadeOrgUsuarioExDesc("");
		}
		
		if(this.getUnidadeOrgProprietarioUsuarioEx() != null && !this.getUnidadeOrgProprietarioUsuarioEx().equals("")){
			ConsultarDescricaoUnidOrganizacionalSaidaDTO descUnidOrgSaidaDTO = new ConsultarDescricaoUnidOrganizacionalSaidaDTO();
			ConsultarDescricaoUnidOrganizacionalEntradaDTO descUnidOrgEntradaDTO =  new ConsultarDescricaoUnidOrganizacionalEntradaDTO();
			descUnidOrgEntradaDTO.setCdPessoaJuridica(Long.valueOf(this.getEmpresaConglomeradoProprietario()));
			descUnidOrgEntradaDTO.setCdTipoUnidadeOrganizacional(this.getTipoUnidadeOrgProprietarioUsuarioEx());
			descUnidOrgEntradaDTO.setNrSequenciaUnidadeOrganizacional(Integer.parseInt(this.getUnidadeOrgProprietarioUsuarioEx()));
			descUnidOrgSaidaDTO = getCadTipPendenciaService().descricaoUnidadeOrganizacional(descUnidOrgEntradaDTO);
			setUnidadeOrgProprietarioUsuarioExDesc(PgitUtil.concatenarCampos(getUnidadeOrgProprietarioUsuarioEx(), descUnidOrgSaidaDTO.getDsNumeroSeqUnidadeOrganizacional()));
		}
		
		if(getObrigatoriedade()!=null && getObrigatoriedade().equals("V")){			
			setEmpresaConglomeradoUsuarioDesc(getEmpresaConglomeradoUsuario() != null && getEmpresaConglomeradoUsuario() != 0l? (String)listaEmpresaConglomeradoHash.get(getEmpresaConglomeradoUsuario()):"");
			setEmpresaConglomeradoProprietarioDesc(getEmpresaConglomeradoProprietario() != null && getEmpresaConglomeradoProprietario() != 0l? (String)listaEmpresaConglomeradoProprietarioHash.get(getEmpresaConglomeradoProprietario()):"");
			return"AVANCAR_ALTERAR_EXCECAO";
		}else{
			return"";
		}
	}
	
	/**
	 * Voltar alterar excecao.
	 *
	 * @return the string
	 */
	public String voltarAlterarExcecao(){
		return"VOLTAR_ALTERAR_EXCECAO";
	}
	
	/**
	 * Confirmar alterar excecao.
	 *
	 * @return the string
	 */
	public String confirmarAlterarExcecao(){
		
		try {
			
			AlterarExcRegraSegregacaoEntradaDTO alterarExcRegraSegregacaoEntradaDTO = new AlterarExcRegraSegregacaoEntradaDTO();
			
			alterarExcRegraSegregacaoEntradaDTO.setCodigoRegra(Integer.parseInt(getCodigoRegraEx()));
			alterarExcRegraSegregacaoEntradaDTO.setCodigoExcecao(Integer.parseInt(getCodigoEx()));
			alterarExcRegraSegregacaoEntradaDTO.setTipoUnidadeOrganizacionalUsuario(getTipoUnidadeOrgUsuarioEx());
			alterarExcRegraSegregacaoEntradaDTO.setEmpresaConglomeradoUsuario(getEmpresaConglomeradoUsuario());
			alterarExcRegraSegregacaoEntradaDTO.setUnidadeOrganizacionalUsuario(Integer.parseInt(getUnidadeOrgProprietarioUsuarioEx()));
			alterarExcRegraSegregacaoEntradaDTO.setTipoUnidadeOrganizacionalProprietario(getTipoUnidadeOrgProprietarioUsuarioEx());
			alterarExcRegraSegregacaoEntradaDTO.setEmpresaConglomeradoProprietario(getEmpresaConglomeradoProprietario());
			alterarExcRegraSegregacaoEntradaDTO.setUnidadeOrganizacionalProprietario(Integer.parseInt(getUnidadeOrgProprietarioUsuarioEx()));
			alterarExcRegraSegregacaoEntradaDTO.setTipoExcecao(getTipoExcecaoRadio());
			alterarExcRegraSegregacaoEntradaDTO.setTipoAbrangenciaExcecao(getTipoAbrangenciaExcecaoRadio());
			
			AlterarExcRegraSegregacaoSaidaDTO alterarExcRegraSegregacaoSaidaDTO = getRegSegregAcessoDadosServiceImpl().alterarExcRegraSegregacao(alterarExcRegraSegregacaoEntradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +alterarExcRegraSegregacaoSaidaDTO.getCodMensagem() + ") " + alterarExcRegraSegregacaoSaidaDTO.getMensagem(), EX_REGRA_SEGREGACAO, BradescoViewExceptionActionType.ACTION, false);			
			carregaListaExcecao();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}	
	
	/**
	 * Confirmar excluir excecao.
	 *
	 * @return the string
	 */
	public String confirmarExcluirExcecao(){
		try {
			
			ExcluirExcRegraSegregacaoEntradaDTO excluirExcRegraSegregacaoEntradaDTO = new ExcluirExcRegraSegregacaoEntradaDTO();
			
			excluirExcRegraSegregacaoEntradaDTO.setCodigoRegra(Integer.parseInt(getCodigoRegraEx()));
			excluirExcRegraSegregacaoEntradaDTO.setCodigoExcecao(Integer.parseInt(getCodigoEx()));
			
			ExcluirExcRegraSegregacaoSaidaDTO excluirExcRegraSegregacaoSaidaDTO = getRegSegregAcessoDadosServiceImpl().excluirExcRegraSegregacao(excluirExcRegraSegregacaoEntradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +excluirExcRegraSegregacaoSaidaDTO.getCodMensagem() + ") " + excluirExcRegraSegregacaoSaidaDTO.getMensagem(), EX_REGRA_SEGREGACAO, BradescoViewExceptionActionType.ACTION, false);
			
			carregaListaExcecao();
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	// FIM METODOS EXCE��O --------------------------------------------------------
		
	// METODOS PRINCIPAL ----------------------------------------------------------
	
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt){
		this.setCodigoRegraFiltro("");
		this.setTipoManutencaoFiltro(null);
		this.setTipoUnidadeOrgFiltro(null);
		this.setNivelPermissaoFiltro(null);
		preencheListaTipoUnidadeOrganizacionalFiltro();
		this.setListaGridRegrasFiltro(null);
		this.setItemSelecionadoListaRegrasFiltro(null);
		this.setListaControleRadioRegrasFiltro(null);		
		this.setTipoManutencao(null);	
		this.setTipoUnidadeOrg(null);
		this.setTipoUnidadeOrgProprietario(null);
		this.setPerfilGerenciadorSegDesc(null);
		this.setNivelPermissao(null);
		this.setObrigatoriedade("");
		this.setTipoUnidadeOrganizacionalUsuarioDesc("");
		this.setTipoUnidadeOrganizacionalProprietarioDadosDesc("");
		this.setPerfilGerenciadorSegurancaDesc("");
		setBtoAcionado(false);
		
		limparPaginaExcecao();
	}
	
	/**
	 * Limpar pagina excecao.
	 */
	public void limparPaginaExcecao(){		
		this.setTipoUnidadeOrgUsuarioExDesc("");
		this.setUnidadeOrgUsuarioEx("");
		this.setTipoUnidadeOrgProprietarioUsuarioEx(null);
		this.setUnidadeOrgProprietarioUsuarioEx("");
		this.setTipoExcecao("");
		this.setTipoAbrangencia("");
		this.setTipoExececaoAcessoFiltro(null);
		this.setTipoUnidadeOrgFiltroEx(null);
		this.setTipoAbrangenciaAcessoFiltro(null);
		this.setListaGridExcecoesFiltro(null);
		this.setItemSelecionadoListaExcecaoFiltro(null);
		this.setListaControleRadioExcecoesFiltro(null);		
		this.setTipoUnidadeOrgUsuarioExDesc(null);
		this.setUnidadeOrgUsuarioEx("");		
		this.setUnidadeOrgProprietarioUsuarioExDesc("");
		this.setTipoExcecaoRadio(null);
		this.setTipoAbrangenciaExcecaoRadio(null);
		this.setTipoUnidadeOrgUsuarioExDesc("");
		this.setTipoUnidadeOrgProprietarioUsuarioDescEx("");
		this.setEmpresaConglomeradoUsuario(null);
		this.setEmpresaConglomeradoProprietario(null);
	}
	
	
	/**
	 * Limpar campos.
	 */
	public void limparCampos(){
		this.setCodigoRegraFiltro("");
		this.setTipoManutencaoFiltro(0);
		this.setTipoUnidadeOrgFiltro(0);
		this.setNivelPermissaoFiltro(0);
		this.setListaTipoUnidadeOrganizacional(null);
		preencheListaTipoUnidadeOrganizacionalFiltro();
		setListaGridRegrasFiltro(null);
		setItemSelecionadoListaRegrasFiltro(null);
		setBtoAcionado(false);
		setRdoSelArgPesq("");
	}
	
	/**
	 * Consultar.
	 */
	public void consultar(){
		try{
			
			listaGridRegrasFiltro = new ArrayList<ListarRegraSegregacaoSaidaDTO>();
			
			ListarRegraSegregacaoEntradaDTO entradaDTO = new ListarRegraSegregacaoEntradaDTO();
			
			entradaDTO.setCodigoRegra(getCodigoRegraFiltro()!= null && !getCodigoRegraFiltro().equals("") ? Integer.parseInt(getCodigoRegraFiltro()) :0);
			entradaDTO.setTipoUnidadeOrganizacionalUsuario(getTipoUnidadeOrgFiltro()!=null? getTipoUnidadeOrgFiltro() :0);
			entradaDTO.setTipoAcao(getTipoManutencaoFiltro()!=null? getTipoManutencaoFiltro():0);
			entradaDTO.setNivelPermissaoAcessoDados(getNivelPermissaoFiltro()!=null? getNivelPermissaoFiltro():0);
				
			setListaGridRegrasFiltro(getRegSegregAcessoDadosServiceImpl().listaRegraSegregacao(entradaDTO));
			setBtoAcionado(true);
			listaControleRadioRegrasFiltro = new ArrayList<SelectItem>();
			for(int i = 0; i < listaGridRegrasFiltro.size();i++ ){
				listaControleRadioRegrasFiltro.add(new SelectItem(i," "));
			}
			
			setItemSelecionadoListaRegrasFiltro(null);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridRegrasFiltro(null);

		}
	}	
	
	/**
	 * Limpar grid.
	 */
	public void limparGrid(){
		this.setListaGridRegrasFiltro(null);
		this.setItemSelecionadoListaRegrasFiltro(null);
		this.setListaControleRadioRegrasFiltro(null);
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt){
		return;
	}
	
	/**
	 * Limpar variaveis.
	 */
	public void limparVariaveis(){
		this.setTipoUnidadeOrg(0);
		this.setTipoUnidadeOrgProprietario(0);
		this.setPerfilGerenciadorSegDesc(0);
		this.setNivelPermissao(0);
		this.setTipoManutencao(0);
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){			
		return "VOLTAR";
	}
	
	/**
	 * Voltar detalhar.
	 *
	 * @return the string
	 */
	public String voltarDetalhar(){
		setItemSelecionadoListaRegrasFiltro(null);

		return"VOLTAR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		setItemSelecionadoListaRegrasFiltro(null);
		
		return "VOLTAR_INCLUIR";
	}
	
	/**
	 * Voltar alterar.
	 *
	 * @return the string
	 */
	public String voltarAlterar(){
		setItemSelecionadoListaRegrasFiltro(null);
		
		return "VOLTAR_ALTERAR";
	}
	
	/**
	 * Voltar excluir.
	 *
	 * @return the string
	 */
	public String voltarExcluir(){
		setItemSelecionadoListaRegrasFiltro(null);
		
		return"VOLTAR";
	}
	
	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar(){
		if(this.getObrigatoriedade().equals("V"))	{
			setTipoUnidadeOrganizacionalUsuarioDesc(getTipoUnidadeOrg() + "-" + (String)listaTipoUnidadeOrganizacionalHash.get(getTipoUnidadeOrg()));
 			setTipoUnidadeOrganizacionalProprietarioDadosDesc(getTipoUnidadeOrgProprietario() + "-" + (String)listaTipoUnidadeOrganizacionalHash.get(getTipoUnidadeOrgProprietario()));
			return "AVANCAR_ALTERAR";
		}
		else{
			return"";
		}
	}
 	
	 /**
	  * Avancar incluir.
	  *
	  * @return the string
	  */
	 public String avancarIncluir(){
 		if(this.getObrigatoriedade().equals("V")){
 			setTipoUnidadeOrganizacionalUsuarioDesc(getTipoUnidadeOrg() + "-" + (String)listaTipoUnidadeOrganizacionalHash.get(getTipoUnidadeOrg()));
 			setTipoUnidadeOrganizacionalProprietarioDadosDesc(getTipoUnidadeOrgProprietario() + "-" + (String)listaTipoUnidadeOrganizacionalHash.get(getTipoUnidadeOrgProprietario()));
 			return "AVANCAR_INCLUIR";
 		}
			
		else{
			return"";
		}
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), REGRA_SEGREGACAO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "DETALHAR"; 
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		limparVariaveis();
		preencheListaTipoUnidadeOrganizacionalFiltro();
		return "INCLUIR"; 
	}
	
	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar(){
		limparVariaveis();
		preencheListaTipoUnidadeOrganizacionalFiltro();
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), REGRA_SEGREGACAO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "ALTERAR"; 
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), REGRA_SEGREGACAO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "EXCLUIR"; 
	}
	
	/**
	 * Excecao.
	 *
	 * @return the string
	 */
	public String excecao(){
		limparPaginaExcecao();
		preencheListaTipoUnidadeOrganizacionalFiltro();
		setCodigoRegraEx(String.valueOf(getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro()).getCodigoRegra()));
		setTipoUnidadeOrgUsuarioExDesc(getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro()).getCdTipoUnidadeOrganizacionalUsuario() + "-" + getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro()).getDsTipoUnidadeOrganizacionalUsuario());
		setTipoUnidadeOrgProprietarioUsuarioDescEx(getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro()).getCdTipoUnidadeOrganizacionalProprietario() + "-" + getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro()).getDsTipoUnidadeOrganizacionalProprietario());
		setTipoManutencao(getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro()).getTipoAcao());
		setNivelPermissao(getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro()).getNivelPermissao());
		setTipoManutencao(getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro()).getTipoAcao());
		setNivelPermissao(getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro()).getNivelPermissao());
		setBtoAcionadoExcecao(false);
		return "EXCECAO"; 
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try {
			ExcluirRegraSegregacaoEntradaDTO excluirRegraSegregacaoEntradaDTO = new ExcluirRegraSegregacaoEntradaDTO();
			ListarRegraSegregacaoSaidaDTO listarRegraSegregacaoSaidaDTO = getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro());

			excluirRegraSegregacaoEntradaDTO.setCdRegraAcessoDado(listarRegraSegregacaoSaidaDTO.getCodigoRegra());

			ExcluirRegraSegregacaoSaidaDTO excluirRegraSegregacaoSaidaDTO = getRegSegregAcessoDadosServiceImpl().excluirRegraSegregacao(excluirRegraSegregacaoEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + excluirRegraSegregacaoSaidaDTO.getCodMensagem() + ") " + excluirRegraSegregacaoSaidaDTO.getMensagem(), "#{regSegregAcessoDadosBean.consultar}", BradescoViewExceptionActionType.ACTION, false);

			return REGRA_SEGREGACAO;
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try {
			
			IncluirRegraSegregacaoEntradaDTO incluirRegraSegregacaoEntradaDTO = new IncluirRegraSegregacaoEntradaDTO();
		
			incluirRegraSegregacaoEntradaDTO.setTipoUnidadeOrganizacionalUsuario(getTipoUnidadeOrg());
			incluirRegraSegregacaoEntradaDTO.setTipoUnidadeOrganizacionalProprietarioDados(getTipoUnidadeOrgProprietario());
			incluirRegraSegregacaoEntradaDTO.setTipoAcao(getTipoManutencao());
			incluirRegraSegregacaoEntradaDTO.setNivelPermissaoDados(getNivelPermissao());
			
			IncluirRegraSegregacaoSaidaDTO incluirRegraSegregacaoSaidaDTO = getRegSegregAcessoDadosServiceImpl().incluirRegraSegregacao(incluirRegraSegregacaoEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +incluirRegraSegregacaoSaidaDTO.getCdMensagem() + ") " + incluirRegraSegregacaoSaidaDTO.getMensagem(), REGRA_SEGREGACAO, BradescoViewExceptionActionType.ACTION, false);
			consultar();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				return null;
			}
			return "";
		
	}
	
	/**
	 * Preenche lista tipo unidade organizacional filtro.
	 */
	@SuppressWarnings("unchecked")
	public void preencheListaTipoUnidadeOrganizacionalFiltro(){
		try{
			listaTipoUnidadeOrganizacional = new ArrayList<SelectItem>();
			
			List<TipoUnidadeOrganizacionalDTO> listaTipoUnidadeOrganizacionalAux;
			
			TipoUnidadeOrganizacionalDTO tipoUnidadeOrganizacionalDTO = new TipoUnidadeOrganizacionalDTO();		
			tipoUnidadeOrganizacionalDTO.setCdSituacaoVinculacaoConta(1);
		
			listaTipoUnidadeOrganizacionalAux = comboService.listarTipoUnidadeOrganizacional(tipoUnidadeOrganizacionalDTO);
			
			listaTipoUnidadeOrganizacionalHash.clear();
			for(TipoUnidadeOrganizacionalDTO combo : listaTipoUnidadeOrganizacionalAux){
				listaTipoUnidadeOrganizacionalHash.put(combo.getCdTipoUnidade(), combo.getDsTipoUnidade());
				this.listaTipoUnidadeOrganizacional.add(new SelectItem(combo.getCdTipoUnidade(), combo.getDsTipoUnidade()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoUnidadeOrganizacional = new ArrayList<SelectItem>();
		}
		
	}
	
	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar(){
		if(getCodigoRegraFiltro() != null && !getCodigoRegraFiltro().equals("")){
			setTipoManutencaoFiltro(0);
			setTipoUnidadeOrgFiltro(0);
			setNivelPermissaoFiltro(0);
		}
		
		return "";
	}
		
	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar(){
		try {
			
			AlterarRegraSegregacaoEntradaDTO alterarRegraSegregacaoEntradaDTO = new AlterarRegraSegregacaoEntradaDTO();
			
			alterarRegraSegregacaoEntradaDTO.setTipoUnidadeOrganizacionalUsuario(getTipoUnidadeOrg());
			alterarRegraSegregacaoEntradaDTO.setTipoUnidadeOrganizacionalProprietarioDados(getTipoUnidadeOrgProprietario());
			alterarRegraSegregacaoEntradaDTO.setTipoAcao(getTipoManutencao());
			alterarRegraSegregacaoEntradaDTO.setNivelPermissaoAcessoDados(getNivelPermissao());
			
			ListarRegraSegregacaoSaidaDTO  listarRegraSegregacaoSaidaDTO = getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro());
			
			alterarRegraSegregacaoEntradaDTO.setCodigoRegra(listarRegraSegregacaoSaidaDTO.getCodigoRegra());
			
			AlterarRegraSegregacaoSaidaDTO alterarRegraSegregacaoSaidaDTO = getRegSegregAcessoDadosServiceImpl().alterarRegraSegregacao(alterarRegraSegregacaoEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +alterarRegraSegregacaoSaidaDTO.getCodMensagem() + ") " + alterarRegraSegregacaoSaidaDTO.getMensagem(), REGRA_SEGREGACAO, BradescoViewExceptionActionType.ACTION, false);
			consultar();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				return null;
			}
			return "";
	}
	
	/**
	 * Preenche dados.
	 */
	public void preencheDados(){
		
		ListarRegraSegregacaoSaidaDTO listarRegraSegregacaoSaidaDTO = getListaGridRegrasFiltro().get(getItemSelecionadoListaRegrasFiltro());
		DetalharRegraSegregacaoEntradaDTO detalharRegraSegregacaoEntradaDTO = new DetalharRegraSegregacaoEntradaDTO();
	
		detalharRegraSegregacaoEntradaDTO.setCdRegraAcessoDado(listarRegraSegregacaoSaidaDTO.getCodigoRegra());
		
		DetalharRegraSegregacaoSaidaDTO detalharRegraSegregacaoSaidaDTO = getRegSegregAcessoDadosServiceImpl().detalharRegraSegregacao(detalharRegraSegregacaoEntradaDTO);
		setCdRegraAcessoDado(detalharRegraSegregacaoSaidaDTO.getCdRegraAcessoDado());
		setTipoUnidadeOrg(detalharRegraSegregacaoSaidaDTO.getCdTipoUnidadeOrganizacionalUsuario());
		setTipoUnidadeOrganizacionalUsuarioDesc(detalharRegraSegregacaoSaidaDTO.getCdTipoUnidadeOrganizacionalUsuario() + "-" + detalharRegraSegregacaoSaidaDTO.getDsTipoUnidadeOrganizacionalUsuario());
		setTipoUnidadeOrgProprietario(detalharRegraSegregacaoSaidaDTO.getCdTipoUnidadeOrganizacionalProprietarioDados());
		setTipoUnidadeOrganizacionalProprietarioDadosDesc(detalharRegraSegregacaoSaidaDTO.getCdTipoUnidadeOrganizacionalProprietarioDados() + "-" + detalharRegraSegregacaoSaidaDTO.getDsTipoUnidadeOrganizacionalProprietarioDados());
		setTipoManutencao(detalharRegraSegregacaoSaidaDTO.getTipoAcao());
		setNivelPermissao(detalharRegraSegregacaoSaidaDTO.getNivelPermissaoAcessoAosDados());
		setUsuarioInclusao(detalharRegraSegregacaoSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharRegraSegregacaoSaidaDTO.getUsuarioManutencao());
		setComplementoInclusao(detalharRegraSegregacaoSaidaDTO.getComplementoInclusao()==null || detalharRegraSegregacaoSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharRegraSegregacaoSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharRegraSegregacaoSaidaDTO.getComplementoManutencao()== null || detalharRegraSegregacaoSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharRegraSegregacaoSaidaDTO.getComplementoManutencao());
		setTipoCanalInclusao(detalharRegraSegregacaoSaidaDTO.getCdCanalInclusao()==0? "" : detalharRegraSegregacaoSaidaDTO.getCdCanalInclusao() + " - " + detalharRegraSegregacaoSaidaDTO.getDsCanalInclusao());
		setTipoCanalManutencao(detalharRegraSegregacaoSaidaDTO.getCdCanalManutencao()==0? "" : detalharRegraSegregacaoSaidaDTO.getCdCanalManutencao() + " - " + detalharRegraSegregacaoSaidaDTO.getDsCanalManutencao());
		setDataHoraInclusao(detalharRegraSegregacaoSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharRegraSegregacaoSaidaDTO.getDataHoraManutencao());
	}

	
	/**
	 * Preenche dados excecao.
	 */
	public void preencheDadosExcecao(){
		
		ListarExcRegraSegregacaoSaidaDTO listarExcRegraSegregacaoSaidaDTO = getListaGridExcecoesFiltro().get(getItemSelecionadoListaExcecaoFiltro());
		
		DetalharExcRegraSegregacaoEntradaDTO detalharExcRegraSegregacaoEntradaDTO = new DetalharExcRegraSegregacaoEntradaDTO();
	
		detalharExcRegraSegregacaoEntradaDTO.setCodigoExcecao(listarExcRegraSegregacaoSaidaDTO.getCdExcecao());
		detalharExcRegraSegregacaoEntradaDTO.setCodigoRegra(listarExcRegraSegregacaoSaidaDTO.getCdRegra());
		
		DetalharExcRegraSegregacaoSaidaDTO detalharExcRegraSegregacaoSaidaDTO = getRegSegregAcessoDadosServiceImpl().detalharExcRegraSegregacao(detalharExcRegraSegregacaoEntradaDTO);
	
		setCodigoRegraEx(String.valueOf(detalharExcRegraSegregacaoSaidaDTO.getCodigoRegra()));		
		setCodigoEx(String.valueOf(detalharExcRegraSegregacaoSaidaDTO.getCodigoExcecao()));				
		setTipoAbrangenciaExcecaoRadio(detalharExcRegraSegregacaoSaidaDTO.getTipoAbrangenciaExcecao());
		setTipoUnidadeOrgUsuarioExDesc(detalharExcRegraSegregacaoSaidaDTO.getDsTipoUnidadeOrganizacionalUsuario());
		setTipoUnidadeOrgUsuarioEx(detalharExcRegraSegregacaoSaidaDTO.getCdTipoUnidadeOrganizacionalUsuario());
		if (detalharExcRegraSegregacaoSaidaDTO.getTipoAbrangenciaExcecao() == 1 ){			
			setEmpresaConglomeradoUsuarioDesc(detalharExcRegraSegregacaoSaidaDTO.getDsEmpresaConglomeradoUsuario());	
			setEmpresaConglomeradoUsuario(detalharExcRegraSegregacaoSaidaDTO.getCdEmpresaConglomeradoUsuario());
			setUnidadeOrgUsuarioExDesc(detalharExcRegraSegregacaoSaidaDTO.getDsUnidadeOrganizacionalUsuario());
			setUnidadeOrgUsuarioEx(detalharExcRegraSegregacaoSaidaDTO.getCdUnidadeOrganizacionalUsuario()!=0?String.valueOf(detalharExcRegraSegregacaoSaidaDTO.getCdUnidadeOrganizacionalUsuario()):"");
		}else{
			setEmpresaConglomeradoUsuarioDesc("");
			setEmpresaConglomeradoUsuario(0l);
			setUnidadeOrgUsuarioExDesc("");
			setUnidadeOrgUsuarioEx("");
		}
		
		
				
		setUnidadeOrgProprietarioUsuarioEx(detalharExcRegraSegregacaoSaidaDTO.getCdUnidadeOrganizacionalUsuario()!=0?String.valueOf(detalharExcRegraSegregacaoSaidaDTO.getCdUnidadeOrganizacionalUsuario()):"");
		
		setTipoUnidadeOrgProprietarioUsuarioDescEx(detalharExcRegraSegregacaoSaidaDTO.getDsTipoUnidadeOrganizacionalProprietario());
		setTipoUnidadeOrgProprietarioUsuarioEx(detalharExcRegraSegregacaoSaidaDTO.getCdTipoUnidadeOrganizacionalProprietario());
		setEmpresaConglomeradoProprietarioDesc(detalharExcRegraSegregacaoSaidaDTO.getDsEmpresaConglomeradoProprietario());
		setEmpresaConglomeradoProprietario(detalharExcRegraSegregacaoSaidaDTO.getCdEmpresaConglomeradoProprietario());
		setUnidadeOrgProprietarioUsuarioExDesc(detalharExcRegraSegregacaoSaidaDTO.getDsUnidadeOrganizacionalProprietario());
		setUnidadeOrgProprietarioUsuarioEx(detalharExcRegraSegregacaoSaidaDTO.getCdUnidadeOrganizacionalProprietario()!=0?String.valueOf(detalharExcRegraSegregacaoSaidaDTO.getCdUnidadeOrganizacionalProprietario()):"");
		setTipoExcecaoRadio(detalharExcRegraSegregacaoSaidaDTO.getTipoExcecao());
		
		setDataHoraInclusao(detalharExcRegraSegregacaoSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharExcRegraSegregacaoSaidaDTO.getDataHoralManutencao());
		
		setUsuarioInclusao(detalharExcRegraSegregacaoSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharExcRegraSegregacaoSaidaDTO.getUsuariolManutencao());
		
		setTipoCanalInclusao(detalharExcRegraSegregacaoSaidaDTO.getCdTipoCanalInclusao() !=0? detalharExcRegraSegregacaoSaidaDTO.getCdTipoCanalInclusao() + " - "+ detalharExcRegraSegregacaoSaidaDTO.getDsTipoCanalInclusao():"");
		setTipoCanalManutencao(detalharExcRegraSegregacaoSaidaDTO.getCdTipoCanalManutencao() !=0 ?detalharExcRegraSegregacaoSaidaDTO.getCdTipoCanalManutencao() + " - " + detalharExcRegraSegregacaoSaidaDTO.getDsTipoCanallManutencao():"");
		
		setComplementoInclusao(detalharExcRegraSegregacaoSaidaDTO.getComplementoUsuario() != null && !detalharExcRegraSegregacaoSaidaDTO.getComplementoUsuario().equals("0")?detalharExcRegraSegregacaoSaidaDTO.getComplementoUsuario():"");
		setComplementoManutencao(detalharExcRegraSegregacaoSaidaDTO.getComplementolManutencao() != null && !detalharExcRegraSegregacaoSaidaDTO.getComplementolManutencao().equals("0")?detalharExcRegraSegregacaoSaidaDTO.getComplementolManutencao():"");
		
		
	}
		

		// FIM METODOS PRINCIPAL ----------------------------------------------------------
	
	// SETS e GETS ************************************

	/**
		 * Get: nivelPermissaoFiltro.
		 *
		 * @return nivelPermissaoFiltro
		 */
		public Integer getNivelPermissaoFiltro() {
		return nivelPermissaoFiltro;
	}
	
	/**
	 * Set: nivelPermissaoFiltro.
	 *
	 * @param nivelPermissaoFiltro the nivel permissao filtro
	 */
	public void setNivelPermissaoFiltro(Integer nivelPermissaoFiltro) {
		this.nivelPermissaoFiltro = nivelPermissaoFiltro;
	}
	
	/**
	 * Get: tipoManutencaoFiltro.
	 *
	 * @return tipoManutencaoFiltro
	 */
	public Integer getTipoManutencaoFiltro() {
		return tipoManutencaoFiltro;
	}
	
	/**
	 * Set: tipoManutencaoFiltro.
	 *
	 * @param tipoManutencaoFiltro the tipo manutencao filtro
	 */
	public void setTipoManutencaoFiltro(Integer tipoManutencaoFiltro) {
		this.tipoManutencaoFiltro = tipoManutencaoFiltro;
	}
	
	/**
	 * Get: tipoUnidadeOrgFiltro.
	 *
	 * @return tipoUnidadeOrgFiltro
	 */
	public Integer getTipoUnidadeOrgFiltro() {
		return tipoUnidadeOrgFiltro;
	}
	
	/**
	 * Set: tipoUnidadeOrgFiltro.
	 *
	 * @param tipoUnidadeOrgFiltro the tipo unidade org filtro
	 */
	public void setTipoUnidadeOrgFiltro(Integer tipoUnidadeOrgFiltro) {
		this.tipoUnidadeOrgFiltro = tipoUnidadeOrgFiltro;
	}
	
	/**
	 * Get: listaGridRegrasFiltro.
	 *
	 * @return listaGridRegrasFiltro
	 */
	public List<ListarRegraSegregacaoSaidaDTO> getListaGridRegrasFiltro() {
		return listaGridRegrasFiltro;
	}

	/**
	 * Set: listaGridRegrasFiltro.
	 *
	 * @param listaGridRegrasFiltro the lista grid regras filtro
	 */
	public void setListaGridRegrasFiltro(
			List<ListarRegraSegregacaoSaidaDTO> listaGridRegrasFiltro) {
		this.listaGridRegrasFiltro = listaGridRegrasFiltro;
	}

	/**
	 * Get: tipoManutencao.
	 *
	 * @return tipoManutencao
	 */
	public Integer getTipoManutencao() {
		return tipoManutencao;
	}

	/**
	 * Set: tipoManutencao.
	 *
	 * @param tipoManutencao the tipo manutencao
	 */
	public void setTipoManutencao(Integer tipoManutencao) {
		this.tipoManutencao = tipoManutencao;
	}

	/**
	 * Get: tipoUnidadeOrgProprietario.
	 *
	 * @return tipoUnidadeOrgProprietario
	 */
	public Integer getTipoUnidadeOrgProprietario() {
		return tipoUnidadeOrgProprietario;
	}
	
	/**
	 * Set: tipoUnidadeOrgProprietario.
	 *
	 * @param tipoUnidadeOrgProprietario the tipo unidade org proprietario
	 */
	public void setTipoUnidadeOrgProprietario(Integer tipoUnidadeOrgProprietario) {
		this.tipoUnidadeOrgProprietario = tipoUnidadeOrgProprietario;
	}
	
	/**
	 * Get: perfilGerenciadorSegDesc.
	 *
	 * @return perfilGerenciadorSegDesc
	 */
	public Integer getPerfilGerenciadorSegDesc() {
		return perfilGerenciadorSegDesc;
	}
	
	/**
	 * Set: perfilGerenciadorSegDesc.
	 *
	 * @param perfilGerenciadorSegDesc the perfil gerenciador seg desc
	 */
	public void setPerfilGerenciadorSegDesc(Integer perfilGerenciadorSegDesc) {
		this.perfilGerenciadorSegDesc = perfilGerenciadorSegDesc;
	}
	
	/**
	 * Get: nivelPermissao.
	 *
	 * @return nivelPermissao
	 */
	public Integer getNivelPermissao() {
		return nivelPermissao;
	}

	/**
	 * Set: nivelPermissao.
	 *
	 * @param nivelPermissao the nivel permissao
	 */
	public void setNivelPermissao(Integer nivelPermissao) {
		this.nivelPermissao = nivelPermissao;
	}

	/**
	 * Get: perfilGerenciadorSegurancaDesc.
	 *
	 * @return perfilGerenciadorSegurancaDesc
	 */
	public String getPerfilGerenciadorSegurancaDesc() {
		return perfilGerenciadorSegurancaDesc;
	}
	
	/**
	 * Set: perfilGerenciadorSegurancaDesc.
	 *
	 * @param perfilGerenciadorSegurancaDesc the perfil gerenciador seguranca desc
	 */
	public void setPerfilGerenciadorSegurancaDesc(
			String perfilGerenciadorSegurancaDesc) {
		this.perfilGerenciadorSegurancaDesc = perfilGerenciadorSegurancaDesc;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalProprietarioDadosDesc.
	 *
	 * @return tipoUnidadeOrganizacionalProprietarioDadosDesc
	 */
	public String getTipoUnidadeOrganizacionalProprietarioDadosDesc() {
		return tipoUnidadeOrganizacionalProprietarioDadosDesc;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalProprietarioDadosDesc.
	 *
	 * @param tipoUnidadeOrganizacionalProprietarioDadosDesc the tipo unidade organizacional proprietario dados desc
	 */
	public void setTipoUnidadeOrganizacionalProprietarioDadosDesc(
			String tipoUnidadeOrganizacionalProprietarioDadosDesc) {
		this.tipoUnidadeOrganizacionalProprietarioDadosDesc = tipoUnidadeOrganizacionalProprietarioDadosDesc;
	}
	
	/**
	 * Get: tipoUnidadeOrganizacionalUsuarioDesc.
	 *
	 * @return tipoUnidadeOrganizacionalUsuarioDesc
	 */
	public String getTipoUnidadeOrganizacionalUsuarioDesc() {
		return tipoUnidadeOrganizacionalUsuarioDesc;
	}
	
	/**
	 * Set: tipoUnidadeOrganizacionalUsuarioDesc.
	 *
	 * @param tipoUnidadeOrganizacionalUsuarioDesc the tipo unidade organizacional usuario desc
	 */
	public void setTipoUnidadeOrganizacionalUsuarioDesc(
			String tipoUnidadeOrganizacionalUsuarioDesc) {
		this.tipoUnidadeOrganizacionalUsuarioDesc = tipoUnidadeOrganizacionalUsuarioDesc;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}
	
	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}
	
	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}
	
	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}
	
	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}
	
	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}
	
	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}
	
	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}
	
	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}
	
	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}
	
	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}
	
	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}
	
	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}
	
	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}
	
	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: tipoExececaoAcessoFiltro.
	 *
	 * @return tipoExececaoAcessoFiltro
	 */
	public Integer getTipoExececaoAcessoFiltro() {
		return tipoExececaoAcessoFiltro;
	}
	
	/**
	 * Set: tipoExececaoAcessoFiltro.
	 *
	 * @param tipoExececaoAcessoFiltro the tipo exececao acesso filtro
	 */
	public void setTipoExececaoAcessoFiltro(Integer tipoExececaoAcessoFiltro) {
		this.tipoExececaoAcessoFiltro = tipoExececaoAcessoFiltro;
	}
	
	/**
	 * Get: tipoUnidadeOrgFiltroEx.
	 *
	 * @return tipoUnidadeOrgFiltroEx
	 */
	public Integer getTipoUnidadeOrgFiltroEx() {
		return tipoUnidadeOrgFiltroEx;
	}
	
	/**
	 * Set: tipoUnidadeOrgFiltroEx.
	 *
	 * @param tipoUnidadeOrgFiltroEx the tipo unidade org filtro ex
	 */
	public void setTipoUnidadeOrgFiltroEx(Integer tipoUnidadeOrgFiltroEx) {
		this.tipoUnidadeOrgFiltroEx = tipoUnidadeOrgFiltroEx;
	}
	
	/**
	 * Get: tipoAbrangenciaAcessoFiltro.
	 *
	 * @return tipoAbrangenciaAcessoFiltro
	 */
	public Integer getTipoAbrangenciaAcessoFiltro() {
		return tipoAbrangenciaAcessoFiltro;
	}
	
	/**
	 * Set: tipoAbrangenciaAcessoFiltro.
	 *
	 * @param tipoAbrangenciaAcessoFiltro the tipo abrangencia acesso filtro
	 */
	public void setTipoAbrangenciaAcessoFiltro(Integer tipoAbrangenciaAcessoFiltro) {
		this.tipoAbrangenciaAcessoFiltro = tipoAbrangenciaAcessoFiltro;
	}
	
	/**
	 * Get: tipoAbrangencia.
	 *
	 * @return tipoAbrangencia
	 */
	public String getTipoAbrangencia() {
		return tipoAbrangencia;
	}

	/**
	 * Set: tipoAbrangencia.
	 *
	 * @param tipoAbrangencia the tipo abrangencia
	 */
	public void setTipoAbrangencia(String tipoAbrangencia) {
		this.tipoAbrangencia = tipoAbrangencia;
	}

	/**
	 * Get: tipoExcecao.
	 *
	 * @return tipoExcecao
	 */
	public String getTipoExcecao() {
		return tipoExcecao;
	}

	/**
	 * Set: tipoExcecao.
	 *
	 * @param tipoExcecao the tipo excecao
	 */
	public void setTipoExcecao(String tipoExcecao) {
		this.tipoExcecao = tipoExcecao;
	}
	
	/**
	 * Get: unidadeOrgProprietarioUsuarioEx.
	 *
	 * @return unidadeOrgProprietarioUsuarioEx
	 */
	public String getUnidadeOrgProprietarioUsuarioEx() {
		return unidadeOrgProprietarioUsuarioEx;
	}

	/**
	 * Set: unidadeOrgProprietarioUsuarioEx.
	 *
	 * @param unidadeOrgProprietarioUsuarioEx the unidade org proprietario usuario ex
	 */
	public void setUnidadeOrgProprietarioUsuarioEx(
			String unidadeOrgProprietarioUsuarioEx) {
		this.unidadeOrgProprietarioUsuarioEx = unidadeOrgProprietarioUsuarioEx;
	}

	/**
	 * Get: unidadeOrgUsuarioEx.
	 *
	 * @return unidadeOrgUsuarioEx
	 */
	public String getUnidadeOrgUsuarioEx() {
		return unidadeOrgUsuarioEx;
	}

	/**
	 * Set: unidadeOrgUsuarioEx.
	 *
	 * @param unidadeOrgUsuarioEx the unidade org usuario ex
	 */
	public void setUnidadeOrgUsuarioEx(String unidadeOrgUsuarioEx) {
		this.unidadeOrgUsuarioEx = unidadeOrgUsuarioEx;
	}
	
	/**
	 * Get: unidadeOrgProprietarioUsuarioExDesc.
	 *
	 * @return unidadeOrgProprietarioUsuarioExDesc
	 */
	public String getUnidadeOrgProprietarioUsuarioExDesc() {
		return unidadeOrgProprietarioUsuarioExDesc;
	}
	
	/**
	 * Set: unidadeOrgProprietarioUsuarioExDesc.
	 *
	 * @param unidadeOrgProprietarioUsuarioExDesc the unidade org proprietario usuario ex desc
	 */
	public void setUnidadeOrgProprietarioUsuarioExDesc(
			String unidadeOrgProprietarioUsuarioExDesc) {
		this.unidadeOrgProprietarioUsuarioExDesc = unidadeOrgProprietarioUsuarioExDesc;
	}

	
	/**
	 * Get: tipoUnidadeOrgProprietarioUsuarioDescEx.
	 *
	 * @return tipoUnidadeOrgProprietarioUsuarioDescEx
	 */
	public String getTipoUnidadeOrgProprietarioUsuarioDescEx() {
		return tipoUnidadeOrgProprietarioUsuarioDescEx;
	}
	
	/**
	 * Set: tipoUnidadeOrgProprietarioUsuarioDescEx.
	 *
	 * @param tipoUnidadeOrgProprietarioUsuarioDescEx the tipo unidade org proprietario usuario desc ex
	 */
	public void setTipoUnidadeOrgProprietarioUsuarioDescEx(
			String tipoUnidadeOrgProprietarioUsuarioDescEx) {
		this.tipoUnidadeOrgProprietarioUsuarioDescEx = tipoUnidadeOrgProprietarioUsuarioDescEx;
	}
	
	/**
	 * Get: tipoUnidadeOrgUsuarioExDesc.
	 *
	 * @return tipoUnidadeOrgUsuarioExDesc
	 */
	public String getTipoUnidadeOrgUsuarioExDesc() {
		return tipoUnidadeOrgUsuarioExDesc;
	}
	
	/**
	 * Set: tipoUnidadeOrgUsuarioExDesc.
	 *
	 * @param tipoUnidadeOrgUsuarioExDesc the tipo unidade org usuario ex desc
	 */
	public void setTipoUnidadeOrgUsuarioExDesc(String tipoUnidadeOrgUsuarioExDesc) {
		this.tipoUnidadeOrgUsuarioExDesc = tipoUnidadeOrgUsuarioExDesc;
	}

	/**
	 * Get: empresaConglomeradoProprietario.
	 *
	 * @return empresaConglomeradoProprietario
	 */
	public Long getEmpresaConglomeradoProprietario() {
		return empresaConglomeradoProprietario;
	}

	/**
	 * Set: empresaConglomeradoProprietario.
	 *
	 * @param empresaConglomeradoProprietario the empresa conglomerado proprietario
	 */
	public void setEmpresaConglomeradoProprietario(
			Long empresaConglomeradoProprietario) {
		this.empresaConglomeradoProprietario = empresaConglomeradoProprietario;
	}

	/**
	 * Get: empresaConglomeradoUsuario.
	 *
	 * @return empresaConglomeradoUsuario
	 */
	public Long getEmpresaConglomeradoUsuario() {
		return empresaConglomeradoUsuario;
	}

	/**
	 * Set: empresaConglomeradoUsuario.
	 *
	 * @param empresaConglomeradoUsuario the empresa conglomerado usuario
	 */
	public void setEmpresaConglomeradoUsuario(Long empresaConglomeradoUsuario) {
		this.empresaConglomeradoUsuario = empresaConglomeradoUsuario;
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: codigoRegraFiltro.
	 *
	 * @return codigoRegraFiltro
	 */
	public String getCodigoRegraFiltro() {
		return codigoRegraFiltro;
	}

	/**
	 * Set: codigoRegraFiltro.
	 *
	 * @param codigoRegraFiltro the codigo regra filtro
	 */
	public void setCodigoRegraFiltro(String codigoRegraFiltro) {
		this.codigoRegraFiltro = codigoRegraFiltro;
	}

	/**
	 * Get: listaTipoUnidadeOrganizacional.
	 *
	 * @return listaTipoUnidadeOrganizacional
	 */
	public List<SelectItem> getListaTipoUnidadeOrganizacional() {
		return listaTipoUnidadeOrganizacional;
	}

	/**
	 * Set: listaTipoUnidadeOrganizacional.
	 *
	 * @param listaTipoUnidadeOrganizacionalFiltro the lista tipo unidade organizacional
	 */
	public void setListaTipoUnidadeOrganizacional(
			List<SelectItem> listaTipoUnidadeOrganizacionalFiltro) {
		this.listaTipoUnidadeOrganizacional = listaTipoUnidadeOrganizacionalFiltro;
	}

	/**
	 * Get: itemSelecionadoListaRegrasFiltro.
	 *
	 * @return itemSelecionadoListaRegrasFiltro
	 */
	public Integer getItemSelecionadoListaRegrasFiltro() {
		return itemSelecionadoListaRegrasFiltro;
	}

	/**
	 * Set: itemSelecionadoListaRegrasFiltro.
	 *
	 * @param itemSelecionadoListaRegrasFiltro the item selecionado lista regras filtro
	 */
	public void setItemSelecionadoListaRegrasFiltro(
			Integer itemSelecionadoListaRegrasFiltro) {
		this.itemSelecionadoListaRegrasFiltro = itemSelecionadoListaRegrasFiltro;
	}

	/**
	 * Get: listaControleRadioRegrasFiltro.
	 *
	 * @return listaControleRadioRegrasFiltro
	 */
	public List<SelectItem> getListaControleRadioRegrasFiltro() {
		return listaControleRadioRegrasFiltro;
	}

	/**
	 * Set: listaControleRadioRegrasFiltro.
	 *
	 * @param listaControleRadioRegrasFiltro the lista controle radio regras filtro
	 */
	public void setListaControleRadioRegrasFiltro(
			List<SelectItem> listaControleRadioRegrasFiltro) {
		this.listaControleRadioRegrasFiltro = listaControleRadioRegrasFiltro;
	}

	/**
	 * Get: tipoUnidadeOrg.
	 *
	 * @return tipoUnidadeOrg
	 */
	public Integer getTipoUnidadeOrg() {
		return tipoUnidadeOrg;
	}

	/**
	 * Set: tipoUnidadeOrg.
	 *
	 * @param tipoUnidadeOrg the tipo unidade org
	 */
	public void setTipoUnidadeOrg(Integer tipoUnidadeOrg) {
		this.tipoUnidadeOrg = tipoUnidadeOrg;
	}



	
	/**
	 * Get: listaGridExcecoesFiltro.
	 *
	 * @return listaGridExcecoesFiltro
	 */
	public List<ListarExcRegraSegregacaoSaidaDTO> getListaGridExcecoesFiltro() {
		return listaGridExcecoesFiltro;
	}

	/**
	 * Set: listaGridExcecoesFiltro.
	 *
	 * @param listaGridExcecoesFiltro the lista grid excecoes filtro
	 */
	public void setListaGridExcecoesFiltro(
			List<ListarExcRegraSegregacaoSaidaDTO> listaGridExcecoesFiltro) {
		this.listaGridExcecoesFiltro = listaGridExcecoesFiltro;
	}

	/**
	 * Get: itemSelecionadoListaExcecaoFiltro.
	 *
	 * @return itemSelecionadoListaExcecaoFiltro
	 */
	public Integer getItemSelecionadoListaExcecaoFiltro() {
		return itemSelecionadoListaExcecaoFiltro;
	}

	/**
	 * Set: itemSelecionadoListaExcecaoFiltro.
	 *
	 * @param itemSelecionadoListaExcecaoFiltro the item selecionado lista excecao filtro
	 */
	public void setItemSelecionadoListaExcecaoFiltro(
			Integer itemSelecionadoListaExcecaoFiltro) {
		this.itemSelecionadoListaExcecaoFiltro = itemSelecionadoListaExcecaoFiltro;
	}

	/**
	 * Get: listaControleRadioExcecoesFiltro.
	 *
	 * @return listaControleRadioExcecoesFiltro
	 */
	public List<SelectItem> getListaControleRadioExcecoesFiltro() {
		return listaControleRadioExcecoesFiltro;
	}

	/**
	 * Set: listaControleRadioExcecoesFiltro.
	 *
	 * @param listaControleRadioExcecoesFiltro the lista controle radio excecoes filtro
	 */
	public void setListaControleRadioExcecoesFiltro(
			List<SelectItem> listaControleRadioExcecoesFiltro) {
		this.listaControleRadioExcecoesFiltro = listaControleRadioExcecoesFiltro;
	}

	/**
	 * Get: codigoRegraEx.
	 *
	 * @return codigoRegraEx
	 */
	public String getCodigoRegraEx() {
		return codigoRegraEx;
	}

	/**
	 * Set: codigoRegraEx.
	 *
	 * @param codigoRegraEx the codigo regra ex
	 */
	public void setCodigoRegraEx(String codigoRegraEx) {
		this.codigoRegraEx = codigoRegraEx;
	}

	/**
	 * Get: tipoAbrangenciaExcecaoRadio.
	 *
	 * @return tipoAbrangenciaExcecaoRadio
	 */
	public Integer getTipoAbrangenciaExcecaoRadio() {
		return tipoAbrangenciaExcecaoRadio;
	}

	/**
	 * Set: tipoAbrangenciaExcecaoRadio.
	 *
	 * @param tipoAbrangenciaExcecaoRadio the tipo abrangencia excecao radio
	 */
	public void setTipoAbrangenciaExcecaoRadio(Integer tipoAbrangenciaExcecaoRadio) {
		this.tipoAbrangenciaExcecaoRadio = tipoAbrangenciaExcecaoRadio;
	}

	/**
	 * Get: tipoUnidadeOrgUsuarioEx.
	 *
	 * @return tipoUnidadeOrgUsuarioEx
	 */
	public Integer getTipoUnidadeOrgUsuarioEx() {
		return tipoUnidadeOrgUsuarioEx;
	}

	/**
	 * Set: tipoUnidadeOrgUsuarioEx.
	 *
	 * @param tipoUnidadeOrgUsuarioEx the tipo unidade org usuario ex
	 */
	public void setTipoUnidadeOrgUsuarioEx(Integer tipoUnidadeOrgUsuarioEx) {
		this.tipoUnidadeOrgUsuarioEx = tipoUnidadeOrgUsuarioEx;
	}

	/**
	 * Get: tipoUnidadeOrgProprietarioUsuarioEx.
	 *
	 * @return tipoUnidadeOrgProprietarioUsuarioEx
	 */
	public Integer getTipoUnidadeOrgProprietarioUsuarioEx() {
		return tipoUnidadeOrgProprietarioUsuarioEx;
	}

	/**
	 * Set: tipoUnidadeOrgProprietarioUsuarioEx.
	 *
	 * @param tipoUnidadeOrgProprietarioUsuarioEx the tipo unidade org proprietario usuario ex
	 */
	public void setTipoUnidadeOrgProprietarioUsuarioEx(
			Integer tipoUnidadeOrgProprietarioUsuarioEx) {
		this.tipoUnidadeOrgProprietarioUsuarioEx = tipoUnidadeOrgProprietarioUsuarioEx;
	}

	/**
	 * Get: tipoExcecaoRadio.
	 *
	 * @return tipoExcecaoRadio
	 */
	public Integer getTipoExcecaoRadio() {
		return tipoExcecaoRadio;
	}

	/**
	 * Set: tipoExcecaoRadio.
	 *
	 * @param tipoExcecaoRadio the tipo excecao radio
	 */
	public void setTipoExcecaoRadio(Integer tipoExcecaoRadio) {
		this.tipoExcecaoRadio = tipoExcecaoRadio;
	}

	/**
	 * Get: unidadeOrgUsuarioExDesc.
	 *
	 * @return unidadeOrgUsuarioExDesc
	 */
	public String getUnidadeOrgUsuarioExDesc() {
		return unidadeOrgUsuarioExDesc;
	}

	/**
	 * Set: unidadeOrgUsuarioExDesc.
	 *
	 * @param unidadeOrgUsuarioExDesc the unidade org usuario ex desc
	 */
	public void setUnidadeOrgUsuarioExDesc(String unidadeOrgUsuarioExDesc) {
		this.unidadeOrgUsuarioExDesc = unidadeOrgUsuarioExDesc;
	}
	
	


	/**
	 * Get: empresaConglomeradoProprietarioDesc.
	 *
	 * @return empresaConglomeradoProprietarioDesc
	 */
	public String getEmpresaConglomeradoProprietarioDesc() {
		return empresaConglomeradoProprietarioDesc;
	}

	/**
	 * Set: empresaConglomeradoProprietarioDesc.
	 *
	 * @param empresaConglomeradoProprietarioDesc the empresa conglomerado proprietario desc
	 */
	public void setEmpresaConglomeradoProprietarioDesc(
			String empresaConglomeradoProprietarioDesc) {
		this.empresaConglomeradoProprietarioDesc = empresaConglomeradoProprietarioDesc;
	}

	/**
	 * Get: empresaConglomeradoUsuarioDesc.
	 *
	 * @return empresaConglomeradoUsuarioDesc
	 */
	public String getEmpresaConglomeradoUsuarioDesc() {
		return empresaConglomeradoUsuarioDesc;
	}

	/**
	 * Set: empresaConglomeradoUsuarioDesc.
	 *
	 * @param empresaConglomeradoUsuarioDesc the empresa conglomerado usuario desc
	 */
	public void setEmpresaConglomeradoUsuarioDesc(
			String empresaConglomeradoUsuarioDesc) {
		this.empresaConglomeradoUsuarioDesc = empresaConglomeradoUsuarioDesc;
	}

	/**
	 * Get: codigoEx.
	 *
	 * @return codigoEx
	 */
	public String getCodigoEx() {
		return codigoEx;
	}

	/**
	 * Set: codigoEx.
	 *
	 * @param codigoEx the codigo ex
	 */
	public void setCodigoEx(String codigoEx) {
		this.codigoEx = codigoEx;
	}

	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: regSegregAcessoDadosServiceImpl.
	 *
	 * @return regSegregAcessoDadosServiceImpl
	 */
	public IRegSegregAcessoDadosService getRegSegregAcessoDadosServiceImpl() {
		return regSegregAcessoDadosServiceImpl;
	}

	/**
	 * Set: regSegregAcessoDadosServiceImpl.
	 *
	 * @param regSegregAcessoDadosServiceImpl the reg segreg acesso dados service impl
	 */
	public void setRegSegregAcessoDadosServiceImpl(
			IRegSegregAcessoDadosService regSegregAcessoDadosServiceImpl) {
		this.regSegregAcessoDadosServiceImpl = regSegregAcessoDadosServiceImpl;
	}

	

	/**
	 * Get: listaTipoUnidadeOrganizacionalHash.
	 *
	 * @return listaTipoUnidadeOrganizacionalHash
	 */
	public Map<Integer, String> getListaTipoUnidadeOrganizacionalHash() {
		return listaTipoUnidadeOrganizacionalHash;
	}


	/**
	 * Set lista tipo unidade organizacional hash.
	 *
	 * @param listaTipoUnidadeOrganizacionalHash the lista tipo unidade organizacional hash
	 */
	public void setListaTipoUnidadeOrganizacionalHash(
			Map<Integer, String> listaTipoUnidadeOrganizacionalHash) {
		this.listaTipoUnidadeOrganizacionalHash = listaTipoUnidadeOrganizacionalHash;
	}


	/**
	 * Get: cdRegraAcessoDado.
	 *
	 * @return cdRegraAcessoDado
	 */
	public Integer getCdRegraAcessoDado() {
		return cdRegraAcessoDado;
	}

	/**
	 * Set: cdRegraAcessoDado.
	 *
	 * @param cdRegraAcessoDado the cd regra acesso dado
	 */
	public void setCdRegraAcessoDado(Integer cdRegraAcessoDado) {
		this.cdRegraAcessoDado = cdRegraAcessoDado;
	}

	/**
	 * Get: listaEmpresaConglomerado.
	 *
	 * @return listaEmpresaConglomerado
	 */
	public List<SelectItem> getListaEmpresaConglomerado() {
		return listaEmpresaConglomerado;
	}

	/**
	 * Set: listaEmpresaConglomerado.
	 *
	 * @param listaEmpresaConglomerado the lista empresa conglomerado
	 */
	public void setListaEmpresaConglomerado(
			List<SelectItem> listaEmpresaConglomerado) {
		this.listaEmpresaConglomerado = listaEmpresaConglomerado;
	}

	/**
	 * Get: listaEmpresaConglomeradoHash.
	 *
	 * @return listaEmpresaConglomeradoHash
	 */
	public Map<Long, String> getListaEmpresaConglomeradoHash() {
		return listaEmpresaConglomeradoHash;
	}


	/**
	 * Set lista empresa conglomerado hash.
	 *
	 * @param listaEmpresaConglomeradoHash the lista empresa conglomerado hash
	 */
	public void setListaEmpresaConglomeradoHash(
			Map<Long, String> listaEmpresaConglomeradoHash) {
		this.listaEmpresaConglomeradoHash = listaEmpresaConglomeradoHash;
	}


	/**
	 * Get: listaEmpresaConglomeradoProprietarioHash.
	 *
	 * @return listaEmpresaConglomeradoProprietarioHash
	 */
	public Map<Long, String> getListaEmpresaConglomeradoProprietarioHash() {
		return listaEmpresaConglomeradoProprietarioHash;
	}


	/**
	 * Set lista empresa conglomerado proprietario hash.
	 *
	 * @param listaEmpresaConglomeradoProprietarioHash the lista empresa conglomerado proprietario hash
	 */
	public void setListaEmpresaConglomeradoProprietarioHash(
			Map<Long, String> listaEmpresaConglomeradoProprietarioHash) {
		this.listaEmpresaConglomeradoProprietarioHash = listaEmpresaConglomeradoProprietarioHash;
	}


	/**
	 * Get: listaEmpresaConglomeradoProprietario.
	 *
	 * @return listaEmpresaConglomeradoProprietario
	 */
	public List<SelectItem> getListaEmpresaConglomeradoProprietario() {
		return listaEmpresaConglomeradoProprietario;
	}


	/**
	 * Set: listaEmpresaConglomeradoProprietario.
	 *
	 * @param listaEmpresaConglomeradoProprietario the lista empresa conglomerado proprietario
	 */
	public void setListaEmpresaConglomeradoProprietario(
			List<SelectItem> listaEmpresaConglomeradoProprietario) {
		this.listaEmpresaConglomeradoProprietario = listaEmpresaConglomeradoProprietario;
	}

	
	/**
	 * Limpar usuario.
	 */
	public void limparUsuario(){
		if (getTipoAbrangenciaExcecaoRadio() != 1){
			setEmpresaConglomeradoUsuario(0l);			
			setUnidadeOrgUsuarioEx("");			
		}
	}


	/**
	 * Get: cadTipPendenciaService.
	 *
	 * @return cadTipPendenciaService
	 */
	public ICadTipPendenciaService getCadTipPendenciaService() {
		return cadTipPendenciaService;
	}


	/**
	 * Set: cadTipPendenciaService.
	 *
	 * @param cadTipPendenciaService the cad tip pendencia service
	 */
	public void setCadTipPendenciaService(
			ICadTipPendenciaService cadTipPendenciaService) {
		this.cadTipPendenciaService = cadTipPendenciaService;
	}


	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}


	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}


	/**
	 * Get: btoAcionadoExcecao.
	 *
	 * @return btoAcionadoExcecao
	 */
	public Boolean getBtoAcionadoExcecao() {
		return btoAcionadoExcecao;
	}


	/**
	 * Set: btoAcionadoExcecao.
	 *
	 * @param btoAcionadoExcecao the bto acionado excecao
	 */
	public void setBtoAcionadoExcecao(Boolean btoAcionadoExcecao) {
		this.btoAcionadoExcecao = btoAcionadoExcecao;
	}


	/**
	 * Get: rdoSelArgPesq.
	 *
	 * @return rdoSelArgPesq
	 */
	public String getRdoSelArgPesq() {
		return rdoSelArgPesq;
	}


	/**
	 * Set: rdoSelArgPesq.
	 *
	 * @param rdoSelArgPesq the rdo sel arg pesq
	 */
	public void setRdoSelArgPesq(String rdoSelArgPesq) {
		this.rdoSelArgPesq = rdoSelArgPesq;
	}
	
}
