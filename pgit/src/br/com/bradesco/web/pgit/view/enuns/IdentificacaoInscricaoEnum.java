/*
 * Nome: br.com.bradesco.web.pgit.view.enuns
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.enuns;

/**
 * Nome: IdentificacaoInscricaoEnum
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public enum IdentificacaoInscricaoEnum {
	
	/** Atributo CPF. */
	CPF(1, "CPF"),
	
	/** Atributo CNPJ. */
	CNPJ(2, "CNPJ"),
	
	/** Atributo OUTROS. */
	OUTROS(3, "Outros");

	/** Atributo cdIdentificacao. */
	private Integer cdIdentificacao = null;
	
	/** Atributo dsIdentificacao. */
	private String dsIdentificacao = null;

	/**
	 * Identificacao inscricao enum.
	 *
	 * @param cdIdentificacao the cd identificacao
	 * @param dsIdentificacao the ds identificacao
	 */
	private IdentificacaoInscricaoEnum(Integer cdIdentificacao, String dsIdentificacao) {
		this.cdIdentificacao = cdIdentificacao;
		this.dsIdentificacao = dsIdentificacao;
	}

	/**
	 * Get: cdIdentificacao.
	 *
	 * @return cdIdentificacao
	 */
	public Integer getCdIdentificacao() {
		return cdIdentificacao;
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this.dsIdentificacao;
	}	

}
