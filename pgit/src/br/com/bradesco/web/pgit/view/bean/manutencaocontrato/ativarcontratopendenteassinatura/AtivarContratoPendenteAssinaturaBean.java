/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.ativarcontratopendenteassinatura
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.ativarcontratopendenteassinatura;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura.IAtivarContratoPendenteAssinaturaService;
import br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura.bean.AtivarContratoPendAssinaturaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.ativarcontratopendenteassinatura.bean.AtivarContratoPendAssinaturaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: AtivarContratoPendenteAssinaturaBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AtivarContratoPendenteAssinaturaBean {
	
	////////////////////////////
	//DECLARA��O DE VARI�VEIS//
	//////////////////////////
	
	/** Atributo cpfCnpjClienteMaster. */
	private String cpfCnpjClienteMaster;
	
	/** Atributo nomeRazaoSocialClienteMaster. */
	private String nomeRazaoSocialClienteMaster;
	
	/** Atributo grupoEconomico. */
	private String grupoEconomico;
	
	/** Atributo atividadeEconomica. */
	private String atividadeEconomica;
	
	/** Atributo segmento. */
	private String segmento;
	
	/** Atributo subSegmento. */
	private String subSegmento;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;
	
	/** Atributo gerenteResponsavel. */
	private String gerenteResponsavel;
	
	/** Atributo empresa. */
	private String empresa;
	
	/** Atributo tipo. */
	private String tipo;
	
	/** Atributo numero. */
	private String numero;
	
	/** Atributo situacao. */
	private String situacao;
	
	/** Atributo motivo. */
	private String motivo;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo participacaoContrato. */
	private String participacaoContrato;
	
	/** Atributo cdEmpresa. */
	private String cdEmpresa;
	
	/** Atributo cdTipo. */
	private String cdTipo;
	
	/** Atributo hiddenObrigatoriedade. */
	private String hiddenObrigatoriedade;
	//private String hiddenObrigatoriedade2;
	
	/** Atributo numeroAditivo. */
	private String numeroAditivo;
	
	/** Atributo situacaoAditivo. */
	private String situacaoAditivo;
	
	/** Atributo dataHoraCadastramentoAditivo. */
	private Date dataHoraCadastramentoAditivo;
	
	/** Atributo motivoAtivacaoCbo. */
	private Integer motivoAtivacaoCbo;
	
	/** Atributo motivoAtivacaoLista. */
	private List<SelectItem> motivoAtivacaoLista =  new ArrayList<SelectItem>();
	
	/** Atributo motivoAtivacaoListaHash. */
	private Map<Integer, String> motivoAtivacaoListaHash = new HashMap<Integer, String>();
	
	/** Atributo motivoAtivacao. */
	private String motivoAtivacao;
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo ativarContratoPendenteAssinaturaServiceImpl. */
	private IAtivarContratoPendenteAssinaturaService ativarContratoPendenteAssinaturaServiceImpl;
	
	/////////////////////
	//M�TODOS DIVERSOS//
	///////////////////
	
	
	/**
	 * Get: ativarContratoPendenteAssinaturaServiceImpl.
	 *
	 * @return ativarContratoPendenteAssinaturaServiceImpl
	 */
	public IAtivarContratoPendenteAssinaturaService getAtivarContratoPendenteAssinaturaServiceImpl() {
		return ativarContratoPendenteAssinaturaServiceImpl;
	}

	/**
	 * Set: ativarContratoPendenteAssinaturaServiceImpl.
	 *
	 * @param ativarContratoPendenteAssinaturaServiceImpl the ativar contrato pendente assinatura service impl
	 */
	public void setAtivarContratoPendenteAssinaturaServiceImpl(
			IAtivarContratoPendenteAssinaturaService ativarContratoPendenteAssinaturaServiceImpl) {
		this.ativarContratoPendenteAssinaturaServiceImpl = ativarContratoPendenteAssinaturaServiceImpl;
	}

	/**
	 * Ativar pesquisar contrato.
	 *
	 * @return the string
	 */
	public String ativarPesquisarContrato(){   //m�todo ativar da p�gina pesquisar Contrato Pendente Assinatura
		
		carregaListaMotivo();

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		this.setCpfCnpjClienteMaster(saidaDTO.getCnpjOuCpfFormatado());
		this.setNomeRazaoSocialClienteMaster(saidaDTO.getNmRazaoSocialRepresentante());
		this.setGrupoEconomico(String.valueOf(saidaDTO.getCdGrupoEconomico()));
		this.setAtividadeEconomica(saidaDTO.getDsAtividadeEconomica());
		this.setSegmento(saidaDTO.getDsSegmentoCliente());
		this.setSubSegmento(saidaDTO.getDsSubSegmentoCliente());
		this.setEmpresa(saidaDTO.getDsPessoaJuridica());
		this.setTipo(saidaDTO.getDsTipoContrato());
		this.setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		this.setSituacao(saidaDTO.getDsSituacaoContrato());
		this.setMotivo(saidaDTO.getDsMotivoSituacao());
		this.setDescricaoContrato(saidaDTO.getDsContrato());
		this.setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		this.setCdEmpresa(String.valueOf(saidaDTO.getCdPessoaJuridica()));
		this.setCdTipo(String.valueOf(saidaDTO.getCdTipoContrato()));
		this.setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		this.setGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());
		
		// limpa o combo motibo.
		limparContratoPendenteAssinatura();
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		
		return "ATIVAR";
	}
	
	/**
	 * Voltar contrato pendente assinatura.
	 *
	 * @return the string
	 */
	public String voltarContratoPendenteAssinatura(){
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Limpar contrato pendente assinatura.
	 */
	public void limparContratoPendenteAssinatura(){
		setMotivoAtivacaoCbo(null);
	}

	/**
	 * Avancar contrato pendente assinatura.
	 *
	 * @return the string
	 */
	public String avancarContratoPendenteAssinatura(){
		// checar hidden
		if(getHiddenObrigatoriedade().equals("T")){
			setMotivoAtivacao((String)motivoAtivacaoListaHash.get(getMotivoAtivacaoCbo()));
			
			return "AVANCAR";
		}else{
			return "";
		}
	}
	
	/**
	 * Confirmar contrato pendente assinatura.
	 *
	 * @return the string
	 */
	public String confirmarContratoPendenteAssinatura(){
		
		try {
			AtivarContratoPendAssinaturaEntradaDTO entradaDTO = new AtivarContratoPendAssinaturaEntradaDTO();
			
			entradaDTO.setCdMotivoSituacaoContrato(getMotivoAtivacaoCbo());
			entradaDTO.setCdPessoaJuridicaContrato(Long.parseLong(getCdEmpresa()));
			entradaDTO.setCdTipoContratoNegocio(Integer.parseInt(getCdTipo()));
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(getNumero()));
			
			AtivarContratoPendAssinaturaSaidaDTO saidaDTO = getAtivarContratoPendenteAssinaturaServiceImpl().ativarContratoPendAssinatura(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "pesqContratoPendenteAssinatura", BradescoViewExceptionActionType.ACTION, false);
			
			identificacaoClienteContratoBean.consultarContrato();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		
		return "CONFIRMAR";
	}

	/**
	 * Voltar confirmar contrato pendente assinatura.
	 *
	 * @return the string
	 */
	public String voltarConfirmarContratoPendenteAssinatura(){
		return "VOLTAR_CONFIRMAR";
	}
	
	/**
	 * Pesquisar.
	 *
	 * @return the string
	 */
	public String pesquisar() {
		// usado no dataScroller;
		return "ok";
	}
	
	/**
	 * Limpar pagina pesq.
	 *
	 * @param evt the evt
	 */
	public void limparPaginaPesq(ActionEvent evt) {
		
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		//carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		//** Apesar de n�o ter na tela de pesquisa o combo Situa��o do contrato, foi mantido a carga do mesmo
		// para n�o ter que mudar a pesquisa toda, feita na identifica��o do cliente.
		// O modulo de Registrar Assinatura de contrato tamb�m est� assim. 

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteAtivContratoPendenteAss");
		identificacaoClienteContratoBean.setPaginaRetorno("pesqContratoPendenteAssinatura");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		
		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();	
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

	}

	//////////////////////
	//GETTERS E SETTERS//
	////////////////////
	
	/**
	 * Get: atividadeEconomica.
	 *
	 * @return atividadeEconomica
	 */
	public String getAtividadeEconomica() {
		return atividadeEconomica;
	}
	
	/**
	 * Set: atividadeEconomica.
	 *
	 * @param atividadeEconomica the atividade economica
	 */
	public void setAtividadeEconomica(String atividadeEconomica) {
		this.atividadeEconomica = atividadeEconomica;
	}
	
	/**
	 * Get: cpfCnpjClienteMaster.
	 *
	 * @return cpfCnpjClienteMaster
	 */
	public String getCpfCnpjClienteMaster() {
		return cpfCnpjClienteMaster;
	}
	
	/**
	 * Set: cpfCnpjClienteMaster.
	 *
	 * @param cpfCnpjClienteMaster the cpf cnpj cliente master
	 */
	public void setCpfCnpjClienteMaster(String cpfCnpjClienteMaster) {
		this.cpfCnpjClienteMaster = cpfCnpjClienteMaster;
	}
	
	/**
	 * Get: nomeRazaoSocialClienteMaster.
	 *
	 * @return nomeRazaoSocialClienteMaster
	 */
	public String getNomeRazaoSocialClienteMaster() {
		return nomeRazaoSocialClienteMaster;
	}
	
	/**
	 * Set: nomeRazaoSocialClienteMaster.
	 *
	 * @param nomeRazaoSocialClienteMaster the nome razao social cliente master
	 */
	public void setNomeRazaoSocialClienteMaster(String nomeRazaoSocialClienteMaster) {
		this.nomeRazaoSocialClienteMaster = nomeRazaoSocialClienteMaster;
	}
	
	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}
	
	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	
	/**
	 * Get: grupoEconomico.
	 *
	 * @return grupoEconomico
	 */
	public String getGrupoEconomico() {
		return grupoEconomico;
	}
	
	/**
	 * Set: grupoEconomico.
	 *
	 * @param grupoEconomico the grupo economico
	 */
	public void setGrupoEconomico(String grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}
	
	/**
	 * Get: motivo.
	 *
	 * @return motivo
	 */
	public String getMotivo() {
		return motivo;
	}
	
	/**
	 * Set: motivo.
	 *
	 * @param motivo the motivo
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}
	
	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	/**
	 * Get: segmento.
	 *
	 * @return segmento
	 */
	public String getSegmento() {
		return segmento;
	}
	
	/**
	 * Set: segmento.
	 *
	 * @param segmento the segmento
	 */
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}
	
	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}
	
	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: subSegmento.
	 *
	 * @return subSegmento
	 */
	public String getSubSegmento() {
		return subSegmento;
	}
	
	/**
	 * Set: subSegmento.
	 *
	 * @param subSegmento the sub segmento
	 */
	public void setSubSegmento(String subSegmento) {
		this.subSegmento = subSegmento;
	}
	
	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}
	
	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	/**
	 * Get: motivoAtivacaoCbo.
	 *
	 * @return motivoAtivacaoCbo
	 */
	public Integer getMotivoAtivacaoCbo() {
		return motivoAtivacaoCbo;
	}
	
	/**
	 * Set: motivoAtivacaoCbo.
	 *
	 * @param motivoAtivacaoCbo the motivo ativacao cbo
	 */
	public void setMotivoAtivacaoCbo(Integer motivoAtivacaoCbo) {
		this.motivoAtivacaoCbo = motivoAtivacaoCbo;
	}
	
	/**
	 * Get: hiddenObrigatoriedade.
	 *
	 * @return hiddenObrigatoriedade
	 */
	public String getHiddenObrigatoriedade() {
		return hiddenObrigatoriedade;
	}
	
	/**
	 * Set: hiddenObrigatoriedade.
	 *
	 * @param hiddenObrigatoriedade the hidden obrigatoriedade
	 */
	public void setHiddenObrigatoriedade(String hiddenObrigatoriedade) {
		this.hiddenObrigatoriedade = hiddenObrigatoriedade;
	}
	
	
	/**
	 * Get: dataHoraCadastramentoAditivo.
	 *
	 * @return dataHoraCadastramentoAditivo
	 */
	public Date getDataHoraCadastramentoAditivo() {
		return dataHoraCadastramentoAditivo;
	}
	
	/**
	 * Set: dataHoraCadastramentoAditivo.
	 *
	 * @param dataHoraCadastramentoAditivo the data hora cadastramento aditivo
	 */
	public void setDataHoraCadastramentoAditivo(Date dataHoraCadastramentoAditivo) {
		this.dataHoraCadastramentoAditivo = dataHoraCadastramentoAditivo;
	}
	
	/**
	 * Get: numeroAditivo.
	 *
	 * @return numeroAditivo
	 */
	public String getNumeroAditivo() {
		return numeroAditivo;
	}
	
	/**
	 * Set: numeroAditivo.
	 *
	 * @param numeroAditivo the numero aditivo
	 */
	public void setNumeroAditivo(String numeroAditivo) {
		this.numeroAditivo = numeroAditivo;
	}
	
	/**
	 * Get: situacaoAditivo.
	 *
	 * @return situacaoAditivo
	 */
	public String getSituacaoAditivo() {
		return situacaoAditivo;
	}
	
	/**
	 * Set: situacaoAditivo.
	 *
	 * @param situacaoAditivo the situacao aditivo
	 */
	public void setSituacaoAditivo(String situacaoAditivo) {
		this.situacaoAditivo = situacaoAditivo;
	}
	
	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}
	
	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: motivoAtivacaoLista.
	 *
	 * @return motivoAtivacaoLista
	 */
	public List<SelectItem> getMotivoAtivacaoLista() {
		return motivoAtivacaoLista;
	}

	/**
	 * Set: motivoAtivacaoLista.
	 *
	 * @param motivoAtivacaoLista the motivo ativacao lista
	 */
	public void setMotivoAtivacaoLista(List<SelectItem> motivoAtivacaoLista) {
		this.motivoAtivacaoLista = motivoAtivacaoLista;
	}

	/**
	 * Get: motivoAtivacao.
	 *
	 * @return motivoAtivacao
	 */
	public String getMotivoAtivacao() {
		return motivoAtivacao;
	}

	/**
	 * Set: motivoAtivacao.
	 *
	 * @param motivoAtivacao the motivo ativacao
	 */
	public void setMotivoAtivacao(String motivoAtivacao) {
		this.motivoAtivacao = motivoAtivacao;
	}
	
	/**
	 * Carrega lista motivo.
	 */
	public void carregaListaMotivo(){
		
		this.motivoAtivacaoLista = new ArrayList<SelectItem>();
		
		List<ListarMotivoSituacaoContratoSaidaDTO> saidaDTO = new ArrayList<ListarMotivoSituacaoContratoSaidaDTO>();
		ListarMotivoSituacaoContratoEntradaDTO entradaDTO = new ListarMotivoSituacaoContratoEntradaDTO();
		entradaDTO.setCdSituacao(3);
		saidaDTO = comboService.listarMotivoSituacaoContrato(entradaDTO);
		motivoAtivacaoListaHash.clear();
		
		for(ListarMotivoSituacaoContratoSaidaDTO combo : saidaDTO){
			motivoAtivacaoListaHash.put(combo.getCdMotivoSituacaoContrato(), combo.getDsMotivoSituacaoContrato());
			this.motivoAtivacaoLista.add(new SelectItem(combo.getCdMotivoSituacaoContrato(), combo.getDsMotivoSituacaoContrato()));
		}	
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: motivoAtivacaoListaHash.
	 *
	 * @return motivoAtivacaoListaHash
	 */
	public Map<Integer, String> getMotivoAtivacaoListaHash() {
		return motivoAtivacaoListaHash;
	}

	/**
	 * Set motivo ativacao lista hash.
	 *
	 * @param motivoAtivacaoListaHash the motivo ativacao lista hash
	 */
	public void setMotivoAtivacaoListaHash(
			Map<Integer, String> motivoAtivacaoListaHash) {
		this.motivoAtivacaoListaHash = motivoAtivacaoListaHash;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public String getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(String cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Get: cdTipo.
	 *
	 * @return cdTipo
	 */
	public String getCdTipo() {
		return cdTipo;
	}

	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(String cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Get: gerenteResponsavel.
	 *
	 * @return gerenteResponsavel
	 */
	public String getGerenteResponsavel() {
		return gerenteResponsavel;
	}

	/**
	 * Set: gerenteResponsavel.
	 *
	 * @param gerenteResponsavel the gerente responsavel
	 */
	public void setGerenteResponsavel(String gerenteResponsavel) {
		this.gerenteResponsavel = gerenteResponsavel;
	}
	
}
