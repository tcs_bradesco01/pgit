/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manassocmsglayoutmsgsist
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manassocmsglayoutmsgsist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultaMensagemEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultaMensagemSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.IdiomaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarRecursoCanalMsgSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.IManAssocMsgLayoutMsgSistService;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ConsultarVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ConsultarVinculacaoMsgLayoutSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.DetalharVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.DetalharVinculacaoMsgLayoutSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ExcluirVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.ExcluirVinculacaoMsgLayoutSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.IncluirVinculacaoMsgLayoutSistemaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manassocmsglayoutmsgsist.bean.IncluirVinculacaoMsgLayoutSistemaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.IManterMensagemLayoutService;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.DetalharMsgLayoutArqRetornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantermensagemlayout.bean.DetalharMsgLayoutArqRetornoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ManAssocMsgLayoutMsgSistBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManAssocMsgLayoutMsgSistBean {
	// NAVEGA��O
	/** Atributo TELA_CONSULTAR. */
	private static final String TELA_CONSULTAR = "TELA_CONSULTAR";

	/** Atributo TELA_INCLUIR. */
	private static final String TELA_INCLUIR = "TELA_INCLUIR";

	/** Atributo TELA_INCLUIR_AVANCAR. */
	private static final String TELA_INCLUIR_AVANCAR = "TELA_INCLUIR_AVANCAR";

	/** Atributo TELA_EXCLUIR. */
	private static final String TELA_EXCLUIR = "TELA_EXCLUIR";

	/** Atributo TELA_DETALHAR. */
	private static final String TELA_DETALHAR = "TELA_DETALHAR";

	// IMPL
	/** Atributo manAssocMsgLayoutMsgSistServiceImpl. */
	private IManAssocMsgLayoutMsgSistService manAssocMsgLayoutMsgSistServiceImpl;
	
	/** Atributo manterMensagemLayoutImpl. */
	private IManterMensagemLayoutService manterMensagemLayoutImpl;

	/** Atributo comboServiceImpl. */
	private IComboService comboServiceImpl;

	// COMBOS
	/** Atributo listaCentroCustoFiltro. */
	private List<SelectItem> listaCentroCustoFiltro = new ArrayList<SelectItem>();

	/** Atributo listaCentroCustoFiltroHash. */
	private Map<String, String> listaCentroCustoFiltroHash = new HashMap<String, String>();

	/** Atributo listaCentroCusto. */
	private List<SelectItem> listaCentroCusto = new ArrayList<SelectItem>();

	/** Atributo listaCentroCustoHash. */
	private Map<String, String> listaCentroCustoHash = new HashMap<String, String>();

	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();

	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer, String> listaTipoLayoutArquivoHash = new HashMap<Integer, String>();

	/** Atributo listaRecurso. */
	private List<SelectItem> listaRecurso = new ArrayList<SelectItem>();

	/** Atributo listaRecursoHash. */
	private Map<Integer, String> listaRecursoHash = new HashMap<Integer, String>();

	/** Atributo listaIdioma. */
	private List<SelectItem> listaIdioma = new ArrayList<SelectItem>();

	/** Atributo listaIdiomaHash. */
	private Map<Integer, String> listaIdiomaHash = new HashMap<Integer, String>();

	// CONSULTAR
	/** Atributo listaGridConsultar. */
	private List<ConsultarVinculacaoMsgLayoutSistemaSaidaDTO> listaGridConsultar;

	/** Atributo listaControleConsultar. */
	private List<SelectItem> listaControleConsultar;

	/** Atributo itemSelecionadoConsultar. */
	private Integer itemSelecionadoConsultar;

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;

	/** Atributo radioTipoConsultaFiltro. */
	private String radioTipoConsultaFiltro;

	/** Atributo cboTipoLayoutArquivoFiltro. */
	private Integer cboTipoLayoutArquivoFiltro;

	/** Atributo codigoMensagemFiltro. */
	private String codigoMensagemFiltro;

	/** Atributo cboCentroCustoFiltro. */
	private String cboCentroCustoFiltro;

	/** Atributo centroCustoFiltro. */
	private String centroCustoFiltro;

	/** Atributo cboRecursoFiltro. */
	private Integer cboRecursoFiltro;

	/** Atributo cboIdiomaFiltro. */
	private Integer cboIdiomaFiltro;

	/** Atributo codigoEventoFiltro. */
	private Integer codigoEventoFiltro;

	// DETALHAR / INCLUIR / EXCLUIR
	/** Atributo cboTipoLayoutArquivo. */
	private Integer cboTipoLayoutArquivo;

	/** Atributo tipoLayoutArquivo. */
	private String tipoLayoutArquivo;

	/** Atributo codigoMensagem. */
	private String codigoMensagem;

	/** Atributo descricaoMensagem. */
	private String descricaoMensagem;

	/** Atributo cboCentroCusto. */
	private String cboCentroCusto;

	/** Atributo centroCusto. */
	private String centroCusto;

	/** Atributo centroCustoDescricao. */
	private String centroCustoDescricao;

	/** Atributo cboRecurso. */
	private Integer cboRecurso;

	/** Atributo recurso. */
	private String recurso;

	/** Atributo cboIdioma. */
	private Integer cboIdioma;

	/** Atributo idioma. */
	private String idioma;

	/** Atributo codigoEvento. */
	private Integer codigoEvento;

	/** Atributo descricaoEvento. */
	private String descricaoEvento;

	// TRILHA AUDITORIA
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo tipoMensagemDesc. */
	private String tipoMensagemDesc;

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		// limpa tudo
		limpar();

		// carrega combo
		listarTipoLayout();
		listarRecurso();
		listarIdioma();
	}

	// LIMPAR
	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar() {
		setRadioTipoConsultaFiltro(null);
		limparCamposConsulta();
		limparInformacaoConsulta();

		return "";
	}

	/**
	 * Limpar informacao consulta.
	 *
	 * @return the string
	 */
	public String limparInformacaoConsulta() {
		setListaGridConsultar(new ArrayList<ConsultarVinculacaoMsgLayoutSistemaSaidaDTO>());
		setListaControleConsultar(new ArrayList<SelectItem>());
		setItemSelecionadoConsultar(null);
		setDisableArgumentosConsulta(false);

		return "";
	}

	/**
	 * Limpar campos.
	 */
	private void limparCampos() {
		setCboTipoLayoutArquivo(null);
		setTipoLayoutArquivo(null);
		setCodigoMensagem(null);
		setCentroCusto(null);
		setCboCentroCusto(null);
		setCentroCustoDescricao(null);
		setListaCentroCusto(new ArrayList<SelectItem>());
		setCboRecurso(null);
		setRecurso(null);
		setCboIdioma(null);
		setIdioma(null);
		setCodigoEvento(null);
		setDescricaoMensagem(null);
		setDescricaoEvento(null);
	}

	/**
	 * Limpar campos consulta.
	 */
	public void limparCamposConsulta() {
		setCboTipoLayoutArquivoFiltro(null);
		setCodigoMensagemFiltro(null);
		setListaCentroCustoFiltro(new ArrayList<SelectItem>());
		setCentroCustoFiltro(null);
		setCboCentroCustoFiltro(null);
		setCboRecursoFiltro(null);
		setCboIdiomaFiltro(null);
		setCodigoEventoFiltro(null);
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt) {
		consultar();
	}

	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar() {
		limparInformacaoConsulta();
		try {
			ConsultarVinculacaoMsgLayoutSistemaEntradaDTO entrada = new ConsultarVinculacaoMsgLayoutSistemaEntradaDTO();

			entrada.setCdTipoLayoutArquivo(getCboTipoLayoutArquivoFiltro());
			entrada.setCdMensagemArquivoRetorno(getCodigoMensagemFiltro());
			entrada.setCdSistema(getCboCentroCustoFiltro());
			entrada.setNrEventoMensagemNegocio(getCodigoEventoFiltro());
			entrada.setCdRecursoGeradorMensagem(getCboRecursoFiltro());
			entrada.setCdIdiomaTextoMensagem(getCboIdiomaFiltro());

			setListaGridConsultar(getManAssocMsgLayoutMsgSistServiceImpl().consultarVinculacaoMsgLayoutSistema(entrada));
			setListaControleConsultar(new ArrayList<SelectItem>());
			for (int i = 0; i < getListaGridConsultar().size(); i++) {
				getListaControleConsultar().add(new SelectItem(i, ""));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}

		return "";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		try {
			preencheDados();
			return TELA_DETALHAR;
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		return "";
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		limparCampos();
		return TELA_INCLUIR;
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		ConsultaMensagemEntradaDTO mensagemEntrada = new ConsultaMensagemEntradaDTO();
		ConsultaMensagemSaidaDTO mensagemSaida = new ConsultaMensagemSaidaDTO();
		// Mensagem do Layout
		setTipoLayoutArquivo(PgitUtil.concatenarCampos(getCboTipoLayoutArquivo(), getListaTipoLayoutArquivoHash().get(getCboTipoLayoutArquivo())));

		// Mensagem do Sistema
		setCentroCustoDescricao(getListaCentroCustoHash().get(getCboCentroCusto()));
		setRecurso(PgitUtil.concatenarCampos(getCboRecurso(), getListaRecursoHash().get(getCboRecurso())));
		setIdioma(getListaIdiomaHash().get(getCboIdioma()));
		
		mensagemEntrada = new ConsultaMensagemEntradaDTO();
		mensagemSaida = new ConsultaMensagemSaidaDTO();

		mensagemEntrada.setCdSistema(getCboCentroCusto());
		mensagemEntrada.setCdIdiomaTextoMensagem(getCboIdioma());
		mensagemEntrada.setCdRecursoGeradorMensagem(getCboRecurso());
		mensagemEntrada.setNrEventoMensagemNegocio(getCodigoEvento());
		
		mensagemSaida = comboServiceImpl.consultarMensagem(mensagemEntrada);
		setTipoMensagemDesc(mensagemSaida.getDsEventoMensagemNegocio());

		return TELA_INCLUIR_AVANCAR;
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {
		try {
			IncluirVinculacaoMsgLayoutSistemaEntradaDTO entrada = new IncluirVinculacaoMsgLayoutSistemaEntradaDTO();

			entrada.setCdTipoLayoutArquivo(getCboTipoLayoutArquivo());
			entrada.setCdSistema(getCboCentroCusto());
			entrada.setCdMensagemArquivoRetorno(getCodigoMensagem());
			entrada.setCdRecursoGeradorMensagem(getCboRecurso());
			entrada.setNrEventoMensagemNegocio(getCodigoEvento());
			entrada.setCdIdiomaTextoMensagem(getCboIdioma());

			IncluirVinculacaoMsgLayoutSistemaSaidaDTO saida = getManAssocMsgLayoutMsgSistServiceImpl()
							.incluirVinculacaoMsgLayoutSistema(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
							"conManAssocMsgLayoutMsgSist", BradescoViewExceptionActionType.ACTION, false);

			if (getListaGridConsultar() != null && !getListaGridConsultar().isEmpty()) {
				consultar();
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		return "";
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		try {
			preencheDados();
			return TELA_EXCLUIR;
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		return "";
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {
		try {
			ExcluirVinculacaoMsgLayoutSistemaEntradaDTO entrada = new ExcluirVinculacaoMsgLayoutSistemaEntradaDTO();
			ConsultarVinculacaoMsgLayoutSistemaSaidaDTO registroSelecionado = getListaGridConsultar().get(
							getItemSelecionadoConsultar());

			entrada.setCdTipoLayoutArquivo(registroSelecionado.getCdTipoLayoutArquivo());
			entrada.setNrMensagemArquivoRetorno(registroSelecionado.getNrMensagemArquivoRetorno());
			entrada.setCdSistema(registroSelecionado.getCdSistema());
			entrada.setNrEventoMensagemNegocio(registroSelecionado.getNrEventoMensagemNegocio());
			entrada.setCdRecursoGeradorMensagem(registroSelecionado.getCdRecursoGeradorMensagem());
			entrada.setCdIdiomaTextoMensagem(registroSelecionado.getCdIdiomaTextoMensagem());

			ExcluirVinculacaoMsgLayoutSistemaSaidaDTO saida = getManAssocMsgLayoutMsgSistServiceImpl()
							.excluirVinculacaoMsgLayoutSistema(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem() + ") " + saida.getMensagem(),
							"conManAssocMsgLayoutMsgSist", BradescoViewExceptionActionType.ACTION, false);
			consultar();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		return "";
	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados() {
		DetalharVinculacaoMsgLayoutSistemaEntradaDTO entrada = new DetalharVinculacaoMsgLayoutSistemaEntradaDTO();
		ConsultarVinculacaoMsgLayoutSistemaSaidaDTO registroSelecionado = getListaGridConsultar().get(
						getItemSelecionadoConsultar());

		entrada.setCdTipoLayoutArquivo(registroSelecionado.getCdTipoLayoutArquivo());
		entrada.setNrMensagemArquivoRetorno(registroSelecionado.getNrMensagemArquivoRetorno());
		entrada.setCdMensagemArquivoRetorno(registroSelecionado.getCdMensagemArquivoRetorno());
		entrada.setCdSistema(registroSelecionado.getCdSistema());
		entrada.setNrEventoMensagemNegocio(registroSelecionado.getNrEventoMensagemNegocio());
		entrada.setCdRecursoGeradorMensagem(registroSelecionado.getCdRecursoGeradorMensagem());
		entrada.setCdIdiomaTextoMensagem(registroSelecionado.getCdIdiomaTextoMensagem());

		DetalharVinculacaoMsgLayoutSistemaSaidaDTO saida = getManAssocMsgLayoutMsgSistServiceImpl()
						.detalharVinculacaoMsgLayoutSistema(entrada);

		// MENSAGEM DO LAYOUT
		setTipoLayoutArquivo(registroSelecionado.getTipoLayoutArquivoFormatado());
		setDescricaoMensagem(registroSelecionado.getDsMensagemLayoutRetorno());
		setCodigoMensagem(registroSelecionado.getCdMensagemArquivoRetorno());

		// MENSAGEM DO SISTEMA
		setCentroCustoDescricao(registroSelecionado.getCentroCustoFormatado());
		setRecurso(registroSelecionado.getRecursoFormatado());
		setIdioma(registroSelecionado.getDsIdiomaTextoMensagem());
		setCodigoEvento(registroSelecionado.getNrEventoMensagemNegocio());
		setDescricaoEvento(registroSelecionado.getDsEventoMensagemNegocio());

		// TRILHA AUDITORIA
		setDataHoraInclusao(saida.getHrInclusaoRegistro());
		setUsuarioInclusao(saida.getCdAutenticacaoSegurancaInclusao());
		setTipoCanalInclusao(saida.getCanalInclusaoFormatado());
		setComplementoInclusao((String) PgitUtil.verificaZero(saida.getNmOperacaoFluxoInclusao()));

		setDataHoraManutencao(saida.getHrManutencaoRegistro());
		setUsuarioManutencao(saida.getCdAutenticacaoSegurancaManutencao());
		setTipoCanalManutencao(saida.getCanalManutencaoFormatado());
		setComplementoManutencao((String) PgitUtil.verificaZero(saida.getNmOperacaoFluxoManutencao()));
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		return TELA_INCLUIR;
	}

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {
		return TELA_CONSULTAR;
	}

	// COMBOS
	/**
	 * Obter centro custo filtro.
	 */
	public void obterCentroCustoFiltro() {
		try {
			setCboCentroCustoFiltro(null);

			setListaCentroCustoFiltro(new ArrayList<SelectItem>());
			setListaCentroCustoFiltroHash(new HashMap<String, String>());

			CentroCustoEntradaDTO entrada = new CentroCustoEntradaDTO();
			entrada.setCdSistema(PgitUtil.verificaStringNula(getCentroCustoFiltro()).toUpperCase());

			List<CentroCustoSaidaDTO> lista = new ArrayList<CentroCustoSaidaDTO>();
			lista = getComboServiceImpl().listarCentroCusto(entrada);

			for (CentroCustoSaidaDTO combo : lista) {
				getListaCentroCustoFiltro().add(
								new SelectItem(combo.getCdCentroCusto(), PgitUtil.concatenarCampos(combo
												.getCdCentroCusto(), combo.getDsCentroCusto(), "-")));
				getListaCentroCustoFiltroHash().put(combo.getCdCentroCusto(),
								PgitUtil.concatenarCampos(combo.getCdCentroCusto(), combo.getDsCentroCusto(), "-"));
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaCentroCustoFiltro(new ArrayList<SelectItem>());
			setListaCentroCustoFiltroHash(new HashMap<String, String>());
		}
	}

	/**
	 * Obter centro custo.
	 */
	public void obterCentroCusto() {
		try {
			setCboCentroCusto(null);

			setListaCentroCusto(new ArrayList<SelectItem>());
			setListaCentroCustoHash(new HashMap<String, String>());

			CentroCustoEntradaDTO entrada = new CentroCustoEntradaDTO();
			entrada.setCdSistema(PgitUtil.verificaStringNula(getCentroCusto()).toUpperCase());

			List<CentroCustoSaidaDTO> lista = new ArrayList<CentroCustoSaidaDTO>();
			lista = getComboServiceImpl().listarCentroCusto(entrada);

			for (CentroCustoSaidaDTO combo : lista) {
				getListaCentroCusto().add(
								new SelectItem(combo.getCdCentroCusto(), PgitUtil.concatenarCampos(combo
												.getCdCentroCusto(), combo.getDsCentroCusto(), "-")));
				getListaCentroCustoHash().put(combo.getCdCentroCusto(),
								PgitUtil.concatenarCampos(combo.getCdCentroCusto(), combo.getDsCentroCusto(), "-"));
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaCentroCusto(new ArrayList<SelectItem>());
			setListaCentroCustoHash(new HashMap<String, String>());
		}
	}

	/**
	 * Listar tipo layout.
	 */
	private void listarTipoLayout() {
		try {
			setListaTipoLayoutArquivo(new ArrayList<SelectItem>());
			setListaTipoLayoutArquivoHash(new HashMap<Integer, String>());

			TipoLayoutArquivoEntradaDTO entrada = new TipoLayoutArquivoEntradaDTO();
			entrada.setCdSituacaoVinculacaoConta(0);

			List<TipoLayoutArquivoSaidaDTO> lista = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			lista = getComboServiceImpl().listarTipoLayoutArquivoCem(entrada);

			for (TipoLayoutArquivoSaidaDTO combo : lista) {
				getListaTipoLayoutArquivo().add(
								new SelectItem(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo()));
				getListaTipoLayoutArquivoHash().put(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaTipoLayoutArquivo(new ArrayList<SelectItem>());
			setListaTipoLayoutArquivoHash(new HashMap<Integer, String>());
		}
	}

	/**
	 * Listar recurso.
	 */
	private void listarRecurso() {
		try {
			setListaRecurso(new ArrayList<SelectItem>());
			setListaRecursoHash(new HashMap<Integer, String>());

			List<ListarRecursoCanalMsgSaidaDTO> lista = new ArrayList<ListarRecursoCanalMsgSaidaDTO>();
			lista = getComboServiceImpl().listarRecursoCanalMsg();

			for (ListarRecursoCanalMsgSaidaDTO combo : lista) {
				getListaRecurso().add(new SelectItem(combo.getCdTipoCanal(), combo.getDsTipoCanal()));
				getListaRecursoHash().put(combo.getCdTipoCanal(), combo.getDsTipoCanal());
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaRecurso(new ArrayList<SelectItem>());
			setListaRecursoHash(new HashMap<Integer, String>());
		}
	}

	/**
	 * Listar idioma.
	 */
	private void listarIdioma() {
		try {
			setListaIdioma(new ArrayList<SelectItem>());
			setListaIdiomaHash(new HashMap<Integer, String>());

			List<IdiomaSaidaDTO> lista = new ArrayList<IdiomaSaidaDTO>();
			lista = getComboServiceImpl().listarIdioma();

			for (IdiomaSaidaDTO combo : lista) {
				getListaIdioma().add(new SelectItem(combo.getCdIdioma(), combo.getDsIdioma()));
				getListaIdiomaHash().put(combo.getCdIdioma(), combo.getDsIdioma());
			}
		} catch (PdcAdapterFunctionalException e) {
			setListaIdioma(new ArrayList<SelectItem>());
			setListaIdiomaHash(new HashMap<Integer, String>());
		}
	}

	// GETTERS E SETTERS

	/**
	 * Get: codigoEventoFiltro.
	 *
	 * @return codigoEventoFiltro
	 */
	public Integer getCodigoEventoFiltro() {
		return codigoEventoFiltro;
	}

	/**
	 * Set: codigoEventoFiltro.
	 *
	 * @param codigoEventoFiltro the codigo evento filtro
	 */
	public void setCodigoEventoFiltro(Integer codigoEventoFiltro) {
		this.codigoEventoFiltro = codigoEventoFiltro;
	}

	/**
	 * Get: codigoMensagemFiltro.
	 *
	 * @return codigoMensagemFiltro
	 */
	public String getCodigoMensagemFiltro() {
		return codigoMensagemFiltro;
	}

	/**
	 * Set: codigoMensagemFiltro.
	 *
	 * @param codigoMensagemFiltro the codigo mensagem filtro
	 */
	public void setCodigoMensagemFiltro(String codigoMensagemFiltro) {
		this.codigoMensagemFiltro = codigoMensagemFiltro;
	}

	/**
	 * Get: comboServiceImpl.
	 *
	 * @return comboServiceImpl
	 */
	public IComboService getComboServiceImpl() {
		return comboServiceImpl;
	}

	/**
	 * Set: comboServiceImpl.
	 *
	 * @param comboServiceImpl the combo service impl
	 */
	public void setComboServiceImpl(IComboService comboServiceImpl) {
		this.comboServiceImpl = comboServiceImpl;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: itemSelecionadoConsultar.
	 *
	 * @return itemSelecionadoConsultar
	 */
	public Integer getItemSelecionadoConsultar() {
		return itemSelecionadoConsultar;
	}

	/**
	 * Set: itemSelecionadoConsultar.
	 *
	 * @param itemSelecionadoConsultar the item selecionado consultar
	 */
	public void setItemSelecionadoConsultar(Integer itemSelecionadoConsultar) {
		this.itemSelecionadoConsultar = itemSelecionadoConsultar;
	}

	/**
	 * Get: listaControleConsultar.
	 *
	 * @return listaControleConsultar
	 */
	public List<SelectItem> getListaControleConsultar() {
		return listaControleConsultar;
	}

	/**
	 * Set: listaControleConsultar.
	 *
	 * @param listaControleConsultar the lista controle consultar
	 */
	public void setListaControleConsultar(List<SelectItem> listaControleConsultar) {
		this.listaControleConsultar = listaControleConsultar;
	}

	/**
	 * Get: listaGridConsultar.
	 *
	 * @return listaGridConsultar
	 */
	public List<ConsultarVinculacaoMsgLayoutSistemaSaidaDTO> getListaGridConsultar() {
		return listaGridConsultar;
	}

	/**
	 * Set: listaGridConsultar.
	 *
	 * @param listaGridConsultar the lista grid consultar
	 */
	public void setListaGridConsultar(List<ConsultarVinculacaoMsgLayoutSistemaSaidaDTO> listaGridConsultar) {
		this.listaGridConsultar = listaGridConsultar;
	}

	/**
	 * Get: listaIdioma.
	 *
	 * @return listaIdioma
	 */
	public List<SelectItem> getListaIdioma() {
		return listaIdioma;
	}

	/**
	 * Set: listaIdioma.
	 *
	 * @param listaIdioma the lista idioma
	 */
	public void setListaIdioma(List<SelectItem> listaIdioma) {
		this.listaIdioma = listaIdioma;
	}

	/**
	 * Get: listaIdiomaHash.
	 *
	 * @return listaIdiomaHash
	 */
	public Map<Integer, String> getListaIdiomaHash() {
		return listaIdiomaHash;
	}

	/**
	 * Set lista idioma hash.
	 *
	 * @param listaIdiomaHash the lista idioma hash
	 */
	public void setListaIdiomaHash(Map<Integer, String> listaIdiomaHash) {
		this.listaIdiomaHash = listaIdiomaHash;
	}

	/**
	 * Get: listaRecurso.
	 *
	 * @return listaRecurso
	 */
	public List<SelectItem> getListaRecurso() {
		return listaRecurso;
	}

	/**
	 * Set: listaRecurso.
	 *
	 * @param listaRecurso the lista recurso
	 */
	public void setListaRecurso(List<SelectItem> listaRecurso) {
		this.listaRecurso = listaRecurso;
	}

	/**
	 * Get: listaRecursoHash.
	 *
	 * @return listaRecursoHash
	 */
	public Map<Integer, String> getListaRecursoHash() {
		return listaRecursoHash;
	}

	/**
	 * Set lista recurso hash.
	 *
	 * @param listaRecursoHash the lista recurso hash
	 */
	public void setListaRecursoHash(Map<Integer, String> listaRecursoHash) {
		this.listaRecursoHash = listaRecursoHash;
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 *
	 * @return listaTipoLayoutArquivo
	 */
	public List<SelectItem> getListaTipoLayoutArquivo() {
		return listaTipoLayoutArquivo;
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 *
	 * @param listaTipoLayoutArquivo the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	/**
	 * Get: listaTipoLayoutArquivoHash.
	 *
	 * @return listaTipoLayoutArquivoHash
	 */
	public Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}

	/**
	 * Set lista tipo layout arquivo hash.
	 *
	 * @param listaTipoLayoutArquivoHash the lista tipo layout arquivo hash
	 */
	public void setListaTipoLayoutArquivoHash(Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}

	/**
	 * Get: manAssocMsgLayoutMsgSistServiceImpl.
	 *
	 * @return manAssocMsgLayoutMsgSistServiceImpl
	 */
	public IManAssocMsgLayoutMsgSistService getManAssocMsgLayoutMsgSistServiceImpl() {
		return manAssocMsgLayoutMsgSistServiceImpl;
	}

	/**
	 * Set: manAssocMsgLayoutMsgSistServiceImpl.
	 *
	 * @param manAssocMsgLayoutMsgSistServiceImpl the man assoc msg layout msg sist service impl
	 */
	public void setManAssocMsgLayoutMsgSistServiceImpl(
					IManAssocMsgLayoutMsgSistService manAssocMsgLayoutMsgSistServiceImpl) {
		this.manAssocMsgLayoutMsgSistServiceImpl = manAssocMsgLayoutMsgSistServiceImpl;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: cboIdioma.
	 *
	 * @return cboIdioma
	 */
	public Integer getCboIdioma() {
		return cboIdioma;
	}

	/**
	 * Set: cboIdioma.
	 *
	 * @param cboIdioma the cbo idioma
	 */
	public void setCboIdioma(Integer cboIdioma) {
		this.cboIdioma = cboIdioma;
	}

	/**
	 * Get: cboRecurso.
	 *
	 * @return cboRecurso
	 */
	public Integer getCboRecurso() {
		return cboRecurso;
	}

	/**
	 * Set: cboRecurso.
	 *
	 * @param cboRecurso the cbo recurso
	 */
	public void setCboRecurso(Integer cboRecurso) {
		this.cboRecurso = cboRecurso;
	}

	/**
	 * Get: cboTipoLayoutArquivo.
	 *
	 * @return cboTipoLayoutArquivo
	 */
	public Integer getCboTipoLayoutArquivo() {
		return cboTipoLayoutArquivo;
	}

	/**
	 * Set: cboTipoLayoutArquivo.
	 *
	 * @param cboTipoLayoutArquivo the cbo tipo layout arquivo
	 */
	public void setCboTipoLayoutArquivo(Integer cboTipoLayoutArquivo) {
		this.cboTipoLayoutArquivo = cboTipoLayoutArquivo;
	}

	/**
	 * Get: codigoEvento.
	 *
	 * @return codigoEvento
	 */
	public Integer getCodigoEvento() {
		return codigoEvento;
	}

	/**
	 * Set: codigoEvento.
	 *
	 * @param codigoEvento the codigo evento
	 */
	public void setCodigoEvento(Integer codigoEvento) {
		this.codigoEvento = codigoEvento;
	}

	/**
	 * Get: codigoMensagem.
	 *
	 * @return codigoMensagem
	 */
	public String getCodigoMensagem() {
		return codigoMensagem;
	}

	/**
	 * Set: codigoMensagem.
	 *
	 * @param codigoMensagem the codigo mensagem
	 */
	public void setCodigoMensagem(String codigoMensagem) {
		this.codigoMensagem = codigoMensagem;
	}

	/**
	 * Get: idioma.
	 *
	 * @return idioma
	 */
	public String getIdioma() {
		return idioma;
	}

	/**
	 * Set: idioma.
	 *
	 * @param idioma the idioma
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	/**
	 * Get: recurso.
	 *
	 * @return recurso
	 */
	public String getRecurso() {
		return recurso;
	}

	/**
	 * Set: recurso.
	 *
	 * @param recurso the recurso
	 */
	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public String getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}

	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(String tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}

	/**
	 * Get: cboIdiomaFiltro.
	 *
	 * @return cboIdiomaFiltro
	 */
	public Integer getCboIdiomaFiltro() {
		return cboIdiomaFiltro;
	}

	/**
	 * Set: cboIdiomaFiltro.
	 *
	 * @param cboIdiomaFiltro the cbo idioma filtro
	 */
	public void setCboIdiomaFiltro(Integer cboIdiomaFiltro) {
		this.cboIdiomaFiltro = cboIdiomaFiltro;
	}

	/**
	 * Get: cboRecursoFiltro.
	 *
	 * @return cboRecursoFiltro
	 */
	public Integer getCboRecursoFiltro() {
		return cboRecursoFiltro;
	}

	/**
	 * Set: cboRecursoFiltro.
	 *
	 * @param cboRecursoFiltro the cbo recurso filtro
	 */
	public void setCboRecursoFiltro(Integer cboRecursoFiltro) {
		this.cboRecursoFiltro = cboRecursoFiltro;
	}

	/**
	 * Get: cboTipoLayoutArquivoFiltro.
	 *
	 * @return cboTipoLayoutArquivoFiltro
	 */
	public Integer getCboTipoLayoutArquivoFiltro() {
		return cboTipoLayoutArquivoFiltro;
	}

	/**
	 * Set: cboTipoLayoutArquivoFiltro.
	 *
	 * @param cboTipoLayoutArquivoFiltro the cbo tipo layout arquivo filtro
	 */
	public void setCboTipoLayoutArquivoFiltro(Integer cboTipoLayoutArquivoFiltro) {
		this.cboTipoLayoutArquivoFiltro = cboTipoLayoutArquivoFiltro;
	}

	/**
	 * Get: radioTipoConsultaFiltro.
	 *
	 * @return radioTipoConsultaFiltro
	 */
	public String getRadioTipoConsultaFiltro() {
		return radioTipoConsultaFiltro;
	}

	/**
	 * Set: radioTipoConsultaFiltro.
	 *
	 * @param radioTipoConsultaFiltro the radio tipo consulta filtro
	 */
	public void setRadioTipoConsultaFiltro(String radioTipoConsultaFiltro) {
		this.radioTipoConsultaFiltro = radioTipoConsultaFiltro;
	}

	/**
	 * Get: descricaoEvento.
	 *
	 * @return descricaoEvento
	 */
	public String getDescricaoEvento() {
		return descricaoEvento;
	}

	/**
	 * Set: descricaoEvento.
	 *
	 * @param descricaoEvento the descricao evento
	 */
	public void setDescricaoEvento(String descricaoEvento) {
		this.descricaoEvento = descricaoEvento;
	}

	/**
	 * Get: descricaoMensagem.
	 *
	 * @return descricaoMensagem
	 */
	public String getDescricaoMensagem() {
		return descricaoMensagem;
	}

	/**
	 * Set: descricaoMensagem.
	 *
	 * @param descricaoMensagem the descricao mensagem
	 */
	public void setDescricaoMensagem(String descricaoMensagem) {
		this.descricaoMensagem = descricaoMensagem;
	}

	/**
	 * Get: cboCentroCusto.
	 *
	 * @return cboCentroCusto
	 */
	public String getCboCentroCusto() {
		return cboCentroCusto;
	}

	/**
	 * Set: cboCentroCusto.
	 *
	 * @param cboCentroCusto the cbo centro custo
	 */
	public void setCboCentroCusto(String cboCentroCusto) {
		this.cboCentroCusto = cboCentroCusto;
	}

	/**
	 * Get: cboCentroCustoFiltro.
	 *
	 * @return cboCentroCustoFiltro
	 */
	public String getCboCentroCustoFiltro() {
		return cboCentroCustoFiltro;
	}

	/**
	 * Set: cboCentroCustoFiltro.
	 *
	 * @param cboCentroCustoFiltro the cbo centro custo filtro
	 */
	public void setCboCentroCustoFiltro(String cboCentroCustoFiltro) {
		this.cboCentroCustoFiltro = cboCentroCustoFiltro;
	}

	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}

	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}

	/**
	 * Get: centroCustoDescricao.
	 *
	 * @return centroCustoDescricao
	 */
	public String getCentroCustoDescricao() {
		return centroCustoDescricao;
	}

	/**
	 * Set: centroCustoDescricao.
	 *
	 * @param centroCustoDescricao the centro custo descricao
	 */
	public void setCentroCustoDescricao(String centroCustoDescricao) {
		this.centroCustoDescricao = centroCustoDescricao;
	}

	/**
	 * Get: centroCustoFiltro.
	 *
	 * @return centroCustoFiltro
	 */
	public String getCentroCustoFiltro() {
		return centroCustoFiltro;
	}

	/**
	 * Set: centroCustoFiltro.
	 *
	 * @param centroCustoFiltro the centro custo filtro
	 */
	public void setCentroCustoFiltro(String centroCustoFiltro) {
		this.centroCustoFiltro = centroCustoFiltro;
	}

	/**
	 * Get: listaCentroCusto.
	 *
	 * @return listaCentroCusto
	 */
	public List<SelectItem> getListaCentroCusto() {
		return listaCentroCusto;
	}

	/**
	 * Set: listaCentroCusto.
	 *
	 * @param listaCentroCusto the lista centro custo
	 */
	public void setListaCentroCusto(List<SelectItem> listaCentroCusto) {
		this.listaCentroCusto = listaCentroCusto;
	}

	/**
	 * Get: listaCentroCustoFiltro.
	 *
	 * @return listaCentroCustoFiltro
	 */
	public List<SelectItem> getListaCentroCustoFiltro() {
		return listaCentroCustoFiltro;
	}

	/**
	 * Set: listaCentroCustoFiltro.
	 *
	 * @param listaCentroCustoFiltro the lista centro custo filtro
	 */
	public void setListaCentroCustoFiltro(List<SelectItem> listaCentroCustoFiltro) {
		this.listaCentroCustoFiltro = listaCentroCustoFiltro;
	}

	/**
	 * Get: listaCentroCustoFiltroHash.
	 *
	 * @return listaCentroCustoFiltroHash
	 */
	public Map<String, String> getListaCentroCustoFiltroHash() {
		return listaCentroCustoFiltroHash;
	}

	/**
	 * Set lista centro custo filtro hash.
	 *
	 * @param listaCentroCustoFiltroHash the lista centro custo filtro hash
	 */
	public void setListaCentroCustoFiltroHash(Map<String, String> listaCentroCustoFiltroHash) {
		this.listaCentroCustoFiltroHash = listaCentroCustoFiltroHash;
	}

	/**
	 * Get: listaCentroCustoHash.
	 *
	 * @return listaCentroCustoHash
	 */
	public Map<String, String> getListaCentroCustoHash() {
		return listaCentroCustoHash;
	}

	/**
	 * Set lista centro custo hash.
	 *
	 * @param listaCentroCustoHash the lista centro custo hash
	 */
	public void setListaCentroCustoHash(Map<String, String> listaCentroCustoHash) {
		this.listaCentroCustoHash = listaCentroCustoHash;
	}

	/**
	 * Get: tipoMensagemDesc.
	 *
	 * @return tipoMensagemDesc
	 */
	public String getTipoMensagemDesc() {
		return tipoMensagemDesc;
	}

	/**
	 * Set: tipoMensagemDesc.
	 *
	 * @param tipoMensagemDesc the tipo mensagem desc
	 */
	public void setTipoMensagemDesc(String tipoMensagemDesc) {
		this.tipoMensagemDesc = tipoMensagemDesc;
	}

	/**
	 * Get: manterMensagemLayoutImpl.
	 *
	 * @return manterMensagemLayoutImpl
	 */
	public IManterMensagemLayoutService getManterMensagemLayoutImpl() {
		return manterMensagemLayoutImpl;
	}

	/**
	 * Set: manterMensagemLayoutImpl.
	 *
	 * @param manterMensagemLayoutImpl the manter mensagem layout impl
	 */
	public void setManterMensagemLayoutImpl(IManterMensagemLayoutService manterMensagemLayoutImpl) {
		this.manterMensagemLayoutImpl = manterMensagemLayoutImpl;
	}
	
}