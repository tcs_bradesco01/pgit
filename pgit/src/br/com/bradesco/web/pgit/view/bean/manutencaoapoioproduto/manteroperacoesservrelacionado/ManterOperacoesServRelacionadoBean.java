/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manteroperacoesservrelacionado
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manteroperacoesservrelacionado;

import static br.com.bradesco.web.pgit.utils.SiteUtil.isNotNull;
import static br.com.bradesco.web.pgit.utils.SiteUtil.isNull;
import static br.com.bradesco.web.pgit.utils.SiteUtil.not;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOperacoesServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOperacoesServicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoRelacionadoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoRelacionadoPgitOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoRelacionadoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.IManterOperacoesServRelacionadoService;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.AlterarOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.AlterarOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ConsultarTarifaCatalogoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ConsultarTarifaCatalogoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharHistoricoOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.DetalharOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ExcluirOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ExcluirOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.IncluirOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.IncluirOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarHistoricoOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manteroperacoesservrelacionado.bean.ListarOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterOperacoesServRelacionadoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterOperacoesServRelacionadoBean {
	
	/** Atributo manterOperacoesServRelacionadoImpl. */
	private IManterOperacoesServRelacionadoService manterOperacoesServRelacionadoImpl;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;
	
	/** Atributo cdTipoServico. */
	private Integer cdTipoServico;
	
	/** Atributo listaTipoServicoFiltro. */
	private List<SelectItem> listaTipoServicoFiltro = new ArrayList<SelectItem>();	
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer,String> listaTipoServicoHash = new HashMap<Integer,String>();
	
	/** Atributo modalidadeServicoFiltro. */
	private Integer modalidadeServicoFiltro;
	
	/** Atributo cdModalidadeServico. */
	private Integer cdModalidadeServico;
	
	/** Atributo listaModalidadeFiltro. */
	private List<SelectItem> listaModalidadeFiltro = new ArrayList<SelectItem>();
	
	/** Atributo listarArgumentoPesquisaServicoModalidade. */
	private List<SelectItem> listarArgumentoPesquisaServicoModalidade =  new ArrayList<SelectItem>();
	
	/** Atributo listaModalidade. */
	private List<SelectItem> listaModalidade = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeHash. */
	private Map<Integer,String> listaModalidadeHash = new HashMap<Integer,String>();
	
	/** Atributo operacaoFiltro. */
	private Integer operacaoFiltro;
	
	/** Atributo cdOperacao. */
	private Integer cdOperacao;
	
	/** Atributo listaOperacaoFiltro. */
	private List<SelectItem> listaOperacaoFiltro = new ArrayList<SelectItem>();
	
	/** Atributo listaOperacao. */
	private List<SelectItem> listaOperacao = new ArrayList<SelectItem>();
	
	/** Atributo listaOperacaoHash. */
	private Map<Integer,String> listaOperacaoHash = new HashMap<Integer,String>();
	
	private List<ListarServicoRelacionadoPgitOcorrenciasSaidaDTO> listaServicoRelacionado = new ArrayList<ListarServicoRelacionadoPgitOcorrenciasSaidaDTO>();
	
	/** Atributo btoAcionado. */
	private boolean btoAcionado;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaGrid. */
	private List<ListarOperacaoServicoPagtoIntegradoSaidaDTO> listaGrid;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();	
	
	/** Atributo codigoOperacao. */
	private String codigoOperacao;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dsModalidadeServico. */
	private String dsModalidadeServico;
	
	/** Atributo dsOperacao. */
	private String dsOperacao;
	
	/** Atributo dsIndicadorManutencao. */
	private String dsIndicadorManutencao;
	
	/** Atributo dsNatureza. */
	private String dsNatureza;
	
	/** Atributo rdoTarifavelContabil. */
	private Integer rdoTarifavelContabil;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo rdoTarifaAlcadaAgencia. */
	private String rdoTarifaAlcadaAgencia;
	
	/** Atributo saidaTarifaCatalogo. */
	private ConsultarTarifaCatalogoSaidaDTO saidaTarifaCatalogo = null;
	
	/** Atributo rdoTarifaAlcadaAgencia. */
	private BigDecimal valorAlcadaTarifaAgencia;
	
	/** Atributo rdoTarifaAlcadaAgencia. */
	private BigDecimal percentualAlcadaTarifaAgencia;
	
	/** Atributo saidaDetalhe. */
	private DetalharOperacaoServicoPagtoIntegradoSaidaDTO saidaDetalhe = null;
	
	/** Atributo VALOR_CEM. */
	private final static BigDecimal VALOR_CEM = new BigDecimal(100);
	
	/** Atributo tipoServicoHistorico. */
    private Integer tipoServicoHistorico;
    
    /** Atributo listaTipoServicoHistorico. */
    private List<SelectItem> listaTipoServicoHistorico = new ArrayList<SelectItem>();  
    
    /** Atributo modalidadeServicoHistorico. */
    private Integer modalidadeServicoHistorico;
    
    /** Atributo listaModalidadeHistorico. */
    private List<SelectItem> listaModalidadeHistorico = new ArrayList<SelectItem>();
    
    /** Atributo operacaoHistorico. */
    private Integer operacaoHistorico;
    
    /** Atributo listaOperacaoHistorico. */
    private List<SelectItem> listaOperacaoHistorico = new ArrayList<SelectItem>();
	
	/**
	 * Atributo Date
	 */
	private Date dataManutencaoInicio;
	
	/**
	 * Atributo Date
	 */
	private Date dataManutencaoFim;
	
	/**
	 * Atributo List<ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO>
	 */
	private List<ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO> listaGridHistorico;
	
	/**
	 * Atributo Integer
	 */
	private Integer itemSelecionadoHistorico;
	
	/**
	 * Atributo List<SelectItem>
	 */
	private List<SelectItem> listaControleRadioHistorico = new ArrayList<SelectItem>();
	
	/**
	 * Atributo DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO
	 */
	private DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO saidaDetalheHistorico = null;
	
	//Metodos
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		//listarTipoServico();
		limparCampos();
		listarArgumentoPesquisaModalidade();
		
	}
	
	/**
	 * Carrega combo modalidade filtro.
	 */
	public void carregaComboModalidadeFiltro(){
		listarModalidadesFiltro();
		setOperacaoFiltro(0);
		listaOperacaoFiltro = new ArrayList<SelectItem>();
		listarOperacoesFiltro();
	}
	
	/**
	 * Nome: carregaComboModalidadeHistorico
	 * 
	 */
	public void carregaComboModalidadeHistorico(){
        listarModalidadesHistorico();
        setOperacaoHistorico(0);
        listaOperacaoHistorico = new ArrayList<SelectItem>();
        listarOperacoesHistorico();
    }
	
	/**
	 * Carrega combo operacao filtro.
	 */
	public void carregaComboOperacaoFiltro(){
		listarOperacoesFiltro();
	}
	
	/**
	 * Nome: carregaComboOperacaoHistorico
	 * 
	 */
	public void carregaComboOperacaoHistorico(){
        listarOperacoesHistorico();
    }
	
	/**
	 * Carrega combo modalidade.
	 */
	public void carregaComboModalidade(){
	    limparTarifaAlcada();
	    
		listarModalidades();
		setCdOperacao(0);
		listaOperacao = new ArrayList<SelectItem>();
		listarOperacoes();
	}
	
	/**
	 * Carrega combo operacao.
	 */
	public void carregaComboOperacao(){
	    limparTarifaAlcada();
		listarOperacoes();
	}
	
	/**
	 * Nome: consultarTarifaCatalogo
	 * 
	 */
	public void consultarTarifaCatalogo(){
	    ConsultarTarifaCatalogoEntradaDTO entrada = new ConsultarTarifaCatalogoEntradaDTO();
	    entrada.setCdProdutoServicoOperacional(getCdTipoServico());
	    entrada.setCdProdutoOperacionalRelacionado(getCdModalidadeServico());
	    entrada.setCdOperacaoProdutoServico(getCdOperacao());
	    
	    try{
	        buscarTarifaCatalogo(entrada);
	        
	        if(not(validarTarifaMaximaNegociada())){
                setValorAlcadaTarifaAgencia(saidaTarifaCatalogo.getVrTarifaMaximaNegociada());
                setPercentualAlcadaTarifaAgencia(new BigDecimal("0"));
            }
	        
	    } catch (PdcAdapterFunctionalException p) {
	        limparTarifaAlcada();           
        }
	}

    /**
     * Nome: buscarTarifaCatalogo
     * 
     * @param entrada
     */
    private void buscarTarifaCatalogo(ConsultarTarifaCatalogoEntradaDTO entrada) {
        limparTarifaAlcada();
        
        saidaTarifaCatalogo = getManterOperacoesServRelacionadoImpl().consultarTarifaCatalogo(entrada);
    }

    /**
     * Nome: validarTarifaMaximaNegociada
     * 
     * @return
     */
    private boolean validarTarifaMaximaNegociada() {
        return saidaTarifaCatalogo.getVrTarifaMaximaNegociada().compareTo(new BigDecimal(0)) == 0;
    }

    /**
     * Nome: limparTarifaAlcada
     * 
     */
    private void limparTarifaAlcada() {
        setRdoTarifaAlcadaAgencia(null);
        limparValoresTarifaAlcada();
    }

    /**
     * Nome: limparValoresTarifaAlcada
     * 
     */
    public void limparValoresTarifaAlcada() {
        setValorAlcadaTarifaAgencia(null);
        setPercentualAlcadaTarifaAgencia(null);
    }
	
	/**
	 * Listar tipo servico.
	 */
	public void listarTipoServico(){
		try{
			listaTipoServicoHash.clear();
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
			
			ListarServicosEntradaDTO entrada = new ListarServicosEntradaDTO();
			entrada.setCdNaturezaServico(0);
			entrada.setNumeroOcorrencias(50);
			
			List<ListarServicosSaidaDTO> list = comboService.listarTipoServico(entrada);
			
			for (ListarServicosSaidaDTO saida : list){
				listaTipoServicoFiltro.add(new SelectItem(saida.getCdServico(), saida.getDsServico()));
				listaTipoServicoHash.put(saida.getCdServico(), saida.getDsServico());
			}
			
		} catch (PdcAdapterFunctionalException p) {
			listaTipoServicoFiltro = new ArrayList<SelectItem>();			
		}			
	}	
	
	/**
	 * Listar modalidades filtro.
	 */
	public void listarModalidadesFiltro(){
		try{
			setModalidadeServicoFiltro(0);
			if (getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0){ 
				listaModalidadeFiltro = new ArrayList<SelectItem>();
				ListarModalidadesEntradaDTO entrada = new ListarModalidadesEntradaDTO();
				entrada.setCdServico(getTipoServicoFiltro());
				entrada.setNumeroOcorrencias(50);
				
				List<ListarModalidadeSaidaDTO> list = listarModalidades(entrada);
				
				for (ListarModalidadeSaidaDTO saida : list){
					listaModalidadeFiltro.add(new SelectItem(saida.getCdModalidade(), saida.getDsModalidade()));
				}
			}else{
				listaModalidadeFiltro = new ArrayList<SelectItem>();
			}
		}catch(PdcAdapterFunctionalException p){
			listaModalidadeFiltro = new ArrayList<SelectItem>();
			setModalidadeServicoFiltro(0);
		}
	}
	
	/**
	 * Nome: listarModalidadesHistorico
	 * 
	 */
	public void listarModalidadesHistorico(){
        try{
            setModalidadeServicoHistorico(0);
            if (getTipoServicoHistorico() != null && getTipoServicoHistorico() != 0){ 
                listaModalidadeHistorico = new ArrayList<SelectItem>();
                ListarModalidadesEntradaDTO entrada = new ListarModalidadesEntradaDTO();
                entrada.setCdServico(getTipoServicoHistorico());
                entrada.setNumeroOcorrencias(50);
                
                List<ListarModalidadeSaidaDTO> list = listarModalidades(entrada);
                
                for (ListarModalidadeSaidaDTO saida : list){
                    listaModalidadeHistorico.add(new SelectItem(saida.getCdModalidade(), saida.getDsModalidade()));
                }
            }else{
                listaModalidadeHistorico = new ArrayList<SelectItem>();
            }
        }catch(PdcAdapterFunctionalException p){
            listaModalidadeHistorico = new ArrayList<SelectItem>();
            setModalidadeServicoHistorico(0);
        }
    }

    /**
     * Nome: listarModalidades
     * 
     * @param entrada
     * @return
     */
    private List<ListarModalidadeSaidaDTO> listarModalidades(ListarModalidadesEntradaDTO entrada) {
        return comboService.listarModalidadesEntrada(entrada);
    }
	 
	/**
	 * Nome: listarArgumentoPesquisaModalidade
	 * 
	 */
	public void listarArgumentoPesquisaModalidade() {
		try {
			setModalidadeServicoFiltro(0);
			
			ListarServicoRelacionadoPgitEntradaDTO entrada = new ListarServicoRelacionadoPgitEntradaDTO();
			entrada.setCdNaturezaServico(0);
			entrada.setMaxOcorrencias(50);
				
			ListarServicoRelacionadoPgitSaidaDTO saida = comboService.listarServicoRelacionadoPgit(entrada);			
			
			listaServicoRelacionado = saida.getOcorrencias();
				
			setListarArgumentoPesquisaServicoModalidade(new ArrayList<SelectItem>());	
			
			for (int i = 0; i < saida.getOcorrencias().size(); i++) {
				listarArgumentoPesquisaServicoModalidade.add(new SelectItem(saida.getOcorrencias().get(i).getCdServico(), 
						saida.getOcorrencias().get(i).getDsServico()));
			}			
			
								
		} catch (PdcAdapterFunctionalException e) {
			listarArgumentoPesquisaServicoModalidade = new ArrayList<SelectItem>();
		}
	}
	
	/**
	 * Listar modalidades.
	 */
	public void listarModalidades(){
		try{
			setCdModalidadeServico(0);
			listaModalidadeHash.clear();
			if (getCdTipoServico() != null && getCdTipoServico() != 0){ 
				listaModalidade = new ArrayList<SelectItem>();
				ListarModalidadesEntradaDTO entrada = new ListarModalidadesEntradaDTO();
				entrada.setCdServico(getCdTipoServico());
				entrada.setNumeroOcorrencias(50);
				
				List<ListarModalidadeSaidaDTO> list = listarModalidades(entrada);
				
				for (ListarModalidadeSaidaDTO saida : list){
					listaModalidade.add(new SelectItem(saida.getCdModalidade(), saida.getDsModalidade()));
					listaModalidadeHash.put(saida.getCdModalidade(), saida.getDsModalidade());
					
				}
			}else{
				listaModalidade = new ArrayList<SelectItem>();
			}
		}catch(PdcAdapterFunctionalException p){
			listaModalidade = new ArrayList<SelectItem>();
			setCdModalidadeServico(0);
		}
	}
	
	/**
	 * Listar operacoes filtro.
	 */
	public void listarOperacoesFiltro(){
		try{
			setOperacaoFiltro(0);
			//if (getModalidadeServicoFiltro() != null && getModalidadeServicoFiltro() != 0){ 
				listaOperacaoFiltro = new ArrayList<SelectItem>();
				ListarOperacoesServicosEntradaDTO entrada = new ListarOperacoesServicosEntradaDTO();
				entrada.setCdProduto(getTipoServicoFiltro());
				
				List<ListarOperacoesServicoSaidaDTO> list = listarOperacoes(entrada);
				
				for (ListarOperacoesServicoSaidaDTO saida : list){
					listaOperacaoFiltro.add(new SelectItem(saida.getCdOperacao(), saida.getDsOperacao()));
				}
			//}else{
				//listaOperacaoFiltro = new ArrayList<SelectItem>();
			//}
			
		}catch(PdcAdapterFunctionalException p){
			listaOperacaoFiltro = new ArrayList<SelectItem>();
			setOperacaoFiltro(0);
		}
	}
	
	/**
	 * Nome: listarOperacoesHistorico
	 * 
	 */
    public void listarOperacoesHistorico() {
        try {
            setOperacaoHistorico(0);
            listaOperacaoHistorico = new ArrayList<SelectItem>();
            ListarOperacoesServicosEntradaDTO entrada = new ListarOperacoesServicosEntradaDTO();
            entrada.setCdProduto(getTipoServicoHistorico());

            List<ListarOperacoesServicoSaidaDTO> list = listarOperacoes(entrada);

            for (ListarOperacoesServicoSaidaDTO saida : list) {
                listaOperacaoHistorico.add(new SelectItem(saida.getCdOperacao(), saida.getDsOperacao()));
            }

        } catch (PdcAdapterFunctionalException p) {
            listaOperacaoHistorico = new ArrayList<SelectItem>();
            setOperacaoHistorico(0);
        }

    }

    private List<ListarOperacoesServicoSaidaDTO> listarOperacoes(ListarOperacoesServicosEntradaDTO entrada) {
        return comboService.listarOperacoesServicos(entrada);
    }
	
	/**
	 * Listar operacoes.
	 */
	public void listarOperacoes(){
		try{
			setCdOperacao(0);
			listaOperacaoHash.clear();
			//if (getCdModalidadeServico() != null && getCdModalidadeServico() != 0){ 
				listaOperacao = new ArrayList<SelectItem>();
				ListarOperacoesServicosEntradaDTO entrada = new ListarOperacoesServicosEntradaDTO();
				entrada.setCdProduto(getCdTipoServico());
				
				List<ListarOperacoesServicoSaidaDTO> list = listarOperacoes(entrada);
				
				for (ListarOperacoesServicoSaidaDTO saida : list){
					listaOperacao.add(new SelectItem(saida.getCdOperacao(), saida.getDsOperacao()));
					listaOperacaoHash.put(saida.getCdOperacao(), saida.getDsOperacao());
					
				}
//			}else{
//				listaOperacao = new ArrayList<SelectItem>();
//			}
			
		}catch(PdcAdapterFunctionalException p){
			listaOperacao = new ArrayList<SelectItem>();
			setCdOperacao(0);
		}
	}
	
	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos(){
	    setTipoServicoFiltro(null);
		setModalidadeServicoFiltro(0);
		listaModalidadeFiltro = new ArrayList<SelectItem>();
		setOperacaoFiltro(0);
		listaOperacaoFiltro = new ArrayList<SelectItem>();
		setBtoAcionado(false);
		setItemSelecionadoLista(null);
		setListaGrid(null);
		
		limparHistorico();
		
		return "";
	}
	
	/**
	 * Nome: limparCamposHistorico
	 * 
	 * @return
	 */
	public String limparCamposHistorico(){
        setTipoServicoHistorico(null);
        setModalidadeServicoHistorico(0);
        listaModalidadeHistorico = new ArrayList<SelectItem>();
        setOperacaoHistorico(0);
        listaOperacaoHistorico = new ArrayList<SelectItem>();
        setBtoAcionado(false);
        setItemSelecionadoHistorico(null);
        
        setDataManutencaoFim(new Date());
        setDataManutencaoInicio(new Date());

        limparHistorico();
        
        return "";
    }
	
	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar(){
		try{
			setBtoAcionado(true);
			
			ListarOperacaoServicoPagtoIntegradoEntradaDTO entrada = new ListarOperacaoServicoPagtoIntegradoEntradaDTO();
			entrada.setCdOperacaoProdutoServico(getOperacaoFiltro());
			entrada.setCdProdutoOperacaoRelacionado(getModalidadeServicoFiltro());
			entrada.setCdprodutoServicoOperacao(getTipoServicoFiltro());
			
			setListaGrid(getManterOperacoesServRelacionadoImpl().listarOperacaoServicoPagtoIntegrado(entrada));
			
			setItemSelecionadoLista(null);			
			this.listaControleRadio =  new ArrayList<SelectItem>();
			
			for (int i = 0; i < getListaGrid().size();i++) {
				this.listaControleRadio.add(new SelectItem(i," "));
			}
		
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setBtoAcionado(false);
		}		
		
		return "";
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt){
		consultar();
	}
	
	/**
	 * Nome: consultarHistorico
	 * 
	 * @param evt
	 */
	public void consultarHistorico(ActionEvent evt){
	    consultarHistorico();
    }
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try{
			preencheDados();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
		return "DETALHAR";
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		setCdTipoServico(null);
		setCdModalidadeServico(0);
		listaModalidade = new ArrayList<SelectItem>();
		setCdOperacao(0);
		listaOperacao = new ArrayList<SelectItem>();
		setCodigoOperacao("");
		setRdoTarifavelContabil(null);	
		limparTarifaAlcada();
			
		return "INCLUIR";
	}
	
	/**
	 * Nome: alterar
	 * 
	 * @return
	 */
	public String alterar(){
	    ConsultarTarifaCatalogoEntradaDTO entrada = new ConsultarTarifaCatalogoEntradaDTO();
        entrada.setCdProdutoServicoOperacional(getListaGrid().get(getItemSelecionadoLista()).getCdprodutoServicoOperacao());
        entrada.setCdProdutoOperacionalRelacionado(getListaGrid().get(getItemSelecionadoLista()).getCdProdutoOperacaoRelacionado());
        entrada.setCdOperacaoProdutoServico(getListaGrid().get(getItemSelecionadoLista()).getCdOperacaoProdutoServico());
        
	    try{
	        preencheDados();
	        setRdoTarifavelContabil(saidaDetalhe.getCdNaturezaOperacaoPagamento());

	        buscarTarifaCatalogo(entrada);
	        
	        if(not(validarTarifaMaximaNegociada())){
	            setRdoTarifaAlcadaAgencia(saidaDetalhe.getCdTipoAlcadaTarifa().toString());
	            if(getRdoTarifaAlcadaAgencia().equals("1")){
	                setValorAlcadaTarifaAgencia(saidaDetalhe.getVrAlcadaTarifaAgencia());
	                
	                calcularPercentualTarifa();
	            }else{
	                setPercentualAlcadaTarifaAgencia(saidaDetalhe.getPcAlcadaTarifaAgencia());
	                
	                calcularValorTarifa();
	            }
	        }
            
        }catch (PdcAdapterFunctionalException p){
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false); 
            return "";
        }
        
	    return "ALTERAR";
	}
	
	/**
	 * Nome: historico
	 * 
	 * @return
	 */
	public String historico(){
	    limparCamposHistorico();
        
        if(isNotNull(getItemSelecionadoLista())){
            setTipoServicoHistorico(getListaGrid().get(getItemSelecionadoLista()).getCdprodutoServicoOperacao());
            
            listarModalidadesHistorico();
            carregarComboOperacoes();
            
            setModalidadeServicoHistorico(getListaGrid().get(getItemSelecionadoLista()).getCdProdutoOperacaoRelacionado());
            setOperacaoHistorico(getListaGrid().get(getItemSelecionadoLista()).getCdOperacaoProdutoServico());
            
           
        }
        
        return "HISTORICO";
    }

    private void carregarComboOperacoes() {

        try {
            ListarOperacoesServicosEntradaDTO entradaListarOperacoes = new ListarOperacoesServicosEntradaDTO();
            ListarServicoRelacionadoEntradaDTO entradaDTO = new ListarServicoRelacionadoEntradaDTO();

            int cdProdutoOperacaoRelacionado = getListaGrid().get(getItemSelecionadoLista()).getCdProdutoOperacaoRelacionado();
            int cdprodutoServicoOperacao = getListaGrid().get(getItemSelecionadoLista()).getCdprodutoServicoOperacao();
            
            entradaDTO.setModalidadeServico(cdProdutoOperacaoRelacionado);
            entradaDTO.setTipoServico(cdprodutoServicoOperacao);
            entradaDTO.setTipoRelacionamento(1);
            entradaDTO.setQtOcorrencias(1);

            ListarServicoRelacionadoSaidaDTO saidaDto = getManterOperacoesServRelacionadoImpl().listarServicoRelacionadoDTO(entradaDTO);
            
            if((saidaDto.getCodServicoComposto() > 0 && saidaDto.getCodServicoComposto() < 8) || 
                            saidaDto.getCodServicoComposto() == 34 || saidaDto.getCodServicoComposto() == 35){
                entradaListarOperacoes.setCdProduto(cdProdutoOperacaoRelacionado);
            }else{
                entradaListarOperacoes.setCdProduto(cdprodutoServicoOperacao);
            }
                      
            List<ListarOperacoesServicoSaidaDTO> list = listarOperacoes(entradaListarOperacoes);
            
            for (ListarOperacoesServicoSaidaDTO saida : list){
                listaOperacaoHistorico.add(new SelectItem(saida.getCdOperacao(), saida.getDsOperacao()));
            }
            
        } catch (PdcAdapterFunctionalException p) {
            listaOperacaoHistorico = new ArrayList<SelectItem>();
            setOperacaoHistorico(0);       

        }
    }
    
   
	/**
	 * Nome: detalharHistorico
	 * 
	 * @return
	 */
	public String detalharHistorico(){
	    ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO historicoSelecionado = getListaGridHistorico().get(getItemSelecionadoHistorico());
	    setDsTipoServico(historicoSelecionado.getDsProdutoServicoOperacao());
	    setDsModalidadeServico(historicoSelecionado.getDsProdutoOperacaoRelacionado());
	    setDsOperacao(historicoSelecionado.getDsOperacaoProdutoServico());
	    setDsIndicadorManutencao(historicoSelecionado.getDsIndicadorTipoManutencao());
	    
	    DetalharHistoricoOperacaoServicoPagtoIntegradoEntradaDTO entrada = new DetalharHistoricoOperacaoServicoPagtoIntegradoEntradaDTO();
	    
        entrada.setCdProdutoServicoOperacional(historicoSelecionado.getCdProdutoServicoOperacional());
        entrada.setCdProdutoOperacionalRelacionado(historicoSelecionado.getCdProdutoOperacionalRelacionado());
        entrada.setCdOperacaoProdutoServico(historicoSelecionado.getCdOperacaoProdutoServico());
        entrada.setHrInclusaoRegistroHistorico(historicoSelecionado.getHrInclusaoRegistroHistorico());
	    
	    try{
	                
	        setSaidaDetalheHistorico(getManterOperacoesServRelacionadoImpl().detalharHistoricoOperacaoServicoPagtoIntegrado(entrada));
	        
        }catch (PdcAdapterFunctionalException p){
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false); 
            return "";
        }
        
	    return "DETALHAR_HISTORICO";
	}
	
	/**
	 * Nome: limparHistorico
	 * 
	 * @return
	 */
	public String limparHistorico(){
        setBtoAcionado(false);
        setListaGridHistorico(null);
        setItemSelecionadoHistorico(null);
        
        return "";
    }
	
	/**
	 * Nome: consultarHistorico
	 * 
	 * @return
	 */
	public String consultarHistorico(){
	    setBtoAcionado(true);
	    
	    try{
            
	        ListarHistoricoOperacaoServicoPagtoIntegradoEntradaDTO entrada = new ListarHistoricoOperacaoServicoPagtoIntegradoEntradaDTO();
	        entrada.setCdProdutoServicoOperacional(getTipoServicoHistorico());
	        entrada.setCdProdutoOperacionalRelacionado(getModalidadeServicoHistorico());
	        entrada.setCdOperacaoProdutoServico(getOperacaoHistorico());
	        entrada.setDtInicialPesquisaHistorico(getDataManutencaoInicio());
	        entrada.setDtFinalPesquisaHistorico(getDataManutencaoFim());
            
            setListaGridHistorico(getManterOperacoesServRelacionadoImpl().listarHistoricoOperacaoServicoPagtoIntegrado(entrada).getOcorrencias());
            
            setItemSelecionadoHistorico(null);          
            this.listaControleRadioHistorico =  new ArrayList<SelectItem>();
            
            for (int i = 0; i < getListaGridHistorico().size();i++) {
                this.listaControleRadioHistorico.add(new SelectItem(i," "));
            }
        
        }catch (PdcAdapterFunctionalException p){
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
            setListaGridHistorico(null);
            setItemSelecionadoHistorico(null);
            setBtoAcionado(false);
        }       
        
        return "";
	}
	
	/**
	 * Nome: calcularPercentualTarifa
	 * 
	 */
	public void calcularPercentualTarifa(){
	    Double percentual = null;
	    
	    if(isNotNull(getValorAlcadaTarifaAgencia())){
	        Double valor1 = getValorAlcadaTarifaAgencia().doubleValue() / getSaidaTarifaCatalogo().getVrTarifaMaximaNegociada().doubleValue();
	        Double valor2 = valor1 * 100;
	        percentual = 100 - valor2;
	 
	        setPercentualAlcadaTarifaAgencia(new BigDecimal(NumberUtils.format(percentual).replace(",", ".")));
	        
	    }
	}
	
	/**
	 * Nome: calcularValorTarifa
	 * 
	 */
	public void calcularValorTarifa(){
	    BigDecimal valorTarifa = null;
	    
	    if(isNotNull(getPercentualAlcadaTarifaAgencia())){
	        BigDecimal valor1 = getSaidaTarifaCatalogo().getVrTarifaMaximaNegociada().multiply(getPercentualAlcadaTarifaAgencia());
	        BigDecimal valor2 = valor1.divide(VALOR_CEM, 2, BigDecimal.ROUND_HALF_UP);
	        valorTarifa = getSaidaTarifaCatalogo().getVrTarifaMaximaNegociada().subtract(valor2);
	         
	        setValorAlcadaTarifaAgencia(valorTarifa);
        }
	    
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try{
			preencheDados();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);	
			return "";
		}
		return "EXCLUIR";
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		setItemSelecionadoLista(null);
		setItemSelecionadoHistorico(null);
		
		return "VOLTAR";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
	    
	    if(not(validarTarifaMaximaNegociada())){
	        if(validarRadidoTarifaAlcadaAgencia()){
	            
	            return null;
	        }else if(validarCampoValor()){

	            return null;
	        }else{
	         
	            if(validarCampoPercentual()){
	                
	                return null;
	            }
	        }
	    }
	    
	    for(int i = 0; i < listaServicoRelacionado.size(); i++){
	        if(getCdTipoServico().equals(listaServicoRelacionado.get(i).getCdServico())){
	            setDsTipoServico(PgitUtil.concatenarCampos((listaServicoRelacionado.get(i).getCdServico()), listaServicoRelacionado.get(i).getDsServico(), "-"));
	        }
	    }
		
		setDsModalidadeServico((String)listaModalidadeHash.get(getCdModalidadeServico()));
		
		if (getDsTipoServico().equalsIgnoreCase(getDsModalidadeServico())) {
			setDsModalidadeServico("");
		}
		
		setDsOperacao((String)listaOperacaoHash.get(getCdOperacao()));
		
		return "INCLUIR";
	}
	
	/**
	 * Nome: validarRadidoTarifaAlcadaAgencia
	 * 
	 * @return
	 */
	private boolean validarRadidoTarifaAlcadaAgencia(){
	    boolean retorno = false;
	    if(isNull(getRdoTarifaAlcadaAgencia())){
	        BradescoFacesUtils.addInfoModalMessage("Tarifa de Al�ada da Ag�ncia, deve estar selecionado.", false);
	        
	        retorno = true;
	    }
	    
	    return retorno;
	    
	}
	
	/**
	 * Nome: validarCampoValor
	 * 
	 * @return
	 */
	private boolean validarCampoValor(){
	    boolean retorno = false;
	    if(getRdoTarifaAlcadaAgencia().equals("1")){
	        
	        if(getValorAlcadaTarifaAgencia().compareTo(BigDecimal.ZERO) == -1 
	                        || getValorAlcadaTarifaAgencia().compareTo(getSaidaTarifaCatalogo().getVrTarifaMaximaNegociada()) == 1){
	            
	            BradescoFacesUtils.addInfoModalMessage("O campo Valor dever estar emtre 0 e  " + 
	                PgitUtil.formatNumber(getSaidaTarifaCatalogo().getVrTarifaMaximaNegociada().doubleValue(), 2, ",", false), false);
	            
	            retorno = true;
	        }
	    }
	    
	    return retorno;
	}
	
	/**
	 * Nome: validarCampoPercentual
	 * 
	 * @return
	 */
	private boolean validarCampoPercentual(){
        boolean retorno = false;
        
        if(getPercentualAlcadaTarifaAgencia().compareTo(BigDecimal.ZERO) == -1 
                        || getPercentualAlcadaTarifaAgencia().compareTo(VALOR_CEM) == 1){
            
            BradescoFacesUtils.addInfoModalMessage("O campo Percentual dever estar emtre 0 e  100.", false);
                
                retorno = true;
            }
        
        return retorno;
    }
	
	
	/**
	 * Nome: avancarAlterar
	 * 
	 * @return
	 */
	public String avancarAlterar (){
	    if(not(validarTarifaMaximaNegociada())){
            if(validarRadidoTarifaAlcadaAgencia()){
                
                return null;
            }else if(validarCampoValor()){

                return null;
            }else{
             
                if(validarCampoPercentual()){
                    
                    return null;
                }
            }
        }
	    
	    return "AVANCAR_ALTERAR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		try {
			IncluirOperacaoServicoPagtoIntegradoEntradaDTO entradaDTO = new IncluirOperacaoServicoPagtoIntegradoEntradaDTO();
			entradaDTO.setCdprodutoServicoOperacao(getCdTipoServico());
			entradaDTO.setCdProdutoOperacaoRelacionado(getCdModalidadeServico());
			entradaDTO.setCdOperacaoProdutoServico(getCdOperacao());
			entradaDTO.setCdOperacaoServicoIntegrado(Integer.parseInt(getCodigoOperacao()));
			
			entradaDTO.setVrAlcadaTarifaAgencia(getValorAlcadaTarifaAgencia());
			entradaDTO.setPcAlcadaTarifaAgencia(getPercentualAlcadaTarifaAgencia());
			if(isNotNull(getRdoTarifaAlcadaAgencia())){
			    entradaDTO.setCdTipoAlcadaTarifa(new Integer(getRdoTarifaAlcadaAgencia()));
			}
			
			entradaDTO.setCdNaturezaOperacaoPagamento(getRdoTarifavelContabil());
			
			IncluirOperacaoServicoPagtoIntegradoSaidaDTO saidaDTO = getManterOperacoesServRelacionadoImpl().incluirOperacaoServicoPagtoIntegrado(entradaDTO);
	
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conManterOperacoesServRelacionado", BradescoViewExceptionActionType.ACTION, false);
			
			if(getListaGrid() != null){
				consultar();
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Nome: confirmarAlterar
	 * 
	 * @return
	 */
	public String confirmarAlterar(){
	    
        try {
            AlterarOperacaoServicoPagtoIntegradoEntradaDTO entradaDTO = new AlterarOperacaoServicoPagtoIntegradoEntradaDTO();
            entradaDTO.setCdProdutoServicoOperacional(getCdTipoServico());
            entradaDTO.setCdProdutoOperacionalRelacionado(getCdModalidadeServico());
            entradaDTO.setCdOperacaoProdutoServico(getCdOperacao());
            entradaDTO.setCdOperacaoServicoPagamentoIntegrado(Integer.valueOf(getListaGrid().get(getItemSelecionadoLista()).getCdOperacaoServicoIntegrado()));
            entradaDTO.setCdNaturezaOperacaoPagamento(getRdoTarifavelContabil());
            entradaDTO.setVrAlcadaTarifaAgencia(getValorAlcadaTarifaAgencia());
            entradaDTO.setPcAlcadaTarifaAgencia(getPercentualAlcadaTarifaAgencia());
            
            if(isNotNull(getRdoTarifaAlcadaAgencia())){
                entradaDTO.setCdTipoAlcadaTarifa(Integer.valueOf(getRdoTarifaAlcadaAgencia()));
            }
            
            AlterarOperacaoServicoPagtoIntegradoSaidaDTO saidaDTO = getManterOperacoesServRelacionadoImpl().alterarOperacaoServicoPagtoIntegrado(entradaDTO);
    
            BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conManterOperacoesServRelacionado", BradescoViewExceptionActionType.ACTION, false);
            
            if(getListaGrid() != null){
                consultar();
            }
        } catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
            return null;
        }
        
        return "";
    }
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try {
			ExcluirOperacaoServicoPagtoIntegradoEntradaDTO entradaDTO = new ExcluirOperacaoServicoPagtoIntegradoEntradaDTO();
			entradaDTO.setCdprodutoServicoOperacao(getCdTipoServico());
			entradaDTO.setCdProdutoOperacaoRelacionado(getCdModalidadeServico());
			entradaDTO.setCdOperacaoProdutoServico(getCdOperacao());
						
			ExcluirOperacaoServicoPagtoIntegradoSaidaDTO saidaDTO = getManterOperacoesServRelacionadoImpl().excluirOperacaoServicoPagtoIntegrado(entradaDTO);
	
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conManterOperacoesServRelacionado", BradescoViewExceptionActionType.ACTION, false);
			
			consultar();
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		ListarOperacaoServicoPagtoIntegradoSaidaDTO itemSelecionado = getListaGrid().get(getItemSelecionadoLista());
		DetalharOperacaoServicoPagtoIntegradoEntradaDTO entradaDTO = new DetalharOperacaoServicoPagtoIntegradoEntradaDTO();
		
		entradaDTO.setCdprodutoServicoOperacao(itemSelecionado.getCdprodutoServicoOperacao());
		entradaDTO.setCdProdutoOperacaoRelacionado(itemSelecionado.getCdProdutoOperacaoRelacionado());
		entradaDTO.setCdOperacaoProdutoServico(itemSelecionado.getCdOperacaoProdutoServico());
				
		saidaDetalhe = getManterOperacoesServRelacionadoImpl().detalharOperacaoServicoPagtoIntegrado(entradaDTO);
		
		setCdTipoServico(itemSelecionado.getCdprodutoServicoOperacao());
		setDsTipoServico(itemSelecionado.getDsTipoServico());
		setCdModalidadeServico(itemSelecionado.getCdProdutoOperacaoRelacionado());
		setDsModalidadeServico(itemSelecionado.getDsModalidadeServico());
		setCdOperacao(itemSelecionado.getCdOperacaoProdutoServico());
		setDsOperacao(itemSelecionado.getDsOperacaoCatalogo());
		setCodigoOperacao(String.valueOf(itemSelecionado.getCdOperacaoServicoIntegrado()));
		setDsNatureza(itemSelecionado.getDsNatureza());
		
		setDataHoraInclusao(FormatarData.formatarDataTrilha(saidaDetalhe.getHrInclusaoRegistro()));
		setUsuarioInclusao(saidaDetalhe.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDetalhe.getCdTipoCanalInclusao());
		setComplementoInclusao(saidaDetalhe.getComplementoInclusaoFormatado());
		setDataHoraManutencao(FormatarData.formatarDataTrilha(saidaDetalhe.getHrManutencaoRegistro()));
		setUsuarioManutencao(saidaDetalhe.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDetalhe.getCdTipoCanalManutencao());
		setComplementoManutencao(saidaDetalhe.getComplementoManutencaoFormatado());
	}
	
	/**
	 * Nome: getPercentualAlcadaTarifaAgenciaFormatado
	 * 
	 * @return
	 */
	public String getPercentualAlcadaTarifaAgenciaFormatado(){
	    return PgitUtil.formatNumberPercent(getPercentualAlcadaTarifaAgencia());
	}
	
	/**
	 * Nome: isDesabilitarTarifaAlcada
	 * 
	 */
	public boolean isDesabilitarTarifaAlcada(){
	    return getCdOperacao() == null || getCdOperacao().compareTo(0) == 0 
	        || saidaTarifaCatalogo == null || validarTarifaMaximaNegociada();
	}
	
	/**
	 * Nome: isDesabilitarValorTarifa
	 * 
	 * @return
	 */
	public boolean isDesabilitarValorTarifa(){
	    return isRdoTarifaAlcadaAgenciaNull() || getRdoTarifaAlcadaAgencia().equals("2");
	}
	
	public boolean isDesabilitarPercentualTarifa(){
        return isRdoTarifaAlcadaAgenciaNull() || getRdoTarifaAlcadaAgencia().equals("1");
    }

    /**
     * Nome: isRdoTarifaAlcadaAgenciaNull
     * 
     * @return
     */
    private boolean isRdoTarifaAlcadaAgenciaNull() {
        return getRdoTarifaAlcadaAgencia() == null;
    }
	
	//Set's e Get's
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: manterOperacoesServRelacionadoImpl.
	 *
	 * @return manterOperacoesServRelacionadoImpl
	 */
	public IManterOperacoesServRelacionadoService getManterOperacoesServRelacionadoImpl() {
		return manterOperacoesServRelacionadoImpl;
	}
	
	/**
	 * Set: manterOperacoesServRelacionadoImpl.
	 *
	 * @param manterOperacoesServRelacionadoImpl the manter operacoes serv relacionado impl
	 */
	public void setManterOperacoesServRelacionadoImpl(
			IManterOperacoesServRelacionadoService manterOperacoesServRelacionadoImpl) {
		this.manterOperacoesServRelacionadoImpl = manterOperacoesServRelacionadoImpl;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Get: modalidadeServicoFiltro.
	 *
	 * @return modalidadeServicoFiltro
	 */
	public Integer getModalidadeServicoFiltro() {
		return modalidadeServicoFiltro;
	}

	/**
	 * Set: modalidadeServicoFiltro.
	 *
	 * @param modalidadeServicoFiltro the modalidade servico filtro
	 */
	public void setModalidadeServicoFiltro(Integer modalidadeServicoFiltro) {
		this.modalidadeServicoFiltro = modalidadeServicoFiltro;
	}

	/**
	 * Get: listaTipoServicoFiltro.
	 *
	 * @return listaTipoServicoFiltro
	 */
	public List<SelectItem> getListaTipoServicoFiltro() {
		return listaTipoServicoFiltro;
	}

	/**
	 * Set: listaTipoServicoFiltro.
	 *
	 * @param listaTipoServicoFiltro the lista tipo servico filtro
	 */
	public void setListaTipoServicoFiltro(List<SelectItem> listaTipoServicoFiltro) {
		this.listaTipoServicoFiltro = listaTipoServicoFiltro;
	}

	/**
	 * Get: listaModalidadeFiltro.
	 *
	 * @return listaModalidadeFiltro
	 */
	public List<SelectItem> getListaModalidadeFiltro() {
		return listaModalidadeFiltro;
	}

	/**
	 * Set: listaModalidadeFiltro.
	 *
	 * @param listaModalidadeFiltro the lista modalidade filtro
	 */
	public void setListaModalidadeFiltro(List<SelectItem> listaModalidadeFiltro) {
		this.listaModalidadeFiltro = listaModalidadeFiltro;
	}

	/**
	 * Get: listaModalidadeHash.
	 *
	 * @return listaModalidadeHash
	 */
	public Map<Integer, String> getListaModalidadeHash() {
		return listaModalidadeHash;
	}

	/**
	 * Set lista modalidade hash.
	 *
	 * @param listaModalidadeHash the lista modalidade hash
	 */
	public void setListaModalidadeHash(Map<Integer, String> listaModalidadeHash) {
		this.listaModalidadeHash = listaModalidadeHash;
	}

	/**
	 * Get: operacaoFiltro.
	 *
	 * @return operacaoFiltro
	 */
	public Integer getOperacaoFiltro() {
		return operacaoFiltro;
	}

	/**
	 * Set: operacaoFiltro.
	 *
	 * @param operacaoFiltro the operacao filtro
	 */
	public void setOperacaoFiltro(Integer operacaoFiltro) {
		this.operacaoFiltro = operacaoFiltro;
	}

	/**
	 * Get: listaOperacaoFiltro.
	 *
	 * @return listaOperacaoFiltro
	 */
	public List<SelectItem> getListaOperacaoFiltro() {
		return listaOperacaoFiltro;
	}

	/**
	 * Set: listaOperacaoFiltro.
	 *
	 * @param listaOperacaoFiltro the lista operacao filtro
	 */
	public void setListaOperacaoFiltro(List<SelectItem> listaOperacaoFiltro) {
		this.listaOperacaoFiltro = listaOperacaoFiltro;
	}

	/**
	 * Get: listaOperacaoHash.
	 *
	 * @return listaOperacaoHash
	 */
	public Map<Integer, String> getListaOperacaoHash() {
		return listaOperacaoHash;
	}

	/**
	 * Set lista operacao hash.
	 *
	 * @param listaOperacaoHash the lista operacao hash
	 */
	public void setListaOperacaoHash(Map<Integer, String> listaOperacaoHash) {
		this.listaOperacaoHash = listaOperacaoHash;
	}

	/**
	 * Is bto acionado.
	 *
	 * @return true, if is bto acionado
	 */
	public boolean isBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarOperacaoServicoPagtoIntegradoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(
			List<ListarOperacaoServicoPagtoIntegradoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: cdModalidadeServico.
	 *
	 * @return cdModalidadeServico
	 */
	public Integer getCdModalidadeServico() {
		return cdModalidadeServico;
	}

	/**
	 * Set: cdModalidadeServico.
	 *
	 * @param cdModalidadeServico the cd modalidade servico
	 */
	public void setCdModalidadeServico(Integer cdModalidadeServico) {
		this.cdModalidadeServico = cdModalidadeServico;
	}

	/**
	 * Get: cdOperacao.
	 *
	 * @return cdOperacao
	 */
	public Integer getCdOperacao() {
		return cdOperacao;
	}

	/**
	 * Set: cdOperacao.
	 *
	 * @param cdOperacao the cd operacao
	 */
	public void setCdOperacao(Integer cdOperacao) {
		this.cdOperacao = cdOperacao;
	}

	/**
	 * Get: cdTipoServico.
	 *
	 * @return cdTipoServico
	 */
	public Integer getCdTipoServico() {
		return cdTipoServico;
	}

	/**
	 * Set: cdTipoServico.
	 *
	 * @param cdTipoServico the cd tipo servico
	 */
	public void setCdTipoServico(Integer cdTipoServico) {
		this.cdTipoServico = cdTipoServico;
	}

	/**
	 * Get: listaModalidade.
	 *
	 * @return listaModalidade
	 */
	public List<SelectItem> getListaModalidade() {
		return listaModalidade;
	}

	/**
	 * Set: listaModalidade.
	 *
	 * @param listaModalidade the lista modalidade
	 */
	public void setListaModalidade(List<SelectItem> listaModalidade) {
		this.listaModalidade = listaModalidade;
	}

	/**
	 * Get: listaOperacao.
	 *
	 * @return listaOperacao
	 */
	public List<SelectItem> getListaOperacao() {
		return listaOperacao;
	}

	/**
	 * Set: listaOperacao.
	 *
	 * @param listaOperacao the lista operacao
	 */
	public void setListaOperacao(List<SelectItem> listaOperacao) {
		this.listaOperacao = listaOperacao;
	}

	/**
	 * Get: codigoOperacao.
	 *
	 * @return codigoOperacao
	 */
	public String getCodigoOperacao() {
		return codigoOperacao;
	}

	/**
	 * Set: codigoOperacao.
	 *
	 * @param codigoOperacao the codigo operacao
	 */
	public void setCodigoOperacao(String codigoOperacao) {
		this.codigoOperacao = codigoOperacao;
	}

	/**
	 * Get: dsModalidadeServico.
	 *
	 * @return dsModalidadeServico
	 */
	public String getDsModalidadeServico() {
		return dsModalidadeServico;
	}

	/**
	 * Set: dsModalidadeServico.
	 *
	 * @param dsModalidadeServico the ds modalidade servico
	 */
	public void setDsModalidadeServico(String dsModalidadeServico) {
		this.dsModalidadeServico = dsModalidadeServico;
	}

	/**
	 * Get: dsOperacao.
	 *
	 * @return dsOperacao
	 */
	public String getDsOperacao() {
		return dsOperacao;
	}

	/**
	 * Set: dsOperacao.
	 *
	 * @param dsOperacao the ds operacao
	 */
	public void setDsOperacao(String dsOperacao) {
		this.dsOperacao = dsOperacao;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: rdoTarifavelContabil.
	 *
	 * @return rdoTarifavelContabil
	 */
	public Integer getRdoTarifavelContabil() {
	    return rdoTarifavelContabil;
	}

	/**
	 * Set: rdoTarifavelContabil.
	 *
	 * @param rdoTarifavelContabil the rdo tarifavel contabil
	 */
	public void setRdoTarifavelContabil(Integer rdoTarifavelContabil) {
	    this.rdoTarifavelContabil = rdoTarifavelContabil;
	}

	/**
	 * Get: dsNatureza.
	 *
	 * @return dsNatureza
	 */
	public String getDsNatureza() {
	    return dsNatureza;
	}

	/**
	 * Set: dsNatureza.
	 *
	 * @param dsNatureza the ds natureza
	 */
	public void setDsNatureza(String dsNatureza) {
	    this.dsNatureza = dsNatureza;
	}

	public List<SelectItem> getListarArgumentoPesquisaServicoModalidade() {
		return listarArgumentoPesquisaServicoModalidade;
	}

	public void setListarArgumentoPesquisaServicoModalidade(
			List<SelectItem> listarArgumentoPesquisaServicoModalidade) {
		this.listarArgumentoPesquisaServicoModalidade = listarArgumentoPesquisaServicoModalidade;
	}

    /**
     * Nome: getSaidaTarifaCatalogo
     *
     * @return saidaTarifaCatalogo
     */
    public ConsultarTarifaCatalogoSaidaDTO getSaidaTarifaCatalogo() {
        return saidaTarifaCatalogo;
    }

    /**
     * Nome: setSaidaTarifaCatalogo
     *
     * @param saidaTarifaCatalogo
     */
    public void setSaidaTarifaCatalogo(ConsultarTarifaCatalogoSaidaDTO saidaTarifaCatalogo) {
        this.saidaTarifaCatalogo = saidaTarifaCatalogo;
    }

    /**
     * Nome: getRdoTarifaAlcadaAgencia
     *
     * @return rdoTarifaAlcadaAgencia
     */
    public String getRdoTarifaAlcadaAgencia() {
        return rdoTarifaAlcadaAgencia;
    }

    /**
     * Nome: setRdoTarifaAlcadaAgencia
     *
     * @param rdoTarifaAlcadaAgencia
     */
    public void setRdoTarifaAlcadaAgencia(String rdoTarifaAlcadaAgencia) {
        this.rdoTarifaAlcadaAgencia = rdoTarifaAlcadaAgencia;
    }

    /**
     * Nome: getValorAlcadaTarifaAgencia
     *
     * @return valorAlcadaTarifaAgencia
     */
    public BigDecimal getValorAlcadaTarifaAgencia() {
        return valorAlcadaTarifaAgencia;
    }

    /**
     * Nome: setValorAlcadaTarifaAgencia
     *
     * @param valorAlcadaTarifaAgencia
     */
    public void setValorAlcadaTarifaAgencia(BigDecimal valorAlcadaTarifaAgencia) {
        this.valorAlcadaTarifaAgencia = valorAlcadaTarifaAgencia;
    }

    /**
     * Nome: getPercentualAlcadaTarifaAgencia
     *
     * @return percentualAlcadaTarifaAgencia
     */
    public BigDecimal getPercentualAlcadaTarifaAgencia() {
        return percentualAlcadaTarifaAgencia;
    }

    /**
     * Nome: setPercentualAlcadaTarifaAgencia
     *
     * @param percentualAlcadaTarifaAgencia
     */
    public void setPercentualAlcadaTarifaAgencia(BigDecimal percentualAlcadaTarifaAgencia) {
        this.percentualAlcadaTarifaAgencia = percentualAlcadaTarifaAgencia;
    }

    /**
     * Nome: getSaidaDetalhe
     *
     * @return saidaDetalhe
     */
    public DetalharOperacaoServicoPagtoIntegradoSaidaDTO getSaidaDetalhe() {
        return saidaDetalhe;
    }

    /**
     * Nome: setSaidaDetalhe
     *
     * @param saidaDetalhe
     */
    public void setSaidaDetalhe(DetalharOperacaoServicoPagtoIntegradoSaidaDTO saidaDetalhe) {
        this.saidaDetalhe = saidaDetalhe;
    }

    /**
     * Nome: getDataManutencaoInicio
     *
     * @return dataManutencaoInicio
     */
    public Date getDataManutencaoInicio() {
        return dataManutencaoInicio;
    }

    /**
     * Nome: setDataManutencaoInicio
     *
     * @param dataManutencaoInicio
     */
    public void setDataManutencaoInicio(Date dataManutencaoInicio) {
        this.dataManutencaoInicio = dataManutencaoInicio;
    }

    /**
     * Nome: getDataManutencaoFim
     *
     * @return dataManutencaoFim
     */
    public Date getDataManutencaoFim() {
        return dataManutencaoFim;
    }

    /**
     * Nome: setDataManutencaoFim
     *
     * @param dataManutencaoFim
     */
    public void setDataManutencaoFim(Date dataManutencaoFim) {
        this.dataManutencaoFim = dataManutencaoFim;
    }

    /**
     * Nome: getListaGridHistorico
     *
     * @return listaGridHistorico
     */
    public List<ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO> getListaGridHistorico() {
        return listaGridHistorico;
    }

    /**
     * Nome: setListaGridHistorico
     *
     * @param listaGridHistorico
     */
    public void setListaGridHistorico(List<ListarHistoricoOperacaoServicoPagtoIntegradoOcorrenciasDTO> listaGridHistorico) {
        this.listaGridHistorico = listaGridHistorico;
    }

    /**
     * Nome: getItemSelecionadoHistorico
     *
     * @return itemSelecionadoHistorico
     */
    public Integer getItemSelecionadoHistorico() {
        return itemSelecionadoHistorico;
    }

    /**
     * Nome: setItemSelecionadoHistorico
     *
     * @param itemSelecionadoHistorico
     */
    public void setItemSelecionadoHistorico(Integer itemSelecionadoHistorico) {
        this.itemSelecionadoHistorico = itemSelecionadoHistorico;
    }

    /**
     * Nome: getListaControleRadioHistorico
     *
     * @return listaControleRadioHistorico
     */
    public List<SelectItem> getListaControleRadioHistorico() {
        return listaControleRadioHistorico;
    }

    /**
     * Nome: setListaControleRadioHistorico
     *
     * @param listaControleRadioHistorico
     */
    public void setListaControleRadioHistorico(List<SelectItem> listaControleRadioHistorico) {
        this.listaControleRadioHistorico = listaControleRadioHistorico;
    }

    /**
     * Nome: getSaidaDetalheHistorico
     *
     * @return saidaDetalheHistorico
     */
    public DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO getSaidaDetalheHistorico() {
        return saidaDetalheHistorico;
    }

    /**
     * Nome: setSaidaDetalheHistorico
     *
     * @param saidaDetalheHistorico
     */
    public void setSaidaDetalheHistorico(DetalharHistoricoOperacaoServicoPagtoIntegradoSaidaDTO saidaDetalheHistorico) {
        this.saidaDetalheHistorico = saidaDetalheHistorico;
    }

    /**
     * Nome: getDsIndicadorManutencao
     *
     * @return dsIndicadorManutencao
     */
    public String getDsIndicadorManutencao() {
        return dsIndicadorManutencao;
    }

    /**
     * Nome: setDsIndicadorManutencao
     *
     * @param dsIndicadorManutencao
     */
    public void setDsIndicadorManutencao(String dsIndicadorManutencao) {
        this.dsIndicadorManutencao = dsIndicadorManutencao;
    }

    /**
     * Nome: getListaServicoRelacionado
     *
     * @return listaServicoRelacionado
     */
    public List<ListarServicoRelacionadoPgitOcorrenciasSaidaDTO> getListaServicoRelacionado() {
        return listaServicoRelacionado;
    }

    /**
     * Nome: setListaServicoRelacionado
     *
     * @param listaServicoRelacionado
     */
    public void setListaServicoRelacionado(List<ListarServicoRelacionadoPgitOcorrenciasSaidaDTO> listaServicoRelacionado) {
        this.listaServicoRelacionado = listaServicoRelacionado;
    }

    /**
     * Nome: getTipoServicoHistorico
     *
     * @return tipoServicoHistorico
     */
    public Integer getTipoServicoHistorico() {
        return tipoServicoHistorico;
    }

    /**
     * Nome: setTipoServicoHistorico
     *
     * @param tipoServicoHistorico
     */
    public void setTipoServicoHistorico(Integer tipoServicoHistorico) {
        this.tipoServicoHistorico = tipoServicoHistorico;
    }

    /**
     * Nome: getListaTipoServicoHistorico
     *
     * @return listaTipoServicoHistorico
     */
    public List<SelectItem> getListaTipoServicoHistorico() {
        return listaTipoServicoHistorico;
    }

    /**
     * Nome: setListaTipoServicoHistorico
     *
     * @param listaTipoServicoHistorico
     */
    public void setListaTipoServicoHistorico(List<SelectItem> listaTipoServicoHistorico) {
        this.listaTipoServicoHistorico = listaTipoServicoHistorico;
    }

    /**
     * Nome: getModalidadeServicoHistorico
     *
     * @return modalidadeServicoHistorico
     */
    public Integer getModalidadeServicoHistorico() {
        return modalidadeServicoHistorico;
    }

    /**
     * Nome: setModalidadeServicoHistorico
     *
     * @param modalidadeServicoHistorico
     */
    public void setModalidadeServicoHistorico(Integer modalidadeServicoHistorico) {
        this.modalidadeServicoHistorico = modalidadeServicoHistorico;
    }

    /**
     * Nome: getListaModalidadeHistorico
     *
     * @return listaModalidadeHistorico
     */
    public List<SelectItem> getListaModalidadeHistorico() {
        return listaModalidadeHistorico;
    }

    /**
     * Nome: setListaModalidadeHistorico
     *
     * @param listaModalidadeHistorico
     */
    public void setListaModalidadeHistorico(List<SelectItem> listaModalidadeHistorico) {
        this.listaModalidadeHistorico = listaModalidadeHistorico;
    }

    /**
     * Nome: getOperacaoHistorico
     *
     * @return operacaoHistorico
     */
    public Integer getOperacaoHistorico() {
        return operacaoHistorico;
    }

    /**
     * Nome: setOperacaoHistorico
     *
     * @param operacaoHistorico
     */
    public void setOperacaoHistorico(Integer operacaoHistorico) {
        this.operacaoHistorico = operacaoHistorico;
    }

    /**
     * Nome: getListaOperacaoHistorico
     *
     * @return listaOperacaoHistorico
     */
    public List<SelectItem> getListaOperacaoHistorico() {
        return listaOperacaoHistorico;
    }

    /**
     * Nome: setListaOperacaoHistorico
     *
     * @param listaOperacaoHistorico
     */
    public void setListaOperacaoHistorico(List<SelectItem> listaOperacaoHistorico) {
        this.listaOperacaoHistorico = listaOperacaoHistorico;
    }
		
}
