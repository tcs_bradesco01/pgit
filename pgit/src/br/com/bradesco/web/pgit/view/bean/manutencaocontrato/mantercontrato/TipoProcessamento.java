package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

public enum TipoProcessamento {

	CICLICO(1l), DIARIO(2l), EXPRESSO(3l), INSTANTANEO(4l);

	private Long codigoTipoProcessamento;

	private TipoProcessamento(Long codigoTipoProcessamento) {
		this.codigoTipoProcessamento = codigoTipoProcessamento;
	}

	public Long getCodigoTipoProcessamento() {
		return codigoTipoProcessamento;
	}

}
