/*
 * Nome: br.com.bradesco.web.pgit.view.converters
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 11/03/2015
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.converters;

import java.util.regex.Pattern;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import br.com.bradesco.web.pgit.utils.NumberUtils;

// TODO: Auto-generated Javadoc
/**
 * Nome: MesAnoConverter
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 * @see Converter
 */
public class MesAnoConverter implements Converter {

    /**
     * Instantiates a new mes ano converter.
     */
    public MesAnoConverter(){
	super();
    }

    /**
     * (non-Javadoc).
     *
     * @param context the context
     * @param component the component
     * @param value the value
     * @return the as object
     * @throws ConverterException the converter exception
     * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.String)
     */
    public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
	// Verifica se value � diferente de null e est� no formato correto
	if (value != null && Pattern.matches("\\d{1,2}/\\d{4}", value)) {
	    return Integer.valueOf(value.replaceAll("/", ""));
	}

	return null;
    }

    /**
     * (non-Javadoc).
     *
     * @param context the context
     * @param component the component
     * @param value the value
     * @return the as string
     * @throws ConverterException the converter exception
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.Object)
     */
    public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
	if (value instanceof Integer) {
	    int mesAno = (Integer) value;

		String mes = NumberUtils.format(mesAno / 10000, "00");
		return String.format("%s/%d", mes, (mesAno % 10000));
	}

	return null;
    }

}
