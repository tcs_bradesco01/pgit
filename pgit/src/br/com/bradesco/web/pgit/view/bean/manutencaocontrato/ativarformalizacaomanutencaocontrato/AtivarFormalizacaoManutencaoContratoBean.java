/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.ativarformalizacaomanutencaocontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.ativarformalizacaomanutencaocontrato;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.IAtivarFormalizacaoManutencaoContratoService;
import br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.bean.AtivarAditivoContratoPendAssEntradaDTO;
import br.com.bradesco.web.pgit.service.business.ativarformalizacaomanutencaocontrato.bean.AtivarAditivoContratoPendAssSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoAditivoContEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoAditivoContSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.IManterformalizacaoalteracaocontratoService;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConsultarManterFormAltContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterformalizacaoalteracaocontrato.bean.ConsultarManterFormAltContratoSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: AtivarFormalizacaoManutencaoContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AtivarFormalizacaoManutencaoContratoBean {
	
	
	////////////////////////////
	//DECLARA��O DE VARI�VEIS//
	//////////////////////////
	
	/** Atributo cpfCnpjClienteMaster. */
	private String cpfCnpjClienteMaster;
	
	/** Atributo nomeRazaoSocialClienteMaster. */
	private String nomeRazaoSocialClienteMaster;
	
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;
	
	/** Atributo grupoEconomico. */
	private String grupoEconomico;
	
	/** Atributo atividadeEconomica. */
	private String atividadeEconomica;
	
	/** Atributo segmento. */
	private String segmento;
	
	/** Atributo subSegmento. */
	private String subSegmento;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;
	
	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;
	
	/** Atributo empresa. */
	private String empresa;
	
	/** Atributo tipo. */
	private String tipo;
	
	/** Atributo numero. */
	private String numero;
	
	/** Atributo situacao. */
	private String situacao;
	
	/** Atributo motivo. */
	private String motivo;
	
	/** Atributo participacaoContrato. */
	private String participacaoContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo cdEmpresa. */
	private String cdEmpresa;
	
	/** Atributo cdTipo. */
	private String cdTipo;
	
	/** Atributo motivoAtivacaoCbo. */
	private Integer motivoAtivacaoCbo;
	
	/** Atributo motivoAtivacaoLista. */
	private List<SelectItem> motivoAtivacaoLista =  new ArrayList<SelectItem>();
	
	/** Atributo motivoAtivacaoListaHash. */
	private Map<Integer, String> motivoAtivacaoListaHash = new HashMap<Integer, String>();
	
	/** Atributo motivoAtivacao. */
	private String motivoAtivacao;
	
	/** Atributo hiddenObrigatoriedade2. */
	private String hiddenObrigatoriedade2;
	
	/** Atributo listaGrid. */
	private List<ConsultarManterFormAltContratoSaidaDTO> listaGrid;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo numeroAditivo. */
	private String numeroAditivo;
	
	/** Atributo situacaoAditivo. */
	private String situacaoAditivo;
	
	/** Atributo dataHoraCadastramentoAditivo. */
	private String dataHoraCadastramentoAditivo;
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo ativarFormalizacaoManutencaoContratoServiceImpl. */
	private IAtivarFormalizacaoManutencaoContratoService ativarFormalizacaoManutencaoContratoServiceImpl;
	
	/** Atributo manterFormalizacaoAlteracaoContratoService. */
	private IManterformalizacaoalteracaocontratoService manterFormalizacaoAlteracaoContratoService;
	
	/////////////////////
	//M�TODOS DIVERSOS//
	///////////////////
		
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Formalizar formalizacao contrato.
	 *
	 * @return the string
	 */
	public String formalizarFormalizacaoContrato(){  //m�todo formalizar da pg pesquisar Formaliza��o de alteracao do contrato pendente de assinatura
		
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		this.setCpfCnpjClienteMaster(saidaDTO.getCnpjOuCpfFormatado());
		this.setNomeRazaoSocialClienteMaster(saidaDTO.getNmRazaoSocialRepresentante());
		this.setGrupoEconomico(String.valueOf(saidaDTO.getCdGrupoEconomico()));
		this.setAtividadeEconomica(saidaDTO.getDsAtividadeEconomica());
		this.setSegmento(saidaDTO.getDsSegmentoCliente());
		this.setSubSegmento(saidaDTO.getDsSubSegmentoCliente());
		this.setEmpresa(saidaDTO.getDsPessoaJuridica());
		this.setTipo(saidaDTO.getDsTipoContrato());
		this.setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		this.setSituacao(saidaDTO.getDsSituacaoContrato());
		this.setMotivo(saidaDTO.getDsMotivoSituacao());
		this.setDescricaoContrato(saidaDTO.getDsContrato());
		this.setParticipacaoContrato(saidaDTO.getCdTipoParticipacao());
		this.setCdEmpresa(String.valueOf(saidaDTO.getCdPessoaJuridica()));
		this.setCdTipo(String.valueOf(saidaDTO.getCdTipoContrato()));
		this.setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		this.setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		
		} else {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		}

		carregaLista();
		
		return "FORMALIZAR";
	}
	
	/**
	 * Voltar formalizacao.
	 *
	 * @return the string
	 */
	public String voltarFormalizacao(){
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "VOLTAR";
	}

	/**
	 * Ativar formalizacao.
	 *
	 * @return the string
	 */
	public String ativarFormalizacao(){
		
		carregaListaMotivo();
		
		setMotivoAtivacaoCbo(0);
		
		ConsultarManterFormAltContratoSaidaDTO saidaDTO = getListaGrid().get(getItemSelecionadoLista());
		
		setNumeroAditivo(String.valueOf(saidaDTO.getNrAditivoContratoNegocio()));
		setSituacaoAditivo(saidaDTO.getDsSitucaoAditivoContrato());
		setDataHoraCadastramentoAditivo(saidaDTO.getDtInclusaoAditivoContrato().concat(" - ").concat(saidaDTO.getHrInclusaoAditivoContrato()));
		
		return "ATIVAR";
	}

	/**
	 * Voltar formalizacao ativar.
	 *
	 * @return the string
	 */
	public String voltarFormalizacaoAtivar(){
		setItemSelecionadoLista(null);
		return "VOLTAR";
	}

	/**
	 * Limpar formalizacao.
	 */
	public void limparFormalizacao(){  
		// zerar o combo 
		this.setMotivoAtivacaoCbo(null);
	}
	
	/**
	 * Avancar formalizacao ativar.
	 *
	 * @return the string
	 */
	public String avancarFormalizacaoAtivar(){
		// checar hidden
		if(getHiddenObrigatoriedade2().equals("T")){
			
			setMotivoAtivacao((String)motivoAtivacaoListaHash.get(getMotivoAtivacaoCbo()));
			
			return "AVANCAR";
		}else{
			return "";
		}
	}

	/**
	 * Voltar confirmar contrato pendente assinatura.
	 *
	 * @return the string
	 */
	public String voltarConfirmarContratoPendenteAssinatura(){
		return "VOLTAR_CONFIRMAR";
	}

	/**
	 * Pesquisar.
	 *
	 * @return the string
	 */
	public String pesquisar() {
		// usado no dataScroller;
		return "ok";
	}
	
	/**
	 * Confirmar formalizacao.
	 *
	 * @return the string
	 */
	public String confirmarFormalizacao(){
				
		try {
			AtivarAditivoContratoPendAssEntradaDTO entradaDTO = new AtivarAditivoContratoPendAssEntradaDTO();
			
			ListarContratosPgitSaidaDTO saidaContratoDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
			
			entradaDTO.setCdMotivoSituacaoContrato(getMotivoAtivacaoCbo());
			entradaDTO.setCdPessoaJuridicaContrato(saidaContratoDTO.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(saidaContratoDTO.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(saidaContratoDTO.getNrSequenciaContrato());
			entradaDTO.setNrAditivoContratoNegocio(getListaGrid().get(getItemSelecionadoLista()).getNrAditivoContratoNegocio());
			
			AtivarAditivoContratoPendAssSaidaDTO saidaDTO = getAtivarFormalizacaoManutencaoContratoServiceImpl().ativarFormalizacaoManutencaoContrato(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "pesqAtivarFormalizacaoAlteracaoContratoPendenteAssinatura", BradescoViewExceptionActionType.ACTION, false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		
		
		return "CONFIRMAR";
	}

	/**
	 * Voltar confirmar.
	 *
	 * @return the string
	 */
	public String voltarConfirmar(){
		return "VOLTAR_CONFIRMAR";
	}
	
	/**
	 * Limpar pagina form.
	 *
	 * @param evt the evt
	 */
	public void limparPaginaForm(ActionEvent evt) {
		
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		//carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteAtivFormManutContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("pesqAtivarFormalizacaoAlteracaoContratoPendenteAssinatura");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		
		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();	
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

	}
	
	/**
	 * Carrega lista.
	 */
	public void carregaLista(){
		listaGrid = new ArrayList<ConsultarManterFormAltContratoSaidaDTO>();
		
		
		
		try{
			
			ConsultarManterFormAltContratoEntradaDTO entradaDTO = new ConsultarManterFormAltContratoEntradaDTO();
			
			ListarContratosPgitSaidaDTO saidaContratosDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
				
			
			entradaDTO.setCdPessoaJuridicaContrato(saidaContratosDTO.getCdPessoaJuridica());
			entradaDTO.setNrSequenciaContratoNegocio(saidaContratosDTO.getNrSequenciaContrato());
			entradaDTO.setCdTipoContratoNegocio(saidaContratosDTO.getCdTipoContrato());
			entradaDTO.setCdAcesso(0);
			entradaDTO.setCdSituacaoAditivoContrato(1);
			entradaDTO.setDtFimInclusaoContrato("");
			entradaDTO.setDtInicioInclusaoContrato("");
			entradaDTO.setNrAditivoContratoNegocio(0l);
			
			setListaGrid(getManterFormalizacaoAlteracaoContratoService().consultarListaManterFormAltContrato(entradaDTO));
			
			this.listaControle=  new ArrayList<SelectItem>();
			for (int i = 0; i <= getListaGrid().size();i++) {
				this.listaControle.add(new SelectItem(i," "));
			}
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);
			setItemSelecionadoLista(null);

		}
	}
	
	
	//////////////////////
	//GETTERS E SETTERS//
	////////////////////
	
	/**
	 * Get: atividadeEconomica.
	 *
	 * @return atividadeEconomica
	 */
	public String getAtividadeEconomica() {
		return atividadeEconomica;
	}
	
	/**
	 * Set: atividadeEconomica.
	 *
	 * @param atividadeEconomica the atividade economica
	 */
	public void setAtividadeEconomica(String atividadeEconomica) {
		this.atividadeEconomica = atividadeEconomica;
	}
	
	/**
	 * Get: cpfCnpjClienteMaster.
	 *
	 * @return cpfCnpjClienteMaster
	 */
	public String getCpfCnpjClienteMaster() {
		return cpfCnpjClienteMaster;
	}
	
	/**
	 * Set: cpfCnpjClienteMaster.
	 *
	 * @param cpfCnpjClienteMaster the cpf cnpj cliente master
	 */
	public void setCpfCnpjClienteMaster(String cpfCnpjClienteMaster) {
		this.cpfCnpjClienteMaster = cpfCnpjClienteMaster;
	}
	
	/**
	 * Get: nomeRazaoSocialClienteMaster.
	 *
	 * @return nomeRazaoSocialClienteMaster
	 */
	public String getNomeRazaoSocialClienteMaster() {
		return nomeRazaoSocialClienteMaster;
	}
	
	/**
	 * Set: nomeRazaoSocialClienteMaster.
	 *
	 * @param nomeRazaoSocialClienteMaster the nome razao social cliente master
	 */
	public void setNomeRazaoSocialClienteMaster(String nomeRazaoSocialClienteMaster) {
		this.nomeRazaoSocialClienteMaster = nomeRazaoSocialClienteMaster;
	}
	
	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}
	
	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	
	/**
	 * Get: grupoEconomico.
	 *
	 * @return grupoEconomico
	 */
	public String getGrupoEconomico() {
		return grupoEconomico;
	}
	
	/**
	 * Set: grupoEconomico.
	 *
	 * @param grupoEconomico the grupo economico
	 */
	public void setGrupoEconomico(String grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}
	
	/**
	 * Get: motivo.
	 *
	 * @return motivo
	 */
	public String getMotivo() {
		return motivo;
	}
	
	/**
	 * Set: motivo.
	 *
	 * @param motivo the motivo
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}
	
	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	/**
	 * Get: segmento.
	 *
	 * @return segmento
	 */
	public String getSegmento() {
		return segmento;
	}
	
	/**
	 * Set: segmento.
	 *
	 * @param segmento the segmento
	 */
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}
	
	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}
	
	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	/**
	 * Get: subSegmento.
	 *
	 * @return subSegmento
	 */
	public String getSubSegmento() {
		return subSegmento;
	}
	
	/**
	 * Set: subSegmento.
	 *
	 * @param subSegmento the sub segmento
	 */
	public void setSubSegmento(String subSegmento) {
		this.subSegmento = subSegmento;
	}
	
	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}
	
	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	/**
	 * Get: motivoAtivacaoCbo.
	 *
	 * @return motivoAtivacaoCbo
	 */
	public Integer getMotivoAtivacaoCbo() {
		return motivoAtivacaoCbo;
	}
	
	/**
	 * Set: motivoAtivacaoCbo.
	 *
	 * @param motivoAtivacaoCbo the motivo ativacao cbo
	 */
	public void setMotivoAtivacaoCbo(Integer motivoAtivacaoCbo) {
		this.motivoAtivacaoCbo = motivoAtivacaoCbo;
	}
	
	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}
	
	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}
	
	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}
	
	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}
	
	/**
	 * Get: dataHoraCadastramentoAditivo.
	 *
	 * @return dataHoraCadastramentoAditivo
	 */
	public String getDataHoraCadastramentoAditivo() {
		return dataHoraCadastramentoAditivo;
	}
	
	/**
	 * Set: dataHoraCadastramentoAditivo.
	 *
	 * @param dataHoraCadastramentoAditivo the data hora cadastramento aditivo
	 */
	public void setDataHoraCadastramentoAditivo(String dataHoraCadastramentoAditivo) {
		this.dataHoraCadastramentoAditivo = dataHoraCadastramentoAditivo;
	}
	
	/**
	 * Get: numeroAditivo.
	 *
	 * @return numeroAditivo
	 */
	public String getNumeroAditivo() {
		return numeroAditivo;
	}
	
	/**
	 * Set: numeroAditivo.
	 *
	 * @param numeroAditivo the numero aditivo
	 */
	public void setNumeroAditivo(String numeroAditivo) {
		this.numeroAditivo = numeroAditivo;
	}
	
	/**
	 * Get: situacaoAditivo.
	 *
	 * @return situacaoAditivo
	 */
	public String getSituacaoAditivo() {
		return situacaoAditivo;
	}
	
	/**
	 * Set: situacaoAditivo.
	 *
	 * @param situacaoAditivo the situacao aditivo
	 */
	public void setSituacaoAditivo(String situacaoAditivo) {
		this.situacaoAditivo = situacaoAditivo;
	}
	
	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}
	
	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: motivoAtivacaoLista.
	 *
	 * @return motivoAtivacaoLista
	 */
	public List<SelectItem> getMotivoAtivacaoLista() {
		return motivoAtivacaoLista;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Set: motivoAtivacaoLista.
	 *
	 * @param motivoAtivacaoLista the motivo ativacao lista
	 */
	public void setMotivoAtivacaoLista(List<SelectItem> motivoAtivacaoLista) {
		this.motivoAtivacaoLista = motivoAtivacaoLista;
	}

	/**
	 * Get: motivoAtivacao.
	 *
	 * @return motivoAtivacao
	 */
	public String getMotivoAtivacao() {
		return motivoAtivacao;
	}

	/**
	 * Set: motivoAtivacao.
	 *
	 * @param motivoAtivacao the motivo ativacao
	 */
	public void setMotivoAtivacao(String motivoAtivacao) {
		this.motivoAtivacao = motivoAtivacao;
	}

	/**
	 * Get: hiddenObrigatoriedade2.
	 *
	 * @return hiddenObrigatoriedade2
	 */
	public String getHiddenObrigatoriedade2() {
		return hiddenObrigatoriedade2;
	}

	/**
	 * Set: hiddenObrigatoriedade2.
	 *
	 * @param hiddenObrigatoriedade2 the hidden obrigatoriedade2
	 */
	public void setHiddenObrigatoriedade2(String hiddenObrigatoriedade2) {
		this.hiddenObrigatoriedade2 = hiddenObrigatoriedade2;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public String getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(String cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Get: cdTipo.
	 *
	 * @return cdTipo
	 */
	public String getCdTipo() {
		return cdTipo;
	}

	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(String cdTipo) {
		this.cdTipo = cdTipo;
	}
	
	/**
	 * Get: motivoAtivacaoListaHash.
	 *
	 * @return motivoAtivacaoListaHash
	 */
	public Map<Integer, String> getMotivoAtivacaoListaHash() {
		return motivoAtivacaoListaHash;
	}

	/**
	 * Set motivo ativacao lista hash.
	 *
	 * @param motivoAtivacaoListaHash the motivo ativacao lista hash
	 */
	public void setMotivoAtivacaoListaHash(
			Map<Integer, String> motivoAtivacaoListaHash) {
		this.motivoAtivacaoListaHash = motivoAtivacaoListaHash;
	}

	/**
	 * Carrega lista motivo.
	 */
	public void carregaListaMotivo(){
		
		this.motivoAtivacaoLista = new ArrayList<SelectItem>();
		
		List<ListarMotivoSituacaoAditivoContSaidaDTO> saidaDTO = new ArrayList<ListarMotivoSituacaoAditivoContSaidaDTO>();
		ListarMotivoSituacaoAditivoContEntradaDTO entradaDTO = new ListarMotivoSituacaoAditivoContEntradaDTO();
		entradaDTO.setCdSituacao(3);
		saidaDTO = comboService.listarMotivoSituacaoAditivoContrato(entradaDTO);
		motivoAtivacaoListaHash.clear();
		
		for(ListarMotivoSituacaoAditivoContSaidaDTO combo : saidaDTO){
			motivoAtivacaoListaHash.put(combo.getCdMotivoSituacaoOperacao(), combo.getDsMotivoSituacaoOperacao());
			this.motivoAtivacaoLista.add(new SelectItem(combo.getCdMotivoSituacaoOperacao(), combo.getDsMotivoSituacaoOperacao()));
		}	
	}

	/**
	 * Get: ativarFormalizacaoManutencaoContratoServiceImpl.
	 *
	 * @return ativarFormalizacaoManutencaoContratoServiceImpl
	 */
	public IAtivarFormalizacaoManutencaoContratoService getAtivarFormalizacaoManutencaoContratoServiceImpl() {
		return ativarFormalizacaoManutencaoContratoServiceImpl;
	}

	/**
	 * Set: ativarFormalizacaoManutencaoContratoServiceImpl.
	 *
	 * @param ativarFormalizacaoManutencaoContratoServiceImpl the ativar formalizacao manutencao contrato service impl
	 */
	public void setAtivarFormalizacaoManutencaoContratoServiceImpl(
			IAtivarFormalizacaoManutencaoContratoService ativarFormalizacaoManutencaoContratoServiceImpl) {
		this.ativarFormalizacaoManutencaoContratoServiceImpl = ativarFormalizacaoManutencaoContratoServiceImpl;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ConsultarManterFormAltContratoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ConsultarManterFormAltContratoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: manterFormalizacaoAlteracaoContratoService.
	 *
	 * @return manterFormalizacaoAlteracaoContratoService
	 */
	public IManterformalizacaoalteracaocontratoService getManterFormalizacaoAlteracaoContratoService() {
		return manterFormalizacaoAlteracaoContratoService;
	}

	/**
	 * Set: manterFormalizacaoAlteracaoContratoService.
	 *
	 * @param manterFormalizacaoAlteracaoContratoService the manter formalizacao alteracao contrato service
	 */
	public void setManterFormalizacaoAlteracaoContratoService(
			IManterformalizacaoalteracaocontratoService manterFormalizacaoAlteracaoContratoService) {
		this.manterFormalizacaoAlteracaoContratoService = manterFormalizacaoAlteracaoContratoService;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: gerenteResponsavel.
	 *
	 * @param gerenteResponsavel the gerente responsavel
	 */
	public void setGerenteResponsavel(String gerenteResponsavel) {
		this.dsGerenteResponsavel = gerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}
	
	
}
