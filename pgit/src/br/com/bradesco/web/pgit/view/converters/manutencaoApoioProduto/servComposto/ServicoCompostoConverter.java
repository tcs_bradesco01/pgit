/*
 * Nome: br.com.bradesco.web.pgit.view.converters.manutencaoApoioProduto.servComposto
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.converters.manutencaoApoioProduto.servComposto;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import br.com.bradesco.web.pgit.service.business.manterservicocomposto.bean.ListarServicoCompostoSaidaDTO;

/**
 * Nome: ServicoCompostoConverter
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ServicoCompostoConverter implements Converter {
	
	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		if (value == null || value.trim().equals("")) {
			return null;
		}
		
		String[] arrAgenda = value.split(";");
		if (arrAgenda == null || arrAgenda.length != 5) {
			return null;
		}

		ListarServicoCompostoSaidaDTO consulta = new ListarServicoCompostoSaidaDTO();
		consulta.setCdServicoCompostoPagamento(Long.valueOf(arrAgenda[0]));
		consulta.setCdTipoServicoCnab(Integer.parseInt(arrAgenda[1]));
		consulta.setCdFormaLancamentoCnab(Integer.parseInt(arrAgenda[2]));
		consulta.setCdFormaLiquidacao(Integer.parseInt(arrAgenda[3]));
		consulta.setDsServicoCompostoPagamento(arrAgenda[4]);
		
		return consulta;
	}

	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		if (value == null) {
			return null;
		}

		ListarServicoCompostoSaidaDTO consulta = (ListarServicoCompostoSaidaDTO) value;
		StringBuilder consultaIncAltExcServiComposto = new StringBuilder();
		consultaIncAltExcServiComposto.append(consulta.getCdServicoCompostoPagamento());
		consultaIncAltExcServiComposto.append(";");
		consultaIncAltExcServiComposto.append(consulta.getCdTipoServicoCnab());
		consultaIncAltExcServiComposto.append(";");
		consultaIncAltExcServiComposto.append(consulta.getCdFormaLancamentoCnab());
		consultaIncAltExcServiComposto.append(";");
		consultaIncAltExcServiComposto.append(consulta.getCdFormaLiquidacao());
		consultaIncAltExcServiComposto.append(";");
		consultaIncAltExcServiComposto.append(consulta.getDsServicoCompostoPagamento());
		
		return consultaIncAltExcServiComposto.toString();
	}

}