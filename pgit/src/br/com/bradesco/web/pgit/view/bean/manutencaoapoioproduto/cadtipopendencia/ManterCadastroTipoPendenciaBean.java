/*
, * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.cadtipopendencia
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.cadtipopendencia;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.log.ILogManager;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.ICadTipPendenciaService;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.ICadTipPendenciaServiceConstants;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.ConsultarDescricaoUnidOrganizacionalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoUnidadeOrganizacionalDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: ManterCadastroTipoPendenciaBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterCadastroTipoPendenciaBean {

    /** Atributo filtroCodigoTipoPendencia. */
    private String filtroCodigoTipoPendencia;

    /** Atributo filtroTipoUnidadeOrganizacional. */
    private String filtroTipoUnidadeOrganizacional;

    /** Atributo filtroTipoBaixa. */
    private String filtroTipoBaixa;

    /** Atributo filtroIndicadorResponsabilidade. */
    private String filtroIndicadorResponsabilidade;

    /** Atributo obrigatoriedade. */
    private String obrigatoriedade;

    /** Atributo codigo. */
    private String codigo;

    /** Atributo descricao. */
    private String descricao;

    /** Atributo observacoes. */
    private String observacoes;

    /** Atributo tipoUnidadeOrganizacional. */
    private String tipoUnidadeOrganizacional;

    /** Atributo tipoBaixa. */
    private String tipoBaixa;

    /** Atributo indicadorResponsabilidade. */
    private String indicadorResponsabilidade;

    /** Atributo tipoUnidadeOrganizacionalDesc. */
    private String tipoUnidadeOrganizacionalDesc;

    /** Atributo unidadeOrganizacionalDesc. */
    private String unidadeOrganizacionalDesc;

    /** Atributo logger. */
    private ILogManager logger;

    /** Atributo manterCadastroTipoPendenciaImpl. */
    private ICadTipPendenciaService manterCadastroTipoPendenciaImpl;

    /** Atributo listaPesquisa. */
    private List<TipoPendenciaSaidaDTO> listaPesquisa;

    /** Atributo itemSelecionadoLista. */
    private TipoPendenciaSaidaDTO itemSelecionadoLista;

    /** Atributo mostraBotoes. */
    private boolean mostraBotoes;

    /** Atributo dataHoraManutencao. */
    private String dataHoraManutencao;

    /** Atributo usuarioManutencao. */
    private String usuarioManutencao;

    /** Atributo tipoCanalManutencao. */
    private String tipoCanalManutencao;

    /** Atributo complementoManutencao. */
    private String complementoManutencao;

    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao;

    /** Atributo usuarioInclusao. */
    private String usuarioInclusao;

    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;

    /** Atributo complementoInclusao. */
    private String complementoInclusao;

    /** Atributo listaPesquisaControle. */
    private List<SelectItem> listaPesquisaControle = new ArrayList<SelectItem>();

    /** Atributo listaTipoUnidadeOrganizacional. */
    private List<SelectItem> listaTipoUnidadeOrganizacional = new ArrayList<SelectItem>();

    /** Atributo comboService. */
    private IComboService comboService;

    /** Atributo cadTipPendenciaService. */
    private ICadTipPendenciaService cadTipPendenciaService;

    /** Atributo filtroEmpresaConglomerado. */
    private String filtroEmpresaConglomerado;

    /** Atributo listaEmpresaConglomerado. */
    private List<SelectItem> listaEmpresaConglomerado = new ArrayList<SelectItem>();

    /** Atributo filtroUnidadeOrganizacional. */
    private String filtroUnidadeOrganizacional;

    /** Atributo empresaConglomerado. */
    private String empresaConglomerado;

    /** Atributo empresaConglomeradoDesc. */
    private String empresaConglomeradoDesc;

    /** Atributo unidadeOrganizacional. */
    private String unidadeOrganizacional;
    
    /** Atributo email. */
    private String email;

    /** Atributo descUnidadeOrganizacional. */
    private String descUnidadeOrganizacional;

    /** Atributo codPessoaJuridica. */
    private String codPessoaJuridica;

    /** Atributo hiddenConfirma. */
    private boolean hiddenConfirma;

    /** Atributo btoAcionado. */
    private Boolean btoAcionado;

    /**
     * Iniciar tela.
     *
     * @param evt the evt
     */
    public void iniciarTela(ActionEvent evt) {

	setFiltroCodigoTipoPendencia("");
	setFiltroTipoUnidadeOrganizacional("");
	setFiltroTipoBaixa("0");
	setFiltroIndicadorResponsabilidade("0");
	setListaPesquisa(null);
	listaEmpresaConglomerado = new ArrayList<SelectItem>();
	listaTipoUnidadeOrganizacional = new ArrayList<SelectItem>();

	setFiltroUnidadeOrganizacional("");
	setObrigatoriedade("F");
	preencheListaEmpresaConglomerado();
	preencheListaTipoUnidadeOrganizacional();
	setFiltroEmpresaConglomerado("0");
	setBtoAcionado(false);

    }

    /**
     * Limpa conglomerado.
     */
    public void limpaConglomerado() {
	if (!getFiltroIndicadorResponsabilidade().equals("2")) {
	    setEmpresaConglomerado("");
	    setUnidadeOrganizacional("");
	    setFiltroEmpresaConglomerado("0");
	    setFiltroTipoUnidadeOrganizacional("");
	    setFiltroUnidadeOrganizacional("");
	} else {
	    setEmpresaConglomerado(this.empresaConglomerado);
	    setUnidadeOrganizacional(this.unidadeOrganizacional);
	}
    }

    /**
     * Limpa conglomerado alterar.
     */
    public void limpaConglomeradoAlterar() {
	if (!getIndicadorResponsabilidade().equals("2")) {
	    setEmpresaConglomerado("2269651");
	    setUnidadeOrganizacional("");
	    setTipoUnidadeOrganizacional("");
	} else {
	    setEmpresaConglomerado("2269651");
	    setUnidadeOrganizacional(this.unidadeOrganizacional);
	    setTipoUnidadeOrganizacional(this.tipoUnidadeOrganizacional);
	}
    }

    /**
     * Get: complementoInclusao.
     *
     * @return complementoInclusao
     */
    public String getComplementoInclusao() {
	return complementoInclusao;
    }

    /**
     * Set: complementoInclusao.
     *
     * @param complementoInclusao the complemento inclusao
     */
    public void setComplementoInclusao(String complementoInclusao) {
	this.complementoInclusao = complementoInclusao;
    }

    /**
     * Get: complementoManutencao.
     *
     * @return complementoManutencao
     */
    public String getComplementoManutencao() {
	return complementoManutencao;
    }

    /**
     * Set: complementoManutencao.
     *
     * @param complementoManutencao the complemento manutencao
     */
    public void setComplementoManutencao(String complementoManutencao) {
	this.complementoManutencao = complementoManutencao;
    }

    /**
     * Get: dataHoraInclusao.
     *
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
	return dataHoraInclusao;
    }

    /**
     * Set: dataHoraInclusao.
     *
     * @param dataHoraInclusao the data hora inclusao
     */
    public void setDataHoraInclusao(String dataHoraInclusao) {
	this.dataHoraInclusao = dataHoraInclusao;
    }

    /**
     * Get: tipoCanalInclusao.
     *
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
	return tipoCanalInclusao;
    }

    /**
     * Set: tipoCanalInclusao.
     *
     * @param tipoCanalInclusao the tipo canal inclusao
     */
    public void setTipoCanalInclusao(String tipoCanalInclusao) {
	this.tipoCanalInclusao = tipoCanalInclusao;
    }

    /**
     * Get: tipoCanalManutencao.
     *
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
	return tipoCanalManutencao;
    }

    /**
     * Set: tipoCanalManutencao.
     *
     * @param tipoCanalManutencao the tipo canal manutencao
     */
    public void setTipoCanalManutencao(String tipoCanalManutencao) {
	this.tipoCanalManutencao = tipoCanalManutencao;
    }

    /**
     * Get: usuarioInclusao.
     *
     * @return usuarioInclusao
     */
    public String getUsuarioInclusao() {
	return usuarioInclusao;
    }

    /**
     * Set: usuarioInclusao.
     *
     * @param usuarioInclusao the usuario inclusao
     */
    public void setUsuarioInclusao(String usuarioInclusao) {
	this.usuarioInclusao = usuarioInclusao;
    }

    /**
     * Get: usuarioManutencao.
     *
     * @return usuarioManutencao
     */
    public String getUsuarioManutencao() {
	return usuarioManutencao;
    }

    /**
     * Set: usuarioManutencao.
     *
     * @param usuarioManutencao the usuario manutencao
     */
    public void setUsuarioManutencao(String usuarioManutencao) {
	this.usuarioManutencao = usuarioManutencao;
    }

    /**
     * Get: filtroCodigoTipoPendencia.
     *
     * @return filtroCodigoTipoPendencia
     */
    public String getFiltroCodigoTipoPendencia() {
	return filtroCodigoTipoPendencia;
    }

    /**
     * Set: filtroCodigoTipoPendencia.
     *
     * @param filtroCodigoTipoPendencia the filtro codigo tipo pendencia
     */
    public void setFiltroCodigoTipoPendencia(String filtroCodigoTipoPendencia) {
	this.filtroCodigoTipoPendencia = filtroCodigoTipoPendencia;
    }

    /**
     * Get: filtroTipoUnidadeOrganizacional.
     *
     * @return filtroTipoUnidadeOrganizacional
     */
    public String getFiltroTipoUnidadeOrganizacional() {
	return filtroTipoUnidadeOrganizacional;
    }

    /**
     * Set: filtroTipoUnidadeOrganizacional.
     *
     * @param filtroTipoUnidadeOrganizacional the filtro tipo unidade organizacional
     */
    public void setFiltroTipoUnidadeOrganizacional(String filtroTipoUnidadeOrganizacional) {
	this.filtroTipoUnidadeOrganizacional = filtroTipoUnidadeOrganizacional;
    }

    /**
     * Get: manterCadastroTipoPendenciaImpl.
     *
     * @return manterCadastroTipoPendenciaImpl
     */
    public ICadTipPendenciaService getManterCadastroTipoPendenciaImpl() {
	return manterCadastroTipoPendenciaImpl;
    }

    /**
     * Set: manterCadastroTipoPendenciaImpl.
     *
     * @param manterCadastroTipoPendenciaImpl the manter cadastro tipo pendencia impl
     */
    public void setManterCadastroTipoPendenciaImpl(ICadTipPendenciaService manterCadastroTipoPendenciaImpl) {
	this.manterCadastroTipoPendenciaImpl = manterCadastroTipoPendenciaImpl;
    }

    /**
     * Get: codigo.
     *
     * @return codigo
     */
    public String getCodigo() {
	return codigo;
    }

    /**
     * Set: codigo.
     *
     * @param codigo the codigo
     */
    public void setCodigo(String codigo) {
	this.codigo = codigo;
    }

    /**
     * Get: descricao.
     *
     * @return descricao
     */
    public String getDescricao() {
	return descricao;
    }

    /**
     * Set: descricao.
     *
     * @param descricao the descricao
     */
    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }

    /**
     * Get: indicadorResponsabilidade.
     *
     * @return indicadorResponsabilidade
     */
    public String getIndicadorResponsabilidade() {
	return indicadorResponsabilidade;
    }

    /**
     * Set: indicadorResponsabilidade.
     *
     * @param indicadorResponsabilidade the indicador responsabilidade
     */
    public void setIndicadorResponsabilidade(String indicadorResponsabilidade) {
	this.indicadorResponsabilidade = indicadorResponsabilidade;
    }

    /**
     * Get: observacoes.
     *
     * @return observacoes
     */
    public String getObservacoes() {
	return observacoes;
    }

    /**
     * Set: observacoes.
     *
     * @param observacoes the observacoes
     */
    public void setObservacoes(String observacoes) {
	this.observacoes = observacoes;
    }

    /**
     * Get: tipoUnidadeOrganizacional.
     *
     * @return tipoUnidadeOrganizacional
     */
    public String getTipoUnidadeOrganizacional() {
	return tipoUnidadeOrganizacional;
    }

    /**
     * Set: tipoUnidadeOrganizacional.
     *
     * @param tipoUnidadeOrganizacional the tipo unidade organizacional
     */
    public void setTipoUnidadeOrganizacional(String tipoUnidadeOrganizacional) {
	this.tipoUnidadeOrganizacional = tipoUnidadeOrganizacional;
    }

    /**
     * Get: unidadeOrganizacional.
     *
     * @return unidadeOrganizacional
     */
    public String getUnidadeOrganizacional() {
	return unidadeOrganizacional;
    }

    /**
     * Get: dataHoraManutencao.
     *
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
	return dataHoraManutencao;
    }

    /**
     * Set: dataHoraManutencao.
     *
     * @param dataHoraManutencao the data hora manutencao
     */
    public void setDataHoraManutencao(String dataHoraManutencao) {
	this.dataHoraManutencao = dataHoraManutencao;
    }

    /**
     * Set: unidadeOrganizacional.
     *
     * @param unidadeOrganizacional the unidade organizacional
     */
    public void setUnidadeOrganizacional(String unidadeOrganizacional) {
	this.unidadeOrganizacional = unidadeOrganizacional;
    }

    /**
     * Get: tipoBaixa.
     *
     * @return tipoBaixa
     */
    public String getTipoBaixa() {
	return tipoBaixa;
    }

    /**
     * Set: tipoBaixa.
     *
     * @param tipoBaixa the tipo baixa
     */
    public void setTipoBaixa(String tipoBaixa) {
	this.tipoBaixa = tipoBaixa;
    }

    /**
     * Get: listaPesquisa.
     *
     * @return listaPesquisa
     */
    public List<TipoPendenciaSaidaDTO> getListaPesquisa() {
	return listaPesquisa;
    }

    /**
     * Set: listaPesquisa.
     *
     * @param listaPesquisa the lista pesquisa
     */
    public void setListaPesquisa(List<TipoPendenciaSaidaDTO> listaPesquisa) {
	this.listaPesquisa = listaPesquisa;
    }

    /**
     * Is mostra botoes.
     *
     * @return true, if is mostra botoes
     */
    public boolean isMostraBotoes() {
	return mostraBotoes;
    }

    /**
     * Set: mostraBotoes.
     *
     * @param mostraBotoes the mostra botoes
     */
    public void setMostraBotoes(boolean mostraBotoes) {
	this.mostraBotoes = mostraBotoes;
    }

    /**
     * Get: listaPesquisaControle.
     *
     * @return listaPesquisaControle
     */
    public List<SelectItem> getListaPesquisaControle() {
	return listaPesquisaControle;
    }

    /**
     * Set: listaPesquisaControle.
     *
     * @param listaPesquisaControle the lista pesquisa controle
     */
    public void setListaPesquisaControle(List<SelectItem> listaPesquisaControle) {
	this.listaPesquisaControle = listaPesquisaControle;
    }

    /**
     * Limpar campos.
     *
     * @return the string
     */
    public String limparCampos() {

	this.setFiltroCodigoTipoPendencia("");
	this.setFiltroIndicadorResponsabilidade("0");
	this.setFiltroTipoBaixa("0");
	setFiltroUnidadeOrganizacional("");
	setFiltroEmpresaConglomerado("0");
	this.setFiltroTipoUnidadeOrganizacional("");
	listaPesquisa = new ArrayList<TipoPendenciaSaidaDTO>();
	setBtoAcionado(false);
	return "CONSULTAR";
    }

    /**
     * Avancar alterar.
     *
     * @return the string
     */
    public String avancarAlterar() {

	TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO = new TipoPendenciaSaidaDTO();
	TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO = new TipoPendenciaEntradaDTO();
	tipoPendenciaEntradaDTO.setCdTipoPendenciaPagamentoIntegrado(itemSelecionadoLista.getCdPendenciaPagamentoIntegrado());
	tipoPendenciaEntradaDTO.setCdTipoUnidadeOrganizacional(itemSelecionadoLista.getCdTipoUnidadeOrganizacional());

	tipoPendenciaSaidaDTO = getCadTipPendenciaService().detalharTipoPendencia(tipoPendenciaEntradaDTO);
	preencheDados(tipoPendenciaSaidaDTO);

	return "AVANCAR_ALTERAR";
    }

    /**
     * Avancar excluir.
     *
     * @return the string
     */
    public String avancarExcluir() {
	TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO = new TipoPendenciaSaidaDTO();
	TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO = new TipoPendenciaEntradaDTO();
	tipoPendenciaEntradaDTO.setCdTipoPendenciaPagamentoIntegrado(itemSelecionadoLista.getCdPendenciaPagamentoIntegrado());
	tipoPendenciaEntradaDTO.setCdTipoUnidadeOrganizacional(itemSelecionadoLista.getCdTipoUnidadeOrganizacional());

	tipoPendenciaSaidaDTO = getCadTipPendenciaService().detalharTipoPendencia(tipoPendenciaEntradaDTO);

	preencheDados(tipoPendenciaSaidaDTO);

	return "AVANCAR_EXCLUIR";
    }

    /**
     * Voltar pesquisar.
     *
     * @return the string
     */
    public String voltarPesquisar() {
	setItemSelecionadoLista(null);
	return "VOLTAR_CONSULTAR";
    }

    /**
     * Avancar detalhar.
     *
     * @return the string
     */
    public String avancarDetalhar() {
	TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO = new TipoPendenciaSaidaDTO();
	TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO = new TipoPendenciaEntradaDTO();
	tipoPendenciaEntradaDTO.setCdTipoPendenciaPagamentoIntegrado(itemSelecionadoLista.getCdPendenciaPagamentoIntegrado());
	tipoPendenciaEntradaDTO.setCdTipoUnidadeOrganizacional(itemSelecionadoLista.getCdTipoUnidadeOrganizacional());

	tipoPendenciaSaidaDTO = getCadTipPendenciaService().detalharTipoPendencia(tipoPendenciaEntradaDTO);

	preencheDados(tipoPendenciaSaidaDTO);

	return "AVANCAR_DETALHAR";
    }

    /**
     * Preenche dados.
     *
     * @param tipoPendenciaSaidaDTO the tipo pendencia saida dto
     */
    private void preencheDados(TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO) {

	setCodigo(String.valueOf(tipoPendenciaSaidaDTO.getCdPendenciaPagamentoIntegrado()));
	setDescricao(tipoPendenciaSaidaDTO.getDsPendenciaPagamentoIntegrado());
	setObservacoes(tipoPendenciaSaidaDTO.getDsResumoPendenciaPagamento());
	setTipoUnidadeOrganizacional(String.valueOf(tipoPendenciaSaidaDTO.getCdTipoUnidadeOrganizacional()));
	setTipoUnidadeOrganizacionalDesc(tipoPendenciaSaidaDTO.getDsTipoUnidadeOrganizacional());
	setEmpresaConglomeradoDesc(tipoPendenciaSaidaDTO.getDsCodigoPessoaJuridica());
	setUnidadeOrganizacional(tipoPendenciaSaidaDTO.getNrSequenciaUnidadeOrganizacional() == 0 ? "" : String.valueOf(tipoPendenciaSaidaDTO
		.getNrSequenciaUnidadeOrganizacional()));
	setUnidadeOrganizacionalDesc(tipoPendenciaSaidaDTO.getDsNumeroSequenciaUnidadeOrganizacional());
	setEmail(tipoPendenciaSaidaDTO.getDsEmailRespPgto());
	setTipoBaixa(String.valueOf(tipoPendenciaSaidaDTO.getCdTipoBaixaPendencia()));
	setIndicadorResponsabilidade(String.valueOf(tipoPendenciaSaidaDTO.getCdIndicadorResponsavelPagamento()));
	setEmpresaConglomerado(String.valueOf(tipoPendenciaSaidaDTO.getCdPessoaJuridica()));

	setDataHoraInclusao(tipoPendenciaSaidaDTO.getHrInclusaoRegistro());
	setUsuarioInclusao(tipoPendenciaSaidaDTO.getCdAutenticacaoSegurancaInclusao());
	setTipoCanalInclusao(tipoPendenciaSaidaDTO.getCdCanalInclusao() + " - " + tipoPendenciaSaidaDTO.getDsCanalInclusao());
	setComplementoInclusao(tipoPendenciaSaidaDTO.getNrOperacaoFluxoInclusao().equals("0") ? "" : tipoPendenciaSaidaDTO
		.getNrOperacaoFluxoInclusao());

	setDataHoraManutencao(tipoPendenciaSaidaDTO.getHrManutencaoRegistroManutencao());
	setUsuarioManutencao(tipoPendenciaSaidaDTO.getCdAutenticacaoSegurancaManutencao());
	setTipoCanalManutencao(String.valueOf(tipoPendenciaSaidaDTO.getCdCanalManutencao()).equals("0") ? "" : tipoPendenciaSaidaDTO
		.getCdCanalManutencao()
		+ " - " + tipoPendenciaSaidaDTO.getDsCanalManutencao());
	setComplementoManutencao(tipoPendenciaSaidaDTO.getNrOperacaoFluxoManutencao());

    }

    /**
     * Voltar incluir.
     *
     * @return the string
     */
    public String voltarIncluir() {
	return "VOLTAR_INCLUIR";
    }

    /**
     * Avancar incluir confirmar.
     *
     * @return the string
     */
    public String avancarIncluirConfirmar() {

	if (this.getObrigatoriedade().equals("T")) {

	    if (this.getUnidadeOrganizacional() != null && !this.getUnidadeOrganizacional().equals("")) {
		ConsultarDescricaoUnidOrganizacionalSaidaDTO descUnidOrgSaidaDTO = new ConsultarDescricaoUnidOrganizacionalSaidaDTO();
		ConsultarDescricaoUnidOrganizacionalEntradaDTO descUnidOrgEntradaDTO = new ConsultarDescricaoUnidOrganizacionalEntradaDTO();
		descUnidOrgEntradaDTO.setCdPessoaJuridica(Long.valueOf(this.getEmpresaConglomerado()));
		descUnidOrgEntradaDTO.setCdTipoUnidadeOrganizacional(Integer.parseInt(this.getTipoUnidadeOrganizacional()));
		descUnidOrgEntradaDTO.setNrSequenciaUnidadeOrganizacional(Integer.parseInt(this.getUnidadeOrganizacional()));
		descUnidOrgSaidaDTO = getCadTipPendenciaService().descricaoUnidadeOrganizacional(descUnidOrgEntradaDTO);
		setDescUnidadeOrganizacional(descUnidOrgSaidaDTO.getDsNumeroSeqUnidadeOrganizacional());
	    }

	    preencherTipoUnidadeOrganizacionalDesc();
	    preencherEmpresaConglomeradoDesc();

	    return "AVANCAR_INCLUIR2";
	}

	return "ok";
    }

    /**
     * Preencher tipo unidade organizacional desc.
     */
    private void preencherTipoUnidadeOrganizacionalDesc() {
	setTipoUnidadeOrganizacionalDesc("");
	SelectItem selectItemTipoUnidadeOrganizacional;

	for (int j = 0; j < getListaTipoUnidadeOrganizacional().size(); j++) {

	    selectItemTipoUnidadeOrganizacional = getListaTipoUnidadeOrganizacional().get(j);

	    if (getTipoUnidadeOrganizacional().equals(selectItemTipoUnidadeOrganizacional.getValue())) {
		setTipoUnidadeOrganizacionalDesc(selectItemTipoUnidadeOrganizacional.getLabel());
		break;

	    }
	}

    }

    /**
     * Preencher empresa conglomerado desc.
     */
    private void preencherEmpresaConglomeradoDesc() {
	setEmpresaConglomeradoDesc("");
	SelectItem selectItemEmpresaConglomerado;

	for (int j = 0; j < getListaEmpresaConglomerado().size(); j++) {

	    selectItemEmpresaConglomerado = getListaEmpresaConglomerado().get(j);

	    if (getEmpresaConglomerado().equals(selectItemEmpresaConglomerado.getValue())) {
		setEmpresaConglomeradoDesc(selectItemEmpresaConglomerado.getLabel());
		break;

	    }
	}

    }

    /**
     * Avancar incluir.
     *
     * @return the string
     */
    public String avancarIncluir() {

	setObrigatoriedade("");
	setCodigo("");
	setDescricao("");
	setObservacoes("");
	setTipoUnidadeOrganizacional("");
	setTipoBaixa("");
	setIndicadorResponsabilidade("");
	setTipoUnidadeOrganizacional("");
	setUnidadeOrganizacional("");
	setEmail("");
	setEmpresaConglomerado("2269651");
	
	return "AVANCAR_INCLUIR";
    }

    /**
     * Avancar alterar confirmar.
     *
     * @return the string
     */
    public String avancarAlterarConfirmar() {
	if (this.getObrigatoriedade().equals("T")) {	    
        	setDescUnidadeOrganizacional(null);
        	if (this.getUnidadeOrganizacional() != null && !this.getUnidadeOrganizacional().equals("")) {
        	    ConsultarDescricaoUnidOrganizacionalSaidaDTO descUnidOrgSaidaDTO = new ConsultarDescricaoUnidOrganizacionalSaidaDTO();
        	    ConsultarDescricaoUnidOrganizacionalEntradaDTO descUnidOrgEntradaDTO = new ConsultarDescricaoUnidOrganizacionalEntradaDTO();
        	    descUnidOrgEntradaDTO.setCdPessoaJuridica(Long.valueOf(this.getEmpresaConglomerado()));
        	    descUnidOrgEntradaDTO.setCdTipoUnidadeOrganizacional(Integer.parseInt(this.getTipoUnidadeOrganizacional()));
        	    descUnidOrgEntradaDTO.setNrSequenciaUnidadeOrganizacional(Integer.parseInt(this.getUnidadeOrganizacional()));
        	    descUnidOrgSaidaDTO = getCadTipPendenciaService().descricaoUnidadeOrganizacional(descUnidOrgEntradaDTO);
        	    setDescUnidadeOrganizacional(descUnidOrgSaidaDTO.getDsNumeroSeqUnidadeOrganizacional());
        	}
                
                preencherTipoUnidadeOrganizacionalDesc();
                preencherEmpresaConglomeradoDesc();
                return "AVANCAR_ALTERAR2";
	}
	return "ok";
    }

    /**
     * Voltar alterar.
     *
     * @return the string
     */
    public String voltarAlterar() {
	return "VOLTAR_ALTERAR";
    }

    /**
     * Get: obrigatoriedade.
     *
     * @return obrigatoriedade
     */
    public String getObrigatoriedade() {
	return obrigatoriedade;
    }

    /**
     * Set: obrigatoriedade.
     *
     * @param obrigatoriedade the obrigatoriedade
     */
    public void setObrigatoriedade(String obrigatoriedade) {
	this.obrigatoriedade = obrigatoriedade;
    }

    /**
     * Get: logger.
     *
     * @return logger
     */
    public ILogManager getLogger() {
	return logger;
    }

    /**
     * Set: logger.
     *
     * @param logger the logger
     */
    public void setLogger(ILogManager logger) {
	this.logger = logger;
    }

    /**
     * Get: filtroIndicadorResponsabilidade.
     *
     * @return filtroIndicadorResponsabilidade
     */
    public String getFiltroIndicadorResponsabilidade() {
	return filtroIndicadorResponsabilidade;
    }

    /**
     * Set: filtroIndicadorResponsabilidade.
     *
     * @param filtroIndicadorResponsabilidade the filtro indicador responsabilidade
     */
    public void setFiltroIndicadorResponsabilidade(String filtroIndicadorResponsabilidade) {
	this.filtroIndicadorResponsabilidade = filtroIndicadorResponsabilidade;
    }

    /**
     * Get: filtroTipoBaixa.
     *
     * @return filtroTipoBaixa
     */
    public String getFiltroTipoBaixa() {
	return filtroTipoBaixa;
    }

    /**
     * Set: filtroTipoBaixa.
     *
     * @param filtroTipoBaixa the filtro tipo baixa
     */
    public void setFiltroTipoBaixa(String filtroTipoBaixa) {
	this.filtroTipoBaixa = filtroTipoBaixa;
    }

    /**
     * Get: unidadeOrganizacionalDesc.
     *
     * @return unidadeOrganizacionalDesc
     */
    public String getUnidadeOrganizacionalDesc() {
	return unidadeOrganizacionalDesc;
    }

    /**
     * Set: unidadeOrganizacionalDesc.
     *
     * @param inputTipoUnidadeOrganizacionalDesc the unidade organizacional desc
     */
    public void setUnidadeOrganizacionalDesc(String inputTipoUnidadeOrganizacionalDesc) {
	this.unidadeOrganizacionalDesc = inputTipoUnidadeOrganizacionalDesc;
    }

    /**
     * Carrega lista.
     */
    public void carregaLista() {

	listaPesquisa = new ArrayList<TipoPendenciaSaidaDTO>();
	TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO = new TipoPendenciaEntradaDTO();
	tipoPendenciaEntradaDTO.setCdTipoPendenciaPagamentoIntegrado(getFiltroCodigoTipoPendencia() != null
		&& !getFiltroCodigoTipoPendencia().equals("") ? Long.parseLong(getFiltroCodigoTipoPendencia()) : 0);
	tipoPendenciaEntradaDTO.setCdTipoBaixaPendencia(getFiltroTipoBaixa() == null ? 0 : Integer.parseInt(getFiltroTipoBaixa()));
	tipoPendenciaEntradaDTO.setCdIndicadorResponsavelPagamento(getFiltroIndicadorResponsabilidade() == null ? 0 : Integer
		.parseInt(getFiltroIndicadorResponsabilidade()));
	tipoPendenciaEntradaDTO.setCdTipoUnidadeOrganizacional(getFiltroTipoUnidadeOrganizacional() != null
		&& !getFiltroTipoUnidadeOrganizacional().equals("") ? Integer.parseInt(getFiltroTipoUnidadeOrganizacional()) : 0);
	tipoPendenciaEntradaDTO.setCdPessoaJuridica(getFiltroEmpresaConglomerado() != null && !getFiltroEmpresaConglomerado().equals("") ? Long
		.parseLong(getFiltroEmpresaConglomerado()) : 0);
	tipoPendenciaEntradaDTO
		.setCdUnidadeOrganizacional(getFiltroUnidadeOrganizacional() != null && !getFiltroUnidadeOrganizacional().equals("") ? Integer
			.parseInt(getFiltroUnidadeOrganizacional()) : 0);
	tipoPendenciaEntradaDTO.setNumeroConsultas(ICadTipPendenciaServiceConstants.NUMERO_OCORRENCIAS);

	setListaPesquisa(getCadTipPendenciaService().listarTipoPendencia(tipoPendenciaEntradaDTO));
	setBtoAcionado(true);
    }

    /**
     * Carrega lista sucesso.
     */
    public void carregaListaSucesso() {
	try {
	    listaPesquisa = new ArrayList<TipoPendenciaSaidaDTO>();
	    TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO = new TipoPendenciaEntradaDTO();
	    tipoPendenciaEntradaDTO.setCdTipoPendenciaPagamentoIntegrado(getFiltroCodigoTipoPendencia() != null
		    && !getFiltroCodigoTipoPendencia().equals("") ? Long.parseLong(getFiltroCodigoTipoPendencia()) : 0);
	    tipoPendenciaEntradaDTO.setCdTipoUnidadeOrganizacional(getFiltroTipoUnidadeOrganizacional() != null
		    && !getFiltroTipoUnidadeOrganizacional().equals("") ? Integer.parseInt(getFiltroTipoUnidadeOrganizacional()) : 0);
	    tipoPendenciaEntradaDTO.setCdPessoaJuridica(getFiltroEmpresaConglomerado() != null && !getFiltroEmpresaConglomerado().equals("") ? Long
		    .parseLong(getFiltroEmpresaConglomerado()) : 0);
	    tipoPendenciaEntradaDTO.setCdUnidadeOrganizacional(getFiltroUnidadeOrganizacional() != null
		    && !getFiltroUnidadeOrganizacional().equals("") ? Integer.parseInt(getFiltroUnidadeOrganizacional()) : 0);
	    tipoPendenciaEntradaDTO.setCdTipoBaixaPendencia(getFiltroTipoBaixa() != null && !getFiltroTipoBaixa().equals("") ? Integer
		    .parseInt(getFiltroTipoBaixa()) : 0);
	    tipoPendenciaEntradaDTO.setCdIndicadorResponsavelPagamento(getFiltroIndicadorResponsabilidade() != null
		    && !getFiltroIndicadorResponsabilidade().equals("") ? Integer.parseInt(getFiltroIndicadorResponsabilidade()) : 0);
	    tipoPendenciaEntradaDTO.setNumeroConsultas(ICadTipPendenciaServiceConstants.NUMERO_OCORRENCIAS);

	    setListaPesquisa(getCadTipPendenciaService().listarTipoPendencia(tipoPendenciaEntradaDTO));

	} catch (PdcAdapterFunctionalException e) {
	    if (!StringUtils.right(e.getCode(), 8).equals("PGIT0003")) {
		this.setListaPesquisa(new ArrayList<TipoPendenciaSaidaDTO>());
		throw e;
	    }
	}
    }

    /**
     * Get: comboService.
     *
     * @return comboService
     */
    public IComboService getComboService() {
	return comboService;
    }

    /**
     * Set: comboService.
     *
     * @param comboService the combo service
     */
    public void setComboService(IComboService comboService) {
	this.comboService = comboService;
    }

    /**
     * Preenche lista tipo unidade organizacional.
     *
     * @return the list< select item>
     */
    public List<SelectItem> preencheListaTipoUnidadeOrganizacional() {
	try {
	    listaTipoUnidadeOrganizacional = new ArrayList<SelectItem>();
	    List<TipoUnidadeOrganizacionalDTO> listaTipoUnidadeOrganizacionalAux;

	    TipoUnidadeOrganizacionalDTO tipoUnidadeOrganizacionalDTO = new TipoUnidadeOrganizacionalDTO();
	    tipoUnidadeOrganizacionalDTO.setCdSituacaoVinculacaoConta(1);

	    listaTipoUnidadeOrganizacionalAux = comboService.listarTipoUnidadeOrganizacional(tipoUnidadeOrganizacionalDTO);

	    for (int i = 0; i < listaTipoUnidadeOrganizacionalAux.size(); i++) {
		this.listaTipoUnidadeOrganizacional.add(new SelectItem(String.valueOf(listaTipoUnidadeOrganizacionalAux.get(i).getCdTipoUnidade()),
			listaTipoUnidadeOrganizacionalAux.get(i).getDsTipoUnidade()));
	    }
	} catch (PdcAdapterFunctionalException e) {
	    listaTipoUnidadeOrganizacional = new ArrayList<SelectItem>();
	}

	return this.listaTipoUnidadeOrganizacional;
    }

    /**
     * Get: listaTipoUnidadeOrganizacional.
     *
     * @return listaTipoUnidadeOrganizacional
     */
    public List<SelectItem> getListaTipoUnidadeOrganizacional() {

	return this.listaTipoUnidadeOrganizacional;
    }

    /**
     * Preenche lista empresa conglomerado.
     *
     * @return the list< select item>
     */
    public List<SelectItem> preencheListaEmpresaConglomerado() {
	try {
	    listaEmpresaConglomerado = new ArrayList<SelectItem>();
	    List<EmpresaConglomeradoSaidaDTO> listaConglomerado;

	    EmpresaConglomeradoEntradaDTO tipoUnidadeOrganizacionalDTO = new EmpresaConglomeradoEntradaDTO();
	    tipoUnidadeOrganizacionalDTO.setNrOcorrencias(50);
	    tipoUnidadeOrganizacionalDTO.setCdSituacao(0);

	    listaConglomerado = comboService.listarEmpresaConglomerado(tipoUnidadeOrganizacionalDTO);

	    for (int i = 0; i < listaConglomerado.size(); i++) {
		this.listaEmpresaConglomerado.add(new SelectItem(String.valueOf(listaConglomerado.get(i).getCodClub()), PgitUtil.concatenarCampos(listaConglomerado.get(i).getCodClub(),  listaConglomerado.get(i)
			.getDsRazaoSocial())));
	    }
	} catch (PdcAdapterFunctionalException e) {
	    listaEmpresaConglomerado = new ArrayList<SelectItem>();
	}

	return this.listaEmpresaConglomerado;
    }

    /**
     * Get: listaEmpresaConglomerado.
     *
     * @return listaEmpresaConglomerado
     */
    public List<SelectItem> getListaEmpresaConglomerado() {

	return this.listaEmpresaConglomerado;
    }

    /**
     * Set: listaTipoUnidadeOrganizacional.
     *
     * @param listaTipoUnidadeOrganizacional the lista tipo unidade organizacional
     */
    public void setListaTipoUnidadeOrganizacional(List<SelectItem> listaTipoUnidadeOrganizacional) {
	this.listaTipoUnidadeOrganizacional = listaTipoUnidadeOrganizacional;
    }

    /**
     * Get: cadTipPendenciaService.
     *
     * @return cadTipPendenciaService
     */
    public ICadTipPendenciaService getCadTipPendenciaService() {
	return cadTipPendenciaService;
    }

    /**
     * Set: cadTipPendenciaService.
     *
     * @param cadTipPendenciaService the cad tip pendencia service
     */
    public void setCadTipPendenciaService(ICadTipPendenciaService cadTipPendenciaService) {
	this.cadTipPendenciaService = cadTipPendenciaService;
    }

    /**
     * Get: filtroEmpresaConglomerado.
     *
     * @return filtroEmpresaConglomerado
     */
    public String getFiltroEmpresaConglomerado() {
	return filtroEmpresaConglomerado;
    }

    /**
     * Set: filtroEmpresaConglomerado.
     *
     * @param filtroEmpresaConglomerado the filtro empresa conglomerado
     */
    public void setFiltroEmpresaConglomerado(String filtroEmpresaConglomerado) {
	this.filtroEmpresaConglomerado = filtroEmpresaConglomerado;
    }

    /**
     * Set: listaEmpresaConglomerado.
     *
     * @param listaEmpresaConglomerado the lista empresa conglomerado
     */
    public void setListaEmpresaConglomerado(List<SelectItem> listaEmpresaConglomerado) {
	this.listaEmpresaConglomerado = listaEmpresaConglomerado;
    }

    /**
     * Get: filtroUnidadeOrganizacional.
     *
     * @return filtroUnidadeOrganizacional
     */
    public String getFiltroUnidadeOrganizacional() {
	return filtroUnidadeOrganizacional;
    }

    /**
     * Set: filtroUnidadeOrganizacional.
     *
     * @param filtroUnidadeOrganizacional the filtro unidade organizacional
     */
    public void setFiltroUnidadeOrganizacional(String filtroUnidadeOrganizacional) {
	this.filtroUnidadeOrganizacional = filtroUnidadeOrganizacional;
    }

    /**
     * Get: empresaConglomerado.
     *
     * @return empresaConglomerado
     */
    public String getEmpresaConglomerado() {
	return empresaConglomerado;
    }

    /**
     * Set: empresaConglomerado.
     *
     * @param empresaConglomerado the empresa conglomerado
     */
    public void setEmpresaConglomerado(String empresaConglomerado) {
	this.empresaConglomerado = empresaConglomerado;
    }

    /**
     * Get: tipoUnidadeOrganizacionalDesc.
     *
     * @return tipoUnidadeOrganizacionalDesc
     */
    public String getTipoUnidadeOrganizacionalDesc() {
	return tipoUnidadeOrganizacionalDesc;
    }

    /**
     * Set: tipoUnidadeOrganizacionalDesc.
     *
     * @param tipoUnidadeOrganizacionalDesc the tipo unidade organizacional desc
     */
    public void setTipoUnidadeOrganizacionalDesc(String tipoUnidadeOrganizacionalDesc) {
	this.tipoUnidadeOrganizacionalDesc = tipoUnidadeOrganizacionalDesc;
    }

    /**
     * Confirmar excluir.
     *
     * @return the string
     */
    public String confirmarExcluir() {

	TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO = new TipoPendenciaEntradaDTO();
	tipoPendenciaEntradaDTO.setCdTipoPendenciaPagamentoIntegrado(Long.parseLong(getCodigo()));
	tipoPendenciaEntradaDTO.setCdTipoUnidadeOrganizacional(getTipoUnidadeOrganizacional().equals("") ? 0 : Integer
		.parseInt(getTipoUnidadeOrganizacional()));

	TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO = getCadTipPendenciaService().excluirTipoPendencia(tipoPendenciaEntradaDTO);
	BradescoFacesUtils.addInfoModalMessage("(" + tipoPendenciaSaidaDTO.getCodMensagem() + ") " + tipoPendenciaSaidaDTO.getMensagem(),
		"conManterCadastroTipoPendencia", BradescoViewExceptionActionType.ACTION, false);

	carregaListaSucesso();
	return "CONFIRMAR_EXCLUIR";

    }

    /**
     * Get: hiddenConfirma.
     *
     * @return hiddenConfirma
     */
    public boolean getHiddenConfirma() {
	return hiddenConfirma;
    }

    /**
     * Set: hiddenConfirma.
     *
     * @param hiddenConfirma the hidden confirma
     */
    public void setHiddenConfirma(boolean hiddenConfirma) {
	this.hiddenConfirma = hiddenConfirma;
    }

    /**
     * Confirmar incluir.
     *
     * @return the string
     */
    public String confirmarIncluir() {

	TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO = new TipoPendenciaEntradaDTO();
	tipoPendenciaEntradaDTO.setCdTipoPendenciaPagamentoIntegrado(Long.parseLong(getCodigo()));
	tipoPendenciaEntradaDTO.setCdTipoUnidadeOrganizacional(getTipoUnidadeOrganizacional().equals("") ? 0 : Integer
		.parseInt(getTipoUnidadeOrganizacional()));
	tipoPendenciaEntradaDTO.setCdPessoaJuridica(getEmpresaConglomerado().equals("") ? Long.valueOf(0) : Long.parseLong(getEmpresaConglomerado()));
	tipoPendenciaEntradaDTO.setCdUnidadeOrganizacional(getUnidadeOrganizacional().equals("") ? 0 : Integer.parseInt(getUnidadeOrganizacional()));
	tipoPendenciaEntradaDTO.setDsEmailRespPgto(getEmail() == null ? " " : getEmail().equals("") ? " " : getEmail());
	
	tipoPendenciaEntradaDTO.setDsPendenciaPagamentoIntegrado(getDescricao());
	tipoPendenciaEntradaDTO.setRsResumoPendenciaPagamento(getObservacoes());
	tipoPendenciaEntradaDTO.setCdTipoBaixaPendencia(Integer.parseInt(getTipoBaixa()));
	tipoPendenciaEntradaDTO.setCdIndicadorResponsavelPagamento(Integer.parseInt(getIndicadorResponsabilidade()));

	TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO = getCadTipPendenciaService().incluirTipoPendencia(tipoPendenciaEntradaDTO);
	BradescoFacesUtils.addInfoModalMessage("(" + tipoPendenciaSaidaDTO.getCodMensagem() + ") " + tipoPendenciaSaidaDTO.getMensagem(),
		"conManterCadastroTipoPendencia", BradescoViewExceptionActionType.ACTION, false);
	carregaListaSucesso();
	return "CONFIRMAR_INCLUIR";

    }

    /**
     * Confirmar alterar.
     *
     * @return the string
     */
    public String confirmarAlterar() {

	TipoPendenciaEntradaDTO tipoPendenciaEntradaDTO = new TipoPendenciaEntradaDTO();
	tipoPendenciaEntradaDTO.setCdTipoPendenciaPagamentoIntegrado(getCodigo() == null ? Long.valueOf(0) : Long.parseLong(getCodigo()));
	tipoPendenciaEntradaDTO.setCdTipoUnidadeOrganizacional(getTipoUnidadeOrganizacional().equals("") ? 0 : Integer
		.parseInt(getTipoUnidadeOrganizacional()));
	tipoPendenciaEntradaDTO.setCdPessoaJuridica(getEmpresaConglomerado().equals("") ? Long.valueOf(0) : Long.parseLong(getEmpresaConglomerado()));
	tipoPendenciaEntradaDTO.setCdUnidadeOrganizacional(getUnidadeOrganizacional().equals("") ? 0 : Integer.parseInt(getUnidadeOrganizacional()));
	tipoPendenciaEntradaDTO.setDsEmailRespPgto(getEmail() == null ? " " : getEmail().equals("") ? " " : getEmail());
	tipoPendenciaEntradaDTO.setDsPendenciaPagamentoIntegrado(getDescricao());
	tipoPendenciaEntradaDTO.setRsResumoPendenciaPagamento(getObservacoes());
	tipoPendenciaEntradaDTO.setCdTipoBaixaPendencia(Integer.parseInt(getTipoBaixa()));
	tipoPendenciaEntradaDTO.setCdIndicadorResponsavelPagamento(Integer.parseInt(getIndicadorResponsabilidade()));

	TipoPendenciaSaidaDTO tipoPendenciaSaidaDTO = getCadTipPendenciaService().alterarTipoPendencia(tipoPendenciaEntradaDTO);
	BradescoFacesUtils.addInfoModalMessage("(" + tipoPendenciaSaidaDTO.getCodMensagem() + ") " + tipoPendenciaSaidaDTO.getMensagem(),
		"conManterCadastroTipoPendencia", BradescoViewExceptionActionType.ACTION, false);

	carregaListaSucesso();

	return "CONFIRMAR_ALTERAR";
    }

    /**
     * Pesquisar.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisar(ActionEvent evt) {
	return "";
    }

    /**
     * Get: itemSelecionadoLista.
     *
     * @return itemSelecionadoLista
     */
    public TipoPendenciaSaidaDTO getItemSelecionadoLista() {
	return itemSelecionadoLista;
    }

    /**
     * Set: itemSelecionadoLista.
     *
     * @param itemSelecionadoLista the item selecionado lista
     */
    public void setItemSelecionadoLista(TipoPendenciaSaidaDTO itemSelecionadoLista) {
	this.itemSelecionadoLista = itemSelecionadoLista;
    }

    /**
     * Get: codPessoaJuridica.
     *
     * @return codPessoaJuridica
     */
    public String getCodPessoaJuridica() {
	return codPessoaJuridica;
    }

    /**
     * Set: codPessoaJuridica.
     *
     * @param codPessoaJuridica the cod pessoa juridica
     */
    public void setCodPessoaJuridica(String codPessoaJuridica) {
	this.codPessoaJuridica = codPessoaJuridica;
    }

    /**
     * Get: empresaConglomeradoDesc.
     *
     * @return empresaConglomeradoDesc
     */
    public String getEmpresaConglomeradoDesc() {
	return empresaConglomeradoDesc;
    }

    /**
     * Set: empresaConglomeradoDesc.
     *
     * @param empresaConglomeradoDesc the empresa conglomerado desc
     */
    public void setEmpresaConglomeradoDesc(String empresaConglomeradoDesc) {
	this.empresaConglomeradoDesc = empresaConglomeradoDesc;
    }

    /**
     * Get: descUnidadeOrganizacional.
     *
     * @return descUnidadeOrganizacional
     */
    public String getDescUnidadeOrganizacional() {
	return descUnidadeOrganizacional;
    }

    /**
     * Set: descUnidadeOrganizacional.
     *
     * @param descUnidadeOrganizacional the desc unidade organizacional
     */
    public void setDescUnidadeOrganizacional(String descUnidadeOrganizacional) {
	this.descUnidadeOrganizacional = descUnidadeOrganizacional;
    }

    /**
     * Get: btoAcionado.
     *
     * @return btoAcionado
     */
    public Boolean getBtoAcionado() {
	return btoAcionado;
    }

    /**
     * Set: btoAcionado.
     *
     * @param btoAcionado the bto acionado
     */
    public void setBtoAcionado(Boolean btoAcionado) {
	this.btoAcionado = btoAcionado;
    }

	/**
	 * Get: email.
	 *
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Set: email.
	 *
	 * @param email the email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
    
}
