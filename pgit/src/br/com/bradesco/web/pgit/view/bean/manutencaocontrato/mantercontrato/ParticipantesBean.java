/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoBloqueioDesbEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoBloqueioDesbSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.IContaComplementarService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarClubesClientesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarParticipanteContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarParticipanteContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.BloquearDesbloquearContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.BloquearDesbloquearContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarManParticipanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarManParticipanteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirParticipanteContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ExcluirParticipanteContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirParticipanteContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.IncluirParticipanteContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasExclusaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasExclusaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasPossiveisInclusaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarContasPossiveisInclusaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ParticipantesDTO;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.view.converters.FormatarData;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

// TODO: Auto-generated Javadoc
/**
 * Nome: ParticipantesBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ParticipantesBean {

	// Constante Utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo PARTICIPANTES_MANTER_CONTRATO. */
	private static final String PARTICIPANTES_MANTER_CONTRATO = "conParticipantesManterContrato";

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;
	
	/** Atributo filtroIdentificaoImpl. */
	private IFiltroIdentificaoService filtroIdentificaoImpl;
	
	/** Atributo filtroIdentificaoImpl. */
	private IContaComplementarService filtroContaComplementarImpl;

	// Pesquisar
	/** Atributo listaGrid. */
	private List<ListarParticipantesSaidaDTO> listaGrid;
	
	/** Atributo listaGridControle. */
	private List<SelectItem> listaGridControle;
	
    /** Atributo itemSelecionadoGrid. */
    private Integer itemSelecionadoListaParticipantes;

	/** Atributo itemSelecionadoGrid. */
	private Integer itemSelecionadoGrid;

	/** Atributo cpfCnpjRepresentante. */
	private String cpfCnpjRepresentante;

	/** Atributo nomeRazaoRepresentante. */
	private String nomeRazaoRepresentante;

	/** Atributo grupEconRepresentante. */
	private String grupEconRepresentante;

	/** Atributo ativEconRepresentante. */
	private String ativEconRepresentante;

	/** Atributo segRepresentante. */
	private String segRepresentante;

	/** Atributo subSegRepresentante. */
	private String subSegRepresentante;

	/** Atributo empresaContrato. */
	private String empresaContrato;

	/** Atributo tipoContrato. */
	private String tipoContrato;

	/** Atributo numeroContrato. */
	private String numeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo situacaoContrato. */
	private String situacaoContrato;

	/** Atributo motivoContrato. */
	private String motivoContrato;

	/** Atributo participacaoContrato. */
	private String participacaoContrato;

	/** Atributo aditivosContrato. */
	private String aditivosContrato;

	/** Atributo dataHoraCadastramento. */
	private String dataHoraCadastramento;

	/** Atributo vigenciaContrato. */
	private String vigenciaContrato;

	/** Atributo tpManutencao. */
	private String tpManutencao;

	/** Atributo cdEmpresaContrato. */
	private Long cdEmpresaContrato;

	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;

	/** Atributo nrSequenciaContrato. */
	private String nrSequenciaContrato;

	// Fim Pesquisar

	// Incluir Excluir Detalhar
	/** Atributo cnpj. */
	private String cnpj;

	/** Atributo cnpj2. */
	private String cnpj2;

	/** Atributo cnpj3. */
	private String cnpj3;

	/** Atributo cpf. */
	private String cpf;

	/** Atributo cpf2. */
	private String cpf2;

	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;

	/** Atributo banco. */
	private String banco;

	/** Atributo agencia. */
	private String agencia;

	/** Atributo agenciaDig. */
	private String agenciaDig;

	/** Atributo conta. */
	private String conta;

	/** Atributo contaDig. */
	private String contaDig;

	/** Atributo itemSelecionado. */
	private String itemSelecionado;
	
	/** Atributo listaPesquisa. */
	private List<ListarClubesClientesSaidaDTO> listaPesquisa;
	
	/** Atributo listaSelecionados. */
	private List<ParticipantesDTO> listaSelecionados = new ArrayList<ParticipantesDTO>();

	/** Atributo listaPesquisaControle. */
	private List<SelectItem> listaPesquisaControle;
	
	/** Atributo listaSelecionadosControle. */
	private List<SelectItem> listaSelecionadosControle;

	/** Atributo itemSelecionadoPesquisa. */
	private Integer itemSelecionadoPesquisa;

	/** Atributo checkAll. */
	private boolean checkAll;
	
	/** Atributo checkAll. */
    private boolean checkAll2;

	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;

	/** Atributo tipoParticipacao. */
	private Integer tipoParticipacao;

	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;

	/** Atributo cpfCnpjParticipanteDet. */
	private String cpfCnpjParticipanteDet;

	/** Atributo nomeRazaoParticipanteDet. */
	private String nomeRazaoParticipanteDet;

	/** Atributo grupoEconParticipante. */
	private String grupoEconParticipante;

	/** Atributo atividadeEconParticipante. */
	private String atividadeEconParticipante;

	/** Atributo segmentoParticipante. */
	private String segmentoParticipante;

	/** Atributo subSegmentoParticipante. */
	private String subSegmentoParticipante;

	/** Atributo tipoParticipacaoDesc. */
	private String tipoParticipacaoDesc;

	/** Atributo situacaoParticipacao. */
	private String situacaoParticipacao;

	/** Atributo motivoSituacao. */
	private String motivoSituacao;

	/** Atributo areaNegocio. */
	private String areaNegocio;

	/** Atributo cdClassificacaoParticipante. */
	private Integer cdClassificacaoParticipante;

	/** Atributo dsClassificacaoParticipante. */
	private String dsClassificacaoParticipante;

	/** Atributo cdTipoParticipante. */
	private Integer cdTipoParticipante;

	/** Atributo cdPessoaParticipante. */
	private Long cdPessoaParticipante;

	/** Atributo btnSelecionar. */
	private boolean btnSelecionar;

	/** Atributo bloquearCampos. */
	private boolean bloquearCampos;

	// Fim Incluir Excluir Detalhar

	// VARIAVEIS TRILHA
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	// FIM VARIAVEIS TRILHA

	// Historico ---
	/** Atributo itemSelecionado2. */
	private String itemSelecionado2;

	/** Atributo cpfHist. */
	private String cpfHist;

	/** Atributo cpfHist2. */
	private String cpfHist2;

	/** Atributo cnpjHist. */
	private String cnpjHist;

	/** Atributo cnpjHist2. */
	private String cnpjHist2;

	/** Atributo cnpjHist3. */
	private String cnpjHist3;

	/** Atributo dataInicial. */
	private Date dataInicial;

	/** Atributo dataFinal. */
	private Date dataFinal;

	/** Atributo listaPesquisa2. */
	private List<ConsultarManParticipanteSaidaDTO> listaPesquisa2;

	/** Atributo listaPesquisaControle2. */
	private List<SelectItem> listaPesquisaControle2;

	/** Atributo itemSelecionadoPesquisa2. */
	private Integer itemSelecionadoPesquisa2;

	/* Bloquear/Desbloquear Participante */
	/** Atributo condicaoBloqueioDesb. */
	private Integer condicaoBloqueioDesb;

	/** Atributo motivoBloqueioDesbloqueio. */
	private Integer motivoBloqueioDesbloqueio;

	/** Atributo dsMotivoBloqueioDesbloqueio. */
	private String dsMotivoBloqueioDesbloqueio;

	/** Atributo listaMotivoBloqueioDesb. */
	private List<SelectItem> listaMotivoBloqueioDesb;

	/** Atributo listaMotivoBloqueioDesbHash. */
	private Map<Integer, String> listaMotivoBloqueioDesbHash = new HashMap<Integer, String>();
		   
    /** Atributo participantesSelecionado. */
    private ParticipantesDTO participantesSelecionado = null;
    
    /** Atributo bancoFiltroContasParticipantes. */
    private Integer bancoFiltroContasParticipantes;

    /** Atributo agenciaFiltroContasParticipantes. */
    private Integer agenciaFiltroContasParticipantes;

    /** Atributo contaFiltroContasParticipantes. */
    private Long contaFiltroContasParticipantes;
    
    /** Atributo bancoFiltroContasParticipantes. */
    private Integer cdBanco;

    /** Atributo agenciaFiltroContasParticipantes. */
    private Integer cdAgencia;

    /** Atributo contaFiltroContasParticipantes. */
    private Long cdConta;
    
    /** Atributo listaContasPossiveisInclusao. */
    List<ListarContasPossiveisInclusaoSaidaDTO> listaContasPossiveisInclusao;
    
    /** Atributo listaGridControle. */
    private List<SelectItem> listaContasPossiveisInclusaoControle;

    
    /** Atributo listaContasParaInclusao. */
    List<ListarContasPossiveisInclusaoSaidaDTO> listaContasParaInclusao;
    
    /** Atributo listaGridControle. */
    private List<SelectItem> listaContasParaInclusaoControle;
    

    /** Atributo habilitarBtnSelecionar. */
    private boolean habilitarBtnSelecionar;
    
    /** Atributo habilitarBtnRemover. */
    private boolean habilitarBtnRemover;
    
    /** Atributo saidaListarContasExclusao. */
    private ListarContasExclusaoSaidaDTO saidaListarContasExclusao;
    
    /** Atributo cdPessoaJuridica. */
    private Long cdPessoaJuridica;

	// Metodos
	/**
	 * Limpar argumentos pesquisa hist.
	 */
	public void limparArgumentosPesquisaHist() {
		setCpfHist("");
		setCpfHist2("");

		setCnpjHist("");
		setCnpjHist2("");
		setCnpjHist3("");

		setDataInicial(new Date());
		setDataFinal(new Date());

		setBloquearCampos(false);
	}

	/**
	 * Habilita botao selecionar.
	 */
	public void habilitaBotaoSelecionar() {
		int aux = 0;
		if (!isBtnSelecionar()) {
			setBtnSelecionar(true);
			return;
		} else {
			if (isBtnSelecionar()) {

				for (int i = 0; i < getListaPesquisa().size(); i++) {
					if (!getListaPesquisa().get(i).isCheck()) {
						aux++;
					}
				}
				if (aux == getListaPesquisa().size()) {
					setBtnSelecionar(false);
				}
				return;
			}
		}
	}

	/**
	 * Nome: voltar.
	 *
	 * @return the string
	 * @see
	 */
	public String voltar() {
		setItemSelecionadoGrid(null);
		setItemSelecionadoPesquisa2(null);		
		return "VOLTAR";
	}
	
	/**
	 * Checa todos.
	 */
	public void checaTodasContasPossiveisInclusao() {
		for (int i = 0; i < getListaContasPossiveisInclusao().size(); i++) {
		    getListaContasPossiveisInclusao().get(i).setCheck(isCheckAll());
		}
		setHabilitarBtnSelecionar(isCheckAll());
    }

    /**
     * Checa todas contas para inclusao.
     */
    public void checaTodasContasParaInclusao() {
        for (int i = 0; i < getListaContasParaInclusao().size(); i++) {
            getListaContasParaInclusao().get(i).setCheck(isCheckAll2());
        }
        setHabilitarBtnRemover(isCheckAll2());
    }

	/**
	 * Carrega lista historico.
	 */
	public void carregaListaHistorico() {
		listaPesquisa2 = new ArrayList<ConsultarManParticipanteSaidaDTO>();

		try {
			ConsultarManParticipanteEntradaDTO entrada = new ConsultarManParticipanteEntradaDTO();

			if (this.getItemSelecionado2().equals("0")) {
				entrada.setCdCorpoCpfCnpj(!getCpfHist().equals("") ? Long.parseLong(getCpfHist()) : 0l);
				entrada.setCdControleCpfCnpj(0);
				entrada.setCdDigitoCpfCnpj(!getCpfHist2().equals("") ? Integer.parseInt(getCpfHist2()) : 0);
			} else {
				if (this.getItemSelecionado2().equals("1")) {
					entrada.setCdCorpoCpfCnpj(!getCnpjHist().equals("") ? Long.parseLong(getCnpjHist()) : 0l);
					entrada.setCdControleCpfCnpj(!getCnpjHist2().equals("") ? Integer.parseInt(getCnpjHist2()) : 0);
					entrada.setCdDigitoCpfCnpj(!getCnpjHist3().equals("") ? Integer.parseInt(getCnpjHist3()) : 0);
				} else {
					entrada.setCdCorpoCpfCnpj(0l);
					entrada.setCdControleCpfCnpj(0);
					entrada.setCdDigitoCpfCnpj(0);
				}
			}
			entrada.setCdPessoaJuridica(getCdEmpresaContrato() != null ? getCdEmpresaContrato() : 0);
			entrada.setCdTipoContratoNegocio(getCdTipoContrato() != null ? getCdTipoContrato() : 0);
			entrada.setNrSeqContratoNegocio(!getNrSequenciaContrato().equals("") ? Long
							.parseLong(getNrSequenciaContrato()) : 0);

			entrada.setDtFim(getDataFinal() != null ? FormatarData.formataDiaMesAno(getDataFinal()) : "");
			entrada.setDtInicio(getDataInicial() != null ? FormatarData.formataDiaMesAno(getDataInicial()) : "");

			setListaPesquisa2(getManterContratoImpl().consultarManutencaoParticipante(entrada));

			listaPesquisaControle2 = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaPesquisa2().size(); i++) {
				listaPesquisaControle2.add(new SelectItem(i, ""));
			}
			setItemSelecionadoPesquisa2(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaPesquisa2(null);
			setItemSelecionadoPesquisa2(null);
		}
	}

	/**
	 * Limpar lista.
	 */
	public void limparLista() {
		setListaPesquisa2(new ArrayList<ConsultarManParticipanteSaidaDTO>());
	}
	
	public void limparPesquisaComplementar(){
		this.setCnpj(null);
		this.setCnpj2(null);
		this.setCnpj3(null);
		this.setCpf(null);
		this.setCpf2(null);
		this.setBanco("");
		this.setAgencia("");
		this.setConta("");
	}
	
	/**
	 * Limpar selecao lista.
	 */
	public void limparSelecaoLista() {
		setItemSelecionadoGrid(null);
	}

	/**
	 * Limpar dados historico.
	 */
	public void limparDadosHistorico() {
		setItemSelecionado2("");
		setItemSelecionadoGrid(null);
		setCpfHist("");
		setCpfHist2("");

		setCnpjHist("");
		setCnpjHist2("");
		setCnpjHist3("");

		setDataInicial(new Date());
		setDataFinal(new Date());

		setListaPesquisa2(null);
		setListaPesquisaControle2(null);
		setItemSelecionadoPesquisa2(null);
	}
	
	/**
	 * Detalhar historico.
	 *
	 * @return the string
	 */
	public String detalharHistorico() {
		try {
			preencheDadosHistorico();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							"hisParticipantesManterContrato", BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "DETALHAR";
	}

	/**
	 * Preenche dados historico.
	 */
	public void preencheDadosHistorico(){
		ConsultarConManParticipantesEntradaDTO entrada = new ConsultarConManParticipantesEntradaDTO();
		ConsultarManParticipanteSaidaDTO dto = listaPesquisa2.get(getItemSelecionadoPesquisa2());
		
		entrada.setCdDependenciaOperacao(0);		
		entrada.setCdTipoParticipacaoPessoa(dto.getCdTipoParticipacao());
		
		entrada.setCdPessoaJuridicaContrato(getCdEmpresaContrato());
		entrada.setCdTipoContratoNegocio(getCdTipoContrato());
		entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContrato()));
		

		entrada.setCdUsuario(dto.getCdUsuarioManutencao());	
		entrada.setHrInclusaoRegistroHistorico(dto.getHrInclusaoRegistro());
		
		entrada.setCdPessoaJuridicaParticipante(dto.getCdPessoa());
				
		ConsultarConManParticipantesSaidaDTO saida = getManterContratoImpl().consultarConManParticipantes(entrada);
		
		setCpfCnpjParticipanteDet(saida.getCpfFormatado());
		setNomeRazaoParticipanteDet(saida.getNome());
		setTipoParticipacaoDesc(saida.getDsTipoParticipacao());
		setSituacaoParticipacao(saida.getDsSituacao());
		setMotivoSituacao(saida.getDsMotivoSituacaoParticipante());
		setAreaNegocio(saida.getDsClassificacaoAreaParticipante());
		setTpManutencao(dto.getCdIndicadorTipoManutencao());
		
		setDataHoraManutencao(saida.getHrManutencaoRegistro());
		setUsuarioManutencao("".equals(saida.getCdUsuarioManutencao()) ? saida.getCdUsuarioManutencaoExterno() : saida.getCdUsuarioManutencao());
		setTipoCanalManutencao(saida.getCdCanalManutencao() == 0 ? "" : saida.getCdCanalManutencao() + " - " + saida.getDsCanalManutencao());
		setComplementoManutencao(saida.getNrOperacaoFluxoManutencao().equals("0")?"":saida.getNrOperacaoFluxoManutencao());
		
		setDataHoraInclusao(saida.getHrInclusaoRegistro());
		setUsuarioInclusao(saida.getCdUsuarioInclusao());
		setTipoCanalInclusao(saida.getCdCanalInclusao() == 0 ? "": saida.getCdCanalInclusao() + " - " + saida.getDsCanalInclusao());
		setComplementoInclusao(saida.getNrOperacaoFluxoInclusao().equals("0")?"":saida.getNrOperacaoFluxoInclusao());
		
	}

	/**
	 * Inciar tela.
	 */
	public void inciarTela() {

		setListaGrid(null);
		setListaGridControle(null);
		setItemSelecionadoGrid(null);
		setCpfCnpjRepresentante("");
		setNomeRazaoRepresentante("");
		setAtivEconRepresentante("");
		setSegRepresentante("");
		setSubSegRepresentante("");
		setEmpresaContrato("");
		setTipoContrato("");
		setNumeroContrato("");
		setSituacaoContrato("");
		setMotivoContrato("");
		setParticipacaoContrato("");
		setAditivosContrato("");
		setDataHoraCadastramento("");
		setVigenciaContrato("");
		
		setCdEmpresaContrato(0l);
		setCdTipoContrato(null);
		setNrSequenciaContrato("");

		setCnpj("");
		setCnpj2("");
		setCnpj3("");
		setCpf("");
		setCpf2("");
		setNomeRazaoSocial("");
		setBanco("");
		setAgencia("");
		setAgenciaDig("");
		setConta("");
		setContaDig("");
		setItemSelecionado(null);
		setListaPesquisa(null);

		setListaPesquisaControle(null);
		setListaSelecionadosControle(null);
		setItemSelecionadoPesquisa(null);
		setObrigatoriedade("");

		setTipoParticipacao(null);
		setCpfCnpjParticipante("");
		setNomeRazaoParticipante("");
		setGrupoEconParticipante("");
		setAtividadeEconParticipante("");
		setSegmentoParticipante("");
		setSubSegmentoParticipante("");

		setTipoParticipacaoDesc("");
		setSituacaoParticipacao("");

		setDataHoraManutencao("");
		setUsuarioManutencao("");
		setTipoCanalManutencao("");
		setComplementoManutencao("");
		setDataHoraInclusao("");
		setUsuarioInclusao("");
		setTipoCanalInclusao("");
		setComplementoInclusao("");

		setBtnSelecionar(false);
		setCheckAll(false);

		setItemSelecionado2("");
		setCpfHist("");
		setCpfHist2("");

		setCnpjHist("");
		setCnpjHist2("");
		setCnpjHist3("");

		setDataInicial(new Date());
		setDataFinal(new Date());

		setListaPesquisa2(null);
		setListaPesquisaControle2(null);
		setItemSelecionadoPesquisa2(null);

		setBloquearCampos(false);
	}

	/**
	 * Listar motivo bloqueio desbloqueio.
	 */
	public void listarMotivoBloqueioDesbloqueio() {
		this.listaMotivoBloqueioDesb = new ArrayList<SelectItem>();
		this.listaMotivoBloqueioDesbHash = new HashMap<Integer, String>();

		if (getCondicaoBloqueioDesb() != null && getCondicaoBloqueioDesb() != 0) {

			List<ListarMotivoBloqueioDesbSaidaDTO> list = new ArrayList<ListarMotivoBloqueioDesbSaidaDTO>();

			ListarMotivoBloqueioDesbEntradaDTO entradaDTO = new ListarMotivoBloqueioDesbEntradaDTO();
			entradaDTO.setCdSituacaoParticipanteContrato(getCondicaoBloqueioDesb());

			list = comboService.listarMotivoBloqueioDesb(entradaDTO);
			listaMotivoBloqueioDesb.clear();

			for (ListarMotivoBloqueioDesbSaidaDTO combo : list) {
				listaMotivoBloqueioDesbHash.put(combo.getCdMotivoSituacaoParticipante(), combo
								.getDsMotivoSituacaoParticipante());
				listaMotivoBloqueioDesb.add(new SelectItem(combo.getCdMotivoSituacaoParticipante(), combo
								.getDsMotivoSituacaoParticipante()));
			}
		}
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {
		try {
			ExcluirParticipanteContratoPgitEntradaDTO entradaDTO = new ExcluirParticipanteContratoPgitEntradaDTO();
			ListarParticipantesSaidaDTO listarParticipantesSaidaDTO = listaGrid.get(getItemSelecionadoGrid());

			entradaDTO.setCodEmpresaContrato(getCdEmpresaContrato());
			entradaDTO.setTipoContrato(getCdTipoContrato());
			entradaDTO.setNumSequenciaContrato(Long.parseLong(getNrSequenciaContrato()));
			entradaDTO.setCodTipoParticipacao(listarParticipantesSaidaDTO.getCdTipoParticipacao());
			entradaDTO.setCodParticipante(listarParticipantesSaidaDTO.getCdPessoa());
			entradaDTO.setCpfCnpjParticipante(listarParticipantesSaidaDTO.getCdCpfCnpj());
			entradaDTO.setCodFilialCnpj(listarParticipantesSaidaDTO.getCdFilialCnpj());
			entradaDTO.setCodControleCpfCnpj(listarParticipantesSaidaDTO.getCdControleCnpj());

			ExcluirParticipanteContratoPgitSaidaDTO saidaDTO = getManterContratoImpl().excluirParticipanteContratoPgit(
							entradaDTO);

			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
							PARTICIPANTES_MANTER_CONTRATO, BradescoViewExceptionActionType.ACTION, false);
			carregaListaConsultar();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			return null;
		}
		return "";
	}
	
	public String confirmarExcluirSemLoop() {
		try {
			ExcluirParticipanteContratoPgitEntradaDTO entradaDTO = new ExcluirParticipanteContratoPgitEntradaDTO();
			ListarParticipantesSaidaDTO listarParticipantesSaidaDTO = listaGrid.get(getItemSelecionadoGrid());

			entradaDTO.setCodEmpresaContrato(getCdEmpresaContrato());
			entradaDTO.setTipoContrato(getCdTipoContrato());
			entradaDTO.setNumSequenciaContrato(Long.parseLong(getNrSequenciaContrato()));
			entradaDTO.setCodTipoParticipacao(listarParticipantesSaidaDTO.getCdTipoParticipacao());
			entradaDTO.setCodParticipante(listarParticipantesSaidaDTO.getCdPessoa());
			entradaDTO.setCpfCnpjParticipante(listarParticipantesSaidaDTO.getCdCpfCnpj());
			entradaDTO.setCodFilialCnpj(listarParticipantesSaidaDTO.getCdFilialCnpj());
			entradaDTO.setCodControleCpfCnpj(listarParticipantesSaidaDTO.getCdControleCnpj());

			ExcluirParticipanteContratoPgitSaidaDTO saidaDTO = getManterContratoImpl().excluirParticipanteContratoPgit(
							entradaDTO);

			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
							PARTICIPANTES_MANTER_CONTRATO, BradescoViewExceptionActionType.ACTION, false);
			carregaListaConsultarSemLoop();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			return null;
		}
		return "";
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {

		try {
			IncluirParticipanteContratoPgitEntradaDTO entradaDto = new IncluirParticipanteContratoPgitEntradaDTO();
			
			entradaDto.setCdPessoaJuridica(getCdPessoaJuridica());
			entradaDto.setCodEmpresaContrato(getCdEmpresaContrato());
	        entradaDto.setCodTipoContrato(getCdTipoContrato());
	           if(!getNrSequenciaContrato().equals(null)){
	               entradaDto.setNumSequenciaContrato(Long.parseLong(getNrSequenciaContrato()));
	            }else{
	                entradaDto.setNumSequenciaContrato(0L);
	            }
	        entradaDto.setCodParticipante(getParticipantesSelecionado().getCdClub());

			
			IncluirParticipanteContratoPgitSaidaDTO saidaDTO = getManterContratoImpl().incluirParticipanteContratoPgit(
							entradaDto, getListaContasParaInclusao());

			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
							PARTICIPANTES_MANTER_CONTRATO, BradescoViewExceptionActionType.ACTION, false);

			carregaListaConsultar();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			return null;
		}
		return "";
	}
	
	public String confirmarIncluirSemLoop() {

		try {
			IncluirParticipanteContratoPgitEntradaDTO entradaDto = new IncluirParticipanteContratoPgitEntradaDTO();
			
			entradaDto.setCdPessoaJuridica(getCdPessoaJuridica());
			entradaDto.setCodEmpresaContrato(getCdEmpresaContrato());
	        entradaDto.setCodTipoContrato(getCdTipoContrato());
	           if(!getNrSequenciaContrato().equals(null)){
	               entradaDto.setNumSequenciaContrato(Long.parseLong(getNrSequenciaContrato()));
	            }else{
	                entradaDto.setNumSequenciaContrato(0L);
	            }
	        entradaDto.setCodParticipante(getParticipantesSelecionado().getCdClub());

			
			IncluirParticipanteContratoPgitSaidaDTO saidaDTO = getManterContratoImpl().incluirParticipanteContratoPgit(
							entradaDto, getListaContasParaInclusao());

			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
							PARTICIPANTES_MANTER_CONTRATO, BradescoViewExceptionActionType.ACTION, false);

			carregaListaConsultarSemLoop();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			return null;
		}
		return "";
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		/*
		 * for(int i=0;i<listaSelecionados.size();i++){
		 * listaSelecionados.get(i).setDsTipoParticipacao((String)listaTipoParticipacaoHash
		 * .get(listaSelecionados.get(i).getTipoParticipacao())); }
		 */
		return "AVANCAR_INCLUIR";
	}

	/**
	 * Limpar dados incluir.
	 */
	public void limparDadosIncluir() {
		this.setCnpj("");
		this.setCnpj2("");
		this.setCnpj3("");
		this.setCpf("");
		this.setCpf2("");
		this.setNomeRazaoSocial("");
		this.setBanco("");
		this.setAgencia("");
		this.setAgenciaDig("");
		this.setConta("");
		this.setContaDig("");

		this.setItemSelecionado(null);
		this.setListaPesquisa(null);
		this.setListaPesquisaControle(null);
		this.setItemSelecionadoPesquisa(null);
		this.setCheckAll(false);

		this.setBtnSelecionar(false);

		this.setListaSelecionadosControle(null);

	}
	
	/**
	 * Carrega lista inc.
	 */
	public void carregaListaInc() {

		listaPesquisa = new ArrayList<ListarClubesClientesSaidaDTO>();
		try {

			ConsultarListaClientePessoasEntradaDTO entradaDTO = new ConsultarListaClientePessoasEntradaDTO();

			entradaDTO.setCdAgenciaBancaria(!(getAgencia().equals("")) ? Integer.parseInt(getAgencia()) : 0);
			entradaDTO.setCdBanco(!(getBanco().equals("")) ? Integer.parseInt(getBanco()) : 0);
			entradaDTO.setCdContaBancaria(!(getConta().equals("")) ? Long.parseLong(getConta()) : 0);
			entradaDTO.setCdTipoConta(!(getContaDig().equals("")) ? Integer.parseInt(getContaDig()) : 0);
			entradaDTO.setCdCpfCnpj(!(getCnpj().equals("")) ? Long.parseLong(getCnpj()) : 0);
			entradaDTO.setCdFilialCnpj(!(getCnpj2().equals("")) ? Integer.parseInt(getCnpj2()) : 0);
			entradaDTO.setCdControleCnpj(!(getCnpj3().equals("")) ? Integer.parseInt(getCnpj3()) : 0);
			entradaDTO.setCdCpf(!(getCpf().equals("")) ? Long.parseLong(getCpf()) : 0);
			entradaDTO.setCdControleCpf(!(getCpf2().equals("")) ? Integer.parseInt(getCpf2()) : 0);
			entradaDTO.setDsNomeRazaoSocial(!(getNomeRazaoSocial().equals("")) ? getNomeRazaoSocial() : "");
			entradaDTO.setDtNascimentoFundacao("");
			entradaDTO.setNumeroOcorrencias(0);

			setListaPesquisa(getFiltroIdentificaoImpl().listarClubesClientes(entradaDTO));
			this.listaPesquisaControle = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaPesquisa().size(); i++) {
				listaPesquisaControle.add(new SelectItem(i, " "));
			}
			
			setItemSelecionadoListaParticipantes(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaPesquisa(null);
			setCheckAll(false);
		}

	}

	/**
	 * Carrega lista consultar.
	 */
	public void carregaListaConsultar() {
		listaGrid = new ArrayList<ListarParticipantesSaidaDTO>();
		this.listaGridControle = new ArrayList<SelectItem>();
		try {

			ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();

			entradaDTO.setMaxOcorrencias(20);
			entradaDTO.setCodPessoaJuridica(getCdEmpresaContrato());
			entradaDTO.setCodTipoContrato(getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(Long.parseLong(getNrSequenciaContrato()));

			setListaGrid(getManterContratoImpl().listarParticipantes(entradaDTO));

			for (int i = 0; i < getListaGrid().size(); i++) {
				listaGridControle.add(new SelectItem(i, " "));
			}
			setItemSelecionadoGrid(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaGrid(null);
			setItemSelecionadoGrid(null);
		}
	}
	
	public void carregaListaConsultarSemLoop() {
		listaGrid = new ArrayList<ListarParticipantesSaidaDTO>();
		this.listaGridControle = new ArrayList<SelectItem>();
		try {

			ListarParticipantesEntradaDTO entradaDTO = new ListarParticipantesEntradaDTO();

			entradaDTO.setMaxOcorrencias(20);
			entradaDTO.setCodPessoaJuridica(getCdEmpresaContrato());
			entradaDTO.setCodTipoContrato(getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(Long.parseLong(getNrSequenciaContrato()));

			setListaGrid(getManterContratoImpl().listarParticipantesSemLoop(entradaDTO));

			for (int i = 0; i < getListaGrid().size(); i++) {
				listaGridControle.add(new SelectItem(i, " "));
			}
			setItemSelecionadoGrid(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaGrid(null);
			setItemSelecionadoGrid(null);
		}
	}

	/**
	 * Selecionar.
	 *
	 * @return the string
	 */
	public String selecionar() {
	    try {

    		if(!getListaPesquisa().isEmpty() && getItemSelecionadoListaParticipantes() != null ){
    		    
    		    ListarClubesClientesSaidaDTO itemSelecionado = 
    		        getListaPesquisa().get(getItemSelecionadoListaParticipantes()); 
    		    
    		    setParticipantesSelecionado(new ParticipantesDTO());
    		    
    		    getParticipantesSelecionado().setCdAtividadeEconomica(itemSelecionado.getCdAtividadeEconomica());
                getParticipantesSelecionado().setCdClub(itemSelecionado.getCdClub());
                getParticipantesSelecionado().setCdControleCnpj(itemSelecionado.getCdControleCnpj());
                getParticipantesSelecionado().setCdCpfCnpj(itemSelecionado.getCdCpfCnpj());
                getParticipantesSelecionado().setCdFilialCnpj(itemSelecionado.getCdFilialCnpj());
                getParticipantesSelecionado().setCdGrupoEconomico(itemSelecionado.getCdGrupoEconomico());
                getParticipantesSelecionado().setCdSegmentoCliente(itemSelecionado.getCdSegmentoCliente());
                getParticipantesSelecionado().setCdSubSegmentoCliente(itemSelecionado.getCdSubSegmentoCliente());
                getParticipantesSelecionado().setDsAtividadeEconomica(itemSelecionado.getDsAtividadeEconomica());
                getParticipantesSelecionado().setDsGrupoEconomico(itemSelecionado.getDsGrupoEconomico());
                getParticipantesSelecionado().setCnpjOuCpfFormatado(itemSelecionado.getCnpjOuCpfFormatado());
                getParticipantesSelecionado().setDsNomeRazao(itemSelecionado.getDsNomeRazao());
                getParticipantesSelecionado().setDsSegmentoCliente(itemSelecionado.getDsSegmentoCliente());
                getParticipantesSelecionado().setDsSubSegmentoCliente(itemSelecionado.getDsSubSegmentoCliente());
    		    
                setTipoParticipacao(0);
                
                limparCamposPerquisaContasParticipantes();
                setListaContasPossiveisInclusao(null);
                
                setHabilitarBtnRemover(false);
                setHabilitarBtnSelecionar(false);
                consultarContasParticipantes();
                return "SELECIONAR";
    		}
    		
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			if (StringUtils.right(p.getCode(), 8).equals("PGIT1446")
					|| StringUtils.right(p.getCode(), 8).equals("PGIT0003")
						//IN 3470907
						|| StringUtils.right(p.getCode(), 8).equals("PGIT1198")) { 
				return "SELECIONAR";
			}
		}	
		
	    return "";
	}
	
    /**
     * Consultar contas participantes.
     */
    public void consultarContasParticipantes() {
        setCdBanco(null);
        setCdAgencia(null);
        setCdConta(null);

        if (getBancoFiltroContasParticipantes() != null && getAgenciaFiltroContasParticipantes() != null
            && getContaFiltroContasParticipantes() != null) {

            setCdBanco(getBancoFiltroContasParticipantes());
            setCdAgencia(getAgenciaFiltroContasParticipantes());
            setCdConta(getContaFiltroContasParticipantes());
        }

        pesquisarContasParticipantes();
    }

	/**
	 * Pesquisar contas participantes.
	 */
	private void pesquisarContasParticipantes(){
	    ListarContasPossiveisInclusaoEntradaDTO entrada = new ListarContasPossiveisInclusaoEntradaDTO();
	    
	    entrada.setCdPessoaJuridicaContrato(getCdEmpresaContrato());
	    entrada.setCdTipoContratoNegocio(getCdTipoContrato());
	    if(getNrSequenciaContrato() != null){
	        entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContrato()));
	    }else{
	        entrada.setNrSequenciaContratoNegocio(0L);
	    }
	    
	    entrada.setCdCorpoCpfCnpj(getParticipantesSelecionado().getCdCpfCnpj());
	    entrada.setCdFilialCpfCnpj(getParticipantesSelecionado().getCdFilialCnpj());
	    entrada.setCdDigitoCpfCnpj(getParticipantesSelecionado().getCdControleCnpj()); 

	    entrada.setCdBanco(getCdBanco());
	    entrada.setCdAgencia(getCdAgencia());
	    entrada.setCdConta(getCdConta());

	   setListaContasPossiveisInclusao(new ArrayList<ListarContasPossiveisInclusaoSaidaDTO>());
	   setListaContasPossiveisInclusao(getManterContratoImpl().listarContasPossiveisInclusao(entrada));
	   setListaContasPossiveisInclusaoControle(new ArrayList<SelectItem>());
	   
       for (int i = 0; i < getListaContasPossiveisInclusao().size(); i++) {
           listaContasPossiveisInclusaoControle.add(new SelectItem(i, " "));
       }
       
       setCheckAll(false);
       setHabilitarBtnSelecionar(false);
	}
	
	
	/**
	 * Paginar listaontas participantes.
	 *
	 * @param event the event
	 */
	public void paginarListaontasParticipantes(ActionEvent event){
	    pesquisarContasParticipantes();
	    
	}
	
	
	/**
	 * Limpar campos perquisa contas participantes.
	 */
	public void limparCamposPerquisaContasParticipantes(){
	   setBancoFiltroContasParticipantes(237);
	   setAgenciaFiltroContasParticipantes(null);
	   setContaFiltroContasParticipantes(null);
	   
	   setCdBanco(null);
	   setCdAgencia(null);
	   setCdConta(null);
	   
	   setListaContasParaInclusao(new ArrayList<ListarContasPossiveisInclusaoSaidaDTO>());
	   consultarContasParticipantes(); 
	   
	   setHabilitarBtnRemover(false);
	   setHabilitarBtnSelecionar(false);
	}
	
	
	/**
	 * Adicionar contas para inclusao.
	 */
	public void adicionarContasParaInclusao(){ 
	    for (int i = 0; i < getListaContasPossiveisInclusao().size(); i++) {
	        ListarContasPossiveisInclusaoSaidaDTO item =  getListaContasPossiveisInclusao().get(i); 
            if(item.isCheck()){
                item.setCheck(false);
                
                if(!listaContasParaInclusao.contains(item)){
                    listaContasParaInclusao.add(item.clone());
                }

            }
        }
	    
	    setCheckAll(false);
	    setHabilitarBtnSelecionar(false);
	}
    
	/**
	 * Remover contas para inclusao.
	 */
	public void removerContasParaInclusao(){ 
	    
	    for (int i = 0; i < getListaContasParaInclusao().size(); i++) {
            ListarContasPossiveisInclusaoSaidaDTO item =  getListaContasParaInclusao().get(i); 
            if(item.isCheck()){
                item.setCheck(false);
                listaContasParaInclusao.remove(i);

                i--;
            }
        }
	    
	    pesquisarContasParticipantes();
	    setCheckAll2(false);
        setHabilitarBtnRemover(false);
	}
		
	/**
	 * Avancar concluir inclusao.
	 *
	 * @return the string
	 */
	public String avancarConcluirInclusao(){ 

	    return "AVANCAR_INCLUIR";
	}
	
   /**
    * Habilitar selecionar.
    */
   public void habilitarSelecionar(){ 
       setHabilitarBtnSelecionar(false);
       for (int i = 0; i < getListaContasPossiveisInclusao().size(); i++) {
           if(getListaContasPossiveisInclusao().get(i).isCheck()){
               setHabilitarBtnSelecionar(true);
           }
       }     
	        
  }
   
   /**
    * Habilitar remover.
    */
   public void habilitarRemover(){ 
       setHabilitarBtnRemover(false);
       for (int i = 0; i < getListaContasParaInclusao().size(); i++) {
           if(getListaContasParaInclusao().get(i).isCheck()){
               setHabilitarBtnRemover(true);
           }
       }
   }

	/**
	 * Limpar incluir.
	 */
	public void limparIncluir() {
		setListaPesquisa(null);
		setListaPesquisaControle(null);
		setCheckAll(false);
	}
	
	/**
	 * Preenche dados.
	 */
	public void preencheDados() {
		ListarParticipantesSaidaDTO saidaDTO = listaGrid.get(getItemSelecionadoGrid());

		setCdPessoaParticipante(saidaDTO.getCdPessoa());
		setCdTipoParticipante(saidaDTO.getCdTipoParticipacao());
		setCpfCnpjParticipanteDet(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoParticipanteDet(saidaDTO.getNomeRazao());
		setGrupoEconParticipante(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconParticipante(saidaDTO.getDsAtividadeEconomica());
		setSegmentoParticipante(saidaDTO.getDsSegmentoEconomico());
		setSubSegmentoParticipante(saidaDTO.getDsSubSegmento());
		setTipoParticipacaoDesc(saidaDTO.getDsTipoParticipacao());
		setSituacaoParticipacao(saidaDTO.getDsSituacaoParticipacao());
		setCdClassificacaoParticipante(saidaDTO.getCdClasificacaoParticipante());
		setDsClassificacaoParticipante(saidaDTO.getDsClasificacaoParticipante());
		setMotivoSituacao(saidaDTO.getDsMotivoParticipacao());

		setDataHoraManutencao(saidaDTO.getHoraManutencaoResAlteracao());
		setUsuarioManutencao(saidaDTO.getCdUsuarioAlteracao());
		setTipoCanalManutencao(saidaDTO.getCdCanalAlteracao() == 0 ? "" : saidaDTO.getCdCanalAlteracao() + " - "
						+ saidaDTO.getDsCanalAlteracao());
		setComplementoManutencao(saidaDTO.getNomeOperacaoFluxoAlteracao().equals("0") ? "" : saidaDTO
						.getNomeOperacaoFluxoAlteracao());

		setDataHoraInclusao(saidaDTO.getHoraManutencaoRegInclusao());
		setUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
		setTipoCanalInclusao(saidaDTO.getCdCanalInclusao() == 0 ? "" : saidaDTO.getCdCanalInclusao() + " - "
						+ saidaDTO.getDsCanalInclusao());
		setComplementoInclusao(saidaDTO.getNomeOperacaoFluxoInclusao().equals("0") ? "" : saidaDTO
						.getNomeOperacaoFluxoInclusao());

	}
	
    /**
     * Listar contas exclusao.
     */
    private void listarContasExclusao() {

        ListarParticipantesSaidaDTO itemSelecionado = listaGrid.get(getItemSelecionadoGrid());
        ListarContasExclusaoEntradaDTO entrada = new ListarContasExclusaoEntradaDTO();

        entrada.setNrMaximoOcorrencias(50);
        entrada.setCdPessoaJuridicaNegocio(getCdEmpresaContrato());
        entrada.setCdTipoContratoNegocio(getCdTipoContrato());
        if (!getNrSequenciaContrato().equals(null)) {
            entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContrato()));
        } else {
            entrada.setNrSequenciaContratoNegocio(0L);
        }

        entrada.setCdCorpoCpfCnpj(itemSelecionado.getCdCpfCnpj());
        entrada.setCdFilialCnpjParticipante(itemSelecionado.getCdFilialCnpj());
        entrada.setCdControleCpfCnpj(itemSelecionado.getCdControleCnpj());

        setSaidaListarContasExclusao(manterContratoImpl.listarContasExclusao(entrada));

    }

	/**
	 * Limpar campos.
	 */
	public void limparCampos() {
		this.setCnpj("");
		this.setCnpj2("");
		this.setCnpj3("");
		this.setCpf("");
		this.setCpf2("");
		this.setNomeRazaoSocial("");
		this.setBanco("");
		this.setAgencia("");
		this.setAgenciaDig("");
		this.setConta("");
		this.setContaDig("");

		if (getItemSelecionado().equals("3")){
			setBanco("237");
		}
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt) {
	    carregaListaConsultar();
	}
	
	public void pesquisarSemLoop(ActionEvent evt) {
	    carregaListaConsultarSemLoop();
	}

	/**
     * Voltar lista.
     * 
     * @return the string
     */
	public String voltarLista() {
	    carregaListaConsultar();
	    return voltar();
	}
	
	public String voltarListaSemLoop() {
	    carregaListaConsultarSemLoop();
	    return voltar();
	}

	/**
	 * Voltar consulta duplicacao.
	 *
	 * @return the string
	 */
	public String voltarConsultaDuplicacao() {
		setItemSelecionadoGrid(null);
		setItemSelecionadoPesquisa2(null);

		return "selDuplicacaoArquivoRetorno";
	}

	/**
	 * Avancar confirmar bloq desbloq.
	 *
	 * @return the string
	 */
	public String avancarConfirmarBloqDesbloq() {
		setDsMotivoBloqueioDesbloqueio((String) getListaMotivoBloqueioDesbHash().get(getMotivoBloqueioDesbloqueio()));

		return "AVANCAR";
	}

	/**
	 * Voltar bloqueio desbloqueio.
	 *
	 * @return the string
	 */
	public String voltarBloqueioDesbloqueio() {
		return "VOLTAR";
	}

	/**
	 * Confirmar bloqueio desbloqueio.
	 *
	 * @return the string
	 */
	public String confirmarBloqueioDesbloqueio() {
		try {
			BloquearDesbloquearContratoPgitEntradaDTO entradaDTO = new BloquearDesbloquearContratoPgitEntradaDTO();
			ListarParticipantesSaidaDTO listarParticipantesSaidaDTO = listaGrid.get(getItemSelecionadoGrid());

			entradaDTO.setCdpessoaJuridicaContrato(getCdEmpresaContrato());
			entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContrato()));

			entradaDTO.setCdMotivoSituacaoParticipante(getMotivoBloqueioDesbloqueio());
			entradaDTO.setCdPessoa(listarParticipantesSaidaDTO.getCdPessoa());

			// Radio Condi��o
			entradaDTO.setCdSituacaoParticipanteContrato(getCondicaoBloqueioDesb());

			entradaDTO.setCdTipoParticipacaoPessoa(listarParticipantesSaidaDTO.getCdTipoParticipacao());

			BloquearDesbloquearContratoPgitSaidaDTO saidaDTO = getManterContratoImpl().bloquearDesbloquearContratoPgit(
							entradaDTO);

			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
							PARTICIPANTES_MANTER_CONTRATO, BradescoViewExceptionActionType.ACTION, false);
			carregaListaConsultar();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			return null;
		}

		return "";
	}
	
	/**
	 * Confirmar bloqueio desbloqueio aux.
	 *
	 * @return the string
	 */
	public String confirmarBloqueioDesbloqueioAux() {
		try {
			BloquearDesbloquearContratoPgitEntradaDTO entradaDTO = new BloquearDesbloquearContratoPgitEntradaDTO();
			ListarParticipantesSaidaDTO listarParticipantesSaidaDTO = listaGrid.get(getItemSelecionadoGrid());

			entradaDTO.setCdpessoaJuridicaContrato(getCdEmpresaContrato());
			entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(getNrSequenciaContrato()));

			entradaDTO.setCdMotivoSituacaoParticipante(getMotivoBloqueioDesbloqueio());
			entradaDTO.setCdPessoa(listarParticipantesSaidaDTO.getCdPessoa());

			// Radio Condi��o
			entradaDTO.setCdSituacaoParticipanteContrato(getCondicaoBloqueioDesb());

			entradaDTO.setCdTipoParticipacaoPessoa(listarParticipantesSaidaDTO.getCdTipoParticipacao());

			BloquearDesbloquearContratoPgitSaidaDTO saidaDTO = getManterContratoImpl().bloquearDesbloquearContratoPgit(
							entradaDTO);

			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
							"conParticipantesBloquearDesbloquear", BradescoViewExceptionActionType.ACTION, false);
			carregaListaConsultar();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			return null;
		}

		return "";
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		for (int i = 0; i < getListaPesquisa().size(); i++) {
			getListaPesquisa().get(i).setCheck(false);
		}
		setBtnSelecionar(false);
		setCheckAll(false);
		return "VOLTAR";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							PARTICIPANTES_MANTER_CONTRATO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "DETALHAR";
	}

	/**
	 * Get: listaClassificacaoAreaNegocio.
	 *
	 * @return listaClassificacaoAreaNegocio
	 */
	public List<SelectItem> getListaClassificacaoAreaNegocio() {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem(1, "CORRETORA DE VALORES"));
		selectItems.add(new SelectItem(2, "INSTIT. FINANCEIRA"));
		selectItems.add(new SelectItem(3, "CART�RIO"));
		selectItems.add(new SelectItem(4, "INVESTIDOR"));
		selectItems.add(new SelectItem(5, "EMPRESA/PESSOA FISICA"));
		selectItems.add(new SelectItem(6, "COMERCIO"));
		selectItems.add(new SelectItem(7, "INDUSTRIA"));
		selectItems.add(new SelectItem(8, "OUTROS"));
		return selectItems;
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		String ret = detalhar();

		if (ret == null) {
			return null;
		}

		return "ALTERAR";
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {
		setDsClassificacaoParticipante("");

		List<SelectItem> selectItems = getListaClassificacaoAreaNegocio();
		for (SelectItem item : selectItems) {
			if (item.getValue().equals(getCdClassificacaoParticipante())) {
				setDsClassificacaoParticipante(item.getLabel());
				break;
			}
		}

		return "AVANCAR_ALTERAR";
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {
		AlterarParticipanteContratoPgitEntradaDTO participanteAlterado = new AlterarParticipanteContratoPgitEntradaDTO();
		participanteAlterado.setCdPessoaJuridica(getCdEmpresaContrato());
		participanteAlterado.setCdTipoContrato(getCdTipoContrato());
		participanteAlterado.setNrSequenciaContrato(NumberUtils.parseLong(getNrSequenciaContrato()));
		participanteAlterado.setCdTipoParticipante(getCdTipoParticipante());
		participanteAlterado.setCdPessoaParticipacao(getCdPessoaParticipante());
		participanteAlterado.setCdclassificacaoAreaParticipante(getCdClassificacaoParticipante());

		AlterarParticipanteContratoPgitSaidaDTO retorno = getManterContratoImpl().alterarParticipanteContratoPgit(
						participanteAlterado);

		PgitFacesUtils.addInfoModalMessage(retorno.getCodMensagem(), retorno.getMensagem(),
						"#{manterContratoParticipantesBean.carregaListaConsultarAlteracao}");

		return null;
	}

	/**
	 * Carrega lista consultar alteracao.
	 *
	 * @return the string
	 */
	public String carregaListaConsultarAlteracao() {
		carregaListaConsultarSemLoop();
		return PARTICIPANTES_MANTER_CONTRATO;
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		limparDadosIncluir();
		return "INCLUIR";
	}

	/**
	 * Bloquear desbloquear.
	 *
	 * @return the string
	 */
	public String bloquearDesbloquear() {

		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							PARTICIPANTES_MANTER_CONTRATO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		setCondicaoBloqueioDesb(null);
		setMotivoBloqueioDesbloqueio(null);
		this.listaMotivoBloqueioDesb = new ArrayList<SelectItem>();

		return "BLOQ_DESBLOQ";
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		try {
			preencheDados();
			listarContasExclusao();
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							PARTICIPANTES_MANTER_CONTRATO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "EXCLUIR";
	}

	/**
	 * Historico.
	 *
	 * @return the string
	 */
	public String historico() {
		if (getItemSelecionadoGrid() != null) {
			limparHistorico();
			ListarParticipantesSaidaDTO saidaDTO = listaGrid.get(getItemSelecionadoGrid());
			if (saidaDTO.getCdFilialCnpj() == 0) {
				setCpfCnpjParticipante(saidaDTO.getCnpjOuCpfFormatado());
				setNomeRazaoParticipante(saidaDTO.getNomeRazao());
				setCpfHist(saidaDTO.getCdCpfCnpj() + "");
				setCpfHist2(saidaDTO.getCdControleCnpj() + "");
				setItemSelecionado2("0");
			} else {
				setCnpjHist(saidaDTO.getCdCpfCnpj() + "");
				setCnpjHist2(saidaDTO.getCdFilialCnpj() + "");
				setCnpjHist3(saidaDTO.getCdControleCnpj() + "");
				setItemSelecionado2("1");
			}
			this.setDataInicial(new Date());
			this.setDataFinal(new Date());
			return "HISTORICO";
		} else if (getItemSelecionadoGrid() == null) {
			limparHistorico();
			return "HISTORICO";
		}
		return "";
	}

	/**
	 * Limpar historico.
	 */
	public void limparHistorico() {
		setItemSelecionado2("");

		setCpfHist("");
		setCpfHist2("");

		setCnpjHist("");
		setCnpjHist2("");
		setCnpjHist3("");

		setDataInicial(new Date());
		setDataFinal(new Date());

		setListaPesquisa2(null);
		setListaPesquisaControle2(null);
		setItemSelecionadoPesquisa2(null);

		setBloquearCampos(false);
	}

	/**
	 * Carrega lista historico participante.
	 */
	public void carregaListaHistoricoParticipante() {
		listaPesquisa2 = new ArrayList<ConsultarManParticipanteSaidaDTO>();

		try {
			ConsultarManParticipanteEntradaDTO entrada = new ConsultarManParticipanteEntradaDTO();
			ListarParticipantesSaidaDTO dto = listaGrid.get(getItemSelecionadoGrid());

			entrada.setCdCorpoCpfCnpj(dto.getCdCpfCnpj());
			entrada.setCdControleCpfCnpj(dto.getCdFilialCnpj());
			entrada.setCdDigitoCpfCnpj(dto.getCdControleCnpj());

			entrada.setCdPessoaJuridica(getCdEmpresaContrato() != null ? getCdEmpresaContrato() : 0);
			entrada.setCdTipoContratoNegocio(getCdTipoContrato() != null ? getCdTipoContrato() : 0);
			entrada.setNrSeqContratoNegocio(!getNrSequenciaContrato().equals("") ? Long
							.parseLong(getNrSequenciaContrato()) : 0);

			entrada.setDtFim("");
			entrada.setDtInicio("");

			// Preenche os Campos de Pesquisa Historico
			if (dto.getCdFilialCnpj() == 0) {
				setCpfHist(String.valueOf(dto.getCdCpfCnpj()));
				setCpfHist2(String.valueOf(dto.getCdControleCnpj()));
				setItemSelecionado2("0");
			} else if (dto.getCdFilialCnpj() != 0) {
				setCnpjHist(String.valueOf(dto.getCdCpfCnpj()));
				setCnpjHist2(String.valueOf(dto.getCdFilialCnpj()));
				setCnpjHist3(String.valueOf(dto.getCdControleCnpj()));
				setItemSelecionado2("1");
			}
			setDataInicial(null);
			setDataFinal(null);
			setBloquearCampos(true);

			setListaPesquisa2(getManterContratoImpl().consultarManutencaoParticipante(entrada));

			listaPesquisaControle2 = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaPesquisa2().size(); i++) {
				listaPesquisaControle2.add(new SelectItem(i, ""));
			}
			setItemSelecionadoPesquisa2(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaPesquisa2(null);
			setItemSelecionadoPesquisa2(null);
		}
	}

	// gets sets
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: itemSelecionadoGrid.
	 *
	 * @return itemSelecionadoGrid
	 */
	public Integer getItemSelecionadoGrid() {
		return itemSelecionadoGrid;
	}

	/**
	 * Set: itemSelecionadoGrid.
	 *
	 * @param itemSelecionadoGrid the item selecionado grid
	 */
	public void setItemSelecionadoGrid(Integer itemSelecionadoGrid) {
		this.itemSelecionadoGrid = itemSelecionadoGrid;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarParticipantesSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarParticipantesSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: listaGridControle.
	 *
	 * @return listaGridControle
	 */
	public List<SelectItem> getListaGridControle() {
		return listaGridControle;
	}

	/**
	 * Set: listaGridControle.
	 *
	 * @param listaGridControle the lista grid controle
	 */
	public void setListaGridControle(List<SelectItem> listaGridControle) {
		this.listaGridControle = listaGridControle;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public String getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(String itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: itemSelecionadoPesquisa.
	 *
	 * @return itemSelecionadoPesquisa
	 */
	public Integer getItemSelecionadoPesquisa() {
		return itemSelecionadoPesquisa;
	}

	/**
	 * Set: itemSelecionadoPesquisa.
	 *
	 * @param itemSelecionadoPesquisa the item selecionado pesquisa
	 */
	public void setItemSelecionadoPesquisa(Integer itemSelecionadoPesquisa) {
		this.itemSelecionadoPesquisa = itemSelecionadoPesquisa;
	}

	/**
	 * Get: listaPesquisa.
	 *
	 * @return listaPesquisa
	 */
	public List<ListarClubesClientesSaidaDTO> getListaPesquisa() {
		return listaPesquisa;
	}

	/**
	 * Set: listaPesquisa.
	 *
	 * @param listaPesquisa the lista pesquisa
	 */
	public void setListaPesquisa(List<ListarClubesClientesSaidaDTO> listaPesquisa) {
		this.listaPesquisa = listaPesquisa;
	}

	/**
	 * Get: listaPesquisaControle.
	 *
	 * @return listaPesquisaControle
	 */
	public List<SelectItem> getListaPesquisaControle() {
		return listaPesquisaControle;
	}

	/**
	 * Set: listaPesquisaControle.
	 *
	 * @param listaPesquisaControle the lista pesquisa controle
	 */
	public void setListaPesquisaControle(List<SelectItem> listaPesquisaControle) {
		this.listaPesquisaControle = listaPesquisaControle;
	}

	/**
	 * Get: tipoParticipacao.
	 *
	 * @return tipoParticipacao
	 */
	public Integer getTipoParticipacao() {
		return tipoParticipacao;
	}

	/**
	 * Set: tipoParticipacao.
	 *
	 * @param tipoParticipacao the tipo participacao
	 */
	public void setTipoParticipacao(Integer tipoParticipacao) {
		this.tipoParticipacao = tipoParticipacao;
	}

	/**
	 * Get: tipoParticipacaoDesc.
	 *
	 * @return tipoParticipacaoDesc
	 */
	public String getTipoParticipacaoDesc() {
		return tipoParticipacaoDesc;
	}

	/**
	 * Set: tipoParticipacaoDesc.
	 *
	 * @param tipoParticipacaoDesc the tipo participacao desc
	 */
	public void setTipoParticipacaoDesc(String tipoParticipacaoDesc) {
		this.tipoParticipacaoDesc = tipoParticipacaoDesc;
	}

	/**
	 * Get: situacaoParticipacao.
	 *
	 * @return situacaoParticipacao
	 */
	public String getSituacaoParticipacao() {
		return situacaoParticipacao;
	}

	/**
	 * Set: situacaoParticipacao.
	 *
	 * @param situacaoParticipacao the situacao participacao
	 */
	public void setSituacaoParticipacao(String situacaoParticipacao) {
		this.situacaoParticipacao = situacaoParticipacao;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * Set: agencia.
	 *
	 * @param agencia the agencia
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * Set: banco.
	 *
	 * @param banco the banco
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}

	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * Get: contaDig.
	 *
	 * @return contaDig
	 */
	public String getContaDig() {
		return contaDig;
	}

	/**
	 * Set: contaDig.
	 *
	 * @param contaDig the conta dig
	 */
	public void setContaDig(String contaDig) {
		this.contaDig = contaDig;
	}

	/**
	 * Get: atividadeEconParticipante.
	 *
	 * @return atividadeEconParticipante
	 */
	public String getAtividadeEconParticipante() {
		return atividadeEconParticipante;
	}

	/**
	 * Set: atividadeEconParticipante.
	 *
	 * @param atividadeEconParticipante the atividade econ participante
	 */
	public void setAtividadeEconParticipante(String atividadeEconParticipante) {
		this.atividadeEconParticipante = atividadeEconParticipante;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: grupoEconParticipante.
	 *
	 * @return grupoEconParticipante
	 */
	public String getGrupoEconParticipante() {
		return grupoEconParticipante;
	}

	/**
	 * Set: grupoEconParticipante.
	 *
	 * @param grupoEconParticipante the grupo econ participante
	 */
	public void setGrupoEconParticipante(String grupoEconParticipante) {
		this.grupoEconParticipante = grupoEconParticipante;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: segmentoParticipante.
	 *
	 * @return segmentoParticipante
	 */
	public String getSegmentoParticipante() {
		return segmentoParticipante;
	}

	/**
	 * Set: segmentoParticipante.
	 *
	 * @param segmentoParticipante the segmento participante
	 */
	public void setSegmentoParticipante(String segmentoParticipante) {
		this.segmentoParticipante = segmentoParticipante;
	}

	/**
	 * Get: subSegmentoParticipante.
	 *
	 * @return subSegmentoParticipante
	 */
	public String getSubSegmentoParticipante() {
		return subSegmentoParticipante;
	}

	/**
	 * Set: subSegmentoParticipante.
	 *
	 * @param subSegmentoParticipante the sub segmento participante
	 */
	public void setSubSegmentoParticipante(String subSegmentoParticipante) {
		this.subSegmentoParticipante = subSegmentoParticipante;
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: filtroIdentificaoImpl.
	 *
	 * @return filtroIdentificaoImpl
	 */
	public IFiltroIdentificaoService getFiltroIdentificaoImpl() {
		return filtroIdentificaoImpl;
	}

	/**
	 * Set: filtroIdentificaoImpl.
	 *
	 * @param filtroIdentificaoImpl the filtro identificao impl
	 */
	public void setFiltroIdentificaoImpl(IFiltroIdentificaoService filtroIdentificaoImpl) {
		this.filtroIdentificaoImpl = filtroIdentificaoImpl;
	}

	/**
	 * Get: cnpj.
	 *
	 * @return cnpj
	 */
	public String getCnpj() {
		return cnpj;
	}

	/**
	 * Set: cnpj.
	 *
	 * @param cnpj the cnpj
	 */
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	/**
	 * Get: cnpj2.
	 *
	 * @return cnpj2
	 */
	public String getCnpj2() {
		return cnpj2;
	}

	/**
	 * Set: cnpj2.
	 *
	 * @param cnpj2 the cnpj2
	 */
	public void setCnpj2(String cnpj2) {
		this.cnpj2 = cnpj2;
	}

	/**
	 * Get: cnpj3.
	 *
	 * @return cnpj3
	 */
	public String getCnpj3() {
		return cnpj3;
	}

	/**
	 * Set: cnpj3.
	 *
	 * @param cnpj3 the cnpj3
	 */
	public void setCnpj3(String cnpj3) {
		this.cnpj3 = cnpj3;
	}

	/**
	 * Get: cpf.
	 *
	 * @return cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Set: cpf.
	 *
	 * @param cpf the cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Get: cpf2.
	 *
	 * @return cpf2
	 */
	public String getCpf2() {
		return cpf2;
	}

	/**
	 * Set: cpf2.
	 *
	 * @param cpf2 the cpf2
	 */
	public void setCpf2(String cpf2) {
		this.cpf2 = cpf2;
	}

	/**
	 * Get: agenciaDig.
	 *
	 * @return agenciaDig
	 */
	public String getAgenciaDig() {
		return agenciaDig;
	}

	/**
	 * Set: agenciaDig.
	 *
	 * @param agenciaDig the agencia dig
	 */
	public void setAgenciaDig(String agenciaDig) {
		this.agenciaDig = agenciaDig;
	}

	/**
	 * Get: aditivosContrato.
	 *
	 * @return aditivosContrato
	 */
	public String getAditivosContrato() {
		return aditivosContrato;
	}

	/**
	 * Set: aditivosContrato.
	 *
	 * @param aditivosContrato the aditivos contrato
	 */
	public void setAditivosContrato(String aditivosContrato) {
		this.aditivosContrato = aditivosContrato;
	}

	/**
	 * Get: ativEconRepresentante.
	 *
	 * @return ativEconRepresentante
	 */
	public String getAtivEconRepresentante() {
		return ativEconRepresentante;
	}

	/**
	 * Set: ativEconRepresentante.
	 *
	 * @param ativEconRepresentante the ativ econ representante
	 */
	public void setAtivEconRepresentante(String ativEconRepresentante) {
		this.ativEconRepresentante = ativEconRepresentante;
	}

	/**
	 * Get: cpfCnpjRepresentante.
	 *
	 * @return cpfCnpjRepresentante
	 */
	public String getCpfCnpjRepresentante() {
		return cpfCnpjRepresentante;
	}

	/**
	 * Set: cpfCnpjRepresentante.
	 *
	 * @param cpfCnpjRepresentante the cpf cnpj representante
	 */
	public void setCpfCnpjRepresentante(String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	/**
	 * Get: dataHoraCadastramento.
	 *
	 * @return dataHoraCadastramento
	 */
	public String getDataHoraCadastramento() {
		return dataHoraCadastramento;
	}

	/**
	 * Set: dataHoraCadastramento.
	 *
	 * @param dataHoraCadastramento the data hora cadastramento
	 */
	public void setDataHoraCadastramento(String dataHoraCadastramento) {
		this.dataHoraCadastramento = dataHoraCadastramento;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: grupEconRepresentante.
	 *
	 * @return grupEconRepresentante
	 */
	public String getGrupEconRepresentante() {
		return grupEconRepresentante;
	}

	/**
	 * Set: grupEconRepresentante.
	 *
	 * @param grupEconRepresentante the grup econ representante
	 */
	public void setGrupEconRepresentante(String grupEconRepresentante) {
		this.grupEconRepresentante = grupEconRepresentante;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: nomeRazaoRepresentante.
	 *
	 * @return nomeRazaoRepresentante
	 */
	public String getNomeRazaoRepresentante() {
		return nomeRazaoRepresentante;
	}

	/**
	 * Set: nomeRazaoRepresentante.
	 *
	 * @param nomeRazaoRepresentante the nome razao representante
	 */
	public void setNomeRazaoRepresentante(String nomeRazaoRepresentante) {
		this.nomeRazaoRepresentante = nomeRazaoRepresentante;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: segRepresentante.
	 *
	 * @return segRepresentante
	 */
	public String getSegRepresentante() {
		return segRepresentante;
	}

	/**
	 * Set: segRepresentante.
	 *
	 * @param segRepresentante the seg representante
	 */
	public void setSegRepresentante(String segRepresentante) {
		this.segRepresentante = segRepresentante;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: subSegRepresentante.
	 *
	 * @return subSegRepresentante
	 */
	public String getSubSegRepresentante() {
		return subSegRepresentante;
	}

	/**
	 * Set: subSegRepresentante.
	 *
	 * @param subSegRepresentante the sub seg representante
	 */
	public void setSubSegRepresentante(String subSegRepresentante) {
		this.subSegRepresentante = subSegRepresentante;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: vigenciaContrato.
	 *
	 * @return vigenciaContrato
	 */
	public String getVigenciaContrato() {
		return vigenciaContrato;
	}

	/**
	 * Set: vigenciaContrato.
	 *
	 * @param vigenciaContrato the vigencia contrato
	 */
	public void setVigenciaContrato(String vigenciaContrato) {
		this.vigenciaContrato = vigenciaContrato;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public String getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(String nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

	/**
	 * Get: dataFinal.
	 *
	 * @return dataFinal
	 */
	public Date getDataFinal() {
		return dataFinal;
	}

	/**
	 * Set: dataFinal.
	 *
	 * @param dataFinal the data final
	 */
	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	/**
	 * Get: dataInicial.
	 *
	 * @return dataInicial
	 */
	public Date getDataInicial() {
		return dataInicial;
	}

	/**
	 * Set: dataInicial.
	 *
	 * @param dataInicial the data inicial
	 */
	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	/**
	 * Get: cnpjHist.
	 *
	 * @return cnpjHist
	 */
	public String getCnpjHist() {
		return cnpjHist;
	}

	/**
	 * Set: cnpjHist.
	 *
	 * @param cnpjHist the cnpj hist
	 */
	public void setCnpjHist(String cnpjHist) {
		this.cnpjHist = cnpjHist;
	}

	/**
	 * Get: cnpjHist2.
	 *
	 * @return cnpjHist2
	 */
	public String getCnpjHist2() {
		return cnpjHist2;
	}

	/**
	 * Set: cnpjHist2.
	 *
	 * @param cnpjHist2 the cnpj hist2
	 */
	public void setCnpjHist2(String cnpjHist2) {
		this.cnpjHist2 = cnpjHist2;
	}

	/**
	 * Get: cnpjHist3.
	 *
	 * @return cnpjHist3
	 */
	public String getCnpjHist3() {
		return cnpjHist3;
	}

	/**
	 * Set: cnpjHist3.
	 *
	 * @param cnpjHist3 the cnpj hist3
	 */
	public void setCnpjHist3(String cnpjHist3) {
		this.cnpjHist3 = cnpjHist3;
	}

	/**
	 * Get: cpfHist.
	 *
	 * @return cpfHist
	 */
	public String getCpfHist() {
		return cpfHist;
	}

	/**
	 * Set: cpfHist.
	 *
	 * @param cpfHist the cpf hist
	 */
	public void setCpfHist(String cpfHist) {
		this.cpfHist = cpfHist;
	}

	/**
	 * Get: cpfHist2.
	 *
	 * @return cpfHist2
	 */
	public String getCpfHist2() {
		return cpfHist2;
	}

	/**
	 * Set: itemSelecionado2.
	 *
	 * @param itemSelecionado2 the item selecionado2
	 */
	public void setItemSelecionado2(String itemSelecionado2) {
		this.itemSelecionado2 = itemSelecionado2;
	}

	/**
	 * Get: itemSelecionadoPesquisa2.
	 *
	 * @return itemSelecionadoPesquisa2
	 */
	public Integer getItemSelecionadoPesquisa2() {
		return itemSelecionadoPesquisa2;
	}

	/**
	 * Set: itemSelecionadoPesquisa2.
	 *
	 * @param itemSelecionadoPesquisa2 the item selecionado pesquisa2
	 */
	public void setItemSelecionadoPesquisa2(Integer itemSelecionadoPesquisa2) {
		this.itemSelecionadoPesquisa2 = itemSelecionadoPesquisa2;
	}

	/**
	 * Get: listaPesquisa2.
	 *
	 * @return listaPesquisa2
	 */
	public List<ConsultarManParticipanteSaidaDTO> getListaPesquisa2() {
		return listaPesquisa2;
	}

	/**
	 * Set: listaPesquisa2.
	 *
	 * @param listaPesquisa2 the lista pesquisa2
	 */
	public void setListaPesquisa2(List<ConsultarManParticipanteSaidaDTO> listaPesquisa2) {
		this.listaPesquisa2 = listaPesquisa2;
	}

	/**
	 * Get: listaPesquisaControle2.
	 *
	 * @return listaPesquisaControle2
	 */
	public List<SelectItem> getListaPesquisaControle2() {
		return listaPesquisaControle2;
	}

	/**
	 * Set: listaPesquisaControle2.
	 *
	 * @param listaPesquisaControle2 the lista pesquisa controle2
	 */
	public void setListaPesquisaControle2(List<SelectItem> listaPesquisaControle2) {
		this.listaPesquisaControle2 = listaPesquisaControle2;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Is check all.
	 *
	 * @return true, if is check all
	 */
	public boolean isCheckAll() {
		return checkAll;
	}

	/**
	 * Set: checkAll.
	 *
	 * @param checkAll the check all
	 */
	public void setCheckAll(boolean checkAll) {
		this.checkAll = checkAll;
	}

	/**
	 * Is btn selecionar.
	 *
	 * @return true, if is btn selecionar
	 */
	public boolean isBtnSelecionar() {
		return btnSelecionar;
	}

	/**
	 * Set: btnSelecionar.
	 *
	 * @param btnSelecionar the btn selecionar
	 */
	public void setBtnSelecionar(boolean btnSelecionar) {
		this.btnSelecionar = btnSelecionar;
	}

	/**
	 * Get: listaSelecionados.
	 *
	 * @return listaSelecionados
	 */
	public List<ParticipantesDTO> getListaSelecionados() {
		return listaSelecionados;
	}

	/**
	 * Set: listaSelecionados.
	 *
	 * @param listaSelecionados the lista selecionados
	 */
	public void setListaSelecionados(List<ParticipantesDTO> listaSelecionados) {
		this.listaSelecionados = listaSelecionados;
	}

	/**
	 * Get: listaSelecionadosControle.
	 *
	 * @return listaSelecionadosControle
	 */
	public List<SelectItem> getListaSelecionadosControle() {
		return listaSelecionadosControle;
	}

	/**
	 * Set: listaSelecionadosControle.
	 *
	 * @param listaSelecionadosControle the lista selecionados controle
	 */
	public void setListaSelecionadosControle(List<SelectItem> listaSelecionadosControle) {
		this.listaSelecionadosControle = listaSelecionadosControle;
	}

	/**
	 * Get: cdEmpresaContrato.
	 *
	 * @return cdEmpresaContrato
	 */
	public Long getCdEmpresaContrato() {
		return cdEmpresaContrato;
	}

	/**
	 * Set: cdEmpresaContrato.
	 *
	 * @param cdEmpresaContrato the cd empresa contrato
	 */
	public void setCdEmpresaContrato(Long cdEmpresaContrato) {
		this.cdEmpresaContrato = cdEmpresaContrato;
	}

	/**
	 * Get: itemSelecionado2.
	 *
	 * @return itemSelecionado2
	 */
	public String getItemSelecionado2() {
		return itemSelecionado2;
	}

	/**
	 * Set: cpfHist2.
	 *
	 * @param cpfHist2 the cpf hist2
	 */
	public void setCpfHist2(String cpfHist2) {
		this.cpfHist2 = cpfHist2;
	}

	/**
	 * Is bloquear campos.
	 *
	 * @return true, if is bloquear campos
	 */
	public boolean isBloquearCampos() {
		return bloquearCampos;
	}

	/**
	 * Set: bloquearCampos.
	 *
	 * @param bloquearCampos the bloquear campos
	 */
	public void setBloquearCampos(boolean bloquearCampos) {
		this.bloquearCampos = bloquearCampos;
	}

	/**
	 * Get: cpfCnpjParticipanteDet.
	 *
	 * @return cpfCnpjParticipanteDet
	 */
	public String getCpfCnpjParticipanteDet() {
		return cpfCnpjParticipanteDet;
	}

	/**
	 * Set: cpfCnpjParticipanteDet.
	 *
	 * @param cpfCnpjParticipanteDet the cpf cnpj participante det
	 */
	public void setCpfCnpjParticipanteDet(String cpfCnpjParticipanteDet) {
		this.cpfCnpjParticipanteDet = cpfCnpjParticipanteDet;
	}

	/**
	 * Get: nomeRazaoParticipanteDet.
	 *
	 * @return nomeRazaoParticipanteDet
	 */
	public String getNomeRazaoParticipanteDet() {
		return nomeRazaoParticipanteDet;
	}

	/**
	 * Set: nomeRazaoParticipanteDet.
	 *
	 * @param nomeRazaoParticipanteDet the nome razao participante det
	 */
	public void setNomeRazaoParticipanteDet(String nomeRazaoParticipanteDet) {
		this.nomeRazaoParticipanteDet = nomeRazaoParticipanteDet;
	}

	/**
	 * Get: areaNegocio.
	 *
	 * @return areaNegocio
	 */
	public String getAreaNegocio() {
		return areaNegocio;
	}

	/**
	 * Set: areaNegocio.
	 *
	 * @param areaNegocio the area negocio
	 */
	public void setAreaNegocio(String areaNegocio) {
		this.areaNegocio = areaNegocio;
	}

	/**
	 * Get: motivoSituacao.
	 *
	 * @return motivoSituacao
	 */
	public String getMotivoSituacao() {
		return motivoSituacao;
	}

	/**
	 * Set: motivoSituacao.
	 *
	 * @param motivoSituacao the motivo situacao
	 */
	public void setMotivoSituacao(String motivoSituacao) {
		this.motivoSituacao = motivoSituacao;
	}

	/**
	 * Get: condicaoBloqueioDesb.
	 *
	 * @return condicaoBloqueioDesb
	 */
	public Integer getCondicaoBloqueioDesb() {
		return condicaoBloqueioDesb;
	}

	/**
	 * Set: condicaoBloqueioDesb.
	 *
	 * @param condicaoBloqueioDesb the condicao bloqueio desb
	 */
	public void setCondicaoBloqueioDesb(Integer condicaoBloqueioDesb) {
		this.condicaoBloqueioDesb = condicaoBloqueioDesb;
	}

	/**
	 * Get: listaMotivoBloqueioDesb.
	 *
	 * @return listaMotivoBloqueioDesb
	 */
	public List<SelectItem> getListaMotivoBloqueioDesb() {
		return listaMotivoBloqueioDesb;
	}

	/**
	 * Set: listaMotivoBloqueioDesb.
	 *
	 * @param listaMotivoBloqueioDesb the lista motivo bloqueio desb
	 */
	public void setListaMotivoBloqueioDesb(List<SelectItem> listaMotivoBloqueioDesb) {
		this.listaMotivoBloqueioDesb = listaMotivoBloqueioDesb;
	}

	/**
	 * Get: listaMotivoBloqueioDesbHash.
	 *
	 * @return listaMotivoBloqueioDesbHash
	 */
	public Map<Integer, String> getListaMotivoBloqueioDesbHash() {
		return listaMotivoBloqueioDesbHash;
	}

	/**
	 * Set lista motivo bloqueio desb hash.
	 *
	 * @param listaMotivoBloqueioDesbHash the lista motivo bloqueio desb hash
	 */
	public void setListaMotivoBloqueioDesbHash(Map<Integer, String> listaMotivoBloqueioDesbHash) {
		this.listaMotivoBloqueioDesbHash = listaMotivoBloqueioDesbHash;
	}

	/**
	 * Get: motivoBloqueioDesbloqueio.
	 *
	 * @return motivoBloqueioDesbloqueio
	 */
	public Integer getMotivoBloqueioDesbloqueio() {
		return motivoBloqueioDesbloqueio;
	}

	/**
	 * Set: motivoBloqueioDesbloqueio.
	 *
	 * @param motivoBloqueioDesbloqueio the motivo bloqueio desbloqueio
	 */
	public void setMotivoBloqueioDesbloqueio(Integer motivoBloqueioDesbloqueio) {
		this.motivoBloqueioDesbloqueio = motivoBloqueioDesbloqueio;
	}

	/**
	 * Get: dsMotivoBloqueioDesbloqueio.
	 *
	 * @return dsMotivoBloqueioDesbloqueio
	 */
	public String getDsMotivoBloqueioDesbloqueio() {
		return dsMotivoBloqueioDesbloqueio;
	}

	/**
	 * Set: dsMotivoBloqueioDesbloqueio.
	 *
	 * @param dsMotivoBloqueioDesbloqueio the ds motivo bloqueio desbloqueio
	 */
	public void setDsMotivoBloqueioDesbloqueio(String dsMotivoBloqueioDesbloqueio) {
		this.dsMotivoBloqueioDesbloqueio = dsMotivoBloqueioDesbloqueio;
	}

	/**
	 * Get: dsClassificacaoParticipante.
	 *
	 * @return dsClassificacaoParticipante
	 */
	public String getDsClassificacaoParticipante() {
		return dsClassificacaoParticipante;
	}

	/**
	 * Set: dsClassificacaoParticipante.
	 *
	 * @param dsClassificacaoParticipante the ds classificacao participante
	 */
	public void setDsClassificacaoParticipante(String dsClassificacaoParticipante) {
		this.dsClassificacaoParticipante = dsClassificacaoParticipante;
	}

	/**
	 * Get: cdClassificacaoParticipante.
	 *
	 * @return cdClassificacaoParticipante
	 */
	public Integer getCdClassificacaoParticipante() {
		return cdClassificacaoParticipante;
	}

	/**
	 * Set: cdClassificacaoParticipante.
	 *
	 * @param cdClassificacaoParticipante the cd classificacao participante
	 */
	public void setCdClassificacaoParticipante(Integer cdClassificacaoParticipante) {
		this.cdClassificacaoParticipante = cdClassificacaoParticipante;
	}

	/**
	 * Get: cdPessoaParticipante.
	 *
	 * @return cdPessoaParticipante
	 */
	public Long getCdPessoaParticipante() {
		return cdPessoaParticipante;
	}

	/**
	 * Set: cdPessoaParticipante.
	 *
	 * @param cdPessoaParticipante the cd pessoa participante
	 */
	public void setCdPessoaParticipante(Long cdPessoaParticipante) {
		this.cdPessoaParticipante = cdPessoaParticipante;
	}

	/**
	 * Get: cdTipoParticipante.
	 *
	 * @return cdTipoParticipante
	 */
	public Integer getCdTipoParticipante() {
		return cdTipoParticipante;
	}

	/**
	 * Set: cdTipoParticipante.
	 *
	 * @param cdTipoParticipante the cd tipo participante
	 */
	public void setCdTipoParticipante(Integer cdTipoParticipante) {
		this.cdTipoParticipante = cdTipoParticipante;
	}

	/**
	 * Get: tpManutencao.
	 *
	 * @return tpManutencao
	 */
	public String getTpManutencao() {
		return tpManutencao;
	}

	/**
	 * Set: tpManutencao.
	 *
	 * @param tpManutencao the tp manutencao
	 */
	public void setTpManutencao(String tpManutencao) {
		this.tpManutencao = tpManutencao;
	}

    /**
     * Get: participantesSelecionado.
     *
     * @return participantesSelecionado
     */
    public ParticipantesDTO getParticipantesSelecionado() {
        return participantesSelecionado;
    }

    /**
     * Set: participantesSelecionado.
     *
     * @param participantesSelecionado the participantes selecionado
     */
    public void setParticipantesSelecionado(ParticipantesDTO participantesSelecionado) {
        this.participantesSelecionado = participantesSelecionado;
    }

    /**
     * Get: bancoFiltroContasParticipantes.
     *
     * @return bancoFiltroContasParticipantes
     */
    public Integer getBancoFiltroContasParticipantes() {
        return bancoFiltroContasParticipantes;
    }

    /**
     * Set: bancoFiltroContasParticipantes.
     *
     * @param bancoFiltroContasParticipantes the banco filtro contas participantes
     */
    public void setBancoFiltroContasParticipantes(Integer bancoFiltroContasParticipantes) {
        this.bancoFiltroContasParticipantes = bancoFiltroContasParticipantes;
    }

    /**
     * Get: agenciaFiltroContasParticipantes.
     *
     * @return agenciaFiltroContasParticipantes
     */
    public Integer getAgenciaFiltroContasParticipantes() {
        return agenciaFiltroContasParticipantes;
    }

    /**
     * Set: agenciaFiltroContasParticipantes.
     *
     * @param agenciaFiltroContasParticipantes the agencia filtro contas participantes
     */
    public void setAgenciaFiltroContasParticipantes(Integer agenciaFiltroContasParticipantes) {
        this.agenciaFiltroContasParticipantes = agenciaFiltroContasParticipantes;
    }

    /**
     * Get: contaFiltroContasParticipantes.
     *
     * @return contaFiltroContasParticipantes
     */
    public Long getContaFiltroContasParticipantes() {
        return contaFiltroContasParticipantes;
    }

    /**
     * Set: contaFiltroContasParticipantes.
     *
     * @param contaFiltroContasParticipantes the conta filtro contas participantes
     */
    public void setContaFiltroContasParticipantes(Long contaFiltroContasParticipantes) {
        this.contaFiltroContasParticipantes = contaFiltroContasParticipantes;
    }

    /**
     * Get: listaContasPossiveisInclusao.
     *
     * @return listaContasPossiveisInclusao
     */
    public List<ListarContasPossiveisInclusaoSaidaDTO> getListaContasPossiveisInclusao() {
        return listaContasPossiveisInclusao;
    }

    /**
     * Set: listaContasPossiveisInclusao.
     *
     * @param listaContasPossiveisInclusao the lista contas possiveis inclusao
     */
    public void setListaContasPossiveisInclusao(List<ListarContasPossiveisInclusaoSaidaDTO> listaContasPossiveisInclusao) {
        this.listaContasPossiveisInclusao = listaContasPossiveisInclusao;
    }

    /**
     * Get: listaContasPossiveisInclusaoControle.
     *
     * @return listaContasPossiveisInclusaoControle
     */
    public List<SelectItem> getListaContasPossiveisInclusaoControle() {
        return listaContasPossiveisInclusaoControle;
    }

    /**
     * Set: listaContasPossiveisInclusaoControle.
     *
     * @param listaContasPossiveisInclusaoControle the lista contas possiveis inclusao controle
     */
    public void setListaContasPossiveisInclusaoControle(List<SelectItem> listaContasPossiveisInclusaoControle) {
        this.listaContasPossiveisInclusaoControle = listaContasPossiveisInclusaoControle;
    }

    /**
     * Get: listaContasParaInclusao.
     *
     * @return listaContasParaInclusao
     */
    public List<ListarContasPossiveisInclusaoSaidaDTO> getListaContasParaInclusao() {
        return listaContasParaInclusao;
    }

    /**
     * Set: listaContasParaInclusao.
     *
     * @param listaContasParaInclusao the lista contas para inclusao
     */
    public void setListaContasParaInclusao(List<ListarContasPossiveisInclusaoSaidaDTO> listaContasParaInclusao) {
        this.listaContasParaInclusao = listaContasParaInclusao;
    }

    /**
     * Get: listaContasParaInclusaoControle.
     *
     * @return listaContasParaInclusaoControle
     */
    public List<SelectItem> getListaContasParaInclusaoControle() {
        return listaContasParaInclusaoControle;
    }

    /**
     * Set: listaContasParaInclusaoControle.
     *
     * @param listaContasParaInclusaoControle the lista contas para inclusao controle
     */
    public void setListaContasParaInclusaoControle(List<SelectItem> listaContasParaInclusaoControle) {
        this.listaContasParaInclusaoControle = listaContasParaInclusaoControle;
    }

    /**
     * Is habilitar btn selecionar.
     *
     * @return true, if is habilitar btn selecionar
     */
    public boolean isHabilitarBtnSelecionar() {
        return habilitarBtnSelecionar;
    }

    /**
     * Set: habilitarBtnSelecionar.
     *
     * @param habilitarBtnSelecionar the habilitar btn selecionar
     */
    public void setHabilitarBtnSelecionar(boolean habilitarBtnSelecionar) {
        this.habilitarBtnSelecionar = habilitarBtnSelecionar;
    }

    /**
     * Is habilitar btn remover.
     *
     * @return true, if is habilitar btn remover
     */
    public boolean isHabilitarBtnRemover() {
        return habilitarBtnRemover;
    }

    /**
     * Set: habilitarBtnRemover.
     *
     * @param habilitarBtnRemover the habilitar btn remover
     */
    public void setHabilitarBtnRemover(boolean habilitarBtnRemover) {
        this.habilitarBtnRemover = habilitarBtnRemover;
    }

    /**
     * Is check all2.
     *
     * @return true, if is check all2
     */
    public boolean isCheckAll2() {
        return checkAll2;
    }

    /**
     * Set: checkAll2.
     *
     * @param checkAll2 the check all2
     */
    public void setCheckAll2(boolean checkAll2) {
        this.checkAll2 = checkAll2;
    }

    /**
     * Get: cdBanco.
     *
     * @return cdBanco
     */
    public Integer getCdBanco() {
        return cdBanco;
    }

    /**
     * Set: cdBanco.
     *
     * @param cdBanco the cd banco
     */
    public void setCdBanco(Integer cdBanco) {
        this.cdBanco = cdBanco;
    }

    /**
     * Get: cdAgencia.
     *
     * @return cdAgencia
     */
    public Integer getCdAgencia() {
        return cdAgencia;
    }

    /**
     * Set: cdAgencia.
     *
     * @param cdAgencia the cd agencia
     */
    public void setCdAgencia(Integer cdAgencia) {
        this.cdAgencia = cdAgencia;
    }

    /**
     * Get: cdConta.
     *
     * @return cdConta
     */
    public Long getCdConta() {
        return cdConta;
    }

    /**
     * Set: cdConta.
     *
     * @param cdConta the cd conta
     */
    public void setCdConta(Long cdConta) {
        this.cdConta = cdConta;
    }

    /**
     * Get: saidaListarContasExclusao.
     *
     * @return saidaListarContasExclusao
     */
    public ListarContasExclusaoSaidaDTO getSaidaListarContasExclusao() {
        return saidaListarContasExclusao;
    }

    /**
     * Set: saidaListarContasExclusao.
     *
     * @param saidaListarContasExclusao the saida listar contas exclusao
     */
    public void setSaidaListarContasExclusao(ListarContasExclusaoSaidaDTO saidaListarContasExclusao) {
        this.saidaListarContasExclusao = saidaListarContasExclusao;
    }

    /**
     * Get: cdPessoaJuridica.
     *
     * @return cdPessoaJuridica
     */
    public Long getCdPessoaJuridica() {
        return cdPessoaJuridica;
    }

    /**
     * Set: cdPessoaJuridica.
     *
     * @param cdPessoaJuridica the cd pessoa juridica
     */
    public void setCdPessoaJuridica(Long cdPessoaJuridica) {
        this.cdPessoaJuridica = cdPessoaJuridica;
    }

	public IContaComplementarService getFiltroContaComplementarImpl() {
		return filtroContaComplementarImpl;
	}

	public void setFiltroContaComplementarImpl(
			IContaComplementarService filtroContaComplementarImpl) {
		this.filtroContaComplementarImpl = filtroContaComplementarImpl;
	}

	public Integer getItemSelecionadoListaParticipantes() {
		return itemSelecionadoListaParticipantes;
	}

	public void setItemSelecionadoListaParticipantes(
			Integer itemSelecionadoListaParticipantes) {
		this.itemSelecionadoListaParticipantes = itemSelecionadoListaParticipantes;
	}
}