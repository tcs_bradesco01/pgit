/*
 * Nome: br.com.bradesco.web.pgit.view.enuns
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.enuns;

/**
 * Nome: SituacaoEnum
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public enum SituacaoEnum {
	
	/** Atributo ATIVO. */
	ATIVO(1, "Ativo"),
	
	/** Atributo BLOQUEADO. */
	BLOQUEADO(2, "Bloqueado"),
	
	/** Atributo INATIVO. */
	INATIVO(3, "Inativo"),
	
	/** Atributo EXCLUIDO. */
	EXCLUIDO(4, "Exclu�do");

	/** Atributo cdSituacao. */
	private Integer cdSituacao = null;
	
	/** Atributo dsSituacao. */
	private String dsSituacao = null;

	/**
	 * Situacao enum.
	 *
	 * @param cdSituacao the cd situacao
	 * @param dsSituacao the ds situacao
	 */
	private SituacaoEnum(Integer cdSituacao, String dsSituacao) {
		this.cdSituacao = cdSituacao;
		this.dsSituacao = dsSituacao;
	}

	/**
	 * Get: cdSituacao.
	 *
	 * @return cdSituacao
	 */
	public Integer getCdSituacao() {
		return cdSituacao;
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this.dsSituacao;
		
	}

}
