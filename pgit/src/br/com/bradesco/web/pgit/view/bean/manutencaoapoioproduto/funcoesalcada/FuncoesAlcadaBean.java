/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.funcoesalcada
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.funcoesalcada;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOperacoesServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOperacoesServicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoAcaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.IFuncoesAlcadaService;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.AlterarFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.AlterarFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.DetalharFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.DetalharFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ExcluirFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ExcluirFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.IncluirFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.IncluirFuncoesAlcadaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ListarFuncoesAlcadaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.funcoesalcada.bean.ListarFuncoesAlcadaSaidaDTO;

/**
 * Nome: FuncoesAlcadaBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class FuncoesAlcadaBean {
	
	// Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_FUNCOES_ALCADAS. */
	private static final String CON_FUNCOES_ALCADAS = "conFuncoesAlcadas";
	
	/** Atributo codigoAplicacaoFiltro. */
	private String codigoAplicacaoFiltro;
	
	/** Atributo acaoFiltro. */
	private Integer acaoFiltro;
	
	/** Atributo indicadorAcessoFiltro. */
	private Integer indicadorAcessoFiltro;
	
	/** Atributo codigoAplicacao. */
	private String codigoAplicacao;
	
	/** Atributo acao. */
	private Integer acao;
	
	/** Atributo dsAcao. */
	private String dsAcao;
	
	/** Atributo indicadorAcesso. */
	private Integer indicadorAcesso;
	
	/** Atributo regra. */
	private String regra;
	
	/** Atributo regraDesc. */
	private String regraDesc;
	
	/** Atributo servico. */
	private Integer servico;
	
	/** Atributo dsServico. */
	private String dsServico;
	
	/** Atributo servicoRelacionado. */
	private Integer servicoRelacionado;
	
	/** Atributo dsServicoRelacionado. */
	private String  dsServicoRelacionado;
	
	/** Atributo listaCentroCustoConsulta. */
	private List<SelectItem> listaCentroCustoConsulta = new ArrayList<SelectItem>();	
	
	/** Atributo centroCustoFiltro. */
	private String centroCustoFiltro;
	
	/** Atributo centroCustoPesqFiltro. */
	private String centroCustoPesqFiltro;
	
	
	/** Atributo listaCentroCusto. */
	private List<SelectItem> listaCentroCusto = new ArrayList<SelectItem>();
	
	/** Atributo listaCentroCustoHash. */
	private Map<String,String> listaCentroCustoHash = new HashMap<String,String>();
	
	/** Atributo centroCusto. */
	private String centroCusto;
	
	/** Atributo centroCustoDesc. */
	private String centroCustoDesc;
	
	/** Atributo centroCustoPesq. */
	private String centroCustoPesq;
	
	/** Atributo listaCentroCustoRetorno. */
	private List<SelectItem> listaCentroCustoRetorno = new ArrayList<SelectItem>();
	
	/** Atributo listaCentroCustoRetornoHash. */
	private Map<String,String> listaCentroCustoRetornoHash = new HashMap<String,String>();
	
	/** Atributo centroCustoRetorno. */
	private String centroCustoRetorno;
	
	/** Atributo centroCustoRetornoDesc. */
	private String centroCustoRetornoDesc;
	
	/** Atributo centroCustoRetornoPesq. */
	private String centroCustoRetornoPesq;
	
	
	/** Atributo listaAcao. */
	private List<SelectItem> listaAcao = new ArrayList<SelectItem>();
	
	/** Atributo listaServico. */
	private List<SelectItem> listaServico = new ArrayList<SelectItem>();
	
	/** Atributo listaServicoRelacionado. */
	private List<SelectItem> listaServicoRelacionado = new ArrayList<SelectItem>();
	
	/** Atributo mostraBotoes. */
	private boolean mostraBotoes;
	
	/** Atributo listaGridPesquisa. */
	private List<ListarFuncoesAlcadaSaidaDTO> listaGridPesquisa;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();
			
	/** Atributo codRegraRadio. */
	private Integer codRegraRadio;
	
	/** Atributo codListaRadio. */
	private Integer codListaRadio;
	
	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;	
	
	/** Atributo radio. */
	private Integer radio;
	
	
	/** Atributo listaTipoAcaoHash. */
	private Map<Integer,String> listaTipoAcaoHash = new HashMap<Integer,String>();
	
	/** Atributo listaServicoHash. */
	private Map<Integer,String> listaServicoHash = new HashMap<Integer,String>();	
	
	/** Atributo listaServicoRelacionadoHash. */
	private Map<Integer,String> listaServicoRelacionadoHash = new HashMap<Integer,String>();

	// controle de campos
	/** Atributo regraOuServicoSelecionado. */
	private String regraOuServicoSelecionado;
	
	/** Atributo habilitaRegra. */
	private boolean habilitaRegra;
	
	/** Atributo habilitaServico. */
	private boolean habilitaServico;
	
	/** Atributo habilitaServicoRelacionado. */
	private boolean habilitaServicoRelacionado;
	
	/** Atributo habilitaAssincrono. */
	private boolean habilitaAssincrono;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;	
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;	
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo tipoTratamentoAlcada. */
	private Integer tipoTratamentoAlcada;
	
	/** Atributo tipoRegraAlcada. */
	private String tipoRegraAlcada;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;

	/** Atributo codigoAplicacaoRetorno. */
	private String codigoAplicacaoRetorno;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo funcoesAlcadaImpl. */
	private IFuncoesAlcadaService funcoesAlcadaImpl;	
	
	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}


	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		
		setListaGridPesquisa(null);
		setCentroCustoFiltro("");
		setCodigoAplicacaoFiltro("");
		setAcaoFiltro(null);
		setIndicadorAcessoFiltro(null);
		setItemSelecionadoLista(null);
		
		//carregar combos na tela de consultar
		setCentroCustoPesqFiltro("");
		setCentroCusto("");
		setBtoAcionado(false);
		listarAcao();		
	}
	
	/**
	 * Get: codRegraRadio.
	 *
	 * @return codRegraRadio
	 */
	public Integer getCodRegraRadio() {
		return codRegraRadio;
	}

	/**
	 * Set: codRegraRadio.
	 *
	 * @param codRegraRadio the cod regra radio
	 */
	public void setCodRegraRadio(Integer codRegraRadio) {
		this.codRegraRadio = codRegraRadio;
	}


	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<ListarFuncoesAlcadaSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}
	

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(List<ListarFuncoesAlcadaSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Is mostra botoes.
	 *
	 * @return true, if is mostra botoes
	 */
	public boolean isMostraBotoes() {
		return mostraBotoes;
	}

	/**
	 * Set: mostraBotoes.
	 *
	 * @param mostraBotoes the mostra botoes
	 */
	public void setMostraBotoes(boolean mostraBotoes) {
		this.mostraBotoes = mostraBotoes;
	}
	
	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados(){
		
		this.setCentroCustoFiltro("");
		this.setCodigoAplicacaoFiltro("");
		this.setIndicadorAcessoFiltro(null);
		setCentroCustoPesqFiltro("");
		this.setAcaoFiltro(null);
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
		setListaCentroCustoConsulta(new ArrayList<SelectItem>());
		setBtoAcionado(false);
		
		return "ok";
	}

	/**
	 * Get: acaoFiltro.
	 *
	 * @return acaoFiltro
	 */
	public Integer getAcaoFiltro() {
		return acaoFiltro;
	}

	/**
	 * Set: acaoFiltro.
	 *
	 * @param acaoFiltro the acao filtro
	 */
	public void setAcaoFiltro(Integer acaoFiltro) {
		this.acaoFiltro = acaoFiltro;
	}

	/**
	 * Get: centroCustoFiltro.
	 *
	 * @return centroCustoFiltro
	 */
	public String getCentroCustoFiltro() {
		return centroCustoFiltro;
	}

	/**
	 * Set: centroCustoFiltro.
	 *
	 * @param centroCustoFiltro the centro custo filtro
	 */
	public void setCentroCustoFiltro(String centroCustoFiltro) {
		this.centroCustoFiltro = centroCustoFiltro;
	}

	/**
	 * Get: codigoAplicacaoFiltro.
	 *
	 * @return codigoAplicacaoFiltro
	 */
	public String getCodigoAplicacaoFiltro() {
		return codigoAplicacaoFiltro;
	}

	/**
	 * Set: codigoAplicacaoFiltro.
	 *
	 * @param codigoAplicacaoFiltro the codigo aplicacao filtro
	 */
	public void setCodigoAplicacaoFiltro(String codigoAplicacaoFiltro) {
		this.codigoAplicacaoFiltro = codigoAplicacaoFiltro;
	}

	/**
	 * Get: codListaRadio.
	 *
	 * @return codListaRadio
	 */
	public Integer getCodListaRadio() {
		return codListaRadio;
	}

	/**
	 * Set: codListaRadio.
	 *
	 * @param codListaRadio the cod lista radio
	 */
	public void setCodListaRadio(Integer codListaRadio) {
		this.codListaRadio = codListaRadio;
	}

	/**
	 * Get: indicadorAcessoFiltro.
	 *
	 * @return indicadorAcessoFiltro
	 */
	public Integer getIndicadorAcessoFiltro() {
		return indicadorAcessoFiltro;
	}

	/**
	 * Set: indicadorAcessoFiltro.
	 *
	 * @param indicadorAcessoFiltro the indicador acesso filtro
	 */
	public void setIndicadorAcessoFiltro(Integer indicadorAcessoFiltro) {
		this.indicadorAcessoFiltro = indicadorAcessoFiltro;
	}
			
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		return "ok";
	}
	
	/**
	 * Set: listaServico.
	 *
	 * @param listaServico the lista servico
	 */
	public void setListaServico(List<SelectItem> listaServico) {
		this.listaServico = listaServico;
	}

	/**
	 * Set: listaServicoRelacionado.
	 *
	 * @param listaServicoRelacionado the lista servico relacionado
	 */
	public void setListaServicoRelacionado(List<SelectItem> listaServicoRelacionado) {
		this.listaServicoRelacionado = listaServicoRelacionado;
	}
	
	/**
	 * Get: listaServico.
	 *
	 * @return listaServico
	 */
	public List<SelectItem> getListaServico() {
		return listaServico;
	}

	/**
	 * Get: listaServicoRelacionado.
	 *
	 * @return listaServicoRelacionado
	 */
	public List<SelectItem> getListaServicoRelacionado() {
		
		return listaServicoRelacionado;
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		
		//carregar combos na tela de incluir
		
		listarAcao();		
		listarServico();

		setAcao(0);
		setCodRegraRadio(-1);
		setCentroCusto("");
		setCodigoAplicacao("");
		setRegraOuServicoSelecionado("1");
		setRegra("");
		setServico(0);
		setServicoRelacionado(0);
		setTipoRegraAlcada("1");
		setTipoTratamentoAlcada(1);
		setHabilitaRegra(false);
		setHabilitaServico(true);
		setHabilitaServicoRelacionado(true);
		setHabilitaAssincrono(true);
		setObrigatoriedade("T");
		setCentroCustoRetorno("");
		setCodigoAplicacaoRetorno("");
		setCentroCusto("");
		setCentroCustoPesq("");
		setCentroCustoRetorno("");
		setCentroCustoRetornoPesq("");
		setListaCentroCusto(new ArrayList<SelectItem>());
		setListaCentroCustoRetorno(new ArrayList<SelectItem>());
		
		return "INCLUIR";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_FUNCOES_ALCADAS, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 

		return "EXCLUIR";
	}
	
	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar(){

		try {
			preencheDados();
			
			
				
			// Sincrono selecionado.	
			if (getTipoRegraAlcada().equals("1") ){
				setHabilitaAssincrono(true);
			}
			//Assincrono selecionado.
			if (getTipoRegraAlcada().equals("2") ){
				setHabilitaAssincrono(false);
			}	

			setRegraOuServicoSelecionado(String.valueOf(getIndicadorAcesso()));
			
			setHabilitaRegra(getIndicadorAcesso()!=1);
			setHabilitaServico(getIndicadorAcesso()!=2);
			setHabilitaServicoRelacionado(getIndicadorAcesso() != 2);
			
			listarServico();
			
			listarOperacao();
		
				
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_FUNCOES_ALCADAS, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 

		return "ALTERAR";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		
		if (getObrigatoriedade().equals("T")){
			
			setCentroCustoDesc((String)listaCentroCustoHash.get(getCentroCusto()));
			setCentroCustoRetornoDesc((String)listaCentroCustoHash.get(getCentroCustoRetorno()));
			setDsAcao((String)listaTipoAcaoHash.get(getAcao()));
			setDsServico((String)listaServicoHash.get(getServico()));
			setDsServicoRelacionado((String)listaServicoRelacionadoHash.get(getServicoRelacionado()));
			setIndicadorAcesso(Integer.parseInt(getRegraOuServicoSelecionado()));
			setCodigoAplicacao(getCodigoAplicacao().toUpperCase());
			setCodigoAplicacaoRetorno(getCodigoAplicacaoRetorno()!=null && !getCodigoAplicacaoRetorno().equals("")?getCodigoAplicacaoRetorno().toUpperCase():"");
			setCentroCustoRetornoDesc(getCentroCustoRetorno() != null && !getCentroCustoRetorno().equals("")?(String)listaCentroCustoRetornoHash.get(getCentroCustoRetorno()):"");
			return "AVANCAR_INCLUIR";
		}
		return "ok";
	}
	
	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar(){

		
		
		if (getObrigatoriedade().equals("T")){
			setCentroCustoDesc((String)listaCentroCustoHash.get(getCentroCusto()));
			setCentroCustoRetornoDesc((String)listaCentroCustoHash.get(getCentroCustoRetorno()));
			setDsServico((String)listaServicoHash.get(getServico()));
			setDsServicoRelacionado((String)listaServicoRelacionadoHash.get(getServicoRelacionado()));
			setIndicadorAcesso(Integer.parseInt(getRegraOuServicoSelecionado()));
			setRegraDesc(getRegra());
			setCodigoAplicacaoRetorno(getCodigoAplicacaoRetorno()!=null && !getCodigoAplicacaoRetorno().equals("")?getCodigoAplicacaoRetorno().toUpperCase():"");
			setCentroCustoRetornoDesc(getCentroCustoRetorno() != null && !getCentroCustoRetorno().equals("")?(String)listaCentroCustoRetornoHash.get(getCentroCustoRetorno()):"");
			return "AVANCAR_ALTERAR";
		}
		return "";
	}
	
	/**
	 * Preenche dados.
	 */
	private void preencheDados(){
		
		ListarFuncoesAlcadaSaidaDTO listarFuncoesAlcadaSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());
		
		DetalharFuncoesAlcadaEntradaDTO detalharFuncoesAlcadaEntradaDTO = new DetalharFuncoesAlcadaEntradaDTO();

		detalharFuncoesAlcadaEntradaDTO.setCentroCusto(listarFuncoesAlcadaSaidaDTO.getCentroCusto());
		detalharFuncoesAlcadaEntradaDTO.setCodigoAplicacao(listarFuncoesAlcadaSaidaDTO.getCodigoAplicacao());
		detalharFuncoesAlcadaEntradaDTO.setAcao(listarFuncoesAlcadaSaidaDTO.getCdAcao());
		detalharFuncoesAlcadaEntradaDTO.setIndicadorAcesso(listarFuncoesAlcadaSaidaDTO.getIndicadorAcesso());
		
		DetalharFuncoesAlcadaSaidaDTO detalharFuncoesAlcadaSaidaDTO = getFuncoesAlcadaImpl().detalharFuncoesAlcadas(detalharFuncoesAlcadaEntradaDTO);
		if (detalharFuncoesAlcadaSaidaDTO.getCentroCusto() != null && !detalharFuncoesAlcadaSaidaDTO.getCentroCusto().equals("")){
			setCentroCusto(detalharFuncoesAlcadaSaidaDTO.getCentroCusto());
			setCentroCustoDesc(detalharFuncoesAlcadaSaidaDTO.getCentroCusto() + "-" + detalharFuncoesAlcadaSaidaDTO.getDsCentroCusto());
		}else{
			setCentroCusto("");
			setCentroCustoDesc("");
		}
		setCodigoAplicacao(detalharFuncoesAlcadaSaidaDTO.getCodigoAplicacao());
		setAcao(detalharFuncoesAlcadaSaidaDTO.getCdAcao());
		setDsAcao(detalharFuncoesAlcadaSaidaDTO.getDsAcao());
		setIndicadorAcesso(detalharFuncoesAlcadaSaidaDTO.getIndicadorAcesso());
		setTipoRegraAlcada(String.valueOf(detalharFuncoesAlcadaSaidaDTO.getTipoRegraAlcada()));
		setTipoTratamentoAlcada(detalharFuncoesAlcadaSaidaDTO.getTipoTratamento());
		if (detalharFuncoesAlcadaSaidaDTO.getCentroCustoRetorno() != null && !detalharFuncoesAlcadaSaidaDTO.getCentroCustoRetorno().equals("")){
			setCentroCustoRetorno(detalharFuncoesAlcadaSaidaDTO.getCentroCustoRetorno());
			setCentroCustoRetornoDesc(detalharFuncoesAlcadaSaidaDTO.getCentroCustoRetorno() + " - " + detalharFuncoesAlcadaSaidaDTO.getDsCentroCustoRetorno());
			setCentroCustoRetornoPesq(detalharFuncoesAlcadaSaidaDTO.getCentroCustoRetorno());
			//buscarCentroCustoRetorno();
		}else{
			setCentroCustoRetorno("");
			setCentroCustoRetornoDesc("");
			setCentroCustoRetornoPesq("");
			setListaCentroCustoRetorno(new ArrayList<SelectItem>());
		}
		
		setCodigoAplicacaoRetorno(detalharFuncoesAlcadaSaidaDTO.getCodigoAplicacaoRetorno());
		if (detalharFuncoesAlcadaSaidaDTO.getIndicadorAcesso() == 2){
			setServico(detalharFuncoesAlcadaSaidaDTO.getCdServico());			
			setDsServico(detalharFuncoesAlcadaSaidaDTO.getDsServico());
			setServicoRelacionado(detalharFuncoesAlcadaSaidaDTO.getCdOperacao());
			setDsServicoRelacionado(detalharFuncoesAlcadaSaidaDTO.getDsOperacao());
		}else{
			setServico(0);			
			setDsServico("");
			setServicoRelacionado(0);
			setDsServicoRelacionado("");
		}
		if (detalharFuncoesAlcadaSaidaDTO.getIndicadorAcesso() == 1){
			setRegraDesc(detalharFuncoesAlcadaSaidaDTO.getCdRegra() != 0?detalharFuncoesAlcadaSaidaDTO.getCdRegra() + " - "  + detalharFuncoesAlcadaSaidaDTO.getDsRegra():"");		
			setRegra(String.valueOf(detalharFuncoesAlcadaSaidaDTO.getCdRegra()));
		}else{
			setRegraDesc("");		
			setRegra("");
		}
		
		setUsuarioInclusao(detalharFuncoesAlcadaSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharFuncoesAlcadaSaidaDTO.getUsuarioManutencao());
		
		setComplementoInclusao(detalharFuncoesAlcadaSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharFuncoesAlcadaSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharFuncoesAlcadaSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharFuncoesAlcadaSaidaDTO.getComplementoManutencao());
		
		setTipoCanalInclusao(detalharFuncoesAlcadaSaidaDTO.getCdCanalInclusao()==0? "" : detalharFuncoesAlcadaSaidaDTO.getCdCanalInclusao() + " - " +  detalharFuncoesAlcadaSaidaDTO.getDsCanalInclusao()); 
		setTipoCanalManutencao(detalharFuncoesAlcadaSaidaDTO.getCdCanalManutencao()==0? "" : detalharFuncoesAlcadaSaidaDTO.getCdCanalManutencao() + " - " + detalharFuncoesAlcadaSaidaDTO.getDsCanalManutencao()); 
		
		setDataHoraInclusao(detalharFuncoesAlcadaSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharFuncoesAlcadaSaidaDTO.getDataHoraManutencao());
		
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_FUNCOES_ALCADAS, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "DETALHAR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		
		try {
			IncluirFuncoesAlcadaEntradaDTO incluirFuncoesAlcadaEntradaDTO = new IncluirFuncoesAlcadaEntradaDTO();
			
			incluirFuncoesAlcadaEntradaDTO.setCodigoAplicacao(getCodigoAplicacao());
			incluirFuncoesAlcadaEntradaDTO.setAcao(getAcao());
			incluirFuncoesAlcadaEntradaDTO.setTipoAcesso(Integer.parseInt(getRegraOuServicoSelecionado()));
			incluirFuncoesAlcadaEntradaDTO.setTipoRegra(Integer.parseInt(getTipoRegraAlcada()));
			incluirFuncoesAlcadaEntradaDTO.setTipoTratamento(getTipoTratamentoAlcada());
			if (getCentroCustoRetorno()==null){
				setCentroCustoRetorno("");
			}
			incluirFuncoesAlcadaEntradaDTO.setCentroCustoRetorno(getCentroCustoRetorno());
			incluirFuncoesAlcadaEntradaDTO.setCentroCusto(getCentroCusto());
			if (getCodigoAplicacaoRetorno()==null){
				setCodigoAplicacaoRetorno("");
			}
			incluirFuncoesAlcadaEntradaDTO.setCodigoAplicacaoRetorno(getCodigoAplicacaoRetorno());
			incluirFuncoesAlcadaEntradaDTO.setServico(getServico());
			incluirFuncoesAlcadaEntradaDTO.setOperacao(getServicoRelacionado());			
			incluirFuncoesAlcadaEntradaDTO.setRegra(getRegra() !=null && !getRegra().equals("")?Integer.parseInt(getRegra()):0);
			
			IncluirFuncoesAlcadaSaidaDTO incluirFuncoesAlcadaSaidaDTO = getFuncoesAlcadaImpl().incluirFuncoesAlcada(incluirFuncoesAlcadaEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +incluirFuncoesAlcadaSaidaDTO.getCodMensagem() + ") " + incluirFuncoesAlcadaSaidaDTO.getMensagem(), CON_FUNCOES_ALCADAS, BradescoViewExceptionActionType.ACTION, false);
			carregarGrid();
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		
		return "";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		
		try {
			
			ExcluirFuncoesAlcadaEntradaDTO excluirFuncoesAlcadaEntradaDTO = new ExcluirFuncoesAlcadaEntradaDTO();
			
			ListarFuncoesAlcadaSaidaDTO listarFuncoesAlcadaSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());	
			
			excluirFuncoesAlcadaEntradaDTO.setCentroCusto(listarFuncoesAlcadaSaidaDTO.getCentroCusto());
			excluirFuncoesAlcadaEntradaDTO.setCodigoAplicacao(listarFuncoesAlcadaSaidaDTO.getCodigoAplicacao());
			excluirFuncoesAlcadaEntradaDTO.setAcao(listarFuncoesAlcadaSaidaDTO.getCdAcao());
			excluirFuncoesAlcadaEntradaDTO.setIndicadorAcesso(listarFuncoesAlcadaSaidaDTO.getIndicadorAcesso());
			
			ExcluirFuncoesAlcadaSaidaDTO excluirFuncoesAlcadaSaidaDTO = getFuncoesAlcadaImpl().excluirFuncoesAlcada(excluirFuncoesAlcadaEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + excluirFuncoesAlcadaSaidaDTO.getCodMensagem() + ") " + excluirFuncoesAlcadaSaidaDTO.getMensagem(), CON_FUNCOES_ALCADAS, BradescoViewExceptionActionType.ACTION, false);

			carregarGrid();
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	
	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar(){
		
		try {
			
			AlterarFuncoesAlcadaEntradaDTO alterarFuncoesAlcadaEntradaDTO = new AlterarFuncoesAlcadaEntradaDTO();
			
			ListarFuncoesAlcadaSaidaDTO listarFuncoesAlcadaSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());	
			
			alterarFuncoesAlcadaEntradaDTO.setCentroCusto(listarFuncoesAlcadaSaidaDTO.getCentroCusto());
			alterarFuncoesAlcadaEntradaDTO.setCodigoAplicacao(listarFuncoesAlcadaSaidaDTO.getCodigoAplicacao());
			alterarFuncoesAlcadaEntradaDTO.setAcao(listarFuncoesAlcadaSaidaDTO.getCdAcao());
			alterarFuncoesAlcadaEntradaDTO.setTipoAcesso(Integer.parseInt(getRegraOuServicoSelecionado()));
			alterarFuncoesAlcadaEntradaDTO.setTipoRegra(Integer.parseInt(getTipoRegraAlcada()));
			alterarFuncoesAlcadaEntradaDTO.setTipoTratamento(getTipoTratamentoAlcada());
			if (getCentroCustoRetorno()==null){
				setCentroCustoRetorno("");
			}
			alterarFuncoesAlcadaEntradaDTO.setCentroCustoRetorno(getCentroCustoRetorno());
			if (getCodigoAplicacaoRetorno()==null){
				setCodigoAplicacaoRetorno("");
			}
			alterarFuncoesAlcadaEntradaDTO.setCodigoAplicacaoRetorno(getCodigoAplicacaoRetorno());
			alterarFuncoesAlcadaEntradaDTO.setServico(getServico());
			alterarFuncoesAlcadaEntradaDTO.setOperacao(getServicoRelacionado());			
			alterarFuncoesAlcadaEntradaDTO.setRegra(getRegra()!=null && !getRegra().equals("")?Integer.parseInt(getRegra()):0);
			
			
			AlterarFuncoesAlcadaSaidaDTO alterarFuncoesAlcadaSaidaDTO = getFuncoesAlcadaImpl().alterarFuncoesAlcada(alterarFuncoesAlcadaEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + alterarFuncoesAlcadaSaidaDTO.getCodMensagem() + ") " + alterarFuncoesAlcadaSaidaDTO.getMensagem(), CON_FUNCOES_ALCADAS, BradescoViewExceptionActionType.ACTION, false);

			carregarGrid();
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";

	}

	/**
	 * Get: acao.
	 *
	 * @return acao
	 */
	public Integer getAcao() {		
		return acao;
	}

	/**
	 * Set: acao.
	 *
	 * @param acao the acao
	 */
	public void setAcao(Integer acao) {
		this.acao = acao;
	}

	/**
	 * Get: centroCusto.
	 *
	 * @return centroCusto
	 */
	public String getCentroCusto() {
		return centroCusto;
	}

	/**
	 * Set: centroCusto.
	 *
	 * @param centroCusto the centro custo
	 */
	public void setCentroCusto(String centroCusto) {
		this.centroCusto = centroCusto;
	}

	/**
	 * Get: codigoAplicacao.
	 *
	 * @return codigoAplicacao
	 */
	public String getCodigoAplicacao() {
		return codigoAplicacao;
	}

	/**
	 * Set: codigoAplicacao.
	 *
	 * @param codigoAplicacao the codigo aplicacao
	 */
	public void setCodigoAplicacao(String codigoAplicacao) {
		this.codigoAplicacao = codigoAplicacao;
	}

	/**
	 * Get: indicadorAcesso.
	 *
	 * @return indicadorAcesso
	 */
	public Integer getIndicadorAcesso() {
		return indicadorAcesso;
	}

	/**
	 * Set: indicadorAcesso.
	 *
	 * @param indicadorAcesso the indicador acesso
	 */
	public void setIndicadorAcesso(Integer indicadorAcesso) {
		this.indicadorAcesso = indicadorAcesso;
	}

	/**
	 * Get: servico.
	 *
	 * @return servico
	 */
	public Integer getServico() {
		return servico;
	}

	/**
	 * Set: servico.
	 *
	 * @param servico the servico
	 */
	public void setServico(Integer servico) {
		this.servico = servico;
	}

	/**
	 * Get: servicoRelacionado.
	 *
	 * @return servicoRelacionado
	 */
	public Integer getServicoRelacionado() {
		return servicoRelacionado;
	}

	/**
	 * Set: servicoRelacionado.
	 *
	 * @param servicoRelacionado the servico relacionado
	 */
	public void setServicoRelacionado(Integer servicoRelacionado) {
		this.servicoRelacionado = servicoRelacionado;
	}

	/**
	 * Get: regra.
	 *
	 * @return regra
	 */
	public String getRegra() {
		return regra;
	}

	/**
	 * Set: regra.
	 *
	 * @param regra the regra
	 */
	public void setRegra(String regra) {
		this.regra = regra;
	}

	/**
	 * Get: listaAcao.
	 *
	 * @return listaAcao
	 */
	public List<SelectItem> getListaAcao() {
		return listaAcao;
	}

	/**
	 * Set: listaAcao.
	 *
	 * @param listaAcao the lista acao
	 */
	public void setListaAcao(List<SelectItem> listaAcao) {
		this.listaAcao = listaAcao;
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}
	
	/**
	 * Get: radio.
	 *
	 * @return radio
	 */
	public Integer getRadio() {
		return radio;
	}

	/**
	 * Set: radio.
	 *
	 * @param radio the radio
	 */
	public void setRadio(Integer radio) {
		this.radio = radio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}
	
	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar() {
		
		carregarGrid();
		
		setItemSelecionadoLista(null);
		
		return "ok";
	}
	
	/**
	 * Carregar grid.
	 */
	private void carregarGrid() {
		
		try{
			
			ListarFuncoesAlcadaEntradaDTO entradaDTO = new ListarFuncoesAlcadaEntradaDTO();
							
			entradaDTO.setCentroCusto(getCentroCustoFiltro()!=null?getCentroCustoFiltro():"");
			entradaDTO.setCodigoAplicacao(getCodigoAplicacaoFiltro()!=null?getCodigoAplicacaoFiltro():"");
			entradaDTO.setAcao(getAcaoFiltro()!=null?getAcaoFiltro():0);
			entradaDTO.setIndicadorAcesso(getIndicadorAcessoFiltro()!=null?getIndicadorAcessoFiltro():0);

			setListaGridPesquisa(getFuncoesAlcadaImpl().listarFuncoesAlcadas(entradaDTO));
			setBtoAcionado(true);
			this.listaControleRadio =  new ArrayList<SelectItem>();
			for (int i = 0; i <= getListaGridPesquisa().size();i++) {
				this.listaControleRadio.add(new SelectItem(i," "));
			}
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridPesquisa(null);

		}
		
		
	}
	
	/**
	 * Habilita dados retorno.
	 */
	public void habilitaDadosRetorno(){
		
		setCentroCustoRetorno ("");
		setCentroCustoRetornoPesq("");
		setCodigoAplicacaoRetorno("");
		listaCentroCustoRetorno =  new ArrayList<SelectItem>();
		listaCentroCustoRetornoHash.clear();
		
		// Sincrono selecionado.	
		if (getTipoRegraAlcada().equals("1") ){
			setHabilitaAssincrono(true);
			return;
		}
		//Assincrono selecionado.
		if (getTipoRegraAlcada().equals("2") ){
			setHabilitaAssincrono(false);
			return;
		}	
	}

	
	/**
	 * Habilita regra ou servico.
	 */
	public void habilitaRegraOuServico(){
		setRegra("");
		setServico(0);
		listarOperacao();
		setServicoRelacionado(0);
		
		// regra selecionado.	
		if (getRegraOuServicoSelecionado().equals("1") ){
			setHabilitaRegra(false);
			setHabilitaServico(true);
			setHabilitaServicoRelacionado(true);
			
		}
		//servico selecionado.
		if (getRegraOuServicoSelecionado().equals("2") ){
			setHabilitaRegra(true);
			setHabilitaServico(false);
			setHabilitaServicoRelacionado(false);
		
		}	
	}

	/**
	 * Is habilita regra.
	 *
	 * @return true, if is habilita regra
	 */
	public boolean isHabilitaRegra() {
		return habilitaRegra;
	}

	/**
	 * Set: habilitaRegra.
	 *
	 * @param habilitaRegra the habilita regra
	 */
	public void setHabilitaRegra(boolean habilitaRegra) {
		this.habilitaRegra = habilitaRegra;
	}

	/**
	 * Is habilita servico.
	 *
	 * @return true, if is habilita servico
	 */
	public boolean isHabilitaServico() {
		return habilitaServico;
	}

	/**
	 * Set: habilitaServico.
	 *
	 * @param habilitaServico the habilita servico
	 */
	public void setHabilitaServico(boolean habilitaServico) {
		this.habilitaServico = habilitaServico;
	}

	/**
	 * Is habilita servico relacionado.
	 *
	 * @return true, if is habilita servico relacionado
	 */
	public boolean isHabilitaServicoRelacionado() {
		return habilitaServicoRelacionado;
	}

	/**
	 * Set: habilitaServicoRelacionado.
	 *
	 * @param habilitaServicoRelacionado the habilita servico relacionado
	 */
	public void setHabilitaServicoRelacionado(boolean habilitaServicoRelacionado) {
		this.habilitaServicoRelacionado = habilitaServicoRelacionado;
	}

	/**
	 * Get: regraOuServicoSelecionado.
	 *
	 * @return regraOuServicoSelecionado
	 */
	public String getRegraOuServicoSelecionado() {
		return regraOuServicoSelecionado;
	}

	/**
	 * Set: regraOuServicoSelecionado.
	 *
	 * @param regraOuServicoSelecionado the regra ou servico selecionado
	 */
	public void setRegraOuServicoSelecionado(String regraOuServicoSelecionado) {
		this.regraOuServicoSelecionado = regraOuServicoSelecionado;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	

	
	/**
	 * Listar acao.
	 */
	private void listarAcao() {
		try{
			this.listaAcao = new ArrayList<SelectItem>();
			
			List<TipoAcaoSaidaDTO> listaTipoAcaoSaida = new ArrayList<TipoAcaoSaidaDTO>();
			
			listaTipoAcaoSaida = comboService.listarTipoAcao();
			
			listaTipoAcaoHash.clear();
			for(TipoAcaoSaidaDTO combo : listaTipoAcaoSaida){
				listaTipoAcaoHash.put(combo.getCdAcao(),combo.getDsAcao());
				this.listaAcao.add(new SelectItem(combo.getCdAcao(),combo.getDsAcao()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaAcao = new ArrayList<SelectItem>();
		}
			
		
	}
	
	/**
	 * Listar operacao.
	 */
	public void listarOperacao() {
		
		if (getServico() !=null && getServico() != 0){
			this.listaServicoRelacionado = new ArrayList<SelectItem>();
			
			List<ListarOperacoesServicoSaidaDTO> saida = new ArrayList<ListarOperacoesServicoSaidaDTO>();
			
			ListarOperacoesServicosEntradaDTO entrada = new ListarOperacoesServicosEntradaDTO (); 
			entrada.setCdProduto(getServico());
			
			saida = comboService.listarOperacoesServicos(entrada);
			
			listaServicoRelacionadoHash.clear();
			for(ListarOperacoesServicoSaidaDTO combo : saida){
				listaServicoRelacionadoHash.put(combo.getCdOperacao(),combo.getDsOperacao());
				this.listaServicoRelacionado.add(new SelectItem(combo.getCdOperacao(),combo.getDsOperacao()));
			}
		}else{
			listaServicoRelacionadoHash.clear();
			listaServicoRelacionado.clear();
				
		}
		
	}

	/**
	 * Get: listaCentroCustoConsulta.
	 *
	 * @return listaCentroCustoConsulta
	 */
	public List<SelectItem> getListaCentroCustoConsulta() {
		return listaCentroCustoConsulta;
		
	}

	/**
	 * Set: listaCentroCustoConsulta.
	 *
	 * @param listaCentroCusto the lista centro custo consulta
	 */
	public void setListaCentroCustoConsulta(List<SelectItem> listaCentroCusto) {
		this.listaCentroCustoConsulta = listaCentroCusto;
	}

	/**
	 * Get: tipoTratamentoAlcada.
	 *
	 * @return tipoTratamentoAlcada
	 */
	public Integer getTipoTratamentoAlcada() {
		return tipoTratamentoAlcada;
	}

	/**
	 * Set: tipoTratamentoAlcada.
	 *
	 * @param tipoTratamentoAlcada the tipo tratamento alcada
	 */
	public void setTipoTratamentoAlcada(Integer tipoTratamentoAlcada) {
		this.tipoTratamentoAlcada = tipoTratamentoAlcada;
	}

	/**
	 * Get: tipoRegraAlcada.
	 *
	 * @return tipoRegraAlcada
	 */
	public String getTipoRegraAlcada() {
		return tipoRegraAlcada;
	}

	/**
	 * Set: tipoRegraAlcada.
	 *
	 * @param tipoRegraAlcada the tipo regra alcada
	 */
	public void setTipoRegraAlcada(String tipoRegraAlcada) {
		this.tipoRegraAlcada = tipoRegraAlcada;
	}

	/**
	 * Get: centroCustoRetorno.
	 *
	 * @return centroCustoRetorno
	 */
	public String getCentroCustoRetorno() {
		return centroCustoRetorno;
	}

	/**
	 * Set: centroCustoRetorno.
	 *
	 * @param centroCustoRetorno the centro custo retorno
	 */
	public void setCentroCustoRetorno(String centroCustoRetorno) {
		this.centroCustoRetorno = centroCustoRetorno;
	}

	/**
	 * Get: codigoAplicacaoRetorno.
	 *
	 * @return codigoAplicacaoRetorno
	 */
	public String getCodigoAplicacaoRetorno() {
		return codigoAplicacaoRetorno;
	}

	/**
	 * Set: codigoAplicacaoRetorno.
	 *
	 * @param codigoAplicacaoRetorno the codigo aplicacao retorno
	 */
	public void setCodigoAplicacaoRetorno(String codigoAplicacaoRetorno) {
		this.codigoAplicacaoRetorno = codigoAplicacaoRetorno;
	}

	/**
	 * Get: concatTipoCanal.
	 *
	 * @return concatTipoCanal
	 */
	public String getConcatTipoCanal() {
		return this.tipoCanalInclusao + " - " + this.tipoCanalManutencao;
		
	}

	/**
	 * Get: listaCentroCustoHash.
	 *
	 * @return listaCentroCustoHash
	 */
	Map<String, String> getListaCentroCustoHash() {
		return listaCentroCustoHash;
	}

	/**
	 * Set lista centro custo hash.
	 *
	 * @param listaCentroCustoHash the lista centro custo hash
	 */
	void setListaCentroCustoHash(Map<String, String> listaCentroCustoHash) {
		this.listaCentroCustoHash = listaCentroCustoHash;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: funcoesAlcadaImpl.
	 *
	 * @return funcoesAlcadaImpl
	 */
	public IFuncoesAlcadaService getFuncoesAlcadaImpl() {
		return funcoesAlcadaImpl;
	}

	/**
	 * Set: funcoesAlcadaImpl.
	 *
	 * @param funcoesAlcadaImpl the funcoes alcada impl
	 */
	public void setFuncoesAlcadaImpl(IFuncoesAlcadaService funcoesAlcadaImpl) {
		this.funcoesAlcadaImpl = funcoesAlcadaImpl;
	}

	/**
	 * Get: listaTipoAcaoHash.
	 *
	 * @return listaTipoAcaoHash
	 */
	Map<Integer, String> getListaTipoAcaoHash() {
		return listaTipoAcaoHash;
	}

	/**
	 * Set lista tipo acao hash.
	 *
	 * @param listaTipoAcaoHash the lista tipo acao hash
	 */
	void setListaTipoAcaoHash(Map<Integer, String> listaTipoAcaoHash) {
		this.listaTipoAcaoHash = listaTipoAcaoHash;
	}

	/**
	 * Is habilita assincrono.
	 *
	 * @return true, if is habilita assincrono
	 */
	public boolean isHabilitaAssincrono() {
		return habilitaAssincrono;
	}

	/**
	 * Set: habilitaAssincrono.
	 *
	 * @param habilitaAssincrono the habilita assincrono
	 */
	public void setHabilitaAssincrono(boolean habilitaAssincrono) {
		this.habilitaAssincrono = habilitaAssincrono;
	}
	
	/**
	 * Listar servico.
	 */
	private void listarServico() {
	
		try{
			this.listaServico = new ArrayList<SelectItem>();
			
			List<ListarServicosSaidaDTO> listaServicoSaida = new ArrayList<ListarServicosSaidaDTO>();
			
			
			listaServicoSaida = comboService.listarTipoServicos(1);
			
			listaServicoHash.clear();
			for(ListarServicosSaidaDTO combo : listaServicoSaida){
				listaServicoHash.put(combo.getCdServico(),combo.getDsServico());
				this.listaServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
			}
		}catch(PdcAdapterFunctionalException e){
			this.listaServico = new ArrayList<SelectItem>();
		}
			
		
	}

	/**
	 * Get: listaCentroCustoRetornoHash.
	 *
	 * @return listaCentroCustoRetornoHash
	 */
	Map<String, String> getListaCentroCustoRetornoHash() {
		return listaCentroCustoRetornoHash;
	}

	/**
	 * Set lista centro custo retorno hash.
	 *
	 * @param listaCentroCustoRetornoHash the lista centro custo retorno hash
	 */
	void setListaCentroCustoRetornoHash(
			Map<String, String> listaCentroCustoRetornoHash) {
		this.listaCentroCustoRetornoHash = listaCentroCustoRetornoHash;
	}
	
	/**
	 * Get: listaServicoHash.
	 *
	 * @return listaServicoHash
	 */
	Map<Integer, String> getListaServicoHash() {
		return listaServicoHash;
	}

	/**
	 * Set lista servico hash.
	 *
	 * @param listaServicoHash the lista servico hash
	 */
	void setListaServicoHash(Map<Integer, String> listaServicoHash) {
		this.listaServicoHash = listaServicoHash;
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		setItemSelecionadoLista(null);
		return "VOLTAR_INCLUIR";
		
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		setItemSelecionadoLista(null);
		return "conFuncoesAlcadas";
	}

	/**
	 * Get: listaServicoRelacionadoHash.
	 *
	 * @return listaServicoRelacionadoHash
	 */
	Map<Integer, String> getListaServicoRelacionadoHash() {
		return listaServicoRelacionadoHash;
	}

	/**
	 * Set lista servico relacionado hash.
	 *
	 * @param listaServicoRelacionadoHash the lista servico relacionado hash
	 */
	void setListaServicoRelacionadoHash(
			Map<Integer, String> listaServicoRelacionadoHash) {
		this.listaServicoRelacionadoHash = listaServicoRelacionadoHash;
	}

	/**
	 * Get: dsAcao.
	 *
	 * @return dsAcao
	 */
	public String getDsAcao() {
		return dsAcao;
	}

	/**
	 * Set: dsAcao.
	 *
	 * @param dsAcao the ds acao
	 */
	public void setDsAcao(String dsAcao) {
		this.dsAcao = dsAcao;
	}

	/**
	 * Get: dsServico.
	 *
	 * @return dsServico
	 */
	public String getDsServico() {
		return dsServico;
	}

	/**
	 * Set: dsServico.
	 *
	 * @param dsServico the ds servico
	 */
	public void setDsServico(String dsServico) {
		this.dsServico = dsServico;
	}

	/**
	 * Get: dsServicoRelacionado.
	 *
	 * @return dsServicoRelacionado
	 */
	public String getDsServicoRelacionado() {
		return dsServicoRelacionado;
	}

	/**
	 * Set: dsServicoRelacionado.
	 *
	 * @param dsServicoRelacionado the ds servico relacionado
	 */
	public void setDsServicoRelacionado(String dsServicoRelacionado) {
		this.dsServicoRelacionado = dsServicoRelacionado;
	}

	/**
	 * Get: centroCustoDesc.
	 *
	 * @return centroCustoDesc
	 */
	public String getCentroCustoDesc() {
		return centroCustoDesc;
	}

	/**
	 * Set: centroCustoDesc.
	 *
	 * @param centroCustoDesc the centro custo desc
	 */
	public void setCentroCustoDesc(String centroCustoDesc) {
		this.centroCustoDesc = centroCustoDesc;
	}

	/**
	 * Get: centroCustoRetornoDesc.
	 *
	 * @return centroCustoRetornoDesc
	 */
	public String getCentroCustoRetornoDesc() {
		return centroCustoRetornoDesc;
	}

	/**
	 * Set: centroCustoRetornoDesc.
	 *
	 * @param centroCustoRetornoDesc the centro custo retorno desc
	 */
	public void setCentroCustoRetornoDesc(String centroCustoRetornoDesc) {
		this.centroCustoRetornoDesc = centroCustoRetornoDesc;
	}

	/**
	 * Get: regraDesc.
	 *
	 * @return regraDesc
	 */
	public String getRegraDesc() {
		return regraDesc;
	}

	/**
	 * Set: regraDesc.
	 *
	 * @param regraDesc the regra desc
	 */
	public void setRegraDesc(String regraDesc) {
		this.regraDesc = regraDesc;
	}

	/**
	 * Get: centroCustoPesq.
	 *
	 * @return centroCustoPesq
	 */
	public String getCentroCustoPesq() {
		return centroCustoPesq;
	}

	/**
	 * Set: centroCustoPesq.
	 *
	 * @param centroCustoPesq the centro custo pesq
	 */
	public void setCentroCustoPesq(String centroCustoPesq) {
		this.centroCustoPesq = centroCustoPesq;
	}

	/**
	 * Get: centroCustoPesqFiltro.
	 *
	 * @return centroCustoPesqFiltro
	 */
	public String getCentroCustoPesqFiltro() {
		return centroCustoPesqFiltro;
	}

	/**
	 * Set: centroCustoPesqFiltro.
	 *
	 * @param centroCustoPesqFiltro the centro custo pesq filtro
	 */
	public void setCentroCustoPesqFiltro(String centroCustoPesqFiltro) {
		this.centroCustoPesqFiltro = centroCustoPesqFiltro;
	}
	
	
	/**
	 * Buscar centro custo consultar.
	 */
	public void buscarCentroCustoConsultar(){
		
		if (getCentroCustoPesqFiltro()!=null && !getCentroCustoPesqFiltro().equals("")){
			
			this.listaCentroCustoConsulta= new ArrayList<SelectItem>();			
			List<CentroCustoSaidaDTO> listaCentroCustoSaida = new ArrayList<CentroCustoSaidaDTO>();
			CentroCustoEntradaDTO centroCustoEntradaDTO = new CentroCustoEntradaDTO();			
			centroCustoEntradaDTO.setCdSistema(getCentroCustoPesqFiltro());
			
			listaCentroCustoSaida = comboService.listarCentroCusto(centroCustoEntradaDTO);
			
			for(CentroCustoSaidaDTO combo : listaCentroCustoSaida){				
				this.listaCentroCustoConsulta.add(new SelectItem(combo.getCdCentroCusto(), combo.getCdCentroCusto() + "-" +combo.getDsCentroCusto()));
			}
		}else{
			this.listaCentroCustoConsulta= new ArrayList<SelectItem>();
		}		
			
	}
	
	/**
	 * Buscar centro custo.
	 */
	public void buscarCentroCusto(){
		
		if (getCentroCustoPesq()!=null && !getCentroCustoPesq().equals("")){
			
			this.listaCentroCusto= new ArrayList<SelectItem>();
			
			List<CentroCustoSaidaDTO> listaCentroCustoSaida = new ArrayList<CentroCustoSaidaDTO>();
			CentroCustoEntradaDTO centroCustoEntradaDTO = new CentroCustoEntradaDTO();
			
			centroCustoEntradaDTO.setCdSistema(getCentroCustoPesq());
			
			listaCentroCustoSaida = comboService.listarCentroCusto(centroCustoEntradaDTO);
			
			listaCentroCustoHash.clear();
			for(CentroCustoSaidaDTO combo : listaCentroCustoSaida){
				listaCentroCustoHash.put(combo.getCdCentroCusto(), combo.getCdCentroCusto() + "-"+combo.getDsCentroCusto());
				this.listaCentroCusto.add(new SelectItem(combo.getCdCentroCusto(), combo.getCdCentroCusto() + "-"+combo.getDsCentroCusto()));
			}
		}else{
			listaCentroCustoHash.clear();
			this.listaCentroCusto= new ArrayList<SelectItem>();
		}		
			
	}
	
	/**
	 * Buscar centro custo retorno.
	 */
	public void buscarCentroCustoRetorno(){
		
		if (getCentroCustoRetornoPesq()!=null && !getCentroCustoRetornoPesq().equals("")){
			
			this.listaCentroCustoRetorno= new ArrayList<SelectItem>();
			
			List<CentroCustoSaidaDTO> listaCentroCustoSaida = new ArrayList<CentroCustoSaidaDTO>();
			CentroCustoEntradaDTO centroCustoEntradaDTO = new CentroCustoEntradaDTO();
			
			centroCustoEntradaDTO.setCdSistema(getCentroCustoRetornoPesq());
			
			listaCentroCustoSaida = comboService.listarCentroCusto(centroCustoEntradaDTO);
			
			listaCentroCustoRetornoHash.clear();
			for(CentroCustoSaidaDTO combo : listaCentroCustoSaida){
				listaCentroCustoRetornoHash.put(combo.getCdCentroCusto(), combo.getCdCentroCusto() + "-"+combo.getDsCentroCusto());
				this.listaCentroCustoRetorno.add(new SelectItem(combo.getCdCentroCusto(), combo.getCdCentroCusto() + "-"+combo.getDsCentroCusto()));
			}
		}else{
			listaCentroCustoRetornoHash.clear();
			this.listaCentroCustoRetorno= new ArrayList<SelectItem>();
		}		
			
	}

	/**
	 * Get: centroCustoRetornoPesq.
	 *
	 * @return centroCustoRetornoPesq
	 */
	public String getCentroCustoRetornoPesq() {
		return centroCustoRetornoPesq;
	}

	/**
	 * Set: centroCustoRetornoPesq.
	 *
	 * @param centroCustoRetornoPesq the centro custo retorno pesq
	 */
	public void setCentroCustoRetornoPesq(String centroCustoRetornoPesq) {
		this.centroCustoRetornoPesq = centroCustoRetornoPesq;
	}

	/**
	 * Get: listaCentroCusto.
	 *
	 * @return listaCentroCusto
	 */
	public List<SelectItem> getListaCentroCusto() {
		return listaCentroCusto;
	}

	/**
	 * Set: listaCentroCusto.
	 *
	 * @param listaCentroCusto the lista centro custo
	 */
	public void setListaCentroCusto(List<SelectItem> listaCentroCusto) {
		this.listaCentroCusto = listaCentroCusto;
	}

	/**
	 * Get: listaCentroCustoRetorno.
	 *
	 * @return listaCentroCustoRetorno
	 */
	public List<SelectItem> getListaCentroCustoRetorno() {
		return listaCentroCustoRetorno;
	}

	/**
	 * Set: listaCentroCustoRetorno.
	 *
	 * @param listaCentroCustoRetorno the lista centro custo retorno
	 */
	public void setListaCentroCustoRetorno(List<SelectItem> listaCentroCustoRetorno) {
		this.listaCentroCustoRetorno = listaCentroCustoRetorno;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	
} // fim	


