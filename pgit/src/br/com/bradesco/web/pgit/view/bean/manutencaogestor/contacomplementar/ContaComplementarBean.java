/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaogestor.contacomplementar
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 18/10/2017
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaogestor.contacomplementar;

import static br.com.bradesco.web.pgit.view.enuns.TipoMontagemEnum.TIPO_SERVICO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarContasVinculadasContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarContasVinculadasContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.AlterarContaComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.DetalharContaComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.DetalharContaComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.IncluirContaComplementarEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.IncluirContaComplementarSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplParticipantesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplParticipantesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplementarEntrada;
import br.com.bradesco.web.pgit.service.business.contacomplementar.bean.ListarContaComplementarOcorrenciasSaida;
import br.com.bradesco.web.pgit.service.business.contrato.IContratoService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.IImprimirAnexoPrimeiroContratoService;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.AnexoContratoDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasContas;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasLayout;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasParticipantes;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.IImprimirAnexoSegundoContratoService;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.PagamentosDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ServicosDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ServicosModalidadesDTO;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.IImprimircontratoService;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.IManterCadContaDestinoService;
import br.com.bradesco.web.pgit.service.business.mantercadcontadestino.bean.ConsultarBancoAgenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarListaTipoRetornoLayoutSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarTarifaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarTarifaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarPropostaAndamentoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarUsuarioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarUsuarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.IManterPerfilTrocaArqLayoutService;
import br.com.bradesco.web.pgit.service.business.manterperfiltrocaarqlayout.bean.ConsultarListaPerfilTrocaArquivoSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.ContasBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.DadosBasicosBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.LayoutsArquivosBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.MeiosTransmissaoBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.ParticipantesBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.RepresentanteBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.ServicosBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.TarifasBean;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

public class ContaComplementarBean {

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo manterContratoContasBean. */
	private ContasBean manterContratoContasBean;

	/** Atributo manterContratoRepresentanteBean. */
	private RepresentanteBean manterContratoRepresentanteBean;

	/** Atributo manterContratoParticipantesBean. */
	private ParticipantesBean manterContratoParticipantesBean;

	/** Atributo manterContratoServicosBean. */
	private ServicosBean manterContratoServicosBean;

	/** Atributo manterContratoDadosBasicosBean. */
	private DadosBasicosBean manterContratoDadosBasicosBean;

	/** Atributo manterContratoLayoutsArquivosBean. */
	private LayoutsArquivosBean manterContratoLayoutsArquivosBean;

	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

	/** Atributo meiosTransmissaoBean. */
	private MeiosTransmissaoBean meiosTransmissaoBean;

	/** Atributo manterContratoTarifasBean. */
	private TarifasBean manterContratoTarifasBean;

	/** Atributo imprimirContratoService. */
	private IImprimircontratoService imprimirContratoService;

	/** Atributo contratoPgitSelecionado. */
	private ListarContratosPgitSaidaDTO contratoPgitSelecionado = new ListarContratosPgitSaidaDTO();;

	/** Atributo bancoAgenciaDTO. */
	private ConsultarBancoAgenciaSaidaDTO bancoAgenciaDTO = new ConsultarBancoAgenciaSaidaDTO();

	/** Atributo imprimirService. */
	private IImprimirService imprimirService = null;

	/** Atributo imprimirAnexoPrimeiroContratoService. */
	private IImprimirAnexoPrimeiroContratoService imprimirAnexoPrimeiroContratoService;

	/** Atributo manterCadContaDestinoService. */
	private IManterCadContaDestinoService manterCadContaDestinoService;

	/** Atributo imprimirAnexoSegundoContratoService. */
	private IImprimirAnexoSegundoContratoService imprimirAnexoSegundoContratoService;

	/** Atributo manterPerfilTrocaArqLayoutServiceImpl. */
	private IManterPerfilTrocaArqLayoutService manterPerfilTrocaArqLayoutServiceImpl;

	/** Atributo saidaMeioTransmissao. */
	private ListarContratosPgitSaidaDTO saidaMeioTransmissao;

	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado;

	/** Atributo cpfCnpjMaster. */
	private String cpfCnpjMaster;

	/** Atributo cpfCnpjMaster. */
	private List<ListarServicosSaidaDTO> dsServicoSelecionado = new ArrayList<ListarServicosSaidaDTO>(1);

	/** Atributo nomeRazaoSocialMaster. */
	private String nomeRazaoSocialMaster;

	/** Atributo grupoEconomicoMaster. */
	private String grupoEconomicoMaster;

	/** Atributo atividadeEconomicaMaster. */
	private String atividadeEconomicaMaster;

	/** Atributo segmentoMaster. */
	private String segmentoMaster;

	/** Atributo subSegmentoMaster. */
	private String subSegmentoMaster;

	/** Atributo empresa. */
	private String empresa;

	/** Atributo cdEmpresa. */
	private Long cdEmpresa;

	/** Atributo tipo. */
	private String tipo;

	/** Atributo cdTipo. */
	private Integer cdTipo;

	/** Atributo numero. */
	private String numero;

	/** Atributo numeroContrato. */
	private Long numeroContrato;

	/** Atributo motivoDesc. */
	private String motivoDesc;

	/** Atributo situacaoDesc. */
	private String situacaoDesc;

	/** Atributo participacao. */
	private String participacao;

	/** Atributo possuiAditivos. */
	private String possuiAditivos;

	/** Atributo dataHoraCadastramento. */
	private String dataHoraCadastramento;

	/** Atributo inicioVigencia. */
	private String inicioVigencia;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo cdAgenciaGestora. */
	private int cdAgenciaGestora;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;

	/** Atributo cdGerenteResponsavel. */
	private Long cdGerenteResponsavel;

	/** Atributo dsNomeRazaoSocialParticipante. */
	private String dsNomeRazaoSocialParticipante;

	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo cpfCnpj. */
	private String cpfCnpj;

	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;

	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos = false;

	/** Atributo renegociavel. */
	private String renegociavel;

	/** Atributo codigoAgenciaGestora. */
	private Integer codigoAgenciaGestora;

	/** Atributo descAgenciaGestora. */
	private Integer descAgenciaGestora;

	/** Atributo codDescAgenciaGestora. */
	private String codDescAgenciaGestora;

	/** Atributo saidaValidar. */
	private ValidarPropostaAndamentoSaidaDTO saidaValidar;

	/** Atributo urlRenegociacaoNovo. */
	private String urlRenegociacaoNovo;

	/** Atributo saidaValidarUsuario. */
	private ValidarUsuarioSaidaDTO saidaValidarUsuario;

	/** Atributo entradaValidarUsuario. */
	private ValidarUsuarioEntradaDTO entradaValidarUsuario;

	/** Atributo restringirAcessoBotoes. */
	private boolean restringirAcessoBotoes;

	/** Atributo dsServicoSegundaTela. */
	private String dsServicoSegundaTela;

	/** The saida lista perfil troca arquivo. */
	private ConsultarListaPerfilTrocaArquivoSaidaDTO saidaListaPerfilTrocaArquivo = null;

	/** Atributo listaGridTarifasModalidade. */
	private List<ConsultarTarifaContratoSaidaDTO> listaGridTarifasModalidade = null;

	/** Atributo flagMetodoImprimirAnexo. */
	private boolean flagMetodoImprimirAnexo = false;	

	/** Atributo listaServicoModalidadeTelaResumo. */
	List<PagamentosDTO> listaServicoModalidadeTelaResumo = null;

	/** Atributo listaPagamentosFornec. */
	private List<PagamentosDTO> listaPagamentosFornec = null;

	/** Atributo listaPagamentosBeneficios. */
	private List<PagamentosDTO> listaPagamentosBeneficios = null;

	/** Atributo listaPagamentosSalarios. */
	private List<PagamentosDTO> listaPagamentosSalarios = null;	

	/** Atributo listaBeneficiosModularidades. */
	private List<PagamentosDTO> listaBeneficiosModularidades = null;

	/** Atributo listaServicos. */
	private List<ServicosDTO> listaServicos = null;

	/** Atributo listaServicos. */
	private List<ServicosDTO> listaServicosCompl = null;

	/** Atributo listaPagamentosTri. */
	private List<PagamentosDTO> listaPagamentosTri = null;	
	/** Atributo listaTitulos. */
	private List<PagamentosDTO> listaTitulos = null;

	/** Atributo listaFornModularidades. */
	private List<PagamentosDTO> listaFornModularidades = null;

	/** Atributo listaTri. */
	private List<PagamentosDTO> listaTri = null;	

	/** Atributo listaServicosModalidades. */
	private List<ServicosModalidadesDTO> listaServicosModalidades = null;

	/** Atributo listaFornecedorOutrasModularidades. */
	private List<PagamentosDTO> listaFornecedorOutrasModularidades = null; 

	/** Atributo listaAnexoContrato. */
	private List<AnexoContratoDTO> listaAnexoContrato = null;

	/** Atributo listaSalariosMod. */
	private List<PagamentosDTO> listaSalariosMod = null;

	/** Atributo listaParticipantes. */
	private List<OcorrenciasParticipantes> listaParticipantes = null;

	/** Atributo listaContas. */
	private List<OcorrenciasContas> listaContas = null;

	/** Atributo listaLayouts. */
	private List<OcorrenciasLayout> listaLayouts = null;

	/** Atributo exibePgtoFornecedor. */
	private String exibePgtoFornecedor = null;

	/** Atributo exibePgtoTributos. */
	private String exibePgtoTributos = null;

	/** Atributo exibePgtoSalarios. */
	private String exibePgtoSalarios = null;

	/** Atributo exibePgtoBeneficios. */
	private String exibePgtoBeneficios = null;

	/** Atributo imprimirGridTarifa. */
	private String imprimirGridTarifa = null; 

	/** Atributo nomeTabelaAtual. */
	private String nomeTabelaAtual = null;
	
	/** Atributo omiteServicosResumoContrato. */
	private boolean omiteServicosResumoContrato = false;
	
    /** Atributo contratoServiceImpl. */
    private IContratoService contratoServiceImpl = null;
    
	/** Atributo listaPesquisaControle. */
	private List<SelectItem> listaPesquisaControle;
    
    private DetalharContaComplementarSaidaDTO detalharContaComplementar = new DetalharContaComplementarSaidaDTO();  
    
	// Pesquisar
	/** Atributo listaGridComplemento. */
	private List<ListarContaComplementarOcorrenciasSaida> listaGridComplementar;
	
	// Pesquisar
	/** Atributo listaGridComplementarParticipante. */
	private List<ListarContaComplParticipantesSaidaDTO> listaGridComplementarParticipante = new ArrayList<ListarContaComplParticipantesSaidaDTO>();
	
	// Pesquisar
	/** Atributo listaGridAgenciaConta. */
	private List<ListarContasVinculadasContratoSaidaDTO> listaGridAgenciaConta = new ArrayList<ListarContasVinculadasContratoSaidaDTO>();

	/** Atributo itemSelecionadoGrid. */
    private Integer itemSelecionadoListaParticipantes;
    
    private String cdCpfCGCIncComplementar;
    
    private String cdnomeRazaoSocialIncComplementar;
    
    private String cdBancoIncComplementar;
    
    private String cdAgenciaIncComplementar;
    
    private String cdContaIncComplementar;
    
    private String cdDataHoraInclusao;
    
    private String cdDataHoraInclusaoUsuario;
    
    private String cdDataHoraManutencao;
    
    private String cdDataHoraManutencaoUsuario;
    
    private String cdTrocaOpcao;    
    
    /** Atributo itemSelecionadoListaAgenciaConta. */
    private Integer itemSelecionadoListaAgenciaConta;
    
    /** Atributo itemSelecionadoListaAgenciaConta. */
    private Integer itemSelecionadoListaManterCtaPesquisar;
    
    /** Atributo cdContaPointerIncComplementar. */
    private Integer cdContaPointerIncComplementar;
    
	/** Atributo itemSelecionadoConsultaParticipante. */
	private String itemSelecionadoConsultaParticipante;
	
	/** Atributo itemSelecionadoConsultaAgenciaConta. */
	private String itemSelecionadoConsultaAgenciaConta;
    
	/** Atributo listaPesquisaControleParticipante. */
	private List<SelectItem> listaPesquisaControleParticipante;
	
	/** Atributo listaPesquisaControleAgenciaConta. */
	private List<SelectItem> listaPesquisaControleAgenciaConta;
	
	/** Atributo itemSelecionadoPesquisa. */
	private Integer itemSelecionadoPesquisa;	
	
	
	// Incluir Excluir Detalhar
	/** Atributo cnpj. */
	private String cnpj;

	/** Atributo cnpj2. */
	private String cnpj2;

	/** Atributo cnpj3. */
	private String cnpj3;

	/** Atributo cpf. */
	private String cpf;

	/** Atributo cpf2. */
	private String cpf2;

	/** Atributo banco. */
	private String banco;

	/** Atributo agencia. */
	private String agencia;

	/** Atributo agenciaDig. */
	private String agenciaDig;

	/** Atributo conta. */
	private String conta;

	/** Atributo contaDig. */
	private String contaDig;	
	
	public void limparIncluirComplementar() {
		setListaGridComplementar(null);
		setListaPesquisaControle(null);
		PgitFacesUtils.resetPdcPagination();
	}
    
	/**
	 * Limpar argumentos pesquisa Consulta Participante.
	 */
    public String selecionaParticipantes() {
    	
    	listaGridComplementarParticipante = new ArrayList<ListarContaComplParticipantesSaidaDTO>(); 
    	
    	setItemSelecionadoListaParticipantes(null);
    	
    	setCnpj("");
    	setCnpj2("");
    	setCnpj3("");
    	setCpf("");
    	setCpf2("");
    	
    	setItemSelecionadoConsultaParticipante(null);
    	
    	setCdCpfCGCIncComplementar("");
    	setCdnomeRazaoSocialIncComplementar("");

    	return "CONSULTAPARTICIPANTE";
    }
    
	/**
	 * Limpar dados incluir.
	 */
	public void limparDadosIncluir() {
		manterContratoParticipantesBean.setCnpj("");
		manterContratoParticipantesBean.setCnpj2("");
		manterContratoParticipantesBean.setCnpj3("");
		manterContratoParticipantesBean.setCpf("");
		manterContratoParticipantesBean.setCpf2("");
		manterContratoParticipantesBean.setBanco("");
		manterContratoParticipantesBean.setAgencia("");
		manterContratoParticipantesBean.setConta("");		
		
    	setCnpj("");
    	setCnpj2("");
    	setCnpj3("");
    	setCpf("");
    	setCpf2("");
    	
		this.setNomeRazaoSocial("");
		setBanco("");
		setAgencia("");
		setAgenciaDig("");
		setConta("");
		setContaDig("");

		manterContratoParticipantesBean.setItemSelecionado(null);
		manterContratoParticipantesBean.setListaPesquisa(null);
		this.setItemSelecionadoPesquisa(null);
	}
    
	
	public void limparPesquisaParticipantes() {
    	this.setCnpj("");
    	this.setCnpj2("");
    	this.setCnpj3("");
    	this.setCpf("");
    	this.setCpf2("");
	}
	
	public void limparPesquisaBcoAgeCtaParticipantes() {
		this.setCnpj("");
    	this.setCnpj2("");
    	this.setCnpj3("");
    	this.setCpf("");
    	this.setCpf2("");
    	this.setBanco("");
    	this.setAgencia("");
    	this.setConta("");    	
	}
	
	public String voltarLista() {
		return "VOLTAR";
	}
    
	/**
	 * Limpar argumentos pesquisa Consulta Participante.
	 */
    public String selecionaAgenciaConta() {
    	
    	listaGridAgenciaConta = new ArrayList<ListarContasVinculadasContratoSaidaDTO>(); 
    	
    	setItemSelecionadoListaAgenciaConta(null);
    	
    	setCnpj("");
    	setCnpj2("");
    	setCnpj3("");
    	setCpf("");
    	setCpf2("");
		setBanco("");
		setAgencia("");
		setAgenciaDig("");
		setConta("");
		setContaDig("");    	
    	setCdTrocaOpcao(null);
    	
    	setCdBancoIncComplementar("");
    	setCdAgenciaIncComplementar("");
    	setCdContaIncComplementar("");
    	
    	setItemSelecionadoConsultaAgenciaConta(null);
    	
    	return "CONSULTAAGENCIACONTA";
    }
    
    public String selecionaAgenciaContaAlterar() {
    	
    	listaGridAgenciaConta = new ArrayList<ListarContasVinculadasContratoSaidaDTO>(); 
    	
    	setItemSelecionadoListaAgenciaConta(null);
    	
    	setCnpj("");
    	setCnpj2("");
    	setCnpj3("");
    	setCpf("");
    	setCpf2("");
		setBanco("");
		setAgencia("");
		setAgenciaDig("");
		setConta("");
		setContaDig("");    	
    	
    	setCdTrocaOpcao(" ");
    	setItemSelecionadoConsultaAgenciaConta(null);
    	
    	return "CONSULTAAGENCIACONTA";
    }
        
    public String voltarIncCtaComplementar() {
    	return "VOLTAR";
    }
    
    public String voltarTelaAnterior() {
    	if (getCdTrocaOpcao() == null) {
    		return "VOLTAR";
    	}else{
    		return "VOLTARALTERAR";
    	}
    }
    
    public String confirmarAgenciaConta() {
    	
    	setCdBancoIncComplementar(getListaGridAgenciaConta().get(getItemSelecionadoListaAgenciaConta()).getCdBanco().toString());
    	setCdAgenciaIncComplementar(getListaGridAgenciaConta().get(getItemSelecionadoListaAgenciaConta()).getCdAgencia().toString());
    	setCdContaIncComplementar(getListaGridAgenciaConta().get(getItemSelecionadoListaAgenciaConta()).getCdContaCdDigito());
    	
    	if (getCdTrocaOpcao() == null) {
    		return "VOLTAR";
    	}else{
    		return "VOLTARALTERAR";
    	}
    }
    
	public void limparDadosConsultaParticipantes() {
    	this.setCnpj("");
    	this.setCnpj2("");
    	this.setCnpj3("");
    	this.setCpf("");
    	this.setCpf2("");		
		setItemSelecionadoConsultaParticipante(null);
	}
	
	public void limparDadosConsultaAgenciaConta() {
		this.setCnpj("");
    	this.setCnpj2("");
    	this.setCnpj3("");
    	this.setCpf("");
    	this.setCpf2("");
    	this.setBanco("");
    	this.setAgencia("");
    	this.setConta("");  		
		setItemSelecionadoConsultaAgenciaConta(null);
	}    
    
    public String confirmarParticipante() {
    	
    	setCdCpfCGCIncComplementar(getListaGridComplementarParticipante().get(getItemSelecionadoListaParticipantes()).getCnpjOuCpfFormatado());
    	setCdnomeRazaoSocialIncComplementar(getListaGridComplementarParticipante().get(getItemSelecionadoListaParticipantes()).getDsNomeRazaoSocial());
    	
    	return "VOLTAR";
    }    
    
	public void carregarPaginacaoListaParticipantes(ActionEvent event) {
		carregaListaParticipantes();
	}    
    
	public String carregaListaParticipantes() {
		
		try {
			ListarContaComplParticipantesEntradaDTO entradaDTO = new ListarContaComplParticipantesEntradaDTO();
			
			entradaDTO.setMaxOcorrencias(20);
			entradaDTO.setCdpessoaJuridicaContrato(manterContratoParticipantesBean.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(manterContratoParticipantesBean.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(manterContratoParticipantesBean.getNrSequenciaContrato()));			
			
			if ("0".equals(getItemSelecionadoConsultaParticipante())) {
				entradaDTO.setCdCorpoCpfCnpj(!(getCnpj().equals("")) ? Long.parseLong(getCnpj()) : 0);
				entradaDTO.setCdFilialCpfCnpj(!(getCnpj2().equals("")) ? Integer.parseInt(getCnpj2()) : 0);
				entradaDTO.setCdControleCpfCnpj(!(getCnpj3().equals("")) ? Integer.parseInt(getCnpj3()) : 0);
			} else if ("1".equals(getItemSelecionadoConsultaParticipante())) {
				entradaDTO.setCdCorpoCpfCnpj(!(getCpf().equals("")) ? Long.parseLong(getCpf()) : 0);
				entradaDTO.setCdFilialCpfCnpj(0);
				entradaDTO.setCdControleCpfCnpj(!(getCpf2().equals("")) ? Integer.parseInt(getCpf2()) : 0);
			}else{
				entradaDTO.setCdCorpoCpfCnpj(0L);
				entradaDTO.setCdFilialCpfCnpj(0);
				entradaDTO.setCdControleCpfCnpj(0);
			}
			
			setListaGridComplementarParticipante(manterContratoParticipantesBean.getFiltroContaComplementarImpl().listaParticipantes(entradaDTO));
			
			this.listaPesquisaControleParticipante  = new ArrayList<SelectItem>();
			
			for (int i = 0; i < getListaGridComplementarParticipante().size(); i++) {
				listaPesquisaControleParticipante.add(new SelectItem(i, " "));
			}
			
			setItemSelecionadoListaParticipantes(null);
			
			return "CONSULTAPARTICIPANTE";

		}catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			
			listaPesquisaControleParticipante = null;
			listaGridComplementarParticipante = null;
			PgitFacesUtils.resetPdcPagination();
			return "";
		 }
	}
	
	public void carregarPaginacaoListaAgenciaConta(ActionEvent event) {
		carregaListaAgenciaConta();
	}	
	
	public String carregaListaAgenciaConta() {
		
		try {
			
			ListarContasVinculadasContratoEntradaDTO entradaDTO = new ListarContasVinculadasContratoEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaNegocio(manterContratoParticipantesBean.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(manterContratoParticipantesBean.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(manterContratoParticipantesBean.getNrSequenciaContrato()));				
			entradaDTO.setCdTipoContratoNegocio(manterContratoParticipantesBean.getCdTipoContrato());
	        entradaDTO.setCdFinalidade(0);
			
			if ("0".equals(getItemSelecionadoConsultaAgenciaConta())) {
				entradaDTO.setCdCorpoCfpCnpj(!(getCnpj().equals("")) ? Long.parseLong(getCnpj()) : 0);
				entradaDTO.setCdControleCpfCnpj(!(getCnpj2().equals("")) ? Integer.parseInt(getCnpj2()) : 0);
				entradaDTO.setCdDigitoCpfCnpj(!(getCnpj3().equals("")) ? Integer.parseInt(getCnpj3()) : 0);
			} else if ("1".equals(getItemSelecionadoConsultaAgenciaConta())) {
				entradaDTO.setCdCorpoCfpCnpj(!(getCpf().equals("")) ? Long.parseLong(getCpf()) : 0);
				entradaDTO.setCdControleCpfCnpj(0);
				entradaDTO.setCdDigitoCpfCnpj(!(getCpf2().equals("")) ? Integer.parseInt(getCpf2()) : 0);
			}else{
				entradaDTO.setCdCorpoCfpCnpj(0L);
				entradaDTO.setCdControleCpfCnpj(0);
				entradaDTO.setCdDigitoCpfCnpj(0);
			}
			
			entradaDTO.setCdBanco(!(getBanco().equals("")) ? Integer.parseInt(getBanco()) : 0);
			entradaDTO.setCdAgencia(!(getAgencia().equals("")) ? Integer.parseInt(getAgencia()) : 0);
			entradaDTO.setCdConta(!(getConta().equals("")) ? Long.parseLong(getConta()) : 0);
			
			setListaGridAgenciaConta(getComboService().listarContasVinculadasContrato(entradaDTO));
			
			this.listaPesquisaControleAgenciaConta  = new ArrayList<SelectItem>();
			
			for (int i = 0; i < getListaGridAgenciaConta().size(); i++) {
				listaPesquisaControleAgenciaConta.add(new SelectItem(i, " "));
			}
			
		}catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			
			listaPesquisaControleAgenciaConta = null;
			listaGridAgenciaConta = null;
			PgitFacesUtils.resetPdcPagination();
		 }
		return "";
	}    
	
	/**
	 * Nome: iniciarPagina.
	 *
	 * @param evt the evt
	 * @see
	 */
	public void iniciarPagina(ActionEvent evt) {
		this.identificacaoClienteContratoBean
		.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
		.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean
		.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean
		.setPaginaCliente("identificacaoClienteManterContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conManterContaComplementar");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean
		.setHabilitaEmpresaGestoraTipoContrato(true);

		setDsGerenteResponsavel("");
		setCpfCnpjParticipante("");
		setDsNomeRazaoSocialParticipante("");
		setCpfCnpj("");
		setNomeRazaoSocial("");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		setImprimirGridTarifa("N");
		setOmiteServicosResumoContrato(false);
		
		this.setCnpj("");
    	this.setCnpj2("");
    	this.setCnpj3("");
    	this.setCpf("");
    	this.setCpf2("");
    	this.setBanco("");
    	this.setAgencia("");
    	this.setConta("");  		
		setItemSelecionadoConsultaAgenciaConta(null);		
	}
	
	public void carregarPaginacaoContaComplementar(ActionEvent event) {
	    carregaListaContaComplementar();
	}
	
	/**
	 *  Carrega Lista Conta Complementar
	 */
	public String carregaListaContaComplementar() {
		
		try {
			ListarContaComplementarEntrada entradaDTO = new ListarContaComplementarEntrada();
			
			entradaDTO.setMaxOcorrencias(50);
			entradaDTO.setCdpessoaJuridicaContrato(manterContratoParticipantesBean.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(manterContratoParticipantesBean.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(manterContratoParticipantesBean.getNrSequenciaContrato()));
			
			entradaDTO.setCdAgencia(!(manterContratoParticipantesBean.getAgencia().equals("")) ? Integer.parseInt(manterContratoParticipantesBean.getAgencia()) : 0);
			entradaDTO.setCdBanco(!(manterContratoParticipantesBean.getBanco().equals("")) ? Integer.parseInt(manterContratoParticipantesBean.getBanco()) : 0);
			entradaDTO.setCdConta(!(manterContratoParticipantesBean.getConta().equals("")) ? Long.parseLong(manterContratoParticipantesBean.getConta()) : 0);
			entradaDTO.setCdDigitoConta(!(manterContratoParticipantesBean.getContaDig().equals("")) ? manterContratoParticipantesBean.getContaDig() : " ");
			
			if ("0".equals(manterContratoParticipantesBean.getItemSelecionado())) {
				entradaDTO.setCdCorpoCnpjCpf(!(manterContratoParticipantesBean.getCnpj().equals("")) ? Long.parseLong(manterContratoParticipantesBean.getCnpj()) : 0);
				entradaDTO.setCdFilialCnpjCpf(!(manterContratoParticipantesBean.getCnpj2().equals("")) ? Integer.parseInt(manterContratoParticipantesBean.getCnpj2()) : 0);
				entradaDTO.setCdControleCnpjCpf(!(manterContratoParticipantesBean.getCnpj3().equals("")) ? Integer.parseInt(manterContratoParticipantesBean.getCnpj3()) : 0);
			} else if ("1".equals(manterContratoParticipantesBean.getItemSelecionado())) {
				entradaDTO.setCdCorpoCnpjCpf(!(manterContratoParticipantesBean.getCpf().equals("")) ? Long.parseLong(manterContratoParticipantesBean.getCpf()) : 0);
				entradaDTO.setCdFilialCnpjCpf(0);
				entradaDTO.setCdControleCnpjCpf(!(manterContratoParticipantesBean.getCpf2().equals("")) ? Integer.parseInt(manterContratoParticipantesBean.getCpf2()) : 0);
			}else{
				entradaDTO.setCdCorpoCnpjCpf(0L);
				entradaDTO.setCdFilialCnpjCpf(0);
				entradaDTO.setCdControleCnpjCpf(0);
			}
			setListaGridComplementar(manterContratoParticipantesBean.
					getFiltroContaComplementarImpl().listarContasComplementar(entradaDTO).getOcorrencias());
			
			this.listaPesquisaControle = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridComplementar().size(); i++) {
				listaPesquisaControle.add(new SelectItem(i, " "));
			}			
			setItemSelecionadoListaManterCtaPesquisar(null);
			
			return "ATUALIZARTELA";
			
		 } catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
			listaGridComplementar = null;
			listaPesquisaControle = null;
			PgitFacesUtils.resetPdcPagination();
			return "";
		 }
	}

	/** Atributo cdTipoArquivoRetorno. */
	private Integer cdTipoArquivoRetorno;

	/** Atributo listaTipoArquivoRetorno. */
	private List<SelectItem> listaTipoArquivoRetorno = new ArrayList<SelectItem>();

	/** Atributo listaTipoArquivoRetornoHash. */
	private Map<Integer, String> listaTipoArquivoRetornoHash = new HashMap<Integer, String>();

	/**
	 * Nome: getComboTipoMontagem.
	 *
	 * @return comboTipoMontagem
	 * @see
	 */
	public List<SelectItem> getComboTipoMontagem() {

		ConsultarListaTipoRetornoLayoutSaidaDTO ob = null;

		if (manterContratoLayoutsArquivosBean.getItemSelecionadoListaRetorno() != null) {
			ob = manterContratoLayoutsArquivosBean.getListaRetorno().get(
					manterContratoLayoutsArquivosBean
					.getItemSelecionadoListaRetorno());
		}

		List<SelectItem> combo = null;

		combo = manterContratoLayoutsArquivosBean.getTipoMontagem();

		if (!"PTRB".equals(manterContratoLayoutsArquivosBean.getDsTipoLayout())) {

			if (ob != null) {
				if ((ob.getCdTipoArquivoRetorno() >= 1 && ob
						.getCdTipoArquivoRetorno() <= 5)
						|| ob.getCdTipoArquivoRetorno() == 23) {
					combo.add(new SelectItem(TIPO_SERVICO.getCdTipoMontagem(),
							TIPO_SERVICO.getDsTipoMontagem()));
				}

			}
		}

		return combo;
	}

	/**
	 * Nome: contasComplementarPesquisa.
	 *
	 * @return void
	 * @see
	 */	
	public String conManterContasComplPesquisa() {
		
		setListaGridComplementar(null);
		setItemSelecionadoListaManterCtaPesquisar(null);
		
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDataHoraCadastramento(null);
		setDescricaoContrato(saidaDTO.getDsContrato());

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getDescFuncionarioBradesco());
		setGerenteResponsavelFormatado(saidaDTO.getCdFuncionarioBradesco()
				+ " - " + saidaDTO.getDescFuncionarioBradesco());

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat(
			"yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1
						.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}
		}

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}
		manterContratoParticipantesBean.inciarTela();

		manterContratoParticipantesBean.setCpfCnpjRepresentante(saidaDTO
				.getCnpjOuCpfFormatado());
		manterContratoParticipantesBean.setNomeRazaoRepresentante(saidaDTO
				.getNmRazaoSocialRepresentante());
		manterContratoParticipantesBean
		.setGrupEconRepresentante(getGrupoEconomicoMaster());
		manterContratoParticipantesBean
		.setAtivEconRepresentante(getAtividadeEconomicaMaster());
		manterContratoParticipantesBean
		.setSegRepresentante(getSegmentoMaster());
		manterContratoParticipantesBean
		.setSubSegRepresentante(getSubSegmentoMaster());
		manterContratoParticipantesBean.setEmpresaContrato(getEmpresa());
		manterContratoParticipantesBean.setTipoContrato(getTipo());
		manterContratoParticipantesBean.setNumeroContrato(getNumero());
		manterContratoParticipantesBean.setSituacaoContrato(getSituacaoDesc());
		manterContratoParticipantesBean.setMotivoContrato(getMotivoDesc());
		manterContratoParticipantesBean
		.setParticipacaoContrato(getParticipacao());
		manterContratoParticipantesBean
		.setAditivosContrato(getPossuiAditivos());
		manterContratoParticipantesBean
		.setDataHoraCadastramento(getDataHoraCadastramento());
		manterContratoParticipantesBean
		.setDescricaoContrato(getDescricaoContrato());

		if (identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null
				|| "".equals(identificacaoClienteContratoBean
						.getSaidaConsultarListaClientePessoas()
						.getCnpjOuCpfFormatado())) {
			manterContratoParticipantesBean.setNomeRazaoParticipante("");
			manterContratoParticipantesBean.setCpfCnpjParticipante("");
		} else {
			manterContratoParticipantesBean
			.setNomeRazaoParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getDsNomeRazao());
			manterContratoParticipantesBean
			.setCpfCnpjParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
		}

		manterContratoParticipantesBean.setCdEmpresaContrato(getCdEmpresa());
		manterContratoParticipantesBean.setCdTipoContrato(getCdTipo());
		manterContratoParticipantesBean.setNrSequenciaContrato(getNumero());

		manterContratoParticipantesBean.setCdPessoaJuridica(saidaDTO.getCdPessoaJuridica());
		
		manterContratoParticipantesBean.carregaListaConsultar();
		return "CONTASCOMPLPESQUISA";
	}

	/**
	 * Nome: voltarAgenciaGestora.
	 *
	 * @return the string
	 * @see
	 */
	public String voltarAgenciaGestora() {
		return "VOLTAR";
	}

	/**
	 * Nome: confirmarAlterarRepresentante.
	 *
	 * @return the string
	 * @see
	 */
	public String confirmarAlterarRepresentante() {
		try {
			manterContratoRepresentanteBean.confirmarAlterar();
			identificacaoClienteContratoBean.consultarContrato();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);
			return null;
		}

		return "";
	}
	
	/**
	 * Nome: renegociar.
	 *
	 * @see
	 */
	public void renegociar() {

		contratoPgitSelecionado = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());
	}	

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		
		setCdCpfCGCIncComplementar("");
		setCdnomeRazaoSocialIncComplementar("");
		setCdBancoIncComplementar("");
		setCdAgenciaIncComplementar("");
		setCdContaIncComplementar("");
		setCdContaPointerIncComplementar(null);
		
		return "INCLUIR";
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String alterar() {

		try {
			DetalharContaComplementarEntradaDTO entradaDTO = new DetalharContaComplementarEntradaDTO();
			
			entradaDTO.setCdpessoaJuridicaContrato(manterContratoParticipantesBean.getCdPessoaJuridica());
			
			entradaDTO.setCdTipoContratoNegocio(manterContratoParticipantesBean.getCdTipoContrato());
			
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(manterContratoParticipantesBean.getNrSequenciaContrato()));
			
			entradaDTO.setCdContaComplementar(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdContaComplementar());
			
			entradaDTO.setCdCorpoCnpjCpf(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdCorpoCnpjCpf());
			
			entradaDTO.setCdFilialCnpjCpf(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdFilialCpfCnpj());
			
			entradaDTO.setCdControleCnpjCpf(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdCtrlCpfCnpj());
			
			setDetalharContaComplementar(manterContratoParticipantesBean.getFiltroContaComplementarImpl().detalharContaComplementar(entradaDTO));  
			
			setCdContaPointerIncComplementar(detalharContaComplementar.getCdContaComplementar());
			setCdCpfCGCIncComplementar(
					CpfCnpjUtils.formatCpfCnpjCompleto(detalharContaComplementar.getCdCorpoCnpjCpf() != null  ? detalharContaComplementar.getCdCorpoCnpjCpf() : 0L, 
							detalharContaComplementar.getCdFilialCpfCnpj() != null ? detalharContaComplementar.getCdFilialCpfCnpj() : 0, 
							detalharContaComplementar.getCdCtrlCpfCnpj() != null ? detalharContaComplementar.getCdCtrlCpfCnpj() : 0)
					);
			
			setCdnomeRazaoSocialIncComplementar(detalharContaComplementar.getDsNomeParticipante());
			setCdBancoIncComplementar(detalharContaComplementar.getCdBanco().toString());
			setCdAgenciaIncComplementar(detalharContaComplementar.getCdAgencia().toString());
			
			String varConta = Long.toString(detalharContaComplementar.getCdConta());
			String varDigito = detalharContaComplementar.getCdDigitoConta() != null ?  detalharContaComplementar.getCdDigitoConta() : "";
			
			setCdContaIncComplementar(varConta+"-"+varDigito);
			
			setCdDataHoraInclusao(detalharContaComplementar.getHrInclusaoRegistro());
			setCdDataHoraInclusaoUsuario(detalharContaComplementar.getCdUsuarioInclusao());
			
			setCdDataHoraManutencao(detalharContaComplementar.getHrManutencaoRegistro());
			setCdDataHoraManutencaoUsuario(detalharContaComplementar.getCdUsuarioManutencao());
			
			return "ALTERAR";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}

		return "";		
	}
	
    public String alterarAvancarComplemento() {
    	
    	if (getCdBancoIncComplementar()== null || getCdBancoIncComplementar() == "") {
			BradescoFacesUtils.addInfoModalMessage("Favor selecionar uma conta",false);
			return "";
    	}
    	return "AVANCARALTERAR";
    }
    
    
    public String incluirAvancarComplemento() {
    	
    	if (getCdContaPointerIncComplementar() == null) {
			BradescoFacesUtils.addInfoModalMessage("Favor informar uma conta pointer",false);
			return "";
    	}
    	
    	if (getCdCpfCGCIncComplementar()== null || getCdCpfCGCIncComplementar() == "") {
			BradescoFacesUtils.addInfoModalMessage("Favor selecionar um participante",false);
			return "";
    	}
    	
    	if (getCdBancoIncComplementar()== null || getCdBancoIncComplementar() == "") {
			BradescoFacesUtils.addInfoModalMessage("Favor selecionar uma conta",false);
			return "";
    	}
    	return "AVANCARINCLUIR";
    }
    
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		
		try {
			DetalharContaComplementarEntradaDTO entradaDTO = new DetalharContaComplementarEntradaDTO();
			
			entradaDTO.setCdpessoaJuridicaContrato(manterContratoParticipantesBean.getCdPessoaJuridica());
			
			entradaDTO.setCdTipoContratoNegocio(manterContratoParticipantesBean.getCdTipoContrato());
			
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(manterContratoParticipantesBean.getNrSequenciaContrato()));
			
			entradaDTO.setCdContaComplementar(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdContaComplementar());
			
			entradaDTO.setCdCorpoCnpjCpf(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdCorpoCnpjCpf());
			
			entradaDTO.setCdFilialCnpjCpf(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdFilialCpfCnpj());
			
			entradaDTO.setCdControleCnpjCpf(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdCtrlCpfCnpj());
			
			setDetalharContaComplementar(manterContratoParticipantesBean.getFiltroContaComplementarImpl().detalharContaComplementar(entradaDTO));  
			
			setCdContaPointerIncComplementar(detalharContaComplementar.getCdContaComplementar());
			setCdCpfCGCIncComplementar(
					CpfCnpjUtils.formatCpfCnpjCompleto(detalharContaComplementar.getCdCorpoCnpjCpf() != null  ? detalharContaComplementar.getCdCorpoCnpjCpf() : 0L, 
							detalharContaComplementar.getCdFilialCpfCnpj() != null ? detalharContaComplementar.getCdFilialCpfCnpj() : 0, 
							detalharContaComplementar.getCdCtrlCpfCnpj() != null ? detalharContaComplementar.getCdCtrlCpfCnpj() : 0)
					);
			setCdnomeRazaoSocialIncComplementar(detalharContaComplementar.getDsNomeParticipante());
			setCdBancoIncComplementar(detalharContaComplementar.getCdBanco().toString());
			setCdAgenciaIncComplementar(detalharContaComplementar.getCdAgencia().toString());
			
			String varConta = Long.toString(detalharContaComplementar.getCdConta());
			String varDigito = detalharContaComplementar.getCdDigitoConta() != null ?  detalharContaComplementar.getCdDigitoConta() : "";
			
			setCdContaIncComplementar(varConta+"-"+varDigito);
			
			setCdDataHoraInclusao(detalharContaComplementar.getHrInclusaoRegistro());
			setCdDataHoraInclusaoUsuario(detalharContaComplementar.getCdUsuarioInclusao());
			
			setCdDataHoraManutencao(detalharContaComplementar.getHrManutencaoRegistro());
			setCdDataHoraManutencaoUsuario(detalharContaComplementar.getCdUsuarioManutencao());
			
			return "EXCLUIR";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
		return "";			
	}
	
	public void excluirCtaComplemento() {
		
		try {
			
			IncluirContaComplementarEntradaDTO entradaDTO = new IncluirContaComplementarEntradaDTO();
			
			entradaDTO.setCdpessoaJuridicaContrato(manterContratoParticipantesBean.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(manterContratoParticipantesBean.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(manterContratoParticipantesBean.getNrSequenciaContrato()));	
			
			entradaDTO.setCdContaComplementar(getCdContaPointerIncComplementar());
			
			entradaDTO.setCdCorpoCnpjCpf(detalharContaComplementar.getCdCorpoCnpjCpf());
			entradaDTO.setCdFilialCnpjCpf(detalharContaComplementar.getCdFilialCpfCnpj());
			entradaDTO.setCdControleCnpjCpf(detalharContaComplementar.getCdCtrlCpfCnpj());
			
			entradaDTO.setCdpessoaJuridicaContratoConta(getItemSelecionadoListaAgenciaConta() != null ? 
							getListaGridAgenciaConta().
							get(getItemSelecionadoListaAgenciaConta()).getCdPessoaJuridicaVinculo() :	
					detalharContaComplementar.getCdPessoaJuridicaContratoConta());
			
			entradaDTO.setCdTipoContratoConta(getItemSelecionadoListaAgenciaConta() != null ?
							getListaGridAgenciaConta().
							get(getItemSelecionadoListaAgenciaConta()).getCdTipoContratoVinculo() :
					detalharContaComplementar.getCdTipoContratoConta());
			
			entradaDTO.setNrSequenciaContratoConta(getItemSelecionadoListaAgenciaConta() != null ?
							getListaGridAgenciaConta().
							get(getItemSelecionadoListaAgenciaConta()).getNrSequenciaContratoVinculo() :
					detalharContaComplementar.getNrSequenciaContratoConta());
			
			AlterarContaComplementarSaidaDTO retorno = manterContratoParticipantesBean.getFiltroContaComplementarImpl().excluirContaComplementar(entradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(retorno.getCodMensagem(), 8) + ") " + retorno.getMensagem(),
					"#{contaComplementarBean.carregaListaContaComplementar}", BradescoViewExceptionActionType.ACTION, false);			
			
		}catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
		 }
	}		
	
	
	public void gravarIncCtaComplemento() {
		
		try {
			
			IncluirContaComplementarEntradaDTO entradaDTO = new IncluirContaComplementarEntradaDTO();
			
			entradaDTO.setCdpessoaJuridicaContrato(manterContratoParticipantesBean.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(manterContratoParticipantesBean.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(manterContratoParticipantesBean.getNrSequenciaContrato()));	
			
			entradaDTO.setCdContaComplementar(getCdContaPointerIncComplementar());
			
			entradaDTO.setCdCorpoCnpjCpf(getListaGridComplementarParticipante().get(getItemSelecionadoListaParticipantes()).getCdCorpoCpfCnpj());
			entradaDTO.setCdFilialCnpjCpf(getListaGridComplementarParticipante().get(getItemSelecionadoListaParticipantes()).getCdFilialCpfCnpj());
			entradaDTO.setCdControleCnpjCpf(getListaGridComplementarParticipante().get(getItemSelecionadoListaParticipantes()).getCdControleCpfCnpj());
			
			entradaDTO.setCdpessoaJuridicaContratoConta(getListaGridAgenciaConta().
					get(getItemSelecionadoListaAgenciaConta()).getCdPessoaJuridicaVinculo());
			
			entradaDTO.setCdTipoContratoConta(getListaGridAgenciaConta().
					get(getItemSelecionadoListaAgenciaConta()).getCdTipoContratoVinculo());
			
			entradaDTO.setNrSequenciaContratoConta(getListaGridAgenciaConta().
					get(getItemSelecionadoListaAgenciaConta()).getNrSequenciaContratoVinculo());
			
			IncluirContaComplementarSaidaDTO retorno = manterContratoParticipantesBean.getFiltroContaComplementarImpl().incluirContaComplementar(entradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(retorno.getCodMensagem(), 8) + ") " + retorno.getMensagem(),
					"#{contaComplementarBean.carregaListaContaComplementar}", BradescoViewExceptionActionType.ACTION, false);			
			
		}catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
		 }
	}
	
	public void gravarAltCtaComplemento() {
		
		try {
			
			IncluirContaComplementarEntradaDTO entradaDTO = new IncluirContaComplementarEntradaDTO();
			
			entradaDTO.setCdpessoaJuridicaContrato(manterContratoParticipantesBean.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(manterContratoParticipantesBean.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(manterContratoParticipantesBean.getNrSequenciaContrato()));	
			
			entradaDTO.setCdContaComplementar(getCdContaPointerIncComplementar());
			
			entradaDTO.setCdCorpoCnpjCpf(detalharContaComplementar.getCdCorpoCnpjCpf());
			entradaDTO.setCdFilialCnpjCpf(detalharContaComplementar.getCdFilialCpfCnpj());
			entradaDTO.setCdControleCnpjCpf(detalharContaComplementar.getCdCtrlCpfCnpj());
			
			entradaDTO.setCdpessoaJuridicaContratoConta(getItemSelecionadoListaAgenciaConta() != null ? 
							getListaGridAgenciaConta().
							get(getItemSelecionadoListaAgenciaConta()).getCdPessoaJuridicaVinculo() :	
					detalharContaComplementar.getCdPessoaJuridicaContratoConta());
			
			entradaDTO.setCdTipoContratoConta(getItemSelecionadoListaAgenciaConta() != null ?
							getListaGridAgenciaConta().
							get(getItemSelecionadoListaAgenciaConta()).getCdTipoContratoVinculo() :
					detalharContaComplementar.getCdTipoContratoConta());
			
			entradaDTO.setNrSequenciaContratoConta(getItemSelecionadoListaAgenciaConta() != null ?
							getListaGridAgenciaConta().
							get(getItemSelecionadoListaAgenciaConta()).getNrSequenciaContratoVinculo() :
					detalharContaComplementar.getNrSequenciaContratoConta());
			
			AlterarContaComplementarSaidaDTO retorno = manterContratoParticipantesBean.getFiltroContaComplementarImpl().alterarContaComplementar(entradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(retorno.getCodMensagem(), 8) + ") " + retorno.getMensagem(),
					"#{contaComplementarBean.carregaListaContaComplementar}", BradescoViewExceptionActionType.ACTION, false);			
			
		}catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
					false);
		 }
	}	
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String detalhar() {
		
		try {
			DetalharContaComplementarEntradaDTO entradaDTO = new DetalharContaComplementarEntradaDTO();
			
			entradaDTO.setCdpessoaJuridicaContrato(manterContratoParticipantesBean.getCdPessoaJuridica());
			
			entradaDTO.setCdTipoContratoNegocio(manterContratoParticipantesBean.getCdTipoContrato());
			
			entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(manterContratoParticipantesBean.getNrSequenciaContrato()));
			
			entradaDTO.setCdContaComplementar(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdContaComplementar());
			
			entradaDTO.setCdCorpoCnpjCpf(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdCorpoCnpjCpf());
			
			entradaDTO.setCdFilialCnpjCpf(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdFilialCpfCnpj());
			
			entradaDTO.setCdControleCnpjCpf(getListaGridComplementar().
					get(getItemSelecionadoListaManterCtaPesquisar()).getCdCtrlCpfCnpj());
			
			setDetalharContaComplementar(manterContratoParticipantesBean.getFiltroContaComplementarImpl().detalharContaComplementar(entradaDTO));  
			
			setCdContaPointerIncComplementar(detalharContaComplementar.getCdContaComplementar());
			setCdCpfCGCIncComplementar(
					CpfCnpjUtils.formatCpfCnpjCompleto(detalharContaComplementar.getCdCorpoCnpjCpf() != null  ? detalharContaComplementar.getCdCorpoCnpjCpf() : 0L, 
							detalharContaComplementar.getCdFilialCpfCnpj() != null ? detalharContaComplementar.getCdFilialCpfCnpj() : 0, 
							detalharContaComplementar.getCdCtrlCpfCnpj() != null ? detalharContaComplementar.getCdCtrlCpfCnpj() : 0)
					);
			setCdnomeRazaoSocialIncComplementar(detalharContaComplementar.getDsNomeParticipante());
			setCdBancoIncComplementar(detalharContaComplementar.getCdBanco().toString());
			setCdAgenciaIncComplementar(detalharContaComplementar.getCdAgencia().toString());
			
			String varConta = Long.toString(detalharContaComplementar.getCdConta());
			String varDigito = detalharContaComplementar.getCdDigitoConta() != null ?  detalharContaComplementar.getCdDigitoConta() : "";
			
			setCdContaIncComplementar(varConta+"-"+varDigito);
			
			setCdDataHoraInclusao(detalharContaComplementar.getHrInclusaoRegistro());
			setCdDataHoraInclusaoUsuario(detalharContaComplementar.getCdUsuarioInclusao());
			
			setCdDataHoraManutencao(detalharContaComplementar.getHrManutencaoRegistro());
			setCdDataHoraManutencaoUsuario(detalharContaComplementar.getCdUsuarioManutencao());
			
			return "DETALHAR";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}

		return "";			
	}	

	/**
	 * Paginar tabela tarifas.
	 *
	 * @param event the event
	 */
	public void paginarTabelaTarifas(ActionEvent event){
		listarGridTafifasModalidade();
	}
	
	/**
	 * Listar grid tafifas modalidade.
	 */
	public void listarGridTafifasModalidade(){
		listaGridTarifasModalidade = new ArrayList<ConsultarTarifaContratoSaidaDTO>();	


		try {
			ConsultarTarifaContratoEntradaDTO entradaTarifa = new ConsultarTarifaContratoEntradaDTO();
			entradaTarifa.setCdPessoaJuridica(identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getCdPessoaJuridica());
			entradaTarifa.setCdTipoContrato(identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getCdTipoContrato());
			entradaTarifa.setNrSequenciaContrato(identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista()).getNrSequenciaContrato());	
			setListaGridTarifasModalidade(getManterContratoImpl().consultarTarifaContratoLoop(entradaTarifa));		

		} catch (PdcAdapterFunctionalException e) {
		    BradescoCommonServiceFactory.getLogManager().error(e.getMessage(), e);
		}
	}	


	/**
	 * Nome: validarUsuario.
	 *
	 * @see
	 */
	public void validarUsuario() {
		setEntradaValidarUsuario(new ValidarUsuarioEntradaDTO());

		try {
			entradaValidarUsuario.setMaxOcorrencias(PgitUtil.verificaIntegerNulo(0));
			saidaValidarUsuario = manterContratoImpl.validarUsuario(entradaValidarUsuario);

			Integer cdAcessoDepartamento = saidaValidarUsuario.getCdAcessoDepartamento();
			
			if(cdAcessoDepartamento != null){
			    if(cdAcessoDepartamento == 1){
	                setRestringirAcessoBotoes(false);
	            }else if(cdAcessoDepartamento == 2){
	                setRestringirAcessoBotoes(true);
	            }
			}

			getManterContratoLayoutsArquivosBean().setCdAcessoDepartamento(cdAcessoDepartamento);
			
		} catch (PdcAdapterFunctionalException e) {
			BradescoFacesUtils.addInfoModalMessage("("
					+ StringUtils.right(e.getCode(), 8) + ") "
					+ e.getMessage(), false);
		}

	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		return "VOLTAR";
	}
	
	/**
     * Voltar pesquisa.
     * 
     * @return the string
     */
	public String voltarPesquisa() {
	    identificacaoClienteContratoBean.consultarContrato();
	    return "VOLTAR";
    }

	/**
	 * Nome: getDescricaoContrato.
	 *
	 * @return descricaoContrato
	 * @see
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Nome: setDescricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 * @see
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Nome: getCpfCnpj.
	 *
	 * @return cpfCnpj
	 * @see
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Nome: setCpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 * @see
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Nome: getNomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 * @see
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Nome: setNomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 * @see
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Nome: getDataHoraCadastramento.
	 *
	 * @return dataHoraCadastramento
	 * @see
	 */
	public String getDataHoraCadastramento() {
		return dataHoraCadastramento;
	}

	/**
	 * Nome: setDataHoraCadastramento.
	 *
	 * @param dataHoraCadastramento the data hora cadastramento
	 * @see
	 */
	public void setDataHoraCadastramento(String dataHoraCadastramento) {
		this.dataHoraCadastramento = dataHoraCadastramento;
	}

	/**
	 * Nome: getInicioVigencia.
	 *
	 * @return inicioVigencia
	 * @see
	 */
	public String getInicioVigencia() {
		return inicioVigencia;
	}

	/**
	 * Nome: setInicioVigencia.
	 *
	 * @param inicioVigencia the inicio vigencia
	 * @see
	 */
	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	/**
	 * Nome: getParticipacao.
	 *
	 * @return participacao
	 * @see
	 */
	public String getParticipacao() {
		return participacao;
	}

	/**
	 * Nome: setParticipacao.
	 *
	 * @param participacao the participacao
	 * @see
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	/**
	 * Nome: getPossuiAditivos.
	 *
	 * @return possuiAditivos
	 * @see
	 */
	public String getPossuiAditivos() {
		return possuiAditivos;
	}

	/**
	 * Nome: setPossuiAditivos.
	 *
	 * @param possuiAditivos the possui aditivos
	 * @see
	 */
	public void setPossuiAditivos(String possuiAditivos) {
		this.possuiAditivos = possuiAditivos;
	}

	/**
	 * Nome: getCdEmpresa.
	 *
	 * @return cdEmpresa
	 * @see
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Nome: setCdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 * @see
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Nome: getCdTipo.
	 *
	 * @return cdTipo
	 * @see
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}

	/**
	 * Nome: setCdTipo.
	 *
	 * @param cdTipo the cd tipo
	 * @see
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Nome: getEmpresa.
	 *
	 * @return empresa
	 * @see
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Nome: setEmpresa.
	 *
	 * @param empresa the empresa
	 * @see
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Nome: getMotivoDesc.
	 *
	 * @return motivoDesc
	 * @see
	 */
	public String getMotivoDesc() {
		return motivoDesc;
	}

	/**
	 * Nome: setMotivoDesc.
	 *
	 * @param motivoDesc the motivo desc
	 * @see
	 */
	public void setMotivoDesc(String motivoDesc) {
		this.motivoDesc = motivoDesc;
	}

	/**
	 * Nome: getNumero.
	 *
	 * @return numero
	 * @see
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Nome: setNumero.
	 *
	 * @param numero the numero
	 * @see
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Nome: getSituacaoDesc.
	 *
	 * @return situacaoDesc
	 * @see
	 */
	public String getSituacaoDesc() {
		return situacaoDesc;
	}

	/**
	 * Nome: setSituacaoDesc.
	 *
	 * @param situacaoDesc the situacao desc
	 * @see
	 */
	public void setSituacaoDesc(String situacaoDesc) {
		this.situacaoDesc = situacaoDesc;
	}

	/**
	 * Nome: getTipo.
	 *
	 * @return tipo
	 * @see
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Nome: setTipo.
	 *
	 * @param tipo the tipo
	 * @see
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Nome: getAtividadeEconomicaMaster.
	 *
	 * @return atividadeEconomicaMaster
	 * @see
	 */
	public String getAtividadeEconomicaMaster() {
		return atividadeEconomicaMaster;
	}

	/**
	 * Nome: setAtividadeEconomicaMaster.
	 *
	 * @param atividadeEconomicaMaster the atividade economica master
	 * @see
	 */
	public void setAtividadeEconomicaMaster(String atividadeEconomicaMaster) {
		this.atividadeEconomicaMaster = atividadeEconomicaMaster;
	}

	/**
	 * Nome: getCpfCnpjMaster.
	 *
	 * @return cpfCnpjMaster
	 * @see
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}

	/**
	 * Nome: setCpfCnpjMaster.
	 *
	 * @param cpfCnpjMaster the cpf cnpj master
	 * @see
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}

	/**
	 * Nome: getCdAgenciaGestora.
	 *
	 * @return cdAgenciaGestora
	 * @see
	 */
	public int getCdAgenciaGestora() {
		return cdAgenciaGestora;
	}

	/**
	 * Nome: setCdAgenciaGestora.
	 *
	 * @param cdAgenciaGestora the cd agencia gestora
	 * @see
	 */
	public void setCdAgenciaGestora(int cdAgenciaGestora) {
		this.cdAgenciaGestora = cdAgenciaGestora;
	}

	/**
	 * Nome: getDsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 * @see
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Nome: setDsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 * @see
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Nome: getGrupoEconomicoMaster.
	 *
	 * @return grupoEconomicoMaster
	 * @see
	 */
	public String getGrupoEconomicoMaster() {
		return grupoEconomicoMaster;
	}

	/**
	 * Nome: setGrupoEconomicoMaster.
	 *
	 * @param grupoEconomicoMaster the grupo economico master
	 * @see
	 */
	public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
		this.grupoEconomicoMaster = grupoEconomicoMaster;
	}

	/**
	 * Nome: getNomeRazaoSocialMaster.
	 *
	 * @return nomeRazaoSocialMaster
	 * @see
	 */
	public String getNomeRazaoSocialMaster() {
		return nomeRazaoSocialMaster;
	}

	/**
	 * Nome: setNomeRazaoSocialMaster.
	 *
	 * @param nomeRazaoSocialMaster the nome razao social master
	 * @see
	 */
	public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
		this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
	}

	/**
	 * Nome: getSegmentoMaster.
	 *
	 * @return segmentoMaster
	 * @see
	 */
	public String getSegmentoMaster() {
		return segmentoMaster;
	}

	/**
	 * Nome: setSegmentoMaster.
	 *
	 * @param segmentoMaster the segmento master
	 * @see
	 */
	public void setSegmentoMaster(String segmentoMaster) {
		this.segmentoMaster = segmentoMaster;
	}

	/**
	 * Nome: getSubSegmentoMaster.
	 *
	 * @return subSegmentoMaster
	 * @see
	 */
	public String getSubSegmentoMaster() {
		return subSegmentoMaster;
	}

	/**
	 * Nome: setSubSegmentoMaster.
	 *
	 * @param subSegmentoMaster the sub segmento master
	 * @see
	 */
	public void setSubSegmentoMaster(String subSegmentoMaster) {
		this.subSegmentoMaster = subSegmentoMaster;
	}

	/**
	 * Nome: getIdentificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 * @see
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Nome: setIdentificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 * @see
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Nome: getManterContratoContasBean.
	 *
	 * @return manterContratoContasBean
	 * @see
	 */
	public ContasBean getManterContratoContasBean() {
		return manterContratoContasBean;
	}

	/**
	 * Nome: setManterContratoContasBean.
	 *
	 * @param manterContratoContasBean the manter contrato contas bean
	 * @see
	 */
	public void setManterContratoContasBean(ContasBean manterContratoContasBean) {
		this.manterContratoContasBean = manterContratoContasBean;
	}

	/**
	 * Nome: getManterContratoDadosBasicosBean.
	 *
	 * @return manterContratoDadosBasicosBean
	 * @see
	 */
	public DadosBasicosBean getManterContratoDadosBasicosBean() {
		return manterContratoDadosBasicosBean;
	}

	/**
	 * Nome: setManterContratoDadosBasicosBean.
	 *
	 * @param manterContratoDadosBasicosBean the manter contrato dados basicos bean
	 * @see
	 */
	public void setManterContratoDadosBasicosBean(
			DadosBasicosBean manterContratoDadosBasicosBean) {
		this.manterContratoDadosBasicosBean = manterContratoDadosBasicosBean;
	}

	/**
	 * Nome: getManterContratoParticipantesBean.
	 *
	 * @return manterContratoParticipantesBean
	 * @see
	 */
	public ParticipantesBean getManterContratoParticipantesBean() {
		return manterContratoParticipantesBean;
	}

	/**
	 * Nome: setManterContratoParticipantesBean.
	 *
	 * @param manterContratoParticipantesBean the manter contrato participantes bean
	 * @see
	 */
	public void setManterContratoParticipantesBean(
			ParticipantesBean manterContratoParticipantesBean) {
		this.manterContratoParticipantesBean = manterContratoParticipantesBean;
	}

	/**
	 * Nome: getComboService.
	 *
	 * @return comboService
	 * @see
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Nome: setComboService.
	 *
	 * @param comboService the combo service
	 * @see
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Nome: getManterContratoImpl.
	 *
	 * @return manterContratoImpl
	 * @see
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Nome: setManterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 * @see
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Nome: getManterContratoLayoutsArquivosBean.
	 *
	 * @return manterContratoLayoutsArquivosBean
	 * @see
	 */
	public LayoutsArquivosBean getManterContratoLayoutsArquivosBean() {
		return manterContratoLayoutsArquivosBean;
	}

	/**
	 * Nome: setManterContratoLayoutsArquivosBean.
	 *
	 * @param manterContratoLayoutsArquivosBean the manter contrato layouts arquivos bean
	 * @see
	 */
	public void setManterContratoLayoutsArquivosBean(
			LayoutsArquivosBean manterContratoLayoutsArquivosBean) {
		this.manterContratoLayoutsArquivosBean = manterContratoLayoutsArquivosBean;
	}

	/**
	 * Nome: getDsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 * @see
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Nome: setDsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 * @see
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/*
	 * public IImprimircontratoService getImprimirContratoService() { return
	 * imprimirContratoService; }
	 * 
	 * public void setImprimirContratoService( IImprimircontratoService
	 * imprimirContratoService) { this.imprimirContratoService =
	 * imprimirContratoService; }
	 */

	/**
	 * Nome: getImprimirContratoService.
	 *
	 * @return imprimirContratoService
	 * @see
	 */
	public IImprimircontratoService getImprimirContratoService() {
		return imprimirContratoService;
	}

	/**
	 * Nome: setImprimirContratoService.
	 *
	 * @param imprimirContratoService the imprimir contrato service
	 * @see
	 */
	public void setImprimirContratoService(
			IImprimircontratoService imprimirContratoService) {
		this.imprimirContratoService = imprimirContratoService;
	}

	/**
	 * Nome: getContratoPgitSelecionado.
	 *
	 * @return contratoPgitSelecionado
	 * @see
	 */
	public ListarContratosPgitSaidaDTO getContratoPgitSelecionado() {
		return contratoPgitSelecionado;
	}

	/**
	 * Nome: setContratoPgitSelecionado.
	 *
	 * @param contratoPgitSelecionado the contrato pgit selecionado
	 * @see
	 */
	public void setContratoPgitSelecionado(
			ListarContratosPgitSaidaDTO contratoPgitSelecionado) {
		this.contratoPgitSelecionado = contratoPgitSelecionado;
	}

	/**
	 * Nome: getManterContratoRepresentanteBean.
	 *
	 * @return manterContratoRepresentanteBean
	 * @see
	 */
	public RepresentanteBean getManterContratoRepresentanteBean() {
		return manterContratoRepresentanteBean;
	}

	/**
	 * Nome: setManterContratoRepresentanteBean.
	 *
	 * @param manterContratoRepresentanteBean the manter contrato representante bean
	 * @see
	 */
	public void setManterContratoRepresentanteBean(
			RepresentanteBean manterContratoRepresentanteBean) {
		this.manterContratoRepresentanteBean = manterContratoRepresentanteBean;
	}

	/**
	 * Nome: isOpcaoChecarTodos.
	 *
	 * @return true, if is opcao checar todos
	 * @see
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Nome: setOpcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 * @see
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Nome: getImprimirService.
	 *
	 * @return imprimirService
	 * @see
	 */
	public IImprimirService getImprimirService() {
		return imprimirService;
	}

	/**
	 * Nome: setImprimirService.
	 *
	 * @param imprimirService the imprimir service
	 * @see
	 */
	public void setImprimirService(IImprimirService imprimirService) {
		this.imprimirService = imprimirService;
	}

	/**
	 * Nome: getImprimirAnexoPrimeiroContratoService.
	 *
	 * @return imprimirAnexoPrimeiroContratoService
	 * @see
	 */
	public IImprimirAnexoPrimeiroContratoService getImprimirAnexoPrimeiroContratoService() {
		return imprimirAnexoPrimeiroContratoService;
	}

	/**
	 * Nome: setImprimirAnexoPrimeiroContratoService.
	 *
	 * @param imprimirAnexoPrimeiroContratoService the imprimir anexo primeiro contrato service
	 * @see
	 */
	public void setImprimirAnexoPrimeiroContratoService(
			IImprimirAnexoPrimeiroContratoService imprimirAnexoPrimeiroContratoService) {
		this.imprimirAnexoPrimeiroContratoService = imprimirAnexoPrimeiroContratoService;
	}

	/**
	 * Nome: getImprimirAnexoSegundoContratoService.
	 *
	 * @return imprimirAnexoSegundoContratoService
	 * @see
	 */
	public IImprimirAnexoSegundoContratoService getImprimirAnexoSegundoContratoService() {
		return imprimirAnexoSegundoContratoService;
	}

	/**
	 * Nome: setImprimirAnexoSegundoContratoService.
	 *
	 * @param imprimirAnexoSegundoContratoService the imprimir anexo segundo contrato service
	 * @see
	 */
	public void setImprimirAnexoSegundoContratoService(
			IImprimirAnexoSegundoContratoService imprimirAnexoSegundoContratoService) {
		this.imprimirAnexoSegundoContratoService = imprimirAnexoSegundoContratoService;
	}

	/**
	 * Nome: getCdGerenteResponsavel.
	 *
	 * @return cdGerenteResponsavel
	 * @see
	 */
	public Long getCdGerenteResponsavel() {
		return cdGerenteResponsavel;
	}

	/**
	 * Nome: setCdGerenteResponsavel.
	 *
	 * @param cdGerenteResponsavel the cd gerente responsavel
	 * @see
	 */
	public void setCdGerenteResponsavel(Long cdGerenteResponsavel) {
		this.cdGerenteResponsavel = cdGerenteResponsavel;
	}

	/**
	 * Nome: getGerenteResponsavelFormatado.
	 *
	 * @return gerenteResponsavelFormatado
	 * @see
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}

	/**
	 * Nome: setGerenteResponsavelFormatado.
	 *
	 * @param gerenteResponsavelFormatado the gerente responsavel formatado
	 * @see
	 */
	public void setGerenteResponsavelFormatado(
			String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}

	/**
	 * Nome: getDsNomeRazaoSocialParticipante.
	 *
	 * @return dsNomeRazaoSocialParticipante
	 * @see
	 */
	public String getDsNomeRazaoSocialParticipante() {
		return dsNomeRazaoSocialParticipante;
	}

	/**
	 * Nome: setDsNomeRazaoSocialParticipante.
	 *
	 * @param dsNomeRazaoSocialParticipante the ds nome razao social participante
	 * @see
	 */
	public void setDsNomeRazaoSocialParticipante(
			String dsNomeRazaoSocialParticipante) {
		this.dsNomeRazaoSocialParticipante = dsNomeRazaoSocialParticipante;
	}

	/**
	 * Nome: getCpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 * @see
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Nome: setCpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 * @see
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Nome: getNumeroContrato.
	 *
	 * @return numeroContrato
	 * @see
	 */
	public Long getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Nome: setNumeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 * @see
	 */
	public void setNumeroContrato(Long numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Nome: getRenegociavel.
	 *
	 * @return renegociavel
	 * @see
	 */
	public String getRenegociavel() {
		return renegociavel;
	}

	/**
	 * Nome: setRenegociavel.
	 *
	 * @param renegociavel the renegociavel
	 * @see
	 */
	public void setRenegociavel(String renegociavel) {
		this.renegociavel = renegociavel;
	}

	/**
	 * Nome: getManterPerfilTrocaArqLayoutServiceImpl.
	 *
	 * @return manterPerfilTrocaArqLayoutServiceImpl
	 * @see
	 */
	public IManterPerfilTrocaArqLayoutService getManterPerfilTrocaArqLayoutServiceImpl() {
		return manterPerfilTrocaArqLayoutServiceImpl;
	}

	/**
	 * Nome: setManterPerfilTrocaArqLayoutServiceImpl.
	 *
	 * @param manterPerfilTrocaArqLayoutServiceImpl the manter perfil troca arq layout service impl
	 * @see
	 */
	public void setManterPerfilTrocaArqLayoutServiceImpl(
			IManterPerfilTrocaArqLayoutService manterPerfilTrocaArqLayoutServiceImpl) {
		this.manterPerfilTrocaArqLayoutServiceImpl = manterPerfilTrocaArqLayoutServiceImpl;
	}

	/**
	 * Nome: getSaidaMeioTransmissao.
	 *
	 * @return saidaMeioTransmissao
	 * @see
	 */
	public ListarContratosPgitSaidaDTO getSaidaMeioTransmissao() {
		return saidaMeioTransmissao;
	}

	/**
	 * Nome: setSaidaMeioTransmissao.
	 *
	 * @param saidaMeioTransmissao the saida meio transmissao
	 * @see
	 */
	public void setSaidaMeioTransmissao(
			ListarContratosPgitSaidaDTO saidaMeioTransmissao) {
		this.saidaMeioTransmissao = saidaMeioTransmissao;
	}

	/**
	 * Nome: getMeiosTransmissaoBean.
	 *
	 * @return meiosTransmissaoBean
	 * @see
	 */
	public MeiosTransmissaoBean getMeiosTransmissaoBean() {
		return meiosTransmissaoBean;
	}

	/**
	 * Nome: getMeiosTransmissaoBean.
	 *
	 * @param meiosTransmissaoBean the meios transmissao bean
	 * @see
	 */
	public void setMeiosTransmissaoBean(
			MeiosTransmissaoBean meiosTransmissaoBean) {
		this.meiosTransmissaoBean = meiosTransmissaoBean;
	}

	/**
	 * Nome: getDescAgenciaGestora.
	 *
	 * @return descAgenciaGestora
	 * @see
	 */
	public Integer getDescAgenciaGestora() {
		return descAgenciaGestora;
	}

	/**
	 * Nome: setDescAgenciaGestora.
	 *
	 * @param descAgenciaGestora the desc agencia gestora
	 * @see
	 */
	public void setDescAgenciaGestora(Integer descAgenciaGestora) {
		this.descAgenciaGestora = descAgenciaGestora;
	}

	/**
	 * Nome: getCodigoAgenciaGestora.
	 *
	 * @return codigoAgenciaGestora
	 * @see
	 */
	public Integer getCodigoAgenciaGestora() {
		return codigoAgenciaGestora;
	}

	/**
	 * Nome: setCodigoAgenciaGestora.
	 *
	 * @param codigoAgenciaGestora the codigo agencia gestora
	 * @see
	 */
	public void setCodigoAgenciaGestora(Integer codigoAgenciaGestora) {
		this.codigoAgenciaGestora = codigoAgenciaGestora;
	}

	/**
	 * Nome: getBancoAgenciaDTO.
	 *
	 * @return bancoAgenciaDTO
	 * @see
	 */
	public ConsultarBancoAgenciaSaidaDTO getBancoAgenciaDTO() {
		return bancoAgenciaDTO;
	}

	/**
	 * Nome: setBancoAgenciaDTO.
	 *
	 * @param bancoAgenciaDTO the banco agencia dto
	 * @see
	 */
	public void setBancoAgenciaDTO(ConsultarBancoAgenciaSaidaDTO bancoAgenciaDTO) {
		this.bancoAgenciaDTO = bancoAgenciaDTO;
	}

	/**
	 * Nome: getManterCadContaDestinoService.
	 *
	 * @return manterCadContaDestinoService
	 * @see
	 */
	public IManterCadContaDestinoService getManterCadContaDestinoService() {
		return manterCadContaDestinoService;
	}

	/**
	 * Nome: setManterCadContaDestinoService.
	 *
	 * @param manterCadContaDestinoService the manter cad conta destino service
	 * @see
	 */
	public void setManterCadContaDestinoService(
			IManterCadContaDestinoService manterCadContaDestinoService) {
		this.manterCadContaDestinoService = manterCadContaDestinoService;
	}

	/**
	 * Nome: getCodDescAgenciaGestora.
	 *
	 * @return codDescAgenciaGestora
	 * @see
	 */
	public String getCodDescAgenciaGestora() {
		return codDescAgenciaGestora;
	}

	/**
	 * Nome: setCodDescAgenciaGestora.
	 *
	 * @param codDescAgenciaGestora the cod desc agencia gestora
	 * @see
	 */
	public void setCodDescAgenciaGestora(String codDescAgenciaGestora) {
		this.codDescAgenciaGestora = codDescAgenciaGestora;
	}

	/**
	 * Nome: getManterContratoTarifasBean.
	 *
	 * @return manterContratoTarifasBean
	 * @see
	 */
	public TarifasBean getManterContratoTarifasBean() {
		return manterContratoTarifasBean;
	}

	/**
	 * Nome: setManterContratoTarifasBean.
	 *
	 * @param manterContratoTarifasBean the manter contrato tarifas bean
	 * @see
	 */
	public void setManterContratoTarifasBean(
			TarifasBean manterContratoTarifasBean) {
		this.manterContratoTarifasBean = manterContratoTarifasBean;
	}

	/**
	 * Nome: setManterContratoServicosBean.
	 *
	 * @param manterContratoServicosBean the manter contrato servicos bean
	 * @see
	 */
	public void setManterContratoServicosBean(
			ServicosBean manterContratoServicosBean) {
		this.manterContratoServicosBean = manterContratoServicosBean;
	}

	/**
	 * Nome: getManterContratoServicosBean.
	 *
	 * @return manterContratoServicosBean
	 * @see
	 */
	public ServicosBean getManterContratoServicosBean() {
		return manterContratoServicosBean;
	}

	/**
	 * Nome: getSaidaValidar.
	 *
	 * @return saidaValidar
	 */
	public ValidarPropostaAndamentoSaidaDTO getSaidaValidar() {
		return saidaValidar;
	}

	/**
	 * Nome: setSaidaValidar.
	 *
	 * @param saidaValidar the saida validar
	 */
	public void setSaidaValidar(ValidarPropostaAndamentoSaidaDTO saidaValidar) {
		this.saidaValidar = saidaValidar;
	}

	/**
	 * Nome: setUrlRenegociacaoNovo.
	 *
	 * @param urlRenegociacaoNovo the url renegociacao novo
	 */
	public void setUrlRenegociacaoNovo(String urlRenegociacaoNovo) {
		this.urlRenegociacaoNovo = urlRenegociacaoNovo;
	}

	/**
	 * Nome: getUrlRenegociacaoNovo.
	 *
	 * @return urlRenegociacaoNovo
	 */
	public String getUrlRenegociacaoNovo() {
		return urlRenegociacaoNovo;
	}

	/**
	 * Get: dsServicoSelecionado.
	 *
	 * @return the dsServicoSelecionado
	 */
	public List<ListarServicosSaidaDTO> getDsServicoSelecionado() {
		return dsServicoSelecionado;
	}

	/**
	 * Set: dsServicoSelecionado.
	 *
	 * @param dsServicoSelecionado the dsServicoSelecionado to set
	 */
	public void setDsServicoSelecionado(
			List<ListarServicosSaidaDTO> dsServicoSelecionado) {
		this.dsServicoSelecionado = dsServicoSelecionado;
	}

	/**
	 * Set: cdTipoArquivoRetorno.
	 *
	 * @param cdTipoArquivoRetorno the cdTipoArquivoRetorno to set
	 */
	public void setCdTipoArquivoRetorno(Integer cdTipoArquivoRetorno) {
		this.cdTipoArquivoRetorno = cdTipoArquivoRetorno;
	}

	/**
	 * Get: cdTipoArquivoRetorno.
	 *
	 * @return the cdTipoArquivoRetorno
	 */
	public Integer getCdTipoArquivoRetorno() {
		return cdTipoArquivoRetorno;
	}

	/**
	 * Get: saidaValidarUsuario.
	 *
	 * @return saidaValidarUsuario
	 */
	public ValidarUsuarioSaidaDTO getSaidaValidarUsuario() {
		return saidaValidarUsuario;
	}

	/**
	 * Set: saidaValidarUsuario.
	 *
	 * @param saidaValidarUsuario the saida validar usuario
	 */
	public void setSaidaValidarUsuario(ValidarUsuarioSaidaDTO saidaValidarUsuario) {
		this.saidaValidarUsuario = saidaValidarUsuario;
	}

	/**
	 * Get: entradaValidarUsuario.
	 *
	 * @return entradaValidarUsuario
	 */
	public ValidarUsuarioEntradaDTO getEntradaValidarUsuario() {
		return entradaValidarUsuario;
	}

	/**
	 * Set: entradaValidarUsuario.
	 *
	 * @param entradaValidarUsuario the entrada validar usuario
	 */
	public void setEntradaValidarUsuario(
			ValidarUsuarioEntradaDTO entradaValidarUsuario) {
		this.entradaValidarUsuario = entradaValidarUsuario;
	}

	/**
	 * Is restringir acesso botoes.
	 *
	 * @return true, if is restringir acesso botoes
	 */
	public boolean isRestringirAcessoBotoes() {
		return restringirAcessoBotoes;
	}

	/**
	 * Set: restringirAcessoBotoes.
	 *
	 * @param restringirAcessoBotoes the restringir acesso botoes
	 */
	public void setRestringirAcessoBotoes(boolean restringirAcessoBotoes) {
		this.restringirAcessoBotoes = restringirAcessoBotoes;
	}

	/**
	 * Get: saidaListaPerfilTrocaArquivo.
	 *
	 * @return the saidaListaPerfilTrocaArquivo
	 */
	public ConsultarListaPerfilTrocaArquivoSaidaDTO getSaidaListaPerfilTrocaArquivo() {
		return saidaListaPerfilTrocaArquivo;
	}

	/**
	 * Set: saidaListaPerfilTrocaArquivo.
	 *
	 * @param saidaListaPerfilTrocaArquivo the saidaListaPerfilTrocaArquivo to set
	 */
	public void setSaidaListaPerfilTrocaArquivo(
			ConsultarListaPerfilTrocaArquivoSaidaDTO saidaListaPerfilTrocaArquivo) {
		this.saidaListaPerfilTrocaArquivo = saidaListaPerfilTrocaArquivo;
	}
	
	/**
	 * Get: dsServicoSegundaTela.
	 *
	 * @return the dsServicoSegundaTela
	 */
	public String getDsServicoSegundaTela() {
		return dsServicoSegundaTela;
	}

	/**
	 * Set: dsServicoSegundaTela.
	 *
	 * @param dsServicoSegundaTela the dsServicoSegundaTela to set
	 */
	public void setDsServicoSegundaTela(String dsServicoSegundaTela) {
		this.dsServicoSegundaTela = dsServicoSegundaTela;
	}

	/**
	 * Get: listaGridTarifasModalidade.
	 *
	 * @return listaGridTarifasModalidade
	 */
	public List<ConsultarTarifaContratoSaidaDTO> getListaGridTarifasModalidade() {
		return listaGridTarifasModalidade;
	}

	/**
	 * Set: listaGridTarifasModalidade.
	 *
	 * @param listaGridTarifasModalidade the lista grid tarifas modalidade
	 */
	public void setListaGridTarifasModalidade(
			List<ConsultarTarifaContratoSaidaDTO> listaGridTarifasModalidade) {
		this.listaGridTarifasModalidade = listaGridTarifasModalidade;
	}

	/**
	 * Is flag metodo imprimir anexo.
	 *
	 * @return true, if is flag metodo imprimir anexo
	 */
	public boolean isFlagMetodoImprimirAnexo() {
		return flagMetodoImprimirAnexo;
	}

	/**
	 * Set: flagMetodoImprimirAnexo.
	 *
	 * @param flagMetodoImprimirAnexo the flag metodo imprimir anexo
	 */
	public void setFlagMetodoImprimirAnexo(boolean flagMetodoImprimirAnexo) {
		this.flagMetodoImprimirAnexo = flagMetodoImprimirAnexo;
	}

	/**
	 * Get: listaTipoArquivoRetorno.
	 *
	 * @return listaTipoArquivoRetorno
	 */
	public List<SelectItem> getListaTipoArquivoRetorno() {
		return listaTipoArquivoRetorno;
	}

	/**
	 * Set: listaTipoArquivoRetorno.
	 *
	 * @param listaTipoArquivoRetorno the lista tipo arquivo retorno
	 */
	public void setListaTipoArquivoRetorno(List<SelectItem> listaTipoArquivoRetorno) {
		this.listaTipoArquivoRetorno = listaTipoArquivoRetorno;
	}

	/**
	 * Get: listaTipoArquivoRetornoHash.
	 *
	 * @return listaTipoArquivoRetornoHash
	 */
	public Map<Integer, String> getListaTipoArquivoRetornoHash() {
		return listaTipoArquivoRetornoHash;
	}

	/**
	 * Set lista tipo arquivo retorno hash.
	 *
	 * @param listaTipoArquivoRetornoHash the lista tipo arquivo retorno hash
	 */
	public void setListaTipoArquivoRetornoHash(
			Map<Integer, String> listaTipoArquivoRetornoHash) {
		this.listaTipoArquivoRetornoHash = listaTipoArquivoRetornoHash;
	}

	/**
	 * Get: listaServicoModalidadeTelaResumo.
	 *
	 * @return listaServicoModalidadeTelaResumo
	 */
	public List<PagamentosDTO> getListaServicoModalidadeTelaResumo() {
		return listaServicoModalidadeTelaResumo;
	}

	/**
	 * Set: listaServicoModalidadeTelaResumo.
	 *
	 * @param listaServicoModalidadeTelaResumo the lista servico modalidade tela resumo
	 */
	public void setListaServicoModalidadeTelaResumo(
			List<PagamentosDTO> listaServicoModalidadeTelaResumo) {
		this.listaServicoModalidadeTelaResumo = listaServicoModalidadeTelaResumo;
	}

	/**
	 * Get: listaPagamentosFornec.
	 *
	 * @return listaPagamentosFornec
	 */
	public List<PagamentosDTO> getListaPagamentosFornec() {
		return listaPagamentosFornec;
	}

	/**
	 * Set: listaPagamentosFornec.
	 *
	 * @param listaPagamentosFornec the lista pagamentos fornec
	 */
	public void setListaPagamentosFornec(List<PagamentosDTO> listaPagamentosFornec) {
		this.listaPagamentosFornec = listaPagamentosFornec;
	}

	/**
	 * Get: listaPagamentosBeneficios.
	 *
	 * @return listaPagamentosBeneficios
	 */
	public List<PagamentosDTO> getListaPagamentosBeneficios() {
		return listaPagamentosBeneficios;
	}

	/**
	 * Set: listaPagamentosBeneficios.
	 *
	 * @param listaPagamentosBeneficios the lista pagamentos beneficios
	 */
	public void setListaPagamentosBeneficios(
			List<PagamentosDTO> listaPagamentosBeneficios) {
		this.listaPagamentosBeneficios = listaPagamentosBeneficios;
	}

	/**
	 * Get: listaBeneficiosModularidades.
	 *
	 * @return listaBeneficiosModularidades
	 */
	public List<PagamentosDTO> getListaBeneficiosModularidades() {
		return listaBeneficiosModularidades;
	}

	/**
	 * Set: listaBeneficiosModularidades.
	 *
	 * @param listaBeneficiosModularidades the lista beneficios modularidades
	 */
	public void setListaBeneficiosModularidades(
			List<PagamentosDTO> listaBeneficiosModularidades) {
		this.listaBeneficiosModularidades = listaBeneficiosModularidades;
	}

	/**
	 * Get: listaServicos.
	 *
	 * @return listaServicos
	 */
	public List<ServicosDTO> getListaServicos() {
		return listaServicos;
	}

	/**
	 * Set: listaServicos.
	 *
	 * @param listaServicos the lista servicos
	 */
	public void setListaServicos(List<ServicosDTO> listaServicos) {
		this.listaServicos = listaServicos;
	}

	/**
	 * Get: listaPagamentosSalarios.
	 *
	 * @return listaPagamentosSalarios
	 */
	public List<PagamentosDTO> getListaPagamentosSalarios() {
		return listaPagamentosSalarios;
	}

	/**
	 * Set: listaPagamentosSalarios.
	 *
	 * @param listaPagamentosSalarios the lista pagamentos salarios
	 */
	public void setListaPagamentosSalarios(
			List<PagamentosDTO> listaPagamentosSalarios) {
		this.listaPagamentosSalarios = listaPagamentosSalarios;
	}

	/**
	 * Get: listaPagamentosTri.
	 *
	 * @return listaPagamentosTri
	 */
	public List<PagamentosDTO> getListaPagamentosTri() {
		return listaPagamentosTri;
	}

	/**
	 * Set: listaPagamentosTri.
	 *
	 * @param listaPagamentosTri the lista pagamentos tri
	 */
	public void setListaPagamentosTri(List<PagamentosDTO> listaPagamentosTri) {
		this.listaPagamentosTri = listaPagamentosTri;
	}

	/**
	 * Get: listaTitulos.
	 *
	 * @return listaTitulos
	 */
	public List<PagamentosDTO> getListaTitulos() {
		return listaTitulos;
	}

	/**
	 * Set: listaTitulos.
	 *
	 * @param listaTitulos the lista titulos
	 */
	public void setListaTitulos(List<PagamentosDTO> listaTitulos) {
		this.listaTitulos = listaTitulos;
	}

	/**
	 * Get: listaFornModularidades.
	 *
	 * @return listaFornModularidades
	 */
	public List<PagamentosDTO> getListaFornModularidades() {
		return listaFornModularidades;
	}

	/**
	 * Set: listaFornModularidades.
	 *
	 * @param listaFornModularidades the lista forn modularidades
	 */
	public void setListaFornModularidades(List<PagamentosDTO> listaFornModularidades) {
		this.listaFornModularidades = listaFornModularidades;
	}

	/**
	 * Get: listaTri.
	 *
	 * @return listaTri
	 */
	public List<PagamentosDTO> getListaTri() {
		return listaTri;
	}

	/**
	 * Set: listaTri.
	 *
	 * @param listaTri the lista tri
	 */
	public void setListaTri(List<PagamentosDTO> listaTri) {
		this.listaTri = listaTri;
	}

	/**
	 * Get: listaServicosCompl.
	 *
	 * @return listaServicosCompl
	 */
	public List<ServicosDTO> getListaServicosCompl() {
		return listaServicosCompl;
	}

	/**
	 * Set: listaServicosCompl.
	 *
	 * @param listaServicosCompl the lista servicos compl
	 */
	public void setListaServicosCompl(List<ServicosDTO> listaServicosCompl) {
		this.listaServicosCompl = listaServicosCompl;
	}

	/**
	 * Get: listaServicosModalidades.
	 *
	 * @return listaServicosModalidades
	 */
	public List<ServicosModalidadesDTO> getListaServicosModalidades() {
		return listaServicosModalidades;
	}

	/**
	 * Set: listaServicosModalidades.
	 *
	 * @param listaServicosModalidades the lista servicos modalidades
	 */
	public void setListaServicosModalidades(
			List<ServicosModalidadesDTO> listaServicosModalidades) {
		this.listaServicosModalidades = listaServicosModalidades;
	}

	/**
	 * Get: listaFornecedorOutrasModularidades.
	 *
	 * @return listaFornecedorOutrasModularidades
	 */
	public List<PagamentosDTO> getListaFornecedorOutrasModularidades() {
		return listaFornecedorOutrasModularidades;
	}

	/**
	 * Set: listaFornecedorOutrasModularidades.
	 *
	 * @param listaFornecedorOutrasModularidades the lista fornecedor outras modularidades
	 */
	public void setListaFornecedorOutrasModularidades(
			List<PagamentosDTO> listaFornecedorOutrasModularidades) {
		this.listaFornecedorOutrasModularidades = listaFornecedorOutrasModularidades;
	}

	/**
	 * Get: listaAnexoContrato.
	 *
	 * @return listaAnexoContrato
	 */
	public List<AnexoContratoDTO> getListaAnexoContrato() {
		return listaAnexoContrato;
	}

	/**
	 * Set: listaAnexoContrato.
	 *
	 * @param listaAnexoContrato the lista anexo contrato
	 */
	public void setListaAnexoContrato(List<AnexoContratoDTO> listaAnexoContrato) {
		this.listaAnexoContrato = listaAnexoContrato;
	}

	/**
	 * Get: listaSalariosMod.
	 *
	 * @return listaSalariosMod
	 */
	public List<PagamentosDTO> getListaSalariosMod() {
		return listaSalariosMod;
	}

	/**
	 * Set: listaSalariosMod.
	 *
	 * @param listaSalariosMod the lista salarios mod
	 */
	public void setListaSalariosMod(List<PagamentosDTO> listaSalariosMod) {
		this.listaSalariosMod = listaSalariosMod;
	}

	/**
	 * Get: listaParticipantes.
	 *
	 * @return listaParticipantes
	 */
	public List<OcorrenciasParticipantes> getListaParticipantes() {
		return listaParticipantes;
	}

	/**
	 * Set: listaParticipantes.
	 *
	 * @param listaParticipantes the lista participantes
	 */
	public void setListaParticipantes(
			List<OcorrenciasParticipantes> listaParticipantes) {
		this.listaParticipantes = listaParticipantes;
	}

	/**
	 * Get: listaContas.
	 *
	 * @return listaContas
	 */
	public List<OcorrenciasContas> getListaContas() {
		return listaContas;
	}

	/**
	 * Set: listaContas.
	 *
	 * @param listaContas the lista contas
	 */
	public void setListaContas(List<OcorrenciasContas> listaContas) {
		this.listaContas = listaContas;
	}

	/**
	 * Get: listaLayouts.
	 *
	 * @return listaLayouts
	 */
	public List<OcorrenciasLayout> getListaLayouts() {
		return listaLayouts;
	}

	/**
	 * Set: listaLayouts.
	 *
	 * @param listaLayouts the lista layouts
	 */
	public void setListaLayouts(List<OcorrenciasLayout> listaLayouts) {
		this.listaLayouts = listaLayouts;
	}

	/**
	 * Get: exibePgtoFornecedor.
	 *
	 * @return exibePgtoFornecedor
	 */
	public String getExibePgtoFornecedor() {
		return exibePgtoFornecedor;
	}

	/**
	 * Set: exibePgtoFornecedor.
	 *
	 * @param exibePgtoFornecedor the exibe pgto fornecedor
	 */
	public void setExibePgtoFornecedor(String exibePgtoFornecedor) {
		this.exibePgtoFornecedor = exibePgtoFornecedor;
	}

	/**
	 * Get: exibePgtoTributos.
	 *
	 * @return exibePgtoTributos
	 */
	public String getExibePgtoTributos() {
		return exibePgtoTributos;
	}

	/**
	 * Set: exibePgtoTributos.
	 *
	 * @param exibePgtoTributos the exibe pgto tributos
	 */
	public void setExibePgtoTributos(String exibePgtoTributos) {
		this.exibePgtoTributos = exibePgtoTributos;
	}

	/**
	 * Get: exibePgtoSalarios.
	 *
	 * @return exibePgtoSalarios
	 */
	public String getExibePgtoSalarios() {
		return exibePgtoSalarios;
	}

	/**
	 * Set: exibePgtoSalarios.
	 *
	 * @param exibePgtoSalarios the exibe pgto salarios
	 */
	public void setExibePgtoSalarios(String exibePgtoSalarios) {
		this.exibePgtoSalarios = exibePgtoSalarios;
	}

	/**
	 * Get: exibePgtoBeneficios.
	 *
	 * @return exibePgtoBeneficios
	 */
	public String getExibePgtoBeneficios() {
		return exibePgtoBeneficios;
	}

	/**
	 * Set: exibePgtoBeneficios.
	 *
	 * @param exibePgtoBeneficios the exibe pgto beneficios
	 */
	public void setExibePgtoBeneficios(String exibePgtoBeneficios) {
		this.exibePgtoBeneficios = exibePgtoBeneficios;
	}

	/**
	 * Get: imprimirGridTarifa.
	 *
	 * @return imprimirGridTarifa
	 */
	public String getImprimirGridTarifa() {
		return imprimirGridTarifa;
	}

	/**
	 * Set: imprimirGridTarifa.
	 *
	 * @param imprimirGridTarifa the imprimir grid tarifa
	 */
	public void setImprimirGridTarifa(String imprimirGridTarifa) {
		this.imprimirGridTarifa = imprimirGridTarifa;
	}

	/**
	 * Get: nomeTabelaAtual.
	 *
	 * @return nomeTabelaAtual
	 */
	public String getNomeTabelaAtual() {
		return nomeTabelaAtual;
	}

	/**
	 * Set: nomeTabelaAtual.
	 *
	 * @param nomeTabelaAtual the nome tabela atual
	 */
	public void setNomeTabelaAtual(String nomeTabelaAtual) {
		this.nomeTabelaAtual = nomeTabelaAtual;
	}

	/**
	 * Is omite servicos resumo contrato.
	 *
	 * @return true, if is omite servicos resumo contrato
	 */
	public boolean isOmiteServicosResumoContrato() {
		return omiteServicosResumoContrato;
	}

	/**
	 * Set: omiteServicosResumoContrato.
	 *
	 * @param omiteServicosResumoContrato the omite servicos resumo contrato
	 */
	public void setOmiteServicosResumoContrato(boolean omiteServicosResumoContrato) {
		this.omiteServicosResumoContrato = omiteServicosResumoContrato;
	}
    /**
     * Nome: setContratoServiceImpl.
     *
     * @param contratoServiceImpl the contrato service impl
     */
    public void setContratoServiceImpl(IContratoService contratoServiceImpl) {
        this.contratoServiceImpl = contratoServiceImpl;
    }

    /**
     * Nome: getContratoServiceImpl.
     *
     * @return contratoServiceImpl
     */
    public IContratoService getContratoServiceImpl() {
        return contratoServiceImpl;
    }

	public DetalharContaComplementarSaidaDTO getDetalharContaComplementar() {
		return detalharContaComplementar;
	}

	public void setDetalharContaComplementar(
			DetalharContaComplementarSaidaDTO detalharContaComplementar) {
		this.detalharContaComplementar = detalharContaComplementar;
	}


	public List<SelectItem> getListaPesquisaControle() {
		return listaPesquisaControle;
	}


	public void setListaPesquisaControle(List<SelectItem> listaPesquisaControle) {
		this.listaPesquisaControle = listaPesquisaControle;
	}


	public List<ListarContaComplementarOcorrenciasSaida> getListaGridComplementar() {
		return listaGridComplementar;
	}


	public void setListaGridComplementar(
			List<ListarContaComplementarOcorrenciasSaida> listaGridComplementar) {
		this.listaGridComplementar = listaGridComplementar;
	}


	public List<ListarContaComplParticipantesSaidaDTO> getListaGridComplementarParticipante() {
		return listaGridComplementarParticipante;
	}


	public void setListaGridComplementarParticipante(
			List<ListarContaComplParticipantesSaidaDTO> listaGridComplementarParticipante) {
		this.listaGridComplementarParticipante = listaGridComplementarParticipante;
	}


	public List<ListarContasVinculadasContratoSaidaDTO> getListaGridAgenciaConta() {
		return listaGridAgenciaConta;
	}


	public void setListaGridAgenciaConta(
			List<ListarContasVinculadasContratoSaidaDTO> listaGridAgenciaConta) {
		this.listaGridAgenciaConta = listaGridAgenciaConta;
	}


	public Integer getItemSelecionadoListaParticipantes() {
		return itemSelecionadoListaParticipantes;
	}


	public void setItemSelecionadoListaParticipantes(
			Integer itemSelecionadoListaParticipantes) {
		this.itemSelecionadoListaParticipantes = itemSelecionadoListaParticipantes;
	}


	public String getCdCpfCGCIncComplementar() {
		return cdCpfCGCIncComplementar;
	}


	public void setCdCpfCGCIncComplementar(String cdCpfCGCIncComplementar) {
		this.cdCpfCGCIncComplementar = cdCpfCGCIncComplementar;
	}


	public String getCdnomeRazaoSocialIncComplementar() {
		return cdnomeRazaoSocialIncComplementar;
	}


	public void setCdnomeRazaoSocialIncComplementar(
			String cdnomeRazaoSocialIncComplementar) {
		this.cdnomeRazaoSocialIncComplementar = cdnomeRazaoSocialIncComplementar;
	}


	public String getCdBancoIncComplementar() {
		return cdBancoIncComplementar;
	}


	public void setCdBancoIncComplementar(String cdBancoIncComplementar) {
		this.cdBancoIncComplementar = cdBancoIncComplementar;
	}


	public String getCdAgenciaIncComplementar() {
		return cdAgenciaIncComplementar;
	}


	public void setCdAgenciaIncComplementar(String cdAgenciaIncComplementar) {
		this.cdAgenciaIncComplementar = cdAgenciaIncComplementar;
	}


	public String getCdContaIncComplementar() {
		return cdContaIncComplementar;
	}


	public void setCdContaIncComplementar(String cdContaIncComplementar) {
		this.cdContaIncComplementar = cdContaIncComplementar;
	}


	public String getCdDataHoraInclusao() {
		return cdDataHoraInclusao;
	}


	public void setCdDataHoraInclusao(String cdDataHoraInclusao) {
		this.cdDataHoraInclusao = cdDataHoraInclusao;
	}


	public String getCdDataHoraInclusaoUsuario() {
		return cdDataHoraInclusaoUsuario;
	}


	public void setCdDataHoraInclusaoUsuario(String cdDataHoraInclusaoUsuario) {
		this.cdDataHoraInclusaoUsuario = cdDataHoraInclusaoUsuario;
	}


	public String getCdDataHoraManutencao() {
		return cdDataHoraManutencao;
	}


	public void setCdDataHoraManutencao(String cdDataHoraManutencao) {
		this.cdDataHoraManutencao = cdDataHoraManutencao;
	}


	public String getCdDataHoraManutencaoUsuario() {
		return cdDataHoraManutencaoUsuario;
	}


	public void setCdDataHoraManutencaoUsuario(String cdDataHoraManutencaoUsuario) {
		this.cdDataHoraManutencaoUsuario = cdDataHoraManutencaoUsuario;
	}


	public String getCdTrocaOpcao() {
		return cdTrocaOpcao;
	}


	public void setCdTrocaOpcao(String cdTrocaOpcao) {
		this.cdTrocaOpcao = cdTrocaOpcao;
	}


	public Integer getItemSelecionadoListaAgenciaConta() {
		return itemSelecionadoListaAgenciaConta;
	}


	public void setItemSelecionadoListaAgenciaConta(
			Integer itemSelecionadoListaAgenciaConta) {
		this.itemSelecionadoListaAgenciaConta = itemSelecionadoListaAgenciaConta;
	}


	public Integer getItemSelecionadoListaManterCtaPesquisar() {
		return itemSelecionadoListaManterCtaPesquisar;
	}


	public void setItemSelecionadoListaManterCtaPesquisar(
			Integer itemSelecionadoListaManterCtaPesquisar) {
		this.itemSelecionadoListaManterCtaPesquisar = itemSelecionadoListaManterCtaPesquisar;
	}


	public Integer getCdContaPointerIncComplementar() {
		return cdContaPointerIncComplementar;
	}


	public void setCdContaPointerIncComplementar(
			Integer cdContaPointerIncComplementar) {
		this.cdContaPointerIncComplementar = cdContaPointerIncComplementar;
	}


	public String getItemSelecionadoConsultaParticipante() {
		return itemSelecionadoConsultaParticipante;
	}


	public void setItemSelecionadoConsultaParticipante(
			String itemSelecionadoConsultaParticipante) {
		this.itemSelecionadoConsultaParticipante = itemSelecionadoConsultaParticipante;
	}


	public String getItemSelecionadoConsultaAgenciaConta() {
		return itemSelecionadoConsultaAgenciaConta;
	}


	public void setItemSelecionadoConsultaAgenciaConta(
			String itemSelecionadoConsultaAgenciaConta) {
		this.itemSelecionadoConsultaAgenciaConta = itemSelecionadoConsultaAgenciaConta;
	}


	public List<SelectItem> getListaPesquisaControleParticipante() {
		return listaPesquisaControleParticipante;
	}


	public void setListaPesquisaControleParticipante(
			List<SelectItem> listaPesquisaControleParticipante) {
		this.listaPesquisaControleParticipante = listaPesquisaControleParticipante;
	}


	public List<SelectItem> getListaPesquisaControleAgenciaConta() {
		return listaPesquisaControleAgenciaConta;
	}


	public void setListaPesquisaControleAgenciaConta(
			List<SelectItem> listaPesquisaControleAgenciaConta) {
		this.listaPesquisaControleAgenciaConta = listaPesquisaControleAgenciaConta;
	}

	public Integer getItemSelecionadoPesquisa() {
		return itemSelecionadoPesquisa;
	}

	public void setItemSelecionadoPesquisa(Integer itemSelecionadoPesquisa) {
		this.itemSelecionadoPesquisa = itemSelecionadoPesquisa;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCnpj2() {
		return cnpj2;
	}

	public void setCnpj2(String cnpj2) {
		this.cnpj2 = cnpj2;
	}

	public String getCnpj3() {
		return cnpj3;
	}

	public void setCnpj3(String cnpj3) {
		this.cnpj3 = cnpj3;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCpf2() {
		return cpf2;
	}

	public void setCpf2(String cpf2) {
		this.cpf2 = cpf2;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getAgenciaDig() {
		return agenciaDig;
	}

	public void setAgenciaDig(String agenciaDig) {
		this.agenciaDig = agenciaDig;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public String getContaDig() {
		return contaDig;
	}

	public void setContaDig(String contaDig) {
		this.contaDig = contaDig;
	}
	
}