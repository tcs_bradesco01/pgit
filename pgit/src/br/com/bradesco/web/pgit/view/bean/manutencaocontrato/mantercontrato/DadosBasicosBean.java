/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarListaValoresDiscretosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarListaValoresDiscretosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.IdiomaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarParametrosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarParametrosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.MoedaEconomicaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.MoedaEconomicaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.IListarFuncBradescoService;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarDadosBasicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarDadosBasicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.VerificarAtributosDadosBasicosEntradaDto;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.VerificarAtributosDadosBasicosSaidaDto;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.IParametroContratoService;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ExcluirParametroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ExcluirParametroContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.IncluirParametroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.IncluirParametroContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ListarParametroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ListarParametroContratoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.parametrocontrato.bean.ListarParametroContratoSaidaDTO;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.selectitem.SelectItemUtils;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: DadosBasicosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DadosBasicosBean {
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo comboService. */
	private IParametroContratoService parametroContratoServiceImpl;
	
	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;
	
	/** Atributo gerenteDTO. */
	private ListarFuncionarioSaidaDTO gerenteDTO = new ListarFuncionarioSaidaDTO();
	
	/** Atributo listarGerenteBradescoService. */
	private IListarFuncBradescoService listarGerenteBradescoService;
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo historicoBean. */
	private HistoricoBean historicoBean;	
	
	/** Atributo cpfCnpjClienteMaster. */
	private String cpfCnpjClienteMaster;
	
	/** Atributo nomeRazaoClienteMaster. */
	private String nomeRazaoClienteMaster;
	
	/** Atributo cdGrupoEconomicoClienteMaster. */
	private Long cdGrupoEconomicoClienteMaster;
	
	/** Atributo grupoEconomicoClienteMaster. */
	private String grupoEconomicoClienteMaster;
	
	/** Atributo cdAtividadeEconomicaClienteMaster. */
	private int cdAtividadeEconomicaClienteMaster;
	
	/** Atributo atividadeEconomicaClienteMaster. */
	private String atividadeEconomicaClienteMaster;
	
	/** Atributo cdSegmentoClienteMaster. */
	private int cdSegmentoClienteMaster;
	
	/** Atributo segmentoClienteMaster. */
	private String segmentoClienteMaster;
	
	/** Atributo cdSubSegmentoClienteMaster. */
	private int cdSubSegmentoClienteMaster;
	
	/** Atributo subSegmentoClienteMaster. */
	private String subSegmentoClienteMaster;
	
	/** Atributo cdEmpresaGestoraContrato. */
	private Long cdEmpresaGestoraContrato;
	
	/** Atributo empresaGestoraContrato. */
	private String empresaGestoraContrato;
	
	/** Atributo cdTipoContrato. */
	private int cdTipoContrato;
	
	/** Atributo tipoContrato. */
	private String tipoContrato;
	
	/** Atributo numeroContrato. */
	private String numeroContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo cdIdiomaContrato. */
	private int cdIdiomaContrato;
	
	/** Atributo idiomaContrato. */
	private String idiomaContrato;
	
	/** Atributo cdMoedaContrato. */
	private int cdMoedaContrato;
	
	/** Atributo moedaContrato. */
	private String moedaContrato;
	
	/** Atributo cdOrigemContrato. */
	private int cdOrigemContrato;
	
	/** Atributo origemContrato. */
	private String origemContrato;
	
	/** Atributo possuiAditivosContrato. */
	private String possuiAditivosContrato;
	
	/** Atributo cdSituacaoContrato. */
	private int cdSituacaoContrato;
	
	/** Atributo situacaoContrato. */
	private String situacaoContrato;
	
	/** Atributo cdMotivoContrato. */
	private int cdMotivoContrato;
	
	/** Atributo motivoContrato. */
	private String motivoContrato;
	
	/** Atributo inicioVigenciaContrato. */
	private String inicioVigenciaContrato;
	
	/** Atributo fimVigenciaContrato. */
	private String fimVigenciaContrato;
	
	/** Atributo numeroComercialContrato. */
	private String numeroComercialContrato;
	
	/** Atributo participacaoContrato. */
	private String participacaoContrato;
	
	/** Atributo dataHoraAssinaturaContrato. */
	private String dataHoraAssinaturaContrato;
	
	/** Atributo cdPermiteEncerramentoContrato. */
	private int cdPermiteEncerramentoContrato;
	
	/** Atributo numeroContratoUnidOrg. */
	private int numeroContratoUnidOrg;
	
	/** Atributo cdOperadoraUnidOrg. */
	private int cdOperadoraUnidOrg;
	
	/** Atributo cdContabilUnidOrg. */
	private int cdContabilUnidOrg;
	
	/** Atributo cdGestoraUnidOrg. */
	private int cdGestoraUnidOrg;
	
	/** Atributo gestoraUnidOrg. */
	private String gestoraUnidOrg;
	
	/** Atributo cdGerenteResponsavelUnidOrg. */
	private Long cdGerenteResponsavelUnidOrg;
	
	/** Atributo gerenteResponsavelUnidOrg. */
	private String gerenteResponsavelUnidOrg;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	/** Atributo descricaoDadosBasicos. */
	private String descricaoDadosBasicos;
	
	/** Atributo codigoGerente. */
	private String codigoGerente;
	
	/** Atributo nomeGerente. */
	private String nomeGerente;
	
	/** Atributo codIdioma. */
	private Integer codIdioma;
	
	/** Atributo dsIdioma. */
	private String dsIdioma;
	
	/** Atributo codMoeda. */
	private Integer codMoeda;
	
	/** Atributo dsMoeda. */
	private String dsMoeda;
	
	/** Atributo codMotivoSituacao. */
	private Integer codMotivoSituacao;
	
	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;
	
	/** Atributo codSituacaoContrato. */
	private Integer codSituacaoContrato;
	
	/** Atributo cdClub. */
	private Long cdClub;
	
	/** Atributo listaIdioma. */
	private List<SelectItem> listaIdioma = new ArrayList<SelectItem>();
	
	/** Atributo listaIdiomaHash. */
	private Map<Integer,String> listaIdiomaHash = new HashMap<Integer,String>();
	
	/** Atributo listaMoeda. */
	private List<SelectItem> listaMoeda = new ArrayList<SelectItem>();
	
	/** Atributo listaMoedaHash. */
	private Map<Integer,String> listaMoedaHash = new HashMap<Integer,String>();
	
	/** Atributo listaMotivo. */
	private List<SelectItem> listaMotivo = new ArrayList<SelectItem>();
	
	/** Atributo listaMotivoHash. */
	private Map<Integer,String> listaMotivoHash = new HashMap<Integer,String>();
	
	/** Atributo listaSetor. */
	private List<SelectItem> listaSetor = new ArrayList<SelectItem>();
	
	/** Atributo listaSetorHash. */
	private Map<Integer,String> listaSetorHash = new HashMap<Integer,String>();
	
	/** Atributo numeroSequencialUnidadeOrganizacional. */
	private int numeroSequencialUnidadeOrganizacional;
	
	/** Atributo codPessoaJuridica. */
	private Long codPessoaJuridica;
	
	/** Atributo dsGerente. */
	private String dsGerente;
	
	/** Atributo dsNomeRazaoSocialParticipante. */
	private String dsNomeRazaoSocialParticipante; 
	
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;
	
	/** Atributo cdSetorContrato. */
	private Integer cdSetorContrato;
	
	/** Atributo dsSetorContrato. */
	private String dsSetorContrato;

	/** Atributo cdSetorContratoAlteracao. */
	private Integer cdSetorContratoAlteracao = null;
	
	/** Atributo dsSetorContratoFiltro. */
	private String dsSetorContratoFiltro;
	
	/** Atributo empresaGestoraProposta. */
	private String empresaGestoraProposta;
	
	/** Atributo tipoProposta. */
	private String tipoProposta;
	
	/** Atributo numeroProposta. */
	private Long numeroProposta;
	
	/** Atributo vlSaldoVirtualTeste. */
	private BigDecimal vlSaldoVirtualTeste;

	/** Atributo vlSaldoVirtualTesteAlteracao. */
	private BigDecimal vlSaldoVirtualTesteAlteracao = null;
	
	/** Atributo cdDeptoGestor. */
	private Integer cdDeptoGestor;
	
	/** Atributo nmDepto. */
	private String nmDepto;
	
	/** Atributo nrSequenciaContratoOutros. */
	private Long nrSequenciaContratoOutros;
	
	/** Atributo nrSequenciaContratoPagamentoSalario. */
	private Long nrSequenciaContratoPagamentoSalario;

	/** Atributo cdFormaAutorizacaoPagamento. */
	private Integer cdFormaAutorizacaoPagamento = null;

	/** Atributo dsFormaAutorizacaoPagamento. */
	private String dsFormaAutorizacaoPagamento = null;

	/** Atributo cdFormaAutorizacaoPagamentoAlteracao. */
	private Integer cdFormaAutorizacaoPagamentoAlteracao = null;

	/** Atributo listaControleRadioParametro. */
	private List<SelectItem> listaControleRadioParametro;
	
	/** Atributo listaGridPesquisaParametro. */
	private List<ListarParametroContratoOcorrenciasSaidaDTO> listaGridPesquisaParametro;
	
	/** Atributo comboParametros. */
	private List<SelectItem> comboParametros;
	
	/** Atributo saidaListarParametros. */
	private ListarParametrosSaidaDTO saidaListarParametros;
	
	/** Atributo itemSelecionadoComboParametro. */
	private Integer itemSelecionadoComboParametro;
	
	/** Atributo itemSelecionadoListaParamentro. */
	private Integer itemSelecionadoListaParamentro;
	
	/** Atributo cdIndicadorUtilizaDescricao. */
	private Integer cdIndicadorUtilizaDescricao;
	
	/** Atributo dsParametroContrato. */
	private String dsParametroContrato;
	
	/** Atributo parametroSelecionado. */
	private String parametroSelecionado;

	/** Atributo listaOpcoesRadioFormaAutorizacao. */
	private List<SelectItem> listaOpcoesRadioFormaAutorizacao = null;

	/** Atributo saidaVerificaDadosBasico. */
    private VerificarAtributosDadosBasicosSaidaDto saidaVerificaDadosBasico =
        new VerificarAtributosDadosBasicosSaidaDto();

	//M�todos//
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		//identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		carregaListaIdioma();
		carregaListaMoeda();
		carregaListaSetor();
		carregaRadioFormaAutorizacao();

		setDescricaoDadosBasicos(getDescricaoContrato());
		setCodIdioma(getCdIdiomaContrato());
		setCodMoeda(getCdMoedaContrato());
		setCodigoGerente(String.valueOf(getCdGerenteResponsavelUnidOrg()));
		
		setGerenteDTO(new ListarFuncionarioSaidaDTO());
		getGerenteDTO().setCdFuncionario(getCdGerenteResponsavelUnidOrg());
		getGerenteDTO().setDsFuncionario(getDsGerente());

		cdSetorContratoAlteracao = cdSetorContrato;
		cdFormaAutorizacaoPagamentoAlteracao = cdFormaAutorizacaoPagamento;
		vlSaldoVirtualTesteAlteracao = vlSaldoVirtualTeste;

		VerificarAtributosDadosBasicosEntradaDto entradaVerificar = new VerificarAtributosDadosBasicosEntradaDto();
		entradaVerificar.setCdPessoaJuridicaContrato(cdEmpresaGestoraContrato);
		entradaVerificar.setCdTipoContratoNegocio(cdTipoContrato);
		entradaVerificar.setNrSequenciaContratoNegocio(NumberUtils.parseLong(numeroContrato));

        saidaVerificaDadosBasico = manterContratoImpl.verificarAtributosDadosBasicos(entradaVerificar);

		return "ALTERAR";
	}
	
	/**
	 * Historico.
	 *
	 * @return the string
	 */
	public String historico(){
		historicoBean.limpaDados();
		
		historicoBean.setEmpresaGestoraContrato(getCdEmpresaGestoraContrato());
		historicoBean.setTipoContrato(getCdTipoContrato());
		historicoBean.setNumero(getNumeroContrato());
		historicoBean.setCdEmpresaGestoraContratoFiltro(getCdEmpresaGestoraContrato());
		historicoBean.setCdTipoContratoFiltro(getCdTipoContrato());
		historicoBean.setNumeroContratoFiltro(getNumeroContrato());
		
		return "HISTORICO";
	}
	
	/**
	 * Limpar alterar.
	 */
	public void limparAlterar() {
		this.descricaoDadosBasicos = "";
		this.codMoeda = 0;
		this.codIdioma = 0;
		this.cdSetorContratoAlteracao = 0;
		this.cdFormaAutorizacaoPagamentoAlteracao = null;
		this.vlSaldoVirtualTesteAlteracao = BigDecimal.ZERO;
		this.codigoGerente = "";
		gerenteDTO = new ListarFuncionarioSaidaDTO();
	}
	
	/**
	 * Buscar gerente.
	 *
	 * @return the string
	 */
	public String buscarGerente(){
		try {
			gerenteDTO = new ListarFuncionarioSaidaDTO();
			setGerenteDTO(getListarGerenteBradescoService().listarFuncionario(Long.parseLong(getCodigoGerente())));
				
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setGerenteDTO(new ListarFuncionarioSaidaDTO());
		}
		
		return "";
	}
	
	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {
		setDsIdioma((String)listaIdiomaHash.get(getCodIdioma()));
		setDsMoeda((String)listaMoedaHash.get(getCodMoeda()));
		setDsSetorContratoFiltro((String)listaSetorHash.get(getCdSetorContrato()));
		setNomeGerente(gerenteDTO.getDsFuncionario());
        setDsFormaAutorizacaoPagamento(SelectItemUtils.obterLabelSelecItem(listaOpcoesRadioFormaAutorizacao,
            cdFormaAutorizacaoPagamentoAlteracao));
		return "AVANCAR_ALTERAR";
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {
		AlterarDadosBasicoContratoEntradaDTO entradaDTO = new AlterarDadosBasicoContratoEntradaDTO();
	
		entradaDTO.setCdPessoaJuridicaContrato(getCdEmpresaGestoraContrato());
		entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato());
		entradaDTO.setNrSequenciaContratoNegocio(Long.parseLong(getNumeroContrato()));
		entradaDTO.setCdSituacaoContrato(getCdSituacaoContrato());
		entradaDTO.setCdMotivoSituacaoOperacao(getCdMotivoContrato());
		entradaDTO.setCdPermissaoEncerramentoContrato(Integer.toString(getCdPermiteEncerramentoContrato()));
		entradaDTO.setCdUsuario(getUsuarioInclusao());
		entradaDTO.setCdUnidadeOperacionalContrato(getCdOperadoraUnidOrg());
		entradaDTO.setCdPessoaJuridica(getCodPessoaJuridica());
		entradaDTO.setNrSequenciaUnidadeOrganizacional(getNumeroSequencialUnidadeOrganizacional());
		entradaDTO.setVlSaldoVirtualTeste(vlSaldoVirtualTesteAlteracao);
		entradaDTO.setCdFormaAutorizacaoPagamento(cdFormaAutorizacaoPagamentoAlteracao);

		//Dados B�sicos - Altera��o
		
        if (getDescricaoDadosBasicos() == null) {
            entradaDTO.setDsContratoNegocio("");
        } else {
            entradaDTO.setDsContratoNegocio(getDescricaoDadosBasicos());
        }
        if (getCodMoeda() == null || getCodMoeda().equals("0")) {
            entradaDTO.setCdIndicadorEconomicoMoeda(0);
        } else {
            entradaDTO.setCdIndicadorEconomicoMoeda(getCodMoeda());
        }
        if (getCodIdioma() == null || getCodIdioma().equals("0")) {
            entradaDTO.setCdIdioma(0);
        } else {
            entradaDTO.setCdIdioma(getCodIdioma());
        }

        if (getCodigoGerente().equals("")) {
            entradaDTO.setCdFuncionarioBradesco("");
        } else {
            entradaDTO.setCdFuncionarioBradesco(getCodigoGerente());
        }

		entradaDTO.setCdSetorContrato(cdSetorContratoAlteracao);

		AlterarDadosBasicoContratoSaidaDTO saidaDTO = getManterContratoImpl().alterarDadosBasicoContrato(entradaDTO);
		
        BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
            "conManterContrato", BradescoViewExceptionActionType.ACTION, false);

		identificacaoClienteContratoBean.consultarContrato();

		return "";
	}
	
	/**
     * Parametro.
     * 
     * @return the string
     */
	public String parametro(){
		consultarParametros();
		return "PARAMETROCONTRATO";
	}
	
	/**
     * Consultar parametros.
     * 
     * @return the string
     */
	public String consultarParametros(){
		try {
			setListaGridPesquisaParametro(new ArrayList<ListarParametroContratoOcorrenciasSaidaDTO>());
			ListarParametroContratoEntradaDTO entrada =  new ListarParametroContratoEntradaDTO();
			entrada.setNrMaximoOcorrencias(50);
			entrada.setCdpessoaJuridicaContrato(getCdEmpresaGestoraContrato());
			entrada.setCdTipoContratoNegocio(getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNumeroContrato()));
			
			ListarParametroContratoSaidaDTO saida =  getParametroContratoServiceImpl().listarParametroContrato(entrada);
			
			setListaGridPesquisaParametro(saida.getOcorrencias());
		
			listaControleRadioParametro =  new ArrayList<SelectItem>();
			for (int i = 0; i < saida.getNrLinhas(); i++) {
				listaControleRadioParametro.add(new SelectItem(saida.getOcorrencias().get(i).getCdParametro(), ""));
			}
			
		} catch (PdcAdapterFunctionalException e) {
			if(!e.getMessage().equals("NAO EXISTEM DADOS A SEREM LISTADOS")){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") "
						+ e.getMessage(), false);
				return null;
			}
		}
		return "PARAMETROCONTRATO";
	}
	
	/**
     * Excluir parametro.
     * 
     * @return the string
     */
	public String excluirParametro(){
		
		for (int i = 0; i < getListaGridPesquisaParametro().size(); i++) {
			if(getListaGridPesquisaParametro().get(i).getCdParametro().equals(itemSelecionadoListaParamentro)){
				setParametroSelecionado(getListaGridPesquisaParametro().get(i).getDsParametro());
				setDsParametroContrato(getListaGridPesquisaParametro().get(i).getDsParametroContrato());
			}
		}
		
		return "EXCLUIR";
	}

	/**
     * Confirmar exclusao parametro.
     * 
     * @return the string
     */
	public String confirmarExclusaoParametro(){
		try{
			ExcluirParametroContratoEntradaDTO entrada = new ExcluirParametroContratoEntradaDTO();

			entrada.setCdpessoaJuridicaContrato(getCdEmpresaGestoraContrato());
			entrada.setCdTipoContratoNegocio(getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNumeroContrato()));
			entrada.setCdParametro(itemSelecionadoListaParamentro);

			ExcluirParametroContratoSaidaDTO saida = 
				getParametroContratoServiceImpl().excluirParametroContrato(entrada);


			BradescoFacesUtils.addInfoModalMessage("(" +saida.getCodMensagem() + ") " + saida.getMensagem(), 
					"CONFIRMAR", "#{manterContratoDadosBasicosBean.listarParametros}", false);

		} catch (PdcAdapterFunctionalException e) {
			if(!e.getMessage().equals("NAO EXISTEM DADOS A SEREM LISTADOS")){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") "
						+ e.getMessage(), false);
				return null;
			}
		}

		return "";

	}
	
	/**
     * Incluir parametro.
     * 
     * @return the string
     */
	public String incluirParametro(){
		carregaComboParametros();
		atribuiValoresDefaultIncluirParamentro();	
		return "INCLUIR";
	}
	
	/**
     * Avancar incluir parametro.
     * 
     * @return the string
     */
	public String avancarIncluirParametro(){
		if(getItemSelecionadoComboParametro().equals(0)){
			BradescoFacesUtils.addInfoModalMessage("O Campo Par�metro � Obrigat�rio.", false);
			return "";
		}
		
		if(getCdIndicadorUtilizaDescricao().equals(1) && getDsParametroContrato().trim().equals("")){
			BradescoFacesUtils.addInfoModalMessage("O Campo Dom�nio � Obrigat�rio.", false);
			return "";
		}

		return "AVANCAR_INCLUIR";
	}

	/**
     * Confirmar inclusao parametro.
     * 
     * @return the string
     */
	public String confirmarInclusaoParametro(){
		try{
			IncluirParametroContratoEntradaDTO entrada = new IncluirParametroContratoEntradaDTO(); 

			entrada.setCdpessoaJuridicaContrato(getCdEmpresaGestoraContrato());
			entrada.setCdTipoContratoNegocio(getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(PgitUtil.isStringNullLong(getNumeroContrato()));
			entrada.setCdParametro(itemSelecionadoComboParametro);
			entrada.setCdIndicadorUtilizaDescricao(cdIndicadorUtilizaDescricao);
			entrada.setDsParametroContrato(dsParametroContrato);

			IncluirParametroContratoSaidaDTO saida = 
				getParametroContratoServiceImpl().incluirParametroContrato(entrada);
			
			BradescoFacesUtils.addInfoModalMessage("(" +saida.getCodMensagem() + ") " + saida.getMensagem(), 
					"CONFIRMAR", "#{manterContratoDadosBasicosBean.listarParametros}", false);
			
		} catch (PdcAdapterFunctionalException e) {
			if(!e.getMessage().equals("NAO EXISTEM DADOS A SEREM LISTADOS")){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") "
						+ e.getMessage(), false);
				return null;
			}
		}

		return "";
	}
	
	/**
     * Listar parametros.
     * 
     * @param event
     *            the event
     * @return the string
     */
	public String listarParametros(ActionEvent event){
		try {
			setListaGridPesquisaParametro(new ArrayList<ListarParametroContratoOcorrenciasSaidaDTO>());
			ListarParametroContratoEntradaDTO entrada =  new ListarParametroContratoEntradaDTO();
			entrada.setNrMaximoOcorrencias(50);
			entrada.setCdpessoaJuridicaContrato(getCdEmpresaGestoraContrato());
			entrada.setCdTipoContratoNegocio(getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(Long.parseLong(getNumeroContrato()));
			
			ListarParametroContratoSaidaDTO saida =  getParametroContratoServiceImpl().listarParametroContrato(entrada);
			
			setListaGridPesquisaParametro(saida.getOcorrencias());
		
			listaControleRadioParametro =  new ArrayList<SelectItem>();
			for (int i = 0; i < saida.getNrLinhas(); i++) {
				listaControleRadioParametro.add(new SelectItem(saida.getOcorrencias().get(i).getCdParametro(), ""));
			}
			
		} catch (PdcAdapterFunctionalException e) {
			if(!e.getMessage().equals("NAO EXISTEM DADOS A SEREM LISTADOS")){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(e.getCode(), 8) + ") "
						+ e.getMessage(), false);
				return null;
			}
		}
		return "PARAMETROCONTRATO";
	}
	
	/**
     * Voltar parametro.
     * 
     * @return the string
     */
	public String voltarParametro(){
		
		return "VOLTAR";
	}

	/**
     * Carrega combo parametros.
     */
	private void carregaComboParametros(){
		setItemSelecionadoComboParametro(null);
		saidaListarParametros = getComboService().listarParametros(new ListarParametrosEntradaDTO());

		comboParametros = new ArrayList<SelectItem>();
		for (int i = 0; i < saidaListarParametros.getNrLinhas(); i++) {
			comboParametros.add(new SelectItem(saidaListarParametros.getOcorrencias().get(i).getCdParametro(), 
					saidaListarParametros.getOcorrencias().get(i).getDsParametro()));
		}

	}
	
	/**
     * Verifica obrigatoriedade dominio.
     */
	public void verificaObrigatoriedadeDominio(){
		atribuiValoresDefaultIncluirParamentro();
		for (int i = 0; i < comboParametros.size(); i++) {
			if(saidaListarParametros.getOcorrencias().get(i).getCdParametro().equals(itemSelecionadoComboParametro)){
				setCdIndicadorUtilizaDescricao(
						saidaListarParametros.getOcorrencias().get(i).getCdIndicadorUtilizaDescricao());
				setParametroSelecionado(saidaListarParametros.getOcorrencias().get(i).getDsParametro());
				break;
			}
		}
	}
	
	/**
     * Atribui valores default incluir paramentro.
     */
	private void atribuiValoresDefaultIncluirParamentro(){
		setParametroSelecionado("");
		setDsParametroContrato("");
		setCdIndicadorUtilizaDescricao(2);
	}
	/**
	 * Carrega lista idioma.
	 */
	public void carregaListaIdioma(){
		
		this.listaIdioma = new ArrayList<SelectItem>();
		List<IdiomaSaidaDTO> listaIdiomaSaida = new ArrayList<IdiomaSaidaDTO>();
		
		listaIdiomaSaida = comboService.listarIdioma();
	
		listaIdiomaHash.clear();
			
		for(IdiomaSaidaDTO combo : listaIdiomaSaida){
			listaIdiomaHash.put(combo.getCdIdioma(),combo.getDsIdioma());
			this.listaIdioma.add(new SelectItem(combo.getCdIdioma(), combo.getDsIdioma()));
		}
	}
	
	/**
	 * Carrega lista moeda.
	 */
	public void carregaListaMoeda(){
		
		this.listaMoeda = new ArrayList<SelectItem>();
		List<MoedaEconomicaSaidaDTO> listaMoedaSaida = new ArrayList<MoedaEconomicaSaidaDTO>();
		MoedaEconomicaEntradaDTO moedaEconomicaEntradaDTO = new MoedaEconomicaEntradaDTO();
		moedaEconomicaEntradaDTO.setCdSituacao(0);
		
		listaMoedaSaida = comboService.listarMoedaEconomica(moedaEconomicaEntradaDTO);
		
		listaMoedaHash.clear();
		
		for(MoedaEconomicaSaidaDTO combo : listaMoedaSaida){
			listaMoedaHash.put(combo.getCdIndicadorEconomico(), combo.getDsIndicadorEconomico());
			listaMoeda.add(new SelectItem(combo.getCdIndicadorEconomico(), combo.getDsIndicadorEconomico()));
		}
	}
	
	/**
	 * Carrega lista motivo.
	 */
	public void carregaListaMotivo(){
	
		this.listaMotivo = new ArrayList<SelectItem>();
		
		List<ListarMotivoSituacaoContratoSaidaDTO> saidaDTO = new ArrayList<ListarMotivoSituacaoContratoSaidaDTO>();
		ListarMotivoSituacaoContratoEntradaDTO entradaDTO = new ListarMotivoSituacaoContratoEntradaDTO();
		entradaDTO.setCdSituacao(getCodSituacaoContrato());
		saidaDTO = comboService.listarMotivoSituacaoContrato(entradaDTO);
		listaMotivoHash.clear();
		
		for(ListarMotivoSituacaoContratoSaidaDTO combo : saidaDTO){
			listaMotivoHash.put(combo.getCdMotivoSituacaoContrato(), combo.getDsMotivoSituacaoContrato());
			this.listaMotivo.add(new SelectItem(combo.getCdMotivoSituacaoContrato(), combo.getDsMotivoSituacaoContrato()));
		}	
	}
	
	/**
	 * Listar setor.
	 *
	 * @param numeroCombo the numero combo
	 * @return the list< consultar lista valores discretos saida dt o>
	 */
	public List<ConsultarListaValoresDiscretosSaidaDTO> listarSetor(Integer numeroCombo){
		try{
			ConsultarListaValoresDiscretosEntradaDTO setorEntradaDTO = new ConsultarListaValoresDiscretosEntradaDTO();
			setorEntradaDTO.setNumeroCombo(numeroCombo);
			
			return getComboService().consultarListaValoresDiscretos(setorEntradaDTO);
		}catch (PdcAdapterFunctionalException p) {			
			return new ArrayList<ConsultarListaValoresDiscretosSaidaDTO>();
		}
	}
	
	/**
	 * Carrega lista setor.
	 */
	public void carregaListaSetor(){
		setListaSetor(new ArrayList<SelectItem>());
		listaSetorHash.clear();
		List<ConsultarListaValoresDiscretosSaidaDTO> lista = listarSetor(1);
		
		for(ConsultarListaValoresDiscretosSaidaDTO combo : lista){
			getListaSetor().add(new SelectItem(combo.getCdCombo().intValue(), combo.getDsCombo()));
			listaSetorHash.put(combo.getCdCombo().intValue(), combo.getDsCombo());
		}		
	}

	/**
     * Carrega radio forma autorizacao.
     */
	private void carregaRadioFormaAutorizacao() {
        listaOpcoesRadioFormaAutorizacao = new ArrayList<SelectItem>();
        listaOpcoesRadioFormaAutorizacao.add(new SelectItem(1, MessageHelperUtils
            .getI18nMessage("label_net_empresa_desautorizado")));
        listaOpcoesRadioFormaAutorizacao.add(new SelectItem(3, MessageHelperUtils
            .getI18nMessage("label_arquivo_remessa_net_empresa__autorizado_desautorizado")));
	}

	/**
     * Is radio forma autorizacao desabilitado.
     * 
     * @return true, if is radio forma autorizacao desabilitado
     */
	public boolean isRadioFormaAutorizacaoDesabilitado() {
        return saidaVerificaDadosBasico == null
            || saidaVerificaDadosBasico.getCdFormaAutorizacaoPagamentoAlterar() == null
            || !saidaVerificaDadosBasico.getCdFormaAutorizacaoPagamentoAlterar().equals(Integer.valueOf(1));
	}

	//Set's e Get's
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}
	
	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: atividadeEconomicaClienteMaster.
	 *
	 * @return atividadeEconomicaClienteMaster
	 */
	public String getAtividadeEconomicaClienteMaster() {
		return atividadeEconomicaClienteMaster;
	}

	/**
	 * Set: atividadeEconomicaClienteMaster.
	 *
	 * @param atividadeEconomicaClienteMaster the atividade economica cliente master
	 */
	public void setAtividadeEconomicaClienteMaster(
			String atividadeEconomicaClienteMaster) {
		this.atividadeEconomicaClienteMaster = atividadeEconomicaClienteMaster;
	}

	/**
	 * Get: cpfCnpjClienteMaster.
	 *
	 * @return cpfCnpjClienteMaster
	 */
	public String getCpfCnpjClienteMaster() {
		return cpfCnpjClienteMaster;
	}

	/**
	 * Set: cpfCnpjClienteMaster.
	 *
	 * @param cpfCnpjClienteMaster the cpf cnpj cliente master
	 */
	public void setCpfCnpjClienteMaster(String cpfCnpjClienteMaster) {
		this.cpfCnpjClienteMaster = cpfCnpjClienteMaster;
	}

	/**
	 * Get: grupoEconomicoClienteMaster.
	 *
	 * @return grupoEconomicoClienteMaster
	 */
	public String getGrupoEconomicoClienteMaster() {
		return grupoEconomicoClienteMaster;
	}

	/**
	 * Set: grupoEconomicoClienteMaster.
	 *
	 * @param grupoEconomicoClienteMaster the grupo economico cliente master
	 */
	public void setGrupoEconomicoClienteMaster(String grupoEconomicoClienteMaster) {
		this.grupoEconomicoClienteMaster = grupoEconomicoClienteMaster;
	}

	/**
	 * Get: nomeRazaoClienteMaster.
	 *
	 * @return nomeRazaoClienteMaster
	 */
	public String getNomeRazaoClienteMaster() {
		return nomeRazaoClienteMaster;
	}

	/**
	 * Set: nomeRazaoClienteMaster.
	 *
	 * @param nomeRazaoClienteMaster the nome razao cliente master
	 */
	public void setNomeRazaoClienteMaster(String nomeRazaoClienteMaster) {
		this.nomeRazaoClienteMaster = nomeRazaoClienteMaster;
	}

	/**
	 * Get: segmentoClienteMaster.
	 *
	 * @return segmentoClienteMaster
	 */
	public String getSegmentoClienteMaster() {
		return segmentoClienteMaster;
	}

	/**
	 * Set: segmentoClienteMaster.
	 *
	 * @param segmentoClienteMaster the segmento cliente master
	 */
	public void setSegmentoClienteMaster(String segmentoClienteMaster) {
		this.segmentoClienteMaster = segmentoClienteMaster;
	}

	/**
	 * Get: subSegmentoClienteMaster.
	 *
	 * @return subSegmentoClienteMaster
	 */
	public String getSubSegmentoClienteMaster() {
		return subSegmentoClienteMaster;
	}

	/**
	 * Set: subSegmentoClienteMaster.
	 *
	 * @param subSegmentoClienteMaster the sub segmento cliente master
	 */
	public void setSubSegmentoClienteMaster(String subSegmentoClienteMaster) {
		this.subSegmentoClienteMaster = subSegmentoClienteMaster;
	}

	/**
	 * Get: dataHoraAssinaturaContrato.
	 *
	 * @return dataHoraAssinaturaContrato
	 */
	public String getDataHoraAssinaturaContrato() {
		return dataHoraAssinaturaContrato;
	}

	/**
	 * Set: dataHoraAssinaturaContrato.
	 *
	 * @param dataHoraAssinaturaContrato the data hora assinatura contrato
	 */
	public void setDataHoraAssinaturaContrato(String dataHoraAssinaturaContrato) {
		this.dataHoraAssinaturaContrato = dataHoraAssinaturaContrato;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: empresaGestoraContrato.
	 *
	 * @return empresaGestoraContrato
	 */
	public String getEmpresaGestoraContrato() {
		return empresaGestoraContrato;
	}

	/**
	 * Set: empresaGestoraContrato.
	 *
	 * @param empresaGestoraContrato the empresa gestora contrato
	 */
	public void setEmpresaGestoraContrato(String empresaGestoraContrato) {
		this.empresaGestoraContrato = empresaGestoraContrato;
	}

	/**
	 * Get: idiomaContrato.
	 *
	 * @return idiomaContrato
	 */
	public String getIdiomaContrato() {
		return idiomaContrato;
	}

	/**
	 * Set: idiomaContrato.
	 *
	 * @param idiomaContrato the idioma contrato
	 */
	public void setIdiomaContrato(String idiomaContrato) {
		this.idiomaContrato = idiomaContrato;
	}

	/**
	 * Get: dsGerente.
	 *
	 * @return dsGerente
	 */
	public String getDsGerente() {
		return dsGerente;
	}

	/**
	 * Set: dsGerente.
	 *
	 * @param dsGerente the ds gerente
	 */
	public void setDsGerente(String dsGerente) {
		this.dsGerente = dsGerente;
	}

	/**
	 * Get: inicioVigenciaContrato.
	 *
	 * @return inicioVigenciaContrato
	 */
	public String getInicioVigenciaContrato() {
		return inicioVigenciaContrato;
	}

	/**
	 * Set: inicioVigenciaContrato.
	 *
	 * @param inicioVigenciaContrato the inicio vigencia contrato
	 */
	public void setInicioVigenciaContrato(String inicioVigenciaContrato) {
		this.inicioVigenciaContrato = inicioVigenciaContrato;
	}

	/**
	 * Get: moedaContrato.
	 *
	 * @return moedaContrato
	 */
	public String getMoedaContrato() {
		return moedaContrato;
	}

	/**
	 * Set: moedaContrato.
	 *
	 * @param moedaContrato the moeda contrato
	 */
	public void setMoedaContrato(String moedaContrato) {
		this.moedaContrato = moedaContrato;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: numeroComercialContrato.
	 *
	 * @return numeroComercialContrato
	 */
	public String getNumeroComercialContrato() {
		return numeroComercialContrato;
	}

	/**
	 * Set: numeroComercialContrato.
	 *
	 * @param numeroComercialContrato the numero comercial contrato
	 */
	public void setNumeroComercialContrato(String numeroComercialContrato) {
		this.numeroComercialContrato = numeroComercialContrato;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: origemContrato.
	 *
	 * @return origemContrato
	 */
	public String getOrigemContrato() {
		return origemContrato;
	}

	/**
	 * Set: origemContrato.
	 *
	 * @param origemContrato the origem contrato
	 */
	public void setOrigemContrato(String origemContrato) {
		this.origemContrato = origemContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: possuiAditivosContrato.
	 *
	 * @return possuiAditivosContrato
	 */
	public String getPossuiAditivosContrato() {
		return possuiAditivosContrato;
	}

	/**
	 * Set: possuiAditivosContrato.
	 *
	 * @param possuiAditivosContrato the possui aditivos contrato
	 */
	public void setPossuiAditivosContrato(String possuiAditivosContrato) {
		this.possuiAditivosContrato = possuiAditivosContrato;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: gerenteResponsavelUnidOrg.
	 *
	 * @return gerenteResponsavelUnidOrg
	 */
	public String getGerenteResponsavelUnidOrg() {
		return gerenteResponsavelUnidOrg;
	}

	/**
	 * Set: gerenteResponsavelUnidOrg.
	 *
	 * @param gerenteResponsavelUnidOrg the gerente responsavel unid org
	 */
	public void setGerenteResponsavelUnidOrg(String gerenteResponsavelUnidOrg) {
		this.gerenteResponsavelUnidOrg = gerenteResponsavelUnidOrg;
	}

	/**
	 * Get: gestoraUnidOrg.
	 *
	 * @return gestoraUnidOrg
	 */
	public String getGestoraUnidOrg() {
		return gestoraUnidOrg;
	}

	/**
	 * Set: gestoraUnidOrg.
	 *
	 * @param gestoraUnidOrg the gestora unid org
	 */
	public void setGestoraUnidOrg(String gestoraUnidOrg) {
		this.gestoraUnidOrg = gestoraUnidOrg;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}
	
	/**
	 * Get: descricaoDadosBasicos.
	 *
	 * @return descricaoDadosBasicos
	 */
	public String getDescricaoDadosBasicos() {
		return descricaoDadosBasicos;
	}

	/**
	 * Set: descricaoDadosBasicos.
	 *
	 * @param descricaoDadosBasicos the descricao dados basicos
	 */
	public void setDescricaoDadosBasicos(String descricaoDadosBasicos) {
		this.descricaoDadosBasicos = descricaoDadosBasicos;
	}
	
	/**
	 * Get: listaIdioma.
	 *
	 * @return listaIdioma
	 */
	public List<SelectItem> getListaIdioma() {
		return listaIdioma;
	}

	/**
	 * Set: listaIdioma.
	 *
	 * @param listaIdioma the lista idioma
	 */
	public void setListaIdioma(List<SelectItem> listaIdioma) {
		this.listaIdioma = listaIdioma;
	}

	/**
	 * Get: listaIdiomaHash.
	 *
	 * @return listaIdiomaHash
	 */
	public Map<Integer, String> getListaIdiomaHash() {
		return listaIdiomaHash;
	}

	/**
	 * Set lista idioma hash.
	 *
	 * @param listaIdiomaHash the lista idioma hash
	 */
	public void setListaIdiomaHash(Map<Integer, String> listaIdiomaHash) {
		this.listaIdiomaHash = listaIdiomaHash;
	}

	/**
	 * Get: codIdioma.
	 *
	 * @return codIdioma
	 */
	public Integer getCodIdioma() {
		return codIdioma;
	}

	/**
	 * Set: codIdioma.
	 *
	 * @param codIdioma the cod idioma
	 */
	public void setCodIdioma(Integer codIdioma) {
		this.codIdioma = codIdioma;
	}

	/**
	 * Get: codMoeda.
	 *
	 * @return codMoeda
	 */
	public Integer getCodMoeda() {
		return codMoeda;
	}

	/**
	 * Set: codMoeda.
	 *
	 * @param codMoeda the cod moeda
	 */
	public void setCodMoeda(Integer codMoeda) {
		this.codMoeda = codMoeda;
	}

	/**
	 * Get: listaMoeda.
	 *
	 * @return listaMoeda
	 */
	public List<SelectItem> getListaMoeda() {
		return listaMoeda;
	}

	/**
	 * Set: listaMoeda.
	 *
	 * @param listaMoeda the lista moeda
	 */
	public void setListaMoeda(List<SelectItem> listaMoeda) {
		this.listaMoeda = listaMoeda;
	}
	
	/**
	 * Get: listaMoedaHash.
	 *
	 * @return listaMoedaHash
	 */
	Map<Integer, String> getListaMoedaHash() {
		return listaMoedaHash;
	}

	/**
	 * Set lista moeda hash.
	 *
	 * @param listaMoedaHash the lista moeda hash
	 */
	void setListaMoedaHash(Map<Integer, String> listaMoedaHash) {
		this.listaMoedaHash = listaMoedaHash;
	}

	/**
	 * Get: codigoGerente.
	 *
	 * @return codigoGerente
	 */
	public String getCodigoGerente() {
		return codigoGerente;
	}

	/**
	 * Set: codigoGerente.
	 *
	 * @param codigoGerente the codigo gerente
	 */
	public void setCodigoGerente(String codigoGerente) {
		this.codigoGerente = codigoGerente;
	}
	
	/**
	 * Get: nomeGerente.
	 *
	 * @return nomeGerente
	 */
	public String getNomeGerente() {
		return nomeGerente;
	}

	/**
	 * Set: nomeGerente.
	 *
	 * @param nomeGerente the nome gerente
	 */
	public void setNomeGerente(String nomeGerente) {
		this.nomeGerente = nomeGerente;
	}
	
	/**
	 * Get: dsIdioma.
	 *
	 * @return dsIdioma
	 */
	public String getDsIdioma() {
		return dsIdioma;
	}

	/**
	 * Set: dsIdioma.
	 *
	 * @param dsIdioma the ds idioma
	 */
	public void setDsIdioma(String dsIdioma) {
		this.dsIdioma = dsIdioma;
	}

	/**
	 * Get: dsMoeda.
	 *
	 * @return dsMoeda
	 */
	public String getDsMoeda() {
		return dsMoeda;
	}

	/**
	 * Set: dsMoeda.
	 *
	 * @param dsMoeda the ds moeda
	 */
	public void setDsMoeda(String dsMoeda) {
		this.dsMoeda = dsMoeda;
	}
	
	/**
	 * Get: gerenteDTO.
	 *
	 * @return gerenteDTO
	 */
	public ListarFuncionarioSaidaDTO getGerenteDTO() {
		return gerenteDTO;
	}

	/**
	 * Set: gerenteDTO.
	 *
	 * @param gerenteDTO the gerente dto
	 */
	public void setGerenteDTO(ListarFuncionarioSaidaDTO gerenteDTO) {
		this.gerenteDTO = gerenteDTO;
	}
	
	/**
	 * Get: listarGerenteBradescoService.
	 *
	 * @return listarGerenteBradescoService
	 */
	public IListarFuncBradescoService getListarGerenteBradescoService() {
		return listarGerenteBradescoService;
	}

	/**
	 * Set: listarGerenteBradescoService.
	 *
	 * @param listarGerenteBradescoService the listar gerente bradesco service
	 */
	public void setListarGerenteBradescoService(
			IListarFuncBradescoService listarGerenteBradescoService) {
		this.listarGerenteBradescoService = listarGerenteBradescoService;
	}
	
	/**
	 * Get: listaMotivo.
	 *
	 * @return listaMotivo
	 */
	public List<SelectItem> getListaMotivo() {
		return listaMotivo;
	}

	/**
	 * Set: listaMotivo.
	 *
	 * @param listaMotivo the lista motivo
	 */
	public void setListaMotivo(List<SelectItem> listaMotivo) {
		this.listaMotivo = listaMotivo;
	}
	
	/**
	 * Get: listaMotivoHash.
	 *
	 * @return listaMotivoHash
	 */
	public Map<Integer, String> getListaMotivoHash() {
		return listaMotivoHash;
	}

	/**
	 * Set lista motivo hash.
	 *
	 * @param listaMotivoHash the lista motivo hash
	 */
	public void setListaMotivoHash(Map<Integer, String> listaMotivoHash) {
		this.listaMotivoHash = listaMotivoHash;
	}

	/**
	 * Get: codMotivoSituacao.
	 *
	 * @return codMotivoSituacao
	 */
	public Integer getCodMotivoSituacao() {
		return codMotivoSituacao;
	}

	/**
	 * Set: codMotivoSituacao.
	 *
	 * @param codMotivoSituacao the cod motivo situacao
	 */
	public void setCodMotivoSituacao(Integer codMotivoSituacao) {
		this.codMotivoSituacao = codMotivoSituacao;
	}
	
	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}
	
	/**
	 * Get: codSituacaoContrato.
	 *
	 * @return codSituacaoContrato
	 */
	public Integer getCodSituacaoContrato() {
		this.codSituacaoContrato = 6;
		return codSituacaoContrato;
	}

	/**
	 * Set: codSituacaoContrato.
	 *
	 * @param codSituacaoContrato the cod situacao contrato
	 */
	public void setCodSituacaoContrato(Integer codSituacaoContrato) {
		this.codSituacaoContrato = codSituacaoContrato;
	}
	
	/**
	 * Get: cdGrupoEconomicoClienteMaster.
	 *
	 * @return cdGrupoEconomicoClienteMaster
	 */
	public Long getCdGrupoEconomicoClienteMaster() {
		return cdGrupoEconomicoClienteMaster;
	}

	/**
	 * Set: cdGrupoEconomicoClienteMaster.
	 *
	 * @param cdGrupoEconomicoClienteMaster the cd grupo economico cliente master
	 */
	public void setCdGrupoEconomicoClienteMaster(Long cdGrupoEconomicoClienteMaster) {
		this.cdGrupoEconomicoClienteMaster = cdGrupoEconomicoClienteMaster;
	}

	/**
	 * Get: cdAtividadeEconomicaClienteMaster.
	 *
	 * @return cdAtividadeEconomicaClienteMaster
	 */
	public int getCdAtividadeEconomicaClienteMaster() {
		return cdAtividadeEconomicaClienteMaster;
	}

	/**
	 * Set: cdAtividadeEconomicaClienteMaster.
	 *
	 * @param cdAtividadeEconomicaClienteMaster the cd atividade economica cliente master
	 */
	public void setCdAtividadeEconomicaClienteMaster(
			int cdAtividadeEconomicaClienteMaster) {
		this.cdAtividadeEconomicaClienteMaster = cdAtividadeEconomicaClienteMaster;
	}

	/**
	 * Get: cdSegmentoClienteMaster.
	 *
	 * @return cdSegmentoClienteMaster
	 */
	public int getCdSegmentoClienteMaster() {
		return cdSegmentoClienteMaster;
	}

	/**
	 * Set: cdSegmentoClienteMaster.
	 *
	 * @param cdSegmentoClienteMaster the cd segmento cliente master
	 */
	public void setCdSegmentoClienteMaster(int cdSegmentoClienteMaster) {
		this.cdSegmentoClienteMaster = cdSegmentoClienteMaster;
	}

	/**
	 * Get: cdSubSegmentoClienteMaster.
	 *
	 * @return cdSubSegmentoClienteMaster
	 */
	public int getCdSubSegmentoClienteMaster() {
		return cdSubSegmentoClienteMaster;
	}

	/**
	 * Set: cdSubSegmentoClienteMaster.
	 *
	 * @param cdSubSegmentoClienteMaster the cd sub segmento cliente master
	 */
	public void setCdSubSegmentoClienteMaster(int cdSubSegmentoClienteMaster) {
		this.cdSubSegmentoClienteMaster = cdSubSegmentoClienteMaster;
	}

	/**
	 * Get: cdIdiomaContrato.
	 *
	 * @return cdIdiomaContrato
	 */
	public int getCdIdiomaContrato() {
		return cdIdiomaContrato;
	}

	/**
	 * Set: cdIdiomaContrato.
	 *
	 * @param cdIdiomaContrato the cd idioma contrato
	 */
	public void setCdIdiomaContrato(int cdIdiomaContrato) {
		this.cdIdiomaContrato = cdIdiomaContrato;
	}

	/**
	 * Get: cdMoedaContrato.
	 *
	 * @return cdMoedaContrato
	 */
	public int getCdMoedaContrato() {
		return cdMoedaContrato;
	}

	/**
	 * Set: cdMoedaContrato.
	 *
	 * @param cdMoedaContrato the cd moeda contrato
	 */
	public void setCdMoedaContrato(int cdMoedaContrato) {
		this.cdMoedaContrato = cdMoedaContrato;
	}

	/**
	 * Get: cdOrigemContrato.
	 *
	 * @return cdOrigemContrato
	 */
	public int getCdOrigemContrato() {
		return cdOrigemContrato;
	}

	/**
	 * Set: cdOrigemContrato.
	 *
	 * @param cdOrigemContrato the cd origem contrato
	 */
	public void setCdOrigemContrato(int cdOrigemContrato) {
		this.cdOrigemContrato = cdOrigemContrato;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public int getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(int cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdPermiteEncerramentoContrato.
	 *
	 * @return cdPermiteEncerramentoContrato
	 */
	public int getCdPermiteEncerramentoContrato() {
		return cdPermiteEncerramentoContrato;
	}

	/**
	 * Set: cdPermiteEncerramentoContrato.
	 *
	 * @param cdPermiteEncerramentoContrato the cd permite encerramento contrato
	 */
	public void setCdPermiteEncerramentoContrato(int cdPermiteEncerramentoContrato) {
		this.cdPermiteEncerramentoContrato = cdPermiteEncerramentoContrato;
	}

	/**
	 * Get: cdContabilUnidOrg.
	 *
	 * @return cdContabilUnidOrg
	 */
	public int getCdContabilUnidOrg() {
		return cdContabilUnidOrg;
	}

	/**
	 * Set: cdContabilUnidOrg.
	 *
	 * @param cdContabilUnidOrg the cd contabil unid org
	 */
	public void setCdContabilUnidOrg(int cdContabilUnidOrg) {
		this.cdContabilUnidOrg = cdContabilUnidOrg;
	}

	/**
	 * Get: cdGestoraUnidOrg.
	 *
	 * @return cdGestoraUnidOrg
	 */
	public int getCdGestoraUnidOrg() {
		return cdGestoraUnidOrg;
	}

	/**
	 * Set: cdGestoraUnidOrg.
	 *
	 * @param cdGestoraUnidOrg the cd gestora unid org
	 */
	public void setCdGestoraUnidOrg(int cdGestoraUnidOrg) {
		this.cdGestoraUnidOrg = cdGestoraUnidOrg;
	}

	/**
	 * Get: cdOperadoraUnidOrg.
	 *
	 * @return cdOperadoraUnidOrg
	 */
	public int getCdOperadoraUnidOrg() {
		return cdOperadoraUnidOrg;
	}

	/**
	 * Set: cdOperadoraUnidOrg.
	 *
	 * @param cdOperadoraUnidOrg the cd operadora unid org
	 */
	public void setCdOperadoraUnidOrg(int cdOperadoraUnidOrg) {
		this.cdOperadoraUnidOrg = cdOperadoraUnidOrg;
	}

	/**
	 * Get: cdGerenteResponsavelUnidOrg.
	 *
	 * @return cdGerenteResponsavelUnidOrg
	 */
	public Long getCdGerenteResponsavelUnidOrg() {
		return cdGerenteResponsavelUnidOrg;
	}

	/**
	 * Set: cdGerenteResponsavelUnidOrg.
	 *
	 * @param cdGerenteResponsavelUnidOrg the cd gerente responsavel unid org
	 */
	public void setCdGerenteResponsavelUnidOrg(Long cdGerenteResponsavelUnidOrg) {
		this.cdGerenteResponsavelUnidOrg = cdGerenteResponsavelUnidOrg;
	}

	/**
	 * Get: cdEmpresaGestoraContrato.
	 *
	 * @return cdEmpresaGestoraContrato
	 */
	public Long getCdEmpresaGestoraContrato() {
		return cdEmpresaGestoraContrato;
	}

	/**
	 * Set: cdEmpresaGestoraContrato.
	 *
	 * @param cdEmpresaGestoraContrato the cd empresa gestora contrato
	 */
	public void setCdEmpresaGestoraContrato(Long cdEmpresaGestoraContrato) {
		this.cdEmpresaGestoraContrato = cdEmpresaGestoraContrato;
	}
	
	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public int getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(int cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: cdMotivoContrato.
	 *
	 * @return cdMotivoContrato
	 */
	public int getCdMotivoContrato() {
		return cdMotivoContrato;
	}

	/**
	 * Set: cdMotivoContrato.
	 *
	 * @param cdMotivoContrato the cd motivo contrato
	 */
	public void setCdMotivoContrato(int cdMotivoContrato) {
		this.cdMotivoContrato = cdMotivoContrato;
	}

	/**
	 * Get: numeroContratoUnidOrg.
	 *
	 * @return numeroContratoUnidOrg
	 */
	public int getNumeroContratoUnidOrg() {
		return numeroContratoUnidOrg;
	}

	/**
	 * Set: numeroContratoUnidOrg.
	 *
	 * @param numeroContratoUnidOrg the numero contrato unid org
	 */
	public void setNumeroContratoUnidOrg(int numeroContratoUnidOrg) {
		this.numeroContratoUnidOrg = numeroContratoUnidOrg;
	}

	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public Long getCdClub() {
		return cdClub;
	}

	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}
	
	/**
	 * Get: numeroSequencialUnidadeOrganizacional.
	 *
	 * @return numeroSequencialUnidadeOrganizacional
	 */
	public int getNumeroSequencialUnidadeOrganizacional() {
		return numeroSequencialUnidadeOrganizacional;
	}

	/**
	 * Set: numeroSequencialUnidadeOrganizacional.
	 *
	 * @param numeroSequencialUnidadeOrganizacional the numero sequencial unidade organizacional
	 */
	public void setNumeroSequencialUnidadeOrganizacional(
			int numeroSequencialUnidadeOrganizacional) {
		this.numeroSequencialUnidadeOrganizacional = numeroSequencialUnidadeOrganizacional;
	}

	/**
	 * Get: codPessoaJuridica.
	 *
	 * @return codPessoaJuridica
	 */
	public Long getCodPessoaJuridica() {
		return codPessoaJuridica;
	}

	/**
	 * Set: codPessoaJuridica.
	 *
	 * @param codPessoaJuridica the cod pessoa juridica
	 */
	public void setCodPessoaJuridica(Long codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}
	
	/**
	 * Get: fimVigenciaContrato.
	 *
	 * @return fimVigenciaContrato
	 */
	public String getFimVigenciaContrato() {
		return fimVigenciaContrato;
	}

	/**
	 * Set: fimVigenciaContrato.
	 *
	 * @param fimVigenciaContrato the fim vigencia contrato
	 */
	public void setFimVigenciaContrato(String fimVigenciaContrato) {
		this.fimVigenciaContrato = fimVigenciaContrato;
	}
	
	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}	

	/**
	 * Get: historicoBean.
	 *
	 * @return historicoBean
	 */
	public HistoricoBean getHistoricoBean() {
		return historicoBean;
	}

	/**
	 * Set: historicoBean.
	 *
	 * @param historicoBean the historico bean
	 */
	public void setHistoricoBean(HistoricoBean historicoBean) {
		this.historicoBean = historicoBean;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: dsNomeRazaoSocialParticipante.
	 *
	 * @return dsNomeRazaoSocialParticipante
	 */
	public String getDsNomeRazaoSocialParticipante() {
		return dsNomeRazaoSocialParticipante;
	}

	/**
	 * Set: dsNomeRazaoSocialParticipante.
	 *
	 * @param dsNomeRazaoSocialParticipante the ds nome razao social participante
	 */
	public void setDsNomeRazaoSocialParticipante(
			String dsNomeRazaoSocialParticipante) {
		this.dsNomeRazaoSocialParticipante = dsNomeRazaoSocialParticipante;
	}

	/**
	 * Get: dsSetorContrato.
	 *
	 * @return dsSetorContrato
	 */
	public String getDsSetorContrato() {
		return dsSetorContrato;
	}

	/**
	 * Set: dsSetorContrato.
	 *
	 * @param dsSetorContrato the ds setor contrato
	 */
	public void setDsSetorContrato(String dsSetorContrato) {
		this.dsSetorContrato = dsSetorContrato;
	}

	/**
	 * Get: listaSetor.
	 *
	 * @return listaSetor
	 */
	public List<SelectItem> getListaSetor() {
		return listaSetor;
	}

	/**
	 * Set: listaSetor.
	 *
	 * @param listaSetor the lista setor
	 */
	public void setListaSetor(List<SelectItem> listaSetor) {
		this.listaSetor = listaSetor;
	}

	/**
	 * Get: dsSetorContratoFiltro.
	 *
	 * @return dsSetorContratoFiltro
	 */
	public String getDsSetorContratoFiltro() {
		return dsSetorContratoFiltro;
	}

	/**
	 * Set: dsSetorContratoFiltro.
	 *
	 * @param dsSetorContratoFiltro the ds setor contrato filtro
	 */
	public void setDsSetorContratoFiltro(String dsSetorContratoFiltro) {
		this.dsSetorContratoFiltro = dsSetorContratoFiltro;
	}

	/**
	 * Get: empresaGestoraProposta.
	 *
	 * @return empresaGestoraProposta
	 */
	public String getEmpresaGestoraProposta() {
	    return empresaGestoraProposta;
	}

	/**
	 * Set: empresaGestoraProposta.
	 *
	 * @param empresaGestoraProposta the empresa gestora proposta
	 */
	public void setEmpresaGestoraProposta(String empresaGestoraProposta) {
	    this.empresaGestoraProposta = empresaGestoraProposta;
	}

	/**
	 * Get: numeroProposta.
	 *
	 * @return numeroProposta
	 */
	public Long getNumeroProposta() {
	    return numeroProposta;
	}

	/**
	 * Set: numeroProposta.
	 *
	 * @param numeroProposta the numero proposta
	 */
	public void setNumeroProposta(Long numeroProposta) {
	    this.numeroProposta = numeroProposta;
	}

	/**
	 * Get: tipoProposta.
	 *
	 * @return tipoProposta
	 */
	public String getTipoProposta() {
	    return tipoProposta;
	}

	/**
	 * Set: tipoProposta.
	 *
	 * @param tipoProposta the tipo proposta
	 */
	public void setTipoProposta(String tipoProposta) {
	    this.tipoProposta = tipoProposta;
	}

	/**
	 * Get: vlSaldoVirtualTeste.
	 *
	 * @return vlSaldoVirtualTeste
	 */
	public BigDecimal getVlSaldoVirtualTeste() {
		return vlSaldoVirtualTeste;
	}

	/**
	 * Set: vlSaldoVirtualTeste.
	 *
	 * @param vlSaldoVirtualTeste the vl saldo virtual teste
	 */
	public void setVlSaldoVirtualTeste(BigDecimal vlSaldoVirtualTeste) {
		this.vlSaldoVirtualTeste = vlSaldoVirtualTeste;
	}

	/**
	 * Set: cdDeptoGestor.
	 *
	 * @param cdDeptoGestor the cd depto gestor
	 */
	public void setCdDeptoGestor(Integer cdDeptoGestor) {
		this.cdDeptoGestor = cdDeptoGestor;
	}

	/**
	 * Get: cdDeptoGestor.
	 *
	 * @return cdDeptoGestor
	 */
	public Integer getCdDeptoGestor() {
		return cdDeptoGestor;
	}

	/**
	 * Set: nmDepto.
	 *
	 * @param nmDepto the nm depto
	 */
	public void setNmDepto(String nmDepto) {
		this.nmDepto = nmDepto;
	}

	/**
	 * Get: nmDepto.
	 *
	 * @return nmDepto
	 */
	public String getNmDepto() {
		return nmDepto;
	}

	/**
	 * @param nrSequenciaContratoOutros the nrSequenciaContratoOutros to set
	 */
	public void setNrSequenciaContratoOutros(Long nrSequenciaContratoOutros) {
		this.nrSequenciaContratoOutros = nrSequenciaContratoOutros;
	}

	/**
	 * @return the nrSequenciaContratoOutros
	 */
	public Long getNrSequenciaContratoOutros() {
		return nrSequenciaContratoOutros;
	}

	/**
	 * @param nrSequenciaContratoPagamentoSalario the nrSequenciaContratoPagamentoSalario to set
	 */
	public void setNrSequenciaContratoPagamentoSalario(
			Long nrSequenciaContratoPagamentoSalario) {
		this.nrSequenciaContratoPagamentoSalario = nrSequenciaContratoPagamentoSalario;
	}

	/**
	 * @return the nrSequenciaContratoPagamentoSalario
	 */
	public Long getNrSequenciaContratoPagamentoSalario() {
		return nrSequenciaContratoPagamentoSalario;
	}

	/**
	 * @param parametroContratoServiceImpl the parametroContratoServiceImpl to set
	 */
	public void setParametroContratoServiceImpl(
			IParametroContratoService parametroContratoServiceImpl) {
		this.parametroContratoServiceImpl = parametroContratoServiceImpl;
	}

	/**
	 * @return the parametroContratoServiceImpl
	 */
	public IParametroContratoService getParametroContratoServiceImpl() {
		return parametroContratoServiceImpl;
	}

	/**
     * Get: itemSelecionadoListaParamentro.
     * 
     * @return itemSelecionadoListaParamentro
     */
	public Integer getItemSelecionadoListaParamentro() {
		return itemSelecionadoListaParamentro;
	}

	/**
     * Set: itemSelecionadoListaParamentro.
     * 
     * @param itemSelecionadoListaParamentro
     *            the item selecionado lista paramentro
     */
	public void setItemSelecionadoListaParamentro(
			Integer itemSelecionadoListaParamentro) {
		this.itemSelecionadoListaParamentro = itemSelecionadoListaParamentro;
	}

	/**
     * Get: listaControleRadioParametro.
     * 
     * @return listaControleRadioParametro
     */
	public List<SelectItem> getListaControleRadioParametro() {
		return listaControleRadioParametro;
	}

	/**
     * Set: listaControleRadioParametro.
     * 
     * @param listaControleRadioParametro
     *            the lista controle radio parametro
     */
	public void setListaControleRadioParametro(
			List<SelectItem> listaControleRadioParametro) {
		this.listaControleRadioParametro = listaControleRadioParametro;
	}

	/**
	 * @param listaGridPesquisaParametro the listaGridPesquisaParametro to set
	 */
	public void setListaGridPesquisaParametro(
			List<ListarParametroContratoOcorrenciasSaidaDTO> listaGridPesquisaParametro) {
		this.listaGridPesquisaParametro = listaGridPesquisaParametro;
	}

	/**
	 * @return the listaGridPesquisaParametro
	 */
	public List<ListarParametroContratoOcorrenciasSaidaDTO> getListaGridPesquisaParametro() {
		return listaGridPesquisaParametro;
	}

	/**
     * Get: saidaListarParametros.
     * 
     * @return saidaListarParametros
     */
	public ListarParametrosSaidaDTO getSaidaListarParametros() {
		return saidaListarParametros;
	}

	/**
     * Set: saidaListarParametros.
     * 
     * @param saidaListarParametros
     *            the saida listar parametros
     */
	public void setSaidaListarParametros(
			ListarParametrosSaidaDTO saidaListarParametros) {
		this.saidaListarParametros = saidaListarParametros;
	}

	/**
	 * @param comboParametros the comboParametros to set
	 */
	public void setComboParametros(List<SelectItem> comboParametros) {
		this.comboParametros = comboParametros;
	}

	/**
	 * @return the comboParametros
	 */
	public List<SelectItem> getComboParametros() {
		return comboParametros;
	}

	/**
     * Get: itemSelecionadoComboParametro.
     * 
     * @return itemSelecionadoComboParametro
     */
	public Integer getItemSelecionadoComboParametro() {
		return itemSelecionadoComboParametro;
	}

	/**
     * Set: itemSelecionadoComboParametro.
     * 
     * @param itemSelecionadoComboParametro
     *            the item selecionado combo parametro
     */
	public void setItemSelecionadoComboParametro(
			Integer itemSelecionadoComboParametro) {
		this.itemSelecionadoComboParametro = itemSelecionadoComboParametro;
	}

	/**
     * Get: cdIndicadorUtilizaDescricao.
     * 
     * @return cdIndicadorUtilizaDescricao
     */
	public Integer getCdIndicadorUtilizaDescricao() {
		return cdIndicadorUtilizaDescricao;
	}

	/**
     * Set: cdIndicadorUtilizaDescricao.
     * 
     * @param cdIndicadorUtilizaDescricao
     *            the cd indicador utiliza descricao
     */
	public void setCdIndicadorUtilizaDescricao(Integer cdIndicadorUtilizaDescricao) {
		this.cdIndicadorUtilizaDescricao = cdIndicadorUtilizaDescricao;
	}

	/**
     * Get: dsParametroContrato.
     * 
     * @return dsParametroContrato
     */
	public String getDsParametroContrato() {
		return dsParametroContrato;
	}

	/**
     * Set: dsParametroContrato.
     * 
     * @param dsParametroContrato
     *            the ds parametro contrato
     */
	public void setDsParametroContrato(String dsParametroContrato) {
		this.dsParametroContrato = dsParametroContrato;
	}

	/**
     * Get: parametroSelecionado.
     * 
     * @return parametroSelecionado
     */
	public String getParametroSelecionado() {
		return parametroSelecionado;
	}

	/**
     * Set: parametroSelecionado.
     * 
     * @param parametroSelecionado
     *            the parametro selecionado
     */
	public void setParametroSelecionado(String parametroSelecionado) {
		this.parametroSelecionado = parametroSelecionado;
	}

    /**
     * Nome: getDsFormaAutorizacaoPagamento
     *
     * @return dsFormaAutorizacaoPagamento
     */
    public String getDsFormaAutorizacaoPagamento() {
        return dsFormaAutorizacaoPagamento;
    }

    /**
     * Nome: setDsFormaAutorizacaoPagamento
     *
     * @param dsFormaAutorizacaoPagamento
     */
    public void setDsFormaAutorizacaoPagamento(String dsFormaAutorizacaoPagamento) {
        this.dsFormaAutorizacaoPagamento = dsFormaAutorizacaoPagamento;
    }

    /**
     * Nome: getListaOpcoesRadioFormaAutorizacao
     *
     * @return listaOpcoesRadioFormaAutorizacao
     */
    public List<SelectItem> getListaOpcoesRadioFormaAutorizacao() {
        return listaOpcoesRadioFormaAutorizacao;
    }

    /**
     * Nome: setListaOpcoesRadioFormaAutorizacao
     *
     * @param listaOpcoesRadioFormaAutorizacao
     */
    public void setListaOpcoesRadioFormaAutorizacao(List<SelectItem> listaOpcoesRadioFormaAutorizacao) {
        this.listaOpcoesRadioFormaAutorizacao = listaOpcoesRadioFormaAutorizacao;
    }

    /**
     * Nome: getCdFormaAutorizacaoPagamento
     *
     * @return cdFormaAutorizacaoPagamento
     */
    public Integer getCdFormaAutorizacaoPagamento() {
        return cdFormaAutorizacaoPagamento;
    }

    /**
     * Nome: setCdFormaAutorizacaoPagamento
     *
     * @param cdFormaAutorizacaoPagamento
     */
    public void setCdFormaAutorizacaoPagamento(Integer cdFormaAutorizacaoPagamento) {
        this.cdFormaAutorizacaoPagamento = cdFormaAutorizacaoPagamento;
    }

    /**
     * Nome: getCdFormaAutorizacaoPagamentoAlteracao
     *
     * @return cdFormaAutorizacaoPagamentoAlteracao
     */
    public Integer getCdFormaAutorizacaoPagamentoAlteracao() {
        return cdFormaAutorizacaoPagamentoAlteracao;
    }

    /**
     * Nome: setCdFormaAutorizacaoPagamentoAlteracao
     *
     * @param cdFormaAutorizacaoPagamentoAlteracao
     */
    public void setCdFormaAutorizacaoPagamentoAlteracao(Integer cdFormaAutorizacaoPagamentoAlteracao) {
        this.cdFormaAutorizacaoPagamentoAlteracao = cdFormaAutorizacaoPagamentoAlteracao;
    }

    /**
     * Nome: getCdSetorContrato
     *
     * @return cdSetorContrato
     */
    public Integer getCdSetorContrato() {
        return cdSetorContrato;
    }

    /**
     * Nome: setCdSetorContrato
     *
     * @param cdSetorContrato
     */
    public void setCdSetorContrato(Integer cdSetorContrato) {
        this.cdSetorContrato = cdSetorContrato;
    }

    /**
     * Nome: getCdSetorContratoAlteracao
     *
     * @return cdSetorContratoAlteracao
     */
    public Integer getCdSetorContratoAlteracao() {
        return cdSetorContratoAlteracao;
    }

    /**
     * Nome: setCdSetorContratoAlteracao
     *
     * @param cdSetorContratoAlteracao
     */
    public void setCdSetorContratoAlteracao(Integer cdSetorContratoAlteracao) {
        this.cdSetorContratoAlteracao = cdSetorContratoAlteracao;
    }

    /**
     * Nome: getVlSaldoVirtualTesteAlteracao
     *
     * @return vlSaldoVirtualTesteAlteracao
     */
    public BigDecimal getVlSaldoVirtualTesteAlteracao() {
        return vlSaldoVirtualTesteAlteracao;
    }

    /**
     * Nome: setVlSaldoVirtualTesteAlteracao
     *
     * @param vlSaldoVirtualTesteAlteracao
     */
    public void setVlSaldoVirtualTesteAlteracao(BigDecimal vlSaldoVirtualTesteAlteracao) {
        this.vlSaldoVirtualTesteAlteracao = vlSaldoVirtualTesteAlteracao;
    }
}
