/*
 * Nome: br.com.bradesco.web.pgit.view.enuns
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.enuns;

/**
 * Nome: IdentificacaoClienteEnum
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public enum IdentificacaoClienteEnum {

	/** Atributo BANCO_AG_CONTA. */
	BANCO_AG_CONTA(1, "BANCO/AG./CONTA"),
	
	/** Atributo CPF_CNPJ. */
	CPF_CNPJ(2, "CPF/CNPJ");

	/** Atributo cdIdentificacao. */
	private Integer cdIdentificacao = null;

	/** Atributo dsIdentificacao. */
	private String dsIdentificacao = null;

	/**
	 * Identificacao cliente enum.
	 *
	 * @param cdIdentificacao the cd identificacao
	 * @param dsIdentificacao the ds identificacao
	 */
	private IdentificacaoClienteEnum(Integer cdIdentificacao, String dsIdentificacao) {
		this.cdIdentificacao = cdIdentificacao;
		this.dsIdentificacao = dsIdentificacao;
	}

	/**
	 * Get: cdIdentificacao.
	 *
	 * @return cdIdentificacao
	 */
	public Integer getCdIdentificacao() {
		return cdIdentificacao;
	}

	/**
	 * (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this.dsIdentificacao;
	}	
}
