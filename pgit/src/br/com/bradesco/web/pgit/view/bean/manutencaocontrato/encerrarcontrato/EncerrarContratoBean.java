/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.encerrarcontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.encerrarcontrato;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.exception.PgitException;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService;
import br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.IImprimirRecisaoContratoPgitMulticanalService;
import br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.bean.ImprimirRecisaoContratoPgitMulticanalEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimirrecisaocontratopgitmulticanal.bean.ImprimirRecisaoContratoPgitMulticanalSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.EncerrarContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.EncerrarContratoSaidaDTO;
import br.com.bradesco.web.pgit.utils.MessageUtils;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: EncerrarContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class EncerrarContratoBean {
	
	//Atributos
	/** Atributo imprimirService. */
	private IImprimirService imprimirService = null;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo imprimirRecisaoContratoService. */
	private IImprimirRecisaoContratoPgitMulticanalService imprimirRecisaoContratoService;
	
	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;		
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;
	
	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;
	
	/** Atributo cpfCnpjClienteMaster. */
	private String cpfCnpjClienteMaster;
	
	/** Atributo nomeRazaoClienteMaster. */
	private String nomeRazaoClienteMaster;
	
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;
	
	/** Atributo cdGrupoEconomicoClienteMaster. */
	private Long cdGrupoEconomicoClienteMaster;
	
	/** Atributo grupoEconomicoClienteMaster. */
	private String grupoEconomicoClienteMaster;
	
	/** Atributo cdAtividadeEconomicaClienteMaster. */
	private int cdAtividadeEconomicaClienteMaster;
	
	/** Atributo atividadeEconomicaClienteMaster. */
	private String atividadeEconomicaClienteMaster;
	
	/** Atributo cdSegmentoClienteMaster. */
	private int cdSegmentoClienteMaster;
	
	/** Atributo segmentoClienteMaster. */
	private String segmentoClienteMaster;
	
	/** Atributo cdSubSegmentoClienteMaster. */
	private int cdSubSegmentoClienteMaster;
	
	/** Atributo subSegmentoClienteMaster. */
	private String subSegmentoClienteMaster;
	
	/** Atributo codPessoaJuridica. */
	private Long codPessoaJuridica;
	
	/** Atributo numeroSequencialUnidadeOrganizacional. */
	private int numeroSequencialUnidadeOrganizacional;
	
	/** Atributo cdClub. */
	private Long cdClub;
	
	/** Atributo cdEmpresaGestoraContrato. */
	private Long cdEmpresaGestoraContrato;
	
	/** Atributo empresaGestoraContrato. */
	private String empresaGestoraContrato;
	
	/** Atributo cdTipoContrato. */
	private int cdTipoContrato;
	
	/** Atributo tipoContrato. */
	private String tipoContrato;
	
	/** Atributo numeroContrato. */
	private String numeroContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo cdIdiomaContrato. */
	private int cdIdiomaContrato;
	
	/** Atributo idiomaContrato. */
	private String idiomaContrato;
	
	/** Atributo cdMoedaContrato. */
	private int cdMoedaContrato;
	
	/** Atributo moedaContrato. */
	private String moedaContrato;
	
	/** Atributo cdOrigemContrato. */
	private int cdOrigemContrato;
	
	/** Atributo origemContrato. */
	private String origemContrato;
	
	/** Atributo possuiAditivosContrato. */
	private String possuiAditivosContrato;
	
	/** Atributo cdSituacaoContrato. */
	private int cdSituacaoContrato;
	
	/** Atributo situacaoContrato. */
	private String situacaoContrato;
	
	/** Atributo cdMotivoContrato. */
	private int cdMotivoContrato;
	
	/** Atributo motivoContrato. */
	private String motivoContrato;
	
	/** Atributo inicioVigenciaContrato. */
	private String inicioVigenciaContrato;
	
	/** Atributo fimVigenciaContrato. */
	private String fimVigenciaContrato;
	
	/** Atributo numeroComercialContrato. */
	private String numeroComercialContrato;
	
	/** Atributo participacaoContrato. */
	private String participacaoContrato;
	
	/** Atributo dataHoraAssinaturaContrato. */
	private String dataHoraAssinaturaContrato;
	
	/** Atributo dataAssinaturaContrato. */
	private String dataAssinaturaContrato;
	
	/** Atributo cdPermiteEncerramentoContrato. */
	private int cdPermiteEncerramentoContrato;
	
	/** Atributo cdOperadoraUnidOrg. */
	private int cdOperadoraUnidOrg;
	
	/** Atributo cdContabilUnidOrg. */
	private int cdContabilUnidOrg;
	
	/** Atributo cdGestoraUnidOrg. */
	private int cdGestoraUnidOrg;
	
	/** Atributo gestoraUnidOrg. */
	private String gestoraUnidOrg;
	
	/** Atributo cdGerenteResponsavelUnidOrg. */
	private Long cdGerenteResponsavelUnidOrg;
	
	/** Atributo gerenteResponsavelUnidOrg. */
	private String gerenteResponsavelUnidOrg;
	
	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado;
	
	/**
	 * Get: gerenteResponsavelFormatado.
	 *
	 * @return gerenteResponsavelFormatado
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}

	/**
	 * Set: gerenteResponsavelFormatado.
	 *
	 * @param gerenteResponsavelFormatado the gerente responsavel formatado
	 */
	public void setGerenteResponsavelFormatado(String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}

	/** Atributo codSituacaoContrato. */
	private Integer codSituacaoContrato;
	
	/** Atributo codMotivoSituacao. */
	private Integer codMotivoSituacao;
	
	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;
	
	/** Atributo listaMotivo. */
	private List<SelectItem> listaMotivo = new ArrayList<SelectItem>();
	
	/** Atributo listaMotivoHash. */
	private Map<Integer,String> listaMotivoHash = new HashMap<Integer,String>();
	
	//M�todos
	/**
	 * Iniciar pagina.
	 *
	 * @param evt the evt
	 */
	public void iniciarPagina(ActionEvent evt) {
		
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteEncerrarContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("conEncerrarContrato");

		//carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		
		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();	
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
	}

	/**
	 * Encerrar.
	 *
	 * @return the string
	 */
	public String encerrar(){
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

		ConsultarDadosBasicoContratoEntradaDTO entradaDTO = new ConsultarDadosBasicoContratoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
		entradaDTO.setCdTipoContrato(saidaDTO.getCdTipoContrato());
		entradaDTO.setNumeroSequenciaContrato(saidaDTO.getNrSequenciaContrato());
		entradaDTO.setCdClub(saidaDTO.getCdClubRepresentante());
		entradaDTO.setCdCgcCpf(saidaDTO.getCdCpfCnpjRepresentante());
		entradaDTO.setCdFilialCgcCpf(saidaDTO.getCdFilialCpfCnpjRepresentante());
		entradaDTO.setCdControleCgcCpf(saidaDTO.getCdControleCpfCnpjRepresentante());
		entradaDTO.setCdAgencia(saidaDTO.getCdAgenciaOperadora());
		ConsultarDadosBasicoContratoSaidaDTO consultarDadosBasicoSaidaDTO = getManterContratoImpl().consultarDadosBasicoContrato(entradaDTO);

		setCpfCnpjClienteMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoClienteMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setCdGrupoEconomicoClienteMaster(consultarDadosBasicoSaidaDTO.getCdGrupoEconomicoParticipante());
		setGrupoEconomicoClienteMaster(consultarDadosBasicoSaidaDTO.getDsGrupoEconomico());
		setCdAtividadeEconomicaClienteMaster(consultarDadosBasicoSaidaDTO.getCdAtividadeEconomicaParticipante());
		setAtividadeEconomicaClienteMaster(consultarDadosBasicoSaidaDTO.getDsAtividadeEconomica());
		setCdSegmentoClienteMaster(consultarDadosBasicoSaidaDTO.getCdSegmentoEconomicoParticipante());
		setSegmentoClienteMaster(consultarDadosBasicoSaidaDTO.getDsSegmentoEconomico());
		setCdSubSegmentoClienteMaster(consultarDadosBasicoSaidaDTO.getCdSubSegmentoEconomico());
		setSubSegmentoClienteMaster(consultarDadosBasicoSaidaDTO.getDsSubSegmentoEconomico());

		//	Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());

		setCodPessoaJuridica(consultarDadosBasicoSaidaDTO.getCdPessoaJuridica());
		setNumeroSequencialUnidadeOrganizacional(consultarDadosBasicoSaidaDTO.getNumeroSequencialUnidadeOrganizacional());
		setCdClub(saidaDTO.getCdClubRepresentante());
		setCdEmpresaGestoraContrato(saidaDTO.getCdPessoaJuridica());
		setEmpresaGestoraContrato(saidaDTO.getDsPessoaJuridica());
		setCdTipoContrato(saidaDTO.getCdTipoContrato());
		setTipoContrato(saidaDTO.getDsTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setDescricaoContrato(saidaDTO.getDsContrato());
		setCdIdiomaContrato(consultarDadosBasicoSaidaDTO.getCdIdioma());
		setIdiomaContrato(consultarDadosBasicoSaidaDTO.getDsIdioma());
		setCdMoedaContrato(consultarDadosBasicoSaidaDTO.getCdMoeda());
		setMoedaContrato(consultarDadosBasicoSaidaDTO.getDsMoeda());
		setCdOrigemContrato(consultarDadosBasicoSaidaDTO.getCdOrigem());
		setOrigemContrato(consultarDadosBasicoSaidaDTO.getDsOrigem());
		setPossuiAditivosContrato(consultarDadosBasicoSaidaDTO.getDsPossuiAditivo());
		setGerenteResponsavelFormatado(consultarDadosBasicoSaidaDTO.getCdFuncionarioBradesco() + " - " + consultarDadosBasicoSaidaDTO.getDsFuncionarioBradesco());

		//o f�bio de almeida, pediu no email do dia 02/07/2010 �s 18:58 para pegar esses dados do campo cdSituacaoContrato do contrato
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setSituacaoContrato(saidaDTO.getDsSituacaoContrato());

		//o f�bio de almeida, pediu no email do dia 02/07/2010 �s 18:58 para pegar esses dados do campo cdSituacaoMotivoContrato da consulta de dados b�sicos
		setCdMotivoContrato(consultarDadosBasicoSaidaDTO.getCdSituacaoContrato());
		setMotivoContrato(consultarDadosBasicoSaidaDTO.getDsSituacaoContrato());

		setInicioVigenciaContrato(consultarDadosBasicoSaidaDTO.getDataVigenciaContrato() );
		setFimVigenciaContrato(consultarDadosBasicoSaidaDTO.getDataCadastroContrato() );
		setNumeroComercialContrato(consultarDadosBasicoSaidaDTO.getNumeroComercialContrato());
		setParticipacaoContrato(consultarDadosBasicoSaidaDTO.getDsTipoParticipacaoContrato());
		setDataHoraAssinaturaContrato(consultarDadosBasicoSaidaDTO.getDataAssinaturaContrato() + " - " + consultarDadosBasicoSaidaDTO.getHoraAssinaturaContrato());
		setDataAssinaturaContrato(consultarDadosBasicoSaidaDTO.getDataAssinaturaContrato());
		setCdPermiteEncerramentoContrato(consultarDadosBasicoSaidaDTO.getCdIndicaPermiteEncerramento());
		setCdOperadoraUnidOrg(consultarDadosBasicoSaidaDTO.getCdAgenciaOperadora());
		setCdContabilUnidOrg(consultarDadosBasicoSaidaDTO.getCdAgenciaContabil());
		setCdGestoraUnidOrg(consultarDadosBasicoSaidaDTO.getCdDeptoGestor());

		if(getCdGestoraUnidOrg() != 0){
			setGestoraUnidOrg(consultarDadosBasicoSaidaDTO.getCdDeptoGestor() + " - " + consultarDadosBasicoSaidaDTO.getDsDeptoGestor());
		}else{
			setGestoraUnidOrg("");
		}

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		} else {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		}

		setCdGerenteResponsavelUnidOrg(consultarDadosBasicoSaidaDTO.getCdFuncionarioBradesco());
		setGerenteResponsavelUnidOrg(consultarDadosBasicoSaidaDTO.getCdFuncionarioBradesco() + " - " + consultarDadosBasicoSaidaDTO.getDsFuncionarioBradesco());

		carregaListaMotivo();
		limparEncerrar();

		return "ENCERRAR";
	}
	
	/**
	 * Carrega lista motivo.
	 */
	public void carregaListaMotivo(){
		
		this.listaMotivo = new ArrayList<SelectItem>();
		
		List<ListarMotivoSituacaoContratoSaidaDTO> saidaDTO = new ArrayList<ListarMotivoSituacaoContratoSaidaDTO>();
		ListarMotivoSituacaoContratoEntradaDTO entradaDTO = new ListarMotivoSituacaoContratoEntradaDTO();
		entradaDTO.setCdSituacao(getCodSituacaoContrato());
		saidaDTO = comboService.listarMotivoSituacaoContrato(entradaDTO);
		listaMotivoHash.clear();
		
		for(ListarMotivoSituacaoContratoSaidaDTO combo : saidaDTO){
			listaMotivoHash.put(combo.getCdMotivoSituacaoContrato(), combo.getDsMotivoSituacaoContrato());
			this.listaMotivo.add(new SelectItem(combo.getCdMotivoSituacaoContrato(), combo.getDsMotivoSituacaoContrato()));
		}	
	}
	
	/**
	 * Limpar encerrar.
	 */
	public void limparEncerrar(){
		this.codMotivoSituacao = 0;
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		//identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Avancar encerrar.
	 *
	 * @return the string
	 */
	public String avancarEncerrar(){
		if (this.getObrigatoriedade().equals("T")){
			setDsMotivoSituacao((String)listaMotivoHash.get(getCodMotivoSituacao()));
			return "AVANCAR_ENCERRAR";
		}else{
			return "";
		}
	}
	
	/**
	 * Confirmar encerrar.
	 *
	 * @return the string
	 */
	public String confirmarEncerrar(){
		try {
			
			EncerrarContratoEntradaDTO entradaDTO = new EncerrarContratoEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaContrato(getCdEmpresaGestoraContrato());
			entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato());
			entradaDTO.setNumeroSequencialContratoNegocio(Long.parseLong(getNumeroContrato()));
			entradaDTO.setCdSituacaoContrato(getCodMotivoSituacao());
							
			EncerrarContratoSaidaDTO saidaDTO = getManterContratoImpl().encerrarContrato(entradaDTO);
						
			
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem()
					+ ") " + saidaDTO.getMensagem(),
					"conEncerrarContrato",
					"#{encerrarContratoBean.consultarAposAutorizar}", false);
		
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	

    /**
     * Consultar apos autorizar.
     *
     * @param event the event
     * @return the string
     */
    public String consultarAposAutorizar(ActionEvent event) {
        try {
        	identificacaoClienteContratoBean.consultarContrato();
        } catch (PdcAdapterFunctionalException e) {
            BradescoFacesUtils.addErrorModalMessage("(" + MessageUtils.getMessageCode(e) + ") " + e.getMessage());
            return "";
        }
        return "conEncerrarContrato";
    }

	/**
	 * Imprimir recisao contrato.
	 *
	 * @return the string
	 */
	public String imprimirRecisaoContrato() {
		ListarContratosPgitSaidaDTO saidaLista = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		ImprimirRecisaoContratoPgitMulticanalEntradaDTO entradaDto = new ImprimirRecisaoContratoPgitMulticanalEntradaDTO();

		entradaDto.setCdPessoaJuridicaContrato(saidaLista.getCdPessoaJuridica());
		entradaDto.setCdTipoContratoNegocio(saidaLista.getCdTipoContrato());
		entradaDto.setNrSequenciaContratoNegocio(saidaLista.getNrSequenciaContrato());
		entradaDto.setCdVersao(1);

		ImprimirRecisaoContratoPgitMulticanalSaidaDTO saidaDto = getImprimirRecisaoContratoService().imprimirRecisaoContrato(entradaDto);

		try {
			getImprimirService().imprimirArquivo(FacesUtils.getServletResponse().getOutputStream(), saidaDto.getProtocolo());
		} catch (IOException e) {
			throw new PgitException(e.getMessage(), e, "");
		}

		PgitFacesUtils.generateReportResponse("contratoRecisao");

		return "";
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt) {
		getIdentificacaoClienteContratoBean().consultarContrato();
	}

	//Set's e Get's
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: atividadeEconomicaClienteMaster.
	 *
	 * @return atividadeEconomicaClienteMaster
	 */
	public String getAtividadeEconomicaClienteMaster() {
		return atividadeEconomicaClienteMaster;
	}

	/**
	 * Set: atividadeEconomicaClienteMaster.
	 *
	 * @param atividadeEconomicaClienteMaster the atividade economica cliente master
	 */
	public void setAtividadeEconomicaClienteMaster(
			String atividadeEconomicaClienteMaster) {
		this.atividadeEconomicaClienteMaster = atividadeEconomicaClienteMaster;
	}

	/**
	 * Get: cpfCnpjClienteMaster.
	 *
	 * @return cpfCnpjClienteMaster
	 */
	public String getCpfCnpjClienteMaster() {
		return cpfCnpjClienteMaster;
	}

	/**
	 * Set: cpfCnpjClienteMaster.
	 *
	 * @param cpfCnpjClienteMaster the cpf cnpj cliente master
	 */
	public void setCpfCnpjClienteMaster(String cpfCnpjClienteMaster) {
		this.cpfCnpjClienteMaster = cpfCnpjClienteMaster;
	}

	/**
	 * Get: grupoEconomicoClienteMaster.
	 *
	 * @return grupoEconomicoClienteMaster
	 */
	public String getGrupoEconomicoClienteMaster() {
		return grupoEconomicoClienteMaster;
	}

	/**
	 * Set: grupoEconomicoClienteMaster.
	 *
	 * @param grupoEconomicoClienteMaster the grupo economico cliente master
	 */
	public void setGrupoEconomicoClienteMaster(String grupoEconomicoClienteMaster) {
		this.grupoEconomicoClienteMaster = grupoEconomicoClienteMaster;
	}

	/**
	 * Get: nomeRazaoClienteMaster.
	 *
	 * @return nomeRazaoClienteMaster
	 */
	public String getNomeRazaoClienteMaster() {
		return nomeRazaoClienteMaster;
	}

	/**
	 * Set: nomeRazaoClienteMaster.
	 *
	 * @param nomeRazaoClienteMaster the nome razao cliente master
	 */
	public void setNomeRazaoClienteMaster(String nomeRazaoClienteMaster) {
		this.nomeRazaoClienteMaster = nomeRazaoClienteMaster;
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: cdAtividadeEconomicaClienteMaster.
	 *
	 * @return cdAtividadeEconomicaClienteMaster
	 */
	public int getCdAtividadeEconomicaClienteMaster() {
		return cdAtividadeEconomicaClienteMaster;
	}

	/**
	 * Set: cdAtividadeEconomicaClienteMaster.
	 *
	 * @param cdAtividadeEconomicaClienteMaster the cd atividade economica cliente master
	 */
	public void setCdAtividadeEconomicaClienteMaster(
			int cdAtividadeEconomicaClienteMaster) {
		this.cdAtividadeEconomicaClienteMaster = cdAtividadeEconomicaClienteMaster;
	}

	/**
	 * Get: cdGrupoEconomicoClienteMaster.
	 *
	 * @return cdGrupoEconomicoClienteMaster
	 */
	public Long getCdGrupoEconomicoClienteMaster() {
		return cdGrupoEconomicoClienteMaster;
	}

	/**
	 * Set: cdGrupoEconomicoClienteMaster.
	 *
	 * @param cdGrupoEconomicoClienteMaster the cd grupo economico cliente master
	 */
	public void setCdGrupoEconomicoClienteMaster(Long cdGrupoEconomicoClienteMaster) {
		this.cdGrupoEconomicoClienteMaster = cdGrupoEconomicoClienteMaster;
	}

	/**
	 * Get: cdSegmentoClienteMaster.
	 *
	 * @return cdSegmentoClienteMaster
	 */
	public int getCdSegmentoClienteMaster() {
		return cdSegmentoClienteMaster;
	}

	/**
	 * Set: cdSegmentoClienteMaster.
	 *
	 * @param cdSegmentoClienteMaster the cd segmento cliente master
	 */
	public void setCdSegmentoClienteMaster(int cdSegmentoClienteMaster) {
		this.cdSegmentoClienteMaster = cdSegmentoClienteMaster;
	}

	/**
	 * Get: cdSubSegmentoClienteMaster.
	 *
	 * @return cdSubSegmentoClienteMaster
	 */
	public int getCdSubSegmentoClienteMaster() {
		return cdSubSegmentoClienteMaster;
	}

	/**
	 * Set: cdSubSegmentoClienteMaster.
	 *
	 * @param cdSubSegmentoClienteMaster the cd sub segmento cliente master
	 */
	public void setCdSubSegmentoClienteMaster(int cdSubSegmentoClienteMaster) {
		this.cdSubSegmentoClienteMaster = cdSubSegmentoClienteMaster;
	}

	/**
	 * Get: segmentoClienteMaster.
	 *
	 * @return segmentoClienteMaster
	 */
	public String getSegmentoClienteMaster() {
		return segmentoClienteMaster;
	}

	/**
	 * Set: segmentoClienteMaster.
	 *
	 * @param segmentoClienteMaster the segmento cliente master
	 */
	public void setSegmentoClienteMaster(String segmentoClienteMaster) {
		this.segmentoClienteMaster = segmentoClienteMaster;
	}

	/**
	 * Get: subSegmentoClienteMaster.
	 *
	 * @return subSegmentoClienteMaster
	 */
	public String getSubSegmentoClienteMaster() {
		return subSegmentoClienteMaster;
	}

	/**
	 * Set: subSegmentoClienteMaster.
	 *
	 * @param subSegmentoClienteMaster the sub segmento cliente master
	 */
	public void setSubSegmentoClienteMaster(String subSegmentoClienteMaster) {
		this.subSegmentoClienteMaster = subSegmentoClienteMaster;
	}

	/**
	 * Get: cdClub.
	 *
	 * @return cdClub
	 */
	public Long getCdClub() {
		return cdClub;
	}

	/**
	 * Set: cdClub.
	 *
	 * @param cdClub the cd club
	 */
	public void setCdClub(Long cdClub) {
		this.cdClub = cdClub;
	}

	/**
	 * Get: cdEmpresaGestoraContrato.
	 *
	 * @return cdEmpresaGestoraContrato
	 */
	public Long getCdEmpresaGestoraContrato() {
		return cdEmpresaGestoraContrato;
	}

	/**
	 * Set: cdEmpresaGestoraContrato.
	 *
	 * @param cdEmpresaGestoraContrato the cd empresa gestora contrato
	 */
	public void setCdEmpresaGestoraContrato(Long cdEmpresaGestoraContrato) {
		this.cdEmpresaGestoraContrato = cdEmpresaGestoraContrato;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public int getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(int cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: codPessoaJuridica.
	 *
	 * @return codPessoaJuridica
	 */
	public Long getCodPessoaJuridica() {
		return codPessoaJuridica;
	}

	/**
	 * Set: codPessoaJuridica.
	 *
	 * @param codPessoaJuridica the cod pessoa juridica
	 */
	public void setCodPessoaJuridica(Long codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: empresaGestoraContrato.
	 *
	 * @return empresaGestoraContrato
	 */
	public String getEmpresaGestoraContrato() {
		return empresaGestoraContrato;
	}

	/**
	 * Set: empresaGestoraContrato.
	 *
	 * @param empresaGestoraContrato the empresa gestora contrato
	 */
	public void setEmpresaGestoraContrato(String empresaGestoraContrato) {
		this.empresaGestoraContrato = empresaGestoraContrato;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: numeroSequencialUnidadeOrganizacional.
	 *
	 * @return numeroSequencialUnidadeOrganizacional
	 */
	public int getNumeroSequencialUnidadeOrganizacional() {
		return numeroSequencialUnidadeOrganizacional;
	}

	/**
	 * Set: numeroSequencialUnidadeOrganizacional.
	 *
	 * @param numeroSequencialUnidadeOrganizacional the numero sequencial unidade organizacional
	 */
	public void setNumeroSequencialUnidadeOrganizacional(
			int numeroSequencialUnidadeOrganizacional) {
		this.numeroSequencialUnidadeOrganizacional = numeroSequencialUnidadeOrganizacional;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: cdIdiomaContrato.
	 *
	 * @return cdIdiomaContrato
	 */
	public int getCdIdiomaContrato() {
		return cdIdiomaContrato;
	}

	/**
	 * Set: cdIdiomaContrato.
	 *
	 * @param cdIdiomaContrato the cd idioma contrato
	 */
	public void setCdIdiomaContrato(int cdIdiomaContrato) {
		this.cdIdiomaContrato = cdIdiomaContrato;
	}

	/**
	 * Get: cdMoedaContrato.
	 *
	 * @return cdMoedaContrato
	 */
	public int getCdMoedaContrato() {
		return cdMoedaContrato;
	}

	/**
	 * Set: cdMoedaContrato.
	 *
	 * @param cdMoedaContrato the cd moeda contrato
	 */
	public void setCdMoedaContrato(int cdMoedaContrato) {
		this.cdMoedaContrato = cdMoedaContrato;
	}

	/**
	 * Get: cdOrigemContrato.
	 *
	 * @return cdOrigemContrato
	 */
	public int getCdOrigemContrato() {
		return cdOrigemContrato;
	}

	/**
	 * Set: cdOrigemContrato.
	 *
	 * @param cdOrigemContrato the cd origem contrato
	 */
	public void setCdOrigemContrato(int cdOrigemContrato) {
		this.cdOrigemContrato = cdOrigemContrato;
	}

	/**
	 * Get: idiomaContrato.
	 *
	 * @return idiomaContrato
	 */
	public String getIdiomaContrato() {
		return idiomaContrato;
	}

	/**
	 * Set: idiomaContrato.
	 *
	 * @param idiomaContrato the idioma contrato
	 */
	public void setIdiomaContrato(String idiomaContrato) {
		this.idiomaContrato = idiomaContrato;
	}

	/**
	 * Get: moedaContrato.
	 *
	 * @return moedaContrato
	 */
	public String getMoedaContrato() {
		return moedaContrato;
	}

	/**
	 * Set: moedaContrato.
	 *
	 * @param moedaContrato the moeda contrato
	 */
	public void setMoedaContrato(String moedaContrato) {
		this.moedaContrato = moedaContrato;
	}

	/**
	 * Get: origemContrato.
	 *
	 * @return origemContrato
	 */
	public String getOrigemContrato() {
		return origemContrato;
	}

	/**
	 * Set: origemContrato.
	 *
	 * @param origemContrato the origem contrato
	 */
	public void setOrigemContrato(String origemContrato) {
		this.origemContrato = origemContrato;
	}

	/**
	 * Get: possuiAditivosContrato.
	 *
	 * @return possuiAditivosContrato
	 */
	public String getPossuiAditivosContrato() {
		return possuiAditivosContrato;
	}

	/**
	 * Set: possuiAditivosContrato.
	 *
	 * @param possuiAditivosContrato the possui aditivos contrato
	 */
	public void setPossuiAditivosContrato(String possuiAditivosContrato) {
		this.possuiAditivosContrato = possuiAditivosContrato;
	}

	/**
	 * Get: cdMotivoContrato.
	 *
	 * @return cdMotivoContrato
	 */
	public int getCdMotivoContrato() {
		return cdMotivoContrato;
	}

	/**
	 * Set: cdMotivoContrato.
	 *
	 * @param cdMotivoContrato the cd motivo contrato
	 */
	public void setCdMotivoContrato(int cdMotivoContrato) {
		this.cdMotivoContrato = cdMotivoContrato;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public int getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(int cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: fimVigenciaContrato.
	 *
	 * @return fimVigenciaContrato
	 */
	public String getFimVigenciaContrato() {
		return fimVigenciaContrato;
	}

	/**
	 * Set: fimVigenciaContrato.
	 *
	 * @param fimVigenciaContrato the fim vigencia contrato
	 */
	public void setFimVigenciaContrato(String fimVigenciaContrato) {
		this.fimVigenciaContrato = fimVigenciaContrato;
	}

	/**
	 * Get: inicioVigenciaContrato.
	 *
	 * @return inicioVigenciaContrato
	 */
	public String getInicioVigenciaContrato() {
		return inicioVigenciaContrato;
	}

	/**
	 * Set: inicioVigenciaContrato.
	 *
	 * @param inicioVigenciaContrato the inicio vigencia contrato
	 */
	public void setInicioVigenciaContrato(String inicioVigenciaContrato) {
		this.inicioVigenciaContrato = inicioVigenciaContrato;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: cdOperadoraUnidOrg.
	 *
	 * @return cdOperadoraUnidOrg
	 */
	public int getCdOperadoraUnidOrg() {
		return cdOperadoraUnidOrg;
	}

	/**
	 * Set: cdOperadoraUnidOrg.
	 *
	 * @param cdOperadoraUnidOrg the cd operadora unid org
	 */
	public void setCdOperadoraUnidOrg(int cdOperadoraUnidOrg) {
		this.cdOperadoraUnidOrg = cdOperadoraUnidOrg;
	}

	/**
	 * Get: cdPermiteEncerramentoContrato.
	 *
	 * @return cdPermiteEncerramentoContrato
	 */
	public int getCdPermiteEncerramentoContrato() {
		return cdPermiteEncerramentoContrato;
	}

	/**
	 * Set: cdPermiteEncerramentoContrato.
	 *
	 * @param cdPermiteEncerramentoContrato the cd permite encerramento contrato
	 */
	public void setCdPermiteEncerramentoContrato(int cdPermiteEncerramentoContrato) {
		this.cdPermiteEncerramentoContrato = cdPermiteEncerramentoContrato;
	}

	/**
	 * Get: dataHoraAssinaturaContrato.
	 *
	 * @return dataHoraAssinaturaContrato
	 */
	public String getDataHoraAssinaturaContrato() {
		return dataHoraAssinaturaContrato;
	}

	/**
	 * Set: dataHoraAssinaturaContrato.
	 *
	 * @param dataHoraAssinaturaContrato the data hora assinatura contrato
	 */
	public void setDataHoraAssinaturaContrato(String dataHoraAssinaturaContrato) {
		this.dataHoraAssinaturaContrato = dataHoraAssinaturaContrato;
	}

	/**
	 * Get: numeroComercialContrato.
	 *
	 * @return numeroComercialContrato
	 */
	public String getNumeroComercialContrato() {
		return numeroComercialContrato;
	}

	/**
	 * Set: numeroComercialContrato.
	 *
	 * @param numeroComercialContrato the numero comercial contrato
	 */
	public void setNumeroComercialContrato(String numeroComercialContrato) {
		this.numeroComercialContrato = numeroComercialContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: cdContabilUnidOrg.
	 *
	 * @return cdContabilUnidOrg
	 */
	public int getCdContabilUnidOrg() {
		return cdContabilUnidOrg;
	}

	/**
	 * Set: cdContabilUnidOrg.
	 *
	 * @param cdContabilUnidOrg the cd contabil unid org
	 */
	public void setCdContabilUnidOrg(int cdContabilUnidOrg) {
		this.cdContabilUnidOrg = cdContabilUnidOrg;
	}

	/**
	 * Get: cdGestoraUnidOrg.
	 *
	 * @return cdGestoraUnidOrg
	 */
	public int getCdGestoraUnidOrg() {
		return cdGestoraUnidOrg;
	}

	/**
	 * Set: cdGestoraUnidOrg.
	 *
	 * @param cdGestoraUnidOrg the cd gestora unid org
	 */
	public void setCdGestoraUnidOrg(int cdGestoraUnidOrg) {
		this.cdGestoraUnidOrg = cdGestoraUnidOrg;
	}

	/**
	 * Get: gestoraUnidOrg.
	 *
	 * @return gestoraUnidOrg
	 */
	public String getGestoraUnidOrg() {
		return gestoraUnidOrg;
	}

	/**
	 * Set: gestoraUnidOrg.
	 *
	 * @param gestoraUnidOrg the gestora unid org
	 */
	public void setGestoraUnidOrg(String gestoraUnidOrg) {
		this.gestoraUnidOrg = gestoraUnidOrg;
	}

	/**
	 * Get: cdGerenteResponsavelUnidOrg.
	 *
	 * @return cdGerenteResponsavelUnidOrg
	 */
	public Long getCdGerenteResponsavelUnidOrg() {
		return cdGerenteResponsavelUnidOrg;
	}

	/**
	 * Set: cdGerenteResponsavelUnidOrg.
	 *
	 * @param cdGerenteResponsavelUnidOrg the cd gerente responsavel unid org
	 */
	public void setCdGerenteResponsavelUnidOrg(Long cdGerenteResponsavelUnidOrg) {
		this.cdGerenteResponsavelUnidOrg = cdGerenteResponsavelUnidOrg;
	}

	/**
	 * Get: gerenteResponsavelUnidOrg.
	 *
	 * @return gerenteResponsavelUnidOrg
	 */
	public String getGerenteResponsavelUnidOrg() {
		return gerenteResponsavelUnidOrg;
	}

	/**
	 * Set: gerenteResponsavelUnidOrg.
	 *
	 * @param gerenteResponsavelUnidOrg the gerente responsavel unid org
	 */
	public void setGerenteResponsavelUnidOrg(String gerenteResponsavelUnidOrg) {
		this.gerenteResponsavelUnidOrg = gerenteResponsavelUnidOrg;
	}

	/**
	 * Get: codSituacaoContrato.
	 *
	 * @return codSituacaoContrato
	 */
	public Integer getCodSituacaoContrato() {
		this.codSituacaoContrato = 6;
		return codSituacaoContrato;
	}

	/**
	 * Set: codSituacaoContrato.
	 *
	 * @param codSituacaoContrato the cod situacao contrato
	 */
	public void setCodSituacaoContrato(Integer codSituacaoContrato) {
		this.codSituacaoContrato = codSituacaoContrato;
	}

	/**
	 * Get: listaMotivo.
	 *
	 * @return listaMotivo
	 */
	public List<SelectItem> getListaMotivo() {
		return listaMotivo;
	}

	/**
	 * Set: listaMotivo.
	 *
	 * @param listaMotivo the lista motivo
	 */
	public void setListaMotivo(List<SelectItem> listaMotivo) {
		this.listaMotivo = listaMotivo;
	}

	/**
	 * Get: listaMotivoHash.
	 *
	 * @return listaMotivoHash
	 */
	public Map<Integer, String> getListaMotivoHash() {
		return listaMotivoHash;
	}

	/**
	 * Set lista motivo hash.
	 *
	 * @param listaMotivoHash the lista motivo hash
	 */
	public void setListaMotivoHash(Map<Integer, String> listaMotivoHash) {
		this.listaMotivoHash = listaMotivoHash;
	}

	/**
	 * Get: codMotivoSituacao.
	 *
	 * @return codMotivoSituacao
	 */
	public Integer getCodMotivoSituacao() {
		return codMotivoSituacao;
	}

	/**
	 * Set: codMotivoSituacao.
	 *
	 * @param codMotivoSituacao the cod motivo situacao
	 */
	public void setCodMotivoSituacao(Integer codMotivoSituacao) {
		this.codMotivoSituacao = codMotivoSituacao;
	}

	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Get: dataAssinaturaContrato.
	 *
	 * @return dataAssinaturaContrato
	 */
	public String getDataAssinaturaContrato() {
		return dataAssinaturaContrato;
	}

	/**
	 * Set: dataAssinaturaContrato.
	 *
	 * @param dataAssinaturaContrato the data assinatura contrato
	 */
	public void setDataAssinaturaContrato(String dataAssinaturaContrato) {
		this.dataAssinaturaContrato = dataAssinaturaContrato;
	}

	/**
	 * Get: imprimirRecisaoContratoService.
	 *
	 * @return imprimirRecisaoContratoService
	 */
	public IImprimirRecisaoContratoPgitMulticanalService getImprimirRecisaoContratoService() {
		return imprimirRecisaoContratoService;
	}

	/**
	 * Set: imprimirRecisaoContratoService.
	 *
	 * @param imprimirRecisaoContratoService the imprimir recisao contrato service
	 */
	public void setImprimirRecisaoContratoService(
			IImprimirRecisaoContratoPgitMulticanalService imprimirRecisaoContratoService) {
		this.imprimirRecisaoContratoService = imprimirRecisaoContratoService;
	}

	/**
	 * Get: imprimirService.
	 *
	 * @return imprimirService
	 */
	public IImprimirService getImprimirService() {
		return imprimirService;
	}

	/**
	 * Set: imprimirService.
	 *
	 * @param imprimirService the imprimir service
	 */
	public void setImprimirService(IImprimirService imprimirService) {
		this.imprimirService = imprimirService;
	}
	
	
	
	

}
