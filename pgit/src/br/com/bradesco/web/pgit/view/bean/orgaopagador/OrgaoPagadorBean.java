/*
 * Nome: br.com.bradesco.web.pgit.view.bean.orgaopagador
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.orgaopagador;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterException;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaConglomeradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoUnidadeOrganizacionalDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.IOrgaoPagadorService;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorDetalheSaidaDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.orgaopagador.bean.OrgaoPagadorSaidaDTO;
import br.com.bradesco.web.pgit.utils.constants.ErrorMessageConstants;

/**
 * Nome: OrgaoPagadorBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class OrgaoPagadorBean {

	/** Atributo codEmpresa. */
	private String codEmpresa;

	/** Atributo orgaoPagadorFiltro. */
	private OrgaoPagadorEntradaDTO orgaoPagadorFiltro = new OrgaoPagadorEntradaDTO();

	/** Atributo orgaoPagadorInclusao. */
	private OrgaoPagadorEntradaDTO orgaoPagadorInclusao = new OrgaoPagadorEntradaDTO();
	
	/** Atributo orgaoPagadorAlteracao. */
	private OrgaoPagadorEntradaDTO orgaoPagadorAlteracao = new OrgaoPagadorEntradaDTO();

	/** Atributo orgaoPagadorBloqueio. */
	private OrgaoPagadorEntradaDTO orgaoPagadorBloqueio = new OrgaoPagadorEntradaDTO();

	/** Atributo detalheSaidaDTO. */
	private OrgaoPagadorDetalheSaidaDTO detalheSaidaDTO = new OrgaoPagadorDetalheSaidaDTO();

	/** Atributo listaTipoUnidade. */
	private List<TipoUnidadeOrganizacionalDTO> listaTipoUnidade = new ArrayList<TipoUnidadeOrganizacionalDTO>();

	/** Atributo listaEmpresaGrupoBradesco. */
	private List<EmpresaConglomeradoSaidaDTO> listaEmpresaGrupoBradesco = new ArrayList<EmpresaConglomeradoSaidaDTO>();

	/** Atributo listaEmpresaParceira. */
	private List<EmpresaConglomeradoSaidaDTO> listaEmpresaParceira = new ArrayList<EmpresaConglomeradoSaidaDTO>();

	/** Atributo listaCategoria. */
	private List<SelectItem> listaCategoria = new ArrayList<SelectItem>();

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo listaGridPesquisa. */
	private List<OrgaoPagadorSaidaDTO> listaGridPesquisa;

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo orgaoPagadorService. */
	private IOrgaoPagadorService orgaoPagadorService;

	/** Atributo EMPRESA_BRADESCO. */
	private static final String EMPRESA_BRADESCO = "1";

	/** Atributo EMPRESA_PARCEIRA. */
	private static final String EMPRESA_PARCEIRA = "2";
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;

	/**
	 * Navegar tela inicial.
	 *
	 * @return the string
	 */
	public String navegarTelaInicial() {
		try{
			setListaTipoUnidade(getComboService().listarTipoUnidadeOrganizacional(new TipoUnidadeOrganizacionalDTO()));
		}catch(PdcAdapterFunctionalException e){
			listaTipoUnidade = new ArrayList<TipoUnidadeOrganizacionalDTO>();
		}
		
		try{
			setListaEmpresaGrupoBradesco(getComboService().listarEmpresaConglomerado(new EmpresaConglomeradoEntradaDTO()));
		}catch(PdcAdapterFunctionalException e){
			listaEmpresaGrupoBradesco = new ArrayList<EmpresaConglomeradoSaidaDTO>();
		}
		setBtoAcionado(false);
		setListaCategoria(obterListaCategoria());
		orgaoPagadorFiltro.setCdPessoaJuridica(2269651L);

		return "conOrgaoPagador";
	}

	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {
		this.setOrgaoPagadorFiltro(new OrgaoPagadorEntradaDTO());
		this.setListaGridPesquisa(null);
		this.setItemSelecionadoLista(null);
		setBtoAcionado(false);
		orgaoPagadorFiltro.setCdPessoaJuridica(2269651L);
		return "conOrgaoPagador";
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		carregaLista();

		return "conOrgaoPagador";
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		setOrgaoPagadorInclusao(new OrgaoPagadorEntradaDTO());
		setCodEmpresa("");
		orgaoPagadorInclusao.setCdPessoaJuridicaBradesco(2269651L);
		
		return "INCLUIR";
	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO = new OrgaoPagadorEntradaDTO();
		orgaoPagadorEntradaDTO.setCdOrgaoPagador(getItemSelecionadoLista());

		setDetalheSaidaDTO(getOrgaoPagadorService().detalharOrgaoPagador(orgaoPagadorEntradaDTO));

		return "EXCLUIR";
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		OrgaoPagadorSaidaDTO orgaoPagadorSelecionado = obterOrgaoPagadorSelecionado();

		OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO = new OrgaoPagadorEntradaDTO();
		orgaoPagadorEntradaDTO.setCdOrgaoPagador(orgaoPagadorSelecionado.getCdOrgaoPagador());
		orgaoPagadorEntradaDTO.setCdTipoUnidadeOrganizacional(orgaoPagadorSelecionado.getCdTipoUnidadeOrganizacional());
		orgaoPagadorEntradaDTO.setCdPessoaJuridicaBradesco(orgaoPagadorSelecionado.getCdPessoaJuridica());
		orgaoPagadorEntradaDTO.setNrSeqUnidadeOrganizacionalBradesco(orgaoPagadorSelecionado.getNrSeqUnidadeOrganizacional());
		orgaoPagadorEntradaDTO.setCdSituacao(orgaoPagadorSelecionado.getCdSituacao());

		setOrgaoPagadorAlteracao(orgaoPagadorEntradaDTO);

		setDetalheSaidaDTO(getOrgaoPagadorService().detalharOrgaoPagador(orgaoPagadorEntradaDTO));

		getOrgaoPagadorAlteracao().setCdTarifOrgaoPagador(getDetalheSaidaDTO().getCdTarifOrgaoPagador());

		setCodEmpresa(EMPRESA_BRADESCO);
		orgaoPagadorAlteracao.setCdPessoaJuridicaBradesco(2269651L);
		
		return "ALTERAR";
	}

	/**
	 * Obter orgao pagador selecionado.
	 *
	 * @return the orgao pagador saida dto
	 */
	private OrgaoPagadorSaidaDTO obterOrgaoPagadorSelecionado() {
		OrgaoPagadorSaidaDTO orgaoPagadorSelecionado = new OrgaoPagadorSaidaDTO();
		orgaoPagadorSelecionado.setCdOrgaoPagador(getItemSelecionadoLista());

		int indexSelecionado = getListaGridPesquisa().indexOf(orgaoPagadorSelecionado);

		if (indexSelecionado < 0) {
			throw new BradescoViewException("Item selecionado n�o encontrado!", "");
		}

		orgaoPagadorSelecionado = getListaGridPesquisa().get(indexSelecionado);
		return orgaoPagadorSelecionado;
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		atribuirValoresAvancar(getOrgaoPagadorInclusao());

		return "AVANCAR_INCLUIR";
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {
		atribuirValoresAvancar(getOrgaoPagadorAlteracao());

		return "AVANCAR_ALTERAR";
	}
	
	/**
	 * Atribuir valores avancar.
	 *
	 * @param orgaoPagadorEntradaDTO the orgao pagador entrada dto
	 */
	private void atribuirValoresAvancar(OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO) {
		if (getCodEmpresa() != null) {
			if (getCodEmpresa().equals(EMPRESA_BRADESCO)) {
				orgaoPagadorEntradaDTO.setCdPessoaJuridica(orgaoPagadorEntradaDTO.getCdPessoaJuridicaBradesco());
				orgaoPagadorEntradaDTO.setDsPessoaJuridica(obterDescricaoEmpresa(orgaoPagadorEntradaDTO.getCdPessoaJuridicaBradesco()));
				orgaoPagadorEntradaDTO.setNrSeqUnidadeOrganizacional(orgaoPagadorEntradaDTO.getNrSeqUnidadeOrganizacionalBradesco());
			} else if (getCodEmpresa().equals(EMPRESA_PARCEIRA)) {
				orgaoPagadorEntradaDTO.setCdPessoaJuridica(orgaoPagadorEntradaDTO.getCdPessoaJuridicaParceira());
				orgaoPagadorEntradaDTO.setDsPessoaJuridica("" + orgaoPagadorEntradaDTO.getCdPessoaJuridicaParceira());
				orgaoPagadorEntradaDTO.setNrSeqUnidadeOrganizacional(orgaoPagadorEntradaDTO.getNrSeqUnidadeOrganizacionalParceira());
			}
		}

		orgaoPagadorEntradaDTO.setDsTipoUnidadeOrganizacional(obterDescricaoTipoUnidade(orgaoPagadorEntradaDTO.getCdTipoUnidadeOrganizacional()));
	}
	
	/**
	 * Voltar pesquisa.
	 *
	 * @return the string
	 */
	public String voltarPesquisa(){
		setItemSelecionadoLista(null);
		
		return "VOLTAR_DADOS";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO = new OrgaoPagadorEntradaDTO();
		orgaoPagadorEntradaDTO.setCdOrgaoPagador(getItemSelecionadoLista());

		setDetalheSaidaDTO(getOrgaoPagadorService().detalharOrgaoPagador(orgaoPagadorEntradaDTO));

		return "DETALHAR";
	}

	/**
	 * Bloquear.
	 *
	 * @return the string
	 */
	public String bloquear() {
		OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO = new OrgaoPagadorEntradaDTO();
		orgaoPagadorEntradaDTO.setCdOrgaoPagador(getItemSelecionadoLista());

		setDetalheSaidaDTO(getOrgaoPagadorService().detalharOrgaoPagador(orgaoPagadorEntradaDTO));

		OrgaoPagadorSaidaDTO orgaoPagadorSelecionado = obterOrgaoPagadorSelecionado();
		OrgaoPagadorEntradaDTO bloqueio = new OrgaoPagadorEntradaDTO();
		bloqueio.setCdOrgaoPagador(orgaoPagadorSelecionado.getCdOrgaoPagador());
		bloqueio.setCdSituacao(orgaoPagadorSelecionado.getCdSituacao());
		setOrgaoPagadorBloqueio(bloqueio);

		return "BLOQUEAR";
	}

	/**
	 * Confirmar bloquear.
	 *
	 * @return the string
	 */
	public String confirmarBloquear() {
		OrgaoPagadorSaidaDTO orgaoPagadorSaidaDTO = getOrgaoPagadorService().bloquearOrgaoPagador(getOrgaoPagadorBloqueio());

		BradescoFacesUtils.addInfoModalMessage("(" + orgaoPagadorSaidaDTO.getCodMensagem() + ") " + orgaoPagadorSaidaDTO.getMensagem(), "conOrgaoPagador", BradescoViewExceptionActionType.ACTION, false);
		
		carregaLista();
		
		setItemSelecionadoLista(null);

		return null;
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {
		OrgaoPagadorSaidaDTO orgaoPagadorSaidaDTO = getOrgaoPagadorService().incluirOrgaoPagador(getOrgaoPagadorInclusao());

		BradescoFacesUtils.addInfoModalMessage("(" + orgaoPagadorSaidaDTO.getCodMensagem() + ") " + orgaoPagadorSaidaDTO.getMensagem(), "conOrgaoPagador", BradescoViewExceptionActionType.ACTION, false);

		carregaLista();

		return null;
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {
		OrgaoPagadorEntradaDTO orgaoPagadorEntradaDTO = new OrgaoPagadorEntradaDTO();
		orgaoPagadorEntradaDTO.setCdOrgaoPagador(getDetalheSaidaDTO().getCdOrgaoPagador());

		OrgaoPagadorSaidaDTO orgaoPagadorSaidaDTO = getOrgaoPagadorService().excluirOrgaoPagador(orgaoPagadorEntradaDTO);

		BradescoFacesUtils.addInfoModalMessage("(" + orgaoPagadorSaidaDTO.getCodMensagem() + ") " + orgaoPagadorSaidaDTO.getMensagem(), "conOrgaoPagador", BradescoViewExceptionActionType.ACTION, false);
		
		try {
			carregaLista();
		} catch (PdcAdapterFunctionalException e) {
			if (e.getCode() != null && e.getCode().endsWith(ErrorMessageConstants.PGIT0003)) {
				setListaGridPesquisa(new ArrayList<OrgaoPagadorSaidaDTO>());
				return null;
			}
			throw e;
		}
		
		setItemSelecionadoLista(null);
		
		return null;
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {
		OrgaoPagadorSaidaDTO orgaoPagadorSaidaDTO = getOrgaoPagadorService().alterarOrgaoPagador(getOrgaoPagadorAlteracao());

		BradescoFacesUtils.addInfoModalMessage("(" + orgaoPagadorSaidaDTO.getCodMensagem() + ") " + orgaoPagadorSaidaDTO.getMensagem(), "conOrgaoPagador", BradescoViewExceptionActionType.ACTION, false);
		
		carregaLista();
		
		setItemSelecionadoLista(null);

		return null;
	}

	/**
	 * Set: listaTipoUnidade.
	 *
	 * @param listaTipoUnidade the lista tipo unidade
	 */
	public void setListaTipoUnidade(List<TipoUnidadeOrganizacionalDTO> listaTipoUnidade) {
		this.listaTipoUnidade = listaTipoUnidade;
	}

	/**
	 * Get: codEmpresa.
	 *
	 * @return codEmpresa
	 */
	public String getCodEmpresa() {
		return codEmpresa;
	}

	/**
	 * Set: codEmpresa.
	 *
	 * @param codEmpresa the cod empresa
	 */
	public void setCodEmpresa(String codEmpresa) {
		this.codEmpresa = codEmpresa;
	}

	/**
	 * Get: listaEmpresaGrupoBradesco.
	 *
	 * @return listaEmpresaGrupoBradesco
	 */
	public List<EmpresaConglomeradoSaidaDTO> getListaEmpresaGrupoBradesco() {
		return listaEmpresaGrupoBradesco;
	}

	/**
	 * Set: listaEmpresaGrupoBradesco.
	 *
	 * @param listaEmpresaGrupoBradesco the lista empresa grupo bradesco
	 */
	public void setListaEmpresaGrupoBradesco(
			List<EmpresaConglomeradoSaidaDTO> listaEmpresaGrupoBradesco) {
		this.listaEmpresaGrupoBradesco = listaEmpresaGrupoBradesco;
	}

	/**
	 * Get: listaEmpresaParceira.
	 *
	 * @return listaEmpresaParceira
	 */
	public List<EmpresaConglomeradoSaidaDTO> getListaEmpresaParceira() {
		return listaEmpresaParceira;
	}

	/**
	 * Set: listaEmpresaParceira.
	 *
	 * @param listaEmpresaParceira the lista empresa parceira
	 */
	public void setListaEmpresaParceira(List<EmpresaConglomeradoSaidaDTO> listaEmpresaParceira) {
		this.listaEmpresaParceira = listaEmpresaParceira;
	}

	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
		setListaGridPesquisa(null);
		setCodEmpresa("");

		this.setItemSelecionadoLista(null);
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(List<OrgaoPagadorSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<OrgaoPagadorSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Carrega lista.
	 */
	public void carregaLista() {
		try {
			setListaGridPesquisa(getOrgaoPagadorService().listarOrgaoPagador(getOrgaoPagadorFiltro()));
			setBtoAcionado(true);
			this.itemSelecionadoLista = null;
		} catch (PdcAdapterException p) {
			this.setListaGridPesquisa(new ArrayList<OrgaoPagadorSaidaDTO>());
			throw p;
		} 
	}

	/**
	 * Habilitar campos empresa.
	 */
	public void habilitarCamposEmpresa() {
		habilitarEmpresa(getOrgaoPagadorInclusao());
		orgaoPagadorInclusao.setCdPessoaJuridicaBradesco(2269651L);
	}

	/**
	 * Habilitar campos empresa alteracao.
	 */
	public void habilitarCamposEmpresaAlteracao() {
		habilitarEmpresa(getOrgaoPagadorAlteracao());
		orgaoPagadorAlteracao.setCdPessoaJuridicaBradesco(2269651L);
	}

	/**
	 * Habilitar empresa.
	 *
	 * @param entradaDTO the entrada dto
	 */
	private void habilitarEmpresa(OrgaoPagadorEntradaDTO entradaDTO) {
		if (getCodEmpresa().equals(EMPRESA_BRADESCO)) {
			entradaDTO.setCdPessoaJuridicaParceira(null);
			entradaDTO.setNrSeqUnidadeOrganizacionalParceira(null);
		} else if (getCodEmpresa().equals(EMPRESA_PARCEIRA)) {
			entradaDTO.setCdPessoaJuridicaBradesco(null);
			entradaDTO.setNrSeqUnidadeOrganizacionalBradesco(null);
		}
	}

	/**
	 * Obter descricao empresa.
	 *
	 * @param codClub the cod club
	 * @return the string
	 */
	public String obterDescricaoEmpresa(Long codClub) {
		if (codClub == null) {
			return "";
		}
		
		if (getListaEmpresaGrupoBradesco() != null) {
			EmpresaConglomeradoSaidaDTO empresaConglomeradoSaidaDTO = new EmpresaConglomeradoSaidaDTO();
			empresaConglomeradoSaidaDTO.setCodClub(codClub);
			int indexEmpresa = getListaEmpresaGrupoBradesco().indexOf(empresaConglomeradoSaidaDTO);
			
			if (indexEmpresa < 0) {
				return "";
			}
			
			String descricaoCompleta = "" + getListaEmpresaGrupoBradesco().get(indexEmpresa).getCodClub() + getListaEmpresaGrupoBradesco().get(indexEmpresa).getDsRazaoSocial();

			return descricaoCompleta;
		}
		return "";
	}

	/**
	 * Obter descricao tipo unidade.
	 *
	 * @param tipoUnidade the tipo unidade
	 * @return the string
	 */
	public String obterDescricaoTipoUnidade(Integer tipoUnidade) {
		if (tipoUnidade == null) {
			return "";
		}
		
		if (getListaTipoUnidade() != null) {
			TipoUnidadeOrganizacionalDTO tipoUnidadeOrganizacionalDTO = new TipoUnidadeOrganizacionalDTO();
			tipoUnidadeOrganizacionalDTO.setCdTipoUnidade(tipoUnidade);
			int indexTipoUnidade = getListaTipoUnidade().indexOf(tipoUnidadeOrganizacionalDTO);

			if (indexTipoUnidade < 0) {
				return "";
			}

			return getListaTipoUnidade().get(indexTipoUnidade).getDsTipoUnidade();
		}
		return "";
	}

	/**
	 * Obter lista categoria.
	 *
	 * @return the list< select item>
	 */
	private List<SelectItem> obterListaCategoria() {
		List<SelectItem> lCategoria = new ArrayList<SelectItem>();
		lCategoria.add(new SelectItem(1, MessageHelperUtils.getI18nMessage("incOrgaoPagador_label_convencional")));
		lCategoria.add(new SelectItem(2, MessageHelperUtils.getI18nMessage("incOrgaoPagador_label_pioneira_normal")));
		lCategoria.add(new SelectItem(3, MessageHelperUtils.getI18nMessage("incOrgaoPagador_label_pioneira_diferenciada")));
		return lCategoria;
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: orgaoPagadorService.
	 *
	 * @return orgaoPagadorService
	 */
	public IOrgaoPagadorService getOrgaoPagadorService() {
		return orgaoPagadorService;
	}

	/**
	 * Set: orgaoPagadorService.
	 *
	 * @param orgaoPagadorService the orgao pagador service
	 */
	public void setOrgaoPagadorService(IOrgaoPagadorService orgaoPagadorService) {
		this.orgaoPagadorService = orgaoPagadorService;
	}

	/**
	 * Get: orgaoPagadorFiltro.
	 *
	 * @return orgaoPagadorFiltro
	 */
	public OrgaoPagadorEntradaDTO getOrgaoPagadorFiltro() {
		return orgaoPagadorFiltro;
	}

	/**
	 * Set: orgaoPagadorFiltro.
	 *
	 * @param orgaoPagadorFiltro the orgao pagador filtro
	 */
	public void setOrgaoPagadorFiltro(OrgaoPagadorEntradaDTO orgaoPagadorFiltro) {
		this.orgaoPagadorFiltro = orgaoPagadorFiltro;
	}

	/**
	 * Get: orgaoPagadorInclusao.
	 *
	 * @return orgaoPagadorInclusao
	 */
	public OrgaoPagadorEntradaDTO getOrgaoPagadorInclusao() {
		return orgaoPagadorInclusao;
	}

	/**
	 * Set: orgaoPagadorInclusao.
	 *
	 * @param orgaoPagadorInclusao the orgao pagador inclusao
	 */
	public void setOrgaoPagadorInclusao(OrgaoPagadorEntradaDTO orgaoPagadorInclusao) {
		this.orgaoPagadorInclusao = orgaoPagadorInclusao;
	}

	/**
	 * Get: orgaoPagadorAlteracao.
	 *
	 * @return orgaoPagadorAlteracao
	 */
	public OrgaoPagadorEntradaDTO getOrgaoPagadorAlteracao() {
		return orgaoPagadorAlteracao;
	}

	/**
	 * Set: orgaoPagadorAlteracao.
	 *
	 * @param orgaoPagadorAlteracao the orgao pagador alteracao
	 */
	public void setOrgaoPagadorAlteracao(
			OrgaoPagadorEntradaDTO orgaoPagadorAlteracao) {
		this.orgaoPagadorAlteracao = orgaoPagadorAlteracao;
	}

	/**
	 * Get: detalheSaidaDTO.
	 *
	 * @return detalheSaidaDTO
	 */
	public OrgaoPagadorDetalheSaidaDTO getDetalheSaidaDTO() {
		return detalheSaidaDTO;
	}

	/**
	 * Set: detalheSaidaDTO.
	 *
	 * @param detalheSaidaDTO the detalhe saida dto
	 */
	public void setDetalheSaidaDTO(OrgaoPagadorDetalheSaidaDTO detalheSaidaDTO) {
		this.detalheSaidaDTO = detalheSaidaDTO;
	}

	/**
	 * Get: listaTipoUnidade.
	 *
	 * @return listaTipoUnidade
	 */
	public List<TipoUnidadeOrganizacionalDTO> getListaTipoUnidade() {
		return listaTipoUnidade;
	}

	/**
	 * Get: orgaoPagadorBloqueio.
	 *
	 * @return orgaoPagadorBloqueio
	 */
	public OrgaoPagadorEntradaDTO getOrgaoPagadorBloqueio() {
		return orgaoPagadorBloqueio;
	}

	/**
	 * Set: orgaoPagadorBloqueio.
	 *
	 * @param orgaoPagadorBloqueio the orgao pagador bloqueio
	 */
	public void setOrgaoPagadorBloqueio(OrgaoPagadorEntradaDTO orgaoPagadorBloqueio) {
		this.orgaoPagadorBloqueio = orgaoPagadorBloqueio;
	}

	/**
	 * Get: listaCategoria.
	 *
	 * @return listaCategoria
	 */
	public List<SelectItem> getListaCategoria() {
		return listaCategoria;
	}

	/**
	 * Set: listaCategoria.
	 *
	 * @param listaCategoria the lista categoria
	 */
	public void setListaCategoria(List<SelectItem> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

}
