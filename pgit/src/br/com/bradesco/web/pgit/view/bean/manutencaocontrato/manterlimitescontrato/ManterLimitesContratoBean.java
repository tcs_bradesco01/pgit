/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterlimitescontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterlimitescontrato;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.IManterLimitesContratoService;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimDIaOpePgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimDIaOpePgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimiteIntraPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.AlterarLimiteIntraPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimDiarioUtilizadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimDiarioUtilizadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteHistoricoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteHistoricoOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteHistoricoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteIntraPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarLimiteIntraPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarListaServLimDiaIndvEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterlimitescontrato.bean.ConsultarListaServLimDiaIndvSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ManterLimitesContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterLimitesContratoBean {

	/** Atributo manterLimitesContratoServiceImpl. */
	private IManterLimitesContratoService manterLimitesContratoServiceImpl;
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;

	/* CABE�ALHO */
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;
	
	/** Atributo cpfCnpjMaster. */
	private String cpfCnpjMaster;
	
	/** Atributo nomeRazaoSocialMaster. */
	private String nomeRazaoSocialMaster;
	
	/** Atributo grupoEconomicoMaster. */
	private String grupoEconomicoMaster;
	
	/** Atributo atividadeEconomicaMaster. */
	private String atividadeEconomicaMaster;
	
	/** Atributo segmentoMaster. */
	private String segmentoMaster;
	
	/** Atributo subSegmentoMaster. */
	private String subSegmentoMaster;
	
	/** Atributo empresa. */
	private String empresa;
	
	/** Atributo tipo. */
	private String tipo;
	
	/** Atributo numero. */
	private String numero;
	
	/** Atributo situacaoDesc. */
	private String situacaoDesc;
	
	/** Atributo motivoDesc. */
	private String motivoDesc;
	
	/** Atributo participacao. */
	private String participacao;
	
	/** Atributo dsDescricaoContrato. */
	private String dsDescricaoContrato;
	
	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;
	
	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;
	
	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;
	
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/* LIMITE CONTA */
	/** Atributo rdoLimiteConta. */
	private String rdoLimiteConta;
	
	/** Atributo cdCpfCnpjFiltro. */
	private String cdCpfCnpjFiltro;
	
	/** Atributo cdFilialCnpjFiltro. */
	private String cdFilialCnpjFiltro;
	
	/** Atributo cdControleCnpjFiltro. */
	private String cdControleCnpjFiltro;
	
	/** Atributo cdCpfFiltro. */
	private String cdCpfFiltro;
	
	/** Atributo cdControleCpfFiltro. */
	private String cdControleCpfFiltro;
	
	/** Atributo bancoFiltro. */
	private String bancoFiltro;
	
	/** Atributo agenciaFiltro. */
	private String agenciaFiltro;
	
	/** Atributo contaFiltro. */
	private String contaFiltro;
	
	/** Atributo digitoFiltro. */
	private String digitoFiltro;
	
	/** Atributo tipoContaFiltro. */
	private Integer tipoContaFiltro;
	
	/** Atributo listaTipoConta. */
	private List<SelectItem> listaTipoConta = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoContaHash. */
	private Map<Integer, String> listaTipoContaHash = new HashMap<Integer, String>();

	/** Atributo listaGridLimiteConta. */
	private List<ConsultarLimiteIntraPgitSaidaDTO> listaGridLimiteConta;
	
	/** Atributo listaGridLimiteConta. */
	private List<ConsultarLimiteHistoricoOcorrenciasSaidaDTO> listaGridHistoricoLimiteConta;
	
	/** Atributo itemSelecionadoLimiteConta. */
	private Integer itemSelecionadoLimiteConta;
	
	/** The item selecionado historico limite conta. */
	private Integer itemSelecionadoHistoricoLimiteConta;
	
	/** Atributo listaGridControleLimiteConta. */
	private List<SelectItem> listaGridControleLimiteConta = new ArrayList<SelectItem>();
	
	/** The lista radio historico limite conta. */
	private List<SelectItem> listaRadioHistoricoLimiteConta = new ArrayList<SelectItem>();

	/** Atributo bancoDesc. */
	private String bancoDesc;
	
	/** Atributo agenciaDesc. */
	private String agenciaDesc;
	
	/** Atributo contaDesc. */
	private String contaDesc;
	
	/** Atributo tipoDesc. */
	private String tipoDesc;
	
	/** Atributo cpfCnpjDesc. */
	private String cpfCnpjDesc;
	
	/** Atributo razaoSocialDesc. */
	private String razaoSocialDesc;

	/** Atributo valorLimite. */
	private BigDecimal valorLimite;
	
	/** Atributo dataPrazoInicio. */
	private Date dataPrazoInicio;
	
	/** Atributo dataPrazoFim. */
	private Date dataPrazoFim;
	
	/** Atributo valorLimiteTED. */
	private BigDecimal valorLimiteTED;
	
	/** Atributo dataPrazoInicioTED. */
	private Date dataPrazoInicioTED;
	
	/** Atributo dataPrazoFimTED. */
	private Date dataPrazoFimTED;
	
	/** Atributo prazoValidadeAltDesc. */
	private String prazoValidadeAltDesc;
	
	/** Atributo prazoValidadeTEDAltDesc. */
	private String prazoValidadeTEDAltDesc;
	
	/** Atributo habilitaPainelDatas1. */
	private Boolean habilitaPainelDatas1;
	
	/** Atributo habilitaPainelDatas2. */
	private Boolean habilitaPainelDatas2;
	
	/** Atributo habilitaPainelDatasTED1. */
	private Boolean habilitaPainelDatasTED1;
	
	/** Atributo habilitaPainelDatasTED2. */
	private Boolean habilitaPainelDatasTED2;

	/* LIMITE MODALIDADE */
	/** Atributo listaGridLimiteModalidade. */
	private List<ConsultarListaServLimDiaIndvSaidaDTO> listaGridLimiteModalidade;
	
	/** Atributo itemSelecionadoLimiteModalidade. */
	private Integer itemSelecionadoLimiteModalidade;
	
	/** Atributo listaGridControleLimiteModalidade. */
	private List<SelectItem> listaGridControleLimiteModalidade = new ArrayList<SelectItem>();

	/** Atributo tipoServicoDesc. */
	private String tipoServicoDesc;
	
	/** Atributo modalidadeDesc. */
	private String modalidadeDesc;
	
	/** Atributo valorLimiteIndividual. */
	private BigDecimal valorLimiteIndividual;
	
	/** Atributo valorLimiteDiario. */
	private BigDecimal valorLimiteDiario;

	/** Atributo listaGridLimiteUtilizado. */
	private List<ConsultarLimDiarioUtilizadoSaidaDTO> listaGridLimiteUtilizado;
	
	/** The consultar limite conta saida. */
	private ConsultarLimiteContaSaidaDTO consultarLimiteContaSaida = new ConsultarLimiteContaSaidaDTO();

	private ConsultarLimiteHistoricoSaidaDTO consultarLimiteHistoricoSaida = new ConsultarLimiteHistoricoSaidaDTO();
	
	/** The item selecionado grid limite conta. */
	private ConsultarLimiteIntraPgitSaidaDTO itemSelecionadoGridLimiteConta;

	private ConsultarLimiteHistoricoOcorrenciasSaidaDTO itemSelecionadoGridHistorico;
	
	private Date dataManutencaoInicio;
	
	private Date dataManutencaoFim;

	

	/* M�TODOS */
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
		this.identificacaoClienteContratoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean
				.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean
				.setPaginaCliente("identificacaoClienteManLimContrato");
		identificacaoClienteContratoBean
				.setPaginaRetorno("conManterLimitesContrato");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
	}

	public String limparHistorico() {
	    
	    setDataManutencaoInicio(new Date());
        setDataManutencaoFim(new Date());
        
        listaGridHistoricoLimiteConta = new ArrayList<ConsultarLimiteHistoricoOcorrenciasSaidaDTO>();
        
        return "HISTORICO_LIMITE_CONTA";
	    
	}
	
	/**
	 * Carrega cabecalho.
	 */
	public void carregaCabecalho() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(
						identificacaoClienteContratoBean
								.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " +  saidaDTO.getDescFuncionarioBradesco());

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		
		} else {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		}

		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setTipo(saidaDTO.getDsTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setDsDescricaoContrato(saidaDTO.getDsContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
	}

	/**
	 * Limite conta.
	 *
	 * @return the string
	 */
	public String limiteConta() {
		carregaCabecalho();
		carregaListaTipoConta();
		limparDadosLimiteConta();
		return "LIMITE_CONTA";
	}

	/**
	 * Carrega lista tipo conta.
	 */
	public void carregaListaTipoConta() {
		listaTipoConta = new ArrayList<SelectItem>();
		List<ListarTipoContaSaidaDTO> listaSaida = new ArrayList<ListarTipoContaSaidaDTO>();
		listaSaida = this.getComboService().listarTipoConta();

		listaTipoContaHash.clear();

		for (ListarTipoContaSaidaDTO combo : listaSaida) {
			listaTipoContaHash.put(combo.getCdTipoConta(), combo
					.getDsTipoConta());
			this.listaTipoConta.add(new SelectItem(combo.getCdTipoConta(),
					combo.getDsTipoConta()));
		}

	}

	/**
	 * Limpar argumentos limite conta.
	 */
	public void limparArgumentosLimiteConta() {
		setCdCpfCnpjFiltro(null);
		setCdFilialCnpjFiltro(null);
		setCdControleCnpjFiltro(null);
		setCdCpfFiltro(null);
		setCdControleCpfFiltro(null);
		
		if("2".equals(getRdoLimiteConta())){
		    setBancoFiltro("237");
		}else{
		    setBancoFiltro(null);
		}
		
		setAgenciaFiltro(null);
		setContaFiltro(null);
		setDigitoFiltro(null);
		setTipoContaFiltro(null);
	}

	/**
	 * Limpar dados limite conta.
	 */
	public void limparDadosLimiteConta() {
		setRdoLimiteConta("");
		limparArgumentosLimiteConta();
		limparLimiteConta();
	}

	/**
	 * Consultar limite conta.
	 */
	public void consultarLimiteConta() {
		try {
			ListarContratosPgitSaidaDTO itemSelecionado = identificacaoClienteContratoBean
					.getListaGridPesquisa().get(
							identificacaoClienteContratoBean
									.getItemSelecionadoLista());
			listaGridLimiteConta = new ArrayList<ConsultarLimiteIntraPgitSaidaDTO>();
			ConsultarLimiteIntraPgitEntradaDTO entrada = new ConsultarLimiteIntraPgitEntradaDTO();

			entrada.setCdPessoaJuridicaNegocio(itemSelecionado
					.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(itemSelecionado
					.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(itemSelecionado
					.getNrSequenciaContrato());

			if (getRdoLimiteConta() != null) {
				if (getRdoLimiteConta().equals("0")) {
					entrada.setCdCorpoCpfCnpj(Long
							.parseLong(getCdCpfCnpjFiltro()));
					entrada.setCdControleCpfCnpj(Integer
							.parseInt(getCdFilialCnpjFiltro()));
					entrada.setCdDigitoCpfCnpj(Integer
							.parseInt(getCdControleCnpjFiltro()));
				} else {
					if (getRdoLimiteConta().equals("1")) {
						entrada.setCdCorpoCpfCnpj(Long
								.parseLong(getCdCpfFiltro()));
						entrada.setCdControleCpfCnpj(0);
						entrada.setCdDigitoCpfCnpj(Integer
								.parseInt(getCdControleCpfFiltro()));
					} else {
						entrada.setCdCorpoCpfCnpj(0L);
						entrada.setCdControleCpfCnpj(0);
						entrada.setCdDigitoCpfCnpj(0);
					}

				}
			} else {
				entrada.setCdCorpoCpfCnpj(0L);
				entrada.setCdControleCpfCnpj(0);
				entrada.setCdDigitoCpfCnpj(0);
			}

			entrada.setCdFinalidade(0);
			entrada.setCdBanco(getBancoFiltro() != null
					&& !getBancoFiltro().equals("") ? Integer
					.parseInt(getBancoFiltro()) : 0);
			entrada.setCdAgencia(getAgenciaFiltro() != null
					&& !getAgenciaFiltro().equals("") ? Integer
					.parseInt(getAgenciaFiltro()) : 0);
			entrada.setCdDigitoAgencia(0);
			entrada.setCdConta(getContaFiltro() != null
					&& !getContaFiltro().equals("") ? Long
					.parseLong(getContaFiltro()) : 0L);
			entrada.setCdDigitoConta(getDigitoFiltro());
			entrada.setCdTipoConta(getTipoContaFiltro());

			setListaGridLimiteConta(getManterLimitesContratoServiceImpl()
					.consultarLimiteIntraPgit(entrada));

			listaGridControleLimiteConta = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridLimiteConta().size(); i++) {
				listaGridControleLimiteConta.add(new SelectItem(i, ""));
			}

			setItemSelecionadoLimiteConta(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridLimiteConta(null);
			setItemSelecionadoLimiteConta(null);
		}
	}
	
	public String detalharLimiteConta(){
		ConsultarLimiteContaEntradaDTO entrada = new ConsultarLimiteContaEntradaDTO();
		itemSelecionadoGridLimiteConta = new ConsultarLimiteIntraPgitSaidaDTO();
		
		try{
			ListarContratosPgitSaidaDTO itemSelecionadoGridPesquisa = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
			
			itemSelecionadoGridLimiteConta = getListaGridLimiteConta()
				.get(getItemSelecionadoLimiteConta());
			
			entrada.setCdpessoaJuridicaContrato(itemSelecionadoGridPesquisa.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(itemSelecionadoGridPesquisa.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(itemSelecionadoGridPesquisa.getNrSequenciaContrato());
			
			entrada.setCdPessoaJuridicaVinculo(itemSelecionadoGridLimiteConta.getCdPessoaJuridicaVinculo());
			entrada.setCdTipoContratoVinculo(itemSelecionadoGridLimiteConta.getCdTipoContratoVinculo());
			entrada.setNrSequenciaContratoVinculo(itemSelecionadoGridLimiteConta.getNrSequenciaContratoVinculo());
			entrada.setCdTipoVincContrato(itemSelecionadoGridLimiteConta.getCdTipoVinculoContrato());
					
			setConsultarLimiteContaSaida(getManterLimitesContratoServiceImpl().consultarLimiteConta(entrada));
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " 
					+ p.getMessage(), false);
			return "";
		}
		return "DETALHAR_LIMITE_CONTA";
	}
	
	public String historicoLimiteConta(){
	    setDataManutencaoInicio(new Date());
	    setDataManutencaoFim(new Date());
	    
	    listaGridHistoricoLimiteConta = new ArrayList<ConsultarLimiteHistoricoOcorrenciasSaidaDTO>();
        
		return "HISTORICO_LIMITE_CONTA";
	}
	
	public String consultarListoricoLimiteConta(){
		ConsultarLimiteHistoricoEntradaDTO entrada = new ConsultarLimiteHistoricoEntradaDTO();
		
		try{
			ListarContratosPgitSaidaDTO itemSelecionadoGridPesquisa = identificacaoClienteContratoBean
				.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
						
			entrada.setCdpessoaJuridicaContrato(itemSelecionadoGridPesquisa.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(itemSelecionadoGridPesquisa.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(itemSelecionadoGridPesquisa.getNrSequenciaContrato());
			
			entrada.setCdPessoaJuridicaVinculo(itemSelecionadoGridLimiteConta.getCdPessoaJuridicaVinculo());
			entrada.setCdTipoContratoVinculo(itemSelecionadoGridLimiteConta.getCdTipoContratoVinculo());
			entrada.setNrSequenciaContratoVinculo(itemSelecionadoGridLimiteConta.getNrSequenciaContratoVinculo());
			entrada.setCdTipoVincContrato(itemSelecionadoGridLimiteConta.getCdTipoVinculoContrato());
			
			entrada.setDtInicioManutencao(FormatarData.formataDiaMesAno(getDataManutencaoInicio()));
	        entrada.setDtFimManutencao(FormatarData.formataDiaMesAno(getDataManutencaoFim()));
					
			setConsultarLimiteHistoricoSaida(getManterLimitesContratoServiceImpl().consultarLimiteHistorico(entrada));
			
			setListaGridHistoricoLimiteConta(getConsultarLimiteHistoricoSaida().getOcorrencias());
			
			listaRadioHistoricoLimiteConta = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridHistoricoLimiteConta().size(); i++) {
				listaRadioHistoricoLimiteConta.add(new SelectItem(i, ""));
			}

			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " 
					+ p.getMessage(), false);
		}
		return "";
	}
	
	public String detalharHistoricoLimiteConta(){
		itemSelecionadoGridHistorico = new ConsultarLimiteHistoricoOcorrenciasSaidaDTO();
		itemSelecionadoGridHistorico = getListaGridHistoricoLimiteConta()
		.get(getItemSelecionadoHistoricoLimiteConta());
		
	
		
		return "DETALHAR_HISTORICO_LIMITE_CONTA";
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt) {
		consultarLimiteConta();
	}

	/**
	 * Limpar limite conta.
	 */
	public void limparLimiteConta() {
		setListaGridLimiteConta(null);
		setItemSelecionadoLimiteConta(null);
	}

	/**
	 * Alterar limite conta.
	 *
	 * @return the string
	 */
	public String alterarLimiteConta() {
		ConsultarLimiteIntraPgitSaidaDTO itemSelecionado = getListaGridLimiteConta()
				.get(getItemSelecionadoLimiteConta());

		setBancoDesc(itemSelecionado.getBancoFormatado());
		setAgenciaDesc(itemSelecionado.getAgenciaFormatada());
		setContaDesc(itemSelecionado.getContaFormatada());
		setTipoDesc(itemSelecionado.getDsTipoConta());
		setCpfCnpjDesc(itemSelecionado.getCdCpfCnpj());
		setRazaoSocialDesc(itemSelecionado.getNmParticipante());

		if (PgitUtil
				.verificaZero(itemSelecionado.getVlAdicionalContratoConta()) == null) {
			setValorLimite(null);
			setDataPrazoInicio(null);
			setDataPrazoFim(null);
			setHabilitaPainelDatas1(false);
			setHabilitaPainelDatas2(true);
		} else {
			setValorLimite(itemSelecionado.getVlAdicionalContratoConta());
			setDataPrazoInicio(FormatarData
					.formataDiaMesAnoFromPdc(itemSelecionado
							.getDtInicioValorAdicional()));
			setDataPrazoFim(FormatarData
					.formataDiaMesAnoFromPdc(itemSelecionado
							.getDtFimValorAdicional()));
			setHabilitaPainelDatas1(true);
			setHabilitaPainelDatas2(false);
		}
		
		if (PgitUtil
				.verificaZero(itemSelecionado.getVlAdcContrCtaTed()) == null) {
			setValorLimiteTED(null);
			setDataPrazoInicioTED(null);
			setDataPrazoFimTED(null);
			setHabilitaPainelDatasTED1(false);
			setHabilitaPainelDatasTED2(true);
		} else {
			setValorLimiteTED(itemSelecionado.getVlAdcContrCtaTed());
			setDataPrazoInicioTED(FormatarData
					.formataDiaMesAnoFromPdc(itemSelecionado
							.getDtIniVlrAdicionalTed()));
			setDataPrazoFimTED(FormatarData
					.formataDiaMesAnoFromPdc(itemSelecionado
							.getDtFimVlrAdicionalTed()));
			setHabilitaPainelDatasTED1(true);
			setHabilitaPainelDatasTED2(false);
		}

		return "ALTERAR_LIMITE_CONTA";
	}

	/**
	 * Voltar limite conta.
	 *
	 * @return the string
	 */
	public String voltarLimiteConta() {
		setItemSelecionadoLimiteConta(null);
		return "VOLTAR";
	}

	/**
	 * Avancar alterar conta.
	 *
	 * @return the string
	 */
	public String avancarAlterarConta() {
		if (getValorLimite() == null) {
			setValorLimite(BigDecimal.ZERO);
		}
		setPrazoValidadeAltDesc(PgitUtil.concatenarCampos(FormatarData
				.formataDiaMesAno(getDataPrazoInicio()), FormatarData
				.formataDiaMesAno(getDataPrazoFim())));
		
		if (getValorLimiteTED() == null) {
			setValorLimiteTED(BigDecimal.ZERO);
		}
		setPrazoValidadeTEDAltDesc(PgitUtil.concatenarCampos(FormatarData
				.formataDiaMesAno(getDataPrazoInicioTED()), FormatarData
				.formataDiaMesAno(getDataPrazoFimTED())));

		return "AVANCAR_ALTERAR_CONTA";
	}

	/**
	 * Voltar alterar conta.
	 *
	 * @return the string
	 */
	public String voltarAlterarConta() {
		return "VOLTAR";
	}

	/**
	 * Confirmar alterar conta.
	 *
	 * @return the string
	 */
	public String confirmarAlterarConta() {
		try {
			ConsultarLimiteIntraPgitSaidaDTO itemSelecionadoConta = getListaGridLimiteConta()
					.get(getItemSelecionadoLimiteConta());
			ListarContratosPgitSaidaDTO itemSelecionado = identificacaoClienteContratoBean
					.getListaGridPesquisa().get(
							identificacaoClienteContratoBean
									.getItemSelecionadoLista());
			AlterarLimiteIntraPgitEntradaDTO entrada = new AlterarLimiteIntraPgitEntradaDTO();

			entrada.setCdpessoaJuridicaContrato(itemSelecionado.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(itemSelecionado.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(itemSelecionado.getNrSequenciaContrato());
			entrada.setCdPessoaJuridicaVinculo(itemSelecionadoConta.getCdPessoaJuridicaVinculo());
			entrada.setCdTipoContratoVinculo(itemSelecionadoConta.getCdTipoContratoVinculo());
			entrada.setNrSequenciaContratoVinculo(itemSelecionadoConta.getNrSequenciaContratoVinculo());
			entrada.setCdTipoVincContrato(itemSelecionadoConta.getCdTipoVinculoContrato());
			entrada.setVlAdicionalContratoConta(getValorLimite());
			entrada.setDtInicioValorAdicional(FormatarData.formataDiaMesAnoToPdc(getDataPrazoInicio()));
			entrada.setDtFimValorAdicional(FormatarData.formataDiaMesAnoToPdc(getDataPrazoFim()));
			entrada.setVlAdicContrCtaTed(getValorLimiteTED());
			entrada.setDtIniVlrAdicionalTed(FormatarData.formataDiaMesAnoToPdc(getDataPrazoInicioTED()));
			entrada.setDtFimVlrAdicionalTed(FormatarData.formataDiaMesAnoToPdc(getDataPrazoFimTED()));
			
			AlterarLimiteIntraPgitSaidaDTO saida = getManterLimitesContratoServiceImpl()
					.alterarLimiteIntraPgit(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(), "conLimiteConta",
					BradescoViewExceptionActionType.ACTION, false);
			consultarLimiteConta();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
		return "";
	}

	/**
	 * Voltar pesquisa.
	 *
	 * @return the string
	 */
	public String voltarPesquisa() {
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "VOLTAR";
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		return "VOLTAR";
	}

	/**
	 * Limite modalidade.
	 *
	 * @return the string
	 */
	public String limiteModalidade() {
		carregaCabecalho();
		return consultarLimiteModalidade();
	}

	/**
	 * Consultar limite modalidade.
	 *
	 * @return the string
	 */
	public String consultarLimiteModalidade() {
		try {
			ListarContratosPgitSaidaDTO itemSelecionado = identificacaoClienteContratoBean
					.getListaGridPesquisa().get(
							identificacaoClienteContratoBean
									.getItemSelecionadoLista());
			listaGridLimiteModalidade = new ArrayList<ConsultarListaServLimDiaIndvSaidaDTO>();
			ConsultarListaServLimDiaIndvEntradaDTO entrada = new ConsultarListaServLimDiaIndvEntradaDTO();

			entrada.setCdpessoaJuridicaContrato(itemSelecionado
					.getCdPessoaJuridica());
			entrada.setCdTipoContrato(itemSelecionado.getCdTipoContrato());
			entrada.setNrSequenciaContrato(itemSelecionado
					.getNrSequenciaContrato());

			entrada.setCdProdutoServicoOperacao(0);
			entrada.setCdProdutoOperacaoRelacionado(0);

			setListaGridLimiteModalidade(getManterLimitesContratoServiceImpl()
					.consultarListaServLimDiaIndv(entrada));

			listaGridControleLimiteModalidade = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridLimiteModalidade().size(); i++) {
				listaGridControleLimiteModalidade.add(new SelectItem(i, ""));
			}

			setItemSelecionadoLimiteModalidade(null);

			return "LIMITE_MODALIDADE";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridLimiteModalidade(null);
			setItemSelecionadoLimiteModalidade(null);

			return "";
		}
	}

	/**
	 * Pesquisar modalidade.
	 *
	 * @param evt the evt
	 */
	public void pesquisarModalidade(ActionEvent evt) {
		consultarLimiteModalidade();
	}

	/**
	 * Limpar limite modalidade.
	 */
	public void limparLimiteModalidade() {
		setItemSelecionadoLimiteModalidade(null);
	}

	/**
	 * Alterar limite modalidade.
	 *
	 * @return the string
	 */
	public String alterarLimiteModalidade() {
		ConsultarListaServLimDiaIndvSaidaDTO itemSelecionado = getListaGridLimiteModalidade()
				.get(getItemSelecionadoLimiteModalidade());

		setTipoServicoDesc(itemSelecionado.getDsServico());
		setModalidadeDesc(itemSelecionado.getDsMadalidade());

		if (PgitUtil.verificaZero(itemSelecionado.getVlLimiteDiaPagamento()) == null) {
			setValorLimiteDiario(null);
		} else {
			setValorLimiteDiario(itemSelecionado.getVlLimiteDiaPagamento());
		}

		if (PgitUtil.verificaZero(itemSelecionado.getVlLimiteIndvdPagamento()) == null) {
			setValorLimiteIndividual(null);
		} else {
			setValorLimiteIndividual(itemSelecionado
					.getVlLimiteIndvdPagamento());
		}

		return "ALTERAR_MODALIDADE";
	}

	/**
	 * Voltar limite modalidade.
	 *
	 * @return the string
	 */
	public String voltarLimiteModalidade() {
		setItemSelecionadoLimiteModalidade(null);
		return "VOLTAR";
	}

	/**
	 * Avancar alterar modalidade.
	 *
	 * @return the string
	 */
	public String avancarAlterarModalidade() {
		if (this.getValorLimiteIndividual() == null) {
			this.setValorLimiteIndividual(BigDecimal.ZERO);
		}

		if (this.getValorLimiteDiario() == null) {
			this.setValorLimiteDiario(BigDecimal.ZERO);
		}

		return "AVANCAR_ALTERAR_MODALIDADE";
	}

	/**
	 * Voltar alterar modalidade.
	 *
	 * @return the string
	 */
	public String voltarAlterarModalidade() {
		return "VOLTAR";
	}

	/**
	 * Confirmar alterar modalidade.
	 *
	 * @return the string
	 */
	public String confirmarAlterarModalidade() {
		try {
			ConsultarListaServLimDiaIndvSaidaDTO itemSelecionadoModalidade = getListaGridLimiteModalidade()
					.get(getItemSelecionadoLimiteModalidade());
			ListarContratosPgitSaidaDTO itemSelecionado = identificacaoClienteContratoBean
					.getListaGridPesquisa().get(
							identificacaoClienteContratoBean
									.getItemSelecionadoLista());
			AlterarLimDIaOpePgitEntradaDTO entrada = new AlterarLimDIaOpePgitEntradaDTO();

			entrada.setCdpessoaJuridicaContrato(itemSelecionado
					.getCdPessoaJuridica());
			entrada.setCdTipoContratoNegocio(itemSelecionado
					.getCdTipoContrato());
			entrada.setNrSequenciaContratoNegocio(itemSelecionado
					.getNrSequenciaContrato());
			entrada.setCdProdutoServicoOperacao(itemSelecionadoModalidade
					.getCdProdutoServicoOperacao());
			entrada.setCdProdutoOperacaoRelacionado(itemSelecionadoModalidade
					.getCdProdutoOperacaoRelacionado());
			entrada.setVlLimiteDiaPagamento(getValorLimiteDiario());
			entrada.setVlLimiteIndvdPagamento(getValorLimiteIndividual());

			entrada.setCdusuario("");
			entrada.setCdUsuarioExterno("");
			entrada.setCdCanal(0);
			entrada.setNmOperacaoFluxo("");
			entrada.setCdEmpresaOperante(0L);
			entrada.setCdDependenteOperante(0);

			AlterarLimDIaOpePgitSaidaDTO saida = getManterLimitesContratoServiceImpl()
					.alterarLimDIaOpePgit(entrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(), "conLimiteModalidade",
					BradescoViewExceptionActionType.ACTION, false);

			consultarLimiteModalidade();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
		return "";
	}

	/**
	 * Limite utilizado modalidade.
	 *
	 * @return the string
	 */
	public String limiteUtilizadoModalidade() {
		ConsultarListaServLimDiaIndvSaidaDTO itemSelecionado = getListaGridLimiteModalidade()
				.get(getItemSelecionadoLimiteModalidade());

		setTipoServicoDesc(itemSelecionado.getDsServico());
		setModalidadeDesc(itemSelecionado.getDsMadalidade());

		return consultarLimiteUtilizado();
	}

	/**
	 * Consultar limite utilizado.
	 *
	 * @return the string
	 */
	public String consultarLimiteUtilizado() {
		try {
			ConsultarListaServLimDiaIndvSaidaDTO itemSelecionadoModalidade = getListaGridLimiteModalidade()
					.get(getItemSelecionadoLimiteModalidade());
			ListarContratosPgitSaidaDTO itemSelecionado = identificacaoClienteContratoBean
					.getListaGridPesquisa().get(
							identificacaoClienteContratoBean
									.getItemSelecionadoLista());
			ConsultarLimDiarioUtilizadoEntradaDTO entrada = new ConsultarLimDiarioUtilizadoEntradaDTO();

			entrada.setCdpessoaJuridicaContrato(itemSelecionado
					.getCdPessoaJuridica());
			entrada.setCdTipoContrato(itemSelecionado.getCdTipoContrato());
			entrada.setNrSequenciaContrato(itemSelecionado
					.getNrSequenciaContrato());
			entrada.setCdProdutoServicoOperacao(itemSelecionadoModalidade
					.getCdProdutoServicoOperacao());
			entrada.setCdProdutoOperacaoRelacionado(itemSelecionadoModalidade
					.getCdProdutoOperacaoRelacionado());

			setListaGridLimiteUtilizado(getManterLimitesContratoServiceImpl()
					.consultarLimDiarioUtilizado(entrada));

			return "LIMITE_UTILIZADO";
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridLimiteUtilizado(null);

			return "";
		}

	}

	/**
	 * Desabilita campos datas.
	 */
	public void desabilitaCamposDatas() {
		if (getValorLimite() == null
				|| getValorLimite().compareTo(BigDecimal.ZERO) == 0) {
			setDataPrazoInicio(null);
			setDataPrazoFim(null);
			setHabilitaPainelDatas1(false);
			setHabilitaPainelDatas2(true);
		} else {
			ConsultarLimiteIntraPgitSaidaDTO itemSelecionado = getListaGridLimiteConta()
					.get(getItemSelecionadoLimiteConta());

			String dataAtual = FormatarData.formataDiaMesAnoToPdc(new Date());

			if (itemSelecionado.getDtInicioValorAdicional() != null
					&& !itemSelecionado.getDtInicioValorAdicional().contains(
							" ")
					&& !itemSelecionado.getDtInicioValorAdicional().equals("")) {
				setDataPrazoInicio(FormatarData
						.formataDiaMesAnoFromPdc(itemSelecionado
								.getDtInicioValorAdicional()));
			} else {
				setDataPrazoInicio(FormatarData
						.formataDiaMesAnoFromPdc(dataAtual));
			}

			if (itemSelecionado.getDtFimValorAdicional() != null
					&& !itemSelecionado.getDtFimValorAdicional().contains(" ")
					&& !itemSelecionado.getDtFimValorAdicional().equals("")) {
				setDataPrazoFim(FormatarData
						.formataDiaMesAnoFromPdc(itemSelecionado
								.getDtFimValorAdicional()));
			} else {
				setDataPrazoFim(FormatarData.formataDiaMesAnoFromPdc(dataAtual));
			}

			setHabilitaPainelDatas1(true);
			setHabilitaPainelDatas2(false);
		}
	}
	
	/**
	 * Desabilita campos datas ted.
	 */
	public void desabilitaCamposDatasTED() {
		if (getValorLimiteTED() == null
				|| getValorLimiteTED().compareTo(BigDecimal.ZERO) == 0) {
			setDataPrazoInicioTED(null);
			setDataPrazoFimTED(null);
			setHabilitaPainelDatasTED1(false);
			setHabilitaPainelDatasTED2(true);
		} else {
			ConsultarLimiteIntraPgitSaidaDTO itemSelecionado = getListaGridLimiteConta()
					.get(getItemSelecionadoLimiteConta());

			String dataAtual = FormatarData.formataDiaMesAnoToPdc(new Date());

			if (itemSelecionado.getDtIniVlrAdicionalTed() != null
					&& !itemSelecionado.getDtIniVlrAdicionalTed().contains(
							" ")
					&& !itemSelecionado.getDtIniVlrAdicionalTed().equals("")) {
				setDataPrazoInicioTED(FormatarData
						.formataDiaMesAnoFromPdc(itemSelecionado
								.getDtIniVlrAdicionalTed()));
			} else {
				setDataPrazoInicioTED(FormatarData
						.formataDiaMesAnoFromPdc(dataAtual));
			}

			if (itemSelecionado.getDtFimVlrAdicionalTed() != null
					&& !itemSelecionado.getDtFimVlrAdicionalTed().contains(" ")
					&& !itemSelecionado.getDtFimVlrAdicionalTed().equals("")) {
				setDataPrazoFimTED(FormatarData
						.formataDiaMesAnoFromPdc(itemSelecionado
								.getDtFimVlrAdicionalTed()));
			} else {
				setDataPrazoFimTED(FormatarData.formataDiaMesAnoFromPdc(dataAtual));
			}

			setHabilitaPainelDatasTED1(true);
			setHabilitaPainelDatasTED2(false);
		}
	}

	/**
	 * Pesquisar limite utilizado.
	 *
	 * @param evt the evt
	 */
	public void pesquisarLimiteUtilizado(ActionEvent evt) {
		consultarLimiteUtilizado();
	}

	/* Getters e Setters */
	/**
	 * Get: atividadeEconomicaMaster.
	 *
	 * @return atividadeEconomicaMaster
	 */
	public String getAtividadeEconomicaMaster() {
		return atividadeEconomicaMaster;
	}

	/**
	 * Set: atividadeEconomicaMaster.
	 *
	 * @param atividadeEconomicaMaster the atividade economica master
	 */
	public void setAtividadeEconomicaMaster(String atividadeEconomicaMaster) {
		this.atividadeEconomicaMaster = atividadeEconomicaMaster;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: cpfCnpjMaster.
	 *
	 * @return cpfCnpjMaster
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}

	/**
	 * Set: cpfCnpjMaster.
	 *
	 * @param cpfCnpjMaster the cpf cnpj master
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}

	/**
	 * Get: dsAgenciaGestora.
  	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: dsDescricaoContrato.
	 *
	 * @return dsDescricaoContrato
	 */
	public String getDsDescricaoContrato() {
		return dsDescricaoContrato;
	}

	/**
	 * Set: dsDescricaoContrato.
	 *
	 * @param dsDescricaoContrato the ds descricao contrato
	 */
	public void setDsDescricaoContrato(String dsDescricaoContrato) {
		this.dsDescricaoContrato = dsDescricaoContrato;
	}

	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Get: grupoEconomicoMaster.
	 *
	 * @return grupoEconomicoMaster
	 */
	public String getGrupoEconomicoMaster() {
		return grupoEconomicoMaster;
	}

	/**
	 * Set: grupoEconomicoMaster.
	 *
	 * @param grupoEconomicoMaster the grupo economico master
	 */
	public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
		this.grupoEconomicoMaster = grupoEconomicoMaster;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: motivoDesc.
	 *
	 * @return motivoDesc
	 */
	public String getMotivoDesc() {
		return motivoDesc;
	}

	/**
	 * Set: motivoDesc.
	 *
	 * @param motivoDesc the motivo desc
	 */
	public void setMotivoDesc(String motivoDesc) {
		this.motivoDesc = motivoDesc;
	}

	/**
	 * Get: nomeRazaoSocialMaster.
	 *
	 * @return nomeRazaoSocialMaster
	 */
	public String getNomeRazaoSocialMaster() {
		return nomeRazaoSocialMaster;
	}

	/**
	 * Set: nomeRazaoSocialMaster.
	 *
	 * @param nomeRazaoSocialMaster the nome razao social master
	 */
	public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
		this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
	}

	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Get: participacao.
	 *
	 * @return participacao
	 */
	public String getParticipacao() {
		return participacao;
	}

	/**
	 * Set: participacao.
	 *
	 * @param participacao the participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	/**
	 * Get: segmentoMaster.
	 *
	 * @return segmentoMaster
	 */
	public String getSegmentoMaster() {
		return segmentoMaster;
	}

	/**
	 * Set: segmentoMaster.
	 *
	 * @param segmentoMaster the segmento master
	 */
	public void setSegmentoMaster(String segmentoMaster) {
		this.segmentoMaster = segmentoMaster;
	}

	/**
	 * Get: situacaoDesc.
	 *
	 * @return situacaoDesc
	 */
	public String getSituacaoDesc() {
		return situacaoDesc;
	}

	/**
	 * Set: situacaoDesc.
	 *
	 * @param situacaoDesc the situacao desc
	 */
	public void setSituacaoDesc(String situacaoDesc) {
		this.situacaoDesc = situacaoDesc;
	}

	/**
	 * Get: subSegmentoMaster.
	 *
	 * @return subSegmentoMaster
	 */
	public String getSubSegmentoMaster() {
		return subSegmentoMaster;
	}

	/**
	 * Set: subSegmentoMaster.
	 *
	 * @param subSegmentoMaster the sub segmento master
	 */
	public void setSubSegmentoMaster(String subSegmentoMaster) {
		this.subSegmentoMaster = subSegmentoMaster;
	}

	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: agenciaFiltro.
	 *
	 * @return agenciaFiltro
	 */
	public String getAgenciaFiltro() {
		return agenciaFiltro;
	}

	/**
	 * Set: agenciaFiltro.
	 *
	 * @param agenciaFiltro the agencia filtro
	 */
	public void setAgenciaFiltro(String agenciaFiltro) {
		this.agenciaFiltro = agenciaFiltro;
	}

	/**
	 * Get: bancoFiltro.
	 *
	 * @return bancoFiltro
	 */
	public String getBancoFiltro() {
		return bancoFiltro;
	}

	/**
	 * Set: bancoFiltro.
	 *
	 * @param bancoFiltro the banco filtro
	 */
	public void setBancoFiltro(String bancoFiltro) {
		this.bancoFiltro = bancoFiltro;
	}

	/**
	 * Get: cdControleCnpjFiltro.
	 *
	 * @return cdControleCnpjFiltro
	 */
	public String getCdControleCnpjFiltro() {
		return cdControleCnpjFiltro;
	}

	/**
	 * Set: cdControleCnpjFiltro.
	 *
	 * @param cdControleCnpjFiltro the cd controle cnpj filtro
	 */
	public void setCdControleCnpjFiltro(String cdControleCnpjFiltro) {
		this.cdControleCnpjFiltro = cdControleCnpjFiltro;
	}

	/**
	 * Get: cdControleCpfFiltro.
	 *
	 * @return cdControleCpfFiltro
	 */
	public String getCdControleCpfFiltro() {
		return cdControleCpfFiltro;
	}

	/**
	 * Set: cdControleCpfFiltro.
	 *
	 * @param cdControleCpfFiltro the cd controle cpf filtro
	 */
	public void setCdControleCpfFiltro(String cdControleCpfFiltro) {
		this.cdControleCpfFiltro = cdControleCpfFiltro;
	}

	/**
	 * Get: cdCpfCnpjFiltro.
	 *
	 * @return cdCpfCnpjFiltro
	 */
	public String getCdCpfCnpjFiltro() {
		return cdCpfCnpjFiltro;
	}

	/**
	 * Set: cdCpfCnpjFiltro.
	 *
	 * @param cdCpfCnpjFiltro the cd cpf cnpj filtro
	 */
	public void setCdCpfCnpjFiltro(String cdCpfCnpjFiltro) {
		this.cdCpfCnpjFiltro = cdCpfCnpjFiltro;
	}

	/**
	 * Get: cdCpfFiltro.
	 *
	 * @return cdCpfFiltro
	 */
	public String getCdCpfFiltro() {
		return cdCpfFiltro;
	}

	/**
	 * Set: cdCpfFiltro.
	 *
	 * @param cdCpfFiltro the cd cpf filtro
	 */
	public void setCdCpfFiltro(String cdCpfFiltro) {
		this.cdCpfFiltro = cdCpfFiltro;
	}

	/**
	 * Get: cdFilialCnpjFiltro.
	 *
	 * @return cdFilialCnpjFiltro
	 */
	public String getCdFilialCnpjFiltro() {
		return cdFilialCnpjFiltro;
	}

	/**
	 * Set: cdFilialCnpjFiltro.
	 *
	 * @param cdFilialCnpjFiltro the cd filial cnpj filtro
	 */
	public void setCdFilialCnpjFiltro(String cdFilialCnpjFiltro) {
		this.cdFilialCnpjFiltro = cdFilialCnpjFiltro;
	}

	/**
	 * Get: contaFiltro.
	 *
	 * @return contaFiltro
	 */
	public String getContaFiltro() {
		return contaFiltro;
	}

	/**
	 * Set: contaFiltro.
	 *
	 * @param contaFiltro the conta filtro
	 */
	public void setContaFiltro(String contaFiltro) {
		this.contaFiltro = contaFiltro;
	}

	/**
	 * Get: digitoFiltro.
	 *
	 * @return digitoFiltro
	 */
	public String getDigitoFiltro() {
		return digitoFiltro;
	}

	/**
	 * Set: digitoFiltro.
	 *
	 * @param digitoFiltro the digito filtro
	 */
	public void setDigitoFiltro(String digitoFiltro) {
		this.digitoFiltro = digitoFiltro;
	}

	/**
	 * Get: listaTipoConta.
	 *
	 * @return listaTipoConta
	 */
	public List<SelectItem> getListaTipoConta() {
		return listaTipoConta;
	}

	/**
	 * Set: listaTipoConta.
	 *
	 * @param listaTipoConta the lista tipo conta
	 */
	public void setListaTipoConta(List<SelectItem> listaTipoConta) {
		this.listaTipoConta = listaTipoConta;
	}

	/**
	 * Get: listaTipoContaHash.
	 *
	 * @return listaTipoContaHash
	 */
	public Map<Integer, String> getListaTipoContaHash() {
		return listaTipoContaHash;
	}

	/**
	 * Set lista tipo conta hash.
	 *
	 * @param listaTipoContaHash the lista tipo conta hash
	 */
	public void setListaTipoContaHash(Map<Integer, String> listaTipoContaHash) {
		this.listaTipoContaHash = listaTipoContaHash;
	}

	/**
	 * Get: rdoLimiteConta.
	 *
	 * @return rdoLimiteConta
	 */
	public String getRdoLimiteConta() {
		return rdoLimiteConta;
	}

	/**
	 * Set: rdoLimiteConta.
	 *
	 * @param rdoLimiteConta the rdo limite conta
	 */
	public void setRdoLimiteConta(String rdoLimiteConta) {
		this.rdoLimiteConta = rdoLimiteConta;
	}

	/**
	 * Get: tipoContaFiltro.
	 *
	 * @return tipoContaFiltro
	 */
	public Integer getTipoContaFiltro() {
		return tipoContaFiltro;
	}

	/**
	 * Set: tipoContaFiltro.
	 *
	 * @param tipoContaFiltro the tipo conta filtro
	 */
	public void setTipoContaFiltro(Integer tipoContaFiltro) {
		this.tipoContaFiltro = tipoContaFiltro;
	}

	/**
	 * Get: manterLimitesContratoServiceImpl.
	 *
	 * @return manterLimitesContratoServiceImpl
	 */
	public IManterLimitesContratoService getManterLimitesContratoServiceImpl() {
		return manterLimitesContratoServiceImpl;
	}

	/**
	 * Set: manterLimitesContratoServiceImpl.
	 *
	 * @param manterLimitesContratoServiceImpl the manter limites contrato service impl
	 */
	public void setManterLimitesContratoServiceImpl(
			IManterLimitesContratoService manterLimitesContratoServiceImpl) {
		this.manterLimitesContratoServiceImpl = manterLimitesContratoServiceImpl;
	}

	/**
	 * Get: itemSelecionadoLimiteConta.
	 *
	 * @return itemSelecionadoLimiteConta
	 */
	public Integer getItemSelecionadoLimiteConta() {
		return itemSelecionadoLimiteConta;
	}

	/**
	 * Set: itemSelecionadoLimiteConta.
	 *
	 * @param itemSelecionadoLimiteConta the item selecionado limite conta
	 */
	public void setItemSelecionadoLimiteConta(Integer itemSelecionadoLimiteConta) {
		this.itemSelecionadoLimiteConta = itemSelecionadoLimiteConta;
	}

	/**
	 * Get: listaGridControleLimiteConta.
	 *
	 * @return listaGridControleLimiteConta
	 */
	public List<SelectItem> getListaGridControleLimiteConta() {
		return listaGridControleLimiteConta;
	}

	/**
	 * Set: listaGridControleLimiteConta.
	 *
	 * @param listaGridControleLimiteConta the lista grid controle limite conta
	 */
	public void setListaGridControleLimiteConta(
			List<SelectItem> listaGridControleLimiteConta) {
		this.listaGridControleLimiteConta = listaGridControleLimiteConta;
	}

	/**
	 * Get: listaGridLimiteConta.
	 *
	 * @return listaGridLimiteConta
	 */
	public List<ConsultarLimiteIntraPgitSaidaDTO> getListaGridLimiteConta() {
		return listaGridLimiteConta;
	}

	/**
	 * Set: listaGridLimiteConta.
	 *
	 * @param listaGridLimiteConta the lista grid limite conta
	 */
	public void setListaGridLimiteConta(
			List<ConsultarLimiteIntraPgitSaidaDTO> listaGridLimiteConta) {
		this.listaGridLimiteConta = listaGridLimiteConta;
	}

	/**
	 * Get: itemSelecionadoLimiteModalidade.
	 *
	 * @return itemSelecionadoLimiteModalidade
	 */
	public Integer getItemSelecionadoLimiteModalidade() {
		return itemSelecionadoLimiteModalidade;
	}

	/**
	 * Set: itemSelecionadoLimiteModalidade.
	 *
	 * @param itemSelecionadoLimiteModalidade the item selecionado limite modalidade
	 */
	public void setItemSelecionadoLimiteModalidade(
			Integer itemSelecionadoLimiteModalidade) {
		this.itemSelecionadoLimiteModalidade = itemSelecionadoLimiteModalidade;
	}

	/**
	 * Get: listaGridControleLimiteModalidade.
	 *
	 * @return listaGridControleLimiteModalidade
	 */
	public List<SelectItem> getListaGridControleLimiteModalidade() {
		return listaGridControleLimiteModalidade;
	}

	/**
	 * Set: listaGridControleLimiteModalidade.
	 *
	 * @param listaGridControleLimiteModalidade the lista grid controle limite modalidade
	 */
	public void setListaGridControleLimiteModalidade(
			List<SelectItem> listaGridControleLimiteModalidade) {
		this.listaGridControleLimiteModalidade = listaGridControleLimiteModalidade;
	}

	/**
	 * Get: listaGridLimiteModalidade.
	 *
	 * @return listaGridLimiteModalidade
	 */
	public List<ConsultarListaServLimDiaIndvSaidaDTO> getListaGridLimiteModalidade() {
		return listaGridLimiteModalidade;
	}

	/**
	 * Set: listaGridLimiteModalidade.
	 *
	 * @param listaGridLimiteModalidade the lista grid limite modalidade
	 */
	public void setListaGridLimiteModalidade(
			List<ConsultarListaServLimDiaIndvSaidaDTO> listaGridLimiteModalidade) {
		this.listaGridLimiteModalidade = listaGridLimiteModalidade;
	}

	/**
	 * Get: modalidadeDesc.
	 *
	 * @return modalidadeDesc
	 */
	public String getModalidadeDesc() {
		return modalidadeDesc;
	}

	/**
	 * Set: modalidadeDesc.
	 *
	 * @param modalidadeDesc the modalidade desc
	 */
	public void setModalidadeDesc(String modalidadeDesc) {
		this.modalidadeDesc = modalidadeDesc;
	}

	/**
	 * Get: tipoServicoDesc.
	 *
	 * @return tipoServicoDesc
	 */
	public String getTipoServicoDesc() {
		return tipoServicoDesc;
	}

	/**
	 * Set: tipoServicoDesc.
	 *
	 * @param tipoServicoDesc the tipo servico desc
	 */
	public void setTipoServicoDesc(String tipoServicoDesc) {
		this.tipoServicoDesc = tipoServicoDesc;
	}

	/**
	 * Get: valorLimiteDiario.
	 *
	 * @return valorLimiteDiario
	 */
	public BigDecimal getValorLimiteDiario() {
		return valorLimiteDiario;
	}

	/**
	 * Set: valorLimiteDiario.
	 *
	 * @param valorLimiteDiario the valor limite diario
	 */
	public void setValorLimiteDiario(BigDecimal valorLimiteDiario) {
		this.valorLimiteDiario = valorLimiteDiario;
	}

	/**
	 * Get: valorLimiteIndividual.
	 *
	 * @return valorLimiteIndividual
	 */
	public BigDecimal getValorLimiteIndividual() {
		return valorLimiteIndividual;
	}

	/**
	 * Set: valorLimiteIndividual.
	 *
	 * @param valorLimiteIndividual the valor limite individual
	 */
	public void setValorLimiteIndividual(BigDecimal valorLimiteIndividual) {
		this.valorLimiteIndividual = valorLimiteIndividual;
	}

	/**
	 * Get: listaGridLimiteUtilizado.
	 *
	 * @return listaGridLimiteUtilizado
	 */
	public List<ConsultarLimDiarioUtilizadoSaidaDTO> getListaGridLimiteUtilizado() {
		return listaGridLimiteUtilizado;
	}

	/**
	 * Set: listaGridLimiteUtilizado.
	 *
	 * @param listaGridLimiteUtilizado the lista grid limite utilizado
	 */
	public void setListaGridLimiteUtilizado(
			List<ConsultarLimDiarioUtilizadoSaidaDTO> listaGridLimiteUtilizado) {
		this.listaGridLimiteUtilizado = listaGridLimiteUtilizado;
	}

	/**
	 * Get: agenciaDesc.
	 *
	 * @return agenciaDesc
	 */
	public String getAgenciaDesc() {
		return agenciaDesc;
	}

	/**
	 * Set: agenciaDesc.
	 *
	 * @param agenciaDesc the agencia desc
	 */
	public void setAgenciaDesc(String agenciaDesc) {
		this.agenciaDesc = agenciaDesc;
	}

	/**
	 * Get: bancoDesc.
	 *
	 * @return bancoDesc
	 */
	public String getBancoDesc() {
		return bancoDesc;
	}

	/**
	 * Set: bancoDesc.
	 *
	 * @param bancoDesc the banco desc
	 */
	public void setBancoDesc(String bancoDesc) {
		this.bancoDesc = bancoDesc;
	}

	/**
	 * Get: contaDesc.
	 *
	 * @return contaDesc
	 */
	public String getContaDesc() {
		return contaDesc;
	}

	/**
	 * Set: contaDesc.
	 *
	 * @param contaDesc the conta desc
	 */
	public void setContaDesc(String contaDesc) {
		this.contaDesc = contaDesc;
	}

	/**
	 * Get: cpfCnpjDesc.
	 *
	 * @return cpfCnpjDesc
	 */
	public String getCpfCnpjDesc() {
		return cpfCnpjDesc;
	}

	/**
	 * Set: cpfCnpjDesc.
	 *
	 * @param cpfCnpjDesc the cpf cnpj desc
	 */
	public void setCpfCnpjDesc(String cpfCnpjDesc) {
		this.cpfCnpjDesc = cpfCnpjDesc;
	}

	/**
	 * Get: razaoSocialDesc.
	 *
	 * @return razaoSocialDesc
	 */
	public String getRazaoSocialDesc() {
		return razaoSocialDesc;
	}

	/**
	 * Set: razaoSocialDesc.
	 *
	 * @param razaoSocialDesc the razao social desc
	 */
	public void setRazaoSocialDesc(String razaoSocialDesc) {
		this.razaoSocialDesc = razaoSocialDesc;
	}

	/**
	 * Get: tipoDesc.
	 *
	 * @return tipoDesc
	 */
	public String getTipoDesc() {
		return tipoDesc;
	}

	/**
	 * Set: tipoDesc.
	 *
	 * @param tipoDesc the tipo desc
	 */
	public void setTipoDesc(String tipoDesc) {
		this.tipoDesc = tipoDesc;
	}

	/**
	 * Get: dataPrazoFim.
	 *
	 * @return dataPrazoFim
	 */
	public Date getDataPrazoFim() {
		return dataPrazoFim;
	}

	/**
	 * Set: dataPrazoFim.
	 *
	 * @param dataPrazoFim the data prazo fim
	 */
	public void setDataPrazoFim(Date dataPrazoFim) {
		this.dataPrazoFim = dataPrazoFim;
	}

	/**
	 * Get: dataPrazoInicio.
	 *
	 * @return dataPrazoInicio
	 */
	public Date getDataPrazoInicio() {
		return dataPrazoInicio;
	}

	/**
	 * Set: dataPrazoInicio.
	 *
	 * @param dataPrazoInicio the data prazo inicio
	 */
	public void setDataPrazoInicio(Date dataPrazoInicio) {
		this.dataPrazoInicio = dataPrazoInicio;
	}

	/**
	 * Get: valorLimite.
	 *
	 * @return valorLimite
	 */
	public BigDecimal getValorLimite() {
		return valorLimite;
	}

	/**
	 * Set: valorLimite.
	 *
	 * @param valorLimite the valor limite
	 */
	public void setValorLimite(BigDecimal valorLimite) {
		this.valorLimite = valorLimite;
	}

	/**
	 * Get: prazoValidadeAltDesc.
	 *
	 * @return prazoValidadeAltDesc
	 */
	public String getPrazoValidadeAltDesc() {
		return prazoValidadeAltDesc;
	}

	/**
	 * Set: prazoValidadeAltDesc.
	 *
	 * @param prazoValidadeAltDesc the prazo validade alt desc
	 */
	public void setPrazoValidadeAltDesc(String prazoValidadeAltDesc) {
		this.prazoValidadeAltDesc = prazoValidadeAltDesc;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}

	/**
	 * Get: habilitaPainelDatas1.
	 *
	 * @return habilitaPainelDatas1
	 */
	public Boolean getHabilitaPainelDatas1() {
		return habilitaPainelDatas1;
	}

	/**
	 * Set: habilitaPainelDatas1.
	 *
	 * @param habilitaPainelDatas1 the habilita painel datas1
	 */
	public void setHabilitaPainelDatas1(Boolean habilitaPainelDatas1) {
		this.habilitaPainelDatas1 = habilitaPainelDatas1;
	}

	/**
	 * Get: habilitaPainelDatas2.
	 *
	 * @return habilitaPainelDatas2
	 */
	public Boolean getHabilitaPainelDatas2() {
		return habilitaPainelDatas2;
	}

	/**
	 * Set: habilitaPainelDatas2.
	 *
	 * @param habilitaPainelDatas2 the habilita painel datas2
	 */
	public void setHabilitaPainelDatas2(Boolean habilitaPainelDatas2) {
		this.habilitaPainelDatas2 = habilitaPainelDatas2;
	}

	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Get: valorLimiteTED.
	 *
	 * @return valorLimiteTED
	 */
	public BigDecimal getValorLimiteTED() {
		return valorLimiteTED;
	}

	/**
	 * Set: valorLimiteTED.
	 *
	 * @param valorLimiteTED the valor limite ted
	 */
	public void setValorLimiteTED(BigDecimal valorLimiteTED) {
		this.valorLimiteTED = valorLimiteTED;
	}

	/**
	 * Get: dataPrazoInicioTED.
	 *
	 * @return dataPrazoInicioTED
	 */
	public Date getDataPrazoInicioTED() {
		return dataPrazoInicioTED;
	}

	/**
	 * Set: dataPrazoInicioTED.
	 *
	 * @param dataPrazoInicioTED the data prazo inicio ted
	 */
	public void setDataPrazoInicioTED(Date dataPrazoInicioTED) {
		this.dataPrazoInicioTED = dataPrazoInicioTED;
	}

	/**
	 * Get: dataPrazoFimTED.
	 *
	 * @return dataPrazoFimTED
	 */
	public Date getDataPrazoFimTED() {
		return dataPrazoFimTED;
	}

	/**
	 * Set: dataPrazoFimTED.
	 *
	 * @param dataPrazoFimTED the data prazo fim ted
	 */
	public void setDataPrazoFimTED(Date dataPrazoFimTED) {
		this.dataPrazoFimTED = dataPrazoFimTED;
	}

	/**
	 * Get: habilitaPainelDatasTED1.
	 *
	 * @return habilitaPainelDatasTED1
	 */
	public Boolean getHabilitaPainelDatasTED1() {
		return habilitaPainelDatasTED1;
	}

	/**
	 * Set: habilitaPainelDatasTED1.
	 *
	 * @param habilitaPainelDatasTED1 the habilita painel datas te d1
	 */
	public void setHabilitaPainelDatasTED1(Boolean habilitaPainelDatasTED1) {
		this.habilitaPainelDatasTED1 = habilitaPainelDatasTED1;
	}

	/**
	 * Get: habilitaPainelDatasTED2.
	 *
	 * @return habilitaPainelDatasTED2
	 */
	public Boolean getHabilitaPainelDatasTED2() {
		return habilitaPainelDatasTED2;
	}

	/**
	 * Set: habilitaPainelDatasTED2.
	 *
	 * @param habilitaPainelDatasTED2 the habilita painel datas te d2
	 */
	public void setHabilitaPainelDatasTED2(Boolean habilitaPainelDatasTED2) {
		this.habilitaPainelDatasTED2 = habilitaPainelDatasTED2;
	}

	/**
	 * Get: prazoValidadeTEDAltDesc.
	 *
	 * @return prazoValidadeTEDAltDesc
	 */
	public String getPrazoValidadeTEDAltDesc() {
		return prazoValidadeTEDAltDesc;
	}

	/**
	 * Set: prazoValidadeTEDAltDesc.
	 *
	 * @param prazoValidadeTEDAltDesc the prazo validade ted alt desc
	 */
	public void setPrazoValidadeTEDAltDesc(String prazoValidadeTEDAltDesc) {
		this.prazoValidadeTEDAltDesc = prazoValidadeTEDAltDesc;
	}

	/**
	 * @return the consultarLimiteContaSaida
	 */
	public ConsultarLimiteContaSaidaDTO getConsultarLimiteContaSaida() {
		return consultarLimiteContaSaida;
	}

	/**
	 * @param consultarLimiteContaSaida the consultarLimiteContaSaida to set
	 */
	public void setConsultarLimiteContaSaida(
			ConsultarLimiteContaSaidaDTO consultarLimiteContaSaida) {
		this.consultarLimiteContaSaida = consultarLimiteContaSaida;
	}

	/**
	 * @return the itemSelecionadoGridLimiteConta
	 */
	public ConsultarLimiteIntraPgitSaidaDTO getItemSelecionadoGridLimiteConta() {
		return itemSelecionadoGridLimiteConta;
	}

	/**
	 * @param itemSelecionadoGridLimiteConta the itemSelecionadoGridLimiteConta to set
	 */
	public void setItemSelecionadoGridLimiteConta(
			ConsultarLimiteIntraPgitSaidaDTO itemSelecionadoGridLimiteConta) {
		this.itemSelecionadoGridLimiteConta = itemSelecionadoGridLimiteConta;
	}

	/**
	 * @return the listaGridHistoricoLimiteConta
	 */
	public List<ConsultarLimiteHistoricoOcorrenciasSaidaDTO> getListaGridHistoricoLimiteConta() {
		return listaGridHistoricoLimiteConta;
	}

	/**
	 * @param listaGridHistoricoLimiteConta the listaGridHistoricoLimiteConta to set
	 */
	public void setListaGridHistoricoLimiteConta(
			List<ConsultarLimiteHistoricoOcorrenciasSaidaDTO> listaGridHistoricoLimiteConta) {
		this.listaGridHistoricoLimiteConta = listaGridHistoricoLimiteConta;
	}

	/**
	 * @return the itemSelecionadoHistoricoLimiteConta
	 */
	public Integer getItemSelecionadoHistoricoLimiteConta() {
		return itemSelecionadoHistoricoLimiteConta;
	}

	/**
	 * @param itemSelecionadoHistoricoLimiteConta the itemSelecionadoHistoricoLimiteConta to set
	 */
	public void setItemSelecionadoHistoricoLimiteConta(
			Integer itemSelecionadoHistoricoLimiteConta) {
		this.itemSelecionadoHistoricoLimiteConta = itemSelecionadoHistoricoLimiteConta;
	}

	/**
	 * @return the listaRadioHistoricoLimiteConta
	 */
	public List<SelectItem> getListaRadioHistoricoLimiteConta() {
		return listaRadioHistoricoLimiteConta;
	}

	/**
	 * @param listaRadioHistoricoLimiteConta the listaRadioHistoricoLimiteConta to set
	 */
	public void setListaRadioHistoricoLimiteConta(
			List<SelectItem> listaRadioHistoricoLimiteConta) {
		this.listaRadioHistoricoLimiteConta = listaRadioHistoricoLimiteConta;
	}

	public void setConsultarLimiteHistoricoSaida(
			ConsultarLimiteHistoricoSaidaDTO consultarLimiteHistoricoSaida) {
		this.consultarLimiteHistoricoSaida = consultarLimiteHistoricoSaida;
	}

	public ConsultarLimiteHistoricoSaidaDTO getConsultarLimiteHistoricoSaida() {
		return consultarLimiteHistoricoSaida;
	}

	public void setItemSelecionadoGridHistorico(
			ConsultarLimiteHistoricoOcorrenciasSaidaDTO itemSelecionadoGridHistorico) {
		this.itemSelecionadoGridHistorico = itemSelecionadoGridHistorico;
	}

	public ConsultarLimiteHistoricoOcorrenciasSaidaDTO getItemSelecionadoGridHistorico() {
		return itemSelecionadoGridHistorico;
	}

	/**
	 * @return the dataManutencaoInicio
	 */
	public Date getDataManutencaoInicio() {
		return dataManutencaoInicio;
	}

	/**
	 * @param dataManutencaoInicio the dataManutencaoInicio to set
	 */
	public void setDataManutencaoInicio(Date dataManutencaoInicio) {
		this.dataManutencaoInicio = dataManutencaoInicio;
	}

	/**
	 * @return the dataManutencaoFim
	 */
	public Date getDataManutencaoFim() {
		return dataManutencaoFim;
	}

	/**
	 * @param dataManutencaoFim the dataManutencaoFim to set
	 */
	public void setDataManutencaoFim(Date dataManutencaoFim) {
		this.dataManutencaoFim = dataManutencaoFim;
	}
}
