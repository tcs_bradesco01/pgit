/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterservicorelacionados
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.manterservicorelacionados;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListaServicoCompostoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoRelacionadoPgitCdpsEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoRelacionadoPgitCdpsOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoRelacionadoPgitCdpsSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoRelacionadoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoRelacionadoPgitOcorrenciasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicoRelacionadoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.IManterServicoRelacionadosService;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.AlterarServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.AlterarServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.DetalharServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.DetalharServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ExcluirServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ExcluirServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.IncluirServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.IncluirServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterservicorelacionados.bean.ListarServicoRelacionadoSaidaDTO;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;

// TODO: Auto-generated Javadoc
/**
 * Nome: ManterServicosRelacionadosBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class ManterServicosRelacionadosBean {

	/** The Constant ZERO. */
	private static final int ZERO = 0;
	
	/** The Constant DOIS. */
	private static final int DOIS = 2;
	
	/** The Constant UM. */
	private static final int UM = 1;
	/** Atributo CON_MANTER_SERVICO_RELACIONADOS. */
	private static final String CON_MANTER_SERVICO_RELACIONADOS = "conManterServicoRelacionados";
	
	/** The Constant NEGOCIACAO. */
	private static final String NEGOCIACAO = "Negocia��o";
	
	/** The Constant CONTINGENCIA. */
	private static final String CONTINGENCIA = "Conting�ncia";
	
	/** The Constant MANUTENCAO. */
	private static final String MANUTENCAO = "Manuten��o";
	
	/** The Constant SIM. */
	private static final String SIM = "SIM";

	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;

	/** Atributo modalidadeServicoFiltro. */
	private Integer modalidadeServicoFiltro;

	/** Atributo listaGrid. */
	private List<ListarServicoRelacionadoSaidaDTO> listaGrid;

	/** Atributo itemSelecionadoListaGrid. */
	private Integer itemSelecionadoListaGrid;

	/** Atributo listaGridControle. */
	private List<SelectItem> listaGridControle = new ArrayList<SelectItem>();

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo dsServicoComposto. */
	private String dsServicoComposto;

	/** Atributo tipoServico. */
	private Integer tipoServico;

	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico;
	
	/** Atributo listarArgumentoPesquisaServico. */
	private List<SelectItem> listarArgumentoPesquisaServico;
	
	/** Atributo listaTipoServicoIncluir. */
	private List<SelectItem> listaTipoServicoIncluir;

	/** Atributo listaTipoServicoIncluirHash. */
	private Map<Integer, ListarServicosSaidaDTO> listaTipoServicoIncluirHash = new HashMap<Integer, ListarServicosSaidaDTO>();

	/** Atributo listaServicoCompostoHash. */
	private Map<Long, ListaServicoCompostoSaidaDTO> listaServicoCompostoHash = new HashMap<Long, ListaServicoCompostoSaidaDTO>();

	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();

	/** Atributo servicoRelacionado. */
	private Integer servicoRelacionado;

	/** Atributo listaModalidadeServico. */
	private List<SelectItem> listaModalidadeServico = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeServicoHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO>();

	/** Atributo listaModalidadeServicoFiltro. */
	private List<SelectItem> listaModalidadeServicoFiltro = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeServicoFiltroHash. */
	private Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoFiltroHash = new HashMap<Integer, ListarModalidadeSaidaDTO>();
	
	/** Atributo listaServicoRelacionadoPgitCdPsOcorrencias. */
	private List<ListarServicoRelacionadoPgitCdpsOcorrenciasSaidaDTO> listaServicoRelacionadoPgitCdPsOcorrencias = null;

	/** Atributo listaServicoComposto. */
	private List<SelectItem> listaServicoComposto;

	/** Atributo servicoDesc. */
	private String servicoDesc;

	/** Atributo servicoRelacionadoDesc. */
	private String servicoRelacionadoDesc;

	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;

	/** Atributo servicoComposto. */
	private Long servicoComposto;

	/** Atributo naturezaServico. */
	private Integer naturezaServico;

	/** Atributo btoAcionado. */
	private boolean btoAcionado;

	/** Atributo manterServicoRelacionado. */
	private IManterServicoRelacionadosService manterServicoRelacionado;

	/** Atributo comboService. */
	private IComboService comboService;

	/** The nr ordenacao servico relacionado. */
	private Integer nrOrdenacaoServicoRelacionado;
	
	/** The cd indicador servico negociavel. */
	private String cdIndicadorServicoNegociavel;
	
	/** The ds indicador servico negociavel. */
	private String dsIndicadorServicoNegociavel;
	
	/** The cd indicador negociavel servico. */
	private String cdIndicadorNegociavelServico;
	
	/** The cd indicador contigencia servico. */
	private String cdIndicadorContigenciaServico;
	
	/** The cd indicador manutencao servico. */
	private String cdIndicadorManutencaoServico;
	
	/** The chk negociacao. */
	private boolean chkNegociacao;
	
	/** The chk contingencia. */
	private boolean chkContingencia;
	
	/** The chk manutencao. */
	private boolean chkManutencao;

	/** The entrada alteracao. */
	private AlterarServicoRelacionadoEntradaDTO entradaAlteracao;

	/** The saida detalhe. */
	private DetalharServicoRelacionadoSaidaDTO saidaDetalhe;



	/**
	 * Pesquisar.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void pesquisar(ActionEvent evt) {
		return;
	}

	/**
	 * Limpar pagina.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void limparPagina(ActionEvent evt) {
		setTipoServicoFiltro(null);
		setModalidadeServicoFiltro(null);
		setTipoServico(null);
		setServicoRelacionado(null);

		setServicoDesc("");
		setServicoRelacionadoDesc("");

		setServicoComposto(null);
		setNaturezaServico(null);
		
		this.modalidadeServicoFiltro = 0;
		setBtoAcionado(false);
		
		listarArgumentoPesquisaServ();
	}

	/**
	 * Limpar variaveis.
	 */
	public void limparVariaveis() {

		setServicoDesc("");
		setServicoRelacionadoDesc("");
		setServicoComposto(null);
		setNaturezaServico(0);

	}

	/**
	 * Limpar campos.
	 */
	public void limparCampos() {
		setTipoServicoFiltro(0);
		setModalidadeServicoFiltro(0);
		setItemSelecionadoListaGrid(null);
		setListaGrid(null);
		setBtoAcionado(false);
		listaModalidadeServicoFiltro = new ArrayList<SelectItem>();
	}

	/**
	 * Limpar Check Boxes.
	 */
	public void limparCheckBoxes() {
		setChkNegociacao(false);
		setChkContingencia(false);
		setChkManutencao(false);
	}

	/**
	 * Limpar Check Boxes Inclusao.
	 */
	public void limparCheckBoxesInclusao() {
		if (!isIndicadorServicoNegociavel()) {
			limparCheckBoxes();
		}
	}

	/**
	 * Limpar Check Boxes Alteracao.
	 */
	public void limparCheckBoxesAlteracao() {
		if (!entradaAlteracao.isIndicadorServicoNegociavel()) {
			limparCheckBoxes();
		}
	}

	/**
	 * Checks if is indicador servico negociavel.
	 *
	 * @return true, if checks if is indicador servico negociavel
	 */
	public boolean isIndicadorServicoNegociavel() {
		return getCdIndicadorServicoNegociavel() != null
		&& getCdIndicadorServicoNegociavel().equals("1");
	}


	private void listarArgumentoPesquisaServ() {
						
		ListarServicoRelacionadoPgitCdpsSaidaDTO saida =  comboService.listarServicoRelacionadoPgitCdps(
				new ListarServicoRelacionadoPgitCdpsEntradaDTO());
		
		setListarArgumentoPesquisaServico(new ArrayList<SelectItem>(saida.getNrLinhas()));
		
		listaServicoRelacionadoPgitCdPsOcorrencias = saida.getOcorrencias();
		
		for (int i = 0; i < saida.getNrLinhas(); i++) {
			listarArgumentoPesquisaServico.add(new SelectItem(listaServicoRelacionadoPgitCdPsOcorrencias.get(i).getCdProdutoOperacaoRelacionado(), 
					listaServicoRelacionadoPgitCdPsOcorrencias.get(i).getDsProdutoServicoRelacionado()));
		}
				
	}
	
	
	/**
	 * Listar tipo servico.
	 */
	private void listarTipoServico() {
		
		this.listaTipoServico = new ArrayList<SelectItem>();
		
		List<ListarServicosSaidaDTO> saidaDTO = new ArrayList<ListarServicosSaidaDTO>();
		
		saidaDTO = comboService.listarTipoServicosRelacionados();
		
		listaTipoServicoHash.clear();
		for (ListarServicosSaidaDTO combo : saidaDTO) {
			listaTipoServicoHash
			.put(combo.getCdServico(), combo.getDsServico());
			this.listaTipoServico.add(new SelectItem(combo.getCdServico(),
					combo.getDsServico()));
		}
	}

	/**
	 * Listar tipo servico incluir.
	 */
	private void listarTipoServicoIncluir() {

		this.listaTipoServicoIncluir = new ArrayList<SelectItem>();

		try {

			List<ListarServicosSaidaDTO> saidaDTO = new ArrayList<ListarServicosSaidaDTO>();

			saidaDTO = comboService.listarTipoServicosRelacionados();

			listaTipoServicoIncluirHash.clear();
			for (ListarServicosSaidaDTO combo : saidaDTO) {
				listaTipoServicoIncluirHash.put(combo.getCdServico(), combo);
				this.listaTipoServicoIncluir.add(new SelectItem(combo
						.getCdServico(), combo.getDsServico()));
			}
		} catch (PdcAdapterFunctionalException e) {
			listaTipoServicoIncluir = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar servico composto.
	 */
	private void listarServicoComposto() {
		try {
			List<ListaServicoCompostoSaidaDTO> lista = comboService
			.listarServicoComposto();

			listaServicoComposto = new ArrayList<SelectItem>();

			for (ListaServicoCompostoSaidaDTO saidaDTO : lista) {
				listaServicoComposto.add(new SelectItem(saidaDTO
						.getCdServicoComposto(), saidaDTO
						.getDsServicoComposto()));
				listaServicoCompostoHash.put(saidaDTO.getCdServicoComposto(),
						saidaDTO);
			}
		} catch (PdcAdapterFunctionalException e) {
			listaServicoComposto = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar modalidade servico filtro.
	 */
	public void listarModalidadeServicoFiltro() {

		listaModalidadeServicoFiltro = new ArrayList<SelectItem>();

		try {
			if (getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0) {

				List<ListarModalidadeSaidaDTO> listaModalidades = new ArrayList<ListarModalidadeSaidaDTO>();
				ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();

				listarModalidadeEntradaDTO.setCdServico(getTipoServicoFiltro());

				listaModalidades = comboService
				.listarModalidadesRealcionados(listarModalidadeEntradaDTO);

				listaModalidadeServicoFiltroHash.clear();
				for (ListarModalidadeSaidaDTO combo : listaModalidades) {
					listaModalidadeServicoFiltroHash.put(combo
							.getCdModalidade(), combo);
					this.listaModalidadeServicoFiltro.add(new SelectItem(combo
							.getCdModalidade(), combo.getDsModalidade()));
				}
				setServicoRelacionado(0);
			} else {

				listaModalidadeServicoFiltroHash.clear();
				listaModalidadeServicoFiltro.clear();
				setTipoServicoFiltro(0);
			}
		} catch (PdcAdapterFunctionalException e) {
			listaModalidadeServicoFiltro = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar modalidade servico.
	 */
	public void listarModalidadeServico() {

		listaModalidadeServico = new ArrayList<SelectItem>();
		try {
			if (getTipoServico() != null && getTipoServico() != 0) {
				List<ListarModalidadeSaidaDTO> listaModalidadesIncluir = new ArrayList<ListarModalidadeSaidaDTO>();
				ListarModalidadesEntradaDTO listarModalidadeEntradaDTO = new ListarModalidadesEntradaDTO();

				listarModalidadeEntradaDTO.setCdServico(getTipoServico());

				listaModalidadesIncluir = comboService
				.listarModalidadesRealcionados(listarModalidadeEntradaDTO);

				listaModalidadeServicoHash.clear();

				for (ListarModalidadeSaidaDTO combo : listaModalidadesIncluir) {
					this.listaModalidadeServico.add(new SelectItem(combo
							.getCdModalidade(), combo.getDsModalidade()));
					listaModalidadeServicoHash.put(combo.getCdModalidade(),
							combo);

				}
			} else {
				listaModalidadeServicoHash = new HashMap<Integer, ListarModalidadeSaidaDTO>();
				listaModalidadeServico.clear();
				setServicoRelacionado(0);
			}
		} catch (PdcAdapterFunctionalException e) {
			listaModalidadeServico = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Consultar.
	 */
	public void consultar() {

		try {

			ListarServicoRelacionadoEntradaDTO entradaDTO = new ListarServicoRelacionadoEntradaDTO();

			entradaDTO
			.setModalidadeServico(getModalidadeServicoFiltro() != null
					&& getModalidadeServicoFiltro() != 0 ? getModalidadeServicoFiltro()
							: 0);
			entradaDTO
			.setTipoServico(getTipoServicoFiltro() != null
					&& getTipoServicoFiltro() != 0 ? getTipoServicoFiltro()
							: 0);
			entradaDTO
			.setTipoRelacionamento(getModalidadeServicoFiltro() != null
					&& getModalidadeServicoFiltro() != 0 ? listaModalidadeServicoFiltroHash
							.get(getModalidadeServicoFiltro())
							.getCdRelacionamentoProduto()
							: 0);

			setListaGrid(getManterServicoRelacionado()
					.listarServicoRelacionadoDTO(entradaDTO));
			setBtoAcionado(true);
			listaGridControle = new ArrayList<SelectItem>();
			for (int i = 0; i < this.getListaGrid().size(); i++) {
				listaGridControle.add(new SelectItem(i, " "));
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);
			setListaGrid(null);
			setItemSelecionadoListaGrid(null);

		}
		setItemSelecionadoListaGrid(null);
	}

	/**
	 * Voltar.
	 * 
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoListaGrid(null);
		limparCheckBoxes();

		return "VOLTAR";
	}

	/**
	 * Voltar Altera��o.
	 * 
	 * @return the string
	 */
	public String voltarAlteracao() {		
		
		return "VOLTAR";
	}

	/**
	 * Avancar incluir.
	 * 
	 * @return the string
	 */
	public String avancarIncluir() {			
		
		if (getObrigatoriedade().equals("T")) {
		    
			if (listaServicoRelacionadoPgitCdPsOcorrencias.size() > 0) {
			    for (int i = 0;  i < listaServicoRelacionadoPgitCdPsOcorrencias.size(); i++){
			        if(listaServicoRelacionadoPgitCdPsOcorrencias.get(i).getCdProdutoOperacaoRelacionado().equals(getTipoServico())){
    			        setServicoDesc(PgitUtil.concatenarCampos(listaServicoRelacionadoPgitCdPsOcorrencias.get(i).getCdProdutoOperacaoRelacionado(),
    						listaServicoRelacionadoPgitCdPsOcorrencias.get(i).getDsProdutoServicoRelacionado()));
			        }
			    }
			}else {
				setServicoDesc("");
			}
			setServicoRelacionadoDesc(getServicoRelacionado() == 0 ? ""
					: PgitUtil.concatenarCampos(listaModalidadeServicoHash.get(
							getServicoRelacionado()).getCdModalidade(),
							listaModalidadeServicoHash.get(
									getServicoRelacionado()).getDsModalidade()));
			setDsServicoComposto(PgitUtil.concatenarCampos(
					listaServicoCompostoHash.get(getServicoComposto())
					.getCdServicoComposto(), listaServicoCompostoHash
					.get(servicoComposto).getDsServicoComposto()));

			setDsIndicadorServicoNegociavel("Sim");
			if (getCdIndicadorServicoNegociavel().equals("2")) {
				setDsIndicadorServicoNegociavel("N�o");
			}

			setCdIndicadorNegociavelServico(null);
			setCdIndicadorContigenciaServico(null);
			setCdIndicadorManutencaoServico(null);

			if (isChkNegociacao()) {
				setCdIndicadorNegociavelServico(NEGOCIACAO);
			}

			if (isChkContingencia()) {
				setCdIndicadorContigenciaServico(CONTINGENCIA);
			}

			if (isChkManutencao()) {
				setCdIndicadorManutencaoServico(MANUTENCAO);
			}

			return "AVANCAR_INCLUIR";
		}
		return "";
	}

	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {

		entradaAlteracao.setCdIndicadorNegociavelServico(DOIS);
		entradaAlteracao.setCdIndicadorContigenciaServico(DOIS);
		entradaAlteracao.setCdIndicadorManutencaoServico(DOIS);

		if (isChkNegociacao()) {
			entradaAlteracao.setCdIndicadorNegociavelServico(UM);
		}

		if (isChkContingencia()) {
			entradaAlteracao.setCdIndicadorContigenciaServico(UM);
		}

		if (isChkManutencao()) {
			entradaAlteracao.setCdIndicadorManutencaoServico(UM);
		}

		return "AVANCAR_ALTERAR";
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {

		try {

			AlterarServicoRelacionadoSaidaDTO saidaAlteracao = getManterServicoRelacionado()
			.alterarServicoRelacionado(entradaAlteracao);

			BradescoFacesUtils.addInfoModalMessage("("
					+ saidaAlteracao.getCodMensagem() + ") "
					+ saidaAlteracao.getMensagem(),
					CON_MANTER_SERVICO_RELACIONADOS,
					BradescoViewExceptionActionType.ACTION, false);

			consultar();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);
			return null;
		}

		return "";
	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados() {

		ListarServicoRelacionadoSaidaDTO listarServicoRelacionadoSaidaDTO = getListaGrid()
		.get(getItemSelecionadoListaGrid());
		DetalharServicoRelacionadoEntradaDTO entradaDTO = new DetalharServicoRelacionadoEntradaDTO();

		entradaDTO.setCodModalidadeServico(listarServicoRelacionadoSaidaDTO
				.getCodModalidadeServico());
		entradaDTO.setCodTipoRelacionamento(listarServicoRelacionadoSaidaDTO
				.getCodRelacionamentoProd());
		entradaDTO.setCodTipoServico(listarServicoRelacionadoSaidaDTO
				.getCodTipoServico());

		saidaDetalhe = getManterServicoRelacionado()
		.detalharServicoRelacionadoDTO(entradaDTO);

		setServicoDesc(PgitUtil.concatenarCampos(saidaDetalhe
				.getCodTipoServico(), saidaDetalhe.getDsTipoServico()));
		setServicoRelacionadoDesc(PgitUtil.concatenarCampos(saidaDetalhe
				.getCodModalidadeServico(), saidaDetalhe
				.getDsModalidadeServico()));
		setDsServicoComposto(PgitUtil.concatenarCampos(saidaDetalhe
				.getCodServicoComposto(), saidaDetalhe.getDsServicoComposto()));
		setNaturezaServico(saidaDetalhe.getCodNaturezaServico());

		/* Trilha de Auditoria */
		setDataHoraManutencao(saidaDetalhe.getDataHraManutencao());
		setDataHoraInclusao(saidaDetalhe.getDataHraInclusao());

		setTipoCanalInclusao(saidaDetalhe.getCodTipoCanalInclusao() != 0 ? saidaDetalhe
				.getCodTipoCanalInclusao()
				+ " - " + saidaDetalhe.getDsTipoCanalInclusao()
				: "");
		setTipoCanalManutencao(saidaDetalhe.getCodTipoCanalManutencao() == 0 ? ""
				: saidaDetalhe.getCodTipoCanalManutencao() + " - "
				+ saidaDetalhe.getDsTipoCanalManutencao());

		setUsuarioInclusao(saidaDetalhe.getCodUsuarioInclusao());
		setUsuarioManutencao(saidaDetalhe.getCodUsuarioManutencao());

		setComplementoInclusao(saidaDetalhe.getComplementoInclusao() == null
				|| saidaDetalhe.getComplementoInclusao().equals("0") ? ""
						: saidaDetalhe.getComplementoInclusao());
		setComplementoManutencao(saidaDetalhe.getComplementoManutencao() == null
				|| saidaDetalhe.getComplementoManutencao().equals("0") ? ""
						: saidaDetalhe.getComplementoManutencao());

		setNrOrdenacaoServicoRelacionado(saidaDetalhe
				.getNrOrdenacaoServicoRelacionado());
		setCdIndicadorServicoNegociavel(saidaDetalhe
				.getCdIndicadorServicoNegociavel());

		if (saidaDetalhe.getCdIndicadorNegociavelServico()
				.equalsIgnoreCase(SIM)) {
			setCdIndicadorNegociavelServico(NEGOCIACAO);
		}else {
			setCdIndicadorNegociavelServico(null);
		}

		if (saidaDetalhe.getCdIndicadorContigenciaServico().equalsIgnoreCase(
				SIM)) {
			setCdIndicadorContigenciaServico(CONTINGENCIA);
		}else {
			setCdIndicadorContigenciaServico(null);
		}

		if (saidaDetalhe.getCdIndicadorManutencaoServico()
				.equalsIgnoreCase(SIM)) {
			setCdIndicadorManutencaoServico(MANUTENCAO);
		}else {
			setCdIndicadorManutencaoServico(null);
		}

	}

	/**
	 * Detalhar.
	 * 
	 * @return the string
	 */
	public String detalhar() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), CON_MANTER_SERVICO_RELACIONADOS,
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "DETALHAR";
	}

	/**
	 * Incluir.
	 * 
	 * @return the string
	 */
	public String incluir() {
		listarServicoComposto();
		setTipoServico(null);
		setServicoRelacionado(0);
		setNaturezaServico(0);
		listarTipoServicoIncluir();
		listarModalidadeServico();
		setServicoComposto(null);
		setNrOrdenacaoServicoRelacionado(null);
		listarArgumentoPesquisaServ();

		return "INCLUIR";
	}

	/**
	 * Excluir.
	 * 
	 * @return the string
	 */
	public String excluir() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), CON_MANTER_SERVICO_RELACIONADOS,
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "EXCLUIR";
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		entradaAlteracao = new AlterarServicoRelacionadoEntradaDTO();

		try {
			preencheDados();

			ListarServicoRelacionadoSaidaDTO listarServicoRelacionadoSaidaDTO = getListaGrid()
			.get(getItemSelecionadoListaGrid());
			entradaAlteracao
			.setCdProdutoServicoOperacao(listarServicoRelacionadoSaidaDTO
					.getCodTipoServico());
			entradaAlteracao
			.setCdProdutoOperacaoRelacionado(listarServicoRelacionadoSaidaDTO
					.getCodModalidadeServico());
			entradaAlteracao
			.setCdRelacionamentoProduto(listarServicoRelacionadoSaidaDTO
					.getCodRelacionamentoProd());
			entradaAlteracao
			.setCdServicoCompostoPagamento(listarServicoRelacionadoSaidaDTO
					.getCodServicoComposto());
			entradaAlteracao
			.setCdNaturezaOperacaoPagamento(listarServicoRelacionadoSaidaDTO
					.getCodNaturezaServico());

			entradaAlteracao.setNrOrdenacaoServicoRelacionado(saidaDetalhe
					.getNrOrdenacaoServicoRelacionado());

			if (SIM.equalsIgnoreCase(saidaDetalhe
					.getCdIndicadorServicoNegociavel())) {
				entradaAlteracao.setCdIndicadorServicoNegociavel(UM);
			} else {
				entradaAlteracao.setCdIndicadorServicoNegociavel(DOIS);
			}

			if (SIM.equalsIgnoreCase(saidaDetalhe
					.getCdIndicadorNegociavelServico())) {
				entradaAlteracao.setCdIndicadorNegociavelServico(UM);
			} else {
				entradaAlteracao.setCdIndicadorNegociavelServico(DOIS);
			}

			if (SIM.equalsIgnoreCase(saidaDetalhe
					.getCdIndicadorContigenciaServico())) {
				entradaAlteracao.setCdIndicadorContigenciaServico(UM);
			} else {
				entradaAlteracao.setCdIndicadorContigenciaServico(DOIS);
			}

			if (SIM.equalsIgnoreCase(saidaDetalhe
					.getCdIndicadorManutencaoServico())) {
				entradaAlteracao.setCdIndicadorManutencaoServico(UM);
			} else {
				entradaAlteracao.setCdIndicadorManutencaoServico(DOIS);
			}

			configurarCamposTelaAlteracao();

			limparCheckBoxesAlteracao();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), CON_MANTER_SERVICO_RELACIONADOS,
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}

		return "ALTERAR";
	}

	/**
	 * Configurar campos tela alteracao.
	 */
	private void configurarCamposTelaAlteracao() {
		setChkNegociacao(entradaAlteracao.getCdIndicadorNegociavelServico()
				.compareTo(UM) == ZERO);
		setChkContingencia(entradaAlteracao.getCdIndicadorContigenciaServico()
				.compareTo(UM) == ZERO);
		setChkManutencao(entradaAlteracao.getCdIndicadorManutencaoServico()
				.compareTo(UM) == ZERO);
	}

	/**
	 * Confirmar excluir.
	 * 
	 * @return the string
	 */
	public String confirmarExcluir() {
		try {

			ListarServicoRelacionadoSaidaDTO listarServicoRelacionadoSaidaDTO = getListaGrid()
			.get(getItemSelecionadoListaGrid());

			ExcluirServicoRelacionadoEntradaDTO excluirServicoRelacionadoEntradaDTO = new ExcluirServicoRelacionadoEntradaDTO();

			excluirServicoRelacionadoEntradaDTO
			.setCodModalidadeServico(listarServicoRelacionadoSaidaDTO
					.getCodModalidadeServico());
			excluirServicoRelacionadoEntradaDTO
			.setCodTipoRelacionamento(listarServicoRelacionadoSaidaDTO
					.getCodRelacionamentoProd());
			excluirServicoRelacionadoEntradaDTO
			.setCodTipoServico(listarServicoRelacionadoSaidaDTO
					.getCodTipoServico());

			ExcluirServicoRelacionadoSaidaDTO excluirServicoRelacionadoSaidaDTO = getManterServicoRelacionado()
			.excluirServicoRelacionadoDTO(
					excluirServicoRelacionadoEntradaDTO);

			BradescoFacesUtils.addInfoModalMessage("("
					+ excluirServicoRelacionadoSaidaDTO.getCodMensagem() + ") "
					+ excluirServicoRelacionadoSaidaDTO.getMensagem(),
					CON_MANTER_SERVICO_RELACIONADOS,
					BradescoViewExceptionActionType.ACTION, false);
			consultar();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);
			return null;
		}

		return "";
	}

	/**
	 * Confirmar incluir.
	 * 
	 * @return the string
	 */
	public String confirmarIncluir() {

		try {

			IncluirServicoRelacionadoEntradaDTO entrada = new IncluirServicoRelacionadoEntradaDTO();

			entrada.setCodModalidadeServico(getServicoRelacionado());
			entrada.setCodNaturezaServico(getNaturezaServico());

			entrada.setCodServicoComposto(null);
			if (getServicoComposto() != null) {
				entrada.setCodServicoComposto(getServicoComposto().intValue());
			}

			entrada.setCodTipoRelacionamento(null);
			if (getServicoRelacionado() != null
					&& getServicoRelacionado().compareTo(0) != 0) {
				entrada.setCodTipoRelacionamento(listaModalidadeServicoHash
						.get(getServicoRelacionado())
						.getCdRelacionamentoProduto());
			}

			entrada.setCodTipoServico(getTipoServico());

			entrada
			.setNrOrdenacaoServicoRelacionado(getNrOrdenacaoServicoRelacionado());

			entrada.setCdIndicadorNegociavelServico(2);
			entrada.setCdIndicadorContigenciaServico(2);
			entrada.setCdIndicadorManutencaoServico(2);
			entrada.setCdIndicadorServicoNegociavel(NumberUtils
					.parseInteger(getCdIndicadorServicoNegociavel()));

			if (isChkNegociacao()) {
				entrada.setCdIndicadorNegociavelServico(1);
			}

			if (isChkContingencia()) {
				entrada.setCdIndicadorContigenciaServico(1);
			}

			if (isChkManutencao()) {
				entrada.setCdIndicadorManutencaoServico(1);
			}

			IncluirServicoRelacionadoSaidaDTO incluirServicoRelacionadoSaidaDTO = getManterServicoRelacionado()
			.incluirServicoRelacionadoDTO(entrada);

			BradescoFacesUtils.addInfoModalMessage("("
					+ incluirServicoRelacionadoSaidaDTO.getCodMensagem() + ") "
					+ incluirServicoRelacionadoSaidaDTO.getMensagem(),
					CON_MANTER_SERVICO_RELACIONADOS,
					BradescoViewExceptionActionType.ACTION, false);
			consultar();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);
			return null;
		}

		return "";

	}

	/**
	 * Get: tipoServicoFiltro.
	 * 
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 * 
	 * @param tipoServicoFiltro
	 *            the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Get: itemSelecionadoListaGrid.
	 * 
	 * @return itemSelecionadoListaGrid
	 */
	public Integer getItemSelecionadoListaGrid() {
		return itemSelecionadoListaGrid;
	}

	/**
	 * Set: itemSelecionadoListaGrid.
	 * 
	 * @param itemSelecionadoListaGrid
	 *            the item selecionado lista grid
	 */
	public void setItemSelecionadoListaGrid(Integer itemSelecionadoListaGrid) {
		this.itemSelecionadoListaGrid = itemSelecionadoListaGrid;
	}

	/**
	 * Get: listaGridControle.
	 * 
	 * @return listaGridControle
	 */
	public List<SelectItem> getListaGridControle() {
		return listaGridControle;
	}

	/**
	 * Get: listaModalidadeServico.
	 * 
	 * @return listaModalidadeServico
	 */
	public List<SelectItem> getListaModalidadeServico() {
		return listaModalidadeServico;
	}

	/**
	 * Set: listaModalidadeServico.
	 * 
	 * @param listaModalidadeServico
	 *            the lista modalidade servico
	 */
	public void setListaModalidadeServico(
			List<SelectItem> listaModalidadeServico) {
		this.listaModalidadeServico = listaModalidadeServico;
	}

	/**
	 * Set: listaGridControle.
	 * 
	 * @param listaGridControle
	 *            the lista grid controle
	 */
	public void setListaGridControle(List<SelectItem> listaGridControle) {
		this.listaGridControle = listaGridControle;
	}

	/**
	 * Get: servicoRelacionado.
	 * 
	 * @return servicoRelacionado
	 */
	public Integer getServicoRelacionado() {
		return servicoRelacionado;
	}

	/**
	 * Set: servicoRelacionado.
	 * 
	 * @param servicoRelacionado
	 *            the servico relacionado
	 */
	public void setServicoRelacionado(Integer servicoRelacionado) {
		this.servicoRelacionado = servicoRelacionado;
	}

	/**
	 * Get: servicoDesc.
	 * 
	 * @return servicoDesc
	 */
	public String getServicoDesc() {
		return servicoDesc;
	}

	/**
	 * Set: servicoDesc.
	 * 
	 * @param servicoDesc
	 *            the servico desc
	 */
	public void setServicoDesc(String servicoDesc) {
		this.servicoDesc = servicoDesc;
	}

	/**
	 * Get: servicoRelacionadoDesc.
	 * 
	 * @return servicoRelacionadoDesc
	 */
	public String getServicoRelacionadoDesc() {
		return servicoRelacionadoDesc;
	}

	/**
	 * Set: servicoRelacionadoDesc.
	 * 
	 * @param servicoRelacionadoDesc
	 *            the servico relacionado desc
	 */
	public void setServicoRelacionadoDesc(String servicoRelacionadoDesc) {
		this.servicoRelacionadoDesc = servicoRelacionadoDesc;
	}

	/**
	 * Get: complementoInclusao.
	 * 
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 * 
	 * @param complementoInclusao
	 *            the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 * 
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 * 
	 * @param complementoManutencao
	 *            the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 * 
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 * 
	 * @param dataHoraInclusao
	 *            the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 * 
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 * 
	 * @param dataHoraManutencao
	 *            the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 * 
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 * 
	 * @param tipoCanalInclusao
	 *            the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 * 
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 * 
	 * @param tipoCanalManutencao
	 *            the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 * 
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 * 
	 * @param usuarioInclusao
	 *            the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 * 
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 * 
	 * @param usuarioManutencao
	 *            the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: obrigatoriedade.
	 * 
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 * 
	 * @param obrigatoriedade
	 *            the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: servicoComposto.
	 * 
	 * @return servicoComposto
	 */
	public Long getServicoComposto() {
		return servicoComposto;
	}

	/**
	 * Set: servicoComposto.
	 * 
	 * @param servicoComposto
	 *            the servico composto
	 */
	public void setServicoComposto(Long servicoComposto) {
		this.servicoComposto = servicoComposto;
	}

	/**
	 * Get: naturezaServico.
	 * 
	 * @return naturezaServico
	 */
	public Integer getNaturezaServico() {
		return naturezaServico;
	}

	/**
	 * Set: naturezaServico.
	 * 
	 * @param naturezaServico
	 *            the natureza servico
	 */
	public void setNaturezaServico(Integer naturezaServico) {
		this.naturezaServico = naturezaServico;
	}

	/**
	 * Get: listaTipoServico.
	 * 
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 * 
	 * @param listaTipoServico
	 *            the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: modalidadeServicoFiltro.
	 * 
	 * @return modalidadeServicoFiltro
	 */
	public Integer getModalidadeServicoFiltro() {
		return modalidadeServicoFiltro;
	}

	/**
	 * Set: modalidadeServicoFiltro.
	 * 
	 * @param modalidadeServicoFiltro
	 *            the modalidade servico filtro
	 */
	public void setModalidadeServicoFiltro(Integer modalidadeServicoFiltro) {
		this.modalidadeServicoFiltro = modalidadeServicoFiltro;
	}

	/**
	 * Get: tipoServico.
	 * 
	 * @return tipoServico
	 */
	public Integer getTipoServico() {
		return tipoServico;
	}

	/**
	 * Set: tipoServico.
	 * 
	 * @param tipoServico
	 *            the tipo servico
	 */
	public void setTipoServico(Integer tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Get: manterServicoRelacionado.
	 * 
	 * @return manterServicoRelacionado
	 */
	public IManterServicoRelacionadosService getManterServicoRelacionado() {
		return manterServicoRelacionado;
	}

	/**
	 * Set: manterServicoRelacionado.
	 * 
	 * @param manterServicoRelacionado
	 *            the manter servico relacionado
	 */
	public void setManterServicoRelacionado(
			IManterServicoRelacionadosService manterServicoRelacionado) {
		this.manterServicoRelacionado = manterServicoRelacionado;
	}

	/**
	 * Get: listaModalidadeServicoFiltro.
	 * 
	 * @return listaModalidadeServicoFiltro
	 */
	public List<SelectItem> getListaModalidadeServicoFiltro() {
		return listaModalidadeServicoFiltro;
	}

	/**
	 * Set: listaModalidadeServicoFiltro.
	 * 
	 * @param listaModalidadeServicoFiltro
	 *            the lista modalidade servico filtro
	 */
	public void setListaModalidadeServicoFiltro(
			List<SelectItem> listaModalidadeServicoFiltro) {
		this.listaModalidadeServicoFiltro = listaModalidadeServicoFiltro;
	}

	/**
	 * Get: listaModalidadeServicoFiltroHash.
	 * 
	 * @return listaModalidadeServicoFiltroHash
	 */
	Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoFiltroHash() {
		return listaModalidadeServicoFiltroHash;
	}

	/**
	 * Set lista modalidade servico filtro hash.
	 * 
	 * @param listaModalidadeServicoFiltroHash
	 *            the lista modalidade servico filtro hash
	 */
	void setListaModalidadeServicoFiltroHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoFiltroHash) {
		this.listaModalidadeServicoFiltroHash = listaModalidadeServicoFiltroHash;
	}

	/**
	 * Get: listaModalidadeServicoHash.
	 * 
	 * @return listaModalidadeServicoHash
	 */
	Map<Integer, ListarModalidadeSaidaDTO> getListaModalidadeServicoHash() {
		return listaModalidadeServicoHash;
	}

	/**
	 * Set lista modalidade servico hash.
	 * 
	 * @param listaModalidadeServicoHash
	 *            the lista modalidade servico hash
	 */
	void setListaModalidadeServicoHash(
			Map<Integer, ListarModalidadeSaidaDTO> listaModalidadeServicoHash) {
		this.listaModalidadeServicoHash = listaModalidadeServicoHash;
	}

	/**
	 * Get: listaGrid.
	 * 
	 * @return listaGrid
	 */
	public List<ListarServicoRelacionadoSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 * 
	 * @param listaGrid
	 *            the lista grid
	 */
	public void setListaGrid(List<ListarServicoRelacionadoSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Get: dsServicoComposto.
	 * 
	 * @return dsServicoComposto
	 */
	public String getDsServicoComposto() {
		return dsServicoComposto;
	}

	/**
	 * Set: dsServicoComposto.
	 * 
	 * @param dsServicoComposto
	 *            the ds servico composto
	 */
	public void setDsServicoComposto(String dsServicoComposto) {
		this.dsServicoComposto = dsServicoComposto;
	}

	/**
	 * Get: listaServicoComposto.
	 * 
	 * @return listaServicoComposto
	 */
	public List<SelectItem> getListaServicoComposto() {
		return listaServicoComposto;
	}

	/**
	 * Set: listaServicoComposto.
	 * 
	 * @param listaServicoComposto
	 *            the lista servico composto
	 */
	public void setListaServicoComposto(List<SelectItem> listaServicoComposto) {
		this.listaServicoComposto = listaServicoComposto;
	}

	/**
	 * Get: listaServicoCompostoHash.
	 * 
	 * @return listaServicoCompostoHash
	 */
	Map<Long, ListaServicoCompostoSaidaDTO> getListaServicoCompostoHash() {
		return listaServicoCompostoHash;
	}

	/**
	 * Set lista servico composto hash.
	 * 
	 * @param listaServicoCompostoHash
	 *            the lista servico composto hash
	 */
	void setListaServicoCompostoHash(
			Map<Long, ListaServicoCompostoSaidaDTO> listaServicoCompostoHash) {
		this.listaServicoCompostoHash = listaServicoCompostoHash;
	}

	/**
	 * Get: listaTipoServicoIncluir.
	 * 
	 * @return listaTipoServicoIncluir
	 */
	public List<SelectItem> getListaTipoServicoIncluir() {
		return listaTipoServicoIncluir;
	}

	/**
	 * Set: listaTipoServicoIncluir.
	 * 
	 * @param listaTipoServicoIncluir
	 *            the lista tipo servico incluir
	 */
	public void setListaTipoServicoIncluir(
			List<SelectItem> listaTipoServicoIncluir) {
		this.listaTipoServicoIncluir = listaTipoServicoIncluir;
	}

	/**
	 * Is bto acionado.
	 * 
	 * @return true, if is bto acionado
	 */
	public boolean isBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 * 
	 * @param btoAcionado
	 *            the bto acionado
	 */
	public void setBtoAcionado(boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: listaTipoServicoHash.
	 * 
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 * 
	 * @param listaTipoServicoHash
	 *            the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(
			Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: listaTipoServicoIncluirHash.
	 * 
	 * @return listaTipoServicoIncluirHash
	 */
	public Map<Integer, ListarServicosSaidaDTO> getListaTipoServicoIncluirHash() {
		return listaTipoServicoIncluirHash;
	}

	/**
	 * Set lista tipo servico incluir hash.
	 * 
	 * @param listaTipoServicoIncluirHash
	 *            the lista tipo servico incluir hash
	 */
	public void setListaTipoServicoIncluirHash(
			Map<Integer, ListarServicosSaidaDTO> listaTipoServicoIncluirHash) {
		this.listaTipoServicoIncluirHash = listaTipoServicoIncluirHash;
	}

	/**
	 * Gets the nr ordenacao servico relacionado.
	 *
	 * @return the nr ordenacao servico relacionado
	 */
	public Integer getNrOrdenacaoServicoRelacionado() {
		return nrOrdenacaoServicoRelacionado;
	}

	/**
	 * Sets the nr ordenacao servico relacionado.
	 *
	 * @param nrOrdenacaoServicoRelacionado the nr ordenacao servico relacionado
	 */
	public void setNrOrdenacaoServicoRelacionado(
			Integer nrOrdenacaoServicoRelacionado) {
		this.nrOrdenacaoServicoRelacionado = nrOrdenacaoServicoRelacionado;
	}

	/**
	 * Gets the cd indicador servico negociavel.
	 *
	 * @return the cd indicador servico negociavel
	 */
	public String getCdIndicadorServicoNegociavel() {
		return cdIndicadorServicoNegociavel;
	}

	/**
	 * Sets the cd indicador servico negociavel.
	 *
	 * @param cdIndicadorServicoNegociavel the cd indicador servico negociavel
	 */
	public void setCdIndicadorServicoNegociavel(
			String cdIndicadorServicoNegociavel) {
		this.cdIndicadorServicoNegociavel = cdIndicadorServicoNegociavel;
	}

	/**
	 * Gets the cd indicador negociavel servico.
	 *
	 * @return the cd indicador negociavel servico
	 */
	public String getCdIndicadorNegociavelServico() {
		return cdIndicadorNegociavelServico;
	}

	/**
	 * Sets the cd indicador negociavel servico.
	 *
	 * @param cdIndicadorNegociavelServico the cd indicador negociavel servico
	 */
	public void setCdIndicadorNegociavelServico(
			String cdIndicadorNegociavelServico) {
		this.cdIndicadorNegociavelServico = cdIndicadorNegociavelServico;
	}

	/**
	 * Gets the cd indicador contigencia servico.
	 *
	 * @return the cd indicador contigencia servico
	 */
	public String getCdIndicadorContigenciaServico() {
		return cdIndicadorContigenciaServico;
	}

	/**
	 * Sets the cd indicador contigencia servico.
	 *
	 * @param cdIndicadorContigenciaServico the cd indicador contigencia servico
	 */
	public void setCdIndicadorContigenciaServico(
			String cdIndicadorContigenciaServico) {
		this.cdIndicadorContigenciaServico = cdIndicadorContigenciaServico;
	}

	/**
	 * Gets the cd indicador manutencao servico.
	 *
	 * @return the cd indicador manutencao servico
	 */
	public String getCdIndicadorManutencaoServico() {
		return cdIndicadorManutencaoServico;
	}

	/**
	 * Sets the cd indicador manutencao servico.
	 *
	 * @param cdIndicadorManutencaoServico the cd indicador manutencao servico
	 */
	public void setCdIndicadorManutencaoServico(
			String cdIndicadorManutencaoServico) {
		this.cdIndicadorManutencaoServico = cdIndicadorManutencaoServico;
	}

	/**
	 * Checks if is chk negociacao.
	 *
	 * @return true, if checks if is chk negociacao
	 */
	public boolean isChkNegociacao() {
		return chkNegociacao;
	}

	/**
	 * Sets the chk negociacao.
	 *
	 * @param chkNegociacao the chk negociacao
	 */
	public void setChkNegociacao(boolean chkNegociacao) {
		this.chkNegociacao = chkNegociacao;
	}

	/**
	 * Checks if is chk contingencia.
	 *
	 * @return true, if checks if is chk contingencia
	 */
	public boolean isChkContingencia() {
		return chkContingencia;
	}

	/**
	 * Sets the chk contingencia.
	 *
	 * @param chkContingencia the chk contingencia
	 */
	public void setChkContingencia(boolean chkContingencia) {
		this.chkContingencia = chkContingencia;
	}

	/**
	 * Checks if is chk manutencao.
	 *
	 * @return true, if checks if is chk manutencao
	 */
	public boolean isChkManutencao() {
		return chkManutencao;
	}

	/**
	 * Sets the chk manutencao.
	 *
	 * @param chkManutencao the chk manutencao
	 */
	public void setChkManutencao(boolean chkManutencao) {
		this.chkManutencao = chkManutencao;
	}

	/**
	 * Gets the entrada alteracao.
	 *
	 * @return the entrada alteracao
	 */
	public AlterarServicoRelacionadoEntradaDTO getEntradaAlteracao() {
		return entradaAlteracao;
	}

	/**
	 * Sets the entrada alteracao.
	 *
	 * @param entradaAlteracao the entrada alteracao
	 */
	public void setEntradaAlteracao(
			AlterarServicoRelacionadoEntradaDTO entradaAlteracao) {
		this.entradaAlteracao = entradaAlteracao;
	}

	/**
	 * Gets the saida detalhe.
	 *
	 * @return the saida detalhe
	 */
	public DetalharServicoRelacionadoSaidaDTO getSaidaDetalhe() {
		return saidaDetalhe;
	}

	/**
	 * Sets the saida detalhe.
	 *
	 * @param saidaDetalhe the saida detalhe
	 */
	public void setSaidaDetalhe(DetalharServicoRelacionadoSaidaDTO saidaDetalhe) {
		this.saidaDetalhe = saidaDetalhe;
	}

	/**
	 * Gets the ds indicador servico negociavel.
	 *
	 * @return the ds indicador servico negociavel
	 */
	public String getDsIndicadorServicoNegociavel() {
		return dsIndicadorServicoNegociavel;
	}

	/**
	 * Sets the ds indicador servico negociavel.
	 *
	 * @param dsIndicadorServicoNegociavel the ds indicador servico negociavel
	 */
	public void setDsIndicadorServicoNegociavel(
			String dsIndicadorServicoNegociavel) {
		this.dsIndicadorServicoNegociavel = dsIndicadorServicoNegociavel;
	}
	/**
	 * Get: comboService.
	 * 
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 * 
	 * @param comboService
	 *            the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	public List<SelectItem> getListarArgumentoPesquisaServico() {
		return listarArgumentoPesquisaServico;
	}

	public void setListarArgumentoPesquisaServico(
			List<SelectItem> listarArgumentoPesquisaServico) {
		this.listarArgumentoPesquisaServico = listarArgumentoPesquisaServico;
	}

}
