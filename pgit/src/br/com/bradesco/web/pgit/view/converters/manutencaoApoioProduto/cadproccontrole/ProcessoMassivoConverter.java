/*
 * Nome: br.com.bradesco.web.pgit.view.converters.manutencaoApoioProduto.cadproccontrole
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.converters.manutencaoApoioProduto.cadproccontrole;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoSaidaDTO;

/**
 * Nome: ProcessoMassivoConverter
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ProcessoMassivoConverter implements Converter {

	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		if (value == null || value.trim().equals("")) {
			return null;
		}
		
		String[] arrProcessoMassivo = value.split(";");
		if (arrProcessoMassivo == null || arrProcessoMassivo.length != 2) {
			return null;
		}

		ProcessoMassivoSaidaDTO consulta = new ProcessoMassivoSaidaDTO();
		consulta.setCdSistema(arrProcessoMassivo[0]);
		consulta.setCdProcessoSistema(Integer.parseInt(arrProcessoMassivo[1]));

		return consulta;
	}

	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		if (value == null) {
			return null;
		}

		ProcessoMassivoSaidaDTO consulta = (ProcessoMassivoSaidaDTO) value;
		StringBuilder consultaProcessoMassivo = new StringBuilder();
		consultaProcessoMassivo.append(consulta.getCdSistema());
		consultaProcessoMassivo.append(";");
		consultaProcessoMassivo.append(consulta.getCdProcessoSistema());

		return consultaProcessoMassivo.toString();
	}

}
