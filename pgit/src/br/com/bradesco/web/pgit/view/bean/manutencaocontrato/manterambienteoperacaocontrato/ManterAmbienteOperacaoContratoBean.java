/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterambienteoperacaocontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterambienteoperacaocontrato;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.IManterAmbienteOperacaoContratoService;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ConsultarSolicitacaoMudancaAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.ExcluirAlteracaoAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbienteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterambienteoperacaocontrato.bean.IncluirAlteracaoAmbienteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarModalidadeTipoServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarModalidadeTipoServicoSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: ManterAmbienteOperacaoContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterAmbienteOperacaoContratoBean {

	// //////////////////////////
	// DECLARA��O DE VARI�VEIS//
	// ////////////////////////

	// Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo VOLTAR. */
	private static final String VOLTAR = "VOLTAR";

	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo manterContratoService. */
	private IManterContratoService manterContratoService;

	/** Atributo manterAmbienteOperacaoContratoServiceImpl. */
	private IManterAmbienteOperacaoContratoService manterAmbienteOperacaoContratoServiceImpl;

	/** Atributo cpfCnpjClienteMaster. */
	private String cpfCnpjClienteMaster;

	/** Atributo nomeRazaoSocialClienteMaster. */
	private String nomeRazaoSocialClienteMaster;

	/** Atributo grupoEconomico. */
	private String grupoEconomico;

	/** Atributo atividadeEconomica. */
	private String atividadeEconomica;

	/** Atributo segmento. */
	private String segmento;

	/** Atributo subSegmento. */
	private String subSegmento;

	/** Atributo empresa. */
	private String empresa;

	/** Atributo tipo. */
	private String tipo;

	/** Atributo numero. */
	private String numero;

	/** Atributo situacao. */
	private String situacao;

	/** Atributo motivo. */
	private String motivo;

	/** Atributo participacao. */
	private String participacao;

	/** Atributo cdEmpresa. */
	private Long cdEmpresa;

	/** Atributo cdTipo. */
	private Integer cdTipo;

	/** Atributo cnpjCpf. */
	private String cnpjCpf;

	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo periodoSolicitacaoInicio. */
	private Date periodoSolicitacaoInicio;

	/** Atributo periodoSolicitacaoFinal. */
	private Date periodoSolicitacaoFinal;

	/** Atributo listaGrid. */
	private List<ConsultarSolicitacaoMudancaAmbienteSaidaDTO> listaGrid;

	/** Atributo listaControle. */
	private List<SelectItem> listaControle;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo mostraLimpar. */
	private boolean mostraLimpar = false;

	/** Atributo servicoDesc. */
	private String servicoDesc;

	/** Atributo servico. */
	private Integer servico;

	/** Atributo modalidade. */
	private Integer modalidade;

	/** Atributo modalidadeDesc. */
	private String modalidadeDesc;

	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();

	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();

	/** Atributo listaModalidade. */
	private List<SelectItem> listaModalidade = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeHash. */
	private Map<Integer, String> listaModalidadeHash = new HashMap<Integer, String>();

	/** Atributo dataProgramada. */
	private Date dataProgramada;

	/** Atributo dataProgramadaSolicitacaoDesc. */
	private String dataProgramadaSolicitacaoDesc;

	/** Atributo habilitaRadioAmbiente. */
	private Boolean habilitaRadioAmbiente;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo tipoMudancaSolicitacao. */
	private String tipoMudancaSolicitacao;
	
	/** Atributo habilitaMudancaSolicitacao. */
	private boolean habilitaMudancaSolicitacao;

	/** Atributo formaEfetivacaoSolicitacao. */
	private String formaEfetivacaoSolicitacao;

	/** Atributo manterOperacoesAmbienteAtualSolicitacao. */
	private String manterOperacoesAmbienteAtualSolicitacao;

	/** Atributo dataProgramadaSolicitacao. */
	private Date dataProgramadaSolicitacao;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

	/** Atributo hiddenObrigatoriedade. */
	private String hiddenObrigatoriedade;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;
	
	/** Atributo listaServico. */
	private List<ListarModalidadeTipoServicoSaidaDTO> listaServico = new ArrayList<ListarModalidadeTipoServicoSaidaDTO>();

	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo nomeRazaoParticipante. */
	private String nomeRazaoParticipante;

	// ///////////////////////
	// M�TODOS DIVERSOS //
	// /////////////////////

	/**
	 * Valorizar tipo ambiente.
	 */
	public void valorizarTipoAmbiente() {

		if(servico != null && servico > 0){
		
			ListarModalidadeTipoServicoSaidaDTO saidaAux = new ListarModalidadeTipoServicoSaidaDTO();
			
			String dsAmbiente = "";
	
			for(int x=0;x<listaServico.size();x++){
				saidaAux = listaServico.get(x);
				
				if(saidaAux.getCdProdutoOperacaoRelacionado().equals(servico)){
					dsAmbiente = saidaAux.getCdAmbienteServicoContrato();
					break;
				}
			}
			
			if(dsAmbiente.equalsIgnoreCase("TESTE")){
				setTipoMudancaSolicitacao("0");
			}else{
				setTipoMudancaSolicitacao("1");
			}
			
		}else{
			
			setTipoMudancaSolicitacao(null);
			
		}
		
	}

	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {

		this.identificacaoClienteContratoBean
						.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
						.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteMantAmbienteOperacaoContrato");
		identificacaoClienteContratoBean.setPaginaRetorno("pesqManterSolicitacoesMudancaAmbienteOperacao");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.limparDadosCliente();

		setMostraLimpar(false);

	}

	/**
	 * Carrega lista tipo servico.
	 */
	public void carregaListaTipoServico() {

		this.listaTipoServico = new ArrayList<SelectItem>();

		ListarModalidadeTipoServicoEntradaDTO entrada = new ListarModalidadeTipoServicoEntradaDTO();
		
		ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());
		
		entrada.setCdModalidade(0);
		entrada.setCdPessoaJuridica(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
		entrada.setCdTipoContrato(listarContratosPgitSaidaDTO.getCdTipoContrato());
		entrada.setCdTipoServico(0);
		entrada.setNrSequenciaContrato(listarContratosPgitSaidaDTO.getNrSequenciaContrato());
		
		listaServico = manterContratoService.listarModalidadeTipoServico(entrada);

		listaTipoServicoHash.clear();

		for (ListarModalidadeTipoServicoSaidaDTO combo : listaServico) {
			listaTipoServicoHash.put(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado());
			listaTipoServico.add(new SelectItem(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado()));				
		}

	}

	/**
	 * Carrega lista.
	 *
	 * @return the string
	 */
	public String carregaLista() {
		try {

			setMostraLimpar(true);

			ConsultarSolicitacaoMudancaAmbienteEntradaDTO entradaDTO = new ConsultarSolicitacaoMudancaAmbienteEntradaDTO();

			ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = identificacaoClienteContratoBean
							.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

			/* Dados do Contrato */

			entradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO.getNrSequenciaContrato());

			/* Dados Argumentos de Pesquisa */

			entradaDTO.setDtFimSolicitacao("0000000000");
			entradaDTO.setDtInicioSolicitacao("0000000000");

			setListaGrid(getManterAmbienteOperacaoContratoServiceImpl().consultarSolicitacaoMudancaAmbiente(entradaDTO));

			listaControle = new ArrayList<SelectItem>();

			for (int i = 0; i < listaGrid.size(); i++) {
				listaControle.add(new SelectItem(i, ""));
			}

			setItemSelecionadoLista(null);

			if (listaGrid.size() == 0) {
				setMostraLimpar(false);
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setMostraLimpar(false);

		}

		return "CONSULTAR";
	}
	
	/**
	 * Carrega lista retorno.
	 *
	 * @return the string
	 */
	public String carregaListaRetorno() {
		try {

			setMostraLimpar(true);

			ConsultarSolicitacaoMudancaAmbienteEntradaDTO entradaDTO = new ConsultarSolicitacaoMudancaAmbienteEntradaDTO();

			ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO = identificacaoClienteContratoBean
							.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

			/* Dados do Contrato */

			entradaDTO.setCdPessoaJuridica(listarContratosPgitSaidaDTO.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(listarContratosPgitSaidaDTO.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(listarContratosPgitSaidaDTO.getNrSequenciaContrato());

			/* Dados Argumentos de Pesquisa */

			entradaDTO.setDtFimSolicitacao("0000000000");
			entradaDTO.setDtInicioSolicitacao("0000000000");

			setListaGrid(getManterAmbienteOperacaoContratoServiceImpl().consultarSolicitacaoMudancaAmbiente(entradaDTO));

			listaControle = new ArrayList<SelectItem>();

			for (int i = 0; i < listaGrid.size(); i++) {
				listaControle.add(new SelectItem(i, ""));
			}

			setItemSelecionadoLista(null);

			if (listaGrid.size() == 0) {
				setMostraLimpar(false);
			}

		} catch (PdcAdapterFunctionalException p) {
			setListaGrid(null);
			setItemSelecionadoLista(null);
			setMostraLimpar(false);

		}

		return "CONSULTAR";
	}

	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {
		setMostraLimpar(false);
		setPeriodoSolicitacaoFinal(new Date());
		setPeriodoSolicitacaoInicio(new Date());
		setListaControle(null);
		setListaGrid(null);
		setItemSelecionadoLista(null);
		return "LIMPAR_DADOS";
	}

	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar() {
		setItemSelecionadoLista(null);
		return "LIMPAR";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {

		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							"conFinalidadeEnderecoProdutoOperacao", BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "DETALHAR";
	}

	/**
	 * Preenche dados.
	 */
	private void preencheDados() {

		ConsultarSolicitacaoAmbienteEntradaDTO entrada = new ConsultarSolicitacaoAmbienteEntradaDTO(); // Detalhar

		ConsultarSolicitacaoMudancaAmbienteSaidaDTO consultarSolicitacaoMudancaAmbienteSaidaDTO = getListaGrid().get(
						getItemSelecionadoLista()); // Pega o item selecionado da Lista

		entrada.setCdSolicitacaoPagamentoIntegrado(consultarSolicitacaoMudancaAmbienteSaidaDTO.getCdSolicitacao());
		entrada.setNrSolicitacaoPagamentoIntegrado(consultarSolicitacaoMudancaAmbienteSaidaDTO.getNrSolicitacao());

		ConsultarSolicitacaoAmbienteSaidaDTO saida = getManterAmbienteOperacaoContratoServiceImpl()
						.consultarSolicitacaoAmbiente(entrada); // Detalhar

		/* Dados de Saida */
		setServicoDesc(saida.getDsServico());
		setDataProgramadaSolicitacaoDesc(saida.getDtProgramada());
		setSituacao(consultarSolicitacaoMudancaAmbienteSaidaDTO.getDsSituacaoSolicitacao());
		setFormaEfetivacaoSolicitacao(saida.getCdFormaEfetivacao());
		setTipoMudancaSolicitacao(saida.getCdTipoMudanca());
		setManterOperacoesAmbienteAtualSolicitacao("NAO".equals(saida.getCdManterOperacao()) ? saida.getCdManterOperacao() + " (A limpeza dos dados � efetivada em processamento noturno)" : saida.getCdManterOperacao());

		setDataHoraManutencao(saida.getDtManutencao());
		setDataHoraInclusao(saida.getDtInclusao());

		setTipoCanalInclusao(saida.getCdTipoCanalInclusao() != 0 ? saida.getCdTipoCanalInclusao() + " - "
						+ saida.getDsCanalInclusao() : "");
		setTipoCanalManutencao(saida.getCdTipoCanalManutencao() == 0 ? "" : saida.getCdTipoCanalManutencao() + " - "
						+ saida.getDsCanalManutencao());

		setUsuarioInclusao(saida.getCdUsuarioInclusao());
		setUsuarioManutencao(saida.getCdUsuarioManutencao());

		setComplementoInclusao(saida.getDsCanalInclusao() == null || saida.getDsCanalInclusao().equals("0") ? ""
						: saida.getDsCanalInclusao());
		setComplementoManutencao(saida.getDsCanalManutencao() == null || saida.getDsCanalManutencao().equals("0") ? ""
						: saida.getDsCanalManutencao());

		// em 11/8/2010 para atender slide 175 do doc 27, 28 e 29.07.10 - Corre��es Telas da homologa��o do dia 14 e
		// 15.07.10.ppt
		setModalidadeDesc(saida.getDsModalidade());

	}

	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							"conFinalidadeEnderecoProdutoOperacao", BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "EXCLUIR";
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		String ambienteAtivoSolicitacao = "";
			
		if(itemSelecionadoLista != null){
			ambienteAtivoSolicitacao = getListaGrid().get(itemSelecionadoLista).getCdAmbienteAtivoSolicitacao();
		}
	
		if(ambienteAtivoSolicitacao.equalsIgnoreCase("TESTE")){
			setTipoMudancaSolicitacao("0");
			setHabilitaMudancaSolicitacao(true);
		}
		else{
			setTipoMudancaSolicitacao(null);
			setHabilitaMudancaSolicitacao(false);
		}
		
		setServico(0);		
		setFormaEfetivacaoSolicitacao("");
		setManterOperacoesAmbienteAtualSolicitacao("");
		setDataProgramadaSolicitacao(new Date());
		setModalidade(0);
		listaModalidade.clear();
		carregaListaTipoServico();

		return "INCLUIR";
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {

		if (getHiddenObrigatoriedade().equals("T")) {

			SimpleDateFormat simpleDataFormat = new SimpleDateFormat("dd/MM/yyyy");
			setDataProgramadaSolicitacaoDesc(simpleDataFormat.format(getDataProgramadaSolicitacao()));
			setServicoDesc((String) getListaTipoServicoHash().get(getServico()));
			setModalidadeDesc((String) getListaModalidadeHash().get(getModalidade()));
			return "AVANCAR";
		}

		return "";

	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {

		try {
			IncluirAlteracaoAmbienteEntradaDTO incluirAlteracaoAmbienteEntradaDTO = new IncluirAlteracaoAmbienteEntradaDTO();

			/* Dados Contratos */
			ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
							identificacaoClienteContratoBean.getItemSelecionadoLista());
			incluirAlteracaoAmbienteEntradaDTO.setCdPessoaJuridicaNegocio(Integer.parseInt(String.valueOf(saidaDTO
							.getCdPessoaJuridica())));
			incluirAlteracaoAmbienteEntradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			incluirAlteracaoAmbienteEntradaDTO.setNrSequenciaContratoNegocio(Integer.parseInt(String.valueOf(saidaDTO
							.getNrSequenciaContrato())));

			/* DE-PARA enviado por email no dia "Email dia sexta-feira, 23 de julho de 2010 15:01" */

			// Esse campo deve ser valorizados com ZERO - Email dia sexta-feira, 23 de julho de 2010 15:01
			incluirAlteracaoAmbienteEntradaDTO.setCdRelacionamentoProduto(0);

			// Esse campo deve ser valorizado com 01 � quando a op��o na tela �manter Opera��o no ambiente atual:� for
			// Sim e com 02 Quando a op��o for N�O.
			incluirAlteracaoAmbienteEntradaDTO
							.setCdIndicadorArmazenamentoInformacao(getManterOperacoesAmbienteAtualSolicitacao().equals(
											"0") ? 1 : 2);

			/*
			 * O campo cdAmbienteAtivoSolicitacao deve ser formatado com PROD quando for secionada a op��o �TESTE PARA
			 * PRODUCAO� e com TEST quando for escolhido a op��o �PRODUCAO PARA TESTE�
			 */
			incluirAlteracaoAmbienteEntradaDTO
							.setCdAmbienteAtivoSolicitacao(getTipoMudancaSolicitacao().equals("0") ? "PROD" : "TEST");

			// o Campo cdFormaMudanca deve ser formatado com 1 quando for �BATCH� , e com 2 quando for de �ON-LINE�
			incluirAlteracaoAmbienteEntradaDTO.setCdFormaMudanca(getFormaEfetivacaoSolicitacao().equals("0") ? 1 : 2);

			// Como Modalidade
			incluirAlteracaoAmbienteEntradaDTO.setCdModalidade(getModalidade());

			// Combo Servico
			incluirAlteracaoAmbienteEntradaDTO.setCdServico(getServico());

			DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
			if (getFormaEfetivacaoSolicitacao().equals("0")) {
				incluirAlteracaoAmbienteEntradaDTO.setDtMudanca(df.format(getDataProgramadaSolicitacao()));
			}

			IncluirAlteracaoAmbienteSaidaDTO incluirAlteracaoAmbienteSaidaDTO = getManterAmbienteOperacaoContratoServiceImpl()
							.incluirAlteracaoAmbiente(incluirAlteracaoAmbienteEntradaDTO);

			carregaListaRetorno();
			setItemSelecionadoLista(null);
			BradescoFacesUtils.addInfoModalMessage("(" + incluirAlteracaoAmbienteSaidaDTO.getCodMensagem() + ") "
							+ incluirAlteracaoAmbienteSaidaDTO.getMensagem(),
							"solManterSolicitacoesMudancaAmbienteOperacao", BradescoViewExceptionActionType.ACTION,
							false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			return null;
		}

		return "";
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {

        try {

                ExcluirAlteracaoAmbienteEntradaDTO excluirAlteracaoAmbienteEntradaDTO = new ExcluirAlteracaoAmbienteEntradaDTO();

                ConsultarSolicitacaoMudancaAmbienteSaidaDTO consultarSolicitacaoMudancaAmbienteSaidaDTO = getListaGrid()
                                                .get(getItemSelecionadoLista()); // Item selecionado da Lista
                ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
                                                identificacaoClienteContratoBean.getItemSelecionadoLista());

                excluirAlteracaoAmbienteEntradaDTO
                                                .setCdSolicitacaoPagamentoIntegrado(consultarSolicitacaoMudancaAmbienteSaidaDTO
                                                                                .getCdSolicitacao());
                excluirAlteracaoAmbienteEntradaDTO
                                                .setNrSolicitacaoPagamentoIntegrado(consultarSolicitacaoMudancaAmbienteSaidaDTO
                                                                                .getNrSolicitacao());
                excluirAlteracaoAmbienteEntradaDTO.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
                excluirAlteracaoAmbienteEntradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
                excluirAlteracaoAmbienteEntradaDTO.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());

                ExcluirAlteracaoAmbienteSaidaDTO excluirAlteracaoAmbienteSaidaDTO = getManterAmbienteOperacaoContratoServiceImpl()
                                                .excluirAlteracaoAmbiente(excluirAlteracaoAmbienteEntradaDTO);

                BradescoFacesUtils.addInfoModalMessage("(" + excluirAlteracaoAmbienteSaidaDTO.getCodMensagem() + ") "
                                                + excluirAlteracaoAmbienteSaidaDTO.getMensagem(),
                                                "#{manterAmbienteOperacaoContratoBean.carregaLista}", BradescoViewExceptionActionType.ACTION,
                                                false);

                setItemSelecionadoLista(null);

        } catch (PdcAdapterFunctionalException p) {
                BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
                                                false);
                return null;
        }

        return "";

	}


	/**
	 * Solicitar.
	 *
	 * @return the string
	 */
	public String solicitar() {

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		setCpfCnpjClienteMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialClienteMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomico(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomica(saidaDTO.getDsAtividadeEconomica());
		setSegmento(saidaDTO.getDsSegmentoCliente());
		setSubSegmento(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacao(saidaDTO.getDsSituacaoContrato());
		setMotivo(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setDescricaoContrato(saidaDTO.getDsContrato());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		
		} else {
			setCpfCnpjParticipante("");
			setNomeRazaoParticipante("");
		}

		setPeriodoSolicitacaoInicio(new Date());
		setPeriodoSolicitacaoFinal(new Date());

		carregaLista();

		return "SOLICITAR";
	}

	/**
	 * Desabilita ambiente.
	 *
	 * @return the string
	 */
	public String desabilitaAmbiente() {
		if("1".equals(getTipoMudancaSolicitacao())){
			setManterOperacoesAmbienteAtualSolicitacao("0");
			setHabilitaRadioAmbiente(true);
		}else{
			setHabilitaRadioAmbiente(false);
		}
		return "";
	}

	/**
	 * Limpar pesquisar.
	 *
	 * @return the string
	 */
	public String limparPesquisar() {
		return "LIMPAR";
	}

	/**
	 * Voltar detalhar.
	 *
	 * @return the string
	 */
	public String voltarDetalhar() {
		return VOLTAR;
	}

	/**
	 * Voltar excluir.
	 *
	 * @return the string
	 */
	public String voltarExcluir() {
		return VOLTAR;
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		return VOLTAR;
	}

	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir() {
		return VOLTAR;
	}

	/**
	 * Voltar incluir2.
	 *
	 * @return the string
	 */
	public String voltarIncluir2() {
		return VOLTAR;
	}

	// ////////////////////
	// GETTERS E SETTERS//
	// //////////////////

	/**
	 * Get: atividadeEconomica.
	 *
	 * @return atividadeEconomica
	 */
	public String getAtividadeEconomica() {
		return atividadeEconomica;
	}

	/**
	 * Set: atividadeEconomica.
	 *
	 * @param atividadeEconomica the atividade economica
	 */
	public void setAtividadeEconomica(String atividadeEconomica) {
		this.atividadeEconomica = atividadeEconomica;
	}

	/**
	 * Get: cpfCnpjClienteMaster.
	 *
	 * @return cpfCnpjClienteMaster
	 */
	public String getCpfCnpjClienteMaster() {
		return cpfCnpjClienteMaster;
	}

	/**
	 * Set: cpfCnpjClienteMaster.
	 *
	 * @param cpfCnpjClienteMaster the cpf cnpj cliente master
	 */
	public void setCpfCnpjClienteMaster(String cpfCnpjClienteMaster) {
		this.cpfCnpjClienteMaster = cpfCnpjClienteMaster;
	}

	/**
	 * Get: grupoEconomico.
	 *
	 * @return grupoEconomico
	 */
	public String getGrupoEconomico() {
		return grupoEconomico;
	}

	/**
	 * Set: grupoEconomico.
	 *
	 * @param grupoEconomico the grupo economico
	 */
	public void setGrupoEconomico(String grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}

	/**
	 * Get: nomeRazaoSocialClienteMaster.
	 *
	 * @return nomeRazaoSocialClienteMaster
	 */
	public String getNomeRazaoSocialClienteMaster() {
		return nomeRazaoSocialClienteMaster;
	}

	/**
	 * Set: nomeRazaoSocialClienteMaster.
	 *
	 * @param nomeRazaoSocialClienteMaster the nome razao social cliente master
	 */
	public void setNomeRazaoSocialClienteMaster(String nomeRazaoSocialClienteMaster) {
		this.nomeRazaoSocialClienteMaster = nomeRazaoSocialClienteMaster;
	}

	/**
	 * Get: segmento.
	 *
	 * @return segmento
	 */
	public String getSegmento() {
		return segmento;
	}

	/**
	 * Set: segmento.
	 *
	 * @param segmento the segmento
	 */
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	/**
	 * Get: subSegmento.
	 *
	 * @return subSegmento
	 */
	public String getSubSegmento() {
		return subSegmento;
	}

	/**
	 * Set: subSegmento.
	 *
	 * @param subSegmento the sub segmento
	 */
	public void setSubSegmento(String subSegmento) {
		this.subSegmento = subSegmento;
	}

	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Get: motivo.
	 *
	 * @return motivo
	 */
	public String getMotivo() {
		return motivo;
	}

	/**
	 * Set: motivo.
	 *
	 * @param motivo the motivo
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Get: participacao.
	 *
	 * @return participacao
	 */
	public String getParticipacao() {
		return participacao;
	}

	/**
	 * Set: participacao.
	 *
	 * @param participacao the participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}

	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Get: periodoSolicitacaoFinal.
	 *
	 * @return periodoSolicitacaoFinal
	 */
	public Date getPeriodoSolicitacaoFinal() {
		return periodoSolicitacaoFinal;
	}

	/**
	 * Set: periodoSolicitacaoFinal.
	 *
	 * @param periodoSolicitacaoFinal the periodo solicitacao final
	 */
	public void setPeriodoSolicitacaoFinal(Date periodoSolicitacaoFinal) {
		this.periodoSolicitacaoFinal = periodoSolicitacaoFinal;
	}

	/**
	 * Get: periodoSolicitacaoInicio.
	 *
	 * @return periodoSolicitacaoInicio
	 */
	public Date getPeriodoSolicitacaoInicio() {
		return periodoSolicitacaoInicio;
	}

	/**
	 * Set: periodoSolicitacaoInicio.
	 *
	 * @param periodoSolicitacaoInicio the periodo solicitacao inicio
	 */
	public void setPeriodoSolicitacaoInicio(Date periodoSolicitacaoInicio) {
		this.periodoSolicitacaoInicio = periodoSolicitacaoInicio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ConsultarSolicitacaoMudancaAmbienteSaidaDTO> getListaGrid() {
		return listaGrid;
	}

	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ConsultarSolicitacaoMudancaAmbienteSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	/**
	 * Is mostra limpar.
	 *
	 * @return true, if is mostra limpar
	 */
	public boolean isMostraLimpar() {
		return mostraLimpar;
	}

	/**
	 * Set: mostraLimpar.
	 *
	 * @param mostraLimpar the mostra limpar
	 */
	public void setMostraLimpar(boolean mostraLimpar) {
		this.mostraLimpar = mostraLimpar;
	}

	/**
	 * Get: dataProgramada.
	 *
	 * @return dataProgramada
	 */
	public Date getDataProgramada() {
		return dataProgramada;
	}

	/**
	 * Set: dataProgramada.
	 *
	 * @param dataProgramada the data programada
	 */
	public void setDataProgramada(Date dataProgramada) {
		this.dataProgramada = dataProgramada;
	}

	/**
	 * Get: servicoDesc.
	 *
	 * @return servicoDesc
	 */
	public String getServicoDesc() {
		return servicoDesc;
	}

	/**
	 * Set: servicoDesc.
	 *
	 * @param servico the servico desc
	 */
	public void setServicoDesc(String servico) {
		this.servicoDesc = servico;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: dataProgramadaSolicitacao.
	 *
	 * @return dataProgramadaSolicitacao
	 */
	public Date getDataProgramadaSolicitacao() {
		return dataProgramadaSolicitacao;
	}

	/**
	 * Set: dataProgramadaSolicitacao.
	 *
	 * @param dataProgramadaSolicitacao the data programada solicitacao
	 */
	public void setDataProgramadaSolicitacao(Date dataProgramadaSolicitacao) {
		this.dataProgramadaSolicitacao = dataProgramadaSolicitacao;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: manterAmbienteOperacaoContratoServiceImpl.
	 *
	 * @return manterAmbienteOperacaoContratoServiceImpl
	 */
	public IManterAmbienteOperacaoContratoService getManterAmbienteOperacaoContratoServiceImpl() {
		return manterAmbienteOperacaoContratoServiceImpl;
	}

	/**
	 * Set: manterAmbienteOperacaoContratoServiceImpl.
	 *
	 * @param manterAmbienteOperacaoContratoServiceImpl the manter ambiente operacao contrato service impl
	 */
	public void setManterAmbienteOperacaoContratoServiceImpl(
					IManterAmbienteOperacaoContratoService manterAmbienteOperacaoContratoServiceImpl) {
		this.manterAmbienteOperacaoContratoServiceImpl = manterAmbienteOperacaoContratoServiceImpl;
	}

	/**
	 * Get: hiddenObrigatoriedade.
	 *
	 * @return hiddenObrigatoriedade
	 */
	public String getHiddenObrigatoriedade() {
		return hiddenObrigatoriedade;
	}

	/**
	 * Set: hiddenObrigatoriedade.
	 *
	 * @param hiddenObrigatoriedade the hidden obrigatoriedade
	 */
	public void setHiddenObrigatoriedade(String hiddenObrigatoriedade) {
		this.hiddenObrigatoriedade = hiddenObrigatoriedade;
	}

	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Get: cdTipo.
	 *
	 * @return cdTipo
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}

	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Get: cnpjCpf.
	 *
	 * @return cnpjCpf
	 */
	public String getCnpjCpf() {
		return cnpjCpf;
	}

	/**
	 * Set: cnpjCpf.
	 *
	 * @param cnpjCpf the cnpj cpf
	 */
	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: servico.
	 *
	 * @return servico
	 */
	public Integer getServico() {
		return servico;
	}

	/**
	 * Set: servico.
	 *
	 * @param servico the servico
	 */
	public void setServico(Integer servico) {
		this.servico = servico;
	}

	/**
	 * Get: dataProgramadaSolicitacaoDesc.
	 *
	 * @return dataProgramadaSolicitacaoDesc
	 */
	public String getDataProgramadaSolicitacaoDesc() {
		return dataProgramadaSolicitacaoDesc;
	}

	/**
	 * Set: dataProgramadaSolicitacaoDesc.
	 *
	 * @param dataProgramadaSolicitacaoDesc the data programada solicitacao desc
	 */
	public void setDataProgramadaSolicitacaoDesc(String dataProgramadaSolicitacaoDesc) {
		this.dataProgramadaSolicitacaoDesc = dataProgramadaSolicitacaoDesc;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Get: formaEfetivacaoSolicitacao.
	 *
	 * @return formaEfetivacaoSolicitacao
	 */
	public String getFormaEfetivacaoSolicitacao() {
		return formaEfetivacaoSolicitacao;
	}

	/**
	 * Set: formaEfetivacaoSolicitacao.
	 *
	 * @param formaEfetivacaoSolicitacao the forma efetivacao solicitacao
	 */
	public void setFormaEfetivacaoSolicitacao(String formaEfetivacaoSolicitacao) {
		this.formaEfetivacaoSolicitacao = formaEfetivacaoSolicitacao;
	}

	/**
	 * Get: manterOperacoesAmbienteAtualSolicitacao.
	 *
	 * @return manterOperacoesAmbienteAtualSolicitacao
	 */
	public String getManterOperacoesAmbienteAtualSolicitacao() {
		return manterOperacoesAmbienteAtualSolicitacao;
	}

	/**
	 * Set: manterOperacoesAmbienteAtualSolicitacao.
	 *
	 * @param manterOperacoesAmbienteAtualSolicitacao the manter operacoes ambiente atual solicitacao
	 */
	public void setManterOperacoesAmbienteAtualSolicitacao(String manterOperacoesAmbienteAtualSolicitacao) {
		this.manterOperacoesAmbienteAtualSolicitacao = manterOperacoesAmbienteAtualSolicitacao;
	}

	/**
	 * Get: tipoMudancaSolicitacao.
	 *
	 * @return tipoMudancaSolicitacao
	 */
	public String getTipoMudancaSolicitacao() {
		return tipoMudancaSolicitacao;
	}

	/**
	 * Set: tipoMudancaSolicitacao.
	 *
	 * @param tipoMudancaSolicitacao the tipo mudanca solicitacao
	 */
	public void setTipoMudancaSolicitacao(String tipoMudancaSolicitacao) {
		this.tipoMudancaSolicitacao = tipoMudancaSolicitacao;
	}

	/**
	 * Get: listaModalidade.
	 *
	 * @return listaModalidade
	 */
	public List<SelectItem> getListaModalidade() {
		return listaModalidade;
	}

	/**
	 * Set: listaModalidade.
	 *
	 * @param listaModalidade the lista modalidade
	 */
	public void setListaModalidade(List<SelectItem> listaModalidade) {
		this.listaModalidade = listaModalidade;
	}

	/**
	 * Get: modalidade.
	 *
	 * @return modalidade
	 */
	public Integer getModalidade() {
		return modalidade;
	}

	/**
	 * Set: modalidade.
	 *
	 * @param modalidade the modalidade
	 */
	public void setModalidade(Integer modalidade) {
		this.modalidade = modalidade;
	}

	/**
	 * Get: modalidadeDesc.
	 *
	 * @return modalidadeDesc
	 */
	public String getModalidadeDesc() {
		return modalidadeDesc;
	}

	/**
	 * Set: modalidadeDesc.
	 *
	 * @param modalidadeDesc the modalidade desc
	 */
	public void setModalidadeDesc(String modalidadeDesc) {
		this.modalidadeDesc = modalidadeDesc;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: listaModalidadeHash.
	 *
	 * @return listaModalidadeHash
	 */
	public Map<Integer, String> getListaModalidadeHash() {
		return listaModalidadeHash;
	}

	/**
	 * Set lista modalidade hash.
	 *
	 * @param listaModalidadeHash the lista modalidade hash
	 */
	public void setListaModalidadeHash(Map<Integer, String> listaModalidadeHash) {
		this.listaModalidadeHash = listaModalidadeHash;
	}

	/**
	 * Get: habilitaRadioAmbiente.
	 *
	 * @return habilitaRadioAmbiente
	 */
	public Boolean getHabilitaRadioAmbiente() {
		return habilitaRadioAmbiente;
	}

	/**
	 * Set: habilitaRadioAmbiente.
	 *
	 * @param habilitaRadioAmbiente the habilita radio ambiente
	 */
	public void setHabilitaRadioAmbiente(Boolean habilitaRadioAmbiente) {
		this.habilitaRadioAmbiente = habilitaRadioAmbiente;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/**
	 * Get: voltar.
	 *
	 * @return voltar
	 */
	public static String getVoltar() {
		return VOLTAR;
	}

	/**
	 * Is habilita mudanca solicitacao.
	 *
	 * @return true, if is habilita mudanca solicitacao
	 */
	public boolean isHabilitaMudancaSolicitacao() {
		return habilitaMudancaSolicitacao;
	}

	/**
	 * Set: habilitaMudancaSolicitacao.
	 *
	 * @param habilitaMudancaSolicitacao the habilita mudanca solicitacao
	 */
	public void setHabilitaMudancaSolicitacao(boolean habilitaMudancaSolicitacao) {
		this.habilitaMudancaSolicitacao = habilitaMudancaSolicitacao;
	}

	/**
	 * Get: manterContratoService.
	 *
	 * @return manterContratoService
	 */
	public IManterContratoService getManterContratoService() {
		return manterContratoService;
	}

	/**
	 * Set: manterContratoService.
	 *
	 * @param manterContratoService the manter contrato service
	 */
	public void setManterContratoService(
			IManterContratoService manterContratoService) {
		this.manterContratoService = manterContratoService;
	}

	/**
	 * Get: listaServico.
	 *
	 * @return listaServico
	 */
	public List<ListarModalidadeTipoServicoSaidaDTO> getListaServico() {
		return listaServico;
	}

	/**
	 * Set: listaServico.
	 *
	 * @param listaServico the lista servico
	 */
	public void setListaServico(
			List<ListarModalidadeTipoServicoSaidaDTO> listaServico) {
		this.listaServico = listaServico;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: nomeRazaoParticipante.
	 *
	 * @return nomeRazaoParticipante
	 */
	public String getNomeRazaoParticipante() {
		return nomeRazaoParticipante;
	}

	/**
	 * Set: nomeRazaoParticipante.
	 *
	 * @param nomeRazaoParticipante the nome razao participante
	 */
	public void setNomeRazaoParticipante(String nomeRazaoParticipante) {
		this.nomeRazaoParticipante = nomeRazaoParticipante;
	}
	
}

