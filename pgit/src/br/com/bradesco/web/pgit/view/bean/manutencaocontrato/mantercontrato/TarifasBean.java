/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarListaValoresDiscretosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarListaValoresDiscretosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarIndiceEconomicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarIndiceEconomicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarModalidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOperacaoServicoPagtoIntegradoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarOperacaoServicoPagtoIntegradoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.PeriodicidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.PeriodicidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.impl.bean.ListarModalidadesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarCondCobTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarCondCobTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarCondReajusteTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.AlterarCondReajusteTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManServicosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManTarifasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarConManTarifasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarCondCobTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarCondCobTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarCondReajusteTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarCondReajusteTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarTarifaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarTarifaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManTarifaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarConManTarifaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarModalidadeTipoServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarTarifasContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ListarTarifasContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ValidarQtdDiasCobrancaApsApuracaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.vincmsglanctopecontratada.bean.ListarVincMsgLancOperSaidaDTO;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.utils.SiteUtil;
import br.com.bradesco.web.pgit.view.converters.FormatarData;


// TODO: Auto-generated Javadoc
/**
 * Nome: TarifasBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class TarifasBean {

	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;

	/** Atributo cpfCnpjRepresentante. */
	private String cpfCnpjRepresentante;

	/** Atributo nomeRazaoRepresentante. */
	private String nomeRazaoRepresentante;

	/** Atributo grupEconRepresentante. */
	private String grupEconRepresentante;

	/** Atributo ativEconRepresentante. */
	private String ativEconRepresentante;

	/** Atributo segRepresentante. */
	private String segRepresentante;

	/** Atributo subSegRepresentante. */
	private String subSegRepresentante;

	/** Atributo empresaContrato. */
	private String empresaContrato;

	/** Atributo tipoContrato. */
	private String tipoContrato;

	/** Atributo numeroContrato. */
	private String numeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo situacaoContrato. */
	private String situacaoContrato;

	/** Atributo motivoContrato. */
	private String motivoContrato;

	/** Atributo participacaoContrato. */
	private String participacaoContrato;

	/** Atributo cdRelacionamentoProduto. */
	private int cdRelacionamentoProduto;

	/** Atributo codPessoaJuridica. */
	private Long codPessoaJuridica;

	/** Atributo cdTipoContrato. */
	private Integer cdTipoContrato;

	/** Atributo nrSequenciaContrato. */
	private Long nrSequenciaContrato;

	/** Atributo listaGridPesquisa. */
	private List<ListarTarifasContratoSaidaDTO> listaGridPesquisa;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo codTipoServico. */
	private int codTipoServico;

	/** Atributo codModalidade. */
	private int codModalidade;

	/** Atributo produtoServicoDesc. */
	private String produtoServicoDesc;

	/** Atributo cdPeriodicidade. */
	private int cdPeriodicidade;

	/** Atributo periodicidade. */
	private String periodicidade;

	/** Atributo diaFechamento. */
	private String diaFechamento;

	/** Atributo diaFechamento. */
	private String dsDiaFechamento;

	/** Atributo quantDia. */
	private String quantDia;

	/** Atributo quantDia. */
	private String dsQuantDia;

	/** Atributo periodicidadeHist. */
	private String periodicidadeHist;

	/** Atributo diaFechamentoHist. */
	private String diaFechamentoHist;

	/** Atributo quantDiaHist. */
	private String quantDiaHist;

	/** Atributo dataHoraInclusaoHist. */
	private String dataHoraInclusaoHist;

	/** Atributo cdUsuarioInclusaoHist. */
	private String cdUsuarioInclusaoHist;

	/** Atributo cdCanalInclusaoFormatadoHist. */
	private String cdCanalInclusaoFormatadoHist;

	/** Atributo dsComplementoInclusaoHist. */
	private String dsComplementoInclusaoHist;

	/** Atributo dataHoraAlteracaoHist. */
	private String dataHoraAlteracaoHist;

	/** Atributo cdUsuarioAlteracaoHist. */
	private String cdUsuarioAlteracaoHist;

	/** Atributo cdCanalAlteracaoFormatadoHist. */
	private String cdCanalAlteracaoFormatadoHist;

	/** Atributo dsComplementoAlteracaoHist. */
	private String dsComplementoAlteracaoHist;

	/** Atributo dsTipoReajusteTarifaHist. */
	private String dsTipoReajusteTarifaHist;

	/** Atributo qtdeMesReajusteTarifaHist. */
	private String qtdeMesReajusteTarifaHist;

	/** Atributo cdIndiceEconomicoReajusteHist. */
	private String cdIndiceEconomicoReajusteHist;

	/** Atributo dsIndiceEconomicoReajusteHist. */
	private String dsIndiceEconomicoReajusteHist;

	/** Atributo percentualIndiceReajusteTarifaHist. */
	private String percentualIndiceReajusteTarifaHist;

	/** Atributo percentualFlexCatReajsuteTarifaHist. */
	private String percentualFlexCatReajsuteTarifaHist;

	/** Atributo cdPeriodicidadeFiltro. */
	private Integer cdPeriodicidadeFiltro;

	/** Atributo diaFechamentoFiltro. */
	private String diaFechamentoFiltro;

	/** Atributo quantDiaFiltro. */
	private String quantDiaFiltro;

	/** Atributo produtoServicoCd. */
	private int produtoServicoCd;

	/** Atributo listaPeriodicidade. */
	private List<SelectItem> listaPeriodicidade = new ArrayList<SelectItem>();

	/** Atributo listaPeriodicidadeHash. */
	private Map<Integer, String> listaPeriodicidadeHash = new HashMap<Integer, String>();

	/** Atributo PERIODICIDADE_MENSAL. */
	private static final int PERIODICIDADE_MENSAL = 4;

	/** Atributo listaGridOculta. */
	private List<ListarModalidadeTipoServicoSaidaDTO> listaGridOculta = new ArrayList<ListarModalidadeTipoServicoSaidaDTO>();

	/** Atributo dsPeriodicidade. */
	private String dsPeriodicidade;

	/** Atributo cdTipoReajuste. */
	private int cdTipoReajuste;

	/** Atributo tipoReajuste. */
	private Integer tipoReajuste;

	/** Atributo quantMes. */
	private String quantMes;

	/** Atributo cdIndiceEconomico. */
	private int cdIndiceEconomico;

	/** Atributo indiceEconomico. */
	private String indiceEconomico;

	/** Atributo cdPercentualIndice. */
	private BigDecimal cdPercentualIndice;

	/** Atributo percentualIndiceDesc. */
	private String percentualIndiceDesc; 

	/** Atributo percentualIndice. */
	private String percentualIndice;

	/** Atributo cdPercentualFlexibilizacao. */
	private BigDecimal cdPercentualFlexibilizacao;

	/** Atributo percentualFlexibilizacao. */
	private String percentualFlexibilizacao;

	/** Atributo percentualFlexibilizacaoDesc. */
	private String percentualFlexibilizacaoDesc; 

	/** Atributo condReajusteTarifa. */
	private ConsultarCondReajusteTarifaSaidaDTO condReajusteTarifa = new ConsultarCondReajusteTarifaSaidaDTO();

	/** Atributo cdTipoReajusteFiltro. */
	private Integer cdTipoReajusteFiltro;

	/** Atributo quantMesFiltro. */
	private String quantMesFiltro;

	/** Atributo cdIndiceEconomicoFiltro. */
	private Integer cdIndiceEconomicoFiltro;

	/** Atributo listaIndiceEconomico. */
	private List<SelectItem> listaIndiceEconomico = new ArrayList<SelectItem>();

	/** Atributo listaIndiceEconomicoHash. */
	private Map<Integer, String> listaIndiceEconomicoHash = new HashMap<Integer, String>();

	/** Atributo percentualIndiceFiltro. */
	private BigDecimal percentualIndiceFiltro;

	/** Atributo percentualFlexibilizacaoFiltro. */
	private BigDecimal percentualFlexibilizacaoFiltro;

	/** Atributo listaTipoReajuste. */
	private List<SelectItem> listaTipoReajuste = new ArrayList<SelectItem>();

	/** Atributo dsIndiceEconomico. */
	private String dsIndiceEconomico;

	/** Atributo dsTipoReajuste. */
	private String dsTipoReajuste;

	/** Atributo modalidade. */
	private String modalidade;

	/** Atributo dsTipoServicoModalidade. */
	private String dsTipoServicoModalidade;

	/** Atributo dsModModalidade. */
	private String dsModModalidade;

	/** Atributo listaGridTarifasModalidade. */
	private List<ConsultarTarifaContratoSaidaDTO> listaGridTarifasModalidade;

	/** Atributo itemSelecionadoTarifas. */
	private Integer itemSelecionadoTarifas;

	/** Atributo listaControleTarifas. */
	private List<SelectItem> listaControleTarifas = new ArrayList<SelectItem>();

	/** Atributo dsOperacao. */
	private String dsOperacao;

	/** Atributo dsValorContratado. */
	private String dsValorContratado;

	/** Atributo dsValorReferenciaMin. */
	private String dsValorReferenciaMin;

	/** Atributo dsValorReferenciaMax. */
	private String dsValorReferenciaMax;

	/** Atributo dsPeriodoVigencia. */
	private String dsPeriodoVigencia;

	/** Atributo dtInicioVig. */
	private String dtInicioVig;

	/** Atributo dtFimVig. */
	private String dtFimVig;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo tpManutencao. */
	private String tpManutencao;

	/** Atributo valorContratadoFiltro. */
	private String valorContratadoFiltro;

	/** Atributo periodoValidade. */
	private String periodoValidade;

	/** Atributo cdTipoServicoFiltro. */
	private Integer cdTipoServicoFiltro;

	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();

	/** Atributo listaTipoServicoHash. */
	private Map<Integer, String> listaTipoServicoHash = new HashMap<Integer, String>();

	/** Atributo cdModalidadeFiltro. */
	private Integer cdModalidadeFiltro;

	/** Atributo listaModalidade. */
	private List<SelectItem> listaModalidade = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeHash. */
	private Map<Integer, String> listaModalidadeHash = new HashMap<Integer, String>();

	/** Atributo cdOperacaoFiltro. */
	private Integer cdOperacaoFiltro;

	/** Atributo listaOperacao. */
	private List<SelectItem> listaOperacao = new ArrayList<SelectItem>();

	/** Atributo listaOperacaoHash. */
	private Map<Integer, String> listaOperacaoHash = new HashMap<Integer, String>();

	/** Atributo filtroDataDe. */
	private Date filtroDataDe;

	/** Atributo filtroDataAte. */
	private Date filtroDataAte;

	/** Atributo listaGridPesquisa3. */
	private List<ListarConManTarifaSaidaDTO> listaGridPesquisa3;

	/** Atributo listaGridPesquisaHistCobr. */
	private List<ListarConManServicoSaidaDTO> listaGridPesquisaHistCobr;

	/** Atributo listaGridPesquisaHistReaj. */
	private List<ListarConManServicoSaidaDTO> listaGridPesquisaHistReaj;

	/** Atributo itemSelecionadoListaHistReaj. */
	private Integer itemSelecionadoListaHistReaj;

	/** Atributo listaControleRadioHistReaj. */
	private List<SelectItem> listaControleRadioHistReaj = new ArrayList<SelectItem>();

	/** Atributo itemSelecionadoLista3. */
	private Integer itemSelecionadoLista3;

	/** Atributo listaControleRadio3. */
	private List<SelectItem> listaControleRadio3 = new ArrayList<SelectItem>();

	/** Atributo itemSelecionadoListaHistCobr. */
	private Integer itemSelecionadoListaHistCobr;

	/** Atributo listaControleRadioHistCobr. */
	private List<SelectItem> listaControleRadioHistCobr = new ArrayList<SelectItem>();

	/** Atributo desabilitarCombo. */
	private boolean desabilitarCombo;

	/** Atributo desabilitarCombo2. */
	private boolean desabilitarCombo2;

	/** Atributo desabilitarCombo3. */
	private boolean desabilitarCombo3;

	/** Atributo btnConsultarHist. */
	private boolean btnConsultarHist;

	/** Atributo obrigatoriedadeHist. */
	private String obrigatoriedadeHist;

	/** Atributo btoAcionado. */
	private boolean btoAcionado;

	/** Atributo dsTipoServico. */
	private String dsTipoServico;

	/** Atributo dsModalidade. */
	private String dsModalidade;

	/** Atributo valorReferenciaMin. */
	private String valorReferenciaMin;

	/** Atributo valorReferenciaMax. */
	private String valorReferenciaMax;

	/** Atributo valorContratado. */
	private String valorContratado;

	/** Atributo periodoValidadeDe. */
	private String periodoValidadeDe;

	/** Atributo periodoValidadeAte. */
	private String periodoValidadeAte;

	/** Atributo listaGridPesquisa4. */
	private List<ListarVincMsgLancOperSaidaDTO> listaGridPesquisa4;

	/** Atributo itemSelecionadoLista4. */
	private Integer itemSelecionadoLista4;

	/** Atributo listaControleRadio4. */
	private List<SelectItem> listaControleRadio4 = new ArrayList<SelectItem>();

	/** Atributo cdIndicadorTipoReajusteBloqueio. */
	private boolean cdIndicadorTipoReajusteBloqueio;

	/** The chk qtd dia cobranca. */
	private boolean chkQtdDiaCobranca;

	/** The chk dia fechamento. */
	private boolean chkDiaFechamento;


	/**
	 * Iniciar tela.
	 */
	public void iniciarTela() {
		this.itemSelecionadoLista = null;
		this.listaGridPesquisa = null;
		this.cpfCnpjRepresentante = "";
		this.nomeRazaoRepresentante = "";
		this.grupEconRepresentante = "";
		this.ativEconRepresentante = "";
		this.segRepresentante = "";
		this.subSegRepresentante = "";
		this.empresaContrato = "";
		this.tipoContrato = "";
		this.numeroContrato = "";
		this.situacaoContrato = "";
		this.motivoContrato = "";
		this.participacaoContrato = "";
	}

	/**
	 * Carrega lista consultar.
	 */
	public void carregaListaConsultar() {
		listaGridPesquisa = new ArrayList<ListarTarifasContratoSaidaDTO>();

		ListarTarifasContratoEntradaDTO entradaDTO = new ListarTarifasContratoEntradaDTO();

		entradaDTO.setCdPessoaJuridica(getCodPessoaJuridica());
		entradaDTO.setCdTipoContrato(getCdTipoContrato());
		entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
		entradaDTO.setCdTipoServico(0);
		entradaDTO.setCdModalidade(0);
		entradaDTO.setCdRelacionamentoProduto(0);

		setListaGridPesquisa(getManterContratoImpl().listarTarifasContrato(entradaDTO));

		this.listaControleRadio = new ArrayList<SelectItem>();

		for (int i = 0; i < getListaGridPesquisa().size(); i++) {
			listaControleRadio.add(new SelectItem(i, " "));
		}

		setItemSelecionadoLista(null);

	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoLista(null);
		
		if("0".equals(diaFechamentoFiltro)){
		    setDiaFechamentoFiltro("");
		}

		return "VOLTAR";
	}

	/**
	 * Voltar alteracao condicoes.
	 *
	 * @return the string
	 */
	public String voltarAlteracaoCondicoes() {
		return "VOLTAR";
	}

	/**
	 * Condicoes cobranca.
	 *
	 * @return the string
	 */
	public String condicoesCobranca() {
		try {
			preencheDadosCondicoesCobranca();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setItemSelecionadoLista(null);
			return "";
		}
		return "CONDICOES_COBRANCA";
	}

	/**
	 * Condicoes reajuste.
	 *
	 * @return the string
	 */
	public String condicoesReajuste() {
		try {
			carregaListaIndiceEconomico();
			preencheDadosCondicoesReajuste();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setItemSelecionadoLista(null);
			return "";
		}

		return "CONDICOES_REAJUSTE";
	}

	/**
	 * Tarifa modalidade.
	 *
	 * @return the string
	 */
	public String tarifaModalidade() {
		carregaGridTarifasModalidade();
		return "TARIFA";
	}

	/**
	 * Alterar condicoes cobranca.
	 *
	 * @return the string
	 */
	public String alterarCondicoesCobranca() {
		carregaListaPeriodicidade();
		setCdPeriodicidadeFiltro(getCdPeriodicidade());
		if(getCdPeriodicidade() != 4){
			setDiaFechamentoFiltro("");
		}else{
			setDiaFechamentoFiltro(getDiaFechamento());
		}

		if(getQuantDia() != null && getDsQuantDia().equals("Cobran�a no dia da apura��o")){
			setQuantDiaFiltro("");
		} else {
			setQuantDiaFiltro(getQuantDia());
		}
		selecionaChk();
		return "ALTERAR_CONDICOES_COBRANCA";
	}


	private void selecionaChk(){
		setChkDiaFechamento(false);
		setChkQtdDiaCobranca(false);

		if(getCdPeriodicidade() == 4 && (getDiaFechamento() == null || "".equals(getDiaFechamento()))){
			setChkDiaFechamento(true);
			limpaCampoDiaFechamento();
		}

		if(getQuantDia() == null || getDsQuantDia().equals("Cobran�a no dia da apura��o") || getDsQuantDia().equals("")){
			setChkQtdDiaCobranca(true);
			limpaCampoQtdDia();
		}
	}

	/**
	 * Mudar periodicidade.
	 */
	public void mudarPeriodicidade() {
		setChkDiaFechamento(false);
		setChkQtdDiaCobranca(false);
		if (!isPeriodicidadeMensal()) {
			this.diaFechamentoFiltro = "";
			this.quantDiaFiltro = "";
		}

	}

	/**
	 * Is periodicidade mensal.
	 *
	 * @return true, if is periodicidade mensal
	 */
	public boolean isPeriodicidadeMensal() {
		return (cdPeriodicidadeFiltro != null && cdPeriodicidadeFiltro == PERIODICIDADE_MENSAL);
	}

	/**
	 * Historico condicoes cobranca.
	 *
	 * @return the string
	 */
	public String historicoCondicoesCobranca() {
		limparDadosHistorico();

		return "HISTORICO_CONDICOES_COBRANCA";
	}

	/**
	 * Historico condicoes reajuste.
	 *
	 * @return the string
	 */
	public String historicoCondicoesReajuste() {
		limparDadosHistorico();

		return "HISTORICO_CONDICOES_REAJUSTE";
	}

	// altCondicoesCobranca
	/**
	 * Avancar confirmar condicoes cobranca.
	 *
	 * @return the string
	 */
	public String avancarConfirmarCondicoesCobranca() {
		try {
			ValidarQtdDiasCobrancaApsApuracaoEntradaDTO entradaDTO = new ValidarQtdDiasCobrancaApsApuracaoEntradaDTO();

			entradaDTO.setCdPeriodicidadeCobrancaTarifa(getCdPeriodicidadeFiltro());
			
			if(getQuantDiaFiltro() != null &&  !"".equals(getQuantDiaFiltro())) {
				entradaDTO.setQuantidadeDiasCobrancaTarifa(Integer.parseInt(getQuantDiaFiltro()));
			} else {
				entradaDTO.setQuantidadeDiasCobrancaTarifa(0);
			}

			getManterContratoImpl().validarQtdDiasCobrancaApsApuracao(entradaDTO);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}

		if (this.getObrigatoriedade() != null && getObrigatoriedade().equals("T")) {
			setDsPeriodicidade(listaPeriodicidadeHash.get(getCdPeriodicidadeFiltro()));
			if(getCdPeriodicidadeFiltro() != null && getCdPeriodicidadeFiltro() != 4){
				setDiaFechamentoFiltro("0");
			}

			return "AVANCAR_CONFIRMAR";
		} else {
			return "";
		}
	}

	// altCondicoesCobranca2
	/**
	 * Confirmar alteracao condicoes cobranca.
	 *
	 * @return the string
	 */
	public String confirmarAlteracaoCondicoesCobranca() {
		try {
			AlterarCondCobTarifaEntradaDTO entradaDTO = new AlterarCondCobTarifaEntradaDTO();

			entradaDTO.setCdPessoaContrato(getCodPessoaJuridica());
			entradaDTO.setCdTipoContrato(getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
			entradaDTO.setCdTipoServico(getCodTipoServico());
			entradaDTO.setCdModalidade(getCodModalidade());
			entradaDTO.setCdPeriodicidadeCobrancaTarifa(getCdPeriodicidadeFiltro());

			if(chkQtdDiaCobranca){
				entradaDTO.setQuantidadeDiasCobrancaTarifa(0);
			}else{
				entradaDTO.setQuantidadeDiasCobrancaTarifa(Integer.parseInt(getQuantDiaFiltro()));
			}

			int diaFechamentoNum = 0;
			if (!chkDiaFechamento && !SiteUtil.isEmptyOrNull(getDiaFechamentoFiltro())) {
				diaFechamentoNum = Integer.parseInt(getDiaFechamentoFiltro());
			}else{
				diaFechamentoNum = 99;
			}
			entradaDTO.setNrFechamentoApuracaoTarifa(diaFechamentoNum);

			AlterarCondCobTarifaSaidaDTO saidaDTO = getManterContratoImpl().alterarCondCobTarifa(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), 
					"#{manterContratoTarifasBean.limparFiltros}",
					BradescoViewExceptionActionType.ACTION, false);

			carregaListaConsultar();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}

	public String limparFiltros(){
		setQuantDiaFiltro("");
		setQuantDia("");
		setDsQuantDia("");
		setDiaFechamentoFiltro("");
		setDiaFechamento("");
		setDsDiaFechamento("");
		return "conTarifasManterContrato";
	}

	// detCondicoesReajuste
	/**
	 * Alterar condicoes reajuste.
	 *
	 * @return the string
	 */
	public String alterarCondicoesReajuste() {
		carregaListaIndiceEconomico();
		carregaComboTipoReajuste();
		preencheDadosCondicoesReajuste();
		return "ALTERAR_CONDICOES_REAJUSTE";
	}

	/**
	 * Carrega combo tipo reajuste.
	 */
	private void carregaComboTipoReajuste() {
		ConsultarListaValoresDiscretosEntradaDTO comboEntrada = new ConsultarListaValoresDiscretosEntradaDTO();
		comboEntrada.setNumeroCombo(47);
		List<ConsultarListaValoresDiscretosSaidaDTO> listaSaida = getComboService().consultarListaValoresDiscretos(comboEntrada);
		List<SelectItem> listaSelectItem = new ArrayList<SelectItem>();
		for (ConsultarListaValoresDiscretosSaidaDTO comboSaida : listaSaida) {
			listaSelectItem.add(new SelectItem(comboSaida.getCdCombo().intValue(), comboSaida.getDsCombo()));
		}
		setListaTipoReajuste(listaSelectItem);
	}

	/**
	 * Alterar selecao combo tipo reajuste.
	 */
	public void alterarSelecaoComboTipoReajuste() {
		if (isComboTipoReajusteSemReajuste()) {
			quantMesFiltro = "";
			cdIndiceEconomicoFiltro = 0;
			percentualIndiceFiltro = null;
			percentualFlexibilizacaoFiltro = null;
		} else if (isComboTipoReajustePorNegociacao()) {
			cdIndiceEconomicoFiltro = 0;
			percentualIndiceFiltro = null;
			percentualFlexibilizacaoFiltro = null;
		} else if (isComboTipoReajusteIndiceEconomico()) {
			percentualFlexibilizacaoFiltro = null;
		} else if (isComboTipoReajusteCatalogo()) {
			cdIndiceEconomicoFiltro = 0;
			percentualIndiceFiltro = null;
		}
	}

	/**
	 * Is combo tipo reajuste sem reajuste.
	 *
	 * @return true, if is combo tipo reajuste sem reajuste
	 */
	public boolean isComboTipoReajusteSemReajuste() {
		return cdTipoReajusteFiltro != null && cdTipoReajusteFiltro == 1;
	}

	/**
	 * Is combo tipo reajuste por negociacao.
	 *
	 * @return true, if is combo tipo reajuste por negociacao
	 */
	public boolean isComboTipoReajustePorNegociacao() {
		return cdTipoReajusteFiltro != null && cdTipoReajusteFiltro == 2;
	}

	/**
	 * Is combo tipo reajuste indice economico.
	 *
	 * @return true, if is combo tipo reajuste indice economico
	 */
	public boolean isComboTipoReajusteIndiceEconomico() {
		return cdTipoReajusteFiltro != null && cdTipoReajusteFiltro == 3;
	}

	/**
	 * Is combo tipo reajuste catalogo.
	 *
	 * @return true, if is combo tipo reajuste catalogo
	 */
	public boolean isComboTipoReajusteCatalogo() {
		return cdTipoReajusteFiltro != null && cdTipoReajusteFiltro == 4;
	}

	/**
	 * Is combo tipo reajuste sem selecao.
	 *
	 * @return true, if is combo tipo reajuste sem selecao
	 */
	private boolean isComboTipoReajusteSemSelecao() {
		return cdTipoReajusteFiltro == null || cdTipoReajusteFiltro == 0;
	}

	/**
	 * Is quantidade mes desabilitado.
	 *
	 * @return true, if is quantidade mes desabilitado
	 */
	public boolean isQuantidadeMesDesabilitado() {
		return isComboTipoReajusteSemSelecao() || isComboTipoReajusteSemReajuste();
	}

	/**
	 * Is indice economico desabilitado.
	 *
	 * @return true, if is indice economico desabilitado
	 */
	public boolean isIndiceEconomicoDesabilitado() {
		return isComboTipoReajusteSemSelecao() || !isComboTipoReajusteIndiceEconomico();
	}

	/**
	 * Is indice flex desabilitado.
	 *
	 * @return true, if is indice flex desabilitado
	 */
	public boolean isIndiceFlexDesabilitado() {
		return isComboTipoReajusteSemSelecao() || !isComboTipoReajusteCatalogo();
	}

	// altCondicoesReajuste
	/**
	 * Avancar confirmar condicoes reajuste.
	 *
	 * @return the string
	 */
	public String avancarConfirmarCondicoesReajuste() {
		if (this.getObrigatoriedade() != null && getObrigatoriedade().equals("T")) {
			setDsTipoReajuste(obterTextoComboSelecionado(listaTipoReajuste, cdTipoReajusteFiltro));
			setDsIndiceEconomico(listaIndiceEconomicoHash.get(getCdIndiceEconomicoFiltro()));
			setPercentualIndiceDesc(NumberUtils.format(getPercentualIndiceFiltro()));
			setPercentualFlexibilizacaoDesc(NumberUtils.format(getPercentualFlexibilizacaoFiltro()));
			return "AVANCAR_CONFIRMAR";
		} else {
			return "";
		}
	}

	/**
	 * Obter texto combo selecionado.
	 *
	 * @param listaCombo the lista combo
	 * @param valorSelecionado the valor selecionado
	 * @return the string
	 */
	private String obterTextoComboSelecionado(List<SelectItem> listaCombo, Object valorSelecionado) {
		for (SelectItem item : listaCombo) {
			if (item.getValue().equals(valorSelecionado)) {
				return item.getLabel();
			}
		}
		return "";
	}

	// altCondicoesReajuste2
	/**
	 * Confirmar alteracao condicoes reajuste.
	 *
	 * @return the string
	 */
	public String confirmarAlteracaoCondicoesReajuste() {
		try {
			AlterarCondReajusteTarifaEntradaDTO entradaDTO = new AlterarCondReajusteTarifaEntradaDTO();

			entradaDTO.setCdPessoaJuridicaNegocio(getCodPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(getNrSequenciaContrato());
			entradaDTO.setCdServico(getCodTipoServico());
			entradaDTO.setCdModalidade(getCodModalidade());
			entradaDTO.setCdRelacionamentoProduto(getCdRelacionamentoProduto());
			entradaDTO.setCdTipoReajusteTarifa(getCdTipoReajusteFiltro());
			int qMes = 0;
			if (!SiteUtil.isEmptyOrNull(getQuantMesFiltro())) {
				qMes = Integer.parseInt(getQuantMesFiltro());
			}
			entradaDTO.setQtMesReajusteTarifa(qMes);
			entradaDTO.setCdIndicadorEconomicoReajuste(getCdIndiceEconomicoFiltro());
			entradaDTO.setCdPercentualIndice(getPercentualIndiceFiltro());
			entradaDTO.setCdPercentualFlexibilizacao(getPercentualFlexibilizacaoFiltro());

			AlterarCondReajusteTarifaSaidaDTO saidaDTO = getManterContratoImpl().alterarCondReajusteTarifa(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "conTarifasManterContrato",
					BradescoViewExceptionActionType.ACTION, false);

			carregaListaConsultar();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}

	// histModalidade
	/**
	 * Limpar tarifas.
	 */
	public void limparTarifas() {
		this.itemSelecionadoLista4 = null;
	}

	// hisModalidade
	/**
	 * Detalhar historico modalidade.
	 *
	 * @return the string
	 */
	public String detalharHistoricoModalidade() {
		return "DETALHAR";
	}

	// hisModalidade
	/**
	 * Renegociar.
	 *
	 * @return the string
	 */
	public String renegociar() {
		this.valorContratadoFiltro = "";
		return "RENEGOCIAR";
	}

	// regTarifasOperacoes
	/**
	 * Avancar confirmar renegociar.
	 *
	 * @return the string
	 */
	public String avancarConfirmarRenegociar() {
		return "AVANCAR_RENEGOCIAR";
	}

	/**
	 * Confirmar renegociacao.
	 *
	 * @return the string
	 */
	public String confirmarRenegociacao() {
		return "";
	}

	/**
	 * Limpar dados historico.
	 */
	public void limparDadosHistorico() {
		this.cdTipoServicoFiltro = 0;

		setListaModalidade(new ArrayList<SelectItem>());
		this.setCdModalidadeFiltro(0);
		setListaOperacao(new ArrayList<SelectItem>());
		this.setCdOperacaoFiltro(0);
		setDesabilitarCombo(false);
		setDesabilitarCombo2(false);
		setDesabilitarCombo3(true);
		setBtnConsultarHist(true);

		setFiltroDataDe(new Date());
		setFiltroDataAte(new Date());

		setListaGridPesquisa3(null);
		setItemSelecionadoLista3(null);
		setListaControleRadio3(null);

		setListaGridPesquisaHistCobr(null);
		setItemSelecionadoListaHistCobr(null);
		setListaControleRadioHistCobr(null);

		setListaGridPesquisaHistReaj(null);
		setItemSelecionadoListaHistReaj(null);
		setListaControleRadioHistReaj(null);

		setBtoAcionado(false);
	}

	/**
	 * Historico modalidade.
	 *
	 * @return the string
	 */
	public String historicoModalidade() {
		limparDadosHistorico();
		carregaComboTipoServico();
		setObrigatoriedadeHist("");

		if (itemSelecionadoTarifas != null) {
			ConsultarTarifaContratoSaidaDTO dto = listaGridTarifasModalidade.get(getItemSelecionadoTarifas());

			setCdTipoServicoFiltro(dto.getCdProduto());
			setCdModalidadeFiltro(dto.getCdProdutoServicoRelacionado());

			carregaComboModalidade();
			setCdModalidadeFiltro(dto.getCdProdutoServicoRelacionado());

			carregaComboOperacao(getCdTipoServicoFiltro());
			setCdOperacaoFiltro(dto.getCdOperacao());

			setDesabilitarCombo(true);
		} else {
			carregaComboTipoServico();

			setCdTipoServicoFiltro(0);
			setCdModalidadeFiltro(0);

			setCdOperacaoFiltro(0);
		}

		return "HISTORICO_MODALIDADE";
	}

	/**
	 * Consultar historico.
	 */
	public void consultarHistorico() {
		listaGridPesquisa3 = new ArrayList<ListarConManTarifaSaidaDTO>();
		try {
			ListarConManTarifaEntradaDTO entrada = new ListarConManTarifaEntradaDTO();

			entrada.setCdPessoaJuridicaNegocio(getCodPessoaJuridica() != null ? getCodPessoaJuridica() : 0L);
			entrada.setCdTipoContratoNegocio(getCdTipoContrato() != null ? getCdTipoContrato() : 0);
			entrada.setNrSequenciaContratoNegocio(getNrSequenciaContrato() != null ? getNrSequenciaContrato() : 0L);

			entrada.setCdOperacaoProdutoServico(getCdOperacaoFiltro() != null ? getCdOperacaoFiltro() : 0);
			entrada.setCdProdutoOperacaoRelacionado(getCdModalidadeFiltro() != null ? getCdModalidadeFiltro() : 0);
			entrada.setCdProdutoServicoOperacao(getCdTipoServicoFiltro() != null ? getCdTipoServicoFiltro() : 0);
			entrada.setDtInicio(getFiltroDataDe() != null ? FormatarData.formataDiaMesAno(getFiltroDataDe()) : "");
			entrada.setDtFim(getFiltroDataAte() != null ? FormatarData.formataDiaMesAno(getFiltroDataAte()) : "");

			setListaGridPesquisa3(getManterContratoImpl().listarConManTarifa(entrada));

			listaControleRadio3 = new ArrayList<SelectItem>();
			for (int i = 0; i < listaGridPesquisa3.size(); i++) {
				listaControleRadio3.add(new SelectItem(i, ""));
			}
			setItemSelecionadoLista3(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridPesquisa3(null);
			setItemSelecionadoLista3(null);
		}
	}

	/**
	 * Consultar historico cond cobr.
	 */
	public void consultarHistoricoCondCobr() {
		listaGridPesquisaHistCobr = new ArrayList<ListarConManServicoSaidaDTO>();
		try {
			ListarConManServicoEntradaDTO entrada = new ListarConManServicoEntradaDTO();

			entrada.setCdPessoaJuridicaNegocio(getCodPessoaJuridica());
			entrada.setCdProdutoOperacaoRelacionado(listaGridPesquisa.get(itemSelecionadoLista).getCdModalidade());
			entrada.setCdProdutoServicoOperacao(listaGridPesquisa.get(itemSelecionadoLista).getCdServico());
			entrada.setCdTipoContratoNegocio(getCdTipoContrato());
			entrada.setCdTipoServico(2); // Passar sempre 2 conforme informado
			// pela Beatriz. 07/08/2012
			entrada.setDtInicio(FormatarData.formataDiaMesAno(getFiltroDataDe()));
			entrada.setDtFim(FormatarData.formataDiaMesAno(getFiltroDataAte()));
			entrada.setNrSequenciaContratoNegocio(getNrSequenciaContrato());

			setListaGridPesquisaHistCobr(getManterContratoImpl().listarHistCondCobrReajuste(entrada));

			listaControleRadioHistCobr = new ArrayList<SelectItem>();
			for (int i = 0; i < listaGridPesquisaHistCobr.size(); i++) {
				listaControleRadioHistCobr.add(new SelectItem(i, ""));
			}

			setItemSelecionadoListaHistCobr(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridPesquisaHistCobr(null);
			setItemSelecionadoListaHistCobr(null);
		}
	}

	/**
	 * Paginar lista historico cond cobr.
	 *
	 * @param evt the evt
	 */
	public void paginarListaHistoricoCondCobr(ActionEvent evt) {
		listaGridPesquisaHistCobr = new ArrayList<ListarConManServicoSaidaDTO>();
		try {
			ListarConManServicoEntradaDTO entrada = new ListarConManServicoEntradaDTO();

			entrada.setCdPessoaJuridicaNegocio(getCodPessoaJuridica());
			entrada.setCdProdutoOperacaoRelacionado(listaGridPesquisa.get(itemSelecionadoLista).getCdModalidade());
			entrada.setCdProdutoServicoOperacao(listaGridPesquisa.get(itemSelecionadoLista).getCdServico());
			entrada.setCdTipoContratoNegocio(getCdTipoContrato());
			entrada.setCdTipoServico(2); // Passar sempre 2 conforme informado
			// pela Beatriz. 07/08/2012
			entrada.setDtInicio(FormatarData.formataDiaMesAno(getFiltroDataDe()));
			entrada.setDtFim(FormatarData.formataDiaMesAno(getFiltroDataAte()));
			entrada.setNrSequenciaContratoNegocio(getNrSequenciaContrato());

			setListaGridPesquisaHistCobr(getManterContratoImpl().listarHistCondCobrReajuste(entrada));

			listaControleRadioHistCobr = new ArrayList<SelectItem>();
			for (int i = 0; i < listaGridPesquisaHistCobr.size(); i++) {
				listaControleRadioHistCobr.add(new SelectItem(i, ""));
			}

			setItemSelecionadoListaHistCobr(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridPesquisaHistCobr(null);
			setItemSelecionadoListaHistCobr(null);
		}
	}

	/**
	 * Consultar historico cond reaj.
	 */
	public void consultarHistoricoCondReaj() {
		listaGridPesquisaHistReaj = new ArrayList<ListarConManServicoSaidaDTO>();
		try {
			ListarConManServicoEntradaDTO entrada = new ListarConManServicoEntradaDTO();

			entrada.setCdPessoaJuridicaNegocio(getCodPessoaJuridica());
			entrada.setCdProdutoOperacaoRelacionado(listaGridPesquisa.get(itemSelecionadoLista).getCdModalidade());
			entrada.setCdProdutoServicoOperacao(listaGridPesquisa.get(itemSelecionadoLista).getCdServico());
			entrada.setCdTipoContratoNegocio(getCdTipoContrato());
			entrada.setCdTipoServico(2); // Passar sempre 2 conforme informado
			// pela Beatriz. 07/08/2012
			entrada.setDtInicio(FormatarData.formataDiaMesAno(getFiltroDataDe()));
			entrada.setDtFim(FormatarData.formataDiaMesAno(getFiltroDataAte()));
			entrada.setNrSequenciaContratoNegocio(getNrSequenciaContrato());

			setListaGridPesquisaHistReaj(getManterContratoImpl().listarHistCondCobrReajuste(entrada));

			listaControleRadioHistReaj = new ArrayList<SelectItem>();
			for (int i = 0; i < listaGridPesquisaHistReaj.size(); i++) {
				listaControleRadioHistReaj.add(new SelectItem(i, ""));
			}

			setItemSelecionadoListaHistReaj(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridPesquisaHistReaj(null);
			setItemSelecionadoListaHistReaj(null);
		}
	}

	/**
	 * Paginar lista historico cond reaj.
	 *
	 * @param evt the evt
	 */
	public void paginarListaHistoricoCondReaj(ActionEvent evt) {
		listaGridPesquisaHistReaj = new ArrayList<ListarConManServicoSaidaDTO>();
		try {
			ListarConManServicoEntradaDTO entrada = new ListarConManServicoEntradaDTO();

			entrada.setCdPessoaJuridicaNegocio(getCodPessoaJuridica());
			entrada.setCdProdutoOperacaoRelacionado(listaGridPesquisa.get(itemSelecionadoLista).getCdModalidade());
			entrada.setCdProdutoServicoOperacao(listaGridPesquisa.get(itemSelecionadoLista).getCdServico());
			entrada.setCdTipoContratoNegocio(getCdTipoContrato());
			entrada.setCdTipoServico(2); // Passar sempre 2 conforme informado
			// pela Beatriz. 07/08/2012
			entrada.setDtInicio(FormatarData.formataDiaMesAno(getFiltroDataDe()));
			entrada.setDtFim(FormatarData.formataDiaMesAno(getFiltroDataAte()));
			entrada.setNrSequenciaContratoNegocio(getNrSequenciaContrato());

			setListaGridPesquisaHistReaj(getManterContratoImpl().listarHistCondCobrReajuste(entrada));

			listaControleRadioHistReaj = new ArrayList<SelectItem>();
			for (int i = 0; i < listaGridPesquisaHistReaj.size(); i++) {
				listaControleRadioHistReaj.add(new SelectItem(i, ""));
			}

			setItemSelecionadoListaHistReaj(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridPesquisaHistReaj(null);
			setItemSelecionadoListaHistReaj(null);
		}
	}

	/**
	 * Voltar hist cond cobr.
	 *
	 * @return the string
	 */
	public String voltarHistCondCobr() {
		return "detCondicoesCobranca";
	}

	/**
	 * Voltar det hist cond cobr.
	 *
	 * @return the string
	 */
	public String voltarDetHistCondCobr() {
		return "histCondicoesCobranca";
	}

	/**
	 * Voltar hist cond reaj.
	 *
	 * @return the string
	 */
	public String voltarHistCondReaj() {
		return "detCondicoesReajuste";
	}

	/**
	 * Voltar det hist cond reaj.
	 *
	 * @return the string
	 */
	public String voltarDetHistCondReaj() {
		return "histCondicoesReajuste";
	}


	// histTarifasManterContrato
	/**
	 * Detalhar historico.
	 *
	 * @return the string
	 */
	public String detalharHistorico() {
		try {
			preencheDadosHist();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "histTarifasManterContrato",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "DETALHAR_HISTORICO";
	}

	/**
	 * Preenche dados hist.
	 */
	public void preencheDadosHist() {

		ConsultarConManTarifasEntradaDTO entrada = new ConsultarConManTarifasEntradaDTO();
		ListarConManTarifaSaidaDTO dto = listaGridPesquisa3.get(getItemSelecionadoLista3());

		entrada.setCdOperacaoProdutoServico(dto.getCodOperacaoModalidadeServico());
		entrada.setCdProdutoServicoOperacao(dto.getCodTipoServico());
		entrada.setCdProdutoServicoRelacionado(dto.getCodModalidadeServico());
		entrada.setDtInicioVigenciaTarifa(FormatarData.formatarDataToPdc(dto.getDtInicioVigencia()));
		entrada.setHrInclusaoRegistroHistorico(dto.getHrInclusaoRegistroHistorico());

		entrada.setCdPessoaJuridicaContrato(getCodPessoaJuridica());
		entrada.setCdTipoContratoNegocio(getCdTipoContrato());
		entrada.setNrSequenciaContratoNegocio(getNrSequenciaContrato());

		ConsultarConManTarifasSaidaDTO saida = getManterContratoImpl().consultarConManTarifas(entrada);

		setDsTipoServico(saida.getDsProdutoServicoOperacao());
		setDsOperacao(saida.getDsOperacaoProdutoServico());
		setDsModalidade(saida.getDsProdutoServicoRelacionado());
		setValorReferenciaMin(NumberUtils.format(saida.getVlTarifaReferenciaMinima()));
		setValorReferenciaMax(NumberUtils.format(saida.getVlTarifaReferenciaMaxima()));

		setValorContratado(NumberUtils.format(saida.getVlTarifaContrato()));
		/*
		 * setPeriodoValidadeDe(FormatarData.formatarDataFromPdc(saida.getDtInicioVigenciaTarifa
		 * ()));setPeriodoValidadeAte(FormatarData.formatarDataFromPdc(saida.
		 * getDtFimVigenciaTarifa()));
		 */
		setDtInicioVig(FormatarData.formatarDataFromPdc(saida.getDtInicioVigenciaTarifa()));
		setDtFimVig(FormatarData.formatarDataFromPdc(saida.getDtFimVigenciaTarifa()));
		setTpManutencao(dto.getDsTipoManutencao());
		/* Trilha de Auditoria */
		setDataHoraInclusao(saida.getHrInclusaoRegistro());
		setUsuarioInclusao(saida.getCdUsuarioInclusao());
		setTipoCanalInclusao(saida.getCdCanalInclusao() == 0 ? "" : saida.getCdCanalInclusao() + " - " + saida.getDsCanalInclusao());
		setComplementoInclusao(saida.getNrOperacaoFluxoInclusao().equals("0") ? "" : saida.getNrOperacaoFluxoInclusao());
		setDataHoraManutencao(saida.getHrManutencaoResgistro());
		setUsuarioManutencao(saida.getCdUsuarioManutecao());
		setTipoCanalManutencao(saida.getCdCanalManutencao() == 0 ? "" : saida.getCdCanalManutencao() + " - " + saida.getDsCanalManutencao());
		setComplementoManutencao(saida.getNrOperacaoFluxoManuntencao().equals("0") ? "" : saida.getNrOperacaoFluxoManuntencao());
	}

	/**
	 * Detalhar historico cond cobr.
	 *
	 * @return the string
	 */
	public String detalharHistoricoCondCobr() {
		try {
			preencheDadosHistCondCobr();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "histCondicoesCobranca",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "detHistCondicoesCobranca";
	}

	/**
	 * Detalhar historico cond reaj.
	 *
	 * @return the string
	 */
	public String detalharHistoricoCondReaj() {
		try {
			carregaListaIndiceEconomico();
			preencheDadosHistCondReaj();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "histCondicoesReajuste",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "detHistCondicoesReajuste";
	}

	/**
	 * Preenche dados hist cond cobr.
	 */
	public void preencheDadosHistCondCobr() {
		ConsultarConManServicosEntradaDTO entrada = new ConsultarConManServicosEntradaDTO();
		ListarConManServicoSaidaDTO dto = listaGridPesquisaHistCobr.get(itemSelecionadoListaHistCobr);

		entrada.setCdParamentro(1);
		entrada.setCdParametroTela(PgitUtil.verificaIntegerNulo(dto.getCdParametroTela()));
		entrada.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(getCodPessoaJuridica()));
		entrada.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(listaGridPesquisa.get(itemSelecionadoLista).getCdModalidade()));
		entrada.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(dto.getCdProduto()));
		entrada.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(getCdTipoContrato()));
		entrada.setHrInclusaoRegistroHistorico(PgitUtil.verificaStringNula(dto.getHrInclusaoRegistroHistorico()));
		entrada.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(getNrSequenciaContrato()));

		ConsultarConManServicosSaidaDTO saida = manterContratoImpl.consultarConManServico(entrada);

		setPeriodicidadeHist(saida.getDsPeriodicidadeCobrancaTarifa());
		setDiaFechamentoHist(Long.toString(saida.getNrFechamentoApuracaoTarifa()));
		setQuantDiaHist(Integer.toString(saida.getQuantidadeDiaCobrancaTarifa()));
		setDataHoraInclusaoHist(saida.getHrManutencaoRegistroInclusao());
		setCdUsuarioInclusaoHist(saida.getCdUsuarioInclusao());
		setCdCanalInclusaoFormatadoHist(saida.getCdCanalInclusao() + " - " + saida.getDsCanalInclusao());
		setDsComplementoInclusaoHist(saida.getNrOperacaoFluxoInclusao());
		setDataHoraAlteracaoHist(saida.getHrManutencaoRegistroAlteracao());
		setCdUsuarioAlteracaoHist(saida.getCdUsuarioAlteracao());
		setCdCanalAlteracaoFormatadoHist(saida.getCdCanalAlteracao() + " - " + saida.getDsCanalAlteracao());
		setDsComplementoAlteracaoHist(saida.getNrOperacaoFluxoAlteracao());
	}

	/**
	 * Preenche dados hist cond reaj.
	 */
	public void preencheDadosHistCondReaj() {
		ConsultarConManServicosEntradaDTO entrada = new ConsultarConManServicosEntradaDTO();
		ListarConManServicoSaidaDTO dto = listaGridPesquisaHistReaj.get(itemSelecionadoListaHistReaj);

		entrada.setCdParamentro(1);
		entrada.setCdParametroTela(PgitUtil.verificaIntegerNulo(dto.getCdParametroTela()));
		entrada.setCdPessoaJuridicaContrato(PgitUtil.verificaLongNulo(getCodPessoaJuridica()));
		entrada.setCdProdutoOperacaoRelacionado(PgitUtil.verificaIntegerNulo(listaGridPesquisa.get(itemSelecionadoLista).getCdModalidade()));
		entrada.setCdProdutoServicoOperacao(PgitUtil.verificaIntegerNulo(dto.getCdProduto()));
		entrada.setCdTipoContratoNegocio(PgitUtil.verificaIntegerNulo(getCdTipoContrato()));
		entrada.setHrInclusaoRegistroHistorico(PgitUtil.verificaStringNula(dto.getHrInclusaoRegistroHistorico()));
		entrada.setNrSequenciaContratoNegocio(PgitUtil.verificaLongNulo(getNrSequenciaContrato()));

		ConsultarConManServicosSaidaDTO saida = manterContratoImpl.consultarConManServico(entrada);

		setDsTipoReajusteTarifaHist(saida.getDsTipoReajusteTarifa());
		setQtdeMesReajusteTarifaHist(Integer.toString(saida.getQuantidadeMesReajusteTarifa()));
		setCdIndiceEconomicoReajusteHist(Integer.toString(saida.getCdIndicadorEconomicoReajuste()));
		setDsIndiceEconomicoReajusteHist(saida.getDsIndicadorEconomicoReajuste());
		setPercentualIndiceReajusteTarifaHist(NumberUtils.format(saida.getPercentualIndiceReajusteTarifa()));
		setPercentualFlexCatReajsuteTarifaHist(NumberUtils.format(saida.getPeriodicidadeTarifaCatalogo()));
		setDataHoraInclusaoHist(saida.getHrManutencaoRegistroInclusao());
		setCdUsuarioInclusaoHist(saida.getCdUsuarioInclusao());
		setCdCanalInclusaoFormatadoHist(saida.getCdCanalInclusao() + " - " + saida.getDsCanalInclusao());
		setDsComplementoInclusaoHist(saida.getNrOperacaoFluxoInclusao());
		setDataHoraAlteracaoHist(saida.getHrManutencaoRegistroAlteracao());
		setCdUsuarioAlteracaoHist(saida.getCdUsuarioAlteracao());
		setCdCanalAlteracaoFormatadoHist(saida.getCdCanalAlteracao() + " - " + saida.getDsCanalAlteracao());
		setDsComplementoAlteracaoHist(saida.getNrOperacaoFluxoAlteracao());

		// substitui descri��o indice economico de acordo com o a lista do
		// indice
		if (saida.getCdIndicadorEconomicoReajuste() == 0) {
			setDsIndiceEconomicoReajusteHist("");
		} else {
			setIndiceEconomico(getListaIndiceEconomicoHash().get(saida.getCdIndicadorEconomicoReajuste()));
		}

	}

	/**
	 * Carrega combo tipo servico.
	 */
	public void carregaComboTipoServico() {
		this.listaTipoServico = new ArrayList<SelectItem>();

		List<ListarServicosSaidaDTO> saida = new ArrayList<ListarServicosSaidaDTO>();

		saida = comboService.listarTipoServicos(0);
		listaTipoServicoHash.clear();

		for (ListarServicosSaidaDTO combo : saida) {
			listaTipoServicoHash.put(combo.getCdServico(), combo.getDsServico());
			listaTipoServico.add(new SelectItem(combo.getCdServico(), combo.getDsServico()));
		}
	}

	/**
	 * Carrega combos.
	 */
	public void carregaCombos() {
		if (getCdTipoServicoFiltro() != 0) {
			setCdModalidadeFiltro(0);
			carregaComboModalidade();
			setDesabilitarCombo(true);
			setDesabilitarCombo2(false);
			setCdOperacaoFiltro(0);
			// if(getListaModalidade().size() == 0){
			carregaComboOperacao(getCdTipoServicoFiltro());
			// setDesabilitarCombo(false);
			setDesabilitarCombo2(true);
			// }
			return;
		}
		if (getCdTipoServicoFiltro() == 0) {
			setListaModalidade(new ArrayList<SelectItem>());
			setCdModalidadeFiltro(0);
			setListaOperacao(new ArrayList<SelectItem>());
			setCdOperacaoFiltro(0);
			setDesabilitarCombo(false);
			setDesabilitarCombo2(false);
			return;
		}
	}

	/**
	 * Carrega combos op.
	 */
	public void carregaCombosOp() {
		if (getCdTipoServicoFiltro() == 0) {
			setListaOperacao(new ArrayList<SelectItem>());
			setCdOperacaoFiltro(0);
			setDesabilitarCombo2(false);
		} else {
			if (getCdModalidadeFiltro() != 0) {
				carregaComboOperacao(getCdTipoServicoFiltro());
				setDesabilitarCombo2(true);
			} else {
				carregaComboOperacao(getCdTipoServicoFiltro());
				setDesabilitarCombo2(true);
			}
		}

	}

	/**
	 * Carrega combo modalidade.
	 */
	public void carregaComboModalidade() {
		this.listaModalidade = new ArrayList<SelectItem>();

		List<ListarModalidadeSaidaDTO> saida = new ArrayList<ListarModalidadeSaidaDTO>();
		ListarModalidadesEntradaDTO entrada = new ListarModalidadesEntradaDTO();
		entrada.setCdServico(getCdTipoServicoFiltro() == null ? 0 : getCdTipoServicoFiltro());

		saida = comboService.listarModalidades(entrada);
		listaModalidadeHash.clear();

		for (ListarModalidadeSaidaDTO combo : saida) {
			listaModalidadeHash.put(combo.getCdModalidade(), combo.getDsModalidade());
			listaModalidade.add(new SelectItem(combo.getCdModalidade(), combo.getDsModalidade()));
		}
	}

	/**
	 * Carrega combo operacao.
	 *
	 * @param a the a
	 */
	public void carregaComboOperacao(int a) {
		this.listaOperacao = new ArrayList<SelectItem>();

		List<ListarOperacaoServicoPagtoIntegradoSaidaDTO> saida = new ArrayList<ListarOperacaoServicoPagtoIntegradoSaidaDTO>();
		ListarOperacaoServicoPagtoIntegradoEntradaDTO entrada = new ListarOperacaoServicoPagtoIntegradoEntradaDTO();
		entrada.setCdProdutoOperacaoRelacionado(getCdModalidadeFiltro());
		entrada.setCdprodutoServicoOperacao(a);

		try {
			saida = comboService.listarOperacaoServicoPagtoIntegrado(entrada);
			listaOperacaoHash.clear();

			for (ListarOperacaoServicoPagtoIntegradoSaidaDTO combo : saida) {
				listaOperacaoHash.put(combo.getCdOperacaoProdutoServico(), combo.getDsOperacaoCatalogo());
				listaOperacao.add(new SelectItem(combo.getCdOperacaoProdutoServico(), combo.getDsOperacaoCatalogo()));
			}
		} catch (PdcAdapterFunctionalException e) {
			saida = new ArrayList<ListarOperacaoServicoPagtoIntegradoSaidaDTO>();
		}
	}

	/**
	 * Carrega lista periodicidade.
	 */
	public void carregaListaPeriodicidade() {

		this.listaPeriodicidade = new ArrayList<SelectItem>();

		List<PeriodicidadeSaidaDTO> saidaDTO = new ArrayList<PeriodicidadeSaidaDTO>();
		PeriodicidadeEntradaDTO entradaDTO = new PeriodicidadeEntradaDTO();
		entradaDTO.setCdSituacaoVinculacaoConta(0);

		saidaDTO = comboService.consultarListaPeriodicidade(entradaDTO);
		listaPeriodicidadeHash.clear();

		for (PeriodicidadeSaidaDTO combo : saidaDTO) {
			listaPeriodicidadeHash.put(combo.getCdPeriodicidade(), combo.getDsPeriodicidade());
			this.listaPeriodicidade.add(new SelectItem(combo.getCdPeriodicidade(), combo.getDsPeriodicidade()));
		}
	}

	/**
	 * Carrega lista indice economico.
	 */
	private void carregaListaIndiceEconomico() {
		this.listaIndiceEconomico = new ArrayList<SelectItem>();

		ListarTarifasContratoSaidaDTO listarTarifasContratoSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());

		List<ListarIndiceEconomicoSaidaDTO> saidaDTO = new ArrayList<ListarIndiceEconomicoSaidaDTO>();
		saidaDTO = comboService.listarIndiceEconomico(new ListarIndiceEconomicoEntradaDTO(listarTarifasContratoSaidaDTO.getCdModalidade()));
		listaIndiceEconomicoHash.clear();

		for (ListarIndiceEconomicoSaidaDTO combo : saidaDTO) {
			listaIndiceEconomicoHash.put(combo.getCdIndicadorEconomico(), combo.getDsIndicadorEconomico());
			this.listaIndiceEconomico.add(new SelectItem(combo.getCdIndicadorEconomico(), combo.getDsIndicadorEconomico(), String.valueOf(combo
					.getDsSiglaEconomico())));
		}
	}

	/**
	 * Preenche dados condicoes cobranca.
	 */
	public void preencheDadosCondicoesCobranca() {
		ListarTarifasContratoSaidaDTO listarTarifasContratoSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());
		ConsultarCondCobTarifaEntradaDTO entradaDTO = new ConsultarCondCobTarifaEntradaDTO();

		entradaDTO.setCdPessoaContrato(getCodPessoaJuridica());
		entradaDTO.setCdTipoContrato(getCdTipoContrato());
		entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
		entradaDTO.setCdServico(listarTarifasContratoSaidaDTO.getCdServico());
		entradaDTO.setCdModalidade(listarTarifasContratoSaidaDTO.getCdModalidade());
		entradaDTO.setCdRelacionamentoProduto(listarTarifasContratoSaidaDTO.getCdRelacionamentoProduto());

		ConsultarCondCobTarifaSaidaDTO saidaDTO = getManterContratoImpl().consultarCondCobTarifa(entradaDTO);

		setCodTipoServico(listarTarifasContratoSaidaDTO.getCdServico());
		setProdutoServicoDesc(listarTarifasContratoSaidaDTO.getDsModalidade());
		setCodModalidade(listarTarifasContratoSaidaDTO.getCdModalidade());
		setCdPeriodicidade(saidaDTO.getCdPeriodicidadeCobrancaTarifa());
		setPeriodicidade(saidaDTO.getDsPeriodicidadeCobrancaTarifa());

		if(saidaDTO.getNrFechamentoApuracaoTarifa() == 99){
			setDiaFechamento("");
			setDsDiaFechamento("Ultimo dia �til do m�s");
		}else{
			setDsDiaFechamento(Integer.toString(saidaDTO.getNrFechamentoApuracaoTarifa()));
			setDiaFechamento(Integer.toString(saidaDTO.getNrFechamentoApuracaoTarifa()));
		}

		if(saidaDTO.getQtDiaCobrancaTarifa() == 0){
			setDsQuantDia("Cobran�a no dia da apura��o");
		}else{
			setQuantDia(Integer.toString(saidaDTO.getQtDiaCobrancaTarifa()));
			setDsQuantDia(Integer.toString(saidaDTO.getQtDiaCobrancaTarifa()));
		}
	}

	/**
	 * Preenche dados condicoes reajuste.
	 */
	public void preencheDadosCondicoesReajuste() {
		ListarTarifasContratoSaidaDTO listarTarifasContratoSaidaDTO = getListaGridPesquisa().get(getItemSelecionadoLista());
		ConsultarCondReajusteTarifaEntradaDTO entradaDTO = new ConsultarCondReajusteTarifaEntradaDTO();

		entradaDTO.setCdPessoaContrato(getCodPessoaJuridica());
		entradaDTO.setCdTipoContrato(getCdTipoContrato());
		entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());
		entradaDTO.setCdServico(listarTarifasContratoSaidaDTO.getCdServico());
		entradaDTO.setCdModalidade(listarTarifasContratoSaidaDTO.getCdModalidade());
		entradaDTO.setCdRelacionamentoProduto(listarTarifasContratoSaidaDTO.getCdRelacionamentoProduto());

		ConsultarCondReajusteTarifaSaidaDTO saidaDTO = getManterContratoImpl().consultarCondReajusteTarifa(entradaDTO);

		setCodTipoServico(listarTarifasContratoSaidaDTO.getCdServico());
		setProdutoServicoDesc(listarTarifasContratoSaidaDTO.getDsModalidade());
		setCdRelacionamentoProduto(listarTarifasContratoSaidaDTO.getCdRelacionamentoProduto());
		setCodModalidade(listarTarifasContratoSaidaDTO.getCdModalidade());

		setCondReajusteTarifa(saidaDTO);

		setCdTipoReajusteFiltro(saidaDTO.getCdTipoReajusteTarifa());
		if (saidaDTO.getCdTipoReajusteTarifa() == 0) {
			setTipoReajuste(0);
		} else {
			setTipoReajuste(saidaDTO.getCdTipoReajusteTarifa());
		}

		if (saidaDTO.getQtMesReajusteTarifa() == 0) {
			setQuantMes("");
			setQuantMesFiltro("");
		} else {
			setQuantMes(Integer.toString(saidaDTO.getQtMesReajusteTarifa()));
			setQuantMesFiltro(Integer.toString(saidaDTO.getQtMesReajusteTarifa()));
		}
		setCdIndiceEconomicoFiltro(saidaDTO.getCdIndicadorEconomicoReajuste());
		if (saidaDTO.getCdIndicadorEconomicoReajuste() == 0) {
			setIndiceEconomico("");
		} else {
			setIndiceEconomico(getListaIndiceEconomicoHash().get(saidaDTO.getCdIndicadorEconomicoReajuste()));
		}

		if (saidaDTO.getCdPercentualReajusteTarifa().signum() == 0) {
			setPercentualIndice("");
			setPercentualIndiceFiltro(null);
		} else {
			setPercentualIndice(NumberUtils.format(saidaDTO.getCdPercentualReajusteTarifa()));
			setPercentualIndiceFiltro(saidaDTO.getCdPercentualReajusteTarifa());
		}

		if (saidaDTO.getCdPercentualTarifaCatalogo().signum() == 0) {
			setPercentualFlexibilizacao("");
			setPercentualFlexibilizacaoFiltro(null);
		} else {
			setPercentualFlexibilizacao(NumberUtils.format(saidaDTO.getCdPercentualTarifaCatalogo()));
			setPercentualFlexibilizacaoFiltro(saidaDTO.getCdPercentualTarifaCatalogo());
		}

		cdIndicadorTipoReajusteBloqueio = verificaTipoBloqueio(saidaDTO.getCdIndicadorTipoReajusteBloqueio());

	}

	/**
	 * Verifica tipo bloqueio.
	 *
	 * @param flag the flag
	 * @return true, if verifica tipo bloqueio
	 */
	public boolean verificaTipoBloqueio(String flag) {

		if ("N".equals(flag))
			return true;
		return false;

	}

	/**
	 * Carrega grid tarifas modalidade.
	 */
	public void carregaGridTarifasModalidade() {

		listaGridTarifasModalidade = new ArrayList<ConsultarTarifaContratoSaidaDTO>();

		try {
			ListarTarifasContratoSaidaDTO itemSelecionado = null;

			if (getItemSelecionadoLista() != null) {
				itemSelecionado = getListaGridPesquisa().get(getItemSelecionadoLista());
			}

			ConsultarTarifaContratoEntradaDTO entradaDTO = new ConsultarTarifaContratoEntradaDTO();

			/* Dados do Contrato */
			entradaDTO.setCdPessoaJuridica(getCodPessoaJuridica());
			entradaDTO.setCdTipoContrato(getCdTipoContrato());
			entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());

			/* Dados Tarifa */
			entradaDTO.setCdTipoServico(itemSelecionado == null ? 0 : itemSelecionado.getCdServico());
			entradaDTO.setCdModalidade(itemSelecionado == null ? 0 : itemSelecionado.getCdModalidade());
			entradaDTO.setCdRelacionamentoProduto(0);

			setListaGridTarifasModalidade(getManterContratoImpl().consultarTarifaContrato(entradaDTO));

			this.listaControleTarifas = new ArrayList<SelectItem>();

			for (int i = 0; i < getListaGridTarifasModalidade().size(); i++) {
				listaControleTarifas.add(new SelectItem(i, " "));
			}

			setItemSelecionadoTarifas(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridTarifasModalidade(null);
			setItemSelecionadoTarifas(null);
		}
	}

	/**
	 * Preenche detalhar tarifas.
	 *
	 * @return the string
	 */
	public String preencheDetalharTarifas() {

		ConsultarTarifaContratoEntradaDTO entradaDTO = new ConsultarTarifaContratoEntradaDTO();
		ConsultarTarifaContratoSaidaDTO consultarTarifaContratoSaidaTO = getListaGridTarifasModalidade().get(getItemSelecionadoTarifas());

		/* Dados do Contrato */
		entradaDTO.setCdPessoaJuridica(getCodPessoaJuridica());
		entradaDTO.setCdTipoContrato(getCdTipoContrato());
		entradaDTO.setNrSequenciaContrato(getNrSequenciaContrato());

		entradaDTO.setCdTipoServico(consultarTarifaContratoSaidaTO.getCdProduto());
		entradaDTO.setCdModalidade(consultarTarifaContratoSaidaTO.getCdProdutoServicoRelacionado());
		entradaDTO.setCdRelacionamentoProduto(0);

		/* Dados da Modalidade */

		setDsModModalidade(consultarTarifaContratoSaidaTO.getDsProdutoServicoRelacionado());
		setDsTipoServicoModalidade(consultarTarifaContratoSaidaTO.getDsProduto());

		/* Dados da Tarifa */

		setDsPeriodoVigencia(consultarTarifaContratoSaidaTO.getDtInicioVigencia() + " - " + consultarTarifaContratoSaidaTO.getDtFimVigencia());
		setDtInicioVig(consultarTarifaContratoSaidaTO.getDtInicioVigencia());
		setDtFimVig(consultarTarifaContratoSaidaTO.getDtFimVigencia());
		setDsValorContratado(consultarTarifaContratoSaidaTO.getVlTarifa()); // conforme
		setDsValorReferenciaMin(consultarTarifaContratoSaidaTO.getVlrReferenciaMinimo());
		setDsValorReferenciaMax(consultarTarifaContratoSaidaTO.getVlrReferenciaMaximo());
		setDsOperacao(consultarTarifaContratoSaidaTO.getDsOperacao());

		/* Trilha de Auditoria */

		setDataHoraManutencao(consultarTarifaContratoSaidaTO.getHrManutencaoRegistro());
		setDataHoraInclusao(consultarTarifaContratoSaidaTO.getHrInclusaoRegistro());

		setTipoCanalInclusao(consultarTarifaContratoSaidaTO.getCdTipoCanalInclusao() != 0 ? consultarTarifaContratoSaidaTO.getCdTipoCanalInclusao()
				+ " - " + consultarTarifaContratoSaidaTO.getDsTipoCanalInclusao() : "");
		setTipoCanalManutencao(consultarTarifaContratoSaidaTO.getCdTipoCanalManutencao() == 0 ? "" : consultarTarifaContratoSaidaTO
				.getCdTipoCanalManutencao()
				+ " - " + consultarTarifaContratoSaidaTO.getDsTipoCanalManutencao());

		setUsuarioInclusao(consultarTarifaContratoSaidaTO.getCdUsuarioInclusao());
		setUsuarioManutencao(consultarTarifaContratoSaidaTO.getCdUsuarioManutencao());

		setComplementoInclusao("");
		setComplementoManutencao("");

		return "DETALHAR_TARIFA_MODALIDADE";
	}

	/**
	 * Limpar tarifas modalidade.
	 */
	public void limparTarifasModalidade() {
		setItemSelecionadoTarifas(null);
	}

	public void limpaCampoQtdDia(){
		setQuantDiaFiltro("");
	}

	public void limpaCampoDiaFechamento(){
		setDiaFechamentoFiltro("");
	}

	/**
	 * Voltar tarifas.
	 *
	 * @return the string
	 */
	public String voltarTarifas() {
		setItemSelecionadoLista(null);
		return "VOLTAR_TARIFAS";
	}

	/**
	 * Voltar tarifas operacoes.
	 *
	 * @return the string
	 */
	public String voltarTarifasOperacoes() {
		setItemSelecionadoTarifas(null);
		return "VOLTAR";
	}

	/**
	 * Voltar tarifa historico.
	 *
	 * @return the string
	 */
	public String voltarTarifaHistorico() {
		setItemSelecionadoTarifas(null);
		return "VOLTAR";
	}

	/**
	 * Voltar tarifa historico detalhar.
	 *
	 * @return the string
	 */
	public String voltarTarifaHistoricoDetalhar() {
		setItemSelecionadoLista3(null);
		return "VOLTAR";
	}

	/**
	 * Get: dsValorReferenciaMax.
	 *
	 * @return dsValorReferenciaMax
	 */
	public String getDsValorReferenciaMax() {
		return dsValorReferenciaMax;
	}

	/**
	 * Set: dsValorReferenciaMax.
	 *
	 * @param dsValorReferenciaMax the ds valor referencia max
	 */
	public void setDsValorReferenciaMax(String dsValorReferenciaMax) {
		this.dsValorReferenciaMax = dsValorReferenciaMax;
	}

	/**
	 * Get: percentualFlexibilizacaoDesc.
	 *
	 * @return percentualFlexibilizacaoDesc
	 */
	public String getPercentualFlexibilizacaoDesc() {
		return percentualFlexibilizacaoDesc;
	}

	/**
	 * Set: percentualFlexibilizacaoDesc.
	 *
	 * @param percentualFlexibilizacaoDesc the percentual flexibilizacao desc
	 */
	public void setPercentualFlexibilizacaoDesc(String percentualFlexibilizacaoDesc) {
		this.percentualFlexibilizacaoDesc = percentualFlexibilizacaoDesc;
	}

	/**
	 * Get: percentualIndiceDesc.
	 *
	 * @return percentualIndiceDesc
	 */
	public String getPercentualIndiceDesc() {
		return percentualIndiceDesc;
	}

	/**
	 * Set: percentualIndiceDesc.
	 *
	 * @param percentualIndiceDesc the percentual indice desc
	 */
	public void setPercentualIndiceDesc(String percentualIndiceDesc) {
		this.percentualIndiceDesc = percentualIndiceDesc;
	}

	/**
	 * Get: periodoValidadeAte.
	 *
	 * @return periodoValidadeAte
	 */
	public String getPeriodoValidadeAte() {
		return periodoValidadeAte;
	}

	/**
	 * Set: periodoValidadeAte.
	 *
	 * @param periodoValidadeAte the periodo validade ate
	 */
	public void setPeriodoValidadeAte(String periodoValidadeAte) {
		this.periodoValidadeAte = periodoValidadeAte;
	}

	/**
	 * Get: periodoValidadeDe.
	 *
	 * @return periodoValidadeDe
	 */
	public String getPeriodoValidadeDe() {
		return periodoValidadeDe;
	}

	/**
	 * Set: periodoValidadeDe.
	 *
	 * @param periodoValidadeDe the periodo validade de
	 */
	public void setPeriodoValidadeDe(String periodoValidadeDe) {
		this.periodoValidadeDe = periodoValidadeDe;
	}

	/**
	 * Get: valorContratado.
	 *
	 * @return valorContratado
	 */
	public String getValorContratado() {
		return valorContratado;
	}

	/**
	 * Set: valorContratado.
	 *
	 * @param valorContratado the valor contratado
	 */
	public void setValorContratado(String valorContratado) {
		this.valorContratado = valorContratado;
	}

	/**
	 * Get: listaModalidadeHash.
	 *
	 * @return listaModalidadeHash
	 */
	Map<Integer, String> getListaModalidadeHash() {
		return listaModalidadeHash;
	}

	/**
	 * Set lista modalidade hash.
	 *
	 * @param listaModalidadeHash the lista modalidade hash
	 */
	void setListaModalidadeHash(Map<Integer, String> listaModalidadeHash) {
		this.listaModalidadeHash = listaModalidadeHash;
	}

	/**
	 * Get: listaOperacaoHash.
	 *
	 * @return listaOperacaoHash
	 */
	Map<Integer, String> getListaOperacaoHash() {
		return listaOperacaoHash;
	}

	/**
	 * Set lista operacao hash.
	 *
	 * @param listaOperacaoHash the lista operacao hash
	 */
	void setListaOperacaoHash(Map<Integer, String> listaOperacaoHash) {
		this.listaOperacaoHash = listaOperacaoHash;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: obrigatoriedadeHist.
	 *
	 * @return obrigatoriedadeHist
	 */
	public String getObrigatoriedadeHist() {
		return obrigatoriedadeHist;
	}

	/**
	 * Set: obrigatoriedadeHist.
	 *
	 * @param obrigatoriedadeHist the obrigatoriedade hist
	 */
	public void setObrigatoriedadeHist(String obrigatoriedadeHist) {
		this.obrigatoriedadeHist = obrigatoriedadeHist;
	}

	/**
	 * Is desabilitar combo.
	 *
	 * @return true, if is desabilitar combo
	 */
	public boolean isDesabilitarCombo() {
		return desabilitarCombo;
	}

	/**
	 * Set: desabilitarCombo.
	 *
	 * @param desabilitarCombo the desabilitar combo
	 */
	public void setDesabilitarCombo(boolean desabilitarCombo) {
		this.desabilitarCombo = desabilitarCombo;
	}

	/**
	 * Is desabilitar combo2.
	 *
	 * @return true, if is desabilitar combo2
	 */
	public boolean isDesabilitarCombo2() {
		return desabilitarCombo2;
	}

	/**
	 * Set: desabilitarCombo2.
	 *
	 * @param desabilitarCombo2 the desabilitar combo2
	 */
	public void setDesabilitarCombo2(boolean desabilitarCombo2) {
		this.desabilitarCombo2 = desabilitarCombo2;
	}

	/**
	 * Is desabilitar combo3.
	 *
	 * @return true, if is desabilitar combo3
	 */
	public boolean isDesabilitarCombo3() {
		return desabilitarCombo3;
	}

	/**
	 * Set: desabilitarCombo3.
	 *
	 * @param desabilitarCombo3 the desabilitar combo3
	 */
	public void setDesabilitarCombo3(boolean desabilitarCombo3) {
		this.desabilitarCombo3 = desabilitarCombo3;
	}

	/**
	 * Is btn consultar hist.
	 *
	 * @return true, if is btn consultar hist
	 */
	public boolean isBtnConsultarHist() {
		return btnConsultarHist;
	}

	/**
	 * Set: btnConsultarHist.
	 *
	 * @param btnConsultarHist the btn consultar hist
	 */
	public void setBtnConsultarHist(boolean btnConsultarHist) {
		this.btnConsultarHist = btnConsultarHist;
	}

	/**
	 * Get: valorReferenciaMax.
	 *
	 * @return valorReferenciaMax
	 */
	public String getValorReferenciaMax() {
		return valorReferenciaMax;
	}

	/**
	 * Set: valorReferenciaMax.
	 *
	 * @param valorReferenciaMax the valor referencia max
	 */
	public void setValorReferenciaMax(String valorReferenciaMax) {
		this.valorReferenciaMax = valorReferenciaMax;
	}

	/**
	 * Get: valorReferenciaMin.
	 *
	 * @return valorReferenciaMin
	 */
	public String getValorReferenciaMin() {
		return valorReferenciaMin;
	}

	/**
	 * Set: valorReferenciaMin.
	 *
	 * @param valorReferenciaMin the valor referencia min
	 */
	public void setValorReferenciaMin(String valorReferenciaMin) {
		this.valorReferenciaMin = valorReferenciaMin;
	}

	/**
	 * Is bto acionado.
	 *
	 * @return true, if is bto acionado
	 */
	public boolean isBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: condReajusteTarifa.
	 *
	 * @return condReajusteTarifa
	 */
	public ConsultarCondReajusteTarifaSaidaDTO getCondReajusteTarifa() {
		return condReajusteTarifa;
	}

	/**
	 * Set: condReajusteTarifa.
	 *
	 * @param condReajusteTarifa the cond reajuste tarifa
	 */
	public void setCondReajusteTarifa(ConsultarCondReajusteTarifaSaidaDTO condReajusteTarifa) {
		this.condReajusteTarifa = condReajusteTarifa;
	}

	/**
	 * Get: listaTipoReajuste.
	 *
	 * @return listaTipoReajuste
	 */
	public List<SelectItem> getListaTipoReajuste() {
		return listaTipoReajuste;
	}

	/**
	 * Set: listaTipoReajuste.
	 *
	 * @param listaTipoReajuste the lista tipo reajuste
	 */
	public void setListaTipoReajuste(List<SelectItem> listaTipoReajuste) {
		this.listaTipoReajuste = listaTipoReajuste;
	}

	/**
	 * Get: dsTipoReajuste.
	 *
	 * @return dsTipoReajuste
	 */
	public String getDsTipoReajuste() {
		return dsTipoReajuste;
	}

	/**
	 * Set: dsTipoReajuste.
	 *
	 * @param dsTipoReajuste the ds tipo reajuste
	 */
	public void setDsTipoReajuste(String dsTipoReajuste) {
		this.dsTipoReajuste = dsTipoReajuste;
	}

	/**
	 * Get: tpManutencao.
	 *
	 * @return tpManutencao
	 */
	public String getTpManutencao() {
		return tpManutencao;
	}

	/**
	 * Set: tpManutencao.
	 *
	 * @param tpManutencao the tp manutencao
	 */
	public void setTpManutencao(String tpManutencao) {
		this.tpManutencao = tpManutencao;
	}

	/**
	 * Get: dtInicioVig.
	 *
	 * @return dtInicioVig
	 */
	public String getDtInicioVig() {
		return dtInicioVig;
	}

	/**
	 * Set: dtInicioVig.
	 *
	 * @param dtInicioVig the dt inicio vig
	 */
	public void setDtInicioVig(String dtInicioVig) {
		this.dtInicioVig = dtInicioVig;
	}

	/**
	 * Get: dtFimVig.
	 *
	 * @return dtFimVig
	 */
	public String getDtFimVig() {
		return dtFimVig;
	}

	/**
	 * Set: dtFimVig.
	 *
	 * @param dtFimVig the dt fim vig
	 */
	public void setDtFimVig(String dtFimVig) {
		this.dtFimVig = dtFimVig;
	}

	/**
	 * Get: listaGridPesquisaHistCobr.
	 *
	 * @return listaGridPesquisaHistCobr
	 */
	public List<ListarConManServicoSaidaDTO> getListaGridPesquisaHistCobr() {
		return listaGridPesquisaHistCobr;
	}

	/**
	 * Set: listaGridPesquisaHistCobr.
	 *
	 * @param listaGridPesquisaHistCobr the lista grid pesquisa hist cobr
	 */
	public void setListaGridPesquisaHistCobr(List<ListarConManServicoSaidaDTO> listaGridPesquisaHistCobr) {
		this.listaGridPesquisaHistCobr = listaGridPesquisaHistCobr;
	}

	/**
	 * Get: listaGridPesquisaHistReaj.
	 *
	 * @return listaGridPesquisaHistReaj
	 */
	public List<ListarConManServicoSaidaDTO> getListaGridPesquisaHistReaj() {
		return listaGridPesquisaHistReaj;
	}

	/**
	 * Set: listaGridPesquisaHistReaj.
	 *
	 * @param listaGridPesquisaHistReaj the lista grid pesquisa hist reaj
	 */
	public void setListaGridPesquisaHistReaj(List<ListarConManServicoSaidaDTO> listaGridPesquisaHistReaj) {
		this.listaGridPesquisaHistReaj = listaGridPesquisaHistReaj;
	}

	/**
	 * Get: itemSelecionadoListaHistCobr.
	 *
	 * @return itemSelecionadoListaHistCobr
	 */
	public Integer getItemSelecionadoListaHistCobr() {
		return itemSelecionadoListaHistCobr;
	}

	/**
	 * Set: itemSelecionadoListaHistCobr.
	 *
	 * @param itemSelecionadoListaHistCobr the item selecionado lista hist cobr
	 */
	public void setItemSelecionadoListaHistCobr(Integer itemSelecionadoListaHistCobr) {
		this.itemSelecionadoListaHistCobr = itemSelecionadoListaHistCobr;
	}

	/**
	 * Get: listaControleRadioHistCobr.
	 *
	 * @return listaControleRadioHistCobr
	 */
	public List<SelectItem> getListaControleRadioHistCobr() {
		return listaControleRadioHistCobr;
	}

	/**
	 * Set: listaControleRadioHistCobr.
	 *
	 * @param listaControleRadioHistCobr the lista controle radio hist cobr
	 */
	public void setListaControleRadioHistCobr(List<SelectItem> listaControleRadioHistCobr) {
		this.listaControleRadioHistCobr = listaControleRadioHistCobr;
	}

	/**
	 * Get: itemSelecionadoListaHistReaj.
	 *
	 * @return itemSelecionadoListaHistReaj
	 */
	public Integer getItemSelecionadoListaHistReaj() {
		return itemSelecionadoListaHistReaj;
	}

	/**
	 * Set: itemSelecionadoListaHistReaj.
	 *
	 * @param itemSelecionadoListaHistReaj the item selecionado lista hist reaj
	 */
	public void setItemSelecionadoListaHistReaj(Integer itemSelecionadoListaHistReaj) {
		this.itemSelecionadoListaHistReaj = itemSelecionadoListaHistReaj;
	}

	/**
	 * Get: listaControleRadioHistReaj.
	 *
	 * @return listaControleRadioHistReaj
	 */
	public List<SelectItem> getListaControleRadioHistReaj() {
		return listaControleRadioHistReaj;
	}

	/**
	 * Set: listaControleRadioHistReaj.
	 *
	 * @param listaControleRadioHistReaj the lista controle radio hist reaj
	 */
	public void setListaControleRadioHistReaj(List<SelectItem> listaControleRadioHistReaj) {
		this.listaControleRadioHistReaj = listaControleRadioHistReaj;
	}

	/**
	 * Get: listaGridOculta.
	 *
	 * @return listaGridOculta
	 */
	public List<ListarModalidadeTipoServicoSaidaDTO> getListaGridOculta() {
		return listaGridOculta;
	}

	/**
	 * Set: listaGridOculta.
	 *
	 * @param listaGridOculta the lista grid oculta
	 */
	public void setListaGridOculta(List<ListarModalidadeTipoServicoSaidaDTO> listaGridOculta) {
		this.listaGridOculta = listaGridOculta;
	}

	/**
	 * Get: periodicidadeHist.
	 *
	 * @return periodicidadeHist
	 */
	public String getPeriodicidadeHist() {
		return periodicidadeHist;
	}

	/**
	 * Set: periodicidadeHist.
	 *
	 * @param periodicidadeHist the periodicidade hist
	 */
	public void setPeriodicidadeHist(String periodicidadeHist) {
		this.periodicidadeHist = periodicidadeHist;
	}

	/**
	 * Get: diaFechamentoHist.
	 *
	 * @return diaFechamentoHist
	 */
	public String getDiaFechamentoHist() {
		return diaFechamentoHist;
	}

	/**
	 * Set: diaFechamentoHist.
	 *
	 * @param diaFechamentoHist the dia fechamento hist
	 */
	public void setDiaFechamentoHist(String diaFechamentoHist) {
		this.diaFechamentoHist = diaFechamentoHist;
	}

	/**
	 * Get: quantDiaHist.
	 *
	 * @return quantDiaHist
	 */
	public String getQuantDiaHist() {
		return quantDiaHist;
	}

	/**
	 * Set: quantDiaHist.
	 *
	 * @param quantDiaHist the quant dia hist
	 */
	public void setQuantDiaHist(String quantDiaHist) {
		this.quantDiaHist = quantDiaHist;
	}

	/**
	 * Get: dataHoraInclusaoHist.
	 *
	 * @return dataHoraInclusaoHist
	 */
	public String getDataHoraInclusaoHist() {
		return dataHoraInclusaoHist;
	}

	/**
	 * Set: dataHoraInclusaoHist.
	 *
	 * @param dataHoraInclusaoHist the data hora inclusao hist
	 */
	public void setDataHoraInclusaoHist(String dataHoraInclusaoHist) {
		this.dataHoraInclusaoHist = dataHoraInclusaoHist;
	}

	/**
	 * Get: dataHoraAlteracaoHist.
	 *
	 * @return dataHoraAlteracaoHist
	 */
	public String getDataHoraAlteracaoHist() {
		return dataHoraAlteracaoHist;
	}

	/**
	 * Set: dataHoraAlteracaoHist.
	 *
	 * @param dataHoraAlteracaoHist the data hora alteracao hist
	 */
	public void setDataHoraAlteracaoHist(String dataHoraAlteracaoHist) {
		this.dataHoraAlteracaoHist = dataHoraAlteracaoHist;
	}

	/**
	 * Get: cdUsuarioInclusaoHist.
	 *
	 * @return cdUsuarioInclusaoHist
	 */
	public String getCdUsuarioInclusaoHist() {
		return cdUsuarioInclusaoHist;
	}

	/**
	 * Set: cdUsuarioInclusaoHist.
	 *
	 * @param cdUsuarioInclusaoHist the cd usuario inclusao hist
	 */
	public void setCdUsuarioInclusaoHist(String cdUsuarioInclusaoHist) {
		this.cdUsuarioInclusaoHist = cdUsuarioInclusaoHist;
	}

	/**
	 * Get: cdCanalInclusaoFormatadoHist.
	 *
	 * @return cdCanalInclusaoFormatadoHist
	 */
	public String getCdCanalInclusaoFormatadoHist() {
		return cdCanalInclusaoFormatadoHist;
	}

	/**
	 * Set: cdCanalInclusaoFormatadoHist.
	 *
	 * @param cdCanalInclusaoFormatadoHist the cd canal inclusao formatado hist
	 */
	public void setCdCanalInclusaoFormatadoHist(String cdCanalInclusaoFormatadoHist) {
		this.cdCanalInclusaoFormatadoHist = cdCanalInclusaoFormatadoHist;
	}

	/**
	 * Get: dsComplementoInclusaoHist.
	 *
	 * @return dsComplementoInclusaoHist
	 */
	public String getDsComplementoInclusaoHist() {
		return dsComplementoInclusaoHist;
	}

	/**
	 * Set: dsComplementoInclusaoHist.
	 *
	 * @param dsComplementoInclusaoHist the ds complemento inclusao hist
	 */
	public void setDsComplementoInclusaoHist(String dsComplementoInclusaoHist) {
		this.dsComplementoInclusaoHist = dsComplementoInclusaoHist;
	}

	/**
	 * Get: cdUsuarioAlteracaoHist.
	 *
	 * @return cdUsuarioAlteracaoHist
	 */
	public String getCdUsuarioAlteracaoHist() {
		return cdUsuarioAlteracaoHist;
	}

	/**
	 * Set: cdUsuarioAlteracaoHist.
	 *
	 * @param cdUsuarioAlteracaoHist the cd usuario alteracao hist
	 */
	public void setCdUsuarioAlteracaoHist(String cdUsuarioAlteracaoHist) {
		this.cdUsuarioAlteracaoHist = cdUsuarioAlteracaoHist;
	}

	/**
	 * Get: cdCanalAlteracaoFormatadoHist.
	 *
	 * @return cdCanalAlteracaoFormatadoHist
	 */
	public String getCdCanalAlteracaoFormatadoHist() {
		return cdCanalAlteracaoFormatadoHist;
	}

	/**
	 * Set: cdCanalAlteracaoFormatadoHist.
	 *
	 * @param cdCanalAlteracaoFormatadoHist the cd canal alteracao formatado hist
	 */
	public void setCdCanalAlteracaoFormatadoHist(String cdCanalAlteracaoFormatadoHist) {
		this.cdCanalAlteracaoFormatadoHist = cdCanalAlteracaoFormatadoHist;
	}

	/**
	 * Get: dsComplementoAlteracaoHist.
	 *
	 * @return dsComplementoAlteracaoHist
	 */
	public String getDsComplementoAlteracaoHist() {
		return dsComplementoAlteracaoHist;
	}

	/**
	 * Set: dsComplementoAlteracaoHist.
	 *
	 * @param dsComplementoAlteracaoHist the ds complemento alteracao hist
	 */
	public void setDsComplementoAlteracaoHist(String dsComplementoAlteracaoHist) {
		this.dsComplementoAlteracaoHist = dsComplementoAlteracaoHist;
	}

	/**
	 * Get: dsTipoReajusteTarifaHist.
	 *
	 * @return dsTipoReajusteTarifaHist
	 */
	public String getDsTipoReajusteTarifaHist() {
		return dsTipoReajusteTarifaHist;
	}

	/**
	 * Set: dsTipoReajusteTarifaHist.
	 *
	 * @param dsTipoReajusteTarifaHist the ds tipo reajuste tarifa hist
	 */
	public void setDsTipoReajusteTarifaHist(String dsTipoReajusteTarifaHist) {
		this.dsTipoReajusteTarifaHist = dsTipoReajusteTarifaHist;
	}

	/**
	 * Get: qtdeMesReajusteTarifaHist.
	 *
	 * @return qtdeMesReajusteTarifaHist
	 */
	public String getQtdeMesReajusteTarifaHist() {
		return qtdeMesReajusteTarifaHist;
	}

	/**
	 * Set: qtdeMesReajusteTarifaHist.
	 *
	 * @param qtdeMesReajusteTarifaHist the qtde mes reajuste tarifa hist
	 */
	public void setQtdeMesReajusteTarifaHist(String qtdeMesReajusteTarifaHist) {
		this.qtdeMesReajusteTarifaHist = qtdeMesReajusteTarifaHist;
	}

	/**
	 * Get: cdIndiceEconomicoReajusteHist.
	 *
	 * @return cdIndiceEconomicoReajusteHist
	 */
	public String getCdIndiceEconomicoReajusteHist() {
		return cdIndiceEconomicoReajusteHist;
	}

	/**
	 * Set: cdIndiceEconomicoReajusteHist.
	 *
	 * @param cdIndiceEconomicoReajusteHist the cd indice economico reajuste hist
	 */
	public void setCdIndiceEconomicoReajusteHist(String cdIndiceEconomicoReajusteHist) {
		this.cdIndiceEconomicoReajusteHist = cdIndiceEconomicoReajusteHist;
	}

	/**
	 * Get: percentualIndiceReajusteTarifaHist.
	 *
	 * @return percentualIndiceReajusteTarifaHist
	 */
	public String getPercentualIndiceReajusteTarifaHist() {
		return percentualIndiceReajusteTarifaHist;
	}

	/**
	 * Set: percentualIndiceReajusteTarifaHist.
	 *
	 * @param percentualIndiceReajusteTarifaHist the percentual indice reajuste tarifa hist
	 */
	public void setPercentualIndiceReajusteTarifaHist(String percentualIndiceReajusteTarifaHist) {
		this.percentualIndiceReajusteTarifaHist = percentualIndiceReajusteTarifaHist;
	}

	/**
	 * Get: percentualFlexCatReajsuteTarifaHist.
	 *
	 * @return percentualFlexCatReajsuteTarifaHist
	 */
	public String getPercentualFlexCatReajsuteTarifaHist() {
		return percentualFlexCatReajsuteTarifaHist;
	}

	/**
	 * Set: percentualFlexCatReajsuteTarifaHist.
	 *
	 * @param percentualFlexCatReajsuteTarifaHist the percentual flex cat reajsute tarifa hist
	 */
	public void setPercentualFlexCatReajsuteTarifaHist(String percentualFlexCatReajsuteTarifaHist) {
		this.percentualFlexCatReajsuteTarifaHist = percentualFlexCatReajsuteTarifaHist;
	}

	/**
	 * Get: dsIndiceEconomicoReajusteHist.
	 *
	 * @return dsIndiceEconomicoReajusteHist
	 */
	public String getDsIndiceEconomicoReajusteHist() {
		return dsIndiceEconomicoReajusteHist;
	}

	/**
	 * Set: dsIndiceEconomicoReajusteHist.
	 *
	 * @param dsIndiceEconomicoReajusteHist the ds indice economico reajuste hist
	 */
	public void setDsIndiceEconomicoReajusteHist(String dsIndiceEconomicoReajusteHist) {
		this.dsIndiceEconomicoReajusteHist = dsIndiceEconomicoReajusteHist;
	}

	/**
	 * Is cd indicador tipo reajuste bloqueio.
	 *
	 * @return true, if is cd indicador tipo reajuste bloqueio
	 */
	public boolean isCdIndicadorTipoReajusteBloqueio() {
		return cdIndicadorTipoReajusteBloqueio;
	}

	// Set's e Get's//

	/**
	 * Checks if is chk dia fechamento.
	 *
	 * @return true, if is chk dia fechamento
	 */
	public boolean isChkDiaFechamento() {
		return chkDiaFechamento;
	}

	/**
	 * Sets the chk dia fechamento.
	 *
	 * @param chkDiaFechamento the new chk dia fechamento
	 */
	public void setChkDiaFechamento(boolean chkDiaFechamento) {
		this.chkDiaFechamento = chkDiaFechamento;
	}

	/**
	 * Get: dsIndiceEconomico.
	 *
	 * @return dsIndiceEconomico
	 */
	public String getDsIndiceEconomico() {
		return dsIndiceEconomico;
	}

	/**
	 * Set: dsIndiceEconomico.
	 *
	 * @param dsIndiceEconomico the ds indice economico
	 */
	public void setDsIndiceEconomico(String dsIndiceEconomico) {
		this.dsIndiceEconomico = dsIndiceEconomico;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<ListarTarifasContratoSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(List<ListarTarifasContratoSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: listaIndiceEconomico.
	 *
	 * @return listaIndiceEconomico
	 */
	public List<SelectItem> getListaIndiceEconomico() {
		return listaIndiceEconomico;
	}

	/**
	 * Set: listaIndiceEconomico.
	 *
	 * @param listaIndiceEconomico the lista indice economico
	 */
	public void setListaIndiceEconomico(List<SelectItem> listaIndiceEconomico) {
		this.listaIndiceEconomico = listaIndiceEconomico;
	}

	/**
	 * Get: cdPeriodicidadeFiltro.
	 *
	 * @return cdPeriodicidadeFiltro
	 */
	public Integer getCdPeriodicidadeFiltro() {
		return cdPeriodicidadeFiltro;
	}

	/**
	 * Get: dsModModalidade.
	 *
	 * @return dsModModalidade
	 */
	public String getDsModModalidade() {
		return dsModModalidade;
	}

	/**
	 * Set: dsModModalidade.
	 *
	 * @param dsModModalidade the ds mod modalidade
	 */
	public void setDsModModalidade(String dsModModalidade) {
		this.dsModModalidade = dsModModalidade;
	}

	/**
	 * Get: dsTipoServicoModalidade.
	 *
	 * @return dsTipoServicoModalidade
	 */
	public String getDsTipoServicoModalidade() {
		return dsTipoServicoModalidade;
	}

	/**
	 * Set: dsTipoServicoModalidade.
	 *
	 * @param dsTipoServicoModalidade the ds tipo servico modalidade
	 */
	public void setDsTipoServicoModalidade(String dsTipoServicoModalidade) {
		this.dsTipoServicoModalidade = dsTipoServicoModalidade;
	}

	/**
	 * Set: cdPeriodicidadeFiltro.
	 *
	 * @param cdPeriodicidadeFiltro the cd periodicidade filtro
	 */
	public void setCdPeriodicidadeFiltro(Integer cdPeriodicidadeFiltro) {
		this.cdPeriodicidadeFiltro = cdPeriodicidadeFiltro;
	}

	/**
	 * Get: diaFechamento.
	 *
	 * @return diaFechamento
	 */
	public String getDiaFechamento() {
		return diaFechamento;
	}

	/**
	 * Set: diaFechamento.
	 *
	 * @param diaFechamento the dia fechamento
	 */
	public void setDiaFechamento(String diaFechamento) {
		this.diaFechamento = diaFechamento;
	}

	/**
	 * Get: itemSelecionadoTarifas.
	 *
	 * @return itemSelecionadoTarifas
	 */
	public Integer getItemSelecionadoTarifas() {
		return itemSelecionadoTarifas;
	}

	/**
	 * Set: itemSelecionadoTarifas.
	 *
	 * @param itemSelecionadoTarifas the item selecionado tarifas
	 */
	public void setItemSelecionadoTarifas(Integer itemSelecionadoTarifas) {
		this.itemSelecionadoTarifas = itemSelecionadoTarifas;
	}

	/**
	 * Get: indiceEconomico.
	 *
	 * @return indiceEconomico
	 */
	public String getIndiceEconomico() {
		return indiceEconomico;
	}

	/**
	 * Set: indiceEconomico.
	 *
	 * @param indiceEconomico the indice economico
	 */
	public void setIndiceEconomico(String indiceEconomico) {
		this.indiceEconomico = indiceEconomico;
	}

	/**
	 * Get: percentualFlexibilizacao.
	 *
	 * @return percentualFlexibilizacao
	 */
	public String getPercentualFlexibilizacao() {
		return percentualFlexibilizacao;
	}

	/**
	 * Set: percentualFlexibilizacao.
	 *
	 * @param percentualFlexibilizacao the percentual flexibilizacao
	 */
	public void setPercentualFlexibilizacao(String percentualFlexibilizacao) {
		this.percentualFlexibilizacao = percentualFlexibilizacao;
	}

	/**
	 * Get: periodicidade.
	 *
	 * @return periodicidade
	 */
	public String getPeriodicidade() {
		return periodicidade;
	}

	/**
	 * Get: listaGridTarifasModalidade.
	 *
	 * @return listaGridTarifasModalidade
	 */
	public List<ConsultarTarifaContratoSaidaDTO> getListaGridTarifasModalidade() {
		return listaGridTarifasModalidade;
	}

	/**
	 * Set: listaGridTarifasModalidade.
	 *
	 * @param listaGridTarifasModalidade the lista grid tarifas modalidade
	 */
	public void setListaGridTarifasModalidade(List<ConsultarTarifaContratoSaidaDTO> listaGridTarifasModalidade) {
		this.listaGridTarifasModalidade = listaGridTarifasModalidade;
	}

	/**
	 * Set: periodicidade.
	 *
	 * @param periodicidade the periodicidade
	 */
	public void setPeriodicidade(String periodicidade) {
		this.periodicidade = periodicidade;
	}

	/**
	 * Get: produtoServicoDesc.
	 *
	 * @return produtoServicoDesc
	 */
	public String getProdutoServicoDesc() {
		return produtoServicoDesc;
	}

	/**
	 * Set: produtoServicoDesc.
	 *
	 * @param produtoServicoDesc the produto servico desc
	 */
	public void setProdutoServicoDesc(String produtoServicoDesc) {
		this.produtoServicoDesc = produtoServicoDesc;
	}

	/**
	 * Get: dsOperacao.
	 *
	 * @return dsOperacao
	 */
	public String getDsOperacao() {
		return dsOperacao;
	}

	/**
	 * Set: dsOperacao.
	 *
	 * @param dsOperacao the ds operacao
	 */
	public void setDsOperacao(String dsOperacao) {
		this.dsOperacao = dsOperacao;
	}

	/**
	 * Get: quantDia.
	 *
	 * @return quantDia
	 */
	public String getQuantDia() {
		return quantDia;
	}

	/**
	 * Set: quantDia.
	 *
	 * @param quantDia the quant dia
	 */
	public void setQuantDia(String quantDia) {
		this.quantDia = quantDia;
	}

	/**
	 * Get: quantMes.
	 *
	 * @return quantMes
	 */
	public String getQuantMes() {
		return quantMes;
	}

	/**
	 * Set: quantMes.
	 *
	 * @param quantMes the quant mes
	 */
	public void setQuantMes(String quantMes) {
		this.quantMes = quantMes;
	}

	/**
	 * Get: tipoReajuste.
	 *
	 * @return tipoReajuste
	 */
	public Integer getTipoReajuste() {
		return tipoReajuste;
	}

	/**
	 * Set: tipoReajuste.
	 *
	 * @param tipoReajuste the tipo reajuste
	 */
	public void setTipoReajuste(Integer tipoReajuste) {
		this.tipoReajuste = tipoReajuste;
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: percentualIndice.
	 *
	 * @return percentualIndice
	 */
	public String getPercentualIndice() {
		return percentualIndice;
	}

	/**
	 * Set: percentualIndice.
	 *
	 * @param percentualIndice the percentual indice
	 */
	public void setPercentualIndice(String percentualIndice) {
		this.percentualIndice = percentualIndice;
	}

	/**
	 * Get: cdTipoReajusteFiltro.
	 *
	 * @return cdTipoReajusteFiltro
	 */
	public Integer getCdTipoReajusteFiltro() {
		return cdTipoReajusteFiltro;
	}

	/**
	 * Set: cdTipoReajusteFiltro.
	 *
	 * @param cdTipoReajusteFiltro the cd tipo reajuste filtro
	 */
	public void setCdTipoReajusteFiltro(Integer cdTipoReajusteFiltro) {
		this.cdTipoReajusteFiltro = cdTipoReajusteFiltro;
	}

	/**
	 * Get: diaFechamentoFiltro.
	 *
	 * @return diaFechamentoFiltro
	 */
	public String getDiaFechamentoFiltro() {
		return diaFechamentoFiltro;
	}

	/**
	 * Set: diaFechamentoFiltro.
	 *
	 * @param diaFechamentoFiltro the dia fechamento filtro
	 */
	public void setDiaFechamentoFiltro(String diaFechamentoFiltro) {
		this.diaFechamentoFiltro = diaFechamentoFiltro;
	}

	/**
	 * Get: dsPeriodicidade.
	 *
	 * @return dsPeriodicidade
	 */
	public String getDsPeriodicidade() {
		return dsPeriodicidade;
	}

	/**
	 * Set: dsPeriodicidade.
	 *
	 * @param dsPeriodicidade the ds periodicidade
	 */
	public void setDsPeriodicidade(String dsPeriodicidade) {
		this.dsPeriodicidade = dsPeriodicidade;
	}

	/**
	 * Get: quantDiaFiltro.
	 *
	 * @return quantDiaFiltro
	 */
	public String getQuantDiaFiltro() {
		return quantDiaFiltro;
	}

	/**
	 * Set: quantDiaFiltro.
	 *
	 * @param quantDiaFiltro the quant dia filtro
	 */
	public void setQuantDiaFiltro(String quantDiaFiltro) {
		this.quantDiaFiltro = quantDiaFiltro;
	}

	/**
	 * Get: quantMesFiltro.
	 *
	 * @return quantMesFiltro
	 */
	public String getQuantMesFiltro() {
		return quantMesFiltro;
	}

	/**
	 * Set: quantMesFiltro.
	 *
	 * @param quantMesFiltro the quant mes filtro
	 */
	public void setQuantMesFiltro(String quantMesFiltro) {
		this.quantMesFiltro = quantMesFiltro;
	}

	/**
	 * Get: cdIndiceEconomicoFiltro.
	 *
	 * @return cdIndiceEconomicoFiltro
	 */
	public Integer getCdIndiceEconomicoFiltro() {
		return cdIndiceEconomicoFiltro;
	}

	/**
	 * Set: cdIndiceEconomicoFiltro.
	 *
	 * @param cdIndiceEconomicoFiltro the cd indice economico filtro
	 */
	public void setCdIndiceEconomicoFiltro(Integer cdIndiceEconomicoFiltro) {
		this.cdIndiceEconomicoFiltro = cdIndiceEconomicoFiltro;
	}

	/**
	 * Get: modalidade.
	 *
	 * @return modalidade
	 */
	public String getModalidade() {
		return modalidade;
	}

	/**
	 * Set: modalidade.
	 *
	 * @param modalidade the modalidade
	 */
	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: valorContratadoFiltro.
	 *
	 * @return valorContratadoFiltro
	 */
	public String getValorContratadoFiltro() {
		return valorContratadoFiltro;
	}

	/**
	 * Set: valorContratadoFiltro.
	 *
	 * @param valorContratadoFiltro the valor contratado filtro
	 */
	public void setValorContratadoFiltro(String valorContratadoFiltro) {
		this.valorContratadoFiltro = valorContratadoFiltro;
	}

	/**
	 * Get: filtroDataAte.
	 *
	 * @return filtroDataAte
	 */
	public Date getFiltroDataAte() {
		return filtroDataAte;
	}

	/**
	 * Set: filtroDataAte.
	 *
	 * @param filtroDataAte the filtro data ate
	 */
	public void setFiltroDataAte(Date filtroDataAte) {
		this.filtroDataAte = filtroDataAte;
	}

	/**
	 * Get: filtroDataDe.
	 *
	 * @return filtroDataDe
	 */
	public Date getFiltroDataDe() {
		return filtroDataDe;
	}

	/**
	 * Set: filtroDataDe.
	 *
	 * @param filtroDataDe the filtro data de
	 */
	public void setFiltroDataDe(Date filtroDataDe) {
		this.filtroDataDe = filtroDataDe;
	}

	/**
	 * Get: itemSelecionadoLista3.
	 *
	 * @return itemSelecionadoLista3
	 */
	public Integer getItemSelecionadoLista3() {
		return itemSelecionadoLista3;
	}

	/**
	 * Set: itemSelecionadoLista3.
	 *
	 * @param itemSelecionadoLista3 the item selecionado lista3
	 */
	public void setItemSelecionadoLista3(Integer itemSelecionadoLista3) {
		this.itemSelecionadoLista3 = itemSelecionadoLista3;
	}

	/**
	 * Get: listaControleRadio3.
	 *
	 * @return listaControleRadio3
	 */
	public List<SelectItem> getListaControleRadio3() {
		return listaControleRadio3;
	}

	/**
	 * Set: listaControleRadio3.
	 *
	 * @param listaControleRadio3 the lista controle radio3
	 */
	public void setListaControleRadio3(List<SelectItem> listaControleRadio3) {
		this.listaControleRadio3 = listaControleRadio3;
	}

	/**
	 * Get: listaGridPesquisa3.
	 *
	 * @return listaGridPesquisa3
	 */
	public List<ListarConManTarifaSaidaDTO> getListaGridPesquisa3() {
		return listaGridPesquisa3;
	}

	/**
	 * Set: listaGridPesquisa3.
	 *
	 * @param listaGridPesquisa3 the lista grid pesquisa3
	 */
	public void setListaGridPesquisa3(List<ListarConManTarifaSaidaDTO> listaGridPesquisa3) {
		this.listaGridPesquisa3 = listaGridPesquisa3;
	}

	/**
	 * Get: cdTipoServicoFiltro.
	 *
	 * @return cdTipoServicoFiltro
	 */
	public Integer getCdTipoServicoFiltro() {
		return cdTipoServicoFiltro;
	}

	/**
	 * Set: cdTipoServicoFiltro.
	 *
	 * @param cdTipoServicoFiltro the cd tipo servico filtro
	 */
	public void setCdTipoServicoFiltro(Integer cdTipoServicoFiltro) {
		this.cdTipoServicoFiltro = cdTipoServicoFiltro;
	}

	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {
		return listaTipoServico;
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: cdModalidadeFiltro.
	 *
	 * @return cdModalidadeFiltro
	 */
	public Integer getCdModalidadeFiltro() {
		return cdModalidadeFiltro;
	}

	/**
	 * Set: cdModalidadeFiltro.
	 *
	 * @param cdModalidadeFiltro the cd modalidade filtro
	 */
	public void setCdModalidadeFiltro(Integer cdModalidadeFiltro) {
		this.cdModalidadeFiltro = cdModalidadeFiltro;
	}

	/**
	 * Get: cdOperacaoFiltro.
	 *
	 * @return cdOperacaoFiltro
	 */
	public Integer getCdOperacaoFiltro() {
		return cdOperacaoFiltro;
	}

	/**
	 * Set: cdOperacaoFiltro.
	 *
	 * @param cdOperacaoFiltro the cd operacao filtro
	 */
	public void setCdOperacaoFiltro(Integer cdOperacaoFiltro) {
		this.cdOperacaoFiltro = cdOperacaoFiltro;
	}

	/**
	 * Get: listaModalidade.
	 *
	 * @return listaModalidade
	 */
	public List<SelectItem> getListaModalidade() {
		return listaModalidade;
	}

	/**
	 * Set: listaModalidade.
	 *
	 * @param listaModalidade the lista modalidade
	 */
	public void setListaModalidade(List<SelectItem> listaModalidade) {
		this.listaModalidade = listaModalidade;
	}

	/**
	 * Get: listaOperacao.
	 *
	 * @return listaOperacao
	 */
	public List<SelectItem> getListaOperacao() {
		return listaOperacao;
	}

	/**
	 * Set: listaOperacao.
	 *
	 * @param listaOperacao the lista operacao
	 */
	public void setListaOperacao(List<SelectItem> listaOperacao) {
		this.listaOperacao = listaOperacao;
	}

	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}

	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: ativEconRepresentante.
	 *
	 * @return ativEconRepresentante
	 */
	public String getAtivEconRepresentante() {
		return ativEconRepresentante;
	}

	/**
	 * Set: ativEconRepresentante.
	 *
	 * @param ativEconRepresentante the ativ econ representante
	 */
	public void setAtivEconRepresentante(String ativEconRepresentante) {
		this.ativEconRepresentante = ativEconRepresentante;
	}

	/**
	 * Get: cpfCnpjRepresentante.
	 *
	 * @return cpfCnpjRepresentante
	 */
	public String getCpfCnpjRepresentante() {
		return cpfCnpjRepresentante;
	}

	/**
	 * Set: cpfCnpjRepresentante.
	 *
	 * @param cpfCnpjRepresentante the cpf cnpj representante
	 */
	public void setCpfCnpjRepresentante(String cpfCnpjRepresentante) {
		this.cpfCnpjRepresentante = cpfCnpjRepresentante;
	}

	/**
	 * Get: empresaContrato.
	 *
	 * @return empresaContrato
	 */
	public String getEmpresaContrato() {
		return empresaContrato;
	}

	/**
	 * Set: empresaContrato.
	 *
	 * @param empresaContrato the empresa contrato
	 */
	public void setEmpresaContrato(String empresaContrato) {
		this.empresaContrato = empresaContrato;
	}

	/**
	 * Get: grupEconRepresentante.
	 *
	 * @return grupEconRepresentante
	 */
	public String getGrupEconRepresentante() {
		return grupEconRepresentante;
	}

	/**
	 * Set: grupEconRepresentante.
	 *
	 * @param grupEconRepresentante the grup econ representante
	 */
	public void setGrupEconRepresentante(String grupEconRepresentante) {
		this.grupEconRepresentante = grupEconRepresentante;
	}

	/**
	 * Get: motivoContrato.
	 *
	 * @return motivoContrato
	 */
	public String getMotivoContrato() {
		return motivoContrato;
	}

	/**
	 * Set: motivoContrato.
	 *
	 * @param motivoContrato the motivo contrato
	 */
	public void setMotivoContrato(String motivoContrato) {
		this.motivoContrato = motivoContrato;
	}

	/**
	 * Get: nomeRazaoRepresentante.
	 *
	 * @return nomeRazaoRepresentante
	 */
	public String getNomeRazaoRepresentante() {
		return nomeRazaoRepresentante;
	}

	/**
	 * Set: nomeRazaoRepresentante.
	 *
	 * @param nomeRazaoRepresentante the nome razao representante
	 */
	public void setNomeRazaoRepresentante(String nomeRazaoRepresentante) {
		this.nomeRazaoRepresentante = nomeRazaoRepresentante;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: participacaoContrato.
	 *
	 * @return participacaoContrato
	 */
	public String getParticipacaoContrato() {
		return participacaoContrato;
	}

	/**
	 * Set: participacaoContrato.
	 *
	 * @param participacaoContrato the participacao contrato
	 */
	public void setParticipacaoContrato(String participacaoContrato) {
		this.participacaoContrato = participacaoContrato;
	}

	/**
	 * Get: segRepresentante.
	 *
	 * @return segRepresentante
	 */
	public String getSegRepresentante() {
		return segRepresentante;
	}

	/**
	 * Set: segRepresentante.
	 *
	 * @param segRepresentante the seg representante
	 */
	public void setSegRepresentante(String segRepresentante) {
		this.segRepresentante = segRepresentante;
	}

	/**
	 * Get: situacaoContrato.
	 *
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 *
	 * @param situacaoContrato the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Get: subSegRepresentante.
	 *
	 * @return subSegRepresentante
	 */
	public String getSubSegRepresentante() {
		return subSegRepresentante;
	}

	/**
	 * Set: subSegRepresentante.
	 *
	 * @param subSegRepresentante the sub seg representante
	 */
	public void setSubSegRepresentante(String subSegRepresentante) {
		this.subSegRepresentante = subSegRepresentante;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: cdPeriodicidade.
	 *
	 * @return cdPeriodicidade
	 */
	public int getCdPeriodicidade() {
		return cdPeriodicidade;
	}

	/**
	 * Set: cdPeriodicidade.
	 *
	 * @param cdPeriodicidade the cd periodicidade
	 */
	public void setCdPeriodicidade(int cdPeriodicidade) {
		this.cdPeriodicidade = cdPeriodicidade;
	}

	/**
	 * Get: listaPeriodicidade.
	 *
	 * @return listaPeriodicidade
	 */
	public List<SelectItem> getListaPeriodicidade() {
		return listaPeriodicidade;
	}

	/**
	 * Set: listaPeriodicidade.
	 *
	 * @param listaPeriodicidade the lista periodicidade
	 */
	public void setListaPeriodicidade(List<SelectItem> listaPeriodicidade) {
		this.listaPeriodicidade = listaPeriodicidade;
	}

	/**
	 * Get: listaPeriodicidadeHash.
	 *
	 * @return listaPeriodicidadeHash
	 */
	Map<Integer, String> getListaPeriodicidadeHash() {
		return listaPeriodicidadeHash;
	}

	/**
	 * Set lista periodicidade hash.
	 *
	 * @param listaPeriodicidadeHash the lista periodicidade hash
	 */
	void setListaPeriodicidadeHash(Map<Integer, String> listaPeriodicidadeHash) {
		this.listaPeriodicidadeHash = listaPeriodicidadeHash;
	}

	/**
	 * Get: produtoServicoCd.
	 *
	 * @return produtoServicoCd
	 */
	public int getProdutoServicoCd() {
		return produtoServicoCd;
	}

	/**
	 * Set: produtoServicoCd.
	 *
	 * @param produtoServicoCd the produto servico cd
	 */
	public void setProdutoServicoCd(int produtoServicoCd) {
		this.produtoServicoCd = produtoServicoCd;
	}

	/**
	 * Get: cdTipoReajuste.
	 *
	 * @return cdTipoReajuste
	 */
	public int getCdTipoReajuste() {
		return cdTipoReajuste;
	}

	/**
	 * Set: cdTipoReajuste.
	 *
	 * @param cdTipoReajuste the cd tipo reajuste
	 */
	public void setCdTipoReajuste(int cdTipoReajuste) {
		this.cdTipoReajuste = cdTipoReajuste;
	}

	/**
	 * Get: cdIndiceEconomico.
	 *
	 * @return cdIndiceEconomico
	 */
	public int getCdIndiceEconomico() {
		return cdIndiceEconomico;
	}

	/**
	 * Get: dsPeriodoVigencia.
	 *
	 * @return dsPeriodoVigencia
	 */
	public String getDsPeriodoVigencia() {
		return dsPeriodoVigencia;
	}

	/**
	 * Set: dsPeriodoVigencia.
	 *
	 * @param dsPeriodoVigencia the ds periodo vigencia
	 */
	public void setDsPeriodoVigencia(String dsPeriodoVigencia) {
		this.dsPeriodoVigencia = dsPeriodoVigencia;
	}

	/**
	 * Get: dsValorContratado.
	 *
	 * @return dsValorContratado
	 */
	public String getDsValorContratado() {
		return dsValorContratado;
	}

	/**
	 * Set: dsValorContratado.
	 *
	 * @param dsValorContratado the ds valor contratado
	 */
	public void setDsValorContratado(String dsValorContratado) {
		this.dsValorContratado = dsValorContratado;
	}

	/**
	 * Get: dsValorReferenciaMin.
	 *
	 * @return dsValorReferenciaMin
	 */
	public String getDsValorReferenciaMin() {
		return dsValorReferenciaMin;
	}

	/**
	 * Set: dsValorReferenciaMin.
	 *
	 * @param dsValorReferencia the ds valor referencia min
	 */
	public void setDsValorReferenciaMin(String dsValorReferencia) {
		this.dsValorReferenciaMin = dsValorReferencia;
	}

	/**
	 * Set: cdIndiceEconomico.
	 *
	 * @param cdIndiceEconomico the cd indice economico
	 */
	public void setCdIndiceEconomico(int cdIndiceEconomico) {
		this.cdIndiceEconomico = cdIndiceEconomico;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: listaIndiceEconomicoHash.
	 *
	 * @return listaIndiceEconomicoHash
	 */
	Map<Integer, String> getListaIndiceEconomicoHash() {
		return listaIndiceEconomicoHash;
	}

	/**
	 * Set lista indice economico hash.
	 *
	 * @param listaIndiceEconomicoHash the lista indice economico hash
	 */
	void setListaIndiceEconomicoHash(Map<Integer, String> listaIndiceEconomicoHash) {
		this.listaIndiceEconomicoHash = listaIndiceEconomicoHash;
	}

	/**
	 * Get: periodoValidade.
	 *
	 * @return periodoValidade
	 */
	public String getPeriodoValidade() {
		return periodoValidade;
	}

	/**
	 * Set: periodoValidade.
	 *
	 * @param periodoValidade the periodo validade
	 */
	public void setPeriodoValidade(String periodoValidade) {
		this.periodoValidade = periodoValidade;
	}

	/**
	 * Get: cdTipoContrato.
	 *
	 * @return cdTipoContrato
	 */
	public Integer getCdTipoContrato() {
		return cdTipoContrato;
	}

	/**
	 * Set: cdTipoContrato.
	 *
	 * @param cdTipoContrato the cd tipo contrato
	 */
	public void setCdTipoContrato(Integer cdTipoContrato) {
		this.cdTipoContrato = cdTipoContrato;
	}

	/**
	 * Get: codPessoaJuridica.
	 *
	 * @return codPessoaJuridica
	 */
	public Long getCodPessoaJuridica() {
		return codPessoaJuridica;
	}

	/**
	 * Set: codPessoaJuridica.
	 *
	 * @param codPessoaJuridica the cod pessoa juridica
	 */
	public void setCodPessoaJuridica(Long codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}

	/**
	 * Get: nrSequenciaContrato.
	 *
	 * @return nrSequenciaContrato
	 */
	public Long getNrSequenciaContrato() {
		return nrSequenciaContrato;
	}

	/**
	 * Set: nrSequenciaContrato.
	 *
	 * @param nrSequenciaContrato the nr sequencia contrato
	 */
	public void setNrSequenciaContrato(Long nrSequenciaContrato) {
		this.nrSequenciaContrato = nrSequenciaContrato;
	}

	/**
	 * Get: itemSelecionadoLista4.
	 *
	 * @return itemSelecionadoLista4
	 */
	public Integer getItemSelecionadoLista4() {
		return itemSelecionadoLista4;
	}

	/**
	 * Set: itemSelecionadoLista4.
	 *
	 * @param itemSelecionadoLista4 the item selecionado lista4
	 */
	public void setItemSelecionadoLista4(Integer itemSelecionadoLista4) {
		this.itemSelecionadoLista4 = itemSelecionadoLista4;
	}

	/**
	 * Get: listaControleRadio4.
	 *
	 * @return listaControleRadio4
	 */
	public List<SelectItem> getListaControleRadio4() {
		return listaControleRadio4;
	}

	/**
	 * Set: listaControleRadio4.
	 *
	 * @param listaControleRadio4 the lista controle radio4
	 */
	public void setListaControleRadio4(List<SelectItem> listaControleRadio4) {
		this.listaControleRadio4 = listaControleRadio4;
	}

	/**
	 * Get: listaGridPesquisa4.
	 *
	 * @return listaGridPesquisa4
	 */
	public List<ListarVincMsgLancOperSaidaDTO> getListaGridPesquisa4() {
		return listaGridPesquisa4;
	}

	/**
	 * Set: listaGridPesquisa4.
	 *
	 * @param listaGridPesquisa4 the lista grid pesquisa4
	 */
	public void setListaGridPesquisa4(List<ListarVincMsgLancOperSaidaDTO> listaGridPesquisa4) {
		this.listaGridPesquisa4 = listaGridPesquisa4;
	}

	/**
	 * Get: codModalidade.
	 *
	 * @return codModalidade
	 */
	public int getCodModalidade() {
		return codModalidade;
	}

	/**
	 * Set: codModalidade.
	 *
	 * @param codModalidade the cod modalidade
	 */
	public void setCodModalidade(int codModalidade) {
		this.codModalidade = codModalidade;
	}

	/**
	 * Get: codTipoServico.
	 *
	 * @return codTipoServico
	 */
	public int getCodTipoServico() {
		return codTipoServico;
	}

	/**
	 * Get: listaControleTarifas.
	 *
	 * @return listaControleTarifas
	 */
	public List<SelectItem> getListaControleTarifas() {
		return listaControleTarifas;
	}

	/**
	 * Set: listaControleTarifas.
	 *
	 * @param listaControleTarifas the lista controle tarifas
	 */
	public void setListaControleTarifas(List<SelectItem> listaControleTarifas) {
		this.listaControleTarifas = listaControleTarifas;
	}

	/**
	 * Set: codTipoServico.
	 *
	 * @param codTipoServico the cod tipo servico
	 */
	public void setCodTipoServico(int codTipoServico) {
		this.codTipoServico = codTipoServico;
	}

	/**
	 * Get: cdPercentualFlexibilizacao.
	 *
	 * @return cdPercentualFlexibilizacao
	 */
	public BigDecimal getCdPercentualFlexibilizacao() {
		return cdPercentualFlexibilizacao;
	}

	/**
	 * Set: cdPercentualFlexibilizacao.
	 *
	 * @param cdPercentualFlexibilizacao the cd percentual flexibilizacao
	 */
	public void setCdPercentualFlexibilizacao(BigDecimal cdPercentualFlexibilizacao) {
		this.cdPercentualFlexibilizacao = cdPercentualFlexibilizacao;
	}

	/**
	 * Get: cdPercentualIndice.
	 *
	 * @return cdPercentualIndice
	 */
	public BigDecimal getCdPercentualIndice() {
		return cdPercentualIndice;
	}

	/**
	 * Set: cdPercentualIndice.
	 *
	 * @param cdPercentualIndice the cd percentual indice
	 */
	public void setCdPercentualIndice(BigDecimal cdPercentualIndice) {
		this.cdPercentualIndice = cdPercentualIndice;
	}

	/**
	 * Get: percentualFlexibilizacaoFiltro.
	 *
	 * @return percentualFlexibilizacaoFiltro
	 */
	public BigDecimal getPercentualFlexibilizacaoFiltro() {
		return percentualFlexibilizacaoFiltro;
	}

	/**
	 * Set: percentualFlexibilizacaoFiltro.
	 *
	 * @param percentualFlexibilizacaoFiltro the percentual flexibilizacao filtro
	 */
	public void setPercentualFlexibilizacaoFiltro(BigDecimal percentualFlexibilizacaoFiltro) {
		this.percentualFlexibilizacaoFiltro = percentualFlexibilizacaoFiltro;
	}

	/**
	 * Get: percentualIndiceFiltro.
	 *
	 * @return percentualIndiceFiltro
	 */
	public BigDecimal getPercentualIndiceFiltro() {
		return percentualIndiceFiltro;
	}

	/**
	 * Set: percentualIndiceFiltro.
	 *
	 * @param percentualIndiceFiltro the percentual indice filtro
	 */
	public void setPercentualIndiceFiltro(BigDecimal percentualIndiceFiltro) {
		this.percentualIndiceFiltro = percentualIndiceFiltro;
	}

	/**
	 * Get: cdRelacionamentoProduto.
	 *
	 * @return cdRelacionamentoProduto
	 */
	public int getCdRelacionamentoProduto() {
		return cdRelacionamentoProduto;
	}

	/**
	 * Set: cdRelacionamentoProduto.
	 *
	 * @param cdRelacionamentoProduto the cd relacionamento produto
	 */
	public void setCdRelacionamentoProduto(int cdRelacionamentoProduto) {
		this.cdRelacionamentoProduto = cdRelacionamentoProduto;
	}

	/**
	 * Checks if is chk qtd dia cobranca.
	 *
	 * @return true, if is chk qtd dia cobranca
	 */
	public boolean isChkQtdDiaCobranca() {
		return chkQtdDiaCobranca;
	}

	/**
	 * Sets the chk qtd dia cobranca.
	 *
	 * @param chkQtdDiaCobranca the new chk qtd dia cobranca
	 */
	public void setChkQtdDiaCobranca(boolean chkQtdDiaCobranca) {
		this.chkQtdDiaCobranca = chkQtdDiaCobranca;
	}

	public String getDsDiaFechamento() {
		return dsDiaFechamento;
	}

	public void setDsDiaFechamento(String dsDiaFechamento) {
		this.dsDiaFechamento = dsDiaFechamento;
	}

	public String getDsQuantDia() {
		return dsQuantDia;
	}

	public void setDsQuantDia(String dsQuantDia) {
		this.dsQuantDia = dsQuantDia;
	}

}