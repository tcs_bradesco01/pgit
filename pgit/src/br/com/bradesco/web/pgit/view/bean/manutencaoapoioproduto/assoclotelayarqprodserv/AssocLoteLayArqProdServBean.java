/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assoclotelayarqprodserv
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.assoclotelayarqprodserv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.log.ILogManager;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.IAssocLoteLayArqProdServService;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.DetalharAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.DetalharAsLotLaArProdServSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ExcluirAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ExcluirAsLotLaArProdServSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.IncluirAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.IncluirAsLotLaArProdServSaidaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ListarAsLotLaArProdServEntradaDTO;
import br.com.bradesco.web.pgit.service.business.assoclotelayarqprodserv.bean.ListarAsLotLaArProdServSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarServicosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLayoutArquivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLoteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoLoteSaidaDTO;

/**
 * Nome: AssocLoteLayArqProdServBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class AssocLoteLayArqProdServBean {
	
	/** Atributo CON_ASSOCIACAO_LOTE_LAYOUT_ARQUIVO_PRODUTO_SERVICO. */
	private static final String CON_ASSOCIACAO_LOTE_LAYOUT_ARQUIVO_PRODUTO_SERVICO = "conAssociacaoLoteLayoutArquivoProdutoServico";
	
	/** Atributo assocLoteOperacaoProdServImpl. */
	private IAssocLoteLayArqProdServService assocLoteOperacaoProdServImpl;
	
	/** Atributo filtroServicos. */
	private Integer filtroServicos;
	
	/** Atributo filtroTipoLote. */
	private Integer filtroTipoLote;
	
	/** Atributo filtroTipoLayoutArquivo. */
	private Integer filtroTipoLayoutArquivo;
	
	/** Atributo servico. */
	private String servico;
	
	/** Atributo tipoLote. */
	private String tipoLote;  
	
	/** Atributo tipoLayoutArquivo. */
	private String tipoLayoutArquivo;
	
	/** Atributo cdServico. */
	private Integer cdServico;
	
	/** Atributo cdTipoLote. */
	private Integer cdTipoLote;
	
	/** Atributo cdTipoLayoutArquivo. */
	private Integer cdTipoLayoutArquivo;
	
	/** Atributo codServicos. */
	private Integer codServicos;
	
	/** Atributo codTipoLote. */
	private Integer codTipoLote;
	
	/** Atributo codTipoLayoutArquivo. */
	private Integer codTipoLayoutArquivo;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo listaControle. */
	private List<SelectItem> listaControle = new ArrayList<SelectItem>();
	
	/** Atributo listaGrid. */
	private List<ListarAsLotLaArProdServSaidaDTO> listaGrid;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	
	/** Atributo logger. */
	private ILogManager logger;
	
	/** Atributo listaTipoServico. */
	private List<SelectItem> listaTipoServico = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLayoutArquivo. */
	private List<SelectItem> listaTipoLayoutArquivo = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoLote. */
	private List<SelectItem> listaTipoLote = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer,String> listaTipoServicoHash = new HashMap<Integer,String>();
	
	/** Atributo listaTipoLayoutArquivoHash. */
	private Map<Integer,String> listaTipoLayoutArquivoHash = new HashMap<Integer,String>();
	
	/** Atributo listaTipoLoteHash. */
	private Map<Integer,String> listaTipoLoteHash = new HashMap<Integer,String>();
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo saidaTipoLayoutArquivoSaidaDTO. */
	private TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO;
	
	
	
	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}

	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Get: cdTipoLayoutArquivo.
	 *
	 * @return cdTipoLayoutArquivo
	 */
	public Integer getCdTipoLayoutArquivo() {
		return cdTipoLayoutArquivo;
	}

	/**
	 * Set: cdTipoLayoutArquivo.
	 *
	 * @param cdTipoLayoutArquivo the cd tipo layout arquivo
	 */
	public void setCdTipoLayoutArquivo(Integer cdTipoLayoutArquivo) {
		this.cdTipoLayoutArquivo = cdTipoLayoutArquivo;
	}

	/**
	 * Get: cdTipoLote.
	 *
	 * @return cdTipoLote
	 */
	public Integer getCdTipoLote() {
		return cdTipoLote;
	}

	/**
	 * Set: cdTipoLote.
	 *
	 * @param cdTipoLote the cd tipo lote
	 */
	public void setCdTipoLote(Integer cdTipoLote) {
		this.cdTipoLote = cdTipoLote;
	}

	/**
	 * Get: listaTipoLayoutArquivoHash.
	 *
	 * @return listaTipoLayoutArquivoHash
	 */
	Map<Integer, String> getListaTipoLayoutArquivoHash() {
		return listaTipoLayoutArquivoHash;
	}

	/**
	 * Set lista tipo layout arquivo hash.
	 *
	 * @param listaTipoLayoutArquivoHash the lista tipo layout arquivo hash
	 */
	void setListaTipoLayoutArquivoHash(
			Map<Integer, String> listaTipoLayoutArquivoHash) {
		this.listaTipoLayoutArquivoHash = listaTipoLayoutArquivoHash;
	}

	/**
	 * Get: listaTipoLoteHash.
	 *
	 * @return listaTipoLoteHash
	 */
	Map<Integer, String> getListaTipoLoteHash() {
		return listaTipoLoteHash;
	}

	/**
	 * Set lista tipo lote hash.
	 *
	 * @param listaTipoLoteHash the lista tipo lote hash
	 */
	void setListaTipoLoteHash(Map<Integer, String> listaTipoLoteHash) {
		this.listaTipoLoteHash = listaTipoLoteHash;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Get: logger.
	 *
	 * @return logger
	 */
	public ILogManager getLogger() {
		return logger;
	}

	/**
	 * Set: logger.
	 *
	 * @param logger the logger
	 */
	public void setLogger(ILogManager logger) {
		this.logger = logger;
	}
	
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		setFiltroServicos(0);
		setFiltroTipoLayoutArquivo(0);
		setFiltroTipoLote(0);
		setListaGrid(null);
		setItemSelecionadoLista(null);
		preencherListaTipoServico();
		preencherListaTipoLayoutArquivo();
		preencherListaTipoLote();
		setBtoAcionado(false);
		
	}
	
	
	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: codServicos.
	 *
	 * @return codServicos
	 */
	public Integer getCodServicos() {
		return codServicos;
	}
	
	/**
	 * Set: codServicos.
	 *
	 * @param codServicos the cod servicos
	 */
	public void setCodServicos(Integer codServicos) {
		this.codServicos = codServicos;
	}
	
	/**
	 * Get: codTipoLayoutArquivo.
	 *
	 * @return codTipoLayoutArquivo
	 */
	public Integer getCodTipoLayoutArquivo() {
		return codTipoLayoutArquivo;
	}
	
	/**
	 * Set: codTipoLayoutArquivo.
	 *
	 * @param codTipoLayoutArquivo the cod tipo layout arquivo
	 */
	public void setCodTipoLayoutArquivo(
			Integer codTipoLayoutArquivo) {
		this.codTipoLayoutArquivo = codTipoLayoutArquivo;
	}
	
	/**
	 * Get: codTipoLote.
	 *
	 * @return codTipoLote
	 */
	public Integer getCodTipoLote() {
		return codTipoLote;
	}
	
	/**
	 * Set: codTipoLote.
	 *
	 * @param codTipoLote the cod tipo lote
	 */
	public void setCodTipoLote(Integer codTipoLote) {
		this.codTipoLote = codTipoLote;
	}
	
	/**
	 * Get: filtroServicos.
	 *
	 * @return filtroServicos
	 */
	public Integer getFiltroServicos() {
		return filtroServicos;
	}
	
	/**
	 * Set: filtroServicos.
	 *
	 * @param filtroServicos the filtro servicos
	 */
	public void setFiltroServicos(Integer filtroServicos) {
		this.filtroServicos = filtroServicos;
	}
	
	/**
	 * Get: filtroTipoLayoutArquivo.
	 *
	 * @return filtroTipoLayoutArquivo
	 */
	public Integer getFiltroTipoLayoutArquivo() {
		return filtroTipoLayoutArquivo;
	}
	
	/**
	 * Set: filtroTipoLayoutArquivo.
	 *
	 * @param filtroTipoLayoutArquivo the filtro tipo layout arquivo
	 */
	public void setFiltroTipoLayoutArquivo(Integer filtroTipoLayoutArquivo) {
		this.filtroTipoLayoutArquivo = filtroTipoLayoutArquivo;
	}
	
	/**
	 * Get: filtroTipoLote.
	 *
	 * @return filtroTipoLote
	 */
	public Integer getFiltroTipoLote() {
		return filtroTipoLote;
	}
	
	/**
	 * Set: filtroTipoLote.
	 *
	 * @param filtroTipoLote the filtro tipo lote
	 */
	public void setFiltroTipoLote(Integer filtroTipoLote) {
		this.filtroTipoLote = filtroTipoLote;
	}
	
	/**
	 * Get: servico.
	 *
	 * @return servico
	 */
	public String getServico() {
		return servico;
	}
	
	/**
	 * Set: servico.
	 *
	 * @param servico the servico
	 */
	public void setServico(String servico) {
		this.servico = servico;
	}
	
	/**
	 * Get: tipoLayoutArquivo.
	 *
	 * @return tipoLayoutArquivo
	 */
	public String getTipoLayoutArquivo() {
		return tipoLayoutArquivo;
	}
	
	/**
	 * Set: tipoLayoutArquivo.
	 *
	 * @param tipoLayoutArquivo the tipo layout arquivo
	 */
	public void setTipoLayoutArquivo(String tipoLayoutArquivo) {
		this.tipoLayoutArquivo = tipoLayoutArquivo;
	}
	
	/**
	 * Get: tipoLote.
	 *
	 * @return tipoLote
	 */
	public String getTipoLote() {
		return tipoLote;
	}
	
	/**
	 * Set: tipoLote.
	 *
	 * @param tipoLote the tipo lote
	 */
	public void setTipoLote(String tipoLote) {
		this.tipoLote = tipoLote;
	}
	
	
	
	/**
	 * Avancar detalhar.
	 *
	 * @return the string
	 */
	public String avancarDetalhar(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_ASSOCIACAO_LOTE_LAYOUT_ARQUIVO_PRODUTO_SERVICO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "AVANCAR_DETALHAR";
	}
	
	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir(){
		setCodServicos(0);
		setCodTipoLote(0);
		setCodTipoLayoutArquivo(0);
		return "AVANCAR_INCLUIR";
	}
	
	/**
	 * Avancar excluir.
	 *
	 * @return the string
	 */
	public String avancarExcluir(){
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), CON_ASSOCIACAO_LOTE_LAYOUT_ARQUIVO_PRODUTO_SERVICO, BradescoViewExceptionActionType.ACTION, false);
			return null;
		} 
		return "AVANCAR_EXCLUIR";
	}
	
	/**
	 * Limpar campos.
	 */
	public void limparCampos(){
		this.setFiltroServicos(0);
		this.setFiltroTipoLayoutArquivo(0);
		this.setFiltroTipoLote(0);
		setItemSelecionadoLista(null);
		setListaGrid(null);
		setBtoAcionado(false);
	}
	
	/**
	 * Voltar pesquisar.
	 *
	 * @return the string
	 */
	public String voltarPesquisar(){
		setItemSelecionadoLista(null);
		return "VOLTAR_PESQUISAR";
	}
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try {
			ExcluirAsLotLaArProdServEntradaDTO excluirAsLotLaArProdServEntradaDTO = new ExcluirAsLotLaArProdServEntradaDTO();
			ListarAsLotLaArProdServSaidaDTO listarAsLotLaArProdServSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
			
			excluirAsLotLaArProdServEntradaDTO.setTipoLote(listarAsLotLaArProdServSaidaDTO.getCdTipoLote());
			excluirAsLotLaArProdServEntradaDTO.setTipoLayoutArquivo(listarAsLotLaArProdServSaidaDTO.getCdTipoLayoutArquivo());
			excluirAsLotLaArProdServEntradaDTO.setTipoServico(listarAsLotLaArProdServSaidaDTO.getCdTipoServico());
			
			ExcluirAsLotLaArProdServSaidaDTO excluirAsLotLaArProdServSaidaDTO = getAssocLoteOperacaoProdServImpl().excluirAssocLoteLayArProdServ(excluirAsLotLaArProdServEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + excluirAsLotLaArProdServSaidaDTO.getCodMensagem() + ") " + excluirAsLotLaArProdServSaidaDTO.getMensagem(), CON_ASSOCIACAO_LOTE_LAYOUT_ARQUIVO_PRODUTO_SERVICO, BradescoViewExceptionActionType.ACTION, false);

			carregaLista();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Avancar confirmar incluir.
	 *
	 * @return the string
	 */
	public String avancarConfirmarIncluir(){
		setServico((String)listaTipoServicoHash.get(getCodServicos()));
		setTipoLote((String)listaTipoLoteHash.get(getCodTipoLote()));
		setTipoLayoutArquivo((String)listaTipoLayoutArquivoHash.get(getCodTipoLayoutArquivo()));
		
		return "AVANCAR_CONFIRMAR_INCLUIR";
	}
	
	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir(){
		
		try {
			
			IncluirAsLotLaArProdServEntradaDTO incluirAsLotLaArProdServEntradaDTO = new IncluirAsLotLaArProdServEntradaDTO();
			
			incluirAsLotLaArProdServEntradaDTO.setTipoLote(getCodTipoLote());
			incluirAsLotLaArProdServEntradaDTO.setTipoLayoutArquivo(getCodTipoLayoutArquivo());
			incluirAsLotLaArProdServEntradaDTO.setTipoServico(getCodServicos());
			
			IncluirAsLotLaArProdServSaidaDTO incluirAssAsLotLaArProdServSaidaDTO = getAssocLoteOperacaoProdServImpl().incluirAssocLoteLayArProdServ(incluirAsLotLaArProdServEntradaDTO);	
			BradescoFacesUtils.addInfoModalMessage("(" +incluirAssAsLotLaArProdServSaidaDTO.getCodMensagem() + ") " + incluirAssAsLotLaArProdServSaidaDTO.getMensagem(), CON_ASSOCIACAO_LOTE_LAYOUT_ARQUIVO_PRODUTO_SERVICO, BradescoViewExceptionActionType.ACTION, false);
			carregaLista();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				return null;
			}
			return "";
		
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		return "VOLTAR_INCLUIR";
	}
	
	/**
	 * Get: assocLoteOperacaoProdServImpl.
	 *
	 * @return assocLoteOperacaoProdServImpl
	 */
	public IAssocLoteLayArqProdServService getAssocLoteOperacaoProdServImpl() {
		return assocLoteOperacaoProdServImpl;
	}
	
	/**
	 * Set: assocLoteOperacaoProdServImpl.
	 *
	 * @param assocLoteOperacaoProdServImpl the assoc lote operacao prod serv impl
	 */
	public void setAssocLoteOperacaoProdServImpl(
			IAssocLoteLayArqProdServService assocLoteOperacaoProdServImpl) {
		this.assocLoteOperacaoProdServImpl = assocLoteOperacaoProdServImpl;
	}
	
	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}
	
	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}
	
	/**
	 * Get: listaGrid.
	 *
	 * @return listaGrid
	 */
	public List<ListarAsLotLaArProdServSaidaDTO> getListaGrid() {
		return listaGrid;
	}
	
	/**
	 * Set: listaGrid.
	 *
	 * @param listaGrid the lista grid
	 */
	public void setListaGrid(List<ListarAsLotLaArProdServSaidaDTO> listaGrid) {
		this.listaGrid = listaGrid;
	}

	
	
	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}
	
	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}
	
	/**
	 * Carrega lista.
	 */
	public void carregaLista() {
		
		
		try{
			
			ListarAsLotLaArProdServEntradaDTO entradaDTO = new ListarAsLotLaArProdServEntradaDTO();
			
			entradaDTO.setTipoLote(getFiltroTipoLote()!=null?getFiltroTipoLote():0);
			entradaDTO.setTipoLayoutArquivo(getFiltroTipoLayoutArquivo()!=null?getFiltroTipoLayoutArquivo():0);
			entradaDTO.setTipoServico(getFiltroServicos()!=null?getFiltroServicos():0);
			
			
			setListaGrid(getAssocLoteOperacaoProdServImpl().listarAssocLoteLayArProdServ(entradaDTO));
			setBtoAcionado(true);
			
			for (int i = 0; i <= this.getListaGrid().size(); i++) {
				listaControle.add(new SelectItem(i, " "));
			}

		   
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGrid(null);

		}
		setItemSelecionadoLista(null);
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: listaTipoServico.
	 *
	 * @return listaTipoServico
	 */
	public List<SelectItem> getListaTipoServico() {		
		return listaTipoServico;
	}
	
	/**
	 * Preencher lista tipo servico.
	 */
	public void preencherListaTipoServico(){
		
		try{	
			this.listaTipoServico = new ArrayList<SelectItem>();
			
			List<ListarServicosSaidaDTO> listaServico = new ArrayList<ListarServicosSaidaDTO>();
			
			listaServico = comboService.listarTipoServicos(0);
			
			listaTipoServicoHash.clear();
			for(ListarServicosSaidaDTO combo : listaServico){
				listaTipoServicoHash.put(combo.getCdServico(), combo.getDsServico());
				listaTipoServico.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoServico = new ArrayList<SelectItem>();
		}
		
	}

	/**
	 * Set: listaTipoServico.
	 *
	 * @param listaTipoServico the lista tipo servico
	 */
	public void setListaTipoServico(List<SelectItem> listaTipoServico) {
		this.listaTipoServico = listaTipoServico;
	}

	/**
	 * Get: listaTipoLayoutArquivo.
	 *
	 * @return listaTipoLayoutArquivo
	 */
	public List<SelectItem> getListaTipoLayoutArquivo() {
		return listaTipoLayoutArquivo;
	}
	
	/**
	 * Preencher lista tipo layout arquivo.
	 */
	public void preencherListaTipoLayoutArquivo(){
		
		try{	
			this.listaTipoLayoutArquivo = new ArrayList<SelectItem>();
			
			List<TipoLayoutArquivoSaidaDTO> listaTipoLayoutArquivoSaida = new ArrayList<TipoLayoutArquivoSaidaDTO>();
			TipoLayoutArquivoEntradaDTO tipoLayoutArquivoEntrada = new TipoLayoutArquivoEntradaDTO();
			tipoLayoutArquivoEntrada.setCdSituacaoVinculacaoConta(0);
			
			saidaTipoLayoutArquivoSaidaDTO = comboService.listarTipoLayoutArquivo(tipoLayoutArquivoEntrada);
			listaTipoLayoutArquivoSaida = saidaTipoLayoutArquivoSaidaDTO.getOcorrencias();
			
			listaTipoLayoutArquivoHash.clear();
			for(TipoLayoutArquivoSaidaDTO combo : listaTipoLayoutArquivoSaida){
				listaTipoLayoutArquivoHash.put(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo());
				listaTipoLayoutArquivo.add(new SelectItem(combo.getCdTipoLayoutArquivo(), combo.getDsTipoLayoutArquivo()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoLayoutArquivo = new ArrayList<SelectItem>();
		}
		
	}

	/**
	 * Set: listaTipoLayoutArquivo.
	 *
	 * @param listaTipoLayoutArquivo the lista tipo layout arquivo
	 */
	public void setListaTipoLayoutArquivo(List<SelectItem> listaTipoLayoutArquivo) {
		this.listaTipoLayoutArquivo = listaTipoLayoutArquivo;
	}

	/**
	 * Get: listaTipoLote.
	 *
	 * @return listaTipoLote
	 */
	public List<SelectItem> getListaTipoLote() {
		return listaTipoLote;
	}
	
	/**
	 * Preencher lista tipo lote.
	 */
	public void preencherListaTipoLote(){
		
		try{
			this.listaTipoLote = new ArrayList<SelectItem>();
			
			List<TipoLoteSaidaDTO> listaTipoLoteSaida = new ArrayList<TipoLoteSaidaDTO>();
			TipoLoteEntradaDTO tipoLoteEntrada = new TipoLoteEntradaDTO();
			tipoLoteEntrada.setCdSituacaoVinculacaoConta(0);
			
			listaTipoLoteSaida = comboService.listarTipoLote(tipoLoteEntrada);
			
			listaTipoLoteHash.clear();
			for(TipoLoteSaidaDTO combo : listaTipoLoteSaida){
				listaTipoLoteHash.put(combo.getCdTipoLoteLayout(), combo.getDsTipoLoteLayout());
				listaTipoLote.add(new SelectItem(combo.getCdTipoLoteLayout(), combo.getDsTipoLoteLayout()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoLote = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Set: listaTipoLote.
	 *
	 * @param listaTipoLote the lista tipo lote
	 */
	public void setListaTipoLote(List<SelectItem> listaTipoLote) {
		this.listaTipoLote = listaTipoLote;
	}
	
	/**
	 * Preenche dados.
	 */
	public void preencheDados(){
		ListarAsLotLaArProdServSaidaDTO listarAsLotLaArProdServSaidaDTO = getListaGrid().get(getItemSelecionadoLista());
		DetalharAsLotLaArProdServEntradaDTO detalharAsLotLaArProdServEntradaDTO = new DetalharAsLotLaArProdServEntradaDTO();
		
		detalharAsLotLaArProdServEntradaDTO.setTipoLayoutArquivo(listarAsLotLaArProdServSaidaDTO.getCdTipoLayoutArquivo());
		detalharAsLotLaArProdServEntradaDTO.setTipoLote(listarAsLotLaArProdServSaidaDTO.getCdTipoLote());
		detalharAsLotLaArProdServEntradaDTO.setTipoServico(listarAsLotLaArProdServSaidaDTO.getCdTipoServico());
		
		DetalharAsLotLaArProdServSaidaDTO detalharAsLotLaArProdServSaidaDTO = getAssocLoteOperacaoProdServImpl().detalharAssocLoteLayArProdServ(detalharAsLotLaArProdServEntradaDTO);
		setCdServico(detalharAsLotLaArProdServSaidaDTO.getCdTipoServico());
		setServico(detalharAsLotLaArProdServSaidaDTO.getDsTipoServico());
		setCdTipoLayoutArquivo(detalharAsLotLaArProdServSaidaDTO.getCdLayoutArquivo());
		setTipoLayoutArquivo(detalharAsLotLaArProdServSaidaDTO.getDsLayoutArquivo());
		setCdTipoLote(detalharAsLotLaArProdServSaidaDTO.getCdTipoLote());
		setTipoLote(detalharAsLotLaArProdServSaidaDTO.getDsTipoLote());
		setUsuarioInclusao(detalharAsLotLaArProdServSaidaDTO.getUsuarioInclusao());
		setUsuarioManutencao(detalharAsLotLaArProdServSaidaDTO.getUsuarioManutencao());
		setComplementoInclusao(detalharAsLotLaArProdServSaidaDTO.getComplementoInclusao().equals("0")? "" : detalharAsLotLaArProdServSaidaDTO.getComplementoInclusao());
		setComplementoManutencao(detalharAsLotLaArProdServSaidaDTO.getComplementoManutencao().equals("0")? "" : detalharAsLotLaArProdServSaidaDTO.getComplementoManutencao());
		setTipoCanalInclusao(detalharAsLotLaArProdServSaidaDTO.getCdCanalInclusao()==0? "" : detalharAsLotLaArProdServSaidaDTO.getCdCanalInclusao() + " - " + detalharAsLotLaArProdServSaidaDTO.getDsCanalInclusao());
		setTipoCanalManutencao(detalharAsLotLaArProdServSaidaDTO.getCdCanalManutencao()==0? "" : detalharAsLotLaArProdServSaidaDTO.getCdCanalManutencao() + " - " + detalharAsLotLaArProdServSaidaDTO.getDsCanalManutencao());
		
		setDataHoraInclusao(detalharAsLotLaArProdServSaidaDTO.getDataHoraInclusao());
		setDataHoraManutencao(detalharAsLotLaArProdServSaidaDTO.getDataHoraManutencao());
		
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Nome: setSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @param saidaTipoLayoutArquivoSaidaDTO
	 */
	public void setSaidaTipoLayoutArquivoSaidaDTO(
			TipoLayoutArquivoSaidaDTO saidaTipoLayoutArquivoSaidaDTO) {
		this.saidaTipoLayoutArquivoSaidaDTO = saidaTipoLayoutArquivoSaidaDTO;
	}

	/**
	 * Nome: getSaidaTipoLayoutArquivoSaidaDTO
	 *
	 * @exception
	 * @throws
	 * @return saidaTipoLayoutArquivoSaidaDTO
	 */
	public TipoLayoutArquivoSaidaDTO getSaidaTipoLayoutArquivoSaidaDTO() {
		return saidaTipoLayoutArquivoSaidaDTO;
	}
	
}
