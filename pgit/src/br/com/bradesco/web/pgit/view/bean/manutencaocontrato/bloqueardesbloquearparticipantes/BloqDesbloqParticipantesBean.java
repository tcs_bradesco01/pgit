/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.bloqueardesbloquearparticipantes
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.bloqueardesbloquearparticipantes;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimir.IImprimirService;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.IImprimirAnexoPrimeiroContratoService;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.AnexoContratoDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.ImprimirAnexoPrimeiroContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.ImprimirAnexoPrimeiroContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasContas;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasLayout;
import br.com.bradesco.web.pgit.service.business.imprimiranexoprimeirocontrato.bean.OcorrenciasParticipantes;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.IImprimirAnexoSegundoContratoService;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ImprimirAnexoSegundoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ImprimirAnexoSegundoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasAvisoFormularios;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasAvisos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasComprovanteSalariosDiversos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasComprovantes;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasCreditos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasModularidades;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasOperacoes;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasServicoPagamentos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasTitulos;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.OcorrenciasTriTributo;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.PagamentosDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ServicosDTO;
import br.com.bradesco.web.pgit.service.business.imprimiranexosegundocontrato.bean.ServicosModalidadesDTO;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.IImprimircontratoService;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.bean.ConsultarDadosBasicoContratoSaidaDTO;
import br.com.bradesco.web.pgit.utils.NumberUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.ContasBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.DadosBasicosBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.LayoutsArquivosBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.ParticipantesBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.RepresentanteBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.ServicosBean;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato.TarifasBean;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: BloqDesbloqParticipantesBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class BloqDesbloqParticipantesBean {
	
//	 Atributos
	/** Atributo comboService. */
private IComboService comboService;

	/** Atributo manterContratoContasBean. */
	private ContasBean manterContratoContasBean;

	/** Atributo manterContratoRepresentanteBean. */
	private RepresentanteBean manterContratoRepresentanteBean;

	/** Atributo manterContratoParticipantesBean. */
	private ParticipantesBean manterContratoParticipantesBean;

	/** Atributo manterContratoServicosBean. */
	private ServicosBean manterContratoServicosBean;

	/** Atributo manterContratoDadosBasicosBean. */
	private DadosBasicosBean manterContratoDadosBasicosBean;

	/** Atributo manterContratoLayoutsArquivosBean. */
	private LayoutsArquivosBean manterContratoLayoutsArquivosBean;

	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

	/** Atributo manterContratoTarifasBean. */
	private TarifasBean manterContratoTarifasBean;

	/** Atributo imprimirContratoService. */
	private IImprimircontratoService imprimirContratoService;

	/** Atributo contratoPgitSelecionado. */
	private ListarContratosPgitSaidaDTO contratoPgitSelecionado = new ListarContratosPgitSaidaDTO();;

	/** Atributo imprimirService. */
	private IImprimirService imprimirService = null;

	/** Atributo imprimirAnexoPrimeiroContratoService. */
	private IImprimirAnexoPrimeiroContratoService imprimirAnexoPrimeiroContratoService;

	/** Atributo imprimirAnexoSegundoContratoService. */
	private IImprimirAnexoSegundoContratoService imprimirAnexoSegundoContratoService;

	// usadas nas proximas telas
	/** Atributo gerenteResponsavelFormatado. */
	private String gerenteResponsavelFormatado;

	/** Atributo cpfCnpjMaster. */
	private String cpfCnpjMaster;

	/** Atributo nomeRazaoSocialMaster. */
	private String nomeRazaoSocialMaster;

	/** Atributo grupoEconomicoMaster. */
	private String grupoEconomicoMaster;

	/** Atributo atividadeEconomicaMaster. */
	private String atividadeEconomicaMaster;

	/** Atributo segmentoMaster. */
	private String segmentoMaster;

	/** Atributo subSegmentoMaster. */
	private String subSegmentoMaster;

	/** Atributo empresa. */
	private String empresa;

	/** Atributo cdEmpresa. */
	private Long cdEmpresa;

	/** Atributo tipo. */
	private String tipo;

	/** Atributo cdTipo. */
	private Integer cdTipo;

	/** Atributo numero. */
	private String numero;

	/** Atributo numeroContrato. */
	private Long numeroContrato;

	/** Atributo motivoDesc. */
	private String motivoDesc;

	/** Atributo situacaoDesc. */
	private String situacaoDesc;

	/** Atributo participacao. */
	private String participacao;

	/** Atributo possuiAditivos. */
	private String possuiAditivos;

	/** Atributo dataHoraCadastramento. */
	private String dataHoraCadastramento;

	/** Atributo inicioVigencia. */
	private String inicioVigencia;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo cdAgenciaGestora. */
	private int cdAgenciaGestora;

	/** Atributo dsAgenciaGestora. */
	private String dsAgenciaGestora;

	/** Atributo dsGerenteResponsavel. */
	private String dsGerenteResponsavel;

	/** Atributo cdGerenteResponsavel. */
	private Long cdGerenteResponsavel;
	
	/** Atributo dsNomeRazaoSocialParticipante. */
	private String dsNomeRazaoSocialParticipante;
	
	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo cpfCnpj. */
	private String cpfCnpj;

	/** Atributo nomeRazaoSocial. */
	private String nomeRazaoSocial;

	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos = false;

	// M�todos
	/**
	 * Limpar pagina.
	 *
	 * @param evt the evt
	 */
	public void limparPagina(ActionEvent evt) {
		this.identificacaoClienteContratoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setPaginaCliente("identificacaoClienteBloqDesbloqParticipantes");
		identificacaoClienteContratoBean.setPaginaRetorno("conBloquearDesbloquearParticipantes");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		
		setDsGerenteResponsavel("");
		setCpfCnpjParticipante("");
		setDsNomeRazaoSocialParticipante("");
		setCpfCnpj("");
		setNomeRazaoSocial("");
	}

	/**
	 * Imprimir relatorio contas.
	 *
	 * @return the string
	 */
	public String imprimirRelatorioContas() {
		try {
			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
			String logoPath = servletContext.getRealPath(caminho);

			Map<String, Object> par = new HashMap<String, Object>();

			par.put("titulo", "Pagamento Integrado - Contas");
			par.put("empresa", getEmpresa());
			par.put("numero", getNumero());
			par.put("tipo", getTipo());
			par.put("descricaoContrato", manterContratoContasBean.getDescricaoContrato());
			par.put("logoBradesco", logoPath);

			try {
				// M�todo que gera o relat�rio PDF.
				PgitUtil.geraRelatorioPdf("/relatorios/manutencaoContrato/manterContrato/contas",
								"imprimirContasManterContrato", manterContratoContasBean.getListaGridPesquisaContas(),
								par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}

		return "";
	}

	/**
	 * Conta.
	 *
	 * @return the string
	 */
	public String conta() {

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDataHoraCadastramento(null);
		setCdAgenciaGestora(saidaDTO.getCdAgenciaOperadora());
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getDescFuncionarioBradesco());

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}

		// setInicioVigencia(inicioVigencia)

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ||
				"".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
			setDsNomeRazaoSocialParticipante("");
			setCpfCnpjParticipante("");
		} else {
			setDsNomeRazaoSocialParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}

		manterContratoContasBean.setCdPessoaJuridica(getCdEmpresa());
		manterContratoContasBean.setCdTipoContratoNegocio(getCdTipo());
		manterContratoContasBean.setNrSequenciaContratoNegocio(getNumero());
		manterContratoContasBean.setDescricaoContrato(saidaDTO.getDsContrato());

		manterContratoContasBean.iniciarTela();
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		return "CONTAS";
	}

	/**
	 * Imprimir relatorio participantes.
	 *
	 * @return the string
	 */
	public String imprimirRelatorioParticipantes() {
		try {
			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
			String logoPath = servletContext.getRealPath(caminho);

			Map<String, Object> par = new HashMap<String, Object>();

			par.put("titulo", "Pagamento Integrado - Participantes");
			par.put("empresa", getEmpresa());
			par.put("numero", getNumero());
			par.put("tipo", getTipo());
			par.put("descricaoContrato", manterContratoParticipantesBean.getDescricaoContrato());
			par.put("logoBradesco", logoPath);

			try {
				// M�todo que gera o relat�rio PDF.
				PgitUtil.geraRelatorioPdf("/relatorios/manutencaoContrato/manterContrato/participantes",
								"imprimirParticipantesManterContrato", manterContratoParticipantesBean.getListaGrid(),
								par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}

		return "";
	}

	/**
	 * Participantes.
	 *
	 * @return the string
	 */
	public String participantes() {

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDataHoraCadastramento(null);
		setDescricaoContrato(saidaDTO.getDsContrato());

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getDescFuncionarioBradesco());

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}

		// setInicioVigencia(inicioVigencia)

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}
		manterContratoParticipantesBean.inciarTela();

		manterContratoParticipantesBean.setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		manterContratoParticipantesBean.setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		manterContratoParticipantesBean.setGrupEconRepresentante(getGrupoEconomicoMaster());
		manterContratoParticipantesBean.setAtivEconRepresentante(getAtividadeEconomicaMaster());
		manterContratoParticipantesBean.setSegRepresentante(getSegmentoMaster());
		manterContratoParticipantesBean.setSubSegRepresentante(getSubSegmentoMaster());
		manterContratoParticipantesBean.setEmpresaContrato(getEmpresa());
		manterContratoParticipantesBean.setTipoContrato(getTipo());
		manterContratoParticipantesBean.setNumeroContrato(getNumero());
		manterContratoParticipantesBean.setSituacaoContrato(getSituacaoDesc());
		manterContratoParticipantesBean.setMotivoContrato(getMotivoDesc());
		manterContratoParticipantesBean.setParticipacaoContrato(getParticipacao());
		manterContratoParticipantesBean.setAditivosContrato(getPossuiAditivos());
		manterContratoParticipantesBean.setDataHoraCadastramento(getDataHoraCadastramento());
		// manterContratoParticipantesBean.setVigenciaContrato(vigenciaContrato);
		manterContratoParticipantesBean.setDescricaoContrato(getDescricaoContrato());

		// manterContratoParticipantesBean.setNomeRazaoParticipante(this.getIdentificacaoClienteContratoBean().getDsNomeRazaoSocialParticipante()
		// == null ? getNomeRazaoSocial() :
		// this.getIdentificacaoClienteContratoBean().getDsNomeRazaoSocialParticipante());
		// manterContratoParticipantesBean.setCpfCnpjParticipante(this.getIdentificacaoClienteContratoBean().getCpfCnpjParticipante()
		// == null ? getCpfCnpj() : this.getIdentificacaoClienteContratoBean().getCpfCnpjParticipante());
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ||
				"".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
			manterContratoParticipantesBean.setNomeRazaoParticipante("");
			manterContratoParticipantesBean.setCpfCnpjParticipante("");
		} else {
			manterContratoParticipantesBean.setNomeRazaoParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
			manterContratoParticipantesBean.setCpfCnpjParticipante(identificacaoClienteContratoBean
					.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
		}
		
		
		
		manterContratoParticipantesBean.setCdEmpresaContrato(saidaDTO.getCdPessoaJuridica());
		manterContratoParticipantesBean.setCdTipoContrato(saidaDTO.getCdTipoContrato());
		manterContratoParticipantesBean.setNrSequenciaContrato(saidaDTO.getNrSequenciaContrato().toString());

		manterContratoParticipantesBean.carregaListaConsultar();
		//identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		
		if ("S".equalsIgnoreCase(identificacaoClienteContratoBean.getFlagDuplicacao())) {
			return "selParticipantesDuplicacaoArquivoRetorno";
		} else {
			return "PARTICIPANTES";
		}
	}

	/**
	 * Confirmar alterar representante.
	 *
	 * @return the string
	 */
	public String confirmarAlterarRepresentante() {
		try {
			manterContratoRepresentanteBean.confirmarAlterar();
			identificacaoClienteContratoBean.consultarContrato();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			return null;
		}

		return "";
	}

	/**
	 * Alterar representante.
	 *
	 * @return the string
	 */
	public String alterarRepresentante() {

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDataHoraCadastramento(null);
		setDescricaoContrato(saidaDTO.getDsContrato());

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getDescFuncionarioBradesco());

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}

		// setInicioVigencia(inicioVigencia)

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}
		manterContratoRepresentanteBean.iniciarTela();

		manterContratoRepresentanteBean.setCpfCnpjRepresentante(saidaDTO.getCnpjOuCpfFormatado());
		manterContratoRepresentanteBean.setNomeRazaoRepresentante(saidaDTO.getNmRazaoSocialRepresentante());
		manterContratoRepresentanteBean.setGrupEconRepresentante(getGrupoEconomicoMaster());
		manterContratoRepresentanteBean.setAtivEconRepresentante(getAtividadeEconomicaMaster());
		manterContratoRepresentanteBean.setSegRepresentante(getSegmentoMaster());
		manterContratoRepresentanteBean.setSubSegRepresentante(getSubSegmentoMaster());
		manterContratoRepresentanteBean.setEmpresaContrato(getEmpresa());
		manterContratoRepresentanteBean.setTipoContrato(getTipo());
		manterContratoRepresentanteBean.setNumeroContrato(getNumero());
		manterContratoRepresentanteBean.setSituacaoContrato(getSituacaoDesc());
		manterContratoRepresentanteBean.setMotivoContrato(getMotivoDesc());
		manterContratoRepresentanteBean.setParticipacaoContrato(getParticipacao());
		manterContratoRepresentanteBean.setDescricaoContrato(getDescricaoContrato());

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ||
				"".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
			manterContratoRepresentanteBean.setCpfCnpjParticipante("");
			manterContratoRepresentanteBean.setNomeRazaoParticipante("");
		} else {
			manterContratoRepresentanteBean.setCpfCnpjParticipante(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? ""
				: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado());
			manterContratoRepresentanteBean.setNomeRazaoParticipante(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
				: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}

		manterContratoRepresentanteBean.setCdClub(saidaDTO.getCdClubRepresentante());
		manterContratoRepresentanteBean.setCdEmpresaContrato(getCdEmpresa());
		manterContratoRepresentanteBean.setCdTipoContrato(getCdTipo());
		manterContratoRepresentanteBean.setNrSequenciaContrato(getNumero());

		manterContratoRepresentanteBean.carregaListaAlterar();
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "REPRESENTANTE";
	}

	/**
	 * Dados basicos.
	 *
	 * @return the string
	 */
	public String dadosBasicos() {

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		ConsultarDadosBasicoContratoEntradaDTO entradaDTO = new ConsultarDadosBasicoContratoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
		entradaDTO.setCdTipoContrato(saidaDTO.getCdTipoContrato());
		entradaDTO.setNumeroSequenciaContrato(saidaDTO.getNrSequenciaContrato());
		entradaDTO.setCdClub(saidaDTO.getCdClubRepresentante());
		entradaDTO.setCdCgcCpf(saidaDTO.getCdCpfCnpjRepresentante());
		entradaDTO.setCdFilialCgcCpf(saidaDTO.getCdFilialCpfCnpjRepresentante());
		entradaDTO.setCdControleCgcCpf(saidaDTO.getCdControleCpfCnpjRepresentante());
		// Pdc foi regerado e foi retirado esse campo - 28/03/2011
		// entradaDTO.setCdAgencia(saidaDTO.getCdAgenciaOperadora());
		ConsultarDadosBasicoContratoSaidaDTO consultarDadosBasicoSaidaDTO = getManterContratoImpl()
						.consultarDadosBasicoContrato(entradaDTO);

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());
		setDsGerenteResponsavel(saidaDTO.getDescFuncionarioBradesco());

		manterContratoDadosBasicosBean.setCpfCnpjClienteMaster(saidaDTO.getCnpjOuCpfFormatado());
		manterContratoDadosBasicosBean.setNomeRazaoClienteMaster(saidaDTO.getNmRazaoSocialRepresentante());
		manterContratoDadosBasicosBean.setCdGrupoEconomicoClienteMaster(consultarDadosBasicoSaidaDTO
						.getCdGrupoEconomicoParticipante());
		manterContratoDadosBasicosBean.setGrupoEconomicoClienteMaster(consultarDadosBasicoSaidaDTO
						.getDsGrupoEconomico());
		manterContratoDadosBasicosBean.setCdAtividadeEconomicaClienteMaster(consultarDadosBasicoSaidaDTO
						.getCdAtividadeEconomicaParticipante());
		manterContratoDadosBasicosBean.setAtividadeEconomicaClienteMaster(consultarDadosBasicoSaidaDTO
						.getDsAtividadeEconomica());
		manterContratoDadosBasicosBean.setCdSegmentoClienteMaster(consultarDadosBasicoSaidaDTO
						.getCdSegmentoEconomicoParticipante());
		manterContratoDadosBasicosBean.setSegmentoClienteMaster(consultarDadosBasicoSaidaDTO.getDsSegmentoEconomico());
		manterContratoDadosBasicosBean.setCdSubSegmentoClienteMaster(consultarDadosBasicoSaidaDTO
						.getCdSubSegmentoEconomico());
		manterContratoDadosBasicosBean.setSubSegmentoClienteMaster(consultarDadosBasicoSaidaDTO
						.getDsSubSegmentoEconomico());
		manterContratoDadosBasicosBean.setCdSetorContrato(consultarDadosBasicoSaidaDTO.getCdSetorContrato());
		manterContratoDadosBasicosBean.setDsSetorContrato(consultarDadosBasicoSaidaDTO.getDsSetorContrato());

		manterContratoDadosBasicosBean.setCodPessoaJuridica(consultarDadosBasicoSaidaDTO.getCdPessoaJuridica());
		manterContratoDadosBasicosBean.setNumeroSequencialUnidadeOrganizacional(consultarDadosBasicoSaidaDTO
						.getNumeroSequencialUnidadeOrganizacional());
		manterContratoDadosBasicosBean.setCdClub(saidaDTO.getCdClubRepresentante());
		manterContratoDadosBasicosBean.setCdEmpresaGestoraContrato(saidaDTO.getCdPessoaJuridica());
		manterContratoDadosBasicosBean.setEmpresaGestoraContrato(PgitUtil.concatenarCampos(saidaDTO
						.getCdPessoaJuridica(), saidaDTO.getDsPessoaJuridica()));
		manterContratoDadosBasicosBean.setCdTipoContrato(saidaDTO.getCdTipoContrato());
		manterContratoDadosBasicosBean.setTipoContrato(saidaDTO.getDsTipoContrato());
		manterContratoDadosBasicosBean.setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		manterContratoDadosBasicosBean.setVlSaldoVirtualTeste(consultarDadosBasicoSaidaDTO.getVlSaldoVirtualTeste());
		manterContratoDadosBasicosBean.setDescricaoContrato(saidaDTO.getDsContrato());
		manterContratoDadosBasicosBean.setCdIdiomaContrato(consultarDadosBasicoSaidaDTO.getCdIdioma());
		manterContratoDadosBasicosBean.setIdiomaContrato(consultarDadosBasicoSaidaDTO.getDsIdioma());
		manterContratoDadosBasicosBean.setCdMoedaContrato(consultarDadosBasicoSaidaDTO.getCdMoeda());
		manterContratoDadosBasicosBean.setMoedaContrato(consultarDadosBasicoSaidaDTO.getDsMoeda());
		manterContratoDadosBasicosBean.setCdOrigemContrato(consultarDadosBasicoSaidaDTO.getCdOrigem());
		manterContratoDadosBasicosBean.setOrigemContrato(consultarDadosBasicoSaidaDTO.getDsOrigem());
		manterContratoDadosBasicosBean.setPossuiAditivosContrato(consultarDadosBasicoSaidaDTO.getDsPossuiAditivo());

		// o f�bio de almeida, pediu no email do dia 02/07/2010 �s 18:58 para pegar esses dados do campo
		// cdSituacaoContrato do contrato
		manterContratoDadosBasicosBean.setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		manterContratoDadosBasicosBean.setSituacaoContrato(saidaDTO.getDsSituacaoContrato());

		// o f�bio de almeida, pediu no email do dia 02/07/2010 �s 18:58 para pegar esses dados do campo
		// cdSituacaoMotivoContrato da consulta de dados b�sicos
		manterContratoDadosBasicosBean.setCdMotivoContrato(consultarDadosBasicoSaidaDTO.getCdSituacaoContrato());
		manterContratoDadosBasicosBean.setMotivoContrato(consultarDadosBasicoSaidaDTO.getDsSituacaoContrato());

		manterContratoDadosBasicosBean
						.setInicioVigenciaContrato(consultarDadosBasicoSaidaDTO.getDataVigenciaContrato());
		manterContratoDadosBasicosBean.setFimVigenciaContrato(consultarDadosBasicoSaidaDTO.getDataCadastroContrato());
		manterContratoDadosBasicosBean.setNumeroComercialContrato(consultarDadosBasicoSaidaDTO
						.getNumeroComercialContrato());
		manterContratoDadosBasicosBean.setParticipacaoContrato(consultarDadosBasicoSaidaDTO
						.getDsTipoParticipacaoContrato());
		manterContratoDadosBasicosBean.setDataHoraAssinaturaContrato(consultarDadosBasicoSaidaDTO
						.getDataAssinaturaContrato());
		manterContratoDadosBasicosBean.setCdPermiteEncerramentoContrato(consultarDadosBasicoSaidaDTO
						.getCdIndicaPermiteEncerramento());
		manterContratoDadosBasicosBean.setCdOperadoraUnidOrg(consultarDadosBasicoSaidaDTO.getCdAgenciaOperadora());
		manterContratoDadosBasicosBean.setCdContabilUnidOrg(consultarDadosBasicoSaidaDTO.getCdAgenciaContabil());
		manterContratoDadosBasicosBean.setCdGestoraUnidOrg(consultarDadosBasicoSaidaDTO.getCdDeptoGestor());
		if (manterContratoDadosBasicosBean.getCdGestoraUnidOrg() != 0) {
			manterContratoDadosBasicosBean.setGestoraUnidOrg(PgitUtil.concatenarCampos(consultarDadosBasicoSaidaDTO
							.getCdDeptoGestor(), consultarDadosBasicoSaidaDTO.getDsDeptoGestor()));
		} else {
			manterContratoDadosBasicosBean.setGestoraUnidOrg("");
		}
		manterContratoDadosBasicosBean.setCdGerenteResponsavelUnidOrg(consultarDadosBasicoSaidaDTO
						.getCdFuncionarioBradesco());
		manterContratoDadosBasicosBean.setGerenteResponsavelUnidOrg(consultarDadosBasicoSaidaDTO
						.getCdFuncionarioBradesco()
						+ " - " + consultarDadosBasicoSaidaDTO.getDsFuncionarioBradesco());

		manterContratoDadosBasicosBean.setDataHoraInclusao(PgitUtil.concatenarCampos(consultarDadosBasicoSaidaDTO
						.getDataInclusaoContrato(), consultarDadosBasicoSaidaDTO.getHoraInclusaoContrato()));
		manterContratoDadosBasicosBean.setUsuarioInclusao(consultarDadosBasicoSaidaDTO.getUsuarioInclusaoContrato());
		manterContratoDadosBasicosBean
						.setTipoCanalInclusao(consultarDadosBasicoSaidaDTO.getCdTipoCanalInclusao() == 0 ? ""
										: PgitUtil.concatenarCampos(consultarDadosBasicoSaidaDTO
														.getCdTipoCanalInclusao(), consultarDadosBasicoSaidaDTO
														.getDsTipoCanalInclusao()));
		manterContratoDadosBasicosBean
						.setComplementoInclusao(consultarDadosBasicoSaidaDTO.getOperacaoFluxoInclusao() == null
										|| consultarDadosBasicoSaidaDTO.getOperacaoFluxoInclusao().equals("0") ? ""
										: consultarDadosBasicoSaidaDTO.getOperacaoFluxoInclusao());

		manterContratoDadosBasicosBean.setDataHoraManutencao(PgitUtil.concatenarCampos(consultarDadosBasicoSaidaDTO
						.getDataManutencaoContrato(), consultarDadosBasicoSaidaDTO.getHoraManutencaoContrato()));
		manterContratoDadosBasicosBean
						.setUsuarioManutencao(consultarDadosBasicoSaidaDTO.getUsuarioManutencaoContrato());
		manterContratoDadosBasicosBean
						.setTipoCanalManutencao(consultarDadosBasicoSaidaDTO.getCdTipoCanalManutencao() == 0 ? ""
										: PgitUtil.concatenarCampos(consultarDadosBasicoSaidaDTO
														.getCdTipoCanalManutencao(), consultarDadosBasicoSaidaDTO
														.getDsTipoCanalManutencao()));
		manterContratoDadosBasicosBean.setComplementoManutencao(consultarDadosBasicoSaidaDTO
						.getOperacaoFluxoManutencao() == null
						|| consultarDadosBasicoSaidaDTO.getOperacaoFluxoManutencao().equals("0") ? ""
						: consultarDadosBasicoSaidaDTO.getOperacaoFluxoManutencao());
		manterContratoDadosBasicosBean.setDsGerente(consultarDadosBasicoSaidaDTO.getDsFuncionarioBradesco());

		// PREENCHE SE USU�RIO TIVER FEITO PESQUISA POR CLIENTE - Denize Almeida De Brito - quarta-feira, 3 de agosto de
		// 2011 16:13

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ||
				"".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		} else {
			setCpfCnpj(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? ""
				: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
				: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}

		manterContratoDadosBasicosBean.setEmpresaGestoraProposta(PgitUtil.concatenarCampos(consultarDadosBasicoSaidaDTO
						.getCdPessoaJuridicaProposta(), consultarDadosBasicoSaidaDTO.getDsPessoaJuridicaProposta()));
		manterContratoDadosBasicosBean.setTipoProposta(PgitUtil.concatenarCampos(consultarDadosBasicoSaidaDTO
						.getCdTipoContratoProposta(), consultarDadosBasicoSaidaDTO.getDsTipoContratoProposta()));
		manterContratoDadosBasicosBean.setNumeroProposta(consultarDadosBasicoSaidaDTO.getNrContratoProposta());

		return "DADOSBASICOS";

	}

	/**
	 * Servicos.
	 *
	 * @return the string
	 */
	public String servicos() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setDsGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getNmFuncionarioBradesco());

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
				&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		
		} else {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		}

		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setNumeroContrato(saidaDTO.getNrSequenciaContrato());
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDescricaoContrato(saidaDTO.getDsContrato());

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}
		
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ||
				"".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		} else {
			setCpfCnpj(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? ""
				: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
				: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}

		this.manterContratoServicosBean.setCdPessoaJuridica(this.cdEmpresa);
		this.manterContratoServicosBean.setCdTipoContrato(this.cdTipo);
		this.manterContratoServicosBean.setNrSequenciaContrato(Long.parseLong(this.numero));

		manterContratoServicosBean.carregaLista();
		manterContratoServicosBean.setPossuiModalidade(false);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "SERVICOS";
	}

	/**
	 * Get: manterContratoTarifasBean.
	 *
	 * @return manterContratoTarifasBean
	 */
	public TarifasBean getManterContratoTarifasBean() {
		return manterContratoTarifasBean;
	}

	/**
	 * Set: manterContratoTarifasBean.
	 *
	 * @param manterContratoTarifasBean the manter contrato tarifas bean
	 */
	public void setManterContratoTarifasBean(TarifasBean manterContratoTarifasBean) {
		this.manterContratoTarifasBean = manterContratoTarifasBean;
	}

	/**
	 * Tarifas.
	 *
	 * @return the string
	 */
	public String tarifas() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setGerenteResponsavelFormatado(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getNmFuncionarioBradesco());

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());

		setDataHoraCadastramento(null);

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}

		// setInicioVigencia(inicioVigencia)

		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}
		
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ||
			"".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
			setCpfCnpj("");
			setNomeRazaoSocial("");
		} else {
			setCpfCnpj(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ? ""
				: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
				.getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean
				.getSaidaConsultarListaClientePessoas().getDsNomeRazao() == null ? ""
				: identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}
		
		manterContratoTarifasBean.iniciarTela();
		manterContratoTarifasBean.setCpfCnpjRepresentante(getCpfCnpjMaster());
		manterContratoTarifasBean.setNomeRazaoRepresentante(getNomeRazaoSocialMaster());
		manterContratoTarifasBean.setGrupEconRepresentante(getGrupoEconomicoMaster());
		manterContratoTarifasBean.setAtivEconRepresentante(getAtividadeEconomicaMaster());
		manterContratoTarifasBean.setSegRepresentante(getSegmentoMaster());
		manterContratoTarifasBean.setSubSegRepresentante(getSubSegmentoMaster());
		manterContratoTarifasBean.setEmpresaContrato(getEmpresa());
		manterContratoTarifasBean.setTipoContrato(getTipo());
		manterContratoTarifasBean.setNumeroContrato(getNumero());
		manterContratoTarifasBean.setDescricaoContrato(saidaDTO.getDsContrato());
		manterContratoTarifasBean.setSituacaoContrato(getSituacaoDesc());
		manterContratoTarifasBean.setMotivoContrato(getMotivoDesc());
		manterContratoTarifasBean.setParticipacaoContrato(getParticipacao());

		manterContratoTarifasBean.setCodPessoaJuridica(getCdEmpresa());
		manterContratoTarifasBean.setCdTipoContrato(getCdTipo());
		manterContratoTarifasBean.setNrSequenciaContrato(Long.parseLong(getNumero()));

		manterContratoTarifasBean.carregaListaConsultar();
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "TARIFAS";
	}

	/**
	 * Set: manterContratoServicosBean.
	 *
	 * @param manterContratoServicosBean the manter contrato servicos bean
	 */
	public void setManterContratoServicosBean(ServicosBean manterContratoServicosBean) {
		this.manterContratoServicosBean = manterContratoServicosBean;
	}

	/**
	 * Get: manterContratoServicosBean.
	 *
	 * @return manterContratoServicosBean
	 */
	public ServicosBean getManterContratoServicosBean() {
		return manterContratoServicosBean;
	}

	/**
	 * Imprimir contrato.
	 *
	 * @throws IOException the IO exception
	 */
	public void imprimirContrato() throws IOException {
		ImprimirContratoEntradaDTO entradaImprimirContrato = new ImprimirContratoEntradaDTO();

		ListarContratosPgitSaidaDTO contratosPgitSelecionado = identificacaoClienteContratoBean.getListaGridPesquisa()
						.get(identificacaoClienteContratoBean.getItemSelecionadoLista());

		entradaImprimirContrato.setCdPessoaJuridicaContrato(contratosPgitSelecionado.getCdPessoaJuridica());
		entradaImprimirContrato.setCdTipoContratoNegocio(contratosPgitSelecionado.getCdTipoContrato());
		entradaImprimirContrato.setNrSequenciaContratoNegocio(contratosPgitSelecionado.getNrSequenciaContrato());

		long protocolo = getImprimirContratoService().imprimirContratoMulticanal(entradaImprimirContrato);

		getImprimirService().imprimirArquivo(FacesUtils.getServletResponse().getOutputStream(), protocolo);

		PgitFacesUtils.generateReportResponse("contrato");
	}

	/**
	 * Renegociar.
	 */
	public void renegociar() {

		contratoPgitSelecionado = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		/*
		 * ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		 * 
		 * HttpServletRequest httpServletRequest = (HttpServletRequest) externalContext.getRequest();
		 * 
		 * String url = externalContext.encodeResourceURL("http://" +
		 * httpServletRequest.getRemoteHost()+"/pgit_negociacao/proposta.renegociacao.jsf?" + "nseqContrNegoc=" +
		 * contratoPgitSelecionado.getNrSequenciaContrato() +"?" + "cpssoaJuridContr=" +
		 * contratoPgitSelecionado.getCdPessoaJuridica() + "?" + "ctpoContrNegoc=" +
		 * contratoPgitSelecionado.getCdTipoContrato() );
		 * 
		 * 
		 * 
		 * 
		 * try { externalContext.redirect(url); } catch (Exception error) { error.printStackTrace(); }
		 */

	}

	/**
	 * Layouts.
	 *
	 * @return the string
	 */
	public String layouts() {
		// Layouts

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(
						identificacaoClienteContratoBean.getItemSelecionadoLista());

		setCpfCnpjMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoSocialMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCdEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumero(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDescricaoContrato(saidaDTO.getDsContrato());
		setGerenteResponsavelFormatado(saidaDTO.getCdFuncionarioBradesco() + " - " + saidaDTO.getNmFuncionarioBradesco());
		setDsGerenteResponsavel(saidaDTO.getDescFuncionarioBradesco());

		setDataHoraCadastramento(null);

		// Campo posto na tela dia 08/11 conforme orienta��o da Denise
		setDsAgenciaGestora(saidaDTO.getDsAgenciaOperadora());

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}
		}

		// setInicioVigencia(inicioVigencia)
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() != null
						&& !identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
										.getCnpjOuCpfFormatado().equals("")) {
			setCpfCnpj(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado());
			setNomeRazaoSocial(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());

		} else {
			setCpfCnpj(getCpfCnpjMaster());
			setNomeRazaoSocial(getNomeRazaoSocialMaster());
		}

		//Verifica se consulta e feita por contrato, se nao for, mostra dados participante.
		if (identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado() == null ||
				"".equals(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCnpjOuCpfFormatado())) {
			setCpfCnpjParticipante("");
			setDsNomeRazaoSocialParticipante("");
		} else {
			setCpfCnpjParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas()
					.getCnpjOuCpfFormatado());
			setDsNomeRazaoSocialParticipante(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
		}

		manterContratoLayoutsArquivosBean.setCdPessoaJuridica(getCdEmpresa());
		manterContratoLayoutsArquivosBean.setCdTipoContratoNegocio(getCdTipo());
		manterContratoLayoutsArquivosBean.setNrSequenciaContratoNegocio(getNumero());

		manterContratoLayoutsArquivosBean.carregaListaLayout();
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		return "LAYOUT";
	}

	/**
	 * Carregar parametros.
	 *
	 * @return the string
	 */
	public String carregarParametros() {
		limparPagina(null);

		Long empresaGestora = 0l;
		Integer tipoContrato = 0;

		try {
			empresaGestora = Long.parseLong(PgitUtil.verificaStringNula((String) FacesUtils
							.getRequestParameter("empresaGestora")));
		} catch (NumberFormatException e) {
			empresaGestora = 0L;
		}

		try {
			tipoContrato = Integer.parseInt(PgitUtil.verificaStringNula((String) FacesUtils
							.getRequestParameter("tipoContrato")));
		} catch (NumberFormatException e) {
			tipoContrato = 0;
		}

		String numeroContrato = PgitUtil.verificaStringNula((String) FacesUtils.getRequestParameter("numeroContrato"));

		if (empresaGestora == 0L || tipoContrato == 0 || numeroContrato.equals("0") || numeroContrato.equals("")) {
			identificacaoClienteContratoBean.setEmpresaGestoraFiltro(0l);
			identificacaoClienteContratoBean.setTipoContratoFiltro(0);
			identificacaoClienteContratoBean.setNumeroFiltro("");
			return "conManterContrato";
		}

		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(empresaGestora);
		identificacaoClienteContratoBean.setTipoContratoFiltro(tipoContrato);
		identificacaoClienteContratoBean.setNumeroFiltro(numeroContrato);

		identificacaoClienteContratoBean.consultarContrato();

		return "conManterContrato";
	}

	/**
	 * Imprimir relatorio layout.
	 *
	 * @return the string
	 */
	public String imprimirRelatorioLayout() {

		String caminho = "/images/Bradesco_logo.JPG";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
		String logoPath = servletContext.getRealPath(caminho);

		// Par�metros do Relat�rio
		Map<String, Object> par = new HashMap<String, Object>();

		par.put("tipo", getTipo());
		par.put("empresa", getEmpresa());
		par.put("numero", getNumero());
		par.put("descContrato", getDescricaoContrato());
		par.put("logoBradesco", logoPath);

		par.put("titulo", "Pagamento Integrado - Layout");

		try {

			// M�todo que gera o relat�rio PDF.
			PgitUtil.geraRelatorioPdf("/relatorios/manutencaoContrato/manterContrato/layout", "imprimirLayoutArquivo",
							manterContratoLayoutsArquivosBean.getListaLayout(), par);

		} /* Exce��o do relat�rio */
		catch (Exception e) {
			throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
		}

		return "";
	}

	/**
	 * Imprimir anexo contrato.
	 *
	 * @throws IOException the IO exception
	 */
	public void imprimirAnexoContrato() throws IOException {
		ImprimirAnexoPrimeiroContratoEntradaDTO entradaPrimeiroContrato = new ImprimirAnexoPrimeiroContratoEntradaDTO();
		ImprimirAnexoSegundoContratoEntradaDTO entradaSegundoContrato = new ImprimirAnexoSegundoContratoEntradaDTO();
		ImprimirAnexoPrimeiroContratoSaidaDTO saidaPrimeiroContrato = null;
		ImprimirAnexoSegundoContratoSaidaDTO saidaSegundoContrato = null;

		ListarContratosPgitSaidaDTO contratosPgitSelecionado = identificacaoClienteContratoBean.getListaGridPesquisa()
						.get(identificacaoClienteContratoBean.getItemSelecionadoLista());

		entradaPrimeiroContrato.setCdPessoaJuridicaContrato(contratosPgitSelecionado.getCdPessoaJuridica());
		entradaPrimeiroContrato.setCdTipoContratoNegocio(contratosPgitSelecionado.getCdTipoContrato());
		entradaPrimeiroContrato.setNrSequenciaContratoNegocio(contratosPgitSelecionado.getNrSequenciaContrato());
		saidaPrimeiroContrato = getImprimirAnexoPrimeiroContratoService().imprimirAnexoPrimeiroContrato(
						entradaPrimeiroContrato);

		entradaSegundoContrato.setCdPessoaJuridicaContrato(contratosPgitSelecionado.getCdPessoaJuridica());
		entradaSegundoContrato.setCdTipoContratoNegocio(contratosPgitSelecionado.getCdTipoContrato());
		entradaSegundoContrato.setNrSequenciaContratoNegocio(contratosPgitSelecionado.getNrSequenciaContrato());
		saidaSegundoContrato = getImprimirAnexoSegundoContratoService().imprimirAnexoPrimeiroContrato(entradaSegundoContrato);

		AnexoContratoDTO anexoContratoDTO = new AnexoContratoDTO();
		try {
			BeanUtils.copyProperties(anexoContratoDTO, saidaPrimeiroContrato);
			BeanUtils.copyProperties(anexoContratoDTO, saidaSegundoContrato);

		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		List<AnexoContratoDTO> listaAnexoContrato = new ArrayList<AnexoContratoDTO>();
		listaAnexoContrato.add(anexoContratoDTO);

		String caminho = "/images/Bradesco_logo.JPG";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
		String logoPath = servletContext.getRealPath(caminho);

		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");

		// Dados de Participantes
		String pathArquivos = "/relatorios/manutencaoContrato/manterContrato/anexoContrato";
		String arquivoParticipantes = pathArquivos + "/relParticipantes.jasper";
		String pathSubRelParticipantes = servletContext.getRealPath(arquivoParticipantes);
		List<OcorrenciasParticipantes> listaParticipantes = new ArrayList<OcorrenciasParticipantes>();
		int qtdeParticipantes = 0;
		JRBeanCollectionDataSource beanParamParticipantes = null;
		if (anexoContratoDTO.getQtdeParticipante() != 0) {
			for (OcorrenciasParticipantes ocorrenciasParticipantes : anexoContratoDTO.getListaParticipantes()) {
				listaParticipantes.add(ocorrenciasParticipantes);
				qtdeParticipantes++;
				if (qtdeParticipantes == anexoContratoDTO.getQtdeParticipante()) {
					break;
				}
			}
			beanParamParticipantes = new JRBeanCollectionDataSource(listaParticipantes);
		}

		// Dados de contas
		String arquivoContas = pathArquivos + "/relContas.jasper";
		String pathSubRelContas = servletContext.getRealPath(arquivoContas);
		List<OcorrenciasContas> listaContas = new ArrayList<OcorrenciasContas>();
		int qtdeContas = 0;
		JRBeanCollectionDataSource beanParamContas = null;
		if (anexoContratoDTO.getQtdeConta() != 0) {
			for (OcorrenciasContas ocorrenciasContas : anexoContratoDTO.getListaContas()) {
				ocorrenciasContas.setDigitoContaConta(ocorrenciasContas.getDigitoContaConta().substring(7, 15));
				listaContas.add(ocorrenciasContas);
				qtdeContas++;
				if (qtdeContas == anexoContratoDTO.getQtdeConta()) {
					break;
				}
			}
			beanParamContas = new JRBeanCollectionDataSource(listaContas);
		}

		// Configura��o de Layout
		String arquivoLayouts = pathArquivos + "/relLayouts.jasper";
		String pathSubRelLayouts = servletContext.getRealPath(arquivoLayouts);
		List<OcorrenciasLayout> listaLayouts = new ArrayList<OcorrenciasLayout>();
		int qtdeLayouts = 0;
		JRBeanCollectionDataSource beanParamLayout = null;
		if (anexoContratoDTO.getQtLayout() != 0) {
			for (OcorrenciasLayout ocorrenciasLayout : anexoContratoDTO.getListaLayouts()) {
				listaLayouts.add(ocorrenciasLayout);
				qtdeLayouts++;
				if (qtdeLayouts == anexoContratoDTO.getQtLayout()) {
					break;
				}
			}
			beanParamLayout = new JRBeanCollectionDataSource(listaLayouts);
		}

		String arquivoServModalidades = pathArquivos + "/relServicosModalidades.jasper";
		String pathSubServModalidades = servletContext.getRealPath(arquivoServModalidades);

		// Pagamentos
		String arquivoPagamentosFornecedores = pathArquivos + "/relPagamentosFornecedores.jasper";
		String pathSubRelPagamentosFornecedores = servletContext.getRealPath(arquivoPagamentosFornecedores);

		String arquivoPagamentosFornecedoresTitulos = pathArquivos + "/relPagamentosFornecedoresTitulos.jasper";
		String pathSubRelPagamentosFornecedoresTitulos = servletContext
						.getRealPath(arquivoPagamentosFornecedoresTitulos);

		String arquivoPagamentosFornecedoresModularidades = pathArquivos
						+ "/relPagamentosFornecedoresModularidades.jasper";
		String pathSubRelPagamentosFornecedoresModularidades = servletContext
						.getRealPath(arquivoPagamentosFornecedoresModularidades);

		String arquivoPagamentosFornecedoresOutrasModularidades = pathArquivos
						+ "/relPagamentosFornecedoresOutrasModularidades.jasper";
		String pathSubRelPagamentosFornecedoresOutrasModularidades = servletContext
						.getRealPath(arquivoPagamentosFornecedoresOutrasModularidades);

		String arquivoPagamentosFornecedoresCompl = pathArquivos + "/relPagamentosFornecedoresCompl.jasper";
		String pathSubRelPagamentosFornecedoresCompl = servletContext.getRealPath(arquivoPagamentosFornecedoresCompl);

		String arquivoPagamentosTributos = pathArquivos + "/relPagamentosTributos.jasper";
		String pathSubRelPagamentosTributos = servletContext.getRealPath(arquivoPagamentosTributos);

		String arquivoTributos = pathArquivos + "/relPagamentosTributosModalidades.jasper";
		String pathSubRelTributos = servletContext.getRealPath(arquivoTributos);

		String arquivoPagamentosSalarios = pathArquivos + "/relPagamentosSalarios.jasper";
		String pathSubRelPagamentosSalarios = servletContext.getRealPath(arquivoPagamentosSalarios);

		String arquivoPagamentosSalariosModularidades = pathArquivos + "/relPagamentosSalariosModularidades.jasper";
		String pathSubRelPagamentosSalariosModularidades = servletContext
						.getRealPath(arquivoPagamentosSalariosModularidades);

		String arquivoPagamentosBeneficios = pathArquivos + "/relPagamentosBeneficios.jasper";
		String pathSubRelPagamentosBeneficios = servletContext.getRealPath(arquivoPagamentosBeneficios);

		String arquivoPagamentosBeneficiosModularidades = pathArquivos + "/relPagamentosBeneficiosModularidades.jasper";
		String pathSubRelPagamentosBeneficiosModularidades = servletContext
						.getRealPath(arquivoPagamentosBeneficiosModularidades);

		List<PagamentosDTO> listaFornecTitulos = new ArrayList<PagamentosDTO>();
		List<PagamentosDTO> listaFornecModularidades = new ArrayList<PagamentosDTO>();
		List<PagamentosDTO> listaFornecOutrasModularidades = new ArrayList<PagamentosDTO>();
		PagamentosDTO pagamentosFornecedoresDTO = new PagamentosDTO();
		pagamentosFornecedoresDTO.setDsFtiTipoTitulo("N");
		pagamentosFornecedoresDTO.setDsModTipoServico("N");
		pagamentosFornecedoresDTO.setDsModTipoOutrosServico("N");
		String exibePgtoFornecedor = "N";

		List<ServicosModalidadesDTO> listaServModalidades = new ArrayList<ServicosModalidadesDTO>();
		ServicosModalidadesDTO servicosModalidadesDTO = null;

		boolean achouCreditoConta = false;
		boolean achouBradesco = false;
		boolean achouOutros = false;
		boolean achouDOC = false;
		boolean achouTED = false;
		boolean achouDC = false;
		boolean achouOC = false;
		boolean achouDI = false;
		boolean achouOrdemPagamento = false;

		// Pagamento de fornecedores
		int exibeLinhaFornecedor = saidaSegundoContrato.getQtModalidadeFornecedor() / 2;
		pgtoFornecedor: for (int i = 0; i < saidaSegundoContrato.getQtModalidadeFornecedor(); i++) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico(saidaSegundoContrato.getCdServicoFornecedor().toUpperCase());
			servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato.getListModFornecedores().get(i).getDsModalidadeFornecedor());

			if (saidaSegundoContrato.getQtModalidadeFornecedor() % 2 == 0) {
				if (i + 1 == exibeLinhaFornecedor) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			} else {
				if (i == exibeLinhaFornecedor) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			}

			listaServModalidades.add(servicosModalidadesDTO);

			// Bloco de Servi�o de Pagamento de Fornecedores

			PagamentosDTO fornecTitulos = null;
			PagamentosDTO fornecModularidades = null;

			for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato.getListaServicoPagamento()) {
				if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("F")) {
					exibePgtoFornecedor = "S";

					try {
						BeanUtils.copyProperties(pagamentosFornecedoresDTO, ocorrencias);
						pagamentosFornecedoresDTO
										.setDsServicoTipoServico(saidaSegundoContrato.getCdServicoFornecedor());
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Busca pela Modalidade Cr�dito em conta
					if (!achouCreditoConta) {
						for (OcorrenciasCreditos ocorrenciasCreditos : saidaSegundoContrato.getListaCreditos()) {
							if (ocorrenciasCreditos.getDsCretipoServico().equalsIgnoreCase("F")) {
								achouCreditoConta = true;
								try {
									BeanUtils.copyProperties(pagamentosFornecedoresDTO, ocorrenciasCreditos);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								pagamentosFornecedoresDTO.setDsCretipoServico(saidaSegundoContrato
												.getListModFornecedores().get(i).getDsModalidadeFornecedor());
								pagamentosFornecedoresDTO.setPcCreMaximoInconsistente(ocorrenciasCreditos
												.getPcCreMaximoInconsistente().toString());
								pagamentosFornecedoresDTO.setQtCreMaximaInconsistencia(PgitUtil.formatarNumero(
												ocorrenciasCreditos.getQtCreMaximaInconsistencia(), true));
								pagamentosFornecedoresDTO.setVlCreMaximoFavorecidoNao(NumberUtils
												.formatDiferenteZero(ocorrenciasCreditos.getVlCreMaximoFavorecidoNao()));
								pagamentosFornecedoresDTO.setVlCreLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasCreditos
												.getVlCreLimiteDiario()));
								pagamentosFornecedoresDTO.setVlCreLimiteIndividual(NumberUtils
												.formatDiferenteZero(ocorrenciasCreditos.getVlCreLimiteIndividual()));
								pagamentosFornecedoresDTO.setQtCreDiaRepique(ocorrenciasCreditos.getQtCreDiaRepique()
												.toString());
								pagamentosFornecedoresDTO.setQtCreDiaFloating(ocorrenciasCreditos.getQtCreDiaFloating()
												.toString());

								continue pgtoFornecedor;
							}
						}
						achouCreditoConta = true;
						pagamentosFornecedoresDTO.setDsCretipoServico("N");
					}

					// Busca pela Modalidade Pagemanto de titulos (Bradesco / Outros)
					if (!achouBradesco || !achouOutros) {
						for (OcorrenciasTitulos ocorrenciasTitulos : saidaSegundoContrato.getListaTitulos()) {
							fornecTitulos = new PagamentosDTO();
							try {
								BeanUtils.copyProperties(fornecTitulos, ocorrenciasTitulos);
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							if (!achouBradesco) {
								if (ocorrenciasTitulos.getDsFtiTipoTitulo().equalsIgnoreCase("B")) {
									pagamentosFornecedoresDTO.setDsFtiTipoTitulo("S");
									fornecTitulos.setDsFtiTipoTitulo(saidaSegundoContrato.getListModFornecedores().get(
													i).getDsModalidadeFornecedor());

									fornecTitulos.setPcFtiMaximoInconsistente(ocorrenciasTitulos
													.getPcFtiMaximoInconsistente().toString());
									fornecTitulos.setQtFtiMaximaInconsistencia(PgitUtil.formatarNumero(
													ocorrenciasTitulos.getQtFtiMaximaInconsistencia(), true));
									fornecTitulos.setVlFtiMaximoFavorecidoNao(NumberUtils.formatDiferenteZero(ocorrenciasTitulos
													.getVlFtiMaximoFavorecidoNao()));
									fornecTitulos.setVlFtiLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasTitulos
													.getVlFtiLimiteDiario()));
									fornecTitulos.setVlFtiLimiteIndividual(NumberUtils.formatDiferenteZero(ocorrenciasTitulos
													.getVlFtiLimiteIndividual()));
									fornecTitulos.setQtFtiDiaFloating(ocorrenciasTitulos.getQtFtiDiaFloating()
													.toString());
									fornecTitulos
													.setQtFtiDiaRepique(ocorrenciasTitulos.getQtFtiDiaRepique()
																	.toString());

									listaFornecTitulos.add(fornecTitulos);

									achouBradesco = true;
									continue pgtoFornecedor;
								}
							}
							if (!achouOutros) {
								if (ocorrenciasTitulos.getDsFtiTipoTitulo().equalsIgnoreCase("O")) {
									pagamentosFornecedoresDTO.setDsFtiTipoTitulo("S");
									fornecTitulos.setDsFtiTipoTitulo(saidaSegundoContrato.getListModFornecedores().get(
													i).getDsModalidadeFornecedor());
									achouOutros = true;

									fornecTitulos.setPcFtiMaximoInconsistente(ocorrenciasTitulos
													.getPcFtiMaximoInconsistente().toString());
									fornecTitulos.setQtFtiMaximaInconsistencia(PgitUtil.formatarNumero(
													ocorrenciasTitulos.getQtFtiMaximaInconsistencia(), true));
									fornecTitulos.setVlFtiMaximoFavorecidoNao(NumberUtils.formatDiferenteZero(ocorrenciasTitulos
													.getVlFtiMaximoFavorecidoNao()));
									fornecTitulos.setVlFtiLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasTitulos
													.getVlFtiLimiteDiario()));
									fornecTitulos.setVlFtiLimiteIndividual(NumberUtils.formatDiferenteZero(ocorrenciasTitulos
													.getVlFtiLimiteIndividual()));
									fornecTitulos.setQtFtiDiaFloating(ocorrenciasTitulos.getQtFtiDiaFloating()
													.toString());
									fornecTitulos
													.setQtFtiDiaRepique(ocorrenciasTitulos.getQtFtiDiaRepique()
																	.toString());

									listaFornecTitulos.add(fornecTitulos);

									continue pgtoFornecedor;
								}
							}

						}
						achouBradesco = true;
						achouOutros = true;
					}

					// Busca pela Modalidade Pagemanto de modularidades (DOC / TED / Debito em Conta)
					if (!achouDOC || !achouTED || !achouDC) {
						for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
										.getListaModularidades()) {
							if (ocorrenciasModularidades.getDsModTipoServico().equalsIgnoreCase("F")) {
								fornecModularidades = new PagamentosDTO();

								try {
									BeanUtils.copyProperties(fornecModularidades, ocorrenciasModularidades);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								if (!achouDOC) {
									if (ocorrenciasModularidades.getDsModTipoModalidade().equalsIgnoreCase("D")) {
										pagamentosFornecedoresDTO.setDsModTipoServico("S");
										fornecModularidades.setDsModTipoServico(saidaSegundoContrato
														.getListModFornecedores().get(i).getDsModalidadeFornecedor());

										fornecModularidades
														.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
																		.getCdModIndicadorPercentualMaximoInconsistente()
																		.toString());
										fornecModularidades.setQtModMaximoInconsistencia(PgitUtil.formatarNumero(
														ocorrenciasModularidades.getQtModMaximoInconsistencia(), true));
										fornecModularidades
														.setVlModMaximoFavorecidoNao(NumberUtils
																		.formatDiferenteZero(ocorrenciasModularidades
																						.getVlModMaximoFavorecidoNao()));
										fornecModularidades.setVlModLimiteDiario(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteDiario()));
										fornecModularidades.setVlModLimiteIndividual(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteIndividual()));
										fornecModularidades.setQtModDiaFloating(ocorrenciasModularidades
														.getQtModDiaFloating().toString());
										fornecModularidades.setQtModDiaRepique(ocorrenciasModularidades
														.getQtModDiaRepique().toString());

										listaFornecModularidades.add(fornecModularidades);
										achouDOC = true;
										continue pgtoFornecedor;
									}
								}
								if (!achouTED) {
									if (ocorrenciasModularidades.getDsModTipoModalidade().equalsIgnoreCase("T")) {
										pagamentosFornecedoresDTO.setDsModTipoServico("S");
										fornecModularidades.setDsModTipoServico(saidaSegundoContrato
														.getListModFornecedores().get(i).getDsModalidadeFornecedor());

										fornecModularidades
														.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
																		.getCdModIndicadorPercentualMaximoInconsistente()
																		.toString());
										fornecModularidades.setQtModMaximoInconsistencia(PgitUtil.formatarNumero(
														ocorrenciasModularidades.getQtModMaximoInconsistencia(), true));
										fornecModularidades
														.setVlModMaximoFavorecidoNao(NumberUtils
																		.formatDiferenteZero(ocorrenciasModularidades
																						.getVlModMaximoFavorecidoNao()));
										fornecModularidades.setVlModLimiteDiario(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteDiario()));
										fornecModularidades.setVlModLimiteIndividual(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteIndividual()));
										fornecModularidades.setQtModDiaFloating(ocorrenciasModularidades
														.getQtModDiaFloating().toString());
										fornecModularidades.setQtModDiaRepique(ocorrenciasModularidades
														.getQtModDiaRepique().toString());

										listaFornecModularidades.add(fornecModularidades);
										achouTED = true;
										continue pgtoFornecedor;
									}
								}
								if (!achouDC) {
									if (ocorrenciasModularidades.getDsModTipoModalidade().equalsIgnoreCase("C")) {
										pagamentosFornecedoresDTO.setDsModTipoServico("S");
										fornecModularidades.setDsModTipoServico(saidaSegundoContrato
														.getListModFornecedores().get(i).getDsModalidadeFornecedor());

										fornecModularidades
														.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
																		.getCdModIndicadorPercentualMaximoInconsistente()
																		.toString());
										fornecModularidades.setQtModMaximoInconsistencia(PgitUtil.formatarNumero(
														ocorrenciasModularidades.getQtModMaximoInconsistencia(), true));
										fornecModularidades
														.setVlModMaximoFavorecidoNao(NumberUtils
																		.formatDiferenteZero(ocorrenciasModularidades
																						.getVlModMaximoFavorecidoNao()));
										fornecModularidades.setVlModLimiteDiario(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteDiario()));
										fornecModularidades.setVlModLimiteIndividual(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteIndividual()));
										fornecModularidades.setQtModDiaFloating(ocorrenciasModularidades
														.getQtModDiaFloating().toString());
										fornecModularidades.setQtModDiaRepique(ocorrenciasModularidades
														.getQtModDiaRepique().toString());

										listaFornecModularidades.add(fornecModularidades);
										achouDC = true;
										continue pgtoFornecedor;
									}
								}
							}
						}
						achouDOC = true;
						achouTED = true;
						achouDC = true;
					}

					// Busca pela Modalidade Ordem de Pagamento
					if (!achouOrdemPagamento) {
						for (OcorrenciasOperacoes ocorrenciasOperacoes : saidaSegundoContrato.getListaOperacoes()) {
							if (ocorrenciasOperacoes.getDsOpgTipoServico().equalsIgnoreCase("F")) {
								achouOrdemPagamento = true;
								try {
									BeanUtils.copyProperties(pagamentosFornecedoresDTO, ocorrenciasOperacoes);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								pagamentosFornecedoresDTO.setDsOpgTipoServico(saidaSegundoContrato
												.getListModFornecedores().get(i).getDsModalidadeFornecedor());
								pagamentosFornecedoresDTO.setPcOpgMaximoInconsistencia(ocorrenciasOperacoes
												.getPcOpgMaximoInconsistencia().toString());
								pagamentosFornecedoresDTO.setQtOpgMaximoInconsistencia(PgitUtil.formatarNumero(
												ocorrenciasOperacoes.getQtOpgMaximoInconsistencia(), true));
								pagamentosFornecedoresDTO.setVlOpgLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasOperacoes
												.getVlOpgLimiteDiario()));
								pagamentosFornecedoresDTO.setVlOpgLimiteIndividual(NumberUtils
												.formatDiferenteZero(ocorrenciasOperacoes.getVlOpgLimiteIndividual()));
								pagamentosFornecedoresDTO.setQtOpgDiaFloating(ocorrenciasOperacoes
												.getQtOpgDiaFloating().toString());
								pagamentosFornecedoresDTO.setVlOpgMaximoFavorecidoNao(NumberUtils
												.formatDiferenteZero(ocorrenciasOperacoes.getVlOpgMaximoFavorecidoNao()));
								pagamentosFornecedoresDTO.setQtOpgDiaRepique(ocorrenciasOperacoes.getQtOpgDiaRepique()
												.toString());
								pagamentosFornecedoresDTO.setQtOpgDiaExpiracao(ocorrenciasOperacoes
												.getQtOpgDiaExpiracao().toString());

								continue pgtoFornecedor;
							}
						}
						achouOrdemPagamento = true;
						pagamentosFornecedoresDTO.setDsOpgTipoServico("N");
					}

					// Busca pela Modalidade Pagemanto de modularidades (Ordem de Credito / Deposito Identificado)
					if (!achouOC || !achouDI) {
						for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
										.getListaModularidades()) {
							if (ocorrenciasModularidades.getDsModTipoServico().equalsIgnoreCase("F")) {
								fornecModularidades = new PagamentosDTO();

								try {
									BeanUtils.copyProperties(fornecModularidades, ocorrenciasModularidades);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								if (!achouOC) {
									if (ocorrenciasModularidades.getDsModTipoModalidade().equalsIgnoreCase("O")) {
										pagamentosFornecedoresDTO.setDsModTipoOutrosServico("S");
										fornecModularidades.setDsModTipoServico(saidaSegundoContrato
														.getListModFornecedores().get(i).getDsModalidadeFornecedor());

										fornecModularidades
														.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
																		.getCdModIndicadorPercentualMaximoInconsistente()
																		.toString());
										fornecModularidades.setQtModMaximoInconsistencia(PgitUtil.formatarNumero(
														ocorrenciasModularidades.getQtModMaximoInconsistencia(), true));
										fornecModularidades
														.setVlModMaximoFavorecidoNao(NumberUtils
																		.formatDiferenteZero(ocorrenciasModularidades
																						.getVlModMaximoFavorecidoNao()));
										fornecModularidades.setVlModLimiteDiario(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteDiario()));
										fornecModularidades.setVlModLimiteIndividual(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteIndividual()));
										fornecModularidades.setQtModDiaFloating(ocorrenciasModularidades
														.getQtModDiaFloating().toString());
										fornecModularidades.setQtModDiaRepique(ocorrenciasModularidades
														.getQtModDiaRepique().toString());

										listaFornecOutrasModularidades.add(fornecModularidades);
										achouOC = true;
										continue pgtoFornecedor;
									}
								}

								if (!achouDI) {
									if (ocorrenciasModularidades.getDsModTipoModalidade().equalsIgnoreCase("I")) {
										pagamentosFornecedoresDTO.setDsModTipoOutrosServico("S");
										fornecModularidades.setDsModTipoServico(saidaSegundoContrato
														.getListModFornecedores().get(i).getDsModalidadeFornecedor());

										fornecModularidades
														.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
																		.getCdModIndicadorPercentualMaximoInconsistente()
																		.toString());
										fornecModularidades.setQtModMaximoInconsistencia(PgitUtil.formatarNumero(
														ocorrenciasModularidades.getQtModMaximoInconsistencia(), true));
										fornecModularidades
														.setVlModMaximoFavorecidoNao(NumberUtils
																		.formatDiferenteZero(ocorrenciasModularidades
																						.getVlModMaximoFavorecidoNao()));
										fornecModularidades.setVlModLimiteDiario(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteDiario()));
										fornecModularidades.setVlModLimiteIndividual(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteIndividual()));
										fornecModularidades.setQtModDiaFloating(ocorrenciasModularidades
														.getQtModDiaFloating().toString());
										fornecModularidades.setQtModDiaRepique(ocorrenciasModularidades
														.getQtModDiaRepique().toString());

										listaFornecOutrasModularidades.add(fornecModularidades);
										achouDI = true;
										continue pgtoFornecedor;
									}
								}
							}
						}
						achouOC = true;
						achouDI = true;
					}

					// Bloco Rastreamentos
					if (saidaSegundoContrato.getDsFraTipoRastreabilidade().trim().equals("")) {
						pagamentosFornecedoresDTO.setDsFraTipoRastreabilidade("N");
					} else {
						pagamentosFornecedoresDTO.setDsFraTipoRastreabilidade(saidaSegundoContrato
										.getDsFraTipoRastreabilidade());
						pagamentosFornecedoresDTO.setDsFraRastreabilidadeTerceiro(saidaSegundoContrato
										.getDsFraRastreabilidadeTerceiro());
						pagamentosFornecedoresDTO.setDsFraRastreabilidadeNota(saidaSegundoContrato
										.getDsFraRastreabilidadeNota());
						pagamentosFornecedoresDTO.setDsFraCaptacaoTitulo(saidaSegundoContrato.getDsFraCaptacaoTitulo());
						pagamentosFornecedoresDTO.setDsFraAgendaCliente(saidaSegundoContrato.getDsFraAgendaCliente());
						pagamentosFornecedoresDTO.setDsFraAgendaFilial(saidaSegundoContrato.getDsFraAgendaFilial());
						pagamentosFornecedoresDTO.setDsFraBloqueioEmissao(saidaSegundoContrato
										.getDsFraBloqueioEmissao());
						pagamentosFornecedoresDTO.setDtFraInicioBloqueio(saidaSegundoContrato.getDtFraInicioBloqueio());
						pagamentosFornecedoresDTO.setDtFraInicioRastreabilidade(saidaSegundoContrato
										.getDtFraInicioRastreabilidade());
						pagamentosFornecedoresDTO.setDsFraAdesaoSacado(saidaSegundoContrato.getDsFraAdesaoSacado());
					}
				}
			}
		}

		// Pagamento de tributos
		int exibeLinhaTributos = saidaSegundoContrato.getQtModalidadeTributos() / 2;
		String exibePgtoTributos = "N";
		PagamentosDTO pagamentosTributosDTO = new PagamentosDTO();
		PagamentosDTO tributosDTO = null;
		pagamentosTributosDTO.setDsTibutoAgendadoDebitoVeicular(saidaSegundoContrato
						.getDsTibutoAgendadoDebitoVeicular());
		pagamentosTributosDTO.setDsTdvTipoConsultaProposta(saidaSegundoContrato.getDsTdvTipoConsultaProposta());
		pagamentosTributosDTO.setDsTdvTipoTratamentoValor(saidaSegundoContrato.getDsTdvTipoTratamentoValor());

		List<PagamentosDTO> listaPagamentosTri = new ArrayList<PagamentosDTO>();
		List<PagamentosDTO> listaTri = new ArrayList<PagamentosDTO>();

		boolean achouDV = false;
		boolean achouGR = false;
		boolean achouDR = false;
		boolean achouGP = false;
		boolean achouCB = false;

		pgtoTributos: for (int i = 0; i < saidaSegundoContrato.getQtModalidadeTributos(); i++) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico(saidaSegundoContrato.getCdServicoTributos().toUpperCase());
			servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato.getListaModTributos().get(i)
							.getCdModalidadeTributos());

			if (saidaSegundoContrato.getQtModalidadeTributos() % 2 == 0) {
				if (i + 1 == exibeLinhaTributos) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			} else {
				if (i == exibeLinhaTributos) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			}

			listaServModalidades.add(servicosModalidadesDTO);

			// Bloco de Servi�o de Pagamento de Tributos
			for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato.getListaServicoPagamento()) {
				if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("T")) {
					exibePgtoTributos = "S";
					try {
						BeanUtils.copyProperties(pagamentosTributosDTO, ocorrencias);
						pagamentosTributosDTO.setDsServicoTipoServico(saidaSegundoContrato.getCdServicoTributos());
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Busca pela Modalidades (Debito de Veiculos / GARE / DARF / GPS / C�digo de Barras)
					if (!achouDV || !achouGR || !achouDR || !achouGP || !achouCB) {
						for (OcorrenciasTriTributo ocorrenciasTriTributo : saidaSegundoContrato.getListaTriTributos()) {
							tributosDTO = new PagamentosDTO();
							try {
								BeanUtils.copyProperties(tributosDTO, ocorrenciasTriTributo);
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							if (!achouDV) {
								if (ocorrenciasTriTributo.getDsTriTipoTributo().equalsIgnoreCase("V")) {
									tributosDTO.setExibeDebitoVeiculo("S");

									tributosDTO.setDsTriTipoTributo(saidaSegundoContrato.getListaModTributos().get(i)
													.getCdModalidadeTributos());

									tributosDTO.setDsTdvTipoTratamentoValor(saidaSegundoContrato
													.getDsTdvTipoTratamentoValor());
									tributosDTO.setDsTdvTipoConsultaProposta(saidaSegundoContrato
													.getDsTdvTipoConsultaProposta());
									tributosDTO.setVlTriMaximoFavorecidoNao(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriMaximoFavorecidoNao()));
									tributosDTO.setVlTriLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriLimiteDiario()));
									tributosDTO.setVlTriLimiteIndividual(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriLimiteIndividual()));
									tributosDTO.setQttriDiaFloating(ocorrenciasTriTributo.getQttriDiaFloating()
													.toString());
									tributosDTO.setQtTriDiaRepique(ocorrenciasTriTributo.getQtTriDiaRepique()
													.toString());

									listaTri.add(tributosDTO);

									achouDV = true;
									continue pgtoTributos;
								}
							}
							if (!achouGR) {
								if (ocorrenciasTriTributo.getDsTriTipoTributo().equalsIgnoreCase("G")) {
									tributosDTO.setExibeDebitoVeiculo("N");
									tributosDTO.setDsTriTipoTributo(saidaSegundoContrato.getListaModTributos().get(i)
													.getCdModalidadeTributos());

									tributosDTO.setVlTriMaximoFavorecidoNao(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriMaximoFavorecidoNao()));
									tributosDTO.setVlTriLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriLimiteDiario()));
									tributosDTO.setVlTriLimiteIndividual(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriLimiteIndividual()));
									tributosDTO.setQttriDiaFloating(ocorrenciasTriTributo.getQttriDiaFloating()
													.toString());
									tributosDTO.setQtTriDiaRepique(ocorrenciasTriTributo.getQtTriDiaRepique()
													.toString());

									listaTri.add(tributosDTO);

									achouGR = true;
									continue pgtoTributos;
								}
							}
							if (!achouDR) {
								if (ocorrenciasTriTributo.getDsTriTipoTributo().equalsIgnoreCase("D")) {
									tributosDTO.setExibeDebitoVeiculo("N");
									tributosDTO.setDsTriTipoTributo(saidaSegundoContrato.getListaModTributos().get(i)
													.getCdModalidadeTributos());

									tributosDTO.setVlTriMaximoFavorecidoNao(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriMaximoFavorecidoNao()));
									tributosDTO.setVlTriLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriLimiteDiario()));
									tributosDTO.setVlTriLimiteIndividual(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriLimiteIndividual()));
									tributosDTO.setQttriDiaFloating(ocorrenciasTriTributo.getQttriDiaFloating()
													.toString());
									tributosDTO.setQtTriDiaRepique(ocorrenciasTriTributo.getQtTriDiaRepique()
													.toString());

									listaTri.add(tributosDTO);

									achouDR = true;
									continue pgtoTributos;
								}
							}

							if (!achouGP) {
								if (ocorrenciasTriTributo.getDsTriTipoTributo().equalsIgnoreCase("P")) {
									tributosDTO.setExibeDebitoVeiculo("N");
									tributosDTO.setDsTriTipoTributo(saidaSegundoContrato.getListaModTributos().get(i)
													.getCdModalidadeTributos());

									tributosDTO.setVlTriMaximoFavorecidoNao(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriMaximoFavorecidoNao()));
									tributosDTO.setVlTriLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriLimiteDiario()));
									tributosDTO.setVlTriLimiteIndividual(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriLimiteIndividual()));
									tributosDTO.setQttriDiaFloating(ocorrenciasTriTributo.getQttriDiaFloating()
													.toString());
									tributosDTO.setQtTriDiaRepique(ocorrenciasTriTributo.getQtTriDiaRepique()
													.toString());

									listaTri.add(tributosDTO);

									achouGP = true;
									continue pgtoTributos;
								}
							}
							if (!achouCB) {
								if (ocorrenciasTriTributo.getDsTriTipoTributo().equalsIgnoreCase("B")) {
									tributosDTO.setExibeDebitoVeiculo("N");
									tributosDTO.setDsTriTipoTributo(saidaSegundoContrato.getListaModTributos().get(i)
													.getCdModalidadeTributos());

									tributosDTO.setVlTriMaximoFavorecidoNao(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriMaximoFavorecidoNao()));
									tributosDTO.setVlTriLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriLimiteDiario()));
									tributosDTO.setVlTriLimiteIndividual(NumberUtils.formatDiferenteZero(ocorrenciasTriTributo
													.getVlTriLimiteIndividual()));
									tributosDTO.setQttriDiaFloating(ocorrenciasTriTributo.getQttriDiaFloating()
													.toString());
									tributosDTO.setQtTriDiaRepique(ocorrenciasTriTributo.getQtTriDiaRepique()
													.toString());

									listaTri.add(tributosDTO);

									achouCB = true;
									continue pgtoTributos;
								}
							}
						}
					}

				}
			}
		}
		listaPagamentosTri.add(pagamentosTributosDTO);

		// Pagamento de sal�rios
		String exibePgtoSalarios = "N";
		PagamentosDTO pagamentosSalariosDTO = new PagamentosDTO();
		pagamentosSalariosDTO.setDsSalarioEmissaoAntecipadoCartao(saidaSegundoContrato
						.getDsSalarioEmissaoAntecipadoCartao());
		pagamentosSalariosDTO.setCdSalarioQuantidadeLimiteSolicitacaoCartao(saidaSegundoContrato
						.getCdSalarioQuantidadeLimiteSolicitacaoCartao().toString());
		pagamentosSalariosDTO.setDsSalarioAberturaContaExpressa(saidaSegundoContrato
						.getDsSalarioAberturaContaExpressa());

		List<PagamentosDTO> listaSalariosModularidades = new ArrayList<PagamentosDTO>();

		achouCreditoConta = false;
		achouDOC = false;
		achouTED = false;
		achouOrdemPagamento = false;

		int exibeLinhaSalario = saidaSegundoContrato.getQtModalidadeSalario() / 2;
		pgtoSalarios: for (int i = 0; i < saidaSegundoContrato.getQtModalidadeSalario(); i++) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico(saidaSegundoContrato.getCdServicoSalarial().toUpperCase());
			servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato.getListaModSalario().get(i)
							.getCdModalidadeSalarial());

			if (saidaSegundoContrato.getQtModalidadeSalario() % 2 == 0) {
				if (i + 1 == exibeLinhaSalario) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			} else {
				if (i == exibeLinhaSalario) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			}

			listaServModalidades.add(servicosModalidadesDTO);

			// Bloco de Servi�o de Pagamento de Sal�rios
			PagamentosDTO salariosModularidades = null;
			for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato.getListaServicoPagamento()) {
				if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("S")) {
					exibePgtoSalarios = "S";
					try {
						BeanUtils.copyProperties(pagamentosSalariosDTO, ocorrencias);
						pagamentosSalariosDTO.setDsServicoTipoServico(saidaSegundoContrato.getCdServicoSalarial());
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Busca pela Modalidade Cr�dito em conta
					if (!achouCreditoConta) {
						for (OcorrenciasCreditos ocorrenciasCreditos : saidaSegundoContrato.getListaCreditos()) {
							if (ocorrenciasCreditos.getDsCretipoServico().equalsIgnoreCase("S")) {
								achouCreditoConta = true;
								try {
									BeanUtils.copyProperties(pagamentosSalariosDTO, ocorrenciasCreditos);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								pagamentosSalariosDTO.setDsCretipoServico(saidaSegundoContrato.getListaModSalario()
												.get(i).getCdModalidadeSalarial());
								pagamentosSalariosDTO.setPcCreMaximoInconsistente(ocorrenciasCreditos
												.getPcCreMaximoInconsistente().toString());
								pagamentosSalariosDTO.setQtCreMaximaInconsistencia(PgitUtil.formatarNumero(
												ocorrenciasCreditos.getQtCreMaximaInconsistencia(), true));
								pagamentosSalariosDTO.setVlCreMaximoFavorecidoNao(NumberUtils
												.formatDiferenteZero(ocorrenciasCreditos
												.getVlCreMaximoFavorecidoNao()));
								pagamentosSalariosDTO.setVlCreLimiteDiario(NumberUtils
												.formatDiferenteZero(ocorrenciasCreditos.getVlCreLimiteDiario()
												));
								pagamentosSalariosDTO.setVlCreLimiteIndividual(NumberUtils
												.formatDiferenteZero(ocorrenciasCreditos
												.getVlCreLimiteIndividual()));
								pagamentosSalariosDTO.setQtCreDiaRepique(ocorrenciasCreditos.getQtCreDiaRepique()
												.toString());
								pagamentosSalariosDTO.setQtCreDiaFloating(ocorrenciasCreditos.getQtCreDiaFloating()
												.toString());

								continue pgtoSalarios;
							}
						}
						achouCreditoConta = true;
						pagamentosSalariosDTO.setDsCretipoServico("N");
					}

					// Busca pela Modalidade Pagemanto de modularidades (DOC / TED)
					if (!achouDOC || !achouTED) {
						for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
										.getListaModularidades()) {
							if (ocorrenciasModularidades.getDsModTipoServico().equalsIgnoreCase("S")) {
								salariosModularidades = new PagamentosDTO();

								try {
									BeanUtils.copyProperties(salariosModularidades, ocorrenciasModularidades);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								if (!achouDOC) {
									if (ocorrenciasModularidades.getDsModTipoModalidade().equalsIgnoreCase("D")) {
										pagamentosSalariosDTO.setDsModTipoServico("S");
										salariosModularidades.setDsModTipoServico(saidaSegundoContrato
														.getListaModSalario().get(i).getCdModalidadeSalarial());

										salariosModularidades
														.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
																		.getCdModIndicadorPercentualMaximoInconsistente()
																		.toString());
										salariosModularidades.setQtModMaximoInconsistencia(PgitUtil.formatarNumero(
														ocorrenciasModularidades.getQtModMaximoInconsistencia(), true));
										salariosModularidades
														.setVlModMaximoFavorecidoNao(NumberUtils
																		.formatDiferenteZero(ocorrenciasModularidades
																						.getVlModMaximoFavorecidoNao()));
										salariosModularidades.setVlModLimiteDiario(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteDiario()));
										salariosModularidades.setVlModLimiteIndividual(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteIndividual()));
										salariosModularidades.setQtModDiaFloating(ocorrenciasModularidades
														.getQtModDiaFloating().toString());
										salariosModularidades.setQtModDiaRepique(ocorrenciasModularidades
														.getQtModDiaRepique().toString());

										listaSalariosModularidades.add(salariosModularidades);
										achouDOC = true;
										continue pgtoSalarios;
									}
								}
								if (!achouTED) {
									if (ocorrenciasModularidades.getDsModTipoModalidade().equalsIgnoreCase("T")) {
										pagamentosSalariosDTO.setDsModTipoServico("S");
										salariosModularidades.setDsModTipoServico(saidaSegundoContrato
														.getListaModSalario().get(i).getCdModalidadeSalarial());

										salariosModularidades
														.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
																		.getCdModIndicadorPercentualMaximoInconsistente()
																		.toString());
										salariosModularidades.setQtModMaximoInconsistencia(PgitUtil.formatarNumero(
														ocorrenciasModularidades.getQtModMaximoInconsistencia(), true));
										salariosModularidades
														.setVlModMaximoFavorecidoNao(NumberUtils
																		.formatDiferenteZero(ocorrenciasModularidades
																						.getVlModMaximoFavorecidoNao()));
										salariosModularidades.setVlModLimiteDiario(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteDiario()));
										salariosModularidades.setVlModLimiteIndividual(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteIndividual()));
										salariosModularidades.setQtModDiaFloating(ocorrenciasModularidades
														.getQtModDiaFloating().toString());
										salariosModularidades.setQtModDiaRepique(ocorrenciasModularidades
														.getQtModDiaRepique().toString());

										listaSalariosModularidades.add(salariosModularidades);
										achouTED = true;
										continue pgtoSalarios;
									}
								}
							}
						}
						achouDOC = true;
						achouTED = true;
						pagamentosSalariosDTO.setDsModTipoServico("N");
					}

					// Busca pela Modalidade Ordem de Pagamento
					if (!achouOrdemPagamento) {
						for (OcorrenciasOperacoes ocorrenciasOperacoes : saidaSegundoContrato.getListaOperacoes()) {
							if (ocorrenciasOperacoes.getDsOpgTipoServico().equalsIgnoreCase("S")) {
								achouOrdemPagamento = true;
								try {
									BeanUtils.copyProperties(pagamentosSalariosDTO, ocorrenciasOperacoes);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								pagamentosSalariosDTO.setDsOpgTipoServico(saidaSegundoContrato.getListaModSalario()
												.get(i).getCdModalidadeSalarial());
								pagamentosSalariosDTO.setPcOpgMaximoInconsistencia(ocorrenciasOperacoes
												.getPcOpgMaximoInconsistencia().toString());
								pagamentosSalariosDTO.setQtOpgMaximoInconsistencia(PgitUtil.formatarNumero(
												ocorrenciasOperacoes.getQtOpgMaximoInconsistencia(), true));
								pagamentosSalariosDTO.setVlOpgLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasOperacoes
												.getVlOpgLimiteDiario()));
								pagamentosSalariosDTO.setVlOpgLimiteIndividual(NumberUtils.formatDiferenteZero(ocorrenciasOperacoes
												.getVlOpgLimiteIndividual()));
								pagamentosSalariosDTO.setQtOpgDiaFloating(ocorrenciasOperacoes.getQtOpgDiaFloating()
												.toString());
								pagamentosSalariosDTO.setVlOpgMaximoFavorecidoNao(NumberUtils
												.formatDiferenteZero(ocorrenciasOperacoes.getVlOpgMaximoFavorecidoNao()));
								pagamentosSalariosDTO.setQtOpgDiaRepique(ocorrenciasOperacoes.getQtOpgDiaRepique()
												.toString());
								pagamentosSalariosDTO.setQtOpgDiaExpiracao(ocorrenciasOperacoes.getQtOpgDiaExpiracao()
												.toString());

								continue pgtoSalarios;
							}
						}
						achouOrdemPagamento = true;
						pagamentosSalariosDTO.setDsOpgTipoServico("N");
					}
				}
			}
		}

		// Pagamento de benef�cios
		String exibePgtoBeneficios = "N";
		PagamentosDTO pagamentosBeneficiosDTO = new PagamentosDTO();
		pagamentosBeneficiosDTO.setDsBprTipoAcao(saidaSegundoContrato.getDsBprTipoAcao().trim().equals("") ? "N"
						: saidaSegundoContrato.getDsBprTipoAcao());
		pagamentosBeneficiosDTO.setDsBeneficioInformadoAgenciaConta(saidaSegundoContrato
						.getDsBeneficioInformadoAgenciaConta());
		pagamentosBeneficiosDTO.setDsBeneficioTipoIdentificacaoBeneficio(saidaSegundoContrato
						.getDsBeneficioTipoIdentificacaoBeneficio());
		pagamentosBeneficiosDTO.setDsBeneficioUtilizacaoCadastroOrganizacao(saidaSegundoContrato
						.getDsBeneficioUtilizacaoCadastroOrganizacao());
		pagamentosBeneficiosDTO.setDsBeneficioUtilizacaoCadastroProcuradores(saidaSegundoContrato
						.getDsBeneficioUtilizacaoCadastroProcuradores());
		pagamentosBeneficiosDTO.setDsBeneficioPossuiExpiracaoCredito(saidaSegundoContrato
						.getDsBeneficioPossuiExpiracaoCredito());
		pagamentosBeneficiosDTO.setCdBeneficioQuantidadeDiasExpiracaoCredito(saidaSegundoContrato
						.getCdBeneficioQuantidadeDiasExpiracaoCredito().toString());
		pagamentosBeneficiosDTO.setCdBeneficioQuantidadeDiasExpiracaoCredito(saidaSegundoContrato
						.getCdBeneficioQuantidadeDiasExpiracaoCredito().toString());
		pagamentosBeneficiosDTO.setQtBprMesProvisorio(saidaSegundoContrato.getQtBprMesProvisorio().toString());
		pagamentosBeneficiosDTO.setCdBprIndicadorEmissaoAviso(saidaSegundoContrato.getCdBprIndicadorEmissaoAviso());
		pagamentosBeneficiosDTO.setQtBprDiaAnteriorAviso(saidaSegundoContrato.getQtBprDiaAnteriorAviso().toString());
		pagamentosBeneficiosDTO.setQtBprDiaAnteriorVencimento(saidaSegundoContrato.getQtBprDiaAnteriorVencimento()
						.toString());
		pagamentosBeneficiosDTO.setCdBprUtilizadoMensagemPersonalizada(saidaSegundoContrato
						.getCdBprUtilizadoMensagemPersonalizada());
		pagamentosBeneficiosDTO.setDsBprDestino(saidaSegundoContrato.getDsBprDestino());
		pagamentosBeneficiosDTO.setDsBprCadastroEnderecoUtilizado(saidaSegundoContrato
						.getDsBprCadastroEnderecoUtilizado());

		List<PagamentosDTO> listaBeneficiosModularidades = new ArrayList<PagamentosDTO>();

		achouCreditoConta = false;
		achouDOC = false;
		achouTED = false;
		achouOrdemPagamento = false;

		int exibeLinhaBeneficio = saidaSegundoContrato.getQtModalidadeBeneficio() / 2;
		pgtoBeneficios: for (int i = 0; i < saidaSegundoContrato.getQtModalidadeBeneficio(); i++) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico(saidaSegundoContrato.getCdServicoBeneficio().toUpperCase());
			servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato.getListaModBeneficio().get(i)
							.getCdModalidadeBeneficio());

			if (saidaSegundoContrato.getQtModalidadeBeneficio() % 2 == 0) {
				if (i + 1 == exibeLinhaBeneficio) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			} else {
				if (i == exibeLinhaBeneficio) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			}

			listaServModalidades.add(servicosModalidadesDTO);

			// Bloco de Servi�o de Pagamento de Benef�cios
			PagamentosDTO beneficiosModularidades = null;
			for (OcorrenciasServicoPagamentos ocorrencias : saidaSegundoContrato.getListaServicoPagamento()) {
				if (ocorrencias.getDsServicoTipoServico().equalsIgnoreCase("B")) {
					exibePgtoBeneficios = "S";
					try {
						BeanUtils.copyProperties(pagamentosBeneficiosDTO, ocorrencias);
						pagamentosBeneficiosDTO.setDsServicoTipoServico(saidaSegundoContrato.getCdServicoBeneficio());
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Busca pela Modalidade Cr�dito em conta
					if (!achouCreditoConta) {
						for (OcorrenciasCreditos ocorrenciasCreditos : saidaSegundoContrato.getListaCreditos()) {
							if (ocorrenciasCreditos.getDsCretipoServico().equalsIgnoreCase("B")) {
								achouCreditoConta = true;
								try {
									BeanUtils.copyProperties(pagamentosBeneficiosDTO, ocorrenciasCreditos);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								pagamentosBeneficiosDTO.setDsCretipoServico(saidaSegundoContrato.getListaModBeneficio()
												.get(i).getCdModalidadeBeneficio());
								pagamentosBeneficiosDTO.setPcCreMaximoInconsistente(ocorrenciasCreditos
												.getPcCreMaximoInconsistente().toString());
								pagamentosBeneficiosDTO.setQtCreMaximaInconsistencia(PgitUtil.formatarNumero(
												ocorrenciasCreditos.getQtCreMaximaInconsistencia(), true));
								pagamentosBeneficiosDTO.setVlCreMaximoFavorecidoNao(NumberUtils
												.formatDiferenteZero(ocorrenciasCreditos.getVlCreMaximoFavorecidoNao()));
								pagamentosBeneficiosDTO.setVlCreLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasCreditos
												.getVlCreLimiteDiario()));
								pagamentosBeneficiosDTO.setVlCreLimiteIndividual(NumberUtils.formatDiferenteZero(ocorrenciasCreditos
												.getVlCreLimiteIndividual()));
								pagamentosBeneficiosDTO.setQtCreDiaRepique(ocorrenciasCreditos.getQtCreDiaRepique()
												.toString());
								pagamentosBeneficiosDTO.setQtCreDiaFloating(ocorrenciasCreditos.getQtCreDiaFloating()
												.toString());

								continue pgtoBeneficios;
							}
						}
						achouCreditoConta = true;
						pagamentosBeneficiosDTO.setDsCretipoServico("N");
					}

					// Busca pela Modalidade Pagemanto de modularidades (DOC / TED)
					if (!achouDOC || !achouTED) {
						for (OcorrenciasModularidades ocorrenciasModularidades : saidaSegundoContrato
										.getListaModularidades()) {
							if (ocorrenciasModularidades.getDsModTipoServico().equalsIgnoreCase("B")) {
								beneficiosModularidades = new PagamentosDTO();

								try {
									BeanUtils.copyProperties(beneficiosModularidades, ocorrenciasModularidades);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								if (!achouDOC) {
									if (ocorrenciasModularidades.getDsModTipoModalidade().equalsIgnoreCase("D")) {
										pagamentosBeneficiosDTO.setDsModTipoServico("S");

										beneficiosModularidades.setDsModTipoServico(saidaSegundoContrato
														.getListaModBeneficio().get(i).getCdModalidadeBeneficio());
										beneficiosModularidades
														.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
																		.getCdModIndicadorPercentualMaximoInconsistente()
																		.toString());
										beneficiosModularidades.setQtModMaximoInconsistencia(PgitUtil.formatarNumero(
														ocorrenciasModularidades.getQtModMaximoInconsistencia(), true));
										beneficiosModularidades
														.setVlModMaximoFavorecidoNao(NumberUtils
																		.formatDiferenteZero(ocorrenciasModularidades
																						.getVlModMaximoFavorecidoNao()));
										beneficiosModularidades.setVlModLimiteDiario(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteDiario()));
										beneficiosModularidades.setVlModLimiteIndividual(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteIndividual()));
										beneficiosModularidades.setQtModDiaFloating(ocorrenciasModularidades
														.getQtModDiaFloating().toString());
										beneficiosModularidades.setQtModDiaRepique(ocorrenciasModularidades
														.getQtModDiaRepique().toString());

										listaBeneficiosModularidades.add(beneficiosModularidades);
										achouDOC = true;
										continue pgtoBeneficios;
									}
								}
								if (!achouTED) {
									if (ocorrenciasModularidades.getDsModTipoModalidade().equalsIgnoreCase("T")) {
										pagamentosSalariosDTO.setDsModTipoServico("S");
										pagamentosBeneficiosDTO.setDsModTipoServico("S");

										beneficiosModularidades.setDsModTipoServico(saidaSegundoContrato
														.getListaModBeneficio().get(i).getCdModalidadeBeneficio());
										beneficiosModularidades
														.setCdModIndicadorPercentualMaximoInconsistente(ocorrenciasModularidades
																		.getCdModIndicadorPercentualMaximoInconsistente()
																		.toString());
										beneficiosModularidades.setQtModMaximoInconsistencia(PgitUtil.formatarNumero(
														ocorrenciasModularidades.getQtModMaximoInconsistencia(), true));
										beneficiosModularidades
														.setVlModMaximoFavorecidoNao(NumberUtils
																		.formatDiferenteZero(ocorrenciasModularidades
																						.getVlModMaximoFavorecidoNao()));
										beneficiosModularidades.setVlModLimiteDiario(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteDiario()));
										beneficiosModularidades.setVlModLimiteIndividual(NumberUtils
														.formatDiferenteZero(ocorrenciasModularidades.getVlModLimiteIndividual()));
										beneficiosModularidades.setQtModDiaFloating(ocorrenciasModularidades
														.getQtModDiaFloating().toString());
										beneficiosModularidades.setQtModDiaRepique(ocorrenciasModularidades
														.getQtModDiaRepique().toString());

										listaBeneficiosModularidades.add(beneficiosModularidades);
										achouTED = true;
										continue pgtoBeneficios;
									}
								}
							}
						}
						achouDOC = true;
						achouTED = true;
						pagamentosBeneficiosDTO.setDsModTipoServico("N");
					}

					// Busca pela Modalidade Ordem de Pagamento
					if (!achouOrdemPagamento) {
						for (OcorrenciasOperacoes ocorrenciasOperacoes : saidaSegundoContrato.getListaOperacoes()) {
							if (ocorrenciasOperacoes.getDsOpgTipoServico().equalsIgnoreCase("B")) {
								achouOrdemPagamento = true;
								try {
									BeanUtils.copyProperties(pagamentosBeneficiosDTO, ocorrenciasOperacoes);
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								pagamentosBeneficiosDTO.setDsOpgTipoServico(saidaSegundoContrato.getListaModBeneficio()
												.get(i).getCdModalidadeBeneficio());
								pagamentosBeneficiosDTO.setPcOpgMaximoInconsistencia(ocorrenciasOperacoes
												.getPcOpgMaximoInconsistencia().toString());
								pagamentosBeneficiosDTO.setQtOpgMaximoInconsistencia(PgitUtil.formatarNumero(
												ocorrenciasOperacoes.getQtOpgMaximoInconsistencia(), true));
								pagamentosBeneficiosDTO.setVlOpgLimiteDiario(NumberUtils.formatDiferenteZero(ocorrenciasOperacoes
												.getVlOpgLimiteDiario()));
								pagamentosBeneficiosDTO.setVlOpgLimiteIndividual(NumberUtils
												.formatDiferenteZero(ocorrenciasOperacoes.getVlOpgLimiteIndividual()));
								pagamentosBeneficiosDTO.setQtOpgDiaFloating(ocorrenciasOperacoes.getQtOpgDiaFloating()
												.toString());
								pagamentosBeneficiosDTO.setVlOpgMaximoFavorecidoNao(NumberUtils
												.formatDiferenteZero(ocorrenciasOperacoes.getVlOpgMaximoFavorecidoNao()));
								pagamentosBeneficiosDTO.setQtOpgDiaRepique(ocorrenciasOperacoes.getQtOpgDiaRepique()
												.toString());
								pagamentosBeneficiosDTO.setQtOpgDiaExpiracao(ocorrenciasOperacoes
												.getQtOpgDiaExpiracao().toString());

								continue pgtoBeneficios;
							}
						}
						achouOrdemPagamento = true;
						pagamentosBeneficiosDTO.setDsOpgTipoServico("N");
					}
				}
			}

		}

		// Emiss�o de aviso de movimenta��o
		if (saidaSegundoContrato.getCdIndicadorAviso().equalsIgnoreCase("S")) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico("AVISO DE MOVIMENTA��O");
			StringBuilder bufferMovimentacao = new StringBuilder();
			bufferMovimentacao.append("(");
			bufferMovimentacao.append(saidaSegundoContrato.getCdIndicadorAvisoPagador());
			bufferMovimentacao.append(")PAGADOR (");
			bufferMovimentacao.append(saidaSegundoContrato.getCdIndicadorAvisoFavorecido());
			bufferMovimentacao.append(")FAVORECIDO");
			servicosModalidadesDTO.setDsModalidade(bufferMovimentacao.toString());
			servicosModalidadesDTO.setImprimeLinhaServico("S");

			listaServModalidades.add(servicosModalidadesDTO);
		}

		// Emiss�o de comprovante de pagamento
		if (saidaSegundoContrato.getCdIndicadorComplemento().equalsIgnoreCase("S")) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico("COMPROVANTE DE PAGAMENTO");
			StringBuilder bufferComplemento = new StringBuilder();
			bufferComplemento.append("(");
			bufferComplemento.append(saidaSegundoContrato.getCdIndicadorComplementoPagador());
			bufferComplemento.append(")PAGADOR (");
			bufferComplemento.append(saidaSegundoContrato.getCdIndicadorComplementoFavorecido());
			bufferComplemento.append(")FAVORECIDO (");
			bufferComplemento.append(saidaSegundoContrato.getCdIndicadorComplementoSalarial());
			bufferComplemento.append(")SALARIAL (");
			bufferComplemento.append(saidaSegundoContrato.getCdIndicadorComplementoDivergente());
			bufferComplemento.append(")DIVERSOS");
			servicosModalidadesDTO.setDsModalidade(bufferComplemento.toString());
			servicosModalidadesDTO.setImprimeLinhaServico("S");

			listaServModalidades.add(servicosModalidadesDTO);
		}

		// Cadastro de Favorecidos
		StringBuilder bufferCadFavorecido = new StringBuilder();
		servicosModalidadesDTO = new ServicosModalidadesDTO();
		servicosModalidadesDTO.setDsServico("CADASTRO DE FAVORECIDOS");
		bufferCadFavorecido
						.append(saidaSegundoContrato.getCdIndcadorCadastroFavorecido().equalsIgnoreCase("S") ? "(X)SIM ()N�O"
										: "()SIM (X)N�O");
		servicosModalidadesDTO.setDsModalidade(bufferCadFavorecido.toString());
		servicosModalidadesDTO.setImprimeLinhaServico("S");

		listaServModalidades.add(servicosModalidadesDTO);

		// Recadastramento de Benefici�rios
		int exibeLinhaRecadastramento = saidaSegundoContrato.getQtModalidadeRecadastro() / 2;
		for (int i = 0; i < saidaSegundoContrato.getQtModalidadeRecadastro(); i++) {
			servicosModalidadesDTO = new ServicosModalidadesDTO();
			servicosModalidadesDTO.setDsServico(saidaSegundoContrato.getDsServicoRecadastro());
			servicosModalidadesDTO.setDsModalidade(saidaSegundoContrato.getListaModRecadastro().get(i)
							.getCdModalidadeRecadastro());
			if (saidaSegundoContrato.getQtModalidadeRecadastro() % 2 == 0) {
				if (i + 1 == exibeLinhaRecadastramento) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			} else {
				if (i == exibeLinhaRecadastramento) {
					servicosModalidadesDTO.setImprimeLinhaServico("S");
				}
			}

			listaServModalidades.add(servicosModalidadesDTO);
		}

		JRBeanCollectionDataSource beanParamServModalidades = new JRBeanCollectionDataSource(listaServModalidades);
		JRBeanCollectionDataSource beanParamFornecTitulos = new JRBeanCollectionDataSource(listaFornecTitulos);
		JRBeanCollectionDataSource beanParamFornecModularidades = new JRBeanCollectionDataSource(
						listaFornecModularidades);
		JRBeanCollectionDataSource beanParamFornecOutrasModularidades = new JRBeanCollectionDataSource(
						listaFornecOutrasModularidades);
		JRBeanCollectionDataSource beanParamPagamentosTributos = new JRBeanCollectionDataSource(listaPagamentosTri);
		JRBeanCollectionDataSource beanParamTributos = new JRBeanCollectionDataSource(listaTri);

		List<PagamentosDTO> listaPagamentosFornec = new ArrayList<PagamentosDTO>();
		listaPagamentosFornec.add(pagamentosFornecedoresDTO);
		JRBeanCollectionDataSource beanParamPagamentosFornecedores = new JRBeanCollectionDataSource(
						listaPagamentosFornec);

		JRBeanCollectionDataSource beanParamPagamentosFornecedoresCompl = new JRBeanCollectionDataSource(
						listaPagamentosFornec);

		List<PagamentosDTO> listaPagamentosSalarios = new ArrayList<PagamentosDTO>();
		listaPagamentosSalarios.add(pagamentosSalariosDTO);
		JRBeanCollectionDataSource beanParamPagamentosSalarios = new JRBeanCollectionDataSource(listaPagamentosSalarios);
		JRBeanCollectionDataSource beanParamSalariosModularidades = new JRBeanCollectionDataSource(
						listaSalariosModularidades);

		List<PagamentosDTO> listaPagamentosBeneficios = new ArrayList<PagamentosDTO>();
		listaPagamentosBeneficios.add(pagamentosBeneficiosDTO);
		JRBeanCollectionDataSource beanParamPagamentosBeneficios = new JRBeanCollectionDataSource(
						listaPagamentosBeneficios);
		JRBeanCollectionDataSource beanParamPagamentosBeneficiosModularidades = new JRBeanCollectionDataSource(
						listaBeneficiosModularidades);

		String servicos = pathArquivos + "/relServicos.jasper";
		String pathSubRelServicos = servletContext.getRealPath(servicos);

		String servicosCompl = pathArquivos + "/relServicosCompl.jasper";
		String pathSubRelServicosCompl = servletContext.getRealPath(servicosCompl);

		ServicosDTO servicosDTO = new ServicosDTO();
		boolean avisoFavorecido = false;
		boolean avisoPagador = false;
		boolean comprovanteFavorecido = false;
		boolean comprovantePagador = false;

		for (OcorrenciasAvisos ocorrenciasAvisos : saidaSegundoContrato.getListaAvisos()) {

			if (!ocorrenciasAvisos.getDsAvisoTipoAviso().trim().equals("")) {
				if (ocorrenciasAvisos.getDsAvisoTipoAviso().equalsIgnoreCase("F")) {
					avisoFavorecido = true;
					servicosDTO.setDsAvisoTipoAvisoFavorecido("SERVI�O: Aviso Movimenta��o Favorecido");

					servicosDTO.setDsAvisoPeriodicidadeEmissaoFavorecido(ocorrenciasAvisos
									.getDsAvisoPeriodicidadeEmissao());
					servicosDTO.setDsAvisoDestinoFavorecido(ocorrenciasAvisos.getDsAvisoDestino());
					servicosDTO.setDsAvisoPermissaoCorrespondenciaAberturaFavorecido(ocorrenciasAvisos
									.getDsAvisoPermissaoCorrespondenciaAbertura());
					servicosDTO.setDsAvisoDemontracaoInformacaoReservadaFavorecido(ocorrenciasAvisos
									.getDsAvisoDemontracaoInformacaoReservada());
					servicosDTO.setCdAvisoQuantidadeViasEmitirFavorecido(ocorrenciasAvisos
									.getCdAvisoQuantidadeViasEmitir().toString());
					servicosDTO.setDsAvisoAgrupamentoCorrespondenciaFavorecido(ocorrenciasAvisos
									.getDsAvisoAgrupamentoCorrespondencia());

				} else {
					avisoPagador = true;
					servicosDTO.setDsAvisoTipoAvisoPagador("SERVI�O: Aviso Movimenta��o Pagador");

					servicosDTO.setDsAvisoPeriodicidadeEmissaoPagador(ocorrenciasAvisos
									.getDsAvisoPeriodicidadeEmissao());
					servicosDTO.setDsAvisoDestinoPagador(ocorrenciasAvisos.getDsAvisoDestino());
					servicosDTO.setDsAvisoPermissaoCorrespondenciaAberturaPagador(ocorrenciasAvisos
									.getDsAvisoPermissaoCorrespondenciaAbertura());
					servicosDTO.setDsAvisoDemontracaoInformacaoReservadaPagador(ocorrenciasAvisos
									.getDsAvisoDemontracaoInformacaoReservada());
					servicosDTO.setCdAvisoQuantidadeViasEmitirPagador(ocorrenciasAvisos
									.getCdAvisoQuantidadeViasEmitir().toString());
					servicosDTO.setDsAvisoAgrupamentoCorrespondenciaPagador(ocorrenciasAvisos
									.getDsAvisoAgrupamentoCorrespondencia());
				}
			} else {
				if (!avisoFavorecido) {
					servicosDTO.setDsAvisoTipoAvisoFavorecido("N");
				}

				if (!avisoPagador) {
					servicosDTO.setDsAvisoTipoAvisoPagador("N");
				}
			}

		}

		for (OcorrenciasComprovantes ocorrenciasComprovantes : saidaSegundoContrato.getListaComprovantes()) {
			if (!comprovanteFavorecido || !comprovantePagador) {
				if (!ocorrenciasComprovantes.getCdComprovanteTipoComprovante().trim().equals("")) {
					if (ocorrenciasComprovantes.getCdComprovanteTipoComprovante().equalsIgnoreCase("F")) {
						comprovanteFavorecido = true;

						servicosDTO
										.setCdComprovanteTipoComprovanteFavorecido("SERVI�O: Comprovante Pagamento Favorecido");

						servicosDTO.setDsComprovantePeriodicidadeEmissaoFavorecido(ocorrenciasComprovantes
										.getDsComprovantePeriodicidadeEmissao());
						servicosDTO
										.setDsComprovanteDestinoFavorecido(ocorrenciasComprovantes
														.getDsComprovanteDestino());
						servicosDTO.setDsComprovantePermissaoCorrespondenteAberturaFavorecido(ocorrenciasComprovantes
										.getDsComprovantePermissaoCorrespondenteAbertura());
						servicosDTO.setDsDemonstrativoInformacaoReservadaFavorecido(ocorrenciasComprovantes
										.getDsDemonstrativoInformacaoReservada());
						servicosDTO.setQtViasEmitirFavorecido(ocorrenciasComprovantes.getQtViasEmitir().toString());
						servicosDTO.setDsAgrupamentoCorrespondenteFavorecido(ocorrenciasComprovantes
										.getDsAgrupamentoCorrespondente());
					} else {
						comprovantePagador = true;

						servicosDTO.setCdComprovanteTipoComprovantePagador("SERVI�O: Comprovante Pagamento Pagador");

						servicosDTO.setDsComprovantePeriodicidadeEmissaoPagador(ocorrenciasComprovantes
										.getDsComprovantePeriodicidadeEmissao());
						servicosDTO.setDsComprovanteDestinoPagador(ocorrenciasComprovantes.getDsComprovanteDestino());
						servicosDTO.setDsComprovantePermissaoCorrespondenteAberturaPagador(ocorrenciasComprovantes
										.getDsComprovantePermissaoCorrespondenteAbertura());
						servicosDTO.setDsDemonstrativoInformacaoReservadaPagador(ocorrenciasComprovantes
										.getDsDemonstrativoInformacaoReservada());
						servicosDTO.setQtViasEmitirPagador(ocorrenciasComprovantes.getQtViasEmitir().toString());
						servicosDTO.setDsAgrupamentoCorrespondentePagador(ocorrenciasComprovantes
										.getDsAgrupamentoCorrespondente());
					}
				} else {
					if (!comprovanteFavorecido) {
						servicosDTO.setCdComprovanteTipoComprovanteFavorecido("N");
					}

					if (!comprovantePagador) {
						servicosDTO.setCdComprovanteTipoComprovantePagador("N");
					}
				}
			}
		}

		boolean compSalarial = false;
		boolean compDiversos = false;
		for (OcorrenciasComprovanteSalariosDiversos ocorrenciasComprovanteSalariosDiversos : saidaSegundoContrato
						.getListaComprovanteSalarioDiversos()) {

			if (!compSalarial || !compDiversos) {
				if (!ocorrenciasComprovanteSalariosDiversos.getDsCsdTipoComprovante().trim().equals("")) {
					if (ocorrenciasComprovanteSalariosDiversos.getDsCsdTipoComprovante().equalsIgnoreCase("S")) {
						compSalarial = true;

						servicosDTO.setDsCsdTipoComprovanteSalarial("SERVI�O: Comprovante Salarial");

						servicosDTO.setDsCsdTipoRejeicaoLoteSalarial(ocorrenciasComprovanteSalariosDiversos
										.getDsCsdTipoRejeicaoLote());
						servicosDTO
										.setDsCsdMeioDisponibilizacaoCorrentistaSalarial(ocorrenciasComprovanteSalariosDiversos
														.getDsCsdMeioDisponibilizacaoCorrentista());
						servicosDTO
										.setDsCsdMediaDisponibilidadeCorrentistaSalarial(ocorrenciasComprovanteSalariosDiversos
														.getDsCsdMediaDisponibilidadeCorrentista());
						servicosDTO
										.setDsCsdMediaDisponibilidadeNumeroCorrentistasSalarial(ocorrenciasComprovanteSalariosDiversos
														.getDsCsdMediaDisponibilidadeNumeroCorrentistas());
						servicosDTO.setDsCsdDestinoSalarial(ocorrenciasComprovanteSalariosDiversos.getDsCsdDestino());
						servicosDTO.setQtCsdMesEmissaoSalarial(ocorrenciasComprovanteSalariosDiversos
										.getQtCsdMesEmissao().toString());
						servicosDTO.setDsCsdCadastroConservadorEnderecoSalarial(ocorrenciasComprovanteSalariosDiversos
										.getDsCsdCadastroConservadorEndereco());
						servicosDTO.setCdCsdQuantidadeViasEmitirSalarial(ocorrenciasComprovanteSalariosDiversos
										.getCdCsdQuantidadeViasEmitir() == 0 ? "N"
										: ocorrenciasComprovanteSalariosDiversos.getCdCsdQuantidadeViasEmitir()
														.toString());
					} else {
						compDiversos = true;

						servicosDTO.setDsCsdTipoComprovanteDiversos("SERVI�O: Comprovante Diversos");

						servicosDTO.setDsCsdTipoRejeicaoLoteDiversos(ocorrenciasComprovanteSalariosDiversos
										.getDsCsdTipoRejeicaoLote());
						servicosDTO
										.setDsCsdMeioDisponibilizacaoCorrentistaDiversos(ocorrenciasComprovanteSalariosDiversos
														.getDsCsdMeioDisponibilizacaoCorrentista());
						servicosDTO
										.setDsCsdMediaDisponibilidadeCorrentistaDiversos(ocorrenciasComprovanteSalariosDiversos
														.getDsCsdMediaDisponibilidadeCorrentista());
						servicosDTO
										.setDsCsdMediaDisponibilidadeNumeroCorrentistasDiversos(ocorrenciasComprovanteSalariosDiversos
														.getDsCsdMediaDisponibilidadeNumeroCorrentistas());
						servicosDTO.setDsCsdDestinoDiversos(ocorrenciasComprovanteSalariosDiversos.getDsCsdDestino());
						servicosDTO.setQtCsdMesEmissaoDiversos(ocorrenciasComprovanteSalariosDiversos
										.getQtCsdMesEmissao().toString());
						servicosDTO.setDsCsdCadastroConservadorEnderecoDiversos(ocorrenciasComprovanteSalariosDiversos
										.getDsCsdCadastroConservadorEndereco());
						servicosDTO.setCdCsdQuantidadeViasEmitirDiversos(ocorrenciasComprovanteSalariosDiversos
										.getCdCsdQuantidadeViasEmitir() == 0 ? "N"
										: ocorrenciasComprovanteSalariosDiversos.getCdCsdQuantidadeViasEmitir()
														.toString());
					}
				} else {
					if (!compSalarial) {
						servicosDTO.setDsCsdTipoComprovanteSalarial("N");
					}

					if (!compDiversos) {
						servicosDTO.setDsCsdTipoComprovanteDiversos("N");
					}
				}
			}
		}

		servicosDTO.setCdIndcadorCadastroFavorecido(saidaSegundoContrato.getCdIndcadorCadastroFavorecido()
						.equalsIgnoreCase("S") ? saidaSegundoContrato.getCdIndcadorCadastroFavorecido() : "N");

		servicosDTO.setDsCadastroFormaManutencao(saidaSegundoContrato.getDsCadastroFormaManutencao());
		servicosDTO.setCdCadastroQuantidadeDiasInativos(saidaSegundoContrato.getCdCadastroQuantidadeDiasInativos()
						.toString());
		servicosDTO.setDsCadastroTipoConsistenciaInscricao(saidaSegundoContrato
						.getDsCadastroTipoConsistenciaInscricao());

		servicosDTO.setDsServicoRecadastro(saidaSegundoContrato.getDsServicoRecadastro().trim().equals("") ? "N"
						: saidaSegundoContrato.getDsServicoRecadastro());
		servicosDTO.setDsRecTipoIdentificacaoBeneficio(saidaSegundoContrato.getDsRecTipoIdentificacaoBeneficio());
		servicosDTO.setDsRecTipoConsistenciaIdentificacao(saidaSegundoContrato.getDsRecTipoConsistenciaIdentificacao());
		servicosDTO.setDsRecCondicaoEnquadramento(saidaSegundoContrato.getDsRecCondicaoEnquadramento());
		servicosDTO.setDsRecCriterioPrincipalEnquadramento(saidaSegundoContrato
						.getDsRecCriterioPrincipalEnquadramento());
		servicosDTO.setDsRecTipoCriterioCompostoEnquadramento(saidaSegundoContrato
						.getDsRecTipoCriterioCompostoEnquadramento());
		servicosDTO.setQtRecEtapaRecadastramento(saidaSegundoContrato.getQtRecEtapaRecadastramento().toString());
		servicosDTO
						.setQtRecFaseEtapaRecadastramento(saidaSegundoContrato.getQtRecFaseEtapaRecadastramento()
										.toString());
		servicosDTO.setDtRecInicioRecadastramento(saidaSegundoContrato.getDtRecInicioRecadastramento());
		servicosDTO.setDtRecFinalRecadastramento(saidaSegundoContrato.getDtRecFinalRecadastramento());
		servicosDTO.setDsRecGeradorRetornoInternet(saidaSegundoContrato.getDsRecGeradorRetornoInternet());

		boolean aviso = false;
		boolean formulario = false;

		for (OcorrenciasAvisoFormularios ocorrenciasAvisoFormularios : saidaSegundoContrato.getListaAvisoFormularios()) {
			if (!aviso || !formulario) {
				if (!ocorrenciasAvisoFormularios.getDsRecTipoModalidade().trim().equals("")) {
					if (ocorrenciasAvisoFormularios.getDsRecTipoModalidade().equalsIgnoreCase("A")) {
						aviso = true;

						servicosDTO.setDsRecTipoModalidadeAviso("Aviso de Recadastramento");

						servicosDTO.setDsRecMomentoEnvioAviso(ocorrenciasAvisoFormularios.getDsRecMomentoEnvio());
						servicosDTO.setDsRecDestinoAviso(ocorrenciasAvisoFormularios.getDsRecDestino());
						servicosDTO.setDsRecCadastroEnderecoUtilizadoAviso(ocorrenciasAvisoFormularios
										.getDsRecCadastroEnderecoUtilizado());
						servicosDTO.setQtRecDiaAvisoAntecipadoAviso(ocorrenciasAvisoFormularios
										.getQtRecDiaAvisoAntecipado().toString());
						servicosDTO.setDsRecPermissaoAgrupamentoAviso(ocorrenciasAvisoFormularios
										.getDsRecPermissaoAgrupamento());
					} else {
						formulario = true;

						servicosDTO.setDsRecTipoModalidadeFormulario("Formul�rio de Recadastramento");

						servicosDTO.setDsRecMomentoEnvioFormulario(ocorrenciasAvisoFormularios.getDsRecMomentoEnvio());
						servicosDTO.setDsRecDestinoFormulario(ocorrenciasAvisoFormularios.getDsRecDestino());
						servicosDTO.setDsRecCadastroEnderecoUtilizadoFormulario(ocorrenciasAvisoFormularios
										.getDsRecCadastroEnderecoUtilizado());
						servicosDTO.setQtRecDiaAntecipadoFormulario(ocorrenciasAvisoFormularios
										.getQtRecDiaAvisoAntecipado().toString());
						servicosDTO.setDsRecPermissaoAgrupamentoFormulario(ocorrenciasAvisoFormularios
										.getDsRecPermissaoAgrupamento());
					}
				} else {
					if (!aviso) {
						servicosDTO.setDsRecTipoModalidadeAviso("N");
					}

					if (!formulario) {
						servicosDTO.setDsRecTipoModalidadeFormulario("N");
					}
				}
			}
		}

		List<ServicosDTO> listaServicos = new ArrayList<ServicosDTO>();
		listaServicos.add(servicosDTO);
		JRBeanCollectionDataSource beanParamServicos = new JRBeanCollectionDataSource(listaServicos);
		JRBeanCollectionDataSource beanParamServicosCompl = new JRBeanCollectionDataSource(listaServicos);

		// Par�metros do Relat�rio
		Map<String, Object> par = new HashMap<String, Object>();
		par.put("exibePgtoFornecedor", exibePgtoFornecedor);
		par.put("exibePgtoTributos", exibePgtoTributos);
		par.put("exibePgtoSalarios", exibePgtoSalarios);
		par.put("exibePgtoBeneficios", exibePgtoBeneficios);

		par.put("data", formatoData.format(new Date()));
		par.put("hora", formatoHora.format(new Date()));
		par.put("logoBradesco", logoPath);
		par.put("participantes", pathSubRelParticipantes);
		par.put("contas", pathSubRelContas);
		par.put("layouts", pathSubRelLayouts);
		par.put("servModal", pathSubServModalidades);
		par.put("pagamentosFornecedores", pathSubRelPagamentosFornecedores);
		par.put("pagamentosFornecedoresTitulos", pathSubRelPagamentosFornecedoresTitulos);
		par.put("pagamentosFornecedoresModularidades", pathSubRelPagamentosFornecedoresModularidades);
		par.put("pagamentosFornecedoresOutrasModularidades", pathSubRelPagamentosFornecedoresOutrasModularidades);
		par.put("pagamentosFornecedoresCompl", pathSubRelPagamentosFornecedoresCompl);
		par.put("pagamentosTributos", pathSubRelPagamentosTributos);
		par.put("tributos", pathSubRelTributos);
		par.put("pagamentosSalarios", pathSubRelPagamentosSalarios);
		par.put("pagamentosSalariosModularidades", pathSubRelPagamentosSalariosModularidades);
		par.put("pagamentosBeneficios", pathSubRelPagamentosBeneficios);
		par.put("pagamentosBeneficiosModularidades", pathSubRelPagamentosBeneficiosModularidades);
		par.put("servicos", pathSubRelServicos);
		par.put("servicosCompl", pathSubRelServicosCompl);
		par.put("listaParticipantes", beanParamParticipantes);
		par.put("listaContas", beanParamContas);
		par.put("listaLayouts", beanParamLayout);
		par.put("listaServModal", beanParamServModalidades);
		par.put("listaPagamentosFornecedores", beanParamPagamentosFornecedores);
		par.put("listaFornecTitulos", beanParamFornecTitulos);
		par.put("listaFornecModularidades", beanParamFornecModularidades);
		par.put("listaFornecOutrasModularidades", beanParamFornecOutrasModularidades);
		par.put("listaPagamentosFornecedoresCompl", beanParamPagamentosFornecedoresCompl);
		par.put("listaPagamentosTributos", beanParamPagamentosTributos);
		par.put("listaTributos", beanParamTributos);
		par.put("listaPagamentosSalarios", beanParamPagamentosSalarios);
		par.put("listaSalariosModularidades", beanParamSalariosModularidades);
		par.put("listaPagamentosBeneficios", beanParamPagamentosBeneficios);
		par.put("listaPagamentosBeneficiosModularidades", beanParamPagamentosBeneficiosModularidades);
		par.put("listaServicos", beanParamServicos);
		par.put("listaServicosCompl", beanParamServicosCompl);
		par.put("titulo", "ANEXO I - PAGAMENTO INTEGRADO BRADESCO");

		// getImprimirService().imprimirArquivo(FacesUtils.getServletResponse().getOutputStream(), protocolo);

		// PgitFacesUtils.generateReportResponse("contrato");

		try {

			// M�todo que gera o relat�rio PDF.
			PgitUtil.geraRelatorioPdf("/relatorios/manutencaoContrato/manterContrato/anexoContrato/",
							"imprimirAnexoContrato", listaAnexoContrato, par);

		} /* Exce��o do relat�rio */
		catch (Exception e) {
			throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
		}

	}

	/**
	 * Imprimir relatorio servicos.
	 *
	 * @return the string
	 */
	public String imprimirRelatorioServicos() {

		String caminho = "/images/Bradesco_logo.JPG";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
		String logoPath = servletContext.getRealPath(caminho);

		// Par�metros do Relat�rio
		Map<String, Object> par = new HashMap<String, Object>();

		par.put("tipo", getTipo());
		par.put("empresa", getEmpresa());
		par.put("numero", getNumero());
		par.put("descContrato", getDescricaoContrato());
		par.put("logoBradesco", logoPath);

		par.put("titulo", "Pagamento Integrado - Servi�os");

		try {

			// M�todo que gera o relat�rio PDF.
			PgitUtil.geraRelatorioPdf("/relatorios/manutencaoContrato/manterContrato/servicos", "imprimirServicos",
							manterContratoServicosBean.getListaGridPesquisa(), par);

		} /* Exce��o do relat�rio */
		catch (Exception e) {
			throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
		}

		return "";
	}

	/**
	 * Imprimir relatorios tarifas.
	 *
	 * @return the string
	 */
	public String imprimirRelatoriosTarifas() {

		String caminho = "/images/Bradesco_logo.JPG";
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
		String logoPath = servletContext.getRealPath(caminho);

		// Par�metros do Relat�rio
		Map<String, Object> par = new HashMap<String, Object>();

		par.put("tipo", manterContratoTarifasBean.getTipoContrato());
		par.put("empresa", manterContratoTarifasBean.getEmpresaContrato());
		par.put("numero", manterContratoTarifasBean.getNumeroContrato());
		par.put("descContrato", manterContratoTarifasBean.getDescricaoContrato());
		par.put("logoBradesco", logoPath);

		par.put("titulo", "Pagamento Integrado - Tarifas");

		try {

			// M�todo que gera o relat�rio PDF.
			PgitUtil.geraRelatorioPdf("/relatorios/manutencaoContrato/manterContrato/tarifas", "imprimirTarifas",
							manterContratoTarifasBean.getListaGridPesquisa(), par);

		} /* Exce��o do relat�rio */
		catch (Exception e) {
			throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
		}

		return "";
	}
	
	/**
	 * Limpar dados argumentos pesquisa.
	 */
	public void limparDadosArgumentosPesquisa(){
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(0l);
		identificacaoClienteContratoBean.setTipoContratoFiltro(0);
		identificacaoClienteContratoBean.setNumeroFiltro("");
		identificacaoClienteContratoBean.setAgenciaOperadoraFiltro("");
		identificacaoClienteContratoBean.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		identificacaoClienteContratoBean.setCodigoFunc("");
		identificacaoClienteContratoBean.setSituacaoFiltro(0);		
		identificacaoClienteContratoBean.listarMotivoSituacao();
		identificacaoClienteContratoBean.setHabilitaCampos(false);
		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean.setListaGridPesquisa(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		identificacaoClienteContratoBean.setEmpresaGestoraContrato(null);
		setNumeroContrato(null);
		setDescricaoContrato(null);
		identificacaoClienteContratoBean.setSituacaoFiltro(null);
		identificacaoClienteContratoBean.limparRadioFiltro();
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(false);
	}
	
	/**
	 * Consultar contrato.
	 *
	 * @return the string
	 */
	public String consultarContrato(){
		if (identificacaoClienteContratoBean.getObrigatoriedade() != null && identificacaoClienteContratoBean.getObrigatoriedade().equals("T")){

			// desmarca o radio da Grid - vreghini - 01/7/2010.
			identificacaoClienteContratoBean.setItemSelecionadoLista(null);
			identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);

			try{
				ListarContratosPgitEntradaDTO entradaDTO = new ListarContratosPgitEntradaDTO();

				entradaDTO.setCdAgencia(identificacaoClienteContratoBean.getAgenciaOperadoraFiltro()!=null&&!identificacaoClienteContratoBean.getAgenciaOperadoraFiltro().equals("")?Integer.parseInt(identificacaoClienteContratoBean.getAgenciaOperadoraFiltro()):0);
				entradaDTO.setCdClubPessoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdClub()!=null?identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdClub():0L);
				entradaDTO.setCdFuncionario(identificacaoClienteContratoBean.getCodigoFunc()!=null&&!identificacaoClienteContratoBean.getCodigoFunc().equals("")?Long.parseLong(identificacaoClienteContratoBean.getCodigoFunc()):0L);				
				entradaDTO.setCdPessoaJuridica(identificacaoClienteContratoBean.getEmpresaGestoraFiltro()!=null?identificacaoClienteContratoBean.getEmpresaGestoraFiltro():0);
				entradaDTO.setCdSituacaoContrato(identificacaoClienteContratoBean.getSituacaoFiltro() != null ? identificacaoClienteContratoBean.getSituacaoFiltro() : 0);
				entradaDTO.setCdTipoContratoNegocio(identificacaoClienteContratoBean.getTipoContratoFiltro()!=null?identificacaoClienteContratoBean.getTipoContratoFiltro():0);
				entradaDTO.setNrSequenciaContrato(identificacaoClienteContratoBean.getNumeroFiltro()!=null&&!identificacaoClienteContratoBean.getNumeroFiltro().equals("")?Long.valueOf(identificacaoClienteContratoBean.getNumeroFiltro()):Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdCpfCnpj());
				entradaDTO.setCdFilialCnpjPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdFilialCnpj());
				entradaDTO.setCdControleCpfPssoa(identificacaoClienteContratoBean.getSaidaConsultarListaClientePessoas().getCdControleCnpj());
				
				if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conManterSolManutContrato")) {
					entradaDTO.setParametroPrograma(1);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conManterPendenciasContrato")) {
					entradaDTO.setParametroPrograma(2);
				} else if(identificacaoClienteContratoBean.getPaginaRetorno().equals("conRecuperarContratoEncerrado")) {
					entradaDTO.setParametroPrograma(3);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("pesqContratoPendenteAssinatura")) {
					entradaDTO.setParametroPrograma(4);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conEncerrarContrato")) {
					entradaDTO.setParametroPrograma(5);
				} else if (identificacaoClienteContratoBean.getPaginaRetorno().equals("pesqManterSolicitacoesMudancaAmbienteOperacao")) {
					entradaDTO.setParametroPrograma(6);
				} else {
					entradaDTO.setParametroPrograma(0);
				}

				if (identificacaoClienteContratoBean.getPaginaRetorno().equals("conEncerrarContrato")){
					List<ListarContratosPgitSaidaDTO> listaGridTemp = identificacaoClienteContratoBean.getFiltroIdentificaoService().pesquisarManutencaoContrato(entradaDTO);

					identificacaoClienteContratoBean.setListaGridPesquisa(new ArrayList<ListarContratosPgitSaidaDTO>());

					for (ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO : listaGridTemp) {
						if(listarContratosPgitSaidaDTO.getCdSituacaoContrato() == 1 || listarContratosPgitSaidaDTO.getCdSituacaoContrato() == 3){
							identificacaoClienteContratoBean.getListaGridPesquisa().add(listarContratosPgitSaidaDTO);
						}
					}
				}
				else{
					identificacaoClienteContratoBean.setSaidaContrato(new ListarContratosPgitSaidaDTO());
					identificacaoClienteContratoBean.setListaGridPesquisa(identificacaoClienteContratoBean.getFiltroIdentificaoService().pesquisarManutencaoContrato(entradaDTO));
					identificacaoClienteContratoBean.setSaidaContrato(identificacaoClienteContratoBean.getListaGridPesquisa().get(0));
					
				}

				this.identificacaoClienteContratoBean.setListaControleRadio(new ArrayList<SelectItem>());
				for (int i = 0; i < identificacaoClienteContratoBean.getListaGridPesquisa().size();i++) {
					this.identificacaoClienteContratoBean.getListaControleRadio().add(new SelectItem(i," "));
				}
				bloquearArgumentosPesquisaAposConsulta();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
				identificacaoClienteContratoBean.setListaGridPesquisa(null);

			}

			return identificacaoClienteContratoBean.getPaginaRetorno();
		}
		
		return "";
	}
	
	/**
	 * Bloquear argumentos pesquisa apos consulta.
	 */
	public void bloquearArgumentosPesquisaAposConsulta(){
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
	}


	// Getters-Setters
	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}
	
	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Get: nomeRazaoSocial.
	 *
	 * @return nomeRazaoSocial
	 */
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	/**
	 * Set: nomeRazaoSocial.
	 *
	 * @param nomeRazaoSocial the nome razao social
	 */
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	/**
	 * Get: dataHoraCadastramento.
	 *
	 * @return dataHoraCadastramento
	 */
	public String getDataHoraCadastramento() {
		return dataHoraCadastramento;
	}

	/**
	 * Set: dataHoraCadastramento.
	 *
	 * @param dataHoraCadastramento the data hora cadastramento
	 */
	public void setDataHoraCadastramento(String dataHoraCadastramento) {
		this.dataHoraCadastramento = dataHoraCadastramento;
	}

	/**
	 * Get: inicioVigencia.
	 *
	 * @return inicioVigencia
	 */
	public String getInicioVigencia() {
		return inicioVigencia;
	}

	/**
	 * Set: inicioVigencia.
	 *
	 * @param inicioVigencia the inicio vigencia
	 */
	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	/**
	 * Get: participacao.
	 *
	 * @return participacao
	 */
	public String getParticipacao() {
		return participacao;
	}

	/**
	 * Set: participacao.
	 *
	 * @param participacao the participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	/**
	 * Get: possuiAditivos.
	 *
	 * @return possuiAditivos
	 */
	public String getPossuiAditivos() {
		return possuiAditivos;
	}

	/**
	 * Set: possuiAditivos.
	 *
	 * @param possuiAditivos the possui aditivos
	 */
	public void setPossuiAditivos(String possuiAditivos) {
		this.possuiAditivos = possuiAditivos;
	}

	/**
	 * Get: cdEmpresa.
	 *
	 * @return cdEmpresa
	 */
	public Long getCdEmpresa() {
		return cdEmpresa;
	}

	/**
	 * Set: cdEmpresa.
	 *
	 * @param cdEmpresa the cd empresa
	 */
	public void setCdEmpresa(Long cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	/**
	 * Get: cdTipo.
	 *
	 * @return cdTipo
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}

	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Get: motivoDesc.
	 *
	 * @return motivoDesc
	 */
	public String getMotivoDesc() {
		return motivoDesc;
	}

	/**
	 * Set: motivoDesc.
	 *
	 * @param motivoDesc the motivo desc
	 */
	public void setMotivoDesc(String motivoDesc) {
		this.motivoDesc = motivoDesc;
	}

	/**
	 * Get: numero.
	 *
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * Set: numero.
	 *
	 * @param numero the numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * Get: situacaoDesc.
	 *
	 * @return situacaoDesc
	 */
	public String getSituacaoDesc() {
		return situacaoDesc;
	}

	/**
	 * Set: situacaoDesc.
	 *
	 * @param situacaoDesc the situacao desc
	 */
	public void setSituacaoDesc(String situacaoDesc) {
		this.situacaoDesc = situacaoDesc;
	}

	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Get: atividadeEconomicaMaster.
	 *
	 * @return atividadeEconomicaMaster
	 */
	public String getAtividadeEconomicaMaster() {
		return atividadeEconomicaMaster;
	}

	/**
	 * Set: atividadeEconomicaMaster.
	 *
	 * @param atividadeEconomicaMaster the atividade economica master
	 */
	public void setAtividadeEconomicaMaster(String atividadeEconomicaMaster) {
		this.atividadeEconomicaMaster = atividadeEconomicaMaster;
	}

	/**
	 * Get: cpfCnpjMaster.
	 *
	 * @return cpfCnpjMaster
	 */
	public String getCpfCnpjMaster() {
		return cpfCnpjMaster;
	}

	/**
	 * Get: cdAgenciaGestora.
	 *
	 * @return cdAgenciaGestora
	 */
	public int getCdAgenciaGestora() {
		return cdAgenciaGestora;
	}

	/**
	 * Set: cdAgenciaGestora.
	 *
	 * @param cdAgenciaGestora the cd agencia gestora
	 */
	public void setCdAgenciaGestora(int cdAgenciaGestora) {
		this.cdAgenciaGestora = cdAgenciaGestora;
	}

	/**
	 * Get: dsAgenciaGestora.
	 *
	 * @return dsAgenciaGestora
	 */
	public String getDsAgenciaGestora() {
		return dsAgenciaGestora;
	}

	/**
	 * Set: dsAgenciaGestora.
	 *
	 * @param dsAgenciaGestora the ds agencia gestora
	 */
	public void setDsAgenciaGestora(String dsAgenciaGestora) {
		this.dsAgenciaGestora = dsAgenciaGestora;
	}

	/**
	 * Set: cpfCnpjMaster.
	 *
	 * @param cpfCnpjMaster the cpf cnpj master
	 */
	public void setCpfCnpjMaster(String cpfCnpjMaster) {
		this.cpfCnpjMaster = cpfCnpjMaster;
	}

	/**
	 * Get: grupoEconomicoMaster.
	 *
	 * @return grupoEconomicoMaster
	 */
	public String getGrupoEconomicoMaster() {
		return grupoEconomicoMaster;
	}

	/**
	 * Set: grupoEconomicoMaster.
	 *
	 * @param grupoEconomicoMaster the grupo economico master
	 */
	public void setGrupoEconomicoMaster(String grupoEconomicoMaster) {
		this.grupoEconomicoMaster = grupoEconomicoMaster;
	}

	/**
	 * Get: nomeRazaoSocialMaster.
	 *
	 * @return nomeRazaoSocialMaster
	 */
	public String getNomeRazaoSocialMaster() {
		return nomeRazaoSocialMaster;
	}

	/**
	 * Set: nomeRazaoSocialMaster.
	 *
	 * @param nomeRazaoSocialMaster the nome razao social master
	 */
	public void setNomeRazaoSocialMaster(String nomeRazaoSocialMaster) {
		this.nomeRazaoSocialMaster = nomeRazaoSocialMaster;
	}

	/**
	 * Get: segmentoMaster.
	 *
	 * @return segmentoMaster
	 */
	public String getSegmentoMaster() {
		return segmentoMaster;
	}

	/**
	 * Set: segmentoMaster.
	 *
	 * @param segmentoMaster the segmento master
	 */
	public void setSegmentoMaster(String segmentoMaster) {
		this.segmentoMaster = segmentoMaster;
	}

	/**
	 * Get: subSegmentoMaster.
	 *
	 * @return subSegmentoMaster
	 */
	public String getSubSegmentoMaster() {
		return subSegmentoMaster;
	}

	/**
	 * Set: subSegmentoMaster.
	 *
	 * @param subSegmentoMaster the sub segmento master
	 */
	public void setSubSegmentoMaster(String subSegmentoMaster) {
		this.subSegmentoMaster = subSegmentoMaster;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: manterContratoContasBean.
	 *
	 * @return manterContratoContasBean
	 */
	public ContasBean getManterContratoContasBean() {
		return manterContratoContasBean;
	}

	/**
	 * Set: manterContratoContasBean.
	 *
	 * @param manterContratoContasBean the manter contrato contas bean
	 */
	public void setManterContratoContasBean(ContasBean manterContratoContasBean) {
		this.manterContratoContasBean = manterContratoContasBean;
	}

	/**
	 * Get: manterContratoDadosBasicosBean.
	 *
	 * @return manterContratoDadosBasicosBean
	 */
	public DadosBasicosBean getManterContratoDadosBasicosBean() {
		return manterContratoDadosBasicosBean;
	}

	/**
	 * Set: manterContratoDadosBasicosBean.
	 *
	 * @param manterContratoDadosBasicosBean the manter contrato dados basicos bean
	 */
	public void setManterContratoDadosBasicosBean(DadosBasicosBean manterContratoDadosBasicosBean) {
		this.manterContratoDadosBasicosBean = manterContratoDadosBasicosBean;
	}

	/**
	 * Get: manterContratoParticipantesBean.
	 *
	 * @return manterContratoParticipantesBean
	 */
	public ParticipantesBean getManterContratoParticipantesBean() {
		return manterContratoParticipantesBean;
	}

	/**
	 * Set: manterContratoParticipantesBean.
	 *
	 * @param manterContratoParticipantesBean the manter contrato participantes bean
	 */
	public void setManterContratoParticipantesBean(ParticipantesBean manterContratoParticipantesBean) {
		this.manterContratoParticipantesBean = manterContratoParticipantesBean;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: manterContratoLayoutsArquivosBean.
	 *
	 * @return manterContratoLayoutsArquivosBean
	 */
	public LayoutsArquivosBean getManterContratoLayoutsArquivosBean() {
		return manterContratoLayoutsArquivosBean;
	}

	/**
	 * Set: manterContratoLayoutsArquivosBean.
	 *
	 * @param manterContratoLayoutsArquivosBean the manter contrato layouts arquivos bean
	 */
	public void setManterContratoLayoutsArquivosBean(LayoutsArquivosBean manterContratoLayoutsArquivosBean) {
		this.manterContratoLayoutsArquivosBean = manterContratoLayoutsArquivosBean;
	}

	/**
	 * Get: dsGerenteResponsavel.
	 *
	 * @return dsGerenteResponsavel
	 */
	public String getDsGerenteResponsavel() {
		return dsGerenteResponsavel;
	}

	/**
	 * Set: dsGerenteResponsavel.
	 *
	 * @param dsGerenteResponsavel the ds gerente responsavel
	 */
	public void setDsGerenteResponsavel(String dsGerenteResponsavel) {
		this.dsGerenteResponsavel = dsGerenteResponsavel;
	}

	/*
	 * public IImprimircontratoService getImprimirContratoService() { return imprimirContratoService; }
	 * 
	 * public void setImprimirContratoService( IImprimircontratoService imprimirContratoService) {
	 * this.imprimirContratoService = imprimirContratoService; }
	 */

	/**
	 * Get: imprimirContratoService.
	 *
	 * @return imprimirContratoService
	 */
	public IImprimircontratoService getImprimirContratoService() {
		return imprimirContratoService;
	}

	/**
	 * Set: imprimirContratoService.
	 *
	 * @param imprimirContratoService the imprimir contrato service
	 */
	public void setImprimirContratoService(IImprimircontratoService imprimirContratoService) {
		this.imprimirContratoService = imprimirContratoService;
	}

	/**
	 * Get: contratoPgitSelecionado.
	 *
	 * @return contratoPgitSelecionado
	 */
	public ListarContratosPgitSaidaDTO getContratoPgitSelecionado() {
		return contratoPgitSelecionado;
	}

	/**
	 * Set: contratoPgitSelecionado.
	 *
	 * @param contratoPgitSelecionado the contrato pgit selecionado
	 */
	public void setContratoPgitSelecionado(ListarContratosPgitSaidaDTO contratoPgitSelecionado) {
		this.contratoPgitSelecionado = contratoPgitSelecionado;
	}

	/**
	 * Get: manterContratoRepresentanteBean.
	 *
	 * @return manterContratoRepresentanteBean
	 */
	public RepresentanteBean getManterContratoRepresentanteBean() {
		return manterContratoRepresentanteBean;
	}

	/**
	 * Set: manterContratoRepresentanteBean.
	 *
	 * @param manterContratoRepresentanteBean the manter contrato representante bean
	 */
	public void setManterContratoRepresentanteBean(RepresentanteBean manterContratoRepresentanteBean) {
		this.manterContratoRepresentanteBean = manterContratoRepresentanteBean;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Get: imprimirService.
	 *
	 * @return imprimirService
	 */
	public IImprimirService getImprimirService() {
		return imprimirService;
	}

	/**
	 * Set: imprimirService.
	 *
	 * @param imprimirService the imprimir service
	 */
	public void setImprimirService(IImprimirService imprimirService) {
		this.imprimirService = imprimirService;
	}

	/**
	 * Get: imprimirAnexoPrimeiroContratoService.
	 *
	 * @return imprimirAnexoPrimeiroContratoService
	 */
	public IImprimirAnexoPrimeiroContratoService getImprimirAnexoPrimeiroContratoService() {
		return imprimirAnexoPrimeiroContratoService;
	}

	/**
	 * Set: imprimirAnexoPrimeiroContratoService.
	 *
	 * @param imprimirAnexoPrimeiroContratoService the imprimir anexo primeiro contrato service
	 */
	public void setImprimirAnexoPrimeiroContratoService(
					IImprimirAnexoPrimeiroContratoService imprimirAnexoPrimeiroContratoService) {
		this.imprimirAnexoPrimeiroContratoService = imprimirAnexoPrimeiroContratoService;
	}

	/**
	 * Get: imprimirAnexoSegundoContratoService.
	 *
	 * @return imprimirAnexoSegundoContratoService
	 */
	public IImprimirAnexoSegundoContratoService getImprimirAnexoSegundoContratoService() {
		return imprimirAnexoSegundoContratoService;
	}

	/**
	 * Set: imprimirAnexoSegundoContratoService.
	 *
	 * @param imprimirAnexoSegundoContratoService the imprimir anexo segundo contrato service
	 */
	public void setImprimirAnexoSegundoContratoService(
					IImprimirAnexoSegundoContratoService imprimirAnexoSegundoContratoService) {
		this.imprimirAnexoSegundoContratoService = imprimirAnexoSegundoContratoService;
	}

	/**
	 * Get: cdGerenteResponsavel.
	 *
	 * @return cdGerenteResponsavel
	 */
	public Long getCdGerenteResponsavel() {
		return cdGerenteResponsavel;
	}

	/**
	 * Set: cdGerenteResponsavel.
	 *
	 * @param cdGerenteResponsavel the cd gerente responsavel
	 */
	public void setCdGerenteResponsavel(Long cdGerenteResponsavel) {
		this.cdGerenteResponsavel = cdGerenteResponsavel;
	}

	/**
	 * Get: gerenteResponsavelFormatado.
	 *
	 * @return gerenteResponsavelFormatado
	 */
	public String getGerenteResponsavelFormatado() {
		return gerenteResponsavelFormatado;
	}

	/**
	 * Set: gerenteResponsavelFormatado.
	 *
	 * @param gerenteResponsavelFormatado the gerente responsavel formatado
	 */
	public void setGerenteResponsavelFormatado(String gerenteResponsavelFormatado) {
		this.gerenteResponsavelFormatado = gerenteResponsavelFormatado;
	}

	/**
	 * Get: dsNomeRazaoSocialParticipante.
	 *
	 * @return dsNomeRazaoSocialParticipante
	 */
	public String getDsNomeRazaoSocialParticipante() {
		return dsNomeRazaoSocialParticipante;
	}

	/**
	 * Set: dsNomeRazaoSocialParticipante.
	 *
	 * @param dsNomeRazaoSocialParticipante the ds nome razao social participante
	 */
	public void setDsNomeRazaoSocialParticipante(
			String dsNomeRazaoSocialParticipante) {
		this.dsNomeRazaoSocialParticipante = dsNomeRazaoSocialParticipante;
	}

	/**
	 * Get: cpfCnpjParticipante.
	 *
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 *
	 * @param cpfCnpjParticipante the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public Long getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(Long numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
		
}