/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.canlibpagtoconsolidadosemconsaldo
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.canlibpagtoconsolidadosemconsaldo;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.ICanLibPagtoConsolidadoSemConSaldoService;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.CancelarLiberacaoParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.ConsultarCanLibPagtosConsSemConsSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.DetalharCanLibPagtosConsSemConsSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.DetalharCanLibPagtosConsSemConsSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.canlibpagtoconsolidadosemconsaldo.bean.OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: CanLibPagtoConsolidadoSemConSaldoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class CanLibPagtoConsolidadoSemConSaldoBean extends PagamentosBean {
	
	/** Atributo canLibPagtoConsolidadoSemConSaldoImpl. */
	private ICanLibPagtoConsolidadoSemConSaldoService canLibPagtoConsolidadoSemConSaldoImpl;

	// Atributos canLibPgtoConSemConSaldo
	/** Atributo listaGridCanLibPagtoConSemConSaldo. */
	private List<ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO> listaGridCanLibPagtoConSemConSaldo;

	/** Atributo listaGridDetCanLibPagtoConSemConSaldo. */
	private List<OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO> listaGridDetCanLibPagtoConSemConSaldo;

	/** Atributo listaGridDetCanLibPagtoConSemConSaldoSelecionados. */
	private List<OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO> listaGridDetCanLibPagtoConSemConSaldoSelecionados;

	/** Atributo listaRadios. */
	private List<SelectItem> listaRadios;

	/** Atributo listaRadiosDetalhe. */
	private List<SelectItem> listaRadiosDetalhe;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo itemSelecionadoListaDetalhe. */
	private Integer itemSelecionadoListaDetalhe;

	/** Atributo exibeBeneficiario. */
	private boolean exibeBeneficiario;

	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos = false;

	/** Atributo habilitaBotaoCancelar. */
	private boolean habilitaBotaoCancelar = false;

	/** Atributo tipoTela. */
	private Integer tipoTela;

	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;

	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;

	/** Atributo dsEmpresa. */
	private String dsEmpresa;

	/** Atributo nroContrato. */
	private String nroContrato;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo dsTipoServico. */
	private String dsTipoServico;

	/** Atributo dsIndicadorModalidade. */
	private String dsIndicadorModalidade;

	/** Atributo qtdePagamentos. */
	private Long qtdePagamentos;

	/** Atributo vlrTotalPagamentos. */
	private BigDecimal vlrTotalPagamentos;

	/** Atributo qtdePagamentosCancelados. */
	private Integer qtdePagamentosCancelados;

	/** Atributo vlrTotalPagamentosCancelados. */
	private BigDecimal vlrTotalPagamentosCancelados;

	// detCanLibPgtoConSemConSaldo
	// Conta de D�bito
	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;

	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;

	/** Atributo contaDebito. */
	private String contaDebito;

	/** Atributo tipoContaDebito. */
	private String tipoContaDebito;

	// Favorecido
	/** Atributo numeroInscricaoFavorecido. */
	private String numeroInscricaoFavorecido;

	/** Atributo tipoFavorecido. */
	private Integer tipoFavorecido;

	/** Atributo descTipoFavorecido. */
	private String descTipoFavorecido;

	/** Atributo dsFavorecido. */
	private String dsFavorecido;

	/** Atributo cdFavorecido. */
	private String cdFavorecido;

	/** Atributo dsBancoFavorecido. */
	private String dsBancoFavorecido;

	/** Atributo dsAgenciaFavorecido. */
	private String dsAgenciaFavorecido;

	/** Atributo dsContaFavorecido. */
	private String dsContaFavorecido;

	/** Atributo dsTipoContaFavorecido. */
	private String dsTipoContaFavorecido;

	// Benefici�rio
	/** Atributo numeroInscricaoBeneficiario. */
	private String numeroInscricaoBeneficiario;

	/** Atributo nomeBeneficiario. */
	private String nomeBeneficiario;

	/** Atributo dtNascimentoBeneficiario. */
	private String dtNascimentoBeneficiario;

	/** Atributo dsTipoBeneficiario. */
	private String dsTipoBeneficiario;

	/** Atributo cdTipoBeneficiario. */
	private Integer cdTipoBeneficiario;

	// Pagamento
	/** Atributo nrSequenciaArquivoRemessa. */
	private String nrSequenciaArquivoRemessa;

	/** Atributo nrLoteArquivoRemessa. */
	private String nrLoteArquivoRemessa;

	/** Atributo numeroPagamento. */
	private String numeroPagamento;

	/** Atributo cdListaDebito. */
	private String cdListaDebito;

	/** Atributo dtAgendamento. */
	private String dtAgendamento;

	/** Atributo dtPagamento. */
	private String dtPagamento;
	
	/** Atributo dtDevolucaoEstorno. */
	private String dtDevolucaoEstorno;

	/** Atributo dtVencimento. */
	private String dtVencimento;		

	/** Atributo cdIndicadorEconomicoMoeda. */
	private String cdIndicadorEconomicoMoeda;

	/** Atributo qtMoeda. */
	private BigDecimal qtMoeda;

	/** Atributo vlrAgendamento. */
	private BigDecimal vlrAgendamento;

	/** Atributo vlrEfetivacao. */
	private BigDecimal vlrEfetivacao;

	/** Atributo cdSituacaoOperacaoPagamento. */
	private String cdSituacaoOperacaoPagamento;

	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;

	/** Atributo dsPagamento. */
	private String dsPagamento;

	/** Atributo nrDocumento. */
	private String nrDocumento;

	/** Atributo tipoDocumento. */
	private String tipoDocumento;

	/** Atributo cdSerieDocumento. */
	private String cdSerieDocumento;

	/** Atributo vlDocumento. */
	private BigDecimal vlDocumento;

	/** Atributo dtEmissaoDocumento. */
	private String dtEmissaoDocumento;

	/** Atributo dsUsoEmpresa. */
	private String dsUsoEmpresa;

	/** Atributo dsMensagemPrimeiraLinha. */
	private String dsMensagemPrimeiraLinha;

	/** Atributo dsMensagemSegundaLinha. */
	private String dsMensagemSegundaLinha;

	/** Atributo dddContribuinte. */
	private String dddContribuinte;

	/** Atributo telefoneContribuinte. */
	private String telefoneContribuinte;

	/** Atributo inscricaoEstadualContribuinte. */
	private String inscricaoEstadualContribuinte;

	/** Atributo codigoBarras. */
	private String codigoBarras;

	/** Atributo codigoReceita. */
	private String codigoReceita;

	/** Atributo agenteArrecadador. */
	private String agenteArrecadador;

	/** Atributo codigoTributo. */
	private String codigoTributo;

	/** Atributo numeroReferencia. */
	private String numeroReferencia;

	/** Atributo numeroCotaParcela. */
	private String numeroCotaParcela;

	/** Atributo percentualReceita. */
	private BigDecimal percentualReceita;

	/** Atributo periodoApuracao. */
	private String periodoApuracao;

	/** Atributo anoExercicio. */
	private String anoExercicio;

	/** Atributo dataReferencia. */
	private String dataReferencia;

	/** Atributo dataCompetencia. */
	private String dataCompetencia;

	/** Atributo codigoCnae. */
	private String codigoCnae;

	/** Atributo placaVeiculo. */
	private String placaVeiculo;

	/** Atributo numeroParcelamento. */
	private String numeroParcelamento;

	/** Atributo codigoOrgaoFavorecido. */
	private String codigoOrgaoFavorecido;

	/** Atributo nomeOrgaoFavorecido. */
	private String nomeOrgaoFavorecido;

	/** Atributo codigoUfFavorecida. */
	private String codigoUfFavorecida;

	/** Atributo descricaoUfFavorecida. */
	private String descricaoUfFavorecida;

	/** Atributo vlReceita. */
	private BigDecimal vlReceita;

	/** Atributo vlPrincipal. */
	private BigDecimal vlPrincipal;

	/** Atributo vlAtualizacaoMonetaria. */
	private BigDecimal vlAtualizacaoMonetaria;

	/** Atributo vlAcrescimoFinanceiro. */
	private BigDecimal vlAcrescimoFinanceiro;

	/** Atributo vlHonorarioAdvogado. */
	private BigDecimal vlHonorarioAdvogado;

	/** Atributo vlDesconto. */
	private BigDecimal vlDesconto;

	/** Atributo vlAbatimento. */
	private BigDecimal vlAbatimento;

	/** Atributo vlMulta. */
	private BigDecimal vlMulta;

	/** Atributo vlMora. */
	private BigDecimal vlMora;

	/** Atributo vlTotal. */
	private BigDecimal vlTotal;

	/** Atributo observacaoTributo. */
	private String observacaoTributo;

	/** Atributo cdBancoCredito. */
	private String cdBancoCredito;

	/** Atributo agenciaDigitoCredito. */
	private String agenciaDigitoCredito;

	/** Atributo contaDigitoCredito. */
	private String contaDigitoCredito;

	/** Atributo tipoContaCredito. */
	private String tipoContaCredito;

	/** Atributo cdBancoDestino. */
	private String cdBancoDestino;

	/** Atributo agenciaDigitoDestino. */
	private String agenciaDigitoDestino;

	/** Atributo contaDigitoDestino. */
	private String contaDigitoDestino;

	/** Atributo tipoContaDestino. */
	private String tipoContaDestino;

	/** Atributo vlDeducao. */
	private BigDecimal vlDeducao;

	/** Atributo vlAcrescimo. */
	private BigDecimal vlAcrescimo;

	/** Atributo vlImpostoRenda. */
	private BigDecimal vlImpostoRenda;

	/** Atributo vlIss. */
	private BigDecimal vlIss;

	/** Atributo vlInss. */
	private BigDecimal vlInss;

	/** Atributo vlIof. */
	private BigDecimal vlIof;

	/** Atributo codigoRenavam. */
	private String codigoRenavam;

	/** Atributo municipioTributo. */
	private String municipioTributo;

	/** Atributo ufTributo. */
	private String ufTributo;

	/** Atributo numeroNsu. */
	private String numeroNsu;

	/** Atributo opcaoTributo. */
	private String opcaoTributo;

	/** Atributo opcaoRetiradaTributo. */
	private String opcaoRetiradaTributo;

	/** Atributo clienteDepositoIdentificado. */
	private String clienteDepositoIdentificado;

	/** Atributo tipoDespositoIdentificado. */
	private String tipoDespositoIdentificado;

	/** Atributo produtoDepositoIdentificado. */
	private String produtoDepositoIdentificado;

	/** Atributo sequenciaProdutoDepositoIdentificado. */
	private String sequenciaProdutoDepositoIdentificado;

	/** Atributo bancoCartaoDepositoIdentificado. */
	private String bancoCartaoDepositoIdentificado;

	/** Atributo cartaoDepositoIdentificado. */
	private String cartaoDepositoIdentificado;

	/** Atributo bimCartaoDepositoIdentificado. */
	private String bimCartaoDepositoIdentificado;

	/** Atributo viaCartaoDepositoIdentificado. */
	private String viaCartaoDepositoIdentificado;

	/** Atributo codigoDepositante. */
	private String codigoDepositante;

	/** Atributo nomeDepositante. */
	private String nomeDepositante;

	/** Atributo camaraCentralizadora. */
	private String camaraCentralizadora;

	/** Atributo finalidadeDoc. */
	private String finalidadeDoc;

	/** Atributo identificacaoTransferenciaDoc. */
	private String identificacaoTransferenciaDoc;

	/** Atributo identificacaoTitularidade. */
	private String identificacaoTitularidade;

	/** Atributo codigoInss. */
	private String codigoInss;

	/** Atributo vlOutrasEntidades. */
	private BigDecimal vlOutrasEntidades;

	/** Atributo codigoPagador. */
	private String codigoPagador;

	/** Atributo codigoOrdemCredito. */
	private String codigoOrdemCredito;

	/** Atributo numeroOp. */
	private String numeroOp;

	/** Atributo dataExpiracaoCredito. */
	private String dataExpiracaoCredito;

	/** Atributo instrucaoPagamento. */
	private String instrucaoPagamento;

	/** Atributo numeroCheque. */
	private String numeroCheque;

	/** Atributo serieCheque. */
	private String serieCheque;

	/** Atributo indicadorAssociadoUnicaAgencia. */
	private String indicadorAssociadoUnicaAgencia;

	/** Atributo identificadorTipoRetirada. */
	private String identificadorTipoRetirada;

	/** Atributo identificadorRetirada. */
	private String identificadorRetirada;

	/** Atributo dataRetirada. */
	private String dataRetirada;

	/** Atributo vlImpostoMovFinanceira. */
	private BigDecimal vlImpostoMovFinanceira;

	/** Atributo finalidadeTed. */
	private String finalidadeTed;

	/** Atributo identificacaoTransferenciaTed. */
	private String identificacaoTransferenciaTed;

	/** Atributo nossoNumero. */
	private String nossoNumero;

	/** Atributo dsOrigemPagamento. */
	private String dsOrigemPagamento;

	/** Atributo cdIndentificadorJudicial. */
	private String cdIndentificadorJudicial;

	/** Atributo dtLimiteDescontoPagamento. */
	private String dtLimiteDescontoPagamento;
	
	/** Atributo dtLimitePagamento. */
	private String dtLimitePagamento;
	
	/** Atributo dsRastreado. */
	private String dsRastreado;
	
	/** Atributo carteira. */
	private String carteira;

	/** Atributo cdSituacaoTranferenciaAutomatica. */
	private String cdSituacaoTranferenciaAutomatica;

	/** Atributo nrRemessa. */
	private Long nrRemessa;
	
	/** Atributo numControleInternoLote. */
	private Long numControleInternoLote;
	

	// Trilha
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo logoPath. */
	private String logoPath;

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo cdBancoOriginal. */
	private Integer cdBancoOriginal;

	/** Atributo dsBancoOriginal. */
	private String dsBancoOriginal;

	/** Atributo cdAgenciaBancariaOriginal. */
	private Integer cdAgenciaBancariaOriginal;

	/** Atributo cdDigitoAgenciaOriginal. */
	private String cdDigitoAgenciaOriginal;

	/** Atributo dsAgenciaOriginal. */
	private String dsAgenciaOriginal;

	/** Atributo cdContaBancariaOriginal. */
	private Long cdContaBancariaOriginal;

	/** Atributo cdDigitoContaOriginal. */
	private String cdDigitoContaOriginal;

	/** Atributo dsTipoContaOriginal. */
	private String dsTipoContaOriginal;

	/** Atributo dtFloatingPagamento. */
	private String dtFloatingPagamento;
	
	/** Atributo dtEfetivFloatPgto. */
	private String dtEfetivFloatPgto;
	
	/** Atributo vlFloatingPagamento. */
	private BigDecimal vlFloatingPagamento;
	
	/** Atributo cdOperacaoDcom. */
	private Long cdOperacaoDcom;

	/** Atributo dsSituacaoDcom. */
	private String dsSituacaoDcom;
	
	// Flag
	/** Atributo exibeDcom. */
	private Boolean exibeDcom;
	
	/*
	 * M�todos
	 */
	// Inicializa��o
	/**
	 * Iniciar tela.
	 *
	 * @param e the e
	 */
	public void iniciarTela(ActionEvent e) {
		limpar();

		// Tela de Filtro
		getFiltroAgendamentoEfetivacaoEstornoBean().setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno("canLibPagtoConSemConSaldo");
		getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);

		// Carregamento dos Combos
		listarTipoServicoPagto();
		listarConsultarSituacaoPagamento();

		setHabilitaArgumentosPesquisa(false);

		// Radio de Agendados � Pagos/N�o Pagos, ficara sempre protegido e com a
		// op��o �Agendados� selecionada;
		setAgendadosPagosNaoPagos("1");

		setDisableArgumentosConsulta(false);
	}

	// M�todo sobreposto para n�o limpar os agendados - pagos/nao pagos
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#limparCampos()
	 */
	public void limparCampos() {
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.limparParticipanteContrato();
		setChkContaDebito(false);
		limparContaDebito();
		limparRemessaFavorecidoBeneficiarioLinha();
		limparSituacaoMotivo();
		limparTipoServicoModalidade();

		setDataInicialPagamentoFiltro(new Date());
		setDataFinalPagamentoFiltro(new Date());
		setChkParticipanteContrato(false);
		setChkRemessa(false);
		setChkServicoModalidade(false);
		setChkSituacaoMotivo(false);
		setRadioPesquisaFiltroPrincipal("");
		setRadioFavorecidoFiltro("");
		setRadioBeneficiarioFiltro("");

		setClassificacaoFiltro(null);
	}

	/**
	 * Pesquisar consultar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarConsultar(ActionEvent evt) {
		carregaListaConsultarCanLibPagtoConsSemConSaldo();
		return "";
	}

	/**
	 * Pesquisar integral parcial detalhar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarIntegralParcialDetalhar(ActionEvent evt) {
		carregaListaDetalharCanLibPagtosConsSemConSaldo("");
		return "";
	}

	/**
	 * Limpar selecionados.
	 */
	public void limparSelecionados() {
		setHabilitaBotaoCancelar(false);
		setOpcaoChecarTodos(false);
		for (OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO saida : getListaGridDetCanLibPagtoConSemConSaldo()) {
			saida.setRegistroSelecionado(false);
		}
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		limparCampos();
		limparInformacaoConsulta();
	}

	/**
	 * Limpar informacao consulta.
	 */
	public void limparInformacaoConsulta() {
		setListaGridCanLibPagtoConSemConSaldo(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);

		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());
	}

	/**
	 * Limpar.
	 */
	public void limpar() {
		limparFiltroPrincipal();
		limparInformacaoConsulta();
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(
				null);
	}

	/**
	 * Limpar tela principal.
	 */
	public void limparTelaPrincipal() {
		limparFiltroPrincipal();
		limparInformacaoConsulta();
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
		limparInformacaoConsulta();

		limparCampos();

		return "";

	}

	// Navega��o
	/**
	 * Cancelar integral.
	 *
	 * @return the string
	 */
	public String cancelarIntegral() {
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());

		return preencheDadosFiltroDetalharPagamentosConsolidados("CANCELAR_INTEGRAL");
	}

	/**
	 * Cancelar parcial.
	 *
	 * @return the string
	 */
	public String cancelarParcial() {
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());
		setHabilitaBotaoCancelar(false);
		setOpcaoChecarTodos(false);

		return preencheDadosFiltroDetalharPagamentosConsolidados("CANCELAR_PARCIAL");
	}

	/**
	 * Confirmar can liberacao parcial.
	 *
	 * @return the string
	 */
	public String confirmarCanLiberacaoParcial() {
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());
		carregaListaSelecionados();

		return "CANCELAR_PARCIAL";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		try {
			setTipoTela(getListaGridDetCanLibPagtoConSemConSaldo().get(
					getItemSelecionadoListaDetalhe()).getCdTipoTela());

			limparVariaveisDetalhes();
			if (getTipoTela() != null) {
				if (getTipoTela().intValue() == 1) {
					preencheDadosPagtoCreditoConta();
					return "DETALHE_CREDITO_CONTA";
				}
				if (getTipoTela().intValue() == 2) {
					preencheDadosPagtoDebitoConta();
					return "DETALHE_DEBITO_CONTA";
				}
				if (getTipoTela().intValue() == 3) {
					preencheDadosPagtoDOC();
					return "DETALHE_DOC";
				}
				if (getTipoTela().intValue() == 4) {
					preencheDadosPagtoTED();
					return "DETALHE_TED";
				}
				if (getTipoTela().intValue() == 5) {
					preencheDadosPagtoOrdemPagto();
					return "DETALHE_ORDEM_PAGTO";
				}
				if (getTipoTela().intValue() == 6) {
					preencheDadosPagtoTituloBradesco();
					return "DETALHE_TITULOS_BRADESCO";
				}
				if (getTipoTela().intValue() == 7) {
					preencheDadosPagtoTituloOutrosBancos();
					return "DETALHE_TITULOS_OUTROS_BANCOS";
				}
				if (getTipoTela().intValue() == 8) {
					preencheDadosPagtoDARF();
					return "DETALHE_DARF";
				}
				if (getTipoTela().intValue() == 9) {
					preencheDadosPagtoGARE();
					return "DETALHE_GARE";
				}
				if (getTipoTela().intValue() == 10) {
					preencheDadosPagtoGPS();
					return "DETALHE_GPS";
				}
				if (getTipoTela().intValue() == 11) {
					preencheDadosPagtoCodigoBarras();
					return "DETALHE_CODIGO_BARRAS";
				}
				if (getTipoTela().intValue() == 12) {
					preencheDadosPagtoDebitoVeiculos();
					return "DETALHE_DEBITO_VEICULOS";
				}
				if (getTipoTela().intValue() == 13) {
					preencheDadosPagtoDepIdentificado();
					return "DETALHE_DEPOSITO_IDENTIFICADO";
				}
				if (getTipoTela().intValue() == 14) {
					preencheDadosPagtoOrdemCredito();
					return "DETALHE_ORDEM_CREDITO";
				}
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "";
	}

	/**
	 * Detalhar filtro.
	 *
	 * @return the string
	 */
	public String detalharFiltro() {
		return preencheDadosFiltroDetalharPagamentosConsolidados("DETALHARFILTRO");
	}

	// In�cio Relat�rio

	/**
	 * Imprimir parcial.
	 */
	public void imprimirParcial() {
		try {

			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
					.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par
					.put("titulo",
							"Cancelar Libera��o de Pagamentos Consolidados Sem Consulta de Saldo");

			par.put("cpfCnpj", getNrCnpjCpf());
			par.put("nomeRazaoSocial", getDsRazaoSocial());
			par.put("empresaConglomerado", getDsEmpresa());
			par.put("nrContrato", getNroContrato());
			par.put("dsContratacao", getDsContrato());
			par.put("situacao", getCdSituacaoContrato());
			par.put("banco", getDsBancoDebito());
			par.put("agencia", getDsAgenciaDebito());
			par.put("conta", getContaDebito());
			par.put("tipo", getTipoContaDebito());
			par.put("logoBradesco", getLogoPath());

			par.put("qtdePagamentos", getQtdePagamentos());
			par.put("vlTotalPagamentos", getVlrTotalPagamentos());
			par.put("qtdePagamentosCancelados", getQtdePagamentosCancelados());
			par.put("vlTotalPagamentosCancelados",
					getVlrTotalPagamentosCancelados());
			try {
				// M�todo que gera o relat�rio PDF.
				PgitUtil
						.geraRelatorioPdf(
								"/relatorios/agendamentoEfetivacaoEstorno/liberarPagtoSemConSaldos/cancelarLibPagtosConSemConSaldo/parcial",
								"imprimirCanLibPagtoConsolidadosSemConSaldoParcial",
								getListaGridDetCanLibPagtoConSemConSaldoSelecionados(),
								par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

	}

	/**
	 * Imprimir integral.
	 */
	public void imprimirIntegral() {
		try {

			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext
					.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par
					.put("titulo",
							"Cancelar Libera��o de Pagamentos Consolidados Sem Consulta de Saldo");

			par.put("cpfCnpj", getNrCnpjCpf());
			par.put("nomeRazaoSocial", getDsRazaoSocial());
			par.put("empresaConglomerado", getDsEmpresa());
			par.put("nrContrato", getNroContrato());
			par.put("dsContratacao", getDsContrato());
			par.put("situacao", getCdSituacaoContrato());
			par.put("banco", getDsBancoDebito());
			par.put("agencia", getDsAgenciaDebito());
			par.put("conta", getContaDebito());
			par.put("tipo", getTipoContaDebito());
			par.put("logoBradesco", getLogoPath());

			par.put("qtdePagamentos", getQtdePagamentos());
			par.put("vlTotalPagamentos", getVlrTotalPagamentos());
			try {
				// M�todo que gera o relat�rio PDF.
				PgitUtil
						.geraRelatorioPdf(
								"/relatorios/agendamentoEfetivacaoEstorno/liberarPagtoSemConSaldos/cancelarLibPagtosConSemConSaldo/integral",
								"imprimirCanLibPagtoConsolidadosSemConSaldoIntegral",
								getListaGridDetCanLibPagtoConSemConSaldo(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "",
						BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	// Fim Relat�rio

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoLista(null);

		return "VOLTAR";
	}

	/**
	 * Voltar parcial.
	 *
	 * @return the string
	 */
	public String voltarParcial() {
		setOpcaoChecarTodos(false);

		for (int i = 0; i < getListaGridDetCanLibPagtoConSemConSaldo().size(); i++) {
			getListaGridDetCanLibPagtoConSemConSaldo().get(i)
					.setRegistroSelecionado(false);
		}

		return "VOLTAR";
	}

	/**
	 * Voltar liberar parcial.
	 *
	 * @return the string
	 */
	public String voltarLiberarParcial() {
		habilitarBotaoCancelar();
		return "VOLTAR";
	}

	/**
	 * Voltar detalhe.
	 *
	 * @return the string
	 */
	public String voltarDetalhe() {
		setItemSelecionadoListaDetalhe(null);

		return "VOLTAR";
	}

	/**
	 * Limpar variaveis detalhes.
	 */
	public void limparVariaveisDetalhes() {
		setNumeroInscricaoFavorecido("");
		setTipoFavorecido(null);
		setDsFavorecido("");
		setCdFavorecido("");
		setDsBancoFavorecido("");
		setDsAgenciaFavorecido("");
		setDsContaFavorecido("");
		setDsTipoContaFavorecido("");
		setNrSequenciaArquivoRemessa("");
		setNrLoteArquivoRemessa("");
		setNumeroPagamento("");
		setCdListaDebito("");
		setDtAgendamento("");
		setDtPagamento("");
		setDtVencimento("");
		setCdIndicadorEconomicoMoeda("");
		setQtMoeda(null);
		setVlrAgendamento(null);
		setVlrEfetivacao(null);
		setCdSituacaoOperacaoPagamento("");
		setDsMotivoSituacao("");
		setDsPagamento("");
		setNrDocumento("");
		setTipoDocumento("");
		setCdSerieDocumento("");
		setVlDocumento(null);
		setDtEmissaoDocumento("");
		setDsUsoEmpresa("");
		setDsMensagemPrimeiraLinha("");
		setDsMensagemSegundaLinha("");
		setDddContribuinte("");
		setTelefoneContribuinte("");
		setInscricaoEstadualContribuinte("");
		setCodigoBarras("");
		setCodigoReceita("");
		setAgenteArrecadador("");
		setCodigoTributo("");
		setNumeroReferencia("");
		setNumeroCotaParcela("");
		setPercentualReceita(null);
		setPeriodoApuracao("");
		setAnoExercicio("");
		setDataReferencia("");
		setDataCompetencia("");
		setCodigoCnae("");
		setPlacaVeiculo("");
		setNumeroParcelamento("");
		setCodigoOrgaoFavorecido("");
		setNomeOrgaoFavorecido("");
		setCodigoUfFavorecida("");
		setDescricaoUfFavorecida("");
		setVlReceita(null);
		setVlPrincipal(null);
		setVlAtualizacaoMonetaria(null);
		setVlAcrescimoFinanceiro(null);
		setVlHonorarioAdvogado(null);
		setVlDesconto(null);
		setVlAbatimento(null);
		setVlMulta(null);
		setVlMora(null);
		setVlTotal(null);
		setObservacaoTributo("");
		setCdBancoCredito("");
		setAgenciaDigitoCredito("");
		setContaDigitoCredito("");
		setTipoContaCredito("");
		setCdBancoDestino("");
		setAgenciaDigitoDestino("");
		setContaDigitoDestino("");
		setTipoContaDestino("");
		setVlDeducao(null);
		setVlAcrescimo(null);
		setVlImpostoRenda(null);
		setVlIss(null);
		setVlInss(null);
		setVlIof(null);
		setCodigoRenavam("");
		setMunicipioTributo("");
		setUfTributo("");
		setNumeroNsu("");
		setOpcaoTributo("");
		setOpcaoRetiradaTributo("");
		setClienteDepositoIdentificado("");
		setTipoDespositoIdentificado("");
		setProdutoDepositoIdentificado("");
		setSequenciaProdutoDepositoIdentificado("");
		setBancoCartaoDepositoIdentificado("");
		setCartaoDepositoIdentificado("");
		setBimCartaoDepositoIdentificado("");
		setViaCartaoDepositoIdentificado("");
		setCodigoDepositante("");
		setNomeDepositante("");
		setCamaraCentralizadora("");
		setFinalidadeDoc("");
		setIdentificacaoTransferenciaDoc("");
		setIdentificacaoTitularidade("");
		setCodigoInss("");
		setVlOutrasEntidades(null);
		setCodigoPagador("");
		setCodigoOrdemCredito("");
		setNumeroOp("");
		setDataExpiracaoCredito("");
		setInstrucaoPagamento("");
		setNumeroCheque("");
		setSerieCheque("");
		setIndicadorAssociadoUnicaAgencia("");
		setIdentificadorTipoRetirada("");
		setIdentificadorRetirada("");
		setDataRetirada("");
		setVlImpostoMovFinanceira(null);
		setFinalidadeTed("");
		setIdentificacaoTransferenciaTed("");
	}

	/**
	 * Carrega lista selecionados.
	 */
	public void carregaListaSelecionados() {
		setListaGridDetCanLibPagtoConSemConSaldoSelecionados(new ArrayList<OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO>());
		setQtdePagamentosCancelados(0);
		setVlrTotalPagamentosCancelados(BigDecimal.ZERO);
		for (OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO saida : getListaGridDetCanLibPagtoConSemConSaldo()) {
			if (saida.isRegistroSelecionado()) {
				getListaGridDetCanLibPagtoConSemConSaldoSelecionados().add(
						saida);
				setVlrTotalPagamentosCancelados(getVlrTotalPagamentosCancelados()
						.add(saida.getVlPagamento()));
			}
		}
		setQtdePagamentosCancelados(getListaGridDetCanLibPagtoConSemConSaldoSelecionados()
				.size());
	}

	/**
	 * Checar todos.
	 */
	public void checarTodos() {
		for (OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO saida : getListaGridDetCanLibPagtoConSemConSaldo()) {
			saida.setRegistroSelecionado(isOpcaoChecarTodos());
		}
		setHabilitaBotaoCancelar(isOpcaoChecarTodos());
	}

	/**
	 * Habilitar botao cancelar.
	 */
	public void habilitarBotaoCancelar() {
		setHabilitaBotaoCancelar(false);
		for (OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO saida : getListaGridDetCanLibPagtoConSemConSaldo()) {
			if (saida.isRegistroSelecionado()) {
				setHabilitaBotaoCancelar(true);
				break;
			}
		}
	}

	/**
	 * Carrega lista consultar can lib pagto cons sem con saldo.
	 *
	 * @param evt the evt
	 */
	public void carregaListaConsultarCanLibPagtoConsSemConSaldo(ActionEvent evt) {
		carregaListaConsultarCanLibPagtoConsSemConSaldo();
	}

	// PDC
	/**
	 * Carrega lista consultar can lib pagto cons sem con saldo.
	 */
	public void carregaListaConsultarCanLibPagtoConsSemConSaldo() {
		try {
			ConsultarCanLibPagtosConsSemConsSaldoEntradaDTO entrada = new ConsultarCanLibPagtosConsSemConsSaldoEntradaDTO();

			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("0")
					|| getFiltroAgendamentoEfetivacaoEstornoBean()
							.getTipoFiltroSelecionado().equals("1")) {
				entrada.setCdTipoPesquisa(1);
				entrada
						.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdPessoaJuridicaContrato() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdPessoaJuridicaContrato()
								: 0L);
				entrada
						.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdTipoContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdTipoContratoNegocio()
								: 0);
				entrada
						.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio()
								: 0L);
				entrada.setCdBanco(0);
				entrada.setCdAgencia(0);
				entrada.setCdConta(0L);
				entrada.setCdDigitoConta("00");
			}
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("2")) {
				entrada.setCdTipoPesquisa(2);
				entrada.setCdPessoaJuridicaContrato(0L);
				entrada.setCdTipoContratoNegocio(0);
				entrada.setNrSequenciaContratoNegocio(0L);
				entrada.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getBancoContaDebitoFiltro());
				entrada
						.setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getAgenciaContaDebitoBancariaFiltro());
				entrada.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getContaBancariaContaDebitoFiltro());
				entrada
						.setCdDigitoConta(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getDigitoContaDebitoFiltro());
			}

			entrada.setCdAgendadosPagosNaoPagos(Integer
					.parseInt(getAgendadosPagosNaoPagos()));

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			entrada
					.setDtCreditoPagamentoInicio((getDataInicialPagamentoFiltro() == null) ? ""
							: sdf.format(getDataInicialPagamentoFiltro()));
			entrada
					.setDtCreditoPagamentoFim((getDataFinalPagamentoFiltro() == null) ? ""
							: sdf.format(getDataFinalPagamentoFiltro()));
			entrada
					.setNrArquivoRemessaPagamento(getNumeroRemessaFiltro()
							.equals("") ? 0L : Long
							.parseLong(getNumeroRemessaFiltro()));
			if (isChkContaDebito()) {
				entrada.setCdBanco(getCdBancoContaDebito());
				entrada.setCdAgencia(getCdAgenciaBancariaContaDebito());
				entrada.setCdConta(getCdContaBancariaContaDebito());
				entrada
						.setCdDigitoConta(getCdDigitoContaContaDebito() != null
								&& !getCdDigitoContaContaDebito().equals("") ? getCdDigitoContaContaDebito()
								: "00");
			}
			entrada
					.setCdProdutoServicoOperacao(getTipoServicoFiltro() != null ? getTipoServicoFiltro()
							: 0);
			entrada
					.setCdProdutoServicoRelacionado(getModalidadeFiltro() != null ? getModalidadeFiltro()
							: 0);
			// Foi pedido para retirar o cambo de situa��o e motivo da tela -
			// email beatriz nogueira de santana dia
			// 29/03/2011 as 13:58
			entrada.setCdSituacaoOperacaoPagamento(0);
			entrada.setCdMotivoSituacaoPagamento(0);

			setListaGridCanLibPagtoConSemConSaldo(getCanLibPagtoConsolidadoSemConSaldoImpl()
					.consultarCanLibPagtosConsSemConsSaldo(entrada));

			setListaRadios(new ArrayList<SelectItem>());

			for (int i = 0; i < getListaGridCanLibPagtoConSemConSaldo().size(); i++) {
				getListaRadios().add(new SelectItem(i, ""));
			}
			setDisableArgumentosConsulta(true);
			setItemSelecionadoLista(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridCanLibPagtoConSemConSaldo(null);
			setItemSelecionadoLista(null);
			setDisableArgumentosConsulta(false);
		}
	}

	/**
	 * Carrega lista detalhar can lib pagtos cons sem con saldo.
	 *
	 * @param retorno the retorno
	 * @return the string
	 */
	public String carregaListaDetalharCanLibPagtosConsSemConSaldo(String retorno) {
		try {
			DetalharCanLibPagtosConsSemConsSaldoEntradaDTO entrada = new DetalharCanLibPagtosConsSemConsSaldoEntradaDTO();
			ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO selecaoConsulta = getListaGridCanLibPagtoConSemConSaldo()
					.get(getItemSelecionadoLista());

			entrada.setCdPessoaJuridicaContrato(selecaoConsulta
					.getCdPessoaJuridicaContrato());
			entrada.setCdTipoContratoNegocio(selecaoConsulta
					.getCdTipoContratoNegocio());
			entrada.setNrSequenciaContratoNegocio(selecaoConsulta
					.getNrContratoOrigem());
			entrada.setNrCnpjCpf(selecaoConsulta.getNrCnpjCpf());
			entrada.setNrFilialcnpjCpf(selecaoConsulta.getNrFilialCnpjCpf());
			entrada.setNrControleCnpjCpf(selecaoConsulta.getNrDigitoCnpjCpf());
			entrada.setNrArquivoRemessaPagamento(selecaoConsulta
					.getNrArquivoRemessaPagamento());
			entrada.setDtCreditoPagamento(selecaoConsulta
					.getDtCreditoPagamento());
			entrada.setCdBanco(selecaoConsulta.getCdBanco());
			entrada.setCdAgencia(selecaoConsulta.getCdAgencia());
			entrada.setCdConta(selecaoConsulta.getCdConta());
			entrada.setCdDigitoConta(selecaoConsulta.getCdDigitoConta());
			entrada.setCdProdutoServicoOperacao(selecaoConsulta
					.getCdProdutoServicoOperacao());
			entrada.setCdProdutoServicoRelacionado(selecaoConsulta
					.getCdProdutoServicoRelacionado());
			entrada.setCdSituacaoOperacaoPagamento(selecaoConsulta
					.getCdSituacaoOperacaoPagamento());
			entrada.setCdAgendadoPagaoNaoPago(Integer
					.parseInt(getAgendadosPagosNaoPagos()));
			entrada.setCdPessoaContratoDebito(selecaoConsulta
					.getCdPessoaContratoDebito());
			entrada.setCdTipoContratoDebito(selecaoConsulta
					.getCdTipoContratoDebito());
			entrada.setNrSequenciaContratoDebito(selecaoConsulta
					.getNrSequenciaContratoDebito());

			DetalharCanLibPagtosConsSemConsSaldoSaidaDTO saidaDTO = getCanLibPagtoConsolidadoSemConSaldoImpl()
					.detalharCanLibPagtosConsSemConsSaldo(entrada);
			setListaGridDetCanLibPagtoConSemConSaldo(saidaDTO
					.getListaDetalharCanLibPagtosConsSemConsSaldo());

			// in�cio do preenchimento do cabecalho
			setNrCnpjCpf(CpfCnpjUtils.formatCpfCnpjCompleto(selecaoConsulta
					.getNrCnpjCpf(), selecaoConsulta.getNrFilialCnpjCpf(),
					selecaoConsulta.getNrDigitoCnpjCpf()));
			setDsEmpresa(selecaoConsulta.getDsRazaoSocial());
			setNroContrato(Long.toString(selecaoConsulta.getNrContratoOrigem()));

			setDsRazaoSocial(saidaDTO.getNmCliente());
			setDsContrato(saidaDTO.getDsContrato());
			setCdSituacaoContrato(saidaDTO.getDsSituacaoContrato());

			setDsBancoDebito(PgitUtil.concatenarCampos(selecaoConsulta
					.getCdBanco(), saidaDTO.getDsBancoDebito()));
			setDsAgenciaDebito(PgitUtil.formatAgencia(selecaoConsulta
					.getCdAgencia(), selecaoConsulta.getCdDigitoAgencia(),
					saidaDTO.getDsAgenciaDebito(), false));
			setContaDebito(PgitUtil.formatConta(selecaoConsulta.getCdConta(),
					selecaoConsulta.getCdDigitoConta(), false));
			setTipoContaDebito(saidaDTO.getDsTipoContaDebito());
			// fim do preenchimento do cabecalho

			setListaRadiosDetalhe(new ArrayList<SelectItem>());
			for (int i = 0; i < getListaGridDetCanLibPagtoConSemConSaldo()
					.size(); i++) {
				getListaRadiosDetalhe().add(new SelectItem(i, ""));
			}
			setItemSelecionadoListaDetalhe(null);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridDetCanLibPagtoConSemConSaldo(null);
			setItemSelecionadoListaDetalhe(null);
			// Se der algum erro n�o avan�a, permanece na mesma tela
			return "";
		}
		return retorno;
	}

	/**
	 * Cancelar liberacao integral.
	 *
	 * @return the string
	 */
	public String cancelarLiberacaoIntegral() {
		try {
			CancelarLiberacaoIntegralEntradaDTO entradaDTO = new CancelarLiberacaoIntegralEntradaDTO();
			ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO selecaoConsulta = getListaGridCanLibPagtoConSemConSaldo()
					.get(getItemSelecionadoLista());

			entradaDTO.setCdAgenciaDebito(selecaoConsulta.getCdAgencia());
			entradaDTO.setCdBancoDebito(selecaoConsulta.getCdBanco());
			entradaDTO.setCdContaDebito(selecaoConsulta.getCdConta());
			entradaDTO.setCdControleCnpjCpf(selecaoConsulta
					.getNrDigitoCnpjCpf());
			entradaDTO.setCdCorpoCnpjCpf(selecaoConsulta.getNrCnpjCpf());
			entradaDTO.setCdFilialCnpjCpf(selecaoConsulta.getNrFilialCnpjCpf());
			entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
					.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
					.getCdTipoContratoNegocio());
			entradaDTO.setCdProdutoServicoOperacao(selecaoConsulta
					.getCdProdutoServicoOperacao());
			entradaDTO.setCdProdutoServicoRelacionado(selecaoConsulta
					.getCdProdutoServicoRelacionado());
			entradaDTO.setCdSituacao(selecaoConsulta
					.getCdSituacaoOperacaoPagamento());
			entradaDTO.setDtAgendamentoPagamento(selecaoConsulta
					.getDtCreditoPagamento());
			entradaDTO.setHrInclusaoRemessaSistema(" ");
			entradaDTO.setNrSequenciaArquivoRemessa(selecaoConsulta
					.getNrArquivoRemessaPagamento());
			entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
					.getNrContratoOrigem());

			CancelarLiberacaoIntegralSaidaDTO saidaDTO = getCanLibPagtoConsolidadoSemConSaldoImpl()
					.cancelarLiberacaoIntegral(entradaDTO);

			BradescoFacesUtils
					.addInfoModalMessage(
							"(" + saidaDTO.getCodMensagem() + ") "
									+ saidaDTO.getMensagem(),
							"canLibPagtoConSemConSaldo",
							"#{canLibPagtoConsolidadoSemConSaldoBean.carregaListaConsultarCanLibPagtoConsSemConSaldo}",
							false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}
		return "";
	}

	/**
	 * Cancelar liberacao parcial.
	 *
	 * @return the string
	 */
	public String cancelarLiberacaoParcial() {
		try {
			List<CancelarLiberacaoParcialEntradaDTO> listaEntradaDTO = new ArrayList<CancelarLiberacaoParcialEntradaDTO>();
			ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO selecaoConsulta = getListaGridCanLibPagtoConSemConSaldo()
					.get(getItemSelecionadoLista());
			for (int i = 0; i < getListaGridDetCanLibPagtoConSemConSaldoSelecionados()
					.size(); i++) {
				CancelarLiberacaoParcialEntradaDTO entradaDTO = new CancelarLiberacaoParcialEntradaDTO();

				entradaDTO.setCdPessoaJuridicaContrato(selecaoConsulta
						.getCdPessoaJuridicaContrato());
				entradaDTO.setCdTipoContratoNegocio(selecaoConsulta
						.getCdTipoContratoNegocio());
				entradaDTO.setCdProdutoServicoOperacao(selecaoConsulta
						.getCdProdutoServicoOperacao());
				entradaDTO.setNrSequenciaContratoNegocio(selecaoConsulta
						.getNrContratoOrigem());
				entradaDTO.setCdOperacaoProdutoServico(selecaoConsulta
						.getCdProdutoServicoRelacionado());
				entradaDTO
						.setCdControlePagamento(getListaGridDetCanLibPagtoConSemConSaldoSelecionados()
								.get(i).getNrPagamento());
				entradaDTO
						.setCdTipoCanal(getListaGridDetCanLibPagtoConSemConSaldoSelecionados()
								.get(i).getCdTipoCanal());

				listaEntradaDTO.add(entradaDTO);
			}

			CancelarLiberacaoParcialSaidaDTO saidaDTO = getCanLibPagtoConsolidadoSemConSaldoImpl()
					.cancelarLiberacaoParcial(listaEntradaDTO);

			BradescoFacesUtils
					.addInfoModalMessage(
							"(" + saidaDTO.getCodMensagem() + ") "
									+ saidaDTO.getMensagem(),
							"canLibPagtoConSemConSaldo",
							"#{canLibPagtoConsolidadoSemConSaldoBean.carregaListaConsultarCanLibPagtoConsSemConSaldo}",
							false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}
		return "";
	}

	// Preenche dados detalhe de pagamentos
	/**
	 * Preenche dados filtro detalhar pagamentos consolidados.
	 *
	 * @param retorno the retorno
	 * @return the string
	 */
	public String preencheDadosFiltroDetalharPagamentosConsolidados(
			String retorno) {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO selecaoConsulta = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);

		setDsTipoServico(selecaoConsulta.getDsResumoProdutoServico());
		setDsIndicadorModalidade(selecaoConsulta.getDsOperacaoProdutoServico());
		setQtdePagamentos(selecaoConsulta.getQtPagamento());
		setVlrTotalPagamentos(selecaoConsulta.getVlEfetivoPagamentoCliente());
		setNrRemessa(selecaoConsulta.getNrArquivoRemessaPagamento());
		setDtPagamento(FormatarData.formatarDataFromPdc(selecaoConsulta
				.getDtCreditoPagamento()));

		return carregaListaDetalharCanLibPagtosConsSemConSaldo(retorno);
	}

	/**
	 * Preenche dados pagto codigo barras.
	 */
	public void preencheDadosPagtoCodigoBarras() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);

		DetalharPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharPagtoCodigoBarrasEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoCodigoBarras(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil
				.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
				.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddTelefone());
		setTelefoneContribuinte(saidaDTO.getNrTelefone());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
		setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
		setCodigoReceita(saidaDTO.getCdReceitaTributo());
		setAgenteArrecadador(saidaDTO.getCdAgenteArrecadador());
		setCodigoTributo(saidaDTO.getCdTributoArrecadado());
		setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTributo());
		setPercentualReceita(saidaDTO.getCdPercentualTributo());
		setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
		setAnoExercicio(saidaDTO.getCdAnoExercicioTributo());
		setDataReferencia(saidaDTO.getDtReferenciaTributo());
		setDataCompetencia(saidaDTO.getDtCompensacao());
		setCodigoCnae(saidaDTO.getCdCnae());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setNumeroParcelamento(saidaDTO.getNrParcelamentoTributo());
		setCodigoOrgaoFavorecido(saidaDTO.getCdOrgaoFavorecido());
		setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
		setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
		setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
		setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setObservacaoTributo(saidaDTO.getDsObservacao());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto credito conta.
	 */
	public void preencheDadosPagtoCreditoConta() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoCreditoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoCreditoConta(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null
				|| saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
				.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
				saidaDTO.getDsBancoDebito(), false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(
				saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
				saidaDTO.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
				saidaDTO.getDsDigAgenciaDebito(), false));
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidade() == 1 || saidaDTO.getCdIndicadorModalidade() == 3) {
			setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
					.getCdTipoInscricaoFavorecido(), saidaDTO
					.getNmInscriFavorecido()));
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil
					.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido() == null
					|| saidaDTO.getCdFavorecido().equals("") ? null : saidaDTO
					.getCdFavorecido());
			setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO
					.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(),
					false));
			setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
					.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
					saidaDTO.getDsAgenciaFavorecido(), false));
			setDsContaFavorecido(PgitUtil.formatConta(saidaDTO
					.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
					false));
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidade() == 1 ? true : false);

		} else {
			setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

			exibeBeneficiario = !PgitUtil.verificaStringNula(
					getNumeroInscricaoBeneficiario()).equals("")
					|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
							.equals(""))
					|| !PgitUtil.verificaStringNula(getNomeBeneficiario())
							.equals("");
			setExibeDcom(false);

		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlrAgendamento());
		setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
				saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
				saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil.formatConta(
				saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
				false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
		setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
		setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
		setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setCdSituacaoTranferenciaAutomatica(saidaDTO
				.getCdSituacaoTranferenciaAutomatica());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
		setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
		
		//Favorecidos
		setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
		setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - " + saidaDTO.getDsBancoOriginal());
		setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
		setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
		setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-" + saidaDTO.getCdDigitoAgenciaOriginal() + " - " + saidaDTO.getDsAgenciaOriginal());
		setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
		setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-" + saidaDTO.getCdDigitoContaOriginal());
		setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());
	}

	/**
	 * Limpar campos favorecido beneficiario.
	 */
	private void limparCamposFavorecidoBeneficiario() {
		// Favorecido
		setDsFavorecido("");
		setCdFavorecido("");
		setDsBancoFavorecido("");
		setDsAgenciaFavorecido("");
		setDsContaFavorecido("");
		setDsTipoContaFavorecido("");
		setNumeroInscricaoFavorecido("");
		setTipoFavorecido(0);

		// Benefici�rio
		setNumeroInscricaoBeneficiario("");
		setCdTipoBeneficiario(0);
		setNomeBeneficiario("");
		setDtNascimentoBeneficiario("");
		setDsTipoBeneficiario("");

	}

	/**
	 * Preenche dados pagto darf.
	 */
	public void preencheDadosPagtoDARF() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoDARFEntradaDTO entradaDTO = new DetalharPagtoDARFEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoDARF(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil
				.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(registroSelecionadoDetalhar.getNrPagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddTelefone());
		setTelefoneContribuinte(saidaDTO.getCdTelefone());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
		setAgenteArrecadador(saidaDTO.getCdAgenteArrecad());
		setCodigoReceita(saidaDTO.getCdReceitaTributo());
		setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcela());
		setPercentualReceita(saidaDTO.getCdPercentualReceita());
		setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
		setAnoExercicio(saidaDTO.getCdDanoExercicio());
		setDataReferencia(saidaDTO.getDtReferenciaTributo());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
		setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlTotal(saidaDTO.getVlTotalTributo());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto debito conta.
	 */
	public void preencheDadosPagtoDebitoConta() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoDebitoContaEntradaDTO entradaDTO = new DetalharPagtoDebitoContaEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoDebitoConta(entradaDTO);

		// Cabe�alho - Cliente
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null
				|| saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
				.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// Sempre Carregar Conta de D�bito
		setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(),
				saidaDTO.getDsBancoDebito(), false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(
				saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
				saidaDTO.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(),
				saidaDTO.getDsDigAgenciaDebito(), false));
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
				.getCdTipoInscricaoFavorecido(), saidaDTO
				.getNmInscriFavorecido()));
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil
				.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
				saidaDTO.getDsBancoFavorecido(), false));
		setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
				saidaDTO.getDsAgenciaFavorecido(), false));
		setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(),
				saidaDTO.getCdDigContaCredito(), false));
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlrAgendamento());
		setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
				saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
				saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil.formatConta(
				saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
				false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
		setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
		setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto debito veiculos.
	 */
	public void preencheDadosPagtoDebitoVeiculos() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharPagtoDebitoVeiculosEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoDebitoVeiculos(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil
				.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
				.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTrib());
		setAnoExercicio(saidaDTO.getDtAnoExercTributo());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
		setMunicipioTributo(saidaDTO.getCdMunicArrecadTrib());
		setUfTributo(saidaDTO.getCdUfTributo());
		setNumeroNsu(saidaDTO.getNrNsuTributo());
		setOpcaoTributo(saidaDTO.getCdOpcaoTributo());
		setOpcaoRetiradaTributo(saidaDTO.getCdOpcaoRetirada());
		setVlPrincipal(saidaDTO.getVlPrincipalTributo());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlTotal(saidaDTO.getVlTotalTributo());
		setObservacaoTributo(saidaDTO.getDsObservacaoTributo());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto dep identificado.
	 */
	public void preencheDadosPagtoDepIdentificado() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharPagtoDepIdentificadoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoDepIdentificado(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Beneficiario
		setNumeroInscricaoBeneficiario(saidaDTO
				.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil
				.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
				.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
				saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
				saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil.formatConta(
				saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
				false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setClienteDepositoIdentificado(saidaDTO.getCdClienteDepositoId());
		setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
		setProdutoDepositoIdentificado(saidaDTO.getCdProdutoDepositoId());
		setSequenciaProdutoDepositoIdentificado(saidaDTO
				.getCdSequenciaProdutoDepositoId());
		setBancoCartaoDepositoIdentificado(saidaDTO
				.getCdBancoCartaoDepositanteId());
		setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
		setBimCartaoDepositoIdentificado(saidaDTO.getCdBinCartaoDepositanteId());
		setViaCartaoDepositoIdentificado(saidaDTO.getCdViaCartaoDepositanteId());
		setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
		setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
		setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
		setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
		setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
		setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
		setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
		setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
		setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
		setVlIss(saidaDTO.getVlIssCreditoPagamento());
		setVlIof(saidaDTO.getVlIofCreditoPagamento());
		setVlInss(saidaDTO.getVlInssCreditoPagamento());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto doc.
	 */
	public void preencheDadosPagtoDOC() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoDOCEntradaDTO entradaDTO = new DetalharPagtoDOCEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoDOC(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null
				|| saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
				.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
					.getCdTipoInscricaoFavorecido(), saidaDTO
					.getNmInscriFavorecido()));
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil
					.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(
					getNumeroInscricaoBeneficiario()).equals("")
					|| !PgitUtil.verificaStringNula(getDsTipoBeneficiario())
							.equals("")
					|| !PgitUtil.verificaStringNula(getNomeBeneficiario())
							.equals("");
			setExibeDcom(false);

		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
		setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
		setIdentificacaoTransferenciaDoc(saidaDTO
				.getDsIdentificacaoTransferenciaPagto());
		setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto gare.
	 */
	public void preencheDadosPagtoGARE() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoGAREEntradaDTO entradaDTO = new DetalharPagtoGAREEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoGARE(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil
				.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
				.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddContribuinte());
		setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
		setInscricaoEstadualContribuinte(saidaDTO
				.getCdInscricaoEstadualContribuinte());
		setCodigoReceita(saidaDTO.getCdReceita());
		setNumeroReferencia(saidaDTO.getNrReferencia());
		setNumeroCotaParcela(saidaDTO.getNrCota());
		setPercentualReceita(saidaDTO.getCdPercentualReceita());
		setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
		setDataReferencia(saidaDTO.getDtReferencia());
		setCodigoCnae(saidaDTO.getCdCnae());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setNumeroParcelamento(saidaDTO.getNrParcela());
		setCodigoOrgaoFavorecido(saidaDTO.getCdOrFavorecido());
		setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
		setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
		setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
		setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setObservacaoTributo(saidaDTO.getDsTributo());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto gps.
	 */
	public void preencheDadosPagtoGPS() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoGPSEntradaDTO entradaDTO = new DetalharPagtoGPSEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoGPS(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil
				.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
				.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddContribuinte());
		setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
		setCodigoInss(saidaDTO.getCdInss());
		setDataCompetencia(saidaDTO.getDsMesAnoCompt());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto ordem credito.
	 */
	public void preencheDadosPagtoOrdemCredito() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharPagtoOrdemCreditoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoOrdemCredito(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Beneficiario
		setNumeroInscricaoBeneficiario(saidaDTO
				.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil
				.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null
				|| saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
				.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
				saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO
				.getCdAgenciaCredito(), saidaDTO.getCdDigAgenciaCredito(),
				saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil.formatConta(
				saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
				false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCodigoPagador(saidaDTO.getCdPagador());
		setCodigoOrdemCredito(saidaDTO.getCdOrdemCreditoPagamento());
		setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
		setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
		setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
		setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
		setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
		setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
		setVlIss(saidaDTO.getVlIssCreditoPagamento());
		setVlIof(saidaDTO.getVlIofCreditoPagamento());
		setVlInss(saidaDTO.getVlInssCreditoPagamento());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto ordem pagto.
	 */
	public void preencheDadosPagtoOrdemPagto() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharPagtoOrdemPagtoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoOrdemPagtoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoOrdemPagto(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			// Favorecido
			setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO
					.getCdTipoInscricaoFavorecido(), saidaDTO
					.getNmInscriFavorecido()));
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil
					.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);

		} else {
			// Benefici�rio
			setNumeroInscricaoBeneficiario(saidaDTO
					.getNumeroInscricaoBeneficiarioFormatado());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

			exibeBeneficiario = !PgitUtil.verificaStringNula(
					getNumeroInscricaoBeneficiario()).equals("")
					|| !PgitUtil.verificaStringNula(getDsTipoBeneficiario())
							.equals("")
					|| !PgitUtil.verificaStringNula(getNomeBeneficiario())
							.equals("");
			setExibeDcom(false);

		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setNumeroOp(saidaDTO.getNrOperacao());
		setDataExpiracaoCredito(saidaDTO.getDtExpedicaoCredito());
		setInstrucaoPagamento(saidaDTO.getCdInsPagamento());
		setNumeroCheque(saidaDTO.getNrCheque());
		setSerieCheque(saidaDTO.getNrSerieCheque());
		setIndicadorAssociadoUnicaAgencia(saidaDTO.getDsUnidadeAgencia());
		setIdentificadorTipoRetirada(saidaDTO.getDsIdentificacaoTipoRetirada());
		setIdentificadorRetirada(saidaDTO.getDsIdentificacaoRetirada());
		setDataRetirada(saidaDTO.getDtRetirada());
		setVlImpostoMovFinanceira(saidaDTO.getVlImpostoMovimentacaoFinanceira());
		setVlDesconto(saidaDTO.getVlDescontoCredito());
		setVlAbatimento(saidaDTO.getVlAbatidoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlIrCredito());
		setVlIss(saidaDTO.getVlIssCredito());
		setVlIof(saidaDTO.getVlIofCredito());
		setVlInss(saidaDTO.getVlInssCredito());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto ted.
	 */
	public void preencheDadosPagtoTED() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoTEDEntradaDTO entradaDTO = new DetalharPagtoTEDEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTEDSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoTED(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparCamposFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			setNumeroInscricaoFavorecido(saidaDTO
					.getNumeroInscricaoFavorecidoFormatado());
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil
					.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);

		} else {
			setNumeroInscricaoBeneficiario(saidaDTO
					.getNumeroInscricaoBeneficiarioFormatado());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(
					getNumeroInscricaoBeneficiario()).equals("")
					|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario())
							.equals(""))
					|| !PgitUtil.verificaStringNula(getNomeBeneficiario())
							.equals("");
			setExibeDcom(false);

		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());

		setFinalidadeTed(PgitUtil.concatenarCampos(saidaDTO.getCdFinalidadeTed(),saidaDTO.getDsFinalidadeDocTed(), " - "));
		setIdentificacaoTransferenciaTed(saidaDTO
				.getDsIdentificacaoTransferenciaPagto());
		setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());

		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatidoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlIrCredito());
		setVlIss(saidaDTO.getVlIssCredito());
		setVlIof(saidaDTO.getVlIofCredito());
		setVlInss(saidaDTO.getVlInssCredito());
		setCdIndentificadorJudicial(saidaDTO.getCdIndentificadorJudicial());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto titulo bradesco.
	 */
	public void preencheDadosPagtoTituloBradesco() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharPagtoTituloBradescoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoTituloBradesco(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil
				.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setCarteira(saidaDTO.getCarteira());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCodigoBarras(saidaDTO.getCdBarras());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
		setNossoNumero(saidaDTO.getNossoNumero());
		setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
		setDsRastreado(saidaDTO.getCdRastreado() == 1 ? "Sim":"N�o");
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Preenche dados pagto titulo outros bancos.
	 */
	public void preencheDadosPagtoTituloOutrosBancos() {
		ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionado = getListaGridCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoLista);
		OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO registroSelecionadoDetalhar = getListaGridDetCanLibPagtoConSemConSaldo()
				.get(itemSelecionadoListaDetalhe);
		DetalharPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharPagtoTituloOutrosBancosEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado
				.getCdPessoaJuridicaContrato());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado
				.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
				.getNrContratoOrigem());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar
				.getNrPagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
		entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
		entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar
				.getCdAgencia());
		entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
		entradaDTO.setCdAgendadosPagoNaoPago(Integer
				.parseInt(getAgendadosPagosNaoPagos()));
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado
				.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
				.detalharPagtoTituloOutrosBancos(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO
				.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil
				.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setCarteira(saidaDTO.getCarteira());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCodigoBarras(saidaDTO.getCdBarras());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
		setNossoNumero(saidaDTO.getNossoNumero());
		setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
		setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
		setDsRastreado(saidaDTO.getCdRastreado() == 1 ? "Sim":"N�o");
		setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
		setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
		setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/*
	 * Getters e Setters
	 */
	/**
	 * Get: tipoTela.
	 *
	 * @return tipoTela
	 */
	public Integer getTipoTela() {
		return tipoTela;
	}

	/**
	 * Set: tipoTela.
	 *
	 * @param tipoTela the tipo tela
	 */
	public void setTipoTela(Integer tipoTela) {
		this.tipoTela = tipoTela;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: agenteArrecadador.
	 *
	 * @return agenteArrecadador
	 */
	public String getAgenteArrecadador() {
		return agenteArrecadador;
	}

	/**
	 * Set: agenteArrecadador.
	 *
	 * @param agenteArrecadador the agente arrecadador
	 */
	public void setAgenteArrecadador(String agenteArrecadador) {
		this.agenteArrecadador = agenteArrecadador;
	}

	/**
	 * Get: anoExercicio.
	 *
	 * @return anoExercicio
	 */
	public String getAnoExercicio() {
		return anoExercicio;
	}

	/**
	 * Set: anoExercicio.
	 *
	 * @param anoExercicio the ano exercicio
	 */
	public void setAnoExercicio(String anoExercicio) {
		this.anoExercicio = anoExercicio;
	}

	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public String getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}

	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(String cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}

	/**
	 * Get: cdListaDebito.
	 *
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}

	/**
	 * Set: cdListaDebito.
	 *
	 * @param cdListaDebito the cd lista debito
	 */
	public void setCdListaDebito(String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}

	/**
	 * Get: cdSerieDocumento.
	 *
	 * @return cdSerieDocumento
	 */
	public String getCdSerieDocumento() {
		return cdSerieDocumento;
	}

	/**
	 * Set: cdSerieDocumento.
	 *
	 * @param cdSerieDocumento the cd serie documento
	 */
	public void setCdSerieDocumento(String cdSerieDocumento) {
		this.cdSerieDocumento = cdSerieDocumento;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public String getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(
			String cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: codigoBarras.
	 *
	 * @return codigoBarras
	 */
	public String getCodigoBarras() {
		return codigoBarras;
	}

	/**
	 * Set: codigoBarras.
	 *
	 * @param codigoBarras the codigo barras
	 */
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	/**
	 * Get: codigoCnae.
	 *
	 * @return codigoCnae
	 */
	public String getCodigoCnae() {
		return codigoCnae;
	}

	/**
	 * Set: codigoCnae.
	 *
	 * @param codigoCnae the codigo cnae
	 */
	public void setCodigoCnae(String codigoCnae) {
		this.codigoCnae = codigoCnae;
	}

	/**
	 * Get: codigoOrgaoFavorecido.
	 *
	 * @return codigoOrgaoFavorecido
	 */
	public String getCodigoOrgaoFavorecido() {
		return codigoOrgaoFavorecido;
	}

	/**
	 * Set: codigoOrgaoFavorecido.
	 *
	 * @param codigoOrgaoFavorecido the codigo orgao favorecido
	 */
	public void setCodigoOrgaoFavorecido(String codigoOrgaoFavorecido) {
		this.codigoOrgaoFavorecido = codigoOrgaoFavorecido;
	}

	/**
	 * Get: codigoReceita.
	 *
	 * @return codigoReceita
	 */
	public String getCodigoReceita() {
		return codigoReceita;
	}

	/**
	 * Set: codigoReceita.
	 *
	 * @param codigoReceita the codigo receita
	 */
	public void setCodigoReceita(String codigoReceita) {
		this.codigoReceita = codigoReceita;
	}

	/**
	 * Get: codigoTributo.
	 *
	 * @return codigoTributo
	 */
	public String getCodigoTributo() {
		return codigoTributo;
	}

	/**
	 * Set: codigoTributo.
	 *
	 * @param codigoTributo the codigo tributo
	 */
	public void setCodigoTributo(String codigoTributo) {
		this.codigoTributo = codigoTributo;
	}

	/**
	 * Get: codigoUfFavorecida.
	 *
	 * @return codigoUfFavorecida
	 */
	public String getCodigoUfFavorecida() {
		return codigoUfFavorecida;
	}

	/**
	 * Set: codigoUfFavorecida.
	 *
	 * @param codigoUfFavorecida the codigo uf favorecida
	 */
	public void setCodigoUfFavorecida(String codigoUfFavorecida) {
		this.codigoUfFavorecida = codigoUfFavorecida;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: contaDebito.
	 *
	 * @return contaDebito
	 */
	public String getContaDebito() {
		return contaDebito;
	}

	/**
	 * Set: contaDebito.
	 *
	 * @param contaDebito the conta debito
	 */
	public void setContaDebito(String contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * Get: dataCompetencia.
	 *
	 * @return dataCompetencia
	 */
	public String getDataCompetencia() {
		return dataCompetencia;
	}

	/**
	 * Set: dataCompetencia.
	 *
	 * @param dataCompetencia the data competencia
	 */
	public void setDataCompetencia(String dataCompetencia) {
		this.dataCompetencia = dataCompetencia;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: dataReferencia.
	 *
	 * @return dataReferencia
	 */
	public String getDataReferencia() {
		return dataReferencia;
	}

	/**
	 * Set: dataReferencia.
	 *
	 * @param dataReferencia the data referencia
	 */
	public void setDataReferencia(String dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	/**
	 * Get: dddContribuinte.
	 *
	 * @return dddContribuinte
	 */
	public String getDddContribuinte() {
		return dddContribuinte;
	}

	/**
	 * Set: dddContribuinte.
	 *
	 * @param dddContribuinte the ddd contribuinte
	 */
	public void setDddContribuinte(String dddContribuinte) {
		this.dddContribuinte = dddContribuinte;
	}

	/**
	 * Get: descricaoUfFavorecida.
	 *
	 * @return descricaoUfFavorecida
	 */
	public String getDescricaoUfFavorecida() {
		return descricaoUfFavorecida;
	}

	/**
	 * Set: descricaoUfFavorecida.
	 *
	 * @param descricaoUfFavorecida the descricao uf favorecida
	 */
	public void setDescricaoUfFavorecida(String descricaoUfFavorecida) {
		this.descricaoUfFavorecida = descricaoUfFavorecida;
	}

	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsAgenciaFavorecido.
	 *
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}

	/**
	 * Set: dsAgenciaFavorecido.
	 *
	 * @param dsAgenciaFavorecido the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}

	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: dsBancoFavorecido.
	 *
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}

	/**
	 * Set: dsBancoFavorecido.
	 *
	 * @param dsBancoFavorecido the ds banco favorecido
	 */
	public void setDsBancoFavorecido(String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}

	/**
	 * Get: dsContaFavorecido.
	 *
	 * @return dsContaFavorecido
	 */
	public String getDsContaFavorecido() {
		return dsContaFavorecido;
	}

	/**
	 * Set: dsContaFavorecido.
	 *
	 * @param dsContaFavorecido the ds conta favorecido
	 */
	public void setDsContaFavorecido(String dsContaFavorecido) {
		this.dsContaFavorecido = dsContaFavorecido;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}

	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}

	/**
	 * Get: dsIndicadorModalidade.
	 *
	 * @return dsIndicadorModalidade
	 */
	public String getDsIndicadorModalidade() {
		return dsIndicadorModalidade;
	}

	/**
	 * Set: dsIndicadorModalidade.
	 *
	 * @param dsIndicadorModalidade the ds indicador modalidade
	 */
	public void setDsIndicadorModalidade(String dsIndicadorModalidade) {
		this.dsIndicadorModalidade = dsIndicadorModalidade;
	}

	/**
	 * Get: dsMensagemPrimeiraLinha.
	 *
	 * @return dsMensagemPrimeiraLinha
	 */
	public String getDsMensagemPrimeiraLinha() {
		return dsMensagemPrimeiraLinha;
	}

	/**
	 * Set: dsMensagemPrimeiraLinha.
	 *
	 * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
	 */
	public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
		this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
	}

	/**
	 * Get: dsMensagemSegundaLinha.
	 *
	 * @return dsMensagemSegundaLinha
	 */
	public String getDsMensagemSegundaLinha() {
		return dsMensagemSegundaLinha;
	}

	/**
	 * Set: dsMensagemSegundaLinha.
	 *
	 * @param dsMensagemSegundaLinha the ds mensagem segunda linha
	 */
	public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
		this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
	}

	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	/**
	 * Get: dsPagamento.
	 *
	 * @return dsPagamento
	 */
	public String getDsPagamento() {
		return dsPagamento;
	}

	/**
	 * Set: dsPagamento.
	 *
	 * @param dsPagamento the ds pagamento
	 */
	public void setDsPagamento(String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: dsTipoContaFavorecido.
	 *
	 * @return dsTipoContaFavorecido
	 */
	public String getDsTipoContaFavorecido() {
		return dsTipoContaFavorecido;
	}

	/**
	 * Set: dsTipoContaFavorecido.
	 *
	 * @param dsTipoContaFavorecido the ds tipo conta favorecido
	 */
	public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
		this.dsTipoContaFavorecido = dsTipoContaFavorecido;
	}

	/**
	 * Get: dsUsoEmpresa.
	 *
	 * @return dsUsoEmpresa
	 */
	public String getDsUsoEmpresa() {
		return dsUsoEmpresa;
	}

	/**
	 * Set: dsUsoEmpresa.
	 *
	 * @param dsUsoEmpresa the ds uso empresa
	 */
	public void setDsUsoEmpresa(String dsUsoEmpresa) {
		this.dsUsoEmpresa = dsUsoEmpresa;
	}

	/**
	 * Get: dtAgendamento.
	 *
	 * @return dtAgendamento
	 */
	public String getDtAgendamento() {
		return dtAgendamento;
	}

	/**
	 * Set: dtAgendamento.
	 *
	 * @param dtAgendamento the dt agendamento
	 */
	public void setDtAgendamento(String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	/**
	 * Get: dtEmissaoDocumento.
	 *
	 * @return dtEmissaoDocumento
	 */
	public String getDtEmissaoDocumento() {
		return dtEmissaoDocumento;
	}

	/**
	 * Set: dtEmissaoDocumento.
	 *
	 * @param dtEmissaoDocumento the dt emissao documento
	 */
	public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
		this.dtEmissaoDocumento = dtEmissaoDocumento;
	}

	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}

	/**
	 * Get: dtVencimento.
	 *
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 *
	 * @param dtVencimento the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: inscricaoEstadualContribuinte.
	 *
	 * @return inscricaoEstadualContribuinte
	 */
	public String getInscricaoEstadualContribuinte() {
		return inscricaoEstadualContribuinte;
	}

	/**
	 * Set: inscricaoEstadualContribuinte.
	 *
	 * @param inscricaoEstadualContribuinte the inscricao estadual contribuinte
	 */
	public void setInscricaoEstadualContribuinte(
			String inscricaoEstadualContribuinte) {
		this.inscricaoEstadualContribuinte = inscricaoEstadualContribuinte;
	}

	/**
	 * Get: nomeOrgaoFavorecido.
	 *
	 * @return nomeOrgaoFavorecido
	 */
	public String getNomeOrgaoFavorecido() {
		return nomeOrgaoFavorecido;
	}

	/**
	 * Set: nomeOrgaoFavorecido.
	 *
	 * @param nomeOrgaoFavorecido the nome orgao favorecido
	 */
	public void setNomeOrgaoFavorecido(String nomeOrgaoFavorecido) {
		this.nomeOrgaoFavorecido = nomeOrgaoFavorecido;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nrDocumento.
	 *
	 * @return nrDocumento
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}

	/**
	 * Set: nrDocumento.
	 *
	 * @param nrDocumento the nr documento
	 */
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	/**
	 * Get: nrLoteArquivoRemessa.
	 *
	 * @return nrLoteArquivoRemessa
	 */
	public String getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}

	/**
	 * Set: nrLoteArquivoRemessa.
	 *
	 * @param nrLoteArquivoRemessa the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: nrSequenciaArquivoRemessa.
	 *
	 * @return nrSequenciaArquivoRemessa
	 */
	public String getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}

	/**
	 * Set: nrSequenciaArquivoRemessa.
	 *
	 * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}

	/**
	 * Get: numeroCotaParcela.
	 *
	 * @return numeroCotaParcela
	 */
	public String getNumeroCotaParcela() {
		return numeroCotaParcela;
	}

	/**
	 * Set: numeroCotaParcela.
	 *
	 * @param numeroCotaParcela the numero cota parcela
	 */
	public void setNumeroCotaParcela(String numeroCotaParcela) {
		this.numeroCotaParcela = numeroCotaParcela;
	}

	/**
	 * Get: numeroInscricaoFavorecido.
	 *
	 * @return numeroInscricaoFavorecido
	 */
	public String getNumeroInscricaoFavorecido() {
		return numeroInscricaoFavorecido;
	}

	/**
	 * Set: numeroInscricaoFavorecido.
	 *
	 * @param numeroInscricaoFavorecido the numero inscricao favorecido
	 */
	public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
		this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
	}

	/**
	 * Get: numeroPagamento.
	 *
	 * @return numeroPagamento
	 */
	public String getNumeroPagamento() {
		return numeroPagamento;
	}

	/**
	 * Set: numeroPagamento.
	 *
	 * @param numeroPagamento the numero pagamento
	 */
	public void setNumeroPagamento(String numeroPagamento) {
		this.numeroPagamento = numeroPagamento;
	}

	/**
	 * Get: numeroParcelamento.
	 *
	 * @return numeroParcelamento
	 */
	public String getNumeroParcelamento() {
		return numeroParcelamento;
	}

	/**
	 * Set: numeroParcelamento.
	 *
	 * @param numeroParcelamento the numero parcelamento
	 */
	public void setNumeroParcelamento(String numeroParcelamento) {
		this.numeroParcelamento = numeroParcelamento;
	}

	/**
	 * Get: numeroReferencia.
	 *
	 * @return numeroReferencia
	 */
	public String getNumeroReferencia() {
		return numeroReferencia;
	}

	/**
	 * Set: numeroReferencia.
	 *
	 * @param numeroReferencia the numero referencia
	 */
	public void setNumeroReferencia(String numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}

	/**
	 * Get: observacaoTributo.
	 *
	 * @return observacaoTributo
	 */
	public String getObservacaoTributo() {
		return observacaoTributo;
	}

	/**
	 * Set: observacaoTributo.
	 *
	 * @param observacaoTributo the observacao tributo
	 */
	public void setObservacaoTributo(String observacaoTributo) {
		this.observacaoTributo = observacaoTributo;
	}

	/**
	 * Get: periodoApuracao.
	 *
	 * @return periodoApuracao
	 */
	public String getPeriodoApuracao() {
		return periodoApuracao;
	}

	/**
	 * Set: periodoApuracao.
	 *
	 * @param periodoApuracao the periodo apuracao
	 */
	public void setPeriodoApuracao(String periodoApuracao) {
		this.periodoApuracao = periodoApuracao;
	}

	/**
	 * Get: placaVeiculo.
	 *
	 * @return placaVeiculo
	 */
	public String getPlacaVeiculo() {
		return placaVeiculo;
	}

	/**
	 * Set: placaVeiculo.
	 *
	 * @param placaVeiculo the placa veiculo
	 */
	public void setPlacaVeiculo(String placaVeiculo) {
		this.placaVeiculo = placaVeiculo;
	}

	/**
	 * Get: qtMoeda.
	 *
	 * @return qtMoeda
	 */
	public BigDecimal getQtMoeda() {
		return qtMoeda;
	}

	/**
	 * Set: qtMoeda.
	 *
	 * @param qtMoeda the qt moeda
	 */
	public void setQtMoeda(BigDecimal qtMoeda) {
		this.qtMoeda = qtMoeda;
	}

	/**
	 * Get: telefoneContribuinte.
	 *
	 * @return telefoneContribuinte
	 */
	public String getTelefoneContribuinte() {
		return telefoneContribuinte;
	}

	/**
	 * Set: telefoneContribuinte.
	 *
	 * @param telefoneContribuinte the telefone contribuinte
	 */
	public void setTelefoneContribuinte(String telefoneContribuinte) {
		this.telefoneContribuinte = telefoneContribuinte;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: tipoContaDebito.
	 *
	 * @return tipoContaDebito
	 */
	public String getTipoContaDebito() {
		return tipoContaDebito;
	}

	/**
	 * Set: tipoContaDebito.
	 *
	 * @param tipoContaDebito the tipo conta debito
	 */
	public void setTipoContaDebito(String tipoContaDebito) {
		this.tipoContaDebito = tipoContaDebito;
	}

	/**
	 * Get: tipoDocumento.
	 *
	 * @return tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Set: tipoDocumento.
	 *
	 * @param tipoDocumento the tipo documento
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Get: tipoFavorecido.
	 *
	 * @return tipoFavorecido
	 */
	public Integer getTipoFavorecido() {
		return tipoFavorecido;
	}

	/**
	 * Set: tipoFavorecido.
	 *
	 * @param tipoFavorecido the tipo favorecido
	 */
	public void setTipoFavorecido(Integer tipoFavorecido) {
		this.tipoFavorecido = tipoFavorecido;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: dtNascimentoBeneficiario.
	 *
	 * @return dtNascimentoBeneficiario
	 */
	public String getDtNascimentoBeneficiario() {
		return dtNascimentoBeneficiario;
	}

	/**
	 * Set: dtNascimentoBeneficiario.
	 *
	 * @param dtNascimentoBeneficiario the dt nascimento beneficiario
	 */
	public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
		this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
	}

	/**
	 * Get: nomeBeneficiario.
	 *
	 * @return nomeBeneficiario
	 */
	public String getNomeBeneficiario() {
		return nomeBeneficiario;
	}

	/**
	 * Set: nomeBeneficiario.
	 *
	 * @param nomeBeneficiario the nome beneficiario
	 */
	public void setNomeBeneficiario(String nomeBeneficiario) {
		this.nomeBeneficiario = nomeBeneficiario;
	}

	/**
	 * Get: numeroInscricaoBeneficiario.
	 *
	 * @return numeroInscricaoBeneficiario
	 */
	public String getNumeroInscricaoBeneficiario() {
		return numeroInscricaoBeneficiario;
	}

	/**
	 * Set: numeroInscricaoBeneficiario.
	 *
	 * @param numeroInscricaoBeneficiario the numero inscricao beneficiario
	 */
	public void setNumeroInscricaoBeneficiario(
			String numeroInscricaoBeneficiario) {
		this.numeroInscricaoBeneficiario = numeroInscricaoBeneficiario;
	}

	/**
	 * Get: agenciaDigitoCredito.
	 *
	 * @return agenciaDigitoCredito
	 */
	public String getAgenciaDigitoCredito() {
		return agenciaDigitoCredito;
	}

	/**
	 * Set: agenciaDigitoCredito.
	 *
	 * @param agenciaDigitoCredito the agencia digito credito
	 */
	public void setAgenciaDigitoCredito(String agenciaDigitoCredito) {
		this.agenciaDigitoCredito = agenciaDigitoCredito;
	}

	/**
	 * Get: agenciaDigitoDestino.
	 *
	 * @return agenciaDigitoDestino
	 */
	public String getAgenciaDigitoDestino() {
		return agenciaDigitoDestino;
	}

	/**
	 * Set: agenciaDigitoDestino.
	 *
	 * @param agenciaDigitoDestino the agencia digito destino
	 */
	public void setAgenciaDigitoDestino(String agenciaDigitoDestino) {
		this.agenciaDigitoDestino = agenciaDigitoDestino;
	}

	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public String getCdBancoCredito() {
		return cdBancoCredito;
	}

	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(String cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}

	/**
	 * Get: cdBancoDestino.
	 *
	 * @return cdBancoDestino
	 */
	public String getCdBancoDestino() {
		return cdBancoDestino;
	}

	/**
	 * Set: cdBancoDestino.
	 *
	 * @param cdBancoDestino the cd banco destino
	 */
	public void setCdBancoDestino(String cdBancoDestino) {
		this.cdBancoDestino = cdBancoDestino;
	}

	/**
	 * Get: contaDigitoCredito.
	 *
	 * @return contaDigitoCredito
	 */
	public String getContaDigitoCredito() {
		return contaDigitoCredito;
	}

	/**
	 * Set: contaDigitoCredito.
	 *
	 * @param contaDigitoCredito the conta digito credito
	 */
	public void setContaDigitoCredito(String contaDigitoCredito) {
		this.contaDigitoCredito = contaDigitoCredito;
	}

	/**
	 * Get: contaDigitoDestino.
	 *
	 * @return contaDigitoDestino
	 */
	public String getContaDigitoDestino() {
		return contaDigitoDestino;
	}

	/**
	 * Set: contaDigitoDestino.
	 *
	 * @param contaDigitoDestino the conta digito destino
	 */
	public void setContaDigitoDestino(String contaDigitoDestino) {
		this.contaDigitoDestino = contaDigitoDestino;
	}

	/**
	 * Get: tipoContaCredito.
	 *
	 * @return tipoContaCredito
	 */
	public String getTipoContaCredito() {
		return tipoContaCredito;
	}

	/**
	 * Set: tipoContaCredito.
	 *
	 * @param tipoContaCredito the tipo conta credito
	 */
	public void setTipoContaCredito(String tipoContaCredito) {
		this.tipoContaCredito = tipoContaCredito;
	}

	/**
	 * Get: tipoContaDestino.
	 *
	 * @return tipoContaDestino
	 */
	public String getTipoContaDestino() {
		return tipoContaDestino;
	}

	/**
	 * Set: tipoContaDestino.
	 *
	 * @param tipoContaDestino the tipo conta destino
	 */
	public void setTipoContaDestino(String tipoContaDestino) {
		this.tipoContaDestino = tipoContaDestino;
	}

	/**
	 * Get: codigoRenavam.
	 *
	 * @return codigoRenavam
	 */
	public String getCodigoRenavam() {
		return codigoRenavam;
	}

	/**
	 * Set: codigoRenavam.
	 *
	 * @param codigoRenavam the codigo renavam
	 */
	public void setCodigoRenavam(String codigoRenavam) {
		this.codigoRenavam = codigoRenavam;
	}

	/**
	 * Get: municipioTributo.
	 *
	 * @return municipioTributo
	 */
	public String getMunicipioTributo() {
		return municipioTributo;
	}

	/**
	 * Set: municipioTributo.
	 *
	 * @param municipioTributo the municipio tributo
	 */
	public void setMunicipioTributo(String municipioTributo) {
		this.municipioTributo = municipioTributo;
	}

	/**
	 * Get: numeroNsu.
	 *
	 * @return numeroNsu
	 */
	public String getNumeroNsu() {
		return numeroNsu;
	}

	/**
	 * Set: numeroNsu.
	 *
	 * @param numeroNsu the numero nsu
	 */
	public void setNumeroNsu(String numeroNsu) {
		this.numeroNsu = numeroNsu;
	}

	/**
	 * Get: opcaoRetiradaTributo.
	 *
	 * @return opcaoRetiradaTributo
	 */
	public String getOpcaoRetiradaTributo() {
		return opcaoRetiradaTributo;
	}

	/**
	 * Set: opcaoRetiradaTributo.
	 *
	 * @param opcaoRetiradaTributo the opcao retirada tributo
	 */
	public void setOpcaoRetiradaTributo(String opcaoRetiradaTributo) {
		this.opcaoRetiradaTributo = opcaoRetiradaTributo;
	}

	/**
	 * Get: opcaoTributo.
	 *
	 * @return opcaoTributo
	 */
	public String getOpcaoTributo() {
		return opcaoTributo;
	}

	/**
	 * Set: opcaoTributo.
	 *
	 * @param opcaoTributo the opcao tributo
	 */
	public void setOpcaoTributo(String opcaoTributo) {
		this.opcaoTributo = opcaoTributo;
	}

	/**
	 * Get: ufTributo.
	 *
	 * @return ufTributo
	 */
	public String getUfTributo() {
		return ufTributo;
	}

	/**
	 * Set: ufTributo.
	 *
	 * @param ufTributo the uf tributo
	 */
	public void setUfTributo(String ufTributo) {
		this.ufTributo = ufTributo;
	}

	/**
	 * Get: bancoCartaoDepositoIdentificado.
	 *
	 * @return bancoCartaoDepositoIdentificado
	 */
	public String getBancoCartaoDepositoIdentificado() {
		return bancoCartaoDepositoIdentificado;
	}

	/**
	 * Set: bancoCartaoDepositoIdentificado.
	 *
	 * @param bancoCartaoDepositoIdentificado the banco cartao deposito identificado
	 */
	public void setBancoCartaoDepositoIdentificado(
			String bancoCartaoDepositoIdentificado) {
		this.bancoCartaoDepositoIdentificado = bancoCartaoDepositoIdentificado;
	}

	/**
	 * Get: bimCartaoDepositoIdentificado.
	 *
	 * @return bimCartaoDepositoIdentificado
	 */
	public String getBimCartaoDepositoIdentificado() {
		return bimCartaoDepositoIdentificado;
	}

	/**
	 * Set: bimCartaoDepositoIdentificado.
	 *
	 * @param bimCartaoDepositoIdentificado the bim cartao deposito identificado
	 */
	public void setBimCartaoDepositoIdentificado(
			String bimCartaoDepositoIdentificado) {
		this.bimCartaoDepositoIdentificado = bimCartaoDepositoIdentificado;
	}

	/**
	 * Get: cartaoDepositoIdentificado.
	 *
	 * @return cartaoDepositoIdentificado
	 */
	public String getCartaoDepositoIdentificado() {
		return cartaoDepositoIdentificado;
	}

	/**
	 * Set: cartaoDepositoIdentificado.
	 *
	 * @param cartaoDepositoIdentificado the cartao deposito identificado
	 */
	public void setCartaoDepositoIdentificado(String cartaoDepositoIdentificado) {
		this.cartaoDepositoIdentificado = cartaoDepositoIdentificado;
	}

	/**
	 * Get: clienteDepositoIdentificado.
	 *
	 * @return clienteDepositoIdentificado
	 */
	public String getClienteDepositoIdentificado() {
		return clienteDepositoIdentificado;
	}

	/**
	 * Set: clienteDepositoIdentificado.
	 *
	 * @param clienteDepositoIdentificado the cliente deposito identificado
	 */
	public void setClienteDepositoIdentificado(
			String clienteDepositoIdentificado) {
		this.clienteDepositoIdentificado = clienteDepositoIdentificado;
	}

	/**
	 * Get: codigoDepositante.
	 *
	 * @return codigoDepositante
	 */
	public String getCodigoDepositante() {
		return codigoDepositante;
	}

	/**
	 * Set: codigoDepositante.
	 *
	 * @param codigoDepositante the codigo depositante
	 */
	public void setCodigoDepositante(String codigoDepositante) {
		this.codigoDepositante = codigoDepositante;
	}

	/**
	 * Get: nomeDepositante.
	 *
	 * @return nomeDepositante
	 */
	public String getNomeDepositante() {
		return nomeDepositante;
	}

	/**
	 * Set: nomeDepositante.
	 *
	 * @param nomeDepositante the nome depositante
	 */
	public void setNomeDepositante(String nomeDepositante) {
		this.nomeDepositante = nomeDepositante;
	}

	/**
	 * Get: produtoDepositoIdentificado.
	 *
	 * @return produtoDepositoIdentificado
	 */
	public String getProdutoDepositoIdentificado() {
		return produtoDepositoIdentificado;
	}

	/**
	 * Set: produtoDepositoIdentificado.
	 *
	 * @param produtoDepositoIdentificado the produto deposito identificado
	 */
	public void setProdutoDepositoIdentificado(
			String produtoDepositoIdentificado) {
		this.produtoDepositoIdentificado = produtoDepositoIdentificado;
	}

	/**
	 * Get: sequenciaProdutoDepositoIdentificado.
	 *
	 * @return sequenciaProdutoDepositoIdentificado
	 */
	public String getSequenciaProdutoDepositoIdentificado() {
		return sequenciaProdutoDepositoIdentificado;
	}

	/**
	 * Set: sequenciaProdutoDepositoIdentificado.
	 *
	 * @param sequenciaProdutoDepositoIdentificado the sequencia produto deposito identificado
	 */
	public void setSequenciaProdutoDepositoIdentificado(
			String sequenciaProdutoDepositoIdentificado) {
		this.sequenciaProdutoDepositoIdentificado = sequenciaProdutoDepositoIdentificado;
	}

	/**
	 * Get: tipoDespositoIdentificado.
	 *
	 * @return tipoDespositoIdentificado
	 */
	public String getTipoDespositoIdentificado() {
		return tipoDespositoIdentificado;
	}

	/**
	 * Set: tipoDespositoIdentificado.
	 *
	 * @param tipoDespositoIdentificado the tipo desposito identificado
	 */
	public void setTipoDespositoIdentificado(String tipoDespositoIdentificado) {
		this.tipoDespositoIdentificado = tipoDespositoIdentificado;
	}

	/**
	 * Get: viaCartaoDepositoIdentificado.
	 *
	 * @return viaCartaoDepositoIdentificado
	 */
	public String getViaCartaoDepositoIdentificado() {
		return viaCartaoDepositoIdentificado;
	}

	/**
	 * Set: viaCartaoDepositoIdentificado.
	 *
	 * @param viaCartaoDepositoIdentificado the via cartao deposito identificado
	 */
	public void setViaCartaoDepositoIdentificado(
			String viaCartaoDepositoIdentificado) {
		this.viaCartaoDepositoIdentificado = viaCartaoDepositoIdentificado;
	}

	/**
	 * Get: camaraCentralizadora.
	 *
	 * @return camaraCentralizadora
	 */
	public String getCamaraCentralizadora() {
		return camaraCentralizadora;
	}

	/**
	 * Set: camaraCentralizadora.
	 *
	 * @param camaraCentralizadora the camara centralizadora
	 */
	public void setCamaraCentralizadora(String camaraCentralizadora) {
		this.camaraCentralizadora = camaraCentralizadora;
	}

	/**
	 * Get: finalidadeDoc.
	 *
	 * @return finalidadeDoc
	 */
	public String getFinalidadeDoc() {
		return finalidadeDoc;
	}

	/**
	 * Set: finalidadeDoc.
	 *
	 * @param finalidadeDoc the finalidade doc
	 */
	public void setFinalidadeDoc(String finalidadeDoc) {
		this.finalidadeDoc = finalidadeDoc;
	}

	/**
	 * Get: identificacaoTitularidade.
	 *
	 * @return identificacaoTitularidade
	 */
	public String getIdentificacaoTitularidade() {
		return identificacaoTitularidade;
	}

	/**
	 * Set: identificacaoTitularidade.
	 *
	 * @param identificacaoTitularidade the identificacao titularidade
	 */
	public void setIdentificacaoTitularidade(String identificacaoTitularidade) {
		this.identificacaoTitularidade = identificacaoTitularidade;
	}

	/**
	 * Get: identificacaoTransferenciaDoc.
	 *
	 * @return identificacaoTransferenciaDoc
	 */
	public String getIdentificacaoTransferenciaDoc() {
		return identificacaoTransferenciaDoc;
	}

	/**
	 * Set: identificacaoTransferenciaDoc.
	 *
	 * @param identificacaoTransferenciaDoc the identificacao transferencia doc
	 */
	public void setIdentificacaoTransferenciaDoc(
			String identificacaoTransferenciaDoc) {
		this.identificacaoTransferenciaDoc = identificacaoTransferenciaDoc;
	}

	/**
	 * Get: codigoInss.
	 *
	 * @return codigoInss
	 */
	public String getCodigoInss() {
		return codigoInss;
	}

	/**
	 * Set: codigoInss.
	 *
	 * @param codigoInss the codigo inss
	 */
	public void setCodigoInss(String codigoInss) {
		this.codigoInss = codigoInss;
	}

	/**
	 * Get: codigoOrdemCredito.
	 *
	 * @return codigoOrdemCredito
	 */
	public String getCodigoOrdemCredito() {
		return codigoOrdemCredito;
	}

	/**
	 * Set: codigoOrdemCredito.
	 *
	 * @param codigoOrdemCredito the codigo ordem credito
	 */
	public void setCodigoOrdemCredito(String codigoOrdemCredito) {
		this.codigoOrdemCredito = codigoOrdemCredito;
	}

	/**
	 * Get: codigoPagador.
	 *
	 * @return codigoPagador
	 */
	public String getCodigoPagador() {
		return codigoPagador;
	}

	/**
	 * Set: codigoPagador.
	 *
	 * @param codigoPagador the codigo pagador
	 */
	public void setCodigoPagador(String codigoPagador) {
		this.codigoPagador = codigoPagador;
	}

	/**
	 * Get: dataExpiracaoCredito.
	 *
	 * @return dataExpiracaoCredito
	 */
	public String getDataExpiracaoCredito() {
		return dataExpiracaoCredito;
	}

	/**
	 * Set: dataExpiracaoCredito.
	 *
	 * @param dataExpiracaoCredito the data expiracao credito
	 */
	public void setDataExpiracaoCredito(String dataExpiracaoCredito) {
		this.dataExpiracaoCredito = dataExpiracaoCredito;
	}

	/**
	 * Get: dataRetirada.
	 *
	 * @return dataRetirada
	 */
	public String getDataRetirada() {
		return dataRetirada;
	}

	/**
	 * Set: dataRetirada.
	 *
	 * @param dataRetirada the data retirada
	 */
	public void setDataRetirada(String dataRetirada) {
		this.dataRetirada = dataRetirada;
	}

	/**
	 * Get: identificadorRetirada.
	 *
	 * @return identificadorRetirada
	 */
	public String getIdentificadorRetirada() {
		return identificadorRetirada;
	}

	/**
	 * Set: identificadorRetirada.
	 *
	 * @param identificadorRetirada the identificador retirada
	 */
	public void setIdentificadorRetirada(String identificadorRetirada) {
		this.identificadorRetirada = identificadorRetirada;
	}

	/**
	 * Get: identificadorTipoRetirada.
	 *
	 * @return identificadorTipoRetirada
	 */
	public String getIdentificadorTipoRetirada() {
		return identificadorTipoRetirada;
	}

	/**
	 * Set: identificadorTipoRetirada.
	 *
	 * @param identificadorTipoRetirada the identificador tipo retirada
	 */
	public void setIdentificadorTipoRetirada(String identificadorTipoRetirada) {
		this.identificadorTipoRetirada = identificadorTipoRetirada;
	}

	/**
	 * Get: indicadorAssociadoUnicaAgencia.
	 *
	 * @return indicadorAssociadoUnicaAgencia
	 */
	public String getIndicadorAssociadoUnicaAgencia() {
		return indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Set: indicadorAssociadoUnicaAgencia.
	 *
	 * @param indicadorAssociadoUnicaAgencia the indicador associado unica agencia
	 */
	public void setIndicadorAssociadoUnicaAgencia(
			String indicadorAssociadoUnicaAgencia) {
		this.indicadorAssociadoUnicaAgencia = indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Get: instrucaoPagamento.
	 *
	 * @return instrucaoPagamento
	 */
	public String getInstrucaoPagamento() {
		return instrucaoPagamento;
	}

	/**
	 * Set: instrucaoPagamento.
	 *
	 * @param instrucaoPagamento the instrucao pagamento
	 */
	public void setInstrucaoPagamento(String instrucaoPagamento) {
		this.instrucaoPagamento = instrucaoPagamento;
	}

	/**
	 * Get: numeroCheque.
	 *
	 * @return numeroCheque
	 */
	public String getNumeroCheque() {
		return numeroCheque;
	}

	/**
	 * Set: numeroCheque.
	 *
	 * @param numeroCheque the numero cheque
	 */
	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}

	/**
	 * Get: numeroOp.
	 *
	 * @return numeroOp
	 */
	public String getNumeroOp() {
		return numeroOp;
	}

	/**
	 * Set: numeroOp.
	 *
	 * @param numeroOp the numero op
	 */
	public void setNumeroOp(String numeroOp) {
		this.numeroOp = numeroOp;
	}

	/**
	 * Get: serieCheque.
	 *
	 * @return serieCheque
	 */
	public String getSerieCheque() {
		return serieCheque;
	}

	/**
	 * Set: serieCheque.
	 *
	 * @param serieCheque the serie cheque
	 */
	public void setSerieCheque(String serieCheque) {
		this.serieCheque = serieCheque;
	}

	/**
	 * Get: finalidadeTed.
	 *
	 * @return finalidadeTed
	 */
	public String getFinalidadeTed() {
		return finalidadeTed;
	}

	/**
	 * Set: finalidadeTed.
	 *
	 * @param finalidadeTed the finalidade ted
	 */
	public void setFinalidadeTed(String finalidadeTed) {
		this.finalidadeTed = finalidadeTed;
	}

	/**
	 * Get: identificacaoTransferenciaTed.
	 *
	 * @return identificacaoTransferenciaTed
	 */
	public String getIdentificacaoTransferenciaTed() {
		return identificacaoTransferenciaTed;
	}

	/**
	 * Set: identificacaoTransferenciaTed.
	 *
	 * @param identificacaoTransferenciaTed the identificacao transferencia ted
	 */
	public void setIdentificacaoTransferenciaTed(
			String identificacaoTransferenciaTed) {
		this.identificacaoTransferenciaTed = identificacaoTransferenciaTed;
	}

	/**
	 * Get: vlAbatimento.
	 *
	 * @return vlAbatimento
	 */
	public BigDecimal getVlAbatimento() {
		return vlAbatimento;
	}

	/**
	 * Set: vlAbatimento.
	 *
	 * @param vlAbatimento the vl abatimento
	 */
	public void setVlAbatimento(BigDecimal vlAbatimento) {
		this.vlAbatimento = vlAbatimento;
	}

	/**
	 * Get: vlAcrescimo.
	 *
	 * @return vlAcrescimo
	 */
	public BigDecimal getVlAcrescimo() {
		return vlAcrescimo;
	}

	/**
	 * Set: vlAcrescimo.
	 *
	 * @param vlAcrescimo the vl acrescimo
	 */
	public void setVlAcrescimo(BigDecimal vlAcrescimo) {
		this.vlAcrescimo = vlAcrescimo;
	}

	/**
	 * Get: vlAcrescimoFinanceiro.
	 *
	 * @return vlAcrescimoFinanceiro
	 */
	public BigDecimal getVlAcrescimoFinanceiro() {
		return vlAcrescimoFinanceiro;
	}

	/**
	 * Set: vlAcrescimoFinanceiro.
	 *
	 * @param vlAcrescimoFinanceiro the vl acrescimo financeiro
	 */
	public void setVlAcrescimoFinanceiro(BigDecimal vlAcrescimoFinanceiro) {
		this.vlAcrescimoFinanceiro = vlAcrescimoFinanceiro;
	}

	/**
	 * Get: vlAtualizacaoMonetaria.
	 *
	 * @return vlAtualizacaoMonetaria
	 */
	public BigDecimal getVlAtualizacaoMonetaria() {
		return vlAtualizacaoMonetaria;
	}

	/**
	 * Set: vlAtualizacaoMonetaria.
	 *
	 * @param vlAtualizacaoMonetaria the vl atualizacao monetaria
	 */
	public void setVlAtualizacaoMonetaria(BigDecimal vlAtualizacaoMonetaria) {
		this.vlAtualizacaoMonetaria = vlAtualizacaoMonetaria;
	}

	/**
	 * Get: vlDeducao.
	 *
	 * @return vlDeducao
	 */
	public BigDecimal getVlDeducao() {
		return vlDeducao;
	}

	/**
	 * Set: vlDeducao.
	 *
	 * @param vlDeducao the vl deducao
	 */
	public void setVlDeducao(BigDecimal vlDeducao) {
		this.vlDeducao = vlDeducao;
	}

	/**
	 * Get: vlDesconto.
	 *
	 * @return vlDesconto
	 */
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}

	/**
	 * Set: vlDesconto.
	 *
	 * @param vlDesconto the vl desconto
	 */
	public void setVlDesconto(BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	/**
	 * Get: vlDocumento.
	 *
	 * @return vlDocumento
	 */
	public BigDecimal getVlDocumento() {
		return vlDocumento;
	}

	/**
	 * Set: vlDocumento.
	 *
	 * @param vlDocumento the vl documento
	 */
	public void setVlDocumento(BigDecimal vlDocumento) {
		this.vlDocumento = vlDocumento;
	}

	/**
	 * Get: vlHonorarioAdvogado.
	 *
	 * @return vlHonorarioAdvogado
	 */
	public BigDecimal getVlHonorarioAdvogado() {
		return vlHonorarioAdvogado;
	}

	/**
	 * Set: vlHonorarioAdvogado.
	 *
	 * @param vlHonorarioAdvogado the vl honorario advogado
	 */
	public void setVlHonorarioAdvogado(BigDecimal vlHonorarioAdvogado) {
		this.vlHonorarioAdvogado = vlHonorarioAdvogado;
	}

	/**
	 * Get: vlImpostoMovFinanceira.
	 *
	 * @return vlImpostoMovFinanceira
	 */
	public BigDecimal getVlImpostoMovFinanceira() {
		return vlImpostoMovFinanceira;
	}

	/**
	 * Set: vlImpostoMovFinanceira.
	 *
	 * @param vlImpostoMovFinanceira the vl imposto mov financeira
	 */
	public void setVlImpostoMovFinanceira(BigDecimal vlImpostoMovFinanceira) {
		this.vlImpostoMovFinanceira = vlImpostoMovFinanceira;
	}

	/**
	 * Get: vlImpostoRenda.
	 *
	 * @return vlImpostoRenda
	 */
	public BigDecimal getVlImpostoRenda() {
		return vlImpostoRenda;
	}

	/**
	 * Set: vlImpostoRenda.
	 *
	 * @param vlImpostoRenda the vl imposto renda
	 */
	public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
		this.vlImpostoRenda = vlImpostoRenda;
	}

	/**
	 * Get: vlInss.
	 *
	 * @return vlInss
	 */
	public BigDecimal getVlInss() {
		return vlInss;
	}

	/**
	 * Set: vlInss.
	 *
	 * @param vlInss the vl inss
	 */
	public void setVlInss(BigDecimal vlInss) {
		this.vlInss = vlInss;
	}

	/**
	 * Get: vlIof.
	 *
	 * @return vlIof
	 */
	public BigDecimal getVlIof() {
		return vlIof;
	}

	/**
	 * Set: vlIof.
	 *
	 * @param vlIof the vl iof
	 */
	public void setVlIof(BigDecimal vlIof) {
		this.vlIof = vlIof;
	}

	/**
	 * Get: vlIss.
	 *
	 * @return vlIss
	 */
	public BigDecimal getVlIss() {
		return vlIss;
	}

	/**
	 * Set: vlIss.
	 *
	 * @param vlIss the vl iss
	 */
	public void setVlIss(BigDecimal vlIss) {
		this.vlIss = vlIss;
	}

	/**
	 * Get: vlMora.
	 *
	 * @return vlMora
	 */
	public BigDecimal getVlMora() {
		return vlMora;
	}

	/**
	 * Set: vlMora.
	 *
	 * @param vlMora the vl mora
	 */
	public void setVlMora(BigDecimal vlMora) {
		this.vlMora = vlMora;
	}

	/**
	 * Get: vlMulta.
	 *
	 * @return vlMulta
	 */
	public BigDecimal getVlMulta() {
		return vlMulta;
	}

	/**
	 * Set: vlMulta.
	 *
	 * @param vlMulta the vl multa
	 */
	public void setVlMulta(BigDecimal vlMulta) {
		this.vlMulta = vlMulta;
	}

	/**
	 * Get: vlOutrasEntidades.
	 *
	 * @return vlOutrasEntidades
	 */
	public BigDecimal getVlOutrasEntidades() {
		return vlOutrasEntidades;
	}

	/**
	 * Set: vlOutrasEntidades.
	 *
	 * @param vlOutrasEntidades the vl outras entidades
	 */
	public void setVlOutrasEntidades(BigDecimal vlOutrasEntidades) {
		this.vlOutrasEntidades = vlOutrasEntidades;
	}

	/**
	 * Get: vlPrincipal.
	 *
	 * @return vlPrincipal
	 */
	public BigDecimal getVlPrincipal() {
		return vlPrincipal;
	}

	/**
	 * Set: vlPrincipal.
	 *
	 * @param vlPrincipal the vl principal
	 */
	public void setVlPrincipal(BigDecimal vlPrincipal) {
		this.vlPrincipal = vlPrincipal;
	}

	/**
	 * Get: vlrAgendamento.
	 *
	 * @return vlrAgendamento
	 */
	public BigDecimal getVlrAgendamento() {
		return vlrAgendamento;
	}

	/**
	 * Set: vlrAgendamento.
	 *
	 * @param vlrAgendamento the vlr agendamento
	 */
	public void setVlrAgendamento(BigDecimal vlrAgendamento) {
		this.vlrAgendamento = vlrAgendamento;
	}

	/**
	 * Get: vlReceita.
	 *
	 * @return vlReceita
	 */
	public BigDecimal getVlReceita() {
		return vlReceita;
	}

	/**
	 * Set: vlReceita.
	 *
	 * @param vlReceita the vl receita
	 */
	public void setVlReceita(BigDecimal vlReceita) {
		this.vlReceita = vlReceita;
	}

	/**
	 * Get: vlrEfetivacao.
	 *
	 * @return vlrEfetivacao
	 */
	public BigDecimal getVlrEfetivacao() {
		return vlrEfetivacao;
	}

	/**
	 * Set: vlrEfetivacao.
	 *
	 * @param vlrEfetivacao the vlr efetivacao
	 */
	public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
		this.vlrEfetivacao = vlrEfetivacao;
	}

	/**
	 * Get: vlTotal.
	 *
	 * @return vlTotal
	 */
	public BigDecimal getVlTotal() {
		return vlTotal;
	}

	/**
	 * Set: vlTotal.
	 *
	 * @param vlTotal the vl total
	 */
	public void setVlTotal(BigDecimal vlTotal) {
		this.vlTotal = vlTotal;
	}

	/**
	 * Get: percentualReceita.
	 *
	 * @return percentualReceita
	 */
	public BigDecimal getPercentualReceita() {
		return percentualReceita;
	}

	/**
	 * Set: percentualReceita.
	 *
	 * @param percentualReceita the percentual receita
	 */
	public void setPercentualReceita(BigDecimal percentualReceita) {
		this.percentualReceita = percentualReceita;
	}

	/**
	 * Get: canLibPagtoConsolidadoSemConSaldoImpl.
	 *
	 * @return canLibPagtoConsolidadoSemConSaldoImpl
	 */
	public ICanLibPagtoConsolidadoSemConSaldoService getCanLibPagtoConsolidadoSemConSaldoImpl() {
		return canLibPagtoConsolidadoSemConSaldoImpl;
	}

	/**
	 * Set: canLibPagtoConsolidadoSemConSaldoImpl.
	 *
	 * @param canLibPagtoConsolidadoSemConSaldoImpl the can lib pagto consolidado sem con saldo impl
	 */
	public void setCanLibPagtoConsolidadoSemConSaldoImpl(
			ICanLibPagtoConsolidadoSemConSaldoService canLibPagtoConsolidadoSemConSaldoImpl) {
		this.canLibPagtoConsolidadoSemConSaldoImpl = canLibPagtoConsolidadoSemConSaldoImpl;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: itemSelecionadoListaDetalhe.
	 *
	 * @return itemSelecionadoListaDetalhe
	 */
	public Integer getItemSelecionadoListaDetalhe() {
		return itemSelecionadoListaDetalhe;
	}

	/**
	 * Set: itemSelecionadoListaDetalhe.
	 *
	 * @param itemSelecionadoListaDetalhe the item selecionado lista detalhe
	 */
	public void setItemSelecionadoListaDetalhe(
			Integer itemSelecionadoListaDetalhe) {
		this.itemSelecionadoListaDetalhe = itemSelecionadoListaDetalhe;
	}

	/**
	 * Get: listaGridCanLibPagtoConSemConSaldo.
	 *
	 * @return listaGridCanLibPagtoConSemConSaldo
	 */
	public List<ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO> getListaGridCanLibPagtoConSemConSaldo() {
		return listaGridCanLibPagtoConSemConSaldo;
	}

	/**
	 * Set: listaGridCanLibPagtoConSemConSaldo.
	 *
	 * @param listaGridCanLibPagtoConSemConSaldo the lista grid can lib pagto con sem con saldo
	 */
	public void setListaGridCanLibPagtoConSemConSaldo(
			List<ConsultarCanLibPagtosConsSemConsSaldoSaidaDTO> listaGridCanLibPagtoConSemConSaldo) {
		this.listaGridCanLibPagtoConSemConSaldo = listaGridCanLibPagtoConSemConSaldo;
	}

	/**
	 * Get: listaGridDetCanLibPagtoConSemConSaldo.
	 *
	 * @return listaGridDetCanLibPagtoConSemConSaldo
	 */
	public List<OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO> getListaGridDetCanLibPagtoConSemConSaldo() {
		return listaGridDetCanLibPagtoConSemConSaldo;
	}

	/**
	 * Set: listaGridDetCanLibPagtoConSemConSaldo.
	 *
	 * @param listaGridDetCanLibPagtoConSemConSaldo the lista grid det can lib pagto con sem con saldo
	 */
	public void setListaGridDetCanLibPagtoConSemConSaldo(
			List<OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO> listaGridDetCanLibPagtoConSemConSaldo) {
		this.listaGridDetCanLibPagtoConSemConSaldo = listaGridDetCanLibPagtoConSemConSaldo;
	}

	/**
	 * Get: listaRadios.
	 *
	 * @return listaRadios
	 */
	public List<SelectItem> getListaRadios() {
		return listaRadios;
	}

	/**
	 * Set: listaRadios.
	 *
	 * @param listaRadios the lista radios
	 */
	public void setListaRadios(List<SelectItem> listaRadios) {
		this.listaRadios = listaRadios;
	}

	/**
	 * Get: listaRadiosDetalhe.
	 *
	 * @return listaRadiosDetalhe
	 */
	public List<SelectItem> getListaRadiosDetalhe() {
		return listaRadiosDetalhe;
	}

	/**
	 * Set: listaRadiosDetalhe.
	 *
	 * @param listaRadiosDetalhe the lista radios detalhe
	 */
	public void setListaRadiosDetalhe(List<SelectItem> listaRadiosDetalhe) {
		this.listaRadiosDetalhe = listaRadiosDetalhe;
	}

	/**
	 * Is habilita botao cancelar.
	 *
	 * @return true, if is habilita botao cancelar
	 */
	public boolean isHabilitaBotaoCancelar() {
		return habilitaBotaoCancelar;
	}

	/**
	 * Set: habilitaBotaoCancelar.
	 *
	 * @param habilitaBotaoCancelar the habilita botao cancelar
	 */
	public void setHabilitaBotaoCancelar(boolean habilitaBotaoCancelar) {
		this.habilitaBotaoCancelar = habilitaBotaoCancelar;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Get: listaGridDetCanLibPagtoConSemConSaldoSelecionados.
	 *
	 * @return listaGridDetCanLibPagtoConSemConSaldoSelecionados
	 */
	public List<OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO> getListaGridDetCanLibPagtoConSemConSaldoSelecionados() {
		return listaGridDetCanLibPagtoConSemConSaldoSelecionados;
	}

	/**
	 * Set: listaGridDetCanLibPagtoConSemConSaldoSelecionados.
	 *
	 * @param listaGridDetCanLibPagtoConSemConSaldoSelecionados the lista grid det can lib pagto con sem con saldo selecionados
	 */
	public void setListaGridDetCanLibPagtoConSemConSaldoSelecionados(
			List<OcorrenciasDetalharCanLibPagtosConsSemConsSaldoSaidaDTO> listaGridDetCanLibPagtoConSemConSaldoSelecionados) {
		this.listaGridDetCanLibPagtoConSemConSaldoSelecionados = listaGridDetCanLibPagtoConSemConSaldoSelecionados;
	}

	/**
	 * Get: logoPath.
	 *
	 * @return logoPath
	 */
	public String getLogoPath() {
		return logoPath;
	}

	/**
	 * Set: logoPath.
	 *
	 * @param logoPath the logo path
	 */
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	/**
	 * Get: vlrTotalPagamentos.
	 *
	 * @return vlrTotalPagamentos
	 */
	public BigDecimal getVlrTotalPagamentos() {
		return vlrTotalPagamentos;
	}

	/**
	 * Set: vlrTotalPagamentos.
	 *
	 * @param vlrTotalPagamentos the vlr total pagamentos
	 */
	public void setVlrTotalPagamentos(BigDecimal vlrTotalPagamentos) {
		this.vlrTotalPagamentos = vlrTotalPagamentos;
	}

	/**
	 * Get: cdTipoBeneficiario.
	 *
	 * @return cdTipoBeneficiario
	 */
	public Integer getCdTipoBeneficiario() {
		return cdTipoBeneficiario;
	}

	/**
	 * Get: qtdePagamentos.
	 *
	 * @return qtdePagamentos
	 */
	public Long getQtdePagamentos() {
		return qtdePagamentos;
	}

	/**
	 * Set: qtdePagamentos.
	 *
	 * @param qtdePagamentos the qtde pagamentos
	 */
	public void setQtdePagamentos(Long qtdePagamentos) {
		this.qtdePagamentos = qtdePagamentos;
	}

	/**
	 * Set: cdTipoBeneficiario.
	 *
	 * @param cdTipoBeneficiario the cd tipo beneficiario
	 */
	public void setCdTipoBeneficiario(Integer cdTipoBeneficiario) {
		this.cdTipoBeneficiario = cdTipoBeneficiario;
	}

	/**
	 * Get: qtdePagamentosCancelados.
	 *
	 * @return qtdePagamentosCancelados
	 */
	public Integer getQtdePagamentosCancelados() {
		return qtdePagamentosCancelados;
	}

	/**
	 * Set: qtdePagamentosCancelados.
	 *
	 * @param qtdePagamentosCancelados the qtde pagamentos cancelados
	 */
	public void setQtdePagamentosCancelados(Integer qtdePagamentosCancelados) {
		this.qtdePagamentosCancelados = qtdePagamentosCancelados;
	}

	/**
	 * Get: vlrTotalPagamentosCancelados.
	 *
	 * @return vlrTotalPagamentosCancelados
	 */
	public BigDecimal getVlrTotalPagamentosCancelados() {
		return vlrTotalPagamentosCancelados;
	}

	/**
	 * Set: vlrTotalPagamentosCancelados.
	 *
	 * @param vlrTotalPagamentosCancelados the vlr total pagamentos cancelados
	 */
	public void setVlrTotalPagamentosCancelados(
			BigDecimal vlrTotalPagamentosCancelados) {
		this.vlrTotalPagamentosCancelados = vlrTotalPagamentosCancelados;
	}

	/**
	 * Get: dsOrigemPagamento.
	 *
	 * @return dsOrigemPagamento
	 */
	public String getDsOrigemPagamento() {
		return dsOrigemPagamento;
	}

	/**
	 * Set: dsOrigemPagamento.
	 *
	 * @param dsOrigemPagamento the ds origem pagamento
	 */
	public void setDsOrigemPagamento(String dsOrigemPagamento) {
		this.dsOrigemPagamento = dsOrigemPagamento;
	}

	/**
	 * Get: nossoNumero.
	 *
	 * @return nossoNumero
	 */
	public String getNossoNumero() {
		return nossoNumero;
	}

	/**
	 * Set: nossoNumero.
	 *
	 * @param nossoNumero the nosso numero
	 */
	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Is exibe beneficiario.
	 *
	 * @return true, if is exibe beneficiario
	 */
	public boolean isExibeBeneficiario() {
		return exibeBeneficiario;
	}

	/**
	 * Set: exibeBeneficiario.
	 *
	 * @param exibeBeneficiario the exibe beneficiario
	 */
	public void setExibeBeneficiario(boolean exibeBeneficiario) {
		this.exibeBeneficiario = exibeBeneficiario;
	}

	/**
	 * Get: carteira.
	 *
	 * @return carteira
	 */
	public String getCarteira() {
		return carteira;
	}

	/**
	 * Set: carteira.
	 *
	 * @param carteira the carteira
	 */
	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}

	/**
	 * Get: cdIndentificadorJudicial.
	 *
	 * @return cdIndentificadorJudicial
	 */
	public String getCdIndentificadorJudicial() {
		return cdIndentificadorJudicial;
	}

	/**
	 * Set: cdIndentificadorJudicial.
	 *
	 * @param cdIndentificadorJudicial the cd indentificador judicial
	 */
	public void setCdIndentificadorJudicial(String cdIndentificadorJudicial) {
		this.cdIndentificadorJudicial = cdIndentificadorJudicial;
	}

	/**
	 * Get: cdSituacaoTranferenciaAutomatica.
	 *
	 * @return cdSituacaoTranferenciaAutomatica
	 */
	public String getCdSituacaoTranferenciaAutomatica() {
		return cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Set: cdSituacaoTranferenciaAutomatica.
	 *
	 * @param cdSituacaoTranferenciaAutomatica the cd situacao tranferencia automatica
	 */
	public void setCdSituacaoTranferenciaAutomatica(
			String cdSituacaoTranferenciaAutomatica) {
		this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Get: dtLimiteDescontoPagamento.
	 *
	 * @return dtLimiteDescontoPagamento
	 */
	public String getDtLimiteDescontoPagamento() {
		return dtLimiteDescontoPagamento;
	}

	/**
	 * Set: dtLimiteDescontoPagamento.
	 *
	 * @param dtLimiteDescontoPagamento the dt limite desconto pagamento
	 */
	public void setDtLimiteDescontoPagamento(String dtLimiteDescontoPagamento) {
		this.dtLimiteDescontoPagamento = dtLimiteDescontoPagamento;
	}

	/**
	 * Get: nrRemessa.
	 *
	 * @return nrRemessa
	 */
	public Long getNrRemessa() {
		return nrRemessa;
	}

	/**
	 * Set: nrRemessa.
	 *
	 * @param nrRemessa the nr remessa
	 */
	public void setNrRemessa(Long nrRemessa) {
		this.nrRemessa = nrRemessa;
	}

	/**
	 * Get: descTipoFavorecido.
	 *
	 * @return descTipoFavorecido
	 */
	public String getDescTipoFavorecido() {
		return descTipoFavorecido;
	}

	/**
	 * Set: descTipoFavorecido.
	 *
	 * @param descTipoFavorecido the desc tipo favorecido
	 */
	public void setDescTipoFavorecido(String descTipoFavorecido) {
		this.descTipoFavorecido = descTipoFavorecido;
	}

	/**
	 * Get: dsTipoBeneficiario.
	 *
	 * @return dsTipoBeneficiario
	 */
	public String getDsTipoBeneficiario() {
		return dsTipoBeneficiario;
	}

	/**
	 * Set: dsTipoBeneficiario.
	 *
	 * @param dsTipoBeneficiario the ds tipo beneficiario
	 */
	public void setDsTipoBeneficiario(String dsTipoBeneficiario) {
		this.dsTipoBeneficiario = dsTipoBeneficiario;
	}

	/**
	 * Get: cdBancoOriginal.
	 *
	 * @return cdBancoOriginal
	 */
	public Integer getCdBancoOriginal() {
		return cdBancoOriginal;
	}

	/**
	 * Set: cdBancoOriginal.
	 *
	 * @param cdBancoOriginal the cd banco original
	 */
	public void setCdBancoOriginal(Integer cdBancoOriginal) {
		this.cdBancoOriginal = cdBancoOriginal;
	}

	/**
	 * Get: dsBancoOriginal.
	 *
	 * @return dsBancoOriginal
	 */
	public String getDsBancoOriginal() {
		return dsBancoOriginal;
	}

	/**
	 * Set: dsBancoOriginal.
	 *
	 * @param dsBancoOriginal the ds banco original
	 */
	public void setDsBancoOriginal(String dsBancoOriginal) {
		this.dsBancoOriginal = dsBancoOriginal;
	}

	/**
	 * Get: cdAgenciaBancariaOriginal.
	 *
	 * @return cdAgenciaBancariaOriginal
	 */
	public Integer getCdAgenciaBancariaOriginal() {
		return cdAgenciaBancariaOriginal;
	}

	/**
	 * Set: cdAgenciaBancariaOriginal.
	 *
	 * @param cdAgenciaBancariaOriginal the cd agencia bancaria original
	 */
	public void setCdAgenciaBancariaOriginal(Integer cdAgenciaBancariaOriginal) {
		this.cdAgenciaBancariaOriginal = cdAgenciaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoAgenciaOriginal.
	 *
	 * @return cdDigitoAgenciaOriginal
	 */
	public String getCdDigitoAgenciaOriginal() {
		return cdDigitoAgenciaOriginal;
	}

	/**
	 * Set: cdDigitoAgenciaOriginal.
	 *
	 * @param cdDigitoAgenciaOriginal the cd digito agencia original
	 */
	public void setCdDigitoAgenciaOriginal(String cdDigitoAgenciaOriginal) {
		this.cdDigitoAgenciaOriginal = cdDigitoAgenciaOriginal;
	}

	/**
	 * Get: dsAgenciaOriginal.
	 *
	 * @return dsAgenciaOriginal
	 */
	public String getDsAgenciaOriginal() {
		return dsAgenciaOriginal;
	}

	/**
	 * Set: dsAgenciaOriginal.
	 *
	 * @param dsAgenciaOriginal the ds agencia original
	 */
	public void setDsAgenciaOriginal(String dsAgenciaOriginal) {
		this.dsAgenciaOriginal = dsAgenciaOriginal;
	}

	/**
	 * Get: cdContaBancariaOriginal.
	 *
	 * @return cdContaBancariaOriginal
	 */
	public Long getCdContaBancariaOriginal() {
		return cdContaBancariaOriginal;
	}

	/**
	 * Set: cdContaBancariaOriginal.
	 *
	 * @param cdContaBancariaOriginal the cd conta bancaria original
	 */
	public void setCdContaBancariaOriginal(Long cdContaBancariaOriginal) {
		this.cdContaBancariaOriginal = cdContaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoContaOriginal.
	 *
	 * @return cdDigitoContaOriginal
	 */
	public String getCdDigitoContaOriginal() {
		return cdDigitoContaOriginal;
	}

	/**
	 * Set: cdDigitoContaOriginal.
	 *
	 * @param cdDigitoContaOriginal the cd digito conta original
	 */
	public void setCdDigitoContaOriginal(String cdDigitoContaOriginal) {
		this.cdDigitoContaOriginal = cdDigitoContaOriginal;
	}

	/**
	 * Get: dsTipoContaOriginal.
	 *
	 * @return dsTipoContaOriginal
	 */
	public String getDsTipoContaOriginal() {
		return dsTipoContaOriginal;
	}

	/**
	 * Set: dsTipoContaOriginal.
	 *
	 * @param dsTipoContaOriginal the ds tipo conta original
	 */
	public void setDsTipoContaOriginal(String dsTipoContaOriginal) {
		this.dsTipoContaOriginal = dsTipoContaOriginal;
	}

	/**
	 * Get: dtDevolucaoEstorno.
	 *
	 * @return dtDevolucaoEstorno
	 */
	public String getDtDevolucaoEstorno() {
		return dtDevolucaoEstorno;
	}

	/**
	 * Set: dtDevolucaoEstorno.
	 *
	 * @param dtDevolucaoEstorno the dt devolucao estorno
	 */
	public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
		this.dtDevolucaoEstorno = dtDevolucaoEstorno;
	}

	/**
	 * Get: dtLimitePagamento.
	 *
	 * @return dtLimitePagamento
	 */
	public String getDtLimitePagamento() {
		return dtLimitePagamento;
	}

	/**
	 * Set: dtLimitePagamento.
	 *
	 * @param dtLimitePagamento the dt limite pagamento
	 */
	public void setDtLimitePagamento(String dtLimitePagamento) {
		this.dtLimitePagamento = dtLimitePagamento;
	}

	/**
	 * Get: dsRastreado.
	 *
	 * @return dsRastreado
	 */
	public String getDsRastreado() {
		return dsRastreado;
	}

	/**
	 * Set: dsRastreado.
	 *
	 * @param dsRastreado the ds rastreado
	 */
	public void setDsRastreado(String dsRastreado) {
		this.dsRastreado = dsRastreado;
	}

	/**
	 * Get: dtFloatingPagamento.
	 *
	 * @return dtFloatingPagamento
	 */
	public String getDtFloatingPagamento() {
		return dtFloatingPagamento;
	}

	/**
	 * Set: dtFloatingPagamento.
	 *
	 * @param dtFloatingPagamento the dt floating pagamento
	 */
	public void setDtFloatingPagamento(String dtFloatingPagamento) {
		this.dtFloatingPagamento = dtFloatingPagamento;
	}

	/**
	 * Get: dtEfetivFloatPgto.
	 *
	 * @return dtEfetivFloatPgto
	 */
	public String getDtEfetivFloatPgto() {
		return dtEfetivFloatPgto;
	}

	/**
	 * Set: dtEfetivFloatPgto.
	 *
	 * @param dtEfetivFloatPgto the dt efetiv float pgto
	 */
	public void setDtEfetivFloatPgto(String dtEfetivFloatPgto) {
		this.dtEfetivFloatPgto = dtEfetivFloatPgto;
	}

	/**
	 * Get: vlFloatingPagamento.
	 *
	 * @return vlFloatingPagamento
	 */
	public BigDecimal getVlFloatingPagamento() {
		return vlFloatingPagamento;
	}

	/**
	 * Set: vlFloatingPagamento.
	 *
	 * @param vlFloatingPagamento the vl floating pagamento
	 */
	public void setVlFloatingPagamento(BigDecimal vlFloatingPagamento) {
		this.vlFloatingPagamento = vlFloatingPagamento;
	}

	/**
	 * Get: cdOperacaoDcom.
	 *
	 * @return cdOperacaoDcom
	 */
	public Long getCdOperacaoDcom() {
		return cdOperacaoDcom;
	}

	/**
	 * Set: cdOperacaoDcom.
	 *
	 * @param cdOperacaoDcom the cd operacao dcom
	 */
	public void setCdOperacaoDcom(Long cdOperacaoDcom) {
		this.cdOperacaoDcom = cdOperacaoDcom;
	}

	/**
	 * Get: dsSituacaoDcom.
	 *
	 * @return dsSituacaoDcom
	 */
	public String getDsSituacaoDcom() {
		return dsSituacaoDcom;
	}

	/**
	 * Set: dsSituacaoDcom.
	 *
	 * @param dsSituacaoDcom the ds situacao dcom
	 */
	public void setDsSituacaoDcom(String dsSituacaoDcom) {
		this.dsSituacaoDcom = dsSituacaoDcom;
	}

	/**
	 * Get: exibeDcom.
	 *
	 * @return exibeDcom
	 */
	public Boolean getExibeDcom() {
		return exibeDcom;
	}

	/**
	 * Set: exibeDcom.
	 *
	 * @param exibeDcom the exibe dcom
	 */
	public void setExibeDcom(Boolean exibeDcom) {
		this.exibeDcom = exibeDcom;
	}

	/**
	 * Get: numControleInternoLote.
	 *
	 * @return numControleInternoLote
	 */
	public Long getNumControleInternoLote() {
		return numControleInternoLote;
	}

	/**
	 * Set: numControleInternoLote.
	 *
	 * @param numControleInternoLote the num controle interno lote
	 */
	public void setNumControleInternoLote(Long numControleInternoLote) {
		this.numControleInternoLote = numControleInternoLote;
	}

}