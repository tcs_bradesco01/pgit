/*
 * Nome: br.com.bradesco.web.pgit.view.bean.pesquisacontratonegociacao
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.pesquisacontratonegociacao;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;

/**
 * Nome: PesquisaContratoNegociacaoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class PesquisaContratoNegociacaoBean {

	// Atributos
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;
	
	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

	// M�todos
	/**
	 * Iniciar pagina.
	 *
	 * @return the string
	 */
	public String iniciarPagina() {

		this.identificacaoClienteContratoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean
				.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela de pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean
				.setPaginaCliente("identificacaoClienteContratoNegociacao");
		identificacaoClienteContratoBean
				.setPaginaRetorno("pesquisaContratoNegociacao");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean
				.setHabilitaEmpresaGestoraTipoContrato(true);

		return "pesquisaContratoNegociacao";
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		return "VOLTAR";
	}

	/**
	 * Selecionar.
	 */
	public void selecionar() {

		FacesContext facesContext = FacesContext.getCurrentInstance();

		HttpServletRequest request = (HttpServletRequest) facesContext
				.getExternalContext().getRequest();

		/*
		 * String url =facesContext.getExternalContext().encodeResourceURL(
		 * "http://localhost:8080/marq/conArquivoRetornoAux.jsf?" +
		 * "codigo=10");
		 */

		String url = "http://"
				+ request.getServerName()
				+ ":"
				+ request.getServerPort()
				+ "/negociacao/xxxx.jsf?<<parametros de retorno do registro selecionado>>";

		url = facesContext.getExternalContext().encodeResourceURL(url);

		try {
			facesContext.getExternalContext().redirect(url);
		} catch (Exception error) {
			BradescoFacesUtils.addInfoModalMessage(error.getMessage(), false);
		}

	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 */
	public void pesquisar(ActionEvent evt) {
		return;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

}
