/*
 * Nome: br.com.bradesco.web.pgit.view.converters.manutencaoApoioProduto.cadTipoPendencia
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.converters.manutencaoApoioProduto.cadTipoPendencia;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import br.com.bradesco.web.pgit.service.business.cadtipopendencia.bean.TipoPendenciaSaidaDTO;

/**
 * Nome: CadTipoPendenciaConveter
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class CadTipoPendenciaConveter implements Converter {
	
	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		if (value == null || value.trim().equals("")) {
			return null;
		}
		
		String[] arrAgenda = value.split(";");
		if (arrAgenda == null || arrAgenda.length != 2) {
			return null;
		}

		TipoPendenciaSaidaDTO consulta = new TipoPendenciaSaidaDTO();
		consulta.setCdPendenciaPagamentoIntegrado(Integer.parseInt(arrAgenda[0]));
		consulta.setCdTipoUnidadeOrganizacional(Integer.parseInt(arrAgenda[1]));
		
		return consulta;
	}

	/**
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		if (value == null) {
			return null;
		}

		TipoPendenciaSaidaDTO consulta = (TipoPendenciaSaidaDTO) value;
		StringBuilder consultaIncAltExcTipoPendencia = new StringBuilder();
		consultaIncAltExcTipoPendencia.append(consulta.getCdPendenciaPagamentoIntegrado());
		consultaIncAltExcTipoPendencia.append(";");
		consultaIncAltExcTipoPendencia.append(consulta.getCdTipoUnidadeOrganizacional());
		
		return consultaIncAltExcTipoPendencia.toString();
	}

}