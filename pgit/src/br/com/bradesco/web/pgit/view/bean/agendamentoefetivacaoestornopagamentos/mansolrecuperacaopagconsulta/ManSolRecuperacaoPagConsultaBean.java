/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.mansolrecuperacaopagconsulta
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.mansolrecuperacaopagconsulta;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jdt.internal.core.util.Util;

import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.Messages;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.IManSolRecuperacaoPagConsultaService;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarSolRecuperacaoPagtosBackupEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ConsultarSolRecuperacaoPagtosBackupSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ExcluirSolRecPagtosVencidoNaoPagoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.mansolrecuperacaopagconsulta.bean.ExcluirSolRecPagtosVencidoNaoPagoSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;

/**
 * Nome: ManSolRecuperacaoPagConsultaBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManSolRecuperacaoPagConsultaBean {
	/* Camada de Implementa��o */
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo manSolRecuperacaoPagConsultaServiceImpl. */
	private IManSolRecuperacaoPagConsultaService manSolRecuperacaoPagConsultaServiceImpl;
	
	/** Atributo listaConsulta. */
	private List<ConsultarSolRecuperacaoPagtosBackupSaidaDTO> listaConsulta;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle;
	
	/** Atributo itemSelecionado. */
	private Integer itemSelecionado;
	
	/** Atributo foco. */
	private String foco;
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	/** Atributo chkServicoModalidade. */
	private boolean chkServicoModalidade;
	
	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;
	
	/** Atributo modalidadeFiltro. */
	private Integer modalidadeFiltro;	
	
	/** Atributo listaTipoServicoFiltro. */
	private List<SelectItem> listaTipoServicoFiltro = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeFiltro. */
	private List<SelectItem> listaModalidadeFiltro = new ArrayList<SelectItem>();	
	
	/** Atributo dataInicialSolicitacaoFiltro. */
	private Date dataInicialSolicitacaoFiltro;
	
	/** Atributo dataFinalSolicitacaoFiltro. */
	private Date dataFinalSolicitacaoFiltro;
	
	/** Atributo listaConsultarSituacaoPagamentoFiltro. */
	private List<SelectItem> listaConsultarSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
	
	/** Atributo chkSituacaoSolicitacao. */
	private boolean chkSituacaoSolicitacao;
	
	/** Atributo listaConsultarMotivoSituacaoPagamentoFiltro. */
	private List<SelectItem> listaConsultarMotivoSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
	
	/** Atributo cboMotivoSituacaoFiltro. */
	private Integer cboMotivoSituacaoFiltro;
	
	/** Atributo cboSituacaoPagamentoFiltro. */
	private Integer cboSituacaoPagamentoFiltro;

	private Integer numeroSolicitacao;
	
	/* Detalhar */
	//Cabe�alho
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo nroContrato. */
	private String nroContrato;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;
	
	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;
	
	/** Atributo dsNumero. */
	private String dsNumero;
	
	/** Atributo dsSituacaoSolicitacao. */
	private String dsSituacaoSolicitacao;
	
	/** Atributo dsMotivoSolicitacao. */
	private String dsMotivoSolicitacao;
	
	/** Atributo dsDataSolicitacao. */
	private String dsDataSolicitacao;
	
	/** Atributo dsDataProgramadaRecuperacao. */
	private String dsDataProgramadaRecuperacao;
	
	/** Atributo dsDataNovoVencimento. */
	private String dsDataNovoVencimento;
	
	/** Atributo dsDataInicioPesquisa. */
	private String dsDataInicioPesquisa;
	
	/** Atributo dsDataFinalPesquisa. */
	private String dsDataFinalPesquisa;
	
	/** Atributo dsTipoServico. */
	private String dsTipoServico;
	
	/** Atributo dsModalidade. */
	private String dsModalidade;
	
	/** Atributo dsRemessa. */
	private String dsRemessa;
	
	/** Atributo dsLinhaDigitavel. */
	private String dsLinhaDigitavel;
	
	/** Atributo dsSituacaoPagamento. */
	private String dsSituacaoPagamento;
	
	/** Atributo dsMotivoSituacaoPagamento. */
	private String dsMotivoSituacaoPagamento;
	
	/** Atributo dsNumeroPagtoInicial. */
	private String dsNumeroPagtoInicial;
	
	/** Atributo dsNumeroPagtoFinal. */
	private String dsNumeroPagtoFinal;
	
	/** Atributo dsValorPagtoInicial. */
	private BigDecimal dsValorPagtoInicial;
	
	/** Atributo dsValorPagtoFinal. */
	private BigDecimal dsValorPagtoFinal;
	
	/** Atributo dsClubPessoa. */
	private String dsClubPessoa;
	
	/** Atributo dsNomePessoa. */
	private String dsNomePessoa;
	
	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;
	
	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;
	
	/** Atributo contaDebito. */
	private String contaDebito;
	
	/** Atributo tipoContaDebito. */
	private String tipoContaDebito;
	
	/** Atributo numeroInscricaoFavorecido. */
	private String numeroInscricaoFavorecido;
	
	/** Atributo tipoFavorecido. */
	private Integer tipoFavorecido;
	
	/** Atributo descTipoFavorecido. */
	private String descTipoFavorecido;
	
	/** Atributo dsFavorecido. */
	private String dsFavorecido;
	
	/** Atributo cdFavorecido. */
	private String cdFavorecido;
	
	/** Atributo dsBancoFavorecido. */
	private String dsBancoFavorecido;
	
	/** Atributo dsAgenciaFavorecido. */
	private String dsAgenciaFavorecido;
	
	/** Atributo dsContaFavorecido. */
	private String dsContaFavorecido;
	
	/** Atributo dsDigContaFavorecido. */
	private String dsDigContaFavorecido;
	
	/** Atributo tipoContaFavorecido. */
	private String tipoContaFavorecido;	
	
	/* Trilha Auditoria */
	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;
	
	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;
	
	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;
	
	/** Atributo complementoInclusao. */
	private String complementoInclusao;
	
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;
	
	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;
	
	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;
	
	/** Atributo complementoManutencao. */
	private String complementoManutencao;
	
	private Integer bancoSalario;
	private Integer agenciaSalario;
	private Long contaSalario;
	private String digitoSalario;
	
	private Integer destinoPagamento;
	private String dsDestinoPagamento;
	
	private Integer bancoPgtoDestino;
	private String ispbPgtoDestino;
	private Long contaPgtoDestino;
	
	private Long loteInterno;

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	
	public void iniciarTela(ActionEvent evt){
		//Tela de Filtro
		filtroAgendamentoEfetivacaoEstornoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		filtroAgendamentoEfetivacaoEstornoBean.setPaginaRetorno("conManSolRecuperacaoPagConsulta");
		
		setListaConsulta(null);
		setItemSelecionado(null);
		
		limparCampos();
		
		//Carregamento dos combos
		listarTipoServicoPagto();
		listarConsultarSituacaoPagamento();
		
		setHabilitaArgumentosPesquisa(false);
		
		setDisableArgumentosConsulta(false);
		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
		filtroAgendamentoEfetivacaoEstornoBean.setEmpresaGestoraFiltro(2269651L);
	}	
	
	/**
	 * Limpar informacao consulta.
	 */
	public void limparInformacaoConsulta(){
		setListaConsulta(null);
		setItemSelecionado(null);		setDisableArgumentosConsulta(false);
	}	
	
	/**
	 * Consultar.
	 *
	 * @param evt the evt
	 */
	public void consultar(ActionEvent evt){
		consultar();
	}
	
	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar(){
		try{
			limparInformacaoConsulta();
			
			ConsultarSolRecuperacaoPagtosBackupEntradaDTO entrada = new ConsultarSolRecuperacaoPagtosBackupEntradaDTO();
			
			entrada.setCdFinalidadeRecuperacao(2);
			entrada.setCdSelecaoRecuperacao(0);
			entrada.setCdSolicitacaoPagamento(9);
			entrada.setCdTipoContaDebito(0);
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			entrada.setDtFimPesquisa((getDataFinalSolicitacaoFiltro() == null) ? "" : sdf.format(getDataFinalSolicitacaoFiltro()) );
			entrada.setDtInicioPesquisa((getDataInicialSolicitacaoFiltro() == null) ? "" : sdf.format(getDataInicialSolicitacaoFiltro()));	
			
			entrada.setCdProduto(getTipoServicoFiltro());
			entrada.setCdProdutoRelacionado(getModalidadeFiltro());
			entrada.setCdSituacaoSolicitacaoPagamento(getCboSituacaoPagamentoFiltro());
			entrada.setCdMotivoSolicitacao(getCboMotivoSituacaoFiltro());
		    entrada.setNmrSoltcPgto(PgitUtil.verificaIntegerNulo(getNumeroSolicitacao()));
		        
			if ( PgitUtil.verificaStringNula(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado()).equals("2") ){
				/* Se selecionado �Filtrar por Conta D�bito� mover zeros para os cambos abaixo */
				entrada.setCdPessoaJuridicaContrato(0L);
				entrada.setCdTipoContratoNegocio(0);
				entrada.setNrSequenciaContratoNegocio(0L);
				entrada.setCdBancoDebito(filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro());
				entrada.setCdAgenciaDebito(filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro());
				entrada.setCdContaDebito(filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro());
				entrada.setCdDigitoContaDebito(filtroAgendamentoEfetivacaoEstornoBean.getDigitoContaDebitoFiltro());
				entrada.setCdDigitoAgenciaDebito(0);
			}else{
				entrada.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato() != null ? filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato()	: 0L);
				entrada.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio() != null ? filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio() : 0);
				entrada.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio() != null ? filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio() : 0L);
				entrada.setCdBancoDebito(0);
				entrada.setCdAgenciaDebito(0);
				entrada.setCdContaDebito(0L);
				entrada.setCdDigitoAgenciaDebito(0);
				entrada.setCdDigitoContaDebito("");					
			}
			
			setListaConsulta(getManSolRecuperacaoPagConsultaServiceImpl().consultarSolRecuperacaoPagtosBackup(entrada));
			
			setListaControle(new ArrayList<SelectItem>());
			
			for (int i = 0; i < getListaConsulta().size();i++) {
				getListaControle().add(new SelectItem(i," "));
				
			}
			
			setDisableArgumentosConsulta(true);
		
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setDisableArgumentosConsulta(false);
		}		
		
		return "";
	}
	
	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt){
		consultar();
		return "";
	}
	
	/**
	 * Carrega cabecalho.
	 */
	private void carregaCabecalho(){
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");	
		if(filtroAgendamentoEfetivacaoEstornoBean.isClienteContratoSelecionado()){
			if(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("0")){ //Filtrar por cliente
				setNrCnpjCpf(String.valueOf(filtroAgendamentoEfetivacaoEstornoBean.getNrCpfCnpjCliente()));
				setDsRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoRazaoSocialCliente());
				setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean.getEmpresaGestoraDescCliente());
				setNroContrato(filtroAgendamentoEfetivacaoEstornoBean.getNumeroDescCliente());
				setDsContrato(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoContratoDescCliente());
				setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getSituacaoDescCliente());
			}else{
				if(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("1")){ //Filtrar por contrato
					setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean.getEmpresaGestoraDescContrato());
					setNroContrato(filtroAgendamentoEfetivacaoEstornoBean.getNumeroDescContrato());
					setDsContrato(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoContratoDescContrato());
					setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getSituacaoDescContrato());
				}
			}
		}
	}	
	
	/**
	 * Preenche dados.
	 */
	public void preencheDados(){
		
		carregaCabecalho();
		ConsultarSolRecuperacaoPagtosBackupSaidaDTO registroSelecionado = getListaConsulta().get(getItemSelecionado());
		
		ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO entrada = new ConsultarPagtosSolicitacaoRecuperacaoEntradaDTO();
		entrada.setCdSolicitacaoPagamento(getListaConsulta().get(getItemSelecionado()).getCdSolicitacaoPagamento());
		entrada.setNrSolicitacaoPagamento(getListaConsulta().get(getItemSelecionado()).getNrSolicitacaoPagamento());
		
		ConsultarPagtosSolicitacaoRecuperacaoSaidaDTO saida = getManSolRecuperacaoPagConsultaServiceImpl().consultarPagtosSolicitacaoRecuperacao(entrada);
		
		//Sobrescrevendo os dados do cliente a pedido da Kelli - email Manter Solicita��o de Recupera��o de Pagamentos para Consulta - Backup - quarta-feira, 17 de agosto de 2011 9:33
		setNrCnpjCpf(registroSelecionado.getCdCpfCnpjParticipante());
		setDsRazaoSocial(registroSelecionado.getDsRazaoSocial());

		setDsSituacaoSolicitacao(registroSelecionado.getDsSituacaoSolicitacao());
		setDsMotivoSolicitacao(registroSelecionado.getDsMotivoSolicitacao());
		setDsSituacaoPagamento(saida.getDsSituacaoPagamento());
		setDsMotivoSituacaoPagamento(saida.getDsMotivoSituacaoPagamento());
		setDsNumero(registroSelecionado.getNrSolicitacaoPagamento() != null ? registroSelecionado.getNrSolicitacaoPagamento().toString(): "");
		setDsDataSolicitacao(registroSelecionado.getDsDataSolicitacao());
		setDsDataProgramadaRecuperacao(saida.getDtRecuperacaoPagamento());
		setDsDataNovoVencimento(saida.getDtNovoVencimentoPagamento());
		setDsDataInicioPesquisa(saida.getDtInicioPeriodoPesquisa());
		setDsDataFinalPesquisa(saida.getDtFimPeriodoPesquisa());
		setDsTipoServico(registroSelecionado.getDsResumoProdutoServico());
		setDsModalidade(registroSelecionado.getDsResumoServicoRelacionado());
		setDsRemessa((String)PgitUtil.verificaZero(String.valueOf(saida.getNrSequenciaArquivoRemessa())));
		
		if  (saida.getLoteInterno() != null && saida.getLoteInterno() != 0) {
			setLoteInterno(saida.getLoteInterno());
		} else {
			setLoteInterno(null);
		}
		
		setDsLinhaDigitavel(PgitUtil.formatarLinhaDigitavel(saida.getLinhaDigitavel()));
		setDsNumeroPagtoInicial((String)PgitUtil.verificaZero(saida.getCdControlePagamentoInicial()));
		setDsNumeroPagtoFinal((String)PgitUtil.verificaZero(saida.getCdControlePagamentoFinal()));
		setDsValorPagtoInicial(PgitUtil.verificaBigDecimalNulo((saida.getVlrInicio())));
		setDsValorPagtoFinal(PgitUtil.verificaBigDecimalNulo((saida.getVlFim())));
		setDsClubPessoa((String)PgitUtil.verificaZero(saida.getCdPessoa().toString()));
		setDsNomePessoa(saida.getDsPessoas());
		setDsBancoDebito(PgitUtil.formatBanco(saida.getCdBancoDebito() ,saida.getDsBancoDebito(), false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(saida.getCdAgenciaDebito(),saida.getCdDigitoAgenciaDebito(), saida.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(saida.getCdContaDebito(), saida.getCdDigitoContaDebito(), false));
		setTipoContaDebito(saida.getDsContaDebito());
		
		if (saida.getCdBancoCredito().equals(0)){
			setDsBancoFavorecido("");
			setDsAgenciaFavorecido("");
			setDsContaFavorecido("");
			setDsDigContaFavorecido("");
		} else {
			setDsBancoFavorecido(PgitUtil.formatBanco(saida.getCdBancoCredito(), saida.getDsBancoCredito(), false));
			setDsAgenciaFavorecido(PgitUtil.formatAgencia(saida.getCdAgenciaCredito(), saida.getCdDigitoAgenciaCredito(), saida.getDsAgenciaCredito(), false));
			setDsContaFavorecido(Long.toString(saida.getCdContaCredito()));
			setDsDigContaFavorecido(saida.getDigitoContaCredito());
		}
		
		setTipoContaFavorecido(saida.getDsContaCredito());
		setNumeroInscricaoFavorecido(saida.getCpfCnpjFormatado());
		
		setTipoFavorecido(saida.getCdTipoInscricaoRecebedor());
		setDescTipoFavorecido(validaTipoFavorecidoSolicitacao());
		setDsFavorecido(saida.getDsNomeFavorecido());
		
		setCdFavorecido(saida.getCdFavorecidoCliente());
		
		if (saida.getBancoSalario().equals(0)){
			setBancoSalario(null);
			setAgenciaSalario(null);
			setContaSalario(null);
			setDigitoSalario(null);
		} else {
			setBancoSalario(saida.getBancoSalario());
			setAgenciaSalario(saida.getAgenciaSalario());
			setContaSalario(saida.getContaSalario());
			setDigitoSalario(saida.getDigitoSalario());
		}
    	
        if (saida.getBancoPgtoDestino().equals(0)){
        	setBancoPgtoDestino(null);
        	setIspbPgtoDestino(null);
        	setContaPgtoDestino(null);
        } else {
        	setBancoPgtoDestino(saida.getBancoPgtoDestino());
        	setIspbPgtoDestino(saida.getIspbPgtoDestino());
        	setContaPgtoDestino(saida.getContaPgtoDestino());
        }
        
        switch (saida.getDestinoPagamento()) {
		case 1: 
			setDsDestinoPagamento("Relat�rio");
			break;

		case 2:
			setDsDestinoPagamento("Arquivo ISD");
			break;
			
		case 3:
			setDsDestinoPagamento("Base Online");
			break;
			
		default:
			setDsDestinoPagamento("");
			break;
		}
        
		//trilhaAuditoria
		setDataHoraInclusao(PgitUtil.concatenarCampos(saida.getDtInclusao() ,saida.getHrInclusao()));
		setUsuarioInclusao(saida.getCdUsuarioInclusao());
		setTipoCanalInclusao(PgitUtil.concatenarCampos(saida.getCdTipoCanalInclusao(),saida.getDsCanalInclusao()));
		setComplementoInclusao(saida.getCdOperacaoCanalInclusao() == null || saida.getCdOperacaoCanalInclusao().equals("0") ? "" : saida.getCdOperacaoCanalInclusao() );
		setDataHoraManutencao(PgitUtil.concatenarCampos(saida.getDtManutencao(), saida.getHrManutencao()));
		setUsuarioManutencao(saida.getCdUsuarioManutencao()); 
		setTipoCanalManutencao(PgitUtil.concatenarCampos(saida.getCdTipoCanalManutencao(), saida.getDsCanalManutencao()));
		setComplementoManutencao(saida.getCdOperacaoCanalManutencao() == null || saida.getCdOperacaoCanalManutencao().equals("0") ? "" : saida.getCdOperacaoCanalManutencao());

			
				
	}
	
	/**
	 * Listar tipo servico pagto.
	 */
	public void listarTipoServicoPagto(){
		try{
			this.listaTipoServicoFiltro = new ArrayList<SelectItem>();
			
			List<ConsultarServicoPagtoSaidaDTO> listaServico = new ArrayList<ConsultarServicoPagtoSaidaDTO>();
			
			listaServico = comboService.consultarServicoPagto();
			
			for(ConsultarServicoPagtoSaidaDTO combo : listaServico){
				listaTipoServicoFiltro.add(new SelectItem(combo.getCdServico(),combo.getDsServico()));
			}
		}catch(PdcAdapterFunctionalException e){
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
		}
	}
	
	/**
	 * Limpar tela principal.
	 */
	public void limparTelaPrincipal(){
		limparFiltroPrincipal();

		limparCampos();	
	}
	
	/**
	 * Limpar filtro principal.
	 */
	public void limparFiltroPrincipal(){
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		
		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);
	}
	
	
	/**
	 * Limpar grid.
	 *
	 * @return the string
	 */
	public String limparGrid(){
		setListaConsulta(null);
		setItemSelecionado(null);
		setDisableArgumentosConsulta(false);
		return "";
	}	
	
	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente(){
		//Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		
		setListaConsulta(null);
		setItemSelecionado(null);
		
		limparCampos();
		
		return "";
	}
	
	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt){
		limparCampos();
		setListaConsulta(null);
		setItemSelecionado(null);		
	}
	
	/**
	 * Controlar argumentos pesquisa.
	 */
	public void controlarArgumentosPesquisa(){
		if (filtroAgendamentoEfetivacaoEstornoBean.getBancoContaDebitoFiltro() == null ||
		filtroAgendamentoEfetivacaoEstornoBean.getAgenciaContaDebitoBancariaFiltro() == null || 
		filtroAgendamentoEfetivacaoEstornoBean.getContaBancariaContaDebitoFiltro() == null ){
			setHabilitaArgumentosPesquisa(true);
			limparCampos();
		}
		else{
			setHabilitaArgumentosPesquisa(true);
		}
	}	
	
	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos(){
		setListaConsulta(null);
		setItemSelecionado(null);
		setDisableArgumentosConsulta(false);
		
		setDataInicialSolicitacaoFiltro(new Date());
		setDataFinalSolicitacaoFiltro(new Date());
		setChkServicoModalidade(false);
		setChkSituacaoSolicitacao(false);
		setNumeroSolicitacao(null);
		setModalidadeFiltro(null);
		setListaModalidadeFiltro(new ArrayList<SelectItem>());
		setTipoServicoFiltro(null);
		setCboMotivoSituacaoFiltro(null);
		setListaConsultarMotivoSituacaoPagamentoFiltro(new ArrayList<SelectItem>());
		setCboSituacaoPagamentoFiltro(null);
		
		return "";
	}
	
	/**
	 * Limpar tela.
	 *
	 * @return the string
	 */
	public String limparTela(){
		limparCampos();
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado("");
		filtroAgendamentoEfetivacaoEstornoBean.setItemFiltroSelecionado("");

		return "";
	}
	
	/**
	 * Controle servico modalidade.
	 */
	public void controleServicoModalidade(){
		setModalidadeFiltro(null);
		setTipoServicoFiltro(null);
		listaModalidadeFiltro = new ArrayList<SelectItem>();
	}	
	
	/**
	 * Controle situacao motivo solicitacao.
	 */
	public void controleSituacaoMotivoSolicitacao(){
		setCboSituacaoPagamentoFiltro(null);
		setCboMotivoSituacaoFiltro(null);
		setNumeroSolicitacao(null);
		setListaConsultarMotivoSituacaoPagamentoFiltro(new ArrayList<SelectItem>());
	}	
	
	/**
	 * Listar consultar situacao pagamento.
	 */
	public void listarConsultarSituacaoPagamento(){
		try{
			listaConsultarSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
			
			ConsultarSituacaoSolicitacaoEstornoEntradaDTO entrada = new ConsultarSituacaoSolicitacaoEstornoEntradaDTO();
			
			entrada.setCdSituacao(0);
			entrada.setNumeroOcorrencias(30);
			entrada.setCdIndicador(2);
			
			List<ConsultarSituacaoSolicitacaoEstornoSaidaDTO> list = comboService.consultarSituacaoSolicitacaoEstorno(entrada);
	
			for (ConsultarSituacaoSolicitacaoEstornoSaidaDTO saida : list){
				listaConsultarSituacaoPagamentoFiltro.add(new SelectItem(saida.getCodigo(), saida.getDescricao()));
			}
		}catch(PdcAdapterFunctionalException p){
			listaConsultarSituacaoPagamentoFiltro = new ArrayList<SelectItem>();
		}
	}		
	
	/**
	 * Listar consultar motivo pagamento.
	 */
	public void listarConsultarMotivoPagamento(){
		try{
			setCboMotivoSituacaoFiltro(null);
			
			setListaConsultarMotivoSituacaoPagamentoFiltro(new ArrayList<SelectItem>());
			setCboMotivoSituacaoFiltro(null);
			
			ConsultarMotivoSituacaoEstornoEntradaDTO entrada = new ConsultarMotivoSituacaoEstornoEntradaDTO();
			entrada.setCdSituacao(getCboSituacaoPagamentoFiltro() != null ? getCboSituacaoPagamentoFiltro() : 0);
			
			List<ConsultarMotivoSituacaoEstornoSaidaDTO> list = getComboService().consultarMotivoSituacaoEstornoSaida(entrada);
	
			for (ConsultarMotivoSituacaoEstornoSaidaDTO saida : list){
				getListaConsultarMotivoSituacaoPagamentoFiltro().add(new SelectItem(saida.getCodigo(), saida.getDescricao()));
			}
		}catch (PdcAdapterFunctionalException p){
			setListaConsultarMotivoSituacaoPagamentoFiltro(new ArrayList<SelectItem>());			
		}			
	}
	
	/**
	 * Listar modalidades pagto.
	 */
	public void listarModalidadesPagto(){
		try{
			listaModalidadeFiltro = new ArrayList<SelectItem>();
			
			if ( getTipoServicoFiltro() != null &&  getTipoServicoFiltro()!=0){
				List<ConsultarModalidadePagtoSaidaDTO> listaModalidades = new ArrayList<ConsultarModalidadePagtoSaidaDTO>();
			
				listaModalidades = comboService.consultarModalidadePagto(getTipoServicoFiltro());
	
				for(ConsultarModalidadePagtoSaidaDTO combo : listaModalidades){
					this.listaModalidadeFiltro.add(new SelectItem(combo.getCdProdutoOperacaoRelacionado(),combo.getDsProdutoOperacaoRelacionado()));
				}
			}else{
				listaModalidadeFiltro = new ArrayList<SelectItem>();
				setModalidadeFiltro(null);
				
			}
		}catch(PdcAdapterFunctionalException e){
			listaModalidadeFiltro = new ArrayList<SelectItem>();
		}
	}	
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		try{
			preencheDados();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
		return "detManSolRecuperacaoPagConsulta";
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		try{
			preencheDados();
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return "";
		}
		
		return "excManSolRecuperacaoPagConsulta";
	}	
	
	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir(){
		try{
			ExcluirSolRecPagtosVencidoNaoPagoEntradaDTO entradaDTO = new ExcluirSolRecPagtosVencidoNaoPagoEntradaDTO();
			entradaDTO.setCdSolicitacaoPagamento(getListaConsulta().get(getItemSelecionado()).getCdSolicitacaoPagamento());
			entradaDTO.setNrSolicitacaoPagamento(getListaConsulta().get(getItemSelecionado()).getNrSolicitacaoPagamento()); 
		
			ExcluirSolRecPagtosVencidoNaoPagoSaidaDTO saida = getManSolRecuperacaoPagConsultaServiceImpl().excluirSolRecPagtosVencidoNaoPago(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" +saida.getCodMensagem() + ") " + saida.getMensagem(), "conManSolRecuperacaoPagConsulta", "#{manSolRecuperacaoPagConsultaBean.consultar}", false);
		}catch (PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			
		}
		
		return "";
	}
	
	/**
	 * Voltar detalhe.
	 *
	 * @return the string
	 */
	public String voltarDetalhe(){
		setItemSelecionado(null);
		
		return "conManSolRecuperacaoPagConsulta";
	}
	
	/**
	 * Voltar excluir.
	 *
	 * @return the string
	 */
	public String voltarExcluir(){
		setItemSelecionado(null);
		
		return "conManSolRecuperacaoPagConsulta";
	}
	
	/**
	 * M�todo que retornar� uma descri��o para cada n�mero do tipo de favorecido.
	 * @param tipoFavorecido
	 * @return descricao do tipo de favorecido
	 */
	public String validaTipoFavorecidoSolicitacao(){
		
		String descTipoFavorerido="";
		
		if(getNumeroInscricaoFavorecido() == null || getNumeroInscricaoFavorecido().equals("0") || getNumeroInscricaoFavorecido().equals("") ){
			descTipoFavorerido = "";
		}else{
			if (getTipoFavorecido() == 0 ){
				descTipoFavorerido = MessageHelperUtils.getI18nMessage("label_isento_nao_informado");
			} else if (getTipoFavorecido() == 1) {
				descTipoFavorerido = MessageHelperUtils.getI18nMessage("label_cpf");
			} else if (getTipoFavorecido() == 2) {
				descTipoFavorerido = MessageHelperUtils.getI18nMessage("label_cnpj");
			} else if (getTipoFavorecido() == 3) {
				descTipoFavorerido = MessageHelperUtils.getI18nMessage("label_pis_pasep");
			} else {
				descTipoFavorerido = MessageHelperUtils.getI18nMessage("label_outros_maiusc");
			}
		}
		
		return descTipoFavorerido;
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: manSolRecuperacaoPagConsultaServiceImpl.
	 *
	 * @return manSolRecuperacaoPagConsultaServiceImpl
	 */
	public IManSolRecuperacaoPagConsultaService getManSolRecuperacaoPagConsultaServiceImpl() {
		return manSolRecuperacaoPagConsultaServiceImpl;
	}
	
	/**
	 * Set: manSolRecuperacaoPagConsultaServiceImpl.
	 *
	 * @param manSolRecuperacaoPagConsultaServiceImpl the man sol recuperacao pag consulta service impl
	 */
	public void setManSolRecuperacaoPagConsultaServiceImpl(
			IManSolRecuperacaoPagConsultaService manSolRecuperacaoPagConsultaServiceImpl) {
		this.manSolRecuperacaoPagConsultaServiceImpl = manSolRecuperacaoPagConsultaServiceImpl;
	}
	
	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}
	
	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}
	
	/**
	 * Get: foco.
	 *
	 * @return foco
	 */
	public String getFoco() {
		return foco;
	}
	
	/**
	 * Set: foco.
	 *
	 * @param foco the foco
	 */
	public void setFoco(String foco) {
		this.foco = foco;
	}

	/**
	 * Is chk servico modalidade.
	 *
	 * @return true, if is chk servico modalidade
	 */
	public boolean isChkServicoModalidade() {
		return chkServicoModalidade;
	}

	/**
	 * Set: chkServicoModalidade.
	 *
	 * @param chkServicoModalidade the chk servico modalidade
	 */
	public void setChkServicoModalidade(boolean chkServicoModalidade) {
		this.chkServicoModalidade = chkServicoModalidade;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}

	/**
	 * Get: modalidadeFiltro.
	 *
	 * @return modalidadeFiltro
	 */
	public Integer getModalidadeFiltro() {
		return modalidadeFiltro;
	}

	/**
	 * Set: modalidadeFiltro.
	 *
	 * @param modalidadeFiltro the modalidade filtro
	 */
	public void setModalidadeFiltro(Integer modalidadeFiltro) {
		this.modalidadeFiltro = modalidadeFiltro;
	}

	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Get: listaModalidadeFiltro.
	 *
	 * @return listaModalidadeFiltro
	 */
	public List<SelectItem> getListaModalidadeFiltro() {
		return listaModalidadeFiltro;
	}

	/**
	 * Set: listaModalidadeFiltro.
	 *
	 * @param listaModalidadeFiltro the lista modalidade filtro
	 */
	public void setListaModalidadeFiltro(List<SelectItem> listaModalidadeFiltro) {
		this.listaModalidadeFiltro = listaModalidadeFiltro;
	}

	/**
	 * Get: listaTipoServicoFiltro.
	 *
	 * @return listaTipoServicoFiltro
	 */
	public List<SelectItem> getListaTipoServicoFiltro() {
		return listaTipoServicoFiltro;
	}

	/**
	 * Set: listaTipoServicoFiltro.
	 *
	 * @param listaTipoServicoFiltro the lista tipo servico filtro
	 */
	public void setListaTipoServicoFiltro(List<SelectItem> listaTipoServicoFiltro) {
		this.listaTipoServicoFiltro = listaTipoServicoFiltro;
	}

	/**
	 * Get: dataFinalSolicitacaoFiltro.
	 *
	 * @return dataFinalSolicitacaoFiltro
	 */
	public Date getDataFinalSolicitacaoFiltro() {
		return dataFinalSolicitacaoFiltro;
	}

	/**
	 * Set: dataFinalSolicitacaoFiltro.
	 *
	 * @param dataFinalSolicitacaoFiltro the data final solicitacao filtro
	 */
	public void setDataFinalSolicitacaoFiltro(Date dataFinalSolicitacaoFiltro) {
		this.dataFinalSolicitacaoFiltro = dataFinalSolicitacaoFiltro;
	}

	/**
	 * Get: dataInicialSolicitacaoFiltro.
	 *
	 * @return dataInicialSolicitacaoFiltro
	 */
	public Date getDataInicialSolicitacaoFiltro() {
		return dataInicialSolicitacaoFiltro;
	}

	/**
	 * Set: dataInicialSolicitacaoFiltro.
	 *
	 * @param dataInicialSolicitacaoFiltro the data inicial solicitacao filtro
	 */
	public void setDataInicialSolicitacaoFiltro(Date dataInicialSolicitacaoFiltro) {
		this.dataInicialSolicitacaoFiltro = dataInicialSolicitacaoFiltro;
	}



	/**
	 * Get: cboMotivoSituacaoFiltro.
	 *
	 * @return cboMotivoSituacaoFiltro
	 */
	public Integer getCboMotivoSituacaoFiltro() {
		return cboMotivoSituacaoFiltro;
	}



	/**
	 * Set: cboMotivoSituacaoFiltro.
	 *
	 * @param cboMotivoSituacaoFiltro the cbo motivo situacao filtro
	 */
	public void setCboMotivoSituacaoFiltro(Integer cboMotivoSituacaoFiltro) {
		this.cboMotivoSituacaoFiltro = cboMotivoSituacaoFiltro;
	}



	/**
	 * Get: cboSituacaoPagamentoFiltro.
	 *
	 * @return cboSituacaoPagamentoFiltro
	 */
	public Integer getCboSituacaoPagamentoFiltro() {
		return cboSituacaoPagamentoFiltro;
	}



	/**
	 * Set: cboSituacaoPagamentoFiltro.
	 *
	 * @param cboSituacaoPagamentoFiltro the cbo situacao pagamento filtro
	 */
	public void setCboSituacaoPagamentoFiltro(Integer cboSituacaoPagamentoFiltro) {
		this.cboSituacaoPagamentoFiltro = cboSituacaoPagamentoFiltro;
	}



	/**
	 * Is chk situacao solicitacao.
	 *
	 * @return true, if is chk situacao solicitacao
	 */
	public boolean isChkSituacaoSolicitacao() {
		return chkSituacaoSolicitacao;
	}



	/**
	 * Set: chkSituacaoSolicitacao.
	 *
	 * @param chkSituacaoSolicitacao the chk situacao solicitacao
	 */
	public void setChkSituacaoSolicitacao(boolean chkSituacaoSolicitacao) {
		this.chkSituacaoSolicitacao = chkSituacaoSolicitacao;
	}



	/**
	 * Get: listaConsultarMotivoSituacaoPagamentoFiltro.
	 *
	 * @return listaConsultarMotivoSituacaoPagamentoFiltro
	 */
	public List<SelectItem> getListaConsultarMotivoSituacaoPagamentoFiltro() {
		return listaConsultarMotivoSituacaoPagamentoFiltro;
	}



	/**
	 * Set: listaConsultarMotivoSituacaoPagamentoFiltro.
	 *
	 * @param listaConsultarMotivoSituacaoPagamentoFiltro the lista consultar motivo situacao pagamento filtro
	 */
	public void setListaConsultarMotivoSituacaoPagamentoFiltro(
			List<SelectItem> listaConsultarMotivoSituacaoPagamentoFiltro) {
		this.listaConsultarMotivoSituacaoPagamentoFiltro = listaConsultarMotivoSituacaoPagamentoFiltro;
	}



	/**
	 * Get: listaConsultarSituacaoPagamentoFiltro.
	 *
	 * @return listaConsultarSituacaoPagamentoFiltro
	 */
	public List<SelectItem> getListaConsultarSituacaoPagamentoFiltro() {
		return listaConsultarSituacaoPagamentoFiltro;
	}



	/**
	 * Set: listaConsultarSituacaoPagamentoFiltro.
	 *
	 * @param listaConsultarSituacaoPagamentoFiltro the lista consultar situacao pagamento filtro
	 */
	public void setListaConsultarSituacaoPagamentoFiltro(
			List<SelectItem> listaConsultarSituacaoPagamentoFiltro) {
		this.listaConsultarSituacaoPagamentoFiltro = listaConsultarSituacaoPagamentoFiltro;
	}

	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: dsClubPessoa.
	 *
	 * @return dsClubPessoa
	 */
	public String getDsClubPessoa() {
		return dsClubPessoa;
	}

	/**
	 * Set: dsClubPessoa.
	 *
	 * @param dsClubPessoa the ds club pessoa
	 */
	public void setDsClubPessoa(String dsClubPessoa) {
		this.dsClubPessoa = dsClubPessoa;
	}

	/**
	 * Get: dsDataFinalPesquisa.
	 *
	 * @return dsDataFinalPesquisa
	 */
	public String getDsDataFinalPesquisa() {
		return dsDataFinalPesquisa;
	}

	/**
	 * Set: dsDataFinalPesquisa.
	 *
	 * @param dsDataFinalPesquisa the ds data final pesquisa
	 */
	public void setDsDataFinalPesquisa(String dsDataFinalPesquisa) {
		this.dsDataFinalPesquisa = dsDataFinalPesquisa;
	}

	/**
	 * Get: dsDataInicioPesquisa.
	 *
	 * @return dsDataInicioPesquisa
	 */
	public String getDsDataInicioPesquisa() {
		return dsDataInicioPesquisa;
	}

	/**
	 * Set: dsDataInicioPesquisa.
	 *
	 * @param dsDataInicioPesquisa the ds data inicio pesquisa
	 */
	public void setDsDataInicioPesquisa(String dsDataInicioPesquisa) {
		this.dsDataInicioPesquisa = dsDataInicioPesquisa;
	}

	/**
	 * Get: dsDataProgramadaRecuperacao.
	 *
	 * @return dsDataProgramadaRecuperacao
	 */
	public String getDsDataProgramadaRecuperacao() {
		return dsDataProgramadaRecuperacao;
	}

	/**
	 * Set: dsDataProgramadaRecuperacao.
	 *
	 * @param dsDataProgramadaRecuperacao the ds data programada recuperacao
	 */
	public void setDsDataProgramadaRecuperacao(String dsDataProgramadaRecuperacao) {
		this.dsDataProgramadaRecuperacao = dsDataProgramadaRecuperacao;
	}

	/**
	 * Get: dsDataSolicitacao.
	 *
	 * @return dsDataSolicitacao
	 */
	public String getDsDataSolicitacao() {
		return dsDataSolicitacao;
	}

	/**
	 * Set: dsDataSolicitacao.
	 *
	 * @param dsDataSolicitacao the ds data solicitacao
	 */
	public void setDsDataSolicitacao(String dsDataSolicitacao) {
		this.dsDataSolicitacao = dsDataSolicitacao;
	}

	/**
	 * Get: dsLinhaDigitavel.
	 *
	 * @return dsLinhaDigitavel
	 */
	public String getDsLinhaDigitavel() {
		return dsLinhaDigitavel;
	}

	/**
	 * Set: dsLinhaDigitavel.
	 *
	 * @param dsLinhaDigitavel the ds linha digitavel
	 */
	public void setDsLinhaDigitavel(String dsLinhaDigitavel) {
		this.dsLinhaDigitavel = dsLinhaDigitavel;
	}

	/**
	 * Get: dsModalidade.
	 *
	 * @return dsModalidade
	 */
	public String getDsModalidade() {
		return dsModalidade;
	}

	/**
	 * Set: dsModalidade.
	 *
	 * @param dsModalidade the ds modalidade
	 */
	public void setDsModalidade(String dsModalidade) {
		this.dsModalidade = dsModalidade;
	}

	/**
	 * Get: dsMotivoSituacaoPagamento.
	 *
	 * @return dsMotivoSituacaoPagamento
	 */
	public String getDsMotivoSituacaoPagamento() {
		return dsMotivoSituacaoPagamento;
	}

	/**
	 * Set: dsMotivoSituacaoPagamento.
	 *
	 * @param dsMotivoSituacaoPagamento the ds motivo situacao pagamento
	 */
	public void setDsMotivoSituacaoPagamento(String dsMotivoSituacaoPagamento) {
		this.dsMotivoSituacaoPagamento = dsMotivoSituacaoPagamento;
	}

	/**
	 * Get: dsMotivoSolicitacao.
	 *
	 * @return dsMotivoSolicitacao
	 */
	public String getDsMotivoSolicitacao() {
		return dsMotivoSolicitacao;
	}

	/**
	 * Set: dsMotivoSolicitacao.
	 *
	 * @param dsMotivoSolicitacao the ds motivo solicitacao
	 */
	public void setDsMotivoSolicitacao(String dsMotivoSolicitacao) {
		this.dsMotivoSolicitacao = dsMotivoSolicitacao;
	}

	/**
	 * Get: dsNomePessoa.
	 *
	 * @return dsNomePessoa
	 */
	public String getDsNomePessoa() {
		return dsNomePessoa;
	}

	/**
	 * Set: dsNomePessoa.
	 *
	 * @param dsNomePessoa the ds nome pessoa
	 */
	public void setDsNomePessoa(String dsNomePessoa) {
		this.dsNomePessoa = dsNomePessoa;
	}

	/**
	 * Get: dsNumero.
	 *
	 * @return dsNumero
	 */
	public String getDsNumero() {
		return dsNumero;
	}

	/**
	 * Set: dsNumero.
	 *
	 * @param dsNumero the ds numero
	 */
	public void setDsNumero(String dsNumero) {
		this.dsNumero = dsNumero;
	}

	/**
	 * Get: dsNumeroPagtoFinal.
	 *
	 * @return dsNumeroPagtoFinal
	 */
	public String getDsNumeroPagtoFinal() {
		return dsNumeroPagtoFinal;
	}

	/**
	 * Set: dsNumeroPagtoFinal.
	 *
	 * @param dsNumeroPagtoFinal the ds numero pagto final
	 */
	public void setDsNumeroPagtoFinal(String dsNumeroPagtoFinal) {
		this.dsNumeroPagtoFinal = dsNumeroPagtoFinal;
	}

	/**
	 * Get: dsNumeroPagtoInicial.
	 *
	 * @return dsNumeroPagtoInicial
	 */
	public String getDsNumeroPagtoInicial() {
		return dsNumeroPagtoInicial;
	}

	/**
	 * Set: dsNumeroPagtoInicial.
	 *
	 * @param dsNumeroPagtoInicial the ds numero pagto inicial
	 */
	public void setDsNumeroPagtoInicial(String dsNumeroPagtoInicial) {
		this.dsNumeroPagtoInicial = dsNumeroPagtoInicial;
	}

	/**
	 * Get: dsRemessa.
	 *
	 * @return dsRemessa
	 */
	public String getDsRemessa() {
		return dsRemessa;
	}

	/**
	 * Set: dsRemessa.
	 *
	 * @param dsRemessa the ds remessa
	 */
	public void setDsRemessa(String dsRemessa) {
		this.dsRemessa = dsRemessa;
	}

	/**
	 * Get: dsSituacaoPagamento.
	 *
	 * @return dsSituacaoPagamento
	 */
	public String getDsSituacaoPagamento() {
		return dsSituacaoPagamento;
	}

	/**
	 * Set: dsSituacaoPagamento.
	 *
	 * @param dsSituacaoPagamento the ds situacao pagamento
	 */
	public void setDsSituacaoPagamento(String dsSituacaoPagamento) {
		this.dsSituacaoPagamento = dsSituacaoPagamento;
	}

	/**
	 * Get: dsSituacaoSolicitacao.
	 *
	 * @return dsSituacaoSolicitacao
	 */
	public String getDsSituacaoSolicitacao() {
		return dsSituacaoSolicitacao;
	}

	/**
	 * Set: dsSituacaoSolicitacao.
	 *
	 * @param dsSituacaoSolicitacao the ds situacao solicitacao
	 */
	public void setDsSituacaoSolicitacao(String dsSituacaoSolicitacao) {
		this.dsSituacaoSolicitacao = dsSituacaoSolicitacao;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: contaDebito.
	 *
	 * @return contaDebito
	 */
	public String getContaDebito() {
		return contaDebito;
	}

	/**
	 * Set: contaDebito.
	 *
	 * @param contaDebito the conta debito
	 */
	public void setContaDebito(String contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsAgenciaFavorecido.
	 *
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}

	/**
	 * Set: dsAgenciaFavorecido.
	 *
	 * @param dsAgenciaFavorecido the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}

	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: dsBancoFavorecido.
	 *
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}

	/**
	 * Set: dsBancoFavorecido.
	 *
	 * @param dsBancoFavorecido the ds banco favorecido
	 */
	public void setDsBancoFavorecido(String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}

	/**
	 * Get: dsContaFavorecido.
	 *
	 * @return dsContaFavorecido
	 */
	public String getDsContaFavorecido() {
		return dsContaFavorecido;
	}

	/**
	 * Set: dsContaFavorecido.
	 *
	 * @param dsContaFavorecido the ds conta favorecido
	 */
	public void setDsContaFavorecido(String dsContaFavorecido) {
		this.dsContaFavorecido = dsContaFavorecido;
	}

	public String getDsDigContaFavorecido() {
		return dsDigContaFavorecido;
	}

	public void setDsDigContaFavorecido(String dsDigContaFavorecido) {
		this.dsDigContaFavorecido = dsDigContaFavorecido;
	}

	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}

	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}

	/**
	 * Get: numeroInscricaoFavorecido.
	 *
	 * @return numeroInscricaoFavorecido
	 */
	public String getNumeroInscricaoFavorecido() {
		return numeroInscricaoFavorecido;
	}

	/**
	 * Set: numeroInscricaoFavorecido.
	 *
	 * @param numeroInscricaoFavorecido the numero inscricao favorecido
	 */
	public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
		this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
	}

	/**
	 * Get: tipoContaDebito.
	 *
	 * @return tipoContaDebito
	 */
	public String getTipoContaDebito() {
		return tipoContaDebito;
	}

	/**
	 * Set: tipoContaDebito.
	 *
	 * @param tipoContaDebito the tipo conta debito
	 */
	public void setTipoContaDebito(String tipoContaDebito) {
		this.tipoContaDebito = tipoContaDebito;
	}

	/**
	 * Get: tipoContaFavorecido.
	 *
	 * @return tipoContaFavorecido
	 */
	public String getTipoContaFavorecido() {
		return tipoContaFavorecido;
	}

	/**
	 * Set: tipoContaFavorecido.
	 *
	 * @param tipoContaFavorecido the tipo conta favorecido
	 */
	public void setTipoContaFavorecido(String tipoContaFavorecido) {
		this.tipoContaFavorecido = tipoContaFavorecido;
	}

	/**
	 * Get: tipoFavorecido.
	 *
	 * @return tipoFavorecido
	 */
	public Integer getTipoFavorecido() {
		return tipoFavorecido;
	}

	/**
	 * Set: tipoFavorecido.
	 *
	 * @param tipoFavorecido the tipo favorecido
	 */
	public void setTipoFavorecido(Integer tipoFavorecido) {
		this.tipoFavorecido = tipoFavorecido;
	}

	/**
	 * Get: listaConsulta.
	 *
	 * @return listaConsulta
	 */
	public List<ConsultarSolRecuperacaoPagtosBackupSaidaDTO> getListaConsulta() {
		return listaConsulta;
	}

	/**
	 * Set: listaConsulta.
	 *
	 * @param listaConsulta the lista consulta
	 */
	public void setListaConsulta(
			List<ConsultarSolRecuperacaoPagtosBackupSaidaDTO> listaConsulta) {
		this.listaConsulta = listaConsulta;
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: itemSelecionado.
	 *
	 * @return itemSelecionado
	 */
	public Integer getItemSelecionado() {
		return itemSelecionado;
	}

	/**
	 * Set: itemSelecionado.
	 *
	 * @param itemSelecionado the item selecionado
	 */
	public void setItemSelecionado(Integer itemSelecionado) {
		this.itemSelecionado = itemSelecionado;
	}

	/**
	 * Get: dsDataNovoVencimento.
	 *
	 * @return dsDataNovoVencimento
	 */
	public String getDsDataNovoVencimento() {
		return dsDataNovoVencimento;
	}

	/**
	 * Set: dsDataNovoVencimento.
	 *
	 * @param dsDataNovoVencimento the ds data novo vencimento
	 */
	public void setDsDataNovoVencimento(String dsDataNovoVencimento) {
		this.dsDataNovoVencimento = dsDataNovoVencimento;
	}

	/**
	 * Get: dsValorPagtoFinal.
	 *
	 * @return dsValorPagtoFinal
	 */
	public BigDecimal getDsValorPagtoFinal() {
		return dsValorPagtoFinal;
	}

	/**
	 * Set: dsValorPagtoFinal.
	 *
	 * @param dsValorPagtoFinal the ds valor pagto final
	 */
	public void setDsValorPagtoFinal(BigDecimal dsValorPagtoFinal) {
		this.dsValorPagtoFinal = dsValorPagtoFinal;
	}

	/**
	 * Get: dsValorPagtoInicial.
	 *
	 * @return dsValorPagtoInicial
	 */
	public BigDecimal getDsValorPagtoInicial() {
		return dsValorPagtoInicial;
	}

	/**
	 * Set: dsValorPagtoInicial.
	 *
	 * @param dsValorPagtoInicial the ds valor pagto inicial
	 */
	public void setDsValorPagtoInicial(BigDecimal dsValorPagtoInicial) {
		this.dsValorPagtoInicial = dsValorPagtoInicial;
	}

	/**
	 * Get: descTipoFavorecido.
	 *
	 * @return descTipoFavorecido
	 */
	public String getDescTipoFavorecido() {
		return descTipoFavorecido;
	}

	/**
	 * Set: descTipoFavorecido.
	 *
	 * @param descTipoFavorecido the desc tipo favorecido
	 */
	public void setDescTipoFavorecido(String descTipoFavorecido) {
		this.descTipoFavorecido = descTipoFavorecido;
	}

	public Long getContaSalario() {
		return contaSalario;
	}

	public void setContaSalario(Long contaSalario) {
		this.contaSalario = contaSalario;
	}

	public Integer getBancoSalario() {
		return bancoSalario;
	}

	public void setBancoSalario(Integer bancoSalario) {
		this.bancoSalario = bancoSalario;
	}

	public Integer getAgenciaSalario() {
		return agenciaSalario;
	}

	public void setAgenciaSalario(Integer agenciaSalario) {
		this.agenciaSalario = agenciaSalario;
	}

	public String getDigitoSalario() {
		return digitoSalario;
	}

	public void setDigitoSalario(String digitoSalario) {
		this.digitoSalario = digitoSalario;
	}

	public Integer getDestinoPagamento() {
		return destinoPagamento;
	}

	public void setDestinoPagamento(Integer destinoPagamento) {
		this.destinoPagamento = destinoPagamento;
	}

	public String getDsDestinoPagamento() {
		return dsDestinoPagamento;
	}

	public void setDsDestinoPagamento(String dsDestinoPagamento) {
		this.dsDestinoPagamento = dsDestinoPagamento;
	}

	public Integer getBancoPgtoDestino() {
		return bancoPgtoDestino;
	}

	public void setBancoPgtoDestino(Integer bancoPgtoDestino) {
		this.bancoPgtoDestino = bancoPgtoDestino;
	}

	public Long getContaPgtoDestino() {
		return contaPgtoDestino;
	}

	public void setContaPgtoDestino(Long contaPgtoDestino) {
		this.contaPgtoDestino = contaPgtoDestino;
	}

	public String getIspbPgtoDestino() {
		return ispbPgtoDestino;
	}

	public void setIspbPgtoDestino(String ispbPgtoDestino) {
		this.ispbPgtoDestino = ispbPgtoDestino;
	}

	public Long getLoteInterno() {
		return loteInterno;
	}

	public void setLoteInterno(Long loteInterno) {
		this.loteInterno = loteInterno;
	}

	public Integer getNumeroSolicitacao() {
		return numeroSolicitacao;
	}

	public void setNumeroSolicitacao(Integer numeroSolicitacao) {
		this.numeroSolicitacao = numeroSolicitacao;
	}
}
