/*
 * Nome: br.com.bradesco.web.pgit.view.bean.teste
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.teste;

import javax.faces.event.ActionEvent;

import br.com.bradesco.web.pgit.service.business.teste.IDadosFinanceirosTesteService;
import br.com.bradesco.web.pgit.service.business.teste.bean.DadosFinanceirosTesteDTO;

/**
 * Nome: DadosFinanceirosTesteBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class DadosFinanceirosTesteBean {

	/** Atributo numBaseCnpj. */
	private Long numBaseCnpj;
	
	/** Atributo numBaseCpf. */
	private Long numBaseCpf;
	
	/** Atributo numFilialCnpj. */
	private Integer numFilialCnpj;
	
	/** Atributo numDigitoCnpj. */
	private Integer numDigitoCnpj;
	
	/** Atributo numDigitoCpf. */
	private Integer numDigitoCpf;
	
	/** Atributo tipoPesquisaSelecionada. */
	private Integer tipoPesquisaSelecionada;
	
	/** Atributo dtAnoMesFormatada. */
	private Integer dtAnoMesFormatada;
	
	/** Atributo dtAno. */
	private String dtAno;
	
	/** Atributo dtMes. */
	private String dtMes;
	
	/** Atributo dadosFinanceirosTesteSaida. */
	private DadosFinanceirosTesteDTO dadosFinanceirosTesteSaida;
	
	/** Atributo habilitaBtnConsulta. */
	private boolean habilitaBtnConsulta;

	/** Atributo dadosFinanceirosTesteService. */
	private IDadosFinanceirosTesteService dadosFinanceirosTesteService;

	/**
	 * Get: dadosFinanceirosTesteService.
	 *
	 * @return dadosFinanceirosTesteService
	 */
	public IDadosFinanceirosTesteService getDadosFinanceirosTesteService() {
		return dadosFinanceirosTesteService;
	}

	/**
	 * Set: dadosFinanceirosTesteService.
	 *
	 * @param dadosFinanceirosTesteService the dados financeiros teste service
	 */
	public void setDadosFinanceirosTesteService(IDadosFinanceirosTesteService dadosFinanceirosTesteService) {
		this.dadosFinanceirosTesteService = dadosFinanceirosTesteService;
	}

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		limparCampos();
	}

	/**
	 * Limpar campos.
	 */
	public void limparCampos() {
		setNumBaseCnpj(null);
		setNumFilialCnpj(null);
		setNumDigitoCnpj(null);
		setNumBaseCpf(null);
		setNumDigitoCpf(null);
		setHabilitaBtnConsulta(true);
	}

	/**
	 * Consultar.
	 */
	public void consultar() {
		if (!dtAno.equals("") || !dtMes.equals("")) {
			setDtAnoMesFormatada(Integer.parseInt(dtAno+dtMes));
		}

		dadosFinanceirosTesteSaida = dadosFinanceirosTesteService.consultar(numBaseCnpj, numFilialCnpj, numDigitoCnpj, numBaseCpf, numDigitoCpf, dtAnoMesFormatada, tipoPesquisaSelecionada);
	}

	/**
	 * Checa campos preenchidos.
	 */
	public void checaCamposPreenchidos() {
		if (tipoPesquisaSelecionada == 1) {
			if (numBaseCnpj == null || numFilialCnpj == null || numDigitoCnpj == null) {
				setHabilitaBtnConsulta(true);
			} else {
				setHabilitaBtnConsulta(false);
			}
		} else if (tipoPesquisaSelecionada == 2) {
			if (numBaseCpf == null || numDigitoCpf == null) {
				setHabilitaBtnConsulta(true);
			} else {
				setHabilitaBtnConsulta(false);
			}
		}
	}

	/**
	 * Get: tipoPesquisaSelecionada.
	 *
	 * @return tipoPesquisaSelecionada
	 */
	public Integer getTipoPesquisaSelecionada() {
		return tipoPesquisaSelecionada;
	}

	/**
	 * Set: tipoPesquisaSelecionada.
	 *
	 * @param tipoPesquisaSelecionada the tipo pesquisa selecionada
	 */
	public void setTipoPesquisaSelecionada(Integer tipoPesquisaSelecionada) {
		this.tipoPesquisaSelecionada = tipoPesquisaSelecionada;
	}

	/**
	 * Get: dtAno.
	 *
	 * @return dtAno
	 */
	public String getDtAno() {
		return dtAno;
	}

	/**
	 * Set: dtAno.
	 *
	 * @param dtAno the dt ano
	 */
	public void setDtAno(String dtAno) {
		this.dtAno = dtAno;
	}

	/**
	 * Get: dtMes.
	 *
	 * @return dtMes
	 */
	public String getDtMes() {
		return dtMes;
	}

	/**
	 * Set: dtMes.
	 *
	 * @param dtMes the dt mes
	 */
	public void setDtMes(String dtMes) {
		this.dtMes = dtMes;
	}

	/**
	 * Get: dtAnoMesFormatada.
	 *
	 * @return dtAnoMesFormatada
	 */
	public Integer getDtAnoMesFormatada() {
		return dtAnoMesFormatada;
	}

	/**
	 * Set: dtAnoMesFormatada.
	 *
	 * @param dtAnoMesFormatada the dt ano mes formatada
	 */
	public void setDtAnoMesFormatada(Integer dtAnoMesFormatada) {
		this.dtAnoMesFormatada = dtAnoMesFormatada;
	}

	/**
	 * Get: dadosFinanceirosTesteSaida.
	 *
	 * @return dadosFinanceirosTesteSaida
	 */
	public DadosFinanceirosTesteDTO getDadosFinanceirosTesteSaida() {
		return dadosFinanceirosTesteSaida;
	}

	/**
	 * Set: dadosFinanceirosTesteSaida.
	 *
	 * @param dadosFinanceirosTesteSaida the dados financeiros teste saida
	 */
	public void setDadosFinanceirosTesteSaida(DadosFinanceirosTesteDTO dadosFinanceirosTesteSaida) {
		this.dadosFinanceirosTesteSaida = dadosFinanceirosTesteSaida;
	}

	/**
	 * Get: numBaseCnpj.
	 *
	 * @return numBaseCnpj
	 */
	public Long getNumBaseCnpj() {
		return numBaseCnpj;
	}

	/**
	 * Set: numBaseCnpj.
	 *
	 * @param numBaseCnpj the num base cnpj
	 */
	public void setNumBaseCnpj(Long numBaseCnpj) {
		this.numBaseCnpj = numBaseCnpj;
	}

	/**
	 * Get: numBaseCpf.
	 *
	 * @return numBaseCpf
	 */
	public Long getNumBaseCpf() {
		return numBaseCpf;
	}

	/**
	 * Set: numBaseCpf.
	 *
	 * @param numBaseCpf the num base cpf
	 */
	public void setNumBaseCpf(Long numBaseCpf) {
		this.numBaseCpf = numBaseCpf;
	}

	/**
	 * Get: numDigitoCnpj.
	 *
	 * @return numDigitoCnpj
	 */
	public Integer getNumDigitoCnpj() {
		return numDigitoCnpj;
	}

	/**
	 * Set: numDigitoCnpj.
	 *
	 * @param numDigitoCnpj the num digito cnpj
	 */
	public void setNumDigitoCnpj(Integer numDigitoCnpj) {
		this.numDigitoCnpj = numDigitoCnpj;
	}

	/**
	 * Get: numDigitoCpf.
	 *
	 * @return numDigitoCpf
	 */
	public Integer getNumDigitoCpf() {
		return numDigitoCpf;
	}

	/**
	 * Set: numDigitoCpf.
	 *
	 * @param numDigitoCpf the num digito cpf
	 */
	public void setNumDigitoCpf(Integer numDigitoCpf) {
		this.numDigitoCpf = numDigitoCpf;
	}

	/**
	 * Get: numFilialCnpj.
	 *
	 * @return numFilialCnpj
	 */
	public Integer getNumFilialCnpj() {
		return numFilialCnpj;
	}

	/**
	 * Set: numFilialCnpj.
	 *
	 * @param numFilialCnpj the num filial cnpj
	 */
	public void setNumFilialCnpj(Integer numFilialCnpj) {
		this.numFilialCnpj = numFilialCnpj;
	}

	/**
	 * Is habilita btn consulta.
	 *
	 * @return true, if is habilita btn consulta
	 */
	public boolean isHabilitaBtnConsulta() {
		return habilitaBtnConsulta;
	}

	/**
	 * Set: habilitaBtnConsulta.
	 *
	 * @param habilitaBtnConsulta the habilita btn consulta
	 */
	public void setHabilitaBtnConsulta(boolean habilitaBtnConsulta) {
		this.habilitaBtnConsulta = habilitaBtnConsulta;
	}
}