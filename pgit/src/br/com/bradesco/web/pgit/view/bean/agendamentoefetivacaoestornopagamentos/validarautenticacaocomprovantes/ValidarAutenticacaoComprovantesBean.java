/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.validarautenticacaocomprovantes
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.validarautenticacaocomprovantes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicosCatalogoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicosCatalogoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.IValidarAutenticacaoComprovantesService;
import br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.bean.ConsultarAutenticacaoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.validarautenticacaocomprovantes.bean.ConsultarAutenticacaoSaidaDTO;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: ValidarAutenticacaoComprovantesBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ValidarAutenticacaoComprovantesBean { 
	
	
	/** Atributo banco. */
	private String banco;
	
	/** Atributo agencia. */
	private String agencia;
	
	/** Atributo conta. */
	private String conta;
	
	/** Atributo digitoConta. */
	private String digitoConta;
	
	/** Atributo valor. */
	private BigDecimal valor;
	
	/** Atributo dataPagamento. */
	private Date dataPagamento;
	
	/** Atributo cdServico. */
	private Integer cdServico;
	
	/** Atributo listaServico. */
	private List<SelectItem> listaServico;	
	
	/** Atributo cdModalidade. */
	private Integer cdModalidade;
	
	/** Atributo listaModalidade. */
	private List<SelectItem> listaModalidade= new ArrayList<SelectItem>();
	
	/** Atributo cdAutenticacaoEletronica. */
	private String cdAutenticacaoEletronica;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo validarAutenticacaoComprovantesService. */
	private IValidarAutenticacaoComprovantesService validarAutenticacaoComprovantesService;
	
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	private ConsultarServicosCatalogoSaidaDTO saidaConsultarServicosCatalogo = null;

    private String codBarras1 = null;
    private String codBarras2 = null;
    private String codBarras3 = null;
    private String codBarras4 = null;
    private String numeroPagamento = null;
	
	/**
	 * Iniciar tela.
	 *
	 * @param e the e
	 */
	public void iniciarTela(ActionEvent e){
		carregaTipoServico();
		
		setBanco("");
		setAgencia("");
		setConta("");
		setDigitoConta("");
		setValor(null);
		//setDataPagamento(new Date());
		setCdServico(0);
		setCdModalidade(0);
		setCdAutenticacaoEletronica(null);
		setDisableArgumentosConsulta(false);
		
		
	}

	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * Set: agencia.
	 *
	 * @param agencia the agencia
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * Set: banco.
	 *
	 * @param banco the banco
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}

	/**
	 * Get: cdModalidade.
	 *
	 * @return cdModalidade
	 */
	public Integer getCdModalidade() {
		return cdModalidade;
	}

	/**
	 * Set: cdModalidade.
	 *
	 * @param cdModalidade the cd modalidade
	 */
	public void setCdModalidade(Integer cdModalidade) {
		this.cdModalidade = cdModalidade;
	}

	/**
	 * Get: cdServico.
	 *
	 * @return cdServico
	 */
	public Integer getCdServico() {
		return cdServico;
	}

	/**
	 * Set: cdServico.
	 *
	 * @param cdServico the cd servico
	 */
	public void setCdServico(Integer cdServico) {
		this.cdServico = cdServico;
	}

	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * Get: dataPagamento.
	 *
	 * @return dataPagamento
	 */
	public Date getDataPagamento() {
		return dataPagamento;
	}

	/**
	 * Set: dataPagamento.
	 *
	 * @param dataPagamento the data pagamento
	 */
	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	/**
	 * Get: digitoConta.
	 *
	 * @return digitoConta
	 */
	public String getDigitoConta() {
		return digitoConta;
	}

	/**
	 * Set: digitoConta.
	 *
	 * @param digitoConta the digito conta
	 */
	public void setDigitoConta(String digitoConta) {
		this.digitoConta = digitoConta;
	}

	/**
	 * Get: valor.
	 *
	 * @return valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * Set: valor.
	 *
	 * @param valor the valor
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	/**
	 * Get: cdAutenticacaoEletronica.
	 *
	 * @return cdAutenticacaoEletronica
	 */
	public String getCdAutenticacaoEletronica() {
		return cdAutenticacaoEletronica;
	}

	/**
	 * Set: cdAutenticacaoEletronica.
	 *
	 * @param cdAutenticacaoEletronica the cd autenticacao eletronica
	 */
	public void setCdAutenticacaoEletronica(String cdAutenticacaoEletronica) {
		this.cdAutenticacaoEletronica = cdAutenticacaoEletronica;
	}

	/**
	 * Carrega lista modalidade.
	 */
	public void carregaListaModalidade() {
		try{
			this.listaModalidade= new ArrayList<SelectItem>();
			setCdModalidade(null);
			
			if (getCdServico() != null && getCdServico().intValue() != 0){
				this.listaModalidade= new ArrayList<SelectItem>();
				List<ConsultarModalidadePagtoSaidaDTO> list = new ArrayList<ConsultarModalidadePagtoSaidaDTO>();
				
				list = comboService.consultarModalidadePagto(getCdServico());
				listaModalidade.clear();
				for(ConsultarModalidadePagtoSaidaDTO combo : list){				
					listaModalidade.add(new SelectItem(combo.getCdProdutoOperacaoRelacionado(), combo.getDsProdutoOperacaoRelacionado()));
				}
			}
		}catch(PdcAdapterFunctionalException p){
			this.listaModalidade= new ArrayList<SelectItem>();
			setCdModalidade(null);
		}
	}
	
	/**
	 * Carrega tipo servico.
	 */
	public void carregaTipoServico(){
		try{
			this.listaServico= new ArrayList<SelectItem>();
			
			List<ConsultarServicoPagtoSaidaDTO> list = new ArrayList<ConsultarServicoPagtoSaidaDTO>();
			list = comboService.consultarServicoPagto();
			
			for(ConsultarServicoPagtoSaidaDTO combo : list){				
				listaServico.add(new SelectItem(combo.getCdServico(), combo.getDsServico()));
			}
		}catch(PdcAdapterFunctionalException p){
			this.listaServico= new ArrayList<SelectItem>();
		}
	}

	/**
	 * Set: listaModalidade.
	 *
	 * @param listaModalidade the lista modalidade
	 */
	public void setListaModalidade(List<SelectItem> listaModalidade) {
		this.listaModalidade = listaModalidade;
	}

	/**
	 * Set: listaServico.
	 *
	 * @param listaServico the lista servico
	 */
	public void setListaServico(List<SelectItem> listaServico) {
		this.listaServico = listaServico;
	}

	/**
	 * Get: listaServico.
	 *
	 * @return listaServico
	 */
	public List<SelectItem> getListaServico() {
		return listaServico;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: listaModalidade.
	 *
	 * @return listaModalidade
	 */
	public List<SelectItem> getListaModalidade() {
		return listaModalidade;
	}
	
	
	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar(){		
		setBanco("");
		setAgencia("");
		setConta("");
		setValor(null);
		setDataPagamento(null);
		setCdServico(0);
		carregaListaModalidade();
		setCdAutenticacaoEletronica(null);
		setDisableArgumentosConsulta(false);
		limparCamposModalidade();
		return "";
	}
	
	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar(){
		try {
			setDisableArgumentosConsulta(true);
			ConsultarAutenticacaoEntradaDTO entradaDTO = new ConsultarAutenticacaoEntradaDTO();
			entradaDTO.setCdBanco(getBanco() != null ? Integer.parseInt(getBanco()) : 0);
			entradaDTO.setCdAgencia(getConta() != null ? Integer.parseInt(getAgencia()) : 0);
			entradaDTO.setCdConta(getConta() != null ? Long.parseLong(getConta()) : 0);
			entradaDTO.setCdDigitoConta(getDigitoConta());
			entradaDTO.setValorPagamento(getValor());
			entradaDTO.setDtPagamento(getDataPagamento() != null ? FormatarData.formataDiaMesAno(getDataPagamento()) : "");
			entradaDTO.setCdServico(getCdServico());
			entradaDTO.setCdModalidade(getCdModalidade());
			if(getSaidaConsultarServicosCatalogo().getCdProdtPgit() == 25){
			    entradaDTO.setCdFlCodigoBarras(getCodBarras1() + getCodBarras2() + getCodBarras3() + getCodBarras4());
			    entradaDTO.setCdFlagModalidade(1);
			}else{
			    entradaDTO.setNrPagamento(getNumeroPagamento());
			    entradaDTO.setCdFlagModalidade(2);
			}
			
			ConsultarAutenticacaoSaidaDTO saidaDTO = validarAutenticacaoComprovantesService.consultarAutenticacao(entradaDTO);
			
			setCdAutenticacaoEletronica(saidaDTO.getCdAutenticacao());
			
			BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "validarAutenticacaoComprovantes", BradescoViewExceptionActionType.ACTION, false);
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setCdAutenticacaoEletronica(null);
			setDisableArgumentosConsulta(false);
			return null;
		}
		
		return "";
	}
	
	public void consultarServicosCatalogo(){
	    ConsultarServicosCatalogoEntradaDTO entrada = new ConsultarServicosCatalogoEntradaDTO();
	    
	    limparCamposModalidade();
	    
	    entrada.setCdProducaoServicoCdps(getCdServico());
	    entrada.setCdProdtRelacionadoCdps(getCdModalidade());
	    entrada.setCdProdutoPgit(0l);
	    entrada.setCdTipoAcesso(3);
	    
	    try{
	        saidaConsultarServicosCatalogo = comboService.consultarServicosCatalogo(entrada);
	    }catch (PdcAdapterFunctionalException p) {
            BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
        }
	}

    /**
     * Nome: limparCamposModalidade
     * 
     */
    private void limparCamposModalidade() {
        setCodBarras1("");
	    setCodBarras2("");
	    setCodBarras3("");
	    setCodBarras4("");
	    setNumeroPagamento("");
    }

	/**
	 * Get: validarAutenticacaoComprovantesService.
	 *
	 * @return validarAutenticacaoComprovantesService
	 */
	public IValidarAutenticacaoComprovantesService getValidarAutenticacaoComprovantesService() {
		return validarAutenticacaoComprovantesService;
	}

	/**
	 * Set: validarAutenticacaoComprovantesService.
	 *
	 * @param validarAutenticacaoComprovantesService the validar autenticacao comprovantes service
	 */
	public void setValidarAutenticacaoComprovantesService(
			IValidarAutenticacaoComprovantesService validarAutenticacaoComprovantesService) {
		this.validarAutenticacaoComprovantesService = validarAutenticacaoComprovantesService;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}
	
	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

    /**
     * Nome: getSaidaConsultarServicosCatalogo
     *
     * @return saidaConsultarServicosCatalogo
     */
    public ConsultarServicosCatalogoSaidaDTO getSaidaConsultarServicosCatalogo() {
        return saidaConsultarServicosCatalogo;
    }

    /**
     * Nome: setSaidaConsultarServicosCatalogo
     *
     * @param saidaConsultarServicosCatalogo
     */
    public void setSaidaConsultarServicosCatalogo(ConsultarServicosCatalogoSaidaDTO saidaConsultarServicosCatalogo) {
        this.saidaConsultarServicosCatalogo = saidaConsultarServicosCatalogo;
    }

    /**
     * Nome: getCodBarras1
     *
     * @return codBarras1
     */
    public String getCodBarras1() {
        return codBarras1;
    }

    /**
     * Nome: setCodBarras1
     *
     * @param codBarras1
     */
    public void setCodBarras1(String codBarras1) {
        this.codBarras1 = codBarras1;
    }

    /**
     * Nome: getCodBarras2
     *
     * @return codBarras2
     */
    public String getCodBarras2() {
        return codBarras2;
    }

    /**
     * Nome: setCodBarras2
     *
     * @param codBarras2
     */
    public void setCodBarras2(String codBarras2) {
        this.codBarras2 = codBarras2;
    }

    /**
     * Nome: getCodBarras3
     *
     * @return codBarras3
     */
    public String getCodBarras3() {
        return codBarras3;
    }

    /**
     * Nome: setCodBarras3
     *
     * @param codBarras3
     */
    public void setCodBarras3(String codBarras3) {
        this.codBarras3 = codBarras3;
    }

    /**
     * Nome: getCodBarras4
     *
     * @return codBarras4
     */
    public String getCodBarras4() {
        return codBarras4;
    }

    /**
     * Nome: setCodBarras4
     *
     * @param codBarras4
     */
    public void setCodBarras4(String codBarras4) {
        this.codBarras4 = codBarras4;
    }

    /**
     * Nome: getNumeroPagamento
     *
     * @return numeroPagamento
     */
    public String getNumeroPagamento() {
        return numeroPagamento;
    }

    /**
     * Nome: setNumeroPagamento
     *
     * @param numeroPagamento
     */
    public void setNumeroPagamento(String numeroPagamento) {
        this.numeroPagamento = numeroPagamento;
    }
	
	

}
