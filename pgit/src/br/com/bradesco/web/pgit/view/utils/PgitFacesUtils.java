/*
 * Nome: br.com.bradesco.web.pgit.view.utils
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.utils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.pdc.util.PDCServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;

/**
 * Nome: PgitFacesUtils
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public final class PgitFacesUtils {

	/**
	 * Pgit faces utils.
	 */
	private PgitFacesUtils() {
		super();
	}

	/**
	 * Set session attribute.
	 *
	 * @param attributeName the attribute name
	 * @param attribute the attribute
	 */
	@SuppressWarnings("unchecked")
	public static void setSessionAttribute(String attributeName, Object attribute) {
		FacesUtils.getContext().getExternalContext().getSessionMap().put(attributeName, attribute);
	}

	/**
	 * Add info modal message.
	 *
	 * @param codMensagem the cod mensagem
	 * @param mensagem the mensagem
	 * @param action the action
	 */
	public static void addInfoModalMessage(String codMensagem, String mensagem, String action) {
		StringBuilder msg = new StringBuilder();
		if (codMensagem != null) {
			msg.append("(");
			msg.append(codMensagem);
			msg.append(") ");
		}
		if (mensagem != null) {
			msg.append(mensagem);
		}

		if (action == null) {
			BradescoFacesUtils.addInfoModalMessage(msg.toString(), false);
		} else {
			BradescoFacesUtils.addInfoModalMessage(msg.toString(), action, BradescoViewExceptionActionType.ACTION, false);
		}
	}

	/**
	 * Add info modal message.
	 *
	 * @param codMensagem the cod mensagem
	 * @param mensagem the mensagem
	 */
	public static void addInfoModalMessage(String codMensagem, String mensagem) {
		addInfoModalMessage(codMensagem, mensagem, null);
	}

	/**
	 * Generate report response.
	 *
	 * @param reportName the report name
	 */
	public static void generateReportResponse(String reportName) {
		FacesUtils.getServletResponse().setContentType("application/pdf");
    	FacesUtils.getServletResponse().setHeader("Content-Disposition", "attachment;filename=" + reportName + ".pdf");
        FacesUtils.getContext().responseComplete();
	}
	
	/**
     * Reset pdc pagination.
     */
	public static void resetPdcPagination() {
	    PDCDataBean pdcDataBean = PDCServiceFactory.getPdcSessionManager().getPDCDataBean(null, false);
	    if (pdcDataBean != null) {
	        pdcDataBean.resetPdcPaginationBean();
	    }
	}
}
