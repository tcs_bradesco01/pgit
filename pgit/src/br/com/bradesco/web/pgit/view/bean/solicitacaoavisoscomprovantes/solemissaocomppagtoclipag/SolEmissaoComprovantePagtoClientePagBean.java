/*
 * Nome: br.com.bradesco.web.pgit.view.bean.solicitacaoavisoscomprovantes.solemissaocomppagtoclipag
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.solicitacaoavisoscomprovantes.solemissaocomppagtoclipag;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEmailSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEnderecoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOperadoraEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOperadoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.DetalharOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.DetalharSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.DetalharSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ExcluirSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ExcluirSolEmiComprovanteCliePagadorOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ExcluirSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.IncluirOcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.IncluirSolEmiComprovanteCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.IncluirSolEmiComprovanteCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ListarIncluirSolEmiComprovanteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ListarIncluirSolEmiComprovanteSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.bean.solicitacaoavisoscomprovantes.filtroenderecoemail.FiltroEnderecoEmailBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: SolEmissaoComprovantePagtoClientePagBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolEmissaoComprovantePagtoClientePagBean{
	/** Atributos **/
	private ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl;
	
	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo filtroEnderecoEmailBean. */
	private FiltroEnderecoEmailBean filtroEnderecoEmailBean;
	
	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;
	
	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;
	
	/** Atributo cdBancoContaDebito. */
	private Integer cdBancoContaDebito;
	
	/** Atributo cdAgenciaBancariaContaDebito. */
	private Integer cdAgenciaBancariaContaDebito;
	
	/** Atributo cdContaBancariaContaDebito. */
	private Long cdContaBancariaContaDebito;
	
	/** Atributo cdDigitoContaContaDebito. */
	private String cdDigitoContaContaDebito;
	
	/** Atributo cdBancoContaCredito. */
	private Integer cdBancoContaCredito;
	
	/** Atributo cdAgenciaBancariaContaCredito. */
	private Integer cdAgenciaBancariaContaCredito;
	
	/** Atributo cdContaBancariaContaCredito. */
	private Long cdContaBancariaContaCredito;
	
	/** Atributo cdDigitoContaCredito. */
	private String cdDigitoContaCredito;
	
	/** Atributo consultasService. */
	private IConsultasService consultasService;
	
	//solEmissaoComprovantePagtoClientePag
	/** Atributo dtInicioSolicitacao. */
	private Date dtInicioSolicitacao;
	
	/** Atributo dtFimSolicitacao. */
	private Date dtFimSolicitacao;
	
	/** Atributo cboSituacaoSolicitacaoEstornoFiltro. */
	private Integer cboSituacaoSolicitacaoEstornoFiltro;
	
	/** Atributo listaSituacaoSolicitacaoEstornoFiltro. */
	private List<SelectItem> listaSituacaoSolicitacaoEstornoFiltro;
	
	/** Atributo listaGridSolEmiComprovanteCliePagador. */
	private List<ConsultarSolEmiComprovanteCliePagadorSaidaDTO> listaGridSolEmiComprovanteCliePagador;
	
	/** Atributo listaRadios. */
	private List<SelectItem> listaRadios;
	
	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;
	
	//detSolEmissaoComprovantePagtoClientePag
	/** Atributo cepFormatado. */
	private String cepFormatado;
	
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;
	
	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;
	
	/** Atributo dsEmpresa. */
	private String dsEmpresa;
	
	/** Atributo nroContrato. */
	private String nroContrato;
	
	/** Atributo dsContrato. */
	private String dsContrato;
	
	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;
	
	/** Atributo cdSolicitacaoPagamentoIntegrado. */
	private Integer cdSolicitacaoPagamentoIntegrado;
    
    /** Atributo nrSolicitacaoPagamentoIntegrado. */
    private Integer nrSolicitacaoPagamentoIntegrado;
    
    /** Atributo dtInicioPeriodoMovimentacao. */
    private String dtInicioPeriodoMovimentacao;
    
    /** Atributo dtFimPeriodoMovimentacao. */
    private String dtFimPeriodoMovimentacao;
    
    /** Atributo cdProdutoServicoOperacao. */
    private Integer cdProdutoServicoOperacao;
    
    /** Atributo dsProdutoServicoOperacao. */
    private String dsProdutoServicoOperacao;
    
    /** Atributo cdProdutoOperacaoRelacionado. */
    private Integer cdProdutoOperacaoRelacionado;
    
    /** Atributo dsProdutoOperacaoRelacionado. */
    private String dsProdutoOperacaoRelacionado;
    
    /** Atributo cdTipoInscricaoRecebedor. */
    private Integer cdTipoInscricaoRecebedor;
    
    /** Atributo cdCpfCnpjRecebedor. */
    private Long cdCpfCnpjRecebedor;
    
    /** Atributo cdFilialCnpjRecebedor. */
    private Integer cdFilialCnpjRecebedor;
    
    /** Atributo cdControleCpfRecebedor. */
    private Integer cdControleCpfRecebedor;
    
    /** Atributo cdDestinoCorrespSolicitacao. */
    private Integer cdDestinoCorrespSolicitacao;
    
    /** Atributo cdTipoPostagemSolicitacao. */
    private String cdTipoPostagemSolicitacao;
    
    /** Atributo dsLogradouroPagador. */
    private String dsLogradouroPagador;
    
    /** Atributo dsNumeroLogradouroPagador. */
    private String dsNumeroLogradouroPagador;
    
    /** Atributo dsComplementoLogradouroPagador. */
    private String dsComplementoLogradouroPagador;
    
    /** Atributo dsBairroClientePagador. */
    private String dsBairroClientePagador;
    
    /** Atributo dsMunicipioClientePagador. */
    private String dsMunicipioClientePagador;
    
    /** Atributo cdSiglaUfPagador. */
    private String cdSiglaUfPagador;
    
    /** Atributo cdCepPagador. */
    private Integer cdCepPagador;
    
    /** Atributo cdCepComplementoPagador. */
    private Integer cdCepComplementoPagador;
    
    /** Atributo dsEmailClientePagador. */
    private String dsEmailClientePagador;
    
    /** Atributo cdAgenciaOperadora. */
    private Integer cdAgenciaOperadora;
    
    /** Atributo dsAgenciaOperadora. */
    private String dsAgenciaOperadora;
    
    /** Atributo departamentoDestinoEntrega. */
    private String departamentoDestinoEntrega;
    
    /** Atributo vlTarifa. */
    private BigDecimal vlTarifa;
    
    /** Atributo percentualDescTarifa. */
    private BigDecimal percentualDescTarifa;
    
    /** Atributo cdUsuarioInclusao. */
    private String cdUsuarioInclusao;
    
    /** Atributo hrInclusaoRegistro. */
    private String hrInclusaoRegistro;
    
    /** Atributo cdOperacaoCanalInclusao. */
    private String cdOperacaoCanalInclusao;
    
    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;
    
    /** Atributo dsTipoCanalInclusao. */
    private String dsTipoCanalInclusao;
    
    /** Atributo comprovantes. */
    private List<DetalharOcorrenciasDTO> comprovantes;
    
    /** Atributo vlTarifaAtualizada. */
    private BigDecimal vlTarifaAtualizada;
    
    //incSolEmissaoComprovantePagtoClientePagadorFiltro
    /** Atributo radioIncluirSelecionado. */
    private String radioIncluirSelecionado;
    
    /** Atributo radioIncluirFavorecido. */
    private String radioIncluirFavorecido;
    
    /** Atributo dtInicioPeriodoPagamento. */
    private Date dtInicioPeriodoPagamento;
    
    /** Atributo dtFimPeriodoPagamento. */
    private Date dtFimPeriodoPagamento;
    
    /** Atributo tipoServicoFiltro. */
    private Integer tipoServicoFiltro;
	
	/** Atributo modalidadeFiltro. */
	private Integer modalidadeFiltro;
	
	/** Atributo numeroPagamentoDe. */
	private String numeroPagamentoDe;
	
	/** Atributo numeroPagamentoAte. */
	private String numeroPagamentoAte;
	
	/** Atributo valorPagamentoDe. */
	private BigDecimal valorPagamentoDe;
	
	/** Atributo valorPagamentoAte. */
	private BigDecimal valorPagamentoAte;
	
	/** Atributo cdFavorecido. */
	private Long cdFavorecido;
    
    /** Atributo inscricaoFavorecido. */
    private String inscricaoFavorecido;
	
	/** Atributo tipoInscricaoFiltro. */
	private Integer tipoInscricaoFiltro;
	
	/** Atributo listaTipoServicoFiltro. */
	private List<SelectItem> listaTipoServicoFiltro = new ArrayList<SelectItem>();
	
	/** Atributo listaTipoServicoHash. */
	private Map<Integer,String> listaTipoServicoHash = new HashMap<Integer,String>();
	
	/** Atributo listaModalidadeFiltro. */
	private List<SelectItem> listaModalidadeFiltro = new ArrayList<SelectItem>();
	
	/** Atributo listaModalidadeHash. */
	private Map<Integer,String> listaModalidadeHash = new HashMap<Integer,String>();
	
	/** Atributo listaTipoInscricaoFiltro. */
	private List<SelectItem>  listaTipoInscricaoFiltro = new ArrayList<SelectItem>();
    
    /** Atributo listaIncluirComprovantes. */
    private List<ListarIncluirSolEmiComprovanteSaidaDTO> listaIncluirComprovantes;
    
    /** Atributo opcaoChecarTodos. */
    private boolean opcaoChecarTodos = false;
	
	/** Atributo panelBotoes. */
	private boolean panelBotoes = false;
	
	/** Atributo habilitaCampoEndereco. */
	private boolean habilitaCampoEndereco;
	
	
    //incSolEmissaoComprovantePagtoClientePagInfoAdicionais
	/** Atributo listaIncluirComprovantesSelecionados. */
    private List<ListarIncluirSolEmiComprovanteSaidaDTO> listaIncluirComprovantesSelecionados;
	
	/** Atributo radioInfoAdicionais. */
	private String radioInfoAdicionais;
	
	/** Atributo radioTipoPostagem. */
	private String radioTipoPostagem;
	
	/** Atributo radioAgenciaDepto. */
	private String radioAgenciaDepto;
	
	/** Atributo agenciaOperadora. */
	private String agenciaOperadora;
	
	/** Atributo codAgenciaOperadora. */
	private Integer codAgenciaOperadora;
	
	/** Atributo departamentoFiltro. */
	private String departamentoFiltro;
	
	/** Atributo cdPessoaJuridicaDepartamento. */
	private Long cdPessoaJuridicaDepartamento;
    
    /** Atributo cdTipoContratoNegocio. */
    private Integer cdTipoContratoNegocio;
    
    /** Atributo nrSequenciaDepartamento. */
    private Integer nrSequenciaDepartamento;
    
	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo disableArgumentosIncluir. */
	private boolean disableArgumentosIncluir;
	
	/** Atributo desabilitaDescontoTarifa. */
	private boolean desabilitaDescontoTarifa;

    
	/** M�todos **/
	
	public void listarCmbTipoFavorecido(){
		try{
			listaTipoInscricaoFiltro = new ArrayList<SelectItem>();
			/*
			List<InscricaoFavorecidosSaidaDTO> list = new ArrayList<InscricaoFavorecidosSaidaDTO>();
			
			list = comboService.listarTipoInscricao();
			listaTipoInscricaoFiltro.clear();
			
			for(InscricaoFavorecidosSaidaDTO combo : list){			
				listaTipoInscricaoFiltro.add(new SelectItem(combo.getCdTipoInscricaoFavorecidos(),combo.getDsTipoInscricaoFavorecidos()));
			}
			*/
			listaTipoInscricaoFiltro.add(new SelectItem(1,MessageHelperUtils.getI18nMessage("label_cpf")));
			listaTipoInscricaoFiltro.add(new SelectItem(2,MessageHelperUtils.getI18nMessage("label_cnpj")));
			
		}catch(PdcAdapterFunctionalException p){
			listaTipoInscricaoFiltro = new ArrayList<SelectItem>();
		}
	}
	
	//Inicializa��o
	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt){
		limparTelaPrincipal();
		
		//Carregamento do Combo
		listarConsultarSituacaoSolicitacaoEstorno();		
		
		//Tela de Filtro
		filtroAgendamentoEfetivacaoEstornoBean.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();
		
		filtroAgendamentoEfetivacaoEstornoBean.setPaginaRetorno("solEmissaoComprovantePagtoClientePag");
		setPanelBotoes(false);
		
		setHabilitaArgumentosPesquisa(false);
		filtroAgendamentoEfetivacaoEstornoBean.setClienteContratoSelecionado(false);		
		limparIncluir();
		setDisableArgumentosConsulta(false);
		setDesabilitaDescontoTarifa(false);
		filtroAgendamentoEfetivacaoEstornoBean.setEmpresaGestoraFiltro(2269651L);	
		
		//Tela de Filtro de Endere�o/Email
		getFiltroEnderecoEmailBean().setPaginaRetorno("incSolEmissaoComprovantePagtoClientePagInfAdicionais");		
	}
	
	//Consultar Situa��o de Solicita��o de Estorno
	/**
	 * Listar consultar situacao solicitacao estorno.
	 */
	public void listarConsultarSituacaoSolicitacaoEstorno(){
		try{
			listaSituacaoSolicitacaoEstornoFiltro = new ArrayList<SelectItem>();
			
			ConsultarSituacaoSolicitacaoEstornoEntradaDTO entrada = new ConsultarSituacaoSolicitacaoEstornoEntradaDTO();
			
			entrada.setCdSituacao(0);
			entrada.setNumeroOcorrencias(30);
			entrada.setCdIndicador(1);
			
			
			List<ConsultarSituacaoSolicitacaoEstornoSaidaDTO> list = getComboService().consultarSituacaoSolicitacaoEstorno(entrada);
	
			for (ConsultarSituacaoSolicitacaoEstornoSaidaDTO saida : list){
				listaSituacaoSolicitacaoEstornoFiltro.add(new SelectItem(saida.getCodigo(), saida.getDescricao()));
			}
		}catch(PdcAdapterFunctionalException p){
			listaSituacaoSolicitacaoEstornoFiltro = new ArrayList<SelectItem>();
		}		
	}
	
	/**
	 * Consultar contrato.
	 */
	public void consultarContrato(){
		getFiltroAgendamentoEfetivacaoEstornoBean().consultarContrato();
	}
	
	//Controle Exibi��o de Dados	
	/**
	 * Carrega cabecalho.
	 */
	private void carregaCabecalho(){
		if(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("0")){ //Filtrar por cliente
			setNrCnpjCpf(filtroAgendamentoEfetivacaoEstornoBean.getNrCpfCnpjCliente());
			setDsRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoRazaoSocialCliente());
			setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean.getEmpresaGestoraDescCliente());
			setNroContrato(filtroAgendamentoEfetivacaoEstornoBean.getNumeroDescCliente());
			setDsContrato(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoContratoDescCliente());
			setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getSituacaoDescCliente());
		}
		else{
			if(filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado().equals("1")){ //Filtrar por contrato
				try{
					DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
					entradaDTO.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
					entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio());
					entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio());				
					DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService().detalharDadosContrato(entradaDTO);
					setNrCnpjCpf(saidaDTO.getCdCnpjCpfTitular());
					setDsRazaoSocial(saidaDTO.getDsParticipanteTitular());
				} catch (PdcAdapterFunctionalException p) {	
					setNrCnpjCpf("");
					setDsRazaoSocial("");
				}
				setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean.getEmpresaGestoraDescContrato());
				setNroContrato(filtroAgendamentoEfetivacaoEstornoBean.getNumeroDescContrato());
				setDsContrato(filtroAgendamentoEfetivacaoEstornoBean.getDescricaoContratoDescContrato());
				setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean.getSituacaoDescContrato());
			}
		}
	}
	
	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt){
		setDtInicioSolicitacao(new Date());
		setDtFimSolicitacao(new Date());
		setCboSituacaoSolicitacaoEstornoFiltro(null);
		setListaGridSolEmiComprovanteCliePagador(null);
		setItemSelecionadoLista(null);
		setPanelBotoes(false);
		
	}
	
	/**
	 * Limpar argumentos incluir.
	 */
	public void limparArgumentosIncluir(){
		setRadioIncluirFavorecido("");
		setNumeroPagamentoDe("");
		setNumeroPagamentoAte("");
		setValorPagamentoDe(null);
		setValorPagamentoAte(null);
		limparArgumentosFavorecido();
		limparListaIncluir();
	}
	
	/**
	 * Limpar argumentos favorecido.
	 */
	public void limparArgumentosFavorecido(){
		setCdFavorecido(null);
	    setInscricaoFavorecido("");
	    setTipoInscricaoFiltro(null);
	    limparListaIncluir();
	}
	
	/**
	 * Limpar info adicionais.
	 */
	public void limparInfoAdicionais(){
		setRadioTipoPostagem("");
		setRadioAgenciaDepto("");
		setDepartamentoFiltro("");
		getFiltroEnderecoEmailBean().limparEnderecoEmail();
		getFiltroEnderecoEmailBean().setEmpresaGestoraContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());		
	}
	
	/**
	 * Limpar tipo postagem.
	 */
	public void limparTipoPostagem(){
		getFiltroEnderecoEmailBean().limparEnderecoEmail();
		getFiltroEnderecoEmailBean().setFlagEnderecoEmail(getRadioTipoPostagem());
		setHabilitaCampoEndereco(true);
		setCdCepPagador(0);
		setCdCepComplementoPagador(0);
	}
	
	/**
	 * Limpar agencia operadora.
	 */
	public void limparAgenciaOperadora(){
		setDepartamentoFiltro("");
	}
	
	/**
	 * Limpar tela principal.
	 */
	public void limparTelaPrincipal(){
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		setDtInicioSolicitacao(new Date());
		setDtFimSolicitacao(new Date());
		setCboSituacaoSolicitacaoEstornoFiltro(null);
		setListaGridSolEmiComprovanteCliePagador(null);
		setItemSelecionadoLista(null);
		setPanelBotoes(false);
	}
	
	/**
	 * Limpar campos.
	 *
	 * @return the string
	 */
	public String limparCampos(){
		limparTelaPrincipal();
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado("");
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		return "";
	}
	
	/**
	 * Limpar grid consultar.
	 *
	 * @return the string
	 */
	public String limparGridConsultar(){
		setListaGridSolEmiComprovanteCliePagador(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		return "";
	}
	
	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente(){
		//Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		setDtInicioSolicitacao(new Date());
		setDtFimSolicitacao(new Date());
		setCboSituacaoSolicitacaoEstornoFiltro(null);
		setListaGridSolEmiComprovanteCliePagador(null);
		setItemSelecionadoLista(null);
		setPanelBotoes(false);
		
		return "";

	}		
	
	/**
	 * Pesquisar.
	 */
	public void pesquisar(){
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
		for (int i = 0; i < getListaIncluirComprovantes().size(); i++) {
			getListaIncluirComprovantes().get(i).setCheck(false);			
		}
	}
	
	/**
	 * Pesquisar incluir.
	 *
	 * @param e the e
	 * @return the string
	 */
	public String pesquisarIncluir(ActionEvent e){		
		listarIncluirSolEmiComprovante();
		return "";
	}
	
	/**
	 * Pesquisar consulta.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarConsulta(ActionEvent evt){
		consultarSolEmiComprovanteClientePagador();
		return "";
	}
	
	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes(){
		for(int i=0;i<getListaIncluirComprovantes().size();i++){
			if(getListaIncluirComprovantes().get(i).isCheck()){
				setPanelBotoes(true);
				return;
			}
		}
		setPanelBotoes(false);		
	}
	
	/**
	 * Limpar variaveis detalhes.
	 */
	public void limparVariaveisDetalhes(){
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");
		setNrSolicitacaoPagamentoIntegrado(null);
	    setDtInicioPeriodoMovimentacao("");
	    setDtFimPeriodoMovimentacao("");
	    setDsProdutoServicoOperacao("");
	    setDsProdutoOperacaoRelacionado("");
	    setDsLogradouroPagador("");
	    setDsNumeroLogradouroPagador("");
	    setDsComplementoLogradouroPagador("");
	    setDsBairroClientePagador("");
	    setDsMunicipioClientePagador("");
	    setCdSiglaUfPagador("");
	    setCepFormatado("");
	    setDsEmailClientePagador("");
	    setDsAgenciaOperadora("");
	    setDepartamentoDestinoEntrega(null);
	    setVlTarifa(BigDecimal.ZERO);
	    setPercentualDescTarifa(BigDecimal.ZERO);
	    setVlTarifaAtualizada(BigDecimal.ZERO);
	    setHrInclusaoRegistro("");
	    setCdUsuarioInclusao("");
	    setTipoCanalInclusao("");
	    setCdOperacaoCanalInclusao("");
	}
	
	/**
	 * Limpar incluir.
	 */
	public void limparIncluir(){
		setRadioIncluirSelecionado("");
	    setRadioIncluirFavorecido("");
		setDtInicioPeriodoPagamento(new Date());
		setDtFimPeriodoPagamento(new Date());
	    setTipoServicoFiltro(null);
		setModalidadeFiltro(null);
		limparArgumentosIncluir();
		limparArgumentosFavorecido();
		limparListaIncluir();
		setListaIncluirComprovantes(null);
		setPanelBotoes(false);
		setOpcaoChecarTodos(false);

		setListaModalidadeFiltro(new ArrayList<SelectItem>());
	}
	
	/**
	 * Limpar lista incluir.
	 */
	public void limparListaIncluir(){
		setListaIncluirComprovantes(null);
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
		setDisableArgumentosIncluir(false);
	}
	
	/**
	 * Consultar sol emi comprovante cliente pagador.
	 *
	 * @param evt the evt
	 */
	public void consultarSolEmiComprovanteClientePagador(ActionEvent evt){
		consultarSolEmiComprovanteClientePagador();
	}
	
	//Carrega Lista
	/**
	 * Consultar sol emi comprovante cliente pagador.
	 */
	public void consultarSolEmiComprovanteClientePagador(){
		try{
			ConsultarSolEmiComprovanteCliePagadorEntradaDTO entradaDTO = new ConsultarSolEmiComprovanteCliePagadorEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato() != null ? filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato()	: 0L);
			entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio() != null ? filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio() : 0);
			entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio() != null ? filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio() : 0L);	
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			entradaDTO.setDtInicioSolicitacao((getDtInicioSolicitacao() == null) ? "" : sdf.format(getDtInicioSolicitacao()) );
			entradaDTO.setDtFimSolicitacao((getDtFimSolicitacao() == null) ? "" : sdf.format(getDtFimSolicitacao()));
	    	entradaDTO.setCdSituacaoSolicitacao(getCboSituacaoSolicitacaoEstornoFiltro() != null ? getCboSituacaoSolicitacaoEstornoFiltro() : 0);
	    	
	    	setListaGridSolEmiComprovanteCliePagador(getSolEmissaoComprovantePagtoClientePagServiceImpl().consultarSolEmiComprovanteCliePagador(entradaDTO));
	    	
	    	listaRadios = new ArrayList<SelectItem>();
			
			for(int i=0;i<getListaGridSolEmiComprovanteCliePagador().size();i++){
				listaRadios.add(new SelectItem(i,""));
			}
			setDisableArgumentosConsulta(true);
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaGridSolEmiComprovanteCliePagador(null);
			setDisableArgumentosConsulta(false);
		}
		finally{
			setItemSelecionadoLista(null);
		}
	}
	
	/**
	 * Pesquisar excluir detalhar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarExcluirDetalhar(ActionEvent evt){
		preencheDadosSolEmiComprovanteClientePagador("");
		return "";
	}
	
	/**
	 * Preenche dados sol emi comprovante cliente pagador.
	 *
	 * @param retorno the retorno
	 * @return the string
	 */
	public String preencheDadosSolEmiComprovanteClientePagador(String retorno){
		try{
			DetalharSolEmiComprovanteCliePagadorEntradaDTO entradaDTO = new DetalharSolEmiComprovanteCliePagadorEntradaDTO();
			
			limparVariaveisDetalhes();
			carregaCabecalho();
			
			entradaDTO.setCdPessoaJuridicaEmpr(getListaGridSolEmiComprovanteCliePagador().get(itemSelecionadoLista).getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(getListaGridSolEmiComprovanteCliePagador().get(itemSelecionadoLista).getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(getListaGridSolEmiComprovanteCliePagador().get(itemSelecionadoLista).getNrSequenciaContrato());
	    	entradaDTO.setCdTipoSolicitacaoPagamento(getListaGridSolEmiComprovanteCliePagador().get(itemSelecionadoLista).getCdTipoSolicitacao());
	    	entradaDTO.setCdSolicitacao(getListaGridSolEmiComprovanteCliePagador().get(itemSelecionadoLista).getNrSolicitacao());
	    	
	    	DetalharSolEmiComprovanteCliePagadorSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl().detalharSolEmiComprovanteCliePagador(entradaDTO);
	    	
	    	setCdSolicitacaoPagamentoIntegrado(saidaDTO.getCdSolicitacaoPagamentoIntegrado());
		    setNrSolicitacaoPagamentoIntegrado(saidaDTO.getNrSolicitacaoPagamentoIntegrado());
		    setDtInicioPeriodoMovimentacao(saidaDTO.getDtInicioPeriodoMovimentacao());
		    setDtFimPeriodoMovimentacao(saidaDTO.getDtFimPeriodoMovimentacao());
		    setCdProdutoServicoOperacao(saidaDTO.getCdProdutoServicoOperacao());
		    setDsProdutoServicoOperacao(saidaDTO.getDsProdutoServicoOperacao());
		    setCdProdutoOperacaoRelacionado(saidaDTO.getCdProdutoOperacaoRelacionado());
		    setDsProdutoOperacaoRelacionado(saidaDTO.getDsProdutoOperacaoRelacionado());
		    setCdTipoInscricaoRecebedor(saidaDTO.getCdTipoInscricaoRecebedor());
		    
		    if(saidaDTO.getCdDestinoCorrespSolicitacao() == 1){
		    	if(saidaDTO.getCdTipoPostagemSolicitacao().equals("C")){
		    		setDsLogradouroPagador(saidaDTO.getDsLogradouroPagador());
				    setDsNumeroLogradouroPagador(saidaDTO.getDsNumeroLogradouroPagador());
				    setDsComplementoLogradouroPagador(saidaDTO.getDsComplementoLogradouroPagador());
				    setDsBairroClientePagador(saidaDTO.getDsBairroClientePagador());
				    setDsMunicipioClientePagador(saidaDTO.getDsMunicipioClientePagador());
				    setCdSiglaUfPagador(saidaDTO.getCdSiglaUfPagador());
				    setCepFormatado(saidaDTO.getCepFormatado());
				}else{
		    		if(saidaDTO.getCdTipoPostagemSolicitacao().equals("E")){
		    			setDsEmailClientePagador(saidaDTO.getDsEmailClientePagador());
		    		}
		    	}
		    }else{
		    	if(saidaDTO.getCdDestinoCorrespSolicitacao() == 2){
		    		if(saidaDTO.getCdTipoPostagemSolicitacao().equals("A")){
		    			setDsAgenciaOperadora(PgitUtil.concatenarCampos(saidaDTO.getCdAgenciaOperadora(), saidaDTO.getDsAgencia()));
		    		}else{
		    			if(saidaDTO.getCdTipoPostagemSolicitacao().equals("D")){
		    				setDepartamentoDestinoEntrega(PgitUtil.concatenarCampos(saidaDTO.getCdDepartamentoUnidade(), saidaDTO.getDsAgencia()));
		    			}
		    		}
		    	}
		    }
		    
		    setVlTarifa(saidaDTO.getVlTarifa());
		    setPercentualDescTarifa(saidaDTO.getCdPercentualDescTarifa());
		    setVlTarifaAtualizada(PgitUtil.calcularValorTarifaAtualizada(saidaDTO.getVlTarifa(), saidaDTO.getCdPercentualDescTarifa()));
		    setComprovantes(saidaDTO.getListaOcorrencias());
		    
		    //Trilha
		    setCdUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
		    setHrInclusaoRegistro(saidaDTO.getHrInclusaoRegistro());
		    setCdOperacaoCanalInclusao(saidaDTO.getCdOperacaoCanalInclusao());
		    setTipoCanalInclusao(PgitUtil.concatenarCampos(saidaDTO.getCdTipoCanalInclusao(), saidaDTO.getDsTipoCanalInclusao()));
		
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setComprovantes(null);
			return "";
		}
		
		return retorno;
	}
	
	/**
	 * Excluir sol emi comprovante cliente pagador.
	 *
	 * @return the string
	 */
	public String excluirSolEmiComprovanteClientePagador(){
		try{
			ExcluirSolEmiComprovanteCliePagadorEntradaDTO entradaDTO = new ExcluirSolEmiComprovanteCliePagadorEntradaDTO();
			
			entradaDTO.setCdSolicitacaoPagamentoIntegrado(getCdSolicitacaoPagamentoIntegrado());
	    	entradaDTO.setNrSolicitacaoPagamentoIntegrado(getNrSolicitacaoPagamentoIntegrado());
	        entradaDTO.setCdPessoaJuridicaContrato(getListaGridSolEmiComprovanteCliePagador().get(itemSelecionadoLista).getCdPessoaJuridica());
	        entradaDTO.setNrSequenciaContratoNegocio(getListaGridSolEmiComprovanteCliePagador().get(itemSelecionadoLista).getNrSequenciaContrato());
	        entradaDTO.setCdTipoContratoNegocio(getListaGridSolEmiComprovanteCliePagador().get(itemSelecionadoLista).getCdTipoContrato());
	            
	        List<ExcluirSolEmiComprovanteCliePagadorOcorrenciasDTO> lista = new ArrayList<ExcluirSolEmiComprovanteCliePagadorOcorrenciasDTO>();
	        ExcluirSolEmiComprovanteCliePagadorOcorrenciasDTO listaEntradaDTO;
			
	        for (int i=0; i < getComprovantes().size(); i++){   
				listaEntradaDTO = new ExcluirSolEmiComprovanteCliePagadorOcorrenciasDTO();
				listaEntradaDTO.setCdTipoCanal(getComprovantes().get(i).getCdTipoCanal());
				listaEntradaDTO.setCdControlePagamento(getComprovantes().get(i).getCdControlePagamento());
				lista.add(listaEntradaDTO);  	    	  
	     	} 	
		 
			entradaDTO.setComprovantes(lista);
			
	        ExcluirSolEmiComprovanteCliePagadorSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl().excluirSolEmiComprovanteCliePagador(entradaDTO);
			
			BradescoFacesUtils.addInfoModalMessage("(" +saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "solEmissaoComprovantePagtoClientePag", "#{solEmissaoComprovantePagtoClientePagBean.consultarSolEmiComprovanteClientePagador}", false);
			
		}catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			return null;
		}
		return "";
	}
	
	/**
	 * Incluir sol emi comprovante cliente pagador.
	 *
	 * @return the string
	 */
	public String incluirSolEmiComprovanteClientePagador(){
		try{
			IncluirSolEmiComprovanteCliePagadorEntradaDTO entradaDTO = new IncluirSolEmiComprovanteCliePagadorEntradaDTO();
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			entradaDTO.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio());
	        entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio());
	        entradaDTO.setDtInicioPeriodoMovimentacao(sdf.format(getDtInicioPeriodoPagamento()));
	        entradaDTO.setDtFimPeriodoMovimentacao(sdf.format(getDtFimPeriodoPagamento()));
			entradaDTO.setCdRecebedorCredito(getCdFavorecido());
			entradaDTO.setCdTipoInscricaoRecebedor(getTipoInscricaoFiltro());
			entradaDTO.setCdProdutoServicoOperacao(tipoServicoFiltro == null ? 0 : tipoServicoFiltro);
			entradaDTO.setCdProdutoOperacaoRelacionado(modalidadeFiltro == null ? 0 : modalidadeFiltro);

			setInscricaoFavorecido(PgitUtil.complementaDigito(PgitUtil.verificaStringNula(getInscricaoFavorecido()), 15));
			entradaDTO.setCdCpfCnpjRecebedor(Long.valueOf(getInscricaoFavorecido().substring(0, 9)));
	        entradaDTO.setCdFilialCnpjRecebedor(Integer.parseInt(getInscricaoFavorecido().substring(9, 13)));
	        entradaDTO.setCdControleCpfRecebedor(Integer.parseInt(getInscricaoFavorecido().substring(13, 15)));
			if(getRadioInfoAdicionais().equals("0") && getRadioTipoPostagem().equals("0")){
				entradaDTO.setCdTipoPostagemSolicitacao("C");
				entradaDTO.setCdDestinoCorrespSolicitacao(1);
			}else{
				if(getRadioInfoAdicionais().equals("0") && getRadioTipoPostagem().equals("1")){
					entradaDTO.setCdTipoPostagemSolicitacao("E");
					entradaDTO.setCdDestinoCorrespSolicitacao(1);
				}else{
					if(getRadioInfoAdicionais().equals("1") && getRadioAgenciaDepto().equals("0")){
						entradaDTO.setCdTipoPostagemSolicitacao("A");
						entradaDTO.setCdPessoaJuridicaDepto(getCdPessoaJuridicaDepartamento());
						entradaDTO.setNrSequenciaUnidadeDepto(getNrSequenciaDepartamento());
						entradaDTO.setCdDestinoCorrespSolicitacao(2);
					}else{
						if(getRadioInfoAdicionais().equals("1") && getRadioAgenciaDepto().equals("1")){
							entradaDTO.setCdTipoPostagemSolicitacao("D");
							entradaDTO.setCdPessoaJuridicaDepto(getCdPessoaJuridicaDepartamento());
							entradaDTO.setNrSequenciaUnidadeDepto(getNrSequenciaDepartamento());
							entradaDTO.setCdDestinoCorrespSolicitacao(2);
						}
					}
				}
			}
			entradaDTO.setDsLogradouroPagador(getDsLogradouroPagador());
	        entradaDTO.setDsNumeroLogradouroPagador(getDsNumeroLogradouroPagador());
	        entradaDTO.setDsComplementoLogradouroPagador(getDsComplementoLogradouroPagador());
	        entradaDTO.setDsBairroClientePagador(getDsBairroClientePagador());
	        entradaDTO.setDsMunicipioClientePagador(getDsMunicipioClientePagador());
	        entradaDTO.setCdSiglaUfPagador(getCdSiglaUfPagador());
	        entradaDTO.setCdCepPagador(getCdCepPagador());
	        entradaDTO.setCdCepComplementoPagador(getCdCepComplementoPagador());
	        entradaDTO.setDsEmailClientePagador(getDsEmailClientePagador());
	        entradaDTO.setCdPercentualBonifTarifaSolicitacao(getPercentualDescTarifa());
	        entradaDTO.setVlTarifaNegocioSolicitacao(getVlTarifa());
	        entradaDTO.setCdDepartamentoUnidade(getDepartamentoFiltro().equals("")?0:Integer.parseInt(getDepartamentoFiltro()));
	        
	        IncluirOcorrenciasDTO ocorrencia;
	        for(int i=0; i<getListaIncluirComprovantesSelecionados().size(); i++){
	        	ocorrencia = new IncluirOcorrenciasDTO();
	        	
	        	ocorrencia.setCdControlePagamento(getListaIncluirComprovantesSelecionados().get(i).getCdControlePagamento());
	        	ocorrencia.setCdTipoCanal(0);
	     	    	ocorrencia.setCdModalidadePagamentoCliente(getListaIncluirComprovantesSelecionados().get(i).getCdTipoTela());
	     	    
	     	    entradaDTO.getListaOcorrencias().add(ocorrencia);
	        }
	        
			IncluirSolEmiComprovanteCliePagadorSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl().incluirSolEmiComprovanteCliePagador(entradaDTO);
			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(), "solEmissaoComprovantePagtoClientePag", BradescoViewExceptionActionType.ACTION, false);
			
			if(getListaGridSolEmiComprovanteCliePagador() != null){
				consultarSolEmiComprovanteClientePagador();
			}
		
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), "incSolEmissaoComprovantePagtoClientePagInfAdicionais", BradescoViewExceptionActionType.ACTION, false);
			setPanelBotoes(false);
			return "";
		}
		return "";
	}
	
	/**
	 * Listar incluir sol emi comprovante.
	 */
	public void listarIncluirSolEmiComprovante(){
		try{
			ListarIncluirSolEmiComprovanteEntradaDTO entradaDTO = new ListarIncluirSolEmiComprovanteEntradaDTO();
			
	        entradaDTO.setNrOcorrencias(30);
	        entradaDTO.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
	        entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio());
	        entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio());
	        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	        entradaDTO.setDtCreditoPagamentoInicio((getDtInicioPeriodoPagamento() == null) ? "" : sdf.format(getDtInicioPeriodoPagamento()) );
			entradaDTO.setDtCreditoPagamentoFim((getDtFimPeriodoPagamento() == null) ? "" : sdf.format(getDtFimPeriodoPagamento()));
	        entradaDTO.setCdProdutoServicoOperacao((getTipoServicoFiltro() == null) ? 0 : getTipoServicoFiltro());
	        entradaDTO.setCdProdutoServicoRelacionado((getModalidadeFiltro() == null) ? 0 :getModalidadeFiltro());
	        entradaDTO.setCdControlePagamentoDe(getNumeroPagamentoDe());
	        entradaDTO.setCdControlePagamentoAte(getNumeroPagamentoAte());
	        entradaDTO.setVlPagamentoDe(getValorPagamentoDe());
	        entradaDTO.setVlPagamentoAte(getValorPagamentoAte());
	        entradaDTO.setCdFavorecidoClientePagador((getCdFavorecido() == null) ? 0L : getCdFavorecido());
	        entradaDTO.setCdInscricaoFavorecido((getInscricaoFavorecido().equals("")) ? 0L :Long.valueOf(getInscricaoFavorecido()));
	        entradaDTO.setCdIdentificacaoInscricaoFavorecido((getTipoInscricaoFiltro() == null) ? 0 :getTipoInscricaoFiltro());

			setListaIncluirComprovantes(getSolEmissaoComprovantePagtoClientePagServiceImpl().listarIncluirSolEmiComprovante(entradaDTO));
			
			setPanelBotoes(false);
			setOpcaoChecarTodos(false);
			setDisableArgumentosIncluir(true);
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
			setListaIncluirComprovantes(null);
			setPanelBotoes(false);
			setDisableArgumentosIncluir(false);
		}
		finally{
			setOpcaoChecarTodos(false);
		}
	}
	
	/**
	 * Consultar agencia operadora.
	 */
	public void consultarAgenciaOperadora(){
		try{
			ConsultarAgenciaOperadoraEntradaDTO entradaDTO = new ConsultarAgenciaOperadoraEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
	        entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio());
	        entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio());
	        
			ConsultarAgenciaOperadoraSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl().consultarAgenciaOperadora(entradaDTO);
			
			setCodAgenciaOperadora(saidaDTO.getCdAgencia());
		    setAgenciaOperadora(PgitUtil.concatenarCampos(saidaDTO.getCdAgencia(), saidaDTO.getDsAgencia()));
		    setCdPessoaJuridicaDepartamento(saidaDTO.getCdPessoaJuridicaDepartamento());
		    setNrSequenciaDepartamento(saidaDTO.getNrSequenciaDepartamento());
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
	}

	/**
	 * Consultar agencia ope tarifa padrao.
	 */
	public void consultarAgenciaOpeTarifaPadrao(){
		try{
			ConsultarAgenciaOpeTarifaPadraoEntradaDTO entradaDTO = new ConsultarAgenciaOpeTarifaPadraoEntradaDTO();
			
			entradaDTO.setCdPessoaJuridicaEmpresa(filtroAgendamentoEfetivacaoEstornoBean.getCdPessoaJuridicaContrato());
	        entradaDTO.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getCdTipoContratoNegocio());
	        entradaDTO.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean.getNrSequenciaContratoNegocio());
	        entradaDTO.setCdProdutoServicoOperacao(0);
	        entradaDTO.setCdProdutoServicoRelacionado(0);
	        entradaDTO.setCdTipoTarifa(3);

			ConsultarAgenciaOpeTarifaPadraoSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl().consultarAgenciaOpeTarifaPadrao(entradaDTO);
			
			setVlTarifa(saidaDTO.getVlTarifaPadrao());
		}
		catch(PdcAdapterFunctionalException p){
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(), false);
		}
	}
	
	/**
	 * Carrega lista comprovantes selecionados.
	 */
	public void carregaListaComprovantesSelecionados(){	
		carregaCabecalho();
		listaIncluirComprovantesSelecionados = new ArrayList<ListarIncluirSolEmiComprovanteSaidaDTO>();
		
		for(ListarIncluirSolEmiComprovanteSaidaDTO saida : getListaIncluirComprovantes()){
			if(saida.isCheck()){
				listaIncluirComprovantesSelecionados.add(saida);
			}
		}
	}
	
	/**
	 * Checar todos.
	 */
	public void checarTodos(){
		for(ListarIncluirSolEmiComprovanteSaidaDTO saida : getListaIncluirComprovantes()){
			saida.setRegistroSelecionado(isOpcaoChecarTodos());
		}
		setPanelBotoes(isOpcaoChecarTodos());
	}
	
	
	
	/**
	 * Listar tipo servico.
	 */
	public void listarTipoServico(){
		try{
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
			
			/* ListarServicosEntradaDTO entrada = new ListarServicosEntradaDTO();
			entrada.setCdNaturezaServico(1);
			entrada.setNumeroOcorrencias(30); */
			
			List<ConsultarServicoPagtoSaidaDTO> list = comboService.consultarServicoPagto();
			listaTipoServicoHash.clear();
			for (ConsultarServicoPagtoSaidaDTO saida : list){
				listaTipoServicoFiltro.add(new SelectItem(saida.getCdServico(), saida.getDsServico()));
				listaTipoServicoHash.put(saida.getCdServico(), saida.getDsServico());
				
			}
			
		} catch (PdcAdapterFunctionalException p) {
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
			listaTipoServicoHash = new HashMap<Integer,String>();
		}			
	}
	
	/**
	 * Listar tipo servico sem pagto.
	 */
	public void listarTipoServicoSemPagto(){
		try{
			listaTipoServicoFiltro = new ArrayList<SelectItem>();

			List<ConsultarServicoPagtoSaidaDTO> list = comboService.consultarServicoPagto();
			listaTipoServicoHash.clear();
			for (ConsultarServicoPagtoSaidaDTO saida : list){
				listaTipoServicoFiltro.add(new SelectItem(saida.getCdServico(), saida.getDsServico()));
				listaTipoServicoHash.put(saida.getCdServico(), saida.getDsServico());
				
			}
			
		} catch (PdcAdapterFunctionalException p) {
			listaTipoServicoFiltro = new ArrayList<SelectItem>();
			listaTipoServicoHash = new HashMap<Integer,String>();
		}			
	}
	
	/**
	 * Listar modalidades.
	 */
	public void listarModalidades(){
		try{
			setModalidadeFiltro(null);
			if (getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0){ 
					listaModalidadeFiltro = new ArrayList<SelectItem>();
									
					List<ConsultarModalidadePagtoSaidaDTO> list = comboService.consultarModalidadePagto(getTipoServicoFiltro());
					listaModalidadeHash.clear();
					for (ConsultarModalidadePagtoSaidaDTO saida : list){
						listaModalidadeFiltro.add(new SelectItem(saida.getCdProdutoOperacaoRelacionado(), saida.getDsProdutoOperacaoRelacionado()));
						listaModalidadeHash.put(saida.getCdProdutoOperacaoRelacionado(), saida.getDsProdutoOperacaoRelacionado());
					}
			}else{
				listaModalidadeFiltro = new ArrayList<SelectItem>();
			}
		} catch (PdcAdapterFunctionalException p) {
			listaModalidadeFiltro = new ArrayList<SelectItem>();
			listaModalidadeHash = new HashMap<Integer,String>();
		}			
	}
	
	//Navega��o
	
	/**
	 * Limpar campos incluir.
	 */
	public void limparCamposIncluir(){
		setDsLogradouroPagador("");
		setDsNumeroLogradouroPagador("");
		setDsComplementoLogradouroPagador("");
		setDsBairroClientePagador("");
		setDsMunicipioClientePagador("");
		setCdSiglaUfPagador("");
		setCepFormatado("");
		setDsEmailClientePagador("");
		setDsAgenciaOperadora("");
		setDepartamentoDestinoEntrega("");
		setVlTarifaAtualizada(BigDecimal.ZERO);
	}
	
	/**
	 * Confirmar inclusao.
	 *
	 * @return the string
	 */
	public String confirmarInclusao(){
		//Dados Solicitacao
		setDtInicioPeriodoMovimentacao(FormatarData.formataDiaMesAno(getDtInicioPeriodoPagamento()));
		setDtFimPeriodoMovimentacao(FormatarData.formataDiaMesAno(getDtFimPeriodoPagamento()));
		setDsProdutoServicoOperacao(getListaTipoServicoHash().get(getTipoServicoFiltro()));
		setDsProdutoOperacaoRelacionado(getListaModalidadeHash().get(getModalidadeFiltro()));
		
		//DadosDestino
		limparCamposIncluir();
		if(getRadioInfoAdicionais().equals("0")){
			if(getRadioTipoPostagem().equals("0")){
				ConsultarEnderecoSaidaDTO enderecoSelecionado = getFiltroEnderecoEmailBean().getEnderecoSelecionado();
				setDsLogradouroPagador(enderecoSelecionado.getDsLogradouroPagador());
				setDsNumeroLogradouroPagador(enderecoSelecionado.getDsNumeroLogradouroPagador());
				setDsComplementoLogradouroPagador(enderecoSelecionado.getDsComplementoLogradouroPagador());
				setDsBairroClientePagador(enderecoSelecionado.getDsBairroClientePagador());
				setDsMunicipioClientePagador(enderecoSelecionado.getDsMunicipioClientePagador());
				setCdSiglaUfPagador(enderecoSelecionado.getCdSiglaUfPagador().toUpperCase());
				setCepFormatado(PgitUtil.complementaDigito(String.valueOf(enderecoSelecionado.getCdCepPagador()), 5) + "-"
						+ PgitUtil.complementaDigito(String.valueOf(enderecoSelecionado.getCdCepComplementoPagador()), 3));
				setCdCepPagador(enderecoSelecionado.getCdCepPagador());
			    setCdCepComplementoPagador(enderecoSelecionado.getCdCepComplementoPagador());
			}else{
				if(getRadioTipoPostagem().equals("1")){
					ConsultarEmailSaidaDTO emailSelecionado = getFiltroEnderecoEmailBean().getEmailSelecionado();
					setDsEmailClientePagador(emailSelecionado.getCdEnderecoEletronico());
				}
			}
		}else{
			if(getRadioInfoAdicionais().equals("1")){
				if(getRadioAgenciaDepto().equals("0")){
					setDsAgenciaOperadora(getAgenciaOperadora());
				}else{
					if(getRadioAgenciaDepto().equals("1")){
						setDepartamentoDestinoEntrega(getDepartamentoFiltro());
					}
				}
			}
		}

		setPercentualDescTarifa(percentualDescTarifa == null ? new BigDecimal(0) : percentualDescTarifa);

		//Tarifa
		setVlTarifaAtualizada(PgitUtil.calcularValorTarifaAtualizada(getVlTarifa(), getPercentualDescTarifa()));
		return "CONFIRMARINCLUSAO";
	}
	
	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar(){
		return preencheDadosSolEmiComprovanteClientePagador("DETALHAR");
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir(){
		return preencheDadosSolEmiComprovanteClientePagador("EXCLUIR");
	}
	
	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir(){
		limparIncluir();
		carregaCabecalho();
		listarTipoServicoSemPagto();
		listarCmbTipoFavorecido();
		setDisableArgumentosIncluir(false);
		setHabilitaCampoEndereco(true);
		return "INCLUIR";
	}
	
	/**
	 * Inf adicionais.
	 *
	 * @return the string
	 */
	public String infAdicionais(){
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(new PDCDataBean());		
		
		carregaListaComprovantesSelecionados();
		consultarAgenciaOpeTarifaPadrao();
		consultarAgenciaOperadora();
		setPercentualDescTarifa(null);
		limparInfoAdicionais();
		setRadioInfoAdicionais("");
		limparTipoPostagem();
		if("0.00".equals(getVlTarifa())){
			setDesabilitaDescontoTarifa(true);
		}
		return "INFADICIONAIS";
	}
	
	/**
	 * Limpar selecionados.
	 */
	public void limparSelecionados(){
		for(int i=0;i<getListaIncluirComprovantes().size();i++){
			getListaIncluirComprovantes().get(i).setCheck(false);
		}
		setPanelBotoes(false);
		setOpcaoChecarTodos(false);
	}
	
	
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		setItemSelecionadoLista(null);
		
		return "VOLTAR";
	}
	
	/**
	 * Voltar incluir.
	 *
	 * @return the string
	 */
	public String voltarIncluir(){
		limparSelecionados();
		
		return "VOLTAR";
	}
	
	/**
	 * Voltar inf.
	 *
	 * @return the string
	 */
	public String voltarInf(){
		return "VOLTAR";
	}

    /** Getters e Setters**/
	public Integer getCboSituacaoSolicitacaoEstornoFiltro() {
		return cboSituacaoSolicitacaoEstornoFiltro;
	}
	
	/**
	 * Set: cboSituacaoSolicitacaoEstornoFiltro.
	 *
	 * @param cboSituacaoSolicitacaoEstornoFiltro the cbo situacao solicitacao estorno filtro
	 */
	public void setCboSituacaoSolicitacaoEstornoFiltro(
			Integer cboSituacaoSolicitacaoEstornoFiltro) {
		this.cboSituacaoSolicitacaoEstornoFiltro = cboSituacaoSolicitacaoEstornoFiltro;
	}
	
	/**
	 * Get: cdAgenciaBancariaContaCredito.
	 *
	 * @return cdAgenciaBancariaContaCredito
	 */
	public Integer getCdAgenciaBancariaContaCredito() {
		return cdAgenciaBancariaContaCredito;
	}
	
	/**
	 * Set: cdAgenciaBancariaContaCredito.
	 *
	 * @param cdAgenciaBancariaContaCredito the cd agencia bancaria conta credito
	 */
	public void setCdAgenciaBancariaContaCredito(
			Integer cdAgenciaBancariaContaCredito) {
		this.cdAgenciaBancariaContaCredito = cdAgenciaBancariaContaCredito;
	}
	
	/**
	 * Get: cdAgenciaBancariaContaDebito.
	 *
	 * @return cdAgenciaBancariaContaDebito
	 */
	public Integer getCdAgenciaBancariaContaDebito() {
		return cdAgenciaBancariaContaDebito;
	}
	
	/**
	 * Set: cdAgenciaBancariaContaDebito.
	 *
	 * @param cdAgenciaBancariaContaDebito the cd agencia bancaria conta debito
	 */
	public void setCdAgenciaBancariaContaDebito(Integer cdAgenciaBancariaContaDebito) {
		this.cdAgenciaBancariaContaDebito = cdAgenciaBancariaContaDebito;
	}
	
	/**
	 * Get: cdAgenciaOperadora.
	 *
	 * @return cdAgenciaOperadora
	 */
	public Integer getCdAgenciaOperadora() {
		return cdAgenciaOperadora;
	}
	
	/**
	 * Set: cdAgenciaOperadora.
	 *
	 * @param cdAgenciaOperadora the cd agencia operadora
	 */
	public void setCdAgenciaOperadora(Integer cdAgenciaOperadora) {
		this.cdAgenciaOperadora = cdAgenciaOperadora;
	}
	
	/**
	 * Get: cdBancoContaCredito.
	 *
	 * @return cdBancoContaCredito
	 */
	public Integer getCdBancoContaCredito() {
		return cdBancoContaCredito;
	}
	
	/**
	 * Set: cdBancoContaCredito.
	 *
	 * @param cdBancoContaCredito the cd banco conta credito
	 */
	public void setCdBancoContaCredito(Integer cdBancoContaCredito) {
		this.cdBancoContaCredito = cdBancoContaCredito;
	}
	
	/**
	 * Get: cdBancoContaDebito.
	 *
	 * @return cdBancoContaDebito
	 */
	public Integer getCdBancoContaDebito() {
		return cdBancoContaDebito;
	}
	
	/**
	 * Set: cdBancoContaDebito.
	 *
	 * @param cdBancoContaDebito the cd banco conta debito
	 */
	public void setCdBancoContaDebito(Integer cdBancoContaDebito) {
		this.cdBancoContaDebito = cdBancoContaDebito;
	}
	
	/**
	 * Get: cdCepComplementoPagador.
	 *
	 * @return cdCepComplementoPagador
	 */
	public Integer getCdCepComplementoPagador() {
		return cdCepComplementoPagador;
	}
	
	/**
	 * Set: cdCepComplementoPagador.
	 *
	 * @param cdCepComplementoPagador the cd cep complemento pagador
	 */
	public void setCdCepComplementoPagador(Integer cdCepComplementoPagador) {
		this.cdCepComplementoPagador = cdCepComplementoPagador;
	}
	
	/**
	 * Get: cdCepPagador.
	 *
	 * @return cdCepPagador
	 */
	public Integer getCdCepPagador() {
		return cdCepPagador;
	}
	
	/**
	 * Set: cdCepPagador.
	 *
	 * @param cdCepPagador the cd cep pagador
	 */
	public void setCdCepPagador(Integer cdCepPagador) {
		this.cdCepPagador = cdCepPagador;
	}
	
	/**
	 * Get: cdContaBancariaContaCredito.
	 *
	 * @return cdContaBancariaContaCredito
	 */
	public Long getCdContaBancariaContaCredito() {
		return cdContaBancariaContaCredito;
	}
	
	/**
	 * Set: cdContaBancariaContaCredito.
	 *
	 * @param cdContaBancariaContaCredito the cd conta bancaria conta credito
	 */
	public void setCdContaBancariaContaCredito(Long cdContaBancariaContaCredito) {
		this.cdContaBancariaContaCredito = cdContaBancariaContaCredito;
	}
	
	/**
	 * Get: cdContaBancariaContaDebito.
	 *
	 * @return cdContaBancariaContaDebito
	 */
	public Long getCdContaBancariaContaDebito() {
		return cdContaBancariaContaDebito;
	}
	
	/**
	 * Set: cdContaBancariaContaDebito.
	 *
	 * @param cdContaBancariaContaDebito the cd conta bancaria conta debito
	 */
	public void setCdContaBancariaContaDebito(Long cdContaBancariaContaDebito) {
		this.cdContaBancariaContaDebito = cdContaBancariaContaDebito;
	}
	
	/**
	 * Get: cdControleCpfRecebedor.
	 *
	 * @return cdControleCpfRecebedor
	 */
	public Integer getCdControleCpfRecebedor() {
		return cdControleCpfRecebedor;
	}
	
	/**
	 * Set: cdControleCpfRecebedor.
	 *
	 * @param cdControleCpfRecebedor the cd controle cpf recebedor
	 */
	public void setCdControleCpfRecebedor(Integer cdControleCpfRecebedor) {
		this.cdControleCpfRecebedor = cdControleCpfRecebedor;
	}
	
	/**
	 * Get: cdCpfCnpjRecebedor.
	 *
	 * @return cdCpfCnpjRecebedor
	 */
	public Long getCdCpfCnpjRecebedor() {
		return cdCpfCnpjRecebedor;
	}
	
	/**
	 * Set: cdCpfCnpjRecebedor.
	 *
	 * @param cdCpfCnpjRecebedor the cd cpf cnpj recebedor
	 */
	public void setCdCpfCnpjRecebedor(Long cdCpfCnpjRecebedor) {
		this.cdCpfCnpjRecebedor = cdCpfCnpjRecebedor;
	}
	
	/**
	 * Get: cdDestinoCorrespSolicitacao.
	 *
	 * @return cdDestinoCorrespSolicitacao
	 */
	public Integer getCdDestinoCorrespSolicitacao() {
		return cdDestinoCorrespSolicitacao;
	}
	
	/**
	 * Set: cdDestinoCorrespSolicitacao.
	 *
	 * @param cdDestinoCorrespSolicitacao the cd destino corresp solicitacao
	 */
	public void setCdDestinoCorrespSolicitacao(Integer cdDestinoCorrespSolicitacao) {
		this.cdDestinoCorrespSolicitacao = cdDestinoCorrespSolicitacao;
	}
	
	/**
	 * Get: cdDigitoContaContaDebito.
	 *
	 * @return cdDigitoContaContaDebito
	 */
	public String getCdDigitoContaContaDebito() {
		return cdDigitoContaContaDebito;
	}
	
	/**
	 * Set: cdDigitoContaContaDebito.
	 *
	 * @param cdDigitoContaContaDebito the cd digito conta conta debito
	 */
	public void setCdDigitoContaContaDebito(String cdDigitoContaContaDebito) {
		this.cdDigitoContaContaDebito = cdDigitoContaContaDebito;
	}
	
	/**
	 * Get: cdDigitoContaCredito.
	 *
	 * @return cdDigitoContaCredito
	 */
	public String getCdDigitoContaCredito() {
		return cdDigitoContaCredito;
	}
	
	/**
	 * Set: cdDigitoContaCredito.
	 *
	 * @param cdDigitoContaCredito the cd digito conta credito
	 */
	public void setCdDigitoContaCredito(String cdDigitoContaCredito) {
		this.cdDigitoContaCredito = cdDigitoContaCredito;
	}
	
	/**
	 * Get: cdFilialCnpjRecebedor.
	 *
	 * @return cdFilialCnpjRecebedor
	 */
	public Integer getCdFilialCnpjRecebedor() {
		return cdFilialCnpjRecebedor;
	}
	
	/**
	 * Set: cdFilialCnpjRecebedor.
	 *
	 * @param cdFilialCnpjRecebedor the cd filial cnpj recebedor
	 */
	public void setCdFilialCnpjRecebedor(Integer cdFilialCnpjRecebedor) {
		this.cdFilialCnpjRecebedor = cdFilialCnpjRecebedor;
	}
	
	/**
	 * Get: cdOperacaoCanalInclusao.
	 *
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}
	
	/**
	 * Set: cdOperacaoCanalInclusao.
	 *
	 * @param cdOperacaoCanalInclusao the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}
	
	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 *
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 *
	 * @param cdProdutoOperacaoRelacionado the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: cdProdutoServicoOperacao.
	 *
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}
	
	/**
	 * Set: cdProdutoServicoOperacao.
	 *
	 * @param cdProdutoServicoOperacao the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}
	
	/**
	 * Get: cdSiglaUfPagador.
	 *
	 * @return cdSiglaUfPagador
	 */
	public String getCdSiglaUfPagador() {
		return cdSiglaUfPagador;
	}
	
	/**
	 * Set: cdSiglaUfPagador.
	 *
	 * @param cdSiglaUfPagador the cd sigla uf pagador
	 */
	public void setCdSiglaUfPagador(String cdSiglaUfPagador) {
		this.cdSiglaUfPagador = cdSiglaUfPagador;
	}
	
	/**
	 * Get: cdSituacaoContrato.
	 *
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}
	
	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}
	
	/**
	 * Get: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @return cdSolicitacaoPagamentoIntegrado
	 */
	public Integer getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 *
	 * @param cdSolicitacaoPagamentoIntegrado the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(
			Integer cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: cdTipoInscricaoRecebedor.
	 *
	 * @return cdTipoInscricaoRecebedor
	 */
	public Integer getCdTipoInscricaoRecebedor() {
		return cdTipoInscricaoRecebedor;
	}
	
	/**
	 * Set: cdTipoInscricaoRecebedor.
	 *
	 * @param cdTipoInscricaoRecebedor the cd tipo inscricao recebedor
	 */
	public void setCdTipoInscricaoRecebedor(Integer cdTipoInscricaoRecebedor) {
		this.cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
	}
	
	/**
	 * Get: cdTipoPostagemSolicitacao.
	 *
	 * @return cdTipoPostagemSolicitacao
	 */
	public String getCdTipoPostagemSolicitacao() {
		return cdTipoPostagemSolicitacao;
	}
	
	/**
	 * Set: cdTipoPostagemSolicitacao.
	 *
	 * @param cdTipoPostagemSolicitacao the cd tipo postagem solicitacao
	 */
	public void setCdTipoPostagemSolicitacao(String cdTipoPostagemSolicitacao) {
		this.cdTipoPostagemSolicitacao = cdTipoPostagemSolicitacao;
	}
	
	/**
	 * Get: cdUsuarioInclusao.
	 *
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}
	
	/**
	 * Set: cdUsuarioInclusao.
	 *
	 * @param cdUsuarioInclusao the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}
	
	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}
	
	/**
	 * Get: dsBairroClientePagador.
	 *
	 * @return dsBairroClientePagador
	 */
	public String getDsBairroClientePagador() {
		return dsBairroClientePagador;
	}
	
	/**
	 * Set: dsBairroClientePagador.
	 *
	 * @param dsBairroClientePagador the ds bairro cliente pagador
	 */
	public void setDsBairroClientePagador(String dsBairroClientePagador) {
		this.dsBairroClientePagador = dsBairroClientePagador;
	}
	
	/**
	 * Get: dsComplementoLogradouroPagador.
	 *
	 * @return dsComplementoLogradouroPagador
	 */
	public String getDsComplementoLogradouroPagador() {
		return dsComplementoLogradouroPagador;
	}
	
	/**
	 * Set: dsComplementoLogradouroPagador.
	 *
	 * @param dsComplementoLogradouroPagador the ds complemento logradouro pagador
	 */
	public void setDsComplementoLogradouroPagador(
			String dsComplementoLogradouroPagador) {
		this.dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
	}
	
	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}
	
	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}
	
	/**
	 * Get: dsEmailClientePagador.
	 *
	 * @return dsEmailClientePagador
	 */
	public String getDsEmailClientePagador() {
		return dsEmailClientePagador;
	}
	
	/**
	 * Set: dsEmailClientePagador.
	 *
	 * @param dsEmailClientePagador the ds email cliente pagador
	 */
	public void setDsEmailClientePagador(String dsEmailClientePagador) {
		this.dsEmailClientePagador = dsEmailClientePagador;
	}
	
	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}
	
	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}
	
	/**
	 * Get: dsLogradouroPagador.
	 *
	 * @return dsLogradouroPagador
	 */
	public String getDsLogradouroPagador() {
		return dsLogradouroPagador;
	}
	
	/**
	 * Set: dsLogradouroPagador.
	 *
	 * @param dsLogradouroPagador the ds logradouro pagador
	 */
	public void setDsLogradouroPagador(String dsLogradouroPagador) {
		this.dsLogradouroPagador = dsLogradouroPagador;
	}
	
	/**
	 * Get: dsMunicipioClientePagador.
	 *
	 * @return dsMunicipioClientePagador
	 */
	public String getDsMunicipioClientePagador() {
		return dsMunicipioClientePagador;
	}
	
	/**
	 * Set: dsMunicipioClientePagador.
	 *
	 * @param dsMunicipioClientePagador the ds municipio cliente pagador
	 */
	public void setDsMunicipioClientePagador(String dsMunicipioClientePagador) {
		this.dsMunicipioClientePagador = dsMunicipioClientePagador;
	}
	
	/**
	 * Get: dsNumeroLogradouroPagador.
	 *
	 * @return dsNumeroLogradouroPagador
	 */
	public String getDsNumeroLogradouroPagador() {
		return dsNumeroLogradouroPagador;
	}
	
	/**
	 * Set: dsNumeroLogradouroPagador.
	 *
	 * @param dsNumeroLogradouroPagador the ds numero logradouro pagador
	 */
	public void setDsNumeroLogradouroPagador(String dsNumeroLogradouroPagador) {
		this.dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
	}
	
	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 *
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 *
	 * @param dsProdutoOperacaoRelacionado the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}
	
	/**
	 * Get: dsProdutoServicoOperacao.
	 *
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}
	
	/**
	 * Set: dsProdutoServicoOperacao.
	 *
	 * @param dsProdutoServicoOperacao the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}
	
	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}
	
	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}
	
	/**
	 * Get: dsTipoCanalInclusao.
	 *
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}
	
	/**
	 * Set: dsTipoCanalInclusao.
	 *
	 * @param dsTipoCanalInclusao the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}
	
	/**
	 * Get: dtFimPeriodoMovimentacao.
	 *
	 * @return dtFimPeriodoMovimentacao
	 */
	public String getDtFimPeriodoMovimentacao() {
		return dtFimPeriodoMovimentacao;
	}
	
	/**
	 * Set: dtFimPeriodoMovimentacao.
	 *
	 * @param dtFimPeriodoMovimentacao the dt fim periodo movimentacao
	 */
	public void setDtFimPeriodoMovimentacao(String dtFimPeriodoMovimentacao) {
		this.dtFimPeriodoMovimentacao = dtFimPeriodoMovimentacao;
	}
	
	/**
	 * Get: dtFimSolicitacao.
	 *
	 * @return dtFimSolicitacao
	 */
	public Date getDtFimSolicitacao() {
		return dtFimSolicitacao;
	}
	
	/**
	 * Set: dtFimSolicitacao.
	 *
	 * @param dtFimSolicitacao the dt fim solicitacao
	 */
	public void setDtFimSolicitacao(Date dtFimSolicitacao) {
		this.dtFimSolicitacao = dtFimSolicitacao;
	}
	
	/**
	 * Get: dtInicioPeriodoMovimentacao.
	 *
	 * @return dtInicioPeriodoMovimentacao
	 */
	public String getDtInicioPeriodoMovimentacao() {
		return dtInicioPeriodoMovimentacao;
	}
	
	/**
	 * Set: dtInicioPeriodoMovimentacao.
	 *
	 * @param dtInicioPeriodoMovimentacao the dt inicio periodo movimentacao
	 */
	public void setDtInicioPeriodoMovimentacao(String dtInicioPeriodoMovimentacao) {
		this.dtInicioPeriodoMovimentacao = dtInicioPeriodoMovimentacao;
	}
	
	/**
	 * Get: dtInicioSolicitacao.
	 *
	 * @return dtInicioSolicitacao
	 */
	public Date getDtInicioSolicitacao() {
		return dtInicioSolicitacao;
	}
	
	/**
	 * Set: dtInicioSolicitacao.
	 *
	 * @param dtInicioSolicitacao the dt inicio solicitacao
	 */
	public void setDtInicioSolicitacao(Date dtInicioSolicitacao) {
		this.dtInicioSolicitacao = dtInicioSolicitacao;
	}
	
	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}
	
	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 *
	 * @param filtroAgendamentoEfetivacaoEstornoBean the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}
	
	/**
	 * Is habilita argumentos pesquisa.
	 *
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}
	
	/**
	 * Set: habilitaArgumentosPesquisa.
	 *
	 * @param habilitaArgumentosPesquisa the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}
	
	/**
	 * Get: hrInclusaoRegistro.
	 *
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}
	
	/**
	 * Set: hrInclusaoRegistro.
	 *
	 * @param hrInclusaoRegistro the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}
	
	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}
	
	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}
	
	/**
	 * Get: listaGridSolEmiComprovanteCliePagador.
	 *
	 * @return listaGridSolEmiComprovanteCliePagador
	 */
	public List<ConsultarSolEmiComprovanteCliePagadorSaidaDTO> getListaGridSolEmiComprovanteCliePagador() {
		return listaGridSolEmiComprovanteCliePagador;
	}
	
	/**
	 * Set: listaGridSolEmiComprovanteCliePagador.
	 *
	 * @param listaGridSolEmiComprovanteCliePagador the lista grid sol emi comprovante clie pagador
	 */
	public void setListaGridSolEmiComprovanteCliePagador(
			List<ConsultarSolEmiComprovanteCliePagadorSaidaDTO> listaGridSolEmiComprovanteCliePagador) {
		this.listaGridSolEmiComprovanteCliePagador = listaGridSolEmiComprovanteCliePagador;
	}
	
	/**
	 * Get: listaRadios.
	 *
	 * @return listaRadios
	 */
	public List<SelectItem> getListaRadios() {
		return listaRadios;
	}
	
	/**
	 * Set: listaRadios.
	 *
	 * @param listaRadios the lista radios
	 */
	public void setListaRadios(List<SelectItem> listaRadios) {
		this.listaRadios = listaRadios;
	}
	
	/**
	 * Get: listaSituacaoSolicitacaoEstornoFiltro.
	 *
	 * @return listaSituacaoSolicitacaoEstornoFiltro
	 */
	public List<SelectItem> getListaSituacaoSolicitacaoEstornoFiltro() {
		return listaSituacaoSolicitacaoEstornoFiltro;
	}
	
	/**
	 * Set: listaSituacaoSolicitacaoEstornoFiltro.
	 *
	 * @param listaSituacaoSolicitacaoEstornoFiltro the lista situacao solicitacao estorno filtro
	 */
	public void setListaSituacaoSolicitacaoEstornoFiltro(
			List<SelectItem> listaSituacaoSolicitacaoEstornoFiltro) {
		this.listaSituacaoSolicitacaoEstornoFiltro = listaSituacaoSolicitacaoEstornoFiltro;
	}
	
	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}
	
	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}
	
	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}
	
	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}
	
	/**
	 * Get: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @return nrSolicitacaoPagamentoIntegrado
	 */
	public Integer getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Set: nrSolicitacaoPagamentoIntegrado.
	 *
	 * @param nrSolicitacaoPagamentoIntegrado the nr solicitacao pagamento integrado
	 */
	public void setNrSolicitacaoPagamentoIntegrado(
			Integer nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}
	
	/**
	 * Get: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @return solEmissaoComprovantePagtoClientePagServiceImpl
	 */
	public ISolEmissaoComprovantePagtoClientePagService getSolEmissaoComprovantePagtoClientePagServiceImpl() {
		return solEmissaoComprovantePagtoClientePagServiceImpl;
	}
	
	/**
	 * Set: solEmissaoComprovantePagtoClientePagServiceImpl.
	 *
	 * @param solEmissaoComprovantePagtoClientePagServiceImpl the sol emissao comprovante pagto cliente pag service impl
	 */
	public void setSolEmissaoComprovantePagtoClientePagServiceImpl(
			ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl) {
		this.solEmissaoComprovantePagtoClientePagServiceImpl = solEmissaoComprovantePagtoClientePagServiceImpl;
	}
	
	/**
	 * Get: vlTarifa.
	 *
	 * @return vlTarifa
	 */
	public BigDecimal getVlTarifa() {
		return vlTarifa;
	}
	
	/**
	 * Set: vlTarifa.
	 *
	 * @param vlTarifa the vl tarifa
	 */
	public void setVlTarifa(BigDecimal vlTarifa) {
		this.vlTarifa = vlTarifa;
	}

	/**
	 * Get: agenciaOperadora.
	 *
	 * @return agenciaOperadora
	 */
	public String getAgenciaOperadora() {
		return agenciaOperadora;
	}

	/**
	 * Set: agenciaOperadora.
	 *
	 * @param agenciaOperadora the agencia operadora
	 */
	public void setAgenciaOperadora(String agenciaOperadora) {
		this.agenciaOperadora = agenciaOperadora;
	}

	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public Long getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdPessoaJuridicaDepartamento.
	 *
	 * @return cdPessoaJuridicaDepartamento
	 */
	public Long getCdPessoaJuridicaDepartamento() {
		return cdPessoaJuridicaDepartamento;
	}

	/**
	 * Set: cdPessoaJuridicaDepartamento.
	 *
	 * @param cdPessoaJuridicaDepartamento the cd pessoa juridica departamento
	 */
	public void setCdPessoaJuridicaDepartamento(Long cdPessoaJuridicaDepartamento) {
		this.cdPessoaJuridicaDepartamento = cdPessoaJuridicaDepartamento;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 *
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 *
	 * @param cdTipoContratoNegocio the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: codAgenciaOperadora.
	 *
	 * @return codAgenciaOperadora
	 */
	public Integer getCodAgenciaOperadora() {
		return codAgenciaOperadora;
	}

	/**
	 * Set: codAgenciaOperadora.
	 *
	 * @param codAgenciaOperadora the cod agencia operadora
	 */
	public void setCodAgenciaOperadora(Integer codAgenciaOperadora) {
		this.codAgenciaOperadora = codAgenciaOperadora;
	}

	/**
	 * Get: comprovantes.
	 *
	 * @return comprovantes
	 */
	public List<DetalharOcorrenciasDTO> getComprovantes() {
		return comprovantes;
	}

	/**
	 * Set: comprovantes.
	 *
	 * @param comprovantes the comprovantes
	 */
	public void setComprovantes(List<DetalharOcorrenciasDTO> comprovantes) {
		this.comprovantes = comprovantes;
	}

	/**
	 * Get: departamentoFiltro.
	 *
	 * @return departamentoFiltro
	 */
	public String getDepartamentoFiltro() {
		return departamentoFiltro;
	}

	/**
	 * Set: departamentoFiltro.
	 *
	 * @param departamentoFiltro the departamento filtro
	 */
	public void setDepartamentoFiltro(String departamentoFiltro) {
		this.departamentoFiltro = departamentoFiltro;
	}

	/**
	 * Get: dtFimPeriodoPagamento.
	 *
	 * @return dtFimPeriodoPagamento
	 */
	public Date getDtFimPeriodoPagamento() {
		return dtFimPeriodoPagamento;
	}

	/**
	 * Set: dtFimPeriodoPagamento.
	 *
	 * @param dtFimPeriodoPagamento the dt fim periodo pagamento
	 */
	public void setDtFimPeriodoPagamento(Date dtFimPeriodoPagamento) {
		this.dtFimPeriodoPagamento = dtFimPeriodoPagamento;
	}

	/**
	 * Get: dtInicioPeriodoPagamento.
	 *
	 * @return dtInicioPeriodoPagamento
	 */
	public Date getDtInicioPeriodoPagamento() {
		return dtInicioPeriodoPagamento;
	}

	/**
	 * Set: dtInicioPeriodoPagamento.
	 *
	 * @param dtInicioPeriodoPagamento the dt inicio periodo pagamento
	 */
	public void setDtInicioPeriodoPagamento(Date dtInicioPeriodoPagamento) {
		this.dtInicioPeriodoPagamento = dtInicioPeriodoPagamento;
	}

	/**
	 * Get: inscricaoFavorecido.
	 *
	 * @return inscricaoFavorecido
	 */
	public String getInscricaoFavorecido() {
		return inscricaoFavorecido;
	}

	/**
	 * Set: inscricaoFavorecido.
	 *
	 * @param inscricaoFavorecido the inscricao favorecido
	 */
	public void setInscricaoFavorecido(String inscricaoFavorecido) {
		this.inscricaoFavorecido = inscricaoFavorecido;
	}

	/**
	 * Get: listaIncluirComprovantes.
	 *
	 * @return listaIncluirComprovantes
	 */
	public List<ListarIncluirSolEmiComprovanteSaidaDTO> getListaIncluirComprovantes() {
		return listaIncluirComprovantes;
	}

	/**
	 * Set: listaIncluirComprovantes.
	 *
	 * @param listaIncluirComprovantes the lista incluir comprovantes
	 */
	public void setListaIncluirComprovantes(
			List<ListarIncluirSolEmiComprovanteSaidaDTO> listaIncluirComprovantes) {
		this.listaIncluirComprovantes = listaIncluirComprovantes;
	}

	/**
	 * Get: listaIncluirComprovantesSelecionados.
	 *
	 * @return listaIncluirComprovantesSelecionados
	 */
	public List<ListarIncluirSolEmiComprovanteSaidaDTO> getListaIncluirComprovantesSelecionados() {
		return listaIncluirComprovantesSelecionados;
	}

	/**
	 * Set: listaIncluirComprovantesSelecionados.
	 *
	 * @param listaIncluirComprovantesSelecionados the lista incluir comprovantes selecionados
	 */
	public void setListaIncluirComprovantesSelecionados(
			List<ListarIncluirSolEmiComprovanteSaidaDTO> listaIncluirComprovantesSelecionados) {
		this.listaIncluirComprovantesSelecionados = listaIncluirComprovantesSelecionados;
	}

	/**
	 * Get: listaModalidadeFiltro.
	 *
	 * @return listaModalidadeFiltro
	 */
	public List<SelectItem> getListaModalidadeFiltro() {
		return listaModalidadeFiltro;
	}

	/**
	 * Set: listaModalidadeFiltro.
	 *
	 * @param listaModalidadeFiltro the lista modalidade filtro
	 */
	public void setListaModalidadeFiltro(List<SelectItem> listaModalidadeFiltro) {
		this.listaModalidadeFiltro = listaModalidadeFiltro;
	}

	/**
	 * Get: listaTipoInscricaoFiltro.
	 *
	 * @return listaTipoInscricaoFiltro
	 */
	public List<SelectItem> getListaTipoInscricaoFiltro() {
		return listaTipoInscricaoFiltro;
	}

	/**
	 * Set: listaTipoInscricaoFiltro.
	 *
	 * @param listaTipoInscricaoFiltro the lista tipo inscricao filtro
	 */
	public void setListaTipoInscricaoFiltro(
			List<SelectItem> listaTipoInscricaoFiltro) {
		this.listaTipoInscricaoFiltro = listaTipoInscricaoFiltro;
	}

	/**
	 * Get: listaTipoServicoFiltro.
	 *
	 * @return listaTipoServicoFiltro
	 */
	public List<SelectItem> getListaTipoServicoFiltro() {
		return listaTipoServicoFiltro;
	}

	/**
	 * Set: listaTipoServicoFiltro.
	 *
	 * @param listaTipoServicoFiltro the lista tipo servico filtro
	 */
	public void setListaTipoServicoFiltro(List<SelectItem> listaTipoServicoFiltro) {
		this.listaTipoServicoFiltro = listaTipoServicoFiltro;
	}

	/**
	 * Get: modalidadeFiltro.
	 *
	 * @return modalidadeFiltro
	 */
	public Integer getModalidadeFiltro() {
		return modalidadeFiltro;
	}

	/**
	 * Set: modalidadeFiltro.
	 *
	 * @param modalidadeFiltro the modalidade filtro
	 */
	public void setModalidadeFiltro(Integer modalidadeFiltro) {
		this.modalidadeFiltro = modalidadeFiltro;
	}

	/**
	 * Get: nrSequenciaDepartamento.
	 *
	 * @return nrSequenciaDepartamento
	 */
	public Integer getNrSequenciaDepartamento() {
		return nrSequenciaDepartamento;
	}

	/**
	 * Set: nrSequenciaDepartamento.
	 *
	 * @param nrSequenciaDepartamento the nr sequencia departamento
	 */
	public void setNrSequenciaDepartamento(Integer nrSequenciaDepartamento) {
		this.nrSequenciaDepartamento = nrSequenciaDepartamento;
	}

	/**
	 * Get: numeroPagamentoAte.
	 *
	 * @return numeroPagamentoAte
	 */
	public String getNumeroPagamentoAte() {
		return numeroPagamentoAte;
	}

	/**
	 * Set: numeroPagamentoAte.
	 *
	 * @param numeroPagamentoAte the numero pagamento ate
	 */
	public void setNumeroPagamentoAte(String numeroPagamentoAte) {
		this.numeroPagamentoAte = numeroPagamentoAte;
	}

	/**
	 * Get: numeroPagamentoDe.
	 *
	 * @return numeroPagamentoDe
	 */
	public String getNumeroPagamentoDe() {
		return numeroPagamentoDe;
	}

	/**
	 * Set: numeroPagamentoDe.
	 *
	 * @param numeroPagamentoDe the numero pagamento de
	 */
	public void setNumeroPagamentoDe(String numeroPagamentoDe) {
		this.numeroPagamentoDe = numeroPagamentoDe;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Get: radioAgenciaDepto.
	 *
	 * @return radioAgenciaDepto
	 */
	public String getRadioAgenciaDepto() {
		return radioAgenciaDepto;
	}

	/**
	 * Set: radioAgenciaDepto.
	 *
	 * @param radioAgenciaDepto the radio agencia depto
	 */
	public void setRadioAgenciaDepto(String radioAgenciaDepto) {
		this.radioAgenciaDepto = radioAgenciaDepto;
	}

	/**
	 * Get: radioIncluirFavorecido.
	 *
	 * @return radioIncluirFavorecido
	 */
	public String getRadioIncluirFavorecido() {
		return radioIncluirFavorecido;
	}

	/**
	 * Set: radioIncluirFavorecido.
	 *
	 * @param radioIncluirFavorecido the radio incluir favorecido
	 */
	public void setRadioIncluirFavorecido(String radioIncluirFavorecido) {
		this.radioIncluirFavorecido = radioIncluirFavorecido;
	}

	/**
	 * Get: radioIncluirSelecionado.
	 *
	 * @return radioIncluirSelecionado
	 */
	public String getRadioIncluirSelecionado() {
		return radioIncluirSelecionado;
	}

	/**
	 * Set: radioIncluirSelecionado.
	 *
	 * @param radioIncluirSelecionado the radio incluir selecionado
	 */
	public void setRadioIncluirSelecionado(String radioIncluirSelecionado) {
		this.radioIncluirSelecionado = radioIncluirSelecionado;
	}

	/**
	 * Get: radioInfoAdicionais.
	 *
	 * @return radioInfoAdicionais
	 */
	public String getRadioInfoAdicionais() {
		return radioInfoAdicionais;
	}

	/**
	 * Set: radioInfoAdicionais.
	 *
	 * @param radioInfoAdicionais the radio info adicionais
	 */
	public void setRadioInfoAdicionais(String radioInfoAdicionais) {
		this.radioInfoAdicionais = radioInfoAdicionais;
	}

	/**
	 * Get: radioTipoPostagem.
	 *
	 * @return radioTipoPostagem
	 */
	public String getRadioTipoPostagem() {
		return radioTipoPostagem;
	}

	/**
	 * Set: radioTipoPostagem.
	 *
	 * @param radioTipoPostagem the radio tipo postagem
	 */
	public void setRadioTipoPostagem(String radioTipoPostagem) {
		this.radioTipoPostagem = radioTipoPostagem;
	}

	/**
	 * Get: tipoInscricaoFiltro.
	 *
	 * @return tipoInscricaoFiltro
	 */
	public Integer getTipoInscricaoFiltro() {
		return tipoInscricaoFiltro;
	}

	/**
	 * Set: tipoInscricaoFiltro.
	 *
	 * @param tipoInscricaoFiltro the tipo inscricao filtro
	 */
	public void setTipoInscricaoFiltro(Integer tipoInscricaoFiltro) {
		this.tipoInscricaoFiltro = tipoInscricaoFiltro;
	}

	/**
	 * Get: tipoServicoFiltro.
	 *
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 *
	 * @param tipoServicoFiltro the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Is panel botoes.
	 *
	 * @return true, if is panel botoes
	 */
	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	/**
	 * Set: panelBotoes.
	 *
	 * @param panelBotoes the panel botoes
	 */
	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	/**
	 * Get: vlTarifaAtualizada.
	 *
	 * @return vlTarifaAtualizada
	 */
	public BigDecimal getVlTarifaAtualizada() {
		return vlTarifaAtualizada;
	}

	/**
	 * Set: vlTarifaAtualizada.
	 *
	 * @param vlTarifaAtualizada the vl tarifa atualizada
	 */
	public void setVlTarifaAtualizada(BigDecimal vlTarifaAtualizada) {
		this.vlTarifaAtualizada = vlTarifaAtualizada;
	}

	/**
	 * Get: valorPagamentoAte.
	 *
	 * @return valorPagamentoAte
	 */
	public BigDecimal getValorPagamentoAte() {
		return valorPagamentoAte;
	}

	/**
	 * Set: valorPagamentoAte.
	 *
	 * @param valorPagamentoAte the valor pagamento ate
	 */
	public void setValorPagamentoAte(BigDecimal valorPagamentoAte) {
		this.valorPagamentoAte = valorPagamentoAte;
	}

	/**
	 * Get: valorPagamentoDe.
	 *
	 * @return valorPagamentoDe
	 */
	public BigDecimal getValorPagamentoDe() {
		return valorPagamentoDe;
	}

	/**
	 * Set: valorPagamentoDe.
	 *
	 * @param valorPagamentoDe the valor pagamento de
	 */
	public void setValorPagamentoDe(BigDecimal valorPagamentoDe) {
		this.valorPagamentoDe = valorPagamentoDe;
	}

	/**
	 * Get: departamentoDestinoEntrega.
	 *
	 * @return departamentoDestinoEntrega
	 */
	public String getDepartamentoDestinoEntrega() {
		return departamentoDestinoEntrega;
	}

	/**
	 * Set: departamentoDestinoEntrega.
	 *
	 * @param departamentoDestinoEntrega the departamento destino entrega
	 */
	public void setDepartamentoDestinoEntrega(String departamentoDestinoEntrega) {
		this.departamentoDestinoEntrega = departamentoDestinoEntrega;
	}

	/**
	 * Get: dsAgenciaOperadora.
	 *
	 * @return dsAgenciaOperadora
	 */
	public String getDsAgenciaOperadora() {
		return dsAgenciaOperadora;
	}

	/**
	 * Set: dsAgenciaOperadora.
	 *
	 * @param dsAgenciaOperadora the ds agencia operadora
	 */
	public void setDsAgenciaOperadora(String dsAgenciaOperadora) {
		this.dsAgenciaOperadora = dsAgenciaOperadora;
	}

	/**
	 * Get: percentualDescTarifa.
	 *
	 * @return percentualDescTarifa
	 */
	public BigDecimal getPercentualDescTarifa() {
		return percentualDescTarifa;
	}

	/**
	 * Set: percentualDescTarifa.
	 *
	 * @param percentualDescTarifa the percentual desc tarifa
	 */
	public void setPercentualDescTarifa(BigDecimal percentualDescTarifa) {
		this.percentualDescTarifa = percentualDescTarifa;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: cepFormatado.
	 *
	 * @return cepFormatado
	 */
	public String getCepFormatado() {
		return cepFormatado;
	}

	/**
	 * Set: cepFormatado.
	 *
	 * @param cepFormatado the cep formatado
	 */
	public void setCepFormatado(String cepFormatado) {
		this.cepFormatado = cepFormatado;
	}
	
	/**
	 * Get: listaModalidadeHash.
	 *
	 * @return listaModalidadeHash
	 */
	public Map<Integer, String> getListaModalidadeHash() {
		return listaModalidadeHash;
	}

	/**
	 * Set lista modalidade hash.
	 *
	 * @param listaModalidadeHash the lista modalidade hash
	 */
	public void setListaModalidadeHash(Map<Integer, String> listaModalidadeHash) {
		this.listaModalidadeHash = listaModalidadeHash;
	}

	/**
	 * Get: listaTipoServicoHash.
	 *
	 * @return listaTipoServicoHash
	 */
	public Map<Integer, String> getListaTipoServicoHash() {
		return listaTipoServicoHash;
	}

	/**
	 * Set lista tipo servico hash.
	 *
	 * @param listaTipoServicoHash the lista tipo servico hash
	 */
	public void setListaTipoServicoHash(Map<Integer, String> listaTipoServicoHash) {
		this.listaTipoServicoHash = listaTipoServicoHash;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Is disable argumentos incluir.
	 *
	 * @return true, if is disable argumentos incluir
	 */
	public boolean isDisableArgumentosIncluir() {
		return disableArgumentosIncluir;
	}

	/**
	 * Set: disableArgumentosIncluir.
	 *
	 * @param disableArgumentosIncluir the disable argumentos incluir
	 */
	public void setDisableArgumentosIncluir(boolean disableArgumentosIncluir) {
		this.disableArgumentosIncluir = disableArgumentosIncluir;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasService the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
		this.consultasService = consultasService;
	}

	/**
	 * Is desabilita desconto tarifa.
	 *
	 * @return true, if is desabilita desconto tarifa
	 */
	public boolean isDesabilitaDescontoTarifa() {
		return desabilitaDescontoTarifa;
	}

	/**
	 * Set: desabilitaDescontoTarifa.
	 *
	 * @param desabilitaDescontoTarifa the desabilita desconto tarifa
	 */
	public void setDesabilitaDescontoTarifa(boolean desabilitaDescontoTarifa) {
		this.desabilitaDescontoTarifa = desabilitaDescontoTarifa;
	}

	/**
	 * Get: filtroEnderecoEmailBean.
	 *
	 * @return filtroEnderecoEmailBean
	 */
	public FiltroEnderecoEmailBean getFiltroEnderecoEmailBean() {
		return filtroEnderecoEmailBean;
	}

	/**
	 * Set: filtroEnderecoEmailBean.
	 *
	 * @param filtroEnderecoEmailBean the filtro endereco email bean
	 */
	public void setFiltroEnderecoEmailBean(
			FiltroEnderecoEmailBean filtroEnderecoEmailBean) {
		this.filtroEnderecoEmailBean = filtroEnderecoEmailBean;
	}

	/**
	 * Is habilita campo endereco.
	 *
	 * @return true, if is habilita campo endereco
	 */
	public boolean isHabilitaCampoEndereco() {
		return habilitaCampoEndereco;
	}

	/**
	 * Set: habilitaCampoEndereco.
	 *
	 * @param habilitaCampoEndereco the habilita campo endereco
	 */
	public void setHabilitaCampoEndereco(boolean habilitaCampoEndereco) {
		this.habilitaCampoEndereco = habilitaCampoEndereco;
	}	
	
}