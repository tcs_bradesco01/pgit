/*
 * Nome: br.com.bradesco.web.pgit.view.bean.filtropesquisa
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.filtropesquisa;

import static br.com.bradesco.web.pgit.utils.SiteUtil.isEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarMotivoSituacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.IFiltroAgendamentoEfetivacaoEstornoService;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ConsultarContratoClienteFiltroEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroagendamentoefetivacaoestorno.bean.ConsultarContratoClienteFiltroSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.IFiltroIdentificaoService;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaContratosPessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarClubesClientesEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarClubesClientesSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.IListarFuncBradescoService;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.manutencaocontrato.incluircontratocontingencia.IncluirContratoContingenciaBean;
import br.com.bradesco.web.pgit.view.bean.tratarquivosremessa.consolicitacaorecuperacaoremessa.SolicitacaoRecuperacaoRemessaBean;

/**
 * Nome: IdentificacaoClienteContratoBean
 * <p>
 * Prop�sito:
 * </p>
 * .
 * 
 * @author : todo!
 * @version :
 */
public class IdentificacaoClienteContratoBean {

	/** Atributo CONTRATO_PENDENTE. */
	private static final int CONTRATO_PENDENTE = 2;

	/** Atributo CONTRATO_ENCERRADO. */
	private static final int CONTRATO_ENCERRADO = 6;

	/** Atributo entradaConsultarListaClientePessoas. */
	private ConsultarListaClientePessoasEntradaDTO entradaConsultarListaClientePessoas;

	/** Atributo entradaListarClubesClientes. */
	private ListarClubesClientesEntradaDTO entradaListarClubesClientes;

	/** Atributo entradaConsultarListaClientePessoasManutencao. */
	private ConsultarListaClientePessoasEntradaDTO entradaConsultarListaClientePessoasManutencao;

	/** Atributo saidaConsultarListaClientePessoas. */
	private ConsultarListaClientePessoasSaidaDTO saidaConsultarListaClientePessoas;

	/** Atributo saidaListarClubesClientes. */
	private ListarClubesClientesSaidaDTO saidaListarClubesClientes;

	/** Atributo saidaConsultarListaClientePessoasManutencao. */
	private ConsultarListaClientePessoasSaidaDTO saidaConsultarListaClientePessoasManutencao;

	/** Atributo listaConsultarListaClientePessoas. */
	private List<ConsultarListaClientePessoasSaidaDTO> listaConsultarListaClientePessoas;

	/** Atributo listaListarClubesClientes. */
	private List<ListarClubesClientesSaidaDTO> listaListarClubesClientes;

	/** Atributo saidaContrato. */
	private ListarContratosPgitSaidaDTO saidaContrato;

	/** Atributo saidaConsultarContratoClienteFiltro. */
	private ConsultarContratoClienteFiltroSaidaDTO saidaConsultarContratoClienteFiltro;

	/** Atributo listaSaidaConsultarContratoClienteFiltro. */
	private List<ConsultarContratoClienteFiltroSaidaDTO> listaSaidaConsultarContratoClienteFiltro;

	/** Atributo listaConsultarListaContratosPessoas. */
	private List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoas;

	/** Atributo solicitacaoRecuperacaoRemessaBean. */
	private SolicitacaoRecuperacaoRemessaBean solicitacaoRecuperacaoRemessaBean = new SolicitacaoRecuperacaoRemessaBean();

	/** Atributo itemFiltroSelecionado. */
	private String itemFiltroSelecionado;

	/** Atributo itemClienteSelecionado. */
	private Integer itemClienteSelecionado;

	/** Atributo bloqueiaRadio. */
	private boolean bloqueiaRadio = false;

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo listaEmpresaGestora. */
	private List<SelectItem> listaEmpresaGestora = new ArrayList<SelectItem>();

	/** Atributo listaTipoContrato. */
	private List<SelectItem> listaTipoContrato = new ArrayList<SelectItem>();

	/** Atributo listaSituacaoContrato. */
	private List<SelectItem> listaSituacaoContrato = new ArrayList<SelectItem>();

	/** Atributo listaSituacaoContratoHash. */
	private Map<Integer, String> listaSituacaoContratoHash = new HashMap<Integer, String>();

	/** Atributo listaMotivoSituacao. */
	private List<SelectItem> listaMotivoSituacao = new ArrayList<SelectItem>();

	/** Atributo listaMotivoSituacaoHash. */
	private Map<Integer, String> listaMotivoSituacaoHash = new HashMap<Integer, String>();

	/** Atributo paginaRetorno. */
	private String paginaRetorno;

	/** Atributo paginaCliente. */
	private String paginaCliente;

	/** Atributo flagDuplicacao. */
	private String flagDuplicacao;

	/** Atributo empresaGestoraFiltro. */
	private Long empresaGestoraFiltro;

	/** Atributo tipoContratoFiltro. */
	private Integer tipoContratoFiltro;

	/** Atributo numeroFiltro. */
	private String numeroFiltro;

	/** Atributo agenciaOperadoraFiltro. */
	private String agenciaOperadoraFiltro;

	/** Atributo situacaoFiltro. */
	private Integer situacaoFiltro;

	/** Atributo funcionarioDTO. */
	private ListarFuncionarioSaidaDTO funcionarioDTO = new ListarFuncionarioSaidaDTO();

	/** Atributo codigoFunc. */
	private String codigoFunc;

	/** Atributo obrigatoriedade. */
	private String obrigatoriedade;

	/** Atributo dsNomeRazaoSocialParticipante. */
	private String dsNomeRazaoSocialParticipante;

	/** Atributo cpfCnpjParticipante. */
	private String cpfCnpjParticipante;

	/** Atributo empresaGestoraContratoCliente. */
	private String empresaGestoraContratoCliente;

	/** Atributo numeroContratoCliente. */
	private String numeroContratoCliente;

	/** Atributo descricaoContratoCliente. */
	private String descricaoContratoCliente;

	/** Atributo situacaoContratoCliente. */
	private String situacaoContratoCliente;

	/** Atributo empresaGestoraContrato. */
	private String empresaGestoraContrato;

	/** Atributo numeroContrato. */
	private String numeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo situacaoContrato. */
	private String situacaoContrato;

	/** Atributo cdPessoaJuridicaContrato. */
	private Long cdPessoaJuridicaContrato;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;

	/** Atributo nrSequenciaContratoNegocio. */
	private Long nrSequenciaContratoNegocio;

	/** Atributo habilitaFiltroDeCliente. */
	private boolean habilitaFiltroDeCliente;

	/** Atributo habilitaCampos. */
	private boolean habilitaCampos;

	/** Atributo habilitaEmpresaGestoraTipoContrato. */
	private boolean habilitaEmpresaGestoraTipoContrato;

	/** Atributo flagPesquisouPorIdentCliente. */
	private boolean flagPesquisouPorIdentCliente;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo listaGridPesquisa. */
	private List<ListarContratosPgitSaidaDTO> listaGridPesquisa;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo listarFuncBradescoService. */
	private IListarFuncBradescoService listarFuncBradescoService;

	/** Atributo filtroIdentificaoService. */
	private IFiltroIdentificaoService filtroIdentificaoService;

	/** Atributo filtroAgendamentoEfetivacaoEstornoImpl. */
	private IFiltroAgendamentoEfetivacaoEstornoService filtroAgendamentoEfetivacaoEstornoImpl;

	/** Atributo itemRadioFiltroSelecionado. */
	private String itemRadioFiltroSelecionado;

	/** Atributo itemSelecionadoListaCliente. */
	private Integer itemSelecionadoListaCliente;

	/** Atributo listaControleRadioConsultarCliente. */
	private List<SelectItem> listaControleRadioConsultarCliente = new ArrayList<SelectItem>();

	/** Atributo listaSaidaConsultarListaContratosPessoas. */
	private List<ConsultarListaContratosPessoasSaidaDTO> listaSaidaConsultarListaContratosPessoas;

	/** Atributo saidaConsultarListaContratosPessoas. */
	private ConsultarListaContratosPessoasSaidaDTO saidaConsultarListaContratosPessoas;

	/**
	 * Identificacao cliente contrato bean.
	 * 
	 * @param paginaRetorno
	 *            the pagina retorno
	 */
	public IdentificacaoClienteContratoBean(String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}

	/**
	 * Identificacao cliente contrato bean.
	 * 
	 * @param paginaRetorno
	 *            the pagina retorno
	 * @param paginaCliente
	 *            the pagina cliente
	 */
	public IdentificacaoClienteContratoBean(String paginaRetorno,
			String paginaCliente) {
		this.paginaRetorno = paginaRetorno;
		this.paginaCliente = paginaCliente;
	}

	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora() {
		try {
			this.listaEmpresaGestora = new ArrayList<SelectItem>();
			List<EmpresaGestoraSaidaDTO> list = new ArrayList<EmpresaGestoraSaidaDTO>();

			list = comboService.listarEmpresasGestoras();
			listaEmpresaGestora.clear();
			for (EmpresaGestoraSaidaDTO combo : list) {
				listaEmpresaGestora.add(new SelectItem(combo
						.getCdPessoaJuridicaContrato(), combo.getDsCnpj()));
			}
		} catch (PdcAdapterFunctionalException e) {
			listaEmpresaGestora = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Listar situacao contrato.
	 */
	public void listarSituacaoContrato() {
		try {
			this.listaSituacaoContrato = new ArrayList<SelectItem>();

			List<ListarSituacaoContratoSaidaDTO> saidaDTO = new ArrayList<ListarSituacaoContratoSaidaDTO>();

			saidaDTO = comboService.listarSituacaoContrato();

			listaSituacaoContratoHash.clear();
			for (ListarSituacaoContratoSaidaDTO combo : saidaDTO) {
				if (isEquals(getPaginaRetorno(), "conEncerrarContrato")
						&& isEquals(combo.getCdSituacaoContrato(),
								CONTRATO_ENCERRADO)) {
					continue;
				}

				listaSituacaoContratoHash.put(combo.getCdSituacaoContrato(),
						combo.getDsSituacaoContrato());
				this.listaSituacaoContrato
						.add(new SelectItem(combo.getCdSituacaoContrato(),
								combo.getDsSituacaoContrato()));
			}
		} catch (PdcAdapterFunctionalException e) {
			listaSituacaoContrato = new ArrayList<SelectItem>();
		}

	}

	/**
	 * Listar motivo situacao.
	 */
	public void listarMotivoSituacao() {

		if (getSituacaoFiltro() != null && getSituacaoFiltro() != 0) {

			this.listaMotivoSituacao = new ArrayList<SelectItem>();

			List<ListarMotivoSituacaoContratoSaidaDTO> saidaDTO = new ArrayList<ListarMotivoSituacaoContratoSaidaDTO>();

			ListarMotivoSituacaoContratoEntradaDTO entradaDTO = new ListarMotivoSituacaoContratoEntradaDTO();
			entradaDTO.setCdSituacao(getSituacaoFiltro());

			saidaDTO = comboService.listarMotivoSituacaoContrato(entradaDTO);

			listaMotivoSituacaoHash.clear();
			for (ListarMotivoSituacaoContratoSaidaDTO combo : saidaDTO) {
				listaMotivoSituacaoHash.put(
						combo.getCdMotivoSituacaoContrato(), combo
								.getDsMotivoSituacaoContrato());
				this.listaMotivoSituacao.add(new SelectItem(combo
						.getCdMotivoSituacaoContrato(), combo
						.getDsMotivoSituacaoContrato()));
			}

		} else {
			listaMotivoSituacaoHash.clear();
			listaMotivoSituacao.clear();
		}

	}

	/**
	 * Voltar.
	 * 
	 * @return the string
	 */
	public String voltar() {
		return this.paginaRetorno;
	}

	/**
	 * Listar tipo contrato.
	 */
	public void listarTipoContrato() {
		try {

			listaTipoContrato = new ArrayList<SelectItem>();

			List<TipoContratoSaidaDTO> list = comboService.listarTipoContrato();

			for (TipoContratoSaidaDTO saida : list) {
				listaTipoContrato.add(new SelectItem(saida.getCdTipoContrato(),
						saida.getDsTipoContrato()));
			}
		} catch (PdcAdapterFunctionalException e) {
			listaTipoContrato = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Pesquisar clientes.
	 * 
	 * @param entrada
	 *            the entrada
	 */
	private void pesquisarClientes(
			ConsultarListaClientePessoasEntradaDTO entrada) {
		this.setListaConsultarListaClientePessoas(this
				.getFiltroIdentificaoService().pesquisarClientes(entrada));
	}

	/**
	 * Pesquisar listar clientes.
	 * 
	 * @param entrada
	 *            the entrada
	 */
	private void pesquisarListarClientes(
			ConsultarListaClientePessoasEntradaDTO entrada) {
		this.setListaListarClubesClientes(this.getFiltroIdentificaoService()
				.listarClubesClientes(entrada));
	}

	public void limparPesquisaRrdoFiltroCliente() {
		
		getEntradaConsultarListaClientePessoas().setCdBanco(null);
		getEntradaConsultarListaClientePessoas().setCdAgenciaBancaria(null);
		getEntradaConsultarListaClientePessoas().setCdContaBancaria(null);
		
		getEntradaConsultarListaClientePessoas().setCdCpfCnpj(null);
		getEntradaConsultarListaClientePessoas().setCdFilialCnpj(null);
		getEntradaConsultarListaClientePessoas().setCdControleCnpj(null);
		
		getEntradaConsultarListaClientePessoas().setCdCpf(null);
		getEntradaConsultarListaClientePessoas().setCdControleCpf(null);
		
		getEntradaConsultarListaClientePessoas().setDsNomeRazaoSocial(null);
	}
	
	/**
	 * Consultar clientes.
	 * 
	 * @return the string
	 */
	public String consultarClientes() {
		this.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setHabilitaFiltroDeCliente(true);
		setFlagPesquisouPorIdentCliente(false);
		try {
			this.setItemClienteSelecionado(null);
			
			if(!"3".equals(getItemFiltroSelecionado())){
				getEntradaConsultarListaClientePessoas().setCdBanco(null);
				getEntradaConsultarListaClientePessoas().setCdAgenciaBancaria(null);
				getEntradaConsultarListaClientePessoas().setCdContaBancaria(null);
			}
			
			this.pesquisarClientes(this.getEntradaConsultarListaClientePessoas());

			if (this.getListaConsultarListaClientePessoas().size() != 0) {
				if (this.getListaConsultarListaClientePessoas().size() == 1) {
					this.setSaidaConsultarListaClientePessoas(this.getListaConsultarListaClientePessoas().get(0));
					this.setBloqueiaRadio(true);
					setEmpresaGestoraFiltro(2269651l);
					setTipoContratoFiltro(0);
					setNumeroFiltro("");
					setAgenciaOperadoraFiltro("");
					setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
					setCodigoFunc("");
					setSituacaoFiltro(0);
					listarMotivoSituacao();
					this.setHabilitaCampos(false);
					setDsNomeRazaoSocialParticipante(this.getSaidaConsultarListaClientePessoas().getDsNomeRazao());
					setCpfCnpjParticipante(CpfCnpjUtils.formatarCpfCnpj(this.getSaidaConsultarListaClientePessoas()
							.getCdCpfCnpj(), this.getSaidaConsultarListaClientePessoas().getCdFilialCnpj(), this
							.getSaidaConsultarListaClientePessoas().getCdControleCnpj()));
					setFlagPesquisouPorIdentCliente(true);
					return this.getPaginaRetorno();
				} else {
					this.setBloqueiaRadio(false);
					setHabilitaFiltroDeCliente(false);
					return this.getPaginaCliente();
				}
			} else {
				setHabilitaFiltroDeCliente(false);
				return this.getPaginaRetorno();
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), "/" + this.getPaginaRetorno()
							+ ".jsf", BradescoViewExceptionActionType.PATH,
					false);
			setHabilitaFiltroDeCliente(false);
			setFlagPesquisouPorIdentCliente(false);
			return "";
		}
	}

	/**
	 * Consultar clientes duplicacao.
	 * 
	 * @return the string
	 */
	public String consultarClientesDuplicacao() {
		this
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setHabilitaFiltroDeCliente(true);
		try {
			this.setItemClienteSelecionado(null);
			this.pesquisarClientes(this
					.getEntradaConsultarListaClientePessoas());

			if (this.getListaConsultarListaClientePessoas().size() != 0) {
				if (this.getListaConsultarListaClientePessoas().size() == 1) {
					this.setSaidaConsultarListaClientePessoas(this
							.getListaConsultarListaClientePessoas().get(0));
					this.setBloqueiaRadio(true);
					setTipoContratoFiltro(0);
					setNumeroFiltro("");
					setAgenciaOperadoraFiltro("");
					setEmpresaGestoraFiltro(2269651L);
					setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
					setCodigoFunc("");
					setSituacaoFiltro(0);
					listarMotivoSituacao();
					this.setHabilitaCampos(false);
					setDsNomeRazaoSocialParticipante(this
							.getSaidaConsultarListaClientePessoas()
							.getDsNomeRazao());
					setCpfCnpjParticipante(CpfCnpjUtils.formatarCpfCnpj(this
							.getSaidaConsultarListaClientePessoas()
							.getCdCpfCnpj(), this
							.getSaidaConsultarListaClientePessoas()
							.getCdFilialCnpj(), this
							.getSaidaConsultarListaClientePessoas()
							.getCdControleCnpj()));
					return "selDuplicacaoArquivoRetorno";
				} else {
					this.setBloqueiaRadio(false);
					setHabilitaFiltroDeCliente(false);
					return "selDuplicacaoArquivoRetorno";
				}
			} else {
				setHabilitaFiltroDeCliente(false);
				return "selDuplicacaoArquivoRetorno";
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setHabilitaFiltroDeCliente(false);
			return "";
		}
	}

	/**
	 * Listar clientes.
	 * 
	 * @return the string
	 */
	public String listarClientes() {

		this.setSaidaListarClubesClientes(new ListarClubesClientesSaidaDTO());
		setHabilitaFiltroDeCliente(true);
		try {
			this.setItemClienteSelecionado(null);
			this.pesquisarListarClientes(this
					.getEntradaConsultarListaClientePessoas());

			if (this.getListaListarClubesClientes().size() != 0) {
				if (this.getListaListarClubesClientes().size() == 1) {
					this.setSaidaListarClubesClientes(this
							.getListaListarClubesClientes().get(0));
					this.setBloqueiaRadio(true);
					setEmpresaGestoraFiltro(0l);
					setTipoContratoFiltro(0);
					setNumeroFiltro("");
					setAgenciaOperadoraFiltro("");
					setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
					setCodigoFunc("");
					setSituacaoFiltro(0);
					listarMotivoSituacao();
					this.setHabilitaCampos(false);
					setDsNomeRazaoSocialParticipante(this
							.getSaidaListarClubesClientes().getDsNomeRazao());
					setCpfCnpjParticipante(CpfCnpjUtils.formatarCpfCnpj(this
							.getSaidaListarClubesClientes().getCdCpfCnpj(),
							this.getSaidaListarClubesClientes()
									.getCdFilialCnpj(), this
									.getSaidaListarClubesClientes()
									.getCdControleCnpj()));
					return this.getPaginaRetorno();
				} else {
					this.setBloqueiaRadio(false);
					setHabilitaFiltroDeCliente(false);
					return this.getPaginaCliente();
				}
			} else {
				setHabilitaFiltroDeCliente(false);
				return this.getPaginaRetorno();
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), "/" + this.getPaginaRetorno()
							+ ".jsf", BradescoViewExceptionActionType.PATH,
					false);
			setHabilitaFiltroDeCliente(false);
			return "";
		}
	}

	/**
	 * Limpar dados pesquisa contrato.
	 */
	public void limparDadosPesquisaContrato() {
		setEmpresaGestoraFiltro(0l);
		setTipoContratoFiltro(0);
		setNumeroFiltro("");
		setAgenciaOperadoraFiltro("");
		setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		setCodigoFunc("");
		setSituacaoFiltro(0);
		listarMotivoSituacao();
		this.setHabilitaCampos(false);
		setHabilitaFiltroDeCliente(false);
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
		setEmpresaGestoraContrato(null);
		setNumeroContrato(null);
		setDescricaoContrato(null);
		setSituacaoContrato(null);
		limparRadioFiltro();
		setHabilitaEmpresaGestoraTipoContrato(true);
	}

	/**
	 * Is bloquear renegociar.
	 * 
	 * @return true, if is bloquear renegociar
	 */
	public boolean isBloquearRenegociar() {
		if (listaGridPesquisa != null && itemSelecionadoLista != null) {
			if (listaGridPesquisa.get(itemSelecionadoLista)
					.getCdSituacaoContrato() == 4
					|| listaGridPesquisa.get(itemSelecionadoLista)
							.getCdSituacaoContrato() == 5
					|| listaGridPesquisa.get(itemSelecionadoLista)
							.getCdSituacaoContrato() == 6) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Selecionar cliente.
	 * 
	 * @return the string
	 */
	public String selecionarCliente() {
		this
				.setSaidaConsultarListaClientePessoas(getListaConsultarListaClientePessoas()
						.get(this.getItemClienteSelecionado()));
		setHabilitaFiltroDeCliente(true);
		setBloqueiaRadio(true);
		// limparDadosPesquisaContrato();
		setTipoContratoFiltro(0);
		setNumeroFiltro("");
		setAgenciaOperadoraFiltro("");
		setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		setCodigoFunc("");
		setSituacaoFiltro(0);
		listarMotivoSituacao();
		this.setHabilitaCampos(false);
		setEmpresaGestoraFiltro(2269651L);
		return this.getPaginaRetorno();
	}

	/**
	 * Selecionar listar cliente.
	 * 
	 * @return the string
	 */
	public String selecionarListarCliente() {
		this.setSaidaListarClubesClientes(getListaListarClubesClientes().get(
				this.getItemClienteSelecionado()));
		setHabilitaFiltroDeCliente(true);
		setBloqueiaRadio(true);
		// limparDadosPesquisaContrato();
		setEmpresaGestoraFiltro(0l);
		setTipoContratoFiltro(0);
		setNumeroFiltro("");
		setAgenciaOperadoraFiltro("");
		setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		setCodigoFunc("");
		setSituacaoFiltro(0);
		listarMotivoSituacao();
		this.setHabilitaCampos(false);
		return this.getPaginaRetorno();
	}

	/**
	 * Limpar radio filtro.
	 */
	public void limparRadioFiltro() {
		if (getItemRadioFiltroSelecionado() != null
				&& getItemRadioFiltroSelecionado().equals("0")) {
			this.setBloqueiaRadio(false);
			setHabilitaEmpresaGestoraTipoContrato(false);
			setItemFiltroSelecionado(null);
			limparCamposClientePessoas();
			setEmpresaGestoraContrato(null);
			setEmpresaGestoraContratoCliente(null);
			setNumeroContrato(null);
			setNumeroContratoCliente(null);
			setDescricaoContrato(null);
			setDescricaoContratoCliente(null);
			setSituacaoContrato(null);
			setSituacaoContratoCliente(null);
			setEmpresaGestoraFiltro(2269651L);
			setTipoContratoFiltro(21);
			setNumeroFiltro(null);
			setListaConsultarListaContratosPessoas(null);
			limparCampos();

		} else if (getItemRadioFiltroSelecionado() != null
				&& getItemRadioFiltroSelecionado().equals("1")) {
			this.setBloqueiaRadio(true);
			setHabilitaEmpresaGestoraTipoContrato(true);
			setItemFiltroSelecionado(null);
			limparCamposClientePessoas();
			setEmpresaGestoraContrato(null);
			setEmpresaGestoraContratoCliente(null);
			setNumeroContrato(null);
			setNumeroContratoCliente(null);
			setDescricaoContrato(null);
			setDescricaoContratoCliente(null);
			setSituacaoContrato(null);
			setSituacaoContratoCliente(null);
			setEmpresaGestoraFiltro(2269651L);
			setTipoContratoFiltro(21);
			setNumeroFiltro(null);
			setListaConsultarListaContratosPessoas(null);
			limparCampos();

		} else {
			this.setBloqueiaRadio(false);
			setHabilitaEmpresaGestoraTipoContrato(false);
			setItemFiltroSelecionado(null);
			limparCamposClientePessoas();
			setEmpresaGestoraContrato(null);
			setEmpresaGestoraContratoCliente(null);
			setNumeroContrato(null);
			setNumeroContratoCliente(null);
			setDescricaoContrato(null);
			setDescricaoContratoCliente(null);
			setSituacaoContrato(null);
			setSituacaoContratoCliente(null);
			setEmpresaGestoraFiltro(2269651L);
			setTipoContratoFiltro(21);
			setNumeroFiltro(null);
			setListaConsultarListaContratosPessoas(null);
			limparCampos();
		}
	}

	/**
	 * Limpar campos cliente pessoas.
	 */
	private void limparCamposClientePessoas() {
		if (getSaidaConsultarListaClientePessoas() != null) {
			getSaidaConsultarListaClientePessoas().setCnpjOuCpfFormatado(null);
			getSaidaConsultarListaClientePessoas().setDsNomeRazao(null);
		}
	}

	/**
	 * Is habilita argumentos pesquisa.
	 * 
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		if ((listaConsultarListaContratosPessoas != null && listaConsultarListaContratosPessoas
				.size() > 0)
				|| (empresaGestoraContratoCliente != null && !empresaGestoraContratoCliente
						.trim().equals(""))
				|| (listaSaidaConsultarListaContratosPessoas != null && listaSaidaConsultarListaContratosPessoas
						.size() > 0)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Tela consultar clientes.
	 * 
	 * @return the string
	 */
	public String telaConsultarClientes() {
		this
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());

		ConsultarListaClientePessoasSaidaDTO saidaConsultarListaClientePessoas2 = new ConsultarListaClientePessoasSaidaDTO();
		try {
			this.pesquisarClientes(this
					.getEntradaConsultarListaClientePessoas());

			if (this.getListaConsultarListaClientePessoas().size() != 0) {
				if (this.getListaConsultarListaClientePessoas().size() == 1) {
					saidaConsultarListaClientePessoas2 = listaConsultarListaClientePessoas
							.get(0);
					this
							.setSaidaConsultarListaClientePessoas(saidaConsultarListaClientePessoas2);
				}
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), "/" + this.getPaginaRetorno()
							+ ".jsf", BradescoViewExceptionActionType.PATH,
					false);
			return "";
		}

		ConsultarContratoClienteFiltroEntradaDTO entrada = new ConsultarContratoClienteFiltroEntradaDTO();
		entrada.setCdClubPessoa(saidaConsultarListaClientePessoas2.getCdClub());
		entrada.setCdControleCpfPssoa(saidaConsultarListaClientePessoas2
				.getCdControleCnpj());
		entrada.setCdCpfCnpjPssoa(saidaConsultarListaClientePessoas2
				.getCdCpfCnpj());
		entrada.setCdFilialCnpjPssoa(saidaConsultarListaClientePessoas2
				.getCdFilialCnpj());

		listaSaidaConsultarContratoClienteFiltro = filtroAgendamentoEfetivacaoEstornoImpl
				.consultarContratoClienteFiltro(entrada);

		listaControleRadioConsultarCliente = new ArrayList<SelectItem>();
		for (int i = 0; i < getListaSaidaConsultarContratoClienteFiltro()
				.size(); i++) {
			this.listaControleRadioConsultarCliente.add(new SelectItem(i, " "));
		}

		setItemSelecionadoListaCliente(null);

		return "IDENT_CLIENTE";
	}

	/**
	 * Voltar ident cliente.
	 * 
	 * @return the string
	 */
	public String voltarIdentCliente() {
		limparCampos();

		return "VOLTAR_CONSULTA";
	}

	/**
	 * Selecionar consulta cliente.
	 * 
	 * @return the string
	 */
	public String selecionarConsultaCliente() {
		ConsultarContratoClienteFiltroSaidaDTO registroSelecionado = listaSaidaConsultarContratoClienteFiltro
				.get(itemSelecionadoListaCliente);
		setEmpresaGestoraContratoCliente(registroSelecionado
				.getDescricaoPessoaJuridicaFormatada());
		setNumeroContratoCliente(PgitUtil.longToString(registroSelecionado
				.getNrSequenciaContrato()));
		setDescricaoContratoCliente(registroSelecionado.getDsContrato());
		setSituacaoContratoCliente(registroSelecionado.getDsSituacaoContrato());
		setEmpresaGestoraContrato(registroSelecionado
				.getDescricaoPessoaJuridicaFormatada());
		setNumeroContrato(PgitUtil.longToString(registroSelecionado
				.getNrSequenciaContrato()));
		setDescricaoContrato(registroSelecionado.getDsContrato());
		setSituacaoContrato(registroSelecionado.getDsSituacaoContrato());

		setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridica());
		setNrSequenciaContratoNegocio(registroSelecionado
				.getNrSequenciaContrato());
		setCdTipoContratoNegocio(registroSelecionado.getCdTipoContrato());

		return "SELECIONAR";
	}

	/**
	 * Limpar campos.
	 */
	public void limparCampos() {
		this
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		if ("1".equals(getItemRadioFiltroSelecionado())) {
			setHabilitaEmpresaGestoraTipoContrato(false);
		} else {
			setHabilitaEmpresaGestoraTipoContrato(true);
		}
		if ("3".equals(getItemFiltroSelecionado())) {
			getEntradaConsultarListaClientePessoas().setCdBanco(237);
		}
	}

	public boolean isHabBtoConsultarCliente() {
		return isHabItemFiltroSelecionado();
	}

	public void valorizaBanco() {
		if (itemFiltroSelecionado.equals("3")) {
			entradaConsultarListaClientePessoas.setCdBanco(237);
		}
	}

	/**
	 * Is Hab Item Filtro Selecionado.
	 * 
	 * @return true, if is hab item filtro selecionado
	 */
	public boolean isHabItemFiltroSelecionado() {
		IncluirContratoContingenciaBean incluirContratoContingenciaBean = (IncluirContratoContingenciaBean) FacesUtils
				.getManagedBean("incluirContratoContingenciaBean");

		if (itemFiltroSelecionado != null) {
			if (itemFiltroSelecionado.equals("3")) {
				if (entradaConsultarListaClientePessoas.getCdAgenciaBancaria() != null
						&& entradaConsultarListaClientePessoas
								.getCdContaBancaria() != null) {
					if (incluirContratoContingenciaBean.getCdAgenciaGestora() == null
							|| incluirContratoContingenciaBean
									.getCdAgenciaGestora().equals("")) {
						return true;
					} else {
						return false;
					}
				} else {
					return true;
				}
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

    public void submitCliente(ActionEvent evt) {
        consultarClientes();
    }
	
	/**
	 * Is desabilita consultar contrato.
	 * 
	 * @return true, if is desabilita consultar contrato
	 */
	public boolean isDesabilitaConsultarContrato() {
		if (itemRadioFiltroSelecionado != null
				&& itemRadioFiltroSelecionado.equals("1")) {
			if (empresaGestoraFiltro != null && empresaGestoraFiltro != 0L) {
				if (tipoContratoFiltro != null && tipoContratoFiltro != 0) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Get: agenciaOperadoraFiltro.
	 * 
	 * @return agenciaOperadoraFiltro
	 */
	public String getAgenciaOperadoraFiltro() {
		return agenciaOperadoraFiltro;
	}

	/**
	 * Set: agenciaOperadoraFiltro.
	 * 
	 * @param agenciaOperadoraFiltro
	 *            the agencia operadora filtro
	 */
	public void setAgenciaOperadoraFiltro(String agenciaOperadoraFiltro) {
		this.agenciaOperadoraFiltro = agenciaOperadoraFiltro;
	}

	/**
	 * Get: empresaGestoraFiltro.
	 * 
	 * @return empresaGestoraFiltro
	 */
	public Long getEmpresaGestoraFiltro() {
		return empresaGestoraFiltro;
	}

	/**
	 * Set: empresaGestoraFiltro.
	 * 
	 * @param empresaGestoraFiltro
	 *            the empresa gestora filtro
	 */
	public void setEmpresaGestoraFiltro(Long empresaGestoraFiltro) {
		this.empresaGestoraFiltro = empresaGestoraFiltro;
	}

	/**
	 * Get: numeroFiltro.
	 * 
	 * @return numeroFiltro
	 */
	public String getNumeroFiltro() {
		return numeroFiltro;
	}

	/**
	 * Set: numeroFiltro.
	 * 
	 * @param numeroFiltro
	 *            the numero filtro
	 */
	public void setNumeroFiltro(String numeroFiltro) {
		this.numeroFiltro = numeroFiltro;
	}

	/**
	 * Get: situacaoFiltro.
	 * 
	 * @return situacaoFiltro
	 */
	public Integer getSituacaoFiltro() {
		return situacaoFiltro;
	}

	/**
	 * Set: situacaoFiltro.
	 * 
	 * @param situacaoFiltro
	 *            the situacao filtro
	 */
	public void setSituacaoFiltro(Integer situacaoFiltro) {
		this.situacaoFiltro = situacaoFiltro;
	}

	/**
	 * Get: tipoContratoFiltro.
	 * 
	 * @return tipoContratoFiltro
	 */
	public Integer getTipoContratoFiltro() {
		return tipoContratoFiltro;
	}

	/**
	 * Set: tipoContratoFiltro.
	 * 
	 * @param tipoContratoFiltro
	 *            the tipo contrato filtro
	 */
	public void setTipoContratoFiltro(Integer tipoContratoFiltro) {
		this.tipoContratoFiltro = tipoContratoFiltro;
	}

	/**
	 * Get: codigoFunc.
	 * 
	 * @return codigoFunc
	 */
	public String getCodigoFunc() {
		return codigoFunc;
	}

	/**
	 * Set: codigoFunc.
	 * 
	 * @param codigoFunc
	 *            the codigo func
	 */
	public void setCodigoFunc(String codigoFunc) {
		this.codigoFunc = codigoFunc;
	}

	/**
	 * Get: funcionarioDTO.
	 * 
	 * @return funcionarioDTO
	 */
	public ListarFuncionarioSaidaDTO getFuncionarioDTO() {
		return funcionarioDTO;
	}

	/**
	 * Set: funcionarioDTO.
	 * 
	 * @param funcionarioDTO
	 *            the funcionario dto
	 */
	public void setFuncionarioDTO(ListarFuncionarioSaidaDTO funcionarioDTO) {
		this.funcionarioDTO = funcionarioDTO;
	}

	/**
	 * Carrega lista funcionarios.
	 * 
	 * @return the string
	 */
	public String carregaListaFuncionarios() {

		if (!getCodigoFunc().equals("") && getCodigoFunc() != null) {
			try {
				funcionarioDTO = new ListarFuncionarioSaidaDTO();
				setFuncionarioDTO(getListarFuncBradescoService()
						.listarFuncionario(Long.parseLong(getCodigoFunc())));
				setCodigoFunc(Long.toString(funcionarioDTO.getCdFuncionario()));
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
				setCodigoFunc(null);

			}
		}

		return "";
	}

	/**
	 * Busca funcionarios.
	 */
	public void buscaFuncionarios() {

		if (!getCodigoFunc().equals("") && getCodigoFunc() != null) {
			try {
				funcionarioDTO = new ListarFuncionarioSaidaDTO();
				setFuncionarioDTO(getListarFuncBradescoService()
						.listarFuncionario(Long.parseLong(getCodigoFunc())));
				setCodigoFunc(Long.toString(funcionarioDTO.getCdFuncionario()));
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
			}
		} else {
			funcionarioDTO = new ListarFuncionarioSaidaDTO();
		}

	}

	/**
	 * Get: listarFuncBradescoService.
	 * 
	 * @return listarFuncBradescoService
	 */
	public IListarFuncBradescoService getListarFuncBradescoService() {
		return listarFuncBradescoService;
	}

	/**
	 * Set: listarFuncBradescoService.
	 * 
	 * @param listarFuncBradescoService
	 *            the listar func bradesco service
	 */
	public void setListarFuncBradescoService(
			IListarFuncBradescoService listarFuncBradescoService) {
		this.listarFuncBradescoService = listarFuncBradescoService;
	}

	/**
	 * Get: habilitaFiltroDeCliente.
	 * 
	 * @return habilitaFiltroDeCliente
	 */
	public boolean getHabilitaFiltroDeCliente() {
		return habilitaFiltroDeCliente;
	}

	/**
	 * Set: habilitaFiltroDeCliente.
	 * 
	 * @param habilitaFiltroDeCliente
	 *            the habilita filtro de cliente
	 */
	public void setHabilitaFiltroDeCliente(boolean habilitaFiltroDeCliente) {
		this.habilitaFiltroDeCliente = habilitaFiltroDeCliente;
	}
	
	public String limparDadosClienteComplementar() {
		setHabilitaFiltroDeCliente(false);
		setFlagPesquisouPorIdentCliente(false);
		limparCamposClientePessoas();
		limparCamposClientePessoas();
		setItemFiltroSelecionado(null);
		setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setBloqueiaRadio(false);
		limparRadioFiltro();
		return "";
	}
	
	public void limparDadosPesquisaContratoComplemento() {
		setEmpresaGestoraFiltro(0l);
		setTipoContratoFiltro(0);
		setNumeroFiltro("");
		setAgenciaOperadoraFiltro("");
		setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
		setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setCodigoFunc("");
		setSituacaoFiltro(0);		
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
		setEmpresaGestoraContratoCliente(null);
		setNumeroContratoCliente(null);
		setDescricaoContratoCliente(null);
		setSituacaoContratoCliente(null);
		setEmpresaGestoraFiltro(2269651L);
		this.setHabilitaCampos(false);
		setHabilitaEmpresaGestoraTipoContrato(true);
		setEmpresaGestoraContrato(null);
		setNumeroContrato(null);
		setDescricaoContrato(null);
		setSituacaoContrato(null);
		limparRadioFiltro();		
	}

	/**
	 * Limpar dados cliente.
	 * 
	 * @return the string
	 */
	public String limparDadosCliente() {
		setHabilitaFiltroDeCliente(false);
		setFlagPesquisouPorIdentCliente(false);
		setItemFiltroSelecionado(null);
		setItemClienteSelecionado(null);
		limparRadioFiltro();
		setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		setListaGridPesquisa(null);
		setItemSelecionadoLista(null);
		setEmpresaGestoraContratoCliente(null);
		setNumeroContratoCliente(null);
		setDescricaoContratoCliente(null);
		setSituacaoContratoCliente(null);
		setEmpresaGestoraFiltro(2269651L);
		this.setHabilitaCampos(false);
		setHabilitaEmpresaGestoraTipoContrato(true);
		if (flagDuplicacao != null) {
			return "S".equalsIgnoreCase(flagDuplicacao) ? ""
					: getPaginaRetorno();
		}
		return "";
	}

	/**
	 * Get: selectItemCliente.
	 * 
	 * @return selectItemCliente
	 */
	public List<SelectItem> getSelectItemCliente() {
		if (this.getListaConsultarListaClientePessoas() == null
				|| this.getListaConsultarListaClientePessoas().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this
					.getListaConsultarListaClientePessoas().size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}
			return list;
		}
	}

	/**
	 * Get: selectItemListarCliente.
	 * 
	 * @return selectItemListarCliente
	 */
	public List<SelectItem> getSelectItemListarCliente() {
		if (this.getListaListarClubesClientes() == null
				|| this.getListaListarClubesClientes().isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();

			for (int index = 0; index < this.getListaListarClubesClientes()
					.size(); index++) {
				list.add(new SelectItem(String.valueOf(index), ""));
			}

			return list;
		}
	}

	/**
	 * Consultar contrato.
	 * 
	 * @return the string
	 */
	public String consultarContrato() {
		if (getObrigatoriedade() != null && getObrigatoriedade().equals("T")) {

			// desmarca o radio da Grid - vreghini - 01/7/2010.
			setItemSelecionadoLista(null);
			setHabilitaEmpresaGestoraTipoContrato(true);

			try {
				ListarContratosPgitEntradaDTO entradaDTO = new ListarContratosPgitEntradaDTO();

				entradaDTO.setCdAgencia(getAgenciaOperadoraFiltro() != null
						&& !getAgenciaOperadoraFiltro().equals("") ? Integer
						.parseInt(getAgenciaOperadoraFiltro()) : 0);
				entradaDTO
						.setCdClubPessoa(getSaidaConsultarListaClientePessoas()
								.getCdClub() != null ? getSaidaConsultarListaClientePessoas()
								.getCdClub()
								: 0L);
				entradaDTO.setCdFuncionario(getCodigoFunc() != null
						&& !getCodigoFunc().equals("") ? Long
						.parseLong(getCodigoFunc()) : 0L);
				entradaDTO
						.setCdPessoaJuridica(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro()
								: 0);

				if (isEquals(getPaginaRetorno(),
						"confirmarAssinaturaContratoContingencia")) {
					entradaDTO.setCdSituacaoContrato(CONTRATO_PENDENTE);
				} else {
					entradaDTO
							.setCdSituacaoContrato(getSituacaoFiltro() != null ? getSituacaoFiltro()
									: 0);
				}

				entradaDTO
						.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro()
								: 0);
				entradaDTO.setNrSequenciaContrato(getNumeroFiltro() != null
						&& !getNumeroFiltro().equals("") ? Long
						.valueOf(getNumeroFiltro()) : Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(saidaConsultarListaClientePessoas
						.getCdCpfCnpj());
				entradaDTO
						.setCdFilialCnpjPssoa(saidaConsultarListaClientePessoas
								.getCdFilialCnpj());
				entradaDTO
						.setCdControleCpfPssoa(saidaConsultarListaClientePessoas
								.getCdControleCnpj());

				if (getPaginaRetorno().equals("conManterSolManutContrato")) {
					entradaDTO.setParametroPrograma(1);
				} else if (getPaginaRetorno().equals("conManterPendenciasContrato")) {
					entradaDTO.setParametroPrograma(2);
				} else if (getPaginaRetorno().equals("conRecuperarContratoEncerrado")) {
					entradaDTO.setParametroPrograma(3);
				} else if (getPaginaRetorno().equals("pesqContratoPendenteAssinatura")) {
					entradaDTO.setParametroPrograma(4);
				} else if (getPaginaRetorno().equals("conEncerrarContrato")) {
					entradaDTO.setParametroPrograma(5);
				} else if (getPaginaRetorno().equals("pesqManterSolicitacoesMudancaAmbienteOperacao")) {
					entradaDTO.setParametroPrograma(6);
				} else if (getPaginaRetorno().equals("confirmarAssinaturaContratoContingencia")) {
                    entradaDTO.setParametroPrograma(4);
				} else {
					entradaDTO.setParametroPrograma(0);
				}

				if (getPaginaRetorno().equals("conEncerrarContrato")) {
					List<ListarContratosPgitSaidaDTO> listaGridTemp = getFiltroIdentificaoService()
							.pesquisarManutencaoContrato(entradaDTO);

					setListaGridPesquisa(new ArrayList<ListarContratosPgitSaidaDTO>());

					for (ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO : listaGridTemp) {
							getListaGridPesquisa().add(
									listarContratosPgitSaidaDTO);
					}
				} else {
					saidaContrato = new ListarContratosPgitSaidaDTO();
					setListaGridPesquisa(getFiltroIdentificaoService()
							.pesquisarManutencaoContrato(entradaDTO));
					setSaidaContrato(getListaGridPesquisa().get(0));

				}

				this.listaControleRadio = new ArrayList<SelectItem>();
				for (int i = 0; i < getListaGridPesquisa().size(); i++) {
					this.listaControleRadio.add(new SelectItem(i, " "));
				}
				bloquearArgumentosPesquisaAposConsulta();
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				setListaGridPesquisa(null);

			}

			return getPaginaRetorno();
		}

		return "";
	}

	/**
	 * Bloquear argumentos pesquisa apos consulta.
	 */
	public void bloquearArgumentosPesquisaAposConsulta() {
		setHabilitaEmpresaGestoraTipoContrato(false);
	}

	/**
	 * Consultar contrato solicitacao recuperacao remessa.
	 * 
	 * @return the string
	 */
	public String consultarContratoSolicitacaoRecuperacaoRemessa() {
		if (getObrigatoriedade() != null && getObrigatoriedade().equals("T")) {

			// desmarca o radio da Grid - vreghini - 01/7/2010.
			setItemSelecionadoLista(null);
			setHabilitaEmpresaGestoraTipoContrato(true);

			try {
				ConsultarListaContratosPessoasEntradaDTO entradaDTO = new ConsultarListaContratosPessoasEntradaDTO();

				entradaDTO
						.setCdClub(getSaidaConsultarListaClientePessoas()
								.getCdClub() != null ? getSaidaConsultarListaClientePessoas()
								.getCdClub()
								: 0L);
				entradaDTO
						.setCdPessoaJuridicaContrato(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro()
								: 0);
				entradaDTO
						.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro()
								: 0);
				entradaDTO.setNrSeqContratoNegocio(getNumeroFiltro() != null
						&& !getNumeroFiltro().equals("") ? Long
						.valueOf(getNumeroFiltro()) : Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(saidaConsultarListaClientePessoas
						.getCdCpfCnpj());
				entradaDTO
						.setCdFilialCnpjPssoa(saidaConsultarListaClientePessoas
								.getCdFilialCnpj());
				entradaDTO
						.setCdControleCpfPssoa(saidaConsultarListaClientePessoas
								.getCdControleCnpj());

				setListaSaidaConsultarListaContratosPessoas(new ArrayList<ConsultarListaContratosPessoasSaidaDTO>());
				listaSaidaConsultarListaContratosPessoas = getFiltroIdentificaoService()
						.pesquisarContratos(entradaDTO);

				saidaConsultarListaContratosPessoas = listaSaidaConsultarListaContratosPessoas
						.get(0);

				setEmpresaGestoraContrato(saidaConsultarListaContratosPessoas
						.getDescricaoPessoaJuridicaFormatada());
				setNumeroContrato(saidaConsultarListaContratosPessoas
						.getNrSeqContratoNegocio().toString());
				setDescricaoContrato(saidaConsultarListaContratosPessoas
						.getDsContrato());
				setSituacaoContrato(saidaConsultarListaContratosPessoas
						.getDsSituacaoContratoNegocio());
				setCdPessoaJuridicaContrato(saidaConsultarListaContratosPessoas
						.getCdPessoaJuridicaContrato());
				setNrSequenciaContratoNegocio(saidaConsultarListaContratosPessoas
						.getNrSeqContratoNegocio());
				setCdTipoContratoNegocio(saidaConsultarListaContratosPessoas
						.getCdTipoContratoNegocio());

				setHabilitaEmpresaGestoraTipoContrato(false);

			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
			}
		}

		return "";
	}

	/**
	 * Consultar contrato alterar valor tarifa.
	 * 
	 * @return the string
	 */
	public String consultarContratoAlterarValorTarifa() {
		if (getObrigatoriedade() != null && getObrigatoriedade().equals("T")) {

			// desmarca o radio da Grid - vreghini - 01/7/2010.
			setItemSelecionadoLista(null);
			setHabilitaEmpresaGestoraTipoContrato(true);

			try {

				ListarContratosPgitEntradaDTO entradaDTO = new ListarContratosPgitEntradaDTO();

				entradaDTO.setCdAgencia(getAgenciaOperadoraFiltro() != null
						&& !getAgenciaOperadoraFiltro().equals("") ? Integer
						.parseInt(getAgenciaOperadoraFiltro()) : 0);
				entradaDTO
						.setCdClubPessoa(getSaidaConsultarListaClientePessoas()
								.getCdClub() != null ? getSaidaConsultarListaClientePessoas()
								.getCdClub()
								: 0L);
				entradaDTO.setCdFuncionario(getCodigoFunc() != null
						&& !getCodigoFunc().equals("") ? Long
						.parseLong(getCodigoFunc()) : 0L);
				entradaDTO
						.setCdPessoaJuridica(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro()
								: 0);
				entradaDTO
						.setCdSituacaoContrato(getSituacaoFiltro() != null ? getSituacaoFiltro()
								: 0);
				entradaDTO
						.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro()
								: 0);
				entradaDTO.setNrSequenciaContrato(getNumeroFiltro() != null
						&& !getNumeroFiltro().equals("") ? Long
						.valueOf(getNumeroFiltro()) : Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(saidaConsultarListaClientePessoas
						.getCdCpfCnpj());
				entradaDTO
						.setCdFilialCnpjPssoa(saidaConsultarListaClientePessoas
								.getCdFilialCnpj());
				entradaDTO
						.setCdControleCpfPssoa(saidaConsultarListaClientePessoas
								.getCdControleCnpj());

				if (getPaginaRetorno().equals("conManterSolManutContrato")) {
					entradaDTO.setParametroPrograma(1);
				} else if (getPaginaRetorno().equals(
						"conManterPendenciasContrato")) {
					entradaDTO.setParametroPrograma(2);
				} else if (getPaginaRetorno().equals(
						"conRecuperarContratoEncerrado")) {
					entradaDTO.setParametroPrograma(3);
				} else if (getPaginaRetorno().equals(
						"pesqContratoPendenteAssinatura")) {
					entradaDTO.setParametroPrograma(4);
				} else if (getPaginaRetorno().equals("conEncerrarContrato")) {
					entradaDTO.setParametroPrograma(5);
				} else if (getPaginaRetorno().equals(
						"pesqManterSolicitacoesMudancaAmbienteOperacao")) {
					entradaDTO.setParametroPrograma(6);
				} else {
					entradaDTO.setParametroPrograma(0);
				}

				if (getPaginaRetorno().equals("conEncerrarContrato")) {
					List<ListarContratosPgitSaidaDTO> listaGridTemp = getFiltroIdentificaoService()
							.pesquisarManutencaoContrato(entradaDTO);

					setListaGridPesquisa(new ArrayList<ListarContratosPgitSaidaDTO>());

					for (ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO : listaGridTemp) {
						if (listarContratosPgitSaidaDTO.getCdSituacaoContrato() == 1
								|| listarContratosPgitSaidaDTO
										.getCdSituacaoContrato() == 3) {
							getListaGridPesquisa().add(
									listarContratosPgitSaidaDTO);
						}
					}
				} else {
					saidaContrato = new ListarContratosPgitSaidaDTO();
					setListaGridPesquisa(getFiltroIdentificaoService()
							.pesquisarManutencaoContrato(entradaDTO));
					setSaidaContrato(getListaGridPesquisa().get(0));

				}

				this.listaControleRadio = new ArrayList<SelectItem>();
				for (int i = 0; i < getListaGridPesquisa().size(); i++) {
					this.listaControleRadio.add(new SelectItem(i, " "));
				}
				setHabilitaEmpresaGestoraTipoContrato(false);
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				setListaGridPesquisa(null);

			}

			return getPaginaRetorno();
		}

		return "";
	}

	/**
	 * Con contrato alterar valor tarifa.
	 * 
	 * @return the string
	 */
	public String conContratoAlterarValorTarifa() {
		if (getObrigatoriedade() != null && getObrigatoriedade().equals("T")) {

			setItemSelecionadoLista(null);
			setHabilitaEmpresaGestoraTipoContrato(true);

			try {

				ListarContratosPgitEntradaDTO entradaDTO = new ListarContratosPgitEntradaDTO();

				entradaDTO
						.setCdClubPessoa(getSaidaConsultarListaClientePessoas()
								.getCdClub() != null ? getSaidaConsultarListaClientePessoas()
								.getCdClub()
								: 0L);
				entradaDTO
						.setCdPessoaJuridica(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro()
								: 0);
				entradaDTO
						.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro()
								: 0);
				entradaDTO.setNrSequenciaContrato(getNumeroFiltro() != null
						&& !getNumeroFiltro().equals("") ? Long
						.valueOf(getNumeroFiltro()) : Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(saidaConsultarListaClientePessoas
						.getCdCpfCnpj());
				entradaDTO
						.setCdFilialCnpjPssoa(saidaConsultarListaClientePessoas
								.getCdFilialCnpj());
				entradaDTO
						.setCdControleCpfPssoa(saidaConsultarListaClientePessoas
								.getCdControleCnpj());

				saidaContrato = new ListarContratosPgitSaidaDTO();
				setListaGridPesquisa(getFiltroIdentificaoService()
						.listarContratosRenegociacao(entradaDTO));
				setSaidaContrato(getListaGridPesquisa().get(0));

				this.listaControleRadio = new ArrayList<SelectItem>();
				for (int i = 0; i < getListaGridPesquisa().size(); i++) {
					this.listaControleRadio.add(new SelectItem(i, " "));
				}
				setHabilitaEmpresaGestoraTipoContrato(false);
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				setListaGridPesquisa(null);

			}

			return getPaginaRetorno();
		}

		return "";
	}

	/**
	 * Consultar contrato alterar valor tarifa2.
	 */
	public void consultarContratoAlterarValorTarifa2() {
		try {
			ConsultarListaContratosPessoasEntradaDTO entrada = new ConsultarListaContratosPessoasEntradaDTO();
			entrada.setCdClub(0L);
			entrada
					.setCdPessoaJuridicaContrato(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro()
							: 0L);
			entrada
					.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro()
							: 0);
			entrada
					.setNrSeqContratoNegocio(!getNumeroFiltro().equals("") ? Long
							.parseLong(getNumeroFiltro())
							: 0);

			setListaConsultarListaContratosPessoas(getFiltroIdentificaoService()
					.pesquisarContratos(entrada));

			ConsultarListaContratosPessoasSaidaDTO saida = getListaConsultarListaContratosPessoas()
					.get(0);

			setEmpresaGestoraContrato(saida
					.getDescricaoPessoaJuridicaFormatada());
			setNumeroContrato(saida.getNrSeqContratoNegocio().toString());
			setDescricaoContrato(saida.getDsContrato());
			setSituacaoContrato(saida.getDsSituacaoContratoNegocio());
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaConsultarListaContratosPessoas(null);
			setEmpresaGestoraContrato(null);
			setNumeroContrato(null);
			setDescricaoContrato(null);
			setSituacaoContrato(null);
		}
	}

	/**
	 * Consultar contrato duplicacao.
	 * 
	 * @return the string
	 */
	public String consultarContratoDuplicacao() {
		if (getObrigatoriedade() != null && getObrigatoriedade().equals("T")) {

			// desmarca o radio da Grid - vreghini - 01/7/2010.
			setItemSelecionadoLista(null);
			setHabilitaEmpresaGestoraTipoContrato(true);

			try {

				ListarContratosPgitEntradaDTO entradaDTO = new ListarContratosPgitEntradaDTO();

				entradaDTO.setCdAgencia(getAgenciaOperadoraFiltro() != null
						&& !getAgenciaOperadoraFiltro().equals("") ? Integer
						.parseInt(getAgenciaOperadoraFiltro()) : 0);
				entradaDTO
						.setCdClubPessoa(getSaidaConsultarListaClientePessoas()
								.getCdClub() != null ? getSaidaConsultarListaClientePessoas()
								.getCdClub()
								: 0L);
				entradaDTO.setCdFuncionario(getCodigoFunc() != null
						&& !getCodigoFunc().equals("") ? Long
						.parseLong(getCodigoFunc()) : 0L);
				entradaDTO
						.setCdPessoaJuridica(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro()
								: 0);
				entradaDTO
						.setCdSituacaoContrato(getSituacaoFiltro() != null ? getSituacaoFiltro()
								: 0);
				entradaDTO
						.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro()
								: 0);
				entradaDTO.setNrSequenciaContrato(getNumeroFiltro() != null
						&& !getNumeroFiltro().equals("") ? Long
						.valueOf(getNumeroFiltro()) : Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(saidaConsultarListaClientePessoas
						.getCdCpfCnpj());
				entradaDTO
						.setCdFilialCnpjPssoa(saidaConsultarListaClientePessoas
								.getCdFilialCnpj());
				entradaDTO
						.setCdControleCpfPssoa(saidaConsultarListaClientePessoas
								.getCdControleCnpj());

				if (getPaginaRetorno().equals("conManterSolManutContrato")) {
					entradaDTO.setParametroPrograma(1);
				} else if (getPaginaRetorno().equals(
						"conManterPendenciasContrato")) {
					entradaDTO.setParametroPrograma(2);
				} else if (getPaginaRetorno().equals(
						"conRecuperarContratoEncerrado")) {
					entradaDTO.setParametroPrograma(3);
				} else if (getPaginaRetorno().equals(
						"pesqContratoPendenteAssinatura")) {
					entradaDTO.setParametroPrograma(4);
				} else if (getPaginaRetorno().equals("conEncerrarContrato")) {
					entradaDTO.setParametroPrograma(5);
				} else if (getPaginaRetorno().equals(
						"pesqManterSolicitacoesMudancaAmbienteOperacao")) {
					entradaDTO.setParametroPrograma(6);
				} else {
					entradaDTO.setParametroPrograma(0);
				}

				if (getPaginaRetorno().equals("conEncerrarContrato")) {
					List<ListarContratosPgitSaidaDTO> listaGridTemp = getFiltroIdentificaoService()
							.pesquisarManutencaoContrato(entradaDTO);

					setListaGridPesquisa(new ArrayList<ListarContratosPgitSaidaDTO>());

					for (ListarContratosPgitSaidaDTO listarContratosPgitSaidaDTO : listaGridTemp) {
						if (listarContratosPgitSaidaDTO.getCdSituacaoContrato() == 1
								|| listarContratosPgitSaidaDTO
										.getCdSituacaoContrato() == 3) {
							getListaGridPesquisa().add(
									listarContratosPgitSaidaDTO);
						}
					}
				} else {
					setListaGridPesquisa(getFiltroIdentificaoService()
							.pesquisarManutencaoContrato(entradaDTO));
				}

				this.listaControleRadio = new ArrayList<SelectItem>();
				for (int i = 0; i < getListaGridPesquisa().size(); i++) {
					this.listaControleRadio.add(new SelectItem(i, " "));
				}
				setHabilitaEmpresaGestoraTipoContrato(false);
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				setListaGridPesquisa(null);

			}

			return "";
		}
		return "";
	}

	/**
	 * Con contrato alterar valor tarifa.
	 * 
	 * @return the string
	 */
	public String conConsultarVinculoContratoDebitoTr() {
		if (getObrigatoriedade() != null && getObrigatoriedade().equals("T")) {

			setItemSelecionadoLista(null);
			setHabilitaEmpresaGestoraTipoContrato(true);

			try {

				ListarContratosPgitEntradaDTO entradaDTO = new ListarContratosPgitEntradaDTO();

				entradaDTO
						.setCdClubPessoa(getSaidaConsultarListaClientePessoas()
								.getCdClub() != null ? getSaidaConsultarListaClientePessoas()
								.getCdClub()
								: 0L);
				entradaDTO
						.setCdPessoaJuridica(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro()
								: 0);
				entradaDTO
						.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro()
								: 0);
				entradaDTO.setNrSequenciaContrato(getNumeroFiltro() != null
						&& !getNumeroFiltro().equals("") ? Long
						.valueOf(getNumeroFiltro()) : Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(saidaConsultarListaClientePessoas
						.getCdCpfCnpj());
				entradaDTO
						.setCdFilialCnpjPssoa(saidaConsultarListaClientePessoas
								.getCdFilialCnpj());
				entradaDTO
						.setCdControleCpfPssoa(saidaConsultarListaClientePessoas
								.getCdControleCnpj());

				saidaContrato = new ListarContratosPgitSaidaDTO();
				setListaGridPesquisa(getFiltroIdentificaoService()
						.listarContratosRenegociacao(entradaDTO));
				setSaidaContrato(getListaGridPesquisa().get(0));

				this.listaControleRadio = new ArrayList<SelectItem>();
				for (int i = 0; i < getListaGridPesquisa().size(); i++) {
					this.listaControleRadio.add(new SelectItem(i, " "));
				}
				setHabilitaEmpresaGestoraTipoContrato(false);
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				setListaGridPesquisa(null);

			}

			return getPaginaRetorno();
		}

		return "";
	}

	/**
	 * Pesquisar.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void pesquisar(ActionEvent evt) {
		consultarContrato();
	}

	/**
	 * Pesquisar con alterar valor tarifa.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void pesquisarConAlterarValorTarifa(ActionEvent evt) {
		conContratoAlterarValorTarifa();
	}

	/**
	 * paginarConVincContrDebitoTr.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void paginarConVincContrDebitoTr(ActionEvent evt) {
		conConsultarVinculoContratoDebitoTr();
	}

	/**
	 * Pagina contrato.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void paginaContrato(ActionEvent evt) {
		if (getObrigatoriedade() != null && getObrigatoriedade().equals("T")) {

			// desmarca o radio da Grid - vreghini - 01/7/2010.
			setItemSelecionadoLista(null);
			setHabilitaEmpresaGestoraTipoContrato(true);

			try {

				ListarContratosPgitEntradaDTO entradaDTO = new ListarContratosPgitEntradaDTO();

				entradaDTO.setCdAgencia(getAgenciaOperadoraFiltro() != null
						&& !getAgenciaOperadoraFiltro().equals("") ? Integer
						.parseInt(getAgenciaOperadoraFiltro()) : 0);
				entradaDTO
						.setCdClubPessoa(getSaidaConsultarListaClientePessoas()
								.getCdClub() != null ? getSaidaConsultarListaClientePessoas()
								.getCdClub()
								: 0L);
				entradaDTO.setCdFuncionario(getCodigoFunc() != null
						&& !getCodigoFunc().equals("") ? Long
						.parseLong(getCodigoFunc()) : 0L);
				entradaDTO
						.setCdPessoaJuridica(getEmpresaGestoraFiltro() != null ? getEmpresaGestoraFiltro()
								: 0);
				entradaDTO
						.setCdSituacaoContrato(getSituacaoFiltro() != null ? getSituacaoFiltro()
								: 0);
				entradaDTO
						.setCdTipoContratoNegocio(getTipoContratoFiltro() != null ? getTipoContratoFiltro()
								: 0);
				entradaDTO.setNrSequenciaContrato(getNumeroFiltro() != null
						&& !getNumeroFiltro().equals("") ? Long
						.valueOf(getNumeroFiltro()) : Long.valueOf(0));
				entradaDTO.setCdCpfCnpjPssoa(saidaConsultarListaClientePessoas
						.getCdCpfCnpj());
				entradaDTO
						.setCdFilialCnpjPssoa(saidaConsultarListaClientePessoas
								.getCdFilialCnpj());
				entradaDTO
						.setCdControleCpfPssoa(saidaConsultarListaClientePessoas
								.getCdControleCnpj());

				if (getPaginaRetorno().equals("conManterSolManutContrato")) {
					entradaDTO.setParametroPrograma(1);
				} else if (getPaginaRetorno().equals(
						"conManterPendenciasContrato")) {
					entradaDTO.setParametroPrograma(2);
				} else if (getPaginaRetorno().equals(
						"conRecuperarContratoEncerrado")) {
					entradaDTO.setParametroPrograma(3);
				} else if (getPaginaRetorno().equals(
						"pesqContratoPendenteAssinatura")) {
					entradaDTO.setParametroPrograma(4);
				} else if (getPaginaRetorno().equals("conEncerrarContrato")) {
					entradaDTO.setParametroPrograma(5);
				} else {
					entradaDTO.setParametroPrograma(0);
				}

				setListaGridPesquisa(getFiltroIdentificaoService()
						.pesquisarManutencaoContrato(entradaDTO));

				this.listaControleRadio = new ArrayList<SelectItem>();
				for (int i = 0; i < getListaGridPesquisa().size(); i++) {
					this.listaControleRadio.add(new SelectItem(i, " "));
				}
				setHabilitaEmpresaGestoraTipoContrato(false);
			} catch (PdcAdapterFunctionalException p) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(), false);
				setListaGridPesquisa(null);

			}
		}
	}

	/**
	 * Get: obrigatoriedade.
	 * 
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 * 
	 * @param obrigatoriedade
	 *            the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Desabilita campos contrato.
	 */
	public void desabilitaCamposContrato() {
		if (!numeroFiltro.equals("")) {
			this.setHabilitaCampos(true);
			this.setAgenciaOperadoraFiltro("");
			this.setCodigoFunc("");
			this.getFuncionarioDTO().setDsFuncionario("");
			this.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());
			this.setSituacaoFiltro(0);
			this.setListaMotivoSituacao(new ArrayList<SelectItem>());

		} else {
			this.setHabilitaCampos(false);
		}
	}

	/**
	 * Is habilita campos.
	 * 
	 * @return true, if is habilita campos
	 */
	public boolean isHabilitaCampos() {
		return habilitaCampos;
	}

	/**
	 * Set: habilitaCampos.
	 * 
	 * @param habilitaCampos
	 *            the habilita campos
	 */
	public void setHabilitaCampos(boolean habilitaCampos) {
		this.habilitaCampos = habilitaCampos;
	}

	/**
	 * Get: listaControleRadio.
	 * 
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 * 
	 * @param listaControleRadio
	 *            the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: listaGridPesquisa.
	 * 
	 * @return listaGridPesquisa
	 */
	public List<ListarContratosPgitSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: listaGridPesquisa.
	 * 
	 * @param listaGridPesquisa
	 *            the lista grid pesquisa
	 */
	public void setListaGridPesquisa(
			List<ListarContratosPgitSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: itemSelecionadoLista.
	 * 
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 * 
	 * @param itemSelecionadoLista
	 *            the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Limpar tela.
	 */
	public void limparTela() {
		// limparDadosCliente();

		setHabilitaFiltroDeCliente(false);
		setBloqueiaRadio(false);

		// limparDadosPesquisaContrato();
		this.setHabilitaCampos(false);
		setListaGridPesquisa(new ArrayList<ListarContratosPgitSaidaDTO>());
		setItemSelecionadoLista(null);
		setHabilitaEmpresaGestoraTipoContrato(true);
	}

	/**
	 * Is habilita empresa gestora tipo contrato.
	 * 
	 * @return true, if is habilita empresa gestora tipo contrato
	 */
	public boolean isHabilitaEmpresaGestoraTipoContrato() {
		return habilitaEmpresaGestoraTipoContrato;
	}

	/**
	 * Set: habilitaEmpresaGestoraTipoContrato.
	 * 
	 * @param habilitaEmpresaGestoraTipoContrato
	 *            the habilita empresa gestora tipo contrato
	 */
	public void setHabilitaEmpresaGestoraTipoContrato(
			boolean habilitaEmpresaGestoraTipoContrato) {
		this.habilitaEmpresaGestoraTipoContrato = habilitaEmpresaGestoraTipoContrato;
	}

	/**
	 * Limpar gerente responsavel.
	 */
	public void limparGerenteResponsavel() {
		// Se Ag�ncia Respons�vel n�o foi informada, limpar dados do Gerente
		// respons�vel.
		if (getAgenciaOperadoraFiltro() == null
				|| getAgenciaOperadoraFiltro().equals("")) {
			setCodigoFunc("");
			funcionarioDTO = new ListarFuncionarioSaidaDTO();
		}
	}

	/**
	 * Get: cpfCnpjParticipante.
	 * 
	 * @return cpfCnpjParticipante
	 */
	public String getCpfCnpjParticipante() {
		return cpfCnpjParticipante;
	}

	/**
	 * Set: cpfCnpjParticipante.
	 * 
	 * @param cpfCnpjParticipante
	 *            the cpf cnpj participante
	 */
	public void setCpfCnpjParticipante(String cpfCnpjParticipante) {
		this.cpfCnpjParticipante = cpfCnpjParticipante;
	}

	/**
	 * Get: dsNomeRazaoSocialParticipante.
	 * 
	 * @return dsNomeRazaoSocialParticipante
	 */
	public String getDsNomeRazaoSocialParticipante() {
		return dsNomeRazaoSocialParticipante;
	}

	/**
	 * Set: dsNomeRazaoSocialParticipante.
	 * 
	 * @param dsNomeRazaoSocialParticipante
	 *            the ds nome razao social participante
	 */
	public void setDsNomeRazaoSocialParticipante(
			String dsNomeRazaoSocialParticipante) {
		this.dsNomeRazaoSocialParticipante = dsNomeRazaoSocialParticipante;
	}

	/**
	 * Get: listaSituacaoContratoHash.
	 * 
	 * @return listaSituacaoContratoHash
	 */
	public Map<Integer, String> getListaSituacaoContratoHash() {
		return listaSituacaoContratoHash;
	}

	/**
	 * Set lista situacao contrato hash.
	 * 
	 * @param listaSituacaoContratoHash
	 *            the lista situacao contrato hash
	 */
	public void setListaSituacaoContratoHash(
			Map<Integer, String> listaSituacaoContratoHash) {
		this.listaSituacaoContratoHash = listaSituacaoContratoHash;
	}

	/**
	 * Get: saidaListarClubesClientes.
	 * 
	 * @return saidaListarClubesClientes
	 */
	public ListarClubesClientesSaidaDTO getSaidaListarClubesClientes() {
		return saidaListarClubesClientes;
	}

	/**
	 * Set: saidaListarClubesClientes.
	 * 
	 * @param saidaListarClubesClientes
	 *            the saida listar clubes clientes
	 */
	public void setSaidaListarClubesClientes(
			ListarClubesClientesSaidaDTO saidaListarClubesClientes) {
		this.saidaListarClubesClientes = saidaListarClubesClientes;
	}

	/**
	 * Get: listaListarClubesClientes.
	 * 
	 * @return listaListarClubesClientes
	 */
	public List<ListarClubesClientesSaidaDTO> getListaListarClubesClientes() {
		return listaListarClubesClientes;
	}

	/**
	 * Set: listaListarClubesClientes.
	 * 
	 * @param listaListarClubesClientes
	 *            the lista listar clubes clientes
	 */
	public void setListaListarClubesClientes(
			List<ListarClubesClientesSaidaDTO> listaListarClubesClientes) {
		this.listaListarClubesClientes = listaListarClubesClientes;
	}

	/**
	 * Get: entradaListarClubesClientes.
	 * 
	 * @return entradaListarClubesClientes
	 */
	public ListarClubesClientesEntradaDTO getEntradaListarClubesClientes() {
		return entradaListarClubesClientes;
	}

	/**
	 * Set: entradaListarClubesClientes.
	 * 
	 * @param entradaListarClubesClientes
	 *            the entrada listar clubes clientes
	 */
	public void setEntradaListarClubesClientes(
			ListarClubesClientesEntradaDTO entradaListarClubesClientes) {
		this.entradaListarClubesClientes = entradaListarClubesClientes;
	}

	/**
	 * Get: flagDuplicacao.
	 * 
	 * @return flagDuplicacao
	 */
	public String getFlagDuplicacao() {
		return flagDuplicacao;
	}

	/**
	 * Set: flagDuplicacao.
	 * 
	 * @param flagDuplicacao
	 *            the flag duplicacao
	 */
	public void setFlagDuplicacao(String flagDuplicacao) {
		this.flagDuplicacao = flagDuplicacao;
	}

	/**
	 * Get: saidaContrato.
	 * 
	 * @return saidaContrato
	 */
	public ListarContratosPgitSaidaDTO getSaidaContrato() {
		return saidaContrato;
	}

	/**
	 * Set: saidaContrato.
	 * 
	 * @param saidaContrato
	 *            the saida contrato
	 */
	public void setSaidaContrato(ListarContratosPgitSaidaDTO saidaContrato) {
		this.saidaContrato = saidaContrato;
	}

	/**
	 * Is flag pesquisou por ident cliente.
	 * 
	 * @return true, if is flag pesquisou por ident cliente
	 */
	public boolean isFlagPesquisouPorIdentCliente() {
		return flagPesquisouPorIdentCliente;
	}

	/**
	 * Set: flagPesquisouPorIdentCliente.
	 * 
	 * @param flagPesquisouPorIdentCliente
	 *            the flag pesquisou por ident cliente
	 */
	public void setFlagPesquisouPorIdentCliente(
			boolean flagPesquisouPorIdentCliente) {
		this.flagPesquisouPorIdentCliente = flagPesquisouPorIdentCliente;
	}

	/**
	 * Get: comboService.
	 * 
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 * 
	 * @param comboService
	 *            the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: entradaConsultarListaClientePessoas.
	 * 
	 * @return entradaConsultarListaClientePessoas
	 */
	public ConsultarListaClientePessoasEntradaDTO getEntradaConsultarListaClientePessoas() {
		return entradaConsultarListaClientePessoas;
	}

	/**
	 * Set: entradaConsultarListaClientePessoas.
	 * 
	 * @param entradaConsultarListaClientePessoas
	 *            the entrada consultar lista cliente pessoas
	 */
	public void setEntradaConsultarListaClientePessoas(
			ConsultarListaClientePessoasEntradaDTO entradaConsultarListaClientePessoas) {
		this.entradaConsultarListaClientePessoas = entradaConsultarListaClientePessoas;
	}

	/**
	 * Get: entradaConsultarListaClientePessoasManutencao.
	 * 
	 * @return entradaConsultarListaClientePessoasManutencao
	 */
	public ConsultarListaClientePessoasEntradaDTO getEntradaConsultarListaClientePessoasManutencao() {
		return entradaConsultarListaClientePessoasManutencao;
	}

	/**
	 * Set: entradaConsultarListaClientePessoasManutencao.
	 * 
	 * @param entradaConsultarListaClientePessoasManutencao
	 *            the entrada consultar lista cliente pessoas manutencao
	 */
	public void setEntradaConsultarListaClientePessoasManutencao(
			ConsultarListaClientePessoasEntradaDTO entradaConsultarListaClientePessoasManutencao) {
		this.entradaConsultarListaClientePessoasManutencao = entradaConsultarListaClientePessoasManutencao;
	}

	/**
	 * Get: listaConsultarListaClientePessoas.
	 * 
	 * @return listaConsultarListaClientePessoas
	 */
	public List<ConsultarListaClientePessoasSaidaDTO> getListaConsultarListaClientePessoas() {
		return listaConsultarListaClientePessoas;
	}

	/**
	 * Set: listaConsultarListaClientePessoas.
	 * 
	 * @param listaConsultarListaClientePessoas
	 *            the lista consultar lista cliente pessoas
	 */
	public void setListaConsultarListaClientePessoas(
			List<ConsultarListaClientePessoasSaidaDTO> listaConsultarListaClientePessoas) {
		this.listaConsultarListaClientePessoas = listaConsultarListaClientePessoas;
	}

	/**
	 * Get: saidaConsultarListaClientePessoas.
	 * 
	 * @return saidaConsultarListaClientePessoas
	 */
	public ConsultarListaClientePessoasSaidaDTO getSaidaConsultarListaClientePessoas() {
		return saidaConsultarListaClientePessoas;
	}

	/**
	 * Set: saidaConsultarListaClientePessoas.
	 * 
	 * @param saidaConsultarListaClientePessoas
	 *            the saida consultar lista cliente pessoas
	 */
	public void setSaidaConsultarListaClientePessoas(
			ConsultarListaClientePessoasSaidaDTO saidaConsultarListaClientePessoas) {
		this.saidaConsultarListaClientePessoas = saidaConsultarListaClientePessoas;
	}

	/**
	 * Get: saidaConsultarListaClientePessoasManutencao.
	 * 
	 * @return saidaConsultarListaClientePessoasManutencao
	 */
	public ConsultarListaClientePessoasSaidaDTO getSaidaConsultarListaClientePessoasManutencao() {
		return saidaConsultarListaClientePessoasManutencao;
	}

	/**
	 * Set: saidaConsultarListaClientePessoasManutencao.
	 * 
	 * @param saidaConsultarListaClientePessoasManutencao
	 *            the saida consultar lista cliente pessoas manutencao
	 */
	public void setSaidaConsultarListaClientePessoasManutencao(
			ConsultarListaClientePessoasSaidaDTO saidaConsultarListaClientePessoasManutencao) {
		this.saidaConsultarListaClientePessoasManutencao = saidaConsultarListaClientePessoasManutencao;
	}

	/**
	 * Get: filtroIdentificaoService.
	 * 
	 * @return filtroIdentificaoService
	 */
	public IFiltroIdentificaoService getFiltroIdentificaoService() {
		return filtroIdentificaoService;
	}

	/**
	 * Set: filtroIdentificaoService.
	 * 
	 * @param filtroIdentificaoService
	 *            the filtro identificao service
	 */
	public void setFiltroIdentificaoService(
			IFiltroIdentificaoService filtroIdentificaoService) {
		this.filtroIdentificaoService = filtroIdentificaoService;
	}

	/**
	 * Identificacao cliente contrato bean.
	 */
	public IdentificacaoClienteContratoBean() {
	}

	/**
	 * Set: itemRadioFiltroSelecionado.
	 * 
	 * @param itemRadioFiltroSelecionado
	 *            the item radio filtro selecionado
	 */
	public void setItemRadioFiltroSelecionado(String itemRadioFiltroSelecionado) {
		this.itemRadioFiltroSelecionado = itemRadioFiltroSelecionado;
	}

	/**
	 * Get: itemRadioFiltroSelecionado.
	 * 
	 * @return itemRadioFiltroSelecionado
	 */
	public String getItemRadioFiltroSelecionado() {
		return itemRadioFiltroSelecionado;
	}

	/**
	 * Is bloqueia radio.
	 * 
	 * @return true, if is bloqueia radio
	 */
	public boolean isBloqueiaRadio() {
		return bloqueiaRadio;
	}

	/**
	 * Set: bloqueiaRadio.
	 * 
	 * @param bloqueiaRadio
	 *            the bloqueia radio
	 */
	public void setBloqueiaRadio(boolean bloqueiaRadio) {
		this.bloqueiaRadio = bloqueiaRadio;
	}

	/**
	 * Get: listaEmpresaGestora.
	 * 
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 * 
	 * @param listaEmpresaGestora
	 *            the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Get: listaTipoContrato.
	 * 
	 * @return listaTipoContrato
	 */
	public List<SelectItem> getListaTipoContrato() {
		return listaTipoContrato;
	}

	/**
	 * Set: listaTipoContrato.
	 * 
	 * @param listaTipoContrato
	 *            the lista tipo contrato
	 */
	public void setListaTipoContrato(List<SelectItem> listaTipoContrato) {
		this.listaTipoContrato = listaTipoContrato;
	}

	/**
	 * Get: itemFiltroSelecionado.
	 * 
	 * @return itemFiltroSelecionado
	 */
	public String getItemFiltroSelecionado() {
		return itemFiltroSelecionado;
	}

	/**
	 * Set: itemFiltroSelecionado.
	 * 
	 * @param itemFiltroSelecionado
	 *            the item filtro selecionado
	 */
	public void setItemFiltroSelecionado(String itemFiltroSelecionado) {
		this.itemFiltroSelecionado = itemFiltroSelecionado;
	}

	/**
	 * Get: listaMotivoSituacao.
	 * 
	 * @return listaMotivoSituacao
	 */
	public List<SelectItem> getListaMotivoSituacao() {
		return listaMotivoSituacao;
	}

	/**
	 * Set: listaMotivoSituacao.
	 * 
	 * @param listaMotivoSituacao
	 *            the lista motivo situacao
	 */
	public void setListaMotivoSituacao(List<SelectItem> listaMotivoSituacao) {
		this.listaMotivoSituacao = listaMotivoSituacao;
	}

	/**
	 * Get: listaMotivoSituacaoHash.
	 * 
	 * @return listaMotivoSituacaoHash
	 */
	public Map<Integer, String> getListaMotivoSituacaoHash() {
		return listaMotivoSituacaoHash;
	}

	/**
	 * Set lista motivo situacao hash.
	 * 
	 * @param listaMotivoSituacaoHash
	 *            the lista motivo situacao hash
	 */
	public void setListaMotivoSituacaoHash(
			Map<Integer, String> listaMotivoSituacaoHash) {
		this.listaMotivoSituacaoHash = listaMotivoSituacaoHash;
	}

	/**
	 * Get: listaSituacaoContrato.
	 * 
	 * @return listaSituacaoContrato
	 */
	public List<SelectItem> getListaSituacaoContrato() {
		return listaSituacaoContrato;
	}

	/**
	 * Set: listaSituacaoContrato.
	 * 
	 * @param listaSituacaoContrato
	 *            the lista situacao contrato
	 */
	public void setListaSituacaoContrato(List<SelectItem> listaSituacaoContrato) {
		this.listaSituacaoContrato = listaSituacaoContrato;
	}

	/**
	 * Get: itemClienteSelecionado.
	 * 
	 * @return itemClienteSelecionado
	 */
	public Integer getItemClienteSelecionado() {
		return itemClienteSelecionado;
	}

	/**
	 * Set: itemClienteSelecionado.
	 * 
	 * @param itemClienteSelecionado
	 *            the item cliente selecionado
	 */
	public void setItemClienteSelecionado(Integer itemClienteSelecionado) {
		this.itemClienteSelecionado = itemClienteSelecionado;
	}

	/**
	 * Get: paginaCliente.
	 * 
	 * @return paginaCliente
	 */
	public String getPaginaCliente() {
		return paginaCliente;
	}

	/**
	 * Set: paginaCliente.
	 * 
	 * @param paginaCliente
	 *            the pagina cliente
	 */
	public void setPaginaCliente(String paginaCliente) {
		this.paginaCliente = paginaCliente;
	}

	/**
	 * Get: paginaRetorno.
	 * 
	 * @return paginaRetorno
	 */
	public String getPaginaRetorno() {
		return paginaRetorno;
	}

	/**
	 * Set: paginaRetorno.
	 * 
	 * @param paginaRetorno
	 *            the pagina retorno
	 */
	public void setPaginaRetorno(String paginaRetorno) {
		this.paginaRetorno = paginaRetorno;
	}

	/**
	 * Set: itemSelecionadoListaCliente.
	 * 
	 * @param itemSelecionadoListaCliente
	 *            the item selecionado lista cliente
	 */
	public void setItemSelecionadoListaCliente(
			Integer itemSelecionadoListaCliente) {
		this.itemSelecionadoListaCliente = itemSelecionadoListaCliente;
	}

	/**
	 * Get: itemSelecionadoListaCliente.
	 * 
	 * @return itemSelecionadoListaCliente
	 */
	public Integer getItemSelecionadoListaCliente() {
		return itemSelecionadoListaCliente;
	}

	/**
	 * Set: listaControleRadioConsultarCliente.
	 * 
	 * @param listaControleRadioConsultarCliente
	 *            the lista controle radio consultar cliente
	 */
	public void setListaControleRadioConsultarCliente(
			List<SelectItem> listaControleRadioConsultarCliente) {
		this.listaControleRadioConsultarCliente = listaControleRadioConsultarCliente;
	}

	/**
	 * Get: listaControleRadioConsultarCliente.
	 * 
	 * @return listaControleRadioConsultarCliente
	 */
	public List<SelectItem> getListaControleRadioConsultarCliente() {
		return listaControleRadioConsultarCliente;
	}

	/**
	 * Set: saidaConsultarContratoClienteFiltro.
	 * 
	 * @param saidaConsultarContratoClienteFiltro
	 *            the saida consultar contrato cliente filtro
	 */
	public void setSaidaConsultarContratoClienteFiltro(
			ConsultarContratoClienteFiltroSaidaDTO saidaConsultarContratoClienteFiltro) {
		this.saidaConsultarContratoClienteFiltro = saidaConsultarContratoClienteFiltro;
	}

	/**
	 * Get: saidaConsultarContratoClienteFiltro.
	 * 
	 * @return saidaConsultarContratoClienteFiltro
	 */
	public ConsultarContratoClienteFiltroSaidaDTO getSaidaConsultarContratoClienteFiltro() {
		return saidaConsultarContratoClienteFiltro;
	}

	/**
	 * Set: listaSaidaConsultarContratoClienteFiltro.
	 * 
	 * @param listaSaidaConsultarContratoClienteFiltro
	 *            the lista saida consultar contrato cliente filtro
	 */
	public void setListaSaidaConsultarContratoClienteFiltro(
			List<ConsultarContratoClienteFiltroSaidaDTO> listaSaidaConsultarContratoClienteFiltro) {
		this.listaSaidaConsultarContratoClienteFiltro = listaSaidaConsultarContratoClienteFiltro;
	}

	/**
	 * Get: listaSaidaConsultarContratoClienteFiltro.
	 * 
	 * @return listaSaidaConsultarContratoClienteFiltro
	 */
	public List<ConsultarContratoClienteFiltroSaidaDTO> getListaSaidaConsultarContratoClienteFiltro() {
		return listaSaidaConsultarContratoClienteFiltro;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoImpl.
	 * 
	 * @return filtroAgendamentoEfetivacaoEstornoImpl
	 */
	public IFiltroAgendamentoEfetivacaoEstornoService getFiltroAgendamentoEfetivacaoEstornoImpl() {
		return filtroAgendamentoEfetivacaoEstornoImpl;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoImpl.
	 * 
	 * @param filtroAgendamentoEfetivacaoEstornoImpl
	 *            the filtro agendamento efetivacao estorno impl
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoImpl(
			IFiltroAgendamentoEfetivacaoEstornoService filtroAgendamentoEfetivacaoEstornoImpl) {
		this.filtroAgendamentoEfetivacaoEstornoImpl = filtroAgendamentoEfetivacaoEstornoImpl;
	}

	/**
	 * Get: empresaGestoraContrato.
	 * 
	 * @return empresaGestoraContrato
	 */
	public String getEmpresaGestoraContrato() {
		return empresaGestoraContrato;
	}

	/**
	 * Set: empresaGestoraContrato.
	 * 
	 * @param empresaGestoraContrato
	 *            the empresa gestora contrato
	 */
	public void setEmpresaGestoraContrato(String empresaGestoraContrato) {
		this.empresaGestoraContrato = empresaGestoraContrato;
	}

	/**
	 * Get: numeroContrato.
	 * 
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 * 
	 * @param numeroContrato
	 *            the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: descricaoContrato.
	 * 
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 * 
	 * @param descricaoContrato
	 *            the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: situacaoContrato.
	 * 
	 * @return situacaoContrato
	 */
	public String getSituacaoContrato() {
		return situacaoContrato;
	}

	/**
	 * Set: situacaoContrato.
	 * 
	 * @param situacaoContrato
	 *            the situacao contrato
	 */
	public void setSituacaoContrato(String situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	/**
	 * Set: listaConsultarListaContratosPessoas.
	 * 
	 * @param listaConsultarListaContratosPessoas
	 *            the lista consultar lista contratos pessoas
	 */
	public void setListaConsultarListaContratosPessoas(
			List<ConsultarListaContratosPessoasSaidaDTO> listaConsultarListaContratosPessoas) {
		this.listaConsultarListaContratosPessoas = listaConsultarListaContratosPessoas;
	}

	/**
	 * Get: listaConsultarListaContratosPessoas.
	 * 
	 * @return listaConsultarListaContratosPessoas
	 */
	public List<ConsultarListaContratosPessoasSaidaDTO> getListaConsultarListaContratosPessoas() {
		return listaConsultarListaContratosPessoas;
	}

	/**
	 * Get: numeroContratoCliente.
	 * 
	 * @return numeroContratoCliente
	 */
	public String getNumeroContratoCliente() {
		return numeroContratoCliente;
	}

	/**
	 * Set: numeroContratoCliente.
	 * 
	 * @param numeroContratoCliente
	 *            the numero contrato cliente
	 */
	public void setNumeroContratoCliente(String numeroContratoCliente) {
		this.numeroContratoCliente = numeroContratoCliente;
	}

	/**
	 * Get: descricaoContratoCliente.
	 * 
	 * @return descricaoContratoCliente
	 */
	public String getDescricaoContratoCliente() {
		return descricaoContratoCliente;
	}

	/**
	 * Set: descricaoContratoCliente.
	 * 
	 * @param descricaoContratoCliente
	 *            the descricao contrato cliente
	 */
	public void setDescricaoContratoCliente(String descricaoContratoCliente) {
		this.descricaoContratoCliente = descricaoContratoCliente;
	}

	/**
	 * Get: situacaoContratoCliente.
	 * 
	 * @return situacaoContratoCliente
	 */
	public String getSituacaoContratoCliente() {
		return situacaoContratoCliente;
	}

	/**
	 * Set: situacaoContratoCliente.
	 * 
	 * @param situacaoContratoCliente
	 *            the situacao contrato cliente
	 */
	public void setSituacaoContratoCliente(String situacaoContratoCliente) {
		this.situacaoContratoCliente = situacaoContratoCliente;
	}

	/**
	 * Set: empresaGestoraContratoCliente.
	 * 
	 * @param empresaGestoraContratoCliente
	 *            the empresa gestora contrato cliente
	 */
	public void setEmpresaGestoraContratoCliente(
			String empresaGestoraContratoCliente) {
		this.empresaGestoraContratoCliente = empresaGestoraContratoCliente;
	}

	/**
	 * Get: empresaGestoraContratoCliente.
	 * 
	 * @return empresaGestoraContratoCliente
	 */
	public String getEmpresaGestoraContratoCliente() {
		return empresaGestoraContratoCliente;
	}

	/**
	 * Set: solicitacaoRecuperacaoRemessaBean.
	 * 
	 * @param solicitacaoRecuperacaoRemessaBean
	 *            the solicitacao recuperacao remessa bean
	 */
	public void setSolicitacaoRecuperacaoRemessaBean(
			SolicitacaoRecuperacaoRemessaBean solicitacaoRecuperacaoRemessaBean) {
		this.solicitacaoRecuperacaoRemessaBean = solicitacaoRecuperacaoRemessaBean;
	}

	/**
	 * Get: solicitacaoRecuperacaoRemessaBean.
	 * 
	 * @return solicitacaoRecuperacaoRemessaBean
	 */
	public SolicitacaoRecuperacaoRemessaBean getSolicitacaoRecuperacaoRemessaBean() {
		return solicitacaoRecuperacaoRemessaBean;
	}

	/**
	 * Get: cdPessoaJuridicaContrato.
	 * 
	 * @return cdPessoaJuridicaContrato
	 */
	public Long getCdPessoaJuridicaContrato() {
		return cdPessoaJuridicaContrato;
	}

	/**
	 * Set: cdPessoaJuridicaContrato.
	 * 
	 * @param cdPessoaJuridicaContrato
	 *            the cd pessoa juridica contrato
	 */
	public void setCdPessoaJuridicaContrato(Long cdPessoaJuridicaContrato) {
		this.cdPessoaJuridicaContrato = cdPessoaJuridicaContrato;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 * 
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 * 
	 * @param cdTipoContratoNegocio
	 *            the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: nrSequenciaContratoNegocio.
	 * 
	 * @return nrSequenciaContratoNegocio
	 */
	public Long getNrSequenciaContratoNegocio() {
		return nrSequenciaContratoNegocio;
	}

	/**
	 * Set: nrSequenciaContratoNegocio.
	 * 
	 * @param nrSequenciaContratoNegocio
	 *            the nr sequencia contrato negocio
	 */
	public void setNrSequenciaContratoNegocio(Long nrSequenciaContratoNegocio) {
		this.nrSequenciaContratoNegocio = nrSequenciaContratoNegocio;
	}

	/**
	 * Set: listaSaidaConsultarListaContratosPessoas.
	 * 
	 * @param listaSaidaConsultarListaContratosPessoas
	 *            the lista saida consultar lista contratos pessoas
	 */
	public void setListaSaidaConsultarListaContratosPessoas(
			List<ConsultarListaContratosPessoasSaidaDTO> listaSaidaConsultarListaContratosPessoas) {
		this.listaSaidaConsultarListaContratosPessoas = listaSaidaConsultarListaContratosPessoas;
	}

	/**
	 * Get: listaSaidaConsultarListaContratosPessoas.
	 * 
	 * @return listaSaidaConsultarListaContratosPessoas
	 */
	public List<ConsultarListaContratosPessoasSaidaDTO> getListaSaidaConsultarListaContratosPessoas() {
		return listaSaidaConsultarListaContratosPessoas;
	}

	/**
	 * Get: saidaConsultarListaContratosPessoas.
	 * 
	 * @return saidaConsultarListaContratosPessoas
	 */
	public ConsultarListaContratosPessoasSaidaDTO getSaidaConsultarListaContratosPessoas() {
		return saidaConsultarListaContratosPessoas;
	}

	/**
	 * Set: saidaConsultarListaContratosPessoas.
	 * 
	 * @param saidaConsultarListaContratosPessoas
	 *            the saida consultar lista contratos pessoas
	 */
	public void setSaidaConsultarListaContratosPessoas(
			ConsultarListaContratosPessoasSaidaDTO saidaConsultarListaContratosPessoas) {
		this.saidaConsultarListaContratosPessoas = saidaConsultarListaContratosPessoas;
	}

}