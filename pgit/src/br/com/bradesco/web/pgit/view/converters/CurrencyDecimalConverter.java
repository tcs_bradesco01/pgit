/*
 * Nome: br.com.bradesco.web.pgit.view.converters
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 07/10/2015
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.converters;

import java.math.BigDecimal;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import br.com.bradesco.web.pgit.utils.PgitUtil;

/**
 * Nome: CurrencyDecimalConverter
 * <p>
 * Prop�sito:
 * </p>
 * 	
 * @author : todo!
 * 
 * @version :
 */
public class CurrencyDecimalConverter implements Converter {

    /**
     * (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsObject(
     * javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
     */
    public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
        throw new UnsupportedOperationException();
    }

    /**
     * (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(
     * javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
        if (value instanceof BigDecimal) {
            return PgitUtil.formatarValorDinheiro((BigDecimal) value);
        }
        return "";
    }

}
