/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.solrecpagvennaopagos.bean
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.solrecpagvennaopagos.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.ISolRecPagVenNaoPagosService;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.ConsultarPagotsVencNaoPagosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.ConsultarPagotsVencNaoPagosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.OcorrenciasDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.SolicitarRecPagtosVencidosNaoPagosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solrecpagvennaopagos.bean.SolicitarRecPagtosVencidosNaoPagosSaidaDTO;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: SolRecPagVenNaoPagosBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class SolRecPagVenNaoPagosBean extends PagamentosBean {
	/* Camada de Implementa��o */
	/** Atributo solRecPagVenNaoPagosServiceImpl. */
	private ISolRecPagVenNaoPagosService solRecPagVenNaoPagosServiceImpl;

	/** Atributo FLAG_PRIMEIRA_SOLICITACAO. */
	private static final String FLAG_PRIMEIRA_SOLICITACAO = "FLAG_PRIMEIRA_SOLICITACAO";
	
	/** Atributo CODIGO_SOLICITACAO. */
	private static final String CODIGO_SOLICITACAO = "CODIGO_SOLICITACAO";
	
	/** Atributo NUMERO_SOLICITACAO. */
	private static final String NUMERO_SOLICITACAO = "NUMERO_SOLICITACAO";

	/** Atributo listaGridConsultar. */
	private List<ConsultarPagotsVencNaoPagosSaidaDTO> listaGridConsultar;
	
	/** Atributo listaGridRecuperarSelecionados. */
	private List<ConsultarPagotsVencNaoPagosSaidaDTO> listaGridRecuperarSelecionados;

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;
	
	/** Atributo panelBotoes. */
	private boolean panelBotoes;
	
	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos;
	
	/** Atributo listaControle. */
	private List<SelectItem> listaControle;

	// Variaveis Cliente
	/** Atributo cpfCnpj. */
	private String cpfCnpj;
	
	/** Atributo nomeRazao. */
	private String nomeRazao;
	
	/** Atributo empresaConglomerado. */
	private String empresaConglomerado;
	
	/** Atributo numeroPagamento. */
	private String numeroPagamento;
	
	/** Atributo numeroContrato. */
	private String numeroContrato;
	
	/** Atributo descricaoContrato. */
	private String descricaoContrato;
	
	/** Atributo situacao. */
	private String situacao;

	// Variaveis Conta Debito
	/** Atributo banco. */
	private String banco;
	
	/** Atributo agencia. */
	private String agencia;
	
	/** Atributo conta. */
	private String conta;
	
	/** Atributo tipo. */
	private String tipo;

	/** Atributo qtdePagtosSeremRecuperados. */
	private Integer qtdePagtosSeremRecuperados;

	/** Atributo vlrTotalPagtosSeremRecuperados. */
	private BigDecimal vlrTotalPagtosSeremRecuperados;

	/** Atributo dataQuitacaoConfirmar. */
	private String dataQuitacaoConfirmar;

	/** Atributo dataQuitacao. */
	private Date dataQuitacao;

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		// Tela de Filtro
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno(
				"conSolRecPagVenNaoPagos");

		limparCampos();
		setListaGridConsultar(null);
		setDisableArgumentosConsulta(false);

		// Carregamento dos combos
		listarTipoServicoPagto();
		listarInscricaoFavorecido();
		listarConsultarMotivoSituacaoPagamentoSolicitacao();

		setHabilitaArgumentosPesquisa(false);
		setPanelBotoes(false);
		setOpcaoChecarTodos(false);

		getFiltroAgendamentoEfetivacaoEstornoBean()
				.setClienteContratoSelecionado(false);
		getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);
		getFiltroAgendamentoEfetivacaoEstornoBean().setVoltarParticipante(
				"conSolRecPagVenNaoPagos");

		PgitUtil.setSessionAttribute(FLAG_PRIMEIRA_SOLICITACAO, "S");
	}

	/**
	 * Limpar tela principal.
	 *
	 * @return the string
	 */
	public String limparTelaPrincipal() {
		// Limpar Argumentos de Pesquisa e Lista
		limparFiltroPrincipal();
		setListaGridConsultar(null);
		setPanelBotoes(false);
		setOpcaoChecarTodos(false);

		return "";
	}

	/**
	 * Voltar consultar.
	 *
	 * @return the string
	 */
	public String voltarConsultar() {
		limparSelecionados();

		return "conSolRecPagVenNaoPagos";
	}

	/**
	 * Avancar confirmar.
	 *
	 * @return the string
	 */
	public String avancarConfirmar() {

		setDataQuitacaoConfirmar(FormatarData
				.formataDiaMesAno(getDataQuitacao()));
		return "recSolRecPagVencNaoPagConf";
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
		setListaGridConsultar(null);

		limparCampos();

		setPanelBotoes(false);
		setOpcaoChecarTodos(false);

		return "";
	}

	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar() {
		limparFiltroPrincipal();
		setListaGridConsultar(null);
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(
				null);
		setDisableArgumentosConsulta(false);
		setPanelBotoes(false);
		setOpcaoChecarTodos(false);

		return "";
	}

	/**
	 * Pesquisar.
	 */
	public void pesquisar() {
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
		for (int i = 0; i < getListaGridConsultar().size(); i++) {
			getListaGridConsultar().get(i).setCheck(false);
		}
	}

	/**
	 * Consultar.
	 *
	 * @param evt the evt
	 */
	public void consultar(ActionEvent evt) {
		consultar();
	}

	/**
	 * Consultar.
	 *
	 * @return the string
	 */
	public String consultar() {
		try {

			PgitUtil.setSessionAttribute(FLAG_PRIMEIRA_SOLICITACAO, "S");
			setDisableArgumentosConsulta(true);
			ConsultarPagotsVencNaoPagosEntradaDTO entrada = new ConsultarPagotsVencNaoPagosEntradaDTO();

			/*
			 * Se selecionado �Filtrar por Cliente� ou �Filtrar por Contrato�
			 * mover �1�. Se selecionado �Filtrar por Conta D�bito� mover �2�;
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("0")
					|| getFiltroAgendamentoEfetivacaoEstornoBean()
							.getTipoFiltroSelecionado().equals("1")) {
				entrada.setCdTipoPesquisa(1);
			} else {
				entrada.setCdTipoPesquisa(2);
			}

			entrada.setCdAgendadosPagosNaoPagos(2);

			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("0")
					|| getFiltroAgendamentoEfetivacaoEstornoBean()
							.getTipoFiltroSelecionado().equals("1")) {
				/*
				 * >> Se Selecionado Filtrar por Cliente Mover Valores vindos da
				 * lista >> Se Selecionado Filtrar por Contrato Passar valor
				 * retornado da lista(Verificado por email), na especifica��o
				 * definia passar os combo e inputtext, o que poderia gerar
				 * problemas caso o usu�rio altere os valores do combo ap�s ter
				 * feito a pesquisa
				 */
				entrada
						.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdPessoaJuridicaContrato());
				entrada
						.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdTipoContratoNegocio());
				entrada
						.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio());
			}

			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("2")) {
				/*
				 * Se selecionado �Filtrar por Conta D�bito� mover zeros para os
				 * cambos abaixo
				 */
				entrada.setCdPessoaJuridicaContrato(0L);
				entrada.setCdTipoContratoNegocio(0);
				entrada.setNrSequenciaContratoNegocio(0L);
			}

			/*
			 * Se estiver selecionado "Filtrar por Conta D�bito", passar os
			 * campos deste filtro Se estiver selecionado outro tipo de filtro,
			 * verificar se o "Conta D�bito" dos argumentos de pesquisa esta
			 * marcado Se estiver marcado, passar os valores correspondentes da
			 * tela,caso contr�rio passar zeros
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("2")) {
				entrada.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getBancoContaDebitoFiltro());
				entrada
						.setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getAgenciaContaDebitoBancariaFiltro());
				entrada.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getContaBancariaContaDebitoFiltro());
				entrada.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
			} else {
				if (isChkContaDebito()) {
					entrada.setCdBanco(getCdBancoContaDebito());
					entrada.setCdAgencia(getCdAgenciaBancariaContaDebito());
					entrada.setCdConta(getCdContaBancariaContaDebito());
					entrada.setCdDigitoConta(getCdDigitoContaContaDebito());
				} else {
					entrada.setCdBanco(0);
					entrada.setCdAgencia(0);
					entrada.setCdConta(0L);
					entrada.setCdDigitoConta("00");
				}
			}

			entrada.setDtCreditoPagamentoInicio(FormatarData
					.formataDiaMesAnoToPdc(getDataInicialPagamentoFiltro()));
			entrada.setDtCreditoPagamentoFim(FormatarData
					.formataDiaMesAnoToPdc(getDataFinalPagamentoFiltro()));

			entrada
					.setNrArquivoRemssaPagamento(getNumeroRemessaFiltro()
							.equals("") ? 0L : Long
							.parseLong(getNumeroRemessaFiltro()));

			if (isChkParticipanteContrato()) {
				entrada
						.setCdParticipante(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCodigoParticipanteFiltro());
			} else {
				entrada.setCdParticipante(0L);
			}

			entrada.setCdProdutoServicoOperacao(getTipoServicoFiltro());
			entrada.setCdProdutoServicoRelacionado(getModalidadeFiltro());
			entrada.setCdControlePagamentoDe(getNumeroPagamentoDeFiltro());
			entrada.setCdControlePagamentoAte(getNumeroPagamentoDeFiltro());
			entrada.setVlPagamentoDe(getValorPagamentoDeFiltro());
			entrada.setVlPagamentoAte(getValorPagamentoAteFiltro());
			entrada
					.setCdSituacaoOperacaoPagamento(getCboSituacaoPagamentoFiltro());
			entrada.setCdMotivoSituacaoPagamento(getCboMotivoSituacaoFiltro());
			entrada.setCdBarraDocumento(PgitUtil
					.verificaStringNula(getLinhaDigitavel1())
					+ PgitUtil.verificaStringNula(getLinhaDigitavel2())
					+ PgitUtil.verificaStringNula(getLinhaDigitavel3())
					+ PgitUtil.verificaStringNula(getLinhaDigitavel4())
					+ PgitUtil.verificaStringNula(getLinhaDigitavel5())
					+ PgitUtil.verificaStringNula(getLinhaDigitavel6())
					+ PgitUtil.verificaStringNula(getLinhaDigitavel7())
					+ PgitUtil.verificaStringNula(getLinhaDigitavel8()));
			entrada.setCdFavorecidoClientePagador(!getCodigoFavorecidoFiltro()
					.equals("") ? Long.parseLong(getCodigoFavorecidoFiltro())
					: 0L);
			entrada
					.setCdInscricaoFavorecido(!getInscricaoFiltro().equals("") ? Long
							.parseLong(getInscricaoFiltro())
							: 0L);
			entrada
					.setCdIdentificacaoInscricaoFavorecido(getTipoInscricaoFiltro());
			if (getRadioPesquisaFiltroPrincipal() != null
					&& getRadioPesquisaFiltroPrincipal().equals("3")
					&& getRadioFavorecidoFiltro() != null
					&& getRadioFavorecidoFiltro().equals("2")) {
				entrada
						.setCdBancoBeneficiario(getCdBancoContaCredito() != null ? getCdBancoContaCredito()
								: 0);
				entrada
						.setCdAgenciaBeneficiario(getCdAgenciaBancariaContaCredito() != null ? getCdAgenciaBancariaContaCredito()
								: 0);
			} else {
				entrada.setCdBancoBeneficiario(0);
				entrada.setCdAgenciaBeneficiario(0);
			}
			entrada.setCdContaBeneficiario(getCdContaBancariaContaCredito());
			entrada
					.setCdDigitoContaBeneficiario(getCdDigitoContaCredito() != null ? PgitUtil
							.complementaDigito(getCdDigitoContaCredito(), 2)
							: "00");
			entrada.setNrUnidadeOrganizacional(0);
			entrada.setCdBeneficio(0L);
			entrada.setCdEspecieBeneficioInss(0);
			entrada
					.setCdClassificacao(getClassificacaoFiltro() != null ? getClassificacaoFiltro()
							: 000000000);

			// conforme solicitado via email por Kelli Cristina dia 12/04/2011,
			// retirar o combo Tipo Conta da tela e passar os campos zerados
			// para o pdc
			entrada.setCdEmpresaContrato(0L);
			entrada.setCdTipoConta(0);
			entrada.setNrContrato(0L);

			setListaGridConsultar(getSolRecPagVenNaoPagosServiceImpl()
					.consultarPagotsVencNaoPagos(entrada));
			setPanelBotoes(false);
			setOpcaoChecarTodos(false);

			listaControle = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridConsultar().size(); i++) {
				listaControle.add(new SelectItem(i, ""));
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridConsultar(null);
			setDisableArgumentosConsulta(false);
			setPanelBotoes(false);
			setOpcaoChecarTodos(false);
		}

		return "";
	}

	/**
	 * Limpar selecionados.
	 */
	public void limparSelecionados() {
		for (int i = 0; i < getListaGridConsultar().size(); i++) {
			getListaGridConsultar().get(i).setCheck(false);
		}

		setPanelBotoes(false);
		setOpcaoChecarTodos(false);
	}

	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes() {
		for (int i = 0; i < getListaGridConsultar().size(); i++) {
			if (getListaGridConsultar().get(i).isCheck()) {
				setPanelBotoes(true);
				return;
			}
		}
		setPanelBotoes(false);
	}

	/**
	 * Voltar recuperacao.
	 *
	 * @return the string
	 */
	public String voltarRecuperacao() {
		return "recSolRecPagVencNaoPag";
	}

	/**
	 * Confirmar recuperacao.
	 *
	 * @return the string
	 */
	public String confirmarRecuperacao() {
		try {

			Integer codigo = (Integer) PgitUtil
					.getSessionAttribute(CODIGO_SOLICITACAO);
			Integer numero = (Integer) PgitUtil
					.getSessionAttribute(NUMERO_SOLICITACAO);

			SolicitarRecPagtosVencidosNaoPagosEntradaDTO entrada = new SolicitarRecPagtosVencidosNaoPagosEntradaDTO();

			entrada.setDtQuitacao(FormatarData
					.formataDiaMesAnoToPdc(getDataQuitacao()));
			entrada.setNrPagamento(getListaGridRecuperarSelecionados().size());
			entrada.setFlagPrimeiraSolicitacao((String) PgitUtil
					.getSessionAttribute(FLAG_PRIMEIRA_SOLICITACAO));
			entrada.setCdSolicitacaoPagamento(codigo == null ? 0 : codigo);
			entrada.setNrSolicitacaoPagamento(numero == null ? 0 : numero);
			entrada.setCdProduto(getListaGridRecuperarSelecionados().get(0)
					.getCdProdutoServicoOperacao());
			entrada.setCdProdutoRelacionado(getListaGridRecuperarSelecionados()
					.get(0).getCdProdutoServicoRelacionado());

			List<OcorrenciasDTO> listaSelecionados = new ArrayList<OcorrenciasDTO>();
			OcorrenciasDTO registro;
			for (int i = 0; i < getListaGridRecuperarSelecionados().size(); i++) {
				registro = new OcorrenciasDTO();

				registro
						.setCdControlePagamento(getListaGridRecuperarSelecionados()
								.get(i).getCdControlePagamento());
				registro
						.setCdModalidadePagamento(getListaGridRecuperarSelecionados()
								.get(i).getCdTipoTela());
				registro
						.setCdPessoaJuridicaContrato(getListaGridRecuperarSelecionados()
								.get(i).getCdEmpresa());
				registro
						.setNrSequenciaContratoNegocio(getListaGridRecuperarSelecionados()
								.get(i).getNrContrato());
				registro
						.setCdTipoContratoNegocio(getListaGridRecuperarSelecionados()
								.get(i).getCdTipoContratoNegocio());
				registro
						.setDsEfetivacaoPagamento(getListaGridRecuperarSelecionados()
								.get(i).getDsEfetivacaoPagamento());

				listaSelecionados.add(registro);

			}

			entrada.setOcorrencias(listaSelecionados);

			SolicitarRecPagtosVencidosNaoPagosSaidaDTO saida = getSolRecPagVenNaoPagosServiceImpl()
					.solicitarRecPagtosVencidosNaoPagos(entrada);

			// Campos retirados conforme solicita��o via email dia 29/04/2011
			// por Rodrigo Fernando Mafioletti Staejak
			/*
			 * PgitUtil.setSessionAttribute(FLAG_PRIMEIRA_SOLICITACAO, "N");
			 * PgitUtil.setSessionAttribute(CODIGO_SOLICITACAO,
			 * saida.getCdSolicitacaoPagamento() );
			 * PgitUtil.setSessionAttribute(NUMERO_SOLICITACAO,
			 * saida.getNrSolicitacaoPagamento());
			 */

			BradescoFacesUtils.addInfoModalMessage("(" + saida.getCodMensagem()
					+ ") " + saida.getMensagem(), "conSolRecPagVenNaoPagos",
					"#{solRecPagVenNaoPagosBean.consultar}", false);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}

		return "";
	}

	/**
	 * Recuperar selecionados.
	 *
	 * @return the string
	 */
	public String recuperarSelecionados() {
		this.vlrTotalPagtosSeremRecuperados = BigDecimal.ZERO;
		this.qtdePagtosSeremRecuperados = null;
		setDataQuitacao(new Date());

		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(
				new PDCDataBean());

		limparVariaveis();
		carregaCabecalho();
		carregaSelecionados();

		return "recSolRecPagVencNaoPag";
	}

	/**
	 * Carrega cabecalho.
	 */
	private void carregaCabecalho() {

		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getTipoFiltroSelecionado().equals("0")) { // Filtrar por
															// cliente
			setCpfCnpj(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getNrCpfCnpjCliente());
			setNomeRazao(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getDescricaoRazaoSocialCliente());
			setEmpresaConglomerado(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getEmpresaGestoraDescCliente());
			setNumeroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getNumeroDescCliente());
			setDescricaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getDescricaoContratoDescCliente());
			setSituacao(getFiltroAgendamentoEfetivacaoEstornoBean()
					.getSituacaoDescCliente());
		} else {
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getTipoFiltroSelecionado().equals("1")) { // Filtrar por
																// contrato
				setEmpresaConglomerado(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getEmpresaGestoraDescContrato());
				setNumeroContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getNumeroDescContrato());
				setDescricaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getDescricaoContratoDescContrato());
				setSituacao(getFiltroAgendamentoEfetivacaoEstornoBean()
						.getSituacaoDescContrato());
			} else {
				if (getFiltroAgendamentoEfetivacaoEstornoBean()
						.getTipoFiltroSelecionado().equals("2")) { // Filtrar
																	// por Conta
																	// D�bito
					getFiltroAgendamentoEfetivacaoEstornoBean()
							.carregaDescricaoConta();
					setBanco(PgitUtil.concatenarCampos(
							getFiltroAgendamentoEfetivacaoEstornoBean()
									.getBancoContaDebitoFiltro(),
							getFiltroAgendamentoEfetivacaoEstornoBean()
									.getDescricaoBancoContaDebito()));
					setAgencia(PgitUtil.concatenarCampos(
							getFiltroAgendamentoEfetivacaoEstornoBean()
									.getAgenciaContaDebitoBancariaFiltro(),
							getFiltroAgendamentoEfetivacaoEstornoBean()
									.getDescricaoAgenciaContaDebitoBancaria()));
					setConta(PgitUtil.concatenarCampos(
							getFiltroAgendamentoEfetivacaoEstornoBean()
									.getContaBancariaContaDebitoFiltro(),
							getFiltroAgendamentoEfetivacaoEstornoBean()
									.getDigitoContaDebitoFiltro()));
					setTipo(getFiltroAgendamentoEfetivacaoEstornoBean()
							.getDescricaoTipoContaDebito());
				}
			}
		}
	}

	/**
	 * Carrega selecionados.
	 */
	private void carregaSelecionados() {
		listaGridRecuperarSelecionados = new ArrayList<ConsultarPagotsVencNaoPagosSaidaDTO>();

		for (int i = 0; i < getListaGridConsultar().size(); i++) {
			if (getListaGridConsultar().get(i).isCheck()) {
				listaGridRecuperarSelecionados.add(getListaGridConsultar().get(
						i));
				vlrTotalPagtosSeremRecuperados = vlrTotalPagtosSeremRecuperados
						.add(getListaGridConsultar().get(i)
								.getVlEfetivoPagamento());
			}
		}

		this.qtdePagtosSeremRecuperados = listaGridRecuperarSelecionados.size();
	}

	/**
	 * Limpar variaveis.
	 */
	private void limparVariaveis() {
		setCpfCnpj(null);
		setNomeRazao("");
		setEmpresaConglomerado("");
		setNumeroContrato("");
		setDescricaoContrato("");
		setSituacao("");
		setBanco("");
		setAgencia("");
		setConta(null);
		setTipo("");
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		limparCampos();
		setListaGridConsultar(null);
		setPanelBotoes(false);
		setOpcaoChecarTodos(false);
	}

	/**
	 * Pesquisar consulta.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarConsulta(ActionEvent evt) {
		consultar();
		return "";
	}

	/**
	 * Limpar grid.
	 *
	 * @return the string
	 */
	public String limparGrid() {
		setListaGridConsultar(null);
		setDisableArgumentosConsulta(false);
		setPanelBotoes(false);
		setOpcaoChecarTodos(false);

		return "";

	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		return "";
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Get: listaGridConsultar.
	 *
	 * @return listaGridConsultar
	 */
	public List<ConsultarPagotsVencNaoPagosSaidaDTO> getListaGridConsultar() {
		return listaGridConsultar;
	}

	/**
	 * Set: listaGridConsultar.
	 *
	 * @param listaGridConsultar the lista grid consultar
	 */
	public void setListaGridConsultar(
			List<ConsultarPagotsVencNaoPagosSaidaDTO> listaGridConsultar) {
		this.listaGridConsultar = listaGridConsultar;
	}

	/**
	 * Get: solRecPagVenNaoPagosServiceImpl.
	 *
	 * @return solRecPagVenNaoPagosServiceImpl
	 */
	public ISolRecPagVenNaoPagosService getSolRecPagVenNaoPagosServiceImpl() {
		return solRecPagVenNaoPagosServiceImpl;
	}

	/**
	 * Set: solRecPagVenNaoPagosServiceImpl.
	 *
	 * @param solRecPagVenNaoPagosServiceImpl the sol rec pag ven nao pagos service impl
	 */
	public void setSolRecPagVenNaoPagosServiceImpl(
			ISolRecPagVenNaoPagosService solRecPagVenNaoPagosServiceImpl) {
		this.solRecPagVenNaoPagosServiceImpl = solRecPagVenNaoPagosServiceImpl;
	}

	/**
	 * Is panel botoes.
	 *
	 * @return true, if is panel botoes
	 */
	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	/**
	 * Set: panelBotoes.
	 *
	 * @param panelBotoes the panel botoes
	 */
	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Get: listaControle.
	 *
	 * @return listaControle
	 */
	public List<SelectItem> getListaControle() {
		return listaControle;
	}

	/**
	 * Set: listaControle.
	 *
	 * @param listaControle the lista controle
	 */
	public void setListaControle(List<SelectItem> listaControle) {
		this.listaControle = listaControle;
	}

	/**
	 * Get: cpfCnpj.
	 *
	 * @return cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * Set: cpfCnpj.
	 *
	 * @param cpfCnpj the cpf cnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: empresaConglomerado.
	 *
	 * @return empresaConglomerado
	 */
	public String getEmpresaConglomerado() {
		return empresaConglomerado;
	}

	/**
	 * Set: empresaConglomerado.
	 *
	 * @param empresaConglomerado the empresa conglomerado
	 */
	public void setEmpresaConglomerado(String empresaConglomerado) {
		this.empresaConglomerado = empresaConglomerado;
	}

	/**
	 * Get: nomeRazao.
	 *
	 * @return nomeRazao
	 */
	public String getNomeRazao() {
		return nomeRazao;
	}

	/**
	 * Set: nomeRazao.
	 *
	 * @param nomeRazao the nome razao
	 */
	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: numeroPagamento.
	 *
	 * @return numeroPagamento
	 */
	public String getNumeroPagamento() {
		return numeroPagamento;
	}

	/**
	 * Set: numeroPagamento.
	 *
	 * @param numeroPagamento the numero pagamento
	 */
	public void setNumeroPagamento(String numeroPagamento) {
		this.numeroPagamento = numeroPagamento;
	}

	/**
	 * Get: situacao.
	 *
	 * @return situacao
	 */
	public String getSituacao() {
		return situacao;
	}

	/**
	 * Set: situacao.
	 *
	 * @param situacao the situacao
	 */
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	/**
	 * Get: dataQuitacao.
	 *
	 * @return dataQuitacao
	 */
	public Date getDataQuitacao() {
		return dataQuitacao;
	}

	/**
	 * Set: dataQuitacao.
	 *
	 * @param dataQuitacao the data quitacao
	 */
	public void setDataQuitacao(Date dataQuitacao) {
		this.dataQuitacao = dataQuitacao;
	}

	/**
	 * Get: listaGridRecuperarSelecionados.
	 *
	 * @return listaGridRecuperarSelecionados
	 */
	public List<ConsultarPagotsVencNaoPagosSaidaDTO> getListaGridRecuperarSelecionados() {
		return listaGridRecuperarSelecionados;
	}

	/**
	 * Set: listaGridRecuperarSelecionados.
	 *
	 * @param listaGridRecuperarSelecionados the lista grid recuperar selecionados
	 */
	public void setListaGridRecuperarSelecionados(
			List<ConsultarPagotsVencNaoPagosSaidaDTO> listaGridRecuperarSelecionados) {
		this.listaGridRecuperarSelecionados = listaGridRecuperarSelecionados;
	}

	/**
	 * Get: agencia.
	 *
	 * @return agencia
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * Set: agencia.
	 *
	 * @param agencia the agencia
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * Get: banco.
	 *
	 * @return banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * Set: banco.
	 *
	 * @param banco the banco
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}

	/**
	 * Get: conta.
	 *
	 * @return conta
	 */
	public String getConta() {
		return conta;
	}

	/**
	 * Set: conta.
	 *
	 * @param conta the conta
	 */
	public void setConta(String conta) {
		this.conta = conta;
	}

	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Get: dataQuitacaoConfirmar.
	 *
	 * @return dataQuitacaoConfirmar
	 */
	public String getDataQuitacaoConfirmar() {
		return dataQuitacaoConfirmar;
	}

	/**
	 * Set: dataQuitacaoConfirmar.
	 *
	 * @param dataQuitacaoConfirmar the data quitacao confirmar
	 */
	public void setDataQuitacaoConfirmar(String dataQuitacaoConfirmar) {
		this.dataQuitacaoConfirmar = dataQuitacaoConfirmar;
	}

	/**
	 * Get: qtdePagtosSeremRecuperados.
	 *
	 * @return qtdePagtosSeremRecuperados
	 */
	public Integer getQtdePagtosSeremRecuperados() {
		return qtdePagtosSeremRecuperados;
	}

	/**
	 * Set: qtdePagtosSeremRecuperados.
	 *
	 * @param qtdePagtosSeremRecuperados the qtde pagtos serem recuperados
	 */
	public void setQtdePagtosSeremRecuperados(Integer qtdePagtosSeremRecuperados) {
		this.qtdePagtosSeremRecuperados = qtdePagtosSeremRecuperados;
	}

	/**
	 * Get: vlrTotalPagtosSeremRecuperados.
	 *
	 * @return vlrTotalPagtosSeremRecuperados
	 */
	public BigDecimal getVlrTotalPagtosSeremRecuperados() {
		return vlrTotalPagtosSeremRecuperados;
	}

	/**
	 * Set: vlrTotalPagtosSeremRecuperados.
	 *
	 * @param vlrTotalPagtosSeremRecuperados the vlr total pagtos serem recuperados
	 */
	public void setVlrTotalPagtosSeremRecuperados(
			BigDecimal vlrTotalPagtosSeremRecuperados) {
		this.vlrTotalPagtosSeremRecuperados = vlrTotalPagtosSeremRecuperados;
	}

}
