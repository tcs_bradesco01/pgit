/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.liberarpagtosindividuaispend
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.liberarpagtosindividuaispend;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.session.bean.PDCDataBean;
import br.com.bradesco.web.aq.application.util.BradescoCommonServiceFactory;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoPagPendenteEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarMotivoSituacaoPagPendenteSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.IConsultasService;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarDescBancoAgenciaContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.ILiberarPagtosIndividuaisPendService;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.ConsultarPagtoPendIndividualEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.ConsultarPagtoPendIndividualSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoIndividualPendenciaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoIndividualPendenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoPendIndividualEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagtosindividuaispend.bean.LiberarPagtoPendIndividualSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;

/**
 * Nome: LiberarPagtosIndividuaisPendBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class LiberarPagtosIndividuaisPendBean extends PagamentosBean {
	
	/** Atributo TELA_LIBERAR_PROCESSAMENTO. */
	private static final String TELA_LIBERAR_PROCESSAMENTO = "TELA_LIBERAR_PROCESSAMENTO";

	/** Atributo TELA_LIBERAR_PENDENCIA. */
	private static final String TELA_LIBERAR_PENDENCIA = "TELA_LIBERAR_PENDENCIA";

	/** Atributo TELA_CONSULTAR. */
	private static final String TELA_CONSULTAR = "TELA_CONSULTAR";

	/** Atributo TELA_DETALHE_CREDITO_CONTA. */
	private static final String TELA_DETALHE_CREDITO_CONTA = "TELA_DETALHE_CREDITO_CONTA";

	/** Atributo TELA_DETALHE_DEBITO_CONTA. */
	private static final String TELA_DETALHE_DEBITO_CONTA = "TELA_DETALHE_DEBITO_CONTA";

	/** Atributo TELA_DETALHE_DOC. */
	private static final String TELA_DETALHE_DOC = "TELA_DETALHE_DOC";

	/** Atributo TELA_DETALHE_TED. */
	private static final String TELA_DETALHE_TED = "TELA_DETALHE_TED";

	/** Atributo TELA_DETALHE_ORDEM_PAGTO. */
	private static final String TELA_DETALHE_ORDEM_PAGTO = "TELA_DETALHE_ORDEM_PAGTO";

	/** Atributo TELA_DETALHE_TITULOS_BRADESCO. */
	private static final String TELA_DETALHE_TITULOS_BRADESCO = "TELA_DETALHE_TITULOS_BRADESCO";

	/** Atributo TELA_DETALHE_TITULOS_OUTROS_BANCOS. */
	private static final String TELA_DETALHE_TITULOS_OUTROS_BANCOS = "TELA_DETALHE_TITULOS_OUTROS_BANCOS";

	/** Atributo TELA_DETALHE_DARF. */
	private static final String TELA_DETALHE_DARF = "TELA_DETALHE_DARF";

	/** Atributo TELA_DETALHE_GARE. */
	private static final String TELA_DETALHE_GARE = "TELA_DETALHE_GARE";

	/** Atributo TELA_DETALHE_GPS. */
	private static final String TELA_DETALHE_GPS = "TELA_DETALHE_GPS";

	/** Atributo TELA_DETALHE_CODIGO_BARRAS. */
	private static final String TELA_DETALHE_CODIGO_BARRAS = "TELA_DETALHE_CODIGO_BARRAS";

	/** Atributo TELA_DETALHE_DEBITO_VEICULOS. */
	private static final String TELA_DETALHE_DEBITO_VEICULOS = "TELA_DETALHE_DEBITO_VEICULOS";

	/** Atributo TELA_DETALHE_DEPOSITO_IDENTIFICADO. */
	private static final String TELA_DETALHE_DEPOSITO_IDENTIFICADO = "TELA_DETALHE_DEPOSITO_IDENTIFICADO";

	/** Atributo TELA_DETALHE_ORDEM_CREDITO. */
	private static final String TELA_DETALHE_ORDEM_CREDITO = "TELA_DETALHE_ORDEM_CREDITO";

	/** Atributo liberarPagtosIndividuaisPendImpl. */
	private ILiberarPagtosIndividuaisPendService liberarPagtosIndividuaisPendImpl;

	/** Atributo disableArgumentosConsulta. */
	private boolean disableArgumentosConsulta;

	/** Atributo disableDetalhar. */
	private boolean disableDetalhar;

	/** Atributo consultasService. */
	private IConsultasService consultasService;

	/** Atributo listaGridLiberarPagtos. */
	private List<ConsultarPagtoPendIndividualSaidaDTO> listaGridLiberarPagtos;

	/** Atributo listaGridLiberarPagtosSelecionados. */
	private List<ConsultarPagtoPendIndividualSaidaDTO> listaGridLiberarPagtosSelecionados;

	/** Atributo listaControleLiberarPagtos. */
	private List<SelectItem> listaControleLiberarPagtos;

	/** Atributo listaConsultarMotivoSituacaoPagamentoFixoFiltro. */
	private List<SelectItem> listaConsultarMotivoSituacaoPagamentoFixoFiltro = new ArrayList<SelectItem>();

	/** Atributo opcaoChecarTodos. */
	private boolean opcaoChecarTodos;

	/** Atributo panelBotoes. */
	private boolean panelBotoes;

	// Variaveis Cliente
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;

	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;

	/** Atributo dsEmpresa. */
	private String dsEmpresa;

	/** Atributo nroContrato. */
	private String nroContrato;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo descricaoEmpresa. */
	private String descricaoEmpresa;

	/** Atributo descricaoNumeroContrato. */
	private String descricaoNumeroContrato;

	/** Atributo descricaoContrato. */
	private String descricaoContrato;

	/** Atributo descricaoSituacaoContrato. */
	private String descricaoSituacaoContrato;

	// Variaveis Conta Debito
	/** Atributo dsBancoDebito. */
	private String dsBancoDebito;

	/** Atributo dsAgenciaDebito. */
	private String dsAgenciaDebito;

	/** Atributo contaDebito. */
	private String contaDebito;

	/** Atributo tipoContaDebito. */
	private String tipoContaDebito;

	/** Atributo descricaoTipoContaDebito. */
	private String descricaoTipoContaDebito;

	/** Atributo descricaoBancoContaDebito. */
	private String descricaoBancoContaDebito;

	/** Atributo descricaoAgenciaContaDebitoBancaria. */
	private String descricaoAgenciaContaDebitoBancaria;

	// Trilha Auditoria
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	// Telas Detalhe
	/** Atributo tipoCanal. */
	private String tipoCanal;

	/** Atributo canal. */
	private String canal;

	/** Atributo orgaoPagador. */
	private String orgaoPagador;

	/** Atributo manutencao. */
	private boolean manutencao;

	/** Atributo cdFavorecido. */
	private String cdFavorecido;

	/** Atributo dsBancoFavorecido. */
	private String dsBancoFavorecido;

	/** Atributo dsContaFavorecido. */
	private String dsContaFavorecido;

	/** Atributo dsAgenciaFavorecido. */
	private String dsAgenciaFavorecido;

	/** Atributo cdTipoContaFavorecido. */
	private Integer cdTipoContaFavorecido;

	/** Atributo dsTipoContaFavorecido. */
	private String dsTipoContaFavorecido;

	/** Atributo dtNascimentoBeneficiario. */
	private String dtNascimentoBeneficiario;

	/** Atributo dsTipoServico. */
	private String dsTipoServico;

	/** Atributo dsTipoManutencao. */
	private String dsTipoManutencao;

	/** Atributo dsIndicadorModalidade. */
	private String dsIndicadorModalidade;

	/** Atributo nrSequenciaArquivoRemessa. */
	private String nrSequenciaArquivoRemessa;

	/** Atributo nrLoteArquivoRemessa. */
	private String nrLoteArquivoRemessa;

	/** Atributo numeroPagamento. */
	private String numeroPagamento;

	/** Atributo cdListaDebito. */
	private String cdListaDebito;

	/** Atributo dtAgendamento. */
	private String dtAgendamento;

	/** Atributo dtPagamento. */
	private String dtPagamento;
	
	/** Atributo dtDevolucaoEstorno. */
	private String dtDevolucaoEstorno;

	/** Atributo dtVencimento. */
	private String dtVencimento;

	/** Atributo cdIndicadorEconomicoMoeda. */
	private String cdIndicadorEconomicoMoeda;

	/** Atributo qtMoeda. */
	private BigDecimal qtMoeda;

	/** Atributo vlrAgendamento. */
	private BigDecimal vlrAgendamento;

	/** Atributo vlrEfetivacao. */
	private BigDecimal vlrEfetivacao;

	/** Atributo cdSituacaoOperacaoPagamento. */
	private String cdSituacaoOperacaoPagamento;

	/** Atributo cdMotivoSituacaoPagamento. */
	private Integer cdMotivoSituacaoPagamento;

	/** Atributo dsMotivoSituacao. */
	private String dsMotivoSituacao;

	/** Atributo dsPagamento. */
	private String dsPagamento;

	/** Atributo nrDocumento. */
	private String nrDocumento;

	/** Atributo tipoDocumento. */
	private String tipoDocumento;

	/** Atributo cdSerieDocumento. */
	private String cdSerieDocumento;

	/** Atributo vlDocumento. */
	private BigDecimal vlDocumento;

	/** Atributo dtEmissaoDocumento. */
	private String dtEmissaoDocumento;

	/** Atributo dsUsoEmpresa. */
	private String dsUsoEmpresa;

	/** Atributo dsMensagemPrimeiraLinha. */
	private String dsMensagemPrimeiraLinha;

	/** Atributo dsMensagemSegundaLinha. */
	private String dsMensagemSegundaLinha;

	/** Atributo cdBancoCredito. */
	private String cdBancoCredito;

	/** Atributo agenciaDigitoCredito. */
	private String agenciaDigitoCredito;

	/** Atributo contaDigitoCredito. */
	private String contaDigitoCredito;

	/** Atributo tipoContaCredito. */
	private String tipoContaCredito;

	/** Atributo cdBancoDestino. */
	private String cdBancoDestino;

	/** Atributo agenciaDigitoDestino. */
	private String agenciaDigitoDestino;

	/** Atributo contaDigitoDestino. */
	private String contaDigitoDestino;

	/** Atributo tipoContaDestino. */
	private String tipoContaDestino;

	/** Atributo vlDesconto. */
	private BigDecimal vlDesconto;

	/** Atributo vlAbatimento. */
	private BigDecimal vlAbatimento;

	/** Atributo vlMulta. */
	private BigDecimal vlMulta;

	/** Atributo vlMora. */
	private BigDecimal vlMora;

	/** Atributo vlDeducao. */
	private BigDecimal vlDeducao;

	/** Atributo vlAcrescimo. */
	private BigDecimal vlAcrescimo;

	/** Atributo vlImpostoRenda. */
	private BigDecimal vlImpostoRenda;

	/** Atributo vlIss. */
	private BigDecimal vlIss;

	/** Atributo vlIof. */
	private BigDecimal vlIof;

	/** Atributo vlInss. */
	private BigDecimal vlInss;

	/** Atributo camaraCentralizadora. */
	private String camaraCentralizadora;

	/** Atributo finalidadeTed. */
	private String finalidadeTed;

	/** Atributo finalidadeDoc. */
	private String finalidadeDoc;

	/** Atributo identificacaoTransferenciaTed. */
	private String identificacaoTransferenciaTed;

	/** Atributo identificacaoTransferenciaDoc. */
	private String identificacaoTransferenciaDoc;

	/** Atributo identificacaoTitularidade. */
	private String identificacaoTitularidade;

	/** Atributo numeroOp. */
	private String numeroOp;

	/** Atributo dataExpiracaoCredito. */
	private String dataExpiracaoCredito;

	/** Atributo instrucaoPagamento. */
	private String instrucaoPagamento;

	/** Atributo numeroCheque. */
	private String numeroCheque;

	/** Atributo serieCheque. */
	private String serieCheque;

	/** Atributo indicadorAssociadoUnicaAgencia. */
	private String indicadorAssociadoUnicaAgencia;

	/** Atributo identificadorTipoRetirada. */
	private String identificadorTipoRetirada;

	/** Atributo identificadorRetirada. */
	private String identificadorRetirada;

	/** Atributo dataRetirada. */
	private String dataRetirada;

	/** Atributo vlImpostoMovFinanceira. */
	private BigDecimal vlImpostoMovFinanceira;

	/** Atributo codigoBarras. */
	private String codigoBarras;

	/** Atributo dddContribuinte. */
	private String dddContribuinte;

	/** Atributo telefoneContribuinte. */
	private String telefoneContribuinte;

	/** Atributo inscricaoEstadualContribuinte. */
	private String inscricaoEstadualContribuinte;

	/** Atributo agenteArrecadador. */
	private String agenteArrecadador;

	/** Atributo codigoReceita. */
	private String codigoReceita;

	/** Atributo numeroReferencia. */
	private String numeroReferencia;

	/** Atributo numeroCotaParcela. */
	private String numeroCotaParcela;

	/** Atributo percentualReceita. */
	private BigDecimal percentualReceita;

	/** Atributo periodoApuracao. */
	private String periodoApuracao;

	/** Atributo anoExercicio. */
	private String anoExercicio;

	/** Atributo dataReferencia. */
	private String dataReferencia;

	/** Atributo vlReceita. */
	private BigDecimal vlReceita;

	/** Atributo vlPrincipal. */
	private BigDecimal vlPrincipal;

	/** Atributo vlAtualizacaoMonetaria. */
	private BigDecimal vlAtualizacaoMonetaria;

	/** Atributo vlTotal. */
	private BigDecimal vlTotal;

	/** Atributo codigoCnae. */
	private String codigoCnae;

	/** Atributo placaVeiculo. */
	private String placaVeiculo;

	/** Atributo numeroParcelamento. */
	private String numeroParcelamento;

	/** Atributo codigoOrgaoFavorecido. */
	private String codigoOrgaoFavorecido;

	/** Atributo nomeOrgaoFavorecido. */
	private String nomeOrgaoFavorecido;

	/** Atributo codigoUfFavorecida. */
	private String codigoUfFavorecida;

	/** Atributo descricaoUfFavorecida. */
	private String descricaoUfFavorecida;

	/** Atributo vlAcrescimoFinanceiro. */
	private BigDecimal vlAcrescimoFinanceiro;

	/** Atributo vlHonorarioAdvogado. */
	private BigDecimal vlHonorarioAdvogado;

	/** Atributo observacaoTributo. */
	private String observacaoTributo;

	/** Atributo codigoInss. */
	private String codigoInss;

	/** Atributo dataCompetencia. */
	private String dataCompetencia;

	/** Atributo vlOutrasEntidades. */
	private BigDecimal vlOutrasEntidades;

	/** Atributo codigoTributo. */
	private String codigoTributo;

	/** Atributo codigoRenavam. */
	private String codigoRenavam;

	/** Atributo municipioTributo. */
	private String municipioTributo;

	/** Atributo ufTributo. */
	private String ufTributo;

	/** Atributo numeroNsu. */
	private String numeroNsu;

	/** Atributo opcaoTributo. */
	private String opcaoTributo;

	/** Atributo opcaoRetiradaTributo. */
	private String opcaoRetiradaTributo;

	/** Atributo clienteDepositoIdentificado. */
	private String clienteDepositoIdentificado;

	/** Atributo tipoDespositoIdentificado. */
	private String tipoDespositoIdentificado;

	/** Atributo produtoDepositoIdentificado. */
	private String produtoDepositoIdentificado;

	/** Atributo sequenciaProdutoDepositoIdentificado. */
	private String sequenciaProdutoDepositoIdentificado;

	/** Atributo bancoCartaoDepositoIdentificado. */
	private String bancoCartaoDepositoIdentificado;

	/** Atributo cartaoDepositoIdentificado. */
	private String cartaoDepositoIdentificado;

	/** Atributo bimCartaoDepositoIdentificado. */
	private String bimCartaoDepositoIdentificado;

	/** Atributo viaCartaoDepositoIdentificado. */
	private String viaCartaoDepositoIdentificado;

	/** Atributo codigoDepositante. */
	private String codigoDepositante;

	/** Atributo nomeDepositante. */
	private String nomeDepositante;

	/** Atributo codigoPagador. */
	private String codigoPagador;

	/** Atributo codigoOrdemCredito. */
	private String codigoOrdemCredito;

	/** Atributo numeroInscricaoFavorecido. */
	private String numeroInscricaoFavorecido;

	/** Atributo dsFavorecido. */
	private String dsFavorecido;

	/** Atributo tipoFavorecido. */
	private Integer tipoFavorecido;

	/** Atributo descTipoFavorecido. */
	private String descTipoFavorecido;

	/** Atributo numeroInscricaoBeneficiario. */
	private String numeroInscricaoBeneficiario;

	/** Atributo tipoBeneficiario. */
	private String tipoBeneficiario;

	/** Atributo cdTipoBeneficiario. */
	private Integer cdTipoBeneficiario;

	/** Atributo dsTipoBeneficiario. */
	private String dsTipoBeneficiario;

	/** Atributo nomeBeneficiario. */
	private String nomeBeneficiario;

	/** Atributo nossoNumero. */
	private String nossoNumero;

	/** Atributo dsOrigemPagamento. */
	private String dsOrigemPagamento;

	/** Atributo carteira. */
	private String carteira;

	/** Atributo dtLimiteDescontoPagamento. */
	private String dtLimiteDescontoPagamento;
	
	/** Atributo dtLimitePagamento. */
	private String dtLimitePagamento;
	
	/** Atributo dsRastreado. */
	private String dsRastreado;
	
	/** Atributo cdSituacaoTranferenciaAutomatica. */
	private String cdSituacaoTranferenciaAutomatica;

	/** Atributo cdIndentificadorJudicial. */
	private String cdIndentificadorJudicial;

	/** Atributo exibeBeneficiario. */
	private boolean exibeBeneficiario;

	/** Atributo cdBancoOriginal. */
	private Integer cdBancoOriginal;

	/** Atributo dsBancoOriginal. */
	private String dsBancoOriginal;

	/** Atributo cdAgenciaBancariaOriginal. */
	private Integer cdAgenciaBancariaOriginal;

	/** Atributo cdDigitoAgenciaOriginal. */
	private String cdDigitoAgenciaOriginal;

	/** Atributo dsAgenciaOriginal. */
	private String dsAgenciaOriginal;

	/** Atributo cdContaBancariaOriginal. */
	private Long cdContaBancariaOriginal;

	/** Atributo cdDigitoContaOriginal. */
	private String cdDigitoContaOriginal;

	/** Atributo dsTipoContaOriginal. */
	private String dsTipoContaOriginal;
	
	/** Atributo cdOperacaoDcom. */
	private Long cdOperacaoDcom;
	
	/** Atributo dsSituacaoDcom. */
	private String dsSituacaoDcom;
	
	/** Atributo numControleInternoLote. */
	private Long numControleInternoLote;
	
	// Flag
	/** Atributo exibeDcom. */
	private Boolean exibeDcom;

	// relatorio
	/** Atributo logoPath. */
	private String logoPath;

	/**
	 * Iniciar tela.
	 *
	 * @param evt the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		// Tela de Filtro
		getFiltroAgendamentoEfetivacaoEstornoBean().setEntradaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasEntradaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().setSaidaConsultarListaClientePessoas(
						new ConsultarListaClientePessoasSaidaDTO());
		getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
		getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
		getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno("conLiberarPagtosIndividuaisPend");

		// Limpa Lista
		limpar();

		// Carregamento dos combos
		listarTipoServicoPagto();
		listarInscricaoFavorecido();
		listarConsultarMotivoPagamento();

		setHabilitaArgumentosPesquisa(false);
		getFiltroAgendamentoEfetivacaoEstornoBean().setClienteContratoSelecionado(false);
		setDisableArgumentosConsulta(false);
		getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);

		// Radio de Agendados � Pagos/N�o Pagos, ficara sempre protegido e com a
		// op��o �Agendados� selecionada;
		setAgendadosPagosNaoPagos("1");

		getFiltroAgendamentoEfetivacaoEstornoBean().setVoltarParticipante("conLiberarPagtosIndividuaisPend");
	}

	/**
	 * Listar consultar motivo pagamento.
	 */
	public void listarConsultarMotivoPagamento() {
		try {
			setListaConsultarMotivoSituacaoPagamentoFixoFiltro(new ArrayList<SelectItem>());
			setCboMotivoSituacaoFiltro(null);

			ConsultarMotivoSituacaoPagPendenteEntradaDTO entrada = new ConsultarMotivoSituacaoPagPendenteEntradaDTO();
			// SITUA��O DO PAGAMENTO SER� SEMPRE 2 NESTA TELA
			entrada.setCdSituacaoPagamento(2);

			List<ConsultarMotivoSituacaoPagPendenteSaidaDTO> list = getComboService()
							.consultarMotivoSituacaoPagPendente(entrada);

			for (ConsultarMotivoSituacaoPagPendenteSaidaDTO saida : list) {
				getListaConsultarMotivoSituacaoPagamentoFixoFiltro().add(
								new SelectItem(saida.getCdMotivoSituacaoPagamento(), saida.getDsMotivoSituacaoPag()));
			}
		} catch (PdcAdapterFunctionalException p) {
			setListaConsultarMotivoSituacaoPagamentoFixoFiltro(new ArrayList<SelectItem>());
		}
	}

	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#limparSituacaoMotivo()
	 */
	public void limparSituacaoMotivo() {
		setCboMotivoSituacaoFiltro(null);
	}

	/**
	 * Imprimir processamento.
	 */
	public void imprimirProcessamento() {
		try {

			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			/*
			 * if(getListaGridLiberarPagtosSelecionados().size() > 1){ carregaCabecalho(); }else{
			 * carregaCabecalhoIntermediario()(); }
			 */

			carregaCabecalho();

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par.put("titulo", "Liberar Pagamentos Individuais Pendentes - Liberar Processamento");

			par.put("cpfCnpj", getFiltroAgendamentoEfetivacaoEstornoBean().getCpfCnpjFormatado());
			par.put("nomeRazaoSocial", getFiltroAgendamentoEfetivacaoEstornoBean().getNomeRazaoParticipante());
			par.put("empresa", getDsEmpresa());
			par.put("numero", getNroContrato());
			par.put("descContrato", getDsContrato());
			par.put("situacao", getCdSituacaoContrato());
			par.put("bancoDebito", getDsBancoDebito());
			par.put("agenciaDebito", getDsAgenciaDebito());
			par.put("contaDebito", getContaDebito());
			par.put("tipoContaDebito", getTipoContaDebito());
			par.put("logoBradesco", getLogoPath());
			par.put("flagBanda", Integer.parseInt(getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado()));
			try {

				// M�todo que gera o relat�rio PDF.
				PgitUtil.geraRelatorioPdf(
								"/relatorios/agendamentoEfetivacaoEstorno/liberarPagtosIndPendentes/libProcessamento",
								"imprimirLibPagtosIndPenLibProc", getListaGridLiberarPagtosSelecionados(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
	}

	/**
	 * Imprimir pendencia.
	 */
	public void imprimirPendencia() {
		try {

			String caminho = "/images/Bradesco_logo.JPG";
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
			logoPath = servletContext.getRealPath(caminho);

			/*
			 * if(getListaGridLiberarPagtosSelecionados().size() > 1){ carregaCabecalho(); }else{
			 * carregaCabecalhoIntermediario()(); }
			 */

			carregaCabecalho();

			// Par�metros do Relat�rio
			Map<String, Object> par = new HashMap<String, Object>();
			par.put("titulo", "Liberar Pagamentos Individuais Pendentes - Liberar Pend�ncia");

			par.put("cpfCnpj", getFiltroAgendamentoEfetivacaoEstornoBean().getCpfCnpjFormatado());
			par.put("nomeRazaoSocial", getFiltroAgendamentoEfetivacaoEstornoBean().getNomeRazaoParticipante());
			par.put("empresa", getDsEmpresa());
			par.put("numero", getNroContrato());
			par.put("descContrato", getDsContrato());
			par.put("situacao", getCdSituacaoContrato());
			par.put("bancoDebito", getDsBancoDebito());
			par.put("agenciaDebito", getDsAgenciaDebito());
			par.put("contaDebito", getContaDebito());
			par.put("tipoContaDebito", getTipoContaDebito());
			par.put("logoBradesco", getLogoPath());
			par.put("flagBanda", Integer.parseInt(getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado()));
			try {

				// M�todo que gera o relat�rio PDF.
				PgitUtil.geraRelatorioPdf(
								"/relatorios/agendamentoEfetivacaoEstorno/liberarPagtosIndPendentes/libPendencia",
								"imprimirLibPagtosIndPenLibPend", getListaGridLiberarPagtosSelecionados(), par);

			} /* Exce��o do relat�rio */
			catch (Exception e) {
				throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
			}

		} /* Exce��o do PDC */
		catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
	}

	/**
	 * Pesquisar selecionados.
	 *
	 * @param evt the evt
	 */
	public void pesquisarSelecionados(ActionEvent evt) {

	}

	/**
	 * Pesquisar tela consulta.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarTelaConsulta(ActionEvent evt) {
		consultar();
		return "";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		try {
			carregaCabecalhoIntermediario();

			ConsultarPagtoPendIndividualSaidaDTO registroSelecionado = null;
			for (ConsultarPagtoPendIndividualSaidaDTO o : getListaGridLiberarPagtos()) {
				if (o.isCheck()) {
					registroSelecionado = o;
					break;
				}
			}

			if (registroSelecionado != null && registroSelecionado.getCdTipoTela() != null) {
				if (registroSelecionado.getCdTipoTela().intValue() == 1) {
					preencheDadosPagtoCreditoConta(registroSelecionado);
					return TELA_DETALHE_CREDITO_CONTA;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 2) {
					preencheDadosPagtoDebitoConta(registroSelecionado);
					return TELA_DETALHE_DEBITO_CONTA;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 3) {
					preencheDadosPagtoDOC(registroSelecionado);
					return TELA_DETALHE_DOC;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 4) {
					preencheDadosPagtoTED(registroSelecionado);
					return TELA_DETALHE_TED;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 5) {
					preencheDadosPagtoOrdemPagto(registroSelecionado);
					return TELA_DETALHE_ORDEM_PAGTO;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 6) {
					preencheDadosPagtoTituloBradesco(registroSelecionado);
					return TELA_DETALHE_TITULOS_BRADESCO;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 7) {
					preencheDadosPagtoTituloOutrosBancos(registroSelecionado);
					return TELA_DETALHE_TITULOS_OUTROS_BANCOS;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 8) {
					preencheDadosPagtoDARF(registroSelecionado);
					return TELA_DETALHE_DARF;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 9) {
					preencheDadosPagtoGARE(registroSelecionado);
					return TELA_DETALHE_GARE;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 10) {
					preencheDadosPagtoGPS(registroSelecionado);
					return TELA_DETALHE_GPS;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 11) {
					preencheDadosPagtoCodigoBarras(registroSelecionado);
					return TELA_DETALHE_CODIGO_BARRAS;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 12) {
					preencheDadosPagtoDebitoVeiculos(registroSelecionado);
					return TELA_DETALHE_DEBITO_VEICULOS;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 13) {
					preencheDadosPagtoDepIdentificado(registroSelecionado);
					return TELA_DETALHE_DEPOSITO_IDENTIFICADO;
				}
				if (registroSelecionado.getCdTipoTela().intValue() == 14) {
					preencheDadosPagtoOrdemCredito(registroSelecionado);
					return TELA_DETALHE_ORDEM_CREDITO;
				}
			}
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}

		return "";
	}

	/**
	 * Limpar tela principal.
	 *
	 * @return the string
	 */
	public String limparTelaPrincipal() {
		// Limpar Argumentos de Pesquisa e Lista
		limparFiltroPrincipal();
		limparInformacaoConsulta();
		limparSituacaoMotivo();

		return "";
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 *
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
		limparInformacaoConsulta();
		limparSituacaoMotivo();
		limparCampos();
		getFiltroAgendamentoEfetivacaoEstornoBean().setItemFiltroSelecionado(null);
		return "";

	}

	/**
	 * Limpar cliente.
	 *
	 * @return the string
	 */
	public String limparCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
		limparInformacaoConsulta();
		limparSituacaoMotivo();
		limparCampos();
		return "";
	}

	/**
	 * Confirmar processamento.
	 */
	public void confirmarProcessamento() {
		try {
			List<LiberarPagtoPendIndividualEntradaDTO> listaEntrada = new ArrayList<LiberarPagtoPendIndividualEntradaDTO>();

			for (ConsultarPagtoPendIndividualSaidaDTO o : getListaGridLiberarPagtosSelecionados()) {
				LiberarPagtoPendIndividualEntradaDTO entrada = new LiberarPagtoPendIndividualEntradaDTO();
				entrada.setCdPessoaJuridicaContrato(o.getCdEmpresa());
				entrada.setCdTipoContratoNegocio(o.getCdTipoContratoNegocio());
				entrada.setNrSequenciaContratoNegocio(o.getNrContrato());
				entrada.setCdTipoCanal(o.getCdTipoCanal());
				entrada.setCdControlePagamento(o.getCdControlePagamento());
				entrada.setCdModalidade(o.getCdTipoTela());

				listaEntrada.add(entrada);
			}

			LiberarPagtoPendIndividualSaidaDTO saidaDTO = getLiberarPagtosIndividuaisPendImpl()
							.liberarPagtoPendIndividual(listaEntrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
							"conLiberarPagtosIndividuaisPend", "#{liberarPagtosIndividuaisPendBean.consultar}", false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
	}

	/**
	 * Confirmar pendencia.
	 */
	public void confirmarPendencia() {
		try {
			List<LiberarPagtoIndividualPendenciaEntradaDTO> listaEntrada = new ArrayList<LiberarPagtoIndividualPendenciaEntradaDTO>();

			for (ConsultarPagtoPendIndividualSaidaDTO o : getListaGridLiberarPagtosSelecionados()) {
				LiberarPagtoIndividualPendenciaEntradaDTO entrada = new LiberarPagtoIndividualPendenciaEntradaDTO();
				entrada.setCdPessoaJuridicaContrato(o.getCdEmpresa());
				entrada.setCdTipoContratoNegocio(o.getCdTipoContratoNegocio());
				entrada.setNrSequenciaContratoNegocio(o.getNrContrato());
				entrada.setCdTipoCanal(o.getCdTipoCanal());
				entrada.setCdControlePagamento(o.getCdControlePagamento());
				entrada.setCdModalidade(o.getCdTipoTela());
				entrada.setCdMotivoPendencia(o.getCdMotivoSituacaoPagamento());

				listaEntrada.add(entrada);
			}

			LiberarPagtoIndividualPendenciaSaidaDTO saidaDTO = getLiberarPagtosIndividuaisPendImpl()
							.liberarPagtoIndividualPendencia(listaEntrada);

			BradescoFacesUtils.addInfoModalMessage("(" + saidaDTO.getCodMensagem() + ") " + saidaDTO.getMensagem(),
							"conLiberarPagtosIndividuaisPend", "#{liberarPagtosIndividuaisPendBean.consultar}", false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
		}
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 *
	 * @param evt the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		limparCampos();
		limparInformacaoConsulta();
		limparSituacaoMotivo();
	}

	/**
	 * Consultar.
	 *
	 * @param evt the evt
	 */
	public void consultar(ActionEvent evt) {
		consultar();
	}

	/**
	 * Consultar.
	 */
	public void consultar() {
		limparInformacaoConsulta();
		try {
			ConsultarPagtoPendIndividualEntradaDTO entrada = new ConsultarPagtoPendIndividualEntradaDTO();

			/*
			 * Se selecionado �Filtrar por Cliente� ou �Filtrar por Contrato� mover �1�. Se selecionado �Filtrar por
			 * Conta D�bito� mover �2�;
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")
							|| getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) {
				entrada.setCdTipoPesquisa(1);
			} else {
				entrada.setCdTipoPesquisa(2);
			}

			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")
							|| getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) {
				/*
				 * >> Se Selecionado Filtrar por Cliente Mover Valores vindos da lista >> Se Selecionado Filtrar por
				 * Contrato Passar valor retornado da lista(Verificado por email), na especifica��o definia passar os
				 * combo e inputtext, o que poderia gerar problemas caso o usu�rio altere os valores do combo ap�s ter
				 * feito a pesquisa
				 */
				entrada.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdPessoaJuridicaContrato() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
								.getCdPessoaJuridicaContrato() : 0L);
				entrada
								.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
												.getCdTipoContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
												.getCdTipoContratoNegocio()
												: 0);
				entrada.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
								.getNrSequenciaContratoNegocio() : 0L);
			}

			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
				/*
				 * Se selecionado �Filtrar por Conta D�bito� mover zeros para os cambos abaixo
				 */
				entrada.setCdPessoaJuridicaContrato(0L);
				entrada.setCdTipoContratoNegocio(0);
				entrada.setNrSequenciaContratoNegocio(0L);
			}

			/*
			 * Se estiver selecionado "Filtrar por Conta D�bito", passar os campos deste filtro Se estiver selecionado
			 * outro tipo de filtro, verificar se o "Conta D�bito" dos argumentos de pesquisa esta marcado Se estiver
			 * marcado, passar os valores correspondentes da tela,caso contr�rio passar zeros
			 */
			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
				entrada.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean().getBancoContaDebitoFiltro());
				entrada.setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean().getAgenciaContaDebitoBancariaFiltro());
				entrada.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean().getContaBancariaContaDebitoFiltro());
				entrada.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
			} else {
				if (isChkContaDebito()) {
					entrada.setCdBanco(getCdBancoContaDebito());
					entrada.setCdAgencia(getCdAgenciaBancariaContaDebito());
					entrada.setCdConta(getCdContaBancariaContaDebito());
					entrada.setCdDigitoConta(PgitUtil.DIGITO_CONTA);
				} else {
					entrada.setCdBanco(0);
					entrada.setCdAgencia(0);
					entrada.setCdConta(0L);
					entrada.setCdDigitoConta("00");
				}
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			entrada.setDtCreditoPagamentoInicio((getDataInicialPagamentoFiltro() == null) ? "" : sdf
							.format(getDataInicialPagamentoFiltro()));
			entrada.setDtCreditoPagamentoFim((getDataFinalPagamentoFiltro() == null) ? "" : sdf
							.format(getDataFinalPagamentoFiltro()));

			entrada.setNrArquivoRemssaPagamento(getNumeroRemessaFiltro().equals("") ? 0L : Long
							.parseLong(getNumeroRemessaFiltro()));

			if (isChkParticipanteContrato()) {
				entrada.setCdParticipante(getFiltroAgendamentoEfetivacaoEstornoBean().getCodigoParticipanteFiltro());
			} else {
				entrada.setCdParticipante(0L);
			}

			entrada.setCdProdutoServicoOperacao(getTipoServicoFiltro() != null ? getTipoServicoFiltro() : 0);
			entrada.setCdProdutoServicoRelacionado(getModalidadeFiltro() != null ? getModalidadeFiltro() : 0);
			entrada.setCdControlePagamentoDe(getNumeroPagamentoDeFiltro() != null ? getNumeroPagamentoDeFiltro() : "");
			entrada.setCdControlePagamentoAte(getNumeroPagamentoDeFiltro() != null ? getNumeroPagamentoDeFiltro() : "");
			entrada.setVlPagamentoDe(getValorPagamentoDeFiltro() != null ? getValorPagamentoDeFiltro()
							: new BigDecimal("0.00"));
			entrada.setVlPagamentoAte(getValorPagamentoAteFiltro() != null ? getValorPagamentoAteFiltro()
							: new BigDecimal("0.00"));

			entrada.setCdMotivoSituacaoPagamento(getCboMotivoSituacaoFiltro());

			entrada.setCdBarraDocumento(PgitUtil.verificaStringNula(getLinhaDigitavel1())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel2())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel3())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel4())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel5())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel6())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel7())
							+ PgitUtil.verificaStringNula(getLinhaDigitavel8()));
			entrada.setCdFavorecidoClientePagador(!getCodigoFavorecidoFiltro().equals("") ? Long
							.parseLong(getCodigoFavorecidoFiltro()) : 0L);
			entrada.setCdInscricaoFavorecido(!getInscricaoFiltro().equals("") ? Long.parseLong(getInscricaoFiltro())
							: 0L);
			entrada.setCdIdentificacaoInscricaoFavorecido(getTipoInscricaoFiltro() != null ? getTipoInscricaoFiltro()
							: 0);
			entrada.setCdBancoBeneficiario(getCdBancoContaCredito() != null ? getCdBancoContaCredito() : 0);
			entrada
							.setCdAgenciaBeneficiario(getCdAgenciaBancariaContaCredito() != null ? getCdAgenciaBancariaContaCredito()
											: 0);
			entrada.setCdContaBeneficiario(getCdContaBancariaContaCredito() != null ? getCdContaBancariaContaCredito()
							: 0L);
			entrada.setCdDigitoContaBeneficiario(getCdDigitoContaCredito() != null
							&& !getCdDigitoContaCredito().equals("") ? getCdDigitoContaCredito() : "00");
			entrada.setNrUnidadeOrganizacional(0);
			entrada.setCdBeneficio(0L);
			entrada.setCdEspecieBeneficioInss(0);
			entrada.setCdClassificacao(getClassificacaoFiltro());

			// conforme solicitado via email por Kelli Cristina dia 12/04/2011,
			// retirar o combo Tipo Conta da tela e
			// passar os campos zerados para o pdc
			entrada.setCdEmpresaContrato(0L);
			entrada.setCdTipoConta(0);
			entrada.setNrContrato(0L);

			setListaGridLiberarPagtos(getLiberarPagtosIndividuaisPendImpl().consultarPagtoPendIndividual(entrada));

			setListaControleLiberarPagtos(new ArrayList<SelectItem>());
			for (int i = 0; i < getListaGridLiberarPagtos().size(); i++) {
				getListaControleLiberarPagtos().add(new SelectItem(i, ""));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
							false);
			setDisableArgumentosConsulta(false);
		}
	}

	/**
	 * Carrega lista selecionados.
	 */
	private void carregaListaSelecionados() {
		setListaGridLiberarPagtosSelecionados(new ArrayList<ConsultarPagtoPendIndividualSaidaDTO>());
		for (ConsultarPagtoPendIndividualSaidaDTO o : getListaGridLiberarPagtos()) {
			if (o.isCheck()) {
				getListaGridLiberarPagtosSelecionados().add(o);
			}
		}

		/*
		 * if(getListaGridLiberarPagtosSelecionados().size() > 1){ carregaCabecalho(); }else{
		 * carregaCabecalhoIntermediario()(); }
		 */

		carregaCabecalho();
	}

	/**
	 * Limpar selecionados.
	 */
	public void limparSelecionados() {
		for (ConsultarPagtoPendIndividualSaidaDTO o : getListaGridLiberarPagtos()) {
			o.setCheck(false);
		}
		setDisableDetalhar(true);
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
	}

	/**
	 * Carrega cabecalho.
	 */
	private void carregaCabecalho() {

		ConsultarPagtoPendIndividualSaidaDTO registroSelecionado = new ConsultarPagtoPendIndividualSaidaDTO();

		for (int i = 0; i < getListaGridLiberarPagtos().size(); i++) {
			if (getListaGridLiberarPagtos().get(i).isCheck()) {
				registroSelecionado = getListaGridLiberarPagtos().get(i);
				break;
			}
		}

		limparVariaveis();
		if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")) { // Filtrar por
			// cliente
			setNrCnpjCpf(getFiltroAgendamentoEfetivacaoEstornoBean().getNrCpfCnpjCliente());
			setDsRazaoSocial(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoRazaoSocialCliente());
			setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescCliente());
			setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescCliente());
			setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescCliente());
			setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescCliente());
		} else {
			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) { // Filtrar por
				// contrato
				setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescContrato());
				setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescContrato());
				setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescContrato());
				setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescContrato());
			} else {
				if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) { // Filtrar
					// por
					// Conta
					// D�bito
					getFiltroAgendamentoEfetivacaoEstornoBean().carregaDescricaoConta();
					setContaDebito(PgitUtil.formatConta(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getContaBancariaContaDebitoFiltro(), registroSelecionado.getCdDigitoContaDebito(),
									false));
					setTipoContaDebito(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoTipoContaDebito());
					setDsBancoDebito(PgitUtil.formatBanco(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getBancoContaDebitoFiltro(), getFiltroAgendamentoEfetivacaoEstornoBean()
									.getDescricaoBancoContaDebito(), false));
					setDsAgenciaDebito(PgitUtil.formatAgencia(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getAgenciaContaDebitoBancariaFiltro(), registroSelecionado
									.getCdDigitoAgenciaDebito(), getFiltroAgendamentoEfetivacaoEstornoBean()
									.getDescricaoAgenciaContaDebitoBancaria(), false));
				}
			}
		}
	}

	/**
	 * Carrega cabecalho intermediario.
	 */
	private void carregaCabecalhoIntermediario() {

		ConsultarPagtoPendIndividualSaidaDTO registroSelecionado = new ConsultarPagtoPendIndividualSaidaDTO();

		for (int i = 0; i < getListaGridLiberarPagtos().size(); i++) {
			if (getListaGridLiberarPagtos().get(i).isCheck()) {
				registroSelecionado = getListaGridLiberarPagtos().get(i);
				break;
			}
		}

		limparVariaveis();
		if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")) { // Filtrar por
			// cliente
			setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescCliente());
			setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescCliente());
			setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescCliente());
			setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescCliente());
			carregaContaBancoAgenciaDebito();
		} else {
			if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) { // Filtrar por
				// contrato
				setDsEmpresa(getFiltroAgendamentoEfetivacaoEstornoBean().getEmpresaGestoraDescContrato());
				setNroContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getNumeroDescContrato());
				setDsContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoContratoDescContrato());
				setCdSituacaoContrato(getFiltroAgendamentoEfetivacaoEstornoBean().getSituacaoDescContrato());
				carregaContaBancoAgenciaDebito();
			} else {
				if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) { // Filtrar
					// contaDebito
					carregaDadosContrato();
					getFiltroAgendamentoEfetivacaoEstornoBean().carregaDescricaoConta();
					setContaDebito(PgitUtil.formatConta(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getContaBancariaContaDebitoFiltro(), registroSelecionado.getCdDigitoContaDebito(),
									false));
					setTipoContaDebito(getFiltroAgendamentoEfetivacaoEstornoBean().getDescricaoTipoContaDebito());
					setDsBancoDebito(PgitUtil.formatBanco(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getBancoContaDebitoFiltro(), getFiltroAgendamentoEfetivacaoEstornoBean()
									.getDescricaoBancoContaDebito(), false));
					setDsAgenciaDebito(PgitUtil.formatAgencia(getFiltroAgendamentoEfetivacaoEstornoBean()
									.getAgenciaContaDebitoBancariaFiltro(), registroSelecionado
									.getCdDigitoAgenciaDebito(), getFiltroAgendamentoEfetivacaoEstornoBean()
									.getDescricaoAgenciaContaDebitoBancaria(), false));
				}
			}

		}

		setNrCnpjCpf(CpfCnpjUtils.formatCpfCnpjCompleto(registroSelecionado.getNrCnpjCpf(), registroSelecionado
						.getNrFilialCnpjCpf(), registroSelecionado.getNrDigitoCnpjCpf()));
		setDsRazaoSocial(registroSelecionado.getDsRazaoSocial());

	}

	/**
	 * Carrega conta banco agencia debito.
	 */
	public void carregaContaBancoAgenciaDebito() {

		ConsultarPagtoPendIndividualSaidaDTO registroSelecionado = new ConsultarPagtoPendIndividualSaidaDTO();

		for (int i = 0; i < getListaGridLiberarPagtos().size(); i++) {
			if (getListaGridLiberarPagtos().get(i).isCheck()) {
				registroSelecionado = getListaGridLiberarPagtos().get(i);
				break;
			}
		}

		try {
			ConsultarDescBancoAgenciaContaEntradaDTO consultarDescBancoAgenciaContaEntradaDTO = new ConsultarDescBancoAgenciaContaEntradaDTO();
			consultarDescBancoAgenciaContaEntradaDTO.setCdBanco(registroSelecionado.getCdBancoDebito());
			consultarDescBancoAgenciaContaEntradaDTO.setCdAgencia(registroSelecionado.getCdAgenciaDebito());
			consultarDescBancoAgenciaContaEntradaDTO.setCdConta(registroSelecionado.getCdContaDebito());
			consultarDescBancoAgenciaContaEntradaDTO.setCdDigitoConta(PgitUtil
							.complementaDigitoConta(registroSelecionado.getCdDigitoContaDebito()));

			ConsultarDescBancoAgenciaContaSaidaDTO consultarDescBancoAgenciaContaSaidaDTO = getConsultasService()
							.consultarDescBancoAgenciaConta(consultarDescBancoAgenciaContaEntradaDTO);

			setDescricaoTipoContaDebito(consultarDescBancoAgenciaContaSaidaDTO.getDsTipoConta());
			setDescricaoBancoContaDebito(consultarDescBancoAgenciaContaSaidaDTO.getDsBanco());
			setDescricaoAgenciaContaDebitoBancaria(consultarDescBancoAgenciaContaSaidaDTO.getDsAgencia());
		} catch (PdcAdapterFunctionalException e) {
			setDescricaoTipoContaDebito("");
			setDescricaoBancoContaDebito("");
			setDescricaoAgenciaContaDebitoBancaria("");
		}

		setDsBancoDebito(PgitUtil.formatBanco(registroSelecionado.getCdBancoDebito(), getDescricaoBancoContaDebito(),
						false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(registroSelecionado.getCdAgenciaDebito(), registroSelecionado
						.getCdDigitoAgenciaDebito(), getDescricaoAgenciaContaDebitoBancaria(), false));
		setContaDebito(PgitUtil.formatConta(registroSelecionado.getCdContaDebito(), registroSelecionado
						.getCdDigitoContaDebito(), false));
		setTipoContaDebito(getDescricaoTipoContaDebito());
	}

	/**
	 * Carrega dados contrato.
	 */
	public void carregaDadosContrato() {

		ConsultarPagtoPendIndividualSaidaDTO registroSelecionado = new ConsultarPagtoPendIndividualSaidaDTO();

		for (int i = 0; i < getListaGridLiberarPagtos().size(); i++) {
			if (getListaGridLiberarPagtos().get(i).isCheck()) {
				registroSelecionado = getListaGridLiberarPagtos().get(i);
				break;
			}
		}

		try {
			DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
			entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
			entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
			entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
			DetalharDadosContratoSaidaDTO saidaDTO = getConsultasService().detalharDadosContrato(entradaDTO);
			setDescricaoContrato(saidaDTO.getDsContrato());
			setDescricaoNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContratoNegocio()));
			setDescricaoEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
			setDescricaoSituacaoContrato(saidaDTO.getDsSituacaoContratoNegocio());
		} catch (PdcAdapterFunctionalException p) {
			setDescricaoContrato("");
			setDescricaoNumeroContrato("");
			setDescricaoEmpresa("");
			setDescricaoSituacaoContrato("");
		}

		setDsEmpresa(getDescricaoSituacaoContrato());
		setNroContrato(getDescricaoNumeroContrato());
		setDsContrato(getDescricaoContrato());
		setCdSituacaoContrato(getDescricaoSituacaoContrato());

	}

	// M�todos
	/**
	 * Limpar variaveis.
	 */
	public void limparVariaveis() {
		setNrCnpjCpf(null);
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");
		setDsBancoDebito("");
		setDsAgenciaDebito("");
		setContaDebito(null);
		setTipoContaDebito("");
		setDescricaoContrato("");
		setDescricaoNumeroContrato("");
		setDescricaoEmpresa("");
		setDescricaoSituacaoContrato("");
		setDescricaoTipoContaDebito("");
		setDescricaoBancoContaDebito("");
		setDescricaoAgenciaContaDebitoBancaria("");

	}

	// M�todo sobreposto para n�o limpar os agendados - pagos/nao pagos
	/**
	 * (non-Javadoc)
	 * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#limparCampos()
	 */
	public void limparCampos() {
		getFiltroAgendamentoEfetivacaoEstornoBean().limparParticipanteContrato();
		setChkContaDebito(false);
		limparContaDebito();
		limparRemessaFavorecidoBeneficiarioLinha();
		if (getFiltroAgendamentoEfetivacaoEstornoBean() != null
						&& getFiltroAgendamentoEfetivacaoEstornoBean().getPaginaRetorno() != null
						&& getFiltroAgendamentoEfetivacaoEstornoBean().getPaginaRetorno().equals(
										"conSolRecPagVenNaoPagos")) {
			limparMotivo();
		} else {
			limparSituacaoMotivo();
		}
		limparTipoServicoModalidade();

		setDataInicialPagamentoFiltro(new Date());
		setDataFinalPagamentoFiltro(new Date());
		setChkParticipanteContrato(false);
		setChkRemessa(false);
		setChkServicoModalidade(false);
		setChkSituacaoMotivo(false);
		setRadioPesquisaFiltroPrincipal("");
		setRadioFavorecidoFiltro("");
		setRadioBeneficiarioFiltro("");

		setClassificacaoFiltro(null);

		setChkNumeroListaDebito(false);
		setNumListaDebitoDeFiltro("");
		setNumListaDebitoAteFiltro("");

		setCboSituacaoListaDebito(null);
		setChkSituacaoListaDebito(false);
		setValorPagamentoAteFiltro(null);
		setValorPagamentoDeFiltro(null);
	}

	/**
	 * Limpar.
	 *
	 * @return the string
	 */
	public String limpar() {
		limparCampos();
		getFiltroAgendamentoEfetivacaoEstornoBean().limparFiltroPrincipal();
		getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado("");
		setHabilitaArgumentosPesquisa(false);
		limparInformacaoConsulta();
		limparSituacaoMotivo();
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Limpar informacao consulta.
	 */
	public void limparInformacaoConsulta() {
		setListaControleLiberarPagtos(new ArrayList<SelectItem>());
		setListaGridLiberarPagtos(new ArrayList<ConsultarPagtoPendIndividualSaidaDTO>());
		setOpcaoChecarTodos(false);
		setPanelBotoes(false);
		setDisableDetalhar(true);
		setDisableArgumentosConsulta(false);
	}

	/**
	 * Habilitar panel botoes.
	 */
	public void habilitarPanelBotoes() {
		setDisableDetalhar(true);
		setPanelBotoes(false);
		int cont = 0;
		for (ConsultarPagtoPendIndividualSaidaDTO o : getListaGridLiberarPagtos()) {
			if (o.isCheck()) {
				cont++;
				setPanelBotoes(true);
			}
		}

		if (cont == 1) {
			setDisableDetalhar(false);
		}
	}

	/**
	 * Liberar processamento.
	 *
	 * @return the string
	 */
	public String liberarProcessamento() {
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(new PDCDataBean());

		carregaListaSelecionados();
		return TELA_LIBERAR_PROCESSAMENTO;
	}

	/**
	 * Liberar pendencia.
	 *
	 * @return the string
	 */
	public String liberarPendencia() {
		BradescoCommonServiceFactory.getSessionManager().setPDCDataBean(new PDCDataBean());
		carregaListaSelecionados();
		return TELA_LIBERAR_PENDENCIA;
	}

	/**
	 * Voltar detalhe.
	 *
	 * @return the string
	 */
	public String voltarDetalhe() {
		limparSelecionados();

		return TELA_CONSULTAR;
	}

	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar() {
		limparSelecionados();

		return TELA_CONSULTAR;
	}

	/**
	 * Limpar favorecido beneficiario.
	 */
	public void limparFavorecidoBeneficiario() {
		setNumeroInscricaoFavorecido("");
		setTipoFavorecido(0);
		setDsFavorecido("");
		setCdFavorecido("");
		setDsBancoFavorecido("");
		setDsAgenciaFavorecido("");
		setDsContaFavorecido("");
		setDsTipoContaFavorecido("");
		setNumeroInscricaoBeneficiario("");
		setCdTipoBeneficiario(0);
		setNomeBeneficiario("");
		setDtNascimentoBeneficiario("");
		setDsTipoBeneficiario("");
	}

	// Detalhar Pagamentos Individuais Pendentes - Cr�dito em Conta
	/**
	 * Preenche dados pagto credito conta.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoCreditoConta(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoCreditoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoCreditoConta(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
						.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(), saidaDTO.getDsBancoDebito(), false));
		setDsAgenciaDebito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
						saidaDTO.getDsAgenciaDebito(), false));
		setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(), saidaDTO.getDsDigAgenciaDebito(), false));
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidade() == 1 || saidaDTO.getCdIndicadorModalidade() == 3) {
			setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
							.getNmInscriFavorecido()));
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(),
							false));
			setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
							.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
			setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
							false));
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidade() == 1 ? true : false);
		} else {
			setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlrAgendamento());
		setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
						.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil
						.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setCdSituacaoTranferenciaAutomatica(saidaDTO.getCdSituacaoTranferenciaAutomatica());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());
		setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

		// trilhaAuditoria
		setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
		setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
		setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
		
		//Favorecidos
		setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
		setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - " + saidaDTO.getDsBancoOriginal());
		setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
		setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
		setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-" + saidaDTO.getCdDigitoAgenciaOriginal() + " - " + saidaDTO.getDsAgenciaOriginal());
		setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
		setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-" + saidaDTO.getCdDigitoContaOriginal());
		setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());
	}

	// Detalhar Pagamentos Individuais Pendentes - D�bito em conta
	/**
	 * Preenche dados pagto debito conta.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoDebitoConta(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoDebitoContaEntradaDTO entradaDTO = new DetalharPagtoDebitoContaEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDebitoConta(entradaDTO);

		// Favorecido
		setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
						.getCdFavorecido()));
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(registroSelecionado.getDsFavorecido());
		setCdFavorecido(registroSelecionado.getCdInscricaoFavorecido());
		setDsBancoFavorecido(PgitUtil.concatenarCampos(registroSelecionado.getBancoCreditoFormatado(), saidaDTO
						.getDsBancoFavorecido()));
		setDsAgenciaFavorecido(PgitUtil.concatenarCampos(registroSelecionado.getAgenciaCreditoFormatada(), saidaDTO
						.getDsAgenciaFavorecido()));
		setDsContaFavorecido(registroSelecionado.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(registroSelecionado.getDsResumoProdutoServico());
		setDsIndicadorModalidade(registroSelecionado.getDsOperacaoProdutoServico());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(registroSelecionado.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(registroSelecionado.getDtCreditoPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlrAgendamento());
		setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
		setCdSituacaoOperacaoPagamento(registroSelecionado.getDsSituacaoOperacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.concatenarCampos(registroSelecionado.getBancoCreditoFormatado(), saidaDTO
						.getDsBancoFavorecido()));
		setAgenciaDigitoCredito(PgitUtil.concatenarCampos(registroSelecionado.getAgenciaCreditoFormatada(), saidaDTO
						.getDsAgenciaFavorecido()));
		setContaDigitoCredito(registroSelecionado.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());

		// trilhaAuditoria
		setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
		setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
		setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - DOC
	/**
	 * Preenche dados pagto doc.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoDOC(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoDOCEntradaDTO entradaDTO = new DetalharPagtoDOCEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoDOC(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
						.formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
							.getNmInscriFavorecido()));
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			setNumeroInscricaoBeneficiario(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
							.getNmInscriBeneficio()));
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| !PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals("")
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
		setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
		setIdentificacaoTransferenciaDoc(saidaDTO.getDsIdentificacaoTransferenciaPagto());
		setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlDeducao(saidaDTO.getVlDeducao());
		setVlAcrescimo(saidaDTO.getVlAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
		setVlIss(saidaDTO.getVlIss());
		setVlIof(saidaDTO.getVlIof());
		setVlInss(saidaDTO.getVlInss());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - TED
	/**
	 * Preenche dados pagto ted.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoTED(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoTEDEntradaDTO entradaDTO = new DetalharPagtoTEDEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTEDSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoTED(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido());
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
		setFinalidadeTed(saidaDTO.getDsFinalidadeDocTed());
		setIdentificacaoTransferenciaTed(saidaDTO.getDsIdentificacaoTransferenciaPagto());
		setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatidoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlIrCredito());
		setVlIss(saidaDTO.getVlIssCredito());
		setVlIof(saidaDTO.getVlIofCredito());
		setVlInss(saidaDTO.getVlInssCredito());
		setCdIndentificadorJudicial(saidaDTO.getCdIndentificadorJudicial());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - Ordem de Pagamento
	/**
	 * Preenche dados pagto ordem pagto.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoOrdemPagto(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharPagtoOrdemPagtoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoOrdemPagtoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoOrdemPagto(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		limparFavorecidoBeneficiario();
		exibeBeneficiario = false;
		if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
			setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
			setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
			setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
			setDsFavorecido(saidaDTO.getDsNomeFavorecido());
			setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null
							: (saidaDTO.getCdFavorecido()));
			setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
			setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
			setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
			setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
			setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
		} else {
			setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
			setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
			setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
			exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
							|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
							|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
			setExibeDcom(false);
		}

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
		setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
		setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setNumeroOp(saidaDTO.getNrOperacao());
		setDataExpiracaoCredito(saidaDTO.getDtExpedicaoCredito());
		setInstrucaoPagamento(saidaDTO.getCdInsPagamento());
		setNumeroCheque(saidaDTO.getNrCheque());
		setSerieCheque(saidaDTO.getNrSerieCheque());
		setIndicadorAssociadoUnicaAgencia(saidaDTO.getDsUnidadeAgencia());

		setIdentificadorTipoRetirada(saidaDTO.getDsIdentificacaoTipoRetirada());
		setIdentificadorRetirada(saidaDTO.getDsIdentificacaoRetirada());

		setDataRetirada(saidaDTO.getDtRetirada());
		setVlImpostoMovFinanceira(saidaDTO.getVlImpostoMovimentacaoFinanceira());
		setVlDesconto(saidaDTO.getVlDescontoCredito());
		setVlAbatimento(saidaDTO.getVlAbatidoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
		setVlImpostoRenda(saidaDTO.getVlIrCredito());
		setVlIss(saidaDTO.getVlIssCredito());
		setVlIof(saidaDTO.getVlIofCredito());
		setVlInss(saidaDTO.getVlInssCredito());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
		setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - Titulos Bradesco
	/**
	 * Preenche dados pagto titulo bradesco.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoTituloBradesco(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharPagtoTituloBradescoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoTituloBradesco(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCodigoBarras(saidaDTO.getCdBarras());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
		setNossoNumero(saidaDTO.getNossoNumero());
		setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
		setCarteira(saidaDTO.getCarteira());
		setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
		setDsRastreado(saidaDTO.getCdRastreado() == 1 ? "Sim":"N�o");

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - Titulos Outros Bancos
	/**
	 * Preenche dados pagto titulo outros bancos.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoTituloOutrosBancos(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharPagtoTituloOutrosBancosEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoTituloOutrosBancos(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCodigoBarras(saidaDTO.getCdBarras());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlDeducao(saidaDTO.getVlOutrasDeducoes());
		setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
		setNossoNumero(saidaDTO.getNossoNumero());
		setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
		setCarteira(saidaDTO.getCarteira());
		setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
		setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
		setDsRastreado(saidaDTO.getCdRastreado() == 1 ? "Sim":"N�o");

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - DARF
	/**
	 * Preenche dados pagto darf.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoDARF(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoDARFEntradaDTO entradaDTO = new DetalharPagtoDARFEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDARF(entradaDTO);

		// Cabe�alho
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddTelefone());
		setTelefoneContribuinte(saidaDTO.getCdTelefone());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
		setAgenteArrecadador(saidaDTO.getCdAgenteArrecad());
		setCodigoReceita(saidaDTO.getCdReceitaTributo());
		setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcela());
		setPercentualReceita(saidaDTO.getCdPercentualReceita());
		setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
		setAnoExercicio(saidaDTO.getCdDanoExercicio());
		setDataReferencia(saidaDTO.getDtReferenciaTributo());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
		setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
		setVlMulta(saidaDTO.getVlMultaCredito());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlTotal(saidaDTO.getVlTotalTributo());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - GARE
	/**
	 * Preenche dados pagto gare.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoGARE(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoGAREEntradaDTO entradaDTO = new DetalharPagtoGAREEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoGARE(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddContribuinte());
		setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadualContribuinte());
		setCodigoReceita(saidaDTO.getCdReceita());
		setNumeroReferencia(saidaDTO.getNrReferencia());
		setNumeroCotaParcela(saidaDTO.getNrCota());
		setPercentualReceita(saidaDTO.getCdPercentualReceita());
		setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
		setDataReferencia(saidaDTO.getDtReferencia());
		setCodigoCnae(saidaDTO.getCdCnae());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setNumeroParcelamento(saidaDTO.getNrParcela());
		setCodigoOrgaoFavorecido(saidaDTO.getCdOrFavorecido());
		setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
		setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
		setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
		setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setObservacaoTributo(saidaDTO.getDsTributo());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - GPS
	/**
	 * Preenche dados pagto gps.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoGPS(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoGPSEntradaDTO entradaDTO = new DetalharPagtoGPSEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoGPS(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddContribuinte());
		setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
		setCodigoInss(saidaDTO.getCdInss());
		setDataCompetencia(saidaDTO.getDsMesAnoCompt());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - C�digo de Barras
	/**
	 * Preenche dados pagto codigo barras.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoCodigoBarras(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharPagtoCodigoBarrasEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoCodigoBarras(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setDddContribuinte(saidaDTO.getCdDddTelefone());
		setTelefoneContribuinte(saidaDTO.getNrTelefone());
		setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
		setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
		setCodigoReceita(saidaDTO.getCdReceitaTributo());
		setAgenteArrecadador(saidaDTO.getCdAgenteArrecadador());
		setCodigoTributo(saidaDTO.getCdTributoArrecadado());
		setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTributo());
		setPercentualReceita(saidaDTO.getCdPercentualTributo());
		setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
		setAnoExercicio(saidaDTO.getCdAnoExercicioTributo());
		setDataReferencia(saidaDTO.getDtReferenciaTributo());
		setDataCompetencia(saidaDTO.getDtCompensacao());
		setCodigoCnae(saidaDTO.getCdCnae());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setNumeroParcelamento(saidaDTO.getNrParcelamentoTributo());
		setCodigoOrgaoFavorecido(saidaDTO.getCdOrgaoFavorecido());
		setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
		setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
		setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
		setVlReceita(saidaDTO.getVlReceita());
		setVlPrincipal(saidaDTO.getVlPrincipal());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
		setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMora());
		setVlTotal(saidaDTO.getVlTotal());
		setObservacaoTributo(saidaDTO.getDsObservacao());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
		
		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - D�bito de Veiculos
	/**
	 * Preenche dados pagto debito veiculos.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoDebitoVeiculos(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharPagtoDebitoVeiculosEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDebitoVeiculos(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTrib());
		setAnoExercicio(saidaDTO.getDtAnoExercTributo());
		setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
		setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
		setMunicipioTributo(saidaDTO.getCdMunicArrecadTrib());
		setUfTributo(saidaDTO.getCdUfTributo());
		setNumeroNsu(saidaDTO.getNrNsuTributo());
		setOpcaoTributo(saidaDTO.getCdOpcaoTributo());
		setOpcaoRetiradaTributo(saidaDTO.getCdOpcaoRetirada());
		setVlPrincipal(saidaDTO.getVlPrincipalTributo());
		setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
		setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
		setVlDesconto(saidaDTO.getVlDesconto());
		setVlAbatimento(saidaDTO.getVlAbatimento());
		setVlMulta(saidaDTO.getVlMulta());
		setVlMora(saidaDTO.getVlMoraJuros());
		setVlTotal(saidaDTO.getVlTotalTributo());
		setObservacaoTributo(saidaDTO.getDsObservacaoTributo());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - Deposito Identificado
	/**
	 * Preenche dados pagto dep identificado.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoDepIdentificado(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharPagtoDepIdentificadoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoDepIdentificado(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Beneficiario
		setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
						.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil
						.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setClienteDepositoIdentificado(saidaDTO.getCdClienteDepositoId());
		setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
		setProdutoDepositoIdentificado(saidaDTO.getCdProdutoDepositoId());
		setSequenciaProdutoDepositoIdentificado(saidaDTO.getCdSequenciaProdutoDepositoId());
		setBancoCartaoDepositoIdentificado(saidaDTO.getCdBancoCartaoDepositanteId());
		setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
		setBimCartaoDepositoIdentificado(saidaDTO.getCdBinCartaoDepositanteId());
		setViaCartaoDepositoIdentificado(saidaDTO.getCdViaCartaoDepositanteId());
		setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
		setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
		setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
		setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
		setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
		setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
		setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
		setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
		setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
		setVlIss(saidaDTO.getVlIssCreditoPagamento());
		setVlIof(saidaDTO.getVlIofCreditoPagamento());
		setVlInss(saidaDTO.getVlInssCreditoPagamento());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	// Detalhar Pagamentos Individuais Pendentes - Ordem de Cr�dito
	/**
	 * Preenche dados pagto ordem credito.
	 *
	 * @param registroSelecionado the registro selecionado
	 */
	public void preencheDadosPagtoOrdemCredito(ConsultarPagtoPendIndividualSaidaDTO registroSelecionado) {
		DetalharPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharPagtoOrdemCreditoEntradaDTO();

		entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdEmpresa());
		entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContrato());
		entradaDTO.setDsEfetivacaoPagamento("");
		entradaDTO.setCdControlePagamento(registroSelecionado.getCdControlePagamento());
		entradaDTO.setCdBancoDebito(registroSelecionado.getCdBancoDebito());
		entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgenciaDebito());
		entradaDTO.setCdContaDebito(registroSelecionado.getCdContaDebito());
		entradaDTO.setCdBancoCredito(registroSelecionado.getCdBancoCredito());
		entradaDTO.setCdAgenciaCredito(registroSelecionado.getCdAgenciaCredito());
		entradaDTO.setCdContaCredito(registroSelecionado.getCdContaCredito());
		entradaDTO.setCdAgendadosPagoNaoPago(1);
		entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoOperacao());
		entradaDTO.setDtHoraManutencao("");

		DetalharPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
						.detalharPagtoOrdemCredito(entradaDTO);

		// CLIENTE
		setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
		setDsRazaoSocial(saidaDTO.getNomeCliente());
		setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
		setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
		setDsContrato(saidaDTO.getDsContrato());
		setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

		// CONTA D�BITO
		setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
		setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
		setContaDebito(saidaDTO.getContaDebitoFormatada());
		setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

		// Beneficiario
		setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		// Favorecido
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null : (saidaDTO
						.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

		// Pagamento
		setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
		setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
		setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
		setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
		setNumeroPagamento(saidaDTO.getCdControlePagamento());
		setCdListaDebito(saidaDTO.getCdListaDebito());
		setDtAgendamento(saidaDTO.getDtAgendamento());
		setDtPagamento(saidaDTO.getDtPagamento());
		setDtVencimento(saidaDTO.getDtVencimento());
		setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
		setQtMoeda(saidaDTO.getQtMoeda());
		setVlrAgendamento(saidaDTO.getVlAgendado());
		setVlrEfetivacao(saidaDTO.getVlEfetivo());
		setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
		setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
		setDsPagamento(saidaDTO.getDsPagamento());
		setNrDocumento(saidaDTO.getNrDocumento());
		setTipoDocumento(saidaDTO.getDsTipoDocumento());
		setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
		setVlDocumento(saidaDTO.getVlDocumento());
		setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
		setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
		setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
		setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
		setCdBancoCredito(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
		setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
						.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setContaDigitoCredito(PgitUtil
						.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(), false));
		setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
		setCodigoPagador(saidaDTO.getCdPagador());
		setCodigoOrdemCredito(saidaDTO.getCdOrdemCreditoPagamento());
		setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
		setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
		setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
		setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
		setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
		setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
		setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
		setVlIss(saidaDTO.getVlIssCreditoPagamento());
		setVlIof(saidaDTO.getVlIofCreditoPagamento());
		setVlInss(saidaDTO.getVlInssCreditoPagamento());
		setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());

		// trilhaAuditoria
		setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
		setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
		setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
		setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
		setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/** * GETTERS E SETTERS ** */

	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 *
	 * @param cdSituacaoContrato the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: contaDebito.
	 *
	 * @return contaDebito
	 */
	public String getContaDebito() {
		return contaDebito;
	}

	/**
	 * Set: contaDebito.
	 *
	 * @param contaDebito the conta debito
	 */
	public void setContaDebito(String contaDebito) {
		this.contaDebito = contaDebito;
	}

	/**
	 * Get: dsAgenciaDebito.
	 *
	 * @return dsAgenciaDebito
	 */
	public String getDsAgenciaDebito() {
		return dsAgenciaDebito;
	}

	/**
	 * Get: telaLiberarProcessamento.
	 *
	 * @return telaLiberarProcessamento
	 */
	public static String getTelaLiberarProcessamento() {
		return TELA_LIBERAR_PROCESSAMENTO;
	}

	/**
	 * Get: telaLiberarPendencia.
	 *
	 * @return telaLiberarPendencia
	 */
	public static String getTelaLiberarPendencia() {
		return TELA_LIBERAR_PENDENCIA;
	}

	/**
	 * Get: telaConsultar.
	 *
	 * @return telaConsultar
	 */
	public static String getTelaConsultar() {
		return TELA_CONSULTAR;
	}

	/**
	 * Get: telaDetalheCreditoConta.
	 *
	 * @return telaDetalheCreditoConta
	 */
	public static String getTelaDetalheCreditoConta() {
		return TELA_DETALHE_CREDITO_CONTA;
	}

	/**
	 * Get: telaDetalheDebitoConta.
	 *
	 * @return telaDetalheDebitoConta
	 */
	public static String getTelaDetalheDebitoConta() {
		return TELA_DETALHE_DEBITO_CONTA;
	}

	/**
	 * Get: telaDetalheDoc.
	 *
	 * @return telaDetalheDoc
	 */
	public static String getTelaDetalheDoc() {
		return TELA_DETALHE_DOC;
	}

	/**
	 * Get: telaDetalheTed.
	 *
	 * @return telaDetalheTed
	 */
	public static String getTelaDetalheTed() {
		return TELA_DETALHE_TED;
	}

	/**
	 * Get: telaDetalheOrdemPagto.
	 *
	 * @return telaDetalheOrdemPagto
	 */
	public static String getTelaDetalheOrdemPagto() {
		return TELA_DETALHE_ORDEM_PAGTO;
	}

	/**
	 * Get: telaDetalheTitulosBradesco.
	 *
	 * @return telaDetalheTitulosBradesco
	 */
	public static String getTelaDetalheTitulosBradesco() {
		return TELA_DETALHE_TITULOS_BRADESCO;
	}

	/**
	 * Get: telaDetalheTitulosOutrosBancos.
	 *
	 * @return telaDetalheTitulosOutrosBancos
	 */
	public static String getTelaDetalheTitulosOutrosBancos() {
		return TELA_DETALHE_TITULOS_OUTROS_BANCOS;
	}

	/**
	 * Get: telaDetalheDarf.
	 *
	 * @return telaDetalheDarf
	 */
	public static String getTelaDetalheDarf() {
		return TELA_DETALHE_DARF;
	}

	/**
	 * Get: telaDetalheGare.
	 *
	 * @return telaDetalheGare
	 */
	public static String getTelaDetalheGare() {
		return TELA_DETALHE_GARE;
	}

	/**
	 * Get: telaDetalheGps.
	 *
	 * @return telaDetalheGps
	 */
	public static String getTelaDetalheGps() {
		return TELA_DETALHE_GPS;
	}

	/**
	 * Get: telaDetalheCodigoBarras.
	 *
	 * @return telaDetalheCodigoBarras
	 */
	public static String getTelaDetalheCodigoBarras() {
		return TELA_DETALHE_CODIGO_BARRAS;
	}

	/**
	 * Get: telaDetalheDebitoVeiculos.
	 *
	 * @return telaDetalheDebitoVeiculos
	 */
	public static String getTelaDetalheDebitoVeiculos() {
		return TELA_DETALHE_DEBITO_VEICULOS;
	}

	/**
	 * Get: telaDetalheDepositoIdentificado.
	 *
	 * @return telaDetalheDepositoIdentificado
	 */
	public static String getTelaDetalheDepositoIdentificado() {
		return TELA_DETALHE_DEPOSITO_IDENTIFICADO;
	}

	/**
	 * Get: telaDetalheOrdemCredito.
	 *
	 * @return telaDetalheOrdemCredito
	 */
	public static String getTelaDetalheOrdemCredito() {
		return TELA_DETALHE_ORDEM_CREDITO;
	}

	/**
	 * Set: dsAgenciaDebito.
	 *
	 * @param dsAgenciaDebito the ds agencia debito
	 */
	public void setDsAgenciaDebito(String dsAgenciaDebito) {
		this.dsAgenciaDebito = dsAgenciaDebito;
	}

	/**
	 * Get: dsBancoDebito.
	 *
	 * @return dsBancoDebito
	 */
	public String getDsBancoDebito() {
		return dsBancoDebito;
	}

	/**
	 * Set: dsBancoDebito.
	 *
	 * @param dsBancoDebito the ds banco debito
	 */
	public void setDsBancoDebito(String dsBancoDebito) {
		this.dsBancoDebito = dsBancoDebito;
	}

	/**
	 * Get: dsContrato.
	 *
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 *
	 * @param dsContrato the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmpresa.
	 *
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 *
	 * @param dsEmpresa the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsRazaoSocial.
	 *
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 *
	 * @param dsRazaoSocial the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: liberarPagtosIndividuaisPendImpl.
	 *
	 * @return liberarPagtosIndividuaisPendImpl
	 */
	public ILiberarPagtosIndividuaisPendService getLiberarPagtosIndividuaisPendImpl() {
		return liberarPagtosIndividuaisPendImpl;
	}

	/**
	 * Set: liberarPagtosIndividuaisPendImpl.
	 *
	 * @param liberarPagtosIndividuaisPendImpl the liberar pagtos individuais pend impl
	 */
	public void setLiberarPagtosIndividuaisPendImpl(
					ILiberarPagtosIndividuaisPendService liberarPagtosIndividuaisPendImpl) {
		this.liberarPagtosIndividuaisPendImpl = liberarPagtosIndividuaisPendImpl;
	}

	/**
	 * Get: listaControleLiberarPagtos.
	 *
	 * @return listaControleLiberarPagtos
	 */
	public List<SelectItem> getListaControleLiberarPagtos() {
		return listaControleLiberarPagtos;
	}

	/**
	 * Set: listaControleLiberarPagtos.
	 *
	 * @param listaControleLiberarPagtos the lista controle liberar pagtos
	 */
	public void setListaControleLiberarPagtos(List<SelectItem> listaControleLiberarPagtos) {
		this.listaControleLiberarPagtos = listaControleLiberarPagtos;
	}

	/**
	 * Get: listaGridLiberarPagtos.
	 *
	 * @return listaGridLiberarPagtos
	 */
	public List<ConsultarPagtoPendIndividualSaidaDTO> getListaGridLiberarPagtos() {
		return listaGridLiberarPagtos;
	}

	/**
	 * Set: listaGridLiberarPagtos.
	 *
	 * @param listaGridLiberarPagtos the lista grid liberar pagtos
	 */
	public void setListaGridLiberarPagtos(List<ConsultarPagtoPendIndividualSaidaDTO> listaGridLiberarPagtos) {
		this.listaGridLiberarPagtos = listaGridLiberarPagtos;
	}

	/**
	 * Get: listaGridLiberarPagtosSelecionados.
	 *
	 * @return listaGridLiberarPagtosSelecionados
	 */
	public List<ConsultarPagtoPendIndividualSaidaDTO> getListaGridLiberarPagtosSelecionados() {
		return listaGridLiberarPagtosSelecionados;
	}

	/**
	 * Set: listaGridLiberarPagtosSelecionados.
	 *
	 * @param listaGridLiberarPagtosSelecionados the lista grid liberar pagtos selecionados
	 */
	public void setListaGridLiberarPagtosSelecionados(
					List<ConsultarPagtoPendIndividualSaidaDTO> listaGridLiberarPagtosSelecionados) {
		this.listaGridLiberarPagtosSelecionados = listaGridLiberarPagtosSelecionados;
	}

	/**
	 * Get: nrCnpjCpf.
	 *
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 *
	 * @param nrCnpjCpf the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 *
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 *
	 * @param nroContrato the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Is opcao checar todos.
	 *
	 * @return true, if is opcao checar todos
	 */
	public boolean isOpcaoChecarTodos() {
		return opcaoChecarTodos;
	}

	/**
	 * Set: opcaoChecarTodos.
	 *
	 * @param opcaoChecarTodos the opcao checar todos
	 */
	public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
		this.opcaoChecarTodos = opcaoChecarTodos;
	}

	/**
	 * Get: tipoContaDebito.
	 *
	 * @return tipoContaDebito
	 */
	public String getTipoContaDebito() {
		return tipoContaDebito;
	}

	/**
	 * Set: tipoContaDebito.
	 *
	 * @param tipoContaDebito the tipo conta debito
	 */
	public void setTipoContaDebito(String tipoContaDebito) {
		this.tipoContaDebito = tipoContaDebito;
	}

	/**
	 * Get: agenciaDigitoCredito.
	 *
	 * @return agenciaDigitoCredito
	 */
	public String getAgenciaDigitoCredito() {
		return agenciaDigitoCredito;
	}

	/**
	 * Set: agenciaDigitoCredito.
	 *
	 * @param agenciaDigitoCredito the agencia digito credito
	 */
	public void setAgenciaDigitoCredito(String agenciaDigitoCredito) {
		this.agenciaDigitoCredito = agenciaDigitoCredito;
	}

	/**
	 * Get: agenciaDigitoDestino.
	 *
	 * @return agenciaDigitoDestino
	 */
	public String getAgenciaDigitoDestino() {
		return agenciaDigitoDestino;
	}

	/**
	 * Set: agenciaDigitoDestino.
	 *
	 * @param agenciaDigitoDestino the agencia digito destino
	 */
	public void setAgenciaDigitoDestino(String agenciaDigitoDestino) {
		this.agenciaDigitoDestino = agenciaDigitoDestino;
	}

	/**
	 * Get: agenteArrecadador.
	 *
	 * @return agenteArrecadador
	 */
	public String getAgenteArrecadador() {
		return agenteArrecadador;
	}

	/**
	 * Set: agenteArrecadador.
	 *
	 * @param agenteArrecadador the agente arrecadador
	 */
	public void setAgenteArrecadador(String agenteArrecadador) {
		this.agenteArrecadador = agenteArrecadador;
	}

	/**
	 * Get: anoExercicio.
	 *
	 * @return anoExercicio
	 */
	public String getAnoExercicio() {
		return anoExercicio;
	}

	/**
	 * Set: anoExercicio.
	 *
	 * @param anoExercicio the ano exercicio
	 */
	public void setAnoExercicio(String anoExercicio) {
		this.anoExercicio = anoExercicio;
	}

	/**
	 * Get: bancoCartaoDepositoIdentificado.
	 *
	 * @return bancoCartaoDepositoIdentificado
	 */
	public String getBancoCartaoDepositoIdentificado() {
		return bancoCartaoDepositoIdentificado;
	}

	/**
	 * Set: bancoCartaoDepositoIdentificado.
	 *
	 * @param bancoCartaoDepositoIdentificado the banco cartao deposito identificado
	 */
	public void setBancoCartaoDepositoIdentificado(String bancoCartaoDepositoIdentificado) {
		this.bancoCartaoDepositoIdentificado = bancoCartaoDepositoIdentificado;
	}

	/**
	 * Get: bimCartaoDepositoIdentificado.
	 *
	 * @return bimCartaoDepositoIdentificado
	 */
	public String getBimCartaoDepositoIdentificado() {
		return bimCartaoDepositoIdentificado;
	}

	/**
	 * Set: bimCartaoDepositoIdentificado.
	 *
	 * @param bimCartaoDepositoIdentificado the bim cartao deposito identificado
	 */
	public void setBimCartaoDepositoIdentificado(String bimCartaoDepositoIdentificado) {
		this.bimCartaoDepositoIdentificado = bimCartaoDepositoIdentificado;
	}

	/**
	 * Get: camaraCentralizadora.
	 *
	 * @return camaraCentralizadora
	 */
	public String getCamaraCentralizadora() {
		return camaraCentralizadora;
	}

	/**
	 * Set: camaraCentralizadora.
	 *
	 * @param camaraCentralizadora the camara centralizadora
	 */
	public void setCamaraCentralizadora(String camaraCentralizadora) {
		this.camaraCentralizadora = camaraCentralizadora;
	}

	/**
	 * Get: canal.
	 *
	 * @return canal
	 */
	public String getCanal() {
		return canal;
	}

	/**
	 * Set: canal.
	 *
	 * @param canal the canal
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}

	/**
	 * Get: cartaoDepositoIdentificado.
	 *
	 * @return cartaoDepositoIdentificado
	 */
	public String getCartaoDepositoIdentificado() {
		return cartaoDepositoIdentificado;
	}

	/**
	 * Set: cartaoDepositoIdentificado.
	 *
	 * @param cartaoDepositoIdentificado the cartao deposito identificado
	 */
	public void setCartaoDepositoIdentificado(String cartaoDepositoIdentificado) {
		this.cartaoDepositoIdentificado = cartaoDepositoIdentificado;
	}

	/**
	 * Get: cdBancoCredito.
	 *
	 * @return cdBancoCredito
	 */
	public String getCdBancoCredito() {
		return cdBancoCredito;
	}

	/**
	 * Set: cdBancoCredito.
	 *
	 * @param cdBancoCredito the cd banco credito
	 */
	public void setCdBancoCredito(String cdBancoCredito) {
		this.cdBancoCredito = cdBancoCredito;
	}

	/**
	 * Get: cdBancoDestino.
	 *
	 * @return cdBancoDestino
	 */
	public String getCdBancoDestino() {
		return cdBancoDestino;
	}

	/**
	 * Set: cdBancoDestino.
	 *
	 * @param cdBancoDestino the cd banco destino
	 */
	public void setCdBancoDestino(String cdBancoDestino) {
		this.cdBancoDestino = cdBancoDestino;
	}

	/**
	 * Get: cdFavorecido.
	 *
	 * @return cdFavorecido
	 */
	public String getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 *
	 * @param cdFavorecido the cd favorecido
	 */
	public void setCdFavorecido(String cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: cdIndicadorEconomicoMoeda.
	 *
	 * @return cdIndicadorEconomicoMoeda
	 */
	public String getCdIndicadorEconomicoMoeda() {
		return cdIndicadorEconomicoMoeda;
	}

	/**
	 * Set: cdIndicadorEconomicoMoeda.
	 *
	 * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
	 */
	public void setCdIndicadorEconomicoMoeda(String cdIndicadorEconomicoMoeda) {
		this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
	}

	/**
	 * Get: cdListaDebito.
	 *
	 * @return cdListaDebito
	 */
	public String getCdListaDebito() {
		return cdListaDebito;
	}

	/**
	 * Set: cdListaDebito.
	 *
	 * @param cdListaDebito the cd lista debito
	 */
	public void setCdListaDebito(String cdListaDebito) {
		this.cdListaDebito = cdListaDebito;
	}

	/**
	 * Get: cdMotivoSituacaoPagamento.
	 *
	 * @return cdMotivoSituacaoPagamento
	 */
	public Integer getCdMotivoSituacaoPagamento() {
		return cdMotivoSituacaoPagamento;
	}

	/**
	 * Set: cdMotivoSituacaoPagamento.
	 *
	 * @param cdMotivoSituacaoPagamento the cd motivo situacao pagamento
	 */
	public void setCdMotivoSituacaoPagamento(Integer cdMotivoSituacaoPagamento) {
		this.cdMotivoSituacaoPagamento = cdMotivoSituacaoPagamento;
	}

	/**
	 * Get: cdSerieDocumento.
	 *
	 * @return cdSerieDocumento
	 */
	public String getCdSerieDocumento() {
		return cdSerieDocumento;
	}

	/**
	 * Set: cdSerieDocumento.
	 *
	 * @param cdSerieDocumento the cd serie documento
	 */
	public void setCdSerieDocumento(String cdSerieDocumento) {
		this.cdSerieDocumento = cdSerieDocumento;
	}

	/**
	 * Get: cdSituacaoOperacaoPagamento.
	 *
	 * @return cdSituacaoOperacaoPagamento
	 */
	public String getCdSituacaoOperacaoPagamento() {
		return cdSituacaoOperacaoPagamento;
	}

	/**
	 * Set: cdSituacaoOperacaoPagamento.
	 *
	 * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
	 */
	public void setCdSituacaoOperacaoPagamento(String cdSituacaoOperacaoPagamento) {
		this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
	}

	/**
	 * Get: cdTipoBeneficiario.
	 *
	 * @return cdTipoBeneficiario
	 */
	public Integer getCdTipoBeneficiario() {
		return cdTipoBeneficiario;
	}

	/**
	 * Set: cdTipoBeneficiario.
	 *
	 * @param cdTipoBeneficiario the cd tipo beneficiario
	 */
	public void setCdTipoBeneficiario(Integer cdTipoBeneficiario) {
		this.cdTipoBeneficiario = cdTipoBeneficiario;
	}

	/**
	 * Get: cdTipoContaFavorecido.
	 *
	 * @return cdTipoContaFavorecido
	 */
	public Integer getCdTipoContaFavorecido() {
		return cdTipoContaFavorecido;
	}

	/**
	 * Set: cdTipoContaFavorecido.
	 *
	 * @param cdTipoContaFavorecido the cd tipo conta favorecido
	 */
	public void setCdTipoContaFavorecido(Integer cdTipoContaFavorecido) {
		this.cdTipoContaFavorecido = cdTipoContaFavorecido;
	}

	/**
	 * Get: clienteDepositoIdentificado.
	 *
	 * @return clienteDepositoIdentificado
	 */
	public String getClienteDepositoIdentificado() {
		return clienteDepositoIdentificado;
	}

	/**
	 * Set: clienteDepositoIdentificado.
	 *
	 * @param clienteDepositoIdentificado the cliente deposito identificado
	 */
	public void setClienteDepositoIdentificado(String clienteDepositoIdentificado) {
		this.clienteDepositoIdentificado = clienteDepositoIdentificado;
	}

	/**
	 * Get: codigoBarras.
	 *
	 * @return codigoBarras
	 */
	public String getCodigoBarras() {
		return codigoBarras;
	}

	/**
	 * Set: codigoBarras.
	 *
	 * @param codigoBarras the codigo barras
	 */
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	/**
	 * Get: codigoCnae.
	 *
	 * @return codigoCnae
	 */
	public String getCodigoCnae() {
		return codigoCnae;
	}

	/**
	 * Set: codigoCnae.
	 *
	 * @param codigoCnae the codigo cnae
	 */
	public void setCodigoCnae(String codigoCnae) {
		this.codigoCnae = codigoCnae;
	}

	/**
	 * Get: codigoDepositante.
	 *
	 * @return codigoDepositante
	 */
	public String getCodigoDepositante() {
		return codigoDepositante;
	}

	/**
	 * Set: codigoDepositante.
	 *
	 * @param codigoDepositante the codigo depositante
	 */
	public void setCodigoDepositante(String codigoDepositante) {
		this.codigoDepositante = codigoDepositante;
	}

	/**
	 * Get: codigoInss.
	 *
	 * @return codigoInss
	 */
	public String getCodigoInss() {
		return codigoInss;
	}

	/**
	 * Set: codigoInss.
	 *
	 * @param codigoInss the codigo inss
	 */
	public void setCodigoInss(String codigoInss) {
		this.codigoInss = codigoInss;
	}

	/**
	 * Get: codigoOrdemCredito.
	 *
	 * @return codigoOrdemCredito
	 */
	public String getCodigoOrdemCredito() {
		return codigoOrdemCredito;
	}

	/**
	 * Set: codigoOrdemCredito.
	 *
	 * @param codigoOrdemCredito the codigo ordem credito
	 */
	public void setCodigoOrdemCredito(String codigoOrdemCredito) {
		this.codigoOrdemCredito = codigoOrdemCredito;
	}

	/**
	 * Get: codigoOrgaoFavorecido.
	 *
	 * @return codigoOrgaoFavorecido
	 */
	public String getCodigoOrgaoFavorecido() {
		return codigoOrgaoFavorecido;
	}

	/**
	 * Set: codigoOrgaoFavorecido.
	 *
	 * @param codigoOrgaoFavorecido the codigo orgao favorecido
	 */
	public void setCodigoOrgaoFavorecido(String codigoOrgaoFavorecido) {
		this.codigoOrgaoFavorecido = codigoOrgaoFavorecido;
	}

	/**
	 * Get: codigoPagador.
	 *
	 * @return codigoPagador
	 */
	public String getCodigoPagador() {
		return codigoPagador;
	}

	/**
	 * Set: codigoPagador.
	 *
	 * @param codigoPagador the codigo pagador
	 */
	public void setCodigoPagador(String codigoPagador) {
		this.codigoPagador = codigoPagador;
	}

	/**
	 * Get: codigoReceita.
	 *
	 * @return codigoReceita
	 */
	public String getCodigoReceita() {
		return codigoReceita;
	}

	/**
	 * Set: codigoReceita.
	 *
	 * @param codigoReceita the codigo receita
	 */
	public void setCodigoReceita(String codigoReceita) {
		this.codigoReceita = codigoReceita;
	}

	/**
	 * Get: codigoRenavam.
	 *
	 * @return codigoRenavam
	 */
	public String getCodigoRenavam() {
		return codigoRenavam;
	}

	/**
	 * Set: codigoRenavam.
	 *
	 * @param codigoRenavam the codigo renavam
	 */
	public void setCodigoRenavam(String codigoRenavam) {
		this.codigoRenavam = codigoRenavam;
	}

	/**
	 * Get: codigoTributo.
	 *
	 * @return codigoTributo
	 */
	public String getCodigoTributo() {
		return codigoTributo;
	}

	/**
	 * Set: codigoTributo.
	 *
	 * @param codigoTributo the codigo tributo
	 */
	public void setCodigoTributo(String codigoTributo) {
		this.codigoTributo = codigoTributo;
	}

	/**
	 * Get: codigoUfFavorecida.
	 *
	 * @return codigoUfFavorecida
	 */
	public String getCodigoUfFavorecida() {
		return codigoUfFavorecida;
	}

	/**
	 * Set: codigoUfFavorecida.
	 *
	 * @param codigoUfFavorecida the codigo uf favorecida
	 */
	public void setCodigoUfFavorecida(String codigoUfFavorecida) {
		this.codigoUfFavorecida = codigoUfFavorecida;
	}

	/**
	 * Get: contaDigitoCredito.
	 *
	 * @return contaDigitoCredito
	 */
	public String getContaDigitoCredito() {
		return contaDigitoCredito;
	}

	/**
	 * Set: contaDigitoCredito.
	 *
	 * @param contaDigitoCredito the conta digito credito
	 */
	public void setContaDigitoCredito(String contaDigitoCredito) {
		this.contaDigitoCredito = contaDigitoCredito;
	}

	/**
	 * Get: contaDigitoDestino.
	 *
	 * @return contaDigitoDestino
	 */
	public String getContaDigitoDestino() {
		return contaDigitoDestino;
	}

	/**
	 * Set: contaDigitoDestino.
	 *
	 * @param contaDigitoDestino the conta digito destino
	 */
	public void setContaDigitoDestino(String contaDigitoDestino) {
		this.contaDigitoDestino = contaDigitoDestino;
	}

	/**
	 * Get: dataCompetencia.
	 *
	 * @return dataCompetencia
	 */
	public String getDataCompetencia() {
		return dataCompetencia;
	}

	/**
	 * Set: dataCompetencia.
	 *
	 * @param dataCompetencia the data competencia
	 */
	public void setDataCompetencia(String dataCompetencia) {
		this.dataCompetencia = dataCompetencia;
	}

	/**
	 * Get: dataExpiracaoCredito.
	 *
	 * @return dataExpiracaoCredito
	 */
	public String getDataExpiracaoCredito() {
		return dataExpiracaoCredito;
	}

	/**
	 * Set: dataExpiracaoCredito.
	 *
	 * @param dataExpiracaoCredito the data expiracao credito
	 */
	public void setDataExpiracaoCredito(String dataExpiracaoCredito) {
		this.dataExpiracaoCredito = dataExpiracaoCredito;
	}

	/**
	 * Get: dataReferencia.
	 *
	 * @return dataReferencia
	 */
	public String getDataReferencia() {
		return dataReferencia;
	}

	/**
	 * Set: dataReferencia.
	 *
	 * @param dataReferencia the data referencia
	 */
	public void setDataReferencia(String dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	/**
	 * Get: dataRetirada.
	 *
	 * @return dataRetirada
	 */
	public String getDataRetirada() {
		return dataRetirada;
	}

	/**
	 * Set: dataRetirada.
	 *
	 * @param dataRetirada the data retirada
	 */
	public void setDataRetirada(String dataRetirada) {
		this.dataRetirada = dataRetirada;
	}

	/**
	 * Get: dddContribuinte.
	 *
	 * @return dddContribuinte
	 */
	public String getDddContribuinte() {
		return dddContribuinte;
	}

	/**
	 * Set: dddContribuinte.
	 *
	 * @param dddContribuinte the ddd contribuinte
	 */
	public void setDddContribuinte(String dddContribuinte) {
		this.dddContribuinte = dddContribuinte;
	}

	/**
	 * Get: descricaoUfFavorecida.
	 *
	 * @return descricaoUfFavorecida
	 */
	public String getDescricaoUfFavorecida() {
		return descricaoUfFavorecida;
	}

	/**
	 * Set: descricaoUfFavorecida.
	 *
	 * @param descricaoUfFavorecida the descricao uf favorecida
	 */
	public void setDescricaoUfFavorecida(String descricaoUfFavorecida) {
		this.descricaoUfFavorecida = descricaoUfFavorecida;
	}

	/**
	 * Get: dsAgenciaFavorecido.
	 *
	 * @return dsAgenciaFavorecido
	 */
	public String getDsAgenciaFavorecido() {
		return dsAgenciaFavorecido;
	}

	/**
	 * Set: dsAgenciaFavorecido.
	 *
	 * @param dsAgenciaFavorecido the ds agencia favorecido
	 */
	public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
		this.dsAgenciaFavorecido = dsAgenciaFavorecido;
	}

	/**
	 * Get: dsBancoFavorecido.
	 *
	 * @return dsBancoFavorecido
	 */
	public String getDsBancoFavorecido() {
		return dsBancoFavorecido;
	}

	/**
	 * Set: dsBancoFavorecido.
	 *
	 * @param dsBancoFavorecido the ds banco favorecido
	 */
	public void setDsBancoFavorecido(String dsBancoFavorecido) {
		this.dsBancoFavorecido = dsBancoFavorecido;
	}

	/**
	 * Get: dsContaFavorecido.
	 *
	 * @return dsContaFavorecido
	 */
	public String getDsContaFavorecido() {
		return dsContaFavorecido;
	}

	/**
	 * Set: dsContaFavorecido.
	 *
	 * @param dsContaFavorecido the ds conta favorecido
	 */
	public void setDsContaFavorecido(String dsContaFavorecido) {
		this.dsContaFavorecido = dsContaFavorecido;
	}

	/**
	 * Get: dsFavorecido.
	 *
	 * @return dsFavorecido
	 */
	public String getDsFavorecido() {
		return dsFavorecido;
	}

	/**
	 * Set: dsFavorecido.
	 *
	 * @param dsFavorecido the ds favorecido
	 */
	public void setDsFavorecido(String dsFavorecido) {
		this.dsFavorecido = dsFavorecido;
	}

	/**
	 * Get: dsIndicadorModalidade.
	 *
	 * @return dsIndicadorModalidade
	 */
	public String getDsIndicadorModalidade() {
		return dsIndicadorModalidade;
	}

	/**
	 * Set: dsIndicadorModalidade.
	 *
	 * @param dsIndicadorModalidade the ds indicador modalidade
	 */
	public void setDsIndicadorModalidade(String dsIndicadorModalidade) {
		this.dsIndicadorModalidade = dsIndicadorModalidade;
	}

	/**
	 * Get: dsMensagemPrimeiraLinha.
	 *
	 * @return dsMensagemPrimeiraLinha
	 */
	public String getDsMensagemPrimeiraLinha() {
		return dsMensagemPrimeiraLinha;
	}

	/**
	 * Set: dsMensagemPrimeiraLinha.
	 *
	 * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
	 */
	public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
		this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
	}

	/**
	 * Get: dsMensagemSegundaLinha.
	 *
	 * @return dsMensagemSegundaLinha
	 */
	public String getDsMensagemSegundaLinha() {
		return dsMensagemSegundaLinha;
	}

	/**
	 * Set: dsMensagemSegundaLinha.
	 *
	 * @param dsMensagemSegundaLinha the ds mensagem segunda linha
	 */
	public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
		this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
	}

	/**
	 * Get: dsMotivoSituacao.
	 *
	 * @return dsMotivoSituacao
	 */
	public String getDsMotivoSituacao() {
		return dsMotivoSituacao;
	}

	/**
	 * Set: dsMotivoSituacao.
	 *
	 * @param dsMotivoSituacao the ds motivo situacao
	 */
	public void setDsMotivoSituacao(String dsMotivoSituacao) {
		this.dsMotivoSituacao = dsMotivoSituacao;
	}

	/**
	 * Get: dsPagamento.
	 *
	 * @return dsPagamento
	 */
	public String getDsPagamento() {
		return dsPagamento;
	}

	/**
	 * Set: dsPagamento.
	 *
	 * @param dsPagamento the ds pagamento
	 */
	public void setDsPagamento(String dsPagamento) {
		this.dsPagamento = dsPagamento;
	}

	/**
	 * Get: dsTipoContaFavorecido.
	 *
	 * @return dsTipoContaFavorecido
	 */
	public String getDsTipoContaFavorecido() {
		return dsTipoContaFavorecido;
	}

	/**
	 * Set: dsTipoContaFavorecido.
	 *
	 * @param dsTipoContaFavorecido the ds tipo conta favorecido
	 */
	public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
		this.dsTipoContaFavorecido = dsTipoContaFavorecido;
	}

	/**
	 * Get: dsTipoManutencao.
	 *
	 * @return dsTipoManutencao
	 */
	public String getDsTipoManutencao() {
		return dsTipoManutencao;
	}

	/**
	 * Set: dsTipoManutencao.
	 *
	 * @param dsTipoManutencao the ds tipo manutencao
	 */
	public void setDsTipoManutencao(String dsTipoManutencao) {
		this.dsTipoManutencao = dsTipoManutencao;
	}

	/**
	 * Get: dsTipoServico.
	 *
	 * @return dsTipoServico
	 */
	public String getDsTipoServico() {
		return dsTipoServico;
	}

	/**
	 * Set: dsTipoServico.
	 *
	 * @param dsTipoServico the ds tipo servico
	 */
	public void setDsTipoServico(String dsTipoServico) {
		this.dsTipoServico = dsTipoServico;
	}

	/**
	 * Get: dsUsoEmpresa.
	 *
	 * @return dsUsoEmpresa
	 */
	public String getDsUsoEmpresa() {
		return dsUsoEmpresa;
	}

	/**
	 * Set: dsUsoEmpresa.
	 *
	 * @param dsUsoEmpresa the ds uso empresa
	 */
	public void setDsUsoEmpresa(String dsUsoEmpresa) {
		this.dsUsoEmpresa = dsUsoEmpresa;
	}

	/**
	 * Get: dtAgendamento.
	 *
	 * @return dtAgendamento
	 */
	public String getDtAgendamento() {
		return dtAgendamento;
	}

	/**
	 * Set: dtAgendamento.
	 *
	 * @param dtAgendamento the dt agendamento
	 */
	public void setDtAgendamento(String dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}

	/**
	 * Get: dtEmissaoDocumento.
	 *
	 * @return dtEmissaoDocumento
	 */
	public String getDtEmissaoDocumento() {
		return dtEmissaoDocumento;
	}

	/**
	 * Set: dtEmissaoDocumento.
	 *
	 * @param dtEmissaoDocumento the dt emissao documento
	 */
	public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
		this.dtEmissaoDocumento = dtEmissaoDocumento;
	}

	/**
	 * Get: dtNascimentoBeneficiario.
	 *
	 * @return dtNascimentoBeneficiario
	 */
	public String getDtNascimentoBeneficiario() {
		return dtNascimentoBeneficiario;
	}

	/**
	 * Set: dtNascimentoBeneficiario.
	 *
	 * @param dtNascimentoBeneficiario the dt nascimento beneficiario
	 */
	public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
		this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
	}

	/**
	 * Get: dtPagamento.
	 *
	 * @return dtPagamento
	 */
	public String getDtPagamento() {
		return dtPagamento;
	}

	/**
	 * Set: dtPagamento.
	 *
	 * @param dtPagamento the dt pagamento
	 */
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}	
	
	/**
	 * Get: dtVencimento.
	 *
	 * @return dtVencimento
	 */
	public String getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Set: dtVencimento.
	 *
	 * @param dtVencimento the dt vencimento
	 */
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Get: finalidadeDoc.
	 *
	 * @return finalidadeDoc
	 */
	public String getFinalidadeDoc() {
		return finalidadeDoc;
	}

	/**
	 * Set: finalidadeDoc.
	 *
	 * @param finalidadeDoc the finalidade doc
	 */
	public void setFinalidadeDoc(String finalidadeDoc) {
		this.finalidadeDoc = finalidadeDoc;
	}

	/**
	 * Get: finalidadeTed.
	 *
	 * @return finalidadeTed
	 */
	public String getFinalidadeTed() {
		return finalidadeTed;
	}

	/**
	 * Set: finalidadeTed.
	 *
	 * @param finalidadeTed the finalidade ted
	 */
	public void setFinalidadeTed(String finalidadeTed) {
		this.finalidadeTed = finalidadeTed;
	}

	/**
	 * Get: identificacaoTitularidade.
	 *
	 * @return identificacaoTitularidade
	 */
	public String getIdentificacaoTitularidade() {
		return identificacaoTitularidade;
	}

	/**
	 * Set: identificacaoTitularidade.
	 *
	 * @param identificacaoTitularidade the identificacao titularidade
	 */
	public void setIdentificacaoTitularidade(String identificacaoTitularidade) {
		this.identificacaoTitularidade = identificacaoTitularidade;
	}

	/**
	 * Get: identificacaoTransferenciaDoc.
	 *
	 * @return identificacaoTransferenciaDoc
	 */
	public String getIdentificacaoTransferenciaDoc() {
		return identificacaoTransferenciaDoc;
	}

	/**
	 * Set: identificacaoTransferenciaDoc.
	 *
	 * @param identificacaoTransferenciaDoc the identificacao transferencia doc
	 */
	public void setIdentificacaoTransferenciaDoc(String identificacaoTransferenciaDoc) {
		this.identificacaoTransferenciaDoc = identificacaoTransferenciaDoc;
	}

	/**
	 * Get: identificacaoTransferenciaTed.
	 *
	 * @return identificacaoTransferenciaTed
	 */
	public String getIdentificacaoTransferenciaTed() {
		return identificacaoTransferenciaTed;
	}

	/**
	 * Set: identificacaoTransferenciaTed.
	 *
	 * @param identificacaoTransferenciaTed the identificacao transferencia ted
	 */
	public void setIdentificacaoTransferenciaTed(String identificacaoTransferenciaTed) {
		this.identificacaoTransferenciaTed = identificacaoTransferenciaTed;
	}

	/**
	 * Get: identificadorRetirada.
	 *
	 * @return identificadorRetirada
	 */
	public String getIdentificadorRetirada() {
		return identificadorRetirada;
	}

	/**
	 * Set: identificadorRetirada.
	 *
	 * @param identificadorRetirada the identificador retirada
	 */
	public void setIdentificadorRetirada(String identificadorRetirada) {
		this.identificadorRetirada = identificadorRetirada;
	}

	/**
	 * Get: identificadorTipoRetirada.
	 *
	 * @return identificadorTipoRetirada
	 */
	public String getIdentificadorTipoRetirada() {
		return identificadorTipoRetirada;
	}

	/**
	 * Set: identificadorTipoRetirada.
	 *
	 * @param identificadorTipoRetirada the identificador tipo retirada
	 */
	public void setIdentificadorTipoRetirada(String identificadorTipoRetirada) {
		this.identificadorTipoRetirada = identificadorTipoRetirada;
	}

	/**
	 * Get: indicadorAssociadoUnicaAgencia.
	 *
	 * @return indicadorAssociadoUnicaAgencia
	 */
	public String getIndicadorAssociadoUnicaAgencia() {
		return indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Set: indicadorAssociadoUnicaAgencia.
	 *
	 * @param indicadorAssociadoUnicaAgencia the indicador associado unica agencia
	 */
	public void setIndicadorAssociadoUnicaAgencia(String indicadorAssociadoUnicaAgencia) {
		this.indicadorAssociadoUnicaAgencia = indicadorAssociadoUnicaAgencia;
	}

	/**
	 * Get: inscricaoEstadualContribuinte.
	 *
	 * @return inscricaoEstadualContribuinte
	 */
	public String getInscricaoEstadualContribuinte() {
		return inscricaoEstadualContribuinte;
	}

	/**
	 * Set: inscricaoEstadualContribuinte.
	 *
	 * @param inscricaoEstadualContribuinte the inscricao estadual contribuinte
	 */
	public void setInscricaoEstadualContribuinte(String inscricaoEstadualContribuinte) {
		this.inscricaoEstadualContribuinte = inscricaoEstadualContribuinte;
	}

	/**
	 * Get: instrucaoPagamento.
	 *
	 * @return instrucaoPagamento
	 */
	public String getInstrucaoPagamento() {
		return instrucaoPagamento;
	}

	/**
	 * Set: instrucaoPagamento.
	 *
	 * @param instrucaoPagamento the instrucao pagamento
	 */
	public void setInstrucaoPagamento(String instrucaoPagamento) {
		this.instrucaoPagamento = instrucaoPagamento;
	}

	/**
	 * Is manutencao.
	 *
	 * @return true, if is manutencao
	 */
	public boolean isManutencao() {
		return manutencao;
	}

	/**
	 * Set: manutencao.
	 *
	 * @param manutencao the manutencao
	 */
	public void setManutencao(boolean manutencao) {
		this.manutencao = manutencao;
	}

	/**
	 * Get: municipioTributo.
	 *
	 * @return municipioTributo
	 */
	public String getMunicipioTributo() {
		return municipioTributo;
	}

	/**
	 * Set: municipioTributo.
	 *
	 * @param municipioTributo the municipio tributo
	 */
	public void setMunicipioTributo(String municipioTributo) {
		this.municipioTributo = municipioTributo;
	}

	/**
	 * Get: nomeBeneficiario.
	 *
	 * @return nomeBeneficiario
	 */
	public String getNomeBeneficiario() {
		return nomeBeneficiario;
	}

	/**
	 * Set: nomeBeneficiario.
	 *
	 * @param nomeBeneficiario the nome beneficiario
	 */
	public void setNomeBeneficiario(String nomeBeneficiario) {
		this.nomeBeneficiario = nomeBeneficiario;
	}

	/**
	 * Get: nomeDepositante.
	 *
	 * @return nomeDepositante
	 */
	public String getNomeDepositante() {
		return nomeDepositante;
	}

	/**
	 * Set: nomeDepositante.
	 *
	 * @param nomeDepositante the nome depositante
	 */
	public void setNomeDepositante(String nomeDepositante) {
		this.nomeDepositante = nomeDepositante;
	}

	/**
	 * Get: nomeOrgaoFavorecido.
	 *
	 * @return nomeOrgaoFavorecido
	 */
	public String getNomeOrgaoFavorecido() {
		return nomeOrgaoFavorecido;
	}

	/**
	 * Set: nomeOrgaoFavorecido.
	 *
	 * @param nomeOrgaoFavorecido the nome orgao favorecido
	 */
	public void setNomeOrgaoFavorecido(String nomeOrgaoFavorecido) {
		this.nomeOrgaoFavorecido = nomeOrgaoFavorecido;
	}

	/**
	 * Get: nrDocumento.
	 *
	 * @return nrDocumento
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}

	/**
	 * Set: nrDocumento.
	 *
	 * @param nrDocumento the nr documento
	 */
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	/**
	 * Get: nrLoteArquivoRemessa.
	 *
	 * @return nrLoteArquivoRemessa
	 */
	public String getNrLoteArquivoRemessa() {
		return nrLoteArquivoRemessa;
	}

	/**
	 * Set: nrLoteArquivoRemessa.
	 *
	 * @param nrLoteArquivoRemessa the nr lote arquivo remessa
	 */
	public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
		this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
	}

	/**
	 * Get: nrSequenciaArquivoRemessa.
	 *
	 * @return nrSequenciaArquivoRemessa
	 */
	public String getNrSequenciaArquivoRemessa() {
		return nrSequenciaArquivoRemessa;
	}

	/**
	 * Set: nrSequenciaArquivoRemessa.
	 *
	 * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
	 */
	public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
		this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
	}

	/**
	 * Get: numeroCheque.
	 *
	 * @return numeroCheque
	 */
	public String getNumeroCheque() {
		return numeroCheque;
	}

	/**
	 * Set: numeroCheque.
	 *
	 * @param numeroCheque the numero cheque
	 */
	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}

	/**
	 * Get: numeroCotaParcela.
	 *
	 * @return numeroCotaParcela
	 */
	public String getNumeroCotaParcela() {
		return numeroCotaParcela;
	}

	/**
	 * Set: numeroCotaParcela.
	 *
	 * @param numeroCotaParcela the numero cota parcela
	 */
	public void setNumeroCotaParcela(String numeroCotaParcela) {
		this.numeroCotaParcela = numeroCotaParcela;
	}

	/**
	 * Get: numeroInscricaoBeneficiario.
	 *
	 * @return numeroInscricaoBeneficiario
	 */
	public String getNumeroInscricaoBeneficiario() {
		return numeroInscricaoBeneficiario;
	}

	/**
	 * Set: numeroInscricaoBeneficiario.
	 *
	 * @param numeroInscricaoBeneficiario the numero inscricao beneficiario
	 */
	public void setNumeroInscricaoBeneficiario(String numeroInscricaoBeneficiario) {
		this.numeroInscricaoBeneficiario = numeroInscricaoBeneficiario;
	}

	/**
	 * Get: numeroInscricaoFavorecido.
	 *
	 * @return numeroInscricaoFavorecido
	 */
	public String getNumeroInscricaoFavorecido() {
		return numeroInscricaoFavorecido;
	}

	/**
	 * Set: numeroInscricaoFavorecido.
	 *
	 * @param numeroInscricaoFavorecido the numero inscricao favorecido
	 */
	public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
		this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
	}

	/**
	 * Get: numeroNsu.
	 *
	 * @return numeroNsu
	 */
	public String getNumeroNsu() {
		return numeroNsu;
	}

	/**
	 * Set: numeroNsu.
	 *
	 * @param numeroNsu the numero nsu
	 */
	public void setNumeroNsu(String numeroNsu) {
		this.numeroNsu = numeroNsu;
	}

	/**
	 * Get: numeroOp.
	 *
	 * @return numeroOp
	 */
	public String getNumeroOp() {
		return numeroOp;
	}

	/**
	 * Set: numeroOp.
	 *
	 * @param numeroOp the numero op
	 */
	public void setNumeroOp(String numeroOp) {
		this.numeroOp = numeroOp;
	}

	/**
	 * Get: numeroPagamento.
	 *
	 * @return numeroPagamento
	 */
	public String getNumeroPagamento() {
		return numeroPagamento;
	}

	/**
	 * Set: numeroPagamento.
	 *
	 * @param numeroPagamento the numero pagamento
	 */
	public void setNumeroPagamento(String numeroPagamento) {
		this.numeroPagamento = numeroPagamento;
	}

	/**
	 * Get: numeroParcelamento.
	 *
	 * @return numeroParcelamento
	 */
	public String getNumeroParcelamento() {
		return numeroParcelamento;
	}

	/**
	 * Set: numeroParcelamento.
	 *
	 * @param numeroParcelamento the numero parcelamento
	 */
	public void setNumeroParcelamento(String numeroParcelamento) {
		this.numeroParcelamento = numeroParcelamento;
	}

	/**
	 * Get: numeroReferencia.
	 *
	 * @return numeroReferencia
	 */
	public String getNumeroReferencia() {
		return numeroReferencia;
	}

	/**
	 * Set: numeroReferencia.
	 *
	 * @param numeroReferencia the numero referencia
	 */
	public void setNumeroReferencia(String numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}

	/**
	 * Get: observacaoTributo.
	 *
	 * @return observacaoTributo
	 */
	public String getObservacaoTributo() {
		return observacaoTributo;
	}

	/**
	 * Set: observacaoTributo.
	 *
	 * @param observacaoTributo the observacao tributo
	 */
	public void setObservacaoTributo(String observacaoTributo) {
		this.observacaoTributo = observacaoTributo;
	}

	/**
	 * Get: opcaoRetiradaTributo.
	 *
	 * @return opcaoRetiradaTributo
	 */
	public String getOpcaoRetiradaTributo() {
		return opcaoRetiradaTributo;
	}

	/**
	 * Set: opcaoRetiradaTributo.
	 *
	 * @param opcaoRetiradaTributo the opcao retirada tributo
	 */
	public void setOpcaoRetiradaTributo(String opcaoRetiradaTributo) {
		this.opcaoRetiradaTributo = opcaoRetiradaTributo;
	}

	/**
	 * Get: opcaoTributo.
	 *
	 * @return opcaoTributo
	 */
	public String getOpcaoTributo() {
		return opcaoTributo;
	}

	/**
	 * Set: opcaoTributo.
	 *
	 * @param opcaoTributo the opcao tributo
	 */
	public void setOpcaoTributo(String opcaoTributo) {
		this.opcaoTributo = opcaoTributo;
	}

	/**
	 * Get: orgaoPagador.
	 *
	 * @return orgaoPagador
	 */
	public String getOrgaoPagador() {
		return orgaoPagador;
	}

	/**
	 * Set: orgaoPagador.
	 *
	 * @param orgaoPagador the orgao pagador
	 */
	public void setOrgaoPagador(String orgaoPagador) {
		this.orgaoPagador = orgaoPagador;
	}

	/**
	 * Get: percentualReceita.
	 *
	 * @return percentualReceita
	 */
	public BigDecimal getPercentualReceita() {
		return percentualReceita;
	}

	/**
	 * Set: percentualReceita.
	 *
	 * @param percentualReceita the percentual receita
	 */
	public void setPercentualReceita(BigDecimal percentualReceita) {
		this.percentualReceita = percentualReceita;
	}

	/**
	 * Get: periodoApuracao.
	 *
	 * @return periodoApuracao
	 */
	public String getPeriodoApuracao() {
		return periodoApuracao;
	}

	/**
	 * Set: periodoApuracao.
	 *
	 * @param periodoApuracao the periodo apuracao
	 */
	public void setPeriodoApuracao(String periodoApuracao) {
		this.periodoApuracao = periodoApuracao;
	}

	/**
	 * Get: placaVeiculo.
	 *
	 * @return placaVeiculo
	 */
	public String getPlacaVeiculo() {
		return placaVeiculo;
	}

	/**
	 * Set: placaVeiculo.
	 *
	 * @param placaVeiculo the placa veiculo
	 */
	public void setPlacaVeiculo(String placaVeiculo) {
		this.placaVeiculo = placaVeiculo;
	}

	/**
	 * Get: produtoDepositoIdentificado.
	 *
	 * @return produtoDepositoIdentificado
	 */
	public String getProdutoDepositoIdentificado() {
		return produtoDepositoIdentificado;
	}

	/**
	 * Set: produtoDepositoIdentificado.
	 *
	 * @param produtoDepositoIdentificado the produto deposito identificado
	 */
	public void setProdutoDepositoIdentificado(String produtoDepositoIdentificado) {
		this.produtoDepositoIdentificado = produtoDepositoIdentificado;
	}

	/**
	 * Get: qtMoeda.
	 *
	 * @return qtMoeda
	 */
	public BigDecimal getQtMoeda() {
		return qtMoeda;
	}

	/**
	 * Set: qtMoeda.
	 *
	 * @param qtMoeda the qt moeda
	 */
	public void setQtMoeda(BigDecimal qtMoeda) {
		this.qtMoeda = qtMoeda;
	}

	/**
	 * Get: sequenciaProdutoDepositoIdentificado.
	 *
	 * @return sequenciaProdutoDepositoIdentificado
	 */
	public String getSequenciaProdutoDepositoIdentificado() {
		return sequenciaProdutoDepositoIdentificado;
	}

	/**
	 * Set: sequenciaProdutoDepositoIdentificado.
	 *
	 * @param sequenciaProdutoDepositoIdentificado the sequencia produto deposito identificado
	 */
	public void setSequenciaProdutoDepositoIdentificado(String sequenciaProdutoDepositoIdentificado) {
		this.sequenciaProdutoDepositoIdentificado = sequenciaProdutoDepositoIdentificado;
	}

	/**
	 * Get: serieCheque.
	 *
	 * @return serieCheque
	 */
	public String getSerieCheque() {
		return serieCheque;
	}

	/**
	 * Set: serieCheque.
	 *
	 * @param serieCheque the serie cheque
	 */
	public void setSerieCheque(String serieCheque) {
		this.serieCheque = serieCheque;
	}

	/**
	 * Get: telefoneContribuinte.
	 *
	 * @return telefoneContribuinte
	 */
	public String getTelefoneContribuinte() {
		return telefoneContribuinte;
	}

	/**
	 * Set: telefoneContribuinte.
	 *
	 * @param telefoneContribuinte the telefone contribuinte
	 */
	public void setTelefoneContribuinte(String telefoneContribuinte) {
		this.telefoneContribuinte = telefoneContribuinte;
	}

	/**
	 * Get: tipoBeneficiario.
	 *
	 * @return tipoBeneficiario
	 */
	public String getTipoBeneficiario() {
		return tipoBeneficiario;
	}

	/**
	 * Set: tipoBeneficiario.
	 *
	 * @param tipoBeneficiario the tipo beneficiario
	 */
	public void setTipoBeneficiario(String tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}

	/**
	 * Get: tipoCanal.
	 *
	 * @return tipoCanal
	 */
	public String getTipoCanal() {
		return tipoCanal;
	}

	/**
	 * Set: tipoCanal.
	 *
	 * @param tipoCanal the tipo canal
	 */
	public void setTipoCanal(String tipoCanal) {
		this.tipoCanal = tipoCanal;
	}

	/**
	 * Get: tipoContaCredito.
	 *
	 * @return tipoContaCredito
	 */
	public String getTipoContaCredito() {
		return tipoContaCredito;
	}

	/**
	 * Set: tipoContaCredito.
	 *
	 * @param tipoContaCredito the tipo conta credito
	 */
	public void setTipoContaCredito(String tipoContaCredito) {
		this.tipoContaCredito = tipoContaCredito;
	}

	/**
	 * Get: tipoContaDestino.
	 *
	 * @return tipoContaDestino
	 */
	public String getTipoContaDestino() {
		return tipoContaDestino;
	}

	/**
	 * Set: tipoContaDestino.
	 *
	 * @param tipoContaDestino the tipo conta destino
	 */
	public void setTipoContaDestino(String tipoContaDestino) {
		this.tipoContaDestino = tipoContaDestino;
	}

	/**
	 * Get: tipoDespositoIdentificado.
	 *
	 * @return tipoDespositoIdentificado
	 */
	public String getTipoDespositoIdentificado() {
		return tipoDespositoIdentificado;
	}

	/**
	 * Set: tipoDespositoIdentificado.
	 *
	 * @param tipoDespositoIdentificado the tipo desposito identificado
	 */
	public void setTipoDespositoIdentificado(String tipoDespositoIdentificado) {
		this.tipoDespositoIdentificado = tipoDespositoIdentificado;
	}

	/**
	 * Get: tipoDocumento.
	 *
	 * @return tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * Set: tipoDocumento.
	 *
	 * @param tipoDocumento the tipo documento
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * Get: tipoFavorecido.
	 *
	 * @return tipoFavorecido
	 */
	public Integer getTipoFavorecido() {
		return tipoFavorecido;
	}

	/**
	 * Set: tipoFavorecido.
	 *
	 * @param tipoFavorecido the tipo favorecido
	 */
	public void setTipoFavorecido(Integer tipoFavorecido) {
		this.tipoFavorecido = tipoFavorecido;
	}

	/**
	 * Get: ufTributo.
	 *
	 * @return ufTributo
	 */
	public String getUfTributo() {
		return ufTributo;
	}

	/**
	 * Set: ufTributo.
	 *
	 * @param ufTributo the uf tributo
	 */
	public void setUfTributo(String ufTributo) {
		this.ufTributo = ufTributo;
	}

	/**
	 * Get: viaCartaoDepositoIdentificado.
	 *
	 * @return viaCartaoDepositoIdentificado
	 */
	public String getViaCartaoDepositoIdentificado() {
		return viaCartaoDepositoIdentificado;
	}

	/**
	 * Set: viaCartaoDepositoIdentificado.
	 *
	 * @param viaCartaoDepositoIdentificado the via cartao deposito identificado
	 */
	public void setViaCartaoDepositoIdentificado(String viaCartaoDepositoIdentificado) {
		this.viaCartaoDepositoIdentificado = viaCartaoDepositoIdentificado;
	}

	/**
	 * Get: vlAbatimento.
	 *
	 * @return vlAbatimento
	 */
	public BigDecimal getVlAbatimento() {
		return vlAbatimento;
	}

	/**
	 * Set: vlAbatimento.
	 *
	 * @param vlAbatimento the vl abatimento
	 */
	public void setVlAbatimento(BigDecimal vlAbatimento) {
		this.vlAbatimento = vlAbatimento;
	}

	/**
	 * Get: vlAcrescimo.
	 *
	 * @return vlAcrescimo
	 */
	public BigDecimal getVlAcrescimo() {
		return vlAcrescimo;
	}

	/**
	 * Set: vlAcrescimo.
	 *
	 * @param vlAcrescimo the vl acrescimo
	 */
	public void setVlAcrescimo(BigDecimal vlAcrescimo) {
		this.vlAcrescimo = vlAcrescimo;
	}

	/**
	 * Get: vlAcrescimoFinanceiro.
	 *
	 * @return vlAcrescimoFinanceiro
	 */
	public BigDecimal getVlAcrescimoFinanceiro() {
		return vlAcrescimoFinanceiro;
	}

	/**
	 * Set: vlAcrescimoFinanceiro.
	 *
	 * @param vlAcrescimoFinanceiro the vl acrescimo financeiro
	 */
	public void setVlAcrescimoFinanceiro(BigDecimal vlAcrescimoFinanceiro) {
		this.vlAcrescimoFinanceiro = vlAcrescimoFinanceiro;
	}

	/**
	 * Get: vlAtualizacaoMonetaria.
	 *
	 * @return vlAtualizacaoMonetaria
	 */
	public BigDecimal getVlAtualizacaoMonetaria() {
		return vlAtualizacaoMonetaria;
	}

	/**
	 * Set: vlAtualizacaoMonetaria.
	 *
	 * @param vlAtualizacaoMonetaria the vl atualizacao monetaria
	 */
	public void setVlAtualizacaoMonetaria(BigDecimal vlAtualizacaoMonetaria) {
		this.vlAtualizacaoMonetaria = vlAtualizacaoMonetaria;
	}

	/**
	 * Get: vlDeducao.
	 *
	 * @return vlDeducao
	 */
	public BigDecimal getVlDeducao() {
		return vlDeducao;
	}

	/**
	 * Set: vlDeducao.
	 *
	 * @param vlDeducao the vl deducao
	 */
	public void setVlDeducao(BigDecimal vlDeducao) {
		this.vlDeducao = vlDeducao;
	}

	/**
	 * Get: vlDesconto.
	 *
	 * @return vlDesconto
	 */
	public BigDecimal getVlDesconto() {
		return vlDesconto;
	}

	/**
	 * Set: vlDesconto.
	 *
	 * @param vlDesconto the vl desconto
	 */
	public void setVlDesconto(BigDecimal vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	/**
	 * Get: vlDocumento.
	 *
	 * @return vlDocumento
	 */
	public BigDecimal getVlDocumento() {
		return vlDocumento;
	}

	/**
	 * Set: vlDocumento.
	 *
	 * @param vlDocumento the vl documento
	 */
	public void setVlDocumento(BigDecimal vlDocumento) {
		this.vlDocumento = vlDocumento;
	}

	/**
	 * Get: vlHonorarioAdvogado.
	 *
	 * @return vlHonorarioAdvogado
	 */
	public BigDecimal getVlHonorarioAdvogado() {
		return vlHonorarioAdvogado;
	}

	/**
	 * Set: vlHonorarioAdvogado.
	 *
	 * @param vlHonorarioAdvogado the vl honorario advogado
	 */
	public void setVlHonorarioAdvogado(BigDecimal vlHonorarioAdvogado) {
		this.vlHonorarioAdvogado = vlHonorarioAdvogado;
	}

	/**
	 * Get: vlImpostoMovFinanceira.
	 *
	 * @return vlImpostoMovFinanceira
	 */
	public BigDecimal getVlImpostoMovFinanceira() {
		return vlImpostoMovFinanceira;
	}

	/**
	 * Set: vlImpostoMovFinanceira.
	 *
	 * @param vlImpostoMovFinanceira the vl imposto mov financeira
	 */
	public void setVlImpostoMovFinanceira(BigDecimal vlImpostoMovFinanceira) {
		this.vlImpostoMovFinanceira = vlImpostoMovFinanceira;
	}

	/**
	 * Get: vlImpostoRenda.
	 *
	 * @return vlImpostoRenda
	 */
	public BigDecimal getVlImpostoRenda() {
		return vlImpostoRenda;
	}

	/**
	 * Set: vlImpostoRenda.
	 *
	 * @param vlImpostoRenda the vl imposto renda
	 */
	public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
		this.vlImpostoRenda = vlImpostoRenda;
	}

	/**
	 * Get: vlInss.
	 *
	 * @return vlInss
	 */
	public BigDecimal getVlInss() {
		return vlInss;
	}

	/**
	 * Set: vlInss.
	 *
	 * @param vlInss the vl inss
	 */
	public void setVlInss(BigDecimal vlInss) {
		this.vlInss = vlInss;
	}

	/**
	 * Get: vlIof.
	 *
	 * @return vlIof
	 */
	public BigDecimal getVlIof() {
		return vlIof;
	}

	/**
	 * Set: vlIof.
	 *
	 * @param vlIof the vl iof
	 */
	public void setVlIof(BigDecimal vlIof) {
		this.vlIof = vlIof;
	}

	/**
	 * Get: vlIss.
	 *
	 * @return vlIss
	 */
	public BigDecimal getVlIss() {
		return vlIss;
	}

	/**
	 * Set: vlIss.
	 *
	 * @param vlIss the vl iss
	 */
	public void setVlIss(BigDecimal vlIss) {
		this.vlIss = vlIss;
	}

	/**
	 * Get: vlMora.
	 *
	 * @return vlMora
	 */
	public BigDecimal getVlMora() {
		return vlMora;
	}

	/**
	 * Set: vlMora.
	 *
	 * @param vlMora the vl mora
	 */
	public void setVlMora(BigDecimal vlMora) {
		this.vlMora = vlMora;
	}

	/**
	 * Get: vlMulta.
	 *
	 * @return vlMulta
	 */
	public BigDecimal getVlMulta() {
		return vlMulta;
	}

	/**
	 * Set: vlMulta.
	 *
	 * @param vlMulta the vl multa
	 */
	public void setVlMulta(BigDecimal vlMulta) {
		this.vlMulta = vlMulta;
	}

	/**
	 * Get: vlOutrasEntidades.
	 *
	 * @return vlOutrasEntidades
	 */
	public BigDecimal getVlOutrasEntidades() {
		return vlOutrasEntidades;
	}

	/**
	 * Set: vlOutrasEntidades.
	 *
	 * @param vlOutrasEntidades the vl outras entidades
	 */
	public void setVlOutrasEntidades(BigDecimal vlOutrasEntidades) {
		this.vlOutrasEntidades = vlOutrasEntidades;
	}

	/**
	 * Get: vlPrincipal.
	 *
	 * @return vlPrincipal
	 */
	public BigDecimal getVlPrincipal() {
		return vlPrincipal;
	}

	/**
	 * Set: vlPrincipal.
	 *
	 * @param vlPrincipal the vl principal
	 */
	public void setVlPrincipal(BigDecimal vlPrincipal) {
		this.vlPrincipal = vlPrincipal;
	}

	/**
	 * Get: vlrAgendamento.
	 *
	 * @return vlrAgendamento
	 */
	public BigDecimal getVlrAgendamento() {
		return vlrAgendamento;
	}

	/**
	 * Set: vlrAgendamento.
	 *
	 * @param vlrAgendamento the vlr agendamento
	 */
	public void setVlrAgendamento(BigDecimal vlrAgendamento) {
		this.vlrAgendamento = vlrAgendamento;
	}

	/**
	 * Get: vlReceita.
	 *
	 * @return vlReceita
	 */
	public BigDecimal getVlReceita() {
		return vlReceita;
	}

	/**
	 * Set: vlReceita.
	 *
	 * @param vlReceita the vl receita
	 */
	public void setVlReceita(BigDecimal vlReceita) {
		this.vlReceita = vlReceita;
	}

	/**
	 * Get: vlrEfetivacao.
	 *
	 * @return vlrEfetivacao
	 */
	public BigDecimal getVlrEfetivacao() {
		return vlrEfetivacao;
	}

	/**
	 * Set: vlrEfetivacao.
	 *
	 * @param vlrEfetivacao the vlr efetivacao
	 */
	public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
		this.vlrEfetivacao = vlrEfetivacao;
	}

	/**
	 * Get: vlTotal.
	 *
	 * @return vlTotal
	 */
	public BigDecimal getVlTotal() {
		return vlTotal;
	}

	/**
	 * Set: vlTotal.
	 *
	 * @param vlTotal the vl total
	 */
	public void setVlTotal(BigDecimal vlTotal) {
		this.vlTotal = vlTotal;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Is panel botoes.
	 *
	 * @return true, if is panel botoes
	 */
	public boolean isPanelBotoes() {
		return panelBotoes;
	}

	/**
	 * Set: panelBotoes.
	 *
	 * @param panelBotoes the panel botoes
	 */
	public void setPanelBotoes(boolean panelBotoes) {
		this.panelBotoes = panelBotoes;
	}

	/**
	 * Get: listaConsultarMotivoSituacaoPagamentoFixoFiltro.
	 *
	 * @return listaConsultarMotivoSituacaoPagamentoFixoFiltro
	 */
	public List<SelectItem> getListaConsultarMotivoSituacaoPagamentoFixoFiltro() {
		return listaConsultarMotivoSituacaoPagamentoFixoFiltro;
	}

	/**
	 * Set: listaConsultarMotivoSituacaoPagamentoFixoFiltro.
	 *
	 * @param listaConsultarMotivoSituacaoPagamentoFixoFiltro the lista consultar motivo situacao pagamento fixo filtro
	 */
	public void setListaConsultarMotivoSituacaoPagamentoFixoFiltro(
					List<SelectItem> listaConsultarMotivoSituacaoPagamentoFixoFiltro) {
		this.listaConsultarMotivoSituacaoPagamentoFixoFiltro = listaConsultarMotivoSituacaoPagamentoFixoFiltro;
	}

	/**
	 * Get: logoPath.
	 *
	 * @return logoPath
	 */
	public String getLogoPath() {
		return logoPath;
	}

	/**
	 * Set: logoPath.
	 *
	 * @param logoPath the logo path
	 */
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	/**
	 * Get: dsOrigemPagamento.
	 *
	 * @return dsOrigemPagamento
	 */
	public String getDsOrigemPagamento() {
		return dsOrigemPagamento;
	}

	/**
	 * Set: dsOrigemPagamento.
	 *
	 * @param dsOrigemPagamento the ds origem pagamento
	 */
	public void setDsOrigemPagamento(String dsOrigemPagamento) {
		this.dsOrigemPagamento = dsOrigemPagamento;
	}

	/**
	 * Get: nossoNumero.
	 *
	 * @return nossoNumero
	 */
	public String getNossoNumero() {
		return nossoNumero;
	}

	/**
	 * Set: nossoNumero.
	 *
	 * @param nossoNumero the nosso numero
	 */
	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	/**
	 * Is disable argumentos consulta.
	 *
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 *
	 * @param disableArgumentosConsulta the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Is exibe beneficiario.
	 *
	 * @return true, if is exibe beneficiario
	 */
	public boolean isExibeBeneficiario() {
		return exibeBeneficiario;
	}

	/**
	 * Set: exibeBeneficiario.
	 *
	 * @param exibeBeneficiario the exibe beneficiario
	 */
	public void setExibeBeneficiario(boolean exibeBeneficiario) {
		this.exibeBeneficiario = exibeBeneficiario;
	}

	/**
	 * Get: descricaoAgenciaContaDebitoBancaria.
	 *
	 * @return descricaoAgenciaContaDebitoBancaria
	 */
	public String getDescricaoAgenciaContaDebitoBancaria() {
		return descricaoAgenciaContaDebitoBancaria;
	}

	/**
	 * Set: descricaoAgenciaContaDebitoBancaria.
	 *
	 * @param descricaoAgenciaContaDebitoBancaria the descricao agencia conta debito bancaria
	 */
	public void setDescricaoAgenciaContaDebitoBancaria(String descricaoAgenciaContaDebitoBancaria) {
		this.descricaoAgenciaContaDebitoBancaria = descricaoAgenciaContaDebitoBancaria;
	}

	/**
	 * Get: descricaoBancoContaDebito.
	 *
	 * @return descricaoBancoContaDebito
	 */
	public String getDescricaoBancoContaDebito() {
		return descricaoBancoContaDebito;
	}

	/**
	 * Set: descricaoBancoContaDebito.
	 *
	 * @param descricaoBancoContaDebito the descricao banco conta debito
	 */
	public void setDescricaoBancoContaDebito(String descricaoBancoContaDebito) {
		this.descricaoBancoContaDebito = descricaoBancoContaDebito;
	}

	/**
	 * Get: descricaoTipoContaDebito.
	 *
	 * @return descricaoTipoContaDebito
	 */
	public String getDescricaoTipoContaDebito() {
		return descricaoTipoContaDebito;
	}

	/**
	 * Set: descricaoTipoContaDebito.
	 *
	 * @param descricaoTipoContaDebito the descricao tipo conta debito
	 */
	public void setDescricaoTipoContaDebito(String descricaoTipoContaDebito) {
		this.descricaoTipoContaDebito = descricaoTipoContaDebito;
	}

	/**
	 * Get: consultasService.
	 *
	 * @return consultasService
	 */
	public IConsultasService getConsultasService() {
		return consultasService;
	}

	/**
	 * Set: consultasService.
	 *
	 * @param consultasService the consultas service
	 */
	public void setConsultasService(IConsultasService consultasService) {
		this.consultasService = consultasService;
	}

	/**
	 * Get: descricaoContrato.
	 *
	 * @return descricaoContrato
	 */
	public String getDescricaoContrato() {
		return descricaoContrato;
	}

	/**
	 * Set: descricaoContrato.
	 *
	 * @param descricaoContrato the descricao contrato
	 */
	public void setDescricaoContrato(String descricaoContrato) {
		this.descricaoContrato = descricaoContrato;
	}

	/**
	 * Get: descricaoEmpresa.
	 *
	 * @return descricaoEmpresa
	 */
	public String getDescricaoEmpresa() {
		return descricaoEmpresa;
	}

	/**
	 * Set: descricaoEmpresa.
	 *
	 * @param descricaoEmpresa the descricao empresa
	 */
	public void setDescricaoEmpresa(String descricaoEmpresa) {
		this.descricaoEmpresa = descricaoEmpresa;
	}

	/**
	 * Get: descricaoNumeroContrato.
	 *
	 * @return descricaoNumeroContrato
	 */
	public String getDescricaoNumeroContrato() {
		return descricaoNumeroContrato;
	}

	/**
	 * Set: descricaoNumeroContrato.
	 *
	 * @param descricaoNumeroContrato the descricao numero contrato
	 */
	public void setDescricaoNumeroContrato(String descricaoNumeroContrato) {
		this.descricaoNumeroContrato = descricaoNumeroContrato;
	}

	/**
	 * Get: descricaoSituacaoContrato.
	 *
	 * @return descricaoSituacaoContrato
	 */
	public String getDescricaoSituacaoContrato() {
		return descricaoSituacaoContrato;
	}

	/**
	 * Get: carteira.
	 *
	 * @return carteira
	 */
	public String getCarteira() {
		return carteira;
	}

	/**
	 * Set: carteira.
	 *
	 * @param carteira the carteira
	 */
	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}

	/**
	 * Set: descricaoSituacaoContrato.
	 *
	 * @param descricaoSituacaoContrato the descricao situacao contrato
	 */
	public void setDescricaoSituacaoContrato(String descricaoSituacaoContrato) {
		this.descricaoSituacaoContrato = descricaoSituacaoContrato;
	}

	/**
	 * Get: cdIndentificadorJudicial.
	 *
	 * @return cdIndentificadorJudicial
	 */
	public String getCdIndentificadorJudicial() {
		return cdIndentificadorJudicial;
	}

	/**
	 * Set: cdIndentificadorJudicial.
	 *
	 * @param cdIndentificadorJudicial the cd indentificador judicial
	 */
	public void setCdIndentificadorJudicial(String cdIndentificadorJudicial) {
		this.cdIndentificadorJudicial = cdIndentificadorJudicial;
	}

	/**
	 * Get: cdSituacaoTranferenciaAutomatica.
	 *
	 * @return cdSituacaoTranferenciaAutomatica
	 */
	public String getCdSituacaoTranferenciaAutomatica() {
		return cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Set: cdSituacaoTranferenciaAutomatica.
	 *
	 * @param cdSituacaoTranferenciaAutomatica the cd situacao tranferencia automatica
	 */
	public void setCdSituacaoTranferenciaAutomatica(String cdSituacaoTranferenciaAutomatica) {
		this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
	}

	/**
	 * Get: dtLimiteDescontoPagamento.
	 *
	 * @return dtLimiteDescontoPagamento
	 */
	public String getDtLimiteDescontoPagamento() {
		return dtLimiteDescontoPagamento;
	}

	/**
	 * Set: dtLimiteDescontoPagamento.
	 *
	 * @param dtLimiteDescontoPagamento the dt limite desconto pagamento
	 */
	public void setDtLimiteDescontoPagamento(String dtLimiteDescontoPagamento) {
		this.dtLimiteDescontoPagamento = dtLimiteDescontoPagamento;
	}

	/**
	 * Is disable detalhar.
	 *
	 * @return true, if is disable detalhar
	 */
	public boolean isDisableDetalhar() {
		return disableDetalhar;
	}

	/**
	 * Set: disableDetalhar.
	 *
	 * @param disableDetalhar the disable detalhar
	 */
	public void setDisableDetalhar(boolean disableDetalhar) {
		this.disableDetalhar = disableDetalhar;
	}

	/**
	 * Get: descTipoFavorecido.
	 *
	 * @return descTipoFavorecido
	 */
	public String getDescTipoFavorecido() {
		return descTipoFavorecido;
	}

	/**
	 * Set: descTipoFavorecido.
	 *
	 * @param descTipoFavorecido the desc tipo favorecido
	 */
	public void setDescTipoFavorecido(String descTipoFavorecido) {
		this.descTipoFavorecido = descTipoFavorecido;
	}

	/**
	 * Get: dsTipoBeneficiario.
	 *
	 * @return dsTipoBeneficiario
	 */
	public String getDsTipoBeneficiario() {
		return dsTipoBeneficiario;
	}

	/**
	 * Set: dsTipoBeneficiario.
	 *
	 * @param dsTipoBeneficiario the ds tipo beneficiario
	 */
	public void setDsTipoBeneficiario(String dsTipoBeneficiario) {
		this.dsTipoBeneficiario = dsTipoBeneficiario;
	}

	/**
	 * Get: cdBancoOriginal.
	 *
	 * @return cdBancoOriginal
	 */
	public Integer getCdBancoOriginal() {
		return cdBancoOriginal;
	}

	/**
	 * Set: cdBancoOriginal.
	 *
	 * @param cdBancoOriginal the cd banco original
	 */
	public void setCdBancoOriginal(Integer cdBancoOriginal) {
		this.cdBancoOriginal = cdBancoOriginal;
	}

	/**
	 * Get: dsBancoOriginal.
	 *
	 * @return dsBancoOriginal
	 */
	public String getDsBancoOriginal() {
		return dsBancoOriginal;
	}

	/**
	 * Set: dsBancoOriginal.
	 *
	 * @param dsBancoOriginal the ds banco original
	 */
	public void setDsBancoOriginal(String dsBancoOriginal) {
		this.dsBancoOriginal = dsBancoOriginal;
	}

	/**
	 * Get: cdAgenciaBancariaOriginal.
	 *
	 * @return cdAgenciaBancariaOriginal
	 */
	public Integer getCdAgenciaBancariaOriginal() {
		return cdAgenciaBancariaOriginal;
	}

	/**
	 * Set: cdAgenciaBancariaOriginal.
	 *
	 * @param cdAgenciaBancariaOriginal the cd agencia bancaria original
	 */
	public void setCdAgenciaBancariaOriginal(Integer cdAgenciaBancariaOriginal) {
		this.cdAgenciaBancariaOriginal = cdAgenciaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoAgenciaOriginal.
	 *
	 * @return cdDigitoAgenciaOriginal
	 */
	public String getCdDigitoAgenciaOriginal() {
		return cdDigitoAgenciaOriginal;
	}

	/**
	 * Set: cdDigitoAgenciaOriginal.
	 *
	 * @param cdDigitoAgenciaOriginal the cd digito agencia original
	 */
	public void setCdDigitoAgenciaOriginal(String cdDigitoAgenciaOriginal) {
		this.cdDigitoAgenciaOriginal = cdDigitoAgenciaOriginal;
	}

	/**
	 * Get: dsAgenciaOriginal.
	 *
	 * @return dsAgenciaOriginal
	 */
	public String getDsAgenciaOriginal() {
		return dsAgenciaOriginal;
	}

	/**
	 * Set: dsAgenciaOriginal.
	 *
	 * @param dsAgenciaOriginal the ds agencia original
	 */
	public void setDsAgenciaOriginal(String dsAgenciaOriginal) {
		this.dsAgenciaOriginal = dsAgenciaOriginal;
	}

	/**
	 * Get: cdContaBancariaOriginal.
	 *
	 * @return cdContaBancariaOriginal
	 */
	public Long getCdContaBancariaOriginal() {
		return cdContaBancariaOriginal;
	}

	/**
	 * Set: cdContaBancariaOriginal.
	 *
	 * @param cdContaBancariaOriginal the cd conta bancaria original
	 */
	public void setCdContaBancariaOriginal(Long cdContaBancariaOriginal) {
		this.cdContaBancariaOriginal = cdContaBancariaOriginal;
	}

	/**
	 * Get: cdDigitoContaOriginal.
	 *
	 * @return cdDigitoContaOriginal
	 */
	public String getCdDigitoContaOriginal() {
		return cdDigitoContaOriginal;
	}

	/**
	 * Set: cdDigitoContaOriginal.
	 *
	 * @param cdDigitoContaOriginal the cd digito conta original
	 */
	public void setCdDigitoContaOriginal(String cdDigitoContaOriginal) {
		this.cdDigitoContaOriginal = cdDigitoContaOriginal;
	}

	/**
	 * Get: dsTipoContaOriginal.
	 *
	 * @return dsTipoContaOriginal
	 */
	public String getDsTipoContaOriginal() {
		return dsTipoContaOriginal;
	}

	/**
	 * Set: dsTipoContaOriginal.
	 *
	 * @param dsTipoContaOriginal the ds tipo conta original
	 */
	public void setDsTipoContaOriginal(String dsTipoContaOriginal) {
		this.dsTipoContaOriginal = dsTipoContaOriginal;
	}

	/**
	 * Get: dtDevolucaoEstorno.
	 *
	 * @return dtDevolucaoEstorno
	 */
	public String getDtDevolucaoEstorno() {
		return dtDevolucaoEstorno;
	}

	/**
	 * Set: dtDevolucaoEstorno.
	 *
	 * @param dtDevolucaoEstorno the dt devolucao estorno
	 */
	public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
		this.dtDevolucaoEstorno = dtDevolucaoEstorno;
	}

	/**
	 * Get: dtLimitePagamento.
	 *
	 * @return dtLimitePagamento
	 */
	public String getDtLimitePagamento() {
		return dtLimitePagamento;
	}

	/**
	 * Set: dtLimitePagamento.
	 *
	 * @param dtLimitePagamento the dt limite pagamento
	 */
	public void setDtLimitePagamento(String dtLimitePagamento) {
		this.dtLimitePagamento = dtLimitePagamento;
	}

	/**
	 * Get: dsRastreado.
	 *
	 * @return dsRastreado
	 */
	public String getDsRastreado() {
		return dsRastreado;
	}

	/**
	 * Set: dsRastreado.
	 *
	 * @param dsRastreado the ds rastreado
	 */
	public void setDsRastreado(String dsRastreado) {
		this.dsRastreado = dsRastreado;
	}

	/**
	 * Get: cdOperacaoDcom.
	 *
	 * @return cdOperacaoDcom
	 */
	public Long getCdOperacaoDcom() {
		return cdOperacaoDcom;
	}

	/**
	 * Set: cdOperacaoDcom.
	 *
	 * @param cdOperacaoDcom the cd operacao dcom
	 */
	public void setCdOperacaoDcom(Long cdOperacaoDcom) {
		this.cdOperacaoDcom = cdOperacaoDcom;
	}

	/**
	 * Get: dsSituacaoDcom.
	 *
	 * @return dsSituacaoDcom
	 */
	public String getDsSituacaoDcom() {
		return dsSituacaoDcom;
	}

	/**
	 * Set: dsSituacaoDcom.
	 *
	 * @param dsSituacaoDcom the ds situacao dcom
	 */
	public void setDsSituacaoDcom(String dsSituacaoDcom) {
		this.dsSituacaoDcom = dsSituacaoDcom;
	}

	/**
	 * Get: exibeDcom.
	 *
	 * @return exibeDcom
	 */
	public Boolean getExibeDcom() {
		return exibeDcom;
	}

	/**
	 * Set: exibeDcom.
	 *
	 * @param exibeDcom the exibe dcom
	 */
	public void setExibeDcom(Boolean exibeDcom) {
		this.exibeDcom = exibeDcom;
	}

	/**
	 * Get: numControleInternoLote.
	 *
	 * @return numControleInternoLote
	 */
	public Long getNumControleInternoLote() {
		return numControleInternoLote;
	}

	/**
	 * Set: numControleInternoLote.
	 *
	 * @param numControleInternoLote the num controle interno lote
	 */
	public void setNumControleInternoLote(Long numControleInternoLote) {
		this.numControleInternoLote = numControleInternoLote;
	}

}