/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.mantercontrato;

import java.io.IOException;

import br.com.bradesco.web.aq.view.util.FacesUtils;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.IImprimircontratoService;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.imprimircontrato.bean.ImprimirContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.mantercontrato.IManterContratoService;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

import com.lowagie.text.DocumentException;
 

/**
 * Nome: ContratoPdfBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ContratoPdfBean {
	
	/** Atributo imprimirContratoService. */
	private IImprimircontratoService imprimirContratoService;
	
	/** Atributo manterContratoImpl. */
	private IManterContratoService manterContratoImpl;
	
	/** Atributo entradaImprimirContrato. */
	private ImprimirContratoEntradaDTO entradaImprimirContrato = new ImprimirContratoEntradaDTO();

	/**
	 * Imprimir contrato.
	 *
	 * @throws DocumentException the document exception
	 * @throws IOException the IO exception
	 */
	public void imprimirContrato() throws DocumentException, IOException {
		
		String cdClubPessoa = FacesUtils.getRequestParameter("cdClubPessoa");
		String cdPessoaJuridicaContrato = FacesUtils.getRequestParameter("cdPessoaJuridicaContrato");
		String cdTipoContratoNegocio = FacesUtils.getRequestParameter("cdTipoContratoNegocio");
		String nrSequenciaContratoNegocio = FacesUtils.getRequestParameter("nrSequenciaContratoNegocio");
		String cdCorpoCpfCnpj = FacesUtils.getRequestParameter("cdCorpoCpfCnpj");
		String cdControleCpfCnpj = FacesUtils.getRequestParameter("cdControleCpfCnpj");
		String cdDigitoCpfCnpj = FacesUtils.getRequestParameter("cdDigitoCpfCnpj");
		String cdCpfCnpjParticipante = FacesUtils.getRequestParameter("cdCpfCnpjParticipante");
		String codFuncionalBradesco = FacesUtils.getRequestParameter("codFuncionalBradesco");
		String nmParticipante = FacesUtils.getRequestParameter("nmParticipante");
		String nmEmpresa = FacesUtils.getRequestParameter("nmEmpresa");
		
		entradaImprimirContrato.setCdClubPessoa(Long.valueOf(cdClubPessoa));
		entradaImprimirContrato.setCdPessoaJuridicaContrato(Long.valueOf(cdPessoaJuridicaContrato));
		entradaImprimirContrato.setCdTipoContratoNegocio(Integer.valueOf(cdTipoContratoNegocio));
		entradaImprimirContrato.setNrSequenciaContratoNegocio(Long.valueOf(nrSequenciaContratoNegocio));
		entradaImprimirContrato.setCdCorpoCpfCnpj(Long.valueOf(cdCorpoCpfCnpj));
		entradaImprimirContrato.setCdControleCpfCnpj(Integer.valueOf(cdControleCpfCnpj));
		entradaImprimirContrato.setCdDigitoCpfCnpj(Integer.valueOf(cdDigitoCpfCnpj));
		entradaImprimirContrato.setCdCpfCnpjParticipante(cdCpfCnpjParticipante);
		entradaImprimirContrato.setCodFuncionalBradesco(Long.valueOf(codFuncionalBradesco));
		entradaImprimirContrato.setNmParticipante(nmParticipante);
		entradaImprimirContrato.setNmEmpresa(nmEmpresa);
			
		ImprimirContratoSaidaDTO saidaImprimirContrato = getImprimirContratoService().imprimirContratoSaidaDTO(entradaImprimirContrato);
		
		getManterContratoImpl().gerarManterContrato(saidaImprimirContrato, FacesUtils.getServletResponse().getOutputStream());
		PgitFacesUtils.generateReportResponse("contrato");
	}

	
	// Getters  and Setters //
	
	/**
	 * Get: imprimirContratoService.
	 *
	 * @return imprimirContratoService
	 */
	public IImprimircontratoService getImprimirContratoService() {
		return imprimirContratoService;
	}

	/**
	 * Set: imprimirContratoService.
	 *
	 * @param imprimirContratoService the imprimir contrato service
	 */
	public void setImprimirContratoService(
			IImprimircontratoService imprimirContratoService) {
		this.imprimirContratoService = imprimirContratoService;
	}

	/**
	 * Get: manterContratoImpl.
	 *
	 * @return manterContratoImpl
	 */
	public IManterContratoService getManterContratoImpl() {
		return manterContratoImpl;
	}

	/**
	 * Set: manterContratoImpl.
	 *
	 * @param manterContratoImpl the manter contrato impl
	 */
	public void setManterContratoImpl(IManterContratoService manterContratoImpl) {
		this.manterContratoImpl = manterContratoImpl;
	}

	/**
	 * Get: entradaImprimirContrato.
	 *
	 * @return entradaImprimirContrato
	 */
	public ImprimirContratoEntradaDTO getEntradaImprimirContrato() {
		return entradaImprimirContrato;
	}

	/**
	 * Set: entradaImprimirContrato.
	 *
	 * @param entradaImprimirContrato the entrada imprimir contrato
	 */
	public void setEntradaImprimirContrato(
			ImprimirContratoEntradaDTO entradaImprimirContrato) {
		this.entradaImprimirContrato = entradaImprimirContrato;
	}
	
}
