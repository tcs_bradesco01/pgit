/*
 * Nome: br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.liberarpagconsemconsultsaldo
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.liberarpagconsemconsultsaldo;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCodigoBarrasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoCreditoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDARFSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDOCSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoContaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDebitoVeiculosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoDepIdentificadoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGAREEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGARESaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoGPSSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemCreditoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoOrdemPagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTEDSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloBradescoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultarpagamentosindividual.bean.DetalharPagtoTituloOutrosBancosSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.ILiberarPagConsolidadoSemConsultSaldoService;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.ConsultarLibPagtosConsSemConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.DetalharLibPagtosConsSemConsultaSaldoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.DetalharLibPagtosConsSemConsultaSaldoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarIntegralEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarIntegralSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarParcialEntradaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.LiberarParcialSaidaDTO;
import br.com.bradesco.web.pgit.service.business.liberarpagconsolidadosemconsultsaldo.bean.OcorrenciasDetLibPagConsSemConsSalSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: LiberarPagConsolidadoSemConsultSaldoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class LiberarPagConsolidadoSemConsultSaldoBean extends PagamentosBean {

    /** Atributo TELA_DETALHAR. */
    private static final String TELA_DETALHAR = "TELA_DETALHAR";

    /** Atributo TELA_CONFIRMAR_LIBERAR_PARCIAL. */
    private static final String TELA_CONFIRMAR_LIBERAR_PARCIAL = "TELA_CONFIRMAR_LIBERAR_PARCIAL";

    /** Atributo TELA_LIBERAR_PARCIAL. */
    private static final String TELA_LIBERAR_PARCIAL = "TELA_LIBERAR_PARCIAL";

    /** Atributo TELA_LIBERAR_INTEGRAL. */
    private static final String TELA_LIBERAR_INTEGRAL = "TELA_LIBERAR_INTEGRAL";

    /** Atributo TELA_CONSULTAR. */
    private static final String TELA_CONSULTAR = "TELA_CONSULTAR";

    /** Atributo liberarPagConsolidadoSemConsultSaldoImpl. */
    private ILiberarPagConsolidadoSemConsultSaldoService liberarPagConsolidadoSemConsultSaldoImpl;

    /** Atributo listaGridLiberarPagto. */
    private List<ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO> listaGridLiberarPagto;

    /** Atributo listaGridDetalharLiberarPagto. */
    private List<OcorrenciasDetLibPagConsSemConsSalSaidaDTO> listaGridDetalharLiberarPagto;

    /** Atributo listaGridDetalharLiberarPagtoSelecionados. */
    private List<OcorrenciasDetLibPagConsSemConsSalSaidaDTO> listaGridDetalharLiberarPagtoSelecionados;

    /** Atributo listaControleLiberarPagto. */
    private List<SelectItem> listaControleLiberarPagto;

    /** Atributo listaControleDetalharLiberarPagto. */
    private List<SelectItem> listaControleDetalharLiberarPagto;

    /** Atributo itemSelecionadoGridLiberarPagto. */
    private Integer itemSelecionadoGridLiberarPagto;

    /** Atributo itemSelecionadoGridDetalharLiberarPagto. */
    private Integer itemSelecionadoGridDetalharLiberarPagto;

    /** Atributo opcaoChecarTodos. */
    private boolean opcaoChecarTodos;

    /** Atributo exibeBeneficiario. */
    private boolean exibeBeneficiario;

    // Variaveis Cliente
    /** Atributo nrCnpjCpf. */
    private String nrCnpjCpf;

    /** Atributo dsRazaoSocial. */
    private String dsRazaoSocial;

    /** Atributo dsEmpresa. */
    private String dsEmpresa;

    /** Atributo nroContrato. */
    private String nroContrato;

    /** Atributo dsContrato. */
    private String dsContrato;

    /** Atributo cdSituacaoContrato. */
    private String cdSituacaoContrato;

    // Variaveis Conta Debito
    /** Atributo dsBancoDebito. */
    private String dsBancoDebito;

    /** Atributo dsAgenciaDebito. */
    private String dsAgenciaDebito;

    /** Atributo contaDebito. */
    private String contaDebito;

    /** Atributo tipoContaDebito. */
    private String tipoContaDebito;

    // Variaveis Pagamentos
    /** Atributo qtdePagamentos. */
    private Long qtdePagamentos;

    /** Atributo vlTotalPagamentos. */
    private BigDecimal vlTotalPagamentos;

    /** Atributo qtdePagamentosAutorizados. */
    private Long qtdePagamentosAutorizados;

    /** Atributo vlTotalPagamentosAutorizados. */
    private BigDecimal vlTotalPagamentosAutorizados;

    /** Atributo dataPagamento. */
    private String dataPagamento;

    /** Atributo nrRemessa. */
    private Long nrRemessa;

    // Favorecido
    /** Atributo numeroInscricaoFavorecido. */
    private String numeroInscricaoFavorecido;

    /** Atributo tipoFavorecido. */
    private Integer tipoFavorecido;

    /** Atributo descTipoFavorecido. */
    private String descTipoFavorecido;

    /** Atributo dsFavorecido. */
    private String dsFavorecido;

    /** Atributo cdFavorecido. */
    private String cdFavorecido;

    /** Atributo dsBancoFavorecido. */
    private String dsBancoFavorecido;

    /** Atributo dsAgenciaFavorecido. */
    private String dsAgenciaFavorecido;

    /** Atributo dsContaFavorecido. */
    private String dsContaFavorecido;

    /** Atributo dsTipoContaFavorecido. */
    private String dsTipoContaFavorecido;

    // Benefici�rio
    /** Atributo numeroInscricaoBeneficiario. */
    private String numeroInscricaoBeneficiario;

    /** Atributo tipoBeneficiario. */
    private String tipoBeneficiario;

    /** Atributo cdTipoBeneficiario. */
    private Integer cdTipoBeneficiario;

    /** Atributo nomeBeneficiario. */
    private String nomeBeneficiario;

    /** Atributo dsTipoBeneficiario. */
    private String dsTipoBeneficiario;

    /** Atributo dtNascimentoBeneficiario. */
    private String dtNascimentoBeneficiario;

    // Pagamento
    /** Atributo dsTipoServico. */
    private String dsTipoServico;

    /** Atributo dsIndicadorModalidade. */
    private String dsIndicadorModalidade;

    /** Atributo nrSequenciaArquivoRemessa. */
    private String nrSequenciaArquivoRemessa;

    /** Atributo nrLoteArquivoRemessa. */
    private String nrLoteArquivoRemessa;

    /** Atributo nrPagamento. */
    private String nrPagamento;

    /** Atributo cdListaDebito. */
    private String cdListaDebito;

    /** Atributo dtAgendamento. */
    private String dtAgendamento;

    /** Atributo dtPagamento. */
    private String dtPagamento;

    /** Atributo dtDevolucaoEstorno. */
    private String dtDevolucaoEstorno;

    /** Atributo dtVencimento. */
    private String dtVencimento;

    /** Atributo cdIndicadorEconomicoMoeda. */
    private String cdIndicadorEconomicoMoeda;

    /** Atributo qtMoeda. */
    private BigDecimal qtMoeda;

    /** Atributo vlrAgendamento. */
    private BigDecimal vlrAgendamento;

    /** Atributo vlrEfetivacao. */
    private BigDecimal vlrEfetivacao;

    /** Atributo cdSituacaoOperacaoPagamento. */
    private String cdSituacaoOperacaoPagamento;

    /** Atributo dsMotivoSituacao. */
    private String dsMotivoSituacao;

    /** Atributo dsPagamento. */
    private String dsPagamento;

    /** Atributo nrDocumento. */
    private String nrDocumento;

    /** Atributo tipoDocumento. */
    private String tipoDocumento;

    /** Atributo cdSerieDocumento. */
    private String cdSerieDocumento;

    /** Atributo vlDocumento. */
    private BigDecimal vlDocumento;

    /** Atributo dtEmissaoDocumento. */
    private String dtEmissaoDocumento;

    /** Atributo dsUsoEmpresa. */
    private String dsUsoEmpresa;

    /** Atributo dsMensagemPrimeiraLinha. */
    private String dsMensagemPrimeiraLinha;

    /** Atributo dsMensagemSegundaLinha. */
    private String dsMensagemSegundaLinha;

    /** Atributo cdBancoCredito. */
    private String cdBancoCredito;

    /** Atributo agenciaDigitoCredito. */
    private String agenciaDigitoCredito;

    /** Atributo contaDigitoCredito. */
    private String contaDigitoCredito;

    /** Atributo tipoContaCredito. */
    private String tipoContaCredito;

    /** Atributo cdBancoDestino. */
    private String cdBancoDestino;

    /** Atributo agenciaDigitoDestino. */
    private String agenciaDigitoDestino;

    /** Atributo contaDigitoDestino. */
    private String contaDigitoDestino;

    /** Atributo tipoContaDestino. */
    private String tipoContaDestino;

    /** Atributo vlDesconto. */
    private BigDecimal vlDesconto;

    /** Atributo vlAbatimento. */
    private BigDecimal vlAbatimento;

    /** Atributo vlMulta. */
    private BigDecimal vlMulta;

    /** Atributo vlMora. */
    private BigDecimal vlMora;

    /** Atributo vlDeducao. */
    private BigDecimal vlDeducao;

    /** Atributo vlAcrescimo. */
    private BigDecimal vlAcrescimo;

    /** Atributo vlImpostoRenda. */
    private BigDecimal vlImpostoRenda;

    /** Atributo vlIss. */
    private BigDecimal vlIss;

    /** Atributo vlIof. */
    private BigDecimal vlIof;

    /** Atributo vlInss. */
    private BigDecimal vlInss;

    /** Atributo cdIndentificadorJudicial. */
    private String cdIndentificadorJudicial;

    /** Atributo dtLimiteDescontoPagamento. */
    private String dtLimiteDescontoPagamento;

    /** Atributo dtLimitePagamento. */
    private String dtLimitePagamento;

    /** Atributo dsRastreado. */
    private String dsRastreado;

    /** Atributo carteira. */
    private String carteira;

    /** Atributo cdSituacaoTranferenciaAutomatica. */
    private String cdSituacaoTranferenciaAutomatica;

    // Trilha Auditoria
    /** Atributo dataHoraManutencao. */
    private String dataHoraManutencao;

    /** Atributo usuarioManutencao. */
    private String usuarioManutencao;

    /** Atributo tipoCanalManutencao. */
    private String tipoCanalManutencao;

    /** Atributo complementoManutencao. */
    private String complementoManutencao;

    /** Atributo dataHoraInclusao. */
    private String dataHoraInclusao;

    /** Atributo usuarioInclusao. */
    private String usuarioInclusao;

    /** Atributo tipoCanalInclusao. */
    private String tipoCanalInclusao;

    /** Atributo complementoInclusao. */
    private String complementoInclusao;


    /** Atributo numControleInternoLote. */
    private Long numControleInternoLote;



    // Lael - In�cio

    // Atributos
    /** Atributo tipoCanal. */
    private Integer tipoCanal;

    /** Atributo tipoTela. */
    private Integer tipoTela;

    /** Atributo numeroPagamento. */
    private String numeroPagamento;

    /** Atributo camaraCentralizadora. */
    private String camaraCentralizadora;

    /** Atributo finalidadeDoc. */
    private String finalidadeDoc;

    /** Atributo identificacaoTransferenciaDoc. */
    private String identificacaoTransferenciaDoc;

    /** Atributo identificacaoTitularidade. */
    private String identificacaoTitularidade;

    /** Atributo finalidadeTed. */
    private String finalidadeTed;

    /** Atributo identificacaoTransferenciaTed. */
    private String identificacaoTransferenciaTed;

    /** Atributo numeroOp. */
    private String numeroOp;

    /** Atributo dataExpiracaoCredito. */
    private String dataExpiracaoCredito;

    /** Atributo instrucaoPagamento. */
    private String instrucaoPagamento;

    /** Atributo numeroCheque. */
    private String numeroCheque;

    /** Atributo serieCheque. */
    private String serieCheque;

    /** Atributo indicadorAssociadoUnicaAgencia. */
    private String indicadorAssociadoUnicaAgencia;

    /** Atributo identificadorTipoRetirada. */
    private String identificadorTipoRetirada;

    /** Atributo identificadorRetirada. */
    private String identificadorRetirada;

    /** Atributo dataRetirada. */
    private String dataRetirada;

    /** Atributo vlImpostoMovFinanceira. */
    private BigDecimal vlImpostoMovFinanceira;

    /** Atributo codigoBarras. */
    private String codigoBarras;

    /** Atributo dddContribuinte. */
    private String dddContribuinte;

    /** Atributo telefoneContribuinte. */
    private String telefoneContribuinte;

    /** Atributo inscricaoEstadualContribuinte. */
    private String inscricaoEstadualContribuinte;

    /** Atributo agenteArrecadador. */
    private String agenteArrecadador;

    /** Atributo codigoReceita. */
    private String codigoReceita;

    /** Atributo numeroReferencia. */
    private String numeroReferencia;

    /** Atributo numeroCotaParcela. */
    private String numeroCotaParcela;

    /** Atributo percentualReceita. */
    private BigDecimal percentualReceita;

    /** Atributo periodoApuracao. */
    private String periodoApuracao;

    /** Atributo anoExercicio. */
    private String anoExercicio;

    /** Atributo dataReferencia. */
    private String dataReferencia;

    /** Atributo vlReceita. */
    private BigDecimal vlReceita;

    /** Atributo vlPrincipal. */
    private BigDecimal vlPrincipal;

    /** Atributo vlAtualizacaoMonetaria. */
    private BigDecimal vlAtualizacaoMonetaria;

    /** Atributo vlTotal. */
    private BigDecimal vlTotal;

    /** Atributo codigoCnae. */
    private String codigoCnae;

    /** Atributo placaVeiculo. */
    private String placaVeiculo;

    /** Atributo numeroParcelamento. */
    private String numeroParcelamento;

    /** Atributo codigoOrgaoFavorecido. */
    private String codigoOrgaoFavorecido;

    /** Atributo nomeOrgaoFavorecido. */
    private String nomeOrgaoFavorecido;

    /** Atributo codigoUfFavorecida. */
    private String codigoUfFavorecida;

    /** Atributo descricaoUfFavorecida. */
    private String descricaoUfFavorecida;

    /** Atributo vlAcrescimoFinanceiro. */
    private BigDecimal vlAcrescimoFinanceiro;

    /** Atributo vlHonorarioAdvogado. */
    private BigDecimal vlHonorarioAdvogado;

    /** Atributo observacaoTributo. */
    private String observacaoTributo;

    /** Atributo codigoInss. */
    private String codigoInss;

    /** Atributo dataCompetencia. */
    private String dataCompetencia;

    /** Atributo vlOutrasEntidades. */
    private BigDecimal vlOutrasEntidades;

    /** Atributo codigoTributo. */
    private String codigoTributo;

    /** Atributo codigoRenavam. */
    private String codigoRenavam;

    /** Atributo municipioTributo. */
    private String municipioTributo;

    /** Atributo ufTributo. */
    private String ufTributo;

    /** Atributo numeroNsu. */
    private String numeroNsu;

    /** Atributo opcaoTributo. */
    private String opcaoTributo;

    /** Atributo opcaoRetiradaTributo. */
    private String opcaoRetiradaTributo;

    /** Atributo clienteDepositoIdentificado. */
    private String clienteDepositoIdentificado;

    /** Atributo tipoDespositoIdentificado. */
    private String tipoDespositoIdentificado;

    /** Atributo produtoDepositoIdentificado. */
    private String produtoDepositoIdentificado;

    /** Atributo sequenciaProdutoDepositoIdentificado. */
    private String sequenciaProdutoDepositoIdentificado;

    /** Atributo bancoCartaoDepositoIdentificado. */
    private String bancoCartaoDepositoIdentificado;

    /** Atributo cartaoDepositoIdentificado. */
    private String cartaoDepositoIdentificado;

    /** Atributo bimCartaoDepositoIdentificado. */
    private String bimCartaoDepositoIdentificado;

    /** Atributo viaCartaoDepositoIdentificado. */
    private String viaCartaoDepositoIdentificado;

    /** Atributo codigoDepositante. */
    private String codigoDepositante;

    /** Atributo nomeDepositante. */
    private String nomeDepositante;

    /** Atributo codigoPagador. */
    private String codigoPagador;

    /** Atributo codigoOrdemCredito. */
    private String codigoOrdemCredito;

    /** Atributo panelBotoes. */
    private Boolean panelBotoes;

    /** Atributo nossoNumero. */
    private String nossoNumero;

    /** Atributo dsOrigemPagamento. */
    private String dsOrigemPagamento;

    /** Atributo disableArgumentosConsulta. */
    private boolean disableArgumentosConsulta;

    /** Atributo cdBancoOriginal. */
    private Integer cdBancoOriginal;

    /** Atributo dsBancoOriginal. */
    private String dsBancoOriginal;

    /** Atributo cdAgenciaBancariaOriginal. */
    private Integer cdAgenciaBancariaOriginal;

    /** Atributo cdDigitoAgenciaOriginal. */
    private String cdDigitoAgenciaOriginal;

    /** Atributo dsAgenciaOriginal. */
    private String dsAgenciaOriginal;

    /** Atributo cdContaBancariaOriginal. */
    private Long cdContaBancariaOriginal;

    /** Atributo cdDigitoContaOriginal. */
    private String cdDigitoContaOriginal;

    /** Atributo dsTipoContaOriginal. */
    private String dsTipoContaOriginal;

    /** Atributo dtFloatingPagamento. */
    private String dtFloatingPagamento;

    /** Atributo dtEfetivFloatPgto. */
    private String dtEfetivFloatPgto;

    /** Atributo vlFloatingPagamento. */
    private BigDecimal vlFloatingPagamento;

    /** Atributo cdOperacaoDcom. */
    private Long cdOperacaoDcom;

    /** Atributo dsSituacaoDcom. */
    private String dsSituacaoDcom;

    // Flag
    /** Atributo exibeDcom. */
    private Boolean exibeDcom;

    /**
     * Pesquisar.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisar(ActionEvent evt) {
	consultar();
	return "";
    }

    /**
     * Pesquisar integral.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisarIntegral(ActionEvent evt) {
	return "";
    }

    /**
     * Pesquisar selecionados.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisarSelecionados(ActionEvent evt) {
	return "";
    }

    /**
     * Pesquisar detalhar.
     *
     * @param evt the evt
     * @return the string
     */
    public String pesquisarDetalhar(ActionEvent evt) {
	preencheDetalharLiberarPagto("");
	return "";
    }

    /**
     * Limpa variaveis.
     */
    public void limpaVariaveis() {
	// Cliente e Contrato
	setNrCnpjCpf(null);
	setDsRazaoSocial("");
	setDsEmpresa("");
	setNroContrato("");
	setDsContrato("");
	setCdSituacaoContrato("");
	// Conta D�bito
	setDsBancoDebito("");
	setDsAgenciaDebito("");
	setContaDebito(null);
	setTipoContaDebito("");
    }

    /**
     * Limpar campos favorecido beneficiario.
     */
    public void limparCamposFavorecidoBeneficiario() {
	setNumeroInscricaoFavorecido("");
	setTipoFavorecido(0);
	setDsFavorecido("");
	setCdFavorecido("");
	setDsBancoFavorecido("");
	setDsAgenciaFavorecido("");
	setDsContaFavorecido("");
	setDsTipoContaFavorecido("");
	setNumeroInscricaoBeneficiario("");
	setTipoBeneficiario("");
	setNomeBeneficiario("");
	setDtNascimentoBeneficiario("");
	setCdTipoBeneficiario(0);
	setDsTipoBeneficiario("");
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - Cr�dito
    // Conta
    /**
     * Preenche dados pagto credito conta.
     */
    public void preencheDadosPagtoCreditoConta() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoCreditoContaEntradaDTO entradaDTO = new DetalharPagtoCreditoContaEntradaDTO();
	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoCreditoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
	    .detalharPagtoCreditoConta(entradaDTO);

	    limparCamposFavorecidoBeneficiario();
	    exibeBeneficiario = false;

	    // Cabe�alho
	    setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
		    .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
	    setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(), saidaDTO.getDsBancoDebito(), false));
	    setDsAgenciaDebito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
		    saidaDTO.getDsAgenciaDebito(), false));
	    setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(), saidaDTO.getDsDigAgenciaDebito(), false));
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    if (saidaDTO.getCdIndicadorModalidade() == 1 || saidaDTO.getCdIndicadorModalidade() == 3) {
		setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
			saidaDTO.getNmInscriFavorecido()));
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null
			: saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(),
			saidaDTO.getDsBancoFavorecido(), false));
		setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
			.getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
		setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(),
			saidaDTO.getCdDigContaCredito(), false));
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
		setExibeDcom(saidaDTO.getCdIndicadorModalidade() == 1 ? true : false);
	    } else {
		setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
		|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
		|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
		setExibeDcom(false);
	    }

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setCdBancoCredito(PgitUtil
		    .formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
	    setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
		    .getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
	    setContaDigitoCredito(PgitUtil.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
		    false));
	    setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
	    setTipoContaDestino(saidaDTO.getDsTipoContaDestino());
	    setVlDesconto(saidaDTO.getVlDesconto());
	    setVlAbatimento(saidaDTO.getVlAbatimento());
	    setVlMulta(saidaDTO.getVlMulta());
	    setVlMora(saidaDTO.getVlMora());
	    setVlDeducao(saidaDTO.getVlDeducao());
	    setVlAcrescimo(saidaDTO.getVlAcrescimo());
	    setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
	    setVlIss(saidaDTO.getVlIss());
	    setVlIof(saidaDTO.getVlIof());
	    setVlInss(saidaDTO.getVlInss());
	    setCdSituacaoTranferenciaAutomatica(saidaDTO.getCdSituacaoTranferenciaAutomatica());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
	    setVlrAgendamento(saidaDTO.getVlrAgendamento());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
	    setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
	    setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
	    setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
	    setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	    setNumControleInternoLote(saidaDTO.getNumControleInternoLote());

	    // Favorecidos
	    setCdBancoOriginal(saidaDTO.getCdBancoOriginal());
	    setDsBancoOriginal(saidaDTO.getCdBancoOriginal() + " - " + saidaDTO.getDsBancoOriginal());
	    setCdAgenciaBancariaOriginal(saidaDTO.getCdAgenciaBancariaOriginal());
	    setCdDigitoAgenciaOriginal(saidaDTO.getCdDigitoAgenciaOriginal());
	    setDsAgenciaOriginal(saidaDTO.getCdAgenciaBancariaOriginal() + "-" + saidaDTO.getCdDigitoAgenciaOriginal()
		    + " - " + saidaDTO.getDsAgenciaOriginal());
	    setCdContaBancariaOriginal(saidaDTO.getCdContaBancariaOriginal());
	    setCdDigitoContaOriginal(saidaDTO.getCdContaBancariaOriginal() + "-" + saidaDTO.getCdDigitoContaOriginal());
	    setDsTipoContaOriginal(saidaDTO.getDsTipoContaOriginal());

	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - D�bito
    // em conta
    /**
     * Preenche dados pagto debito conta.
     */
    public void preencheDadosPagtoDebitoConta() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoDebitoContaEntradaDTO entradaDTO = new DetalharPagtoDebitoContaEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoDebitoContaSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
	    .detalharPagtoDebitoConta(entradaDTO);

	    // Cabe�alho - Cliente
	    setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
		    .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // Sempre Carregar Conta de D�bito
	    setDsBancoDebito(PgitUtil.formatBanco(saidaDTO.getCdBancoDebito(), saidaDTO.getDsBancoDebito(), false));
	    setDsAgenciaDebito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaDebito(), saidaDTO.getCdDigAgenciaDebto(),
		    saidaDTO.getDsAgenciaDebito(), false));
	    setContaDebito(PgitUtil.formatConta(saidaDTO.getCdContaDebito(), saidaDTO.getDsDigAgenciaDebito(), false));
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    // Favorecido
	    setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(), saidaDTO
		    .getNmInscriFavorecido()));
	    setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
	    setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
	    setDsFavorecido(saidaDTO.getDsNomeFavorecido());
	    setCdFavorecido(saidaDTO.getCdFavorecido());
	    setDsBancoFavorecido(PgitUtil.formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(),
		    false));
	    setDsAgenciaFavorecido(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
		    .getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
	    setDsContaFavorecido(PgitUtil.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
		    false));
	    setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlrAgendamento());
	    setVlrEfetivacao(saidaDTO.getVlrEfetivacao());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setCdBancoCredito(PgitUtil
		    .formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
	    setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
		    .getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
	    setContaDigitoCredito(PgitUtil.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
		    false));
	    setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
	    setVlDesconto(saidaDTO.getVlDesconto());
	    setVlAbatimento(saidaDTO.getVlAbatimento());
	    setVlMulta(saidaDTO.getVlMulta());
	    setVlMora(saidaDTO.getVlMora());
	    setVlDeducao(saidaDTO.getVlDeducao());
	    setVlAcrescimo(saidaDTO.getVlAcrescimo());
	    setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
	    setVlIss(saidaDTO.getVlIss());
	    setVlIof(saidaDTO.getVlIof());
	    setVlInss(saidaDTO.getVlInss());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
	    setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
	    setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

	    // trilhaAuditoria
	    setCdBancoDestino(saidaDTO.getBancoDestinoFormatado());
	    setAgenciaDigitoDestino(saidaDTO.getAgenciaDestinoFormatada());
	    setContaDigitoDestino(saidaDTO.getContaDestinoFormatada());
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - DOC
    /**
     * Preenche dados pagto doc.
     */
    public void preencheDadosPagtoDOC() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoDOCEntradaDTO entradaDTO = new DetalharPagtoDOCEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoDOCSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoDOC(
		    entradaDTO);

	    limparCamposFavorecidoBeneficiario();

	    // Cabe�alho
	    setNrCnpjCpf(saidaDTO.getCdCpfCnpjCliente() == null || saidaDTO.getCdCpfCnpjCliente() == 0L ? "" : PgitUtil
		    .formatCpfCnpj(saidaDTO.getCdCpfCnpjCliente().toString()));
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    exibeBeneficiario = false;
	    if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
		setNumeroInscricaoFavorecido(PgitUtil.formataFavorecido(saidaDTO.getCdTipoInscricaoFavorecido(),
			saidaDTO.getNmInscriFavorecido()));
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
		setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
	    } else {
		setNumeroInscricaoBeneficiario(saidaDTO.getNmInscriBeneficio());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

		exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
		|| !PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals("")
		|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
		setExibeDcom(false);
	    }

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
	    setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
	    setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
	    setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
	    setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
	    setFinalidadeDoc(saidaDTO.getDsFinalidadeDocTed());
	    setIdentificacaoTransferenciaDoc(saidaDTO.getDsIdentificacaoTransferenciaPagto());
	    setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
	    setVlDesconto(saidaDTO.getVlDesconto());
	    setVlAbatimento(saidaDTO.getVlAbatimento());
	    setVlMulta(saidaDTO.getVlMulta());
	    setVlMora(saidaDTO.getVlMora());
	    setVlDeducao(saidaDTO.getVlDeducao());
	    setVlAcrescimo(saidaDTO.getVlAcrescimo());
	    setVlImpostoRenda(saidaDTO.getVlImpostoRenda());
	    setVlIss(saidaDTO.getVlIss());
	    setVlIof(saidaDTO.getVlIof());
	    setVlInss(saidaDTO.getVlInss());
	    setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
	    setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
	    setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - TED
    /**
     * Preenche dados pagto ted.
     */
    public void preencheDadosPagtoTED() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoTEDEntradaDTO entradaDTO = new DetalharPagtoTEDEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoTEDSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoTED(
		    entradaDTO);

	    // CLIENTE
	    setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // CONTA D�BITO
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    limparCamposFavorecidoBeneficiario();
	    exibeBeneficiario = false;
	    if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido());
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
		setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
	    } else {
		setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
		exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
		|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
		|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
		setExibeDcom(false);
	    }

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
	    setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
	    setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
	    setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
	    setCamaraCentralizadora(saidaDTO.getCdCamaraCentral());
	    setFinalidadeTed(PgitUtil.concatenarCampos(saidaDTO.getCdFinalidadeTed(),saidaDTO.getDsFinalidadeDocTed(), " - "));
	    setIdentificacaoTransferenciaTed(saidaDTO.getDsIdentificacaoTransferenciaPagto());
	    setIdentificacaoTitularidade(saidaDTO.getDsidentificacaoTitularPagto());
	    setVlDesconto(saidaDTO.getVlDesconto());
	    setVlAbatimento(saidaDTO.getVlAbatidoCredito());
	    setVlMulta(saidaDTO.getVlMultaCredito());
	    setVlMora(saidaDTO.getVlMoraJuros());
	    setVlDeducao(saidaDTO.getVlOutrasDeducoes());
	    setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
	    setVlImpostoRenda(saidaDTO.getVlIrCredito());
	    setVlIss(saidaDTO.getVlIssCredito());
	    setVlIof(saidaDTO.getVlIofCredito());
	    setVlInss(saidaDTO.getVlInssCredito());
	    setCdIndentificadorJudicial(saidaDTO.getCdIndentificadorJudicial());
	    setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
	    setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
	    setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - Ordem de
    // Pagamento
    /**
     * Preenche dados pagto ordem pagto.
     */
    public void preencheDadosPagtoOrdemPagto() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoOrdemPagtoEntradaDTO entradaDTO = new DetalharPagtoOrdemPagtoEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());

	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoOrdemPagtoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
	    .detalharPagtoOrdemPagto(entradaDTO);

	    // CLIENTE
	    setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // CONTA D�BITO
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    limparCamposFavorecidoBeneficiario();
	    exibeBeneficiario = false;
	    if (saidaDTO.getCdIndicadorModalidadePgit() == 1 || saidaDTO.getCdIndicadorModalidadePgit() == 3) {
		setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
		setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
		setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
		setDsFavorecido(saidaDTO.getDsNomeFavorecido());
		setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null
			: (saidaDTO.getCdFavorecido()));
		setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
		setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
		setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
		setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());
		setExibeDcom(saidaDTO.getCdIndicadorModalidadePgit() == 1 ? true : false);
	    } else {
		setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
		setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
		setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());
		exibeBeneficiario = !PgitUtil.verificaStringNula(getNumeroInscricaoBeneficiario()).equals("")
		|| (!PgitUtil.verificaStringNula(getDsTipoBeneficiario()).equals(""))
		|| !PgitUtil.verificaStringNula(getNomeBeneficiario()).equals("");
		setExibeDcom(false);
	    }

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setCdBancoCredito(saidaDTO.getBancoCreditoFormatado());
	    setAgenciaDigitoCredito(saidaDTO.getAgenciaCreditoFormatada());
	    setContaDigitoCredito(saidaDTO.getContaCreditoFormatada());
	    setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
	    setNumeroOp(saidaDTO.getNrOperacao());
	    setDataExpiracaoCredito(saidaDTO.getDtExpedicaoCredito());
	    setInstrucaoPagamento(saidaDTO.getCdInsPagamento());
	    setNumeroCheque(saidaDTO.getNrCheque());
	    setSerieCheque(saidaDTO.getNrSerieCheque());
	    setIndicadorAssociadoUnicaAgencia(saidaDTO.getDsUnidadeAgencia());

	    setIdentificadorTipoRetirada(saidaDTO.getDsIdentificacaoTipoRetirada());
	    setIdentificadorRetirada(saidaDTO.getDsIdentificacaoRetirada());

	    setDataRetirada(saidaDTO.getDtRetirada());
	    setVlImpostoMovFinanceira(saidaDTO.getVlImpostoMovimentacaoFinanceira());
	    setVlDesconto(saidaDTO.getVlDescontoCredito());
	    setVlAbatimento(saidaDTO.getVlAbatidoCredito());
	    setVlMulta(saidaDTO.getVlMultaCredito());
	    setVlMora(saidaDTO.getVlMoraJuros());
	    setVlDeducao(saidaDTO.getVlOutrasDeducoes());
	    setVlAcrescimo(saidaDTO.getVlOutroAcrescimo());
	    setVlImpostoRenda(saidaDTO.getVlIrCredito());
	    setVlIss(saidaDTO.getVlIssCredito());
	    setVlIof(saidaDTO.getVlIofCredito());
	    setVlInss(saidaDTO.getVlInssCredito());
	    setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
	    setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
	    setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - -
    // Titulos Bradesco
    /**
     * Preenche dados pagto titulo bradesco.
     */
    public void preencheDadosPagtoTituloBradesco() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoTituloBradescoEntradaDTO entradaDTO = new DetalharPagtoTituloBradescoEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());

	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoTituloBradescoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
	    .detalharPagtoTituloBradesco(entradaDTO);

	    // CLIENTE
	    setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // CONTA D�BITO
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    // Favorecido
	    setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
	    setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
	    setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
	    setDsFavorecido(saidaDTO.getDsNomeFavorecido());
	    setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null
		    : (saidaDTO.getCdFavorecido()));
	    setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
	    setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
	    setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
	    setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setCodigoBarras(saidaDTO.getCdBarras());
	    setVlDesconto(saidaDTO.getVlDesconto());
	    setVlAbatimento(saidaDTO.getVlAbatimento());
	    setVlMulta(saidaDTO.getVlMulta());
	    setVlMora(saidaDTO.getVlMoraJuros());
	    setVlDeducao(saidaDTO.getVlOutrasDeducoes());
	    setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
	    setNossoNumero(saidaDTO.getNossoNumero());
	    setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
	    setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
	    setCarteira(saidaDTO.getCarteira());
	    setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
	    setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
	    setDsRastreado(saidaDTO.getCdRastreado() == 1 ? "Sim":"N�o");
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - -
    // Titulos Outros Bancos
    /**
     * Preenche dados pagto titulo outros bancos.
     */
    public void preencheDadosPagtoTituloOutrosBancos() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoTituloOutrosBancosEntradaDTO entradaDTO = new DetalharPagtoTituloOutrosBancosEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoTituloOutrosBancosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
	    .detalharPagtoTituloOutrosBancos(entradaDTO);

	    // CLIENTE
	    setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // CONTA D�BITO
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    // Favorecido
	    setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
	    setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
	    setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
	    setDsFavorecido(saidaDTO.getDsNomeFavorecido());
	    setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null
		    : (saidaDTO.getCdFavorecido()));
	    setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
	    setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
	    setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
	    setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setCodigoBarras(saidaDTO.getCdBarras());
	    setVlDesconto(saidaDTO.getVlDesconto());
	    setVlAbatimento(saidaDTO.getVlAbatimento());
	    setVlMulta(saidaDTO.getVlMulta());
	    setVlMora(saidaDTO.getVlMoraJuros());
	    setVlDeducao(saidaDTO.getVlOutrasDeducoes());
	    setVlAcrescimo(saidaDTO.getVlOutrosAcrescimos());
	    setNossoNumero(saidaDTO.getNossoNumero());
	    setDsOrigemPagamento(saidaDTO.getDsOrigemPagamento());
	    setDtLimiteDescontoPagamento(saidaDTO.getDtLimiteDescontoPagamento());
	    setCarteira(saidaDTO.getCarteira());
	    setDtLimitePagamento(saidaDTO.getDtLimitePagamento());
	    setDsRastreado(saidaDTO.getCdRastreado() == 1 ? "Sim":"N�o");
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());

	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - DARF
    /**
     * Preenche dados pagto darf.
     */
    public void preencheDadosPagtoDARF() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoDARFEntradaDTO entradaDTO = new DetalharPagtoDARFEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoDARFSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoDARF(
		    entradaDTO);

	    // CLIENTE
	    setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // CONTA D�BITO
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    // Favorecido
	    setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
	    setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
	    setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
	    setDsFavorecido(saidaDTO.getDsNomeFavorecido());
	    setCdFavorecido(saidaDTO.getCdFavorecido());
	    setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
	    setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
	    setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
	    setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setDddContribuinte(saidaDTO.getCdDddTelefone());
	    setTelefoneContribuinte(saidaDTO.getCdTelefone());
	    setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
	    setAgenteArrecadador(saidaDTO.getCdAgenteArrecad());
	    setCodigoReceita(saidaDTO.getCdReceitaTributo());
	    setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
	    setNumeroCotaParcela(saidaDTO.getNrCotaParcela());
	    setPercentualReceita(saidaDTO.getCdPercentualReceita());
	    setPeriodoApuracao(saidaDTO.getDsPeriodoApuracao());
	    setAnoExercicio(saidaDTO.getCdDanoExercicio());
	    setDataReferencia(saidaDTO.getDtReferenciaTributo());
	    setVlReceita(saidaDTO.getVlReceita());
	    setVlPrincipal(saidaDTO.getVlPrincipal());
	    setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonet());
	    setVlAbatimento(saidaDTO.getVlAbatimentoCredito());
	    setVlMulta(saidaDTO.getVlMultaCredito());
	    setVlMora(saidaDTO.getVlMoraJuros());
	    setVlTotal(saidaDTO.getVlTotalTributo());
	    setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
	    setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
	    setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - GARE
    /**
     * Preenche dados pagto gare.
     */
    public void preencheDadosPagtoGARE() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoGAREEntradaDTO entradaDTO = new DetalharPagtoGAREEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoGARESaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoGARE(
		    entradaDTO);

	    // CLIENTE
	    setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // CONTA D�BITO
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    // Favorecido
	    setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
	    setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
	    setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
	    setDsFavorecido(saidaDTO.getDsNomeFavorecido());
	    setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null
		    : (saidaDTO.getCdFavorecido()));
	    setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
	    setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
	    setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
	    setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setDddContribuinte(saidaDTO.getCdDddContribuinte());
	    setTelefoneContribuinte(saidaDTO.getNrTelefoneContribuinte());
	    setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadualContribuinte());
	    setCodigoReceita(saidaDTO.getCdReceita());
	    setNumeroReferencia(saidaDTO.getNrReferencia());
	    setNumeroCotaParcela(saidaDTO.getNrCota());
	    setPercentualReceita(saidaDTO.getCdPercentualReceita());
	    setPeriodoApuracao(saidaDTO.getDsApuracaoTributo());
	    setDataReferencia(saidaDTO.getDtReferencia());
	    setCodigoCnae(saidaDTO.getCdCnae());
	    setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
	    setNumeroParcelamento(saidaDTO.getNrParcela());
	    setCodigoOrgaoFavorecido(saidaDTO.getCdOrFavorecido());
	    setNomeOrgaoFavorecido(saidaDTO.getDsOrFavorecido());
	    setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
	    setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
	    setVlReceita(saidaDTO.getVlReceita());
	    setVlPrincipal(saidaDTO.getVlPrincipal());
	    setVlAtualizacaoMonetaria(saidaDTO.getVlAtual());
	    setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
	    setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
	    setVlAbatimento(saidaDTO.getVlAbatimento());
	    setVlMulta(saidaDTO.getVlMulta());
	    setVlMora(saidaDTO.getVlMora());
	    setVlTotal(saidaDTO.getVlTotal());
	    setObservacaoTributo(saidaDTO.getDsTributo());
	    setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - GPS
    /**
     * Preenche dados pagto gps.
     */
    public void preencheDadosPagtoGPS() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoGPSEntradaDTO entradaDTO = new DetalharPagtoGPSEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoGPSSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl().detalharPagtoGPS(
		    entradaDTO);

	    // CLIENTE
	    setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // CONTA D�BITO
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    // Favorecido
	    setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
	    setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
	    setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
	    setDsFavorecido(saidaDTO.getDsNomeFavorecido());
	    setCdFavorecido(saidaDTO.getCdFavorecido());
	    setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
	    setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
	    setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
	    setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setDddContribuinte(saidaDTO.getCdDddContribuinte());
	    setTelefoneContribuinte(saidaDTO.getCdTelContribuinte());
	    setCodigoInss(saidaDTO.getCdInss());
	    setDataCompetencia(saidaDTO.getDsMesAnoCompt());
	    setVlPrincipal(saidaDTO.getVlPrincipal());
	    setVlOutrasEntidades(saidaDTO.getVlOutraEntidade());
	    setVlAbatimento(saidaDTO.getVlAbatimento());
	    setVlMulta(saidaDTO.getVlMulta());
	    setVlMora(saidaDTO.getVlMora());
	    setVlTotal(saidaDTO.getVlTotal());
	    setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - C�digo
    // de Barras
    /**
     * Preenche dados pagto codigo barras.
     */
    public void preencheDadosPagtoCodigoBarras() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoCodigoBarrasEntradaDTO entradaDTO = new DetalharPagtoCodigoBarrasEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoCodigoBarrasSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
	    .detalharPagtoCodigoBarras(entradaDTO);

	    // CLIENTE
	    setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // CONTA D�BITO
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    // Favorecido
	    setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
	    setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
	    setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
	    setDsFavorecido(saidaDTO.getDsNomeFavorecido());
	    setCdFavorecido(saidaDTO.getCdFavorecido());
	    setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
	    setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
	    setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
	    setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setDddContribuinte(saidaDTO.getCdDddTelefone());
	    setTelefoneContribuinte(saidaDTO.getNrTelefone());
	    setInscricaoEstadualContribuinte(saidaDTO.getCdInscricaoEstadual());
	    setCodigoBarras(saidaDTO.getDsCodigoBarraTributo());
	    setCodigoReceita(saidaDTO.getCdReceitaTributo());
	    setAgenteArrecadador(saidaDTO.getCdAgenteArrecadador());
	    setCodigoTributo(saidaDTO.getCdTributoArrecadado());
	    setNumeroReferencia(saidaDTO.getNrReferenciaTributo());
	    setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTributo());
	    setPercentualReceita(saidaDTO.getCdPercentualTributo());
	    setPeriodoApuracao(saidaDTO.getCdPeriodoApuracao());
	    setAnoExercicio(saidaDTO.getCdAnoExercicioTributo());
	    setDataReferencia(saidaDTO.getDtReferenciaTributo());
	    setDataCompetencia(saidaDTO.getDtCompensacao());
	    setCodigoCnae(saidaDTO.getCdCnae());
	    setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
	    setNumeroParcelamento(saidaDTO.getNrParcelamentoTributo());
	    setCodigoOrgaoFavorecido(saidaDTO.getCdOrgaoFavorecido());
	    setNomeOrgaoFavorecido(saidaDTO.getDsOrgaoFavorecido());
	    setCodigoUfFavorecida(saidaDTO.getCdUfFavorecido());
	    setDescricaoUfFavorecida(saidaDTO.getDsUfFavorecido());
	    setVlReceita(saidaDTO.getVlReceita());
	    setVlPrincipal(saidaDTO.getVlPrincipal());
	    setVlAtualizacaoMonetaria(saidaDTO.getVlAtualizado());
	    setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimo());
	    setVlHonorarioAdvogado(saidaDTO.getVlHonorario());
	    setVlDesconto(saidaDTO.getVlDesconto());
	    setVlAbatimento(saidaDTO.getVlAbatimento());
	    setVlMulta(saidaDTO.getVlMulta());
	    setVlMora(saidaDTO.getVlMora());
	    setVlTotal(saidaDTO.getVlTotal());
	    setObservacaoTributo(saidaDTO.getDsObservacao());
	    setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - D�bito
    // de Veiculos
    /**
     * Preenche dados pagto debito veiculos.
     */
    public void preencheDadosPagtoDebitoVeiculos() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());

	    DetalharPagtoDebitoVeiculosEntradaDTO entradaDTO = new DetalharPagtoDebitoVeiculosEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoDebitoVeiculosSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
	    .detalharPagtoDebitoVeiculos(entradaDTO);

	    setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // CONTA D�BITO
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    // Favorecido
	    setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
	    setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
	    setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
	    setDsFavorecido(saidaDTO.getDsNomeFavorecido());
	    setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null
		    : (saidaDTO.getCdFavorecido()));
	    setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
	    setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
	    setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
	    setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setNumeroCotaParcela(saidaDTO.getNrCotaParcelaTrib());
	    setAnoExercicio(saidaDTO.getDtAnoExercTributo());
	    setPlacaVeiculo(saidaDTO.getCdPlacaVeiculo());
	    setCodigoRenavam(saidaDTO.getCdRenavamVeicTributo());
	    setMunicipioTributo(saidaDTO.getCdMunicArrecadTrib());
	    setUfTributo(saidaDTO.getCdUfTributo());
	    setNumeroNsu(saidaDTO.getNrNsuTributo());
	    setOpcaoTributo(saidaDTO.getCdOpcaoTributo());
	    setOpcaoRetiradaTributo(saidaDTO.getCdOpcaoRetirada());
	    setVlPrincipal(saidaDTO.getVlPrincipalTributo());
	    setVlAtualizacaoMonetaria(saidaDTO.getVlAtualMonetar());
	    setVlAcrescimoFinanceiro(saidaDTO.getVlAcrescimoFinanc());
	    setVlDesconto(saidaDTO.getVlDesconto());
	    setVlAbatimento(saidaDTO.getVlAbatimento());
	    setVlMulta(saidaDTO.getVlMulta());
	    setVlMora(saidaDTO.getVlMoraJuros());
	    setVlTotal(saidaDTO.getVlTotalTributo());
	    setObservacaoTributo(saidaDTO.getDsObservacaoTributo());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - Deposito
    // Identificado
    /**
     * Preenche dados pagto dep identificado.
     */
    public void preencheDadosPagtoDepIdentificado() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());
	    DetalharPagtoDepIdentificadoEntradaDTO entradaDTO = new DetalharPagtoDepIdentificadoEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoDepIdentificadoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
	    .detalharPagtoDepIdentificado(entradaDTO);

	    // CLIENTE
	    setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // CONTA D�BITO
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    // Beneficiario
	    setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
	    setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
	    setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

	    // Favorecido
	    setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
	    setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
	    setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
	    setDsFavorecido(saidaDTO.getDsNomeFavorecido());
	    setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null
		    : (saidaDTO.getCdFavorecido()));
	    setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
	    setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
	    setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
	    setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setCdBancoCredito(PgitUtil
		    .formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
	    setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
		    .getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
	    setContaDigitoCredito(PgitUtil.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
		    false));
	    setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
	    setClienteDepositoIdentificado(saidaDTO.getCdClienteDepositoId());
	    setTipoDespositoIdentificado(saidaDTO.getCdTipoDepositoId());
	    setProdutoDepositoIdentificado(saidaDTO.getCdProdutoDepositoId());
	    setSequenciaProdutoDepositoIdentificado(saidaDTO.getCdSequenciaProdutoDepositoId());
	    setBancoCartaoDepositoIdentificado(saidaDTO.getCdBancoCartaoDepositanteId());
	    setCartaoDepositoIdentificado(saidaDTO.getCdCartaoDepositante());
	    setBimCartaoDepositoIdentificado(saidaDTO.getCdBinCartaoDepositanteId());
	    setViaCartaoDepositoIdentificado(saidaDTO.getCdViaCartaoDepositanteId());
	    setCodigoDepositante(saidaDTO.getCdDepositoAnteriorId());
	    setNomeDepositante(saidaDTO.getDsDepositoAnteriorId());
	    setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
	    setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
	    setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
	    setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
	    setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
	    setVlAcrescimo(saidaDTO.getVloutroAcrescimoCreditoPagamento());
	    setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
	    setVlIss(saidaDTO.getVlIssCreditoPagamento());
	    setVlIof(saidaDTO.getVlIofCreditoPagamento());
	    setVlInss(saidaDTO.getVlInssCreditoPagamento());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
	    setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
	    setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Detalhar Liberar Pagamentos Consolidados sem Consulta de Saldo - Ordem de
    // Cr�dito
    /**
     * Preenche dados pagto ordem credito.
     */
    public void preencheDadosPagtoOrdemCredito() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());
	    OcorrenciasDetLibPagConsSemConsSalSaidaDTO registroSelecionadoDetalhar = getListaGridDetalharLiberarPagto()
	    .get(getItemSelecionadoGridDetalharLiberarPagto());
	    DetalharPagtoOrdemCreditoEntradaDTO entradaDTO = new DetalharPagtoOrdemCreditoEntradaDTO();

	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setDsEfetivacaoPagamento("");
	    entradaDTO.setCdControlePagamento(registroSelecionadoDetalhar.getNrPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdBancoCredito(registroSelecionadoDetalhar.getCdBanco());
	    entradaDTO.setCdAgenciaCredito(registroSelecionadoDetalhar.getCdAgencia());
	    entradaDTO.setCdContaCredito(registroSelecionadoDetalhar.getCdConta());
	    entradaDTO.setCdAgendadosPagoNaoPago(Integer.valueOf(getAgendadosPagosNaoPagos()));
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setDtHoraManutencao("");

	    DetalharPagtoOrdemCreditoSaidaDTO saidaDTO = getConsultarPagamentosIndividualServiceImpl()
	    .detalharPagtoOrdemCredito(entradaDTO);

	    // CLIENTE
	    setNrCnpjCpf(saidaDTO.getCpfCnpjClienteFormatado());
	    setDsRazaoSocial(saidaDTO.getNomeCliente());
	    setDsEmpresa(saidaDTO.getDsPessoaJuridicaContrato());
	    setNroContrato(saidaDTO.getNrSequenciaContratoNegocio());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getCdSituacaoContrato());

	    // CONTA D�BITO
	    setDsBancoDebito(saidaDTO.getBancoDebitoFormatado());
	    setDsAgenciaDebito(saidaDTO.getAgenciaDebitoFormatada());
	    setContaDebito(saidaDTO.getContaDebitoFormatada());
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());

	    // Beneficiario
	    setNumeroInscricaoBeneficiario(saidaDTO.getNumeroInscricaoBeneficiarioFormatado());
	    setDsTipoBeneficiario(saidaDTO.getCdTipoInscriBeneficio());
	    setNomeBeneficiario(saidaDTO.getCdNomeBeneficio());

	    // Favorecido
	    setNumeroInscricaoFavorecido(saidaDTO.getNumeroInscricaoFavorecidoFormatado());
	    setTipoFavorecido(saidaDTO.getCdTipoInscricaoFavorecido());
	    setDescTipoFavorecido(PgitUtil.validaTipoFavorecido(getTipoFavorecido()));
	    setDsFavorecido(saidaDTO.getDsNomeFavorecido());
	    setCdFavorecido(saidaDTO.getCdFavorecido() == null || saidaDTO.getCdFavorecido().equals("") ? null
		    : (saidaDTO.getCdFavorecido()));
	    setDsBancoFavorecido(saidaDTO.getBancoCreditoFormatado());
	    setDsAgenciaFavorecido(saidaDTO.getAgenciaCreditoFormatada());
	    setDsContaFavorecido(saidaDTO.getContaCreditoFormatada());
	    setDsTipoContaFavorecido(saidaDTO.getDsTipoContaFavorecido());

	    // Pagamento
	    setDsTipoServico(saidaDTO.getDsTipoServicoOperacao());
	    setDsIndicadorModalidade(saidaDTO.getDsModalidadeRelacionado());
	    setNrSequenciaArquivoRemessa(saidaDTO.getNrSequenciaArquivoRemessa());
	    setNrLoteArquivoRemessa(saidaDTO.getNrLoteArquivoRemessa());
	    setNumeroPagamento(saidaDTO.getCdControlePagamento());
	    setCdListaDebito(saidaDTO.getCdListaDebito());
	    setDtAgendamento(saidaDTO.getDtAgendamento());
	    setDtPagamento(saidaDTO.getDtPagamento());
	    setDtVencimento(saidaDTO.getDtVencimento());
	    setCdIndicadorEconomicoMoeda(saidaDTO.getDsMoeda());
	    setQtMoeda(saidaDTO.getQtMoeda());
	    setVlrAgendamento(saidaDTO.getVlAgendado());
	    setVlrEfetivacao(saidaDTO.getVlEfetivo());
	    setCdSituacaoOperacaoPagamento(saidaDTO.getCdSituacaoPagamento());
	    setDsMotivoSituacao(saidaDTO.getDsMotivoSituacao());
	    setDsPagamento(saidaDTO.getDsPagamento());
	    setNrDocumento(saidaDTO.getNrDocumento());
	    setTipoDocumento(saidaDTO.getDsTipoDocumento());
	    setCdSerieDocumento(saidaDTO.getCdSerieDocumento());
	    setVlDocumento(saidaDTO.getVlDocumento());
	    setDtEmissaoDocumento(saidaDTO.getDtEmissaoDocumento());
	    setDsUsoEmpresa(saidaDTO.getDsUsoEmpresa());
	    setDsMensagemPrimeiraLinha(saidaDTO.getDsMensagemPrimeiraLinha());
	    setDsMensagemSegundaLinha(saidaDTO.getDsMensagemSegundaLinha());
	    setCdBancoCredito(PgitUtil
		    .formatBanco(saidaDTO.getCdBancoCredito(), saidaDTO.getDsBancoFavorecido(), false));
	    setAgenciaDigitoCredito(PgitUtil.formatAgencia(saidaDTO.getCdAgenciaCredito(), saidaDTO
		    .getCdDigAgenciaCredito(), saidaDTO.getDsAgenciaFavorecido(), false));
	    setContaDigitoCredito(PgitUtil.formatConta(saidaDTO.getCdContaCredito(), saidaDTO.getCdDigContaCredito(),
		    false));
	    setTipoContaCredito(saidaDTO.getDsTipoContaFavorecido());
	    setCodigoPagador(saidaDTO.getCdPagador());
	    setCodigoOrdemCredito(saidaDTO.getCdOrdemCreditoPagamento());
	    setVlDesconto(saidaDTO.getVlDescontoCreditoPagamento());
	    setVlAbatimento(saidaDTO.getVlAbatidoCreditoPagamento());
	    setVlMulta(saidaDTO.getVlMultaCreditoPagamento());
	    setVlMora(saidaDTO.getVlMoraJurosCreditoPagamento());
	    setVlDeducao(saidaDTO.getVlOutraDeducaoCreditoPagamento());
	    setVlAcrescimo(saidaDTO.getVlOutroAcrescimoCreditoPagamento());
	    setVlImpostoRenda(saidaDTO.getVlIrCreditoPagamento());
	    setVlIss(saidaDTO.getVlIssCreditoPagamento());
	    setVlIof(saidaDTO.getVlIofCreditoPagamento());
	    setVlInss(saidaDTO.getVlInssCreditoPagamento());
	    setDtDevolucaoEstorno(saidaDTO.getDtDevolucaoEstorno());
	    setDtFloatingPagamento(saidaDTO.getDtFloatingPagamento());
	    setDtEfetivFloatPgto(saidaDTO.getDtEfetivFloatPgto());
	    setVlFloatingPagamento(saidaDTO.getVlFloatingPagamento());
	    setCdOperacaoDcom(saidaDTO.getCdOperacaoDcom());
	    setDsSituacaoDcom(saidaDTO.getDsSituacaoDcom());

	    // trilhaAuditoria
	    setDataHoraInclusao(saidaDTO.getDataHoraInclusaoFormatada());
	    setUsuarioInclusao(saidaDTO.getUsuarioInclusaoFormatado());
	    setTipoCanalInclusao(saidaDTO.getTipoCanalInclusaoFormatado());
	    setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
	    setDataHoraManutencao(saidaDTO.getDataHoraManutencaoFormatada());
	    setUsuarioManutencao(saidaDTO.getUsuarioManutencaoFormatado());
	    setTipoCanalManutencao(saidaDTO.getTipoCanalManutencaoFormatado());
	    setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    // Lael - Fim

    /**
     * Iniciar tela.
     *
     * @param evt the evt
     */
    public void iniciarTela(ActionEvent evt) {
	// Tela de Filtro
	getFiltroAgendamentoEfetivacaoEstornoBean().setEntradaConsultarListaClientePessoas(
		new ConsultarListaClientePessoasEntradaDTO());
	getFiltroAgendamentoEfetivacaoEstornoBean().setSaidaConsultarListaClientePessoas(
		new ConsultarListaClientePessoasSaidaDTO());
	getFiltroAgendamentoEfetivacaoEstornoBean().listarEmpresaGestora();
	getFiltroAgendamentoEfetivacaoEstornoBean().listarTipoContrato();
	getFiltroAgendamentoEfetivacaoEstornoBean().setPaginaRetorno("conLiberarPagtosConsolidadosSemConSaldo");

	limparInformacaoConsulta();

	limparCampos();

	// Carregamento dos combos
	listarTipoServicoPagto();
	listarInscricaoFavorecido();
	listarConsultarSituacaoPagamento();

	listarOrgaoPagador();
	setHabilitaArgumentosPesquisa(false);
	getFiltroAgendamentoEfetivacaoEstornoBean().setClienteContratoSelecionado(false);
	getFiltroAgendamentoEfetivacaoEstornoBean().setEmpresaGestoraFiltro(2269651L);

	// Radio de Agendados � Pagos/N�o Pagos, ficara sempre protegido e com a
	// op��o �Agendados� selecionada;
	setAgendadosPagosNaoPagos("1");

	setDisableArgumentosConsulta(false);
	setPanelBotoes(false);
    }

    // M�todo sobreposto para n�o limpar os agendados - pagos/nao pagos
    /**
     * (non-Javadoc)
     * @see br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.PagamentosBean#limparCampos()
     */
    public void limparCampos() {
	getFiltroAgendamentoEfetivacaoEstornoBean().limparParticipanteContrato();
	setChkContaDebito(false);
	limparContaDebito();
	limparRemessaFavorecidoBeneficiarioLinha();
	limparSituacaoMotivo();
	limparTipoServicoModalidade();

	setDataInicialPagamentoFiltro(new Date());
	setDataFinalPagamentoFiltro(new Date());
	setChkParticipanteContrato(false);
	setChkRemessa(false);
	setChkServicoModalidade(false);
	setChkSituacaoMotivo(false);
	setRadioPesquisaFiltroPrincipal("");
	setRadioFavorecidoFiltro("");
	setRadioBeneficiarioFiltro("");

	setClassificacaoFiltro(null);
    }

    /**
     * Pesquisar parcial.
     *
     * @param evt the evt
     */
    public void pesquisarParcial(ActionEvent evt) {

	for (int i = 0; i < listaGridDetalharLiberarPagto.size(); i++) {
	    listaGridDetalharLiberarPagto.get(i).setCheck(false);
	}
	setOpcaoChecarTodos(false);
    }

    /**
     * Pesquisar.
     */
    public void pesquisar() {
	setOpcaoChecarTodos(false);
	setPanelBotoes(false);
	for (int i = 0; i < getListaGridDetalharLiberarPagto().size(); i++) {
	    getListaGridDetalharLiberarPagto().get(i).setCheck(false);
	}
    }

    /**
     * Carrega lista selecionados.
     */
    private void carregaListaSelecionados() {
	setVlTotalPagamentosAutorizados(BigDecimal.ZERO);
	setListaGridDetalharLiberarPagtoSelecionados(new ArrayList<OcorrenciasDetLibPagConsSemConsSalSaidaDTO>());
	for (OcorrenciasDetLibPagConsSemConsSalSaidaDTO o : getListaGridDetalharLiberarPagto()) {
	    if (o.isCheck()) {
		getListaGridDetalharLiberarPagtoSelecionados().add(o);
		setVlTotalPagamentosAutorizados(new BigDecimal(getVlTotalPagamentosAutorizados().doubleValue()
			+ o.getVlPagamento().doubleValue()));
	    }
	}
	setQtdePagamentosAutorizados(new Long(getListaGridDetalharLiberarPagtoSelecionados().size()));
    }

    /**
     * Consultar.
     *
     * @param evt the evt
     */
    public void consultar(ActionEvent evt) {
	consultar();
    }

    /**
     * Consultar.
     */
    public void consultar() {
	setListaGridLiberarPagto(new ArrayList<ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO>());
	setItemSelecionadoGridLiberarPagto(null);

	try {
	    ConsultarLibPagtosConsSemConsultaSaldoEntradaDTO entrada = new ConsultarLibPagtosConsSemConsultaSaldoEntradaDTO();

	    /*
	     * Se selecionado �Filtrar por Cliente� ou �Filtrar por Contrato� mover �1�. Se selecionado �Filtrar por
	     * Conta D�bito� mover �2�;
	     */
	    if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")
		    || getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) {
		entrada.setCdTipoPesquisa(1);
	    } else {
		entrada.setCdTipoPesquisa(2);
	    }

	    entrada.setCdAgendadosPagosNaoPagos(Integer.parseInt(getAgendadosPagosNaoPagos()));

	    if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("0")
		    || getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("1")) {
		/*
		 * >> Se Selecionado Filtrar por Cliente Mover Valores vindos da lista >> Se Selecionado Filtrar por
		 * Contrato Passar valor retornado da lista(Verificado por email), na especifica��o definia passar os
		 * combo e inputtext, o que poderia gerar problemas caso o usu�rio altere os valores do combo ap�s ter
		 * feito a pesquisa
		 */
		entrada.setCdPessoaJuridicaContrato(getFiltroAgendamentoEfetivacaoEstornoBean()
			.getCdPessoaJuridicaContrato() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
				.getCdPessoaJuridicaContrato() : 0L);
		entrada
		.setCdTipoContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
			.getCdTipoContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
				.getCdTipoContratoNegocio()
				: 0);
		entrada.setNrSequenciaContratoNegocio(getFiltroAgendamentoEfetivacaoEstornoBean()
			.getNrSequenciaContratoNegocio() != null ? getFiltroAgendamentoEfetivacaoEstornoBean()
				.getNrSequenciaContratoNegocio() : 0L);
	    }

	    if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
		/*
		 * Se selecionado �Filtrar por Conta D�bito� mover zeros para os cambos abaixo
		 */
		entrada.setCdPessoaJuridicaContrato(0L);
		entrada.setCdTipoContratoNegocio(0);
		entrada.setNrSequenciaContratoNegocio(0L);
	    }

	    /*
	     * Se estiver selecionado "Filtrar por Conta D�bito", passar os campos deste filtro Se estiver selecionado
	     * outro tipo de filtro, verificar se o "Conta D�bito" dos argumentos de pesquisa esta marcado Se estiver
	     * marcado, passar os valores correspondentes da tela,caso contr�rio passar zeros
	     */
	    if (getFiltroAgendamentoEfetivacaoEstornoBean().getTipoFiltroSelecionado().equals("2")) {
		entrada.setCdBanco(getFiltroAgendamentoEfetivacaoEstornoBean().getBancoContaDebitoFiltro());
		entrada.setCdAgencia(getFiltroAgendamentoEfetivacaoEstornoBean().getAgenciaContaDebitoBancariaFiltro());
		entrada.setCdConta(getFiltroAgendamentoEfetivacaoEstornoBean().getContaBancariaContaDebitoFiltro());
		entrada.setCdDigitoConta(getFiltroAgendamentoEfetivacaoEstornoBean().getDigitoContaDebitoFiltro());
	    } else {
		if (isChkContaDebito()) {
		    entrada.setCdBanco(getCdBancoContaDebito());
		    entrada.setCdAgencia(getCdAgenciaBancariaContaDebito());
		    entrada.setCdConta(getCdContaBancariaContaDebito());
		    entrada.setCdDigitoConta(getCdDigitoContaContaDebito() != null
			    && !getCdDigitoContaContaDebito().equals("") ? getCdDigitoContaContaDebito() : "0");
		} else {
		    entrada.setCdBanco(0);
		    entrada.setCdAgencia(0);
		    entrada.setCdConta(0L);
		    entrada.setCdDigitoConta("00");
		}
	    }

	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	    entrada.setDtCreditoPagamentoInicio((getDataInicialPagamentoFiltro() == null) ? "" : sdf
		    .format(getDataInicialPagamentoFiltro()));
	    entrada.setDtCreditoPagamentoFim((getDataFinalPagamentoFiltro() == null) ? "" : sdf
		    .format(getDataFinalPagamentoFiltro()));

	    entrada.setNrArquivoRemessaPagamento(getNumeroRemessaFiltro().equals("") ? 0L : Long
		    .parseLong(getNumeroRemessaFiltro()));

	    entrada.setCdProdutoServicoOperacao(getTipoServicoFiltro() != null ? getTipoServicoFiltro() : 0);
	    entrada.setCdProdutoServicoRelacionado(getModalidadeFiltro() != null ? getModalidadeFiltro() : 0);

	    // Foi pedido para retirar o cambo de situa��o e motivo da tela -
	    // email beatriz nogueira de santana dia
	    // 29/03/2011 as 13:58
	    entrada.setCdSituacaoOperacaoPagamento(0);
	    entrada.setCdMotivoSituacaoPagamento(0);
	    setListaGridLiberarPagto(getLiberarPagConsolidadoSemConsultSaldoImpl()
		    .consultarLibPagtosConsSemConsultaSaldo(entrada));
	    setListaControleLiberarPagto(new ArrayList<SelectItem>());
	    for (int i = 0; i < getListaGridLiberarPagto().size(); i++) {
		getListaControleLiberarPagto().add(new SelectItem(i, ""));
	    }
	    setDisableArgumentosConsulta(true);
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	    setListaGridLiberarPagto(null);
	    setDisableArgumentosConsulta(false);
	}
    }

    /**
     * Limpar selecionados.
     */
    public void limparSelecionados() {
	for (OcorrenciasDetLibPagConsSemConsSalSaidaDTO o : getListaGridDetalharLiberarPagto()) {
	    o.setCheck(false);
	}
	setPanelBotoes(false);
	setOpcaoChecarTodos(false);
    }

    /**
     * Limpar informacao consulta.
     */
    public void limparInformacaoConsulta() {
	setOpcaoChecarTodos(false);
	setListaGridLiberarPagto(null);
	setItemSelecionadoGridLiberarPagto(null);
    }

    /**
     * Limpar args lista apos pesquisa cliente contrato.
     *
     * @param evt the evt
     */
    public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
	limparCampos();
	limparInformacaoConsulta();
    }

    /**
     * Limpar tela principal.
     *
     * @return the string
     */
    public String limparTelaPrincipal() {
	limparFiltroPrincipal();
	limparInformacaoConsulta();
	setPanelBotoes(false);

	return "";
    }

    /**
     * Limpar apos alterar opcao cliente.
     *
     * @return the string
     */
    public String limparAposAlterarOpcaoCliente() {
	// Limpar Argumentos de Pesquisa e Lista
	getFiltroAgendamentoEfetivacaoEstornoBean().limpar();
	limparInformacaoConsulta();
	setDisableArgumentosConsulta(false);
	setPanelBotoes(false);

	limparCampos();

	return "";
    }

    /**
     * Limpar.
     *
     * @return the string
     */
    public String limpar() {
	limparFiltroPrincipal();
	limparInformacaoConsulta();
	getFiltroAgendamentoEfetivacaoEstornoBean().setTipoFiltroSelecionado(null);
	setListaGridLiberarPagto(null);
	setItemSelecionadoGridLiberarPagto(null);
	setDisableArgumentosConsulta(false);
	return "";
    }

    /**
     * Limpar grid.
     *
     * @return the string
     */
    public String limparGrid() {
	setListaGridLiberarPagto(null);
	setItemSelecionadoGridLiberarPagto(null);
	setDisableArgumentosConsulta(false);
	return "";
    }

    /**
     * Detalhar.
     *
     * @return the string
     */
    public String detalhar() {
	return preencheDetalharLiberarPagto(TELA_DETALHAR);
    }

    /**
     * Voltar detalhe.
     *
     * @return the string
     */
    public String voltarDetalhe() {
	setItemSelecionadoGridDetalharLiberarPagto(null);

	return TELA_DETALHAR;
    }

    /**
     * Preenche detalhar liberar pagto.
     *
     * @param retorno the retorno
     * @return the string
     */
    private String preencheDetalharLiberarPagto(String retorno) {
	setListaGridDetalharLiberarPagto(new ArrayList<OcorrenciasDetLibPagConsSemConsSalSaidaDTO>());
	setItemSelecionadoGridDetalharLiberarPagto(null);
	Long qtdeTotalPagamentos = new Long(0L);
	BigDecimal valorTotalPagamentos = new BigDecimal(0);
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    itemSelecionadoGridLiberarPagto);
	    qtdeTotalPagamentos = new Long(getListaGridLiberarPagto().get(itemSelecionadoGridLiberarPagto).getQtPagamento());
	    valorTotalPagamentos = getListaGridLiberarPagto().get(itemSelecionadoGridLiberarPagto).getVlEfetivoPagamentoCliente();
	    setDsTipoServico(registroSelecionado.getDsResumoProdutoServico());
	    setDsIndicadorModalidade(registroSelecionado.getDsOperacaoProdutoServico());

	    DetalharLibPagtosConsSemConsultaSaldoEntradaDTO entradaDTO = new DetalharLibPagtosConsSemConsultaSaldoEntradaDTO();
	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setNrCnpjCpf(registroSelecionado.getNrCnpjCpf());
	    entradaDTO.setNrFilialCnpjCpf(registroSelecionado.getNrFilialCnpjCpf());
	    entradaDTO.setNrControleCnpjCpf(registroSelecionado.getNrDigitoCnpjCpf());
	    entradaDTO.setNrArquivoRemessaPagamento(registroSelecionado.getNrArquivoRemessaPagamento());
	    entradaDTO.setDtCreditoPagamento(registroSelecionado.getDtCreditoPagamento());
	    entradaDTO.setCdBanco(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgencia(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdConta(registroSelecionado.getCdConta());
	    entradaDTO.setCdDigitoConta(registroSelecionado.getCdDigitoConta());
	    entradaDTO.setCdProdutoServicoOperacao(registroSelecionado.getCdProdutoServicoOperacao());
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setCdSituacaoOperacaoPagamento(registroSelecionado.getCdSituacaoOperacaoPagamento());
	    entradaDTO.setCdAgendadoPagaoNaoPago((getAgendadosPagosNaoPagos() == null
		    || "".equals(getAgendadosPagosNaoPagos()) ? 0 : Integer
			    .valueOf(getAgendadosPagosNaoPagos())));
	    entradaDTO.setCdPessoaContratoDebito(registroSelecionado.getCdPessoaContratoDebito());
	    entradaDTO.setCdTipoContratoDebito(registroSelecionado.getCdTipoContratoDebito());
	    entradaDTO.setNrSequenciaContratoDebito(registroSelecionado.getNrSequenciaContratoDebito());

	    DetalharLibPagtosConsSemConsultaSaldoSaidaDTO saidaDTO = 
		getLiberarPagConsolidadoSemConsultSaldoImpl().detalharLibPagtosConsSemConsultaSaldo(entradaDTO);
	    setListaGridDetalharLiberarPagto(saidaDTO.getListaDetalharLibPagtosConsSemConsultaSaldo());

	    // in�cio do preenchimento do cabecalho
	    setNrCnpjCpf(CpfCnpjUtils.formatCpfCnpjCompleto(registroSelecionado.getNrCnpjCpf(), registroSelecionado
		    .getNrFilialCnpjCpf(), registroSelecionado.getNrDigitoCnpjCpf()));
	    setDsEmpresa(registroSelecionado.getDsRazaoSocial());
	    setNroContrato(Long.toString(registroSelecionado.getNrContratoOrigem()));

	    setDsRazaoSocial(saidaDTO.getNmCliente());
	    setDsContrato(saidaDTO.getDsContrato());
	    setCdSituacaoContrato(saidaDTO.getDsSituacaoContrato());

	    setDsBancoDebito(PgitUtil.concatenarCampos(registroSelecionado.getCdBanco(), saidaDTO.getDsBancoDebito()));
	    setDsAgenciaDebito(PgitUtil.formatAgencia(registroSelecionado.getCdAgencia(), registroSelecionado
		    .getCdDigitoAgencia(), saidaDTO.getDsAgenciaDebito(), false));
	    setContaDebito(PgitUtil.formatConta(registroSelecionado.getCdConta(), registroSelecionado
		    .getCdDigitoConta(), false));
	    setTipoContaDebito(saidaDTO.getDsTipoContaDebito());
	    // fim do preenchimento do cabecalho

	    setListaControleDetalharLiberarPagto(new ArrayList<SelectItem>());
	    
	    for (int i = 0; i < getListaGridDetalharLiberarPagto().size(); i++) {
		getListaControleDetalharLiberarPagto().add(new SelectItem(i, ""));
//		setVlTotalPagamentos(new BigDecimal(getVlTotalPagamentos().doubleValue()
//			+ getListaGridDetalharLiberarPagto().get(i).getVlPagamento().doubleValue()));
	    }
	    
	    setQtdePagamentos(new Long(qtdeTotalPagamentos));
	    setVlTotalPagamentos(valorTotalPagamentos);
	    setNrRemessa(registroSelecionado.getNrArquivoRemessaPagamento());
	    setDataPagamento(FormatarData.formatarDataFromPdc(registroSelecionado.getDtCreditoPagamento()));

	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	    setListaControleDetalharLiberarPagto(null);
	    // Se der algum erro n�o avan�a, permanece na mesma tela
	    return "";
	}
	return retorno;
    }

    /**
     * Confirmar liberar integral.
     */
    public void confirmarLiberarIntegral() {
	try {
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());

	    LiberarIntegralEntradaDTO entradaDTO = new LiberarIntegralEntradaDTO();

	    // Preenchidos com informa��es vindas da tela Liberar Pagamentos
	    // Consolidados
	    entradaDTO.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
	    entradaDTO.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
	    entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
	    entradaDTO.setCdProdutoServicoOperacao(registroSelecionado.getCdProdutoServicoOperacao());
	    entradaDTO.setCdProdutoServicoRelacionado(registroSelecionado.getCdProdutoServicoRelacionado());
	    entradaDTO.setCdCnpjCpf(registroSelecionado.getNrCnpjCpf());
	    entradaDTO.setCdFilialCnpjCpf(registroSelecionado.getNrFilialCnpjCpf());
	    entradaDTO.setCdControleCnpjCpf(registroSelecionado.getNrDigitoCnpjCpf());
	    entradaDTO.setNrArquivoRemessa(registroSelecionado.getNrArquivoRemessaPagamento());
	    entradaDTO.setHrInclusaoRemessaSistema("");
	    entradaDTO.setDtAgendamentoPagamento(registroSelecionado.getDtCreditoPagamento());
	    entradaDTO.setCdBancoDebito(registroSelecionado.getCdBanco());
	    entradaDTO.setCdAgenciaDebito(registroSelecionado.getCdAgencia());
	    entradaDTO.setCdContaDebito(registroSelecionado.getCdConta());
	    entradaDTO.setCdSituacao(registroSelecionado.getCdSituacaoOperacaoPagamento());

	    LiberarIntegralSaidaDTO saida = getLiberarPagConsolidadoSemConsultSaldoImpl().liberarIntegral(entradaDTO);
	    BradescoFacesUtils.addInfoModalMessage("(" + saida.getCdMensagem() + ") " + saida.getMensagem(),
		    "conLiberarPagtosConsolidadosSemConSaldo",
		    "#{liberarPagConsolidadoSemConsultSaldoBean.consultar}", false);
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    /**
     * Confirmar liberar parcial.
     */
    public void confirmarLiberarParcial() {
	try {
	    List<LiberarParcialEntradaDTO> listaOcorrenciasLiberarParcial = new ArrayList<LiberarParcialEntradaDTO>();
	    ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO registroSelecionado = getListaGridLiberarPagto().get(
		    getItemSelecionadoGridLiberarPagto());

	    for (int i = 0; i < (getListaGridDetalharLiberarPagtoSelecionados().size()); i++) {
		LiberarParcialEntradaDTO ocorrencia = new LiberarParcialEntradaDTO();

		// Preenchidos com informa��es vindas da tela Liberar Pagamentos
		// Consolidados
		ocorrencia.setCdPessoaJuridicaContrato(registroSelecionado.getCdPessoaJuridicaContrato());
		ocorrencia.setCdTipoContratoNegocio(registroSelecionado.getCdTipoContratoNegocio());
		ocorrencia.setNrSequenciaContratoNegocio(registroSelecionado.getNrContratoOrigem());
		ocorrencia.setCdProdutoServicoOperacao(registroSelecionado.getCdProdutoServicoOperacao());
		ocorrencia.setCdOperacaoProdutoServico(registroSelecionado.getCdProdutoServicoRelacionado());
		// Preenchidos com informa��es vindas da tela Liberar Pagamentos
		// Consolidados Parcial
		ocorrencia.setCdTipoCanal(getListaGridDetalharLiberarPagtoSelecionados().get(i).getCdTipoCanal());
		ocorrencia.setCdControlePagamento(getListaGridDetalharLiberarPagtoSelecionados().get(i)
			.getNrPagamento());

		listaOcorrenciasLiberarParcial.add(ocorrencia);
	    }

	    LiberarParcialEntradaDTO entradaDTO = new LiberarParcialEntradaDTO();
	    entradaDTO.setListaLiberarParcial(listaOcorrenciasLiberarParcial);

	    LiberarParcialSaidaDTO saida = getLiberarPagConsolidadoSemConsultSaldoImpl().liberarParcial(entradaDTO);
	    BradescoFacesUtils.addInfoModalMessage("(" + saida.getCdMensagem() + ") " + saida.getMensagem(),
		    "conLiberarPagtosConsolidadosSemConSaldo",
		    "#{liberarPagConsolidadoSemConsultSaldoBean.consultar}", false);
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    /**
     * Abrir confirmar liberar parcial.
     *
     * @return the string
     */
    public String abrirConfirmarLiberarParcial() {
	carregaListaSelecionados();
	return TELA_CONFIRMAR_LIBERAR_PARCIAL;
    }

    /**
     * Voltar liberar parcial.
     *
     * @return the string
     */
    public String voltarLiberarParcial() {
	setOpcaoChecarTodos(false);

	for (int i = 0; i < getListaGridDetalharLiberarPagto().size(); i++) {
	    getListaGridDetalharLiberarPagto().get(i).setCheck(false);
	}

	setPanelBotoes(false);

	return TELA_LIBERAR_PARCIAL;
    }

    /**
     * Liberar parcial.
     *
     * @return the string
     */
    public String liberarParcial() {
	setOpcaoChecarTodos(false);
	setPanelBotoes(false);
	return preencheDetalharLiberarPagto(TELA_LIBERAR_PARCIAL);
    }

    /**
     * Liberar integral.
     *
     * @return the string
     */
    public String liberarIntegral() {
	return preencheDetalharLiberarPagto(TELA_LIBERAR_INTEGRAL);
    }

    /**
     * Detalhar selecionado.
     *
     * @return the string
     */
    public String detalharSelecionado() {
	try {
	    tipoTela = listaGridDetalharLiberarPagto.get(getItemSelecionadoGridDetalharLiberarPagto()).getCdTipoTela();

	    switch (tipoTela) {
	    case 1:
		preencheDadosPagtoCreditoConta();
		return "DETALHE_CREDITO_CONTA";
	    case 2:
		preencheDadosPagtoDebitoConta();
		return "DETALHE_DEBITO_CONTA";
	    case 3:
		preencheDadosPagtoDOC();
		return "DETALHE_DOC";
	    case 4:
		preencheDadosPagtoTED();
		return "DETALHE_TED";
	    case 5:
		preencheDadosPagtoOrdemPagto();
		return "DETALHE_ORDEM_PAGTO";
	    case 6:
		preencheDadosPagtoTituloBradesco();
		return "DETALHE_TITULOS_BRADESCO";
	    case 7:
		preencheDadosPagtoTituloOutrosBancos();
		return "DETALHE_TITULOS_OUTROS_BANCOS";
	    case 8:
		preencheDadosPagtoDARF();
		return "DETALHE_DARF";
	    case 9:
		preencheDadosPagtoGARE();
		return "DETALHE_GARE";
	    case 10:
		preencheDadosPagtoGPS();
		return "DETALHE_GPS";
	    case 11:
		preencheDadosPagtoCodigoBarras();
		return "DETALHE_CODIGO_BARRAS";
	    case 12:
		preencheDadosPagtoDebitoVeiculos();
		return "DETALHE_DEBITO_VEICULOS";
	    case 13:
		preencheDadosPagtoDepIdentificado();
		return "DETALHE_DEPOSITO_IDENTIFICADO";
	    case 14:
		preencheDadosPagtoOrdemCredito();
		return "DETALHE_ORDEM_CREDITO";
	    default:
		return "";
	    }
	} catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	    return "";
	}
    }

    /**
     * Voltar consultar.
     *
     * @return the string
     */
    public String voltarConsultar() {
	setItemSelecionadoGridLiberarPagto(null);

	return TELA_CONSULTAR;
    }

    /**
     * Voltar.
     *
     * @return the string
     */
    public String voltar() {
	setItemSelecionadoGridDetalharLiberarPagto(null);
	setListaGridDetalharLiberarPagto(null);
	return TELA_CONSULTAR;
    }

    // In�cio Relat�rio
    /** Atributo logoPath. */
    private String logoPath;

    /**
     * Imprimir relatorio integral.
     */
    public void imprimirRelatorioIntegral() {

	try {

	    String caminho = "/images/Bradesco_logo.JPG";
	    FacesContext facesContext = FacesContext.getCurrentInstance();
	    ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
	    logoPath = servletContext.getRealPath(caminho);

	    carregaListaSelecionados();

	    // Par�metros do Relat�rio
	    Map<String, Object> par = new HashMap<String, Object>();
	    par.put("titulo", "Liberar Pagamentos Consolidados Sem Consulta de Saldo");

	    par.put("cpfCnpj", getNrCnpjCpf());
	    par.put("nomeRazaoSocial", getDsRazaoSocial());
	    par.put("empresaConglomerado", getDsEmpresa());
	    par.put("nrContrato", getNroContrato());
	    par.put("dsContratacao", getDsContrato());
	    par.put("situacao", getCdSituacaoContrato());
	    par.put("banco", getDsBancoDebito());
	    par.put("agencia", getDsAgenciaDebito());
	    par.put("conta", getContaDebito());
	    par.put("tipo", getTipoContaDebito());
	    par.put("logoBradesco", getLogoPath());

	    par.put("qtdePagamentos", getQtdePagamentos());
	    par.put("vlTotalPagamentos", getVlTotalPagamentos());
	    try {

		// M�todo que gera o relat�rio PDF.
		PgitUtil
		.geraRelatorioPdf(
			"/relatorios/agendamentoEfetivacaoEstorno/liberarPagtoSemConSaldos/liberarPagtoConsolidadosSemConSaldos/integral",
			"imprimirLiberarPagtosConsSemConSaldoIntegral",
			getListaGridDetalharLiberarPagto(), par);

	    } /* Exce��o do relat�rio */
	    catch (Exception e) {
		throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
	    }

	} /* Exce��o do PDC */
	catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    /**
     * Imprimir relatorio parcial.
     */
    public void imprimirRelatorioParcial() {

	try {

	    String caminho = "/images/Bradesco_logo.JPG";
	    FacesContext facesContext = FacesContext.getCurrentInstance();
	    ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
	    logoPath = servletContext.getRealPath(caminho);

	    carregaListaSelecionados();

	    // Par�metros do Relat�rio
	    Map<String, Object> par = new HashMap<String, Object>();
	    par.put("titulo", "Liberar Pagamentos Consolidados Sem Consulta de Saldo");

	    par.put("cpfCnpj", getNrCnpjCpf());
	    par.put("nomeRazaoSocial", getDsRazaoSocial());
	    par.put("empresaConglomerado", getDsEmpresa());
	    par.put("nrContrato", getNroContrato());
	    par.put("dsContratacao", getDsContrato());
	    par.put("situacao", getCdSituacaoContrato());
	    par.put("banco", getDsBancoDebito());
	    par.put("agencia", getDsAgenciaDebito());
	    par.put("conta", getContaDebito());
	    par.put("tipo", getTipoContaDebito());
	    par.put("logoBradesco", getLogoPath());

	    par.put("dsIndicadorModalidade", getDsIndicadorModalidade());
	    par.put("dsTipoServico", getDsTipoServico());
	    par.put("qtdePagamentos", getQtdePagamentos());
	    par.put("vlTotalPagamentos", getVlTotalPagamentos());
	    par.put("qtdePagamentosAutorizados", getQtdePagamentosAutorizados());
	    par.put("vlTotalPagamentosAutorizados", getVlTotalPagamentosAutorizados());
	    try {

		// M�todo que gera o relat�rio PDF.
		PgitUtil
		.geraRelatorioPdf(
			"/relatorios/agendamentoEfetivacaoEstorno/liberarPagtoSemConSaldos/liberarPagtoConsolidadosSemConSaldos/parcial",
			"imprimirLiberarPagtosConsSemConSaldoParcial",
			getListaGridDetalharLiberarPagtoSelecionados(), par);

	    } /* Exce��o do relat�rio */
	    catch (Exception e) {
		throw new BradescoViewException(e.getMessage(), e, "", "", BradescoViewExceptionActionType.ACTION);
	    }

	} /* Exce��o do PDC */
	catch (PdcAdapterFunctionalException p) {
	    BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") " + p.getMessage(),
		    false);
	}
    }

    /**
     * Habilitar panel botoes.
     */
    public void habilitarPanelBotoes() {
	for (int i = 0; i < getListaGridDetalharLiberarPagto().size(); i++) {
	    if (getListaGridDetalharLiberarPagto().get(i).isCheck()) {
		setPanelBotoes(true);
		return;
	    }
	}
	setPanelBotoes(false);
    }

    /** * GETTERS E SETTERS ** */

    public String getAgenciaDigitoDestino() {
	return agenciaDigitoDestino;
    }

    /**
     * Set: agenciaDigitoDestino.
     *
     * @param agenciaDigitoDestino the agencia digito destino
     */
    public void setAgenciaDigitoDestino(String agenciaDigitoDestino) {
	this.agenciaDigitoDestino = agenciaDigitoDestino;
    }

    /**
     * Get: cdBancoDestino.
     *
     * @return cdBancoDestino
     */
    public String getCdBancoDestino() {
	return cdBancoDestino;
    }

    /**
     * Set: cdBancoDestino.
     *
     * @param cdBancoDestino the cd banco destino
     */
    public void setCdBancoDestino(String cdBancoDestino) {
	this.cdBancoDestino = cdBancoDestino;
    }

    /**
     * Get: cdFavorecido.
     *
     * @return cdFavorecido
     */
    public String getCdFavorecido() {
	return cdFavorecido;
    }

    /**
     * Set: cdFavorecido.
     *
     * @param cdFavorecido the cd favorecido
     */
    public void setCdFavorecido(String cdFavorecido) {
	this.cdFavorecido = cdFavorecido;
    }

    /**
     * Get: cdListaDebito.
     *
     * @return cdListaDebito
     */
    public String getCdListaDebito() {
	return cdListaDebito;
    }

    /**
     * Set: cdListaDebito.
     *
     * @param cdListaDebito the cd lista debito
     */
    public void setCdListaDebito(String cdListaDebito) {
	this.cdListaDebito = cdListaDebito;
    }

    /**
     * Get: cdSerieDocumento.
     *
     * @return cdSerieDocumento
     */
    public String getCdSerieDocumento() {
	return cdSerieDocumento;
    }

    /**
     * Set: cdSerieDocumento.
     *
     * @param cdSerieDocumento the cd serie documento
     */
    public void setCdSerieDocumento(String cdSerieDocumento) {
	this.cdSerieDocumento = cdSerieDocumento;
    }

    /**
     * Get: cdTipoBeneficiario.
     *
     * @return cdTipoBeneficiario
     */
    public Integer getCdTipoBeneficiario() {
	return cdTipoBeneficiario;
    }

    /**
     * Set: cdTipoBeneficiario.
     *
     * @param cdTipoBeneficiario the cd tipo beneficiario
     */
    public void setCdTipoBeneficiario(Integer cdTipoBeneficiario) {
	this.cdTipoBeneficiario = cdTipoBeneficiario;
    }

    /**
     * Get: complementoInclusao.
     *
     * @return complementoInclusao
     */
    public String getComplementoInclusao() {
	return complementoInclusao;
    }

    /**
     * Set: complementoInclusao.
     *
     * @param complementoInclusao the complemento inclusao
     */
    public void setComplementoInclusao(String complementoInclusao) {
	this.complementoInclusao = complementoInclusao;
    }

    /**
     * Get: complementoManutencao.
     *
     * @return complementoManutencao
     */
    public String getComplementoManutencao() {
	return complementoManutencao;
    }

    /**
     * Set: complementoManutencao.
     *
     * @param complementoManutencao the complemento manutencao
     */
    public void setComplementoManutencao(String complementoManutencao) {
	this.complementoManutencao = complementoManutencao;
    }

    /**
     * Get: contaDigitoCredito.
     *
     * @return contaDigitoCredito
     */
    public String getContaDigitoCredito() {
	return contaDigitoCredito;
    }

    /**
     * Set: contaDigitoCredito.
     *
     * @param contaDigitoCredito the conta digito credito
     */
    public void setContaDigitoCredito(String contaDigitoCredito) {
	this.contaDigitoCredito = contaDigitoCredito;
    }

    /**
     * Get: contaDigitoDestino.
     *
     * @return contaDigitoDestino
     */
    public String getContaDigitoDestino() {
	return contaDigitoDestino;
    }

    /**
     * Set: contaDigitoDestino.
     *
     * @param contaDigitoDestino the conta digito destino
     */
    public void setContaDigitoDestino(String contaDigitoDestino) {
	this.contaDigitoDestino = contaDigitoDestino;
    }

    /**
     * Get: dataHoraInclusao.
     *
     * @return dataHoraInclusao
     */
    public String getDataHoraInclusao() {
	return dataHoraInclusao;
    }

    /**
     * Set: dataHoraInclusao.
     *
     * @param dataHoraInclusao the data hora inclusao
     */
    public void setDataHoraInclusao(String dataHoraInclusao) {
	this.dataHoraInclusao = dataHoraInclusao;
    }

    /**
     * Get: dataHoraManutencao.
     *
     * @return dataHoraManutencao
     */
    public String getDataHoraManutencao() {
	return dataHoraManutencao;
    }

    /**
     * Set: dataHoraManutencao.
     *
     * @param dataHoraManutencao the data hora manutencao
     */
    public void setDataHoraManutencao(String dataHoraManutencao) {
	this.dataHoraManutencao = dataHoraManutencao;
    }

    /**
     * Get: dsAgenciaFavorecido.
     *
     * @return dsAgenciaFavorecido
     */
    public String getDsAgenciaFavorecido() {
	return dsAgenciaFavorecido;
    }

    /**
     * Set: dsAgenciaFavorecido.
     *
     * @param dsAgenciaFavorecido the ds agencia favorecido
     */
    public void setDsAgenciaFavorecido(String dsAgenciaFavorecido) {
	this.dsAgenciaFavorecido = dsAgenciaFavorecido;
    }

    /**
     * Get: dsBancoFavorecido.
     *
     * @return dsBancoFavorecido
     */
    public String getDsBancoFavorecido() {
	return dsBancoFavorecido;
    }

    /**
     * Set: dsBancoFavorecido.
     *
     * @param dsBancoFavorecido the ds banco favorecido
     */
    public void setDsBancoFavorecido(String dsBancoFavorecido) {
	this.dsBancoFavorecido = dsBancoFavorecido;
    }

    /**
     * Get: dsFavorecido.
     *
     * @return dsFavorecido
     */
    public String getDsFavorecido() {
	return dsFavorecido;
    }

    /**
     * Set: dsFavorecido.
     *
     * @param dsFavorecido the ds favorecido
     */
    public void setDsFavorecido(String dsFavorecido) {
	this.dsFavorecido = dsFavorecido;
    }

    /**
     * Get: dsIndicadorModalidade.
     *
     * @return dsIndicadorModalidade
     */
    public String getDsIndicadorModalidade() {
	return dsIndicadorModalidade;
    }

    /**
     * Set: dsIndicadorModalidade.
     *
     * @param dsIndicadorModalidade the ds indicador modalidade
     */
    public void setDsIndicadorModalidade(String dsIndicadorModalidade) {
	this.dsIndicadorModalidade = dsIndicadorModalidade;
    }

    /**
     * Get: dsMensagemPrimeiraLinha.
     *
     * @return dsMensagemPrimeiraLinha
     */
    public String getDsMensagemPrimeiraLinha() {
	return dsMensagemPrimeiraLinha;
    }

    /**
     * Set: dsMensagemPrimeiraLinha.
     *
     * @param dsMensagemPrimeiraLinha the ds mensagem primeira linha
     */
    public void setDsMensagemPrimeiraLinha(String dsMensagemPrimeiraLinha) {
	this.dsMensagemPrimeiraLinha = dsMensagemPrimeiraLinha;
    }

    /**
     * Get: dsMensagemSegundaLinha.
     *
     * @return dsMensagemSegundaLinha
     */
    public String getDsMensagemSegundaLinha() {
	return dsMensagemSegundaLinha;
    }

    /**
     * Set: dsMensagemSegundaLinha.
     *
     * @param dsMensagemSegundaLinha the ds mensagem segunda linha
     */
    public void setDsMensagemSegundaLinha(String dsMensagemSegundaLinha) {
	this.dsMensagemSegundaLinha = dsMensagemSegundaLinha;
    }

    /**
     * Get: dsMotivoSituacao.
     *
     * @return dsMotivoSituacao
     */
    public String getDsMotivoSituacao() {
	return dsMotivoSituacao;
    }

    /**
     * Set: dsMotivoSituacao.
     *
     * @param dsMotivoSituacao the ds motivo situacao
     */
    public void setDsMotivoSituacao(String dsMotivoSituacao) {
	this.dsMotivoSituacao = dsMotivoSituacao;
    }

    /**
     * Get: dsPagamento.
     *
     * @return dsPagamento
     */
    public String getDsPagamento() {
	return dsPagamento;
    }

    /**
     * Set: dsPagamento.
     *
     * @param dsPagamento the ds pagamento
     */
    public void setDsPagamento(String dsPagamento) {
	this.dsPagamento = dsPagamento;
    }

    /**
     * Get: dsTipoContaFavorecido.
     *
     * @return dsTipoContaFavorecido
     */
    public String getDsTipoContaFavorecido() {
	return dsTipoContaFavorecido;
    }

    /**
     * Set: dsTipoContaFavorecido.
     *
     * @param dsTipoContaFavorecido the ds tipo conta favorecido
     */
    public void setDsTipoContaFavorecido(String dsTipoContaFavorecido) {
	this.dsTipoContaFavorecido = dsTipoContaFavorecido;
    }

    /**
     * Get: dsTipoServico.
     *
     * @return dsTipoServico
     */
    public String getDsTipoServico() {
	return dsTipoServico;
    }

    /**
     * Set: dsTipoServico.
     *
     * @param dsTipoServico the ds tipo servico
     */
    public void setDsTipoServico(String dsTipoServico) {
	this.dsTipoServico = dsTipoServico;
    }

    /**
     * Get: dsUsoEmpresa.
     *
     * @return dsUsoEmpresa
     */
    public String getDsUsoEmpresa() {
	return dsUsoEmpresa;
    }

    /**
     * Set: dsUsoEmpresa.
     *
     * @param dsUsoEmpresa the ds uso empresa
     */
    public void setDsUsoEmpresa(String dsUsoEmpresa) {
	this.dsUsoEmpresa = dsUsoEmpresa;
    }

    /**
     * Get: dtAgendamento.
     *
     * @return dtAgendamento
     */
    public String getDtAgendamento() {
	return dtAgendamento;
    }

    /**
     * Set: dtAgendamento.
     *
     * @param dtAgendamento the dt agendamento
     */
    public void setDtAgendamento(String dtAgendamento) {
	this.dtAgendamento = dtAgendamento;
    }

    /**
     * Get: dtEmissaoDocumento.
     *
     * @return dtEmissaoDocumento
     */
    public String getDtEmissaoDocumento() {
	return dtEmissaoDocumento;
    }

    /**
     * Set: dtEmissaoDocumento.
     *
     * @param dtEmissaoDocumento the dt emissao documento
     */
    public void setDtEmissaoDocumento(String dtEmissaoDocumento) {
	this.dtEmissaoDocumento = dtEmissaoDocumento;
    }

    /**
     * Get: dtNascimentoBeneficiario.
     *
     * @return dtNascimentoBeneficiario
     */
    public String getDtNascimentoBeneficiario() {
	return dtNascimentoBeneficiario;
    }

    /**
     * Set: dtNascimentoBeneficiario.
     *
     * @param dtNascimentoBeneficiario the dt nascimento beneficiario
     */
    public void setDtNascimentoBeneficiario(String dtNascimentoBeneficiario) {
	this.dtNascimentoBeneficiario = dtNascimentoBeneficiario;
    }

    /**
     * Get: dtPagamento.
     *
     * @return dtPagamento
     */
    public String getDtPagamento() {
	return dtPagamento;
    }

    /**
     * Set: dtPagamento.
     *
     * @param dtPagamento the dt pagamento
     */
    public void setDtPagamento(String dtPagamento) {
	this.dtPagamento = dtPagamento;
    }

    /**
     * Get: dtVencimento.
     *
     * @return dtVencimento
     */
    public String getDtVencimento() {
	return dtVencimento;
    }

    /**
     * Set: dtVencimento.
     *
     * @param dtVencimento the dt vencimento
     */
    public void setDtVencimento(String dtVencimento) {
	this.dtVencimento = dtVencimento;
    }

    /**
     * Get: nomeBeneficiario.
     *
     * @return nomeBeneficiario
     */
    public String getNomeBeneficiario() {
	return nomeBeneficiario;
    }

    /**
     * Set: nomeBeneficiario.
     *
     * @param nomeBeneficiario the nome beneficiario
     */
    public void setNomeBeneficiario(String nomeBeneficiario) {
	this.nomeBeneficiario = nomeBeneficiario;
    }

    /**
     * Get: nrDocumento.
     *
     * @return nrDocumento
     */
    public String getNrDocumento() {
	return nrDocumento;
    }

    /**
     * Set: nrDocumento.
     *
     * @param nrDocumento the nr documento
     */
    public void setNrDocumento(String nrDocumento) {
	this.nrDocumento = nrDocumento;
    }

    /**
     * Get: nrLoteArquivoRemessa.
     *
     * @return nrLoteArquivoRemessa
     */
    public String getNrLoteArquivoRemessa() {
	return nrLoteArquivoRemessa;
    }

    /**
     * Set: nrLoteArquivoRemessa.
     *
     * @param nrLoteArquivoRemessa the nr lote arquivo remessa
     */
    public void setNrLoteArquivoRemessa(String nrLoteArquivoRemessa) {
	this.nrLoteArquivoRemessa = nrLoteArquivoRemessa;
    }

    /**
     * Get: nrPagamento.
     *
     * @return nrPagamento
     */
    public String getNrPagamento() {
	return nrPagamento;
    }

    /**
     * Set: nrPagamento.
     *
     * @param nrPagamento the nr pagamento
     */
    public void setNrPagamento(String nrPagamento) {
	this.nrPagamento = nrPagamento;
    }

    /**
     * Get: nrSequenciaArquivoRemessa.
     *
     * @return nrSequenciaArquivoRemessa
     */
    public String getNrSequenciaArquivoRemessa() {
	return nrSequenciaArquivoRemessa;
    }

    /**
     * Set: nrSequenciaArquivoRemessa.
     *
     * @param nrSequenciaArquivoRemessa the nr sequencia arquivo remessa
     */
    public void setNrSequenciaArquivoRemessa(String nrSequenciaArquivoRemessa) {
	this.nrSequenciaArquivoRemessa = nrSequenciaArquivoRemessa;
    }

    /**
     * Get: numeroInscricaoBeneficiario.
     *
     * @return numeroInscricaoBeneficiario
     */
    public String getNumeroInscricaoBeneficiario() {
	return numeroInscricaoBeneficiario;
    }

    /**
     * Set: numeroInscricaoBeneficiario.
     *
     * @param numeroInscricaoBeneficiario the numero inscricao beneficiario
     */
    public void setNumeroInscricaoBeneficiario(String numeroInscricaoBeneficiario) {
	this.numeroInscricaoBeneficiario = numeroInscricaoBeneficiario;
    }

    /**
     * Get: numeroInscricaoFavorecido.
     *
     * @return numeroInscricaoFavorecido
     */
    public String getNumeroInscricaoFavorecido() {
	return numeroInscricaoFavorecido;
    }

    /**
     * Set: numeroInscricaoFavorecido.
     *
     * @param numeroInscricaoFavorecido the numero inscricao favorecido
     */
    public void setNumeroInscricaoFavorecido(String numeroInscricaoFavorecido) {
	this.numeroInscricaoFavorecido = numeroInscricaoFavorecido;
    }

    /**
     * Get: qtMoeda.
     *
     * @return qtMoeda
     */
    public BigDecimal getQtMoeda() {
	return qtMoeda;
    }

    /**
     * Set: qtMoeda.
     *
     * @param qtMoeda the qt moeda
     */
    public void setQtMoeda(BigDecimal qtMoeda) {
	this.qtMoeda = qtMoeda;
    }

    /**
     * Get: tipoCanalInclusao.
     *
     * @return tipoCanalInclusao
     */
    public String getTipoCanalInclusao() {
	return tipoCanalInclusao;
    }

    /**
     * Set: tipoCanalInclusao.
     *
     * @param tipoCanalInclusao the tipo canal inclusao
     */
    public void setTipoCanalInclusao(String tipoCanalInclusao) {
	this.tipoCanalInclusao = tipoCanalInclusao;
    }

    /**
     * Get: tipoCanalManutencao.
     *
     * @return tipoCanalManutencao
     */
    public String getTipoCanalManutencao() {
	return tipoCanalManutencao;
    }

    /**
     * Set: tipoCanalManutencao.
     *
     * @param tipoCanalManutencao the tipo canal manutencao
     */
    public void setTipoCanalManutencao(String tipoCanalManutencao) {
	this.tipoCanalManutencao = tipoCanalManutencao;
    }

    /**
     * Get: tipoContaCredito.
     *
     * @return tipoContaCredito
     */
    public String getTipoContaCredito() {
	return tipoContaCredito;
    }

    /**
     * Set: tipoContaCredito.
     *
     * @param tipoContaCredito the tipo conta credito
     */
    public void setTipoContaCredito(String tipoContaCredito) {
	this.tipoContaCredito = tipoContaCredito;
    }

    /**
     * Get: tipoContaDestino.
     *
     * @return tipoContaDestino
     */
    public String getTipoContaDestino() {
	return tipoContaDestino;
    }

    /**
     * Set: tipoContaDestino.
     *
     * @param tipoContaDestino the tipo conta destino
     */
    public void setTipoContaDestino(String tipoContaDestino) {
	this.tipoContaDestino = tipoContaDestino;
    }

    /**
     * Get: tipoDocumento.
     *
     * @return tipoDocumento
     */
    public String getTipoDocumento() {
	return tipoDocumento;
    }

    /**
     * Set: tipoDocumento.
     *
     * @param tipoDocumento the tipo documento
     */
    public void setTipoDocumento(String tipoDocumento) {
	this.tipoDocumento = tipoDocumento;
    }

    /**
     * Get: tipoFavorecido.
     *
     * @return tipoFavorecido
     */
    public Integer getTipoFavorecido() {
	return tipoFavorecido;
    }

    /**
     * Set: tipoFavorecido.
     *
     * @param tipoFavorecido the tipo favorecido
     */
    public void setTipoFavorecido(Integer tipoFavorecido) {
	this.tipoFavorecido = tipoFavorecido;
    }

    /**
     * Get: usuarioInclusao.
     *
     * @return usuarioInclusao
     */
    public String getUsuarioInclusao() {
	return usuarioInclusao;
    }

    /**
     * Set: usuarioInclusao.
     *
     * @param usuarioInclusao the usuario inclusao
     */
    public void setUsuarioInclusao(String usuarioInclusao) {
	this.usuarioInclusao = usuarioInclusao;
    }

    /**
     * Get: usuarioManutencao.
     *
     * @return usuarioManutencao
     */
    public String getUsuarioManutencao() {
	return usuarioManutencao;
    }

    /**
     * Set: usuarioManutencao.
     *
     * @param usuarioManutencao the usuario manutencao
     */
    public void setUsuarioManutencao(String usuarioManutencao) {
	this.usuarioManutencao = usuarioManutencao;
    }

    /**
     * Get: vlAbatimento.
     *
     * @return vlAbatimento
     */
    public BigDecimal getVlAbatimento() {
	return vlAbatimento;
    }

    /**
     * Set: vlAbatimento.
     *
     * @param vlAbatimento the vl abatimento
     */
    public void setVlAbatimento(BigDecimal vlAbatimento) {
	this.vlAbatimento = vlAbatimento;
    }

    /**
     * Get: vlAcrescimo.
     *
     * @return vlAcrescimo
     */
    public BigDecimal getVlAcrescimo() {
	return vlAcrescimo;
    }

    /**
     * Set: vlAcrescimo.
     *
     * @param vlAcrescimo the vl acrescimo
     */
    public void setVlAcrescimo(BigDecimal vlAcrescimo) {
	this.vlAcrescimo = vlAcrescimo;
    }

    /**
     * Get: vlDeducao.
     *
     * @return vlDeducao
     */
    public BigDecimal getVlDeducao() {
	return vlDeducao;
    }

    /**
     * Set: vlDeducao.
     *
     * @param vlDeducao the vl deducao
     */
    public void setVlDeducao(BigDecimal vlDeducao) {
	this.vlDeducao = vlDeducao;
    }

    /**
     * Get: vlDesconto.
     *
     * @return vlDesconto
     */
    public BigDecimal getVlDesconto() {
	return vlDesconto;
    }

    /**
     * Set: vlDesconto.
     *
     * @param vlDesconto the vl desconto
     */
    public void setVlDesconto(BigDecimal vlDesconto) {
	this.vlDesconto = vlDesconto;
    }

    /**
     * Get: vlDocumento.
     *
     * @return vlDocumento
     */
    public BigDecimal getVlDocumento() {
	return vlDocumento;
    }

    /**
     * Set: vlDocumento.
     *
     * @param vlDocumento the vl documento
     */
    public void setVlDocumento(BigDecimal vlDocumento) {
	this.vlDocumento = vlDocumento;
    }

    /**
     * Get: vlImpostoRenda.
     *
     * @return vlImpostoRenda
     */
    public BigDecimal getVlImpostoRenda() {
	return vlImpostoRenda;
    }

    /**
     * Set: vlImpostoRenda.
     *
     * @param vlImpostoRenda the vl imposto renda
     */
    public void setVlImpostoRenda(BigDecimal vlImpostoRenda) {
	this.vlImpostoRenda = vlImpostoRenda;
    }

    /**
     * Get: vlInss.
     *
     * @return vlInss
     */
    public BigDecimal getVlInss() {
	return vlInss;
    }

    /**
     * Set: vlInss.
     *
     * @param vlInss the vl inss
     */
    public void setVlInss(BigDecimal vlInss) {
	this.vlInss = vlInss;
    }

    /**
     * Get: vlIof.
     *
     * @return vlIof
     */
    public BigDecimal getVlIof() {
	return vlIof;
    }

    /**
     * Set: vlIof.
     *
     * @param vlIof the vl iof
     */
    public void setVlIof(BigDecimal vlIof) {
	this.vlIof = vlIof;
    }

    /**
     * Get: vlIss.
     *
     * @return vlIss
     */
    public BigDecimal getVlIss() {
	return vlIss;
    }

    /**
     * Set: vlIss.
     *
     * @param vlIss the vl iss
     */
    public void setVlIss(BigDecimal vlIss) {
	this.vlIss = vlIss;
    }

    /**
     * Get: vlMora.
     *
     * @return vlMora
     */
    public BigDecimal getVlMora() {
	return vlMora;
    }

    /**
     * Set: vlMora.
     *
     * @param vlMora the vl mora
     */
    public void setVlMora(BigDecimal vlMora) {
	this.vlMora = vlMora;
    }

    /**
     * Get: vlMulta.
     *
     * @return vlMulta
     */
    public BigDecimal getVlMulta() {
	return vlMulta;
    }

    /**
     * Set: vlMulta.
     *
     * @param vlMulta the vl multa
     */
    public void setVlMulta(BigDecimal vlMulta) {
	this.vlMulta = vlMulta;
    }

    /**
     * Get: vlrAgendamento.
     *
     * @return vlrAgendamento
     */
    public BigDecimal getVlrAgendamento() {
	return vlrAgendamento;
    }

    /**
     * Set: vlrAgendamento.
     *
     * @param vlrAgendamento the vlr agendamento
     */
    public void setVlrAgendamento(BigDecimal vlrAgendamento) {
	this.vlrAgendamento = vlrAgendamento;
    }

    /**
     * Get: vlrEfetivacao.
     *
     * @return vlrEfetivacao
     */
    public BigDecimal getVlrEfetivacao() {
	return vlrEfetivacao;
    }

    /**
     * Set: vlrEfetivacao.
     *
     * @param vlrEfetivacao the vlr efetivacao
     */
    public void setVlrEfetivacao(BigDecimal vlrEfetivacao) {
	this.vlrEfetivacao = vlrEfetivacao;
    }

    /**
     * Get: liberarPagConsolidadoSemConsultSaldoImpl.
     *
     * @return liberarPagConsolidadoSemConsultSaldoImpl
     */
    public ILiberarPagConsolidadoSemConsultSaldoService getLiberarPagConsolidadoSemConsultSaldoImpl() {
	return liberarPagConsolidadoSemConsultSaldoImpl;
    }

    /**
     * Set: liberarPagConsolidadoSemConsultSaldoImpl.
     *
     * @param liberarPagConsolidadoSemConsultSaldoImpl the liberar pag consolidado sem consult saldo impl
     */
    public void setLiberarPagConsolidadoSemConsultSaldoImpl(
	    ILiberarPagConsolidadoSemConsultSaldoService liberarPagConsolidadoSemConsultSaldoImpl) {
	this.liberarPagConsolidadoSemConsultSaldoImpl = liberarPagConsolidadoSemConsultSaldoImpl;
    }

    /**
     * Get: listaControleLiberarPagto.
     *
     * @return listaControleLiberarPagto
     */
    public List<SelectItem> getListaControleLiberarPagto() {
	return listaControleLiberarPagto;
    }

    /**
     * Set: listaControleLiberarPagto.
     *
     * @param listaControleLiberarPagto the lista controle liberar pagto
     */
    public void setListaControleLiberarPagto(List<SelectItem> listaControleLiberarPagto) {
	this.listaControleLiberarPagto = listaControleLiberarPagto;
    }

    /**
     * Get: listaGridLiberarPagto.
     *
     * @return listaGridLiberarPagto
     */
    public List<ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO> getListaGridLiberarPagto() {
	return listaGridLiberarPagto;
    }

    /**
     * Set: listaGridLiberarPagto.
     *
     * @param listaGridLiberarPagto the lista grid liberar pagto
     */
    public void setListaGridLiberarPagto(List<ConsultarLibPagtosConsSemConsultaSaldoSaidaDTO> listaGridLiberarPagto) {
	this.listaGridLiberarPagto = listaGridLiberarPagto;
    }

    /**
     * Get: itemSelecionadoGridLiberarPagto.
     *
     * @return itemSelecionadoGridLiberarPagto
     */
    public Integer getItemSelecionadoGridLiberarPagto() {
	return itemSelecionadoGridLiberarPagto;
    }

    /**
     * Set: itemSelecionadoGridLiberarPagto.
     *
     * @param itemSelecionadoGridLiberarPagto the item selecionado grid liberar pagto
     */
    public void setItemSelecionadoGridLiberarPagto(Integer itemSelecionadoGridLiberarPagto) {
	this.itemSelecionadoGridLiberarPagto = itemSelecionadoGridLiberarPagto;
    }

    /**
     * Get: listaGridDetalharLiberarPagto.
     *
     * @return listaGridDetalharLiberarPagto
     */
    public List<OcorrenciasDetLibPagConsSemConsSalSaidaDTO> getListaGridDetalharLiberarPagto() {
	return listaGridDetalharLiberarPagto;
    }

    /**
     * Set: listaGridDetalharLiberarPagto.
     *
     * @param listaGridDetalharLiberarPagto the lista grid detalhar liberar pagto
     */
    public void setListaGridDetalharLiberarPagto(
	    List<OcorrenciasDetLibPagConsSemConsSalSaidaDTO> listaGridDetalharLiberarPagto) {
	this.listaGridDetalharLiberarPagto = listaGridDetalharLiberarPagto;
    }

    /**
     * Get: itemSelecionadoGridDetalharLiberarPagto.
     *
     * @return itemSelecionadoGridDetalharLiberarPagto
     */
    public Integer getItemSelecionadoGridDetalharLiberarPagto() {
	return itemSelecionadoGridDetalharLiberarPagto;
    }

    /**
     * Set: itemSelecionadoGridDetalharLiberarPagto.
     *
     * @param itemSelecionadoGridDetalharLiberarPagto the item selecionado grid detalhar liberar pagto
     */
    public void setItemSelecionadoGridDetalharLiberarPagto(Integer itemSelecionadoGridDetalharLiberarPagto) {
	this.itemSelecionadoGridDetalharLiberarPagto = itemSelecionadoGridDetalharLiberarPagto;
    }

    /**
     * Get: listaControleDetalharLiberarPagto.
     *
     * @return listaControleDetalharLiberarPagto
     */
    public List<SelectItem> getListaControleDetalharLiberarPagto() {
	return listaControleDetalharLiberarPagto;
    }

    /**
     * Set: listaControleDetalharLiberarPagto.
     *
     * @param listaControleDetalharLiberarPagto the lista controle detalhar liberar pagto
     */
    public void setListaControleDetalharLiberarPagto(List<SelectItem> listaControleDetalharLiberarPagto) {
	this.listaControleDetalharLiberarPagto = listaControleDetalharLiberarPagto;
    }

    /**
     * Get: cdSituacaoContrato.
     *
     * @return cdSituacaoContrato
     */
    public String getCdSituacaoContrato() {
	return cdSituacaoContrato;
    }

    /**
     * Set: cdSituacaoContrato.
     *
     * @param cdSituacaoContrato the cd situacao contrato
     */
    public void setCdSituacaoContrato(String cdSituacaoContrato) {
	this.cdSituacaoContrato = cdSituacaoContrato;
    }

    /**
     * Get: contaDebito.
     *
     * @return contaDebito
     */
    public String getContaDebito() {
	return contaDebito;
    }

    /**
     * Set: contaDebito.
     *
     * @param contaDebito the conta debito
     */
    public void setContaDebito(String contaDebito) {
	this.contaDebito = contaDebito;
    }

    /**
     * Get: dsAgenciaDebito.
     *
     * @return dsAgenciaDebito
     */
    public String getDsAgenciaDebito() {
	return dsAgenciaDebito;
    }

    /**
     * Set: dsAgenciaDebito.
     *
     * @param dsAgenciaDebito the ds agencia debito
     */
    public void setDsAgenciaDebito(String dsAgenciaDebito) {
	this.dsAgenciaDebito = dsAgenciaDebito;
    }

    /**
     * Get: dsBancoDebito.
     *
     * @return dsBancoDebito
     */
    public String getDsBancoDebito() {
	return dsBancoDebito;
    }

    /**
     * Set: dsBancoDebito.
     *
     * @param dsBancoDebito the ds banco debito
     */
    public void setDsBancoDebito(String dsBancoDebito) {
	this.dsBancoDebito = dsBancoDebito;
    }

    /**
     * Get: dsContrato.
     *
     * @return dsContrato
     */
    public String getDsContrato() {
	return dsContrato;
    }

    /**
     * Set: dsContrato.
     *
     * @param dsContrato the ds contrato
     */
    public void setDsContrato(String dsContrato) {
	this.dsContrato = dsContrato;
    }

    /**
     * Get: dsEmpresa.
     *
     * @return dsEmpresa
     */
    public String getDsEmpresa() {
	return dsEmpresa;
    }

    /**
     * Set: dsEmpresa.
     *
     * @param dsEmpresa the ds empresa
     */
    public void setDsEmpresa(String dsEmpresa) {
	this.dsEmpresa = dsEmpresa;
    }

    /**
     * Get: dsRazaoSocial.
     *
     * @return dsRazaoSocial
     */
    public String getDsRazaoSocial() {
	return dsRazaoSocial;
    }

    /**
     * Set: dsRazaoSocial.
     *
     * @param dsRazaoSocial the ds razao social
     */
    public void setDsRazaoSocial(String dsRazaoSocial) {
	this.dsRazaoSocial = dsRazaoSocial;
    }

    /**
     * Get: nrCnpjCpf.
     *
     * @return nrCnpjCpf
     */
    public String getNrCnpjCpf() {
	return nrCnpjCpf;
    }

    /**
     * Set: nrCnpjCpf.
     *
     * @param nrCnpjCpf the nr cnpj cpf
     */
    public void setNrCnpjCpf(String nrCnpjCpf) {
	this.nrCnpjCpf = nrCnpjCpf;
    }

    /**
     * Get: nroContrato.
     *
     * @return nroContrato
     */
    public String getNroContrato() {
	return nroContrato;
    }

    /**
     * Set: nroContrato.
     *
     * @param nroContrato the nro contrato
     */
    public void setNroContrato(String nroContrato) {
	this.nroContrato = nroContrato;
    }

    /**
     * Get: tipoContaDebito.
     *
     * @return tipoContaDebito
     */
    public String getTipoContaDebito() {
	return tipoContaDebito;
    }

    /**
     * Set: tipoContaDebito.
     *
     * @param tipoContaDebito the tipo conta debito
     */
    public void setTipoContaDebito(String tipoContaDebito) {
	this.tipoContaDebito = tipoContaDebito;
    }

    /**
     * Is opcao checar todos.
     *
     * @return true, if is opcao checar todos
     */
    public boolean isOpcaoChecarTodos() {
	return opcaoChecarTodos;
    }

    /**
     * Set: opcaoChecarTodos.
     *
     * @param opcaoChecarTodos the opcao checar todos
     */
    public void setOpcaoChecarTodos(boolean opcaoChecarTodos) {
	this.opcaoChecarTodos = opcaoChecarTodos;
    }

    /**
     * Get: listaGridDetalharLiberarPagtoSelecionados.
     *
     * @return listaGridDetalharLiberarPagtoSelecionados
     */
    public List<OcorrenciasDetLibPagConsSemConsSalSaidaDTO> getListaGridDetalharLiberarPagtoSelecionados() {
	return listaGridDetalharLiberarPagtoSelecionados;
    }

    /**
     * Set: listaGridDetalharLiberarPagtoSelecionados.
     *
     * @param listaGridDetalharLiberarPagtoSelecionados the lista grid detalhar liberar pagto selecionados
     */
    public void setListaGridDetalharLiberarPagtoSelecionados(
	    List<OcorrenciasDetLibPagConsSemConsSalSaidaDTO> listaGridDetalharLiberarPagtoSelecionados) {
	this.listaGridDetalharLiberarPagtoSelecionados = listaGridDetalharLiberarPagtoSelecionados;
    }

    /**
     * Get: numeroPagamento.
     *
     * @return numeroPagamento
     */
    public String getNumeroPagamento() {
	return numeroPagamento;
    }

    /**
     * Set: numeroPagamento.
     *
     * @param numeroPagamento the numero pagamento
     */
    public void setNumeroPagamento(String numeroPagamento) {
	this.numeroPagamento = numeroPagamento;
    }

    /**
     * Get: tipoCanal.
     *
     * @return tipoCanal
     */
    public Integer getTipoCanal() {
	return tipoCanal;
    }

    /**
     * Set: tipoCanal.
     *
     * @param tipoCanal the tipo canal
     */
    public void setTipoCanal(Integer tipoCanal) {
	this.tipoCanal = tipoCanal;
    }

    /**
     * Get: camaraCentralizadora.
     *
     * @return camaraCentralizadora
     */
    public String getCamaraCentralizadora() {
	return camaraCentralizadora;
    }

    /**
     * Set: camaraCentralizadora.
     *
     * @param camaraCentralizadora the camara centralizadora
     */
    public void setCamaraCentralizadora(String camaraCentralizadora) {
	this.camaraCentralizadora = camaraCentralizadora;
    }

    /**
     * Get: finalidadeDoc.
     *
     * @return finalidadeDoc
     */
    public String getFinalidadeDoc() {
	return finalidadeDoc;
    }

    /**
     * Set: finalidadeDoc.
     *
     * @param finalidadeDoc the finalidade doc
     */
    public void setFinalidadeDoc(String finalidadeDoc) {
	this.finalidadeDoc = finalidadeDoc;
    }

    /**
     * Get: identificacaoTitularidade.
     *
     * @return identificacaoTitularidade
     */
    public String getIdentificacaoTitularidade() {
	return identificacaoTitularidade;
    }

    /**
     * Set: identificacaoTitularidade.
     *
     * @param identificacaoTitularidade the identificacao titularidade
     */
    public void setIdentificacaoTitularidade(String identificacaoTitularidade) {
	this.identificacaoTitularidade = identificacaoTitularidade;
    }

    /**
     * Get: identificacaoTransferenciaDoc.
     *
     * @return identificacaoTransferenciaDoc
     */
    public String getIdentificacaoTransferenciaDoc() {
	return identificacaoTransferenciaDoc;
    }

    /**
     * Set: identificacaoTransferenciaDoc.
     *
     * @param identificacaoTransferenciaDoc the identificacao transferencia doc
     */
    public void setIdentificacaoTransferenciaDoc(String identificacaoTransferenciaDoc) {
	this.identificacaoTransferenciaDoc = identificacaoTransferenciaDoc;
    }

    /**
     * Get: finalidadeTed.
     *
     * @return finalidadeTed
     */
    public String getFinalidadeTed() {
	return finalidadeTed;
    }

    /**
     * Set: finalidadeTed.
     *
     * @param finalidadeTed the finalidade ted
     */
    public void setFinalidadeTed(String finalidadeTed) {
	this.finalidadeTed = finalidadeTed;
    }

    /**
     * Get: identificacaoTransferenciaTed.
     *
     * @return identificacaoTransferenciaTed
     */
    public String getIdentificacaoTransferenciaTed() {
	return identificacaoTransferenciaTed;
    }

    /**
     * Set: identificacaoTransferenciaTed.
     *
     * @param identificacaoTransferenciaTed the identificacao transferencia ted
     */
    public void setIdentificacaoTransferenciaTed(String identificacaoTransferenciaTed) {
	this.identificacaoTransferenciaTed = identificacaoTransferenciaTed;
    }

    /**
     * Get: dataExpiracaoCredito.
     *
     * @return dataExpiracaoCredito
     */
    public String getDataExpiracaoCredito() {
	return dataExpiracaoCredito;
    }

    /**
     * Set: dataExpiracaoCredito.
     *
     * @param dataExpiracaoCredito the data expiracao credito
     */
    public void setDataExpiracaoCredito(String dataExpiracaoCredito) {
	this.dataExpiracaoCredito = dataExpiracaoCredito;
    }

    /**
     * Get: dataRetirada.
     *
     * @return dataRetirada
     */
    public String getDataRetirada() {
	return dataRetirada;
    }

    /**
     * Set: dataRetirada.
     *
     * @param dataRetirada the data retirada
     */
    public void setDataRetirada(String dataRetirada) {
	this.dataRetirada = dataRetirada;
    }

    /**
     * Get: identificadorRetirada.
     *
     * @return identificadorRetirada
     */
    public String getIdentificadorRetirada() {
	return identificadorRetirada;
    }

    /**
     * Set: identificadorRetirada.
     *
     * @param identificadorRetirada the identificador retirada
     */
    public void setIdentificadorRetirada(String identificadorRetirada) {
	this.identificadorRetirada = identificadorRetirada;
    }

    /**
     * Get: identificadorTipoRetirada.
     *
     * @return identificadorTipoRetirada
     */
    public String getIdentificadorTipoRetirada() {
	return identificadorTipoRetirada;
    }

    /**
     * Set: identificadorTipoRetirada.
     *
     * @param identificadorTipoRetirada the identificador tipo retirada
     */
    public void setIdentificadorTipoRetirada(String identificadorTipoRetirada) {
	this.identificadorTipoRetirada = identificadorTipoRetirada;
    }

    /**
     * Get: indicadorAssociadoUnicaAgencia.
     *
     * @return indicadorAssociadoUnicaAgencia
     */
    public String getIndicadorAssociadoUnicaAgencia() {
	return indicadorAssociadoUnicaAgencia;
    }

    /**
     * Set: indicadorAssociadoUnicaAgencia.
     *
     * @param indicadorAssociadoUnicaAgencia the indicador associado unica agencia
     */
    public void setIndicadorAssociadoUnicaAgencia(String indicadorAssociadoUnicaAgencia) {
	this.indicadorAssociadoUnicaAgencia = indicadorAssociadoUnicaAgencia;
    }

    /**
     * Get: instrucaoPagamento.
     *
     * @return instrucaoPagamento
     */
    public String getInstrucaoPagamento() {
	return instrucaoPagamento;
    }

    /**
     * Set: instrucaoPagamento.
     *
     * @param instrucaoPagamento the instrucao pagamento
     */
    public void setInstrucaoPagamento(String instrucaoPagamento) {
	this.instrucaoPagamento = instrucaoPagamento;
    }

    /**
     * Get: numeroCheque.
     *
     * @return numeroCheque
     */
    public String getNumeroCheque() {
	return numeroCheque;
    }

    /**
     * Set: numeroCheque.
     *
     * @param numeroCheque the numero cheque
     */
    public void setNumeroCheque(String numeroCheque) {
	this.numeroCheque = numeroCheque;
    }

    /**
     * Get: numeroOp.
     *
     * @return numeroOp
     */
    public String getNumeroOp() {
	return numeroOp;
    }

    /**
     * Set: numeroOp.
     *
     * @param numeroOp the numero op
     */
    public void setNumeroOp(String numeroOp) {
	this.numeroOp = numeroOp;
    }

    /**
     * Get: serieCheque.
     *
     * @return serieCheque
     */
    public String getSerieCheque() {
	return serieCheque;
    }

    /**
     * Set: serieCheque.
     *
     * @param serieCheque the serie cheque
     */
    public void setSerieCheque(String serieCheque) {
	this.serieCheque = serieCheque;
    }

    /**
     * Get: vlImpostoMovFinanceira.
     *
     * @return vlImpostoMovFinanceira
     */
    public BigDecimal getVlImpostoMovFinanceira() {
	return vlImpostoMovFinanceira;
    }

    /**
     * Set: vlImpostoMovFinanceira.
     *
     * @param vlImpostoMovFinanceira the vl imposto mov financeira
     */
    public void setVlImpostoMovFinanceira(BigDecimal vlImpostoMovFinanceira) {
	this.vlImpostoMovFinanceira = vlImpostoMovFinanceira;
    }

    /**
     * Get: codigoBarras.
     *
     * @return codigoBarras
     */
    public String getCodigoBarras() {
	return codigoBarras;
    }

    /**
     * Set: codigoBarras.
     *
     * @param codigoBarras the codigo barras
     */
    public void setCodigoBarras(String codigoBarras) {
	this.codigoBarras = codigoBarras;
    }

    /**
     * Get: agenteArrecadador.
     *
     * @return agenteArrecadador
     */
    public String getAgenteArrecadador() {
	return agenteArrecadador;
    }

    /**
     * Set: agenteArrecadador.
     *
     * @param agenteArrecadador the agente arrecadador
     */
    public void setAgenteArrecadador(String agenteArrecadador) {
	this.agenteArrecadador = agenteArrecadador;
    }

    /**
     * Get: anoExercicio.
     *
     * @return anoExercicio
     */
    public String getAnoExercicio() {
	return anoExercicio;
    }

    /**
     * Set: anoExercicio.
     *
     * @param anoExercicio the ano exercicio
     */
    public void setAnoExercicio(String anoExercicio) {
	this.anoExercicio = anoExercicio;
    }

    /**
     * Get: codigoReceita.
     *
     * @return codigoReceita
     */
    public String getCodigoReceita() {
	return codigoReceita;
    }

    /**
     * Set: codigoReceita.
     *
     * @param codigoReceita the codigo receita
     */
    public void setCodigoReceita(String codigoReceita) {
	this.codigoReceita = codigoReceita;
    }

    /**
     * Get: dataReferencia.
     *
     * @return dataReferencia
     */
    public String getDataReferencia() {
	return dataReferencia;
    }

    /**
     * Set: dataReferencia.
     *
     * @param dataReferencia the data referencia
     */
    public void setDataReferencia(String dataReferencia) {
	this.dataReferencia = dataReferencia;
    }

    /**
     * Get: dddContribuinte.
     *
     * @return dddContribuinte
     */
    public String getDddContribuinte() {
	return dddContribuinte;
    }

    /**
     * Set: dddContribuinte.
     *
     * @param dddContribuinte the ddd contribuinte
     */
    public void setDddContribuinte(String dddContribuinte) {
	this.dddContribuinte = dddContribuinte;
    }

    /**
     * Get: inscricaoEstadualContribuinte.
     *
     * @return inscricaoEstadualContribuinte
     */
    public String getInscricaoEstadualContribuinte() {
	return inscricaoEstadualContribuinte;
    }

    /**
     * Set: inscricaoEstadualContribuinte.
     *
     * @param inscricaoEstadualContribuinte the inscricao estadual contribuinte
     */
    public void setInscricaoEstadualContribuinte(String inscricaoEstadualContribuinte) {
	this.inscricaoEstadualContribuinte = inscricaoEstadualContribuinte;
    }

    /**
     * Get: numeroCotaParcela.
     *
     * @return numeroCotaParcela
     */
    public String getNumeroCotaParcela() {
	return numeroCotaParcela;
    }

    /**
     * Set: numeroCotaParcela.
     *
     * @param numeroCotaParcela the numero cota parcela
     */
    public void setNumeroCotaParcela(String numeroCotaParcela) {
	this.numeroCotaParcela = numeroCotaParcela;
    }

    /**
     * Get: numeroReferencia.
     *
     * @return numeroReferencia
     */
    public String getNumeroReferencia() {
	return numeroReferencia;
    }

    /**
     * Set: numeroReferencia.
     *
     * @param numeroReferencia the numero referencia
     */
    public void setNumeroReferencia(String numeroReferencia) {
	this.numeroReferencia = numeroReferencia;
    }

    /**
     * Get: percentualReceita.
     *
     * @return percentualReceita
     */
    public BigDecimal getPercentualReceita() {
	return percentualReceita;
    }

    /**
     * Set: percentualReceita.
     *
     * @param percentualReceita the percentual receita
     */
    public void setPercentualReceita(BigDecimal percentualReceita) {
	this.percentualReceita = percentualReceita;
    }

    /**
     * Get: periodoApuracao.
     *
     * @return periodoApuracao
     */
    public String getPeriodoApuracao() {
	return periodoApuracao;
    }

    /**
     * Set: periodoApuracao.
     *
     * @param periodoApuracao the periodo apuracao
     */
    public void setPeriodoApuracao(String periodoApuracao) {
	this.periodoApuracao = periodoApuracao;
    }

    /**
     * Get: telefoneContribuinte.
     *
     * @return telefoneContribuinte
     */
    public String getTelefoneContribuinte() {
	return telefoneContribuinte;
    }

    /**
     * Set: telefoneContribuinte.
     *
     * @param telefoneContribuinte the telefone contribuinte
     */
    public void setTelefoneContribuinte(String telefoneContribuinte) {
	this.telefoneContribuinte = telefoneContribuinte;
    }

    /**
     * Get: vlAtualizacaoMonetaria.
     *
     * @return vlAtualizacaoMonetaria
     */
    public BigDecimal getVlAtualizacaoMonetaria() {
	return vlAtualizacaoMonetaria;
    }

    /**
     * Set: vlAtualizacaoMonetaria.
     *
     * @param vlAtualizacaoMonetaria the vl atualizacao monetaria
     */
    public void setVlAtualizacaoMonetaria(BigDecimal vlAtualizacaoMonetaria) {
	this.vlAtualizacaoMonetaria = vlAtualizacaoMonetaria;
    }

    /**
     * Get: vlPrincipal.
     *
     * @return vlPrincipal
     */
    public BigDecimal getVlPrincipal() {
	return vlPrincipal;
    }

    /**
     * Set: vlPrincipal.
     *
     * @param vlPrincipal the vl principal
     */
    public void setVlPrincipal(BigDecimal vlPrincipal) {
	this.vlPrincipal = vlPrincipal;
    }

    /**
     * Get: vlReceita.
     *
     * @return vlReceita
     */
    public BigDecimal getVlReceita() {
	return vlReceita;
    }

    /**
     * Set: vlReceita.
     *
     * @param vlReceita the vl receita
     */
    public void setVlReceita(BigDecimal vlReceita) {
	this.vlReceita = vlReceita;
    }

    /**
     * Get: vlTotal.
     *
     * @return vlTotal
     */
    public BigDecimal getVlTotal() {
	return vlTotal;
    }

    /**
     * Set: vlTotal.
     *
     * @param vlTotal the vl total
     */
    public void setVlTotal(BigDecimal vlTotal) {
	this.vlTotal = vlTotal;
    }

    /**
     * Get: codigoCnae.
     *
     * @return codigoCnae
     */
    public String getCodigoCnae() {
	return codigoCnae;
    }

    /**
     * Set: codigoCnae.
     *
     * @param codigoCnae the codigo cnae
     */
    public void setCodigoCnae(String codigoCnae) {
	this.codigoCnae = codigoCnae;
    }

    /**
     * Get: codigoOrgaoFavorecido.
     *
     * @return codigoOrgaoFavorecido
     */
    public String getCodigoOrgaoFavorecido() {
	return codigoOrgaoFavorecido;
    }

    /**
     * Set: codigoOrgaoFavorecido.
     *
     * @param codigoOrgaoFavorecido the codigo orgao favorecido
     */
    public void setCodigoOrgaoFavorecido(String codigoOrgaoFavorecido) {
	this.codigoOrgaoFavorecido = codigoOrgaoFavorecido;
    }

    /**
     * Get: codigoUfFavorecida.
     *
     * @return codigoUfFavorecida
     */
    public String getCodigoUfFavorecida() {
	return codigoUfFavorecida;
    }

    /**
     * Set: codigoUfFavorecida.
     *
     * @param codigoUfFavorecida the codigo uf favorecida
     */
    public void setCodigoUfFavorecida(String codigoUfFavorecida) {
	this.codigoUfFavorecida = codigoUfFavorecida;
    }

    /**
     * Get: descricaoUfFavorecida.
     *
     * @return descricaoUfFavorecida
     */
    public String getDescricaoUfFavorecida() {
	return descricaoUfFavorecida;
    }

    /**
     * Set: descricaoUfFavorecida.
     *
     * @param descricaoUfFavorecida the descricao uf favorecida
     */
    public void setDescricaoUfFavorecida(String descricaoUfFavorecida) {
	this.descricaoUfFavorecida = descricaoUfFavorecida;
    }

    /**
     * Get: nomeOrgaoFavorecido.
     *
     * @return nomeOrgaoFavorecido
     */
    public String getNomeOrgaoFavorecido() {
	return nomeOrgaoFavorecido;
    }

    /**
     * Set: nomeOrgaoFavorecido.
     *
     * @param nomeOrgaoFavorecido the nome orgao favorecido
     */
    public void setNomeOrgaoFavorecido(String nomeOrgaoFavorecido) {
	this.nomeOrgaoFavorecido = nomeOrgaoFavorecido;
    }

    /**
     * Get: numeroParcelamento.
     *
     * @return numeroParcelamento
     */
    public String getNumeroParcelamento() {
	return numeroParcelamento;
    }

    /**
     * Set: numeroParcelamento.
     *
     * @param numeroParcelamento the numero parcelamento
     */
    public void setNumeroParcelamento(String numeroParcelamento) {
	this.numeroParcelamento = numeroParcelamento;
    }

    /**
     * Get: observacaoTributo.
     *
     * @return observacaoTributo
     */
    public String getObservacaoTributo() {
	return observacaoTributo;
    }

    /**
     * Set: observacaoTributo.
     *
     * @param observacaoTributo the observacao tributo
     */
    public void setObservacaoTributo(String observacaoTributo) {
	this.observacaoTributo = observacaoTributo;
    }

    /**
     * Get: placaVeiculo.
     *
     * @return placaVeiculo
     */
    public String getPlacaVeiculo() {
	return placaVeiculo;
    }

    /**
     * Set: placaVeiculo.
     *
     * @param placaVeiculo the placa veiculo
     */
    public void setPlacaVeiculo(String placaVeiculo) {
	this.placaVeiculo = placaVeiculo;
    }

    /**
     * Get: vlAcrescimoFinanceiro.
     *
     * @return vlAcrescimoFinanceiro
     */
    public BigDecimal getVlAcrescimoFinanceiro() {
	return vlAcrescimoFinanceiro;
    }

    /**
     * Set: vlAcrescimoFinanceiro.
     *
     * @param vlAcrescimoFinanceiro the vl acrescimo financeiro
     */
    public void setVlAcrescimoFinanceiro(BigDecimal vlAcrescimoFinanceiro) {
	this.vlAcrescimoFinanceiro = vlAcrescimoFinanceiro;
    }

    /**
     * Get: vlHonorarioAdvogado.
     *
     * @return vlHonorarioAdvogado
     */
    public BigDecimal getVlHonorarioAdvogado() {
	return vlHonorarioAdvogado;
    }

    /**
     * Set: vlHonorarioAdvogado.
     *
     * @param vlHonorarioAdvogado the vl honorario advogado
     */
    public void setVlHonorarioAdvogado(BigDecimal vlHonorarioAdvogado) {
	this.vlHonorarioAdvogado = vlHonorarioAdvogado;
    }

    /**
     * Get: codigoInss.
     *
     * @return codigoInss
     */
    public String getCodigoInss() {
	return codigoInss;
    }

    /**
     * Set: codigoInss.
     *
     * @param codigoInss the codigo inss
     */
    public void setCodigoInss(String codigoInss) {
	this.codigoInss = codigoInss;
    }

    /**
     * Get: dataCompetencia.
     *
     * @return dataCompetencia
     */
    public String getDataCompetencia() {
	return dataCompetencia;
    }

    /**
     * Set: dataCompetencia.
     *
     * @param dataCompetencia the data competencia
     */
    public void setDataCompetencia(String dataCompetencia) {
	this.dataCompetencia = dataCompetencia;
    }

    /**
     * Get: vlOutrasEntidades.
     *
     * @return vlOutrasEntidades
     */
    public BigDecimal getVlOutrasEntidades() {
	return vlOutrasEntidades;
    }

    /**
     * Set: vlOutrasEntidades.
     *
     * @param vlOutrasEntidades the vl outras entidades
     */
    public void setVlOutrasEntidades(BigDecimal vlOutrasEntidades) {
	this.vlOutrasEntidades = vlOutrasEntidades;
    }

    /**
     * Get: codigoTributo.
     *
     * @return codigoTributo
     */
    public String getCodigoTributo() {
	return codigoTributo;
    }

    /**
     * Set: codigoTributo.
     *
     * @param codigoTributo the codigo tributo
     */
    public void setCodigoTributo(String codigoTributo) {
	this.codigoTributo = codigoTributo;
    }

    /**
     * Get: codigoRenavam.
     *
     * @return codigoRenavam
     */
    public String getCodigoRenavam() {
	return codigoRenavam;
    }

    /**
     * Set: codigoRenavam.
     *
     * @param codigoRenavam the codigo renavam
     */
    public void setCodigoRenavam(String codigoRenavam) {
	this.codigoRenavam = codigoRenavam;
    }

    /**
     * Get: municipioTributo.
     *
     * @return municipioTributo
     */
    public String getMunicipioTributo() {
	return municipioTributo;
    }

    /**
     * Set: municipioTributo.
     *
     * @param municipioTributo the municipio tributo
     */
    public void setMunicipioTributo(String municipioTributo) {
	this.municipioTributo = municipioTributo;
    }

    /**
     * Get: numeroNsu.
     *
     * @return numeroNsu
     */
    public String getNumeroNsu() {
	return numeroNsu;
    }

    /**
     * Set: numeroNsu.
     *
     * @param numeroNsu the numero nsu
     */
    public void setNumeroNsu(String numeroNsu) {
	this.numeroNsu = numeroNsu;
    }

    /**
     * Get: opcaoRetiradaTributo.
     *
     * @return opcaoRetiradaTributo
     */
    public String getOpcaoRetiradaTributo() {
	return opcaoRetiradaTributo;
    }

    /**
     * Set: opcaoRetiradaTributo.
     *
     * @param opcaoRetiradaTributo the opcao retirada tributo
     */
    public void setOpcaoRetiradaTributo(String opcaoRetiradaTributo) {
	this.opcaoRetiradaTributo = opcaoRetiradaTributo;
    }

    /**
     * Get: opcaoTributo.
     *
     * @return opcaoTributo
     */
    public String getOpcaoTributo() {
	return opcaoTributo;
    }

    /**
     * Set: opcaoTributo.
     *
     * @param opcaoTributo the opcao tributo
     */
    public void setOpcaoTributo(String opcaoTributo) {
	this.opcaoTributo = opcaoTributo;
    }

    /**
     * Get: ufTributo.
     *
     * @return ufTributo
     */
    public String getUfTributo() {
	return ufTributo;
    }

    /**
     * Set: ufTributo.
     *
     * @param ufTributo the uf tributo
     */
    public void setUfTributo(String ufTributo) {
	this.ufTributo = ufTributo;
    }

    /**
     * Get: bancoCartaoDepositoIdentificado.
     *
     * @return bancoCartaoDepositoIdentificado
     */
    public String getBancoCartaoDepositoIdentificado() {
	return bancoCartaoDepositoIdentificado;
    }

    /**
     * Set: bancoCartaoDepositoIdentificado.
     *
     * @param bancoCartaoDepositoIdentificado the banco cartao deposito identificado
     */
    public void setBancoCartaoDepositoIdentificado(String bancoCartaoDepositoIdentificado) {
	this.bancoCartaoDepositoIdentificado = bancoCartaoDepositoIdentificado;
    }

    /**
     * Get: bimCartaoDepositoIdentificado.
     *
     * @return bimCartaoDepositoIdentificado
     */
    public String getBimCartaoDepositoIdentificado() {
	return bimCartaoDepositoIdentificado;
    }

    /**
     * Set: bimCartaoDepositoIdentificado.
     *
     * @param bimCartaoDepositoIdentificado the bim cartao deposito identificado
     */
    public void setBimCartaoDepositoIdentificado(String bimCartaoDepositoIdentificado) {
	this.bimCartaoDepositoIdentificado = bimCartaoDepositoIdentificado;
    }

    /**
     * Get: cartaoDepositoIdentificado.
     *
     * @return cartaoDepositoIdentificado
     */
    public String getCartaoDepositoIdentificado() {
	return cartaoDepositoIdentificado;
    }

    /**
     * Set: cartaoDepositoIdentificado.
     *
     * @param cartaoDepositoIdentificado the cartao deposito identificado
     */
    public void setCartaoDepositoIdentificado(String cartaoDepositoIdentificado) {
	this.cartaoDepositoIdentificado = cartaoDepositoIdentificado;
    }

    /**
     * Get: clienteDepositoIdentificado.
     *
     * @return clienteDepositoIdentificado
     */
    public String getClienteDepositoIdentificado() {
	return clienteDepositoIdentificado;
    }

    /**
     * Set: clienteDepositoIdentificado.
     *
     * @param clienteDepositoIdentificado the cliente deposito identificado
     */
    public void setClienteDepositoIdentificado(String clienteDepositoIdentificado) {
	this.clienteDepositoIdentificado = clienteDepositoIdentificado;
    }

    /**
     * Get: codigoDepositante.
     *
     * @return codigoDepositante
     */
    public String getCodigoDepositante() {
	return codigoDepositante;
    }

    /**
     * Set: codigoDepositante.
     *
     * @param codigoDepositante the codigo depositante
     */
    public void setCodigoDepositante(String codigoDepositante) {
	this.codigoDepositante = codigoDepositante;
    }

    /**
     * Get: nomeDepositante.
     *
     * @return nomeDepositante
     */
    public String getNomeDepositante() {
	return nomeDepositante;
    }

    /**
     * Set: nomeDepositante.
     *
     * @param nomeDepositante the nome depositante
     */
    public void setNomeDepositante(String nomeDepositante) {
	this.nomeDepositante = nomeDepositante;
    }

    /**
     * Get: produtoDepositoIdentificado.
     *
     * @return produtoDepositoIdentificado
     */
    public String getProdutoDepositoIdentificado() {
	return produtoDepositoIdentificado;
    }

    /**
     * Set: produtoDepositoIdentificado.
     *
     * @param produtoDepositoIdentificado the produto deposito identificado
     */
    public void setProdutoDepositoIdentificado(String produtoDepositoIdentificado) {
	this.produtoDepositoIdentificado = produtoDepositoIdentificado;
    }

    /**
     * Get: sequenciaProdutoDepositoIdentificado.
     *
     * @return sequenciaProdutoDepositoIdentificado
     */
    public String getSequenciaProdutoDepositoIdentificado() {
	return sequenciaProdutoDepositoIdentificado;
    }

    /**
     * Set: sequenciaProdutoDepositoIdentificado.
     *
     * @param sequenciaProdutoDepositoIdentificado the sequencia produto deposito identificado
     */
    public void setSequenciaProdutoDepositoIdentificado(String sequenciaProdutoDepositoIdentificado) {
	this.sequenciaProdutoDepositoIdentificado = sequenciaProdutoDepositoIdentificado;
    }

    /**
     * Get: tipoDespositoIdentificado.
     *
     * @return tipoDespositoIdentificado
     */
    public String getTipoDespositoIdentificado() {
	return tipoDespositoIdentificado;
    }

    /**
     * Set: tipoDespositoIdentificado.
     *
     * @param tipoDespositoIdentificado the tipo desposito identificado
     */
    public void setTipoDespositoIdentificado(String tipoDespositoIdentificado) {
	this.tipoDespositoIdentificado = tipoDespositoIdentificado;
    }

    /**
     * Get: viaCartaoDepositoIdentificado.
     *
     * @return viaCartaoDepositoIdentificado
     */
    public String getViaCartaoDepositoIdentificado() {
	return viaCartaoDepositoIdentificado;
    }

    /**
     * Set: viaCartaoDepositoIdentificado.
     *
     * @param viaCartaoDepositoIdentificado the via cartao deposito identificado
     */
    public void setViaCartaoDepositoIdentificado(String viaCartaoDepositoIdentificado) {
	this.viaCartaoDepositoIdentificado = viaCartaoDepositoIdentificado;
    }

    /**
     * Get: codigoOrdemCredito.
     *
     * @return codigoOrdemCredito
     */
    public String getCodigoOrdemCredito() {
	return codigoOrdemCredito;
    }

    /**
     * Set: codigoOrdemCredito.
     *
     * @param codigoOrdemCredito the codigo ordem credito
     */
    public void setCodigoOrdemCredito(String codigoOrdemCredito) {
	this.codigoOrdemCredito = codigoOrdemCredito;
    }

    /**
     * Get: codigoPagador.
     *
     * @return codigoPagador
     */
    public String getCodigoPagador() {
	return codigoPagador;
    }

    /**
     * Set: codigoPagador.
     *
     * @param codigoPagador the codigo pagador
     */
    public void setCodigoPagador(String codigoPagador) {
	this.codigoPagador = codigoPagador;
    }

    /**
     * Get: tipoBeneficiario.
     *
     * @return tipoBeneficiario
     */
    public String getTipoBeneficiario() {
	return tipoBeneficiario;
    }

    /**
     * Set: tipoBeneficiario.
     *
     * @param tipoBeneficiario the tipo beneficiario
     */
    public void setTipoBeneficiario(String tipoBeneficiario) {
	this.tipoBeneficiario = tipoBeneficiario;
    }

    /**
     * Get: qtdePagamentos.
     *
     * @return qtdePagamentos
     */
    public Long getQtdePagamentos() {
	return qtdePagamentos;
    }

    /**
     * Set: qtdePagamentos.
     *
     * @param qtdePagamentos the qtde pagamentos
     */
    public void setQtdePagamentos(Long qtdePagamentos) {
	this.qtdePagamentos = qtdePagamentos;
    }

    /**
     * Get: qtdePagamentosAutorizados.
     *
     * @return qtdePagamentosAutorizados
     */
    public Long getQtdePagamentosAutorizados() {
	return qtdePagamentosAutorizados;
    }

    /**
     * Set: qtdePagamentosAutorizados.
     *
     * @param qtdePagamentosAutorizados the qtde pagamentos autorizados
     */
    public void setQtdePagamentosAutorizados(Long qtdePagamentosAutorizados) {
	this.qtdePagamentosAutorizados = qtdePagamentosAutorizados;
    }

    /**
     * Get: vlTotalPagamentos.
     *
     * @return vlTotalPagamentos
     */
    public BigDecimal getVlTotalPagamentos() {
	return vlTotalPagamentos;
    }

    /**
     * Set: vlTotalPagamentos.
     *
     * @param vlTotalPagamentos the vl total pagamentos
     */
    public void setVlTotalPagamentos(BigDecimal vlTotalPagamentos) {
	this.vlTotalPagamentos = vlTotalPagamentos;
    }

    /**
     * Get: vlTotalPagamentosAutorizados.
     *
     * @return vlTotalPagamentosAutorizados
     */
    public BigDecimal getVlTotalPagamentosAutorizados() {
	return vlTotalPagamentosAutorizados;
    }

    /**
     * Set: vlTotalPagamentosAutorizados.
     *
     * @param vlTotalPagamentosAutorizados the vl total pagamentos autorizados
     */
    public void setVlTotalPagamentosAutorizados(BigDecimal vlTotalPagamentosAutorizados) {
	this.vlTotalPagamentosAutorizados = vlTotalPagamentosAutorizados;
    }

    /**
     * Get: tipoTela.
     *
     * @return tipoTela
     */
    public Integer getTipoTela() {
	return tipoTela;
    }

    /**
     * Set: tipoTela.
     *
     * @param tipoTela the tipo tela
     */
    public void setTipoTela(Integer tipoTela) {
	this.tipoTela = tipoTela;
    }

    /**
     * Get: logoPath.
     *
     * @return logoPath
     */
    public String getLogoPath() {
	return logoPath;
    }

    /**
     * Set: logoPath.
     *
     * @param logoPath the logo path
     */
    public void setLogoPath(String logoPath) {
	this.logoPath = logoPath;
    }

    /**
     * Set: agenciaDigitoCredito.
     *
     * @param agenciaDigitoCredito the agencia digito credito
     */
    public void setAgenciaDigitoCredito(String agenciaDigitoCredito) {
	this.agenciaDigitoCredito = agenciaDigitoCredito;
    }

    /**
     * Set: cdBancoCredito.
     *
     * @param cdBancoCredito the cd banco credito
     */
    public void setCdBancoCredito(String cdBancoCredito) {
	this.cdBancoCredito = cdBancoCredito;
    }

    /**
     * Set: cdIndicadorEconomicoMoeda.
     *
     * @param cdIndicadorEconomicoMoeda the cd indicador economico moeda
     */
    public void setCdIndicadorEconomicoMoeda(String cdIndicadorEconomicoMoeda) {
	this.cdIndicadorEconomicoMoeda = cdIndicadorEconomicoMoeda;
    }

    /**
     * Set: cdSituacaoOperacaoPagamento.
     *
     * @param cdSituacaoOperacaoPagamento the cd situacao operacao pagamento
     */
    public void setCdSituacaoOperacaoPagamento(String cdSituacaoOperacaoPagamento) {
	this.cdSituacaoOperacaoPagamento = cdSituacaoOperacaoPagamento;
    }

    /**
     * Set: dsContaFavorecido.
     *
     * @param dsContaFavorecido the ds conta favorecido
     */
    public void setDsContaFavorecido(String dsContaFavorecido) {
	this.dsContaFavorecido = dsContaFavorecido;
    }

    /**
     * Get: agenciaDigitoCredito.
     *
     * @return agenciaDigitoCredito
     */
    public String getAgenciaDigitoCredito() {
	return agenciaDigitoCredito;
    }

    /**
     * Get: cdBancoCredito.
     *
     * @return cdBancoCredito
     */
    public String getCdBancoCredito() {
	return cdBancoCredito;
    }

    /**
     * Get: cdIndicadorEconomicoMoeda.
     *
     * @return cdIndicadorEconomicoMoeda
     */
    public String getCdIndicadorEconomicoMoeda() {
	return cdIndicadorEconomicoMoeda;
    }

    /**
     * Get: cdSituacaoOperacaoPagamento.
     *
     * @return cdSituacaoOperacaoPagamento
     */
    public String getCdSituacaoOperacaoPagamento() {
	return cdSituacaoOperacaoPagamento;
    }

    /**
     * Get: dsContaFavorecido.
     *
     * @return dsContaFavorecido
     */
    public String getDsContaFavorecido() {
	return dsContaFavorecido;
    }

    /**
     * Get: panelBotoes.
     *
     * @return panelBotoes
     */
    public Boolean getPanelBotoes() {
	return panelBotoes;
    }

    /**
     * Set: panelBotoes.
     *
     * @param panelBotoes the panel botoes
     */
    public void setPanelBotoes(Boolean panelBotoes) {
	this.panelBotoes = panelBotoes;
    }

    /**
     * Get: dsOrigemPagamento.
     *
     * @return dsOrigemPagamento
     */
    public String getDsOrigemPagamento() {
	return dsOrigemPagamento;
    }

    /**
     * Set: dsOrigemPagamento.
     *
     * @param dsOrigemPagamento the ds origem pagamento
     */
    public void setDsOrigemPagamento(String dsOrigemPagamento) {
	this.dsOrigemPagamento = dsOrigemPagamento;
    }

    /**
     * Get: nossoNumero.
     *
     * @return nossoNumero
     */
    public String getNossoNumero() {
	return nossoNumero;
    }

    /**
     * Set: nossoNumero.
     *
     * @param nossoNumero the nosso numero
     */
    public void setNossoNumero(String nossoNumero) {
	this.nossoNumero = nossoNumero;
    }

    /**
     * Is disable argumentos consulta.
     *
     * @return true, if is disable argumentos consulta
     */
    public boolean isDisableArgumentosConsulta() {
	return disableArgumentosConsulta;
    }

    /**
     * Set: disableArgumentosConsulta.
     *
     * @param disableArgumentosConsulta the disable argumentos consulta
     */
    public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
	this.disableArgumentosConsulta = disableArgumentosConsulta;
    }

    /**
     * Is exibe beneficiario.
     *
     * @return true, if is exibe beneficiario
     */
    public boolean isExibeBeneficiario() {
	return exibeBeneficiario;
    }

    /**
     * Set: exibeBeneficiario.
     *
     * @param exibeBeneficiario the exibe beneficiario
     */
    public void setExibeBeneficiario(boolean exibeBeneficiario) {
	this.exibeBeneficiario = exibeBeneficiario;
    }

    /**
     * Get: carteira.
     *
     * @return carteira
     */
    public String getCarteira() {
	return carteira;
    }

    /**
     * Set: carteira.
     *
     * @param carteira the carteira
     */
    public void setCarteira(String carteira) {
	this.carteira = carteira;
    }

    /**
     * Get: cdIndentificadorJudicial.
     *
     * @return cdIndentificadorJudicial
     */
    public String getCdIndentificadorJudicial() {
	return cdIndentificadorJudicial;
    }

    /**
     * Set: cdIndentificadorJudicial.
     *
     * @param cdIndentificadorJudicial the cd indentificador judicial
     */
    public void setCdIndentificadorJudicial(String cdIndentificadorJudicial) {
	this.cdIndentificadorJudicial = cdIndentificadorJudicial;
    }

    /**
     * Get: cdSituacaoTranferenciaAutomatica.
     *
     * @return cdSituacaoTranferenciaAutomatica
     */
    public String getCdSituacaoTranferenciaAutomatica() {
	return cdSituacaoTranferenciaAutomatica;
    }

    /**
     * Set: cdSituacaoTranferenciaAutomatica.
     *
     * @param cdSituacaoTranferenciaAutomatica the cd situacao tranferencia automatica
     */
    public void setCdSituacaoTranferenciaAutomatica(String cdSituacaoTranferenciaAutomatica) {
	this.cdSituacaoTranferenciaAutomatica = cdSituacaoTranferenciaAutomatica;
    }

    /**
     * Get: dtLimiteDescontoPagamento.
     *
     * @return dtLimiteDescontoPagamento
     */
    public String getDtLimiteDescontoPagamento() {
	return dtLimiteDescontoPagamento;
    }

    /**
     * Set: dtLimiteDescontoPagamento.
     *
     * @param dtLimiteDescontoPagamento the dt limite desconto pagamento
     */
    public void setDtLimiteDescontoPagamento(String dtLimiteDescontoPagamento) {
	this.dtLimiteDescontoPagamento = dtLimiteDescontoPagamento;
    }

    /**
     * Get: nrRemessa.
     *
     * @return nrRemessa
     */
    public Long getNrRemessa() {
	return nrRemessa;
    }

    /**
     * Set: nrRemessa.
     *
     * @param nrRemessa the nr remessa
     */
    public void setNrRemessa(Long nrRemessa) {
	this.nrRemessa = nrRemessa;
    }

    /**
     * Get: dataPagamento.
     *
     * @return dataPagamento
     */
    public String getDataPagamento() {
	return dataPagamento;
    }

    /**
     * Set: dataPagamento.
     *
     * @param dataPagamento the data pagamento
     */
    public void setDataPagamento(String dataPagamento) {
	this.dataPagamento = dataPagamento;
    }

    /**
     * Get: descTipoFavorecido.
     *
     * @return descTipoFavorecido
     */
    public String getDescTipoFavorecido() {
	return descTipoFavorecido;
    }

    /**
     * Set: descTipoFavorecido.
     *
     * @param descTipoFavorecido the desc tipo favorecido
     */
    public void setDescTipoFavorecido(String descTipoFavorecido) {
	this.descTipoFavorecido = descTipoFavorecido;
    }

    /**
     * Get: dsTipoBeneficiario.
     *
     * @return dsTipoBeneficiario
     */
    public String getDsTipoBeneficiario() {
	return dsTipoBeneficiario;
    }

    /**
     * Set: dsTipoBeneficiario.
     *
     * @param dsTipoBeneficiario the ds tipo beneficiario
     */
    public void setDsTipoBeneficiario(String dsTipoBeneficiario) {
	this.dsTipoBeneficiario = dsTipoBeneficiario;
    }

    /**
     * Get: cdBancoOriginal.
     *
     * @return cdBancoOriginal
     */
    public Integer getCdBancoOriginal() {
	return cdBancoOriginal;
    }

    /**
     * Set: cdBancoOriginal.
     *
     * @param cdBancoOriginal the cd banco original
     */
    public void setCdBancoOriginal(Integer cdBancoOriginal) {
	this.cdBancoOriginal = cdBancoOriginal;
    }

    /**
     * Get: dsBancoOriginal.
     *
     * @return dsBancoOriginal
     */
    public String getDsBancoOriginal() {
	return dsBancoOriginal;
    }

    /**
     * Set: dsBancoOriginal.
     *
     * @param dsBancoOriginal the ds banco original
     */
    public void setDsBancoOriginal(String dsBancoOriginal) {
	this.dsBancoOriginal = dsBancoOriginal;
    }

    /**
     * Get: cdAgenciaBancariaOriginal.
     *
     * @return cdAgenciaBancariaOriginal
     */
    public Integer getCdAgenciaBancariaOriginal() {
	return cdAgenciaBancariaOriginal;
    }

    /**
     * Set: cdAgenciaBancariaOriginal.
     *
     * @param cdAgenciaBancariaOriginal the cd agencia bancaria original
     */
    public void setCdAgenciaBancariaOriginal(Integer cdAgenciaBancariaOriginal) {
	this.cdAgenciaBancariaOriginal = cdAgenciaBancariaOriginal;
    }

    /**
     * Get: cdDigitoAgenciaOriginal.
     *
     * @return cdDigitoAgenciaOriginal
     */
    public String getCdDigitoAgenciaOriginal() {
	return cdDigitoAgenciaOriginal;
    }

    /**
     * Set: cdDigitoAgenciaOriginal.
     *
     * @param cdDigitoAgenciaOriginal the cd digito agencia original
     */
    public void setCdDigitoAgenciaOriginal(String cdDigitoAgenciaOriginal) {
	this.cdDigitoAgenciaOriginal = cdDigitoAgenciaOriginal;
    }

    /**
     * Get: dsAgenciaOriginal.
     *
     * @return dsAgenciaOriginal
     */
    public String getDsAgenciaOriginal() {
	return dsAgenciaOriginal;
    }

    /**
     * Set: dsAgenciaOriginal.
     *
     * @param dsAgenciaOriginal the ds agencia original
     */
    public void setDsAgenciaOriginal(String dsAgenciaOriginal) {
	this.dsAgenciaOriginal = dsAgenciaOriginal;
    }

    /**
     * Get: cdContaBancariaOriginal.
     *
     * @return cdContaBancariaOriginal
     */
    public Long getCdContaBancariaOriginal() {
	return cdContaBancariaOriginal;
    }

    /**
     * Set: cdContaBancariaOriginal.
     *
     * @param cdContaBancariaOriginal the cd conta bancaria original
     */
    public void setCdContaBancariaOriginal(Long cdContaBancariaOriginal) {
	this.cdContaBancariaOriginal = cdContaBancariaOriginal;
    }

    /**
     * Get: cdDigitoContaOriginal.
     *
     * @return cdDigitoContaOriginal
     */
    public String getCdDigitoContaOriginal() {
	return cdDigitoContaOriginal;
    }

    /**
     * Set: cdDigitoContaOriginal.
     *
     * @param cdDigitoContaOriginal the cd digito conta original
     */
    public void setCdDigitoContaOriginal(String cdDigitoContaOriginal) {
	this.cdDigitoContaOriginal = cdDigitoContaOriginal;
    }

    /**
     * Get: dsTipoContaOriginal.
     *
     * @return dsTipoContaOriginal
     */
    public String getDsTipoContaOriginal() {
	return dsTipoContaOriginal;
    }

    /**
     * Set: dsTipoContaOriginal.
     *
     * @param dsTipoContaOriginal the ds tipo conta original
     */
    public void setDsTipoContaOriginal(String dsTipoContaOriginal) {
	this.dsTipoContaOriginal = dsTipoContaOriginal;
    }

    /**
     * Get: dtDevolucaoEstorno.
     *
     * @return dtDevolucaoEstorno
     */
    public String getDtDevolucaoEstorno() {
	return dtDevolucaoEstorno;
    }

    /**
     * Set: dtDevolucaoEstorno.
     *
     * @param dtDevolucaoEstorno the dt devolucao estorno
     */
    public void setDtDevolucaoEstorno(String dtDevolucaoEstorno) {
	this.dtDevolucaoEstorno = dtDevolucaoEstorno;
    }

    /**
     * Get: dtLimitePagamento.
     *
     * @return dtLimitePagamento
     */
    public String getDtLimitePagamento() {
	return dtLimitePagamento;
    }

    /**
     * Set: dtLimitePagamento.
     *
     * @param dtLimitePagamento the dt limite pagamento
     */
    public void setDtLimitePagamento(String dtLimitePagamento) {
	this.dtLimitePagamento = dtLimitePagamento;
    }

    /**
     * Get: dsRastreado.
     *
     * @return dsRastreado
     */
    public String getDsRastreado() {
	return dsRastreado;
    }

    /**
     * Set: dsRastreado.
     *
     * @param dsRastreado the ds rastreado
     */
    public void setDsRastreado(String dsRastreado) {
	this.dsRastreado = dsRastreado;
    }

    /**
     * Get: dtFloatingPagamento.
     *
     * @return dtFloatingPagamento
     */
    public String getDtFloatingPagamento() {
	return dtFloatingPagamento;
    }

    /**
     * Set: dtFloatingPagamento.
     *
     * @param dtFloatingPagamento the dt floating pagamento
     */
    public void setDtFloatingPagamento(String dtFloatingPagamento) {
	this.dtFloatingPagamento = dtFloatingPagamento;
    }

    /**
     * Get: dtEfetivFloatPgto.
     *
     * @return dtEfetivFloatPgto
     */
    public String getDtEfetivFloatPgto() {
	return dtEfetivFloatPgto;
    }

    /**
     * Set: dtEfetivFloatPgto.
     *
     * @param dtEfetivFloatPgto the dt efetiv float pgto
     */
    public void setDtEfetivFloatPgto(String dtEfetivFloatPgto) {
	this.dtEfetivFloatPgto = dtEfetivFloatPgto;
    }

    /**
     * Get: vlFloatingPagamento.
     *
     * @return vlFloatingPagamento
     */
    public BigDecimal getVlFloatingPagamento() {
	return vlFloatingPagamento;
    }

    /**
     * Set: vlFloatingPagamento.
     *
     * @param vlFloatingPagamento the vl floating pagamento
     */
    public void setVlFloatingPagamento(BigDecimal vlFloatingPagamento) {
	this.vlFloatingPagamento = vlFloatingPagamento;
    }

    /**
     * Get: cdOperacaoDcom.
     *
     * @return cdOperacaoDcom
     */
    public Long getCdOperacaoDcom() {
	return cdOperacaoDcom;
    }

    /**
     * Set: cdOperacaoDcom.
     *
     * @param cdOperacaoDcom the cd operacao dcom
     */
    public void setCdOperacaoDcom(Long cdOperacaoDcom) {
	this.cdOperacaoDcom = cdOperacaoDcom;
    }

    /**
     * Get: dsSituacaoDcom.
     *
     * @return dsSituacaoDcom
     */
    public String getDsSituacaoDcom() {
	return dsSituacaoDcom;
    }

    /**
     * Set: dsSituacaoDcom.
     *
     * @param dsSituacaoDcom the ds situacao dcom
     */
    public void setDsSituacaoDcom(String dsSituacaoDcom) {
	this.dsSituacaoDcom = dsSituacaoDcom;
    }

    /**
     * Get: exibeDcom.
     *
     * @return exibeDcom
     */
    public Boolean getExibeDcom() {
	return exibeDcom;
    }

    /**
     * Set: exibeDcom.
     *
     * @param exibeDcom the exibe dcom
     */
    public void setExibeDcom(Boolean exibeDcom) {
	this.exibeDcom = exibeDcom;
    }

    /**
     * Get: numControleInternoLote.
     *
     * @return numControleInternoLote
     */
    public Long getNumControleInternoLote() {
	return numControleInternoLote;
    }

    /**
     * Set: numControleInternoLote.
     *
     * @param numControleInternoLote the num controle interno lote
     */
    public void setNumControleInternoLote(Long numControleInternoLote) {
	this.numControleInternoLote = numControleInternoLote;
    }

}