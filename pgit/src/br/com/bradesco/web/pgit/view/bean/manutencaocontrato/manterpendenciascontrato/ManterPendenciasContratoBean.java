/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterpendenciascontrato
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaocontrato.manterpendenciascontrato;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.EmpresaGestoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarSituacaoPendContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ListarTipoPendenciaSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.MotivoSituacaoPendenciaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.MotivoSituacaoPendenciaContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoUnidadeOrganizacionalDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ListarContratosPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.listarfuncbradesco.bean.ListarFuncionarioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.IManterPendenciasContratoService;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarListaPendenciasContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarListaPendenciasContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarPendenciaContratoPgitEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ConsultarPendenciaContratoPgitSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ReenviarEmailEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.ReenviarEmailSaidaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.RegistrarPendenciaContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.manterpendenciascontrato.bean.RegistrarPendenciaContratoSaidaDTO;
import br.com.bradesco.web.pgit.view.bean.filtropesquisa.IdentificacaoClienteContratoBean;
import br.com.bradesco.web.pgit.view.utils.PgitFacesUtils;

/**
 * Nome: ManterPendenciasContratoBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class ManterPendenciasContratoBean {

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo manterPendenciasContrato. */
	private IManterPendenciasContratoService manterPendenciasContrato;

	/** Atributo identificacaoClienteContratoBean. */
	private IdentificacaoClienteContratoBean identificacaoClienteContratoBean;

	/* Consulta */

	/** Atributo tipoContrato. */
	private String tipoContrato;

	/** Atributo codPessoaJuridica. */
	private Integer codPessoaJuridica; // codigo da empresa que vem da outra
										// tela

	/** Atributo codPessoaJuridicaContrato. */
										private Integer codPessoaJuridicaContrato; // codigo de contrato que vem da
												// outra tela

	/** Atributo codTipoContrato. */
												private Integer codTipoContrato; // codigo de tipo de contrato q vem da
										// outra tela

	/** Atributo codNumContratoNegocio. */
										private Integer codNumContratoNegocio; // codigo do numero do contrato de
											// negocio q vem da outra tela

	// pendencias
	/** Atributo obrigatoriedade. */
											private String obrigatoriedade;

	/** Atributo cpfCnpjClienteMaster. */
	private String cpfCnpjClienteMaster;

	/** Atributo nomeRazaoClienteMaster. */
	private String nomeRazaoClienteMaster;

	/** Atributo grupoEconomicoClienteMaster. */
	private String grupoEconomicoClienteMaster;

	/** Atributo atividadeEconomicaClienteMaster. */
	private String atividadeEconomicaClienteMaster;

	/** Atributo segmentoClienteMaster. */
	private String segmentoClienteMaster;

	/** Atributo subSegmentoClienteMaster. */
	private String subSegmentoClienteMaster;

	/** Atributo tipo. */
	private String tipo;

	/** Atributo cdTipo. */
	private Integer cdTipo;

	/** Atributo numeroContrato. */
	private String numeroContrato;

	/** Atributo motivoDesc. */
	private String motivoDesc;

	/** Atributo situacaoDesc. */
	private String situacaoDesc;

	/** Atributo participacao. */
	private String participacao;

	/** Atributo possuiAditivos. */
	private String possuiAditivos;

	/** Atributo dataHoraCadastramento. */
	private String dataHoraCadastramento;

	/** Atributo inicioVigencia. */
	private String inicioVigencia;

	/** Atributo empresa. */
	private String empresa;

	/** Atributo codEmpresa. */
	private Long codEmpresa;

	/** Atributo dsDescricaoContrato. */
	private String dsDescricaoContrato;

	/** Atributo habilitaRegistrar. */
	private boolean habilitaRegistrar;

	/** Atributo dataBaixa. */
	private String dataBaixa;

	/** Atributo dsAgenciaOperadora. */
	private String dsAgenciaOperadora;

	/** Atributo gerenteResponsavel. */
	private String gerenteResponsavel;

	/** Atributo listaGridPesquisa. */
	private List<ConsultarListaPendenciasContratoSaidaDTO> listaGridPesquisa;

	/** Atributo listaControleRadio. */
	private List<SelectItem> listaControleRadio = new ArrayList<SelectItem>();

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	/** Atributo tipoBaixa. */
	private Integer tipoBaixa;

	/** Atributo listaTipoUnidadeOrganizacional. */
	private List<SelectItem> listaTipoUnidadeOrganizacional = new ArrayList<SelectItem>();

	/** Atributo listaPendencia. */
	private List<SelectItem> listaPendencia = new ArrayList<SelectItem>();

	/** Atributo listaSituacao. */
	private List<SelectItem> listaSituacao = new ArrayList<SelectItem>();

	/** Atributo listaMotivoBaixa. */
	private List<SelectItem> listaMotivoBaixa = new ArrayList<SelectItem>();

	/** Atributo listaEmpresaGestora. */
	private List<SelectItem> listaEmpresaGestora = new ArrayList<SelectItem>();

	/* Detalhar */
	/** Atributo descricaoResumidaPendencia. */
	private String descricaoResumidaPendencia;

	/** Atributo dsTipoUnidadeOrganizacinal. */
	private String dsTipoUnidadeOrganizacinal;

	/** Atributo dsSituacao. */
	private String dsSituacao;

	/** Atributo dataGeracao. */
	private String dataGeracao;

	/** Atributo dataAtendimento. */
	private String dataAtendimento;

	/** Atributo dsDataBaixa. */
	private String dsDataBaixa;

	/** Atributo codDataBaixa. */
	private int codDataBaixa;

	/** Atributo unidadeOrganizacional. */
	private String unidadeOrganizacional;

	/** Atributo codMotivo. */
	private int codMotivo;

	/** Atributo dsMotivo. */
	private String dsMotivo;

	/** Atributo dsTipo. */
	private String dsTipo;

	/* Trilha de Auditoria */
	/** Atributo dataHoraManutencao. */
	private String dataHoraManutencao;

	/** Atributo usuarioManutencao. */
	private String usuarioManutencao;

	/** Atributo tipoCanalManutencao. */
	private String tipoCanalManutencao;

	/** Atributo complementoManutencao. */
	private String complementoManutencao;

	/** Atributo dataHoraInclusao. */
	private String dataHoraInclusao;

	/** Atributo usuarioInclusao. */
	private String usuarioInclusao;

	/** Atributo tipoCanalInclusao. */
	private String tipoCanalInclusao;

	/** Atributo complementoInclusao. */
	private String complementoInclusao;

	/* Registrar Baixa */
	/** Atributo motivoTipoBaixa. */
	private int motivoTipoBaixa;

	/** Atributo dsMotivoBaixa. */
	private String dsMotivoBaixa;

	/** Atributo listaMotivoBaixaHash. */
	private Map<Integer, String> listaMotivoBaixaHash = new HashMap<Integer, String>();

	/** Atributo radioRecusaAtende. */
	private String radioRecusaAtende;

	/** Atributo diaDataAtendimento. */
	private String diaDataAtendimento;

	/** Atributo mesDataAtendimento. */
	private String mesDataAtendimento;

	/** Atributo anoDataAtendimento. */
	private String anoDataAtendimento;
	
	// ** tratando hora ** //
	
	/** Atributo dataHoraGeracao. */
	private String dataHoraGeracao;
	
	/** Atributo de verifica��o de duplicidade no code de erro*/
	private Boolean verificaerrorcode;
	

	/* M�todos */

	/**
	 * Get: dataHoraGeracao.
	 *
	 * @return dataHoraGeracao
	 */
	public String getDataHoraGeracao() {
		return dataHoraGeracao;
	}

	/**
	 * Set: dataHoraGeracao.
	 *
	 * @param dataHoraGeracao the data hora geracao
	 */
	public void setDataHoraGeracao(String dataHoraGeracao) {
		this.dataHoraGeracao = dataHoraGeracao;
	}

	/**
	 * Inicia tela.
	 *
	 * @param evt the evt
	 */
	public void iniciaTela(ActionEvent evt) {

		this.identificacaoClienteContratoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		this.identificacaoClienteContratoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		this.identificacaoClienteContratoBean
				.setFuncionarioDTO(new ListarFuncionarioSaidaDTO());

		// carrega os combos da tela da primeira pesquisa
		identificacaoClienteContratoBean.listarEmpresaGestora();
		identificacaoClienteContratoBean.listarTipoContrato();
		identificacaoClienteContratoBean.listarSituacaoContrato();

		identificacaoClienteContratoBean.setHabilitaFiltroDeCliente(false);
		identificacaoClienteContratoBean
				.setPaginaCliente("identificacaoClientePendenciasContrato");
		identificacaoClienteContratoBean
				.setPaginaRetorno("conManterPendenciasContrato");
		identificacaoClienteContratoBean.setItemClienteSelecionado(null);
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);

		this.identificacaoClienteContratoBean.setItemFiltroSelecionado("");
		this.identificacaoClienteContratoBean.setBloqueiaRadio(false);

		identificacaoClienteContratoBean.limparDadosPesquisaContrato();
		identificacaoClienteContratoBean.limparDadosCliente();
		identificacaoClienteContratoBean.setObrigatoriedade("T");
		identificacaoClienteContratoBean.setHabilitaEmpresaGestoraTipoContrato(true);
		identificacaoClienteContratoBean.setEmpresaGestoraFiltro(2269651L);
		
		this.setHabilitaRegistrar(false);

	}

	/**
	 * Carrega lista pendencia.
	 *
	 * @return the list< select item>
	 */
	public List<SelectItem> carregaListaPendencia() {

		listaPendencia = new ArrayList<SelectItem>();
		List<ListarTipoPendenciaSaidaDTO> listaPendenciaAux;

		try{
			
			listaPendenciaAux = comboService.listarTipoPendencia();
			for (int i = 0; i < listaPendenciaAux.size(); i++) {
				this.listaPendencia
						.add(new SelectItem(String.valueOf(listaPendenciaAux.get(i)
								.getCodPendenciaPagamentoIntegrado()),
								listaPendenciaAux.get(i)
										.getDsPendenciaPagamentoIntegrado()));
			}			
		}
		catch (PdcAdapterFunctionalException p) {
			if (!getVerificaerrorcode()){
				BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "
					+ p.getMessage(), false);
				setVerificaerrorcode(true);
			}
		}		

		return this.listaPendencia;
	}

	/**
	 * Listar empresa gestora.
	 */
	public void listarEmpresaGestora() {

		this.listaEmpresaGestora = new ArrayList<SelectItem>();
		List<EmpresaGestoraSaidaDTO> list = new ArrayList<EmpresaGestoraSaidaDTO>();
		
		try{
    		list = comboService.listarEmpresasGestoras();
    		listaEmpresaGestora.clear();
    		for (EmpresaGestoraSaidaDTO combo : list) {
    			listaEmpresaGestora.add(new SelectItem(combo
    					.getCdPessoaJuridicaContrato(), combo.getDsCnpj()));
    		}
		}catch (PdcAdapterFunctionalException p) {
            if (!getVerificaerrorcode()){
                BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "
                    + p.getMessage(), false);
                setVerificaerrorcode(true);
            }
        }       
	}

	/**
	 * Carrega lista situacao.
	 *
	 * @return the list< select item>
	 */
	public List<SelectItem> carregaListaSituacao() {

		listaSituacao = new ArrayList<SelectItem>();
		List<ListarSituacaoPendContratoSaidaDTO> listaSituacaoAux;

		try{
    		listaSituacaoAux = comboService.listarSituacaoPendContrato();
    
    		for (int i = 0; i < listaSituacaoAux.size(); i++) {
    			this.listaSituacao.add(new SelectItem(String
    					.valueOf(listaSituacaoAux.get(i)
    							.getCodSituacaoPendenciaContrato()),
    					listaSituacaoAux.get(i).getDsSituacaoPendenciaContrato()));
    		}
		}catch (PdcAdapterFunctionalException p) {
            if (!getVerificaerrorcode()){
                BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "
                    + p.getMessage(), false);
                setVerificaerrorcode(true);
            }
        }       
		
		return this.listaSituacao;

	}

	/**
	 * Carrega lista motivo baixa.
	 *
	 * @return the list< select item>
	 */
	public List<SelectItem> carregaListaMotivoBaixa() {

		listaMotivoBaixa = new ArrayList<SelectItem>();
		List<MotivoSituacaoPendenciaContratoSaidaDTO> listaMotivoBaixaAux;
		listaMotivoBaixaHash.clear();

		MotivoSituacaoPendenciaContratoEntradaDTO motivoSituacaoPendenciaContratoEntradaDTO = new MotivoSituacaoPendenciaContratoEntradaDTO();

		// Conforme nova especifica��o (email enviado por Guilherme Fernandes -
		// quinta-feira, 28 de junho de 2012
		// 11:30 ) o combo sera carregado com valor 1 quando o radio selecionado
		// for o de Atende, quando o radio selecionado for o de Recusa
		// o como sera carregado com valor 4.

		if ("0".equals(radioRecusaAtende)) {
			motivoSituacaoPendenciaContratoEntradaDTO
					.setCdSituacaoPendenciaContrato(1);
		} else if ("1".equals(radioRecusaAtende)) {
			motivoSituacaoPendenciaContratoEntradaDTO
					.setCdSituacaoPendenciaContrato(4);
		} else {
			motivoSituacaoPendenciaContratoEntradaDTO
					.setCdSituacaoPendenciaContrato(0);
		}

		listaMotivoBaixaAux = comboService
				.listarMotivoSituacaoPendContrato(motivoSituacaoPendenciaContratoEntradaDTO);

		for (int i = 0; i < listaMotivoBaixaAux.size(); i++) {
			listaMotivoBaixaHash.put(listaMotivoBaixaAux.get(i)
					.getCdMotivoSituacaoPendencia(), listaMotivoBaixaAux.get(i)
					.getDsMotivoSituacaoPendencia());
			this.listaMotivoBaixa.add(new SelectItem(String
					.valueOf(listaMotivoBaixaAux.get(i)
							.getCdMotivoSituacaoPendencia()),
					listaMotivoBaixaAux.get(i).getDsMotivoSituacaoPendencia()));
		}

		return this.listaMotivoBaixa;
	}

	/**
	 * Recusa atende.
	 */
	public void recusaAtende() {

		GregorianCalendar calendar = new GregorianCalendar();
		int dia = calendar.get(GregorianCalendar.DAY_OF_MONTH);
		int mes = calendar.get(GregorianCalendar.MONTH) + 1;
		int ano = calendar.get(GregorianCalendar.YEAR);

		setDiaDataAtendimento(String.valueOf(dia));
		setMesDataAtendimento(String.valueOf(mes));
		setAnoDataAtendimento(String.valueOf(ano));
		setMotivoTipoBaixa(0);

		carregaListaMotivoBaixa();
		
		setMotivoTipoBaixa(3);

		/*
		 * if(getRadioRecusaAtende().equals("1")){ carregaListaMotivoBaixa();
		 * }else getListaMotivoBaixa().clear();
		 * 
		 * }
		 */
	}

	/**
	 * Limpar dados registrar.
	 */
	public void limparDadosRegistrar() {
		this.setDataAtendimento("");
		this.setMotivoTipoBaixa(0);
		this.setDsMotivoBaixa("");
		setVerificaerrorcode(false);
		
		GregorianCalendar calendar = new GregorianCalendar();
		int dia = calendar.get(GregorianCalendar.DAY_OF_MONTH);
		int mes = calendar.get(GregorianCalendar.MONTH) + 1;
		int ano = calendar.get(GregorianCalendar.YEAR);

		setDiaDataAtendimento(String.valueOf(dia));
		setMesDataAtendimento(String.valueOf(mes));
		setAnoDataAtendimento(String.valueOf(ano));

	}

	/**
	 * Carrega lista tipo unidade organizacional.
	 *
	 * @return the list< select item>
	 */
	public List<SelectItem> carregaListaTipoUnidadeOrganizacional() {

		listaTipoUnidadeOrganizacional = new ArrayList<SelectItem>();
		List<TipoUnidadeOrganizacionalDTO> listaTipoUnidadeOrganizacionalAux;

		TipoUnidadeOrganizacionalDTO tipoUnidadeOrganizacionalDTO = new TipoUnidadeOrganizacionalDTO();
		tipoUnidadeOrganizacionalDTO.setCdSituacaoVinculacaoConta(1);
		
		try{
    		listaTipoUnidadeOrganizacionalAux = comboService
    				.listarTipoUnidadeOrganizacional(tipoUnidadeOrganizacionalDTO);
    
    		for (int i = 0; i < listaTipoUnidadeOrganizacionalAux.size(); i++) {
    			this.listaTipoUnidadeOrganizacional
    					.add(new SelectItem(String
    							.valueOf(listaTipoUnidadeOrganizacionalAux.get(i)
    									.getCdTipoUnidade()),
    							listaTipoUnidadeOrganizacionalAux.get(i)
    									.getDsTipoUnidade()));
    		}
		}catch (PdcAdapterFunctionalException p) {
            if (!getVerificaerrorcode()){
                BradescoFacesUtils.addInfoModalMessage("(" + StringUtils.right(p.getCode(), 8) + ") "
                    + p.getMessage(), false);
                setVerificaerrorcode(true);
            }
        }       

		return this.listaTipoUnidadeOrganizacional;
	}

	/**
	 * Carrega lista tipo baixa.
	 */
	public void carregaListaTipoBaixa() {

	}

	/**
	 * Carrega lista.
	 */
	public void carregaLista() {

		try {

			listaGridPesquisa = new ArrayList<ConsultarListaPendenciasContratoSaidaDTO>();

			ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
					.getListaGridPesquisa().get(
							identificacaoClienteContratoBean
									.getItemSelecionadoLista());

			ConsultarListaPendenciasContratoEntradaDTO entradaDTO = new ConsultarListaPendenciasContratoEntradaDTO();

			/* Dados do Contrato */
			entradaDTO.setNumSequenciaContratoNegocio(saidaDTO
					.getNrSequenciaContrato());
			entradaDTO.setCodPessoaJuridicaContrato(Integer.parseInt(String
					.valueOf(saidaDTO.getCdPessoaJuridica())));
			entradaDTO.setCodTipoContratoNegocio(Integer.parseInt(String
					.valueOf(saidaDTO.getCdTipoContrato())));

			entradaDTO.setCodPendenciaPagamentoIntegrado(0);
			entradaDTO.setCodPessoaJuridica(0);
			entradaDTO.setCodSituacaoPendenciaContrato(0);
			entradaDTO.setCodTipoBaixa(0);
			entradaDTO.setNumSequenciaUnidadeOrg(0);
			entradaDTO.setCdTipoUnidade(0);
			entradaDTO.setCodAcesso(0);
			entradaDTO.setDataInicioBaixa("");
			entradaDTO.setDataFimBaixa("");

			setListaGridPesquisa(getManterPendenciasContrato()
					.consultarListaPendenciaContrato(entradaDTO));

			listaControleRadio = new ArrayList<SelectItem>();

			for (int i = 0; i < listaGridPesquisa.size(); i++) {
				listaControleRadio.add(new SelectItem(i, ""));
			}

			setItemSelecionadoLista(null);

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setItemSelecionadoLista(null);
			setListaGridPesquisa(null);
			setVerificaerrorcode(true);
		}

	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		return "ok";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		armazenarDataHoraGeracao();
		try {
			preencheDados();
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), "conManterPendenciasContrato2",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "DETALHAR";

	}

	/**
	 * Limpar tela.
	 *
	 * @return the string
	 */
	public String limparTela() {

		setItemSelecionadoLista(null);
		setHabilitaRegistrar(false);
		return "LIMPAR";

	}

	/**
	 * Armazenar data hora geracao.
	 */
	public void armazenarDataHoraGeracao(){
		if(listaGridPesquisa!=null)
			if(listaGridPesquisa.get(itemSelecionadoLista)!= null){
				dataHoraGeracao = listaGridPesquisa.get(itemSelecionadoLista).getDataHoraGeracaoFormatada();
		}
	}
	
	/**
	 * Registrar baixa.
	 *
	 * @return the string
	 */
	public String registrarBaixa() {
		armazenarDataHoraGeracao();
		preencheDados();
		limparDadosRegistrar();
		try {
			carregaListaMotivoBaixa();
		} catch (PdcAdapterFunctionalException p) {
			listaMotivoBaixa = new ArrayList<SelectItem>();
		}
		
		return "REGBAIXA";
	}

	/**
	 * Voltar consulta.
	 *
	 * @return the string
	 */
	public String voltarConsulta() {
		identificacaoClienteContratoBean.setItemSelecionadoLista(null);
		
		identificacaoClienteContratoBean.consultarContrato();
		
		return "VOLTAR";
	}

	/**
	 * Habilita registrar.
	 *
	 * @return the string
	 */
	public String habilitaRegistrar() {
		ConsultarListaPendenciasContratoSaidaDTO consultarListaPendenciasContratoSaidaDTO = getListaGridPesquisa()
				.get(getItemSelecionadoLista());

		if (consultarListaPendenciasContratoSaidaDTO.getCodTipoBaixa() == 1) {
			setHabilitaRegistrar(true);
		} else {
			setHabilitaRegistrar(false);
		}
		return "";
	}

	/**
	 * Voltar consulta pendencia.
	 *
	 * @return the string
	 */
	public String voltarConsultaPendencia() {

		GregorianCalendar calendar = new GregorianCalendar();
		int dia = calendar.get(GregorianCalendar.DAY_OF_MONTH);
		int mes = calendar.get(GregorianCalendar.MONTH) + 1;
		int ano = calendar.get(GregorianCalendar.YEAR);

		setDiaDataAtendimento(String.valueOf(dia));
		setMesDataAtendimento(String.valueOf(mes));
		setAnoDataAtendimento(String.valueOf(ano));
		setRadioRecusaAtende("");
		setMotivoTipoBaixa(0);
		setItemSelecionadoLista(null);
		setHabilitaRegistrar(false);
		setDataHoraGeracao("");
		return "VOLTAR_CONSULTA";
	}

	/**
	 * Limpar tela registrar baixa.
	 */
	public void limparTelaRegistrarBaixa() {

		GregorianCalendar calendar = new GregorianCalendar();
		int dia = calendar.get(GregorianCalendar.DAY_OF_MONTH);
		int mes = calendar.get(GregorianCalendar.MONTH) + 1;
		int ano = calendar.get(GregorianCalendar.YEAR);

		setDiaDataAtendimento(String.valueOf(dia));
		setMesDataAtendimento(String.valueOf(mes));
		setAnoDataAtendimento(String.valueOf(ano));
		this.setRadioRecusaAtende("");
		this.setMotivoTipoBaixa(0);
	}

	/**
	 * Confirmar.
	 *
	 * @return the string
	 */
	public String confirmar() {
		SimpleDateFormat simpleDataFormat = new SimpleDateFormat("dd/MM/yyyy");

		setDataBaixa(simpleDataFormat.format(new Date()));
		setDsMotivoBaixa((String) getListaMotivoBaixaHash().get(
				getMotivoTipoBaixa()));
		setDataAtendimento(getDiaDataAtendimento() + "/"
				+ getMesDataAtendimento() + "/" + getAnoDataAtendimento());

		return "CONFIRMAR";
	}

	/**
	 * Voltar registrar baixa.
	 *
	 * @return the string
	 */
	public String voltarRegistrarBaixa() {
		return "VOLTAR_REGBAIXA";
	}

	/**
	 * Confirmar registro.
	 *
	 * @return the string
	 */
	public String confirmarRegistro() {

		try {

			RegistrarPendenciaContratoEntradaDTO registrarPendenciaContratoEntradaDTO = new RegistrarPendenciaContratoEntradaDTO();
			ConsultarListaPendenciasContratoSaidaDTO consultarListaPendenciasContratoSaidaDTO = getListaGridPesquisa()
					.get(getItemSelecionadoLista());

			/* Dados da Pendencia */

			registrarPendenciaContratoEntradaDTO
					.setCdPendenciaPagamentoIntegrado(Integer.parseInt(String
							.valueOf(consultarListaPendenciasContratoSaidaDTO
									.getCodPendenciaPagInt())));

			// Conforme email de Ot�vio Tonelli Anchieta enviado dia 22 de julho
			// de 2010 o campo
			// codSituacaoPendenciaContrato recebe o valor do radio selecionado

			if (getRadioRecusaAtende() != null
					&& getRadioRecusaAtende().equals("0")) {
				registrarPendenciaContratoEntradaDTO
						.setCodSituacaoPendenciaContrato(1);
			} else {
				registrarPendenciaContratoEntradaDTO
						.setCodSituacaoPendenciaContrato(2);
			}

			registrarPendenciaContratoEntradaDTO
					.setCodTipoBaixaPendencia(consultarListaPendenciasContratoSaidaDTO
							.getCodTipoBaixa());
			registrarPendenciaContratoEntradaDTO
					.setNrPendenciaPagamentoIntegrado(Integer.parseInt(String
							.valueOf(consultarListaPendenciasContratoSaidaDTO
									.getNumPendenciaContratoNeg())));

			/* Dados do Registro */

			if (getRadioRecusaAtende() != null
					&& getRadioRecusaAtende().equals("0")) {
				registrarPendenciaContratoEntradaDTO
						.setDtAtendimentoPendenciaContrato(getDtAtendPdc());
				registrarPendenciaContratoEntradaDTO
						.setCodMotivoBaixaPendencia(getMotivoTipoBaixa());
			}
			if (getRadioRecusaAtende() != null
					&& getRadioRecusaAtende().equals("1")) {
				registrarPendenciaContratoEntradaDTO
						.setDtAtendimentoPendenciaContrato(getDtAtendPdc());
				registrarPendenciaContratoEntradaDTO
						.setCodMotivoBaixaPendencia(getMotivoTipoBaixa());
			}

			/* Dados Contratos */
			ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
					.getListaGridPesquisa().get(
							identificacaoClienteContratoBean
									.getItemSelecionadoLista());

			/* Dados do Contrato */
			registrarPendenciaContratoEntradaDTO
					.setCdPessoaJuridicaContrato(Integer.parseInt(String
							.valueOf(saidaDTO.getCdPessoaJuridica())));
			registrarPendenciaContratoEntradaDTO
					.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
			registrarPendenciaContratoEntradaDTO
					.setNrSequenciaContratoNegocio(Integer.parseInt(String
							.valueOf(saidaDTO.getNrSequenciaContrato())));

			RegistrarPendenciaContratoSaidaDTO registrarPendenciaContratoSaidaDTO = getManterPendenciasContrato()
					.registrarPendenciaContrato(
							registrarPendenciaContratoEntradaDTO);
			BradescoFacesUtils.addInfoModalMessage("("
					+ registrarPendenciaContratoSaidaDTO.getCodMensagem()
					+ ") " + registrarPendenciaContratoSaidaDTO.getMensagem(),
					"conManterPendenciasContrato2",
					BradescoViewExceptionActionType.ACTION, false);

			// carregarGrid();
			setItemSelecionadoLista(null);
			setHabilitaRegistrar(false);
			listaGridPesquisa.clear();
			carregaLista();
			setRadioRecusaAtende("");
			limparDadosRegistrar();

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}
		setDataHoraGeracao("");
		return "CONFIRMAR";
	}

	/**
	 * Pendencias.
	 *
	 * @return the string
	 */
	public String pendencias() {

		setVerificaerrorcode(false);
		limparDadosRegistrar();
		
		limparTelaRegistrarBaixa();

		// carrega combos
		carregaListaPendencia();
		carregaListaSituacao();
		carregaListaTipoBaixa();
		carregaListaTipoUnidadeOrganizacional();
		listarEmpresaGestora();
		carregaLista();
		
		if (getVerificaerrorcode()){
            return "";
        }

		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean
		.getListaGridPesquisa().get(
				identificacaoClienteContratoBean
				.getItemSelecionadoLista());

		setCpfCnpjClienteMaster(saidaDTO.getCnpjOuCpfFormatado());
		setNomeRazaoClienteMaster(saidaDTO.getNmRazaoSocialRepresentante());
		setGrupoEconomicoClienteMaster(saidaDTO.getDsGrupoEconomico());
		setAtividadeEconomicaClienteMaster(saidaDTO.getDsAtividadeEconomica());
		setSegmentoClienteMaster(saidaDTO.getDsSegmentoCliente());
		setSubSegmentoClienteMaster(saidaDTO.getDsSubSegmentoCliente());
		setEmpresa(String.valueOf(saidaDTO.getDsPessoaJuridica()));
		setCodEmpresa(saidaDTO.getCdPessoaJuridica());
		setTipo(saidaDTO.getDsTipoContrato());
		setCdTipo(saidaDTO.getCdTipoContrato());
		setNumeroContrato(String.valueOf(saidaDTO.getNrSequenciaContrato()));
		setSituacaoDesc(saidaDTO.getDsSituacaoContrato());
		setMotivoDesc(saidaDTO.getDsMotivoSituacao());
		setParticipacao(saidaDTO.getCdTipoParticipacao());
		setPossuiAditivos(saidaDTO.getCdAditivo());
		setDsDescricaoContrato(saidaDTO.getDsContrato());
		setDsAgenciaOperadora(saidaDTO.getDsAgenciaOperadora());
		setGerenteResponsavel(saidaDTO.getCdFuncionarioBradesco() + " - "
				+ saidaDTO.getDescFuncionarioBradesco());

		// setDataHoraCadastramento(null);

		if (getDataHoraCadastramento() != null) {

			SimpleDateFormat formato1 = new SimpleDateFormat(
					"yyyy-MM-dd-HH.mm.ss");
			SimpleDateFormat formato2 = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");

			String stringData = getDataHoraCadastramento().substring(0, 19);

			try {
				setDataHoraCadastramento(formato2.format(formato1
						.parse(stringData)));
			} catch (ParseException e) {
				setDataHoraCadastramento("");
			}

		}
		return "PENDENCIAS";
	}

	/*
	 * private void limparGridPendencias(){
	 * 
	 * 
	 * setListaGridPesquisa(null); setItemSelecionadoLista(null);
	 * 
	 * }
	 */

	/**
	 * Preenche dados.
	 */
	private void preencheDados() {

		ConsultarListaPendenciasContratoSaidaDTO consultarListaPendenciasContratoSaidaDTO = getListaGridPesquisa()
				.get(getItemSelecionadoLista());
		ConsultarPendenciaContratoPgitEntradaDTO entradaDTO = new ConsultarPendenciaContratoPgitEntradaDTO(); // tela
		// Detalhar

		entradaDTO.setCodPendenciaPagamentoIntegrado(consultarListaPendenciasContratoSaidaDTO
						.getCodPendenciaPagInt());
		entradaDTO.setNrPendenciaContratoNegocio(Integer.parseInt(String
				.valueOf(consultarListaPendenciasContratoSaidaDTO
						.getNumPendenciaContratoNeg())));

		/* Dados do Contrato */
		ListarContratosPgitSaidaDTO saidaDTOContrato = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

		entradaDTO.setCodPessoaJuridicaContrato(saidaDTOContrato.getCdPessoaJuridica());
		entradaDTO.setCodTipoContratoNegocio(saidaDTOContrato
				.getCdTipoContrato());
		entradaDTO.setNrSeqContratoNegocio(saidaDTOContrato.getNrSequenciaContrato());

		/* Dados de Saida */
		ConsultarPendenciaContratoPgitSaidaDTO saidaDTO = getManterPendenciasContrato().consultarPendenciaContratoPgit(entradaDTO);

		setDsTipoUnidadeOrganizacinal(consultarListaPendenciasContratoSaidaDTO
				.getDsTipoUnidOrg());
		setUnidadeOrganizacional(consultarListaPendenciasContratoSaidaDTO
				.getDsUnidadeOrg());
		setDsSituacao(saidaDTO.getDsSituacaoPendenciaContrato());
		setDataAtendimento(saidaDTO.getDataAtendimentoPendenciaContrato());
		setDescricaoResumidaPendencia(saidaDTO.getDsPendenciaPagIntegrado());
		setDataGeracao(consultarListaPendenciasContratoSaidaDTO
				.getDataGeracaoPendencia());
		setDsDataBaixa(saidaDTO.getHoraBaixaPendenciaContrato());
		setTipoBaixa(consultarListaPendenciasContratoSaidaDTO.getCodTipoBaixa());
		setDsMotivo(saidaDTO.getCodMotivoSituacaoPendencia());
		setDsTipo(consultarListaPendenciasContratoSaidaDTO
				.getCodPendenciaPagInt()
				+ " - " + saidaDTO.getDsPendenciaPagIntegrado());

		/* Trilha de Auditoria */

		setDataHoraManutencao(saidaDTO.getHorarioManutencaoRegistro());
		setDataHoraInclusao(saidaDTO.getHoraInclusaoRegistro());

		setTipoCanalInclusao(saidaDTO.getCodTipoCanalInclusao() != 0 ? saidaDTO
				.getCodTipoCanalInclusao()
				+ " - " + saidaDTO.getDsCanalInclusao() : "");
		setTipoCanalManutencao(saidaDTO.getCodTipoCanalManutencao() == 0 ? ""
				: saidaDTO.getCodTipoCanalManutencao() + " - "
						+ saidaDTO.getDsCanalManutencao());

		setUsuarioInclusao(saidaDTO.getCodUsuarioInclusao());
		setUsuarioManutencao(saidaDTO.getCodUsuarioManutencao());
		setComplementoInclusao(saidaDTO.getComplementoInclusaoFormatado());
		setComplementoManutencao(saidaDTO.getComplementoManutencaoFormatado());
	}

	/**
	 * Reenviar email.
	 */
	public void reenviarEmail() {
		ListarContratosPgitSaidaDTO saidaDTO = identificacaoClienteContratoBean.getListaGridPesquisa().get(identificacaoClienteContratoBean.getItemSelecionadoLista());

		ReenviarEmailEntradaDTO entradaDTO = new ReenviarEmailEntradaDTO();
		entradaDTO.setNrSequenciaContratoNegocio(saidaDTO.getNrSequenciaContrato());
		entradaDTO.setCdpessoaJuridicaContrato(saidaDTO.getCdPessoaJuridica());
		entradaDTO.setCdTipoContratoNegocio(saidaDTO.getCdTipoContrato());
		entradaDTO.setCdPendenciaPagamentoIntegrado(listaGridPesquisa.get(itemSelecionadoLista).getCodPendenciaPagInt());
		entradaDTO.setNrPendenciaContratoNegocio(listaGridPesquisa.get(itemSelecionadoLista).getNumPendenciaContratoNeg());

		ReenviarEmailSaidaDTO retorno = manterPendenciasContrato.reenviarEmail(entradaDTO);

		if("PGIT1099".equals(retorno.getCodMensagem())) {
			PgitFacesUtils.addInfoModalMessage(retorno.getCodMensagem(), retorno.getMensagem());
		}
	}

	/**
	 * Is desabilita reenviar email.
	 *
	 * @return true, if is desabilita reenviar email
	 */
	public boolean isDesabilitaReenviarEmail() {
		if (itemSelecionadoLista == null) {
			return true;
		} else {
			Long cdPendencia = null;
			Integer cdSituacaoPendencia = null;
			cdPendencia = listaGridPesquisa.get(itemSelecionadoLista)
					.getCodPendenciaPagInt();
			cdSituacaoPendencia = listaGridPesquisa.get(itemSelecionadoLista).getCodSituacaoPendContrato();

			if ((cdPendencia == 8 || cdPendencia == 9 || cdPendencia == 10
					|| cdPendencia == 11) && (cdSituacaoPendencia == 3)) {
				return false;
			} else {
				return true;
			}
		}

	}

	/* Getters e Setters */

	/**
	 * Get: atividadeEconomicaClienteMaster.
	 *
	 * @return atividadeEconomicaClienteMaster
	 */
	public String getAtividadeEconomicaClienteMaster() {
		return atividadeEconomicaClienteMaster;
	}

	/**
	 * Set: atividadeEconomicaClienteMaster.
	 *
	 * @param atividadeEconomicaClienteMaster the atividade economica cliente master
	 */
	public void setAtividadeEconomicaClienteMaster(
			String atividadeEconomicaClienteMaster) {
		this.atividadeEconomicaClienteMaster = atividadeEconomicaClienteMaster;
	}

	/**
	 * Get: cpfCnpjClienteMaster.
	 *
	 * @return cpfCnpjClienteMaster
	 */
	public String getCpfCnpjClienteMaster() {
		return cpfCnpjClienteMaster;
	}

	/**
	 * Set: cpfCnpjClienteMaster.
	 *
	 * @param cpfCnpjClienteMaster the cpf cnpj cliente master
	 */
	public void setCpfCnpjClienteMaster(String cpfCnpjClienteMaster) {
		this.cpfCnpjClienteMaster = cpfCnpjClienteMaster;
	}

	/**
	 * Get: codEmpresa.
	 *
	 * @return codEmpresa
	 */
	public Long getCodEmpresa() {
		return codEmpresa;
	}

	/**
	 * Set: codEmpresa.
	 *
	 * @param codEmpresa the cod empresa
	 */
	public void setCodEmpresa(Long codEmpresa) {
		this.codEmpresa = codEmpresa;
	}

	/**
	 * Get: empresa.
	 *
	 * @return empresa
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * Set: empresa.
	 *
	 * @param empresa the empresa
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * Get: grupoEconomicoClienteMaster.
	 *
	 * @return grupoEconomicoClienteMaster
	 */
	public String getGrupoEconomicoClienteMaster() {
		return grupoEconomicoClienteMaster;
	}

	/**
	 * Set: grupoEconomicoClienteMaster.
	 *
	 * @param grupoEconomicoClienteMaster the grupo economico cliente master
	 */
	public void setGrupoEconomicoClienteMaster(
			String grupoEconomicoClienteMaster) {
		this.grupoEconomicoClienteMaster = grupoEconomicoClienteMaster;
	}

	/**
	 * Get: listaPendencia.
	 *
	 * @return listaPendencia
	 */
	public List<SelectItem> getListaPendencia() {
		return listaPendencia;
	}

	/**
	 * Set: listaPendencia.
	 *
	 * @param listaPendencia the lista pendencia
	 */
	public void setListaPendencia(List<SelectItem> listaPendencia) {
		this.listaPendencia = listaPendencia;
	}

	/**
	 * Get: listaSituacao.
	 *
	 * @return listaSituacao
	 */
	public List<SelectItem> getListaSituacao() {
		return listaSituacao;
	}

	/**
	 * Set: listaSituacao.
	 *
	 * @param listaSituacao the lista situacao
	 */
	public void setListaSituacao(List<SelectItem> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}

	/**
	 * Get: nomeRazaoClienteMaster.
	 *
	 * @return nomeRazaoClienteMaster
	 */
	public String getNomeRazaoClienteMaster() {
		return nomeRazaoClienteMaster;
	}

	/**
	 * Set: nomeRazaoClienteMaster.
	 *
	 * @param nomeRazaoClienteMaster the nome razao cliente master
	 */
	public void setNomeRazaoClienteMaster(String nomeRazaoClienteMaster) {
		this.nomeRazaoClienteMaster = nomeRazaoClienteMaster;
	}

	/**
	 * Get: numeroContrato.
	 *
	 * @return numeroContrato
	 */
	public String getNumeroContrato() {
		return numeroContrato;
	}

	/**
	 * Set: numeroContrato.
	 *
	 * @param numeroContrato the numero contrato
	 */
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	/**
	 * Get: obrigatoriedade.
	 *
	 * @return obrigatoriedade
	 */
	public String getObrigatoriedade() {
		return obrigatoriedade;
	}

	/**
	 * Set: obrigatoriedade.
	 *
	 * @param obrigatoriedade the obrigatoriedade
	 */
	public void setObrigatoriedade(String obrigatoriedade) {
		this.obrigatoriedade = obrigatoriedade;
	}

	/**
	 * Get: segmentoClienteMaster.
	 *
	 * @return segmentoClienteMaster
	 */
	public String getSegmentoClienteMaster() {
		return segmentoClienteMaster;
	}

	/**
	 * Set: segmentoClienteMaster.
	 *
	 * @param segmentoClienteMaster the segmento cliente master
	 */
	public void setSegmentoClienteMaster(String segmentoClienteMaster) {
		this.segmentoClienteMaster = segmentoClienteMaster;
	}

	/**
	 * Get: subSegmentoClienteMaster.
	 *
	 * @return subSegmentoClienteMaster
	 */
	public String getSubSegmentoClienteMaster() {
		return subSegmentoClienteMaster;
	}

	/**
	 * Set: subSegmentoClienteMaster.
	 *
	 * @param subSegmentoClienteMaster the sub segmento cliente master
	 */
	public void setSubSegmentoClienteMaster(String subSegmentoClienteMaster) {
		this.subSegmentoClienteMaster = subSegmentoClienteMaster;
	}

	/**
	 * Get: tipoContrato.
	 *
	 * @return tipoContrato
	 */
	public String getTipoContrato() {
		return tipoContrato;
	}

	/**
	 * Set: tipoContrato.
	 *
	 * @param tipoContrato the tipo contrato
	 */
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: listaTipoUnidadeOrganizacional.
	 *
	 * @return listaTipoUnidadeOrganizacional
	 */
	public List<SelectItem> getListaTipoUnidadeOrganizacional() {
		return listaTipoUnidadeOrganizacional;
	}

	/**
	 * Set: listaTipoUnidadeOrganizacional.
	 *
	 * @param listaTipoUnidadeOrganizacional the lista tipo unidade organizacional
	 */
	public void setListaTipoUnidadeOrganizacional(
			List<SelectItem> listaTipoUnidadeOrganizacional) {
		this.listaTipoUnidadeOrganizacional = listaTipoUnidadeOrganizacional;
	}

	/**
	 * Get: manterPendenciasContrato.
	 *
	 * @return manterPendenciasContrato
	 */
	public IManterPendenciasContratoService getManterPendenciasContrato() {
		return manterPendenciasContrato;
	}

	/**
	 * Set: manterPendenciasContrato.
	 *
	 * @param manterPendenciasContrato the manter pendencias contrato
	 */
	public void setManterPendenciasContrato(
			IManterPendenciasContratoService manterPendenciasContrato) {
		this.manterPendenciasContrato = manterPendenciasContrato;
	}

	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<ConsultarListaPendenciasContratoSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(
			List<ConsultarListaPendenciasContratoSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: codPessoaJuridica.
	 *
	 * @return codPessoaJuridica
	 */
	public Integer getCodPessoaJuridica() {
		return codPessoaJuridica;
	}

	/**
	 * Set: codPessoaJuridica.
	 *
	 * @param codPessoaJuridica the cod pessoa juridica
	 */
	public void setCodPessoaJuridica(Integer codPessoaJuridica) {
		this.codPessoaJuridica = codPessoaJuridica;
	}

	/**
	 * Get: codPessoaJuridicaContrato.
	 *
	 * @return codPessoaJuridicaContrato
	 */
	public Integer getCodPessoaJuridicaContrato() {
		return codPessoaJuridicaContrato;
	}

	/**
	 * Set: codPessoaJuridicaContrato.
	 *
	 * @param codPessoaJuridicaContrato the cod pessoa juridica contrato
	 */
	public void setCodPessoaJuridicaContrato(Integer codPessoaJuridicaContrato) {
		this.codPessoaJuridicaContrato = codPessoaJuridicaContrato;
	}

	/**
	 * Get: codTipoContrato.
	 *
	 * @return codTipoContrato
	 */
	public Integer getCodTipoContrato() {
		return codTipoContrato;
	}

	/**
	 * Get: motivoTipoBaixa.
	 *
	 * @return motivoTipoBaixa
	 */
	public int getMotivoTipoBaixa() {
		return motivoTipoBaixa;
	}

	/**
	 * Set: motivoTipoBaixa.
	 *
	 * @param motivoTipoBaixa the motivo tipo baixa
	 */
	public void setMotivoTipoBaixa(int motivoTipoBaixa) {
		this.motivoTipoBaixa = motivoTipoBaixa;
	}

	/**
	 * Set: codTipoContrato.
	 *
	 * @param codTipoContrato the cod tipo contrato
	 */
	public void setCodTipoContrato(Integer codTipoContrato) {
		this.codTipoContrato = codTipoContrato;
	}

	/**
	 * Get: codNumContratoNegocio.
	 *
	 * @return codNumContratoNegocio
	 */
	public Integer getCodNumContratoNegocio() {
		return codNumContratoNegocio;
	}

	/**
	 * Set: codNumContratoNegocio.
	 *
	 * @param codNumContratoNegocio the cod num contrato negocio
	 */
	public void setCodNumContratoNegocio(Integer codNumContratoNegocio) {
		this.codNumContratoNegocio = codNumContratoNegocio;
	}

	/**
	 * Get: itemSelecionadoLista.
	 *
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 *
	 * @param itemSelecionadoLista the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: listaControleRadio.
	 *
	 * @return listaControleRadio
	 */
	public List<SelectItem> getListaControleRadio() {
		return listaControleRadio;
	}

	/**
	 * Set: listaControleRadio.
	 *
	 * @param listaControleRadio the lista controle radio
	 */
	public void setListaControleRadio(List<SelectItem> listaControleRadio) {
		this.listaControleRadio = listaControleRadio;
	}

	/**
	 * Get: dataAtendimento.
	 *
	 * @return dataAtendimento
	 */
	public String getDataAtendimento() {
		return dataAtendimento;
	}

	/**
	 * Get: dtAtendPdc.
	 *
	 * @return dtAtendPdc
	 */
	public Date getDtAtendPdc() {
		Calendar calendar = Calendar.getInstance();

		calendar.set(GregorianCalendar.DAY_OF_MONTH, Integer.parseInt(""
				.equals(diaDataAtendimento) ? "0" : diaDataAtendimento));
		calendar.set(GregorianCalendar.MONTH, Integer.parseInt(""
				.equals(mesDataAtendimento) ? "0" : mesDataAtendimento) - 1);
		calendar.set(GregorianCalendar.YEAR, Integer.parseInt(""
				.equals(anoDataAtendimento) ? "0" : anoDataAtendimento));

		return calendar.getTime();
	}

	/**
	 * Set: dataAtendimento.
	 *
	 * @param dataAtendimento the data atendimento
	 */
	public void setDataAtendimento(String dataAtendimento) {
		this.dataAtendimento = dataAtendimento;
	}

	/**
	 * Get: dataGeracao.
	 *
	 * @return dataGeracao
	 */
	public String getDataGeracao() {
		return dataGeracao;
	}

	/**
	 * Set: dataGeracao.
	 *
	 * @param dataGeracao the data geracao
	 */
	public void setDataGeracao(String dataGeracao) {
		this.dataGeracao = dataGeracao;
	}

	/**
	 * Get: descricaoResumidaPendencia.
	 *
	 * @return descricaoResumidaPendencia
	 */
	public String getDescricaoResumidaPendencia() {
		return descricaoResumidaPendencia;
	}

	/**
	 * Set: descricaoResumidaPendencia.
	 *
	 * @param descricaoResumidaPendencia the descricao resumida pendencia
	 */
	public void setDescricaoResumidaPendencia(String descricaoResumidaPendencia) {
		this.descricaoResumidaPendencia = descricaoResumidaPendencia;
	}

	/**
	 * Get: dsDataBaixa.
	 *
	 * @return dsDataBaixa
	 */
	public String getDsDataBaixa() {
		return dsDataBaixa;
	}

	/**
	 * Set: dsDataBaixa.
	 *
	 * @param dsDataBaixa the ds data baixa
	 */
	public void setDsDataBaixa(String dsDataBaixa) {
		this.dsDataBaixa = dsDataBaixa;
	}

	/**
	 * Get: dsSituacao.
	 *
	 * @return dsSituacao
	 */
	public String getDsSituacao() {
		return dsSituacao;
	}

	/**
	 * Set: dsSituacao.
	 *
	 * @param dsSituacao the ds situacao
	 */
	public void setDsSituacao(String dsSituacao) {
		this.dsSituacao = dsSituacao;
	}

	/**
	 * Get: dsTipoUnidadeOrganizacinal.
	 *
	 * @return dsTipoUnidadeOrganizacinal
	 */
	public String getDsTipoUnidadeOrganizacinal() {
		return dsTipoUnidadeOrganizacinal;
	}

	/**
	 * Set: dsTipoUnidadeOrganizacinal.
	 *
	 * @param dsTipoUnidadeOrganizacinal the ds tipo unidade organizacinal
	 */
	public void setDsTipoUnidadeOrganizacinal(String dsTipoUnidadeOrganizacinal) {
		this.dsTipoUnidadeOrganizacinal = dsTipoUnidadeOrganizacinal;
	}

	/**
	 * Get: unidadeOrganizacional.
	 *
	 * @return unidadeOrganizacional
	 */
	public String getUnidadeOrganizacional() {
		return unidadeOrganizacional;
	}

	/**
	 * Set: unidadeOrganizacional.
	 *
	 * @param unidadeOrganizacional the unidade organizacional
	 */
	public void setUnidadeOrganizacional(String unidadeOrganizacional) {
		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	/**
	 * Get: complementoInclusao.
	 *
	 * @return complementoInclusao
	 */
	public String getComplementoInclusao() {
		return complementoInclusao;
	}

	/**
	 * Set: complementoInclusao.
	 *
	 * @param complementoInclusao the complemento inclusao
	 */
	public void setComplementoInclusao(String complementoInclusao) {
		this.complementoInclusao = complementoInclusao;
	}

	/**
	 * Get: complementoManutencao.
	 *
	 * @return complementoManutencao
	 */
	public String getComplementoManutencao() {
		return complementoManutencao;
	}

	/**
	 * Set: complementoManutencao.
	 *
	 * @param complementoManutencao the complemento manutencao
	 */
	public void setComplementoManutencao(String complementoManutencao) {
		this.complementoManutencao = complementoManutencao;
	}

	/**
	 * Get: dataHoraInclusao.
	 *
	 * @return dataHoraInclusao
	 */
	public String getDataHoraInclusao() {
		return dataHoraInclusao;
	}

	/**
	 * Set: dataHoraInclusao.
	 *
	 * @param dataHoraInclusao the data hora inclusao
	 */
	public void setDataHoraInclusao(String dataHoraInclusao) {
		this.dataHoraInclusao = dataHoraInclusao;
	}

	/**
	 * Get: dataHoraManutencao.
	 *
	 * @return dataHoraManutencao
	 */
	public String getDataHoraManutencao() {
		return dataHoraManutencao;
	}

	/**
	 * Set: dataHoraManutencao.
	 *
	 * @param dataHoraManutencao the data hora manutencao
	 */
	public void setDataHoraManutencao(String dataHoraManutencao) {
		this.dataHoraManutencao = dataHoraManutencao;
	}

	/**
	 * Get: tipoCanalInclusao.
	 *
	 * @return tipoCanalInclusao
	 */
	public String getTipoCanalInclusao() {
		return tipoCanalInclusao;
	}

	/**
	 * Set: tipoCanalInclusao.
	 *
	 * @param tipoCanalInclusao the tipo canal inclusao
	 */
	public void setTipoCanalInclusao(String tipoCanalInclusao) {
		this.tipoCanalInclusao = tipoCanalInclusao;
	}

	/**
	 * Get: tipoCanalManutencao.
	 *
	 * @return tipoCanalManutencao
	 */
	public String getTipoCanalManutencao() {
		return tipoCanalManutencao;
	}

	/**
	 * Set: tipoCanalManutencao.
	 *
	 * @param tipoCanalManutencao the tipo canal manutencao
	 */
	public void setTipoCanalManutencao(String tipoCanalManutencao) {
		this.tipoCanalManutencao = tipoCanalManutencao;
	}

	/**
	 * Get: usuarioInclusao.
	 *
	 * @return usuarioInclusao
	 */
	public String getUsuarioInclusao() {
		return usuarioInclusao;
	}

	/**
	 * Set: usuarioInclusao.
	 *
	 * @param usuarioInclusao the usuario inclusao
	 */
	public void setUsuarioInclusao(String usuarioInclusao) {
		this.usuarioInclusao = usuarioInclusao;
	}

	/**
	 * Get: usuarioManutencao.
	 *
	 * @return usuarioManutencao
	 */
	public String getUsuarioManutencao() {
		return usuarioManutencao;
	}

	/**
	 * Set: usuarioManutencao.
	 *
	 * @param usuarioManutencao the usuario manutencao
	 */
	public void setUsuarioManutencao(String usuarioManutencao) {
		this.usuarioManutencao = usuarioManutencao;
	}

	/**
	 * Get: codDataBaixa.
	 *
	 * @return codDataBaixa
	 */
	public int getCodDataBaixa() {
		return codDataBaixa;
	}

	/**
	 * Set: codDataBaixa.
	 *
	 * @param codDataBaixa the cod data baixa
	 */
	public void setCodDataBaixa(int codDataBaixa) {
		this.codDataBaixa = codDataBaixa;
	}

	/**
	 * Get: listaMotivoBaixa.
	 *
	 * @return listaMotivoBaixa
	 */
	public List<SelectItem> getListaMotivoBaixa() {
		return listaMotivoBaixa;
	}

	/**
	 * Set: listaMotivoBaixa.
	 *
	 * @param listaMotivoBaixa the lista motivo baixa
	 */
	public void setListaMotivoBaixa(List<SelectItem> listaMotivoBaixa) {
		this.listaMotivoBaixa = listaMotivoBaixa;
	}

	/**
	 * Get: dsMotivoBaixa.
	 *
	 * @return dsMotivoBaixa
	 */
	public String getDsMotivoBaixa() {
		return dsMotivoBaixa;
	}

	/**
	 * Set: dsMotivoBaixa.
	 *
	 * @param dsMotivoBaixa the ds motivo baixa
	 */
	public void setDsMotivoBaixa(String dsMotivoBaixa) {
		this.dsMotivoBaixa = dsMotivoBaixa;
	}

	/**
	 * Get: identificacaoClienteContratoBean.
	 *
	 * @return identificacaoClienteContratoBean
	 */
	public IdentificacaoClienteContratoBean getIdentificacaoClienteContratoBean() {
		return identificacaoClienteContratoBean;
	}

	/**
	 * Set: identificacaoClienteContratoBean.
	 *
	 * @param identificacaoClienteContratoBean the identificacao cliente contrato bean
	 */
	public void setIdentificacaoClienteContratoBean(
			IdentificacaoClienteContratoBean identificacaoClienteContratoBean) {
		this.identificacaoClienteContratoBean = identificacaoClienteContratoBean;
	}

	/**
	 * Get: cdTipo.
	 *
	 * @return cdTipo
	 */
	public Integer getCdTipo() {
		return cdTipo;
	}

	/**
	 * Set: cdTipo.
	 *
	 * @param cdTipo the cd tipo
	 */
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}

	/**
	 * Get: dataHoraCadastramento.
	 *
	 * @return dataHoraCadastramento
	 */
	public String getDataHoraCadastramento() {
		return dataHoraCadastramento;
	}

	/**
	 * Set: dataHoraCadastramento.
	 *
	 * @param dataHoraCadastramento the data hora cadastramento
	 */
	public void setDataHoraCadastramento(String dataHoraCadastramento) {
		this.dataHoraCadastramento = dataHoraCadastramento;
	}

	/**
	 * Get: inicioVigencia.
	 *
	 * @return inicioVigencia
	 */
	public String getInicioVigencia() {
		return inicioVigencia;
	}

	/**
	 * Set: inicioVigencia.
	 *
	 * @param inicioVigencia the inicio vigencia
	 */
	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	/**
	 * Get: motivoDesc.
	 *
	 * @return motivoDesc
	 */
	public String getMotivoDesc() {
		return motivoDesc;
	}

	/**
	 * Set: motivoDesc.
	 *
	 * @param motivoDesc the motivo desc
	 */
	public void setMotivoDesc(String motivoDesc) {
		this.motivoDesc = motivoDesc;
	}

	/**
	 * Get: participacao.
	 *
	 * @return participacao
	 */
	public String getParticipacao() {
		return participacao;
	}

	/**
	 * Set: participacao.
	 *
	 * @param participacao the participacao
	 */
	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	/**
	 * Get: possuiAditivos.
	 *
	 * @return possuiAditivos
	 */
	public String getPossuiAditivos() {
		return possuiAditivos;
	}

	/**
	 * Set: possuiAditivos.
	 *
	 * @param possuiAditivos the possui aditivos
	 */
	public void setPossuiAditivos(String possuiAditivos) {
		this.possuiAditivos = possuiAditivos;
	}

	/**
	 * Get: situacaoDesc.
	 *
	 * @return situacaoDesc
	 */
	public String getSituacaoDesc() {
		return situacaoDesc;
	}

	/**
	 * Set: situacaoDesc.
	 *
	 * @param situacaoDesc the situacao desc
	 */
	public void setSituacaoDesc(String situacaoDesc) {
		this.situacaoDesc = situacaoDesc;
	}

	/**
	 * Get: tipo.
	 *
	 * @return tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Set: tipo.
	 *
	 * @param tipo the tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Get: listaEmpresaGestora.
	 *
	 * @return listaEmpresaGestora
	 */
	public List<SelectItem> getListaEmpresaGestora() {
		return listaEmpresaGestora;
	}

	/**
	 * Set: listaEmpresaGestora.
	 *
	 * @param listaEmpresaGestora the lista empresa gestora
	 */
	public void setListaEmpresaGestora(List<SelectItem> listaEmpresaGestora) {
		this.listaEmpresaGestora = listaEmpresaGestora;
	}

	/**
	 * Is habilita registrar.
	 *
	 * @return true, if is habilita registrar
	 */
	public boolean isHabilitaRegistrar() {
		return habilitaRegistrar;
	}

	/**
	 * Set: habilitaRegistrar.
	 *
	 * @param habilitaRegistrar the habilita registrar
	 */
	public void setHabilitaRegistrar(boolean habilitaRegistrar) {
		this.habilitaRegistrar = habilitaRegistrar;
	}

	/**
	 * Get: listaMotivoBaixaHash.
	 *
	 * @return listaMotivoBaixaHash
	 */
	public Map<Integer, String> getListaMotivoBaixaHash() {
		return listaMotivoBaixaHash;
	}

	/**
	 * Set lista motivo baixa hash.
	 *
	 * @param listaMotivoBaixaHash the lista motivo baixa hash
	 */
	public void setListaMotivoBaixaHash(
			Map<Integer, String> listaMotivoBaixaHash) {
		this.listaMotivoBaixaHash = listaMotivoBaixaHash;
	}

	/**
	 * Get: tipoBaixa.
	 *
	 * @return tipoBaixa
	 */
	public Integer getTipoBaixa() {
		return tipoBaixa;
	}

	/**
	 * Set: tipoBaixa.
	 *
	 * @param tipoBaixa the tipo baixa
	 */
	public void setTipoBaixa(Integer tipoBaixa) {
		this.tipoBaixa = tipoBaixa;
	}

	/**
	 * Get: radioRecusaAtende.
	 *
	 * @return radioRecusaAtende
	 */
	public String getRadioRecusaAtende() {
		return radioRecusaAtende;
	}

	/**
	 * Set: radioRecusaAtende.
	 *
	 * @param radioRecusaAtende the radio recusa atende
	 */
	public void setRadioRecusaAtende(String radioRecusaAtende) {
		this.radioRecusaAtende = radioRecusaAtende;
	}

	/**
	 * Get: codMotivo.
	 *
	 * @return codMotivo
	 */
	public int getCodMotivo() {
		return codMotivo;
	}

	/**
	 * Set: codMotivo.
	 *
	 * @param codMotivo the cod motivo
	 */
	public void setCodMotivo(int codMotivo) {
		this.codMotivo = codMotivo;
	}

	/**
	 * Get: dsMotivo.
	 *
	 * @return dsMotivo
	 */
	public String getDsMotivo() {
		return dsMotivo;
	}

	/**
	 * Set: dsMotivo.
	 *
	 * @param dsMotivo the ds motivo
	 */
	public void setDsMotivo(String dsMotivo) {
		this.dsMotivo = dsMotivo;
	}

	/**
	 * Get: dsDescricaoContrato.
	 *
	 * @return dsDescricaoContrato
	 */
	public String getDsDescricaoContrato() {
		return dsDescricaoContrato;
	}

	/**
	 * Set: dsDescricaoContrato.
	 *
	 * @param dsDescricaoContrato the ds descricao contrato
	 */
	public void setDsDescricaoContrato(String dsDescricaoContrato) {
		this.dsDescricaoContrato = dsDescricaoContrato;
	}

	/**
	 * Get: anoDataAtendimento.
	 *
	 * @return anoDataAtendimento
	 */
	public String getAnoDataAtendimento() {
		return anoDataAtendimento;
	}

	/**
	 * Set: anoDataAtendimento.
	 *
	 * @param anoDataAtendimento the ano data atendimento
	 */
	public void setAnoDataAtendimento(String anoDataAtendimento) {
		this.anoDataAtendimento = anoDataAtendimento;
	}

	/**
	 * Get: diaDataAtendimento.
	 *
	 * @return diaDataAtendimento
	 */
	public String getDiaDataAtendimento() {
		return diaDataAtendimento;
	}

	/**
	 * Set: diaDataAtendimento.
	 *
	 * @param diaDataAtendimento the dia data atendimento
	 */
	public void setDiaDataAtendimento(String diaDataAtendimento) {
		this.diaDataAtendimento = diaDataAtendimento;
	}

	/**
	 * Get: mesDataAtendimento.
	 *
	 * @return mesDataAtendimento
	 */
	public String getMesDataAtendimento() {
		return mesDataAtendimento;
	}

	/**
	 * Set: mesDataAtendimento.
	 *
	 * @param mesDataAtendimento the mes data atendimento
	 */
	public void setMesDataAtendimento(String mesDataAtendimento) {
		this.mesDataAtendimento = mesDataAtendimento;
	}

	/**
	 * Get: dsTipo.
	 *
	 * @return dsTipo
	 */
	public String getDsTipo() {
		return dsTipo;
	}

	/**
	 * Set: dsTipo.
	 *
	 * @param dsTipo the ds tipo
	 */
	public void setDsTipo(String dsTipo) {
		this.dsTipo = dsTipo;
	}

	/**
	 * Get: dataBaixa.
	 *
	 * @return dataBaixa
	 */
	public String getDataBaixa() {
		return dataBaixa;
	}

	/**
	 * Set: dataBaixa.
	 *
	 * @param dataBaixa the data baixa
	 */
	public void setDataBaixa(String dataBaixa) {
		this.dataBaixa = dataBaixa;
	}

	/**
	 * Get: dsAgenciaOperadora.
	 *
	 * @return dsAgenciaOperadora
	 */
	public String getDsAgenciaOperadora() {
		return dsAgenciaOperadora;
	}

	/**
	 * Set: dsAgenciaOperadora.
	 *
	 * @param dsAgenciaOperadora the ds agencia operadora
	 */
	public void setDsAgenciaOperadora(String dsAgenciaOperadora) {
		this.dsAgenciaOperadora = dsAgenciaOperadora;
	}

	/**
	 * Get: gerenteResponsavel.
	 *
	 * @return gerenteResponsavel
	 */
	public String getGerenteResponsavel() {
		return gerenteResponsavel;
	}

	/**
	 * Set: gerenteResponsavel.
	 *
	 * @param gerenteResponsavel the gerente responsavel
	 */
	public void setGerenteResponsavel(String gerenteResponsavel) {
		this.gerenteResponsavel = gerenteResponsavel;
	}

	/**
	 * @return the verificaerrorcode
	 */
	public Boolean getVerificaerrorcode() {
		return verificaerrorcode;
	}

	/**
	 * @param verificaerrorcode the verificaerrorcode to set
	 */
	public void setVerificaerrorcode(Boolean verificaerrorcode) {
		this.verificaerrorcode = verificaerrorcode;
	}

}
