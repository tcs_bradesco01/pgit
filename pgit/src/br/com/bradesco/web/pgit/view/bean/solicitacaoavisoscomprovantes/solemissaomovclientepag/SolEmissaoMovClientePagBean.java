/**
 * Nome: br.com.bradesco.web.pgit.view.bean.solicitacaoavisoscomprovantes.solemissaomovclientepag
 * Compilador: JDK 1.5
 * Prop�sito: INSERIR O PROP�SITO DAS CLASSES DO PACOTE
 * Data da cria��o: <dd/MM/yyyy>
 * Par�metros de compila��o: -d
 */
package br.com.bradesco.web.pgit.view.bean.solicitacaoavisoscomprovantes.solemissaomovclientepag;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarModalidadePagtoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarServicoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.ConsultarSituacaoSolicitacaoEstornoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.ConsultarEnderecoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.consultas.bean.DetalharDadosContratoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasEntradaDTO;
import br.com.bradesco.web.pgit.service.business.filtroidentificao.bean.ConsultarListaClientePessoasSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.ISolEmissaoComprovantePagtoClientePagService;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOpeTarifaPadraoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOperadoraEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaocomppagtoclipag.bean.ConsultarAgenciaOperadoraSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.ISolEmissaoMovClientePagService;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ConsultarSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ConsultarSolEmiAviMovtoCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.DetalharSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.DetalharSolEmiAviMovtoCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ExcluirSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.ExcluirSolEmiAviMovtoCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.IncluirSolEmiAviMovtoCliePagadorEntradaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.IncluirSolEmiAviMovtoCliePagadorSaidaDTO;
import br.com.bradesco.web.pgit.service.business.solemissaomovclientepag.bean.OcorrenciasDetalharSaidaDTO;
import br.com.bradesco.web.pgit.utils.CpfCnpjUtils;
import br.com.bradesco.web.pgit.utils.PgitUtil;
import br.com.bradesco.web.pgit.view.bean.agendamentoefetivacaoestornopagamentos.filtroagendamentoefetivacaoestorno.FiltroAgendamentoEfetivacaoEstornoBean;
import br.com.bradesco.web.pgit.view.bean.solicitacaoavisoscomprovantes.filtroenderecoemail.FiltroEnderecoEmailBean;
import br.com.bradesco.web.pgit.view.converters.FormatarData;

/**
 * Nome: SolEmissaoMovClientePagBean
 * <p>
 * Prop�sito: Implementa��o do Bean SolEmissaoMovClientePagBean
 * </p>
 * 
 * @author todo! - Solu��es em Tecnologia / TI Melhorias - Arquitetura
 * @version 1.0
 */
public class SolEmissaoMovClientePagBean {
	/** Atributos **/
	private boolean disableArgumentosConsulta;

	/** Atributo solEmissaoMovClientePagServiceImpl. */
	private ISolEmissaoMovClientePagService solEmissaoMovClientePagServiceImpl;

	/** Atributo comboService. */
	private IComboService comboService;

	/** Atributo filtroEnderecoEmailBean. */
	private FiltroEnderecoEmailBean filtroEnderecoEmailBean;

	/** Atributo filtroAgendamentoEfetivacaoEstornoBean. */
	private FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean;

	/** Atributo solEmissaoComprovantePagtoClientePagServiceImpl. */
	private ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl;

	/** Atributo habilitaArgumentosPesquisa. */
	private boolean habilitaArgumentosPesquisa;

	/** Atributo indicadorDescontoBloqueio. */
	private boolean indicadorDescontoBloqueio;

	/** Atributo dtInicioSolicitacao. */
	private Date dtInicioSolicitacao;

	/** Atributo dtFimSolicitacao. */
	private Date dtFimSolicitacao;

	/** Atributo cboSituacaoSolicitacaoEstornoFiltro. */
	private Integer cboSituacaoSolicitacaoEstornoFiltro;

	/** Atributo listaSituacaoSolicitacaoEstornoFiltro. */
	private List<SelectItem> listaSituacaoSolicitacaoEstornoFiltro;

	/** Atributo listaGridSolEmissaoMovClientePag. */
	private List<ConsultarSolEmiAviMovtoCliePagadorSaidaDTO> listaGridSolEmissaoMovClientePag;

	/** Atributo listaControleConsultar. */
	private List<SelectItem> listaControleConsultar;

	/** Atributo itemSelecionadoLista. */
	private Integer itemSelecionadoLista;

	// Cliente
	/** Atributo nrCnpjCpf. */
	private String nrCnpjCpf;

	/** Atributo dsRazaoSocial. */
	private String dsRazaoSocial;

	/** Atributo dsEmpresa. */
	private String dsEmpresa;

	/** Atributo nroContrato. */
	private String nroContrato;

	/** Atributo dsContrato. */
	private String dsContrato;

	/** Atributo cdSituacaoContrato. */
	private String cdSituacaoContrato;

	/** Atributo cdSolicitacaoPagamentoIntegrado. */
	private Integer cdSolicitacaoPagamentoIntegrado;

	/** Atributo nrSolicitacaoPagamentoIntegrado. */
	private Integer nrSolicitacaoPagamentoIntegrado;

	/** Atributo situacaoSolicitacao. */
	private String situacaoSolicitacao;

	/** Atributo dtInicioPeriodoMovimentacao. */
	private String dtInicioPeriodoMovimentacao;

	/** Atributo dtFimPeriodoMovimentacao. */
	private String dtFimPeriodoMovimentacao;

	/** Atributo cdProdutoServicoOperacao. */
	private Integer cdProdutoServicoOperacao;

	/** Atributo dsProdutoServicoOperacao. */
	private String dsProdutoServicoOperacao;

	/** Atributo cdProdutoOperacaoRelacionado. */
	private Integer cdProdutoOperacaoRelacionado;

	/** Atributo dsProdutoOperacaoRelacionado. */
	private String dsProdutoOperacaoRelacionado;

	/** Atributo cdRecebedorCredito. */
	private Long cdRecebedorCredito;

	/** Atributo cdTipoInscricaoRecebedor. */
	private Integer cdTipoInscricaoRecebedor;

	/** Atributo cpfCnpjRecebedorFormatado. */
	private String cpfCnpjRecebedorFormatado;

	/** Atributo cdDestinoCorrespSolicitacao. */
	private Integer cdDestinoCorrespSolicitacao;

	/** Atributo cdTipoPostagemSolicitacao. */
	private String cdTipoPostagemSolicitacao;

	/** Atributo dsLogradouroPagador. */
	private String dsLogradouroPagador;

	/** Atributo dsNumeroLogradouroPagador. */
	private String dsNumeroLogradouroPagador;

	/** Atributo dsComplementoLogradouroPagador. */
	private String dsComplementoLogradouroPagador;

	/** Atributo dsBairroClientePagador. */
	private String dsBairroClientePagador;

	/** Atributo dsMunicipioClientePagador. */
	private String dsMunicipioClientePagador;

	/** Atributo cdSiglaUfPagador. */
	private String cdSiglaUfPagador;

	/** Atributo cdCepPagador. */
	private Integer cdCepPagador;

	/** Atributo cdCepComplementoPagador. */
	private Integer cdCepComplementoPagador;

	/** Atributo dsEmailClientePagador. */
	private String dsEmailClientePagador;

	/** Atributo cdAgenciaOperadora. */
	private Integer cdAgenciaOperadora;

	/** Atributo dsAgencia. */
	private String dsAgencia;

	/** Atributo departamentoDestinoEntrega. */
	private String departamentoDestinoEntrega;

	/** Atributo vlTarifa. */
	private BigDecimal vlTarifa;

	/** Atributo vlTarifaPadrao. */
	private BigDecimal vlTarifaPadrao;

	/** Atributo vlrTarifaAtual. */
	private BigDecimal vlrTarifaAtual;

	/** Atributo cdPercentualDescTarifa. */
	private BigDecimal cdPercentualDescTarifa;

	/** Atributo cdUsuarioInclusao. */
	private String cdUsuarioInclusao;

	/** Atributo hrInclusaoRegistro. */
	private String hrInclusaoRegistro;

	/** Atributo cdOperacaoCanalInclusao. */
	private String cdOperacaoCanalInclusao;

	/** Atributo cdTipoCanalInclusao. */
	private String cdTipoCanalInclusao;

	/** Atributo dsTipoCanalInclusao. */
	private String dsTipoCanalInclusao;

	/** Atributo comprovantes. */
	private List<OcorrenciasDetalharSaidaDTO> comprovantes;

	/** Atributo departamento. */
	private String departamento;

	/** Atributo dtInicioPeriodoPagamento. */
	private Date dtInicioPeriodoPagamento;

	/** Atributo dtFimPeriodoPagamento. */
	private Date dtFimPeriodoPagamento;

	/** Atributo habilitaCampoEndereco. */
	private boolean habilitaCampoEndereco;

	/** Atributo tipoServicoFiltro. */
	private Integer tipoServicoFiltro;

	/** Atributo listaTipoServicoFiltro. */
	private List<SelectItem> listaTipoServicoFiltro = new ArrayList<SelectItem>();

	/** Atributo listaTipoServicoFiltroHash. */
	private Map<Integer, String> listaTipoServicoFiltroHash = new HashMap<Integer, String>();

	/** Atributo modalidadeFiltro. */
	private Integer modalidadeFiltro;

	/** Atributo listaModalidadeFiltro. */
	private List<SelectItem> listaModalidadeFiltro = new ArrayList<SelectItem>();

	/** Atributo listaModalidadeFiltroHash. */
	private Map<Integer, String> listaModalidadeFiltroHash = new HashMap<Integer, String>();

	/** Atributo radioIncluirFavorecido. */
	private String radioIncluirFavorecido;

	/** Atributo cdFavorecido. */
	private Long cdFavorecido;

	/** Atributo inscricaoFavorecido. */
	private Long inscricaoFavorecido;

	/** Atributo inscricaoFavorecidoFormatado. */
	private String inscricaoFavorecidoFormatado;

	/** Atributo tipoInscricaoFiltro. */
	private Integer tipoInscricaoFiltro;

	/** Atributo listaTipoInscricaoFiltro. */
	private List<SelectItem> listaTipoInscricaoFiltro = new ArrayList<SelectItem>();

	/** Atributo radioInfoAdicionais. */
	private String radioInfoAdicionais;

	/** Atributo radioAgenciaDepto. */
	private String radioAgenciaDepto;

	/** Atributo agenciaOperadora. */
	private String agenciaOperadora;

	/** Atributo departamentoFiltro. */
	private String departamentoFiltro;

	/** Atributo codAgenciaOperadora. */
	private Integer codAgenciaOperadora;

	/** Atributo cdPessoaJuridicaDepartamento. */
	private Long cdPessoaJuridicaDepartamento;

	/** Atributo cdTipoContratoNegocio. */
	private Integer cdTipoContratoNegocio;

	/** Atributo nrSequenciaDepartamento. */
	private Integer nrSequenciaDepartamento;

	/** Atributo cepFormatado. */
	private String cepFormatado;

	/** Atributo desabilitaDescontoTarifa. */
	private boolean desabilitaDescontoTarifa;

	/** Atributo disabledPercentualDesconto. */
	private boolean disabledPercentualDesconto;

	/** Atributo disabledBtnCalcular. */
	private boolean disabledBtnCalcular;

	/** M�todos **/
	// Inicializa��o

	public void limparAgenciaOperadora() {
		setDepartamentoFiltro("");
	}

	/**
	 * Limpar argumentos favorecido.
	 */
	public void limparArgumentosFavorecido() {
		setCdFavorecido(null);
		setInscricaoFavorecido(null);
		setTipoInscricaoFiltro(null);
	}

	/**
	 * Limpar info adicionais.
	 */
	public void limparInfoAdicionais() {
		
		if (getRadioInfoAdicionais() != null && "0".equals(getRadioInfoAdicionais())) {
			setDisabledPercentualDesconto(false);
		}
		
		setRadioAgenciaDepto("");
		setDepartamentoFiltro("");
		limparTipoPostagem();
	}

	/**
	 * Consultar agencia ope tarifa padrao.
	 */
	public void consultarAgenciaOpeTarifaPadrao() {
		try {
			ConsultarAgenciaOpeTarifaPadraoEntradaDTO entradaDTO = new ConsultarAgenciaOpeTarifaPadraoEntradaDTO();

			entradaDTO
					.setCdPessoaJuridicaEmpresa(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entradaDTO.setCdProdutoServicoOperacao(0);
			entradaDTO.setCdProdutoServicoRelacionado(0);
			entradaDTO.setCdTipoTarifa(5);

			ConsultarAgenciaOpeTarifaPadraoSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl()
					.consultarAgenciaOpeTarifaPadrao(entradaDTO);

			indicadorDescontoBloqueio = verificaStatusBloqueadoDesconto(saidaDTO
					.getCdIndicadorDescontoBloqueio());

			setVlTarifa(saidaDTO.getVlTarifaPadrao());
			
			verificarTarifaAtualizada();
			
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}
	
	private void verificarTarifaAtualizada(){
		if (indicadorDescontoBloqueio) {
			setVlrTarifaAtual(vlTarifa);				
		}else {
			setVlrTarifaAtual(BigDecimal.ZERO);
		}
	}
	
	/**
	 * Verifica status bloqueado desconto.
	 * 
	 * @param flag
	 *            the flag
	 * @return true, if verifica status bloqueado desconto
	 */
	boolean verificaStatusBloqueadoDesconto(String flag) {

		if ("N".equals(flag)) {
			return true;
		}

		return false;
	}

	/**
	 * Consultar agencia operadora.
	 */
	public void consultarAgenciaOperadora() {
		try {
			ConsultarAgenciaOperadoraEntradaDTO entradaDTO = new ConsultarAgenciaOperadoraEntradaDTO();

			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());

			ConsultarAgenciaOperadoraSaidaDTO saidaDTO = getSolEmissaoComprovantePagtoClientePagServiceImpl()
					.consultarAgenciaOperadora(entradaDTO);

			setCodAgenciaOperadora(saidaDTO.getCdAgencia());
			setAgenciaOperadora(PgitUtil.concatenarCampos(saidaDTO
					.getCdAgencia(), saidaDTO.getDsAgencia()));
			setCdPessoaJuridicaDepartamento(saidaDTO
					.getCdPessoaJuridicaDepartamento());
			setNrSequenciaDepartamento(saidaDTO.getNrSequenciaDepartamento());
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
		}
	}

	/**
	 * Limpar tipo postagem.
	 */
	public void limparTipoPostagem() {
		getFiltroEnderecoEmailBean().limparEnderecoEmail();
		setHabilitaCampoEndereco(true);
		setCdCepPagador(null);
		setCdCepComplementoPagador(null);
	}

	/**
	 * Limpar incluir.
	 * 
	 * @return the string
	 */
	public String limparIncluir() {
		setRadioIncluirFavorecido("");
		setDtInicioPeriodoPagamento(new Date());
		setDtFimPeriodoPagamento(new Date());
		setTipoServicoFiltro(0);
		setModalidadeFiltro(0);
		limparArgumentosFavorecido();
		limparInfoAdicionais();
		getFiltroEnderecoEmailBean().limparEnderecoEmail();
		getFiltroEnderecoEmailBean().setEmpresaGestoraContrato(
				filtroAgendamentoEfetivacaoEstornoBean
						.getCdPessoaJuridicaContrato());
		setRadioAgenciaDepto("");
		setRadioInfoAdicionais("");
		setCdPercentualDescTarifa(BigDecimal.ZERO);
		setVlrTarifaAtual(BigDecimal.ZERO);

		return "";
	}

	/**
	 * Iniciar tela.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void iniciarTela(ActionEvent evt) {
		limparTelaPrincipal();

		// Carregamento do Combo
		listarConsultarSituacaoSolicitacaoEstorno();

		// Tela de Filtro
		filtroAgendamentoEfetivacaoEstornoBean
				.setEntradaConsultarListaClientePessoas(new ConsultarListaClientePessoasEntradaDTO());
		filtroAgendamentoEfetivacaoEstornoBean
				.setSaidaConsultarListaClientePessoas(new ConsultarListaClientePessoasSaidaDTO());
		filtroAgendamentoEfetivacaoEstornoBean.listarEmpresaGestora();
		filtroAgendamentoEfetivacaoEstornoBean.listarTipoContrato();

		filtroAgendamentoEfetivacaoEstornoBean
				.setPaginaRetorno("solEmissaoMovClientePag");
		filtroAgendamentoEfetivacaoEstornoBean
				.setClienteContratoSelecionado(false);
		filtroAgendamentoEfetivacaoEstornoBean
				.setEmpresaGestoraFiltro(2269651L);

		// Tela de Filtro de Endere�o/Email
		getFiltroEnderecoEmailBean().setPaginaRetorno(
				"incSolEmissaoMovClientePagFiltro");

		setHabilitaArgumentosPesquisa(false);
		setListaGridSolEmissaoMovClientePag(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		setDesabilitaDescontoTarifa(false);
	}

	/**
	 * Limpar tela principal.
	 */
	public void limparTelaPrincipal() {
		filtroAgendamentoEfetivacaoEstornoBean.limparFiltroPrincipal();
		setDtInicioSolicitacao(new Date());
		setDtFimSolicitacao(new Date());
		setCboSituacaoSolicitacaoEstornoFiltro(null);
		setListaGridSolEmissaoMovClientePag(null);
		setItemSelecionadoLista(null);
	}

	/**
	 * Limpar apos alterar opcao cliente.
	 * 
	 * @return the string
	 */
	public String limparAposAlterarOpcaoCliente() {
		// Limpar Argumentos de Pesquisa e Lista
		filtroAgendamentoEfetivacaoEstornoBean.limpar();
		setDtInicioSolicitacao(new Date());
		setDtFimSolicitacao(new Date());
		setCboSituacaoSolicitacaoEstornoFiltro(null);
		setListaGridSolEmissaoMovClientePag(null);
		setItemSelecionadoLista(null);

		return "";

	}

	/**
	 * Limpar.
	 * 
	 * @return the string
	 */
	public String limpar() {
		limparTelaPrincipal();
		filtroAgendamentoEfetivacaoEstornoBean.setTipoFiltroSelecionado("");
		limparAposAlterarOpcaoCliente();
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Limpar grid consultar.
	 * 
	 * @return the string
	 */
	public String limparGridConsultar() {
		setListaGridSolEmissaoMovClientePag(null);
		setItemSelecionadoLista(null);
		setDisableArgumentosConsulta(false);
		return "";
	}

	/**
	 * Limpar args lista apos pesquisa cliente contrato.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void limparArgsListaAposPesquisaClienteContrato(ActionEvent evt) {
		setDtInicioSolicitacao(new Date());
		setDtFimSolicitacao(new Date());
		setCboSituacaoSolicitacaoEstornoFiltro(null);
		setListaGridSolEmissaoMovClientePag(null);
		setItemSelecionadoLista(null);
	}

	/**
	 * Listar cmb tipo favorecido.
	 */
	public void listarCmbTipoFavorecido() {
		try {

			setListaTipoInscricaoFiltro(new ArrayList<SelectItem>());

			/*
			 * List<InscricaoFavorecidosSaidaDTO> list = new
			 * ArrayList<InscricaoFavorecidosSaidaDTO>(); list =
			 * getComboService().listarTipoInscricao();
			 * 
			 * for(InscricaoFavorecidosSaidaDTO combo : list){
			 * getListaTipoInscricaoFiltro().add(new
			 * SelectItem(combo.getCdTipoInscricaoFavorecidos
			 * (),combo.getDsTipoInscricaoFavorecidos())); }
			 */
			listaTipoInscricaoFiltro.add(new SelectItem(1, MessageHelperUtils
					.getI18nMessage("label_cpf")));
			listaTipoInscricaoFiltro.add(new SelectItem(2, MessageHelperUtils
					.getI18nMessage("label_cnpj")));

		} catch (PdcAdapterFunctionalException p) {
			listaTipoInscricaoFiltro = new ArrayList<SelectItem>();
		}

	}

	/**
	 * Listar tipo servico.
	 */
	public void listarTipoServico() {
		try {
			setListaTipoServicoFiltro(new ArrayList<SelectItem>());
			ConsultarServicoEntradaDTO entrada = new ConsultarServicoEntradaDTO();

			entrada
					.setCdpessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entrada.setCdTipoContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getCdTipoContratoNegocio());
			entrada
					.setNrSequenciaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entrada.setNumeroOcorrencias(100);
			getListaTipoServicoFiltroHash().clear();

			List<ConsultarServicoSaidaDTO> list = getComboService()
					.consultarServico(entrada);

			for (ConsultarServicoSaidaDTO saida : list) {
				getListaTipoServicoFiltro().add(
						new SelectItem(saida.getCdServico(), saida
								.getDsServico()));
				getListaTipoServicoFiltroHash().put(saida.getCdServico(),
						saida.getDsServico());
			}

		} catch (PdcAdapterFunctionalException p) {
			if ("PGIT1087".equals(StringUtils.right(p.getCode(), 8))) {
				BradescoFacesUtils.addInfoModalMessage("("
						+ StringUtils.right(p.getCode(), 8) + ") "
						+ p.getMessage(),
						"#{solEmissaoMovClientePagBean.limpar}",
						BradescoViewExceptionActionType.ACTION, false);
			}

			setListaTipoServicoFiltro(new ArrayList<SelectItem>());
			getListaTipoServicoFiltroHash().clear();
		}
	}

	/**
	 * Listar modalidades.
	 */
	public void listarModalidades() {
		try {
			setModalidadeFiltro(0);
			if (getTipoServicoFiltro() != null && getTipoServicoFiltro() != 0) {

				setListaModalidadeFiltro(new ArrayList<SelectItem>());

				getListaModalidadeFiltroHash().clear();
				List<ConsultarModalidadePagtoSaidaDTO> list = getComboService()
						.consultarModalidadePagto(getTipoServicoFiltro());
				for (ConsultarModalidadePagtoSaidaDTO saida : list) {
					getListaModalidadeFiltro().add(
							new SelectItem(saida
									.getCdProdutoOperacaoRelacionado(), saida
									.getDsProdutoOperacaoRelacionado()));
					getListaModalidadeFiltroHash().put(
							saida.getCdProdutoOperacaoRelacionado(),
							saida.getDsProdutoOperacaoRelacionado());
				}
			} else {
				setListaModalidadeFiltro(new ArrayList<SelectItem>());
			}
		} catch (PdcAdapterFunctionalException p) {
			setListaModalidadeFiltro(new ArrayList<SelectItem>());
			setModalidadeFiltro(0);
		}
	}

	/**
	 * Consultar contrato.
	 * 
	 * @return the string
	 */
	public String consultarContrato() {
		String retorno = "";
		getFiltroAgendamentoEfetivacaoEstornoBean().consultarContrato();

		if (getFiltroAgendamentoEfetivacaoEstornoBean()
				.getListaConsultarListaContratosPessoas() != null) {
			if (getFiltroAgendamentoEfetivacaoEstornoBean()
					.getListaConsultarListaContratosPessoas().size() != 0
					&& getFiltroAgendamentoEfetivacaoEstornoBean()
							.getListaConsultarListaContratosPessoas().size() != 1) {
				retorno = "identificacaoContratoAgendamento";
			} else {
				listarTipoServico();
			}

			return retorno;
		}
		return retorno;
	}

	/**
	 * Carrega cabecalho.
	 */
	public void carregaCabecalho() {
		setNrCnpjCpf("");
		setDsRazaoSocial("");
		setDsEmpresa("");
		setNroContrato("");
		setDsContrato("");
		setCdSituacaoContrato("");
		if (filtroAgendamentoEfetivacaoEstornoBean.getTipoFiltroSelecionado()
				.equals("0")) { // Filtrar
			// por
			// cliente
			setNrCnpjCpf(filtroAgendamentoEfetivacaoEstornoBean
					.getNrCpfCnpjCliente());
			setDsRazaoSocial(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoRazaoSocialCliente());
			setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
					.getEmpresaGestoraDescCliente());
			setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getNumeroDescCliente());
			setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getDescricaoContratoDescCliente());
			setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
					.getSituacaoDescCliente());
		} else {
			if (filtroAgendamentoEfetivacaoEstornoBean
					.getTipoFiltroSelecionado().equals("1")) { // Filtrar
				// por
				// contrato
				setDsEmpresa(filtroAgendamentoEfetivacaoEstornoBean
						.getEmpresaGestoraDescContrato());
				setNroContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getNumeroDescContrato());
				setDsContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getDescricaoContratoDescContrato());
				setCdSituacaoContrato(filtroAgendamentoEfetivacaoEstornoBean
						.getSituacaoDescContrato());

				try {
					DetalharDadosContratoEntradaDTO entradaDTO = new DetalharDadosContratoEntradaDTO();
					entradaDTO
							.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
									.getCdPessoaJuridicaContrato());
					entradaDTO
							.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
									.getCdTipoContratoNegocio());
					entradaDTO
							.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
									.getNrSequenciaContratoNegocio());

					DetalharDadosContratoSaidaDTO saidaDTO = filtroAgendamentoEfetivacaoEstornoBean
							.getConsultasService().detalharDadosContrato(
									entradaDTO);
					setNrCnpjCpf(saidaDTO.getCdCnpjCpfTitular());
					setDsRazaoSocial(saidaDTO.getDsParticipanteTitular());
				} catch (PdcAdapterFunctionalException e) {
					setNrCnpjCpf("");
					setDsRazaoSocial("");
				}

			}
		}
	}

	/**
	 * Pesquisar consulta.
	 * 
	 * @param evt
	 *            the evt
	 * @return the string
	 */
	public String pesquisarConsulta(ActionEvent evt) {
		consultarSolEmiAviMovtoCliePagador();
		return "";
	}

	/**
	 * Consultar sol emi avi movto clie pagador.
	 * 
	 * @param evt
	 *            the evt
	 */
	public void consultarSolEmiAviMovtoCliePagador(ActionEvent evt) {
		consultarSolEmiAviMovtoCliePagador();
	}

	/**
	 * Consultar sol emi avi movto clie pagador.
	 * 
	 * @return the string
	 */
	public String consultarSolEmiAviMovtoCliePagador() {
		listaGridSolEmissaoMovClientePag = new ArrayList<ConsultarSolEmiAviMovtoCliePagadorSaidaDTO>();
		try {
			setItemSelecionadoLista(null);
			ConsultarSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO = new ConsultarSolEmiAviMovtoCliePagadorEntradaDTO();

			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entradaDTO.setDtInicioSolicitacao(FormatarData
					.formataDiaMesAno(this.getDtInicioSolicitacao()));
			entradaDTO.setDtFimSolicitacao(FormatarData.formataDiaMesAno(this
					.getDtFimSolicitacao()));
			entradaDTO.setCdSituacaoSolicitacao(this
					.getCboSituacaoSolicitacaoEstornoFiltro());

			setListaGridSolEmissaoMovClientePag(getSolEmissaoMovClientePagServiceImpl()
					.consultarSolEmiAviMovtoCliePagador(entradaDTO));

			listaControleConsultar = new ArrayList<SelectItem>();
			for (int i = 0; i < getListaGridSolEmissaoMovClientePag().size(); i++) {
				listaControleConsultar.add(new SelectItem(i, ""));
			}
			setDisableArgumentosConsulta(true);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setListaGridSolEmissaoMovClientePag(null);
			setItemSelecionadoLista(null);
			setDisableArgumentosConsulta(false);
		}

		return "";
	}

	/**
	 * Excluir confirmar.
	 * 
	 * @return the string
	 */
	public String excluirConfirmar() {
		try {
			// ConsultarSolEmiAviMovtoCliePagadorSaidaDTO registroSelecionado =
			// getListaGridSolEmissaoMovClientePag().get(getItemSelecionadoLista());
			ExcluirSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO = new ExcluirSolEmiAviMovtoCliePagadorEntradaDTO();

			entradaDTO.setCdSolicitacaoPagamentoIntegrado(this
					.getCdSolicitacaoPagamentoIntegrado());
			entradaDTO.setNrSolicitacaoPagamentoIntegrado(this
					.getNrSolicitacaoPagamentoIntegrado());
			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());

			ExcluirSolEmiAviMovtoCliePagadorSaidaDTO saidaDTO = getSolEmissaoMovClientePagServiceImpl()
					.excluirSolEmiAviMovtoCliePagador(entradaDTO);

			BradescoFacesUtils
					.addInfoModalMessage(
							"(" + saidaDTO.getCodMensagem() + ") "
									+ saidaDTO.getMensagem(),
							"solEmissaoMovClientePag",
							"#{solEmissaoMovClientePagBean.consultarSolEmiAviMovtoCliePagador}",
							false);
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			return null;
		}

		return "";
	}

	/**
	 * Limpar variaveis incluir.
	 */
	public void limparVariaveisIncluir() {
		setDsLogradouroPagador("");
		setDsNumeroLogradouroPagador("");
		setDsComplementoLogradouroPagador("");
		setDsBairroClientePagador("");
		setDsMunicipioClientePagador("");
		setCdSiglaUfPagador("");
		setCepFormatado("");
		setDsEmailClientePagador("");
		setDsAgencia("");
		setDepartamento("");
	}

	/**
	 * Calcular tarifa atualizada.
	 */
	public void calcularTarifaAtualizada() {
		vlrTarifaAtual = vlTarifa.subtract(vlTarifa.multiply(
				cdPercentualDescTarifa.divide(new BigDecimal(100))).setScale(2,
				BigDecimal.ROUND_HALF_UP));
	}

	/**
	 * Avancar incluir.
	 * 
	 * @return the string
	 */
	public String avancarIncluir() {

		// Calcula o valor da Tarifa Atualizada
		calcularTarifaAtualizada();

		limparVariaveisIncluir();

		setDtInicioPeriodoMovimentacao(FormatarData.formataDiaMesAno(this
				.getDtInicioPeriodoPagamento()));
		setDtFimPeriodoMovimentacao(FormatarData.formataDiaMesAno(this
				.getDtFimPeriodoPagamento()));
		setDsProdutoServicoOperacao(this.getListaTipoServicoFiltroHash().get(
				this.getTipoServicoFiltro()));
		setDsProdutoOperacaoRelacionado(getDsModalidadeFiltro());

		setInscricaoFavorecidoFormatado(PgitUtil.formatCpfCnpj(
				getInscricaoFavorecido(), getTipoInscricaoFiltro()));
		setCdRecebedorCredito(this.getCdFavorecido());
		if (getRadioInfoAdicionais() != null
				&& !getRadioInfoAdicionais().equals("")) {
			if (getRadioInfoAdicionais().equals("0")) {
				ConsultarEnderecoSaidaDTO enderecoSelecionado = getFiltroEnderecoEmailBean()
						.getEnderecoSelecionado();
				setDsLogradouroPagador(enderecoSelecionado
						.getDsLogradouroPagador());
				setDsNumeroLogradouroPagador(enderecoSelecionado
						.getDsNumeroLogradouroPagador());
				setDsComplementoLogradouroPagador(enderecoSelecionado
						.getDsComplementoLogradouroPagador());
				setDsBairroClientePagador(enderecoSelecionado
						.getDsBairroClientePagador());
				setDsMunicipioClientePagador(enderecoSelecionado
						.getDsMunicipioClientePagador());
				setCdSiglaUfPagador(enderecoSelecionado.getCdSiglaUfPagador());
				setCepFormatado(PgitUtil.complementaDigito(String
						.valueOf(enderecoSelecionado.getCdCepPagador()), 5)
						+ "-"
						+ PgitUtil.complementaDigito(String
								.valueOf(enderecoSelecionado
										.getCdCepComplementoPagador()), 3));
				setCdCepPagador(enderecoSelecionado.getCdCepPagador());
				setCdCepComplementoPagador(enderecoSelecionado
						.getCdCepComplementoPagador());

			} else {
				if (getRadioInfoAdicionais().equals("1")) {
					if (getRadioAgenciaDepto().equals("0")) {
						setDsAgencia(getAgenciaOperadora());
					} else {
						if (getRadioAgenciaDepto().equals("1")) {
							setDepartamento(getDepartamentoFiltro());
						}
					}
				}
			}
		}

		return "AVANCAR_INCLUIR";
	}

	/**
	 * Incluir confirmar.
	 * 
	 * @return the string
	 */
	public String incluirConfirmar() {
		try {
			IncluirSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO = new IncluirSolEmiAviMovtoCliePagadorEntradaDTO();

			entradaDTO
					.setCdPessoaJuridicaContrato(filtroAgendamentoEfetivacaoEstornoBean
							.getCdPessoaJuridicaContrato());
			entradaDTO
					.setCdTipoContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getCdTipoContratoNegocio());
			entradaDTO
					.setNrSequenciaContratoNegocio(filtroAgendamentoEfetivacaoEstornoBean
							.getNrSequenciaContratoNegocio());
			entradaDTO.setDtInicioPerMovimentacao(FormatarData
					.formataDiaMesAnoToPdc(getDtInicioPeriodoPagamento()));
			entradaDTO.setDsFimPerMovimentacao(FormatarData
					.formataDiaMesAnoToPdc(getDtFimPeriodoPagamento()));
			entradaDTO.setCdProdutoServicoOperacao(this.getTipoServicoFiltro());
			entradaDTO.setCdProdutoOperacaoRelacionado(this
					.getModalidadeFiltro());
			entradaDTO.setCdRecebedorCredito(this.getCdFavorecido());
			entradaDTO.setCdTipoInscricaoRecebedor(this
					.getTipoInscricaoFiltro());

			entradaDTO.setCdCpfCnpjRecebedor(0L);
			entradaDTO.setCdFilialCnpjRecebedor(0);
			entradaDTO.setCdControleCpfRecebedor(0);
			if (getInscricaoFavorecido() != null
					&& !getInscricaoFavorecido().equals("")
					&& getTipoInscricaoFiltro() != null) {
				if (getTipoInscricaoFiltro().intValue() == 1) {
					String aux = PgitUtil.complementaDigito(PgitUtil
							.verificaStringNula(getInscricaoFavorecido()
									.toString()), 11);
					entradaDTO.setCdCpfCnpjRecebedor(Long.valueOf(aux
							.substring(0, 9)));
					entradaDTO.setCdFilialCnpjRecebedor(0);
					entradaDTO.setCdControleCpfRecebedor(Integer.parseInt(aux
							.substring(9, 11)));
				} else {
					if (getTipoInscricaoFiltro().intValue() == 2) {
						String aux = PgitUtil.complementaDigito(PgitUtil
								.verificaStringNula(getInscricaoFavorecido()
										.toString()), 15);
						entradaDTO.setCdCpfCnpjRecebedor(Long.valueOf(aux
								.substring(0, 9)));
						entradaDTO.setCdFilialCnpjRecebedor(Integer
								.parseInt(aux.substring(9, 13)));
						entradaDTO.setCdControleCpfRecebedor(Integer
								.parseInt(aux.substring(13, 15)));
					}
				}
			}

			if (getRadioInfoAdicionais().equals("0")) {
				entradaDTO.setCdTipoPostagemSolicitacao("C");
				entradaDTO.setCdDestinoCorrespondenciaSolic(1);
			} else {
				if (getRadioInfoAdicionais().equals("1")
						&& getRadioAgenciaDepto().equals("0")) {
					entradaDTO.setCdTipoPostagemSolicitacao("A");
					entradaDTO
							.setCdPessoaJuridicaDepto(getCdPessoaJuridicaDepartamento());
					entradaDTO
							.setNrSequenciaUnidadeDepto(getNrSequenciaDepartamento());
					entradaDTO.setCdDestinoCorrespondenciaSolic(2);
				} else {
					if (getRadioInfoAdicionais().equals("1")
							&& getRadioAgenciaDepto().equals("1")) {
						entradaDTO.setCdTipoPostagemSolicitacao("D");
						entradaDTO
								.setCdPessoaJuridicaDepto(getCdPessoaJuridicaDepartamento());
						entradaDTO
								.setNrSequenciaUnidadeDepto(getNrSequenciaDepartamento());
						entradaDTO.setCdDestinoCorrespondenciaSolic(2);
					}
				}
			}

			entradaDTO.setDsLogradouroPagador(this.getDsLogradouroPagador());
			entradaDTO.setDsNumeroLogradouroPagador(this
					.getDsNumeroLogradouroPagador());
			entradaDTO.setDsComplementoLogradouroPagador(this
					.getDsComplementoLogradouroPagador());
			entradaDTO.setDsBairroClientePagador(this
					.getDsBairroClientePagador());
			entradaDTO.setDsMunicipioClientePagador(this
					.getDsMunicipioClientePagador());
			entradaDTO.setCdSiglaUfPagador(this.getCdSiglaUfPagador()
					.toUpperCase());
			entradaDTO.setCdCepPagador(this.getCdCepPagador());
			entradaDTO.setCdCepComplementoPagador(this
					.getCdCepComplementoPagador());
			entradaDTO
					.setDsEmailClientePagador(this.getDsEmailClientePagador());
			entradaDTO.setCdPessoaJuridicaDepto(this
					.getCdPessoaJuridicaDepartamento());
			entradaDTO.setNrSequenciaUnidadeDepto(this
					.getNrSequenciaDepartamento().intValue());
			entradaDTO.setVrTarifaPadraoSolic(vlTarifa);
			entradaDTO.setCdPercentualTarifaSolic(cdPercentualDescTarifa);
			entradaDTO.setVlTarifaNegocioSolic(vlrTarifaAtual);
			entradaDTO.setCdDepartamentoUnidade(getDepartamentoFiltro().equals(
					"") ? 0 : Integer.parseInt(getDepartamentoFiltro()));

			IncluirSolEmiAviMovtoCliePagadorSaidaDTO saidaDTO = getSolEmissaoMovClientePagServiceImpl()
					.incluirSolEmiAviMovtoCliePagador(entradaDTO);

			BradescoFacesUtils.addInfoModalMessage(
					"(" + saidaDTO.getCodMensagem() + ") "
							+ saidaDTO.getMensagem(),
					"solEmissaoMovClientePag",
					BradescoViewExceptionActionType.ACTION, false);

			if (getListaGridSolEmissaoMovClientePag() != null) {
				consultarSolEmiAviMovtoCliePagador();
			}

		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(),
					"incSolEmissaoMovClientePagFiltro",
					BradescoViewExceptionActionType.ACTION, false);
			return null;
		}
		return "";
	}

	/**
	 * Preenche dados.
	 */
	public void preencheDados() {
		try {
			ConsultarSolEmiAviMovtoCliePagadorSaidaDTO registroSelecionado = getListaGridSolEmissaoMovClientePag()
					.get(getItemSelecionadoLista());
			DetalharSolEmiAviMovtoCliePagadorEntradaDTO entradaDTO = new DetalharSolEmiAviMovtoCliePagadorEntradaDTO();

			entradaDTO.setCdPessoaJuridicaEmpr(registroSelecionado
					.getCdPessoaJuridica());
			entradaDTO.setCdTipoContratoNegocio(registroSelecionado
					.getCdTipoContrato());
			entradaDTO.setNrSequenciaContratoNegocio(registroSelecionado
					.getNrSequenciaContrato());
			entradaDTO.setCdTipoSolicitacaoPagamento(registroSelecionado
					.getCdTipoSolicitacao());
			entradaDTO.setCdSolicitacao(registroSelecionado.getNrSolicitacao());

			DetalharSolEmiAviMovtoCliePagadorSaidaDTO saidaDTO = getSolEmissaoMovClientePagServiceImpl()
					.detalharSolEmiAviMovtoCliePagador(entradaDTO);

			carregaCabecalho();
			limparVariaveisIncluir();

			setCdSolicitacaoPagamentoIntegrado(saidaDTO
					.getCdSolicitacaoPagamentoIntegrado());
			setNrSolicitacaoPagamentoIntegrado(saidaDTO
					.getNrSolicitacaoPagamentoIntegrado());
			setDtInicioPeriodoMovimentacao(saidaDTO
					.getDtInicioPeriodoMovimentacao());
			setDtFimPeriodoMovimentacao(saidaDTO.getDtFimPeriodoMovimentacao());
			setCdProdutoServicoOperacao(saidaDTO.getCdProdutoServicoOperacao());
			setCdProdutoOperacaoRelacionado(saidaDTO
					.getCdProdutoOperacaoRelacionado());
			setDsProdutoServicoOperacao(saidaDTO.getDsProdutoServicoOperacao());
			setDsProdutoOperacaoRelacionado(saidaDTO
					.getDsProdutoOperacaoRelacionado());
			setCdRecebedorCredito((Long) PgitUtil.verificaZero(saidaDTO
					.getCdRecebedorCredito()));
			if (saidaDTO.getCdCpfCnpjRecebedor().longValue() == 0
					&& saidaDTO.getCdFilialCnpjRecebedor().intValue() == 0
					&& saidaDTO.getCdControleCpfRecebedor().intValue() == 0) {
				setCpfCnpjRecebedorFormatado("");
			} else {
				setCpfCnpjRecebedorFormatado(CpfCnpjUtils.formatarCpfCnpj(
						saidaDTO.getCdCpfCnpjRecebedor(), saidaDTO
								.getCdFilialCnpjRecebedor(), saidaDTO
								.getCdControleCpfRecebedor()));
			}

			if (saidaDTO.getCdDestinoCorrespSolicitacao() == 1) {
				if (saidaDTO.getCdTipoPostagemSolicitacao().equals("E")) {
					setDsEmailClientePagador(saidaDTO
							.getDsEmailClientePagador());
				}
				if (saidaDTO.getCdTipoPostagemSolicitacao().equals("C")) {
					setDsLogradouroPagador(saidaDTO.getDsLogradouroPagador());
					setDsNumeroLogradouroPagador(saidaDTO
							.getDsNumeroLogradouroPagador());
					setDsComplementoLogradouroPagador(saidaDTO
							.getDsComplementoLogradouroPagador());
					setDsBairroClientePagador(saidaDTO
							.getDsBairroClientePagador());
					setDsMunicipioClientePagador(saidaDTO
							.getDsMunicipioClientePagador());
					setCdSiglaUfPagador(saidaDTO.getCdSiglaUfPagador());
					setCdCepPagador(saidaDTO.getCdCepPagador());
					setCdCepComplementoPagador(saidaDTO
							.getCdCepComplementoPagador());
					setCepFormatado(PgitUtil.formatCep(saidaDTO
							.getCdCepPagador(), saidaDTO
							.getCdCepComplementoPagador()));
				}
			}

			else {
				if (saidaDTO.getCdDestinoCorrespSolicitacao() == 2) {
					if (saidaDTO.getCdTipoPostagemSolicitacao().equals("A")) {
						setDsAgencia(PgitUtil.concatenarCampos(saidaDTO
								.getCdAgenciaOperadora(), saidaDTO
								.getDsAgencia()));
					} else {
						if (saidaDTO.getCdTipoPostagemSolicitacao().equals("D")) {
							setDepartamento(PgitUtil.concatenarCampos(saidaDTO
									.getCdDepartamentoUnidade(), saidaDTO
									.getDsAgencia()));
						}
					}
				}
			}

			setVlTarifa(saidaDTO.getVlTarifa());
			setVlTarifaPadrao(saidaDTO.getVlTarifaPadrao());
			setVlrTarifaAtual(saidaDTO.getVlrTarifaAtual());
			setCdPercentualDescTarifa(saidaDTO.getCdPercentualDescTarifa());
			setCdUsuarioInclusao(saidaDTO.getCdUsuarioInclusao());
			setHrInclusaoRegistro(saidaDTO.getHrInclusaoRegistro());
			setCdOperacaoCanalInclusao(saidaDTO.getCdOperacaoCanalInclusao());
			setCdTipoCanalInclusao(PgitUtil.concatenarCampos(saidaDTO
					.getCdTipoCanalInclusao(), saidaDTO
					.getDsTipoCanalInclusao()));
		} catch (PdcAdapterFunctionalException p) {
			BradescoFacesUtils.addInfoModalMessage(
					"(" + StringUtils.right(p.getCode(), 8) + ") "
							+ p.getMessage(), false);
			setComprovantes(null);
		}
	}

	/**
	 * Listar consultar situacao solicitacao estorno.
	 */
	public void listarConsultarSituacaoSolicitacaoEstorno() {
		try {
			listaSituacaoSolicitacaoEstornoFiltro = new ArrayList<SelectItem>();

			ConsultarSituacaoSolicitacaoEstornoEntradaDTO entrada = new ConsultarSituacaoSolicitacaoEstornoEntradaDTO();

			entrada.setCdSituacao(0);
			entrada.setNumeroOcorrencias(30);
			entrada.setCdIndicador(1);

			List<ConsultarSituacaoSolicitacaoEstornoSaidaDTO> list = getComboService()
					.consultarSituacaoSolicitacaoEstorno(entrada);

			for (ConsultarSituacaoSolicitacaoEstornoSaidaDTO saida : list) {
				listaSituacaoSolicitacaoEstornoFiltro.add(new SelectItem(saida
						.getCodigo(), saida.getDescricao()));
			}
		} catch (PdcAdapterFunctionalException p) {
			listaSituacaoSolicitacaoEstornoFiltro = new ArrayList<SelectItem>();
		}
	}

	/**
	 * Voltar.
	 * 
	 * @return the string
	 */
	public String voltar() {
		setItemSelecionadoLista(null);

		return "VOLTAR";
	}

	/**
	 * Voltar incluir.
	 * 
	 * @return the string
	 */
	public String voltarIncluir() {
		return "VOLTAR";
	}

	/**
	 * Detalhar.
	 * 
	 * @return the string
	 */
	public String detalhar() {
		preencheDados();
		return "DETALHAR";
	}

	/**
	 * Excluir.
	 * 
	 * @return the string
	 */
	public String excluir() {
		preencheDados();
		return "EXCLUIR";
	}

	/**
	 * Incluir.
	 * 
	 * @return the string
	 */
	public String incluir() {
		setHabilitaCampoEndereco(true);
		limparIncluir();
		carregaCabecalho();
		listarTipoServico();
		listarCmbTipoFavorecido();
		consultarAgenciaOperadora();
		consultarAgenciaOpeTarifaPadrao();

		return "INCLUIR";
	}

	/**
	 * Selecionar tipo postagem.
	 */
	public void selecionarTipoPostagem() {
		limparTipoPostagem();
		aplicarRegraTarifa();
	}

	/**
	 * Selecionar agencia departamento.
	 */
	public void selecionarAgenciaDepartamento() {
		limparAgenciaOperadora();
		aplicarRegraTarifa();
	}

	/**
	 * Aplicar regra tarifa.
	 */
	public void aplicarRegraTarifa() {
		cdPercentualDescTarifa = BigDecimal.ZERO;
		vlrTarifaAtual = vlTarifa;
		disabledPercentualDesconto = true;
		disabledBtnCalcular = true;

		if (!indicadorDescontoBloqueio) {
			vlrTarifaAtual = BigDecimal.ZERO;
			disabledPercentualDesconto = false;
			disabledBtnCalcular = false;

			if ("1".equals(radioAgenciaDepto)) {
				cdPercentualDescTarifa = new BigDecimal(100);
				disabledPercentualDesconto = true;
				disabledBtnCalcular = true;
			}
		}

	}

	/** Getters e Setters **/
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 * 
	 * @param comboService
	 *            the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: filtroAgendamentoEfetivacaoEstornoBean.
	 * 
	 * @return filtroAgendamentoEfetivacaoEstornoBean
	 */
	public FiltroAgendamentoEfetivacaoEstornoBean getFiltroAgendamentoEfetivacaoEstornoBean() {
		return filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Set: filtroAgendamentoEfetivacaoEstornoBean.
	 * 
	 * @param filtroAgendamentoEfetivacaoEstornoBean
	 *            the filtro agendamento efetivacao estorno bean
	 */
	public void setFiltroAgendamentoEfetivacaoEstornoBean(
			FiltroAgendamentoEfetivacaoEstornoBean filtroAgendamentoEfetivacaoEstornoBean) {
		this.filtroAgendamentoEfetivacaoEstornoBean = filtroAgendamentoEfetivacaoEstornoBean;
	}

	/**
	 * Is habilita argumentos pesquisa.
	 * 
	 * @return true, if is habilita argumentos pesquisa
	 */
	public boolean isHabilitaArgumentosPesquisa() {
		return habilitaArgumentosPesquisa;
	}

	/**
	 * Set: habilitaArgumentosPesquisa.
	 * 
	 * @param habilitaArgumentosPesquisa
	 *            the habilita argumentos pesquisa
	 */
	public void setHabilitaArgumentosPesquisa(boolean habilitaArgumentosPesquisa) {
		this.habilitaArgumentosPesquisa = habilitaArgumentosPesquisa;
	}

	/**
	 * Get: dtFimSolicitacao.
	 * 
	 * @return dtFimSolicitacao
	 */
	public Date getDtFimSolicitacao() {
		return dtFimSolicitacao;
	}

	/**
	 * Set: dtFimSolicitacao.
	 * 
	 * @param dtFimSolicitacao
	 *            the dt fim solicitacao
	 */
	public void setDtFimSolicitacao(Date dtFimSolicitacao) {
		this.dtFimSolicitacao = dtFimSolicitacao;
	}

	/**
	 * Get: dtInicioSolicitacao.
	 * 
	 * @return dtInicioSolicitacao
	 */
	public Date getDtInicioSolicitacao() {
		return dtInicioSolicitacao;
	}

	/**
	 * Set: dtInicioSolicitacao.
	 * 
	 * @param dtInicioSolicitacao
	 *            the dt inicio solicitacao
	 */
	public void setDtInicioSolicitacao(Date dtInicioSolicitacao) {
		this.dtInicioSolicitacao = dtInicioSolicitacao;
	}

	/**
	 * Get: cboSituacaoSolicitacaoEstornoFiltro.
	 * 
	 * @return cboSituacaoSolicitacaoEstornoFiltro
	 */
	public Integer getCboSituacaoSolicitacaoEstornoFiltro() {
		return cboSituacaoSolicitacaoEstornoFiltro;
	}

	/**
	 * Set: cboSituacaoSolicitacaoEstornoFiltro.
	 * 
	 * @param cboSituacaoSolicitacaoEstornoFiltro
	 *            the cbo situacao solicitacao estorno filtro
	 */
	public void setCboSituacaoSolicitacaoEstornoFiltro(
			Integer cboSituacaoSolicitacaoEstornoFiltro) {
		this.cboSituacaoSolicitacaoEstornoFiltro = cboSituacaoSolicitacaoEstornoFiltro;
	}

	/**
	 * Get: listaGridSolEmissaoMovClientePag.
	 * 
	 * @return listaGridSolEmissaoMovClientePag
	 */
	public List<ConsultarSolEmiAviMovtoCliePagadorSaidaDTO> getListaGridSolEmissaoMovClientePag() {
		return listaGridSolEmissaoMovClientePag;
	}

	/**
	 * Set: listaGridSolEmissaoMovClientePag.
	 * 
	 * @param listaGridSolEmissaoMovClientePag
	 *            the lista grid sol emissao mov cliente pag
	 */
	public void setListaGridSolEmissaoMovClientePag(
			List<ConsultarSolEmiAviMovtoCliePagadorSaidaDTO> listaGridSolEmissaoMovClientePag) {
		this.listaGridSolEmissaoMovClientePag = listaGridSolEmissaoMovClientePag;
	}

	/**
	 * Get: listaSituacaoSolicitacaoEstornoFiltro.
	 * 
	 * @return listaSituacaoSolicitacaoEstornoFiltro
	 */
	public List<SelectItem> getListaSituacaoSolicitacaoEstornoFiltro() {
		return listaSituacaoSolicitacaoEstornoFiltro;
	}

	/**
	 * Set: listaSituacaoSolicitacaoEstornoFiltro.
	 * 
	 * @param listaSituacaoSolicitacaoEstornoFiltro
	 *            the lista situacao solicitacao estorno filtro
	 */
	public void setListaSituacaoSolicitacaoEstornoFiltro(
			List<SelectItem> listaSituacaoSolicitacaoEstornoFiltro) {
		this.listaSituacaoSolicitacaoEstornoFiltro = listaSituacaoSolicitacaoEstornoFiltro;
	}

	/**
	 * Get: listaControleConsultar.
	 * 
	 * @return listaControleConsultar
	 */
	public List<SelectItem> getListaControleConsultar() {
		return listaControleConsultar;
	}

	/**
	 * Set: listaControleConsultar.
	 * 
	 * @param listaControleConsultar
	 *            the lista controle consultar
	 */
	public void setListaControleConsultar(
			List<SelectItem> listaControleConsultar) {
		this.listaControleConsultar = listaControleConsultar;
	}

	/**
	 * Get: itemSelecionadoLista.
	 * 
	 * @return itemSelecionadoLista
	 */
	public Integer getItemSelecionadoLista() {
		return itemSelecionadoLista;
	}

	/**
	 * Set: itemSelecionadoLista.
	 * 
	 * @param itemSelecionadoLista
	 *            the item selecionado lista
	 */
	public void setItemSelecionadoLista(Integer itemSelecionadoLista) {
		this.itemSelecionadoLista = itemSelecionadoLista;
	}

	/**
	 * Get: cdAgenciaOperadora.
	 * 
	 * @return cdAgenciaOperadora
	 */
	public Integer getCdAgenciaOperadora() {
		return cdAgenciaOperadora;
	}

	/**
	 * Set: cdAgenciaOperadora.
	 * 
	 * @param cdAgenciaOperadora
	 *            the cd agencia operadora
	 */
	public void setCdAgenciaOperadora(Integer cdAgenciaOperadora) {
		this.cdAgenciaOperadora = cdAgenciaOperadora;
	}

	/**
	 * Get: cdCepComplementoPagador.
	 * 
	 * @return cdCepComplementoPagador
	 */
	public Integer getCdCepComplementoPagador() {
		return cdCepComplementoPagador;
	}

	/**
	 * Set: cdCepComplementoPagador.
	 * 
	 * @param cdCepComplementoPagador
	 *            the cd cep complemento pagador
	 */
	public void setCdCepComplementoPagador(Integer cdCepComplementoPagador) {
		this.cdCepComplementoPagador = cdCepComplementoPagador;
	}

	/**
	 * Get: cdCepPagador.
	 * 
	 * @return cdCepPagador
	 */
	public Integer getCdCepPagador() {
		return cdCepPagador;
	}

	/**
	 * Set: cdCepPagador.
	 * 
	 * @param cdCepPagador
	 *            the cd cep pagador
	 */
	public void setCdCepPagador(Integer cdCepPagador) {
		this.cdCepPagador = cdCepPagador;
	}

	/**
	 * Get: cdDestinoCorrespSolicitacao.
	 * 
	 * @return cdDestinoCorrespSolicitacao
	 */
	public Integer getCdDestinoCorrespSolicitacao() {
		return cdDestinoCorrespSolicitacao;
	}

	/**
	 * Set: cdDestinoCorrespSolicitacao.
	 * 
	 * @param cdDestinoCorrespSolicitacao
	 *            the cd destino corresp solicitacao
	 */
	public void setCdDestinoCorrespSolicitacao(
			Integer cdDestinoCorrespSolicitacao) {
		this.cdDestinoCorrespSolicitacao = cdDestinoCorrespSolicitacao;
	}

	/**
	 * Get: cdOperacaoCanalInclusao.
	 * 
	 * @return cdOperacaoCanalInclusao
	 */
	public String getCdOperacaoCanalInclusao() {
		return cdOperacaoCanalInclusao;
	}

	/**
	 * Set: cdOperacaoCanalInclusao.
	 * 
	 * @param cdOperacaoCanalInclusao
	 *            the cd operacao canal inclusao
	 */
	public void setCdOperacaoCanalInclusao(String cdOperacaoCanalInclusao) {
		this.cdOperacaoCanalInclusao = cdOperacaoCanalInclusao;
	}

	/**
	 * Get: cdPercentualDescTarifa.
	 * 
	 * @return cdPercentualDescTarifa
	 */
	public BigDecimal getCdPercentualDescTarifa() {
		return cdPercentualDescTarifa;
	}

	/**
	 * Set: cdPercentualDescTarifa.
	 * 
	 * @param cdPercentualDescTarifa
	 *            the cd percentual desc tarifa
	 */
	public void setCdPercentualDescTarifa(BigDecimal cdPercentualDescTarifa) {
		this.cdPercentualDescTarifa = cdPercentualDescTarifa;
	}

	/**
	 * Get: cdProdutoOperacaoRelacionado.
	 * 
	 * @return cdProdutoOperacaoRelacionado
	 */
	public Integer getCdProdutoOperacaoRelacionado() {
		return cdProdutoOperacaoRelacionado;
	}

	/**
	 * Set: cdProdutoOperacaoRelacionado.
	 * 
	 * @param cdProdutoOperacaoRelacionado
	 *            the cd produto operacao relacionado
	 */
	public void setCdProdutoOperacaoRelacionado(
			Integer cdProdutoOperacaoRelacionado) {
		this.cdProdutoOperacaoRelacionado = cdProdutoOperacaoRelacionado;
	}

	/**
	 * Get: cdProdutoServicoOperacao.
	 * 
	 * @return cdProdutoServicoOperacao
	 */
	public Integer getCdProdutoServicoOperacao() {
		return cdProdutoServicoOperacao;
	}

	/**
	 * Set: cdProdutoServicoOperacao.
	 * 
	 * @param cdProdutoServicoOperacao
	 *            the cd produto servico operacao
	 */
	public void setCdProdutoServicoOperacao(Integer cdProdutoServicoOperacao) {
		this.cdProdutoServicoOperacao = cdProdutoServicoOperacao;
	}

	/**
	 * Get: cdRecebedorCredito.
	 * 
	 * @return cdRecebedorCredito
	 */
	public Long getCdRecebedorCredito() {
		return cdRecebedorCredito;
	}

	/**
	 * Set: cdRecebedorCredito.
	 * 
	 * @param cdRecebedorCredito
	 *            the cd recebedor credito
	 */
	public void setCdRecebedorCredito(Long cdRecebedorCredito) {
		this.cdRecebedorCredito = cdRecebedorCredito;
	}

	/**
	 * Get: cdSiglaUfPagador.
	 * 
	 * @return cdSiglaUfPagador
	 */
	public String getCdSiglaUfPagador() {
		return cdSiglaUfPagador;
	}

	/**
	 * Set: cdSiglaUfPagador.
	 * 
	 * @param cdSiglaUfPagador
	 *            the cd sigla uf pagador
	 */
	public void setCdSiglaUfPagador(String cdSiglaUfPagador) {
		this.cdSiglaUfPagador = cdSiglaUfPagador;
	}

	/**
	 * Get: cdSituacaoContrato.
	 * 
	 * @return cdSituacaoContrato
	 */
	public String getCdSituacaoContrato() {
		return cdSituacaoContrato;
	}

	/**
	 * Set: cdSituacaoContrato.
	 * 
	 * @param cdSituacaoContrato
	 *            the cd situacao contrato
	 */
	public void setCdSituacaoContrato(String cdSituacaoContrato) {
		this.cdSituacaoContrato = cdSituacaoContrato;
	}

	/**
	 * Get: cdSolicitacaoPagamentoIntegrado.
	 * 
	 * @return cdSolicitacaoPagamentoIntegrado
	 */
	public Integer getCdSolicitacaoPagamentoIntegrado() {
		return cdSolicitacaoPagamentoIntegrado;
	}

	/**
	 * Set: cdSolicitacaoPagamentoIntegrado.
	 * 
	 * @param cdSolicitacaoPagamentoIntegrado
	 *            the cd solicitacao pagamento integrado
	 */
	public void setCdSolicitacaoPagamentoIntegrado(
			Integer cdSolicitacaoPagamentoIntegrado) {
		this.cdSolicitacaoPagamentoIntegrado = cdSolicitacaoPagamentoIntegrado;
	}

	/**
	 * Get: cdTipoCanalInclusao.
	 * 
	 * @return cdTipoCanalInclusao
	 */
	public String getCdTipoCanalInclusao() {
		return cdTipoCanalInclusao;
	}

	/**
	 * Set: cdTipoCanalInclusao.
	 * 
	 * @param cdTipoCanalInclusao
	 *            the cd tipo canal inclusao
	 */
	public void setCdTipoCanalInclusao(String cdTipoCanalInclusao) {
		this.cdTipoCanalInclusao = cdTipoCanalInclusao;
	}

	/**
	 * Get: cdTipoInscricaoRecebedor.
	 * 
	 * @return cdTipoInscricaoRecebedor
	 */
	public Integer getCdTipoInscricaoRecebedor() {
		return cdTipoInscricaoRecebedor;
	}

	/**
	 * Set: cdTipoInscricaoRecebedor.
	 * 
	 * @param cdTipoInscricaoRecebedor
	 *            the cd tipo inscricao recebedor
	 */
	public void setCdTipoInscricaoRecebedor(Integer cdTipoInscricaoRecebedor) {
		this.cdTipoInscricaoRecebedor = cdTipoInscricaoRecebedor;
	}

	/**
	 * Get: cdTipoPostagemSolicitacao.
	 * 
	 * @return cdTipoPostagemSolicitacao
	 */
	public String getCdTipoPostagemSolicitacao() {
		return cdTipoPostagemSolicitacao;
	}

	/**
	 * Set: cdTipoPostagemSolicitacao.
	 * 
	 * @param cdTipoPostagemSolicitacao
	 *            the cd tipo postagem solicitacao
	 */
	public void setCdTipoPostagemSolicitacao(String cdTipoPostagemSolicitacao) {
		this.cdTipoPostagemSolicitacao = cdTipoPostagemSolicitacao;
	}

	/**
	 * Get: cdUsuarioInclusao.
	 * 
	 * @return cdUsuarioInclusao
	 */
	public String getCdUsuarioInclusao() {
		return cdUsuarioInclusao;
	}

	/**
	 * Set: cdUsuarioInclusao.
	 * 
	 * @param cdUsuarioInclusao
	 *            the cd usuario inclusao
	 */
	public void setCdUsuarioInclusao(String cdUsuarioInclusao) {
		this.cdUsuarioInclusao = cdUsuarioInclusao;
	}

	/**
	 * Get: cpfCnpjRecebedorFormatado.
	 * 
	 * @return cpfCnpjRecebedorFormatado
	 */
	public String getCpfCnpjRecebedorFormatado() {
		return cpfCnpjRecebedorFormatado;
	}

	/**
	 * Set: cpfCnpjRecebedorFormatado.
	 * 
	 * @param cpfCnpjRecebedorFormatado
	 *            the cpf cnpj recebedor formatado
	 */
	public void setCpfCnpjRecebedorFormatado(String cpfCnpjRecebedorFormatado) {
		this.cpfCnpjRecebedorFormatado = cpfCnpjRecebedorFormatado;
	}

	/**
	 * Get: departamentoDestinoEntrega.
	 * 
	 * @return departamentoDestinoEntrega
	 */
	public String getDepartamentoDestinoEntrega() {
		return departamentoDestinoEntrega;
	}

	/**
	 * Set: departamentoDestinoEntrega.
	 * 
	 * @param departamentoDestinoEntrega
	 *            the departamento destino entrega
	 */
	public void setDepartamentoDestinoEntrega(String departamentoDestinoEntrega) {
		this.departamentoDestinoEntrega = departamentoDestinoEntrega;
	}

	/**
	 * Get: dsAgencia.
	 * 
	 * @return dsAgencia
	 */
	public String getDsAgencia() {
		return dsAgencia;
	}

	/**
	 * Set: dsAgencia.
	 * 
	 * @param dsAgencia
	 *            the ds agencia
	 */
	public void setDsAgencia(String dsAgencia) {
		this.dsAgencia = dsAgencia;
	}

	/**
	 * Get: dsBairroClientePagador.
	 * 
	 * @return dsBairroClientePagador
	 */
	public String getDsBairroClientePagador() {
		return dsBairroClientePagador;
	}

	/**
	 * Set: dsBairroClientePagador.
	 * 
	 * @param dsBairroClientePagador
	 *            the ds bairro cliente pagador
	 */
	public void setDsBairroClientePagador(String dsBairroClientePagador) {
		this.dsBairroClientePagador = dsBairroClientePagador;
	}

	/**
	 * Get: dsComplementoLogradouroPagador.
	 * 
	 * @return dsComplementoLogradouroPagador
	 */
	public String getDsComplementoLogradouroPagador() {
		return dsComplementoLogradouroPagador;
	}

	/**
	 * Set: dsComplementoLogradouroPagador.
	 * 
	 * @param dsComplementoLogradouroPagador
	 *            the ds complemento logradouro pagador
	 */
	public void setDsComplementoLogradouroPagador(
			String dsComplementoLogradouroPagador) {
		this.dsComplementoLogradouroPagador = dsComplementoLogradouroPagador;
	}

	/**
	 * Get: dsContrato.
	 * 
	 * @return dsContrato
	 */
	public String getDsContrato() {
		return dsContrato;
	}

	/**
	 * Set: dsContrato.
	 * 
	 * @param dsContrato
	 *            the ds contrato
	 */
	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	/**
	 * Get: dsEmailClientePagador.
	 * 
	 * @return dsEmailClientePagador
	 */
	public String getDsEmailClientePagador() {
		return dsEmailClientePagador;
	}

	/**
	 * Set: dsEmailClientePagador.
	 * 
	 * @param dsEmailClientePagador
	 *            the ds email cliente pagador
	 */
	public void setDsEmailClientePagador(String dsEmailClientePagador) {
		this.dsEmailClientePagador = dsEmailClientePagador;
	}

	/**
	 * Get: dsEmpresa.
	 * 
	 * @return dsEmpresa
	 */
	public String getDsEmpresa() {
		return dsEmpresa;
	}

	/**
	 * Set: dsEmpresa.
	 * 
	 * @param dsEmpresa
	 *            the ds empresa
	 */
	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

	/**
	 * Get: dsLogradouroPagador.
	 * 
	 * @return dsLogradouroPagador
	 */
	public String getDsLogradouroPagador() {
		return dsLogradouroPagador;
	}

	/**
	 * Set: dsLogradouroPagador.
	 * 
	 * @param dsLogradouroPagador
	 *            the ds logradouro pagador
	 */
	public void setDsLogradouroPagador(String dsLogradouroPagador) {
		this.dsLogradouroPagador = dsLogradouroPagador;
	}

	/**
	 * Get: dsMunicipioClientePagador.
	 * 
	 * @return dsMunicipioClientePagador
	 */
	public String getDsMunicipioClientePagador() {
		return dsMunicipioClientePagador;
	}

	/**
	 * Set: dsMunicipioClientePagador.
	 * 
	 * @param dsMunicipioClientePagador
	 *            the ds municipio cliente pagador
	 */
	public void setDsMunicipioClientePagador(String dsMunicipioClientePagador) {
		this.dsMunicipioClientePagador = dsMunicipioClientePagador;
	}

	/**
	 * Get: dsNumeroLogradouroPagador.
	 * 
	 * @return dsNumeroLogradouroPagador
	 */
	public String getDsNumeroLogradouroPagador() {
		return dsNumeroLogradouroPagador;
	}

	/**
	 * Set: dsNumeroLogradouroPagador.
	 * 
	 * @param dsNumeroLogradouroPagador
	 *            the ds numero logradouro pagador
	 */
	public void setDsNumeroLogradouroPagador(String dsNumeroLogradouroPagador) {
		this.dsNumeroLogradouroPagador = dsNumeroLogradouroPagador;
	}

	/**
	 * Get: dsProdutoOperacaoRelacionado.
	 * 
	 * @return dsProdutoOperacaoRelacionado
	 */
	public String getDsProdutoOperacaoRelacionado() {
		return dsProdutoOperacaoRelacionado;
	}

	/**
	 * Set: dsProdutoOperacaoRelacionado.
	 * 
	 * @param dsProdutoOperacaoRelacionado
	 *            the ds produto operacao relacionado
	 */
	public void setDsProdutoOperacaoRelacionado(
			String dsProdutoOperacaoRelacionado) {
		this.dsProdutoOperacaoRelacionado = dsProdutoOperacaoRelacionado;
	}

	/**
	 * Get: dsProdutoServicoOperacao.
	 * 
	 * @return dsProdutoServicoOperacao
	 */
	public String getDsProdutoServicoOperacao() {
		return dsProdutoServicoOperacao;
	}

	/**
	 * Set: dsProdutoServicoOperacao.
	 * 
	 * @param dsProdutoServicoOperacao
	 *            the ds produto servico operacao
	 */
	public void setDsProdutoServicoOperacao(String dsProdutoServicoOperacao) {
		this.dsProdutoServicoOperacao = dsProdutoServicoOperacao;
	}

	/**
	 * Get: dsRazaoSocial.
	 * 
	 * @return dsRazaoSocial
	 */
	public String getDsRazaoSocial() {
		return dsRazaoSocial;
	}

	/**
	 * Set: dsRazaoSocial.
	 * 
	 * @param dsRazaoSocial
	 *            the ds razao social
	 */
	public void setDsRazaoSocial(String dsRazaoSocial) {
		this.dsRazaoSocial = dsRazaoSocial;
	}

	/**
	 * Get: dsTipoCanalInclusao.
	 * 
	 * @return dsTipoCanalInclusao
	 */
	public String getDsTipoCanalInclusao() {
		return dsTipoCanalInclusao;
	}

	/**
	 * Set: dsTipoCanalInclusao.
	 * 
	 * @param dsTipoCanalInclusao
	 *            the ds tipo canal inclusao
	 */
	public void setDsTipoCanalInclusao(String dsTipoCanalInclusao) {
		this.dsTipoCanalInclusao = dsTipoCanalInclusao;
	}

	/**
	 * Get: dtFimPeriodoMovimentacao.
	 * 
	 * @return dtFimPeriodoMovimentacao
	 */
	public String getDtFimPeriodoMovimentacao() {
		return dtFimPeriodoMovimentacao;
	}

	/**
	 * Set: dtFimPeriodoMovimentacao.
	 * 
	 * @param dtFimPeriodoMovimentacao
	 *            the dt fim periodo movimentacao
	 */
	public void setDtFimPeriodoMovimentacao(String dtFimPeriodoMovimentacao) {
		this.dtFimPeriodoMovimentacao = dtFimPeriodoMovimentacao;
	}

	/**
	 * Get: dtInicioPeriodoMovimentacao.
	 * 
	 * @return dtInicioPeriodoMovimentacao
	 */
	public String getDtInicioPeriodoMovimentacao() {
		return dtInicioPeriodoMovimentacao;
	}

	/**
	 * Set: dtInicioPeriodoMovimentacao.
	 * 
	 * @param dtInicioPeriodoMovimentacao
	 *            the dt inicio periodo movimentacao
	 */
	public void setDtInicioPeriodoMovimentacao(
			String dtInicioPeriodoMovimentacao) {
		this.dtInicioPeriodoMovimentacao = dtInicioPeriodoMovimentacao;
	}

	/**
	 * Get: hrInclusaoRegistro.
	 * 
	 * @return hrInclusaoRegistro
	 */
	public String getHrInclusaoRegistro() {
		return hrInclusaoRegistro;
	}

	/**
	 * Set: hrInclusaoRegistro.
	 * 
	 * @param hrInclusaoRegistro
	 *            the hr inclusao registro
	 */
	public void setHrInclusaoRegistro(String hrInclusaoRegistro) {
		this.hrInclusaoRegistro = hrInclusaoRegistro;
	}

	/**
	 * Get: nrCnpjCpf.
	 * 
	 * @return nrCnpjCpf
	 */
	public String getNrCnpjCpf() {
		return nrCnpjCpf;
	}

	/**
	 * Set: nrCnpjCpf.
	 * 
	 * @param nrCnpjCpf
	 *            the nr cnpj cpf
	 */
	public void setNrCnpjCpf(String nrCnpjCpf) {
		this.nrCnpjCpf = nrCnpjCpf;
	}

	/**
	 * Get: nroContrato.
	 * 
	 * @return nroContrato
	 */
	public String getNroContrato() {
		return nroContrato;
	}

	/**
	 * Set: nroContrato.
	 * 
	 * @param nroContrato
	 *            the nro contrato
	 */
	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	/**
	 * Get: nrSolicitacaoPagamentoIntegrado.
	 * 
	 * @return nrSolicitacaoPagamentoIntegrado
	 */
	public Integer getNrSolicitacaoPagamentoIntegrado() {
		return nrSolicitacaoPagamentoIntegrado;
	}

	/**
	 * Set: nrSolicitacaoPagamentoIntegrado.
	 * 
	 * @param nrSolicitacaoPagamentoIntegrado
	 *            the nr solicitacao pagamento integrado
	 */
	public void setNrSolicitacaoPagamentoIntegrado(
			Integer nrSolicitacaoPagamentoIntegrado) {
		this.nrSolicitacaoPagamentoIntegrado = nrSolicitacaoPagamentoIntegrado;
	}

	/**
	 * Get: situacaoSolicitacao.
	 * 
	 * @return situacaoSolicitacao
	 */
	public String getSituacaoSolicitacao() {
		return situacaoSolicitacao;
	}

	/**
	 * Set: situacaoSolicitacao.
	 * 
	 * @param situacaoSolicitacao
	 *            the situacao solicitacao
	 */
	public void setSituacaoSolicitacao(String situacaoSolicitacao) {
		this.situacaoSolicitacao = situacaoSolicitacao;
	}

	/**
	 * Get: vlTarifa.
	 * 
	 * @return vlTarifa
	 */
	public BigDecimal getVlTarifa() {
		return vlTarifa;
	}

	/**
	 * Set: vlTarifa.
	 * 
	 * @param vlTarifa
	 *            the vl tarifa
	 */
	public void setVlTarifa(BigDecimal vlTarifa) {
		this.vlTarifa = vlTarifa;
	}

	/**
	 * Get: comprovantes.
	 * 
	 * @return comprovantes
	 */
	public List<OcorrenciasDetalharSaidaDTO> getComprovantes() {
		return comprovantes;
	}

	/**
	 * Set: comprovantes.
	 * 
	 * @param comprovantes
	 *            the comprovantes
	 */
	public void setComprovantes(List<OcorrenciasDetalharSaidaDTO> comprovantes) {
		this.comprovantes = comprovantes;
	}

	/**
	 * Get: departamento.
	 * 
	 * @return departamento
	 */
	public String getDepartamento() {
		return departamento;
	}

	/**
	 * Set: departamento.
	 * 
	 * @param departamento
	 *            the departamento
	 */
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	/**
	 * Get: solEmissaoMovClientePagServiceImpl.
	 * 
	 * @return solEmissaoMovClientePagServiceImpl
	 */
	public ISolEmissaoMovClientePagService getSolEmissaoMovClientePagServiceImpl() {
		return solEmissaoMovClientePagServiceImpl;
	}

	/**
	 * Set: solEmissaoMovClientePagServiceImpl.
	 * 
	 * @param solEmissaoMovClientePagServiceImpl
	 *            the sol emissao mov cliente pag service impl
	 */
	public void setSolEmissaoMovClientePagServiceImpl(
			ISolEmissaoMovClientePagService solEmissaoMovClientePagServiceImpl) {
		this.solEmissaoMovClientePagServiceImpl = solEmissaoMovClientePagServiceImpl;
	}

	/**
	 * Get: dtFimPeriodoPagamento.
	 * 
	 * @return dtFimPeriodoPagamento
	 */
	public Date getDtFimPeriodoPagamento() {
		return dtFimPeriodoPagamento;
	}

	/**
	 * Set: dtFimPeriodoPagamento.
	 * 
	 * @param dtFimPeriodoPagamento
	 *            the dt fim periodo pagamento
	 */
	public void setDtFimPeriodoPagamento(Date dtFimPeriodoPagamento) {
		this.dtFimPeriodoPagamento = dtFimPeriodoPagamento;
	}

	/**
	 * Get: dtInicioPeriodoPagamento.
	 * 
	 * @return dtInicioPeriodoPagamento
	 */
	public Date getDtInicioPeriodoPagamento() {
		return dtInicioPeriodoPagamento;
	}

	/**
	 * Set: dtInicioPeriodoPagamento.
	 * 
	 * @param dtInicioPeriodoPagamento
	 *            the dt inicio periodo pagamento
	 */
	public void setDtInicioPeriodoPagamento(Date dtInicioPeriodoPagamento) {
		this.dtInicioPeriodoPagamento = dtInicioPeriodoPagamento;
	}

	/**
	 * Get: listaModalidadeFiltro.
	 * 
	 * @return listaModalidadeFiltro
	 */
	public List<SelectItem> getListaModalidadeFiltro() {
		return listaModalidadeFiltro;
	}

	/**
	 * Set: listaModalidadeFiltro.
	 * 
	 * @param listaModalidadeFiltro
	 *            the lista modalidade filtro
	 */
	public void setListaModalidadeFiltro(List<SelectItem> listaModalidadeFiltro) {
		this.listaModalidadeFiltro = listaModalidadeFiltro;
	}

	/**
	 * Get: listaTipoServicoFiltro.
	 * 
	 * @return listaTipoServicoFiltro
	 */
	public List<SelectItem> getListaTipoServicoFiltro() {
		return listaTipoServicoFiltro;
	}

	/**
	 * Set: listaTipoServicoFiltro.
	 * 
	 * @param listaTipoServicoFiltro
	 *            the lista tipo servico filtro
	 */
	public void setListaTipoServicoFiltro(
			List<SelectItem> listaTipoServicoFiltro) {
		this.listaTipoServicoFiltro = listaTipoServicoFiltro;
	}

	/**
	 * Get: modalidadeFiltro.
	 * 
	 * @return modalidadeFiltro
	 */
	public Integer getModalidadeFiltro() {
		return modalidadeFiltro;
	}

	/**
	 * Set: modalidadeFiltro.
	 * 
	 * @param modalidadeFiltro
	 *            the modalidade filtro
	 */
	public void setModalidadeFiltro(Integer modalidadeFiltro) {
		this.modalidadeFiltro = modalidadeFiltro;
	}

	/**
	 * Get: dsModalidadeFiltro.
	 * 
	 * @return dsModalidadeFiltro
	 */
	private String getDsModalidadeFiltro() {
		String dsModalidade = "";
		if (getModalidadeFiltro() == 0 && getTipoServicoFiltro() != 0) {
			dsModalidade = "TODOS";
		} else if (getModalidadeFiltro() != 0) {
			dsModalidade = this.getListaModalidadeFiltroHash().get(
					this.getModalidadeFiltro());
		}
		return dsModalidade;
	}

	/**
	 * Get: tipoServicoFiltro.
	 * 
	 * @return tipoServicoFiltro
	 */
	public Integer getTipoServicoFiltro() {
		return tipoServicoFiltro;
	}

	/**
	 * Set: tipoServicoFiltro.
	 * 
	 * @param tipoServicoFiltro
	 *            the tipo servico filtro
	 */
	public void setTipoServicoFiltro(Integer tipoServicoFiltro) {
		this.tipoServicoFiltro = tipoServicoFiltro;
	}

	/**
	 * Get: listaModalidadeFiltroHash.
	 * 
	 * @return listaModalidadeFiltroHash
	 */
	public Map<Integer, String> getListaModalidadeFiltroHash() {
		return listaModalidadeFiltroHash;
	}

	/**
	 * Set lista modalidade filtro hash.
	 * 
	 * @param listaModalidadeFiltroHash
	 *            the lista modalidade filtro hash
	 */
	public void setListaModalidadeFiltroHash(
			Map<Integer, String> listaModalidadeFiltroHash) {
		this.listaModalidadeFiltroHash = listaModalidadeFiltroHash;
	}

	/**
	 * Get: listaTipoServicoFiltroHash.
	 * 
	 * @return listaTipoServicoFiltroHash
	 */
	public Map<Integer, String> getListaTipoServicoFiltroHash() {
		return listaTipoServicoFiltroHash;
	}

	/**
	 * Set lista tipo servico filtro hash.
	 * 
	 * @param listaTipoServicoFiltroHash
	 *            the lista tipo servico filtro hash
	 */
	public void setListaTipoServicoFiltroHash(
			Map<Integer, String> listaTipoServicoFiltroHash) {
		this.listaTipoServicoFiltroHash = listaTipoServicoFiltroHash;
	}

	/**
	 * Get: cdFavorecido.
	 * 
	 * @return cdFavorecido
	 */
	public Long getCdFavorecido() {
		return cdFavorecido;
	}

	/**
	 * Set: cdFavorecido.
	 * 
	 * @param cdFavorecido
	 *            the cd favorecido
	 */
	public void setCdFavorecido(Long cdFavorecido) {
		this.cdFavorecido = cdFavorecido;
	}

	/**
	 * Get: inscricaoFavorecido.
	 * 
	 * @return inscricaoFavorecido
	 */
	public Long getInscricaoFavorecido() {
		return inscricaoFavorecido;
	}

	/**
	 * Set: inscricaoFavorecido.
	 * 
	 * @param inscricaoFavorecido
	 *            the inscricao favorecido
	 */
	public void setInscricaoFavorecido(Long inscricaoFavorecido) {
		this.inscricaoFavorecido = inscricaoFavorecido;
	}

	/**
	 * Get: listaTipoInscricaoFiltro.
	 * 
	 * @return listaTipoInscricaoFiltro
	 */
	public List<SelectItem> getListaTipoInscricaoFiltro() {
		return listaTipoInscricaoFiltro;
	}

	/**
	 * Set: listaTipoInscricaoFiltro.
	 * 
	 * @param listaTipoInscricaoFiltro
	 *            the lista tipo inscricao filtro
	 */
	public void setListaTipoInscricaoFiltro(
			List<SelectItem> listaTipoInscricaoFiltro) {
		this.listaTipoInscricaoFiltro = listaTipoInscricaoFiltro;
	}

	/**
	 * Get: radioIncluirFavorecido.
	 * 
	 * @return radioIncluirFavorecido
	 */
	public String getRadioIncluirFavorecido() {
		return radioIncluirFavorecido;
	}

	/**
	 * Set: radioIncluirFavorecido.
	 * 
	 * @param radioIncluirFavorecido
	 *            the radio incluir favorecido
	 */
	public void setRadioIncluirFavorecido(String radioIncluirFavorecido) {
		this.radioIncluirFavorecido = radioIncluirFavorecido;
	}

	/**
	 * Get: tipoInscricaoFiltro.
	 * 
	 * @return tipoInscricaoFiltro
	 */
	public Integer getTipoInscricaoFiltro() {
		return tipoInscricaoFiltro;
	}

	/**
	 * Set: tipoInscricaoFiltro.
	 * 
	 * @param tipoInscricaoFiltro
	 *            the tipo inscricao filtro
	 */
	public void setTipoInscricaoFiltro(Integer tipoInscricaoFiltro) {
		this.tipoInscricaoFiltro = tipoInscricaoFiltro;
	}

	/**
	 * Get: agenciaOperadora.
	 * 
	 * @return agenciaOperadora
	 */
	public String getAgenciaOperadora() {
		return agenciaOperadora;
	}

	/**
	 * Set: agenciaOperadora.
	 * 
	 * @param agenciaOperadora
	 *            the agencia operadora
	 */
	public void setAgenciaOperadora(String agenciaOperadora) {
		this.agenciaOperadora = agenciaOperadora;
	}

	/**
	 * Get: departamentoFiltro.
	 * 
	 * @return departamentoFiltro
	 */
	public String getDepartamentoFiltro() {
		return departamentoFiltro;
	}

	/**
	 * Set: departamentoFiltro.
	 * 
	 * @param departamentoFiltro
	 *            the departamento filtro
	 */
	public void setDepartamentoFiltro(String departamentoFiltro) {
		this.departamentoFiltro = departamentoFiltro;
	}

	/**
	 * Get: radioAgenciaDepto.
	 * 
	 * @return radioAgenciaDepto
	 */
	public String getRadioAgenciaDepto() {
		return radioAgenciaDepto;
	}

	/**
	 * Set: radioAgenciaDepto.
	 * 
	 * @param radioAgenciaDepto
	 *            the radio agencia depto
	 */
	public void setRadioAgenciaDepto(String radioAgenciaDepto) {
		this.radioAgenciaDepto = radioAgenciaDepto;
	}

	/**
	 * Get: radioInfoAdicionais.
	 * 
	 * @return radioInfoAdicionais
	 */
	public String getRadioInfoAdicionais() {
		return radioInfoAdicionais;
	}

	/**
	 * Set: radioInfoAdicionais.
	 * 
	 * @param radioInfoAdicionais
	 *            the radio info adicionais
	 */
	public void setRadioInfoAdicionais(String radioInfoAdicionais) {
		this.radioInfoAdicionais = radioInfoAdicionais;
	}

	/**
	 * Get: solEmissaoComprovantePagtoClientePagServiceImpl.
	 * 
	 * @return solEmissaoComprovantePagtoClientePagServiceImpl
	 */
	public ISolEmissaoComprovantePagtoClientePagService getSolEmissaoComprovantePagtoClientePagServiceImpl() {
		return solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	/**
	 * Set: solEmissaoComprovantePagtoClientePagServiceImpl.
	 * 
	 * @param solEmissaoComprovantePagtoClientePagServiceImpl
	 *            the sol emissao comprovante pagto cliente pag service impl
	 */
	public void setSolEmissaoComprovantePagtoClientePagServiceImpl(
			ISolEmissaoComprovantePagtoClientePagService solEmissaoComprovantePagtoClientePagServiceImpl) {
		this.solEmissaoComprovantePagtoClientePagServiceImpl = solEmissaoComprovantePagtoClientePagServiceImpl;
	}

	/**
	 * Get: cdPessoaJuridicaDepartamento.
	 * 
	 * @return cdPessoaJuridicaDepartamento
	 */
	public Long getCdPessoaJuridicaDepartamento() {
		return cdPessoaJuridicaDepartamento;
	}

	/**
	 * Set: cdPessoaJuridicaDepartamento.
	 * 
	 * @param cdPessoaJuridicaDepartamento
	 *            the cd pessoa juridica departamento
	 */
	public void setCdPessoaJuridicaDepartamento(
			Long cdPessoaJuridicaDepartamento) {
		this.cdPessoaJuridicaDepartamento = cdPessoaJuridicaDepartamento;
	}

	/**
	 * Get: cdTipoContratoNegocio.
	 * 
	 * @return cdTipoContratoNegocio
	 */
	public Integer getCdTipoContratoNegocio() {
		return cdTipoContratoNegocio;
	}

	/**
	 * Set: cdTipoContratoNegocio.
	 * 
	 * @param cdTipoContratoNegocio
	 *            the cd tipo contrato negocio
	 */
	public void setCdTipoContratoNegocio(Integer cdTipoContratoNegocio) {
		this.cdTipoContratoNegocio = cdTipoContratoNegocio;
	}

	/**
	 * Get: codAgenciaOperadora.
	 * 
	 * @return codAgenciaOperadora
	 */
	public Integer getCodAgenciaOperadora() {
		return codAgenciaOperadora;
	}

	/**
	 * Set: codAgenciaOperadora.
	 * 
	 * @param codAgenciaOperadora
	 *            the cod agencia operadora
	 */
	public void setCodAgenciaOperadora(Integer codAgenciaOperadora) {
		this.codAgenciaOperadora = codAgenciaOperadora;
	}

	/**
	 * Get: nrSequenciaDepartamento.
	 * 
	 * @return nrSequenciaDepartamento
	 */
	public Integer getNrSequenciaDepartamento() {
		return nrSequenciaDepartamento;
	}

	/**
	 * Set: nrSequenciaDepartamento.
	 * 
	 * @param nrSequenciaDepartamento
	 *            the nr sequencia departamento
	 */
	public void setNrSequenciaDepartamento(Integer nrSequenciaDepartamento) {
		this.nrSequenciaDepartamento = nrSequenciaDepartamento;
	}

	/**
	 * Get: cepFormatado.
	 * 
	 * @return cepFormatado
	 */
	public String getCepFormatado() {
		return cepFormatado;
	}

	/**
	 * Set: cepFormatado.
	 * 
	 * @param cepFormatado
	 *            the cep formatado
	 */
	public void setCepFormatado(String cepFormatado) {
		this.cepFormatado = cepFormatado;
	}

	/**
	 * Is disable argumentos consulta.
	 * 
	 * @return true, if is disable argumentos consulta
	 */
	public boolean isDisableArgumentosConsulta() {
		return disableArgumentosConsulta;
	}

	/**
	 * Set: disableArgumentosConsulta.
	 * 
	 * @param disableArgumentosConsulta
	 *            the disable argumentos consulta
	 */
	public void setDisableArgumentosConsulta(boolean disableArgumentosConsulta) {
		this.disableArgumentosConsulta = disableArgumentosConsulta;
	}

	/**
	 * Is desabilita desconto tarifa.
	 * 
	 * @return true, if is desabilita desconto tarifa
	 */
	public boolean isDesabilitaDescontoTarifa() {
		return desabilitaDescontoTarifa;
	}

	/**
	 * Set: desabilitaDescontoTarifa.
	 * 
	 * @param desabilitaDescontoTarifa
	 *            the desabilita desconto tarifa
	 */
	public void setDesabilitaDescontoTarifa(boolean desabilitaDescontoTarifa) {
		this.desabilitaDescontoTarifa = desabilitaDescontoTarifa;
	}

	/**
	 * Get: filtroEnderecoEmailBean.
	 * 
	 * @return filtroEnderecoEmailBean
	 */
	public FiltroEnderecoEmailBean getFiltroEnderecoEmailBean() {
		return filtroEnderecoEmailBean;
	}

	/**
	 * Set: filtroEnderecoEmailBean.
	 * 
	 * @param filtroEnderecoEmailBean
	 *            the filtro endereco email bean
	 */
	public void setFiltroEnderecoEmailBean(
			FiltroEnderecoEmailBean filtroEnderecoEmailBean) {
		this.filtroEnderecoEmailBean = filtroEnderecoEmailBean;
	}

	/**
	 * Is habilita campo endereco.
	 * 
	 * @return true, if is habilita campo endereco
	 */
	public boolean isHabilitaCampoEndereco() {
		return habilitaCampoEndereco;
	}

	/**
	 * Set: habilitaCampoEndereco.
	 * 
	 * @param habilitaCampoEndereco
	 *            the habilita campo endereco
	 */
	public void setHabilitaCampoEndereco(boolean habilitaCampoEndereco) {
		this.habilitaCampoEndereco = habilitaCampoEndereco;
	}

	/**
	 * Get: inscricaoFavorecidoFormatado.
	 * 
	 * @return inscricaoFavorecidoFormatado
	 */
	public String getInscricaoFavorecidoFormatado() {
		return inscricaoFavorecidoFormatado;
	}

	/**
	 * Set: inscricaoFavorecidoFormatado.
	 * 
	 * @param inscricaoFavorecidoFormatado
	 *            the inscricao favorecido formatado
	 */
	public void setInscricaoFavorecidoFormatado(
			String inscricaoFavorecidoFormatado) {
		this.inscricaoFavorecidoFormatado = inscricaoFavorecidoFormatado;
	}

	/**
	 * Is indicador desconto bloqueio.
	 * 
	 * @return true, if is indicador desconto bloqueio
	 */
	public boolean isIndicadorDescontoBloqueio() {
		return indicadorDescontoBloqueio;
	}

	/**
	 * Get: vlTarifaPadrao.
	 * 
	 * @return vlTarifaPadrao
	 */
	public BigDecimal getVlTarifaPadrao() {
		return vlTarifaPadrao;
	}

	/**
	 * Set: vlTarifaPadrao.
	 * 
	 * @param vlTarifaPadrao
	 *            the vl tarifa padrao
	 */
	public void setVlTarifaPadrao(BigDecimal vlTarifaPadrao) {
		this.vlTarifaPadrao = vlTarifaPadrao;
	}

	/**
	 * Get: vlrTarifaAtual.
	 * 
	 * @return vlrTarifaAtual
	 */
	public BigDecimal getVlrTarifaAtual() {
		return vlrTarifaAtual;
	}

	/**
	 * Set: vlrTarifaAtual.
	 * 
	 * @param vlrTarifaAtual
	 *            the vlr tarifa atual
	 */
	public void setVlrTarifaAtual(BigDecimal vlrTarifaAtual) {
		this.vlrTarifaAtual = vlrTarifaAtual;
	}

	/**
	 * Is disabled percentual desconto.
	 * 
	 * @return true, if is disabled percentual desconto
	 */
	public boolean isDisabledPercentualDesconto() {
		return disabledPercentualDesconto;
	}

	/**
	 * Is disabled btn calcular.
	 * 
	 * @return true, if is disabled btn calcular
	 */
	public boolean isDisabledBtnCalcular() {
		return disabledBtnCalcular;
	}

	public void setIndicadorDescontoBloqueio(boolean indicadorDescontoBloqueio) {
		this.indicadorDescontoBloqueio = indicadorDescontoBloqueio;
	}

	public void setDisabledPercentualDesconto(boolean disabledPercentualDesconto) {
		this.disabledPercentualDesconto = disabledPercentualDesconto;
	}

	public void setDisabledBtnCalcular(boolean disabledBtnCalcular) {
		this.disabledBtnCalcular = disabledBtnCalcular;
	}

}