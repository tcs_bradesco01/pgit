/*
 * Nome: br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.cadproccontrole
 * 
 * Compilador:
 * 
 * Prop�sito:
 * 
 * Data da cria��o: 17/09/2014
 * 
 * Par�metros de Compila��o:
 */
package br.com.bradesco.web.pgit.view.bean.manutencaoapoioproduto.cadproccontrole;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import br.com.bradesco.web.aq.application.error.BradescoViewException;
import br.com.bradesco.web.aq.application.error.BradescoViewException.BradescoViewExceptionActionType;
import br.com.bradesco.web.aq.application.error.i18n.MessageHelperUtils;
import br.com.bradesco.web.aq.application.pdc.adapter.exception.PdcAdapterFunctionalException;
import br.com.bradesco.web.aq.application.util.faces.BradescoFacesUtils;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.ICadProcControleService;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.HistoricoBloqueioEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.HistoricoBloqueioSaidaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.cadproccontrole.bean.ProcessoMassivoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.IComboService;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.CentroCustoSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.PeriodicidadeEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.PeriodicidadeSaidaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoProcessoEntradaDTO;
import br.com.bradesco.web.pgit.service.business.combo.bean.TipoProcessoSaidaDTO;
import br.com.bradesco.web.pgit.utils.constants.ErrorMessageConstants;

/**
 * Nome: CadastroProcessosControleBean
 * <p>
 * Prop�sito:
 * </p>.
 *
 * @author : todo!
 * @version :
 */
public class CadastroProcessosControleBean {
	
	//Constante utilizada para evitar redund�ncia apontada pelo Sonar
	/** Atributo CON_CADASTRO_PROCESSOS_CONTROLE. */
	private static final String CON_CADASTRO_PROCESSOS_CONTROLE = "conCadastroProcessosControle";
	
	/** Atributo itemSelecionadoConsulta. */
	private ProcessoMassivoSaidaDTO itemSelecionadoConsulta;

	/** Atributo itemSelecionadoHistorico. */
	private String itemSelecionadoHistorico;

	/** Atributo listaGridHistorico. */
	private List<HistoricoBloqueioSaidaDTO> listaGridHistorico = new ArrayList<HistoricoBloqueioSaidaDTO>();

	/** Atributo historicoBloqueioFiltro. */
	private HistoricoBloqueioEntradaDTO historicoBloqueioFiltro = new HistoricoBloqueioEntradaDTO();

	/** Atributo historicoBloqueioDetalhe. */
	private HistoricoBloqueioSaidaDTO historicoBloqueioDetalhe = new HistoricoBloqueioSaidaDTO();

	/** Atributo centroCustoLupaFiltro. */
	private String centroCustoLupaFiltro;
	
	/** Atributo centroCustoLupa. */
	private String centroCustoLupa;

	/** Atributo processoMassivoFiltro. */
	private ProcessoMassivoEntradaDTO processoMassivoFiltro = new ProcessoMassivoEntradaDTO();
	
	/** Atributo processoMassivoInclusao. */
	private ProcessoMassivoEntradaDTO processoMassivoInclusao = new ProcessoMassivoEntradaDTO();
	
	/** Atributo processoMassivoAlteracao. */
	private ProcessoMassivoEntradaDTO processoMassivoAlteracao = new ProcessoMassivoEntradaDTO();
	
	/** Atributo processoMassivoDetalhe. */
	private ProcessoMassivoSaidaDTO processoMassivoDetalhe = new ProcessoMassivoSaidaDTO();
	
	/** Atributo processoMassivoBloqueio. */
	private ProcessoMassivoEntradaDTO processoMassivoBloqueio = new ProcessoMassivoEntradaDTO();

	/** Atributo listaCentroCusto. */
	private List<CentroCustoSaidaDTO> listaCentroCusto = new ArrayList<CentroCustoSaidaDTO>();
	
	/** Atributo listaCentroCustoLupa. */
	private List<CentroCustoSaidaDTO> listaCentroCustoLupa = new ArrayList<CentroCustoSaidaDTO>();

	/** Atributo listaTipoProcesso. */
	private List<TipoProcessoSaidaDTO> listaTipoProcesso = new ArrayList<TipoProcessoSaidaDTO>();
	
	/** Atributo listaPeriodicidadeExecucao. */
	private List<PeriodicidadeSaidaDTO> listaPeriodicidadeExecucao = new ArrayList<PeriodicidadeSaidaDTO>();

	/** Atributo listaGridPesquisa. */
	private List<ProcessoMassivoSaidaDTO> listaGridPesquisa = new ArrayList<ProcessoMassivoSaidaDTO>();

	/** Atributo listaSituacao. */
	private List<SelectItem> listaSituacao = new ArrayList<SelectItem>();

	/** Atributo comboService. */
	private IComboService comboService;
	
	/** Atributo cadProcControleService. */
	private ICadProcControleService cadProcControleService;  
	
	/** Atributo btoAcionado. */
	private Boolean btoAcionado;
	
	/** Atributo btoAcionadoHistorico. */
	private Boolean btoAcionadoHistorico;

	/** Atributo selecionadoHistorico. */
	private HistoricoBloqueioSaidaDTO selecionadoHistorico = new HistoricoBloqueioSaidaDTO();

	/**
	 * Get: selecionadoHistorico.
	 *
	 * @return selecionadoHistorico
	 */
	public HistoricoBloqueioSaidaDTO getSelecionadoHistorico() {
		return selecionadoHistorico;
	}

	/**
	 * Set: selecionadoHistorico.
	 *
	 * @param selecionadoHistorico the selecionado historico
	 */
	public void setSelecionadoHistorico(
			HistoricoBloqueioSaidaDTO selecionadoHistorico) {
		this.selecionadoHistorico = selecionadoHistorico;
	}

	/**
	 * Navegar tela inicial.
	 *
	 * @return the string
	 */
	public String navegarTelaInicial() {
		try{
			setListaTipoProcesso(getComboService().listarTipoProcesso(new TipoProcessoEntradaDTO()));
		}catch(PdcAdapterFunctionalException e){
			listaTipoProcesso = new ArrayList<TipoProcessoSaidaDTO>();
		}
		
		try{
			setListaPeriodicidadeExecucao(getComboService().listarPeriodicidade(new PeriodicidadeEntradaDTO()));
		}catch(PdcAdapterFunctionalException e){
			listaPeriodicidadeExecucao = new ArrayList<PeriodicidadeSaidaDTO>();
		}
		setBtoAcionado(false);
		setListaSituacao(obterSituacao());

		return CON_CADASTRO_PROCESSOS_CONTROLE;
	}

	/**
	 * Pesquisar.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisar(ActionEvent evt) {
		carregaListaProcessos();

		return "ok";
	}

	/**
	 * Pesquisar historico.
	 *
	 * @param evt the evt
	 * @return the string
	 */
	public String pesquisarHistorico(ActionEvent evt) {
		carregaListaHistorico();

		return "ok";
	}

	/**
	 * Incluir.
	 *
	 * @return the string
	 */
	public String incluir() {
		setCentroCustoLupa("");
		setProcessoMassivoInclusao(new ProcessoMassivoEntradaDTO());
		setListaCentroCustoLupa(new ArrayList<CentroCustoSaidaDTO>());

		return "INCLUIR";
	}

	/**
	 * Avancar incluir.
	 *
	 * @return the string
	 */
	public String avancarIncluir() {
		getProcessoMassivoInclusao().setDsSistema(obterDescricaoCentroCustoLupa(getProcessoMassivoInclusao().getCdSistema()));
		getProcessoMassivoInclusao().setDsTipoProcessoSistema(obterDescricaoTipoProcesso(getProcessoMassivoInclusao().getCdTipoProcessoSistema()));
		getProcessoMassivoInclusao().setDsPeriodicidade(obterDescricaoPeriodicidade(getProcessoMassivoInclusao().getCdPeriodicidade()));

		return "AVANCAR_INCLUIR";
	}
	
	/**
	 * Avancar alterar.
	 *
	 * @return the string
	 */
	public String avancarAlterar() {
		getProcessoMassivoAlteracao().setDsTipoProcessoSistema(obterDescricaoTipoProcesso(getProcessoMassivoAlteracao().getCdTipoProcessoSistema()));
		getProcessoMassivoAlteracao().setDsPeriodicidade(obterDescricaoPeriodicidade(getProcessoMassivoAlteracao().getCdPeriodicidade()));

		return "AVANCAR_ALTERAR";
	}

	/**
	 * Obter centro custo filtro.
	 */
	public void obterCentroCustoFiltro() {
		setListaCentroCusto(new ArrayList<CentroCustoSaidaDTO>());
		if (getCentroCustoLupaFiltro() != null && !getCentroCustoLupaFiltro().trim().equals("")) {
			setListaCentroCusto(getComboService().listarCentroCusto(new CentroCustoEntradaDTO(getCentroCustoLupaFiltro().toUpperCase())));
		}
	}

	/**
	 * Obter centro custo.
	 */
	public void obterCentroCusto() {
		setListaCentroCustoLupa(new ArrayList<CentroCustoSaidaDTO>());
		if (getCentroCustoLupa() != null && !getCentroCustoLupa().trim().equals("")) {
			setListaCentroCustoLupa(getComboService().listarCentroCusto(new CentroCustoEntradaDTO(getCentroCustoLupa().toUpperCase())));
		}
	}

	/**
	 * Obter descricao periodicidade.
	 *
	 * @param periodicidade the periodicidade
	 * @return the string
	 */
	private String obterDescricaoPeriodicidade(Integer periodicidade) {
		if (periodicidade == null) {
			return "";
		}
		
		if (getListaPeriodicidadeExecucao() != null) {
			PeriodicidadeSaidaDTO periodicidadeDTO = new PeriodicidadeSaidaDTO();
			periodicidadeDTO.setCdPeriodicidade(periodicidade);
			int indexPeriodicidade = getListaPeriodicidadeExecucao().indexOf(periodicidadeDTO);

			if (indexPeriodicidade < 0) {
				return "";
			}

			return getListaPeriodicidadeExecucao().get(indexPeriodicidade).getDsPeriodicidade();
		}
		return "";
	}

	/**
	 * Obter descricao tipo processo.
	 *
	 * @param tipoProcesso the tipo processo
	 * @return the string
	 */
	private String obterDescricaoTipoProcesso(Integer tipoProcesso) {
		if (tipoProcesso == null) {
			return "";
		}

		if (getListaTipoProcesso() != null) {
			TipoProcessoSaidaDTO tipoProcessoDTO = new TipoProcessoSaidaDTO();
			tipoProcessoDTO.setCdTipoProcesso(tipoProcesso);
			int indexTipoProcesso = getListaTipoProcesso().indexOf(tipoProcessoDTO);

			if (indexTipoProcesso < 0) {
				return "";
			}

			return getListaTipoProcesso().get(indexTipoProcesso).getDsTipoProcesso();
		}
		return "";
	}
	
	/**
	 * Obter descricao centro custo lupa.
	 *
	 * @param centroCusto the centro custo
	 * @return the string
	 */
	private String obterDescricaoCentroCustoLupa(String centroCusto) {
		if (centroCusto == null) {
			return "";
		}

		if (getListaCentroCustoLupa() != null) {
			CentroCustoSaidaDTO centroCustoSaidaDTO = new CentroCustoSaidaDTO();
			centroCustoSaidaDTO.setCdCentroCusto(centroCusto);
			int indexCentroCusto = getListaCentroCustoLupa().indexOf(centroCustoSaidaDTO);

			if (indexCentroCusto < 0) {
				return "";
			}

			return getListaCentroCustoLupa().get(indexCentroCusto).getDsCentroCusto();
		}
		return "";
	}

	/**
	 * Limpar dados.
	 *
	 * @return the string
	 */
	public String limparDados() {
		setProcessoMassivoFiltro(new ProcessoMassivoEntradaDTO());
		setCentroCustoLupaFiltro("");
		setListaCentroCusto(new ArrayList<CentroCustoSaidaDTO>());
		setItemSelecionadoConsulta(null);
		setListaGridPesquisa(new ArrayList<ProcessoMassivoSaidaDTO>());
		setBtoAcionado(false);
		return "ok";
	}

	/**
	 * Limpar historico.
	 *
	 * @return the string
	 */
	public String limparHistorico() {
		getHistoricoBloqueioFiltro().setDtDe(new Date());
		getHistoricoBloqueioFiltro().setDtAte(new Date());

		setItemSelecionadoHistorico(null);
		setListaGridHistorico(new ArrayList<HistoricoBloqueioSaidaDTO>());
		setBtoAcionadoHistorico(false);
		return null;
	}

	/**
	 * Confirmar incluir.
	 *
	 * @return the string
	 */
	public String confirmarIncluir() {
		ProcessoMassivoSaidaDTO processoMassivoSaidaDTO = getCadProcControleService().incluirProcessoMassivo(getProcessoMassivoInclusao());

		BradescoFacesUtils.addInfoModalMessage("(" + processoMassivoSaidaDTO.getCodMensagem() + ") " + processoMassivoSaidaDTO.getMensagem(), CON_CADASTRO_PROCESSOS_CONTROLE, BradescoViewExceptionActionType.ACTION, false);

		setItemSelecionadoConsulta(null);
		
		if(getListaGridPesquisa().size() != 0 ){
			carregaListaProcessos();
		}
		
		return null;
	}

	/**
	 * Confirmar excluir.
	 *
	 * @return the string
	 */
	public String confirmarExcluir() {
		ProcessoMassivoEntradaDTO entradaDTO = new ProcessoMassivoEntradaDTO();
		entradaDTO.setCdSistema(getProcessoMassivoDetalhe().getCdSistema());
		entradaDTO.setCdProcessoSistema(getProcessoMassivoDetalhe().getCdProcessoSistema());
		ProcessoMassivoSaidaDTO processoMassivoSaidaDTO = getCadProcControleService().excluirProcessoMassivo(entradaDTO);

		BradescoFacesUtils.addInfoModalMessage("(" + processoMassivoSaidaDTO.getCodMensagem() + ") " + processoMassivoSaidaDTO.getMensagem(), CON_CADASTRO_PROCESSOS_CONTROLE, BradescoViewExceptionActionType.ACTION, false);

		setItemSelecionadoConsulta(null);
		try {
			carregaListaProcessos();
		} catch (PdcAdapterFunctionalException e) {
			if (e.getCode() != null && e.getCode().endsWith(ErrorMessageConstants.PGIT0003)) {
				setListaGridPesquisa(new ArrayList<ProcessoMassivoSaidaDTO>());
				return null;
			}
			throw e;
		}

		return null;
	}

	/**
	 * Confirmar alterar.
	 *
	 * @return the string
	 */
	public String confirmarAlterar() {
		ProcessoMassivoSaidaDTO processoMassivoSaidaDTO = getCadProcControleService().alterarProcessoMassivo(getProcessoMassivoAlteracao());

		BradescoFacesUtils.addInfoModalMessage("(" + processoMassivoSaidaDTO.getCodMensagem() + ") " + processoMassivoSaidaDTO.getMensagem(), CON_CADASTRO_PROCESSOS_CONTROLE, BradescoViewExceptionActionType.ACTION, false);

		setItemSelecionadoConsulta(null);
		carregaListaProcessos();

		return null;
	}

	/**
	 * Confirmar bloquear.
	 *
	 * @return the string
	 */
	public String confirmarBloquear(){
		ProcessoMassivoSaidaDTO processoMassivoSaidaDTO = getCadProcControleService().bloquearProcessoMassivo(getProcessoMassivoBloqueio());

		BradescoFacesUtils.addInfoModalMessage("(" + processoMassivoSaidaDTO.getCodMensagem() + ") " + processoMassivoSaidaDTO.getMensagem(), CON_CADASTRO_PROCESSOS_CONTROLE, BradescoViewExceptionActionType.ACTION, false);

		setItemSelecionadoConsulta(null);
		carregaListaProcessos();

		return null;
	}
	
	/**
	 * Limpar bloqueio.
	 *
	 * @return the string
	 */
	public String limparBloqueio() {
		getProcessoMassivoBloqueio().setCdSituacaoProcessoSistema(0);
		getProcessoMassivoBloqueio().setDsBloqueioProcessoSistema("");

		return null;
	}
	
	/**
	 * Consultar processos.
	 *
	 * @return the string
	 */
	public String consultarProcessos() {
		this.carregaListaProcessos();

		return "ok";
	}

	/**
	 * Consultar historico.
	 *
	 * @return the string
	 */
	public String consultarHistorico() {
		
		this.carregaListaHistorico();

		return "ok";
	}

	/**
	 * Carrega lista processos.
	 */
	public void carregaListaProcessos() {
		setListaGridPesquisa(new ArrayList<ProcessoMassivoSaidaDTO>());
		try {
			setListaGridPesquisa(getCadProcControleService().listarProcessoMassivo(getProcessoMassivoFiltro()));
			setBtoAcionado(true);
		} catch (PdcAdapterFunctionalException e) {
			setListaGridPesquisa(new ArrayList<ProcessoMassivoSaidaDTO>());
			throw e;
		}
	}

	/**
	 * Limpar alterar.
	 *
	 * @return the string
	 */
	public String limparAlterar() {
		getProcessoMassivoAlteracao().setDsProcessoSistema("");
		getProcessoMassivoAlteracao().setCdNetProcessamentoPgto("");
		getProcessoMassivoAlteracao().setCdJobProcessamentoPgto("");
		getProcessoMassivoAlteracao().setCdPeriodicidade(null);

		return null;
	}

	/**
	 * Carrega lista historico.
	 */
	public void carregaListaHistorico() {
		try {
			setListaGridHistorico(getCadProcControleService().listarHistoricoProcessoMassivo(getHistoricoBloqueioFiltro()));
			setBtoAcionadoHistorico(true);
		} catch (PdcAdapterFunctionalException e) {
			setListaGridHistorico(new ArrayList<HistoricoBloqueioSaidaDTO>());
			throw e;
		}
	}
	
	/**
	 * Excluir.
	 *
	 * @return the string
	 */
	public String excluir() {
		setProcessoMassivoDetalhe(obterDetalheProcessoMassivo());

		return "EXCLUIR";
	}

	/**
	 * Detalhar.
	 *
	 * @return the string
	 */
	public String detalhar() {
		setProcessoMassivoDetalhe(obterDetalheProcessoMassivo());

		return "DETALHAR";
	}

	/**
	 * Historico.
	 *
	 * @return the string
	 */
	public String historico() {
		setBtoAcionadoHistorico(false);
		int indexSelecionado = getListaGridPesquisa().indexOf(getItemSelecionadoConsulta());
		
		if (indexSelecionado < 0) {
			throw new BradescoViewException("Item selecionado n�o encontrado!", "");
		}
		
		setProcessoMassivoDetalhe(getListaGridPesquisa().get(indexSelecionado));

		HistoricoBloqueioEntradaDTO historicoBloqueioEntradaDTO = new HistoricoBloqueioEntradaDTO();
		historicoBloqueioEntradaDTO.setCdSistema(getItemSelecionadoConsulta().getCdSistema());
		historicoBloqueioEntradaDTO.setCdProcessoSistema(getItemSelecionadoConsulta().getCdProcessoSistema());
		setHistoricoBloqueioFiltro(historicoBloqueioEntradaDTO);
		getHistoricoBloqueioFiltro().setDtDe(new Date());
		getHistoricoBloqueioFiltro().setDtAte(new Date());

		setListaGridHistorico(new ArrayList<HistoricoBloqueioSaidaDTO>());

		return "HISTORICO";
	}

	/**
	 * Alterar.
	 *
	 * @return the string
	 */
	public String alterar() {
		setProcessoMassivoDetalhe(obterDetalheProcessoMassivo());

		setCentroCustoLupa(getProcessoMassivoDetalhe().getCdSistema());
		obterCentroCusto();

		ProcessoMassivoEntradaDTO entradaDTO = new ProcessoMassivoEntradaDTO();
		entradaDTO.setCdSistema(getProcessoMassivoDetalhe().getCdSistema());
		entradaDTO.setCdProcessoSistema(getProcessoMassivoDetalhe().getCdProcessoSistema());
		entradaDTO.setCdTipoProcessoSistema(getProcessoMassivoDetalhe().getCdTipoProcessoSistema());
		entradaDTO.setCdPeriodicidade(getProcessoMassivoDetalhe().getCdPeriodicidade());
		entradaDTO.setDsProcessoSistema(getProcessoMassivoDetalhe().getDsProcessoSistema());
		entradaDTO.setCdNetProcessamentoPgto(getProcessoMassivoDetalhe().getCdNetProcessamentoPgto());
		entradaDTO.setCdJobProcessamentoPgto(getProcessoMassivoDetalhe().getCdJobProcessamentoPgto());
		entradaDTO.setDsTipoProcessoSistema(obterDescricaoTipoProcesso(getProcessoMassivoDetalhe().getCdTipoProcessoSistema()));
		setProcessoMassivoAlteracao(entradaDTO);
		
		return "ALTERAR";
	}

	/**
	 * Bloquear.
	 *
	 * @return the string
	 */
	public String bloquear() {
		setProcessoMassivoDetalhe(obterDetalheProcessoMassivo());

		ProcessoMassivoEntradaDTO processoMassivoEntradaDTO = new ProcessoMassivoEntradaDTO();
		processoMassivoEntradaDTO.setCdSistema(getProcessoMassivoDetalhe().getCdSistema());
		processoMassivoEntradaDTO.setCdProcessoSistema(getProcessoMassivoDetalhe().getCdProcessoSistema());
		processoMassivoEntradaDTO.setCdSituacaoProcessoSistema(getProcessoMassivoDetalhe().getCdSituacaoProcessoSistema());
		setProcessoMassivoBloqueio(processoMassivoEntradaDTO);

		return "BLOQUEAR";
	}

	/**
	 * Obter detalhe processo massivo.
	 *
	 * @return the processo massivo saida dto
	 */
	private ProcessoMassivoSaidaDTO obterDetalheProcessoMassivo() {
		ProcessoMassivoEntradaDTO entradaDTO = new ProcessoMassivoEntradaDTO();
		entradaDTO.setCdSistema(getItemSelecionadoConsulta().getCdSistema());
		entradaDTO.setCdProcessoSistema(getItemSelecionadoConsulta().getCdProcessoSistema());
		return getCadProcControleService().detalharProcessoMassivo(entradaDTO);
	}

	/**
	 * Detalhar historico.
	 *
	 * @return the string
	 */
	public String detalharHistorico() {
		 selecionadoHistorico = getListaGridHistorico().get(Integer.parseInt(getItemSelecionadoHistorico()));
		
		setHistoricoBloqueioDetalhe(obterDetalheHistoricoProcessoMassivo());

		return "DETALHAR_HISTORICO";
	}
	
	/**
	 * Get: selectItemLista.
	 *
	 * @return selectItemLista
	 */
	public List<SelectItem> getSelectItemLista() {
		if (this.listaGridHistorico == null || this.listaGridHistorico.isEmpty()) {
			return new ArrayList<SelectItem>();
		} else {
			List<SelectItem> list = new ArrayList<SelectItem>();
		for (int index = 0; index < this.listaGridHistorico.size(); index++) {
			list.add(new SelectItem(String.valueOf(index), ""));
		}
		return list;
		}
	}

	/**
	 * Obter detalhe historico processo massivo.
	 *
	 * @return the historico bloqueio saida dto
	 */
	private HistoricoBloqueioSaidaDTO obterDetalheHistoricoProcessoMassivo() {
		selecionadoHistorico = getListaGridHistorico().get(Integer.parseInt(getItemSelecionadoHistorico()));
		HistoricoBloqueioEntradaDTO entradaDTO = new HistoricoBloqueioEntradaDTO();
		entradaDTO.setCdSistema(getHistoricoBloqueioFiltro().getCdSistema());
		entradaDTO.setCdProcessoSistema(getHistoricoBloqueioFiltro().getCdProcessoSistema());
		entradaDTO.setDtConsulta(selecionadoHistorico.getHrBloqueioProcessoSistema());
		return getCadProcControleService().detalharHistoricoProcessoMassivo(entradaDTO);
	}

	/**
	 * Obter situacao.
	 *
	 * @return the list< select item>
	 */
	private List<SelectItem> obterSituacao() {
		List<SelectItem> listaSituacaoAux = new ArrayList<SelectItem>();
		listaSituacaoAux.add(new SelectItem(1, MessageHelperUtils.getI18nMessage("desbCadastroProcessosControle_label_situacao_liberado")));
		listaSituacaoAux.add(new SelectItem(2, MessageHelperUtils.getI18nMessage("desbCadastroProcessosControle_label_situacao_bloqueado")));
		return listaSituacaoAux;
	}
	
	/**
	 * Voltar.
	 *
	 * @return the string
	 */
	public String voltar(){
		
		setItemSelecionadoConsulta(null);
		setListaGridPesquisa(new ArrayList<ProcessoMassivoSaidaDTO>());
	
		
		return "VOLTAR_DADOS";
	}
	
	/**
	 * Voltar historico.
	 *
	 * @return the string
	 */
	public String voltarHistorico(){
		setItemSelecionadoHistorico(null);
		
		return "VOLTAR_HISTORICO";
	}

	/**
	 * Get: listaGridPesquisa.
	 *
	 * @return listaGridPesquisa
	 */
	public List<ProcessoMassivoSaidaDTO> getListaGridPesquisa() {
		return listaGridPesquisa;
	}

	/**
	 * Set: listaGridPesquisa.
	 *
	 * @param listaGridPesquisa the lista grid pesquisa
	 */
	public void setListaGridPesquisa(List<ProcessoMassivoSaidaDTO> listaGridPesquisa) {
		this.listaGridPesquisa = listaGridPesquisa;
	}

	/**
	 * Get: listaGridHistorico.
	 *
	 * @return listaGridHistorico
	 */
	public List<HistoricoBloqueioSaidaDTO> getListaGridHistorico() {
		return listaGridHistorico;
	}

	/**
	 * Set: listaGridHistorico.
	 *
	 * @param listaGridHistorico the lista grid historico
	 */
	public void setListaGridHistorico(List<HistoricoBloqueioSaidaDTO> listaGridHistorico) {
		this.listaGridHistorico = listaGridHistorico;
	}

	/**
	 * Get: itemSelecionadoConsulta.
	 *
	 * @return itemSelecionadoConsulta
	 */
	public ProcessoMassivoSaidaDTO getItemSelecionadoConsulta() {
		return itemSelecionadoConsulta;
	}
	
	/**
	 * Set: itemSelecionadoConsulta.
	 *
	 * @param itemSelecionadoConsulta the item selecionado consulta
	 */
	public void setItemSelecionadoConsulta(ProcessoMassivoSaidaDTO itemSelecionadoConsulta) {
		this.itemSelecionadoConsulta = itemSelecionadoConsulta;
	}
	
	/**
	 * Get: comboService.
	 *
	 * @return comboService
	 */
	public IComboService getComboService() {
		return comboService;
	}

	/**
	 * Set: comboService.
	 *
	 * @param comboService the combo service
	 */
	public void setComboService(IComboService comboService) {
		this.comboService = comboService;
	}

	/**
	 * Get: listaCentroCusto.
	 *
	 * @return listaCentroCusto
	 */
	public List<CentroCustoSaidaDTO> getListaCentroCusto() {
		return listaCentroCusto;
	}

	/**
	 * Set: listaCentroCusto.
	 *
	 * @param listaCentroCusto the lista centro custo
	 */
	public void setListaCentroCusto(List<CentroCustoSaidaDTO> listaCentroCusto) {
		this.listaCentroCusto = listaCentroCusto;
	}

	/**
	 * Get: listaTipoProcesso.
	 *
	 * @return listaTipoProcesso
	 */
	public List<TipoProcessoSaidaDTO> getListaTipoProcesso() {
		return listaTipoProcesso;
	}

	/**
	 * Set: listaTipoProcesso.
	 *
	 * @param listaTipoProcesso the lista tipo processo
	 */
	public void setListaTipoProcesso(List<TipoProcessoSaidaDTO> listaTipoProcesso) {
		this.listaTipoProcesso = listaTipoProcesso;
	}

	/**
	 * Get: listaPeriodicidadeExecucao.
	 *
	 * @return listaPeriodicidadeExecucao
	 */
	public List<PeriodicidadeSaidaDTO> getListaPeriodicidadeExecucao() {
		return listaPeriodicidadeExecucao;
	}

	/**
	 * Set: listaPeriodicidadeExecucao.
	 *
	 * @param listaPeriodicidadeExecucao the lista periodicidade execucao
	 */
	public void setListaPeriodicidadeExecucao(
			List<PeriodicidadeSaidaDTO> listaPeriodicidadeExecucao) {
		this.listaPeriodicidadeExecucao = listaPeriodicidadeExecucao;
	}

	/**
	 * Get: processoMassivoFiltro.
	 *
	 * @return processoMassivoFiltro
	 */
	public ProcessoMassivoEntradaDTO getProcessoMassivoFiltro() {
		return processoMassivoFiltro;
	}

	/**
	 * Set: processoMassivoFiltro.
	 *
	 * @param processoMassivoFiltro the processo massivo filtro
	 */
	public void setProcessoMassivoFiltro(
			ProcessoMassivoEntradaDTO processoMassivoFiltro) {
		this.processoMassivoFiltro = processoMassivoFiltro;
	}

	/**
	 * Get: cadProcControleService.
	 *
	 * @return cadProcControleService
	 */
	public ICadProcControleService getCadProcControleService() {
		return cadProcControleService;
	}

	/**
	 * Set: cadProcControleService.
	 *
	 * @param cadProcControleService the cad proc controle service
	 */
	public void setCadProcControleService(
			ICadProcControleService cadProcControleService) {
		this.cadProcControleService = cadProcControleService;
	}

	/**
	 * Get: centroCustoLupa.
	 *
	 * @return centroCustoLupa
	 */
	public String getCentroCustoLupa() {
		return centroCustoLupa;
	}

	/**
	 * Set: centroCustoLupa.
	 *
	 * @param centroCustoLupa the centro custo lupa
	 */
	public void setCentroCustoLupa(String centroCustoLupa) {
		this.centroCustoLupa = centroCustoLupa;
	}

	/**
	 * Get: listaCentroCustoLupa.
	 *
	 * @return listaCentroCustoLupa
	 */
	public List<CentroCustoSaidaDTO> getListaCentroCustoLupa() {
		return listaCentroCustoLupa;
	}

	/**
	 * Set: listaCentroCustoLupa.
	 *
	 * @param listaCentroCustoLupa the lista centro custo lupa
	 */
	public void setListaCentroCustoLupa(
			List<CentroCustoSaidaDTO> listaCentroCustoLupa) {
		this.listaCentroCustoLupa = listaCentroCustoLupa;
	}

	/**
	 * Get: processoMassivoInclusao.
	 *
	 * @return processoMassivoInclusao
	 */
	public ProcessoMassivoEntradaDTO getProcessoMassivoInclusao() {
		return processoMassivoInclusao;
	}

	/**
	 * Set: processoMassivoInclusao.
	 *
	 * @param processoMassivoInclusao the processo massivo inclusao
	 */
	public void setProcessoMassivoInclusao(
			ProcessoMassivoEntradaDTO processoMassivoInclusao) {
		this.processoMassivoInclusao = processoMassivoInclusao;
	}

	/**
	 * Get: processoMassivoDetalhe.
	 *
	 * @return processoMassivoDetalhe
	 */
	public ProcessoMassivoSaidaDTO getProcessoMassivoDetalhe() {
		return processoMassivoDetalhe;
	}

	/**
	 * Set: processoMassivoDetalhe.
	 *
	 * @param processoMassivoDetalhe the processo massivo detalhe
	 */
	public void setProcessoMassivoDetalhe(
			ProcessoMassivoSaidaDTO processoMassivoDetalhe) {
		this.processoMassivoDetalhe = processoMassivoDetalhe;
	}

	/**
	 * Get: processoMassivoAlteracao.
	 *
	 * @return processoMassivoAlteracao
	 */
	public ProcessoMassivoEntradaDTO getProcessoMassivoAlteracao() {
		return processoMassivoAlteracao;
	}

	/**
	 * Set: processoMassivoAlteracao.
	 *
	 * @param processoMassivoAlteracao the processo massivo alteracao
	 */
	public void setProcessoMassivoAlteracao(
			ProcessoMassivoEntradaDTO processoMassivoAlteracao) {
		this.processoMassivoAlteracao = processoMassivoAlteracao;
	}

	/**
	 * Get: processoMassivoBloqueio.
	 *
	 * @return processoMassivoBloqueio
	 */
	public ProcessoMassivoEntradaDTO getProcessoMassivoBloqueio() {
		return processoMassivoBloqueio;
	}

	/**
	 * Set: processoMassivoBloqueio.
	 *
	 * @param processoMassivoBloqueio the processo massivo bloqueio
	 */
	public void setProcessoMassivoBloqueio(
			ProcessoMassivoEntradaDTO processoMassivoBloqueio) {
		this.processoMassivoBloqueio = processoMassivoBloqueio;
	}

	/**
	 * Get: centroCustoLupaFiltro.
	 *
	 * @return centroCustoLupaFiltro
	 */
	public String getCentroCustoLupaFiltro() {
		return centroCustoLupaFiltro;
	}

	/**
	 * Set: centroCustoLupaFiltro.
	 *
	 * @param centroCustoLupaFiltro the centro custo lupa filtro
	 */
	public void setCentroCustoLupaFiltro(String centroCustoLupaFiltro) {
		this.centroCustoLupaFiltro = centroCustoLupaFiltro;
	}

	/**
	 * Get: historicoBloqueioFiltro.
	 *
	 * @return historicoBloqueioFiltro
	 */
	public HistoricoBloqueioEntradaDTO getHistoricoBloqueioFiltro() {
		return historicoBloqueioFiltro;
	}

	/**
	 * Set: historicoBloqueioFiltro.
	 *
	 * @param historicoBloqueioFiltro the historico bloqueio filtro
	 */
	public void setHistoricoBloqueioFiltro(
			HistoricoBloqueioEntradaDTO historicoBloqueioFiltro) {
		this.historicoBloqueioFiltro = historicoBloqueioFiltro;
	}

	/**
	 * Get: itemSelecionadoHistorico.
	 *
	 * @return itemSelecionadoHistorico
	 */
	public String getItemSelecionadoHistorico() {
		return itemSelecionadoHistorico;
	}

	/**
	 * Set: itemSelecionadoHistorico.
	 *
	 * @param itemSelecionadoHistorico the item selecionado historico
	 */
	public void setItemSelecionadoHistorico(
			String itemSelecionadoHistorico) {
		this.itemSelecionadoHistorico = itemSelecionadoHistorico;
	}

	/**
	 * Get: historicoBloqueioDetalhe.
	 *
	 * @return historicoBloqueioDetalhe
	 */
	public HistoricoBloqueioSaidaDTO getHistoricoBloqueioDetalhe() {
		return historicoBloqueioDetalhe;
	}

	/**
	 * Set: historicoBloqueioDetalhe.
	 *
	 * @param historicoBloqueioDetalhe the historico bloqueio detalhe
	 */
	public void setHistoricoBloqueioDetalhe(
			HistoricoBloqueioSaidaDTO historicoBloqueioDetalhe) {
		this.historicoBloqueioDetalhe = historicoBloqueioDetalhe;
	}

	/**
	 * Get: listaSituacao.
	 *
	 * @return listaSituacao
	 */
	public List<SelectItem> getListaSituacao() {
		return listaSituacao;
	}

	/**
	 * Set: listaSituacao.
	 *
	 * @param listaSituacao the lista situacao
	 */
	public void setListaSituacao(List<SelectItem> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}

	/**
	 * Get: btoAcionado.
	 *
	 * @return btoAcionado
	 */
	public Boolean getBtoAcionado() {
		return btoAcionado;
	}

	/**
	 * Set: btoAcionado.
	 *
	 * @param btoAcionado the bto acionado
	 */
	public void setBtoAcionado(Boolean btoAcionado) {
		this.btoAcionado = btoAcionado;
	}

	/**
	 * Get: btoAcionadoHistorico.
	 *
	 * @return btoAcionadoHistorico
	 */
	public Boolean getBtoAcionadoHistorico() {
		return btoAcionadoHistorico;
	}

	/**
	 * Set: btoAcionadoHistorico.
	 *
	 * @param btoAcionadoHistorico the bto acionado historico
	 */
	public void setBtoAcionadoHistorico(Boolean btoAcionadoHistorico) {
		this.btoAcionadoHistorico = btoAcionadoHistorico;
	}

	
}
